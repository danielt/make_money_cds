<?php
	include_once dirname(__FILE__).DIRECTORY_SEPARATOR ."np_db_mysql_class.php" ;
	include_once dirname(__FILE__).DIRECTORY_SEPARATOR ."np_db_mysqli.class.php" ;
	include_once dirname(__FILE__).DIRECTORY_SEPARATOR."np_db_mysql_with_cache.class.php";
	include_once dirname(__FILE__).DIRECTORY_SEPARATOR."np_db_mysql_with_hash_table.class.php";

include_once dirname(__FILE__).DIRECTORY_SEPARATOR . 'np_db_mysqli_singleton.class.php';
function np_db_build_config( $host, $user, $passwd, $db_name,$db_type = NP_DB_TYPE_MYSQL, $db_log = NP_DB_LOG_ERROR,$db_access_weight=1,$db_process_max_limit=500 )
{
	$config = array();
	$config["host"] = $host;
	$config["user"] = $user;
	$config["passwd"] = $passwd;
	$config["db_name"] = $db_name;

	$config["db_type"] = $db_type;
	$config["db_log"] = $db_log;

	$config["db_access_weight"]=$db_access_weight;
	$config["db_process_max_limit"]=$db_process_max_limit;

	return $config;
}
function np_db_factory_create( $config )
{
	$config=!is_array($config)?array():$config;
	if( !array_key_exists( "db_type", $config ) )
		return NULL;

	$db_type = intval( $config["db_type"] );
	//echo "db_type:".$db_type;
	if( $db_type ===  NP_DB_TYPE_MYSQL ){
		if(defined('db_query_cache_enabled')&&(db_query_cache_enabled==1)&&(!empty($config['db_cache_config']))&&($config['db_cache_params_config']['ng_cache_enable']))
		{
			return new np_db_mysql_with_cache( $config );
		}
		if(defined('mysql_driver_type')&&strtolower(mysql_driver_type)==='mysqli')
		{
			return new np_db_mysqli_class( $config );
		}
		else if (defined('mysql_driver_type') && strtolower(mysql_driver_type) === 'mysqli_singleton')
        {
            return new np_db_mysqli_singleton_class($config);
        }
		return new np_db_mysql_class( $config );
	}
	if( $db_type ===  NP_DB_TYPE_HASH_TABLE_MYSQL ){
		$config["db_base_extend_class"] = 'mysql';
		if(defined('db_query_cache_enabled')&&(db_query_cache_enabled==1)&&(!empty($config['db_cache_config']))&&($config['db_cache_params_config']['ng_cache_enable']))
		{
			$config["db_base_extend_class"] = 'mysql_with_cache';
		}
		if(defined('mysql_driver_type')&&strtolower(mysql_driver_type)==='mysqli')
		{
			$config["db_base_extend_class"] = 'mysqli';
		}
		return new np_db_mysql_with_hash_table( $config );
	}

	return NULL;
}

?>