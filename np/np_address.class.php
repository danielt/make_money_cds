<?php
	/**
	 * 公共的和地址相关的操作，比如根据ip获取地址
	 * User: yxt
	 * Date: 2015/5/26
	 * Time: 15:15
	 */
	include_once dirname(__FILE__) . '/np_php_base.php';
	include_once dirname(__FILE__) . '/np_http_curl.class.php';

	class np_address
	{

		private $str_from='sina';
		/**
		 * 根据ip获取地址
		 * @author yxt
		 * @data 2015年5月26日15:40:15
		 * @param string $str_ip ip地址，不传会自动获取，自动获取的时候会过滤代理
		 * @return array $arr_re=array('ret'=>0成功，1失败，'data'=>array('country'=>国家,'province'=>省,'city'=>地区));
		 */
		private function _get_address_from_sina ($str_ip=null)
		{
			if($str_ip===null){
				$str_ip=np_get_client_ip_proxy_support();
			}
			$obj_curl=new np_http_curl_class();
			$str_url="http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json&ip=".$str_ip;
			$str_re=$obj_curl->get($str_url,null,null,5);
			$arr_re=json_decode($str_re,true);
			if(!is_array($arr_re)){
				$arr_return=array(
					'ret'=>1,
					'data'=>array(),
				);
			}else{
				$arr_return=array(
					'ret'=>0,
					'data'=>array('country'=>$arr_re['country'],'province'=>$arr_re['province'],'city'=>$arr_re['city'])
				);
			}
			return $arr_return;
		}

		/**
		 * 根据ip获取地址
		 * @author yxt
		 * @data 2015年5月26日15:40:15
		 * @param string $str_ip ip地址，不传会自动获取，自动获取的时候会过滤代理
		 * @return array $arr_re=array('ret'=>0成功，1失败，'data'=>array('country'=>国家,'province'=>省,'city'=>地区));
		 */
		public function get_address($str_ip=null){
			$str_func='_get_address_from_'.$this->str_from;
			return $this->$str_func($str_ip);
		}
	}


//	$obj_add=new np_address();
//	$a=$obj_add->get_address('58.83.191.62');
//	echo "<pre>";print_r($a);