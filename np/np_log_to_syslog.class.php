<?php
/**
 * 将日志写入rsyslog
 * @author yxt
 * @date 2016年10月12日13:52:41
 */
class np_log_to_syslog
{
	private $bool_enable=true;

    public function __construct()
    {
    	/**
    	 * 在下面的方法【set_local()】中无法获取到常量，php升级到7之后openlog()方法
    	 * 第二三个参数必须是int类型，否则会报警告错误，故重新定义
    	 * add by hanwen.liang
    	 * add time:2018-01-05
    	 * add version:5.21
    	 */
    	if(!defined('LOG_LOCAL0'))
    	{
    		define ('LOG_LOCAL0', 128);
    	}
    	if(!defined('LOG_LOCAL1'))
    	{
    		define ('LOG_LOCAL1', 136);
    	}
    	if(!defined('LOG_LOCAL2'))
    	{
    		define ('LOG_LOCAL2', 144);
    	}
    	if(!defined('LOG_LOCAL3'))
    	{
    		define ('LOG_LOCAL3', 152);
    	}
    	if(!defined('LOG_LOCAL4'))
    	{
    		define ('LOG_LOCAL4', 160);
    	}
    	if(!defined('LOG_LOCAL5'))
    	{
    		define ('LOG_LOCAL5', 168);
    	}
		if(!defined('LOG_LOCAL6'))
    	{
    		define ('LOG_LOCAL6', 176);
    	}
    	if(!defined('LOG_LOCAL7'))
    	{
    		define ('LOG_LOCAL7', 184);
    	}
    	if(!defined('LOG_ODELAY'))
    	{
    		define ('LOG_ODELAY', 4);
    	}
    }

	/**
	 * 设置local
	 * @author yxt
	 * @date 2016年10月12日13:52:41
	 * @param int $int_local 使用local几,可以是0-7
	 * @return bool 成功返回true，失败返回false
	 */
	public function set_local($int_local)
	{
		$int_tmp_local=0;
		switch($int_local)
		{
			case 0:$int_tmp_local = LOG_LOCAL0;break;
			case 1:$int_tmp_local = LOG_LOCAL1;break;
			case 2:$int_tmp_local = LOG_LOCAL2;break;
			case 3:$int_tmp_local = LOG_LOCAL3;break;
			case 4:$int_tmp_local = LOG_LOCAL4;break;
			case 5:$int_tmp_local = LOG_LOCAL5;break;
			case 6:$int_tmp_local = LOG_LOCAL6;break;
			case 7:$int_tmp_local = LOG_LOCAL7;break;
			default:$this->bool_enable=false;
		}
		if($this->bool_enable)
		{
			$this->bool_enable=openlog('STARTLOG',LOG_ODELAY,$int_tmp_local);
		}
		return $this->bool_enable;
	}

	/**
	 * 写入info的json格式的log信息
	 * @author yxt
	 * @date 2016年10月12日14:05:55
	 * @param string $str_code 错误码
	 * @param string $str_log_message 日志信息,必须是json格式
	 * @return bool 成功返回true，失败返回false
	 */
	public function write_info_json_log($str_code,$str_log_message)
	{
		return $this->_write_json_log('I',$str_code,$str_log_message);
	}

	/**
	 * 写入warning的json格式的log信息
	 * @author yxt
	 * @date 2016年10月12日14:05:55
	 * @param string $str_code 错误码
	 * @param string $str_log_message 日志信息，必须是json格式
	 * @return bool 成功返回true，失败返回false
	 */
	public function write_warning_json_log($str_code,$str_log_message)
	{
		return $this->_write_json_log('W',$str_code,$str_log_message);
	}

	/**
	 * 写入error的json格式的log信息
	 * @author yxt
	 * @date 2016年10月12日14:05:55
	 * @param string $str_code 错误码
	 * @param string $str_log_message 日志信息，必须是json格式
	 * @return bool 成功返回true，失败返回false
	 */
	public function write_error_json_log($str_code,$str_log_message)
	{
		return $this->_write_json_log('E',$str_code,$str_log_message);
	}

	/**
	 * 写入log信息
	 * @author yxt
	 * @date 2016年10月12日14:05:55
	 * @param string $str_log_level 日志等级（I/W/E）
	 * @param $str_code 错误码
	 * @param string $str_log_message 日志信息
	 * @return bool 成功返回true，失败返回false
	 */
	private function _write_json_log($str_log_level='I',$str_code,$str_log_message='')
	{
		//判断是否是json格式
		$mixed_is_json=json_decode($str_log_message);
		if($mixed_is_json===null)
		{
			return false;
		}
		//开启了才写入
		if($this->bool_enable)
		{
			$str_log_data="1<-+><-+>{$str_code}<-+>{$str_log_level}<-+>".$str_log_message;
			return syslog(LOG_INFO,$str_log_data);
		}
		return false;
	}

	public function __destruct()
	{
		closelog();
	}

}
