<?php
/**
 * Created by PhpStorm.
 * author     : Wu Xiang <xiang.wu@starcor.cn>
 * time       : 2015/7/9 11:30
 * copyright  : 2015 Starcor
 */

//加载测试文件
require_once(__DIR__."/../np_mysql.php");

class np_mysql_Test extends \PHPUnit_Framework_TestCase
{
    const PHPUNIT_SUCCESS_DB_HOST = '127.0.0.1';//正确连接MySQL的HOST
    const PHPUNIT_SUCCESS_DB_NAME = 'mysql';//正确连接的数据库名
    const PHPUNIT_SUCCESS_DB_USER = 'root';//正确连接时的用户名
    const PHPUNIT_SUCCESS_DB_PWD = 'root';//正确连接时的用户密码

    const PHPUNIT_ERROR_DB_HOST = '127.0.0.2';//错误连接MySQL的Host
    const PHPUNIT_ERROR_DB_NAME = 'xxx';//错误连接的数据库名
    const PHPUNIT_ERROR_DB_USER = 'others';//错误连接时的用户名
    const PHPUNIT_ERROR_DB_PWD = '123456';//错误连接时的用户密码
    /**
     * 单元测试开始.
     *
     * @return void
     */
    public function setUp()
    {
    }

    /**
     * 单元测试结束.
     *
     * @return void
     */
    public function tearDown()
    {
    }

    /**
     * 功能，执行指定SQL语句，返回结果
     * 问题：参数类型检查
     *
     * @dataProvider data_test_np_mysql_query
     * @param String $host 主机地址 1代表使用配置的正常参数，0代表使用配置的错误参数
     * @param String $db 数据库名 1代表使用配置的正常参数，0代表使用配置的错误参数
     * @param String $user 用户名 1代表使用配置的正常参数，0代表使用配置的错误参数
     * @param String $pwd 密码 1代表使用配置的正常参数，0代表使用配置的错误参数
     * @param String $sql SQL语句
     * @param String $input_result 预期结果类型
     *
     * @return void
     */
    public function test_np_mysql_query($host, $db, $user, $pwd, $sql, $input_result)
    {
        $result = @np_mysql_query($host, $db, $user, $pwd, $sql);
        if (is_string($input_result))
        {
            $this->assertEquals($input_result, gettype($result));
        } else
        {
            $this->assertEquals($result, $input_result);
        }
    }

    /**
     * 为test_np_mysql_query提供测试参数
     *
     * @return array
     */
    public function data_test_np_mysql_query()
    {
        return array(
            array(
                self::PHPUNIT_SUCCESS_DB_HOST,
                self::PHPUNIT_SUCCESS_DB_NAME,
                self::PHPUNIT_SUCCESS_DB_USER,
                self::PHPUNIT_SUCCESS_DB_PWD,
                'select user from user',
                'array'
            ),
            array(
                self::PHPUNIT_ERROR_DB_HOST,
                self::PHPUNIT_SUCCESS_DB_NAME,
                self::PHPUNIT_SUCCESS_DB_USER,
                self::PHPUNIT_ERROR_DB_PWD,
                '',
                false
            ),
            array(
                self::PHPUNIT_SUCCESS_DB_HOST,
                self::PHPUNIT_ERROR_DB_NAME,
                self::PHPUNIT_SUCCESS_DB_USER,
                self::PHPUNIT_SUCCESS_DB_PWD,
                'select user from user',
                false
            ),
            array(
                self::PHPUNIT_SUCCESS_DB_HOST,
                self::PHPUNIT_SUCCESS_DB_NAME,
                self::PHPUNIT_SUCCESS_DB_USER,
                self::PHPUNIT_ERROR_DB_PWD,
                'select user from userxxx',
                false
            )
            /*错误情况，使用非String类型变量
            ,array(
                array(),
                array()
            )*/
        );
    }

    /**
     * 功能，执行指定SQL语句，返回结果
     * 问题：参数类型检查
     *
     * @dataProvider data_test_np_mysql_exec
     * @param String $host 主机地址 1代表使用配置的正常参数，0代表使用配置的错误参数
     * @param String $db 数据库名 1代表使用配置的正常参数，0代表使用配置的错误参数
     * @param String $user 用户名 1代表使用配置的正常参数，0代表使用配置的错误参数
     * @param String $pwd 密码 1代表使用配置的正常参数，0代表使用配置的错误参数
     * @param String $sql SQL语句
     * @param String $input_result 预期结果
     *
     * @return void
     */
    public function test_np_mysql_exec($host, $db, $user, $pwd, $sql, $input_result)
    {
        $result = @np_mysql_exec($host, $db, $user, $pwd, $sql);
        if (!is_bool($input_result))
        {
            $this->assertEquals($input_result, gettype($result));
        } else
        {
            $this->assertEquals($result, $input_result);
        }
    }

    /**
     * 为test_np_mysql_exec提供测试参数
     *
     * @return array
     */
    public function data_test_np_mysql_exec()
    {
        return array(
            array(
                self::PHPUNIT_SUCCESS_DB_HOST,
                self::PHPUNIT_SUCCESS_DB_NAME,
                self::PHPUNIT_SUCCESS_DB_USER,
                self::PHPUNIT_ERROR_DB_PWD,
                '',
                false
            ),
            array(
                self::PHPUNIT_SUCCESS_DB_HOST,
                self::PHPUNIT_ERROR_DB_NAME,
                self::PHPUNIT_SUCCESS_DB_USER,
                self::PHPUNIT_SUCCESS_DB_PWD,
                '',
                false
            ),
            array(
                self::PHPUNIT_SUCCESS_DB_HOST,
                self::PHPUNIT_SUCCESS_DB_NAME,
                self::PHPUNIT_SUCCESS_DB_USER,
                self::PHPUNIT_SUCCESS_DB_PWD,
                'select user from user',
                'resource'
            ),
            array(
                self::PHPUNIT_SUCCESS_DB_HOST,
                self::PHPUNIT_SUCCESS_DB_NAME,
                self::PHPUNIT_SUCCESS_DB_USER,
                self::PHPUNIT_SUCCESS_DB_PWD,
                'select user from userxxx',
                false
            )
            /*错误情况，使用非String类型变量
            ,array(
                array(),
                array()
            )*/
        );
    }
}