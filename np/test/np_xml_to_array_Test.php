<?php
/**
 * Created by PhpStorm.
 * author     : Wu Xiang <xiang.wu@starcor.cn>
 * time       : 2015/7/13 10:42
 * copyright  : 2015 Starcor
 */

//加载待测试文件
require_once(__DIR__."/../np_xml_to_array.class.php");

class np_xml_to_array_Test extends \PHPUnit_Framework_TestCase
{
    /**
     * 单元测试开始.
     *
     * @return void
     */
    public function setUp()
    {
    }

    /**
     * 单元测试结束.
     *
     * @return void
     */
    public function tearDown()
    {
    }

    /**
     * 功能，将xml格式的数据转为层次不变数组
     * 问题：入参检查必须是String类型,值不能为空
     *
     * @dataProvider data_test
     * @param string $xml 需要转化的XML字符串
     * @param array $input_result 预期结果
     *
     * @return void
     */
    public function test_parse($xml, $input_result)
    {
        $result = np_xml_to_array::parse($xml);
        if (!empty($input_result))
        {
            $this->assertEquals($result, $input_result);
        }
    }

    /**
     * 为test_parse,test_parse2提供测试数据
     *
     * @return array
     */
    public function data_test()
    {
        return array(
            array(
                '<?xml version="1.0" encoding="UTF-8"?>
                <recipe>
                <recipename>Ice Cream Sundae</recipename>
                <ingredlist>
                <listitem>
                <quantity>3</quantity>
                <itemdescription>chocolate syrup or chocolate fudge</itemdescription>
                </listitem>
                <listitem>
                <quantity>1</quantity>
                <itemdescription>nuts</itemdescription>
                </listitem>
                <listitem>
                <quantity>1</quantity>
                <itemdescription>cherry</itemdescription>
                </listitem>
                </ingredlist>
                <preptime>5 minutes</preptime>
                </recipe>',
                array(
                    'recipename' => 'Ice Cream Sundae',
                    'ingredlist' => array(
                            'listitem' => array(
                                            '0' => array(
                                                    'quantity' => 3,
                                                    'itemdescription' => 'chocolate syrup or chocolate fudge'
                                                ),
                                            '1' => array(
                                                    'quantity' => 1,
                                                    'itemdescription' => 'nuts'
                                                ),
                                            '2' => array(
                                                    'quantity' => 1,
                                                    'itemdescription' => 'cherry'
                                                )
                                        )
                        ),
                    'preptime' => '5 minutes'
                ),
                ''
            ),
            array(
                '<xml>
                 <ToUserName><![CDATA[toUser]]></ToUserName>
                 <FromUserName><![CDATA[fromUser]]></FromUserName>
                 <CreateTime>1348831860</CreateTime>
                 <MsgType><![CDATA[text]]></MsgType>
                 <Content><![CDATA[this is a test]]></Content>
                 <MsgId>1234567890123456</MsgId>
                 </xml>',
                array(
                    'ToUserName' => 'toUser',
                    'FromUserName' => 'fromUser',
                    'CreateTime' => 1348831860,
                    'MsgType' => 'text',
                    'Content' => 'this is a test',
                    'MsgId' => 1234567890123456
                ),
                ''
            ),
            array(
                '<note date="13/07/2015">
                <to>George</to>
                <from>John</from>
                </note>',
                array(
                    'to' => 'George',
                    'from' => 'John',
                    'date' => '13/07/2015'
                ),
                array(
                    '@attributes' => array(
                        'date' => '13/07/2015'
                    ),
                    'to' => 'George',
                    'from' => 'John'
                )
            )
        );
    }

    /**
     * 功能，将xml格式的数据转为层次不变数组
     * 问题：入参检查必须是String类型,且判断非空
     *
     * @dataProvider data_test
     * @param string $xml 需要转化的字符串
     * @param array $input_result 预期结果
     * @param array $opt 预期结果，当此项不为空时，结果以这个参数为准
     *
     * @return void
     */
    public function test_parse2($xml, $input_result, $opt)
    {
        $result = np_xml_to_array::parse2($xml);
        if (empty($opt))
        {
            $this->assertEquals($result, $input_result);
        } else
        {
            $this->assertEquals($result, $opt);
        }
    }
}