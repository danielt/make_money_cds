<?php
/**
 * 
 * @FileName : np_php_base_Test.php
 * @Author : LiuXiao <xiao.liu@startor.cn>
 * @DateTime : 2015-07-06 14:32
 */
require_once(__DIR__."/../np_php_base.php");
class np_php_base_Test extends \PHPUnit_Framework_TestCase {

    /**
     * 单元测试开始.
     *
     * @return void
     */
    public function setUp()
    {
    }

    /**
     * 单元测试结束.
     *
     * @return void
     */
    public function tearDown()
    {
    }

    /**
     * 功能,通过检查模式读取数组值
     *
     * @dataProvider data_np_array_get_value_safe
     * @param string $reason 结果.
     * @param string $key KEY
     * @param array $data 输入数据.
     *
     * @return void
     */
    public function test_np_array_get_value_safe($data,$key,$reason)
    {
		$result = np_array_get_value_safe($data,$key);
        $this->assertEquals($reason, $result);
    }

    /**
     * 需要测试的信息.
     *
     * @return array
     */
    public function data_np_array_get_value_safe()
    {
        return array(
            array(
                array (
                    'nns_func' => 'auth_play',
                    'nns_user_id' => '0120018164234',
                    'nns_video_id' => '011224fc920f04662eca3ff9360efa28',
                ),
                'nns_func',
                'auth_play',
            ),
            array(
                array (
                    'nns_func' => 'auth_play',
                    'nns_user_id' => '0120018164234',
                    'nns_video_id' => '011224fc920f04662eca3ff9360efa28',
                ),
                'nns_user_id',
                '0120018164234',
            ),
            array(
                array (
                    'nns_func' => 'auth_play',
                    'nns_user_id' => '0120018164234',
                    'nns_video_id' => '011224fc920f04662eca3ff9360efa28',
                ),
                '啦啦啦啦',
                '',
            ),
            array(
                "",
                '啦啦啦啦',
                '',
            ),
        );
    }

    /**
     * 功能：测试给一个二维数组,根据KEY重建索引
     * 此待测试函数没有判断是否为二维数组
     *
     * @dataProvider data_test_np_array_rekey
     * @param Array $input_array 输入数组
     * @param Array $key KEY
     * @param Array $input_result 预期结果
     *
     * @return void
     */
    public function test_np_array_rekey($input_array, $key, $input_result)
    {
        $result = np_array_rekey($input_array, $key);
        $compare_result = ($result === $input_result);
        $this->assertTrue($compare_result, 'np_php_base.php FILE np_array_rekey FUNCTION FAILURES');
    }

    /**
     * 数据供给器，为test_np_array_rekey提供测试数据
     *
     * @return Array
     */
    public function data_test_np_array_rekey()
    {
        return array(
            array(//正常情况
                array(
                    array(
                        'a' => 2,
                        'b' => 3,
                        'c' => 4
                    ),
                    array(
                        'a' => 5,
                        'b' => 'di 6',
                        'c' => 'd7'
                    )),
                'a',
                array(
                    2 => array(
                        'a' => 2,
                        'b' => 3,
                        'c' => 4
                    ),
                    5 => array(
                        'a' => 5,
                        'b' => 'di 6',
                        'c' => 'd7'
                    )
                )
            ),
            array(//正常情况
                array(
                    array(
                        2 => 222,
                        'b' => 3,
                        'c' => 4
                    ),
                    array(
                        2 => 5555,
                        'b' => 'di 6',
                        'c' => 'd7'
                    )),
                2,
                array(
                    222 => array(
                        2 => 222,
                        'b' => 3,
                        'c' => 4
                    ),
                    5555 => array(
                        2 => 5555,
                        'b' => 'di 6',
                        'c' => 'd7'
                    )
                )
            ),
            /*array(//一维数组保错，未判断是否是二维数组
                array(1,2,3,4),
                0,
                array()
            ),*/
            array(//一维空数组
                array(),
                'a',
                array()
            ),
            array(//非数组
                '',
                'a',
                array()
            )
        );
    }

    /**
     * 功能,根据数组，指定长度返回其中任意元素
     * 问题：没有数组参数类型进行检查,对于关联数组KEY相同的情况不知是否需要考虑
     *
     * @dataProvider data_test_np_array_random
     * @param $arr 输入数组
     * @param $num 指定长度
     *
     * @return void
     */
    public function test_np_array_random($arr, $num)
    {
        $result = np_array_random($arr, $num);
        $compare_result = (count($result) == $num);
        foreach ($result as $value)
        {
            if (!in_array($value, $arr))
            {
                $compare_result = false;
                break;
            }
        }
        $this->assertTrue($compare_result, 'np_php_base.php FILE np_array_random FUNCTION FAILURES');
    }

    /**
     * 为test_np_array_random提供测试数据
     *
     * @return Array array
     */
    public function data_test_np_array_random()
    {
        return array(
            array(//正常情况
                array(1,2,3,4,5,6,7,8,9,0),
                5
            ),
            array(//正常情况
                array('1'=>321,'2'=>322,'3'=>3435,'4'=>3234,'5'=>7655,'6'=>87876,'7'=>6567,'8'=>87798,'9'=>76759,'10'=>213240),
                5
            ),
            array(//正常情况
                array(1=>'dsd1', 2=>'cdgv2', 3=>'dsdsd3', 4=>'ghgfh4', 5=>'htjt5', 6=>'kuik6', 7=>'ewqeq7', 8=>'kjkh8', 9=>'rew9', 10=>'qewr0'),
                5
            )
            /*,
            array(//数组与长度为空
                '',
                ''
            )
            array(//数组为空，长度为>0，运行报错，没有对入参进行检查
                '',
                5
            )*/
        );
    }

    /**
     * 功能，递归去掉数组空元素
     * 问题：没有对入参进行类型检查
     *
     * @dataProvider data_test_np_array_remove_empty
     * @param Array $arr 输入数组
     * @param Array $input_result 预期结果
     *
     * @return void
     */
    public function test_np_array_remove_empty($arr, $input_result)
    {
        $result = np_array_remove_empty($arr);
        $compare_result = ($result == $input_result);
        $this->assertTrue($compare_result, 'np_php_base.php FILE test_np_array_remove_empty FUNCTION FAILURES');
    }

    /**
     * 为test_np_array_remove_empty提供测试数据
     *
     * @return Array array
     */
    public function data_test_np_array_remove_empty()
    {
        return array(
            array(//正常情况
                array(1, 2, 3, 4, 5, 6, ''),
                array(1, 2, 3, 4, 5, 6)
            ),
            array(//正常情况
                array('dsdsdsdsew', 'dsdewewef', 'dswrtrtr', 'fdsffduyut', 5, 6, ''),
                array('dsdsdsdsew', 'dsdewewef', 'dswrtrtr', 'fdsffduyut', 5, 6)
            ),
            array(//正常情况
                array(1 => 1, 2 => 2, 3 => '3', 4 => '4', 5 => '', 6 => ''),
                array(1 => 1, 2 => 2, 3 => '3', 4 => '4')
            ),
            array(//正常情况
                array(
                    array(1, 2, 3, 4, 5, 6, ''),
                    array(1 => 1, 2 => 2, 3 => '3', 4 => '4', 5 => '', 6 => '')
                ),
                array(
                    array(1, 2, 3, 4, 5, 6),
                    array(1 => 1, 2 => 2, 3 => '3', 4 => '4')
                ),
            )
            /*
            array(//非法情况
                ''
            )*/
        );
    }

    /**
     * 功能，检查参数是否合理
     * 问题：没有入参检查
     *
     * @dataProvider data_test_np_check_params_policy
     * @param Array $params 输入数组
     * @param Array $input_result 预期结果
     *
     * @return void
     */
    public function test_np_check_params_policy($params, $input_result)
    {
        $result = np_check_params_policy($params);
        $compare_result = ($result == $input_result);
        $this->assertTrue($compare_result, 'np_php_base.php FILE test_np_check_params_policy FUNCTION FAILURES');
    }

    /**
     * 为test_np_check_params_policy提供数据
     *
     * @return array
     */
    public function data_test_np_check_params_policy()
    {
        return array(
            array(//正常情况
                array(
                    array(
                        'key' => 'nns_version',
                        'value' => 1,
                        'rule' => 'noempty'
                    )
                ),
                array(
                    'state' => true,
                    'reason' => ''
                )
            ),
            array(//正常情况
                array(
                    array(
                        'key' => 'nns_version',
                        'value' => 1,
                        'rule' => 'int'
                    )
                ),
                array(
                    'state' => true,
                    'reason' => ''
                )
            ),
            array(//正常情况
                array(
                    array(
                        'key' => 'nns_version',
                        'value' => 0,
                        'rule' => 'int'
                    )
                ),
                array(
                    'state' => true,
                    'reason' => ''
                )
            ),
            array(//错误情况
                array(
                    array(
                        'key' => 'nns_version',
                        'value' => 'test',
                        'rule' => 'int'
                    )
                ),
                array(//错误情况
                    'state' => false,
                    'reason' => 'nns_version is not int.'
                )
            ),
            array(//错误情况
                array(
                    array(
                        'key' => 'nns_version',
                        'value' => '',
                        'rule' => 'noempty'
                    )
                ),
                array(//错误情况
                    'state' => false,
                    'reason' => 'nns_version is not noempty.'
                )
            )
        );
    }

    /**
     * 功能，获取终端的IP，如果终端通过了代理，则跳过代理的IP
     * 这个待测试函数与np_get_client_ip_proxy_support相同，故np_get_client_ip_proxy_support不进行单独测试
     * 这个函数无法进行准确测试
     *
     * @return void
     */
    public function test_np_get_ip()
    {
        $this->assertEquals(np_get_ip(), '0.0.0.0');
    }

    /**
     * 功能，判断编码是否为UTF-8
     *问题：需要入参检查
     *
     * @dataProvider data_test_np_is_utf8
     * @param $needed 输入字符
     * @param $input_result 预期结果
     *
     * @return void
     */
    public function test_np_is_utf8($needed, $input_result)
    {
        $result = np_is_utf8($needed);
        $this->assertEquals($result, $input_result);
    }

    /**
     * 为test_np_is_utf8提供测试数据
     *
     * @return array
     */
    public function data_test_np_is_utf8()
    {
        return array(
            array(
                '‘’，。',
                false
            ),
            array(
                '',
                false
            )
            /*非法参数，不支持数组
            array(
                array(
                    'dsdsds'
                ),
                false
            )
            */
        );
    }

    /**
     * 功能，兼容获取请求头信息
     * CLI模式下无消息
     *
     * @return void
     */
    /*public function test_getallheaders()
    {

    }*/

    /**
     * 功能，执行Linux脚本
     *
     * @dataProvider data_test_np_exec_shell_command
     * @param String $command 需要执行的命令
     * @param Int $input_result 预期结果
     *
     * @return void
     */
    public function test_np_exec_shell_command($command, $input_result)
    {
        $result = np_exec_shell_command($command);
        $this->assertEquals($result, $input_result);
    }

    /**
     * 为test_np_exec_shell_command提供测试数据
     *
     * @return Array
     */
    public function data_test_np_exec_shell_command()
    {
        return array(
            array(
                'echo off >test.html|exi',
                0
            )
        );
    }

    /**
     * 功能，检查IP地址合法性
     * 问题：参数过滤，过滤非字符串
     *
     * @dataProvider np_check_ip_valid
     * @param String $ip IP地址
     * @param Boolean $input_result 预期结果
     *
     * @return void
     */
    public function test_np_check_ip_valid($ip, $input_result)
    {
        $this->assertEquals(np_check_ip_valid($ip), $input_result);
    }

    /**
     * 为test_np_check_ip_valid提供数据
     *
     * @return Array array
     */
    public function np_check_ip_valid()
    {
        return array(
            array(
                '127.0.0.1',
                true
            ),
            array(
                'X.X.X.X',
                false
            ),
            array(
                '',
                false
            )
            /*,不支持数组
            array(
                array(
                    '127.0.0.1'
                ),
                true
            )*/
        );
    }

    /**
     * 获取代理的IP，如果终端未通过代理，则返回终端的IP
     * 这个待测试函数与np_get_client_ip_proxy_support相同，故np_get_client_ip_proxy_support不进行单独测试
     * 这个函数无法进行准确测试
     * @return void
     */
    public function test_np_get_client_ip_remote_support()
    {
        $this->assertEquals(np_get_client_ip_remote_support(), '0.0.0.0');
    }

    /**
     * 功能，二维组据按照某列排序
     *
     * @dataProvider data_test_np_multi_array_sort
     * @param Array $multi_array 需要排序的数组
     * @param  String $sort_key 排序依据的键名
     * @param Int $sort 升序、倒序
     * @param Array $input_result 预期结果
     *
     * @return void
     */
    public function test_np_multi_array_sort($multi_array, $sort_key, $sort, $input_result)
    {
        $result = np_multi_array_sort($multi_array, $sort_key, $sort);
        $compare_result = ($result == $input_result);
        $this->assertTrue($compare_result, 'np_php_base.php FILE test_np_multi_array_sort FUNCTION FAILURES');
    }

    /**
     * 为test_np_multi_array_sort提供测试数据
     *
     * @return array
     */
    public function data_test_np_multi_array_sort()
    {
        return array(
            array(
                array(
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>76
                    ),
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>176
                    ),
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>276
                    )
                ),
                'iu',
                SORT_DESC,
                array(
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>276
                    ),
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>176
                    ),
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>76
                    )
                )
            ),
            array(
                array(
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>76
                    ),
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>176
                    ),
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>276
                    )
                ),
                'iu',
                SORT_ASC,
                array(
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>76
                    ),
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>176
                    ),
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>276
                    )
                )
            ),
            array(
                array(
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>76
                    ),
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>176
                    ),
                    22222
                ),
                'iu',
                SORT_ASC,
                false
            )
            /*不存在的KEY，执行报错
            array(
                array(
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>76
                    ),
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>176
                    ),
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>276
                    )
                ),
                'dddiu',
                SORT_ASC,
                array(
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>76
                    ),
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>176
                    ),
                    array(
                        'dsds'=>2,'dsdsfds'=>54,'iu'=>276
                    )
                )
            )
            */
        );
    }
}