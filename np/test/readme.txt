基于np库代码的单元测试说明

命名规则说明：
1.测试文件全部位于test文件中。
2.测试文件命名采用待测试文件名_Test.php的方式。如np_address.class.php对应np_address_Test.php。get_address对应test_get_address方法。

需求环境说明：
1.正确安装PHPUnit，部分集成环境如xampp已经自带该库。
2.PHPUnit3.7及以上版本。
3.正确添加环境变量 执行'phpunit'命令查看详细使用情况。
4.官方安装说明文档地址：https://phpunit.de/manual/current/zh_cn/installation.html

运行配置说明；
1.确保Mysql服务已经启动。
2.修改test文件中的np_mysql_Test.php。根据提示进行常量配置。

运行参数说明：
首先进入np文件夹目录，
1.普通运行 使用'phpunit ./test' 此命令是执行test文件夹中全部PHPUnit。
2.生成代码覆盖率报告 使用'phpunit --coverage-html ./report ./test' 在当前路径的report文件夹(不存在自动创建，已经存在覆盖)生成html格式的test文件夹中的PHPUnit的覆盖率报告。