<?php
/**
 * Created by PhpStorm.
 * author     : Wu Xiang <xiang.wu@starcor.cn>
 * time       : 2015/7/10 11:24
 * copyright  : 2015 Starcor
 */
//加载待测试类文件
require_once(__DIR__."/../curl.class.php");

class curl_Test extends \PHPUnit_Framework_TestCase
{
    //申明curl对象
    public $curl;
    /**
     * 单元测试开始.
     *
     * @return void
     */
    public function setUp()
    {
        $config = array(
            //访问的端口,http默认是 80
            'port' => 80,
            //客户端 USERAGENT,如:"Mozilla/4.0",为空则使用用户的浏览器
            'userAgent' => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36',
            //连接超时时间
            'timeOut' => 30,
            //是否使用 COOKIE 建议打开，因为一般网站都会用到
            'useCookie' => true,
            //是否支持SSL
            'ssl' => false,
            //客户端是否支持 gzip压缩
            'gzip' => true,

            //是否使用代理
            'proxy' => false,
            //代理类型,可选择 HTTP 或 SOCKS5
            'proxyType' => 'HTTP',
            //代理的主机地址,如果是 HTTP 方式则要写成URL形式如:"http://www.proxy.com"
            //SOCKS5 方式则直接写主机域名为IP的形式，如:"192.168.1.1"
            'proxyHost' => 'http://npss.hifuntv.com',
            //代理主机的端口
            'proxyPort' => 1234,
            //代理是否要身份认证(HTTP方式时)
            'proxyAuth' => false,
            //认证的方式.可选择 BASIC 或 NTLM 方式
            'proxyAuthType' => 'BASIC',
            //认证的用户名和密码
            'proxyAuthUser' => 'user',
            'proxyAuthPwd' => 'password',
        );
        $this->curl = new curl($config);
    }

    /**
     * 单元测试结束.
     *
     * @return void
     */
    public function tearDown()
    {
    }

    /**
     * 功能，以 GET 方式执行请求
     *
     * @dataProvider data_test_get
     * @param string $url 需要访问的URL
     *
     * @return void
     */
    public function test_get($url)
    {
        $this->assertTrue((Boolean)$this->curl->get($url));
    }

    /**
     * 功能，获取Header信息
     *
     * @dataProvider data_test_get
     * @param $url
     */
    public function test_getInfo($url){
        $this->curl->get($url);
        $this->assertTrue((Boolean)$this->curl->getInfo());
    }

    /**
     * 为test_get提供测试数据
     *
     * @return array
     */
    public function data_test_get()
    {
        return array(
            array(
                'http://www.jd.com/?cu=true&utm_source=baidu-pinzhuan&utm_medium=cpc&utm_campaign=t_288551095_baidupinzhuan&utm_term=0f3d30c8dba7459bb52f2eb5eba8ac7d_0_ed4d6fddd9bc43e38cecf532e61acc07'
            )
        );
    }

    /**
     * 功能，获取错误信息
     *
     * @dataProvider data_test_error
     * @param $url
     */
    public function test_error_errno($url)
    {
        $this->curl->get($url);
        $this->assertTrue((Boolean)$this->curl->error());
        $this->assertTrue(is_int($this->curl->errno()));
    }

    /**
     * 提供测试数据
     *
     * @return array
     */
    public function data_test_error()
    {
        return array(
            array(
                'https://baidu.com.404.php/'
            )
        );
    }

    /**
     * 功能，POST访问
     *
     * @dataProvider data_test_post
     * @param $url
     *
     * @return void
     */
    public function test_post($url)
    {
        $this->assertTrue((Boolean)$this->curl->post($url));
    }

    /**
     * @return array
     */
    public function data_test_post()
    {
        return array(
            array(
                'http://php.net/'
            )
        );
    }
}