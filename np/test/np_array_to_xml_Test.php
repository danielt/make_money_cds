<?php
/**
 * Created by PhpStorm.
 * author     : Wu Xiang <xiang.wu@starcor.cn>
 * time       : 2015/7/13 16:13
 * copyright  : 2015 Starcor
 */

//加载测试类
require_once(__DIR__."/../np_array_to_xml.class.php");

class np_array_to_xml_Test extends \PHPUnit_Framework_TestCase
{
    /**
     * 单元测试结束.
     *
     * @return void
     */
    public function setUp()
    {
    }

    /**
     * 单元测试结束.
     *
     * @return void
     */
    public function tearDown()
    {
    }

    /**
     * 功能，将数组转化成xml数据
     * 问题：入参检查
     *
     * @dataProvider data_test_array_to_xml
     * @param array $arr 需要转化的数组
     * @param string $input_result 预期结果
     *
     * @return void
     */
    public function test_array_to_xml($arr, $input_result)
    {

        $sum = '';
        similar_text($input_result, np_array_to_xml::array_to_xml($arr), $sum);
        $this->assertTrue(($sum > 99));
    }

    public function data_test_array_to_xml()
    {
        return array(
            array(
                array(
                    'to' => 'George',
                    'from' => 'John',
                    'date' => '13/07/2015'
                ),
                '<?xml version="1.0" encoding="utf-8" ?>
<root><to>George</to><from>John</from><date>13/07/2015</date></root>'
            ),
            array(
                array(
                    'ToUserName' => 'toUser',
                    'FromUserName' => 'fromUser',
                    'CreateTime' => 1348831860,
                    'MsgType' => 'text',
                    'Content' => 'this is a test',
                    'MsgId' => 1234567890123456
                ),
                '<?xml version="1.0" encoding="utf-8" ?>
<root><ToUserName>toUser</ToUserName><FromUserName>fromUser</FromUserName><CreateTime>1348831860</CreateTime>
<MsgType>text</MsgType><Content><![CDATA[this is a test]]></Content><MsgId><![CDATA[1.2345678901235E+15]]></MsgId></root>'
            ),
            array(
                array
                (
                    "weather" => array
                    (
                        "0" => array(
                            "high" => "37",
                            "low" => "22",
                            "weather1" => "雷阵雨",
                            "weather2" => "雷阵雨",
                            "wind1" => "无持续风向微风",
                            "wind2" => "无持续风向微风",
                            "weather1_img" => array(),
                            "weather2_img" => array(),
                        ),
                        "1" => array(
                            "high" => "32",
                            "low" => "23",
                            "weather1" => "雷阵雨",
                            "weather2" => "雷阵雨",
                            "wind1" => "无持续风向微风",
                            "wind2" => "无持续风向微风",
                            "weather1_img" => array(),
                            "weather2_img" => array()
                        ),
                        "2" => array(
                            "high" => "30",
                            "low" => "21",
                            "weather1" => "雷阵雨",
                            "weather2" => "雷阵雨",
                            "wind1" => "无持续风向微风",
                            "wind2" => "无持续风向微风",
                            "weather1_img" => array(),
                            "weather2_img" => array()
                        )
                    ),
                    "city" => "石景山",
                    "date" => "2015年07月14日",
                    "tip" => array
                    (
                        "0" => array
                        (
                            "advice" => "有降水，较不宜晨练，室外锻炼请携带雨具。建议年老体弱人群适当减少晨练时间。",
                            "name" => "晨练指数",
                            "level" => "较不宜"
                        ),
                        "1" => array
                        (
                            "advice" => "白天虽然天气以阴为主，但由于天热，加上湿度较大，您会感到很闷热，很不舒适。",
                            "name" => "舒适度指数",
                            "level" => "很不舒适"
                        ),
                        "2" => array
                        (
                            "advice" => "天气炎热，建议着短衫、短裙、短裤、薄型T恤衫等清凉夏季服装。",
                            "name" => "穿衣指数",
                            "level" => "炎热"
                        )
                    ),
                    "result" => array
                    (
                        "state" => 0,
                        "reason" => "OK"
                    )

                ),
                '<?xml version="1.0" encoding="utf-8" ?>
<root><weather><item><high>37</high><low>22</low><weather1><![CDATA[雷阵雨]]></weather1><weather2><![CDATA[雷阵雨]]></weather2><wind1><![CDATA[无持续风向微风]]></wind1><wind2><![CDATA[无持续风向微风]]></wind2><weather1_img></weather1_img><weather2_img></weather2_img></item><item><high>32</high><low>23</low><weather1><![CDATA[雷阵雨]]></weather1><weather2><![CDATA[雷阵雨]]></weather2><wind1><![CDATA[无持续风向微风]]></wind1><wind2><![CDATA[无持续风向微风]]></wind2><weather1_img></weather1_img><weather2_img></weather2_img></item><item><high>30</high><low>21</low><weather1><![CDATA[雷阵雨]]></weather1><weather2><![CDATA[雷阵雨]]></weather2><wind1><![CDATA[无持续风向微风]]></wind1><wind2><![CDATA[无持续风向微风]]></wind2><weather1_img></weather1_img><weather2_img></weather2_img></item></weather><city><![CDATA[石景山]]></city><date><![CDATA[2015年07月14日]]></date><tip><item><advice><![CDATA[有降水，较不宜晨练，室外锻炼请携带雨具。建议年老体弱人群适当减少晨练时间。]]></advice><name><![CDATA[晨练指数]]></name><level><![CDATA[较不宜]]></level></item><item><advice><![CDATA[白天虽然天气以阴为主，但由于天热，加上湿度较大，您会感到很闷热，很不舒适。]]></advice><name><![CDATA[舒适度指数]]></name><level><![CDATA[很不舒适]]></level></item><item><advice><![CDATA[天气炎热，建议着短衫、短裙、短裤、薄型T恤衫等清凉夏季服装。]]></advice><name><![CDATA[穿衣指数]]></name><level><![CDATA[炎热]]></level></item></tip><result><state>0</state><reason>OK</reason></result></root>'
            ),
            array(
                array(
                    'test' => array(
                        'NL_XML_ATTRIBUTE' => array(
                            'one' => 1
                        )
                    )
                ),
                '<?xml version="1.0" encoding="utf-8" ?>
<root><test one="1" ></test></root>'
            )
        );
    }

    /**
     *
     * @dataProvider data_test_convert
     * @param $arr
     * @param $input_result
     *
     * @return void
     */
    public function test_convert($arr, $input_result)
    {
        $obj = new np_array_to_xml('root');
        $this->assertFalse(($obj->convert($arr) == $input_result));
    }

    public function data_test_convert()
    {
        return array(
            array(
                array(
                    'test' => 'just test'
                ),
                '<pre><?xml version="1.0" encoding="utf-8"?>
<root name="test"/>'
            )
        );
    }
}