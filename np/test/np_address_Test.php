<?php
/**
 * Created by PhpStorm.
 * author     : Wu Xiang <xiang.wu@starcor.cn>
 * time       : 2015/7/10 14:30
 * copyright  : 2015 Starcor
 */

//加载待测试类
require_once(__DIR__."/../np_address.class.php");

class np_address_Test extends \PHPUnit_Framework_TestCase
{
    /**
     * 单元测试开始.
     *
     * @return void
     */
    public function setUp()
    {
    }

    /**
     * 单元测试结束.
     *
     * @return void
     */
    public function tearDown()
    {
    }

    /**
     * 功能，获取IP所在国家，省，市信息
     *
     * @dataProvider data_test_get_address
     * @param string $ip
     *
     * @return void
     */
    public function test_get_address($ip)
    {
        $obj_add = new np_address();
    	$this->assertTrue((Boolean)$obj_add->get_address($ip));
    }

    /**
     * 为test_get_address提供测试数据
     *
     * @return array
     */
    public function data_test_get_address()
    {
        return array(
            array(
                '58.83.191.62'
            ),
            array(
                '58.83.191.62'
            ),
            array(
                '127.0.0.1'
            ),
            array(
                null
            )
        );
    }
}