<?php
/**
 * Created by PhpStorm.
 * author     : Wu Xiang <xiang.wu@starcor.cn>
 * time       : 2015/7/8 14:56
 * copyright  : 2015 Starcor
 */

//加载需要测试的文件
require_once(__DIR__."/../np_string.php");

class np_string_Test extends \PHPUnit_Framework_TestCase
{
    /**
     * 单元测试开始.
     *
     * @return void
     */
    public function setUp()
    {
    }

    /**
     * 单元测试结束.
     *
     * @return void
     */
    public function tearDown()
    {
    }

    /**
     * 功能，安全的获取字符串长度
     *
     * @dataProvider data_test_np_strlen_safe
     * @param $string
     * @param $input_result
     *
     * @return void
     */
    public function test_np_strlen_safe($string, $input_result)
    {
        $this->assertEquals($input_result, np_strlen_safe($string));
    }

    /**
     * 为test_np_strlen_safe提供测试数据
     *
     * @return array
     */
    public function data_test_np_strlen_safe()
    {
        return array(
           array(
               '12345678',
               8
           ),
            array(
                array(),
                0
            ),
            array(
                12333333,
                0
            ),
            array(
                '这是测试',
                12//8
            )
        );
    }

    /**
     * 功能，将输入的字符串转化为ASCII值，再转化成16进制值
     * 问题：没有检查入参类型
     *
     * @dataProvider data_test_np_c1_encode
     * @param $str
     * @param $input_result
     */
    public function test_np_c1_encode($str, $input_result)
    {
        $this->assertEquals($input_result, np_c1_encode($str));
    }

    /**
     * 为test_np_c1_encode提供数据
     *
     * @return array
     */
    public function data_test_np_c1_encode()
    {
        return array(
            array(
                'hello world',
                '68656c6c6f20776f726c64'
            ),
            array(
                123456,
                '000000'
            )
            /*错误情况
            array(
                array(),
                '000000'
            ),
            array(
                123456,
                '000000'
            ) */
        );
    }

    /**
     * 功能，对应np_c1_encode解码
     * 问题：没有检查入参类型
     *
     * @dataProvider data_test_np_c1_decode
     * @param $str
     * @param $input_result
     *
     * @return void
     */
    public function test_np_c1_decode($str, $input_result)
    {
        $this->assertEquals($input_result, np_c1_decode($str));
    }

    /**
     * 为test_np_c1_decode提供数据
     *
     * @return array
     */
    public function data_test_np_c1_decode()
    {
        return array(
            array(
                '68656c6c6f20776f726c64',
                'hello world'
            )
            /*错误情况
            array(
                array(),
                '000000'
            ),
            array(
                '000000',
                123456
            )
            */
        );
    }

    /**
     * 功能，生成随机32位GUID
     * 问题：没有进行入参检查
     *
     * @dataProvider data_test_np_guid_rand
     * @param $rand
     *
     * @return void
     */
    public function test_np_guid_rand($rand)
    {
        $result = np_guid_rand($rand);
        $this->assertEquals(32, strlen($result));
    }

    /**
     * 为test_np_guid_rand提供数据
     *
     * @return array
     */
    public function data_test_np_guid_rand()
    {
        return array(
            array(
                'test'
            )/*错误情况
            array(
                array(),
            )*/
        );
    }

    /**
     * 功能，检查GUID参数的合法性
     *
     * @dataProvider data_test_np_guid_check_valid
     * @param $str
     * @param $input_result
     *
     * @return void
     */
    public function test_np_guid_check_valid($str, $input_result)
    {
        $this->assertEquals($input_result, np_guid_check_valid($str));
    }

    /**
     * 为test_np_guid_check_valid提供测试数据
     *
     * @return array
     */
    public function data_test_np_guid_check_valid()
    {
        return array(
            array(
                '51dce172120b72299c79005fd043f62a',
                true
            ),
            array(
                '1234567890-=qw24v5h3hgs,.trf.,64',
                false
            )
             ,array(
                array(),
                false
            )
        );
    }

    /**
     * 功能，从一个URL请求串，分解出请求中的GET参数
     * 问题：缺少对URL的检查，当URL不包含？时报错,不是string类型报错
     *
     * @dataProvider data_test_np_build_query_from_url_string
     * @param $url
     * @param $decode
     * @param $input_result
     */
    public function test_np_build_query_from_url_string($url, $decode, $input_result)
    {
        $this->assertEquals($input_result,np_build_query_from_url_string($url, $decode));
    }

    /**
     * 为test_np_build_query_from_url_string提供测试数据
     *
     * @return array
     */
    public function data_test_np_build_query_from_url_string()
    {
        return array(
            array(
                'http://www.starcor.cn/index.php?m=content&c=index&a=lists&catid=9',
                false,
                array(
                    'm' => 'content',
                    'c' => 'index',
                    'a' => 'lists',
                    'catid' => 9
                )
            ),
            array(
                'http://www.starcor.cn/index.php?m=content&c=index&a=lists&catid=%e6%b5%8b%e8%af%95',
                true,
                array(
                    'm' => 'content',
                    'c' => 'index',
                    'a' => 'lists',
                    'catid' => '测试'
                )
            ),
            array(
                'http://www.starcor.cn/index.php?m=content&c=index&a=lists&catid=%e6%b5%8b%e8%af%95',
                false,
                array(
                    'm' => 'content',
                    'c' => 'index',
                    'a' => 'lists',
                    'catid' => '%e6%b5%8b%e8%af%95'
                )
            )
            /*错误情况
            array(
                'http://www.starcor.cn/index.php',
                false,
                array(
                    'm' => 'content',
                    'c' => 'index',
                    'a' => 'lists',
                    'catid' => '%e6%b5%8b%e8%af%95'
                )
            ),
            array(
                array('http://www.starcor.cn/index.php?m=content&c=index&a=lists&catid=%e6%b5%8b%e8%af%95'),
                false,
                array(
                    'm' => 'content',
                    'c' => 'index',
                    'a' => 'lists',
                    'catid' => '%e6%b5%8b%e8%af%95'
                )
            )
            */
        );
    }

    /**
     * 功能，使用指定字符串替换掉、，、|字符
     * 问题：入参检查，String类型
     *
     * @dataProvider data_test_np_build_split_string
     * @param String $str 需要替换的字符串
     * @param $split_str 指定的替换以后的字符
     * @param $space 是否替换字符串中空格
     * @param $input_result 预期结果
     *
     * @return void
     */
    public function test_np_build_split_string($str, $split_str, $space, $input_result)
    {
        $this->assertEquals($input_result, np_build_split_string($str, $split_str, $space));
    }

    /**
     * 为test_np_build_split_string提供测试数据
     *
     * @return array
     */
    public function data_test_np_build_split_string()
    {
        return array(
            array(
                '12、',
                'xxx',
                false,
                '12xxx'
            ),
            array(
                12,
                'xxx',
                false,
                12
            ),
            array(
                '12、，|,',
                'xxx',
                false,
                '12xxxxxxxxxxxx'
            ),
            array(
                ' 12、，|, ',
                'xxx',
                false,
                '12xxxxxxxxxxxx'
            ),
            array(
                '124312dffdad',
                'xxx',
                false,
                '124312dffdad'
            ),
            array(
                '124312 dffdad',
                'xxx',
                true,
                '124312xxxdffdad'
            )
            /*错误情况，需要替换的变量不能是数组
            array(
                array(
                    'xxx'
                ),
                'xxx',
                false,
                'xxx'
            )
            */
        );
    }

    /**
     * 功能，生成url安全的base64字符串
     * 问题：入参检查，String类型
     *
     * @dataProvider data_test_np_base64url_encode
     * @param String $str URL
     *
     * @return void
     */
    public function test_np_base64url_encode($str)
    {
        $base64 = np_base64url_encode($str);
        $this->assertTrue((strpos($base64, '+')?false:true), 'np_string.php FILE np_base64url_encode FUNCTION FAILURES');
        $this->assertFalse(substr($base64, -1) == '=', 'np_string.php FILE np_base64url_encode FUNCTION FAILURES');
    }

    /**
     * 为test_np_base64url_encode提供测试数据
     *
     * @return array
     */
    public function data_test_np_base64url_encode()
    {
        return array(
           array(
               123443
           ),
           array(
               'http://localhost/test.php='
           ),
            array(
                'http://php.net/manual/zh/function.strpos.php'
            )
            /*错误情况
            array(
                array()
            )
            */
        );
    }

    /**
     * 功能，解码base64字符串
     * 问题：入参检查，String类型
     *
     * @dataProvider data_test_np_base64url_decode
     * @param String $str 编码后的字符串
     * @param String $input_result 预期结果
     *
     * @return void
     */
    public function test_np_base64url_decode($str, $input_result)
    {
        $result = np_base64url_decode($str);
        $this->assertEquals($result, $input_result);
    }

    /**
     * 为test_np_base64url_decode提供测试数据
     *
     * @return array
     */
    public function data_test_np_base64url_decode()
    {
        return array(
            array(
                'aHR0cHM6Ly93d3cuc2l0ZXBlbi5jb20vYmxvZy8yMDA4LzA3LzE2L2pzb25xdWVyeS1kYXRhLXF1ZXJ5aW5nLWJleW9uZC1qc29ucGF0aC8',
                'https://www.sitepen.com/blog/2008/07/16/jsonquery-data-querying-beyond-jsonpath/'
            ),
            array(
                'aHR0cDovL3d3dy5mYXFzLm9yZy9yZmNzLw',
                'http://www.faqs.org/rfcs/'
            ),
            array(
                'MTIzNDQz',
                123443
            )
            /*错误情况，不能是数组
            array(
                array(),
                ''
            )
             */
        );
    }
}