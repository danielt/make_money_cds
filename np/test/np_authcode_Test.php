<?php
/**
 * Created by PhpStorm.
 * author     : Wu Xiang <xiang.wu@starcor.cn>
 * time       : 2015/7/10 10:20
 * copyright  : 2015 Starcor
 */

//加载待测试文件
require_once(__DIR__."/../np_authcode.php");

class np_authcode_Test extends \PHPUnit_Framework_TestCase
{
    /**
     * 单元测试开始.
     *
     * @return void
     */
    public function setUp()
    {
    }

    /**
     * 单元测试结束.
     *
     * @return void
     */
    public function tearDown()
    {
    }

    /**
     * 功能，使用内置函数的加密解密函数
     * 问题：入参类型检查
     *
     * @dataProvider data_test_np_authcode
     * @param $string 需要加密的字符串或者是密文
     * @param string $operation 操作-加密，解密
     * @param string $key 秘钥
     * @param int $expiry 有效期
     * @param $input_result 预期结果
     *
     * @return void
     */
    public function test_np_authcode($string, $operation = 'ENCODE',$key='', $expiry = 0, $input_result)
    {
        $result = np_authcode($string, $operation, $key, $expiry);
        $this->assertEquals($result, $input_result);
    }

    /**
     * 为test_np_authcode提供测试数据
     *
     * @return array
     */
    public function data_test_np_authcode()
    {
        return array(
            array(
                'hello world',
                'ENCODE',
                'just test',
                1200,
                '99AA2DD13940D281F20A6B5AAE00425E'
            ),
            array(
                '99AA2DD13940D281F20A6B5AAE00425E',
                'DECODE',
                'just test',
                1200,
                'hello world'
            )
            /*错误情况，使用数字或者是string类型
            array(
                array(),
                array(),
                array(),
                array(),
                array()
            )
             */
        );
    }

    /**
     * 功能。使用加密算法进行加密解密
     * 问题：入参检查
     *
     * @dataProvider data_test_np_authcode_old
     * @param $string
     * @param $operation
     * @param $key
     * @param $expiry
     * @param $input_result
     *
     * @return void
     */
    public function test_np_authcode_old($string, $operation, $key, $expiry, $input_result)
    {
        $result = np_authcode_old($string, $operation, $key, $expiry);
        if ($operation == 'ENCODE')
        {
            $decode_result = np_authcode_old($result, 'DECODE', $key, $expiry);
            $this->assertEquals($decode_result, $string);
        } else
        {
            $this->assertEquals($result, $input_result);
        }
    }

    public function data_test_np_authcode_old()
    {
        return array(
            array(
                'hello world',
                'ENCODE',
                'just test',
                1200000,
                '13fdX22pZMqnSkpQaKaGjjBElJ6yYc4hoXAGz5ALI230kp8RVbTL2Q'
            ),
            array(
                '13fdX22pZMqnSkpQaKaGjjBElJ6yYc4hoXAGz5ALI230kp8RVbTL2Q',
                'DECODE',
                'just test',
                1200000,
                'hello world'
            ),
            array(
                '13fdX22pZMqnSkpQaKaGjjBElJ6yYc4hoXAGz5ALI230kp8RVbTL2Q',
                'DECODE',
                '结果为空',
                1200000,
                ''
            )
            /*错误情况，使用数字或者是string类型
            array(
                array(),
                array(),
                array(),
                array(),
                array()
            )
             */
        );
    }
}