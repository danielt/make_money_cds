<?php
/**
 * Created by PhpStorm.
 * author     : Wu Xiang <xiang.wu@starcor.cn>
 * time       : 2015/7/13 14:17
 * copyright  : 2015 Starcor
 */

//加载测试文件
require_once(__DIR__."/../np_xml2array.class.php");

class np_xml2array_Test extends \PHPUnit_Framework_TestCase
{
    /**
     * 单元测试开始.
     *
     * @return void
     */
    public function setUp()
    {
    }

    /**
     * 单元测试结束.
     *
     * @return void
     */
    public function tearDown()
    {
    }

    /**
     * 功能，将想xml字符串转化为数组
     * 问题：始终出现Notice错误
     *
     * @dataProvider data_test_test_getArrayData
     * @param $xml
     * @param $input_result
     *
     * @return void
     */
    public function test_getArrayData($xml, $input_result)
    {
        $result = @np_xml2array::getArrayData($xml);
        $this->assertEquals($result, $input_result);
    }

    /**
     * @return array
     */
    public function data_test_test_getArrayData()
    {
        return array(
            array(
                '<xml>
                <ToUserName><![CDATA[toUser]]></ToUserName>
                <FromUserName><![CDATA[fromUser]]></FromUserName>
                <CreateTime>1348831860</CreateTime>
                <MsgType><![CDATA[text]]></MsgType>
                <Content><![CDATA[this is a test]]></Content>
                <MsgId>1234567890123456</MsgId>
                </xml>',
                array(
                    'ret' => 1,
                    'reason' => '成功',
                    'data' => array(
                        'name' => 'xml',
                        'namespace' => '',
                        'namespaces' => array(
                        ),
                        'children' => array(
                            '0' => array(
                                'name' => 'ToUserName',
                                'content' => 'toUser'
                            ),
                            '1' => array(
                                'name' => 'FromUserName',
                                'content' => 'fromUser'
                            ),
                            '2' => array(
                                'name' => 'CreateTime',
                                'content' => 1348831860
                            ),
                            '3' => array(
                                'name' => 'MsgType',
                                'content' => 'text'
                            ),
                            '4' => array(
                                'name' => 'Content',
                                'content' => 'this is a test'
                            ),
                            '5' => array(
                                'name' => 'MsgId',
                                'content' => 1234567890123456
                            )
                        )
                    )
                )
            ),
            array(
                '',
                array(
                    'ret' => 0,
                    'reason' => '解析失败'
                )
            ),
            array(
                '<?xml version="1.0" encoding="ISO-8859-1"?>
                <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
                <xsl:template match="/">
                <html>
                <body>
                  <h2>My CD Collection</h2>
                  <table border="1">
                    <tr>
                      <th align="left">Title</th>
                      <th align="left">Artist</th>
                    </tr>
                    <xsl:for-each select="catalog/cd">
                    <tr>
                      <td><xsl:value-of select="title"/></td>
                      <td><xsl:value-of select="artist"/></td>
                    </tr>
                    </xsl:for-each>
                  </table>
                </body>
                </html>
                </xsl:template>
                </xsl:stylesheet>',
                array(
                    'ret' => 1,
                    'reason' => '成功',
                    'data' => array(
                        'name' => 'stylesheet',
                        'namespace' => 'xsl',
                        'namespaces' => array(
                            'xsl' => 'http://www.w3.org/1999/XSL/Transform'
                        ),
                        'children' => array(
                            '0' => array(
                                'name' => 'template',
                                'namespace' => 'xsl',
                                'children' => array(
                                    '0' => array(
                                        'name' => 'html',
                                        'children' => array(
                                            '0' => array(
                                                'name' => 'body',
                                                'children' => array(
                                                    '0' => array(
                                                        'name' => 'h2',
                                                        'content' => 'My CD Collection'
                                                    ),
                                                    '1' => array(
                                                        'name' => 'table',
                                                        'children' => array(
                                                            '0' => array(
                                                                'name' => 'for-each',
                                                                'namespace' => 'xsl',
                                                                'children' => array(
                                                                    '0' => array(
                                                                        'name' => 'tr',
                                                                        'children' => array(
                                                                            '0' => array(
                                                                                'name' => 'td',
                                                                                'children' => array(
                                                                                    '0' => array(
                                                                                        'name' => 'value-of',
                                                                                        'namespace' => 'xsl',
                                                                                        'attributes' => array(
                                                                                            'select' => 'title'
                                                                                        )
                                                                                    )
                                                                                )
                                                                            ),
                                                                            '1' => array(
                                                                                'name' => 'td',
                                                                                'children' => array(
                                                                                    '0' => array(
                                                                                        'name' => 'value-of',
                                                                                        'namespace' => 'xsl',
                                                                                        'attributes' => array(
                                                                                            'select' => 'artist'
                                                                                        )
                                                                                    )
                                                                                )
                                                                            )
                                                                        )
                                                                    )
                                                                ),
                                                                'attributes' => array(
                                                                    'select' => 'catalog/cd'
                                                                )
                                                            ),
                                                            '1' => array(
                                                                'name' => 'tr',
                                                                'children' => array(
                                                                    '0' => array(
                                                                        'name' => 'th',
                                                                        'content' => 'Title',
                                                                        'attributes' => array(
                                                                            'align' => 'left'
                                                                        )
                                                                    ),
                                                                    '1' => array(
                                                                        'name' => 'th',
                                                                        'content' => 'Artist',
                                                                        'attributes' => array(
                                                                            'align' => 'left'
                                                                        )
                                                                    )
                                                                )
                                                            )
                                                        ),
                                                        'attributes' => array(
                                                            'border' => 1
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                ),
                                'attributes' => array(
                                    'match' => '/'
                                )
                            )
                        ),
                        'attributes' => array(
                            'version' => '1.0'
                        )
                    )
                )
            )
        );
    }
}