<?php
/**
 * Created by PhpStorm.
 * author     : Wu Xiang <xiang.wu@starcor.cn>
 * time       : 2015/7/8 10:03
 * copyright  : 2015 Starcor
 */

//引入待测试文件
require_once(__DIR__."/../np_time.php");

class np_time_Test extends \PHPUnit_Framework_TestCase
{
    /**
     * 单元测试开始.
     *
     * @return void
     */
    public function setUp()
    {
    }

    /**
     * 单元测试结束.
     *
     * @return void
     */
    public function tearDown()
    {
    }

    /**
     * 功能，读取毫秒，并取整数
     * 没有准确的测试，通过判断是Int类型且值小于1000
     *
     * @return void
     */
    public function test_np_millisecond()
    {
        $time = np_millisecond();
        if (is_int($time) && $time < 1000)
        {
            $compare_result = true;
        } else
        {
            $compare_result = false;
        }
        $this->assertTrue($compare_result, 'np_time.php FILE np_millisecond FUNCTION FAILURES');
    }

    /**
     * 功能，返回一个当前时间的float类型的时间戳
     *
     * @return void
     */
    public function test_np_millisecond_f()
    {
        $time = np_millisecond_f();
        if (!is_double($time))
        {
            $compare_result = false;
        } else
        {
            $compare_result = true;
        }
        $this->assertTrue($compare_result, 'np_time.php FILE np_millisecond_f FUNCTION FAILURES');
    }

    /**
     * 功能，用来代码测时获取代码运行时间
     * 由于需要进行测试，所以代码sleep了1秒
     * @return void
     */
    public function test_np_runtime()
    {
        $this->assertEquals('0.0', np_runtime(false));
        $begin = np_runtime(false);
        sleep(1);
        $end = np_runtime(true);
        if (!is_double($end) || $end - $begin < 1)
        {
            $compare_result = false;
        } else
        {
            $compare_result = true;
        }
        $this->assertTrue($compare_result,  'np_time.php FILE np_runtime FUNCTION FAILURES');
    }

    /**
     * 功能，根据相应标签返回程序执行时间
     * 由于需要进行测试，所以代码sleep了1秒
     * 问题：需要对$tag类型进行检测
     *
     * @return void
     */
    public function test_np_runtime_ex()
    {
        $begin = np_runtime_ex('test', false);
        $this->assertEquals('0.0', $begin);
        sleep(1);
        $time = np_runtime_ex('test', true);
        if (!is_double($time) || $time - $begin < 1)
        {
            $compare_result = false;
        } else
        {
            $compare_result = true;
        }
        $this->assertTrue($compare_result, 'np_time.php FILE np_runtime FUNCTION FAILURES');
    }

    /**
     * 功能，将ISO时间转化成GMT时间
     * 问题：参数检查不完整，只判断了长度
     * @dataProvider data_test_np_time_from_iso
     * @param $iso_time 需要转化的时间
     * @param $input_result 预期结果
     *
     * @return void
     */
    public function test_np_time_from_iso($iso_time, $input_result)
    {
        $this->assertEquals($input_result, np_time_from_iso($iso_time));
    }

    /**
     * 为test_np_time_from_iso提供测试数据
     *
     * @return array
     */
    public function data_test_np_time_from_iso()
    {
        return array(
            array(
                '20120102T030405',
                '1325473445',
            ),
            array(
                '20150708T084206',
                '1436344926'
            )
            /*错误情况
            array(
                'zzzzzzzzzzzzzzz',
                ''
            )*/
        );
    }

    /**
     * 功能，生成时间随机数字GUID
     * 问题：参数类型检查
     *
     * @dataProvider data_test_np_millitime_rand
     * @param $num
     *
     * @return void
     */
    public function test_np_millitime_rand($num)
    {
        $len = $num;
        if ($len < 16)
        {
            $len = 16;
        } else
        {
            $len = $num;
        }
        $this->assertEquals($len, strlen(np_millitime_rand($num)));
    }

    /**
     * 为test_np_millitime_rand提供测试数据
     *
     * @return array
     */
    public function data_test_np_millitime_rand()
    {
        return array(
            array(0),
            array('xxxx'),
            array(16),
            array(32),
            array(1000)
            /*错误情况
            array(
                array()
            )*/
        );
    }
}