<?php
class asset_category_model
{
    public static function get_asset_category($id = null, $orderBy = null,$sp_id=null)
    {
        $sql = 'SELECT * FROM `nns_mgtvbk_category`';
        if (ctype_alpha($id)) {
            $sql .= " WHERE `nns_id`='{$id}'";
        }
        if ($orderBy) {
            list($col, $policy) = each($orderBy);
            $sql .= " ORDER BY {$col} {$policy}";
        }
        $db = nn_get_db(NL_DB_READ);
     
        return nl_query_by_db($sql, $db);
    }
    static public function get_category_all($spid){
    	$db = nn_get_db(NL_DB_READ);
		$sql = "SELECT * FROM `nns_mgtvbk_category` where nns_org_id='$spid' ";
		return nl_db_get_all($sql, $db);
    }
    public static function get_asset_category_list($sp_id){
    	//
    	$sql = "SELECT * FROM `nns_mgtvbk_category` where nns_org_id='$sp_id' ";
        $db = nn_get_db(NL_DB_READ);

        return nl_query_by_db($sql, $db);    	
    }
    public static function add_asset_category($data)
    {
    	$sort = 0;
    	if(empty($data['nns_sp_category_id'])){
    		$data['nns_sp_category_id'] = $data['nns_id'];
    	}
        $db = nn_get_db(NL_DB_WRITE);
        $sql = "INSERT INTO `nns_mgtvbk_category` (
            `nns_id`,
            `nns_org_id`,
            `nns_sp_category_id`,
            `nns_name`,
            `nns_create_time`,
            `nns_modify_time`
            
        ) VALUES (
            '$data[nns_id]',
            '$data[nns_org_id]',
            '$data[nns_sp_category_id]',
            '$data[nns_name]',
            '$data[nns_create_time]',
            '$data[nns_modify_time]'
            
        )";
        $result = nl_execute_by_db($sql, $db);
        if($result){
        	
        	$category_info = array(
                   'nns_category_id'=>$data['nns_sp_category_id'],
                   'nns_parent_id'=>substr($data['nns_sp_category_id'],0,-3),
                   'nns_name'=>$data['nns_name'],
                    'nns_sort'=>$sort,
                    'nns_task_id'=>$data['nns_sp_category_id'],
                    'nns_task_name'=>$data['nns_name'],
                    );
        	c2_task_model::add_categoy( $category_info);
        }
        return $result;
    }
    public static function syn_asset_category($data){
         	$category_info = array(
               'nns_category_id'=>$data['nns_sp_category_id'],
               'nns_parent_id'=>substr($data['nns_sp_category_id'],0,-3),
               'nns_name'=>$data['nns_name'],
               'nns_sort'=>0,
               'nns_task_id'=>$data['nns_sp_category_id'],
               'nns_task_name'=>$data['nns_name'],               
         	);
        	return c2_task_model::add_categoy( $category_info);
    }

    public static function exists_asset_category($nns_id,$nns_org_id){
    	$db = nn_get_db(NL_DB_READ);
    	$sql = "select * from nns_mgtvbk_category where nns_id='$nns_id' and nns_org_id='$nns_org_id'";
    	$q = nl_query_by_db($sql,$db);
    	if($q===true){
    		return false;
    	}else{
    		return true;
    	}
    }
    
    public static function modify_asset_category($arrData, $condition)
    {
        if (count($condition) == 0) {
            trigger_error('That condition must be array');
        } 
        $setBlock = $whereBlock = array();
        foreach ($arrData as $col => $val) {
            $setBlock[] = "$col='$val'";
        }
        foreach ($condition as $col => $val) {
            $whereBlock[] = "$col='$val'";
        }
        $whereBlock = implode(' AND ', $whereBlock);
        $setBlock = implode(',', $setBlock);
        $sql = "UPDATE `nns_mgtvbk_category` SET $setBlock WHERE $whereBlock";
        $db = nn_get_db(NL_DB_WRITE);
 
        $result =  nl_execute_by_db($sql, $db);
        if($result){
        	$db = nn_get_db(NL_DB_READ);
        	$sql = "select nns_sp_category_id from nns_mgtvbk_category where nns_id='".$condition['nns_id']."' and nns_org_id='{$condition['nns_org_id']}'";
        	$nns_sp_category_id = nl_db_get_col($sql,$db);
        	$category_info = array(
				'nns_category_id'=>$nns_sp_category_id,
				'nns_parent_id'=>substr($nns_sp_category_id,0,-3),
				'nns_name'=>$arrData['nns_name'],
				//'nns_sort'=>$arrData['nns_sort'],
				'nns_task_id'=>$nns_sp_category_id,
				'nns_task_name'=>$arrData['nns_name'],

        	);
        	c2_task_model::update_category($category_info);
        }        
         return $result;
        
    }
    public static function delete_asset_category($id, $org_id,$delSon = false, $condition = null)
    {
    	$db = nn_get_db(NL_DB_READ);
    	$sql = "select * from nns_mgtvbk_category where nns_id='".$id."' and nns_org_id='$org_id'";
        $category_info = nl_db_get_one($sql,$db);
    	$category_info = array(
		'nns_id'=>$category_info['nns_sp_category_id'],
		'nns_task_id'=> $id,
		'nns_task_name'=> $category_info['nns_name'],
		);
    	$result = c2_task_model::delete_category( $category_info);
    	if($result==false){
    		return false;
    	}
            	
        if ($delSon) {
            $likeId = "LIKE '$id%'";
        } else {
            $likeId = '';
        }
        $whereBlock = array();
        if ($condition) foreach ($condition as $col => $val) {
            $whereBlock[] = "$col='$val'";
        }
        $whereBlock = implode(' AND ', $whereBlock);
        $sql = "DELETE FROM `nns_mgtvbk_category` WHERE `nns_id`='$id' and nns_org_id='$org_id'  $likeId $whereBlock";
        $db = nn_get_db(NL_DB_WRITE);

        $result = nl_execute_by_db($sql, $db);

        return $result;
    }
    public static function sort_asset_category($id,$direct='up'){
        $db_w = nn_get_db(NL_DB_WRITE);
   	
        $sql = null;  	
    	switch($direct){
			case 'top':
			$sql = "update nns_mgtvbk_category SET nns_sort=999 where nns_id='$id'";
			break;    		
			case 'bottom':
			$sql = "update nns_mgtvbk_category SET nns_sort=0 where nns_id='$id'";
			break;
		
    	}
		if($sql != null){
			return nl_execute_by_db($sql,$db_w);
		}    	
    	

        
	
 
        $sql = null;  	
    	switch($direct){
			case 'top':
			$sql = "update nns_mgtvbk_category SET nns_sort=999 where nns_id='$id'";
			break;    		
    		case 'up':
    		$sql = "update nns_mgtvbk_category SET nns_sort=LEAST(999,nns_sort+1) ,nns_modify_time=now() where nns_id='$id'";
    		break;
    		case 'down':
    		$sql = "update nns_mgtvbk_category SET nns_sort=GREATEST(0,nns_sort-1)  where nns_id='$id'";
    		break;
			case 'bottom':
			$sql = "update nns_mgtvbk_category SET nns_sort=0 where nns_id='$id'";
			break;	
			default:
			 $sql = "update nns_mgtvbk_category SET  nns_sort=0 where nns_id='$id'";  		
    	}
		if($sql != null){
			nl_execute_by_db($sql,$db_w);
			
	
	        return true;		
		}
		
		
		
		return false;    	
    }

    public static function build_tree_use_array($arrData, $rootName, $rootId, $categoryId = null) {
        $return_str = '<script type="text/javascript">';
        $return_str .= 'd = new dTree("d");';
        $firstSpCategoryId = $arrData[0]['nns_sp_category_id'];
        if ($categoryId) {
            $return_str .= 'd.add("' . $categoryId . '",' .
                '"-1",' .
                '"' . $rootName . '",' .
                '"javascript:select_tree_item(' .
                '\'' . $rootId . '\',' .
                '\'' . $rootName . '\',' .
                '\'0\',' .
                '\'\'' .
                ')",' .
                '"0");';
        } else {
            $return_str .= 'd.add("10000",' .
                '"-1",' .
                '"' . $rootName . '",' .
                '"javascript:select_tree_item(' .
                '\'' . $rootId . '\',' .
                '\'' . $rootName . '\',' .
                '\'0\',' .
                '\'\'' .
                ')",' .
                '"0");';
        }

        if (is_array($arrData)) {
            foreach ($arrData as $item) {
                $parentId = substr($item['nns_id'], 0, -3);
                $return_str .= 'd.add("' . $item['nns_id'] . '",' .
                    '"' . $parentId . '",' .
                    '"' . $item['nns_name'] . '",' .
                    '"javascript:select_tree_item(' .
                    '\'' . $item['nns_id'] . '\',' .
                    '\'' . $item['nns_name'] . '\',' .
                    '\'' . $parentId . '\',' .
                    '\'' . $item['nns_sp_category_id'] . '\'' .
                    ')",' .
                    '"' . $item['nns_id'] . '","'.$item['nns_sp_category_id'].'");';
                    $return_str .="\r\n";
            }
        }
        $return_str .= 'document.write(d);';
        $return_str .= '$(document).ready(function(){';
        $return_str .= 'var node=d.selectedNode;if (!node) node=0;' .
            'var node_id=d.aNodes[node].id;' .
            'var node_parent_id=d.aNodes[node].pid;' .
            'var node_name=d.aNodes[node].name;' .
            'if (node_id==0){node_id="' . $rootId . '";}' .
            'if (node_parent_id==-1){node_parent_id=0;}' .
            'var node_data=d.aNodes[node].data;' .
            'select_tree_item(node_id,node_name,node_parent_id,node_data);';
        $return_str .= '});';
        $return_str .= '</script>';

        return $return_str;
    }


	//绑定资源科栏目
	/*
	 * $params = array(	
	'sp_id' => $_GET['sp_id'],
	'sp_category_id' => $_GET['sp_category_id'],
	'bind_category_id' => $_GET['bind_category_id'],//需要绑定地栏目ID
	);
	 * 
	 */ 
	static public function bind_vod_item($params){		
		$db_w = nn_get_db(NL_DB_WRITE);
		$spid = $params['sp_id'];
		$sp_category_id = $params['sp_category_id'];
		$bind_category_id = $params['bind_category_id'];
		$video_type = $params['video_type'];
		$sql = "update nns_mgtvbk_category set bind_vod_item='{$bind_category_id}',nns_video_type='$video_type' where nns_org_id='$spid' and nns_sp_category_id='{$sp_category_id}'";
		$re =  nl_execute_by_db($sql,$db_w);
		if($re){
			return 0;
		}else{
			return 1;
		}
	}

	/**
	 * 获取绑定
	 */
	static public function get_bind_vod_item($sp_id,$nns_id)
	{
			$db_w = nn_get_db(NL_DB_WRITE);
			$sql = "select bind_vod_item,nns_video_type from  nns_mgtvbk_category where nns_org_id='$sp_id' and nns_id='{$nns_id}'";
			$re =  nl_db_get_one($sql,$db_w);
			if(empty($re['bind_vod_item'])){
				if(empty($re['nns_video_type'])){
					$re['nns_video_type']=2;
				}
				return array('data'=>1,'video_type'=>$re['nns_video_type']);
			}
			if($re){
				$category_name = "";
				if($re['nns_video_type']=='0'){					
					$sql = "select * from nns_depot where nns_org_type='0' and nns_type='0'";
					$result = nl_db_get_one($sql,$db_w);unset($sql);				
					$dom = new DOMDocument('1.0', 'utf-8');
					$dom->loadXML($result['nns_category']);
					$categorys = $dom->getElementsByTagName('category');
					foreach ($categorys as $category) {
						if ($category->getAttribute('id') === (string)$re['bind_vod_item']) {
							$category_name = $category->getAttribute('name');						
							break;
						}
					}
				}elseif($re['nns_video_type']=='1'){
					$sql = "select * from nns_depot where nns_org_type='0' and nns_type='1'";
					$result = nl_db_get_one($sql,$db_w);unset($sql);				
					$dom = new DOMDocument('1.0', 'utf-8');
					$dom->loadXML($result['nns_category']);
					$categorys = $dom->getElementsByTagName('category');
					foreach ($categorys as $category) {
						if ($category->getAttribute('id') === (string)$re['bind_vod_item']) {
							$category_name = $category->getAttribute('name');						
							break;
						}
					}
				}				
				return array('data'=>0,'name'=>$category_name,'sort'=>0,'video_type'=>$re['nns_video_type']);
			}else{
				return array('data'=>1,'video_type'=>2);
			}
	}
	
	/**
	 * 解除绑定
	 */
	static public function unbind_vod_item($spid,$nns_id){
		$db_w = nn_get_db(NL_DB_WRITE);
		$sql = "update nns_mgtvbk_category set bind_vod_item=null,nns_video_type=null   where nns_org_id='$spid' and nns_id='{$nns_id}'";
		$re =  nl_execute_by_db($sql,$db_w);	
		if($re){
			return 0;
		}else{
			return 1;
		}
	}
}	







