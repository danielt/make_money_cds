<?php
//echo $upload_path = dirname(dirname(dirname(__FILE__))).'/data/downimg/prev/KsImg/';
class index_img_model {

	static public function edit($arr, $file) {
		include 'nn_upload.class.php';
		$nns_image_id = np_guid_rand();
		$upload_path = dirname(dirname(dirname(__FILE__))) . '/data/downimg/prev/KsImg/';
		if (!is_dir($upload_path)) {
			mkdir($upload_path, 0777, true);
		}
		$upload_config = array('upload_path' => $upload_path);
		$upload = new nn_upload_class($upload_config);
		if ($upload -> do_upload('nns_img')) {
			$image_url1 = $upload -> file_name;
		} else {
			$image_url1 = '';
		}
		$db = nn_get_db(NL_DB_WRITE);
		$db_r = nn_get_db(NL_DB_READ);
		$nns_img = 'prev/KsImg/' . $image_url1;
		$sql = "update nns_mgtvbk_index_img set nns_img='$nns_img' where nns_id='" . $arr['nns_id'] . "'";
		$re = nl_execute_by_db($sql, $db);
		if ($re) {
			$sql = "select img.*,vod.nns_name,vod.nns_all_index from nns_mgtvbk_index_img as img left join nns_vod as vod on img.nns_video_id=vod.nns_id
		where   img.nns_org_id='$sp_id' and img.nns_id ='" . $arr['nns_id'] . "'";
			$list = nl_db_get_one($sql, $db_r);
			if ($list['nns_all_index'] == 1) {
				$nns_id = nl_db_get_col("select nns_id from nns_vod_index where nns_vod_id='{$list['nns_video_id']}'", $db);
				$flag = 0;
			} else {
				$nns_id = $list['nns_video_id'];
				$flag = 1;
			}
			$data = array('nns_id' => $nns_id, 'flag'=>$flag, 'nns_img' => $list['nns_img'], 'nns_task_id' => $list['nns_video_id'], 'nns_task_name' => $list['nns_name'], );
			c2_task_model::do_img_delete($data);
		}

		return $re;

	}


	static public function import_category($sp_id, $nns_id, $action){
		$db = nn_get_db(NL_DB_READ);
		$db_w = nn_get_db(NL_DB_WRITE);
		if (empty($nns_id)) {
			return false;
		}
		$arr = explode(',', $nns_id);
		array_pop($arr);

		$ids = "'" . implode("','", $arr) . "'";
		$sql = "select img.*,vod.nns_name,vod.nns_all_index from nns_mgtvbk_index_img as img left join nns_vod as vod on img.nns_video_id=vod.nns_id
		where   img.nns_org_id='$sp_id' and img.nns_id in ($ids)";
		$list = nl_db_get_all($sql, $db);
		
		
		foreach ($list as $re) {
			if (is_array($re) && $re != null) {
				if ($action == 'reg') {	
				if (empty($re['nns_img'])) {
					continue;
				}
				}
				if ($re['nns_all_index'] == 1) {
					$flag = 0;
					$nns_id = nl_db_get_col("select nns_id from nns_vod_index where nns_vod_id='{$re['nns_video_id']}'", $db);
				} else {
					$nns_id = $re['nns_video_id'];
					$flag = 1;
				}
				$data = array('category_id'=>10000010,'nns_id' => $nns_id,'action'=>$action, 'flag'=>$flag,'nns_img' => $re['nns_img'],  'nns_task_name' => $re['nns_name'].'--绑定栏目', );
				$result = c2_task_model::band_category($data);
				if ($result) {
					$sql = "update nns_mgtvbk_index_img set nns_status=0,nns_img=null where nns_id='{$re['nns_id']}'";
					nl_execute_by_db($sql, $db_w);
				}				
			}
		}
	}

	static function import($sp_id, $nns_id, $action) {
		$db = nn_get_db(NL_DB_READ);
		$db_w = nn_get_db(NL_DB_WRITE);
		if (empty($nns_id)) {
			return false;
		}
		$arr = explode(',', $nns_id);
		array_pop($arr);

		$ids = "'" . implode("','", $arr) . "'";
		$sql = "select img.*,vod.nns_name,vod.nns_all_index from nns_mgtvbk_index_img as img left join nns_vod as vod on img.nns_video_id=vod.nns_id
		where   img.nns_org_id='$sp_id' and img.nns_id in ($ids)";
		$list = nl_db_get_all($sql, $db);
		foreach ($list as $re) {
			if (is_array($re) && $re != null) {
				if ($action == 'reg') {	
				if (empty($re['nns_img'])) {
					continue;
				}
				}
				if ($re['nns_all_index'] == 1) {
					$flag = 0;
					$nns_id = nl_db_get_col("select nns_id from nns_vod_index where nns_vod_id='{$re['nns_video_id']}'", $db);
				} else {
					$nns_id = $re['nns_video_id'];
					$flag = 1;
				}
				$data = array('nns_id' => $nns_id, 'flag'=>$flag,'nns_img' => $re['nns_img'], 'nns_task_id' => $re['nns_video_id'], 'nns_task_name' => $re['nns_name'], );
				if ($action == 'reg') {
					$result = c2_task_model::do_img($data);
					if ($result) {
						$sql = "update nns_mgtvbk_index_img set nns_status=2 where nns_id='{$re['nns_id']}'";
						nl_execute_by_db($sql, $db_w);
					}
				} elseif ($action == 'del') {
					$result = c2_task_model::do_img_delete($data);
					if ($result) {
						$sql = "update nns_mgtvbk_index_img set nns_status=1,nns_img=null where nns_id='{$re['nns_id']}'";
						nl_execute_by_db($sql, $db_w);
					}
				}
			}
		}
		return true;
	}

	static public function list_index($sp_id, $offset, $limit) {
		$db = nn_get_db(NL_DB_READ);
		$sql = "select img.*,vod.nns_name from nns_mgtvbk_index_img as img left join nns_vod as vod on img.nns_video_id=vod.nns_id
		where  img.nns_org_id='$sp_id'";
		$sql .= " order by img.nns_create_time desc ";
		$sql .= " limit $offset,$limit";
		$count_sql = "select count(*) as temp_count from nns_mgtvbk_index_img where nns_org_id='$sp_id'";
		$data = nl_db_get_all($sql, $db);
		$rows = nl_db_get_col($count_sql, $db);
		return array('data' => $data, 'rows' => $rows);
	}

	static public function get_info($nns_id) {
		$db = nn_get_db(NL_DB_READ);
		$sql = "select img.*,vod.nns_name from nns_mgtvbk_index_img as img left join nns_vod as vod on img.nns_video_id=vod.nns_id
		where  img.nns_id='$nns_id'";
		return nl_db_get_one($sql, $db);

	}

	static public function bind($sp_id, $nns_id) {
		$db_w = nn_get_db(NL_DB_WRITE);
		$db_r = nn_get_db(NL_DB_READ);
		$sql = "select * from nns_mgtvbk_category_content where nns_id='$nns_id' and nns_video_type=0";
		$info = nl_db_get_one($sql, $db_r);
		$vod_id = $info['nns_video_id'];
		$sql = "select * from nns_vod where nns_id='$vod_id'";
		$vod_info = nl_db_get_one($sql, $db_r);
		$c2_id = $vod_id;
		$type='vod';
		if($vod_info['nns_all_index']==1){
			$c2_id = nl_db_get_col("select nns_id from nns_vod_index where nns_vod_id='$vod_id'", $db_r);
			$type='index';
		}		
		//查询是否已经注入
		$sql="select * from nns_mgtvbk_c2_task where nns_org_id='$sp_id' and nns_type='$type' and nns_ref_id='$c2_id'";
		//die($sql);
		$c2_info = nl_db_get_one($sql, $db_r);
		
		if (empty($c2_info)) {
			return 2;
		}
		
		if($c2_info['nns_action']=='destroy'||($c2_info['nns_action']=='add'&&$c2_info['nns_status']!=0)){
			return 2;
		}
		
		if (empty($info)) {
			return 1;
		}
		$table = 'nns_mgtvbk_index_img';
		$data = array('nns_id' => np_guid_rand(), 'nns_video_id' => $info['nns_video_id'], 'nns_org_id' => $sp_id, 'nns_status' => 1,
		'nns_create_time' => date('Y-m-d H:i:s'),
		);
		$re = nl_db_insert($db_w, $table, $data);
		if ($re === false) {
			return 1;
		} else {
			return 0;
		}
	}

}
?>