<?php
//include_once dirname(dirname(__FILE__)).'/nn_db.class.php';
class sp_model{
	static public function get_sp_list($sp_id=null){
		$db = nn_get_db(NL_DB_READ);
		
		$sql  = "select * from nns_mgtvbk_sp";
		if($sp_id!=null){
			$sql  = "select * from nns_mgtvbk_sp where nns_id='".$sp_id."'";
		}
		

		
		return nl_db_get_all($sql,$db);
	}
	static public function get_sp_info($sp_id){
		$db = nn_get_db(NL_DB_READ);

		$sql ="select * from nns_mgtvbk_sp where nns_id='".$sp_id."'";
		return nl_db_get_one($sql,$db);
	}
	static public function exists_sp($id){
		$db = nn_get_db(NL_DB_READ);
	
		$sql = "select 1 from nns_mgtvbk_sp where nns_id='".$id."'";
		$result = nl_db_get_all($sql,$db);
		if(is_array($result) && count($result)>0){
			return true;
		}else{
			return false;
		}
	}
	static public function add_sp($sp_info){
		$db = nn_get_db(NL_DB_WRITE);

		$sp_info['nns_create_time'] = date('Y-m-d H:i:s');
		//$sp_info['nns_rule'] = isset($sp_info['nns_rule']) ? implode(',',$sp_info['nns_rule']): null;
		return nl_db_insert($db,'nns_mgtvbk_sp',$sp_info);
	}
	static public function update_sp($sp_info){
		$db = nn_get_db(NL_DB_WRITE);
		
		

		$set = array(
		'nns_name'=>$sp_info['nns_name'],
		'nns_modify_time'=>date('Y-m-d H:i:s'),
		'nns_telphone' =>  $sp_info['nns_telphone'],
		'nns_email' =>  $sp_info['nns_email'],
		'nns_contact' =>  $sp_info['nns_contact'],
		//'nns_rule' =>  isset($sp_info['nns_rule'] ) ? implode(',',$sp_info['nns_rule']) : null,
		);
		$where = "nns_id='".$sp_info['nns_id']."'";
		return nl_db_update($db,'nns_mgtvbk_sp',$set,$where);
	}
	static public function delete_sp($sp_id){
		$db = nn_get_db(NL_DB_WRITE);

		$sql = "delete from nns_sp where nns_id='".$sp_id."'";
	}
	static public function batch_delete_sp($sp_ids){
		$db = nn_get_db(NL_DB_WRITE);
	
		$sql = "delete from nns_mgtvbk_sp where ".nl_db_in($sp_ids,'nns_id');
		return nl_execute_by_db($sql,$db);
	}

}
