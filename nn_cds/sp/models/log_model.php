<?php
class log_model{
	static public function log_write($log_summary){
		$db = nn_get_db(NL_DB_WRITE);
		$table='nns_op_log';
		$nns_mgr_name = isset($_SESSION["nns_mgr_name"]) ? $_SESSION["nns_mgr_name"] : null;
		$nns_mgr_id = isset($_SESSION["nns_mgr_id"]) ? $_SESSION["nns_mgr_id"] : null;
		$action = SP_ORG_ID;
		$nns_manager_type = isset($_SESSION["nns_manager_type"]) ? $_SESSION["nns_manager_type"] : null;
		$nns_org_id = isset($_SESSION["nns_org_id"]) ? $_SESSION["nns_org_id"] : null;
		
		$data = array(
			'nns_id'=>np_guid_rand(),
			'nns_op_admin_id'=>$nns_mgr_id,
			'nns_op_type'=>$action,
			'nns_op_time'=>date('Y-m-d H:i:s'),
			'nns_op_desc'=>$log_summary,
			'nns_org_type'=>$nns_manager_type,
			'nns_org_id'=>$nns_org_id,
		);
		nl_db_insert($db, $table, $data);
		
	}	
}
?>