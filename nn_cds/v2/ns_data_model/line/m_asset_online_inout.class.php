<?php
/**
 *
 * @author: jing.chen
 * @date: 2017/12/26 9:41
 */
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_data_model.m_data_model");
\ns_core\m_load::load_old("nn_logic/asset_online/asset_online.class.php");
\ns_core\m_load::load_old("nn_logic/asset/asset_incidence_relation.class.php");
class m_asset_online_inout extends m_data_model
{
    /**
     * 添加操作参数检查模板
     * @var array
     */
    public $add_params = array(
        'base_info'     =>      array(
            'nns_message_id'       =>array(
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-64',
                'desc'      => '消息ID',
            ),
             'nns_video_name'       =>array(
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-255',
                'desc'      => '影片名称',
            ),
            'nns_import_id'       =>array(
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-128',
                'desc'      => '媒资注入id',
            ),
            'nns_category_id'       =>array(
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-64',
                'desc'      => '媒资栏目',
            ),
            'nns_order'       =>array(
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '主媒资序号',
            ),
            'nns_action'       =>array(
                'rule'      => 'noempty',
                'default'   => '0',
                'length'    => '1-8',
                'desc'      => '操作 0 online 上线,1 unline 下线',
            ),
            'nns_cp_id'       =>array(
                'rule'      => 'noempty',
                'default'   => '0',
                'length'    => '1-32',
                'desc'      => 'CP',
            ),
            'nns_vod_id'       =>array(
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '上下线媒资id',
            ),
            'nns_type'       =>array(
                'rule'      => 'noempty',
                'default'   => 'video',
                'length'    => '1-32',
                'desc'      => '上下线媒资类型',
            ),
            'nns_asset_id'       =>array(
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '媒资包id',
            ),
        ),
        'hide_info'     =>      array(
            'nns_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => 'GUID',
                'length'    => '32',
                'desc'      => '打点GUID',
            ),
            'nns_org_id'       =>array(
                'rule'      => 'noempty',
                'default'   => '0',
                'length'    => '1-32',
                'desc'      => '运营商id',
            ),
            'nns_audit'       =>array(
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '审核状态 1 审核通过，0审核不通过',
            ),
            'nns_status'       =>array(
                'rule'      => 'tinyint',
                'default'   => '1',
                'length'    => '',
                'desc'      => '状态 0 成功 | 1 等待注入 | 2 注入失败 | 3 等待反馈 | 4 反馈失败 | 5 主媒资不是最终状态 | 6 分集不是最终状态 | 7 片源不是最终状态',
            ),
            'nns_delete'       =>array(
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '删除状态 0 未删除 | 1已删除',
            ),
            'nns_create_time'       =>array(
                'rule'      => 'datetime',
                'default'   => '',
                'length'    => '',
                'desc'      => '创建时间',
            ),
            'nns_modify_time'       =>array(
                'rule'      => 'datetime',
                'default'   => '0000-00-00 00:00:00',
                'length'    => '',
                'desc'      => '修改时间',
            ),
            'nns_fail_time'       =>array(
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '注入失败时间',
            ),
        )
    );
    /**
     * 检查注入参数
     * @param array $in_params 注入参数
     * @param string $action 操作参数行为
     * @return array array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function check_input_params($in_params,$action)
    {
        $str_params = "{$action}_params";
        if (!isset($this->$str_params))
        {
            return self::return_data(1, '没有设置' . $str_params . '参数的判断模式');
        }
        if (!is_array($this->$str_params))
        {
            return self::return_data(1, '参数非数组');
        }
        return $this->_check_input_params($this->$str_params, $in_params);
    }

    /**
     * 添加媒资上下线队列
     * @param array $params 添加数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function add($params)
    {
        //根据CP查询绑定的SP
        $arr_sp = m_config::_get_cp_bind_sp_info($params['base_info']['nns_cp_id']);
        if (!is_array($arr_sp['data_info']) || empty($arr_sp['data_info']))
        {
            return self::return_data(NS_CDS_FAIL, '没有查询到相关cp绑定关系，cp_id为:' . $params['base_info']['nns_cp_id']);
        }
        $temp_array = array();
        //先检查上下线数据是否存在
        $check_video_exist = nl_asset_incidence_relation::query_asset_relation(\m_config::get_dc(), NS_CDS_VIDEO, $params['base_info']['nns_import_id'],  $params['base_info']['nns_cp_id'], 0);
        if (!is_array($check_video_exist['data_info']) || empty($check_video_exist['data_info']))
        {
            return self::return_data(NS_CDS_FAIL, "检查主媒资在平台不存在或已删除.原因" . $check_video_exist['reason']);
        }
        $params['base_info']['nns_video_name'] = $check_video_exist['data_info'][0]['nns_name'];
        $params['base_info']['nns_vod_id'] = $check_video_exist['data_info'][0]['nns_id'];

        foreach ($arr_sp['data_info'] as $sp_val)
        {
            $sp_config = $sp_val['nns_config'];
            //检查媒资上下线队列开关是否开启  如果未开启 直接continue
            if (!isset($sp_config['online_disabled']) || $sp_config['online_disabled'] != 1)
            {
                $temp_array[] = 'SP为:' . $sp_val . '的媒资上下线开关未开启,值为' . $sp_config['online_disabled'];
                $this->write_message_info('SP为:' . $sp_val . '的媒资上下线开关未开启,值为' . $sp_config['online_disabled']);
                continue;
            }
            //0 为进入媒资上下线为自动审核通过，1 为审核不通过，2为下线审核不通过/上线自动审核通过
            $online_disabled = (isset($sp_config['online_audit']) && in_array($sp_config['online_audit'], array(0, 1, 2))) ? $sp_config['online_audit'] : 1;
            $nns_audit = 0;
            if ($params['base_info']['nns_action'] == 'online')
            {
                if ($online_disabled == '0' || $online_disabled == '2')
                {
                    $nns_audit = 1;
                }
            } else if ($params['base_info']['nns_action'] == 'unline')
            {
                if ($online_disabled == '0')
                {
                    $nns_audit = 1;
                }
            }
            else
            {
                $temp_array[] = 'video_id为：' . $params['base_info']['video_id'] . ';SP_ID为:' . $sp_val[0]['nns_id'] . '的操作行为为:' . $params['nns_action'] . ',但是只有online、unline可走逻辑';
                continue;
            }
            $params['hide_info']=array(
                'nns_org_id' => $sp_val[0]['nns_id'],
                'nns_audit' => $nns_audit,
                'nns_create_time'=>date("Y-m-d H:i:s"),
            );
            //上下线注入参数检查，模板参考
            $check_input_params = $this->check_input_params($params, 'add');
            if($check_input_params['ret'] !=0)
            {
                return $check_input_params;
            }
            if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
            {
                return self::return_data(1,'上下线参数检查反馈数据为空');
            }
            $params = $check_input_params['data_info']['info']['out'];
            $in_params = array(
                'nns_vod_id' => $params['base_info']['nns_video_id'],
                'nns_message_id' => $params['base_info']['nns_message_id'],
                'nns_video_name' => $params['base_info']['nns_video_name'],
                'nns_import_id' => $params['base_info']['nns_import_id'],
                'nns_category_id' => $params['base_info']['nns_category_id'],
                'nns_order' => $params['base_info']['nns_order'],
                'nns_action' =>  $params['base_info']['nns_action'],
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
                'nns_org_id' => $params['base_info']['nns_org_id'],
                'nns_status' => $params['base_info']['nns_status'],
                'nns_audit' => $params['base_info']['nns_audit'],
                'nns_fail_time' =>$params['base_info']['nns_fail_time'],
                'nns_delete' => $params['base_info']['nns_delete'],
                'nns_type' => $params['base_info']['nns_type'],
                'nns_asset_id' => $params['base_info']['nns_asset_id'],
            );
            //检查当前队列中是否存在 等待注入
            $wait_params = array(
                'nns_import_id' => $params['base_info']['nns_import_id'],
                'nns_category_id' =>$params['base_info']['nns_category_id'],
                'nns_org_id' => $sp_val[0]['nns_id'],
                'nns_cp_id' => $params['base_info']['cp_id'],
                'nns_status' => '1',
                'nns_type' => $params['base_info']['nns_type'],
                'nns_vod_id' => $params['base_info']['nns_vod_id'],
                //'nns_vod_id' => $video_id,
            );
            $wait_re = \nl_asset_online::check_online_queue(\m_config::get_dc(), $wait_params);
            if ($wait_re['ret'] != 0)
            {
                return $wait_re;
            }
            if ($wait_re['data_info']['count'] > 0)
            {
                //存在相同媒资等待注入的任务队列，则进行修改
                \nl_asset_online::update(m_config::$obj_dc, $in_params, $wait_params);
                $temp_array[] = 'video_id为：' . $in_params['nns_vod_id'] . ';SP_ID为:' . $in_params['nns_org_id'] . '存在相同媒资等待注入的任务队列，则进行修改';
                unset($in_params);
                unset($wait_params);
                continue;
            }
            $fail_params = array(
                'nns_import_id' => $params['base_info']['nns_import_id'],
                'nns_category_id' =>$params['base_info']['nns_category_id'],
                'nns_org_id' => $sp_val[0]['nns_id'],
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
                'nns_status' => '1',
                'nns_type' => ['nns_type'],
                'nns_vod_id' => $params['base_info']['nns_vod_id'],
            );
            //检查队列中是否存在  注入失败/反馈失败等任务
            $fail_re = \nl_asset_online::check_online_queue(\m_config::get_dc(), $fail_params);
            if ($fail_re['ret'] != 0)
            {
                return $fail_re;
            }
            if ($fail_re['data_info']['count'] > 0)
            {
                //存在则删掉之前的任务
                $result_del_by_condition = \nl_asset_online::del_by_condition(\m_config::get_dc(), $fail_params);
                if ($result_del_by_condition['ret'] != 0)
                {
                    return $result_del_by_condition;
                }
                unset($fail_params);
            }
            $result_add = \nl_asset_online::add(\m_config::get_dc(), $params['base_info']);
            if ($result_add['ret'] != 0)
            {
                return $result_add;
            }
            unset($in_params);
            unset($sp_config);
        }
        return self::return_data(NS_CDS_SUCCE, '上下线注入成功'.var_export($temp_array,true));
    }
}