<?php
\ns_core\m_load::load("ns_core.m_public");
\ns_core\m_load::load("ns_core.m_image");
\ns_core\m_load::load("ns_model.m_queue_model");
/**
 * 数据模板核心层
 * @author pan.liang
 */
class m_data_model extends m_public
{
    public $arr_message = null;
    
    public function __construct($arr_message = null)
    {
        $this->arr_message = $arr_message;
    }
    /**
     * 参数模板检查参数错误信息
     * @var array $arr_notice_params = array(
     *                  'notice'=>'注意信息',
     *                  'error'=>'致命错误信息',
     * );
     */
    public $arr_notice_params = null;
    
    /**
     * 修改的时候要在修改之前清理图片信息
     * @param array $data_model 数据模板参数
     * @param array $in_params 注入参数
     * @param string $queue_type
     * @return  array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function _check_and_del_input_image($data_model, $in_params)
    {
        $last_data = null;
        foreach ($data_model as $key => $val)
        {
            if (!is_array($val))
            {
                continue;
            }
            foreach ($val as $_params_key=>$_params)
            {
                if(!isset($_params['rule']) || strlen($_params['rule']) <1 || !preg_match('/^image\#[0-9a-z_|]+/i', $_params['rule']))
                {
                    continue;
                }
                $in_params[$_params_key] = trim($in_params[$_params_key]);
                if(strlen($in_params[$_params_key]) <1)
                {
                    continue;
                }
                if (stripos($in_params[$_params_key], 'http://') !== FALSE && stripos($in_params[$_params_key], 'https://') !== FALSE && stripos($in_params[$_params_key], 'ftp://') !== FALSE)
                {
                    continue;
                }
                $last_data[] = $in_params[$_params_key];
            }
        }
        if(is_array($last_data) && !empty($last_data))
        {
            m_image::img_del_handel($last_data);
        }
        return self::return_data(0,'ok',$last_data);
    }
    
    /**
     * 检查注入参数
     * @param array $data_model 数据模板参数
     * @param array $in_params 注入参数
     * @return array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function _check_input_params($data_model,$in_params)
    {
        $temp_arr_unset_filed = $temp_parent_data = null;
        $flag_log_url = false;
        $queue_type = '';
        $arr_unset = null;
        if (!is_array($data_model) || empty($data_model))
        {
            return self::return_data(1, '参数模板检查,参数非数组或者数组为空');
        }
        $image_params = $last_params = null;
        $this->arr_notice_params['info']['in'] = $in_params;
        foreach ($data_model as $key => $val)
        {
            if (!is_array($val))
            {
                continue;
            }
            foreach ($val as $_params_key=>$_params)
            {
                $temp_in_params_data='';
                //如果设置了 去设置的值，如果未设置，默认为‘’
                if(isset($in_params[$key][$_params_key]))
                {
//                     $in_params[$key][$_params_key] = ($in_params[$key][$_params_key] === null || $in_params[$key][$_params_key] === NULL) ? '' : $in_params[$key][$_params_key];
                    if(!is_string($in_params[$key][$_params_key]) && !is_int($in_params[$key][$_params_key]) && !is_float($in_params[$key][$_params_key]))
                    {
                        $this->arr_notice_params['error'][$key][$_params_key] = "注入的参数非字符串[".var_export($in_params[$key][$_params_key],true)."]";
                        continue;
                    }
                    $temp_in_params_data = trim($in_params[$key][$_params_key]);
                    unset($in_params[$key][$_params_key]);
                }
                else if($key != 'hide_info')
                {
                    $this->arr_notice_params['notice']['miss'][$key][$_params_key] = "注入的参数缺失字段[$_params_key]";
                }
                if($key == 'hide_info' && in_array($_params['default'], array("GUID","datetime",'date','time','year')))
                {
                    if($_params['default'] == 'GUID' && strlen($temp_in_params_data) < 1 )
                    {
                        $temp_in_params_data = np_guid_rand();
                    }
                    elseif ($_params['default'] == 'datetime')
                    {
                        $temp_in_params_data = date("Y-m-d H:i:s");
                    }
                    elseif ($_params['default'] == 'date')
                    {
                        $temp_in_params_data = date("Y-m-d");
                    }
                    elseif ($_params['default'] == 'time')
                    {
                        $temp_in_params_data = date("H:i:s");
                    }
                    elseif ($_params['default'] == 'year')
                    {
                        $temp_in_params_data = date("Y");
                    }
                }
                elseif ($key == 'hide_info' && preg_match('/^[a-z_]+\|log_url$/i', $_params['default']))
                {
                    $temp_default_value = explode('|', $_params['default']);
                    $queue_type = array_shift($temp_default_value);
                    $flag_log_url  = true;
                    continue;
                }
                elseif ($key == 'hide_info' && preg_match('/^parent\|[a-z_]+\|((base_info\+|ex_info\+)(unset\+|set\+)[a-z0-9_]+\+[a-z0-9_]+#?)+\|(base_info\+|ex_info\+)[a-z0-9_]+$/i', $_params['default']))
                {
                    //获取查询字段匹配信息
                    $temp_default_value = explode('|', $_params['default']);
                    array_shift($temp_default_value);//
                    $str_action = array_shift($temp_default_value);//得到action；index_action、vod_action
                    $query_field_info = array_shift($temp_default_value);//得到要赋值的;base_info+unset+nns_index_import_id+nns_import_id#base_info+set+nns_cp_id+nns_cp_id#base_info+set+nns_import_source+nns_import_source
                    $query_field_info = trim($query_field_info,'#');
                    $query_field_info = explode('#', $query_field_info);
                    //获取查询字段信息
                    $replace_field_info = array_shift($temp_default_value);//要查询的字段；nns_id
                    $replace_field_info = trim($replace_field_info,'#');
                    $replace_field_info = explode('+', $replace_field_info);
                    if(count($replace_field_info) !=2)
                    {
                        return m_config::return_data(1,"字段[{$key}][{$_params_key}][default]参数默认值配置错误无法识别replace_field_info");
                    }
                    $is_need_ext = $replace_field_info[0] == 'base_info' ? false : true;
                    //MD5暂存消息
                    $temp_parent_data_value = explode('+', trim($_params['default'],'#'));
                    array_pop($temp_parent_data_value);
                    $temp_parent_data_value_last = '';
                    foreach ($temp_parent_data_value as $value)
                    {
                        $temp_parent_data_value_last.=$value;
                    }
                    $temp_parent_data_value_last .= $is_need_ext ? 'need_ext' : 'no_need_ext';
                    $temp_parent_data_value_last = md5($temp_parent_data_value_last);
                    //如果设置了 不再查询数据
                    if(isset($temp_parent_data[$temp_parent_data_value_last]))
                    {
                        $result_query = $temp_parent_data[$temp_parent_data_value_last];
                    }
                    //如果未设置查询数据
                    else
                    {
                        $arr_query = null;
                        foreach ($query_field_info as $query_field_val)
                        {
                            $query_field_val = explode('+', $query_field_val);
                            if(count($query_field_val) !=4)
                            {
                                return m_config::return_data(1,"字段[{$key}][{$_params_key}][default]参数默认值配置错误无法识别query_field_info");
                            }
                            if($query_field_val[1] == 'unset')
                            {
                                $temp_arr_unset_filed[$query_field_val[0] == 'hide_info' ? 'base_info' : $query_field_val[0]][] = $query_field_val[2];
                            }
                            if(!isset($last_params['base_info'][$query_field_val[2]]))
                            {
                                return m_config::return_data(1,"字段[base_info|ex_info][{$query_field_val[2]}]未设置");
                            }
                            $arr_query[$query_field_val[0]][$query_field_val[3]] = $last_params['base_info'][$query_field_val[2]];
                        }
                        $message_explain = new \ns_model\message\message_explain();
                        $result_query = $message_explain->$str_action('query',$arr_query,$is_need_ext);
                        unset($message_explain);
                        $message_explain = null;
                        if($result_query['ret'] !=0)
                        {
                            $result_query['reason'] = "模板层查询数据不存在匹配方法[{$str_action}]".$result_query['reason'];
                            return $result_query;
                        }
                        if(!isset($result_query['data_info']) || !is_array($result_query['data_info']) || empty($result_query['data_info']))
                        {
                            return m_config::return_data(1,"查询父级信息func[{$str_action}]无数据");
                        }
                        if(count($result_query['data_info']) !=1)
                        {
                            return m_config::return_data(1,"查询父级信息func[{$str_action}]数据存在多条不再执行");
                        }
                        $result_query = $result_query['data_info'][0];
                        //放入临时缓存
                        if(!isset($temp_parent_data[$temp_parent_data_value_last]))
                        {
                            $temp_parent_data[$temp_parent_data_value_last] = $result_query;
                        }
                    }
                    if(!isset($result_query[$replace_field_info[0]][$replace_field_info[1]]))
                    {
                        return m_config::return_data(1,"字段[{$key}][{$_params_key}][default]查询出的结果没有匹配该字段的值[{$replace_field_info[0]}][{$replace_field_info[1]}]");
                    }
                    $temp_in_params_data = strlen($result_query[$replace_field_info[0]][$replace_field_info[1]]) > 0 ? $result_query[$replace_field_info[0]][$replace_field_info[1]] : '';
                }
                elseif ($key == 'hide_info' && preg_match('/^(original|content)\|(base_info|ex_info)\|.*/i', $_params['default']))
                {
                    $temp_default_value = explode('|', $_params['default']);
                    $temp_default_type = array_shift($temp_default_value);
                    $temp_default_key = array_shift($temp_default_value);
                    $temp_default_val = implode('|', $temp_default_value);
                    if(isset($this->arr_notice_params['info']['in'][$temp_default_key][$temp_default_val]) && strlen($this->arr_notice_params['info']['in'][$temp_default_key][$temp_default_val]) >0)
                    {
                        $result_write_message_recive_execute_log = self::write_message_recive_execute_log($last_params[$key == 'hide_info' ? 'base_info' : $key]['nns_cp_id'],$last_params[$key == 'hide_info' ? 'base_info' : $key]['nns_id'],$this->arr_notice_params['info']['in'][$temp_default_key][$temp_default_val],$temp_default_type);
                        if($result_write_message_recive_execute_log['ret'] !=0)
                        {
                            $this->arr_notice_params['error'][$key][$_params_key] = "文件写入失败：{$result_write_message_recive_execute_log['reason']}";
                        }
                        $temp_in_params_data = (isset($result_write_message_recive_execute_log['data_info']['base_dir']) && strlen($result_write_message_recive_execute_log['data_info']['base_dir']) >0) ? $result_write_message_recive_execute_log['data_info']['base_dir'] : '';
                    }
                    $arr_unset[$key == 'hide_info' ? 'base_info' : $key][]=$temp_default_val;
                }
                else if($key == 'hide_info')
                {
                    if(strlen($temp_in_params_data) <1)
                    {
                        $temp_in_params_data = $_params['default'];
                        $this->arr_notice_params['notice'][$key][$_params_key] = "[默认值为:{$temp_in_params_data},设置为系统默认值]";
                    }
                }
                //值长度< 取默认值
                if(strlen($temp_in_params_data) <1 && $key != 'hide_info')
                {
                    $temp_in_params_data = $_params['default'];
                    $this->arr_notice_params['notice'][$key][$_params_key] = "[默认值为:{$temp_in_params_data},设置为系统默认值]";
                }
                //如果设置有rule规则，
                if(!empty($_params['rule']))
                {
                    $result_rule = $this->check_rule($_params['rule'], $temp_in_params_data, $_params_key, $key, $_params);
                    $temp_in_params_data = $result_rule['data_info'];
                    if(preg_match('/^image\#[0-9a-z_|]+/i', $_params['rule']))
                    {
                        $image_params[$key][$_params_key] = array(
                            'value'=>$temp_in_params_data,
                            'rule'=>$_params['rule'],
                        );
                    }
                }
                //如果设置有legth规则，
                if(strlen($_params['length']) >0)
                {
                    $result_length = $this->check_length($_params['length'], $temp_in_params_data, $_params_key, $key, $_params);
                    $temp_in_params_data = $result_length['data_info'];
                }
                $last_params[$key == 'hide_info' ? 'base_info' : $key][$_params_key] = $temp_in_params_data;
            }
        }
        if(isset($image_params) && is_array($image_params))
        {
            $str_img_cp_id = (isset($last_params['base_info']['nns_cp_id']) && strlen($last_params['base_info']['nns_cp_id']) >0) ? $last_params['base_info']['nns_cp_id'] : 'public';
            foreach ($image_params as $image_key=>$image_val)
            {
                if(!is_array($image_val) || empty($image_val))
                {
                    continue;
                }
                foreach ($image_val as $image_k=>$image_v)
                {
                    $image_v['value'] = trim(trim(trim($image_v['value'],'/'),'\\'));
                    $image_v['rule'] = trim(trim($image_v['rule'],'|'));
                    $temp_rule = explode("|", $image_v['rule']);
                    $temp_rule = isset($temp_rule[0]) ?  $temp_rule[0] : '';
                    $temp_rule = explode("#", $temp_rule);
                    $temp_rule = isset($temp_rule[1]) ? $temp_rule[1] : '';
                    $temp_rule = trim($temp_rule);
                    $temp_rule = strlen($temp_rule) <1 ? 'public' : $temp_rule;
                    if(strlen($image_v['value']) <1)
                    {
                        continue;
                    }
                    $result_add_image = m_image::img_add_handel($str_img_cp_id,$temp_rule,$image_v['value'], $last_params['base_info']['nns_name']);
                    if($result_add_image['ret'] !=0)
                    {
                        return self::return_data(1, '图片处理失败', $this->arr_notice_params,$result_add_image);
                    }
                    $last_params[$image_key == 'hide_info' ? 'base_info' : $image_key][$image_k] = $result_add_image['data_info'];
                }
            }
        }
        if(isset($in_params['base_info']) && empty($in_params['base_info']))
        {
            unset($in_params['base_info']);
        }
        if(isset($in_params['ex_info']) && empty($in_params['ex_info']))
        {
            unset($in_params['ex_info']);
        }
        if((is_array($in_params) && !empty($in_params)))
        {
            $this->arr_notice_params['notice']['leftout'] = $in_params;
        }
        if(isset($arr_unset) && !empty($arr_unset))
        {
            foreach ($arr_unset as $key=>$val)
            {
                if(!is_array($val) || empty($val))
                {
                    continue;
                }
                foreach($val as $_v)
                {
                    if(isset($last_params[$key][$_v]))
                    {
                        $last_params[$key][$_v] = '';
                    }
                }
            }
        }
        if(isset($temp_arr_unset_filed) && !empty($temp_arr_unset_filed))
        {
            foreach ($temp_arr_unset_filed as $key=>$val)
            {
                if(!is_array($val) || empty($val))
                {
                    continue;
                }
                foreach($val as $_v)
                {
                    if(isset($last_params[$key][$_v]))
                    {
                        unset($last_params[$key][$_v]);
                    }
                }
            }
        }
        $last_params = $this->mix_htmlspecialchars($last_params);
        $this->arr_notice_params['info']['out'] = $last_params;
        if($flag_log_url)
        {
            $get_push_message_desc = $this->get_push_message_desc($queue_type, $last_params['base_info']['nns_id']);
            if($get_push_message_desc['ret'])
            {
                return $get_push_message_desc;
            }
        }
        $temp_arr_notice_params = $this->arr_notice_params;
        $this->arr_notice_params = null;
        if (isset($temp_arr_notice_params['error']) && !empty($temp_arr_notice_params['error']))
        {
            return self::return_data(1, '参数传入错误', $temp_arr_notice_params);
        }
        if (!is_array($last_params) || empty($last_params))
        {
            return self::return_data(1, '参数检查反馈出的数据为非数组',$temp_arr_notice_params);
        }
        return self::return_data(0, '组装xml OK', $temp_arr_notice_params);
    }
    
    
    /**
     * 底层实现sql添加bug
     * @param unknown $last_params
     * @return string
     */
    public function mix_htmlspecialchars($last_params)
    {
        $str_pregs = "/\'|\/\*|\#|\"|\--|\ --|\/|\*|\-|\+|\=|\~|\*@|\*!|\$|\%|\^|\&/";
        if(is_array($last_params))
        {
            foreach ($last_params as $key=>$value)
            {
                if(is_array($value))
                {
                    $last_params[$key] = $this->mix_htmlspecialchars($value);
                    continue;
                }
                else if(is_string($value) && preg_match($str_pregs, $value))
                {
                    $last_params[$key] = htmlspecialchars($value, ENT_QUOTES);
                }
            }
        }
        else if(is_string($last_params) && preg_match($str_pregs, $last_params))
        {
            $last_params = htmlspecialchars($last_params, ENT_QUOTES);
        }
        return $last_params;
    }
    
    /**
     * 获取参数验证描述 且推入日志文件
     * @param unknown $queue_type
     * @param unknown $queue_id
     * @return multitype:number
     */
    public function get_push_message_desc($queue_type,$queue_id)
    {
        $str_queue_message = '';
        if(isset($this->arr_notice_params['info']['in']))
        {
            $str_queue_message.="初始传入参数：\r\n".var_export($this->arr_notice_params['info']['in'],true)."\r\n";
        }
        else
        {
            $str_queue_message.="初始传入参数：[无]\r\n";
        }
        if(isset($this->arr_notice_params['info']['out']))
        {
            $str_queue_message.="最终写入参数：\r\n".var_export($this->arr_notice_params['info']['out'],true)."\r\n";
        }
        else
        {
            $str_queue_message.="最终写入参数：[无]\r\n";
        }
        if(isset($this->arr_notice_params['info']['error']))
        {
            $str_queue_message.="错误信息：\r\n".var_export($this->arr_notice_params['info']['error'],true)."\r\n";
        }
        else
        {
            $str_queue_message.="错误信息：[无]\r\n";
        }
        if(isset($this->arr_notice_params['notice']['miss']))
        {
            $str_queue_message.="警告缺失字段信息：\r\n".var_export($this->arr_notice_params['notice']['miss'],true)."\r\n";
            unset($this->arr_notice_params['notice']['miss']);
        }
        else
        {
            $str_queue_message.="警告缺失字段信息：[无]\r\n";
        }
        if(isset($this->arr_notice_params['notice']['leftout']))
        {
            $str_queue_message.="警告余留字段信息：\r\n".var_export($this->arr_notice_params['notice']['leftout'],true)."\r\n";
            unset($this->arr_notice_params['notice']['leftout']);
        }
        else
        {
            $str_queue_message.="警告余留字段信息：[无]\r\n";
        }
        if(isset($this->arr_notice_params['notice']))
        {
            $str_queue_message.="警告信息：\r\n".var_export($this->arr_notice_params['notice'],true)."\r\n";
        }
        else
        {
            $str_queue_message.="警告信息：[无]\r\n";
        }
        $result_write_queue_execute_log = self::write_queue_execute_log($queue_type,'push',$queue_id,$str_queue_message);

        if($result_write_queue_execute_log['ret'] !=0)
        {
            return self::return_data(1, '队列执行文件写入失败', $this->arr_notice_params,$result_write_queue_execute_log);
        }
        $this->arr_notice_params['info']['out']['base_info']['nns_bk_queue_excute_url'] = (isset($result_write_queue_execute_log['data_info']['base_dir']) && strlen($result_write_queue_execute_log['data_info']['base_dir']) >0) ? $result_write_queue_execute_log['data_info']['base_dir'] : '';
        return self::return_data(0,'ok');
    }
    
    /**
     * 验证字段规则
     * @param string $str_rule 验证规则
     * @param string $str_in_param 验证的值
     * @param string $_params_key 字段key
     * @param string $key 初始字段key
     * @param array $_params 数据数组
     * @return array:number |array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function check_rule($str_rule,$str_in_param,$_params_key,$key,$_params)
    {
        $str_rule =trim($str_rule);
        if(strlen($str_rule)<1)
        {
            return self::return_data(0,'ok',$str_in_param);
        }
        $arr_rule = explode('|', $_params['rule']);
        $arr_rule = array_filter($arr_rule);
        if(!is_array($arr_rule) || empty($arr_rule))
        {
            return self::return_data(0,'ok',$str_in_param);
        }
        foreach ($arr_rule as $rule_val)
        {
            if(preg_match('/^image\#[0-9a-z_]+/i', $rule_val))
            {
                continue;
            }
            switch ($rule_val)
            {
                case 'noempty':
                    if (strlen($str_in_param) <1)
                    {
                        $this->arr_notice_params['error'][$key][$_params_key] = "[参数规则检查,入参值为:{$str_in_param},描述为:{$_params['desc']},入参为空,规则:{$rule_val}]";
                    }
                    break;
                case 'tinyint':
                    if (is_numeric($str_in_param) && strpos($str_in_param, ".") === false)
                    {
                        if($str_in_param > 255 || $str_in_param < 0)
                        {
                            $this->arr_notice_params['error'][$key][$_params_key] = "[参数规则检查,入参值为:{$str_in_param},描述为:{$_params['desc']},值不在0-255之间,规则:{$rule_val}]";
                        }
                    }
                    else
                    {
                        $this->arr_notice_params['error'][$key][$_params_key] = "[参数规则检查,入参值为:{$str_in_param},描述为:{$_params['desc']},不为数字类型,规则:{$rule_val}]";
                    }
                    break;
                case 'smallint':
                    if (is_numeric($str_in_param) && strpos($str_in_param, ".") === false)
                    {
                        if($str_in_param > 65535 || $str_in_param < 0)
                        {
                            $this->arr_notice_params['error'][$key][$_params_key] = "[参数规则检查,入参值为:{$str_in_param},描述为:{$_params['desc']},值不在0-65535之间,规则:{$rule_val}]";
                        }
                    }
                    else
                    {
                        $this->arr_notice_params['error'][$key][$_params_key] = "[参数规则检查,入参值为:{$str_in_param},描述为:{$_params['desc']},不为数字类型,规则:{$rule_val}]";
                    }
                    break;
                case 'mediumint':
                    if (is_numeric($str_in_param) && strpos($str_in_param, ".") === false)
                    {
                        if($str_in_param > 16777215 || $str_in_param < 0)
                        {
                            $this->arr_notice_params['error'][$key][$_params_key] = "[参数规则检查,入参值为:{$str_in_param},描述为:{$_params['desc']},值不在0-16777215之间,规则:{$rule_val}]";
                        }
                    }
                    else
                    {
                        $this->arr_notice_params['error'][$key][$_params_key] = "[参数规则检查,入参值为:{$str_in_param},描述为:{$_params['desc']},不为数字类型,规则:{$rule_val}]";
                    }
                    break;
                case 'int':
                    if (is_numeric($str_in_param) && strpos($str_in_param, ".") === false)
                    {
                        if($str_in_param > 4294967295 || $str_in_param < 0)
                        {
                            $this->arr_notice_params['error'][$key][$_params_key] = "[参数规则检查,入参值为:{$str_in_param},描述为:{$_params['desc']},值不在0-4294967295之间,规则:{$rule_val}]";
                        }
                    }
                    else
                    {
                        $this->arr_notice_params['error'][$key][$_params_key] = "[参数规则检查,入参值为:{$str_in_param},描述为:{$_params['desc']},不为数字类型,规则:{$rule_val}]";
                    }
                    break;
                case 'bigint':
                    if (is_numeric($str_in_param) && strpos($str_in_param, ".") === false)
                    {
                        if($str_in_param > 18446744073709551615 || $str_in_param < 0)
                        {
                            $this->arr_notice_params['error'][$key][$_params_key] = "[参数规则检查,入参值为:{$str_in_param},描述为:{$_params['desc']},值不在0-18446744073709551615之间,规则:{$rule_val}]";
                        }
                    }
                    else
                    {
                        $this->arr_notice_params['error'][$key][$_params_key] = "[参数规则检查,入参值为:{$str_in_param},描述为:{$_params['desc']},不为数字类型,规则:{$rule_val}]";
                    }
                    break;
                case 'year':
                    if (is_numeric($str_in_param) && strpos($str_in_param, ".") === false)
                    {
                        if($str_in_param > 2155 || $str_in_param < 1901)
                        {
                            $this->arr_notice_params['error'][$key][$_params_key] = "[参数规则检查,入参值为:{$str_in_param},描述为:{$_params['desc']},值不在1901-2155之间,规则:{$rule_val}]";
                        }
                    }
                    else
                    {
                        $this->arr_notice_params['error'][$key][$_params_key] = "[参数规则检查,入参值为:{$str_in_param},描述为:{$_params['desc']},不为数字类型,规则:{$rule_val}]";
                    }
                    break;
                case 'date':
                    $str_in_param = (strlen($str_in_param) > 0) ? trim($str_in_param) : '';
                    if (preg_match ("/\d{4}-1[0-2]|0?[1-9]-0?[1-9]|[12][0-9]|3[01]/", $str_in_param))
                    {
                        $str_in_param = date("Y-m-d", strtotime($str_in_param));
                    }
                    else
                    {
                        $this->arr_notice_params['error'][$key][$_params_key] = "[参数规则检查,入参值为:{$str_in_param},描述为:{$_params['desc']},不匹配日期,规则:{$rule_val}]";
                    }
                    break;
                case 'datetime':
                    $str_in_param = (strlen($str_in_param) > 0) ? trim($str_in_param) : '';
                    if (preg_match ("/\d{4}-1[0-2]|0?[1-9]-0?[1-9]|[12][0-9]|3[01]\s([0-1][0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9])/", $str_in_param))
                    {
                        $str_in_param = date("Y-m-d H:i:s", strtotime($str_in_param));
                    }
                    else if(preg_match ("/0000-00-00 00:00:00/", $str_in_param))
                    {
                        $str_in_param = '0000-00-00 00:00:00';
                    }
                    else
                    {
                        $this->arr_notice_params['error'][$key][$_params_key] = "[参数规则检查,入参值为:{$str_in_param},描述为:{$_params['desc']},不匹配时间,规则:{$rule_val}]";
                    }
                    break;
                case 'time':
                    $str_in_param = (strlen($str_in_param) > 0) ? trim($str_in_param) : '';
                    if (preg_match ("/([0-1][0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9])/", $str_in_param))
                    {
                        $str_in_param = date("H:i:s", strtotime($str_in_param));
                    }
                    else
                    {
                        $this->arr_notice_params['error'][$key][$_params_key] = "[参数规则检查,入参值为:{$str_in_param},描述为:{$_params['desc']},不匹配时间,规则:{$rule_val}]";
                    }
                    break;
                default:
                    $this->arr_notice_params['notice'][$key][$_params_key] = "[参数规则检查,入参值为:{$str_in_param},描述为:{$_params['desc']},规则未知,规则:{$rule_val}]";
            }
        }
        unset($arr_rule,$_params_key,$key,$_params);
        return self::return_data(0,'ok',$str_in_param);
    }
    
    
    /**
     * 验证字符串长度
     * @param string $str_length 长度验证规则
     * @param string $str_in_param 验证的值
     * @param string $_params_key 字段key
     * @param string $key 初始字段key
     * @param array $_params 数组
     * @return array:number |array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function check_length($str_length,$str_in_param,$_params_key,$key,$_params)
    {
        $str_length =trim($str_length);
        if(strlen($str_length)<1)
        {
            return self::return_data(0,'ok',$str_in_param);
        }
        $arr_length = explode('-', $str_length);
        if(!is_array($arr_length) || empty($arr_length))
        {
            return self::return_data(0,'ok',$str_in_param);
        }
        $temp_str_legth = mb_strlen($str_in_param);
        $temp_count = count($arr_length);
        if($temp_count == 1)
        {
            if($temp_str_legth != $arr_length[0])
            {
                $this->arr_notice_params['error'][$key][$_params_key] = "[参数长度检查,入参值为:{$str_in_param},描述为:{$_params['desc']},值长度不为{$arr_length[0]},长度为{$temp_str_legth}]";
            }
        }
        else if($temp_count == 2)
        {
            if($temp_str_legth < $arr_length[0] || $temp_str_legth > $arr_length[1])
            {
                $this->arr_notice_params['error'][$key][$_params_key] = "[参数长度检查,入参值为:{$str_in_param},描述为:{$_params['desc']},值长度不在{$arr_length[0]}与{$arr_length[1]}之间，长度为{$temp_str_legth}]";
            }
        }
        unset($_params_key,$key,$_params,$arr_legth,$str_length);
        return self::return_data(0,'ok',$str_in_param);
    }

    /**
     * 写入执行日志
     * @param $result
     * @param string $str_action
     * @return array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function write_message_info($result,$str_action='pop')
    {
        if(isset($this->arr_message['nns_bk_queue_excute_url']))
        {
            return m_config::write_queue_execute_log('', $str_action, '',$result,$this->arr_message['nns_bk_queue_excute_url']);
        }
        else
        {
            if($result['ret'] == '0')
            {
                return m_config::write_queue_execute_log('', $str_action, '',$result,$result['data_info']['nns_bk_queue_excute_url']);
            }
            else
            {
                return m_config::write_queue_execute_log('', $str_action, '',$result,$result['nns_bk_queue_excute_url']);
            }
        }
    }
    
    /**
     * 队列推入中心注入指令中
     * @param array $arr_info 参数数组
     * @param string $video_type 影片类型
     * @param int $int_weight 权重
     * @param unknown $str_action 操作行为
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function push_to_center_import($arr_info,$video_type,$int_weight=0, $str_action)
    {
        $enable_center_import = get_config_v2('g_enable_center_import_v2');
        if(!$enable_center_import)
        {
            \ns_core\m_load::load("ns_model.center_op.center_op_queue");
            // nns_org_id,nns_name,nns_ex_data,nns_from,nns_encode_flag
            $message = array(
                'base_info' => array(
                    'nns_video_type' => $video_type,
                    'nns_action' => $str_action,
                    'nns_id' => $arr_info['base_info']['nns_id'],
                    'nns_weight' => (int)$int_weight >=0 ? (int)$int_weight : 0,
                    'nns_message_id'=>isset($this->arr_message['nns_message_id']) ? $this->arr_message['nns_message_id'] : '',
                    'nns_is_group'=>(isset($this->arr_message['nns_content_number']) && (int)$this->arr_message['nns_content_number'] > 1) ? 1 : 0,
                    'nns_cp_id'=>$arr_info['base_info']['nns_cp_id'],
                ),
            );
            //$center_op_queue = new \ns_model\center_import\center_import_queue();
            $center_op_queue = new \ns_model\center_op\center_op_queue();
            $result = $center_op_queue->push($message);
            unset($center_op_queue);
            return $result;
        }
        else
        {
            \ns_core\m_load::load("ns_model.center_import.center_import_queue");
            $message = array(
                'base_info'=>array(
                    'nns_message_id'=>isset($this->arr_message['nns_message_id']) ? $this->arr_message['nns_message_id'] : '',
                    'nns_video_id'=>$arr_info['base_info']['nns_id'],
                    'nns_video_type'=>$video_type,
                    'nns_op_mtime'=>round(microtime(TRUE) * 1000),
                    'nns_weight'=>(int)$int_weight >=0 ? (int)$int_weight : 0,
                    'nns_is_group'=>(isset($this->arr_message['nns_content_number']) && (int)$this->arr_message['nns_content_number'] > 1) ? 1 : 0,
                    'nns_cp_id'=>$arr_info['base_info']['nns_cp_id'],
                    'nns_action' => $str_action,
                ),
            );
            $center_import_queue = new \ns_model\center_import\center_import_queue();
            $result = $center_import_queue->push($message);
            unset($center_import_queue);
            return $result;
        }

    }

    /**
     * @param $sp_id
     * @param $video_type   //媒资类型
     * @param $video_info
     * @param string $mode
     * @param null $str_header
     * @return array   //返回信息s4
     * @author xingcheng.hu
     * @date 2016/04/29
     */
    public function get_asset_id_by_sp_id($sp_id, $video_type,$video_info,$mode='cdn',$str_header=null)
    {
        if (strlen($sp_id) < 1)
        {
            return self::return_data('1', 'sp_id为空', '');
        }
        if (strlen($video_type) < 1)
        {
            return self::return_data('1', 'video_type为空', '');
        }
        if($mode === 'epg')
        {
            $config_action = $mode . "_" . $video_type . "_import_id_mode";
        }
        else
        {
            $config_action = "import_id_mode";
        }
        if (!isset(\m_config::$arr_sp_config[$sp_id]['nns_config'][$config_action]) || strlen(\m_config::$arr_sp_config[$sp_id]['nns_config'][$config_action]) < 1 || (int)\m_config::$arr_sp_config[$sp_id]['nns_config'][$config_action] < 1)
        {
            return self::return_data(0, 'ok', $video_info['nns_id']);
        }
        //兼容结束
        switch ($video_type)
        {
            case 'video':
                $str_func = 'get_vod_info';
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_asset_import_id';
                $import_id_model_flags = '1';
                $import_id_model_flags_1 = '000000';
                $import_id_model_flags_2 = '0000';
                $import_id_model_flags_3 = '000001';
                // 				$import_id_model_flags_4 = 'PT';
                $import_id_model_flags_4 = 'PA';
                break;
            case 'index':
                $str_func = 'get_vod_index_info';
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_import_id';
                $import_id_model_flags = '2';
                $import_id_model_flags_1 = '001000';
                $import_id_model_flags_2 = '0001';
                $import_id_model_flags_3 = '000002';
                // 				$import_id_model_flags_4 = 'TI';
                $import_id_model_flags_4 = 'TE';
                break;
            case 'media':
                $str_func = 'get_vod_media_info';
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_import_id';
                $import_id_model_flags = '3';
                $import_id_model_flags_1 = '002000';
                $import_id_model_flags_2 = '0002';
                $import_id_model_flags_3 = '000003';
                // 				$import_id_model_flags_4 = 'MO';
                $import_id_model_flags_4 = 'MV';
                break;
            case 'live':
                $str_func = 'get_live_info';
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_import_id';
                $import_id_model_flags = '4';
                $import_id_model_flags_1 = '004000';
                $import_id_model_flags_2 = '0004';
                $import_id_model_flags_3 = '010001';
                $import_id_model_flags_4 = 'CH';
                break;
            case 'live_index':
                $str_func = 'get_live_index_info';
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_import_id';
                $import_id_model_flags = '5';
                $import_id_model_flags_1 = '005000';
                $import_id_model_flags_2 = '0005';
                $import_id_model_flags_3 = '010002';
                $import_id_model_flags_4 = 'LI';
                break;
            case 'live_media':
                $str_func = 'get_live_media_info';
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_content_id';
                $import_id_model_flags = '6';
                $import_id_model_flags_1 = '006000';
                $import_id_model_flags_2 = '0006';
                $import_id_model_flags_3 = '010003';
                $import_id_model_flags_4 = 'PC';
                break;
            case 'playbill':
                $str_func = 'get_playbill_info';
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_playbill_import_id';
                $import_id_model_flags = '7';
                $import_id_model_flags_1 = '007000';
                $import_id_model_flags_2 = '0007';
                $import_id_model_flags_3 = '020001';
                $import_id_model_flags_4 = 'SD';
                break;
            case 'file':
                $str_func = 'get_file_package_info';
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_import_id';
                $import_id_model_flags = '8';
                $import_id_model_flags_1 = '000000';
                $import_id_model_flags_2 = '0000';
                $import_id_model_flags_3 = '030001';
                $import_id_model_flags_4 = 'FI';
                break;
            default:
                return self::return_data('1', '媒资类型错误！');
        }
        $str_csp_id = defined('CSP_ID') ? CSP_ID : isset($video_info['import_csp_id']) ? \m_config::$arr_sp_config[$sp_id]['nns_config']['import_csp_id'] : '';
    
        switch (\m_config::$arr_sp_config[$sp_id]['nns_config'][$config_action])
        {
            case '1':
                $asset_id = $import_id_model_flags_1 . $video_info[$str_integer_id];
                $asset_id = str_pad($asset_id, 32, "0");
                break;
            case '2':
                $asset_id = $video_info[$str_import_id];
                break;
            case '3':
                $asset_id = $str_csp_id . $import_id_model_flags_2 . $video_info[$str_integer_id];
                $asset_id = str_pad($asset_id, 32, "0");
                break;
            case '4':
                $arr_csp_id = explode("|", $str_csp_id);
                $str_length = strlen($arr_csp_id[0]) + strlen($arr_csp_id[1]) +1;
                $lest_num = 32 - $str_length;
                $asset_id = $arr_csp_id[0] . $import_id_model_flags . str_pad($video_info[$str_integer_id], $lest_num, "0", STR_PAD_LEFT) . $arr_csp_id[1];
                break;
            case '5':
                $str_length = strlen($import_id_model_flags_3);
                $lest_num = 32 - $str_length;
                $asset_id = $import_id_model_flags_3 . str_pad($video_info[$str_integer_id], $lest_num, "0", STR_PAD_LEFT);
                break;
            case '6':
                $str_length = strlen($import_id_model_flags_1);
                $lest_num = 32 - $str_length;
                $asset_id = $import_id_model_flags_1 . str_pad($video_info[$str_integer_id], $lest_num, "0", STR_PAD_LEFT);
                break;
            case '7':
                $str_header = (strlen($str_header) < 8) ? str_pad($str_header, 8, "0", STR_PAD_RIGHT) : substr($str_header, 0,8);
                $asset_id = $str_header.$import_id_model_flags_4 . str_pad($video_info[$str_integer_id], 10, "0", STR_PAD_LEFT);
                break;
            default:
                $asset_id = $video_info['nns_id'];
                break;
        }
        return self::return_data(0, 'ok', $asset_id);
    }

    /**
     * 第三方注入id  反解析
     * @param $sp_id
     * @param $video_type
     * @param $nns_id
     * @param null $cp_id
     * @param null $sp_config
     * @param string $mode
     * @return array
     */
    public function get_asset_id_by_sp_import_id($sp_id, $video_type, $nns_id,$cp_id=null,$sp_config = null,$mode = "cdn")
    {
        if (strlen($sp_id) < 1)
        {
            return self::return_data('1', 'sp_id为空', '');
        }
        if (strlen($video_type) < 1)
        {
            return self::return_data('1', 'video_type为空', '');
        }
        if (strlen($nns_id) < 1)
        {
            return self::return_data('1', 'nns_id为空', '');
        }
        if (!isset($sp_config) || empty($sp_config) || !is_array($sp_config))
        {
            $sp_config = sp_model::get_sp_config($sp_id);
        }
        $array_data = null;
        //谁加的这个，整的CDN与EPG的注入ID没有分离了。update by zhiyong.luo 2017-09-19
        //        if (!isset($sp_config['import_id_mode']) || strlen($sp_config['import_id_mode']) < 1 || (int)$sp_config['import_id_mode'] < 1)
            //        {
            //            $array_data['nns_id'] = $nns_id;
            //            $video_info = video_model::get_back_info_one($video_type,$array_data);
            //            $last_data=array(
            //                'query_info'=>$array_data,
            //                'data_info'=>$video_info
            //            );
            //            return self::return_data(0, 'ok', $last_data);
            //        }
        //兼容-begin
        if($mode === 'epg')
        {
            $config_action = $mode . "_" . $video_type . "_import_id_mode";
        }
        else
        {
            $config_action = "import_id_mode";
        }
        if (!isset($sp_config[$config_action]) || strlen($sp_config[$config_action]) < 1 || (int)$sp_config[$config_action] < 1)
        {
            $array_data['nns_id'] = $nns_id;
            $video_info = video_model::get_back_info_one($video_type,$array_data);
            $last_data=array(
                'query_info'=>$array_data,
                'data_info'=>$video_info
            );
            return self::return_data(0, 'ok', $last_data);
        } //兼容-end
        else
        {
            switch ($video_type)
            {
                case 'video':
                    $str_func = 'get_vod_info';
                    $str_integer_id = 'nns_integer_id';
                    $str_import_id = 'nns_asset_import_id';
                    $import_id_model_flags = '1';
                    $import_id_model_flags_1 = '000000';
                    $import_id_model_flags_2 = '0000';
                    $import_id_model_flags_3 = '000001';
                    break;
                case 'index':
                    $str_func = 'get_vod_index_info';
                    $str_integer_id = 'nns_integer_id';
                    $str_import_id = 'nns_import_id';
                    $import_id_model_flags = '2';
                    $import_id_model_flags_1 = '001000';
                    $import_id_model_flags_2 = '0001';
                    $import_id_model_flags_3 = '000002';
                    break;
                case 'media':
                    $str_func = 'get_vod_media_info';
                    $str_integer_id = 'nns_integer_id';
                    $str_import_id = 'nns_import_id';
                    $import_id_model_flags = '3';
                    $import_id_model_flags_1 = '002000';
                    $import_id_model_flags_2 = '0002';
                    $import_id_model_flags_3 = '000003';
                    break;
                case 'live':
                    $str_func = 'get_live_info';
                    $str_integer_id = 'nns_integer_id';
                    $str_import_id = 'nns_import_id';
                    $import_id_model_flags = '4';
                    $import_id_model_flags_1 = '004000';
                    $import_id_model_flags_2 = '0004';
                    $import_id_model_flags_3 = '010001';
                    break;
                case 'live_index':
                    $str_func = 'get_live_index_info';
                    $str_integer_id = 'nns_integer_id';
                    $str_import_id = 'nns_import_id';
                    $import_id_model_flags = '5';
                    $import_id_model_flags_1 = '005000';
                    $import_id_model_flags_2 = '0005';
                    $import_id_model_flags_3 = '010002';
                    break;
                case 'live_media':
                    $str_func = 'get_live_media_info';
                    $str_integer_id = 'nns_integer_id';
                    $str_import_id = 'nns_content_id';
                    $import_id_model_flags = '6';
                    $import_id_model_flags_1 = '006000';
                    $import_id_model_flags_2 = '0006';
                    $import_id_model_flags_3 = '010003';
                    break;
                case 'playbill':
                    $str_func = 'get_playbill_info';
                    $str_integer_id = 'nns_integer_id';
                    $str_import_id = 'nns_playbill_import_id';
                    $import_id_model_flags = '7';
                    $import_id_model_flags_1 = '007000';
                    $import_id_model_flags_2 = '0007';
                    $import_id_model_flags_3 = '020001';
                    break;
                case 'file':
                    $str_func = 'get_file_package_info';
                    $str_integer_id = 'nns_integer_id';
                    $str_import_id = 'nns_import_id';
                    $import_id_model_flags = '8';
                    $import_id_model_flags_1 = '000000';
                    $import_id_model_flags_2 = '0000';
                    $import_id_model_flags_3 = '030001';
                    break;
                default:
                    return self::return_data('1', '媒资类型错误！');
            }
            $str_csp_id = defined('CSP_ID') ? CSP_ID : isset($sp_config['import_csp_id']) ? $sp_config['import_csp_id'] : '';
            	
            switch ($sp_config[$config_action])
            {
                case '1':
                    //return self::return_data('1', '不支持此类型！');
                    $array_data['nns_id'] = $nns_id;
                    break;
                case '2':
                    $array_data[$str_import_id] = $nns_id;
                    break;
                case '3':
                    //return self::return_data('1', '不支持此类型！');
                    $array_data['nns_id'] = $nns_id;
                    break;
                case '4':
                    $arr_csp_id = explode("|", $str_csp_id);
                    $num_begin = strlen($arr_csp_id[0] . $import_id_model_flags);
                    $num_begin = $num_begin>0 ? $num_begin : 0;
                    $num_end = strlen($arr_csp_id[1]);
                    $num_end = $num_end>0 ? $num_end : 0;
                    $nns_id = substr($nns_id, $num_begin);
                    $array_data[$str_integer_id] = ($num_end >0) ? substr($nns_id, 0,-$num_end) : $nns_id;
                    break;
                case '5':
                    $num_begin = strlen($import_id_model_flags_3);
                    $num_begin = $num_begin>0 ? $num_begin : 0;
                    $array_data[$str_integer_id] = preg_replace('/^0*/', '',substr($nns_id, $num_begin));
                    break;
                case '7':
                    $array_data[$str_integer_id] = preg_replace('/^0*/', '',substr($nns_id, 10));
                    break;
                default:
                    $array_data['nns_id'] = $nns_id;
            }
        }
        if(isset($cp_id) && strlen($cp_id)>0)
        {
            $array_data['nns_cp_id']=$cp_id;
        }
        $video_info = video_model::get_back_info_one($video_type,$array_data);
        $last_data=array(
            'query_info'=>$array_data,
            'data_info'=>$video_info
        );
        return self::return_data(0, 'ok', $last_data);
    }
}