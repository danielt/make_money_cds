<?php
\ns_core\m_load::load("ns_data_model.m_data_model");
\ns_core\m_load::load_old("nn_logic/op_queue/op_queue.class.php");
class m_center_sync_inout  extends m_data_model
{
    /**
     * 修改参数检查模板
     * @var unknown
     */
    public $add_params  = array(
        'base_info'     =>      array(
            'nns_message_id'                =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-64',
                'desc'      => '上游消息ID（后期会改为我们自身平台的GUID）',
            ),
            'nns_video_name'                =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '0-512',
                'desc'      => '队列名称',
            ),
            'nns_video_id'                  =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '落地数据GUID',
            ),
            'nns_video_type'                =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '影片类型',
            ),
            'nns_action'                    =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-16',
                'desc'      => '操作行为',
            ),
            'nns_op_mtime'                  =>      array (
                'rule'      => 'bigint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '操作时间（毫秒）',
            ),
            'nns_op_sp'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-256',
                'desc'      => '下游平台ID',
            ),
            'nns_weight'                    =>      array (
                'rule'      => 'smallint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '权重值越高，权重越高',
            ),
            'nns_is_group'                  =>      array (
                'rule'      => 'smallint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '是否是分组  默认0  0 不是分组消息 | 1是分组消息',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '0',
                'length'    => '1-32',
                'desc'      => 'cp_id',
            ),
        ),
        'hide_info'     =>      array(
            'nns_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => 'GUID',
                'length'    => '32',
                'desc'      => '消息队列GUID',
            ),
            'nns_bk_queue_excute_url'       =>      array (
                'rule'      => '',
                'default'   => 'center_import_queue|log_url',
                'length'    => '0-255',
                'desc'      => '消息队列执行的结果url 所有情况',
            ),
        ),
    );
    /**
     * 检查注入参数
     * @param array $in_params 注入参数
     * @param string $action 操作参数行为
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function check_input_params($in_params,$action)
    {
        $notice_params = $last_params = array ();
        $str_params = "{$action}_params";
        if (!isset($this->$str_params))
        {
            return self::return_data(1, '没有设置' . $str_params . '参数的判断模式');
        }
        if (!is_array($this->$str_params))
        {
            return self::return_data(1, '参数非数组');
        }
        return $this->_check_input_params($this->$str_params, $in_params);
    }
    
    
    /**
     * 添加主媒资数据
     * @param array $params 添加数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function add($params)
    {
        //注入主媒资参数检查，模板参考
        $check_input_params = $this->check_input_params($params,'add');
        if($check_input_params['ret'] !=0)
        {
            return $check_input_params;
        }
        if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return $this->_return_data(1,'主媒资参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];
        $result_excute = nl_import_op::add_v2(m_config::get_dc(), $params['base_info']);
        if($result_excute['ret'] !=0)
        {
            return self::return_data(1,'消息队列入库数据库执行失败',$check_input_params['data_info'],$result_excute['reason']);
        }
        else
        {
            return self::return_data(0,'ok',$check_input_params['data_info'],$result_excute['reason']);
        }
    }
    
    /**
     * 查询主媒资数据
     * @param array $array_query 条件数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function query($array_query)
    {
        $last_data = null;
        $result_vod = nl_message::query_by_condition(m_config::$obj_dc, $array_query);
        if($result_vod['ret'] !=0 || !isset($result_vod['data_info']) || !is_array($result_vod['data_info']) || empty($result_vod['data_info']))
        {
            return $result_vod;
        }
        foreach ($result_vod['data_info'] as $data_info)
        {
            $last_data[]=array(
                'base_info'=>$data_info,
            );
        }
        return self::return_data(0,'OK',$last_data);
    }
}