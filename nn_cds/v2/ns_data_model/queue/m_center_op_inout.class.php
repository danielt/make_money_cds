<?php
\ns_core\m_load::load("ns_data_model.m_data_model");
\ns_core\m_load::load("ns_data_model.command.m_command_task_log_inout");
\ns_core\m_load::load_old("nn_logic/op_queue/op_queue.class.php");
class m_center_op_inout  extends m_data_model
{
    /**
     * 修改参数检查模板
     * @var unknown
     */
    public $add_params  = array(
        'base_info'     =>      array(
            'nns_type'                =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '0-12',
                'desc'      => '类型',
            ),
            'nns_org_id'                =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => 'SPID',
            ),
            'nns_video_id'                  =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '数据GUID',
            ),
            'nns_index_id'                =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-32',
                'desc'      => '分集ID',
            ),
            'nns_media_id'                =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-32',
                'desc'      => '片源ID',
            ),
            'nns_action'                    =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-16',
                'desc'      => '操作行为',
            ),
            'nns_op_mtime'                  =>      array (
                'rule'      => 'bigint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '操作时间（毫秒）',
            ),
            'nns_status'                     =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '0-32',
                'desc'      => '状态码(0等待注入 1正在注入)',
            ),
            'nns_weight'                    =>      array (
                'rule'      => 'smallint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '权重值越高，权重越高',
            ),
            'nns_is_group'                  =>      array (
                'rule'      => 'smallint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '是否是分组  默认0  0 不是分组消息 | 1是分组消息',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => 'cp_id',
            ),
            'nns_release_time'                     =>      array (
                'rule'      => 'datetime',
                'default'   => '',
                'length'    => '',
                'desc'      => '上映时间',
            ),
            'nns_state'                     =>      array (
                'rule'      => 'tinyint',
                'default'   => '1',
                'length'    => '1',
                'desc'      => '状态',
            ),
            'nns_ex_data'                     =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-2048',
                'desc'      => '扩展信息',
            ),
            'nns_from'                     =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-32',
                'desc'      => '来源',
            ),
            'nns_encode_flag'                     =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-32',
                'desc'      => '转码url标示（MD5）',
            ),
            'nns_command_task_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '消息指令状态池ID',
            ),
            'nns_name'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '0-512',
                'desc'      => '队列名称',
            ),
            'nns_message_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-64',
                'desc'      => '消息ID',
            ),
        ),
        'hide_info'     =>      array(
            'nns_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => 'GUID',
                'length'    => '32',
                'desc'      => '消息队列GUID',
            ),
            'nns_bk_queue_excute_url'       =>      array (
                'rule'      => '',
                'default'   => 'center_op|log_url',
                'length'    => '0-255',
                'desc'      => '消息队列执行的结果url所有情况',
            ),
            'nns_create_time'       =>      array (
                'rule'      => '',
                'default'   => 'datetime',
                'length'    => '',
                'desc'      => '消息队列生成时间',
            ),
        ),
    );


    public $media_type = array('media', 'live_media', 'file');
    /**
     * 检查注入参数
     * @param array $in_params 注入参数
     * @param string $action 操作参数行为
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function check_input_params($in_params,$action)
    {
        $notice_params = $last_params = array ();
        $str_params = "{$action}_params";
        if (!isset($this->$str_params))
        {
            return self::return_data(1, '没有设置' . $str_params . '参数的判断模式');
        }
        if (!is_array($this->$str_params))
        {
            return self::return_data(1, '参数非数组');
        }
        return $this->_check_input_params($this->$str_params, $in_params);
    }


    /**
     * 添加
     * @param array $params 添加数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function add($params)
    {
        //注入主媒资参数检查，模板参考
        $check_input_params = $this->check_input_params($params,'add');

        if($check_input_params['ret'] != NS_CDS_SUCCE)
        {
            return $check_input_params;
        }
        if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(NS_CDS_FAIL,'中心同步指令参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];

        #todo 查询是否已经存在队列 xinxin.deng 2018/7/25 16:09
        $op_exist = array(
            'nns_type' => $params['base_info']['nns_type'],
            'nns_org_id' => $params['base_info']['nns_org_id'],
            'nns_cp_id' => $params['base_info']['nns_cp_id'],
            'nns_video_id' => $params['base_info']['nns_video_id'],
            'nns_index_id' => $params['base_info']['nns_index_id'],
            'nns_media_id' => $params['base_info']['nns_media_id'],
        );
        $op_exist_arr = \nl_op_queue::query_by_params(\m_config::get_dc(), $op_exist);
        if ($op_exist_arr['ret'] != 0)
        {
            return self::return_data(NS_CDS_FAIL, '[添加到中心同步指令]查询是否存在队列失败:' . $op_exist_arr['reason']);
        }
        if (!empty($op_exist_arr['data_info']) && is_array($op_exist_arr['data_info']) && count($op_exist_arr['data_info']) > 0)
        {
            foreach ($op_exist_arr['data_info'] as $items)
            {
                if ($items['nns_status'] == NS_CDS_OP_WAIT || $items['nns_status'] == NS_CDS_OP_CLIP_LOADING || $items['nns_status'] == NS_CDS_OP_WAIT_CLIP)
                {
                    if ($params['base_info']['nns_action'] == NS_CDS_DELETE)
                    {
                        self::del(array($items['nns_id']));
                    }
                    elseif ($items['nns_action'] == NS_CDS_ADD && $params['base_info']['nns_action'] == NS_CDS_MODIFY)
                    {
                        self::del(array($items['nns_id']));
                        $params['base_info']['nns_action'] = NS_CDS_MODIFY;
                    }
                }
                if ($items['nns_action'] == NS_CDS_MODIFY && $params['base_info']['nns_action'] == NS_CDS_MODIFY
                    && ($params['base_info']['nns_type'] == NS_CDS_VIDEO || $params['base_info']['nns_type'] == NS_CDS_INDEX))
                {
                    \m_config::write_message_execute_log('[添加到中心同步指令]查询队列已经存在,类型为：' . $params['base_info']['nns_type'] . ',跳过', $params['base_info']['nns_cp_id']);
                    return self::return_data(NS_CDS_SUCCE, '[添加到中心同步指令]查询队列已经存在,类型为：' . $params['base_info']['nns_type'] . ',跳过');
                }
            }
        }
        $arr_sp_data = \m_config::_get_sp_info($params['base_info']['nns_org_id']);
        $sp_config = isset($arr_sp_data['data_info']['nns_config']) ? $arr_sp_data['data_info']['nns_config'] : array();
        /***************如果类型是片源,须走是否需要切片逻辑,将状态变为等待切片*****************/
        if (isset($sp_config['disabled_clip']) && (int)$sp_config['disabled_clip'] != 2 && (int)$sp_config['disabled_clip'] != 3 && $params['base_info']['nns_type'] == NS_CDS_MEDIA)
        {
            if ( $params['base_info']['nns_action']== NS_CDS_ADD || $params['base_info']['nns_action']== NS_CDS_MODIFY)
            {
                $params['base_info']['nns_status'] = NS_CDS_OP_WAIT_CLIP;
            }
        }
        /***************如果类型是片源,须走是否需要切片逻辑,将状态变为等待切片*****************/
        $result_excute = nl_op_queue::add(m_config::get_dc(), $params['base_info']);
        if($result_excute['ret'] !=0)
        {
            return self::return_data(NS_CDS_FAIL,'中心同步队列入库数据库执行失败',$check_input_params['data_info'],$result_excute['reason']);
        }
        else
        {
            /***********添加指令日志log***************/
            $re_w_log = \m_config::write_message_execute_log($result_excute['reason'], $params['base_info']['nns_cp_id']);
            $command_status = NS_CDS_COMMAND_OP_WAIT;
            switch ($params['base_info']['nns_status'])
            {
                case 0:
                    $command_status = NS_CDS_COMMAND_OP_WAIT;
                    break;
                case 2:
                    $command_status = NS_CDS_COMMAND_WAIT_CLIP;
                    break;
                case 3:
                    $command_status = NS_CDS_COMMAND_CLIP_LOADING;
                    break;
                case 4:
                    $command_status = NS_CDS_COMMAND_OP_CANCEL;
                    break;
                case 6:
                    $command_status = NS_CDS_COMMAND_UNAUDITED;
                    break;
                case 7:
                    $command_status = NS_CDS_COMMAND_WAIT_SP;
                    break;
                case 8:
                    $command_status = NS_CDS_COMMAND_ENCODE;
                    break;
                case 9:
                    $command_status = NS_CDS_COMMAND_WAIT_ENCODE;
                    break;
                case 10:
                    $command_status = NS_CDS_COMMAND_ENCODE_FAIL;
                    break;
            }
            $command_task_log_params = array(
                'base_info' => array(
                    'nns_command_task_id' => $params['base_info']['nns_command_task_id'],
                    'nns_status' => $command_status,
                    'nns_desc' => $result_excute['reason'],
                    'nns_execute_url' => $re_w_log['data_info']['base_dir'],
                )
            );
            $obj_command_task_log = new m_command_task_log_inout();
            $obj_command_task_log->add($command_task_log_params);

            return self::return_data(NS_CDS_SUCCE,'ok',$check_input_params['data_info'],$result_excute['reason']);
        }
    }

    /**
     * 查询队列数据
     * @param array $array_query 条件数据 不能为空
     * @param boolean $is_need_ext false 不需要查询扩展数据 | true 需要查询扩展数据
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function query($array_query, $is_need_ext = false)
    {
        $last_data = null;
        $result_op = nl_op_queue::query_by_params(\m_config::get_dc(), $array_query['base_info']);
        if ($result_op['ret'] != 0 || !isset($result_op['data_info']) || !is_array($result_op['data_info']) || empty($result_op['data_info']))
        {
            return $result_op;
        }
        foreach ($result_op['data_info'] as $data_info)
        {
            $temp_epg_data = $temp_cdn_data = $temp_ex_data = null;
            $last_data[] = array(
                'base_info' => $data_info,
                'ex_info' => $temp_ex_data,
                'epg_info' => $temp_epg_data,
                'cdn_info' => $temp_cdn_data,
            );
        }
        return self::return_data(0, 'OK', $last_data);
    }

    /**
     * 删除中心同步指令队列
     * @param $params  //只能是id的数组array('')
     * @return array
     */
    public function del($params)
    {
        $result = \nl_op_queue::delete_op_queue(\m_config::get_dc(), $params);
        return self::return_data($result['ret'], $result['reason']);
    }
}