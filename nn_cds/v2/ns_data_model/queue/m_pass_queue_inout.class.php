<?php
/**
 *
 * @author: jing.chen
 * @date: 2017/12/22 17:25
 */
\ns_core\m_load::load("ns_data_model.m_data_model");
\ns_core\m_load::load_old("nn_logic/pass_queue/pass_queue.class.php");
\ns_core\m_load::load_old("nn_logic/sp/sp.class.php");
class m_pass_queue_inout extends m_data_model
{
    /**
     * 修改参数检查模板
     * @var unknown
     */
    public $add_params  = array(
        'base_info'     =>      array(
            'nns_action'              =>      array (
                'rule'      => 'noempty',
                'default'   => '0',
                'length'    => '',
                'desc'      => '动作0无行为1添加2修改3删除4上线5下线6锁定7解锁8绑定9解绑',
            ),
            'nns_type'              =>      array (
                'rule'      => 'noempty',
                'default'   => '0',
                'length'    => '',
                'desc'      => '队列类型 0栏目同步1栏目推荐2CP同步3栏目图片同步4媒资融合5媒资打包同分6录制节目单7点播锁定解锁98节目单同步99频道锁定解锁',
            ),
            'nns_message_id'              =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-64',
                'desc'      => '消息ID',
            ),
            'nns_cp_id'              =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => 'cp_id',
            ),
            'nns_status'              =>      array (
                'rule'      => 'noempty',
                'default'   => '1',
                'length'    => '',
                'desc'      => '队列状态0注入成功1等待注入2正在注入3注入失败4等待反馈',
            ),
            'nns_content'              =>      array (
                'rule'      => 'text',
                'default'   => '',
                'length'    => '',
                'desc'      => '透传内容',
            ),
        ),
        'hide_info'     =>      array(
            'nns_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => 'GUID',
                'length'    => '32',
                'desc'      => '透传队列GUID',
            ),
            'nns_pause'              =>      array (
                'rule'      => 'noempty',
                'default'   => '0',
                'length'    => '',
                'desc'      => '队列开始 0 正常 1暂停',
            ),
            'nns_org_id'              =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => 'sp_id',
            ),
            'nns_audit'              =>      array (
                'rule'      => 'noempty',
                'default'   => '0',
                'length'    => '',
                'desc'      => '审核状态',
            ),
           /* 'nns_result_desc'              =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-128',
                'desc'      => '队列任务结果描述',
            ),*/
            'nns_queue_time'                        =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-64',
                'desc'      => '队列毫秒级创建时间',
            ),
            'nns_create_time'                        =>      array (
                'rule'      => 'datetime',
                'default'   => '0000-00-00 00:00:00',
                'length'    => '',
                'desc'      => '创建时间',
            ),
            'nns_modify_time'                        =>      array (
                'rule'      => 'datetime',
                'default'   => '0000-00-00 00:00:00',
                'length'    => '',
                'desc'      => '修改时间',
            ),
        ),
    );

    /**
     * 检查注入参数
     * @param array $in_params 注入参数
     * @param string $action 操作参数行为
     * @return array //array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function check_input_params($in_params,$action)
    {
        $notice_params = $last_params = array ();
        $str_params = "{$action}_params";
        if (!isset($this->$str_params))
        {
            return self::return_data(1, '没有设置' . $str_params . '参数的判断模式');
        }
        if (!is_array($this->$str_params))
        {
            return self::return_data(1, '参数非数组');
        }
        return $this->_check_input_params($this->$str_params, $in_params);
    }

    /**
     * 添加透传队列
     * @param array $params 添加数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function add($params)
    {
        //根据CP查询绑定的SP
        $arr_sp = m_config::_get_cp_bind_sp_info($params['base_info']['nns_cp_id']);
        if (!is_array($arr_sp['data_info']) || empty($arr_sp['data_info']))
        {
            return self::return_data(1,'没有查询到相关cp绑定关系，cp_id为:' . $params['base_info']['nns_cp_id']);
        }


        $temp_array = array();
        foreach ($arr_sp['data_info'] as $sp_val)
        {
            $sp_config = $sp_val['nns_config'];
            //检查透传队列开关是否开启  如果未开启 直接continue
            if (!isset($sp_config['pass_queue_disabled']) || $sp_config['pass_queue_disabled'] != 1)
            {
                $temp_array[] = 'SP为:' . $sp_val . '的透传开关未开启,值为' . $sp_config['pass_queue_disabled'];
                continue;
            }

            //0 为进入透传队列为待审核，1 为审核通过  2审核不通过
            $params['hide_info']['nns_audit'] = (isset($sp_config['pass_queue_audit']) && (int)$sp_config['pass_queue_audit'] === 1) ? $sp_config['pass_queue_audit'] : 0;
            list ($usec, $sec) = explode(' ', microtime());
            $time = str_pad(intval(substr($usec, 2, 4)), 4, '0', STR_PAD_LEFT);
            $micro_time = date('YmdHis', time()) . $time;
            $params['hide_info']['nns_queue_time']=$micro_time;
            $params['hide_info']['nns_org_id']=$sp_val[0]['nns_id'];
            $check_input_params = $this->check_input_params($params,'add');
            if($check_input_params['ret'] !=0)
            {
                return $check_input_params;
            }
            if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
            {
                return self::return_data(1,'透传队列参数检查反馈数据为空');
            }
            $params_data = $check_input_params['data_info']['info']['out'];
            //注入透传队列参数检查，模板参考
            $result = nl_pass_queue::add(m_config::$obj_dc, $params_data['base_info']);
            if ($result['ret'] != 0)
            {
                return self::return_data(1,'透传队列生成失败'.var_export($result,true));
            }
            unset($params_data);
            unset($sp_config);
        }
        return self::return_data(0,'透传队列生成成功');
    }
}