<?php
\ns_core\m_load::load_old("nn_logic/command/command.class.php");
\ns_core\m_load::load("ns_data_model.m_data_model");

class m_command_inout  extends m_data_model
{
    /**
     * 修改参数检查模板
     * @var unknown
     */
    public $add_params  = array(
        'base_info'     =>      array(
            'nns_message_id'                =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '0-64',
                'desc'      => '上游消息ID',
            ),
            'nns_name'                =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-512',
                'desc'      => '队列名称',
            ),
            'nns_content_id'                  =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '落地数据GUID',
            ),
            'nns_type'                =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '影片类型',
            ),
            'nns_source_id'                  =>      array (
                'rule'      => 'noempty',
                'default'   => '0-64',
                'length'    => '',
                'desc'      => '上游来源ID',
            ),
            'nns_original_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-128',
                'desc'      => '下游平台ID',
            ),
        ),
        'hide_info'     =>      array(
            'nns_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => 'GUID',
                'length'    => '32',
                'desc'      => '消息队列GUID',
            ),
        ),
    );
    /**
     * 检查注入参数
     * @param array $in_params 注入参数
     * @param string $action 操作参数行为
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function check_input_params($in_params,$action)
    {
        $notice_params = $last_params = array ();
        $str_params = "{$action}_params";
        if (!isset($this->$str_params))
        {
            return self::return_data(1, '没有设置' . $str_params . '参数的判断模式');
        }
        if (!is_array($this->$str_params))
        {
            return self::return_data(1, '参数非数组');
        }
        return $this->_check_input_params($this->$str_params, $in_params);
    }


    /**
     * 添加指令状态池数据
     * @param array $params 添加数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function add($params)
    {
        //注入主媒资参数检查，模板参考
        $check_input_params = $this->check_input_params($params,'add');

        if($check_input_params['ret'] != NS_CDS_SUCCE)
        {
            return $check_input_params;
        }
        if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(NS_CDS_FAIL,'指令池参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];
        $result_excute = nl_command::add(\m_config::get_hash_dc(), $params['base_info']);
        if($result_excute['ret'] != NS_CDS_SUCCE)
        {
            return self::return_data(NS_CDS_FAIL,'指令状态池入库数据库执行失败',$check_input_params['data_info'],$result_excute['reason']);
        }
        else
        {
            return self::return_data(NS_CDS_SUCCE,'ok',$check_input_params['data_info'],$result_excute['reason']);
        }
    }

    /**
     * 查询指令状态池数据
     * @param array $array_query 条件数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function query($array_query)
    {
        $last_data = null;
        $result_command = \nl_command::query_by_condition(\m_config::get_hash_dc(), $array_query['base_info']);

        if ($result_command['ret'] != 0 || !isset($result_vod['data_info']) || !is_array($result_command['data_info']) || empty($result_command['data_info']))
        {
            return $result_command;
        }

        foreach ($result_command['data_info'] as $data_info)
        {
            $last_data[] = array(
                'base_info' => $data_info,
                'ex_info' => '',
                'epg_info' => '',
                'cdn_info' => '',
            );
        }
        return self::return_data(0,'OK',$last_data);
    }

    /**
     * 删除指令池数据
     * @param $array_del
     * @return array
     */
    public function del($array_del)
    {
        return \nl_command::del_command(\m_config::get_hash_dc(), $array_del['base_info']);
    }
}
