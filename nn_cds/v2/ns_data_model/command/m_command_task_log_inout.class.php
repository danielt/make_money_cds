<?php
\ns_core\m_load::load_old("nn_logic/command/command_task_log.class.php");
\ns_core\m_load::load("ns_data_model.m_data_model");

class m_command_task_log_inout  extends m_data_model
{

    /**
     * 添加参数
     * @var array
     */
    private $add_params = array(
        'base_info' => array(
            'nns_command_task_id' =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '指令队列GUID',
            ),
            'nns_status'=>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '1-2',
                'desc'      => '状态码',
            ),
            'nns_desc'=>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-1024',
                'desc'      => '描述信息',
            ),
            'nns_execute_url'=>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-512',
                'desc'      => '日志执行路径',
            ),
        ),
    );

    /**
     * 检查注入参数
     * @param array $in_params 注入参数
     * @param string $action 操作参数行为
     * @return array array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function check_input_params($in_params,$action)
    {
        $notice_params = $last_params = array ();
        $str_params = "{$action}_params";
        if (!isset($this->$str_params))
        {
            return self::return_data(1, '没有设置' . $str_params . '参数的判断模式');
        }
        if (!is_array($this->$str_params))
        {
            return self::return_data(1, '参数非数组');
        }
        return $this->_check_input_params($this->$str_params, $in_params);
    }


    public function add($params)
    {
        //注入主媒资参数检查，模板参考
        $check_input_params = $this->check_input_params($params,'add');
        if($check_input_params['ret'] != NS_CDS_SUCCE)
        {
            return $check_input_params;
        }
        if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return $this->return_data(NS_CDS_FAIL,'队列参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];
        $result_execute = nl_command_task_log::add(\m_config::get_hash_dc(), $params['base_info']);
        if($result_execute['ret'] != NS_CDS_SUCCE)
        {
            return self::return_data(NS_CDS_FAIL, '指令队列日志入库失败', $check_input_params['data_info'], $result_execute['reason']);
        }
        else
        {
            return self::return_data(NS_CDS_SUCCE, 'ok', $check_input_params['data_info'], $result_execute['reason']);
        }
    }

}