<?php
\ns_core\m_load::load("ns_data_model.m_data_model");
\ns_core\m_load::load_old("nn_logic/vod_media/vod_media.class.php");
\ns_core\m_load::load_old("nn_logic/asset/asset_incidence_relation.class.php");
class m_media_inout extends m_data_model
{
    public $queue_type = 'media';
    /**
     * 删除参数检查模板
     * @var array
     */
    public $del_params = array(
        'base_info' => array(
            'nns_import_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-128',
                'desc' => '分集注入id',
            ),
            'nns_cp_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-32',
                'desc' => 'CP_ID',
            ),
            'nns_import_source' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-32',
                'desc' => '注入来源',
            ),
            'nns_media_service' => array(
                'rule' => '',
                'default' => 'http',
                'length' => '1-32',
                'desc' => '片源服务类型',
            ),
        ),
        'hide_info' => array(
            'nns_deleted' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '是否删除 0 未删除  |  1删除',
            ),
        ),
        'ex_info' => array(
            'nns_weight' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '0-512',
                'desc' => '注入优先级',
            ),
        ),
    );

    /**
     * 添加/修改参数检查模板
     * @var array
     */
    public $add_params = array(
        'base_info' => array(
            'nns_name' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => '片源名称',
            ),
            'nns_type' => array(
                'rule' => 'tinyint',
                'default' => '1',
                'length' => '',
                'desc' => 'nns_type',
            ),
            'nns_url' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '片源url地址',
            ),
            'nns_tag' => array(
                'rule' => 'noempty',
                'default' => '26,',
                'length' => '1-128',
                'desc' => '分集描述',
            ),
            'nns_mode' => array(
                'rule' => 'noempty',
                'default' => 'hd',
                'length' => '1-12',
                'desc' => '清晰度',
            ),
            'nns_kbps' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '',
                'desc' => '视频码率',
            ),
            'nns_content_id' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-32',
                'desc' => '内容ID',
            ),
            'nns_content_state' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '内容状态',
            ),
            'nns_filetype' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '',
                'desc' => '文件类型',
            ),
            'nns_play_count' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '',
                'desc' => '播放次数',
            ),
            'nns_score_total' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '',
                'desc' => '播放次数',
            ),
            'nns_score_count' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '',
                'desc' => '评分次数',
            ),
            'nns_video_import_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-128',
                'desc' => '上游主媒资注入ID',
            ),
            'nns_index_import_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-128',
                'desc' => '上游分集注入ID',
            ),
            'nns_import_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-128',
                'desc' => '片源注入ID',
            ),
            'nns_import_source' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-32',
                'desc' => '注入来源',
            ),
            'nns_dimensions' => array(
                'rule' => 'noempty',
                'default' => '0',
                'length' => '1-8',
                'desc' => '片源类型 2D | 3D | 100001 | 100002 ',
            ),
            'nns_ext_url' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-2048',
                'desc' => '上游方注入url 和其他信息  json存储',
            ),
            'nns_file_size' => array(
                'rule' => 'bigint',
                'default' => '8008',
                'length' => '',
                'desc' => '文件大小',
            ),
            'nns_file_time_len' => array(
                'rule' => 'int',
                'default' => '8008',
                'length' => '',
                'desc' => '播放时长（秒）',
            ),
            'nns_file_frame_rate' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '',
                'desc' => 'nns_file_frame_rate',
            ),
            'nns_file_resolution' => array(
                'rule' => 'noempty',
                'default' => '1920*1080',
                'length' => '1-12',
                'desc' => '分辨率',
            ),
            'nns_cp_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-32',
                'desc' => 'CP_ID',
            ),
            'nns_ext_info' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-2048',
                'desc' => '片源切片扩展信息,json串',
            ),
            'nns_drm_enabled' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '是否DRM加密 0不加密 | 1加密',
            ),
            'nns_drm_encrypt_solution' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-32',
                'desc' => '加密方案',
            ),
            'nns_drm_ext_info' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => 'drm扩展信息（json存储）',
            ),
            'nns_domain' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-32',
                'desc' => '上游注入的网络域',
            ),
            'nns_media_type' => array(
                'rule' => 'tinyint',
                'default' => '1',
                'length' => '',
                'desc' => '片源类型,1点播片源2回看片源',
            ),
            'nns_original_live_id' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-64',
                'desc' => '频道原始id(点播片源时此字段无意义)',
            ),
            'nns_start_time' => array(
                'rule' => '',
                'default' => '0000-00-00 00:00:00',
                'length' => '',
                'desc' => '回看开始时间(点播片源时此字段无意义)',
            ),
            'nns_media_service' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-32',
                'desc' => '片源服务类型',
            ),
            'nns_conf_info' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '用于存放片源扩展配置信息',
            ),
            'nns_encode_flag' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '是否是转码生成的文件 0 否，1是',
            ),
            'nns_live_to_media' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '转码类型  0 点播 | 1直播转点播',
            ),
            'nns_media_service_type' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-32',
                'desc' => '片源服务类型状态',
            ),
        ),
        'ex_info' => array(
            'file_hash' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'file_hash',
            ),
            'file_width' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'file_width',
            ),
            'file_height' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'file_height',
            ),
            'file_scale' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'file_scale',
            ),
            'file_coding' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'file_coding',
            ),
            'third_file_id' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '芒果二级id注入',
            ),
            'nns_weight' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '0-512',
                'desc' => '注入优先级',
            ),
        ),
        'hide_info' => array(
            'nns_id' => array(
                'rule' => 'noempty',
                'default' => 'GUID',
                'length' => '32',
                'desc' => '分集GUID',
            ),
            'nns_deleted' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '是否删除 0 未删除  |  1删除',
            ),
            'nns_vod_index_id' => array(
                'rule' => 'noempty',
                'default' => 'parent|index_action|base_info+unset+nns_index_import_id+nns_import_id#base_info+set+nns_cp_id+nns_cp_id#base_info+set+nns_import_source+nns_import_source#base_info+set+nns_deleted+nns_deleted|base_info+nns_id',
                'length' => '32',
                'desc' => '分集GUID',
            ),
            'nns_vod_id' => array(
                'rule' => 'noempty',
                'default' => 'parent|vod_action|base_info+set+nns_vod_index_id+nns_vod_id#base_info+unset+nns_video_import_id+nns_asset_import_id#base_info+set+nns_cp_id+nns_cp_id#base_info+set+nns_import_source+nns_import_source#base_info+set+nns_deleted+nns_deleted|base_info+nns_id',
                'length' => '32',
                'desc' => '主媒资GUID',
            ),
            'nns_state' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '状态',
            ),
            'nns_check' => array(
                'rule' => 'tinyint',
                'default' => '1',
                'length' => '',
                'desc' => '审核状态',
            ),
            'nns_vod_index' => array(
                'rule' => 'smallint',
                'default' => 'parent|index_action|base_info+unset+nns_index_import_id+nns_import_id#base_info+set+nns_cp_id+nns_cp_id#base_info+set+nns_import_source+nns_import_source#base_info+set+nns_deleted+nns_deleted|base_info+nns_index',
                'length' => '',
                'desc' => '分集号 0开始',
            ),
            'nns_media_caps' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-16',
                'desc' => '影片输出类型',
            ),
            'nns_status' => array(
                'rule' => 'tinyint',
                'default' => '1',
                'length' => '',
                'desc' => '(add:1|modify:2|destroy:3|ok:0|execute:4)',
            ),
            'nns_action' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-12',
                'desc' => 'add|modify|destroy|ok|execute',
            ),
            'nns_media_name' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-64',
                'desc' => 'ts文件名称',
            ),
            'nns_clip_state' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '切片状态0 原始状态 | 1 等待下载  | 2 正在下载 | 3 下载成功 | 4 下载失败 ',
            ),
        ),
    );

    /**
     * 导出参数模板
     * @var array
     */
    public $import_params = array(
        'base_info' => array(
            'nns_name' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => '分集名称',
            ),
            'nns_type' => array(
                'rule' => 'tinyint',
                'default' => '1',
                'length' => '',
                'desc' => 'nns_type',
            ),
            'nns_url' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '片源url地址',
            ),
            'nns_tag' => array(
                'rule' => 'noempty',
                'default' => '26,',
                'length' => '1-128',
                'desc' => '分集描述',
            ),
            'nns_mode' => array(
                'rule' => 'noempty',
                'default' => 'hd',
                'length' => '1-12',
                'desc' => '分集海报',
            ),
            'nns_kbps' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '',
                'desc' => '视频码率',
            ),
            'nns_content_id' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-32',
                'desc' => '内容ID',
            ),
            'nns_content_state' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '内容状态',
            ),
            'nns_filetype' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '',
                'desc' => '文件类型',
            ),
            'nns_play_count' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '',
                'desc' => '播放次数',
            ),
            'nns_score_total' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '',
                'desc' => '播放次数',
            ),
            'nns_score_count' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '',
                'desc' => '评分次数',
            ),
            'nns_video_import_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-128',
                'desc' => '上游主媒资注入ID',
            ),
            'nns_index_import_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-128',
                'desc' => '上游分集注入ID',
            ),
            'nns_import_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-128',
                'desc' => '上游分集注入ID',
            ),
            'nns_import_source' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-32',
                'desc' => '注入来源',
            ),
            'nns_dimensions' => array(
                'rule' => 'noempty',
                'default' => '0',
                'length' => '1-8',
                'desc' => '片源类型 2D | 3D | 100001 | 100002 ',
            ),
            'nns_ext_url' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-2048',
                'desc' => '上游方注入url 和其他信息  json存储',
            ),
            'nns_file_size' => array(
                'rule' => 'int',
                'default' => '8008',
                'length' => '',
                'desc' => '文件大小',
            ),
            'nns_file_time_len' => array(
                'rule' => 'int',
                'default' => '8008',
                'length' => '',
                'desc' => '播放时长（秒）',
            ),
            'nns_file_frame_rate' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '',
                'desc' => 'nns_file_frame_rate',
            ),
            'nns_file_resolution' => array(
                'rule' => 'noempty',
                'default' => '1920*1080',
                'length' => '1-12',
                'desc' => '分辨率',
            ),
            'nns_cp_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-32',
                'desc' => 'CP_ID',
            ),
            'nns_ext_info' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-2048',
                'desc' => '片源切片扩展信息,json串',
            ),
            'nns_drm_enabled' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '是否DRM加密 0不加密 | 1加密',
            ),
            'nns_drm_encrypt_solution' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-32',
                'desc' => '加密方案',
            ),
            'nns_drm_ext_info' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => 'drm扩展信息（json存储）',
            ),
            'nns_domain' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-32',
                'desc' => '上游注入的网络域',
            ),
            'nns_media_type' => array(
                'rule' => 'tinyint',
                'default' => '1',
                'length' => '',
                'desc' => '片源类型,1点播片源2回看片源',
            ),
            'nns_original_live_id' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-64',
                'desc' => '频道原始id(点播片源时此字段无意义)',
            ),
            'nns_start_time' => array(
                'rule' => '',
                'default' => '0000-00-00 00:00:00',
                'length' => '',
                'desc' => '回看开始时间(点播片源时此字段无意义)',
            ),
            'nns_media_service' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-32',
                'desc' => '片源服务类型',
            ),
            'nns_conf_info' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '用于存放片源扩展配置信息',
            ),
            'nns_encode_flag' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '是否是转码生成的文件 0 否，1是',
            ),
            'nns_live_to_media' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '转码类型  0 点播 | 1直播转点播',
            ),
        ),
        'ex_info' => array(
            'file_hash' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'file_hash',
            ),
            'file_width' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'file_width',
            ),
            'file_height' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'file_height',
            ),
            'file_scale' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'file_scale',
            ),
            'file_coding' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'file_coding',
            ),
        ),
        'hide_info' => array(
            'nns_id' => array(
                'rule' => 'noempty',
                'default' => 'GUID',
                'length' => '32',
                'desc' => '分集GUID',
            ),
            'nns_vod_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '32',
                'desc' => '主媒资GUID',
            ),
            'nns_vod_index_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '32',
                'desc' => '分集GUID',
            ),
            'nns_state' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '状态',
            ),
            'nns_deleted' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '是否删除 0 未删除  |  1删除',
            ),
            'nns_check' => array(
                'rule' => 'tinyint',
                'default' => '1',
                'length' => '',
                'desc' => '审核状态',
            ),
            'nns_vod_index' => array(
                'rule' => 'smallint',
                'default' => '',
                'length' => '',
                'desc' => '分集号 0开始',
            ),
            'nns_media_caps' => array(
                'rule' => 'noempty',
                'default' => 'VOD',
                'length' => '1-16',
                'desc' => '分集号 0开始',
            ),
            'nns_status' => array(
                'rule' => 'tinyint',
                'default' => '1',
                'length' => '',
                'desc' => '(add:1|modify:2|destroy:3|ok:0|execute:4)',
            ),
            'nns_action' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-12',
                'desc' => 'add|modify|destroy|ok|execute',
            ),
            'nns_media_name' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-64',
                'desc' => 'ts文件名称',
            ),
            'nns_clip_state' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '切片状态0 原始状态 | 1 等待下载  | 2 正在下载 | 3 下载成功 | 4 下载失败 ',
            ),
        ),
    );

    /**
     * 新片源替换逻辑，清晰度层次关系
     * @var array
     */
    public $media_mode = array(
        'low' => 1,
        'std' => 2,
        'hd' => 3,
        'sd' =>4,
        '4' => 5,
        '4k' => 5
    );

    /**
     * 检查注入参数
     * @param array $in_params 注入参数
     * @param string $action 操作参数行为
     * @return array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function check_input_params($in_params, $action)
    {
        $notice_params = $last_params = array();
        $str_params = "{$action}_params";
        if (!isset($this->$str_params))
        {
            return self::return_data(1, '没有设置' . $str_params . '参数的判断模式');
        }
        if (!is_array($this->$str_params))
        {
            return self::return_data(1, '参数非数组');
        }
        return $this->_check_input_params($this->$str_params, $in_params, 'message_queue');
    }

    /**
     * 片源模板导出
     * @return array|multitype //array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function export()
    {

        return self::return_data(0, 'ok', array('del_params' => $this->del_params, 'import_params' => $this->import_params));
    }

    /**
     * 添加片源数据
     * @param array $params 添加数据 不能为空
     * @return array|multitype //array('ret'=>'状态码','reason'=>'原因','data'=>'数据')/|multitype:number
     */
    public function add($params)
    {
        //先检查父级是否存在
        $check_index_exist = nl_asset_incidence_relation::query_asset_relation(\m_config::get_dc(), NS_CDS_INDEX, $params['base_info']['nns_index_import_id'], $params['base_info']['nns_cp_id'], 1);
        if (!is_array($check_index_exist['data_info']))
        {
            return self::return_data(NS_CDS_FAIL, "检查分集在平台不存在或已删除.原因：" . $check_index_exist['reason']);
        }

        //注入片源参数检查，模板参考
        $check_input_params = $this->check_input_params($params, 'add');
        if ($check_input_params['ret'] != NS_CDS_SUCCE)
        {
            return $check_input_params;
        }
        if (!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(NS_CDS_FAIL, '片源操作,分集参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];
        #TODO CP配置逻辑

        //片源替换逻辑，高清晰度替换低清晰度     xinxin.deng 2018/7/18 19:15
        $arr_cp_data = \m_config::_get_cp_info($params['base_info']['nns_cp_id']);
        $cp_config = isset($arr_cp_data['data_info']['nns_config']) ? $arr_cp_data['data_info']['nns_config'] : array();
        //片源替换模式一【0 关 1开 注入ID不同且匹配信息相同】---老代码没有实际用到
        $media_import_module = isset($cp_config['media_replace_mode_one']) && (int)$cp_config['media_replace_mode_one'] == 1 ? 1 : 0;
        //片源替换模式二【0 关 1开 注入ID相同】
        $media_import_comm = isset($cp_config['media_replace_mode_two']) && (int)$cp_config['media_replace_mode_two'] == 1 ? 1 : 0;
        //片源替换模式三【0 关 1开 高清晰度替换低清晰度规则】
        $media_import_module_v2 = isset($cp_config['media_replace_mode_three']) && (int)$cp_config['media_replace_mode_three'] == 1 ? 1 : 0;

        //根据注入id等查询是否已经存在此片源
        $arr_media_exist = array(
            'nns_import_id' => $params['base_info']['nns_import_id'],
            'nns_import_source' => $params['base_info']['nns_import_source'],
            'nns_cp_id' => $params['base_info']['nns_cp_id'],
            'nns_deleted' => $params['base_info']['nns_deleted'],
            'nns_media_service' => $params['base_info']['nns_media_service'],
            'nns_mode' => $params['base_info']['nns_mode'],
            'nns_kbps' => $params['base_info']['nns_kbps'],
        );

        $result_media_exist = \nl_vod_media_v2::query_by_condition(m_config::$obj_dc, $arr_media_exist);
        if ($result_media_exist['ret'] != NS_CDS_SUCCE)
        {
            return $result_media_exist;
        }

        //$modify_enable = true;
        //$media_import_comm片源替换模式二【0 关 1开 注入ID相同】
        if (isset($result_media_exist['data_info']) && is_array($result_media_exist['data_info']) && !empty($result_media_exist['data_info']))
        {
            if ($media_import_comm)
            {//$media_import_comm片源替换模式二【0 关 1开 注入ID相同】】
                $this->write_message_info('片源替换模式二[注入ID相同，删除之前得]开始进行,已存在片源数据:' . var_export($result_media_exist['data_info'], true));
                $this->write_message_info('片源替换模式二[注入ID相同，删除之前得]开始进行,传入片源数据:' . var_export($params['base_info'], true));
                $del_ret = self::del(array(
                    'base_info' => $arr_media_exist,
                    'ex_info' => array(
                        'nns_weight' => 999
                    ),//将优先级置为最高
                ));//先删除之前的,然后直接进行添加
                $this->write_message_info($del_ret);
                //$modify_enable = false;
            }
            else
            {
                return self::return_data(NS_CDS_FAIL, "查询已经存在此片源【{$params['base_info']['nns_import_id']}】,请先进行回收再添加,或者在播控平台》上游配置》片源替换规则，选择配置为片源替换规则二");
            }
        }

        //查询对应主媒资和对应分集下已经存在的片源
        $media_query_arr = array(
            'nns_vod_id' => $params['base_info']['nns_vod_id'],
            'nns_vod_index_id' => $params['base_info']['nns_vod_index_id'],
            'nns_cp_id' => $params['base_info']['nns_cp_id'],
            'nns_media_service' => $params['base_info']['nns_media_service'],
            'nns_deleted' => 0,
        );
        $media_mode_arr = \nl_vod_media_v2::query_by_condition(\m_config::get_dc(), $media_query_arr);
        if ($media_mode_arr['ret'] != NS_CDS_SUCCE)
        {
            return self::return_data(NS_CDS_FAIL, $media_mode_arr['reason']);
        }

        if ($media_mode_arr['ret'] == 0 && is_array($media_mode_arr['data_info']) && count($media_mode_arr['data_info']) > 0)
        {
            foreach ($media_mode_arr['data_info'] as $item)
            {
                //片源替换模式一【0 关 1开 注入ID不同且匹配信息相同】
                //如果开启，注入id不同，并且清晰度、码率相同，则
                if ($media_import_module && $this->media_mode[$params['base_info']['nns_mode']] == $this->media_mode[$item['nns_mode']]
                    && $params['base_info']['nns_kbps'] = $item['nns_kbps'] && $params['base_info']['nns_import_id'] != $item['nns_import_id'])
                {
                    $this->write_message_info('片源替换模式一[注入ID不同且匹配信息相同]开始进行,已存在片源数据:' . var_export($item, true));
                    $this->write_message_info('片源替换模式一[注入ID不同且匹配信息相同]开始进行,传入片源数据:' . var_export($params['base_info'], true));

                    $del_low_mode_arr = array(
                        'base_info' =>  array(
                            'nns_import_id' => $item['nns_import_id'],
                            'nns_import_source' => $item['nns_import_source'],
                            'nns_cp_id' => $item['nns_cp_id'],
                            'nns_media_service' => $params['base_info']['nns_media_service'],
                        ),
                        'ex_info' => array(
                            'nns_weight' => 999,//将优先级置为最高
                        ),
                    );
                    $del_ret = self::del($del_low_mode_arr);
                    $this->write_message_info($del_ret);
                }

                //片源替换模式三【0 关 1开 高清晰度替换低清晰度规则】
                if ( $media_import_module_v2)
                {
                    //如果清晰度为空或者传入清晰度大于等于已经存在的片源的清晰度就进行删除

                    if (empty($item['nns_mode']) || $this->media_mode[$params['base_info']['nns_mode']] >= $this->media_mode[$item['nns_mode']])
                    {
                        $this->write_message_info('片源替换模式三[高清晰度替换低清晰度]开始进行,已存在片源数据:' . var_export($item, true));
                        $this->write_message_info('片源替换模式三[高清晰度替换低清晰度]开始进行,传入片源数据:' . var_export($params['base_info'], true));
                        $del_low_mode_arr = array(
                            'base_info' =>  array(
                                'nns_import_id' => $item['nns_import_id'],
                                'nns_import_source' => $item['nns_import_source'],
                                'nns_cp_id' => $item['nns_cp_id'],
                                'nns_media_service' => $params['base_info']['nns_media_service'],
                            ),
                            'ex_info' => array(
                                'nns_weight' => 999,//将优先级置为最高
                            ),
                        );
                        $del_ret = self::del($del_low_mode_arr);
                        $this->write_message_info($del_ret);
                    }

                    if ($this->media_mode[$params['base_info']['nns_mode']] < $this->media_mode[$item['nns_mode']])
                    {
                        return self::return_data(NS_CDS_SUCCE, '[执行高清晰度替换低清晰度逻辑],已经存在更高清晰度的片源了');
                    }
                }

            }
        }


        //队列数组
        $arr_queue = array(
            'base_info' => array(
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
            )
        );
        /**************************片源只有添加没有修改 xinxin.deng 2018/11/6 19:05**************************/
        //if ($modify_enable && isset($result_media_exist['data_info']) && is_array($result_media_exist['data_info']) && !empty($result_media_exist['data_info']))
        //{
        //    $check_input_params['data_info']['info']['out']['base_info']['nns_action'] = $params['base_info']['nns_action'] = 'modify';
        //    $check_input_params['data_info']['info']['out']['base_info']['nns_deleted'] = $params['base_info']['nns_deleted'] = '0';
        //    $check_input_params['data_info']['info']['out']['base_info']['nns_status'] = $params['base_info']['nns_status'] = '2';
        //    unset($params['base_info']['nns_id']);
        //    $result_execute = nl_vod_media_v2::edit(m_config::get_dc(), $params['base_info'], $result_media_exist['data_info'][0]['nns_id']);
        //
        //    if ($result_execute['ret'] != NS_CDS_SUCCE)
        //    {
        //        return $this->write_message_info('修改片源信息失败,原因' . $result_execute['reason']);
        //    }
        //
        //    $arr_queue['base_info']['nns_id'] = $result_media_exist['data_info'][0]['nns_id'];
        //}
        //else
        //{
            $check_input_params['data_info']['info']['out']['base_info']['nns_action'] = $params['base_info']['nns_action'] = 'add';
            $check_input_params['data_info']['info']['out']['base_info']['nns_deleted'] = $params['base_info']['nns_deleted'] = '0';
            $check_input_params['data_info']['info']['out']['base_info']['nns_status'] = $params['base_info']['nns_status'] = '1';
            $result_execute = nl_vod_media_v2::add_v2(m_config::$obj_dc, $params['base_info']);
            if ($result_execute['ret'] != NS_CDS_SUCCE)
            {
                $this->write_message_info('添加片源信息失败,原因' . $result_execute['reason']);
                return $result_execute;
            }
            $arr_queue['base_info']['nns_id'] = $params['base_info']['nns_id'];
        //}

        if (!isset($params['ex_info']) || !is_array($params['ex_info']) || empty($params['ex_info']))
        {
            return $result_execute;
        }
        $result_ex_delete = nl_vod_media_v2::del_ex_by_condition(m_config::$obj_dc, array('nns_vod_media_id' => $params['base_info']['nns_import_id'], 'nns_cp_id' => $params['base_info']['nns_cp_id']));
        if ($result_ex_delete['ret'] != NS_CDS_SUCCE)
        {
            $this->write_message_info('删除片源扩展信息失败,原因' . $result_ex_delete['reason']);
            return $result_ex_delete;
        }
        $arr_add_index_ex = null;
        foreach ($params['ex_info'] as $ex_key => $ex_val)
        {
            if (strlen($ex_val) < 1)
            {
                continue;
            }
            $arr_add_index_ex[] = array(
                'nns_vod_media_id' => $params['base_info']['nns_import_id'],
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
                'nns_key' => $ex_key,
                'nns_value' => $ex_val,
            );
        }
        if (!empty($arr_add_index_ex) && is_array($arr_add_index_ex))
        {
            $result_ex_add = nl_vod_media_v2::add_ex_v2(m_config::$obj_dc, $arr_add_index_ex);
            if ($result_ex_add['ret'] != NS_CDS_SUCCE)
            {
                $this->write_message_info('删除片源扩展信息失败,原因' . $result_ex_add['reason']);
                return $result_ex_add;
            }
        }
        //入中心注入指令
        $result = $this->push_to_center_import($arr_queue, 'media', $params['ex_info']['nns_weight'], $params['base_info']['nns_action']);
        $this->write_message_info($result);
        unset($arr_queue);
        return self::return_data(NS_CDS_SUCCE, 'OK', $check_input_params['data_info']);
    }

    /**
     * 修改分集数据
     * @param array $params 修改数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function edit($params)
    {
        return $this->add($params);
    }

    /**
     * 删除分集数据
     * @param array $params 数据数组，不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function del($params)
    {
        //注入分集参数检查，模板参考
        $check_input_params = $this->check_input_params($params, 'del');
        if ($check_input_params['ret'] != NS_CDS_SUCCE)
        {
            return $check_input_params;
        }
        if (!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(NS_CDS_FAIL, '片源参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];
        $result_vod_exist = nl_vod_media_v2::query_by_condition(m_config::$obj_dc, $params['base_info']);
        if ($result_vod_exist['ret'] != NS_CDS_SUCCE)
        {
            return self::return_data(NS_CDS_FAIL, '片源数据查询失败,原因' . $result_vod_exist['reason']);
        }
        if (empty($result_vod_exist['data_info']) || count($result_vod_exist['data_info']) < 1
            || is_null($result_vod_exist['data_info']) || !is_array($result_vod_exist['data_info']))
        {
            return self::return_data(NS_CDS_SUCCE, addslashes("没有需要删除的数据，{$result_vod_exist['reason']}"));
        }
        $arr_del = null;
        if (isset($result_vod_exist['data_info']) && is_array($result_vod_exist['data_info']) && !empty($result_vod_exist['data_info']))
        {
            foreach ($result_vod_exist['data_info'] as $data_info)
            {
                $arr_del[] = $data_info['nns_id'];
            }
        }
        if (empty($arr_del) || !is_array($arr_del))
        {
            return self::return_data(NS_CDS_SUCCE, addslashes("没有需要删除的数据，{$result_vod_exist['reason']}"));
        }
        $result_execute = nl_vod_media_v2::edit(m_config::$obj_dc, array('nns_deleted' => 1), $arr_del);
        if ($result_execute['ret'] != NS_CDS_SUCCE)
        {
            return $result_execute;
        }
        //入中心注入指令
        $arr_queue = array(
            'base_info' => array(
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
            )
        );
        if (!empty($arr_del) && is_array($arr_del))
        {
            foreach ($arr_del as $del_val)
            {
                $arr_queue['base_info']['nns_id'] = $del_val;
                $result = $this->push_to_center_import($arr_queue, 'media', $params['ex_info']['nns_weight'], 'destroy');
                $this->write_message_info($result);
            }
        }
        return self::return_data(NS_CDS_SUCCE, 'ok', $check_input_params['data_info']);
    }
    
    /**
     * 查询分集数据
     * @param array $array_query 条件数据 不能为空
     * @param boolean $is_need_ext false 不需要查询扩展数据 | true 需要查询扩展数据
     * @param boolean $is_need_epg_import_id false 不需要查询EPG扩展数据 | true 需要查询EPG扩展数据
     * @param boolean $is_need_cdn_import_id false 不需要查询CDN扩展数据 | true 需要查询CDN扩展数据
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function query($array_query, $is_need_ext = false, $epg_model = '', $cdn_model = '')
    {
        $last_data = null;
        $result_vod = nl_vod_media_v2::query_by_condition(m_config::$obj_dc, $array_query['base_info']);
        if ($result_vod['ret'] != 0 || !isset($result_vod['data_info']) || !is_array($result_vod['data_info']) || empty($result_vod['data_info']))
        {
            return $result_vod;
        }
        foreach ($result_vod['data_info'] as $data_info)
        {
            $temp_epg_data = $temp_cdn_data = $temp_ex_data = null;
            if ($is_need_ext)
            {
                $data_info['nns_asset_import_id'] = isset($data_info['nns_asset_import_id']) ? $data_info['nns_asset_import_id'] : $data_info['nns_import_id'];
                $result_vod_ex = nl_vod_media_v2::query_ex_by_id(m_config::$obj_dc, $data_info['nns_asset_import_id'], $data_info['nns_cp_id']);
                if ($result_vod_ex['ret'] != 0)
                {
                    return $result_vod_ex;
                }
                if (isset($result_vod_ex['data_info']) && is_array($result_vod_ex['data_info']) && !empty($result_vod_ex['data_info']))
                {
                    foreach ($result_vod_ex['data_info'] as $ex_data_info)
                    {
                        $temp_ex_data[$ex_data_info['nns_key']] = $ex_data_info['nns_value'];
                    }
                }
            }
        
            if (strlen($epg_model) >0)
            {
                $temp_epg_data = $this->get_asset_id_by_sp_id($epg_model,'media',$data_info,'epg');
                if($temp_epg_data['ret'] !=0)
                {
                    return $temp_epg_data;
                }
                $temp_epg_data = (isset($temp_epg_data['data_info']) && strlen($temp_epg_data['data_info']) >0) ? $temp_epg_data['data_info'] : '';
                #TODO
            }
            if (strlen($cdn_model) >0)
            {
                $temp_cdn_data = $this->get_asset_id_by_sp_id($cdn_model,'media',$data_info,'cdn');
                if($temp_cdn_data['ret'] !=0)
                {
                    return $temp_cdn_data;
                }
                $temp_cdn_data = (isset($temp_cdn_data['data_info']) && strlen($temp_cdn_data['data_info']) >0) ? $temp_cdn_data['data_info'] : '';
                #TODO
            }
            $last_data[] = array(
                'base_info' => $data_info,
                'ex_info' => $temp_ex_data,
                'epg_info' => $temp_epg_data,
                'cdn_info' => $temp_cdn_data,
            );
        }
        return self::return_data(0, 'OK', $last_data);
    }
}