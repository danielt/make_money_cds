<?php
\ns_core\m_load::load("ns_data_model.m_data_model");
\ns_core\m_load::load_old("nn_logic/video/vod_index.class.php");
\ns_core\m_load::load_old("nn_logic/vod_media/vod_media.class.php");
\ns_core\m_load::load_old("nn_logic/asset/asset_incidence_relation.class.php");

class m_index_inout  extends m_data_model
{
    /**
     * 删除参数检查模板
     * @var unknown
     */
    public $del_params = array(
        'base_info'     =>      array(
            'nns_import_id'                 =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-128',
                'desc'      => '分集注入id',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => 'CP_ID',
            ),
            'nns_import_source'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '注入来源',
            ),
        ),
        'hide_info'     =>      array(
            'nns_deleted'                   =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '是否删除 0 未删除  |  1删除',
            ),
        ),
        'ex_info' => array(
            'nns_weight' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '0-512',
                'desc' => '注入优先级',
            ),
        ),
    );
    
    
    /**
     * 修改参数检查模板
     * @var array
     */
    public $add_params = array(
        'base_info'     =>      array(
            'nns_name'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => '分集名称',
            ),
            'nns_index'                     =>      array (
                'rule'      => 'smallint',
                'default'   => '',
                'length'    => '',
                'desc'      => '分集号   0 开始',
            ),
            'nns_time_len'                  =>      array (
                'rule'      => 'smallint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '分集时长（秒）',
            ),
            'nns_summary'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-4096',
                'desc'      => '分集描述',
            ),
            'nns_image'                     =>      array (
                'rule'      => 'image#index',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => '分集海报',
            ),
            'nns_play_count'                =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '播放次数',
            ),
            'nns_score_total'               =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '评分总数',
            ),
            'nns_score_count'               =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '评分次数',
            ),
            'nns_video_import_id'           =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-128',
                'desc'      => '上游主媒资注入ID',
            ),
            'nns_import_id'                 =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-128',
                'desc'      => '上游分集注入ID',
            ),
            'nns_import_source'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '注入来源',
            ),
            'nns_actor'                     =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-512',
                'desc'      => '演员，多个演员/分割',
            ),
            'nns_director'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-128',
                'desc'      => '导演，多个导演/分割',
            ),
            'nns_release_time'              =>      array (
                'rule'      => 'datetime',
                'default'   => '',
                'length'    => '',
                'desc'      => '发布日期',
            ),
            'nns_update_time'               =>      array (
                'rule'      => 'datetime',
                'default'   => '',
                'length'    => '',
                'desc'      => '更新时间',
            ),
            'nns_watch_focus'               =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-512',
                'desc'      => '分集看点',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => 'CP_ID',
            ),
            'nns_conf_info'                 =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-512',
                'desc'      => '用于存放分集扩展配置信息',
            ),
            'nns_ext_url'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-2048',
                'desc'      => '上游方注入url 和其他信息  json存储',
            ),
        ),
        'ex_info'       =>      array (
            'isintact'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => 'isintact',
            ),
            'subordinate_name'              =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => 'subordinate_name',
            ),
            'initials'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => 'initials',
            ),
            'publisher'                     =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => 'publisher',
            ),
            'first_spell'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => 'first_spell',
            ),
            'caption_language'              =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => 'caption_language',
            ),
            'language'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => 'language',
            ),
            'region'                        =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => 'region'
            ),
            'adaptor'                       =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => 'adaptor',
            ),
            'sreach_key'                    =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => 'sreach_key',
            ),
            'event_tag'                     =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => 'event_tag',
            ),
            'year'                          =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => 'year',
            ),
            'sort_name'                     =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => 'sort_name',
            ),
            'third_clip_id' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '芒果二级id注入',
            ),
            'nns_weight' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '0-512',
                'desc' => '注入优先级',
            ),
        ),
        'hide_info'     =>      array(
            'nns_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => 'GUID',
                'length'    => '32',
                'desc'      => '分集GUID',
            ),
            'nns_deleted'                   =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '是否删除 0 未删除  |  1删除',
            ),
            'nns_vod_id'                    =>      array (
                'rule'      => 'noempty',
                'default'   => 'parent|vod_action|base_info+unset+nns_video_import_id+nns_asset_import_id#base_info+set+nns_cp_id+nns_cp_id#base_info+set+nns_import_source+nns_import_source#base_info+set+nns_deleted+nns_deleted|base_info+nns_id',
                'length'    => '32',
                'desc'      => '分集GUID',
            ),
            'nns_new_media'                 =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-10',
                'desc'      => '最新片源',
            ),
            'nns_status'                    =>      array (
                'rule'      => 'tinyint',
                'default'   => '1',
                'length'    => '',
                'desc'      => '(add:1|modify:2|destroy:3|ok:0|execute:4)',
            ),
            'nns_new_media_time'            =>      array (
                'rule'      => 'datetime',
                'default'   => 'datetime',
                'length'    => '',
                'desc'      => '新注入的片源时间',
            ),
            'nns_action'                    =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-12',
                'desc'      => 'add|modify|destroy|ok|execute',
            ),
            'nns_state'                     =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '状态',
            ),
        ),
    );

    public $queue_type = 'index';
    /**
     * 检查注入参数
     * @param array $in_params 注入参数
     * @param string $action 操作参数行为
     * @return array|multitype array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function check_input_params($in_params,$action)
    {
        $notice_params = $last_params = array ();
        $str_params = "{$action}_params";
        if (!isset($this->$str_params))
        {
            return self::return_data(1, '没有设置' . $str_params . '参数的判断模式');
        }
        if (!is_array($this->$str_params))
        {
            return self::return_data(1, '参数非数组');
        }
        return $this->_check_input_params($this->$str_params, $in_params,'message_queue');
    }
    
    /**
     * 分集模板导出
     * @return array|multitype array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function export()
    {
        
        return self::return_data(0,'ok',array('del_params'=>$this->del_params,'import_params'=>$this->import_params));
    }
    
    /**
     * 添加分集数据
     * @param array $params 添加数据 不能为空
     * @return array|multitype array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function add($params)
    {
        //先检查父级是否存在
        $check_video_exist = nl_asset_incidence_relation::query_asset_relation(\m_config::get_dc(), NS_CDS_VIDEO, $params['base_info']['nns_video_import_id'], $params['base_info']['nns_cp_id'], 0);
        if (!is_array($check_video_exist['data_info']))
        {
            return self::return_data(NS_CDS_FAIL, "检查主媒资在平台不存在或已删除.原因" . $check_video_exist['reason']);
        }
        $arr_cp_data = \m_config::_get_cp_info($params['base_info']['nns_cp_id']);
        $arr_cp_config = (isset($arr_cp_data['data_info']['nns_config']) && count($arr_cp_data['data_info']['nns_config']) > 0) ? $arr_cp_data['data_info']['nns_config'] : array();

        //注入分集参数检查，模板参考
        $check_input_params = $this->check_input_params($params,'add');
        if($check_input_params['ret'] != NS_CDS_SUCCE)
        {
            return $check_input_params;
        }
        if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(NS_CDS_FAIL,'分集参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];

        //特殊验证分集与主媒资关系
        $int_index =$params['base_info']['nns_index']+1;

        $m_queue_model = new \ns_model\m_queue_model();
        $result_vod_info = $m_queue_model->_get_queue_info($params['base_info']['nns_vod_id'], 'video');
        if($result_vod_info['ret'] != NS_CDS_SUCCE)
        {
            return $result_vod_info;
        }
        if(!isset($result_vod_info['data_info']['video']['base_info']['nns_all_index']))
        {
            return self::return_data(NS_CDS_FAIL,"主媒资总集数未查询到",$check_input_params['data_info']);
        }
        if($int_index > $result_vod_info['data_info']['video']['base_info']['nns_all_index'])
        {
            return self::return_data(NS_CDS_FAIL,"主媒资总集数{$result_vod_info['data_info']['video']['base_info']['nns_all_index']}小于分集集数{$int_index}注入的集数请检查分集集数情况",$check_input_params['data_info']);
        }
        #TODO CP配置逻辑
        $arr_index_exist = array(
            'nns_import_id'=>$params['base_info']['nns_import_id'],
            'nns_import_source'=>$params['base_info']['nns_import_source'],
            'nns_cp_id'=>$params['base_info']['nns_cp_id'],
            //'nns_index'=>$params['base_info']['nns_index'],
            'nns_deleted' => $params['base_info']['nns_deleted'],
        );
        $result_index_exist = nl_vod_index::query_by_condition(\m_config::get_dc(), $arr_index_exist);

        if($result_index_exist['ret'] != NS_CDS_SUCCE)
        {
            return $result_index_exist;
        }
        //队列数组
        $arr_queue = array(
            'base_info' => array(
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
            )
        );

        //是否走修改
        $bool_modify = false;
        if(isset($result_index_exist['data_info']) && is_array($result_index_exist['data_info']) && !empty($result_index_exist['data_info']))
        {
            $count_vod_data = count($result_index_exist['data_info']);
            if($count_vod_data > 1)
            {
                return self::return_data(NS_CDS_FAIL,"查询分集信息相同条件下分集数量>{$count_vod_data}原有的注入有问题,{$result_index_exist['reason']}");
            }
            $bool_modify = true;
            if ($result_index_exist['data_info'][0]['nns_index'] != $params['base_info']['nns_index'])
            {
                if (isset($arr_cp_config['clip_replace_mode_one']) && $arr_cp_config['clip_replace_mode_one'] == 1)
                {
                    return self::return_data(NS_CDS_FAIL,"[注入id查询存在分集,修正分集号]cp配置[分集替换逻辑clip_replace_mode_one]不进行替换");
                }
                $this->write_message_info("[注入id查询存在分集]修改分集集号(存在的分集号[{$result_index_exist['data_info'][0]['nns_index']}]不等于修改后的分集号[{$params['base_info']['nns_index']}],先将存在的分集号先删除)");
                $del_exist_index = array(
                    'base_info' => array(
                        'nns_import_id' => $params['base_info']['nns_import_id'],
                        'nns_import_source' => $params['base_info']['nns_import_source'],
                        'nns_cp_id' => $params['base_info']['nns_cp_id'],
                    ),
                    'ex_info' => array(
                        'nns_weight' => 999,//将注入优先级置为最高
                    ),
                );
                $del_exist_index_re = self::del($del_exist_index);
                $this->write_message_info('删除结果为:' . var_export($del_exist_index_re, true));
                if ($del_exist_index_re['ret'] != NS_CDS_SUCCE)
                {
                    return self::return_data(NS_CDS_FAIL,'[注入id查询存在分集]分集号修改,进行删除失败,结果为:' . var_export($del_exist_index_re, true));
                }
                $bool_modify = false;
            }
        }

        if($bool_modify)
        {
            //$this->_check_and_del_input_image($this->add_params,$result_index_exist['data_info'][0],'message_queue');
            $check_input_params['data_info']['info']['out']['base_info']['nns_action'] = $params['base_info']['nns_action'] = 'modify';
            $check_input_params['data_info']['info']['out']['base_info']['nns_deleted'] = $params['base_info']['nns_deleted'] = '0';
            $check_input_params['data_info']['info']['out']['base_info']['nns_status'] = $params['base_info']['nns_status'] = '2';
            $result_execute = nl_vod_index::edit(\m_config::get_dc(), $params['base_info'],$result_index_exist['data_info'][0]['nns_id']);
            if ($result_execute['ret'] != NS_CDS_SUCCE)
            {
                return self::return_data(NS_CDS_FAIL, '修改分集信息失败,原因' . $result_execute['reason']);
            }

            $arr_queue['base_info']['nns_id'] = $result_index_exist['data_info'][0]['nns_id'];
        }
        else
        {
            //检查该分集号是否已经存在了
            $exist_index_num_arr = array(
                'nns_vod_id' => $params['base_info']['nns_vod_id'],
                'nns_import_source'=>$params['base_info']['nns_import_source'],
                'nns_cp_id'=>$params['base_info']['nns_cp_id'],
                'nns_index'=>$params['base_info']['nns_index'],
                'nns_deleted' => $params['base_info']['nns_deleted'],
            );
            $exist_index_num_re = \nl_vod_index::query_by_condition(\m_config::get_dc(), $exist_index_num_arr);

            if ($exist_index_num_re['ret'] != NS_CDS_SUCCE)
            {
                return self::return_data(NS_CDS_FAIL,'[注入id查询分集不存在]分集号存在,进行查询失败,结果为:' . var_export($exist_index_num_re, true));
            }

            //删除同一个主媒资已经存在的分集号的分集
            if (isset($exist_index_num_re['data_info']) && is_array($exist_index_num_re['data_info'])
                && !empty($exist_index_num_re['data_info']) && count($exist_index_num_re['data_info']) > 0)
            {
                if (isset($arr_cp_config['clip_replace_mode_one']) && $arr_cp_config['clip_replace_mode_one'] == 1)
                {
                    return self::return_data(NS_CDS_FAIL,"[注入id查询存在分集号,修正分集]cp配置[分集替换逻辑clip_replace_mode_one]不进行替换");
                }
                $this->write_message_info("[注入id查询不存在分集而存在相同分集号的分集],先将存在的分集号的分集先删除再添加)");
                $del_exist_index = array(
                    'base_info' => array(
                        'nns_import_id' => $exist_index_num_re['data_info'][0]['nns_import_id'],
                        'nns_import_source' => $exist_index_num_re['data_info'][0]['nns_import_source'],
                        'nns_cp_id' => $exist_index_num_re['data_info'][0]['nns_cp_id'],
                    ),
                    'ex_info' => array(
                        'nns_weight' => 999,//将注入优先级置为最高
                    ),
                );
                $del_exist_index_re = self::del($del_exist_index);
                $this->write_message_info('删除结果为:' . var_export($del_exist_index_re, true));
                if ($del_exist_index_re['ret'] != NS_CDS_SUCCE)
                {
                    return self::return_data(NS_CDS_FAIL,'[注入id查询不存在分集而存在相同分集号的分集]删除原有分集号的分集,进行删除失败,结果为:' . var_export($del_exist_index_re, true));
                }
            }

            $check_input_params['data_info']['info']['out']['base_info']['nns_action'] = $params['base_info']['nns_action'] = 'add';
            $check_input_params['data_info']['info']['out']['base_info']['nns_deleted'] = $params['base_info']['nns_deleted'] = '0';
            $check_input_params['data_info']['info']['out']['base_info']['nns_status'] = $params['base_info']['nns_status'] = '1';
            $result_execute = nl_vod_index::add(\m_config::get_dc(), $params['base_info']);
            if($result_execute['ret'] != NS_CDS_SUCCE)
            {
                return self::return_data(NS_CDS_FAIL,'添加分集信息失败,原因' . $result_execute['reason']);
            }
            $arr_queue['base_info']['nns_id'] = $params['base_info']['nns_id'];
        }

        if(!isset($params['ex_info']) || !is_array($params['ex_info']) || empty($params['ex_info']))
        {
            return $result_execute;
        }
        $result_ex_delete = nl_vod_index::del_ex_by_condition(\m_config::get_dc(), array(
            'nns_vod_index_id'=>$params['base_info']['nns_import_id'],
            'nns_cp_id'=>$params['base_info']['nns_cp_id']));
        if($result_ex_delete['ret'] != NS_CDS_SUCCE)
        {
            return self::return_data(NS_CDS_FAIL,'删除分集扩展信息失败,原因' . $result_ex_delete['reason']);
        }
        $arr_add_index_ex = null;
        foreach ($params['ex_info'] as $ex_key=>$ex_val)
        {
            if(strlen($ex_val) <1)
            {
                continue;
            }
            $arr_add_index_ex[] = array(
                'nns_vod_index_id'=>$params['base_info']['nns_import_id'],
                'nns_cp_id'=>$params['base_info']['nns_cp_id'],
                'nns_key'=>$ex_key,
                'nns_value'=>$ex_val,
            );
        }
        if(!empty($arr_add_index_ex) && is_array($arr_add_index_ex))
        {
            $result_ex_add = nl_vod_index::add_ex(m_config::$obj_dc, $arr_add_index_ex);
            if($result_ex_add['ret'] != NS_CDS_SUCCE)
            {
                return self::return_data(NS_CDS_FAIL,'添加分集扩展信息失败,原因' . $result_ex_add['reason']);
            }
        }

        //入中心注入指令
        $result = $this->push_to_center_import($arr_queue, 'index', $params['ex_info']['nns_weight'], $params['base_info']['nns_action']);
        $this->write_message_info($result);
        unset($arr_queue);
        return self::return_data(NS_CDS_SUCCE,'OK',$check_input_params['data_info']);
    }
    
    /**
     * 修改分集数据
     * @param array $params 修改数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function edit($params)
    {
        return $this->add($params);
    }
    
    /**
     * 删除分集数据
     * @param array $params 数据数组，不能为空
     * @return array|multitype array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function del($params)
    {
        //注入分集参数检查，模板参考
        $check_input_params = $this->check_input_params($params,'del');
        if($check_input_params['ret'] !=0)
        {
            return $check_input_params;
        }
        if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(1,'分集参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];
        //查询分集
        $result_index_exist = nl_vod_index::query_by_condition(m_config::$obj_dc, $params['base_info']);
        if($result_index_exist['ret'] !=0)
        {
            return self::return_data(1,'分集数据查询失败,原因' . $result_index_exist['reason']);
        }
        if (empty($result_index_exist['data_info']) || count($result_index_exist['data_info']) < 1
            || is_null($result_index_exist['data_info']) || !is_array($result_index_exist['data_info']))
        {
            return self::return_data(NS_CDS_SUCCE, addslashes("没有需要删除的数据，{$result_index_exist['reason']}"));
        }
        $arr_del = null;
        if(isset($result_index_exist['data_info']) && is_array($result_index_exist['data_info']) && !empty($result_index_exist['data_info']))
        {
            foreach ($result_index_exist['data_info'] as $data_info)
            {
                $arr_del[] = $data_info['nns_id'];
            }
        }
        if(empty($arr_del) || !is_array($arr_del))
        {
            return self::return_data(NS_CDS_SUCCE, addslashes("没有需要删除的数据，{$result_index_exist['reason']}"));
        }
        //队列数组
        $arr_queue = array(
            'base_info' => array(
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
            ),
        );
        //查询分集下对应的片源
        $obj_execute = new m_media_inout();
        $query_media = array(
            'base_info' => array(
                'nns_vod_index_id'=>$arr_del,
                'nns_deleted'=>0
            )
        );
        $result_media = $obj_execute->query($query_media);
        unset($query_media);

        if($result_media['ret'] !=0)
        {
            return $result_media;
        }
        $arr_media_del = null;
        if(isset($result_media['data_info']) && is_array($result_media['data_info']) && !empty($result_media['data_info']))
        {
            foreach ($result_media['data_info'] as $media_data_info)
            {
                $arr_media_del[] = $media_data_info['base_info']['nns_id'];
            }
        }
        if (isset($arr_media_del) && is_array($arr_media_del) && !empty($arr_media_del))
        {
            //删除分集对应的片源
            $result_execute = nl_vod_media_v2::edit(m_config::$obj_dc, array('nns_deleted'=>1),$arr_media_del);
            if($result_execute['ret'] !=0)
            {
                return $result_execute;
            }

            foreach ($arr_media_del as $del_val)
            {
                $arr_queue['base_info']['nns_id'] = $del_val;
                $this->push_to_center_import($arr_queue, 'media', $params['ex_info']['nns_weight'], 'destroy');
            }
        }

        $result_index_execute = nl_vod_index::edit(m_config::$obj_dc, array('nns_deleted' => 1), $arr_del);

        if($result_index_execute['ret'] != 0)
        {
            return $result_index_execute;
        }

        foreach ($arr_del as $del_val)
        {
            $arr_queue['base_info']['nns_id'] = $del_val;
            $this->push_to_center_import($arr_queue, 'index', $params['ex_info']['nns_weight'], 'destroy');
        }

        unset($arr_queue);
        return self::return_data(0,'ok',$check_input_params['data_info']);
    }
    
    /**
     * 查询分集数据
     * @param array $array_query 条件数据 不能为空
     * @param boolean $is_need_ext false 不需要查询扩展数据 | true 需要查询扩展数据
     * @param boolean $is_need_epg_import_id  false 不需要查询EPG扩展数据 | true 需要查询EPG扩展数据
     * @param boolean $is_need_cdn_import_id  false 不需要查询CDN扩展数据 | true 需要查询CDN扩展数据
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function query($array_query,$is_need_ext=false,$epg_model='',$cdn_model='')
    {
        $last_data = null;
        $result_vod = nl_vod_index::query_by_condition(m_config::$obj_dc, $array_query['base_info']);
        if($result_vod['ret'] !=0 || !isset($result_vod['data_info']) || !is_array($result_vod['data_info']) || empty($result_vod['data_info']))
        {
            return $result_vod;
        }
        foreach ($result_vod['data_info'] as $data_info)
        {
            $temp_epg_data = $temp_cdn_data = $temp_ex_data = null;
            if($is_need_ext)
            {
                $data_info['nns_asset_import_id'] = isset($data_info['nns_asset_import_id']) ? $data_info['nns_asset_import_id'] : $data_info['nns_import_id'];
                $result_vod_ex=nl_vod_index::query_ex_by_id(m_config::$obj_dc, $data_info['nns_asset_import_id'], $data_info['nns_cp_id']);
                if($result_vod_ex['ret'] !=0)
                {
                    return $result_vod_ex;
                }
                if(isset($result_vod_ex['data_info']) && is_array($result_vod_ex['data_info']) && !empty($result_vod_ex['data_info']))
                {
                    foreach ($result_vod_ex['data_info'] as $ex_data_info)
                    {
                        $temp_ex_data[$ex_data_info['nns_key']] = $ex_data_info['nns_value'];
                    }
                }
            }
            if (strlen($epg_model) >0)
            {
                $temp_epg_data = $this->get_asset_id_by_sp_id($epg_model,'index',$data_info,'epg');
                if($temp_epg_data['ret'] !=0)
                {
                    return $temp_epg_data;
                }
                $temp_epg_data = (isset($temp_epg_data['data_info']) && strlen($temp_epg_data['data_info']) >0) ? $temp_epg_data['data_info'] : '';
                #TODO
            }
            if (strlen($cdn_model) >0)
            {
                $temp_cdn_data = $this->get_asset_id_by_sp_id($cdn_model,'index',$data_info,'cdn');
                if($temp_cdn_data['ret'] !=0)
                {
                    return $temp_cdn_data;
                }
                $temp_cdn_data = (isset($temp_cdn_data['data_info']) && strlen($temp_cdn_data['data_info']) >0) ? $temp_cdn_data['data_info'] : '';
                #TODO
            }
            $last_data[]=array(
                'base_info'=>$data_info,
                'ex_info'=>$temp_ex_data,
                'epg_info'=>$temp_epg_data,
                'cdn_info'=>$temp_cdn_data,
            );
        }
        return self::return_data(0,'OK',$last_data);
    }
}