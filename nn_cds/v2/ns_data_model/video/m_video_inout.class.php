<?php
/**
 * 主媒资数据模板
 */
\ns_core\m_load::load("ns_data_model.m_data_model");
\ns_core\m_load::load_old("nn_logic/video/vod.class.php");
\ns_core\m_load::load_old("nn_logic/video/vod_index.class.php");
\ns_core\m_load::load_old("nn_logic/vod_media/vod_media.class.php");
\ns_core\m_load::load_old("nn_logic/depot/depot.class.php");
\ns_core\m_load::load_old("nn_logic/carrier/carrier.class.php");

class m_video_inout extends m_data_model
{
    /**
     * 删除参数检查模板
     * @var array
     */
    public $del_params = array(
        'base_info' => array(
            'nns_asset_import_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-128',
                'desc' => '主媒资注入id',
            ),
            'nns_cp_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-32',
                'desc' => 'CP_ID',
            ),
            'nns_import_source' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-32',
                'desc' => '注入来源',
            ),
        ),
        'hide_info' => array(
            'nns_deleted' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '是否删除 0 未删除  |  1删除',
            ),
        ),
        'ex_info' => array(
            'nns_weight' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '0-512',
                'desc' => '注入优先级',
            ),
        ),
    );

    /**
     * 修改参数检查模板
     * @var array
     */
    public $add_params = array(
        'base_info' => array(
            'nns_cp_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-32',
                'desc' => 'CP_ID',
            ),
            'nns_name' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-255',
                'desc' => '主媒资名称',
            ),
            'nns_view_type' => array(
                'rule' => 'tinyint',
                'default' => '1',
                'length' => '',
                'desc' => '影片类型',
            ),
            'nns_org_type' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '影片类型',
            ),
            'nns_tag' => array(
                'rule' => 'noempty',
                'default' => '26,',
                'length' => '0-128',
                'desc' => '终端标识默认26,',
            ),
            'nns_director' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-128',
                'desc' => '导演，多个导演/分割',
            ),
            'nns_actor' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '演员，多个演员/分割',
            ),
            'nns_show_time' => array(
                'rule' => 'date',
                'default' => '',
                'length' => '',
                'desc' => '发布日期',
            ),
            'nns_view_len' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '',
                'desc' => '影片播放时长（秒）',
            ),
            'nns_all_index' => array(
                'rule' => 'smallint',
                'default' => '',
                'length' => '',
                'desc' => '分集总数必填参数',
            ),
            'nns_new_index' => array(
                'rule' => 'smallint',
                'default' => '',
                'length' => '',
                'desc' => '最新分集号 0开始必填参数',
            ),
            'nns_area' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-64',
                'desc' => '上映地区，多个/分割',
            ),
            'nns_image0' => array(
                'rule' => 'image#video',
                'default' => '',
                'length' => '0-256',
                'desc' => '大图',
            ),
            'nns_image1' => array(
                'rule' => 'image#video',
                'default' => '',
                'length' => '0-256',
                'desc' => '中图',
            ),
            'nns_image2' => array(
                'rule' => 'image#video',
                'default' => '',
                'length' => '0-256',
                'desc' => '小图',
            ),
            'nns_image3' => array(
                'rule' => 'image#video',
                'default' => '',
                'length' => '0-256',
                'desc' => '扩展1',
            ),
            'nns_image4' => array(
                'rule' => 'image#video',
                'default' => '',
                'length' => '0-256',
                'desc' => '0-扩展2',
            ),
            'nns_image5' => array(
                'rule' => 'image#video',
                'default' => '',
                'length' => '0-256',
                'desc' => '扩展3',
            ),
            'nns_summary' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-4096',
                'desc' => '影片描述',
            ),
            'nns_remark' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-4096',
                'desc' => '影片标志',
            ),
            'nns_category_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-32',
                'desc' => '栏目ID',
            ),
            'nns_play_count' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '',
                'desc' => '播放次数',
            ),
            'nns_score_total' => array(
                'rule' => 'float',
                'default' => '0.00',
                'length' => '',
                'desc' => '评分',
            ),
            'nns_score_count' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '',
                'desc' => '总分数',
            ),
            'nns_point' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '评分',
            ),
            'nns_copyright_date' => array(
                'rule' => '',
                'default' => '',
                'length' => '',
                'desc' => '版权日期可为上映时间',
            ),
            'nns_asset_import_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-128',
                'desc' => '主媒资注入id',
            ),
            'nns_pinyin' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-128',
                'desc' => '名称拼音',
            ),
            'nns_pinyin_length' => array(
                'rule' => 'smallint',
                'default' => '0',
                'length' => '',
                'desc' => '拼音长度',
            ),
            'nns_alias_name' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => '别名',
            ),
            'nns_eng_name' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => '英文名称',
            ),
            'nns_language' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-16',
                'desc' => '语言',
            ),
            'nns_text_lang' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-16',
                'desc' => '字幕语言',
            ),
            'nns_producer' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-64',
                'desc' => '内容发布商',
            ),
            'nns_screenwriter' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-64',
                'desc' => '字幕作者',
            ),
            'nns_play_role' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-64',
                'desc' => '播放角色',
            ),
            'nns_copyright_begin_date' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-64',
                'desc' => '版权生效时间',
            ),
            'nns_copyright_range' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-64',
                'desc' => '版权过期时间',
            ),
            'nns_vod_part' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-4',
                'desc' => 'nns_vod_part',
            ),
            'nns_keyword' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-256',
                'desc' => '关键词',
            ),
            'nns_import_source' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-32',
                'desc' => '注入来源',
            ),
            'nns_kind' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '影片类型 如：战争,爱情',
            ),
            'nns_copyright' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-256',
                'desc' => '版权信息 如芒果TV',
            ),
            'nns_clarity' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-32',
                'desc' => 'nns_clarity',
            ),
            'nns_image_v' => array(
                'rule' => 'image#video',
                'default' => '',
                'length' => '0-256',
                'desc' => '横图',
            ),
            'nns_image_s' => array(
                'rule' => 'image#video',
                'default' => '',
                'length' => '0-256',
                'desc' => '方图',
            ),
            'nns_image_h' => array(
                'rule' => 'image#video',
                'default' => '',
                'length' => '0-256',
                'desc' => '竖图',
            ),
            'nns_conf_info' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '用于存放主媒资扩展配置信息',
            ),
            'nns_ext_url' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '上游方注入url 和其他信息  json存储',
            ),
            'nns_ishuaxu' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '媒资正片/花絮类型，0【正片】1【花絮】2【回看-正片】3【限免-正片】4【预告】',
            ),
            'nns_positiveoriginalid' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-36',
                'desc' => '正片媒资原始id',
            ),
        ),
        'ex_info' => array(
            'svc_item_id' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'svc_item_id',
            ),
            'month_clicks' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => '月点击量',
            ),
            'week_clicks' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => '周点击量',
            ),
            'base_id' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'base_id',
            ),
            'asset_path' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'asset_path',
            ),
            'ex_tag' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'asset_path',
            ),
            'full_spell' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'full_spell',
            ),
            'awards' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'awards'
            ),
            'year' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'year',
            ),
            'play_time' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'play_time',
            ),
            'channel' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'channel',
            ),
            'first_spell' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'first_spell',
            ),
            'directornew' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '芒果导演注入',
            ),
            'playernew' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '芒果演员注入',
            ),
            'adaptornew' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '芒果编剧注入',
            ),
            'third_asset_id' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '芒果二级id注入',
            ),
            'nns_weight' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '0-512',
                'desc' => '注入优先级',
            ),
            'online_identify' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '上线标识',
            ),
            'assets_definition' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '媒资清晰度',
            ),
        ),
        'hide_info' => array(
            'nns_id' => array(
                'rule' => 'noempty',
                'default' => 'GUID',
                'length' => '32',
                'desc' => '消息队列GUID',
            ),
            'nns_org_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '32',
                'desc' => '运营商ID',
            ),
            'nns_depot_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '32',
                'desc' => '点播、直播栏目GUID',
            ),
            'nns_state' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '主媒资状态',
            ),
            'nns_status' => array(
                'rule' => 'tinyint',
                'default' => '1',
                'length' => '',
                'desc' => '(add:1|modify:2|destroy:3|ok:0|execute:4)',
            ),
            'nns_action' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-12',
                'desc' => 'add|modify|destroy|ok|execute',
            ),
            'nns_deleted' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '是否删除 0 未删除  |  1删除',
            ),
            'nns_check' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '审核状态  0 审核通过 | 1 审核不通过',
            ),
        ),
    );

    /**
     * 修改参数检查模板
     * @var array
     */
    public $import_params = array(
        'base_info' => array(
            'nns_cp_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-32',
                'desc' => 'CP_ID',
            ),
            'nns_name' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-255',
                'desc' => '主媒资名称',
            ),
            'nns_view_type' => array(
                'rule' => 'tinyint',
                'default' => '1',
                'length' => '',
                'desc' => '影片类型',
            ),
            'nns_org_type' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '影片类型',
            ),
            'nns_tag' => array(
                'rule' => 'noempty',
                'default' => '26,',
                'length' => '0-128',
                'desc' => '终端标识默认26,',
            ),
            'nns_director' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-128',
                'desc' => '导演，多个导演/分割',
            ),
            'nns_actor' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '演员，多个演员/分割',
            ),
            'nns_show_time' => array(
                'rule' => 'date',
                'default' => '',
                'length' => '',
                'desc' => '发布日期',
            ),
            'nns_view_len' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '',
                'desc' => '影片播放时长（秒）',
            ),
            'nns_all_index' => array(
                'rule' => 'smallint',
                'default' => '',
                'length' => '',
                'desc' => '分集总数必填参数',
            ),
            'nns_new_index' => array(
                'rule' => 'smallint',
                'default' => '',
                'length' => '',
                'desc' => '最新分集号 0开始必填参数',
            ),
            'nns_area' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-64',
                'desc' => '上映地区，多个/分割',
            ),
            'nns_image0' => array(
                'rule' => 'image#video',
                'default' => '',
                'length' => '0-256',
                'desc' => '大图',
            ),
            'nns_image1' => array(
                'rule' => 'image#video',
                'default' => '',
                'length' => '0-256',
                'desc' => '中图',
            ),
            'nns_image2' => array(
                'rule' => 'image#video',
                'default' => '',
                'length' => '0-256',
                'desc' => '小图',
            ),
            'nns_image3' => array(
                'rule' => 'image#video',
                'default' => '',
                'length' => '0-256',
                'desc' => '扩展1',
            ),
            'nns_image4' => array(
                'rule' => 'image#video',
                'default' => '',
                'length' => '0-256',
                'desc' => '0-扩展2',
            ),
            'nns_image5' => array(
                'rule' => 'image#video',
                'default' => '',
                'length' => '0-256',
                'desc' => '扩展3',
            ),
            'nns_summary' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-4096',
                'desc' => '影片描述',
            ),
            'nns_remark' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-4096',
                'desc' => '影片标志',
            ),
            'nns_category_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-32',
                'desc' => '栏目ID',
            ),
            'nns_play_count' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '',
                'desc' => '播放次数',
            ),
            'nns_score_total' => array(
                'rule' => 'float',
                'default' => '0.00',
                'length' => '',
                'desc' => '评分',
            ),
            'nns_score_count' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '',
                'desc' => '总分数',
            ),
            'nns_point' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '评分',
            ),
            'nns_copyright_date' => array(
                'rule' => 'date',
                'default' => '',
                'length' => '',
                'desc' => '版权生效日期可为上映时间',
            ),
            'nns_asset_import_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-128',
                'desc' => '主媒资注入id',
            ),
            'nns_pinyin' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-128',
                'desc' => '名称拼音',
            ),
            'nns_pinyin_length' => array(
                'rule' => 'smallint',
                'default' => '0',
                'length' => '',
                'desc' => '拼音长度',
            ),
            'nns_alias_name' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => '别名',
            ),
            'nns_eng_name' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => '英文名称',
            ),
            'nns_language' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-16',
                'desc' => '语言',
            ),
            'nns_text_lang' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-16',
                'desc' => '字幕语言',
            ),
            'nns_producer' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-64',
                'desc' => '内容发布商',
            ),
            'nns_screenwriter' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-64',
                'desc' => '字幕作者',
            ),
            'nns_play_role' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-64',
                'desc' => '播放角色',
            ),
            'nns_copyright_begin_date' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-64',
                'desc' => '版权生效时间',
            ),
            'nns_copyright_range' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-64',
                'desc' => '版权过期时间',
            ),
            'nns_vod_part' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-4',
                'desc' => 'nns_vod_part',
            ),
            'nns_keyword' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-256',
                'desc' => '关键词',
            ),
            'nns_import_source' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-32',
                'desc' => '注入来源',
            ),
            'nns_kind' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '影片类型 如：战争,爱情',
            ),
            'nns_copyright' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-256',
                'desc' => '版权信息 如芒果TV',
            ),
            'nns_clarity' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-32',
                'desc' => 'nns_clarity',
            ),
            'nns_image_v' => array(
                'rule' => 'image#video',
                'default' => '',
                'length' => '0-256',
                'desc' => '横图',
            ),
            'nns_image_s' => array(
                'rule' => 'image#video',
                'default' => '',
                'length' => '0-256',
                'desc' => '方图',
            ),
            'nns_image_h' => array(
                'rule' => 'image#video',
                'default' => '',
                'length' => '0-256',
                'desc' => '竖图',
            ),
            'nns_conf_info' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '用于存放主媒资扩展配置信息',
            ),
            'nns_ext_url' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-512',
                'desc' => '上游方注入url 和其他信息  json存储',
            ),
            'nns_ishuaxu' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '媒资正片/花絮类型，0【正片】1【花絮】2【回看-正片】3【限免-正片】4【预告】',
            ),
            'nns_positiveoriginalid' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-36',
                'desc' => '正片媒资原始id',
            ),
        ),
        'ex_info' => array(
            'svc_item_id' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'svc_item_id',
            ),
            'month_clicks' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => '月点击量',
            ),
            'week_clicks' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => '周点击量',
            ),
            'base_id' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'base_id',
            ),
            'asset_path' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'asset_path',
            ),
            'ex_tag' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'asset_path',
            ),
            'full_spell' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'full_spell',
            ),
            'awards' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'awards'
            ),
            'year' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'year',
            ),
            'play_time' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'play_time',
            ),
            'channel' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'channel',
            ),
            'first_spell' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-255',
                'desc' => 'first_spell',
            ),
        ),
        'hide_info' => array(
            'nns_id' => array(
                'rule' => 'noempty',
                'default' => 'GUID',
                'length' => '32',
                'desc' => '消息队列GUID',
            ),
            'nns_org_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '32',
                'desc' => '运营商ID',
            ),
            'nns_depot_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '32',
                'desc' => '点播、直播栏目GUID',
            ),
            'nns_state' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '主媒资状态',
            ),
            'nns_status' => array(
                'rule' => 'tinyint',
                'default' => '1',
                'length' => '',
                'desc' => '(add:1|modify:2|destroy:3|ok:0|execute:4)',
            ),
            'nns_action' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-12',
                'desc' => 'add|modify|destroy|ok|execute',
            ),
            'nns_deleted' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '是否删除 0 未删除  |  1删除',
            ),
            'nns_check' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '审核状态  0 审核通过 | 1 审核不通过',
            ),
        ),
    );

    /**
     * 检查注入参数
     * @param array $in_params 注入参数
     * @param string $action 操作参数行为
     * @return array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function check_input_params($in_params, $action)
    {
        $notice_params = $last_params = array();
        $str_params = "{$action}_params";
        if (!isset($this->$str_params))
        {
            return self::return_data(1, '没有设置' . $str_params . '参数的判断模式');
        }
        if (!is_array($this->$str_params))
        {
            return self::return_data(1, '参数非数组');
        }
        return $this->_check_input_params($this->$str_params, $in_params);
    }

    /**
     * 主媒资模板导出
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function export()
    {
        return self::return_data(0, 'ok', array('del_params' => $this->del_params, 'import_params' => $this->import_params));
    }

    /**
     * 添加主媒资数据
     * @param array $params 添加数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function add($params)
    {
        $result_carrier = $this->query_carrier();
        if ($result_carrier['ret'] != 0)
        {
            return $result_carrier;
        }
        if (!isset($result_carrier['data_info']) || !is_array($result_carrier['data_info']) || empty($result_carrier['data_info']))
        {
            return self::return_data(1, "获取运营商信息为空");
        }
        $result_deport = $this->query_deport($result_carrier['data_info']['nns_id']);
        if ($result_deport['ret'] != 0)
        {
            return $result_deport;
        }
        if (!isset($result_deport['data_info']) || !is_array($result_deport['data_info']) || empty($result_deport['data_info']))
        {
            return self::return_data(1, "获取栏目信息为空");
        }
        $params['hide_info']['nns_org_id'] = $result_carrier['data_info']['nns_id'];
        $params['hide_info']['nns_depot_id'] = $result_deport['data_info']['nns_id'];
        //注入主媒资参数检查，模板参考
        $check_input_params = $this->check_input_params($params, 'add');

        if ($check_input_params['ret'] != 0)
        {
            return $check_input_params;
        }
        if (!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(1, '主媒资参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];
        #TODO CP配置逻辑
        $arr_vod_exist = array(
            'nns_asset_import_id' => $params['base_info']['nns_asset_import_id'],
            'nns_import_source' => $params['base_info']['nns_import_source'],
            'nns_cp_id' => $params['base_info']['nns_cp_id'],
            'nns_deleted' => $params['base_info']['nns_deleted'],
        );
        $result_vod_exist = nl_vod::query_by_condition(m_config::$obj_dc, $arr_vod_exist);
        if ($result_vod_exist['ret'] != 0)
        {
            return $result_vod_exist;
        }
        //队列数组
        $arr_queue = array(
            'base_info' => array(
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
            )
        );
        if (isset($result_vod_exist['data_info']) && is_array($result_vod_exist['data_info']) && !empty($result_vod_exist['data_info']))
        {
            $count_vod_data = count($result_vod_exist['data_info']);
            if ($count_vod_data > 1)
            {
                return self::return_data(NS_CDS_FAIL, "查询主媒资相同条件下数量小于{$count_vod_data}原有的注入有问题,{$result_vod_exist['reason']}");
            }
            //$this->_check_and_del_input_image($this->add_params, $result_vod_exist['data_info'][0]);
            $check_input_params['data_info']['info']['out']['base_info']['nns_action'] = $params['base_info']['nns_action'] = 'modify';
            $check_input_params['data_info']['info']['out']['base_info']['nns_deleted'] = $params['base_info']['nns_deleted'] = '0';
            $check_input_params['data_info']['info']['out']['base_info']['nns_status'] = $params['base_info']['nns_status'] = '2';
            $result_execute = nl_vod::edit(m_config::$obj_dc, $params['base_info'], $result_vod_exist['data_info'][0]['nns_id']);

            if ($result_execute['ret'] != 0)
            {
                return  self::return_data(NS_CDS_FAIL,'修改主媒资信息失败,原因' . $result_execute['reason']);
            }

            $arr_queue['base_info']['nns_id'] = $result_vod_exist['data_info'][0]['nns_id'];
        }
        else
        {
            $check_input_params['data_info']['info']['out']['base_info']['nns_action'] = $params['base_info']['nns_action'] = 'add';
            $check_input_params['data_info']['info']['out']['base_info']['nns_deleted'] = $params['base_info']['nns_deleted'] = '0';
            $check_input_params['data_info']['info']['out']['base_info']['nns_status'] = $params['base_info']['nns_status'] = '1';
            $result_execute = nl_vod::add(m_config::$obj_dc, $params['base_info']);
            if ($result_execute['ret'] != 0)
            {
                return  self::return_data(NS_CDS_FAIL,'添加主媒资信息失败,原因' . $result_execute['reason']);
            }

            $arr_queue['base_info']['nns_id'] = $params['base_info']['nns_id'];
        }
        if (!isset($params['ex_info']) || !is_array($params['ex_info']) || empty($params['ex_info']))
        {
            return $result_execute;
        }
        $result_ex_delete = nl_vod::del_ex_by_condition(m_config::$obj_dc, array('nns_vod_id' => $params['base_info']['nns_asset_import_id'], 'nns_cp_id' => $params['base_info']['nns_cp_id']));
        if ($result_ex_delete['ret'] != 0)
        {
            return  self::return_data(NS_CDS_FAIL,'删除主媒资扩展信息失败,原因' . $result_ex_delete['reason']);
        }
        $arr_add_vod_ex = null;
        foreach ($params['ex_info'] as $ex_key => $ex_val)
        {
            if (strlen($ex_val) < 1)
            {
                continue;
            }
            $arr_add_vod_ex[] = array(
                'nns_vod_id' => $params['base_info']['nns_asset_import_id'],
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
                'nns_key' => $ex_key,
                'nns_value' => $ex_val,
            );
        }
        if (is_array($arr_add_vod_ex) && !empty($arr_add_vod_ex))
        {
            $result_ex_add = nl_vod::add_ex(m_config::$obj_dc, $arr_add_vod_ex);
            if ($result_ex_add['ret'] != 0)
            {
                return  self::return_data(NS_CDS_FAIL, '添加主媒资扩展信息失败,原因' . $result_ex_add['reason']);
            }
        }

        //入中心注入指令
        $result = $this->push_to_center_import($arr_queue, 'video', $params['ex_info']['nns_weight'], $params['base_info']['nns_action']);
        $this->write_message_info($result);
        unset($arr_queue);
        return self::return_data($result['ret'], $result['reason'], $check_input_params['data_info']);
    }

    /**
     * 修改主媒资数据
     * @param array $params 修改数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function edit($params)
    {
        return $this->add($params);
    }

    /**
     * 删除主媒资数据
     * @param array $params 数据数组，不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function delete($params)
    {
        //注入主媒资参数检查，模板参考
        $check_input_params = $this->check_input_params($params, 'del');
        if ($check_input_params['ret'] != 0)
        {
            return $check_input_params;
        }
        if (!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(1, '主媒资参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];

        #TODO CP配置逻辑
        $arr_vod_exist = array(
            'nns_asset_import_id' => $params['base_info']['nns_asset_import_id'],
            'nns_import_source' => $params['base_info']['nns_import_source'],
            'nns_cp_id' => $params['base_info']['nns_cp_id'],
            'nns_deleted' => 0,
        );
        $result_vod_exist = nl_vod::query_by_condition(m_config::$obj_dc, $arr_vod_exist);
        if ($result_vod_exist['ret'] != 0)
        {
            return self::return_data(1, '主媒资数据查询失败,原因' . $result_vod_exist['reason']);
        }
        if (empty($result_vod_exist['data_info']) || count($result_vod_exist['data_info']) < 1
            || is_null($result_vod_exist['data_info']) || !is_array($result_vod_exist['data_info']))
        {
            return self::return_data(NS_CDS_SUCCE, addslashes("没有需要删除的数据，{$result_vod_exist['reason']}"));
        }
        $arr_del = null;
        if (isset($result_vod_exist['data_info']) && is_array($result_vod_exist['data_info']) && !empty($result_vod_exist['data_info']))
        {
            foreach ($result_vod_exist['data_info'] as $data_info)
            {
                $arr_del[] = $data_info['nns_id'];
            }
        }
        if (empty($arr_del) || !is_array($arr_del))
        {
            return self::return_data(NS_CDS_SUCCE, addslashes("没有需要删除的数据，{$result_vod_exist['reason']}"));
        }

        //查询主媒资对应分集
        $obj_index_execute = new m_index_inout();
        $query_vod = array(
            'base_info' => array(
                'nns_vod_id' => $arr_del,
                'nns_deleted' => 0,
            )
        );
        $result_index = $obj_index_execute->query($query_vod);
        unset($query_vod);

        if($result_index['ret'] != 0)
        {
            return $result_index;
        }
        $arr_index_del = null;
        if(isset($result_index['data_info']) && is_array($result_index['data_info']) && !empty($result_index['data_info']))
        {
            foreach ($result_index['data_info'] as $index_data_info)
            {
                $arr_index_del[] = $index_data_info['base_info']['nns_id'];
            }
        }

        //队列数组
        $arr_queue = array(
            'base_info' => array(
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
            ),
        );

        //对应分集下存在片源则先修改片源
        if (!empty($arr_index_del) && isset($arr_index_del) && is_array($arr_index_del))
        {
            //先修改分集对应的片源
            $obj_execute = new m_media_inout();
            $query_media = array(
                'base_info' => array(
                    'nns_vod_index_id' => $arr_index_del,
                    'nns_deleted' => 0,
                )
            );
            $result_media = $obj_execute->query($query_media);
            unset($query_media);
            if($result_media['ret'] != 0)
            {
                return $result_media;
            }
            $arr_media_del = null;
            if(isset($result_media['data_info']) && is_array($result_media['data_info']) && !empty($result_media['data_info']))
            {
                foreach ($result_media['data_info'] as $media_data_info)
                {
                    $arr_media_del[] = $media_data_info['base_info']['nns_id'];
                }
            }

            //存在片源则删除
            if (isset($arr_media_del) && is_array($arr_media_del) && !empty($arr_media_del))
            {
                $result_media_execute = nl_vod_media_v2::edit(m_config::$obj_dc, array('nns_deleted'=>1), $arr_media_del);
                if($result_media_execute['ret'] != 0)
                {
                    return $result_media_execute;
                }

                //片源操作入队列
                foreach ($arr_media_del as $del_val)
                {
                    $arr_queue['base_info']['nns_id'] = $del_val;
                    $re = $this->push_to_center_import($arr_queue, 'media', $params['ex_info']['nns_weight'], 'destroy');
                    $this->write_message_info($re);
                }
            }

            //再修改分集
            $result_index_execute = nl_vod_index::edit(m_config::$obj_dc, array('nns_deleted' => 1), $arr_index_del);
            if($result_index_execute['ret'] != 0)
            {
                return $result_index_execute;
            }

            //分集操作入中心注入指令
            foreach ($arr_index_del as $del_val)
            {
                $arr_queue['base_info']['nns_id'] = $del_val;
                $re = $this->push_to_center_import($arr_queue, 'index', $params['ex_info']['nns_weight'], 'destroy');
                $this->write_message_info($re);
            }

        }
        //修改主媒资
        $result_execute = nl_vod::edit(m_config::$obj_dc, array('nns_deleted' => 1), $arr_del);

        //入中心同步指令
        foreach ($arr_del as $del_val)
        {
            $arr_queue['base_info']['nns_id'] = $del_val;
            $re = $this->push_to_center_import($arr_queue, 'video', $params['ex_info']['nns_weight'], 'destroy');
            $this->write_message_info($re);
        }

        return $result_execute;
    }

    
    /**
     * 
     * @param array $array_query 条件数据 不能为空
     * @param string $is_need_ext  false 不需要查询扩展数据 | true 需要查询扩展数据
     * @param string $epg_model 为空 不需要查询EPG扩展数据 | 不为空 需要查询EPG扩展数据
     * @param string $cdn_model 为空 不需要查询CDN扩展数据 | 不为空 需要查询CDN扩展数据
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function query($array_query, $is_need_ext = false, $epg_model = '', $cdn_model = '')
    {

        $last_data = null;
        $result_vod = nl_vod::query_by_condition(m_config::$obj_dc, $array_query['base_info']);

        if ($result_vod['ret'] != 0 || !isset($result_vod['data_info']) || !is_array($result_vod['data_info']) || empty($result_vod['data_info']))
        {
            return $result_vod;
        }
        foreach ($result_vod['data_info'] as $data_info)
        {
            $temp_epg_data = $temp_cdn_data = $temp_ex_data = null;
            if ($is_need_ext)
            {
                $result_vod_ex = nl_vod::query_ex_by_id(m_config::$obj_dc, $data_info['nns_asset_import_id'], $data_info['nns_cp_id']);
                if ($result_vod_ex['ret'] != 0)
                {
                    return $result_vod_ex;
                }
                if (isset($result_vod_ex['data_info']) && is_array($result_vod_ex['data_info']) && !empty($result_vod_ex['data_info']))
                {
                    foreach ($result_vod_ex['data_info'] as $ex_data_info)
                    {
                        $temp_ex_data[$ex_data_info['nns_key']] = $ex_data_info['nns_value'];
                    }
                }
            }
            if (strlen($epg_model) >0)
            {
                $temp_epg_data = $this->get_asset_id_by_sp_id($epg_model,'video',$data_info,'epg');
                if($temp_epg_data['ret'] !=0)
                {
                    return $temp_epg_data;
                }
                $temp_epg_data = (isset($temp_epg_data['data_info']) && strlen($temp_epg_data['data_info']) >0) ? $temp_epg_data['data_info'] : '';
                #TODO
            }
            if (strlen($cdn_model) >0)
            {
                $temp_cdn_data = $this->get_asset_id_by_sp_id($cdn_model,'video',$data_info,'cdn');
                if($temp_cdn_data['ret'] !=0)
                {
                    return $temp_cdn_data;
                }
                $temp_cdn_data = (isset($temp_cdn_data['data_info']) && strlen($temp_cdn_data['data_info']) >0) ? $temp_cdn_data['data_info'] : '';
                #TODO
            }
            $last_data[] = array(
                'base_info' => $data_info,
                'ex_info' => $temp_ex_data,
                'epg_info' => $temp_epg_data,
                'cdn_info' => $temp_cdn_data,
            );
        }
        return self::return_data(0, 'OK', $last_data);
    }

    /**
     * 查询运营商 信息
     */
    private function query_carrier()
    {
        $result_carrier = nl_carrier::query_all(m_config::$obj_dc);
        if ($result_carrier['ret'] != 0)
        {
            return $result_carrier;
        }
        $result_carrier = (isset($result_carrier['data_info'][0]) && is_array($result_carrier['data_info'][0]) && !empty($result_carrier['data_info'][0])) ? $result_carrier['data_info'][0] : null;
        if (empty($result_carrier))
        {
            return self::return_data(1, '查询运营商数据 不存在');
        }
        return self::return_data(0, $result_carrier['reason'], $result_carrier);
    }

    /**
     * 查询运营商 信息
     */
    private function query_deport($carrier_id)
    {
        $result_depot = nl_depot::query_by_condition(self::$obj_dc, array('nns_org_id' => $carrier_id, 'nns_type' => 0));
        if ($result_depot['ret'] != 0)
        {
            return $result_depot;
        }
        $result_depot = (isset($result_depot['data_info'][0]) && is_array($result_depot['data_info'][0]) && !empty($result_depot['data_info'][0])) ? $result_depot['data_info'][0] : null;
        if (empty($result_depot))
        {
            return self::return_data(1, '查询栏目数据 不存在');
        }
        return self::return_data(0, $result_depot['reason'], $result_depot);
    }
}