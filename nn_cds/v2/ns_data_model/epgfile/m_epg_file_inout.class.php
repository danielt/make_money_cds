<?php
/**
* Created by <xinxin.deng>.
* Author: xinxin.deng
* Date: 2018/11/8 11:26
*/
\ns_core\m_load::load("ns_data_model.m_data_model");
\ns_core\m_load::load_old("nn_logic/epg_file/epg_file_set.class.php");
\ns_core\m_load::load_old("nn_logic/epg_file/epg_file.class.php");

class m_epg_file_inout extends m_data_model
{
    /**
     * 删除参数检查模板
     * @var array
     */
    public $del_params = array(
        'base_info' => array(
            'nns_cp_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-32',
                'desc' => 'CP_ID',
            ),
            'nns_import_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-255',
                'desc' => '注入id',
            ),
        ),
        'hide_info' => array(
            'nns_deleted' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '是否删除 0 未删除  |  1删除',
            ),
        ),
        'ex_info' => array(
            'nns_weight' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '0-512',
                'desc' => '注入优先级',
            ),
        ),
    );
    /**
     * 修改参数检查模板
     * @var array
     */
    public $add_params = array(
        'base_info' => array(
            'nns_cp_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-32',
                'desc' => 'CP_ID',
            ),
            'nns_import_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-255',
                'desc' => '注入id',
            ),
            'nns_epg_file_set_import_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-255',
                'desc' => '注入id',
            ),
            'nns_dest_path' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-32',
                'desc' => '厂商在EPG Sever 上存储该文件的相对路径。如果是相对路径的根路径，则用空表示。如：epg/bestv/',
            ),
            'nns_dest_file' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-32',
                'desc' => '厂商在EPG Sever 上存储该文件的目标文件名，如：index.php',
            ),
            'nns_md5' => array(
                'rule' => '',
                'default' => '',
                'length' => '0-256',
                'desc' => '文件md5',
            ),
            'nns_url' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '0-256',
                'desc' => '文件地址',
            ),
        ),
        'hide_info' => array(
            'nns_id' => array(
                'rule' => 'noempty',
                'default' => 'GUID',
                'length' => '32',
                'desc' => '消息队列GUID',
            ),
            'nns_deleted' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '是否删除 0 未删除  |  1删除',
            ),
            'nns_epg_file_set_id'  => array(
                'rule'      => 'noempty',
                'default'   => 'parent|epg_file_set_action|base_info+unset+nns_epg_file_set_import_id+nns_import_id#base_info+set+nns_cp_id+nns_cp_id#base_info+set+nns_deleted+nns_deleted|base_info+nns_id',
                'length'    => '32',
                'desc'      => 'epgfileset ID',
            ),
        ),
    );

    /**
     * 检查注入参数
     * @param array $in_params 注入参数
     * @param string $action 操作参数行为
     * @return array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function check_input_params($in_params, $action)
    {
        $notice_params = $last_params = array();
        $str_params = "{$action}_params";
        if (!isset($this->$str_params))
        {
            return self::return_data(NS_CDS_FAIL, '没有设置' . $str_params . '参数的判断模式');
        }
        if (!is_array($this->$str_params))
        {
            return self::return_data(NS_CDS_FAIL, '参数非数组');
        }

        return $this->_check_input_params($this->$str_params, $in_params);
    }

    /**
     * 模板导出
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function export()
    {
        return self::return_data(0, 'ok', array(
            'del_params' => $this->del_params,
            'import_params' => $this->import_params
        ));
    }

    /**
     * 添加主媒资数据
     * @param array $params 添加数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function add($params)
    {
        //注入epgfile参数检查，模板参考
        $check_input_params = $this->check_input_params($params, 'add');
        if ($check_input_params['ret'] != NS_CDS_SUCCE)
        {
            return $check_input_params;
        }
        if (!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(NS_CDS_FAIL, 'EPGFile参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];
        //判断父级是否已经存在
        if (!isset($params['base_info']['nns_epg_file_set_id']) || empty($params['base_info']['nns_epg_file_set_id']) || strlen($params['base_info']['nns_epg_file_set_id']) < 1)
        {
            return self::return_data(NS_CDS_FAIL, 'EPGFile查询父级数据不存在或者已经删除');
        }

        //查询是否已经存在
        $arr_epg_fil_exist = array(
            'nns_import_id' => $params['base_info']['nns_import_id'],
            'nns_cp_id' => $params['base_info']['nns_cp_id'],
            'nns_deleted' => $params['base_info']['nns_deleted'],
        );
        $result_epg_file_exist = \nl_epg_file::query_by_condition(\m_config::get_dc(), $arr_epg_fil_exist);
        if ($result_epg_file_exist['ret'] != NS_CDS_SUCCE)
        {
            return $result_epg_file_exist;
        }

        //队列数组
        $arr_queue = array(
            'base_info' => array(
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
            )
        );
        if (isset($result_epg_file_exist['data_info']) && is_array($result_epg_file_exist['data_info']) && !empty($result_epg_file_exist['data_info']))
        {//修改
            $check_input_params['data_info']['info']['out']['base_info']['nns_action'] = $params['base_info']['nns_action'] = 'modify';
            $check_input_params['data_info']['info']['out']['base_info']['nns_deleted'] = $params['base_info']['nns_deleted'] = '0';
            $check_input_params['data_info']['info']['out']['base_info']['nns_status'] = $params['base_info']['nns_status'] = '2';
            $result_execute = \nl_epg_file::edit(\m_config::get_dc(), $params['base_info'], $result_epg_file_exist['data_info'][0]['nns_id']);
            if ($result_execute['ret'] != 0)
            {
                return self::return_data(NS_CDS_FAIL, '修改epgfile信息失败,原因' . $result_execute['reason']);
            }
            $arr_queue['base_info']['nns_id'] = $result_epg_file_exist['data_info'][0]['nns_id'];
        }
        else
        {//添加
            $check_input_params['data_info']['info']['out']['base_info']['nns_action'] = $params['base_info']['nns_action'] = 'add';
            $check_input_params['data_info']['info']['out']['base_info']['nns_deleted'] = $params['base_info']['nns_deleted'] = '0';
            $check_input_params['data_info']['info']['out']['base_info']['nns_status'] = $params['base_info']['nns_status'] = '1';
            $result_execute = \nl_epg_file::add(\m_config::get_dc(), $params['base_info']);
            if ($result_execute['ret'] != NS_CDS_SUCCE)
            {
                return self::return_data(NS_CDS_FAIL, '添加epgfile信息失败,原因' . $result_execute['reason']);
            }
            $arr_queue['base_info']['nns_id'] = $params['base_info']['nns_id'];
        }
        //入中心注入指令
        $result = $this->push_to_center_import($arr_queue, 'epg_file', $params['ex_info']['nns_weight'], $params['base_info']['nns_action']);
        $this->write_message_info($result);
        unset($arr_queue);

        return self::return_data($result['ret'], $result['reason'], $check_input_params['data_info']);
    }

    /**
     * 修改主媒资数据
     * @param array $params 修改数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function edit($params)
    {
        return $this->add($params);
    }

    /**
     * 删除主媒资数据
     * @param array $params 数据数组，不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function delete($params)
    {
        //注入主媒资参数检查，模板参考
        $check_input_params = $this->check_input_params($params, 'del');
        if ($check_input_params['ret'] != NS_CDS_SUCCE)
        {
            return $check_input_params;
        }
        if (!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(NS_CDS_FAIL, 'EPGfile参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];
        #TODO CP配置逻辑
        $arr_epg_file_exist = array(
            'nns_import_id' => $params['base_info']['nns_import_id'],
            'nns_cp_id' => $params['base_info']['nns_cp_id'],
            'nns_deleted' => 0,
        );
        $result_epg_file_exist = \nl_epg_file::query_by_condition(\m_config::get_dc(), $arr_epg_file_exist);
        if ($result_epg_file_exist['ret'] != NS_CDS_SUCCE)
        {
            return self::return_data(NS_CDS_FAIL, 'EPGfile数据查询失败,原因' . $result_epg_file_exist['reason']);
        }
        if (empty($result_epg_file_exist['data_info']) || count($result_epg_file_exist['data_info']) < 1 || is_null($result_epg_file_exist['data_info']) || !is_array($result_epg_file_exist['data_info']))
        {
            return self::return_data(NS_CDS_SUCCE, addslashes("没有需要删除的数据，{$result_epg_file_exist['reason']}"));
        }
        $arr_del = null;
        if (isset($result_epg_file_exist['data_info']) && is_array($result_epg_file_exist['data_info']) && !empty($result_epg_file_exist['data_info']))
        {
            foreach ($result_epg_file_exist['data_info'] as $data_info)
            {
                $arr_del[] = $data_info['nns_id'];
            }
        }
        if (empty($arr_del) || !is_array($arr_del))
        {
            return self::return_data(NS_CDS_SUCCE, addslashes("没有需要删除的数据，{$result_epg_file_exist['reason']}"));
        }
        //队列数组
        $arr_queue = array(
            'base_info' => array(
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
            ),
        );

        //EPGFile
        $result_execute = \nl_epg_file::edit(\m_config::get_dc(), array('nns_deleted' => 1), $arr_del);
        //入中心同步指令
        foreach ($arr_del as $del_val)
        {
            $arr_queue['base_info']['nns_id'] = $del_val;
            $re = $this->push_to_center_import($arr_queue, 'epg_file', $params['ex_info']['nns_weight'], 'destroy');
            $this->write_message_info($re);
        }

        return $result_execute;
    }

    /**
     *
     * @param array $array_query 条件数据 不能为空
     * @param boolean $is_need_ext false 不需要查询扩展数据 | true 需要查询扩展数据
     * @param string $epg_model 为空 不需要查询EPG扩展数据 | 不为空 需要查询EPG扩展数据
     * @param string $cdn_model 为空 不需要查询CDN扩展数据 | 不为空 需要查询CDN扩展数据
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function query($array_query, $is_need_ext = false, $epg_model = '', $cdn_model = '')
    {
        $last_data = null;
        $result_epg_file = nl_epg_file::query_by_condition(\m_config::get_dc(), $array_query['base_info']);
        if ($result_epg_file['ret'] != NS_CDS_SUCCE || !isset($result_epg_file['data_info'])
            || !is_array($result_epg_file['data_info']) || empty($result_epg_file['data_info']))
        {
            return $result_epg_file;
        }
        foreach ($result_epg_file['data_info'] as $data_info)
        {
            $temp_epg_data = $temp_cdn_data = $temp_ex_data = null;

            $last_data[] = array(
                'base_info' => $data_info,
                'ex_info' => $temp_ex_data,
                'epg_info' => $temp_epg_data,
                'cdn_info' => $temp_cdn_data,
            );
        }

        return self::return_data(0, 'OK', $last_data);
    }

}