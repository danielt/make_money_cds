<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/11/8 11:25
 */
\ns_core\m_load::load("ns_data_model.m_data_model");
\ns_core\m_load::load_old("nn_logic/epg_file/epg_file_set.class.php");
\ns_core\m_load::load_old("nn_logic/epg_file/epg_file.class.php");

class m_epg_file_set_inout extends m_data_model
{
    /**
     * 删除参数检查模板
     * @var array
     */
    public $del_params = array(
        'base_info' => array(
            'nns_cp_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-32',
                'desc' => 'CP_ID',
            ),
            'nns_import_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-255',
                'desc' => '注入id',
            ),
            'nns_group' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '0-128',
                'desc' => '终端标识默认26,',
            ),
        ),
        'hide_info' => array(
            'nns_deleted' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '是否删除 0 未删除  |  1删除',
            ),
        ),
        'ex_info' => array(
            'nns_weight' => array(
                'rule' => 'int',
                'default' => '0',
                'length' => '0-512',
                'desc' => '注入优先级',
            ),
        ),
    );

    /**
     * 修改参数检查模板
     * @var array
     */
    public $add_params = array(
        'base_info' => array(
            'nns_cp_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-32',
                'desc' => 'CP_ID',
            ),
            'nns_import_id' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-255',
                'desc' => '注入id',
            ),
            'nns_system_file' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '该文件集是否为系统层文件,0 否,1 是',
            ),
            'nns_tar' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '文件是否为tar包,0 否,1 是',
            ),
            'nns_group' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '0-128',
                'desc' => '分组名',
            ),
            'nns_epg_addr' => array(
                'rule' => 'noempty',
                'default' => '',
                'length' => '1-256',
                'desc' => '模板目标分发地址',
            ),
            'nns_show_time' => array(
                'rule' => 'datetime',
                'default' => '0000-00-00 00:00:00',
                'length' => '',
                'desc' => '文件生效时间',
            ),
        ),
        'hide_info' => array(
            'nns_id' => array(
                'rule' => 'noempty',
                'default' => 'GUID',
                'length' => '32',
                'desc' => '消息队列GUID',
            ),
            'nns_deleted' => array(
                'rule' => 'tinyint',
                'default' => '0',
                'length' => '',
                'desc' => '是否删除 0 未删除  |  1删除',
            ),
        ),
    );

    /**
     * 检查注入参数
     * @param array $in_params 注入参数
     * @param string $action 操作参数行为
     * @return array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function check_input_params($in_params, $action)
    {
        $notice_params = $last_params = array();
        $str_params = "{$action}_params";
        if (!isset($this->$str_params))
        {
            return self::return_data(NS_CDS_FAIL, '没有设置' . $str_params . '参数的判断模式');
        }
        if (!is_array($this->$str_params))
        {
            return self::return_data(NS_CDS_FAIL, '参数非数组');
        }
        return $this->_check_input_params($this->$str_params, $in_params);
    }

    /**
     * 主媒资模板导出
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function export()
    {
        return self::return_data(NS_CDS_SUCCE, 'ok', array('del_params' => $this->del_params, 'import_params' => $this->import_params));
    }

    /**
     * 添加主媒资数据
     * @param array $params 添加数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function add($params)
    {
        //注入主媒资参数检查，模板参考
        $check_input_params = $this->check_input_params($params, 'add');

        if ($check_input_params['ret'] != NS_CDS_SUCCE)
        {
            return $check_input_params;
        }
        if (!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out'])
            || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(NS_CDS_FAIL, 'EPGFileSet参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];

        //查询是否已经存在
        $arr_epg_file_set_exist = array(
            'nns_import_id' => $params['base_info']['nns_import_id'],
            'nns_cp_id' => $params['base_info']['nns_cp_id'],
            'nns_group' => $params['base_info']['nns_group'],
            'nns_deleted' => $params['base_info']['nns_deleted'],
        );
        $result_epg_file_set_exist = \nl_epg_file_set::query_by_condition(\m_config::get_dc(), $arr_epg_file_set_exist);
        if ($result_epg_file_set_exist['ret'] != NS_CDS_SUCCE)
        {
            return $result_epg_file_set_exist;
        }
        //队列数组
        $arr_queue = array(
            'base_info' => array(
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
            )
        );
        if (isset($result_epg_file_set_exist['data_info']) && is_array($result_epg_file_set_exist['data_info'])
            && !empty($result_epg_file_set_exist['data_info']))
        {//修改
            $count_epg_file_set_data = count($result_epg_file_set_exist['data_info']);
            if ($count_epg_file_set_data > 1)
            {
                return self::return_data(NS_CDS_FAIL, "查询epgfilese相同条件下数量{$count_epg_file_set_data}大于1原有的注入有问题,{$result_epg_file_set_exist['reason']}");
            }
            //$this->_check_and_del_input_image($this->add_params, $result_vod_exist['data_info'][0]);
            $check_input_params['data_info']['info']['out']['base_info']['nns_action'] = $params['base_info']['nns_action'] = 'modify';
            $check_input_params['data_info']['info']['out']['base_info']['nns_deleted'] = $params['base_info']['nns_deleted'] = '0';
            $check_input_params['data_info']['info']['out']['base_info']['nns_status'] = $params['base_info']['nns_status'] = '2';
            $result_execute = \nl_epg_file_set::edit(\m_config::get_dc(), $params['base_info'], $result_epg_file_set_exist['data_info'][0]['nns_id']);

            if ($result_execute['ret'] != NS_CDS_SUCCE)
            {
                return self::return_data(NS_CDS_FAIL, '修改epgfileset信息失败,原因' . $result_execute['reason']);
            }

            $arr_queue['base_info']['nns_id'] = $result_epg_file_set_exist['data_info'][0]['nns_id'];
        }
        else
        {//添加
            $check_input_params['data_info']['info']['out']['base_info']['nns_action'] = $params['base_info']['nns_action'] = 'add';
            $check_input_params['data_info']['info']['out']['base_info']['nns_deleted'] = $params['base_info']['nns_deleted'] = '0';
            $check_input_params['data_info']['info']['out']['base_info']['nns_status'] = $params['base_info']['nns_status'] = '1';
            $result_execute = \nl_epg_file_set::add(\m_config::get_dc(), $params['base_info']);
            if ($result_execute['ret'] != NS_CDS_SUCCE)
            {
                return self::return_data(NS_CDS_FAIL, '添加epgfileset信息失败,原因' . $result_execute['reason']);
            }

            $arr_queue['base_info']['nns_id'] = $params['base_info']['nns_id'];
        }

        //入中心注入指令
        //$result = $this->push_to_center_import($arr_queue, 'epgfileset', $params['ex_info']['nns_weight'], $params['base_info']['nns_action']);
        $this->write_message_info($result_execute);
        unset($arr_queue);
        return self::return_data($result_execute['ret'], $result_execute['reason'], $check_input_params['data_info']);
    }

    /**
     * 修改主媒资数据
     * @param array $params 修改数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function edit($params)
    {
        return $this->add($params);
    }

    /**
     * 删除主媒资数据
     * @param array $params 数据数组，不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function delete($params)
    {
        //注入主媒资参数检查，模板参考
        $check_input_params = $this->check_input_params($params, 'del');
        if ($check_input_params['ret'] != NS_CDS_SUCCE)
        {
            return $check_input_params;
        }
        if (!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(NS_CDS_FAIL, 'epgfileset参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];

        #TODO CP配置逻辑
        $arr_epg_file_set_exist = array(
            'nns_import_id' => $params['base_info']['nns_asset_import_id'],
            'nns_cp_id' => $params['base_info']['nns_cp_id'],
            'nns_deleted' => 0,
        );
        $result_epg_file_set_exist = \nl_epg_file_set::query_by_condition(\m_config::get_dc(), $arr_epg_file_set_exist);
        if ($result_epg_file_set_exist['ret'] != NS_CDS_SUCCE)
        {
            return self::return_data(NS_CDS_FAIL, 'epgfileset数据查询失败,原因' . $result_epg_file_set_exist['reason']);
        }
        if (empty($result_epg_file_set_exist['data_info']) || count($result_epg_file_set_exist['data_info']) < 1
            || is_null($result_epg_file_set_exist['data_info']) || !is_array($result_epg_file_set_exist['data_info']))
        {
            return self::return_data(NS_CDS_SUCCE, addslashes("没有需要删除的数据，{$result_epg_file_set_exist['reason']}"));
        }
        $arr_del = null;
        if (isset($result_epg_file_set_exist['data_info']) && is_array($result_epg_file_set_exist['data_info']) && !empty($result_epg_file_set_exist['data_info']))
        {
            foreach ($result_epg_file_set_exist['data_info'] as $data_info)
            {
                $arr_del[] = $data_info['nns_id'];
            }
        }
        if (empty($arr_del) || !is_array($arr_del))
        {
            return self::return_data(NS_CDS_SUCCE, addslashes("没有需要删除的数据，{$result_epg_file_set_exist['reason']}"));
        }

        //查询EPGFfileset对应EPGFile
        $obj_epg_file_execute = new m_epg_file_inout();
        $query_epg_file = array(
            'base_info' => array(
                'nns_epg_file_set_id' => $arr_del,
                'nns_deleted' => 0,
            )
        );
        $result_epg_file = $obj_epg_file_execute->query($query_epg_file);
        unset($query_epg_file);

        if($result_epg_file['ret'] != NS_CDS_SUCCE)
        {
            return $result_epg_file;
        }
        $arr_epg_file_del = null;
        if(isset($result_epg_file['data_info']) && is_array($result_epg_file['data_info']) && !empty($result_epg_file['data_info']))
        {
            foreach ($result_epg_file['data_info'] as $epg_file_data_info)
            {
                $arr_epg_file_del[] = $epg_file_data_info['base_info']['nns_id'];
            }
        }

        //队列数组
        $arr_queue = array(
            'base_info' => array(
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
            ),
        );

        //先删除下面对应epgfile
        if (!empty($arr_epg_file_del) && isset($arr_epg_file_del) && is_array($arr_epg_file_del))
        {
            $result_epg_file_execute = \nl_epg_file::edit(\m_config::get_dc(), array('nns_deleted' => 1), $arr_epg_file_del);
            if($result_epg_file_execute['ret'] != NS_CDS_SUCCE)
            {
                return $result_epg_file_execute;
            }

            //分集操作入中心注入指令
            foreach ($arr_epg_file_del as $del_val)
            {
                $arr_queue['base_info']['nns_id'] = $del_val;
                $re = $this->push_to_center_import($arr_queue, 'epgfile', $params['ex_info']['nns_weight'], 'destroy');
                $this->write_message_info($re);
            }

        }
        //修改epgfileset
        $result_execute = \nl_epg_file_set::edit(\m_config::get_dc(), array('nns_deleted' => 1), $arr_del);

        //入中心同步指令
        //foreach ($arr_del as $del_val)
        //{
        //    $arr_queue['base_info']['nns_id'] = $del_val;
        //    $re = $this->push_to_center_import($arr_queue, 'epgfileset', $params['ex_info']['nns_weight'], 'destroy');
        //    $this->write_message_info($re);
        //}
        $this->write_message_info($result_execute);
        return $result_execute;
    }


    /**
     *
     * @param array $array_query 条件数据 不能为空
     * @param boolean $is_need_ext  false 不需要查询扩展数据 | true 需要查询扩展数据
     * @param string $epg_model 为空 不需要查询EPG扩展数据 | 不为空 需要查询EPG扩展数据
     * @param string $cdn_model 为空 不需要查询CDN扩展数据 | 不为空 需要查询CDN扩展数据
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function query($array_query, $is_need_ext = false, $epg_model = '', $cdn_model = '')
    {
        $last_data = null;
        $result_epg_file_set = \nl_epg_file_set::query_by_condition(\m_config::get_dc(), $array_query['base_info']);

        if ($result_epg_file_set['ret'] != NS_CDS_SUCCE || !isset($result_epg_file_set['data_info'])
            || !is_array($result_epg_file_set['data_info']) || empty($result_epg_file_set['data_info']))
        {
            return $result_epg_file_set;
        }
        foreach ($result_epg_file_set['data_info'] as $data_info)
        {
            $temp_epg_data = $temp_cdn_data = $temp_ex_data = null;
            $last_data[] = array(
                'base_info' => $data_info,
                'ex_info' => $temp_ex_data,
                'epg_info' => $temp_epg_data,
                'cdn_info' => $temp_cdn_data,
            );
        }
        return self::return_data(NS_CDS_SUCCE, 'OK', $last_data);
    }

}