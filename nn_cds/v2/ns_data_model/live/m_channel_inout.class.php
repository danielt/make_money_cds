<?php
/**
 * @description:直播频道数据模板
 * @author:xinxin.deng
 * @date: 2017/12/7 11:40
 */

\ns_core\m_load::load("ns_data_model.m_data_model");
\ns_core\m_load::load_old("nn_logic/live/live.class.php");
\ns_core\m_load::load_old("nn_logic/depot/depot.class.php");
\ns_core\m_load::load_old("nn_logic/carrier/carrier.class.php");
class m_channel_inout  extends m_data_model
{
    /**
     * 删除参数检查模板
     * @var array
     */
    public $del_params = array(
        'base_info'     =>      array(
            'nns_import_id'           =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '直播频道注入id',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => 'CP_ID',
            ),
            'nns_import_source'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '注入来源',
            ),
        ),
        'ex_info'       =>      array(
            'nns_key'                     =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '',
                'desc'      => '键',
            ),
            'nns_value'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '',
                'desc'      => '值',
            ),
            'nns_cp_id'                   =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => 'CP ID',
            ),
        ),
    );

    /**
     * 修改参数检查模板
     * @var array
     */
    public $add_params   = array(
        'base_info'     =>      array(
            'nns_import_id'                 =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-128',
                'desc'      => '注入id',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => 'CP_ID',
            ),
            'nns_name'                      =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-255',
                'desc'      => '直播频道名称',
            ),
            'nns_view_type'                 =>      array (
                'rule'      => 'tinyint',
                'default'   => '1',
                'length'    => '',
                'desc'      => '直播频道类型',
            ),
            'nns_org_type'                  =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '影片类型',
            ),
            'nns_tag'                       =>      array (
                'rule'      => 'noempty',
                'default'   => '26,',
                'length'    => '0-128',
                'desc'      => '终端标识默认26,',
            ),
            'nns_director'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-128',
                'desc'      => '导演，多个导演/分割',
            ),
            'nns_actor'                     =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-512',
                'desc'      => '演员，多个演员/分割',
            ),
            'nns_show_time'                 =>      array (
                'rule'      => 'date',
                'default'   => '',
                'length'    => '',
                'desc'      => '发布日期',
            ),
            'nns_view_len'                  =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '影片播放时长（秒）',
            ),
            'nns_all_index'                 =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '0-3',
                'desc'      => '分集总数必填参数',
            ),
            'nns_new_index'                 =>      array (
                'rule'      => 'tinyint',
                'default'   => '',
                'length'    => '0-3',
                'desc'      => '最新分集号 0开始必填参数',
            ),
            'nns_area'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-64',
                'desc'      => '上映地区，多个/分割',
            ),
            'nns_image0'                    =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => '大图',
            ),
            'nns_image1'                    =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => '中图',
            ),
            'nns_image2'                    =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => '小图',
            ),
            'nns_image3'                    =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => '扩展1',
            ),
            'nns_image4'                    =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => '0-扩展2',
            ),
            'nns_image5'                    =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => '扩展3',
            ),
            'nns_summary'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-4096',
                'desc'      => '频道描述',
            ),
            'nns_remark'                    =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-4096',
                'desc'      => '频道标志',
            ),
            'nns_category_id'               =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '栏目ID',
            ),
            'nns_play_count'                =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '播放次数',
            ),
            'nns_score'                     =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '评分数',
            ),
            'nns_score_count'               =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '评分次数',
            ),
            'nns_point'                     =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '推荐星级',
            ),
            'nns_copyright_date'            =>      array (
                'rule'      => 'date',
                'default'   => '',
                'length'    => '',
                'desc'      => '版权日期',
            ),
            'nns_pinyin'                    =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-128',
                'desc'      => '名称拼音',
            ),
            'nns_pinyin_length'             =>      array (
                'rule'      => 'smallint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '拼音长度',
            ),
            'nns_alias_name'                =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => '直播别名',
            ),
            'nns_eng_name'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => '英文名称',
            ),
            'nns_language'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-16',
                'desc'      => '语言',
            ),
            'nns_producer'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-64',
                'desc'      => '内容发布商',
            ),
            'nns_import_source'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '注入来源',
            ),
            'nns_integer_id'                =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-32',
                'desc'      => '自增序列号',
            ),
            'nns_image_v'                   =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => '横图',
            ),
            'nns_image_s'                   =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => '方图',
            ),
            'nns_image_h'                   =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => '竖图',
            ),
            'nns_image_logo'                =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-512',
                'desc'      => '播放器内logo',
            ),
            'nns_ext_info'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-128',
                'desc'      => '扩展信息,json字符串',
            ),
            'nns_image_t'                   =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-512',
                'desc'      => '频道内容截屏',
            ),
            'nns_state'               =>      array (
                'rule'      => 'int',
                'default'   => '1',
                'length'    => '',
                'desc'      => '状态',
            ),
            'nns_sub_type'               =>      array (
                'rule'      => 'int',
                'default'   => '1',
                'length'    => '',
                'desc'      => '频道信号源，默认为1 live；2是virtual',
            ),
            'nns_macrovision'               =>      array (
                'rule'      => 'int',
                'default'   => '1',
                'length'    => '',
                'desc'      => '直播频道有无拷贝保护，默认为1有，0为没有',
            ),
            'nns_hotdegree'               =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '直播频道发布到融合平台时队列优先级',
            ),
        ),
        'ex_info'       =>      array(
            'live_pindao_order'            =>      array(
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '频道号',
            ),
            'live_type'                    =>      array(
                'rule'      => 'noempty',
                'default'   => 'LIVE',
                'length'    => '1-32',
                'desc'      => '直播类型，多个以逗号隔开，LIVE直播|TSTV时移|PLAYBACK回看',
            ),
            'dvb'                          =>      array(
                'rule'      => '',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => 'dvb参数',
            ),
            'back_record_day_type'         =>      array(
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '回看类型',
            ),
            'back_record_day'              =>      array(
                'rule'      => 'tinyint',
                'default'   => '',
                'length'    => '',
                'desc'      => '回看天数',
            ),
            'pid'                          =>      array(
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '频道ID，人工设定',
            ),
            'ts_limit_min'                 =>      array(
                'rule'      => 'tinyint',
                'default'   => '',
                'length'    => '',
                'desc'      => '最小时移秒数',
            ),
            'ts_limit_max'                 =>      array(
                'rule'      => 'tinyint',
                'default'   => '',
                'length'     => '',
                'desc'      => '最大时移秒数',
            ),
            'ts_default_pos'               =>      array(
                'rule'      => 'tinyint',
                'default'   => '',
                'length'    => '',
                'desc'      => '默认时移秒数',
            ),
        ),
        'hide_info'     =>      array(
            'nns_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => 'GUID',
                'length'    => '32',
                'desc'      => '消息队列GUID',
            ),
            'nns_org_id'                    =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '运营商ID',
            ),
            'nns_depot_id'                  =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '直播栏目GUID',
            ),
            'nns_state'                     =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '直播频道状态',
            ),
            'nns_deleted'                   =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '是否删除 0 未删除  |  1删除',
            ),
            'nns_check'                     =>      array (
                'rule'      => 'tinyint',
                'default'   => '1',
                'length'    => '',
                'desc'      => '审核状态  1 审核通过 | 0 审核不通过',
            ),
        ),
    );

    /**
     * 导出参数模板
     * @var array
     */
    public $import_params   = array(
        'base_info'     =>      array(
            'nns_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => 'GUID',
                'length'    => '1-32',
                'desc'      => '频道id',
            ),
            'nns_import_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-128',
                'desc'      => '注入id',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => 'CP_ID',
            ),
            'nns_name'                      =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-255',
                'desc'      => '直播频道名称',
            ),
            'nns_state'                      =>      array (
                'rule'      => 'tinyint',
                'default'   => '',
                'length'    => '',
                'desc'      => '直播频道状态',
            ),
            'nns_view_type'                 =>      array (
                'rule'      => 'tinyint',
                'default'   => '1',
                'length'    => '',
                'desc'      => '直播频道类型',
            ),
            'nns_org_type'                  =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '影片类型',
            ),
            'nns_tag'                       =>      array (
                'rule'      => 'noempty',
                'default'   => '26,',
                'length'    => '0-128',
                'desc'      => '终端标识默认26,',
            ),
            'nns_director'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-128',
                'desc'      => '导演，多个导演/分割',
            ),
            'nns_actor'                     =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-512',
                'desc'      => '演员，多个演员/分割',
            ),
            'nns_show_time'                 =>      array (
                'rule'      => 'date',
                'default'   => '',
                'length'    => '',
                'desc'      => '发布日期',
            ),
            'nns_view_len'                  =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '影片播放时长（秒）',
            ),
            'nns_all_index'                 =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '0-3',
                'desc'      => '分集总数必填参数',
            ),
            'nns_new_index'                 =>      array (
                'rule'      => 'tinyint',
                'default'   => '',
                'length'    => '0-3',
                'desc'      => '最新分集号 0开始必填参数',
            ),
            'nns_area'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-64',
                'desc'      => '上映地区，多个/分割',
            ),
            'nns_image0'                    =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => '大图',
            ),
            'nns_image1'                    =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => '中图',
            ),
            'nns_image2'                    =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => '小图',
            ),
            'nns_image3'                    =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => '扩展1',
            ),
            'nns_image4'                    =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => '0-扩展2',
            ),
            'nns_image5'                    =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => '扩展3',
            ),
            'nns_summary'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-4096',
                'desc'      => '频道描述',
            ),
            'nns_remark'                    =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-4096',
                'desc'      => '频道标志',
            ),
            'nns_category_id'               =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '栏目ID',
            ),
            'nns_play_count'                =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '播放次数',
            ),
            'nns_score'               =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '评分数',
            ),
            'nns_score_count'               =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '评分次数',
            ),
            'nns_point'                     =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '推荐星级',
            ),
            'nns_copyright_date'            =>      array (
                'rule'      => 'date',
                'default'   => '',
                'length'    => '',
                'desc'      => '版权日期',
            ),
            'nns_pinyin'                    =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-128',
                'desc'      => '名称拼音',
            ),
            'nns_pinyin_length'             =>      array (
                'rule'      => 'smallint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '拼音长度',
            ),
            'nns_alias_name'                =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => '直播别名',
            ),
            'nns_eng_name'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => '英文名称',
            ),
            'nns_language'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-16',
                'desc'      => '语言',
            ),
            'nns_producer'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-64',
                'desc'      => '内容发布商',
            ),
            'nns_import_source'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '注入来源',
            ),
            'nns_integer_id'                    =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-32',
                'desc'      => '自增序列号',
            ),
            'nns_image_v'                   =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => '横图',
            ),
            'nns_image_s'                   =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => '方图',
            ),
            'nns_image_h'                   =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => '竖图',
            ),
            'nns_image_logo'                 =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-512',
                'desc'      => '播放器内logo',
            ),
            'nns_image_t'                 =>      array (
                'rule'      => 'image#live',
                'default'   => '',
                'length'    => '0-512',
                'desc'      => '频道内容截图',
            ),
            'ex_info'                 =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-512',
                'desc'      => '扩展参数，json串',
            ),
        ),
        'ex_info'       =>      array (
            'nns_key'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-32',
                'desc'      => '扩展参数的键',
            ),
            'nns_value'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-32',
                'desc'      => '扩展参数的值',
            ),
            'nns_cp_id'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-32',
                'desc'      => '扩展参数的CP ID',
            ),
        ),
        'hide_info'     =>      array(
            'nns_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => 'GUID',
                'length'    => '32',
                'desc'      => '消息队列GUID',
            ),
            'nns_org_id'                    =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '运营商ID',
            ),
            'nns_depot_id'                  =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '直播栏目GUID',
            ),
            'nns_state'                     =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '直播频道审核状态',
            ),
            'nns_deleted'                   =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '是否删除 0 未删除  |  1删除',
            ),
            'nns_check'                     =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '审核状态  0 审核通过 | 1 审核不通过',
            ),
        ),
    );

    /**
     * 检查注入参数
     * @param array $in_params 注入参数
     * @param string $action 操作参数行为
     * @return array|multitype array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function check_input_params($in_params, $action)
    {
        $notice_params = $last_params = array ();
        $str_params = "{$action}_params";
        if (!isset($this->$str_params))
        {
            return self::return_data(1, '没有设置' . $str_params . '参数的判断模式');
        }
        if (!is_array($this->$str_params))
        {
            return self::return_data(1, '参数非数组');
        }
        return $this->_check_input_params($this->$str_params, $in_params,'message_queue');
    }

    /**
     * 直播频道模板导出
     * @return array|multitype array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function export()
    {
        return self::return_data(0, 'ok', array('del_params'=>$this->del_params, 'import_params'=>$this->import_params));
    }

    /**
     * 添加/修改直播频道
     * @param $params //添加数据 不能为空
     * @return multitype|array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function add($params)
    {
        $result_carrier = $this->query_carrier();
        if($result_carrier['ret'] != 0)
        {
            return $result_carrier;
        }
        if(!isset($result_carrier['data_info']) || !is_array($result_carrier['data_info']) || empty($result_carrier['data_info']))
        {
            return self::return_data(1, "获取运营商信息为空");
        }
        $result_deport = $this->query_deport($result_carrier['data_info']['nns_id']);
        if($result_deport['ret'] !=0)
        {
            return $result_deport;
        }
        if(!isset($result_deport['data_info']) || !is_array($result_deport['data_info']) || empty($result_deport['data_info']))
        {
            return self::return_data(1, "获取栏目信息为空");
        }
        $params['hide_info']['nns_org_id'] = $result_carrier['data_info']['nns_id'];
        $params['hide_info']['nns_depot_id'] = $result_deport['data_info']['nns_id'];
        //注入频道参数检查，模板参考
        $check_input_params = $this->check_input_params($params, 'add');
        if($check_input_params['ret'] !=0)
        {
            return $check_input_params;
        }
        if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(1, '直播频道参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];
        #TODO CP配置逻辑
        //检查是否存在此直播频道
        $arr_channel_exist = array(
            'nns_import_id' => $params['base_info']['nns_import_id'],
            'nns_import_source' => $params['base_info']['nns_import_source'],
            'nns_cp_id' => $params['base_info']['nns_cp_id'],
        );
        $result_live_exist = nl_live::query_by_condition(m_config::get_dc(), $arr_channel_exist);
        if($result_live_exist['ret'] != 0)
        {
            return $result_live_exist;
        }
        //没有扩展信息
        if(!isset($params['ex_info']) || !is_array($params['ex_info']) || empty($params['ex_info']))
        {
            return self::return_data(1, '直播频道参数检查反馈数据为空');
        }
        //队列数组
        $arr_queue = array(
            'base_info' => array(
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
            )
        );
        if(isset($result_live_exist['data_info']) && is_array($result_live_exist['data_info']) && !empty($result_live_exist['data_info']))
        {
            //若直播频道已经存在，则更新
            $count_live_data = count($result_live_exist['data_info']);
            if($count_live_data >1)
            {
                return self::return_data(1, "查询直播频道相同条件下数量>{$count_live_data}原有的注入有问题,{$result_live_exist['reason']}");
            }
            $this->_check_and_del_input_image($this->add_params, $result_live_exist['data_info'][0],'message_queue');
            $check_input_params['data_info']['info']['out']['base_info']['nns_action'] = $params['base_info']['nns_action'] = 'modify';
            $check_input_params['data_info']['info']['out']['base_info']['nns_deleted'] = $params['base_info']['nns_deleted'] = '0';
            $check_input_params['data_info']['info']['out']['base_info']['nns_status'] = $params['base_info']['nns_status'] = '2';

            $result_execute = nl_live::edit(m_config::get_dc(), $params['base_info'], $result_live_exist['data_info'][0]['nns_id']);

            if($result_execute['ret'] != 0)
            {
                return $result_execute;
            }
            $arr_queue['base_info']['nns_id'] = $result_live_exist['data_info'][0]['nns_id'];
        }
        else
        {
            //不存在则添加
            $check_input_params['data_info']['info']['out']['base_info']['nns_action'] = $params['base_info']['nns_action'] = 'add';
            $check_input_params['data_info']['info']['out']['base_info']['nns_deleted'] = $params['base_info']['nns_deleted'] = '0';
            $check_input_params['data_info']['info']['out']['base_info']['nns_status'] = $params['base_info']['nns_status'] = '1';
            //添加直播频道
            $result_execute = nl_live::add(m_config::get_dc(), $params['base_info']);
            if($result_execute['ret'] != 0)
            {
                return $result_execute;
            }
            $arr_queue['base_info']['nns_id'] = $params['base_info']['nns_id'];
        }
        //先删除该直播的扩展信息
        $result_ex_delete = nl_live::del_ex_by_condition(m_config::get_dc(),
            array('nns_live_id' => $arr_queue['base_info']['nns_id'],
                'nns_cp_id' => $arr_queue['base_info']['nns_cp_id']));
        if ($result_ex_delete['ret'] != 0)
        {
            return $result_ex_delete;
        }
        $arr_add_live_ex = null;
        foreach ($params['ex_info'] as $ex_key => $ex_val)
        {
            if (strlen($ex_val) < 1)
            {
                continue;
            }
            $arr_add_live_ex[] = array(
                'nns_live_id' => $arr_queue['base_info']['nns_id'],
                'nns_cp_id' => $arr_queue['base_info']['nns_cp_id'],
                'nns_key' => $ex_key,
                'nns_value' => $ex_val,
            );
        }
        if (is_array($arr_add_live_ex) && !empty($arr_add_live_ex))
        {
            //向扩展信息表添加扩展信息
            $result_ex_add = nl_live::add_ex(m_config::get_dc(), $arr_add_live_ex);
            if ($result_ex_add['ret'] != 0)
            {
                return $result_ex_add;
            }
        }
        //入中心注入指令
        $re_push = $this->push_to_center_import($arr_queue, 'live', 0, $params['base_info']['nns_action']);
        $this->write_message_info($re_push);
        unset($arr_queue);
        return self::return_data(0, 'OK', $check_input_params['data_info']);
    }

    /**
     * 修改直播频道数据
     * @param array $params 修改数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function edit($params)
    {
        return $this->add($params);
    }

    /**
     * 删除直播频道数据
     * @param array $params 数据数组，不能为空
     * @return multitype|array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function del($params)
    {
        //注入主媒资参数检查，模板参考
        $check_input_params = $this->check_input_params($params,'del');
        if($check_input_params['ret'] != 0)
        {
            return $check_input_params;
        }
        if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(1, '主媒资参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];

        #TODO CP配置逻辑
        $arr_channel_exist = array(
            'nns_import_id' => $params['base_info']['nns_import_id'],
            'nns_import_source' => $params['base_info']['nns_import_source'],
            'nns_cp_id' => $params['base_info']['nns_cp_id'],
        );
        $result_live_exist = nl_live::query_by_condition(m_config::get_dc(), $arr_channel_exist);;
        if($result_live_exist['ret'] != 0)
        {
            return $result_live_exist;
        }
        $arr_del = null;
        if(isset($result_live_exist['data_info']) && is_array($result_live_exist['data_info']) && !empty($result_live_exist['data_info']))
        {
            foreach ($result_live_exist['data_info'] as $data_info)
            {
                $arr_del[] = $data_info['nns_id'];
            }
        }
        if(empty($arr_del) || !is_array($arr_del))
        {
            return self::return_data(1, "没有需要删除的数据，{$result_live_exist['reason']}");
        }

        //首先查询直播频道对应分集
        $obj_index_execute = new m_channel_media_inout();
        $query_index = array(
            'base_info' => array(
                'nns_live_id'=>$arr_del,
                'nns_deleted'=>0
            )
        );
        $result_index = $obj_index_execute->query($query_index);
        unset($query_index);

        if($result_index['ret'] != 0)
        {
            return $result_index;
        }
        $arr_index_del = null;
        if(isset($result_index['data_info']) && is_array($result_index['data_info']) && !empty($result_index['data_info']))
        {
            foreach ($result_index['data_info'] as $index_data_info)
            {
                $arr_index_del[] = $index_data_info['nns_id'];
            }
        }
        //队列数组
        $arr_queue = array(
            'base_info' => array(
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
            ),
        );

        //对应分集下存在片源则先修改片源
        if (!empty($arr_index_del) && isset($arr_index_del) && is_array($arr_index_del))
        {
            //先修改分集对应的片源
            $obj_execute = new m_channel_media_inout();
            $query_media = array(
                'base_info' => array(
                    'nns_live_index_id'=>$arr_index_del,
                    'nns_deleted'=>0
                )
            );
            $result_media = $obj_execute->query($query_media);
            unset($query_media);

            if($result_media['ret'] != 0)
            {
                return $result_media;
            }
            $arr_media_del = null;
            if(isset($result_media['data_info']) && is_array($result_media['data_info']) && !empty($result_media['data_info']))
            {
                foreach ($result_media['data_info'] as $media_data_info)
                {
                    $arr_media_del[] = $media_data_info['nns_id'];
                }
            }

            //存在片源则删除
            if (isset($arr_media_del) && is_array($arr_media_del) && !empty($arr_media_del))
            {
                $result_media_execute = nl_live_media::edit(m_config::get_dc(), array('nns_deleted'=>1), $arr_media_del);
                if($result_media_execute['ret'] != 0)
                {
                    return $result_media_execute;
                }

                //片源操作入队列
                foreach ($arr_media_del as $del_val)
                {
                    $arr_queue['base_info']['nns_id'] = $del_val;
                    $this->push_to_center_import($arr_queue, 'live_media', 0, 'destroy');
                }
            }

            //再修改分集
            $result_index_execute = nl_live_index::edit(m_config::get_dc(), array('nns_deleted' => 1), $arr_media_del);
            if($result_index_execute['ret'] != 0)
            {
                return $result_index_execute;
            }

            //分集操作入中心注入指令
            foreach ($arr_index_del as $del_val)
            {
                $arr_queue['base_info']['nns_id'] = $del_val;
                $this->push_to_center_import($arr_queue, 'live_index', 0, 'destroy');
            }

        }

        //删除直播
        $result_execute = nl_live::delete(m_config::$obj_dc, $arr_del);

        //硬删除扩展
        $result_execute_ex = nl_live::del_ex_by_condition(m_config::get_dc(),
            array('nns_vod_id' => $params['base_info']['nns_import_id'],
                'nns_cp_id' => $params['base_info']['nns_cp_id']));

        if($result_execute['ret'] != 0)
        {
            return $result_execute;
        }

        if($result_execute_ex['ret'] != 0)
        {
            return $result_execute_ex;
        }

        //直播频道入中心注入指令
        foreach ($arr_del as $del_val)
        {
            $arr_queue['base_info']['nns_id'] = $del_val;
            $this->push_to_center_import($arr_queue, 'live', 0);
        }

        return $result_live_exist;
    }

    /**
     * 查询直播频道数据
     * @param array $array_query 条件数据 不能为空
     * @param boolean $is_need_ext false 不需要查询扩展数据 | true 需要查询扩展数据
     * @param boolean $is_need_epg_import_id  false 不需要查询EPG扩展数据 | true 需要查询EPG扩展数据
     * @param boolean $is_need_cdn_import_id  false 不需要查询CDN扩展数据 | true 需要查询CDN扩展数据
     * @return multitype|array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function query($array_query, $is_need_ext = false, $is_need_epg_import_id = false, $is_need_cdn_import_id = false)
    {
        $last_data = null;
        $result_live = nl_live::query_by_condition(m_config::get_dc(), $array_query['base_info']);
        if($result_live['ret'] !=0 || !isset($result_live['data_info']) || !is_array($result_live['data_info']) || empty($result_live['data_info']))
        {
            return $result_live;
        }
        foreach ($result_live['data_info'] as $data_info)
        {
            $temp_epg_data = $temp_cdn_data = $temp_ex_data = null;
            if($is_need_ext)
            {
                $result_live_ex=nl_live::query_ex_by_id(m_config::get_dc(), $data_info['nns_id']);
                if($result_live_ex['ret'] != 0)
                {
                    return $result_live_ex;
                }
                if(isset($result_live_ex['data_info']) && is_array($result_live_ex['data_info']) && !empty($result_live_ex['data_info']))
                {
                    foreach ($result_live_ex['data_info'] as $ex_data_info)
                    {
                        $temp_ex_data[$ex_data_info['nns_key']] = $ex_data_info['nns_value'];
                    }
                }
            }
            if($is_need_epg_import_id)
            {
                #TODO
            }
            if($is_need_cdn_import_id)
            {
                #TODO
            }
            $last_data[]=array(
                'base_info' => $data_info,
                'ex_info' => $temp_ex_data,
                'epg_info' => $temp_epg_data,
                'cdn_info' => $temp_cdn_data,
            );
        }
        return self::return_data(0, 'OK', $last_data);
    }

    /**
     * 查询运营商信息
     */
    private function query_carrier()
    {
        $result_carrier = nl_carrier::query_all(m_config::get_dc());
        if($result_carrier['ret'] !=0)
        {
            return $result_carrier;
        }
        $result_carrier = (isset($result_carrier['data_info'][0]) && is_array($result_carrier['data_info'][0]) && !empty($result_carrier['data_info'][0])) ? $result_carrier['data_info'][0] : null;
        if(empty($result_carrier))
        {
            return self::return_data(1, '查询运营商数据 不存在');
        }
        return self::return_data(0, $result_carrier['reason'], $result_carrier);
    }

    /**
     * 查询栏目
     * @param $carrier_id
     * @return multitype|array
     */
    private function query_deport($carrier_id)
    {
        $result_depot = nl_depot::query_by_condition(m_config::get_dc(), array('nns_org_id'=>$carrier_id, 'nns_type'=>1));
        if($result_depot['ret'] != 0)
        {
            return $result_depot;
        }
        $result_depot = (isset($result_depot['data_info'][0]) && is_array($result_depot['data_info'][0]) && !empty($result_depot['data_info'][0])) ? $result_depot['data_info'][0] : null;
        if(empty($result_depot))
        {
            return self::return_data(1, '查询栏目数据 不存在');
        }
        return self::return_data(0, $result_depot['reason'], $result_depot);
    }
}