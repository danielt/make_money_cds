<?php
/**
 * @description:直播频道片源模板
 * @author:xinxin.deng
 * @date: 2017/12/7 11:39
 */
\ns_core\m_load::load("ns_data_model.m_data_model");
\ns_core\m_load::load_old("nn_logic/live/live_media.class.php");
\ns_core\m_load::load_old("nn_logic/depot/depot.class.php");
\ns_core\m_load::load_old("nn_logic/carrier/carrier.class.php");
class m_channel_media_inout  extends m_data_model
{
    /**
     * 删除参数模板
     * @var array
     */
    public $del_params = array(
        'base_info'     =>      array(
            'nns_content_id'           =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => '注入id',
            ),
            'nns_import_source'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => '注入来源',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => 'CP_ID',
            ),
        ),
    );

    /**
     * 添加修改参数模板
     * @var array
     */
    public $add_params = array(
        'base_info'     =>      array(
            'nns_name'                      =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '0-255',
                'desc'      => '片源名称',
            ),
            'nns_type'                      =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '片源类型',
            ),
            'nns_url'                       =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '',
                'desc'      => '片源url',
            ),
            'nns_tag'                       =>      array (
                'rule'      => 'noempty',
                'default'   => '26,',
                'length'     => '0-128',
                'desc'      => '终端标识默认26,',
            ),
            'nns_mode'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-32',
                'desc'      => '清晰度',
            ),
            'nns_kbps'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-16',
                'desc'      => '码率',
            ),
            'nns_content_id'                =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-32',
                'desc'      => '片源注入id',
            ),
            'nns_content_state'             =>      array (
                'rule'      => 'tinyint',
                'default'   => '',
                'length'     => '',
                'desc'      => '片源媒体状态',
            ),
            'nns_filetype'                  =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => '片源格式,ts|mp4',
            ),
            'nns_play_count'                =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'     => '',
                'desc'      => '播放次数',
            ),
            'nns_score_total'               =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'     => '1-8',
                'desc'      => '总评分',
            ),
            'nns_score_count'               =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'     => '1-8',
                'desc'      => '评分次数',
            ),
            'nns_media_caps'                =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-20',
                'desc'      => '片源媒体格式，TSTV(时移+回看),LIVE(直播)，因为时移和回看逻辑一致',
            ),
            'nns_cast_type'                 =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '0 默认老逻辑注入 | 1 UDP 注入 | 2 单播 注入',
            ),
            'nns_timeshift_status'          =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '时移标识,是否支持时移,1支持0不支持,默认不支持',
            ),
            'nns_timeshift_delay'           =>      array (
                'rule'      => 'int',
                'default'   => '',
                'length'     => '0-11',
                'desc'      => '时移时长(单位分钟),在时移标志生效时,有效',
            ),
            'nns_storage_status'            =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '录播标识,1生效0不生效',
            ),
            'nns_storage_delay'             =>      array (
                'rule'      => 'int',
                'default'   => '',
                'length'     => '0-11',
                'desc'      => '存储时长(单位小时),在录播标志生效时，有效',
            ),
            'nns_drm_enabled'               =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '是否使用drm加密，0否|1是',
            ),
            'nns_drm_encrypt_solution'      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-32',
                'desc'      => '加密方案',
            ),
            'nns_drm_info'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => 'drm扩展信息（json存储）',
            ),
            'nns_import_source'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => '注入来源',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => 'CP_ID',
            ),
            'nns_domain'                    =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-32',
                'desc'      => '上游注入的网络域',
            ),
            'nns_media_source'              =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '',
                'desc'      => '片源服务类型',
            ),
            'nns_ext_url'                    =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '上游方注入url 和其他信息 json存储',
            ),
            'nns_live_index_import_id'      =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-128',
                'desc'      => '上游方直播分集注入ID',
            ),
            'nns_live_import_id'            =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-128',
                'desc'      => '上游方直播注入ID',
            ),
            'nns_cdn_cast_type'                     =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '直播源CDN接收到的频道类型方式    0 单播放 |  1 组播',
            ),
            'nns_storage_duration'                     =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'     => '0-11',
                'desc'      => '直播源回看天数，CMS传值跟所属直播频道是一致的',
            ),
            'nns_multiple_cast_ip'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-128',
                'desc'      => '直播源组播 IP。当SrcCastType为组播时，必填',
            ),
            'nns_multiple_cast_port'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-128',
                'desc'      => '直播源组播端口。当SrcCastType为组播时，必填',
            ),
            'nns_unicast_url'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '直播源单播地址。当SrcCastType为单播时，必填。',
            ),
            'nns_resolution'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-128',
                'desc'      => '分辨率',
            ),
            'nns_hotdegree'                      =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'     => '0-11',
                'desc'      => '直播源发布到融合平台时队列优先级',
            ),
        ),
        'hide_info'     =>      array(
            'nns_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => 'GUID',
                'length'    => '32',
                'desc'      => '片源GUID',
            ),
            'nns_live_index_id'             =>      array (
                'rule'      => 'noempty',
                'default'   => 'parent|live_index_action|base_info+unset+nns_live_index_import_id+nns_import_id#base_info+set+nns_cp_id+nns_cp_id#base_info+set+nns_import_source+nns_import_source|base_info+nns_id',
                'length'     => '32',
                'desc'      => '直播分集id',
            ),
            'nns_live_index'                =>      array (
                'rule'      => 'tinyint',
                'default'   => 'parent|live_index_action|base_info+unset+nns_live_index_import_id+nns_import_id#base_info+set+nns_cp_id+nns_cp_id#base_info+set+nns_import_source+nns_import_source|base_info+nns_index',
                'length'     => '',
                'desc'      => '直播集数',
            ),
            'nns_live_id'                   =>      array (
                'rule'      => 'noempty',
                'default'   => 'parent|live_action|base_info+unset+nns_live_import_id+nns_import_id#base_info+set+nns_cp_id+nns_cp_id#base_info+set+nns_import_source+nns_import_source|base_info+nns_id',
                'length'     => '32',
                'desc'      => '直播id',
            ),
            'nns_original_id'               =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-128',
                'desc'      => '直播原始id',
            ),
            'nns_state'                     =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '片源状态',
            ),
            'nns_check'                     =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '审核状态  0 审核通过 | 1 审核不通过',
            ),
        ),
    );

    /**
     * 导出参数模板
     * @var array
     */
    public $import_params = array(
        'base_info'     =>      array(
            'nns_name'                      =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '0-255',
                'desc'      => '片源名称',
            ),
            'nns_type'                      =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '片源类型',
            ),
            'nns_url'                       =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '',
                'desc'      => '片源url',
            ),
            'nns_tag'                       =>      array (
                'rule'      => 'noempty',
                'default'   => '26,',
                'length'     => '0-128',
                'desc'      => '终端标识默认26,',
            ),
            'nns_mode'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-32',
                'desc'      => '清晰度',
            ),
            'nns_kbps'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-16',
                'desc'      => '码率',
            ),
            'nns_content_id'                =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-32',
                'desc'      => '片源媒体id',
            ),
            'nns_content_state'             =>      array (
                'rule'      => 'tinyint',
                'default'   => '',
                'length'     => '',
                'desc'      => '片源媒体状态',
            ),
            'nns_filetype'                  =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => '片源格式,ts|mp4',
            ),
            'nns_play_count'                =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'     => '',
                'desc'      => '播放次数',
            ),
            'nns_score_total'               =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'     => '1-8',
                'desc'      => '总评分',
            ),
            'nns_score_count'               =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'     => '1-8',
                'desc'      => '评分次数',
            ),
            'nns_media_caps'                =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-16',
                'desc'      => '片源媒体格式，TSTV(时移+回看),LIVE(直播)，因为时移和回看逻辑一致',
            ),
            'nns_cast_type'                 =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '0 默认老逻辑注入 | 1 UDP 注入 | 2 单播 注入',
            ),
            'nns_timeshift_status'          =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '时移标识,是否支持时移,1支持0不支持,默认不支持',
            ),
            'nns_timeshift_delay'           =>      array (
                'rule'      => 'int',
                'default'   => '',
                'length'     => '0-11',
                'desc'      => '时移时长(单位分钟),在时移标志生效时,有效',
            ),
            'nns_storage_status'            =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '录播标识,1生效0不生效',
            ),
            'nns_storage_delay'             =>      array (
                'rule'      => 'int',
                'default'   => '',
                'length'     => '0-11',
                'desc'      => '存储时长(单位小时),在录播标志生效时，有效',
            ),
            'nns_drm_enabled'               =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '是否使用drm加密，0否|1是',
            ),
            'nns_drm_encrypt_solution'      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-32',
                'desc'      => '加密方案',
            ),
            'nns_drm_info'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => 'drm扩展信息（json存储）',
            ),
            'nns_import_source'           =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => '注入来源',
            ),
            'nns_domain'                    =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-32',
                'desc'      => '上游注入的网络域',
            ),
            'nns_media_source'              =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '',
                'desc'      => '片源服务类型',
            ),
            'nns_ex_url'                    =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '上游方注入url 和其他信息 json存储',
            ),
        ),
        'hide_info'     =>      array(
            'nns_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '片源GUID',
            ),
            'nns_live_id'                   =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '32',
                'desc'      => '直播id',
            ),
            'nns_live_index_id'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '32',
                'desc'      => '直播分集id',
            ),
            'nns_live_index'                =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '直播集数',
            ),
            'nns_original_id'               =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-64',
                'desc'      => '直播原始id',
            ),
            'nns_state'                     =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '片源状态',
            ),
            'nns_check'                     =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '审核状态  0 审核通过 | 1 审核不通过',
            ),
        ),
    );
    
    public $arr_notice_params = null;

    /**
     * 注入类型
     * @var string
     */
    public $queue_type = 'media';

    /**
     * 检查注入参数
     * @param array $in_params 注入参数
     * @param string $action 操作参数行为
     * @return multitype|array array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function check_input_params($in_params,$action)
    {
        $notice_params = $last_params = array ();
        $str_params = "{$action}_params";
        if (!isset($this->$str_params))
        {
            return self::return_data(1, '没有设置' . $str_params . '参数的判断模式');
        }
        if (!is_array($this->$str_params))
        {
            return self::return_data(1, '参数非数组');
        }
        return $this->_check_input_params($this->$str_params, $in_params,'message_queue');
    }

    /**
     * 分集模板导出
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function export()
    {

        return self::return_data(0,'ok',array('del_params'=>$this->del_params,'import_params'=>$this->import_params));
    }

    /**
     * 添加片源数据
     * @param array $params 添加数据 不能为空
     * @return multitype|array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function add($params)
    {
        //注入片源参数检查，模板参考
        $check_input_params = $this->check_input_params($params,'add');

        if($check_input_params['ret'] !=0)
        {
            return $check_input_params;
        }
        if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(1, '片源操作,片源参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];
        #TODO CP配置逻辑
        $arr_media_exist = array(
            'nns_content_id'=>$params['base_info']['nns_content_id'],
            'nns_import_source'=>$params['base_info']['nns_import_source'],
            'nns_cp_id'=>$params['base_info']['nns_cp_id'],
        );
        $result_media_exist = nl_live_media::query_by_condition(m_config::get_dc(), $arr_media_exist);
        if($result_media_exist['ret'] != 0)
        {
            return $result_media_exist;
        }
        if(isset($result_media_exist['data_info']) && is_array($result_media_exist['data_info']) && !empty($result_media_exist['data_info']))
        {
            return self::return_data(1, "片源已经存在不在执行操作");
        }
        else
        {
            $check_input_params['data_info']['info']['out']['base_info']['nns_action'] = $params['base_info']['nns_action'] = 'add';
            $check_input_params['data_info']['info']['out']['base_info']['nns_deleted'] = $params['base_info']['nns_deleted'] = '0';
            $check_input_params['data_info']['info']['out']['base_info']['nns_status'] = $params['base_info']['nns_status'] = '1';
            $result_execute = nl_live_media::add(m_config::get_dc(), $params['base_info']);
            if($result_execute['ret'] != 0)
            {
                return $result_execute;
            }
        }
        //入中心注入指令
        $arr_queue = array(
            'base_info' => array(
                'nns_id' => $params['base_info']['nns_id'],
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
            )
        );
        $re_push = $this->push_to_center_import($arr_queue, 'live_media', 0, $params['base_info']['nns_action']);

        $this->write_message_info($re_push);
        return self::return_data(0,'OK',$check_input_params['data_info']);
    }

    /**
     * 修改片源数据
     * @param array $params 修改数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function edit($params)
    {
        return $this->add($params);
    }

    /**
     * 删除分集数据
     * @param array $params 数据数组，不能为空
     * @return multitype|array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function del($params)
    {
        //注入分集参数检查，模板参考
        $check_input_params = $this->check_input_params($params, 'del');
        if($check_input_params['ret'] != 0)
        {
            return $check_input_params;
        }
        if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(1, '片源参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];
        //查询片源
        $result_media_exist = nl_live_media::query_by_condition(m_config::get_dc(), $params['base_info']);
        if($result_media_exist['ret'] !=0)
        {
            return $result_media_exist;
        }
        $arr_del = null;
        if(isset($result_media_exist['data_info']) && is_array($result_media_exist['data_info']) && !empty($result_media_exist['data_info']))
        {
            foreach ($result_media_exist['data_info'] as $data_info)
            {
                $arr_del[] = $data_info['nns_id'];
            }
        }
        if(empty($arr_del) || !is_array($arr_del))
        {
            return self::return_data(1, "没有需要删除的数据，{$result_media_exist['reason']}");
        }

        //修改片源文件
        $result_execute = nl_live_media::edit(m_config::get_dc(), array('nns_deleted' => 1), $arr_del);
        if($result_execute['ret'] !=0)
        {
            return $result_execute;
        }

        //入中心注入指令
        if(!empty($arr_del) && is_array($arr_del))
        {
            $arr_queue = array(
                'base_info' => array(
                    'nns_cp_id' => $params['base_info']['nns_cp_id'],
                ),
            );
            foreach ($arr_del as $del_val)
            {
                $arr_queue['base_info']['nns_id'] = $del_val;
                $re_push = $this->push_to_center_import($arr_queue, 'live_media', 0, 'destroy');
                $this->write_message_info($re_push);
            }
        }
        return self::return_data(0, 'ok', $check_input_params['data_info']);
    }

    /**
     * 查询直播片源数据
     * @param array $array_query 条件数据 不能为空
     * @param boolean $is_need_ext false 不需要查询扩展数据 | true 需要查询扩展数据
     * @param boolean $is_need_epg_import_id  false 不需要查询EPG扩展数据 | true 需要查询EPG扩展数据
     * @param boolean $is_need_cdn_import_id  false 不需要查询CDN扩展数据 | true 需要查询CDN扩展数据
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function query($array_query, $is_need_ext = false, $is_need_epg_import_id = false, $is_need_cdn_import_id = false)
    {
        $last_data = null;
        $result_media = nl_live_media::query_by_condition(m_config::get_dc(), $array_query['base_info']);
        if($result_media['ret'] !=0 || !isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
        {
            return $result_media;
        }
        foreach ($result_media['data_info'] as $data_info)
        {
            $temp_epg_data = $temp_cdn_data = $temp_ex_data = null;

            if($is_need_ext)
            {
                #TODO 预留扩展信息
            }

            if($is_need_epg_import_id)
            {
                #TODO
            }
            if($is_need_cdn_import_id)
            {
                #TODO
            }
            $last_data[]=array(
                'base_info'=>$data_info,
                'ex_info'=>$temp_ex_data,
                'epg_info'=>$temp_epg_data,
                'cdn_info'=>$temp_cdn_data,
            );
        }
        return self::return_data(0,'OK',$last_data);
    }

}