<?php
/**
 * @description:直播频道分集模板
 * @author:xinxin.deng
 * @date: 2017/12/7 11:39
 */
\ns_core\m_load::load("ns_data_model.m_data_model");
\ns_core\m_load::load_old("nn_logic/live/live_index.class.php");
\ns_core\m_load::load_old("nn_logic/depot/depot.class.php");
\ns_core\m_load::load_old("nn_logic/carrier/carrier.class.php");

class m_channel_index_inout  extends m_data_model
{
    /**
     * 删除参数检查模板
     * @var array
     */
    public $del_params = array(
        'base_info'     =>      array(
            'nns_import_id'                 =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-128',
                'desc'      => '分集注入id',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => 'CP_ID',
            ),
            'nns_import_source'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '注入来源',
            ),
        ),
    );

    /**
     * 添加/修改参数检查模板
     * @var array
     */
    public $add_params = array(
        'base_info'     =>      array(
            'nns_name'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => '分集名称',
            ),
            'nns_index'                     =>      array (
                'rule'      => 'smallint',
                'default'   => '',
                'length'    => '',
                'desc'      => '分集号   0 开始',
            ),
            'nns_time_len'                  =>      array (
                'rule'      => 'smallint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '分集时长（秒）',
            ),
            'nns_summary'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-4096',
                'desc'      => '分集描述',
            ),
            'nns_image'                     =>      array (
                'rule'      => 'image#live_index',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => '分集海报',
            ),
            'nns_play_count'                =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '播放次数',
            ),
            'nns_score'                     =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '评分总数',
            ),
            'nns_score_count'               =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '评分次数',
            ),
            'nns_import_id'                 =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '分集注入id',
            ),
            'nns_live_import_id'            =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '直播注入id',
            ),
            'nns_import_source'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '注入来源',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => 'CP_ID',
            ),
            'nns_ext_url'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-2048',
                'desc'      => '上游方注入url 和其他信息  json存储',
            ),
        ),
        'hide_info'     =>      array(
            'nns_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => 'GUID',
                'length'    => '32',
                'desc'      => '分集GUID',
            ),
            'nns_live_id'                   =>      array (
                'rule'      => 'noempty',
                'default'   => 'parent|live_action|base_info+unset+nns_live_import_id+nns_import_id#base_info+set+nns_cp_id+nns_cp_id#base_info+set+nns_import_source+nns_import_source|base_info+nns_id',
                'length'    => '32',
                'desc'      => '直播频道的GUID',
            ),
            'nns_original_id'               =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-32',
                'desc'      => '运营商ID',
            ),
            'nns_deleted'                   =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '是否删除 0 未删除  |  1删除',
            ),
        ),
    );

    /**
     * 导出参数检查模板
     * @var array
     */
    public $import_params = array(
        'base_info'     =>      array(
            'nns_name'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => '分集名称',
            ),
            'nns_index'                     =>      array (
                'rule'      => 'smallint',
                'default'   => '',
                'length'    => '',
                'desc'      => '分集号   0 开始',
            ),
            'nns_time_len'                  =>      array (
                'rule'      => 'smallint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '分集时长（秒）',
            ),
            'nns_summary'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-4096',
                'desc'      => '分集描述',
            ),
            'nns_image'                     =>      array (
                'rule'      => 'image#live_index',
                'default'   => '',
                'length'    => '0-255',
                'desc'      => '分集海报',
            ),
            'nns_play_count'                =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '播放次数',
            ),
            'nns_score'                     =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '评分总数',
            ),
            'nns_score_count'               =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '评分次数',
            ),
            'nns_import_id'                 =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '分集注入id',
            ),
            'nns_live_import_id'            =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '直播注入id',
            ),
            'nns_import_source'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '注入来源',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => 'CP_ID',
            ),
            'nns_ext_url'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-2048',
                'desc'      => '上游方注入url 和其他信息  json存储',
            ),
        ),
        'hide_info'     =>      array(
            'nns_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => 'GUID',
                'length'    => '32',
                'desc'      => '分集GUID',
            ),
            'nns_live_id'                   =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '直播频道的GUID',
            ),
            'nns_original_id'               =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '运营商ID',
            ),
            'nns_deleted'                   =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '是否删除 0 未删除  |  1删除',
            ),
        ),
    );

    /**
     * 检查注入参数
     * @param array $in_params 注入参数
     * @param string $action 操作参数行为
     * @return multitype|array array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function check_input_params($in_params, $action)
    {
        $notice_params = $last_params = array ();
        $str_params = "{$action}_params";
        if (!isset($this->$str_params))
        {
            return self::return_data(1, '没有设置' . $str_params . '参数的判断模式');
        }
        if (!is_array($this->$str_params))
        {
            return self::return_data(1, '参数非数组');
        }
        return $this->_check_input_params($this->$str_params, $in_params,'message_queue');
    }

    /**
     * 分集模板导出
     * @return multitype|array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function export()
    {
        return self::return_data(0, 'ok', array( 'del_params'=>$this->del_params, 'import_params'=>$this->import_params));
    }

    /**
     * 添加分集数据
     * @param array $params 添加数据 不能为空
     * @return multitype|array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function add($params)
    {
        //注入分集参数检查，模板参考
        $check_input_params = $this->check_input_params($params, 'add');
        if($check_input_params['ret'] != 0)
        {
            return $check_input_params;
        }
        if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(1, '分集参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];
        $int_index = $params['base_info']['nns_index']+1;

        $m_queue_model = new \ns_model\m_queue_model();
        $result_live_info = $m_queue_model->_get_queue_info($params['hide_info']['nns_live_id'], 'live');
        if($result_live_info['ret'] !=0)
        {
            return $result_live_info;
        }
        if(!isset($result_live_info['data_info']['live']['base_info']['nns_all_index']))
        {
            return self::return_data(1,"直播总集数未查询到",$check_input_params['data_info']);
        }
        if($int_index > $result_live_info['data_info']['live']['base_info']['nns_all_index'])
        {
            return self::return_data(1,"主媒资总集数{$result_live_info['data_info']['live']['base_info']['nns_all_index']}<分集集数{$int_index}注入的集数请检查分集集数情况",$check_input_params['data_info']);
        }
        #TODO CP配置逻辑
        $arr_index_exist = array(
            'nns_live_id' => $params['base_info']['nns_live_id'],
            'nns_import_source' => $params['base_info']['nns_import_source'],
            'nns_cp_id' => $params['base_info']['nns_cp_id'],
            'nns_index' => $params['base_info']['nns_index'],
        );

        //查询是否存在分集
        $result_index_exist = nl_live_index::query_by_condition(m_config::get_dc(), $arr_index_exist);
        if($result_index_exist['ret'] !=0)
        {
            return $result_index_exist;
        }
        //队列数组
        $arr_queue = array(
            'base_info' => array(
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
            )
        );
        if(isset($result_index_exist['data_info']) && is_array($result_index_exist['data_info']) && !empty($result_index_exist['data_info']))
        {
            $count_vod_data = count($result_index_exist['data_info']);
            if($count_vod_data > 1)
            {
                return self::return_data(1, "查询分集信息相同条件下主媒资数量>{$count_vod_data}原有的注入有问题,{$result_index_exist['reason']}");
            }
            $this->_check_and_del_input_image($this->add_params, $result_index_exist['data_info'][0], 'message_queue');
            $check_input_params['data_info']['info']['out']['base_info']['nns_action'] = $params['base_info']['nns_action'] = 'modify';
            $check_input_params['data_info']['info']['out']['base_info']['nns_deleted'] = $params['base_info']['nns_deleted'] = '0';
            $check_input_params['data_info']['info']['out']['base_info']['nns_status'] = $params['base_info']['nns_status'] = '2';
            $result_execute = nl_live_index::edit(m_config::get_dc(), $params['base_info'], $result_index_exist['data_info'][0]['nns_id']);

            if($result_execute['ret'] != 0)
            {
                return $result_execute;
            }

            $arr_queue['base_info']['nns_id'] = $result_index_exist['data_info'][0]['nns_id'];
        }
        else
        {
            $check_input_params['data_info']['info']['out']['base_info']['nns_action'] = $params['base_info']['nns_action'] = 'add';
            $check_input_params['data_info']['info']['out']['base_info']['nns_deleted'] = $params['base_info']['nns_deleted'] = '0';
            $check_input_params['data_info']['info']['out']['base_info']['nns_status'] = $params['base_info']['nns_status'] = '1';
            $result_execute = nl_live_index::add(m_config::$obj_dc, $params['base_info']);

            if($result_execute['ret'] != 0)
            {
                return $result_execute;
            }
            $arr_queue['base_info']['nns_id'] = $params['base_info']['nns_id'];
        }
        //入中心注入指令
        $re_push = $this->push_to_center_import($arr_queue, 'live_index', 0, $params['base_info']['nns_action']);
        $this->write_message_info($re_push);
        unset($arr_queue);
        return self::return_data(0, 'OK', $check_input_params['data_info']);
    }

    /**
     * 修改分集数据
     * @param array $params 修改数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function edit($params)
    {
        return $this->add($params);
    }

    /**
     * 删除分集数据
     * @param array $params 数据数组，不能为空
     * @return multitype|array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function del($params)
    {
        //注入分集参数检查，模板参考
        $check_input_params = $this->check_input_params($params,'del');
        if($check_input_params['ret'] != 0)
        {
            return $check_input_params;
        }
        if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(1, '分集参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];
        //查询分集
        $result_index_exist = nl_live_index::query_by_condition(m_config::get_dc(), $params['base_info']);
        if($result_index_exist['ret'] !=0)
        {
            return $result_index_exist;
        }
        $arr_del = null;
        if(isset($result_index_exist['data_info']) && is_array($result_index_exist['data_info']) && !empty($result_index_exist['data_info']))
        {
            foreach ($result_index_exist['data_info'] as $data_info)
            {
                $arr_del[] = $data_info['nns_id'];
            }
        }
        if(empty($arr_del) || !is_array($arr_del))
        {
            return self::return_data(1, "没有需要删除的数据，{$result_index_exist['reason']}");
        }

        //队列数组
        $arr_queue = array(
            'base_info' => array(
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
            ),
        );
        //先修改分集对应的片源
        $obj_execute = new m_channel_media_inout();
        $query_media = array(
            'base_info' => array(
                'nns_live_index_id'=>$arr_del,
                'nns_deleted'=>0
            )
        );
        $result_media = $obj_execute->query($query_media);
        unset($query_media);

        if($result_media['ret'] != 0)
        {
            return $result_media;
        }
        $arr_media_del = null;
        if(isset($result_media['data_info']) && is_array($result_media['data_info']) && !empty($result_media['data_info']))
        {
            foreach ($result_media['data_info'] as $media_data_info)
            {
                $arr_media_del[] = $media_data_info['nns_id'];
            }
        }
        if (isset($arr_media_del) && is_array($arr_media_del) && !empty($arr_media_del))
        {
            $result_media_execute = nl_live_media::edit(m_config::get_dc(), array('nns_deleted'=>1), $arr_media_del);
            if($result_media_execute['ret'] != 0)
            {
                return $result_media_execute;
            }

            //对应片源队列
            if(empty($arr_media_del) || !is_array($arr_media_del))
            {
                foreach ($arr_media_del as $del_val)
                {
                    $arr_queue['base_info']['nns_id'] = $del_val;
                    $this->push_to_center_import($arr_queue, 'live_media', 0, 'destroy');
                }
            }
        }

        //再修改分集
        $result_index_execute = nl_live_index::edit(m_config::get_dc(), array('nns_deleted' => 1), $arr_media_del);
        if($result_index_execute['ret'] != 0)
        {
            return $result_index_execute;
        }

        //分集队列
        if(empty($arr_del) || !is_array($arr_del))
        {
            foreach ($arr_del as $del_val)
            {
                $arr_queue['base_info']['nns_id'] = $del_val;
                $this->push_to_center_import($arr_queue, 'live_index', 0, 'destroy');
            }
        }
        unset($arr_queue);
        return self::return_data(0,'ok',$check_input_params['data_info']);
    }

    /**
     * 查询分集数据
     * @param array $array_query 条件数据 不能为空
     * @param boolean $is_need_ext false 不需要查询扩展数据 | true 需要查询扩展数据
     * @param boolean $is_need_epg_import_id  false 不需要查询EPG扩展数据 | true 需要查询EPG扩展数据
     * @param boolean $is_need_cdn_import_id  false 不需要查询CDN扩展数据 | true 需要查询CDN扩展数据
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function query($array_query, $is_need_ext=false, $is_need_epg_import_id = false, $is_need_cdn_import_id = false)
    {
        $last_data = null;
        $result_live_index = nl_live_index::query_by_condition(m_config::get_dc(), $array_query['base_info']);

        if($result_live_index['ret'] !=0 || !isset($result_live_index['data_info']) || !is_array($result_live_index['data_info']) || empty($result_live_index['data_info']))
        {
            return $result_live_index;
        }
        foreach ($result_live_index['data_info'] as $data_info)
        {
            $temp_epg_data = $temp_cdn_data = $temp_ex_data = null;
            if($is_need_ext)
            {
                #TODO 预留扩展参数处理
            }
            if($is_need_epg_import_id)
            {
                #TODO
            }
            if($is_need_cdn_import_id)
            {
                #TODO
            }
            $last_data[]=array(
                'base_info'=>$data_info,
                'ex_info'=>$temp_ex_data,
                'epg_info'=>$temp_epg_data,
                'cdn_info'=>$temp_cdn_data,
            );
        }

        return self::return_data(0,'OK',$last_data);
    }

}