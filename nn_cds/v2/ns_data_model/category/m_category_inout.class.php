<?php
\ns_core\m_load::load("ns_data_model.m_data_model");
\ns_core\m_load::load_old("nn_logic/depot/depot.class.php");
\ns_core\m_load::load_old("nn_logic/carrier/carrier.class.php");
class m_category_inout  extends m_data_model
{
    /**
     * xml的 属性参数
     * @var unknown
     */
    public $arr_fileds = array(
                'nns_id'=>'id',
                'nns_name'=>'name',
                'nns_parent_id'=>'parent',
                'nns_import_category_id'=>'import_id',
                'nns_cp_id'=>'cp_id',
                'nns_import_parent_category_id'=>'parent_import_id',
    );
    /**
     * 添加参数检查模板
     * @var unknown
     */
    public $add_params  = array(
        'base_info'     =>      array(
            'nns_name'                      =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-64',
                'desc'      => '栏目名称',
            ),
            'nns_import_category_id'        =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-128',
                'desc'      => '第三方栏目ID',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '0',
                'length'    => '1-32',
                'desc'      => 'cp_id',
            ),
            'nns_import_parent_category_id' =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-128',
                'desc'      => '第三方栏目ID',
            ),
            'nns_video_type'                =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '影片类型 0 点播 | 1 直播',
            ),
        ),
    );
    
    /**
     * 删除参数检查模板
     * @var unknown
     */
    public $del_params  = array(
        'base_info'     =>      array(
            'nns_name'                      =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-20',
                'desc'      => '栏目名称',
            ),
            'nns_import_category_id'        =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-128',
                'desc'      => '第三方栏目ID',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => 'cp_id',
            ),
            'nns_video_type'                =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '影片类型 0 点播 | 1 直播',
            ),
            'nns_id'                        =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '',
                'desc'      => '平台自身的ID',
            ),
        ),
    );
    /**
     * 检查注入参数
     * @param array $in_params 注入参数
     * @param string $action 操作参数行为
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function check_input_params($in_params,$action)
    {
        $notice_params = $last_params = array ();
        $str_params = "{$action}_params";
        if (!isset($this->$str_params))
        {
            return $this->_return_data(1, '没有设置' . $str_params . '参数的判断模式');
        }
        if (!is_array($this->$str_params))
        {
            return $this->_return_data(1, '参数非数组');
        }
        return $this->_check_input_params($this->$str_params, $in_params,'message_queue');
    }
    
    /**
     * 主媒资模板导出
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function export()
    {
        return self::return_data(0,'ok',array('add_params'=>$this->add_params,'edit_params'=>$this->edit_params,'del_params'=>$this->del_params));
    }
    
    /**
     * 添加栏目数据
     * @param array $params 添加数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function add($params)
    {
        //注入主媒资参数检查，模板参考
        $check_input_params = $this->check_input_params($params,'add');
        if($check_input_params['ret'] !=0)
        {
            return $check_input_params;
        }
        if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return $this->_return_data(1,'主媒资参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];
        $result_cp = m_config::_get_cp_info($params['base_info']['nns_cp_id']);
        if($result_cp['ret'] !=0)
        {
            return $result_cp;
        }
        if(!isset($result_cp['data_info']) || !is_array($result_cp['data_info']) || empty($result_cp['data_info']))
        {
            return self::return_data(1,'未查询到CP信息，不执行 栏目创建');
        }
        $params['base_info']['nns_name'] = str_replace('//', '/', $params['base_info']['nns_name']);
        $params['base_info']['nns_name'] = str_replace('\\', '/', $params['base_info']['nns_name']);
        //栏目允许注入第三方栏目信息ID
        if(isset($params['base_info']['nns_import_category_id']) && strlen($params['base_info']['nns_import_category_id']) >0 && isset($params['base_info']['nns_import_parent_category_id']) && strlen($params['base_info']['nns_import_parent_category_id']) >0)
        {
            $temp_params = $params;
            $temp_params['base_info']['nns_import_parent_category_id'] = '0';
            $temp_params['base_info']['nns_import_category_id'] = 'bk_cp_root';
            $temp_params['base_info']['nns_name'] = $result_cp['data_info']['nns_name'];
            $temp_params['base_info']['nns_parent_id'] = '0';
            $result_parent = $this->insert_cate($temp_params);
            if($result_parent['ret'] !=0)
            {
                return $result_parent;
            }
            if(!isset($result_parent['data_info']['base_info']['nns_id']) || strlen($result_parent['data_info']['base_info']['nns_id']) <1)
            {
                return self::return_data(1,'未查询到父级信息不在创建栏目');
            }
            $result_category = $this->get_cate_info($params['base_info']['nns_video_type']);
            if($result_category['ret'] !=0)
            {
                return $result_category;
            }
            if(!isset($result_category['data_info']) || empty($result_category['data_info']) || !is_array($result_category['data_info']))
            {
                return self::return_data(1,'查询栏目为空,不执行');
            }
            $result_category_last = $result_category = $result_category['data_info'];
            foreach ($result_category as $cate_key=>$cate_val)
            {
                if((string)$cate_val['cp_id'] !== (string)$params['base_info']['nns_cp_id'])
                {
                    unset($result_category[$cate_key]);
                }
            }
            foreach ($result_category as $cate_key=>$cate_val)
            {
                if((string)$cate_val['import_id'] === (string)$params['base_info']['nns_import_category_id'] && (string)$cate_val['cp_id'] === (string)$params['base_info']['nns_cp_id'])
                {
                    $params['base_info']['nns_id'] = (string)$cate_val['id'];
                    if((string)$cate_val['name'] !== (string)$params['base_info']['nns_name'] || (string)$cate_val['parent_import_id'] !== (string)$params['base_info']['nns_import_parent_category_id'])
                    {
                        $result_category_last[$cate_key]['name'] = (string)$params['base_info']['nns_name'];
                        $result_category_last[$cate_key]['parent_import_id'] = (string)$params['base_info']['nns_import_parent_category_id'];
                        return $this->insert_cate_new($result_category_last,$params);
                    }
                    return self::return_data(0,'ok,栏目已经存在了',$params);
                }
            }
            $is_need_add_root = true;
            if((string)$params['base_info']['nns_import_parent_category_id'] === '0')
            {
                $params['base_info']['nns_import_parent_category_id'] = 'bk_cp_root';
            }
            foreach ($result_category as $cate_key=>$cate_val)
            {
                if((string)$cate_val['import_id'] === (string)$params['base_info']['nns_import_parent_category_id'])
                {
                    $is_need_add_root = false;
                    for ($i=1;$i<1000;$i++)
                    {
                        $str_temp_id = $cate_val['id'].str_pad($i, 3, "0", STR_PAD_LEFT);
                        $str_temp_id = (string)$str_temp_id;
                        if(isset($result_category[$str_temp_id]))
                        {
                            continue;
                        }
                        else
                        {
                            $result_category_last[$str_temp_id] = array(
                                'id'=>(string)$str_temp_id,
                                'name'=>(string)$params['base_info']['nns_name'],
                                'parent'=>(string)$cate_val['id'],
                                'import_id'=>(string)$params['base_info']['nns_import_category_id'],
                                'cp_id'=>(string)$params['base_info']['nns_cp_id'],
                                'parent_import_id'=>(string)$params['base_info']['nns_import_parent_category_id'],
                            );
                            $flag_is_surpass = true;
                            $params['base_info']['nns_id'] = (string)$str_temp_id;
                            return $this->insert_cate_new($result_category_last,$params);
                        }
                    }
                    return self::return_data('1','超过了子栏目创建的限制了不再创建栏目');
                }
            }
            return self::return_data(1,'内部错误');
        }
        $str_name = $result_cp['data_info']['nns_name'].'/'.trim(trim($params['base_info']['nns_name'],'/'),'\\');
        $arr_name = explode('/', $str_name);
        if(!is_array($arr_name) || empty($arr_name))
        {
            return self::return_data(1,'栏目名称为空，不创建栏目');
        }
        foreach ($arr_name as $cate_key=>$cate_name)
        {
            $cate_name = trim($cate_name);
            if(strlen($cate_name) <1)
            {
                continue;
            }
            if($cate_key == 0)
            {
                $params['base_info']['nns_name'] = $cate_name;
                $params['base_info']['nns_parent_id'] = '0';
                $result_parent = $this->insert_cate($params);
                if($result_parent['ret'] !=0)
                {
                    return $result_parent;
                }
            }
            else
            {
                if(!isset($result_parent['data_info']['base_info']['nns_id']) || strlen($result_parent['data_info']['base_info']['nns_id']) <1)
                {
                    return self::return_data(1,'未查询到父级信息不在创建栏目');
                }
                $params['base_info']['nns_name'] = $cate_name;
                $params['base_info']['nns_parent_id'] = $result_parent['data_info']['base_info']['nns_id'];
                $result_parent = $this->insert_cate($params);
                if($result_parent['ret'] !=0)
                {
                    return $result_parent;
                }
            }
        }
        return (isset($result_parent)) ? $result_parent : self::return_data(1,'内部错误');
    }
    
    /**
     * 循环添加栏目的基类 注意逻辑哈 
     * @param unknown $params
     */
    private function insert_cate($params)
    {
        $result_category = $this->get_cate_info($params['base_info']['nns_video_type']);
        if($result_category['ret'] !=0)
        {
            return $result_category;
        }
//        if(!isset($result_category['data_info']) || empty($result_category['data_info']) || !is_array($result_category['data_info']))
//        {
//            return self::return_data(1,'查询栏目为空,不执行');
//        }
        $result_make_xml = $this->make_cate_info($result_category['data_info'], $params);
        if($result_make_xml['ret'] !=0)
        {
            return $result_make_xml;
        }
        if(!isset($result_make_xml['data_info']) || strlen($result_make_xml['data_info']) <1)
        {
            return self::return_data(0,$result_make_xml['reason'],$result_make_xml['ext_info']);
        }
        if(!self::is_xml($result_make_xml['data_info']))
        {
            return self::return_data(1,'生成的xml非xml不修改');
        }
        $edit_data = array(
            'nns_name'=>$params['base_info']['nns_video_type']=='0' ? '点播栏目' : '直播栏目',
            'nns_category'=>$result_make_xml['data_info'],
        );
        $result_depot_edit = nl_depot::edit(self::$obj_dc,$edit_data,$result_category['ext_info']['nns_id']);
        if($result_depot_edit['ret'] !=0)
        {
            return $result_depot_edit;
        }
        return self::return_data(0,'ok'.$result_depot_edit['reason'],$result_make_xml['ext_info']);
    }
    
    /**
     * 循环添加栏目的基类 注意逻辑哈
     * @param unknown $params
     */
    private function insert_cate_new($arr_category,$params)
    {
        $result_category = $this->get_cate_info($params['base_info']['nns_video_type']);
        if($result_category['ret'] !=0)
        {
            return $result_category;
        }
        if(!isset($result_category['data_info']) || empty($result_category['data_info']) || !is_array($result_category['data_info']))
        {
            return self::return_data(1,'查询栏目为空,不执行');
        }
        $result_make_xml = $this->make_category_xml($arr_category);
        if($result_make_xml['ret'] !=0)
        {
            return $result_make_xml;
        }
        if(!isset($result_make_xml['data_info']) || strlen($result_make_xml['data_info']) <1)
        {
            return self::return_data(0,$result_make_xml['reason'],$result_make_xml['ext_info']);
        }
        if(!self::is_xml($result_make_xml['data_info']))
        {
            return self::return_data(1,'生成的xml非xml不修改');
        }
        $edit_data = array(
            'nns_name'=>$params['base_info']['nns_video_type']=='0' ? '点播栏目' : '直播栏目',
            'nns_category'=>$result_make_xml['data_info'],
        );
        $result_depot_edit = nl_depot::edit(self::$obj_dc,$edit_data,$result_category['ext_info']['nns_id']);
        if($result_depot_edit['ret'] !=0)
        {
            return $result_depot_edit;
        }
        return self::return_data(0,'ok',$params);
    }
    
    
    public function delete()
    {
        #TODO
    }
    
    /**
     * 查询栏目数据
     * @param array $array_query 条件数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function query($array_query)
    {
        $result_category = $this->get_cate_info($array_query['base_info']['nns_video_type']);
        unset($array_query['base_info']['nns_video_type']);
        if($result_category['ret'] !=0)
        {
            return $result_category;
        }
        if(!isset($result_category['data_info']) || empty($result_category['data_info']) || !is_array($result_category['data_info']))
        {
            return self::return_data(0,'查询栏目为空');
        }
        $last_category = $result_category['data_info'];
        foreach ($last_category as $key=>$val)
        {
            foreach ($this->arr_fileds as $q_key=>$q_val)
            {
                if(!isset($val[$q_val]) || (isset($array_query['base_info'][$q_key]) && (string)$array_query['base_info'][$q_key] !== (string)$val[$q_val]))
                {
                    unset($last_category[$key]);
                }
            }
        }
        $last_category = (is_array($last_category) && !empty($last_category)) ? array_values($last_category) : null;
        return self::return_data(0,'OK',$last_category,$result_category['ext_info']);
    }
    
    /**
     * 生产栏目信息
     * @param unknown $str_category
     */
    private function make_cate_info($arr_category,$pamras)
    {
        $pamras['base_info']['nns_name'] = str_replace("<", "", $pamras['base_info']['nns_name']);
        $pamras['base_info']['nns_name'] = str_replace("/>", "", $pamras['base_info']['nns_name']);
        $pamras['base_info']['nns_name'] = str_replace("\"", "", $pamras['base_info']['nns_name']);
        $pamras['base_info']['nns_name'] = str_replace("'", "", $pamras['base_info']['nns_name']);
        $pamras['base_info']['nns_name'] = str_replace('"', "", $pamras['base_info']['nns_name']);
        $pamras['base_info']['nns_name'] = str_replace(' ', "", $pamras['base_info']['nns_name']);
        if(empty($arr_category) || !is_array($arr_category))
        {
            $arr_category['10000'] = array(
                'id'=>'10000001',
                'name'=>(string)$pamras['base_info']['nns_name'],
                'parent'=>'0',
                'import_id'=>'0',
                'cp_id'=>(string)$pamras['base_info']['nns_cp_id'],
                'parent_import_id'=>'0',
            );
            $pamras['base_info']['nns_id'] = '10000001';
        }
        else
        {
            //是否添加的标示   true 需要新增 | fase 不需要新增
            $flag_is_add = true;
            //是否修改的标示   true 需要修改 | fase 不需要修改 
            $flag_is_edit = false;
            //自身平台的 父级ID
            $str_self_parent_id = '';
            if((string)$pamras['base_info']['nns_parent_id'] === '0')
            {
                foreach ($arr_category as $key=>$val)
                {
                    if((string)$val['parent'] !=0)
                    {
                        continue;
                    }
                    if((string)$pamras['base_info']['nns_name'] === (string)$val['name'])
                    {
                        $flag_is_add  =false;
                        if((string)$val['parent_import_id'] !== (string)$pamras['base_info']['nns_import_parent_category_id'])
                        {
                            $arr_category[$key]['parent_import_id'] = (string)$pamras['base_info']['nns_import_parent_category_id'];
                            $flag_is_edit = true;
                        }
                        if((string)$val['cp_id'] !== (string)$pamras['base_info']['nns_cp_id'])
                        {
                            $arr_category[$key]['cp_id'] = (string)$pamras['base_info']['nns_cp_id'];
                            $flag_is_edit = true;
                        }
                        if((string)$val['import_id'] !== (string)$pamras['base_info']['nns_import_category_id'])
                        {
                            $arr_category[$key]['import_id'] = (string)$pamras['base_info']['nns_import_category_id'];
                            $flag_is_edit = true;
                        }
                        $pamras['base_info']['nns_id'] = (string)$val['id'];
                    }
                    else if((string)$pamras['base_info']['nns_cp_id'] === (string)$val['cp_id'])
                    {
                        $flag_is_add  =false;
                        if((string)$val['parent_import_id'] !== (string)$pamras['base_info']['nns_import_parent_category_id'])
                        {
                            $arr_category[$key]['parent_import_id'] = (string)$pamras['base_info']['nns_import_parent_category_id'];
                            $flag_is_edit = true;
                        }
                        if((string)$val['name'] !== (string)$pamras['base_info']['nns_name'])
                        {
                            $arr_category[$key]['name'] = (string)$pamras['base_info']['nns_name'];
                            $flag_is_edit = true;
                        }
                        if((string)$val['import_id'] !== (string)$pamras['base_info']['nns_import_category_id'])
                        {
                            $arr_category[$key]['import_id'] = (string)$pamras['base_info']['nns_import_category_id'];
                            $flag_is_edit = true;
                        }
                        $pamras['base_info']['nns_id'] = (string)$val['id'];
                    }
                }
                $str_self_parent_id = '0';
            }
            else
            {
                foreach ($arr_category as $key=>$val)
                {
                    if((string)$pamras['base_info']['nns_parent_id'] !== (string)$val['parent'])
                    {
                        $str_self_parent_id = (string)$pamras['base_info']['nns_parent_id'];
                        continue;
                    }
                    $str_self_parent_id = (string)$val['parent'];
                    if((string)$pamras['base_info']['nns_name'] === (string)$val['name'])
                    {
                        $flag_is_add  =false;
                        if((string)$val['parent_import_id'] !== (string)$pamras['base_info']['nns_import_parent_category_id'])
                        {
                            $arr_category[$key]['parent_import_id'] = (string)$pamras['base_info']['nns_import_parent_category_id'];
                            $flag_is_edit = true;
                        }
                        if((string)$val['cp_id'] !== (string)$pamras['base_info']['nns_cp_id'])
                        {
                            $arr_category[$key]['cp_id'] = (string)$pamras['base_info']['nns_cp_id'];
                            $flag_is_edit = true;
                        }
                        if((string)$val['import_id'] !== (string)$pamras['base_info']['nns_import_category_id'])
                        {
                            $arr_category[$key]['import_id'] = (string)$pamras['base_info']['nns_import_category_id'];
                            $flag_is_edit = true;
                        }
                        $pamras['base_info']['nns_id'] = (string)$val['id'];
                    }
                }
            }
            if(strlen($str_self_parent_id) <1)
            {
                return self::return_data('1','未查询到父级注入ID关联信息',$pamras);
            }
            $str_self_parent_id = $str_self_parent_id=='0' ? '10000' : $str_self_parent_id;
            if($flag_is_add)
            {
                $flag_is_surpass = false;
                for ($i=1;$i<1000;$i++)
                {
                    $str_temp_id = $str_self_parent_id.str_pad($i, 3, "0", STR_PAD_LEFT);
                    $str_temp_id = (string)$str_temp_id;
                    if(isset($arr_category[$str_temp_id]))
                    {
                        continue;
                    }
                    else
                    {
                        $arr_category[$str_temp_id] = array(
                            'id'=>(string)$str_temp_id,
                            'name'=>(string)$pamras['base_info']['nns_name'],
                            'parent'=>(string)$str_self_parent_id == '10000' ? 0 : (string)$str_self_parent_id,
                            'import_id'=>(string)$pamras['base_info']['nns_import_category_id'],
                            'cp_id'=>(string)$pamras['base_info']['nns_cp_id'],
                            'parent_import_id'=>(string)$pamras['base_info']['nns_import_parent_category_id'],
                        );
                        $flag_is_surpass = true;
                        $pamras['base_info']['nns_id'] = (string)$str_temp_id;
                        break;
                    }
                }
                if(!$flag_is_surpass)
                {
                    return self::return_data('1','超过了子栏目创建的限制了不再创建栏目');
                }
            }
            elseif(!$flag_is_edit)
            {
                return self::return_data('0','栏目没做任何变化不需要修改','',$pamras);
            }
        }
        $make_category_xml = $this->make_category_xml($arr_category);
        if($make_category_xml['ret'] !=0)
        {
            return $make_category_xml;
        }
        return self::return_data(0,'ok',$make_category_xml['data_info'],$pamras);
    }
    
    
    /**
     * 生产栏目信息
     * @param unknown $str_category
     */
    private function make_category_xml($arr_category)
    {
        $dom = new DOMDocument('1.0', 'utf-8');
        $obj_service = self::make_xml($dom, 'service');
        $arr_category_last = null;
        $arr_object = array(
            '10000'=>$obj_service,
        );
        if(!is_array($arr_category) || empty($arr_category))
        {
            return self::return_data(1,'栏目数组为空');
        }
        foreach ($arr_category as $category_val)
        {
            $length = strlen($category_val['id']);
            $length = (($length-1)/3)-2;
            $arr_category_last[$length][] = $category_val;
        }
        foreach ($arr_category_last as $category_val)
        {
            foreach ($category_val as $_v)
            {
                $str_parent = $_v['parent'] == '0' ? '10000' : $_v['parent'];
                if(!isset($arr_object[$str_parent]))
                {
                    return self::return_data(1,"未查询到父级DOM[{$str_parent}]");
                }
                $temp_obj = self::make_xml($dom,'category',null,$_v,$arr_object[$str_parent]);
                $arr_object[$_v['id']] = $temp_obj;
                unset($temp_obj);
            }
        }
        return self::return_data(0,'ok',$dom->saveXML());
    }
    
    
    
    /**
     * 获取栏目信息
     * @param unknown $str_category
     */
    private function get_cate_info($type)
    {
        $result_carrier = $this->query_carrier();
        if($result_carrier['ret'] !=0)
        {
            return $result_carrier;
        }
        $type = (strlen($type) >0 && in_array($type, array('0','1'))) ? $type : '0';
        $result_depot = nl_depot::query_by_condition(self::$obj_dc,array('nns_org_id'=>$result_carrier['data_info']['nns_id'],'nns_type'=>$type));
        if($result_depot['ret'] !=0)
        {
            return $result_depot;
        }
        if(!isset($result_depot['data_info'][0]) || !is_array($result_depot['data_info'][0]) || empty($result_depot['data_info'][0]))
        {
            return self::return_data(1,'查询栏目信息无信息',$result_depot);
        }
        $str_category = isset($result_depot['data_info'][0]['nns_category']) ? $result_depot['data_info'][0]['nns_category'] : null;
        if(!self::is_xml($str_category))
        {
            return self::return_data(1,'查询的栏目非xml',$str_category);
        }
        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->loadXML($str_category);
        $category_id_arr = null;
        $categorys = $dom->getElementsByTagName('category');
        if($categorys->length >0)
        {
            foreach ($categorys as $category)
            {
                $str_id = $category->getAttribute('id');
                if(strlen($str_id) <1)
                {
                    continue;
                }
                foreach ($this->arr_fileds as $filed_val)
                {
                    $temp_value = $category->getAttribute($filed_val);
                    $temp_value = strlen($temp_value) <1 ? '' : $temp_value;
                    $category_id_arr[$str_id][$filed_val] = $temp_value;
                }
            }
        }
        return self::return_data(0,'ok',$category_id_arr,$result_depot['data_info']['0']);
    }
    
    
    /**
     * 查询运营商 信息
     */
    private function query_carrier()
    {
        $result_carrier = nl_carrier::query_all(m_config::$obj_dc);
        if($result_carrier['ret'] !=0)
        {
            return $result_carrier;
        }
        $str_reason = $result_carrier['reason'];
        $result_carrier = (isset($result_carrier['data_info'][0]) && is_array($result_carrier['data_info'][0]) && !empty($result_carrier['data_info'][0])) ? $result_carrier['data_info'][0] : null;
        if(empty($result_carrier))
        {
            return self::return_data(1,'查询运营商数据 不存在');
        }
        return self::return_data(0,$str_reason,$result_carrier);
    }
}