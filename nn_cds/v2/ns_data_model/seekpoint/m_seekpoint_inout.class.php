<?php
/**
 *
 * @author: jing.chen
 * @date: 2017/12/12 16:19
 */
\ns_core\m_load::load("ns_data_model.m_data_model");
\ns_core\m_load::load_old("nn_logic/seekpoint/seekpoint.class.php");
\ns_core\m_load::load_old("nn_logic/video/vod_index.class.php");
class m_seekpoint_inout  extends m_data_model
{
    /**
     * 删除参数检查模板
     * @var unknown
     */
    public $del_params = array(
        'base_info'     =>      array(
            'nns_import_id'                 =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-128',
                'desc'      => '打点注入id',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => 'CP_ID',
            ),
            'nns_import_source'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '注入来源',
            ),
            'nns_deleted'                   =>      array (
                'rule'      => 'tinyint',
                'default'   => '1',
                'length'    => '',
                'desc'      => '是否删除 0 未删除  |  1删除',
            ),
        ),
    );

    /**
     * 修改参数检查模板
     * @var unknown
     */
    public $add_params = array(
        'base_info'     =>      array(
            'nns_type'             =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '打点类型 0代表点播 1代表直播',
            ),
            'nns_image'             =>      array (
                'rule'      => 'image#seekpoint',
                'default'   => '',
                'length'    => '0-512',
                'desc'      => '图片地址',
            ),
            'nns_begin'             =>      array (
                'rule'      => 'int',
                'default'   => '',
                'length'    => '',
                'desc'      => '开始时间，秒',
            ),
            'nns_end'             =>      array (
                'rule'      => 'int',
                'default'   => '',
                'length'    => '',
                'desc'      => '结束时间，秒',
            ),
            'nns_name'             =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-512',
                'desc'      => '打点信息',
            ),
            'nns_import_id'                 =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-128',
                'desc'      => '上游打点注入ID',
            ),
            'nns_import_source'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '注入来源',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => 'CP_ID',
            ),
            'nns_fragment_type'             =>      array (
                'rule'      => 'int',
                'default'   => '',
                'length'    => '1',
                'desc'      => '分段类型 0代表片中 1代表片头 2代表片尾 3互动',
            ),
        ),
        'hide_info'     =>      array(
            'nns_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => 'GUID',
                'length'    => '32',
                'desc'      => '打点GUID',
            ),
            'nns_video_index'                 =>      array (
                'rule'      => 'int',
                'default'   => '',
                'length'    => '1',
                'desc'      => '分集号',
            ),
            'nns_video_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '影片ID',
            ),
            'nns_vod_index_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '分集GUID',
            ),
            'nns_deleted'                   =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '是否删除 0 未删除  |  1删除',
            ),
        ),
    );


    /**
     * 检查注入参数
     * @param array $in_params 注入参数
     * @param string $action 操作参数行为
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function check_input_params($in_params,$action)
    {
        $notice_params = $last_params = array ();
        $str_params = "{$action}_params";
        if (!isset($this->$str_params))
        {
            return self::return_data(1, '没有设置' . $str_params . '参数的判断模式');
        }
        if (!is_array($this->$str_params))
        {
            return self::return_data(1, '参数非数组');
        }
        return $this->_check_input_params($this->$str_params, $in_params,'message_queue');
    }

    /**
     * 打点模板导出
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function export()
    {

        return self::return_data(0,'ok',array('del_params'=>$this->del_params,'import_params'=>$this->import_params));
    }

    /**
     * 添加打点信息
     * @param array $params 添加数据 不能为空
     * @param  $is_add_queue 是否生成队列 默认生成
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function add($params , $is_add_queue=true)
    {
        if(!isset($params['base_info']['nns_vod_index_id']) || strlen($params['base_info']['nns_vod_index_id']) <1)
        {
            return self::return_data(1,'分集ID为空，直接打回');
        }
        if(!isset($params['base_info']['nns_cp_id']) || strlen($params['base_info']['nns_cp_id']) <1)
        {
            return self::return_data(1,'CP_ID为空，直接打回');
        }
        if(!isset($params['base_info']['nns_import_source']) || strlen($params['base_info']['nns_import_source']) <1)
        {
            return self::return_data(1,'打点信息注入来源为空，直接打回');
        }
        $obj_execute = $params['base_info']['nns_type'] == '1' ? new m_channel_index_inout() :  new m_index_inout();
        $result_vod_index_info = $obj_execute->query(array('base_info'=>array('nns_import_id'=>$params['base_info']['nns_vod_index_id'],'nns_import_source'=>$params['base_info']['nns_import_source'],'nns_cp_id'=>$params['base_info']['nns_cp_id'])));
        if($result_vod_index_info['ret'] !=0)
        {
            return self::return_data(1,'打点检查分集是否存在失败',$result_vod_index_info);
        }
        if(!isset($result_vod_index_info['data_info']) || empty($result_vod_index_info['data_info']) || !is_array($result_vod_index_info['data_info']))
        {
            return self::return_data(1,'打点检查分集是否存在失败，查询无分集数据');
        }
        if(count($result_vod_index_info['data_info']) >1)
        {
            return self::return_data(1,'查询到1个以上相同的分集，不在执行');
        }
        $params['hide_info']['nns_vod_index_id'] = $result_vod_index_info['data_info'][0]['base_info']['nns_id'];
        $params['hide_info']['nns_video_index'] = $result_vod_index_info['data_info'][0]['base_info']['nns_index'];
        $params['hide_info']['nns_video_id'] = $params['base_info']['nns_type'] == '1' ? $result_vod_index_info['data_info'][0]['base_info']['nns_live_id'] :  $result_vod_index_info['data_info'][0]['base_info']['nns_vod_id'];
        //注入打点信息参数检查，模板参考
        $check_input_params = $this->check_input_params($params, 'add');
        if ($check_input_params['ret'] != 0)
        {
            return $check_input_params;
        }
        if (!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(1, '打点信息参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];
        //检查打点信息是否存在
        $arr_seekpoint_exist=array(
            'nns_import_id'=>$params['base_info']['nns_import_id'],
            'nns_import_source'=>$params['base_info']['nns_import_source'],
            'nns_cp_id'=>$params['base_info']['nns_cp_id']
        );
        //队列数组
        $arr_queue = array(
            'base_info' => array(
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
            )
        );
        $result_seekpoint_exist = nl_seekpoint::query_by_condition(m_config::$obj_dc, $arr_seekpoint_exist);
        if ($result_seekpoint_exist['ret'] != 0)
        {
            return $result_seekpoint_exist;
        }
        $nns_action = 'add';
        if(isset($result_seekpoint_exist['data_info']) && is_array($result_seekpoint_exist['data_info']) && !empty($result_seekpoint_exist['data_info']))
        {
            $nns_action = 'modify';
            $count_vod_data = count($result_seekpoint_exist['data_info']);
            if ($count_vod_data > 1)
            {
                return self::return_data(1, "查询打点信息相同条件下数量>{$count_vod_data}原有的注入有问题,{$result_seekpoint_exist['reason']}");
            }
            $this->_check_and_del_input_image($this->add_params, $result_seekpoint_exist['data_info'][0]);
            $result_execute=nl_seekpoint::edit_v2(m_config::$obj_dc, $params['base_info'],$result_seekpoint_exist['data_info'][0]['nns_id']);
            if($result_execute['ret'] !=0)
            {
                return $result_execute;
            }
            $arr_queue['base_info']['nns_id'] = $result_seekpoint_exist['data_info'][0]['nns_id'];
        }
        else
        {
            $result_execute=nl_seekpoint::add_v2(m_config::$obj_dc, $params['base_info']);
            if($result_execute['ret'] !=0)
            {
                return $result_execute;
            }
            $arr_queue['base_info']['nns_id'] = $params['base_info']['nns_id'];
        }

        //入中心注入指令
        if($is_add_queue)
        {
            $this->push_to_center_import($arr_queue, 'seekpoint', 0, $nns_action);
            unset($arr_queue);
        }
        return self::return_data(0,'OK',$check_input_params['data_info']);
    }

    /**
     * 修改打点信息数据
     * @param array $params 修改数据 不能为空
     * @param  $is_add_queue 是否生成队列 默认生成
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function edit($params , $is_add_queue=true)
    {
        return $this->add($params , $is_add_queue);
    }

    /**
     * 删除打点信息数据
     * @param array $params 数据数组，不能为空
     * @param  $is_add_queue 是否生成队列 默认生成
     * @return Ambigous|array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function del($params , $is_add_queue)
    {
        //注入打点参数检查，模板参考
        $check_input_params = $this->check_input_params($params,'del');
        if($check_input_params['ret'] !=0)
        {
            return $check_input_params;
        }
        if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(1,'打点参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];
        //检查未删除打点信息是否存在
        $arr_seekpoint_exist=array(
            'nns_import_id'=>$params['base_info']['nns_import_id'],
            'nns_import_source'=>$params['base_info']['nns_import_source'],
            'nns_cp_id'=>$params['base_info']['nns_cp_id'],
            'nns_deleted'=>0,
        );
        $result_seekpoint_exist = nl_seekpoint::query_by_condition(m_config::$obj_dc, $arr_seekpoint_exist);
        if ($result_seekpoint_exist['ret'] != 0)
        {
            return $result_seekpoint_exist;
        }
        if(isset($result_seekpoint_exist['data_info']) && is_array($result_seekpoint_exist['data_info']) && !empty($result_seekpoint_exist['data_info']))
        {
            $count_vod_data = count($result_seekpoint_exist['data_info']);
            if ($count_vod_data > 1)
            {
                return self::return_data(1, "查询打点信息相同条件下数量>{$count_vod_data}原有的注入有问题,{$result_seekpoint_exist['reason']}");
            }
            $result_execute=nl_seekpoint::edit_v2(m_config::$obj_dc, $params['base_info'],$result_seekpoint_exist['data_info'][0]['nns_id']);
            if($result_execute===false)
            {
                return false;
            }
        }
        else{
            return self::return_data(1,"没有需要删除的数据，{$result_seekpoint_exist['reason']}");
        }
        //队列数组
        $arr_queue = array(
            'base_info' => array(
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
                'nns_id'=> $result_seekpoint_exist['data_info'][0]['nns_id'],
            )
        );
        //入中心注入指令
        if($is_add_queue)
        {
            $this->push_to_center_import($arr_queue, 'seekpoint', 0, 'destroy');
            unset($arr_queue);
        }
        return self::return_data(0,'ok',$check_input_params['data_info']);
    }

    /**
     * 查询打点信息数据
     * @param array $array_query 条件数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function query($array_query, $is_need_ext = false, $epg_model = '', $cdn_model = '')
    {
        $result_seekpoint = nl_seekpoint::query_by_condition(m_config::$obj_dc, $array_query['base_info']);
        if(!is_array($result_seekpoint))
        {
            return self::return_data(1,"没有需要查询的数据");
        }
        foreach ($result_seekpoint['data_info'] as $data_info)
        {
            $temp_epg_data = $temp_cdn_data = $temp_ex_data = null;
            if ($is_need_ext)
            {
                #TODO
            }
            if (strlen($epg_model) >0)
            {
                #TODO
            }
            if (strlen($cdn_model) >0)
            {
                #TODO
            }
            $last_data[] = array(
                'base_info' => $data_info,
                'ex_info' => $temp_ex_data,
                'epg_info' => $temp_epg_data,
                'cdn_info' => $temp_cdn_data,
            );
        }
        return self::return_data(0,'OK',$last_data);
    }

}