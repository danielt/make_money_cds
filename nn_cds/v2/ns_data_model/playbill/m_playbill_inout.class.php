<?php

/**
 * @description:栏目模板
 * @author:xinxin.deng
 * @date: 2017/12/7 11:39
 */
\ns_core\m_load::load("ns_data_model.m_data_model");
\ns_core\m_load::load_old("nn_logic/playbill/playbill_item.class.php");
\ns_core\m_load::load_old("nn_logic/depot/depot.class.php");
\ns_core\m_load::load_old("nn_logic/carrier/carrier.class.php");

class m_playbill_inout  extends m_data_model
{
    /**
     * 删除模板数据
     * @var array
     */
    public $del_params = array(
        'base_info'     =>      array(
            'nns_playbill_import_id'           =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-128',
                'desc'      => '节目单注入id',
            ),
            'nns_import_source'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-32',
                'desc'      => '注入来源',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => 'CP_ID',
            ),
        ),
    );

    /**
     * 添加/修改模板数据
     * @var array
     */
    public $add_params = array(
        'base_info'     =>      array(
            'nns_name'                      =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '',
                'desc'      => '节目单时间',
            ),
            'nns_begin_time'                =>      array (
                'rule'      => 'datetime',
                'default'   => '',
                'length'     => '',
                'desc'      => '节目开始时间',
            ),
            'nns_begin_data'                =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '',
                'desc'      => '节目开始日期（具体哪一天）',
            ),
            'nns_end_time'                  =>      array (
                'rule'      => 'datetime',
                'default'   => '',
                'length'     => '',
                'desc'      => '节目开始时间',
            ),
            'nns_time_len'                  =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'     => '',
                'desc'      => '节目单播放时长',
            ),
            'nns_summary'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-1024',
                'desc'      => '节目单简介',
            ),
            'nns_image0'                    =>      array (
                'rule'      => 'image#playbill',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '大图',
            ),
            'nns_image1'                    =>      array (
                'rule'      => 'image#playbill',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '中图',
            ),
            'nns_image2'                    =>      array (
                'rule'      => 'image#playbill',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '小图',
            ),
            'nns_image3'                    =>      array (
                'rule'      => 'image#playbill',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '横图',
            ),
            'nns_image4'                    =>      array (
                'rule'      => 'image#playbill',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '竖图',
            ),
            'nns_image5'                    =>      array (
                'rule'      => 'image#playbill',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '方图',
            ),
            'nns_pinyin'                    =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-128',
                'desc'      => '拼音',
            ),
            'nns_pinyin_length'             =>      array (
                'rule'      => 'int',
                'default'   => '',
                'length'     => '0-3',
                'desc'      => '拼音长度',
            ),
            'nns_eng_name'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '英文名字',
            ),
            'nns_keyword'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '关键词',
            ),
            'nns_kind'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '节目类型:如 战争 综艺等',
            ),
            'nns_image6'                    =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '0-扩展2',
            ),
            'nns_actor'                     =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-255',
                'desc'      => '演员，以/分隔',
            ),
            'nns_director'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-255',
                'desc'      => '作者，导演，以/分隔',
            ),
            'nns_playbill_import_id'        =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => '节目单注入id',
            ),
//            'nns_live_media_import_id'      =>      array (
//                'rule'      => 'noempty',
//                'default'   => '',
//                'length'     => '1-32',
//                'desc'      => '直播片源注入id',
//            ),
//            'nns_live_import_id'      =>      array (
//                'rule'      => 'noempty',
//                'default'   => '',
//                'length'     => '1-32',
//                'desc'      => '直播频道注入id',
//            ),
             'nns_live_id'      =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => '直播频道nns_id',
            ),
            'nns_domain'                    =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-12',
                'desc'      => '',
            ),
            'nns_hot_dgree'                 =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '-012',
                'desc'      => '',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => 'CP ID',
            ),
            'nns_import_source'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '',
                'desc'      => '注入来源',
            ),
            'nns_ext_url'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-2018',
                'desc'      => '上游方注入url 和其他信息 json存储',
            ),
            'nns_image_v'                   =>      array (
                'rule'      => 'image#playbill',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '竖图',
            ),
            'nns_image_h'                   =>      array (
                'rule'      => 'image#playbill',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '横图',
            ),
            'nns_image_s'                   =>      array (
                'rule'      => 'image#playbill',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '方图',
            ),
        ),
        'ext_info'      =>      array(
            'nns_key'                      =>      array(
                'rule'      => '',
                'default'   => '',
                'length'    => '',
                'desc'      => '键',
            ),
            'nns_value'                    =>      array(
                'rule'      => '',
                'default'   => '',
                'length'    => '',
                'desc'      => '值',
            ),
        ),
        'hide_info'     =>      array(
            'nns_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => 'GUID',
                'length'     => '32',
                'desc'      => '节目单GUID',
            ),
//            'nns_live_media_id'             =>      array (
//                'rule'      => 'noempty',
//                'default'   => 'parent|live_media_action|base_info+unset+nns_live_media_import_id+nns_content_id#base_info+set+nns_cp_id+nns_cp_id#base_info+set+nns_import_source+nns_live_media_id|base_info+nns_id',
//                'length'     => '1-32',
//                'desc'      => '直播片源id',
//            ),
//            'nns_live_id'                   =>      array (
//                'rule'      => 'noempty',
//                'default'   => 'parent|live_action|base_info+unset+nns_live_import_id+nns_import_id#base_info+set+nns_cp_id+nns_cp_id#base_info+set+nns_import_source+nns_live_import_id|base_info+nns_id',
//                'length'     => '1-32',
//                'desc'      => '直播ID',
//            ),

            'nns_original_id'               =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '运营商ID',
            ),
            'nns_depot_id'                  =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '点播、直播栏目GUID',
            ),
            'nns_state'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1',
                'desc'      => '是否禁播，0不禁播，1禁播',
            ),
        )
    );

    /**
     * 导出模板数据
     * @var array
     */
    public $import_params = array(
        'base_info'     =>      array(
            'nns_begin_time'                =>      array (
                'rule'      => 'date',
                'default'   => '',
                'length'     => '',
                'desc'      => '节目开始时间',
            ),
            'nns_end_time'                  =>      array (
                'rule'      => 'date',
                'default'   => '',
                'length'     => '',
                'desc'      => '节目开始时间',
            ),
            'nns_time_len'                  =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'     => '',
                'desc'      => '节目单播放时长',
            ),
            'nns_create_time'               =>      array (
                'rule'      => 'date',
                'default'   => '',
                'length'     => '',
                'desc'      => '创建时间',
            ),
            'nns_summary'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-1024',
                'desc'      => '节目单简介',
            ),
            'nns_image0'                    =>      array (
                'rule'      => 'image#playbill',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '大图',
            ),
            'nns_image1'                    =>      array (
                'rule'      => 'image#playbill',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '中图',
            ),
            'nns_image2'                    =>      array (
                'rule'      => 'image#playbill',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '小图',
            ),
            'nns_image3'                    =>      array (
                'rule'      => 'image#playbill',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '横图',
            ),
            'nns_image4'                    =>      array (
                'rule'      => 'image#playbill',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '竖图',
            ),
            'nns_image5'                    =>      array (
                'rule'      => 'image#playbill',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '方图',
            ),
            'nns_pinyin'                    =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-128',
                'desc'      => '拼音',
            ),
            'nns_pinyin_length'             =>      array (
                'rule'      => 'int',
                'default'   => '',
                'length'     => '0-3',
                'desc'      => '拼音长度',
            ),
            'nns_eng_name'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '英文名字',
            ),
            'nns_keyword'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '关键词',
            ),
            'nns_kind'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '节目类型:如 战争 综艺等',
            ),
            'nns_image6'                    =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '0-扩展2',
            ),
            'nns_actor'                     =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-255',
                'desc'      => '演员，以/分隔',
            ),
            'nns_director'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-255',
                'desc'      => '作者，导演，以/分隔',
            ),
            'nns_playbill_import_id'        =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => '节目单注入id',
            ),
            'nns_domain'                    =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-12',
                'desc'      => '',
            ),
            'nns_hot_dgree'                 =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '-012',
                'desc'      => '',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => 'CP ID',
            ),
            'nns_integer_id'                =>      array (
                'rule'      => 'int',
                'default'   => '',
                'length'     => '1-10',
                'desc'      => '自增ID',
            ),
            'nns_modify_time'               =>      array (
                'rule'      => 'date',
                'default'   => '',
                'length'     => '',
                'desc'      => '修改时间',
            ),
            'nns_import_source'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '',
                'desc'      => '注入来源',
            ),
            'nns_ext_url'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-2018',
                'desc'      => '上游方注入url 和其他信息 json存储',
            ),
            'nns_image_v'                   =>      array (
                'rule'      => 'image#playbill',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '竖图',
            ),
            'nns_image_h'                   =>      array (
                'rule'      => 'image#playbill',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '横图',
            ),
            'nns_image_s'                   =>      array (
                'rule'      => 'image#playbill',
                'default'   => '',
                'length'     => '0-512',
                'desc'      => '方图',
            ),
        ),
        'hide_info'     =>      array(
            'nns_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '32',
                'desc'      => '节目单GUID',
            ),
            'nns_live_id'                   =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => '直播ID',
            ),
            'nns_live_media_id'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => '直播片源id',
            ),
            'nns_original_id'               =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '运营商ID',
            ),
            'nns_depot_id'                  =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '点播、直播栏目GUID',
            ),
            'nns_state'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1',
                'desc'      => '是否禁播，0不禁播，1禁播',
            ),
        )
    );

    public $arr_notice_params = null;

    /**
     * 检查注入参数
     * @param array $in_params 注入参数
     * @param string $action 注入参数
     * @return multitype|array array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function check_input_params($in_params, $action)
    {
        $str_params = "{$action}_params";
        if (!isset($this->$str_params))
        {
            return self::return_data(1, '没有设置' . $str_params . '参数的判断模式');
        }
        if (!is_array($this->$str_params))
        {
            return self::return_data(1, '参数非数组');
        }
        return $this->_check_input_params($this->$str_params, $in_params, 'message_queue');
    }

    /**
     * 节目单模板导出
     * @return multitype|array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function export()
    {
        return self::return_data(0, 'ok', array('del_params'=>$this->del_params, 'import_params'=>$this->import_params));
    }
    /**
     * 添加节目单
     * @param $params
     * @return multitype|array
     */
    public function add($params)
    {
        $result_carrier = $this->query_carrier();
        if($result_carrier['ret'] != 0)
        {
            return $result_carrier;
        }
        if(!isset($result_carrier['data_info']) || !is_array($result_carrier['data_info']) || empty($result_carrier['data_info']))
        {
            return self::return_data(1, "获取运营商信息为空");
        }
        $result_deport = $this->query_deport($result_carrier['data_info']['nns_id']);
        if($result_deport['ret'] != 0)
        {
            return $result_deport;
        }
        if(!isset($result_deport['data_info']) || !is_array($result_deport['data_info']) || empty($result_deport['data_info']))
        {
            return self::return_data(1, "获取栏目信息为空");
        }
        $params['hide_info']['nns_original_id'] = $result_carrier['data_info']['nns_id'];
        $params['hide_info']['nns_depot_id'] = $result_deport['data_info']['nns_id'];

        //注入频道参数检查，模板参考
        $check_input_params = $this->check_input_params($params, 'add');

        if($check_input_params['ret'] != 0)
        {
            return $check_input_params;
        }
        if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(1, '节目单参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];

        #TODO CP配置逻辑
        //检查是否存在此直播频道
        $arr_query = array(
            'nns_playbill_import_id' => $params['base_info']['nns_playbill_import_id'],
            'nns_import_source' => $params['base_info']['nns_import_source'],
            'nns_cp_id' => $params['base_info']['nns_cp_id'],
        );
        $result_playbill_exist = nl_playbill_item::query_by_condition(m_config::get_dc(), $arr_query);

        if($result_playbill_exist['ret'] != 0)
        {
            return $result_playbill_exist;
        }

        //队列数组
        $arr_queue = array(
            'base_info' => array(
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
            )
        );

        //存在则更新
        if(isset($result_playbill_exist['data_info']) && is_array($result_playbill_exist['data_info']) && !empty($result_playbill_exist['data_info']))
        {
            $count_vod_data = count($result_playbill_exist['data_info']);
            if($count_vod_data > 1)
            {
                return self::return_data(1, "查询直播频道相同条件下数量>{$count_vod_data}原有的注入有问题,{$result_playbill_exist['reason']}");
            }
            //$this->_check_and_del_input_image($this->add_params, $result_playbill_exist['data_info'][0], 'message_queue');
            $check_input_params['data_info']['info']['out']['base_info']['nns_action'] = $params['base_info']['nns_action'] = 'modify';
            $check_input_params['data_info']['info']['out']['base_info']['nns_deleted'] = $params['base_info']['nns_deleted'] = '0';
            $check_input_params['data_info']['info']['out']['base_info']['nns_status'] = $params['base_info']['nns_status'] = '2';
            unset($params['base_info']['nns_id']);
            $result_execute = nl_playbill_item::edit(m_config::get_dc(),$params['base_info'], $result_playbill_exist['data_info'][0]['nns_id']);

            if($result_execute['ret'] != 0)
            {
                return $result_execute;
            }
            $arr_queue['base_info']['nns_id'] = $result_playbill_exist['data_info'][0]['nns_id'];
        }
        else
        {
            //不存在则添加
            $check_input_params['data_info']['info']['out']['base_info']['nns_action'] = $params['base_info']['nns_action'] = 'add';
            $check_input_params['data_info']['info']['out']['base_info']['nns_deleted'] = $params['base_info']['nns_deleted'] = '0';
            $check_input_params['data_info']['info']['out']['base_info']['nns_status'] = $params['base_info']['nns_status'] = '1';
            $result_execute = nl_playbill_item::add(m_config::get_dc(), $params['base_info']);
            if($result_execute['ret'] != 0)
            {
                return $result_execute;
            }
            $arr_queue['base_info']['nns_id'] = $params['base_info']['nns_id'];
        }

        //入中心注入指令
        $re_push = $this->push_to_center_import($arr_queue, 'playbill', 0, $params['base_info']['nns_action']);
        $this->write_message_info($re_push);
        unset($arr_queue);
        return self::return_data(0, 'OK', $check_input_params['data_info']);
    }

    /**
     * 修改节目单
     * @param $params
     * @return multitype|array
     */
    public function edit($params)
    {
        return self::add($params);
    }
    /**
     * 删除节目单数据
     * @param array $params 数据数组，不能为空
     * @return multitype|array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function del($params)
    {
        //注入主媒资参数检查，模板参考
        $check_input_params = $this->check_input_params($params, 'del');

        if($check_input_params['ret'] != 0)
        {
            return $check_input_params;
        }
        if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(1, '节目单参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];

        #TODO CP配置逻辑

        //检查是否存在此节目单
        $result_playbill_exist = nl_playbill_item::query_import_info(m_config::$obj_dc, array($params['base_info']['nns_playbill_import_id']),$params['base_info']['nns_cp_id'], $params['base_info']['nns_import_source']);

        if($result_playbill_exist['ret'] != 0)
        {
            return $result_playbill_exist;
        }
        $arr_del = null;
        if(isset($result_playbill_exist['data_info']) && is_array($result_playbill_exist['data_info']) && !empty($result_playbill_exist['data_info']))
        {
            foreach ($result_playbill_exist['data_info'] as $data_info)
            {
                $arr_del[] = $data_info['nns_id'];
            }
        }
        if(empty($arr_del) || !is_array($arr_del))
        {
            return self::return_data(1, "没有需要删除的数据，{$result_playbill_exist['reason']}");
        }
//        $result_execute = nl_playbill_item::del(m_config::$obj_dc, array('nns_id' => $arr_del));
        foreach ($arr_del as $v)
        {
            $result_execute = nl_playbill_item::modify_playbill_item_v2(m_config::$obj_dc, $v,array('nns_deleted' => 1));
            if($result_execute['ret'] != 0)
            {
                return $result_execute;
            }
        }
        //放入队列
        $arr_queue = array(
            'base_info' => array(
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
            ),
        );
        foreach ($arr_del as $del_val)
        {
            $arr_queue['base_info']['nns_id'] = $del_val;
            $re = $this->push_to_center_import($arr_queue, 'playbill', 0, 'destroy');
        }
        return $result_execute;
    }

    /**
     * 查询直播频道数据
     * @param array $array_query 条件数据 不能为空
     * @param boolean $is_need_ext false 不需要查询扩展数据 | true 需要查询扩展数据
     * @param boolean $is_need_epg_import_id  false 不需要查询EPG扩展数据 | true 需要查询EPG扩展数据
     * @param boolean $is_need_cdn_import_id  false 不需要查询CDN扩展数据 | true 需要查询CDN扩展数据
     * @return multitype|array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function query($array_query, $is_need_ext = false, $is_need_epg_import_id = false, $is_need_cdn_import_id = false)
    {
        $last_data = null;
        $result_playbill_item = nl_playbill_item::query_by_condition(m_config::$obj_dc, $array_query['base_info']);

        if($result_playbill_item['ret'] != 0 || !isset($result_playbill_item['data_info']) || !is_array($result_playbill_item['data_info']) || empty($result_playbill_item['data_info']))
        {
            return $result_playbill_item;
        }
        foreach ($result_playbill_item['data_info'] as $data_info)
        {
            $temp_epg_data = $temp_cdn_data = $temp_ex_data = null;
            if($is_need_ext)
            {
                #TODO 预留扩展信息
            }
            if($is_need_epg_import_id)
            {
                #TODO
            }
            if($is_need_cdn_import_id)
            {
                #TODO
            }
            $last_data[] = array(
                'base_info' => $data_info,
                'ex_info' => $temp_ex_data,
                'epg_info' => $temp_epg_data,
                'cdn_info' => $temp_cdn_data,
            );
        }
        return self::return_data(0, 'OK', $last_data);
    }

    /**
     * 查询运营商 信息
     */
    private function query_carrier()
    {
        $result_carrier = nl_carrier::query_all(m_config::$obj_dc);
        if($result_carrier['ret'] != 0)
        {
            return $result_carrier;
        }
        $result_carrier = (isset($result_carrier['data_info'][0]) && is_array($result_carrier['data_info'][0]) && !empty($result_carrier['data_info'][0])) ? $result_carrier['data_info'][0] : null;
        if(empty($result_carrier))
        {
            return self::return_data(1, '查询运营商数据 不存在');
        }
        return self::return_data(0, $result_carrier['reason'], $result_carrier);
    }

    /**
     * 查询栏目信息
     */
    private function query_deport($carrier_id)
    {
        $result_depot = nl_depot::query_by_condition(self::$obj_dc, array('nns_org_id'=>$carrier_id, 'nns_type'=>1));
        if($result_depot['ret'] !=0)
        {
            return $result_depot;
        }
        $result_depot = (isset($result_depot['data_info'][0]) && is_array($result_depot['data_info'][0]) && !empty($result_depot['data_info'][0])) ? $result_depot['data_info'][0] : null;
        if(empty($result_depot))
        {
            return self::return_data(1, '查询栏目数据 不存在');
        }
        return self::return_data(0, $result_depot['reason'], $result_depot);
    }
}