<?php
\ns_core\m_load::load("ns_data_model.m_data_model");
\ns_core\m_load::load_old("nn_logic/message/nl_message.class.php");
class m_message_inout  extends m_data_model
{
    /**
     * 修改参数检查模板
     * @var unknown
     */
    public $add_params  = array(
        'base_info'     =>      array(
            'nns_message_time'              =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-20',
                'desc'      => '消息时间年月日+时间戳',
            ),
            'nns_message_id'                =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '1-64',
                'desc'      => '主媒资状态',
            ),
            'nns_message_xml'               =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '',
                'desc'      => '上游下发的原始内容',
            ),
            'nns_message_content'           =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '',
                'desc'      => '上游下发的需要解析的内容信息',
            ),
            'nns_message_state'             =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '消息队列状态:0等待注入1ftp链接失败2ftp下载失败3注入成功4注入失败',
            ),
            'nns_action'                    =>      array (
                'rule'      => 'tinyint',
                'default'   => '1',
                'length'    => '',
                'desc'      => '操作行为(1 添加 | 2 修改 | 3 删除 | 4 上线 | 5 下线 | 6 栏目同步 | 7 删除栏目 | 8 同步推荐 | 9 取消推荐 | 10 同步CP | 11 同步栏目图片 | 12 删除栏目图片)',
            ),
            'nns_type'                      =>      array (
                'rule'      => 'tinyint',
                'default'   => '1',
                'length'    => '',
                'desc'      => '媒资类型(1 主媒资 | 2 分集 | 3 片源 | 4 直播频道 | 5 直播分集 | 6 直播源 | 7 节目单 | 8 文件包 | 9 打点信息)',
            ),
            'nns_name'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-256',
                'desc'      => '注入消息队列媒资名称',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '0',
                'length'    => '1-32',
                'desc'      => 'cp_id',
            ),
            'nns_package_id'                =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-32',
                'desc'      => '上游package包ID',
            ),
            'nns_xmlurlqc'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-32',
                'desc'      => '广州电信悦ME MD5摘要',
            ),
            'nns_encrypt'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-2048',
                'desc'      => '广州电信悦ME 加密串',
            ),
            'nns_content_number'            =>      array (
                'rule'      => 'smallint',
                'default'   => '1',
                'length'    => '',
                'desc'      => 'xml文件中包含的内容数量',
            ),
        ),
        'hide_info'     =>      array(
            'nns_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => 'GUID',
                'length'    => '32',
                'desc'      => '消息队列GUID',
            ),
            'nns_message_original_url'      =>      array (
                'rule'      => '',
                'default'   => 'original|base_info|nns_message_xml',
                'length'    => '0-255',
                'desc'      => '上游下发的原始URL 服务器存储的相对路径',
            ),
            'nns_message_url'               =>      array (
                'rule'      => '',
                'default'   => 'content|base_info|nns_message_content',
                'length'    => '0-255',
                'desc'      => '下发的内容xml相对路径',
            ),
            'nns_bk_queue_excute_url'       =>      array (
                'rule'      => '',
                'default'   => 'message_queue|log_url',
                'length'    => '0-255',
                'desc'      => '消息队列执行的结果url 所有情况',
            ),
            'nns_again'                     =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '失败后重试次数,默认10次',
            ),
            'nns_delete'                    =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '0为未删除，1为删除',
            ),
            'nns_fail_time'                 =>      array (
                'rule'      => '',
                'default'   => '0',
                'length'    => '0-20',
                'desc'      => '失败时间年月日+时间戳',
            ),
        ),
    );
    /**
     * 检查注入参数
     * @param array $in_params 注入参数
     * @param string $action 操作参数行为
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function check_input_params($in_params,$action)
    {
        $notice_params = $last_params = array ();
        $str_params = "{$action}_params";
        if (!isset($this->$str_params))
        {
            return self::return_data(1, '没有设置' . $str_params . '参数的判断模式');
        }
        if (!is_array($this->$str_params))
        {
            return self::return_data(1, '参数非数组');
        }
        return $this->_check_input_params($this->$str_params, $in_params);
    }
    
    /**
     * 主媒资模板导出
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function export()
    {
        
        return self::return_data(0,'ok',array('add_params'=>$this->add_params));
    }
    
    /**
     * 添加主媒资数据
     * @param array $params 添加数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function add($params)
    {
        //注入主媒资参数检查，模板参考
        $check_input_params = $this->check_input_params($params,'add');
        if($check_input_params['ret'] !=0)
        {
            return $check_input_params;
        }
        if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return $this->_return_data(1,'主媒资参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];
        $result_excute = nl_message::add(m_config::get_dc(), $params['base_info'],1);
        if($result_excute['ret'] !=0)
        {
            return self::return_data(1,'消息队列入库数据库执行失败',$check_input_params['data_info'],$result_excute['reason']);
        }
        else
        {
            return self::return_data(0,'ok',$check_input_params['data_info'],$result_excute['reason']);
        }
    }
    
    /**
     * 查询主媒资数据
     * @param array $array_query 条件数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function query($array_query)
    {
        $last_data = null;
        $result_vod = nl_message::query_by_condition(m_config::get_dc(), $array_query);
        if($result_vod['ret'] !=0 || !isset($result_vod['data_info']) || !is_array($result_vod['data_info']) || empty($result_vod['data_info']))
        {
            return $result_vod;
        }
        foreach ($result_vod['data_info'] as $data_info)
        {
            $last_data[]=array(
                'base_info'=>$data_info,
            );
        }
        return self::return_data(0,'OK',$last_data);
    }
}