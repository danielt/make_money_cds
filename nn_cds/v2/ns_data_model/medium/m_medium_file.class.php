<?php
/**
 * Created by PhpStorm.
 * User: kan.yang@starcor.cn
 * Date: 18-3-22
 * Time: 下午1:50
 */
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_data_model.m_data_model");
\ns_core\m_load::load_old("nn_logic/medium/medium_library_file_logic.class.php");
class m_medium_file extends m_data_model
{
    public static $obj_dc = null;

    /**
     * 默认构造函数
     */
    public function __construct()
    {
        self::$obj_dc = m_config::get_dc();
    }

    /**
     * 添加介质库文件列表
     */
    public function add($arr_params)
    {
        return medium_library_file_logic::add(self::$obj_dc,$arr_params);
    }

    /**
     * 更新介质库文件列表
     */
    public function edit($arr_update_where,$arr_update_params)
    {
        return medium_library_file_logic::edit(self::$obj_dc,$arr_update_where,$arr_update_params);
    }

    /**
     * 删除介质库文件
     */
    public function del($arr_del_where,$bool_sys_where = true)
    {
        return medium_library_file_logic::del(self::$obj_dc,$arr_del_where,$bool_sys_where);
    }

    /**
     * 查询介质库文件列表
     */
    public function query_list($arr_query_params,$arr_limit = array(0,10),$str_order = 'nns_modify_time desc',$str_group = '',$bool_sys_where = true)
    {
        return medium_library_file_logic::query_list(self::$obj_dc,$arr_query_params,$arr_limit,$str_order,$str_group,$bool_sys_where);
    }
    
    /**
     * 查询介质库文件列表
     */
    public function query_sql($str_sql)
    {
        return medium_library_file_logic::query_sql(self::$obj_dc,$str_sql);
    }
    /**
     * 查询介质库文件列表
     */
    public function query_export($arr_query_params)
    {
        return medium_library_file_logic::query_export(self::$obj_dc,$arr_query_params);
    }

    /**
     * 查询介质库文件详情
     */
    public function query_one($arr_query_params,$bool_sys_where = true)
    {
        return medium_library_file_logic::query_one(self::$obj_dc,$arr_query_params,$bool_sys_where);
    }

    /**
     * 查询介质库文件详情
     */
    public function analysis($arr_query_params)
    {
        //引入文件
        include_once dirname(dirname(dirname(__FILE__))) . '/ns_timer/medium/medium_mediainfo.timer.php';
        $obj_medium_mediainfo = new medium_mediainfo();
        $bool_ret = $obj_medium_mediainfo->init($arr_query_params);
        return array('ret' => $bool_ret ? 0 : 1,'reason' => '解析文件失败');
    }
} 