<?php
/**
 * 明星库数据模板
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2017/12/13 15:17
 */
\ns_core\m_load::load("ns_data_model.m_data_model");
\ns_core\m_load::load_old("nn_logic/actor/actor.class.php");
\ns_core\m_load::load_old("nn_logic/depot/depot.class.php");
\ns_core\m_load::load_old("nn_logic/carrier/carrier.class.php");
class m_actor_inout  extends m_data_model
{
    /**
     * 删除参数模板
     * @var array
     */
    public $del_params = array(
        'base_info'     =>      array(
            'nns_import_id'           =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => '注入id',
            ),
            'nns_import_source'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => '注入来源',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => 'CP_ID',
            ),
        ),
    );

    /**
     * 添加修改参数模板
     * @var array
     */
    public $add_params = array(
        'base_info'     =>      array(
            'nns_name'                      =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '0-255',
                'desc'      => '片源名称',
            ),
            'nns_import_id'                 =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-32',
                'desc'      => '片源媒体id',
            ),
            'nns_import_source'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => '注入来源',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => 'CP_ID',
            ),
            'nns_area'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-128',
                'desc'      => '地区',
            ),
            'nns_alias_name'                =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '别名',
            ),
            'nns_profession'                =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '职业',
            ),
            'nns_pinyin'                    =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '拼音',
            ),
            'nns_works'                     =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-1024',
                'desc'      => '代表作',
            ),
            'nns_info'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-4096',
                'desc'      => 'JSON描述额外信息字符串',
            ),
            'nns_label_id'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '绑定的标签id，多个以逗号隔开',
            ),
            'nns_image_v'                   =>      array (
                'rule'      => 'image#actor',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '竖图',
            ),
            'nns_image_s'                   =>      array (
                'rule'      => 'image#actor',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '方图',
            ),
            'nns_image_h'                   =>      array (
                'rule'      => 'image#actor',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '横图',
            ),
            'nns_old_name'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '曾用名',
            ),
            'nns_english_name'              =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '英文名',
            ),
            'nns_country'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '',
                'desc'      => '国家',
            ),
            'nns_full_pinyin'               =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '全拼',
            ),
            'nns_sex'                       =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-32',
                'desc'      => '性别',
            ),
        ),
        'ext_info'      =>      array(),
        'hide_info'     =>      array(
            'nns_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => 'GUID',
                'length'    => '32',
                'desc'      => '片源GUID',
            ),
            'nns_bind_id'                   =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '32',
                'desc'      => '绑定资源id',
            ),
            'nns_type'             =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '类型，0点播|1直',
            ),
            'nns_source_type'                =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '绑定资源类型，0主媒资/直播频道|1分集',
            ),
            'nns_original_id'               =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-64',
                'desc'      => '直播原始id',
            ),
        ),
    );

    /**
     * 导出参数模板
     * @var array
     */
    public $import_params = array(
        'base_info'     =>      array(
            'nns_name'                      =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '0-255',
                'desc'      => '片源名称',
            ),
            'nns_import_id'                 =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-32',
                'desc'      => '片源媒体id',
            ),
            'nns_import_source'             =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => '注入来源',
            ),
            'nns_cp_id'                     =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => 'CP_ID',
            ),
            'nns_area'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-128',
                'desc'      => '地区',
            ),
            'nns_alias_name'                =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '别名',
            ),
            'nns_profession'                =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '职业',
            ),
            'nns_pinyin'                    =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '拼音',
            ),
            'nns_works'                     =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-1024',
                'desc'      => '代表作',
            ),
            'nns_info'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-4096',
                'desc'      => 'JSON描述额外信息字符串',
            ),
            'nns_label_id'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '绑定的标签id，多个以逗号隔开',
            ),
            'nns_image_v'                   =>      array (
                'rule'      => 'image#actor',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '竖图',
            ),
            'nns_image_s'                   =>      array (
                'rule'      => 'image#actor',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '方图',
            ),
            'nns_image_h'                   =>      array (
                'rule'      => 'image#actor',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '横图',
            ),
            'nns_create_time'               =>      array (
                'rule'      => 'datetime',
                'default'   => '',
                'length'     => '',
                'desc'      => '创建时间',
            ),
            'nns_modify_time'               =>      array (
                'rule'      => 'datetime',
                'default'   => '',
                'length'     => '',
                'desc'      => '修改时间',
            ),
            'nns_old_name'                  =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '曾用名',
            ),
            'nns_english_name'              =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '英文名',
            ),
            'nns_country'                   =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '',
                'desc'      => '片源服务类型',
            ),
            'nns_full_pinyin'               =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-256',
                'desc'      => '全拼',
            ),
            'nns_sex'                       =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-32',
                'desc'      => '性别',
            ),
        ),
        'hide_info'     =>      array(
            'nns_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => 'GUID',
                'length'    => '32',
                'desc'      => '片源GUID',
            ),
            'nns_bind_id'                   =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '32',
                'desc'      => '绑定资源id',
            ),
            'nns_type'             =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '类型，0点播|1直',
            ),
            'nns_source_type'                =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'     => '',
                'desc'      => '绑定资源类型，0主媒资/直播频道|1分集',
            ),
            'nns_original_id'               =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-64',
                'desc'      => '直播原始id',
            ),
        ),
    );

    public $arr_notice_params = null;

    /**
     * 注入类型
     * @var string
     */
    public $queue_type = 'media';

    /**
     * 检查注入参数
     * @param array $in_params 注入参数
     * @param string $action 操作参数行为
     * @return multitype|array array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public function check_input_params($in_params,$action)
    {
        $notice_params = $last_params = array ();
        $str_params = "{$action}_params";
        if (!isset($this->$str_params))
        {
            return self::return_data(1, '没有设置' . $str_params . '参数的判断模式');
        }
        if (!is_array($this->$str_params))
        {
            return self::return_data(1, '参数非数组');
        }
        return $this->_check_input_params($this->$str_params, $in_params,'message_queue');
    }

    /**
     * 分集模板导出
     * @return multitype|array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function export()
    {

        return self::return_data(0,'ok',array('del_params'=>$this->del_params,'import_params'=>$this->import_params));
    }

    /**
     * 添加明星数据
     * @param array $params 添加数据 不能为空
     * @return multitype|array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function add($params)
    {
        if(!isset($params['base_info']['nns_import_id']) || strlen($params['base_info']['nns_import_id']) < 1)
        {
            return self::return_data(1, '明星操作,明星注入ID为空，直接打回');
        }
        if(!isset($params['base_info']['nns_cp_id']) || strlen($params['base_info']['nns_cp_id']) < 1)
        {
            return self::return_data(1, '明星操作,明星CP_ID为空，直接打回');
        }
        if(!isset($params['base_info']['nns_import_source']) || strlen($params['base_info']['nns_import_source']) < 1)
        {
            return self::return_data(1, '明星操作,明星注入来源为空，直接打回');
        }
        #TODO CP配置逻辑
        $arr_actor_exist = array(
            'nns_import_id'=>$params['base_info']['nns_import_id'],
            'nns_import_source'=>$params['base_info']['nns_import_source'],
            'nns_cp_id'=>$params['base_info']['nns_cp_id'],
        );
        $result_actor_exist = nl_actor::query_by_condition(m_config::get_dc(), $arr_actor_exist);
        if($result_actor_exist['ret'] != 0)
        {
            return $result_actor_exist;
        }
        //队列数组
        $arr_queue = array(
            'base_info' => array(
                'nns_cp_id' => $params['base_info']['nns_cp_id'],
            )
        );
        if(isset($result_actor_exist['data_info']) && is_array($result_actor_exist['data_info']) && !empty($result_media_exist['data_info']))
        {
            $check_input_params['data_info']['info']['out']['base_info']['nns_action'] = $params['base_info']['nns_action'] = 'modify';
            $check_input_params['data_info']['info']['out']['base_info']['nns_deleted'] = $params['base_info']['nns_deleted'] = '0';
            $check_input_params['data_info']['info']['out']['base_info']['nns_status'] = $params['base_info']['nns_status'] = '2';
            $result_execute = nl_actor::edit(m_config::$obj_dc, $params['base_info'], $result_actor_exist['data_info'][0]['nns_id']);
            if($result_execute['ret'] != 0)
            {
                return $result_execute;
            }
            $arr_queue['base_info']['nns_id'] = $result_actor_exist['data_info'][0]['nns_id'];
        }
        else
        {
            $check_input_params['data_info']['info']['out']['base_info']['nns_action'] = $params['base_info']['nns_action'] = 'add';
            $check_input_params['data_info']['info']['out']['base_info']['nns_deleted'] = $params['base_info']['nns_deleted'] = '0';
            $check_input_params['data_info']['info']['out']['base_info']['nns_status'] = $params['base_info']['nns_status'] = '1';
            $result_execute = nl_actor::add(m_config::$obj_dc, $params['base_info']);
            if($result_execute['ret'] != 0)
            {
                return $result_execute;
            }

            $arr_queue['base_info']['nns_id'] = $params['data_info']['nns_id'];
        }

        //入中心注入指令
        $this->push_to_center_import($arr_queue, 'actor', 0, $params['base_info']['nns_action']);
        return self::return_data(0,'OK',$check_input_params['data_info']);
    }

    /**
     * 修改明星数据
     * @param array $params 修改数据 不能为空
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function edit($params)
    {
        return $this->add($params);
    }

    /**
     * 删除明星数据
     * @param array $params 数据数组，不能为空
     * @return multitype|array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function del($params)
    {
        //注入参数检查，模板参考
        $check_input_params = $this->check_input_params($params, 'del');
        if($check_input_params['ret'] != 0)
        {
            return $check_input_params;
        }
        if(!isset($check_input_params['data_info']['info']['out']) || empty($check_input_params['data_info']['info']['out']) || !is_array($check_input_params['data_info']['info']['out']))
        {
            return self::return_data(1, '片源参数检查反馈数据为空');
        }
        $params = $check_input_params['data_info']['info']['out'];
        //查询明星是否存在
        $result_actor_exist = nl_actor::query_by_condition(m_config::get_dc(), $params['base_info']);
        if($result_actor_exist['ret'] !=0)
        {
            return $result_actor_exist;
        }
        $arr_del = null;
        if(isset($result_actor_exist['data_info']) && is_array($result_actor_exist['data_info']) && !empty($result_actor_exist['data_info']))
        {
            foreach ($result_actor_exist['data_info'] as $data_info)
            {
                $arr_del[] = $data_info['nns_id'];
            }
        }
        if(empty($arr_del) || !is_array($arr_del))
        {
            return self::return_data(1, "没有需要删除的数据，{$result_actor_exist['reason']}");
        }

        //修改片源文件
        $result_execute = nl_actor::del(m_config::get_dc(), array('nns_id' => $arr_del));
        if($result_execute['ret'] !=0)
        {
            return $result_execute;
        }

        //入中心注入指令
        if(empty($arr_del) || !is_array($arr_del))
        {
            $arr_queue = array(
                'base_info' => array(
                    'nns_cp_id' => $params['base_info']['nns_cp_id'],
                ),
            );
            foreach ($arr_del as $del_val)
            {
                $arr_queue['base_info']['nns_id'] = $del_val;
                $this->push_to_center_import($arr_queue, 'actor', 0, 'destroy');
            }
        }
        return self::return_data(0, 'ok', $check_input_params['data_info']);
    }

    /**
     * 查询明星数据
     * @param array $array_query 条件数据 不能为空
     * @param boolean $is_need_ext false 不需要查询扩展数据 | true 需要查询扩展数据
     * @param boolean $is_need_epg_import_id  false 不需要查询EPG扩展数据 | true 需要查询EPG扩展数据
     * @param boolean $is_need_cdn_import_id  false 不需要查询CDN扩展数据 | true 需要查询CDN扩展数据
     * @return multitype|array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function query($array_query, $is_need_ext = false, $is_need_epg_import_id = false, $is_need_cdn_import_id = false)
    {
        $last_data = null;
        $result_actor = nl_actor::query_by_condition(m_config::get_dc(), $array_query);
        if($result_actor['ret'] !=0 || !isset($result_actor['data_info']) || !is_array($result_actor['data_info']) || empty($result_media['data_info']))
        {
            return $result_actor;
        }
        foreach ($result_actor['data_info'] as $data_info)
        {
            if (!empty($data_info['ex_info']))
            {
                $data_info['ex_info'] = json_decode($data_info['ex_info'], true);
            }
            $temp_epg_data = $temp_cdn_data = $temp_ex_data = null;

            if($is_need_ext)
            {
                #TODO 预留扩展信息
            }

            if($is_need_epg_import_id)
            {
                #TODO
            }
            if($is_need_cdn_import_id)
            {
                #TODO
            }
            $last_data[]=array(
                'base_info'=>$data_info,
                'ex_info'=>$temp_ex_data,
                'epg_info'=>$temp_epg_data,
                'cdn_info'=>$temp_cdn_data,
            );
        }
        return self::return_data(0,'OK',$last_data);
    }

}