<?php
\ns_core\m_load::load("ns_core.m_public");
\ns_core\m_load::load_old("nn_class/ftp/ftp.class.php");
class m_image extends m_public
{   
    /**
     * 允许转换成为的格式
     */
    public static $suffix = array('jpg','jpeg','png','webp','WEBP');

    /**
     * 图片 添加
     * @param $cp_id
     * @param $type //图片类型   video 主媒资 | index 分集 | playbill 节目单
     * @param string $img_from //图片路径  可为相对路径  或者绝对路径
     * @param string $video_name //图片的媒资名称
     * @return array //array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public static function img_add_handel($cp_id,$type,$img_from = "", $video_name = '')
    {
        if(strlen($type) < 1)
        {
            return self::return_data(NS_CDS_SUCCE,'图片类型为空不处理','');
        }
        $img_from = trim(trim(trim($img_from,'/'),'\\'));
        //如果参数为空直接反馈数据
        if (empty($img_from))
        {
            return self::return_data(NS_CDS_SUCCE,'图片为空不处理','');
        }
        $down_img = $img_from;
        // 获取CP配置信息
        $arr_cp_config = m_config::_get_cp_info($cp_id);
        if($arr_cp_config['ret'] != NS_CDS_SUCCE)
        {
            return self::return_data(NS_CDS_FAIL,"文件路径[".__FILE__."]类[".__FUNCTION__."]方法[".__FUNCTION__."]获取cp信息失败",$arr_cp_config);
        }
        $arr_cp_config = isset($arr_cp_config['data_info']) ? $arr_cp_config['data_info'] : null;
        if(isset($arr_cp_config['nns_config']['g_ftp_conf']['download_img_enabled']) && $arr_cp_config['nns_config']['g_ftp_conf']['download_img_enabled'] !='0')
        {
            return self::return_data(NS_CDS_SUCCE,"图片文件不处理原样打回",$img_from);
        }
        if (stripos($img_from, 'http://') === FALSE && stripos($img_from, 'https://') === FALSE && stripos($img_from, 'ftp://') === FALSE)
        {
            if (!isset($arr_cp_config['nns_config']['g_ftp_conf']["domain"]) || empty($arr_cp_config['nns_config']['g_ftp_conf']["domain"]))
            {
                return self::return_data(NS_CDS_FAIL,"文件路径[".__FILE__."]类[".__FUNCTION__."]方法[".__FUNCTION__."]获取cp|g_ftp_conf|domain配置信息为空",$arr_cp_config['nns_config']['g_ftp_conf']);
            }
            $down_img = trim(rtrim(rtrim($arr_cp_config['nns_config']['g_ftp_conf']["domain"],'/'),'\\')) .'/'. $img_from;
            // 增加下载图片的规则
            if (isset($arr_cp_config['nns_config']['g_ftp_conf']['rule']) && ! empty($arr_cp_config['nns_config']['g_ftp_conf']["rule"]) && isset($arr_cp_config['nns_config']['g_ftp_conf']['rule_domain']) && ! empty($arr_cp_config['nns_config']['g_ftp_conf']["rule_domain"]))
            {
                if (stripos($img_from, $arr_cp_config['nns_config']['g_ftp_conf']['rule']) !== false)
                {
                    $down_img = trim(rtrim(rtrim($arr_cp_config['nns_config']['g_ftp_conf']["rule_domain"],'/'),'\\')) .'/'. $img_from;
                }
            }
        }
        //播控控制生成文件名
        $temp_img_pathinfo = pathinfo($img_from);
        $temp_img_extension = (isset($temp_img_pathinfo['extension']) && strlen($temp_img_pathinfo['extension']) > 0) ? strtolower($temp_img_pathinfo['extension']) : 'jpg';
        //生成文件名规则，MD5 （媒资名称+文件原名+扩展名） -- xinxin.deng zhiyong.luo 2018/9/28 14:46
        $temp_img_name = md5($video_name . $temp_img_pathinfo['basename']);
        //$save_name = np_guid_rand() . '.' . $temp_img_extension;
        $save_name = $temp_img_name . '.' . $temp_img_extension;

        //播控生成自己的文件名规则了，cp+日期+guid
        //$str_date = date("Ymd");
        //$str_time = date("H");
        //$str_path = "/prev/KsImg/cp_{$cp_id}/{$type}/{$str_date}/{$str_time}/{$save_name}";

        $str_path = "/prev/KsImg/cp_{$cp_id}/{$type}/{$save_name}";

        $result_make_dir = self::make_dir("/downimg".$str_path);
        if($result_make_dir['ret'] != NS_CDS_SUCCE)
        {
            if(isset($arr_cp_config['nns_config']['g_ftp_conf']['ignore_img_dowload_fail']) && $arr_cp_config['nns_config']['g_ftp_conf']['ignore_img_dowload_fail'] == '1')
            {
                return self::return_data(NS_CDS_SUCCE,'图片处理失败，本地文件路径创建失败,但不影响正常注入，返回图片地址为空','');
            }
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            if(isset($arr_cp_config['nns_config']['g_ftp_conf']['ignore_img_dowload_fail']) && $arr_cp_config['nns_config']['g_ftp_conf']['ignore_img_dowload_fail'] == '1')
            {
                return self::return_data(NS_CDS_SUCCE,'图片处理失败，文件路径生产绝对路径为空,但不影响正常注入，返回图片地址为空','');
            }
            return self::return_data(NS_CDS_FAIL,'文件路径生产绝对路径为空');
        }
        $result_save = self::get_mix_url_content_and_save($down_img, $result_make_dir['data_info']['absolute_dir'],30,true);
        if($result_save['ret'] != NS_CDS_SUCCE)
        {
            if(isset($arr_cp_config['nns_config']['g_ftp_conf']['ignore_img_dowload_fail']) && $arr_cp_config['nns_config']['g_ftp_conf']['ignore_img_dowload_fail'] == '1')
            {
                return self::return_data(NS_CDS_SUCCE,'图片处理失败，但不影响正常注入，返回图片地址为空','');
            }
            return self::return_data(NS_CDS_FAIL,"存储图片文件失败:[{$result_save['reason']}]",$result_save['data_info'],$result_save['ext_info']);
        }
        //上传到ftp
        if (isset($arr_cp_config['nns_config']['g_ftp_conf']['upload_img_to_ftp_enabled']) && $arr_cp_config['nns_config']['g_ftp_conf']['upload_img_to_ftp_enabled']
            && isset($arr_cp_config['nns_config']['g_ftp_conf']['upload_img_to_ftp_url']) && strlen($arr_cp_config['nns_config']['g_ftp_conf']['upload_img_to_ftp_url']) > 0)
        {
            $result_upto_ftp = m_config::up_to_ftp($result_make_dir['data_info']['base_dir'], $result_make_dir['data_info']['absolute_dir'], $arr_cp_config['nns_config']['g_ftp_conf']['upload_img_to_ftp_url']);
            if ($result_upto_ftp['ret'] != NS_CDS_SUCCE)
            {
                if(isset($arr_cp_config['nns_config']['g_ftp_conf']['ignore_img_dowload_fail']) && $arr_cp_config['nns_config']['g_ftp_conf']['ignore_img_dowload_fail'] == '1')
                {
                    return self::return_data(NS_CDS_SUCCE,'图片上传FTP失败，但不影响正常注入，返回图片地址为空','');
                }
                return self::return_data(NS_CDS_FAIL, '图片上传到ftp失败:' . $result_upto_ftp['reason']);
            }
            return self::return_data(NS_CDS_SUCCE,"图片上传到ftp处理成功", trim(preg_replace('/\?.*/i', '', $str_path)));
        }
        return self::return_data(NS_CDS_SUCCE,"图片处理成功",trim(preg_replace('/\?.*/i', '', $str_path)));
    }
    
    
    /**
     * 删除原始FTP上的图片文件(数据拷盘才会用到)
     * @param string $cp_id
     * @param string $img_from 图片地址
     * @param int $time_out ftp链接超时时间
     * @param bool $ftp_pasv ftp主被动模式
     * @return array
     */
    public static function img_del_source_handel($cp_id,$img_from = "",$time_out=90,$ftp_pasv=false)
    {
        $img_from = trim(trim(trim($img_from,'/'),'\\'));
        //如果参数为空直接反馈数据
        if (empty($img_from))
        {
            return self::return_data(0,'图片为空不处理','');
        }
        // 获取CP配置信息
        $arr_cp_config = m_config::_get_cp_info($cp_id);
        if($arr_cp_config['ret'] !=0)
        {
            return self::return_data(1,"文件路径[".__FILE__."]类[".__FUNCTION__."]方法[".__FUNCTION__."]获取cp信息失败",$arr_cp_config);
        }
        $down_img = $img_from;
        if (stripos($img_from, 'http://') === FALSE && stripos($img_from, 'https://') === FALSE && stripos($img_from, 'ftp://') === FALSE)
        {
            if (!isset($arr_cp_config['nns_config']['g_ftp_conf']["domain"]) || empty($arr_cp_config['nns_config']['g_ftp_conf']["domain"]))
            {
                return self::return_data(1,"文件路径[".__FILE__."]类[".__FUNCTION__."]方法[".__FUNCTION__."]获取cp|g_ftp_conf|domain配置信息为空",$arr_cp_config['nns_config']['g_ftp_conf']);
            }
            $down_img = trim(rtrim(rtrim($arr_cp_config['nns_config']['g_ftp_conf']["domain"],'/'),'\\')) .'/'. $img_from;
            // 增加下载图片的规则
            if (isset($g_ftp_conf['rule']) && ! empty($g_ftp_conf["rule"]) && isset($g_ftp_conf['rule_domain']) && ! empty($g_ftp_conf["rule_domain"]))
            {
                if (stripos($img_from, $arr_cp_config['nns_config']['g_ftp_conf']['rule']) !== false)
                {
                    $down_img = trim(rtrim(rtrim($arr_cp_config['nns_config']['g_ftp_conf']["rule_domain"],'/'),'\\')) .'/'. $img_from;
                }
            }
        }
        if(stripos($img_from, 'ftp://') === FALSE)
        {
            return self::return_data(0,'非FTP不做任何操作');
        }
        $ftp_url = trim($img_from,'/');
        $arr_ftp_config = parse_url($ftp_url);
        $host_ftp = (isset($arr_ftp_config["host"]) && strlen($arr_ftp_config["host"]) > 0) ? $arr_ftp_config["host"] : '';
        $user_ftp = (isset($arr_ftp_config["user"]) && strlen($arr_ftp_config["user"]) > 0) ? $arr_ftp_config["user"] : '';
        $pass_ftp = (isset($arr_ftp_config["pass"]) && strlen($arr_ftp_config["pass"]) > 0) ? $arr_ftp_config["pass"] : '';
        $path_ftp = (isset($arr_ftp_config["path"]) && strlen($arr_ftp_config["path"]) > 0) ? $arr_ftp_config["path"] : '';
        $port_ftp = (isset($arr_ftp_config["port"]) && strlen($arr_ftp_config["port"]) > 0) ? $arr_ftp_config["port"] : 21;
        $obj_ftp = new nl_ftp($host_ftp, $user_ftp, $pass_ftp,$port_ftp,$time_out,$ftp_pasv);
        $arr_path_info = pathinfo($path_ftp);
        if(!isset($arr_path_info['extension']) || strlen($arr_path_info['extension'])<1)
        {
            return m_config::return_data(1,'文件扩展不符合要求不允许删除');
        }
        $delete_dir_num = 0;
	    if(isset($arr_path_info['dirname']))
	    {
	        $path_info_dirname = str_replace("\\", '/', $arr_path_info['dirname']);
	        $path_info_dirname = str_replace("//", '/', $path_info_dirname);
	        $path_info_dirname = trim(trim($path_info_dirname,'/'));
	        if(strlen($path_info_dirname) >0)
	        {
	            $arr_num_dir = explode('/', $path_info_dirname);
	            $delete_dir_num = (is_array($arr_num_dir) && !empty($arr_num_dir)) ? count($arr_num_dir) : 0;
	        }
	    }
	    $result = $obj_ftp->delete_file($path_ftp,true,$delete_dir_num);
        $obj_ftp->my_destruct();
        unset($obj_ftp);
        return $result;
    }
    
    /**
     * 删除图片
     * @param mix $mix_img 混合参数   可为数组  或者为 字符串
     * @return multitype:number |array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public static function img_del_handel($mix_img)
    {
        if(is_string($mix_img))
        {
            $mix_img = trim(trim(trim($mix_img,'/'),'\\'));
            if(strlen($mix_img) <1)
            {
                return self::return_data(1,'传入的图片相对路径为空,不执行');
            }
            $mix_img = array($mix_img);
        }
        else if(!is_array($mix_img))
        {
            return self::return_data(1,'传入的数据非数组非字符串',$mix_img);
        }
        if(empty($mix_img))
        {
            return self::return_data(1,'传入的图片参数为空,不执行');
        }
        foreach ($mix_img as $img_val)
        {
            $img_val = "/downimg/".trim(trim(trim($img_val,'/'),'\\'));
            self::del_file($img_val);
        }
        return self::return_data(0,'传入的图片删除成功',$mix_img);
    }
    
    /**
     * 图片格式转换
     * @param string $org  原始图片的地址 字符串
     * @param string $suffix 新图片的格式   字符串
     */
    public static function img_format($org,$suffix) 
    {
        if(empty($org))
        {
            return self::return_data(1,'传入的图片参数为空,不执行');
        }   

        if(is_string($org))
        {
            $org = trim(trim(trim($org,'/'),'\\'));
            if(strlen($org) <1)
            {
                return self::return_data(1,'传入的图片相对路径为空,不执行');
            }
        }

        if(!file_exists($org)) 
        {
            return self::return_data(1,'图片文件不存在,不执行');
        }

        if(empty($suffix)) {
            return self::return_data(1,'转换格式参数为空,不执行');
        }

        if(!in_array($suffix, self::$enable_suffix))
        {
            return self::return_data(1,'转换格式参数非法,不执行');
        }

        $im = new imagick($org);
        $im->setImageFormat($suffix);
        $org_suffix = pathinfo($org, PATHINFO_EXTENSION);
        $new_file = basename($org,$org_suffix).$suffix;
        $im->writeImage($new_file);
        
        return self::return_data(0,'图片格式转换成功',$new_file);
    }
}