<?php
\ns_core\m_load::load_old("nn_logic/queue/queue.class.php");
\ns_core\m_load::load_old("nn_logic/queue/queue_pool.class.php");
\ns_core\m_load::load_old("nn_logic/queue/queue_redis.class.php");
/**
 * 定时器模块
 * @author pan.liang
 */
class m_timer
{
    private $timer_name = "";
    private $base_path = null;

	public $timer_path = "";
	public $timer_model = "";
	public $child_path = "";
	public $last_run_time = false; //上一次开始执行时间，对于一些获取数据的脚本有用，格式时间戳
	public $the_run_time = false; //本次开始执行时间
	public $max_execute_time = 1800;
	public $timer_queue_pool = null;
	public $obj_dc = null;
    public $obj_redis = null;
    
    public $arr_params = null;
    
    public $g_arr_queue_pool = null;
    
    public $g_debug_sql_enable = 0;
    public $check_path = "";//定时器执行路径

    /**
     * 默认没开启队列池
     * @var unknown
     */
    public $flag_queue_pool = FALSE;

	public function __construct($timer_path,$timer_model=null,$timer_pool=null,$child_path='',$check_path='')
	{
        $arr_files = pathinfo($timer_path);
        $file_name = $arr_files['filename'];
        $this->base_path = dirname(dirname(dirname(__FILE__)));
        $this->timer_name = "timer_" . $file_name;
	    if(strlen($timer_model)>0)
	    {
		     $this->timer_path = $timer_path.'/'.$timer_model;
	    }
	    else
	    {
	         $this->timer_path = $timer_path;
	    }
		$this->timer_model = $timer_model;
		$this->child_path = $child_path;
		if(strlen($timer_pool) >0)
		{
		    global $g_arr_queue_pool;
		    $g_arr_queue_pool;
		    $arr_queue_pool = null;
		    if(is_array($g_arr_queue_pool))
		    {
		        foreach ($g_arr_queue_pool as $val)
		        {
		            $arr_queue_pool[$val['name']] = $val;
		        }
		    }
		    unset($g_arr_queue_pool);
		    $this->g_arr_queue_pool = $arr_queue_pool;
		    $this->timer_queue_pool = $timer_pool;
		}
		global $g_debug_sql_enable;
		if(isset($g_debug_sql_enable) && $g_debug_sql_enable == 1)
		{
		    m_config::timer_write_log($this->timer_path,"-----------SQL语句收集脚本开关开启------------",$this->child_path);
		    nl_dc::$db_debug_enable = $this->g_debug_sql_enable = 1;
		}
		else
		{
		    m_config::timer_write_log($this->timer_path,"-----------SQL语句收集脚本开关关闭------------",$this->child_path);
		}
		$this->check_path = $check_path;
		unset($g_debug_sql_enable);
	}
	
	
	/**
	 * 全局参数检查哈
	 */
	public function _global_validate_config_info()
    {
        if(!is_array($this->g_arr_queue_pool))
        {
            return m_config::return_data(1,'全局配置g_arr_queue_pool 未配置');
        }
        if(!isset($this->g_arr_queue_pool[$this->timer_queue_pool]))
        {
            return m_config::return_data(1,"全局配置g_arr_queue_pool[".$this->timer_queue_pool."] 未配置");
        }
        $temp_arr_queue_pool = $this->g_arr_queue_pool[$this->timer_queue_pool];
        if(!isset($temp_arr_queue_pool['ext_info']) || !is_array($temp_arr_queue_pool['ext_info']) || empty($temp_arr_queue_pool['ext_info']))
        {
            return m_config::return_data(0,"OK");
        }
        foreach ($temp_arr_queue_pool['ext_info'] as $temp_value)
        {
            switch ($temp_value['rule'])
            {
                case 'noempty':
                    if(!isset($this->arr_params['bk_queue_config']['nns_ext_info'][$temp_value['name']]) || strlen($this->arr_params['bk_queue_config']['nns_ext_info'][$temp_value['name']]) <1)
                    {
                        return m_config::return_data(1,"全局配置g_arr_queue_pool[".$this->timer_queue_pool."][".$temp_value['name']."] 为空");
                    }
                    break;
                case 'int':
                    if(!isset($this->arr_params['bk_queue_config']['nns_ext_info'][$temp_value['name']]) || strlen($this->arr_params['bk_queue_config']['nns_ext_info'][$temp_value['name']]) <1 || $this->arr_params['bk_queue_config']['nns_ext_info'][$temp_value['name']] <1)
                    {
                        return m_config::return_data(1,"全局配置g_arr_queue_pool[".$this->timer_queue_pool."][".$temp_value['name']."] 不在数值范围内");
                    }
                    break;
            }
        }
        return m_config::return_data(0,"OK");
    }
    
   
    /**
     * 队列池数据验证 检查 数据是否该修改，删除，和添加
     * @param unknown $queue_val
     */
    public function _check_all_params($queue_val,$arr_data,$arr_exsist=null,$flag=FALSE)
    {
        if(!is_array($arr_data) || empty($arr_data))
        {
            return m_config::return_data(0,'传入的对比参数为空');
        }
        $arr_add = $arr_edit = $arr_del = null;
        if(!$flag)
        {
            $result_queue_exsist = nl_queue_pool::timer_query_by_queue_id_v2(m_config::get_dc(),$queue_val['nns_id']);
            if($result_queue_exsist['ret'] !=0)
            {
                return $result_queue_exsist;
            }
            
            $result_queue_exsist = (isset($result_queue_exsist['data_info']) && is_array($result_queue_exsist['data_info']) && !empty($result_queue_exsist['data_info'])) ? $result_queue_exsist['data_info'] : null;
            if(is_array($result_queue_exsist))
            {
                foreach ($result_queue_exsist as $exsist_val)
                {
                    $arr_exsist[$exsist_val['nns_value']] = $exsist_val;
                }
            }
        }
        $arr_ex_keys = array_keys($arr_data);
        if(is_array($arr_exsist))
        {
            foreach ($arr_exsist as $exsist_key=>$exs_val)
            {
                if(!in_array($exsist_key, $arr_ex_keys))
                {
                    $arr_del[] = $exsist_key;
                    unset($arr_exsist[$exsist_key]);
                }
            }
        }
        foreach ($arr_data as $data_key=>$data_val)
        {
            if(isset($arr_exsist[$data_key]))
            {
                if($data_val['nns_state'] == $data_val['nns_state'])
                {
                    continue;
                }
                $arr_edit[$data_val['nns_state']][] = $data_key;
            }
            else
            {
                $arr_add[$data_key] = $data_val;
            }
        }
        if(is_array($arr_del) && !empty($arr_del))
        {
            $result_queue_pool = nl_queue_pool::delete_by_value(m_config::get_dc(), $queue_val['nns_id'], $arr_del);
            if($result_queue_pool['ret'] !=0)
            {
                return $result_queue_pool;
            }
        }
        if(is_array($arr_edit) && !empty($arr_edit))
        {
            foreach ($arr_edit as $edit_key=>$edit_val)
            {
                $result_edit = nl_queue_pool::edit_v2(m_config::get_dc(), array('nns_state'=>$edit_key), array('nns_queue_id'=>$queue_val['nns_id'],'nns_value'=>$edit_val));
                if($result_edit['ret'] !=0)
                {
                    return $result_edit;
                }
            }
        }
        if(is_array($arr_add) && !empty($arr_add))
        {
            return $this->push_to_queue_pool_mysql($queue_val, $arr_add);
        }
        return m_config::return_data(0,'OK');
    }
    
	
	/**
	 * 定时器运行
	 * @param string $params
	 */
	public function run($params = NULL)
	{
		$result_check = m_config::return_data(999,'ok');
		if(strlen($this->timer_queue_pool) >0)
		{
		    $result = nl_queue::timer_query_by_queue_id(m_config::get_dc(), $this->timer_queue_pool);
		    if($result['ret'] !=0 || !isset($result['data_info']) || empty($result['data_info']) || !is_array($result['data_info']))
		    {
		        $result_check = m_config::return_data(999,"查询队列池数据库执行失败|或者队列池失效".var_export($result,true));
		    }
		    else
		    {
		        $result = $result['data_info'];
		        $result['nns_ext_info'] = (isset($result['nns_ext_info']) && strlen($result['nns_ext_info']) >0) ? json_decode($result['nns_ext_info'],true) : null;
		        $params['bk_queue_config'] = $result;
		        $this->flag_queue_pool = true;
		        
		        $result_redis_pop = nl_queue_redis::pop(m_config::get_redis_dc(), $this->timer_queue_pool);
		        $temp_queue_val = (isset($result_redis_pop['data_info']) && strlen($result_redis_pop['data_info'])>0) ? $result_redis_pop['data_info'] : '';
		        if(strlen($temp_queue_val) <1)
		        {
		            $result_redis_pop = nl_queue_redis::pop(m_config::get_redis_dc(), $this->timer_queue_pool);
		            $temp_queue_val = (isset($result_redis_pop['data_info']) && strlen($result_redis_pop['data_info'])>0) ? $result_redis_pop['data_info'] : '';
		        }
		        if (strlen($temp_queue_val)<1)
		        {
		            m_config::timer_write_log($this->timer_path,"队列查询两次值都为空有问题啊",$this->child_path);
		            return ;
		        }
		        $params['bk_queue_value'] = $temp_queue_val;
		        m_config::timer_write_log($this->timer_path,"执行队列池[{$this->timer_queue_pool}]值[{$temp_queue_val}]",$this->child_path);
		        $this->arr_params = $params;
		        $result_check = $this->_global_validate_config_info();
		    }
		}
        if(strlen($this->check_path) > 0 && !$this->check_linux_course())
        {
            m_config::timer_write_log($this->timer_path,"/* " . date("Y-m-d H:i:s") . " 进程还在运行-----定时器脚本结束 */",$this->child_path);
            return ;
        }
		if($result_check['ret'] == '0')
		{
		    $this->action($params);
		    if(strlen($this->timer_queue_pool) >0 && isset($result['nns_often_state']) && $result['nns_often_state'] !='1')
		    {
                $result_queue_pool = nl_queue_redis::get_list(nl_get_redis(), $this->timer_queue_pool);
                $result_queue_pool = (isset($result_queue_pool['data_info']) && is_array($result_queue_pool['data_info']) && !empty($result_queue_pool['data_info'])) ? $result_queue_pool['data_info'] : array();
                if(!in_array($temp_queue_val, $result_queue_pool))
                {
                    $result_redis_push = nl_queue_redis::push(nl_get_redis(), $this->timer_queue_pool,$temp_queue_val);
                    if($result_redis_push['ret'] !=0)
                    {
                        m_config::timer_write_log($this->timer_path,"redis队列插入数据有问题咯,模板[{$this->timer_queue_pool}],值[{$temp_queue_val}]",$this->child_path);
                    }
                }
		    }
		}
		else if($result_check['ret'] == '999')
		{
		    m_config::timer_write_log($this->timer_path,"{$result_check['reason']}----------走原来的队列逻辑",$this->child_path);
		    $this->action($params);
		}
		else
		{
		    m_config::timer_write_log($this->timer_path,$result_check['reason'],$this->child_path);
		}
		$this->collect_sql();
	}

	/**
	 * 收集 debug sql
	 */
	public function collect_sql()
	{
	    if($this->g_debug_sql_enable !=1)
	    {
	        return ;
	    }
	    m_config::timer_write_log($this->timer_path,"/*SQL语句收集脚本开始 */",$this->child_path);
	    global $g_debug_sql_ex_second;
	    $debug_sql_ex_second = (isset($g_debug_sql_ex_second) && $g_debug_sql_ex_second >=0) ? ($g_debug_sql_ex_second*1000) : 5000;
	    unset($g_debug_sql_ex_second);
	    if (!is_array(nl_dc::$db_debug_arr))
	    {
	        m_config::timer_write_log($this->timer_path,"/*无任何SQL语句 */",$this->child_path);
	        m_config::timer_write_log($this->timer_path,"/*SQL语句收集脚本结束 */",$this->child_path);
	        return ;
	    }
	    
	    foreach (nl_dc::$db_debug_arr as $db)
	    {
	        $config=$db->get_db_config();
	        $host_ip=$config['host'];
	        $debug_info=$db->get_db_debug();
	        if($debug_sql_ex_second >0)
	        {
	            $this->write_sql_log("/*收集>={$debug_sql_ex_second}ms的sql语句 */");
	            foreach ($debug_info as $debug)
	            {
	                $time=round($debug['time']*1000,3);
	                if($time >= $debug_sql_ex_second)
	                {
	                    $this->write_sql_log("执行时间[{$time}ms]:".$debug['SQL'],$debug);
	                }
	            }
	        }
	        else
	        {
	            $this->write_sql_log("/* 收集全部sql语句 */");
	            $time=round($debug['time']*1000,3);
	            foreach ($debug_info as $debug)
	            {
	                 $this->write_sql_log("执行时间[{$time}ms]:".$debug['SQL'],$debug);
	            }
	        }
	    }
	    m_config::timer_write_log($this->timer_path,"/*SQL语句收集脚本结束 */",$this->child_path);
	}
	
	
	
	/**
	 * 推入队列池redis
	 */
	public function push_to_queue_pool_redis($queue_val)
	{
	    $result_query = nl_queue_pool::timer_query_by_queue_id(m_config::get_dc(),$queue_val['nns_id']);
	    if($result_query['ret']!=0 || !isset($result_query['data_info']) || empty($result_query['data_info']) || !is_array($result_query['data_info']))
	    {
	        return $result_query;
	    }
	    $result_query = $result_query['data_info'];
	    $redis_data = array();
	    foreach ($result_query as $query_val)
	    {
	        $redis_data[] = $query_val['nns_value'];
	    }
	    $result_queue_pool = nl_queue_redis::get_list(nl_get_redis(), $queue_val['nns_queue']);
	    $result_queue_pool = (isset($result_queue_pool['data_info']) && is_array($result_queue_pool['data_info']) && !empty($result_queue_pool['data_info'])) ? $result_queue_pool['data_info'] : array();
	    $redis_data = array_diff($redis_data,$result_queue_pool);
	    if(is_array($redis_data) && !empty($redis_data))
	    {
	        foreach ($redis_data as $redis_data_val)
	        {
	            $result_add_redis = nl_queue_redis::push(nl_get_redis(), $queue_val['nns_queue'], $redis_data_val);
	            if($result_add_redis['ret'] !=0)
	            {
	                return $result_add_redis;
	            }
	        }
	    }
	    return m_config::return_data(0,'ok');
	}
	
	
	/**
	 * 推入队列池mysql
	 * @param unknown $queue_val
	 * @param unknown $arr_insert_data
	 * @return Ambigous|array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 */
	public function push_to_queue_pool_mysql($queue_val,$arr_insert_data)
	{
	    if(empty($queue_val) || !is_array($queue_val))
	    {
	        return m_config::return_data(1,'队列模板参数非数组：'.var_export($queue_val,true));
	    }
	    if(empty($arr_insert_data) || !is_array($arr_insert_data))
	    {
	        return m_config::return_data(1,'队列参数非数组：'.var_export($arr_insert_data,true));
	    }
        $arr_insert_data = array_chunk($arr_insert_data,100,true);
        foreach ($arr_insert_data as $val)
        {
            $temp_array_keys = array_keys($val);
            $result_exsist = nl_queue_pool::query_by_queue_value_id(m_config::get_dc(), $queue_val['nns_id'], $temp_array_keys);
            if($result_exsist['ret'] !=0)
            {
                continue;
            }
            if(isset($result_exsist['data_info']) && !empty($result_exsist['data_info']) && is_array($result_exsist['data_info']))
            {
                foreach ($result_exsist['data_info'] as $exist_val)
                {
                    if(isset($val[$exist_val['nns_value']]))
                    {
                        unset($val[$exist_val['nns_value']]);
                    }
                }
            }
            if(empty($val) || !is_array($val))
            {
                continue;
            }
            $val = array_values($val);
            $result_add_mysql = nl_queue_pool::add(m_config::get_dc(), $val);
            if($result_add_mysql['ret'] !=0)
            {
                continue;
            }
        }
	    return m_config::return_data(0,'ok');
	}
	
	protected function action($params = null)
	{
		//        do something....
	}

	public function set_sql_log()
	{
		if (is_array(nl_dc::$db_debug_arr))
		{
			foreach (nl_dc::$db_debug_arr as $db){
				$config=$db->get_db_config();
				$host_ip=$config['host'];
				$debug_info=$db->get_db_debug();
				foreach ($debug_info as $debug){
					//     				var_dump(strtolower(strpos($debug['SQL']), "nns_mgtvbk_c2_task"),'testtesttest');
					if(strpos(strtolower($debug['SQL']), "nns_mgtvbk_c2_task") === false)
					{
						//     					continue;
					}
					$re=array();
// 					$re['ip']=$host_ip;
					$re['sql']=$debug['SQL'];
// 					$re['desc']=$debug['debug'];
					$re['time']=round($debug['time']*1000,3);
// 					$re['explain_debug']=$debug['explain_debug'];
// 					$result['data'][]=$re;
				}
			}
		}
	}
	
	public function write_sql_log($msg,$debug=null)
	{
		$control_file = $this->base_path . "/data/log/v2/nn_timer/" . $this->timer_name . "/" . $this->timer_model . ".sql";
		$str_log_dir = dirname($control_file);
		if (!is_dir($str_log_dir))
		{
			mkdir($str_log_dir, 0777, true);
		}
		$msg = $msg . "\n";
		echo $msg ;
		echo (isset($debug['debug']) && $debug['debug'] =='OK') ? '' : "<font color='red'>{$debug['debug']}</font>";
		echo "\r\n<br/>";
		@error_log($msg, 3, $control_file);
		unset($msg);
		return;
	}
	
	public function set_running_flag($state)
	{
		$control_file = $this->base_path . "/data/log/v2/nn_timer/" . $this->timer_name . "/" . $this->timer_model. ".txt";
		$str_log_dir = dirname($control_file);
		if (!is_dir($str_log_dir))
		{
			mkdir($str_log_dir, 0777, true);
		}
		$datetime = date("YmdHis", $this->the_run_time);
		$controls = $state . ',' . $datetime;
		file_put_contents($control_file, $controls);
	}

	private function check_running_flag()
	{
		$control_file = $this->base_path . "/data/log/v2/nn_timer/" . $this->timer_name . "/" . $this->timer_model . ".txt";
		if (!defined("CONTROL_STATIC_RUNNING_LOCK_MAX_TIME"))
		{
			$control_times = $this->max_execute_time;
		}
		else
		{
			$control_times = CONTROL_STATIC_RUNNING_LOCK_MAX_TIME;
		}
		m_config::timer_write_log($this->timer_path,"检查上一次脚本是否已经停止运行",$this->child_path);
		
		$this->the_run_time = time(); //定本次脚本执行开始时间
		

		if (!file_exists($control_file))
		{
			$this->set_running_flag(1);
			return TRUE;
		}
		
		$control_content = @file_get_contents($control_file);
		$control_content = trim($control_content);
		$control_contents = explode(",", $control_content);
		
		$this->last_run_time = strtotime($control_contents[1]);
		
		if ($control_contents[0] == 0)
		{
			m_config::timer_write_log($this->timer_path,"上一次脚本已经停止运行, 执行时间：" . $control_contents[1],$this->child_path);
			$this->set_running_flag(1);
			return TRUE;
		}
		if ($control_contents[0] == 1 && ($this->the_run_time - $this->last_run_time >= $control_times))
		{
			m_config::timer_write_log($this->timer_path,"上一次脚本还在运行，但运行了{$control_times}秒，可能是PHP卡死",$this->child_path);
			$this->set_running_flag(1);
			return TRUE;
		}
		m_config::timer_write_log($this->timer_path,"上一次脚本还在运行",$this->child_path);
		return FALSE;
	}

	/**
     * 获取脚本上一次的执行开始时间
     * @return boolean false 表示本次是第一次执行 | 时间戳
     */
	public function get_last_run_time()
	{
		return $this->last_run_time;
	}

	/**
     * 获取脚本本次的执行开始时间
     * @return 时间戳
     */
	public function get_the_run_time()
	{
		return $this->the_run_time;
	}

	/**
	 * 判断定时器脚本是否多进程运行
	 * @param string $str_course_file_name 定时器脚本  进程查询的 相对|绝对路径
	 * @return boolean true | false
	 * @author liangpan
	 * @date 2015-08-05
	 */
	public function check_linux_course()
	{
        if($this->flag_queue_pool)
        {
            return true;
        }
        //$str_course_file_name = str_replace($this->base_path_1.'/', '', $this->file_path);
        $str_course_file_name = $this->check_path;
        m_config::timer_write_log($this->timer_path,"-------进程查询处理开始-------",$this->child_path);
		$str_course_file_name = "ps -ef | grep '" . $str_course_file_name . "' | grep -v grep | awk '{print $2}'";
		m_config::timer_write_log($this->timer_path,"进程查询的命令为：" . $str_course_file_name,$this->child_path);
		@exec($str_course_file_name,$arr_course,$exec_result);
		m_config::timer_write_log($this->timer_path,"进程查询的结果为：" . var_export($arr_course,true),$this->child_path);
		if($exec_result != 0)
		{
			global $g_ignore_exec_error;
			$return = $g_ignore_exec_error ? false : true;
			unset($g_ignore_exec_error);
			$str_desc = $return ? "程序忽略定时器进程控制，继续执行" : "程序需要定时器进程控制，停止执行";
			m_config::timer_write_log($this->timer_path,"进程查询php报错，可能是关闭了exec，也可能没在linux环境运行....".$str_desc,$this->child_path);
			return $return;
		}
		if (!empty($arr_course))
		{
			$count_course =count($arr_course);
			if($count_course == 1)
			{
				m_config::timer_write_log($this->timer_path,"运行的进程有".$count_course."个，进程正常",$this->child_path);
				return true;
			}
			else
			{
				m_config::timer_write_log($this->timer_path,"运行的进程有".$count_course."个，结束进程",$this->child_path);
				return false;
			}
		}
		else
		{
			m_config::timer_write_log($this->timer_path,"一个进程都没开启，继续执行",$this->child_path);
			return true;
		}
		m_config::timer_write_log($this->timer_path,"-------进程查询处理结束-------",$this->child_path);
	}

    /**
     * 项目特殊处理定时器时，需要额外调用进行进程是否执行的判断
     * @param $check_path //定时器执行路径
     * @return bool
     */
	public function check_linux_course_v2($check_path)
    {
        $str_course_file_name = $check_path;
        m_config::timer_write_log($this->timer_path,"-------项目特性处理：调用进程查询处理开始-------",$this->child_path);
        $str_course_file_name = "ps -ef | grep '" . $str_course_file_name . "' | grep -v grep | awk '{print $2}'";
        m_config::timer_write_log($this->timer_path,"项目特性处理：进程查询的命令为：" . $str_course_file_name,$this->child_path);
        @exec($str_course_file_name,$arr_course,$exec_result);
        m_config::timer_write_log($this->timer_path,"项目特性处理：进程查询的结果为：" . var_export($arr_course,true),$this->child_path);
        if($exec_result != 0)
        {
            global $g_ignore_exec_error;
            $return = $g_ignore_exec_error ? false : true;
            unset($g_ignore_exec_error);
            $str_desc = $return ? "项目特性处理：程序忽略定时器进程控制，继续执行" : "项目特性处理：程序需要定时器进程控制，停止执行";
            m_config::timer_write_log($this->timer_path,"项目特性处理：进程查询php报错，可能是关闭了exec，也可能没在linux环境运行....".$str_desc,$this->child_path);
            return $return;
        }
        if (!empty($arr_course))
        {
            $count_course =count($arr_course);
            if($count_course == 0)
            {
                m_config::timer_write_log($this->timer_path,"项目特性处理：运行的进程有".$count_course."个，进程正常",$this->child_path);
                m_config::timer_write_log($this->timer_path,"-------项目特性处理：进程查询处理结束-------",$this->child_path);
                return true;
            }
            else
            {
                m_config::timer_write_log($this->timer_path,"项目特性处理：运行的进程有".$count_course."个，结束进程",$this->child_path);
                m_config::timer_write_log($this->timer_path,"-------项目特性处理：进程查询处理结束-------",$this->child_path);
                return false;
            }
        }
        else
        {
            m_config::timer_write_log($this->timer_path,"项目特性处理：一个进程都没开启，继续执行",$this->child_path);
            m_config::timer_write_log($this->timer_path,"-------项目特性处理：进程查询处理结束-------",$this->child_path);
            return true;
        }
    }
} 