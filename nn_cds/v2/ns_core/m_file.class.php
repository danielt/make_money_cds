<?php
class m_file
{
	public static $base_file_dir = null;
	public static $base_file_dir_copy = null;
	public static $base_file = null;
	public static $error_desc = null;
	public static $ex_file_name = '';
	public static $arr_file_list = null;
	/**
	 * @return the $error_desc
	 */
	public static function get_error_desc()
	{
		return nn_log::$error_desc;
	}

	/**
	 * @param field_type $error_desc
	 */
	public static function set_error_desc($error_desc)
	{
		nn_log::$error_desc[] = $error_desc;
	}

	/**
	 * 生产文件
	 * @param string $xml 文件内容
	 * @return boolean
	 * @author liangpan
	 * @date 2016-08-24
	 */
	public static function make_file($xml,$file_dir,$ex_file_name='xml')
	{
	    $ex_file_name = trim($ex_file_name);
	    self::$ex_file_name = (strlen($ex_file_name) > 0) ? $ex_file_name : 'xml';
		if(strlen($xml) < 1)
		{
			self::set_error_desc("xml为空");
			return false;
		}
		if(strlen($file_dir) < 1)
		{
			self::set_error_desc("file_dir为空");
			return false;
		}
		self::$base_file_dir_copy=self::$base_file_dir = dirname(dirname(__FILE__));
		self::$base_file_dir.="/data/".$file_dir.'/';
		self::$base_file_dir_copy.="/data_copy/".$file_dir.'/';
		$result = self::make_dir(self::$base_file_dir);
		if(!$result)
		{
			return false;
		}
		$result = self::make_dir(self::$base_file_dir_copy);
		if(!$result)
		{
			return false;
		}
		$file_name = np_guid_rand(self::$base_file_dir);
		self::$base_file = self::$base_file_dir.$file_name.".".self::$ex_file_name;	
		$result = self::write_file($xml,self::$base_file);
		if(!$result)
		{
			return false;
		}
		self::$base_file = self::$base_file_dir_copy.$file_name.".".self::$ex_file_name;
		$result = self::write_file($xml,self::$base_file);
		if(!$result)
		{
			return false;
		}
		return true;
	}
	
	/**
	 * 创建文件路径
	 * @param string $xml 文件内容
	 * @return boolean
	 * @author liangpan
	 * @date 2016-08-24
	 */
	public static function make_dir($base_file_dir)
	{
		if (is_dir($base_file_dir))
		{
			return true;
		}
		$result=mkdir($base_file_dir, 0777, true);
		if(!$result)
		{
			self::set_error_desc("创建文件夹失败，路径为：".$base_file_dir);
			return false;
		}
		return true;
	}
	
	/**
	 * 写入文件内容
	 * @param string $xml 文件内容
	 * @return boolean
	 * @author liangpan
	 * @date 2016-08-24
	 */
	public static function write_file($xml,$base_file)
	{
		if(strlen($xml) < 1)
		{
			self::set_error_desc("文件内容为空不写入");
			return false;
		}
		$result = file_put_contents($base_file, $xml, LOCK_EX);
		if($result === false)
		{
			self::set_error_desc("写入文件失败，文件为：".$base_file);
			return false;
		}
		return true;
	}
	
	
	
	
	
	/**
	 * 响应文件
	 * @param unknown $base_file_dir
	 * @param number $dir_child
	 * @return unknown|string|boolean
	 */
	public static function ack_data($ex_file_name='xml')
	{
	    $ex_file_name = trim($ex_file_name);
	    self::$ex_file_name = (strlen($ex_file_name) > 0) ? $ex_file_name : 'xml';
		$base_file_dir = dirname(dirname(__FILE__))."/data";
		$file = self::get_files($base_file_dir);
		if(is_bool($file) || empty($file) || !is_array($file))
		{
			return '';
		}
		$file = array_shift($file);
		$content = self::get_file_data($file);
		if($content === false)
		{
			return false;
		}
		$result = self::delete_file($file);
		if($result === false)
		{
			return false;
		}
		return ($content === true) ? '' : $content;
	}
	
	/**
	 * 获取文件
	 * @param unknown $base_file_dir
	 * @param number $dir_child
	 * @return unknown|string|boolean
	 */
	public static function get_files($base_file_dir,$dir_child=0)
	{
		$dir_child+=1;
		if(!is_dir($base_file_dir))
		{
			return false;
		}
		$arr_base_file_dir = scandir($base_file_dir);
        if((!is_array($arr_base_file_dir) || empty($arr_base_file_dir)) && $dir_child > 1 )
		{
			self::delete_dir($base_file_dir);
			return true;
		}
		$arr_base_file_dir = array_diff($arr_base_file_dir, array('.','..'));
		if((!is_array($arr_base_file_dir) || empty($arr_base_file_dir)) && $dir_child > 1 )
		{
			self::delete_dir($base_file_dir);
			return true;
		}
		ksort($arr_base_file_dir);
		$arr_temp_base_file_dir_1 = $arr_temp_base_file_dir = array();
		foreach ($arr_base_file_dir as $afile)
		{
			if (is_dir($base_file_dir . '/' . $afile))
			{
				$temp_data = self::get_files($base_file_dir . '/' . $afile,$dir_child);
				if(is_bool($temp_data))
				{
					continue;
				}
				return $temp_data;
			}
			$pathinfo_file = pathinfo($afile);
			if(isset(self::$ex_file_name) && strlen(self::$ex_file_name) >1)
			{
			    if(isset($pathinfo_file['extension']) && $pathinfo_file['extension']==self::$ex_file_name && isset($pathinfo_file['filename']) && strlen($pathinfo_file['filename']) > 0)
			    {
			        $arr_temp_base_file_dir[] = $base_file_dir.'/'.$afile;
			        $arr_temp_base_file_dir_1[]=self::get_file_date($base_file_dir.'/'.$afile);
			    }
			}
			else
			{
			    $arr_temp_base_file_dir[] = $base_file_dir.'/'.$afile;
			    $arr_temp_base_file_dir_1[]=self::get_file_date($base_file_dir.'/'.$afile);
			}
		}
		if($dir_child > 1 && empty($arr_temp_base_file_dir))
		{
			self::delete_dir($base_file_dir);
			self::set_error_desc("查无文件，文件路径为：".$base_file_dir);
			return false;
		}
		if(empty($arr_temp_base_file_dir))
		{
			return array();
		}
		array_multisort($arr_temp_base_file_dir,$arr_temp_base_file_dir_1);
		return $arr_temp_base_file_dir;
	}
	
	
	
	/**
	 * 获取文件
	 * @param unknown $base_file_dir
	 * @param number $dir_child
	 * @return unknown|string|boolean
	 */
	public static function get_files_v2($base_file_dir,$flag=false)
	{
		$last_data = array();
	    $base_file_dir = trim(rtrim(rtrim($base_file_dir,'/'),'\\'));
	    $base_file_dir = strlen($base_file_dir) <1 ? '/' : $base_file_dir.'/';
	    if(!is_dir($base_file_dir))
	    {
	        return false;
	    }
	    $arr_base_file_dir = scandir($base_file_dir);
	    if((!is_array($arr_base_file_dir) || empty($arr_base_file_dir)))
	    {
	        return true;
	    }
	    $arr_base_file_dir = array_diff($arr_base_file_dir, array('.','..'));
	    if((!is_array($arr_base_file_dir) || empty($arr_base_file_dir)))
	    {
	        return true;
	    }
	    foreach ($arr_base_file_dir as $afile)
	    {
	        if (is_dir($base_file_dir . $afile))
	        {
	            $data = self::get_files_v2($base_file_dir . $afile,true);
	            if(is_array($data) && !empty($data))
	            {
	                $last_data = array_merge($last_data,$data);
	            }
	            continue;
	        }
            if(isset(self::$ex_file_name) && strlen(self::$ex_file_name) >1)
            {
                $pathinfo_file = pathinfo($base_file_dir.$afile);
                if(!isset($pathinfo_file['extension']) || strlen($pathinfo_file['extension']) <1 || strtolower($pathinfo_file['extension'])!=strtolower(self::$ex_file_name))
                {
                    continue;
                }
            }
	        $last_data[] = array(
	            'path'=>$base_file_dir.$afile,
	            'time'=>self::get_file_date($base_file_dir.$afile)
	        );
	    }
	    if($flag === false)
	    {
            self::$ex_file_name = '';
        }
	    if(empty($last_data))
	    {
	        return true;
	    }
	    return $last_data;
	}
	
	/**
	 * 获取文件的创建时间
	 * @param unknown $file_path
	 * @return number
	 */
	public static function get_file_date($file_path)
	{
		$date_time=filemtime($file_path);
		if($date_time === false)
		{
			$date_time = 0;
		}
		return $date_time;
	}
	
	/**
	 * 删除文件夹
	 * @param unknown $file_dir
	 * @return boolean
	 */
	public static function delete_dir($file_dir)
	{
		if(!is_dir($file_dir))
		{
			self::set_error_desc("删除文件夹失败，没有文件路径：".$file_dir);
	      	return false;
		}
		$files = scandir($file_dir);
		if(!is_array($files) || empty($files))
		{
			self::set_error_desc("删除文件夹失败，文件夹下文件列表：".var_export($files,true));
			return false;
		}
		$files = array_diff($files, array('.','..')); 
	    foreach ($files as $file) 
	    { 
	      	if(is_dir($file_dir . '/' . $file))
	      	{
	      		$result=self::delete_dir($file_dir . '/' . $file);
	      		if(!$result)
	      		{
	      			return false;
	      		}
	      	}
	      	else
	      	{
	      		$result=self::delete_file($file_dir . '/' . $file);
	      		self::set_error_desc("删除文件夹失败，居然还有文件，有问题，文件路径为：".$file_dir.'/'.$file);
	      		if(!$result)
	      		{
	      			return false;
	      		}
	    	}
	    }
	    $result = rmdir($file_dir);
	   	if(!$result)
		{
			self::set_error_desc("删除文件夹失败，文件路径为：".$file_dir);
			return false;
		}
		return true;
	}
	
	/**
	 * 删除文件
	 * @param unknown $file_dir
	 * @return boolean
	 */
	public static function delete_file($file_path)
	{
		if(!file_exists($file_path))
		{
			return true;
		}
		$result = unlink($file_path);
		if(!$result)
		{
			self::set_error_desc("删除文件失败，文件路径为：".$file_path);
			return false;
		}
		return true;
	}
	
	/**
	 * 获取文件内容
	 * @param unknown $file_dir
	 * @return boolean
	 */
	public static function get_file_data($file_path)
	{
		if(!file_exists($file_path))
		{
			return true;
		}
		$result=file_get_contents($file_path);
		if($result === false)
		{
			self::set_error_desc("读取文件失败，文件路径为：".$file_path);
			return false;
		}
		return $result;
	}
}