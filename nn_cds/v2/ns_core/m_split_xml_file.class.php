<?php
/**
 *
 * @author: pan.liang
 * @date: 2018/3/19 18:58
 * @example m_split_xml_file::split_xml("http://ext.tvmao.com/data/jiangsuyouxian/jiangsuyouxian_all_program_33.xml",'program',100,'public',true)
 */
include_once dirname(dirname(__FILE__))."/common.php";
$result = \ns_core\m_load::load("ns_core.m_config");
class m_split_xml_file extends m_config
{
    public static $old_obj_dom = null;
    
    public static $new_obj_dom = null;
    
    public static $split_xml_file_path = null;
    
    
    /**
     * 验证TXT文件的可用性
     * @param unknown $file_path
     */
    public static function check_xml_file($file_path)
    {
        $file_path = trim($file_path);
        if(strlen($file_path) <1)
        {
            return self::return_data(1,"传入参数为空");
        }
        if(!file_exists($file_path))
        {
            return self::return_data(1,"传入分析文件不存在,文件路径{$file_path}");
        }
        $arr_pathinfo = pathinfo($file_path);
        if(!isset($arr_pathinfo['extension']) || strlen($arr_pathinfo['extension']) <1)
        {
            return self::return_data(1,"传入分析文件扩展名为空,文件路径{$arr_pathinfo['extension']}");
        }
        if(strtolower(trim($arr_pathinfo['extension'])) != 'xml')
        {
            return self::return_data(1,"传入分析文件非法只解析xml文件,文件路径{$arr_pathinfo['extension']}");
        }
        return self::return_data(0,"ok");
    }
    
    /**
     * 切割xml
     * @param unknown $str_file_path
     * @param unknown $str_path
     * @param number $int_split_num
     * @param string $str_type
     * @param string $bool_need_source_del
     */
    public static function split_xml($str_file_path,$str_path,$int_split_num=100,$str_type='public',$bool_need_source_del = FALSE)
    {
        if(strpos($str_file_path, 'ftp://') !== false || strpos($str_file_path, 'http://') !== false)
        {
            $result_make_dir = self::make_dir("/split/xml/temp/".np_guid_rand().".xml");
            if($result_make_dir['ret'] !=0)
            {
                return $result_make_dir;
            }
            $result_get_mix_url_content_and_save = self::get_mix_url_content_and_save($str_file_path,$result_make_dir['data_info']['absolute_dir']);
            if($result_get_mix_url_content_and_save['ret'] !=0)
            {
                return $result_get_mix_url_content_and_save;
            }
            $str_file_path = $result_make_dir['data_info']['absolute_dir'];
        }
        $result_check_file_exsist = self::check_xml_file($str_file_path);
        if($result_check_file_exsist['ret'] !=0)
        {
            return $result_check_file_exsist;
        }
        $str_content = file_get_contents($str_file_path);
        if(!self::is_xml($str_content))
        {
            return self::return_data(1,"非XML内容".var_export($str_content));
        }
        self::$old_obj_dom = new DOMDocument('1.0', 'utf-8');
        self::$old_obj_dom->loadXML($str_content);
        $xpathQuery = self::$old_obj_dom->getElementsByTagName($str_path);
        $int_length = $xpathQuery->length;
        if($int_length <1)
        {
            return self::return_data(1,"xml未找到节点[{$str_path}]".var_export($str_content));
        }
//         $num = ceil($int_length/$int_split_num);
//         var_dump($int_length);
        $j=0;
        foreach ($xpathQuery as $xpathQuery_val)
        {
            $j++;
            if($j%$int_split_num == 1)
            {
                self::$new_obj_dom = null;
                self::$new_obj_dom = new DOMDocument('1.0', 'utf-8');
                $obj_document = self::make_xml(self::$new_obj_dom, 'document');
            }
            $clonenode = self::clone_node($xpathQuery_val);
            $obj_document->appendChild($clonenode);
            if(($j+1)%$int_split_num == 1 && $j+1 >=$int_split_num)
            {
                $j = 0;
                $str_xml = self::$new_obj_dom->saveXML();
                $result_make_dir = self::make_dir("/split/xml/{$str_type}/".np_guid_rand().".xml");
                if($result_make_dir['ret'] !=0)
                {
                    return $result_make_dir;
                }
                $result_put = file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_xml);
                if(!$result_put)
                {
                    var_dump($result_make_dir['data_info']['absolute_dir']);
                }
                var_dump($result_make_dir['data_info']['absolute_dir']);
            }
        }
        if(self::$new_obj_dom !== null)
        {
            $str_xml = self::$new_obj_dom->saveXML();
            $result_make_dir = self::make_dir("/split/xml/{$str_type}/".np_guid_rand().".xml");
            if($result_make_dir['ret'] !=0)
            {
                return $result_make_dir;
            }
            $result_put = file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_xml);
            if(!$result_put)
            {
                var_dump($result_make_dir['data_info']['absolute_dir']);
            }
            var_dump($result_make_dir['data_info']['absolute_dir']);
        }
        if($bool_need_source_del)
        {
            @unlink($str_file_path);
        }
        return self::return_data(0,'ok');
    }
    
    
    /**
     * 克隆节点
     * @param unknown $node
     */
    public static function clone_node($node)
    {
        $obj_dom = self::$new_obj_dom->createElement((string)$node->nodeName);
        foreach($node->attributes as $value)
        {
            $obj_dom->setAttribute($value->nodeName,$value->value);
        }
        if(!$node->childNodes)
        {
            return $obj_dom;
        }
        foreach($node->childNodes as $child) 
        {
            if($child->nodeName=="#text")
            {
                $obj_dom->appendChild(self::$new_obj_dom->createTextNode($child->nodeValue));
            }
            else if($child->nodeName=="#cdata-section")
            {
                $obj_dom->appendChild(self::$new_obj_dom->createCDATASection($child->nodeValue));
            }
            else
            {
                $obj_dom->appendChild(self::clone_node($child));
            }
        }
        return $obj_dom;
    }
}