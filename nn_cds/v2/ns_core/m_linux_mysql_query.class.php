<?php 
include_once dirname(dirname(__FILE__))."/common.php";
\ns_core\m_load::load("ns_core.m_config");
/**
 * linux|windows & mysql 查询交互 
 * @author pan.liang
 *
 */
class m_linux_mysql_query extends m_config
{
    //文件执行基本路径
    public static $base_local_dir = null;
    //缓存时间
    public static $int_cache_time = 600;
    /**
     * 初始化
     */
    public static function init($int_cache_time=null)
    {
        self::$base_local_dir=dirname(dirname(dirname(__FILE__))).'/data/';
        if(is_int($int_cache_time) && (int)$int_cache_time >0)
        {
            self::$int_cache_time = $int_cache_time;
        }
    }
    
    public static function make_sql($str_filed,$data,$page=0,$page_size=18,$sql_in_like=true)
    {
        $str='';
        if(!is_array($data) || empty($data))
        {
            return $str;
        }
        $count = count($data);
        $data = array_chunk($data, $page_size);
        $data = isset($data[$page]) ? $data[$page] : null;
        if(!is_array($data) || empty($data))
        {
            return $str;
        }
        if($sql_in_like)
        {
            $last_data = null;
            foreach ($data as $value)
            {
                $last_data[] = " {$str_filed} like '%{$value}%' ";
            }
            $str = " and ( " . implode(" or ", $last_data) . " )";
            unset($last_data);
        }
        else
        {
            $str = " and {$str_filed} in('".implode("','", $data)."') ";
        }
        unset($data);
        return $str;
    }
    
    /**
     * 文件查询交换
     * @param unknown $queue_type
     * @param unknown $type
     * @param string $content
     * @param string $str_cp_id
     * @param string $str_sp_id
     * @param string $date
     */
    public static function linux_mysql_exchange($queue_type,$type,$content='',$str_cp_id='',$str_sp_id='',$date='',$int_cache_time=null)
    {
        self::init($int_cache_time);
        $queue_type = strtolower($queue_type);
        $type = strtolower($type);
        $str_path='';
        switch ($queue_type)
        {
            case "message":
                switch ($type)
                {
                    case 'original':
                        $str_path="api_message/{$type}/";
                        break;
                    case 'content':
                        $str_path="api_message/{$type}/";
                        break;
                }
                if(strlen($str_path) >0)
                {
                    if(isset($str_cp_id) && strlen($str_cp_id) >0)
                    {
                        $str_path.="{$str_cp_id}/";
                    }
                    if(isset($date) && strlen($date) >0)
                    {
                        $str_path.="{$date}/";
                    }
                }
            break;
        }
        $str_path=rtrim(rtrim($str_path,'/'),'\\');
        if(strlen($str_path) <1)
        {
            return self::return_data(1,"queue_type:[{$queue_type}],type:[{$type}]不支持查询");
        }
        return self::exec($str_path,$content);
    }
    
    
    /**
     * 
     * @param unknown $path
     * @param string $content
     */
    public static function exec($path,$content='')
    {
        if (strtolower(substr(php_uname(), 0, 7)) == "windows")//windows下执行
        {
            $str_exec="findstr /D /s /i /m /c:".'"'."{$content}".'"'." ".self::$base_local_dir."{$path}/*";
            $str_func = 'windows_exec';
        }
        else //LINUX下执行
        {
            $str_exec="ls -lt | grep -rl '{$content}' ".self::$base_local_dir."{$path}";
            $str_func = 'linux_exec';
        }
        $str_cache_key = self::get_cache_key($str_func, $str_exec);
        $mix_cache = self::get_cache_value($str_cache_key);
        if ($mix_cache !== FALSE)
        {
            return self::return_data(0,'ok',$mix_cache);
        }
        $last_result = self::$str_func($str_exec);
        self::set_cache_value($str_cache_key,$last_result);
        return self::return_data(0,'ok',$last_result);
    }
    
    /**
     * linux 下执行的命令
     * @param unknown $str_exec
     */
    public static function linux_exec($str_exec)
    {
        exec($str_exec,$result,$return_var);
        if(!is_array($result) || empty($result))
        {
            return null;
        }
        $last_result = null;
        foreach ($result as $val)
        {
//             if(strpos($val, self::$base_local_dir) === false)
//             {
//                 continue;
//             }
//             $val = trim(trim(str_replace(self::$base_local_dir, '', $val),'/'),'\\');
//             if(strlen($val) <1)
//             {
//                 continue;
//             }
            $arr_pathinfo = pathinfo($val);
            if(!isset($arr_pathinfo['filename']) || strlen($arr_pathinfo['filename']) <1)
            {
                continue;
            }
            $last_result[] = $arr_pathinfo['filename'];
        }
        return $last_result;
    }
    
    /**
     * windows 下执行命令
     * @param unknown $str_exec
     */
    public static function windows_exec($str_exec)
    {
        exec($str_exec,$result,$return_var);
        if(!is_array($result) || empty($result))
        {
            return null;
        }
        $last_result = null;
        foreach ($result as $val)
        {
//             if(strpos($val, self::$base_local_dir) === false)
//             {
//                 continue;
//             }
//             $val = trim(trim(str_replace("\\", '/', str_replace(self::$base_local_dir, '', $val)),'/'),'\\');
//             if(strlen($val) <1)
//             {
//                 continue;
//             }
            $arr_pathinfo = pathinfo($val);
            if(!isset($arr_pathinfo['filename']) || strlen($arr_pathinfo['filename']) <1)
            {
                continue;
            }
            $last_result[] = $arr_pathinfo['filename'];
        }
        return $last_result;
    }
    
    /**
     * 获取缓存键
     * @param unknown $str_func
     * @param unknown $str_exec
     */
    public static function get_cache_key($str_func,$str_exec)
    {
        return md5($str_func."|#|".$str_exec);
    }
    
    /**
     * 获取缓存
     * @param unknown $key
     */
    public static function get_cache_value($key)
    {
        return nl_view_cache::get(m_config::get_dc(), $key);
    }
    
    /**
     * 设置缓存
     * @param unknown $key
     * @param unknown $value
     * @param number $cache_time
     * @return Ambigous <true, false>
     */
    public static function set_cache_value($key,$value)
    {
        return nl_view_cache::set(m_config::get_dc(), $key, $value,self::$int_cache_time);
    }
}