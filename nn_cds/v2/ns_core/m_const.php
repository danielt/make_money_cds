<?php
/**
 * 播控V2常量
 */

/**
 * 播控队列状态码
 */
/*****平台返回值常量*****/
define('NS_CDS_SUCCE',0);//成功
define('NS_CDS_FAIL', 1);//失败
/*****中心同步指令任务状态常量*****/
define('NS_CDS_OP_WAIT', 0); //中心同步指令等待注入c2
define('NS_CDS_OP_LOADING', 1); //中心同步指令正在注入c2
define('NS_CDS_OP_WAIT_CLIP', 2); //中心同步指令等待切片
define('NS_CDS_OP_CLIP_LOADING', 3); //中心同步指令正在切片
define('NS_CDS_OP_CANCEL', 4); //中心同步指令注入c2取消
define('NS_CDS_OP_UNAUDITED', 6); //中心同步指令未审核
define('NS_CDS_OP_WAIT_SP', 7); //中心同步指令等待前置sp
define('NS_CDS_OP_ENCODE', 8); //中心同步指令正在转码
define('NS_CDS_OP_WAIT_ENCODE', 9); //中心同步指令等在转码
define('NS_CDS_OP_ENCODE_FAIL', 10); //中心同步指令转码失败
/*****CDN任务状态常量*****/
define('NS_CDS_CDN_SUCCE', 0); //C2注入CDN成功
define('NS_CDS_CDN_FAIL', -1); //C2注入CDN失败
define('NS_CDS_CDN_WAIT', 1); //C2等待注入CDN
define('NS_CDS_CDN_LOADING', 5); //C2正在注入CDN
define('NS_CDS_CDN_CANCEL', 7); //C2注入CDN取消
define('NS_CDS_CDN_CID_LOADING', 8); //C2注入CDN成功正在获取播放ID或播放串
define('NS_CDS_CDN_CID_FAIL', 9); //C2注入CDN成功获取播放ID或播放串失败
define('NS_CDS_CDN_FTP_CONN_FAIL', 10); //C2注入CDN的FTP链接失败
define('NS_CDS_CDN_FTP_MEDIA_FAIL', 11); //C2注入CDN的FTP片源获取失败
define('NS_CDS_CDN_LINE_WAIT', 12); //C2注入CDN等待内容上下线
define('NS_CDS_CDN_LINE_LOADING', 13); //C2注入CDN内容上下线正在注入中
define('NS_CDS_CDN_LINE_FAIL', 14); //C2注入CDN内容上下线失败
/*****第三方回调CDN状态*****/
define('NS_CDS_CDN_NOTIFY_FAIL', -1); //CDN回调失败 此处仅用于异常情况，每个CDN返回的回调失败可能状态码不一致。 0统一标识成功

define('NS_CDS_CDN_NOTIFY_FAIL_1', 1); //CDN回调失败 ，未知错误。 0统一标识成功--c1接口使用
define('NS_CDS_CDN_NOTIFY_FAIL_2', 2); //CDN回调失败 ，无法获取模板同步指令。 0统一标识成功--c1接口使用
define('NS_CDS_CDN_NOTIFY_FAIL_3', 3); //CDN回调失败 ，指令文件无法正确解析。 0统一标识成功--c1接口使用

define('NS_CDS_CDN_NOTIFY_SUCCE', 0); //CDN回调成功
define('NS_CDS_CDN_WAIT_CID', 6); //C2注入CDN成功等待回调播放ID或播放串
/*****EPG任务状态常量*****/
define('NS_CDS_EPG_SUCCE', 99); //注入EPG成功
define('NS_CDS_EPG_FAIL', 96); //注入EPG失败
define('NS_CDS_EPG_LOADING', 98); //正在注入EPG
define('NS_CDS_EPG_WAIT', 97); //等待注入EPG
define('NS_CDS_EPG_CANCEL', 100); //取消注入EPG
define('NS_CDS_EPG_LINE_SUCCESS', 0); //上下线注入成功
define('NS_CDS_EPG_LINE_FAIL', 2); //上下线注入失败
define('NS_CDS_EPG_LINE_LOADING', 3); //上下线等待反馈

/*****注入EPG回调状态*****/
define('NS_CDS_EPG_NOTIFY_SUCCE', 0); //注入EPG回调成功
define('NS_CDS_EPG_NOTIFY_NOTFOUND', 7); //注入EPG回调未找到对应内容
define('NS_CDS_EPG_LINE_NOTIFY_SUCCESS', 0); //注入EPG回调成功
define('NS_CDS_EPG_LINE_NOTIFY_FAIL', 4); //注入EPG回调注入失败
define('NS_CDS_EPG_LINE_NOTIFY_NOT_VOD', 5); //注入EPG回调未找到对应内容

//队列状态
define('NS_TASK_FINA_STATE',10000); //最终注入状态
/*****切片队列状态******/
define('NS_CDS_DRM_FAIL','drm_fail');//DRM加密失败
define('NS_CDS_LIVE_TO_MEDIA_WAIT','live_to_media_wait');//直播转点播片源

/*****指令状态池状态******/
define('NS_CDS_COMMAND_FINALLY', 0);//最终状态成功
define('NS_CDS_COMMAND_OP_WAIT', 1);//中心同步指令等待注入
define('NS_CDS_COMMAND_WAIT_CLIP', 2);//中心同步指令等待切片
define('NS_CDS_COMMAND_CLIP_LOADING', 3);//中心同步指令正在切片
define('NS_CDS_COMMAND_OP_CANCEL', 4);//中心同步指令取消注入
define('NS_CDS_COMMAND_WAIT_SP', 5);//中心同步指令等待前置sp
define('NS_CDS_COMMAND_UNAUDITED', 6);//中心同步指令未审核
define('NS_CDS_COMMAND_ENCODE', 7);//中心同步指令正在转码
define('NS_CDS_COMMAND_WAIT_ENCODE', 8);//中心同步指令等在转码
define('NS_CDS_COMMAND_ENCODE_FAIL', 9);//转码失败
define('NS_CDS_COMMAND_OP_FAIL', 10);//中心同步指令注入失败
define('NS_CDS_COMMAND_C2_WAIT', 11);//C2队列等待注入
define('NS_CDS_COMMAND_CDN_LOADING', 12);//C2队列正在注入CDN
define('NS_CDS_COMMAND_CDN_SUCCESS', 13);//C2队列注入CDN成功
define('NS_CDS_COMMAND_CDN_FAIL', 14);//C2队列注入CDN失败
define('NS_CDS_COMMAND_CDN_CANCEL', 15);//C2队列注入CDN取消
define('NS_CDS_COMMAND_EPG_LOADING', 16);//C2队列正在注入EPG
define('NS_CDS_COMMAND_EPG_SUCCESS', 17);//C2队列注入EPG成功
define('NS_CDS_COMMAND_EPG_FAIL', 18);//C2队列注入EPG失败
define('NS_CDS_COMMAND_EPG_CANCEL', 19);//C2队列注入EPG取消
define('NS_CDS_COMMAND_C2_CANCEL', 20);//C2队列取消
define('NS_CDS_COMMAND_CDN_WAIT', 21);//C2队列等待注入CDN
define('NS_CDS_COMMAND_EPG_WAIT', 22);//C2队列等待注入EPG
define('NS_CDS_COMMAND_OP_TO_CLIP_FAIL', 23);//中心指令发送到切片队列失败
define('NS_CDS_COMMAND_CLIP_FAIL', 24);//切片失败
define('NS_CDS_COMMAND_CLIP_SUCCESS', 25);//切片成功
define('NS_CDS_COMMAND_ENCODE_SUCCESS', 26);//转码成功
define('NS_CDS_COMMAND_ENCODE_LOADING', 27);//正在转码
define('NS_CDS_COMMAND_CLIP_HANDLE', 31);//切片任务已下发

/******透传队列常量********/
define('NS_CDS_PASS_QUEUE_SUCCESS', 0);//注入成功
define('NS_CDS_PASS_QUEUE_WAIT', 1);//等待注入
define('NS_CDS_PASS_QUEUE_LOADING', 2);//正在注入
define('NS_CDS_PASS_QUEUE_FAIL', 3);//注入失败
define('NS_CDS_PASS_QUEUE_WAIT_NOTIFY', 4);//等待反馈

/**
 * 播控队列名命名常量
 */
define('NS_CDS_MESSAGE_RECEIVE','message_receive');//消息接收
define('NS_CDS_MESSAGE_QUEUE','message_queue');//消息队列
define('NS_CDS_CENTER_IMPORT_QUEUE','center_import_queue');//中心注入队列
define('NS_CDS_CENTER_OP_QUEUE','center_op_queue');//中心同步队列
define('NS_CDS_CLIP_QUEUE','clip_queue');//切片队列
define('NS_CDS_TRANSCODE_QUEUE','transcode_queue');//转码队列
define('NS_CDS_EPG_QUEUE','epg_queue');//注入EPG队列
define('NS_CDS_EPG_LINE_QUEUE','epg_line_queue');//注入EPG队列
define('NS_CDS_CDN_QUEUE','cdn_queue');//注入CDN队列
define('NS_CDS_PASS_QUEUE','pass_queue');//注入CDN队列

/**
 * 播控操作行为常量
 */
define('NS_CDS_ADD','add');//新增
define('NS_CDS_DELETE','destroy');//删除
define('NS_CDS_CANCEL','cancel');//取消队列
define('NS_CDS_DELETE_V2','delete');//删除
define('NS_CDS_MODIFY','modify');//修改
define('NS_CDS_ONLINE','online');//上线
define('NS_CDS_UNLINE','unline');//下线
/**
 * 播控操作类型
 */
define ('NS_CDS_MEDIA','media');
define ('NS_CDS_INDEX','index');
define ('NS_CDS_VIDEO','video');
define ('NS_CDS_VIDEO_V2','vod');
define ('NS_CDS_LIVE_MEDIA','live_media');
define ('NS_CDS_LIVE_INDEX','live_index');
define ('NS_CDS_LIVE','live');
define ('NS_CDS_PLAYBILL','playbill');
define ('NS_CDS_FILE','file');
define ('NS_CDS_FILE_PACKAGE','file_package');
define ('NS_CDS_EPG_FILE','epg_file');
define ('NS_CDS_PRODUCT','product');
define ('NS_CDS_SEEKPOINT','seekpoint');

