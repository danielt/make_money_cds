<?php
/**
 * 静态配置类  上游有所有的配置参数就在这个类里面 提供调用
 * @author pan.liang
 */
\ns_core\m_load::load_old("nn_logic/cp/cp.class.php");
\ns_core\m_load::load_old("nn_logic/sp/sp.class.php");
\ns_core\m_load::load_old("nn_logic/fuse/fuse_cp.class.php");
\ns_core\m_load::load("ns_core.m_public");
class m_config extends m_public
{
    /**
     * 所有的上游配置
     * @var $arr_cp_config = array(
     *          'youku'=>array( // 上游CP标示,nns_cp 表字段 nns_id 值
     *                  'nns_id'=>'',
     *                  'nns_name'=>'',
     *                  'nns_other'=>'',
     *                  'nns_config'=>array( // nns_config 字段  json解析出来的  数组
     *                          '转码开关配置'=>'',
     *                          '获取图片配置'=>'',
     *                          '消息反馈配置'=>'',
     *                          ... ...
     *                  ),  //上游平台标示,nns_cp 表字段 信息
     *          )... ...
     * );
     */
    public static $arr_cp_config = null;

    /**
     * 所有的下游配置
     * @var $arr_sp_config = array(
     *          'starcor'=>array( //下游平台标示,nns_mgtvbk_sp 表字段 nns_id 值
     *                  'nns_id'=>'',
     *                  'nns_name'=>'',
     *                  'nns_other'=>'',
     *                  'nns_config'=>array( // nns_config 字段  json解析出来的  数组
     *                          '转码开关配置'=>'',
     *                          '获取图片配置'=>'',
     *                          '消息反馈配置'=>'',
     *                          ... ...
     *                  ),  //下游平台标示,nns_mgtvbk_sp 表字段 信息
     *          )... ...
     * );
     */
    public static $arr_sp_config = null;


    public static $arr_fuse_cp_org_list = null;

    public static $arr_fuse_cp_src_list = null;

    /**
     * 初始化
     * @param string $type 上下游标示  sp 下游 | cp 上游
     * @param string $mask 上下游nns_cp 或者 nns_mgtvbk_sp 字段的GUID
     * @param array $config 配置数组  array(
     *                  'nns_id'=>'',
     *                  'nns_name'=>'',
     *                  'nns_other'=>'',
     *                  'nns_config'=>array( // nns_config 字段  json解析出来的  数组
     *                          '转码开关配置'=>'',
     *                          '获取图片配置'=>'',
     *                          '消息反馈配置'=>'',
     *                          ... ...
     *                  ),
     *          )... ...
     */
    public static function set($type,$mask,$config)
    {
        if(!in_array($type, array('cp','sp')))
        {
            return self::return_data(1,"设置公共参数配置值时,上下游标示错误,值[{$type}]");
        }
        if(strlen($mask) <1)
        {
            return self::return_data(1,"设置公共参数配置值时,上下游标示[{$type}],字段的GUID为空");
        }
        $config = (is_array($config) && !empty($config)) ? $config : null;
        $str_config = '$arr_'.$type.'_config';
        if(isset(self::$str_config[$mask]) && is_array(self::$str_config[$mask]) && !empty(self::$str_config[$mask]))
        {
            return self::return_data(0,"ok");
        }
        self::$str_config[$mask] = $config;
        return self::return_data(0,"ok");
    }

    /**
     * 获取配置信息
     * @param string $type 上下游标示  sp 下游 | cp 上游
     * @param string $mask 上下游nns_cp 或者 nns_mgtvbk_sp 字段的GUID
     */
    public static function get($type,$mask)
    {
        $arr_config = null;
        if(!in_array($type, array('cp','sp')))
        {
            return self::return_data(0,"获取公共参数配置值时,上下游标示错误,值[{$type}]");
        }
        if(strlen($mask)<1)
        {
            return self::return_data(1,"获取公共参数配置值时,上下游标示[{$type}],字段的GUID为空");
        }
        $str_config = '$arr_'.$type.'_config';
        if(isset(self::$str_config[$mask]) && is_array(self::$str_config[$mask]) && !empty(self::$str_config[$mask]))
        {
            $arr_config = self::$str_config[$mask];
        }
        $arr_config = (is_array($arr_config) && !empty($arr_config)) ? $arr_config : null;
        return self::return_data(0,"ok",$arr_config);
    }


    /**
     * 获取CP信息
     * @param $cp_id
     * @return Ambigous|array
     */
    public static function _get_cp_info($cp_id)
    {
        if(strlen($cp_id) <1)
        {
            return self::return_data(1, '获取单个CP配置时$cp_id参数为空');
        }
        if(isset(self::$arr_cp_config[$cp_id]))
        {
            return self::return_data(0, 'ok',self::$arr_cp_config[$cp_id]);
        }
        $result_cp = nl_cp::query_by_id(self::get_dc(),$cp_id);
        if($result_cp['ret'] !=0)
        {
            return $result_cp;
        }
        if(!isset($result_cp['data_info']) || !is_array($result_cp['data_info']) || empty($result_cp['data_info']))
        {
            self::$arr_cp_config[$cp_id] = null;
            return self::return_data(0, 'ok',self::$arr_cp_config[$cp_id]);
        }
        if(self::is_json($result_cp['data_info']['nns_config']))
        {
            $result_cp['data_info']['nns_config'] = json_decode($result_cp['data_info']['nns_config'],true);
        }
        else
        {
            $result_cp['data_info']['nns_config'] = null;
        }
        self::$arr_cp_config[$cp_id]=$result_cp['data_info'];
        return self::return_data(0, 'ok',self::$arr_cp_config[$cp_id]);
    }

    /**
     * 获取CP信息
     * @param unknown $cp_id
     */
    public static function _get_sp_info($sp_id)
    {
        if(strlen($sp_id) <1)
        {
            return self::return_data(1, '获取单个SP配置时$sp_id参数为空');
        }
        if(isset(self::$arr_sp_config[$sp_id]))
        {
            return self::return_data(0, 'ok',self::$arr_sp_config[$sp_id]);
        }
        $result_sp = nl_sp::query_by_id(self::get_dc(),$sp_id);
        if($result_sp['ret'] !=0)
        {
            return $result_sp;
        }
        if(!isset($result_sp['data_info'][0]) || !is_array($result_sp['data_info'][0]) || empty($result_sp['data_info'][0]))
        {
            self::$arr_sp_config[$sp_id] = null;
            return self::return_data(0, 'ok',self::$arr_sp_config[$sp_id]);
        }
        if(self::is_json($result_sp['data_info'][0]['nns_config']))
        {
            $result_sp['data_info']['nns_config'] = json_decode($result_sp['data_info'][0]['nns_config'],true);
        }
        else
        {
            $result_sp['data_info']['nns_config'] = null;
        }
        self::$arr_sp_config[$sp_id]=$result_sp['data_info'];
        return self::return_data(0, 'ok',self::$arr_sp_config[$sp_id]);
    }

    /**
     * 通过CP 查询绑定的 sp
     * @param unknown $cp_id
     */
    public static function _get_cp_bind_sp_info($cp_id)
    {
        if(strlen($cp_id) <1)
        {
            return self::return_data(1, '获取CP绑定的SP时CPID为空');
        }
        $result_sp = nl_sp::get_cp_bind_sp_info(self::$obj_dc,$cp_id);
        if($result_sp['ret'] !=0)
        {
            return $result_sp;
        }
        if(!isset($result_sp['data_info']['group_concat_sp_id']) || strlen($result_sp['data_info']['group_concat_sp_id']) <1)
        {
            return m_config::return_data(1,'查询无绑定的sp信息');
        }
        $arr_sp = explode(',', $result_sp['data_info']['group_concat_sp_id']);
        if(!is_array($arr_sp) || empty($arr_sp))
        {
            return m_config::return_data(1,'查询无绑定的sp信息');
        }
        $last_info = null;
        foreach ($arr_sp as $sp_val)
        {
            if(strlen($sp_val) <1)
            {
                continue;
            }
            $result_sp_info = self::_get_sp_info($sp_val);
            if($result_sp_info['ret'] !=0 || !isset($result_sp_info['data_info']) || empty($result_sp_info['data_info']) || !is_array($result_sp_info['data_info']))
            {
                continue;
            }
            $last_info[$sp_val] = $result_sp_info['data_info'];
        }
        return m_config::return_data(0,'ok',$last_info);
    }

    /**
     * 获取所有sP信息
     * @param unknown $cp_id
     */
    public static function _get_all_sp_info()
    {
        $result_sp = nl_sp::query_all(self::get_dc());
        if($result_sp['ret'] !=0 || !isset($result_sp['data_info']) || !is_array($result_sp['data_info']) || empty($result_sp['data_info']))
        {
            return $result_sp;
        }
        foreach ($result_sp['data_info'] as $sp_val)
        {
            $sp_val['nns_config'] = (self::is_json($sp_val['nns_config'])) ? json_decode($sp_val['nns_config'],true) : null;
            self::$arr_sp_config[$sp_val['nns_id']] = $sp_val;
        }
        return self::return_data(0, 'ok',self::$arr_sp_config);
    }


    /**
     * 获取所有CP信息
     * @param unknown $cp_id
     */
    public static function _get_all_cp_info()
    {
        $result_cp = nl_cp::query_all(self::$obj_dc);
        if($result_cp['ret'] !=0 || !isset($result_cp['data_info']) || !is_array($result_cp['data_info']) || empty($result_cp['data_info']))
        {
            return $result_cp;
        }
        foreach ($result_cp['data_info'] as $cp_val)
        {
            $cp_val['nns_config'] = (self::is_json($cp_val['nns_config'])) ? json_decode($cp_val['nns_config'],true) : null;
            self::$arr_cp_config[$cp_val['nns_id']] = $cp_val;
        }
        return self::return_data(0, 'ok',self::$arr_cp_config);
    }

    /**
     * 注入CP查询出融合CP清单（单个或者多个，或者没有）
     * @param string $cp_org_id  注入CP
     */
    public static function _query_fuse_cp_org($cp_org_id)
    {
        if(strlen($cp_org_id) <1)
        {
            return self::return_data(1, '');
        }
        if(isset(self::$arr_fuse_cp_org_list[$cp_org_id]))
        {
            return self::return_data(0, 'ok',self::$arr_fuse_cp_org_list[$cp_org_id]);
        }
        $result_fuse_org_cp = nl_fuse_cp::query_fuse_cp(self::$obj_dc, $cp_org_id,0);
        if($result_fuse_org_cp['ret'] !=0 || !isset($result_fuse_org_cp['data_info']) || !is_array($result_fuse_org_cp['data_info']) || empty($result_fuse_org_cp['data_info']))
        {
            self::$arr_fuse_cp_org_list[$cp_org_id] = null;
            return $result_fuse_org_cp;
        }
        self::$arr_fuse_cp_org_list[$cp_org_id] = $result_fuse_org_cp['data_info'];
        return self::return_data(0, 'ok',self::$arr_fuse_cp_org_list[$cp_org_id]);
    }

    /**
     * 融合CP查询出注入CP清单（单个，或者没有）
     * @param string $cp_src_id  融合CPID
     */
    public static function _query_fuse_cp_src($cp_src_id)
    {
        if(strlen($cp_src_id) <1)
        {
            return self::return_data(1, '');
        }
        if(isset(self::$arr_fuse_cp_src_list[$cp_src_id]))
        {
            return self::return_data(0, 'ok',self::$arr_fuse_cp_src_list[$cp_src_id]);
        }
        $result_fuse_src_cp = nl_fuse_cp::query_fuse_cp_src(self::$obj_dc, $cp_src_id,0);
        if($result_fuse_src_cp['ret'] !=0 || !isset($result_fuse_src_cp['data_info']) || !is_array($result_fuse_src_cp['data_info']) || empty($result_fuse_cp['data_info']))
        {
            self::$arr_fuse_cp_src_list[$cp_src_id] = null;
            return $result_fuse_src_cp;
        }
        self::$arr_fuse_cp_src_list[$cp_src_id] = $result_fuse_src_cp['data_info'];
        return self::return_data(0, 'ok',self::$arr_fuse_cp_src_list[$cp_src_id]);
    }

    /**
     * 下游下发的CDN時内容存储执行文件（这个属于外部对接处理的范畴 使用该类）
     * @param string $sp_id  下游平台sp 标示
     * @param string $type CDN注入类型
     * @param string $action CDN注入行为动作
     * @param string $cdn_message 注入的消息实体内容
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public static function write_cdn_import_execute_log($sp_id,$type,$action,$cdn_message)
    {
        if(!isset($sp_id) || strlen($sp_id) <1)
        {
            return self::return_data(1,'sP标示为空');
        }
        if(strlen($cdn_message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = "{$sp_id }_{$type}_{$time}_{$action}_{$three_rand}.xml";
        $message_url = "/cdn_import/{$sp_id}/inject_xml/".date("Ymd")."/".date("H")."/{$file_name}";
        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $cdn_message);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }

    /**
     * 通用项目配置文件配置查询。
     * 项目配置文件根据项目自由化配置
     * 此配置文件为 nn_cds/v2/ns_api/{project}/config.php
     * @param $project //项目名,引入公共文件时可以根据evn::get('project');获取传入
     * @param $key //配置key
     * @return string 反馈配置key的值
     * xinxin.deng 2018/11/21 16:41
     */
    public static function get_project_config($project, $key)
    {
        if (empty($project) || empty($key))
        {
            return '';
        }
        include dirname(dirname(__FILE__)) . '/ns_api/' . $project .'/config.php';

        if (isset($$key))
        {
            return $$key;
        }
        else
        {
            return '';
        }
    }
}