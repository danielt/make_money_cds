<?php
\ns_core\m_load::load_old("nn_logic/nl_common.func.php");
// \ns_core\m_load::load_np("np_guid.class.php");
\ns_core\m_load::load_np("np_ftp.php");
\ns_core\m_load::load("ns_core.m_log");
/**
 * 全局公共参数 
 * 功能：
 *      1、DC、redis_dc 初始化
 *      2、消息公共输出模板 return
 *      3、检查参数是否是 json 或者 xml
 *      4、文件、文件夹创建、删除
 * @author pan.liang
 */
class m_public extends m_log
{
    /**
     * 执行的队列类型    true 定时器后台运行 | false 手动在后台运行的队列 
     * @var boolen $flag_manual_operation
     */
    public static $flag_operation = true;
    
    
    /**
     * @var object $obj_dc  mysql 数据库操作对象
     */
    public static $obj_dc = null;
    /**
     * @var object $obj_hash_dc  mysql 数据库操作对象
     */
    public static $obj_hash_dc = null;
    
    /**
     * 检查方法类是否含有此方法
     * @param object $obj 类对象
     * @param string $str_method 方法
     * @return multitype:number string Ambigous <NULL, unknown>
     */
    public static function method_exists($obj,$str_method)
    {
        if(!is_object($obj))
        {
            unset($obj);
            unset($str_method);
            return self::return_data(1,'检查obj方法是否存在,obj为非对象');
        }
        if(!is_string($str_method) || strlen($str_method) <1)
        {
            unset($obj);
            unset($str_method);
            return self::return_data(1,'检查obj方法是否存在,方法为空，或者非字符串');
        }
        $bool = method_exists($obj,$str_method);
        unset($obj);
        unset($str_method);
        return self::return_data(0,'检查obj方法是否存在,方法为空，或者非字符串',$bool);
    }
    
    /**
     * 设置操作标示
     * @param string $flag_operation
     */
    public static function set_operation($flag_operation=true)
    {
        self::$flag_operation = $flag_operation ? true : false;
    }
    
    /**
     * 设置DC
     * @param unknown $obj_dc
     */
    public static function set_dc($obj_dc=null)
    {
        if(isset(self::$obj_dc) && is_object(self::$obj_dc))
        {
            return true;
        }
        if($obj_dc === null || !is_object($obj_dc))
        {
            $obj_dc = nl_get_dc(array(
                'db_policy' => NL_DB_WRITE,
                'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
            ));
        }
        self::$obj_dc = $obj_dc;
        return true;
    }
    /**
     * 设置DC
     */
    public static function set_hash_dc()
    {
        if(isset(self::$obj_hash_dc) && is_object(self::$obj_hash_dc))
        {
            return true;
        }
        $obj_hash_dc = nl_get_dc(
            array('db_policy' => NL_DB_WRITE | NL_DB_READ,'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE),
            array('db_type' => NP_DB_TYPE_HASH_TABLE_MYSQL)
        );
        self::$obj_hash_dc = $obj_hash_dc;
        return true;
    }
    /**
     * 设置DC
     */
    public static function get_dc()
    {
        if(isset(self::$obj_dc) && is_object(self::$obj_dc))
        {
            return self::$obj_dc;
        }
        self::set_dc();
        return self::$obj_dc;
    }

    /**
     * 设置HASHDC
     */
    public static function get_hash_dc()
    {
        if(isset(self::$obj_hash_dc) && is_object(self::$obj_hash_dc))
        {
            return self::$obj_hash_dc;
        }
        self::set_hash_dc();
        return self::$obj_hash_dc;
    }
    /**
     * 获取redis DC （由于没有长连接redis 获取redis的时候都调用一次） 
     * @param unknown $obj_dc
     * @param string $obj_redis_dc
     */
    public static function get_redis_dc()
    {
        return nl_get_redis();
    }
    
    /**
     * 判断是否是json
     * @param unknown $string
     * @return boolean
     */
    static public function is_json($string)
    {
        if(!is_string($string))
        {
            return false;
        }
        $string = strlen($string) <1 ? '' : trim($string);
        if(strlen($string) <1)
        {
            return false;
        }
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
    
    /**
     * 判断是否是xml
     * @param unknown $string
     * @return boolean
     */
    static public function is_xml($string)
    {
        if(!is_string($string))
        {
            return false;
        }
        $string = strlen($string) < 1 ? '' : trim($string);
        if(strlen($string) <1)
        {
            return false;
        }
        $xml_parser = xml_parser_create();
        if (! xml_parse($xml_parser, $string, true))
        {
            xml_parser_free($xml_parser);
            return false;
        }
        return true;
    }
    
    /**
     * 公用消息反馈
     * @param int $ret
     * @param string $reason
     * @param string $data_info
     * @param string $ext_info
     * @return array:number string Ambigous <NULL, unknown>
     */
    public static function return_data($ret=1,$reason='unkonwn error',$data_info=null,$ext_info=null)
    {
        $ret = (isset($ret) && strlen($ret)>0) ? $ret : '1';
        $data_info = $data_info === null ? null : $data_info;
        $ext_info = $ext_info === null ? null : $ext_info;
        if($ret !=0)
        {
            return array(
                'ret'=>$ret,
                'reason'=>$reason,
                'error_data_info'=>$data_info,
                'error_ext_info'=>$ext_info,
            );
        }
        return array(
            'ret'=>$ret,
            'reason'=>$reason,
            'data_info'=>$data_info,
            'ext_info'=>$ext_info,
        );
    }
    
    /**
     * 上游下发的内容存储执行文件存储（这个属于外部对接处理的范畴 使用该类）
     * @param string $cp_id  上游平台cp 标示
     * @param string $queue_id 消息队列GUID 如果传入以传入的为准，如果没传入自生生产一个 32位的 guid
     * @param string $message $message 消息体  支持 array | json | string | xml
     * @param string $type 类型   original 解析了这个original 类型的内容后才能 获取到 content内容信息 | content 消息队列解析的 content内容 需要做分发和注入队列的
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public static function write_message_recive_execute_log($cp_id,$queue_id=null,$message='',$type='content')
    {
        if(!isset($cp_id) || strlen($cp_id) <1)
        {
            return self::return_data(1,'CP标示为空');
        }
        $type = (isset($type) && $type == 'original') ? $type : 'content';
        $message = is_array($message) ? json_encode($message,JSON_UNESCAPED_UNICODE) : $message;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $queue_id = (isset($queue_id) && strlen($queue_id)>0) ? $queue_id : np_guid_rand();
        $file_name = self::is_xml($message) ? $queue_id.'.'.'xml' : $queue_id.'.'.'txt';
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        $message_url = "/api_message/{$type}/{$cp_id}/".date("Ymd")."/".date("H")."/{$file_name}";
        if(file_exists(dirname(dirname(dirname(__FILE__))).'/data'.$message_url))
        {
            $queue_id_rand = np_guid_rand();
            $message_url = "/api_message/{$type}/{$cp_id}/".date("Ymd")."/".date("H")."/{$queue_id_rand}_{$file_name}";
        }
        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $message);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }


    /**
     * 公共定时器文件执行消息
     * @param string $timer_path
     * @param string $message
     * @param string $child_path
     * @param string $queue_id
     * @return multitype:number |array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public static function timer_write_log($timer_path,$message='',$child_path='',$queue_id=null)
    {
        if(strlen($timer_path)<1)
        {
            return self::return_data(1,'队列类型为空');
        }
        $message = is_array($message) ? json_encode($message,JSON_UNESCAPED_UNICODE) : $message;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $str_time = date("H");
        $file_name = self::is_xml($message) ? $str_time.'.xml' : $str_time.'.txt';
        $str_queue_info='';
        if(isset($queue_id) && strlen($queue_id))
        {
            $str_queue_info ="队列ID[{$queue_id}]执行消息:\r\n";
        }
        $child_path = trim(trim(trim($child_path,'/'),'\\'));
        $str_message_info = "[" . date("Y-m-d H:i:s") . "]";
        $message = $str_message_info . $str_queue_info.$message."\r\n";
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        if(strlen($child_path) >1)
        {
            $message_url = "/timer_log/{$str_operation}/{$timer_path}/{$child_path}/".date("Ymd")."/{$file_name}";
        }
        else
        {
            $message_url = "/timer_log/{$str_operation}/{$timer_path}/".date("Ymd")."/{$file_name}";
        }
        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $message,FILE_APPEND);
        echo $message."<br>";
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }
    
    /**
     * 公共文件执行消息
     * @param string $queue_type 队列类型  message  消息队列 | center_import 中心注入指令队列 | center_sync 中心同步指令队列 | clip 切片队列 | transcode 转码队列 | epg_import EPG注入队列 | cdn_import CDN注入队列
     * @param string $action   队列操作类型 request 接收 |  push 写入 | pop 输出
     * @param string $child_path 子文件路径  可为空
     * @param mixed $message  消息体  支持 array | json | string | xml
     * @param string $queue_id  队列GUID尽量写入队列ID 便于快速查询问题 
     * @return multitype:number |array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public static function base_write_log($queue_type,$action,$message='',$child_path='',$queue_id=null)
    {
        if(strlen($queue_type)<1)
        {
            return self::return_data(1,'队列类型为空');
        }
        if(!isset($action) || strlen($action) <1)
        {
            return self::return_data(1,'队列操作行为标示为空');
        }
        $message = is_array($message) ? json_encode($message,JSON_UNESCAPED_UNICODE) : $message;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $str_time = date("H");
        $file_name = self::is_xml($message) ? $str_time.'.xml' : $str_time.'.txt';
        $str_queue_info='';
        if(isset($queue_id) && strlen($queue_id))
        {
            $str_queue_info ="队列ID[{$queue_id}]执行消息:\r\n";
        }
        $child_path = trim(trim(trim($child_path,'/'),'\\'));
        $message = $str_queue_info.$message."\r\n";
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        if(strlen($child_path) >1)
        {
            $message_url = "/base_log/{$queue_type}/{$str_operation}/{$action}/{$child_path}/".date("Ymd")."/{$file_name}";
        }
        else
        {
            $message_url = "/base_log/{$queue_type}/{$str_operation}/{$action}/".date("Ymd")."/{$file_name}";
        }
        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $message,FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }


    /**
     * 公共文件执行消息
     * @param string $queue_type 队列类型  message  消息队列 | center_import 中心注入指令队列 | center_sync 中心同步指令队列 | clip 切片队列 | transcode 转码队列 | epg_import EPG注入队列 | cdn_import CDN注入队列
     * @param string $action   队列操作类型 request 接收 |  push 写入 | pop 输出
     * @param string $child_path 子文件路径  可为空
     * @param mixed $message  消息体  支持 array | json | string | xml
     * @param string $queue_id  队列GUID尽量写入队列ID 便于快速查询问题
     * @return multitype:number |array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public static function base_callback_log($queue_type,$action,$message='',$child_path='',$queue_id=null)
    {
        if(strlen($queue_type)<1)
        {
            return self::return_data(1,'队列类型为空');
        }
        if(!isset($action) || strlen($action) <1)
        {
            return self::return_data(1,'队列操作行为标示为空');
        }
        $message = is_array($message) ? json_encode($message,JSON_UNESCAPED_UNICODE) : $message;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $str_time = date("H");
        $file_name = self::is_xml($message) ? $str_time.'.xml' : $str_time.'.txt';
        $str_queue_info='';
        if(isset($queue_id) && strlen($queue_id))
        {
            $str_queue_info ="队列ID[{$queue_id}]执行消息:\r\n";
        }
        $child_path = trim(trim(trim($child_path,'/'),'\\'));
        $message = $str_queue_info.$message."\r\n";
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        if(strlen($child_path) >1)
        {
            $message_url = "/callback_log/{$queue_type}/{$str_operation}/{$child_path}/".date("Ymd")."/{$file_name}";
        }
        else
        {
            $message_url = "/callback_log/{$queue_type}/{$str_operation}/".date("Ymd")."/{$file_name}";
        }
        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $message,FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }

    /**
     * 外部对接队列执行(发送/接收)日志处理（这个属于外部对接处理的范畴 使用该类 请求一次生产一次文件）
     * @param string $queue_type 队列类型  message  消息队列 | center_import 中心注入指令队列 | center_sync 中心同步指令队列 | clip 切片队列 | transcode 转码队列 | epg_import EPG注入队列 | cdn_import CDN注入队列
     * @param string $sp_id  下游平台sp 标示 如果为空 置为cds_default_sp
     * @param string $cp_id  上游平台cp 标示 如果为空 置为cds_default_cp
     * @param string $action 操作行为  import 标准注入 | unline 上下线 默认import
     * @param string $in_out in 输入 | out 输出 默认in
     * @param string $type 媒资类型 category 栏目 |  video 主媒资 | index 分集 | media 片源 | live 直播 | live_index 直播分集 | live_media 直播片源 | playbill 节目单 | file 文件 | package 包 | asset_category 媒资栏目 | asset 媒资包 | product 产品包 | terminal_UI 终端UI
     * @param string $queue_id  队列GUID 如果为空生成  GUID
     * @param mixed $message 消息体  支持 array | json | string | xml
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public static function write_queue_send_execute_log($queue_type,$sp_id='',$cp_id='',$action='',$in_out='',$type='',$queue_id=null,$message='')
    {
        if(strlen($queue_type)<1)
        {
            return self::return_data(1,'队列类型为空');
        }
        if(!isset($action) || strlen($action) <1)
        {
            $action = 'import';
        }
        if(!isset($in_out) || strlen($in_out) <1)
        {
            $in_out = 'in';
        }
        if(!isset($sp_id) || strlen($sp_id) <1)
        {
            $sp_id='cds_default_sp';
        }
        if(!isset($cp_id) || strlen($cp_id) <1)
        {
            $cp_id='cds_default_cp';
        }
        if(strlen($type) < 1)
        {
            $type = "cds_default_type";
        }
        $message = is_array($message) ? json_encode($message,JSON_UNESCAPED_UNICODE) : $message;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $queue_id = (isset($queue_id) && strlen($queue_id)>0) ? $queue_id : np_guid_rand();
        $file_name = self::is_xml($message) ? $queue_id.'.'.'xml' : $queue_id.'.'.'txt';
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        $message_url = "/api_log/{$str_operation}/{$queue_type}/{$sp_id}/{$cp_id}/{$action}/{$in_out}/{$type}/".date("Ymd")."/".date("H")."/{$file_name}";
        if(file_exists(dirname(dirname(dirname(__FILE__))).'/data'.$message_url))
        {
            $queue_id_rand = np_guid_rand();
            $message_url = "/api_log/{$str_operation}/{$queue_type}/{$sp_id}/{$cp_id}/{$action}/{$in_out}/{$type}/".date("Ymd")."/".date("H")."/{$queue_id_rand}_{$file_name}";
        }

        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $message);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }
    
    /**
     * 队列执行日志处理（这个属于内部处理的范畴 使用该类，文件是在原有的文件下增加内容，没有文件的时候才创建  只适用于队列文件日志的存储）
     * @param string $queue_type   队列类型
     * @param string $queue_action   队列操作类型  push 写入 | pop 输出
     * @param string $queue_id  队列ID
     * @param mix $messag 消息
     * @param string $message_url   队列日志文件相对路径
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public static function write_queue_execute_log($queue_type,$queue_action,$queue_id,$message=null,$message_url=null)
    {
        $message = is_array($message) ? json_encode($message,JSON_UNESCAPED_UNICODE) : $message;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行日志文件的写入");
        }
        $queue_action = (isset($queue_action) && in_array($queue_action, array('push','pop'))) ? $queue_action : 'pop';
        $queue_action = $queue_action == 'push' ? '队列写入' : '队列输出';
        $message_url = trim($message_url);
        if(!isset($message_url) || strlen($message_url) <1)
        {
            if(strlen($queue_type)<1)
            {
                return self::return_data(1,'队列类型为空');
            }
            if(strlen($queue_id)<1)
            {
                return self::return_data(1,'消息队列为空');
            }
            $file_name = self::is_xml($message) ? $queue_id.'.'.'xml' : $queue_id.'.'.'txt';
            $message_url = "/queue_log/{$queue_type}/".date("Ymd")."/".date("H")."/{$file_name}";
        }
        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $str_operation = self::$flag_operation ? '定时器自动操作' : '手工后台操作';
        $getmypid = getmypid();
        $str_message_info = "线程号[{$getmypid}];操作类型[{$str_operation}];日志时间[" . date("Y-m-d H:i:s") . "]\r\n";
        $str_message_info.= "----------------------操作开始----------------------\r\n";
        $str_message_info.= "[{$queue_action}]操作内容：\r\n";
        $str_message_info.= "{$message}\r\n";
        $str_message_info.= "----------------------操作结束----------------------\r\n";
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_message_info, FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }



    /***********************************START*****新改写日志，按执行步骤区分****START***************************************/

    /**
     * @description:消息接收日志
     * @author:xinxin.deng
     * @date: 2018/5/7 16:38
     * @param $content  $content //可以sting|array|xml 描述原因
     * @param $cp_id  $cp_id //消息接收cp_id
     * @return array  array(
     *                      'ret' => 0,//0成功，1失败
     *                      'reason' => '',//原因描述
     *                      'data_info' => array(
     *                                          'base_dir' => ''),//文件相对路基
     *                                          'absolute_dir' => ''),//文件绝对路径
     *                                   );
     */
    public static function write_message_receive_log($content, $cp_id)
    {
        if(!isset($cp_id) || strlen($cp_id) <1)
        {
            $cp_id='cds_default_cp';
        }
        $message = is_array($content) ? json_encode($content,JSON_UNESCAPED_UNICODE) : $content;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $str_operation = self::$flag_operation ? '定时器自动操作' : '手工后台操作';
        $getmypid = getmypid();
        $str_message_info = "线程号[{$getmypid}];操作类型[{$str_operation}];日志时间[" . date("Y-m-d H:i:s") . "]\r\n";
        $str_message_info.= "----------------------操作开始----------------------\r\n";
        $str_message_info.= "{$message}\r\n";
        $str_message_info.= "----------------------操作结束----------------------\r\n";
        $file_name = self::is_xml($content) ? date("H").'.'.'xml' : date("H").'.'.'txt';
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        $message_url = "/message_receive_log/{$cp_id}/{$str_operation}/".date("Ymd")."/{$file_name}";

        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_message_info, FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }

    /**
     * @description:消息执行注入到中心同步指令日志
     * @author:xinxin.deng
     * @date: 2018/5/7 16:38
     * @param $content  $content //可以sting|array|xml 描述原因
     * @param $cp_id  $cp_id //消息cp方
     * @return array  array(
     *                      'ret' => 0,//0成功，1失败
     *                      'reason' => '',//原因描述
     *                      'data_info' => array(
     *                                          'base_dir' => ''),//文件相对路基
     *                                          'absolute_dir' => ''),//文件绝对路径
     *                                   );
     */
    public static function write_message_execute_log($content, $cp_id)
    {
        if(!isset($cp_id) || strlen($cp_id) <1)
        {
            $cp_id='cds_default_cp';
        }
        $message = is_array($content) ? json_encode($content,JSON_UNESCAPED_UNICODE) : $content;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $str_operation = self::$flag_operation ? '定时器自动操作' : '手工后台操作';
        $getmypid = getmypid();
        $str_message_info = "线程号[{$getmypid}];操作类型[{$str_operation}];日志时间[" . date("Y-m-d H:i:s") . "]\r\n";
        $str_message_info.= "----------------------操作开始----------------------\r\n";
        $str_message_info.= "{$message}\r\n";
        $str_message_info.= "----------------------操作结束----------------------\r\n";
        $file_name = self::is_xml($content) ? date("H").'.'.'xml' : date("H").'.'.'txt';
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        $message_url = "/message_execute_log/{$cp_id}/{$str_operation}/".date("Ymd")."/{$file_name}";

        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_message_info, FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }

    /**
     * @description:队列执行统一异步反馈到上游日志
     * @author:xinxin.deng
     * @date: 2018/5/7 16:38
     * @param $queue_id  ,//消息id
     * @param $cp_id
     * @param $content  $content//可以sting|array|xml 描述原因以及记录操作参数等
     * @return array   array(
     *                      'ret' => 0,//0成功，1失败
     *                      'reason' => '',//原因描述
     *                      'data_info' => array(
     *                                          'base_dir' => ''),//文件相对路基
     *                                          'absolute_dir' => ''),//文件绝对路径
     *                                   );
     */
    public static function write_callback_log($content, $cp_id, $queue_id)
    {
        if(!isset($cp_id) || strlen($cp_id) <1)
        {
            $cp_id='cds_default_cp';
        }
        $message = is_array($content) ? json_encode($content,JSON_UNESCAPED_UNICODE) : $content;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $str_operation = self::$flag_operation ? '定时器自动操作' : '手工后台操作';
        $getmypid = getmypid();
        $str_message_info = "线程号[{$getmypid}];操作类型[{$str_operation}];日志时间[" . date("Y-m-d H:i:s") . "]\r\n";
        $str_message_info.= "----------------------操作开始----------------------\r\n";
        $str_message_info.= "{$message}\r\n";
        $str_message_info.= "----------------------操作结束----------------------\r\n";
        $queue_id = (isset($queue_id) && strlen($queue_id)>0) ? $queue_id : np_guid_rand();
        $file_name = self::is_xml($content) ? $queue_id.'.'.'xml' : $queue_id.'.'.'txt';
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        $message_url = "/message_callback_log/{$cp_id}/{$str_operation}/".date("Ymd")."/".date("H")."/{$file_name}";

        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_message_info, FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }

    /**
     * @description:中心同步指令对列执行日志
     * @author:xinxin.deng
     * @date: 2018/5/7 16:38
     * @param $queue_id  ,//队列id
     * @param $sp_id
     * @param $content  $content//可以sting|array|xml 描述原因以及记录操作参数等
     * @return array  array(
     *                      'ret' => 0,//0成功，1失败
     *                      'reason' => '',//原因描述
     *                      'data_info' => array(
     *                                          'base_dir' => ''),//文件相对路基
     *                                          'absolute_dir' => ''),//文件绝对路径
     *                                   );
     */
    public static function write_op_queue_execute_log($content, $sp_id, $queue_id)
    {
        if(!isset($sp_id) || strlen($sp_id) <1)
        {
            $sp_id='cds_default_sp';
        }
        $message = is_array($content) ? json_encode($content,JSON_UNESCAPED_UNICODE) : $content;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $str_operation = self::$flag_operation ? '定时器自动操作' : '手工后台操作';
        $getmypid = getmypid();
        $str_message_info = "线程号[{$getmypid}];操作类型[{$str_operation}];日志时间[" . date("Y-m-d H:i:s") . "]\r\n";
        $str_message_info.= "----------------------操作开始----------------------\r\n";
        $str_message_info.= "{$message}\r\n";
        $str_message_info.= "----------------------操作结束----------------------\r\n";
        $queue_id = (isset($queue_id) && strlen($queue_id)>0) ? $queue_id : np_guid_rand();
        $file_name = self::is_xml($content) ? $queue_id.'.'.'xml' : $queue_id.'.'.'txt';
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        $message_url = "/op_queue_execute_log/{$sp_id}/{$str_operation}/".date("Ymd")."/".date("H")."/{$file_name}";

        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_message_info,FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }

    /**
     * @description:切片队列执行日志
     * @author:xinxin.deng
     * @date: 2018/5/7 16:38
     * @param $queue_id  ,//队列id
     * @param $sp_id
     * @param $content  $content//可以sting|array|xml 描述原因以及记录操作参数等
     * @return array  array(
     *                      'ret' => 0,//0成功，1失败
     *                      'reason' => '',//原因描述
     *                      'data_info' => array(
     *                                          'base_dir' => ''),//文件相对路基
     *                                          'absolute_dir' => ''),//文件绝对路径
     *                                   );
     */
    public static function write_clip_execute_log($content, $sp_id, $queue_id)
    {
        if(!isset($sp_id) || strlen($sp_id) <1)
        {
            $sp_id = 'cds_default_sp';
        }
        $message = is_array($content) ? json_encode($content,JSON_UNESCAPED_UNICODE) : $content;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $str_operation = self::$flag_operation ? '定时器自动操作' : '手工后台操作';
        $getmypid = getmypid();
        $str_message_info = "线程号[{$getmypid}];操作类型[{$str_operation}];日志时间[" . date("Y-m-d H:i:s") . "]\r\n";
        $str_message_info.= "----------------------操作开始----------------------\r\n";
        $str_message_info.= "{$message}\r\n";
        $str_message_info.= "----------------------操作结束----------------------\r\n";
        $queue_id = (isset($queue_id) && strlen($queue_id)>0) ? $queue_id : np_guid_rand();
        $file_name = self::is_xml($content) ? $queue_id.'.'.'xml' : $queue_id.'.'.'txt';
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        $message_url = "/clip_execute_log/{$sp_id}/{$str_operation}/".date("Ymd")."/".date("H")."/{$file_name}";

        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_message_info, FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }

    /**
     * @description:切片服务器异步反馈到播控的执行日志
     * @author:xinxin.deng
     * @date: 2018/5/7 16:38
     * @param $queue_id  ,//队列id
     * @param $sp_id
     * @param $content  $content//可以sting|array|xml 描述原因以及记录操作参数等
     * @return array  array(
     *                      'ret' => 0,//0成功，1失败
     *                      'reason' => '',//原因描述
     *                      'data_info' => array(
     *                                          'base_dir' => ''),//文件相对路基
     *                                          'absolute_dir' => ''),//文件绝对路径
     *                                   );
     */
    public static function write_clip_notify_log($content, $sp_id, $queue_id)
    {
        if(!isset($sp_id) || strlen($sp_id) <1)
        {
            $sp_id = 'cds_default_sp';
        }
        $message = is_array($content) ? json_encode($content,JSON_UNESCAPED_UNICODE) : $content;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $str_operation = self::$flag_operation ? '定时器自动操作' : '手工后台操作';
        $getmypid = getmypid();
        $str_message_info = "线程号[{$getmypid}];操作类型[{$str_operation}];日志时间[" . date("Y-m-d H:i:s") . "]\r\n";
        $str_message_info.= "----------------------操作开始----------------------\r\n";
        $str_message_info.= "{$message}\r\n";
        $str_message_info.= "----------------------操作结束----------------------\r\n";
        $queue_id = (isset($queue_id) && strlen($queue_id)>0) ? $queue_id : np_guid_rand();
        $file_name = self::is_xml($content) ? $queue_id.'.'.'xml' : $queue_id.'.'.'txt';
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        $message_url = "/clip_notify_log/{$sp_id}/{$str_operation}/".date("Ymd")."/".date("H")."/{$file_name}";

        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_message_info, FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }

    /**
     * @description:切片队列请求切片日志
     * @author:xinxin.deng
     * @date: 2018/5/7 16:38
     * @param $content  $content //可以sting|array|xml 描述原因
     * @param $sp_id  $sp_id //消息sp方
     * @return array  array(
     *                      'ret' => 0,//0成功，1失败
     *                      'reason' => '',//原因描述
     *                      'data_info' => array(
     *                                          'base_dir' => ''),//文件相对路基
     *                                          'absolute_dir' => ''),//文件绝对路径
     *                                   );
     */
    public static function write_clip_receive_log($content, $sp_id)
    {
        if(!isset($sp_id) || strlen($sp_id) <1)
        {
            $sp_id='cds_default_sp';
        }
        $message = is_array($content) ? json_encode($content,JSON_UNESCAPED_UNICODE) : $content;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $str_operation = self::$flag_operation ? '定时器自动操作' : '手工后台操作';
        $getmypid = getmypid();
        $str_message_info = "线程号[{$getmypid}];操作类型[{$str_operation}];日志时间[" . date("Y-m-d H:i:s") . "]\r\n";
        $str_message_info.= "----------------------操作开始----------------------\r\n";
        $str_message_info.= "{$message}\r\n";
        $str_message_info.= "----------------------操作结束----------------------\r\n";
        $file_name = self::is_xml($content) ? date("H").'.'.'xml' : date("H").'.'.'txt';
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        $message_url = "/clip_receive_log/{$sp_id}/{$str_operation}/".date("Ymd")."/{$file_name}";

        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_message_info, FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }

    /**
     * @description:转码队列执行日志
     * @author:xinxin.deng
     * @date: 2018/5/7 16:38
     * @param $queue_id  ,//队列id
     * @param $sp_id
     * @param $content  $content//可以sting|array|xml 描述原因以及记录操作参数等
     * @return array  array(
     *                      'ret' => 0,//0成功，1失败
     *                      'reason' => '',//原因描述
     *                      'data_info' => array(
     *                                          'base_dir' => ''),//文件相对路基
     *                                          'absolute_dir' => ''),//文件绝对路径
     *                                   );
     */
    public static function write_encode_execute_log($content, $sp_id, $queue_id)
    {
        if(!isset($sp_id) || strlen($sp_id) <1)
        {
            $sp_id = 'cds_default_sp';
        }
        $message = is_array($content) ? json_encode($content,JSON_UNESCAPED_UNICODE) : $content;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $str_operation = self::$flag_operation ? '定时器自动操作' : '手工后台操作';
        $getmypid = getmypid();
        $str_message_info = "线程号[{$getmypid}];操作类型[{$str_operation}];日志时间[" . date("Y-m-d H:i:s") . "]\r\n";
        $str_message_info.= "----------------------操作开始----------------------\r\n";
        $str_message_info.= "{$message}\r\n";
        $str_message_info.= "----------------------操作结束----------------------\r\n";
        $queue_id = (isset($queue_id) && strlen($queue_id)>0) ? $queue_id : np_guid_rand();
        $file_name = self::is_xml($content) ? $queue_id.'.'.'xml' : $queue_id.'.'.'txt';
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        $message_url = "/encode_execute_log/{$sp_id}/{$str_operation}/".date("Ymd")."/".date("H")."/{$file_name}";

        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_message_info, FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }

    /**
     * @description:转码服务器异步反馈给播控的执行日志
     * @author:xinxin.deng
     * @date: 2018/5/7 16:38
     * @param $queue_id  ,//队列id
     * @param $sp_id
     * @param $content  $content//可以sting|array|xml 描述原因以及记录操作参数等
     * @return array  array(
     *                      'ret' => 0,//0成功，1失败
     *                      'reason' => '',//原因描述
     *                      'data_info' => array(
     *                                          'base_dir' => ''),//文件相对路基
     *                                          'absolute_dir' => ''),//文件绝对路径
     *                                   );
     */
    public static function write_encode_notify_log($content, $sp_id, $queue_id)
    {
        if(!isset($sp_id) || strlen($sp_id) <1)
        {
            $sp_id = 'cds_default_sp';
        }
        $message = is_array($content) ? json_encode($content,JSON_UNESCAPED_UNICODE) : $content;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $str_operation = self::$flag_operation ? '定时器自动操作' : '手工后台操作';
        $getmypid = getmypid();
        $str_message_info = "线程号[{$getmypid}];操作类型[{$str_operation}];日志时间[" . date("Y-m-d H:i:s") . "]\r\n";
        $str_message_info.= "----------------------操作开始----------------------\r\n";
        $str_message_info.= "{$message}\r\n";
        $str_message_info.= "----------------------操作结束----------------------\r\n";
        $queue_id = (isset($queue_id) && strlen($queue_id)>0) ? $queue_id : np_guid_rand();
        $file_name = self::is_xml($content) ? $queue_id.'.'.'xml' : $queue_id.'.'.'txt';
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        $message_url = "/encode_notify_log/{$sp_id}/{$str_operation}/".date("Ymd")."/".date("H")."/{$file_name}";

        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_message_info, FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }

    /**
     * @description:注入cdn执行日志
     * @author:xinxin.deng
     * @date: 2018/5/7 16:38
     * @param $queue_id  ,//队列id
     * @param $sp_id
     * @param $content  $content//可以sting|array|xml 描述原因以及记录操作参数等
     * @return array  array(
     *                      'ret' => 0,//0成功，1失败
     *                      'reason' => '',//原因描述
     *                      'data_info' => array(
     *                                          'base_dir' => ''),//文件相对路基
     *                                          'absolute_dir' => ''),//文件绝对路径
     *                                   );
     */
    public static function write_cdn_execute_log($content, $sp_id, $queue_id)
    {
        if(!isset($sp_id) || strlen($sp_id) < 1)
        {
            $sp_id = 'cds_default_sp';
        }
        $message = is_array($content) ? json_encode($content,JSON_UNESCAPED_UNICODE) : $content;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $str_operation = self::$flag_operation ? '定时器自动操作' : '手工后台操作';
        $getmypid = getmypid();
        $str_message_info = "线程号[{$getmypid}];操作类型[{$str_operation}];日志时间[" . date("Y-m-d H:i:s") . "]\r\n";
        $str_message_info.= "----------------------操作开始----------------------\r\n";
        $str_message_info.= "{$message}\r\n";
        $str_message_info.= "----------------------操作结束----------------------\r\n";
        $queue_id = (isset($queue_id) && strlen($queue_id)>0) ? $queue_id : np_guid_rand();
        $file_name = self::is_xml($content) ? $queue_id.'.'.'xml' : $queue_id.'.'.'txt';
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        $message_url = "/cdn_execute_log/{$sp_id}/{$str_operation}/".date("Ymd")."/".date("H")."/{$file_name}";

        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_message_info, FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }

    /**
     * @description:注入cdn，cdn厂商异步反馈给播控的执行日志
     * @author:xinxin.deng
     * @date: 2018/5/7 16:38
     * @param $queue_id  ,//队列id
     * @param $sp_id
     * @param $content  $content//可以sting|array|xml 描述原因以及记录操作参数等
     * @return array  array(
     *                      'ret' => 0,//0成功，1失败
     *                      'reason' => '',//原因描述
     *                      'data_info' => array(
     *                                          'base_dir' => ''),//文件相对路基
     *                                          'absolute_dir' => ''),//文件绝对路径
     *                                   );
     */
    public static function write_cdn_notify_log($content, $sp_id, $queue_id)
    {
        if(!isset($sp_id) || strlen($sp_id) <1)
        {
            $sp_id = 'cds_default_sp';
        }
        $message = is_array($content) ? json_encode($content,JSON_UNESCAPED_UNICODE) : $content;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $str_operation = self::$flag_operation ? '定时器自动操作' : '手工后台操作';
        $getmypid = getmypid();
        $str_message_info = "线程号[{$getmypid}];操作类型[{$str_operation}];日志时间[" . date("Y-m-d H:i:s") . "]\r\n";
        $str_message_info.= "----------------------操作开始----------------------\r\n";
        $str_message_info.= "{$message}\r\n";
        $str_message_info.= "----------------------操作结束----------------------\r\n";
        $queue_id = (isset($queue_id) && strlen($queue_id)>0) ? $queue_id : date("H");
        $file_name = self::is_xml($content) ? $queue_id.'.'.'xml' : $queue_id.'.'.'txt';
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        $message_url = "/cdn_notify_log/{$sp_id}/{$str_operation}/".date("Ymd")."/".date("H")."/{$file_name}";

        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_message_info, FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }

    /**
     * @description:注入epg的执行日志
     * @author:xinxin.deng
     * @date: 2018/5/7 16:38
     * @param $queue_id  ,//队列id
     * @param $sp_id
     * @param $content  $content//可以sting|array|xml 描述原因以及记录操作参数等
     * @return array  array(
     *                      'ret' => 0,//0成功，1失败
     *                      'reason' => '',//原因描述
     *                      'data_info' => array(
     *                                          'base_dir' => ''),//文件相对路基
     *                                          'absolute_dir' => ''),//文件绝对路径
     *                                   );
     */
    public static function write_epg_execute_log($content, $sp_id, $queue_id)
    {
        if(!isset($sp_id) || strlen($sp_id) <1)
        {
            $sp_id = 'cds_default_sp';
        }
        $message = is_array($content) ? json_encode($content,JSON_UNESCAPED_UNICODE) : $content;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $str_operation = self::$flag_operation ? '定时器自动操作' : '手工后台操作';
        $getmypid = getmypid();
        $str_message_info = "线程号[{$getmypid}];操作类型[{$str_operation}];日志时间[" . date("Y-m-d H:i:s") . "]\r\n";
        $str_message_info.= "----------------------操作开始----------------------\r\n";
        $str_message_info.= "{$message}\r\n";
        $str_message_info.= "----------------------操作结束----------------------\r\n";
        $queue_id = (isset($queue_id) && strlen($queue_id)>0) ? $queue_id : np_guid_rand();
        $file_name = self::is_xml($content) ? $queue_id.'.'.'xml' : $queue_id.'.'.'txt';
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        $message_url = "/epg_execute_log/{$sp_id}/{$str_operation}/".date("Ymd")."/".date("H")."/{$file_name}";

        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_message_info, FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }

    /**
     * @description:注入epg的异步反馈执行日志
     * @author:xinxin.deng
     * @date: 2018/5/7 16:38
     * @param $queue_id  ,//队列id
     * @param $sp_id
     * @param $content  $content//可以sting|array|xml 描述原因以及记录操作参数等
     * @return array  array(
     *                      'ret' => 0,//0成功，1失败
     *                      'reason' => '',//原因描述
     *                      'data_info' => array(
     *                                          'base_dir' => ''),//文件相对路基
     *                                          'absolute_dir' => ''),//文件绝对路径
     *                                   );
     */
    public static function write_epg_notify_log($content, $sp_id, $queue_id)
    {
        if(!isset($sp_id) || strlen($sp_id) <1)
        {
            $sp_id = 'cds_default_sp';
        }
        $message = is_array($content) ? json_encode($content,JSON_UNESCAPED_UNICODE) : $content;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $str_operation = self::$flag_operation ? '定时器自动操作' : '手工后台操作';
        $getmypid = getmypid();
        $str_message_info = "线程号[{$getmypid}];操作类型[{$str_operation}];日志时间[" . date("Y-m-d H:i:s") . "]\r\n";
        $str_message_info.= "----------------------操作开始----------------------\r\n";
        $str_message_info.= "{$message}\r\n";
        $str_message_info.= "----------------------操作结束----------------------\r\n";
        $queue_id = (isset($queue_id) && strlen($queue_id)>0) ? $queue_id : np_guid_rand();
        $file_name = self::is_xml($content) ? $queue_id.'.'.'xml' : $queue_id.'.'.'txt';
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        $message_url = "/epg_notify_log/{$sp_id}/{$str_operation}/".date("Ymd")."/".date("H")."/{$file_name}";

        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_message_info, FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }

    /**
     * @description:媒资上下线队列执行日志
     * @author:xinxin.deng
     * @date: 2018/5/7 16:38
     * @param $queue_id  ,//队列id
     * @param $sp_id
     * @param $content  $content//可以sting|array|xml 描述原因以及记录操作参数等
     * @return array  array(
     *                      'ret' => 0,//0成功，1失败
     *                      'reason' => '',//原因描述
     *                      'data_info' => array(
     *                                          'base_dir' => ''),//文件相对路基
     *                                          'absolute_dir' => ''),//文件绝对路径
     *                                   );
     */
    public static function write_epg_line_execute_log($content, $sp_id, $queue_id)
    {
        if(!isset($sp_id) || strlen($sp_id) <1)
        {
            $sp_id = 'cds_default_sp';
        }
        $message = is_array($content) ? json_encode($content,JSON_UNESCAPED_UNICODE) : $content;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $str_operation = self::$flag_operation ? '定时器自动操作' : '手工后台操作';
        $getmypid = getmypid();
        $str_message_info = "线程号[{$getmypid}];操作类型[{$str_operation}];日志时间[" . date("Y-m-d H:i:s") . "]\r\n";
        $str_message_info.= "----------------------操作开始----------------------\r\n";
        $str_message_info.= "{$message}\r\n";
        $str_message_info.= "----------------------操作结束----------------------\r\n";
        $queue_id = (isset($queue_id) && strlen($queue_id)>0) ? $queue_id : np_guid_rand();
        $file_name = self::is_xml($content) ? $queue_id.'.'.'xml' : $queue_id.'.'.'txt';
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        $message_url = "/epg_line_execute_log/{$sp_id}/{$str_operation}/".date("Ymd")."/".date("H")."/{$file_name}";

        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_message_info, FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }

    /**
     * @description:透传队列执行日志
     * @author: xinxin.deng
     * @date: 2018/10/31 13:43
     * @param $queue_id  ,//队列id
     * @param $sp_id
     * @param $content  $content//可以sting|array|xml 描述原因以及记录操作参数等
     * @return array  array(
     *                      'ret' => 0,//0成功，1失败
     *                      'reason' => '',//原因描述
     *                      'data_info' => array(
     *                                          'base_dir' => ''),//文件相对路基
     *                                          'absolute_dir' => ''),//文件绝对路径
     *                                   );
     */
    public static function write_pass_queue_execute_log($content, $sp_id, $queue_id)
    {
        if(!isset($sp_id) || strlen($sp_id) < 1)
        {
            $sp_id = 'cds_default_sp';
        }
        $message = is_array($content) ? json_encode($content,JSON_UNESCAPED_UNICODE) : $content;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $str_operation = self::$flag_operation ? '定时器自动操作' : '手工后台操作';
        $getmypid = getmypid();
        $str_message_info = "线程号[{$getmypid}];操作类型[{$str_operation}];日志时间[" . date("Y-m-d H:i:s") . "]\r\n";
        $str_message_info.= "----------------------操作开始----------------------\r\n";
        $str_message_info.= "{$message}\r\n";
        $str_message_info.= "----------------------操作结束----------------------\r\n";
        $queue_id = (isset($queue_id) && strlen($queue_id)>0) ? $queue_id : np_guid_rand();
        $file_name = self::is_xml($content) ? $queue_id.'.'.'xml' : $queue_id.'.'.'txt';
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        $message_url = "/pass_queue_execute_log/{$sp_id}/{$str_operation}/".date("Ymd")."/".date("H")."/{$file_name}";

        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_message_info, FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }

    /**
     * @description:透传队列下游反馈日志
     * @author: xinxin.deng
     * @date: 2018/11/23 11:51
     * @param $sp_id
     * @param $content  $content//可以sting|array|xml 描述原因以及记录操作参数等
     * @return array  array(
     *                      'ret' => 0,//0成功，1失败
     *                      'reason' => '',//原因描述
     *                      'data_info' => array(
     *                                          'base_dir' => ''),//文件相对路基
     *                                          'absolute_dir' => ''),//文件绝对路径
     *                                   );
     */
    public static function write_pass_queue_notify_log($content, $sp_id)
    {
        if(!isset($sp_id) || strlen($sp_id) < 1)
        {
            $sp_id = 'cds_default_sp';
        }
        $message = is_array($content) ? json_encode($content,JSON_UNESCAPED_UNICODE) : $content;
        if(strlen($message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $str_operation = self::$flag_operation ? '定时器自动操作' : '手工后台操作';
        $getmypid = getmypid();
        $str_message_info = "线程号[{$getmypid}];操作类型[{$str_operation}];日志时间[" . date("Y-m-d H:i:s") . "]\r\n";
        $str_message_info.= "----------------------操作开始----------------------\r\n";
        $str_message_info.= "{$message}\r\n";
        $str_message_info.= "----------------------操作结束----------------------\r\n";
        $file_name = self::is_xml($content) ? date("H").'.'.'xml' : date("H").'.'.'txt';
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        $message_url = "/pass_queue_notify_log/{$sp_id}/{$str_operation}/".date("Ymd")."/".date("H")."/{$file_name}";

        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_message_info, FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }
    /*************************************END*****新改写日志，按执行步骤区分****END*****************************************/



    /**
     * 创建文件的文件夹 （文件存储在 data目录下）
     * @param string $message_url 文件存储的 相对路径
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public static function make_dir($message_url='')
    {
        $message_url = trim(trim(trim($message_url,'\\'),'/'));
        $temp_message_url = $message_url;
        $arr_pathinfo = pathinfo($message_url);
        if(isset($arr_pathinfo['extension']))
        {
            $temp_message_url = (isset($arr_pathinfo['dirname']) && $arr_pathinfo['dirname'] !='.' && $arr_pathinfo['dirname'] !='..') ? trim(rtrim(rtrim($arr_pathinfo['dirname'],'\\'),'/')) : '';
        }
        if(strlen($temp_message_url) <1)
        {
            return self::return_data(1,'文件夹为空，不创建文件夹');
        }
        $file_base_dir = dirname(dirname(dirname(__FILE__))).'/data/';
        $file_absolute_dir = dirname(dirname(dirname(__FILE__))).'/data/'.$temp_message_url.'/';
        if (!is_dir($file_absolute_dir))
        {
            mkdir($file_absolute_dir, 0777, true);

            if(strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN')
            {
                $stat = stat($file_absolute_dir);
                $result_chown = (posix_getpwuid($stat['uid']));
                if(!isset($result_chown['name']) || $result_chown['name'] != 'www')
                {
                    chown($file_absolute_dir, 'www');
                }
            }
        }
        $arr_dir = array(
            'base_dir'=>$message_url,
            'absolute_dir'=>$file_base_dir.$message_url,
        );
        return self::return_data(0,'创建文件路径成功',$arr_dir);
    }
    
    
    /**
     * 创建文件的文件夹 （文件存储在 data目录下）
     * @param string $message_url 文件存储的 相对路径
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public static function make_dir_out_project($message_url='')
    {
        $message_url = trim(rtrim(rtrim($message_url,'\\'),'/'));
        $temp_message_url = $message_url;
        $arr_pathinfo = pathinfo($message_url);
        if(isset($arr_pathinfo['extension']))
        {
            $temp_message_url = (isset($arr_pathinfo['dirname']) && $arr_pathinfo['dirname'] !='.' && $arr_pathinfo['dirname'] !='..') ? trim(rtrim(rtrim($arr_pathinfo['dirname'],'\\'),'/')) : '';
        }
        if(strlen($temp_message_url) <1)
        {
            return self::return_data(1,'文件夹为空，不创建文件夹');
        }
        $file_absolute_dir = $temp_message_url.'/';
        if (!is_dir($file_absolute_dir))
        {
            $result = mkdir($file_absolute_dir, 0777, true);
            if(strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN')
            {
                $stat = stat($file_absolute_dir);
                $result_chown = (posix_getpwuid($stat['uid']));
                if(!isset($result_chown['name']) || $result_chown['name'] != 'www')
                {
                    chown($result_chown, 'www');
                }
            }
        }
        $arr_dir = array(
            'base_dir'=>$message_url.'/',
            'absolute_dir'=>$message_url.'/',
        );
        return self::return_data(0,'创建文件路径成功',$arr_dir);
    }
    
    /**
     * 删除文件夹（子文件删除后会检查父级文件夹，如果父级文件夹下面 没任何文件夹或者文件  父级文件夹也会做删除，知道指定的相对路径文件夹删除完毕程序才执行完毕 文件存储在 data目录下的都可以）
     * @param string $message_url 文件存储的 相对路径
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public static function del_dir($message_url='')
    {
        $file_base_dir = dirname(dirname(dirname(__FILE__))).'/data/';
        $message_url = trim(trim(trim($message_url,'\\'),'/'));
        $arr_pathinfo = pathinfo($message_url);
        if(isset($arr_pathinfo['extension']))
        {
            $message_url = (isset($arr_pathinfo['dirname']) && $arr_pathinfo['dirname'] !='.' && $arr_pathinfo['dirname'] !='..') ? $arr_pathinfo['dirname'] : trim(trim(trim($message_url,'\\'),'/'));
        }
        $message_url = trim(trim(trim($message_url,'\\'),'/'));
        if(strlen($message_url) <1)
        {
            return self::return_data(1,'消息日志文件路径为空不执行删除');
        }
        if(!is_dir($file_base_dir.$message_url))
        {
            return self::return_data(1,"删除文件夹失败，没有文件路径：".$file_base_dir.$message_url);
        }
        $files = scandir($file_base_dir.$message_url);
        if(!is_array($files) || empty($files))
        {
            return self::return_data(1,"删除文件夹失败，文件夹路径：{$file_base_dir}{$message_url},文件夹下文件列表：".var_export($files,true));
        }
        $files = array_diff($files, array('.','..'));
        if(is_array($files) && !empty($files))
        {
            return self::return_data(0,"文件夹下存在其他文件不执行删除文件夹,文件夹路径：{$file_base_dir}{$message_url}");
        }
        $result = rmdir($file_base_dir.'/'.$message_url);
        if(!$result)
        {
            return self::return_data(1,"删除文件夹失败，文件路径为：".$file_base_dir.$message_url);
        }
        $arr_pathinfo = explode('/', $message_url);
        $arr_pathinfo = (is_array($arr_pathinfo) && !empty($arr_pathinfo)) ? array_filter($arr_pathinfo) : array();
        if(!empty($arr_pathinfo) && is_array($arr_pathinfo))
        {
            array_pop($arr_pathinfo);
        }
        if(!is_array($arr_pathinfo) || empty($arr_pathinfo) || count($arr_pathinfo) <1)
        {
            return self::return_data(0,"删除文件夹成功，文件路径为：".$file_base_dir.$message_url);
        }
        $temp_del_path_url=implode('/', $arr_pathinfo);
        return self::del_dir($temp_del_path_url);
    }
    
    
    /**
     * 删除创建的文件(实用于播控所有的文件  无论是日志文件还是message消息文件  文件存储在 data目录下的都可以)
     * @param string $message_url  文件存储的 相对路径
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public static function del_file($message_url='')
    {
        $message_url = trim(trim(trim($message_url,'\\'),'/'));
        if(strlen($message_url) <1)
        {
            return self::return_data(1,'消息日志文件为空不执行删除');
        }
        $file_base_dir = dirname(dirname(dirname(__FILE__))).'/data/';
        $file_absolute_dir = $file_base_dir.'/'.$message_url;
        if(!file_exists($file_absolute_dir))
        {
            return self::return_data(1,"消息日志文件查询不存在，不执行删除文件路径为:[{$file_absolute_dir}]");
        }
        $result_unlink = @unlink($file_absolute_dir);
        if(!$result_unlink)
        {
            return self::return_data(1,"消息日志文件删除失败，不执行删除文件路径为:[{$file_absolute_dir}]");
        }
        return self::del_dir($message_url);
    }
    
    /**
     * 组装XMl
     * @param object $obj_dom dom
     * @param string $key
     * @param string $val
     * @param string $arr_attr
     * @param object $parent 父级dom
     */
    public static function make_xml($obj_dom,$key, $val = null, $arr_attr = null,$parent=null)
    {
        if(is_null($parent))
        {
            $parent=$obj_dom;
        }
        $$key = isset($val) ? $obj_dom->createElement($key, $val) : $obj_dom->createElement($key);
        if (!empty($arr_attr) && is_array($arr_attr))
        {
            foreach ($arr_attr as $attr_key => $attr_val)
            {
                $domAttribute = $obj_dom->createAttribute($attr_key);
                $domAttribute->value = $attr_val;
                $$key->appendChild($domAttribute);
                $obj_dom->appendChild($$key);
            }
        }
        $parent->appendChild($$key);
        unset($parent);
        return $$key;
    }
    
    /**
     * 获取ftp信息
     * @param unknown $ftp_url
     * @param number $time
     * @param string $passive
     * @param string $file_save_path
     * @return multitype:number
     */
    public static function get_ftp_content_and_save($str_url,$save_path, $time = 30, $passive = TRUE)
    {
        if(strpos($save_path, '?') !== false)
        {
            $save_path = trim(preg_replace('/\?.*/i', '', $save_path));
        }
        if(strlen($save_path) <1)
        {
            return self::return_data(1,'文件存储路径为空');
        }
        //拆分FTP地址
        $url_arr = parse_url($str_url);
        if (empty($url_arr["user"]))
        {
            $url_arr["user"] = '';
        }
        if (empty($url_arr["pass"]))
        {
            $url_arr["pass"] = '';
        }
        $port = (isset($url_arr["port"]) && strlen($url_arr["port"]) > 0) ? $url_arr["port"] : 21;
        \ns_core\m_load::load_np("np_ftp.php");
        $ftp = new nns_ftp($url_arr['host'], $url_arr["user"], $url_arr["pass"], $port);
        $ftpcon = $ftp->connect($time, $passive);
        if (empty($ftpcon))
        {
            return self::return_data(1, "FTP链接失败地址[{$str_url}],端口:[{$port}],超时时间:[{$time}],主被动模式:[{$passive}],文件绝对路径[{$save_path}]");
        }
        $res = $ftp->get($url_arr["path"], $save_path);
        //unset($res);
        if ($res == false)
        {
            return self::return_data(1, "FTP链接失败地址[{$str_url}],端口:[{$port}],超时时间:[{$time}],主被动模式:[{$passive}],文件绝对路径[{$save_path}]");
        }
        return self::return_data(0, 'FTP获取成功', $save_path);
    }

    /**
     * 获取ftp内容
     * @param string $message_id 消息guid
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因')
     */
    public static function get_ftp_content($str_url,$time = 30)
    {
        
    }

    /**
     * 直接保存xml
     * @param $content
     * @param $message_id
     * @param null $save_path
     * @return array
     */
    public static function content_to_save ($content, $message_id, $save_path = null)
    {
        $result_file_put = @file_put_contents($save_path, $content);
        if(!$result_file_put)
        {
            return self::return_data(1,"文件写入失败,文件绝对路径[{$save_path}]");
        }
        return self::return_data(0, '文件写入成功', $save_path);
    }

    /**
     * 获取curl内容
     * @param string $message_id 消息guid
     * @param string $file_path 文件路径
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public static function get_curl_content_and_save($str_url,$save_path,$time = 30)
    {
        if(strpos($save_path, '?') !== false)
        {
            $save_path = trim(preg_replace('/\?.*/i', '', $save_path));
        }
        if(strlen($save_path) <1)
        {
            return self::return_data(1,'文件存储路径为空');
        }
        //curl抓取 远程文件的内容
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $str_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $time);
        $content = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        //如果内容为空  或者根本没这个xml文件
        if (empty($content) || !$content || $http_code >= 400)
        {
            return self::return_data(1, "curl下载文件失败,请求地址:[{$str_url}],超时时间:[{$time}],文件绝对路径[{$save_path}]");
        }
        $result_file_put = @file_put_contents($save_path, $content);
        if(!$result_file_put)
        {
            return self::return_data(1,"文件写入失败,请求地址:[{$str_url}],超时时间:[{$time}],文件绝对路径[{$save_path}]");
        }
        return self::return_data(0, 'CURL获取成功', $save_path);
    }
    
    
    /**
     * 获取curl内容
     * @param string $message_id 消息guid
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因')
     * @author liangpan
     * @date 2016-03-12
     */
    public static function get_curl_content($str_url,$time = 30)
    {
        //curl抓取 远程文件的内容
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $str_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $time);
        $content = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        //如果内容为空  或者根本没这个xml文件
        if (empty($content) || !$content || $http_code >= 400)
        {
            return self::return_data(1, "curl下载文件失败,请求地址:[{$str_url}],超时时间:[{$time}]");
        }
        return self::return_data(0, 'CURL获取成功', $content);
    }
    
    /**
     * 混合获取url 内容信息
     * @param unknown $str_url
     * @param unknown $save_path
     * @param number $time
     * @param string $passive
     * @return multitype:number
     */
    public static function get_mix_url_content_and_save($str_url,$save_path,$time = 30,$passive = TRUE)
    {
        if(strpos($save_path, '?') !== false)
        {
            $save_path = trim(preg_replace('/\?.*/i', '', $save_path));
        }
        if(strlen($save_path) <1)
        {
            return self::return_data(1,'文件存储路径为空');
        }
        if(strpos($str_url, 'ftp://') !== false)
        {
            $result = self::get_ftp_content_and_save($str_url,$save_path, $time, $passive);
            if($result['ret'] !=0)
            {
                $result = self::get_curl_content_and_save($str_url,$save_path, $time);
            }
        }
        else
        {
            $result = self::get_curl_content_and_save($str_url,$save_path, $time);
        }
        return $result;
    }
    
    /**
     * 反馈文件子路径
     * @param unknown $file_path
     */
    public static function return_child_path($file_path,$type='ns_api')
    {
        $file_path = str_replace(array ('/','\\'), '|', $file_path);
        if(strpos($file_path, "|v2|{$type}|") === false)
        {
            return '';
        }
        $arr_file_path = explode("|v2|{$type}|", $file_path);
        if(!is_array($arr_file_path) || empty($arr_file_path) || count($arr_file_path) !=2 || !isset($arr_file_path[1]))
        {
            return '';
        }
        $str_file_path = $arr_file_path[1];
        $str_file_path = trim(trim($str_file_path,"|"));
        $str_file_path = str_replace('|', '/', $str_file_path);
        $arr_pathinfo = pathinfo($str_file_path);
        $str_path  = isset($arr_pathinfo['dirname']) ? trim(trim($arr_pathinfo['dirname'],'/')) : '';
        $arr_path = explode('/', $str_path);
        if($type == 'ns_api' && is_array($arr_path) && !empty($arr_path))
        {
            array_shift($arr_path);
        }
        $str_path = (is_array($arr_path) && !empty($arr_path)) ? implode('/', $arr_path) : '';
        $str_name = isset($arr_pathinfo['filename']) ? trim($arr_pathinfo['filename']) : '';
        if(strlen($str_name)>0)
        {
            $str_path.="/".str_replace('.', '_', $str_name);
        }
        $str_path = trim(trim(str_replace('//', '/', $str_path),'/'));
        return $str_path;
    }
    
    
    /**
     * xml 编码转换为UTF-8
     * @param string $xml xml内容
     * @param string $encode 内容编码
     * @return string|mixed
     * @author liangpan
     * @date 2016-03-12
     */
    public static function trim_xml_header($xml,$encode=null)
    {
        if(strlen($xml) < 1)
        {
            return '';
        }
        if(strlen($encode) > 0 )
        {
            $xml = mb_convert_encoding($xml,'UTF-8', $encode);
        }
        $xml = preg_replace('/\<\?\s*xml\s+version\s*\=\s*(\"|\').*(\"|\')\s+encoding\s*\=\s*(\"|\')[a-z0-9-]+(\"|\')\s*\?\>/i', '', $xml);
        $xml = preg_replace('/\<\?\s*xml\s+encoding\s*\=\s*(\"|\')[a-z0-9-]+(\"|\')\s*\?\>/i', '', $xml);
        $xml = preg_replace('/\<\?\s*xml\s+version\s*\=\s*(\"|\').*(\"|\')\s*\?\>/i', '', $xml);
        //去除XML中的单引号
        //$xml = str_replace(array("\'"), "’", $xml);
        return $xml;
    }

    /**
     * 下游下发的CDN時内容存储执行文件（这个属于外部对接处理的范畴 使用该类）
     * @param string $sp_id  下游平台sp 标示
     * @param string $type CDN注入类型
     * @param string $action CDN注入行为动作
     * @param string $cdn_message 注入的消息实体内容
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public static function write_execute_cdn_import_content($sp_id,$type,$action,$cdn_message)
    {
        if(!isset($sp_id) || strlen($sp_id) <1)
        {
            return self::return_data(1,'sP标示为空');
        }
        if(strlen($cdn_message) <1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        //update zhiyong.luo 2019-11-21
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = "{$sp_id}_{$time}_{$type}_{$action}_{$three_rand}.xml";
        $message_url = "/cdn_import/{$sp_id}/inject_xml/".date("Ymd")."/".date("H")."/{$file_name}";
        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $cdn_message);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }

    /**
     * 把本地文件上傳FTP
     * @param string $relative_path 本地文件相对路径
     * @param string $local_path 本地文件地址
     * @param string $up_ftp  上传FTP的地址
     * @param bool $passive 主被动模式，默认被动模式
     * @return Ambigous <multitype:number , multitype:number string >
     */
    static public function up_to_ftp($relative_path, $local_path,$up_ftp,$passive = true)
    {
        $arr_ftp = parse_url($up_ftp);
        if(!isset($arr_ftp['port']))
        {
            $arr_ftp['port'] = 21;
        }
        $ftp = new nns_ftp($arr_ftp['host'], $arr_ftp['user'], $arr_ftp['pass'], $arr_ftp['port']);
        $ftpcon = $ftp->connect(10, $passive);
        if(!$ftpcon)
        {
            unset($ftp);
            return self::return_data(1,'FTP链接失败'.$up_ftp);
        }
        $path_arr = pathinfo($relative_path);
        /**
         * 1、FTP绝对路径（相对当前账号的根目录的绝对路径）
         * 2、FTP上文件命名名称
         * 3、本地文件地址（绝对路径）
         */
        $ftp_re = $ftp->up($path_arr['dirname'], $path_arr['basename'], $local_path);
        unset($ftp);
        if(!$ftp_re)
        {
            $error_msg = '上传FTP'.$up_ftp.'失败,上传路径:'.$path_arr['dirname'].',上传文件名:'.$path_arr['basename'].',上传的本地文件:'.$local_path;
            return self::return_data(1,$error_msg);
        }
        //$ftp_url = str_replace(array("///","//"),"/",$up_ftp ."/". $relative_path);
        $ftp_url = $up_ftp ."/". $relative_path;
        return self::return_data(0,"上传成功",$ftp_url);
    }

    /**
     * CDN反馈時内容存储执行文件
     * @param string $queue_id 队列ID
     * @param string $cdn_notify 注入的消息实体内容
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public static function write_execute_cdn_notify_content($sp_id,$queue_id,$cdn_notify)
    {
        if(!isset($queue_id) || strlen($queue_id) <1 || !isset($sp_id) || strlen($sp_id) <1)
        {
            return self::return_data(1,'必传参数spid或queueid为空');
        }
        if(strlen($cdn_notify) <1)
        {
            return self::return_data(1,"内容为空不执行对接文件发送或者接收的写入");
        }
        $file_name = self::is_xml($cdn_notify) ? $queue_id.'.'.'xml' : $queue_id.'.'.'txt';
        $message_url = "/cdn_import/{$sp_id}/notify_xml/".date("Ymd")."/".date("H")."/{$file_name}";
        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $cdn_notify);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }

    /**
     * 队列注入流程公用日志文本记录
     * 按分钟生成txt文本日志，与队列单个日志文件不同，主要是有些地方需要在队列之外写一些特殊的日志
     * 在记录此日志时，如果能够知道CPID/SPID/ACTION/TYPE任意一个，必须传
     * @param string $queue_type 队列类型  message_queue  消息队列 | center_import 中心注入指令队列 | center_op 中心同步指令队列 | clip_queue 切片队列 | transcode_queue 转码队列 | epg_queue EPG注入队列 | cdn_queue CDN注入队列
     * @param string $cp_id  上游平台cp 标示 如果为空 置为cds_default_cp
     * @param string $sp_id  下游平台sp 标示 如果为空 置为cds_default_sp
     * @param string $action 操作行为
     * @param string $type 媒资类型 category 栏目 |  video 主媒资 | index 分集 | media 片源 | live 直播 | live_index 直播分集 | live_media 直播片源 | playbill 节目单 | file 文件 | package 包 | asset_category 媒资栏目 | asset 媒资包 | product 产品包 | terminal_UI 终端UI
     * @param mixed $message 消息体
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public static function write_global_execute_log($queue_type,$cp_id='',$sp_id='',$action='',$type='',$message='')
    {
        if(strlen($queue_type) < 1)
        {
            return self::return_data(1,'队列类型为空');
        }
        if(strlen($cp_id) <1)
        {
            $cp_id='cds_default_cp';
        }
        if(strlen($sp_id) <1)
        {
            $sp_id='cds_default_sp';
        }
        if(strlen($action) <1)
        {
            $action='cds_default_action';
        }
        if(strlen($type) < 1)
        {
            $type = "cds_default_type";
        }
        if(strlen($message) < 1)
        {
            return self::return_data(1,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $file_name = date("i") . '.txt';
        $str_operation = self::$flag_operation ? 'timer' : 'manual';
        $message_url = "/global_log/{$str_operation}/{$queue_type}/{$sp_id}/{$cp_id}/{$action}/{$type}/".date("Ymd")."/".date("H")."/{$file_name}";

        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(1,'文件路径生产绝对路径为空');
        }
        $getmypid = getmypid();
        $str_message_info = "[{$getmypid}] [" . date("Y-m-d H:i:s") . "]\r\n";
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $str_message_info.$message."\r\n", FILE_APPEND);
        if(!$result_file_put)
        {
            return self::return_data(1,'日志文件写入失败');
        }
        return self::return_data(0,'OK',$result_make_dir['data_info']);
    }
    /**
     * 下游下发的epg時内容存储执行文件（这个属于外部对接处理的范畴 使用该类）
     * @param string $sp_id  下游平台sp 标示
     * @param string $type 注入类型
     * @param string $action 注入行为动作
     * @param string $epg_message 注入的消息实体内容
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public static function write_execute_epg_import_content($sp_id,$type,$action,$epg_message)
    {
        if(!isset($sp_id) || strlen($sp_id) <1)
        {
            return self::return_data(NS_CDS_FAIL,'SP标示为空');
        }
        if(strlen($epg_message) <1)
        {
            return self::return_data(NS_CDS_FAIL,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = "{$sp_id}_{$time}_{$type}_{$action}_{$three_rand}.xml";
        $message_url = "/epg_import/{$sp_id}/inject_xml/".date("Ymd")."/".date("H")."/{$file_name}";
        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] != 0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(NS_CDS_FAIL,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $epg_message);
        if(!$result_file_put)
        {
            return self::return_data(NS_CDS_FAIL,'日志文件写入失败');
        }
        return self::return_data(NS_CDS_SUCCE,'OK',$result_make_dir['data_info']);
    }

    /**
     * 下游下发的epg上下线時内容存储执行文件（这个属于外部对接处理的范畴 使用该类）
     * @param string $sp_id  下游平台sp 标示
     * @param string $type 注入类型
     * @param string $action 注入行为动作
     * @param string $epg_message 注入的消息实体内容
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public static function write_execute_epg_line_content($sp_id,$type,$action,$epg_message)
    {
        if(!isset($sp_id) || strlen($sp_id) <1)
        {
            return self::return_data(NS_CDS_FAIL,'SP标示为空');
        }
        if(strlen($epg_message) <1)
        {
            return self::return_data(NS_CDS_FAIL,"消息为空，不执行对接文件发送或者接收的写入");
        }
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = "{$sp_id}_{$type}_{$time}_{$action}_{$three_rand}.xml";
        $message_url = "/epg_import/{$sp_id}/line_xml/".date("Ymd")."/".date("H")."/{$file_name}";
        $result_make_dir = self::make_dir($message_url);
        if($result_make_dir['ret'] != 0)
        {
            return $result_make_dir;
        }
        if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
        {
            return self::return_data(NS_CDS_FAIL,'文件路径生产绝对路径为空');
        }
        $result_file_put = @file_put_contents($result_make_dir['data_info']['absolute_dir'], $epg_message);
        if(!$result_file_put)
        {
            return self::return_data(NS_CDS_FAIL,'日志文件写入失败');
        }
        return self::return_data(NS_CDS_SUCCE,'OK',$result_make_dir['data_info']);
    }

    /**
     * 把本地文件上傳FTP V2
     * @param string $local_path 本地文件地址(全路径)
     * @param string $up_ftp  上传FTP的地址 ftp://user:password@ip:port
     * @param string $file_name 指定上传FTP上保存的文件名
     * @param string $relative_path 指定FTP文件路径
     * @param bool $passive 主被动模式，默认被动模式
     * @return Ambigous <multitype:number , multitype:number string >
     */
    static public function up_to_ftp_v2($local_path, $up_ftp, $file_name, $relative_path = '', $passive = true)
    {
        if(empty($local_path) || empty($up_ftp) || empty($file_name))
        {
            return self::return_data(1,'FTP参数传入错误');
        }
        $arr_ftp = parse_url($up_ftp);
        if(!isset($arr_ftp['port']))
        {
            $arr_ftp['port'] = 21;
        }
        $ftp = new nns_ftp($arr_ftp['host'], $arr_ftp['user'], $arr_ftp['pass'], $arr_ftp['port']);
        $ftpcon = $ftp->connect(10, $passive);
        if(!$ftpcon)
        {
            unset($ftp);
            return self::return_data(1,'FTP链接失败'.$up_ftp);
        }
        /**
         * 1、FTP绝对路径（相对当前账号的根目录的绝对路径），未指定上传到根目录
         * 2、FTP上文件命名名称
         * 3、本地文件地址（绝对路径）
         */
        $ftp_re = $ftp->up($relative_path, $file_name, $local_path);
        unset($ftp);
        if(!$ftp_re)
        {
            $error_msg = '上传FTP'.$up_ftp.'失败,上传路径:'.$relative_path.',上传文件名:'.$file_name.',上传的本地文件:'.$local_path;
            return self::return_data(1,$error_msg);
        }
        $ftp_url = str_replace(array("///","//"),"/",$up_ftp ."/". $relative_path.'/'.$file_name);
        return self::return_data(0,"上传成功",$ftp_url);
    }

    /**
     * 生成毫秒时间，主要提供给注入消息使用，为了消息自动解析的时候的提供排序
     * @return string
     * xinxin.deng 2018/9/29 9:46
     */
    public static function get_micro_time()
    {
        list ( $usec, $sec ) = explode ( ' ', microtime () );
        $time = str_pad(intval(substr($usec, 2, 4)),4,'0',STR_PAD_LEFT);
        $micro_time = date('YmdHis', time()) . $time;
        return $micro_time;
    }
}