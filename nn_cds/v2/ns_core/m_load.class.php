<?php
namespace ns_core;
class m_load
{
    /**
     * 加载V2 文件
     * @param $modelName
     * @return bool|string
     */
    static public function load($modelName)
    {
        $modelsPath = dirname(dirname(__FILE__)) . "/";
        
        $models = explode(".", $modelName);
        
        $models_path = implode("/", $models);
        
        $model_file_path = $modelsPath . $models_path . ".class.php";
        
        if (! file_exists($model_file_path)) 
        {
            return false;
        }
        include_once $model_file_path;
        
        $model_name = implode("\\", $models);
        
        return '\\' . $model_name;
    }

    /**
     * 加载除V2 以外的同级目录或者同级目录下的 子集文件
     * @param $modelName
     * @return bool
     */
    static public function load_old($modelName)
    {
        $modelsPath = dirname(dirname(dirname(__FILE__))) . "/";

        $modelName = trim(trim(trim($modelName, '/'), '\\'));
        
        $model_file_path = $modelsPath . $modelName;
        if (! file_exists($model_file_path)) 
        {
            return false;
        }
        include_once $model_file_path;
        return true;
    }

    /**
     * 加载NP库
     * @param $modelName
     * @return bool
     */
    static public function load_np($modelName)
    {
        $modelsPath = dirname(dirname(dirname(dirname(__FILE__)))) . "/np/";
        
        $modelName = trim(trim(trim($modelName, '/'), '\\'));
        
        $model_file_path = $modelsPath . $modelName;
        if (! file_exists($model_file_path)) 
        {
            return false;
        }
        include_once $model_file_path;
        return true;
    }

    /**
     * 加载CDn注入模板
     * @param $file
     * @return \C2_model|null
     */
    static public function load_delivery($file)
    {
        $arr_pathinfo = pathinfo($file);

        if(!isset($arr_pathinfo['dirname']))
        {
            return null;
        }
        $arr_pathinfo = explode("\\", rtrim($arr_pathinfo['dirname'],'\\'));
        $sp_file = array_pop($arr_pathinfo);
        $delivery = array_pop($arr_pathinfo);
        $project = array_pop($arr_pathinfo);

        $modelsPath = dirname(dirname(__FILE__)) . "/ns_tools/{$project}/{$delivery}/{$sp_file}/C2_model.class.php";
        
        if (! file_exists($modelsPath)) 
        {
            return null;
        }
        include_once $modelsPath;
        $object_C2_model = new \C2_model();
        return $object_C2_model;
    }
    /**
     * 加载CDN分发模块并返回分发对象【对外开发使用，目录存放于api/(project)/delivery/spid中】
     * @param $file //传递 __FILE__ 开发当前文件目录地址
     * @return object //返回开发自写的delivery对象
     */
    static public function load_cdn_delivery_object($file)
    {
        $arr_dir = explode('|',str_replace(array('/','\\'), '|', $file));
        $k = 0;
        foreach ($arr_dir as $key => $dir)
        {
            if($dir === 'ns_api')
            {
                $k = $key;
                break;
            }
        }
        $project = $arr_dir[$k+1];
        $sp_id = $arr_dir[$k+3];
        self::load("ns_api.{$project}.delivery.{$sp_id}.delivery");
        return new \delivery($sp_id);
    }

    /**
     * 加载file分发模块并返回分发对象【对外开发使用，目录存放于api/(project)/delivery/spid中】
     * @param $file //传递 __FILE__ 开发当前文件目录地址
     * @return object //返回开发自写的delivery对象
     */
    static public function load_file_delivery_object($file)
    {
        $arr_dir = explode('|',str_replace(array('/','\\'), '|', $file));
        $k = 0;
        foreach ($arr_dir as $key => $dir)
        {
            if($dir === 'ns_api')
            {
                $k = $key;
                break;
            }
        }
        $project = $arr_dir[$k+1];
        $sp_id = $arr_dir[$k+3];
        self::load("ns_api.{$project}.delivery.{$sp_id}.file_delivery");
        return new \file_delivery($sp_id);
    }
}