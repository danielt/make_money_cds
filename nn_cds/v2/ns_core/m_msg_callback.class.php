<?php
namespace ns_core;
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_data_model.command.m_command_task_log_inout");
class m_msg_callback
{
    static $explains = array();

    /**
     * 统一上游反馈数据入口（用于c2队列）
     * @param $source //上游来源，即CP标识
     * @param $message_id //上游消息唯一标识
     * @param int $code 状态码，见m_const常量
     * @param string $reason 原因
     * @param array $data 当前处理的队列数据，是否指定统一格式还是每个队列数据
     * @param array $c2_info
     */
    static public function is_ok($source, $message_id, $code, $reason='', $data=array(), $c2_info=array())
    {
        if (self::$explains == null)
        {
            self::$explains = array();
        }
        if (!isset(self::$explains[$source]) || self::$explains[$source] == null)
        {
            $project = \evn::get("project");
            $classname = \ns_core\m_load::load("ns_api.{$project}.message.{$source}.message");

            if ($classname == false)
            {
                return;
            }
            //排除message，没用命名空间
            $classname_arr = explode("\\",$classname);
            if(end($classname_arr) === 'message')
            {
                $classname = "message";
            }
            self::$explains[$source] = new $classname();
        }
        /**********中间队列处理过程记录 xinxin.deng 2018/8/7 11:26 start***********/
        $is_cdn = substr($reason, 0, 4) == "[epg" ? false : true;
        if ($is_cdn)
        {
            $re_w_log = \m_config::write_cdn_execute_log($reason, $c2_info['nns_org_id'], $c2_info['nns_id']);
        }
        else
        {
            $re_w_log = \m_config::write_epg_execute_log($reason, $c2_info['nns_org_id'], $c2_info['nns_id']);
        }

        $op_q = \nl_op_queue::query_by_params(\m_config::get_dc(), array('nns_id' => $c2_info['nns_op_id']), 1);
        if ($op_q['ret'] == 0 && count($op_q['data_info']) == 1)
        {
            switch ($code)
            {
                case 0:
                    $command_status = NS_CDS_COMMAND_CDN_SUCCESS;
                    break;
                case 5:
                    $command_status = NS_CDS_COMMAND_CDN_LOADING;
                    break;
                case 7:
                    $command_status = NS_CDS_COMMAND_CDN_CANCEL;
                    break;
                case 97:
                    $command_status = NS_CDS_COMMAND_EPG_WAIT;
                    break;
                case 98:
                    $command_status = NS_CDS_COMMAND_EPG_LOADING;
                    break;
                case 99:
                    $command_status = NS_CDS_COMMAND_EPG_SUCCESS;
                    break;
                case 100:
                    $command_status = NS_CDS_COMMAND_EPG_CANCEL;
                    break;
                default:
                    $command_status = $is_cdn ? NS_CDS_COMMAND_CDN_FAIL: NS_CDS_COMMAND_EPG_FAIL;
                    break;
            }
            $command_task_log_params = array(
                'base_info' => array(
                    'nns_command_task_id' => $op_q['data_info'][0]['nns_command_task_id'],
                    'nns_status' => $command_status,
                    'nns_desc' => $reason,
                    'nns_execute_url' => $re_w_log['data_info']['base_dir'],
                )
            );
            $obj_command_task_log = new \m_command_task_log_inout();
            $ret = $obj_command_task_log->add($command_task_log_params);
        }
        /**********中间队列处理过程记录 xinxin.deng 2018/8/7 11:26 end***********/

        //判断任务是否属于最终状态
        $center_op = new \ns_model\center_op\center_op_queue($c2_info['nns_org_id'],$c2_info['nns_type']);
        $finally_re = $center_op->is_finally_status($c2_info);
        $data['is_finally'] = null;
        //判断是最终状态，则删除中心同步指令
        if ($finally_re['ret'] == 0 && isset($c2_info['nns_op_id']) && strlen($c2_info['nns_op_id']) > 0)
        {
            if ($code != 5 || $code != 98)
            {
                $data['is_finally'] = 0;
            }

            $re_report = $center_op->report_op_task($c2_info['nns_op_id']);
            \m_config::base_write_log(NS_CDS_CENTER_OP_QUEUE,'delete','中兴同步指令队列执行删除:结果[ret]=' . $re_report['ret']. '描述:' . $re_report['reason'],'',$c2_info['nns_op_id']);
        }
        unset($center_op);

        if ($code == 99 || $code == 5)
        {
            $code = 0;
        }
        //最终状态成功，其他失败状态是否反馈？
        //其他失败状态、正在注入状态也要反馈 --xinxin.deng 2018/3/5 16:16
        //if($code == NS_TASK_FINA_STATE)
        //{
            self::$explains[$source]->is_ok($message_id, $code, $reason, $data, $c2_info['nns_org_id']);
        //}
    }


    /**
     * 统一上游反馈数据入口(用于直接回调，切片、透传等)
     * ----回调的消息队列的方法封装由开发人员根据项目实际情况进行反馈数据封装
     * @param $source //上游来源，即CP标识
     * @param $message_id //上游消息唯一标识
     * @param int $code 状态码，见m_const常量
     * @param string $reason 原因
     * @param array $data 当前处理的队列数据，是否指定统一格式还是每个队列数据
     * @param array $c2_info
     * @param string $format
     * xinxin.deng 2018/10/31 15:14
     */
    static public function is_ok_v2($source, $message_id, $code, $reason='', $data=array(), $c2_info=array(), $format='xml')
    {

        if (self::$explains == null)
        {
            self::$explains = array();
        }

        if (!isset(self::$explains[$source]) || self::$explains[$source] == null)
        {
            $project = \evn::get("project");
            $classname = \ns_core\m_load::load("ns_api.{$project}.message.{$source}.message");

            if ($classname == false)
            {
                return;
            }
            //排除message，没用命名空间
            $classname_arr = explode("\\",$classname);
            if(end($classname_arr) === 'message')
            {
                $classname = "message";
            }
            self::$explains[$source] = new $classname();
        }
        if ($code == 0)
        {

        }
        self::$explains[$source]->is_ok($message_id, $code, $reason, $data, $c2_info['nns_org_id'], $format);
    }

}