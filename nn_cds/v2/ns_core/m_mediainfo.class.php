<?php 
include_once dirname(dirname(__FILE__))."/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load_old("nn_class/mediainfo/vendor/autoload.php");
use Mhor\MediaInfo\MediaInfo;
/**
 * linux|windows mediainfo 查询 
 * @author pan.liang
 * @example m_mediainfo::exec("D:\starcordemo\php-mediainfo-master\php-mediainfo-master/test.ts","d9a9ecb3798c104471b420d95f130153","mediainfo/172/123/d9a9ecb3798c104471b420d95f130153.txt")
 * @return 
 */
class m_mediainfo extends m_config
{
    public static $arr_media_format = array(
        'ts',
        'mp3',
        'mp4',
        'mpg',
        'flv',
        'wma',
        'm2t',
        'kux',
        'wmv',
        'm2ts',
        'mov',
        'mpeg',
        'rmvb',
        'avi',
    );
    
    public static $arr_media_model = array(
        'version'=>array(
                    'name'=>'mediainfo版本',
                    'value'=>'',
                ),
        'general'=>array(
            'name'=>'基本信息',
            'value'=>array(
                'ID'=>array(
                    'name'=>'ID',
                    'value'=>'',
                ),
                'VideoCount'=>array(
                    'name'=>'视屏数量',
                    'value'=>'',
                ),
                'AudioCount'=>array(
                    'name'=>'音频数量',
                    'value'=>'',
                ),
                'MenuCount'=>array(
                    'name'=>'菜单数量',
                    'value'=>'',
                ),
                'FileExtension'=>array(
                    'name'=>'文件扩展信息',
                    'value'=>'',
                ),
                'Format'=>array(
                    'name'=>'文件格式',
                    'value'=>'',
                ),
                'FileSize'=>array(
                    'name'=>'文件大小',
                    'value'=>'',
                ),
                'Duration'=>array(
                    'name'=>'持续时间',
                    'value'=>'',
                ),
                'OverallBitRate_Mode'=>array(
                    'name'=>'混合码率模式',
                    'value'=>'',
                ),
                'OverallBitRate'=>array(
                    'name'=>'平均混合码率',
                    'value'=>'',
                ),
                'FrameRate'=>array(
                    'name'=>'帧率',
                    'value'=>'',
                ),
                'FrameCount'=>array(
                    'name'=>'总帧数',
                    'value'=>'',
                ),
                'StreamSize'=>array(
                    'name'=>'视屏流大小',
                    'value'=>'',
                ),
                'File_Created_Date'=>array(
                    'name'=>'文件创建时间',
                    'value'=>'',
                ),
                'File_Created_Date_Local'=>array(
                    'name'=>'文件当地创建时间',
                    'value'=>'',
                ),
                'File_Modified_Date'=>array(
                    'name'=>'文件修改时间',
                    'value'=>'',
                ),
                'File_Modified_Date_Local'=>array(
                    'name'=>'文件当地修改时间',
                    'value'=>'',
                ),
                'OverallBitRate_Precision_Min'=>array(
                    'name'=>'最小混合码率',
                    'value'=>'',
                ),
                'OverallBitRate_Precision_Max'=>array(
                    'name'=>'最大混合码率',
                    'value'=>'',
                ),
            ),
        ),
        'Video'=>array(
            'name'=>'视屏基本信息',
            'value'=>array(
                'StreamOrder'=>array(
                    'name'=>'视屏流序号',
                    'value'=>'',
                ),
                'ID'=>array(
                    'name'=>'ID',
                    'value'=>'',
                ),
                'MenuID'=>array(
                    'name'=>'菜单ID',
                    'value'=>'',
                ),
                'Format'=>array(
                    'name'=>'视屏格式',
                    'value'=>'',
                ),
                'Format_Profile'=>array(
                    'name'=>'格式优先级',
                    'value'=>'',
                ),
                'Format_Level'=>array(
                    'name'=>'格式概况',
                    'value'=>'',
                ),
                'Format_Settings_CABAC'=>array(
                    'name'=>'格式设置, CABAC',
                    'value'=>'',
                ),
                'Format_Settings_RefFrames'=>array(
                    'name'=>'格式设置, 参考帧',
                    'value'=>'',
                ),
                'CodecID'=>array(
                    'name'=>'编解码器ID',
                    'value'=>'',
                ),
                'Duration'=>array(
                    'name'=>'视屏时长',
                    'value'=>'',
                ),
                'BitRate'=>array(
                    'name'=>'码率',
                    'value'=>'',
                ),
                'Width'=>array(
                    'name'=>'宽度',
                    'value'=>'',
                ),
                'Height'=>array(
                    'name'=>'高度',
                    'value'=>'',
                ),
                'Sampled_Width'=>array(
                    'name'=>'基本宽度',
                    'value'=>'',
                ),
                'Sampled_Height'=>array(
                    'name'=>'基本高度',
                    'value'=>'',
                ),
                'PixelAspectRatio'=>array(
                    'name'=>'像素纵横比',
                    'value'=>'',
                ),
                'DisplayAspectRatio'=>array(
                    'name'=>'文件当地修改时间',
                    'value'=>'',
                ),
                'FrameRate'=>array(
                    'name'=>'帧码率',
                    'value'=>'',
                ),
                'FrameCount'=>array(
                    'name'=>'帧数量',
                    'value'=>'',
                ),
                'Standard'=>array(
                    'name'=>'标准',
                    'value'=>'',
                ),
                'ColorSpace'=>array(
                    'name'=>'色彩空间',
                    'value'=>'',
                ),
                'ChromaSubsampling'=>array(
                    'name'=>'色度抽样',
                    'value'=>'',
                ),
                'BitDepth'=>array(
                    'name'=>'位深',
                    'value'=>'',
                ),
                'ScanType'=>array(
                    'name'=>'扫描方式',
                    'value'=>'',
                ),
                'Delay'=>array(
                    'name'=>'数据密度【码率/(像素×帧率)】',
                    'value'=>'',
                ),
                'StreamSize'=>array(
                    'name'=>'流大小',
                    'value'=>'',
                ),
            ),
        ),
        'Audio'=>array(
            'name'=>'音屏基本信息',
            'value'=>array(
                'StreamOrder'=>array(
                    'name'=>'音屏流序号',
                    'value'=>'',
                ),
                'ID'=>array(
                    'name'=>'ID',
                    'value'=>'',
                ),
                'MenuID'=>array(
                    'name'=>'菜单ID',
                    'value'=>'',
                ),
                'Format'=>array(
                    'name'=>'音屏格式',
                    'value'=>'',
                ),
                'Format_Version'=>array(
                    'name'=>'格式版本',
                    'value'=>'',
                ),
                'Format_Profile'=>array(
                    'name'=>'格式概况',
                    'value'=>'',
                ),
                'Format_Settings_Mode'=>array(
                    'name'=>'格式设置',
                    'value'=>'',
                ),
                'Format_Settings_ModeExtension'=>array(
                    'name'=>'格式设置, CABAC',
                    'value'=>'',
                ),
                'CodecID'=>array(
                    'name'=>'编解码器ID',
                    'value'=>'',
                ),
                'Duration'=>array(
                    'name'=>'音屏时长',
                    'value'=>'',
                ),
                'BitRate_Mode'=>array(
                    'name'=>'码率模式',
                    'value'=>'',
                ),
                'BitRate'=>array(
                    'name'=>'码率',
                    'value'=>'',
                ),
                'Channels'=>array(
                    'name'=>'声道',
                    'value'=>'',
                ),
                'SamplesPerFrame'=>array(
                    'name'=>'帧率',
                    'value'=>'',
                ),
                'SamplingRate'=>array(
                    'name'=>'采样率',
                    'value'=>'',
                ),
                'SamplingCount'=>array(
                    'name'=>'采样数量',
                    'value'=>'',
                ),
                'FrameRate'=>array(
                    'name'=>'帧率',
                    'value'=>'',
                ),
                'FrameCount'=>array(
                    'name'=>'帧数量',
                    'value'=>'',
                ),
                'Compression_Mode'=>array(
                    'name'=>'压缩模式',
                    'value'=>'',
                ),
                'Delay'=>array(
                    'name'=>'音频延迟',
                    'value'=>'',
                ),
                'Delay_Source'=>array(
                    'name'=>'延迟源',
                    'value'=>'',
                ),
                'StreamSize'=>array(
                    'name'=>'音频流大小',
                    'value'=>'',
                ),
                'StreamSize_Proportion'=>array(
                    'name'=>'音频流均衡',
                    'value'=>'',
                ),
            ),
        ),
        'Menu'=>array(
            'name'=>'菜单信息',
            'value'=>array(
                'StreamOrder'=>array(
                    'name'=>'流排序',
                    'value'=>'',
                ),
                'ID'=>array(
                    'name'=>'ID',
                    'value'=>'',
                ),
                'MenuID'=>array(
                    'name'=>'菜单ID',
                    'value'=>'',
                ),
                'Format'=>array(
                    'name'=>'格式',
                    'value'=>'',
                ),
                'Duration'=>array(
                    'name'=>'时长',
                    'value'=>'',
                ),
                'Delay'=>array(
                    'name'=>'延迟',
                    'value'=>'',
                ),
                'List_StreamKind'=>array(
                    'name'=>'视屏流大小',
                    'value'=>'',
                ),
                'List_StreamPos'=>array(
                    'name'=>'文件创建时间',
                    'value'=>'',
                ),
                'ServiceName'=>array(
                    'name'=>'服务名称',
                    'value'=>'',
                ),
                'ServiceProvider'=>array(
                    'name'=>'服务提供商',
                    'value'=>'',
                ),
                'ServiceType'=>array(
                    'name'=>'服务类型',
                    'value'=>'',
                ),
                'pointer_field'=>array(
                    'name'=>'指示字段',
                    'value'=>'',
                ),
                'section_length'=>array(
                    'name'=>'区域长度',
                    'value'=>'',
                ),
            ),
        ),
        'URL'=>array(
            'name'=>'文件原始路径',
            'value'=>'',
        ),
    );
    
    
    static $arr_linux_windows_exchange = array(
        'General'=>array(
            'Count_of_video_streams'=>'VideoCount',
            'Count_of_audio_streams'=>'AudioCount',
            'Count_of_menu_streams'=>'MenuCount',
            'File_extension'=>'FileExtension',
            'File_size'=>'FileSize',
            'Overall_bit_rate_mode'=>'OverallBitRate_Mode',
            'Overall_bit_rate'=>'OverallBitRate',
            'Frame_rate'=>'FrameRate',
            'Frame_count'=>'FrameCount',
            'Stream_size'=>'StreamSize',
            'File_last_modification_date'=>'File_Modified_Date',
            'File_last_modification_date__local_'=>'File_Modified_Date_Local',
        ),
        'Video'=>array(
            'Format_profile'=>'Format_Profile',
            'Menu_ID'=>'MenuID',
            'Format_settings'=>'Format_Level',
            'Format_settings__CABAC'=>'Format_Settings_CABAC',
            'Format_settings__RefFrames'=>'Format_Settings_RefFrames',
            'Codec_ID'=>'CodecID',
            'Bit_rate'=>'BitRate',
            'Pixel_aspect_ratio'=>'PixelAspectRatio',
            'Display_aspect_ratio'=>'DisplayAspectRatio',
            'Frame_rate'=>'FrameRate',
            'Frame_count'=>'FrameCount',
            'Color_space'=>'ColorSpace',
            'Chroma_subsampling'=>'ChromaSubsampling',
            'Bit_depth'=>'BitDepth',
            'Scan_type'=>'ScanType',
            'Stream_size'=>'StreamSize',
        ),
        'Audio'=>array(
            'Format_version'=>'Format_Version',
            'Menu_ID'=>'MenuID',
            'Format_settings'=>'Format_Settings_Mode',
            'Codec_ID'=>'CodecID',
            'Bit_rate_mode'=>'BitRate_Mode',
            'Bit_rate'=>'BitRate',
            'Channel_s_'=>'Channels',
            'Sampling_rate'=>'SamplingRate',
            'Samples_count'=>'SamplingCount',
            'Frame_count'=>'FrameCount',
            'Compression_mode'=>'Compression_Mode',
            'Delay__origin'=>'Delay_Source',
            'Stream_size'=>'StreamSize',
            'Proportion_of_this_stream'=>'StreamSize_Proportion',
        ),
        'Menu'=>array(
            'Menu_ID'=>'MenuID',
            'Service_name'=>'ServiceName',
            'Service_provider'=>'ServiceProvider',
            'Service_type'=>'ServiceType',
        ),
    );
    
    /**
     * 初步验证片源的格式
     * @param string $str_path 片源文件全路径
     */
    public static function check_media_format($str_path)
    {
        $str_path=trim($str_path);
        if(strlen($str_path) <1)
        {
            return self::return_data(1,"片源路径为空");
        }
        $arr_pathinfo = pathinfo($str_path);
        if(!isset($arr_pathinfo['extension']) || strlen($arr_pathinfo['extension'])<1)
        {
            return self::return_data(1,"片源扩展信息未获取到,路径{$str_path}");
        }
        if(!in_array(strtolower($arr_pathinfo['extension']), self::$arr_media_format))
        {
            return self::return_data(1,"片源扩展信息为{$arr_pathinfo['extension']},路径{$str_path}允许的格式为[".implode(',', self::$arr_media_format)."]");
        }
        return self::return_data(0,'片源格式验证Ok');
    }
    
    /**
     * 检查片源文件是否存在
     * @param string $str_path 片源文件全路径
     */
    public static function check_file_exsist($str_path)
    {
        $str_path=trim($str_path);
        if(strlen($str_path) <1)
        {
            return self::return_data(1,"片源路径为空");
        }
        return (file_exists($str_path)) ? self::return_data(0,'片源文件存在') : self::return_data(1,"片源文件不存在路径为{$str_path}");
    }
    
    /**
     * mediainfo exec
     * @param string $str_path 片源文件全路径
     * @param string $str_guid nns_medium_library_file表的nns_id字段
     * @param string $str_log_path nns_medium_library_file表的nns_ext_url字段 第一次处理文件是不存在的置为空，后续可以传入该路径
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|array:number|string
     */
    public static function exec($str_path,$str_guid,$str_log_path='')
    {
        if(strlen($str_guid) <1)
        {
            return self::return_data(1,"str_guid参数为空");
        }
        $str_log_path = trim(trim($str_log_path),'/');
        $file_temp_dir = dirname(dirname(dirname(__FILE__))).'/data/temp/mediainfo';
        $file_temp_log_dir = dirname(dirname(dirname(__FILE__))).'/data';
        if(strlen($str_log_path) <1 || !file_exists($file_temp_log_dir.'/'.$str_log_path))
        {
            $result_make_dir = self::make_dir('/mediainfo/'.rand(100, 199).'/'.rand(100, 199).'/'.$str_guid.'.txt');
            if($result_make_dir['ret'] !=0)
            {
                return $result_make_dir;
            }
            $str_log_path = $result_make_dir['data_info']['base_dir'];
            $str_absolute_dir = $result_make_dir['data_info']['absolute_dir'];
        }
        else
        {
            $str_absolute_dir = $file_temp_log_dir.'/'.$str_log_path;
        }
        $file_temp_name = np_guid_rand().".xml";
//         $file_temp_name = "5ac24c003eedb1c4127649e391a1bce0.xml";
        if (!is_dir($file_temp_dir))
		{
			@mkdir($file_temp_dir, 0777, true);
		}
        $result_check_path = self::check_file_exsist($str_path);
        if($result_check_path['ret'] !='0')
        {
            @file_put_contents($str_absolute_dir, json_encode($result_check_path));
            $result_check_path['ext_info']['LOGURL'] = $str_log_path;
            return $result_check_path;
        }
        $result_check_fomart = self::check_media_format($str_path);
        if($result_check_fomart['ret'] !='0')
        {
            @file_put_contents($str_absolute_dir, json_encode($result_check_fomart));
            $result_check_fomart['ext_info']['LOGURL'] = $str_log_path;
            return $result_check_fomart;
        }
        if (strtolower(substr(php_uname(), 0, 7)) == "windows")//windows下执行
        {
            $str_exec="cmd /V:ON /E:ON /D /C mediainfo --OUTPUT=XML -f ".'"'."{$str_path}".'"'." 1> {$file_temp_dir}/{$file_temp_name}";
            $str_func = 'windows_exec';
        }
        else //LINUX下执行
        {
            $str_exec="/usr/bin/mediainfo -u www --OUTPUT=XML -f {$str_path} 1> {$file_temp_dir}/{$file_temp_name}";
            $str_exec = $str_path;
            $str_func = 'linux_exec';
        }
        $result = self::$str_func($str_exec,$file_temp_dir.'/'.$file_temp_name);
        if(file_exists($file_temp_dir.'/'.$file_temp_name))
        {
//             @unlink($file_temp_dir.'/'.$file_temp_name);
        }
        @file_put_contents($str_absolute_dir, json_encode($result));
        $result['ext_info']['LOGURL'] = $str_log_path;
        return $result;
    }
    
    
    public static function exec_ftp($ftp_url,$str_guid_file)
    {
        if(file_exists($str_guid_file))
        {
            return self::return_data(1,'文件已存在不做任何处理');
        }
        $str_exec="mediainfo -u www --OUTPUT=XML -f {$ftp_url} > {$str_guid_file}";
        exec($str_exec);
        usleep(5000);
        $result = file_get_contents($str_guid_file);
        @unlink($str_guid_file);
        if(!$result)
        {
            return self::return_data(1,'获取文件内容失败'.$str_guid_file.';EXEC:'.$str_exec);
        }
        if(!self::is_xml($result))
        {
            return self::return_data(1,"内容非XMl".';EXEC:'.$str_exec);
        }
        return self::return_data(0,'获取文件内容成功'.$str_guid_file.';EXEC:'.$str_exec,$result);
    }
    
    public static function xml_windows_params($str_content)
    {
        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->loadXML($str_content);
        $str_content = $dom->saveXML();
        $obj_track = $dom->getElementsByTagName('track');
        $obj_media = $dom->getElementsByTagName('media');
        if($obj_track->length <1)
        {
            return self::return_data(1,'文件解析错误',$str_content);
        }
        if($obj_media->length <1)
        {
            return self::return_data(1,'文件解析错误',$str_content);
        }
        $last_data = self::$arr_media_model;
        $i = 0;
        foreach ($obj_track as $obj_track_val)
        {
            $type = $obj_track_val->getAttribute('type');
            foreach($obj_track_val->childNodes as $subNodes)
            {
                if($subNodes->nodeName == '#text')
                {
                    continue;
                }
                if($subNodes->nodeName == 'extra')
                {
                    foreach($subNodes->childNodes as $subNodes2)
                    {
                        if($subNodes2->nodeName == '#text')
                        {
                            continue;
                        }
                        if(isset($last_data[$type]['value'][$subNodes2->nodeName]))
                        {
                            $last_data[$type]['value'][$subNodes2->nodeName]['value'] = trim($subNodes2->nodeValue);
                        }
                    }
                }
                else
                {
                    if(isset($last_data[$type]['value'][$subNodes->nodeName]))
                    {
                        $last_data[$type]['value'][$subNodes->nodeName]['value'] = trim($subNodes->nodeValue);
                    }
                }
                $i++;
            }
        }
        if($i<50)
        {
            return self::return_data(1,'解析出来的数据很多匹配不上',$str_content);
        }
        foreach ($obj_media as $obj_media_val)
        {
            $str_ref = $obj_media_val->getAttribute('ref');
            if(strlen($str_ref) >0)
            {
                $last_data['URL']['value'] = $str_ref;
                break;
            }            
        }
        return self::return_data(0,'Ok',$last_data);
    }
    
    
    
    public static function xml_linux_params($arr_content)
    {
//         $dom = new DOMDocument('1.0', 'utf-8');
//         $dom->loadXML($str_content);
//         $str_content = $dom->saveXML();
//         $obj_track = $dom->getElementsByTagName('track');
//         if($obj_track->length <1)
//         {
//             return self::return_data(1,'文件解析错误',$str_content);
//         }
//         $last_data = null;
//         $last_data_1 = self::$arr_media_model;
        $last_data = self::$arr_media_model;
        if(!is_array($arr_content) || empty($arr_content))
        {
            return self::return_data(1,'解析出来的数据非数组');
        }
        $i = 0;
        foreach ($last_data as $key=>$value)
        {
            
            
            if(isset(self::$arr_linux_windows_exchange[$key]) && is_array(self::$arr_linux_windows_exchange[$key]) && !empty(self::$arr_linux_windows_exchange[$key]))
            {
                $linuxarr = array_keys(self::$arr_linux_windows_exchange[$key]);
                
                foreach ($value as $_k=>$_v)
                {
                    if($key == 'General' && $_k == 'Complete_name')
                    {
                        $url = $_v;
                        continue;
                    }
                    if(!in_array($_k, $linuxarr))
                    {
                        continue;
                    }
                    if(isset($last_data_1[$key]['value'][self::$arr_linux_windows_exchange[$key][$_k]]['value']))
                    {
                        $last_data_1[$key]['value'][self::$arr_linux_windows_exchange[$key][$_k]]['value'] = $_v;
                    }
                }
            }
        }
        
        
        
        
        foreach ($obj_track as $obj_track_val)
        {
            $type = $obj_track_val->getAttribute('type');
            foreach($obj_track_val->childNodes as $subNodes)
            {
                if($subNodes->nodeName == '#text')
                {
                    continue;
                }
                if($subNodes->nodeName == 'extra')
                {
                    foreach($subNodes->childNodes as $subNodes2)
                    {
                        if($subNodes2->nodeName == '#text')
                        {
                            continue;
                        }
                        if(isset($last_data_1[$type]['value'][$subNodes->nodeName]['value'][$subNodes2->nodeName]))
                        {
                            $last_data_1[$type]['value'][$subNodes->nodeName]['value'][$subNodes2->nodeName]['value'] = trim($subNodes2->nodeValue);
                            continue;
                        }
                        if(!isset($last_data[$type][$subNodes2->nodeName]))
                        {
                            $last_data[$type][$subNodes2->nodeName] = trim($subNodes2->nodeValue);
                        }
                    }
                }
                else
                {
                    if(isset($last_data_1[$type]['value'][$subNodes->nodeName]))
                    {
                        $last_data_1[$type]['value'][$subNodes->nodeName]['value'] = trim($subNodes->nodeValue);
                        continue;
                    }
                    if(!isset($last_data[$type][$subNodes->nodeName]))
                    {
                        $last_data[$type][$subNodes->nodeName] = trim($subNodes->nodeValue);
                    }
                }
                $i++;
            }
        }
        if($i<50)
        {
            return self::return_data(1,'解析出来的数据很多匹配不上',$str_content);
        }
        $url = '';
        foreach ($last_data as $key=>$value)
        {
            if(isset(self::$arr_linux_windows_exchange[$key]) && is_array(self::$arr_linux_windows_exchange[$key]) && !empty(self::$arr_linux_windows_exchange[$key]))
            {
                $linuxarr = array_keys(self::$arr_linux_windows_exchange[$key]);
                
                foreach ($value as $_k=>$_v)
                {
                    if($key == 'General' && $_k == 'Complete_name')
                    {
                        $url = $_v;
                        continue;
                    }
                    if(!in_array($_k, $linuxarr))
                    {
                        continue;
                    }
                    if(isset($last_data_1[$key]['value'][self::$arr_linux_windows_exchange[$key][$_k]]['value']))
                    {
                        $last_data_1[$key]['value'][self::$arr_linux_windows_exchange[$key][$_k]]['value'] = $_v;
                    }
                }
            }
        }
        $last_data_1['URL']['value'] = $url;
        return self::return_data(0,'Ok',$last_data_1);
    }
    
    /**
     * linux 下执行的命令
     * @param unknown $str_exec
     */
    public static function linux_exec($str_exec,$file_temp_dir)
    {
        $mediaInfo = new MediaInfo();
        $mediaInfo->setConfig('command', 'mediainfo');
        $mediaInfoContainer = $mediaInfo->getInfo($str_exec);
        $mediaInfoContainer = (array)json_decode(json_encode($mediaInfoContainer),true);
        return self::return_data(0,"Ok",$mediaInfoContainer);
        $result = self::xml_linux_params($mediaInfoContainer);
        if($result['ret'] !=0)
        {
            return $result;
        }
        if(!isset($result['data_info']) || !is_array($result['data_info']) || empty($result['data_info']))
        {
            return self::return_data(1,"数据解析错误");
        }
        return $result;
    }
    
    /**
     * windows 下执行命令
     * @param unknown $str_exec
     */
    public static function windows_exec($str_exec,$file_temp_dir)
    {
        exec($str_exec,$result,$return_var);
        $result_content = file_get_contents($file_temp_dir);
        if(!m_config::is_xml($result_content))
        {
            return self::return_data(0,"windos mediainfo exec result非xml,执行命令{$str_exec},临时文件路径{$file_temp_dir}");
        }
        $result = self::xml_windows_params($result_content);
        if($result['ret'] !=0)
        {
            return $result;
        }
        if(!isset($result['data_info']) || !is_array($result['data_info']) || empty($result['data_info']))
        {
            return self::return_data(1,"数据解析错误");
        }
        return $result;
    }
}