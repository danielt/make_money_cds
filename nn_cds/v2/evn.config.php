<?php
include_once dirname(dirname(__FILE__)).'/nn_cms_config/nn_cms_global.php';
//获取项目名称
global $g_project_name;
$project_name = isset($g_project_name) ? $g_project_name : '';
unset($g_project_name);
$project_name = strlen($project_name) >0 ? $project_name : 'jscn';
//获取项目运行环境地址
global $g_evn_operation_bin;
$evn_operation_bin = isset($g_evn_operation_bin) ? $g_evn_operation_bin : '';
unset($g_evn_operation_bin);
$evn_operation_bin = strlen($evn_operation_bin) >0 ? $evn_operation_bin : "E:\php\php.exe";
$evn = array(
    'php_execute_path'=>$evn_operation_bin,
    'project'=>$project_name
);
return $evn;