<?php
//error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_model.message.message_explain");
\ns_core\m_load::load("ns_core.m_pinyin");
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load_np("np_http_curl.class.php");

$arr_dir = explode('|', str_replace(array ('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('DEFAULT_SOURCE_ID', $str_dir);

class message extends \ns_model\message\message_explain
{
    private $arr_picture = null;
    private $arr_map = null;
    private $msg_id = '';
    private $source_id = '';
    private $arr_cp_config = array();

    //一级分类,影片种类
    private $category_map = array (
        '0' => '电影',  //电影
        '1' => '电视剧',  //电视剧
        '2' => '娱乐,综艺',  //综艺
        '3' => '动画',  //动漫
        '4' => '音乐',  //音乐
        '5' => '纪实,纪录片',  //
        '6' => '法治,科教',  //教育
        '7' => '体育',  //体育
        '8' => '生活',  //生活
        '9' => '经济',  //财经
        '10' => '特别节目',//微电影
        '11' => '品牌专区',//品牌专区
        '12' => '广告',  //广告
        '13' => '新闻', //新闻
        '14' => '公开课',
        '15' => '外语及其他语言',
        '16' => '青少年',
        '17' => '播客',
        '18' => '游戏',
    );
    //清晰度
    private $media_mode = array(
        'PC_4K' => '4k',
        'PC_2K' => 'hd',//2k
        'PC_HD' => 'hd',
        'PC_STD' => 'std',
        'PC_LOW' => 'low',
        'STB_4K' => '4k',
        'STB_2K' => 'hd',
        'STB_HD' => 'hd',
        'STB_STD' => 'std',
        'STB_LOW' => 'low',
        'PHONE_4K' => '4k',
        'PHONE_2K' => 'hd',
        'PHONE_HD' => 'hd',
        'PHONE_STD' => 'std',
        'PHON_LOW' => 'low',
        'PAD_4K' => '4k',
        'PAD_2K' => 'hd',
        'PAD_HD' => 'hd',
        'PAD_STD' => 'std',
        'PAD_LOW' => 'low',
    );

    public function explain($message)
    {
        \ns_core\m_load::load_np("np_xml2array.class.php");

        $this->msg_id = $message['nns_message_id'];
        $this->source_id = $message['nns_cp_id'];
        $this->arr_cp_config = \m_config::_get_cp_info($message['nns_cp_id']);

        $content = $this->get_message_content($message);
        if($content['ret'] !=0 || strlen($content['data_info']) < 1)
        {
            \m_config::write_message_execute_log("消息接收成功,注入失败，消息ID[{$message['nns_message_id']}]" . $content['reason'],$this->source_id);
            $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL,'消息解析失败');
            return $content;
        }
        $str_content = \m_config::trim_xml_header($content['data_info']);
        if(!m_config::is_xml($str_content))
        {
            \m_config::write_message_execute_log("消息接收成功,注入失败,消息ID[{$message['nns_message_id']}]xml内容非xml结构",$this->source_id);
            $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL,'消息解析失败');
            return \m_config::return_data(NS_CDS_FAIL,"消息ID[{$message['nns_message_id']}]xml内容非xml结构");
        }
        //解析下载的XML
        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->loadXML($str_content);
        $xml = $dom->saveXML();
        $xml_arr = np_xml2array::getArrayData($xml);
        if ($xml_arr["ret"] != 1)
        {
            \m_config::write_message_execute_log("消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],解析得到的媒资元数据错误",$this->source_id);
            $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL,'消息解析失败');
            return \m_config::return_data(NS_CDS_FAIL, "解析得到的媒资元数据错误");
        }

        $Objects = null;

        foreach ($xml_arr['data']['children'] as $v)
        {
            if (empty($Objects) && $v['name'] == 'Objects')
            {
                $Objects = $v['children'];
            }
            else if (empty($this->arr_map) && $v['name'] == 'Mappings')
            {
                $this->arr_map = $v['children'];
            }
        }

        //Series数据数组
        $arr_series = array ();
        //Program数据数组
        $arr_program = array ();
        //Movie数据数组
        $arr_movie = array ();

        //以ContentID字段,其次ID为原始ID
        if (!empty($Objects) && is_array($Objects))
        {
            foreach ($Objects as $obj)
            {
                if ($obj['attributes']['ElementType'] == 'Series') //主媒资
                {
                    $arr_series[] = $this->make_key_value_arr($obj);
                }
                else if ($obj['attributes']['ElementType'] == 'Program') //分集
                {
                    $arr_program[] = $this->make_key_value_arr($obj);
                }
                else if ($obj['attributes']['ElementType'] == 'Movie') //片源
                {
                    $arr_movie[] = $this->make_key_value_arr($obj);
                }
                else if ($obj['attributes']['ElementType'] == 'Picture')//海报
                {
                    $this->arr_picture[] = $this->make_key_value_arr($obj);
                }
            }
        }
        //主媒资操作
        if (!empty($arr_series))
        {
            foreach ($arr_series as $val_series)
            {
                //主媒资删除
                if (strtoupper($val_series['Action']) == 'DELETE')
                {
                    $del_series = array(
                        'base_info'=>array(
                            'nns_asset_import_id' => $val_series['ContentID'],
                            'nns_import_source'=>isset($val_series['nns_import_source']) ? $val_series['nns_import_source'] : evn::get("project"),
                            'nns_cp_id'=>$message['nns_cp_id'],
                        ),
                    );
                    $result = $this->vod_action('delete', $del_series);
                    if ($result['ret'] != NS_CDS_SUCCE)
                    {
                        \m_config::write_message_execute_log("消息接收成功,消息ID[{$message['nns_message_id']}]队列生成结果：" . $result['reason'],$this->source_id);
                        $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL,'工单Series删除任务处理失败');
                        return $result;
                    }
                }
                else
                {
                    //默认一级分类为电影
                    $view_type = 0;
                    $asset_category = $val_series['key_val_list']['PgmCategory'] ? $val_series['key_val_list']['PgmCategory'] : '电影';

                    foreach ($this->category_map as $index => $v)
                    {
                        $v = explode(',', $v);
                        if(!is_array($v) || empty($v))
                        {
                            continue;
                        }
                        foreach ($v as $_v)
                        {
                            if (stripos($asset_category, $_v) !== false)
                            {
                                $view_type = $index;
                                break ;
                            }
                        }
                    }

                    $do_category = array(
                        'base_info'=>array(
                            'nns_name' => $asset_category,   //栏目名称支持多层目录，多层目录的时候 /分割
                            'nns_import_category_id' => '', //栏目名称支持多层目录，多层目录的时候 /分割
                            'nns_cp_id' => $message['nns_cp_id'],
                            'nns_import_parent_category_id' => '',
                            'nns_video_type' => '0',  //媒资类型  0 点播 | 1 直播
                        ),
                    );
                    $result = $this->category_action('add', $do_category);
                    if($result['ret'] != NS_CDS_SUCCE)
                    {
                        \m_config::write_message_execute_log("消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因：" . $result['reason'],$this->source_id);
                        $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL,'工单Series栏目创建失败');
                        return $result;
                    }
                    if(!isset($result['data_info']['base_info']['nns_id']) || strlen($result['data_info']['base_info']['nns_id']) < 1)
                    {
                        \m_config::write_message_execute_log("消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因创建栏目失败：" . $result['reason'],$this->source_id);
                        $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL,'工单Series栏目创建失败');
                        return \m_config::return_data(NS_CDS_FAIL,'创建栏目失败');
                    }
                    $nns_category_id = $result['data_info']['base_info']['nns_id'];

                    if($view_type == 0 && isset($val_series['key_val_list']['VolumnCount']) && $val_series['key_val_list']['VolumnCount'] > 1)
                    {
                        $view_type = 1;
                    }
                    #TODO 获取图片
                    $arr_img = $this->handle_picture_v3($val_series['ContentID']);
                    if (!empty($arr_img) && is_array($arr_img))
                    {
                        $val_series['key_val_list'] = array_merge($val_series['key_val_list'], $arr_img);
                    }
                    $pinyin = \m_pinyin::get_pinyin_letter($val_series['key_val_list']['Name']);
                    if((!isset($val_series['key_val_list']['ReleaseYear']) || empty($val_series['key_val_list']['ReleaseYear'])) && !empty($val_series['key_val_list']['OrgAirDate']))
                    {
                        $val_series['key_val_list']['ReleaseYear'] = $val_series['key_val_list']['OrgAirDate'];
                    }
                    //获取年份
                    $release_year_day = date('Y-m-d',time());
                    $release_year = date('Y',time());

                    if((isset($val_series['key_val_list']['ReleaseYear']) && strlen($val_series['key_val_list']['ReleaseYear']) > 0))
                    {
                        if(strpos($val_series['key_val_list']['ReleaseYear'],"-") !== false)//存在方式为YYYY-MM-DD
                        {
                            $release_year_day = $val_series['key_val_list']['ReleaseYear'];
                            $release_year = date("Y", strtotime($val_series['key_val_list']['ReleaseYear']));
                        }
                        else
                        {
                            $release_year_day = $val_series['key_val_list']['ReleaseYear'].'-1-1';
                            $release_year = $val_series['key_val_list']['ReleaseYear'];
                        }
                    }

                    $add_series = array(
                        'base_info' => array(
                            'nns_name' => isset($val_series['key_val_list']['Name']) ? $val_series['key_val_list']['Name'] : '',
                            'nns_view_type' => $view_type,
                            'nns_org_type' => '0',
                            'nns_tag' => '26,',
                            'nns_director' => isset($val_series['key_val_list']['Director']) ? $val_series['key_val_list']['Director'] : '',
                            'nns_actor' => isset($val_series['key_val_list']['Kpeople']) ? $val_series['key_val_list']['Kpeople'] : '',
                            'nns_show_time' =>  $release_year_day,
                            'nns_view_len' => isset($val_series['key_val_list']['Duration']) ? $val_series['key_val_list']['Duration'] : 0,
                            'nns_all_index' => isset($val_series['key_val_list']['VolumnCount']) ? $val_series['key_val_list']['VolumnCount'] : 1,
                            'nns_new_index' => isset($val_series['key_val_list']['NewCount']) ? $val_series['key_val_list']['NewCount'] : 0,
                            'nns_area' => isset($val_series['key_val_list']['OriginalCountry']) ? $val_series['key_val_list']['OriginalCountry'] : '',
                            'nns_image0' => isset($val_series['key_val_list']['bigpic']) ? $val_series['key_val_list']['bigpic'] : '',
                            'nns_image1' => isset($val_series['key_val_list']['middlepic']) ? $val_series['key_val_list']['middlepic'] : '',
                            'nns_image2' => isset($val_series['key_val_list']['smallpic']) ? $val_series['key_val_list']['smallpic'] : '',
                            'nns_image3' => '',
                            'nns_image4' => '',
                            'nns_image5' => '',
                            'nns_summary' => isset($val_series['key_val_list']['Description']) ? $val_series['key_val_list']['Description'] : '',
                            'nns_remark' => isset($val_series['key_val_list']['Kind']) ? $val_series['key_val_list']['Kind'] : '',
                            'nns_category_id' => $nns_category_id,
                            'nns_play_count' => '0',
                            'nns_score_total' => '0',
                            'nns_score_count' => '0',
                            'nns_point' => isset($val_series['key_val_list']['ViewPoint']) ? $val_series['key_val_list']['ViewPoint'] : '0',
                            'nns_copyright_date' => $release_year_day,
                            'nns_asset_import_id' => $val_series['ContentID'],
                            'nns_pinyin' => (isset($val_series['key_val_list']['Spell']) && strlen($val_series['key_val_list']['Spell']) > 0) ? $val_series['key_val_list']['Spell'] : $pinyin,
                            'nns_pinyin_length' => (isset($val_series['key_val_list']['Spell']) && strlen($val_series['key_val_list']['Spell']) > 0) ? strlen($val_series['key_val_list']['Spell']) : strlen($pinyin),
                            'nns_alias_name' => isset($val_series['key_val_list']['AliasName']) ? $val_series['key_val_list']['AliasName'] : '',
                            'nns_eng_name' => isset($val_series['key_val_list']['EnglishName']) ? $val_series['key_val_list']['EnglishName'] : '',
                            'nns_language' => isset($val_series['key_val_list']['Language']) ? $val_series['key_val_list']['Language'] : '',
                            'nns_text_lang' => isset($val_series['key_val_list']['Language']) ? $val_series['key_val_list']['Language'] : '',
                            'nns_producer' => isset($val_series['key_val_list']['CPID']) ? $val_series['key_val_list']['CPID'] : $message['nns_cp_id'],
                            'nns_screenwriter' => isset($val_series['key_val_list']['WriterDisplay']) ? $val_series['key_val_list']['WriterDisplay'] : '',
                            'nns_play_role' => '',
                            'nns_copyright_begin_date' => !empty($val_series['key_val_list']['LicensingWindowStart']) ? date('Y-m-d H:i:s',strtotime($val_series['key_val_list']['LicensingWindowStart'])) : '',
                            'nns_copyright_range' => !empty($val_series['key_val_list']['LicensingWindowEnd']) ? date('Y-m-d H:i:s',strtotime($val_series['key_val_list']['LicensingWindowEnd'])) : '',
                            'nns_vod_part' => '',
                            'nns_keyword' => isset($val_series['key_val_list']['Keywords']) ? $val_series['key_val_list']['Keywords'] : '',
                            'nns_import_source' => isset($val_series['nns_import_source']) ? $val_series['nns_import_source'] : evn::get("project"),
                            'nns_kind' => isset($val_series['key_val_list']['Kind']) ? $val_series['key_val_list']['Kind'] : '',
                            'nns_copyright' => isset($val_series['key_val_list']['CopyRight']) ? $val_series['key_val_list']['CopyRight'] : '',
                            'nns_clarity' => '',
                            'nns_image_v' => isset($val_series['key_val_list']['verticality_img']) ? $val_series['key_val_list']['verticality_img'] : '',
                            'nns_image_s' => isset($val_series['key_val_list']['square_img']) ? $val_series['key_val_list']['square_img'] : '',
                            'nns_image_h' => isset($val_series['key_val_list']['horizontal_img']) ? $val_series['key_val_list']['horizontal_img'] : '',
                            'nns_cp_id' => $message['nns_cp_id'],
                            'nns_conf_info' => '',
                            'nns_ext_url' => '',
                        ), //基本信息（存储于nns_vod表中）
                        'ex_info' => array(
                            'svc_item_id' => '',
                            'month_clicks' => '',
                            'week_clicks' => '',
                            'base_id' => '',
                            'asset_path' => '',
                            'ex_tag' => '',
                            'full_spell' => '',
                            'awards' => '',
                            'year' => $release_year,
                            'play_time' => '',
                            'channel' => '',
                            'first_spell' => '',
                            'online_identify' => $val_series['key_val_list']['OnlineIdentify'],
                        ), //扩展信息（存储于nns_vod_ex表中）
                    );
                    $result = $this->vod_action('add', $add_series);
                }

                if ($result['ret'] != NS_CDS_SUCCE)
                {
                    \m_config::write_message_execute_log("消息接收成功,消息ID[{$message['nns_message_id']}]队列生成结果：" . $result['reason'],$this->source_id);
                    $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL,'工单Series处理失败');
                    return $result;
                }
            }
        }

        //分集操作
        if (!empty($arr_program))
        {
            foreach ($arr_program as $val_program)
            {
                //分集删除
                if (strtoupper($val_program['Action']) == 'DELETE')
                {
                    $del_program = array(
                        'base_info' => array(
                            'nns_import_id' => $val_program['ContentID'],
                            'nns_import_source' => isset($val_program['nns_import_source']) ? $val_program['nns_import_source'] : evn::get("project"),
                            'nns_cp_id' => $message['nns_cp_id'],
                        ),
                    );
                    $result = $this->index_action('delete', $del_program);
                    if ($result['ret'] != NS_CDS_SUCCE)
                    {
                        \m_config::write_message_execute_log("消息接收成功,消息ID[{$message['nns_message_id']}]队列生成结果：" . $result['reason'],$this->source_id);
                        $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL,'工单program删除处理失败');
                        return $result;
                    }
                }
                else
                {
                    $arr_p_series = $this->get_map_info_v2('Program', $this->arr_map, $val_program['ContentID']);
                    $p_series = array_keys($arr_p_series);

                    $res = nl_asset_incidence_relation::query_asset_relation(\m_config::get_dc(), 'video', $p_series[0], $message['nns_cp_id'], 0);

                    if (!is_array($res['data_info']))
                    {
                        \m_config::write_message_execute_log("消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因主媒资未注入" . $res['reason'],$this->source_id);
                        $result['ret'] = 6;
                        $result['reason'] = "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因主媒资未注入" . $res['reason'];
                        \m_config::write_message_execute_log("消息接收成功,消息ID[{$message['nns_message_id']}]队列生成结果：" . $result['reason'],$this->source_id);
                        $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL,'当前Program的Series工单未注入');
                        return $result;
                    }
                    $arr_img = $this->handle_picture_v3($val_program['ContentID']);
                    if (!empty($arr_img) && is_array($arr_img))
                    {
                        $val_program['key_val_list'] = array_merge($val_program['key_val_list'], $arr_img);
                    }
                    //获取年份
                    $release_year_day = date('Y-m-d',time());
                    $release_year = date('Y',time());

                    if((isset($val_program['key_val_list']['ReleaseYear']) && strlen($val_program['key_val_list']['ReleaseYear']) > 0))
                    {
                        if(strpos($val_program['key_val_list']['ReleaseYear'],"-") !== false)//存在方式为YYYY-MM-DD
                        {
                            $release_year_day = $val_program['key_val_list']['ReleaseYear'];
                            $release_year = date("Y", strtotime($val_program['key_val_list']['ReleaseYear']));
                        }
                        else
                        {
                            $release_year_day = $val_program['key_val_list']['ReleaseYear'].'-1-1';
                            $release_year = $val_program['key_val_list']['ReleaseYear'];
                        }
                    }
                    $add_index = array(
                        'base_info' => array(
                            'nns_name' => isset($val_program['key_val_list']['Name']) ? $val_program['key_val_list']['Name'] : '',
                            'nns_index' => $arr_p_series[$p_series[0]]['key_val_list']['Sequence'],
                            'nns_time_len' => '',
                            'nns_summary' => isset($val_program['key_val_list']['Description']) ? $val_program['key_val_list']['Description'] : '',
                            'nns_image' => isset($val_program['key_val_list']['Picture']) ? $val_program['key_val_list']['Picture'] : '',
                            'nns_play_count' => 0,
                            'nns_score_total' => 0,
                            'nns_score_count' => 0,
                            'nns_video_import_id' => $p_series[0],
                            'nns_import_id' => $val_program['ContentID'],
                            'nns_import_source' => isset($val_program['nns_import_source']) ? $val_program['nns_import_source'] : evn::get("project"),
                            'nns_director' => isset($val_program['key_val_list']['Director']) ? $val_program['key_val_list']['Director'] : '',
                            'nns_actor' => isset($val_program['key_val_list']['Kpeople']) ? $val_program['key_val_list']['Kpeople'] : '',
                            'nns_release_time' => $release_year_day,
                            'nns_update_time' => date("Y-m-d"),
                            'nns_watch_focus' => isset($val_program['key_val_list']['WatchFocus']) ? $val_program['key_val_list']['WatchFocus'] : '',
                            'nns_cp_id' => $message['nns_cp_id'],
                            'nns_conf_info' => '',
                            'nns_ext_url' => '',
                        ),
                        'ex_info' => array(
                            'isintact' => '',
                            'subordinate_name' => '',
                            'initials' => '',
                            'publisher' => '',
                            'first_spell' => '',
                            'caption_language' => '',
                            'language' => isset($val_program['key_val_list']['Language']) ? $val_program['key_val_list']['Language'] : '',
                            'region' => '',
                            'adaptor' => '',
                            'sreach_key' => '',
                            'event_tag' => '',
                            'year' => $release_year,
                            'sort_name' => '',
                        ),
                    );

                    //分集注入
                    $result = $this->index_action('add', $add_index);

                    if ($result['ret'] != NS_CDS_SUCCE)
                    {
                        \m_config::write_message_execute_log("消息接收成功,消息ID[{$message['nns_message_id']}]队列生成结果：" . $result['reason'],$this->source_id);
                        $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL,'工单Program任务处理失败');
                        return $result;
                    }
                }
            }
        }


        //片源操作
        if (!empty($arr_movie))
        {
            foreach ($arr_movie as $val_movie)
            {
                //片源删除
                if (strtoupper($val_movie['Action']) == 'DELETE')
                {
                    $del_movie = array(
                        'base_info' => array(
                            'nns_import_id' => $val_movie['ContentID'],
                            'nns_import_source' => isset($val_movie['nns_import_source']) ? $val_movie['nns_import_source'] : evn::get("project"),
                            'nns_cp_id' => $message['nns_cp_id'],
                        ),
                    );
                    $result = $this->media_action('delete', $del_movie);
                    if ($result['ret'] != NS_CDS_SUCCE)
                    {
                        $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL,'工单Movie删除任务处理失败');
                        \m_config::write_message_execute_log("消息接收成功,消息ID[{$message['nns_message_id']}]队列生成结果：" . $result['reason'],$this->source_id);
                        return $result;
                    }
                }
                else
                {
                    $arr_p_program = $this->get_map_info_v2('Movie', $this->arr_map, $val_movie['ContentID']);
                    $p_program = array_keys($arr_p_program);
                    //验证分集
                    $index_info = nl_asset_incidence_relation::query_asset_relation(\m_config::get_dc(), 'index', $p_program[0], $message['nns_cp_id'], 0);

                    if (!is_array($index_info['data_info']))
                    {
                        \m_config::write_message_execute_log("消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因分集未注入" . $index_info['reason'],$this->source_id);
                        //返回分集未注入
                        $result['ret'] = 7;
                        $result['reason'] = "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因分集未注入".$index_info['reason'];
                        \m_config::write_message_execute_log("消息接收成功,消息ID[{$message['nns_message_id']}]队列生成结果：" . $result['reason'],$this->source_id);
                        $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL,'工单Movie的Program工单未注入');
                        return $result;
                    }

                    $video_info = nl_asset_incidence_relation::query_asset_relation(\m_config::get_dc(), 'index', $p_program[0], $message['nns_cp_id'], 1);
                    if (!is_array($video_info['data_info']))
                    {
                        \m_config::write_message_execute_log("消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因主媒资未注入" . $video_info['reason'],$this->source_id);
                        //返回主媒资未注入
                        $result['ret'] = 6;
                        $result['reason'] = "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因主媒资未注入".$video_info['reason'];
                        \m_config::write_message_execute_log("消息接收成功,消息ID[{$message['nns_message_id']}]队列生成结果：" . $result['reason'],$this->source_id);
                        $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL,'工单Movie的Series工单未注入');
                        return $result;
                    }

                    $index_import_id = $p_program[0];
                    $vod_import_id = $video_info['data_info'][0]['nns_asset_import_id'];
                    //拆分媒体格式
                    $media_info = explode('-', $val_movie['key_val_list']['MediaSpec']);
                    //片源参数
                    $add_media = array(
                        'base_info' => array(
                            'nns_name' => isset($val_movie['key_val_list']['Name']) ? $val_movie['key_val_list']['Name'] : '',
                            'nns_type' => 1,
                            'nns_url' => isset($val_movie['key_val_list']['FileURL']) ? $val_movie['key_val_list']['FileURL'] : '',
                            'nns_tag' => '26,',
                            'nns_mode' => isset($this->media_mode[$val_movie['key_val_list']['AssetTag']]) ? $this->media_mode[$val_movie['key_val_list']['AssetTag']] : 'hd',
                            'nns_kbps' => $media_info[3],
                            'nns_content_id ' => '',
                            'nns_content_state' => 0,
                            'nns_filetype' => strtolower($media_info[0]),
                            'nns_play_count' => '0',
                            'nns_score_total' => '0',
                            'nns_score_count' => '0',
                            'nns_video_import_id' => $vod_import_id,
                            'nns_index_import_id' => $index_import_id,
                            'nns_import_id' => $val_movie['ContentID'],
                            'nns_import_source' => evn::get("project"),
                            'nns_dimensions' => isset($val_movie['key_val_list']['Dimensions']) ? $val_movie['key_val_list']['Dimensions'] : '2D',
                            'nns_ext_url' => '',
                            'nns_file_size' => isset($val_movie['key_val_list']['FileSize']) ? $val_movie['key_val_list']['FileSize'] : 1,
                            'nns_file_time_len' => isset($val_movie['key_val_list']['Duration']) ? $val_movie['key_val_list']['Duration'] : 0,
                            'nns_file_frame_rate' => isset($val_movie['key_val_list']['FrameRate']) ? $val_movie['key_val_list']['FrameRate'] : 0,
                            'nns_file_resolution' => isset($val_movie['key_val_list']['Resolution']) ? $val_movie['key_val_list']['Resolution'] : 0,
                            'nns_cp_id' => $message['nns_cp_id'],
                            'nns_ext_info' => '',
                            'nns_drm_enabled' => isset($val_movie['key_val_list']['DrmEnabled']) ? $val_movie['key_val_list']['DrmEnabled'] : 0,
                            'nns_drm_encrypt_solution' => isset($val_movie['key_val_list']['DrmEncryptSolution']) ? $val_movie['key_val_list']['DrmEncryptSolution'] : '',
                            'nns_drm_ext_info' => '',
                            'nns_domain' => isset($val_movie['key_val_list']['Domain']) ? $val_movie['key_val_list']['Domain'] : 0,
                            'nns_media_type' => isset($val_movie['key_val_list']['VideoType']) ? $val_movie['key_val_list']['VideoType'] : 1,
                            'nns_original_live_id' => '',
                            'nns_start_time' => '',
                            'nns_media_service' => isset($val_movie['key_val_list']['ServiceType']) ? $val_movie['key_val_list']['ServiceType'] : 'HTTP',
                            'nns_conf_info' => '',
                            'nns_encode_flag' => isset($val_movie['key_val_list']['EncodeFlag']) ? $val_movie['key_val_list']['EncodeFlag'] : 0,
                            'nns_live_to_media' => isset($val_movie['key_val_list']['EncodeType']) ? $val_movie['key_val_list']['EncodeType'] : '',
                            'nns_media_service_type' => '',
                        ),
                        'ex_info' => array(
                            'file_hash' => '',
                            'file_width' => '',
                            'file_height' => '',
                            'file_scale' => '',
                            'file_coding' => '',
                        ),
                    );

                    $result = $this->media_action('add', $add_media);
                    if ($result['ret'] != NS_CDS_SUCCE)
                    {
                        \m_config::write_message_execute_log("消息接收成功,消息ID[{$message['nns_message_id']}]队列生成结果：" . $result['reason'],$this->source_id);
                        $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL,'工单Movie任务处理失败');
                        return $result;
                    }
                }
            }
        }

        \m_config::write_message_execute_log("消息接收成功,消息ID[{$message['nns_message_id']}]队列生成结果：" . $result['reason'],$this->source_id);
        return $result;

    }

    private function handle_picture_v3($asset_id)
    {
        $array_img = array();
        $array_img['bigpic']='';
        $array_img['middlepic']='';
        $array_img['smallpic']='';
        $array_img['verticality_img']='';
        $array_img['horizontal_img']='';
        $array_img['square_img']='';
        if(empty($this->arr_picture) || !is_array($this->arr_picture))
        {
            return $array_img;
        }
        $array_map = $this->get_map_info_v2('Picture',$this->arr_map,$asset_id);
        if(empty($array_map))
        {
            return $array_img;
        }
        $array_map_key = array_keys($array_map);
        $temp_array = array();
        foreach ($this->arr_picture as $obj_picture)
        {
            if(!in_array($obj_picture['ContentID'], $array_map_key))
            {
                continue;
            }
            //组成数据,key为图片type,值为url
            if (!isset($obj_picture['key_val_list']['Type']))
            {
                foreach ($array_map as $k_id => $val)
                {
                    if ($obj_picture['ContentID'] == $k_id)
                    {
                        $obj_picture['key_val_list']['Type'] = $val['key_val_list']['Type'];
                    }
                }
            }
            $temp_array[$obj_picture['key_val_list']['Type']] = (isset($obj_picture['key_val_list']['FileURL']) && strlen($obj_picture['key_val_list']['FileURL']) >0) ? trim($obj_picture['key_val_list']['FileURL']) : '';
        }
        $temp_array = array_filter($temp_array);
        if(empty($temp_array))
        {
            return $array_img;
        }
        $arr_combine = $this->combine_picture($array_img, $temp_array);
        return array_combine(array_keys($array_img),$arr_combine);
    }

    private function get_map_info_v2($p_type,$arr_map, $e_id)
    {
        $temp_arr = array();
        if ($p_type == 'Picture')
        {
            foreach ($arr_map as $map)
            {
                if ($map['attributes']['ElementType'] == $p_type && $map['attributes']['ParentID'] == $e_id)
                {
                    $filter_map = $this->make_key_value_arr($map);
                    $temp_arr[$map['attributes']['ElementID']] = $filter_map;
                }
                if (isset($temp_arr[$map['attributes']['ElementID']]))
                {
                    continue;
                }
                if ($map['attributes']['ParentType'] == $p_type && $map['attributes']['ElementID'] == $e_id)
                {
                    $filter_map = $this->make_key_value_arr($map);
                    $temp_arr[$map['attributes']['ParentID']] = $filter_map;
                }
            }

        }
        else
        {
            foreach ($arr_map as $map)
            {
                if ($map['attributes']['ElementType'] == $p_type && $map['attributes']['ParentType'] != 'Picture' && $map['attributes']['ElementID'] == $e_id)
                {
                    $filter_map = $this->make_key_value_arr($map);
                    $temp_arr[$map['attributes']['ParentID']] = $filter_map;
                }
            }
        }
        return $temp_arr;
    }

    /**
     * 获取xml的 attr属性和key value值
     * @param unknown $xml_obj_arr
     * @return Ambigous <NULL, unknown, string>
     */
    private function make_key_value_arr($xml_obj_arr)
    {
        $key_val_array = null;
        if (isset($xml_obj_arr['attributes']) && is_array($xml_obj_arr['attributes']) && !empty($xml_obj_arr['attributes']))
        {
            foreach ($xml_obj_arr['attributes'] as $attr_key => $attr_val)
            {
                $key_val_array[$attr_key] = $attr_val;
            }
            if(!isset($key_val_array['ContentID']) || strlen($key_val_array['ContentID']) < 1)
            {
                $key_val_array['ContentID'] = isset($xml_obj_arr['attributes']['ID']) ? $xml_obj_arr['attributes']['ID'] : '';
            }
            unset($xml_obj_arr['attributes']);
        }
        if (isset($xml_obj_arr['children']) && is_array($xml_obj_arr['children']) && !empty($xml_obj_arr['children']))
        {
            foreach ($xml_obj_arr['children'] as $key_list)
            {
                if (isset($key_list['attributes']['Name']) && strlen($key_list['attributes']['Name']) > 0)
                {
                    $key_val_array['key_val_list'][$key_list['attributes']['Name']] = (isset($key_list['content']) && strlen($key_list['content']) > 0) ? trim($key_list['content']) : '';
                }
            }
            unset($xml_obj_arr['children']);
        }
        return $key_val_array;
    }
    /**
     * 合并图片类型
     * @param $arr_img
     * @param $arr_temp
     * @return array
     */
    private function combine_picture($arr_img, $arr_temp)
    {
        $arr_img = array_values($arr_img);
        foreach ($arr_temp as $k=> $item)
        {
            $arr_img[$k] = $item;
        }
        return $arr_img;
    }
    /**
     * @description:播控注入cdn后统一反馈给二级
     * @author:xinxin.deng
     * @date: 2018/3/2 9:59
     * @param $message_id //消息id
     * @param $code //状态码，失败1，成功0
     * @param $reason //原因描述
     * @param $arr_data //封装好了的反馈信息 array(
     *                                          'cdn_id' => ,注入cdn的id
     *                                          'site_id' => ,配置的站点id
     *                                          'mg_asset_type' => ,注入媒资类型
     *                                          'mg_asset_id' => ,主媒资的消息注入id
     *                                          'mg_part_id' => ,分集的消息注入id
     *                                          'mg_file_id' => ,片源的消息注入id,
     *                                          'is_finally' =>0, 0表示最终状态,反馈的时候将其unset掉
     *                                          );
     * @param $sp_id //sp配置信息
     */
    public function is_ok($message_id, $code, $reason, $arr_data, $sp_id)
    {
        $this->source_id = DEFAULT_SOURCE_ID;
        //获取cp配置
        $arr_cp_data = \m_config::_get_cp_info(DEFAULT_SOURCE_ID);
        $this->arr_cp_config = isset($arr_cp_data['data_info']['nns_config']) ? $arr_cp_data['data_info']['nns_config'] : array();

        $info = array(
            'msgid' => $message_id,
            'state' => $code,//0标识成功，其他标识失败。
            'msg' => $reason,
            'is_finally' => (isset($arr_data['is_finally']) && $arr_data['is_finally']) == 0 ? 1 : 0,
            'info' => array(
                'asset_type' => $arr_data['mg_asset_type'],
                'series_id' => $arr_data['mg_asset_id'],
                'program_id' => $arr_data['mg_part_id'],
                'movie_id' => $arr_data['mg_file_id'],
            ),
        );

        $this->notify_msg($info,true,$this->arr_cp_config['site_callback_url']);

        return;
    }


    /**
     * @description:组装反馈的XML
     * @author:xinxin.deng
     * @date: 2018/2/26 14:25
     * @param $params
     * @return string|void
     */
    public function _build_notify_xml($params)
    {
        if(!is_array($params) || empty($params))
        {
            return ;
        }
        $dom = new DOMDocument('1.0', 'utf-8');
        $xmlresult = $dom->createElement('xmlresult');
        $dom->appendChild($xmlresult);
        foreach ($params as $key=>$value)
        {
            $$key = $dom->createElement($key);
            $xmlresult->appendchild($$key);
            if(!empty($value) && is_array($value))
            {
                foreach ($value as $k=>$val)
                {
                    $$k = $dom->createElement($k);
                    $$key->appendchild($$k);
                    //创建元素值
                    $content = $dom->createTextNode($val);
                    $$k->appendchild($content);
                }
            }
            else
            {
                //创建元素值
                $text = $dom->createTextNode($value);
                $$key->appendchild($text);
            }
        }
        return $dom->saveXML();
    }


    /**
     * 消息队列注入播控统一反馈给芒果二级，组装消息结构
     * @param int $state
     * @param string $reason
     * @param array $info
     */
    private function create_xml_respond_msg_to_csp($state, $reason='')
    {
        $info = array(
            'msgid' => $this->msg_id,
            'state' => $state,//0标识成功，其他标识失败。
            'msg' => $reason,
            'is_finally' => 0,
        );

        $this->notify_msg($info,true,$this->arr_cp_config['site_callback_url']);
    }

    /**
     * 进行消息反馈
     * @param $params
     * @param bool $bool
     * @param string $site_url
     * @return array|string|void
     */
    private function notify_msg($params, $bool = false, $site_url = '')
    {
        $xml = $this->_build_notify_xml($params);
        \m_config::write_callback_log('反馈给上游的数据为：' . var_export($xml,true), $this->source_id, $this->msg_id);

        if(!$bool)//是否异步反馈通知上游
        {
            return $xml;
        }
        if(strlen($site_url) > 0)
        {
            $data = array('cmsresult'=> $xml);
            \m_config::write_callback_log('反馈给上游的最终数据为：' . var_export($data,true), $this->source_id, $this->msg_id);

            $http_header = array("Content-Type: application/x-www-form-urlencoded;charset=UTF-8");

            for ($i = 0; $i < 3; $i++)
            {
                $http_curl = new np_http_curl_class();
                $re = $http_curl->post($this->arr_cp_config['site_callback_url'], $data, $http_header, 2);
                $curl_info = $http_curl->curl_getinfo();
                $http_code = $curl_info['http_code'];
                \m_config::write_callback_log('[' . date('Y-m-d H:i:s') . ']' . '循环请求第' . $i . '次(如果请求返回post结果为true并且状态码不是200,就会默认循环3次,状态码为200,
                就跳出循环),' . '请求返回post结果为:' . var_export($re,true) . '请求返回状态为:' . var_export($http_code, true) . ',HTTP的CURLINFO数据为:' . var_export($curl_info, true)
                    , $this->source_id, $this->msg_id);

                if ($http_code != 200)
                {
                    continue;
                }
                else
                {
                    break;
                }
            }
        }
        else
        {
            \m_config::write_callback_log('没有配置反馈地址', $this->source_id, $this->msg_id);
        }
    }
}