<?php
//error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_model.delivery.cdn_delivery_explain");
\ns_core\m_load::load_np("np_http_curl.class.php");
class delivery extends \ns_model\delivery\cdn_delivery_explain
{

    private $c2_task_info = null;

    /**
     * CDN注入统一解释器入口
     * @param $info array(
     *                      'c2_info'=>array(C2信息),
     *                      'video_info'=>array(
     *                          'base_info' => VIDEO信息
     *                          'ex_info'  => 扩展信息
     *                       ),
     *                      'index_info'=>array(
     *                          'base_info' => INDEX信息,
     *                          'ex_info' => 扩展信息,
     *                      ),
     *                      'media_info'=>array(
     *                          'base_info' => MEDIA信息
     *                          'ex_info' => 扩展信息
     *                      ),
     *                  )
     */
    public function explain($info = array())
    {
        if(isset($info['c2_info']) && is_array($info['c2_info']) && !empty($info['c2_info']))
        {
            //return \m_config::return_data("1","C2任务数据错误，执行注入CDN失败");
            $this->c2_task_info = isset($info['c2_info']) ? $info['c2_info'] : $this->c2_info;
            $this->video_info = isset($info['video_info']) ? $info['video_info'] : $this->video_info;
            $this->index_info = isset($info['index_info']) ? $info['index_info'] : $this->index_info;
            $this->media_info = isset($info['media_info']) ? $info['media_info'] : $this->media_info;
        }
        else
        {
            $this->c2_task_info = $this->c2_info;
        }
        $fun_name = (string)$this->c2_task_info['nns_type'];
        return $this->$fun_name();
    }

	private function video()
    {
        return m_config::return_data(1,'不注入主媒资信息');
	}
	
    private function index()
	{
	    return m_config::return_data(1,'不注入分集信息');
	}

    private function media()
	{
        include_once dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))) . '/mgtv_v2/mgtv_init.php';
        if ($this->c2_task_info['nns_action'] === 'destroy')
	    {
	        return $this->video_destroy( 'DELETE');
	    }
        $this->c2_task_info['nns_action'] = isset($this->c2_task_info['nns_action']) && strlen($this->c2_task_info['nns_action']) > 0 ? $this->c2_task_info['nns_action'] : 'add';
        $query = array('c2_task_id' => $this->c2_task_info['nns_id'], 'action' => $this->c2_task_info['nns_action'], 'status' => SP_C2_WAIT, );
        $re = content_task_model::resend_c2_task($query);

	}
}