<?php
/**
 * Use : 导出注入CDN的片源媒资列表，输出CSV格式文件
 * User: kan.yang@starcor.cn
 * Date: 18-3-8
 * Time: 下午3:51
 */

set_time_limit(0);error_reporting(0);
//header('Content-Type:text/xml; charset=utf-8');
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_core.m_config");
class export_cdn_media
{
    //数据库/缓存处理类
    private $obj_dc = null;
    //SP INFO
    private $arr_sp_info = null;
    //SP ID
    private $str_sp_id = 'bflt_xiaole';
    //分页条数
    private $int_page_size = 0;
    //导出文件保存路径
    private $str_csv_path  = '';
    //查询字段
    private $str_filed = 'log.nns_id nns_log_id, task.nns_name nns_task_name,task.nns_org_id,
                          log.nns_create_time nns_log_create_time,log.nns_url nns_log_url, m.*';
    //CSV文件标题
    private $arr_csv_title = array(
        '内容名称(第一语言)' => '', // 必传，【nns_mgtvbk_c2_log】的 nns_id
        '内容名称(第二语言)' => '', // 选填
        '生效时间' => '20151218172300',// 必传
        '失效时间' => '20991231235959',// 必传
        '搜索名' => '',// 必传，【nns_mgtvbk_c2_task】的 nns_name
        '授权区域' => '所有区域',// 必传
        'Package标识' => '',// 必传
        '价格' => '',// 必传
        '推荐级别' => '热度推荐',
        '控制级别' => '适合所有年龄段（G）',
        '领域' => 'IPTV',
        '简介(第一语言)' => '',
        '简介(第二语言)' => '',
        '地区(第一语言)' => '',
        '地区(第二语言)' => '',
        '语言' => '',
        '分类(第一语言)' => '',
        '风格(第一语言)' => '',
        '关键字' => '',
        '标签(第一语言)' => '',
        '预览支持' => '不支持',
        '预览开始时间' => '',
        '预览时长' => '',
        '出品日期' => '',
        '版权到期日期' => '',
        '版权号' => '',
        '演员' => '',
        '导演' => '',
        '编剧' => '',
        '海报类型' => '',
        '海报所属领域' => '',
        '海报顺序号' => '',
        '海报路径' => '',
        '用户组信息' => '',
        '媒体名称' => '', // 【nns_mgtvbk_c2_task】的 nns_name
        '发布时间' => '', // 【nns_vod_media】的 nns_create_time
        '发布区域' => '所有区域',
        '删除级别' => '1',
        'FTP标识' => 'ftp://202.99.114.109',
        '内容源URL' => '',
        '所属领域' => 'IPTV',
        '清晰度' => '高清',
        '分辨率' => '',
        '视频ProFile' => '',
        '音频类型' => '',
        '视频编码格式' => 'H.264',
        '音频编码格式' => '',
        '格式' => '',
        '码率' => '2.3M(高清VBR)',
        '文件大小' => '',
        '播放时长' => '',
        '是否支持加密' => '不支持',
        '关联类型' => '正片',
        '是否支持下载' => '不支持',
        '节目内容类型' => '',
        '首播日期' => '20151218',
        '点播类型' => '普通点播',
        '产品类型' => '应用视频',
        '上映年份' => '2017',
    );

    /**
     * 默认构造函数
     */
        public function __construct()
    {
        //数据库操作类
        $this->obj_dc = m_config::get_dc();
        //SP ID
        $this->str_sp_id = isset($_REQUEST['nns_sp_id']) && $_REQUEST['nns_sp_id'] > 0 ? $_REQUEST['nns_sp_id'] : $this->str_sp_id;
        //分页条数
        $this->int_page_size = isset($_REQUEST['nns_page_size']) && $_REQUEST['nns_page_size'] > 0 ? $_REQUEST['nns_page_size'] : 1000;
        //导出文件保存路径
        $this->str_csv_path  = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))) . '/data/media_cdn_flow/export/';
    }

        public function init($arr_media_ids = array())
    {
        //查询待导出的片源内容的SQL的公共部分
        $str_comm_sql = " FROM
              nns_mgtvbk_c2_task AS task
              INNER JOIN nns_vod_media m
                ON m.nns_id = task.`nns_ref_id`
              LEFT JOIN nns_mgtvbk_c2_log AS `log`
                ON task.nns_id = log.nns_task_id
            WHERE task.nns_status IN ('6')
              AND task.nns_type = 'media' and log.nns_notify_result=0 and log.nns_notify_fail_reason like 'execute move%' 
              AND task.nns_org_id = '{$this->str_sp_id}'";
        if(!empty($arr_media_ids))
        {
            $str_comm_sql .= " AND m.nns_id IN ('" . implode("','",$arr_media_ids) . "') ";
        }
        //查询待导出片源的总数
        $str_sql = "SELECT COUNT(1) total " . $str_comm_sql;
        $arr_count = nl_db_get_one($str_sql,$this->obj_dc->db());
        if(!is_array($arr_count) || empty($arr_count['total']))
        {
            return $this->_callback_info(1,'未查询到任何待导出的片源信息');
        }
        //创建保存路径
        $arr_create_dir = $this->_create_save_path();
        if($arr_create_dir['ret'] != 0)
        {
            return $arr_create_dir;
        }
        //查询SP INFO
        $str_sql = "SELECT * FROM nns_mgtvbk_sp where nns_id = '" . $this->str_sp_id . "'";
        $this->arr_sp_info = nl_db_get_one($str_sql,$this->obj_dc->db());
        if(!is_array($this->arr_sp_info) || empty($this->arr_sp_info))
        {
            return $this->_callback_info(1,'未查询到任何SP信息');
        }
        $this->arr_sp_info['nns_config'] = json_decode($this->arr_sp_info['nns_config'],true);
        //分页
        $int_count = $arr_count['total'];
        $int_page_total = (int)(($int_count + $this->int_page_size - 1) / $this->int_page_size);
        //遍历导出
        $int_page = 0; $int_page_size_start = 0;
        while($int_page < $int_page_total)
        {
            $int_page_size_start = $int_page * $this->int_page_size;
            //重新创建
            $obj_file = fopen($this->str_csv_path . 'Batchlmport_' . date('md') . '_' . ($int_page + 1) . '.csv', 'w');
            fwrite($obj_file, chr(0xEF).chr(0xBB).chr(0xBF));
            fputcsv($obj_file, array_keys($this->arr_csv_title));
            //查询媒资数据
            $str_sql = "SELECT $this->str_filed $str_comm_sql ORDER BY task.nns_create_time ASC LIMIT $int_page_size_start,$this->int_page_size";
            $arr_media_list =nl_query_by_db($str_sql,$this->obj_dc->db());
            if(is_array($arr_media_list) && !empty($arr_media_list))
            {
                $this->_export_media_data($obj_file,$arr_media_list);
            }
            //关闭文件流，执行下次循环
            fclose($obj_file); $int_page ++; usleep(50000);
        }
        return $this->_callback_info(0,'导出片源数据完成，导出目录：' . $this->str_csv_path);
    }

    /**
     * 导出片源媒资元数据
     * @param $obj_file
     * @param $arr_media_list
     */
    private function _export_media_data($obj_file, $arr_media_list)
    {
        foreach($arr_media_list as $media)
        {
            $media['nns_name'] = $this->_char_handle($media['nns_name']);
            $media['nns_task_name'] = $this->_char_handle($media['nns_task_name']);
            $str_write_content = array(
                $media['nns_log_id'],         //内容名称(第一语言)
                '',                           //内容名称(第二语言)
                "20151218172300" . "\t",             //生效时间
                "20991231235959" . "\t",             //失效时间
                $media['nns_task_name'],      //搜索名
                '所有区域',                    //授权区域
                '',                           //Package标识
                '',                           //价格
                '热度推荐',                    //推荐级别
                '适合所有年龄段（G）',           //控制级别
                'IPTV',                       //领域
                '',                           //简介(第一语言)
                '',                           //简介(第二语言)
                '',                           //地区(第一语言)
                '',                           //地区(第二语言)
                '',                           //语言
                '',                           //分类(第一语言)
                '',                           //分类(第二语言)
                '',                           //关键字
                '',                           //标签(第一语言)
                '不支持',                      //预览支持
                '',                           //预览开始时间
                '',                           //预览时长
                '',                           //出品日期
                '',                           //版权到期日期
                '',                           //版权号
                '',                           //演员
                '',                           //导演
                '',                           //编剧
                '',                           //海报类型
                '',                           //海报所属领域
                '',                           //海报顺序号
                '',                           //海报路径
                '',                           //用户组信息
                (strlen($media['nns_name']) > 0 ? $media['nns_name'] : $media['nns_task_name']),//媒体名称
                !empty($media['nns_log_create_time']) ? date('Ymdhis',strtotime($media['nns_log_create_time'])) . "\t" : '',//发布时间
                '所有区域',                    //所有区域
                '1',                          //删除级别
                (is_object($this->arr_sp_info['nns_config']) ? $this->arr_sp_info['nns_config']->c2_import_push_dst_scan_url : $this->arr_sp_info['nns_config']['c2_import_push_dst_scan_url']),//FTP标识
                $media['nns_cp_id'] . '/' . $media['nns_log_id'] . '/' . $media['nns_log_id'] . '.ts',//内容源URL
                'IPTV',                       //所属领域
                '高清',                        //清晰度
                $media['nns_file_resolution'],//分辨率
                '',                           //视频ProFile
                '',                           //音频类型
                'H.264',                      //视频编码格式
                '',                           //音频编码格式
                $media['nns_filetype'],       //格式
                '2.3M(高清VBR)',               //码率
                $media['nns_file_size'] . "\t",      //文件大小
                $media['nns_file_time_len'] . "\t",  //播放时长
                '不支持',                      //是否支持加密
                '正片',                        //关联类型
                '不支持',                      //是否支持下载
                '',                           //节目内容类型
                '20151218' . "\t",                  //首播日期
                '普通点播',                    //点播类型
                '应用视频',                    //产品类型
                '2017' . "\t",                      //上映年份
            );
            fputcsv($obj_file, $str_write_content);
            usleep(20000);
        }
    }

    /**
     * 创建CSV保存路径
     */
    private function _create_save_path()
    {
        //客户端是否传递保存路径
        if(isset($_REQUEST['nns_save_path']) && strlen($_REQUEST['nns_save_path']) > 0)
        {
            $this->str_csv_path = $_REQUEST['nns_save_path'];
        }
        else
        {
            $this->str_csv_path .= date('Ymd') . '/';
        }
        //验证目录最后是否以“/”结尾
        if(strrchr($this->str_csv_path, '/') != '/')
        {
            $this->str_csv_path .= '/';
        }
        //创建文件目录
        if(!is_dir($this->str_csv_path))
        {
            $bool_dir_ret = mkdir($this->str_csv_path,0777,true);
            if(!$bool_dir_ret)
            {
                return $this->_callback_info(1,'导出片源数据的保存路径创建失败。路径：' . $this->str_csv_path);
            }
        }
        return $this->_callback_info(0,'导出片源数据的保存路径创建成功，路径：' . $this->str_csv_path);
    }

    /**
     * 输出结果
     * @param int $int_ret
     * @param string $str_reason
     * @return bool
     */
    private function _callback_info($int_ret = 0,$str_reason = '')
    {
        return array('ret' => $int_ret, 'reason' => $str_reason);
    }

    /**
     * 替换特殊字符
     * @param $str_text
     * @return mixed
     */
    private function _char_handle($str_text)
    {
        if(strlen($str_text) > 0)
        {
            $str_text = preg_replace('/(>)|(<)|(")|(\\\)|(%)|(\+)|(#)|(_)|(;)|(\')|(\|)/',' ',$str_text);
        }
        return $str_text;
    }

}

if(!isset($_REQUEST['op']))
{
    $obj_export_cdn_media = new export_cdn_media();
    $arr_export_ret = $obj_export_cdn_media->init();
}
