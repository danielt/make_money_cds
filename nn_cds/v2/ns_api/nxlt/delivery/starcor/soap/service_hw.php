<?php
ini_set("soap.wsdl_cache_enabled", "0");
define('ORG_ID', 'starcor');
include_once dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))."/common.php";

class OTT_starcor_hw{
    public function ExecCmd($CSPID, $LSPID, $CorrelateID, $CmdFileURL){
        $arg_list = func_get_args();

        \m_config::write_cdn_execute_log('执行CDN注入请求参数为：ExecCmd params:' . var_export($arg_list,true), ORG_ID, $CorrelateID);
        return array('Result'=>0,'ErrorDescription'=>'ok');
    }
}
$server = new SOAPServer('starcor_hw.wsdl');//starcor_hw.wsdl

$server->setClass('OTT_starcor_w');

$server->handle();
