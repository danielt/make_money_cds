<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/9/25 11:38
 */
//error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_model.message.message_explain");
\ns_core\m_load::load("ns_core.m_pinyin");
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_core.m_completion");
\ns_core\m_load::load_old("nn_logic/log_model/nn_log.class.php");
\ns_core\m_load::load_old("nn_logic/video/vod_index.class.php");
\ns_core\m_load::load_old("nn_logic/video/vod_media.class.php");
\ns_core\m_load::load_np("np_http_curl.class.php");
class message extends \ns_model\message\message_explain
{
    private $arr_picture = null;

    private $arr_map = null;

    //一级分类,影片种类
//    private $category_map = array (
//        '0' => '电影',  //电影
//        '1' => '电视剧',  //电视剧
//        '2' => '娱乐,综艺',  //综艺
//        '3' => '动画',  //动漫
//        '4' => '音乐',  //音乐
//        '5' => '纪实,纪录片',  //
//        '6' => '法治,科教',  //教育
//        '7' => '体育',  //体育
//        '8' => '生活',  //生活
//        '9' => '经济',  //财经
//        '10' => '特别节目',//微电影
//        '11' => '品牌专区',//品牌专区
//        '12' => '广告',  //广告
//        '13' => '新闻', //新闻
//        '14' => '公开课',
//        '15' => '外语及其他语言',
//        '16' => '青少年',
//        '17' => '播客',
//        '18' => '游戏',
//    );

  private $category_map = array(
      '0'=>array(
          "电影","动画电影","4K电影","剧情/警匪","科幻"
      ),
      '1'=>array(
          "电视剧","4K电视剧","海外剧","特色剧","剧情/都市/言情",
      ),
      "2"=>array(
          "综艺","脱口秀/真人秀","生活/真人秀","娱乐",
      ),
      "3"=>array(
          "动漫","动漫剧","少儿动漫","动画片",
      ),
      '4'=>array(
          "音乐",
      ),
      "5"=>array(
          "纪录片","历史",
      ),
      '6'=>array(
          "教育","党员教育","教育/亲子/益智","亲子教育",
      ),
      "18"=>array(
          "游戏",
      ),
      '19'=>array(
          "录制节目",
      ),
      "20"=>array(
          "宠物",
      ),
      "21"=>array(
          "话剧","剧社","戏曲- 丝弦戏","戏剧","楚剧","戏曲","京剧","荆楚戏院",
      ),
      "22"=>array(
          "网络","喜剧/爱情/剧情/网络大电影","犯罪/惊悚/悬疑/网络大电影","惊悚/悬疑/爱情/剧情/网络",
      ),
      "23"=>array(
          "旅游","旅游/优酷出品",
      ),
      "24"=>array(
          "VR",
      ),
      "25"=>array(
          "回看",
      ),
  );


    private $msg_id = '';
    private $source_id = '';
    private $arr_cp_config = array();
    private $fuse_enabled = FALSE;//融合标示  true 融合CP | false 非融合CP

    //清晰度
    private $definition_map = array(
        '标清' => 'std',
        '高清' => 'hd',
        '流畅' => 'low',
        '4K' => '4k',
        '超清' => 'sd',
    );

    public function init()
    {

    }

    public function explain($message)
    {
        $this->msg_id = $message['nns_message_id'];
        $this->source_id = $message['nns_cp_id'];
        $this->arr_cp_config = \m_config::_get_cp_info($message['nns_cp_id']);
        $this->fuse_enabled = $this->arr_cp_config['data_info']['nns_type'];

        $content = $this->get_message_content($message);
        if($content['ret'] !=0 || strlen($content['data_info']) <1)
        {
            $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, "消息接收成功,注入失败，消息ID[{$message['nns_message_id']}]" . $content['reason']);
            return $content;
        }
        $str_content =$content['data_info'];
        if(!m_config::is_json($str_content))
        {
            $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}]json内容非json结构");
            return \m_config::return_data(NS_CDS_FAIL,"消息ID[{$message['nns_message_id']}]json内容非json结构");
        }

        $json_arr = json_decode($str_content, true);
        if (!$json_arr || !is_array($json_arr))
        {
            $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],解析得到的媒资元数据错误");
            return \m_config::return_data(1, "解析得到的媒资元数据错误");
        }

        $Objects = null;
        //Series数据数组
        $arr_series = array ();
        //Program数据数组
        $arr_program = array ();
        //Movie数据数组
        $arr_movie = array ();
        //Picture数据数组
        $arr_picture = array ();
        //Channel数据数组
        $arr_channel = array ();

        //直播分集数据数组
        $arr_live_index = array ();

        //直播片源数据数组
        $arr_live_media = array ();

        //schedulerecord节目单数据数组
        $arr_playbill = array ();

        //打点信息数据数组
        $arr_seekpoint = array ();

        //演员明星数据数组
        $arr_actor = array();

        $array_regist_video = array();
        $array_regist_index = array();
        $array_regist_media = array();

        foreach ($json_arr as $items)
        {
            if ($items['id'] == $items['pid'])
            {
                $arr_series[] = $items;
            }
            else
            {
                $arr_movie[] = $items;
            }
        }
        //主媒资操作   主媒资增删改走以前老逻辑，基本没什么变化
        if (!empty($arr_series))
        {
            foreach ($arr_series as $val_series) {
                //默认一级分类为电影
                $view_type = 0;
                $asset_category = $val_series['type'] ? $val_series['type'] : '电影';

                foreach ($this->category_map as $index => $v) {
//                    $v = explode(',', $v);
                    if (!is_array($v) || empty($v)) {
                        continue;
                    }
                    foreach ($v as $_v) {
                        if (stripos($asset_category, $_v) !== false) {
                            $view_type = $index;
                            $asset_category = $v[0];        //重置栏目名称
                            break 2;
                        }
                    }
                }

                $do_category = array(
                    'base_info' => array(
                        'nns_name' => $asset_category,   //栏目名称支持多层目录，多层目录的时候 /分割
                        'nns_import_category_id' => '', //栏目名称支持多层目录，多层目录的时候 /分割
                        'nns_cp_id' => $message['nns_cp_id'],
                        'nns_import_parent_category_id' => '',
                        'nns_video_type' => '0',  //媒资类型  0 点播 | 1 直播
                    ),
                );
                $result = $this->category_action('add', $do_category);
                if ($result['ret'] != 0) {
                    $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, "消息接收成功,栏目创建失败,消息ID[{$message['nns_message_id']}],原因" . $result['reason']);
                    return $result;
                }
                if (!isset($result['data_info']['base_info']['nns_id']) || strlen($result['data_info']['base_info']['nns_id']) < 1) {
                    $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, "消息接收成功,创建栏目失败,消息ID[{$message['nns_message_id']}],原因" . $result['reason']);
                    return m_config::return_data(1, '创建栏目失败');
                }
                $nns_category_id = $result['data_info']['base_info']['nns_id'];

                #TODO
                $release_time = (isset($val_series['release_date']) && !empty($val_series['release_date']))
                    ? date('Y-m-d', strtotime(strlen($val_series['release_date']) == 4 ? $val_series['release_date'] . '-01-01'
                        : $val_series['release_date'])) : date('Y-m-d');
                $completion_data = array();

                //主媒资的话，由于上游传递下来的名称是 极盗车神_38，所以要减去后面的_38
                $temp_name = isset($val_series['name']) ? explode('_', $val_series['name']) : '';
                $temp_name = isset($temp_name['0']) ? explode('-', $temp_name['0']) : '';
                $temp_name = strtolower($temp_name['0']);
                $temp_name = str_replace(array('std', 'hd', 'low', '4k', 'sd'), '', $temp_name);
                $pinyin = m_pinyin::get_pinyin_letter($temp_name);
                $episode = (isset($val_series['total_episodes']) && $val_series['total_episodes'] > 0) ? $val_series['total_episodes'] : ((isset($val_series['episode']) && $val_series['episode'] > 0) ? $val_series['episode'] : 1);
                //非融合平台才有  媒资补全
                if (!$this->fuse_enabled) {
                    $temp_view_type = m_completion::get_temp_view_type($view_type);
                    //获取酷控媒资数据补全信息
                    m_completion::init();
                    $result_completion = m_completion::public_get_curl_completion_content(\m_config::get_dc(), htmlspecialchars($temp_name), $temp_view_type, $release_time);
                    \m_config::write_message_execute_log('媒资补全原始数据为：' . var_export($result_completion, true), $this->source_id);
                    if ($result_completion['ret'] != 0 || !isset($result_completion['data_info'])
                        || !is_array($result_completion['data_info']) || empty($result_completion['data_info'])
                    ) {
                        \m_config::write_message_execute_log('媒资补全失败,可能补全信息为空,补全结果为：' . var_export($result_completion, true), $this->source_id);
                    }
                    //封装数据，保持与第三方补全的数据键一致
                    $original_data = array(
                        'AliasName' => isset($val_series['alias_name']) ? $val_series['alias_name'] : '',
                        'OriginalCountry' => isset($val_series['region']) ? $val_series['region'] : '',
                        'VolumnCount' => $episode,
                        'Description' => isset($val_series['introduction']) ? $val_series['introduction'] : '',
                        'ActorDisplay' => '',
                        'WriterDisplay' => '',
                        'DirectorDisplay' => '',
                        'ReleaseYear' => $val_series['release_date'],
                        'Kind' => '',
                        'Language' => isset($val_series['language']) ? $val_series['language'] : '',
                    );
                    $result_completion = (isset($result_completion['data_info']) || !is_array($result_completion['data_info'])
                        || empty($result_completion['data_info'])) ? $result_completion['data_info'] : null;
                    //获取酷控媒资数据数据封装
                    $completion_data = m_completion::public_make_completion_content($original_data, $result_completion);
                    if ($completion_data['ret'] == 0 && is_array($completion_data['data_info'])) {
                        $completion_data = $completion_data['data_info'];
                    } else {
                        $completion_data = array();
                    }
                }

                \m_config::write_message_execute_log('最终媒资补全封装后结果为：' . var_export($completion_data, true), $this->source_id);
                nl_message::update_message_state(\m_config::get_dc(),$message['nns_id'],array(
                    'nns_name'=>isset($temp_name) ? $temp_name : '',         //媒资名称
                    'nns_type'=>'1',         //媒资类型
                ),false);
                $add_series = array(
                    'base_info' => array(
                        'nns_name' => isset($temp_name) ? $temp_name : '',
                        'nns_view_type' => $view_type,
                        'nns_org_type' => '0',
                        'nns_tag' => '26,',
                        'nns_area'=>isset($completion_data['OriginalCountry']) ? $completion_data['OriginalCountry'] : '',
                        'nns_director' => isset($completion_data['DirectorDisplay']) ? $completion_data['DirectorDisplay'] : '',
                        'nns_actor' => isset($completion_data['ActorDisplay']) ? $completion_data['ActorDisplay'] : '',
                        'nns_screenwriter' => isset($completion_data['WriterDisplay']) ? $completion_data['WriterDisplay'] : '',
                        'nns_show_time' => (isset($val_series['release_date']) && strlen($val_series['release_date']) > 0) ? date("Y", strtotime($val_series['release_date'])) : date("Y"),
                        'nns_view_len' => isset($val_series['duration']) ? $val_series['duration'] : 0,
                        'nns_all_index' => $episode,
                        'nns_new_index' => isset($val_series['new_episodes']) ? $val_series['new_episodes'] : 0,
//                        'nns_image0' => isset($val_series['poster_url']) && strlen($val_series['poster_url']) > 0  ? $val_series['poster_url'] : $completion_data['bigpic'],
                        'nns_image0' => ( isset($completion_data['bigpic']) && strlen($completion_data['bigpic']) > 0 )  ? $completion_data['bigpic'] : '',
                        'nns_image1' => ( isset($completion_data['middlepic']) && strlen($completion_data['middlepic']) > 0 ) ? $completion_data['middlepic'] : '',
//                        'nns_image2' => isset($completion_data['thumb_url']) && strlen($val_series['thumb_url']) > 0  ? $completion_data['thumb_url'] : $completion_data['smallpic'],
                        'nns_image2' => ( isset($completion_data['smallpic']) && strlen($completion_data['smallpic']) > 0 )  ? $completion_data['smallpic'] : '',
                        'nns_image3' => '',
                        'nns_image4' => '',
                        'nns_image5' => '',
                        'nns_summary' => isset($val_series['introduction']) && strlen($val_series['introduction']) > 0 ? $val_series['introduction'] : $completion_data['Description'],
                        'nns_remark' => isset($completion_data['Kind']) ? $completion_data['Kind'] : '',
                        'nns_category_id' => $nns_category_id,
                        'nns_play_count' => '0',
                        'nns_score_total' => '0',
                        'nns_score_count' => '0',
                        'nns_point' => isset($val_series['score']) ? $val_series['score'] : 0,
                        'nns_copyright_date' => (isset($val_series['release_date']) && strlen($val_series['release_date']) > 0) ? date("Y", strtotime($val_series['release_date'])) : date("Y"),
                        'nns_asset_import_id' => $val_series['id'],
                        'nns_pinyin' => (isset($val_series['spell']) && strlen($val_series['spell']) > 0) ? $val_series['spell'] : $pinyin,
                        'nns_pinyin_length' => (isset($val_series['spell']) && strlen($val_series['spell']) > 0) ? strlen($val_series['spell']) : strlen($pinyin),
                        'nns_alias_name' => isset($val_series['alias_name']) && strlen($val_series['alias_name']) > 0  ? $val_series['alias_name'] : $completion_data['AliasName'],
                        'nns_eng_name' => isset($val_series['english_name']) ? $val_series['english_name'] : '',
                        'nns_language' => isset($val_series['language']) && strlen($val_series['language']) > 0 ? $val_series['language'] : $completion_data['Language'],
                        'nns_text_lang' => isset($val_series['language']) ? $val_series['language'] : '',
                        'nns_producer' => isset($val_series['provider']) ? $val_series['provider'] : $this->source_id,
                        'nns_play_role' => '',
                        'nns_copyright_range' => isset($val_series['content_provider']) ? $val_series['content_provider'] : '',
                        'nns_vod_part' => '',
                        'nns_keyword' => isset($val_series['keywords']) ? $val_series['keywords'] : '',
                        'nns_import_source' => isset($val_series['nns_import_source']) ? $val_series['nns_import_source'] : evn::get("project"),
                        'nns_kind' => isset($completion_data['Kind']) ? $completion_data['Kind'] : '',
                        'nns_copyright' => isset($val_series['content_provider']) ? $val_series['content_provider'] : '',
                        'nns_clarity' => '',
//                        'nns_image_v' => isset($completion_data['verticality_img']) ? $completion_data['verticality_img'] : $val_series['poster_url'],
                        'nns_image_v' => isset($val_series['thumb_url']) ? $val_series['thumb_url'] : $completion_data['verticality_img'],
//                        'nns_image_s' => isset($completion_data['square_img']) ? $completion_data['square_img'] : '',
                        'nns_image_s' => isset($completion_data['square_img']) ? $completion_data['square_img'] : (  isset($val_series['thumb_url']) ? $val_series['thumb_url']:'' ),
                        'nns_image_h' => isset($completion_data['horizontal_img']) ? $completion_data['horizontal_img'] : (  isset($val_series['thumb_url']) ? $val_series['thumb_url']:'' ),

                        'nns_cp_id' => $message['nns_cp_id'],
                        'nns_conf_info' => '',
                        'nns_ext_url' => '',
                    ), //基本信息（存储于nns_vod表中）
                    'ex_info' => array(
                        'svc_item_id' => '',
                        'month_clicks' => '',
                        'week_clicks' => '',
                        'base_id' => '',
                        'asset_path' => '',
                        'ex_tag' => '',
                        'full_spell' => '',
                        'awards' => '',
                        'year' => '',
                        'play_time' => '',
                        'channel' => '',
                        'first_spell' => '',
                    ), //扩展信息（存储于nns_vod_ex表中）
                );
                //字段待修改4-14
                $result = $this->vod_action('add', $add_series);

                if ($result['ret'] != 0)
                {
                    $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],主媒资注入失败,原因" . $result['reason']);
                    $result['ret'] = 1;
                    return $result;
                }
                else
                {
                    //影片注入成功之后，添加明星到透传队列
                    //演员
                    if(!empty($add_series['base_info']['nns_actor']))
                    {
                        $arr_star_name_id = explode(';',$add_series['base_info']['nns_actor']);
                        if(is_array($arr_star_name_id) && !empty($arr_star_name_id))
                        {
                            foreach ($arr_star_name_id as $v)
                            {
                                if(empty($v))
                                {
                                    continue;
                                }
                                $arr_star_id = explode('|',$v);
                                if(is_array($arr_star_id) && !empty($arr_star_id['0']) && !empty($arr_star_id['1']))
                                {
                                    //注入之前，先查看透传队列中是否已经存在该明星，状态为 “等待注入”
                                    $result = nl_pass_queue::query_by_condition(\m_config::get_dc(),array('nns_status'=>1,'nns_result_desc'=>$arr_star_id['0'].'|'.$arr_star_id['1']));
                                    if($result['data_info'] !== true)
                                    {
                                        continue;
                                    }
                                    //酷控获取明星详情
                                    $star_completion_data = m_completion::public_get_star_curl_completion_content(\m_config::get_dc(),$arr_star_id['1']);
                                    \m_config::write_message_execute_log("明星（{$arr_star_id['0']}），ID（{$arr_star_id['1']}）补全结果为：" . var_export($star_completion_data, true), $this->source_id);
                                    //拼接XML，进入透传，为了传CMS 工单。
                                    $star_xml_completion_data = m_completion::make_star_xml_completion_content($star_completion_data,$arr_star_id['1'],$this->source_id);
                                    $add_pass_queue = array(
                                        "base_info"=>array(
                                            "nns_action"=>'1',
                                            "nns_type"=>104,
                                            "nns_result_desc"=>$arr_star_id['0'].'|'.$arr_star_id['1'],
                                            "nns_message_id"=>$this->msg_id,
                                            "nns_cp_id"=>$this->source_id,
                                            "nns_status"=>1,
                                            "nns_content"=> urlencode($star_xml_completion_data),
                                        )
                                    );
                                    $this->pass_queue_action('add', $add_pass_queue);
                                }
                            }
                        }
                    }
                    //导演
                    if(!empty($add_series['base_info']['nns_director']))
                    {
                        $arr_star_name_id = explode(';',$add_series['base_info']['nns_director']);
                        if(is_array($arr_star_name_id) && !empty($arr_star_name_id))
                        {
                            foreach ($arr_star_name_id as $v)
                            {
                                if(empty($v))
                                {
                                    continue;
                                }
                                $arr_star_id = explode('|',$v);
                                if(is_array($arr_star_id) && !empty($arr_star_id['0']) && !empty($arr_star_id['1']))
                                {
                                    //注入之前，先查看透传队列中是否已经存在该明星，状态为 “等待注入”
                                    $result = nl_pass_queue::query_by_condition(\m_config::get_dc(),array('nns_status'=>1,'nns_result_desc'=>$arr_star_id['0'].'|'.$arr_star_id['1']));
                                    if($result['data_info'] !== true)
                                    {
                                        continue;
                                    }
                                    //酷控获取明星详情
                                    $star_completion_data = m_completion::public_get_star_curl_completion_content(\m_config::get_dc(),$arr_star_id['1']);
                                    \m_config::write_message_execute_log("明星（{$arr_star_id['0']}），ID（{$arr_star_id['1']}）补全结果为：" . var_export($star_completion_data, true), $this->source_id);
                                    //拼接XML，进入透传，为了传CMS 工单。
                                    $star_xml_completion_data = m_completion::make_star_xml_completion_content($star_completion_data,$arr_star_id['1'],$this->source_id);
                                    $add_pass_queue = array(
                                        "base_info"=>array(
                                            "nns_action"=>'1',
                                            "nns_type"=>104,
                                            "nns_result_desc"=>$arr_star_id['0'].'|'.$arr_star_id['1'],
                                            "nns_message_id"=>$this->msg_id,
                                            "nns_cp_id"=>$this->source_id,
                                            "nns_status"=>1,
                                            "nns_content"=> urlencode($star_xml_completion_data),
                                        )
                                    );
                                    $this->pass_queue_action('add', $add_pass_queue);
                                }
                            }
                        }
                    }
                    //编剧
                    if(!empty($add_series['base_info']['nns_screenwriter']))
                    {
                        $arr_star_name_id = explode(';',$add_series['base_info']['nns_screenwriter']);
                        if(is_array($arr_star_name_id) && !empty($arr_star_name_id))
                        {
                            foreach ($arr_star_name_id as $v)
                            {
                                if(empty($v))
                                {
                                    continue;
                                }
                                $arr_star_id = explode('|',$v);
                                if(is_array($arr_star_id) && !empty($arr_star_id['0']) && !empty($arr_star_id['1']))
                                {
                                    //注入之前，先查看透传队列中是否已经存在该明星，状态为 “等待注入”
                                    $result = nl_pass_queue::query_by_condition(\m_config::get_dc(),array('nns_status'=>1,'nns_result_desc'=>$arr_star_id['0'].'|'.$arr_star_id['1']));
                                    if($result['data_info'] !== true)
                                    {
                                        continue;
                                    }
                                    //酷控获取明星详情
                                    $star_completion_data = m_completion::public_get_star_curl_completion_content(\m_config::get_dc(),$arr_star_id['1']);
                                    \m_config::write_message_execute_log("明星（{$arr_star_id['0']}），ID（{$arr_star_id['1']}）补全结果为：" . var_export($star_completion_data, true), $this->source_id);
                                    //拼接XML，进入透传，为了传CMS 工单。
                                    $star_xml_completion_data = m_completion::make_star_xml_completion_content($star_completion_data,$arr_star_id['1'],$this->source_id);
                                    $add_pass_queue = array(
                                        "base_info"=>array(
                                            "nns_action"=>'1',
                                            "nns_type"=>104,
                                            "nns_result_desc"=>$arr_star_id['0'].'|'.$arr_star_id['1'],
                                            "nns_message_id"=>$this->msg_id,
                                            "nns_cp_id"=>$this->source_id,
                                            "nns_status"=>1,
                                            "nns_content"=> urlencode($star_xml_completion_data),
                                        )
                                    );
                                    $re = $this->pass_queue_action('add', $add_pass_queue);
                                }
                            }
                        }
                    }

                    //如果是电影，就会默认再构造分集、片源
                    if ($episode == '1')
                    {
                        $add_index = array(
                            'base_info' => array(
                                'nns_name' => isset($temp_name) ? $temp_name : '',
                                'nns_index' =>  0,
                                'nns_time_len' => isset($val_series['duration']) ? $val_series['duration'] : 0,
                                'nns_summary' => isset($val_series['introduction']) && strlen($val_series['introduction']) > 0 ? $val_series['introduction'] : $completion_data['Description'],
                                'nns_image' => isset($val_series['poster_url']) && strlen($val_series['poster_url']) > 0 ? $val_series['poster_url'] : $completion_data['verticality_img'],
                                'nns_play_count' => 0,
                                'nns_score_total' => 0,
                                'nns_score_count' => 0,
                                'nns_video_import_id' => $val_series['id'],
                                'nns_import_id' => $val_series['id'],
                                'nns_import_source' => isset($val_series['nns_import_source']) ? $val_series['nns_import_source'] : evn::get("project"),
                                'nns_director' => isset($completion_data['WriterDisplay']) ? $completion_data['WriterDisplay'] : '',
                                'nns_actor' => isset($completion_data['ActorDisplay']) ? $completion_data['ActorDisplay'] : '',
                                'nns_release_time' => (isset($val_series['release_date']) && strlen($val_series['release_date']) > 0) ? date("Y", strtotime($val_series['release_date'])) : date("Y"),
                                'nns_update_time' => isset($val_series['update_time']) ? date("Y-m-d", $val_series['update_time']) : date("Y-m-d"),
                                'nns_watch_focus' => '',
                                'nns_cp_id' => $message['nns_cp_id'],
                                'nns_conf_info' => '',
                                'nns_ext_url' => '',
                            ),
                            'ex_info' => array(
                                'isintact' => '',
                                'subordinate_name' => '',
                                'initials' => '',
                                'publisher' => '',
                                'first_spell' => '',
                                'caption_language' => '',
                                'language' => isset($completion_data['Language']) ? $completion_data['Language'] : '',
                                'region' => '',
                                'adaptor' => '',
                                'sreach_key' => '',
                                'event_tag' => '',
                                'year' => '',
                                'sort_name' => '',
                            ),
                        );
                        //分集注入
                        $re = $this->index_action('add', $add_index);
                        if ($re['ret'] != 0)
                        {
                            $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],分集注入失败,原因" . $re['reason']);
                            $re['ret'] = 1;
                            return $re;
                        }
                        //片源参数
                        $add_media = array(
                            'base_info' => array(
                                'nns_name' => isset($temp_name) ? $temp_name : '',
                                'nns_type' => 1,
                                'nns_url' => isset($val_series['token']) ? $val_series['token'] : '',
                                'nns_tag' => '26,',
                                'nns_mode' => isset($this->definition_map[$val_series['definition']]) ? $this->definition_map[$val_series['definition']] : 'std',
                                'nns_kbps' => isset($val_series['BitRateType']) ? $val_series['BitRateType'] : '',
                                'nns_content_id ' => $val_series['pid'],
                                'nns_content_state' => 0,
                                'nns_filetype' => 'm3u8',
                                'nns_play_count' => '0',
                                'nns_score_total' => '0',
                                'nns_score_count' => '0',
                                'nns_video_import_id' => $val_series['id'],
                                'nns_index_import_id' => $val_series['id'],
                                'nns_import_id' => $val_series['id'],
                                'nns_import_source' => isset($val_series['nns_import_source']) ? $val_series['nns_import_source'] : evn::get("project"),
                                'nns_dimensions' => isset($val_series['Dimensions']) ? $val_series['Dimensions'] : '2D',
                                'nns_ext_url' => '',
                                'nns_file_size' => isset($val_series['FileSize']) ? $val_series['FileSize'] : 0,
                                'nns_file_time_len' => isset($val_series['duration']) ? $val_series['duration'] : 0,
                                'nns_file_frame_rate' => isset($val_series['FrameRate']) ? $val_series['FrameRate'] : 0,
                                'nns_file_resolution' => isset($val_series['Resolution']) ? $val_series['Resolution'] : 0,
                                'nns_cp_id' => $message['nns_cp_id'],
                                'nns_ext_info' => '',
                                'nns_drm_enabled' => isset($val_series['DrmEnabled']) ? $val_series['DrmEnabled'] : 0,
                                'nns_drm_encrypt_solution' => isset($val_series['DrmEncryptSolution']) ? $val_series['DrmEncryptSolution'] : '',
                                'nns_drm_ext_info' => '',
                                'nns_domain' => isset($val_series['Domain']) ? $val_series['Domain'] : 0,
                                'nns_media_type' => isset($val_series['VideoType']) ? $val_series['VideoType'] : 1,
                                'nns_original_live_id' => '',
                                'nns_start_time' => '',
                                'nns_media_service' => isset($val_series['ServiceType']) ? $val_series['ServiceType'] : 'HTTP',
                                'nns_conf_info' => '',
                                'nns_encode_flag' => isset($val_series['EncodeFlag']) ? $val_series['EncodeFlag'] : 0,
                                'nns_live_to_media' => isset($val_series['EncodeType']) ? $val_series['EncodeType'] : '',
                                'nns_media_service_type' => '',
                            ),
                            'ex_info' => array(
                                'file_hash' => '',
                                'file_width' => '',
                                'file_height' => '',
                                'file_scale' => '',
                                'file_coding' => '',
                            ),
                        );

                        $re = $this->media_action('add', $add_media);
                        if ($re['ret'] != 0)
                        {
                            $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因" . $re['reason']);
                            $re['ret'] = 1;
                            return $re;
                        }
                    }
                    $this->create_xml_respond_msg_to_csp(NS_CDS_SUCCE, "消息接收成功,等待注入,消息ID[{$message['nns_message_id']}]");
                }
            }
        }
        //分集/片源操作---暂时不用
        if (!empty($arr_program))
        {
            foreach ($arr_program as $val_program)
            {
                $res = nl_asset_incidence_relation::query_asset_relation(m_config::get_dc(), 'video', $val_program['pid'], $message['nns_cp_id'], 0);
                if (!is_array($res['data_info']))
                {
                    $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因" . $res['reason']);
                    $res['ret'] = 7;
                    $res['reason'] = "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],主媒资不存在,原因" . $res['reason'];
                    return $res;
                }
                $release_time = (isset($val_series['release_date']) && !empty($val_series['release_date']))
                    ? date('Y-m-d', strtotime(strlen($val_series['release_date']) ==4 ? $val_series['release_date'].'-01-01'
                        : $val_series['release_date'])) : date('Y-m-d');
                $completion_data = array();
                //非融合平台才有  媒资补全
                if (!$this->fuse_enabled)
                {
                    $temp_view_type = m_completion::get_temp_view_type($view_type);
                    //获取酷控媒资数据补全信息
                    m_completion::init();
                    $result_completion = m_completion::public_get_curl_completion_content(\m_config::get_dc(), htmlspecialchars($val_series['name']), $temp_view_type, $release_time);
                    if ($result_completion['ret'] != 0 || isset($result_completion['data_info'])
                        || !is_array($result_completion['data_info']) || empty($result_completion['data_info']))
                    {
                        \m_config::write_message_execute_log('媒资补全失败,可能补全信息为空,补全结果为：' . var_export($result_completion, true), $this->source_id);
                    }
                    //封装数据，保持与第三方补全的数据键一致
                    $original_data = array(
                        'AliasName' => isset($val_series['alias_name']) ? $val_series['alias_name'] : '',
                        'OriginalCountry'=> isset($val_series['region']) ? $val_series['region'] : '',
                        'VolumnCount' => isset($val_series['total_episodes']) ? $val_series['total_episodes'] : ( isset($val_series['episode']) ? $val_series['episode'] : '' ),
                        'Description' => isset($val_series['introduction']) ? $val_series['introduction'] : '',
                        'ActorDisplay' => '',
                        'WriterDisplay' => '',
                        'DirectorDisplay' => '',
                        'ReleaseYear' => $val_series['release_date'],
                        'Kind' => '',
                        'Language' => '',
                    );
                    $result_completion = (isset($result_completion['data_info']) || !is_array($result_completion['data_info'])
                        || empty($result_completion['data_info'])) ? $result_completion['data_info'] : null;
                    //获取酷控媒资数据数据封装
                    $completion_data = m_completion::public_make_completion_content($original_data, $result_completion);
                }
                $add_index = array(
                    'base_info' => array(
                        'nns_name' => isset($val_program['Name']) ? $val_program['Name'] : '',
                        'nns_index' => (isset($val_program['episode']) && (int)$val_program['episode'] > 0) ? (int)$val_program['episode'] -1 : 0,
                        'nns_time_len' => '',
                        'nns_summary' => isset($val_program['introduction']) ? $val_program['introduction'] : '',
                        'nns_image' => isset($val_program['poster_url']) ? $val_program['poster_url'] : '',
                        'nns_play_count' => 0,
                        'nns_score_total' => 0,
                        'nns_score_count' => 0,
                        'nns_video_import_id' => $res['data_info'][0]['nns_asset_import_id'],
                        'nns_import_id' => $val_program['id'],
                        'nns_import_source' => isset($val_program['nns_import_source']) ? $val_program['nns_import_source'] : evn::get("project"),
                        'nns_director' => isset($val_program['director']) ? $val_program['director'] : '',
                        'nns_actor' => isset($val_program['actor']) ? $val_program['actor'] : '',
                        'nns_release_time' => (isset($val_program['release_date']) && strlen($val_program['release_date']) > 0) ? date("Y", strtotime($val_program['release_date'])) : date("Y"),
                        'nns_update_time' => isset($val_program['release_date']) ? date("Y-m-d", $val_program['release_date']) : date("Y-m-d"),
                        'nns_watch_focus' => '',
                        'nns_cp_id' => $message['nns_cp_id'],
                        'nns_conf_info' => '',
                        'nns_ext_url' => '',
                    ),
                    'ex_info' => array(
                        'isintact' => '',
                        'subordinate_name' => '',
                        'initials' => '',
                        'publisher' => '',
                        'first_spell' => '',
                        'caption_language' => '',
                        'language' => isset($val_program['language']) ? $val_program['language'] : '',
                        'region' => '',
                        'adaptor' => '',
                        'sreach_key' => '',
                        'event_tag' => '',
                        'year' => '',
                        'sort_name' => '',
                    ),
                );

                //分集注入
                $re = $this->index_action('add', $add_index);

                if ($re['ret'] != 0)
                {
                    $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因" . $re['reason']);
                    $re['ret'] = 1;
                    return $re;
                }
                else
                {
                    //注入片源
                    $this->create_xml_respond_msg_to_csp(NS_CDS_SUCCE, "消息接收成功,等待注入,消息ID[{$message['nns_message_id']}]");
                }
            }
        }
        //片源操作
        if (!empty($arr_movie))
        {
            foreach ($arr_movie as $val_movie)
            {
//                $arr_p_program = $this->get_map_info_v2('Movie', $this->arr_map, $val_movie['ContentID']);
//                $arr_p_program = array_keys($arr_p_program);
                $index_info = nl_asset_incidence_relation::query_asset_relation(m_config::get_dc(), 'index', $val_movie['id'], $message['nns_cp_id'], 1);
                if ($index_info['ret'] != 0)
                {
                    $re['ret'] = 1;
                    $re['reason'] = "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],查询【分集】失败原因" . $index_info['reason'];
                    $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, $re['reason']);
                    return $re;
                }
                if (!is_array($index_info['data_info']))
                {
                    \m_config::write_message_execute_log('模拟生成分集注入', $this->source_id);
                    //默认一级分类为电影
                    $view_type = 0;
                    $asset_category = $val_movie['type'] ? $val_movie['type'] : '电影';

                    foreach ($this->category_map as $index => $v)
                    {
//                        $v = explode(',', $v);
                        if(!is_array($v) || empty($v))
                        {
                            continue;
                        }
                        foreach ($v as $_v)
                        {
                            if (stripos($asset_category, $_v) !== false)
                            {
                                $view_type = $index;
                                $asset_category = $v[0];        //重置栏目名称
                                break 2;
                            }
                        }
                    }

                    $res = nl_asset_incidence_relation::query_asset_relation(m_config::get_dc(), 'video', $val_movie['pid'], $message['nns_cp_id'], 0);
                    if (!is_array($res['data_info']))
                    {
                        $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因" . $res['reason']);
                        $res['ret'] = 7;
                        $res['reason'] = "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],主媒资不存在,原因" . $res['reason'];
                        return $res;
                    }
                    $release_time = (isset($val_series['release_date']) && !empty($val_series['release_date']))
                        ? date('Y-m-d', strtotime(strlen($val_series['release_date']) == 4 ? $val_series['release_date'].'-01-01'
                            : $val_series['release_date'])) : date('Y-m-d');
                    $completion_data = array();

                    //处理 分集、片源的多余名称
                    $val_movie['name'] = str_replace(array('std','hd','low','4k','sd'),'',$val_movie['name']);
                    //非融合平台才有  媒资补全
                    if (!$this->fuse_enabled)
                    {
                        $temp_view_type = m_completion::get_temp_view_type($view_type);
                        //获取酷控媒资数据补全信息
                        m_completion::init();
                        $result_completion = m_completion::public_get_curl_completion_content(\m_config::get_dc(), htmlspecialchars($val_movie['name']), $temp_view_type, $release_time);
                        if ($result_completion['ret'] != 0 || !isset($result_completion['data_info'])
                            || !is_array($result_completion['data_info']) || empty($result_completion['data_info']))
                        {
                            \m_config::write_message_execute_log('媒资补全失败,可能补全信息为空,补全结果为：' . var_export($result_completion, true), $this->source_id);
                        }

                        $episode = ( isset($val_movie['total_episodes']) && $val_movie['total_episodes'] > 0  ) ? $val_movie['total_episodes'] : ( ( isset($val_movie['episode']) && $val_movie['episode'] > 0 ) ? $val_movie['episode'] : 1 );

                        //封装数据，保持与第三方补全的数据键一致
                        $original_data = array(
                            'AliasName' => isset($val_movie['alias_name']) ? $val_movie['alias_name'] : '',
                            'OriginalCountry'=> isset($val_movie['region']) ? $val_movie['region'] : '',
                            'VolumnCount' => $episode,
                            'Description' => isset($val_movie['introduction']) ? $val_movie['introduction'] : '',
                            'ActorDisplay' => '',
                            'WriterDisplay' => '',
                            'DirectorDisplay' => '',
                            'ReleaseYear' => $val_movie['release_date'],
                            'Kind' => '',
                            'Language' => '',
                        );
                        $result_completion = (isset($result_completion['data_info']) || !is_array($result_completion['data_info'])
                            || empty($result_completion['data_info'])) ? $result_completion['data_info'] : null;
                        //获取酷控媒资数据数据封装
                        $completion_data = m_completion::public_make_completion_content($original_data, $result_completion, false);
                        if ($completion_data['ret'] == 0 && is_array($completion_data['data_info']))
                        {
                            $completion_data = $completion_data['data_info'];
                        }
                        else
                        {
                            $completion_data = array();
                        }
                    }

                    nl_message::update_message_state(\m_config::get_dc(),$message['nns_id'],array(
                        'nns_name'=>isset($val_movie['name']) ? $val_movie['name'] : '',         //媒资名称
                        'nns_type'=>'2',         //媒资类型
                    ),false);

                    $add_index = array(
                        'base_info' => array(
                            'nns_name' => isset($val_movie['name']) ? $val_movie['name'] : '',
                            'nns_index' => (isset($val_movie['episode']) && (int)$val_movie['episode'] > 0) ? (int)$val_movie['episode'] -1 : 0,
                            'nns_time_len' => '',
                            'nns_summary' => isset($val_movie['introduction']) && strlen($val_movie['introduction']) > 0 ? $val_movie['introduction'] : $completion_data['Description'],
                            'nns_image' => isset($val_movie['poster_url']) && strlen($val_movie['poster_url']) > 0 ? $val_movie['poster_url'] : $completion_data['pic'],
                            'nns_play_count' => 0,
                            'nns_score_total' => 0,
                            'nns_score_count' => 0,
                            'nns_video_import_id' => $val_movie['pid'],
                            'nns_import_id' => $val_movie['id'],
                            'nns_import_source' => isset($val_movie['nns_import_source']) ? $val_movie['nns_import_source'] : evn::get("project"),
                            'nns_director' => isset($completion_data['WriterDisplay']) ? $completion_data['WriterDisplay'] : '',
                            'nns_actor' => isset($completion_data['ActorDisplay']) ? $completion_data['ActorDisplay'] : '',
                            'nns_release_time' => (isset($val_movie['release_date']) && strlen($val_movie['release_date']) > 0) ? date("Y", strtotime($val_movie['release_date'])) : date("Y"),
                            'nns_update_time' => isset($val_movie['update_time']) ? date("Y-m-d", $val_movie['update_time']) : date("Y-m-d"),
                            'nns_watch_focus' => '',
                            'nns_cp_id' => $message['nns_cp_id'],
                            'nns_conf_info' => '',
                            'nns_ext_url' => '',
                        ),
                        'ex_info' => array(
                            'isintact' => '',
                            'subordinate_name' => '',
                            'initials' => '',
                            'publisher' => '',
                            'first_spell' => '',
                            'caption_language' => '',
                            'language' => isset($completion_data['Language']) ? $completion_data['Language'] : '',
                            'region' => '',
                            'adaptor' => '',
                            'sreach_key' => '',
                            'event_tag' => '',
                            'year' => '',
                            'sort_name' => '',
                        ),
                    );
                    //分集注入
                    $re = $this->index_action('add', $add_index);

                    if ($re['ret'] != 0)
                    {
                        $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因" . $re['reason']);
                        $re['ret'] = 1;
                        return $re;
                    }
                }

                $index_import_id = $val_movie['id'];
                $vod_import_id = $val_movie['pid'];

                //片源参数
                $add_media = array(
                    'base_info' => array(
                        'nns_name' => isset($val_movie['name']) ? $val_movie['name'] : '',
                        'nns_type' => 1,
                        'nns_url' => isset($val_movie['token']) ? $val_movie['token'] : '',
                        'nns_tag' => '26,',
                        'nns_mode' => isset($this->definition_map[$val_movie['definition']]) ? $this->definition_map[$val_movie['definition']] : 'std',
                        'nns_kbps' => isset($val_movie['BitRateType']) ? $val_movie['BitRateType'] : '',
                        'nns_content_id ' => $val_movie['pid'],
                        'nns_content_state' => 0,
                        'nns_filetype' =>  'm3u8',
                        'nns_play_count' => '0',
                        'nns_score_total' => '0',
                        'nns_score_count' => '0',
                        'nns_video_import_id' => $vod_import_id,
                        'nns_index_import_id' => $index_import_id,
                        'nns_import_id' => $val_movie['id'],
                        'nns_import_source' => isset($val_movie['nns_import_source']) ? $val_movie['nns_import_source'] : evn::get("project"),
                        'nns_dimensions' => isset($val_movie['Dimensions']) ? $val_movie['Dimensions'] : '2D',
                        'nns_ext_url' => '',
                        'nns_file_size' => isset($val_movie['FileSize']) ? $val_movie['FileSize'] : 0,
                        'nns_file_time_len' => isset($val_movie['TimeLen']) ? $val_movie['TimeLen'] : 0,
                        'nns_file_frame_rate' => isset($val_movie['FrameRate']) ? $val_movie['FrameRate'] : 0,
                        'nns_file_resolution' => isset($val_movie['Resolution']) ? $val_movie['Resolution'] : 0,
                        'nns_cp_id' => $message['nns_cp_id'],
                        'nns_ext_info' => '',
                        'nns_drm_enabled' => isset($val_movie['DrmEnabled']) ? $val_movie['DrmEnabled'] : 0,
                        'nns_drm_encrypt_solution' => isset($val_movie['DrmEncryptSolution']) ? $val_movie['DrmEncryptSolution'] : '',
                        'nns_drm_ext_info' => '',
                        'nns_domain' => isset($val_movie['Domain']) ? $val_movie['Domain'] : 0,
                        'nns_media_type' => isset($val_movie['VideoType']) ? $val_movie['VideoType'] : 1,
                        'nns_original_live_id' => '',
                        'nns_start_time' => '',
                        'nns_media_service' => isset($val_movie['ServiceType']) ? $val_movie['ServiceType'] : 'HTTP',
                        'nns_conf_info' => '',
                        'nns_encode_flag' => isset($val_movie['EncodeFlag']) ? $val_movie['EncodeFlag'] : 0,
                        'nns_live_to_media' => isset($val_movie['EncodeType']) ? $val_movie['EncodeType'] : '',
                        'nns_media_service_type' => '',
                    ),
                    'ex_info' => array(
                        'file_hash' => '',
                        'file_width' => '',
                        'file_height' => '',
                        'file_scale' => '',
                        'file_coding' => '',
                    ),
                );

                $re = $this->media_action('add', $add_media);
                if ($re['ret'] != 0)
                {
                    $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因" . $re['reason']);
                    $re['ret'] = 1;
                    return $re;
                }
                else
                {
                    $this->create_xml_respond_msg_to_csp(NS_CDS_SUCCE, "消息接收成功,等待注入,消息ID[{$message['nns_message_id']}]");
                }
            }
        }
        //明星库注入
        if (!empty($arr_actor))
        {
            foreach ($arr_actor as $val_actor)
            {
                //主媒资删除
                if (strtoupper($val_actor['Action']) == 'DELETE')
                {
                    $del_actor = array(
                        'base_info'=>array(
                            'nns_import_id'=>$val_actor['ContentID'],
                            'nns_import_source'=>$val_actor['nns_import_source'],
                            'nns_cp_id'=>$message['nns_cp_id'],
                        ),
                    );
                    $result = $this->actor_action('delete', $del_actor);
                }
                else
                {
                    #TODO
                    $arr_img = $this->handle_picture_v3($val_actor['ContentID']);
                    if (!empty($arr_img) && is_array($arr_img))
                    {
                        $val_series = array_merge($val_actor, $arr_img);
                    }
                    $pinyin = m_pinyin::get_pinyin_letter($val_actor['Name']);
                    $ex_info =  array(
                        'constellation' => isset($val_actor['Constellation']) ? $val_actor['Constellation'] : '',
                        'college' => isset($val_actor['College']) ? $val_actor['College'] : '',
                        'weight' => isset($val_actor['Weight']) ? $val_actor['Weight'] : '',
                        'birthplace' => isset($val_actor['Hometown']) ? $val_actor['Hometown'] : '',
                        'bloodtype' => isset($val_actor['BloodGroup']) ? $val_actor['BloodGroup'] : '',
                        'height' => isset($val_actor['Height']) ? $val_actor['Height'] : '',
                        'introduction' => isset($val_actor['Description']) ? $val_actor['Description'] : '',
                        'birthday' => isset($val_actor['Birthday']) ? date('Y-m-d', strtotime($val_actor['Birthday'])) : '',
                        'story' => isset($val_actor['story']) ? $val_actor['story'] : '',
                    );
                    $ex_info = json_encode($ex_info,JSON_UNESCAPED_UNICODE);
                    $add_actor = array(
                        'base_info' => array(
                            'nns_name' => isset($val_actor['Name']) ? $val_actor['Name'] : '',
                            'nns_area' => isset($val_actor['OriginalCountry']) ? $val_actor['OriginalCountry'] : '',
                            'nns_alias_name' => isset($val_actor['AliasName']) ? $val_actor['AliasName'] : '',
                            'nns_profession' => isset($val_actor['Profession']) ? $val_actor['Profession'] : '',
                            'nns_pinyin' => (isset($val_actor['Spell']) && strlen($val_actor['Spell']) > 0) ? $val_actor['Spell'] : $pinyin,
                            'nns_works' => isset($val_actor['Works']) ? $val_actor['Works'] : 0,
                            'nns_label_id' => isset($val_actor['LabelID']) ? $val_actor['LabelID'] : 0,
                            'nns_image_v' => isset($val_actor['verticality_img']) ? $val_actor['verticality_img'] : '',
                            'nns_image_s' => isset($val_actor['square_img']) ? $val_actor['square_img'] : '',
                            'nns_image_h' => isset($val_actor['horizontal_img']) ? $val_actor['horizontal_img'] : '',
                            'nns_old_name' => '',
                            'nns_info' => $ex_info,
                            'nns_english_name' => isset($val_actor['EnglishName']) ? $val_actor['EnglishName'] : '',
                            'nns_county' => isset($val_actor['County']) ? $val_actor['County'] : '',
                            'nns_sex' => (isset($val_actor['Sex']) && $val_actor['Sex'] == 1) ? '男' : '女',
                            'nns_import_id' => $val_actor['ContentID'],
                            'nns_import_source' => isset($val_actor['nns_import_source']) ? $val_actor['nns_import_source'] : evn::get("project"),
                            'nns_cp_id' => $message['nns_cp_id'],
                            ''
                        ), //基本信息（存储于nns_actor表中）
                        'ex_info' => array(), //扩展信息（存储于nns_actor_ex表中）
                    );
                    //字段待修改4-14
                    $result = $this->actor_action('add', $add_actor);
                }
                if($result['ret'] !=0)
                {
                    $result['ret'] = 1;
                    return $result;
                }
            }
        }
        //主媒资上下线
        if (!empty($this->arr_map) && is_array($this->arr_map))
        {
            foreach ($this->arr_map as $map)
            {
                $map_children = $map['children'];
                $map = $map['attributes'];
                if ($map['ElementType'] == 'Category')
                {
                    //此时是上下线
                    //消息操作指令:1添加2修改3删除,4上线,5下线
                    $action = null;
                    if ($map['Action'] == 'DELETE')
                    {
                        $action = 5;
                    }
                    else
                    {
                        $action = 4;
                    }
                    //注入ID
                    $asset_id = $map['ElementID'];
                    //栏目ID
                    $content = array();
                    $content['category_id'] = $map['ParentID'];
                    $content['cp_id'] = isset($message['nns_cp_id']) ? $message['nns_cp_id'] : 0;
                    $content['action'] = $action;
                    $asset_type = 4;
                    if ($map['ElementType'] == 'Program')
                    {
                        $asset_type = 5;
                        if ($message['nns_cp_id'] == 'CNTV' || $message['nns_cp_id'] == 'YANHUAAMS' || $message['nns_cp_id'] == 'CIBN')
                        {
                            //说明是影片，需要转换成自身生成的主媒资ID
                            $asset_id = substr_replace($asset_id, '00100', 0, 5);
                        }
                    }
                    //上下线主媒资名称
                    $ElementName = null;
                    $Sequence = 0;
                    if (is_array($map_children))
                    {
                        foreach ($map_children as $pro)
                        {
                            if ($pro['attributes']['Name'] == 'ElementName')
                            {
                                $ElementName = $pro['content'];
                            }
                            if ($pro['attributes']['Name'] == 'Sequence')
                            {
                                $Sequence = $pro['content'];
                            }
                        }
                    }
                    $content['order'] = $Sequence;
                    $content['message_id'] = $message['nns_id'];
                    $result = $this->line_asset($content, $asset_id);
                    if ($result['ret'] != 0)
                    {
                        $result['ret'] = 1;
                        return $result;
                    }
                    return $result;
                }

            }
        }

        return m_config::return_data(0,"消息注入成功");
    }

    private function handle_picture_v3($asset_id)
    {
        $array_img = array();
        $array_img['bigpic']='';
        $array_img['middlepic']='';
        $array_img['smallpic']='';
        $array_img['horizontal_img']='';
        $array_img['verticality_img']='';
        $array_img['square_img']='';
        if(empty($this->arr_picture) || !is_array($this->arr_picture))
        {
            return $array_img;
        }
        $array_map = $this->get_map_info_v2('Picture',$this->arr_map,$asset_id);
        if(empty($array_map))
        {
            return $array_img;
        }
        $array_map_key = array_keys($array_map);
        $temp_array = array();
        foreach ($this->arr_picture as $obj_picture)
        {
            if(!in_array($obj_picture['ContentID'], $array_map_key))
            {
                continue;
            }
            //组成数据,key为图片type,值为url
            if (!isset($obj_picture['Type']))
            {
                foreach ($array_map as $k_id => $val)
                {
                    if ($obj_picture['ContentID'] == $k_id)
                    {
                        $obj_picture['Type'] = $val['Type'];
                    }
                }
            }
            $temp_array[$obj_picture['Type']] = (isset($obj_picture['FileURL']) && strlen($obj_picture['FileURL']) >0) ? trim($obj_picture['FileURL']) : '';
        }
        $temp_array = array_filter($temp_array);
        if(empty($temp_array))
        {
            return $array_img;
        }
        $arr_combine = $this->combine_picture($array_img, $temp_array);
        return array_combine(array_keys($array_img),$arr_combine);
    }

    private function get_map_info_v2($p_type,$arr_map, $e_id)
    {
        $temp_arr = array();
        if ($p_type == 'Picture')
        {
            foreach ($arr_map as $map)
            {
                if ($map['attributes']['ElementType'] == $p_type && $map['attributes']['ParentCode'] == $e_id)
                {
                    $filter_map = $this->make_key_value_arr($map);
                    $temp_arr[$map['attributes']['ElementCode']] = $filter_map;
                }
                if (isset($temp_arr[$map['attributes']['ElementCode']]))
                {
                    continue;
                }
                if ($map['attributes']['ParentType'] == $p_type && $map['attributes']['ElementCode'] == $e_id)
                {
                    $filter_map = $this->make_key_value_arr($map);
                    $temp_arr[$map['attributes']['ParentCode']] = $filter_map;
                }
            }

        }
        else
        {
            foreach ($arr_map as $map)
            {
                if ($map['attributes']['ElementType'] == $p_type && $map['attributes']['ParentType'] != 'Picture' && $map['attributes']['ElementCode'] == $e_id)
                {
                    $filter_map = $this->make_key_value_arr($map);
                    $temp_arr[$map['attributes']['ParentCode']] = $filter_map;
                }
            }
        }
        return $temp_arr;
    }
    /**
     * 获取xml的 attr属性和key value值
     * @param unknown $xml_obj_arr
     * @return Ambigous <NULL, unknown, string>
     */
    private function make_key_value_arr($xml_obj_arr)
    {
        $key_val_array = null;
        if (isset($xml_obj_arr['attributes']) && is_array($xml_obj_arr['attributes']) && !empty($xml_obj_arr['attributes']))
        {
            foreach ($xml_obj_arr['attributes'] as $attr_key => $attr_val)
            {
                $key_val_array[$attr_key] = $attr_val;
            }
            if(!isset($key_val_array['ContentID']) || strlen($key_val_array['ContentID'])<1)
            {
                $key_val_array['ContentID'] = isset($xml_obj_arr['attributes']['ID']) ? $xml_obj_arr['attributes']['ID'] : '';
            }
            unset($xml_obj_arr['attributes']);
        }
        if (isset($xml_obj_arr['children']) && is_array($xml_obj_arr['children']) && !empty($xml_obj_arr['children']))
        {
            foreach ($xml_obj_arr['children'] as $key_list)
            {
                if (isset($key_list['attributes']['Name']) && strlen($key_list['attributes']['Name']) > 0)
                {
                    $key_val_array[$key_list['attributes']['Name']] = (isset($key_list['content']) && strlen($key_list['content']) > 0) ? trim($key_list['content']) : '';
                }
            }
            unset($xml_obj_arr['children']);
        }
        return $key_val_array;
    }

    /**
     * 主媒资上下线
     *
     * @param array $content 内容
     * @param string $asset_id 主媒资ID
     * @return array
     */
    public function  line_asset($content,$asset_id){
        $content['asset_type'] = isset($content['asset_type'])?$content['asset_type']:'video';
        //查询主媒资分集映射关系
        $vod_info = nl_asset_incidence_relation::query_asset_relation(m_config::$obj_dc,$content['asset_type'],$asset_id,$content['cp_id']);
        if($vod_info['ret'] !=0)
        {
            return $vod_info;
        }
        $exists_id = (isset($vod_info['data_info'][0]['nns_id']) && !empty($vod_info['data_info'][0]['nns_id'])) ? $vod_info['data_info'][0]['nns_id'] : false;
        if($exists_id === FALSE)
        {
            return array(
                "ret" => 1,
                "reason" => '媒资上下线'.$content['asset_type']."查无数据"
            );
        }
        //获取任务名称
        $task_arr = $this->get_task_name($exists_id, $content['asset_type']);
        $task_name = $task_arr['task_name'];
        //组装上下线需要的参数
        $add_asset_online = array(
            "base_info"=>array(
                'nns_vod_id'=>$exists_id,
                'nns_video_name'=>$task_name,
                'nns_action'=> ($content['action'] == '5') ? 'unline' : 'online',
                'nns_message_id'=>$content['message_id'],
                'nns_import_id'=>$asset_id,
                'nns_order'=>9999-(float)$content['order'],
                'nns_cp_id'=>$content['cp_id'],
                'nns_category_id'=>$content['category_id'],
                'nns_type'=> $content['asset_type']
            )
        );
        $re = $this->asset_online_action('add', $add_asset_online);
        return $re;
    }

    /**
     * 获取任务名称
     * @param string $video_id 视频id
     * @param string $video_type 视频类型
     * @return string task_name
     * @author yunping.yang
     * @date 2014-8-12
     */
    private function get_task_name($video_id, $video_type)
    {
        set_time_limit(0);
        $db = m_config::get_dc()->db();
        //获取任务名称
        $task_name = '';
        $asset_id = '';
        $nns_kbps = 0;
        $nns_cp_id = '';
        $nns_ext_url = '';
        $nns_url = '';
        $nns_live_to_media = 1;
        $nns_filetype = '';
        $ex_data = array();
        switch ($video_type)
        {
            case video:
                $task_name_arr = nl_db_get_one("select nns_name,nns_cp_id,nns_asset_import_id,nns_ext_url from nns_vod where nns_id='{$video_id}'", $db);
                $task_name = $task_name_arr['nns_name'];
                $nns_cp_id = (strlen($task_name_arr['nns_cp_id']) > 0) ? $task_name_arr['nns_cp_id'] : 0;
                $asset_id = $video_id;
                $str_import_id = $task_name_arr['nns_asset_import_id'];
                $nns_ext_url = $task_name_arr['nns_ext_url'];
                $nns_media_encode_flag = 0;
                $nns_ext_info = '';
                $nns_tag = '';
                break;
            case BK_OP_INDEX:
                $task_name_arr = nl_db_get_one("select nns_name,nns_vod_id,nns_index,nns_cp_id,nns_import_id,nns_ext_url from nns_vod_index where nns_id='{$video_id}'", $db);
                if (strlen(trim($task_name_arr['nns_name'])) <= 0)
                {
                    $task_name_arr['nns_index1'] = (int)$task_name_arr['nns_index'] + 1;
                    $task_name_arr['nns_name'] = '第' . $task_name_arr['nns_index1'] . '集';
                }
                $task_name = '[' . $task_name_arr['nns_name'] . ']';
                $asset_id = $task_name_arr['nns_vod_id'];
                $nns_cp_id = (strlen($task_name_arr['nns_cp_id']) > 0) ? $task_name_arr['nns_cp_id'] : 0;
                $str_import_id = $task_name_arr['nns_import_id'];
                $nns_ext_url = $task_name_arr['nns_ext_url'];
                $nns_media_encode_flag = 0;
                $nns_ext_info = '';
                $nns_tag = '';
                break;
            case BK_OP_MEDIA:
                $task_name_arr = nl_db_get_one("select vodindex.nns_name,vodindex.nns_index,media.nns_mode,media.nns_filetype,media.nns_tag as nns_tag,media.nns_media_type as nns_media_type,media.nns_url as nns_url,media.nns_vod_id as nns_vod_id,media.nns_kbps as nns_kbps,media.nns_cp_id as nns_cp_id,media.nns_content_id as nns_content_id,media.nns_import_id as nns_import_id,media.nns_ext_url as nns_ext_url,media.nns_ext_info as nns_ext_info,media.nns_encode_flag as nns_encode_flag  from nns_vod_media media left join nns_vod_index vodindex on media.nns_vod_index_id=vodindex.nns_id   where media.nns_id='{$video_id}'", $db);
                if (strlen(trim($task_name_arr['nns_name'])) <= 0)
                {
                    $task_name_arr['nns_index1'] = (int)$task_name_arr['nns_index'] + 1;
                    $task_name_arr['nns_name'] = '第' . $task_name_arr['nns_index1'] . '集';
                }
                $ex_data['nns_content_id'] = $task_name_arr['nns_content_id'];
                $asset_id = $task_name_arr['nns_vod_id'];
                $nns_kbps = ($task_name_arr['nns_kbps'] > 0) ? $task_name_arr['nns_kbps'] : 0;
                $task_name = '[' . $task_name_arr['nns_name'] . '] [' . $nns_kbps . '/kbps] ' . ' [' . $task_name_arr['nns_tag'] . '] ' . $task_name_arr['nns_mode'];
                $nns_cp_id = (strlen($task_name_arr['nns_cp_id']) > 0) ? $task_name_arr['nns_cp_id'] : 0;
                $str_import_id = $task_name_arr['nns_import_id'];
                $nns_ext_url = $task_name_arr['nns_ext_url'];
                $nns_ext_info = $task_name_arr['nns_ext_info'];
                $nns_tag = (strlen($task_name_arr['nns_tag']) > 0) ? $task_name_arr['nns_tag'] : '';
                $nns_media_encode_flag = (isset($task_name_arr['nns_encode_flag']) && in_array($task_name_arr['nns_encode_flag'], array('0', '1', '2'))) ? $task_name_arr['nns_encode_flag'] : 0;
                $nns_tag = $task_name_arr['nns_tag'];
                $nns_url = $task_name_arr['nns_url'];
                $nns_filetype = $task_name_arr['nns_filetype'];
                $nns_live_to_media = $task_name_arr['nns_media_type'];
                break;
            case BK_OP_LIVE:
                $task_name_arr = nl_db_get_one("select nns_name,nns_cp_id,nns_import_id,nns_integer_id from nns_live where nns_id='{$video_id}'", $db);
                $task_name = $task_name_arr['nns_name'];
                $nns_cp_id = (strlen($task_name_arr['nns_cp_id']) > 0) ? $task_name_arr['nns_cp_id'] : 0;
                $asset_id = $video_id;
                $str_import_id = (strlen($task_name_arr['nns_import_id']) > 0) ? $task_name_arr['nns_import_id'] : $task_name_arr['nns_integer_id'];
                $nns_ext_url = '';
                $nns_media_encode_flag = 0;
                $nns_ext_info = '';
                $nns_tag = '';
                break;
            case BK_OP_LIVE_MEDIA:
                $task_name_arr = nl_db_get_one("select live_index.nns_index,media.nns_mode,media.nns_live_id as nns_vod_id,media.nns_tag as nns_tag,media.nns_kbps as nns_kbps,media.nns_cp_id as nns_cp_id,media.nns_content_id as nns_content_id,media.nns_ext_url as nns_ext_url  from nns_live_media as media left join nns_live_index live_index on media.nns_live_index_id=live_index.nns_id   where media.nns_id='{$video_id}'", $db);
                if (strlen(trim($task_name_arr['nns_name'])) <= 0)
                {
                    $task_name_arr['nns_index1'] = (int)$task_name_arr['nns_index'] + 1;
                    $task_name_arr['nns_name'] = '第' . $task_name_arr['nns_index1'] . '集';
                }
                $ex_data['nns_content_id'] = $task_name_arr['nns_content_id'];
                $asset_id = $task_name_arr['nns_vod_id'];
                $nns_kbps = ($task_name_arr['nns_kbps'] > 0) ? $task_name_arr['nns_kbps'] : 0;
                $task_name = '[' . $task_name_arr['nns_name'] . '] [' . $nns_kbps . '/kbps] ' . ' [' . $task_name_arr['nns_tag'] . '] ' . $task_name_arr['nns_mode'];
                $nns_cp_id = (strlen($task_name_arr['nns_cp_id']) > 0) ? $task_name_arr['nns_cp_id'] : 0;
                $str_import_id = $task_name_arr['nns_content_id'];
                $nns_ext_url = $task_name_arr['nns_ext_url'];
                $nns_media_encode_flag = 0;
                $nns_ext_info = '';
                $nns_tag = '';
                break;
            case BK_OP_PLAYBILL:
                $task_name_arr = nl_db_get_one("select nns_name,nns_cp_id,nns_playbill_import_id,nns_ext_url,nns_begin_time,nns_live_id from nns_live_playbill_item where nns_id='{$video_id}'", $db);
                if (isset($task_name_arr['nns_live_id']) && strlen($task_name_arr['nns_live_id']))
                {
                    $task_name_arr_live = nl_db_get_one("select nns_name from nns_live where nns_id='{$task_name_arr['nns_live_id']}'", $db);
                }
                $live_name_temp = isset($task_name_arr_live['nns_name']) ? $task_name_arr_live['nns_name'] : '';
                $task_name = $live_name_temp . '] [' . $task_name_arr['nns_begin_time'] . '] [' . $task_name_arr['nns_name'];
                $nns_cp_id = (strlen($task_name_arr['nns_cp_id']) > 0) ? $task_name_arr['nns_cp_id'] : 0;
                $asset_id = $video_id;
                $str_import_id = $task_name_arr['nns_playbill_import_id'];
                $nns_ext_url = $task_name_arr['nns_ext_url'];
                $nns_media_encode_flag = 0;
                $nns_ext_info = '';
                $nns_tag = '';
                break;
            case BK_OP_FILE:
                $task_name_arr = nl_db_get_one("select nns_name,nns_cp_id,nns_import_id,nns_ext_url from nns_file_package where nns_id='{$video_id}'", $db);
                $task_name = $task_name_arr['nns_name'];
                $nns_cp_id = (strlen($task_name_arr['nns_cp_id']) > 0) ? $task_name_arr['nns_cp_id'] : 0;
                $asset_id = $video_id;
                $str_import_id = $task_name_arr['nns_import_id'];
                $nns_ext_url = $task_name_arr['nns_ext_url'];
                $nns_ext_info = '';
                $nns_media_encode_flag = 0;
                $nns_tag = '';
                break;
            case BK_OP_PRODUCT:
                $task_name_arr = nl_db_get_one("select nns_order_name as nns_name,nns_cp_id,nns_order_number as nns_import_id from nns_product where nns_id='{$video_id}'", $db);
                $task_name = $task_name_arr['nns_name'];
                $nns_cp_id = (strlen($task_name_arr['nns_cp_id']) > 0) ? $task_name_arr['nns_cp_id'] : 0;
                $asset_id = $video_id;
                $str_import_id = $task_name_arr['nns_import_id'];
                $nns_ext_url = '';
                $nns_ext_info = '';
                $nns_media_encode_flag = 0;
                $nns_tag = '';
                break;
            case  BK_OP_SEEKPOINT:
                $task_name_arr = nl_db_get_one("select * from nns_vod_index_seekpoint where nns_id='{$video_id}'", $db);
                $task_name = $task_name_arr['nns_name'];
                $nns_cp_id = (strlen($task_name_arr['nns_cp_id']) > 0) ? $task_name_arr['nns_cp_id'] : 0;
                $asset_id = $video_id;
                $str_import_id = $task_name_arr['nns_import_id'];
                $nns_ext_url = '';
                $nns_ext_info = '';
                $nns_media_encode_flag = 0;
                $nns_tag = '';
                break;
        }
        if ($video_type == BK_OP_VIDEO || $video_type == BK_OP_PLAYBILL || $video_type == BK_OP_FILE || $video_type == BK_OP_LIVE)
        {
            $task_name = '[' . $task_name . ']';
        } else if ($video_type == BK_OP_LIVE_MEDIA)
        {
            $video_name = nl_db_get_col("select nns_name from nns_live where nns_id='{$asset_id}'", $db);
            $task_name = '[' . $video_name . '] ' . $task_name;
        } else
        {
            $video_name = nl_db_get_col("select nns_name from nns_vod where nns_id='{$asset_id}'", $db);
            $task_name = '[' . $video_name . '] ' . $task_name;
        }
        $array = array(
            'task_name' => $task_name,
            'asset_id' => $asset_id,
            'nns_kbps' => $nns_kbps,
            'cp_id' => $nns_cp_id,
            'ex_data' => $ex_data,
            'import_id' => $str_import_id,
            'nns_ext_url' => $nns_ext_url,
            'nns_ext_info' => $nns_ext_info,
            'nns_media_encode_flag' => $nns_media_encode_flag,
            'nns_tag' => $nns_tag,
            'nns_url' => $nns_url,
            'nns_live_to_media' => $nns_live_to_media,
            'nns_filetype' => $nns_filetype,
        );
        /*print_r($array);die;*/
        return $array;
    }

    public function status($message_id)
    {

    }

    /**
     * 消息队列注入播控统一反馈给芒果二级，组装消息结构
     * @param int $state
     * @param string $reason
     * @param array $info
     */
    public function create_xml_respond_msg_to_csp($state=0, $reason='',$info=array())
    {
        \m_config::write_message_execute_log($reason, $this->source_id);
        $site_url = $this->arr_cp_config['data_info']['nns_config']['site_callback_url'];

        \m_config::write_callback_log('[' . date('Y-m-d H:i:s') . ']' . '反馈到上游地址信息：站点地址为-' . $site_url, $this->source_id, $this->msg_id);

        //反馈状态转换
        $state = $state== 0 ?  0 : -1;
        if(!empty($site_url))
        {
            $params = array(
                'msgid' => $this->msg_id,
                'CmsResult' => $state,
                'ResultMsg' => $reason
            );
            if(!empty($info))
            {
                $params['info'] = $info;
            }
            $this->notify_msg($params,true,$site_url);
        }
        else
        {
            \m_config::write_callback_log('[' . date('Y-m-d H:i:s') . ']' . '没有配置站点信息', $this->source_id, $this->msg_id);
        }
    }

    /**
     * @description:播控注入cdn后统一反馈给二级
     * @author:xinxin.deng
     * @date: 2018/3/2 9:59
     * @param $message_id //消息id
     * @param $code //状态码，失败1，成功0
     * @param $reason //原因描述
     * @param $arr_data //封装好了的反馈信息 array(
     *                                          'cdn_id' => ,注入cdn的id
     *                                          'site_id' => ,配置的站点id
     *                                          'mg_asset_type' => ,注入媒资类型
     *                                          'mg_asset_id' => ,主媒资的消息注入id
     *                                          'mg_part_id' => ,分集的消息注入id
     *                                          'mg_file_id' => ,片源的消息注入id,
     *                                          'is_finally' =>0, 0表示最终状态,反馈的时候将其unset掉
     *                                          );
     * @param $sp_id //sp ID
     */
    public function is_ok($message_id, $code, $reason, $arr_data, $sp_id)
    {
        $arr_sp_data = \m_config::_get_sp_info($sp_id);
        $sp_config = isset($arr_sp_data['data_info']['nns_config']) ? $arr_sp_data['data_info']['nns_config'] : array();
        //不是最终状态不进行反馈
        if (($code == 5 || $code == 98 || $code === 0) && (!isset($arr_data['is_finally']) || $arr_data['is_finally'] !== 0 || $arr_data['is_finally'] != null))
        {
            \m_config::write_callback_log('不是最终成功状态不进行反馈', $sp_id, $message_id);
            return;
        }
        unset($arr_data['is_finally']);
        //兼容湖南电信二级消息反馈，1表示成功，0表示失败。
        $code = $code === 0 ? 1 : 0;
        $flag_message_feedback_enabled = (!isset($sp_config['message_feedback_enabled']) || $sp_config['message_feedback_enabled'] !=1) ? true : false;
        if(!$flag_message_feedback_enabled || !isset($sp_config['message_feedback_mode']) || $sp_config['message_feedback_mode'] != 0)
        {
            \m_config::write_callback_log('[' . date('Y-m-d H:i:s') . ']' . '不允许反馈或者反馈模式不正确：
            [message_feedback_enabled]=' . $sp_config['message_feedback_enabled'] . 'message_feedback_mode=' . $sp_config['message_feedback_mode'], 'szlt', $message_id);
            return;
        }

        //查询sp的上报地址/芒果使用站点ID
        if(isset($sp_config['site_callback_url']) && strlen($sp_config['site_callback_url']) > 0)
        {
            $xml_info = array(
                'msgid' => $message_id,
                'state' => $code,
                'msg' => $reason,
                'info' => $arr_data,
            );


            $xml = self::_build_notify_xml($xml_info);
            $data = array(
                'cmsresult' => $xml,
            );
            \m_config::write_callback_log('[' . date('Y-m-d H:i:s') . ']' . '站点反馈地址为:' . var_export($sp_config['site_callback_url'], true) . ',反馈
            数据为:' . $xml, 'szlt', $message_id);

            $http_curl = new np_http_curl_class();
            for ($i = 0; $i < 3; $i++) {
                //访问媒资注入接口
                $http_header = array("Content-Type: text/xml");
                $re = $http_curl->post($sp_config['site_callback_url'], $data, $http_header, 2);
                $curl_info = $http_curl->curl_getinfo();
                $http_code = $curl_info['http_code'];
                \m_config::write_callback_log('[' . date('Y-m-d H:i:s') . ']' . '循环请求第' . $i . '次(如果请求返回post结果为true并且状态码不是200,就会默认循环3次,状态码为200,
                就跳出循环),' . '请求返回post结果为:' . $re . '请求返回状态为:' . var_export($http_code, true) . ',反回数据为:' . var_export($curl_info, true) . ',
                访问POST结果为:' . var_export($re, true), 'szlt', $message_id);

                if ($http_code != 200 && (int)$re === 1)
                {
                    continue;
                }
                else
                {
                    break;
                }
            }
        }
        return;
    }


    /**
     * @description:组装反馈的XML
     * @author:xinxin.deng
     * @date: 2018/2/26 14:25
     * @param $params
     * @return string|void
     */
    public function _build_notify_xml($params)
    {
        if(!is_array($params) || empty($params))
        {
            return ;
        }
        $dom = new DOMDocument('1.0', 'utf-8');
        $xmlresult = $dom->createElement('xmlresult');
        $dom->appendChild($xmlresult);
        foreach ($params as $key=>$value)
        {
            $$key = $dom->createElement($key);
            $xmlresult->appendchild($$key);
            if(!empty($value) && is_array($value))
            {
                foreach ($value as $k=>$val)
                {
                    $$k = $dom->createElement($k);
                    $$key->appendchild($$k);
                    //创建元素值
                    $content = $dom->createTextNode($val);
                    $$k->appendchild($content);
                }
            }
            else
            {
                //创建元素值
                $text = $dom->createTextNode($value);
                $$key->appendchild($text);
            }
        }
        return $dom->saveXML();
    }


    /**
     * 进行消息反馈
     * @param $params
     * @param bool $bool
     * @param string $site_url
     * @return array|string|void
     */
    public function notify_msg($params, $bool=false, $site_url='')
    {
        $http_curl = new np_http_curl_class();
        $xml = $this->_build_notify_xml($params);
        \m_config::write_callback_log( '[' . date('Y-m-d H:i:s') . ']' . '反馈给上游的数据为：' . var_export($xml, true), $this->source_id, $this->msg_id);
        if(!$bool)//是否异步反馈通知上游
        {
            return $xml;
        }
        if(!empty($site_url))
        {
            $xml = array('cmsresult '=> $xml);
            $http_header = array("Content-Type: text/xml");
            $response = $http_curl->post($site_url, $xml, $http_header,2);
            $curl_info = $http_curl->curl_getinfo();
            \m_config::write_callback_log( '[' . date('Y-m-d H:i:s') . ']' . '上游接收到反馈后响应的结果为：' . $response . ',POST反馈数据为：' . var_export($curl_info, true), $this->source_id, $this->msg_id);
            if ($response === 0 )
            {
                \m_config::write_callback_log('[' . date('Y-m-d H:i:s') . ']' . 'post地址请求失败', $this->source_id, $this->msg_id);
                return;
            }
        }
        else
        {
            \m_config::write_callback_log('[' . date('Y-m-d H:i:s') . ']' . '没有配置反馈地址', $this->source_id, $this->msg_id);
        }
    }

    /**
     * 合并图片类型
     * @param $arr_img
     * @param $arr_temp
     * @return array
     */
    private function combine_picture($arr_img, $arr_temp)
    {
        $arr_img = array_values($arr_img);
        foreach ($arr_temp as $k=> $item)
        {
            $arr_img[$k-1] = $item;
        }
        return $arr_img;
    }
}