<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/9/25 11:38
 */
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.message.message_queue");
//http公共入口
class import extends \ns_model\message\message_queue
{
    /**
     * 注入消息队列模板
     * @var unknown
     */
    public $arr_in_message = array(
        'base_info'=>array(
            'nns_message_time'=>'', //上游消息时间  年月日时分秒毫秒
            'nns_message_id'=>'',  //上游消息ID
            'nns_cp_id'=>'', //上游CP标示
            'nns_message_xml'=>'',  //上游原始的信息  可以为字符串 | xml | json 或则存放消息地址
            'nns_message_content'=>'',  //上游解析后的信息  可以为字符串 | xml | json  生成队列的基本信息
            'nns_action'=>'', //操作 行为
            'nns_type'=>'', //消息 类型
            'nns_name'=>'',  //消息名称
            'nns_package_id'=>'',  //包ID
            'nns_xmlurlqc'=>'', //广州电信悦ME MD5摘要
            'nns_encrypt'=>'', //广州电信悦ME 加密串
            'nns_content_number'=>'', //xml文件中包含的内容数量
        ), //基本信息（存储于nns_mgtvbk_message表中）
    );

    public $in_params = array();
    public $str_cp_id = '';

    public function __construct()
    {
        $arr_dir = explode('|', str_replace(array ('/','\\'), '|', __DIR__));
        $this->str_cp_id = array_pop($arr_dir);

        \ns_core\m_load::load_np("np_xml_to_array.class.php");
        m_config::get_dc();
        \m_config::write_message_receive_log('-----------消息接收开始-----------', $this->str_cp_id);
        $data = $_POST;
        \m_config::write_message_receive_log("接收POST消息：". var_export($data, true), $this->str_cp_id);
        if(empty($data))
        {
            $post_params = file_get_contents('php://input');
            \m_config::write_message_receive_log("接收流文件消息：". var_export($post_params, true), $this->str_cp_id);
            $data =  $post_params;
        }
        $this->in_params = $data;
    }

    public function import()
    {
        //json数据
        $json_data = $this->in_params;
        if(empty($json_data))
        {
            \m_config::write_message_receive_log('POST数据为空', $this->str_cp_id);
            return false;
        }
        if (!m_config::is_json($json_data))
        {
            \m_config::write_message_receive_log('POST数据格式不正确,不是json数据', $this->str_cp_id);
            return false;
        }
        //消息ID
        $this->arr_in_message['base_info']['nns_message_id'] = np_guid_rand();
        $this->arr_in_message['base_info']['nns_message_time'] = date('YmdHis');
        $this->arr_in_message['base_info']['nns_cp_id'] = $this->str_cp_id;

        $json_arr_data = json_decode($json_data, true);
        if(!is_array($json_arr_data))
        {
            \m_config::write_message_receive_log('json数据解析失败', $this->str_cp_id);
            return false;
        }

        $this->arr_in_message['base_info']['nns_message_content'] = $json_data;

        //获取操作(新增,删除)1增加修改; 2 删除;
        $this->arr_in_message['base_info']['nns_action'] = 1;
        //1 主媒资 2分集 3片源
        $this->arr_in_message['base_info']['nns_type'] = 1;

        $params['name'] = '';
        if($this->arr_in_message['base_info']['nns_type'] == 1)
        {
            $this->arr_in_message['base_info']['nns_name'] = '';
        }
        elseif ($this->arr_in_message['base_info']['nns_type'] == 2)
        {
            $this->arr_in_message['base_info']['nns_name'] = '';
        }
        else
        {
            $this->arr_in_message['base_info']['nns_name'] = '';
        }
        $result = $this->push($this->arr_in_message);
        if($result['ret'] != 0)
        {
            return false;
        }
        return true;
    }
}

$http_import = new import();
$result_import = $http_import->import();
if($result_import)
{
    $re = array(
        'message' => '推送数据success',
        'code' => '0000',
        'result' => '',
    );
    echo json_encode($re);
}
else
{
    $re = array(
        'message' => '推送数据失败',
        'code' => '1006',
        'result' => '',
    );
    echo json_encode($re);
}