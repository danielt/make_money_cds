<?php
error_reporting(7);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.message.message_queue");
\ns_core\m_load::load("ns_model.message.message_explain");
\ns_core\m_load::load_old("nn_logic/queue/queue_redis.class.php");
//http公共入口
class http_import extends ns_model\message\message_queue
{
    public $str_cp_id = null;
    /**
     *  参数
     * @var unknown
     */
	public $mixed_params = null;

    private $int_num = 0;


    private $arr_vod_ids = array();
    private $arr_index_ids = array();
    private $arr_media_ids = array();
	
	/**
	 * 注入消息队列模板
	 * @var unknown
	 */
	public $arr_in_message = array(
                              'base_info'=>array(
                                      'nns_message_time'=>'', //上游消息时间  年月日时分秒毫秒
                                      'nns_message_id'=>'',  //上游消息ID
                                      'nns_cp_id'=>'', //上游CP标示
                                      'nns_message_xml'=>'',  //上游原始的信息  可以为字符串 | xml | json
                                      'nns_message_content'=>'',  //上游解析后的信息  可以为字符串 | xml | json  生成队列的基本信息
                                      'nns_action'=>'', //操作 行为
                                      'nns_type'=>'', //消息 类型
                                      'nns_name'=>'',  //消息名称
                                      'nns_package_id'=>'',  //包ID（只对天威用 后期废用）
                                      'nns_xmlurlqc'=>'', //广州电信悦ME MD5摘要
                                      'nns_encrypt'=>'', //广州电信悦ME 加密串
                                      'nns_content_number'=>'1', //xml文件中包含的内容数量
                                      'nns_message_state'=>0,
                              ), //基本信息（存储于nns_mgtvbk_message表中）
                     );
	
	/**
	 * 初始化
	 */
	public function __construct()
	{
	    $arr_dir = explode('|', str_replace(array ('/','\\'), '|', __DIR__));
	    $this->str_cp_id = array_pop($arr_dir);
	    //加载 xml转为数组的np库
	    \ns_core\m_load::load_np("np_xml_to_array.class.php");
	    \ns_core\m_load::load_np("np_xml2array.class.php");
	    //加载 xml转为数组的np库
	    \ns_core\m_load::load_old("nn_class/ftp/ftp.class.php");
	    //调用memcache
	    \ns_core\m_load::load_old("nn_logic/cache/view_cache.class.php");
	    //初始化obj DC
	    m_config::get_dc();
	}
	
	/**
	 * 注入
	 * @return multitype:number |string
	 */
	public function import()
	{
        $this->arr_in_message['base_info']['nns_cp_id'] = $this->str_cp_id;
	    $result_cp_config = m_config::_get_cp_info($this->str_cp_id);
	    if($result_cp_config['ret'] !=0)
	    {
	        return $result_cp_config;
	    }
	    if(!isset($result_cp_config['data_info']) || empty($result_cp_config['data_info']) || empty($result_cp_config['data_info']))
	    {
	        return m_config::return_data(1,"获取CP[".$this->str_cp_id."]信息为空");
	    }
	    $arr_cp_config = $result_cp_config['data_info'];
	    if(!isset($result_cp_config['data_info']['nns_config']['message_ftp_import_enable']) || $result_cp_config['data_info']['nns_config']['message_ftp_import_enable'] !='1')
	    {
	        return m_config::return_data(1,"获取CP[".$this->str_cp_id."]配置[message_ftp_import_enable]FTP消息注入开关关闭");
	    }
	    if(!isset($result_cp_config['data_info']['nns_config']['message_ftp_import_file_url']) || strlen($result_cp_config['data_info']['nns_config']['message_ftp_import_file_url']) <1)
	    {
	        return m_config::return_data(1,"获取CP[".$this->str_cp_id."]配置[message_ftp_import_file_url]FTP地址配置为空");
	    }
	    //ftp 文件的相对路径
        $message_ftp_import_file_url_base = $result_cp_config['data_info']['nns_config']['message_ftp_import_file_url'];
	    $message_ftp_import_file_url = trim($message_ftp_import_file_url_base,'/').'/'.$this->str_cp_id.'/';
	    $message_ftp_import_file_ex = (isset($result_cp_config['data_info']['nns_config']['message_ftp_import_file_ex']) && strlen($result_cp_config['data_info']['nns_config']['message_ftp_import_file_ex']) >0) ? $result_cp_config['data_info']['nns_config']['message_ftp_import_file_ex'] : 'xml';
        //列表缓存时间
	    $message_ftp_import_mem_time = (isset($result_cp_config['data_info']['nns_config']['message_ftp_import_mem_time']) && $result_cp_config['data_info']['nns_config']['message_ftp_import_mem_time'] >0) ? $result_cp_config['data_info']['nns_config']['message_ftp_import_mem_time'] : 1200;
	    //获取读取文件条数
	    $message_ftp_import_num = (isset($result_cp_config['data_info']['nns_config']['message_ftp_import_num']) && $result_cp_config['data_info']['nns_config']['message_ftp_import_num'] >0) ? $result_cp_config['data_info']['nns_config']['message_ftp_import_num'] : 100;
	    
	    $str_cache_key = nl_view_cache::gkey('message_ftp_import_file_url_'.$this->str_cp_id,array('message_ftp_import_file_url_'.$this->str_cp_id,__FILE__));
        $result_redis = nl_queue_redis::pop(m_config::get_redis_dc(), $str_cache_key);

        //验证是否需要扫描硬盘
        if($result_redis['ret'] != 0 || !isset($result_redis['data_info']) || empty($result_redis['data_info']))
        {
            return $this->ftp_get_files($message_ftp_import_file_url,$message_ftp_import_file_ex,$message_ftp_import_num,$str_cache_key);
        }
        else
        {
            $arr_file_list = json_decode($result_redis['data_info'],true);
        }
        unset($result_redis);
	    if(empty($arr_file_list) || !is_array($arr_file_list))
	    {
	        return m_config::return_data(1,"获取CP[".$this->str_cp_id."]ftp文件列表为空相对路径为[{$message_ftp_import_file_url}]");
	    }

        //遍历入库
        foreach ($arr_file_list as $file_key => $file_val)
        {
            $time_rand = microtime(true)*10000;
            //原始数据存储的字段

            $this->arr_in_message['base_info']['nns_message_id'] = md5($file_val);
            $this->arr_in_message['base_info']['nns_message_time'] = $time_rand;
            $result_content = m_config::get_curl_content($file_val);
            if($result_content['ret'] !=0)
            {
                //return $result_content;
                //echo ++ $this->int_num . ',';
                continue;
            }
            else if(!isset($result_content['data_info']) || strlen($result_content['data_info']) <1)
            {
                //return m_config::return_data(1,"获取CP[".$this->str_cp_id."]ftp文件内容为空，地址[{$file_val}]");
                m_config::return_data(1,"获取CP[".$this->str_cp_id."]ftp文件内容为空，地址[{$file_val}]");
                //echo ++ $this->int_num . ',';
                continue;
            }
            else
            {
                $this->arr_in_message['base_info']['nns_message_content'] = $result_content['data_info'];
            }
            $str_content = m_config::trim_xml_header($result_content['data_info']);
            if(!m_config::is_xml($str_content))
            {
                //return m_config::return_data(1,"消息xml内容非xml结构");
                m_config::return_data(1,"消息xml内容非xml结构");
                //echo ++ $this->int_num . ',';
                continue;
            }
            //解析下载的XML
            $dom = new DOMDocument('1.0', 'utf-8');
            $dom->loadXML($str_content);
            $xml = $dom->saveXML();
            $xml_arr = np_xml2array::getArrayData($xml);
            if ($xml_arr["ret"] == 0)
            {
                //return m_config::return_data(1,"消息xml解析失败");
                m_config::return_data(1,"消息xml解析失败");
                //echo ++ $this->int_num . ',';
                continue;
            }
            $Objects = null;
            foreach ($xml_arr['data']['children'] as $v)
            {
                if (empty($Objects) && $v['name'] == 'Objects')
                {
                    $Objects = $v['children'];
                }
                else if (empty($this->arr_map) && $v['name'] == 'Mappings')
                {
                    $this->arr_map = $v['children'];
                }
            }
            $flag = true;
            if (!empty($Objects) && is_array($Objects))
            {
                foreach ($Objects as $obj)
                {
                    if ($obj['attributes']['ElementType'] == 'Series')
                    {
                        if(in_array($obj['attributes']['ID'],$this->arr_vod_ids))
                        {
                            $flag = false;
                            break;
                        }
                        $this->arr_vod_ids[] = $obj['attributes']['ID'];

                        $this->arr_in_message['base_info']['nns_type'] = '1';
                        $this->arr_in_message['base_info']['nns_action'] = ($obj['attributes']['Action'] == 'DELETE') ? 3 : (($obj['attributes']['Action'] == 'REGIST') ? 1 : 2);
                    }
                    else if ($obj['attributes']['ElementType'] == 'Program')
                    {
                        if(in_array($obj['attributes']['ID'],$this->arr_index_ids))
                        {
                            $flag = false;
                            break;
                        }
                        $this->arr_index_ids[] = $obj['attributes']['ID'];

                        $this->arr_in_message['base_info']['nns_type'] = '2';
                        $this->arr_in_message['base_info']['nns_action'] = ($obj['attributes']['Action'] == 'DELETE') ? 3 : (($obj['attributes']['Action'] == 'REGIST') ? 1 : 2);
                    }
                    else if ($obj['attributes']['ElementType'] == 'Movie')
                    {
                        if(in_array($obj['attributes']['ID'],$this->arr_media_ids))
                        {
                            $flag = false;
                            break;
                        }

                        $temp_key_value_arr = $this->make_key_value_arr($obj);
                        $str_FileURL = isset($temp_key_value_arr['key_val_list']['FileURL']) ? trim(ltrim(ltrim($temp_key_value_arr['key_val_list']['FileURL'],'/'),'\\')) : '';
                        if(strlen($str_FileURL) <= 0)
                        {
                            $flag = false;
                            break;
                        }
                        $result = $this->check_file_exsist(trim(trim(trim($message_ftp_import_file_url_base,'/'),'\\')).'/'.$str_FileURL);
                        if($result['ret'] !=0)
                        {
                            $flag = false;
                            break;
                        }
                        $this->arr_in_message['base_info']['nns_type'] = '3';
                        $this->arr_in_message['base_info']['nns_action'] = ($obj['attributes']['Action'] == 'DELETE') ? 3 : (($obj['attributes']['Action'] == 'REGIST') ? 1 : 2);
                    }
                }
            }
            else
            {
                $flag = false;
            }
            if(!$flag)
            {
                //echo ++ $this->int_num . ',';
                continue;
            }
            $result = $this->push($this->arr_in_message);
            if($result['ret'] == '0')
            {
                //$this->del_ftp_file_and_dir($file_val,$message_ftp_import_file_ex);
                unset($arr_file_list[$file_key]);
            }
        }

        //nl_queue_redis::delete_key(m_config::get_redis_dc(), $str_cache_key);

        return m_config::return_data(0,'ok');
	}
	
	/**
	 * 获取ftp列表
	 * @param unknown $ftp_url
	 * @param string $file_ex
	 */
	private function ftp_get_files($ftp_url,$file_ex='xml',$message_ftp_import_num=100,$str_cache_key)
	{
	    $message_ftp_import_num = (int)$message_ftp_import_num>0 ? (int)$message_ftp_import_num : 100;
	    $arr_files = null;
	    $ftp_url = trim($ftp_url,'/');
	    $arr_ftp_config = parse_url($ftp_url);
	    $host_ftp = (isset($arr_ftp_config["host"]) && strlen($arr_ftp_config["host"]) > 0) ? $arr_ftp_config["host"] : '';
	    $user_ftp = (isset($arr_ftp_config["user"]) && strlen($arr_ftp_config["user"]) > 0) ? $arr_ftp_config["user"] : '';
	    $pass_ftp = (isset($arr_ftp_config["pass"]) && strlen($arr_ftp_config["pass"]) > 0) ? $arr_ftp_config["pass"] : '';
	    $path_ftp = (isset($arr_ftp_config["path"]) && strlen($arr_ftp_config["path"]) > 0) ? $arr_ftp_config["path"] : '';
	    $port_ftp = (isset($arr_ftp_config["port"]) && strlen($arr_ftp_config["port"]) > 0) ? $arr_ftp_config["port"] : 21;
	    $obj_ftp = new nl_ftp($host_ftp, $user_ftp, $pass_ftp,$port_ftp);
	    $result_conn = $obj_ftp->base_opration_ftp();
	    if(strlen($path_ftp) >0)
	    {
	        $ftp_url=trim(substr($ftp_url,0, (-strlen($path_ftp))),'/');
	    }
	    if(!$result_conn)
	    {
	        return m_config::return_data(1,"ftp链接失败,原因：".var_export($obj_ftp->make_error_data(),true));
	    }

        //获取可扫描的文件目录
        $str_path_ftp = $obj_ftp->get_valid_recursive_path($ftp_url,$path_ftp);
        if(empty($str_path_ftp))
        {
            return m_config::return_data(1,"ftp文件相对路径[{$ftp_url}]：文件目录均已被扫描或文件目录不存在任何文件");
        }
        $str_path_ftp = rtrim($str_path_ftp,'/') . '/';
        //扫描文件目录
        $result = $obj_ftp->ftp_recursive_file_listing($str_path_ftp,$file_ex);

	    if(empty($result) || !is_array($result))
	    {
	        return m_config::return_data(1,"未查询到任何文件信息,相对路径[{$ftp_url}]");
	    }
	    foreach ($result as $key=>$val)
	    {
	        foreach ($val as $file_val)
	        {
	            $temp_file_url = pathinfo($key.'/'.$file_val);
	            if(!isset($temp_file_url['extension']) || strtolower($temp_file_url['extension']) !=$file_ex)
	            {
	                continue;
	            }
	            $arr_files[] = $ftp_url .'/'.trim($key.'/'.trim($file_val,'/'),'/');
	        }
	    }
	    if(is_array($arr_files) && !empty($arr_files))
	    {
	        $arr_files = array_chunk($arr_files,$message_ftp_import_num);
	    }
	    if(is_array($arr_files) && !empty($arr_files))
	    {
	        foreach ($arr_files as $val_file)
	        {
	            if(empty($val_file) || !is_array($val_file))
	            {
	                continue;
	            }
	            $val_file = json_encode($val_file);
	            $result = nl_queue_redis::push(m_config::get_redis_dc(), $str_cache_key, $val_file);
	            if($result['ret'] !=0)
	            {
	                return $result;
	            }
	        }
	    }

        unset($arr_files);
	    //更新已扫描的标志位状态
        $obj_ftp->modify_valid_flag_file($str_path_ftp,1);

        return m_config::return_data(0,"OK,ftp文件相对路径[{$ftp_url}]");
	}
	
	/**
	 * 检查文件是否存在
	 * @param unknown $path
	 * @return multitype:number
	 */
	private function check_file_exsist($ftp_url)
	{
	    $ftp_url = trim($ftp_url,'/');
	    $arr_ftp_config = parse_url($ftp_url);
	    $host_ftp = (isset($arr_ftp_config["host"]) && strlen($arr_ftp_config["host"]) > 0) ? $arr_ftp_config["host"] : '';
	    $user_ftp = (isset($arr_ftp_config["user"]) && strlen($arr_ftp_config["user"]) > 0) ? $arr_ftp_config["user"] : '';
	    $pass_ftp = (isset($arr_ftp_config["pass"]) && strlen($arr_ftp_config["pass"]) > 0) ? $arr_ftp_config["pass"] : '';
	    $path_ftp = (isset($arr_ftp_config["path"]) && strlen($arr_ftp_config["path"]) > 0) ? $arr_ftp_config["path"] : '';
	    $port_ftp = (isset($arr_ftp_config["port"]) && strlen($arr_ftp_config["port"]) > 0) ? $arr_ftp_config["port"] : 21;
	    $obj_ftp = new nl_ftp($host_ftp, $user_ftp, $pass_ftp,$port_ftp);
	    $result = $obj_ftp->check_file_exsist($path_ftp);
	    unset($obj_ftp);
	    return $result;
	}
	
	/**
	 * 删除文件和空文件夹
	 * @param unknown $ftp_url
	 * @param string $file_ex
	 */
	private function del_ftp_file_and_dir($ftp_url,$file_ex='xml')
	{
	    $ftp_url = trim($ftp_url,'/');
	    $arr_ftp_config = parse_url($ftp_url);
	    $host_ftp = (isset($arr_ftp_config["host"]) && strlen($arr_ftp_config["host"]) > 0) ? $arr_ftp_config["host"] : '';
	    $user_ftp = (isset($arr_ftp_config["user"]) && strlen($arr_ftp_config["user"]) > 0) ? $arr_ftp_config["user"] : '';
	    $pass_ftp = (isset($arr_ftp_config["pass"]) && strlen($arr_ftp_config["pass"]) > 0) ? $arr_ftp_config["pass"] : '';
	    $path_ftp = (isset($arr_ftp_config["path"]) && strlen($arr_ftp_config["path"]) > 0) ? $arr_ftp_config["path"] : '';
	    $port_ftp = (isset($arr_ftp_config["port"]) && strlen($arr_ftp_config["port"]) > 0) ? $arr_ftp_config["port"] : 21;
	    $obj_ftp = new nl_ftp($host_ftp, $user_ftp, $pass_ftp,$port_ftp);
	    $arr_path_info = pathinfo($path_ftp);
	    if(!isset($arr_path_info['extension']) || strtolower($arr_path_info['extension']) != strtolower($file_ex))
	    {
	        return m_config::return_data(1,'文件扩展不符合要求不允许删除');
	    }
	    $delete_dir_num = 0;
	    if(isset($arr_path_info['dirname']))
	    {
	        $path_info_dirname = str_replace("\\", '/', $arr_path_info['dirname']);
	        $path_info_dirname = str_replace("//", '/', $path_info_dirname);
	        $path_info_dirname = trim(trim($path_info_dirname,'/'));
	        if(strlen($path_info_dirname) >0)
	        {
	            $arr_num_dir = explode('/', $path_info_dirname);
	            $delete_dir_num = (is_array($arr_num_dir) && !empty($arr_num_dir)) ? count($arr_num_dir) : 0;
	        }
	    }
	    $result = $obj_ftp->delete_file($path_ftp,true,$delete_dir_num);
	    unset($obj_ftp);
	    return $result;
	}
	
	/**
	 * 获取xml的 attr属性和key value值
	 * @param unknown $xml_obj_arr
	 * @return Ambigous <NULL, unknown, string>
	 */
	private function make_key_value_arr($xml_obj_arr)
	{
	    $key_val_array = null;
	    if (isset($xml_obj_arr['attributes']) && is_array($xml_obj_arr['attributes']) && !empty($xml_obj_arr['attributes']))
	    {
	        foreach ($xml_obj_arr['attributes'] as $attr_key => $attr_val)
	        {
	            $key_val_array[$attr_key] = $attr_val;
	        }
	        if(!isset($key_val_array['ContentID']) || strlen($key_val_array['ContentID'])<1)
	        {
	            $key_val_array['ContentID'] = isset($xml_obj_arr['attributes']['ID']) ? $xml_obj_arr['attributes']['ID'] : '';
	        }
	        unset($xml_obj_arr['attributes']);
	    }
	    if (isset($xml_obj_arr['children']) && is_array($xml_obj_arr['children']) && !empty($xml_obj_arr['children']))
	    {
	        foreach ($xml_obj_arr['children'] as $key_list)
	        {
	            if (isset($key_list['attributes']['Name']) && strlen($key_list['attributes']['Name']) > 0)
	            {
	                $key_val_array['key_val_list'][$key_list['attributes']['Name']] = (isset($key_list['content']) && strlen($key_list['content']) > 0) ? trim($key_list['content']) : '';
	            }
	        }
	        unset($xml_obj_arr['children']);
	    }
	    return $key_val_array;
	}
	
	/**
	 * 类销毁
	 */
	public function __destruct()
	{
	    m_config::base_write_log('message','request',"-----------消息接收结束-----------",m_config::return_child_path(__FILE__));
	}
}
$http_import = new http_import();
$result_import = $http_import->import();
echo json_encode($result_import);