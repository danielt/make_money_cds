<?php
/**
 * Created by PhpStorm.
 * User: yangkan
 * Date: 18-3-28
 * Time: 下午6:40
 */
set_time_limit(0);error_reporting(0);
header('Content-Type:text/xml; charset=utf-8');
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_core.m_config");
//加载 xml转为数组的np库
\ns_core\m_load::load_np("np_xml_to_array.class.php");
\ns_core\m_load::load_np("np_xml2array.class.php");
class notify_destory
{
    //数据库/缓存处理类
    private $obj_dc = null;
    //SP ID
    private $str_sp_id = 'coship';
    //CDN上报XML
    private $str_xml_content = '';

    /**
     * 默认构造函数
     */
    public function __construct($str_xml)
    {
        $this->obj_dc = m_config::get_dc();
        $this->str_xml_content = $str_xml;
    }
    /**
     * 处理CDN上报信息入口
     */
    public function init()
    {
        $bool_ret = false;
        //处理CDN反馈的信息
        $str_content = m_config::trim_xml_header($this->str_xml_content);
        if(m_config::is_xml($str_content))
        {
            $dom = new DOMDocument('1.0', 'utf-8');
            $dom->loadXML($str_content);
            $xml = $dom->saveXML();
            //转化XML为数组格式
            $xml_arr = np_xml2array::getArrayData($xml);
            if(is_array($xml_arr) && !empty($xml_arr) && !empty($xml_arr['data']))
            {
                //处理CDN上报信息核心函数
                $bool_ret = $this->_handler_feedback_content($xml_arr['data']);
            }
        }

        return $bool_ret;
    }

    /**
     * 处理CDN上报信息核心函数
     * @param $arr_content
     * @return bool true成功；false失败
     */
    private function _handler_feedback_content($arr_content)
    {
        //验证参数有效性
        $str_asset_id = isset($arr_content['attributes']['assetID']) ? $arr_content['attributes']['assetID'] : '';
        if(empty($str_asset_id))
        {
            return false;
        }
        //查询C2_TASK信息
        $str_where = 't.nns_type = \'media\' AND t.nns_ref_id = \'' . $str_asset_id . '\' AND UPPER(l.`nns_action`) = \'DELETE\'';
        $str_sql = 'SELECT l.nns_id FROM
                  nns_mgtvbk_c2_task t
                  INNER JOIN nns_mgtvbk_c2_log l
                    ON t.nns_id = l.nns_task_id
                WHERE ' . $str_where;
        $arr_task_info = nl_query_by_db($str_sql,$this->obj_dc->db());
        if(is_array($arr_task_info) && !empty($arr_task_info))
        {
            if(is_array($arr_content['children']) && !empty($arr_content['children']))
            {
                //引入文件
                include_once dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))) . '/mgtv_v2/mgtv_init.php';
                include_once dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))) . '/mgtv_v2/models/c2_task_model.php';
                //更新C2日志
                $int_state = '-1';
                foreach($arr_content['children'] as $children)
                {

                    $int_state = isset($children['attributes']['state']) && strtolower($children['attributes']['state']) == 'complete' ? '0' : '-1';
                    //更新C2队列任务结果
                    foreach($arr_task_info as $log)
                    {
                        c2_task_model::save_c2_notify(array(
                            'nns_id' => $log['nns_id'],
                            'nns_notify_result' => $int_state,
                            'nns_notify_result_url' => '',
                        ));
                    }
                }
            }
        }
        return true;
    }
}

//获取客户端数据流
$str_xml_content = file_get_contents("php://input");
m_config::base_write_log('message','request',date('Y-m-d H:i:s') . ' 注入删除CDN片源媒体内容反馈信息：' . addslashes($str_xml_content));
//处理CDN上报内容
$obj_notify_destory = new notify_destory($str_xml_content);
$obj_notify_destory->init();
//释放资源
unset($str_xml_content); unset($obj_notify);