<?php
/**
 * author  zhihong.yin
 */
error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_model.message.message_explain");
\ns_core\m_load::load("ns_core.m_pinyin");

class message extends \ns_model\message\message_explain
{
    private $sim_obj;

    //主媒资数据
    private $arr_series;

    //分集数据
    private $arr_index;

    //片源数据
    private $arr_media;

    //分集打点数据
    private $arr_index_seekpoint;

    //原始message
    private $message;

    //打点frametype
    private $arr_seekpoint_frametype_map =array(
        'medium'=>0,
        'head'=>1,
        'end'=>2,
        'interact'=>3
    );

    //language类型
    private $arr_language_map = array(
        'zh'=>'中文',
        'en'=>'英文',
        'ja'=>'日文',
        'fr'=>'法文',
        'de'=>'德文',
        'ko'=>'朝鲜文'
    );

    //海报类型
    private $arr_series_poster = array (
            'bigpic' => '',
            'middlepic' => '',
            'smallpic' => '',
            'horizontal_img' => '',
            'verticality_img' => '',
            'square_img' => '',
        );

    //分集海报
    private $arr_index_poster;

    //一级分类,影片种类
    private $category_map = array (
        '0' => '电影',  //电影
        '1' => '电视剧',  //电视剧
        '2' => '娱乐,综艺',  //综艺
        '3' => '动画',  //动漫
        '4' => '音乐',  //音乐
        '5' => '纪实,纪录片',  //
        '6' => '法治,科教',  //教育
        '7' => '体育',  //体育
        '8' => '生活',  //生活
        '9' => '经济',  //财经
        '10' => '特别节目',//微电影
        '11' => '品牌专区',//品牌专区
        '12' => '广告',  //广告
        '13' => '新闻', //新闻
        '14' => '公开课',
        '15' => '外语及其他语言',
        '16' => '青少年',
        '17' => '播客',
    );

	public function explain($message)
    {
        //nns_message_url 不为空，优先从nns_message_url获取内容，否则直接从nns_message_content返回内容
        $content = $this->get_message_content($message);

        $this->message = $message;

        if ($content['ret'] != 0 || strlen($content['data_info']) < 1)
        {
            return $content;
        }
        $str_content = m_config::trim_xml_header($content['data_info']);
        if (!m_config::is_xml($str_content))
        {
            return m_config::return_data(1, "消息GUID[{$message['nns_id']}]xml内容非xml结构");
        }

        //解析xml
        $this->sim_obj = simplexml_load_string($str_content);
        $arr_sim = json_decode(json_encode($this->sim_obj->xpath('Asset/Metadata')), true);

        //共同的metadate部分
//        $arr_common_metadate = $this->handle_metadate(json_decode(json_encode($this->sim_obj->xpath('Metadata')), true));

        /**********主媒资数据start************/
        //主媒资medadata
        $arr_series_metadata = $this->handle_metadate($arr_sim)[0];

        //language部分   ->查找属性值=language的节点的
        $arr_series_language = $this->handle_metadate(json_decode(json_encode($this->sim_obj->xpath('Asset/Asset[1]/Metadata')), true))[0];

        //海报，如果是多集，主媒资存在Content节点就是主媒资海报；如果是单集，片源与海报同级
        $arr_series_poster = json_decode(json_encode($this->sim_obj->xpath('Asset//AMS[contains(@Asset_Class,"poster")]/../following-sibling::Content')), true);

        $this->handle_poster($arr_series_poster, 'series');

        $this->arr_series = array_merge( $arr_series_language, $this->arr_series_poster, $arr_series_metadata);

        /**********主媒资数据end************/

        /*************主媒资操作start**********/
        if (!empty($this->arr_series))
        {
            if (isset($this->arr_series['Verb']) && strtoupper($this->arr_series['Verb']) == 'DELETE')
            {
                $del_series = array(
                    'base_info' => array(
                        'nns_asset_import_id' => $message['nns_message_id'],
                        'nns_import_source' => env::get('project'),
                        'nns_cp_id' => $message['nns_cp_id'],
                    ),
                );
                $this->vod_action('delete', $del_series);
            }
            else
            {
                //增加或修改

                //增加栏目信息
                if (!isset($arr_series['Category']))
                {
                    $this->arr_series['Category'] = "电影";
                }

                $this->arr_series['view_type'] = array_search($this->arr_series['Category'], $this->category_map);

                //栏目处理
                $result = $this->do_category($this->arr_series, 'add');

                if ($result['ret'] != 0)
                {
                    return $result;
                }

                if (!isset($result['data_info']['base_info']['nns_id']) || strlen($result['data_info']['base_info']['nns_id']) < 1)
                {
                    return m_config::return_data(1, '创建栏目失败');
                }
                $this->arr_series['nns_category_id']=$result['data_info']['base_info']['nns_id'];
                $re = $this->do_series($this->arr_series, 'add');
            }
        }
        /*************主媒资操作end**********/

        /**********分集数据+分集打点start************/
        $this->arr_index = $this->sim_obj->xpath('Asset[2]/Metadata');
        if (!empty($this->arr_index))
        {
            //分集metedata
            $this->arr_index = $this->handle_metadate(json_decode(json_encode($this->arr_index), true))[0];

            //分集language
            $arr_index_language = $this->sim_obj->xpath('Asset[2]//AMS[contains(@Asset_Class,"language")]/..');
            if (!empty($arr_index_language))
            {
                $arr_index_language = json_decode(json_encode($arr_index_language), true);
                $this->arr_index = array_merge($arr_index_language, $this->arr_index);
            }

            //分集海报
            $arr_index_poster = json_decode(json_encode($this->sim_obj->xpath('Asset//Content')), true);
            if ($arr_index_poster)
            {
                //设置$this->arr_index_poster
                $this->handle_poster($arr_index_poster, 'index');
                //以逗号分割存储
                $this->arr_index_poster = implode(',', $this->arr_index_poster);
            }


            #分集打点数据都在metadata中
            $arr_index_seekpoint = json_decode(json_encode($this->sim_obj->xpath('Asset[2]//AMS[contains(@Asset_Class,"seekpoint")]/..')), true);
            if ($arr_index_seekpoint)
            {
                //二维数组的形式
                $this->arr_index_seekpoint = $this->handle_metadate($arr_index_seekpoint);
            }
        }

        /**********分集数据end************/

        /***********分集操作start***************/
        if (empty($this->arr_index))
        {
            //是否为片源操作
            if (isset($message['nns_type']) && $message['nns_type'] == 3)
            {
                $this->arr_index['Asset_ID'] = $nns_import_id = np_guid_rand();
                //构造虚拟分集
                $program = array(
                    'base_info' => array(
                        'nns_import_id' => $nns_import_id,
                        'nns_video_import_id'=>$this->arr_series['Asset_ID'],
                        'nns_index'=>0,
                        'nns_release_time'=>$arr_series_language['Creation_Date'],
                        'nns_update_time'=>date('Y-m-d'),
                        'nns_import_source' => evn::get("project"),
                        'nns_cp_id' => $message['nns_cp_id'],
                    ),
                );

                $re = $this->index_action('add', $program);
                if ($re['ret'] != 0)
                {
                    $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因" . $re['reason']);
                    $re['ret'] = 1;
                    return $re;
                }
            }
        } else
        {
            if (isset($this->arr_index['Verb']) && strtoupper($this->arr_index['Verb']) == 'DELETE')
            {
                //分集删除
                $del_program = array(
                    'base_info' => array(
                        'nns_import_id' => $this->arr_index['Asset_ID'],
                        'nns_video_import_id'=>$this->arr_series['Asset_ID'],
                        'nns_import_source' => evn::get("project"),
                        'nns_cp_id' => $message['nns_cp_id'],
                    ),
                );
                $re = $this->index_action('delete', $del_program);
                if ($re['ret'] != 0)
                {
                    $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因" . $re['reason']);
                    $re['ret'] = 1;
                    return $re;
                }
            } else
            {
                //分集增加和修改
                $re = $this->do_index($this->arr_index, 'add');
                if ($re['ret'] != 0)
                {
                    $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因" . $re['reason']);
                    $re['ret'] = 1;
                    return $re;
                }
            }
        }
        /***********分集操作end****************/


        /**********片源数据start************/
        //片源
        $arr_media = $this->sim_obj->xpath('//AMS[contains(@Asset_Class,"movie")]/../..');
        if (!empty($arr_media))
        {
            $arr_media = json_decode(json_encode($arr_media), true);

            $arr_media_matedata = $this->handle_metadate([$arr_media[0]['Metadata']])[0];
            //片源url
            $this->arr_media['media_url'] = $arr_media[0]['Content']['@attributes']['Value'];

            if( !empty($this->arr_media['media_url']) )
            {
                $arr_file_info = pathinfo($this->arr_media['media_url']);
                $this->arr_media['media_file_type'] = isset($arr_file_info['extension'])?$arr_file_info['extension']:'ts';
            }

            $this->arr_media = array_merge($this->arr_media, $arr_media_matedata);
        }
        /**********片源数据end************/


        /***********片源操作start***************/
        if (!empty($this->arr_media))
        {
            if (isset($this->arr_media['Verb']) && strtoupper($this->arr_media['Verb']) == 'DELETE')
            {
                $del_movie = array(
                    'base_info' => array(
                        'nns_import_id' => $this->arr_series['Asset_ID'],
                        'nns_import_source' => evn::get("project"),
                        'nns_cp_id' => $message['nns_cp_id'],
                    ),
                );
                $re = $this->media_action('delete', $del_movie);
                if ($re['ret'] != 0)
                {
                    $re['ret'] = 1;
                    $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因" . $re['reason']);
                    return $re;
                }
            } else
            {
                //分集增加和修改
                $re = $this->do_media($this->arr_media, 'add');
                if ($re['ret'] != 0)
                {
                    $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因" . $re['reason']);
                    $re['ret'] = 1;
                    return $re;
                }
            }
        }
        /***********片源操作end****************/


        /***********分集打点操作start***************/
        if (!empty($this->arr_index_seekpoint) && is_array($this->arr_index_seekpoint))
        {
            if (isset($this->arr_index_seekpoint['Verb']) && strtoupper($this->arr_index_seekpoint['Verb']) == 'DELETE')
            {
                $del_movie = array(
                    'base_info' => array(
                        'nns_import_id' => $this->arr_series['Asset_ID'],
                        'nns_import_source' => evn::get("project"),
                        'nns_cp_id' => $message['nns_cp_id'],
                    ),
                );
                $re = $this->seekpoint_action('delete', $del_movie);
                if ($re['ret'] != 0)
                {
                    $re['ret'] = 1;
                    $this->create_xml_respond_msg_to_csp(NS_CDS_FAIL, "消息接收成功,注入失败,消息ID[{$message['nns_message_id']}],原因" . $re['reason']);
                    return $re;
                }
            } else
            {
                //seskpoint增加和修改
                foreach ($this->arr_index_seekpoint as $seekpoint)
                {
                    if (!empty($seekpoint['SeekPicUrl']))
                    {
                        $result = $this->handle_pic($seekpoint['SeekPicUrl']);
                        $seekpoint['SeekPicUrl'] = $result[2];
                    }
                    $re = $this->do_seekpoint($seekpoint, 'add');
                }
            }
        }
        return $re;
    }
        /***********分集打点操作end****************/

    /**
     * @param $arr_poster_all
     * array(3) {
            [0]=>
                array(1) {
                ["@attributes"]=>
                array(1) {
                ["Value"]=>
                string(115) "ftp://image_jscn:image_jscn@172.26.2.17:21//prev/KsImg/cp_fuse_xiaole/20171129/5a1e22f429ed0efdf1d2b252974ca8cb.jpg"
                }
            }
            [1]=>
                array(1) {
                ["@attributes"]=>
                array(1) {
                ["Value"]=>
                string(115) "ftp://image_jscn:image_jscn@172.26.2.17:21//prev/KsImg/cp_fuse_xiaole/20171129/5a1e22f42a1dc7108c8bf156972c59a8.jpg"
                }
            }
            [2]=>
                array(1) {
                ["@attributes"]=>
                array(1) {
                ["Value"]=>
                string(115) "ftp://image_jscn:image_jscn@172.26.2.17:21//prev/KsImg/cp_fuse_xiaole/20171129/5a1e22f42a38dcd9d945e427d4f60678.jpg"
                }
            }
    }
     * @param $type
     *
     */
    private function handle_poster($arr_poster_all,$type)
    {
        if( is_array($arr_poster_all) && $arr_poster_all )
        {   $arr_poster = [];
            foreach( $arr_poster_all as $poster )
            {
                $arr_poster = array_merge($arr_poster,array_column($poster,'Value'));
            }
        }

        if( !empty($arr_poster) && is_array($arr_poster ) )
        {
            //下载图片
            foreach( $arr_poster as $pic )
            {
//                $re = $this->handle_pic($pic);
                    if( $type == 'series')
                    {
                        //主媒资海报
                        foreach( $this->arr_series_poster as $index => $url_pic )
                        {
                            if( empty($url_pic ) )
                            {
                                $this->arr_series_poster[$index] = $pic;
                                break;
                            }
                        }
                    }
                    else if( $type == 'index')
                    {
                        //分集海报
                        $this->arr_index_poster[]=$pic;
                    }
                }
            }
    }

    /**
     * @param $pic_url  网络图片下载到本地
     * @return array
     */
    private function handle_pic($pic_url)
    {
        $arr_path  = pathinfo($pic_url);
        $extension = isset($arr_path['extension'])?$arr_path['extension']:'jpg';
        $path_pic =dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))."/data/download/pic/{$this->message['nns_cp_id']}/".date('Ymd');
        $path_pic = str_replace('\\','/',$path_pic);
        $name_pic = np_guid_rand().'.'.$extension;
        if( !is_dir($path_pic) )
        {
            mkdir($path_pic,0777,true);
        }

        @$re =  file_put_contents($path_pic.'/'.$name_pic,file_get_contents($pic_url));
        return [$re,$path_pic.'/'.$name_pic];
    }
    /*
     *
array (
  0 =>
  array (
    'AMS' =>
    array (
      '@attributes' =>
      array (
        'Verb' => '',
        'Asset_Class' => 'title',
        'Asset_ID' => 'JSBSDK00PA0000181069',
        'Asset_Name' => '女排世锦赛：非洲“姐妹”联手创历史',
        'Provider_ID' => 'JSBSDK',
        'Provider' => 'JSBSDK',
        'Creation_Date' => '2018-11-07',
        'Description' => '两支非洲球队喀麦隆和肯尼亚双双奏凯，完成了在世锦赛的历史性突破。',
        'Version_Major' => '1',
        'Version_Minor' => '0',
        'Product' => 'MOD',
      ),
    ),
    'App_Data' =>
    array (
      0 =>
      array (
        '@attributes' =>
        array (
          'Value' => 'program',
          'Name' => 'Show_Type',
          'App' => 'MOD',
        ),
      ),
      1 =>
      array (
        '@attributes' =>
        array (
          'Value' => '0',
          'Name' => 'Issue_Number',
          'App' => 'MOD',
        ),
      ),
      2 =>
      array (
        '@attributes' =>
        array (
          'Value' => 'JSBSDK00PA0000181069',
          'Name' => 'Original_Asset_ID',
          'App' => 'MOD',
        ),
      ),
      ),
    ),
  ),
)
     * */
	private function handle_metadate($array)
    {
        foreach( $array as $arr )
        {
            //AMS节点部分
            $arr_AMS = isset($arr['AMS']['@attributes'])?$arr['AMS']['@attributes']:[];

            //app_data部分
            if( isset($arr['App_Data']) )
            {
                $arr_temp = array_column($arr['App_Data'], '@attributes');

                $arr_app_data = array_column($arr_temp, 'Value', 'Name');
            }
            else
            {
                $arr_app_data =[];
            }

            $arr_res[] = array_merge($arr_AMS,$arr_app_data);
        }
        //返回一个二维数组
        return $arr_res;
    }
    
    //栏目操作
    public  function do_category($param,$action)
    {
        $do_category = array(
            'base_info'=>array(
                'nns_name'=>$param['Category'],   //栏目名称支持多层目录，多层目录的时候 /分割
                'nns_import_category_id'=>'', //栏目名称支持多层目录，多层目录的时候 /分割
                'nns_cp_id'=>$this->arr_series['Provider_ID'],
                'nns_import_parent_category_id'=>'',
                'nns_video_type'=>'0',  //媒资类型  0 点播 | 1 直播
            ),
        );
        return $this->category_action($action, $do_category);
    }
    //主媒资操作
    private function do_series($param,$action)
    {
        $pinyin = m_pinyin::get_pinyin_letter($param['Asset_Name']);
        $add_series = array(
            'base_info'=>array(
                'nns_name'=>isset($param['Asset_Name']) ? $param['Asset_Name'] : '',
                'nns_view_type'=>$param['view_type'],
                'nns_org_type'=>'0',
                'nns_tag'=>'26,',
                'nns_director'=>isset($param['Director']) ? $param['Director'] : '',
                'nns_actor'=>isset($param['Actors']) ? $param['Actors'] : '',
                //上映时间
                'nns_show_time'=>isset($param['Creation_Date']) ? $param['Creation_Date']:date("Y"),
                //时长
                'nns_view_len'=> 0,
                'nns_all_index'=>isset($param['Volumn_Count']) ? $param['Volumn_Count']:1,
                'nns_new_index'=>isset($param['New_Count']) ? $param['New_Count']:1,
                'nns_area'=>'',
                'nns_image0'=>isset($this->arr_series_poster['bigpic'])?$this->arr_series_poster['bigpic']:'',
                'nns_image1'=> isset($this->arr_series_poster['middlepic'])?$this->arr_series_poster['middlepic']:'',
                'nns_image2'=>isset($this->arr_series_poster['smallpic'])?$this->arr_series_poster['smallpic']:'',
                'nns_image3'=>'',
                'nns_image4'=>'',
                'nns_image5'=>'',
                'nns_summary'=>isset($param['Description']) ? $param['Description']:1,
                'nns_remark'=>isset($param['Subordinate_Title']) ? $param['Subordinate_Title']:1,
                'nns_category_id' => $param['nns_category_id'],
                'nns_play_count'=>'0',
                'nns_score_total'=>'0',
                'nns_score_count'=>'0',
                'nns_point'=>'',
                'nns_copyright_date'=>isset($param['Subscriber_View_Limit']) ? $param['Subscriber_View_Limit']:date("Y"),
                'nns_asset_import_id'=>$param['Asset_ID'],
                'nns_pinyin'=>isset($param['spell']) ? $param['spell']:$pinyin,
                'nns_pinyin_length'=>(isset($param['Spell']) && strlen($param['Spell'])>0) ? strlen($param['Spell']) : strlen($pinyin),
                'nns_alias_name'=>isset($param['AliasName']) ? $param['AliasName'] : '',
                'nns_eng_name'=>isset($param['EnglishName']) ? $param['EnglishName'] : '',
                'nns_language'=>isset($this->arr_language_map[$param['Language_Type']]) ? $this->arr_language_map[$param['Language_Type']] : '中文',
                'nns_text_lang'=>isset($this->arr_language_map[$param['Language_Type']]) ? $this->arr_language_map[$param['Language_Type']] : '中文',
                'nns_producer'=>isset($param['Producers']) ? $param['Producers'] : '',
                'nns_screenwriter'=>isset($param['WriterDisplay']) ? $param['WriterDisplay'] : '',
                'nns_play_role'=>'',
                'nns_copyright_range'=>isset($param['Producers']) ? $param['Producers'] : '',
                'nns_vod_part'=>'',
                'nns_keyword'=>isset($param['Keywords']) ? $param['Keywords'] : '',
                'nns_import_source'=>isset($param['nns_import_source']) ? $param['nns_import_source'] : evn::get("project"),
                'nns_kind'=>isset($param['Kind']) ? $param['Kind'] : '',
                'nns_copyright'=>isset($param['Producers']) ? $param['Producers'] : '',
                'nns_clarity'=>'',
                'nns_image_v'=>isset($this->arr_series_poster['verticality_img'])?$this->arr_series_poster['verticality_img']:'',
                'nns_image_s'=>isset($this->arr_series_poster['square_img'])?$this->arr_series_poster['square_img']:'',
                'nns_image_h'=>isset($this->arr_series_poster['horizontal_img'])?$this->arr_series_poster['horizontal_img']:'',
                'nns_cp_id'=>isset($param['Provider_ID']) ? $param['Provider_ID'] : '',
                'nns_conf_info'=>'',
                'nns_ext_url'=>'',
            ), //基本信息（存储于nns_vod表中）
            'ex_info'=>array(
                'svc_item_id'=>'',
                'month_clicks'=>'',
                'week_clicks'=>'',
                'base_id'=>'',
                'asset_path'=>'',
                'ex_tag'=>'',
                'full_spell'=>'',
                'awards'=>'',
                'year'=>'',
                'play_time'=>'',
                'channel'=>'',
                'first_spell'=>'',
            ), //扩展信息（存储于nns_vod_ex表中）
        );
        //字段待修改4-14
        return  $this->vod_action($action, $add_series);
    }

    //分集操作
    private  function do_index($param,$action)
    {
        $add_index = array(
            'base_info' => array(
                'nns_name' =>isset($param['Asset_Name']) ? $param['Asset_Name'] : '',
                'nns_index' => isset($param['Chapter'])?$param['Chapter']-1:1,
                'nns_time_len' => '',
                'nns_summary' => isset($param['Description']) ?$param['Description'] : '',
                'nns_image' => '',
//               'nns_image' => $this->arr_index_poster,
                'nns_play_count' => 0,
                'nns_score_total' => 0,
                'nns_score_count' => 0,
                'nns_video_import_id' => isset($this->arr_series['Asset_ID']) ? $this->arr_series['Asset_ID'] : '',
                'nns_import_id' =>  isset($param['Asset_ID']) ?$param['Asset_ID'] : '',
                'nns_import_source' => evn::get("project"),

                'nns_director' => isset($this->arr_series['Director']) ? $this->arr_series['Director'] : '',
                'nns_actor' => isset($this->arr_series['actos']) ? $this->arr_series['actors'] : '',
                'nns_release_time' => (strlen($param['Creation_Date']) > 0 && (int)$param['Creation_Date'] > 1970 && (int)$param['Creation_Date'] < 2020) ? (int)$param['Creation_Date'] : date("Y"),
                'nns_update_time' => date("Y-m-d"),
                'nns_watch_focus' => '',
                'nns_cp_id' => isset($this->arr_series['Provider_ID']) ?$this->arr_series['Provider_ID'] : '',
                'nns_conf_info' => '',
                'nns_ext_url' => '',
            ),
            'ex_info' => array(
                'isintact' => '',
                'subordinate_name' => '',
                'initials' => '',
                'publisher' => '',
                'first_spell' => '',
                'caption_language' => '',
                'language' => '',
                'region' => '',
                'adaptor' => '',
                'sreach_key' => '',
                'event_tag' => '',
                'year' => '',
                'sort_name' => '',
            ),
        );
        //分集注入
        return $this->index_action($action, $add_index);
    }

    //片源操作
    private function do_media($param,$action)
    {
        $add_media = array(
            'base_info' => array(
                'nns_name' => $param['Asset_Name'],
                //???
                'nns_type' => 1,
                'nns_url' => isset($param['media_url']) ? $param['media_url'] : '',
                'nns_tag' => '26,',
                'nns_mode' => isset($param['MediaMode'])?$param['MediaMode']:'',
                'nns_kbps' => isset($param['Video_Bit_Rate'])?$param['Video_Bit_Rate']:0,
                //??
                'nns_content_id' => $param['Original_Asset_ID'],
                'nns_content_state' => 0,
                'nns_filetype' => isset($param['media_file_type'])?$param['media_file_type']:'ts',
                'nns_play_count' => '0',
                'nns_score_total' => '0',
                'nns_score_count' => '0',
                'nns_video_import_id' => isset($this->arr_series['Asset_ID']) ? $this->arr_series['Asset_ID'] : '',
                'nns_index_import_id' => isset($this->arr_index['Asset_ID']) ? $this->arr_index['Asset_ID'] : '',
                'nns_import_id' => isset($this->arr_media['Asset_ID']) ? $this->arr_media['Asset_ID'] : '',
                'nns_import_source' => evn::get("project"),
                'nns_dimensions' => '2D',
                'nns_ext_url' => '',
                'nns_file_size' => isset($param['Content_File_Size'])?$param['Content_File_Size']:0,
                'nns_file_time_len' =>isset($param['Run_Time'])?(strtotime($param['Run_Time']) - strtotime('00:00:00')):0,
                'nns_file_frame_rate' => $param['Audio_Bit_Rate'],
                'nns_file_resolution' => $param['Frame_Height']*$param['Frame_Width'],
                'nns_cp_id' => isset($this->arr_series['Provider_ID']) ? $this->arr_series['Provider_ID'] : '',
                'nns_ext_info' => '',
                'nns_drm_enabled' =>  0,
                'nns_drm_encrypt_solution' => '',
                'nns_drm_ext_info' => '',
                'nns_domain' => 0,
                //1 点播片源  2 回看片源
                'nns_media_type' =>1,
                'nns_original_live_id' => '',
                'nns_start_time' => '',
                'nns_media_service' => isset($param['meida_server'])?$param['media_server']:"HTTP",
                'nns_conf_info' => '',
                'nns_encode_flag' => 0,
                'nns_live_to_media' => '',
                //业务类型
                'nns_media_service_type' => isset($param['Service_Type'])?$param['Service_Type']:'',
            ),
            'ex_info' => array(
                'file_hash' => '',
                'file_width' => '',
                'file_height' => '',
                'file_scale' => '',
                'file_coding' => '',
            ),
        );
        return  $this->media_action($action, $add_media);
    }


    public function is_ok($message_id,$code,$reason){

    }

    //打点操作
    private function do_seekpoint($param,$action)
    {
        $add_seekpoint = array(
            'base_info' => array(
                'nns_video_index'=>isset($this->arr_index['Chapter'])?$this->arr_index['Chapter'] : 0,
                //???
                'nns_vod_index_id'=>'',
                'nns_fragment_type'=>isset($this->arr_seekpoint_frametype_map[$param['FragmentType']])?$this->arr_seekpoint_frametype_map[$param['FragmentType']]:0,
                'nns_type'=>isset($this->arr_seekpoint_frametype_map[$param['FragmentType']])?$this->arr_seekpoint_frametype_map[$param['FragmentType']]:0,
                'nns_image'=>isset($param['SeekPicUrl'])?$param['SeekPicUrl']:'',
                'nns_begin'=>isset($param['BeginTime'])?$param['BeginTime']: 0,
                'nns_end'=>isset($param['EndTime'])?$param['EndTime']: 0,
                'nns_name'=>isset($param['SeekName'])?$param['SeekName']: '',
                'nns_import_id'=>$this->arr_series['Asset_ID'],
                'nns_import_source'=>evn::get("project"),
                'nns_cp_id'=>$this->message['nns_cp_id'],
            ),
        );
        return  $this->seekpoint_action('add', $add_seekpoint);
    }

    /**
     * 消息队列注入播控统一反馈给芒果二级，组装消息结构
     * @param int $state
     * @param string $reason
     * @param array $info
     */
    public function create_xml_respond_msg_to_csp($state=0, $reason='',$info=array())
    {
        if ($state == 0)
        {
            \m_config::write_callback_log('不是最终注入下游成功状态不进行反馈', $this->source_id, $this->msg_id);
            return;
        }

        $site_url = $this->arr_cp_config['data_info']['nns_config']['site_callback_url'];

        \m_config::write_message_execute_log('消息处理结果:' . var_export($reason, true), $this->source_id);
        \m_config::write_callback_log('[' . date('Y-m-d H:i:s') . ']' . '反馈到上游地址信息：站点地址为-' . $site_url, $this->source_id, $this->msg_id);

        //反馈状态转换
        $state = $state== 0 ?  0 : -1;
        if(!empty($site_url))
        {
            $params = array(
                'msgid' => $this->msg_id,
                'CmsResult' => $state,
                'ResultMsg' => $reason
            );
            if(!empty($info))
            {
                $params['info'] = $info;
            }
            $this->notify_msg($params,true,$site_url);
        }
        else
        {
            \m_config::write_callback_log('[' . date('Y-m-d H:i:s') . ']' . '没有配置站点信息', $this->source_id, $this->msg_id);
        }
    }

}


