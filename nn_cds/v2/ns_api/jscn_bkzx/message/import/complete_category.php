<?php
/**
 * author:zhihong.yin
 * 更新nns_asset_category_incre,输入新建的的一级栏目ID,补全栏目表实际插入的栏目ID和父级ID
 */
error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
class complete_category
{
    //db实例
    private $db = null;

    //新建的一级栏目id 如：10000100
    private $id_top_column = null;

    //栏目表明
    private $table_category = 'nns_asset_category_incre';

    public function __construct($id_top_column)
    {
        if( empty($this->db) )
        {
            $this->db = m_config::get_dc()->db();
        }

        $this->id_top_column = $id_top_column;
    }

    public function run()
    {
        $this->returnMsg('start');
        $this->complete_category();
        $this->returnMsg('end');
    }

    public  function complete_category()
    {
        //先把nns_insert_id 和 nns_insert_parent_id 列清空
        $sql_clear = " update {$this->table_category} set nns_insert_id = '',nns_insert_parent_id = '' ";
        nl_execute_by_db($sql_clear,$this->db);

    	for( $num =0;true;$num++ )
    	{
    	    echo 'execute row '.$num.PHP_EOL;
    		$sql="select * from {$this->table_category}
    						order by nns_deep,nns_parent_id 
    						limit {$num},1";
    		$re_select = nl_db_get_one($sql,$this->db);

    		if( empty($re_select) || !is_array($re_select) )
            {
                break;
            }


            if( empty( $re_select['nns_parent_id']) )
            {
                //一级栏目
                $sql = "update {$this->table_category}
                                set  nns_insert_id = {$this->id_top_column},
                                nns_insert_parent_id = 1000
                                where nns_id = {$re_select['nns_id']}";
            }
            else
            {
                //其他栏目
                //加此判断，同一父级的，不再查询，同级栏目直接累加即可
                if( isset($old_re_select) && $old_re_select['nns_parent_id'] == $re_select['nns_parent_id'] )
                {
                    $old_category_num ++;
                    $category_num = $old_category_num;
                    $parent_insert_id = $old_parent_insert_id;
                }
                else
                {
                    //父级栏目新栏目ID
                    $sql = "select nns_insert_id from {$this->table_category}
                                  where nns_id = {$re_select['nns_parent_id']}";
                    $parent_insert_id = nl_db_get_col($sql,$this->db);

                    $old_parent_insert_id = $parent_insert_id;
                    //新的栏目id不为空同级栏目的数量
                    $sql = "select count(*) from {$this->table_category}
                                        where nns_parent_id = {$re_select['nns_parent_id']}
                                        and length(nns_insert_id) > 0";
                    $category_num = nl_db_get_col($sql,$this->db);
                    $old_category_num = $category_num;
                }

                //新栏目id
                $insert_id = $parent_insert_id.substr('000'.($category_num+1),-3);

                $sql = "update {$this->table_category}
                                set  nns_insert_id = {$insert_id},
                                nns_insert_parent_id = {$parent_insert_id}
                                where nns_id = {$re_select['nns_id']}";
            }
            nl_execute_by_db($sql,$this->db);
            $old_re_select = $re_select;
    	}
    }

    /**
     * @param $msg     脚本信息
     * @param null $type    信息类型   die 终止信息  |  其他  一般提示信息
     * 脚本执行信息
     */
    public function returnMsg($msg,$type = null)
    {
        if( is_string($msg) && !empty($msg))
        {
            echo str_pad($msg,50,'-',STR_PAD_BOTH).PHP_EOL;
            if($type == 'die')
            {
                echo str_pad('进程结束',50,'-',STR_PAD_BOTH).PHP_EOL;
                exit;
            }
        }
    }
}
exit('暂时未用');
if( empty($argv[1])  )
{
    exit('不存在新建的八位顶级栏目ID');
}
else if( !preg_match('/^10000\d{3}$/',$argv[1]) )
{
    exit('顶级栏目ID不合法');
}

$obj = new complete_category($argv[1]);
$obj->run();