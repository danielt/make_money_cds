<?php
/**
 * autor:zhihong.yin
 * description:播控资源库媒资，通过epg分发器，分发到epg
 */
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
include_once dirname(dirname(dirname(__FILE__))).'/delivery/jscn_bkzx/epg_delivery.class.php';
\ns_core\m_load::load("ns_core.m_config");
define('CP_ID','jycm');
define('SP_ID','jscn_bkzx');
class bk_to_delivery
{
    //epg分发对象
    private $obj_epg_delivery;
    //数据库对象
    private $db;
    //上游
    private $cp;
    //下游
    private $sp;

    public function __construct()
    {
        $this->cp = CP_ID;
        $this->sp = SP_ID;
        $this->db = m_config::get_dc()->db();
        $this->obj_epg_delivery = new epg_delivery($this->sp);
        //脚本标识
        $this->obj_epg_delivery->flag_is_script_import = TRUE;
        //扩展信息设为空
        $this->obj_epg_delivery->video_info['ex_info'] = array();
        $this->obj_epg_delivery->index_info['ex_info'] = array();
        $this->obj_epg_delivery->media_info['ex_info'] = array();

        //c2_info[‘action’]统一设置为add
        $this->obj_epg_delivery->c2_info['action'] = 'add';

        $this->obj_epg_delivery->str_source_id = CP_ID;

    }

    /**
     * 遍历group->vod->index->media,如果该主媒资id对应的主媒资，分集，片源注入不成功的则，则针对注入
     */
    public function run()
    {
        //查询group表，主要是获取主媒资的注入id，依次主媒资-分集-片源进行分发
        $circle_num = 0;
        while(true)
        {
            $arr_asset = $this->get_asset('group',null,null,$circle_num);
            $circle_num ++;
            if( empty($arr_asset) || !is_array($arr_asset) )
            {
                break;
            }
            $arr_asset = $arr_asset[0];
            //主媒资注入id不存在的
            if( empty($arr_asset['nns_series_id']) )
            {
                $this->returnMsg("媒资id{$arr_asset['nns_assetid']}对应的主媒资注入id不存在");
                continue;
            }
            //nns_vod  主媒资信息,以注入id查询
            $arr_vod = $this->get_asset('video',$arr_asset['nns_series_id']);
            if( empty($arr_vod) || !is_array($arr_vod) )
            {
                continue;
            }
            $this->obj_epg_delivery->video_info['base_info'] = $arr_vod[0];
            //用于传递group的注入id,从而改变每条group的状态
            $this->obj_epg_delivery->assetid = $arr_asset['nns_series_id'];

            //设置c2_info[‘name’]
            $this->obj_epg_delivery->c2_info['nns_name'] = $arr_vod[0]['nns_name'].'  【主媒资-(脚本注入)】';
            $this->obj_epg_delivery->c2_info['nns_type'] = 'video';
            //主媒资没有成功的才执行注入
            if( $arr_asset['nns_series_import_state'] !=1 )
            {
                $this->returnMsg("正在注入--主媒资--【{$arr_vod[0]['nns_name']}】");
                $this->obj_epg_delivery->video();
            }


            //分集,以nns_vod_id查询
            $arr_indexs = $this->get_asset('index',$arr_vod[0]['nns_id']);
            if( empty($arr_indexs) || !is_array($arr_indexs) )
            {
                continue;
            }

            foreach( $arr_indexs as $arr_index )
            {
                if( empty($arr_index) || !is_array($arr_index) )  continue;
                $this->obj_epg_delivery->index_info['base_info'] = $arr_index;
                $this->obj_epg_delivery->c2_info['nns_type'] = 'index';
                //设置c2_info[‘name’]
                $this->obj_epg_delivery->c2_info['nns_name'] = $arr_index['nns_name'].'  【分集-(脚本注入)】';

                //用于传递group的assetid,从而改变每条group的状态
                $this->obj_epg_delivery->assetid = $arr_index['nns_import_id'];

                //反查询group，获取分集的注入状态
                $arr_index_group = $this->get_group_by_assetid($arr_index['nns_import_id']);

                if( !empty($arr_index_group[0]) && is_array($arr_index_group[0]) && $arr_index_group[0]['nns_index_import_state'] !=1 )
                {
                    $this->returnMsg("正在注入--分集--【{$arr_index['nns_name']}】");
                    $this->obj_epg_delivery->index();
                }

                //片源,以nns_vod_id,nns_index_id查询(也考虑到多个片源的情况)
                $arr_medias = $this->get_asset('media',$arr_vod[0]['nns_id'],$arr_index['nns_id']);
                if( empty($arr_medias) || !is_array($arr_medias) )  continue;
                foreach( $arr_medias as $arr_media )
                {
                    if( empty($arr_media) || !is_array($arr_media) )  continue;
                    $this->obj_epg_delivery->media_info['base_info'] = $arr_media;
                    //用于传递group的assetid,从而改变每条group的状态
                    $this->obj_epg_delivery->assetid = $arr_media['nns_import_id'];
                    //设置c2_info[‘name’]
                    $this->obj_epg_delivery->c2_info['nns_name'] = $arr_media['nns_name'].'  【片源-(脚本注入)】';
                    $this->obj_epg_delivery->c2_info['nns_type'] = 'media';

                    //反查询group，获取分集的注入状态
                    $arr_media_group = $this->get_group_by_assetid($arr_media['nns_import_id']);

                    if( !empty($arr_media_group[0]) && is_array($arr_media_group[0]) &&  $arr_media_group[0]['nns_media_import_state'] !=1 )
                    {
                        $this->returnMsg("正在注入--片源--【{$arr_media['nns_name']}】");
                        $this->obj_epg_delivery->media();
                    }
                }
            }
        }
    }

    /**
     * @param $type
     * @param null $video_id  video-注入id  | index-nns_vod_id(主媒资的GUID)   |  media-nns_vod_id(主媒资的GUID)
     * @param int $start
     * @return bool
     */
    private function get_asset($type,$video_id = null,$index_id=null,$start =0)
    {
        switch ($type){
            case 'group':
                $sql = "select * from nns_asset_source_incre_group 
                          where   nns_series_id is not null
                          limit {$start},1";
                break;
            case 'video':
                $sql = "select * from nns_vod
                            where nns_asset_import_id = '{$video_id}'
                            and nns_cp_id = '".$this->cp."'
                            and nns_import_source = '".$this->sp."'
                            and nns_deleted = 0";
                break;

            case 'index':
                $sql = "select * from nns_vod_index
                            where nns_vod_id = '{$video_id}'
                            and nns_cp_id = '".$this->cp."'
                            and nns_import_source = '".$this->sp."'
                            and nns_deleted = 0";
                break;

            case 'media':
                $sql = "select * from nns_vod_media
                            where nns_vod_id = '{$video_id}'
                            and nns_vod_index_id= '{$index_id}'
                            and nns_cp_id = '".$this->cp."'
                            and nns_import_source = '".$this->sp."'
                            and nns_deleted = 0";
                break;
            default:
                return;
        }
        return nl_query_by_db($sql,$this->db);
    }

    /**
     * @param $assetid
     * @return bool
     * 根据nns_assetid获取group信息
     */
    private function get_group_by_assetid($assetid)
    {
        $sql = "select * from nns_asset_source_incre_group
                          where nns_assetid = '{$assetid}'";
        return nl_query_by_db($sql,$this->db);
    }


    /**
     * @param $msg     脚本信息
     * @param null $type    信息类型   die 终止信息  |  其他  一般提示信息
     * 脚本执行信息
     */
    public function returnMsg($msg,$type = null)
    {
        if( is_string($msg) && !empty($msg))
        {
            echo str_pad($msg,50,'-',STR_PAD_BOTH).PHP_EOL;
            if($type == 'die')
            {
                echo str_pad('进程结束',50,'-',STR_PAD_BOTH).PHP_EOL;
                exit;
            }
        }
    }
}

$bk_to_delivery = new bk_to_delivery();
$bk_to_delivery->run();









