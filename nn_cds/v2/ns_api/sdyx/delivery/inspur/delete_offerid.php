<?php
/**
 * 山东有线注入浪潮手动删除OfferId专用
 */
header("content-type:text/html;charset=utf-8");
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";

if(isset($_POST) && isset($_POST['id']) && !empty($_POST['id']))
{
    global $g_bk_web_url;
    $project = evn::get('project');

    $arr_dir = explode('|', str_replace(array('/', '\\'), '|', __DIR__));
    $str_sp_id = array_pop($arr_dir);

    $wsdl_addr = trim(trim($g_bk_web_url, '/'), '\\') . "/v2/ns_api/" . $project . "/delivery/" . $str_sp_id . "/inspur_service.php?wsdl";

    $soap_client = new SOAPClient($wsdl_addr, array('trace' => true));
    //添加soapheader
    $soap_header = new SoapHeader('inspurCMS','HeadUserCheck',(object)array('Username'=>'ContentManage','Password'=>'ContentManage'));
    $soap_client->__setSoapHeaders($soap_header);

    $client_re = $soap_client->DeleteOffer(array('offerUriId' => $_POST['id']));
    $client_code = $client_re->DeleteOfferResult;
    if($client_code == 0)
    {
        $reason = $client_code . "删除成功";
    }
    else
    {
        $inspur_code = array(
            '-1' => '已无此offer的记录',
            '111' => '验证失败',
            '301' => '输入的OfferUriId的格式不正确',
        );
        $reason = isset($inspur_code[$client_code]) ? $client_code.$inspur_code[$client_code] : $client_code.'删除失败';
    }
}
?>
<html>
<head>
    <title>山东有线专用</title>
</head>
<body>
<form action="" method="post">
    <table>
        <tr><td colspan="3"><h3>山东有线注入浪潮手动删除OfferId专用</h3></td></tr>
        <tr>
            <td>OfferuriId：</td>
            <td><input type="text" name="id" style="width:300px;"/>【片源表nns_vod_media中的nns_content_id字段】</td>
            <td><input type="submit" value="提交" /></td>
        </tr>
        <?php if(isset($reason)) {?>
            <tr><td colspan="3"><hr/></td></tr>
            <tr>
                <td><b>删除状态：</b></td>
                <td colspan="2"><font color="red"><?php echo $reason;?></font></td>
            </tr>
        <?php }?>
    </table>
</form>
</body>
</html>