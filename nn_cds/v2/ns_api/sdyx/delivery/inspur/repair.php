<?php
/**
 * 修复注入浪潮失败，需要重新注入的问题【注入失败无法重发，只能重新申请offerid在发送】
 * Created by IntelliJ IDEA.
 * User: LZ
 * Date: 2018/10/29
 * Time: 10:36
 */
header("content-type:text/html;charset=utf-8");
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_core.m_linux_mysql_query");

$arr_dir = explode('|', str_replace(array('/', '\\'), '|', __DIR__));
$str_sp_id = array_pop($arr_dir);

if(isset($_POST) && isset($_POST['id']) && !empty($_POST['id']))
{
    //获取片源的注入ID
    $sql = "select nns_import_id,nns_cp_id from nns_vod_media where nns_id = '{$_POST['id']}' and nns_deleted != 1";
    $media_re = nl_query_by_db($sql,\m_config::get_dc()->db());
    if(!is_array($media_re))
    {
        echo "<script>alert('查询片源注入ID失败');history.go(-1);</script>";die;
    }
    $result = "注入ID为：{$media_re[0]['nns_import_id']},CPID为：{$media_re[0]['nns_cp_id']}";
    $c2_sql = "delete from nns_mgtvbk_c2_task where nns_ref_id = '{$_POST['id']}' and nns_org_id = '{$str_sp_id}';";
    $m_sql = "delete from nns_vod_media where nns_id = '{$_POST['id']}';";
    $op_sql = "delete from nns_mgtvbk_op_queue where nns_media_id = '{$_POST['id']}' and nns_org_id = '{$str_sp_id}';";
    $clip_sql = "delete from nns_mgtvbk_clip_task where nns_video_media_id = '{$_POST['id']}' and nns_org_id = '{$str_sp_id}';";
    //获取消息ID
    $search_re = \m_linux_mysql_query::linux_mysql_exchange('message','content',$media_re[0]['nns_import_id'],$media_re[0]['nns_cp_id']);
    if($search_re['ret'] != 0 || !is_array($search_re['data_info']))
    {
        echo "<script>alert('查询片源注入原始工单失败');history.go(-1);</script>";die;
    }
    //获取当条的消息ID
    $msg_nns_id = implode($search_re['data_info'],"','");
    $msg_sql = "select nns_message_id from nns_mgtvbk_message where nns_id in ('{$msg_nns_id}')";
    $msg_re = nl_query_by_db($msg_sql,\m_config::get_dc()->db());
    if(!is_array($msg_re))
    {
        echo "<script>alert('没有查询到消息工单ID');history.go(-1);</script>";die;
    }
    //根据消息ID计算得出3条日志SQL
    $output_re = array();
    foreach ($msg_re as $r)
    {
        $output_re[$r['nns_message_id']][] = "delete from nns_command__" . get_hash_num($r['nns_message_id']) . " where nns_message_id = '{$r['nns_message_id']}';";
        $command_sql = "select * from nns_command__" . get_hash_num($r['nns_message_id']) . " where nns_message_id = '{$r['nns_message_id']}'";
        $command_re = nl_query_by_db($command_sql, \m_config::get_hash_dc()->db());
        if(is_array($command_re))
        {
            $output_re[$r['nns_message_id']][] = "delete from nns_command_task__" . get_hash_num($command_re[0]['nns_id']) . " where nns_command_id = '{$command_re[0]['nns_id']}';";
            $task_sql = "select * from nns_command_task__" . get_hash_num($command_re[0]['nns_id']) . " where nns_command_id = '{$command_re[0]['nns_id']}'";
            $task_re = nl_query_by_db($task_sql, \m_config::get_hash_dc()->db());
            if(is_array($task_re))
            {
                $output_re[$r['nns_message_id']][] = "delete from nns_command_task_log__" . get_hash_num($task_re[0]['nns_id']) . " where nns_command_task_id = '{$task_re[0]['nns_id']}';";
            }
        }
    }

}

function get_hash_num($value,$slot=100)
{
    $md5 = md5($value);
    $key = substr($md5, -4,4);
    $key = hexdec($key);
    $key = $key%$slot;
    return $key;
}
?>
<html>
<head>
    <title>山东有线专用</title>
</head>
<body>
<form action="" method="post">
    <table>
        <tr><td colspan="3"><h3>山东有线注入浪潮失败查询专用</h3></td></tr>
        <tr>
            <td>片源GUID：</td>
            <td><input type="text" name="id" style="width:300px;"/>【播控注入平台片源ID，可在C2队列查询获取到】</td>
            <td><input type="submit" value="提交" /></td>
        </tr>
        <?php if(isset($result)){ ?>
        <tr><td colspan="3"><hr/></td></tr>
        <tr>
            <td><b>片源属性：</b></td>
            <td colspan="2"><font color="red"><?php echo $result."<br/>".$c2_sql."<br/>".$clip_sql."<br/>".$op_sql."<br/>".$m_sql;?></font></td>
        </tr>
        <?php }?>
        <?php if(isset($output_re) && is_array($output_re)) {
            foreach ($output_re as $id=>$r){
        ?>
        <tr><td colspan="3"><hr/></td></tr>
        <tr>
            <td><b>消息数据：</b></td>
            <td colspan="2"><font color="red"><?php echo "消息ID为：".$id;echo '<br/>';echo implode($r,"<br/>");?></font></td>
        </tr>
        <?php }}?>
    </table>
</form>
</body>
</html>