<?php
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";

$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define("STR_SP_ID", $str_dir);
$postdata = file_get_contents("php://input");
\m_config::write_cdn_notify_log('CDS接口接收到CDN消息反馈数据为:' . $postdata, STR_SP_ID, '');

class notify 
{
	public function injectResponse($offerId,$offerUriId,$termUriId,$code,$reason)
    {
        $parameters = func_get_args();
        \m_config::write_cdn_notify_log('injectResponse方法接收到CDN消息反馈数据为:' . var_export($parameters,true), STR_SP_ID, '');

        $delivery = \ns_core\m_load::load_cdn_delivery_object(__FILE__);
        $ret = $delivery->notify($offerId,$offerUriId,$code,$reason);

        \m_config::write_cdn_notify_log('injectResponse方法处理CDN消息反馈结果为:' . var_export($ret,true), STR_SP_ID, '');
        return array();
    }
}

$server = new SOAPServer('asmx_notify.wsdl');
$server->setClass('notify');
$server->handle();