<?php
ini_set("soap.wsdl_cache_enabled", "0");
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";

class mg_local
{
    private $HeadUserCheck = FALSE;

    public function HeadUserCheck($value)
    {
        if($value->Username =='ContentManage' && $value->Password == 'ContentManage')
        {
            $this->HeadUserCheck = true;
        }
        else
        {
            $this->HeadUserCheck = FALSE;
        }
    }

    public function InjectOffer ($parameters)
    {
        if($this->HeadUserCheck)
        {
            return array('InjectOfferResult' => 0);
        }
        else
        {
            return array('InjectOfferResult' => 111);
        }
    }
    public function DeleteOffer($parameters)
    {
        return array('DeleteOfferResult' => 0);
    }
    public function GetFileId($parameters)
    {
        return array('GetFileIdResult' => 'MGTV2018091117431630');
    }
}
$server = new SOAPServer('asmx.wsdl');
//$server->setClass('mg_local');
$server->handle();
