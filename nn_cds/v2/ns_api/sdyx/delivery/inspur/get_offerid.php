<?php
/**
 * 山东有线注入浪潮手动上传工单获取OfferId专用
 */
header("content-type:text/html;charset=utf-8");
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";

if(isset($_POST) && isset($_POST['id']) && !empty($_POST['id']))
{
    global $g_bk_web_url;
    $project = evn::get('project');

    $arr_dir = explode('|', str_replace(array('/', '\\'), '|', __DIR__));
    $str_sp_id = array_pop($arr_dir);

    $wsdl_addr = trim(trim($g_bk_web_url, '/'), '\\') . "/v2/ns_api/" . $project . "/delivery/" . $str_sp_id . "/inspur_service.php?wsdl";

    $soap_client = new SOAPClient($wsdl_addr, array('trace' => true));
    $ret = $soap_client->GetFileId(array('provider' => strtoupper($_POST['id'])));
    $offerid = $ret->GetFileIdResult;
}
?>
<html>
<head>
    <title>山东有线专用</title>
</head>
<body>
<form action="" method="post">
    <table>
        <tr><td colspan="3"><h3>山东有线注入浪潮手动上传工单获取OfferId专用</h3></td></tr>
        <tr>
            <td>内容提供商：</td>
            <td><input type="text" name="id" style="width:100px;"/>【四位字母，如mgtv】</td>
            <td><input type="submit" value="提交" /></td>
        </tr>
        <?php if(isset($offerid)) {?>
        <tr><td colspan="3"><hr/></td></tr>
        <tr>
            <td><b>OfferId：</b></td>
            <td colspan="2"><font color="red"><?php echo $offerid;?></font></td>
        </tr>
        <?php }?>
    </table>
</form>
</body>
</html>