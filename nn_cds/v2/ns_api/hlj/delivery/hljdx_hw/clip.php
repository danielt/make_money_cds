<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/9/5 10:50
 */
error_reporting(0);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
ns_core\m_load::load('ns_model.clip.clip');
\ns_core\m_load::load_old('nn_logic/cp/cp.class.php');
\ns_core\m_load::load_old('nn_logic/sp/sp.class.php');

$arr_dir = explode('|', str_replace(array ('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
define("IS_LOG_TIMER_OPERATION", true);
$func = isset($_GET['nns_func']) ? $_GET['nns_func']:null;
$class_clip = new clip(ORG_ID,$_GET,$func);
$arr_function = get_class_methods($class_clip);
$arr_function = is_array($arr_function) ? $arr_function : array();
if(empty($_GET['nns_func']))
{
    \m_config::write_clip_receive_log('$_GET获取的nns_func参数值为空', ORG_ID);
    $class_clip->_bulid_result('500','param nns_func is empty',null,'public');
}
if(!in_array($func, $arr_function))
{
    \m_config::write_clip_receive_log('$_GET获取的nns_func参数值为:'.$_GET['nns_func'].',类方法有['.implode(',', $arr_function).']', ORG_ID);
    $class_clip->_bulid_result('500','nl_clip->'.$func.' not found',null,'public');
}
else
{
    \m_config::write_clip_receive_log('$_GET获取的参数值为:'.var_export($_GET,true), ORG_ID);
    $class_clip->$func();
}