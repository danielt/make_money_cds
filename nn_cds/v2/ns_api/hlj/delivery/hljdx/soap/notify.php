<?php
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('display_errors',1);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))."/common.php";
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
array_pop($arr_dir);
$str_dir = array_pop($arr_dir);
define("ORG_ID", $str_dir);
$postdata = file_get_contents("php://input");
\m_config::write_cdn_notify_log('接收到中兴CDN消息反馈数据为:' . $postdata, ORG_ID, '');

class notify{
    public function ResultNotify($CSPID, $LSPID, $CorrelateID, $CmdResult, $ResultFileURL)
    {
        $delivery = \ns_core\m_load::load_cdn_delivery_object(__FILE__);
        $arg_list = func_get_args();
        $msg = "WSDL处理参数：" . var_export($arg_list, true);
        if($CmdResult != 0 )
        {
            $CmdResult = -1;
        }
        $ret = $delivery->notify($CorrelateID, $CmdResult, $ResultFileURL);
        $msg = "notify方法处理后，结果：" . var_export($ret, true);
        \m_config::write_cdn_notify_log($msg, ORG_ID, '');
        if($ret['ret'] == NS_CDS_SUCCE)
        {
            return array('Result' => 0, 'ErrorDescription' => "Success");
        }
        else
        {
            return array('Result' => -1,'ErrorDescription'=>"Error");
        }
    }
}
//ob_start();
$server = new SOAPServer('notify.wsdl');
$server->setClass('notify');
$server->handle();
