<?php
//libxml_disable_entity_loader(false);
ini_set('display_errors',1);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))."/common.php";
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
array_pop($arr_dir);
$str_dir = array_pop($arr_dir);
define("ORG_ID", $str_dir);
define('CSPID','HLJ_STARCOR');
define('LSPID','HLJ_ZTE');
class client{
    static public function ExecCmd($CorrelateID, $CmdFileURL, $URL){
        $arg_list = func_get_args();
        \m_config::write_cdn_execute_log('执行中兴CDN注入请求参数为：ExecCmd params:' . var_export($arg_list,true), ORG_ID, $CorrelateID);
        $client = self::get_soap_client($URL);
        $ret = $client->ExecCmd(CSPID, LSPID, $CorrelateID, $CmdFileURL);
        m_config::write_cdn_execute_log('执行中兴CDN注入结果为：' . var_export($ret,true), ORG_ID, $CorrelateID);
        return $ret;
    }

    public static function get_soap_client($URL){
        return new SOAPClient($URL,array('trace'=>true));
    }
}
