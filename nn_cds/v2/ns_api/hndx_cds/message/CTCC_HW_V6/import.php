<?php
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.message.message_queue");

define('DEFAULT_FTP_URL','ftp://lzy:lzy@127.0.0.1/');
define('DEFAULT_SOURCE_ID','CTCC_HW_V6');

//http公共入口
class http_import extends \ns_model\message\message_queue
{
	/**
	 * 注入消息队列模板
	 * @var unknown
	 */
	public $arr_in_message = array(
                              'base_info'=>array(
                                      'nns_message_time'=>'', //上游消息时间  年月日时分秒毫秒
                                      'nns_message_id'=>'',  //上游消息ID
                                      'nns_cp_id'=>'', //上游CP标示
                                      'nns_message_xml'=>'',  //上游原始的信息  可以为字符串 | xml | json 或则存放消息地址
                                      'nns_message_content'=>'',  //上游解析后的信息  可以为字符串 | xml | json  生成队列的基本信息
                                      'nns_action'=>'', //操作 行为
                                      'nns_type'=>'', //消息 类型
                                      'nns_name'=>'',  //消息名称
                                      'nns_package_id'=>'',  //包ID
                                      'nns_xmlurlqc'=>'', //广州电信悦ME MD5摘要
                                      'nns_encrypt'=>'', //广州电信悦ME 加密串
                                      'nns_content_number'=>'', //xml文件中包含的内容数量
                              ), //基本信息（存储于nns_mgtvbk_message表中）
                     );

    public $in_params = array();

    public function __construct()
    {
        \ns_core\m_load::load_np("np_xml_to_array.class.php");
        m_config::get_dc();
        m_config::write_message_receive_log("-----------消息接收开始-----------", DEFAULT_SOURCE_ID);
        $data = $_POST;
        m_config::write_message_receive_log("接收POST消息：". var_export($data, true), DEFAULT_SOURCE_ID);
        if(empty($data))
        {
            $post_params = file_get_contents('php://input');
            m_config::write_message_receive_log("接收流文件消息：". var_export($post_params, true), DEFAULT_SOURCE_ID);
            $data = array(
                'cmspost' => $post_params
            );
        }
        $this->in_params = $data;
    }

    public function import()
    {
        //cmspost xml数据
        $xml_data = $this->in_params['cmspost'];
        if(empty($xml_data))
        {
            m_config::write_message_receive_log("POST数据为空", DEFAULT_SOURCE_ID);
            return false;
        }

        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->loadXML($xml_data);
        $cmspost = $dom->saveXML();
        $this->arr_in_message['base_info']['nns_message_xml'] = $cmspost;
        $this->arr_in_message['base_info']['nns_cp_id'] = DEFAULT_SOURCE_ID;

        $xml_arr_data = np_xml_to_array::parse2($cmspost);

        if(!is_array($xml_arr_data))
        {
            m_config::write_message_receive_log("XML解析失败", DEFAULT_SOURCE_ID);
            return false;
        }
        //消息ID
        $this->arr_in_message['base_info']['nns_message_id'] = $xml_arr_data['msgid'];
        $this->arr_in_message['base_info']['nns_message_time'] = $xml_arr_data['time'];
        $xml_arr_data['url'] = trim($xml_arr_data['url']);
        //下载xml
        if(empty($xml_arr_data['url']))
        {
            m_config::write_message_receive_log("内容节点的URL地址为空", DEFAULT_SOURCE_ID);
            return false;
        }
        if(stripos($xml_arr_data['url'], 'ftp://') === false && stripos($xml_arr_data['url'], 'http://') === false)
        {
            $xml_arr_data['url'] = DEFAULT_FTP_URL . ltrim($xml_arr_data['url'],'/');
        }
        $this->arr_in_message['base_info']['nns_message_xml'] = $xml_arr_data['url'];
        $result_content = m_config::get_curl_content($xml_arr_data['url']);

        if($result_content['ret'] != 0)
        {
            m_config::write_message_receive_log("访问内容节点的URL地址失败", DEFAULT_SOURCE_ID);
            return false;
        }
        else if(!isset($result_content['data_info']) || strlen($result_content['data_info']) < 1)
        {
            m_config::write_message_receive_log("访问内容节点的URL地址获取的内容为空", DEFAULT_SOURCE_ID);
            return false;
        }


        $this->arr_in_message['base_info']['nns_message_content'] = $result_content['data_info'];

        $xml_obj = simplexml_load_string($result_content['data_info']);
        if(!$xml_obj)
        {
            m_config::write_message_receive_log("下载的XML工单解析失败", DEFAULT_SOURCE_ID);
            return false;
        }
        //获取操作(新增,删除)1增加修改; 2 删除;
        $this->arr_in_message['base_info']['nns_action'] = (int) $xml_obj->operation[0];
        //1 主媒资 2分集 3片源
        $this->arr_in_message['base_info']['nns_type'] = (int) $xml_obj->assetdesc[0];

        $params['name'] = '';
        if($this->arr_in_message['base_info']['nns_type'] == 1)
        {
            $this->arr_in_message['base_info']['nns_name'] = (string)$xml_obj->content->clipname[0];
        }
        elseif ($this->arr_in_message['base_info']['nns_type'] == 2)
        {
            $this->arr_in_message['base_info']['nns_name'] = (string)$xml_obj->content->partname[0];
        }
        else
        {
            $this->arr_in_message['base_info']['nns_name'] = '';
        }
        $result = $this->push($this->arr_in_message);
        if($result['ret'] != 0)
        {
            return false;
        }
        return true;
    }
}

$http_import = new http_import();
$result_import = $http_import->import();
if($result_import)
{
    echo 1;
}
else
{
    echo 0;
}