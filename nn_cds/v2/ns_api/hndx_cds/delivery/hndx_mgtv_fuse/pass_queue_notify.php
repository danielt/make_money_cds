<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/11/23 11:15
 */
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_core.m_config");

$arr_dir = explode('|', str_replace(array ('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);

class pass_queue_notify
{
    public $in_params = array();

    public function __construct()
    {
        \ns_core\m_load::load_np("np_xml_to_array.class.php");
        \m_config::get_dc();
        \m_config::write_pass_queue_notify_log("-----------透传队列反馈消息接收开始-----------", ORG_ID);
        $data = $_POST;
        \m_config::write_pass_queue_notify_log("接收POST消息：". var_export($data, true), ORG_ID);
        if(empty($data))
        {
            $post_params = file_get_contents('php://input');
            \m_config::write_pass_queue_notify_log("接收流文件消息：". var_export($post_params, true), ORG_ID);
            $data = array(
                'cmspost' => $post_params
            );
        }
        //传入的值有可能是http的query格式的值，并且进行了URLEDcode加密
        if(!is_array($data))
        {
            $data = $this->_deal_import_data($data);
        }
        $this->in_params = $data;
    }

    public function pass_queue_notify()
    {
        //cmspost xml数据
        $xml_data = $this->in_params['cmspost'];
        if(empty($xml_data))
        {
            \m_config::write_pass_queue_notify_log("POST数据为空", ORG_ID);
            return false;
        }

        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->loadXML($xml_data);
        $cmspost = $dom->saveXML();

        $xml_arr_data = np_xml_to_array::parse2($cmspost);

        if(!is_array($xml_arr_data))
        {
            \m_config::write_pass_queue_notify_log("XML解析失败", ORG_ID);
            return false;
        }

        if (strlen($xml_arr_data['ret']) < 1 || strlen($xml_arr_data['msgid']) < 1 || strlen($xml_arr_data['reason']) < 1)
        {
            \m_config::write_pass_queue_notify_log("反馈参数为空" . var_export($xml_arr_data, true), ORG_ID);
            return false;
        }

        //查询透传队列数据
        $pass_queue_data = \nl_pass_queue::query_by_id(m_config::get_dc(), $xml_arr_data['msgid']);

        if ($pass_queue_data['ret'] != NS_CDS_SUCCE || !isset($pass_queue_data['data_info'])
            || empty($pass_queue_data['data_info']) || count($pass_queue_data['data_info']) != 1)
        {
            \m_config::write_pass_queue_notify_log('根据消息id【' . $xml_arr_data['msgid'] .'查询透传队列失败或者数据不存在', ORG_ID);
            return false;
        }

        //得到cp id、sp id
        $sp_id = isset($pass_queue_data['data_info'][0]['nns_org_id']) ? $pass_queue_data['data_info'][0]['nns_org_id'] : ORG_ID;
        $cp_id = $pass_queue_data['data_info'][0]['nns_cp_id'];
        $cp_info = \m_config::_get_cp_info($cp_id);
        $cp_config = isset($cp_info['data_info']['nns_config']) ? $cp_info['data_info']['nns_config'] : array();
        //记录的上游消息id
        $message_id = $pass_queue_data['data_info'][0]['nns_message_id'];
        if (empty($cp_id))
        {
            \m_config::write_pass_queue_notify_log('根据消息id【' . $xml_arr_data['msgid'] .'查询透传队列无上游CPID', $sp_id);
            return false;
        }

        //封装结果
        if ($xml_arr_data['ret'] == NS_CDS_SUCCE)
        {
            $code = NS_CDS_PASS_QUEUE_SUCCESS;
            $reason = '注入成功';
        }
        else
        {
            $code = NS_CDS_PASS_QUEUE_FAIL;
            $reason = '注入失败,' . $xml_arr_data['reason'];
        }

        //封装处理参数
        $params =  array(
            'nns_status' => $code,
            'nns_result_desc' => $reason
        );
        $where = array(
            'nns_id' => $xml_arr_data['msgid'],
            'nns_org_id' => $sp_id,
            'nns_cp_id' => $cp_id,
        );

        //进行更新队列状态
        $re = nl_pass_queue::modify(\m_config::get_dc(), $params, $where);

        if ($re['ret'] != NS_CDS_SUCCE)
        {
            \m_config::write_pass_queue_notify_log('根据消息id【' . $xml_arr_data['msgid'] .'更新队列数据失败', $sp_id);
            return false;
        }

        /******************（新版）进行反馈给上游**************************/
        $project = evn::get('project');
        $classname = \ns_core\m_load::load("ns_api.{$project}.message.{$cp_id}.message");
        $obj_feedback = new message();
        if ($classname)
        {
            $obj_feedback->is_ok($message_id, $code, $reason, array(), $sp_id, 'json');
            unset($obj_feedback);
        }
        /******************（新版）进行反馈给上游**************************/
        \m_config::write_pass_queue_notify_log("-----------透传队列反馈消息处理结束,结果为{$re['reason']}-----------", $sp_id);
        return true;

    }

    /**
     * 处理传入的URLEncode的值
     * @param $data
     * @return array
     */
    public function _deal_import_data($data)
    {
        $arr_tmp = array();
        $data = urldecode($data);
        parse_str($data,$arr_tmp);
        return $arr_tmp;
    }
}

$obj_notify = new pass_queue_notify();
$result_notify = $obj_notify->pass_queue_notify();
if($result_notify)
{
    echo 1;
}
else
{
    echo 0;
}