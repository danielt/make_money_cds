<?php
ini_set("soap.wsdl_cache_enabled", "0");
define('ORG_ID', 'hndx_hw');
include_once dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))."/common.php";

class OTT_hndx_hw{
    public function ExecCmd($CSPID, $LSPID, $CorrelateID, $CmdFileURL){
        $arg_list = func_get_args();

        \m_config::write_cdn_execute_log('执行CDN注入请求参数为：ExecCmd params:' . var_export($arg_list,true), ORG_ID, $CorrelateID);
        return array('Result'=>0,'ErrorDescription'=>'ok');
    }
}
$server = new SOAPServer('hw.wsdl');//hndx_hw.wsdl
//$server->setClass('OTT_hndx_hw');
$server->handle();
