<?php
/**
 * 测试使用，正式使用时不会使用此文件。
 * 茁壮注入地址的配置在项目配置文件中
 */
ini_set("soap.wsdl_cache_enabled", "0");
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";

class qxyk
{
    public function IngestAsset ($parameters)
    {
        return array(
            'LSPID' => $parameters->LSPID,
            'AMSID' => $parameters->AMSID,
            'Sequence' => $parameters->Sequence,
            'ResultCode' => 0,
            'ResultMsg' => 'success',
        );
    }

    public function Publish ($parameters)
    {
        return array(
            'LSPID' => $parameters->LSPID,
            'AMSID' => $parameters->AMSID,
            'Sequence' => $parameters->Sequence,
            'ResultCode' => 0,
            'ResultMsg' => 'success',
        );
    }

    public function DeleteAsset ($parameters)
    {
        return array(
            'LSPID' => $parameters->LSPID,
            'AMSID' => $parameters->AMSID,
            'Sequence' => $parameters->Sequence,
            'ResultCode' => 0,
            'ResultMsg' => 'success',
        );
    }

    public function Unpublish ($parameters)
    {
        return array(
            'LSPID' => $parameters->LSPID,
            'AMSID' => $parameters->AMSID,
            'Sequence' => $parameters->Sequence,
            'ResultCode' => 0,
            'ResultMsg' => 'success',
        );
    }
}
$server = new SOAPServer('ipanel.wsdl');
$server->setClass('qxyk');
$server->handle();
