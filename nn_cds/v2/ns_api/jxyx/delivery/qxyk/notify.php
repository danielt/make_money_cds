<?php
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";

$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define("STR_SP_ID", $str_dir);
$postdata = file_get_contents("php://input");
\m_config::write_cdn_notify_log('CDS接口接收到CDN消息反馈数据为:' . $postdata, STR_SP_ID, '');

/***************************解析流-begin*******************************/
include_once NPDIR . "np_xml2array.class.php";
$xml_arr = np_xml2array::getArrayData($postdata);
if($xml_arr['ret'] != 1)
{
    \m_config::write_cdn_notify_log('反馈XML解析失败_SET01', STR_SP_ID, '');
}
$parse_re = array();
$parse_name = '';
if(isset($xml_arr['data']['children'][0]['children'][0]['children']) && is_array($xml_arr['data']['children'][0]['children'][0]['children']))
{
    $parse_name = $xml_arr['data']['children'][0]['children'][0]['name'];
    foreach ($xml_arr['data']['children'][0]['children'][0]['children'] as $value)
    {
        $parse_re[$value['name']] = $value['content'];
    }
}
else
{
    \m_config::write_cdn_notify_log('反馈XML解析失败_SET02', STR_SP_ID, '');
}

if(!empty($parse_re) && !empty($parse_name))
{
    $notify = new notify();
    $parse = (object)$parse_re;
    switch ($parse_name)
    {
        case 'IngestNotify':
            $arr_re = $notify->IngestNotify($parse);
            return $notify->__build_notify('IngestNotify', $arr_re);
            break;
        case 'PublishNotify';
            $arr_re = $notify->PublishNotify($parse);
            return $notify->__build_notify('PublishNotify', $arr_re);
            break;
        default:
            \m_config::write_cdn_notify_log('异步通知反馈行为动作不符', STR_SP_ID, '');
            break;
    }
    unset($notify);
}
else
{
    \m_config::write_cdn_notify_log('反馈XML解析失败_SET03', STR_SP_ID, '');
}
/***************************解析流-end*******************************/

class notify 
{
    /**
     * homed 通知注入结
     * @param $parameters stdClass
     * @param $LSPID
     * @param $AMSID
     * @param $Sequence
     * @param $AssetId
     * @param $PlayId
     * @param $ResultCode
     * @param $ResultMsg
     * @return array
     */
	public function IngestNotify($parameters)
    {
        \m_config::write_cdn_notify_log('IngestNotify方法接收到CDN消息反馈数据为:' . var_export($parameters,true), STR_SP_ID, '');
        $delivery = \ns_core\m_load::load_cdn_delivery_object(__FILE__);
        $ret = $delivery->ingest_notify($parameters->Sequence,$parameters->AssetId,$parameters->PlayId,$parameters->ResultCode,$parameters->ResultMsg);
        \m_config::write_cdn_notify_log('IngestNotify方法处理CDN消息反馈结果为:' . var_export($ret,true), STR_SP_ID, '');
        return array(
            'LSPID' => $parameters->LSPID,
            'AMSID' => $parameters->AMSID,
            'Sequence' => $parameters->Sequence,
        );
    }

    /**
     * homed 通知媒资发布（上架）
     * @param $parameters stdClass
     * @return array
     */
    public function PublishNotify($parameters)
    {
        \m_config::write_cdn_notify_log('PublishNotify方法接收到CDN消息反馈数据为:' . var_export($parameters,true), STR_SP_ID, '');
        $delivery = \ns_core\m_load::load_cdn_delivery_object(__FILE__);
        $ret = $delivery->publish_notify($parameters->Sequence,$parameters->AssetId,$parameters->PlayId);
        \m_config::write_cdn_notify_log('PublishNotify方法处理CDN消息反馈结果为:' . var_export($ret,true), STR_SP_ID, '');
        return array(
            'LSPID' => $parameters->LSPID,
            'AMSID' => $parameters->AMSID,
            'Sequence' => $parameters->Sequence,
        );
    }

    public function __build_notify($action, $params)
    {
        $str_xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $str_xml.= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
        $str_xml.=     '<soapenv:Body>';
        $str_xml.=         '<'.$action.'Response xmlns="http://IngestNotifyService.homed.ipanel.cn/">';
        $str_xml.=             '<LSPID xmlns="">'.$params['LSPID'].'</LSPID>';
        $str_xml.=             '<AMSID xmlns="">'.$params['AMSID'].'</AMSID>';
        $str_xml.=             '<Sequence xmlns="">'.$params['Sequence'].'</Sequence>';
        $str_xml.=         '</'.$action.'Response>';
        $str_xml.=     '</soapenv:Body>';
        $str_xml.= '</soapenv:Envelope>';
        return $str_xml;
    }
}
/******************茁壮的发布反馈有毛病，采用解析流的方式进行接收并处理***********************/
//$server = new SOAPServer('ipanel_notify.wsdl');
//$server->setClass('notify');
//$server->handle();