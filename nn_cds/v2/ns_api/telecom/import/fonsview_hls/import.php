<?php
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.message.message_queue");


class import extends \ns_model\message\message_queue
{
    public $arr_in_message = array(
        'base_info'=>array(
            'nns_message_time'=>'', //上游消息时间  年月日时分秒毫秒
            'nns_message_id'=>'',  //上游消息ID
            'nns_cp_id'=>'', //上游CP标示
            'nns_message_xml'=>'',  //上游原始的信息  可以为字符串 | xml | json 或则存放消息地址
            'nns_message_content'=>'',  //上游解析后的信息  可以为字符串 | xml | json  生成队列的基本信息
            'nns_action'=>'', //操作 行为
            'nns_type'=>'', //消息 类型
            'nns_name'=>'',  //消息名称
            'nns_package_id'=>'',  //包ID
            'nns_xmlurlqc'=>'', //广州电信悦ME MD5摘要
            'nns_encrypt'=>'', //广州电信悦ME 加密串
            'nns_content_number'=>'', //xml文件中包含的内容数量
        ), //基本信息（存储于nns_mgtvbk_message表中）
    );
    public $mixed_params = '';

    public function __construct()
    {
        \ns_core\m_load::load_np("np_xml2array.class.php");
        \m_config::get_dc();
    }
    public function import()
    {
        //原始数据存储的字段
        $this->arr_in_message['base_info']['nns_message_xml'] = $this->mixed_params;

        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->loadXML($this->mixed_params);
        $cmspost = $dom->saveXML();
        $xml_arr_data = np_xml2array::getArrayData($cmspost);
        //消息ID
        if ($xml_arr_data['ret'] != 1 && !is_array($xml_arr_data['data']['children'][0]['children'][0]['children']))
        {
            m_config::write_message_receive_log("报文解析失败","fonsview_hls");
            return $this->return_response(-1, '报文解析失败');
        }

        foreach ($xml_arr_data['data']['children'][0]['children'][0]['children'] as $val)
        {
            switch ($val['name'])
            {
                case 'CMSID':
                    $this->arr_in_message['base_info']['nns_cp_id'] = isset($val['content']) && !empty($val['content']) ? $val['content'] : '';
                    break;
                case 'CorrelateID':
                    $this->arr_in_message['base_info']['nns_message_id'] = isset($val['content']) && !empty($val['content']) ? $val['content'] : '';
                    break;
                case 'ContentMngXMLURL':
                    $str_content_url = isset($val['content']) && !empty($val['content']) ? $val['content'] : '';
                    break;
            }

        }

        if(strlen($str_content_url) < 1 || $this->arr_in_message['base_info']['nns_cp_id'] != 'fonsview_hls' || strlen($this->arr_in_message['base_info']['nns_message_id']) < 1)
        {
            m_config::write_message_receive_log("报文必传参数不全:".var_export($this->arr_in_message,true)."下载地址:".$str_content_url,"fonsview_hls");
            return $this->return_response(-1, '报文必传参数不全');
        }

        //获取ftp 、http 文件内容
        $result_content = m_config::get_curl_content($str_content_url);
        if($result_content['ret'] != 0)
        {
            m_config::write_message_receive_log("工单下载失败","fonsview_hls");
            return $this->return_response(-1, "工单下载失败");
        }
        else if(!isset($result_content['data_info']) || strlen($result_content['data_info']) < 1)
        {
            m_config::write_message_receive_log("工单下载成功但工单内容为空","fonsview_hls");
            return $this->return_response(-1, "工单下载成功但工单内容为空");
        }
        else
        {
            $this->arr_in_message['base_info']['nns_message_content'] = $result_content['data_info'];
        }
        //读取毫秒级时间
        list ($usec, $sec) = explode(' ', microtime());
        $time = str_pad(intval(substr($usec, 2, 4)),4,'0',STR_PAD_LEFT);
        $this->arr_in_message['base_info']['nns_message_time'] = date('YmdHis',time()) . $time;
        //消息队列入库方法
        $push_re = $this->push($this->arr_in_message);
        m_config::write_message_receive_log("消息接收结果:".var_export($push_re,true),"fonsview_hls");
        if($push_re['ret'] != 0)
        {
            return $this->return_response(-1, $push_re['reason']);
        }
        return $this->return_response($push_re['ret'], $push_re['reason']);
    }

    public function return_response($ret,$reason)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="iptv" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $xml .= '<SOAP-ENV:Body>';
        $xml .= '<ns1:ContentPreloadReqResponse>';
        $xml .= '<ContentPreloadReqReturn xsi:type="ns1:ContentPreloadReqResponse">';
        $xml .= '<ResultCode xsi:type="xsd:int">' . $ret . '</ResultCode>';
        $xml .= '<ErrorDescription xsi:type="SOAP-ENC:string">' . $reason . '</ErrorDescription>';
        $xml .= '</ContentPreloadReqReturn>';
        $xml .= '</ns1:ContentPreloadReqResponse>';
        $xml .= '</SOAP-ENV:Body>';
        $xml .= '</SOAP-ENV:Envelope>';
        return $xml;
    }
}

$import = new import();

m_config::write_message_receive_log("消息接收开始", "fonsview_hls");
//获取soap内容
$soap_params = file_get_contents('php://input');

if(empty($soap_params))
{
    m_config::write_message_receive_log("无接收stream内容","fonsview_hls");
    echo $import->return_response('-1','报文下发为空');
    die;
}
$import->mixed_params = $soap_params;

$result_import = $import->import();
m_config::write_message_receive_log("消息接收结束","fonsview_hls");
echo $result_import;