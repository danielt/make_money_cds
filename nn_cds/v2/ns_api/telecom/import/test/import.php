<?php
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.message.message_queue");
\ns_core\m_load::load("ns_core.m_file");
\ns_core\m_load::load("ns_core.encrypt.m_aiqiyi_encrypt");


$time = explode(' ', microtime());
$time = floor($time[0]*100000);
$time = date("YmdHis").'-'.$time;
m_config::base_write_log('message','request',"-----------{$time}消息接收开始-----------",m_config::return_child_path(__FILE__));
if(isset($_REQUEST) && !empty($_REQUEST) && is_array($_REQUEST))
{
    m_config::base_write_log('message','request',"{$time}接收request内容".var_export($_REQUEST,true),m_config::return_child_path(__FILE__));
}
else 
{
    m_config::base_write_log('message','request',"{$time}无接收request内容",m_config::return_child_path(__FILE__));
}
$post_params = file_get_contents('php://input');
if(!empty($post_params))
{
    if(is_array($post_params))
    {
        m_config::base_write_log('message','request',"{$time}接收stream内容".var_export($post_params,true),m_config::return_child_path(__FILE__));
    }
    else
    {
        m_config::base_write_log('message','request',"{$time}接收stream内容".$post_params,m_config::return_child_path(__FILE__));
    }
}
else
{
    m_config::base_write_log('message','request',"{$time}无接收stream内容",m_config::return_child_path(__FILE__));
}
m_config::base_write_log('message','request',"-----------{$time}消息接收结束-----------",m_config::return_child_path(__FILE__));
//http公共入口
class http_import extends ns_model\message\message_queue
{
    
}