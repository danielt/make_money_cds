<?php
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.message.message_queue");
\ns_core\m_load::load_np("np_xml2array.class.php");
//soap注入公共入口
class soap_import extends \ns_model\message\message_queue
{
    /**
     *  参数
     * @var unknown
     */
	public $mixed_params = null;
	
	/**
	 * 注入消息队列模板
	 * @var unknown
	 */
	public $arr_in_message = array(
                              'base_info'=>array(
                                      'nns_message_time'=>'', //上游消息时间  年月日时分秒毫秒
                                      'nns_message_id'=>'',  //上游消息ID
                                      'nns_cp_id'=>'', //上游CP标示
                                      'nns_message_xml'=>'',  //上游原始的信息  可以为字符串 | xml | json
                                      'nns_message_content'=>'',  //上游解析后的信息  可以为字符串 | xml | json  生成队列的基本信息
                                      'nns_action'=>'', //操作 行为
                                      'nns_type'=>'', //消息 类型
                                      'nns_name'=>'',  //消息名称
                                      'nns_package_id'=>'',  //包ID
                                      'nns_xmlurlqc'=>'', //广州电信悦ME MD5摘要
                                      'nns_encrypt'=>'', //广州电信悦ME 加密串
                                      'nns_content_number'=>'', //xml文件中包含的内容数量
                              ), //基本信息（存储于nns_mgtvbk_message表中）
                     );
	
	/**
	 * 初始化
	 */
	public function __construct()
	{
	    \ns_core\m_load::load_np("np_xml_to_array.class.php");
	    m_config::get_dc();
	    m_config::base_write_log('message','request',"-----------消息接收开始-----------",m_config::return_child_path(__FILE__));
	    $data['requset'] = $_POST;
	    m_config::base_write_log('message','request',"接收POST消息：". var_export($data, true),m_config::return_child_path(__FILE__));
        $xml = file_get_contents('php://input');
        m_config::base_write_log('message','request',"接收流文件消息：". var_export($data, true),m_config::return_child_path(__FILE__));
        $data['input']=$xml;
		$this->mixed_params = $data;
	}
	
	/**
	 * 注入
	 * @return multitype:number |string
	 */
	public function ContentDeployReq($CMSID, $SOPID, $CorrelateID, $ContentMngXMLURL)
	{
	    $this->arr_in_message['base_info']['nns_message_xml'] = is_array($this->mixed_params) ? json_encode($this->mixed_params,JSON_UNESCAPED_UNICODE) : $this->mixed_params;
		$this->arr_in_message['base_info']['nns_message_id'] = $CorrelateID;
		$this->arr_in_message['base_info']['nns_message_time'] = date("YmdHis");
    	$this->arr_in_message['base_info']['nns_cp_id'] = $CMSID;
    	if(strlen($ContentMngXMLURL) <1)
    	{
		    return $this->exec(m_config::return_data(1,'nns_message_content url为空，不执行'));
    	}
    	$result_content = m_config::get_curl_content($ContentMngXMLURL);
    	if($result_content['ret'] !=0)
    	{
    	    $this->arr_in_message['base_info']['nns_message_state'] = '1';
    	    $this->arr_in_message['base_info']['nns_message_content'] = $result_content['reason'];
    	    $result = $this->push($this->arr_in_message);
//     	    $this->message_action('add',$this->arr_in_message);
    	    return $this->exec($result_content);
    	}
    	if(!isset($result_content['data_info']) || strlen($result_content['data_info']) <1)
    	{
    	    $this->arr_in_message['base_info']['nns_message_state'] = '1';
    	    $this->arr_in_message['base_info']['nns_message_content'] = "获取内容为空地址:[{$ContentMngXMLURL}]";
    	    $result = $this->push($this->arr_in_message);
//     	    $result = $this->message_action('add',$this->arr_in_message);
    	    return $this->exec(m_config::return_data(1,"内容为空：获取内容为空地址:[{$ContentMngXMLURL}]"));
    	}
    	$nns_message_content = $result_content['data_info'];
    	$nns_message_content = trim($nns_message_content);
    	if(strlen($nns_message_content) <1)
    	{
    	    $this->arr_in_message['base_info']['nns_message_state'] = '1';
    	    $this->arr_in_message['base_info']['nns_message_content'] = "获取内容为空地址:[{$ContentMngXMLURL}]";
//     	    $result = $this->message_action('add',$this->arr_in_message);
    	    $result = $this->push($this->arr_in_message);
    	    return $this->exec(m_config::return_data(1,"内容为空：获取内容为空地址:[{$ContentMngXMLURL}]"));
    	}
    	if(!m_config::is_xml($nns_message_content))
    	{
    	    $this->arr_in_message['base_info']['nns_message_state'] = '2';
    	    $this->arr_in_message['base_info']['nns_message_content'] = "非xml内容:[{$nns_message_content}]";
        	$this->arr_in_message['base_info']['nns_message_content'] = $nns_message_content;
            $this->push($this->arr_in_message);
    	    return $this->exec(m_config::return_data(1,'is not xml content'));
    	}
    	$this->exec_ex_data($nns_message_content);
    	#TODO  这儿依据不同内容做解析
    	$this->arr_in_message['base_info']['nns_message_content'] = $nns_message_content;
    	$this->arr_in_message['base_info']['nns_encrypt'] = $ContentMngXMLURL;
        $result = $this->push($this->arr_in_message);
        include_once dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))).'/api_v3/common.php';
        include_once dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))).'/api_v3/gateway/inner_requet/inner_requet.class.php';
        define('PROJECT_INNER_FILE','gateway');
        $arr_temp_send = array(
            'bk_guid'=>$this->arr_in_message['hide_info']['nns_id'],
            'movie_del_data'=>array(),
        );
        $obj_inner_requet = new nl_inner_requet(m_config::get_dc(),$arr_temp_send,'gateway_log','inner_requet');
        $result_send = $obj_inner_requet->send_msp_info();
	    return $this->exec($result);
	}
	
	/**
	 * 处理扩展数据
	 * @param unknown $nns_message_content
	 */
	public function exec_ex_data($nns_message_content)
	{
	    $int_action = $int_type=1;
	    $nns_message_content = m_config::trim_xml_header($nns_message_content);
	    //解析下载的XML
	    $dom = new DOMDocument('1.0', 'utf-8');
	    $dom->loadXML($nns_message_content);
	    $xml = $dom->saveXML();
	    $xml_arr = np_xml2array::getArrayData($xml);
	    $Objects = null;
	    if(isset($xml_arr['data']['children']) && is_array($xml_arr['data']['children']) && !empty($xml_arr['data']['children']))
	    {
	        foreach ($xml_arr['data']['children'] as $v)
	        {
	            if (empty($Objects) && $v['name'] == 'Objects')
	            {
	                $Objects = $v['children'];
	            }
	        }
	    }
	    if (!empty($Objects) && is_array($Objects))
	    {
	        $str_uuid = np_guid_rand();
            $this->arr_in_message['hide_info']['nns_id'] = $str_uuid;
	        foreach ($Objects as $obj)
	        {
	            $int_action = (isset($obj['attributes']['Action']) && strtoupper($obj['attributes']['Action']) == 'DELETE') ? 3 : ((isset($obj['attributes']['Action']) && strtoupper($obj['attributes']['Action']) == 'UPDATE') ? 2:1);
	
	            //分集
	            if ($obj['attributes']['ElementType'] == 'Program')
	            {
	                $int_type=2;
	            }
	            //片源
	            else if ($obj['attributes']['ElementType'] == 'Movie')
	            {
	                $int_type=3;
	            }
	            //直播频道
	            else if ($obj['attributes']['ElementType'] == 'Channel')
	            {
	                $int_type=4;
	            }
	            //直播片源
	            else if ($obj['attributes']['ElementType'] == 'PhysicalChannel')
	            {
	                $int_type=6;
	            }
	            //节目单
	            else if ($obj['attributes']['ElementType'] == 'ScheduleRecord')
	            {
	                $int_type=7;
	            }
	        }
	    }
        $this->arr_in_message['base_info']['nns_message_state'] = 0;
	    $this->arr_in_message['base_info']['nns_action'] = $int_action;
	    $this->arr_in_message['base_info']['nns_type'] = $int_type;
	    return ;
	}
	
	
	
	
	/**
	 * 
	 * @param unknown $result
	 * @param unknown $params
	 */
	public function exec($result)
	{
	    m_config::base_write_log('message','request',"注入结果：".var_export($result,true),m_config::return_child_path(__FILE__));
    	return array (
	            'ResultCode' => $result['ret'] == '0' ? 0 : -1,
	            'ErrorDescription' => $result['reason']
	        );
	}
	/**
	 * 类销毁
	 */
	public function __destruct()
	{
	    m_config::base_write_log('message','request',"-----------消息接收结束-----------",m_config::return_child_path(__FILE__));
	}
}
$soap_import = new soap_import();
$result = $soap_import->ContentDeployReq('cp_test','starcor','5c123e418fcd81b314020f3897f5e460','ftp://ftp_user:ftp_user@192.168.70.61/upload/simulate/cp_test/20181213/5c123e418fcd81b314020f3897f5e469.xml');
echo json_encode($result);die;


// global $g_bk_web_url;
// $bk_web_url = $g_bk_web_url;
// unset($g_bk_web_url);
// $bk_web_url = (isset($bk_web_url) && strlen($bk_web_url)) ? $bk_web_url : '';
// $bk_web_url = trim(trim($bk_web_url,'/'),'\\');
// $bk_web_url.="/v2/ns_api/telecom/message/cp_test/public/soap_import.wsdl";
$server = new SOAPServer($bk_web_url);
$server->setClass('soap_import');
$server->handle();