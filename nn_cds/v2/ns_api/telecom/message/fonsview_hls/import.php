<?php
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.message.message_queue");

$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('SOURCE_ID', $str_dir);

$soap_params = file_get_contents('php://input');
\m_config::write_message_receive_log("消息接收开始：".var_export($soap_params,true), SOURCE_ID);
unset($soap_params);

class import extends \ns_model\message\message_queue
{
    public $arr_in_message = array(
        'base_info'=>array(
            'nns_message_time'=>'', //上游消息时间  年月日时分秒毫秒
            'nns_message_id'=>'',  //上游消息ID
            'nns_cp_id'=>'', //上游CP标示
            'nns_message_xml'=>'',  //上游原始的信息  可以为字符串 | xml | json 或则存放消息地址
            'nns_message_content'=>'',  //上游解析后的信息  可以为字符串 | xml | json  生成队列的基本信息
            'nns_action'=>'', //操作 行为
            'nns_type'=>'', //消息 类型
            'nns_name'=>'',  //消息名称
            'nns_package_id'=>'',  //包ID
            'nns_xmlurlqc'=>'', //广州电信悦ME MD5摘要
            'nns_encrypt'=>'', //广州电信悦ME 加密串
            'nns_content_number'=>'', //xml文件中包含的内容数量
        ), //基本信息（存储于nns_mgtvbk_message表中）
    );

	public function ContentPreloadReq($CMSID,$CorrelateID,$ContentMngXMLURL)
	{
        $this->arr_in_message['base_info']['nns_message_xml'] = json_encode(func_get_args());
	    if(empty($CMSID) || empty($CorrelateID) || empty($ContentMngXMLURL))
        {
            \m_config::write_message_receive_log("消息接收结束：SOAP注入缺少必传参数",SOURCE_ID);
            return array('ResultCode' => -1, 'ErrorDescription' => '缺少必传参数');
        }
        if($CMSID != SOURCE_ID)
        {
            \m_config::write_message_receive_log("消息接收结束：CMSID未注册",SOURCE_ID);
            return array('ResultCode' => -1, 'ErrorDescription' => 'CMSID未注册');
        }
        $this->arr_in_message['base_info']['nns_cp_id'] = SOURCE_ID;
        $this->arr_in_message['base_info']['nns_message_id'] = $CorrelateID;
        //获取ftp 、http 文件内容
        $result_content = \m_config::get_curl_content($ContentMngXMLURL);
        if($result_content['ret'] != 0)
        {
            \m_config::write_message_receive_log("消息接收结束：工单下载失败".$ContentMngXMLURL,SOURCE_ID);
            return array('ResultCode' => -1, 'ErrorDescription' => '任务工单下载失败');
        }
        else if(!isset($result_content['data_info']) || strlen($result_content['data_info']) < 1)
        {
            \m_config::write_message_receive_log("消息接收结束：工单下载成功但工单内容为空".$ContentMngXMLURL,SOURCE_ID);
            return array('ResultCode' => -1, 'ErrorDescription' => '工单下载成功但工单内容为空');
        }
        else
        {
            $this->arr_in_message['base_info']['nns_message_content'] = $result_content['data_info'];
        }
        //读取毫秒级时间
        list ($usec, $sec) = explode(' ', microtime());
        $time = str_pad(intval(substr($usec, 2, 4)),4,'0',STR_PAD_LEFT);
        $this->arr_in_message['base_info']['nns_message_time'] = date('YmdHis',time()) . $time;

        //消息队列入库方法
        \m_config::write_message_receive_log("工单入库内容为:".var_export($this->arr_in_message['base_info'],true),SOURCE_ID);
        $push_re = $this->push($this->arr_in_message);
        \m_config::write_message_receive_log("消息入库状态:".var_export($push_re,true),SOURCE_ID);
        if($push_re['ret'] != 0)
        {
            \m_config::write_message_receive_log("消息接收结束：工单入库失败",SOURCE_ID);
            return array('ResultCode' => -1, 'ErrorDescription' => '工单入库失败');
        }
        \m_config::write_message_receive_log("消息接收结束：ok",SOURCE_ID);
        return array('ResultCode' => 0, 'ErrorDescription' => 'ok');
    }
}

global $g_bk_web_url;
$wsdl_addr = trim(trim($g_bk_web_url, '/'), '\\') . "/v2/ns_api/telecom/message/fonsview_hls/ContentPreload.wsdl";

$server = new SoapServer($wsdl_addr);
$server->setClass("import");
$server->handle();