<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/9/4 14:21
 */
header('Content-Type: text/xml; charset=utf-8');
error_reporting(0);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/common.php";
\ns_core\m_load::load("ns_model.message.message_explain");
//soap公共入口
class soap_import
{
    /**
     * 初始化
     */
    public function __construct()
    {
        \ns_core\m_load::load_np("np_xml_to_array.class.php");
        m_config::get_dc();
    }
    public function ExecCmd($CSPID, $LSPID, $CorrelateID, $CmdFileURL)
    {
        $arg_list = func_get_args();
        //nn_log::write_log_message(LOG_MODEL_ASSET_IMPORT, 'ExecCmd request args:' . var_export($arg_list, true), ORG_ID);
        $params = array (
            'msgid' => $CorrelateID,
            'url' => $CmdFileURL,
            'cp_id' => $CSPID,
            'lp_id' => $LSPID
        );
        $ret = array (
            'Result' => -1,
            'ErrorDescription' => 'import fail'
        );
        if (!isset($params['cp_id']) || strlen($params['cp_id']) < 1)
        {
            //nn_log::write_log_message(LOG_MODEL_ASSET_IMPORT, 'cp_id为空,参数为：' . var_export($params, true), ORG_ID);
            $ret['ErrorDescription'] = 'empty CSPID';
            return $ret;
        }
        $result_cp = m_config::_get_cp_info($params['cp_id']);
        if ($result_cp['ret'] != 0)
        {
            //nn_log::write_log_message(LOG_MODEL_ASSET_IMPORT, $result_cp['reason'], ORG_ID);
            $ret['ErrorDescription'] = 'db query cpid error';
            return $ret;
        }
        if (empty($result_cp['data_info']['nns_config']))
        {
            //nn_log::write_log_message(LOG_MODEL_ASSET_IMPORT, '查询无CP配置，结果为：' . var_export($result_cp['data_info']['nns_config'], true), ORG_ID);
            $ret['ErrorDescription'] = 'db query cpid no config';
            return $ret;
        }
        $params_p = array(
            'cmspost' => $params,
        );
        $_POST = $params_p;
        include_once dirname(__FILE__) . "/import.php";
        $http_import = new http_import();
        $result_import = $http_import->import();

        return $ret = ($result_import == true) ? array("Result"=>0,"ErrorDescription"=>"import ok") : array("Result"=>-1,"ErrorDescription"=>"import fail");
    }
}
$server = new SOAPServer('http://120.205.22.62:37100/xjcbc_bk/nn_cds/v2/ns_api/xjzg/message/koznakyd/service.php?wsdl');
$server->setClass('soap_import');
$server->handle();