<?php
ini_set('display_errors',1);
set_time_limit(0);
include_once  dirname(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))).'/mgtv_v2/mgtv_init.php';
define('ORG_ID', 'cbczq_zte');
include_once dirname(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))) . '/mgtv_v2/'.ORG_ID.'/init.php';
include_once  dirname(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))).'/mgtv_v2/'.ORG_ID.'/define.php';
include_once dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))."/common.php";
define('CSPID', 'KKTVITVCMS');
define('LSPID', 'ZTEAdapter');
ini_set("soap.wsdl_cache_enabled", "0");
class cbczq_zte_client{
    const WSDL = 'http://127.0.0.1/nn_cds/v2/ns_api/xjzg/delivery/zhongguangchuanbo/soap/cbczq_zte_service.php?wsdl';
    static public function ExecCmd($CorrelateID,$CmdFileURL){
        $arg_list = func_get_args();
        m_config::write_cdn_execute_log('执行CDN注入请求参数为：ExecCmd params:' . var_export($arg_list,true), ORG_ID, $CorrelateID);
        $client = self::get_soap_client();
        $ret = $client->ExecCmd(CSPID,LSPID,$CorrelateID,$CmdFileURL);
        m_config::write_cdn_execute_log('执行CDN注入结果为：' . var_export($ret,true), ORG_ID, $CorrelateID);
        return $ret;
    }
    public static function get_soap_client(){
        return new SOAPClient(self::WSDL,array('trace'=>true));
    }
}
