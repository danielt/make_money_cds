<?php
ini_set("soap.wsdl_cache_enabled", "0");
include_once dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))."/common.php";

class OTT_hndx{
	public function ExecCmd($CSPID,$LSPID,$CorrelateID,$CmdFileURL){
		$arg_list = func_get_args();
        m_config::write_cdn_execute_log('CDN响应请求参数为：ExecCmd params:' . var_export($arg_list,true), $CSPID, $CorrelateID);
		return array('Result'=>0,'ErrorDescription'=>'ok');
	}
}
$server = new SOAPServer('zte.wsdl');
$server->handle();
