<?php
ini_set("soap.wsdl_cache_enabled", "0");
include_once dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))."/common.php";
class OTT_hnlt_zx
{
    public function ExecCmd($CSPID, $LSPID, $CorrelateID, $CmdFileURL)
    {
        $arg_list = func_get_args();
        \m_config::write_cdn_execute_log('执行CDN注入请求参数为：ExecCmd params:' . var_export($arg_list,true), ORG_ID, $CorrelateID);
        return array('Result'=>0, 'ErrorDescription'=>'注入cdn成功,等待cdn反馈');
    }
}
$server = new SOAPServer('hw.wsdl');
//$server->setClass('OTT_hnlt_zx');
$server->handle();