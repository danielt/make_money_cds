<?php
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('display_errors',1);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))."/common.php";
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
array_pop($arr_dir);
$str_dir = array_pop($arr_dir);
define("ORG_ID", $str_dir);
$postdata = file_get_contents("php://input");
\m_config::write_cdn_notify_log('接收到CDN消息反馈数据为:' . $postdata, ORG_ID, '');

class hnlt_notify{
    public function ResultNotify($CSPID, $LSPID, $CorrelateID, $CmdResult, $ResultFileURL)
    {
        $delivery = \ns_core\m_load::load_cdn_delivery_object(__FILE__);
        $arg_list = func_get_args();
        $msg = "WSDL处理参数：" . var_export($arg_list, true);
        \m_config::write_cdn_notify_log($msg, ORG_ID, '');
        $ret = $delivery->notify($CorrelateID, $CmdResult, $ResultFileURL);
        if($ret['ret'] != NS_CDS_SUCCE)
        {
            return array('Result' => 0, 'ErrorDescription' => $ret['reason']);
        }
        else
        {
            return array('Result' => -1,'ErrorDescription'=>$ret['reason']);
        }
    }
}

//ob_start();
$server = new SOAPServer('hnlt_notify.wsdl');
$server->setClass('hnlt_notify');
$server->handle();
