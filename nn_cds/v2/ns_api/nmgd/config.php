<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/12/18 10:46
 */
/**
 * 说明：后期项目配置cp/sp的定制话配置，都在此配置文件中配置，具体配置视情况而定
 * 读取的时候，在分发器里面有，get_project_config($key)方法，
 * 而且在v2/ns_core/m_config.php中有封装的读取方法get_project_config($project, $key)方法
 */
/*********************这里只是记录老版本播控cp/sp的所有配置 start****************************/
$sp_config = array(
    'import_id_mode' => "11",//CDN注入ID模式,(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;11:湖南注入中兴cdn专用)
    'epg_video_import_id_mode' => "0",//EPG主媒资注入ID模式,(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;)
    'epg_index_import_id_mode' => "0",//EPG分集注入ID模式,(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;)
    'epg_media_import_id_mode' => "11",//EPG片源注入ID模式,(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;11:湖南注入中兴cdn专用)
    'epg_live_import_id_mode' => "",//EPG直播频道注入ID模式,(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;)
    'epg_live_index_import_id_mode' => "",//EPG直播分集注入ID模式,(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;)
    'epg_live_media_import_id_mode' => "",//EPG直播源注入ID模式,(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;)
    'epg_playbill_import_id_mode' => "",//EPG节目单注入ID模式,(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;)
    'epg_index_delete_id_mode' => "",//EPG删除动作ID模式,(0:guid;1:原始id;(目前老版本配置1，下发删除分集时根据上游原始id删除（芒果用到）))
    'message_feedback_enabled' => "0",//消息反馈开关,【1关闭（消息反馈关闭），0开启【消息反馈开启】由于加了播控注入播控逻辑，上游播控可以关闭消息反馈】
    'message_feedback_mode' => "0",//消息反馈模式,0 原反馈模式 ,1 CP反馈模式
    'cdn_message_feedback_model' => "",//消息反馈上游协议,http、或者soap（目前老版本用到，新版本根据项目定制化填写消息）
    'site_id' => "xxx",//站点ID（芒果用到）
    'site_callback_url' => "xxx",//消息反馈接口
    'import_csp_id' => "",//内容提供商CSPID
    'asset_retrieve_config' => "",//媒资恢复配置(1:主媒资,2:分集,3:片源：多个以逗号分隔)
    'global_config_bind_key' => "1,",//通用配置分组绑定(全局绑定配置项)
    'import_ratebit_enabled' => "0",//码率配置开关：0关闭任何片源码率都通过该SP，1开启了就通过片源码率开始值和结束值判断
    'import_ratebit_start' => "0",//片源码率开始值，如果为配置则片源码率无下限，如果配置了片源码率至少大于等于该开始值
    'import_ratebit_end' => "10000",//片源码率结束值，如果为配置则片源码率无上限，如果配置了片源码率至少小于等于该开始值
    'online_disabled' => "0",//媒资上下线开关，0 为关闭媒资上下线,1 为开启媒资上下线
    'asset_unline_model' => "0",//媒资上下线注入模式，0|未配置 redis消息队列上下线控制,1 epg注入上下线控制
    'online_audit' => "0",//审核开关，0 为进入媒资上下线为自动审核，1 为未审核，2为下线未审核/上线自动审核
    'check_video_statuc_enabled' => "0",//验证最终状态开关，是否验证影片属于注入最终状态 0 是 1否
    'notify_online_queue_enabled' => "0",//是否通知上下线队列开关，操作队列注入为最终状态是否通知上下线队列0是 1否
    'third_category_enabled' => "0",//第三方媒资上下线关系注入开关，是否属于第三方媒资上下线关系注入 0 默认关闭 1开启
    'online_send_way' => "0",//EPG上下线下发方式，EPG上下线下发方式 0 ftp 1 http
    'online_api' => "",//媒资上下线EPG注入接口，媒资上下线注入接口url地址
    'online_notify_enabled' => "",//是否反馈上游，0是 1否
    'online_notify_url' => "",//通知上游媒资上下线URL地址
    'import_op_enabled' => "0",//中心注入指令开关，0 开启，1关闭【关闭后不再注入中心注入指令，默认开启】（新版没有中心注入指令，但这里必须开启）
    'op_queue_model' => "0",//中心注入指令消息处理模式，0 播控作为TV平台方（原逻辑）, 1 播控作为CDN平台方（新逻辑）【播控以不同的角色展示给客户，即可以作为CDN平台，也可以作为TV平台】
    'import_op_pre_max' => "20",//中心注入指令下发条数,【每次取多少个操作任务（不设置为20）】
    'op_queue_video_enabled' => array(//队列允许通过的媒资开关【未存在数组队列不在接收此类型文件，默认全存在】
                                      '0' => "video",//主媒资
                                      '1' => "index",//分集
                                      '2' => "media",//片源
                                      '3' => "live",//直播频道
                                      '4' => "live_index",//频道分集
                                      '5' => "live_media",//直播源
                                      '6' => "playbill",//节目单
                                      '7' => "file",//文件
                                      '8' => "category",//资源库栏目
                                      '9' => "product",//产品包
                                      '10' => "seekpoint",//打点信息
                                      '11' => "epg_file",//EPGFile模板文件
    ),
    'op_queue_filetype_enabled' => array(//队列允许通过的片源文件类型【未存在数组队列不在接收此片源类型文件，默认全存在】
                                         '0' => "ts",
                                         '1' => "m3u8",
                                         '2' => "flv",
                                         '3' => "mp4",
                                         '4' => "mp3",
                                         '5' => "mpg",
                                         '6' => "mts",
    ),
    'media_import_op_mode' => array(//队列不允许通过的片源清晰度【已存在数组中的清晰度不会生成中心注入队列，默认全部通过即数组为空】
                                    //'0' => "0",//低清(low)
                                    //'1' => "1",//标清(std)
                                    //'2' => "2",//高清(hd)
                                    //'3' => "3",//超清(sd)
                                    //'4' => "4",//4K(4k)
    ),
    'op_queue_media_type_enabled' => array(//队列允许通过的片源类型开关【未存在数组中的不在接收此片源类型文件，默认全部通过即数组为空】
                                           '0' => "0",//原始片源
                                           '1' => "1",//转码片源
                                           '2' => "2",//CDN片源
    ),
    'op_queue_media_tag_enabled' => array(//队列允许通过的片源终端TAG开关【未存在数组不在接收此片源类型文件，默认全存在，如果片源tag标志为空则默认全部通过，无论设置与否】
                                          '0' => "26",//STB
                                          '1' => "27",//IPHONE
                                          '2' => "28",//IPAD
                                          '3' => "29",//ANDROID_PHONE
                                          '4' => "30",//ANDROID_PAD
                                          '5' => "31",//PC
    ),
    'op_queue_enabled' => "1",//中心同步指令开关,0 关闭,1 开启【关闭后不再注入中心同步指令,默认关闭】
    'down_url' => "http://127.0.0.3/nn_cds/down.php?sp_id=hndx&",//中心片库下载地址虚拟接口,一般配置为：http://{平台IP}/nn_cds/down.php?sp_id={SP ID}&
    'op_pre_max' => "20",//中心同步指令下发条数【每次取多少个操作任务（不设置为100）】
    'import_task_clear' => "",//中心注入指令清除非消息任务【0或者不设置不清除中心注入任务非消息任务，1为清除】
    'op_audit_disable' => "",//中心同步指令自动审核【是否关闭自动审核，若为1则所有操作队列进来状态为待审核状态，0则为通过审核】
    'close_queue' => "",//中心同步指令暂停下发【关闭列队任务[0:开启，1:关闭]】
    'sp_default_weight' => "",//中心同步指令存量任务权重【注入存量任务权重】
    'op_task_freeze_time' => "",//中心同步指令冻结时间【冻结时间，这个时间以后的全部冻结 格式：YYYY-mm-dd HH:ii:SS】
    'disabled_sp_queue' => "0",//中心同步指令仅同步删除操作【关闭sp操作队列（0为否，1为是）】
    'op_task_order' => "",//中心同步指令操作排序
    'virtual_destroy_task' => "",//中心同步删除指令生成C2虚拟任务【0:关闭;1:开启（当删除指令在C2中无历史注入任务时，关闭则删除当前删除指令，开启则生成C2虚拟任务）】
    'create_media_mode' => "",//同步指令创建片源模式【0或者未配置:原模式,不生成新的片源 ; 1:以这个片源为准创建一个新的片源放入队列中,片源生成后片源类型变为2（CDN片源注入），原片队列不会再下发下去】
    'media_service_type' => "http",//片源服务类型【片源服务类型标示；同步指令创建片源模式为1后这个参数值必填】
    'clip_set_playurl' => "",//切片播出地址拼接（前缀地址）【有些CDN直接通过磁盘挂载到我方切片服务器，此配置直接配播出地址】
    'fuse_platform_mark' => "xxx",//融合平台标示(SP GUID)【值不为空则可以反向查询出SP（GUID）融合平台的队列数据，配置数据，为空则什么都不处理】
    'clip_import_model' => "0",//片模式【0或则未配置 老模式切片下发 | 1 资源库中片源查询切片下发且只下载不切片】
    'clip_cp_id_sent_model' => "",//切片CPID下发模式【0或则未配置 import_source(片源) | 1 producer（主媒资）】
    'clip_pre_max' => "100",//切片任务下发条数【切片每次从操作列队获取多少个（不设置为100）】
    'clip_timeout' => "60",//切片任务重发时间间隔【获取多少秒之前的失败命令进行重发】
    'is_starcor_telecom' => "",//切片任务下发模式【0或未配置非电信模式，1电信模式(注入到starcor CDN下发domin和cms_id)】
    'disabled_clip' => "0",//切片任务暂停【0:开启BK切片任务;1:关闭BK切片任务且不生成注入队列;2:关闭BK切片任务且生成注入队列;3:切片指令栏目展示且不生成注入队列，切片栏目只提供展示，且CDN拉片在片源操作只下载片源】
    'check_file_exsist' => "",//检查片源是否已经下载过【0或者未配置:不检查,继续下载切片；1:要检查，如果片源存在则不再下载切片，队列为等待切片，如果不存在则下载切片，队列为等待注入】
    'clip_file_repeat_enable' => "0",//是否重新切片,默认0，0否 1是【获取切片文件失败后，是否重新切片】
    'upload_clip_to_ftp_enabled' => "0",//下载or切片完成是否上传至FTP【空或0表示否，默认0，0否 1是】
    'upload_clip_to_ftp_dir_mode' => "",//【未配置或配置成0：不额外指定FTP目录，上传至配置的FTP绝对地址 ； 1：根据片源的nns_ext_url字段进行额外目录的指定，需注意ext_url需为字符串】
    'upload_clip_to_ftp_url' => "",//下载or切片完成上传至FTP地址,绝对路径
    'clip_file_encode_enable' => "0",//转码开关,【切片完成后是否需要转码 1 开启:需要;0 关闭:不需要】
    'clip_file_encode_model' => "1",//转码模式,0 切片与转码在同个队列中,1 切片与转码队列分离,2 不需要切片就可以转码; 【转码模式机制 兼容以前重庆有线转码队列】
    'transcode_load_balance_encode' => "",//转码负载均衡开关【转码负载均衡配置 0或者未配置 关闭 | 1 开启】
    'clip_encode_model' => "",//转码请求厂商模式【0或者未配置 锐马视讯转码；1大洋转码；2云视转码】
    'clip_encode_wait_max' => "",//等待转码条数限制【配置后允许等待转码并发的条数，未配置默认为5】
    'clip_encode_fail_max' => "",//转码失败条数限制【配置后允许转码失败后重发并发的条数，未配置默认失败数据不处理】
    'clip_encode_fail_max_ex' => "",//转码失败执行次数最大限制【配置后允许转码失败后小于该值的次数，未配置默认失败数据不处理】
    'clip_file_addr_input' => "",//转码片源输入地址【转码片源输入的地址目前只支持FTP,运维必须把目录置为可读可写】
    'clip_file_addr_output' => "",//转码片源输出地址【转码片源输出的地址目前只支持FTP,运维必须把目录置为可读可写】
    'drawing_frame_addr_input' => "",//转码抽帧图片输入地址【转码抽帧图片输入的地址目前只支持FTP,运维必须把目录置为可读可写】
    'drawing_frame_addr_output' => "",//转码抽帧图片输出地址【转码抽帧图片输出的地址目前只支持FTP,运维必须把目录置为可读可写】
    'clip_file_encode_way' => "post",//post, get【转码API请求方式,默认为POST】
    'clip_file_encode_api_url' => "",//【转码API的请求地址】
    'media_encode_model' => "",//切片转码模式【0或者未配置:下载完后原片也保留继续走队列;1:原片处理完毕后，原片的队列直接删除，只允许转码后的影片继续走队列】
    'c2_import_cdn_model' => "",//C2指令注入模式【0或则为配置老逻辑c2注入CDN;1新的C2逻辑注入;2视达科MSP注入模式;3江苏广电茁壮注入模式;4视达科标准注入CDN模式;5江苏昆山注入模式;】
    'c2_import_cdn_mapping_model' => "",//C2图片切换mapping模式【0或则未配置以标准C2标准生成mapping;1 以视达科标准生成mapping 当C2指令注入模式为4才用到，后续厂家听话了这个可以去掉】
    'c2_import_push_dst_url' => "",//C2指令推送到目的FTP地址【目前昆山广电用到,上游播控只能单向外推数据的情况用到】
    'c2_import_push_dst_scan_url' => "",//C2指令推送到目的扫描FTP地址【目前昆山广电用到,上游播控只能单向外推数据的情况用到】
    'c2_import_push_go_url' => "",//C2指令片源推送到GO工具的http地址【目前昆山广电用到,上游播控只能单向外推数据的情况用到】
    'c2_import_push_media_base_url' => "",//C2指令片源相对服务器的基本路径【目前昆山广电用到,上游播控只能单向外推数据的情况用到,可配置为空】
    'c2_get_third_play_url' => "",//C2指令片源获取第三方播放串请求地址【目前昆山广电用到,下游播控注入CDN，CDN自己扫描文件，播控自己拉取播放串情况使用】
    'c2_import_asset_url' => "",//C2指令片源内容发布请求注入地址【目前南京广电用到,下游播控注入CDN，xml、ts等文件上传后还需要注入指令才能到VOD平台上去】
    'cdn_video_max' => "",//C2注入主媒资队列上限,不设置为100个
    'cdn_index_check_video_enabled' => "0",//C2分集注入是否检查主媒资是否注入成功【1 开启:需要检查;0 关闭:不需要检查】
    'cdn_index_max' => "",//C2注入分集队列上限,不设置为100个
    'cdn_index_sequence_max' => "",//C2注入分集分集号上限,不设置为999集
    'cdn_media_check_video_enabled' => "0",//C2片源注入是否检查主媒资是否注入成功【1 开启:需要检查;0 关闭:不需要检查】
    'cdn_media_check_index_enabled' => "0",//C2片源注入是否检查分集是否注入成功【1 开启:需要检查;0 关闭:不需要检查】
    'cdn_media_max' => "",//C2注入片源队列上限,不设置为100个
    'c2_count_mode' => "",//C2任务总数限制模式,0:等待执行;1:执行失败;2:正在执行;3:等待获取CDN播放串;4:注入取消;5:正在获取CDN播放串;6:获取播放传失败(多个以逗号,分隔)
    'c2_play_url_ip' => "",//C2任务跨域地址【CDN拼串跨域配置，只支持单个IP】
    'c2_media_notify_mode' => "",//C2片源消息反馈播放串模式【0或者为配置:不处理消息反馈的任何播放串; 1:反馈的播放地址 ; 2:反馈播放ID】
    'cdn_media_playurl_by_csv' => "0",//C2读取CSV文件获取片源播放串,0 关闭;1 开启【目前天津联通用到,C2指令注入模式为[5]时用到】
    'disabled_cdn' => "",//CDN注入暂停, 1为关闭CDN注入，0或则不设置为开启CDN注入
    'cdn_import_state_model' => "",//CDN注入公共状态模式,【0或者未配置原始模式状态都由公共代码部分控制，1自身逻辑控制CDN状态】
    'cdn_assets_enabled' => array(//CDN允许注入媒资开关【未存在数组，不运行注入队列】
                                  '0' => "video",//主媒资
                                  '1' => "index",//分集
                                  '2' => "media",//片源
                                  '3' => "live",//直播频道
                                  '4' => "live_index",//频道分集
                                  '5' => "live_media",//直播源
                                  '6' => "playbill",//节目单
                                  '7' => "file",//文件
                                  '8' => "category",//资源库栏目
                                  '9' => "product",//产品包
                                  '10' => "seekpoint",//打点信息
                                  '11' => "epg_file",//EPGFile模板文件
    ),
    'disabled_category' => "",//CDN注入栏目关闭,1为关闭注入，0或则不设置为开启注入
    'cdn_display_import' => "",//不显示CDN注入,若为1则隐藏，0或者空显示
    'cdn_exec_num' => "",//CDN注入条数,CDN注入条数，默认100
    'cdn_notify_third_party_enable' => "0",//CDN是否通知第三方外部厂家【1 开启:需要通知;0 关闭:不需通知，默认关闭 （目前新疆电信对接福富用到）】
    'cdn_notify_third_party_mark' => "",//第三方外部厂家对接标示【通知第三方外部厂家开启后必须配置，不能为空 （目前新疆电信对接福富用到）】
    'cdn_send_mode' => "0",//发送模式 0 soap;1 http;2 ftp【发送消息到第三方CDN厂家的请求协议 默认SOAP】
    'cdn_send_mode_url' => "",//【使用HTTP的方式注入CDN的URL请求地址，只是注入媒资元数据】
    'cdn_send_cdi_mode_url' => "",//【使用HTTP的方式注入CDN的URL请求地址，只是注入媒资元数据的上下线操作】
    'ftp_cdn_send_mode_status' => "",//使用FTP的方式注入CDN，默认0或不配置上传FTP即注入成功，配置1为FTP需异步通知注入状态】
    'ftp_cdn_send_mode_url' => "",//使用FTP的方式注入CDN的URL】
    'xml_ftp' => "ftp://starcor:dengxinxin@172.31.14.239/ftp",//使用soap注入,CDN注入XML指令FTP地址
    'media_ftp' => "ftp://starcor:dengxinxin@172.31.14.239/ftp/ftp",//使用soap注入,CDN注入片源FTP地址
    'cdn_check_ts_accord_address' => "",//CDN注入片源FTP检测地址,FTP下载和检测地址不一致时配置该配置(主动模式)
    'img_ftp' => "",//CDN注入图片FTP地址
    'cdn_import_serise' => "",//CDN注入三层结构,CDN默认连续剧三层结构，若为1则三层，0则为默认
    'movie_import_cdn_enabled' => "",//电影CDN注入两层结构,1为当CDN注入为非三层结构时电影注入CDN两层结构模式，0默认失效
    'cdn_import_media_mode' => "",//CDN注入片源模式,片源注入模式（2单片码率最高,1混合模式，0单独模式）
    'cdn_import_media_sort' => "",//CDN注入片源先加后删,同分集片源注入排序方式【0或则不设置按照列队排序，1先添加再删除，保证EPG无缝注入】
    'after_execute_sp' => "",//后置CDN配置,不设置标示没有
    'before_execute_sp' => "",//前置CDN配置,不设置标示没有
    'cdn_query_mediaurl' => "",//CDN异步获取播放串,是否需要异步获取播放串：0或则不设置标示不获取，1标示需要获取
    'cdn_update_index_message_waiting' => "",//CDN分集信息更新下发等待片源注入完成,分集信息更新是否等待片源注入CDN完成：0开启，1关闭
    'cdn_single_log_mode' => "",//CDN单日志模式,CDN单日志模式：0开启，1关闭
    'cdn_fail_retry_mode' => "",//CDN失败重试模式,0,根据CDN失败次数下发等待注入的任务,默认不重试
    'cdn_fail_retry_time' => "",//CDN失败重试次数,默认0
    'cdn_exec_fail_num' => "",//CDN失败重试条数,默认20
    'pass_queue_disabled' => "1",//透传队列开关,0 关 1开
    'pass_queue_audit' => "1",//透传队列是否开启自动审核,0 关 1开
    'pass_queue_import_url' => "http://127.0.0.1/nn_cms/api/k/index.php",//透传队列注入地址
    'disabled_epg' => "0",//EPG注入暂停,1为关闭EPG注入，0或则不设置为开启EPG注入
    'epg_assets_enabled' => array(//EPG允许注入媒资开关【未存在队列不运行注入队列】
                                  '0' => "video",//主媒资
                                  '1' => "index",//分集
                                  '2' => "media",//片源
                                  '3' => "live",//直播频道
                                  '4' => "live_index",//频道分集
                                  '5' => "live_media",//直播源
                                  '6' => "playbill",//节目单
                                  '7' => "file",//文件
                                  '8' => "category",//资源库栏目
                                  '9' => "product",//产品包
                                  '10' => "seekpoint",//打点信息
                                  '11' => "epg_file",//EPGFile模板文件
    ),
    'epg_display_import' => "0",//不显示EPG注入,若为1则隐藏，0或者空显示
    'epg_display_import_delete' => "0",//不显示EPG删除按钮,若为1则隐藏，0或者空显示
    'epg_import_model' => "",//注入EPG新方式,【0或者空老逻辑注入cms,1 新逻辑注入cms】
    'epg_single_index_import_model' => "",//多剧集单集注入方式,【0或者未配置为单集主媒资注入CMS；1为注入分集不注入单集主媒资（仅限多剧集以单集形式存在才配置）】
    'epg_pre_max' => "100",//EPG注入下发条数,EPG每次下发的个数（不设置为100）
    'crond_epg_minute' => "1",//EPG注入时间间隔,计划任务EPG多少分钟执行一次(新版没用)
    'import_srouce_url' => "1",//EPG注入切片路径,0为注入原始相对路径，1为注入切片后相对路径
    'epg_url' => "http://127.0.0.1/nn_cms/nn_cms_manager/service/asset_import/k23/asset_api.php",//EPG注入点播接口
    'epg_playbill_url' => "",//EPG节目单注入接口
    'epg_check_video_online_status' => "",//EPG影片上线状态接口
    'epg_check_video_intact_status' => "",//EPG影片完整性接口
    'import_core_bind_id_enabled' => "",//EPG注入携带CORE绑定ID,0 否；1 是
    'epg_retry_time' => "",//EPG失败重试次条数,默认0不重试
    'epg_fail_max' => "",//EPG失败重试条数,EPG失败重试条数,默认20
    'watch_focus_eq_summary_enabled' => "",//EPG注入将分集的简介同步到看点,0 否；1 是
    'asset_cdn_share_addr' => "",//媒资CDN共享地址【提供CMS方拉取文件的相对地址】
    'video_cdn_model' => "",//主媒资全局注入模式【0或则未设置播控作为CMS转发平台;1播控作为CDN响应平台】
    'index_cdn_model' => "",//分集全局注入模式【0或则未设置播控作为CMS转发平台;1播控作为CDN响应平台】
    'media_cdn_model' => "",//片源全局注入模式【0或则未设置播控作为CMS转发平台;1播控作为CDN响应平台;2播控作为CMS转发平台也作为CDN响应平台，且cdn注入后就反馈上游消息】
    'asset_import_cms_model' => "",//媒资注入CMS模式【0或者为配置原有逻辑注入CMS，1播控注入播控推入跳板机(下游方只能拉取数据的情况，不能上游往下游推送)】
    'delete_ftp_media' => "",//片源完成态是否删除ftp片源文件,0 否；1 是
    'delete_ftp_media_address' => "",//【ftp地址(多个请使用逗号分隔)】
    'video_add' => array(//主媒资添加操作
                         'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                         'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                         'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                         'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
                         'cdn_import_model' => "",//CDN注入,0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入
    ),
    'video_modify' => array(//主媒资修改操作
                            'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                            'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                            'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                            'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
                            'cdn_import_model' => "",//CDN注入,0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入
    ),
    'video_destroy' => array(//主媒资删除操作
                             'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                             'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                             'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                             'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
                             'cdn_import_model' => "",//CDN注入,0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入
    ),
    'index_add' => array(//分集添加操作
                         'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                         'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                         'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                         'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
                         'cdn_import_model' => "",//CDN注入,0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入
    ),
    'index_modify' => array(//分集修改操作
                            'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                            'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                            'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                            'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
                            'cdn_import_model' => "",//CDN注入,0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入
    ),
    'index_destroy' => array(//分集删除操作
                             'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                             'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                             'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                             'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
                             'cdn_import_model' => "",//CDN注入,0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入
    ),
    'media_add' => array(//片源添加操作
                         'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                         'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                         'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                         'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
                         'cdn_import_model' => "",//CDN注入,0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入
    ),
    'media_modify' => array(//片源修改操作
                            'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                            'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                            'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                            'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
                            'cdn_import_model' => "",//CDN注入,0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入
    ),
    'media_destroy' => array(//片源删除操作
                             'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                             'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                             'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                             'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
                             'cdn_import_model' => "",//CDN注入,0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入
    ),
    'live_add' => array(//直播频道添加操作
                        'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                        'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                        'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                        'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
    ),
    'live_modify' => array(//直播频道修改操作
                           'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                           'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                           'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                           'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
    ),
    'live_destroy' => array(//直播频道删除操作
                            'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                            'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                            'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                            'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
    ),
    'live_media_add' => array(//直播源添加操作
                              'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                              'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                              'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                              'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
    ),
    'live_media_modify' => array(//直播源修改操作
                                 'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                                 'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                                 'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                                 'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
    ),
    'live_media_destroy' => array(//直播源删除操作
                                  'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                                  'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                                  'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                                  'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
    ),
    'playbill' => array(//
                        'enabled' => "0",//节目单分组开关【1开启：以直播源分组查询节目单信息，批量注入CDN;0关闭：默认，单条节目单注入】
                        'group_num' => "",//节目单分组获取数据条数【如果节目单分组开关开启这个需要配置，未配置默认100条】
    ),
    'playbill_add' => array(//节目单添加操作
                            'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                            'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                            'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                            'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
    ),
    'playbill_modify' => array(//节目单修改操作
                               'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                               'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                               'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                               'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
    ),
    'playbill_destroy' => array(//节目单删除操作
                                'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                                'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                                'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                                'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
    ),
    'product_add' => array(//产品包添加操作
                           'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                           'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                           'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                           'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
    ),
    'product_modify' => array(//产品包修改操作
                              'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                              'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                              'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                              'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
    ),
    'product_destroy' => array(//产品包删除操作
                               'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                               'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                               'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                               'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
    ),
    'epg_file_add' => array(//EPGFile模板文件添加操作
                            'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                            'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                            'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                            'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
    ),
    'epg_file_modify' => array(//EPGFile模板文件修改操作
                               'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                               'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                               'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                               'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
    ),
    'epg_file_destroy' => array(//EPGFile模板文件删除操作
                                'status' => "1",//最终状态,0为注入CDN和EPG，1为注入CDN，2为注入EPG
                                'priority' => "",//优先执行注入,0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注
                                'audit' => "",//自动审核,若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核
                                'retry_time' => "",//添加失败重试次数,EPG注入添加操作失败重试次数,空则为原逻辑
    ),
);
$cp_config = array(
    "global_cp_config_bind_key" => "3,",//通用配置分组绑定
    "message_import_message_model" => "",//消息队列获取消息ID模式
    "message_playbill_import_enable" => "1",//节目单队列注入开关
    "message_import_mode" => "",//片源队列注入开关
    "message_import_time_control" => "",//片源队列注入时间范围控制
    "message_http_import_enable" => "0",//HTTP消息注入开关
    "message_http_import_num" => "",//HTTP消息队列获取条数
    "message_http_import_url" => "xxxxxxxxx",//HTTP消息队列服务器拉取地址
    "message_queue_import_enable" => "1",//MQ消息注入开关
    "message_queue_import_num" => "12",//MQ消息队列获取条数
    "message_queue_import_host" => "localhost",//MQ消息队列服务器IP地址
    "message_queue_import_port" => "5672",//MQ消息队列服务器端口号
    "message_queue_import_user" => "guest",//MQ消息队列账号
    "message_queue_import_pass" => "guest",//MQ消息队列密码
    "message_queue_import_vhost" => "/",//MQ消息队列服务器路径
    "message_queue_import_channel" => "hello",//MQ消息队列路由名
    "message_queue_import_exchange" => "demo",//MQ消息队列交换名
    "message_queue_data_format" => "",//消息队列数据格式
    "message_ftp_import_enable" => "",//FTP消息注入开关
    "message_ftp_import_file_ex" => "",//读取FTP文件的后缀
    "message_ftp_import_file_url" => "",//FTP拉取的地址信息
    "message_ftp_import_mem_time" => "",//FTP拉取memcache缓存时间
    "message_queue_import_product_enable" => "",//产品包注入开关
    "import_product_to_cms_url" => "",//产品同步注入CMS地址
    "third_ftp_scan_enabled" => "",//第三方FTP扫描工单开关
    "third_ftp_scan_url" => "",//第三方FTP扫描工单地址
    "cdn_receive_import_enable" => "1",//CDN接收开关
    "cdn_handele_live_enable" => "",//CDN处理直播频道开关
    "cdn_handele_live_media_template" => "",//CDN处理直播频道模板ID
    "cdn_handele_live_index_enable" => "",//CDN处理直播分集开关
    "cdn_handele_live_media_enable" => "",//CDN处理直播片源开关
    "cdn_handele_playbill_enable" => "",//CDN处理节目单开关
    "site_id" => "",//站点
    "site_callback_url" => "xxxx",//站点反馈接口
    "cdn_notify_third_party_enable" => "1",//CDN是否通知第三方外部厂家
    "content_feedback_mode" => "",//内容反馈模式
    "original_cp_enabled" => "",//原始CP是否下发
    "check_metadata_enabled" => "0",//无条件注入分集/片源
    "sync_delete_media_with_index" => "",//上游分集任务是否同步删除分集下片源
    "message_first_import_num" => "20",//消息队列第一次消息注入的条数
    "fail_message_import_enable" => "0",//失败消息注入开关
    "fail_message_import_num" => "",//失败消息注入条数
    "fail_message_time" => "",//失败消息重发次数
    "message_child_import_enable" => "",//消息队列子级注入开关
    "message_child_import_num" => "",//消息队列子级注入条数
    "message_parse_version" => "0",//解析版本
    "message_parse_model" => "995",//消息解析模式
    "source_import_priority_level" => "",//消息生成注入队列的优先级
    "temp_message_enabled" => "",//临时消息开关
    "message_group_enabled" => "",//消息分组开关
    "message_import_overtime_enabled" => "",//限制消息超时时限开关
    "message_import_overtime" => "",//消息注入超时时限
    "asyn_feedback_message_enabled" => "",//异步消息队列开关
    "delayed_message_import_enabled" => "",//消息延时注入队列开关
    "delayed_message_import_overtime" => "",//延时队列超时时限
    //中心流程方向配置
    "auto_audit_enable" => "",//自动审核开关
    "audit_model" => "",//媒资审核结构
    "sensitive_word_enabled" => "",//敏感词库过滤开关
    "flow_audit" => "",//审核流程
    "flow_audit_notify" => "",//审核反馈方式
    "flow_unline" => "",//上下线流程
    "flow_unline_notify" => "",//上下线反馈方式
    "clip_custom_origin_id" => "",//CP控制切片配置，MSP平台上配置的外部源ID列表,填入的时候添加MSP的外部源ID即可，为空则默认走以前的老逻辑，多个外部源ID以;分割
    "del_cp_file_enabled" => "",//删除CP源物理文件开关
    //上游CP/SP控制转码配置
    "cp_transcode_file_enabled" => "",//上游CP/SP转码开关
    "video_source_notify_url" => "",//上游CP/SP点播转码消息反馈地址
    "cp_playbill_to_media_enabled" => "",//上游CP/SP直播转点播开关
    "source_notify_url" => "",//上游CP/SP直播转点播消息反馈地址
    "main_drm_enabled" => "",//DRM主开关
    "q_drm_identify" => "",//切片DRM厂商标识
    "q_disabled_drm" => "",//切片前DRM开关
    "q_drm_key_file_url" => "",//切片前DRM密钥地址
    "clip_drm_enabled" => "",//DRM切片加密开关
    "clip_drm_config_params" => array(
        "auth_server_ip" => "",//认证授权服务器地址
        "auth_server_port" => "",//认证授权服务器端口号
        "key_server_ip" => "",//内容密钥服务器管理地址
        "key_server_port" => "",//内容密钥服务器端口号
        "program_number" => "0",//节目号，如果为0默认选择码流中的第一套节目
        "preview_time" => "",//预览时间，单位/秒
        "package_mode" => "0",//打包支持模式，1为支持苹果打包模式，0为单文件模式，默认为0
        "p_cid" => "",//内容标识
        "encrypt_mode" => "2",//加密模式
        "max_frame_packet_encryption" => "1",//加密帧数
        "max_frame_packet_encrypt_skip" => "0"//跳过帧数
    ),
    "q_marlin_drm_enable" => "",//Marlin DRM开关
    "marlin_drm_config" => array(
        "marlin_drm_vod_num" => "",//点播生成密钥条数
        "marlin_drm_live_num" => ""//直播生成密钥条数
    ),
    "g_ftp_conf" => array(
        "download_img_enabled" => "1",//是否开启图片下载
        "ignore_img_dowload_fail" => "1",//是否忽略图片处理失败
        "upload_img_to_ftp_enabled" => "",//是否开启图片上传到ftp
        "upload_img_to_ftp_url" => "",//是否开启图片上传到ftp
        "img_enabled" => "0",//图片处理是否采用以下配置
        "down_img_dir" => "",//图片在近端存放地址(绝对路径)
        "domain" => "",//图片在远端存放地址(相对地址)
        "rule" => "",//匹配规则(正则内容)
        "rule_domain" => "",//匹配规则(匹配地址)
        "handle_upload_img" => ""//播控手动上传图片路径
    ),
    "clip_replace_mode_one" => "",//分集替换逻辑
    "media_replace_mode_one" => "",//片源替换模式一 0 关 1开【注入ID不同且匹配信息相同】
    "media_replace_mode_two" => "",//片源替换模式二 0 关 1开【注入ID相同或除注入ID外匹配信息相同】
    "media_replace_mode_three" => "",//片源替换模式三 0 关 1开【高清晰度替换低清晰度规则】
    "down_url_real" => "ftp =>//starcor =>dengxinxin@172.31.14.239/ftp",//中心片库下载地址实际接口
    "down_method" => "1",//中心片库下载是否采用FTP/HTTP 1FTP,0HTTP
    "down_method_ftp" => "1",//中心片库下载FTP模式 1主动模式,0被动模式
    "down_method_ftp_illegal_character" => "",//中心片库下载url中文符号转换模式 1中文符号不需要转换,0或者未配置中文符号需要转换】
    "import_cdn_category" => "",//CP媒资注入CDN栏目配置 在CP媒资注入CDN时，CDN会提供一个栏目ID并要求注入到此CDN时需携带此栏目ID
    "import_msp_cdn_policy" => ""//CP媒资注入MSP CDN策略配置 【CP媒资注入MSP CDN策略配置例如CDN1,CDN2】
);
/*********************这里只是记录老版本播控cp/sp的所有配置 end******************************/



$g_suoluedu_cp_arr = array(
    'slmsyx',//'名师优学'
    'slgkxy',//'高考学园'
    'slxdflc',//'新东方乐词'
    'slqzyyh',//'亲子音乐会'
    'slszx',//'索路时装秀'
    'slgcw'//'索路广场舞'
);
$g_szlt_cp_arr = array(
    'blkg',//百灵k歌
    'ggly',//果果乐园
    'xfjst'//幸福健身团
);