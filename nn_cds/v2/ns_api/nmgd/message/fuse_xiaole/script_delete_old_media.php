<?php
/**
 * Created by PhpStorm.
 * User: yangkan
 * Date: 18-4-11
 * Time: 下午5:11
 */
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_model.message.message_explain");

class script_delete_old_media
{
    private $obj_dc = null;

    private $arr_media_items = array();

    public function __construct()
    {
        $this->obj_dc = m_config::get_dc();
    }

    public function init($str_cp_id)
    {
        $str_cp_where = strlen($str_cp_id) > 1 ? " nns_cp_id = '$str_cp_id' " : ' 1=1 ';
        $str_sql = "SELECT
                      nns_import_id,nns_import_source,nns_cp_id
                    FROM
                      nns_vod_media
                    WHERE nns_deleted = 1
                      AND nns_create_time < '2018-04-08 14:44:00'
                      AND nns_id NOT IN
                      (SELECT
                        nns_media_id
                      FROM
                        `nns_mgtvbk_op_queue`
                      WHERE $str_cp_where
                        AND nns_type = 'media'
                        AND nns_status = 6) AND " . $str_cp_where;

        if(!empty($this->arr_media_items))
        {
            $str_sql = "SELECT * FROM nns_vod_media WHERE nns_id IN ('" . implode("','",$this->arr_media_items) . "')";
        }

        $arr_media_list = nl_query_by_db($str_sql,$this->obj_dc->db());
        if(!is_array($arr_media_list) || empty($arr_media_list))
        {
            echo '无任何数据';
            return false;
        }
        $obj_message_explain = new \ns_model\message\message_explain();
        foreach($arr_media_list as $media)
        {
            $arr_ret = $obj_message_explain->media_action('delete', array(
                'base_info' => array(
                    'nns_import_id' => $media['nns_import_id'],
                    'nns_import_source' => $media['nns_import_source'],
                    'nns_cp_id' => $media['nns_cp_id'],
                ),
            ));

            echo "转移参数：" . var_dump($media); echo "转移结果：" . var_dump($arr_ret);
            usleep(10000);
        }
        return true;
    }

}

//执行脚本
$str_cp_id = '';
if(isset($_SERVER['argv'][1]))
{
    $str_cp_id = $_SERVER['argv'][1];
}
$obj_script_delete_old_media = new script_delete_old_media();
$obj_script_delete_old_media->init($str_cp_id);