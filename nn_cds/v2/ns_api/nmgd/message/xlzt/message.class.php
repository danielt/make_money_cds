<?php
//error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_model.message.message_explain");
\ns_core\m_load::load("ns_core.m_pinyin");
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load_old("nn_logic/log_model/nn_log.class.php");
\ns_core\m_load::load_old("nn_logic/video/vod_index.class.php");
\ns_core\m_load::load_old("nn_logic/video/vod_media.class.php");
\ns_core\m_load::load_np("np_http_curl.class.php");
class message extends \ns_model\message\message_explain
{
    private $arr_picture = null;

    private $arr_map = null;

    //一级分类,影片种类
    private $category_map = array (
        '0' => '电影',  //电影
        '1' => '电视剧',  //电视剧
        '2' => '娱乐,综艺',  //综艺
        '3' => '动画',  //动漫
        '4' => '音乐',  //音乐
        '5' => '纪实,纪录片',  //
        '6' => '法治,科教',  //教育
        '7' => '体育',  //体育
        '8' => '生活',  //生活
        '9' => '经济',  //财经
        '10' => '特别节目',//微电影
        '11' => '品牌专区',//品牌专区
        '12' => '广告',  //广告
        '13' => '新闻', //新闻
        '14' => '公开课',
        '15' => '外语及其他语言',
        '16' => '青少年',
        '17' => '播客',
        '18' => '游戏',
    );
//片源类型
    private $file_type_map = array (
        '1' => 'ts',
        '2' => '3gp',
        '3' => 'mp4',
        '4' => 'mpg',
        '5' => 'mp3',
        '6' => 'mts',
    );

    private $msg_id = '';
    private $source_id = '';
    private $arr_cp_config = array();

    public function init()
    {

    }

    public function explain($message)
    {
        \ns_core\m_load::load_np("np_xml2array.class.php");

        $this->msg_id = $message['nns_message_id'];
        $this->source_id = $message['nns_cp_id'];
        $this->arr_cp_config = \m_config::_get_cp_info($message['nns_cp_id']);

        $content = array(
            'ret' => 0,
            'reason' => 'ok',
            'data_info' => $message['nns_message_content']
        );

        if($content['ret'] !=0 || strlen($content['data_info']) <1)
        {
            return $content;
        }
        $str_content = m_config::trim_xml_header($content['data_info']);
        if(!m_config::is_xml($str_content))
        {
            return \m_config::return_data(NS_CDS_FAIL,"消息ID[{$message['nns_message_id']}]xml内容非xml结构");
        }
        //解析下载的XML
        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->loadXML($str_content);
        $xml = $dom->saveXML();
        $xml_arr = np_xml2array::getArrayData($xml);
        if ($xml_arr["ret"] == 0)
        {
            return \m_config::return_data(1, "解析得到的媒资元数据错误");
        }

        $Objects = null;
        // 		$Maps = null;
        foreach ($xml_arr['data']['children'] as $v)
        {
            if (empty($Objects) && $v['name'] == 'Objects')
            {
                $Objects = $v['children'];
            }
            else if (empty($this->arr_map) && $v['name'] == 'Mappings')
            {
                $this->arr_map = $v['children'];
            }
        }
        //Series数据数组
        $arr_series = array ();
        //Program数据数组
        $arr_program = array ();
        //Movie数据数组
        $arr_movie = array ();
        //Picture数据数组
        $arr_picture = array ();

        $array_regist_video = array();
        $array_regist_index = array();
        $array_regist_media = array();

        if (!empty($Objects) && is_array($Objects))
        {
            foreach ($Objects as $obj)
            {
                if ($obj['attributes']['ElementType'] == 'Series')
                {
                    $arr_series[]=$this->make_key_value_arr($obj);
                }
                else if ($obj['attributes']['ElementType'] == 'Program')
                {
                    $arr_program[]=$this->make_key_value_arr($obj);
                }
                else if ($obj['attributes']['ElementType'] == 'Movie')
                {
                    $arr_movie[]=$this->make_key_value_arr($obj);
                }
                else if ($obj['attributes']['ElementType'] == 'Picture')
                {
                    $this->arr_picture[]=$this->make_key_value_arr($obj);
                }
            }
        }

        //主媒资操作   主媒资增删改走以前老逻辑，基本没什么变化
        if (!empty($arr_series))
        {
            foreach ($arr_series as $val_series)
            {
                //主媒资删除
                if (strtoupper($val_series['Action']) == 'DELETE')
                {
                    $del_series = array(
                        'base_info'=>array(
                            'nns_asset_import_id'=>$val_series['ContentID'],
                            'nns_import_source'=>isset($val_series['nns_import_source']) ? $val_series['nns_import_source'] : evn::get("project"),
                            'nns_cp_id'=>$message['nns_cp_id'],
                        ),
                    );
                    $result = $this->vod_action('delete', $del_series);
                }
                else
                {
                    //默认一级分类为电影
                    $view_type = 0;
                    $asset_category = $val_series['key_val_list']['CategoryName'] ? $val_series['key_val_list']['CategoryName'] : '电影';
                    if(isset($val_series['key_val_list']['ViewType']) && strlen($val_series['key_val_list']['ViewType']) >0)
                    {
                        $view_type = $val_series['key_val_list']['ViewType'];
                    }
                    else
                    {
                        foreach ($this->category_map as $index => $v)
                        {
                            $v = explode(',', $v);
                            if(!is_array($v) || empty($v))
                            {
                                continue;
                            }
                            foreach ($v as $_v)
                            {
                                if (stripos($asset_category, $_v) !== false)
                                {
                                    $view_type = $index;
                                    break 2;
                                }
                            }
                        }
                    }
                    //$asset_category = (isset($val_series['key_val_list']['CategoryName']) && strlen($val_series['key_val_list']['CategoryName']) >0) ? $val_series['key_val_list']['CategoryName'] : '电影';
                    $do_category = array(
                        'base_info'=>array(
                            'nns_name'=>$asset_category,   //栏目名称支持多层目录，多层目录的时候 /分割
                            'nns_import_category_id'=>'', //栏目名称支持多层目录，多层目录的时候 /分割
                            'nns_cp_id'=>$message['nns_cp_id'],
                            'nns_import_parent_category_id'=>'',
                            'nns_video_type'=>'0',  //媒资类型  0 点播 | 1 直播
                        ),
                    );
                    $result = $this->category_action('add', $do_category);
                    if($result['ret'] !=0)
                    {
                        return $result;
                    }
                    if(!isset($result['data_info']['base_info']['nns_id']) || strlen($result['data_info']['base_info']['nns_id']) <1)
                    {
                        return m_config::return_data(1,'创建栏目失败');
                    }
                    $nns_category_id = $result['data_info']['base_info']['nns_id'];
                    foreach ($this->category_map as $index => $v)
                    {
                        if (stripos($v, $asset_category) !== false)
                        {
                            $view_type = $index;
                            break;
                        }
                    }
                    if($view_type == 0 && isset($val_series['key_val_list']['VolumnCount']) && $val_series['key_val_list']['VolumnCount'] >1)
                    {
                        $view_type = 1;
                    }
                    #TODO
                    $arr_img = $this->handle_picture_v3($val_series['ContentID']);
                    if (!empty($arr_img) && is_array($arr_img))
                    {
                        $val_series['key_val_list'] = array_merge($val_series['key_val_list'], $arr_img);
                    }
                    $pinyin = m_pinyin::get_pinyin_letter($val_series['key_val_list']['Name']);
                    $add_series = array(
                        'base_info' => array(
                            'nns_name' => isset($val_series['key_val_list']['Name']) ? $val_series['key_val_list']['Name'] : '',
                            'nns_view_type' => $view_type,
                            'nns_org_type' => '0',
                            'nns_tag' => '26,',
                            'nns_director' => isset($val_series['key_val_list']['WriterDisplay']) ? $val_series['key_val_list']['WriterDisplay'] : '',
                            'nns_actor' => isset($val_series['key_val_list']['ActorDisplay']) ? $val_series['key_val_list']['ActorDisplay'] : '',
                            'nns_show_time' => (isset($val_series['key_val_list']['ReleaseYear']) && strlen($val_series['key_val_list']['ReleaseYear']) > 0) ? date("Y", strtotime($val_series['key_val_list']['ReleaseYear'])) : date("Y"),
                            'nns_view_len' => isset($val_series['key_val_list']['Duration']) ? $val_series['key_val_list']['Duration'] : 0,
                            'nns_all_index' => isset($val_series['key_val_list']['VolumnCount']) ? $val_series['key_val_list']['VolumnCount'] : 1,
                            'nns_new_index' => isset($val_series['key_val_list']['NewCount']) ? $val_series['key_val_list']['NewCount'] : 0,
                            'nns_area' => isset($val_series['key_val_list']['OriginalCountry']) ? $val_series['key_val_list']['OriginalCountry'] : '',
                            'nns_image0' => isset($val_series['key_val_list']['bigpic']) ? $val_series['key_val_list']['bigpic'] : '',
                            'nns_image1' => isset($val_series['key_val_list']['middlepic']) ? $val_series['key_val_list']['middlepic'] : '',
                            'nns_image2' => isset($val_series['key_val_list']['smallpic']) ? $val_series['key_val_list']['smallpic'] : '',
                            'nns_image3' => '',
                            'nns_image4' => '',
                            'nns_image5' => '',
                            'nns_summary' => isset($val_series['key_val_list']['Description']) ? $val_series['key_val_list']['Description'] : '',
                            'nns_remark' => isset($val_series['key_val_list']['Kind']) ? $val_series['key_val_list']['Kind'] : '',
                            'nns_category_id' => $nns_category_id,
                            'nns_play_count' => '0',
                            'nns_score_total' => '0',
                            'nns_score_count' => '0',
                            'nns_point' => isset($val_series['key_val_list']['ViewPoint']) ? $val_series['key_val_list']['ViewPoint'] : '0',
                            'nns_copyright_date' => (isset($val_series['key_val_list']['ReleaseYear']) && strlen($val_series['key_val_list']['ReleaseYear']) > 0) ? date("Y", strtotime($val_series['key_val_list']['ReleaseYear'])) : date("Y"),
                            'nns_asset_import_id' => $val_series['ContentID'],
                            'nns_pinyin' => (isset($val_series['key_val_list']['Spell']) && strlen($val_series['key_val_list']['Spell']) > 0) ? $val_series['key_val_list']['Spell'] : $pinyin,
                            'nns_pinyin_length' => (isset($val_series['key_val_list']['Spell']) && strlen($val_series['key_val_list']['Spell']) > 0) ? strlen($val_series['key_val_list']['Spell']) : strlen($pinyin),
                            'nns_alias_name' => isset($val_series['key_val_list']['AliasName']) ? $val_series['key_val_list']['AliasName'] : '',
                            'nns_eng_name' => isset($val_series['key_val_list']['EnglishName']) ? $val_series['key_val_list']['EnglishName'] : '',
                            'nns_language' => isset($val_series['key_val_list']['Language']) ? $val_series['key_val_list']['Language'] : '',
                            'nns_text_lang' => isset($val_series['key_val_list']['Language']) ? $val_series['key_val_list']['Language'] : '',
                            'nns_producer' => isset($val_series['key_val_list']['ContentProvider']) ? $val_series['key_val_list']['ContentProvider'] : '',
                            'nns_screenwriter' => isset($val_series['key_val_list']['WriterDisplay']) ? $val_series['key_val_list']['WriterDisplay'] : '',
                            'nns_play_role' => '',
                            'nns_copyright_range' => isset($val_series['key_val_list']['ContentProvider']) ? $val_series['key_val_list']['ContentProvider'] : '',
                            'nns_vod_part' => '',
                            'nns_keyword' => isset($val_series['key_val_list']['Keywords']) ? $val_series['key_val_list']['Keywords'] : '',
                            'nns_import_source' => isset($val_series['nns_import_source']) ? $val_series['nns_import_source'] : evn::get("project"),
                            'nns_kind' => isset($val_series['key_val_list']['Kind']) ? $val_series['key_val_list']['Kind'] : '',
                            'nns_copyright' => isset($val_series['key_val_list']['ContentProvider']) ? $val_series['key_val_list']['ContentProvider'] : '',
                            'nns_clarity' => '',
                            'nns_image_v' => isset($val_series['key_val_list']['verticality_img']) ? $val_series['key_val_list']['verticality_img'] : '',
                            'nns_image_s' => isset($val_series['key_val_list']['square_img']) ? $val_series['key_val_list']['square_img'] : '',
                            'nns_image_h' => isset($val_series['key_val_list']['horizontal_img']) ? $val_series['key_val_list']['horizontal_img'] : '',
                            'nns_cp_id' => $message['nns_cp_id'],
                            'nns_conf_info' => '',
                            'nns_ext_url' => '',
                        ), //基本信息（存储于nns_vod表中）
                        'ex_info' => array(
                            'svc_item_id' => '',
                            'month_clicks' => '',
                            'week_clicks' => '',
                            'base_id' => '',
                            'asset_path' => '',
                            'ex_tag' => '',
                            'full_spell' => '',
                            'awards' => '',
                            'year' => '',
                            'play_time' => '',
                            'channel' => '',
                            'first_spell' => '',
                        ), //扩展信息（存储于nns_vod_ex表中）
                    );
                    //字段待修改4-14
                    $result = $this->vod_action('add', $add_series);
                }

                if ($result['ret'] != 0)
                {
                    return $result;
                }

            }
        }

        //分集操作   分集删除走以前的老逻辑，分集注册  则组装分集和片源的信息到redis，分集更新则查询redis关联数据
        if (!empty($arr_program))
        {
            foreach ($arr_program as $val_program)
            {
                //分集删除
                if (strtoupper($val_program['Action']) == 'DELETE')
                {
                    $del_program = array(
                        'base_info' => array(
                            'nns_import_id' => $val_program['ContentID'],
                            'nns_import_source' => isset($val_program['nns_import_source']) ? $val_program['nns_import_source'] : evn::get("project"),
                            'nns_cp_id' => $message['nns_cp_id'],
                        ),
                    );
                    $re = $this->index_action('delete', $del_program);
                    if ($re['ret'] != 0)
                    {
                        $re['ret'] = 1;
                        return $re;
                    }
                } else
                {

                    $arr_p_series = $this->get_map_info_v2('Program', $this->arr_map, $val_program['ContentID']);
                    $p_series = array_keys($arr_p_series);
                    //分集不存在集数字段
                    if (!isset($val_program['key_val_list']['Sequence']) || !strlen($val_program['key_val_list']['Sequence']) > 0)
                    {
                        $val_program['key_val_list']['Sequence'] = isset($arr_p_series[$p_series[0]]['key_val_list']['Sequence']) && (int)$arr_p_series[$p_series[0]]['key_val_list']['Sequence'] > 0 ? (int)$arr_p_series[$p_series[0]]['key_val_list']['Sequence'] : 0;
                    }
                    if (substr($message['nns_cp_id'], 0, 5) == 'fuse_')
                    {
                        $val_program['key_val_list']['Sequence']++;
                    }

                    $res = nl_asset_incidence_relation::query_asset_relation(m_config::get_dc(), 'video', $p_series[0], $message['nns_cp_id'], 0);
                    if (!is_array($res['data_info']))
                    {
                        $res['ret'] = 6;
                        return $res;
                    }
                    $arr_img = $this->handle_picture_v3($val_program['ContentID']);
                    if (!empty($arr_img) && is_array($arr_img))
                    {
                        $val_program['key_val_list'] = array_merge($val_program['key_val_list'], $arr_img);
                    }

                    $add_index = array(
                        'base_info' => array(
                            'nns_name' => isset($val_program['key_val_list']['Name']) ? $val_program['key_val_list']['Name'] : '',
                            'nns_index' => isset($val_program['key_val_list']['Sequence']) && (int)$val_program['key_val_list']['Sequence'] > 0 ?(int)$val_program['key_val_list']['Sequence']-1 : 0,
                            'nns_time_len' => '',
                            'nns_summary' => isset($val_program['key_val_list']['Description']) ? $val_program['key_val_list']['Description'] : '',
                            'nns_image' => isset($val_program['key_val_list']['Picture']) ? $val_program['key_val_list']['Picture'] : '',
                            'nns_play_count' => 0,
                            'nns_score_total' => 0,
                            'nns_score_count' => 0,
                            'nns_video_import_id' => $res['data_info'][0]['nns_asset_import_id'],
                            'nns_import_id' => $val_program['ContentID'],
                            'nns_import_source' => isset($val_program['nns_import_source']) ? $val_program['nns_import_source'] : evn::get("project"),
                            'nns_director' => isset($val_program['key_val_list']['WriterDisplay']) ? $val_program['key_val_list']['WriterDisplay'] : '',
                            'nns_actor' => isset($val_program['key_val_list']['ActorDisplay']) ? $val_program['key_val_list']['ActorDisplay'] : '',
                            'nns_release_time' => (isset($val_program['key_val_list']['ReleaseYear']) && strlen($val_program['key_val_list']['ReleaseYear']) > 0) ? date("Y-m-d", strtotime($val_program['key_val_list']['ReleaseYear'])) : date("Y-m-d"),
                            'nns_update_time' => date("Y-m-d"),
                            'nns_watch_focus' => '',
                            'nns_cp_id' => $message['nns_cp_id'],
                            'nns_conf_info' => '',
                            'nns_ext_url' => '',
                        ),
                        'ex_info' => array(
                            'isintact' => '',
                            'subordinate_name' => '',
                            'initials' => '',
                            'publisher' => '',
                            'first_spell' => '',
                            'caption_language' => '',
                            'language' => isset($val_program['key_val_list']['Language']) ? $val_program['key_val_list']['Language'] : '',
                            'region' => '',
                            'adaptor' => '',
                            'sreach_key' => '',
                            'event_tag' => '',
                            'year' => '',
                            'sort_name' => '',
                        ),
                    );

                    //分集注入
                    $re = $this->index_action('add', $add_index);

                    if ($re['ret'] != 0)
                    {
                        $re['ret'] = 1;
                        return $re;
                    }
                }
            }
        }

        //片源操作
        if (!empty($arr_movie))
        {
            foreach ($arr_movie as $val_movie)
            {
                //片源删除
                if (strtoupper($val_movie['Action']) == 'DELETE')
                {

                    $del_movie = array(
                        'base_info' => array(
                            'nns_content_id' => $val_movie['ContentID'],
                            'nns_import_source' => isset($val_movie['nns_import_source']) ? $val_movie['nns_import_source'] : evn::get("project"),
                            'nns_cp_id' => $message['nns_cp_id'],
                        ),
                    );
                    $re = $this->media_action('delete', $del_movie);
                    if ($re['ret'] != 0)
                    {
                        $re['ret'] = 1;
                        return $re;
                    }
                } //片源修改
                else
                {
                    $arr_p_program = $this->get_map_info_v2('Movie', $this->arr_map, $val_movie['ContentID']);
                    $arr_p_program = array_keys($arr_p_program);
                    $index_info = nl_asset_incidence_relation::query_asset_relation(m_config::get_dc(), 'index', $arr_p_program[0], $message['nns_cp_id'], 1);

                    if (!is_array($index_info['data_info']))
                    {
                        //返回分集未注入
                        return $re['ret'] = 7;
                    }

                    $index_import_id = $arr_p_program[0];
                    $vod_import_id = $index_info['data_info'][0]['nns_asset_import_id'];
                    $type_key = isset($val_movie['key_val_list']['SystemLayer']) ? (int)$val_movie['key_val_list']['SystemLayer'] : 1;
                    $file_type = $this->file_type_map[$type_key];
                    //片源参数
                    $add_media = array(
                        'base_info' => array(
                            'nns_name' => isset($val_movie['key_val_list']['Name']) ? $val_movie['key_val_list']['Name'] : '',
                            'nns_type' => 1,
                            'nns_url' => isset($val_movie['key_val_list']['FileURL']) ? $val_movie['key_val_list']['FileURL'] : '',
                            'nns_tag' => '26,',
                            'nns_mode' => isset($val_movie['key_val_list']['MediaMode']) ? $val_movie['key_val_list']['MediaMode'] : '',
                            'nns_kbps' => isset($val_movie['key_val_list']['BitRateType']) ? $val_movie['key_val_list']['BitRateType'] : '',
                            'nns_content_id ' => $val_movie['ContentID'],
                            'nns_content_state' => 0,
                            'nns_filetype' => isset($val_movie['key_val_list']['FileType']) ? $val_movie['key_val_list']['FileType'] : 'ts',
                            'nns_play_count' => '0',
                            'nns_score_total' => '0',
                            'nns_score_count' => '0',
                            'nns_video_import_id' => $vod_import_id,
                            'nns_index_import_id' => $index_import_id,
                            'nns_import_id' => $val_movie['ContentID'],
                            'nns_import_source' => isset($val_movie['nns_import_source']) ? $val_movie['nns_import_source'] : evn::get("project"),
                            'nns_dimensions' => isset($val_movie['key_val_list']['Dimensions']) ? $val_movie['key_val_list']['Dimensions'] : '2D',
                            'nns_ext_url' => '',
                            'nns_file_size' => isset($val_movie['key_val_list']['FileSize']) ? $val_movie['key_val_list']['FileSize'] : 0,
                            'nns_file_time_len' => isset($val_movie['key_val_list']['TimeLen']) ? $val_movie['key_val_list']['TimeLen'] : 0,
                            'nns_file_frame_rate' => isset($val_movie['key_val_list']['FrameRate']) ? $val_movie['key_val_list']['FrameRate'] : 0,
                            'nns_file_resolution' => isset($val_movie['key_val_list']['Resolution']) ? $val_movie['key_val_list']['Resolution'] : 0,
                            'nns_cp_id' => $message['nns_cp_id'],
                            'nns_ext_info' => '',
                            'nns_drm_enabled' => isset($val_movie['key_val_list']['DrmEnabled']) ? $val_movie['key_val_list']['DrmEnabled'] : 0,
                            'nns_drm_encrypt_solution' => isset($val_movie['key_val_list']['DrmEncryptSolution']) ? $val_movie['key_val_list']['DrmEncryptSolution'] : '',
                            'nns_drm_ext_info' => '',
                            'nns_domain' => isset($val_movie['key_val_list']['Domain']) ? $val_movie['key_val_list']['Domain'] : 0,
                            'nns_media_type' => isset($val_movie['key_val_list']['VideoType']) ? $val_movie['key_val_list']['VideoType'] : 1,
                            'nns_original_live_id' => '',
                            'nns_start_time' => '',
                            'nns_media_service' => isset($val_movie['key_val_list']['ServiceType']) ? $val_movie['key_val_list']['ServiceType'] : 'HTTP',
                            'nns_conf_info' => '',
                            'nns_encode_flag' => isset($val_movie['key_val_list']['EncodeFlag']) ? $val_movie['key_val_list']['EncodeFlag'] : 0,
                            'nns_live_to_media' => isset($val_movie['key_val_list']['EncodeType']) ? $val_movie['key_val_list']['EncodeType'] : '',
                            'nns_media_service_type' => '',
                        ),
                        'ex_info' => array(
                            'file_hash' => '',
                            'file_width' => '',
                            'file_height' => '',
                            'file_scale' => '',
                            'file_coding' => 'H264',
                        ),
                    );

                    $re = $this->media_action('add', $add_media);
                    if ($re['ret'] != 0)
                    {
                        $re['ret'] = 1;
                        return $re;
                    }
                }
            }
        }


    }

    private function handle_picture_v3($asset_id)
    {
        $array_img = array();
        $array_img['bigpic']='';
        $array_img['middlepic']='';
        $array_img['smallpic']='';
        $array_img['horizontal_img']='';
        $array_img['verticality_img']='';
        $array_img['square_img']='';
        if(empty($this->arr_picture) || !is_array($this->arr_picture))
        {
            return $array_img;
        }
        $array_map = $this->get_map_info_v2('Picture',$this->arr_map,$asset_id);
        if(empty($array_map))
        {
            return $array_img;
        }
        $array_map_key = array_keys($array_map);
        $temp_array = array();
        foreach ($this->arr_picture as $obj_picture)
        {
            if(!in_array($obj_picture['ContentID'], $array_map_key))
            {
                continue;
            }
            //组成数据,key为图片type,值为url
            if (!isset($obj_picture['key_val_list']['Type']))
            {
                foreach ($array_map as $k_id => $val)
                {
                    if ($obj_picture['ContentID'] == $k_id)
                    {
                        $obj_picture['key_val_list']['Type'] = $val['key_val_list']['Type'];
                    }
                }
            }
            $temp_array[$obj_picture['key_val_list']['Type']] = (isset($obj_picture['key_val_list']['FileURL']) && strlen($obj_picture['key_val_list']['FileURL']) >0) ? trim($obj_picture['key_val_list']['FileURL']) : '';
        }
        $temp_array = array_filter($temp_array);
        if(empty($temp_array))
        {
            return $array_img;
        }
        $arr_combine = $this->combine_picture($array_img, $temp_array);
        return array_combine(array_keys($array_img),$arr_combine);
    }

    private function get_map_info_v2($p_type,$arr_map, $e_id)
    {
        $temp_arr = array();
        if ($p_type == 'Picture')
        {
            foreach ($arr_map as $map)
            {
                if ($map['attributes']['ElementType'] == $p_type && $map['attributes']['ParentCode'] == $e_id)
                {
                    $filter_map = $this->make_key_value_arr($map);
                    $temp_arr[$map['attributes']['ElementCode']] = $filter_map;
                }
                if (isset($temp_arr[$map['attributes']['ElementCode']]))
                {
                    continue;
                }
                if ($map['attributes']['ParentType'] == $p_type && $map['attributes']['ElementCode'] == $e_id)
                {
                    $filter_map = $this->make_key_value_arr($map);
                    $temp_arr[$map['attributes']['ParentCode']] = $filter_map;
                }
            }

        }
        else
        {
            foreach ($arr_map as $map)
            {
                if ($map['attributes']['ElementType'] == $p_type && $map['attributes']['ParentType'] != 'Picture' && $map['attributes']['ElementCode'] == $e_id)
                {
                    $filter_map = $this->make_key_value_arr($map);
                    $temp_arr[$map['attributes']['ParentCode']] = $filter_map;
                }
            }
        }
        return $temp_arr;
    }

    /**
     * 获取xml的 attr属性和key value值
     * @param unknown $xml_obj_arr
     * @return Ambigous <NULL, unknown, string>
     */
    private function make_key_value_arr($xml_obj_arr)
    {
        $key_val_array = null;
        if (isset($xml_obj_arr['attributes']) && is_array($xml_obj_arr['attributes']) && !empty($xml_obj_arr['attributes']))
        {
            foreach ($xml_obj_arr['attributes'] as $attr_key => $attr_val)
            {
                $key_val_array[$attr_key] = $attr_val;
            }
            if(!isset($key_val_array['ContentID']) || strlen($key_val_array['ContentID'])<1)
            {
                $key_val_array['ContentID'] = isset($xml_obj_arr['attributes']['ID']) ? $xml_obj_arr['attributes']['ID'] : '';
            }
            unset($xml_obj_arr['attributes']);
        }
        if (isset($xml_obj_arr['children']) && is_array($xml_obj_arr['children']) && !empty($xml_obj_arr['children']))
        {
            foreach ($xml_obj_arr['children'] as $key_list)
            {
                if (isset($key_list['attributes']['Name']) && strlen($key_list['attributes']['Name']) > 0)
                {
                    $key_val_array['key_val_list'][$key_list['attributes']['Name']] = (isset($key_list['content']) && strlen($key_list['content']) > 0) ? trim($key_list['content']) : '';
                }
            }
            unset($xml_obj_arr['children']);
        }
        return $key_val_array;
    }

    /**
     * 获取任务名称
     * @param string $video_id 视频id
     * @param string $video_type 视频类型
     * @return string task_name
     * @author yunping.yang
     * @date 2014-8-12
     */
    private function get_task_name($video_id, $video_type)
    {
        set_time_limit(0);
        $db = m_config::get_dc()->db();
        //获取任务名称
        $task_name = '';
        $asset_id = '';
        $nns_kbps = 0;
        $nns_cp_id = '';
        $nns_ext_url = '';
        $nns_url = '';
        $nns_live_to_media = 1;
        $nns_filetype = '';
        $ex_data = array();
        switch ($video_type)
        {
            case video:
                $task_name_arr = nl_db_get_one("select nns_name,nns_cp_id,nns_asset_import_id,nns_ext_url from nns_vod where nns_id='{$video_id}'", $db);
                $task_name = $task_name_arr['nns_name'];
                $nns_cp_id = (strlen($task_name_arr['nns_cp_id']) > 0) ? $task_name_arr['nns_cp_id'] : 0;
                $asset_id = $video_id;
                $str_import_id = $task_name_arr['nns_asset_import_id'];
                $nns_ext_url = $task_name_arr['nns_ext_url'];
                $nns_media_encode_flag = 0;
                $nns_ext_info = '';
                $nns_tag = '';
                break;
            case BK_OP_INDEX:
                $task_name_arr = nl_db_get_one("select nns_name,nns_vod_id,nns_index,nns_cp_id,nns_import_id,nns_ext_url from nns_vod_index where nns_id='{$video_id}'", $db);
                if (strlen(trim($task_name_arr['nns_name'])) <= 0)
                {
                    $task_name_arr['nns_index1'] = (int)$task_name_arr['nns_index'] + 1;
                    $task_name_arr['nns_name'] = '第' . $task_name_arr['nns_index1'] . '集';
                }
                $task_name = '[' . $task_name_arr['nns_name'] . ']';
                $asset_id = $task_name_arr['nns_vod_id'];
                $nns_cp_id = (strlen($task_name_arr['nns_cp_id']) > 0) ? $task_name_arr['nns_cp_id'] : 0;
                $str_import_id = $task_name_arr['nns_import_id'];
                $nns_ext_url = $task_name_arr['nns_ext_url'];
                $nns_media_encode_flag = 0;
                $nns_ext_info = '';
                $nns_tag = '';
                break;
            case BK_OP_MEDIA:
                $task_name_arr = nl_db_get_one("select vodindex.nns_name,vodindex.nns_index,media.nns_mode,media.nns_filetype,media.nns_tag as nns_tag,media.nns_media_type as nns_media_type,media.nns_url as nns_url,media.nns_vod_id as nns_vod_id,media.nns_kbps as nns_kbps,media.nns_cp_id as nns_cp_id,media.nns_content_id as nns_content_id,media.nns_import_id as nns_import_id,media.nns_ext_url as nns_ext_url,media.nns_ext_info as nns_ext_info,media.nns_encode_flag as nns_encode_flag  from nns_vod_media media left join nns_vod_index vodindex on media.nns_vod_index_id=vodindex.nns_id   where media.nns_id='{$video_id}'", $db);
                if (strlen(trim($task_name_arr['nns_name'])) <= 0)
                {
                    $task_name_arr['nns_index1'] = (int)$task_name_arr['nns_index'] + 1;
                    $task_name_arr['nns_name'] = '第' . $task_name_arr['nns_index1'] . '集';
                }
                $ex_data['nns_content_id'] = $task_name_arr['nns_content_id'];
                $asset_id = $task_name_arr['nns_vod_id'];
                $nns_kbps = ($task_name_arr['nns_kbps'] > 0) ? $task_name_arr['nns_kbps'] : 0;
                $task_name = '[' . $task_name_arr['nns_name'] . '] [' . $nns_kbps . '/kbps] ' . ' [' . $task_name_arr['nns_tag'] . '] ' . $task_name_arr['nns_mode'];
                $nns_cp_id = (strlen($task_name_arr['nns_cp_id']) > 0) ? $task_name_arr['nns_cp_id'] : 0;
                $str_import_id = $task_name_arr['nns_import_id'];
                $nns_ext_url = $task_name_arr['nns_ext_url'];
                $nns_ext_info = $task_name_arr['nns_ext_info'];
                $nns_tag = (strlen($task_name_arr['nns_tag']) > 0) ? $task_name_arr['nns_tag'] : '';
                $nns_media_encode_flag = (isset($task_name_arr['nns_encode_flag']) && in_array($task_name_arr['nns_encode_flag'], array('0', '1', '2'))) ? $task_name_arr['nns_encode_flag'] : 0;
                $nns_tag = $task_name_arr['nns_tag'];
                $nns_url = $task_name_arr['nns_url'];
                $nns_filetype = $task_name_arr['nns_filetype'];
                $nns_live_to_media = $task_name_arr['nns_media_type'];
                break;
            case BK_OP_LIVE:
                $task_name_arr = nl_db_get_one("select nns_name,nns_cp_id,nns_import_id,nns_integer_id from nns_live where nns_id='{$video_id}'", $db);
                $task_name = $task_name_arr['nns_name'];
                $nns_cp_id = (strlen($task_name_arr['nns_cp_id']) > 0) ? $task_name_arr['nns_cp_id'] : 0;
                $asset_id = $video_id;
                $str_import_id = (strlen($task_name_arr['nns_import_id']) > 0) ? $task_name_arr['nns_import_id'] : $task_name_arr['nns_integer_id'];
                $nns_ext_url = '';
                $nns_media_encode_flag = 0;
                $nns_ext_info = '';
                $nns_tag = '';
                break;
            case BK_OP_LIVE_MEDIA:
                $task_name_arr = nl_db_get_one("select live_index.nns_index,media.nns_mode,media.nns_live_id as nns_vod_id,media.nns_tag as nns_tag,media.nns_kbps as nns_kbps,media.nns_cp_id as nns_cp_id,media.nns_content_id as nns_content_id,media.nns_ext_url as nns_ext_url  from nns_live_media as media left join nns_live_index live_index on media.nns_live_index_id=live_index.nns_id   where media.nns_id='{$video_id}'", $db);
                if (strlen(trim($task_name_arr['nns_name'])) <= 0)
                {
                    $task_name_arr['nns_index1'] = (int)$task_name_arr['nns_index'] + 1;
                    $task_name_arr['nns_name'] = '第' . $task_name_arr['nns_index1'] . '集';
                }
                $ex_data['nns_content_id'] = $task_name_arr['nns_content_id'];
                $asset_id = $task_name_arr['nns_vod_id'];
                $nns_kbps = ($task_name_arr['nns_kbps'] > 0) ? $task_name_arr['nns_kbps'] : 0;
                $task_name = '[' . $task_name_arr['nns_name'] . '] [' . $nns_kbps . '/kbps] ' . ' [' . $task_name_arr['nns_tag'] . '] ' . $task_name_arr['nns_mode'];
                $nns_cp_id = (strlen($task_name_arr['nns_cp_id']) > 0) ? $task_name_arr['nns_cp_id'] : 0;
                $str_import_id = $task_name_arr['nns_content_id'];
                $nns_ext_url = $task_name_arr['nns_ext_url'];
                $nns_media_encode_flag = 0;
                $nns_ext_info = '';
                $nns_tag = '';
                break;
            case BK_OP_PLAYBILL:
                $task_name_arr = nl_db_get_one("select nns_name,nns_cp_id,nns_playbill_import_id,nns_ext_url,nns_begin_time,nns_live_id from nns_live_playbill_item where nns_id='{$video_id}'", $db);
                if (isset($task_name_arr['nns_live_id']) && strlen($task_name_arr['nns_live_id']))
                {
                    $task_name_arr_live = nl_db_get_one("select nns_name from nns_live where nns_id='{$task_name_arr['nns_live_id']}'", $db);
                }
                $live_name_temp = isset($task_name_arr_live['nns_name']) ? $task_name_arr_live['nns_name'] : '';
                $task_name = $live_name_temp . '] [' . $task_name_arr['nns_begin_time'] . '] [' . $task_name_arr['nns_name'];
                $nns_cp_id = (strlen($task_name_arr['nns_cp_id']) > 0) ? $task_name_arr['nns_cp_id'] : 0;
                $asset_id = $video_id;
                $str_import_id = $task_name_arr['nns_playbill_import_id'];
                $nns_ext_url = $task_name_arr['nns_ext_url'];
                $nns_media_encode_flag = 0;
                $nns_ext_info = '';
                $nns_tag = '';
                break;
            case BK_OP_FILE:
                $task_name_arr = nl_db_get_one("select nns_name,nns_cp_id,nns_import_id,nns_ext_url from nns_file_package where nns_id='{$video_id}'", $db);
                $task_name = $task_name_arr['nns_name'];
                $nns_cp_id = (strlen($task_name_arr['nns_cp_id']) > 0) ? $task_name_arr['nns_cp_id'] : 0;
                $asset_id = $video_id;
                $str_import_id = $task_name_arr['nns_import_id'];
                $nns_ext_url = $task_name_arr['nns_ext_url'];
                $nns_ext_info = '';
                $nns_media_encode_flag = 0;
                $nns_tag = '';
                break;
            case BK_OP_PRODUCT:
                $task_name_arr = nl_db_get_one("select nns_order_name as nns_name,nns_cp_id,nns_order_number as nns_import_id from nns_product where nns_id='{$video_id}'", $db);
                $task_name = $task_name_arr['nns_name'];
                $nns_cp_id = (strlen($task_name_arr['nns_cp_id']) > 0) ? $task_name_arr['nns_cp_id'] : 0;
                $asset_id = $video_id;
                $str_import_id = $task_name_arr['nns_import_id'];
                $nns_ext_url = '';
                $nns_ext_info = '';
                $nns_media_encode_flag = 0;
                $nns_tag = '';
                break;
            case  BK_OP_SEEKPOINT:
                $task_name_arr = nl_db_get_one("select * from nns_vod_index_seekpoint where nns_id='{$video_id}'", $db);
                $task_name = $task_name_arr['nns_name'];
                $nns_cp_id = (strlen($task_name_arr['nns_cp_id']) > 0) ? $task_name_arr['nns_cp_id'] : 0;
                $asset_id = $video_id;
                $str_import_id = $task_name_arr['nns_import_id'];
                $nns_ext_url = '';
                $nns_ext_info = '';
                $nns_media_encode_flag = 0;
                $nns_tag = '';
                break;
        }
        if ($video_type == BK_OP_VIDEO || $video_type == BK_OP_PLAYBILL || $video_type == BK_OP_FILE || $video_type == BK_OP_LIVE)
        {
            $task_name = '[' . $task_name . ']';
        } else if ($video_type == BK_OP_LIVE_MEDIA)
        {
            $video_name = nl_db_get_col("select nns_name from nns_live where nns_id='{$asset_id}'", $db);
            $task_name = '[' . $video_name . '] ' . $task_name;
        } else
        {
            $video_name = nl_db_get_col("select nns_name from nns_vod where nns_id='{$asset_id}'", $db);
            $task_name = '[' . $video_name . '] ' . $task_name;
        }
        $array = array(
            'task_name' => $task_name,
            'asset_id' => $asset_id,
            'nns_kbps' => $nns_kbps,
            'cp_id' => $nns_cp_id,
            'ex_data' => $ex_data,
            'import_id' => $str_import_id,
            'nns_ext_url' => $nns_ext_url,
            'nns_ext_info' => $nns_ext_info,
            'nns_media_encode_flag' => $nns_media_encode_flag,
            'nns_tag' => $nns_tag,
            'nns_url' => $nns_url,
            'nns_live_to_media' => $nns_live_to_media,
            'nns_filetype' => $nns_filetype,
        );
        /*print_r($array);die;*/
        return $array;
    }

    public function status($message_id)
    {

    }


    /**
     * @description:播控注入cdn后统一反馈给二级
     * @author:xinxin.deng
     * @date: 2018/3/2 9:59
     * @param $message_id //消息id
     * @param $code //状态码，失败1，成功0
     * @param $reason //原因描述
     * @param $arr_data //封装好了的反馈信息 array(
     *                                          'cdn_id' => ,注入cdn的id
     *                                          'site_id' => ,配置的站点id
     *                                          'mg_asset_type' => ,注入媒资类型
     *                                          'mg_asset_id' => ,主媒资的消息注入id
     *                                          'mg_part_id' => ,分集的消息注入id
     *                                          'mg_file_id' => ,片源的消息注入id,
     *                                          'is_finally' =>0, 0表示最终状态,反馈的时候将其unset掉
     *                                          );
     * @param $sp_id //sp_id
     */
    public function is_ok($message_id, $code, $reason, $arr_data, $sp_id)
    {
        $arr_sp_data = \m_config::_get_sp_info($sp_id);
        $sp_config = isset($arr_sp_data['data_info']['nns_config']) ? $arr_sp_data['data_info']['nns_config'] : array();
        unset($arr_data['is_finally']);
        //兼容湖南电信二级消息反馈，1表示成功，0表示失败。
        $code = $code === 0 ? 1 : 0;
        $flag_message_feedback_enabled = (!isset($sp_config['message_feedback_enabled']) || $sp_config['message_feedback_enabled'] !=1) ? true : false;
        if(!$flag_message_feedback_enabled || !isset($sp_config['message_feedback_mode']) || $sp_config['message_feedback_mode'] != 0)
        {
            \m_config::base_callback_log(NS_CDS_CDN_QUEUE, 'notify', '[' . date('Y-m-d H:i:s') . ']' . '不允许反馈或者反馈模式不正确：
            [message_feedback_enabled]=' . $sp_config['message_feedback_enabled'] . 'message_feedback_mode=' . $sp_config['message_feedback_mode'], \m_config::return_child_path(__FILE__));
            return;
        }

        //查询sp的上报地址/芒果使用站点ID
        if(isset($sp_config['site_callback_url']) && strlen($sp_config['site_callback_url']) > 0)
        {
            $xml_info = array(
                'msgid' => $message_id,
                'state' => $code,
                'msg' => $reason,
                'info' => $arr_data,
            );


            $xml = self::_build_notify_xml($xml_info);
            $data = array(
                'cmsresult' => $xml,
            );
            \m_config::base_callback_log(NS_CDS_CDN_QUEUE, 'notify', '[' . date('Y-m-d H:i:s') . ']' . '站点反馈地址为:' . var_export($sp_config['site_callback_url'], true) . ',反馈
            数据为:' . var_export($data, true), \m_config::return_child_path(__FILE__));

            $http_curl = new np_http_curl_class();
            for ($i = 0; $i < 3; $i++) {
                //访问媒资注入接口
                $re = $http_curl->post($sp_config['site_callback_url'], $data, null, 2);
                $curl_info = $http_curl->curl_getinfo();
                $http_code = $curl_info['http_code'];
                \m_config::base_callback_log(NS_CDS_CDN_QUEUE, 'notify', '[' . date('Y-m-d H:i:s') . ']' . '循环请求第' . $i . '次(如果请求返回post结果为true并且状态码不是200,就会默认循环3次,状态码为200,
                就跳出循环),' . '请求返回post结果为:' . $re . '请求返回状态为:' . var_export($http_code, true) . ',反回数据为:' . var_export($curl_info, true) . ',
                访问POST结果为:' . var_export($re, true), \m_config::return_child_path(__FILE__));

                if ($http_code != 200 && (int)$re === 1)
                {
                    continue;
                }
                else
                {
                    break;
                }
            }
        }
        return;
    }


    /**
     * @description:组装反馈的XML
     * @author:xinxin.deng
     * @date: 2018/2/26 14:25
     * @param $params
     * @return string|void
     */
    public function _build_notify_xml($params)
    {
        if(!is_array($params) || empty($params))
        {
            return ;
        }
        $dom = new DOMDocument('1.0', 'utf-8');
        $xmlresult = $dom->createElement('xmlresult');
        $dom->appendChild($xmlresult);
        foreach ($params as $key=>$value)
        {
            $$key = $dom->createElement($key);
            $xmlresult->appendchild($$key);
            if(!empty($value) && is_array($value))
            {
                foreach ($value as $k=>$val)
                {
                    $$k = $dom->createElement($k);
                    $$key->appendchild($$k);
                    //创建元素值
                    $content = $dom->createTextNode($val);
                    $$k->appendchild($content);
                }
            }
            else
            {
                //创建元素值
                $text = $dom->createTextNode($value);
                $$key->appendchild($text);
            }
        }
        return $dom->saveXML();
    }

    /**
     * 合并图片类型
     * @param $arr_img
     * @param $arr_temp
     * @return array
     */
    private function combine_picture($arr_img, $arr_temp)
    {
        $arr_img = array_values($arr_img);
        foreach ($arr_temp as $k=> $item)
        {
            $arr_img[$k-1] = $item;
        }
        return $arr_img;
    }
}