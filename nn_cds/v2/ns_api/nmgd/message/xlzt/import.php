<?php
header('Content-Type: text/html; charset=utf-8');
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.message.message_queue");
class import extends \ns_model\message\message_queue {
public $arr_in_message = array(
'base_info'=>array(
'nns_message_time'=>'', //上游消息时间  年月日时分秒毫秒
'nns_message_id'=>'',  //上游消息ID
'nns_cp_id'=>'', //上游CP标示
'nns_message_xml'=>'',  //上游原始的信息  可以为字符串 | xml | json 或则存放消息地址
'nns_message_content'=>'',  //上游解析后的信息  可以为字符串 | xml | json  生成队列的基本信息
'nns_action'=>'', //操作 行为
'nns_type'=>'', //消息 类型
'nns_name'=>'',  //消息名称
'nns_package_id'=>'',  //包ID
'nns_xmlurlqc'=>'', //广州电信悦ME MD5摘要
'nns_encrypt'=>'', //广州电信悦ME 加密串
'nns_content_number'=>'', //xml文件中包含的内容数量
), //基本信息（存储于nns_mgtvbk_message表中）
);
public function __construct(){
}
public function import(){
$result = $this->push($this->arr_in_message);
}
}
$import = new import();
$result_import = $import->import();
return $result_import;
