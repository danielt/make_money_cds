<?php
    /**
     * EPG分发解释器
     * Created by <xinxin.deng>.
     * Author: xinxin.deng
     * Date: 2018/7/9 16:14
     */

\ns_core\m_load::load_old("nn_logic/c2_task/c2_task.class.php");
\ns_core\m_load::load_old("nn_logic/epg_log/epg_log.class.php");
\ns_core\m_load::load_np('np_http_curl.class.php');
\ns_core\m_load::load("ns_data_model.category.m_category_inout");
\ns_core\m_load::load("ns_data_model.epg.m_epg_inout");
\ns_core\m_load::load("ns_model.m_queue_model");
\ns_core\m_load::load("ns_model.center_op.center_op_queue");
class epg_delivery
{
    public $str_sp_id = '';//SPID
    public $str_source_id = '';//CPID
    public $arr_sp_config = array();//SP配置
    public $arr_source_config = array();//CP配置
    public $m_epg_inout = array();//EPG

    private $m_queue_model = array();
    private $c2_info = array();//C2队列数据  写一个key=>value获取
    private $video_info = array();//主媒资数据
    private $index_info = array();//分集数据
    private $seekpoint_info = array();//分集打点数据
    private $media_info = array();//片源数据
    private $live_info = array();//直播
    private $live_index_info = array();//直播分集
    private $live_media_info = array();//直播源
    private $file_info = array();//文件包
    private $package_info = array();//产品打包
    private $playbill_info = array();//节目单

    //主媒资类型。0：正片；3短片；4预告片；5花絮.映射cms的类型
    private $preview_type = array(
        0 => 0,
        1 => 5,
        2 => 0,
        3 => 0,
        4 => 4,
    );

    public function __construct($sp_id='')
    {
        //创建DC
        \m_config::set_dc();
        if(strlen($sp_id) > 0 && !empty($sp_id))
        {
            $this->str_sp_id = $sp_id;
            $arr_sp_config = \m_config::_get_sp_info($this->str_sp_id);
            $this->arr_sp_config = isset($arr_sp_config['data_info']['nns_config']) ? $arr_sp_config['data_info']['nns_config'] : array();
        }
        $this->m_queue_model = new \ns_model\m_queue_model();
    }
    /**
     * epg_delivery_explain constructor.
     * @param $c2_queue_info array(
     *                      'c2_info'=>array(C2信息),
     *                      'video_info'=>array(
     *                          'base_info' => VIDEO信息
     *                          'ex_info'  => 扩展信息
     *                       ),
     *                      'index_info'=>array(
     *                          'base_info' => INDEX信息,
     *                          'ex_info' => 扩展信息,
     *                      ),
     *                      'seekpoint_info'=>array(
     *                          'base_info' => 分集打点信息,
     *                          'ex_info' => 扩展信息,
     *                      ),
     *                      'media_info'=>array(
     *                          'base_info' => MEDIA信息
     *                          'ex_info' => 扩展信息
     *                      ),
     *                      'live_info'=>array(
     *                          'base_info' => MEDIA信息
     *                          'ex_info' => 扩展信息
     *                      ),
     *                      'live_index_info'=>array(
     *                          'base_info' => MEDIA信息
     *                          'ex_info' => 扩展信息
     *                      ),
     *                  )
     */
    public function init($c2_queue_info=array())
    {
        //赋值主要参数
        $this->c2_info = isset($c2_queue_info['c2_info']) ? $c2_queue_info['c2_info'] : array();
        $this->video_info = isset($c2_queue_info['video_info']) ? $c2_queue_info['video_info'] : array();
        $this->index_info = isset($c2_queue_info['index_info']) ? $c2_queue_info['index_info'] : array();
        $this->media_info = isset($c2_queue_info['media_info']) ? $c2_queue_info['media_info'] : array();
        $this->seekpoint_info = isset($c2_queue_info['seekpoint_info']) ? $c2_queue_info['seekpoint_info'] : array();
        $this->live_info = isset($c2_queue_info['live_info']) ? $c2_queue_info['live_info'] : array();
        $this->live_index_info = isset($c2_queue_info['live_index_info']) ? $c2_queue_info['live_index_info'] : array();
        $this->live_media_info = isset($c2_queue_info['live_media_info']) ? $c2_queue_info['live_media_info'] : array();
        $this->file_info = isset($c2_queue_info['file_info']) ? $c2_queue_info['file_info'] : array();
        $this->package_info = isset($c2_queue_info['package_info']) ? $c2_queue_info['package_info'] : array();
        $this->playbill_info = isset($c2_queue_info['playbill_info']) ? $c2_queue_info['playbill_info'] : array();
        //SPID
        if(empty($this->str_sp_id))
        {
            if(isset($c2_queue_info['c2_info']) && !empty($c2_queue_info['c2_info']['nns_org_id']))
            {
                $this->str_sp_id = $c2_queue_info['c2_info']['nns_org_id'];
            }
            $arr_sp_config = \m_config::_get_sp_info($this->str_sp_id);
            $this->arr_sp_config = isset($arr_sp_config['data_info']['nns_config']) ? $arr_sp_config['data_info']['nns_config'] : array();
        }

        //CPID
        if(isset($c2_queue_info['c2_info']) && !empty($c2_queue_info['c2_info']['nns_cp_id']))
        {
            $this->str_source_id = $c2_queue_info['c2_info']['nns_cp_id'];
        }
        $arr_source_config = \m_config::_get_cp_info($this->str_source_id);
        $this->arr_source_config = isset($arr_source_config['data_info']['nns_config']) ? $arr_source_config['data_info']['nns_config'] : array();

        $this->m_epg_inout = new \m_epg_inout();
    }

    /**
     * 获取项目配置文件
     * @param $key
     * @return string
     */
    public function get_project_config($key)
    {
        $project = \evn::get('project');
        return \m_config::get_project_config($project, $key);
    }

    /**
     * 获取DC
     * @return Object
     */
    public function get_dc()
    {
        return \m_config::get_dc();
    }
    /**
     * 获取主媒资数据
     * @return array()
     */
    public function get_video_info()
    {
        return $this->video_info;
    }
    /**
     * 获取分集数据
     * @return array()
     */
    public function get_index_info()
    {
        return $this->index_info;
    }
    /**
     * 获取片源数据
     * @return array()
     */
    public function get_media_info()
    {
        return $this->media_info;
    }
    /**
     * 获取打点信息数据
     * @return array()
     */
    public function get_seekpoint_info()
    {
        return $this->seekpoint_info;
    }
    /**
     * 获取频道信息
     * @return array()
     */
    public function get_live_info()
    {
        return $this->live_info;
    }
    /**
     * 获取频道分集信息
     * @return array()
     */
    public function get_live_index_info()
    {
        return $this->live_index_info;
    }
    /**
     * 获取频道分集片源信息
     * @return array()
     */
    public function get_live_media_info()
    {
        return $this->live_media_info;
    }
    /**
     * 获取文件包信息
     * @return array()
     */
    public function get_file_info()
    {
        return $this->file_info;
    }
    /**
     * 获取产品包信息
     * @return array()
     */
    public function get_package_info()
    {
        return $this->package_info;
    }
    /**
     * 获取节目单信息
     * @return array()
     */
    public function get_playbill_info()
    {
        return $this->playbill_info;
    }
    /**
     * 获取C2任务类型
     * @return string
     */
    public function get_c2_type()
    {
        if(!empty($this->c2_info) && isset($this->c2_info['nns_type']))
        {
            return $this->c2_info['nns_type'];
        }
        return '';
    }
    /**
     * 获取EPG注入ID的模式
     * @param $video_type 资源类型，未传使用当前资源类型，传入时使用传入的类型
     * @return string
     */
    public function get_video_epg_import_id($video_type='')
    {
        if(empty($video_type))
        {
            $video_type = $this->get_c2_type();
            if(empty($video_type))
            {
                return '';
            }
        }

        $key = "get_" . $video_type . "_info";
        if(!method_exists($this,$key))
        {
            return '';
        }
        $mixed_info = $this->$key();
        $query_info = array($video_type=>$mixed_info);
        $cdn_import = $this->m_queue_model->_get_cdn_epg_import_id($this->str_sp_id,$video_type,$query_info,'epg',null);
        if ($cdn_import['ret'] != '0')
        {
            return '';
        }
        return $cdn_import['data_info'];
    }
    /**
     * 获取C2内容
     * @param $key 字段
     * @retrun string
     */
    public function get_c2_info($key='')
    {
        if(empty($key))
        {
            return '';
        }
        if (strpos($key, 'nns_') === FALSE)
        {
            $key = 'nns_' . $key;
        }
        if(!empty($this->c2_info) && isset($this->c2_info[$key]))
        {
            return $this->c2_info[$key];
        }
        return '';
    }
    /**
     * 获取GUID
     * @param $str
     * @return string
     */
    public function get_guid($str='')
    {
        return np_guid_rand($str);
    }
    /**
     * 获取注入平台资源库栏目信息
     * @param $video_info //传入的媒资信息
     * @param $is_vod //属于点播，默认
     * @return array
     */
    public function get_category_info($video_info, $is_vod=true)
    {
        $is_category_v2 = get_config_v2('g_category_v2');
        if($is_vod)
        {
            $str_video_type = 0;
        }
        else
        {
            $str_video_type = 1;
        }

        if ($is_category_v2)
        {
            $m_category_inout = new \m_category_v2_inout();
            $depot_info = $m_category_inout->query(array(
                'base_info' => array(
                    'nns_video_type' => $str_video_type,
                    'nns_category_id' => $video_info['base_info']['nns_category_id'],
                    'nns_cp_id' => $video_info['base_info']['nns_cp_id'],
                )));
        }
        else
        {
            $m_category_inout = new \m_category_inout();
            $depot_info = $m_category_inout->query(array('base_info'=>array('nns_video_type' => $str_video_type)));
        }


        return $depot_info;
    }
    /**
     * 注入EPG的内容写入本地文件并上传至FTP
     * @param $content 写入本地文件的XML
     * @param $ftp_addr 上传FTP的绝对路径
     * @return array(
     *  'ret',
     *  'reason',
     *  'data_info' => array(
     *      'base_dir',//本地相对地址
     *      'absolute_dir',//本地绝对地址
     *      'ftp_url'//上传至FTP地址
     *   )
     * )
     */
    public function save_execute_import_content($content,$ftp_addr='')
    {
        if(empty($content))
        {
            return $this->write_log_return('注入EPG文件内容为空',NS_CDS_FAIL);
        }
        $local_save = \m_config::write_execute_epg_import_content($this->str_sp_id,$this->get_c2_type(),$this->get_c2_info('action'),$content);
        if($local_save['ret'] != NS_CDS_SUCCE)
        {
            return $this->write_log_return($local_save,NS_CDS_FAIL);
        }
        $local_save['data_info']['ftp_url'] = '';
        if(!empty($ftp_addr))
        {
            $up_re = \m_config::up_to_ftp($local_save['data_info']['base_dir'],$local_save['data_info']['absolute_dir'],$ftp_addr);
            if($up_re['ret'] != NS_CDS_SUCCE)
            {
                return $this->write_log_return("本地工单上传FTP失败,FTP为{$ftp_addr},本地地址为:{$local_save['data_info']['base_dir']}|{$local_save['data_info']['absolute_dir']}",NS_CDS_FAIL);
            }
            $local_save['data_info']['ftp_url'] = $up_re['data_info'];
        }
        return $local_save;
    }
    /**
     * 创建EPG注入日志
     * @parma $epg_log_info array(
     *          'nns_video_id' => 视频ID,
     *          'nns_video_type' => 资源类型,
     *          'nns_action' => 行为动作,
     *          'nns_status' => 状态码,
     *          'nns_reason' => 原因,
     *          'nns_data' => 注入数据相对地址,
     *          'nns_name' => 注入名称
     *      )
     * @return array

     */
    public function build_epg_log($epg_log_info)
    {
        if(!isset($epg_log_info['nns_id']) || empty($epg_log_info['nns_id']))
        {
            $c2_log_info['nns_id'] = $this->get_guid("vod");
        }
        if(!empty($epg_log_info['nns_data']))
        {
            $save_dir = $this->save_execute_import_content($epg_log_info['nns_data']);
            if($save_dir['ret'] != NS_CDS_SUCCE)
            {
                $epg_log_info['nns_data'] = '';
            }
            else
            {
                $epg_log_info['nns_data'] = $save_dir['data_info']['base_dir'];
            }
        }
        $epg_log_info['nns_org_id'] = $this->str_sp_id;
        return \nl_epg_log::add($this->get_dc(),$epg_log_info);
    }

    /**
     * 统一EPG注入回调
     * @param $code //详见m_const常量
     * @param $reason //描述
     * @return array
     */
    public function is_ok($code, $reason='')
    {
        $c2_info = $this->c2_info;
        $epg_fail_time = $c2_info['nns_epg_fail_time'];
        //根据注入反馈的状态将task的状态设置为失败或成功
        switch ($code)
        {
            case NS_CDS_EPG_SUCCE: //注入EPG成功
                $c2_task_info = array('nns_epg_status' => NS_CDS_EPG_SUCCE);
                break;
            case NS_CDS_EPG_NOTIFY_NOTFOUND://下游未找到
                $c2_task_info = array('nns_epg_status' => NS_CDS_EPG_SUCCE);
                break;
            case NS_CDS_EPG_LOADING://正在注入EPG
                $c2_task_info = array('nns_epg_status' => NS_CDS_EPG_LOADING);
                break;
            default:
                $epg_fail_time = (int)$epg_fail_time + 1;
                $c2_task_info = array(
                    'nns_epg_status' => NS_CDS_EPG_FAIL,
                    'nns_epg_fail_time' => $epg_fail_time,
                );
                break;
        }

        //修改C2任务
        $c2_re = \nl_c2_task::edit($this->get_dc(), $c2_task_info, $c2_info['nns_id']);
        if($c2_re['ret'] != NS_CDS_SUCCE)
        {
            return $this->write_log_return($c2_re['reason'],$c2_re['ret']);
        }
        $c2_info['nns_epg_status'] = $c2_task_info['nns_epg_status'];

        //获取注入cdn的id，xinxin.deng 2018/2/26 14:27
        $cdn_id = $this->get_video_epg_import_id($c2_info['nns_type']);
        //得到返回数据值
        $mg_asset_type = '';
        $mg_asset_id = '';
        $mg_part_id = '';
        $mg_file_id = '';

        switch ($c2_info['nns_type'])
        {
            case NS_CDS_VIDEO:
                $mg_asset_type = 1;
                $mg_asset_id = $this->video_info['base_info']['nns_asset_import_id'];
                break;
            case NS_CDS_INDEX:
                $mg_asset_type = 2;
                $mg_part_id = $this->index_info['base_info']['nns_import_id'];
                $mg_asset_id = $this->video_info['base_info']['nns_asset_import_id'];
                break;
            case NS_CDS_MEDIA:
                $mg_asset_type = 3;
                $mg_file_id = $this->media_info['base_info']['nns_import_id'];
                $mg_part_id = $this->index_info['base_info']['nns_import_id'];
                $mg_asset_id = $this->video_info['base_info']['nns_asset_import_id'];
                break;
            default:
                break;
        }

        //封装成统一样式数组
        $data = array(
            'cdn_id' => $cdn_id,
            'site_id' => $this->arr_sp_config['site_id'],
            'mg_asset_type' => $mg_asset_type,
            'mg_asset_id' => $mg_asset_id,
            'mg_part_id' => $mg_part_id,
            'mg_file_id' => $mg_file_id,
        );
        //epg的状态
        $ret = $code === 99 ? 0 : -1;
        \ns_core\m_msg_callback::is_ok($c2_info['nns_cp_id'],$c2_info['nns_message_id'], $ret,'[epg注入]' . $reason, $data, $c2_info);
        //}

        return $this->write_log_return($reason);
    }
    /**
     * 根据参数记录日志，并返回
     * @param string $msg 文字消息
     * @param $code //状态码，默认成功0
     * @return array()
     */
    public function write_log_return($msg, $code=NS_CDS_SUCCE)
    {
        //来源ID、SPID、C2有值，则存CDN队列日志 queue_log
        if(!empty($this->get_c2_info('org_id')) && !empty($this->c2_info) && !empty($this->get_c2_info('id')))
        {
            $ret = \m_config::write_epg_execute_log($msg, $this->get_c2_info('org_id'), $this->get_c2_info('id'));
        }
        else //存入公共日志  global_log
        {
            $ret = \m_config::write_global_execute_log(NS_CDS_EPG_QUEUE,$this->str_source_id,$this->str_sp_id,$this->get_c2_info('action'),$this->get_c2_type(),var_export($msg,true));
        }

        return \m_config::return_data($code,$msg,$ret['data_info']);
    }
    /**
     * 保存C2执行过程日志
     * @param $return_info
     * @return mixe
     */
    public function save_exec_log($return_info)
    {
        if($this->get_c2_info('id'))
        {
            if (isset($return_info['data_info']['base_dir']) && !empty($return_info['data_info']['base_dir']))
            {
                $exec_log = array("nns_queue_execute_url" => $return_info['data_info']['base_dir']);
                //修改C2任务
                \nl_c2_task::edit($this->get_dc(), $exec_log, $this->get_c2_info('id'));
            }
            elseif (isset($return_info['error_data_info']['base_dir']) && !empty($return_info['error_data_info']['base_dir']))
            {
                $exec_log = array("nns_queue_execute_url" => $return_info['error_data_info']['base_dir']);
                //修改C2任务
                \nl_c2_task::edit($this->get_dc(), $exec_log, $this->get_c2_info('id'));
            }
        }
        return $return_info;
    }
    /**
     * 注入
     * @param $url 请求地址
     * @param $data 请求数据
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function import($url,$data)
    {
        if(empty($url) || empty($data))
        {
            return $this->write_log_return("注入EPG入参地址或参数错误",NS_CDS_FAIL);
        }
        $curl = new \np_http_curl_class();
        $curl_ret = $curl->post($url,$data);
        $curl_info = $curl->curl_getinfo();
        if($curl_info['http_code'] != '200')
        {
            return $this->write_log_return('EPG注入失败,HTTP状态码'.$curl_info['http_code'],NS_CDS_FAIL);
        }
        $this->write_log_return("CURL成功");
        return \m_config::return_data(NS_CDS_SUCCE,'',$curl_ret);
    }
    /**
     * 变量初始化
     */
    public function destroy_init()
    {
        $this->str_sp_id = '';//SPID
        $this->str_source_id = '';//CPID
        $this->arr_sp_config = array();//SP配置
        $this->arr_cp_config = array();//CP配置

        //$this->m_queue_model = array();
        $this->c2_task_info = array();//C2队列数据  写一个key=>value获取
        $this->video_info = array();//主媒资数据
        $this->index_info = array();//分集数据
        $this->seekpoint_info = array();//分集打点数据
        $this->media_info = array();//片源数据
        $this->live_info = array();//直播
        $this->live_index_info = array();//直播分集
        $this->live_media_info = array();//直播源
        $this->file_info = array();//文件包
        $this->package_info = array();//产品打包
        $this->playbill_info = array();//节目单信息
    }

    /************************************************************标准注入EPG   芒果由于花絮、预告片需重新改写一部分*******************************************************/
    /**
     * EPG注入解释器统一入口
     */
    public function explain()
    {
        $fuc_name = $this->get_c2_type();
        if(empty($fuc_name))
        {
            return $this->write_log_return('C2类型参数为空,无法注入EPG',NS_CDS_FAIL);
        }
        return $this->save_exec_log($this->$fuc_name());
    }
    /**
     * 组装XML数据
     * @param $info 组装XML的数组
     * array(
     *      key => array(value),
     *      key => array(value),
     * )
     * @return string | xml
     */
    public function make_import_xml($info)
    {
        if(!is_array($info) || empty($info))
        {
            return '';
        }
        $dom = new \DOMDocument('1.0', 'utf-8');
        foreach ($info as $key=>$in_value)
        {
            $$key = $dom->createElement($key);
            $dom->appendchild($$key);
            if(!empty($in_value) && is_array($in_value))
            {
                foreach ($in_value as $k=>$val)
                {
                    $$key->setAttribute($k,$val);
                }
            }
        }
        return $dom->saveXML();
    }
    /**
     * 组装打点信息XML数据
     * @param $info 组装XML的数组
     * array(
     *      key => array(
     *          k => array(value),
     *          k => array(value)
     *      ),
     * )
     * @return string | xml
     */
    public function make_seekpoint_import_xml($info)
    {
        if(!is_array($info) || empty($info))
        {
            return '';
        }
        $dom = new \DOMDocument('1.0', 'utf-8');
        foreach ($info as $key=>$in_value)
        {
            $$key = $dom->createElement($key);
            $dom->appendchild($$key);
            if(!empty($in_value) && is_array($in_value))
            {
                foreach ($in_value as $k=>$val)
                {
                    if(!is_int($k))
                    {
                        $$k = $dom->createElement($k);
                        $$key->appendchild($$k);
                    }
                    if(!empty($val) && is_array($val))
                    {
                        foreach ($val as $m=>$n)
                        {
                            if(!is_int($m))
                            {
                                $$m = $dom->createElement($m);
                                if(!is_int($k))
                                {
                                    $$k->appendchild($$m);
                                }
                                else
                                {
                                    $$key->appendchild($$m);
                                }
                            }
                            if(!empty($n) && is_array($n))
                            {
                                foreach ($n as $a=>$b)
                                {
                                    $$m->setAttribute($a,$b);
                                }
                            }
                        }
                    }
                }
            }
        }
        return $dom->saveXML();
    }
    /**
     * 点播、直播、节目单 xml组装
     * @param string $type
     * @param string $data_vod
     * @param string $data_index
     * @param string $data_media
     * @param string $data_vod_ex
     * @param string $data_index_ex
     * @param string $data_media_ex
     * @return string
     */
    public function make_import_cms_xml($type='live',$data_vod=null,$data_index=null,$data_media=null,$data_vod_ex=null,$data_index_ex=null,$data_media_ex=null)
    {
        switch ($type)
        {
            case 'live':
                $str_assettype=2;
                break;
            case 'video':
                $str_assettype=1;
                break;
            case 'playbill':
                $str_assettype=2;
                break;
            default:
                return '';
        }
        $str_xml  = '<?xml version="1.0" encoding="UTF-8"?>';
        $str_xml .= '<assetcontent>';
        $str_xml .= 	'<assettype>'.$str_assettype.'</assettype>';
        $str_xml .= 	'<assetdesc>4</assetdesc>';
        if(!empty($data_vod) && is_array($data_vod))
        {
            $str_xml .= 	'<'.$type.'>';
            $str_operation = ($data_vod['nns_deleted'] !=1) ? 1 : 2;
            $str_xml .= 		'<assetoperation>'.$str_operation.'</assetoperation>';
            $str_xml .= 		'<base_info>';
            foreach ($data_vod as $key=>$val)
            {
                if (in_array($key, array('nns_id','nns_create_time','nns_modify_time','nns_org_id','nns_state','nns_check','nns_deleted','nns_depot_id','nns_domain')))
                {
                    continue;
                }
                $key = str_replace('nns_', '', $key);
                $val = str_replace(array('&','<','>','"',"'"), '',$val);
                $str_xml .= 		'<'.$key.'>'.$val.'</'.$key.'>';
            }
            $str_xml .= 		'</base_info>';
            $str_xml .= 		'<ex_info>';
            if(!empty($data_vod_ex) && is_array($data_vod_ex))
            {
                foreach ($data_vod_ex as $vod_ex_val)
                {
                    $str_xml .= 		'<'.$vod_ex_val['nns_key'].'>'.$vod_ex_val['nns_value'].'</'.$vod_ex_val['nns_key'].'>';
                }
            }
            $str_xml .= 		'</ex_info>';
            $str_xml .= 	'</'.$type.'>';
        }
        if(!empty($data_index) && is_array($data_index))
        {
            $str_xml .= 	'<'.$type.'_index>';
            $str_operation = ($data_index['nns_deleted'] !=1) ? 1 : 2;
            $str_xml .= 		'<assetoperation>'.$str_operation.'</assetoperation>';
            $str_xml .= 		'<base_info>';
            foreach ($data_index as $key=>$val)
            {
                if (in_array($key, array('nns_id','nns_create_time','nns_modify_time','nns_deleted')))
                {
                    continue;
                }
                $key = str_replace('nns_', '', $key);
                $str_xml .= 		'<'.$key.'>'.$val.'</'.$key.'>';
            }
            $str_xml .= 		'</base_info>';
            $str_xml .= 		'<ex_info>';
            if(!empty($data_index_ex) && is_array($data_index_ex))
            {
                foreach ($data_index_ex as $index_ex_val)
                {
                    $str_xml .= 		'<'.$index_ex_val['nns_key'].'>'.$index_ex_val['nns_value'].'</'.$index_ex_val['nns_key'].'>';
                }
            }
            $str_xml .= 		'</ex_info>';
            $str_xml .= 	'</'.$type.'_index>';
        }
        if(!empty($data_media) && is_array($data_media))
        {
            $str_xml .= 	'<'.$type.'_media>';
            $str_operation = ($data_media['nns_deleted'] !=1) ? 1 : 2;
            $str_xml .= 		'<assetoperation>'.$str_operation.'</assetoperation>';
            $str_xml .= 		'<base_info>';
            foreach ($data_media as $key=>$val)
            {
                if (in_array($key, array('nns_id','nns_create_time','nns_modify_time','nns_deleted','nns_domain','nns_drm_flag','nns_ext_url')))
                {
                    continue;
                }
                $key = str_replace('nns_', '', $key);
                if($key == 'drm_ext_info')
                {
                    $val = htmlspecialchars($val);
                }
                $str_xml .= 	'<'.$key.'>'.$val.'</'.$key.'>';
            }
            if(strlen($data_media['nns_ext_url']) <1)
            {
                $str_xml .= 	'<ex_url>'.$data_media['nns_url'].'</ex_url>';
            }
            else
            {
                $str_xml .= 	'<ex_url>'.$data_media['nns_ext_url'].'</ex_url>';
            }
            $str_xml .= 		'</base_info>';
            $str_xml .= 		'<ex_info>';
            if(!empty($data_media_ex) && is_array($data_media_ex))
            {
                foreach ($data_media_ex as $media_ex_val)
                {
                    $str_xml .= 		'<'.$media_ex_val['nns_key'].'>'.$media_ex_val['nns_value'].'</'.$media_ex_val['nns_key'].'>';
                }
            }
            $str_xml .= 		'</ex_info>';
            $str_xml .= 	'</'.$type.'_media>';
        }
        $str_xml .= '</assetcontent>';
        return $str_xml;
    }
    /**
     * 主媒资注入
     * @return array
     */
    public function video()
    {
        $video_type = $this->get_c2_type();
        $video_info = $this->get_video_info();
        if (empty($video_info))
        {
            return $this->write_log_return("C2任务video元数据错误，执行注入EPG失败",NS_CDS_FAIL);
        }

        //根据C2任务的行为动作进行分派
        if ($this->get_c2_info('action') === NS_CDS_DELETE || $video_info['base_info']['nns_deleted'] != 0)
        {
            //删除
            return $this->video_destroy();
        }
        elseif ($this->get_c2_info('action') === NS_CDS_MODIFY)
        {
            $action = NS_CDS_MODIFY;
        }
        else
        {
            $action = NS_CDS_ADD;
        }

        $epg_import_id = $this->get_video_epg_import_id();
        if (empty($epg_import_id))
        {
            return $this->write_log_return("注入EPG时生成注入ID失败",NS_CDS_FAIL);
        }
        //获取播控平台资源库栏目--用于获取栏目对于关系与资源类型
        $is_category_v2 = get_config_v2('g_category_v2');
        $categorys = $this->get_category_info($video_info);
        if($categorys['ret'] != NS_CDS_SUCCE)
        {
            return $this->write_log_return($categorys['reason'],NS_CDS_FAIL);
        }
        if ($is_category_v2)
        {
            $category_name = $categorys['data_info'][0]['nns_name'];
            $parent_assets_category = $categorys['data_info'][0]['nns_parent_name'];
            $parent_category_id = $categorys['data_info'][0]['nns_parent_id'];
        }
        else
        {
            //所有栏目id的集合
            $arr_category_all = array();
            $category_name = '';
            //默认父级栏目id 为0
            $parent_category_id = 0;
            foreach ($categorys['data_info'] as $category)
            {
                if ((string)$category['id'] == (string)$video_info['base_info']['nns_category_id'])
                {
                    $category_name = $category['name'];
                    //查询父级栏目id 如果为0 则为0  如果不为0 则是该父级的id
                    $parent_category_id =  ((string)$category['parent'] == '0') ? 0 : (string)$category['parent'];
                }
                //所有栏目id的集合
                $arr_category_all[(string)$category['id']] = $category['name'];
            }
            //父级栏目的名称
            $parent_assets_category = isset($arr_category_all[$parent_category_id]) ? $arr_category_all[$parent_category_id] : '';
        }
        //EPG注入数据数组
        $epg_import_arr = array(
            "assets_id" => $epg_import_id,
            "video_type" => 0,
            "parent_assets_category" => $parent_assets_category,
            "parent_assets_category_id" => $parent_category_id,
            "assets_category" => $category_name,
            "view_type" => $video_info['base_info']['nns_view_type'],
            "name" => $video_info['base_info']['nns_name'],
            "sreach_key" => $video_info['base_info']['nns_keyword'],
            "director" => $video_info['base_info']['nns_director'],
            "player" => $video_info['base_info']['nns_actor'],
            "user_score_arg" => $video_info['base_info']['nns_score_total'],
            "tag" => $video_info['base_info']['nns_kind'],
            "Tags" => $video_info['base_info']['nns_tag'],
            "region" => $video_info['base_info']['nns_area'],
            "release_time" => $video_info['base_info']['nns_show_time'],
            "totalnum" => $video_info['base_info']['nns_all_index'],
            "smallpic" => $video_info['base_info']['nns_image2'],
            "midpic" => $video_info['base_info']['nns_image1'],
            "bigpic" => $video_info['base_info']['nns_image0'],
            "verticality_img" => $video_info['base_info']['nns_image_v'],
            "horizontal_img" => $video_info['base_info']['nns_image_h'],
            "square_img" => $video_info['base_info']['nns_image_s'],
            "intro" => $video_info['base_info']['nns_summary'],
            "producer" => $video_info['base_info']['nns_producer'],
            "play_role" => $video_info['base_info']['nns_play_role'],
            "subordinate_name" => $video_info['base_info']['nns_alias_name'],
            "english_name" => $video_info['base_info']['nns_eng_name'],
            "language" => $video_info['base_info']['nns_language'],
            "caption_language" => $video_info['base_info']['nns_text_lang'],
            "copyright_range" => $video_info['base_info']['nns_remark'],
            "copyright" => $video_info['base_info']['nns_copyright'],
            "total_clicks" => $video_info['base_info']['nns_play_count'],
            "view_len" => $video_info['base_info']['nns_view_len'],
            "original_id" => $video_info['base_info']['nns_asset_import_id'],
            "preview_type" => isset($this->preview_type[$video_info['base_info']['nns_ishuaxu']]) ? $this->preview_type[$video_info['base_info']['nns_ishuaxu']] : 0,//主媒资类型。0：正片；3短片；4预告片；5花絮。若该参数为空则默认为正片---xinxin.deng 2018/8/31 15:42
        );
        if(is_array($video_info['ex_info']) && !empty($video_info['ex_info']))
        {
            foreach ($video_info['ex_info'] as $key=>$value)
            {
                $epg_import_arr[$key] = $value;
            }
        }
        //根据CP配置判断注入的CP是来源还是原始内容提供商
        if(isset($this->arr_source_config['original_cp_enabled']) && (int)$this->arr_source_config['original_cp_enabled'] === 1)
        {
            $epg_import_arr['asset_source'] = $video_info['base_info']['nns_producer'];
        }
        elseif(isset($this->arr_source_config['original_cp_enabled']) && (int)$this->arr_source_config['original_cp_enabled'] === 2)
        {
            $epg_import_arr['asset_source'] = 'fuse_xiaole';
        }
        else
        {
            $epg_import_arr['asset_source'] = $this->str_source_id;
        }
        //拼装注入EPG的XML
        $import_xml = $this->make_import_xml(array($video_type => $epg_import_arr));
        if(empty($import_xml))
        {
            return $this->write_log_return('注入EPG的XML拼装失败',NS_CDS_FAIL);
        }
        //注入EPG的参数数组
        $data = array(
            "func" => "modify_video",
            "assets_video_type" => 0,
            "assets_content" => $import_xml,
            "assets_video_info" => $import_xml,
            "asset_source_id" => $epg_import_arr['asset_source'],
            "assets_id" => $epg_import_id,
            "bk_message_id" => $this->get_c2_info("message_id"),
            "bk_cp_id" => $this->str_source_id
        );
        $this->write_log_return("请求地址:".$this->arr_sp_config['epg_url']."请求参数:".var_export($data,true));

        //分发到两个epg
        $arr_epg_url = explode(',', $this->arr_sp_config['epg_url']);

        //CURL请求
        $curl_ret = $this->import($arr_epg_url[0],$data);

        if($curl_ret['ret'] != NS_CDS_SUCCE)
        {
            $epg_code = NS_CDS_EPG_FAIL;
            $this->write_log_return("注入EPG[" . $arr_epg_url[0] . "]失败返回：" . $curl_ret['reason'],NS_CDS_FAIL);
        }
        else
        {
            if (isset($arr_epg_url[1]) && strlen($arr_epg_url[1]) > 0)
            {
                //CURL请求
                $curl_ret = $this->import($arr_epg_url[1],$data);
                if ($curl_ret['ret'] != NS_CDS_SUCCE)
                {
                    $this->write_log_return("注入EPG[" . $arr_epg_url[1] . "]失败返回：" . $curl_ret['reason'],NS_CDS_FAIL);
                }
            }
            $this->write_log_return("注入EPG返回结果为：" . var_export($curl_ret['data_info'],true));
            $curl_ret = json_decode(json_encode(@simplexml_load_string($curl_ret['data_info'])),true);
            $this->write_log_return("注入EPG返回结果解析：" . var_export($curl_ret,true));
            if($curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_SUCCE && $curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_NOTFOUND)
            {
                $epg_code = NS_CDS_EPG_FAIL;
            }
            else
            {
                $epg_code = NS_CDS_EPG_SUCCE;
            }
        }
        //注入EPG日志
        $log_data = array (
            'nns_video_id' => $video_info['base_info']['nns_id'],
            'nns_video_type' => NS_CDS_VIDEO_V2,
            'nns_org_id' => $this->str_sp_id,
            'nns_create_time' => date('Y-m-d H:i:s'),
            'nns_action' => $action,
            'nns_status' => $curl_ret['@attributes']['ret'],
            'nns_reason' => addslashes($curl_ret['@attributes']['reason']),
            'nns_data' => $import_xml,
            'nns_name' => $this->get_c2_info('name')
        );
        //注入EPG日志入数据库
        $this->build_epg_log($log_data);
        //上报状态
        return $this->is_ok($epg_code,$curl_ret['@attributes']['reason']);
    }

    /**
     * 主媒资删除注入
     * @return array
     */
    public function video_destroy()
    {
        $video_info = $this->get_video_info();
        if (empty($video_info))
        {
            return $this->write_log_return("C2任务video元数据错误，执行注入EPG失败",NS_CDS_FAIL);
        }
        //获取注入EPG的主媒资注入ID
        $epg_import_id = $this->get_video_epg_import_id();
        if (empty($epg_import_id))
        {
            return $this->write_log_return("注入EPG时生成注入ID失败",NS_CDS_FAIL);
        }
        //根据CP配置判断注入的CP是来源还是原始内容提供商
        if(isset($this->arr_source_config['original_cp_enabled']) && (int)$this->arr_source_config['original_cp_enabled'] === 1)
        {
            $asset_source = $video_info['base_info']['nns_producer'];
        }
        elseif(isset($this->arr_source_config['original_cp_enabled']) && (int)$this->arr_source_config['original_cp_enabled'] === 2)
        {
            $asset_source = 'fuse_xiaole';
        }
        else
        {
            $asset_source = $this->str_source_id;
        }
        //注入EPG的参数数组
        $data = array(
            "func" => "delete_asset",
            "assets_video_type" => 0,
            "assets_id" => $epg_import_id,
            "bk_message_id" => $this->get_c2_info("message_id"),
            "bk_cp_id" => $this->str_source_id,
            "asset_source" => $asset_source
        );
        $this->write_log_return("请求地址:".$this->arr_sp_config['epg_url']."请求参数:".var_export($data,true));

        //分发到两个epg
        $arr_epg_url = explode(',', $this->arr_sp_config['epg_url']);

        //CURL请求
        $curl_ret = $this->import($arr_epg_url[0],$data);

        if($curl_ret['ret'] != NS_CDS_SUCCE)
        {
            $epg_code = NS_CDS_EPG_FAIL;
            $this->write_log_return("注入EPG[" . $arr_epg_url[0] . "]失败返回：" . $curl_ret['reason'],NS_CDS_FAIL);
        }
        else
        {
            if (isset($arr_epg_url[1]) && strlen($arr_epg_url[1]) > 0)
            {
                //CURL请求
                $curl_ret = $this->import($arr_epg_url[1],$data);
                if ($curl_ret['ret'] != NS_CDS_SUCCE)
                {
                    $this->write_log_return("注入EPG[" . $arr_epg_url[1] . "]失败返回：" . $curl_ret['reason'],NS_CDS_FAIL);
                }
            }
            $this->write_log_return("注入EPG返回结果为：" . var_export($curl_ret['data_info'],true));
            $curl_ret = json_decode(json_encode(@simplexml_load_string($curl_ret['data_info'])),true);
            $this->write_log_return("注入EPG返回结果解析：" . var_export($curl_ret,true));
            if($curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_SUCCE && $curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_NOTFOUND)
            {
                $epg_code = NS_CDS_EPG_FAIL;
            }
            else
            {
                $epg_code = NS_CDS_EPG_SUCCE;
            }
        }
        //注入EPG日志
        $log_data = array (
            'nns_video_id' => $video_info['base_info']['nns_id'],
            'nns_video_type' => NS_CDS_VIDEO_V2,
            'nns_org_id' => $this->str_sp_id,
            'nns_create_time' => date('Y-m-d H:i:s'),
            'nns_action' => $this->get_c2_info('action'),
            'nns_status' => $curl_ret['@attributes']['ret'],
            'nns_reason' => addslashes($curl_ret['@attributes']['reason']),
            'nns_data' => '',
            'nns_name' => $this->get_c2_info('name')
        );
        //注入EPG日志入数据库
        $this->build_epg_log($log_data);
        //上报状态
        return $this->is_ok($epg_code,$curl_ret['@attributes']['reason']);
    }
    /**
     * 分集注入
     * @return array
     */
    public function index()
    {
        $video_type = $this->get_c2_type();
        $video_info = $this->get_video_info();
        $index_info = $this->get_index_info();
        $seekpoint_info = $this->get_seekpoint_info();
        if (empty($video_info) || empty($index_info))
        {
            return $this->write_log_return("C2任务video|index元数据错误，执行注入EPG失败",NS_CDS_FAIL);
        }

        $epg_import_id = $this->get_video_epg_import_id(NS_CDS_VIDEO);

        //根据C2任务的行为动作进行分派
        if ($this->get_c2_info('action') === NS_CDS_DELETE || $index_info['base_info']['nns_deleted'] != 0)
        {
            //删除
            return $this->index_destroy();
        }
        elseif ($this->get_c2_info('action') === NS_CDS_MODIFY)
        {
            $action = NS_CDS_MODIFY;
        }
        else
        {
            $action = NS_CDS_ADD;
        }

        $epg_index_import_id = $this->get_video_epg_import_id();
        if (empty($epg_index_import_id))
        {
            return $this->write_log_return("注入EPG时生成video|index注入ID失败",NS_CDS_FAIL);
        }
        $ext_url = (isset($index_info['base_info']['nns_ext_url']) && strlen($index_info['base_info']['nns_ext_url']) > 0) ? json_decode($index_info['base_info']['nns_ext_url'],true) : null;
        //EPG注入数据数组
        $epg_import_arr = array(
            "clip_id" => $epg_index_import_id,
            "name" => $index_info['base_info']['nns_name'],
            "index" => isset($ext_url['sequence']) ? $ext_url['sequence'] : $index_info['base_info']['nns_index'],
            "time_len" => $index_info['base_info']['nns_time_len'],
            "summary" => $index_info['base_info']['nns_summary'],
            "watch_focus" => $index_info['base_info']['nns_watch_focus'],
            "pic" => $index_info['base_info']['nns_image'],
            "director" => $index_info['base_info']['nns_director'],
            "player" => $index_info['base_info']['nns_actor'],
            "total_clicks" => $index_info['base_info']['nns_play_count'],
            "release_time" => $index_info['base_info']['nns_release_time'],
            "update_time" => $index_info['base_info']['nns_update_time'],
            "month_clicks" => "",
            "week_clicks" => "",
            "day_clicks" => "",
            "original_id" => $index_info['base_info']['nns_import_id']
        );
        if(is_array($index_info['ex_info']) && !empty($index_info['ex_info']))
        {
            foreach ($index_info['ex_info'] as $key=>$value)
            {
                $epg_import_arr[$key] = $value;
            }
        }
        //根据CP配置判断注入的CP是来源还是原始内容提供商
        if(isset($this->arr_source_config['original_cp_enabled']) && (int)$this->arr_source_config['original_cp_enabled'] === 1)
        {
            $epg_import_arr['asset_source'] = $video_info['base_info']['nns_producer'];
        }
        elseif(isset($this->arr_source_config['original_cp_enabled']) && (int)$this->arr_source_config['original_cp_enabled'] === 2)
        {
            $epg_import_arr['asset_source'] = 'fuse_xiaole';
        }
        else
        {
            $epg_import_arr['asset_source'] = $this->str_source_id;
        }
        //拼装注入EPG的XML
        $import_xml = $this->make_import_xml(array($video_type => $epg_import_arr));
        if(empty($import_xml))
        {
            return $this->write_log_return('注入EPG的XML拼装失败',NS_CDS_FAIL);
        }
        //拼装打点信息
        $seekpoint_arr = '';
        if(!empty($seekpoint_info))
        {
            foreach ($seekpoint_info['base_info'] as $value)
            {
                $seekpoint_arr['metadata'][] = array(
                    "item" => array(
                        "begin" => $value['nns_begin'],
                        "end" => $value['nns_end'],
                        "type" => $value['nns_type'],
                        "name" => $value['nns_name'],
                        "img" => $value['nns_image']
                    )
                );
            }
        }
        $seekpoint_xml = $this->make_seekpoint_import_xml($seekpoint_arr);
        //注入EPG的参数数组
        $data = array(
            "func" => "import_video_clip",
            "assets_id" => $epg_import_id,
            "assets_video_type" => 0,
            "assets_clip_id" => $epg_index_import_id,
            "assets_clip_info" => $import_xml,
            "asset_source_id" => $epg_import_arr['asset_source'],
            "bk_message_id" => $this->get_c2_info("message_id"),
            "bk_cp_id" => $this->str_source_id,
            "seekpoint" => $seekpoint_xml
        );
        $this->write_log_return("请求地址:".$this->arr_sp_config['epg_url']."请求参数:".var_export($data,true));
        //分发到两个epg
        $arr_epg_url = explode(',', $this->arr_sp_config['epg_url']);

        //CURL请求
        $curl_ret = $this->import($arr_epg_url[0],$data);

        if($curl_ret['ret'] != NS_CDS_SUCCE)
        {
            $epg_code = NS_CDS_EPG_FAIL;
            $this->write_log_return("注入EPG[" . $arr_epg_url[0] . "]失败返回：" . $curl_ret['reason'],NS_CDS_FAIL);
        }
        else
        {
            if (isset($arr_epg_url[1]) && strlen($arr_epg_url[1]) > 0)
            {
                //CURL请求
                $curl_ret = $this->import($arr_epg_url[1],$data);
                if ($curl_ret['ret'] != NS_CDS_SUCCE)
                {
                    $this->write_log_return("注入EPG[" . $arr_epg_url[1] . "]失败返回：" . $curl_ret['reason'],NS_CDS_FAIL);
                }
            }
            $this->write_log_return("注入EPG返回结果为：" . var_export($curl_ret['data_info'],true));
            $curl_ret = json_decode(json_encode(@simplexml_load_string($curl_ret['data_info'])),true);
            $this->write_log_return("注入EPG返回结果解析：" . var_export($curl_ret,true));
            if($curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_SUCCE && $curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_NOTFOUND)
            {
                $epg_code = NS_CDS_EPG_FAIL;
            }
            else
            {
                $epg_code = NS_CDS_EPG_SUCCE;
            }
        }
        //注入EPG日志
        $log_data = array (
            'nns_video_id' => $index_info['base_info']['nns_id'],
            'nns_video_type' => NS_CDS_INDEX,
            'nns_org_id' => $this->str_sp_id,
            'nns_create_time' => date('Y-m-d H:i:s'),
            'nns_action' => $action,
            'nns_status' => $curl_ret['@attributes']['ret'],
            'nns_reason' => addslashes($curl_ret['@attributes']['reason']),
            'nns_data' => $import_xml,
            'nns_name' => $this->get_c2_info('name')
        );
        //注入EPG日志入数据库
        $this->build_epg_log($log_data);
        //上报状态
        return $this->is_ok($epg_code,$curl_ret['@attributes']['reason']);
    }
    /**
     * 分集删除注入
     * @return array
     */
    public function index_destroy()
    {
        $video_info = $this->get_video_info();
        $index_info = $this->get_index_info();
        if (empty($index_info) || empty($video_info))
        {
            return $this->write_log_return("C2任务video|index元数据错误，执行注入EPG失败",NS_CDS_FAIL);
        }
        //获取注入EPG的分集注入ID
        $epg_index_import_id = $this->get_video_epg_import_id();
        if (empty($epg_index_import_id))
        {
            return $this->write_log_return("注入EPG时生成index注入ID失败",NS_CDS_FAIL);
        }
        //根据CP配置判断注入的CP是来源还是原始内容提供商
        if(isset($this->arr_source_config['original_cp_enabled']) && (int)$this->arr_source_config['original_cp_enabled'] === 1)
        {
            $asset_source = $video_info['base_info']['nns_producer'];
        }
        elseif(isset($this->arr_source_config['original_cp_enabled']) && (int)$this->arr_source_config['original_cp_enabled'] === 2)
        {
            $asset_source = 'fuse_xiaole';
        }
        else
        {
            $asset_source = $this->str_source_id;
        }
        //注入EPG的参数数组
        $data = array(
            "func" => "delete_video_clip",
            "assets_video_type" => 0,
            "assets_clip_id" => $epg_index_import_id,
            "bk_message_id" => $this->get_c2_info("message_id"),
            "bk_cp_id" => $this->str_source_id,
            "asset_source" => $asset_source
        );
        $this->write_log_return("请求地址:".$this->arr_sp_config['epg_url']."请求参数:".var_export($data,true));
        //分发到两个epg
        $arr_epg_url = explode(',', $this->arr_sp_config['epg_url']);

        //CURL请求
        $curl_ret = $this->import($arr_epg_url[0],$data);

        if($curl_ret['ret'] != NS_CDS_SUCCE)
        {
            $epg_code = NS_CDS_EPG_FAIL;
            $this->write_log_return("注入EPG[" . $arr_epg_url[0] . "]失败返回：" . $curl_ret['reason'],NS_CDS_FAIL);
        }
        else
        {
            if (isset($arr_epg_url[1]) && strlen($arr_epg_url[1]) > 0)
            {
                //CURL请求
                $curl_ret = $this->import($arr_epg_url[1],$data);
                if ($curl_ret['ret'] != NS_CDS_SUCCE)
                {
                    $this->write_log_return("注入EPG[" . $arr_epg_url[1] . "]失败返回：" . $curl_ret['reason'],NS_CDS_FAIL);
                }
            }
            $this->write_log_return("注入EPG返回结果为：" . var_export($curl_ret['data_info'],true));
            $curl_ret = json_decode(json_encode(@simplexml_load_string($curl_ret['data_info'])),true);
            $this->write_log_return("注入EPG返回结果解析：" . var_export($curl_ret,true));
            if($curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_SUCCE && $curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_NOTFOUND)
            {
                $epg_code = NS_CDS_EPG_FAIL;
            }
            else
            {
                $epg_code = NS_CDS_EPG_SUCCE;
            }
        }
        //注入EPG日志
        $log_data = array (
            'nns_video_id' => $index_info['base_info']['nns_id'],
            'nns_video_type' => NS_CDS_INDEX,
            'nns_org_id' => $this->str_sp_id,
            'nns_create_time' => date('Y-m-d H:i:s'),
            'nns_action' => $this->get_c2_info('action'),
            'nns_status' => $curl_ret['@attributes']['ret'],
            'nns_reason' => addslashes($curl_ret['@attributes']['reason']),
            'nns_data' => '',
            'nns_name' => $this->get_c2_info('name')
        );
        //注入EPG日志入数据库
        $this->build_epg_log($log_data);
        //上报状态
        return $this->is_ok($epg_code,$curl_ret['@attributes']['reason']);
    }
    /**
     * 片源注入
     * @return array
     */
    public function media()
    {
        $video_type = $this->get_c2_type();
        $video_info = $this->get_video_info();
        $index_info = $this->get_index_info();
        $media_info = $this->get_media_info();
        if (empty($video_info) || empty($index_info) || empty($media_info))
        {
            return $this->write_log_return("C2任务video|index|media元数据错误，执行注入EPG失败",NS_CDS_FAIL);
        }

        $epg_import_id = $this->get_video_epg_import_id(NS_CDS_VIDEO);

        //根据C2任务的行为动作进行分派
        if ($this->get_c2_info('action') === NS_CDS_DELETE || $index_info['base_info']['nns_deleted'] != 0)
        {
            //删除，如果上游已经删除，则直接取消当前任务 nns_deleted = 1  的时候
            return $this->media_destroy();
        }
        elseif ($this->get_c2_info('action') === NS_CDS_MODIFY)
        {
            $action = NS_CDS_MODIFY;
        }
        else
        {
            $action = NS_CDS_ADD;
        }

        $epg_index_import_id = $this->get_video_epg_import_id(NS_CDS_INDEX);
        $epg_media_import_id = $this->get_video_epg_import_id();
        if (empty($epg_index_import_id) || empty($epg_media_import_id))
        {
            return $this->write_log_return("注入EPG时生成video|index|media注入ID失败",NS_CDS_FAIL);
        }

        //EPG注入数据数组
        $epg_import_arr = array(
            "file_id" => $epg_media_import_id,
            "file_type" => $media_info['base_info']['nns_filetype'],
            "file_name" => $media_info['base_info']['nns_name'],
            "file_definition" => $media_info['base_info']['nns_mode'],
            "file_time_len" => $media_info['base_info']['nns_file_time_len'],
            "Tags" => $media_info['base_info']['nns_tag'],
            "file_size" => (int)$media_info['base_info']['nns_file_size'] > 0 ? (int)$media_info['base_info']['nns_file_size'] : 8081,
            "file_resolution" => $media_info['base_info']['nns_file_resolution'],
            "file_bit_rate" => $media_info['base_info']['nns_kbps'],
            "file_desc" => '',
            "ex_url" => $this->get_c2_info('ex_url'),
            "drm_flag" => '0',
            "drm_encrypt_solution" =>  '',
            "drm_ext_info" => '',
            "file_3d" => '0',
            "file_3d_type" => '',
            "original_id" => $media_info['base_info']['nns_import_id']
        );
        if(is_array($media_info['ex_info']) && !empty($media_info['ex_info']))
        {
            foreach ($media_info['ex_info'] as $key=>$value)
            {
                $epg_import_arr[$key] = $value;
            }
        }
        //根据CP配置判断注入的CP是来源还是原始内容提供商
        if(isset($this->arr_source_config['original_cp_enabled']) && (int)$this->arr_source_config['original_cp_enabled'] === 1)
        {
            $epg_import_arr['asset_source'] = $video_info['base_info']['nns_producer'];
        }
        elseif(isset($this->arr_source_config['original_cp_enabled']) && (int)$this->arr_source_config['original_cp_enabled'] === 2)
        {
            $epg_import_arr['asset_source'] = 'fuse_xiaole';
        }
        else
        {
            $epg_import_arr['asset_source'] = $this->str_source_id;
        }
        //根据SP配置确定下发的片源路径
        if((isset($this->arr_sp_config['media_cdn_model']) && $this->arr_sp_config['media_cdn_model'] == '1') || (isset($this->arr_sp_config['asset_import_cms_model']) && $this->arr_sp_config['asset_import_cms_model'] == '1'))
        {
            $epg_import_arr['file_path'] = $this->get_c2_info("file_path");
        }
        else
        {
            $epg_import_arr['file_path'] = $media_info['base_info']['nns_url'];
        }
        //根据SP配置确定下发的片源服务类型
        if(isset($this->arr_sp_config['media_service_type']) && strlen($this->arr_sp_config['media_service_type']) > 0)
        {
            $epg_import_arr['media_service_type'] = $this->arr_sp_config['media_service_type'];
        }
        else
        {
            $epg_import_arr['media_service_type'] = $media_info['base_info']['nns_media_service'];
        }
        //下发扩展地址或者播放URL
        if(strlen($this->get_c2_info('ex_url')) < 1 && $this->str_sp_id == 'cqyx')
        {
            $epg_import_arr['ex_url'] = $media_info['base_info']['nns_content_id'];
        }
        else
        {
            if(isset($media_info['base_info']['nns_ext_url']) && !empty($media_info['base_info']['nns_ext_url']))
            {
                $arr_ex_media_url = json_decode($media_info['base_info']['nns_ext_url'],true);
                if(isset($arr_ex_media_url['play_url']) && strlen($arr_ex_media_url['play_url']) > 0)
                {
                    $epg_import_arr['ex_url'] = $arr_ex_media_url['play_url'];
                }
            }
            else if(isset($this->arr_sp_config['asset_import_cms_model']) && $this->arr_sp_config['asset_import_cms_model'] == '2' && isset($this->arr_sp_config['c2_play_url_ip']) && strlen($this->arr_sp_config['c2_play_url_ip']) >0)
            {
                $str_c2_play_url_ip = trim(trim(trim($this->arr_sp_config['c2_play_url_ip'],'/'),'\\'));
                $str_file_path = trim(trim(trim($this->get_c2_info('file_path'),'/'),'\\'));
                $epg_import_arr['ex_url'] = $str_c2_play_url_ip.'/'.$str_file_path;
            }
        }
        //下发CORE平台ID
        if(isset($this->arr_sp_config['import_core_bind_id_enabled']) && (int)$this->arr_sp_config['import_core_bind_id_enabled'] === 1)
        {
            if(isset($media_info['base_info']['nns_content_id']) && strlen($media_info['base_info']['nns_content_id']) > 0)
            {
                $epg_import_arr['file_core_id'] = $media_info['base_info']['nns_content_id'];
            }
            else
            {
                $epg_import_arr['file_core_id'] = $media_info['base_info']['nns_import_id'];
            }
        }
        //下发CORE平台策略ID
        if(isset($this->arr_sp_config['media_cdn_model']) && $this->arr_sp_config['media_cdn_model'] == '1')
        {
            $epg_import_arr['policy'] = $this->str_source_id;
        }
        else
        {
            $epg_import_arr['policy'] = $this->get_c2_info("cdn_policy");
        }
        //下发DRM参数
        if(isset($this->arr_sp_config['main_drm_enabled']) && intval($this->arr_sp_config['main_drm_enabled']) == 1)
        {
            //开启DRM
            $epg_import_arr['drm_flag'] = $media_info['base_info']['nns_drm_enabled'];
            $epg_import_arr['drm_encrypt_solution'] =  $this->arr_sp_config['q_drm_identify'];
        }
        else if (isset($this->arr_source_config['main_drm_enabled']) && intval($this->arr_source_config['main_drm_enabled']) == 1)
        {
            //开启DRM
            $epg_import_arr['drm_flag'] = $media_info['base_info']['nns_drm_enabled'];
            $epg_import_arr['drm_encrypt_solution'] =  $media_info['base_info']['nns_drm_encrypt_solution'];
            $epg_import_arr['drm_ext_info'] = $media_info['base_info']['nns_drm_ext_info'];
        }
        //下发第三方来源ID
        if (isset($this->arr_source_config["clip_custom_origin_id"]) && !empty($this->arr_source_config["clip_custom_origin_id"]))
        {
            $epg_import_arr['cdn_ext_source_id'] = $this->arr_source_config["clip_custom_origin_id"] . '" ';
        }
        //下发片源模式
        if ($media_info['base_info']['nns_dimensions'] == '100000')
        {
            $epg_import_arr['file_3d'] = '1';
            $epg_import_arr['file_3d_type'] =  '3D';
        }
        elseif ($media_info['base_info']['nns_dimensions'] == '100001')
        {
            $epg_import_arr['file_3d'] = '1';
            $epg_import_arr['file_3d_type'] =  '0';
        }
        elseif ($media_info['base_info']['nns_dimensions'] == '100002')
        {
            $epg_import_arr['file_3d'] = '1';
            $epg_import_arr['file_3d_type'] =  '1';
        }
        elseif ($media_info['base_info']['nns_dimensions'] == '100003')
        {
            $epg_import_arr['file_3d'] = '1';
            $epg_import_arr['file_3d_type'] =  '2';
        }

        //拼装注入EPG的XML
        $import_xml = $this->make_import_xml(array($video_type => $epg_import_arr));
        if(empty($import_xml))
        {
            return $this->write_log_return('注入EPG的XML拼装失败',NS_CDS_FAIL);
        }
        //注入EPG的参数数组
        $data = array(
            "func" => "import_video_file",
            "assets_id" => $epg_import_id,
            "assets_video_type" => 0,
            "assets_clip_id" => $epg_index_import_id,
            "assets_file_id" => $epg_media_import_id,
            "assets_file_info" => $import_xml,
            "asset_source_id" => $epg_import_arr['asset_source'],
            "bk_message_id" => $this->get_c2_info("message_id"),
            "bk_cp_id" => $this->str_source_id,
            "policy" => $epg_import_arr['policy'],
            "assets_core_org_id" => (isset($this->arr_sp_config['media_cdn_model']) && $this->arr_sp_config['media_cdn_model'] == '1') ? 1 : 0,
        );
        $this->write_log_return("请求地址:".$this->arr_sp_config['epg_url']."请求参数:".var_export($data,true));
        //分发到两个epg
        $arr_epg_url = explode(',', $this->arr_sp_config['epg_url']);

        //CURL请求
        $curl_ret = $this->import($arr_epg_url[0],$data);

        if($curl_ret['ret'] != NS_CDS_SUCCE)
        {
            $epg_code = NS_CDS_EPG_FAIL;
            $this->write_log_return("注入EPG[" . $arr_epg_url[0] . "]失败返回：" . $curl_ret['reason'],NS_CDS_FAIL);
        }
        else
        {
            if (isset($arr_epg_url[1]) && strlen($arr_epg_url[1]) > 0)
            {
                //CURL请求
                $curl_ret = $this->import($arr_epg_url[1],$data);
                if ($curl_ret['ret'] != NS_CDS_SUCCE)
                {
                    $this->write_log_return("注入EPG[" . $arr_epg_url[1] . "]失败返回：" . $curl_ret['reason'],NS_CDS_FAIL);
                }
            }
            $this->write_log_return("注入EPG返回结果为：" . var_export($curl_ret['data_info'],true));
            $curl_ret = json_decode(json_encode(@simplexml_load_string($curl_ret['data_info'])),true);
            $this->write_log_return("注入EPG返回结果解析：" . var_export($curl_ret,true));
            if($curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_SUCCE && $curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_NOTFOUND)
            {
                $epg_code = NS_CDS_EPG_FAIL;
            }
            else
            {
                $epg_code = NS_CDS_EPG_SUCCE;
            }
        }
        //注入EPG日志
        $log_data = array (
            'nns_video_id' => $media_info['base_info']['nns_id'],
            'nns_video_type' => NS_CDS_MEDIA,
            'nns_org_id' => $this->str_sp_id,
            'nns_create_time' => date('Y-m-d H:i:s'),
            'nns_action' => $action,
            'nns_status' => $curl_ret['@attributes']['ret'],
            'nns_reason' => addslashes($curl_ret['@attributes']['reason']),
            'nns_data' => $import_xml,
            'nns_name' => $this->get_c2_info('name')
        );
        //注入EPG日志入数据库
        $this->build_epg_log($log_data);
        //上报状态
        return $this->is_ok($epg_code,$curl_ret['@attributes']['reason']);
    }
    /**
     * 片源删除注入
     * @return array
     */
    public function media_destroy()
    {
        $video_info = $this->get_video_info();
        $index_info = $this->get_index_info();
        $media_info = $this->get_media_info();
        if (empty($video_info) || empty($index_info) || empty($media_info))
        {
            return $this->write_log_return("C2任务video|index|media元数据错误，执行注入EPG失败",NS_CDS_FAIL);
        }
        //获取注入EPG的片源注入ID
        $epg_media_import_id = $this->get_video_epg_import_id();
        if (empty($epg_media_import_id))
        {
            return $this->write_log_return("注入EPG时生成media注入ID失败",NS_CDS_FAIL);
        }
        //根据CP配置判断注入的CP是来源还是原始内容提供商
        if(isset($this->arr_source_config['original_cp_enabled']) && (int)$this->arr_source_config['original_cp_enabled'] === 1)
        {
            $asset_source = $video_info['base_info']['nns_producer'];
        }
        elseif(isset($this->arr_source_config['original_cp_enabled']) && (int)$this->arr_source_config['original_cp_enabled'] === 2)
        {
            $asset_source = 'fuse_xiaole';
        }
        else
        {
            $asset_source = $this->str_source_id;
        }
        //注入EPG的参数数组
        $data = array(
            "func" => "delete_video_file",
            "assets_video_type" => 0,
            "assets_file_id" => $epg_media_import_id,
            "bk_message_id" => $this->get_c2_info("message_id"),
            "bk_cp_id" => $this->str_source_id,
            "asset_source" => $asset_source
        );
        $this->write_log_return("请求地址:".$this->arr_sp_config['epg_url']."请求参数:".var_export($data,true));
        //分发到两个epg
        $arr_epg_url = explode(',', $this->arr_sp_config['epg_url']);

        //CURL请求
        $curl_ret = $this->import($arr_epg_url[0],$data);

        if($curl_ret['ret'] != NS_CDS_SUCCE)
        {
            $epg_code = NS_CDS_EPG_FAIL;
            $this->write_log_return("注入EPG[" . $arr_epg_url[0] . "]失败返回：" . $curl_ret['reason'],NS_CDS_FAIL);
        }
        else
        {
            if (isset($arr_epg_url[1]) && strlen($arr_epg_url[1]) > 0)
            {
                //CURL请求
                $curl_ret = $this->import($arr_epg_url[1],$data);
                if ($curl_ret['ret'] != NS_CDS_SUCCE)
                {
                    $this->write_log_return("注入EPG[" . $arr_epg_url[1] . "]失败返回：" . $curl_ret['reason'],NS_CDS_FAIL);
                }
            }
            $this->write_log_return("注入EPG返回结果为：" . var_export($curl_ret['data_info'],true));
            $curl_ret = json_decode(json_encode(@simplexml_load_string($curl_ret['data_info'])),true);
            $this->write_log_return("注入EPG返回结果解析：" . var_export($curl_ret,true));
            if($curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_SUCCE && $curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_NOTFOUND)
            {
                $epg_code = NS_CDS_EPG_FAIL;
            }
            else
            {
                $epg_code = NS_CDS_EPG_SUCCE;
            }
        }
        //注入EPG日志
        $log_data = array (
            'nns_video_id' => $index_info['base_info']['nns_id'],
            'nns_video_type' => NS_CDS_INDEX,
            'nns_org_id' => $this->str_sp_id,
            'nns_create_time' => date('Y-m-d H:i:s'),
            'nns_action' => $this->get_c2_info('action'),
            'nns_status' => $curl_ret['@attributes']['ret'],
            'nns_reason' => addslashes($curl_ret['@attributes']['reason']),
            'nns_data' => '',
            'nns_name' => $this->get_c2_info('name')
        );
        //注入EPG日志入数据库
        $this->build_epg_log($log_data);
        //上报状态
        return $this->is_ok($epg_code,$curl_ret['@attributes']['reason']);
    }
    /**
     * 直播频道注入、删除
     * @return array
     */
    public function live()
    {
        $live_info = $this->get_live_info();
        $video_type= $this->get_c2_type();
        if (empty($live_info))
        {
            return $this->write_log_return("C2任务live元数据错误，执行注入EPG失败",NS_CDS_FAIL);
        }
        //根据C2任务的行为动作进行分派
        if ($this->get_c2_info('action') === NS_CDS_DELETE )
        {
            $action = NS_CDS_DELETE;
        }
        elseif ($this->get_c2_info('action') === NS_CDS_MODIFY)
        {
            $action = NS_CDS_MODIFY;
        }
        else
        {
            $action = NS_CDS_ADD;
        }
        $epg_live_import_id = $this->get_video_epg_import_id(NS_CDS_LIVE);
        if (empty($epg_live_import_id))
        {
            return $this->write_log_return("注入EPG时生成live注入ID失败",NS_CDS_FAIL);
        }
        $live_info['base_info']['nns_live_import_id'] = $epg_live_import_id;
        $result_depot = \nl_depot::get_depot_info($this->get_dc(),array('nns_id'=>$live_info['nns_depot_id']),'live');
        //获取栏目信息
        if($result_depot['ret'] !=0)
        {
            return $this->write_log_return("获取频道栏目信息失败",NS_CDS_FAIL);
        }
        $arr_depot = public_model_exec::get_category_info($result_depot['data_info'][0]['nns_category']);
        $live_info['base_info']['nns_category_name'] = (isset($arr_depot['data_info'][$result_depot['nns_category_id']]['name']) && strlen($arr_depot['data_info'][$result_depot['nns_category_id']]['name']) > 0) ? $arr_depot['data_info'][$result_depot['nns_category_id']]['name'] : '卫视台';

        //拼装注入EPG的XML
        $import_xml = $this->make_import_cms_xml('live',$live_info['base_info'],null,null,$live_info['ex_info']);
        if(strlen($import_xml) <1)
        {
            return $this->write_log_return('注入EPG的XML拼装失败',NS_CDS_FAIL);
        }
        //注入EPG的参数数组
        $data = array(
            "func" => "modify_live",
            "assets_content"=>$import_xml,
        );
        $this->write_log_return("请求地址:".$this->arr_sp_config['epg_url']."请求参数:".var_export($data,true));
        //CURL请求
        $curl_ret = $this->import($this->arr_sp_config['epg_url'],$data);
        if($curl_ret['ret'] != NS_CDS_SUCCE)
        {
            $epg_code = NS_CDS_EPG_FAIL;
            $this->write_log_return("注入EPG失败返回：" . $curl_ret['reason'],NS_CDS_FAIL);
        }
        else
        {
            $this->write_log_return("注入EPG返回结果为：" . var_export($curl_ret['data_info'],true));
            $curl_ret = json_decode(json_encode(@simplexml_load_string($curl_ret['data_info'])),true);
            $this->write_log_return("注入EPG返回结果解析：" . var_export($curl_ret,true));
            if($curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_SUCCE && $curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_NOTFOUND)
            {
                $epg_code = NS_CDS_EPG_FAIL;
            }
            else
            {
                $epg_code = NS_CDS_EPG_SUCCE;
            }
        }
        //注入EPG日志
        $log_data = array (
            'nns_video_id' => $live_info['base_info']['nns_id'],
            'nns_video_type' => NS_CDS_LIVE,
            'nns_org_id' => $this->str_sp_id,
            'nns_create_time' => date('Y-m-d H:i:s'),
            'nns_action' => $action,
            'nns_status' => $curl_ret['@attributes']['ret'],
            'nns_reason' => addslashes($curl_ret['@attributes']['reason']),
            'nns_data' => $import_xml,
            'nns_name' => $this->get_c2_info('name')
        );
        //注入EPG日志入数据库
        $this->build_epg_log($log_data);
        //上报状态
        return $this->is_ok($epg_code,$curl_ret['@attributes']['reason']);
    }
    /**
     * 直播源注入、删除
     * @return array
     */
    public function live_media()
    {
        $live_info = $this->get_live_info();
        $live_index_info = $this->get_live_index_info();
        $live_media_info = $this->get_live_media_info();
        $video_type= $this->get_c2_type();
        if (empty($live_media_info)|| empty($live_index_info) || empty($live_info))
        {
            return $this->write_log_return("C2任务live|live_index|live_media元数据错误，执行注入EPG失败",NS_CDS_FAIL);
        }
        //根据C2任务的行为动作进行分派
        if ($this->get_c2_info('action') === NS_CDS_DELETE )
        {
            //删除，如果上游已经删除，则直接取消当前任务 nns_deleted = 1  的时候
            $action = NS_CDS_DELETE;
        }
        elseif ($this->get_c2_info('action') === NS_CDS_MODIFY)
        {
            $action = NS_CDS_MODIFY;
        }
        else
        {
            $action = NS_CDS_ADD;
        }
        $epg_live_import_id = $this->get_video_epg_import_id(NS_CDS_LIVE);
        $epg_live_index_import_id = $this->get_video_epg_import_id(NS_CDS_LIVE_INDEX);
        $epg_live_media_import_id = $this->get_video_epg_import_id(NS_CDS_LIVE_MEDIA);
        if (empty($epg_live_import_id) || empty($epg_live_index_import_id) || empty($epg_live_media_import_id))
        {
            return $this->write_log_return("live|live_index|live_media注入ID失败",NS_CDS_FAIL);
        }
        $live_info['base_info']['nns_live_import_id']=$epg_live_import_id;
        $live_index_info['base_info']['nns_live_index_import_id']=$epg_live_index_import_id;
        $live_media_info['base_info']['nns_live_media_import_id']=$epg_live_media_import_id;
        $result_depot = \nl_depot::get_depot_info($this->get_dc(),array('nns_id'=>$live_info['nns_depot_id']),'live');
        //获取栏目信息
        if($result_depot['ret'] !=0)
        {
            return $this->write_log_return("获取频道栏目信息失败",NS_CDS_FAIL);
        }
        $arr_depot = public_model_exec::get_category_info($result_depot['data_info'][0]['nns_category']);
        $live_info['base_info']['nns_category_name'] = (isset($arr_depot['data_info'][$result_live['nns_category_id']]['name']) && strlen($arr_depot['data_info'][$live_info['base_info']['nns_category_id']]['name']) > 0) ? $arr_depot['data_info'][$live_info['base_info']['nns_category_id']]['name'] : '卫视台';
        //拼装注入EPG的XML
        $import_xml = $this->make_import_cms_xml('live',$live_info['base_info'],$live_index_info['base_info'],$live_media_info['base_info'],$live_info['ex_info']);
        if(strlen($import_xml) <1)
        {
            return $this->write_log_return('注入EPG的XML拼装失败',NS_CDS_FAIL);
        }
        //注入EPG的参数数组
        $data = array(
            "func" => "modify_live",
            "assets_content"=>$import_xml,
        );
        $this->write_log_return("请求地址:".$this->arr_sp_config['epg_url']."请求参数:".var_export($data,true));
        //CURL请求
        $curl_ret = $this->import($this->arr_sp_config['epg_url'],$data);
        if($curl_ret['ret'] != NS_CDS_SUCCE)
        {
            $epg_code = NS_CDS_EPG_FAIL;
            $this->write_log_return("注入EPG失败返回：" . $curl_ret['reason'],NS_CDS_FAIL);
        }
        else
        {
            $this->write_log_return("注入EPG返回结果为：" . var_export($curl_ret['data_info'],true));
            $curl_ret = json_decode(json_encode(@simplexml_load_string($curl_ret['data_info'])),true);
            $this->write_log_return("注入EPG返回结果解析：" . var_export($curl_ret,true));
            if($curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_SUCCE && $curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_NOTFOUND)
            {
                $epg_code = NS_CDS_EPG_FAIL;
            }
            else
            {
                $epg_code = NS_CDS_EPG_SUCCE;
            }
        }
        //注入EPG日志
        $log_data = array (
            'nns_video_id' => $live_media_info['base_info']['nns_id'],
            'nns_video_type' => NS_CDS_LIVE_MEDIA,
            'nns_org_id' => $this->str_sp_id,
            'nns_create_time' => date('Y-m-d H:i:s'),
            'nns_action' => $action,
            'nns_status' => $curl_ret['@attributes']['ret'],
            'nns_reason' => addslashes($curl_ret['@attributes']['reason']),
            'nns_data' => $import_xml,
            'nns_name' => $this->get_c2_info('name')
        );
        //注入EPG日志入数据库
        $this->build_epg_log($log_data);
        //上报状态
        return $this->is_ok($epg_code,$curl_ret['@attributes']['reason']);
    }
    /**
     * 节目单注入、删除
     * @return array
     */
    public function playbill()
    {
        $playbill_info = $this->get_playbill_info();
        $live_media_info=$this->get_live_media_info();
        $live_info=$this->get_live_info();
        $video_type= $this->get_c2_type();
        if (empty($playbill_info)||empty($live_media_info)||empty($live_info))
        {
            return $this->write_log_return("C2任务playbill|live_media|live元数据错误，执行注入EPG失败",NS_CDS_FAIL);
        }
        //根据C2任务的行为动作进行分派
        if ($this->get_c2_info('action') === NS_CDS_DELETE )
        {
            $action = NS_CDS_DELETE;
        }
        elseif ($this->get_c2_info('action') === NS_CDS_MODIFY)
        {
            $action = NS_CDS_MODIFY;
        }
        else
        {
            $action = NS_CDS_ADD;
        }
        $epg_playbill_import_id = $this->get_video_epg_import_id(NS_CDS_PLAYBILL);
        $epg_live_media_import_id = $this->get_video_epg_import_id(NS_CDS_LIVE_MEDIA);
        $epg_live_import_id = $this->get_video_epg_import_id(NS_CDS_LIVE);
        if (empty($epg_playbill_import_id)||empty($epg_live_media_import_id)||empty($epg_live_import_id))
        {
            return $this->write_log_return("注入EPG时生成playbill|live_media|live注入ID失败",NS_CDS_FAIL);
        }
        $playbill_info['base_info']['nns_playbill_import_id'] = $epg_playbill_import_id;
        $live_media_info['base_info']['nns_live_media_import_id'] = $epg_live_media_import_id;
        $live_media_info['base_info']['nns_live_import_id'] = $epg_live_import_id;

        $playbill_info['base_info']['nns_flag'] = $this->arr_source_config['live_playbill_cms_model'] == '1' ? 'live' : 'live_media';

        //拼装注入EPG的XML
        $import_xml = $this->make_import_cms_xml('playbill',$playbill_info['base_info']);
        if(strlen($import_xml) <1)
        {
            return $this->write_log_return('注入EPG的XML拼装失败',NS_CDS_FAIL);
        }
        //注入EPG的参数数组
        $data = array(
            "func" => "modify_playbill",
            "assets_content"=>$import_xml,
        );
        $this->write_log_return("请求地址:".$this->arr_sp_config['epg_url']."请求参数:".var_export($data,true));
        //CURL请求
        $curl_ret = $this->import($this->arr_sp_config['epg_url'],$data);
        if($curl_ret['ret'] != NS_CDS_SUCCE)
        {
            $epg_code = NS_CDS_EPG_FAIL;
            $this->write_log_return("注入EPG失败返回：" . $curl_ret['reason'],NS_CDS_FAIL);
        }
        else
        {
            $this->write_log_return("注入EPG返回结果为：" . var_export($curl_ret['data_info'],true));
            $curl_ret = json_decode(json_encode(@simplexml_load_string($curl_ret['data_info'])),true);
            $this->write_log_return("注入EPG返回结果解析：" . var_export($curl_ret,true));
            if($curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_SUCCE && $curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_NOTFOUND)
            {
                $epg_code = NS_CDS_EPG_FAIL;
            }
            else
            {
                $epg_code = NS_CDS_EPG_SUCCE;
            }
        }
        //注入EPG日志
        $log_data = array (
            'nns_video_id' => $playbill_info['base_info']['nns_id'],
            'nns_video_type' => NS_CDS_PLAYBILL,
            'nns_org_id' => $this->str_sp_id,
            'nns_create_time' => date('Y-m-d H:i:s'),
            'nns_action' => $action,
            'nns_status' => $curl_ret['@attributes']['ret'],
            'nns_reason' => addslashes($curl_ret['@attributes']['reason']),
            'nns_data' => $import_xml,
            'nns_name' => $this->get_c2_info('name')
        );
        //注入EPG日志入数据库
        $this->build_epg_log($log_data);
        //上报状态
        return $this->is_ok($epg_code,$curl_ret['@attributes']['reason']);
    }
    /**
     * 分集打点信息注入
     * @return array
     */
    public function seekpoint()
    {
        $seekpoint_info=$this->get_seekpoint_info();
        $index_info=$this->get_index_info();
        if (empty($seekpoint_info)||empty($index_info))
        {
            return $this->write_log_return("C2任务seekpoint|index元数据错误，执行注入EPG失败",NS_CDS_FAIL);
        }
        //组装打点信息xml
        $str_temp='';
        foreach ($seekpoint_info['base_info'] as $value)
        {
            $str_temp.='<item begin="'.$value['nns_begin'] .'" end="' . $value['nns_end'] . '" type="'.$value['nns_type'].'" name="'.$value['nns_name'].'" img="'.$value['nns_image'].'" ></item>';
        }
        $import_xml=empty($str_temp) ? null : '<metadata>' . $str_temp . '</metadata>';
        //注入EPG的参数数组
        $data = array(
            "func" => "import_seekpoint_data",
            "assets_content"=>$import_xml,
            "assets_clip_id"=>$index_info['base_info']['nns_id']
        );
        $this->write_log_return("请求地址:".$this->arr_sp_config['epg_url']."请求参数:".var_export($data,true));
        //CURL请求
        $curl_ret = $this->import($this->arr_sp_config['epg_url'],$data);
        if($curl_ret['ret'] != NS_CDS_SUCCE)
        {
            $epg_code = NS_CDS_EPG_FAIL;
            $this->write_log_return("注入EPG失败返回：" . $curl_ret['reason'],NS_CDS_FAIL);
        }
        else
        {
            $this->write_log_return("注入EPG返回结果为：" . var_export($curl_ret['data_info'],true));
            $curl_ret = json_decode(json_encode(@simplexml_load_string($curl_ret['data_info'])),true);
            $this->write_log_return("注入EPG返回结果解析：" . var_export($curl_ret,true));
            if($curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_SUCCE && $curl_ret['@attributes']['ret'] != NS_CDS_EPG_NOTIFY_NOTFOUND)
            {
                $epg_code = NS_CDS_EPG_FAIL;
            }
            else
            {
                $epg_code = NS_CDS_EPG_SUCCE;
            }
        }
        //注入EPG日志
        $log_data = array (
            'nns_video_id' => $seekpoint_info['base_info']['nns_id'],
            'nns_video_type' => NS_CDS_PLAYBILL,
            'nns_org_id' => $this->str_sp_id,
            'nns_create_time' => date('Y-m-d H:i:s'),
            'nns_action' => NS_CDS_ADD,
            'nns_status' => $curl_ret['@attributes']['ret'],
            'nns_reason' => addslashes($curl_ret['@attributes']['reason']),
            'nns_data' => $import_xml,
            'nns_name' => $this->get_c2_info('name')
        );
        //注入EPG日志入数据库
        $this->build_epg_log($log_data);
        //上报状态
        return $this->is_ok($epg_code,$curl_ret['@attributes']['reason']);
    }
}