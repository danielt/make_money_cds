<?php
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_core.encrypt.m_aiqiyi_encrypt");
\ns_core\m_load::load_old("nn_logic/project_group/project_key.class.php");
class delivery_import
{
    /**
     * 初始化
     */
    public function __construct()
    {
        //初始化obj DC
        m_config::get_dc();
    }
    
    public function init()
    {
        $result_config = nl_project_key::query_by_key(m_config::get_dc(), "coocaa_info");
        if($result_config['ret'] !=0)
        {
            return $result_config;
        }
        if(!isset($result_config['data_info']['nns_key_value']) || !m_config::is_json($result_config['data_info']['nns_key_value']))
        {
            return m_config::return_data(1,"coocaa_info 配置为空或者非法");
        }
        $result_config = json_decode($result_config['data_info']['nns_key_value'],true);
        if(!isset($result_config['coocaa_url']) || strlen($result_config['coocaa_url']) <1)
        {
            return m_config::return_data(1,"coocaa_info 配置coocaa_url 为空或者未配置，无法获取酷开分类列表");
        }
        if(!isset($result_config['coocaa_token']) || strlen($result_config['coocaa_token']) <1)
        {
            return m_config::return_data(1,"coocaa_info 配置coocaa_token 为空或者未配置，无法获取酷开分类列表");
        }
        $token = $result_config['coocaa_token'];
        //if(!isset($result_config['modId']) || strlen($result_config['modId']) <1)
        //{
        //    return m_config::return_data(1,"aiqiyi_info 配置modId 为空或者未配置");
        //}
        //if(!isset($result_config['partnerId']) || strlen($result_config['partnerId']) <1)
        //{
        //    return m_config::return_data(1,"aiqiyi_info 配置partnerId 为空或者未配置");
        //}
        //$modId = $result_config['modId'];
        //$partnerId = $result_config['partnerId'];
        //$token = m_aiqiyi_encrypt::encrypt($modId, $partnerId);
        $coocaa_url = trim(rtrim(rtrim($result_config['coocaa_url'],'/'),'\\'));

        $coocaa_url.="/category_list?token={$token}";
        $result = m_config::get_curl_content($coocaa_url,60);
        if($result['ret'] !=0)
        {
            return $result;
        }
        if(!isset($result['data_info']) || !m_config::is_json($result['data_info']))
        {
            return m_config::return_data(1,"URL:{$coocaa_url} 获取数据为空或者数据非json");
        }
        $data_info = json_decode($result['data_info'],true);
        if(!isset($data_info['code']) || $data_info['code'] !='C00000')
        {
            return m_config::return_data(1,"URL:{$coocaa_url}，数据错误，状态码：{$data_info['code']}");
        }
        if(!isset($data_info['data']) || !is_array($data_info['data']))
        {
            return m_config::return_data(0,"URL:{$coocaa_url}，数据为空");
        }
        $str_exe = evn::get("php_execute_path") . ' -f ' . dirname(__FILE__) . '/delivery_import.php ';
        foreach ($data_info['data'] as $data)
        {
            if (substr(php_uname(), 0, 7) != "Windows")
            {
                //LINUX下执行
                $str_order = "{$str_exe}  {$data['id']} {$token} > /dev/null &";
            }
            else
            {
                //windows下执行
                $str_order = "{$str_exe}  {$data['id']} {$token} ";
            }
            usleep(50000);
            exec($str_order);
        }
        return m_config::return_data(0,"OK");
    }
    
    public function init_v2($id,$token)
    {
        $result_config = nl_project_key::query_by_key(m_config::get_dc(), "coocaa_info");
        if($result_config['ret'] !=0)
        {
            return $result_config;
        }
        if(!isset($result_config['data_info']['nns_key_value']) || !m_config::is_json($result_config['data_info']['nns_key_value']))
        {
            return m_config::return_data(1,"coocaa_info 配置为空或者非法");
        }
        $result_config = json_decode($result_config['data_info']['nns_key_value'],true);
        if(!isset($result_config['coocaa_url']) || strlen($result_config['coocaa_url']) <1)
        {
            return m_config::return_data(1,"coocaa_info 配置coocaa_url 为空或者未配置");
        }
        //if(!isset($result_config['modId']) || strlen($result_config['modId']) <1)
        //{
        //    return m_config::return_data(1,"aiqiyi_info 配置modId 为空或者未配置");
        //}
        //if(!isset($result_config['partnerId']) || strlen($result_config['partnerId']) <1)
        //{
        //    return m_config::return_data(1,"aiqiyi_info 配置partnerId 为空或者未配置");
        //}
        //$modId = $result_config['modId'];
        //$partnerId = $result_config['partnerId'];
        //$token = m_aiqiyi_encrypt::encrypt($modId, $partnerId);
        $coocaa_url = trim(rtrim(rtrim($result_config['coocaa_url'],'/'),'\\'));
        $coocaa_url.="/full?token={$token}&chnId={$id}";
        $result = m_config::get_curl_content($coocaa_url,60);
        //if($result['ret'] !=0)
        //{
        //    return $result;
        //}
        //if(!isset($result['data_info']) || strlen($result['data_info'])<1)
        //{
        //    return m_config::return_data(1,"URL:{$coocaa_url} 获取数据为空或者数据非json");
        //}
        //$again_url = explode('"', $result['data_info']);
        //$result = m_config::get_curl_content($again_url[1],60);
        if($result['ret'] !=0)
        {
            return $result;
        }
        if(!isset($result['data_info']) || !m_config::is_json($result['data_info']))
        {
            return m_config::return_data(1,"URL:{$coocaa_url} 获取数据为空或者数据非json");
        }
        $data_info = json_decode($result['data_info'],true);
        if(!isset($data_info['code']) || $data_info['code'] !='C00000')
        {
            return m_config::return_data(1,"URL:{$result['data_info']}，数据错误，状态码：{$data_info['code']}");
        }
        if(!isset($data_info['data']) || !is_array($data_info['data']) || empty($data_info['data']))
        {
            return m_config::return_data(0,"URL:{$result['data_info']}，数据为空");
        }
        //foreach ($data_info['data'] as $key=>$value)
        //{
        //    if(substr($value, -2) != '01')
        //    {
        //        unset($data_info['data'][$key]);
        //    }
        //}
        //if(!isset($data_info['data']) || !is_array($data_info['data']) || empty($data_info['data']))
        //{
        //    return m_config::return_data(0,"URL:{$result['data_info']}，数据为空");
        //}
        //这儿是获取了全量专辑ID，等于说获取到了全量媒资ID
        $data = array_chunk($data_info['data'], 15);
        $result_mkdir = m_config::make_dir("temp/coocaa/{$id}");
        if($result_mkdir['ret'] !=0)
        {
            return $result_mkdir;
        }
        $absolute_dir = trim(rtrim(rtrim($result_mkdir['data_info']['absolute_dir'],'/'),'\\'));
        foreach ($data as $key=>$value)
        {
            if(!is_array($value) || empty($value))
            {
                continue;
            }
            file_put_contents($absolute_dir."/".np_guid_rand().".txt", json_encode($value));
        }
        return m_config::return_data(0,'ok');
    }
}
if(isset($_SERVER['argv'][1]) && !empty($_SERVER['argv'][1]))
{
    $delivery_import = new delivery_import();
    $result = $delivery_import->init_v2($_SERVER['argv'][1],$_SERVER['argv'][2]);
}
else
{
    $delivery_import = new delivery_import();
    $result = $delivery_import->init();
}
echo json_encode($result);