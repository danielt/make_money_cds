<?php
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.message.message_queue");
\ns_core\m_load::load_old('/nn_logic/message/nl_message_temp.class.php');
define('CP_ID','JSCN');
//soap注入公共入口
class ftp_import extends \ns_model\message\message_queue
{
    public $int_limit = 100;
    /**
     *  参数
     * @var unknown
     */
	public $mixed_params = null;
	
	public $ContentMngXMLURL = null;


	/**
	 * 注入消息队列模板
	 * @var unknown
	 */
	public $arr_in_message = array(
          'base_info'=>array(
                  'nns_message_time'=>'', //上游消息时间  年月日时分秒毫秒
                  'nns_message_id'=>'',  //上游消息ID
                  'nns_cp_id'=>'', //上游CP标示
                  'nns_message_xml'=>'',  //上游原始的信息  可以为字符串 | xml | json
                  'nns_message_content'=>'',  //上游解析后的信息  可以为字符串 | xml | json  生成队列的基本信息
                  'nns_action'=>'', //操作 行为
                  'nns_type'=>'', //消息 类型
                  'nns_name'=>'',  //消息名称
                  'nns_package_id'=>'',  //包ID
                  'nns_xmlurlqc'=>'', //广州电信悦ME MD5摘要
                  'nns_encrypt'=>'', //广州电信悦ME 加密串
                  'nns_content_number'=>'', //xml文件中包含的内容数量
          ), //基本信息（存储于nns_mgtvbk_message表中）
    );
	
	/**
	 * 初始化
	 */
	public function __construct()
	{
	    \ns_core\m_load::load_np("np_xml_to_array.class.php");
	    m_config::get_dc();
	    m_config::base_write_log('message','request',"-----------消息接收开始-----------",m_config::return_child_path(__FILE__));
	}
    /**
     * 执行入口
     */
    public function init()
    {
        $result_message_temp = nl_message_temp::query_message_table_is_exsist(m_config::get_dc(), CP_ID,$this->int_limit);
        if($result_message_temp['ret'] !=0 || !isset($result_message_temp['data_info']) || empty($result_message_temp['data_info']) ||!is_array($result_message_temp['data_info']))
        {
            return $result_message_temp;
        }
        foreach ($result_message_temp['data_info'] as $value)
        {
            $result_import = $this->import($value['nns_id'],$value['nns_url']);
            if($result_import['Result'] !=0)
            {
                return $result_import;
            }
        }
        return $result_import;
    }
	/**
	 * 注入
	 * @return multitype:number |string
	 */
	public function import($message_id,$ContentMngXMLURL)
	{
        $this->setFuse_enabled(false);
	    $this->arr_in_message['base_info']['nns_message_xml'] = $ContentMngXMLURL;
		$this->arr_in_message['base_info']['nns_message_id'] = $message_id;
		$this->arr_in_message['base_info']['nns_message_time'] = date("YmdHis");
    	$this->arr_in_message['base_info']['nns_cp_id'] = 'JSCN';
    	if(strlen($ContentMngXMLURL) <1)
    	{
		    return $this->exec(m_config::return_data(1,'nns_message_content url为空，不执行'));
    	}
    	$result_content = m_config::get_curl_content($ContentMngXMLURL);
    	if($result_content['ret'] !=0)
    	{
    	    $this->arr_in_message['base_info']['nns_message_state'] = '1';
    	    $this->arr_in_message['base_info']['nns_message_content'] = $result_content['reason'];
    	    $this->message_action('add',$this->arr_in_message);
    	    return $this->exec($result_content);
    	}
    	if(!isset($result_content['data_info']) || strlen($result_content['data_info']) <1)
    	{
    	    $this->arr_in_message['base_info']['nns_message_state'] = '1';
    	    $this->arr_in_message['base_info']['nns_message_content'] = "获取内容为空地址:[{$ContentMngXMLURL}]";
    	    $result = $this->message_action('add',$this->arr_in_message);
    	    return $this->exec(m_config::return_data(1,"内容为空：获取内容为空地址:[{$ContentMngXMLURL}]"));
    	}
    	$nns_message_content = $result_content['data_info'];
    	$nns_message_content = trim($nns_message_content);
    	if(strlen($nns_message_content) <1)
    	{
    	    $this->arr_in_message['base_info']['nns_message_state'] = '1';
    	    $this->arr_in_message['base_info']['nns_message_content'] = "获取内容为空地址:[{$ContentMngXMLURL}]";
    	    $result = $this->message_action('add',$this->arr_in_message);
    	    return $this->exec(m_config::return_data(1,"内容为空：获取内容为空地址:[{$ContentMngXMLURL}]"));
    	}
    	if(!m_config::is_xml($nns_message_content))
    	{
    	    $this->arr_in_message['base_info']['nns_message_state'] = '2';
    	    $this->arr_in_message['base_info']['nns_message_content'] = "非xml内容:[{$nns_message_content}]";
        	$this->arr_in_message['base_info']['nns_message_content'] = $nns_message_content;
            $this->push($this->arr_in_message);
    	    return $this->exec(m_config::return_data(1,'is not xml content'));
    	}
    	#TODO  这儿依据不同内容做解析
    	$this->arr_in_message['base_info']['nns_message_content'] = $nns_message_content;
        $result = $this->push($this->arr_in_message);
	    return $this->exec($result);
	}
	/**
	 * 
	 * @param unknown $result
	 * @param unknown $params
	 */
	public function exec($result)
	{
	    m_config::base_write_log('message','request',"注入结果：".var_export($result,true),m_config::return_child_path(__FILE__));
    	return array (
	            'Result' => $result['ret'] == '0' ? 0 : -1,
	            'ErrorDescription' => $result['reason']
	        );
	}
	/**
	 * 类销毁
	 */
	public function __destruct()
	{
	    m_config::base_write_log('message','request',"-----------消息接收结束-----------",m_config::return_child_path(__FILE__));
	}
}
$ftp_import = new ftp_import();
$result = $ftp_import->init();
echo json_encode($result);die;
