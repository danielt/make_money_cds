<?php
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_core.m_file");
\ns_core\m_load::load_old('/nn_logic/message/nl_message_temp.class.php');
define('CP_ID','JSCN');
//soap注入公共入口
class delivery_import
{
    public $ftp_url='ftp://newsdk:newsdk@172.16.183.24:21/temp_oms_shidake/';
//     public $ftp_url='ftp://ftp_user:ftp_user@192.168.70.55:21/temp_oms_shidake/';

    public $ftp_pasv = true;

    public $base_dir =  '/newauto/temp_oms_shidake';

    /**
     * 初始化
     */
    public function __construct()
    {
        \ns_core\m_load::load_np("np_xml_to_array.class.php");
        m_config::get_dc();
        m_config::base_write_log('message','request',"-----------消息接收开始-----------",m_config::return_child_path(__FILE__));
    }


    public function init()
    {
        m_file::$ex_file_name ='xml';
        $result_files = m_file::get_files_v2($this->base_dir);
        if(empty($result_files) ||!is_array($result_files))
        {
            return m_config::return_data(1,'未获取到文件列表数据');
        }
        $date = date("Y-m-d H:i:s");
        foreach ($result_files as $value)
        {
            $value['path'] = str_replace($this->base_dir, '', $value['path']);
            $arr_pathinfo = pathinfo($value['path']);
            $file_name = isset($arr_pathinfo['filename']) ? trim($arr_pathinfo['filename']) : '';
            if(strlen($file_name) <=20)
            {
                continue;
            }
            $data = array(
                'nns_id'=>$file_name,
                'nns_cp_id'=>CP_ID,
                'nns_url'=>trim(rtrim(rtrim($this->ftp_url,'/'),'\\')).'/'.trim(ltrim(ltrim($value['path'],'/'),'\\')),
                'nns_create_time'=>$date,
                'nns_modify_time'=>$date,
            );
            $result_exsist = nl_message_temp::query_by_condition(m_config::get_dc(), array('nns_id'=>$file_name,'nns_cp_id'=>CP_ID));
            if($result_exsist['ret'] !=0)
            {
                return $result_exsist;
            }
            if(isset($result_exsist['data_info']) && !empty($result_exsist['data_info']) && is_array($result_exsist['data_info']))
            {
                continue;
            }
            $result_add = nl_message_temp::add(m_config::get_dc(), $data);
            if($result_add['ret'] !=0)
            {
                return $result_add;
            }
        }
        return m_config::return_data(0,'获取到文件列表数据',$result_files);
    }

    /**
     * 类销毁
     */
    public function __destruct()
    {
        m_config::base_write_log('message','request',"-----------消息接收结束-----------",m_config::return_child_path(__FILE__));
    }
}
$delivery_import = new delivery_import();
$result = $delivery_import->init();
echo json_encode($result);