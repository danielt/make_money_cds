<?php
error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_model.message.message_explain");
\ns_core\m_load::load("ns_core.m_pinyin");
\ns_core\m_load::load_np("np_xml2array.class.php");
class message extends \ns_model\message\message_explain
{
    private $arr_picture = null;

    private $arr_map = null;
    
    //一级分类,影片种类
    private $category_map = array (
        '0' => '电影',  //电影
        '1' => '电视剧',  //电视剧
        '2' => '娱乐,综艺',  //综艺
        '3' => '动画',  //动漫
        '4' => '音乐',  //音乐
        '5' => '纪实,纪录片',  //
        '6' => '法治,科教',  //教育
        '7' => '体育',  //体育
        '8' => '生活',  //生活
        '9' => '经济',  //财经
        '10' => '特别节目',//微电影
        '11' => '品牌专区',//品牌专区
        '12' => '广告',  //广告
        '13' => '新闻', //新闻
        '14' => '公开课',
        '15' => '外语及其他语言',
        '16' => '青少年',
        '17' => '播客',
    );
    
    public function init()
    {
        
    }
    
	public function explain($message)
	{
	    $content = $this->get_message_content($message);
	    if($content['ret'] !=0 || strlen($content['data_info']) <1)
	    {
	        return $content;
	    }
	    $str_content = m_config::trim_xml_header($content['data_info']);
	    if(!m_config::is_xml($str_content))
	    {
	        return m_config::return_data(1,"消息GUID[{$message['nns_id']}]xml内容非xml结构");
	    }
	    //解析下载的XML
	    $dom = new DOMDocument('1.0', 'utf-8');
	    $dom->loadXML($str_content);
	    $xml = $dom->saveXML();
	    $xml_arr = np_xml2array::getArrayData($xml);
	    if ($xml_arr["ret"] == 0)
	    {
	        return m_config::return_data(1,"消息GUID[{$message['nns_id']}]xml解析失败");
	    }
	    $Objects = null;
	    // 		$Maps = null;
	    foreach ($xml_arr['data']['children'] as $v)
	    {
	        if (empty($Objects) && $v['name'] == 'Objects')
	        {
	            $Objects = $v['children'];
	        }
	        else if (empty($this->arr_map) && $v['name'] == 'Mappings')
	        {
	            $this->arr_map = $v['children'];
	        }
	    }
	    //Series数据数组
	    $arr_series = array ();
	    //Program数据数组
	    $arr_program = array ();
	    //Movie数据数组
	    $arr_movie = array ();
	    //Picture数据数组
	    $arr_picture = array ();
	    //Channel数据数组
	    $arr_channel = array ();
	    //直播分集数据数组
	    $arr_live_index = array ();
        //直播片源数据数组
	    $arr_live_media = array ();

	    //schedulerecord节目单数据数组
	    $arr_playbill = array ();
	    
	    $array_regist_video = array();
	    $array_regist_index = array();
	    $array_regist_media = array();
	    if (!empty($Objects) && is_array($Objects))
	    {
	        foreach ($Objects as $obj)
	        {
	            if ($obj['attributes']['ElementType'] == 'Series')
	            {
	                $arr_series[]=$this->make_key_value_arr($obj);
	            }
	            else if ($obj['attributes']['ElementType'] == 'Program')
	            {
	                $arr_program[]=$this->make_key_value_arr($obj);
	            }
	            else if ($obj['attributes']['ElementType'] == 'Movie')
	            {
	                $arr_movie[]=$this->make_key_value_arr($obj);
	            }
	            //直播频道
	            else if ($obj['attributes']['ElementType'] == 'Channel')
	            {
	                $arr_channel[]=$this->make_key_value_arr($obj);
	            }
	            //直播片源
	            else if ($obj['attributes']['ElementType'] == 'PhysicalChannel')
	            {
	                $arr_live_media[]=$this->make_key_value_arr($obj);
	            }
	            //直播分集
	            else if ($obj['attributes']['ElementType'] == 'PhysicalChannel')
	            {
	                $arr_live_index[]=$this->make_key_value_arr($obj);
	            }
	            //节目单
	            else if ($obj['attributes']['ElementType'] == 'ScheduleRecord')
	            {
                    $arr_playbill[]=$this->make_key_value_arr($obj);
	            }
	            else if ($obj['attributes']['ElementType'] == 'Picture')
	            {
	                $this->arr_picture[]=$this->make_key_value_arr($obj);
	            }
	        }
	    }
	    //主媒资操作   主媒资增删改走以前老逻辑，基本没什么变化
	    if (!empty($arr_series))
	    {
	        foreach ($arr_series as $val_series)
	        {
	            //主媒资删除
	            if (strtoupper($val_series['Action']) == 'DELETE')
	            {
	                $del_series = array(
	                    'base_info'=>array(
	                        'nns_asset_import_id'=>$val_series['ContentID'],
                            'nns_import_source'=>$val_series['nns_import_source'],
                            'nns_cp_id'=>$message['nns_cp_id'],
                        ),
	                );
	                $result = $this->vod_action('delete', $del_series);
	            }
	            else
	            {
	                //默认一级分类为电影
	                $view_type = 0;
	                if ($message['nns_cp_id'] == 'JSCN')
	                {
	                    $val_series['key_val_list']['CategoryName'] = (isset($val_series['key_val_list']['PgmCategory']) && strlen($val_series['key_val_list']['PgmCategory']) >0) ? $val_series['key_val_list']['PgmCategory'] : '电影';
	                }
	                $asset_category = (isset($val_series['key_val_list']['CategoryName']) && strlen($val_series['key_val_list']['CategoryName']) >0) ? $val_series['key_val_list']['CategoryName'] : '电影';
	                $do_category = array(
	                             'base_info'=>array(
	                                      'nns_name'=>$asset_category,   //栏目名称支持多层目录，多层目录的时候 /分割
	                                      'nns_import_category_id'=>'', //栏目名称支持多层目录，多层目录的时候 /分割
	                                      'nns_cp_id'=>$message['nns_cp_id'],
	                                      'nns_import_parent_category_id'=>'',
	                                      'nns_video_type'=>'0',  //媒资类型  0 点播 | 1 直播
	                              ),
                     );
	                $result = $this->category_action('add', $do_category);
	                if($result['ret'] !=0)
	                {
	                    return $result;
	                }
	                if(!isset($result['data_info']['base_info']['nns_id']) || strlen($result['data_info']['base_info']['nns_id']) <1)
	                {
	                    return m_config::return_data(1,'创建栏目失败');
	                }
	                $nns_category_id = $result['data_info']['base_info']['nns_id'];
	                foreach ($this->category_map as $index => $v)
	                {
	                    if (stripos($v, $asset_category) !== false)
	                    {
	                        $view_type = $index;
	                        break;
	                    }
	                }
	                if($view_type == 0 && isset($val_series['key_val_list']['VolumnCount']) && $val_series['key_val_list']['VolumnCount'] >1)
	                {
	                    $view_type = 1;
	                }
	                #TODO
	                $arr_img = $this->handle_picture_v3($val_series['ContentID']);
	                if (!empty($arr_img) && is_array($arr_img))
	                {
	                    $val_series['key_val_list'] = array_merge($val_series['key_val_list'], $arr_img);
	                }
	                $pinyin = m_pinyin::get_pinyin_letter($val_series['key_val_list']['Name']);
	                $add_series = array(
                        'base_info'=>array(
                                'nns_name'=>isset($val_series['key_val_list']['Name']) ? $val_series['key_val_list']['Name'] : '',
                                'nns_view_type'=>$view_type,
                                'nns_org_type'=>'0',
                                'nns_tag'=>'26,',
                                'nns_director'=>isset($val_series['key_val_list']['WriterDisplay']) ? $val_series['key_val_list']['WriterDisplay'] : '',
                                'nns_actor'=>isset($val_series['key_val_list']['ActorDisplay']) ? $val_series['key_val_list']['ActorDisplay'] : '',
                                'nns_show_time'=>(isset($val_series['key_val_list']['ReleaseYear']) && strlen($val_series['key_val_list']['ReleaseYear']) >0) ? date("Y",strtotime($val_series['key_val_list']['ReleaseYear'])) : date("Y"),
                                'nns_view_len'=>isset($val_series['key_val_list']['Duration']) ? $val_series['key_val_list']['Duration'] : 0,
                                'nns_all_index'=>isset($val_series['key_val_list']['VolumnCount']) ? $val_series['key_val_list']['VolumnCount'] : 1,
                                'nns_new_index'=>isset($val_series['key_val_list']['NewCount']) ? $val_series['key_val_list']['NewCount'] : 0,
                                'nns_area'=>isset($val_series['key_val_list']['OriginalCountry']) ? $val_series['key_val_list']['OriginalCountry'] : '',
                                'nns_image0'=>isset($val_series['key_val_list']['bigpic']) ? $val_series['key_val_list']['bigpic'] : '',
                                'nns_image1'=>isset($val_series['key_val_list']['middlepic']) ? $val_series['key_val_list']['middlepic'] : '',
                                'nns_image2'=>isset($val_series['key_val_list']['smallpic']) ? $val_series['key_val_list']['smallpic'] : '',
                                'nns_image3'=>'',
                                'nns_image4'=>'',
                                'nns_image5'=>'',
                                'nns_summary'=>isset($val_series['key_val_list']['Description']) ? $val_series['key_val_list']['Description'] : '',
                                'nns_remark'=>isset($val_series['key_val_list']['Kind']) ? $val_series['key_val_list']['Kind'] : '',
                                'nns_category_id'=>$nns_category_id,
                                'nns_play_count'=>'0',
                                'nns_score_total'=>'0',
                                'nns_score_count'=>'0',
                                'nns_point'=>isset($val_series['key_val_list']['ViewPoint']) ? $val_series['key_val_list']['ViewPoint'] : '0',
                                'nns_copyright_date'=>(isset($val_series['key_val_list']['ReleaseYear']) && strlen($val_series['key_val_list']['ReleaseYear']) >0) ? date("Y",strtotime($val_series['key_val_list']['ReleaseYear'])) : date("Y"),
                                'nns_asset_import_id'=>$val_series['ContentID'],
                                'nns_pinyin'=>(isset($val_series['key_val_list']['Spell']) && strlen($val_series['key_val_list']['Spell'])>0) ? $val_series['key_val_list']['Spell'] : $pinyin,
                                'nns_pinyin_length'=>(isset($val_series['key_val_list']['Spell']) && strlen($val_series['key_val_list']['Spell'])>0) ? strlen($val_series['key_val_list']['Spell']) : strlen($pinyin),
                                'nns_alias_name'=>isset($val_series['key_val_list']['AliasName']) ? $val_series['key_val_list']['AliasName'] : '',
                                'nns_eng_name'=>isset($val_series['key_val_list']['EnglishName']) ? $val_series['key_val_list']['EnglishName'] : '',
                                'nns_language'=>isset($val_series['key_val_list']['Language']) ? $val_series['key_val_list']['Language'] : '',
                                'nns_text_lang'=>isset($val_series['key_val_list']['Language']) ? $val_series['key_val_list']['Language'] : '',
                                'nns_producer'=>isset($val_series['key_val_list']['ContentProvider']) ? $val_series['key_val_list']['ContentProvider'] : '',
                                'nns_screenwriter'=>isset($val_series['key_val_list']['WriterDisplay']) ? $val_series['key_val_list']['WriterDisplay'] : '',
                                'nns_play_role'=>'',
                                'nns_copyright_range'=>isset($val_series['key_val_list']['ContentProvider']) ? $val_series['key_val_list']['ContentProvider'] : '',
                                'nns_vod_part'=>'',
                                'nns_keyword'=>isset($val_series['key_val_list']['Keywords']) ? $val_series['key_val_list']['Keywords'] : '',
                                'nns_import_source'=>isset($val_series['nns_import_source']) ? $val_series['nns_import_source'] : evn::get("project"),
                                'nns_kind'=>isset($val_series['key_val_list']['Kind']) ? $val_series['key_val_list']['Kind'] : '',
                                'nns_copyright'=>isset($val_series['key_val_list']['ContentProvider']) ? $val_series['key_val_list']['ContentProvider'] : '',
                                'nns_clarity'=>'',
                                'nns_image_v'=>isset($val_series['key_val_list']['verticality_img']) ? $val_series['key_val_list']['verticality_img'] : '',
                                'nns_image_s'=>isset($val_series['key_val_list']['square_img']) ? $val_series['key_val_list']['square_img'] : '',
                                'nns_image_h'=>isset($val_series['key_val_list']['horizontal_img']) ? $val_series['key_val_list']['horizontal_img'] : '',
                                'nns_cp_id'=>$message['nns_cp_id'],
                                'nns_conf_info'=>'',
                                'nns_ext_url'=>'',
                            ), //基本信息（存储于nns_vod表中）
                       'ex_info'=>array(
                                'svc_item_id'=>'',
                                'month_clicks'=>'',
                                'week_clicks'=>'',
                                'base_id'=>'',
                                'asset_path'=>'',
                                'ex_tag'=>'',
                                'full_spell'=>'',
                                'awards'=>'',
                                'year'=>'',
                                'play_time'=>'',
                                'channel'=>'',
                                'first_spell'=>'',
                        ), //扩展信息（存储于nns_vod_ex表中）
                    );
	                //字段待修改4-14
	                $result = $this->vod_action('add', $add_series);
	            }
                if($result['ret'] !=0)
                {
                    break;
                }
	        }
	    }
	    //分集操作   分集删除走以前的老逻辑，分集注册  则组装分集和片源的信息到redis，分集更新则查询redis关联数据
	    if(!empty($arr_program))
	    {
	        foreach ($arr_program as $val_program)
	        {
	            //分集删除
	            if(strtoupper($val_program['Action']) == 'DELETE')
	            {
	                $re = $this->delete_video_clip($val_program['key_val_list'],$val_program['ContentID']);
	                if($re['ret'] !=0)
	                {
	                    $re['ret'] = 1;
	                    return $re;
	                }
	            }
	            else
	            {
	                if ($this->cp_id == 'JSCN' && $val_program['key_val_list']['SeriesFlag'] == 0)
	                {
	                    $p_series[0] = $val_program['ContentID'];
	                }
	                else
	                {
	                    $arr_p_series = $this->get_map_info_v2('Program', $this->arr_map, $val_program['ContentID']);
	                    $p_series = array_keys($arr_p_series);
	                    //分集不存在集数字段
	                    if (!isset($val_program['key_val_list']['Sequence']) || !strlen($val_program['key_val_list']['Sequence'])>0)
	                    {
	                        $val_program['key_val_list']['Sequence'] = isset($arr_p_series[$p_series[0]]['key_val_list']['Sequence']) ? $arr_p_series[$p_series[0]]['key_val_list']['Sequence'] : 0;
	                    }
	                    if(substr($this->cp_id, 0,5) == 'fuse_')
	                    {
	                        $val_program['key_val_list']['Sequence']++;
	                    }
	                }
	                $res = nl_asset_incidence_relation::query_asset_relation($this->import->dc,'video',$p_series[0],$this->cp_id,0);
	                if (!is_array($res['data_info']))
	                {
	                    return $re['ret'] = 6;
	                }
	                $arr_img = $this->handle_picture_v3($val_program['ContentID']);
	                if (!empty($arr_img) && is_array($arr_img))
	                {
	                    $val_program['key_val_list'] = array_merge($val_program['key_val_list'], $arr_img);
	                }
	                //分集注入
	                $re = $this->modify_video_clip($val_program['key_val_list'],$p_series[0],$val_program['ContentID']);
	                if($re['ret'] != 0 )
	                {
	                    $re['ret'] = 1;
	                    return $re;
	                }
	            }
	        }
	    }
	    //片源操作
	    if(!empty($arr_movie))
	    {
	        foreach ($arr_movie as $val_movie)
	        {
	            //片源删除
	            if(strtoupper($val_movie['Action']) == 'DELETE')
	            {
	                $re=$this->delete_video_file($val_movie['key_val_list'],$val_movie['ContentID']);
	                if($re['ret'] !=0)
	                {
	                    $re['ret'] = 1;
	                    return $re;
	                }
	            }
	            //片源修改
	            else
	            {
	                $arr_p_program = $this->get_map_info_v2('Movie', $this->arr_map, $val_movie['ContentID']);
	                $arr_p_program = array_keys($arr_p_program);
	                $index_info = nl_asset_incidence_relation::query_asset_relation($this->import->dc,'index',$arr_p_program[0],$this->cp_id,1);
	                if (!is_array($index_info['data_info']))
	                {
	                    //返回分集未注入
	                    return $re['ret'] = 7;
	                }
	                $re = $this->import_video_file($val_movie['key_val_list'],$index_info['data_info'][0]['nns_asset_import_id'],$arr_p_program[0],$val_movie['ContentID']);
	                if($re['ret'] !=0)
	                {
	                    $re['ret'] = 1;
	                    return $re;
	                }
	            }
	        }
	    }

	    //直播频道操作  直播频道的增删改和主媒资的相同
	    if(!empty($arr_channel))
	    {
	        //循环频道，对每个频道进行处理
	        foreach($arr_channel as $val_channel)
	        {
	            //频道删除操作
	            if(strtoupper($val_channel['Action']) == 'DELETE')
	            {
                    $del_channel = array(
                        'base_info'=>array(
                            'nns_import_id' => $val_channel['ContentID'],
                            'nns_import_source' => $val_channel['nns_import_source'],
                            'nns_cp_id' => $message['nns_cp_id'],
                        ),
                    );
                    $result = $this->live_action('delete', $del_channel);
	            }
	            //直播频道更新或者注入
	            else
	            {
                    //默认一级分类为
                    $view_type = 1;
                    if ($message['nns_cp_id'] == 'JSCN')
                    {
                        $val_channel['key_val_list']['CategoryName'] = (isset($val_channel['key_val_list']['PgmCategory']) && strlen($val_channel['key_val_list']['PgmCategory']) >0) ? $val_channel['key_val_list']['PgmCategory'] : '卫士频道';
                    }
                    $asset_category = (isset($val_channel['key_val_list']['CategoryName']) && strlen($val_channel['key_val_list']['CategoryName']) >0) ? $val_channel['key_val_list']['CategoryName'] : '卫士频道';
                    $category_import_id = (isset($val_channel['key_val_list']['CategoryID']) && strlen($val_channel['key_val_list']['CategoryID']) >0) ? $val_channel['key_val_list']['CategoryID'] : '';

                    $do_category = array(
                        'base_info'=>array(
                            'nns_name' => $asset_category,   //栏目名称支持多层目录，多层目录的时候 /分割
                            'nns_import_category_id' => $category_import_id, //栏目名称支持多层目录，多层目录的时候 /分割
                            'nns_cp_id' => $message['nns_cp_id'],
                            'nns_import_parent_category_id' => '',
                            'nns_video_type' => '1',  //媒资类型  0 点播 | 1 直播
                        ),
                    );
                    $result = $this->category_action('add', $do_category);
                    if($result['ret'] !=0)
                    {
                        return $result;
                    }
                    if(!isset($result['data_info']['base_info']['nns_id']) || strlen($result['data_info']['base_info']['nns_id']) <1)
                    {
                        return m_config::return_data(1,'创建栏目失败');
                    }
                    $nns_category_id = $result['data_info']['base_info']['nns_id'];
                    //foreach ($this->category_map as $index => $v)
                    //{
                    //    if (stripos($v, $asset_category) !== false)
                    //    {
                    //        $view_type = $index;
                    //        break;
                    //    }
                    //}
                    //if($view_type == 1 && isset($val_channel['key_val_list']['VolumnCount']) && $val_channel['key_val_list']['VolumnCount'] >1)
                    //{
                    //    $view_type = 1;
                    //}
                    #TODO
                    $arr_img = $this->handle_picture_v3($val_channel['ContentID']);
                    if (!empty($arr_img) && is_array($arr_img))
                    {
                        $val_series['key_val_list'] = array_merge($val_channel['key_val_list'], $arr_img);
                    }
                    $pinyin = m_pinyin::get_pinyin_letter($val_channel['key_val_list']['Name']);

                    $ext_info = array();
                    //扩展表存放的扩展字段--频道号
                    if (!isset($val_channel['key_val_list']['ChannelNumber']) || strlen($val_channel['key_val_list']['ChannelNumber']))
                    {
                        $ext_info[] = array(
                            'nns_key' => 'live_pindao_order',
                            'nns_value' => $val_channel['key_val_list']['ChannelNumber'],
                            'nns_cp_id' => $message['nns_cp_id'],
                        );
                    }
                    $add_channel = array(
                        'base_info' => array(
                            'nns_name'=>isset($val_channel['key_val_list']['Name']) ? $val_channel['key_val_list']['Name'] : '',
                            'nns_view_type'=>$view_type,
                            'nns_org_type'=>'0',
                            'nns_tag'=>'26,',
                            'nns_director'=>isset($val_channel['key_val_list']['WriterDisplay']) ? $val_channel['key_val_list']['WriterDisplay'] : '',
                            'nns_actor'=>isset($val_channel['key_val_list']['ActorDisplay']) ? $val_channel['key_val_list']['ActorDisplay'] : '',
                            'nns_show_time'=>(isset($val_channel['key_val_list']['ReleaseYear']) && strlen($val_channel['key_val_list']['ReleaseYear']) >0) ? date("Y",strtotime($val_channel['key_val_list']['ReleaseYear'])) : date("Y"),
                            'nns_view_len'=>isset($val_channel['key_val_list']['Duration']) ? $val_channel['key_val_list']['Duration'] : 0,
                            'nns_all_index'=>isset($val_channel['key_val_list']['VolumnCount']) ? $val_channel['key_val_list']['VolumnCount'] : 1,
                            'nns_new_index'=>isset($val_channel['key_val_list']['NewCount']) ? $val_channel['key_val_list']['NewCount'] : 0,
                            'nns_area'=>isset($val_channel['key_val_list']['Country']) ? $val_channel['key_val_list']['Country'] : '',
                            'nns_image0'=>isset($val_channel['key_val_list']['bigpic']) ? $val_channel['key_val_list']['bigpic'] : '',
                            'nns_image1'=>isset($val_channel['key_val_list']['middlepic']) ? $val_channel['key_val_list']['middlepic'] : '',
                            'nns_image2'=>isset($val_channel['key_val_list']['smallpic']) ? $val_channel['key_val_list']['smallpic'] : '',
                            'nns_image3'=>'',
                            'nns_image4'=>'',
                            'nns_image5'=>'',
                            'nns_summary'=>isset($val_channel['key_val_list']['Description']) ? $val_channel['key_val_list']['Description'] : '',
                            'nns_remark'=>isset($val_channel['key_val_list']['Kind']) ? $val_channel['key_val_list']['Kind'] : '',
                            'nns_category_id'=>$nns_category_id,
                            'nns_play_count'=>'0',
                            'nns_score'=>'0',
                            'nns_score_count'=>'0',
                            'nns_point'=>isset($val_channel['key_val_list']['ViewPoint']) ? $val_channel['key_val_list']['ViewPoint'] : '0',
                            'nns_copyright_date'=>(isset($val_channel['key_val_list']['ReleaseYear']) && strlen($val_channel['key_val_list']['ReleaseYear']) >0) ? date("Y",strtotime($val_channel['key_val_list']['ReleaseYear'])) : date("Y"),
                            'nns_import_id'=>$val_channel['ContentID'],
                            'nns_pinyin'=>(isset($val_channel['key_val_list']['Spell']) && strlen($val_channel['key_val_list']['Spell'])>0) ? $val_channel['key_val_list']['Spell'] : $pinyin,
                            'nns_pinyin_length'=>(isset($val_channel['key_val_list']['Spell']) && strlen($val_channel['key_val_list']['Spell'])>0) ? strlen($val_channel['key_val_list']['Spell']) : strlen($pinyin),
                            'nns_alias_name'=>isset($val_channel['key_val_list']['CallSign']) ? $val_channel['key_val_list']['CallSign'] : '',
                            'nns_eng_name'=>isset($val_channel['key_val_list']['EnglishName']) ? $val_channel['key_val_list']['EnglishName'] : '',
                            'nns_language'=>isset($val_channel['key_val_list']['Language']) ? $val_channel['key_val_list']['Language'] : '',
                            'nns_producer'=>isset($val_channel['key_val_list']['ContentProvider']) ? $val_channel['key_val_list']['ContentProvider'] : '',
                            'nns_import_source'=>isset($val_channel['nns_import_source']) ? $val_channel['nns_import_source'] : evn::get("project"),
                            'nns_image_v'=>isset($val_channel['key_val_list']['verticality_img']) ? $val_channel['key_val_list']['verticality_img'] : '',
                            'nns_image_s'=>isset($val_channel['key_val_list']['square_img']) ? $val_channel['key_val_list']['square_img'] : '',
                            'nns_image_h'=>isset($val_channel['key_val_list']['horizontal_img']) ? $val_channel['key_val_list']['horizontal_img'] : '',
                            'nns_image_t'=>'',
                            'nns_cp_id'=>$message['nns_cp_id'],
                            'nns_ext_info'=>'',
                        ), //基本信息（存储于nns_live表中）
                        'ex_info' => $ext_info,//扩展信息（存储于nns_live_ex表中）
                        'hide_info' => array(
                            'nns_id' => np_guid_rand(),
                            'nns_state' => isset($val_channel['key_val_list']['State']) ? $val_channel['key_val_list']['State'] : 1,
                            'nns_deleted' => 0,
                            'nns_check' => isset($val_channel['key_val_list']['Check']) ? $val_channel['key_val_list']['Check'] : 1,
                        )
                    );
                    //字段待修改4-14
                    $result = $this->live_action('add', $add_channel);
	            }
                if($result['ret'] != 0)
                {
                    break;
                }
	        }
	    }

	    //直播频道片源
	    if(!empty($arr_live_media))
	    {
	        foreach($arr_live_media as $val_live_media)
	        {
	            //删除
	            if(strtoupper($val_live_media['Action']) == 'DELETE')
	            {
                    $del_live_media = array(
                        'base_info'=>array(
                            'nns_content_id' => $val_live_media['ContentID'],
                            'nns_import_source' => $val_live_media['nns_import_source'],
                            'nns_cp_id' => $message['nns_cp_id'],
                        ),
                    );
                    $re = $this->live_media_action('delete', $del_live_media);
	            }
	            //注入和修改
	            else
	            {
	                $arr_p_channel = $this->get_map_info_v2('PhysicalChannel', $this->arr_map, $val_live_media['ContentID']);
	                $arr_p_channel = array_keys($arr_p_channel);
	                $sql = "select * from nns_live where `nns_import_id` = '{$arr_p_channel[0]}'";
	                $res_channel = nl_query_by_db($sql,$this->import->dc->db());
	                if (!is_array($res_channel))
	                {
	                    //返回频道未注入
	                    return $re['ret'] = 10;
	                }

	                // 仿照分集
	                $str_index_id = np_guid_rand();
	                $add_live_index =array(
	                    'base_info' => array(
	                        'nns_name'=> isset($val_live_media['key_val_list']['Name']) ? $val_live_media['key_val_list']['Name'] : '',
                            'nns_index' => 0,
                            'nns_time_len' => 3600,
                            'nns_summary' => '',
                            'nns_image' => '',
                            'nns_play_count' => 0,
                            'nns_score' => 0,
                            'nns_score_count' => 0,
                            'nns_cp_id' => $message['nns_cp_id'],
                        ),
                        'hide_info' => array(
                            'nns_id' => $str_index_id,
                            'nns_live_id' => $res_channel[0]['nns_id'],
                        )
	                );
	                $sql="select * from nns_live_index where nns_live_id = '{$res_channel[0]['nns_id']}' and nns_cp_id='".$this->cp_id."' limit 1";
	                $result_exist = nl_query_by_db($sql, $this->import->dc->db());
	                if(!is_array($result_exist) || empty($result_exist) || !isset($result_exist[0]) || !is_array($result_exist[0]) || empty($result_exist[0]))
	                {
                        $re = $this->live_index_action('add', $add_live_index);
                        if ($re != 0)
                        {
                            $re['ret'] = 1;
                        }
                        return $re;
	                }
	                else
	                {
	                    $str_index_id = $result_exist[0]['nns_id'];
	                }

	                //直播片源参数
                    $add_live_media = array(
                        'base_info' => array(
                            'nns_name'=> isset($val_live_media['key_val_list']['Name']) ? $val_live_media['key_val_list']['Name'] : '',
                            'nns_type'=> 1,
                            'nns_url' => isset($val_live_media['key_val_list']['Url']) ? $val_live_media['key_val_list']['Url'] : '',
                            'nns_tag' => '26,',
                            'nns_mode' => isset($val_live_media['key_val_list']['Mode']) ? $val_live_media['key_val_list']['Mode'] : '',
                            'nns_kbps' => isset($val_live_media['key_val_list']['UnicastUrl']) ? $val_live_media['key_val_list']['UnicastUrl'] : '',
                            'nns_content_id '=> $val_live_media['ContentID'],
                            'nns_content_state' => 0,
                            'nns_filetype' => isset($val_live_media['key_val_list']['FileType']) ? $val_live_media['key_val_list']['FileType'] : 'ts',
                            'nns_play_count' => '0',
                            'nns_score_total' => '0',
                            'nns_score_count' => '0',
                            'nns_media_cps'=> isset($val_live_media['key_val_list']['MediaCPS']) ? $val_live_media['key_val_list']['MediaCPS'] : 'LIVE',
                            'nns_cp_id' => isset($val_live_media['key_val_list']['CPContentID']) ? $val_live_media['key_val_list']['CPContentID'] : $message['nns_cp_id'],
                            'nns_cast_type' => isset($val_live_media['key_val_list']['CastType']) ? $val_live_media['key_val_list']['CastType'] : 0,
                            'nns_message_id' => $message['nns_id'],
                            'nns_timeshift_status' => isset($val_live_media['key_val_list']['TimeShiftStatus']) ? $val_live_media['key_val_list']['TimeShiftStatus'] : 0,
                            'nns_timeshift_delay' => isset($val_live_media['key_val_list']['TimeShiftDelay']) ? $val_live_media['key_val_list']['TimeShiftDelay'] : '',
                            'nns_storage_status' => isset($val_live_media['key_val_list']['StorageStatus']) ? $val_live_media['key_val_list']['StorageStatus'] : 0,
                            'nns_storage_delay' => isset($val_live_media['key_val_list']['StorageDelay']) ? $val_live_media['key_val_list']['StorageDelay'] : '',
                            'nns_drm_enabled' => isset($val_live_media['key_val_list']['DrmEnabled']) ? $val_live_media['key_val_list']['DrmEnabled'] : 0,
                            'nns_drm_encrypt_solution' => isset($val_live_media['key_val_list']['DrmEncryptSolution']) ? $val_live_media['key_val_list']['DrmEncryptSolution'] : '',
                            'nns_drm_ext_info' => isset($val_live_media['key_val_list']['DrmEncryptExt']) ? $val_live_media['key_val_list']['DrmEncryptExt'] : '',
                            'nns_import_source' => isset($val_live_media['nns_import_source']) ? $val_live_media['nns_import_source'] : evn::get("project"),
                            'nns_domain' => isset($val_live_media['key_val_list']['MultiCastPort']) ? $val_live_media['key_val_list']['MultiCastPort'] : '',
                            'nns_media_service' => isset($val_live_media['key_val_list']['MediaService']) ? $val_live_media['key_val_list']['MediaService'] : 'MSP',
                            'nns_ext_url' => '',
                        ), //基本信息（存储于nns_live表中）
                        'hide_info' => array(
                            'nns_id' => np_guid_rand(),
                            'nns_state' => isset($val_live_media['key_val_list']['State']) ? $val_live_media['key_val_list']['State'] : 1,
                            'nns_deleted' => 0,
                            'nns_check' => isset($val_live_media['key_val_list']['Check']) ? $val_live_media['key_val_list']['Check'] : 1,'nns_original_id' => isset($val_channel['key_val_list']['OriginalId']) ? $val_channel['key_val_list']['OriginalId'] : '',
                            'nns_live_id' => $str_index_id,
                        ),
                    );

	                $re = $this->live_media_action('add', $add_live_media);
	                if($re['ret'] !=0)
	                {
	                    $re['ret'] = 1;
	                }
	                return $re;
	            }
	        }
	    }

	    //节目单注入
        if (!empty($arr_playbill))
        {
            //循环节目单，对每个节目单进行处理
            foreach($arr_playbill as $val_playbill)
            {
                //频道删除操作
                if(strtoupper($val_playbill['Action']) == 'DELETE')
                {
                    $del_playbill = array(
                        'base_info'=>array(
                            'nns_playbill_import_id' => $val_playbill['ContentID'],
                            'nns_import_source' => $val_playbill['nns_import_source'],
                            'nns_cp_id' => $message['nns_cp_id'],
                        ),
                    );
                    $result = $this->playbill_item_action('delete', $del_playbill);
                }
                //节目单更新或者注入
                else
                {
                    #TODO
                    $arr_img = $this->handle_picture_v3($val_playbill['ContentID']);
                    if (!empty($arr_img) && is_array($arr_img))
                    {
                        $val_series['key_val_list'] = array_merge($val_playbill['key_val_list'], $arr_img);
                    }
                    $pinyin = m_pinyin::get_pinyin_letter($val_playbill['key_val_list']['Name']);

                    //开始时间
                    $start_date = isset($val_playbill['key_val_list']['StartDate']) ? $val_playbill['key_val_list']['StartTime'] : date('Ymd');
                    $start_time = isset($val_playbill['key_val_list']['StartTime']) ? $val_playbill['key_val_list']['StartTime'] : date('His');
                    $begin_time = $start_date . " " . $start_time;
                    $begin_time = date('Y-m-d H:i:s', strtotime($begin_time));

                    $time_len = isset($val_playbill['key_val_list']['Duration']) ? $val_playbill['key_val_list']['Duration'] : 0;

                    $end_time = isset($val_playbill['key_val_list']['EndTime']) ? $val_playbill['key_val_list']['EndTime'] : (int)$start_time + (int)$time_len;
                    $end_time = $start_date . " " . $end_time;
                    $end_time = date('Y-m-d H:i:s', strtotime($end_time));

                    $add_playbill = array(
                        'base_info' => array(
                            'nns_name'=>isset($val_playbill['key_val_list']['Name']) ? $val_playbill['key_val_list']['Name'] : '',
                            'nns_view_type'=> 1,
                            'nns_begin_time' => $begin_time,
                            'nns_end_time' => $end_time,
                            'nns_time_len'=>isset($val_playbill['key_val_list']['Duration']) ? $val_playbill['key_val_list']['Duration'] : 0,
                            'nns_live_id' => '',
                            'nns_summary'=>isset($val_playbill['key_val_list']['Description']) ? $val_playbill['key_val_list']['Description'] : '',
                            'nns_image0'=>isset($val_playbill['key_val_list']['bigpic']) ? $val_playbill['key_val_list']['bigpic'] : '',
                            'nns_image1'=>isset($val_playbill['key_val_list']['middlepic']) ? $val_playbill['key_val_list']['middlepic'] : '',
                            'nns_image2'=>isset($val_playbill['key_val_list']['smallpic']) ? $val_playbill['key_val_list']['smallpic'] : '',
                            'nns_image3'=>'',
                            'nns_image4'=>'',
                            'nns_image5'=>'',
                            'nns_pinyin'=>(isset($val_playbill['key_val_list']['Spell']) && strlen($val_playbill['key_val_list']['Spell'])>0) ? $val_playbill['key_val_list']['Spell'] : $pinyin,
                            'nns_pinyin_length'=>(isset($val_playbill['key_val_list']['Spell']) && strlen($val_playbill['key_val_list']['Spell'])>0) ? strlen($val_playbill['key_val_list']['Spell']) : strlen($pinyin),
                            'nns_eng_name'=>isset($val_playbill['key_val_list']['EnglishName']) ? $val_playbill['key_val_list']['EnglishName'] : '',
                            'nns_keyword'=>isset($val_playbill['key_val_list']['Keyword']) ? $val_playbill['key_val_list']['Keyword'] : '',
                            'nns_kind'=>isset($val_playbill['key_val_list']['Kind']) ? $val_playbill['key_val_list']['Kind'] : '',
                            'nns_actor'=>isset($val_playbill['key_val_list']['ActorDisplay']) ? $val_playbill['key_val_list']['ActorDisplay'] : '',
                            'nns_director'=>isset($val_playbill['key_val_list']['WriterDisplay']) ? $val_playbill['key_val_list']['WriterDisplay'] : '',
                            'nns_live_media_id' => isset($val_playbill['PhysicalChannelID']) ? $val_playbill['PhysicalChannelID'] : '',
                            'nns_playbill_import_id'=>$val_playbill['ContentID'],
                            'nns_domain'=>isset($val_playbill['key_val_list']['Domain']) ? $val_playbill['key_val_list']['Domain'] : '',
                            'nns_hot_dgree'=>isset($val_playbill['key_val_list']['HotDgree']) ? $val_playbill['key_val_list']['HotDgree'] : 0,
                            'nns_cp_id'=>$message['nns_cp_id'],
                            'nns_import_source'=>isset($val_playbill['nns_import_source']) ? $val_playbill['nns_import_source'] : evn::get("project"),
                            'nns_ext_url'=>'',
                            'nns_image_v'=>isset($val_playbill['key_val_list']['verticality_img']) ? $val_playbill['key_val_list']['verticality_img'] : '',
                            'nns_image_h'=>isset($val_playbill['key_val_list']['horizontal_img']) ? $val_playbill['key_val_list']['horizontal_img'] : '',
                            'nns_image_s'=>isset($val_playbill['key_val_list']['square_img']) ? $val_playbill['key_val_list']['square_img'] : '',
                        ), //基本信息（存储于nns_live表中）
                        'hide_info' => array(
                            'nns_id' => np_guid_rand(),
                            'nns_state' => isset($val_playbill['key_val_list']['State']) ? $val_playbill['key_val_list']['State'] : 1,
                            'nns_deleted' => 0,
                            'nns_check' => isset($val_playbill['key_val_list']['Check']) ? $val_playbill['key_val_list']['Check'] : 1,
                        )
                    );
                    //字段待修改4-14
                    $result = $this->live_action('add', $add_playbill);
                }
                if($result['ret'] != 0)
                {
                    break;
                }
            }
        }
	}

	private function handle_picture_v3($asset_id)
	{
	    $array_img = array();
	    $array_img['bigpic']='';
	    $array_img['middlepic']='';
	    $array_img['smallpic']='';
	    $array_img['horizontal_img']='';
	    $array_img['verticality_img']='';
	    $array_img['square_img']='';
	    if(empty($this->arr_picture) || !is_array($this->arr_picture))
	    {
	        return $array_img;
	    }
	    $array_map = $this->get_map_info_v2('Picture',$this->arr_map,$asset_id);
	    if(empty($array_map))
	    {
	        return $array_img;
	    }
	    $array_map_key = array_keys($array_map);
	    $temp_array = array();
	    foreach ($this->arr_picture as $obj_picture)
	    {
	        if(!in_array($obj_picture['ContentID'], $array_map_key))
	        {
	            continue;
	        }
	        //组成数据,key为图片type,值为url
	        if (!isset($obj_picture['key_val_list']['Type']))
	        {
	            foreach ($array_map as $k_id => $val)
	            {
	                if ($obj_picture['ContentID'] == $k_id)
	                {
	                    $obj_picture['key_val_list']['Type'] = $val['key_val_list']['Type'];
	                }
	            }
	        }
	        $temp_array[$obj_picture['key_val_list']['Type']] = (isset($obj_picture['key_val_list']['FileURL']) && strlen($obj_picture['key_val_list']['FileURL']) >0) ? trim($obj_picture['key_val_list']['FileURL']) : '';
	    }
	    $temp_array = array_filter($temp_array);
	    if(empty($temp_array))
	    {
	        return $array_img;
	    }
	    $arr_combine = $this->combine_picture($array_img, $temp_array);
	    return array_combine(array_keys($array_img),$arr_combine);
	}
	
	private function get_map_info_v2($p_type,$arr_map, $e_id)
	{
	    $temp_arr = array();
        foreach ($arr_map as $map)
        {
            if ($map['attributes']['ElementType'] == $p_type && $map['attributes']['ParentCode'] == $e_id)
            {
                $filter_map = $this->make_key_value_arr($map);
                $temp_arr[$map['attributes']['ElementCode']] = $filter_map;
            }
            if(isset($temp_arr[$map['attributes']['ElementCode']]))
            {
                continue;
            }
            if ($map['attributes']['ParentType'] == $p_type && $map['attributes']['ElementCode'] == $e_id)
            {
                $filter_map = $this->make_key_value_arr($map);
                $temp_arr[$map['attributes']['ParentCode']] = $filter_map;
            }
        }
	    return $temp_arr;
	}
	
	/**
	 * 获取xml的 attr属性和key value值
	 * @param unknown $xml_obj_arr
	 * @return Ambigous <NULL, unknown, string>
	 */
	private function make_key_value_arr($xml_obj_arr)
	{
	    $key_val_array = null;
	    if (isset($xml_obj_arr['attributes']) && is_array($xml_obj_arr['attributes']) && !empty($xml_obj_arr['attributes']))
	    {
	        foreach ($xml_obj_arr['attributes'] as $attr_key => $attr_val)
	        {
	            $key_val_array[$attr_key] = $attr_val;
	        }
	        if(!isset($key_val_array['ContentID']) || strlen($key_val_array['ContentID'])<1)
	        {
	            $key_val_array['ContentID'] = isset($xml_obj_arr['attributes']['ID']) ? $xml_obj_arr['attributes']['ID'] : '';
	        }
	        unset($xml_obj_arr['attributes']);
	    }
	    if (isset($xml_obj_arr['children']) && is_array($xml_obj_arr['children']) && !empty($xml_obj_arr['children']))
	    {
	        foreach ($xml_obj_arr['children'] as $key_list)
	        {
	            if (isset($key_list['attributes']['Name']) && strlen($key_list['attributes']['Name']) > 0)
	            {
	                $key_val_array['key_val_list'][$key_list['attributes']['Name']] = (isset($key_list['content']) && strlen($key_list['content']) > 0) ? trim($key_list['content']) : '';
	            }
	        }
	        unset($xml_obj_arr['children']);
	    }
	    return $key_val_array;
	}
	
	

	public function status($message_id)
	{
		
	}

	public function is_ok($message_id,$code,$reason){

	}
}