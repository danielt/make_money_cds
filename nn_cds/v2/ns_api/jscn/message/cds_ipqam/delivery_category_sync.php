<?php
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load_old("nn_class/ftp/ftp.class.php");
\ns_core\m_load::load("ns_core.m_split_file");
class delivery_category_sync
{
    public $str_ftp_url='';
    
    
    public function __construct($ftp_url)
    {
        
        $this->str_ftp_url = $ftp_url;
    }
    
    public function init()
    {
        $obj_ftp = new nl_ftp(null,null,null,21,90,false,$this->str_ftp_url);
        $arr_parse = parse_url($this->str_ftp_url);
        $str_path = isset($arr_parse['path']) ? $arr_parse['path'] : '';
        $result_check = $obj_ftp->check_file_exsist($str_path);
        if($result_check['ret'] !=0)
        {
            return $result_check;
        }
        $arr_pathinfo = pathinfo($str_path);
        $temp_dir = "/temp/category_sync/".np_guid_rand().'.'.$arr_pathinfo['extension'];
        $result_make_dir = m_config::make_dir($temp_dir);
        if($result_make_dir['ret'] !=0)
        {
            return $result_make_dir;
        }
        $result_get = $obj_ftp->ftp_get_to_local($result_make_dir['data_info']['absolute_dir'],$str_path);
        if($result_get['ret'] !=0)
        {
            $result_get = m_config::get_curl_content_and_save($this->str_ftp_url, $result_make_dir['data_info']['absolute_dir']);
            if($result_get['ret'] !=0)
            {
                return $result_get;
            }
        }
        $arr_absolute_dir = pathinfo($result_make_dir['data_info']['absolute_dir']);
        //切割大文件
        $obj_split_file = new m_split_file(
            $result_make_dir['data_info']['absolute_dir'],$arr_pathinfo['extension'],$arr_absolute_dir['dirname'],__DIR__,1,100,$arr_absolute_dir['filename'].'_','',1
        );
        return $obj_split_file->run();
    }
    
    public function __destruct()
    {
        
    }
}

$obj_delivery_category_sync = new delivery_category_sync('ftp://jfdata:jfdata@172.26.2.37/category.csv');
$result = $obj_delivery_category_sync->init();
echo json_encode($result);