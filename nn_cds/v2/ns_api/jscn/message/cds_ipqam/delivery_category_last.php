<?php
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_core.m_config");
class delivery_category_last
{
    public $nns_hierarchy_limit = 3;
    
    public function __construct()
    {
        
    }
    
    public function init()
    {
        $sql_update = "update nns_sync_category set nns_is_last='0'";
        $result = nl_execute_by_db($sql_update, (m_config::get_dc()->db()));
        if(!$result)
        {
            return m_config::return_data(1,'SQL ERROR' . $sql_update);
        }
        $sql="select nns_hierarchy from nns_sync_category order by nns_hierarchy desc limit 1";
        $result = nl_sync_category::query_sql(m_config::get_dc(),$sql);
        if($result['ret'] !=0)
        {
            return $result;
        }
        $nns_hierarchy = (isset($result['data_info'][0]['nns_hierarchy']) && (int)$result['data_info'][0]['nns_hierarchy'] >0) ? (int)$result['data_info'][0]['nns_hierarchy'] : 0;
        if($nns_hierarchy <1)
        {
            return m_config::return_data(1,"栏目层级<1");
        }
        for ($i=1;$i<=$nns_hierarchy;$i++)
        {
            $result = $this->query_hierarchy($i);
            if($result['ret'] !=0)
            {
                return $result;
            }
            if(!isset($result['data_info']) || !is_array($result['data_info']) || empty($result['data_info']))
            {
                continue;
            }
            foreach ($result['data_info'] as $value)
            {
                $result_update = $this->updata_childs($value['nns_hierarchy']+1, $value['nns_category_id'], $value['nns_id']);
                if($result_update['ret'] !=0)
                {
                    return $result_update;
                }
            }
        }
        return m_config::return_data(0,"ok");
    }
    
    public function query_hierarchy($nns_hierarchy)
    {
        $sql="select * from nns_sync_category where nns_hierarchy='{$nns_hierarchy}'";
        return nl_sync_category::query_sql(m_config::get_dc(),$sql);
    }
    
    
    public function updata_childs($nns_child_hierarchy,$parent_id,$nns_id)
    {
        if($nns_child_hierarchy <= $this->nns_hierarchy_limit)
        {
            return m_config::return_data(0,'没有需要修改的数据');
        }
        $sql="select count(*) as count from nns_sync_category where nns_hierarchy='{$nns_child_hierarchy}' and nns_parent_id='{$parent_id}' order by nns_category_id asc ";
        $result = nl_sync_category::query_sql(m_config::get_dc(),$sql);
        if($result['ret'] !=0)
        {
            return $result;
        }
        $count = (isset($result['data_info'][0]['count']) && $result['data_info'][0]['count'] > 0) ? $result['data_info'][0]['count'] : 0;
        if($count <=0)
        {
            return nl_sync_category::edit(m_config::get_dc(), array('nns_is_last'=>1), $nns_id);
        }
        return m_config::return_data(0,'没有需要修改的数据');
    }
    
    public function __destruct()
    {
        
    }
}

$delivery_category_last = new delivery_category_last();
$result = $delivery_category_last->init();
echo json_encode($result);