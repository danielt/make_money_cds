<?php
// error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_model.message.message_explain");
\ns_core\m_load::load("ns_core.m_pinyin");
class message extends \ns_model\message\message_explain
{
    public function my_str_replace($str='')
    {
        $str = str_replace(' ', '', trim($str));
        if(strlen($str)<1)
        {
            return $str;
        }
        $str = str_replace('-', '', $str);
        if(strlen($str)<1)
        {
            return $str;
        }
        $str = str_replace(array(',','，','|','#','*'), '/', $str);
        $str = str_replace(' ', '', trim($str));
        return $str;
    }
    
    
    public function do_serice($arr_series)
    {
        $pinyin = m_pinyin::get_pinyin_letter($arr_series['nns_name']);
        $view_type = $arr_series['nns_index'] >1 ? '1' : '0';
        $add_series = array(
            'base_info' => array(
                'nns_name' => $arr_series['nns_name'],
                'nns_view_type' => $view_type,
                'nns_org_type' => '0',
                'nns_tag' => '26,',
                'nns_director' => $this->my_str_replace($arr_series['nns_director']),
                'nns_actor' => $this->my_str_replace($arr_series['nns_actor']),
                'nns_show_time' => (strlen($arr_series['nns_year']) > 0 && (int)$arr_series['nns_year'] > 1970 && (int)$arr_series['nns_year'] < 2020) ? (int)$arr_series['nns_year'] : date("Y"),
                'nns_view_len' => 0,
                'nns_all_index' => $arr_series['nns_index'],
                'nns_new_index' => $arr_series['nns_index']-1,
                'nns_area' => $this->my_str_replace($arr_series['nns_area']),
                'nns_image0' =>  '',
                'nns_image1' => '',
                'nns_image2' => '',
                'nns_image3' => '',
                'nns_image4' => '',
                'nns_image5' => '',
                'nns_summary' => '',
                'nns_remark' => '',
                'nns_category_id' => $arr_series['nns_category_id'],
                'nns_play_count' => '0',
                'nns_score_total' => '0',
                'nns_score_count' => '0',
                'nns_point' => '0',
                'nns_copyright_date' => (strlen($arr_series['nns_year']) > 0 && (int)$arr_series['nns_year'] > 1970 && (int)$arr_series['nns_year'] < 2020) ? (int)$arr_series['nns_year'] : date("Y"),
                'nns_asset_import_id' => $arr_series['nns_video_import_id'],
                'nns_pinyin' => $pinyin,
                'nns_pinyin_length' => strlen($pinyin),
                'nns_alias_name' => $arr_series['nns_name'],
                'nns_eng_name' => '',
                'nns_language' => '',
                'nns_text_lang' => '',
                'nns_producer' => '',
                'nns_screenwriter' => '',
                'nns_play_role' => '',
                'nns_copyright_range' => '',
                'nns_vod_part' => '',
                'nns_keyword' => '',
                'nns_import_source' => evn::get("project"),
                'nns_kind' => '',
                'nns_copyright' => '',
                'nns_clarity' => '',
                'nns_image_v' => '',
                'nns_image_s' => '',
                'nns_image_h' => '',
                'nns_cp_id' => $arr_series['nns_cp_id'],
                'nns_conf_info' => '',
                'nns_ext_url' => '',
            ), //基本信息（存储于nns_vod表中）
            'ex_info' => array(
                'svc_item_id' => '',
                'month_clicks' => '',
                'week_clicks' => '',
                'base_id' => '',
                'asset_path' => '',
                'ex_tag' => '',
                'full_spell' => '',
                'awards' => '',
                'year' => '',
                'play_time' => '',
                'channel' => '',
                'first_spell' => '',
            ), //扩展信息（存储于nns_vod_ex表中）
        );
        //字段待修改4-14
        return $this->vod_action('add', $add_series);
    }
    
    
    public function do_index($arr_index)
    {
        $add_index = array(
            'base_info' => array(
                'nns_name' => $arr_index['nns_name'],
                'nns_index' => $arr_index['nns_index']-1,
                'nns_time_len' => '',
                'nns_summary' => '',
                'nns_image' => '',
                'nns_play_count' => 0,
                'nns_score_total' => 0,
                'nns_score_count' => 0,
                'nns_video_import_id' => $arr_index['nns_video_import_id'],
                'nns_import_id' => $arr_index['nns_index_import_id'],
                'nns_import_source' => evn::get("project"),

                'nns_director' => $this->my_str_replace($arr_index['nns_director']),
                'nns_actor' => $this->my_str_replace($arr_index['nns_actor']),
                'nns_release_time' => (strlen($arr_index['nns_year']) > 0 && (int)$arr_index['nns_year'] > 1970 && (int)$arr_index['nns_year'] < 2020) ? (int)$arr_index['nns_year'] : date("Y"),
                'nns_update_time' => date("Y-m-d"),
                'nns_watch_focus' => '',
                'nns_cp_id' => $arr_index['nns_cp_id'],
                'nns_conf_info' => '',
                'nns_ext_url' => '',
            ),
            'ex_info' => array(
                'isintact' => '',
                'subordinate_name' => '',
                'initials' => '',
                'publisher' => '',
                'first_spell' => '',
                'caption_language' => '',
                'language' => '',
                'region' => '',
                'adaptor' => '',
                'sreach_key' => '',
                'event_tag' => '',
                'year' => '',
                'sort_name' => '',
            ),
        );
        //分集注入
        return $this->index_action('add', $add_index);
    }
    
    
    public function do_media($arr_media)
    {
        $add_media = array(
            'base_info' => array(
                'nns_name' => $arr_media['nns_name'],
                'nns_type' => 1,
                'nns_url' => '',
                'nns_tag' => '26,',
                'nns_mode' => '',
                'nns_kbps' => '',
                'nns_content_id' => $arr_media['nns_assetid'],
                'nns_content_state' => 0,
                'nns_filetype' => 'ts',
                'nns_play_count' => '0',
                'nns_score_total' => '0',
                'nns_score_count' => '0',
                'nns_video_import_id' => $arr_media['nns_video_import_id'],
                'nns_index_import_id' => $arr_media['nns_index_import_id'],
                'nns_import_id' => $arr_media['nns_media_import_id'],
                'nns_import_source' => evn::get("project"),
                'nns_dimensions' => '2D',
                'nns_ext_url' => '',
                'nns_file_size' => 0,
                'nns_file_time_len' => 0,
                'nns_file_frame_rate' => 0,
                'nns_file_resolution' => 0,
                'nns_cp_id' => $arr_media['nns_cp_id'],
                'nns_ext_info' => '',
                'nns_drm_enabled' =>  0,
                'nns_drm_encrypt_solution' => '',
                'nns_drm_ext_info' => '',
                'nns_domain' => 0,
                'nns_media_type' => 1,
                'nns_original_live_id' => '',
                'nns_start_time' => '',
                'nns_media_service' => 'HTTP',
                'nns_conf_info' => '',
                'nns_encode_flag' => 0,
                'nns_live_to_media' => '',
                'nns_media_service_type' => '',
            ),
            'ex_info' => array(
                'file_hash' => '',
                'file_width' => '',
                'file_height' => '',
                'file_scale' => '',
                'file_coding' => '',
            ),
        );
        return  $this->media_action('add', $add_media);
    }
    
    
    public function init_movie()
    {
        $sql="SELECT * FROM `nns_thrid_part_asset_1` where nns_state='1' and nns_index <1 ORDER BY `nns_name` DESC, `nns_index` DESC LIMIT 1000";
        $result = nl_query_by_db($sql, m_config::get_dc()->db());
        if(!$result || !is_array($result) || empty($result))
        {
            return m_config::return_data(0,'ok,查无数据');
        }
        foreach ($result as $value)
        {
            $value['nns_name'] = $this->get_movie_name($value['nns_name']);
            if(strlen($value['nns_name']) <1)
            {
                continue;
            }
            $value['nns_index'] = (int)$value['nns_index'] <1 ? 1 : (int)$value['nns_index'];
            $value['nns_cp_id']='cds_ipqam';
            $asset_category = '单集';
            $do_category = array(
                'base_info'=>array(
                    'nns_name'=>$asset_category,   //栏目名称支持多层目录，多层目录的时候 /分割
                    'nns_import_category_id'=>'', //栏目名称支持多层目录，多层目录的时候 /分割
                    'nns_cp_id'=>$value['nns_cp_id'],
                    'nns_import_parent_category_id'=>'',
                    'nns_video_type'=>'0',  //媒资类型  0 点播 | 1 直播
                ),
            );
            $result_cate = $this->category_action('add', $do_category);
            if($result_cate['ret'] !=0)
            {
                return $result;
            }
            if(!isset($result_cate['data_info']['base_info']['nns_id']) || strlen($result_cate['data_info']['base_info']['nns_id']) <1)
            {
                return m_config::return_data(1,'创建栏目失败');
            }
            $value['nns_category_id'] = $result_cate['data_info']['base_info']['nns_id'];
            $arr_series = $value;
            $arr_series['nns_video_import_id'] = MD5($arr_series['nns_name']);
            $result_video = $this->do_serice($arr_series);
            $arr_index = $arr_series;
            $arr_index['nns_name'] = $arr_index['nns_name']."第{$value['nns_index']}集";
            $arr_index['nns_index_import_id'] = MD5($arr_index['nns_name']);
            $result_index = $this->do_index($arr_index);
            $arr_media = $arr_index;
            $arr_media['nns_name'] = $arr_index['nns_name']."{$value['nns_id']}";
            $arr_media['nns_media_import_id'] = MD5($arr_media['nns_name']);
            $result_media = $this->do_media($arr_media);
            nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='0' where nns_id='{$value['nns_id']}'", m_config::get_dc()->db());
        }
    }
    
    
    public function init_serice()
    {
        $sql="SELECT * FROM `nns_thrid_part_asset_1` where nns_state='1' and nns_index >=1 ORDER BY `nns_name` DESC, `nns_index` DESC LIMIT 4000";
        $result = nl_query_by_db($sql, m_config::get_dc()->db());
        if(!$result || !is_array($result) || empty($result))
        {
            return m_config::return_data(0,'ok,查无数据');
        }
        $last_data = null;
        $count = count($result);
        $i = 0;
        foreach ($result as $value)
        {
            $i++;
            if($i >= $count)
            {
                if(isset($video_id) && $video_id == md5($this->get_name($value['nns_name'])))
                {
                    unset($last_data[$video_id]);
                }
                break;
            }
            $str_name = $this->get_name($value['nns_name']);
            if(strlen($str_name) <1)
            {
                nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='2' where nns_id='{$value['nns_id']}'", m_config::get_dc()->db());
                continue;
            }
            $value['nns_cp_id'] = 'cds_ipqam';
            $video_id = md5($str_name);
            $value['nns_index'] = (int)$value['nns_index'] <1 ? 1 : (int)$value['nns_index'];
            if(!isset($last_data[$video_id]['video']))
            {
                $video_value = $value;
                $video_value['nns_name'] = $str_name;
                $video_value['nns_index'] = (int)$value['nns_index'] <1 ? 1 : (int)$value['nns_index'];
                $video_value['nns_video_import_id'] = $video_id;
                $last_data[$video_id]['video'] = $video_value;
            }
            $index_value = $value;
            $index_value['nns_name'] = $str_name."第{$index_value['nns_index']}集";
            $index_value['nns_index_import_id'] = MD5($index_value['nns_name']);
            $index_value['nns_video_import_id'] = $video_id;
            $last_data[$video_id]['index'][$value['nns_index']] = $index_value;
            $media_value = $index_value;
            $media_value['nns_name'] = $media_value['nns_name']."_{$media_value['nns_id']}";
            $media_value['nns_media_import_id'] = MD5($media_value['nns_name']);
            $last_data[$video_id]['media'][] = $media_value;
            $last_data[$video_id]['ids'][] = $value['nns_id'];
        }
        if(!is_array($last_data) || empty($last_data))
        {
            return ;
        }
        foreach ($last_data as $in_key=>$in_value)
        {
            if(!isset($in_value['media']) || empty($in_value['media']) || !is_array($in_value['media']) || !isset($in_value['index']) || empty($in_value['index']) || !is_array($in_value['index']))
            {
                if(isset($in_value['ids']) && !empty($in_value['ids']) && is_array($in_value['ids']))
                {
                    nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='3' where nns_id in('".implode("','", $in_value['ids'])."')", m_config::get_dc()->db());
                }
                continue;
            }
            if(count($in_value['index']) !=$in_value['video']['nns_index'])
            {
                if(isset($in_value['ids']) && !empty($in_value['ids']) && is_array($in_value['ids']))
                {
                    nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='3' where nns_id in('".implode("','", $in_value['ids'])."')", m_config::get_dc()->db());
                }
                continue;
            }
            $asset_category = '多集';
            $do_category = array(
                'base_info'=>array(
                    'nns_name'=>$asset_category,   //栏目名称支持多层目录，多层目录的时候 /分割
                    'nns_import_category_id'=>'', //栏目名称支持多层目录，多层目录的时候 /分割
                    'nns_cp_id'=>$in_value['video']['nns_cp_id'],
                    'nns_import_parent_category_id'=>'',
                    'nns_video_type'=>'0',  //媒资类型  0 点播 | 1 直播
                ),
            );
            $result_cate = $this->category_action('add', $do_category);
            if($result_cate['ret'] !=0)
            {
                return $result;
            }
            if(!isset($result_cate['data_info']['base_info']['nns_id']) || strlen($result_cate['data_info']['base_info']['nns_id']) <1)
            {
                return m_config::return_data(1,'创建栏目失败');
            }
            $in_value['video']['nns_category_id'] = $result_cate['data_info']['base_info']['nns_id'];
            $result_video = $this->do_serice($in_value['video']);
            foreach ($in_value['index'] as $in_index_value)
            {
                $result_index = $this->do_index($in_index_value);
            }
            foreach ($in_value['media'] as $in_media_value)
            {
                $result_index = $this->do_media($in_media_value);
            }
            if(isset($in_value['ids']) && !empty($in_value['ids']) && is_array($in_value['ids']))
            {
                nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='0' where nns_id in('".implode("','", $in_value['ids'])."')", m_config::get_dc()->db());
            }
        }
        return ;
    }
    
    
    public function init_serice_v2()
    {
        $sql="SELECT * FROM `nns_thrid_part_asset_1` where nns_state='1' and nns_index >=1 and nns_name not REGEXP '[a-zA-Z]|[0-9]' ORDER BY `nns_name` DESC, `nns_index` DESC LIMIT 4000";
        $result = nl_query_by_db($sql, m_config::get_dc()->db());
        if(!$result || !is_array($result) || empty($result))
        {
            return m_config::return_data(0,'ok,查无数据');
        }
        $last_data = null;
        $count = count($result);
        $i = 0;
        foreach ($result as $value)
        {
            $i++;
            if($i >= $count)
            {
                if(isset($video_id) && $video_id == md5($this->get_name($value['nns_name'])))
                {
                    unset($last_data[$video_id]);
                }
                continue;
            }
            $str_name = $value['nns_name'];
            if(strlen($str_name) <1)
            {
                nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='2' where nns_id='{$value['nns_id']}'", m_config::get_dc()->db());
                continue;
            }
            $value['nns_cp_id'] = 'cds_ipqam';
            $video_id = md5($str_name);
            $value['nns_index'] = (int)$value['nns_index'] <1 ? 1 : (int)$value['nns_index'];
            if(!isset($last_data[$video_id]['video']))
            {
                $video_value = $value;
                $video_value['nns_name'] = $str_name;
                $video_value['nns_index'] = (int)$value['nns_index'] <1 ? 1 : (int)$value['nns_index'];
                $video_value['nns_video_import_id'] = $video_id;
                $last_data[$video_id]['video'] = $video_value;
            }
            $index_value = $value;
            $index_value['nns_name'] = $str_name."第{$index_value['nns_index']}集";
            $index_value['nns_index_import_id'] = MD5($index_value['nns_name']);
            $index_value['nns_video_import_id'] = $video_id;
            $last_data[$video_id]['index'][$value['nns_index']] = $index_value;
            $media_value = $index_value;
            $media_value['nns_name'] = $media_value['nns_name']."_{$media_value['nns_id']}";
            $media_value['nns_media_import_id'] = MD5($media_value['nns_name']);
            $last_data[$video_id]['media'][] = $media_value;
            $last_data[$video_id]['ids'][] = $value['nns_id'];
        }
        if(!is_array($last_data) || empty($last_data))
        {
            return ;
        }
        foreach ($last_data as $in_key=>$in_value)
        {
            if(!isset($in_value['media']) || empty($in_value['media']) || !is_array($in_value['media']) || !isset($in_value['index']) || empty($in_value['index']) || !is_array($in_value['index']))
            {
                if(isset($in_value['ids']) && !empty($in_value['ids']) && is_array($in_value['ids']))
                {
                    nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='3' where nns_id in('".implode("','", $in_value['ids'])."')", m_config::get_dc()->db());
                }
                continue;
            }
            $flag = false;
            if(count($in_value['index']) == 1)
            {
                $flag = true;
                if(isset($in_value['ids']) && !empty($in_value['ids']) && is_array($in_value['ids']))
                {
                    nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='3' where nns_id in('".implode("','", $in_value['ids'])."')", m_config::get_dc()->db());
                }
                continue;
            }
            $asset_category = '多集拆分';
            $do_category = array(
                'base_info'=>array(
                    'nns_name'=>$asset_category,   //栏目名称支持多层目录，多层目录的时候 /分割
                    'nns_import_category_id'=>'', //栏目名称支持多层目录，多层目录的时候 /分割
                    'nns_cp_id'=>$in_value['video']['nns_cp_id'],
                    'nns_import_parent_category_id'=>'',
                    'nns_video_type'=>'0',  //媒资类型  0 点播 | 1 直播
                ),
            );
            $result_cate = $this->category_action('add', $do_category);
            if($result_cate['ret'] !=0)
            {
                return $result;
            }
            if(!isset($result_cate['data_info']['base_info']['nns_id']) || strlen($result_cate['data_info']['base_info']['nns_id']) <1)
            {
                return m_config::return_data(1,'创建栏目失败');
            }
            $in_value['video']['nns_category_id'] = $result_cate['data_info']['base_info']['nns_id'];
            if($flag)
            {
                $in_value['video']['nns_index'] = 1;
            }
            $result_video = $this->do_serice($in_value['video']);
            foreach ($in_value['index'] as $in_index_value)
            {
                if($flag)
                {
                    $in_index_value['nns_index'] = 1;
                }
                $result_index = $this->do_index($in_index_value);
            }
            foreach ($in_value['media'] as $in_media_value)
            {
                if($flag)
                {
                    $in_media_value['nns_index'] = 1;
                }
                $result_index = $this->do_media($in_media_value);
            }
            if(isset($in_value['ids']) && !empty($in_value['ids']) && is_array($in_value['ids']))
            {
                nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='0' where nns_id in('".implode("','", $in_value['ids'])."')", m_config::get_dc()->db());
            }
        }
        return ;
    }
    
    
    
    public function init_serice_v3()
    {
        $sql="select * from (SELECT count(*) as count,nns_thrid_part_asset_1.* FROM `nns_thrid_part_asset_1` where nns_state='1' and nns_index >=1 and nns_name not REGEXP '[a-zA-Z]|[0-9]' GROUP BY nns_name) as temp where temp.count =1 limit 1000";
        $result = nl_query_by_db($sql, m_config::get_dc()->db());
        if(!$result || !is_array($result) || empty($result))
        {
            return m_config::return_data(0,'ok,查无数据');
        }
        $last_data = null;
        $count = count($result);
        $i = 0;
        foreach ($result as $value)
        {
            $str_name = $value['nns_name'];
            if(strlen($str_name) <1)
            {
                nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='2' where nns_id='{$value['nns_id']}'", m_config::get_dc()->db());
                continue;
            }
            $value['nns_cp_id'] = 'cds_ipqam';
            $video_id = md5($str_name);
            $value['nns_index'] = 1;
//             $value['nns_index'] = (int)$value['nns_index'] <1 ? 1 : (int)$value['nns_index'];
            if(!isset($last_data[$video_id]['video']))
            {
                $video_value = $value;
                $video_value['nns_name'] = $str_name;
                $video_value['nns_index'] = (int)$value['nns_index'] <1 ? 1 : (int)$value['nns_index'];
                $video_value['nns_video_import_id'] = $video_id;
                $last_data[$video_id]['video'] = $video_value;
            }
            $index_value = $value;
            $index_value['nns_name'] = $str_name."第{$index_value['nns_index']}集";
            $index_value['nns_index_import_id'] = MD5($index_value['nns_name']);
            $index_value['nns_video_import_id'] = $video_id;
            $last_data[$video_id]['index'][$value['nns_index']] = $index_value;
            $media_value = $index_value;
            $media_value['nns_name'] = $media_value['nns_name']."_{$media_value['nns_id']}";
            $media_value['nns_media_import_id'] = MD5($media_value['nns_name']);
            $last_data[$video_id]['media'][] = $media_value;
            $last_data[$video_id]['ids'][] = $value['nns_id'];
        }
        if(!is_array($last_data) || empty($last_data))
        {
            return ;
        }
        foreach ($last_data as $in_key=>$in_value)
        {
            if(!isset($in_value['media']) || empty($in_value['media']) || !is_array($in_value['media']) || !isset($in_value['index']) || empty($in_value['index']) || !is_array($in_value['index']))
            {
                if(isset($in_value['ids']) && !empty($in_value['ids']) && is_array($in_value['ids']))
                {
                    nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='3' where nns_id in('".implode("','", $in_value['ids'])."')", m_config::get_dc()->db());
                }
                continue;
            }
            $asset_category = '单集拆分';
            $do_category = array(
                'base_info'=>array(
                    'nns_name'=>$asset_category,   //栏目名称支持多层目录，多层目录的时候 /分割
                    'nns_import_category_id'=>'', //栏目名称支持多层目录，多层目录的时候 /分割
                    'nns_cp_id'=>$in_value['video']['nns_cp_id'],
                    'nns_import_parent_category_id'=>'',
                    'nns_video_type'=>'0',  //媒资类型  0 点播 | 1 直播
                ),
            );
            $result_cate = $this->category_action('add', $do_category);
            if($result_cate['ret'] !=0)
            {
                return $result;
            }
            if(!isset($result_cate['data_info']['base_info']['nns_id']) || strlen($result_cate['data_info']['base_info']['nns_id']) <1)
            {
                return m_config::return_data(1,'创建栏目失败');
            }
            $in_value['video']['nns_category_id'] = $result_cate['data_info']['base_info']['nns_id'];
            $result_video = $this->do_serice($in_value['video']);
            foreach ($in_value['index'] as $in_index_value)
            {
                $result_index = $this->do_index($in_index_value);
            }
            foreach ($in_value['media'] as $in_media_value)
            {
                $result_index = $this->do_media($in_media_value);
            }
            if(isset($in_value['ids']) && !empty($in_value['ids']) && is_array($in_value['ids']))
            {
                nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='0' where nns_id in('".implode("','", $in_value['ids'])."')", m_config::get_dc()->db());
            }
        }
        return ;
    }
    
    
    public function init_serice_v4()
    {
        $sql="SELECT * FROM `nns_thrid_part_asset_1` where nns_state='1' and nns_index >=1 and length(nns_name)=char_length(nns_name) limit 4000";
        $result = nl_query_by_db($sql, m_config::get_dc()->db());
        if(!$result || !is_array($result) || empty($result))
        {
            return m_config::return_data(0,'ok,查无数据');
        }
        $last_data = null;
        $count = count($result);
        $i = 0;
        foreach ($result as $value)
        {
            $str_name = $value['nns_name'];
            if(strlen($str_name) <1)
            {
                nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='2' where nns_id='{$value['nns_id']}'", m_config::get_dc()->db());
                continue;
            }
            $value['nns_cp_id'] = 'cds_ipqam';
            $video_id = md5($str_name);
            $value['nns_index'] = 1;
            //             $value['nns_index'] = (int)$value['nns_index'] <1 ? 1 : (int)$value['nns_index'];
            if(!isset($last_data[$video_id]['video']))
            {
                $video_value = $value;
                $video_value['nns_name'] = $str_name;
                $video_value['nns_index'] = (int)$value['nns_index'] <1 ? 1 : (int)$value['nns_index'];
                $video_value['nns_video_import_id'] = $video_id;
                $last_data[$video_id]['video'] = $video_value;
            }
            $index_value = $value;
            $index_value['nns_name'] = $str_name."第{$index_value['nns_index']}集";
            $index_value['nns_index_import_id'] = MD5($index_value['nns_name']);
            $index_value['nns_video_import_id'] = $video_id;
            $last_data[$video_id]['index'][$value['nns_index']] = $index_value;
            $media_value = $index_value;
            $media_value['nns_name'] = $media_value['nns_name']."_{$media_value['nns_id']}";
            $media_value['nns_media_import_id'] = MD5($media_value['nns_name']);
            $last_data[$video_id]['media'][] = $media_value;
            $last_data[$video_id]['ids'][] = $value['nns_id'];
        }
        if(!is_array($last_data) || empty($last_data))
        {
            return ;
        }
        foreach ($last_data as $in_key=>$in_value)
        {
            if(!isset($in_value['media']) || empty($in_value['media']) || !is_array($in_value['media']) || !isset($in_value['index']) || empty($in_value['index']) || !is_array($in_value['index']))
            {
                if(isset($in_value['ids']) && !empty($in_value['ids']) && is_array($in_value['ids']))
                {
                    nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='3' where nns_id in('".implode("','", $in_value['ids'])."')", m_config::get_dc()->db());
                }
                continue;
            }
            $asset_category = '全英文';
            $do_category = array(
                'base_info'=>array(
                    'nns_name'=>$asset_category,   //栏目名称支持多层目录，多层目录的时候 /分割
                    'nns_import_category_id'=>'', //栏目名称支持多层目录，多层目录的时候 /分割
                    'nns_cp_id'=>$in_value['video']['nns_cp_id'],
                    'nns_import_parent_category_id'=>'',
                    'nns_video_type'=>'0',  //媒资类型  0 点播 | 1 直播
                ),
            );
            $result_cate = $this->category_action('add', $do_category);
            if($result_cate['ret'] !=0)
            {
                return $result;
            }
            if(!isset($result_cate['data_info']['base_info']['nns_id']) || strlen($result_cate['data_info']['base_info']['nns_id']) <1)
            {
                return m_config::return_data(1,'创建栏目失败');
            }
            $in_value['video']['nns_category_id'] = $result_cate['data_info']['base_info']['nns_id'];
            $result_video = $this->do_serice($in_value['video']);
            foreach ($in_value['index'] as $in_index_value)
            {
                $result_index = $this->do_index($in_index_value);
            }
            foreach ($in_value['media'] as $in_media_value)
            {
                $result_index = $this->do_media($in_media_value);
            }
            if(isset($in_value['ids']) && !empty($in_value['ids']) && is_array($in_value['ids']))
            {
                nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='0' where nns_id in('".implode("','", $in_value['ids'])."')", m_config::get_dc()->db());
            }
        }
        return ;
    }
    
    
    public function init_serice_v5()
    {
        $sql="SELECT * FROM `nns_thrid_part_asset_1` where nns_state='1' and nns_index >=1 ORDER BY `nns_name` DESC, `nns_index` DESC LIMIT 4000";
        $result = nl_query_by_db($sql, m_config::get_dc()->db());
        if(!$result || !is_array($result) || empty($result))
        {
            return m_config::return_data(0,'ok,查无数据');
        }
        $last_data = null;
        $count = count($result);
        $i = 0;
        foreach ($result as $value)
        {
            $i++;
            if($i >= $count)
            {
                if(isset($video_id) && $video_id == md5($this->get_name($value['nns_name'])))
                {
                    unset($last_data[$video_id]);
                }
                break;
            }
            $str_name = $this->get_name_v2($value['nns_name']);
            if(strlen($str_name) <1)
            {
                nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='2' where nns_id='{$value['nns_id']}'", m_config::get_dc()->db());
                continue;
            }
            $value['nns_cp_id'] = 'cds_ipqam';
            $video_id = md5($str_name);
            $value['nns_index'] = (int)$value['nns_index'] <1 ? 1 : (int)$value['nns_index'];
            if(!isset($last_data[$video_id]['video']))
            {
                $video_value = $value;
                $video_value['nns_name'] = $str_name;
                $video_value['nns_index'] = (int)$value['nns_index'] <1 ? 1 : (int)$value['nns_index'];
                $video_value['nns_video_import_id'] = $video_id;
                $last_data[$video_id]['video'] = $video_value;
            }
            $index_value = $value;
            $index_value['nns_name'] = $str_name."第{$index_value['nns_index']}集";
            $index_value['nns_index_import_id'] = MD5($index_value['nns_name']);
            $index_value['nns_video_import_id'] = $video_id;
            $last_data[$video_id]['index'][$value['nns_index']] = $index_value;
            $media_value = $index_value;
            $media_value['nns_name'] = $media_value['nns_name']."_{$media_value['nns_id']}";
            $media_value['nns_media_import_id'] = MD5($media_value['nns_name']);
            $last_data[$video_id]['media'][] = $media_value;
            $last_data[$video_id]['ids'][] = $value['nns_id'];
        }
        if(!is_array($last_data) || empty($last_data))
        {
            return ;
        }
        foreach ($last_data as $in_key=>$in_value)
        {
            if(!isset($in_value['media']) || empty($in_value['media']) || !is_array($in_value['media']) || !isset($in_value['index']) || empty($in_value['index']) || !is_array($in_value['index']))
            {
                if(isset($in_value['ids']) && !empty($in_value['ids']) && is_array($in_value['ids']))
                {
                    nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='3' where nns_id in('".implode("','", $in_value['ids'])."')", m_config::get_dc()->db());
                }
                continue;
            }
            if(count($in_value['index'])<=1)
            {
                if(isset($in_value['ids']) && !empty($in_value['ids']) && is_array($in_value['ids']))
                {
                    nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='3' where nns_id in('".implode("','", $in_value['ids'])."')", m_config::get_dc()->db());
                }
                continue;
            }
            $asset_category = '多集缺集';
            $do_category = array(
                'base_info'=>array(
                    'nns_name'=>$asset_category,   //栏目名称支持多层目录，多层目录的时候 /分割
                    'nns_import_category_id'=>'', //栏目名称支持多层目录，多层目录的时候 /分割
                    'nns_cp_id'=>$in_value['video']['nns_cp_id'],
                    'nns_import_parent_category_id'=>'',
                    'nns_video_type'=>'0',  //媒资类型  0 点播 | 1 直播
                ),
            );
            $result_cate = $this->category_action('add', $do_category);
            if($result_cate['ret'] !=0)
            {
                return $result;
            }
            if(!isset($result_cate['data_info']['base_info']['nns_id']) || strlen($result_cate['data_info']['base_info']['nns_id']) <1)
            {
                return m_config::return_data(1,'创建栏目失败');
            }
            $in_value['video']['nns_category_id'] = $result_cate['data_info']['base_info']['nns_id'];
            $result_video = $this->do_serice($in_value['video']);
            foreach ($in_value['index'] as $in_index_value)
            {
                $result_index = $this->do_index($in_index_value);
            }
            foreach ($in_value['media'] as $in_media_value)
            {
                $result_index = $this->do_media($in_media_value);
            }
            if(isset($in_value['ids']) && !empty($in_value['ids']) && is_array($in_value['ids']))
            {
                nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='0' where nns_id in('".implode("','", $in_value['ids'])."')", m_config::get_dc()->db());
            }
        }
        return ;
    }
    
    
    public function init_serice_v6()
    {
        $sql="select * from `nns_thrid_part_asset_1` where nns_state='1' and nns_index >=2 and nns_name REGEXP '[0-9]' ORDER BY `nns_name`, `nns_index` desc limit 2000";
        $result = nl_query_by_db($sql, m_config::get_dc()->db());
        if(!$result || !is_array($result) || empty($result))
        {
            return m_config::return_data(0,'ok,查无数据');
        }
        $last_data = null;
        $count = count($result);
        $i = 0;
        foreach ($result as $value)
        {
            $str_name = $this->get_name_v3($value['nns_name']);
            $arr_name = explode('|#|', $str_name);
            $arr_name = is_array($arr_name) ? array_filter($arr_name) : null;
            if(!is_array($arr_name) || empty($arr_name))
            {
                nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='2' where nns_id='{$value['nns_id']}'", m_config::get_dc()->db());
                continue;
            }
            if(count($arr_name) !=2)
            {
                nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='2' where nns_id='{$value['nns_id']}'", m_config::get_dc()->db());
                continue;
            }
            $str_name = $arr_name[0];
            if(strlen($str_name) <1)
            {
                nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='2' where nns_id='{$value['nns_id']}'", m_config::get_dc()->db());
                continue;
            }
            $value['nns_cp_id'] = 'cds_ipqam';
            $video_id = md5($str_name);
            $value['nns_index'] = (int)$value['nns_index'] <2 ? 1 : (int)$value['nns_index'];
            $value['nns_index']--;
            if(!isset($last_data[$video_id]['video']))
            {
                $video_value = $value;
                $video_value['nns_name'] = $str_name;
                $video_value['nns_index'] = (int)$value['nns_index'] <1 ? 1 : (int)$value['nns_index'];
                $video_value['nns_video_import_id'] = $video_id;
                $last_data[$video_id]['video'] = $video_value;
            }
            $index_value = $value;
            $index_value['nns_name'] = $str_name.$arr_name[1]."第{$index_value['nns_index']}集";
            $index_value['nns_index_import_id'] = MD5($index_value['nns_name']);
            $index_value['nns_video_import_id'] = $video_id;
            $last_data[$video_id]['index'][$value['nns_index']] = $index_value;
            $media_value = $index_value;
            $media_value['nns_name'] = $media_value['nns_name']."_{$media_value['nns_id']}";
            $media_value['nns_media_import_id'] = MD5($media_value['nns_name']);
            $last_data[$video_id]['media'][] = $media_value;
            $last_data[$video_id]['ids'][] = $value['nns_id'];
        }
        if(!is_array($last_data) || empty($last_data))
        {
            return ;
        }
        foreach ($last_data as $in_key=>$in_value)
        {
            if(!isset($in_value['media']) || empty($in_value['media']) || !is_array($in_value['media']) || !isset($in_value['index']) || empty($in_value['index']) || !is_array($in_value['index']))
            {
                if(isset($in_value['ids']) && !empty($in_value['ids']) && is_array($in_value['ids']))
                {
                    nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='3' where nns_id in('".implode("','", $in_value['ids'])."')", m_config::get_dc()->db());
                }
                continue;
            }
            $asset_category = '多集断点';
            $do_category = array(
                'base_info'=>array(
                    'nns_name'=>$asset_category,   //栏目名称支持多层目录，多层目录的时候 /分割
                    'nns_import_category_id'=>'', //栏目名称支持多层目录，多层目录的时候 /分割
                    'nns_cp_id'=>$in_value['video']['nns_cp_id'],
                    'nns_import_parent_category_id'=>'',
                    'nns_video_type'=>'0',  //媒资类型  0 点播 | 1 直播
                ),
            );
            $result_cate = $this->category_action('add', $do_category);
            if($result_cate['ret'] !=0)
            {
                return $result;
            }
            if(!isset($result_cate['data_info']['base_info']['nns_id']) || strlen($result_cate['data_info']['base_info']['nns_id']) <1)
            {
                return m_config::return_data(1,'创建栏目失败');
            }
            $in_value['video']['nns_category_id'] = $result_cate['data_info']['base_info']['nns_id'];
            $result_video = $this->do_serice($in_value['video']);
            foreach ($in_value['index'] as $in_index_value)
            {
                $result_index = $this->do_index($in_index_value);
            }
            foreach ($in_value['media'] as $in_media_value)
            {
                $result_index = $this->do_media($in_media_value);
            }
            if(isset($in_value['ids']) && !empty($in_value['ids']) && is_array($in_value['ids']))
            {
                nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='0' where nns_id in('".implode("','", $in_value['ids'])."')", m_config::get_dc()->db());
            }
        }
        return ;
    }
    
    
    public function init_serice_v7()
    {
        $sql="select * from `nns_thrid_part_asset_1` where nns_state='1' ORDER BY `nns_name`, `nns_index` desc limit 2000";
        $result = nl_query_by_db($sql, m_config::get_dc()->db());
        if(!$result || !is_array($result) || empty($result))
        {
            return m_config::return_data(0,'ok,查无数据');
        }
        $last_data = null;
        $count = count($result);
        $i = 0;
        foreach ($result as $value)
        {
            $str_name = $value['nns_name'];
            if(strlen($str_name) <1)
            {
                nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='2' where nns_id='{$value['nns_id']}'", m_config::get_dc()->db());
                continue;
            }
            $value['nns_cp_id'] = 'cds_ipqam';
            $video_id = md5($str_name);
            $value['nns_index'] = 1;
            if(!isset($last_data[$video_id]['video']))
            {
                $video_value = $value;
                $video_value['nns_name'] = $str_name;
                $video_value['nns_index'] = (int)$value['nns_index'] <1 ? 1 : (int)$value['nns_index'];
                $video_value['nns_video_import_id'] = $video_id;
                $last_data[$video_id]['video'] = $video_value;
            }
            $index_value = $value;
            $index_value['nns_name'] = $str_name."第{$index_value['nns_index']}集";
            $index_value['nns_index_import_id'] = MD5($index_value['nns_name']);
            $index_value['nns_video_import_id'] = $video_id;
            $last_data[$video_id]['index'][$value['nns_index']] = $index_value;
            $media_value = $index_value;
            $media_value['nns_name'] = $media_value['nns_name']."_{$media_value['nns_id']}";
            $media_value['nns_media_import_id'] = MD5($media_value['nns_name']);
            $last_data[$video_id]['media'][] = $media_value;
            $last_data[$video_id]['ids'][] = $value['nns_id'];
        }
        if(!is_array($last_data) || empty($last_data))
        {
            return ;
        }
        foreach ($last_data as $in_key=>$in_value)
        {
            if(!isset($in_value['media']) || empty($in_value['media']) || !is_array($in_value['media']) || !isset($in_value['index']) || empty($in_value['index']) || !is_array($in_value['index']))
            {
                if(isset($in_value['ids']) && !empty($in_value['ids']) && is_array($in_value['ids']))
                {
                    nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='3' where nns_id in('".implode("','", $in_value['ids'])."')", m_config::get_dc()->db());
                }
                continue;
            }
            $asset_category = '杂乱数据';
            $do_category = array(
                'base_info'=>array(
                    'nns_name'=>$asset_category,   //栏目名称支持多层目录，多层目录的时候 /分割
                    'nns_import_category_id'=>'', //栏目名称支持多层目录，多层目录的时候 /分割
                    'nns_cp_id'=>$in_value['video']['nns_cp_id'],
                    'nns_import_parent_category_id'=>'',
                    'nns_video_type'=>'0',  //媒资类型  0 点播 | 1 直播
                ),
            );
            $result_cate = $this->category_action('add', $do_category);
            if($result_cate['ret'] !=0)
            {
                return $result;
            }
            if(!isset($result_cate['data_info']['base_info']['nns_id']) || strlen($result_cate['data_info']['base_info']['nns_id']) <1)
            {
                return m_config::return_data(1,'创建栏目失败');
            }
            $in_value['video']['nns_category_id'] = $result_cate['data_info']['base_info']['nns_id'];
            $result_video = $this->do_serice($in_value['video']);
            foreach ($in_value['index'] as $in_index_value)
            {
                $result_index = $this->do_index($in_index_value);
            }
            foreach ($in_value['media'] as $in_media_value)
            {
                $result_index = $this->do_media($in_media_value);
            }
            if(isset($in_value['ids']) && !empty($in_value['ids']) && is_array($in_value['ids']))
            {
                nl_execute_by_db("update nns_thrid_part_asset_1 set nns_state='0' where nns_id in('".implode("','", $in_value['ids'])."')", m_config::get_dc()->db());
            }
        }
        return ;
    }
    
    
    public function get_movie_name($str_name)
    {
        return str_replace(array(' '), '', $str_name);
    }
    
    public function get_name($str_name)
    {
        return str_replace(array('0','1','2','3','4','5','6','7','8','9',' '), '', $str_name);
    }
    
    public function get_name_v2($str_name)
    {
        return str_replace(array('0','1','2','3','4','5','6','7','8','9',' '), '', preg_replace('/[a-zA-Z]/i', '', $str_name));
    }
    
    public function get_name_v3($str_name)
    {
        return str_replace(array(' '), '', preg_replace('/[0-9]+/i', '|#|', $str_name));
    }
}

$obj = new message();
$obj->init_movie();