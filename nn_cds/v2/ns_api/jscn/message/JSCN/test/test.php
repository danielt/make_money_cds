<?php 
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))."/common.php";
include_once dirname(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))))."/nn_cms_config/nn_cms_global.php";
include_once dirname(__FILE__)."/client.php";
\ns_core\m_load::load("ns_core.m_config");
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
array_pop($arr_dir);
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
$project_name = evn::get('project');
if(isset($_REQUEST['action']) && $_REQUEST['action']=='soap')
{
    include_once dirname(__FILE__).'/client.php';
    $XMLURLQC=$Encrypt=null;
    if(!isset($_REQUEST['CMSID']) || strlen($_REQUEST['CMSID']) <1)
    {
        echo "<script>alert('请选择CMSID');history.go(-1);</script>";die;
    }
    if(!isset($_REQUEST['SOPID']) || strlen($_REQUEST['SOPID']) <1)
    {
        echo "<script>alert('请输入SOPID');history.go(-1);</script>";die;
    }
    if(!isset($_REQUEST['content']) || strlen($_REQUEST['content']) <1)
    {
        echo "<script>alert('请输入注入内容');history.go(-1);</script>";die;
    }
    $CMSID = $_REQUEST['CMSID'];
    $SOPID = $_REQUEST['SOPID'];
    $content = $_REQUEST['content'];
    $file_name = np_guid_rand();
    if(!m_config::is_xml($_REQUEST['content']))
    {
        return "<script>alert('内容非XML');history.go(-1);</script>";die;
    }
    $xml_simulate_path = 'upload/simulate/'.$CMSID.'/'.date('Ymd').'/'.$file_name.".xml";
    $result_make_dir = m_config::make_dir($xml_simulate_path);
    if($result_make_dir['ret'] !=0)
    {
        return "<script>alert('".$result_make_dir['ret']."');history.go(-1);</script>";die;
    }
    $absolute_dir = $result_make_dir['data_info']['absolute_dir'];
    $CorrelateID = (isset($_REQUEST['CorrelateID']) && strlen($_REQUEST['CorrelateID'])>0) ? $_REQUEST['CorrelateID'] : np_guid_rand();
    try{
        file_put_contents($absolute_dir, $content);
    }catch (Exception $exception)
    {
        var_dump($exception->getMessage());die;
    }
    global $g_soap_test_ftp_url;
    $soap_test_ftp_url = trim(trim($g_soap_test_ftp_url,'/'),'\\');
    unset($g_soap_test_ftp_url);
    if(strlen($soap_test_ftp_url) <1)
    {
        var_dump(array("soap 测试FTP地址为空：".$g_soap_test_ftp_url));die;
    }
    $ContentMngXMLURL = $soap_test_ftp_url."/".$xml_simulate_path;
    global $g_bk_web_url;
    $bk_web_url = $g_bk_web_url;
    unset($g_bk_web_url);
    $bk_web_url = (isset($bk_web_url) && strlen($bk_web_url)) ? $bk_web_url : '';
    $bk_web_url = trim(trim($bk_web_url,'/'),'\\');
    if(strlen($bk_web_url) <1)
    {
        var_dump(array("soap 测试服务器地址为空：".$bk_web_url));die;
    }
    
    $arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
    $str_dir = array_pop($arr_dir);
    
    $WDSL_URL = $bk_web_url.'/v2/ns_api/'.$project_name.'/message/'.ORG_ID.'/test/service.php?wsdl';
    try{
        $obj_soap_client = new notify($WDSL_URL,$CMSID, $SOPID, $CorrelateID,$ContentMngXMLURL);
    }
    catch (Exception $exception)
    {
        var_dump($exception->getMessage());die;
    }
    $result = $obj_soap_client->ExecCmd();
	$result-> soap_base_wsdl_url = $bk_web_url;
	$result-> soap_wsdl_url = $WDSL_URL;
	$result-> soap_base_xml_url = $soap_test_ftp_url;
	$result-> soap_xml_url = $ContentMngXMLURL;
    var_dump($result);die;
}
else
{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>江苏华博 北京新奥特 API soap注入播控测试</title>
    </head>
    <body>
        <form method="post" action="./test.php">
            <input name="action" type='hidden' value='soap'><br/>
            <br>
                    江苏华博 北京新奥特 API soap注入播控测试：<hr/><br>
            CMSID（上游CP ID）:
            <input name='CMSID' type='text' readonly value='<?php echo ORG_ID;?>'/><br/><br>
            SOPID(下游CP ID):
            <input name="SOPID" type='text' value='starcor' readonly><br/><br>
            CorrelateID（消息GUID,不填写自动生成）:
            <input name="CorrelateID" type='text' value=''><br/><br>
                    注入xml文件内容：
            <textarea name="content" cols=180 rows=40></textarea><br/><br/>
            <input type="submit" value="提交"/>
        </form>
    </body>
</html>
<?php 
}
?>