<?php
//error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/common.php";
\ns_core\m_load::load("ns_model.m_queue_model");
class delivery extends \ns_model\m_queue_model
{
    public $obj_delivery = null;
    
    public function __construct()
    {
        $this->obj_delivery = \ns_core\m_load::load_delivery(__FILE__);
    }
    
    
	public function video($message,$cdn_model)
	{
        $queue_main_id = $message['nns_ref_id'];
	    $result = $this->_get_queue_info($queue_main_id,__FUNCTION__,true,'',$cdn_model);
	    if($result['ret'] !=0)
	    {
	        return $result;
	    }
	    if(!isset($result['data_info']) || empty($result['data_info']) || !is_array($result['data_info']))
	    {
	        return m_config::return_data(1,'未查询到点播信息');
	    }
	    $result_xml = $this->obj_delivery->do_video($result['data_info']);


        //封装c2数组
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'series' .'_'. $message['nns_action'] . '_' . $three_rand. '.xml';

        $c2_info = array(
            'nns_task_type'=>'Series',
            'nns_action'=> $message['nns_action'],
            'nns_url' => $file_name,
            'nns_content' => $result_xml,
            'nns_desc' => 'Series,'.$message['nns_action'],
        );
	    //将封装的xml数据写入创建的xml文件
        $sub_path = date('Y-m-d');
        $file = $c2_info['nns_url'];
        $c2_info['nns_url'] = $sub_path.'/'.$file;
        $xml_str = $c2_info['nns_content'];
        $file_path = dirname(dirname(dirname(__FILE__))).'/data/mgtv/'.$sp_id.'/inject_xml/'.$sub_path.'/';
        if(!is_dir($file_path)) mkdir($file_path,0777,true);
        $file_path .= $file;
        file_put_contents($file_path,$xml_str);
        $CmdFileURL = C2_FTP.$c2_info['nns_url'];

	    #TODO
	    $obj_soap = new client();
	    $result = $obj_soap->ContentDeployReq($c2_info['nns_id'], $CmdFileURL,$bk_web_url,$c2_info['nns_task_type']);
	    $result = isset($result->Result) ? $result->Result : isset($result->ResultCode) ? $result->ResultCode : $result->Result;
	}
	
	
    public function index($queue_main_id,$cdn_model)
	{
	    $result = $this->_get_queue_info($queue_main_id,__FUNCTION__,true,'',$cdn_model);
	    if($result['ret'] !=0)
	    {
	        return $result;
	    }
	    if(!isset($result['data_info']) || empty($result['data_info']) || !is_array($result['data_info']))
	    {
	        return m_config::return_data(1,'未查询到点播信息');
	    }
	    $result_xml = $this->obj_delivery->do_video($result['data_info']);
	    #TODO
	}
	
	
	public function media($queue_main_id,$cdn_model)
	{
	    $result = $this->_get_queue_info($queue_main_id,__FUNCTION__,true,'',$cdn_model);
	    if($result['ret'] !=0)
	    {
	        return $result;
	    }
	    if(!isset($result['data_info']) || empty($result['data_info']) || !is_array($result['data_info']))
	    {
	        return m_config::return_data(1,'未查询到点播信息');
	    }
	    $result_xml = $this->obj_delivery->do_video($result['data_info']);
	    #TODO
	}
	
	
	public function live($queue_main_id,$cdn_model)
	{
	    $result = $this->_get_queue_info($queue_main_id,__FUNCTION__,true,'',$cdn_model);
	    if($result['ret'] !=0)
	    {
	        return $result;
	    }
	    if(!isset($result['data_info']) || empty($result['data_info']) || !is_array($result['data_info']))
	    {
	        return m_config::return_data(1,'未查询到点播信息');
	    }
	    $result_xml = $this->obj_delivery->do_video($result['data_info']);
	    #TODO
	}
	
	
	public function live_index($queue_main_id,$cdn_model)
	{
	    $result = $this->_get_queue_info($queue_main_id,__FUNCTION__,true,'',$cdn_model);
	    if($result['ret'] !=0)
	    {
	        return $result;
	    }
	    if(!isset($result['data_info']) || empty($result['data_info']) || !is_array($result['data_info']))
	    {
	        return m_config::return_data(1,'未查询到点播信息');
	    }
	    $result_xml = $this->obj_delivery->do_video($result['data_info']);
	    #TODO
	}
	
	
	public function live_media($queue_main_id,$cdn_model)
	{
	    $result = $this->_get_queue_info($queue_main_id,__FUNCTION__,true,'',$cdn_model);
	    if($result['ret'] !=0)
	    {
	        return $result;
	    }
	    if(!isset($result['data_info']) || empty($result['data_info']) || !is_array($result['data_info']))
	    {
	        return m_config::return_data(1,'未查询到点播信息');
	    }
	    $result_xml = $this->obj_delivery->do_video($result['data_info']);
	    #TODO
	}
	
	public function playbill($queue_main_id,$cdn_model)
	{
	    $result = $this->_get_queue_info($queue_main_id,__FUNCTION__,true,'',$cdn_model);
	    if($result['ret'] !=0)
	    {
	        return $result;
	    }
	    if(!isset($result['data_info']) || empty($result['data_info']) || !is_array($result['data_info']))
	    {
	        return m_config::return_data(1,'未查询到点播信息');
	    }
	    $result_xml = $this->obj_delivery->do_video($result['data_info']);
	    #TODO
	}
}