<?php
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_core.m_timer");
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.epg_import.epg_import_queue");
\ns_core\m_load::load("ns_model.m_queue_model");
\ns_core\m_load::load("ns_timer.epg.delivery_epg_import");
m_config::get_dc();
class delivery_epg_timer extends m_timer
{
	public function action($params = null)
    {
        m_config::timer_write_log($this->timer_path,'开始执行...',$this->child_path);
        $this->init($params);
        m_config::timer_write_log($this->timer_path,'执行结束...',$this->child_path);
    }

    private function init($params = null)
    {
    	/*$message = new \ns_model\import\message();
    	$message->id = "123";
    	$message->import_mode = "cntv";
    	$message->content = "456";
    	$this->explain_message($message);
    	file_put_contents("/data/logs/1.txt", var_export($message,true));
    	return;*/
    	//获取所有的注入方式
//     	var_dump($this->arr_params,$params);die;
    	//获取对应注入类型的message_queue对象
    	//$message_class = m_load::load("ns_model.import.message_queue");
    	$epg_import_queue = new epg_import_queue();
    	//从message弹出1000条信息，这个值应该是注入类型的配置
    	$message_list = $epg_import_queue->pop($this->timer_model,$this->child_path);
        if($message_list['ret'] !=0)
        {
            m_config::timer_write_log($this->timer_path,var_export($message_list,true),$this->child_path);
            return ;
        }
        if(!isset($message_list['data_info']) || empty($message_list['data_info']) || !is_array($message_list['data_info']))
        {
            m_config::timer_write_log($this->timer_path,"查询消息队列数据为空，不在执行",$this->child_path);
            return ;
        }
        
    	foreach ($message_list['data_info'] as $message)
    	{
    	    $m_queue_model = new m_queue_model();
    	    $result = $m_queue_model->_get_queue_info($message['nns_ref_id'],$message['nns_type'],true,'epg','');
    	    if($result['ret'] !=0)
    	    {
    	        return $result;
    	    }
    	    $delivery_cdn_import = new delivery_cdn_import($message['nns_org_id'],$message['nns_cp_id']);
    	    switch ($message['nns_type'])
    	    {
    	        case 'video':
    	            $result_epg = $delivery_cdn_import->import_video($result['data_info']);
    	            break;
    	    }
	        
    	}

    }
}
// $timer = new import_message_timer(m_config::return_child_path(__FILE__,'timer'),$argv[1],null,$argv[2]);
$timer = new delivery_cdn_timer(m_config::return_child_path(__FILE__,'timer'),'JSWS',null,'video');
$timer->run();