<?php
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_core.m_timer");
\ns_core\m_load::load("ns_core.m_config");
class delivery_timer extends m_timer
{
    public function action($params = null)
    {
        m_config::timer_write_log($this->timer_path,'开始执行...',$this->child_path);
        $this->init($params);
        m_config::timer_write_log($this->timer_path,'执行结束...',$this->child_path);
    }

    private function init($params = null)
    {
        /*$php_path = dirname(__FILE__)."/{$this->timer_model}.timer.php cntv > /dev/null &";
        exec(evn::get("php_execute_path")." {$php_path}");
        return;*/
        $arr_type = array(
            'video','index','media','live','live_index','live_media','playbill'
        );
        $arr_sps = m_config::_get_all_sp_info();
        if($arr_sps['ret'] !=0)
        {
            m_config::timer_write_log($this->timer_path,"sp查询失败：{$arr_sps['reason']}",$this->child_path);
            return ;
        }
        if(!isset($arr_sps['data_info']) || !is_array($arr_sps['data_info']) || empty($arr_sps['data_info']))
        {
            m_config::timer_write_log($this->timer_path,"SP查询数据为空",$this->child_path);
            return ;
        }
        $str_exe = evn::get("php_execute_path");
    	//获取所有的分发方式
    	$sps = $this->_get_all_sp_info();
    	foreach ($arr_sps['data_info'] as $sp_val)
    	{
    	    foreach ($arr_type as $video_type)
    	    {
        		//根据定时器种类，及不同的注入模式，获取当前PHP的执行脚本,
        		$php_path = dirname(__FILE__)."/delivery_epg.timer.php ".$sp_val["nns_id"]." ".$video_type." > /dev/null &";
        		//循环根据注入模式 执行对应种类脚本
        		exec($str_exe." {$php_path}");
    	    }
    	}
    	
    }
}
$timer = new delivery_timer(m_config::return_child_path(__FILE__,'ns_timer'));
$timer->run();