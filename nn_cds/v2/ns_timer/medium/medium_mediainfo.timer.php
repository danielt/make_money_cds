<?php
/**
 * Created by PhpStorm.
 * User: kan.yang@starcor.cn
 * Date: 18-3-25
 * Time: 下午12:50
 */
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load_old("nn_logic/medium/medium_library_file_logic.class.php");
\ns_core\m_load::load("ns_core.m_mediainfo");
class medium_mediainfo
{
    private $obj_dc = null;

    private $timer_path = '';

    private $child_path = '';

    private $int_page_size = 100;

    /**
     * 默认构造函数
     */
    public function __construct()
    {
        $this->obj_dc = m_config::get_dc();
        $this->timer_path = m_config::return_child_path(__FILE__);
    }

    /**
     * 定时器入口
     */
    public function init($arr_file_ids = array())
    {
        m_config::timer_write_log($this->timer_path,"-----------解析片源文件文件开始------------",$this->child_path);
        if(empty($arr_file_ids) || !is_array($arr_file_ids))
        {
            $arr_file_list_1 = medium_library_file_logic::query_list($this->obj_dc,array('nns_is_analysis'=>0,'nns_state'=>0,'nns_again'=>5),array(0,100),'nns_modify_time asc');
            if($arr_file_list_1['ret'] != 0)
            {
                m_config::timer_write_log($this->timer_path,"查询待解析的片源文件失败或查询结果为空",$this->child_path);
                return false;
            }
            $arr_file_list_2 = medium_library_file_logic::query_list($this->obj_dc,array('nns_is_analysis'=>2,'nns_state'=>0,'nns_again'=>5),array(0,50),'nns_modify_time asc');
            if($arr_file_list_2['ret'] != 0)
            {
                m_config::timer_write_log($this->timer_path,"查询待解析的片源文件失败或查询结果为空",$this->child_path);
                return false;
            }
            $arr_file_list_1 = is_array($arr_file_list_1['data_info']) ? $arr_file_list_1['data_info'] : array();
            $arr_file_list_2 = is_array($arr_file_list_2['data_info']) ? $arr_file_list_2['data_info'] : array();
            $arr_file_list = array_merge($arr_file_list_1,$arr_file_list_2);
            $arr_file_list = array(
                'ret'=>0,
                'data_info'=>$arr_file_list,
            );
        }
        else
        {
            //查询待解析的片源文件
            $arr_file_list = medium_library_file_logic::query_list($this->obj_dc,$arr_file_ids,array(0,$this->int_page_size));
        }
        if($arr_file_list['ret'] != 0 || empty($arr_file_list['data_info']))
        {
            m_config::timer_write_log($this->timer_path,"查询待解析的片源文件失败或查询结果为空",$this->child_path);
            return false;
        }
        else
        {
            $arr_file_list = $arr_file_list['data_info'];
        }
        //遍历解析
        $arr_media_info = array(); $arr_media_ret = array();
        $temp_id = null;
        foreach($arr_file_list as $file)
        {
            $temp_id[] = $file['nns_id'];
        }
        $str_date = date("Y-m-d H:i:s");
        $result = medium_library_file_logic::execute_sql($this->obj_dc,"update nns_medium_library_file set nns_again=nns_again+1,nns_modify_time='{$str_date}' where nns_id in('".implode("','", $temp_id)."') ");
        foreach($arr_file_list as $file)
        {
            $arr_media_info = m_mediainfo::exec($file['nns_file_url'],$file['nns_id'],$file['nns_ext_url']);
            
            $arr_pathinfo = pathinfo($file['nns_file_url']);
            $str_extension = (isset($arr_pathinfo['extension']) && strlen($arr_pathinfo['extension']) >0) ? trim(strtolower($arr_pathinfo['extension'])) : '';
            
            if($arr_media_info['ret'] == 0 && !empty($arr_media_info['data_info']))
            {
                $arr_media_info_ex = $arr_media_info['ext_info'];
                $arr_media_info = $arr_media_info['data_info'];
                $data = array(
                    'nns_file_size'   => isset($arr_media_info['general']['file_size']['bit']) ? $arr_media_info['general']['file_size']['bit'] : 0,
                    'nns_file_duration' => isset($arr_media_info['general']['duration']['milliseconds']) ? $arr_media_info['general']['duration']['milliseconds'] : 0,
                    'nns_general_fomart'   => isset($arr_media_info['general']['format']['fullName']) ? $arr_media_info['general']['format']['fullName'] : '',
                    'nns_general_bit_rate_mode' => isset($arr_media_info['general']['overall_bit_rate_mode']['shortName']) ? $arr_media_info['general']['overall_bit_rate_mode']['shortName'] : '',
                    'nns_videos_kbps' => isset($arr_media_info['videos'][0]['bit_rate']['absoluteValue']) ? $arr_media_info['videos'][0]['bit_rate']['absoluteValue'] : 0,
                    'nns_videos_bit_rate_mode' => isset($arr_media_info['general']['overall_bit_rate_mode']['shortName']) ? $arr_media_info['general']['overall_bit_rate_mode']['shortName'] : '',
                    'nns_videos_format' => isset($arr_media_info['videos'][0]['format']['shortName']) ? $arr_media_info['videos'][0]['format']['shortName'] : '',
                    'nns_audios_kbps' => isset($arr_media_info['audios'][0]['bit_rate']['absoluteValue']) ? $arr_media_info['audios'][0]['bit_rate']['absoluteValue'] : 0,
                    'nns_audios_bit_rate_mode' => isset($arr_media_info['audios'][0]['bit_rate_mode']['shortName']) ? $arr_media_info['audios'][0]['bit_rate_mode']['shortName'] : '',
                    'nns_audios_format' => isset($arr_media_info['audios'][0]['format']['shortName']) ? $arr_media_info['audios'][0]['format']['shortName'] : '',
                    'nns_ext_url'     => $arr_media_info_ex['LOGURL'],
                    'nns_base_info'   => json_encode($arr_media_info),
                    'nns_is_analysis' => 1,
                    'nns_file_type'=>$str_extension,
                );
                $arr_media_ret = medium_library_file_logic::edit($this->obj_dc,array('nns_id' => $file['nns_id']),$data);
            }
            else
            {
                $arr_media_ret = medium_library_file_logic::edit($this->obj_dc,array('nns_id' => $file['nns_id']),array(
                    'nns_is_analysis' => 2,
                    'nns_file_type'=>$str_extension,
                ));
            }
            m_config::timer_write_log($this->timer_path,"解析片源文件【{$file['nns_id']}】" . ($arr_media_ret['ret'] == 0 ? '成功' : '失败') . "原因：" . var_export($arr_media_ret,true) ,$this->child_path);
            usleep(10000);
        }
        m_config::timer_write_log($this->timer_path,"-----------解析片源文件文件结束------------",$this->child_path);
        return true;
    }
}
if(!isset($_REQUEST['action']))
{
    $obj_medium_mediainfo = new medium_mediainfo();
    $bool_ret = $obj_medium_mediainfo->init();
}