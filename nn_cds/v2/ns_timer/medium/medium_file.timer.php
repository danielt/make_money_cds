<?php
/**
 * Created by PhpStorm.
 * Use : 切分扫描文件且
 * User: kan.yang@starcor.cn
 * Date: 18-3-23
 * Time: 下午6:40
 */
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_core.m_config");
class medium_file
{
    private $str_dir_path= '';

    private $timer_path  = '';

    private $child_path  = '';


    private $int_limit = 200;

    private $bool_linux_sys = true;

    /**
     * 默认构造函数
     */
    public function __construct()
    {
        $this->obj_dc = m_config::get_dc();
        $this->obj_redis_dc = m_config::get_redis_dc();
        $this->timer_path = m_config::return_child_path(__FILE__);
        //导出文件保存路径
        $this->str_dir_path  = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/medium/split/';
        //检测新系统环境
        $this->bool_linux_sys = substr(php_uname(), 0, 7) == "Windows" ? false : true;
     }

    /**
     * 定时器入口
     */
    public function init()
    {
        //获取切割之后的文件列表
        $arr_file_path = array();
        $this->_get_dir_child_file($this->str_dir_path,$arr_file_path);
        //初步解析片源文件
        if(is_array($arr_file_path) && !empty($arr_file_path))
        {
            $str_exe = evn::get("php_execute_path") . ' -f ' . dirname(__FILE__) . '/medium_analyze_file.timer.php ';
            foreach($arr_file_path as $file)
            {
                if ($this->bool_linux_sys)
                {//LINUX下执行

                    $str_order = $str_exe . $file . ' > /dev/null &';
                    //$str_order = $str_exe . $file . ' > /data/logs/medium_file.log &';
                }
                else
                {//windows下执行

                    $str_order = $str_exe . $file;
                }
                exec($str_order);
                m_config::timer_write_log($this->timer_path,'解析TXT文件路径。路径：' . $file,$this->child_path);
                usleep(50000);
            }
        }
        return true;
    }

    /**
     * 递归遍历文件夹
     */
    private function _get_dir_child_file($str_dir, &$arr_file_path)
    {
        $str_dir = rtrim($str_dir,'/') . '/';
        if(@$obj_handle = opendir($str_dir))
        {
            $str_file_path = '';
            while(($str_file = readdir($obj_handle)) !== false)
            {
                //排除根目录
                if($str_file == ".." || $str_file == ".") continue;

                $str_file_path = $str_dir . $str_file;
                if(is_dir($str_file_path))
                {//文件夹，递归

                    $this->_get_dir_child_file($str_file_path, $arr_file_path);
                } else
                {//文件存入数组

                    if(count($arr_file_path) < $this->int_limit)
                    {
                        if(file_exists($str_file_path) && filesize($str_file_path) > 0)
                        {
                            $arr_file_path[] = $str_file_path;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            closedir($obj_handle);
        }
        return true;
    }
}

$obj_medium_file = new medium_file();
$obj_medium_file->init();

