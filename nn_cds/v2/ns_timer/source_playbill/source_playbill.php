<?php
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_core.m_config");

include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/message/nl_message.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api_v2/nn_const.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api_v2/common.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cache/view_cache.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_cms_manager/service/asset_import/import_assets_proc.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/live/live.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/live/live_media.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/live/playbill.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/live/third_live.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api_v2/control/standard/sync_source.php';
class source_playbill
{
    /**
     * 初始化
     */
    public function __construct()
    {
        //初始化obj DC
        m_config::get_dc();
        
    }
    
    public function init()
    {
        $base_file_dir = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))."/data/temp/third_playbill";
	    $result_files = m_file::get_files($base_file_dir);
	    if(!is_array($result_files) || empty($result_files))
	    {
	        return m_config::return_data(0,"路径[{$base_file_dir}];未获取到任何文件");
	    }
	    $result_files = array_chunk($result_files, 10);
	    $result_files = isset($result_files[0]) ? $result_files[0] : null;
	    if(!is_array($result_files) || empty($result_files))
	    {
	        return m_config::return_data(0,"路径[{$base_file_dir}];未获取到任何文件");
	    }
	    $str_exe = evn::get("php_execute_path") . ' -f ' . dirname(__FILE__) . '/source_playbill.php ';
	    foreach ($result_files as $value)
	    {
	        $data_file = fopen($value, 'r');
	        $locked = flock($data_file, LOCK_EX + LOCK_NB);
	        while (!feof($data_file))
	        {
	            $line_item = fgets($data_file);
	            if (!$line_item)
	            {
	                continue;
	            }
	            $line_item = trim($line_item);
	            if(strlen($line_item) <1)
	            {
	                continue;
	            }
	            if (substr(php_uname(), 0, 7) != "Windows")
	            {
	                //LINUX下执行
	                //                 $str_order = "{$str_exe}  $value > /dev/null &";
	                $str_order = "{$str_exe}  $line_item";
	            }
	            else
	            {
	                //windows下执行
	                $str_order = "{$str_exe}  $line_item ";
	            }
	            var_dump($str_order);continue;
	            usleep(5000);
	            exec($str_order);
	        }
	        @unlink($value);
	    }
        return m_config::return_data(0,"OK");
    }
    
    public function init_v2($id)
    {
        $result_config = nl_project_key::query_by_key(m_config::get_dc(), "aiqiyi_info");
        if($result_config['ret'] !=0)
        {
            return $result_config;
        }
        if(!isset($result_config['data_info']['nns_key_value']) || !m_config::is_json($result_config['data_info']['nns_key_value']))
        {
            return m_config::return_data(1,"aiqiyi_info 配置为空或者非法");
        }
        $result_config = json_decode($result_config['data_info']['nns_key_value'],true);
        if(!isset($result_config['aiqiyi_url']) || strlen($result_config['aiqiyi_url']) <1)
        {
            return m_config::return_data(1,"aiqiyi_info 配置aiqiyi_url 为空或者未配置");
        }
        if(!isset($result_config['modId']) || strlen($result_config['modId']) <1)
        {
            return m_config::return_data(1,"aiqiyi_info 配置modId 为空或者未配置");
        }
        if(!isset($result_config['partnerId']) || strlen($result_config['partnerId']) <1)
        {
            return m_config::return_data(1,"aiqiyi_info 配置partnerId 为空或者未配置");
        }
        $modId = $result_config['modId'];
        $partnerId = $result_config['partnerId'];
        $token = m_aiqiyi_encrypt::encrypt($modId, $partnerId);
        $aiqiyi_url = trim(rtrim(rtrim($result_config['aiqiyi_url'],'/'),'\\'));
        $aiqiyi_url.="/snapshot?t={$token}&chnId={$id}&type=0";
        $result = m_config::get_curl_content($aiqiyi_url,60);
        if($result['ret'] !=0)
        {
            return $result;
        }
        if(!isset($result['data_info']) || strlen($result['data_info'])<1)
        {
            return m_config::return_data(1,"URL:{$aiqiyi_url} 获取数据为空或者数据非json");
        }
        $again_url = explode('"', $result['data_info']);
        $result = m_config::get_curl_content($again_url[1],60);
        if($result['ret'] !=0)
        {
            return $result;
        }
        if(!isset($result['data_info']) || !m_config::is_json($result['data_info']))
        {
            return m_config::return_data(1,"URL:{$again_url[1]} 获取数据为空或者数据非json");
        }
        $data_info = json_decode($result['data_info'],true);
        if(!isset($data_info['code']) || $data_info['code'] !='C00000')
        {
            return m_config::return_data(1,"URL:{$result['data_info']}，数据错误，状态码：{$data_info['code']}");
        }
        if(!isset($data_info['data']) || !is_array($data_info['data']) || empty($data_info['data']))
        {
            return m_config::return_data(0,"URL:{$result['data_info']}，数据为空");
        }
        $data = array_chunk($data_info['data'], 15);
        $result_mkdir = m_config::make_dir("temp/aiqiyi/{$id}");
        if($result_mkdir['ret'] !=0)
        {
            return $result_mkdir;
        }
        $absolute_dir = trim(rtrim(rtrim($result_mkdir['data_info']['absolute_dir'],'/'),'\\'));
        foreach ($data as $key=>$value)
        {
            if(!is_array($value) || empty($value))
            {
                continue;
            }
            file_put_contents($absolute_dir."/".np_guid_rand().".txt", json_encode($value));
        }
        return m_config::return_data(0,'ok');
    }
}
if(isset($_SERVER['argv'][1]) && !empty($_SERVER['argv'][1]))
{
    $source_playbill = new source_playbill();
    $result = $source_playbill->init_v2($_SERVER['argv'][1],$_SERVER['argv'][2]);
}
else
{
    $source_playbill = new source_playbill();
    $result = $source_playbill->init();
}
echo json_encode($result);