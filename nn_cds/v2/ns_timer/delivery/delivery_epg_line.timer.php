<?php
/**
 * epg媒资上下线
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/10/10 15:05
 */
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_model.m_queue_model");
\ns_core\m_load::load("ns_core.m_timer");
\ns_core\m_load::load("ns_core.m_public");
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.epg_import.epg_line_queue");
m_config::get_dc();
class delivery_epg_line_timer extends m_timer
{
    public function action($params = null)
    {
        m_config::timer_write_log($this->timer_path,'开始执行...',$this->child_path);
        $this->clip_execute($params);
        m_config::timer_write_log($this->timer_path,'执行结束...',$this->child_path);
    }

    private function clip_execute($params = null)
    {
        $epg_line_queue = new ns_model\epg_import\epg_line_queue($this->timer_model,$this->child_path);
        $epg_line_explain = $epg_line_queue->explain();
        if($epg_line_explain['ret'] != NS_CDS_SUCCE)
        {
            m_config::timer_write_log($this->timer_path,var_export($epg_line_explain['reason'],true),$this->child_path);
            return;
        }
        //获取注入切片队列的条数
        $message_list = $epg_line_queue->pop($this->child_path);

        if($message_list['ret'] != 0)
        {
            m_config::timer_write_log($this->timer_path,var_export($message_list,true),$this->child_path);
            return ;
        }
        if(!isset($message_list['data_info']) || empty($message_list['data_info']) || !is_array($message_list['data_info']))
        {
            m_config::timer_write_log($this->timer_path,"查询媒资上下线队列数据为空，不在执行媒资上下线指令注入", $this->child_path);
            return ;
        }

        //视达科标准媒资上下线指令注入到epg
        \ns_core\m_load::load("ns_model.delivery.epg_line_delivery_explain");
        $explain_inst = new \ns_model\delivery\epg_line_delivery_explain($this->timer_model);

        foreach ($message_list['data_info'] as $message)
        {
            //记录日志
            m_config::timer_write_log($this->timer_path,"执行媒资上下线指令注入到epg：".var_export($message,true),$this->child_path);
            $explain_inst->init($message);
            $explain_result = $explain_inst->explain();
            $explain_inst->destroy_init();
            m_config::timer_write_log($this->timer_path,"执行媒资上下线指令注入到epg结果：".var_export($explain_result,true),$this->child_path);
        }
    }
}

//$timer = new delivery_epg_line_timer(m_config::return_child_path(__FILE__,'ns_timer'),'cbczq_zte',null,'video');
$timer = new delivery_epg_line_timer(m_config::return_child_path(__FILE__,'ns_timer'),$argv[1],null,$argv[2], __FILE__ . " {$argv[1]} {$argv[2]}");
$timer->run();