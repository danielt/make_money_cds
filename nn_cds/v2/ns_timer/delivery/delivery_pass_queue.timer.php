<?php
    /**
     * Created by <xinxin.deng>.
     * Author: xinxin.deng
     * Date: 2018/10/31 10:20
     */
    include_once dirname(dirname(dirname(__FILE__)))."/common.php";
    \ns_core\m_load::load("ns_model.m_queue_model");
    \ns_core\m_load::load("ns_core.m_timer");
    \ns_core\m_load::load("ns_core.m_public");
    \ns_core\m_load::load("ns_core.m_config");
    \ns_core\m_load::load("ns_model.pass_queue.pass_queue");
    m_config::get_dc();

    class delivery_pass_queue_timer extends m_timer
    {
        public function action($params = null)
        {
            m_config::timer_write_log($this->timer_path,'开始执行...',$this->child_path);
            $this->pass_queue_execute($params);
            m_config::timer_write_log($this->timer_path,'执行结束...',$this->child_path);
        }

        private function pass_queue_execute($params = null)
        {
            $pass_queue = new ns_model\pass_queue\pass_queue($this->timer_model);
            $pass_queue_explain = $pass_queue->explain();
            if($pass_queue_explain['ret'] != NS_CDS_SUCCE)
            {
                m_config::timer_write_log($this->timer_path,var_export($pass_queue_explain['reason'],true));
                return;
            }
            //获取注入切片队列的条数
            $message_list = $pass_queue->pop();

            if($message_list['ret'] != 0)
            {
                m_config::timer_write_log($this->timer_path,var_export($message_list,true));
                return ;
            }
            if(!isset($message_list['data_info']) || empty($message_list['data_info']) || !is_array($message_list['data_info']))
            {
                m_config::timer_write_log($this->timer_path,"查询透传队列数据为空，不在执行透传指令注入");
                return ;
            }

            $project = evn::get("project");
            //开发自写透传队列注入，依项目决定
            if(\ns_core\m_load::load("ns_api.{$project}.delivery.{$this->timer_model}.pass_queue_delivery"))
            {
                $explain_inst = new pass_queue_delivery($this->timer_model);
            }
            else
            {
                //视达科标准透传指令注入
                \ns_core\m_load::load("ns_model.delivery.pass_queue_delivery_explain");
                $explain_inst = new \ns_model\delivery\pass_queue_delivery_explain($this->timer_model);
            }

            foreach ($message_list['data_info'] as $message)
            {
                //记录日志
                m_config::timer_write_log($this->timer_path,"执行透传队列指令注入：".var_export($message,true),$this->child_path);
                $explain_inst->init($message);
                $explain_result = $explain_inst->explain();
                $explain_inst->destroy_init();
                m_config::timer_write_log($this->timer_path,"执行透传队列指令注入结果：".var_export($explain_result,true),$this->child_path);
            }
        }
    }

    //$timer = new delivery_pass_queue_timer(m_config::return_child_path(__FILE__,'ns_timer'),'hndx_mgtv_fuse',null,'');
    $timer = new delivery_pass_queue_timer(m_config::return_child_path(__FILE__,'ns_timer'),$argv[1],null,'', __FILE__ . " {$argv[1]}}");
    $timer->run();