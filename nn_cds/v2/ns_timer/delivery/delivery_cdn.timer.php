<?php
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_model.m_queue_model");
\ns_core\m_load::load("ns_core.m_timer");
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.cdn_import.cdn_import_queue");
\ns_core\m_load::load("ns_data_model.command.m_command_task_log_inout");
\ns_core\m_load::load_old('nn_logic/op_queue/op_queue.class.php');
m_config::get_dc();
class delivery_cdn_timer extends m_timer
{
    public function action($params = null)
    {
        m_config::timer_write_log($this->timer_path,'开始执行...',$this->child_path);
        $this->cdn_import_execute($params);
        m_config::timer_write_log($this->timer_path,'执行结束...',$this->child_path);
    }

    private function cdn_import_execute($params = null)
    {
        $cdn_import_queue = new ns_model\cdn_import\cdn_import_queue($this->timer_model,$this->child_path);
        $cdn_explain = $cdn_import_queue->explain();
        if($cdn_explain['ret'] != NS_CDS_SUCCE)
        {
            m_config::timer_write_log($this->timer_path,var_export($cdn_explain['reason'],true),$this->child_path);
            return;
        }
        //获取CDN等待注入条数
        $num = (isset($cdn_import_queue->arr_sp_config['cdn_exec_num']) && (int)$cdn_import_queue->arr_sp_config['cdn_exec_num'] > 0) ? (int)$cdn_import_queue->arr_sp_config['cdn_exec_num'] : 100;
        //获取EPG注入失败重试最大次数
        $retry_time = (isset($cdn_import_queue->arr_sp_config['cdn_fail_retry_time']) && (int)$cdn_import_queue->arr_sp_config['cdn_fail_retry_time'] > 0) ? (int)$cdn_import_queue->arr_sp_config['cdn_fail_retry_time'] : 0;
        //获取EPG注入失败条数
        $fail_num = (isset($cdn_import_queue->arr_sp_config['cdn_exec_fail_num']) && (int)$cdn_import_queue->arr_sp_config['cdn_exec_fail_num'] > 0) ? (int)$cdn_import_queue->arr_sp_config['cdn_exec_fail_num'] : 20;
        //获取注入EPG条数
        $message_list = $cdn_import_queue->pop($num,$retry_time,$fail_num);
        if($message_list['ret'] != 0)
        {
            m_config::timer_write_log($this->timer_path,var_export($message_list,true),$this->child_path);
            return ;
        }
        if(!isset($message_list['data_info']) || empty($message_list['data_info']) || !is_array($message_list['data_info']))
        {
            m_config::timer_write_log($this->timer_path,"查询消息队列数据为空，不在执行CDN注入",$this->child_path);
            return ;
        }
        $project = evn::get("project");
        \ns_core\m_load::load("ns_api.{$project}.delivery.{$this->timer_model}.delivery");
        $explain_inst = new delivery($this->timer_model);
        foreach ($message_list['data_info'] as $message)
        {
            m_config::write_cdn_execute_log("***********开始执行CDN指令注入：".var_export($message,true),$this->timer_model, $message['c2_info']['nns_id']);
            m_config::timer_write_log($this->timer_path,"执行CDN注入：".var_export($message,true),$this->child_path);
            $explain_inst->init($message);
            $explain_result = $explain_inst->explain();
            $explain_inst->destroy_init();
            $re_w_log = m_config::write_cdn_execute_log("***********执行CDN指令结果：".var_export($explain_result,true), $this->timer_model, $message['c2_info']['nns_id']);

            $op_q = \nl_op_queue::query_by_params(\m_config::get_dc(), array('nns_id' => $message['c2_info']['nns_op_id']), 1);
            if ($explain_result['ret'] !=0 && $op_q['ret'] == 0 && count($op_q['data_info']) == 1)
            {
                $command_task_log_params = array(
                    'base_info' => array(
                        'nns_command_task_id' => $op_q['data_info'][0]['nns_command_task_id'],
                        'nns_status' => NS_CDS_COMMAND_CDN_FAIL,
                        'nns_desc' => var_export($explain_result,true),
                        'nns_execute_url' => $re_w_log['data_info']['base_dir'],
                    )
                );
                $obj_command_task_log = new m_command_task_log_inout();
                $obj_command_task_log->add($command_task_log_params);
            }

            m_config::timer_write_log($this->timer_path,"执行CDN结果：".var_export($explain_result,true),$this->child_path);
        }
    }
}

//$timer = new delivery_cdn_timer(m_config::return_child_path(__FILE__,'ns_timer'),'starcor',null,'media');
$timer = new delivery_cdn_timer(m_config::return_child_path(__FILE__,'ns_timer'),$argv[1],null,$argv[2], __FILE__ . " {$argv[1]} {$argv[2]}");
$timer->run();