<?php
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_model.m_queue_model");
\ns_core\m_load::load("ns_core.m_timer");
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.epg_import.epg_import_queue");
\ns_core\m_load::load("ns_data_model.command.m_command_task_log_inout");
\ns_core\m_load::load_old('nn_logic/op_queue/op_queue.class.php');
m_config::get_dc();
class delivery_epg_timer extends m_timer
{
    public function action($params = null)
    {
        m_config::timer_write_log($this->timer_path,'开始执行...',$this->child_path);
        $this->epg_import_execute($params);
        m_config::timer_write_log($this->timer_path,'执行结束...',$this->child_path);
    }

    private function epg_import_execute($params = null)
    {
        //从c2弹出注入EPG数据
        $epg_import_queue = new ns_model\epg_import\epg_import_queue($this->timer_model,$this->child_path);
        $epg_explain = $epg_import_queue->explain();
        if($epg_explain['ret'] != NS_CDS_SUCCE)
        {
            m_config::timer_write_log($this->timer_path,var_export($epg_explain['reason'],true),$this->child_path);
            return;
        }
        //获取EPG等待注入条数
        $num = (isset($epg_import_queue->arr_sp_config['epg_pre_max']) && (int)$epg_import_queue->arr_sp_config['epg_pre_max'] > 0) ? (int)$epg_import_queue->arr_sp_config['epg_pre_max'] : 100;
        //获取EPG注入失败重试最大次数
        $retry_time = (isset($epg_import_queue->arr_sp_config['epg_retry_time']) && (int)$epg_import_queue->arr_sp_config['epg_retry_time'] > 0) ? (int)$epg_import_queue->arr_sp_config['epg_retry_time'] : 0;
        //获取EPG注入失败条数
        $fail_num = (isset($epg_import_queue->arr_sp_config['epg_fail_max']) && (int)$epg_import_queue->arr_sp_config['epg_fail_max'] > 0) ? (int)$epg_import_queue->arr_sp_config['epg_fail_max'] : 20;
        //获取注入EPG条数
        $message_list = $epg_import_queue->pop($num,$retry_time,$fail_num);
        if($message_list['ret'] != 0)
        {
            m_config::timer_write_log($this->timer_path,var_export($message_list,true),$this->child_path);
            return ;
        }
        if(!isset($message_list['data_info']) || empty($message_list['data_info']) || !is_array($message_list['data_info']))
        {
            m_config::timer_write_log($this->timer_path,"查询消息队列数据为空，不在执行EPG注入",$this->child_path);
            return ;
        }
        $project = evn::get("project");
        //开发自写注入EPG，可能是其他EPG方
        if(\ns_core\m_load::load("ns_api.{$project}.delivery.{$this->timer_model}.epg_delivery"))
        {
            $explain_inst = new epg_delivery($this->timer_model);
        }
        else
        {
            //视达科标准EPG注入
            \ns_core\m_load::load("ns_model.delivery.epg_delivery_explain");
            $explain_inst = new \ns_model\delivery\epg_delivery_explain($this->timer_model);
        }
        foreach ($message_list['data_info'] as $message)
        {
            m_config::write_epg_execute_log("***********执行EPG注入：".var_export($message,true), $this->timer_model, $message['c2_info']['nns_id']);
            m_config::timer_write_log($this->timer_path,"执行EPG注入：".var_export($message,true),$this->child_path);
            $init_result = $explain_inst->init($message);
            if($init_result['ret'] == 1 )
            {
                \m_config::timer_write_log($this->timer_path, $init_result['reason'], $this->child_path);
                continue;
            }
            $explain_result = $explain_inst->explain();
            $explain_inst->destroy_init();
            $re_w_log = m_config::write_epg_execute_log("***********执行EPG结果：".var_export($explain_result,true), $this->timer_model, $message['c2_info']['nns_id']);
            $op_q = \nl_op_queue::query_by_params(\m_config::get_dc(), array('nns_id' => $message['c2_info']['nns_op_id']), 1);
            if ($op_q['ret'] == 0 && count($op_q['data_info']) == 1)
            {
                if ($explain_result['ret'] != 0)
                {
                    $command_status = NS_CDS_COMMAND_EPG_FAIL;
                } else
                {
                    $command_status = NS_CDS_COMMAND_EPG_SUCCESS;
                }
                $command_task_log_params = array(
                    'base_info' => array(
                        'nns_command_task_id' => $op_q['data_info'][0]['nns_command_task_id'],
                        'nns_status' => $command_status,
                        'nns_desc' => var_export($explain_result, true),
                        'nns_execute_url' => $re_w_log['data_info']['base_dir'],
                    )
                );
                $obj_command_task_log = new m_command_task_log_inout();
                $obj_command_task_log->add($command_task_log_params);
            }
            m_config::timer_write_log($this->timer_path,"执行EPG结果：".var_export($explain_result,true),$this->child_path);
        }
    }
}

//$timer = new delivery_epg_timer(m_config::return_child_path(__FILE__,'ns_timer'),'sumavision',null,'media');
$timer = new delivery_epg_timer(m_config::return_child_path(__FILE__,'ns_timer'),$argv[1],null,$argv[2], __FILE__ . " {$argv[1]} {$argv[2]}");
/****************湖南电信项目特殊SP处理****************/
$sp_arr = array(
    'hndx' => 'hndx_hw',
    'hndx_hw' => 'hndx',
    'hnlt' => 'hnlt_zx',
    'hnlt_zx' => 'hnlt'
);
if(in_array($argv[1],$sp_arr) && in_array($argv[2],array('video','index')))
{
    $check_bool = $timer->check_linux_course_v2(__FILE__ . " {$sp_arr[$argv[1]]} {$argv[2]}"); //没有进程才是正常
    if(!$check_bool)
    {
        return ;
    }
}
/****************湖南电信项目特殊SP处理****************/
$timer->run();