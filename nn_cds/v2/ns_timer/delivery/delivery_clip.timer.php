<?php
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_model.m_queue_model");
\ns_core\m_load::load("ns_core.m_timer");
\ns_core\m_load::load("ns_core.m_public");
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.clip.clip_queue");
\ns_core\m_load::load("ns_data_model.command.m_command_task_log_inout");
m_config::get_dc();
class delivery_clip_timer extends m_timer
{
    public function action($params = null)
    {
        m_config::timer_write_log($this->timer_path,'开始执行...',$this->child_path);
        $this->clip_execute($params);
        m_config::timer_write_log($this->timer_path,'执行结束...',$this->child_path);
    }

    private function clip_execute($params = null)
    {
        $clip_queue = new ns_model\clip\clip_queue($this->timer_model,$this->child_path);
        $clip_explain = $clip_queue->explain();
        if($clip_explain['ret'] != NS_CDS_SUCCE)
        {
            m_config::timer_write_log($this->timer_path,var_export($clip_explain['reason'],true),$this->child_path);
            return;
        }
        //获取等待注入c2队列条数

        //获取等待注入的条数
        $num = (isset($clip_queue->arr_sp_config['clip_pre_max']) && (int)$clip_queue->arr_sp_config['clip_pre_max'] > 0) ? (int)$clip_queue->arr_sp_config['clip_pre_max'] : 20;
        //获取注入切片队列的条数
        $message_list = $clip_queue->pop($num, $this->child_path);

        if($message_list['ret'] != 0)
        {
            m_config::timer_write_log($this->timer_path,var_export($message_list,true),$this->child_path);
            return ;
        }
        if(!isset($message_list['data_info']) || empty($message_list['data_info']) || !is_array($message_list['data_info']))
        {
            m_config::timer_write_log($this->timer_path,"查询中心同步指令队列数据为空，不在执行中心同步指令注入",$this->child_path);
            return ;
        }

        //视达科标准中心同步指令注入到切片队列
        \ns_core\m_load::load("ns_model.delivery.clip_delivery_explain");
        $explain_inst = new \ns_model\delivery\clip_delivery_explain($this->timer_model);

        foreach ($message_list['data_info'] as $message)
        {
            //记录日志
            m_config::write_clip_execute_log("***********开始执行中心同步指令注入切片队列：".var_export($message,true),$this->timer_model, $message['nns_op_id']);
            m_config::timer_write_log($this->timer_path,"执行中心同步指令注入到切片队列：".var_export($message,true),$this->child_path);
            $explain_inst->init($message);
            $explain_result = $explain_inst->explain();
            $explain_inst->destroy_init();
            m_config::timer_write_log($this->timer_path,"执行中心同步指令注入到切片队列结果：".var_export($explain_result,true),$this->child_path);
            $re_w_log = m_config::write_clip_execute_log("***********执行中心同步指令注入到切片队列结果：".var_export($explain_result,true), $this->timer_model, $message['nns_op_id']);
            if ($explain_result['ret'] != 0)
            {
                $command_status = NS_CDS_COMMAND_OP_TO_CLIP_FAIL;
            }
            else
            {
                $command_status = NS_CDS_COMMAND_WAIT_CLIP;
            }
            $command_task_log_params = array(
                'base_info' => array(
                    'nns_command_task_id' => $message['nns_command_task_id'],
                    'nns_status' => $command_status,
                    'nns_desc' => var_export($explain_result,true),
                    'nns_execute_url' => $re_w_log['data_info']['base_dir'],
                )
            );
            $obj_command_task_log = new m_command_task_log_inout();
            $obj_command_task_log->add($command_task_log_params);
        }
    }
}

//$timer = new delivery_clip_timer(m_config::return_child_path(__FILE__,'ns_timer'),'sumavision',null,'media');
$timer = new delivery_clip_timer(m_config::return_child_path(__FILE__,'ns_timer'),$argv[1],null,$argv[2], __FILE__ . " {$argv[1]} {$argv[2]}");
$timer->run();