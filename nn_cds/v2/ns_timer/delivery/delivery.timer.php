<?php
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_core.m_timer");
\ns_core\m_load::load("ns_core.m_config");
class delivery_timer extends m_timer
{
    public function action($params = null)
    {
        m_config::timer_write_log($this->timer_path,'开始执行...',$this->child_path);
        $this->init($params);
        m_config::timer_write_log($this->timer_path,'执行结束...',$this->child_path);
    }

    private function init($params = null)
    {
        $exec_timer = array(
            'cdn' => array('video','index','media'),
            'live_cdn'=>array('live','live_index','live_media','playbill'),
            'epg' => array('video','index','media','live','live_index','live_media','playbill'),
            'clip' => array('media', 'file', 'live_media', 'epg_file'),
            'center_op' => array('video','index','media'),
            'transcoding' => array(),
            'epg_line' => array('video'),
            'file' => array('epg_file'),
            'pass_queue' => array(),
        );
        $arr_sps = m_config::_get_all_sp_info();
        if($arr_sps['ret'] !=0)
        {
            m_config::timer_write_log($this->timer_path,"sp查询失败：{$arr_sps['reason']}",$this->child_path);
            return ;
        }
        if(!isset($arr_sps['data_info']) || !is_array($arr_sps['data_info']) || empty($arr_sps['data_info']))
        {
            m_config::timer_write_log($this->timer_path,"SP查询数据为空",$this->child_path);
            return ;
        }
        $arr_crontab_php = array();
        $str_exe = evn::get("php_execute_path");
    	foreach ($arr_sps['data_info'] as $sp_val)
    	{
            //根据定时器种类，及不同的注入模式，获取当前PHP的执行脚本
    	    foreach ($exec_timer as $queue=>$exec_value)
    	    {
    	        $queue_timer = "delivery_{$queue}.timer.php";
    	        $php_path = dirname(__FILE__)."/{$queue_timer} {$sp_val["nns_id"]}";
                if(!empty($exec_value))
                {
                    foreach ($exec_value as $value)
                    {
                        $arr_crontab_php[] = $str_exe . " " . $php_path . " {$value}";
                    }
                }
                else
                {
                    $arr_crontab_php[] = $str_exe . " " . $php_path;
                }
    	    }
    	}
        //m_config::timer_write_log($this->timer_path,"执行定时器：" . var_export($arr_crontab_php,true),$this->child_path);
    	if(!empty($arr_crontab_php))
        {
            shuffle($arr_crontab_php);//对定时器进行了进程检测，这里每次打乱定时器执行顺序
            foreach ($arr_crontab_php as $crontab)
            {
                m_config::timer_write_log($this->timer_path,"当前执行定时器：" . var_export($crontab,true),$this->child_path);
                if (substr(php_uname(), 0, 7) == "Windows")//windows下执行
                {
                    exec($crontab);
                }
                else //LINUX下执行
                {
                    exec($crontab . " > /dev/null &");
                }
            }
        }
    }
}
$timer = new delivery_timer(m_config::return_child_path(__FILE__,'ns_timer'),'','','',__FILE__);
$timer->run();