<?php
/**
 * 模板文件分发
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/11/12 11:17
 */
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_model.m_queue_model");
\ns_core\m_load::load("ns_core.m_timer");
\ns_core\m_load::load("ns_core.m_public");
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.file.file_queue");
m_config::get_dc();
class delivery_file_timer extends m_timer
{
    public function action($params = null)
    {
        m_config::timer_write_log($this->timer_path,'开始执行...',$this->child_path);
        $this->file_execute($params);
        m_config::timer_write_log($this->timer_path,'执行结束...',$this->child_path);
    }

    private function file_execute($params = null)
    {
        $file_queue = new ns_model\file\file_queue($this->timer_model, $this->child_path);
        $file_explain = $file_queue->explain();
        if($file_explain['ret'] != NS_CDS_SUCCE)
        {
            m_config::timer_write_log($this->timer_path,var_export($file_explain['reason'],true),$this->child_path);
            return;
        }
        //获取CDN等待注入条数
        $num = (isset($file_queue->arr_sp_config['cdn_exec_num']) && (int)$file_queue->arr_sp_config['cdn_exec_num'] > 0) ? (int)$file_queue->arr_sp_config['cdn_exec_num'] : 100;
        //获取EPG注入失败重试最大次数
        $retry_time = (isset($file_queue->arr_sp_config['cdn_fail_retry_time']) && (int)$file_queue->arr_sp_config['cdn_fail_retry_time'] > 0) ? (int)$file_queue->arr_sp_config['cdn_fail_retry_time'] : 0;
        //获取EPG注入失败条数
        $fail_num = (isset($file_queue->arr_sp_config['cdn_exec_fail_num']) && (int)$file_queue->arr_sp_config['cdn_exec_fail_num'] > 0) ? (int)$file_queue->arr_sp_config['cdn_exec_fail_num'] : 20;

        //获取注入EPG条数
        $message_list = $file_queue->pop($num, $retry_time, $fail_num);

        if($message_list['ret'] != 0)
        {
            m_config::timer_write_log($this->timer_path,var_export($message_list,true),$this->child_path);
            return ;
        }
        if(!isset($message_list['data_info']) || empty($message_list['data_info']) || !is_array($message_list['data_info']))
        {
            m_config::timer_write_log($this->timer_path,"查询媒资上下线队列数据为空，不在执行媒资上下线指令注入", $this->child_path);
            return ;
        }
        $project = evn::get("project");
        //开发自写注入CDN，可能是其他CDN方--目前都是项目定制化,单独分发
        if(\ns_core\m_load::load("ns_api.{$project}.delivery.{$this->timer_model}.delivery"))
        {
            $explain_inst = new delivery($this->timer_model);
        }
        else
        {
            //视达科标准CDN注入--暂时没用
            \ns_core\m_load::load("ns_model.delivery.cdn_delivery_explain");
            $explain_inst = new \ns_model\delivery\cdn_delivery_explain($this->timer_model);
        }


        foreach ($message_list['data_info'] as $message)
        {
            //记录日志
            m_config::timer_write_log($this->timer_path,"执行模板文件指令注入到CDN：".var_export($message,true),$this->child_path);
            $explain_inst->init($message);
            $explain_result = $explain_inst->explain();
            $explain_inst->destroy_init();
            m_config::timer_write_log($this->timer_path,"执行模板文件指令注入到CDN结果：".var_export($explain_result,true),$this->child_path);
        }
    }
}

//$timer = new delivery_epg_line_timer(m_config::return_child_path(__FILE__,'ns_timer'),'cbczq_zte',null,'video');
$timer = new delivery_file_timer(m_config::return_child_path(__FILE__,'ns_timer'),$argv[1],null,$argv[2], __FILE__ . " {$argv[1]} {$argv[2]}");
$timer->run();