<?php
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_core.m_timer");
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.message.message_queue");
m_config::get_dc();
class delivery_import_message_timer extends m_timer
{
	public function action($params = null)
    {
        m_config::timer_write_log($this->timer_path,'开始执行...',$this->child_path);
        $this->init($params);
        m_config::timer_write_log($this->timer_path,'执行结束...',$this->child_path);
    }

    private function init($params = null)
    {
        global $g_bk_web_url;
        $bk_web_url = $g_bk_web_url;
        $bk_web_url = strlen($bk_web_url) <1 ? '' : trim(trim($bk_web_url,'\\'),'/');
        unset($g_bk_web_url);
        m_config::timer_write_log($this->timer_path,"加载全局请求地址[{$bk_web_url}]",$this->child_path);
        if(strlen($bk_web_url)<1)
        {
            return ;
        }
        $file_path = $this->arr_params['bk_queue_value'];
        m_config::timer_write_log($this->timer_path,"加载CP[{$file_path}]",$this->child_path);
        if(strlen($file_path)<1)
        {
            return ;
        }
        $str_project = evn::get("project");
        m_config::timer_write_log($this->timer_path,"加载项目[{$str_project}]",$this->child_path);
        if(strlen($str_project)<1)
        {
            return ;
        }
        $file_dir = dirname(dirname(dirname(__FILE__)))."/ns_api/{$str_project}/message/{$file_path}/import.php";
        m_config::timer_write_log($this->timer_path,"加载文件[{$file_dir}]",$this->child_path);
        if(!file_exists($file_dir))
        {
            return ;
        }

        $str_exe = evn::get("php_execute_path");
        exec($str_exe ." {$file_dir} > /dev/null &");
        //$bk_web_url = $bk_web_url."/v2/ns_api/{$str_project}/message/{$file_path}/import.php";
        //m_config::timer_write_log($this->timer_path,"请求地址[{$bk_web_url}]",$this->child_path);
        //m_config::get_curl_content($bk_web_url,60);
        return m_config::return_data(0,'执行完毕');
    }
}
if(is_array($argv) && !empty($argv))
{
    $timer = new delivery_import_message_timer(m_config::return_child_path(__FILE__,'timer'),$argv[1],'delivery_import_message');
    $timer->run();
}