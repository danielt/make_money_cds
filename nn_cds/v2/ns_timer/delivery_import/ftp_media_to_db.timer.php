<?php
/**
 * Created by PhpStorm.
 * Use : 扫描FTP服务器片源媒体文件信息至上游消息队列
 * User: kan.yang@starcor.com
 * Date: 18-6-1
 * Time: 下午1:27
 */
include_once dirname(dirname(dirname(__FILE__))) . "/common.php";
\ns_core\m_load::load("ns_core.m_timer");
\ns_core\m_load::load("ns_core.m_config");
m_config::get_dc();
class ftp_media_to_db_timer extends m_timer
{
    public function action($params = null)
    {
        m_config::timer_write_log($this->timer_path,'开始执行...',$this->child_path);
        $this->init($params);
        m_config::timer_write_log($this->timer_path,'执行结束...',$this->child_path);
    }

    /**
     * 入口函数
     */
    private function init($params = null)
    {
        //验证队列池扩展参数
        if(!isset($this->arr_params["bk_queue_value"]) || strlen($this->arr_params["bk_queue_value"]) < 1 || $this->arr_params["bk_queue_value"] !='placeholder')
        {
            m_config::timer_write_log($this->timer_path,"[redis节目单池中返回数据为空]".$this->arr_params["bk_queue_value"],$this->child_path);
            return ;
        }
        //验证配置CP参数
        if(!isset($this->arr_params['bk_queue_config']['nns_ext_info']['cp_id']) && strlen($this->arr_params['bk_queue_config']['nns_ext_info']['cp_id']) < 1)
        {
            m_config::timer_write_log($this->timer_path,"[redis节目单池中返回数据：CP ID为空]",$this->child_path);
            return ;
        }
        $arr_cp_ids = explode(',',$this->arr_params['bk_queue_config']['nns_ext_info']['cp_id']);
        unset($this->arr_params['bk_queue_config']['nns_ext_info']['cp_id']);
        //落地项目
        $str_project = evn::get("project");
        if(!isset($str_project) || strlen($str_project) < 1)
        {
            m_config::timer_write_log($this->timer_path,"加载项目[{$str_project}]",$this->child_path);
            return ;
        }
        //传递参数
        $str_timer_params = array();
        foreach($this->arr_params['bk_queue_config']['nns_ext_info'] as $key => $value)
        {
            $str_timer_params[] = $key . '=' . $value;
        }
        $str_timer_params = addslashes(implode(')(',$str_timer_params));
        //PHP执行程序
        $str_exe = evn::get("php_execute_path");
        //遍历执行
        foreach($arr_cp_ids as $id)
        {
            //根据定时器种类，及不同的注入模式，获取当前PHP的执行脚本
            $file_dir = dirname(dirname(dirname(__FILE__)))."/ns_api/{$str_project}/message/{$id}/import_ftp_media.php " . $str_timer_params;
            $crontab_php = $str_exe . " " . $file_dir;
            m_config::timer_write_log($this->timer_path,"执行脚本:[$crontab_php]",$this->child_path);
            //循环根据注入模式 执行对应种类脚本
            if (substr(php_uname(), 0, 7) == "Windows")
            {//windows下执行

                exec($crontab_php);
            }
            else
            {//LINUX下执行

                exec($crontab_php . " > /dev/null &");
            }
        }
    }
}

$timer = new ftp_media_to_db_timer(m_config::return_child_path(__FILE__,'ns_timer'),null,'ftp_media_to_db');
$timer->run();