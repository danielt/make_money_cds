<?php
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_core.m_timer");
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.cdn_import.cdn_import_queue");
\ns_core\m_load::load("ns_model.m_queue_model");
m_config::get_dc();
class delivery_cdn_timer extends m_timer
{
	public function action($params = null)
    {
        m_config::timer_write_log($this->timer_path,'开始执行...',$this->child_path);
        $this->init($params);
        m_config::timer_write_log($this->timer_path,'执行结束...',$this->child_path);
    }

    private function init($params = null)
    {
    	/*$message = new \ns_model\import\message();
    	$message->id = "123";
    	$message->import_mode = "cntv";
    	$message->content = "456";
    	$this->explain_message($message);
    	file_put_contents("/data/logs/1.txt", var_export($message,true));
    	return;*/
    	//获取所有的注入方式
//     	var_dump($this->arr_params,$params);die;
    	//获取对应注入类型的message_queue对象
    	//$message_class = m_load::load("ns_model.import.message_queue");
        if(isset($params['data_info']))
        {
            if(!isset($params['data_info']) || empty($params['data_info']) || !is_array($params['data_info']))
            {
                m_config::timer_write_log($this->timer_path,"查询消息队列数据为空，不在执行",$this->child_path);
                return m_config::return_data(1,'后台查询消息队列数据为空，不在执行');
            }
            $message_list = $params;
        }
        else
        {
        	$cdn_import_queue = new \ns_model\cdn_import\cdn_import_queue($this->timer_model,$this->child_path);
        	//从message弹出1000条信息，这个值应该是注入类型的配置
        	$message_list = $cdn_import_queue->pop();
            if($message_list['ret'] !=0)
            {
                m_config::timer_write_log($this->timer_path,var_export($message_list,true),$this->child_path);
                return ;
            }
            if(!isset($message_list['data_info']) || empty($message_list['data_info']) || !is_array($message_list['data_info']))
            {
                m_config::timer_write_log($this->timer_path,"查询消息队列数据为空，不在执行",$this->child_path);
                return ;
            }
        }
        $project = evn::get("project");
    	$class_name = \ns_core\m_load::load("ns_api.{$project}.delivery.{$this->timer_model}.delivery");
    	foreach ($message_list['data_info'] as $message)
    	{
    		//调用对应的注入类型解释器，如路径：ns_input/{import_mode}/message_explain.class.php
    		$this->explain_message($message);
    	}

    }
    //解释对应的消息
    private function explain_message($message)
    {
    	
    	//$class_name = "\\{$this->timer_model}\\message_explain";
    	#TODO      加命名空间
        $m_queue_model = new \ns_model\m_queue_model();
        $asset_info = $m_queue_model->_get_queue_info($message['nns_ref_id'], $message['nns_type'],true,true,'',$this->timer_model);
        $asset_info = isset($asset_info['data_info']) ? $asset_info['data_info'] : null;
    	$explain_inst = new delivery($message,$asset_info);
    	//$explain_inst->$video_type();
    	$explain_inst->explain(array(
            'c2_info' => $message,
            'video_info' => $asset_info['video'],
            'index_info' => $asset_info['index'],
            'media_info' => $asset_info['media'],
        ));
        //$explain_inst->$message['nns_type']();
    	unset($explain_inst);
    }
}
if(is_array($argv) && !empty($argv))
{
    $timer = new delivery_cdn_timer(m_config::return_child_path(__FILE__,'timer'),$argv[1],null,$argv[2]);
    // $timer = new delivery_cdn_timer(m_config::return_child_path(__FILE__,'timer'),'nmgd',null,'media');
    $timer->run();
}