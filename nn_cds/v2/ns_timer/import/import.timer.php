<?php
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_core.m_timer");
\ns_core\m_load::load("ns_core.m_config");
m_config::get_dc();
class import_timer extends m_timer
{
	public function action($params = null)
    {
        m_config::timer_write_log($this->timer_path,'开始执行...',$this->child_path);
        $this->init($params);
        m_config::timer_write_log($this->timer_path,'执行结束...',$this->child_path);
    }

    private function init($params = null)
    {
    	$arr_cps = m_config::_get_all_cp_info();
    	if($arr_cps['ret'] !=0)
    	{
    	    m_config::timer_write_log($this->timer_path,"cp查询失败：{$arr_cps['reason']}",$this->child_path);
    	    return ;
    	}
    	if(!isset($arr_cps['data_info']) || !is_array($arr_cps['data_info']) || empty($arr_cps['data_info']))
    	{
    	    m_config::timer_write_log($this->timer_path,"cp查询数据为空",$this->child_path);
    	    return ;
    	}
    	$str_exe = evn::get("php_execute_path");
    	foreach ($arr_cps['data_info'] as $cp_val)
    	{
    		//根据定时器种类，及不同的注入模式，获取当前PHP的执行脚本
            $php_path = dirname(__FILE__)."/import_message.timer.php {$cp_val['nns_id']}";
            $crontab_php = $str_exe . " " . $php_path;
    		m_config::timer_write_log($this->timer_path,"执行脚本:[{$str_exe} {$php_path}]",$this->child_path);
            //循环根据注入模式 执行对应种类脚本
            if (substr(php_uname(), 0, 7) == "Windows")//windows下执行
            {
                exec($crontab_php);
            }
            else //LINUX下执行
            {
                exec($crontab_php . " > /dev/null &");
            }
    	}
    }
}
$timer = new import_timer(m_config::return_child_path(__FILE__,'ns_timer'),'','','',__FILE__);
$timer->run();