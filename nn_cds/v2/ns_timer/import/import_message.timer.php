<?php
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_core.m_timer");
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.message.message_queue");
m_config::get_dc();
class import_message_timer extends m_timer
{
    public function action($params = null)
    {
        m_config::timer_write_log($this->timer_path,'开始执行...',$this->child_path);
        $this->init($params);
        m_config::timer_write_log($this->timer_path,'执行结束...',$this->child_path);
    }

    private function init($params = null)
    {
        if(isset($params['data_info']))
        {
            if(!isset($params['data_info']) || empty($params['data_info']) || !is_array($params['data_info']))
            {
                m_config::timer_write_log($this->timer_path,"查询消息队列数据为空，不在执行",$this->child_path);
                return m_config::return_data(1,'后台查询消息队列数据为空，不在执行');
            }
            $message_list = $params;
        }
        else
        {
            $message_queue = new \ns_model\message\message_queue();
            //从message弹出1000条信息，这个值应该是注入类型的配置
            $message_list = $message_queue->pop($this->timer_model);

            if($message_list['ret'] !=0)
            {
                m_config::timer_write_log($this->timer_path,var_export($message_list,true),$this->child_path);
                return m_config::return_data(1,'定时器运行查询数据库执行失败');
            }
            if(!isset($message_list['data_info']) || empty($message_list['data_info']) || !is_array($message_list['data_info']))
            {
                m_config::timer_write_log($this->timer_path,"查询消息队列数据为空，不在执行",$this->child_path);
                return m_config::return_data(1,'定时器运行查询消息队列数据为空，不在执行');
            }
        }
        $project = evn::get("project");
        $class_name = \ns_core\m_load::load("ns_api.{$project}.message.{$this->timer_model}.message");
        foreach ($message_list['data_info'] as $message)
        {
            //调用对应的注入类型解释器，如路径：ns_input/{import_mode}/message_explain.class.php
            $result = $this->explain_message($message);
            m_config::timer_write_log($this->timer_path,"执行消息【{$message['nns_message_id']}】结果：".var_export($result,true),$this->child_path);
            //此处还有反馈其他状态，如主媒资未注入、分集未注入、XML解析失败等
            if($result['ret'] != 0)
            {
                nl_message::update_message_state(m_config::get_dc(),$message['nns_id'],array('nns_message_state'=>4),true);
            }
            else
            {
                nl_message::update_message_state(m_config::get_dc(),$message['nns_id'],array('nns_message_state'=>3),true);
            }
        }
        return m_config::return_data(0,'执行完毕');
    }

    //解释对应的消息
    private function explain_message($message)
    {
        //$class_name = "\\{$this->timer_model}\\message_explain";
        $explain_inst = new message($message);
        $result = $explain_inst->explain($message);
        unset($explain_inst);
        return $result;
    }
}

if(is_array($argv) && !empty($argv))
{
    $timer = new import_message_timer(m_config::return_child_path(__FILE__,'ns_timer'), $argv[1], null, '', __FILE__ . " {$argv[1]}");
    // $timer = new import_message_timer(m_config::return_child_path(__FILE__,'ns_timer'),'starcor');
    $timer->run();
}