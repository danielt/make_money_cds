<?php
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_core.nn_timer");
\ns_core\m_load::load("ns_model.import.center_import_command_queue");

class import_command_timer extends \ns_core\nn_timer{
	public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->init($params);
        $this->msg('执行结束...');
    }

    private function init($params = null){
    	/*$message = new \ns_model\import\message();
    	$message->id = "123";
    	$message->import_mode = "cntv";
    	$message->content = "456";
    	$this->explain_message($message);
    	file_put_contents("/data/logs/1.txt", var_export($message,true));
    	return;*/

    	//获取所有的注入方式
    	$import_mode =  $this->timer_model;
    	//获取对应注入类型的message_queue对象
    	//$message_class = m_load::load("ns_model.import.message_queue");
    	$command_inst = new \ns_model\import\center_import_command_queue();
    	//从message弹出1000条信息，这个值应该是注入类型的配置
    	$command_list = $command_inst->pop_message($import_mode,1000);

    	foreach ($command_list as $command){
    		//调用对应的注入类型解释器，如路径：ns_input/{import_mode}/message_explain.class.php
    		$this->explain_message($message);
    	}
    }
    //解释对应的消息
    private function explain_message($message)
    {
    	
    }
}
//file_put_contents("/data/logs/1.txt", var_export($argv,true));
$timer = new import_command_timer("import_command",$argv[1]);
$timer->run();
