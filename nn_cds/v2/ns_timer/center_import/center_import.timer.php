<?php
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_core.m_timer");
\ns_core\m_load::load("ns_core.m_config");
m_config::get_dc();
class import_timer extends m_timer
{
	public function action($params = null)
    {
        m_config::timer_write_log($this->timer_path,'开始执行...',$this->child_path);
        $this->init($params);
        m_config::timer_write_log($this->timer_path,'执行结束...',$this->child_path);
    }

    private function init($params = null)
    {
    	$str_exe = evn::get("php_execute_path");
		//根据定时器种类，及不同的注入模式，获取当前PHP的执行脚本,
		$php_path = dirname(__FILE__)."/center_import_queue.timer.php center_import_queue > /dev/null &";
		m_config::timer_write_log($this->timer_path,"执行脚本:[{$str_exe} {$php_path}]",$this->child_path);
		//循环根据注入模式 执行对应种类脚本
		exec(evn::get("php_execute_path")." {$php_path}");
    }
}
$timer = new import_timer(m_config::return_child_path(__FILE__,'ns_timer'));
$timer->run();