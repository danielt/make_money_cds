<?php
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_core.m_timer");
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.center_import.center_import_queue");
m_config::get_dc();
class center_import_timer extends m_timer
{
	public function action($params = null)
    {
        m_config::timer_write_log($this->timer_path,'开始执行...',$this->child_path);
        $this->init($params);
        m_config::timer_write_log($this->timer_path,'执行结束...',$this->child_path);
    }

    private function init($params = null)
    {
    	if(isset($params['data_info']))
    	{
    	    if(!isset($params['data_info']) || empty($params['data_info']) || !is_array($params['data_info']))
    	    {
    	        m_config::timer_write_log($this->timer_path,"查询中心注入指令队列数据为空，不在执行",$this->child_path);
    	        return m_config::return_data(1,'后台查询中心注入指令队列数据为空，不在执行');
    	    }
    	    $message_list = $params;
    	}
    	else
    	{
    	    $arr_sps = m_config::_get_all_sp_info();
    	    if($arr_sps['ret'] !=0)
    	    {
    	        m_config::timer_write_log($this->timer_path,"sp查询失败：{$arr_sps['reason']}",$this->child_path);
    	        return m_config::return_data(1,'定时器运行查询SP数据库执行失败');
    	    }
    	    if(!isset($arr_sps['data_info']) || !is_array($arr_sps['data_info']) || empty($arr_sps['data_info']))
    	    {
    	        m_config::timer_write_log($this->timer_path,"SP查询数据为空",$this->child_path);
    	        return m_config::return_data(1,'定时器运行查询SP数据为空');
    	    }
	        $center_import_queue = new center_import_queue();
    	    foreach ($arr_sps['data_info'] as $sp_val)
    	    {
    	        //从message弹出1000条信息，这个值应该是注入类型的配置
    	        $center_import_list = $center_import_queue->pop($sp_val['nns_id']);
    	        if($center_import_list['ret'] !=0)
    	        {
    	            m_config::timer_write_log($this->timer_path,var_export($center_import_list,true),$this->child_path);
    	            return m_config::return_data(1,'定时器运行查询数据库执行失败');
    	        }
    	        if(!isset($center_import_list['data_info']) || empty($center_import_list['data_info']) || !is_array($center_import_list['data_info']))
    	        {
    	            m_config::timer_write_log($this->timer_path,"查询中心注入指令队列数据为空，不在执行",$this->child_path);
    	            return m_config::return_data(1,'定时器运行查询中心注入指令队列数据为空，不在执行');
    	        }
    	        foreach ($center_import_list['data_info'] as $center_import_list_data)
    	        {
    	            
    	        }
    	    }
    	}
        $project = evn::get("project");
    	$class_name = \ns_core\m_load::load("ns_api.{$project}.message.{$this->timer_model}.message");
    	foreach ($center_import_list['data_info'] as $center_import)
    	{
    		//调用对应的注入类型解释器，如路径：ns_input/{import_mode}/message_explain.class.php
    		$result = $this->explain_message($message);
            m_config::timer_write_log($this->timer_path,"执行中心注入指令【{$message['nns_message_id']}】结果：".var_export($result,true),$this->child_path);
    		//此处还有反馈其他状态，如主媒资未注入、分集未注入、XML解析失败等
    		if($result['ret'] != 0)
    		{
    		    nl_message::update_message_state(m_config::get_dc(),$message['nns_id'],array('nns_message_state'=>4),true);
    		}
    		else
    		{
    		    nl_message::update_message_state(m_config::get_dc(),$message['nns_id'],array('nns_message_state'=>3),true);
    		}
    	}
        return m_config::return_data(0,'执行完毕');
    }
    
    //解释对应的中心注入指令
    private function explain_message($message)
    {
    	//$class_name = "\\{$this->timer_model}\\message_explain";
    	$explain_inst = new message($message);
    	$result = $explain_inst->explain($message);
    	unset($explain_inst);
    	return $result;
    }
}
if(is_array($argv) && !empty($argv))
{
    $timer = new center_import_timer(m_config::return_child_path(__FILE__,'timer'),$argv[1]);
    // $timer = new import_message_timer(m_config::return_child_path(__FILE__,'timer'),'starcor');
    $timer->run();
}