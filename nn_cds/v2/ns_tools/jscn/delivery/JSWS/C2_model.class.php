<?php
/**
 * starcor CDN标准规范ADI CDN注入
 * @author liangpan
 */
class C2_model
{
    private $arr_vod_video_img = array(
        'nns_image0'=>3,
        'nns_image1'=>2,
        'nns_image2'=>1,
        'nns_image_v'=>5,
        'nns_image_s'=>6,
        'nns_image_h'=>4,
    );
    
    private $arr_vod_index_img = array(
        'nns_image'=>1,
    );
    
    private $arr_channel_img = array(
        'nns_image0'=>3,
        'nns_image1'=>2,
        'nns_image2'=>1,
        'nns_image_v'=>5,
        'nns_image_s'=>6,
        'nns_image_h'=>4,
        'nns_image_logo'=>7,
    );
    
    private $arr_playbill_img = array(
        'nns_image0'=>3,
        'nns_image1'=>2,
        'nns_image2'=>1,
        'nns_image3'=>4,
        'nns_image4'=>5,
        'nns_image5'=>6,
    );
    
    /**
     * 组装img
     * @param unknown $arr_video
     * @param unknown $type
     */
    private function make_image($arr_video,$parent_type,$parent_id,$action)
    {
        $arr_map = null;
        $str_picture_obj = $str_picture_mapping = '';
        $img_url = isset($this->arr_sp_config['img_ftp']) ? $this->arr_sp_config['img_ftp'] : '';
        $img_url = rtrim(rtrim(trim($img_url),'/'),'\\');
        switch ($parent_type)
        {
            case 'Series':
                $arr_map = $this->arr_vod_video_img;
                break;
            case 'Program':
                $arr_map = $this->arr_vod_video_img;
                break;
            case 'Channel':
                $arr_map = $this->arr_channel_img;
                break;
            case 'ScheduleRecord':
                $arr_map = $this->arr_playbill_img;
                break;
        }
        if(!is_array($arr_map) || empty($arr_map))
        {
            return null;
        }
        foreach ($arr_map as $key=>$val)
        {
            if(isset($arr_video[$key]) && strlen($arr_video[$key]) >0)
            {
                $id = md5($key.$arr_video[$key]);
                $picture = array(
                    'FileURL'=>$img_url.'/'.ltrim(ltrim(trim($arr_video[$key]),'/'),'\\'),
                    'Type'=>$val,
                    'Description'=>'',
                );
                $str_picture_obj .= $this->make_object('Picture', $id,$action,$picture);
                if(isset($this->arr_sp_config['c2_import_cdn_mapping_model']) && $this->arr_sp_config['c2_import_cdn_mapping_model'] == '1')
                {
                    $str_picture_mapping .= $this->make_mapping($parent_type, 'Picture', $parent_id, $id, $action);
                }
                else
                {
                    $str_picture_mapping .= $this->make_mapping('Picture', $parent_type, $id, $parent_id, $action);
                }
            }
        }
        return array(
            'picture'=>$str_picture_obj,
            'mapping'=>$str_picture_mapping,
        );
    }
    
    /**
     * 组装mapping
     * @param unknown $parent_type
     * @param unknown $type
     * @param unknown $parent_id
     * @param unknown $id
     * @param unknown $action
     * @param string $arr_params
     */
    private function make_mapping($parent_type,$type,$parent_id,$id,$action,$arr_params=null)
    {
        $str_xml ='<Mapping ParentType="'.$parent_type.'" ParentID="'.$parent_id.'" ElementType="'.$type.'" ElementID="'.$id.'" ParentCode="'.$parent_id.'" ElementCode="'.$id.'" Action="'.$action.'" Type="1">';
        if(is_array($arr_params) && !empty($arr_params))
        {
            foreach ($arr_params as $key=>$val)
            {
                $str_xml.=	'<Property Name="'.$key.'"><![CDATA['.$val.']]></Property>';
            }
        }
        $str_xml.='</Mapping>';
        return $str_xml;
    }
    
    
	
	
	/**
	 * 生成file xml
	 * @param unknown $arr_file
	 * @param string $arr_file_ex
	 * @var string Description            
	 *     说明：描述信息
	 * @var string FileURL
	 *     说明： 媒体文件 URL ftp://username:password@ip:port/...  标准 FTP协议 或者 http:// ip:port/... 
	 *     注释： 对托管内容注入方式下填写，支持 FTP 和 HTTP 协议获取内容 对非托管内容预注入方式下 则 为 空 ， 填 写ServiceURL 属性 当FileURL和ServiceURL均有值时，
	 *	               首选采用该属性值去获取内容。注入 HLS 内容时，如果属性值为 m3u8 文件结尾，CDN 获取内容时，获取整个 m3u8 的上级目录
	 * @var string ServiceURL
	 *     说明： 内容服务 URL
	 *     注释： 需要指定 CDN 服务 URL 的内容注入/预注入内容接入或实时回源接入内容删除时填写。为 CDN 提供服务的内容服务调用 URL，当 ServiceURL 有值时，该URL 前缀信息应和 CDN 服务配置
     *           表中对应域内容提供 商 的PlaybackURLprefix 保持一致，用户需要基于本URL 访问注入内容
	 * @var string FileSize 
	 *     说明： 文件大小，单位为 Byte
	 * @var string Domain            
	 *     说明： 发布到融合 CDN 后的服务域和服务协议 
	 *     注释： 包含 ServiceDomain 和ProtocolType 两层含义，为 4 字节整数，其中最高三 个 字 节 为ServiceDomain，表示预分发网络域，最低 3 位取值如下： 
	 *               0x000001-IPTV 网络 
	 *               0x000002-互联网网络 
	 *               0x000004-移动网络 
	 *               0x000008~0x000080-预留 
	 *            最 低 一 个 字 节 为ProtocolType，表示服务协议，最低 4 位取值如下：
	 *               0x01-IPTV TS RTSP， 
	 *               0x02-HPD 
	 *               0x04-ISMA RTSP 
	 *               0x08-HLS。 
	 *            如果 Domain 为空，则采用该 CMSID 缺省分发域，缺省分发域可配置
	 * @var string Hotdegree             
	 *     说明： 发布到融合 CDN 时使用热度表示
	 *     注释： 0（普通），1（高） 缺省为 0，该字段值越大，表示优先级越高。CDN 应根据优先级安排处理顺序
	 * @var string deliverTime              
	 *     说明： 内容分发时间
	 *     注释： 北京时间，采用十四位的字 符 串 格 式YYYYMMDDHHMMSS，例如： 20040101123000 ， 代 表 2004 年 1 月 1 日 12点 30 分 00 秒。说明 如果不携带该字段，表示立即分发
	 */
	private function do_file($video_data)
	{
	    $str_Action = ($video_data['file']['base_info']['nns_deleted'] !='1') ? 'DELETE' : ($video_data['file']['base_info']['nns_create_time'] != $video_data['file']['base_info']['nns_modify_time'] ? 'UPDATE' : 'REGIST');
        $xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
	    $xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
	    $xml_str .=    '<Objects>';
	    $xml_str .=        '<Object ElementType="File" PhysicalContentID="'.$video_data['file']['cdn_info']['nns_id'].'" Action="'.$str_Action.'">';
	    $xml_str .= 	      '<Property Name="Description"><![CDATA['.$video_data['file']['base_info']['nns_description'].']]></Property>';
		$xml_str .=	          '<Property Name="FileURL"><![CDATA['.$video_data['file']['base_info']['nns_file_url'].']]></Property>';
		$xml_str .=	          '<Property Name="ServiceURL"><![CDATA['.$video_data['file']['base_info']['nns_service_url'].']]></Property>';
		$xml_str .=	          '<Property Name="FileSize"><![CDATA['.$video_data['file']['base_info']['nns_file_size'].']]></Property>';
	    $xml_str .= 	      '<Property Name="Domain"><![CDATA[257]]></Property>';
	    $xml_str .= 	      '<Property Name="HotDgree"><![CDATA[0]]></Property>';
		$xml_str .=	          '<Property Name="deliverTime"><![CDATA['.date('YmdHis').']]></Property>';
	    $xml_str .=        '</Object>';
	    $xml_str .= 	'</Objects>';
	    $xml_str .= '</ADI>';
	    return $xml_str;
	}
	
	/**
	 * 栏目发布
	 */
	public function do_category($video_data)
	{
	    $action = 'REGIST';
	    $xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
	    $xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
	    $xml_str .=    '<Objects>';
	    $xml_str .=        '<Object ElementType="Category" ContentID="'.$video_data['category']['cdn_info']['nns_id'].'" Action="'.$action.'">';
	    $xml_str .= 	       '<Property Name="ParentID"><![CDATA['.$video_data['category']['base_info']['parent_category'].']]></Property>';
	    $xml_str .=	           '<Property Name="Name"><![CDATA['.$video_data['category']['base_info']['name'].']]></Property>';
	    $xml_str .=	           '<Property Name="Sequence"><![CDATA[0]]></Property>';
	    $xml_str .=	           '<Property Name="Status"><![CDATA[1]]></Property>';
	    $xml_str .= 	       '<Property Name="Description"><![CDATA[]]></Property>';
	    $xml_str .=        '</Object>';
	    $xml_str .= 	'</Objects>';
	    $xml_str .= '</ADI>';
	    return $xml_str;
	}
	
	
	/**
	 * 点播主媒资CDN
	 * @param unknown $movie_id
	 */
	public function do_video($video_data)
	{
	    $str_Action = ($video_data['video']['base_info']['nns_deleted'] !='1') ? 'DELETE' : ($video_data['video']['base_info']['nns_create_time'] != $video_data['video']['base_info']['nns_modify_time'] ? 'UPDATE' : 'REGIST');
        $result_picture = $this->make_image($video_data['video']['base_info'], 'Series', $video_data['video']['cdn_info']['nns_id'], $str_Action);
	    $xml_str  ='<?xml version="1.0" encoding="UTF-8"?>';
	    $xml_str .='<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
	    $xml_str .=    '<Objects>';
	    $xml_str .=        '<Object ElementType="Series" ContentID="'.$video_data['video']['cdn_info']['nns_id'].'" Action="'.$str_Action.'">';
	    $xml_str .=            '<Property Name="Name"><![CDATA['.$video_data['video']['base_info']['nns_name'].']]></Property>';
	    $xml_str .=            '<Property Name="OrderNumber"><![CDATA[1]]></Property>';
	    $xml_str .=            '<Property Name="OriginalName"><![CDATA['.$video_data['video']['base_info']['nns_name'].']]></Property>';
	    $xml_str .=            '<Property Name="AliasName"><![CDATA['.$video_data['video']['base_info']['nns_alias_name'].']]></Property>';
	    $xml_str .=            '<Property Name="EnglishName"><![CDATA['.$video_data['video']['base_info']['nns_eng_name'].']]></Property>';
	    $xml_str .=            '<Property Name="SortName"><![CDATA['.$video_data['video']['base_info']['nns_name'].']]></Property>';
	    $xml_str .=            '<Property Name="SearchName"><![CDATA['.$video_data['video']['base_info']['nns_name'].']]></Property>';
	    $xml_str .=            '<Property Name="OrgAirDate"><![CDATA['.$video_data['video']['base_info']['nns_copyright_date'].']]></Property>';
	    $xml_str .=            '<Property Name="ReleaseYear"><![CDATA['.$video_data['video']['base_info']['nns_show_time'].']]></Property>';
	    $xml_str .=            '<Property Name="LicensingWindowStart"><![CDATA[]]></Property>';
	    $xml_str .=            '<Property Name="LicensingWindowEnd"><![CDATA[]]></Property>';
	    $xml_str .=            '<Property Name="CPContentID"><![CDATA['.$video_data['video']['base_info']['nns_cp_id'].']]></Property>';
	    $xml_str .=            '<Property Name="DisplayAsNew"><![CDATA[0]]></Property>';
	    $xml_str .=            '<Property Name="DisplayAsLastChance"><![CDATA[0]]></Property>';
	    $xml_str .=            '<Property Name="Macrovision"><![CDATA[1]]></Property>';
	    $xml_str .=            '<Property Name="Price"><![CDATA[1]]></Property>';
	    $xml_str .=            '<Property Name="VolumnCount"><![CDATA['.$video_data['video']['base_info']['nns_all_index'].']]></Property>';
	    $xml_str .=            '<Property Name="NewCount"><![CDATA['.$video_data['video']['base_info']['nns_new_index'].']]></Property>';
	    $xml_str .=            '<Property Name="Status"><![CDATA[1]]></Property>';
	    $xml_str .=            '<Property Name="Description"><![CDATA['.$video_data['video']['base_info']['nns_summary'].']]></Property>';
	    $xml_str .=            '<Property Name="ContentProvider"><![CDATA['.$video_data['video']['base_info']['nns_import_source'].']]></Property>';
	    $xml_str .=            '<Property Name="KeyWords"><![CDATA['.$video_data['video']['base_info']['nns_keyword'].']]></Property>';
	    $xml_str .=            '<Property Name="OriginalCountry"><![CDATA['.$video_data['video']['base_info']['nns_area'].']]></Property>';
	    $xml_str .=            '<Property Name="ActorDisplay"><![CDATA['.$video_data['video']['base_info']['nns_actor'].']]></Property>';
	    $xml_str .=            '<Property Name="WriterDisplay"><![CDATA['.$video_data['video']['base_info']['nns_screenwriter'].']]></Property>';
	    $xml_str .=            '<Property Name="Language"><![CDATA['.$video_data['video']['base_info']['nns_language'].']]></Property>';
	    $xml_str .=            '<Property Name="Kind"><![CDATA['.$video_data['video']['base_info']['nns_kind'].']]></Property>';
	    $xml_str .=            '<Property Name="Duration"><![CDATA['.$video_data['video']['base_info']['nns_view_len'].']]></Property>';
	    $xml_str .=            '<Property Name="CategoryName"><![CDATA[电视剧]]></Property>';
	    $xml_str .=            '<Property Name="CategoryID"><![CDATA[10000140142]]></Property>';
	    $xml_str .=            '<Property Name="PlayCount"><![CDATA[0]]></Property>';
	    $xml_str .=            '<Property Name="CategorySort"><![CDATA[0]]></Property>';
	    $xml_str .=            '<Property Name="Tags"><![CDATA['.$video_data['video']['base_info']['nns_tag'].']]></Property>';
	    $xml_str .=            '<Property Name="ViewPoint"><![CDATA['.$video_data['video']['base_info']['nns_summary'].']]></Property>';
	    $xml_str .=            '<Property Name="StarLevel"><![CDATA[6]]></Property>';
	    $xml_str .=            '<Property Name="Rating"/>';
	    $xml_str .=            '<Property Name="Awards"/>';
	    $xml_str .=            '<Property Name="Sort"><![CDATA[0]]></Property>';
	    $xml_str .=            '<Property Name="Hotdegree"><![CDATA[0]]></Property>';
	    $xml_str .=            '<Property Name="Reserve1"/>';
	    $xml_str .=            '<Property Name="Reserve1"/>';
	    $xml_str .=        '</Object>';
	    if(isset($result_picture['picture']) && strlen($result_picture['picture']) >0)
	    {
	        $xml_str .= $result_picture['picture'];
	    }
	    $xml_str .=    '</Objects>';
	    $xml_str .=    '<Mappings>';
	    if(isset($result_picture['mapping']) && strlen($result_picture['mapping']) >0)
	    {
	        $xml_str .= $result_picture['mapping'];
	    }
	    $xml_str .=	   '</Mappings>';
	    $xml_str .='</ADI>';
	    return $xml_str;
	}
	
	
	/**
	 * 点播分集CDN
	 * @param unknown $movie_id
	 */
	public function do_index($video_data)
	{
	    $video_data['index']['base_info']['nns_index'] = (isset($video_data['index']['base_info']['nns_index']) && $video_data['index']['base_info']['nns_index'] >=0) ? $video_data['index']['base_info']['nns_index']+1 : 1;
	    $str_Action = ($video_data['index']['base_info']['nns_deleted'] !='1') ? 'DELETE' : ($video_data['index']['base_info']['nns_create_time'] != $video_data['index']['base_info']['nns_modify_time'] ? 'UPDATE' : 'REGIST');
        $result_picture = $this->make_image($video_data['index']['base_info'], 'Program', $video_data['index']['cdn_info']['nns_id'], $str_Action);
	    $xml_str  ='<?xml version="1.0" encoding="UTF-8"?>';
	    $xml_str .='<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
	    $xml_str .=    '<Objects>';
	    $xml_str .=        '<Object ElementType="Program" ContentID="'.$video_data['index']['cdn_info']['nns_id'].'" Action="'.$str_Action.'">';
	    $xml_str .=		       '<Property Name="Name"><![CDATA['.$video_data['index']['base_info']['nns_name'].']]></Property>';
	    $xml_str .=		       '<Property Name="CPContentID"><![CDATA['.$video_data['index']['base_info']['nns_cp_id'].']]></Property>';
	    $xml_str .=		       '<Property Name="OrderNumber"><![CDATA[1]]></Property>';//0: No DRM 1: BES DRM
	    $xml_str .=		       '<Property Name="OriginalName"><![CDATA['.$video_data['index']['base_info']['nns_name'].']]></Property>';//0: No DRM  1: BES DRM
	    $xml_str .=		       '<Property Name="SortName"><![CDATA[1]]></Property>';
	    $xml_str .=		       '<Property Name="Sequence"><![CDATA['.$video_data['index']['base_info']['nns_index'].']]></Property>';
	    $xml_str .=		       '<Property Name="SortName"><![CDATA[1]]></Property>';
	    $xml_str .=		       '<Property Name="SearchName"><![CDATA['.$video_data['index']['base_info']['nns_name'].']]></Property>';
	    $xml_str .=		       '<Property Name="ActorDisplay"><![CDATA['.$video_data['index']['base_info']['nns_actor'].']]></Property>'; //0: 4x3   1: 16x9(Wide)
	    $xml_str .=		       '<Property Name="OriginalCountry"><![CDATA[]]></Property>';//0:无字幕 1:有字幕
	    $xml_str .=		       '<Property Name="Language"><![CDATA[]]></Property>';
	    $xml_str .=		       '<Property Name="ReleaseYear"><![CDATA[]]></Property>';
	    $xml_str .=		       '<Property Name="OrgAirDate"><![CDATA['.$video_data['index']['base_info']['nns_kbps'].']]></Property>';
	    $xml_str .=		       '<Property Name="LicensingWindowStart"><![CDATA[]]></Property>';
	    $xml_str .=		       '<Property Name="LicensingWindowEnd"><![CDATA[]]></Property>';
	    $xml_str .=		       '<Property Name="DisplayAsNew"><![CDATA[]]></Property>';
	    $xml_str .=		       '<Property Name="DisplayAsLastChance"><![CDATA[]]></Property>';
	    $xml_str .=		       '<Property Name="Macrovision"><![CDATA[1]]></Property>';
	    $xml_str .=		       '<Property Name="Description"><![CDATA['.$video_data['index']['base_info']['nns_summary'].']]></Property>';
	    $xml_str .=		       '<Property Name="PriceTaxIn"><![CDATA[]]></Property>';
	    $xml_str .=		       '<Property Name="Status"><![CDATA[1]]></Property>';
	    $xml_str .=		       '<Property Name="SourceType"><![CDATA[1]]></Property>';
	    $xml_str .=		       '<Property Name="SeriesFlag"><![CDATA[1]]></Property>';
	    $xml_str .=		       '<Property Name="ContentProvider"><![CDATA['.$video_data['index']['base_info']['nns_import_source'].']]></Property>';
	    $xml_str .=		       '<Property Name="KeyWords"><![CDATA['.$video_data['index']['base_info']['nns_summary'].']]></Property>';
	    $xml_str .=		       '<Property Name="Tags"><![CDATA[]]></Property>';
	    $xml_str .=		       '<Property Name="ViewPoint"><![CDATA['.$video_data['index']['base_info']['nns_watch_focus'].']]></Property>';
	    $xml_str .=		       '<Property Name="StarLevel"><![CDATA[]]></Property>';
	    $xml_str .=		       '<Property Name="Rating"><![CDATA[]]></Property>';
	    $xml_str .=		       '<Property Name="Awards"><![CDATA[]]></Property>';
	    $xml_str .=		       '<Property Name="Duration"><![CDATA['.$video_data['index']['base_info']['nns_time_len'].']]></Property>';
	    $xml_str .=		       '<Property Name="Reserve1"><![CDATA[]]></Property>';
	    $xml_str .=		       '<Property Name="Reserve2"><![CDATA[]]></Property>';
	    $xml_str .=		       '<Property Name="Reserve3"><![CDATA[]]></Property>';
	    $xml_str .=		       '<Property Name="Reserve4"><![CDATA[]]></Property>';
	    $xml_str .=		       '<Property Name="Reserve5"><![CDATA[]]></Property>';
	    $xml_str .=        '</Object>';
	    if(isset($result_picture['picture']) && strlen($result_picture['picture']) >0)
	    {
	        $xml_str .= $result_picture['picture'];
	    }
	    $xml_str .=	   '</Objects>';
	    $xml_str .=    '<Mappings>';
	    $xml_str .=     $this->make_mapping('Series','Program',$video_data['video']['cdn_info']['nns_id'],$video_data['index']['cdn_info']['nns_id'],$str_Action,array('Sequence'=>$video_data['index']['base_info']['nns_index']));
	    if(isset($result_picture['mapping']) && strlen($result_picture['mapping']) >0)
	    {
	        $xml_str .= $result_picture['mapping'];
	    }
	    $xml_str .=	   '</Mappings>';
	    $xml_str .= '</ADI>';
	    return $xml_str;
	}
	
	
	/**
	 * 组装片源信息
	 * @param unknown $arr_info 片源基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 * @var string FileURL
	 *     说明： 媒体文件 URL ftp://username:password@ip:port/...  标准 FTP协议 或者 http:// ip:port/...
	 *     注释： 对托管内容注入方式下填写，支持 FTP 和 HTTP 协议获取内容 对非托管内容预注入方式下 则 为 空 ， 填 写ServiceURL 属性 当FileURL和ServiceURL均有值时，
	 *	               首选采用该属性值去获取内容。注入 HLS 内容时，如果属性值为 m3u8 文件结尾，CDN 获取内容时，获取整个 m3u8 的上级目录
	 * @var string ServiceURL
	 *     说明： 内容服务 URL
	 *     注释： 需要指定 CDN 服务 URL 的内容注入/预注入内容接入或实时回源接入内容删除时填写。为 CDN 提供服务的内容服务调用 URL，当 ServiceURL 有值时，该URL 前缀信息应和 CDN 服务配置
	 *           表中对应域内容提供 商 的PlaybackURLprefix 保持一致，用户需要基于本URL 访问注入内容
	 * @var string CPContentID
	 *     说明： CP 对于媒体文件的标识
	 * @var string SourceDRMType
	 *     说明： 0: No DRM 1: BES DRM
	 * @var string DestDRMType
	 *     说明： 0: No DRM 1: BES DRM
	 * @var string AudioType
	 *     说明： 0: 其他 1: Monaural 单声道 2: Stereo 多声道 3: Two-nation monaural 双单声道 4: Two-nation stereo 双多声道  5: AC3(5:1 channel) AC3声道
	 * @var string ScreenFormat
	 *     说明： 0: 4x3   1: 16x9(Wide)
	 * @var string ClosedCaptioning
	 *     说明： 字幕标志 0:无字幕 1:有字幕
	 * @var string Duration
	 *     说明： 播放时长 HHMISSFF （时分秒帧）
	 * @var string FileSize
	 *     说明： 文件大小，单位为 Byte
	 * @var string BitRateType
	 *     说明： 码流 1: 400k 2: 700K 3:  1.3M 4: 2M 5: 2.5M 6:  8M 7: 10M  8：15M 9：20M 10：30M
	 * @var string VideoType
	 *     说明： 编码格式： 1. H.264 2. MPEG4 3. AVS 4. MPEG2 5. MP3 6. WMV 7. H.265
	 * @var string AudioEncodingType
	 *     说明： 编码格式： 1. MP2 2. AAC 3. AMR 4. Mp3
	 * @var string Resolution
	 *     说明： 分辨率类型 1. QCIF 2. QVGA 3. 2/3 D1 4. 3/4 D1 5. D1 6. 720P 7. 1080i 8. 1080P 9. 2k 11. 4k 13. 8k
	 * @var string VideoProfile
	 *     说明： 1. Simple 2. Advanced Simple 3. Baseline 4. Main 5. High 6. JiZhun
	 * @var string SystemLayer
	 *     说明： 1. TS 2. 3GP
	 * @var string Domain
	 *     说明： 发布到融合 CDN 后的服务域和服务协议
	 *     注释： 包含 ServiceDomain 和ProtocolType 两层含义，为 4 字节整数，其中最高三 个 字 节 为ServiceDomain，表示预分发网络域，最低 3 位取值如下：
	 *               0x000001-IPTV 网络
	 *               0x000002-互联网网络
	 *               0x000004-移动网络
	 *               0x000008~0x000080-预留
	 *            最 低 一 个 字 节 为ProtocolType，表示服务协议，最低 4 位取值如下：
	 *               0x01-IPTV TS RTSP，
	 *               0x02-HPD
	 *               0x04-ISMA RTSP
	 *               0x08-HLS。
	 *            如果 Domain 为空，则采用该 CMSID 缺省分发域，缺省分发域可配置
	 * @var string Hotdegree
	 *     说明： 发布到融合 CDN 时使用热度表示
	 *     注释： 0（普通），1（高） 缺省为 0，该字段值越大，表示优先级越高。CDN 应根据优先级安排处理顺序
	 * @var string deliverTime
	 *     说明： 内容分发时间
	 *     注释： 北京时间，采用十四位的字 符 串 格 式YYYYMMDDHHMMSS，例如： 20040101123000 ， 代 表 2004 年 1 月 1 日 12点 30 分 00 秒。说明 如果不携带该字段，表示立即分发
	 */
	public function do_media($video_data)
	{
		$str_Action = ($video_data['media']['base_info']['nns_deleted'] !='1') ? 'DELETE' : ($video_data['media']['base_info']['nns_create_time'] != $video_data['media']['base_info']['nns_modify_time'] ? 'UPDATE' : 'REGIST');
        $action = ($this->arr_params['bk_c2_task_info']['nns_action'] !='destroy') ? ($this->arr_params['bk_c2_task_info']['nns_action'] == 'update') ? 'UPDATE' : 'REGIST' : 'DELETE';
		$xml_str  ='<?xml version="1.0" encoding="UTF-8"?>';
		$xml_str .='<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
		$xml_str .=   '<Objects>';
		$xml_str .=       '<Object ElementType="Movie" ContentID="'.$video_data['media']['cdn_info']['nns_id'].'" Action="'.$str_Action.'" Type="1">';
		$xml_str .=           '<Property Name="FileURL"><![CDATA['.$video_data['media']['base_info']['nns_url'].']]></Property>';
		$xml_str .=           '<Property Name="CPContentID"><![CDATA['.$video_data['media']['base_info']['nns_cp_id'].']]></Property>';
		$xml_str .=           '<Property Name="SourceDRMType"><![CDATA[0]]></Property>';
		$xml_str .=           '<Property Name="DestDRMType"><![CDATA[0]]></Property>';
		$xml_str .=           '<Property Name="AudioType"><![CDATA[1]]></Property>';
		$xml_str .=           '<Property Name="ScreenFormat"><![CDATA[1]]></Property>';
		$xml_str .=           '<Property Name="ClosedCaptioning"><![CDATA[1]]></Property>';
		$xml_str .=           '<Property Name="Tags"><![CDATA['.$video_data['media']['base_info']['nns_tag'].']]></Property>';
		$xml_str .=           '<Property Name="Duration"><![CDATA['.$video_data['media']['base_info']['nns_file_time_len'].']]></Property>';
		$xml_str .=           '<Property Name="FileSize"><![CDATA['.$video_data['media']['base_info']['nns_file_size'].']]></Property>';
		$xml_str .=           '<Property Name="BitRateType"><![CDATA['.$video_data['media']['base_info']['nns_kbps'].']]></Property>';
		$xml_str .=           '<Property Name="VideoType"><![CDATA[1]]></Property>';
		$xml_str .=           '<Property Name="AudioEncodingType"><![CDATA[4]]></Property>';
		$xml_str .=           '<Property Name="Resolution"><![CDATA['.$video_data['media']['base_info']['nns_file_resolution'].']]></Property>';
		$xml_str .=           '<Property Name="MediaMode"><![CDATA[1]]></Property>';
		$xml_str .=           '<Property Name="SystemLayer"><![CDATA[1]]></Property>';
		$xml_str .=           '<Property Name="ServiceType"><![CDATA['.$video_data['media']['base_info']['nns_media_service'].']]></Property>';
		$xml_str .=           '<Property Name="Domain"><![CDATA['.$video_data['media']['base_info']['nns_domain'].']]></Property>';
		$xml_str .=           '<Property Name="Hotdegree"><![CDATA[0]]></Property>';
		$xml_str .=       '</Object>';
		$xml_str .=   '</Objects>';
		$xml_str .=   '<Mappings>';
		$xml_str .=   $this->make_mapping('Program','Movie',$video_data['index']['cdn_info']['nns_id'],$video_data['media']['cdn_info']['nns_id'],$str_Action);
		if($action != 'DELETE')
		{
		      $xml_str .=     $this->make_mapping('Series','Program',$video_data['video']['cdn_info']['nns_id'],$video_data['index']['cdn_info']['nns_id'],$str_Action);
		}
		$xml_str .=   '</Mappings>';
		$xml_str .='</ADI>';
		return $xml_str;
	}
	
	/**
	 * 直播频道处理
	 */
	public function do_live($video_data)
	{
	    $str_Action = ($video_data['live']['base_info']['nns_deleted'] !=1) ? ($video_data['live']['base_info']['nns_modify_time'] > $video_data['live']['base_info']['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
	    $result_picture = $this->make_image($video_data['live']['base_info'], 'Channel', $video_data['live']['cdn_info']['nns_id'], $str_Action);
	    $TimeShift = (isset($video_data['live']['ex_info']['live_type']) && (strpos(strtolower($video_data['live']['ex_info']['live_type']),'tstv') !== false || strpos(strtolower($video_data['live']['ex_info']['live_type']),'playback') !== false)) ? 1 : 0;
	    $xml_str  ='<?xml version="1.0" encoding="UTF-8"?>';
	    $xml_str .='<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
	    $xml_str .=    '<Objects>';
	    $xml_str .=        '<Object ElementType="Channel" ContentID="'.$video_data['live']['base_info']['cdn_info']['nns_id'].'" Action="'.$str_Action.'">';
	    $xml_str .=            '<Property Name="Name"><![CDATA['.$video_data['live']['base_info']['nns_name'].']]></Property>';
	    $xml_str .=            '<Property Name="ChannelNumber"><![CDATA['.$video_data['live']['ex_info']['live_pindao_order'].']]></Property>';
	    $xml_str .=            '<Property Name="CallSign"><![CDATA['.$video_data['live']['base_info']['nns_name'].']]></Property>';
	    $xml_str .=            '<Property Name="TimeShift"><![CDATA['.$TimeShift.']]></Property>';
	    $xml_str .=            '<Property Name="Type"><![CDATA[2]]></Property>';
	    $xml_str .=            '<Property Name="Status"><![CDATA[1]]></Property>';
	    $xml_str .=            '<Property Name="Tags"><![CDATA['.$video_data['live']['base_info']['nns_tags'].']]></Property>';
	    $xml_str .=            '<Property Name="StartTime"><![CDATA[0000]]></Property>';
	    $xml_str .=            '<Property Name="EndTime"><![CDATA[2359]]></Property>';
	    $xml_str .=            '<Property Name="CPContentID"><![CDATA['.$video_data['live']['base_info']['nns_cp_id'].']]></Property>';
	    $xml_str .=            '<Property Name="Country"><![CDATA[]]></Property>';
	    $xml_str .=            '<Property Name="State"><![CDATA[]]></Property>';
	    $xml_str .=            '<Property Name="City"><![CDATA[]]></Property>';
	    $xml_str .=            '<Property Name="ZipCode"><![CDATA[]]></Property>';
	    $xml_str .=            '<Property Name="URL"><![CDATA[]]></Property>';
	    $xml_str .=            '<Property Name="SubType"><![CDATA[1]]></Property>';
	    $xml_str .=            '<Property Name="Language"><![CDATA['.$video_data['live']['base_info']['nns_language'].']]></Property>';
	    $xml_str .=            '<Property Name="Macrovision"><![CDATA[1]]></Property>';
	    $xml_str .=            '<Property Name="VideoType"><![CDATA[]]></Property>';
	    $xml_str .=            '<Property Name="AudioType"><![CDATA[]]></Property>';
	    $xml_str .=            '<Property Name="StreamType"><![CDATA[]]></Property>';
	    $xml_str .=            '<Property Name="Bilingual"><![CDATA[]]></Property>';
	    $xml_str .=        '</Object>';
	    if(isset($result_picture['picture']) && strlen($result_picture['picture']) >0)
	    {
	        $xml_str .= $result_picture['picture'];
	    }
	    $xml_str .=    '</Objects>';
	    if(isset($result_picture['mapping']) && strlen($result_picture['mapping']) >0)
	    {
	        $xml_str .= 	'<Mappings>';
	        $xml_str .= $result_picture['mapping'];
	        $xml_str .= 	'</Mappings>';
	    }
	    $xml_str .= '</ADI>';
	    return $xml_str;
	}
	
	
	
	/**
	 * 组装片源信息
	 * @param unknown $arr_live_media 片源基本信息
	 * @param string $arr_live_media_ex 扩展数据
	 * @return string
	 * @var string DestCastType
	 *     说明： 用户访问改频道使用是单播还是组播
	 *     注释： unicast/multicast
	 * @var string SrcCastType
	 *     说明： CDN 接收到的频道类型方式，可能是单播，也可能是组播
	 *     注释： unicast/multicast
	 * @var string ChannelID
	 *     说明：频 道 ID ， 所 属 的 关 联 的channel 的标识 contentid
	 * @var string Storage
	 *     说明：回看标志 0:不生效 1:生效
	 * @var string TimeShift
	 *     说明：时移标志 0:不生效 1:生效
	 * @var string Status
	 *     说明：状态标志 0:失效 1:生效
	 * @var string StorageDuration
	 *     说明：存储时长，单位小时 仅仅对回看有效
	 * @var string TimeShiftDuration
	 *     说明：默认时移时长, 单位分钟 (Reserved) 仅仅对时移有效
	 * @var string BitRateType
	 *     说明：码流: 1: 400k 2：700K 3:  1.3M 4：2M 5：2.5M 6:  8M 7：10M 8：15M 9：20M 10：30M
	 * @var string ServiceURL
	 *     说明： 内容服务 URL
	 *     注释： 需要指定 CDN 服务 URL 的内容注入/预注入内容接入或实时回源接入内容删除时填写。为 CDN 提供服务的内容服务调用 URL，当 ServiceURL 有值时，该URL 前缀信息应和 CDN 服务配置
	 *           表中对应域内容提供 商 的PlaybackURLprefix 保持一致，用户需要基于本URL 访问注入内容
	 * @var string CPContentID
	 *     说明：内容服务商对于该物理频道的唯一标识
	 * @var string MultiCastIP
	 *     说明：组播 IP（当 srccasttype 为组播时，必填）
	 * @var string MultiCastPort
	 *     说明：组播端口（当 srccasttype 为组播时，必填）
	 * @var string UnicastUrl
	 *     说明：当 srccasttype 为单播时，必填。 SrcCastType 为单播且托管内容注入方式下填写，如果注入的是 HLS 直播内容，取值为编码器推送的直播URL： http://ip:port/xxx/index.m3u8
	 *           对非托管内容预注入方式下 则 为 空 ， 填 写ServiceURL 属性
	 * @var string VideoType
	 *     说明：编码格式： 1. H.264 2. MPEG4 3. AVS 4. MPEG2 5.MP3 6.WMV7.H.265
	 * @var string AudioType
	 *     说明：编码格式： 1. MP2 2.AAC 3.AMR 4.MP3
	 * @var string Resolution
	 *     说明：分辨率类型 1.QCIF 2.QVGA 3.2/3 D1 4.3/4 D1 5.D1 6.720P 7.1080i 8.1080P 9.2K 11.4K 13.8K
	 * @var string VideoProfile
	 *     说明：1.Simple 2.Advanced Simple 3.Baseline 4.Main 5.High 6.JiZhun
	 * @var string SystemLayer
	 *     说明：1.TS 2.3GP 3.mp4 4.flv 5.rtp
	 * @var string Domain
	 *     说明： 发布到融合 CDN 后的服务域和服务协议
	 *     注释： 包含 ServiceDomain 和ProtocolType 两层含义，为 4 字节整数，其中最高三 个 字 节 为ServiceDomain，表示预分发网络域，最低 3 位取值如下：
	 *               0x000001-IPTV 网络
	 *               0x000002-互联网网络
	 *               0x000004-移动网络
	 *               0x000008~0x000080-预留
	 *            最 低 一 个 字 节 为ProtocolType，表示服务协议，最低 4 位取值如下：
	 *               0x01-IPTV TS RTSP，
	 *               0x02-HPD
	 *               0x04-ISMA RTSP
	 *               0x08-HLS。
	 *            如果 Domain 为空，则采用该 CMSID 缺省分发域，缺省分发域可配置
	 * @var string Hotdegree
	 *     说明： 发布到融合 CDN 时使用热度表示
	 *     注释： 0（普通），1（高） 缺省为 0，该字段值越大，表示优先级越高。CDN 应根据优先级安排处理顺序
	 */
	public function do_live_media($video_data)
	{
	    $str_Action = ($video_data['live_media']['base_info']['nns_deleted'] !='1') ? 'DELETE' : ($video_data['live_media']['base_info']['nns_create_time'] != $video_data['live_media']['base_info']['nns_modify_time'] ? 'UPDATE' : 'REGIST');
	    $str_DestCastType = $str_SrcCastType = ($video_data['live_media']['base_info']['nns_cast_type'] == 1) ? 'multicast' : 'unicast';
	    $str_MultiCastIP = '';
	    $str_MultiCastPort = '';
	    $str_UnicastUrl = '';
	    if($str_SrcCastType == 'multicast')
	    {
	        $video_data['live_media']['base_info']['nns_url'] = trim($video_data['live_media']['base_info']['nns_url']);
	        $udp_url = preg_replace('/^udp/i', '', $video_data['live_media']['base_info']['nns_url']);
	        $udp_url = trim(ltrim(ltrim(ltrim(ltrim($udp_url,':'),'/'),'/'),'@'));
	        $arr_udp = explode(':',$udp_url);
	        $str_MultiCastIP = $arr_udp[0];
	        $str_MultiCastPort = $arr_udp[1];
	    }
	    else
	    {
	        $str_UnicastUrl = $video_data['live_media']['base_info']['nns_url'];
	    }
	    $str_BitRateType = (strtolower($video_data['live_media']['base_info']['nns_mode'])=="hd") ? 4 : 2;
		$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
		$xml_str .= 	'<Objects>';
		
		
		
		
		$xml_str.='<Object ElementType="PhysicalChannel" ContentID="'.$video_data['live_media']['cdn_info']['nns_id'].'" Action="'.$str_Action.'">';
		$xml_str.=	'<Property Name="DestCastType"><![CDATA['.$str_DestCastType.']]></Property>';
		$xml_str.=	'<Property Name="SrcCastType"><![CDATA['.$str_SrcCastType.']]></Property>';
		$xml_str.= 	'<Property Name="ChannelID"><![CDATA['.$video_data['live']['cdn_info']['nns_id'].']]></Property>';
		$xml_str.= 	'<Property Name="Storage"><![CDATA['.$video_data['live_media']['base_info']['nns_storage_status'].']]></Property>';
		$xml_str.= 	'<Property Name="TimeShift"><![CDATA['.$video_data['live_media']['base_info']['nns_timeshift_status'].']]></Property>';
		$xml_str.= 	'<Property Name="Tags"><![CDATA['.$video_data['live_media']['base_info']['nns_tags'].']]></Property>';
		$xml_str.= 	'<Property Name="Status"><![CDATA[1]]></Property>';
		$xml_str.= 	'<Property Name="StorageDuration"><![CDATA['.$video_data['live_media']['base_info']['nns_storage_delay'].']]></Property>';
		$xml_str.= 	'<Property Name="TimeShiftDuration"><![CDATA['.$video_data['live_media']['base_info']['nns_timeshift_delay'].']]></Property>';
		$xml_str.= 	'<Property Name="BitRateType"><![CDATA['.$str_BitRateType.']]></Property>';
		$xml_str.=	'<Property Name="ServiceURL"><![CDATA[]]></Property>';
		$xml_str.= 	'<Property Name="CPContentID"><![CDATA['.$video_data['live_media']['base_info']['nns_import_source'].']]></Property>';
		$xml_str.= 	'<Property Name="MultiCastIP"><![CDATA['.$str_MultiCastIP.']]></Property>';
		$xml_str.= 	'<Property Name="MultiCastPort"><![CDATA['.$str_MultiCastPort.']]></Property>';
		if($str_SrcCastType == 'multicast')
		{
		    $xml_str.= 	'<Property Name="UnicastUrl"/>';
		}
		else
		{
		    $xml_str.= 	'<Property Name="UnicastUrl"><![CDATA['.$str_UnicastUrl.']]></Property>';
		}
		$xml_str.=	'<Property Name="VideoType"><![CDATA[1]]></Property>';
		$xml_str.=	'<Property Name="AudioType"><![CDATA[1]]></Property>';
		$xml_str.=	'<Property Name="Resolution"><![CDATA['.$video_data['live_media']['base_info']['nns_kbps'].']]></Property>';
		$xml_str.=	'<Property Name="VideoProfile"><![CDATA[1]]></Property>';
		$xml_str.=	'<Property Name="SystemLayer"><![CDATA[]]></Property>';
		$xml_str.=	'<Property Name="Domain"><![CDATA['.$video_data['live_media']['base_info']['nns_domain'].']]></Property>';
		$xml_str.=	'<Property Name="Hotdegree"><![CDATA[]]></Property>';
		$xml_str.='</Object>';
		$xml_str .= 	'</Objects>';
		$xml_str .= 	'<Mappings>';
		$xml_str .=       $this->make_mapping('Channel','PhysicalChannel',$video_data['live']['cdn_info']['nns_id'],$video_data['live_media']['cdn_info']['nns_id'],$str_Action);
		$xml_str .= 	'</Mappings>';
		$xml_str .= '</ADI>';
		return $xml_str;
	}
	
	
	/**
	 * 组装节目单信息
	 * @param unknown $arr_live_media 片源基本信息
	 * @param string $arr_live_media_ex 扩展数据
	 * @return string 
	 * @var string StartDate       
	 *     说明：节目开播日期(YYYYMMDD)  
	 * @var string StartTime        
	 *     说明：节目开播时间北京时间 (YYYYMMDDHHMMSS)，如果只有 6 字 节 ， 则 格 式 为HH24MISS ， 需 要 和StartDate 一起拼装成 14字 节 的 北 京 时 间YYYYMMDDHHMMSS 
	 * @var string EndTime         
	 *     说明：节目结束时间北京时间(YYYYMMDDHHMMSS) 悦 me 必选
	 * @var string Duration          
	 *     说明：播放时长（单位：秒） 节目时长(HH24MISS) 采用StartDate属性时必填 
	 * @var string CPContentID           
	 *     说明：CP 对于该段录制内容的唯一标识
	 * @var string Description            
	 *     说明：描述信息
	 * @var string Domain            
	 *     说明： 发布到融合 CDN 后的服务域和服务协议 
	 *     注释： 包含 ServiceDomain 和ProtocolType 两层含义，为 4 字节整数，其中最高三 个 字 节 为ServiceDomain，表示预分发网络域，最低 3 位取值如下： 
	 *               0x000001-IPTV 网络 
	 *               0x000002-互联网网络 
	 *               0x000004-移动网络 
	 *               0x000008~0x000080-预留 
	 *            最 低 一 个 字 节 为ProtocolType，表示服务协议，最低 4 位取值如下：
	 *               0x01-IPTV TS RTSP， 
	 *               0x02-HPD 
	 *               0x04-ISMA RTSP 
	 *               0x08-HLS。 
	 *            如果 Domain 为空，则采用该 CMSID 缺省分发域，缺省分发域可配置
	 * @var string Hotdegree             
	 *     说明： 发布到融合 CDN 时使用热度表示
	 *     注释： 0（普通），1（高） 缺省为 0，该字段值越大，表示优先级越高。CDN 应根据优先级安排处理顺序
	 */
	public function do_playbill($video_data)
	{
		$str_Action = ($video_data['playbill']['base_info']['nns_state'] !=1) ? ($video_data['playbill']['base_info']['nns_modify_time'] > $video_data['playbill']['base_info']['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
		$result_picture = $this->make_image($video_data['playbill']['base_info'], 'ScheduleRecord', $video_data['playbill']['cdn_info']['nns_id'], $str_Action);

		$str_EndTime = date('YmdHis',(strtotime($video_data['playbill']['base_info']['nns_begin_time'])+$video_data['playbill']['base_info']['nns_time_len']));
		$str_StartDate = str_replace('-', '', trim(substr($video_data['playbill']['base_info']['nns_begin_time'], 0,10)));
		$str_StartTime = str_replace(':', '', trim(substr($video_data['playbill']['base_info']['nns_begin_time'], 11,8)));
		$str_time_len = gmstrftime("%H%M%S",$video_data['playbill']['base_info']['nns_time_len']);
		$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
		$xml_str .= 	'<Objects>';
		$str_xml ='<Object ElementType="ScheduleRecord" ContentID="'.$video_data['playbill']['cdn_info']['nns_id'].'" PhysicalChannelID="'.$video_data['live']['cdn_info']['nns_id'].'" Action="'.$str_Action.'">';
		$str_xml.=	'<Property Name="Name"><![CDATA['.$video_data['playbill']['base_info']['nns_name'].']]></Property>';
		$str_xml.=	'<Property Name="StartDate"><![CDATA['.$str_StartDate.']]></Property>';
		$str_xml.=	'<Property Name="StartTime"><![CDATA['.$str_StartTime.']]></Property>';
		$str_xml.=	'<Property Name="EndTime"><![CDATA['.$str_EndTime.']]></Property>';
		$str_xml.= 	'<Property Name="Duration"><![CDATA['.$str_time_len.']]></Property>';
		$str_xml.= 	'<Property Name="CPContentID"><![CDATA['.$video_data['playbill']['base_info']['nns_import_source'].']]></Property>';
		$str_xml.= 	'<Property Name="Description"><![CDATA['.$video_data['playbill']['base_info']['nns_summary'].']]></Property>';
		$str_xml.= 	'<Property Name="Domain"><![CDATA[]]></Property>';
		$str_xml.= 	'<Property Name="HotDgree"><![CDATA[0]]></Property>';
		$str_xml.='</Object>';
		if(isset($result_picture['picture']) && strlen($result_picture['picture']) >0)
		{
		    $xml_str .= $result_picture['picture'];
		}
		$xml_str .= 	'</Objects>';
		$xml_str .= 	'<Mappings>';
		$xml_str .=       $this->make_mapping('Channel','ScheduleRecord',$video_data['live']['cdn_info']['nns_id'],$video_data['playbill']['cdn_info']['nns_id'],$str_Action);
		if(isset($result_picture['mapping']) && strlen($result_picture['mapping']) >0)
		{
		    $xml_str .= $result_picture['mapping'];
		}
		$xml_str .= 	'</Mappings>';
		$xml_str .= '</ADI>';
		return $xml_str;
	}
}
