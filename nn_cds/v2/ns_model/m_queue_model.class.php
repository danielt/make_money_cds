<?php
namespace ns_model;
\ns_core\m_load::load("ns_model.message.message_explain");
class m_queue_model
{
    /**
     * 全局查询媒资信息（队列存在的都在这儿查询   涵盖所有的数据）
     * @param string $queue_main_id 队列主GUID
     * @param string $video_type 影片类型
     * @param string $is_need_parent 如果存在父级是否需要查询父级信息    false 否 | true 是
     * @param string $is_need_ext 是否需要扩展    false 否 | true 是
     * @param string $epg_model  为空 不需要查询EPG扩展数据 | 不为空 需要查询EPG扩展数据
     * @param string $cdn_model  为空 不需要查询CDN扩展数据 | 不为空 需要查询CDN扩展数据
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function _get_queue_info($queue_main_id,$video_type,$is_need_parent = false,$is_need_ext = false, $epg_model = '', $cdn_model = '')
    {
        $message_explain = new \ns_model\message\message_explain();
        $message_query = array(
            'base_info'=>array(
                'nns_id' => $queue_main_id,
            ),
        );
        switch ($video_type)
        {
            case 'video':
                $result_base = $message_explain->vod_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                unset($message_explain);
                if($result_base['ret'] !=0)
                {
                    return $result_base;
                }
                if(!isset($result_base['data_info'][0]) || !is_array($result_base['data_info'][0]) || empty($result_base['data_info'][0]))
                {
                    return \m_config::return_data(1,'点播主媒资查询点播主媒资信息无数据');
                }
                $arr_marage = array(
                    $video_type=>$result_base['data_info'][0],
                );
                return \m_config::return_data(0,'ok',$arr_marage);
            case 'index':
                $result_base = $message_explain->index_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                unset($message_explain);
                if($result_base['ret'] !=0)
                {
                    return $result_base;
                }
                if(!isset($result_base['data_info'][0]) || !is_array($result_base['data_info'][0]) || empty($result_base['data_info'][0]))
                {
                    return \m_config::return_data(1,'点播分集查询点播分集无数据');
                }
                $arr_marage = array(
                    $video_type=>$result_base['data_info'][0],
                );
                if($is_need_parent)
                {
                    $result_parent = $this->_get_queue_info($result_base['data_info'][0]['base_info']['nns_vod_id'],'video',$is_need_parent,$is_need_ext,$epg_model,$cdn_model);
                    if($result_parent['ret'] !=0)
                    {
                        return $result_parent;
                    }
                    if(!isset($result_parent['data_info']['video']) || !is_array($result_parent['data_info']['video']) || empty($result_parent['data_info']['video']))
                    {
                        return \m_config::return_data(1,'点播分集查询点播主媒资无数据');
                    }
                    $arr_marage = array_merge_recursive($arr_marage,$result_parent['data_info']);
                }
                return \m_config::return_data(0,'ok',$arr_marage);
            case 'media':
                $result_base = $message_explain->media_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                unset($message_explain);
                if($result_base['ret'] !=0)
                {
                    return $result_base;
                }
                if(!isset($result_base['data_info'][0]) || !is_array($result_base['data_info'][0]) || empty($result_base['data_info'][0]))
                {
                    return \m_config::return_data(1,'点播片源查询点播片源无数据');
                }
                $arr_marage = array(
                    $video_type=>$result_base['data_info'][0],
                );
                if($is_need_parent)
                {
                    $result_parent = $this->_get_queue_info($result_base['data_info'][0]['base_info']['nns_vod_index_id'],'index',$is_need_parent,$is_need_ext,$epg_model,$cdn_model);
                    if($result_parent['ret'] !=0)
                    {
                        return $result_parent;
                    }
                    if(!isset($result_parent['data_info']['index']) || !is_array($result_parent['data_info']['index']) || empty($result_parent['data_info']['index']))
                    {
                        return \m_config::return_data(1,'点播片源查询点播分集无数据');
                    }
                    $arr_marage = array_merge_recursive($arr_marage,$result_parent['data_info']);
                }
                return \m_config::return_data(0,'ok',$arr_marage);
            case 'live':
                $result_base = $message_explain->live_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                unset($message_explain);
                if($result_base['ret'] !=0)
                {
                    return $result_base;
                }
                if(!isset($result_base['data_info'][0]) || !is_array($result_base['data_info'][0]) || empty($result_base['data_info'][0]))
                {
                    return \m_config::return_data(1,'直播频道查询直播频道信息无数据');
                }
                $arr_marage = array(
                    $video_type=>$result_base['data_info'][0],
                );
                return \m_config::return_data(0,'ok',$arr_marage);
            case 'live_index':
                $result_base = $message_explain->live_index_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                unset($message_explain);
                if($result_base['ret'] !=0)
                {
                    return $result_base;
                }
                if(!isset($result_base['data_info'][0]) || !is_array($result_base['data_info'][0]) || empty($result_base['data_info'][0]))
                {
                    return \m_config::return_data(1,'直播分集查询直播分集无数据');
                }
                $arr_marage = array(
                    $video_type=>$result_base['data_info'][0],
                );
                if($is_need_parent)
                {
                    $result_parent = $this->_get_queue_info($result_base['data_info'][0]['base_info']['nns_live_id'],'live',$is_need_parent,$is_need_ext,$epg_model,$cdn_model);
                    if($result_parent['ret'] !=0)
                    {
                        return $result_parent;
                    }
                    if(!isset($result_parent['data_info']['live']) || !is_array($result_parent['data_info']['live']) || empty($result_parent['data_info']['live']))
                    {
                        return \m_config::return_data(1,'直播分集查询直播频道无数据');
                    }
                    $arr_marage = array_merge_recursive($arr_marage,$result_parent['data_info']);
                }
                return \m_config::return_data(0,'ok',$arr_marage);
            case 'live_media':
                $result_base = $message_explain->live_media_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                unset($message_explain);
                if($result_base['ret'] !=0)
                {
                    return $result_base;
                }
                if(!isset($result_base['data_info'][0]) || !is_array($result_base['data_info'][0]) || empty($result_base['data_info'][0]))
                {
                    return \m_config::return_data(1,'直播源查询直播源无数据');
                }
                $arr_marage = array(
                    $video_type=>$result_base['data_info'][0],
                );
                if($is_need_parent)
                {
                    $result_parent = $this->_get_queue_info($result_base['data_info'][0]['base_info']['nns_live_index_id'],'live_index',$is_need_parent,$is_need_ext,$epg_model,$cdn_model);
                    if($result_parent['ret'] !=0)
                    {
                        return $result_parent;
                    }
                    if(!isset($result_parent['data_info']['live_index']) || !is_array($result_parent['data_info']['live_index']) || empty($result_parent['data_info']['live_index']))
                    {
                        return \m_config::return_data(1,'直播源查询直播分集无数据');
                    }
                    $arr_marage = array_merge_recursive($arr_marage,$result_parent['data_info']);
                }
                return \m_config::return_data(0,'ok',$arr_marage);
            case 'playbill':
                $result_base = $message_explain->playbill_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                unset($message_explain);
                if($result_base['ret'] !=0)
                {
                    return $result_base;
                }
                if(!isset($result_base['data_info'][0]) || !is_array($result_base['data_info'][0]) || empty($result_base['data_info'][0]))
                {
                    return \m_config::return_data(1,'查询节目单无数据');
                }
                $arr_marage = array(
                    $video_type=>$result_base['data_info'][0],
                );
                if($is_need_parent)
                {
                    $result_parent = $this->_get_queue_info($result_base['data_info'][0]['base_info']['nns_live_id'],'live',$is_need_parent,$is_need_ext,$epg_model,$cdn_model);
                    if($result_parent['ret'] !=0)
                    {
                        return $result_parent;
                    }
                    if(!isset($result_parent['data_info']['live']) || !is_array($result_parent['data_info']['live']) || empty($result_parent['data_info']['live']))
                    {
                        return \m_config::return_data(1,'节目单查询直播频道无数据');
                    }
                    $arr_marage = array_merge_recursive($arr_marage,$result_parent['data_info']);
                }
                return \m_config::return_data(0,'ok',$arr_marage);
            case 'file':
                $result_base = $message_explain->file_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                unset($message_explain);
                if($result_base['ret'] !=0)
                {
                    return $result_base;
                }
                if(!isset($result_base['data_info'][0]) || !is_array($result_base['data_info'][0]) || empty($result_base['data_info'][0]))
                {
                    return \m_config::return_data(1,'文件包查询文件包信息无数据');
                }
                $arr_marage = array(
                    $video_type=>$result_base['data_info'][0],
                );
                return \m_config::return_data(0,'ok',$arr_marage);
            case 'product':
                $result_base = $message_explain->product_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                unset($message_explain);
                if($result_base['ret'] !=0)
                {
                    return $result_base;
                }
                if(!isset($result_base['data_info'][0]) || !is_array($result_base['data_info'][0]) || empty($result_base['data_info'][0]))
                {
                    return \m_config::return_data(1,'产品包查询产品包信息无数据');
                }
                $arr_marage = array(
                    $video_type=>$result_base['data_info'][0],
                );
                return \m_config::return_data(0,'ok',$arr_marage);
            case 'seekpoint':
                $result_base=$message_explain->seekpoint_action('query',$message_query);
                unset($message_explain);
                if($result_base['ret'] !=0)
                {
                    return $result_base;
                }
                if(!isset($result_base['data_info'][0]) || !is_array($result_base['data_info'][0]) || empty($result_base['data_info'][0]))
                {
                    return \m_config::return_data(1,'查询打点信息无数据');
                }
                $arr_marage = array(
                    $video_type=>$result_base['data_info'][0],
                );
                if($is_need_parent)
                {
                    $temp_video_type = $result_base['data_info'][0]['base_info']['nns_type'] == '1' ?  'live_index' : 'index';
                    $result_parent = $this->_get_queue_info($result_base['data_info'][0]['base_info']['nns_vod_index_id'],$temp_video_type,$is_need_parent,$is_need_ext,$epg_model,$cdn_model);
                    if($result_parent['ret'] !=0)
                    {
                        return $result_parent;
                    }
                    if(!isset($result_parent['data_info'][$temp_video_type]) || !is_array($result_parent['data_info'][$temp_video_type]) || empty($result_parent['data_info'][$temp_video_type]))
                    {
                        return \m_config::return_data(1,'分集信息查询无数据');
                    }
                    $arr_marage = array_merge_recursive($arr_marage,$result_parent['data_info']);
                }
                return \m_config::return_data(0,'ok',$arr_marage);
            case 'epg_file_set':
                $result_base = $message_explain->epg_file_set_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                unset($message_explain);
                if($result_base['ret'] !=0)
                {
                    return $result_base;
                }
                if(!isset($result_base['data_info'][0]) || !is_array($result_base['data_info'][0]) || empty($result_base['data_info'][0]))
                {
                    return \m_config::return_data(1,'EPGFileSet查询信息无数据');
                }
                $arr_marage = array(
                    $video_type=>$result_base['data_info'][0],
                );
                return \m_config::return_data(0,'ok',$arr_marage);
            case 'epg_file':
                $result_base = $message_explain->epg_file_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                unset($message_explain);
                if($result_base['ret'] !=0)
                {
                    return $result_base;
                }
                if(!isset($result_base['data_info'][0]) || !is_array($result_base['data_info'][0]) || empty($result_base['data_info'][0]))
                {
                    return \m_config::return_data(1,'EPGFile查询无数据');
                }
                $arr_marage = array(
                    $video_type=>$result_base['data_info'][0],
                );
                if($is_need_parent)
                {
                    $result_parent = $this->_get_queue_info($result_base['data_info'][0]['base_info']['nns_epg_file_set_id'],'epg_file_set',$is_need_parent,$is_need_ext,$epg_model,$cdn_model);
                    if($result_parent['ret'] !=0)
                    {
                        return $result_parent;
                    }
                    if(!isset($result_parent['data_info']['epg_file_set']) || !is_array($result_parent['data_info']['epg_file_set']) || empty($result_parent['data_info']['epg_file_set']))
                    {
                        return \m_config::return_data(1,'EPGFile查询EPGFileSet无数据');
                    }
                    $arr_marage = array_merge_recursive($arr_marage,$result_parent['data_info']);
                }
                return \m_config::return_data(0,'ok',$arr_marage);
            default:
                return \m_config::return_data(1,"不支持该类型查询[{$video_type}]");
        }
    }
    

    /**
     * 多条件查询非GUID，可能反馈多条数据（无父级查询  队列存在的都在这儿查询   涵盖所有的数据）
     * @param array $message_query  查询条件
     *  array(
     *     'base_info'=>array(
     *          'base_info1'=>'base_info1',
     *          'base_info2'=>'base_info2',
     *          ... ...
     *     ),
     *     'ex_info'=>array(
     *          'ex_info1'=>'ex_info1',
     *          'ex_info2'=>'ex_info2',
     *          ... ...
     *     )
     *  )
     * @param string $video_type 影片类型
     * @param string $is_need_ext 是否需要扩展    false 否 | true 是
     * @param string $epg_model  为空 不需要查询EPG扩展数据 | 不为空 需要查询EPG扩展数据
     * @param string $cdn_model  为空 不需要查询CDN扩展数据 | 不为空 需要查询CDN扩展数据
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function _get_queue_info_by_muli_condition($message_query,$video_type,$is_need_ext=false,$epg_model='',$cdn_model='')
    {
        $message_explain = new \ns_model\message\message_explain();
        switch ($video_type)
        {
            case 'video':
                $result_base = $message_explain->vod_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                $str_desc = "点播主媒资查询点播主媒资元数据";
                break;
            case 'index':
                $result_base = $message_explain->index_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                $str_desc = "点播主分集查询点播分集元数据";
                break;
            case 'media':
                $result_base = $message_explain->media_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                $str_desc = "点播片源查询点播片源元数据";
                break;
            case 'live':
                $result_base = $message_explain->live_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                $str_desc = "直播频道查询直播频道元数据";
                break;
            case 'live_index':
                $result_base = $message_explain->live_index_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                $str_desc = "直播分集查询直播分集元数据";
                break;
            case 'live_media':
                $result_base = $message_explain->live_media_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                $str_desc = "直播片源查询直播片源元数据";
                break;
            case 'playbill':
                $result_base = $message_explain->playbill_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                $str_desc = "节目单查询节目单元数据";
                break;
            case 'file':
                $result_base = $message_explain->file_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                $str_desc = "文件包查询文件包元数据";
                break;
            case 'product':
                $result_base = $message_explain->product_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                $str_desc = "产品包查询产品包元数据";
                break;
            case 'seekpoint':
                $result_base=$message_explain->seekpoint_action('query',$message_query);
                $str_desc = "打点信息查询打点信息元数据";
                break;
            case 'epg_file_set':
                $result_base = $message_explain->epg_file_set_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                $str_desc = "EPGFileSet查询元数据";
                break;
            case 'epg_file':
                $result_base = $message_explain->epg_file_action('query',$message_query,$is_need_ext,$epg_model,$cdn_model);
                $str_desc = "EPGFile查询元数据";
                break;
            default:
                return \m_config::return_data(1,"不支持该类型查询[{$video_type}]");
        }
        unset($message_explain);
        if($result_base['ret'] != 0)
        {
            return $result_base;
        }
        if(!isset($result_base['data_info']) || !is_array($result_base['data_info']) || empty($result_base['data_info']))
        {
            return \m_config::return_data(0,"{$str_desc}信息[无数据]");
        }
        return \m_config::return_data(0,"{$str_desc}信息ok",$result_base['data_info']);
    }
    
    /**
     * 全局查询队列名称（队列存在的都在这儿查询   涵盖所有的队列数据）
     * @param unknown $video_type
     * @param unknown $query_info
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function _get_queue_name($video_type,$query_info)
    {
        switch ($video_type)
        {
            case 'video':
                $str_queue_name = "[{$query_info[$video_type]['base_info']['nns_name']}]";
                break;
            case 'index':
                $query_info[$video_type]['base_info']['nns_index']++;
                $str_queue_name  = "[{$query_info['video']['base_info']['nns_name']}]";
                $str_queue_name .= (isset($query_info[$video_type]['base_info']['nns_name']) && strlen($query_info[$video_type]['base_info']['nns_name']) >0) ? "[{$query_info[$video_type]['base_info']['nns_name']}]" : "[{$query_info['video']['base_info']['nns_name']}]";
                $str_queue_name .= "[第{$query_info[$video_type]['base_info']['nns_index']}集]";
                break;
            case 'media':
                $query_info['index']['base_info']['nns_index']++;
                $str_queue_name  = "[{$query_info['video']['base_info']['nns_name']}]";
                $str_queue_name .= (isset($query_info['index']['base_info']['nns_name']) && strlen($query_info['index']['base_info']['nns_name']) >0) ? "[{$query_info['index']['base_info']['nns_name']}]" : "[{$query_info['video']['base_info']['nns_name']}]";
                $str_queue_name .= "[第{$query_info['index']['base_info']['nns_index']}集]";
                $str_queue_name .= "[{$query_info[$video_type]['base_info']['nns_kbps']}/Kbps]";
                $str_queue_name .= "[{$query_info[$video_type]['base_info']['nns_tag']}]";
                $str_queue_name .= "[{$query_info[$video_type]['base_info']['nns_mode']}]";
                $str_queue_name .= "[{$query_info[$video_type]['base_info']['nns_filetype']}]";
                $str_queue_name .= "[{$query_info[$video_type]['base_info']['nns_media_service']}]";
                break;
            case 'live':
                $str_queue_name  = "[{$query_info[$video_type]['base_info']['nns_name']}]";
                break;
            case 'live_index':
                $query_info[$video_type]['base_info']['nns_index']++;
                $str_queue_name  = "[{$query_info['live']['base_info']['nns_name']}]";
                $str_queue_name .= (isset($query_info[$video_type]['base_info']['nns_name']) && strlen($query_info[$video_type]['base_info']['nns_name']) >0) ? "[{$query_info[$video_type]['base_info']['nns_name']}]" : "[{$query_info['live']['base_info']['nns_name']}]";
                $str_queue_name .= "[第{$query_info[$video_type]['base_info']['nns_index']}集]";
                break;
            case 'live_media':
                $query_info['live_index']['base_info']['nns_index']++;
                $str_queue_name  = "[{$query_info['live']['base_info']['nns_name']}]";
                $str_queue_name .= (isset($query_info['live_index']['base_info']['nns_name']) && strlen($query_info['live_index']['base_info']['nns_name']) >0) ? "[{$query_info['live_index']['base_info']['nns_name']}]" : "[{$query_info['live']['base_info']['nns_name']}]";
                $str_queue_name .= "[第{$query_info['live_index']['base_info']['nns_index']}集]";
                $str_queue_name .= "[{$query_info[$video_type]['base_info']['nns_kbps']}/Kbps]";
                $str_queue_name .= "[{$query_info[$video_type]['base_info']['nns_tag']}]";
                $str_queue_name .= "[{$query_info[$video_type]['base_info']['nns_mode']}]";
                break;
            case 'playbill':
                $str_queue_name  = "[{$query_info['live']['base_info']['nns_name']}]";
                $str_queue_name .= "[{$query_info[$video_type]['base_info']['nns_name']}]";
                $str_queue_name .= "[{$query_info[$video_type]['base_info']['nns_begin_time']}]";
                $str_queue_name .= "[{$query_info[$video_type]['base_info']['nns_time_len']}]";
                break;
            case 'file':
                $str_queue_name  = "[{$query_info[$video_type]['base_info']['nns_name']}]";
                $str_queue_name .= "[{$query_info[$video_type]['base_info']['nns_file_size']}]";
                break;
            case 'product':
                $str_queue_name  = "[{$query_info[$video_type]['base_info']['nns_order_name']}]";
                $str_queue_name .= $query_info[$video_type]['base_info']['nns_order_type'] == 1 ? "[融合]" : "[非融合]";
                $str_queue_name .= "[{$query_info[$video_type]['base_info']['nns_order_price']}]";
                break;
            case 'seekpoint':
                $str_queue_name  = "[{$query_info['video']['base_info']['nns_name']}]";
                $str_queue_name .= ($query_info['live_index'])? "[{$query_info['live_index']['base_info']['nns_name']}]":"[{$query_info['index']['base_info']['nns_name']}]";
                $str_queue_name .="[{$query_info[$video_type]['base_info']['nns_name']}]";
                break;
            case 'epg_file_set':
                $str_queue_name = "[{$query_info[$video_type]['base_info']['nns_group']}]";
                break;
            case 'epg_file':
                $query_info[$video_type]['base_info']['nns_index']++;
                $str_queue_name  = "[{$query_info['epg_file_set']['base_info']['nns_group']}]";
                $str_queue_name .= (isset($query_info[$video_type]['base_info']['nns_group']) && strlen($query_info[$video_type]['base_info']['nns_group']) >0) ? "[{$query_info[$video_type]['base_info']['nns_group']}]" : "[{$query_info['video']['base_info']['nns_group']}]";
                $str_queue_name .= "[第{$query_info[$video_type]['base_info']['nns_index']}个文件]";
                break;
            default:
                return \m_config::return_data(1,"不支持该类型查询[{$video_type}]");
        }
        return \m_config::return_data(0,"OK",$str_queue_name);
    }
    
    
    /**
     * 全局查询队列操作行为（队列存在的都在这儿查询   涵盖所有的队列数据）
     * @param unknown $video_type
     * @param unknown $query_info
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function _get_queue_action($video_type,$query_info)
    {
        if(in_array($video_type, array('video','index','media','live','live_media','file', 'epg_file_set', 'epg_file')))
        {
            if(!isset($query_info[$video_type]['base_info']['nns_deleted']))
            {
                return \m_config::return_data(1,"全局查询队列操作行为未查询到该字段类型为[{$video_type}]字段为[nns_deleted]");
            }
            $str_action = ($query_info[$video_type]['base_info']['nns_deleted'] == '1') ? 'destroy' : (($query_info[$video_type]['base_info']['nns_create_time'] < $query_info[$video_type]['base_info']['nns_modify_time']) ? 'modify' : 'add');
            return \m_config::return_data(0,"Ok",$str_action);
        }
        else if (in_array($video_type, array('playbill')))
        {
            if(!isset($query_info[$video_type]['base_info']['nns_state']))
            {
                return \m_config::return_data(1,"全局查询队列操作行为未查询到该字段类型为[{$video_type}]字段为[nns_state]");
            }
            $str_action = ($query_info[$video_type]['base_info']['nns_state'] == '1') ? 'destroy' : (($query_info[$video_type]['base_info']['nns_create_time'] < $query_info[$video_type]['base_info']['nns_modify_time']) ? 'modify' : 'add');
            return \m_config::return_data(0,"Ok",$str_action);
        }
        return \m_config::return_data(1,"不支持该类型查询[{$video_type}]");
    }
    
    /**
     * 正向获取CDN、EPG注入ID
     * @param string $sp_id  下游平台标示
     * @param string $video_type 影片类型
     * @param array $query_info 媒资数据信息
     * @param string $mode 注入平台模式   cdn 注入cdn平台 | epg 注入epg平台
     * @param string $str_header  头信息（江苏华博需要的参数）
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function _get_cdn_epg_import_id($sp_id,$video_type,$query_info,$mode='cdn',$str_header=null)
    {
        if (strlen($sp_id) < 1)
        {
            return \m_config::return_data('1', 'sp_id为空','');
        }
        $arr_sp_config = \m_config::_get_sp_info($sp_id);
        if($arr_sp_config['ret'] !=0)
        {
            return $arr_sp_config;
        }
        $config_action = ($mode === 'epg') ? $mode . "_" . $video_type . "_import_id_mode" : "import_id_mode";
        if(!isset($query_info[$video_type]['base_info']) || empty($query_info[$video_type]['base_info']) || !is_array($query_info[$video_type]['base_info']))
        {
            return \m_config::return_data('1', '传入的媒资基本数据为空','');
        }
        if (!isset($arr_sp_config['data_info']['nns_config'][$config_action]) || strlen($arr_sp_config['data_info']['nns_config'][$config_action]) < 1 || (int)$arr_sp_config['data_info']['nns_config'][$config_action] < 1)
        {
            return \m_config::return_data(0, 'ok', $query_info[$video_type]['base_info']['nns_id']);
        }
        switch ($video_type)
        {
            case 'video':
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_asset_import_id';
                $cp_id = 'nns_cp_id';
                $import_id_model_flags = '1';
                $import_id_model_flags_1 = '000000';
                $import_id_model_flags_2 = '0000';
                $import_id_model_flags_3 = '000001';
                // 				$import_id_model_flags_4 = 'PT';
                $import_id_model_flags_4 = 'PA';
                break;
            case 'index':
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_import_id';
                $cp_id = 'nns_cp_id';
                $import_id_model_flags = '2';
                $import_id_model_flags_1 = '001000';
                $import_id_model_flags_2 = '0001';
                $import_id_model_flags_3 = '000002';
                // 				$import_id_model_flags_4 = 'TI';
                $import_id_model_flags_4 = 'TE';
                break;
            case 'media':
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_import_id';
                $cp_id = 'nns_cp_id';
                $import_id_model_flags = '3';
                $import_id_model_flags_1 = '002000';
                $import_id_model_flags_2 = '0002';
                $import_id_model_flags_3 = '000003';
                // 				$import_id_model_flags_4 = 'MO';
                $import_id_model_flags_4 = 'MV';
                break;
            case 'live':
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_import_id';
                $cp_id = 'nns_cp_id';
                $import_id_model_flags = '4';
                $import_id_model_flags_1 = '004000';
                $import_id_model_flags_2 = '0004';
                $import_id_model_flags_3 = '010001';
                $import_id_model_flags_4 = 'CH';
                break;
            case 'live_index':
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_import_id';
                $cp_id = 'nns_cp_id';
                $import_id_model_flags = '5';
                $import_id_model_flags_1 = '005000';
                $import_id_model_flags_2 = '0005';
                $import_id_model_flags_3 = '010002';
                $import_id_model_flags_4 = 'LI';
                break;
            case 'live_media':
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_content_id';
                $cp_id = 'nns_cp_id';
                $import_id_model_flags = '6';
                $import_id_model_flags_1 = '006000';
                $import_id_model_flags_2 = '0006';
                $import_id_model_flags_3 = '010003';
                $import_id_model_flags_4 = 'PC';
                break;
            case 'playbill':
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_playbill_import_id';
                $cp_id = 'nns_cp_id';
                $import_id_model_flags = '7';
                $import_id_model_flags_1 = '007000';
                $import_id_model_flags_2 = '0007';
                $import_id_model_flags_3 = '020001';
                $import_id_model_flags_4 = 'SD';
                break;
            case 'file':
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_import_id';
                $cp_id = 'nns_cp_id';
                $import_id_model_flags = '8';
                $import_id_model_flags_1 = '000000';
                $import_id_model_flags_2 = '0000';
                $import_id_model_flags_3 = '030001';
                $import_id_model_flags_4 = 'FI';
                break;
            case 'epg_file':
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_import_id';
                $cp_id = 'nns_cp_id';
                $import_id_model_flags = '9';
                $import_id_model_flags_1 = '000000';
                $import_id_model_flags_2 = '0000';
                $import_id_model_flags_3 = '030001';
                $import_id_model_flags_4 = 'FI';
                break;
            default:
                return \m_config::return_data('1', '媒资类型错误！');
        }
        $str_csp_id = isset($arr_sp_config['data_info']['nns_config']['import_csp_id']) ? $arr_sp_config['data_info']['nns_config']['import_csp_id'] : '';
        switch ($arr_sp_config['data_info']['nns_config'][$config_action])
        {
            case '1'://32位注入ID  import_id_model_flags_1+数字ID+补全0
                $asset_id = $import_id_model_flags_1 . $query_info[$video_type]['base_info'][$str_integer_id];
                $asset_id = str_pad($asset_id, 32, "0");
                break;
            case '2'://32位注入ID  原始ID
                $asset_id = $query_info[$video_type]['base_info'][$str_import_id];
                break;
            case '3'://32位注入ID   CSPID+import_id_model_flags_2+数字ID+补全0【CSPID后台配】
                $asset_id = $str_csp_id . $import_id_model_flags_2 . $query_info[$video_type]['base_info'][$str_integer_id];
                $asset_id = str_pad($asset_id, 32, "0");
                break;
            case '4'://32位注入ID   CSPID(0)+import_id_model_flags+补全0+数字ID+CSPID(1)【CSPID后台配置 竖线分隔】
                $arr_csp_id = explode("|", $str_csp_id);
                $str_length = strlen($arr_csp_id[0]) + strlen($arr_csp_id[1]) +1;
                $lest_num = 32 - $str_length;
                $asset_id = $arr_csp_id[0] . $import_id_model_flags . str_pad($query_info[$video_type]['base_info'][$str_integer_id], $lest_num, "0", STR_PAD_LEFT) . $arr_csp_id[1];
                break;
            case '5'://32位注入ID  import_id_model_flags_3+补全0+integer_id
                $str_length = strlen($import_id_model_flags_3);
                $lest_num = 32 - $str_length;
                $asset_id = $import_id_model_flags_3 . str_pad($query_info[$video_type]['base_info'][$str_integer_id], $lest_num, "0", STR_PAD_LEFT);
                break;
            case '6'://32位注入ID   import_id_model_flags_1+补全0+integer_id
                $str_length = strlen($import_id_model_flags_1);
                $lest_num = 32 - $str_length;
                $asset_id = $import_id_model_flags_1 . str_pad($query_info[$video_type]['base_info'][$str_integer_id], $lest_num, "0", STR_PAD_LEFT);
                break;
            case '7':
                $str_header = (strlen($str_header) < 8) ? str_pad($str_header, 8, "0", STR_PAD_RIGHT) : substr($str_header, 0,8);
                $asset_id = $str_header.$import_id_model_flags_4 . str_pad($query_info[$video_type]['base_info'][$str_integer_id], 10, "0", STR_PAD_LEFT);
                break;
            case '8': //32位注入ID   CPID+import_id_model_flags+补全0+数字ID
                $str_length = strlen($query_info[$video_type]['base_info'][$cp_id]) + strlen($import_id_model_flags);
                $lest_num = 32 - $str_length;
                $asset_id = $query_info[$video_type]['base_info'][$cp_id] . $import_id_model_flags . str_pad($query_info[$video_type]['base_info'][$str_integer_id], $lest_num, "0", STR_PAD_LEFT);
                break;
            case '9': //md5(CPID+import_id)
                $asset_id = md5($query_info[$video_type]['base_info'][$cp_id] . $query_info[$video_type]['base_info'][$str_import_id]);
                break;
            case '12'://生成20位注入ID  import_id_model_flags+补全0+integer_id
                $str_length = strlen($import_id_model_flags);
                $lest_num = 20 - $str_length;
                $asset_id = $import_id_model_flags . str_pad($query_info[$video_type]['base_info'][$str_integer_id], $lest_num, "0", STR_PAD_LEFT);
                break;
            case '11'://32位注入ID  import_id_model_flags_1+数字ID+补全0,湖南注入中兴cdn专用 xinxin.deng 2018/9/13 17:21
                $import_id_model_flags_1 = '001000';
                $asset_id = $import_id_model_flags_1 . $query_info[$video_type]['base_info'][$str_integer_id];
                $asset_id = str_pad($asset_id, 32, "0");
                break;
            default:
                $asset_id = $query_info[$video_type]['base_info']['nns_id'];
                break;
        }
        return \m_config::return_data(0, 'ok', $asset_id);
    }
    
    
    /**
     * 反向通过CDN、EPG注入ID 查询媒资信息
     * @param string $sp_id  下游平台标示
     * @param string $video_type 影片类型
     * @param string $nns_id CDN、EPG注入ID 或者  GUID
     * @param string $str_cp_id CPID
     * @param string $mode 注入平台模式   cdn 注入cdn平台 | epg 注入epg平台
     * @param string $str_header 头信息（江苏华博需要的参数）
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function _get_asset_info_by_cdn_epg_import_id($sp_id, $video_type, $nns_id,$str_cp_id=null,$mode = "cdn",$str_header=null)
    {
        if (strlen($sp_id) < 1)
        {
            return \m_config::return_data('1', 'sp_id为空', '');
        }
        if (strlen($video_type) < 1)
        {
            return \m_config::return_data('1', 'video_type为空', '');
        }
        if (strlen($nns_id) < 1)
        {
            return \m_config::return_data('1', 'nns_id为空', '');
        }
        $arr_sp_config = \m_config::_get_sp_info($sp_id);
        if($arr_sp_config['ret'] !=0)
        {
            return $arr_sp_config;
        }
        $arr_sp_config = $arr_sp_config['data_info'];
        $config_action = ($mode === 'epg') ? $mode . "_" . $video_type . "_import_id_mode" : "import_id_mode";
        if (!isset($arr_sp_config['nns_config'][$config_action]) || strlen($arr_sp_config['nns_config'][$config_action]) < 1 || (int)$arr_sp_config['nns_config'][$config_action] < 1)
        {
            return $this->_get_queue_info($nns_id, $video_type);
        }
        switch ($video_type)
        {
            case 'video':
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_asset_import_id';
                $cp_id = 'nns_cp_id';
                $import_id_model_flags = '1';
                $import_id_model_flags_1 = '000000';
                $import_id_model_flags_2 = '0000';
                $import_id_model_flags_3 = '000001';
                break;
            case 'index':
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_import_id';
                $cp_id = 'nns_cp_id';
                $import_id_model_flags = '2';
                $import_id_model_flags_1 = '001000';
                $import_id_model_flags_2 = '0001';
                $import_id_model_flags_3 = '000002';
                break;
            case 'media':
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_import_id';
                $cp_id = 'nns_cp_id';
                $import_id_model_flags = '3';
                $import_id_model_flags_1 = '002000';
                $import_id_model_flags_2 = '0002';
                $import_id_model_flags_3 = '000003';
                break;
            case 'live':
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_import_id';
                $cp_id = 'nns_cp_id';
                $import_id_model_flags = '4';
                $import_id_model_flags_1 = '004000';
                $import_id_model_flags_2 = '0004';
                $import_id_model_flags_3 = '010001';
                break;
            case 'live_index':
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_import_id';
                $cp_id = 'nns_cp_id';
                $import_id_model_flags = '5';
                $import_id_model_flags_1 = '005000';
                $import_id_model_flags_2 = '0005';
                $import_id_model_flags_3 = '010002';
                break;
            case 'live_media':
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_content_id';
                $cp_id = 'nns_cp_id';
                $import_id_model_flags = '6';
                $import_id_model_flags_1 = '006000';
                $import_id_model_flags_2 = '0006';
                $import_id_model_flags_3 = '010003';
                break;
            case 'playbill':
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_playbill_import_id';
                $cp_id = 'nns_cp_id';
                $import_id_model_flags = '7';
                $import_id_model_flags_1 = '007000';
                $import_id_model_flags_2 = '0007';
                $import_id_model_flags_3 = '020001';
                break;
            case 'file':
                $str_integer_id = 'nns_integer_id';
                $str_import_id = 'nns_import_id';
                $cp_id = 'nns_cp_id';
                $import_id_model_flags = '8';
                $import_id_model_flags_1 = '000000';
                $import_id_model_flags_2 = '0000';
                $import_id_model_flags_3 = '030001';
                break;
            default:
                return \m_config::return_data('1', '媒资类型错误！');
        }
        $str_csp_id = isset($arr_sp_config['nns_config']['import_csp_id']) ? $arr_sp_config['nns_config']['import_csp_id'] : '';
        $arr_query_data = array();
        $arr_query_data['base_info']['nns_deleted'] = 0;
        switch ($arr_sp_config['nns_config'][$config_action])
        {
            case '1':
                $arr_query_data['base_info']['nns_id'] = $nns_id;
                break;
            case '2':
                $arr_query_data['base_info'][$str_import_id] = $nns_id;
                break;
            case '3':
                $arr_query_data['base_info']['nns_id'] = $nns_id;
                break;
            case '4':
                $arr_csp_id = explode("|", $str_csp_id);
                $num_begin = strlen($arr_csp_id[0] . $import_id_model_flags);
                $num_begin = $num_begin>0 ? $num_begin : 0;
                $num_end = strlen($arr_csp_id[1]);
                $num_end = $num_end>0 ? $num_end : 0;
                $nns_id = substr($nns_id, $num_begin);
                $arr_query_data['base_info'][$str_integer_id] = ($num_end >0) ? substr($nns_id, 0,-$num_end) : $nns_id;
                break;
            case '5':
                $num_begin = strlen($import_id_model_flags_3);
                $num_begin = $num_begin>0 ? $num_begin : 0;
                $arr_query_data['base_info'][$str_integer_id] = preg_replace('/^0*/', '',substr($nns_id, $num_begin));
                break;
            case '7':
                $arr_query_data['base_info'][$str_integer_id] = preg_replace('/^0*/', '',substr($nns_id, 10));
                break;
            case '8': //cpid+import_id_model_flags+数字ID
                $num_begin = strlen($cp_id) + strlen($import_id_model_flags);
                $array_data[$str_integer_id] =  preg_replace('/^0*/', '',substr($nns_id, $num_begin));
                break;
            case '11'://湖南注入中兴cdn专有
                $arr_query_data['base_info']['nns_id'] = $nns_id;
                break;
            case '12':
                $num_begin = strlen($import_id_model_flags);
                $num_begin = $num_begin > 0 ? $num_begin : 0;
                $arr_query_data['base_info'][$str_integer_id] = preg_replace('/^0*/', '',substr($nns_id, $num_begin));
                break;
            default:
                $arr_query_data['base_info']['nns_id'] = $nns_id;
        }
        if(isset($str_cp_id) && strlen($str_cp_id)>0)
        {
            $arr_query_data['base_info']['nns_cp_id'] = $str_cp_id;
        }
        $result = $this->_get_queue_info_by_muli_condition($arr_query_data,$video_type);
        if($result['ret'] != 0 || !isset($result['data_info'][0]) || empty($result['data_info'][0]) || !is_array($result['data_info'][0]))
        {
            $result['ret'] = 1;
            return $result;
        }
        if(count($result['data_info']) >1)
        {
            return \m_config::return_data(1,$result['reason']."[查询出多条数据有问题]");
        }
        return \m_config::return_data(0,"ok",array($video_type=>$result['data_info'][0]));
    }
}