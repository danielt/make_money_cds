<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/11/12 11:31
 */
namespace ns_model\file;
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_model.file.file_explain");

class file_queue extends \ns_model\file\file_explain
{
    /**
     * 对透传队列注入队列的解释
     * 确认队列执行条件等
     */
    public function explain()
    {
        /***************效验是否允许CDN注入******************/
        if(isset($this->arr_sp_config['disabled_cdn']) && (int)$this->arr_sp_config['disabled_cdn'] === 1)
        {
            return \m_config::return_data(NS_CDS_FAIL,"sp信息获取spID[{$this->str_sp_id}]不允许CDN注入");
        }
        /***************效验类型是否允许注入******************/
        if(!isset($this->arr_sp_config['cdn_assets_enabled']) || !is_array($this->arr_sp_config['cdn_assets_enabled'])
            || (is_array($this->arr_sp_config['cdn_assets_enabled'])
                && !in_array($this->str_video_type, $this->arr_sp_config['cdn_assets_enabled'])))
        {
            return \m_config::return_data(NS_CDS_FAIL,"sp信息获取spID[{$this->str_sp_id}]注入CDN类型[{$this->str_video_type}]不允许注入");
        }
        return \m_config::return_data(NS_CDS_SUCCE,"ok");
    }

    /**
     * 添加透传队列
     */
    public function push()
    {

    }

    /**
     * 弹出epgfile队列
     * @return array
     * array (
     * ['0'] => Array (1)
    |    |    (
    |    |    |    ['pass_queue_task_info'] => Array (15)
    |    |    |    (
    |    |    |    |    ['nns_id'] = String(32) "5ae2bb7632c7f34d67707fd2285e66e3"
    |    |    |    |    ['nns_action'] = String(1) "1"
    |    |    |    |    ['nns_type'] = String(1) "0"
    |    |    |    |    ['nns_message_id'] = String(32) "626B197A94044563A8B9C5170C923863"
    |    |    |    |    ['nns_org_id'] = String(4) "hndx"
    |    |    |    |    ['nns_cp_id'] = String(7) "ZTEORHW"
    |    |    |    |    ['nns_audit'] = String(1) "1"
    |    |    |    |    ['nns_pause'] = String(1) "0"
    |    |    |    |    ['nns_status'] = String(1) "1"
    |    |    |    |    ['nns_content'] = String(170) "[{"category_id":"883BF531-668E-47CA-A2B8-C35D40007B18","parent_category":"A98CB098-B318-456F-B8A9-BAC1E96DE23D","name":"\u5730\u65b9\u620f\u66f2","display":"1","sort":7}]"
    |    |    |    |    ['nns_result_desc'] = String(0) ""
    |    |    |    |    ['nns_queue_time'] = String(18) "201804271356068126"
    |    |    |    |    ['nns_create_time'] = String(19) "2018-04-27 13:56:06"
    |    |    |    |    ['nns_modify_time'] = String(19) "2018-11-01 09:29:25"
    |    |    |    |    ['nns_queue_execute_url'] = String(78) "global_log/timer/pass_queue/hndx/ZTEORHW/1/cds_default_type/20181101/09/29.txt"
    |    |    |    )
    |    |    )
    |    |    ['1'] => Array (1)
    |    |    (
    |    |    |    ['pass_queue_task_info'] => Array (15)
    |    |    |    (
    |    |    |    |    ['nns_id'] = String(32) "5ae2bd801b4eff3ff40eb21781fb851d"
    |    |    |    |    ['nns_action'] = String(1) "1"
    |    |    |    |    ['nns_type'] = String(1) "0"
    |    |    |    |    ['nns_message_id'] = String(32) "626B197A94044563A8B9C5170C923862"
    |    |    |    |    ['nns_org_id'] = String(4) "hndx"
    |    |    |    |    ['nns_cp_id'] = String(7) "ZTEORHW"
    |    |    |    |    ['nns_audit'] = String(1) "1"
    |    |    |    |    ['nns_pause'] = String(1) "0"
    |    |    |    |    ['nns_status'] = String(1) "1"
    |    |    |    |    ['nns_content'] = String(170) "[{"category_id":"883BF531-668E-45CA-A2B8-C35D40007B18","parent_category":"A98CB098-B318-456F-B8A9-BAC1E96DE23D","name":"\u5730\u65b9\u620f\u66f2","display":"1","sort":7}]"
    |    |    |    |    ['nns_result_desc'] = String(0) ""
    |    |    |    |    ['nns_queue_time'] = String(18) "201804271404484363"
    |    |    |    |    ['nns_create_time'] = String(19) "2018-04-27 14:04:48"
    |    |    |    |    ['nns_modify_time'] = String(19) "2018-11-01 09:29:28"
    |    |    |    |    ['nns_queue_execute_url'] = String(78) "global_log/timer/pass_queue/hndx/ZTEORHW/1/cds_default_type/20181101/09/29.txt"
    |    |    |    )
    |    |    )
     * );
     */
    public function pop()
    {
        $params = array(
            'nns_org_id' => $this->str_sp_id,
            'nns_status' => NS_CDS_CDN_WAIT,//等待注入cdn
        );
        $file_queue = \nl_epg_file::query_by_condition(\m_config::get_dc(), $params);
        if ($file_queue['ret'] != 0 || empty($file_queue['data_info']) || !is_array($file_queue['data_info']))
        {
            return \m_config::return_data(NS_CDS_FAIL,'file task 注入数据队列查询失败或者数据为空');
        }

        /***************批量查询epgfile任务的媒资信息******************/
        $mixed_info = $this->query_mixed_info($file_queue['data_info']);
        if($mixed_info['ret'] != NS_CDS_SUCCE)
        {
            return \m_config::return_data(NS_CDS_FAIL,'CDN注入数据的媒资信息查询失败');
        }
        return \m_config::return_data(NS_CDS_SUCCE,'ok',$mixed_info['data_info']);
    }
}