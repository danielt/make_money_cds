<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/11/12 11:30
 */
namespace ns_model\file;
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load_old('nn_logic/epg_file/epg_file_set.class.php');
\ns_core\m_load::load_old('nn_logic/epg_file/epg_file.class.php');
class file_explain
{
    public $str_sp_id = null;
    public $arr_sp_config = null;
    public $str_video_type = null;

    public function __construct($sp_id, $video_type)
    {
        $this->str_sp_id = $sp_id;
        $arr_sp_info = \m_config::_get_sp_info($sp_id);
        $arr_sp_config = isset($arr_sp_info['data_info']['nns_config']) ? $arr_sp_info['data_info']['nns_config'] : null;
        $this->arr_sp_config = $arr_sp_config;
        $this->str_video_type = $video_type;
    }

    /**
     * 获取file队列详细元数据
     * @param $data
     * @return array
     */
    public function query_mixed_info($data)
    {
        $new_c2_info = array();
        $ref_arr = array();//当前任务的主键ID
        $src_arr = array();//当前任务的父级主键ID
        //组装批量查询数据
        foreach ($data as $c2_info)
        {
            if (!empty($c2_info['nns_ref_id']))
            {
                $ref_arr[] = $c2_info['nns_ref_id'];
            } else
            {
                continue;
            }
            if (!empty($c2_info['nns_src_id']))
            {
                $src_arr[] = $c2_info['nns_src_id'];
            }
        }
        if (empty($ref_arr))
        {
            return \m_config::return_data(NS_CDS_FAIL, '入参参数data错误');
        }
        //根据类型获取不同的数据源
        switch ($this->str_video_type)
        {
            case NS_CDS_EPG_FILE:
                //批量获取EPGFile及扩展信息
                $epg_file_set_result = \nl_epg_file_set::query_data_by_id(\m_config::get_dc(), $src_arr, false);
                if ($epg_file_set_result['ret'] != NS_CDS_SUCCE)
                {
                    return $epg_file_set_result;
                }
                $epg_file_set_result = $epg_file_set_result['data_info'];
                //批量获取分集及扩展信息
                $epg_file_result = \nl_epg_file::query_data_by_id(\m_config::get_dc(), $ref_arr);
                if ($epg_file_result['ret'] != NS_CDS_SUCCE)
                {
                    return $epg_file_result;
                }
                $epg_file_result = $epg_file_result['data_info'];
                foreach ($data as $c2)
                {
                    $c2_data = array();
                    $c2_data['c2_info'] = $c2;
                    if (isset($epg_file_set_result[$c2['nns_src_id']]))
                    {
                        $c2_data['epg_file_set_info'] = $epg_file_set_result[$c2['nns_src_id']];
                    }
                    if (isset($epg_file_result[$c2['nns_ref_id']]))
                    {
                        $c2_data['epg_file_info'] = $epg_file_result[$c2['nns_ref_id']];
                    }
                    $new_c2_info[] = $c2_data;
                    unset($c2_data);
                }
                break;
            case NS_CDS_FILE:
                //批量获取文件包信息
                $file_result = \nl_file_package::query_by_id(\m_config::get_dc(), $ref_arr);
                if ($file_result['ret'] != NS_CDS_SUCCE)
                {
                    return $file_result;
                }
                //数据组装
                $file_result = $file_result['data_info'];
                foreach ($data as $c2)
                {
                    $c2_data = array();
                    $c2_data['c2_info'] = $c2;
                    if (isset($live_result[$c2['nns_ref_id']]))
                    {
                        $c2_data['file_info'] = $file_result[$c2['nns_ref_id']];
                    }
                    $new_c2_info[] = $c2_data;
                    unset($c2_data);
                }
                break;
            case NS_CDS_FILE_PACKAGE:
                break;
            default :
                return \m_config::return_data(NS_CDS_FAIL, '入参参数类型错误');
                break;
        }

        return \m_config::return_data(NS_CDS_SUCCE, '', $new_c2_info);
    }
}