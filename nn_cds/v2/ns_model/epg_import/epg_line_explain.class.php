<?php
/**
 * epg媒资上下线控制
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/10/10 15:08
 */

namespace ns_model\epg_import;

\ns_core\m_load::load("ns_core.nn_timer");
\ns_core\m_load::load("ns_core.m_data_model_inout_put");
\ns_core\m_load::load_old('nn_logic/c2_task/c2_task.class.php');
\ns_core\m_load::load_old('nn_logic/vod_v2/vod_v2.class.php');
\ns_core\m_load::load_old('nn_logic/asset_online/asset_online.class.php');

/*
* 消息解释基础类，针对不同的消息
*/
class epg_line_explain
{
    public $clip_mode;
    public $str_sp_id = null;
    public $arr_sp_config = null;
    public $str_video_type = null;

    /**
     * clip_explain constructor.
     * @param $sp_id
     * @param string $video_type
     */
    public function __construct($sp_id,$video_type)
    {
        $this->str_sp_id = $sp_id;
        $arr_sp_config = \m_config::_get_sp_info($sp_id);
        $arr_sp_config = isset($arr_sp_config['data_info']['nns_config']) ? $arr_sp_config['data_info']['nns_config'] : null;
        $this->arr_sp_config = $arr_sp_config;
        $this->str_video_type = $video_type;
    }

    /**
     * 获取等待注入到epg的上下线指令队列
     * @param string $video_type
     * @param null $limit
     * @return array
     */
    public function get_epg_line_list($video_type = 'video', $limit = null)
    {
        set_time_limit(0);
        $sql = "(select * from nns_import_asset_online where nns_org_id = '{$this->str_sp_id}' and nns_audit='1' and nns_type='{$video_type}' " .
            " and nns_delete='0' and nns_status ='1' and nns_fail_time <= 100 order by nns_create_time asc limit 60) union " .
            "(select * from nns_import_asset_online where nns_org_id = '{$this->str_sp_id}' and nns_audit='1' and nns_type='{$video_type}' " .
            " and nns_delete='0' and nns_status ='2' and nns_fail_time <= 100 order by nns_modify_time asc limit 20) union " .
            "(select * from nns_import_asset_online where nns_org_id = '{$this->str_sp_id}' and nns_audit='1' and nns_type='{$video_type}' " .
            " and nns_delete='0' and nns_status in('4','5','6','7') and nns_fail_time <= 100 order by nns_modify_time asc limit 20)";

        return \nl_asset_online::query_by_sql(\m_config::get_dc(), $sql);
    }

    /**
     * 根据媒资上下线任务类型，批量获取上下线任务的基础信息
     * @param $data //上下线任务数据
     * array(
     *  array(),
     *  array()
     * )
     * @return array(
     *              'ret',
     *              'reason',
     *              'data_info'=>array(
     *                  array(
     *                      'c2_info'=>array(C2信息),
     *                      'video_info'=>array(
     *                          'base_info' => VIDEO信息
     *                          'ex_info'  => 扩展信息
     *                       ),
     *                      'index_info'=>array(
     *                          'base_info' => INDEX信息,
     *                          'ex_info' => 扩展信息,
     *                      ),
     *                      'media_info'=>array(
     *                          'base_info' => MEDIA信息
     *                          'ex_info' => 扩展信息
     *                      ),
     *                  )
     *             )
     *      )
     */
    public function query_mixed_info($data)
    {

        $new_epg_line_queue_info = array();
        $id_arr = array();//当前任务的媒资ID
        //组装批量查询数据
        foreach ($data as $epg_line_queue_info)
        {
            $id_arr[] = $epg_line_queue_info['nns_vod_id'];
        }

        //批量获取主媒资及扩展信息
        $vod_result = \nl_vod_v2::query_vod_by_id(\m_config::get_dc(), $id_arr);
        if($vod_result['ret'] != NS_CDS_SUCCE)
        {
            return $vod_result;
        }
        //数据组装
        $vod_result = $vod_result['data_info'];

        foreach ($data as $epg_line_queue)
        {
            $epg_line_queue_data = array();
            $epg_line_queue_data['epg_line_queue_info'] = $epg_line_queue;
            if(isset($vod_result[$epg_line_queue['nns_vod_id']]))
            {
                $epg_line_queue_data['video_info'] = $vod_result[$epg_line_queue['nns_vod_id']];
            }
            $new_epg_line_queue_info[] = $epg_line_queue_data;
            unset($epg_line_queue_data);
        }

        return \m_config::return_data(NS_CDS_SUCCE,'',$new_epg_line_queue_info);
    }

    /**
     * 检查注入的媒资是否为最终状态
     * @param $video_id
     * @return array
     */
    public function is_finally_status($video_id)
    {
        $media_arr = array();
        $result_vod = \nl_c2_task::query_end_state(\m_config::get_dc(), $video_id, $this->str_sp_id, NS_CDS_VIDEO);
        if($result_vod['ret'] !=0)
        {
            return array('ret'=>4,'reason'=>$result_vod['reason']);
        }
        if(!isset($result_vod['data_info']) || !is_array($result_vod['data_info']) || empty($result_vod['data_info']))
        {
            return array('ret'=>1,'reason'=>'主媒资添加或则修改不是最终状态');
        }
        $result_index = \nl_c2_task::query_end_state(\m_config::get_dc(), $video_id, $this->str_sp_id, NS_CDS_INDEX);
        if($result_index['ret'] !=0)
        {
            return \m_config::return_data(NS_CDS_FAIL, $result_index['reason']);
        }
        if(!isset($result_index['data_info']) || !is_array($result_index['data_info']) || empty($result_index['data_info']))
        {
            return \m_config::return_data(NS_CDS_FAIL, '分集添加或则修改不是最终状态');
        }
        foreach ($result_index['data_info'] as $val)
        {
            $media_arr[]=$val['nns_ref_id'];
        }
        $result_media = \nl_c2_task::query_end_state(\m_config::get_dc(), $media_arr, $this->str_sp_id, NS_CDS_MEDIA);
        if($result_media['ret'] !=0)
        {
            return \m_config::return_data(NS_CDS_FAIL, $result_media['reason']);
        }
        if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
        {
            return \m_config::return_data(NS_CDS_FAIL, '片源添加或则修改不是最终状态');
        }
        return \m_config::return_data(NS_CDS_SUCCE, '媒资是最终状态');
    }
}