<?php
/**
 * epg媒资上下线控制
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/10/10 15:08
 */

namespace ns_model\epg_import;

\ns_core\m_load::load("ns_model.epg_import.epg_line_explain");
/*
epg媒资上下线指令的操作类
提供入栈，出栈方法
*/
class epg_line_queue extends \ns_model\epg_import\epg_line_explain
{

    public function explain()
    {
        /***************效验是否允许切片******************/
        if(isset($this->arr_sp_config['online_disabled']) && (int)$this->arr_sp_config['online_disabled'] !== 1)
        {
            return \m_config::return_data(NS_CDS_FAIL,"sp信息获取spID[{$this->str_sp_id}]不允许切片");
        }
        return \m_config::return_data(NS_CDS_SUCCE,"ok");
    }

    public function push($import_mode,$command){

    }

    public function pop($video_type = 'video')
    {
        //获取注入条数
        $epg_line_queue = $this->get_epg_line_list($video_type);

        if($epg_line_queue['ret'] != NS_CDS_SUCCE || !is_array($epg_line_queue['data_info']) || empty($epg_line_queue['data_info']))
        {
            return \m_config::return_data(NS_CDS_FAIL,'媒资上下线指令注入数据队列查询失败或者数据为空');
        }
        /***************查询媒资上下线任务的媒资信息******************/
        $mixed_info = $this->query_mixed_info($epg_line_queue['data_info']);
        return \m_config::return_data(NS_CDS_SUCCE,'ok', $mixed_info['data_info']);
    }

}