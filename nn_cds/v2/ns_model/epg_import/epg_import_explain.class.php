<?php
namespace ns_model\epg_import;
/**
 * EPG注入流程解释器
 */
class epg_import_explain
{
    public $str_sp_id = null;
    public $arr_sp_config = null;
    public $str_video_type = null;

    public function __construct($sp_id,$video_type)
    {
        $this->str_sp_id = $sp_id;
        $arr_sp_config = \m_config::_get_sp_info($sp_id);
        $arr_sp_config = isset($arr_sp_config['data_info']['nns_config']) ? $arr_sp_config['data_info']['nns_config'] : null;
        $this->arr_sp_config = $arr_sp_config;
        $this->str_video_type = $video_type;
    }

    public function get_epg_task_by_old_func($num,$retry_time=0)
    {

        $sql = "select t1.* from nns_mgtvbk_c2_task t1 where t1.nns_org_id='{$this->str_sp_id}' and (t1.nns_epg_status <> 100 OR t1.nns_epg_status <> 98) ";
        $where_array = array();
        $ext_sql = '';
        switch ($this->str_video_type)
        {
            case NS_CDS_VIDEO:
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_ADD,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_MODIFY,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_DELETE,$retry_time);
                break;
            case NS_CDS_INDEX:
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_ADD,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_MODIFY,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_DELETE,$retry_time);
                $ext_sql = " and  (t1.nns_action='" . NS_CDS_DELETE . "' or exists (select 1 from nns_mgtvbk_c2_task t2 where t2.nns_org_id='{$this->str_sp_id}' 
                and t1.nns_src_id=t2.nns_ref_id and t2.nns_epg_status=99 and t2.nns_type='" . NS_CDS_VIDEO . "'))";
                break;
            case NS_CDS_MEDIA:
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_ADD,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_MODIFY,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_DELETE,$retry_time);
                $ext_sql = " and (t1.nns_action='" . NS_CDS_DELETE . "' or exists (select 1 from nns_mgtvbk_c2_task t2 where t2.nns_org_id='{$this->str_sp_id}' 
                and t1.nns_src_id=t2.nns_ref_id and t2.nns_epg_status in (99) and t2.nns_type='" . NS_CDS_INDEX . "'))";
                break;
            case NS_CDS_LIVE:
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_ADD,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_MODIFY,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_DELETE,$retry_time);
                break;
            case NS_CDS_LIVE_MEDIA:
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_ADD,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_MODIFY,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_DELETE,$retry_time);
                break;
            case NS_CDS_PLAYBILL:
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_ADD,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_MODIFY,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_DELETE,$retry_time);
                break;
            case NS_CDS_PRODUCT:
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_ADD,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_MODIFY,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_DELETE,$retry_time);
                break;
        }
        if (is_array(array_filter($where_array)) && count(array_filter($where_array)) > 0)
        {
            $sql .= ' and (' . implode(array_filter($where_array), ' or ') . ') ';
        }
        else
        {
            return \m_config::return_data(NS_CDS_FAIL,'注入EPG数据SQL拼装错误');
        }
        $sql .= $ext_sql;

        $num = (int)$num <= 0 ? 100 : $num;
        $sql .= ' order by t1.nns_modify_time asc limit ' . $num;
        \m_config::write_epg_execute_log('执行epg注入查询数据sql为:' . $sql, $this->str_sp_id, '');
        //echo $sql;die;
        $result = nl_query_by_db($sql,\m_config::get_dc()->db());
        if(!$result)
        {
            return \m_config::return_data(NS_CDS_FAIL,'注入EPG数据SQL执行错误：'.$sql);
        }
        return \m_config::return_data(NS_CDS_SUCCE,'',$result);
    }

    public function get_epg_live_task_by_old_func($num,$retry_time=0)
    {
        $sql = "select t1.* from nns_mgtvbk_c2_live_task t1 where t1.nns_org_id='{$this->str_sp_id}' and (t1.nns_epg_status <> 100 OR t1.nns_epg_status <> 98) ";
        $where_array = array();
        $ext_sql = '';
        switch ($this->str_video_type)
        {
            case NS_CDS_LIVE:
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_ADD,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_MODIFY,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_DELETE,$retry_time);
                break;
            case NS_CDS_LIVE_MEDIA:
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_ADD,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_MODIFY,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_DELETE,$retry_time);
                break;
            case NS_CDS_PLAYBILL:
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_ADD,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_MODIFY,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_DELETE,$retry_time);
                break;
            case NS_CDS_PRODUCT:
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_ADD,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_MODIFY,$retry_time);
                $where_array[] = $this->__get_child_epg_sql_by_old_func(NS_CDS_DELETE,$retry_time);
                break;
        }
        if (is_array(array_filter($where_array)) && count(array_filter($where_array)) > 0)
        {
            $sql .= ' and (' . implode(array_filter($where_array), ' or ') . ') ';
        }
        else
        {
            return \m_config::return_data(NS_CDS_FAIL,'注入EPG数据SQL拼装错误');
        }
        $sql .= $ext_sql;

        $num = (int)$num <= 0 ? 100 : $num;
        $sql .= ' order by t1.nns_modify_time asc limit ' . $num;
        \m_config::write_epg_execute_log('执行epg注入查询数据sql为:' . $sql, $this->str_sp_id, '');
        //echo $sql;die;
        $result = nl_query_by_db($sql,\m_config::get_dc()->db());
        if(!$result)
        {
            return \m_config::return_data(NS_CDS_FAIL,'注入EPG数据SQL执行错误：'.$sql);
        }
        return \m_config::return_data(NS_CDS_SUCCE,'',$result);
    }

    /**
     * 拼装EPG子级SQL
     * @param $action //操作动作
     * @param $retry_time //重试次数
     * @return null|string
     */
    private function __get_child_epg_sql_by_old_func($action,$retry_time)
    {
        //获取策略
        $config_action = $this->arr_sp_config[$this->str_video_type . '_' . $action];
        //最终状态是1的排除
        if ((int)$config_action['status'] === 1)
        {
            return null;
        }
        $ext_sql = ((int)$retry_time > 0) ? "and t1.nns_epg_status=" . NS_CDS_EPG_FAIL ." and t1.nns_epg_fail_time <= $retry_time" : "and t1.nns_epg_status=" . NS_CDS_EPG_WAIT;
        //CDN优先
        if ((int)$config_action['priority'] === 0)
        {
            //0为注入CDN和EPG，1为注入CDN，2为注入EPG
            if ((int)$config_action['status'] === 0)
            {
                return "(t1.nns_type='{$this->str_video_type}'  and t1.nns_action='{$action}'  and t1.nns_status=" . NS_CDS_CDN_SUCCE . " {$ext_sql})";
            }
            elseif ((int)$config_action['status'] === 2)
            {
                return "(t1.nns_type='{$this->str_video_type}'  and t1.nns_action='{$action}' {$ext_sql})";
            }
        }
        elseif ((int)$config_action['priority'] === 1)
        {
            //0为注入CDN和EPG，1为注入CDN，2为注入EPG
            return "(t1.nns_type='{$this->str_video_type}'  and t1.nns_action='{$action}' {$ext_sql})";
        }
        return null;
    }
    /**
     * 根据C2任务类型，批量获取C2任务的基础信息
     * @param $data C2任务数据
     * @return array(
     *              'ret',
     *              'reason',
     *              'data_info'=>array(
     *                  array(
     *                      'c2_info'=>array(C2信息),
     *                      'video_info'=>array(
     *                          'base_info' => VIDEO信息
     *                          'ex_info'  => 扩展信息
     *                       ),
     *                      'index_info'=>array(
     *                          'base_info' => INDEX信息,
     *                          'ex_info' => 扩展信息,
     *                      ),
     *                      'media_info'=>array(
     *                          'base_info' => MEDIA信息
     *                          'ex_info' => 扩展信息
     *                      ),
     *                  )
     *             )
     *      )
     */
    public function query_mixed_info($data)
    {
        $new_c2_info = array();
        $ref_arr = array();//当前任务的主键ID
        $src_arr = array();//当前任务的父级主键ID
        //组装批量查询数据
        foreach ($data as $c2_info)
        {
            if(!empty($c2_info['nns_ref_id']))
            {
                $ref_arr[] = $c2_info['nns_ref_id'];
            }
            else
            {
                continue;
            }
            if(!empty($c2_info['nns_src_id']))
            {
                $src_arr[] = $c2_info['nns_src_id'];
            }
        }
        if(empty($ref_arr))
        {
            return \m_config::return_data(NS_CDS_FAIL,'EPG注入批量查询媒资数据入参参数错误');
        }
        //根据类型获取不同的数据源
        switch ($this->str_video_type)
        {
            case 'video':
                //批量获取主媒资及扩展信息
                $vod_result = \nl_vod_v2::query_vod_by_id(\m_config::get_dc(),$ref_arr);
                if($vod_result['ret'] != NS_CDS_SUCCE)
                {
                    return $vod_result;
                }
                //数据组装
                $vod_result = $vod_result['data_info'];
                foreach ($data as $c2)
                {
                    $c2_data = array();
                    $c2_data['c2_info'] = $c2;
                    if(isset($vod_result[$c2['nns_ref_id']]))
                    {
                        $c2_data['video_info'] = $vod_result[$c2['nns_ref_id']];
                    }
                    $new_c2_info[] = $c2_data;
                    unset($c2_data);
                }
                break;
            case 'index':
                //批量获取分集及扩展信息
                $vod_result = \nl_vod_v2::query_vod_by_id(\m_config::get_dc(),$src_arr);
                if($vod_result['ret'] != NS_CDS_SUCCE)
                {
                    return $vod_result;
                }
                $vod_result = $vod_result['data_info'];
                //批量获取分集及扩展信息
                $vod_index_result = \nl_vod_index_v2::query_vod_index_by_id(\m_config::get_dc(),$ref_arr);
                if($vod_index_result['ret'] != NS_CDS_SUCCE)
                {
                    return $vod_index_result;
                }
                $vod_index_result = $vod_index_result['data_info'];
                foreach ($data as $c2)
                {
                    $c2_data = array();
                    $c2_data['c2_info'] = $c2;
                    if(isset($vod_result[$c2['nns_src_id']]))
                    {
                        $c2_data['video_info'] = $vod_result[$c2['nns_src_id']];
                    }
                    if(isset($vod_index_result[$c2['nns_ref_id']]))
                    {
                        $c2_data['index_info'] = $vod_index_result[$c2['nns_ref_id']];
                    }
                    $params['nns_video_id']=$vod_result[$c2['nns_src_id']]['nns_vod_id'];
                    $params['nns_video_index']=$vod_index_result[$c2['nns_ref_id']]['nns_id'];
                    $seekpoint_result=\nl_seekpoint::query_by_condition(\m_config::get_dc(), $params);
                    if($seekpoint_result['ret']=== NS_CDS_SUCCE)
                    {
                        $c2_data['seekpoint_info']=$seekpoint_result['result'];
                    }
                    $new_c2_info[] = $c2_data;
                    unset($c2_data);
                }
                break;
            case 'media':
                //批量获取片源及扩展信息
                $vod_index_result = \nl_vod_index_v2::query_vod_index_by_id(\m_config::get_dc(),$src_arr);
                if($vod_index_result['ret'] != NS_CDS_SUCCE)
                {
                    return $vod_index_result;
                }
                $vod_index_result = $vod_index_result['data_info'];
                //批量获取片源及扩展信息
                $vod_media_result = \nl_vod_media_v2::query_vod_media_by_id(\m_config::get_dc(),$ref_arr);
                if($vod_media_result['ret'] != NS_CDS_SUCCE)
                {
                    return $vod_media_result;
                }
                $vod_media_result = $vod_media_result['data_info'];
                //批量获取主媒资及扩展信息
                $vod_guid = array();
                foreach ($vod_index_result as $index_re)
                {
                    $vod_guid[$index_re['base_info']['nns_id']] = $index_re['base_info']['nns_vod_id'];
                }
                $vod_result = \nl_vod_v2::query_vod_by_id(\m_config::get_dc(),array_values($vod_guid));
                if($vod_result['ret'] != NS_CDS_SUCCE)
                {
                    return $vod_result;
                }
                $vod_result = $vod_result['data_info'];
                foreach ($data as $c2)
                {
                    $c2_data =  array();
                    $c2_data['c2_info'] = $c2;
                    if(isset($vod_result[$vod_guid[$c2['nns_src_id']]]))
                    {
                        $c2_data['video_info'] = $vod_result[$vod_guid[$c2['nns_src_id']]];
                    }
                    if(isset($vod_index_result[$c2['nns_src_id']]))
                    {
                        $c2_data['index_info'] = $vod_index_result[$c2['nns_src_id']];
                    }
                    if(isset($vod_media_result[$c2['nns_ref_id']]))
                    {
                        $c2_data['media_info'] = $vod_media_result[$c2['nns_ref_id']];
                    }
                    $new_c2_info[] = $c2_data;
                    unset($c2_data);
                }
                break;
            case 'seekpoint':
                //批量获取打点信息
                $seekpoint_result = \nl_seekpoint::query_seekpoint_by_id(\m_config::get_dc(),$ref_arr);
                if($seekpoint_result['ret'] != NS_CDS_SUCCE)
                {
                    return $seekpoint_result;
                }
                //数据组装
                $seekpoint_result = $seekpoint_result['data_info'];
                foreach ($data as $c2)
                {
                    $c2_data = array();
                    $c2_data['c2_info'] = $c2;
                    if(isset($seekpoint_result[$c2['nns_ref_id']]))
                    {
                        $c2_data['seekpoint_info'] = $seekpoint_result[$c2['nns_ref_id']];
                        //打点分集信息
                        $vod_index_result= \nl_vod_index_v2::query_vod_index_by_id(\m_config::get_dc(),array($c2_data['seekpoint_info']['nns_vod_index_id']));
                        if($vod_index_result['ret'] == NS_CDS_SUCCE)
                        {
                            $c2_data['index_info']= $vod_index_result['data_info'][$c2_data['seekpoint_info']['nns_vod_index_id']];
                        }
                    }
                    $new_c2_info[] = $c2_data;
                    unset($c2_data);
                }
                break;
            case 'live':
                //批量获取直播及扩展信息
                $live_result = \nl_live::query_live_by_id(\m_config::get_dc(),$ref_arr);
                if($live_result['ret'] != NS_CDS_SUCCE)
                {
                    return $live_result;
                }
                //数据组装
                $live_result = $live_result['data_info'];
                foreach ($data as $c2)
                {
                    $c2_data = array();
                    $c2_data['c2_info'] = $c2;
                    if(isset($live_result[$c2['nns_ref_id']]))
                    {
                        $c2_data['live_info'] = $live_result[$c2['nns_ref_id']];
                    }
                    $new_c2_info[] = $c2_data;
                    unset($c2_data);
                }
                break;
            case 'live_index':

                break;
            case 'live_media':
                //批量获取直播分集及扩展信息
                $live_index_result = \nl_live_index::query_live_index_by_id(\m_config::get_dc(),$src_arr);
                if($live_index_result['ret'] != NS_CDS_SUCCE)
                {
                    return $live_index_result;
                }
                $live_index_result = $live_index_result['data_info'];
                //批量获取直播片源及扩展信息
                $live_media_result = \nl_live_media::query_live_media_by_id(\m_config::get_dc(),$ref_arr);
                if($live_media_result['ret'] != NS_CDS_SUCCE)
                {
                    return $live_media_result;
                }
                $live_media_result = $live_media_result['data_info'];
                //批量获取直播及扩展信息
                $live_guid = array();
                foreach ($live_index_result as $index_re)
                {
                    $live_guid[$index_re['base_info']['nns_id']] = $index_re['base_info']['nns_live_id'];
                }
                $live_result = \nl_live::query_live_by_id(\m_config::get_dc(),array_values($live_guid));
                if($live_result['ret'] != NS_CDS_SUCCE)
                {
                    return $live_result;
                }
                $live_result = $live_result['data_info'];
                foreach ($data as $c2)
                {
                    $c2_data =  array();
                    $c2_data['c2_info'] = $c2;
                    if(isset($live_result[$live_guid[$c2['nns_src_id']]]))
                    {
                        $c2_data['live_info'] = $live_result[$live_guid[$c2['nns_src_id']]];
                    }
                    if(isset($live_index_result[$c2['nns_src_id']]))
                    {
                        $c2_data['live_index_info'] = $live_index_result[$c2['nns_src_id']];
                    }
                    if(isset($live_media_result[$c2['nns_ref_id']]))
                    {
                        $c2_data['live_media_info'] = $live_media_result[$c2['nns_ref_id']];
                    }
                    $new_c2_info[] = $c2_data;
                    unset($c2_data);
                }
                break;
            case 'playbill':
                //批量获取直播节目单信息
                $playbill_result = \nl_playbill::query_playbill_by_id(\m_config::get_dc(),$ref_arr);
                //$sql = "select * from nns_live_playbill_item where nns_id in('".implode("','", $ref_arr)."')";
                //$result = nl_query_by_db($sql,\m_config::get_dc()->db());
                if($playbill_result['ret'] != NS_CDS_SUCCE)
                {
                    return $playbill_result;
                }
                //数据组装
                $playbill_result = $playbill_result['data_info'];
                foreach ($data as $c2)
                {
                    $c2_data = array();
                    $c2_data['c2_info'] = $c2;
                    if(isset($playbill_result[$c2['nns_ref_id']]))
                    {
                       $c2_data['playbill_info'] = $playbill_result[$c2['nns_ref_id']];
                        //直播频道信息
                        $live_result=\nl_live::query_live_by_id(\m_config::get_dc(),array($c2_data['playbill_info']['base_info']['nns_live_id']));
                        if($live_result['ret'] == NS_CDS_SUCCE)
                        {
                            $c2_data['live_info']= $live_result['data_info'][$c2_data['playbill_info']['base_info']['nns_live_id']];
                        }
                        //直播源信息，这里特殊，找到该频道下直播源为nns_type = '0'的节目单，正常情况下来讲只有一个
                        $live_media_result = \nl_live_media::query_by_condition(\m_config::get_dc(),array("nns_live_id"=>$c2_data['playbill_info']['base_info']['nns_live_id'],"nns_type"=>'0'));

                        if($live_media_result['ret'] == NS_CDS_SUCCE)
                        {
                            $c2_data['live_media_info']['base_info']= $live_media_result['data_info']['0'];
                        }
                    }
                    $new_c2_info[] = $c2_data;
                    unset($c2_data);
                }
                break;
            case 'file':
                //批量获取文件包信息
                $file_result = \nl_file_package::query_live_by_id(\m_config::get_dc(),$ref_arr);
                if($file_result['ret'] != NS_CDS_SUCCE)
                {
                    return $file_result;
                }
                //数据组装
                $file_result = $file_result['data_info'];
                foreach ($data as $c2)
                {
                    $c2_data = array();
                    $c2_data['c2_info'] = $c2;
                    if(isset($live_result[$c2['nns_ref_id']]))
                    {
                        $c2_data['file_info'] = $file_result[$c2['nns_ref_id']];
                    }
                    $new_c2_info[] = $c2_data;
                    unset($c2_data);
                }
                break;
        }
        return \m_config::return_data(NS_CDS_SUCCE,'',$new_c2_info);
    }
}