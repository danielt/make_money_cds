<?php
namespace ns_model\epg_import;
\ns_core\m_load::load("ns_model.epg_import.epg_import_explain");
\ns_core\m_load::load_old("nn_logic/c2_task/c2_task.class.php");
\ns_core\m_load::load_old("nn_logic/vod_v2/vod_v2.class.php");
\ns_core\m_load::load_old("nn_logic/vod_index/vod_index.class.php");
\ns_core\m_load::load_old("nn_logic/vod_media/vod_media.class.php");
/**
 * EPG注入入库
 */
class epg_import_queue extends \ns_model\epg_import\epg_import_explain
{
    /**
     * 对epg注入队列的解释
     * 确认队列执行条件等
     */
    public function explain()
    {
        /***************效验是否允许EPG注入******************/
        if(isset($this->arr_sp_config['disabled_epg']) && (int)$this->arr_sp_config['disabled_epg'] === 1)
        {
            return \m_config::return_data(NS_CDS_FAIL,"sp信息获取spID[{$this->str_sp_id}]不允许EPG注入");
        }
        /***************效验类型是否允许注入******************/
        if(!isset($this->arr_sp_config['epg_assets_enabled']) || !is_array($this->arr_sp_config['epg_assets_enabled']) || (is_array($this->arr_sp_config['epg_assets_enabled']) && !in_array($this->str_video_type, $this->arr_sp_config['epg_assets_enabled'])))
        {
            return \m_config::return_data(NS_CDS_FAIL,"sp信息获取spID[{$this->str_sp_id}]注入EPG类型[{$this->str_video_type}]不允许注入");
        }
    }
    /**
     * 累加C2任务
     * @param $message
     */
	public function push($message)
    {

	}
    /**
     * 抛出注入EPG的媒资数据
     * @param $wait_num 获取等待条数
     * @param $fail_num 获取失败条数
     * @param $retry_time 重试多少次前的失败条数
     * @return array
     */
	public function pop($wait_num=100,$retry_time=0,$fail_num=100)
    {
        $result_wait = array();
        $result_fail = array();
        /***************获取等待注入EPG的条数******************/
        if(in_array($this->str_video_type,array(NS_CDS_LIVE,NS_CDS_LIVE_MEDIA,NS_CDS_PLAYBILL)))
        {
            $arr_result_wait = $this->get_epg_live_task_by_old_func($wait_num);
        }
        else
        {
            $arr_result_wait = $this->get_epg_task_by_old_func($wait_num);
        }
        if($arr_result_wait['ret'] != '0')
        {
            return \m_config::return_data(NS_CDS_FAIL,$arr_result_wait['reason']);
        }
        if(is_array($arr_result_wait['data_info']))
        {
            $result_wait = $arr_result_wait['data_info'];
        }
        /***************重试注入EPG******************/
        if((int)$retry_time > 0)
        {
            if(in_array($this->str_video_type,array(NS_CDS_LIVE,NS_CDS_LIVE_MEDIA,NS_CDS_PLAYBILL)))
            {
                $arr_result_fail = $this->get_epg_live_task_by_old_func($fail_num,$retry_time);
            }
            else
            {
                $arr_result_fail = $this->get_epg_task_by_old_func($fail_num,$retry_time);
            }
            if($arr_result_fail['ret'] != NS_CDS_SUCCE)
            {
                return \m_config::return_data(NS_CDS_FAIL,$arr_result_fail['reason']);
            }
            if(is_array($arr_result_fail['data_info']))
            {
                $result_fail = $arr_result_fail['data_info'];
            }
        }
        /***************融合等待与重试的C2任务******************/
        $last_data = array_merge($result_wait,$result_fail);
        unset($result_fail);
        unset($result_wait);
        if(empty($last_data))
        {
            return \m_config::return_data(NS_CDS_FAIL,'EPG注入数据为空');
        }
        /***************批量查询C2任务的媒资信息******************/
        $mixed_info = $this->query_mixed_info($last_data);
        if($mixed_info['ret'] != NS_CDS_SUCCE)
        {
            return \m_config::return_data(NS_CDS_FAIL,'EPG注入数据的媒资信息查询失败');
        }
        return \m_config::return_data(NS_CDS_SUCCE,'ok',$mixed_info['data_info']);
	}
}