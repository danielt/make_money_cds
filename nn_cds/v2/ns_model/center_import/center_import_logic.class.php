<?php
namespace ns_model\center_import;
use Symfony\Component\Yaml\Dumper;

class center_import_pop_logic
{
    public $queue_info = null;
    public $data_info = null;
    public $ext_info = null;
    
    public function __construct($queue_info=null,$data_info=null,$ext_info=null)
    {
        $this->queue_info = $queue_info;
        $this->data_info = $data_info;
        $this->ext_info = $ext_info;
    }
    
    public function __destruct()
    {
        $this->data_info = null;
        $this->queue_info = null;
        $this->ext_info = null;
    }
}

class center_import_push_logic
{
    public $queue_info = null;
    public $data_info = null;
    public $ext_info = null;
    
    public function __construct($queue_info=null,$data_info=null,$ext_info=null)
    {
        $this->queue_info = $queue_info;
        $this->data_info = $data_info;
        $this->ext_info = $ext_info;
    }
    
    
    /**
     * 获取CP 绑定SP的  logic
     * @param unknown $this->queue_info
     * @param unknown $this->data_info
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    function get_cp_bind_sp_logic()
    {
        $result_cp = \m_config::_get_cp_info($this->queue_info['base_info']['nns_cp_id']);
        if($result_cp['ret'] !=0)
        {
            return $result_cp;
        }
        $arr_cp_config = $result_cp['data_info'];
        $result_sp = \m_config::_get_cp_bind_sp_info($this->queue_info['base_info']['nns_cp_id']);
        if($result_sp['ret'] !=0)
        {
            return $result_sp;
        }
        if(!isset($result_sp['data_info']) || empty($result_sp['data_info']) || !is_array($result_sp['data_info']))
        {
            return \m_config::return_data(1,'查询无任何绑定的SP信息');
        }
        if(isset($arr_cp_config['nns_config']['message_import_enable']) && $arr_cp_config['nns_config']['message_import_enable'] == '1' && isset($arr_cp_config['nns_config']['message_import_mode']) && $arr_cp_config['nns_config']['message_import_mode'] == '1' && $this->queue_info['base_info']['nns_video_type'] == 'media')
        {
            if(!isset($this->data_info[$this->queue_info['base_info']['nns_video_type']]['base_info']['nns_content_id']) || strlen($this->data_info[$this->queue_info['base_info']['nns_video_type']]['base_info']['nns_content_id']) <1)
            {
                return \m_config::return_data(1,'查询片源无nns_content_id不再继续注入队列');
            }
        }
        $temp_op = null;
        foreach ($result_sp['data_info'] as $sp_id=>$sp_config)
        {
            $sp_config = (isset($sp_config['nns_config']) && !empty($sp_config['nns_config'])) ? $sp_config['nns_config'] : null;
            //此SP关闭注入  SP 停止注入
            if(isset($sp_config['import_op_enabled']) && (int)$sp_config['import_op_enabled'] === 1)
            {
                \m_config::write_message_execute_log('[消息为' . $this->queue_info['base_info']['nns_message_id'] . '][import_op_enabled中心指令开关关闭]'.$sp_id, $this->queue_info['base_info']['nns_cp_id']);
                continue;
            }
            //主媒资、分集、片源、直播、节目单未选择  SP停止注入
            if(isset($sp_config['op_queue_video_enabled']) && is_array($sp_config['op_queue_video_enabled']) &&
                !empty($sp_config['op_queue_video_enabled']) && !in_array($this->queue_info['base_info']['nns_video_type'], $sp_config['op_queue_video_enabled']))
            {
                \m_config::write_message_execute_log('[消息为' . $this->queue_info['base_info']['nns_message_id'] . '][op_queue_video_enable队列不允许通过此媒资类型]'.$sp_id, $this->queue_info['base_info']['nns_cp_id']);
                continue;
            }
            //如果为片源 检查片源文件类型是否 允许注入（ts、MP4、flv...）
            if($this->queue_info['base_info']['nns_video_type'] == 'media' && isset($sp_config['op_queue_filetype_enabled']) && is_array($sp_config['op_queue_filetype_enabled']) &&
                !empty($sp_config['op_queue_filetype_enabled']) && !in_array(strtolower($this->data_info[$this->queue_info['base_info']['nns_video_type']]['base_info']['nns_filetype']), $sp_config['op_queue_filetype_enabled']))
            {
                \m_config::write_message_execute_log('[消息为' . $this->queue_info['base_info']['nns_message_id'] . '][op_queue_filetype_enabled队列不允许通过此片源文件格式类型]'.$sp_id, $this->queue_info['base_info']['nns_cp_id']);
                continue;
            }
            //如果为片源 检查片源转码类型 允许注入（CDN片源、原始片源、转码片源）
            if($this->queue_info['base_info']['nns_video_type'] == 'media' && isset($sp_config['op_queue_media_type_enabled']) && is_array($sp_config['op_queue_media_type_enabled']) &&
                !empty($sp_config['op_queue_media_type_enabled']) && !in_array($this->data_info[$this->queue_info['base_info']['nns_video_type']]['base_info']['nns_encode_flag'], $sp_config['op_queue_media_type_enabled']))
            {
                \m_config::write_message_execute_log('[消息为' . $this->queue_info['base_info']['nns_message_id'] . '][op_queue_media_type_enabled队列不允许通过此片源转码类型]'.$sp_id, $this->queue_info['base_info']['nns_cp_id']);
                continue;
            }
            //过滤片源清晰度
            if($this->queue_info['base_info']['nns_video_type'] === 'media' && isset($sp_config['media_import_op_mode']) && is_array($sp_config['media_import_op_mode']) &&
                !empty($sp_config['media_import_op_mode']) && in_array(strtolower($this->data_info[$this->queue_info['base_info']['nns_video_type']]['base_info']['nns_mode']), $sp_config['media_import_op_mode']))
            {
                \m_config::write_message_execute_log('[消息为' . $this->queue_info['base_info']['nns_message_id'] . '][media_import_op_mode队列不允许通过片源清晰度]'.$sp_id, $this->queue_info['base_info']['nns_cp_id']);
                continue;
            }
            //过滤片源终端展示类型
            if($this->queue_info['base_info']['nns_video_type'] == 'media' && isset($sp_config['op_queue_media_tag_enabled']) && is_array($sp_config['op_queue_media_tag_enabled']) &&
                !empty($sp_config['op_queue_media_tag_enabled']) && isset($this->data_info[$this->queue_info['base_info']['nns_video_type']]['base_info']['nns_tag']) && strlen($this->data_info[$this->queue_info['base_info']['nns_video_type']]['base_info']['nns_tag']) >0 )
            {
                $flag = true;
                $arr_tag = explode(',', $this->data_info[$this->queue_info['base_info']['nns_video_type']]['base_info']['nns_tag']);
                if(is_array($arr_tag) && !empty($arr_tag))
                {
                    foreach ($arr_tag as $tag_val)
                    {
                        if(strlen($tag_val) <1)
                        {
                            continue;
                        }
                        if(in_array($tag_val, $sp_config['op_queue_media_tag_enabled']))
                        {
                            $flag = false;
                            break;
                        }
                    }
                }
                else
                {
                    $flag = false;
                }
                if($flag)
                {
                    \m_config::write_message_execute_log('[消息为' . $this->queue_info['base_info']['nns_message_id'] . '][op_queue_media_tag_enabled队列不允许通过此终端TAG]'.$sp_id, $this->queue_info['base_info']['nns_cp_id']);
                    continue;
                }
            }

            //bug修复，在数据模型层被json的数组nns_conf_info由于格式化htmlspecialchars()函数将引号格式化了，这里需要反格式化html_entity_decode()
            //xinxin.deng 2018/9/12 16:53 ---start
            $video_conf_info_arr = '';
            if(isset($this->data_info[$this->queue_info['base_info']['nns_video_type']]['base_info']['nns_conf_info']) && strlen($this->data_info[$this->queue_info['base_info']['nns_video_type']]['base_info']['nns_conf_info']) >0)
            {
                $video_conf_info_arr = html_entity_decode($this->data_info[$this->queue_info['base_info']['nns_video_type']]['base_info']['nns_conf_info']);
            }
            //---end
            //扩展信息中包含配置信息 筛选
            if(isset($video_conf_info_arr) && strlen($video_conf_info_arr) >0
                && \m_config::is_json($video_conf_info_arr)
                && isset($sp_config['global_config_bind_key']) && !empty($sp_config['global_config_bind_key'])) //扩展信息中包含配置信息
            {
                $conf_info = json_decode($video_conf_info_arr,true);
                $gloabl_config_key = rtrim($sp_config['global_config_bind_key'],",");
                $gloabl_config_key_arr = explode(",", $gloabl_config_key);
                $key_value = \nl_project_group::get_value_by_ids(\m_config::get_dc()->db(),$gloabl_config_key_arr);

                if(!is_array($key_value)) //通用配置未配置条件
                {
                    \m_config::write_message_execute_log('[消息为' . $this->queue_info['base_info']['nns_message_id'] . '][global_config_bind_key通用配置开启但未配置条件，队列未生成]'.$sp_id, $this->queue_info['base_info']['nns_cp_id']);
                    continue;
                }
                if(isset($key_value['import_cdn_mode']))
                {
                    \m_config::write_message_execute_log('[通用配置为' . var_export($conf_info['import_cdn_mode'], true), $this->queue_info['base_info']['nns_cp_id']);
                    $del_sp_bool = false;
                    foreach ($key_value['import_cdn_mode'] as $mode)
                    {
                        if($mode == $conf_info['import_cdn_mode'])
                        {
                            $del_sp_bool = true;//指定SP注入
                            break;
                        }
                    }
                    if(!$del_sp_bool)
                    {
                        \m_config::write_message_execute_log('[消息为' . $this->queue_info['base_info']['nns_message_id'] . '][global_config_bind_key通用配置开启但当前SPID未满足条件，队列未生成]'.$sp_id, $this->queue_info['base_info']['nns_cp_id']);
                        continue;
                    }
                }
            }
            $temp_op[]=$sp_id;
        }

        \m_config::write_message_execute_log('[消息为' . $this->queue_info['base_info']['nns_message_id'] . ']队列分发：'.var_export($temp_op,true), $this->queue_info['base_info']['nns_cp_id']);
        return \m_config::return_data(0,'ok',(isset($temp_op) && is_array($temp_op) && !empty($temp_op)) ? ','.implode(',', $temp_op).',' : '');
    }
    
    
    public function __destruct()
    {
        $this->data_info = null;
        $this->queue_info = null;
        $this->ext_info = null;
    }
}