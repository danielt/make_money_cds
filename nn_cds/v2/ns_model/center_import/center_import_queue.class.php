<?php
namespace ns_model\center_import;
/**
 * 中心同步指令执行器
 * @author pan.liang
 */
\ns_core\m_load::load("ns_data_model.queue.m_center_import_inout");
\ns_core\m_load::load("ns_model.m_queue_model");
\ns_core\m_load::load("ns_model.center_import.center_import_logic");
\ns_core\m_load::load("ns_model.command.command");
\ns_core\m_load::load("ns_model.command.command_task");
class center_import_queue extends \ns_model\m_queue_model
{
    /**
     * push
     * @param unknown $message
     */
	public function push($message)
	{
	    //查询媒资信息
	    $result_info = $this->_get_queue_info($message['base_info']['nns_video_id'],$message['base_info']['nns_video_type'],true);
	    if($result_info['ret'] !=0)
	    {
	        return $result_info;
	    }
	    //查询队列名称
	    $result_info_name = $this->_get_queue_name($message['base_info']['nns_video_type'], $result_info['data_info']);
	    if($result_info_name['ret'])
	    {
	        return $result_info_name;
	    }
	    $message['base_info']['nns_video_name'] = $result_info_name['data_info'];
	    //获取分发策略后分发到sp的队列信息
	    $push_logic = $this->push_logic($message, $result_info['data_info']);
	    if($push_logic['ret'] !=0)
	    {
	        return $push_logic;
	    }
        /************************指令池*************************/
        $original_id = $message['base_info']['nns_video_type'] === 'video' ? $result_info['data_info'][$message['base_info']['nns_video_type']]['base_info']['nns_asset_import_id'] : $result_info['data_info'][$message['base_info']['nns_video_type']]['base_info']['nns_import_id'];
        $command_arr = array(
            'base_info' => array(
                'nns_message_id' => $push_logic['data_info']['base_info']['nns_message_id'],
                'nns_source_id' => $push_logic['data_info']['base_info']['nns_cp_id'],
                'nns_type' => $push_logic['data_info']['base_info']['nns_video_type'],
                'nns_original_id' => $original_id,
                'nns_name' => $result_info['data_info'][$message['base_info']['nns_video_type']]['base_info']['nns_name'],
                'nns_content_id' => $push_logic['data_info']['base_info']['nns_video_id'],
            )
        );
        $command = new \ns_model\command\command();
        //先进性队列指令查询,存在则直接返回
        $query_command_arr = array(
            'base_info' => array(
                'nns_message_id' => $push_logic['data_info']['base_info']['nns_message_id'],
            ),
        );
        $query_command = $command->query($query_command_arr);
        unset($query_command_arr);
        if ($query_command['ret'] != 0 )
        {
            \m_config::write_message_execute_log('[消息为' . $message['base_info']['nns_message_id'] . ']' . var_export($query_command,true), $message['base_info']['nns_cp_id']);
            //return $query_command;
        }
        if (!empty($query_command['data_info']) && is_array($query_command['data_info']) && count($query_command['data_info']) > 0)
        {
            $bool_command_exist = false;
            foreach ($query_command['data_info'] as $val)
            {
                $arr = array(
                    'nns_message_id' => $val['nns_message_id'],
                    'nns_source_id' => $val['nns_source_id'],
                    'nns_type' => $val['nns_type'],
                    'nns_original_id' => $val['nns_original_id'],
                    'nns_name' => $val['nns_name'],
                    'nns_content_id' => $val['nns_content_id'],
                );
                $diff_arr = array_diff($command_arr['base_info'], $arr);
                if (empty($diff_arr))
                {
                    $bool_command_exist = true;
                    continue;
                }
            }
            unset($diff_arr);unset($arr);
            if ($bool_command_exist)
            {
                \m_config::write_message_execute_log('指令队列已经存在' . var_export($command_arr, true), $message['base_info']['nns_cp_id']);
                //return \m_config::return_data(NS_CDS_FAIL,'指令队列已经存在' . var_export($command_arr, true));
                $command_re["data_info"]["info"]["out"]["base_info"]["nns_id"] = $query_command['data_info'][0]['nns_id'];
            }
            else
            {
                $command_re = $command->push($command_arr);
                unset($command);
                if($command_re['ret'] != NS_CDS_SUCCE)
                {
                    \m_config::write_message_execute_log('查询队列池失败' . var_export($command_re,true), $message['base_info']['nns_cp_id']);
                    return $command_re;
                }
            }
        }
        else
        {
            $command_re = $command->push($command_arr);
            unset($command);
            if($command_re['ret'] != NS_CDS_SUCCE)
            {
                \m_config::write_message_execute_log('查询队列池失败' . var_export($command_re,true), $message['base_info']['nns_cp_id']);
                return $command_re;
            }
        }
        /************************指令池*************************/
        /************************指令状态池*************************/
        $command_id = $command_re["data_info"]["info"]["out"]["base_info"]["nns_id"];
        $op_sp_arr = explode(",",$push_logic['data_info']['base_info']['nns_op_sp']);
        $command_task = new \ns_model\command\command_task();
        foreach ($op_sp_arr as $sp)
        {
            if(empty($sp))
            {
                continue;
            }
            $command_task_arr = array(
                'base_info' => array(
                    'nns_command_id' => $command_id,
                    'nns_sp_id' => $sp,
                    'nns_action' => $push_logic['data_info']['base_info']['nns_action'],
                    'nns_status' => 1, //等待注入
                )
            );
            $compare_command_task_arr = array(
                'base_info' => array(
                    'nns_command_id' => $command_id,
                    'nns_sp_id' => $sp,
                    'nns_action' => $push_logic['data_info']['base_info']['nns_action'],
                )
            );
            //先进性队列指令状态池查询,存在则直接返回
            $query_command_task_arr = array(
                'base_info' => array(
                    'nns_command_id' => $command_id,
                )
            );
            $query_command_task = $command_task->query($query_command_task_arr);
            unset($query_command_task_arr);
            if ($query_command_task['ret'] != 0 )
            {
                \m_config::write_message_execute_log('[消息为' . $message['base_info']['nns_message_id'] . ']指令状态池查询失败：' . var_export($query_command_task,true), $message['base_info']['nns_cp_id']);
                //return $query_command_task;
            }

            if (!empty($query_command_task['data_info']) && is_array($query_command_task['data_info']) && count($query_command_task['data_info']) > 0)
            {
                $bool_command_task_exist = false;
                foreach ($query_command_task['data_info'] as $val)
                {
                    $arr = array(
                        'nns_command_id' => $val['nns_command_id'],
                        'nns_sp_id' => $val['nns_sp_id'],
                        'nns_action' => $val['nns_action'],
                    );
                    $diff_arr = array_diff($compare_command_task_arr['base_info'], $arr);
                    if (empty($diff_arr))
                    {
                        $bool_command_task_exist = true;
                        continue;
                    }
                }
                unset($diff_arr);unset($arr);
                if ($bool_command_task_exist)
                {
                    \m_config::write_message_execute_log('[消息为' . $message['base_info']['nns_message_id'] . ']指令状态池数据已存在：' . var_export($query_command_task,true), $message['base_info']['nns_cp_id']);
                    continue;
                }
            }
            $command_task_re = $command_task->push($command_task_arr);
            if($command_task_re['ret'] != NS_CDS_SUCCE)
            {
                return $command_task_re;
            }
        }
        unset($command_task);
        /************************指令状态池*************************/
        //进入中心队列
        $push_logic['data_info']['base_info']['nns_command_id'] = $command_id;
	    $obj_excute = new \m_center_import_inout();
        return $obj_excute->add($push_logic['data_info']);
	}
	
	public function push_logic($message, $result_info)
	{
	    $center_import_push_logic = new \ns_model\center_import\center_import_push_logic($message, $result_info);
	    $result_op_sp =  $center_import_push_logic->get_cp_bind_sp_logic();
	    if($result_op_sp['ret'] !=0)
	    {
	        return $result_op_sp;
	    }
	    if(!isset($result_op_sp['data_info']) || strlen($result_op_sp['data_info']) <1)
	    {
	        return \m_config::return_data(1,'没匹配到任何SP下游平台');
	    }
	    $message['base_info']['nns_op_sp'] = $result_op_sp['data_info'];
	    return \m_config::return_data(0,'Ok',$message);
	}

	/**
	 * pop
	 * @param unknown $import_mode
	 */
	public function pop($import_mode)
	{
	    $sp_config = \m_config::_get_sp_info($import_mode);
	    $import_num = (isset($sp_config['data_info']['nns_config']['import_op_pre_max']) && (int)$sp_config['data_info']['nns_config']['import_op_pre_max'] >0) ? (int)$sp_config['data_info']['nns_config']['import_op_pre_max'] : 20;
	    $query_sql ="select * from nns_mgtvbk_import_op where nns_op_sp like '%,{$import_mode},%' order by nns_weight desc,nns_op_mtime asc limit {$import_num}";
	    return nl_import_op::query_by_sql(\m_config::get_dc(), $query_sql);
	}
	
	public function pop_logic($message, $result_info)
	{
	    $center_import_push_logic = new \ns_model\center_import\center_import_push_logic($message, $result_info);
	    $result_op_sp =  $center_import_push_logic->get_cp_bind_sp_logic();
	    if($result_op_sp['ret'] !=0)
	    {
	        return $result_op_sp;
	    }
	    if(!isset($result_op_sp['data_info']) || strlen($result_op_sp['data_info']) <1)
	    {
	        return \m_config::return_data(1,'没匹配到任何SP下游平台');
	    }
	    $message['base_info']['nns_op_sp'] = $result_op_sp['data_info'];
	    return \m_config::return_data(0,'Ok',$message);
	}
	
}