<?php
namespace ns_model\transcode;
\ns_core\m_load::load("ns_core.nn_timer");
\ns_core\m_load::load("ns_core.m_data_model_inout_put");
/*
* 消息解释基础类，针对不同的消息
*/
class message_explain extends \ns_core\m_data_model_inout_put
{
	public $import_mode;

	/*
	* 这个方法需要各import插件，自己按不同的实现方式对message实现解释
	*/
	public function explain($message)
	{

	}

	/**
	 * 资源库栏目操作(点播、直播)
	 * @param string $action 操作行为  add 添加 | modify 修改 | delete 删除
	 * @param int $video_type 媒资类型  0 点播 | 1 直播
	 * @param array $category
	 */
	public function category_action($action,$video_type,$category)
	{
	    
	}

    /**
     * 主媒资操作（增、删、改）
     * @param string $action 操作行为  add 添加 | modify 修改 | delete 删除
     * @param array $vod 
     * array(
     *      'base_info'=>array(), //基本信息（存储于nns_vod表中）
     *      'ext_info'=>array(),//扩展信息（存储于 nns_vod_ex表中，多条记录，联合主键（nns_cp_id+nns_asset_import_id））
     * );
     */
	public function vod_action($action,$vod)
	{
        $vod = $this->check_input_params('video',$vod,$action);
        if($vod['ret'] !=0)
        {
            return $vod;
        }
        if(!isset($vod['data_info']) || empty($vod['data_info']) || !is_array($vod['data_info']))
        {
            return $this->_return_data(1,'主媒资反馈数据为空');
        }
        if(strtolower($action) == 'delete')
        {
            
        }
        
	}
	
	public function index_action($action,$index)
	{
	    $this->check_input_params('index',$index,$action);
	}

	public function media_action($action,$media)
	{
	    $this->check_input_params('media',$media,$action);
	}

	public function trans_message_action($trans)
	{
	    
	}	
}