<?php
/**
 * 中心同步指令到切片队列指令分发器
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/4/26 14:07
 */

namespace ns_model\delivery;

\ns_core\m_load::load("ns_model.m_queue_model");
\ns_core\m_load::load_old("nn_logic/vod_media/vod_media.class.php");
\ns_core\m_load::load_old("nn_logic/clip_task/clip_task.class.php");
\ns_core\m_load::load_old("nn_logic/op_queue/op_queue.class.php");
\ns_core\m_load::load_old("nn_logic/live/live_media.class.php");
\ns_core\m_load::load_old("nn_logic/file_package/file_package.class.php");
\ns_core\m_load::load_old("nn_logic/clip_task/clip_task_log.class.php");
class clip_delivery_explain
{
    public $str_sp_id = '';//SPID
    public $str_source_id = '';//CPID
    public $arr_sp_config = array();//SP配置
    public $arr_source_config = array();//CP配置

    private $clip_task_info = array();

    public function __construct($sp_id='')
    {
        //创建DC
        \m_config::set_dc();
        if(strlen($sp_id) > 0 && !empty($sp_id))
        {
            $this->str_sp_id = $sp_id;
            $arr_sp_config = \m_config::_get_sp_info($this->str_sp_id);
            $this->arr_sp_config = isset($arr_sp_config['data_info']['nns_config']) ? $arr_sp_config['data_info']['nns_config'] : array();
        }
    }

    /**
     * clip_task_delivery_explain constructor.
     * @param $clip_task_info array()
     */
    public function init($clip_task_info=array())
    {
        //赋值主要参数
        $this->clip_task_info = isset($clip_task_info) ? $clip_task_info : array();

        //SPID
        if(empty($this->str_sp_id))
        {
            if(isset($clip_task_info['nns_org_id']) && !empty($clip_task_info['nns_org_id']))
            {
                $this->str_sp_id = $clip_task_info['nns_org_id'];
            }
            $arr_sp_config = \m_config::_get_sp_info($this->str_sp_id);
            $this->arr_sp_config = isset($arr_sp_config['data_info']['nns_config']) ? $arr_sp_config['data_info']['nns_config'] : array();
        }

        //CPID
        if(isset($clip_task_info['nns_cp_id']) && !empty($clip_task_info['nns_cp_id']))
        {
            $this->str_source_id = $clip_task_info['nns_cp_id'];
        }
        $arr_source_config = \m_config::_get_cp_info($this->str_source_id);
        $this->arr_source_config = isset($arr_source_config['data_info']['nns_config']) ? $arr_source_config['data_info']['nns_config'] : array();

    }


    /**
     * 切片队列注入解释器统一入口
     */
    public function explain()
    {

        $fuc_name = $this->get_clip_type();

        if(empty($fuc_name))
        {
            return $this->write_log_return('中心同步指令队列类型参数为空,无法注入到切片队列',NS_CDS_FAIL);
        }
        return $this->save_exec_log($this->$fuc_name());
    }

    /**
     * 根据参数记录日志，并返回
     * @param $msg //文字消息
     * @param int $code //状态码，默认成功0
     * @return array
     */
    public function write_log_return($msg, $code=NS_CDS_SUCCE)
    {
        //SPID、C2有值，则存切片队列日志 clip_execute_log
        if(!empty($this->str_sp_id) && !empty($this->clip_task_info) && !empty($this->get_clip_task_info('nns_op_id')))
        {
            $ret = \m_config::write_clip_execute_log($msg, $this->str_sp_id, $this->get_clip_task_info('nns_op_id'));
        }
        else //存入公共日志  global_log
        {
            $ret = \m_config::write_global_execute_log(NS_CDS_CLIP_QUEUE, $this->str_source_id,$this->str_sp_id, $this->get_clip_task_info('action'), $this->get_clip_task_info(), var_export($msg,true));
        }

        return \m_config::return_data($code, $msg, $ret['data_info']);
    }

    /**
     * 保存C2执行过程日志
     * @param $return_info
     * @return mixe
     */
    public function save_exec_log($return_info)
    {
        if($this->get_clip_task_info('id'))
        {
            if (isset($return_info['data_info']['base_dir']) && !empty($return_info['data_info']['base_dir']))
            {
                $exec_log = array("nns_queue_execute_url" => $return_info['data_info']['base_dir']);
                //修改C2任务
                \nl_c2_task::edit($this->get_dc(), $exec_log, $this->get_clip_task_info('id'));
            }
            elseif (isset($return_info['error_data_info']['base_dir']) && !empty($return_info['error_data_info']['base_dir']))
            {
                $exec_log = array("nns_queue_execute_url" => $return_info['error_data_info']['base_dir']);
                //修改C2任务
                \nl_c2_task::edit($this->get_dc(), $exec_log, $this->get_clip_task_info('id'));
            }
        }
        return $return_info;
    }

    /**
     * 获取切片队列内容
     * @param string $key
     * @return string
     */
    public function get_clip_task_info($key='')
    {
        if(empty($key))
        {
            return '';
        }
        if (strpos($key, 'nns_') === FALSE)
        {
            $key = 'nns_' . $key;
        }
        if(!empty($this->clip_task_info) && isset($this->clip_task_info[$key]))
        {
            return $this->clip_task_info[$key];
        }
        return '';
    }

    /**
     * 获取中心同步指令任务类型
     * @return string
     */
    public function get_clip_type()
    {
        if(!empty($this->clip_task_info) && isset($this->clip_task_info['nns_video_type']))
        {
            $video_type = '';
            switch ($this->clip_task_info['nns_video_type'])
            {
                case 0:
                    $video_type= NS_CDS_MEDIA;
                    break;
                case 1:
                    $video_type= NS_CDS_LIVE_MEDIA;
                    break;
                case 2:
                    $video_type= NS_CDS_FILE;
                    break;
            }
            return $video_type;
        }
        return '';
    }

    /**
     * 获取DC
     * @return Object
     */
    public function get_dc()
    {
        return \m_config::get_dc();
    }

/********************************START********中心同步指令到切片队列的注入执行*********START**********************************/
    /**
     * 开始执行点播片源下发
     * @return array
     */
    public function media()
    {
        $clip_task_info = $this->clip_task_info;

        //查询是否在直播片源
        $media_arr = $this->get_media_info($clip_task_info);

        if ($media_arr['ret'] != 0)
        {
            return $this->write_log_return('点播片源查询失败' . $media_arr['reason'], NS_CDS_FAIL);
        }
        $media_info = $media_arr['data_info'][0];
        if (empty($media_info) || !is_array($media_info) || !isset($media_info))
        {
            return $this->write_log_return('点播片源不存在', NS_CDS_FAIL);
        }

        //获取切片队列是否已经存在这个切片的队列，存在则取消之前的对列
        $exists_task_info = $this->get_exists_task_info($clip_task_info);

        if ($exists_task_info['ret'] == 0 && !empty($exists_task_info['data_info']) && is_array($exists_task_info['data_info']))
        {
            foreach ($exists_task_info['data_info'] as $item)
            {
                $sql = "update nns_mgtvbk_clip_task set  nns_state='".NS_CDS_CANCEL."',nns_desc='任务已取消'  where nns_id='".$item['nns_id']."'";
                \nl_clip_task::query_by_sql($this->get_dc(), $sql);
                $task_log = array(
                    'nns_task_id' => $item['nns_id'],
                    'nns_video_type' => $clip_task_info['nns_video_type'],
                    'nns_state' => NS_CDS_CANCEL,
                    'nns_desc' => '任务已取消',
                    'nns_video_id' => $clip_task_info['nns_video_id'],
                    'nns_video_index_id' => $clip_task_info['nns_video_index_id']
                );
                $this->build_clip_log($task_log);
            }
        }

        $clip_task_info['nns_create_time'] = date('Y-m-d H:i:s');
        $clip_task_info['nns_priority'] = isset($clip_task_info['nns_priority']) ? (int)$clip_task_info['nns_priority'] : 0;
        unset($clip_task_info['nns_index']);

        $clip_task_info['nns_id'] = np_guid_rand();
        //添加内容到DRM
        //查询DRM 是否关闭
        if(isset($this->arr_sp_config['main_drm_enabled']) && intval($this->arr_sp_config['main_drm_enabled']) == 1 && $this->arr_sp_config['nns_video_type'] == 0)
        {
            //检查是否属于重切
            if($exists_task_info['ret'] == 0 && !empty($exists_task_info['data_info']) && is_array($exists_task_info['data_info']))
            {
                $this->write_log_return('重新新切片片源不进行DRM', NS_CDS_SUCCE);
            }
            else
            {
                $drm_params = array(
                    'nns_media_id' => $media_arr['nns_import_id'],
                    'nns_description' => 'IMPORT_MEDIA'
                );
                $drm_result = \nl_m_media::add_media(\m_config::get_dc()->db(), $drm_params);
                $this->write_log_return('重新新切片片源不进行DMR,DRM注入:' . var_export($drm_result, true), NS_CDS_FAIL);
                if($drm_result['code'] != 'ML_STATE_OK')
                {
                    $task_log = array(
                        'nns_task_id' => $clip_task_info['nns_id'],
                        'nns_video_type' => $clip_task_info['nns_video_type'],
                        'nns_state'   => NS_CDS_DRM_FAIL,
                        'nns_desc'	=> 'DRM加密失败',
                        'nns_video_id'=>$clip_task_info['nns_video_id'],
                        'nns_video_index_id'=>$clip_task_info['nns_video_index_id']
                    );
                    $this->build_clip_log($task_log);
                    return $this->write_log_return('DMR加密失败', NS_CDS_FAIL);
                }
            }
        }

        $add_result = \nl_clip_task::add($this->get_dc(), $clip_task_info);
        if ($add_result['ret'] != 0)
        {
            $task_log = array(
                'nns_task_id'=>$clip_task_info['nns_id'],
                'nns_state' => 1,
                'nns_desc'  => '添加切片队列失败'. $add_result['reason'],
                'nns_video_type' => $clip_task_info['nns_video_type'],
                'nns_video_id'=>$clip_task_info['nns_video_id'],
                'nns_video_index_id'=>$clip_task_info['nns_video_index_id']
            );
            $this->build_clip_log($task_log);
            return $this->write_log_return('添加切片队列失败'. $add_result['reason'], NS_CDS_FAIL);
        }

        $task_log = array(
            'nns_task_id'=>$clip_task_info['nns_id'],
            'nns_state' => NS_CDS_ADD,
            'nns_desc'  => '添加切片队列成功',
            'nns_video_type' => $clip_task_info['nns_video_type'],
            'nns_video_id'=>$clip_task_info['nns_video_id'],
            'nns_video_index_id'=>$clip_task_info['nns_video_index_id']
        );
        $this->build_clip_log($task_log);
        $update_op = $this->task_progress($clip_task_info['nns_op_id']);
        if ($update_op['ret'] == 0)
        {
            return $this->write_log_return('执行中心同步指令注入到切片队列成功', NS_CDS_SUCCE);
        }
        else
        {
            return $this->write_log_return('执行中心同步指令注入到切片队列失败', NS_CDS_FAIL);
        }
    }

    /**
     * 开始执行文件下发
     * @return array
     */
    public function file()
    {
        $clip_task_info = $this->clip_task_info;

        //查询是否在直播片源
        $live_media_arr = $this->get_file_info($clip_task_info);
        if ($live_media_arr['ret'] != 0)
        {
            return $this->write_log_return('直播片源查询失败' . $live_media_arr['reason'], NS_CDS_FAIL);
        }
        $live_media_info = $live_media_arr['data_info'][0];
        if (empty($live_media_info) || !is_array($live_media_info) || !isset($live_media_info))
        {
            return $this->write_log_return('直播片源不存在', NS_CDS_FAIL);
        }

        //获取片源cp配置
        $cp_config = \m_config::_get_cp_info($clip_task_info['nns_cp_id']);

        //获取切片队列是否已经存在这个切片的队列，存在则取消之前的对列
        $exists_task_info = $this->get_exists_task_info($clip_task_info);
        if ($exists_task_info['ret'] == 0 && !empty($exists_task_info['data_info']) && is_array($exists_task_info['data_info']))
        {
            foreach ($exists_task_info['data_info'] as $item)
            {
                $sql = "update nns_mgtvbk_clip_task set  nns_state='".NS_CDS_CANCEL."',nns_desc='任务已取消'  where nns_id='".$item['nns_id']."'";
                \nl_clip_task::query_by_sql($this->get_dc(), $sql);
                $task_log = array(
                    'nns_task_id' => $item['nns_id'],
                    'nns_video_type' => $clip_task_info['nns_video_type'],
                    'nns_state' => NS_CDS_CANCEL,
                    'nns_desc' => '任务已取消',
                    'nns_video_id' => $clip_task_info['nns_video_id'],
                    'nns_video_index_id' => $clip_task_info['nns_video_index_id']
                );
                $this->build_clip_log($task_log);
            }
        }

        $clip_task_info['nns_create_time'] = date('Y-m-d H:i:s');
        $clip_task_info['nns_priority'] = isset($clip_task_info['nns_priority']) ? (int)$clip_task_info['nns_priority'] : 0;
        unset($clip_task_info['nns_index']);

        $clip_task_info['nns_id'] = np_guid_rand();
        //添加内容到DRM
        //查询DRM 是否关闭
        if(isset($this->arr_sp_config['main_drm_enabled']) && intval($this->arr_sp_config['main_drm_enabled']) == 1 && $this->arr_sp_config['nns_video_type'] == 0)
        {
            //检查是否属于重切
            if($exists_task_info['ret'] == 0 && !empty($exists_task_info['data_info']) && is_array($exists_task_info['data_info']))
            {
                $this->write_log_return('重新新切片片源不进行DRM', NS_CDS_FAIL);
            }
            else
            {
                $drm_params = array(
                    'nns_media_id' => $live_media_arr['nns_import_id'],
                    'nns_description' => 'IMPORT_MEDIA'
                );
                $drm_result = \nl_m_media::add_media(\m_config::get_dc()->db(), $drm_params);
                $this->write_log_return('重新新切片片源不进行DMRDRM注入:' . var_export($drm_result, true), NS_CDS_FAIL);
                if($drm_result['code'] != 'ML_STATE_OK')
                {
                    $task_log = array(
                        'nns_task_id' => $clip_task_info['nns_id'],
                        'nns_video_type' => $clip_task_info['nns_video_type'],
                        'nns_state'   => NS_CDS_DRM_FAIL,
                        'nns_desc'	=> 'DRM加密失败',
                        'nns_video_id'=>$clip_task_info['nns_video_id'],
                        'nns_video_index_id'=>$clip_task_info['nns_video_index_id']
                    );
                    $this->build_clip_log($task_log);
                    return $this->write_log_return('DMR加密失败', NS_CDS_FAIL);
                }
            }
        }
        //直播转点播
        if(isset($cp_config['cp_playbill_to_media_enabled']) && $cp_config['cp_playbill_to_media_enabled'] == '1' && isset($cp_config['source_notify_url']) && strlen($cp_config['source_notify_url']) >0)
        {
            $clip_task_info['nns_state'] = NS_CDS_LIVE_TO_MEDIA_WAIT;

            $task_log = array(
                'nns_task_id'=>$clip_task_info['nns_id'],
                'nns_state' => NS_CDS_LIVE_TO_MEDIA_WAIT,
                'nns_desc'  => '等待直播转点播',
                'nns_video_type' => $clip_task_info['nns_video_type'],
                'nns_video_id'=>$clip_task_info['nns_video_id'],
                'nns_video_index_id'=>$clip_task_info['nns_video_index_id']
            );
            $this->build_clip_log($task_log);
        }

        $add_result = \nl_clip_task::add($this->get_dc(), $clip_task_info);
        if ($add_result['ret'] != 0)
        {
            $task_log = array(
                'nns_task_id'=>$clip_task_info['nns_id'],
                'nns_state' => 1,
                'nns_desc'  => '添加切片队列失败'. $add_result['reason'],
                'nns_video_type' => $clip_task_info['nns_video_type'],
                'nns_video_id'=>$clip_task_info['nns_video_id'],
                'nns_video_index_id'=>$clip_task_info['nns_video_index_id']
            );
            $this->build_clip_log($task_log);
            return $this->write_log_return('添加切片队列失败'. $add_result['reason'], NS_CDS_FAIL);
        }

        $task_log = array(
            'nns_task_id'=>$clip_task_info['nns_id'],
            'nns_state' => NS_CDS_ADD,
            'nns_desc'  => '添加切片队列成功',
            'nns_video_type' => $clip_task_info['nns_video_type'],
            'nns_video_id'=>$clip_task_info['nns_video_id'],
            'nns_video_index_id'=>$clip_task_info['nns_video_index_id']
        );
        $this->build_clip_log($task_log);
        $update_op = $this->task_progress($clip_task_info['nns_op_id']);
        if ($update_op['ret'] == 0)
        {
            return $this->write_log_return('执行中心同步指令注入到切片队列成功', NS_CDS_SUCCE);
        }
        else
        {
            return \m_config::return_data('执行中心同步指令注入到切片队列失败', NS_CDS_FAIL);
        }
    }

    /**
     * 执行直播片源下发
     * @return array
     */
    public function live_media()
    {
        $clip_task_info = $this->clip_task_info;

        //查询是否在直播片源
        $live_media_arr = $this->get_live_media_info($clip_task_info);
        if ($live_media_arr['ret'] != 0)
        {
            return $this->write_log_return('直播片源查询失败' . $live_media_arr['reason'], NS_CDS_FAIL);
        }
        $live_media_info = $live_media_arr['data_info'][0];
        if (empty($live_media_info) || !is_array($live_media_info) || !isset($live_media_info))
        {
            return $this->write_log_return('直播片源不存在', NS_CDS_FAIL);
        }

        //获取片源cp配置
        $cp_config = \m_config::_get_cp_info($clip_task_info['nns_cp_id']);

        //获取切片队列是否已经存在这个切片的队列，存在则取消之前的对列
        $exists_task_info = $this->get_exists_task_info($clip_task_info);
        if ($exists_task_info['ret'] == 0 && !empty($exists_task_info['data_info']) && is_array($exists_task_info['data_info']))
        {
            foreach ($exists_task_info['data_info'] as $item)
            {
                $sql = "update nns_mgtvbk_clip_task set  nns_state='".NS_CDS_CANCEL."',nns_desc='任务已取消'  where nns_id='".$item['nns_id']."'";
                \nl_clip_task::query_by_sql($this->get_dc(), $sql);
                $task_log = array(
                    'nns_task_id' => $item['nns_id'],
                    'nns_video_type' => $clip_task_info['nns_video_type'],
                    'nns_state' => NS_CDS_CANCEL,
                    'nns_desc' => '任务已取消',
                    'nns_video_id' => $clip_task_info['nns_video_id'],
                    'nns_video_index_id' => $clip_task_info['nns_video_index_id']
                );
                $this->build_clip_log($task_log);
            }
        }

        $clip_task_info['nns_create_time'] = date('Y-m-d H:i:s');
        $clip_task_info['nns_priority'] = isset($clip_task_info['nns_priority']) ? (int)$clip_task_info['nns_priority'] : 0;
        unset($clip_task_info['nns_index']);

        $clip_task_info['nns_id'] = np_guid_rand();
        //添加内容到DRM
        //查询DRM 是否关闭
        if(isset($this->arr_sp_config['main_drm_enabled']) && intval($this->arr_sp_config['main_drm_enabled']) == 1 && $this->arr_sp_config['nns_video_type'] == 0)
        {
            //检查是否属于重切
            if($exists_task_info['ret'] == 0 && !empty($exists_task_info['data_info']) && is_array($exists_task_info['data_info']))
            {
                $this->write_log_return('重新新切片片源不进行DRM', NS_CDS_FAIL);
            }
            else
            {
                $drm_params = array(
                    'nns_media_id' => $live_media_arr['nns_import_id'],
                    'nns_description' => 'IMPORT_MEDIA'
                );
                $drm_result = \nl_m_media::add_media(\m_config::get_dc()->db(), $drm_params);
                $this->write_log_return('重新新切片片源不进行DMRDRM注入:' . var_export($drm_result, true), NS_CDS_FAIL);
                if($drm_result['code'] != 'ML_STATE_OK')
                {
                    $task_log = array(
                        'nns_task_id' => $clip_task_info['nns_id'],
                        'nns_video_type' => $clip_task_info['nns_video_type'],
                        'nns_state'   => NS_CDS_DRM_FAIL,
                        'nns_desc'	=> 'DRM加密失败',
                        'nns_video_id'=>$clip_task_info['nns_video_id'],
                        'nns_video_index_id'=>$clip_task_info['nns_video_index_id']
                    );
                    $this->build_clip_log($task_log);
                    return $this->write_log_return('DMR加密失败', NS_CDS_FAIL);
                }
            }
        }
        //直播转点播
        if(isset($cp_config['cp_playbill_to_media_enabled']) && $cp_config['cp_playbill_to_media_enabled'] == '1' && isset($cp_config['source_notify_url']) && strlen($cp_config['source_notify_url']) >0)
        {
            $clip_task_info['nns_state'] = NS_CDS_LIVE_TO_MEDIA_WAIT;

            $task_log = array(
                'nns_task_id'=>$clip_task_info['nns_id'],
                'nns_state' => NS_CDS_LIVE_TO_MEDIA_WAIT,
                'nns_desc'  => '等待直播转点播',
                'nns_video_type' => $clip_task_info['nns_video_type'],
                'nns_video_id'=>$clip_task_info['nns_video_id'],
                'nns_video_index_id'=>$clip_task_info['nns_video_index_id']
            );
            $this->build_clip_log($task_log);
        }

        $add_result = \nl_clip_task::add($this->get_dc(), $clip_task_info);
        if ($add_result['ret'] != 0)
        {
            $task_log = array(
                'nns_task_id'=>$clip_task_info['nns_id'],
                'nns_state' => 1,
                'nns_desc'  => '添加切片队列失败'. $add_result['reason'],
                'nns_video_type' => $clip_task_info['nns_video_type'],
                'nns_video_id'=>$clip_task_info['nns_video_id'],
                'nns_video_index_id'=>$clip_task_info['nns_video_index_id']
            );
            $this->build_clip_log($task_log);
            return $this->write_log_return('添加切片队列失败'. $add_result['reason'], NS_CDS_FAIL);
        }

        $task_log = array(
            'nns_task_id'=>$clip_task_info['nns_id'],
            'nns_state' => NS_CDS_ADD,
            'nns_desc'  => '添加切片队列成功',
            'nns_video_type' => $clip_task_info['nns_video_type'],
            'nns_video_id'=>$clip_task_info['nns_video_id'],
            'nns_video_index_id'=>$clip_task_info['nns_video_index_id']
        );
        $this->build_clip_log($task_log);
        $update_op = $this->task_progress($clip_task_info['nns_op_id']);
        if ($update_op['ret'] == 0)
        {
            return $this->write_log_return('执行中心同步指令注入到切片队列成功', NS_CDS_SUCCE);
        }
        else
        {
            return $this->write_log_return('执行中心同步指令注入到切片队列失败', NS_CDS_FAIL);
        }
    }

    /**
     * 查询是否存在该切片队列
     * @param $params
     * @return array
     */
    public function get_exists_task_info($params)
    {
        $sql = "select * from nns_mgtvbk_clip_task " .
            "where nns_org_id='".$params['nns_org_id']."'" .
            " and   nns_video_type='".$params['nns_video_type']."'" .
            " and nns_video_id='".$params['nns_video_id']."' " .
            " and nns_video_index_id='".$params['nns_video_index_id']."'" .
            " and nns_video_media_id='".$params['nns_video_media_id']."'" .
            "  and nns_state!='".NS_CDS_CANCEL."'";
        return \nl_clip_task::query_by_sql($this->get_dc(), $sql);
    }

    /**
     * 注入到切片队列后更新中心同步指令
     * @param $op_queue_id
     * @return array
     */
    public function task_progress($op_queue_id)
    {
        $update_params = array(
            'nns_status' => NS_CDS_OP_CLIP_LOADING,
        );
        $re_execute = \nl_op_queue::edit($this->get_dc(), $update_params, $op_queue_id);
        if ($re_execute['ret'] == 0)
        {
            return $this->write_log_return('更新中心同步指令队列成功', NS_CDS_SUCCE);
        }
        else
        {
            return $this->write_log_return('更新中心同步指令队列失败', NS_CDS_FAIL);
        }
    }

    /**
     * 根据条件获取直播片源信息
     * @param $params array
     * @return array
     */
    public function get_live_media_info($params)
    {
        \ns_core\m_load::load_old("nn_logic.live.live_media.class.php");
        $sql = "select * from nns_live_media" .
            " where nns_id='{$params['nns_video_media_id']}'" .
            " and  nns_deleted != 1 and  nns_vod_id='{$params['nns_video_id']}'" .
            " and nns_vod_index_id='{$params['nns_video_index_id']}'";
        return \nl_live_media::query_by_sql($this->get_dc(), $sql);
    }

    /**
     * 获取片源数据
     * @param $params
     * @return array
     */
    public function get_media_info($params)
    {
        $sql = "select * from nns_vod_media" .
            " where nns_id='{$params['nns_video_media_id']}'" .
            " and  nns_deleted != 1 and  nns_vod_id='{$params['nns_video_id']}'" .
            " and nns_vod_index_id='{$params['nns_video_index_id']}'";
        return \nl_vod_media_v2::query_by_sql($this->get_dc(), $sql);
    }

    /**
     * 获取文件包信息
     * @param $params
     * @return array
     */
    public function get_file_info($params)
    {
        $sql = "select * from nns_file_package" .
            " where nns_id='{$params['nns_video_id']}'" .
            " and  nns_deleted != 1 ";
        return \nl_file_package::query_by_sql($this->get_dc(), $sql);
    }


    /**
     * 变量初始化
     */
    public function destroy_init()
    {
        $this->str_sp_id = '';//SPID
        $this->str_source_id = '';//CPID
        $this->arr_sp_config = array();//SP配置
        $this->arr_source_config = array();//CP配置

        if(isset($this->clip_task_info))
        {
            $this->clip_task_info = array();//中心同步指令队列注入到切片队列数据  写一个key=>value获取
        }
    }


    public function build_clip_log($clip_log_info)
    {
        if(!isset($clip_log_info['nns_id']) || empty($clip_log_info['nns_id']))
        {
            $clip_log_info['nns_id'] = np_guid_rand();
        }
        $task_log['nns_video_type'] = $clip_log_info['nns_video_type'];
        $clip_log_info['nns_create_time'] = date('Y-m-d H:i:s');
        $clip_log_info['nns_create_mic_time'] = microtime(true)*10000;
        return \nl_clip_task_log::add($this->get_dc(), $clip_log_info);
    }

    public function push_message($sp_id,$message)
    {

    }

    public function pop_message($sp_id,$num = 1)
    {

    }


}
