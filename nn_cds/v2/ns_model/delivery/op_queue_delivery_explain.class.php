<?php
/**
 * 中心同步指令分发器
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/4/26 14:07
 */

namespace ns_model\delivery;
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_model.m_queue_model");
\ns_core\m_load::load("ns_model.center_op.center_op_queue");
class op_queue_delivery_explain
{
    public $str_sp_id = '';//SPID
    public $str_source_id = '';//CPID
    public $arr_sp_config = array();//SP配置
    public $arr_source_config = array();//CP配置

    protected $video_info = array();//主媒资数据
    protected $index_info = array();//分集数据
    protected $seekpoint_info = array();//分集打点数据
    protected $media_info = array();//片源数据
    private $live_info = array();//直播
    private $live_index_info = array();//直播分集
    private $live_media_info = array();//直播源
    private $file_info = array();//文件包
    private $package_info = array();//产品打包
    private $playbill_info = array();//节目单
    private $epg_file_info = array();//EPGFile

    private $op_queue_info = array();//中心同步指令队列信息
    private $center_op = null;
    public function __construct($sp_id='')
    {
        //创建DC
        \m_config::set_dc();
        if(strlen($sp_id) > 0 && !empty($sp_id))
        {
            $this->str_sp_id = $sp_id;
            $arr_sp_config = \m_config::_get_sp_info($this->str_sp_id);
            $this->arr_sp_config = isset($arr_sp_config['data_info']['nns_config']) ? $arr_sp_config['data_info']['nns_config'] : array();
        }
    }

    /**
     * op_queue_delivery_explain constructor.
     * @param $op_queue_info array(
     *                      'op_queue_info'=>array(中心同步指令队列信息),
     *                      'video_info'=>array(
     *                          'base_info' => VIDEO信息
     *                          'ex_info'  => 扩展信息
     *                       ),
     *                      'index_info'=>array(
     *                          'base_info' => INDEX信息,
     *                          'ex_info' => 扩展信息,
     *                      ),
     *                      'seekpoint_info'=>array(
     *                          'base_info' => 分集打点信息,
     *                          'ex_info' => 扩展信息,
     *                      ),
     *                      'media_info'=>array(
     *                          'base_info' => MEDIA信息
     *                          'ex_info' => 扩展信息
     *                      ),
     *                      'live_info'=>array(
     *                          'base_info' => MEDIA信息
     *                          'ex_info' => 扩展信息
     *                      ),
     *                      'live_index_info'=>array(
     *                          'base_info' => MEDIA信息
     *                          'ex_info' => 扩展信息
     *                      ),
     *                  )
     */
    public function init($op_queue_info=array())
    {
        $this->video_info = isset($op_queue_info['video_info']) ? $op_queue_info['video_info'] : array();
        $this->index_info = isset($op_queue_info['index_info']) ? $op_queue_info['index_info'] : array();
        $this->media_info = isset($op_queue_info['media_info']) ? $op_queue_info['media_info'] : array();
        $this->seekpoint_info = isset($op_queue_info['seekpoint_info']) ? $op_queue_info['seekpoint_info'] : array();
        $this->live_info = isset($op_queue_info['live_info']) ? $op_queue_info['live_info'] : array();
        $this->live_index_info = isset($op_queue_info['live_index_info']) ? $op_queue_info['live_index_info'] : array();
        $this->live_media_info = isset($op_queue_info['live_media_info']) ? $op_queue_info['live_media_info'] : array();
        $this->file_info = isset($op_queue_info['file_info']) ? $op_queue_info['file_info'] : array();
        $this->package_info = isset($op_queue_info['package_info']) ? $op_queue_info['package_info'] : array();
        $this->playbill_info = isset($op_queue_info['playbill_info']) ? $op_queue_info['playbill_info'] : array();
        $this->epg_file_info = isset($op_queue_info['epg_file_info']) ? $op_queue_info['epg_file_info'] : array();

        //赋值主要参数
        $this->op_queue_info = isset($op_queue_info['op_queue_info']) ? $op_queue_info['op_queue_info'] : array();

        //SPID
        if(empty($this->str_sp_id))
        {
            if(isset($op_queue_info['op_queue_info']) && !empty($op_queue_info['op_queue_info']['nns_org_id']))
            {
                $this->str_sp_id = $op_queue_info['op_queue_info']['nns_org_id'];
            }
            $arr_sp_config = \m_config::_get_sp_info($this->str_sp_id);
            $this->arr_sp_config = isset($arr_sp_config['data_info']['nns_config']) ? $arr_sp_config['data_info']['nns_config'] : array();
        }

        //CPID
        if(isset($this->op_queue_info['op_queue_info']) && !empty($this->op_queue_info['op_queue_info']['nns_cp_id']))
        {
            $this->str_source_id = $this->op_queue_info['op_queue_info']['nns_cp_id'];
        }
        $arr_source_config = \m_config::_get_cp_info($this->str_source_id);
        $this->arr_source_config = isset($arr_source_config['data_info']['nns_config']) ? $arr_source_config['data_info']['nns_config'] : array();

        $this->center_op = new \ns_model\center_op\center_op_queue($this->str_sp_id, $this->op_queue_info['nns_type']);
    }

    /**
     * 获取主媒资数据
     * @return array()
     */
    public function get_video_info()
    {
        return $this->video_info;
    }
    /**
     * 获取分集数据
     * @return array()
     */
    public function get_index_info()
    {
        return $this->index_info;
    }
    /**
     * 获取片源数据
     * @return array()
     */
    public function get_media_info()
    {
        return $this->media_info;
    }
    /**
     * 获取打点信息数据
     * @return array()
     */
    public function get_seekpoint_info()
    {
        return $this->seekpoint_info;
    }
    /**
     * 获取频道信息
     * @return array()
     */
    public function get_live_info()
    {
        return $this->live_info;
    }
    /**
     * 获取频道分集信息
     * @return array()
     */
    public function get_live_index_info()
    {
        return $this->live_index_info;
    }
    /**
     * 获取频道分集片源信息
     * @return array()
     */
    public function get_live_media_info()
    {
        return $this->live_media_info;
    }
    /**
     * 获取文件包信息
     * @return array()
     */
    public function get_file_info()
    {
        return $this->file_info;
    }
    /**
     * 获取产品包信息
     * @return array()
     */
    public function get_package_info()
    {
        return $this->package_info;
    }
    /**
     * 获取节目单信息
     * @return array()
     */
    public function get_playbill_info()
    {
        return $this->playbill_info;
    }

    /**
     * 获取EPGFile信息
     * @return array()
     */
    public function get_epg_file_info()
    {
        return $this->epg_file_info;
    }


    /**
     * EPG注入解释器统一入口
     */
    public function explain()
    {
        $fuc_name = $this->get_op_queue_type();
        if(empty($fuc_name))
        {
            return $this->write_log_return('中心同步指令队列类型参数为空,无法注入到C2队列', NS_CDS_FAIL);
        }
        return $this->save_exec_log($this->$fuc_name());
    }

    /**
     * 根据参数记录日志，并返回
     * @param $msg //文字消息
     * @param int $code //状态码，默认成功0
     * @return array
     */
    public function write_log_return($msg, $code=NS_CDS_SUCCE)
    {
        //SPID、C2有值，则存CDN队列日志 queue_log
        if(!empty($this->str_sp_id) && !empty($this->op_queue_info))
        {
            $ret = \m_config::write_op_queue_execute_log($msg, $this->str_sp_id, $this->get_op_queue_info('id'));
        }
        else //存入公共日志  global_log
        {
            $ret = \m_config::write_global_execute_log(NS_CDS_CENTER_OP_QUEUE, $this->str_source_id, $this->str_sp_id, $this->get_op_queue_info('action'), $this->get_op_queue_type(), var_export($msg,true));
        }

        return \m_config::return_data($code, $msg, $ret['data_info']);
    }
    /**
     * 保存C2执行过程日志
     * @param $return_info
     * @return mixe
     */
    public function save_exec_log($return_info)
    {
        if($this->get_op_queue_info('id'))
        {
            if (isset($return_info['data_info']['base_dir']) && !empty($return_info['data_info']['base_dir']))
            {
                $exec_log = array("nns_queue_execute_url" => $return_info['data_info']['base_dir']);
                //修改C2任务
                \nl_c2_task::edit($this->get_dc(), $exec_log, $this->get_op_queue_info('id'));
            }
            elseif (isset($return_info['error_data_info']['base_dir']) && !empty($return_info['error_data_info']['base_dir']))
            {
                $exec_log = array("nns_queue_execute_url" => $return_info['error_data_info']['base_dir']);
                //修改C2任务
                \nl_c2_task::edit($this->get_dc(), $exec_log, $this->get_op_queue_info('id'));
            }
        }
        return $return_info;
    }

    /**
     * 获取C2内容
     * @param string $key
     * @return string
     */
    public function get_op_queue_info($key='')
    {
        if(empty($key))
        {
            return '';
        }
        if (strpos($key, 'nns_') === FALSE)
        {
            $key = 'nns_' . $key;
        }
        if(!empty($this->op_queue_info) && isset($this->op_queue_info[$key]))
        {
            return $this->op_queue_info[$key];
        }
        return '';
    }

    /**
     * 获取中心同步指令任务类型
     * @return string
     */
    public function get_op_queue_type()
    {
        if(!empty($this->op_queue_info) && isset($this->op_queue_info['nns_type']))
        {
            return $this->op_queue_info['nns_type'];
        }
        return '';
    }

    /**
     * 获取DC
     * @return Object
     */
    public function get_dc()
    {
        return \m_config::get_dc();
    }

/********************************START********中心同步指令到c2队列的注入执行*********START**********************************/
    /**
     * 开始执行分集下发
     * @return array
     */
    public function video()
    {
        $video_info = $this->get_video_info();
        $video_type = $this->get_op_queue_type();
        $op_queue_info = $this->op_queue_info;

        //查询是否在C2队列中是否已经存在此任务
        $c2_task_arr = $this->get_c2_info($video_type, $video_info['base_info']['nns_id'], $op_queue_info['nns_org_id']);
        $c2_task_info = $c2_task_arr['data_info'][0];
        if (empty($c2_task_info) || !is_array($c2_task_info))
        {
            //如果注入进来的删除命令，而c2中又没有这个记录  则忽略
            if ((int)$this->arr_sp_config['virtual_destroy_task'] === 0 && $op_queue_info['nns_action'] == NS_CDS_DELETE)
            {
                $re = $this->center_op->report_op_task($op_queue_info['nns_id']);
                return $this->write_log_return($re['reason'], $re['ret']);
            }
        }
        //是否为最终状态
        $is_finally = $this->center_op->is_finally_status($c2_task_info);

        if ($is_finally['ret'] != 0)
        {
            //如果存在另外一个状态不为最终状态的，中心同步指令下发的队列
            if (!empty($c2_task_info) && $op_queue_info['nns_id'] != $c2_task_info['nns_op_id'])
            {
                //如果存在分组标示 上面的数据要一直存在队列中 等待下面的数据处理完毕才下发下去
                if ($op_queue_info['nns_is_group'] != 0)
                {
                    $temp_millisecond = round(np_millisecond_f() * 1000);
                    $op_execute_sql = "update nns_mgtvbk_op_queue set nns_op_mtime='{$temp_millisecond}' where nns_id='{$op_queue_info['nns_id']}'";
                    \nl_op_queue::query_by_sql($this->get_dc(), $op_execute_sql);
                    return $this->write_log_return('存在分组标示 上面的数据要一直存在队列中 等待下面的数据处理完毕才下发下去', NS_CDS_SUCCE);
                }
                $set = "";
                //存在任意一状态为终态，修改状态为等待注入
                if ($c2_task_info['nns_status'] == 7 || $c2_task_info['nns_epg_status'] == 100)
                {
                    $set = ",nns_status=1,nns_epg_status=97";
                }
                $op_execute_sql = "update nns_mgtvbk_c2_task set nns_cp_id='{$op_queue_info['nns_cp_id']}',nns_message_id='{$op_queue_info['nns_message_id']}',nns_name='{$op_queue_info['nns_name']}', nns_op_id='{$op_queue_info['nns_id']}' $set where nns_id='{$c2_task_info['nns_id']}'";
                \nl_op_queue::query_by_sql($this->get_dc(), $op_execute_sql);
                //将上一个中心同步指令队列删除
                $this->center_op->report_op_task($c2_task_info['nns_op_id']);
                $c2_task_info['nns_op_id'] = $op_queue_info['nns_id'];
            }
        }

        //如果存在任务就修改它的动作
        if (is_array($c2_task_info) && !empty($c2_task_info))
        {
            //如果列队过来的是添加动作，而C2任务中的是添加或则修改，则将c2设置为修改
            if (($c2_task_info['nns_action'] == NS_CDS_MODIFY || $c2_task_info['nns_action'] == NS_CDS_ADD)
                && $op_queue_info['nns_action'] == NS_CDS_ADD && ((int)$c2_task_info['nns_status'] != 7
                    || (int)$c2_task_info['nns_epg_status'] != 100))
            {
                if ($is_finally['ret'] == 0)
                {
                    $op_queue_info['nns_action'] = NS_CDS_MODIFY;
                } else
                {
                    $op_queue_info['nns_action'] = NS_CDS_ADD;
                }
            }
            //如果c2中本身是删除，而任务列队中同步的操作是添加或则修改，则按照新增处理
            if ($c2_task_info['nns_action'] == NS_CDS_DELETE && ($op_queue_info['nns_action'] == NS_CDS_MODIFY || $op_queue_info['nns_action'] == NS_CDS_ADD))
            {
                $op_queue_info['nns_action'] = NS_CDS_ADD;
            }

            //如果中心同步指令队列的操作是删除，则将它的所有子级正在执行的状态设置为cancel
            if ($op_queue_info['nns_action'] == NS_CDS_DELETE)
            {
                $this->set_child_cancel($op_queue_info['nns_type'], $c2_task_info['nns_ref_id']);
            }
            //修改状态和动作
            $update_params = array(
                'nns_epg_fail_time' => 0,
                'nns_cdn_fail_time' => 0,
                'nns_cp_id' => $op_queue_info['nns_cp_id'],
                'nns_is_group' => $op_queue_info['nns_is_group'],
                'nns_weight' => $op_queue_info['nns_weight'],
                'nns_message_id' => $op_queue_info['nns_message_id'],
                'nns_status' => NS_CDS_CDN_WAIT,
                'nns_action' => $op_queue_info['nns_action'],
                'nns_modify_time' => date('Y-m-d H:i:s'),
                'nns_op_id' => $op_queue_info['nns_id'],
                'nns_epg_status' => NS_CDS_EPG_WAIT,
                'nns_name' => $op_queue_info['nns_name'],
            );
            $result = \nl_c2_task::edit($this->get_dc(), $update_params, $c2_task_info['nns_id']);
        }
        //如果注入进来的删除命令，而且c2中又没有这个记录，并且中心同步删除指令生成c2虚拟任务开关打开，则生成虚拟c2任务
        else if (empty($c2_task_info) && !is_array($c2_task_info) && $op_queue_info['nns_action'] == NS_CDS_DELETE
            && (int)$this->arr_sp_config['virtual_destroy_task'] === 1)
        {
            $id = np_guid_rand($this->str_sp_id);
            $insert_params = array(
                'nns_id' => $id,
                'nns_type' => $op_queue_info['nns_type'],
                'nns_name' => $op_queue_info['nns_name'],
                'nns_ref_id' => $video_info['base_info']['nns_id'],
                'nns_action' => NS_CDS_DELETE,
                'nns_status' => NS_CDS_CDN_WAIT,
                'nns_create_time' => date('Y-m-d H:i:s'),
                'nns_org_id' => $this->str_sp_id,
                'nns_src_id' => '',
                'nns_clip_task_id' => null,
                'nns_clip_date' => null,
                'nns_op_id' => $op_queue_info['nns_id'],
                'nns_epg_status' => NS_CDS_EPG_WAIT,
                'nns_message_id' => $op_queue_info['nns_message_id'],
                'nns_is_group' => $op_queue_info['nns_is_group'],
                'nns_weight' => $op_queue_info['nns_weight'],
                'nns_cp_id' => $op_queue_info['nns_cp_id'],
            );
            $result = \nl_c2_task::add($this->get_dc(), $insert_params);
        }
        else
        {
            $id = np_guid_rand($this->str_sp_id);
            $insert_params = array(
                'nns_id' => $id,
                'nns_type' => $op_queue_info['nns_type'],
                'nns_name' => $op_queue_info['nns_name'],
                'nns_ref_id' => $video_info['base_info']['nns_id'],
                'nns_action' => NS_CDS_ADD,
                'nns_status' => NS_CDS_CDN_WAIT,
                'nns_create_time' => date('Y-m-d H:i:s'),
                'nns_org_id' => $this->str_sp_id,
                'nns_src_id' => '',
                'nns_clip_task_id' => null,
                'nns_clip_date' => null,
                'nns_op_id' => $op_queue_info['nns_id'],
                'nns_epg_status' => NS_CDS_EPG_WAIT,
                'nns_message_id' => $op_queue_info['nns_message_id'],
                'nns_is_group' => $op_queue_info['nns_is_group'],
                'nns_weight' => $op_queue_info['nns_weight'],
                'nns_cp_id' => $op_queue_info['nns_cp_id'],
            );
            $result = \nl_c2_task::add($this->get_dc(), $insert_params);
        }

        if ($result['ret'] == 0)
        {
            $update_op = $this->task_progress($op_queue_info['nns_id']);
            if ($update_op['ret'] == 0)
            {
                return $this->write_log_return('执行中心同步指令注入到c2队列成功', NS_CDS_SUCCE);
            }
            else
            {
                return $this->write_log_return('执行中心同步指令注入到c2队列成功，但上报中心同步指令失败', NS_CDS_SUCCE);
            }
        }
        else
        {
            return $this->write_log_return('执行中心同步指令注入到c2队列失败', NS_CDS_FAIL);
        }
    }

    /**
     * 开始执行分集下发
     * @return array
     */
    public function index()
    {
        $index_info = $this->get_index_info();
        $index_type = $this->get_op_queue_type();
        $op_queue_info = $this->op_queue_info;

        $c2_task_arr = $this->get_c2_info($index_type, $index_info['base_info']['nns_id'], $op_queue_info['nns_org_id']);
        $c2_task_info = $c2_task_arr['data_info'][0];
        if (empty($c2_task_info) || !is_array($c2_task_info))
        {
            //如果注入进来的删除命令，而c2中又没有这个记录  则忽略
            if ((int)$this->arr_sp_config['virtual_destroy_task'] === 0 && $op_queue_info['nns_action'] == NS_CDS_DELETE)
            {
                $re = $this->center_op->report_op_task($op_queue_info['nns_id']);
                return $this->write_log_return($re['reason'], $re['ret']);
            }
        }
        //是否为最终状态
        $is_finally = $this->center_op->is_finally_status($c2_task_info);

        if ($is_finally['ret'] != 0)
        {
            //如果存在另外一个状态不为最终状态的，中心同步指令下发的队列
            if (!empty($c2_task_info) && $op_queue_info['nns_id'] != $c2_task_info['nns_op_id'])
            {
                //如果存在分组标示 上面的数据要一直存在队列中 等待下面的数据处理完毕才下发下去
                if ($op_queue_info['nns_is_group'] != 0)
                {
                    $temp_millisecond = round(np_millisecond_f() * 1000);
                    $op_execute_sql = "update nns_mgtvbk_op_queue set nns_op_mtime='{$temp_millisecond}' where nns_id='{$op_queue_info['nns_id']}'";
                    \nl_op_queue::query_by_sql($this->get_dc(), $op_execute_sql);
                    return $this->write_log_return('存在分组标示 上面的数据要一直存在队列中 等待下面的数据处理完毕才下发下去', NS_CDS_SUCCE);
                }
                $set = "";
                //存在任意一状态为终态，修改状态为等待注入
                if ($c2_task_info['nns_status'] == 7 || $c2_task_info['nns_epg_status'] == 100)
                {
                    $set = ",nns_status=1,nns_epg_status=97";
                }
                $op_execute_sql = "update nns_mgtvbk_c2_task set nns_cp_id='{$op_queue_info['nns_cp_id']}',nns_message_id='{$op_queue_info['nns_message_id']}',nns_name='{$op_queue_info['nns_name']}', nns_op_id='{$op_queue_info['nns_id']}' $set where nns_id='{$c2_task_info['nns_id']}'";
                \nl_op_queue::query_by_sql($this->get_dc(), $op_execute_sql);
                //将上一个中心同步指令队列删除
                $this->center_op->report_op_task($c2_task_info['nns_op_id']);
                $c2_task_info['nns_op_id'] = $op_queue_info['nns_id'];
            }
        }

        //如果存在任务就修改它的动作
        if (is_array($c2_task_info) && !empty($c2_task_info))
        {
            //如果列队过来的是添加动作，而C2任务中的是添加或则修改，则将c2设置为修改
            if (($c2_task_info['nns_action'] == NS_CDS_MODIFY || $c2_task_info['nns_action'] == NS_CDS_ADD)
                && $op_queue_info['nns_action'] == NS_CDS_ADD && ((int)$c2_task_info['nns_status'] != 7
                    || (int)$c2_task_info['nns_epg_status'] != 100))
            {
                if ($is_finally['ret'] == 0)
                {
                    $op_queue_info['nns_action'] = NS_CDS_MODIFY;
                } else
                {
                    $op_queue_info['nns_action'] = NS_CDS_ADD;
                }
            }

            //如果c2中本身是删除，而任务列队中同步的操作是添加或则修改，则按照新增处理
            if ($c2_task_info['nns_action'] == NS_CDS_DELETE && ($op_queue_info['nns_action'] == NS_CDS_MODIFY || $op_queue_info['nns_action'] == NS_CDS_ADD))
            {
                $op_queue_info['nns_action'] = NS_CDS_ADD;
            }

            //如果中心同步指令队列的操作是删除，则将它的所有子级正在执行的状态设置为cancel
            if ($op_queue_info['nns_action'] == NS_CDS_DELETE)
            {
                $this->set_child_cancel($op_queue_info['nns_type'], $c2_task_info['nns_ref_id']);
            }
            //修改状态和动作
            $update_params = array(
                'nns_epg_fail_time' => 0,
                'nns_cdn_fail_time' => 0,
                'nns_cp_id' => $op_queue_info['nns_cp_id'],
                'nns_is_group' => $op_queue_info['nns_is_group'],
                'nns_weight' => $op_queue_info['nns_weight'],
                'nns_message_id' => $op_queue_info['nns_message_id'],
                'nns_status' => NS_CDS_CDN_WAIT,
                'nns_action' => $op_queue_info['nns_action'],
                'nns_modify_time' => date('Y-m-d H:i:s'),
                'nns_op_id' => $op_queue_info['nns_id'],
                'nns_epg_status' => NS_CDS_EPG_WAIT,
                'nns_name' => $op_queue_info['nns_name'],
            );
            $result = \nl_c2_task::edit($this->get_dc(), $update_params, $c2_task_info['nns_id']);
        }
        //如果注入进来的删除命令，而且c2中又没有这个记录，并且中心同步删除指令生成c2虚拟任务开关打开，则生成虚拟c2任务
        else if (empty($c2_task_info) && !is_array($c2_task_info) && $op_queue_info['nns_action'] == NS_CDS_DELETE
            && (int)$this->arr_sp_config['virtual_destroy_task'] === 1)
        {
            $id = np_guid_rand($this->str_sp_id);
            $insert_params = array(
                'nns_id' => $id,
                'nns_type' => $op_queue_info['nns_type'],
                'nns_name' => $op_queue_info['nns_name'],
                'nns_ref_id' => $index_info['base_info']['nns_id'],
                'nns_action' => NS_CDS_DELETE,
                'nns_status' => NS_CDS_CDN_WAIT,
                'nns_create_time' => date('Y-m-d H:i:s'),
                'nns_org_id' => $this->str_sp_id,
                'nns_src_id' => $index_info['base_info']['nns_vod_id'],
                'nns_clip_task_id' => null,
                'nns_clip_date' => null,
                'nns_op_id' => $op_queue_info['nns_id'],
                'nns_epg_status' => NS_CDS_EPG_WAIT,
                'nns_message_id' => $op_queue_info['nns_message_id'],
                'nns_is_group' => $op_queue_info['nns_is_group'],
                'nns_weight' => $op_queue_info['nns_weight'],
                'nns_cp_id' => $op_queue_info['nns_cp_id'],
            );
            $result = \nl_c2_task::add($this->get_dc(), $insert_params);
        }
        else
        {
            $id = np_guid_rand($this->str_sp_id);
            $insert_params = array(
                'nns_id' => $id,
                'nns_type' => $op_queue_info['nns_type'],
                'nns_name' => $op_queue_info['nns_name'],
                'nns_ref_id' => $index_info['base_info']['nns_id'],
                'nns_action' => NS_CDS_ADD,
                'nns_status' => NS_CDS_CDN_WAIT,
                'nns_create_time' => date('Y-m-d H:i:s'),
                'nns_org_id' => $this->str_sp_id,
                'nns_src_id' => $index_info['base_info']['nns_vod_id'],
                'nns_clip_task_id' => null,
                'nns_clip_date' => null,
                'nns_op_id' => $op_queue_info['nns_id'],
                'nns_epg_status' => NS_CDS_EPG_WAIT,
                'nns_message_id' => $op_queue_info['nns_message_id'],
                'nns_is_group' => $op_queue_info['nns_is_group'],
                'nns_weight' => $op_queue_info['nns_weight'],
                'nns_cp_id' => $op_queue_info['nns_cp_id'],
            );
            $result = \nl_c2_task::add($this->get_dc(), $insert_params);
        }

        if ($result['ret'] == 0)
        {
            $update_op = $this->task_progress($op_queue_info['nns_id']);
            if ($update_op['ret'] == 0)
            {
                return $this->write_log_return('执行中心同步指令注入到c2队列成功', NS_CDS_SUCCE);
            }
            else
            {
                return $this->write_log_return('执行中心同步指令注入到c2队列成功，但上报中心同步指令失败', NS_CDS_SUCCE);
            }
        }
        else
        {
            return $this->write_log_return('执行中心同步指令注入到c2队列失败', NS_CDS_FAIL);
        }
    }

    /**
     * 执行片源下发
     * @return array
     */
    public function media()
    {
        $media_info = $this->get_media_info();
        $media_type = $this->get_op_queue_type();
        $op_queue_info = $this->op_queue_info;
        $c2_task_arr = $this->get_c2_info($media_type, $media_info['base_info']['nns_id'], $op_queue_info['nns_org_id']);
        $c2_task_info = $c2_task_arr['data_info'][0];
        if (empty($c2_task_info) || !is_array($c2_task_info))
        {
            //如果注入进来的删除命令，而c2中又没有这个记录  则忽略
            if ((int)$this->arr_sp_config['virtual_destroy_task'] == 0 && $op_queue_info['nns_action'] == NS_CDS_DELETE)
            {
                $re = $this->center_op->report_op_task($op_queue_info['nns_id']);
                return $this->write_log_return($re['reason'], $re['ret']);
            }
        }
        $is_finally = $this->center_op->is_finally_status($c2_task_info);

        if ($is_finally['ret'] != 0)
        {
            //如果存在另外一个状态不为最终状态的，中心同步指令下发的队列
            if (!empty($c2_task_info) && $op_queue_info['nns_id'] != $c2_task_info['nns_op_id'])
            {
                //如果存在分组标示 上面的数据要一直存在队列中 等待下面的数据处理完毕才下发下去
                if ($op_queue_info['nns_is_group'] != 0)
                {
                    $temp_millisecond = round(np_millisecond_f() * 1000);
                    $op_execute_sql = "update nns_mgtvbk_op_queue set nns_op_mtime='{$temp_millisecond}' where nns_id='{$op_queue_info['nns_id']}'";
                    \nl_op_queue::query_by_sql($this->get_dc(), $op_execute_sql);
                    return $this->write_log_return('存在分组标示 上面的数据要一直存在队列中 等待下面的数据处理完毕才下发下去', NS_CDS_SUCCE);
                }
                $set = "";
                //存在任意一状态为终态，修改状态为等待注入
                if ($c2_task_arr['data_info'][0]['nns_status'] == 7 || $c2_task_arr['data_info'][0]['nns_epg_status'] == 100)
                {
                    $set = ",nns_status=1,nns_epg_status=97";
                }
                $op_execute_sql = "update nns_mgtvbk_c2_task set nns_cp_id='{$op_queue_info['nns_cp_id']}',nns_message_id='{$op_queue_info['nns_message_id']}',nns_name='{$op_queue_info['nns_name']}', nns_op_id='{$op_queue_info['nns_id']}' $set where nns_id='{$c2_task_info['nns_id']}'";
                \nl_op_queue::query_by_sql($this->get_dc(), $op_execute_sql);
                //将上一个中心同步指令队列删除
                $this->center_op->report_op_task($c2_task_info['nns_op_id']);
                $c2_task_info['nns_op_id'] = $op_queue_info['nns_id'];
            }
        }

        //如果存在任务就修改它的动作
        if (is_array($c2_task_info) && !empty($c2_task_info))
        {
            //如果列队过来的是添加动作，而C2任务中的是添加或则修改，则将c2设置为修改
            if (($c2_task_info['nns_action'] == NS_CDS_MODIFY || $c2_task_info['nns_action'] == NS_CDS_ADD)
                && $op_queue_info['nns_action'] == NS_CDS_ADD && ((int)$c2_task_info['nns_status'] != 7
                    || (int)$c2_task_info['nns_epg_status'] != 100))
            {
                if ($is_finally['ret'] == 0)
                {
                    $op_queue_info['nns_action'] = NS_CDS_MODIFY;
                }
                else
                {
                    $op_queue_info['nns_action'] = NS_CDS_ADD;
                }

            }

            //如果c2中本身是删除，而任务列队中同步的操作是添加或则修改，则按照新增处理
            if ($c2_task_info['nns_action'] == NS_CDS_DELETE && ($op_queue_info['nns_action'] == NS_CDS_MODIFY || $op_queue_info['nns_action'] == NS_CDS_ADD))
            {
                $op_queue_info['nns_action'] = NS_CDS_ADD;
            }
            $ex_data = array();
            //非删除指令，修改信息
            if ($op_queue_info['nns_action'] != NS_CDS_DELETE)
            {
                $ext_data = json_decode($op_queue_info['nns_ex_data'],true);
                if (isset($ext_data['url']))
                {
                    $ex_data['nns_file_path'] = $ext_data['url'];
                }
                if (isset($ext_data['clip_id']))
                {
                    $ex_data['nns_clip_task_id'] = $ext_data['clip_id'];
                }
                if (isset($ext_data['clip_date']))
                {
                    $ex_data['nns_clip_date'] = $ext_data['clip_date'];
                }
                if (isset($ext_data['size']))
                {
                    $ex_data['nns_file_size'] = $ext_data['size'];
                }
                if (isset($ext_data['file_md5']))
                {
                    $ex_data['nns_file_md5'] = $ext_data['file_md5'];
                }
                if (isset($ext_data['policy']))
                {
                    $ex_data['nns_cdn_policy'] = $ext_data['policy'];
                }
                if (isset($op_queue_info['nns_message_id']) && strlen($op_queue_info['nns_message_id']) > 0)
                {
                    $ex_data['nns_message_id'] = $op_queue_info['nns_message_id'];
                }
            }
            //修改状态和动作
            $update_params = array(
                'nns_epg_fail_time' => 0,
                'nns_cdn_fail_time' => 0,
                'nns_cp_id' => $op_queue_info['nns_cp_id'],
                'nns_is_group' => $op_queue_info['nns_is_group'],
                'nns_weight' => $op_queue_info['nns_weight'],
                'nns_message_id' => $op_queue_info['nns_message_id'],
                'nns_status' => NS_CDS_CDN_WAIT,
                'nns_action' => $op_queue_info['nns_action'],
                'nns_modify_time' => date('Y-m-d H:i:s'),
                'nns_op_id' => $op_queue_info['nns_id'],
                'nns_epg_status' => NS_CDS_EPG_WAIT,
                'nns_name' => $op_queue_info['nns_name'],
                ''
            );
            $update_params =  array_merge($update_params, $ex_data);
            $result = \nl_c2_task::edit($this->get_dc(), $update_params, $c2_task_info['nns_id']);
            unset($insert_params);
            unset($ex_data);
        }
        //如果注入进来的删除命令，而且c2中又没有这个记录，并且中心同步删除指令生成c2虚拟任务开关打开，则生成虚拟c2任务
        else if (empty($c2_task_info) && !is_array($c2_task_info) && $op_queue_info['nns_action'] == NS_CDS_DELETE
            && (int)$this->arr_sp_config['virtual_destroy_task'] === 1)
        {
            $id = np_guid_rand($this->str_sp_id);
            $insert_params = array(
                'nns_id' => $id,
                'nns_type' => $media_type,
                'nns_name' => $op_queue_info['nns_name'],
                'nns_ref_id' => $media_info['base_info']['nns_id'],
                'nns_action' => NS_CDS_DELETE,
                'nns_status' => NS_CDS_CDN_WAIT,
                'nns_create_time' => date('Y-m-d H:i:s'),
                'nns_org_id' => $this->str_sp_id,
                'nns_src_id' => $media_info['base_info']['nns_vod_index_id'],
                'nns_clip_task_id' => null,
                'nns_clip_date' => null,
                'nns_op_id' => $op_queue_info['nns_id'],
                'nns_epg_status' => NS_CDS_EPG_WAIT,
                'nns_message_id' => $op_queue_info['nns_message_id'],
                'nns_is_group' => $op_queue_info['nns_is_group'],
                'nns_weight' => $op_queue_info['nns_weight'],
                'nns_cp_id' => $op_queue_info['nns_cp_id'],
            );
            $result = \nl_c2_task::add($this->get_dc(), $insert_params);
            unset($insert_params);
        }
        else
        {
            //如果获取的操作队列动作为删除或者c2没有注入，则将状态修改为添加
            $id = np_guid_rand($this->str_sp_id);

            $ex_data_arr = array();
            if ($op_queue_info['nns_action'] != NS_CDS_DELETE)
            {
                $ext_data = json_decode($op_queue_info['nns_ex_data'],true);
                if (isset($ext_data['url']))
                {
                    $ex_data_arr['nns_file_path'] = $ext_data['url'];
                }
                if (isset($ext_data['clip_id']))
                {
                    $ex_data_arr['nns_clip_task_id'] = $ext_data['clip_id'];
                }
                if (isset($ext_data['clip_date']))
                {
                    $ex_data_arr['nns_clip_date'] = $ext_data['clip_date'];
                }
                if (isset($ext_data['size']))
                {
                    $ex_data_arr['nns_file_size'] = $ext_data['size'];
                }
                if (isset($ext_data['file_md5']))
                {
                    $ex_data_arr['nns_file_md5'] = $ext_data['file_md5'];
                }
                if (isset($ext_data['policy']))
                {
                    $ex_data_arr['nns_cdn_policy'] = $ext_data['policy'];
                }
            }
            if (isset($op_queue_info['nns_message_id']) && strlen($op_queue_info['nns_message_id']) > 0)
            {
                $ex_data_arr['nns_message_id'] = $op_queue_info['nns_message_id'];
            }
            $insert_params = array(
                'nns_cp_id' => $op_queue_info['nns_cp_id'],
                'nns_weight' => $op_queue_info['nns_weight'],
                'nns_is_group' => $op_queue_info['nns_is_group'],
                'nns_id' => $id,
                'nns_type' => $op_queue_info['nns_type'],
                'nns_name' => $op_queue_info['nns_name'],
                'nns_ref_id' => $media_info['base_info']['nns_id'],
                'nns_action' => NS_CDS_ADD,
                'nns_status' => NS_CDS_CDN_WAIT,
                'nns_create_time' => date('Y-m-d H:i:s'),
                'nns_org_id' => $op_queue_info['nns_org_id'],
                'nns_src_id' => $media_info['base_info']['nns_vod_index_id'],
                'nns_op_id' => $op_queue_info['nns_id'],
                'nns_epg_status' => NS_CDS_EPG_WAIT,
            );
            $insert_params = array_merge($insert_params, $ex_data_arr);
            $result = \nl_c2_task::add($this->get_dc(), $insert_params);
            unset($insert_params);
            unset($ex_data_arr);
        }

        if ($result['ret'] == 0)
        {
            $update_op = $this->task_progress($op_queue_info['nns_id']);
            if ($update_op['ret'] == 0)
            {
                return $this->write_log_return('执行中心同步指令注入到c2队列成功', NS_CDS_SUCCE);
            }
            else
            {
                return $this->write_log_return('执行中心同步指令注入到c2队列成功，但上报中心同步指令失败', NS_CDS_SUCCE);
            }
        }
        else
        {
            return $this->write_log_return('执行中心同步指令注入到c2队列失败', NS_CDS_FAIL);
        }
    }

    /**
     * 执行节目单下发
     * @return array
     * @author  song.wang
     */
    public function playbill()
    {
        $playbill_info = $this->get_playbill_info();
        $playbill_type = $this->get_op_queue_type();
        $op_queue_info = $this->op_queue_info;              //中心同步指令的该条数据
        $c2_task_arr = $this->get_c2_live_info($playbill_type, $playbill_info['base_info']['nns_id'], $op_queue_info['nns_org_id']);
        $c2_task_info = $c2_task_arr['data_info'][0];       //c2_live_task里，同资源的数据
        //1、如果c2_live_task里面没有之前同nns_ref_id，同资源成功的命令，是删除的话，直接忽略（具体看配置，也有生成虚拟删除命令的）
        if (empty($c2_task_info) || !is_array($c2_task_info))
        {
            //如果注入进来的删除命令，而c2中又没有这个记录  则忽略
            if ((int)$this->arr_sp_config['virtual_destroy_task'] == 0 && $op_queue_info['nns_action'] == NS_CDS_DELETE)
            {
                $re = $this->center_op->report_op_task($op_queue_info['nns_id']);
                return $this->write_log_return($re['reason'], $re['ret']);
            }
        }
        //2、获取之前同资源在c2_live_task里，是不是已经达到最终状态
        $is_finally = $this->center_op->is_finally_status($c2_task_info);
        //3、如果不是最终状态
//        if ($is_finally['ret'] != 0)
//        {
//            if (!empty($c2_task_info) && $op_queue_info['nns_id'] != $c2_task_info['nns_op_id'])    //如果这两条数据无关联
//            {
//                //如果存在分组标示，上面op_queue的数据要一直存在队列中，等待下面c2_live_task的数据处理完毕才下发下去
//                if ($op_queue_info['nns_is_group'] != 0)
//                {
//                    $temp_millisecond = round(np_millisecond_f() * 1000);
//                    $op_execute_sql = "update nns_mgtvbk_op_queue set nns_op_mtime='{$temp_millisecond}' where nns_id='{$op_queue_info['nns_id']}'";
//                    \nl_op_queue::query_by_sql($this->get_dc(), $op_execute_sql);
//                    return $this->write_log_return('存在分组标示 上面的数据要一直存在队列中 等待下面的数据处理完毕才下发下去', NS_CDS_SUCCE);
//                }
//                //下面是直接把c2_live_task已存在的同资源的数据里面部分字段改为——该op_queue的字段数据，并且删除op_queue的数据
//                $set = "";
//                //存在任意一状态为取消注入状态，修改状态为等待注入
//                if ($c2_task_info['nns_status'] == 7 || $c2_task_info['nns_epg_status'] == 100)
//                {
//                    $set = ",nns_status=1,nns_epg_status=97";
//                }
//                $op_execute_sql = "update nns_mgtvbk_c2_task set nns_cp_id='{$op_queue_info['nns_cp_id']}',nns_message_id='{$op_queue_info['nns_message_id']}',nns_name='{$op_queue_info['nns_name']}', nns_op_id='{$op_queue_info['nns_id']}' $set where nns_id='{$c2_task_info['nns_id']}'";
//                \nl_op_queue::query_by_sql($this->get_dc(), $op_execute_sql);
//                //将op_queue数据删除
//                $this->center_op->report_op_task($c2_task_info['nns_op_id']);
//                $c2_task_info['nns_op_id'] = $op_queue_info['nns_id'];
//            }
//        }
        //4、如果c2_live_task里面有这条数据（就是c2_live_task里面ref_id指的，跟op_queue指的资源是同一个）
        if($op_queue_info['nns_action'] == NS_CDS_DELETE)
        {
            if(!empty($c2_task_info))
            {
                if($c2_task_info['nns_action'] == NS_CDS_DELETE)
                {
                    $re = $this->center_op->report_op_task($op_queue_info['nns_id']);
                    return $this->write_log_return('节目单：中心同步指令删除，但是c2_live_task中也是同节目单的删除，所以没必要下发，并且在op_queue里面删除该条数据', $re['ret']);
                }
                else
                {
                    if($is_finally['ret'] == 0)
                    {
                        $insert_params = array(
                            'nns_id' => np_guid_rand($this->str_sp_id),
                            'nns_type' => $op_queue_info['nns_type'],
                            'nns_name' => $op_queue_info['nns_name'],
                            'nns_ref_id' => $op_queue_info['nns_media_id'],     //节目单的id在op_queue里面保存在nns_media_id字段中
                            'nns_action' => NS_CDS_DELETE,
                            'nns_status' => NS_CDS_CDN_WAIT,
                            'nns_create_time' => date('Y-m-d H:i:s'),
                            'nns_modify_time' => date('Y-m-d H:i:s'),
                            'nns_org_id' => $this->str_sp_id,
                            'nns_src_id' => $op_queue_info['nns_video_id'],     //节目单所属直播频道的id在op_queue里面保存在nns_video_id字段中
                            'nns_clip_task_id' => null,
                            'nns_clip_date' => null,
                            'nns_op_id' => $op_queue_info['nns_id'],
                            'nns_epg_status' => NS_CDS_EPG_WAIT,
                            'nns_message_id' => $op_queue_info['nns_message_id'],
                            'nns_is_group' => $op_queue_info['nns_is_group'],
                            'nns_weight' => $op_queue_info['nns_weight'],
                            'nns_cp_id' => $op_queue_info['nns_cp_id'],
                        );
                        $result = \nl_c2_live_task::edit($this->get_dc(), $insert_params,$c2_task_info['nns_id']);
                        unset($insert_params);
                    }
                    else
                    {
                        return $this->write_log_return('节目单：中心同步指令删除，但是c2_live_task中该节目单的添加指令还未完成，等到添加完成，在进行删除', NS_CDS_SUCCE);
                    }
                }
            }
            else
            {
                if((int)$this->arr_sp_config['virtual_destroy_task'] === 1)
                {
                    $id = np_guid_rand($this->str_sp_id);
                    $insert_params = array(
                        'nns_id' => $id,
                        'nns_type' => $op_queue_info['nns_type'],
                        'nns_name' => $op_queue_info['nns_name'],
                        'nns_ref_id' => $op_queue_info['nns_media_id'],     //节目单的id在op_queue里面保存在nns_media_id字段中
                        'nns_action' => NS_CDS_DELETE,
                        'nns_status' => NS_CDS_CDN_WAIT,
                        'nns_create_time' => date('Y-m-d H:i:s'),
                        'nns_org_id' => $this->str_sp_id,
                        'nns_src_id' => $op_queue_info['nns_video_id'],     //节目单所属直播频道的id在op_queue里面保存在nns_video_id字段中
                        'nns_clip_task_id' => null,
                        'nns_clip_date' => null,
                        'nns_op_id' => $op_queue_info['nns_id'],
                        'nns_epg_status' => NS_CDS_EPG_WAIT,
                        'nns_message_id' => $op_queue_info['nns_message_id'],
                        'nns_is_group' => $op_queue_info['nns_is_group'],
                        'nns_weight' => $op_queue_info['nns_weight'],
                        'nns_cp_id' => $op_queue_info['nns_cp_id'],
                    );
                    $result = \nl_c2_live_task::add($this->get_dc(), $insert_params);
                    unset($insert_params);
                }
                else
                {
                    return $this->write_log_return('节目单：中心同步指令删除，但是c2_live_task中没有该节目单的任何指令，所以不做任何处理，暂时不管', NS_CDS_SUCCE);
                }
            }
        }
        else
        {
            if(!empty($c2_task_info))
            {
                if($c2_task_info['nns_action'] == NS_CDS_DELETE)
                {
                    if($is_finally['ret'] == 0)
                    {
                        $insert_params = array(
                            'nns_id' => np_guid_rand($this->str_sp_id),
                            'nns_type' => $op_queue_info['nns_type'],
                            'nns_name' => $op_queue_info['nns_name'],
                            'nns_ref_id' => $op_queue_info['nns_media_id'],     //节目单的id在op_queue里面保存在nns_media_id字段中
                            'nns_action' => NS_CDS_ADD,
                            'nns_status' => NS_CDS_CDN_WAIT,
                            'nns_create_time' => date('Y-m-d H:i:s'),
                            'nns_modify_time' => date('Y-m-d H:i:s'),
                            'nns_org_id' => $this->str_sp_id,
                            'nns_src_id' => $op_queue_info['nns_video_id'],     //节目单所属直播频道的id在op_queue里面保存在nns_video_id字段中
                            'nns_clip_task_id' => null,
                            'nns_clip_date' => null,
                            'nns_op_id' => $op_queue_info['nns_id'],
                            'nns_epg_status' => NS_CDS_EPG_WAIT,
                            'nns_message_id' => $op_queue_info['nns_message_id'],
                            'nns_is_group' => $op_queue_info['nns_is_group'],
                            'nns_weight' => $op_queue_info['nns_weight'],
                            'nns_cp_id' => $op_queue_info['nns_cp_id'],
                        );
                        $result = \nl_c2_live_task::edit($this->get_dc(), $insert_params,$c2_task_info['nns_id']);
                        unset($insert_params);
                    }
                    else
                    {
                        return $this->write_log_return('节目单：中心同步指令添加，但是c2_live_task中该节目单的删除指令还未完成，等到删除完成，在进行添加', NS_CDS_SUCCE);
                    }
                }
                else
                {
                    return $this->write_log_return('节目单：中心同步指令添加，但是c2_live_task中也是同节目单的添加，所以没必要下发，暂时不管', NS_CDS_SUCCE);
                }
            }
            else
            {
                //判断这里判断同日期下面的节目单有没有被删除
                $params = array(
                    'nns_begin_data'=>$playbill_info['base_info']['nns_begin_data'],
                    'nns_deleted'=>1,
                    'nns_c2_is_deleted'=>0,
                    'nns_live_id'=>$op_queue_info['nns_video_id'],
                );
                $not_deleted_playbill = \nl_playbill_item::query_by_condition(\m_config::get_dc(),$params);
                if($not_deleted_playbill['ret'] != 0 || $not_deleted_playbill['data_info'] === false)
                {
                    return $this->write_log_return('节目单：中心同步指令添加，查询该频道下当天日期下节目单有没有被删除干净，此次查询失败', NS_CDS_FAIL);
                }
                if(is_array($not_deleted_playbill['data_info']) && !empty($not_deleted_playbill['data_info']))
                {
                    return $this->write_log_return('节目单：中心同步指令添加，但是该频道下当天日期下节目单没有被删除干净，等完全删除再进行添加', NS_CDS_SUCCE);
                }
                else
                {
                    $insert_params = array(
                        'nns_id' => np_guid_rand($this->str_sp_id),
                        'nns_type' => $op_queue_info['nns_type'],
                        'nns_name' => $op_queue_info['nns_name'],
                        'nns_ref_id' => $op_queue_info['nns_media_id'],     //节目单的id在op_queue里面保存在nns_media_id字段中
                        'nns_action' => NS_CDS_ADD,
                        'nns_status' => NS_CDS_CDN_WAIT,
                        'nns_create_time' => date('Y-m-d H:i:s'),
                        'nns_org_id' => $this->str_sp_id,
                        'nns_src_id' => $op_queue_info['nns_video_id'],     //节目单所属直播频道的id在op_queue里面保存在nns_video_id字段中
                        'nns_clip_task_id' => null,
                        'nns_clip_date' => null,
                        'nns_op_id' => $op_queue_info['nns_id'],
                        'nns_epg_status' => NS_CDS_EPG_WAIT,
                        'nns_message_id' => $op_queue_info['nns_message_id'],
                        'nns_is_group' => $op_queue_info['nns_is_group'],
                        'nns_weight' => $op_queue_info['nns_weight'],
                        'nns_cp_id' => $op_queue_info['nns_cp_id'],
                    );
                    $result = \nl_c2_live_task::add($this->get_dc(), $insert_params);
                    unset($insert_params);
                }
            }
        }
//        if (is_array($c2_task_info) && !empty($c2_task_info))
//        {
//            //如果注过来的op_queue是删除，且c2_live_task里是添加或者修改
//            //换句话说，删除命令一定要在c2_live_task有成功的添加或者修改命令，并且是最终状态的时候，才能下发
//            if($op_queue_info['nns_action'] == NS_CDS_DELETE && ($c2_task_info['nns_action'] == NS_CDS_MODIFY || $c2_task_info['nns_action'] == NS_CDS_ADD))
//            {
//                //如果c2_live_task已经完成最终状态
//                if($is_finally['ret'] == 0)
//                {
//                    $op_queue_info['nns_action'] = NS_CDS_DELETE;
//                }
//                //如果c2_live_task不是最终状态
//                else
//                {
//                    return $this->write_log_return('该节目单不是最终状态', NS_CDS_SUCCE);
//                }
//            }
//            //如果新注入进来的op_queue是添加，并且c2_live_task里面是添加或者修改，且没有取消CDN和EPG注入，
//            //把op_queue的数据类型改为修改
//            if(($op_queue_info['nns_action'] == NS_CDS_MODIFY || $op_queue_info['nns_action'] == NS_CDS_ADD) && ($c2_task_info['nns_action'] == NS_CDS_MODIFY || $c2_task_info['nns_action'] == NS_CDS_ADD) && ((int)$c2_task_info['nns_status'] != 7 || (int)$c2_task_info['nns_epg_status'] != 100) )
//            {
//                if ($is_finally['ret'] == 0)
//                {
//                    $op_queue_info['nns_action'] = NS_CDS_MODIFY;
//                }
//            }
//            //如果c2_live_task里面是删除，op_queue里面是添加，那么把op_queue的操作置为添加
//            if ($c2_task_info['nns_action'] == NS_CDS_DELETE && ($op_queue_info['nns_action'] == NS_CDS_MODIFY || $op_queue_info['nns_action'] == NS_CDS_ADD))
//            {
//                $op_queue_info['nns_action'] = NS_CDS_ADD;
//            }
//            //修改原c2_live_task里面的状态和动作，强行置为 CDN等待注入、EPG等待注入
//            $update_params = array(
//                'nns_epg_fail_time' => 0,
//                'nns_cdn_fail_time' => 0,
//                'nns_cp_id' => $op_queue_info['nns_cp_id'],
//                'nns_is_group' => $op_queue_info['nns_is_group'],
//                'nns_weight' => $op_queue_info['nns_weight'],
//                'nns_message_id' => $op_queue_info['nns_message_id'],
//                'nns_status' => NS_CDS_CDN_WAIT,
//                'nns_action' => $op_queue_info['nns_action'],
//                'nns_modify_time' => date('Y-m-d H:i:s'),
//                'nns_op_id' => $op_queue_info['nns_id'],
//                'nns_epg_status' => NS_CDS_EPG_WAIT,
//                'nns_name' => $op_queue_info['nns_name'],
//            );
//            $result = \nl_c2_live_task::edit($this->get_dc(), $update_params, $c2_task_info['nns_id']);
//            unset($insert_params);
//            unset($ex_data);
//        }
//        //5、如果注入进来的删除命令，而且c2中又没有这个记录，并且中心同步删除指令生成c2虚拟任务开关打开，则生成虚拟c2任务
//        else
//        {
//            if($op_queue_info['nns_action'] == NS_CDS_DELETE  &&  (int)$this->arr_sp_config['virtual_destroy_task'] === 1)
//            {
//                $id = np_guid_rand($this->str_sp_id);
//                $insert_params = array(
//                    'nns_id' => $id,
//                    'nns_type' => $op_queue_info['nns_type'],
//                    'nns_name' => $op_queue_info['nns_name'],
//                    'nns_ref_id' => $op_queue_info['nns_media_id'],     //节目单的id在op_queue里面保存在nns_media_id字段中
//                    'nns_action' => NS_CDS_DELETE,
//                    'nns_status' => NS_CDS_CDN_WAIT,
//                    'nns_create_time' => date('Y-m-d H:i:s'),
//                    'nns_org_id' => $this->str_sp_id,
//                    'nns_src_id' => $op_queue_info['nns_video_id'],     //节目单所属直播频道的id在op_queue里面保存在nns_video_id字段中
//                    'nns_clip_task_id' => null,
//                    'nns_clip_date' => null,
//                    'nns_op_id' => $op_queue_info['nns_id'],
//                    'nns_epg_status' => NS_CDS_EPG_WAIT,
//                    'nns_message_id' => $op_queue_info['nns_message_id'],
//                    'nns_is_group' => $op_queue_info['nns_is_group'],
//                    'nns_weight' => $op_queue_info['nns_weight'],
//                    'nns_cp_id' => $op_queue_info['nns_cp_id'],
//                );
//                $result = \nl_c2_task::add($this->get_dc(), $insert_params);
//                unset($insert_params);
//            }
//            if($op_queue_info['nns_action'] == NS_CDS_ADD || $op_queue_info['nns_action'] == NS_CDS_MODIFY)
//            {
//                //判断nns_live_playbill_item当天的节目单是不是都删除了
//                $not_deleted_playbill = \nl_playbill_item::query_by_condition(\m_config::get_dc(),array("nns_begin_data"=>$playbill_info['base_info']['nns_begin_data'],"nns_c2_is_deleted"=>0));
//                if(!empty($not_deleted_playbill['data_info']))  //如果有还没删除的
//                {
//                    return $this->write_log_return('该频道下当天节目单还未删除干净', NS_CDS_SUCCE);
//                }
//                //可以直接添加到c2_live_task
//                $id = np_guid_rand($this->str_sp_id);
//                $insert_params = array(
//                    'nns_id' => $id,
//                    'nns_type' => $op_queue_info['nns_type'],
//                    'nns_name' => $op_queue_info['nns_name'],
//                    'nns_ref_id' => $op_queue_info['nns_media_id'],     //节目单的id在op_queue里面保存在nns_media_id字段中
//                    'nns_action' => NS_CDS_DELETE,
//                    'nns_status' => NS_CDS_CDN_WAIT,
//                    'nns_create_time' => date('Y-m-d H:i:s'),
//                    'nns_org_id' => $this->str_sp_id,
//                    'nns_src_id' => $op_queue_info['nns_video_id'],     //节目单所属直播频道的id在op_queue里面保存在nns_video_id字段中
//                    'nns_clip_task_id' => null,
//                    'nns_clip_date' => null,
//                    'nns_op_id' => $op_queue_info['nns_id'],
//                    'nns_epg_status' => NS_CDS_EPG_WAIT,
//                    'nns_message_id' => $op_queue_info['nns_message_id'],
//                    'nns_is_group' => $op_queue_info['nns_is_group'],
//                    'nns_weight' => $op_queue_info['nns_weight'],
//                    'nns_cp_id' => $op_queue_info['nns_cp_id'],
//                );
//                $result = \nl_c2_task::add($this->get_dc(), $insert_params);
//                unset($insert_params);
//            }
//        }
        //6、剩余情况处理
//        else
//        {
//            $id = np_guid_rand($this->str_sp_id);
//            $ex_data_arr = array();
//            if ($op_queue_info['nns_action'] != NS_CDS_DELETE)
//            {
//                $ext_data = json_decode($op_queue_info['nns_ex_data'],true);
//                if (isset($ext_data['url']))
//                {
//                    $ex_data_arr['nns_file_path'] = $ext_data['url'];
//                }
//                if (isset($ext_data['clip_id']))
//                {
//                    $ex_data_arr['nns_clip_task_id'] = $ext_data['clip_id'];
//                }
//                if (isset($ext_data['clip_date']))
//                {
//                    $ex_data_arr['nns_clip_date'] = $ext_data['clip_date'];
//                }
//                if (isset($ext_data['size']))
//                {
//                    $ex_data_arr['nns_file_size'] = $ext_data['size'];
//                }
//                if (isset($ext_data['file_md5']))
//                {
//                    $ex_data_arr['nns_file_md5'] = $ext_data['file_md5'];
//                }
//                if (isset($ext_data['policy']))
//                {
//                    $ex_data_arr['nns_cdn_policy'] = $ext_data['policy'];
//                }
//            }
//            if (isset($op_queue_info['nns_message_id']) && strlen($op_queue_info['nns_message_id']) > 0)
//            {
//                $ex_data_arr['nns_message_id'] = $op_queue_info['nns_message_id'];
//            }
//            $insert_params = array(
//                'nns_cp_id' => $op_queue_info['nns_cp_id'],
//                'nns_weight' => $op_queue_info['nns_weight'],
//                'nns_is_group' => $op_queue_info['nns_is_group'],
//                'nns_id' => $id,
//                'nns_type' => $op_queue_info['nns_type'],
//                'nns_name' => $op_queue_info['nns_name'],
//                'nns_ref_id' => $playbill_info['base_info']['nns_id'],
//                'nns_action' => NS_CDS_ADD,
//                'nns_status' => NS_CDS_CDN_WAIT,
//                'nns_create_time' => date('Y-m-d H:i:s'),
//                'nns_org_id' => $op_queue_info['nns_org_id'],
//                'nns_src_id' => $playbill_info['base_info']['nns_live_id'],    //节目单父级ID，节目单父级ID是直播频道ID
//                'nns_op_id' => $op_queue_info['nns_id'],
//                'nns_epg_status' => NS_CDS_EPG_WAIT,
//            );
//            $insert_params = array_merge($insert_params, $ex_data_arr);
//            $result = \nl_c2_live_task::add($this->get_dc(),$insert_params);
//            unset($insert_params);
//            unset($ex_data_arr);
//        }
        if ($result['ret'] == 0)
        {
            $update_op = $this->task_progress($op_queue_info['nns_id']);
            if ($update_op['ret'] == 0)
            {
                return $this->write_log_return('执行中心同步指令注入到c2队列成功', NS_CDS_SUCCE);
            }
            else
            {
                return $this->write_log_return('执行中心同步指令注入到c2队列成功，但上报中心同步指令失败', NS_CDS_SUCCE);
            }
        }
        else
        {
            return $this->write_log_return('执行中心同步指令注入到c2队列失败', NS_CDS_FAIL);
        }
    }

    /**
     * 执行直播频道下发
     * @return array
     */
    public function live()
    {
        $video_info = $this->get_live_info();
        $video_type = $this->get_op_queue_type();
        $op_queue_info = $this->op_queue_info;

        //查询是否在C2队列中是否已经存在此任务
        $c2_task_arr = $this->get_c2_live_info($video_type, $video_info['base_info']['nns_id'], $op_queue_info['nns_org_id']);
        $c2_task_info = $c2_task_arr['data_info'][0];
        if (empty($c2_task_info) || !is_array($c2_task_info))
        {
            //如果注入进来的删除命令，而c2中又没有这个记录  则忽略
            if ((int)$this->arr_sp_config['virtual_destroy_task'] === 0 && $op_queue_info['nns_action'] == NS_CDS_DELETE)
            {
                $re = $this->center_op->report_op_task($op_queue_info['nns_id']);
                return $this->write_log_return($re['reason'], $re['ret']);
            }
        }
        //是否为最终状态
        $is_finally = $this->center_op->is_finally_status($c2_task_info);
        if ($is_finally['ret'] != 0)
        {
            //如果存在另外一个状态不为最终状态的，中心同步指令下发的队列
            if (!empty($c2_task_info) && $op_queue_info['nns_id'] != $c2_task_info['nns_op_id'])
            {
                //如果存在分组标示 上面的数据要一直存在队列中 等待下面的数据处理完毕才下发下去
                if ($op_queue_info['nns_is_group'] != 0)
                {
                    $temp_millisecond = round(np_millisecond_f() * 1000);
                    $op_execute_sql = "update nns_mgtvbk_op_queue set nns_op_mtime='{$temp_millisecond}' where nns_id='{$op_queue_info['nns_id']}'";
                    \nl_op_queue::query_by_sql($this->get_dc(), $op_execute_sql);
                    return $this->write_log_return('存在分组标示 上面的数据要一直存在队列中 等待下面的数据处理完毕才下发下去', NS_CDS_SUCCE);
                }
                $set = "";
                //存在任意一状态为终态，修改状态为等待注入
                if ($c2_task_info['nns_status'] == 7 || $c2_task_info['nns_epg_status'] == 100)
                {
                    $set = ",nns_status=1,nns_epg_status=97";
                }
                $op_execute_sql = "update nns_mgtvbk_c2_live_task set nns_cp_id='{$op_queue_info['nns_cp_id']}',nns_message_id='{$op_queue_info['nns_message_id']}',nns_name='{$op_queue_info['nns_name']}', nns_op_id='{$op_queue_info['nns_id']}' $set where nns_id='{$c2_task_info['nns_id']}'";
                \nl_op_queue::query_by_sql($this->get_dc(), $op_execute_sql);
                //将上一个中心同步指令队列删除
                $this->center_op->report_op_task($c2_task_info['nns_op_id']);
                $c2_task_info['nns_op_id'] = $op_queue_info['nns_id'];
            }
        }
        //如果存在任务就修改它的动作
        if (is_array($c2_task_info) && !empty($c2_task_info))
        {
            //如果列队过来的是添加动作，而C2任务中的是添加或则修改，则将c2设置为修改
            if (($c2_task_info['nns_action'] == NS_CDS_MODIFY || $c2_task_info['nns_action'] == NS_CDS_ADD)
                && $op_queue_info['nns_action'] == NS_CDS_ADD && ((int)$c2_task_info['nns_status'] != 7
                    || (int)$c2_task_info['nns_epg_status'] != 100))
            {
                if ($is_finally['ret'] == 0)
                {
                    $op_queue_info['nns_action'] = NS_CDS_MODIFY;
                } else
                {
                    $op_queue_info['nns_action'] = NS_CDS_ADD;
                }
            }
            //如果c2中本身是删除，而任务列队中同步的操作是添加或则修改，则按照新增处理
            if ($c2_task_info['nns_action'] == NS_CDS_DELETE && ($op_queue_info['nns_action'] == NS_CDS_MODIFY || $op_queue_info['nns_action'] == NS_CDS_ADD))
            {
                $op_queue_info['nns_action'] = NS_CDS_ADD;
            }

            //如果中心同步指令队列的操作是删除，则将它的所有子级正在执行的状态设置为cancel
            if ($op_queue_info['nns_action'] == NS_CDS_DELETE)
            {
                $this->set_child_cancel($op_queue_info['nns_type'], $c2_task_info['nns_ref_id']);
            }
            //修改状态和动作
            $update_params = array(
                'nns_epg_fail_time' => 0,
                'nns_cdn_fail_time' => 0,
                'nns_cp_id' => $op_queue_info['nns_cp_id'],
                'nns_is_group' => $op_queue_info['nns_is_group'],
                'nns_weight' => $op_queue_info['nns_weight'],
                'nns_message_id' => $op_queue_info['nns_message_id'],
                'nns_status' => NS_CDS_CDN_WAIT,
                'nns_action' => $op_queue_info['nns_action'],
                'nns_modify_time' => date('Y-m-d H:i:s'),
                'nns_op_id' => $op_queue_info['nns_id'],
                'nns_epg_status' => NS_CDS_EPG_WAIT,
                'nns_name' => $op_queue_info['nns_name'],
            );
            $result = \nl_c2_task::edit($this->get_dc(), $update_params, $c2_task_info['nns_id']);
        }
        //如果注入进来的删除命令，而且c2中又没有这个记录，并且中心同步删除指令生成c2虚拟任务开关打开，则生成虚拟c2任务
        else if (empty($c2_task_info) && !is_array($c2_task_info) && $op_queue_info['nns_action'] == NS_CDS_DELETE
            && (int)$this->arr_sp_config['virtual_destroy_task'] === 1)
        {
            $id = np_guid_rand($this->str_sp_id);
            $insert_params = array(
                'nns_id' => $id,
                'nns_type' => $op_queue_info['nns_type'],
                'nns_name' => $op_queue_info['nns_name'],
                'nns_ref_id' => $video_info['base_info']['nns_id'],
                'nns_action' => NS_CDS_DELETE,
                'nns_status' => NS_CDS_CDN_WAIT,
                'nns_create_time' => date('Y-m-d H:i:s'),
                'nns_org_id' => $this->str_sp_id,
                'nns_src_id' => '',
                'nns_clip_task_id' => null,
                'nns_clip_date' => null,
                'nns_op_id' => $op_queue_info['nns_id'],
                'nns_epg_status' => NS_CDS_EPG_WAIT,
                'nns_message_id' => $op_queue_info['nns_message_id'],
                'nns_is_group' => $op_queue_info['nns_is_group'],
                'nns_weight' => $op_queue_info['nns_weight'],
                'nns_cp_id' => $op_queue_info['nns_cp_id'],
            );
            $result = \nl_c2_task::add($this->get_dc(), $insert_params);
        }
        else
        {
            $id = np_guid_rand($this->str_sp_id);
            $insert_params = array(
                'nns_id' => $id,
                'nns_type' => $op_queue_info['nns_type'],
                'nns_name' => $op_queue_info['nns_name'],
                'nns_ref_id' => $video_info['base_info']['nns_id'],
                'nns_action' => NS_CDS_ADD,
                'nns_status' => NS_CDS_CDN_WAIT,
                'nns_create_time' => date('Y-m-d H:i:s'),
                'nns_org_id' => $this->str_sp_id,
                'nns_src_id' => '',
                'nns_clip_task_id' => null,
                'nns_clip_date' => null,
                'nns_op_id' => $op_queue_info['nns_id'],
                'nns_epg_status' => NS_CDS_EPG_WAIT,
                'nns_message_id' => $op_queue_info['nns_message_id'],
                'nns_is_group' => $op_queue_info['nns_is_group'],
                'nns_weight' => $op_queue_info['nns_weight'],
                'nns_cp_id' => $op_queue_info['nns_cp_id'],
            );
            $result = \nl_c2_live_task::add($this->get_dc(), $insert_params);
        }

        if ($result['ret'] == 0)
        {
            $update_op = $this->task_progress($op_queue_info['nns_id']);
            if ($update_op['ret'] == 0)
            {
                return $this->write_log_return('执行中心同步指令注入到c2队列成功', NS_CDS_SUCCE);
            }
            else
            {
                return $this->write_log_return('执行中心同步指令注入到c2队列成功，但上报中心同步指令失败', NS_CDS_SUCCE);
            }
        }
        else
        {
            return $this->write_log_return('执行中心同步指令注入到c2队列失败', NS_CDS_FAIL);
        }
    }

    /**
     * 开始执行epgfile下发
     * @return array
     */
    public function epg_file()
    {
        $epg_file_info = $this->get_epg_file_info();
        $epg_file_type = $this->get_op_queue_type();
        $op_queue_info = $this->op_queue_info;

        $c2_task_arr = $this->get_file_task_info($epg_file_type, $epg_file_info['base_info']['nns_id'], $op_queue_info['nns_org_id']);
        $c2_task_info = $c2_task_arr['data_info'][0];
        if (empty($c2_task_info) || !is_array($c2_task_info))
        {
            //如果注入进来的删除命令，而c2中又没有这个记录  则忽略
            if ((int)$this->arr_sp_config['virtual_destroy_task'] === 0 && $op_queue_info['nns_action'] == NS_CDS_DELETE)
            {
                $re = $this->center_op->report_op_task($op_queue_info['nns_id']);
                return $this->write_log_return($re['reason'], $re['ret']);
            }
        }
        //是否为最终状态
        $is_finally = $this->center_op->is_finally_status($c2_task_info);

        if ($is_finally['ret'] != 0)
        {
            //如果存在另外一个状态不为最终状态的，中心同步指令下发的队列
            if (!empty($c2_task_info) && $op_queue_info['nns_id'] != $c2_task_info['nns_op_id'])
            {
                //如果存在分组标示 上面的数据要一直存在队列中 等待下面的数据处理完毕才下发下去
                if ($op_queue_info['nns_is_group'] != 0)
                {
                    $temp_millisecond = round(np_millisecond_f() * 1000);
                    $op_execute_sql = "update nns_mgtvbk_op_queue set nns_op_mtime='{$temp_millisecond}' where nns_id='{$op_queue_info['nns_id']}'";
                    \nl_op_queue::query_by_sql($this->get_dc(), $op_execute_sql);
                    return $this->write_log_return('存在分组标示 上面的数据要一直存在队列中 等待下面的数据处理完毕才下发下去', NS_CDS_SUCCE);
                }
                $set = "";
                //存在任意一状态为终态，修改状态为等待注入
                if ($c2_task_info['nns_status'] == 7 || $c2_task_info['nns_epg_status'] == 100)
                {
                    $set = ",nns_status=1,nns_epg_status=97";
                }
                $op_execute_sql = "update nns_mgtvbk_c2_task set nns_cp_id='{$op_queue_info['nns_cp_id']}',nns_message_id='{$op_queue_info['nns_message_id']}',nns_name='{$op_queue_info['nns_name']}', nns_op_id='{$op_queue_info['nns_id']}' $set where nns_id='{$c2_task_info['nns_id']}'";
                \nl_op_queue::query_by_sql($this->get_dc(), $op_execute_sql);
                //将上一个中心同步指令队列删除
                $this->center_op->report_op_task($c2_task_info['nns_op_id']);
                $c2_task_info['nns_op_id'] = $op_queue_info['nns_id'];
            }
        }

        //如果存在任务就修改它的动作
        if (is_array($c2_task_info) && !empty($c2_task_info))
        {
            //如果列队过来的是添加动作，而C2任务中的是添加或则修改，则将c2设置为修改
            if (($c2_task_info['nns_action'] == NS_CDS_MODIFY || $c2_task_info['nns_action'] == NS_CDS_ADD)
                && $op_queue_info['nns_action'] == NS_CDS_ADD && ((int)$c2_task_info['nns_status'] != 7
                    || (int)$c2_task_info['nns_epg_status'] != 100))
            {
                if ($is_finally['ret'] == 0)
                {
                    $op_queue_info['nns_action'] = NS_CDS_MODIFY;
                } else
                {
                    $op_queue_info['nns_action'] = NS_CDS_ADD;
                }
            }

            //如果c2中本身是删除，而任务列队中同步的操作是添加或则修改，则按照新增处理
            if ($c2_task_info['nns_action'] == NS_CDS_DELETE && ($op_queue_info['nns_action'] == NS_CDS_MODIFY || $op_queue_info['nns_action'] == NS_CDS_ADD))
            {
                $op_queue_info['nns_action'] = NS_CDS_ADD;
            }

            //如果中心同步指令队列的操作是删除，则将它的所有子级正在执行的状态设置为cancel
            if ($op_queue_info['nns_action'] == NS_CDS_DELETE)
            {
                $this->set_child_cancel($op_queue_info['nns_type'], $c2_task_info['nns_ref_id']);
            }
            //修改状态和动作
            $update_params = array(
                'nns_epg_fail_time' => 0,
                'nns_cdn_fail_time' => 0,
                'nns_cp_id' => $op_queue_info['nns_cp_id'],
                'nns_is_group' => $op_queue_info['nns_is_group'],
                'nns_weight' => $op_queue_info['nns_weight'],
                'nns_message_id' => $op_queue_info['nns_message_id'],
                'nns_status' => NS_CDS_CDN_WAIT,
                'nns_action' => $op_queue_info['nns_action'],
                'nns_modify_time' => date('Y-m-d H:i:s'),
                'nns_op_id' => $op_queue_info['nns_id'],
                'nns_epg_status' => NS_CDS_EPG_WAIT,
                'nns_name' => $op_queue_info['nns_name'],
            );
            $result = \nl_file_task::edit($this->get_dc(), $update_params, $c2_task_info['nns_id']);
        }
        //如果注入进来的删除命令，而且c2中又没有这个记录，并且中心同步删除指令生成c2虚拟任务开关打开，则生成虚拟c2任务
        else if (empty($c2_task_info) && !is_array($c2_task_info) && $op_queue_info['nns_action'] == NS_CDS_DELETE
            && (int)$this->arr_sp_config['virtual_destroy_task'] === 1)
        {
            $id = np_guid_rand($this->str_sp_id);
            $insert_params = array(
                'nns_id' => $id,
                'nns_type' => $op_queue_info['nns_type'],
                'nns_name' => $op_queue_info['nns_name'],
                'nns_ref_id' => $epg_file_info['base_info']['nns_id'],
                'nns_action' => NS_CDS_DELETE,
                'nns_status' => NS_CDS_CDN_WAIT,
                'nns_create_time' => date('Y-m-d H:i:s'),
                'nns_org_id' => $this->str_sp_id,
                'nns_src_id' => $epg_file_info['base_info']['nns_epg_file_set_id'],
                'nns_clip_task_id' => null,
                'nns_clip_date' => null,
                'nns_op_id' => $op_queue_info['nns_id'],
                'nns_epg_status' => NS_CDS_EPG_WAIT,
                'nns_message_id' => $op_queue_info['nns_message_id'],
                'nns_is_group' => $op_queue_info['nns_is_group'],
                'nns_weight' => $op_queue_info['nns_weight'],
                'nns_cp_id' => $op_queue_info['nns_cp_id'],
            );
            $result = \nl_file_task::add($this->get_dc(), $insert_params);
        }
        else
        {
            $id = np_guid_rand($this->str_sp_id);
            $insert_params = array(
                'nns_id' => $id,
                'nns_type' => $op_queue_info['nns_type'],
                'nns_name' => $op_queue_info['nns_name'],
                'nns_ref_id' => $epg_file_info['base_info']['nns_id'],
                'nns_action' => NS_CDS_ADD,
                'nns_status' => NS_CDS_CDN_WAIT,
                'nns_create_time' => date('Y-m-d H:i:s'),
                'nns_org_id' => $this->str_sp_id,
                'nns_src_id' => $epg_file_info['base_info']['nns_epg_file_set_id'],
                'nns_clip_task_id' => null,
                'nns_clip_date' => null,
                'nns_op_id' => $op_queue_info['nns_id'],
                'nns_epg_status' => NS_CDS_EPG_WAIT,
                'nns_message_id' => $op_queue_info['nns_message_id'],
                'nns_is_group' => $op_queue_info['nns_is_group'],
                'nns_weight' => $op_queue_info['nns_weight'],
                'nns_cp_id' => $op_queue_info['nns_cp_id'],
            );
            $result = \nl_file_task::add($this->get_dc(), $insert_params);
        }

        if ($result['ret'] == 0)
        {
            $update_op = $this->task_progress($op_queue_info['nns_id']);
            if ($update_op['ret'] == 0)
            {
                return $this->write_log_return('执行中心同步指令注入到c2队列成功', NS_CDS_SUCCE);
            }
            else
            {
                return $this->write_log_return('执行中心同步指令注入到c2队列成功，但上报中心同步指令失败', NS_CDS_SUCCE);
            }
        }
        else
        {
            return $this->write_log_return('执行中心同步指令注入到c2队列失败', NS_CDS_FAIL);
        }
    }


    /**
     * 注入到c2队列后更新中心同步指令
     * @param $op_queue_id
     * @return array
     */
    public function task_progress($op_queue_id)
    {
        $update_params = array(
            'nns_status' => NS_CDS_OP_LOADING,
        );
        $re_execute = \nl_op_queue::edit($this->get_dc(), $update_params, $op_queue_id);
        if ($re_execute['ret'] == 0)
        {
            return $this->write_log_return('更新中心同步指令队列成功', NS_CDS_SUCCE);
        }
        else
        {
            return $this->write_log_return('更新中心同步指令队列失败', NS_CDS_FAIL);
        }
    }

    /**
     * 根据条件获取点播c2信息
     * @param $video_type
     * @param $video_id
     * @param $sp_id
     * @return array
     */
    public function get_c2_info($video_type, $video_id, $sp_id)
    {
        $params = array(
            'nns_type' => $video_type,
            'nns_ref_id' => $video_id,
            'nns_org_id' => $sp_id,
        );
        return \nl_c2_task::query_by_condition($this->get_dc(), $params, 0, 1, ' order by nns_create_time desc');
    }
    /**
     * 根据条件获取直播c2信息
     * @param $video_type
     * @param $video_id
     * @param $sp_id
     * @return array
     */
    public function get_c2_live_info($video_type, $video_id, $sp_id)
    {
        $params = array(
            'nns_type' => $video_type,
            'nns_ref_id' => $video_id,
            'nns_org_id' => $sp_id,
        );
        return \nl_c2_live_task::query_by_condition($this->get_dc(), $params, 0, 1, ' order by nns_create_time desc');
    }

    /**
     * 根据条件获取file_task信息
     * @param $video_type
     * @param $video_id
     * @param $sp_id
     * @return array
     */
    public function get_file_task_info($video_type, $video_id, $sp_id)
    {
        $params = array(
            'nns_type' => $video_type,
            'nns_ref_id' => $video_id,
            'nns_org_id' => $sp_id,
        );
        return \nl_file_task::query_by_condition($this->get_dc(), $params, 0, 1, ' order by nns_create_time desc');
    }

    /**
     * 取消子集队列
     * @param $video_type
     * @param $nns_video_id
     */
    private function set_child_cancel($video_type, $nns_video_id)
    {
        switch ($video_type)
        {
            case NS_CDS_VIDEO:
                $params = array(
                    'nns_type' => NS_CDS_INDEX,
                    'nns_org_id' => $this->str_sp_id,
                    'nns_src_id' => $nns_video_id,
                );
                $index_list = \nl_c2_task::query_by_condition($this->get_dc(), $params);
                unset($params);
                if ($index_list['ret'] != 0 || !is_array($index_list['data_info']))
                {
                    #to do 记录日志
                    break;
                }
                foreach ($index_list as $index)
                {
                    $this->set_child_cancel(NS_CDS_INDEX, $index['nns_ref_id']);
                    //最终状态判断
                    $is_finally = $this->center_op->is_finally_status($index);

                    //不是最终状态，任务为添加或者修改主媒资，取消该操作任务
                    if ($is_finally['ret'] != 0 && ($index['nns_action'] == NS_CDS_ADD || $index['nns_action'] == NS_CDS_MODIFY))
                    {
                        //删除中心同步指令队列
                        $this->center_op->report_op_task($index['nns_op_id']);
                        //更新c2任务为取消
                        $update_params = array(
                            'nns_status' => 7,
                            'nns_epg_status' => 100
                        );
                        \nl_c2_task::edit($this->get_dc(), $update_params, $index['nns_id']);
                        unset($update_params);
                    }
                }
                break;

            case NS_CDS_INDEX:
                $params = array(
                    'nns_type' => NS_CDS_MEDIA,
                    'nns_org_id' => $this->str_sp_id,
                    'nns_src_id' => $nns_video_id,
                );
                $media_list = \nl_vod_media_v2::query_by_condition($this->get_dc(), $params);
                if ($media_list['ret'] != 0 || !is_array($media_list['data_info']))
                {
                    #to do 记录日志
                    break;
                }
                foreach ($media_list['data_info'] as $media)
                {
                    //最终状态判断
                    $is_finally = $this->center_op->is_finally_status($media);
                    if ($is_finally['ret'] != 0 && ($media['nns_action'] == NS_CDS_ADD || $media['nns_action'] == NS_CDS_MODIFY))
                    {
                        //删除中心同步指令队列
                        $this->center_op->report_op_task($media['nns_op_id']);
                        //更新c2任务为取消
                        $update_params = array(
                            'nns_status' => 7,
                            'nns_epg_status' => 100
                        );
                        \nl_c2_task::edit($this->get_dc(), $update_params, $media['nns_id']);
                        unset($update_params);
                    }
                }
                break;
        }
    }

    /**
     * 变量初始化
     */
    public function destroy_init()
    {
        $this->str_sp_id = '';//SPID
        $this->str_source_id = '';//CPID
        $this->arr_sp_config = array();//SP配置
        $this->arr_source_config = array();//CP配置

        if(isset($this->op_queue_info))
        {
            $this->op_queue_info = array();//中心同步指令队列数据  写一个key=>value获取
        }
        $this->op_queue_info = array();//中心同步指令队列数据  写一个key=>value获取
        $this->video_info = array();//主媒资数据
        $this->index_info = array();//分集数据
        $this->seekpoint_info = array();//分集打点数据
        $this->media_info = array();//片源数据
        $this->live_info = array();//直播
        $this->live_index_info = array();//直播分集
        $this->live_media_info = array();//直播源
        $this->file_info = array();//文件包
        $this->package_info = array();//产品打包
        $this->playbill_info = array();//节目单

        unset($this->center_op);
    }




    public function push_message($sp_id,$message)
    {

    }

    public function pop_message($sp_id,$num = 1)
    {

    }
}
