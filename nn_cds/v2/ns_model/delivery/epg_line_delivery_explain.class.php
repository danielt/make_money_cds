<?php
/**
 * epg媒资上下线分发
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/10/10 15:13
 */

namespace ns_model\delivery;
use Symfony\Component\Yaml\Dumper;

include_once dirname(dirname(dirname(__FILE__))) . "/common.php";
\ns_core\m_load::load("ns_model.m_queue_model");
\ns_core\m_load::load("ns_model.epg_import.epg_import_queue");
\ns_core\m_load::load_old("nn_logic/vod_media/vod_media.class.php");
\ns_core\m_load::load_old("nn_logic/clip_task/clip_task.class.php");
\ns_core\m_load::load_old("nn_logic/op_queue/op_queue.class.php");
\ns_core\m_load::load_old("nn_logic/live/live_media.class.php");
\ns_core\m_load::load_old("nn_logic/file_package/file_package.class.php");
\ns_core\m_load::load_old("nn_logic/clip_task/clip_task_log.class.php");
\ns_core\m_load::load_np('np_http_curl.class.php');
class epg_line_delivery_explain
{
    public $str_sp_id = '';//SPID
    public $str_source_id = '';//CPID
    public $arr_sp_config = array();//SP配置
    public $arr_source_config = array();//CP配置

    private $epg_line_task_info = array();
    private $m_queue_model = array();
    private $video_info = array();//主媒资数据

    public function __construct($sp_id='')
    {
        //创建DC
        \m_config::set_dc();
        if(strlen($sp_id) > 0 && !empty($sp_id))
        {
            $this->str_sp_id = $sp_id;
            $arr_sp_config = \m_config::_get_sp_info($this->str_sp_id);
            $this->arr_sp_config = isset($arr_sp_config['data_info']['nns_config']) ? $arr_sp_config['data_info']['nns_config'] : array();
        }
    }

    /**
     * clip_task_delivery_explain constructor.
     * @param $epg_line_task_info array()
     */
    public function init($epg_line_task_info=array())
    {
        //赋值主要参数
        $this->epg_line_task_info = isset($epg_line_task_info['epg_line_queue_info']) ? $epg_line_task_info['epg_line_queue_info'] : array();
        $this->video_info = isset($epg_line_task_info['video_info']) ? $epg_line_task_info['video_info'] : array();
        //SPID
        if(empty($this->str_sp_id))
        {
            if(isset($epg_line_task_info['nns_org_id']) && !empty($epg_line_task_info['nns_org_id']))
            {
                $this->str_sp_id = $epg_line_task_info['nns_org_id'];
            }
            $arr_sp_config = \m_config::_get_sp_info($this->str_sp_id);
            $this->arr_sp_config = isset($arr_sp_config['data_info']['nns_config']) ? $arr_sp_config['data_info']['nns_config'] : array();
        }

        //CPID
        if(isset($this->epg_line_task_info['nns_cp_id']) && !empty($this->epg_line_task_info['nns_cp_id']))
        {
            $this->str_source_id = $this->epg_line_task_info['nns_cp_id'];
        }
        $arr_source_config = \m_config::_get_cp_info($this->str_source_id);
        $this->arr_source_config = isset($arr_source_config['data_info']['nns_config']) ? $arr_source_config['data_info']['nns_config'] : array();

        $this->m_queue_model = new \ns_model\m_queue_model();
    }


    /**
     * 切片队列注入解释器统一入口
     */
    public function explain()
    {

        $fuc_name = $this->get_epg_line_type();

        if(empty($fuc_name))
        {
            return $this->write_log_return('媒资上下线指令队列类型参数为空,无法注入到切片队列',NS_CDS_FAIL);
        }
        return $this->save_exec_log($this->$fuc_name());
    }

    /**
     * 根据参数记录日志，并返回
     * @param $msg //文字消息
     * @param int $code //状态码，默认成功0
     * @return array
     */
    public function write_log_return($msg, $code=NS_CDS_SUCCE)
    {
        //SPID、C2有值，则存切片队列日志 epg_line_execute_log
        if(!empty($this->str_sp_id) && !empty($this->epg_line_task_info) && !empty($this->get_epg_line_task_info('id')))
        {
            $ret = \m_config::write_epg_line_execute_log($msg, $this->str_sp_id, $this->get_epg_line_task_info('id'));
        }
        else //存入公共日志  global_log
        {
            $ret = \m_config::write_global_execute_log(NS_CDS_EPG_LINE_QUEUE, $this->str_source_id,$this->str_sp_id, $this->get_epg_line_task_info('action'), $this->get_epg_line_task_info(), var_export($msg,true));
        }
        return \m_config::return_data($code, $msg, $ret['data_info']);
    }

    /**
     * 注入EPG的内容写入本地文件并上传至FTP
     * @param $content //写入本地文件的XML
     * @param $ftp_addr //上传FTP的绝对路径
     * @return array(
     *  'ret',
     *  'reason',
     *  'data_info' => array(
     *      'base_dir',//本地相对地址
     *      'absolute_dir',//本地绝对地址
     *      'ftp_url'//上传至FTP地址
     *   )
     * )
     */
    public function save_execute_import_content($content,$ftp_addr='')
    {
        if(empty($content))
        {
            return $this->write_log_return('注入EPG文件内容为空',NS_CDS_FAIL);
        }
        $local_save = \m_config::write_execute_epg_line_content($this->str_sp_id, $this->get_epg_line_type(), $this->get_epg_line_task_info('action'), $content);
        if($local_save['ret'] != NS_CDS_SUCCE)
        {
            return $this->write_log_return($local_save,NS_CDS_FAIL);
        }
        $local_save['data_info']['ftp_url'] = '';
        if(!empty($ftp_addr))
        {
            $up_re = \m_config::up_to_ftp($local_save['data_info']['base_dir'],$local_save['data_info']['absolute_dir'], $ftp_addr);
            if($up_re['ret'] != NS_CDS_SUCCE)
            {
                return $this->write_log_return("本地工单上传FTP失败,FTP为{$ftp_addr},本地地址为:{$local_save['data_info']['base_dir']}|{$local_save['data_info']['absolute_dir']}",NS_CDS_FAIL);
            }
            $local_save['data_info']['ftp_url'] = $up_re['data_info'];
        }
        return $local_save;
    }

    /**
     * 保存C2执行过程日志
     * @param $return_info
     * @return mixe
     */
    public function save_exec_log($return_info)
    {
        if($this->get_epg_line_task_info('id'))
        {
            if (isset($return_info['data_info']['base_dir']) && !empty($return_info['data_info']['base_dir']))
            {
                $exec_log = array("nns_queue_execute_url" => $return_info['data_info']['base_dir']);
                //修改C2任务
                \nl_c2_task::edit($this->get_dc(), $exec_log, $this->get_epg_line_task_info('id'));
            }
            elseif (isset($return_info['error_data_info']['base_dir']) && !empty($return_info['error_data_info']['base_dir']))
            {
                $exec_log = array("nns_queue_execute_url" => $return_info['error_data_info']['base_dir']);
                //修改C2任务
                \nl_c2_task::edit($this->get_dc(), $exec_log, $this->get_epg_line_task_info('id'));
            }
        }
        return $return_info;
    }

    /**
     * 获取上下线队列内容
     * @param string $key
     * @return string
     */
    public function get_epg_line_task_info($key='')
    {
        if(empty($key))
        {
            return '';
        }
        if (strpos($key, 'nns_') === FALSE)
        {
            $key = 'nns_' . $key;
        }
        if(!empty($this->epg_line_task_info) && isset($this->epg_line_task_info[$key]))
        {
            return $this->epg_line_task_info[$key];
        }
        return '';
    }

    /**
     * 获取中心同步指令任务类型
     * @return string
     */
    public function get_epg_line_type()
    {
        if(!empty($this->epg_line_task_info) && isset($this->epg_line_task_info['nns_type']))
        {
            return $this->epg_line_task_info['nns_type'];
        }
        return '';
    }

    /**
     * 获取项目配置文件
     * @param $key
     * @return string
     */
    public function get_project_config($key)
    {
        $project = \evn::get('project');
        return \m_config::get_project_config($project, $key);
    }

    /**
     * 获取DC
     * @return Object
     */
    public function get_dc()
    {
        return \m_config::get_dc();
    }

    /**
     * 获取GUID
     * @param $str
     * @return string
     */
    public function get_guid($str='')
    {
        return np_guid_rand($str);
    }

    /**
     * 获取主媒资数据
     * @return array()
     */
    public function get_video_info()
    {
        return $this->video_info;
    }
    /********************************START********媒资上下线指令到epg注入执行*********START**********************************/

    /**
     * 主媒资上下线
     * @return array
     */
    public function video()
    {
        //媒资上下线注入模式[0|未配置 redis消息队列上下线控制,1 epg注入上下线控制]
        $asset_line_model = (isset($this->arr_sp_config['asset_unline_model']) && $this->arr_sp_config['asset_unline_model'] == '1') ? 1 : 0;
        //EPG上下线下发方式[EPG上下线下发方式 0 ftp 1 http]
        $send_way = (isset($this->arr_sp_config['online_send_way']) && $this->arr_sp_config['online_send_way'] == '1') ? 'http' : 'ftp';
        //第三方媒资上下线关系注入开关[是否属于第三方媒资上下线关系注入  0 默认关闭  1开启]
        $third_category_enabled = (isset($this->arr_sp_config['third_category_enabled']) && $this->arr_sp_config['third_category_enabled'] == '1') ? true : false;
        //是否验证影片属于最终状态 [0 是  1否]
        $check_video_status_enabled = (isset($this->arr_sp_config['check_video_statuc_enabled']) && $this->arr_sp_config['check_video_statuc_enabled'] == '1') ? false : true;
        //注入上下线接口
        $epg_url = $this->arr_sp_config['online_api'];
        $video_info = $this->video_info;

        $line_action = $this->get_epg_line_task_info('action');
        $epg_line_queue = new \ns_model\epg_import\epg_import_queue($this->str_sp_id, NS_CDS_VIDEO);
        if ($check_video_status_enabled && $line_action == NS_CDS_ONLINE)
        {
            $flag_result = $epg_line_queue->is_finally_status($this->get_epg_line_task_info('vod_id'));
            if ($flag_result['ret'] != 0)
            {
                $other_params = null;
                if($flag_result['ret'] == 1)
                {
                    $nns_state = 5;
                }
                else if($flag_result['ret'] == 2)
                {
                    $nns_state = 6;
                }
                else if($flag_result['ret'] == 3)
                {
                    $nns_state = 7;
                }
                else
                {
                    $nns_state = 2;
                    $other_params = 'nns_fail_time=nns_fail_time+1';
                }
                $result = $this->task_progress($nns_state, $other_params);
                if($result['ret'] != 0)
                {
                    $this->write_log_return($result['reason'], NS_CDS_FAIL);
                }
                return $this->write_log_return($flag_result['reason'], NS_CDS_FAIL);
            }
        }
        //获取到媒资的注入id
        $vod_id = $this->get_video_epg_import_id(NS_CDS_VIDEO);

        //根据CP配置判断注入的CP是来源还是原始内容提供商
        //cp检测使用什么cpid
        if(isset($this->arr_source_config['original_cp_enabled']) && (int)$this->arr_source_config['original_cp_enabled'] === 1)
        {
            $new_cp_id = $video_info['nns_producer'];
        }
        elseif(isset($this->arr_source_config['original_cp_enabled']) && (int)$this->arr_source_config['original_cp_enabled'] === 2)
        {
            $new_cp_id = 0;
        }
        else
        {
            $new_cp_id = $this->get_epg_line_task_info('cp_id');
        }

        //注入epg的k23接口
        if($asset_line_model == 1)
        {
            $ex_params =null;
            $assets_category = $this->get_epg_line_task_info('asset_id') . '|' . $this->get_epg_line_task_info('category_id') . '|0|' . $this->get_epg_line_task_info('order');
            $data = array(
                'func' => 'unline_assets',
                'assets_id' => $vod_id,
                'cp_id' => $new_cp_id,
                'assets_unline' => ($line_action == 'unline') ? 1 : 0,
                'assets_id_type' => 0,
                'assets_category' => $assets_category,
            );
            $put_str = json_encode($data);
            $this->write_log_return("通过k23接口上下线注入EPG数据为：" . var_export($data,true));
            $result = $this->import($epg_url, $data);
        }
        else
        {//redis缓存方式注入到nn_cms/api/mgtv/command_category.php接口模式
            //将临时文件存储到data/tmp/xml
            $g_epg_send_way_conf = get_config_v2("g_epg_send_way_conf");
            if (!is_array($g_epg_send_way_conf))
            {
                return $this->write_log_return("上下线配置文件为空或者类型错误" . var_export($g_epg_send_way_conf,true), NS_CDS_FAIL);
            }

            $operation = ($line_action == 'unline') ? 2 : 1;
            $contents  = '<assetcontent>';
            $contents .=    '<assettype>1</assettype>';
            $contents .=    '<operation>' . $operation . '</operation>';
            $contents .=    '<assetdesc>5</assetdesc>';
            $contents .=    '<content>';
            $contents .=        '<extensionfield1>' . $this->get_epg_line_task_info('category_id') . '</extensionfield1>';
            $contents .=        '<oriassetid>'.$vod_id.'</oriassetid>';
            if($third_category_enabled)
            {
                $contents .=    '<categorytype>0</categorytype>';
            }
            $contents .=        '<order>' . $this->get_epg_line_task_info('order') . '</order>';
            $contents .=    '</content>';
            $contents .=    '<info>';


            $contents .=        '<contentProvider>'.$new_cp_id.'</contentProvider>';
            $contents .=    '</info>';
            $contents .= '</assetcontent>';

            $time = \m_config::get_micro_time();
            if($send_way == 'http')
            {
                $save_dir = $this->save_execute_import_content($contents);
                if($save_dir['ret'] != NS_CDS_SUCCE)
                {
                    return $this->write_log_return('写入本地文件失败' . var_export($save_dir, true), NS_CDS_FAIL);
                }
                else
                {
                    $epg_log_info['nns_data'] = $save_dir['data_info']['base_dir'];
                }
                //本地文件存放内容,应该是http文件的内容
                //array(
                //    "http" => "http://127.0.0.3/nn_cds/",
                //    "ftp" => "ftp://dengxinxin:starcor@172.31.14.64:21/",);
                $put_str = '<?xml version="1.0" encoding="utf-8"?><xmlpost><time>'. $time .'</time><msgid>'. $this->get_epg_line_task_info('message_id') .'</msgid><url>'. rtrim($g_epg_send_way_conf['http'],'/') . '/' . $save_dir['data_info']['absolute_dir'] .'</url></xmlpost>';
            }
            else
            {
                //配置文件中ftp配置方式例如为 array('ftp' => 'ftp://123:123@192.168.21.2:21/');
                $save_dir = $this->save_execute_import_content($contents, $g_epg_send_way_conf['ftp']);
                if($save_dir['ret'] != NS_CDS_SUCCE)
                {
                    return $this->write_log_return('写入本地文件失败' . var_export($save_dir, true), NS_CDS_FAIL);
                }
                else
                {
                    $epg_log_info['nns_data'] = $save_dir['data_info']['base_dir'];
                }

                //ftp文件的内容
                $put_str = '<?xml version="1.0" encoding="utf-8"?><xmlpost><time>'. $time .'</time><msgid>'. $this->get_epg_line_task_info('message_id') .'</msgid><url>'. $save_dir['data_info']['ftp_url'] .'</url></xmlpost>';
            }

            $data = array(
                'cmspost' => $put_str,
            );
            $result = $this->import($epg_url, $data);
        }

        if($result['ret'] != NS_CDS_SUCCE)
        {
            $epg_code = NS_CDS_EPG_FAIL;
            $this->write_log_return("注入EPG失败返回：" . $result['reason'],NS_CDS_FAIL);
            $result['@attributes']['ret'] = 1;
            $result['@attributes']['reason'] = $result['reason'];
        }
        else
        {
            if ($asset_line_model == 1)
            {
                $this->write_log_return("注入EPG返回结果为：" . var_export($result['data_info'],true));
                $result = json_decode(json_encode(@simplexml_load_string($result['data_info'])),true);
                $this->write_log_return("注入EPG返回结果解析：" . var_export($result,true));
                if($result['@attributes']['ret'] != NS_CDS_EPG_LINE_NOTIFY_SUCCESS)
                {
                    $epg_code = NS_CDS_EPG_LINE_NOTIFY_FAIL;
                }
                else
                {
                    $epg_code = NS_CDS_EPG_LINE_NOTIFY_SUCCESS;
                }
            }
            else
            {
                $this->write_log_return("注入EPG返回结果为：" . var_export($result['data_info'],true));
                $epg_code =  $result['data_info'] == 1 ? NS_CDS_EPG_LINE_NOTIFY_SUCCESS : NS_CDS_EPG_LINE_NOTIFY_FAIL;
                $result['@attributes']['reason']  =  ($result['data_info'] == 1) ? '注入成功等待反馈' : '注入失败';
            }

        }
        //媒资上下线epg日志数据
        $info_list = array(
            'nns_id' => np_guid_rand(),
            'nns_video_name' => $this->get_epg_line_task_info('video_name'),
            'nns_import_id' => $this->get_epg_line_task_info('import_id'),
            'nns_category_id' => $this->get_epg_line_task_info('category_id'),
            'nns_message_id' => $this->get_epg_line_task_info('message_id'),
            'nns_action' => $this->get_epg_line_task_info('action'),
            'nns_import_content' => $put_str,
            'nns_asset_online_id' => $this->get_epg_line_task_info('id'),
            'nns_reason' => $result['@attributes']['reason'],
            'nns_status' => $epg_code,
        );

        $update_result = array(
            'ret' => NS_CDS_SUCCE,
            'reason' => '注入模型未知',
        );
        if($asset_line_model == 0)
        {
            if($info_list['nns_status'] == 0)
            {
                $update_result = $this->task_progress(NS_CDS_EPG_LINE_LOADING);
            }
            else
            {
                $update_result = $this->task_progress(NS_CDS_EPG_LINE_FAIL);
            }
        }
        elseif($asset_line_model == 1)
        {
            if($info_list['nns_status'] == 0)
            {
                $update_result = $this->task_progress(NS_CDS_EPG_LINE_NOTIFY_SUCCESS);
            }
            else
            {
                $update_result = $this->task_progress(NS_CDS_EPG_LINE_FAIL);
            }
        }
        if ($update_result['ret'] != NS_CDS_SUCCE)
        {
            $this->write_log_return($update_result['reason']);
        }
        //添加上下线日志
        $this->build_epg_line_log($info_list);
        return $this->is_ok($epg_code, $result['@attributes']['reason']);
    }

    /**
     * 注入
     * @param $url //请求地址
     * @param $data //请求数据
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function import($url,$data)
    {
        if(empty($url) || empty($data))
        {
            return $this->write_log_return("注入EPG入参地址或参数错误",NS_CDS_FAIL);
        }
        $curl = new \np_http_curl_class();
        $curl_ret = $curl->post($url,$data);
        $curl_info = $curl->curl_getinfo();
        if($curl_info['http_code'] != '200')
        {
            return $this->write_log_return('EPG注入失败,HTTP状态码'.$curl_info['http_code'],NS_CDS_FAIL);
        }
        $this->write_log_return("CURL成功");
        return \m_config::return_data(NS_CDS_SUCCE,'',$curl_ret);
    }

    /**
     * 创建EPG注入日志
     * @param $epg_line_log_info array(
     *          'nns_video_id' => 视频ID,
     *          'nns_video_type' => 资源类型,
     *          'nns_action' => 行为动作,
     *          'nns_status' => 状态码,
     *          'nns_reason' => 原因,
     *          'nns_data' => 注入数据相对地址,
     *          'nns_name' => 注入名称
     *      )
     * @return array
     */
    public function build_epg_line_log($epg_line_log_info)
    {
        if(!isset($epg_line_log_info['nns_id']) || empty($epg_line_log_info['nns_id']))
        {
            $epg_line_log_info['nns_id'] = $this->get_guid("vod");
        }
        if(!empty($epg_line_log_info['nns_data']))
        {
            $save_dir = $this->save_execute_import_content($epg_line_log_info['nns_data']);
            if($save_dir['ret'] != NS_CDS_SUCCE)
            {
                $epg_line_log_info['nns_data'] = '';
            }
            else
            {
                $epg_line_log_info['nns_data'] = $save_dir['data_info']['base_dir'];
            }
        }
        $epg_line_log_info['nns_org_id'] = $this->str_sp_id;
        return \nl_asset_online_log::add($this->get_dc(), $epg_line_log_info);
    }

    /**
     * 统一EPG注入回调
     * @param $code //详见m_const常量
     * @param $reason //描述
     * @return array
     */
    public function is_ok($code, $reason='')
    {

        $project = \evn::get("project");
        $classname = \ns_core\m_load::load("ns_api.{$project}.message.{$this->str_source_id}.message");

        $obj_feedback = new \message();
        if ($classname)
        {
            $arr_info = array(
                'cdn_id' => '',
                'site_id' => $this->arr_sp_config['site_id'],
                'mg_asset_type' => 1,
                'mg_asset_id' => $this->get_video_epg_import_id(NS_CDS_VIDEO),
                'mg_part_id' => '',
                'mg_file_id' => '',
                'is_finally' => 0,
            );
            //兼容湖南电信二级消息反馈，1表示成功，0表示失败。
            //$code = $state === 1 ? 0 : 1;
            $obj_feedback->is_ok($this->get_epg_line_task_info('message_id'), $code, $reason, $arr_info, $this->str_sp_id);
            unset($obj_feedback);
        }
        else
        {
            $this->write_log_return('引入消息反馈文件失败');
        }
        return $this->write_log_return($reason);
    }

    /**
     * 获取EPG注入ID的模式
     * @param $video_type //资源类型，未传使用当前资源类型，传入时使用传入的类型
     * @return string
     */
    public function get_video_epg_import_id($video_type='')
    {
        if(empty($video_type))
        {
            $video_type = $this->get_epg_line_type();
            if(empty($video_type))
            {
                return '';
            }
        }

        $key = "get_" . $video_type . "_info";
        if(!method_exists($this,$key))
        {
            return '';
        }
        $mixed_info = $this->$key();
        $query_info = array($video_type=>$mixed_info);
        $cdn_import = $this->m_queue_model->_get_cdn_epg_import_id($this->str_sp_id,$video_type,$query_info,'epg',null);

        if ($cdn_import['ret'] != '0')
        {
            return '';
        }
        return $cdn_import['data_info'];
    }

    /**
     * 查询是否存在该切片队列
     * @param $params
     * @return array
     */
    public function get_exists_task_info($params)
    {
        $sql = "select * from nns_mgtvbk_clip_task " .
            "where nns_org_id='".$params['nns_org_id']."'" .
            " and   nns_video_type='".$params['nns_video_type']."'" .
            " and nns_video_id='".$params['nns_video_id']."' " .
            " and nns_video_index_id='".$params['nns_video_index_id']."'" .
            " and nns_video_media_id='".$params['nns_video_media_id']."'" .
            "  and nns_state!='".NS_CDS_CANCEL."'";
        return \nl_clip_task::query_by_sql($this->get_dc(), $sql);
    }

    /**
     * 注入到后更新上下线指令
     * @param $status
     * @param $other_params
     * @return array
     */
    public function task_progress($status, $other_params = '')
    {
        return \nl_asset_online::update($this->get_dc(), array('nns_status' => $status), array('nns_id' => $this->get_epg_line_task_info('id')), $other_params);
    }

    /**
     * 根据条件获取直播片源信息
     * @param $params array
     * @return array
     */
    public function get_live_media_info($params)
    {
        \ns_core\m_load::load_old("nn_logic.live.live_media.class.php");
        $sql = "select * from nns_live_media" .
            " where nns_id='{$params['nns_video_media_id']}'" .
            " and  nns_deleted != 1 and  nns_vod_id='{$params['nns_video_id']}'" .
            " and nns_vod_index_id='{$params['nns_video_index_id']}'";
        return \nl_live_media::query_by_sql($this->get_dc(), $sql);
    }

    /**
     * 获取片源数据
     * @param $params
     * @return array
     */
    public function get_media_info($params)
    {
        $sql = "select * from nns_vod_media" .
            " where nns_id='{$params['nns_video_media_id']}'" .
            " and  nns_deleted != 1 and  nns_vod_id='{$params['nns_video_id']}'" .
            " and nns_vod_index_id='{$params['nns_video_index_id']}'";
        return \nl_vod_media_v2::query_by_sql($this->get_dc(), $sql);
    }

    /**
     * 获取文件包信息
     * @param $params
     * @return array
     */
    public function get_file_info($params)
    {
        $sql = "select * from nns_file_package" .
            " where nns_id='{$params['nns_video_id']}'" .
            " and  nns_deleted != 1 ";
        return \nl_file_package::query_by_sql($this->get_dc(), $sql);
    }


    /**
     * 变量初始化
     */
    public function destroy_init()
    {
        $this->str_sp_id = '';//SPID
        $this->str_source_id = '';//CPID
        $this->arr_sp_config = array();//SP配置
        $this->arr_source_config = array();//CP配置

        if(isset($this->epg_line_task_info))
        {
            $this->epg_line_task_info = array();//中心同步指令队列注入到切片队列数据  写一个key=>value获取
        }
    }


    public function build_clip_log($clip_log_info)
    {
        if(!isset($clip_log_info['nns_id']) || empty($clip_log_info['nns_id']))
        {
            $clip_log_info['nns_id'] = np_guid_rand();
        }
        $task_log['nns_video_type'] = $clip_log_info['nns_video_type'];
        $clip_log_info['nns_create_time'] = date('Y-m-d H:i:s');
        $clip_log_info['nns_create_mic_time'] = microtime(true)*10000;
        return \nl_clip_task_log::add($this->get_dc(), $clip_log_info);
    }

    public function push_message($sp_id,$message)
    {

    }

    public function pop_message($sp_id,$num = 1)
    {

    }


}
