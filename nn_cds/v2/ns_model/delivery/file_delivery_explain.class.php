<?php
/**
 * file到cdn统一分发器
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/11/12 11:30
 */

namespace ns_model\delivery;
\ns_core\m_load::load_old("nn_logic/file_task/file_task.class.php");
\ns_core\m_load::load_old("nn_logic/file_task_log/file_task_log.class.php");
\ns_core\m_load::load("ns_data_model.category.m_category_inout");
\ns_core\m_load::load("ns_model.m_queue_model");
\ns_core\m_load::load("ns_model.center_op.center_op_queue");
\ns_core\m_load::load_old("nn_logic/epg_file/epg_file.class.php");
\ns_core\m_load::load_old("nn_logic/epg_file/epg_file_set.class.php");
class file_delivery_explain
{
    public $str_sp_id = '';//SPID
    public $str_source_id = '';//CPID
    public $arr_sp_config = array();//SP配置
    public $arr_source_config = array();//CP配置

    private $m_queue_model = array();
    protected $c2_info = array();//C2队列数据  写一个key=>value获取
    protected $video_info = array();//主媒资数据
    protected $index_info = array();//分集数据
    protected $seekpoint_info = array();//分集打点数据
    protected $media_info = array();//片源数据
    private $live_info = array();//直播
    private $live_index_info = array();//直播分集
    private $live_media_info = array();//直播源
    private $file_info = array();//文件包
    private $package_info = array();//产品打包
    private $playbill_info = array();//节目单

    private $epg_file_set_info = array();//epg_file_set
    private $epg_file_info = array();//epg_file



    public function __construct($sp_id='')
    {
        //创建DC
        \m_config::set_dc();
        if(strlen($sp_id) > 0 && !empty($sp_id))
        {
            $this->str_sp_id = $sp_id;
            $arr_sp_config = \m_config::_get_sp_info($this->str_sp_id);
            $this->arr_sp_config = isset($arr_sp_config['data_info']['nns_config']) ? $arr_sp_config['data_info']['nns_config'] : array();
        }

        //初始化底层队列运行方法
        $this->m_queue_model = new \ns_model\m_queue_model();
    }
    /**
     * cdn_delivery_explain constructor.
     * @param $c2_queue_info array(
     *                      'c2_info'=>array(C2信息),
     *                      'video_info'=>array(
     *                          'base_info' => VIDEO信息
     *                          'ex_info'  => 扩展信息
     *                       ),
     *                      'index_info'=>array(
     *                          'base_info' => INDEX信息,
     *                          'ex_info' => 扩展信息,
     *                      ),
     *                      'seekpoint_info'=>array(
     *                          'base_info' => 分集打点信息,
     *                          'ex_info' => 扩展信息,
     *                      ),
     *                      'media_info'=>array(
     *                          'base_info' => MEDIA信息
     *                          'ex_info' => 扩展信息
     *                      ),
     *                      'live_info'=>array(
     *                          'base_info' => MEDIA信息
     *                          'ex_info' => 扩展信息
     *                      ),
     *                      'live_index_info'=>array(
     *                          'base_info' => MEDIA信息
     *                          'ex_info' => 扩展信息
     *                      ),
     *                  )
     */
    public function init($c2_queue_info=array())
    {

        //赋值主要参数
        $this->c2_info = isset($c2_queue_info['file_task_info']) ? $c2_queue_info['file_task_info'] : array();

        $this->file_info = isset($c2_queue_info['file_info']) ? $c2_queue_info['file_info'] : array();
        $this->package_info = isset($c2_queue_info['package_info']) ? $c2_queue_info['package_info'] : array();

        $this->epg_file_set_info = isset($c2_queue_info['epg_file_set_info']) ? $c2_queue_info['epg_file_set_info'] : array();
        $this->epg_file_info = isset($c2_queue_info['epg_file_info']) ? $c2_queue_info['epg_file_info'] : array();
        //SPID
        if(empty($this->str_sp_id))
        {
            if(isset($c2_queue_info['c2_info']) && !empty($c2_queue_info['c2_info']['nns_org_id']))
            {
                $this->str_sp_id = $c2_queue_info['c2_info']['nns_org_id'];
            }
            $arr_sp_config = \m_config::_get_sp_info($this->str_sp_id);
            $this->arr_sp_config = isset($arr_sp_config['data_info']['nns_config']) ? $arr_sp_config['data_info']['nns_config'] : array();
        }

        //CPID
        if(isset($c2_queue_info['c2_info']) && !empty($c2_queue_info['c2_info']['nns_cp_id']))
        {
            $this->str_source_id = $c2_queue_info['c2_info']['nns_cp_id'];
        }
        $arr_source_config = \m_config::_get_cp_info($this->str_source_id);
        $this->arr_source_config = isset($arr_source_config['data_info']['nns_config']) ? $arr_source_config['data_info']['nns_config'] : array();
    }

    /**
     * 获取DC
     * @return Object
     */
    public function get_dc()
    {
        return \m_config::get_dc();
    }

    /**
     * 获取epgfileset数据
     * @return array()
     */
    public function get_epg_file_set_info()
    {
        return $this->epg_file_set_info;
    }
    /**
     * 获取epgfile 模板文件数据
     * @return array()
     */
    public function get_epg_file_info()
    {
        return $this->epg_file_info;
    }


    /**
     * 获取文件包信息
     * @return array()
     */
    public function get_file_info()
    {
        return $this->file_info;
    }
    /**
     * 获取产品包信息
     * @return array()
     */
    public function get_package_info()
    {
        return $this->package_info;
    }

    /**
     * 获取C2任务类型
     * @return string
     */
    public function get_c2_type()
    {
        if(!empty($this->c2_info) && isset($this->c2_info['nns_type']))
        {
            return $this->c2_info['nns_type'];
        }
        return '';
    }

    /**
     * 根据键获取C2内容
     * @param string $key
     * @return mixed|string
     */
    public function get_c2_info($key='')
    {
        if(empty($key))
        {
            return '';
        }
        if (strpos($key, 'nns_') === FALSE)
        {
            $key = 'nns_' . $key;
        }
        if(!empty($this->c2_info) && isset($this->c2_info[$key]))
        {
            return $this->c2_info[$key];
        }
        return '';
    }

    /**
     * 获取注入平台资源库栏目信息
     * @param $is_vod //属于点播，默认
     * @return array
     */
    public function get_category_info($is_vod=true)
    {
        $m_category_inout = new \m_category_inout();
        if($is_vod)
        {
            $str_video_type = 0;
        }
        else
        {
            $str_video_type = 1;
        }
        return $m_category_inout->query(array('base_info'=>array('nns_video_type' => $str_video_type)));
    }

    /**
     * 获取CDN注入ID的模式
     * @param $video_type //C2资源类型,传入了就是用，未传就使用当前类型
     * @return string
     */
    public function get_video_cdn_import_id($video_type='')
    {
        if(empty($video_type))
        {
            $video_type = $this->get_c2_type();
            if(empty($video_type))
            {
                return '';
            }
        }

        $key = "get_" . $video_type . "_info";

        if(!method_exists($this,$key))
        {
            return '';
        }
        $mixed_info = $this->$key();
        $query_info = array($video_type=>$mixed_info);

        $cdn_import = $this->m_queue_model->_get_cdn_epg_import_id($this->str_sp_id,$video_type,$query_info,'cdn',null);

        if ($cdn_import['ret'] != '0')
        {
            return '';
        }

        return $cdn_import['data_info'];
    }

    /**
     * 获取GUID
     * @return string
     */
    public function get_guid()
    {
        return np_guid_rand();
    }

    /**
     * 根据条件获取片源信息
     * @param $params
     * @return array
     */
    public function get_media_info_by_condition($params)
    {
        $params['nns_deleted'] = 0;
        //获取片源信息
        return \nl_vod_media_v2::query_by_condition($this->get_dc(), $params);
    }


    public function edit_media_info($params, $media_id)
    {
        //修改片源信息
        $edit_media_result = \nl_vod_media_v2::edit($this->get_dc(), $params, $media_id);
        if($edit_media_result['ret'] != NS_CDS_SUCCE)
        {
            return $this->write_log_return($edit_media_result['reason'],$edit_media_result['ret']);
        }
        return $edit_media_result;
    }

    /**
     * 注入CDN的内容写入本地文件并上传至FTP
     * @param $content //写入本地文件的XML
     * @param $ftp_addr //上传FTP的绝对路径
     * @return array(
     *  'ret',
     *  'reason',
     *  'data_info' => array(
     *      'base_dir',//本地相对地址
     *      'absolute_dir',//本地绝对地址
     *      'ftp_url'//上传至FTP地址
     *   )
     * )
     */
    public function save_execute_import_content($content,$ftp_addr='')
    {
        if(empty($content))
        {
            return $this->write_log_return('注入CDN文件内容为空',NS_CDS_FAIL);
        }
        $local_save = \m_config::write_execute_cdn_import_content($this->str_sp_id,$this->get_c2_type(),$this->get_c2_info('action'),$content);
        if($local_save['ret'] != NS_CDS_SUCCE)
        {
            return $this->write_log_return('本地生成注入CDN的工单XML失败',NS_CDS_FAIL);
        }
        $local_save['data_info']['ftp_url'] = '';
        if(!empty($ftp_addr))
        {
            $up_re = \m_config::up_to_ftp($local_save['data_info']['base_dir'],$local_save['data_info']['absolute_dir'],$ftp_addr);
            if($up_re['ret'] != NS_CDS_SUCCE)
            {
                return $this->write_log_return("本地工单上传FTP失败,FTP为{$ftp_addr},本地地址为:{$local_save['data_info']['base_dir']}|{$local_save['data_info']['absolute_dir']}" . var_export($up_re, true),NS_CDS_FAIL);
            }
            $local_save['data_info']['ftp_url'] = $up_re['data_info'];
        }
        return $local_save;
    }

    /**
     * 注入CDN的内容写入本地文件并上传至FTP V2，比上面的第一版本功能增加【可自由设置上传的FTP目录，文件名】
     * @param $content  //写入本地文件的XML内容
     * @param $ftp_addr //上传FTP地址，如ftp://user:password@ip:port
     * @param $is_ftp_root bool 是否上传至FTP的根目录，true时，以下$ftp_path无作用
     * @param $ftp_path //FTP上的目录【相对FTP根目录的绝对路径】，ftp地址传入了才起作用
     * @param $ftp_file_name //FTP上传的文件命名
     * @return array(
     *  'ret',
     *  'reason',
     *  'data_info' => array(
     *      'base_dir',//本地相对地址
     *      'absolute_dir',//本地绝对地址
     *      'ftp_url'//上传至FTP地址
     *   )
     * )
     */
    public function save_execute_import_content_v2($content, $ftp_addr = '', $is_ftp_root = false, $ftp_path = '', $ftp_file_name = '')
    {
        if(empty($content))
        {
            return $this->write_log_return('注入CDN文件内容为空',NS_CDS_FAIL);
        }
        $local_save = \m_config::write_execute_cdn_import_content($this->str_sp_id,$this->get_c2_type(),$this->get_c2_info('action'),$content);
        if($local_save['ret'] != NS_CDS_SUCCE)
        {
            return $this->write_log_return('本地生成注入CDN的工单XML失败',NS_CDS_FAIL);
        }
        $local_save['data_info']['ftp_url'] = '';
        if(!empty($ftp_addr))
        {
            if(!$is_ftp_root) //不上传至FTP根目录，采用本地文件路径或传入的路径参数
            {
                if(empty($ftp_path))//未指定FTP文件存放目录，采用本地文件路径
                {
                    $path_arr = pathinfo($local_save['data_info']['base_dir']);
                    $ftp_path = $path_arr['dirname'];
                }
            }
            else
            {
                $ftp_path = '';
            }
            if(empty($ftp_file_name)) //未指定上传FTP文件的名称，采用本地文件名
            {
                $path_arr = pathinfo($local_save['data_info']['base_dir']);
                $ftp_file_name = $path_arr['basename'];
            }
            $up_re = \m_config::up_to_ftp_v2($local_save['data_info']['absolute_dir'],$ftp_addr,$ftp_file_name,$ftp_path);
            if($up_re['ret'] != NS_CDS_SUCCE)
            {
                return $this->write_log_return($up_re['reason'],NS_CDS_FAIL);
            }
            $local_save['data_info']['ftp_url'] = $up_re['data_info'];
        }
        return $local_save;
    }

    /**
     * 创建CDN注入日志
     * @parma $c2_log_info array(
     *            'nns_task_type'=>'Series',
     *            'nns_task_id'=> 'C2任務ID',
     *            'nns_task_name'=> '任務名稱',
     *            'nns_action'=> '行為動作',
     *            'nns_result' => 0 成功 其他失敗
     *            'nns_result_fail_reason' => 發送失敗原因描述
     *      )
     * @return array
     */
    public function build_c2_log($c2_log_info)
    {
        if(!isset($c2_log_info['nns_id']) || empty($c2_log_info['nns_id']))
        {
            $c2_log_info['nns_id'] = np_guid_rand();
        }

        $c2_log_info['nns_type'] = 'std';
        $c2_log_info['nns_org_id'] = $this->str_sp_id;
        $c2_log_info['nns_desc'] = $c2_log_info['nns_task_type'].",".$c2_log_info['nns_action'];
        $c2_log_info['nns_result'] = "[{$c2_log_info['nns_result']}]";
        if((isset($c2_log_info['nns_result_fail_reason']) || !empty($c2_log_info['nns_result_fail_reason'])) && $c2_log_info['nns_result'] != '0')
        {
            $c2_log_info['nns_result'] .= $c2_log_info['nns_result_fail_reason'];
        }
        //去掉nns_content内容
        unset($c2_log_info['nns_content']);
        return \nl_file_task_log::add($this->get_dc(),$c2_log_info);
    }

    /**
     * 获取ftp内容
     * @param $ftp_url
     * @return bool|string
     */
    public function save_ftp_content($ftp_url)
    {
        $arr_url = parse_url($ftp_url);
        $path = $arr_url['path'];
        $save_path = dirname(__FILE__). '/' . basename($path);
        $save_re = \m_config::get_curl_content_and_save($ftp_url,$save_path);
        if($save_re['ret'] != NS_CDS_SUCCE)
        {
            $this->write_log_return('文件失败原因' . var_export($save_re, true));
            return '';
        }
        $contents = @file_get_contents($save_path);
        unlink($save_path);
        return $contents;
    }

    /**
     * 统一c2回调
     * @param $code 0成功 5正在注入 详见m_const常量
     * @param string $reason 描述
     * @return array
     */
    public function is_ok($code, $reason='')
    {
        $c2_info = $this->c2_info;
        $cdn_fail_time = $c2_info['nns_cdn_fail_time'];
        //根据注入反馈的状态将task的状态设置为失败或成功
        switch ($code)
        {
            case NS_CDS_CDN_LOADING: //正在注入
                $c2_task_info = array('nns_status' => NS_CDS_CDN_LOADING);
                $reason = '正在注入cdn';
                break;
            case NS_CDS_CDN_FAIL://注入失败
                $cdn_fail_time = (int)$cdn_fail_time+1;
                $c2_task_info = array(
                    'nns_status' => NS_CDS_CDN_FAIL,
                    'nns_cdn_fail_time' => $cdn_fail_time,
                );
                $reason = '注入cdn失败';
                break;
            case NS_CDS_CDN_SUCCE://注入成功
                $c2_task_info = array('nns_status' => NS_CDS_CDN_SUCCE);
                $reason = '注入成功';
                break;
            case NS_CDS_CDN_NOTIFY_FAIL_1://未知错误
                $cdn_fail_time = (int)$cdn_fail_time+1;
                $c2_task_info = array(
                    'nns_status' => NS_CDS_CDN_FAIL,
                    'nns_cdn_fail_time' => $cdn_fail_time,
                );
                $reason = '注入cdn失败,未知错误';
                break;
            case NS_CDS_CDN_NOTIFY_FAIL_2://无法获取模板同步指令
                $cdn_fail_time = (int)$cdn_fail_time+1;
                $c2_task_info = array(
                    'nns_status' => NS_CDS_CDN_FAIL,
                    'nns_cdn_fail_time' => $cdn_fail_time,
                );
                $reason = '注入cdn失败,无法获取模板同步指令';
                break;
            case NS_CDS_CDN_NOTIFY_FAIL_3://指令文件无法正确解析
                $cdn_fail_time = (int)$cdn_fail_time+1;
                $c2_task_info = array(
                    'nns_status' => NS_CDS_CDN_FAIL,
                    'nns_cdn_fail_time' => $cdn_fail_time,
                );
                $reason = '注入cdn失败,指令文件无法正确解析';
                break;
            case NS_CDS_CDN_NOTIFY_SUCCE://CDN回调成功
                $c2_task_info = array('nns_status' => NS_CDS_CDN_SUCCE);
                $reason = '回调成功.' . $reason;
                break;
            default:
                $cdn_fail_time = (int)$cdn_fail_time+1;
                $c2_task_info = array(
                    'nns_status' => NS_CDS_CDN_FAIL,
                    'nns_cdn_fail_time' => $cdn_fail_time,
                );
                $reason = '注入cdn失败';
                break;
        }
        //修改C2任务
        $c2_re = \nl_file_task::edit($this->get_dc(), $c2_task_info, $c2_info['nns_id']);
        if($c2_re['ret'] != NS_CDS_SUCCE)
        {
            return $this->write_log_return($c2_re['reason'], $c2_re['ret']);
        }
        $c2_info['nns_status'] = $c2_task_info['nns_status'];
        //判断任务是否属于最终状态--在中间层进行最终状态判断和中心同步指令得删除
        //$center_op = new \ns_model\center_op\center_op_queue();
        //$finally_re = $center_op->is_finally_status($c2_info);
        ////判断是最终状态，则删除中心同步指令
        //if ($finally_re['ret'] == 0 && isset($c2_info['nns_op_id']) && strlen($c2_info['nns_op_id']) > 0)
        //{
        //    $re_report = $center_op->report_op_task($c2_info['nns_op_id']);
        //    $this->write_log_return('中兴同步指令队列执行删除:' . $re_report['reason'], $re_report['ret']);
        //}
        //unset($center_op);

        //获取注入cdn的id，xinxin.deng 2018/2/26 14:27
        $cdn_id = $this->get_video_cdn_import_id($c2_info['nns_type']);

        //得到返回数据值
        $mg_asset_type = '';
        $mg_asset_id = '';
        $mg_part_id = '';
        $mg_file_id = '';

        switch ($c2_info['nns_type'])
        {
            case NS_CDS_VIDEO:
                $mg_asset_type = 1;
                $mg_asset_id = $this->video_info['base_info']['nns_asset_import_id'];
                break;
            case NS_CDS_INDEX:
                $mg_asset_type = 2;
                $mg_part_id = $this->index_info['base_info']['nns_import_id'];
                $mg_asset_id = $this->video_info['base_info']['nns_asset_import_id'];
                break;
            case NS_CDS_MEDIA:
                $mg_asset_type = 3;
                $mg_file_id = $this->media_info['base_info']['nns_import_id'];
                $mg_part_id = $this->index_info['base_info']['nns_import_id'];
                $mg_asset_id = $this->video_info['base_info']['nns_asset_import_id'];
                break;
            default:
                break;
        }

        //封装成统一样式数组
        $data = array(
            'cdn_id' => $cdn_id,
            'site_id' => $this->arr_sp_config['site_id'],
            'mg_asset_type' => $mg_asset_type,
            'mg_asset_id' => $mg_asset_id,
            'mg_part_id' => $mg_part_id,
            'mg_file_id' => $mg_file_id,
            'EPGAddr' => $c2_info['nns_addr'],
        );

        \ns_core\m_msg_callback::is_ok($c2_info['nns_cp_id'], $c2_info['nns_message_id'], $code, '[cdn注入]' . $reason, $data, $c2_info);

        return $this->write_log_return($reason);
    }

    /**
     * CDN异步回调注入状态
     * @param $c2_correlate_id //c2注入CDN交互信令唯一ID
     * @param $code //详见m_const常量
     * @param string $reason 描述
     * @param string $notify_result CDN反馈的原始数据
     * @return array
     */
    public function is_notify_ok($c2_correlate_id, $code, $reason, $notify_result='')
    {
        if(empty($c2_correlate_id))
        {
            return $this->write_log_return("CDN回调未传递工单唯一表标识ID",NS_CDS_FAIL);
        }
        //根据信令ID获取C2任务ID
        $c2_log = \nl_file_task_log::query_by_condition($this->get_dc(),array("nns_id" => $c2_correlate_id));
        if($c2_log['ret'] != NS_CDS_SUCCE)
        {
            return $this->write_log_return($c2_log['reason'], $c2_log['ret']);
        }
        $notify_url = '';
        if(!empty($notify_result))
        {
            $log_data = \m_config::write_execute_cdn_notify_content($c2_log['data_info'][0]['nns_org_id'],$c2_correlate_id,$notify_result);
            $notify_url = $log_data['data_info']['base_dir'];
        }

        $time = date("Y-m-d H:i:s");

        //根据注入反馈的状态将file_log的状态设置为失败或成功
        switch ($code)
        {
            case NS_CDS_CDN_NOTIFY_SUCCE:
                $c2_log_info = array(
                    'nns_notify_result' => NS_CDS_CDN_NOTIFY_SUCCE,
                    'nns_notify_result_url' => $notify_url,
                    'nns_notify_time' => $time,
                );
                break;
            default:
                $c2_log_info = array(
                    'nns_notify_result' => $code,
                    'nns_notify_fail_reason' => $reason,
                    'nns_notify_result_url' => $notify_url,
                    'nns_notify_time' => $time,
                );
                break;
        }
        //修改nns_mgtvbk_file_log
        $result = \nl_file_task_log::edit($this->get_dc(), $c2_log_info, $c2_correlate_id);
        if($result['ret'] != NS_CDS_SUCCE)
        {
            return $this->write_log_return($result['reason'],$result['ret']);
        }
        //获取C2任务信息
        $c2_task_id = $c2_log['data_info'][0]['nns_task_id'];
        $c2_info = \nl_file_task::query_by_condition($this->get_dc(),array("nns_id" => $c2_task_id));
        if($c2_info['ret'] != NS_CDS_SUCCE)
        {
            return $this->write_log_return($c2_info['reason'],$c2_info['ret']);
        }
        $c2_info = $c2_info['data_info'][0];

        //定制化特殊赋值,目标地址--xinxin.deng 2018/11/13 15:13
        $c2_info['nns_addr'] = json_decode($c2_log['data_info'][0]['nns_ex_info'], true);
        
        $this->c2_info = $c2_info;

        /**********获取到媒资信息***********/
        switch ($this->c2_info['nns_type'])
        {
            case NS_CDS_VIDEO:
                $re_vod_info = \nl_vod::query_by_id($this->get_dc(), $c2_info['nns_ref_id']);
                $this->video_info['base_info'] = $re_vod_info['data_info'];
                break;
            case NS_CDS_INDEX:
                $re_index_info = \nl_vod_index::query_by_id($this->get_dc(), $c2_info['nns_ref_id']);
                $this->index_info['base_info'] = $re_index_info['data_info'];
                $re_vod_info = \nl_vod::query_by_id($this->get_dc(), $re_index_info['data_info']['nns_vod_id']);
                $this->video_info['base_info'] = $re_vod_info['data_info'];
                break;
            case NS_CDS_MEDIA:
                $re_media_info = \nl_vod_media::query_by_id($this->get_dc(), $c2_info['nns_ref_id']);
                $this->media_info['base_info'] = $re_media_info['data_info'];
                $re_index_info = \nl_vod_index::query_by_id($this->get_dc(), $re_media_info['data_info']['nns_vod_index_id']);
                $this->index_info['base_info'] = $re_index_info['data_info'];
                $re_vod_info = \nl_vod::query_by_id($this->get_dc(), $re_media_info['data_info']['nns_vod_id']);
                $this->video_info['base_info'] = $re_vod_info['data_info'];
                break;
            default:
                break;
        }

        return $this->is_ok($code,$reason);
    }

    /**
     * c2条件修改c2_log
     * @param $c2_params
     * @param $code
     * @param $reason
     * @param string $notify_result
     * @return array
     */
    public function is_notify_ok_v2($c2_params,  $code, $reason, $notify_result='')
    {
        if (empty($c2_params) || !is_array($c2_params))
        {
            return $this->write_log_return("c2_task信息为空", NS_CDS_FAIL);
        }

        $c2_info = \nl_c2_task::query_by_condition($this->get_dc(), $c2_params, '', '', 'order by nns_modify_time DESC');

        if($c2_info['ret'] != NS_CDS_SUCCE)
        {
            return $this->write_log_return($c2_info['reason'], $c2_info['ret']);
        }
        $c2_info_ar = $c2_info['data_info'][0];
        unset($c2_info);
        //根据task_id获取C2任务ID
        $c2_log = \nl_c2_log::query_by_condition($this->get_dc(),array('nns_task_id' => $c2_info_ar['nns_id']), 0, 0, 'order by nns_create_time DESC');
        if($c2_log['ret'] != NS_CDS_SUCCE)
        {
            return $this->write_log_return($c2_log['reason'],$c2_log['ret']);
        }
        $notify_url = '';
        if(!empty($notify_result))
        {
            $log_data = \m_config::write_execute_cdn_notify_content($c2_log['data_info'][0]['nns_org_id'], $c2_info_ar['nns_id'], $notify_result);
            $notify_url = $log_data['data_info']['base_dir'];
        }

        $time = date("Y-m-d H:i:s");

        //根据注入反馈的状态将c2_log的状态设置为失败或成功
        switch ($code)
        {
            case NS_CDS_CDN_NOTIFY_SUCCE:
                $c2_log_info = array(
                    'nns_notify_result' => NS_CDS_CDN_NOTIFY_SUCCE,
                    'nns_notify_result_url' => $notify_url,
                    'nns_notify_fail_reason' => $reason,
                    'nns_notify_time' => $time,
                );
                break;
            default:
                $c2_log_info = array(
                    'nns_notify_result' => $code,
                    'nns_notify_fail_reason' => $reason,
                    'nns_notify_result_url' => $notify_url,
                    'nns_notify_time' => $time,
                );
                break;
        }
        //修改C2log
        $result = \nl_c2_log::edit($this->get_dc(), $c2_log_info, $c2_log['data_info'][0]['nns_id']);
        if($result['ret'] != NS_CDS_SUCCE)
        {
            return $this->write_log_return($result['reason'], $result['ret']);
        }

        $this->c2_info = $c2_info_ar;

        /**********获取到媒资信息***********/
        switch ($this->c2_info['nns_type'])
        {
            case NS_CDS_VIDEO:
                $re_vod_info = \nl_vod::query_by_id($this->get_dc(), $this->c2_info['nns_ref_id']);
                $this->video_info['base_info'] = $re_vod_info['data_info'];
                break;
            case NS_CDS_INDEX:
                $re_index_info = \nl_vod_index::query_by_id($this->get_dc(), $this->c2_info['nns_ref_id']);
                $this->index_info['base_info'] = $re_index_info['data_info'];
                $re_vod_info = \nl_vod::query_by_id($this->get_dc(), $re_index_info['data_info']['nns_vod_id']);
                $this->video_info['base_info'] = $re_vod_info['data_info'];
                break;
            case NS_CDS_MEDIA:
                $re_media_info = \nl_vod_media::query_by_id($this->get_dc(), $this->c2_info['nns_ref_id']);
                $this->media_info['base_info'] = $re_media_info['data_info'];
                $re_index_info = \nl_vod_index::query_by_id($this->get_dc(), $re_media_info['data_info']['nns_vod_index_id']);
                $this->index_info['base_info'] = $re_index_info['data_info'];
                $re_vod_info = \nl_vod::query_by_id($this->get_dc(), $re_media_info['data_info']['nns_vod_id']);
                $this->video_info['base_info'] = $re_vod_info['data_info'];
                break;
            default:
                break;
        }

        return $this->is_ok($code,$reason);
    }

    /**
     * 根据参数记录日志，并返回
     * @param string $msg 文字消息
     * @param int $code 状态码，默认成功0
     * @return array()
     */
    public function write_log_return($msg, $code=NS_CDS_SUCCE)
    {
        //来源ID、SPID、C2有值，则存CDN队列日志 queue_log
        if(!empty($this->get_c2_info('org_id')) && !empty($this->c2_info) && !empty($this->get_c2_info('id')))
        {
            $ret = \m_config::write_cdn_execute_log($msg, $this->get_c2_info('org_id'), $this->get_c2_info('id'));
        }
        else //存入公共日志  global_log
        {
            $ret = \m_config::write_global_execute_log(NS_CDS_CDN_QUEUE,$this->str_source_id,$this->str_sp_id,$this->get_c2_info('action'),$this->get_c2_type(),var_export($msg,true));
        }

        return \m_config::return_data($code, $msg, $ret['data_info']);
    }

    /**
     * 保存C2执行过程日志[仅记录队列最近执行时的日志路径]
     * @param $return_info
     * @return mixe
     */
    public function save_exec_log($return_info)
    {
        if($this->get_c2_info('id'))
        {
            if (isset($return_info['data_info']['base_dir']) && !empty($return_info['data_info']['base_dir']))
            {
                $exec_log = array("nns_queue_execute_url" => $return_info['data_info']['base_dir']);
                //修改C2任务
                \nl_file_task::edit($this->get_dc(), $exec_log, $this->get_c2_info('id'));
            }
            elseif (isset($return_info['error_data_info']['base_dir']) && !empty($return_info['error_data_info']['base_dir']))
            {
                $exec_log = array("nns_queue_execute_url" => $return_info['error_data_info']['base_dir']);
                //修改C2任务
                \nl_file_task::edit($this->get_dc(), $exec_log, $this->get_c2_info('id'));
            }
        }
        return $return_info;
    }

    /**
     * 变量初始化
     */
    public function destroy_init()
    {
        $this->str_sp_id = '';//SPID
        $this->str_source_id = '';//CPID
        $this->arr_sp_config = array();//SP配置
        $this->arr_cp_config = array();//CP配置

        //$this->m_queue_model = array();
        if(isset($this->c2_task_info))
        {
            $this->c2_task_info = array();//C2队列数据  写一个key=>value获取
        }
        $this->c2_info = array();//C2队列数据  写一个key=>value获取
        $this->video_info = array();//主媒资数据
        $this->index_info = array();//分集数据
        $this->seekpoint_info = array();//分集打点数据
        $this->media_info = array();//片源数据
        $this->live_info = array();//直播
        $this->live_index_info = array();//直播分集
        $this->live_media_info = array();//直播源
        $this->file_info = array();//文件包
        $this->package_info = array();//产品打包
        $this->playbill_info = array();//节目单
    }
}
