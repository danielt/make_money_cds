<?php
namespace ns_model\center_op;
\ns_core\m_load::load("ns_core.nn_timer");
\ns_core\m_load::load("ns_data_model.m_data_model");
\ns_core\m_load::load_old("nn_logic/op_queue/op_queue.class.php");
\ns_core\m_load::load_old("nn_logic/vod_index/vod_index.class.php");
\ns_core\m_load::load_old("nn_logic/vod_v2/vod_v2.class.php");
\ns_core\m_load::load_old("nn_logic/vod_media/vod_media.class.php");
\ns_core\m_load::load_old("nn_logic/seekpoint/seekpoint.class.php");
\ns_core\m_load::load_old("nn_logic/c2_task/c2_task.class.php");
\ns_core\m_load::load_old("nn_logic/live/playbill.class.php");
/**
 * 中心同步指令解释器
 */
class center_op_explain
{
    public $str_sp_id = null;
    public $arr_sp_config = null;
    public $str_video_type = null;

    /**
     * cdn_import_explain constructor.
     * @param $sp_id
     * @param string $video_type
     */
    public function __construct($sp_id = '',$video_type = '')
    {
        if(!empty($sp_id))
        {
            $arr_sp_config = \m_config::_get_sp_info($sp_id);
            $arr_sp_config = isset($arr_sp_config['data_info']['nns_config']) ? $arr_sp_config['data_info']['nns_config'] : null;
            $this->arr_sp_config = $arr_sp_config;
        }
        $this->str_sp_id = $sp_id;
        $this->str_video_type = $video_type;
    }

    /**
     * 是否属于最终状态
     * @param $c2_task_info c2任务
     * @return array(ret,reason); ret 0 为最终态
     */
    public function is_finally_status($c2_task_info)
    {
        $sp_info = \m_config::_get_sp_info($c2_task_info['nns_org_id']);
        if($sp_info['ret'] != NS_CDS_SUCCE)
        {
            return $sp_info;
        }
        $arr_sp_config = $sp_info['data_info']['nns_config'];
        $finaly = $c2_task_info['nns_type'] . "_" . $c2_task_info['nns_action'];
        //EGP优先
        if ((int)$arr_sp_config[$finaly]['priority'] === 1)
        {
            //全部注入
            if ((int)$arr_sp_config[$finaly]['status'] === 0)
            {
                if ((int)$c2_task_info['nns_status'] === 0 && (int)$c2_task_info['nns_epg_status'] === 99)
                {
                    return \m_config::return_data(NS_CDS_SUCCE,'最终状态');
                }
            }
            elseif ((int)$arr_sp_config[$finaly]['status'] === 1)
            {//只注入CDN
                if ((int)$c2_task_info['nns_status'] === 0)
                {
                    return \m_config::return_data(NS_CDS_SUCCE,'最终状态');
                }
            }
            elseif ((int)$arr_sp_config[$finaly]['status'] === 2)
            {//只注入EPG
                if ((int)$c2_task_info['nns_epg_status'] === 99)
                {
                    return \m_config::return_data(NS_CDS_SUCCE,'最终状态');
                }
            }
            //CDN优先
        }
        elseif ((int)$arr_sp_config[$finaly]['priority'] === 0)
        {
            //全部注入
            if ((int)$arr_sp_config[$finaly]['status'] === 0)
            {
                if ((int)$c2_task_info['nns_status'] === 0 && (int)$c2_task_info['nns_epg_status'] === 99)
                {
                    return \m_config::return_data(NS_CDS_SUCCE,'最终状态');
                }
            }
            elseif ((int)$arr_sp_config[$finaly]['status'] === 1)
            {//只注入CDN
                if ((int)$c2_task_info['nns_status'] === 0)
                {
                    return \m_config::return_data(NS_CDS_SUCCE,'最终状态');
                }
            }
            elseif ((int)$arr_sp_config[$finaly]['status'] === 2)
            {//只注入EPG
                if ((int)$c2_task_info['nns_epg_status'] === 99)
                {
                    return \m_config::return_data(NS_CDS_SUCCE,'最终状态');
                }
            }
        }
        return \m_config::return_data(NS_CDS_FAIL,'不属于最终状态');
    }

    /**
     * 获取中心同步注入指令
     * @param array $import_data 注入内容
     * @param array $import_base_info 注入内容基础数据
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function get_op_queue_params($import_data,$import_base_info)
    {
        //中心注入指令逻辑过滤
        $center_import_push_logic = new \ns_model\center_import\center_import_push_logic($import_data, $import_base_info);
        $result_op_sp =  $center_import_push_logic->get_cp_bind_sp_logic();
        if($result_op_sp['ret'] != NS_CDS_SUCCE)
        {
            return $result_op_sp;
        }
        if(!isset($result_op_sp['data_info']) || strlen($result_op_sp['data_info']) < 1)
        {
            return \m_config::return_data(NS_CDS_FAIL,'没匹配到任何SP下游平台');
        }
        unset($center_import_push_logic);
        #todo 中心同步指令过滤
        $arr_import_sp = array_filter(explode(",",$result_op_sp['data_info']));
        $op_filter_re = $this->op_fiter_sp($import_data,$import_base_info,$arr_import_sp);
        if($op_filter_re['ret'] != NS_CDS_SUCCE || empty($op_filter_re['data_info']))
        {
            return $op_filter_re;
        }

        #todo 需要检查是否存在相同未执行的任务，如果存在则覆盖

        #todo 确认初始注入状态

        $arr_queue = array(
            'base_info' => array(
                'nns_type' => $import_data['base_info']['nns_video_type'],
                'nns_action' => $import_data['base_info']['nns_action'],
                'nns_weight' => $import_data['base_info']['nns_weight'],
                'nns_op_mtime' => round(microtime(TRUE) * 1000),
                'nns_release_time' => empty($import_base_info[$import_data['base_info']['nns_video_type']]['base_info']['nns_release_time']) ? date("Y-m-d") : $import_base_info[$import_data['base_info']['nns_video_type']]['base_info']['nns_release_time'],
                'nns_state' => 0,
                'nns_message_id' => $import_data['base_info']['nns_message_id'],
                'nns_is_group' => $import_data['base_info']['nns_is_group'],
                'nns_cp_id' => $import_data['base_info']['nns_cp_id'],
                'nns_video_id' => '',
                'nns_index_id' => '',
                'nns_media_id' => '',
                'nns_ex_data' => '',
                'nns_from' => '',
                'nns_encode_flag' => '',
            )
        );
        switch ($import_data['base_info']['nns_video_type'])
        {
            case NS_CDS_VIDEO:
                $arr_queue['base_info']['nns_video_id'] = $import_data['base_info']['nns_id'];
                $arr_queue['sp_info'] = $op_filter_re['data_info'];
                break;
            case NS_CDS_INDEX:
                $arr_queue['base_info']['nns_video_id'] = $import_base_info[$import_data['base_info']['nns_video_type']]['base_info']['nns_vod_id'];
                $arr_queue['base_info']['nns_index_id'] = $import_data['base_info']['nns_id'];
                $arr_queue['sp_info'] = $op_filter_re['data_info'];
                break;
            case NS_CDS_MEDIA:
                $arr_queue['base_info']['nns_video_id'] = $import_base_info[$import_data['base_info']['nns_video_type']]['base_info']['nns_vod_id'];
                $arr_queue['base_info']['nns_index_id'] = $import_base_info[$import_data['base_info']['nns_video_type']]['base_info']['nns_vod_index_id'];
                $arr_queue['base_info']['nns_media_id'] = $import_data['base_info']['nns_id'];
                $arr_queue['sp_info'] = $op_filter_re['data_info'];
                break;
            case NS_CDS_LIVE:
                $arr_queue['base_info']['nns_video_id'] = $import_data['base_info']['nns_id'];
                $arr_queue['sp_info'] = $op_filter_re['data_info'];
                break;
            case NS_CDS_LIVE_INDEX:
                $arr_queue['base_info']['nns_video_id'] = $import_base_info[$import_data['base_info']['nns_video_type']]['base_info']['nns_live_id'];
                $arr_queue['base_info']['nns_index_id'] = $import_data['base_info']['nns_id'];
                $arr_queue['sp_info'] = $op_filter_re['data_info'];
                break;
            case NS_CDS_LIVE_MEDIA:
                $arr_queue['base_info']['nns_video_id'] = $import_base_info[$import_data['base_info']['nns_video_type']]['base_info']['nns_live_id'];
                $arr_queue['base_info']['nns_index_id'] = $import_base_info[$import_data['base_info']['nns_video_type']]['base_info']['nns_live_index_id'];
                $arr_queue['base_info']['nns_media_id'] = $import_data['base_info']['nns_id'];
                $arr_queue['sp_info'] = $op_filter_re['data_info'];
                break;
            case NS_CDS_PLAYBILL:
                $arr_queue['base_info']['nns_video_id'] = $import_base_info[$import_data['base_info']['nns_video_type']]['base_info']['nns_live_id'];
                $arr_queue['base_info']['nns_media_id'] = $import_data['base_info']['nns_id'];
                $arr_queue['sp_info'] = $op_filter_re['data_info'];
                break;
            case NS_CDS_FILE:

                break;
            case NS_CDS_SEEKPOINT:

                break;
            case NS_CDS_PRODUCT:

                break;
            case NS_CDS_EPG_FILE:
                $arr_queue['base_info']['nns_video_id'] = $import_base_info[$import_data['base_info']['nns_video_type']]['base_info']['nns_epg_file_set_id'];
                $arr_queue['base_info']['nns_index_id'] = $import_data['base_info']['nns_id'];
                $arr_queue['sp_info'] = $op_filter_re['data_info'];
                break;
        }

        unset($import_data);
        unset($import_base_info);
        return \m_config::return_data(NS_CDS_SUCCE,'Ok',$arr_queue);
    }

    public function op_fiter_sp($import_data,$import_base_info,$arr_sp)
    {

        return \m_config::return_data(NS_CDS_SUCCE,"ok",$arr_sp);
    }

    /**
     * @description:c2上报了为最终状态，则删除中兴同步指令
     * @author:xinxin.deng
     * @date: 2018/4/8 17:17
     * @param $op_id
     * @return array
     */
    public function report_op_task($op_id)
    {
        $op_list = \nl_op_queue::query_by_id(\m_config::get_dc(), $op_id);
        if ($op_list['ret'] != 0 || !is_array($op_list['data_info']) || empty($op_list['data_info']))
        {
            return \m_config::return_data($op_list['ret'], 'op_queue执行查询队列不存在或者执行失败:' . $op_list['reason']);
        }
        $re = \nl_op_queue::delete_op_queue(\m_config::get_dc(), array($op_id));
        return \m_config::return_data($re['ret'], $re['reason']);
    }


    /**
     * 查询满足条件的
     * @return array
     */
    public function get_c2_num_arr()
    {
        //查询c2队列当前满足条件的c2队列的条数
        if(!isset($this->arr_sp_config['disabled_cdn']) || (int)$this->arr_sp_config['disabled_cdn'] === 0)
        {
            $c2_count_model = explode(',', $this->arr_sp_config['c2_count_mode']);//c2计算模式，多个以逗号分隔
            $new_str = '';
            if (!empty($c2_count_model) && is_array($c2_count_model))
            {
                foreach ($c2_count_model as $val)
                {
                    //0:等待执行;1:执行失败;2:正在执行;3:等待获取CDN播放串;4:注入取消;5:正在获取CDN播放串;6:获取播放传失败(多个以逗号,分隔)
                    switch ($val)
                    {
                        case '0':
                            $new_str .= 1 . ',';
                            break;
                        case '1':
                            $new_str .= -1 . ',';
                            break;
                        case '2':
                            $new_str .= 5 . ',';
                            break;
                        case '3':
                            $new_str .= 6 . ',';
                            break;
                        case '4':
                            $new_str .= 7 . ',';
                            break;
                        case '5':
                            $new_str .= 8 . ',';
                            break;
                        case '6':
                            $new_str .= 9 . ',';
                            break;
                        default :
                            $new_str .= 1 . ',';
                            break;
                    }
                }
                $query_str = " and nns_status in (" . rtrim($new_str,',') .") ";
            }
            else
            {
                $query_str = ' and nns_epg_status = 97';
            }
        }
        else
        {
            $query_str = ' and nns_epg_status = 97';
        }

        //song.wang 2018-12-12节目单控制
        if(in_array($this->str_video_type,array("playbill")))
        {
            $table_name = "nns_mgtvbk_c2_live_task";
        }
        else
        {
            $table_name = "nns_mgtvbk_c2_task";
        }
        $sql = "select count(*) as num from {$table_name} where nns_type='" . $this->str_video_type ."' and nns_org_id='".$this->str_sp_id."' $query_str";
        return \nl_c2_task::query_by_sql(\m_config::get_dc(), $sql);
    }

    /**
     * 查询满足条件的
     * @return array
     */
    public function get_file_num_arr()
    {
        //查询c2队列当前满足条件的c2队列的条数
        if(!isset($this->arr_sp_config['disabled_cdn']) || (int)$this->arr_sp_config['disabled_cdn'] === 0)
        {
            $c2_count_model = explode(',', $this->arr_sp_config['c2_count_mode']);//c2计算模式，多个以逗号分隔
            $new_str = '';
            if (!empty($c2_count_model) && is_array($c2_count_model))
            {
                foreach ($c2_count_model as $val)
                {
                    //0:等待执行;1:执行失败;2:正在执行;3:等待获取CDN播放串;4:注入取消;5:正在获取CDN播放串;6:获取播放传失败(多个以逗号,分隔)
                    switch ($val)
                    {
                        case '0':
                            $new_str .= 1 . ',';
                            break;
                        case '1':
                            $new_str .= -1 . ',';
                            break;
                        case '2':
                            $new_str .= 5 . ',';
                            break;
                        case '3':
                            $new_str .= 6 . ',';
                            break;
                        case '4':
                            $new_str .= 7 . ',';
                            break;
                        case '5':
                            $new_str .= 8 . ',';
                            break;
                        case '6':
                            $new_str .= 9 . ',';
                            break;
                        default :
                            $new_str .= 1 . ',';
                            break;
                    }
                }
                $query_str = " and nns_status in (" . rtrim($new_str,',') .") ";

            }
            else
            {
                $query_str = ' and nns_epg_status = 97';
            }
        }
        else
        {
            $query_str = ' and nns_epg_status = 97';
        }
        $sql = "select count(*) as num from nns_mgtvbk_file_task where nns_type='" . $this->str_video_type ."' and nns_org_id='".$this->str_sp_id."' $query_str";

        return \nl_file_task::query_by_sql(\m_config::get_dc(), $sql);
    }

    /**
     * 获取中心同步指令对列
     * @param $video_type
     * @param $limit
     * @return array
     */
    public function get_op_queue_list($video_type, $limit)
    {
        //排序
        $order=empty($this->arr_sp_config['op_task_order']) ? 'nns_weight desc,nns_op_mtime asc,nns_create_time asc,nns_video_id asc':$this->arr_sp_config['op_task_order'];

        //冻结时间获取
        $freeze_time=empty($this->arr_sp_config['op_task_freeze_time']) ? NULL : date($this->arr_sp_config['op_task_freeze_time']);
        $freeze_time_sql='';
        if (!empty($freeze_time)){
            $freeze_time=strtotime($freeze_time)*1000;
            $freeze_time_sql=' and nns_op_mtime<'.$freeze_time;
        }

        $is_execute='select 1 from nns_mgtvbk_op_queue as b where b.nns_org_id="'.$this->str_sp_id.'" and ' ;
        $is_execute_q1=	'(b.nns_status="'.NS_CDS_OP_LOADING.'" ' .
            'and b.nns_video_id=a.nns_video_id and b.nns_index_id=a.nns_index_id and b.nns_media_id=a.nns_media_id) ';

        $is_execute .=$is_execute_q1;

        //直接用SQL来查询会卡住数据库，故分解成2个SQL来进行查询.
        $sql='select * from nns_mgtvbk_op_queue as a where a.nns_org_id="'.$this->str_sp_id.'" ' .
            ' and a.nns_type="' . $video_type . '"' .
            ' and a.nns_state=0 ' .
            '  and a.nns_status=' . NS_CDS_OP_WAIT .
            '  and  not exists (' . $is_execute . ') ' .
            $freeze_time_sql . ' order by ' . $order . ' limit ' . $limit;
        return \nl_op_queue::query_by_sql(\m_config::get_dc(), $sql);

    }

    /**
     * 节目单获取中心同步指令队列
     * @param $video_type
     * @param $limit
     * @return array
     */
    public function playbill_get_op_queue_list($video_type, $limit)
    {
        //排序
        $order=empty($this->arr_sp_config['op_task_order']) ? 'nns_weight desc,nns_op_mtime asc,nns_create_time asc,nns_video_id asc':$this->arr_sp_config['op_task_order'];

        //冻结时间获取
        $freeze_time=empty($this->arr_sp_config['op_task_freeze_time']) ? NULL : date($this->arr_sp_config['op_task_freeze_time']);
        $freeze_time_sql='';
        if (!empty($freeze_time)){
            $freeze_time=strtotime($freeze_time)*1000;
            $freeze_time_sql=' and nns_op_mtime<'.$freeze_time;
        }

        $is_execute='select 1 from nns_mgtvbk_op_queue as b where b.nns_org_id="'.$this->str_sp_id.'" and ' ;
        $is_execute_q1=	'(b.nns_status="'.NS_CDS_OP_LOADING.'" ' .
            'and b.nns_video_id=a.nns_video_id and b.nns_index_id=a.nns_index_id and b.nns_media_id=a.nns_media_id) ';

        $is_execute .=$is_execute_q1;

        $action = "destroy";
        //优先注入节目单中的删除，剩余的再拿其他的补充
        $sql='select * from nns_mgtvbk_op_queue as a where a.nns_org_id="'.$this->str_sp_id.'" ' .
            ' and a.nns_type="' . $video_type . '"' .
            ' and a.nns_action="' . $action . '"' .
            ' and a.nns_state=0 ' .
            '  and a.nns_status=' . NS_CDS_OP_WAIT .
            '  and  not exists (' . $is_execute . ') ' .
            $freeze_time_sql . ' order by ' . $order . ' limit ' . $limit;
        $delete_info = \nl_op_queue::query_by_sql(\m_config::get_dc(), $sql);
        if($delete_info['ret'] != 0 )
        {
            return $delete_info;
        }
        if(!is_array($delete_info['data_info']) || empty($delete_info['data_info']))
        {
            $delete_num = 0;
            $delete_info['data_info'] = array();
        }
        else
        {
            $delete_num = count($delete_info['data_info']);
        }
        $num = $limit - $delete_num;           //还要补全多少个
        $playbill_info['data_info'] = array();
        if($num > 0 )
        {
            $sql='select * from nns_mgtvbk_op_queue as a where a.nns_org_id="'.$this->str_sp_id.'" ' .
                ' and a.nns_type="' . $video_type . '"' .
                ' and a.nns_action !="' . $action . '"' .
                ' and a.nns_state=0 ' .
                '  and a.nns_status=' . NS_CDS_OP_WAIT .
                '  and  not exists (' . $is_execute . ') ' .
                $freeze_time_sql . ' order by ' . $order . ' limit ' . $num;
            $playbill_info = \nl_op_queue::query_by_sql(\m_config::get_dc(), $sql);
            if($playbill_info['ret'] != 0)
            {
                return $playbill_info;
            }
            if(!is_array($playbill_info['data_info']) || empty($playbill_info['data_info']))
            {
                $playbill_info['data_info'] = array();
            }
        }
        return array(
            'ret'=>$delete_info['ret'],
            'reason'=>$delete_info['reason'],
            'data_info'=>array_merge($delete_info['data_info'],$playbill_info['data_info'])
        );
    }

    /**
     * 根据中心同步指令任务类型，批量获取中心同步指令任务的基础信息
     * @param $data //中心同步任务数据
     * array(
     *  array(),
     *  array()
     * )
     * @return array(
     *              'ret',
     *              'reason',
     *              'data_info'=>array(
     *                  array(
     *                      'c2_info'=>array(C2信息),
     *                      'video_info'=>array(
     *                          'base_info' => VIDEO信息
     *                          'ex_info'  => 扩展信息
     *                       ),
     *                      'index_info'=>array(
     *                          'base_info' => INDEX信息,
     *                          'ex_info' => 扩展信息,
     *                      ),
     *                      'media_info'=>array(
     *                          'base_info' => MEDIA信息
     *                          'ex_info' => 扩展信息
     *                      ),
     *                  )
     *             )
     *      )
     */
    public function query_mixed_info($data)
    {
        $new_op_queue_info = array();
        $ref_arr = array();//当前任务的媒资ID
        //组装批量查询数据
        foreach ($data as $op_queue_info)
        {
            //特殊处理其他类型数据，便于后期扩展----xinxin.deng 2018/11/14 16:28
            switch ($this->str_video_type)
            {
                case NS_CDS_EPG_FILE:
                    $str_id = 'nns_index_id';//模板文件当作分集处理
                    break;
                case NS_CDS_PLAYBILL:
                    $str_id = "nns_media_id";   //节目单ID保存在片源字段
                    break;
                default:
                    $str_id = 'nns_' . $this->str_video_type . '_id';
                    break;
            }
            $ref_arr[$this->str_video_type][$str_id][] = $op_queue_info[$str_id];
        }
        //根据类型获取不同的数据源
        switch ($this->str_video_type)
        {
            case 'video':
                //批量获取主媒资及扩展信息
                $vod_result = \nl_vod_v2::query_vod_by_id(\m_config::get_dc(),$ref_arr['video']['nns_video_id']);
                if($vod_result['ret'] != NS_CDS_SUCCE)
                {
                    return $vod_result;
                }
                //数据组装
                $vod_result = $vod_result['data_info'];
                foreach ($data as $op_queue)
                {
                    $op_queue_data = array();
                    $op_queue_data['op_queue_info'] = $op_queue;
                    if(isset($vod_result[$op_queue['nns_video_id']]))
                    {
                        $op_queue_data['video_info'] = $vod_result[$op_queue['nns_video_id']];
                    }
                    $new_op_queue_info[] = $op_queue_data;
                    unset($op_queue_data);
                }
                break;
            case 'index':
                //批量获取分集及扩展信息
                $vod_index_result = \nl_vod_index_v2::query_vod_index_by_id(\m_config::get_dc(),$ref_arr['index']['nns_index_id']);
                if($vod_index_result['ret'] != NS_CDS_SUCCE)
                {
                    return $vod_index_result;
                }
                $vod_index_result = $vod_index_result['data_info'];
                foreach ($data as $op_queue)
                {
                    $op_queue_data = array();
                    $op_queue_data['op_queue_info'] = $op_queue;
                    if(isset($vod_index_result[$op_queue['nns_index_id']]))
                    {
                        $op_queue_data['index_info'] = $vod_index_result[$op_queue['nns_index_id']];
                    }
                    $params['nns_video_index']=$vod_index_result[$op_queue['nns_index_id']]['nns_id'];
                    $seekpoint_result=\nl_seekpoint::query_by_condition(\m_config::get_dc(), $params);
                    if($seekpoint_result['ret']=== NS_CDS_SUCCE)
                    {
                        $op_queue_data['seekpoint_info']=$seekpoint_result['result'];
                    }
                    $new_op_queue_info[] = $op_queue_data;
                    unset($op_queue_data);
                }
                break;
            case 'media':
                //批量获取片源及扩展信息
                $vod_media_result = \nl_vod_media_v2::query_vod_media_by_id(\m_config::get_dc(),$ref_arr['media']['nns_media_id']);
                if($vod_media_result['ret'] != NS_CDS_SUCCE)
                {
                    return $vod_media_result;
                }
                $vod_media_result = $vod_media_result['data_info'];
                foreach ($data as $op_queue)
                {
                    $op_queue_data =  array();
                    $op_queue_data['op_queue_info'] = $op_queue;

                    if(isset($vod_media_result[$op_queue['nns_media_id']]))
                    {
                        $op_queue_data['media_info'] = $vod_media_result[$op_queue['nns_media_id']];
                    }
                    $new_op_queue_info[] = $op_queue_data;
                    unset($op_queue_data);
                }
                break;
            case 'seekpoint':
                //批量获取打点信息
                $seekpoint_result = \nl_seekpoint::query_seekpoint_by_id(\m_config::get_dc(),$ref_arr);
                if($seekpoint_result['ret'] != NS_CDS_SUCCE)
                {
                    return $seekpoint_result;
                }
                //数据组装
                $seekpoint_result = $seekpoint_result['data_info'];
                foreach ($data as $c2)
                {
                    $c2_data = array();
                    $c2_data['c2_info'] = $c2;
                    if(isset($seekpoint_result[$c2['nns_ref_id']]))
                    {
                        $c2_data['seekpoint_info'] = $seekpoint_result[$c2['nns_ref_id']];
                        //打点分集信息
                        $vod_index_result= \nl_vod_index_v2::query_vod_index_by_id(\m_config::get_dc(),array($c2_data['seekpoint_info']['nns_vod_index_id']));
                        if($vod_index_result['ret'] == NS_CDS_SUCCE)
                        {
                            $c2_data['index_info']= $vod_index_result['data_info'][$c2_data['seekpoint_info']['nns_vod_index_id']];
                        }
                    }
                    $new_c2_info[] = $c2_data;
                    unset($c2_data);
                }
                break;
            case 'live':
                //批量获取直播及扩展信息
                $live_result = \nl_live::query_live_by_id(\m_config::get_dc(),array($data['0']['nns_video_id']));
                if($live_result['ret'] != NS_CDS_SUCCE)
                {
                    return $live_result;
                }
                //数据组装
                $live_result = $live_result['data_info'];
                foreach ($data as $c2)
                {
                    $c2_data = array();
                    $c2_data['op_queue_info'] = $c2;
                    if(isset($live_result[$c2['nns_video_id']]))
                    {
                        $c2_data['live_info'] = $live_result[$c2['nns_video_id']];
                    }
                    $new_op_queue_info[] = $c2_data;
                    unset($c2_data);
                }
                break;
            case 'live_index':
                break;
            case 'live_media':
                //批量获取直播分集及扩展信息
                $live_index_result = \nl_live_index::query_live_index_by_id(\m_config::get_dc(),$src_arr);
                if($live_index_result['ret'] != NS_CDS_SUCCE)
                {
                    return $live_index_result;
                }
                $live_index_result = $live_index_result['data_info'];
                //批量获取直播片源及扩展信息
                $live_media_result = \nl_live_media::query_live_media_by_id(\m_config::get_dc(),$ref_arr);
                if($live_media_result['ret'] != NS_CDS_SUCCE)
                {
                    return $live_media_result;
                }
                $live_media_result = $live_media_result['data_info'];
                //批量获取直播及扩展信息
                $live_guid = array();
                foreach ($live_index_result as $index_re)
                {
                    $live_guid[$index_re['base_info']['nns_id']] = $index_re['base_info']['nns_live_id'];
                }
                $live_result = \nl_live::query_live_by_id(\m_config::get_dc(),array_values($live_guid));
                if($live_result['ret'] != NS_CDS_SUCCE)
                {
                    return $live_result;
                }
                $live_result = $live_result['data_info'];
                foreach ($data as $c2)
                {
                    $c2_data =  array();
                    $c2_data['c2_info'] = $c2;
                    if(isset($live_result[$live_guid[$c2['nns_src_id']]]))
                    {
                        $c2_data['live_info'] = $live_result[$live_guid[$c2['nns_src_id']]];
                    }
                    if(isset($live_index_result[$c2['nns_src_id']]))
                    {
                        $c2_data['live_index_info'] = $live_index_result[$c2['nns_src_id']];
                    }
                    if(isset($live_media_result[$c2['nns_ref_id']]))
                    {
                        $c2_data['live_media_info'] = $live_media_result[$c2['nns_ref_id']];
                    }
                    $new_c2_info[] = $c2_data;
                    unset($c2_data);
                }
                break;
            case 'playbill':
                //批量获取直播节目单信息
                $playbill_result = \nl_playbill::query_playbill_by_id(\m_config::get_dc(),$ref_arr[$this->str_video_type]['nns_media_id']);
                if($playbill_result['ret'] != NS_CDS_SUCCE)
                {
                    return $playbill_result;
                }
                //数据组装
                $playbill_result = $playbill_result['data_info'];

                foreach ($data as $c2)
                {
                    $c2_data = array();
                    $c2_data['op_queue_info'] = $c2;
                    if(isset($playbill_result[$c2['nns_media_id']]))
                    {
                        $c2_data['playbill_info'] = $playbill_result[$c2['nns_media_id']];
                        //直播频道信息
                        $live_result=\nl_live::query_live_by_id(\m_config::get_dc(),array($c2_data['playbill_info']['base_info']['nns_live_id']));
                        if($live_result['ret'] == NS_CDS_SUCCE)
                        {
                            $c2_data['live_info']= $live_result['data_info'][$c2_data['playbill_info']['base_info']['nns_live_id']];
                        }
                        //直播源信息
//                        $live_media_result=\nl_live_media::query_live_media_by_id(\m_config::get_dc(),array($c2_data['playbill_info']['base_info']['nns_live_media_id']));
//                        if($live_media_result['ret'] == NS_CDS_SUCCE)
//                        {
//                            $c2_data['live_media_info']= $live_media_result['data_info'][$c2_data['playbill_info']['base_info']['nns_live_media_id']];
//                        }
                    }
                    $new_op_queue_info[] = $c2_data;
                    unset($c2_data);
                }
                break;
            case 'file':
                //批量获取文件包信息
                $file_result = \nl_file_package::query_live_by_id(\m_config::get_dc(),$ref_arr);
                if($file_result['ret'] != NS_CDS_SUCCE)
                {
                    return $file_result;
                }
                //数据组装
                $file_result = $file_result['data_info'];
                foreach ($data as $c2)
                {
                    $c2_data = array();
                    $c2_data['c2_info'] = $c2;
                    if(isset($live_result[$c2['nns_ref_id']]))
                    {
                        $c2_data['file_info'] = $file_result[$c2['nns_ref_id']];
                    }
                    $new_c2_info[] = $c2_data;
                    unset($c2_data);
                }
                break;
            case 'file_package':

                break;
            case NS_CDS_EPG_FILE:
                //批量获取EPGFile信息
                $epg_file_result = \nl_epg_file::query_data_by_id(\m_config::get_dc(),$ref_arr['epg_file']['nns_index_id']);
                if($epg_file_result['ret'] != NS_CDS_SUCCE)
                {
                    return $epg_file_result;
                }
                $epg_file_result = $epg_file_result['data_info'];
                foreach ($data as $op_queue)
                {
                    $op_queue_data = array();
                    $op_queue_data['op_queue_info'] = $op_queue;
                    if(isset($epg_file_result[$op_queue['nns_index_id']]))
                    {
                        $op_queue_data['epg_file_info'] = $epg_file_result[$op_queue['nns_index_id']];
                    }
                    $new_op_queue_info[] = $op_queue_data;
                    unset($op_queue_data);
                }
                break;
                break;
        }
        return \m_config::return_data(NS_CDS_SUCCE,'',$new_op_queue_info);
    }


}