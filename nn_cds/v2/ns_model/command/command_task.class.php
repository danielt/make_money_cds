<?php
namespace ns_model\command;
\ns_core\m_load::load("ns_data_model.command.m_command_task_inout");
/**
 * 指令状态接口机
 */
class command_task
{
    /**
     * 状态池
     */
    public function push($params)
    {
        $m_command_task_inout = new \m_command_task_inout();
        return $m_command_task_inout->add($params);
    }

    /**
     * 根据message_id获取状态层指令
     * @param $params
     * @return array
     */
    public function query($params)
    {
        $m_command_inout = new \m_command_task_inout();
        return $m_command_inout->query($params);
    }



}