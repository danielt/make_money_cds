<?php
namespace ns_model\command;
\ns_core\m_load::load("ns_data_model.command.m_command_inout");
/**
 * 指令状态接口机
 */
class command
{
    /**
     * 消息拆分进入队列，生成指令状态池
     * @param $params
     * @return array
     */
    public function push($params)
    {
        $m_command_inout = new \m_command_inout();
        return $m_command_inout->add($params);
    }

    /**
     * 根据message_id获取状态层指令
     * @param $params
     * @return array
     */
    public function query($params)
    {
        $m_command_inout = new \m_command_inout();
        return $m_command_inout->query($params);
    }

    public function delete($params)
    {
        $m_command_inout = new \m_command_inout();
        return $m_command_inout->del($params);
    }
}