<?php
/**
 * 直播CDN注入解释器--流程分解
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/11/30 11:16
 */
namespace ns_model\cdn_import;
class live_cdn_import_explain
{
    public $str_sp_id = null;
    public $arr_sp_config = null;
    public $str_video_type = null;

    /**
     * cdn_import_explain constructor.
     * @param $sp_id
     * @param string $video_type
     */
    public function __construct($sp_id,$video_type)
    {
        $this->str_sp_id = $sp_id;
        $arr_sp_config = \m_config::_get_sp_info($sp_id);
        $arr_sp_config = isset($arr_sp_config['data_info']['nns_config']) ? $arr_sp_config['data_info']['nns_config'] : null;
        $this->arr_sp_config = $arr_sp_config;
        $this->str_video_type = $video_type;
    }

    /**
     * 获取C2任务的sql
     * @param string $limit 获取条数
     * @param $retry_time 重试次数
     */
    public function get_c2_task_by_old_func($limit,$retry_time=0)
    {
        $sql = "select t1.* from nns_mgtvbk_c2_live_task t1 where t1.nns_org_id='{$this->str_sp_id}'";
        $where_array = array();
        $ext_sql = '';

        switch ($this->str_video_type)
        {
            case NS_CDS_VIDEO:
                $where_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_ADD,$this->str_video_type, $retry_time);
                $where_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_MODIFY,$this->str_video_type, $retry_time);
                $where_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_DELETE,$this->str_video_type, $retry_time);
                break;
            case NS_CDS_INDEX:
                $where_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_ADD,$this->str_video_type, $retry_time);
                $where_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_MODIFY,$this->str_video_type, $retry_time);
                $where_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_DELETE,$this->str_video_type, $retry_time);

                //判断父级是否需要注入CDN
                $parent_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_ADD,NS_CDS_VIDEO, $retry_time);
                $parent_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_MODIFY,NS_CDS_VIDEO, $retry_time);
                $parent_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_DELETE,NS_CDS_VIDEO, $retry_time);

                $nns_status = '';
                if (is_array(array_filter($parent_array)) && count(array_filter($parent_array)) > 0)
                {
                    $nns_status = ' and t2.nns_status=0 ';
                }

                $ext_sql = " and  (t1.nns_action='" . NS_CDS_DELETE . "' or exists (select 1 from nns_mgtvbk_c2_task t2 where t2.nns_org_id='{$this->str_sp_id}' and t1.nns_src_id=t2.nns_ref_id $nns_status and t2.nns_type='" . NS_CDS_VIDEO . "'))";
                break;
            case NS_CDS_MEDIA:
                $where_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_ADD,$this->str_video_type, $retry_time);
                $where_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_MODIFY,$this->str_video_type, $retry_time);
                $where_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_DELETE,$this->str_video_type, $retry_time);

                $parent_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_ADD,NS_CDS_INDEX,$retry_time);
                $parent_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_MODIFY,NS_CDS_INDEX,$retry_time);
                $parent_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_DELETE,NS_CDS_INDEX, $retry_time);

                $nns_status = '';
                if (is_array(array_filter($parent_array)) && count(array_filter($parent_array)) > 0)
                {
                    $nns_status = ' and t2.nns_status=0 ';
                }

                $ext_sql = " and (t1.nns_action='" . NS_CDS_DELETE . "' or exists (select 1 from nns_mgtvbk_c2_task t2 where t2.nns_org_id='{$this->str_sp_id}'  and t1.nns_src_id=t2.nns_ref_id $nns_status and t2.nns_type='" . NS_CDS_INDEX . "'))";
                break;
            case NS_CDS_LIVE_MEDIA:
                $where_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_ADD,$this->str_video_type, $retry_time);
                $where_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_MODIFY,$this->str_video_type, $retry_time);
                $where_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_DELETE,$this->str_video_type, $retry_time);
                break;
            case NS_CDS_PLAYBILL:
                $where_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_ADD,$this->str_video_type, $retry_time);
                $where_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_MODIFY,$this->str_video_type, $retry_time);
                $where_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_DELETE,$this->str_video_type, $retry_time);
                //判断父级是否需要注入CDN
                $nns_status = '';
                if (isset($this->arr_sp_config['cdn_playbill_check_live_enabled']) && $this->arr_sp_config['cdn_playbill_check_live_enabled'] == '1')
                {
                    $parent_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_ADD,NS_CDS_LIVE, $retry_time);
                    $parent_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_MODIFY,NS_CDS_LIVE, $retry_time);
                    $parent_array[] = $this->__get_child_cdn_sql_by_old_func(NS_CDS_DELETE,NS_CDS_LIVE, $retry_time);
                    if (is_array(array_filter($parent_array)) && count(array_filter($parent_array)) > 0)
                    {
                        $nns_status = ' and t2.nns_status=0 ';
                    }
                }
                $ext_sql = " and (t1.nns_action='" . NS_CDS_DELETE . "' or exists (select 1 from nns_mgtvbk_c2_live_task t2 where t2.nns_org_id='{$this->str_sp_id}'
                  and t1.nns_src_id=t2.nns_ref_id $nns_status and t2.nns_type='" . NS_CDS_LIVE . "'))";
                break;
            default:
                return \m_config::return_data(NS_CDS_FAIL,'不支持的注入类型');
        }
        if (is_array(array_filter($where_array)) && count(array_filter($where_array)) > 0)
        {
            $sql .= ' and (' . implode(array_filter($where_array), ' or ') . ') ';
        }
        else
        {
            return \m_config::return_data(NS_CDS_FAIL,'数据SQL拼装错误');
        }

        $sql .= $ext_sql;
        if (isset($this->arr_sp_config[$this->str_video_type]['enabled']) && $this->arr_sp_config[$this->str_video_type]['enabled'] == '1')
        {
            $sql .= ' group by t1.nns_src_id ';
        }
        $sql .= ' order by t1.nns_modify_time asc limit  ' . $limit;
        //\m_config::write_cdn_execute_log('执行cdn获取数据队列sql为：' . $sql, $this->str_sp_id, '');
        $result = nl_query_by_db($sql,\m_config::get_dc()->db());
//        \m_config::timer_write_log("wangsong",$sql,"wangsong123456");
        if(!$result)
        {
            return \m_config::return_data(NS_CDS_FAIL,'注入CDN数据SQL执行错误：'.$sql);
        }
        return \m_config::return_data(NS_CDS_SUCCE,'',$result);
    }

    private function __get_child_cdn_sql_by_old_func($action,$type,$retry_time)
    {
        //获取策略
        $import_action = $this->arr_sp_config[$type . '_' . $action];
        //优先级  0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入
        if ((int)$import_action['status'] === 2)
        {
            return null;
        }
        $ext_sql = ((int)$retry_time > 0) ? "and t1.nns_status=" . NS_CDS_CDN_FAIL ." and t1.nns_cdn_fail_time <= $retry_time" : "and t1.nns_status=" . NS_CDS_CDN_WAIT;
        if ((int)$import_action['priority'] === 0)
        {
            //0为注入CDN和EPG，1为注入CDN，2为注入EPG
            return "(t1.nns_type='$type' and t1.nns_action='$action' $ext_sql)";
        }
        elseif ((int)$import_action['priority'] === 1 && (int)$import_action['status'] === 0)
        {
            //0为注入CDN和EPG，1为注入CDN，2为注入EPG
            return "(t1.nns_type='$type' and t1.nns_action='$action' and t1.nns_epg_status=" . NS_CDS_EPG_SUCCE ." $ext_sql)";
        }
        return null;
    }
    /**
     * 废弃，此处逻辑由CDN分发模块管理
     * 检测C2任务是否执行，不执行时进行上报
     * @param string $task_id
     * @param string $this->str_video_type
     * @param string $action
     * @param $is_series
     * @return array('ret' => 0)//注入，其他过滤
     */
    public function check_import($task_id,$action = 'add',$is_series = true)
    {
        $c2_import = true;//注入CDN
        if($this->str_video_type != 'video' && $this->str_video_type != 'index' && $this->str_video_type != 'media')
        {
            return $c2_import;
        }
        $this->str_video_type = $this->str_video_type . "_" . $action;
        if(!isset($this->arr_sp_config[$this->str_video_type]['cdn_import_model']) || empty($this->arr_sp_config[$this->str_video_type]['cdn_import_model']))//都要注入
        {
            return $c2_import;
        }
        switch ($this->arr_sp_config[$this->str_video_type]['cdn_import_model'])
        {
            case '1': //仅电影注入
                if($is_series) //连续剧
                {
                    $c2_import = false;
                }
                break;
            case '2'://仅连续剧注入
                if(!$is_series) //电影
                {
                    $c2_import = false;
                }
                break;
            case '3':
                $c2_import = false;
                break;
        }
        if(!$c2_import) //连续剧
        {
            $update_params = array(
                'nns_status' => 0,
                'nns_epg_status' => 97
            );
            \nl_c2_task::edit(\m_config::get_dc(),$update_params,$task_id);

            $queue_task_model = new \queue_task_model();
            $queue_task_model->q_report_task($task_id);
            $queue_task_model = null;
        }
        return \m_config::return_data('0','注入CDN');
    }
    /**
     * 获取等待注入C2的任务
     * @return array
     */
    public function get_wait_c2_task($limit_wait)
    {
        //注入分组【暂只有节目单注入使用】
        $str_group ='';
        if (isset($this->arr_sp_config[$this->str_video_type]['enabled']) && $this->arr_sp_config[$this->str_video_type]['enabled'] == '1')
        {
            $str_group = ' group by nns_src_id ';
        }
        $cdn_wait_params = array(
            'nns_org_id' => $this->str_sp_id,
            'nns_type' => $this->str_video_type,
            'nns_status' => 1,
        );
        return \nl_c2_task::timer_get_c2_task(\m_config::get_dc(),$cdn_wait_params,$limit_wait,$str_group,'order by nns_modify_time asc,nns_cdn_fail_time asc');
    }

    /**
     * 获取重试的C2任务
     * @return array
     */
    public function get_fail_c2_task($retry_time,$limit_fail)
    {
        //注入分组【暂只有节目单注入使用】
        $str_group ='';
        if (isset($this->arr_sp_config[$this->str_video_type]['enabled']) && $this->arr_sp_config[$this->str_video_type]['enabled'] == '1')
        {
            $str_group = ' group by nns_src_id ';
        }

        $cdn_fail_params = array(
            'nns_org_id' => $this->str_sp_id,
            'nns_type' => $this->str_video_type,
            'nns_status' => -1,
            'less_than' => array(
                'nns_cdn_fail_time' => $retry_time,
            ),
        );
        return \nl_c2_task::timer_get_c2_task(\m_config::get_dc(),$cdn_fail_params,$limit_fail,$str_group,'order by nns_modify_time asc,nns_cdn_fail_time asc');
    }
    /**
     * 根据C2任务类型，批量获取C2任务的基础信息
     * @param $data C2任务数据
     * array(
     *  array(),
     *  array()
     * )
     * @return array(
     *              'ret',
     *              'reason',
     *              'data_info'=>array(
     *                  array(
     *                      'c2_info'=>array(C2信息),
     *                      'video_info'=>array(
     *                          'base_info' => VIDEO信息
     *                          'ex_info'  => 扩展信息
     *                       ),
     *                      'index_info'=>array(
     *                          'base_info' => INDEX信息,
     *                          'ex_info' => 扩展信息,
     *                      ),
     *                      'media_info'=>array(
     *                          'base_info' => MEDIA信息
     *                          'ex_info' => 扩展信息
     *                      ),
     *                  )
     *             )
     *      )
     */
    public function query_mixed_info($data)
    {
        $new_c2_info = array();
        $ref_arr = array();//当前任务的主键ID
        $src_arr = array();//当前任务的父级主键ID
        $task_status = array();//记录当前任务状态
        //组装批量查询数据
        foreach ($data as $c2_info)
        {
            if(!empty($c2_info['nns_ref_id']))
            {
                $ref_arr[] = $c2_info['nns_ref_id'];
            }
            else
            {
                continue;
            }
            if(!empty($c2_info['nns_src_id']))
            {
                $src_arr[] = $c2_info['nns_src_id'];
            }
        }
        if(empty($ref_arr))
        {
            return \m_config::return_data(NS_CDS_FAIL,'入参参数data错误');
        }
        //根据类型获取不同的数据源
        switch ($this->str_video_type)
        {
            case 'live':
                //批量获取直播及扩展信息
                $live_result = \nl_live::query_live_by_id(\m_config::get_dc(),$ref_arr);
                if($live_result['ret'] != NS_CDS_SUCCE)
                {
                    return $live_result;
                }
                //数据组装
                $live_result = $live_result['data_info'];
                foreach ($data as $c2)
                {
                    $c2_data = array();
                    $c2_data['c2_info'] = $c2;
                    if(isset($live_result[$c2['nns_ref_id']]))
                    {
                        $c2_data['live_info'] = $live_result[$c2['nns_ref_id']];
                    }
                    $new_c2_info[] = $c2_data;
                    unset($c2_data);
                }
                break;
            case 'live_index':

                break;
            case 'live_media':
                //批量获取直播分集及扩展信息
                $live_index_result = \nl_live_index::query_live_index_by_id(\m_config::get_dc(),$src_arr);
                if($live_index_result['ret'] != NS_CDS_SUCCE)
                {
                    return $live_index_result;
                }
                $live_index_result = $live_index_result['data_info'];
                //批量获取直播片源及扩展信息
                $live_media_result = \nl_live_media::query_live_media_by_id(\m_config::get_dc(),$ref_arr);
                if($live_media_result['ret'] != NS_CDS_SUCCE)
                {
                    return $live_media_result;
                }
                $live_media_result = $live_media_result['data_info'];
                //批量获取直播及扩展信息
                $live_guid = array();
                foreach ($live_index_result as $index_re)
                {
                    $live_guid[$index_re['base_info']['nns_id']] = $index_re['base_info']['nns_live_id'];
                }
                $live_result = \nl_live::query_live_by_id(\m_config::get_dc(),array_values($live_guid));
                if($live_result['ret'] != NS_CDS_SUCCE)
                {
                    return $live_result;
                }
                $live_result = $live_result['data_info'];
                foreach ($data as $c2)
                {
                    $c2_data =  array();
                    $c2_data['c2_info'] = $c2;
                    if(isset($live_result[$live_guid[$c2['nns_src_id']]]))
                    {
                        $c2_data['live_info'] = $live_result[$live_guid[$c2['nns_src_id']]];
                    }
                    if(isset($live_index_result[$c2['nns_src_id']]))
                    {
                        $c2_data['live_index_info'] = $live_index_result[$c2['nns_src_id']];
                    }
                    if(isset($live_media_result[$c2['nns_ref_id']]))
                    {
                        $c2_data['live_media_info'] = $live_media_result[$c2['nns_ref_id']];
                    }
                    $new_c2_info[] = $c2_data;
                    unset($c2_data);
                }
                break;
            case 'playbill':
                //获取频道信息
                $live_result = \nl_live::query_live_by_id(\m_config::get_dc(), $src_arr);
                if($live_result['ret'] != NS_CDS_SUCCE)
                {
                    return $live_result;
                }
                $live_result = $live_result['data_info'];
                $new_playbill_data = array();
                if (isset($this->arr_sp_config[$this->str_video_type]['enabled']) && $this->arr_sp_config[$this->str_video_type]['enabled'] == '1')
                {
                    //批量获取频道下节目单信息
                    $group_num = (isset($this->arr_sp_config['playbill']['group_num']) && (int)$this->arr_sp_config['playbill']['group_num'] > 0)
                        ? (int)$this->arr_sp_config['playbill']['group_num'] : 100;
                    foreach ($src_arr as $v)
                    {
                        //首先根据分组的频道id获取c2队列中存在等待注入的节目单
                        $src_query = array(
                            'nns_src_id' => $v,
                            'nns_org_id' => $this->str_sp_id,
                            'nns_status' => NS_CDS_CDN_WAIT,
                        );
                        $c2_playbill_arr = \nl_c2_live_task::query_by_condition(\m_config::get_dc(), $src_query, 0, $group_num);
                        if ($c2_playbill_arr['ret'] != NS_CDS_SUCCE)
                        {
                            return $c2_playbill_arr;
                        }
                        $ref_id_arr = array();
                        $c2_id_arr = array();
                        foreach ($c2_playbill_arr['data_info'] as $items)
                        {
                            $ref_id_arr[] = $items['nns_ref_id'];
                            //将查出来的多个的c2队列数据进行保存,便于后期注入修改状态使用
                            $c2_id_arr[$items['nns_ref_id']] = $items['nns_id'];
                        }
                        //根据获取到的c2的节目单队列获取节目单信息
                        $playbill_result = \nl_playbill::query_by_ids(\m_config::get_dc(),$ref_id_arr);
                        $playbill_result['data_info'] = np_array_rekey($playbill_result['data_info'],'nns_id');
                        if($playbill_result['ret'] != NS_CDS_SUCCE)
                        {
                            return $playbill_result;
                        }
                        //将节目单的c2队列的GUID放到节目单信息中，便于后面信息更改
                        foreach ($ref_id_arr as $value)
                        {
                            if (isset($playbill_result['data_info'][$value])
                                && !empty($playbill_result['data_info'][$value]))
                            {
                                $playbill_result['data_info'][$value]['c2_task_id'] = $c2_id_arr[$value];
                            }
                        }
                        $new_playbill_data[$v] = $playbill_result['data_info'];
                    }
                }
                else
                {
                    //未进行分组注入的节目单数据
                    $playbill_result = \nl_playbill::query_playbill_by_id(\m_config::get_dc(),$ref_arr);
                    if($playbill_result['ret'] != NS_CDS_SUCCE)
                    {
                        return $playbill_result;
                    }
                    $new_playbill_data = $playbill_result['data_info'];
                }
                //数据组装
                foreach ($data as $c2)
                {
                    $c2_data = array();
                    $c2_data['c2_info'] = $c2;

                    //直播频道的数据
                    if(isset($live_result[$c2['nns_src_id']]))
                    {
                        $c2_data['live_info'] = $live_result[$c2['nns_src_id']];
                    }

                    //根据是否分组进行条件分配节目单信息
                    if (isset($this->arr_sp_config[$this->str_video_type]['enabled'])
                        && $this->arr_sp_config[$this->str_video_type]['enabled'] == '1'
                    && isset($new_playbill_data[$c2['nns_src_id']]))
                    {
                        $c2_data['playbill_info'] = $new_playbill_data[$c2['nns_src_id']];
                    }
                    else if (isset($new_playbill_data[$c2['nns_ref_id']]))
                    {
                        $c2_data['playbill_info'] = $new_playbill_data[$c2['nns_ref_id']];
                    }

                    $new_c2_info[] = $c2_data;
                    unset($c2_data);
                }
                break;
        }
        return \m_config::return_data(NS_CDS_SUCCE,'',$new_c2_info);
    }
}