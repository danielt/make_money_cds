<?php
namespace ns_model\message;
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load_old("nn_logic/message/nl_message.class.php");
\ns_core\m_load::load("ns_data_model.message.m_message_inout");
/**
 * 消息队列入库出库
 * @author pan.liang
 */
class message_queue 
{
    public $fuse_enabled = false;
    
	/**
     * @return the $fuse_enabled
     */
    public function getFuse_enabled()
    {
        return $this->fuse_enabled;
    }

    /**
     * @param boolean $fuse_enabled
     */
    public function setFuse_enabled($fuse_enabled=false)
    {
        $this->fuse_enabled = $fuse_enabled ? true : false;
    }

 /*
	  针对不同的注入模式，向消息队列内部写入消息
	  $message格式：
	  $message["id"] 消息ID 如果有，则保存，如果没有自动生成一个
	  $message["content"] 消息内容，这里一定是ADI内容或者是元数据内容了
	  $message["content_url"] 有些content可能是到这个地址去拉取的

	*/
	public function push($message)
	{
	    global $g_fuse_enabled;
	    $fuse_enabled = $g_fuse_enabled;
	    unset($g_fuse_enabled);
	    $fuse_enabled = (isset($fuse_enabled) && strlen($fuse_enabled) >0 && $fuse_enabled == '1') ? true : false;
	    if($fuse_enabled && $this->fuse_enabled)
	    {
	        $arr_fuse_cp=array();
	        $result_fuse_cp = \m_config::_query_fuse_cp_org($message['base_info']['nns_cp_id']);
	        if($result_fuse_cp['ret'] !=0)
	        {
	            return \m_config::return_data(1,$result_fuse_cp['reason']);
	        }
	        if(isset($result_fuse_cp['data_info']) && is_array($result_fuse_cp['data_info']) && !empty($result_fuse_cp['data_info']))
	        {
	            foreach ($result_fuse_cp['data_info'] as $data_val)
	            {
	                $message['base_info']['nns_cp_id'] = $data_val['nns_cp_id'];
	                $obj_excute = new \m_message_inout();
	                $result = $obj_excute->add($message);
	            }
	            return $result;
	        }
	    }
        $obj_excute = new \m_message_inout();
        return $obj_excute->add($message);
	}
	/*
	  根据不同的注入模式，在消息队列向外弹出消息，默认为1条
	  return格式  message对象
	*/
	public function pop($import_mode)
	{
	    $date = date("Y-m-d H:i:s");
	    $arr_cp_config = \m_config::_get_cp_info($import_mode);
	    if($arr_cp_config['ret'] !=0)
	    {
	        return $arr_cp_config;
	    }
	    if(!isset($arr_cp_config['data_info']) || empty($arr_cp_config['data_info']) || !is_array($arr_cp_config['data_info']))
	    {
	        return $arr_cp_config;
	    }
	    $cp_config = isset($arr_cp_config['data_info']['nns_config']) && !empty($arr_cp_config['data_info']['nns_config']) ? $arr_cp_config['data_info']['nns_config'] : null;
	    //失败次数 如果未配置  默认100
	    $fail_message_time = (isset($cp_config['fail_message_time']) && $cp_config['fail_message_time'] >= 0) ? $cp_config['fail_message_time'] : 100;
	    //消息反馈模式
	    //失败返回
	    $flag_fail_mode = (isset($cp_config['content_feedback_mode']) && $cp_config['content_feedback_mode'] == '1') ? true : false;
	    //始终反馈
	    $flag_always_mode = (isset($cp_config['content_feedback_mode']) && $cp_config['content_feedback_mode'] == '2') ? true : false;
	    //消息队列获取第一次消息注入的条数
	    $message_first_import_num = (isset($cp_config['message_first_import_num']) && $cp_config['message_first_import_num'] > 0 ) ? $cp_config['message_first_import_num'] : 50;
	    //失败消息注入开关 0|未配置 关闭 1开启
	    $fail_message_import_enable = (isset($cp_config['fail_message_import_enable']) && $cp_config['fail_message_import_enable'] == '1' ) ? true : false;
	    //失败消息获取条数
	    $fail_message_import_num = (isset($cp_config['fail_message_import_num']) && $cp_config['fail_message_import_num'] > 0 ) ? $cp_config['fail_message_import_num'] : 30;
	    //消息队列子级注入开关 0|未配置 关闭 1开启
	    $message_child_import_enable = (isset($cp_config['message_child_import_enable']) && $cp_config['message_child_import_enable'] == '1' ) ? true : false;
	    //消息队列子级注入条数
	    $message_child_import_num = (isset($cp_config['message_child_import_num']) && $cp_config['message_child_import_num'] > 0 ) ? $cp_config['message_child_import_num'] : 30;
	    
	    #TODO  这个作为策略可配置
	    if (rand(1, 100) == 3 && ($fail_message_import_enable || $message_child_import_enable))
	    {
	        $result_update_delete = \nl_message::logic_delete(\m_config::get_dc(), $date, $$import_mode, $fail_message_time);
	        if ($result_update_delete['ret'] != 0)
	        {
	            $nns_link_file=\nn_log::write_log_message(LOG_MODEL_MESSAGE, $result_update_delete['reason'], $import_mode);
	            \nl_global_error_log::add(\m_config::get_dc(), array('nns_desc'=>$result_update_delete['reason'],'nns_model'=>LOG_MODEL_MESSAGE,'nns_cp_id'=>$this->cp_id,'nns_link_file'=>$nns_link_file));
	            return ;
	        }
	    }
	    //读取队列
	    $result = \nl_message::query_timer_message_info_v2(\m_config::get_dc(), $import_mode,$message_first_import_num,$fail_message_import_enable,$fail_message_import_num,$message_child_import_enable,$message_child_import_num,$fail_message_time);
	    if ($result['ret'] != 0)
	    {
	        $nns_link_file=\nn_log::write_log_message(LOG_MODEL_MESSAGE, $result['reason'], $import_mode, $$import_mode);
	        \nl_global_error_log::add(\m_config::get_dc(), array('nns_desc'=>$result['reason'],'nns_model'=>LOG_MODEL_MESSAGE,'nns_cp_id'=>$import_mode,'nns_link_file'=>$nns_link_file));
	        return ;
	    }
	    $result = $result['data_info'];
	    return \m_config::return_data(0,'OK',$result);
	}
			
}