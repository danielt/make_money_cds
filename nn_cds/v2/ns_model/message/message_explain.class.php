<?php
namespace ns_model\message;
include_once dirname(dirname(dirname(__FILE__))).'/ns_core/m_load.class.php';
\ns_core\m_load::load("ns_model.message.message_logic");
\ns_core\m_load::load("ns_core.m_config");
/**
 * 消息队列解释器
 * @author pan.liang
 */
class message_explain 
{
    public $arr_message = null;
    
    public function __construct($arr_message = null)
    {
        $this->arr_message = $arr_message;
    }
    
    /**
     * 检查消息文件是否存在
     * @param unknown $message
     */
    public function get_message_content($message)
    {
        if(strlen($message['nns_message_url']) < 1 && !empty($message['nns_message_content']))
        {
            return \m_config::return_data(0,"ok",$message['nns_message_content']);
        }
        $nns_message_url = dirname(dirname(dirname(dirname(__FILE__)))).'/data/'.trim(trim(trim($message['nns_message_url'],'/'),'\\'));
        if(!file_exists($nns_message_url))
        {
            return \m_config::return_data(1,"消息GUID[{$message['nns_id']}]文件不存在，路径[{$nns_message_url}]");
        }
        $str_message_content =  file_get_contents($nns_message_url);
        return \m_config::return_data(0,"ok",$str_message_content);
    }
	
	/**
	 * 资源库栏目操作(点播、直播)
	 * @param string $action 操作行为  add 添加
	 * @param int $video_type 媒资类型  0 点播 | 1 直播
	 * @param array $message = array(
     *                              'base_info'=>array(
     *                                      'nns_message_id'=>'',  //上游消息ID
     *                                      'nns_cp_id'=>'xiaole', //上游CP标示
     *                                      'nns_message_xml'=>'xxxx',  //上游原始的信息  可以为字符串 | xml | json
     *                                      'nns_message_content'=>'xxxxx',  //上游解析后的信息  可以为字符串 | xml | json  生成队列的基本信息
     *                                      'nns_action'=>'1', //操作 行为
     *                                      'nns_type'=>'1', //消息 类型
     *                                      'nns_name'=>'电视剧|好先生',  消息名称
     *                                      'nns_package_id'=>'',  //包ID
     *                                      'nns_xmlurlqc'=>'', //广州电信悦ME MD5摘要
     *                                      'nns_encrypt'=>'', //广州电信悦ME 加密串
     *                                      'nns_content_number'=>'1', //xml文件中包含的内容数量
     *                              ), //基本信息（存储于nns_mgtvbk_message表中）
     *                         );
	 */
	public function message_action($action,$message,$is_need_ext = false, $epg_model = '', $cdn_model = '')
	{
	    \ns_core\m_load::load("ns_data_model.message.m_message_inout");
	    $obj_excute = new m_message_inout($this->arr_message);
	    if(strtolower($action) == 'query')
	    {
	        $result = $obj_excute->query($message,$is_need_ext,$epg_model,$cdn_model);
	    }
	    else if(strtolower($action) == 'export')
	    {
	        $result = $obj_excute->export();
	    }
	    else
	    {
	        $result = \m_config::return_data(1,"FUNC：[".__FUNCTION__."];不支持操作行为[{$action}]");
	    }
        unset($obj_excute);
        return $result;
	}

	/**
	 * 资源库栏目操作(点播、直播)
	 * @param string $action 操作行为   add 添加 | delete 删除 | query 查询 | export 导出模板
	 * add：
	 * array(
	 *     'base_info'=>array(
     *              'nns_name'=>'电影/测试',   //栏目名称支持多层目录，多层目录的时候 /分割
     *              'nns_import_category_id'=>'', //栏目名称支持多层目录，多层目录的时候 /分割
     *              'nns_cp_id'=>'manual_import',
     *              'nns_import_parent_category_id'=>'',
     *              'nns_video_type'=>'0',  媒资类型  0 点播 | 1 直播
     *      )
     * );
	 * @param array $category
	 */
	public function category_action($action,$category,$is_need_ext = false, $epg_model = '', $cdn_model = '')
	{
	    $category_v2 = get_config_v2('g_category_v2');
	    if($category_v2)
      {
          return $this->category_v2_action($action,$category,$is_need_ext = false, $epg_model = '', $cdn_model = '');
      }
      \ns_core\m_load::load("ns_data_model.category.m_category_inout");
	    $obj_excute = new \m_category_inout($this->arr_message);
	    if(strtolower($action) == 'add')
	    {
	        $result = $obj_excute->add($category);
	    }
	    else if(strtolower($action) == 'delete')
	    {
	        $result = $obj_excute->delete($category);
	    }
	    else if(strtolower($action) == 'query')
	    {
	        $result = $obj_excute->query($category);
	    }
	    else if(strtolower($action) == 'export')
	    {
	        $result = $obj_excute->export();
	    }
	    else
	    {
	        $result = \m_config::return_data(1,"FUNC：[".__FUNCTION__."];不支持操作行为[{$action}]");
	    }
	    unset($obj_excute);
	    return $result;
	}

    public function category_v2_action($action,$category,$is_need_ext = false, $epg_model = '', $cdn_model = '')
    {
        \ns_core\m_load::load("ns_data_model.category.m_category_v2_inout");
        $obj_excute = new \m_category_v2_inout($this->arr_message);
        if(strtolower($action) == 'add')
        {
            $result = $obj_excute->add($category);
            $obj_excute->write_message_info($result);
        }
        else if(strtolower($action) == 'delete')
        {
            $result = $obj_excute->delete($category);
            $obj_excute->write_message_info($result);
        }
        else if(strtolower($action) == 'query')
        {
            $result = $obj_excute->query($category);
            $obj_excute->write_message_info($result);
        }
        else if(strtolower($action) == 'export')
        {
            $result = $obj_excute->export();
        }
        else
        {
            $result = \m_config::return_data(1,"FUNC：[".__FUNCTION__."];不支持操作行为[{$action}]");
        }
        unset($obj_excute);
        return $result;
    }

    /**
     * 主媒资操作（增、删、改）
     * @param string $action 操作行为  add 添加 | edit 修改 | delete 删除 | query 查询 | export 导出模板（查询只提供绝对查询，没有like的查询）
     * @param array $vod 
     * add:
        array(
            'base_info'=>array(
                    'nns_name'=>'测试',
                    'nns_view_type'=>'0',
                    'nns_org_type'=>'0',
                    'nns_tag'=>'26,',
                    'nns_director'=>'刘德华',
                    'nns_actor'=>'刘德华演员',
                    'nns_show_time'=>'2017',
                    'nns_view_len'=>'2064',
                    'nns_all_index'=>'1',
                    'nns_new_index'=>'0',
                    'nns_area'=>'中国大陆',
                    'nns_image0'=>'http://172.26.2.17/nn_bk/data/downimg/prev/KsImg/cp_jscn/20171103/59fbe6b21fc57017df93c9ea9e440337.jpg?rand=1139054617',
                    'nns_image1'=>'http://172.26.2.17/nn_bk/data/downimg/prev/KsImg/cp_jscn/20171103/59fbe6b23c7e07ce8b7e296dd8c719b4.jpg',
                    'nns_image2'=>'ftp://image_jscn:image_jscn@172.26.2.17:21/prev/KsImg/cp_manual_import_pc/20170815/5992a236139d618a2d9bcf346193aceb.jpg',
                    'nns_image3'=>'cp_fuse_manual_import/20170822/599c4af94882cff8a1a6c18db1cc6d04.jpg',
                    'nns_image4'=>'',
                    'nns_image5'=>'',
                    'nns_summary'=>'描述',
                    'nns_remark'=>'影片标志',
                    'nns_category_id'=>'10000007001',
                    'nns_play_count'=>'0',
                    'nns_score_total'=>'0',
                    'nns_score_count'=>'0',
                    'nns_point'=>'0',
                    'nns_copyright_date'=>'2017',
                    'nns_asset_import_id'=>'import_id',
                    'nns_pinyin'=>'11',
                    'nns_pinyin_length'=>'2',
                    'nns_alias_name'=>'别名',
                    'nns_eng_name'=>'英文名称',
                    'nns_language'=>'简体中文',
                    'nns_text_lang'=>'字幕语言',
                    'nns_producer'=>'内容发布商',
                    'nns_screenwriter'=>'字幕作者',
                    'nns_play_role'=>'播放角色',
                    'nns_copyright_range'=>'版权商',
                    'nns_vod_part'=>'part',
                    'nns_keyword'=>'关键词',
                    'nns_import_source'=>'mgtv',
                    'nns_kind'=>'战争/爱情',
                    'nns_copyright'=>'芒果TV',
                    'nns_clarity'=>'nns_clarity',
                    'nns_image_v'=>'',
                    'nns_image_s'=>'',
                    'nns_image_h'=>'',
                    'nns_cp_id'=>'xiaole',
                    'nns_conf_info'=>'',
                    'nns_ext_url'=>'',
                ), //基本信息（存储于nns_vod表中）
           'ex_info'=>array(
                    'svc_item_id'=>'svc_item_id',
                    'month_clicks'=>'month_clicks',
                    'week_clicks'=>'week_clicks',
                    'base_id'=>'base_id',
                    'asset_path'=>'asset_path',
                    'ex_tag'=>'ex_tag',
                    'full_spell'=>'full_spell',
                    'awards'=>'awards',
                    'year'=>'year',
                    'play_time'=>'play_time',
                    'channel'=>'channel',
                    'first_spell'=>'first_spell',
            ), //扩展信息（存储于nns_vod_ex表中）
        ) 
        return:
           array (
              'ret' => 0,
              'reason' => 'OK',
              'data_info' => 
              array (
                'info' => 
                array (
                  'in' => 
                  array (
                    'base_info' => 
                    array (
                      'nns_name' => '测试',
                      'nns_view_type' => '0',
                      'nns_org_type' => '0',
                      'nns_tag' => '26,',
                      'nns_director' => '刘德华',
                      'nns_actor' => '刘德华演员',
                      'nns_show_time' => '2017',
                      'nns_view_len' => '2064',
                      'nns_all_index' => '1',
                      'nns_new_index' => '0',
                      'nns_area' => '中国大陆',
                      'nns_image0' => 'http://172.26.2.17/nn_bk/data/downimg/prev/KsImg/cp_jscn/20171103/59fbe6b21fc57017df93c9ea9e440337.jpg?rand=1139054617',
                      'nns_image1' => 'http://172.26.2.17/nn_bk/data/downimg/prev/KsImg/cp_jscn/20171103/59fbe6b23c7e07ce8b7e296dd8c719b4.jpg',
                      'nns_image2' => 'ftp://image_jscn:image_jscn@172.26.2.17:21/prev/KsImg/cp_manual_import_pc/20170815/5992a236139d618a2d9bcf346193aceb.jpg',
                      'nns_image3' => 'cp_fuse_manual_import/20170822/599c4af94882cff8a1a6c18db1cc6d04.jpg',
                      'nns_image4' => '',
                      'nns_image5' => '',
                      'nns_summary' => '描述',
                      'nns_remark' => '影片标志',
                      'nns_category_id' => '10000007001',
                      'nns_play_count' => '0',
                      'nns_score_total' => '0',
                      'nns_score_count' => '0',
                      'nns_point' => '0',
                      'nns_copyright_date' => '2017',
                      'nns_asset_import_id' => 'import_id',
                      'nns_pinyin' => '11',
                      'nns_pinyin_length' => '2',
                      'nns_alias_name' => '别名',
                      'nns_eng_name' => '英文名称',
                      'nns_language' => '简体中文',
                      'nns_text_lang' => '字幕语言',
                      'nns_producer' => '内容发布商',
                      'nns_screenwriter' => '字幕作者',
                      'nns_play_role' => '播放角色',
                      'nns_copyright_range' => '版权商',
                      'nns_vod_part' => 'part',
                      'nns_keyword' => '关键词',
                      'nns_import_source' => 'mgtv',
                      'nns_kind' => '战争/爱情',
                      'nns_copyright' => '芒果TV',
                      'nns_clarity' => 'nns_clarity',
                      'nns_image_v' => '',
                      'nns_image_s' => '',
                      'nns_image_h' => '',
                      'nns_cp_id' => 'xiaole',
                      'nns_conf_info' => '',
                      'nns_ext_url' => '',
                    ),
                    'ex_info' => 
                    array (
                      'svc_item_id' => 'svc_item_id',
                      'month_clicks' => 'month_clicks',
                      'week_clicks' => 'week_clicks',
                      'base_id' => 'base_id',
                      'asset_path' => 'asset_path',
                      'ex_tag' => 'ex_tag',
                      'full_spell' => 'full_spell',
                      'awards' => 'awards',
                      'year' => 'year',
                      'play_time' => 'play_time',
                      'channel' => 'channel',
                      'first_spell' => 'first_spell',
                    ),
                    'hide_info' => 
                    array (
                      'nns_org_id' => '7173fca93fbd535824299963a6f88e39',
                      'nns_depot_id' => 'dfdb6ee2bc1a2427ddac6f1f50d2d46f',
                    ),
                  ),
                  'leftout' => 
                  array (
                    'base_info' => 
                    array (
                    ),
                    'ex_info' => 
                    array (
                    ),
                    'hide_info' => 
                    array (
                    ),
                  ),
                  'out' => 
                  array (
                    'base_info' => 
                    array (
                      'nns_cp_id' => 'xiaole',
                      'nns_name' => '测试',
                      'nns_view_type' => '0',
                      'nns_org_type' => '0',
                      'nns_tag' => '26,',
                      'nns_director' => '刘德华',
                      'nns_actor' => '刘德华演员',
                      'nns_show_time' => '2017-11-20',
                      'nns_view_len' => '2064',
                      'nns_all_index' => '1',
                      'nns_new_index' => '0',
                      'nns_area' => '中国大陆',
                      'nns_image0' => '/prev/KsImg/cp_xiaole/video/20171120/22/5a12ec5320fa56f00b69893d5cfee5a7.jpg',
                      'nns_image1' => '/prev/KsImg/cp_xiaole/video/20171120/22/5a12ec5321ee4503e3ff83fbbadbc6aa.jpg',
                      'nns_image2' => '/prev/KsImg/cp_xiaole/video/20171120/22/5a12ec532331b62cda94bc485bb564b2.jpg',
                      'nns_image3' => '/prev/KsImg/cp_xiaole/video/20171120/22/5a12ec532833a53a87f02ce33eb15915.jpg',
                      'nns_image4' => '',
                      'nns_image5' => '',
                      'nns_summary' => '描述',
                      'nns_remark' => '影片标志',
                      'nns_category_id' => '10000007001',
                      'nns_play_count' => '0',
                      'nns_score_total' => '0',
                      'nns_score_count' => '0',
                      'nns_point' => '0',
                      'nns_copyright_date' => '2017-11-20',
                      'nns_asset_import_id' => 'import_id',
                      'nns_pinyin' => '11',
                      'nns_pinyin_length' => '2',
                      'nns_alias_name' => '别名',
                      'nns_eng_name' => '英文名称',
                      'nns_language' => '简体中文',
                      'nns_text_lang' => '字幕语言',
                      'nns_producer' => '内容发布商',
                      'nns_screenwriter' => '字幕作者',
                      'nns_play_role' => '播放角色',
                      'nns_copyright_range' => '版权商',
                      'nns_vod_part' => 'part',
                      'nns_keyword' => '关键词',
                      'nns_import_source' => 'mgtv',
                      'nns_kind' => '战争/爱情',
                      'nns_copyright' => '芒果TV',
                      'nns_clarity' => 'nns_clarity',
                      'nns_image_v' => '',
                      'nns_image_s' => '',
                      'nns_image_h' => '',
                      'nns_conf_info' => '',
                      'nns_ext_url' => '',
                      'nns_id' => '5a12ec5320d97af8b23e57afc993ac19',
                      'nns_org_id' => '7173fca93fbd535824299963a6f88e39',
                      'nns_depot_id' => 'dfdb6ee2bc1a2427ddac6f1f50d2d46f',
                      'nns_state' => '0',
                      'nns_status' => '1',
                      'nns_action' => '2',
                      'nns_deleted' => '0',
                      'nns_check' => '0',
                    ),
                    'ex_info' => 
                    array (
                      'svc_item_id' => 'svc_item_id',
                      'month_clicks' => 'month_clicks',
                      'week_clicks' => 'week_clicks',
                      'base_id' => 'base_id',
                      'asset_path' => 'asset_path',
                      'ex_tag' => 'ex_tag',
                      'full_spell' => 'full_spell',
                      'awards' => 'awards',
                      'year' => 'year',
                      'play_time' => 'play_time',
                      'channel' => 'channel',
                      'first_spell' => 'first_spell',
                    ),
                  ),
                ),
                'notice' => 
                array (
                  'base_info' => 
                  array (
                    'nns_image4' => '[默认值为:,设置为系统默认值]',
                    'nns_image5' => '[默认值为:,设置为系统默认值]',
                    'nns_image_v' => '[默认值为:,设置为系统默认值]',
                    'nns_image_s' => '[默认值为:,设置为系统默认值]',
                    'nns_image_h' => '[默认值为:,设置为系统默认值]',
                    'nns_conf_info' => '[默认值为:,设置为系统默认值]',
                    'nns_ext_url' => '[默认值为:,设置为系统默认值]',
                  ),
                  'miss' => 
                  array (
                    'hide_info' => 
                    array (
                      'nns_id' => '注入的参数缺失字段[nns_id]',
                      'nns_state' => '注入的参数缺失字段[nns_state]',
                      'nns_status' => '注入的参数缺失字段[nns_status]',
                      'nns_action' => '注入的参数缺失字段[nns_action]',
                      'nns_deleted' => '注入的参数缺失字段[nns_deleted]',
                      'nns_check' => '注入的参数缺失字段[nns_check]',
                    ),
                  ),
                  'hide_info' => 
                  array (
                    'nns_state' => '[默认值为:0,设置为系统默认值]',
                    'nns_status' => '[默认值为:1,设置为系统默认值]',
                    'nns_action' => '[默认值为:,设置为系统默认值]',
                    'nns_deleted' => '[默认值为:0,设置为系统默认值]',
                    'nns_check' => '[默认值为:0,设置为系统默认值]',
                  ),
                ),
              ),
              'ext_info' => NULL,
            )
     */
	public function vod_action($action,$vod,$is_need_ext = false, $epg_model = '', $cdn_model = '')
	{
	    \ns_core\m_load::load("ns_data_model.video.m_video_inout");
	    \ns_core\m_load::load("ns_data_model.video.m_index_inout");
	    \ns_core\m_load::load("ns_data_model.video.m_media_inout");
	    $obj_excute = new \m_video_inout($this->arr_message);
        if(strtolower($action) == 'add')
        {
            $result = $obj_excute->add($vod);
            $obj_excute->write_message_info($result);
        }
        else if(strtolower($action) == 'edit')
        {
            $result = $obj_excute->edit($vod);
            $obj_excute->write_message_info($result);
        }
        else if(strtolower($action) == 'delete')
        {
            $result = $obj_excute->delete($vod);
            $obj_excute->write_message_info($result);
        }
        else if(strtolower($action) == 'query')
        {
            $result = $obj_excute->query($vod,$is_need_ext,$epg_model,$cdn_model);
        }
        else if(strtolower($action) == 'export')
        {
            $result = $obj_excute->export();
        }
        else
        {
            $result = \m_config::return_data(1,"FUNC：[".__FUNCTION__."];不支持操作行为[{$action}]");
        }
        unset($obj_excute);
        return $result;
	}
	
	/**
	 * 分集操作（增、删、改）
	 * @param string $action 操作行为  add 添加 | edit 修改 | delete 删除 | query 查询 | export 导出模板（查询只提供绝对查询，没有like的查询）
	 * @param array $index
	 * add:
    	 array(
                'base_info'=>array(
                    'nns_name'=>'测试分集1',
                    'nns_index'=>'0',
                    'nns_time_len'=>'122',
                    'nns_summary'=>'分集描述',
                    'nns_image'=>'http://172.26.2.17/nn_bk/data/downimg/prev/KsImg/cp_jscn/20171103/59fbe6b21fc57017df93c9ea9e440337.jpg?rand=1139054617',
                    'nns_play_count'=>'0',
                    'nns_score_total'=>'0',
                    'nns_score_count'=>'0',
                    'nns_video_import_id'=>'import_id',
                    'nns_import_id'=>'import_index_id_1',
                    'nns_import_source'=>'mgtv',
                    'nns_actor'=>'刘德华',
                    'nns_director'=>'张学友',
                    'nns_release_time'=>date("Y-m-d H:i:s"),
                    'nns_update_time'=>date("Y"),
                    'nns_watch_focus'=>'分集看点',
                    'nns_cp_id'=>'xiaole',
                    'nns_conf_info'=>'',
                    'nns_ext_url'=>'',
                ),
                'ex_info'=>array(
                    "isintact" => "isintact",
                    "subordinate_name" => 'subordinate_name',
                    "initials" => 'initials',
                    "publisher" => 'publisher',
                    "first_spell" => 'first_spell',
                    "caption_language" => 'caption_language',
                    "language" => 'language',
                    "region" => 'region',
                    "adaptor" => 'adaptor',
                    "sreach_key" => 'sreach_key',
                    "event_tag" => 'event_tag',
                    "year" => 'year',
                    "sort_name" => 'sort_name',
                ),
            );
        return:
            array (
              'ret' => 0,
              'reason' => 'OK',
              'data_info' => 
              array (
                'info' => 
                array (
                  'in' => 
                  array (
                    'base_info' => 
                    array (
                      'nns_name' => '测试分集1',
                      'nns_index' => '0',
                      'nns_time_len' => '122',
                      'nns_summary' => '分集描述',
                      'nns_image' => 'http://172.26.2.17/nn_bk/data/downimg/prev/KsImg/cp_jscn/20171103/59fbe6b21fc57017df93c9ea9e440337.jpg?rand=1139054617',
                      'nns_play_count' => '0',
                      'nns_score_total' => '0',
                      'nns_score_count' => '0',
                      'nns_video_import_id' => 'import_id',
                      'nns_import_id' => 'import_index_id_1',
                      'nns_import_source' => 'mgtv',
                      'nns_actor' => '刘德华',
                      'nns_director' => '张学友',
                      'nns_release_time' => '2017-11-21 11:48:52',
                      'nns_update_time' => '2017',
                      'nns_watch_focus' => '分集看点',
                      'nns_cp_id' => 'xiaole',
                      'nns_conf_info' => '',
                      'nns_ext_url' => '',
                    ),
                    'ex_info' => 
                    array (
                      'isintact' => 'isintact',
                      'subordinate_name' => 'subordinate_name',
                      'initials' => 'initials',
                      'publisher' => 'publisher',
                      'first_spell' => 'first_spell',
                      'caption_language' => 'caption_language',
                      'language' => 'language',
                      'region' => 'region',
                      'adaptor' => 'adaptor',
                      'sreach_key' => 'sreach_key',
                      'event_tag' => 'event_tag',
                      'year' => 'year',
                      'sort_name' => 'sort_name',
                    ),
                    'hide_info' => 
                    array (
                      'nns_vod_id' => '5a12e1a126456746360aebdd7e3f95b6',
                    ),
                  ),
                  'leftout' => 
                  array (
                    'base_info' => 
                    array (
                    ),
                    'ex_info' => 
                    array (
                    ),
                    'hide_info' => 
                    array (
                    ),
                  ),
                  'out' => 
                  array (
                    'base_info' => 
                    array (
                      'nns_name' => '测试分集1',
                      'nns_index' => '0',
                      'nns_time_len' => '122',
                      'nns_summary' => '分集描述',
                      'nns_image' => '/prev/KsImg/cp_xiaole/index/20171121/11/5a13a2248704bda637ca689c201b3222.jpg',
                      'nns_play_count' => '0',
                      'nns_score_total' => '0',
                      'nns_score_count' => '0',
                      'nns_video_import_id' => 'import_id',
                      'nns_import_id' => 'import_index_id_1',
                      'nns_import_source' => 'mgtv',
                      'nns_actor' => '刘德华',
                      'nns_director' => '张学友',
                      'nns_release_time' => '2017-11-21 11:48:52',
                      'nns_update_time' => '2017-11-21',
                      'nns_watch_focus' => '分集看点',
                      'nns_cp_id' => 'xiaole',
                      'nns_conf_info' => '',
                      'nns_ext_url' => '',
                      'nns_id' => '5a13a22486d4538710a89ba8bd0c30f6',
                      'nns_vod_id' => '5a12e1a126456746360aebdd7e3f95b6',
                      'nns_new_media' => '',
                      'nns_status' => '1',
                      'nns_new_media_time' => '2017-11-21 11:48:52',
                      'nns_deleted' => '0',
                      'nns_action' => '2',
                      'nns_state' => '0',
                    ),
                    'ex_info' => 
                    array (
                      'isintact' => 'isintact',
                      'subordinate_name' => 'subordinate_name',
                      'initials' => 'initials',
                      'publisher' => 'publisher',
                      'first_spell' => 'first_spell',
                      'caption_language' => 'caption_language',
                      'language' => 'language',
                      'region' => 'region',
                      'adaptor' => 'adaptor',
                      'sreach_key' => 'sreach_key',
                      'event_tag' => 'event_tag',
                      'year' => 'year',
                      'sort_name' => 'sort_name',
                    ),
                  ),
                ),
                'notice' => 
                array (
                  'base_info' => 
                  array (
                    'nns_conf_info' => '[默认值为:,设置为系统默认值]',
                    'nns_ext_url' => '[默认值为:,设置为系统默认值]',
                  ),
                  'miss' => 
                  array (
                    'hide_info' => 
                    array (
                      'nns_id' => '注入的参数缺失字段[nns_id]',
                      'nns_new_media' => '注入的参数缺失字段[nns_new_media]',
                      'nns_status' => '注入的参数缺失字段[nns_status]',
                      'nns_new_media_time' => '注入的参数缺失字段[nns_new_media_time]',
                      'nns_deleted' => '注入的参数缺失字段[nns_deleted]',
                      'nns_action' => '注入的参数缺失字段[nns_action]',
                      'nns_state' => '注入的参数缺失字段[nns_state]',
                    ),
                  ),
                  'hide_info' => 
                  array (
                    'nns_new_media' => '[默认值为:,设置为系统默认值]',
                    'nns_status' => '[默认值为:1,设置为系统默认值]',
                    'nns_deleted' => '[默认值为:0,设置为系统默认值]',
                    'nns_action' => '[默认值为:,设置为系统默认值]',
                    'nns_state' => '[默认值为:0,设置为系统默认值]',
                  ),
                ),
              ),
              'ext_info' => NULL,
            )
	 */
	public function index_action($action,$index,$is_need_ext = false, $epg_model = '', $cdn_model = '')
	{
	    \ns_core\m_load::load("ns_data_model.video.m_video_inout");
	    \ns_core\m_load::load("ns_data_model.video.m_index_inout");
	    \ns_core\m_load::load("ns_data_model.video.m_media_inout");
	    $obj_excute = new \m_index_inout($this->arr_message);
        if(strtolower($action) == 'add')
        {
            $result = $obj_excute->add($index);
            $obj_excute->write_message_info($result);
        }
        else if(strtolower($action) == 'edit')
        {
            $result = $obj_excute->edit($index);
            $obj_excute->write_message_info($result);
        }
        else if(strtolower($action) == 'delete')
        {
            $result = $obj_excute->del($index);
            $obj_excute->write_message_info($result);
        }
        else if(strtolower($action) == 'query')
        {
            $result = $obj_excute->query($index,$is_need_ext,$epg_model,$cdn_model);
        }
        else if(strtolower($action) == 'export')
        {
            $result = $obj_excute->export();
        }
        else
        {
            $result = \m_config::return_data(1,"FUNC：[".__FUNCTION__."];不支持操作行为[{$action}]");
        }
        unset($obj_excute);
        return $result;
	}

	/**
	 * 
	 * @param string $action
	 * @param array $media
	 * add:
	 * array(
            'base_info'=>array(
                'nns_name'=>'测试片源1',
                'nns_type'=>'1',
                'nns_url'=>'test.ts',
                'nns_tag'=>'26,',
                'nns_mode'=>'hd',
                'nns_kbps'=>'0',
                'nns_content_id'=>'',
                'nns_content_state'=>'0',
                'nns_filetype'=>'ts',
                'nns_play_count'=>'0',
                'nns_score_total'=>'0',
                'nns_score_count'=>'0',
                'nns_video_import_id'=>'import_id',
                'nns_index_import_id'=>'import_index_id_1',
                'nns_import_id'=>'import_media_id_1',
                'nns_import_source'=>'mgtv',
                'nns_dimensions'=>'0',
                'nns_ext_url'=>'',
                'nns_file_size'=>234,
                'nns_file_time_len'=>111,
                'nns_file_frame_rate'=>'0',
                'nns_file_resolution'=>'1920*1080',
                'nns_cp_id'=>'xiaole',
                'nns_ext_info'=>'',
                'nns_drm_enabled'=>'0',
                'nns_drm_encrypt_solution'=>'',
                'nns_drm_ext_info'=>'',
                'nns_domain'=>'',
                'nns_media_type'=>'1',
                'nns_original_live_id'=>'',
                'nns_start_time'=>'0000-00-00 00:00:00',
                'nns_media_service'=>'',
                'nns_conf_info'=>'',
                'nns_encode_flag'=>'0',
                'nns_live_to_media'=>'0',
            ),
            'ex_info'=>array(
                "file_hash" => "file_hash",
                "file_width" => 'file_width',
                "file_height" => 'file_height',
                "file_scale" => 'file_scale',
                "file_coding" => 'file_coding',
            ),
        );
	 * return:
	 * array (
          'ret' => 0,
          'reason' => 'OK',
          'data_info' => 
          array (
            'info' => 
            array (
              'in' => 
              array (
                'base_info' => 
                array (
                  'nns_name' => '测试片源1',
                  'nns_type' => '1',
                  'nns_url' => 'test.ts',
                  'nns_tag' => '26,',
                  'nns_mode' => 'hd',
                  'nns_kbps' => '0',
                  'nns_content_id' => '',
                  'nns_content_state' => '0',
                  'nns_filetype' => 'ts',
                  'nns_play_count' => '0',
                  'nns_score_total' => '0',
                  'nns_score_count' => '0',
                  'nns_video_import_id' => 'import_id',
                  'nns_index_import_id' => 'import_index_id_1',
                  'nns_import_id' => 'import_media_id_1',
                  'nns_import_source' => 'mgtv',
                  'nns_dimensions' => '0',
                  'nns_ext_url' => '',
                  'nns_file_size' => 234,
                  'nns_file_time_len' => 111,
                  'nns_file_frame_rate' => '0',
                  'nns_file_resolution' => '1920*1080',
                  'nns_cp_id' => 'xiaole',
                  'nns_ext_info' => '',
                  'nns_drm_enabled' => '0',
                  'nns_drm_encrypt_solution' => '',
                  'nns_drm_ext_info' => '',
                  'nns_domain' => '',
                  'nns_media_type' => '1',
                  'nns_original_live_id' => '',
                  'nns_start_time' => '0000-00-00 00:00:00',
                  'nns_media_service' => '',
                  'nns_conf_info' => '',
                  'nns_encode_flag' => '0',
                  'nns_live_to_media' => '0',
                ),
                'ex_info' => 
                array (
                  'file_hash' => 'file_hash',
                  'file_width' => 'file_width',
                  'file_height' => 'file_height',
                  'file_scale' => 'file_scale',
                  'file_coding' => 'file_coding',
                ),
                'hide_info' => 
                array (
                  'nns_vod_id' => '5a12e1a126456746360aebdd7e3f95b6',
                  'nns_vod_index_id' => '5a139f94217dc5dbc129a58fb1902fa6',
                  'nns_vod_index' => '0',
                ),
              ),
              'leftout' => 
              array (
                'base_info' => 
                array (
                ),
                'ex_info' => 
                array (
                ),
                'hide_info' => 
                array (
                ),
              ),
              'out' => 
              array (
                'base_info' => 
                array (
                  'nns_name' => '测试片源1',
                  'nns_type' => '1',
                  'nns_url' => 'test.ts',
                  'nns_tag' => '26,',
                  'nns_mode' => 'hd',
                  'nns_kbps' => '0',
                  'nns_content_id' => '',
                  'nns_content_state' => '0',
                  'nns_filetype' => 'ts',
                  'nns_play_count' => '0',
                  'nns_score_total' => '0',
                  'nns_score_count' => '0',
                  'nns_video_import_id' => 'import_id',
                  'nns_index_import_id' => 'import_index_id_1',
                  'nns_import_id' => 'import_media_id_1',
                  'nns_import_source' => 'mgtv',
                  'nns_dimensions' => '0',
                  'nns_ext_url' => '',
                  'nns_file_size' => '234',
                  'nns_file_time_len' => '111',
                  'nns_file_frame_rate' => '0',
                  'nns_file_resolution' => '1920*1080',
                  'nns_cp_id' => 'xiaole',
                  'nns_ext_info' => '',
                  'nns_drm_enabled' => '0',
                  'nns_drm_encrypt_solution' => '',
                  'nns_drm_ext_info' => '',
                  'nns_domain' => '',
                  'nns_media_type' => '1',
                  'nns_original_live_id' => '',
                  'nns_start_time' => '0000-00-00 00:00:00',
                  'nns_media_service' => '',
                  'nns_conf_info' => '',
                  'nns_encode_flag' => '0',
                  'nns_live_to_media' => '0',
                  'nns_id' => '5a13cdff3060a117357959eef28a90cd',
                  'nns_vod_id' => '5a12e1a126456746360aebdd7e3f95b6',
                  'nns_vod_index_id' => '5a139f94217dc5dbc129a58fb1902fa6',
                  'nns_state' => '0',
                  'nns_deleted' => '0',
                  'nns_check' => '1',
                  'nns_vod_index' => '0',
                  'nns_media_caps' => 'VOD',
                  'nns_status' => '1',
                  'nns_action' => '1',
                  'nns_media_name' => '',
                  'nns_clip_state' => '0',
                ),
                'ex_info' => 
                array (
                  'file_hash' => 'file_hash',
                  'file_width' => 'file_width',
                  'file_height' => 'file_height',
                  'file_scale' => 'file_scale',
                  'file_coding' => 'file_coding',
                ),
              ),
            ),
            'notice' => 
            array (
              'base_info' => 
              array (
                'nns_content_id' => '[默认值为:,设置为系统默认值]',
                'nns_ext_url' => '[默认值为:,设置为系统默认值]',
                'nns_ext_info' => '[默认值为:,设置为系统默认值]',
                'nns_drm_encrypt_solution' => '[默认值为:,设置为系统默认值]',
                'nns_drm_ext_info' => '[默认值为:,设置为系统默认值]',
                'nns_domain' => '[默认值为:,设置为系统默认值]',
                'nns_original_live_id' => '[默认值为:,设置为系统默认值]',
                'nns_media_service' => '[默认值为:,设置为系统默认值]',
                'nns_conf_info' => '[默认值为:,设置为系统默认值]',
              ),
              'miss' => 
              array (
                'hide_info' => 
                array (
                  'nns_id' => '注入的参数缺失字段[nns_id]',
                  'nns_state' => '注入的参数缺失字段[nns_state]',
                  'nns_deleted' => '注入的参数缺失字段[nns_deleted]',
                  'nns_check' => '注入的参数缺失字段[nns_check]',
                  'nns_media_caps' => '注入的参数缺失字段[nns_media_caps]',
                  'nns_status' => '注入的参数缺失字段[nns_status]',
                  'nns_action' => '注入的参数缺失字段[nns_action]',
                  'nns_media_name' => '注入的参数缺失字段[nns_media_name]',
                  'nns_clip_state' => '注入的参数缺失字段[nns_clip_state]',
                ),
              ),
              'hide_info' => 
              array (
                'nns_state' => '[默认值为:0,设置为系统默认值]',
                'nns_deleted' => '[默认值为:0,设置为系统默认值]',
                'nns_check' => '[默认值为:1,设置为系统默认值]',
                'nns_media_caps' => '[默认值为:VOD,设置为系统默认值]',
                'nns_status' => '[默认值为:1,设置为系统默认值]',
                'nns_action' => '[默认值为:,设置为系统默认值]',
                'nns_media_name' => '[默认值为:,设置为系统默认值]',
                'nns_clip_state' => '[默认值为:0,设置为系统默认值]',
              ),
            ),
          ),
          'ext_info' => NULL,
        )
	 */
	public function media_action($action,$media,$is_need_ext = false, $epg_model = '', $cdn_model = '')
	{
	    \ns_core\m_load::load("ns_data_model.video.m_video_inout");
	    \ns_core\m_load::load("ns_data_model.video.m_index_inout");
	    \ns_core\m_load::load("ns_data_model.video.m_media_inout");
	    $obj_excute = new \m_media_inout($this->arr_message);
        if(strtolower($action) == 'add')
        {
            $result = $obj_excute->add($media);
            $obj_excute->write_message_info($result);
        }
        else if(strtolower($action) == 'edit')
        {
            $result = $obj_excute->edit($media);
            $obj_excute->write_message_info($result);
        }
        else if(strtolower($action) == 'delete')
        {
            $result = $obj_excute->del($media);
            $obj_excute->write_message_info($result);
        }
        else if(strtolower($action) == 'query')
        {
            $result = $obj_excute->query($media,$is_need_ext,$epg_model,$cdn_model);
        }
        else if(strtolower($action) == 'export')
        {
            $result = $obj_excute->export();
        }
        else
        {
            $result = \m_config::return_data(1,"FUNC：[".__FUNCTION__."];不支持操作行为[{$action}]");
        }
        unset($obj_excute);
        return $result;
	}

    /**
     * @description:直播频道操作
     * @author:xinxin.deng
     * @date: 2017/12/7 18:37
     * @param $action
     * @param $channel
     * @return array
     */
	public function live_action($action, $channel,$is_need_ext = false, $epg_model = '', $cdn_model = '')
    {
        \ns_core\m_load::load("ns_data_model.live.m_channel_inout");
        $obj_execute = new \m_channel_inout($this->arr_message);
        if(strtolower($action) == 'add')
        {
            $result = $obj_execute->add($channel);
        }
        else if(strtolower($action) == 'edit')
        {
            $result = $obj_execute->edit($channel);
        }
        else if(strtolower($action) == 'delete')
        {
            $result = $obj_execute->del($channel);
        }
        else if(strtolower($action) == 'query')
        {
            $result = $obj_execute->query($channel,$is_need_ext,$epg_model,$cdn_model);
        }
        else if(strtolower($action) == 'export')
        {
            $result = $obj_execute->export();
        }
        else
        {
            $result = \m_config::return_data(1,"FUNC：[".__FUNCTION__."];不支持操作行为[{$action}]");
        }
        unset($obj_execute);
        return $result;
    }

    /**
     * @description:直播频道分集操作
     * @author:xinxin.deng
     * @date: 2017/12/7 18:37
     * @param $action
     * @param $index
     * @return array
     */
    public function live_index_action($action, $index,$is_need_ext = false, $epg_model = '', $cdn_model = '')
    {
        \ns_core\m_load::load("ns_data_model.live.m_channel_index_inout");
        $obj_execute = new \m_channel_index_inout($this->arr_message);
        if(strtolower($action) == 'add')
        {
            $result = $obj_execute->add($index);
        }
        else if(strtolower($action) == 'edit')
        {
            $result = $obj_execute->edit($index);
        }
        else if(strtolower($action) == 'delete')
        {
            $result = $obj_execute->del($index);
        }
        else if(strtolower($action) == 'query')
        {
            $result = $obj_execute->query($index,$is_need_ext,$epg_model,$cdn_model);
        }
        else if(strtolower($action) == 'export')
        {
            $result = $obj_execute->export();
        }
        else
        {
            $result = \m_config::return_data(1,"FUNC：[".__FUNCTION__."];不支持操作行为[{$action}]");
        }
        unset($obj_execute);
        return $result;
    }

    /**
     * @description:直播频道片源操作
     * @author:xinxin.deng
     * @date: 2017/12/7 18:37
     * @param $action
     * @param $media
     * @return array
     */
    public function live_media_action($action, $media,$is_need_ext = false, $epg_model = '', $cdn_model = '')
    {
        \ns_core\m_load::load("ns_data_model.live.m_channel_media_inout");
        $obj_execute = new \m_channel_media_inout($this->arr_message);
        if(strtolower($action) == 'add')
        {
            $result = $obj_execute->add($media);
        }
        else if(strtolower($action) == 'edit')
        {
            $result = $obj_execute->edit($media);
        }
        else if(strtolower($action) == 'delete')
        {
            $result = $obj_execute->del($media);
        }
        else if(strtolower($action) == 'query')
        {
            $result = $obj_execute->query($media,$is_need_ext,$epg_model,$cdn_model);
        }
        else if(strtolower($action) == 'export')
        {
            $result = $obj_execute->export();
        }
        else
        {
            $result = \m_config::return_data(1,"FUNC：[".__FUNCTION__."];不支持操作行为[{$action}]");
        }
        unset($obj_execute);
        return $result;
    }

    /**
     * @description:节目单操作
     * @author:xinxin.deng
     * @date: 2017/12/7 18:37
     * @param $action
     * @param $playbill
     * @return array
     */
    public function playbill_action($action, $playbill,$is_need_ext = false, $epg_model = '', $cdn_model = '')
    {
        \ns_core\m_load::load("ns_data_model.playbill.m_playbill_inout");
        $obj_execute = new \m_playbill_inout($this->arr_message);
        if(strtolower($action) == 'add')
        {
            $result = $obj_execute->add($playbill);
        }
        else if(strtolower($action) == 'edit')
        {
            $result = $obj_execute->edit($playbill);
        }
        else if(strtolower($action) == 'delete')
        {
            $result = $obj_execute->del($playbill);
        }
        else if(strtolower($action) == 'query')
        {
            $result = $obj_execute->query($playbill,$is_need_ext,$epg_model,$cdn_model);
        }
        else if(strtolower($action) == 'export')
        {
            $result = $obj_execute->export();
        }
        else
        {
            $result = \m_config::return_data(1,"FUNC：[".__FUNCTION__."];不支持操作行为[{$action}]");
        }
        unset($obj_execute);
        return $result;
    }

	public function trans_message_action($trans)
    {

    }

    /**
     * @description:打点信息操作
     * @author:jing.chen
     * @date: 2017/12/13
     * @param $action
     * @param $seekpoint
     * @return array
     */
    public function seekpoint_action($action, $seekpoint,$is_need_ext = false, $epg_model = '', $cdn_model = '',$is_add_queue=true)
    {
        \ns_core\m_load::load("ns_data_model.seekpoint.m_seekpoint_inout");
        \ns_core\m_load::load("ns_data_model.video.m_index_inout");
        \ns_core\m_load::load("ns_data_model.live.m_channel_index_inout");
        $obj_execute = new \m_seekpoint_inout($this->arr_message);
        if(strtolower($action) == 'add')
        {
            $result = $obj_execute->add($seekpoint , $is_add_queue);
        }
        else if(strtolower($action) == 'edit')
        {
            $result = $obj_execute->edit($seekpoint , $is_add_queue);
        }
        else if(strtolower($action) == 'delete')
        {
            $result = $obj_execute->del($seekpoint , $is_add_queue);
        }
        else if(strtolower($action) == 'query')
        {
            $result = $obj_execute->query($seekpoint,$is_need_ext,$epg_model,$cdn_model);
        }
        else if(strtolower($action) == 'export')
        {
            $result = $obj_execute->export();
        }
        else
        {
            $result = \m_config::return_data(1,"FUNC：[".__FUNCTION__."];不支持操作行为[{$action}]");
        }
        unset($obj_execute);
        return $result;
    }

    /**
     * @description:明星库注入
     * @author:xinxin.deng
     * @date: 2017/12/15 9:34
     * @param $action
     * @param $actor
     * @param bool $is_need_ext
     * @param string $epg_model
     * @param string $cdn_model
     * @return array|multitype
     */
    public function actor_action($action, $actor,$is_need_ext = false, $epg_model = '', $cdn_model = '')
    {
        \ns_core\m_load::load("ns_data_model.actor.m_actor_inout");
        $obj_execute = new \m_actor_inout($this->arr_message);
        if(strtolower($action) == 'add')
        {
            $result = $obj_execute->add($actor);
        }
        else if(strtolower($action) == 'edit')
        {
            $result = $obj_execute->edit($actor);
        }
        else if(strtolower($action) == 'delete')
        {
            $result = $obj_execute->del($actor);
        }
        else if(strtolower($action) == 'query')
        {
            $result = $obj_execute->query($actor,$is_need_ext,$epg_model,$cdn_model);
        }
        else if(strtolower($action) == 'export')
        {
            $result = $obj_execute->export();
        }
        else
        {
            $result = \m_config::return_data(1,"FUNC：[".__FUNCTION__."];不支持操作行为[{$action}]");
        }
        unset($obj_execute);
        return $result;
    }

    /**
     * @description:透传队列操作
     * @author:jing.chen
     * @date: 2017/12/25
     * @param $action
     * @param $pass_queue
     * @return array
     */
    public function pass_queue_action($action, $pass_queue)
    {
        \ns_core\m_load::load("ns_data_model.queue.m_pass_queue_inout");
        $obj_execute = new \m_pass_queue_inout($this->arr_message);
        if(strtolower($action) == 'add')
        {
            $result = $obj_execute->add($pass_queue);
        }
        else
        {
            $result = \m_config::return_data(1,"FUNC：[".__FUNCTION__."];不支持操作行为[{$action}]");
        }
        unset($obj_execute);
        return $result;
    }
    /**
     * @description:媒资上下线
     * @author:jing.chen
     * @date: 2017/12/25
     * @param $action
     * @param $pass_queue
     * @return array
     */
    public function asset_online_action($action, $asset_online)
    {
        \ns_core\m_load::load("ns_data_model.line.m_asset_online_inout");
        $obj_execute = new \m_asset_online_inout($this->arr_message);
        if(strtolower($action) == 'add')
        {
            $result = $obj_execute->add($asset_online);
            $obj_execute->write_message_info($result);
        }
        else
        {
            $result = \m_config::return_data(1,"FUNC：[".__FUNCTION__."];不支持操作行为[{$action}]");
        }
        unset($obj_execute);
        return $result;
    }

    /**
     * EPGFileSet
     * @param $action
     * @param $epg_file_set
     * @param bool $is_need_ext
     * @param string $epg_model
     * @param string $cdn_model
     * @return array
     * xinxin.deng 2018/11/8 15:52
     */
    public function epg_file_set_action($action, $epg_file_set, $is_need_ext = false, $epg_model = '', $cdn_model = '')
    {
        \ns_core\m_load::load("ns_data_model.epgfile.m_epg_file_set_inout");
        \ns_core\m_load::load("ns_data_model.epgfile.m_epg_file_inout");
        $obj_execute = new \m_epg_file_set_inout($this->arr_message);
        if(strtolower($action) == 'add')
        {
            $result = $obj_execute->add($epg_file_set);
            $obj_execute->write_message_info($result);
        }
        else if(strtolower($action) == 'edit')
        {
            $result = $obj_execute->edit($epg_file_set);
            $obj_execute->write_message_info($result);
        }
        else if(strtolower($action) == 'delete')
        {
            $result = $obj_execute->delete($epg_file_set);
            $obj_execute->write_message_info($result);
        }
        else if(strtolower($action) == 'query')
        {
            $result = $obj_execute->query($epg_file_set, $is_need_ext, $epg_model, $cdn_model);
        }
        else
        {
            $result = \m_config::return_data(NS_CDS_FAIL, "FUNC：[".__FUNCTION__."];不支持操作行为[{$action}]");
        }

        unset($obj_execute);
        return $result;
    }

    /**
     * EPGFile
     * @param $action
     * @param $epg_file
     * @param bool $is_need_ext
     * @param string $epg_model
     * @param string $cdn_model
     * @return array
     * xinxin.deng 2018/11/8 15:52
     */
    public function epg_file_action($action, $epg_file, $is_need_ext = false, $epg_model = '', $cdn_model = '')
    {
        \ns_core\m_load::load("ns_data_model.epgfile.m_epg_file_set_inout");
        \ns_core\m_load::load("ns_data_model.epgfile.m_epg_file_inout");

        $obj_execute = new \m_epg_file_inout($this->arr_message);
        if(strtolower($action) == 'add')
        {
            $result = $obj_execute->add($epg_file);
            $obj_execute->write_message_info($result);
        }
        else if(strtolower($action) == 'edit')
        {
            $result = $obj_execute->edit($epg_file);
            $obj_execute->write_message_info($result);
        }
        else if(strtolower($action) == 'delete')
        {
            $result = $obj_execute->delete($epg_file);
            $obj_execute->write_message_info($result);
        }
        else if(strtolower($action) == 'query')
        {
            $result = $obj_execute->query($epg_file, $is_need_ext, $epg_model, $cdn_model);
        }
        else
        {
            $result = \m_config::return_data(NS_CDS_FAIL, "FUNC：[".__FUNCTION__."];不支持操作行为[{$action}]");
        }

        unset($obj_execute);
        return $result;
    }
}