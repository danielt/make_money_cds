<?php
namespace ns_model\clip;

\ns_core\m_load::load("ns_model.command.command");
\ns_core\m_load::load("ns_model.command.command_task");
\ns_core\m_load::load("ns_model.clip.clip_explain");
/*
中心注入指令的操作类
提供入栈，出栈方法
*/
class clip_queue extends \ns_model\clip\clip_explain
{

    public function explain()
    {
        /***************效验是否允许切片******************/
        if(isset($this->arr_sp_config['disabled_clip']) && (int)$this->arr_sp_config['disabled_clip'] !== 0)
        {
            return \m_config::return_data(NS_CDS_FAIL,"sp信息获取spID[{$this->str_sp_id}]不允许切片");
        }
        return \m_config::return_data(NS_CDS_SUCCE,"ok");
    }

	public function push($import_mode,$command){

	}

    /**
     * 弹出中心同步指令需要切片的队列
     * @param int $num
     * @param string $video_type
     * @return array
     */
	public function pop($num = 1, $video_type = 'media')
    {
        //类型对应
        switch ($video_type)
        {
            case NS_CDS_MEDIA:
                $nns_video_type = 0;
                break;
            case NS_CDS_LIVE_MEDIA:
                $nns_video_type = 1;
                break;
            case NS_CDS_FILE:
                $nns_video_type = 2;
                break;
            case NS_CDS_EPG_FILE:
                $nns_video_type = 3;
                break;
            default:
                $nns_video_type = 0;
                break;
        }
        //根据策略获取现行数量
        $r_num = $this->get_loading_num();

        if ($r_num['ret'] !=0)
        {
            return \m_config::return_data(NS_CDS_FAIL, '查询切片队列失败,' . $r_num['reason']);
        }
        $limit = null;
        if ((int)$num <= (int)$r_num['data_info']['num'])
        {
            return \m_config::return_data(NS_CDS_FAIL, '切片队列该【' . $video_type . '】类型已经达到等待最大值');
        }
        else
        {
            $limit = (int)$num - (int)$r_num['data']['num'];
        }

        $op_queue = $this->get_op_queue_list($video_type, $limit);

        if($op_queue['ret'] != NS_CDS_SUCCE || !is_array($op_queue['data_info']) || empty($op_queue['data_info']))
        {
            return \m_config::return_data(NS_CDS_FAIL,'中心同步指令注入数据队列查询失败或者数据为空');
        }
        $clip_params = array();
        foreach ($op_queue['data_info'] as $items)
        {
            $clip_params[] = array(
                'nns_video_type' => $nns_video_type,
                'nns_video_id' => $items['nns_video_id'],
                'nns_video_index_id' => $items['nns_index_id'],
                'nns_video_media_id' => $items['nns_media_id'],
                'nns_task_name' => $items['nns_name'],
                'nns_command_task_id' => $items['nns_command_task_id'],
                'nns_op_id' => $items['nns_id'],
                'nns_org_id' => $this->str_sp_id,
                'nns_priority' => isset($items['nns_weight']) ? (int)$items['nns_weight'] : 0,
                'nns_cp_id' => isset($items['nns_cp_id']) ? $items['nns_cp_id'] : '',
            );
        }
        return \m_config::return_data(NS_CDS_SUCCE,'ok', $clip_params);
    }

}