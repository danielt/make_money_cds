<?php
namespace ns_model\clip;

\ns_core\m_load::load("ns_core.nn_timer");
\ns_core\m_load::load("ns_core.m_data_model_inout_put");
/*
* 消息解释基础类，针对不同的消息
*/
class clip_explain
{
    public $clip_mode;
    public $str_sp_id = null;
    public $arr_sp_config = null;
    public $str_video_type = null;

    /**
     * clip_explain constructor.
     * @param $sp_id
     * @param string $video_type
     */
    public function __construct($sp_id,$video_type)
    {
        $this->str_sp_id = $sp_id;
        $arr_sp_config = \m_config::_get_sp_info($sp_id);
        $arr_sp_config = isset($arr_sp_config['data_info']['nns_config']) ? $arr_sp_config['data_info']['nns_config'] : null;
        $this->arr_sp_config = $arr_sp_config;
        $this->str_video_type = $video_type;
    }

    /**
     * 获取正在执行的条数
     * @return array
     */
    public function get_loading_num()
    {
        $sql = "select count(nns_id) as num from nns_mgtvbk_clip_task where nns_org_id='".$this->str_sp_id."' and (ISNULL(nns_state) or nns_state='' or nns_state='add')";
        return \nl_clip_task::query_by_sql(\m_config::get_dc(), $sql);
    }

    /**
     * 获取等待切片的中心同步指令队列
     * @param string $video_type
     * @param null $limit
     * @return array
     */
    public function get_op_queue_list($video_type='media', $limit = null)
    {
        //排序
        $order=empty($this->arr_sp_config['op_task_order']) ? 'nns_weight desc,nns_release_time desc,nns_video_id asc,nns_op_mtime asc':$this->arr_sp_config['op_task_order'];

        //冻结时间获取
        $freeze_time=empty($this->arr_sp_config['op_task_freeze_time']) ? NULL : date($this->arr_sp_config['op_task_freeze_time']);
        $freeze_time_sql='';
        if (!empty($freeze_time)){
            $freeze_time=strtotime($freeze_time)*1000;
            $freeze_time_sql=' and nns_op_mtime<'.$freeze_time;
        }

        $limit_sql = '';
        if (!empty($limit))
        {
            $limit_sql = ' limit '.$limit;
        }

        $sql='select * from nns_mgtvbk_op_queue' .
            ' where nns_type="'.$video_type.'" ' .
            ' and nns_status="'.NS_CDS_OP_WAIT_CLIP.'" ' .
            ' and nns_org_id="'.$this->str_sp_id.'" ' .$freeze_time_sql.
            ' and nns_state="0" ' .
            '  order by ' .$order. $limit_sql;

        return \nl_op_queue::query_by_sql(\m_config::get_dc(), $sql);
    }
}