<?php
    /**
     * Created by <xinxin.deng>.
     * Author: xinxin.deng
     * Date: 2018/10/31 10:31
     */
    namespace ns_model\pass_queue;
    class pass_queue_explain
    {
        public $str_sp_id = null;
        public $arr_sp_config = null;

        public function __construct($sp_id)
        {
            $this->str_sp_id = $sp_id;
            $arr_sp_info = \m_config::_get_sp_info($sp_id);
            $arr_sp_config = isset($arr_sp_info['data_info']['nns_config']) ? $arr_sp_info['data_info']['nns_config'] : null;
            $this->arr_sp_config = $arr_sp_config;
        }
    }