<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/10/31 10:33
 */
namespace ns_model\pass_queue;
include_once dirname(dirname(dirname(__FILE__)))."/common.php";
\ns_core\m_load::load("ns_model.pass_queue.pass_queue_explain");

class pass_queue extends \ns_model\pass_queue\pass_queue_explain
{
    /**
     * 对透传队列注入队列的解释
     * 确认队列执行条件等
     */
    public function explain()
    {
        /***************效验是否允许透传队列注入******************/
        if(isset($this->arr_sp_config['pass_queue_disabled']) && (int)$this->arr_sp_config['pass_queue_disabled'] != 1)
        {
            return \m_config::return_data(NS_CDS_FAIL,"sp信息获取spID[{$this->str_sp_id}]不允许透传队列注入");
        }
        else
        {
                return \m_config::return_data(NS_CDS_SUCCE,"sp信息获取spID[{$this->str_sp_id}]允许透传队列注入");
        }
    }

    /**
     * 添加透传队列
     */
    public function push()
    {

    }

    /**
     * 弹出透传队列
     * @return array
     * array (
     * ['0'] => Array (1)
    |    |    (
    |    |    |    ['pass_queue_task_info'] => Array (15)
    |    |    |    (
    |    |    |    |    ['nns_id'] = String(32) "5ae2bb7632c7f34d67707fd2285e66e3"
    |    |    |    |    ['nns_action'] = String(1) "1"
    |    |    |    |    ['nns_type'] = String(1) "0"
    |    |    |    |    ['nns_message_id'] = String(32) "626B197A94044563A8B9C5170C923863"
    |    |    |    |    ['nns_org_id'] = String(4) "hndx"
    |    |    |    |    ['nns_cp_id'] = String(7) "ZTEORHW"
    |    |    |    |    ['nns_audit'] = String(1) "1"
    |    |    |    |    ['nns_pause'] = String(1) "0"
    |    |    |    |    ['nns_status'] = String(1) "1"
    |    |    |    |    ['nns_content'] = String(170) "[{"category_id":"883BF531-668E-47CA-A2B8-C35D40007B18","parent_category":"A98CB098-B318-456F-B8A9-BAC1E96DE23D","name":"\u5730\u65b9\u620f\u66f2","display":"1","sort":7}]"
    |    |    |    |    ['nns_result_desc'] = String(0) ""
    |    |    |    |    ['nns_queue_time'] = String(18) "201804271356068126"
    |    |    |    |    ['nns_create_time'] = String(19) "2018-04-27 13:56:06"
    |    |    |    |    ['nns_modify_time'] = String(19) "2018-11-01 09:29:25"
    |    |    |    |    ['nns_queue_execute_url'] = String(78) "global_log/timer/pass_queue/hndx/ZTEORHW/1/cds_default_type/20181101/09/29.txt"
    |    |    |    )
    |    |    )
    |    |    ['1'] => Array (1)
    |    |    (
    |    |    |    ['pass_queue_task_info'] => Array (15)
    |    |    |    (
    |    |    |    |    ['nns_id'] = String(32) "5ae2bd801b4eff3ff40eb21781fb851d"
    |    |    |    |    ['nns_action'] = String(1) "1"
    |    |    |    |    ['nns_type'] = String(1) "0"
    |    |    |    |    ['nns_message_id'] = String(32) "626B197A94044563A8B9C5170C923862"
    |    |    |    |    ['nns_org_id'] = String(4) "hndx"
    |    |    |    |    ['nns_cp_id'] = String(7) "ZTEORHW"
    |    |    |    |    ['nns_audit'] = String(1) "1"
    |    |    |    |    ['nns_pause'] = String(1) "0"
    |    |    |    |    ['nns_status'] = String(1) "1"
    |    |    |    |    ['nns_content'] = String(170) "[{"category_id":"883BF531-668E-45CA-A2B8-C35D40007B18","parent_category":"A98CB098-B318-456F-B8A9-BAC1E96DE23D","name":"\u5730\u65b9\u620f\u66f2","display":"1","sort":7}]"
    |    |    |    |    ['nns_result_desc'] = String(0) ""
    |    |    |    |    ['nns_queue_time'] = String(18) "201804271404484363"
    |    |    |    |    ['nns_create_time'] = String(19) "2018-04-27 14:04:48"
    |    |    |    |    ['nns_modify_time'] = String(19) "2018-11-01 09:29:28"
    |    |    |    |    ['nns_queue_execute_url'] = String(78) "global_log/timer/pass_queue/hndx/ZTEORHW/1/cds_default_type/20181101/09/29.txt"
    |    |    |    )
    |    |    )
     * );
     */
    public function pop()
    {
        $new_pass_queue_data = array();
        $pass_params = array(
            'nns_org_id' => $this->str_sp_id,
            'nns_audit' => 1,//审核通过
            'nns_pause' => 0,//运行状态
            'nns_status' => 1,//等待注入
        );
        $pass_queue = \nl_pass_queue::get_pass_queue_for_timer(\m_config::get_dc(), $pass_params, 100);
        if ($pass_queue['ret'] != 0 || empty($pass_queue['data_info']) || !is_array($pass_queue['data_info']))
        {
            return \m_config::return_data(NS_CDS_FAIL,'透传队列指令注入数据队列查询失败或者数据为空');
        }
        else
        {
            foreach ($pass_queue['data_info'] as $item)
            {
                $pass_queue_data = array();
                $item['nns_content'] = html_entity_decode($item['nns_content']);//反格式化
                $pass_queue_data['pass_queue_task_info'] = $item;
                $new_pass_queue_data[] = $pass_queue_data;
                unset($pass_queue_data);
            }
            return \m_config::return_data(NS_CDS_SUCCE,'',$new_pass_queue_data);
        }
    }
}