<?php
$dirname = dirname(__FILE__);
$evn = require $dirname."/evn.config.php";
include_once $dirname."/ns_core/m_load.class.php";
include_once $dirname."/ns_core/m_const.php";
\ns_core\m_load::load("ns_core.m_msg_callback");
\ns_core\m_load::load_old("nn_logic/cache/view_cache.class.php");
class evn
{
    /**
     * 获取全局参数
     * @param $key 获取值
     * @return array|mixed|null
     */
    static public function get($key)
    {
        global $evn;

        $keys = explode(".", $key);
        $data = $evn;
        foreach ($keys as $k)
        {
            if ($data[$k] == null)
            {
                return null;
            }
            $data = $data[$k];
        }

        return $data;
    }

}