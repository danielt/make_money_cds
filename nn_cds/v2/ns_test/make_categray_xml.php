<?php
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
set_time_limit(0);
include_once dirname(dirname(__FILE__))."/common.php";
\ns_core\m_load::load("ns_core.m_config");


function sql_result($i,$parent_id)
{
    $sql="select * from nns_sync_category where nns_hierarchy='{$i}' and nns_parent_id='{$parent_id}'";
    $result = nl_query_by_db($sql, m_config::get_dc()->db());
    return $result;
}


function sql_update($nns_id,$nns_parent_id,$nns_hierarchy,$nns_cms_asset_id,$nns_cms_category_id,$nns_cms_category_name)
{
    $sql="update nns_sync_category set nns_cms_asset_id='{$nns_cms_asset_id}',nns_cms_category_id='{$nns_cms_category_id}',nns_cms_category_name='{$nns_cms_category_name}' where "
        ." nns_id='{$nns_id}' and nns_parent_id='{$nns_parent_id}' and nns_hierarchy='{$nns_hierarchy}'";
    nl_execute_by_db($sql, m_config::get_dc()->db());
}

function make_xml($nns_hierarchy,$query_id,$parent_id='1000',$obj_dom,$parent_dom,$cate_name='/')
{
    $result = sql_result($nns_hierarchy,$query_id);
    if(!is_array($result) ||empty($result))
    {
        return ;
    }
    foreach ($result as $key=>$value)
    {
        $id = $parent_id.str_pad($key+1, 3, "0", STR_PAD_LEFT);
        if($parent_id=='1000')
        {
            $parent_id='0';
        }
        $arr_attr = array(
            'id'=>$id,
            'name'=>$value['nns_name'],
            'subname'=>$value['nns_id'],
            'parent'=>$parent_id,
            'virtual'=>"0",
            'is_enable_area_channel_feature'=>"0",
            'jingxuan_category_max_num'=>"",
            'type'=>"",
            'display'=>"0",
            'en_US'=>"",
            'zh_TW'=>"",
            'zh_HK'=>""
        );
        $ch_dom = m_config::make_xml($obj_dom, 'category',null,$arr_attr,$parent_dom);
        sql_update($value['nns_id'],$value['nns_parent_id'],$value['nns_hierarchy'],'vod',$id,$cate_name.$value['nns_name'].'/');
        make_xml($nns_hierarchy+1,$value['nns_id'],$id,$obj_dom,$ch_dom,$cate_name.$value['nns_name'].'/');
    }
}

$obj_dom = new DOMDocument("1.0", 'utf-8');
$assist = m_config::make_xml($obj_dom, 'assist');
make_xml(2, '4447','1000', $obj_dom, $assist,'/');
$str_xml_content = $obj_dom->saveXML();

echo $str_xml_content;