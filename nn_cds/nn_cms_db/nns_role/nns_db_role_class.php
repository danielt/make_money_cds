<?php
$nns_db_role_dir = dirname ( dirname ( __FILE__ ) );
require_once $nns_db_role_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
require_once $nns_db_role_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";

class nns_db_role_class {
	private $conn;
	private $db_obj;
	
	public function __construct() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
	}
	
	/**
	 * 
	 * 角色统计总数量
	 * @param string $name
	 */
	public function nns_db_role_count($name = null) {
		$str_sql = "select count(1) as num from nns_role ";
		
		if (isset ( $name )) {
			if (! empty ( $name )) {
				$name = nns_db_common_class::nns_db_common_validation ( $name );
				if ($name !== false) {
					$str_sql .= " where nns_name like '%$name%'";
				}
			}
		}
		$result = array ();
		$result = $this->db_obj->nns_db_query ( $str_sql );
		
		return $result;
	
	}
	
	/**
	 * 
	 * 角色列表（统计了各个角色下的管理员数）
	 * @param int $min
	 * @param int $num
	 */
	public function nns_db_role_mgr_num($min = 0, $num = 0) {
		$str_sql = "SELECT a.*,b.mgr_num FROM nns_role a LEFT JOIN (SELECT nns_role_id,COUNT(nns_id)AS mgr_num FROM nns_manager GROUP BY nns_role_id ) b ON a.nns_id=b.nns_role_id";
		$str_sql.=" order by a.nns_create_time desc";
		if (isset ( $min ) && isset ( $num )) {
			if (! empty ( $num )) {
				$min = ( int ) $min;
				$num = ( int ) $num;
				$str_sql .= " limit $min,$num";
			}
		}
		
		$result = array ();
		
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ROLE_NO_DATA, "role data not find" );
			}
		}
		return $result;
	
	}
	
	/**
	 * 
	 * 角色列表（支持模糊查询） ...
	 * @param string $name
	 * @param int $min
	 * @param int $num
	 */
	public function nns_db_role_list($name = null, $min = 0, $num = 0) {
		$str_sql = "select * from nns_role ";
		if (isset ( $name )) {
			if (! empty ( $name )) {
				$name = nns_db_common_class::nns_db_common_validation ( $name );
				if ($name !== false) {
					$str_sql .= " where nns_name like '%$name%'";
				}
			}
		}
		$str_sql.=" order by nns_create_time desc";
		if (isset ( $min ) && isset ( $num )) {
			if (! empty ( $num )) {
				$min = ( int ) $min;
				$num = ( int ) $num;
				$str_sql .= " limit $min,$num";
			}
		}
		
		$result = array ();
		
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ROLE_NO_DATA, "role data not find" );
			}
		}
		
		return $result;
	}
	
	/**
	 * 
	 * 角色详细信息 ...
	 * @param string $id
	 */
	public function nns_db_role_info($id) {
		$result = array ();
		
		if (! isset ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'role info param error ' );
			return $result;
		}
		if (empty ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'role info param error ' );
			return $result;
		}
		//非法字符串验证
		$id = nns_db_common_class::nns_db_common_validation ( $id, 1, 32, '', 1 );
		if ($id === false) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'role info param error ' );
			return $result;
		}
		
		$str_sql = "select * from nns_role where nns_id='$id' limit 1";
		
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ROLE_NOT_FOUND, 'role info not find' );
			}
		}
		
		return $result;
	
	}
	
	/**
	 * 
	 * 添加角色 ...
	 * @param string $id
	 * @param string $name
	 * @param string $pri_id
	 * @param int $state
	 * @param string $desc
	 */
	public function nns_db_role_add($name, $pri_id, $state = 0, $desc = null) {
		$result = array ();
		
		if (! isset ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'role add param error' );
			return $result;
		}
		if (empty ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'role add param error' );
			return $result;
		}
		//非法字符串处理
		$name = nns_db_common_class::nns_db_common_validation($name);
		$pri_id = nns_db_common_class::nns_db_common_validation($pri_id);
		if($name === false || $pri_id === false){
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'role add param error' );
			return $result;
		}
		if(!empty($desc)){
			$desc = nns_db_common_class::nns_db_common_validation($desc);
			if($desc === false){
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'role add param error' );
			    return $result;
			}
		}		
		
		$state = (int)$state;
		//生成GUID
		$guid = new Guid ();
		$id = $guid->toString ();
		
		//exist		
		$str_sql = "select 1 from nns_role where nns_id='$id' or nns_name='$name'";
		
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ROLE_EXIST, "role name exist" );
			return $result;
		}
		//insert
		$str_sql = "insert into nns_role(nns_id,nns_name,nns_pri_id,nns_state,nns_desc,nns_create_time,nns_modify_time)values('$id','$name','$pri_id', $state,'$desc',now(),now())";
		
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		return $result;
	}
	
	/**
	 * 
	 * 修改角色 （若不存在，则添加，否则修改）...
	 * @param string $id
	 * @param string $name
	 * @param string $pri_id
	 * @param int $state
	 * @param string $desc
	 */
	public function nns_db_role_modify($id, $name, $pri_id, $state = 0, $desc = null) {
		$result = array ();
		
		if (! isset ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'role modify param error' );
			return $result;
		}
		if (empty ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'role modify param error' );
			return $result;
		}
		//非法字符串处理
		$id = nns_db_common_class::nns_db_common_validation($id,1,32,'',1);
		$name = nns_db_common_class::nns_db_common_validation($name);
		$pri_id = nns_db_common_class::nns_db_common_validation($pri_id);
		if($id===false || $name === false || $pri_id === false){
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'role add param error' );
			return $result;
		}
		if(!empty($desc)){
			$desc = nns_db_common_class::nns_db_common_validation($desc);
			if($desc === false){
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'role add param error' );
			    return $result;
			}
		}		
		
		$state = (int)$state;
		//name exist
		$str_sql = "select 1 from nns_role where nns_id != '$id' and nns_name = '$name'";
		
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ROLE_EXIST, "role name exist" );
			return $result;
		}
		// id exist
		$str_sql = "select 1 from nns_role where nns_id = '$id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			//update
			$str_sql = "update nns_role set nns_name='$name',nns_pri_id='$pri_id',nns_state=$state,nns_desc='$desc',nns_modify_time=now() where nns_id='$id'";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
		} else {
			//insert
			$str_sql = "insert into nns_role(nns_id,nns_name,nns_pri_id,nns_state,nns_desc,nns_create_time,nns_modify_time)values('$id','$name','$pri_id',$state,'$desc',now(),now())";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
		}
		
		return $result;
	
	}
	
	/**
	 * 
	 * 删除角色
	 * @param string $id
	 */
	public function nns_db_role_delete($id) {
		$result = array ();
		
		if (! isset ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'role delete param error' );
			return $result;
		}
		//非法字符串处理
		$id = nns_db_common_class::nns_db_common_validation($id,1,32,'',1);
		if($id === false){
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'role delete param error' );
			return $result;
		}
		
		$str_sql = "delete from nns_role where nns_id='$id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		
		return $result;
	
	}

}

?>
