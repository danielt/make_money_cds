<?php
$nns_db_group_dir = dirname ( dirname ( __FILE__ ) );
require_once $nns_db_group_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
require_once $nns_db_group_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";
require_once $nns_db_group_dir . DIRECTORY_SEPARATOR . "nns_depot" . DIRECTORY_SEPARATOR . "nns_db_depot_class.php";

class nns_db_group_class {
	private $conn;
	private $db_obj;

	public function __construct() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
	}
	/**
	 *
	 * 统计合作伙伴数量 ...
	 */
	public function nns_db_group_count($name = null) {
		$result = array ();
		$str_sql = "select count(1) as num from nns_group";
		if (isset ( $name )) {
			if (! empty ( $name )) {
				$str_sql .= " where nns_name like '%$name%' ";
			}
		}
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_GROUP_NO_DATA, 'group data not find' );
			}
		}
		return $result;

	}

	public function nns_db_group_list($name = null, $min = 0, $num = 0) {
		$str_sql = "select * from nns_group ";
		$result = array ();
		if (isset ( $name )) {
			if (! empty ( $name )) {
				$str_sql .= " where nns_name like '%$name%' ";
			}
		}
		if (isset ( $min ) && isset ( $num )) {
			if (! empty ( $num )) {
				$min = ( int ) $min;
				$num = ( int ) $num;
				$str_sql .= " limit $min,$num";
			}
		}

		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_GROUP_NO_DATA, 'group data not find' );
			}
		}
		return $result;

	}

	public function nns_db_group_add($name, $state = 0, $desc = null, $contact = null, $telephone = null, $email = null,
	$addr = null,$boss_area_code=null,$boss_type=null,$nns_template_id=null,$nns_template_type=null,$nns_url_link=null,$nns_outside_link='0') {
		$result = array ();
		if (! isset ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'group add param error' );
			return $result;
		}
		if (empty ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'group add param error' );
			return $result;
		}

		//生成GUID
		$guid = new Guid ();
		$id = $guid->toString ();
		//state
		$state = ( int ) $state;
		//exist
		$str_sql = "select 1 from nns_group where nns_id='$id' or nns_name='$name'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_GROUP_EXIST, 'group name exist' );
			return $result;
		}
		//INSERT
		$str_sql = "insert into nns_group(nns_id,nns_name,nns_state,nns_desc,nns_create_time,nns_modify_time,nns_contact," .
				"nns_telephone,nns_email,nns_addr,nns_boss_area_code,nns_boss_type,nns_template_id,nns_template_type,nns_url_link," .
				"nns_outside_link)values('$id','$name',0,'$desc',now(),now()" .
				",'$contact','$telephone','$email','$addr','$boss_area_code','$boss_type','$nns_template_id','$nns_template_type','$nns_url_link','$nns_outside_link')";
		$result = $this->db_obj->nns_db_execute ( $str_sql );

//		创建仓库
		$depot_inst=new nns_db_depot_class();
		$depot_inst->nns_db_depot_modify($name,"",2,$id,0);
		$depot_inst->nns_db_depot_modify($name,"",2,$id,1);
		$depot_inst=null;
		return $result;
	}

	public function nns_db_group_modify($id, $name, $state = 0, $desc = null, $contact = null, $telephone = null, $email = null, $addr = null,
	$boss_area_code=null,$boss_type=null,$nns_template_id=null,$nns_template_type=null,$nns_url_link=null,$nns_outside_link='0') {
		$result = array ();
		if (empty ( $id ) || empty ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'group add param error' );
			return $result;
		}
		/*name exist
		$str_sql = "select 1 from nns_group where nns_id != '$id' and nns_name='$name'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_GROUP_EXIST, 'group name exist' );
			return $result;
		}*/
		//id exist
		$str_sql = "select * from nns_group where nns_id='$id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		$boss_str="";
		$boss_str1="";
		$boss_str2="";
		if (!empty($boss_area_code)){
			$boss_str.=",nns_boss_area_code='$boss_area_code' ";
			$boss_str1.=",nns_boss_area_code";
			$boss_str2.=",'$boss_area_code'";
		}

		if (!empty($boss_type)){
			$boss_str.=",nns_boss_type='$boss_type' ";
			$boss_str1.=",nns_boss_type";
			$boss_str2.=",'$boss_type'";
		}

		if ($this->db_obj->num_rows > 0) {
			//update
			$str_sql = "update nns_group set nns_name='$name',nns_state='$state',nns_desc='$desc',nns_contact='$contact'," .
					"nns_telephone='$telephone',nns_email='$email',nns_addr='$addr',nns_template_id='$nns_template_id'," .
					"nns_template_type='$nns_template_type',nns_url_link='$nns_url_link',nns_outside_link='$nns_outside_link' ".$boss_str." where nns_id='$id'";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
		} else {
			//insert
			$str_sql = "insert into nns_group(nns_id,nns_name,nns_state,nns_desc,nns_create_time,nns_modify_time,nns_contact,nns_telephone,nns_email,nns_addr,nns_template_id,nns_template_type,nns_url_link,nns_outside_link ".$boss_str1.")values('$id','$name',0,'$desc',now(),now(),'$contact','$telephone','$email','$addr','$nns_template_id','$nns_template_type','$nns_url_link','$nns_outside_link' ".$boss_str2.")";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
		}
		return $result;
	}

	public function nns_db_group_info($id) {
		$result = array ();
		if (! isset ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'group info param error' );
			return $result;
		}
		if (empty ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'group info param error' );
			return $result;
		}
		$str_sql = "select * from nns_group where nns_id='$id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_GROUP_NOT_FOUND, 'group info not found' );
			}
		}
		return $result;
	}

	public function nns_db_group_delete($id) {
		$result = array ();
		if (! isset ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'group delete param error' );
			return $result;
		}

		$str_sql = "delete from nns_group where nns_id='$id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );

		//		删除仓库
		$depot_inst=new nns_db_depot_class();
		$depot_inst->nns_db_depot_delete("",2,$id);
		$depot_inst=null;

		return $result;

	}

}

?>