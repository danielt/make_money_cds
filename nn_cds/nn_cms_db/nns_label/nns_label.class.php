<?php
include_once dirname(dirname ( dirname ( __FILE__ ) )).DIRECTORY_SEPARATOR.'nn_logic'.DIRECTORY_SEPARATOR.'label'
		 				.DIRECTORY_SEPARATOR.'video_label_bo.class.php';
//include_once dirname(dirname ( dirname ( __FILE__ ) )).DIRECTORY_SEPARATOR.'nn_logic'.DIRECTORY_SEPARATOR.'label'
//		 				.DIRECTORY_SEPARATOR.'video_label_bind_content.class.php';
class nns_label {

	static private $dc;
    function __construct() {
    	
    }

    
    function dc(){
    	if (self::$dc==NULL){
    		self::$dc=nl_get_dc(
    		array(
				'db_policy'=>NL_DB_WRITE,
				'cache_policy'=>NP_KV_CACHE_TYPE_MEMCACHE
			)
    		);
    		if (self::$dc){
    			self::$dc->open();
    		}
    	}
    	
    	
    	
    	return self::$dc;
    }
    
    
    public function add_label($video_id,$video_type,$params){
    	
    	$dc=$this->dc();
    	foreach ($params as  $key=>$value){
    		nl_video_label_bo::bind_video_label_by_video(
    		$dc,
    		$video_id,
    		$video_type,
    		array(
    			'type_id'=>$key,
    			'name'=>$value
    		)
    		);
    	}
    	
    	
    }
    
    
    public function delete_label($video_id,$video_type){
    	$dc=$this->dc();
    	nl_video_label_bind_content::del_video_label_content($dc,$video_id,$video_type);
    }
    
}
?>