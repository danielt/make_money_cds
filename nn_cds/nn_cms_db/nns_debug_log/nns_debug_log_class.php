<?php
/*
 * Created on 2012-7-6
 *  日志类
 *  usage：
 *  $log =  new nns_debug_log_class();
 *  $log->setlogpath('test.log');
 *  $log->write($msg);
 *
 */
//date_default_timezone_set('Asia/Chongqing');
class nns_debug_log_class {
	static $instance = null;
	private $log_path = null;
	private $path;
	private $sub_path = null;
	public function __construct() {
		$this->path=dirname ( dirname ( dirname ( __FILE__ ) )).DIRECTORY_SEPARATOR."data".DIRECTORY_SEPARATOR."log".DIRECTORY_SEPARATOR;
        if(!is_dir($this->path)) mkdir($this->path,777,true);
	}
    public static function get_instance(){
          if(!self::$instance){
          	   self::$instance = new nns_debug_log_class();
          }

          return self::$instance ;
    }
	public function setlogpath($file) {
		if(empty($file)) return false;
		$file = str_replace('.','-',$file);

		//$this->createdir($this->path);
		//$this->creat_file($this->path,$file);
		//$this->mk_dir($file);
		$this->sub_path = $file;
//
/*
		$file = $file.DIRECTORY_SEPARATOR.date('Ymd').'.txt';
		@touch($this->path.$file);
		$this->log_path=$this->path.$file;
		@chmod($this->path, 0777);
		@chmod($this->path . $file,0777);
*/

	}
	//基于data/log下创建目录
	function mk_dir($absolute_path, $mode = 0777)
	{
	    if (is_dir($absolute_path))
	    {
	        return true;
	    }

	    $root_path      = $this->path;
	    $relative_path  = str_replace($root_path, '', $absolute_path);
	    $each_path      = explode('/', $relative_path);
	    $cur_path       = $root_path; // 当前循环处理的路径
	    $old=umask(0);
	    foreach ($each_path as $path)
	    {
	        if ($path)
	        {
	            $cur_path = $cur_path . '/' . $path;
	            if (!is_dir($cur_path))
	            {
	                if (@mkdir($cur_path, $mode))
	                {
	                    return true;
	                }
	                else
	                {
	                    return false;
	                }
	            }
	        }
	    }
		umask($old);
	    return true;
	}
	function createdir($dir) {
		if (file_exists($dir) && is_dir($dir)) {
		} else {
			@mkdir($dir, 0777);
			@chmod($dir, 0777);
		}
	}
	///创建文件
	function creat_file($PATH,$sFile) {
		if (!file_exists($PATH . $sFile)) {
			$fp = fopen($PATH . $sFile, "w+");
			fclose($fp);
		}
	}

	public function setlog($msg,$method){
		/*多次打开文件，影响性能 by xsong512
		file_put_contents($this->log_path,"-------------------".$method."-----------------\n",FILE_APPEND);
		file_put_contents($this->log_path,$msg."\n",FILE_APPEND);
		file_put_contents($this->log_path,"*************log_time:".date("Y-m-d H:i:s",time())."************\n",FILE_APPEND);
		file_put_contents($this->log_path,"-------------------".$method."-----------------\n",FILE_APPEND);
		*/
		$msg_str = $method."   \n".$msg;
        $this->write($msg_str);
	}
	/**
	 * 记录日志
	 */
	public function write($msg,$sub_path=null){
		if(!$msg) return;
		//创建日志路径
		$file =$log_name  = date('Ymd').'.txt';
		if($sub_path) {
			$this->mk_dir($sub_path);
			$file = $sub_path.DIRECTORY_SEPARATOR.$log_name;

		}else{
			if($this->sub_path){
				$this->mk_dir($this->sub_path);
				$file = $this->sub_path.DIRECTORY_SEPARATOR.$log_name;

			}else{
				$file = $log_name;
			}
		}
		$this->log_path=$this->path . $file;
	    @touch($this->log_path);
	    @chmod($this->log_path,0777);
        //写入日志
		$msg = "[".date('Y-m-d H:i:s')."] ".$msg."\n";
		if(is_file($this->log_path)){
			error_log($msg,3,$this->log_path);
		}else{
			error_log($msg);
		}
	}
}
?>
