<?php
$nns_db_live_media_dir = dirname ( dirname ( __FILE__ ) );
$nns_db_live_media_cms_config = dirname ( dirname ( dirname ( __FILE__ ) ) );
require_once $nns_db_live_media_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
require_once $nns_db_live_media_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";
require_once $nns_db_live_media_cms_config . DIRECTORY_SEPARATOR . "nn_cms_config" . DIRECTORY_SEPARATOR . "nn_cms_global.php";
require_once $nns_db_live_media_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_curl_content_class.php";

class nns_db_live_media_class {
	
	private $conn;
	private $db_obj;
	
	public function __construct() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
	}
	
	//媒体统计
	public function nns_db_live_media_count($live_id = null, $index = null, $index_id = null, $tag = null, $state = null, $check = null, $deleted = NNS_LIVE_MEDIA_DELETED_FALSE) {
		$result = array ();
		$str_sql = "select * from nns_live_media ";
		$str_sql .= $this->nns_db_live_media_where ( 0, 0, $live_id, $index, $index_id, $tag, $state, $check, $deleted );
		//$result = $str_sql;
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_LIVE_MEDIA_NO_DATA, 'live media data not find' );
			}
			//tag 赛选
			$data = $result ["data"];
			if (! empty ( $tag )) {
				if (is_array ( $tag )) {
					
					//遍历结果集，删除不包含tag的项					
					for($i = count ( $data ) - 1; $i >= 0; $i --) {
						$flag = 0;
						$str = ( string ) $data [$i] ["nns_tag"];
						for($j = 0; $j < count ( $tag ); $j ++) {
							$find = ( string ) $tag [$j];
							if ( $find != "" && $find != null) {
								if (strstr ( $str, $find ) !== false){
									$flag = 1;
									break;
								}
							}
							if ($flag == 1) {
								break;
							}
						} //for
						//删除数组元素
						if ($flag == 0) {
							array_splice ( $data, $i, 1 );
						}
					} //for
					$result ["data"] = $data;
				} // end if is_array
			} //end if
			//end if tag 赛选
			$count [0] ["num"] = count ( $data );
			$result ["data"] = $count;
		}
		return $result;
	}
	
	//媒体列表
	public function nns_db_live_media_list($min = 0, $num = 0, $live_id = null, $index = null, $index_id = null, $tag = null, $state = null, $check = null, $deleted = NNS_LIVE_MEDIA_DELETED_FALSE) {
		$result = array ();
		$str_sql = "select * from nns_live_media ";
		$str_sql .= $this->nns_db_live_media_where ( $min, $num, $live_id, $index, $index_id, $tag, $state, $check, $deleted );
		//$result = $str_sql;
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_LIVE_MEDIA_NO_DATA, 'live media data not find' );
			}
			//tag 赛选
			if (! empty ( $tag )) {
				if (is_array ( $tag )) {
					$data = $result ["data"];
					
					//遍历结果集，删除不包含tag的项					
					for($i = count ( $data ) - 1; $i >= 0; $i --) {
						$flag = 0;
						$str = ( string ) $data [$i] ["nns_tag"];
						for($j = 0; $j < count ( $tag ); $j ++) {
							$find = ( string ) $tag [$j];
							if ( $find != "" && $find != null) {
								if (strstr ( $str, $find ) !== false){
									$flag = 1;
									break;
								}
							}
							if ($flag == 1) {
								break;
							}
						} //for
						//删除数组元素
						if ($flag == 0) {
							array_splice ( $data, $i, 1 );
						}
					} //for
					$result ["data"] = $data;
				} // end if is_array
			} //end if
		//end if tag 赛选
		}
		return $result;
	}
	//媒体信息
	public function nns_db_live_media_info($media_id) {
		$result = array ();
		//live_id
		if (! isset ( $media_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index info param error' );
			return $result;
		}
		if (empty ( $media_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index info param error' );
			return $result;
		}
		$str_sql = " select * from nns_live_media where nns_id='$media_id'";
		
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_LIVE_MEDIA_NOT_FOUND, 'live media info not found' );
			}
		}
		
		return $result;
	}
	//修改媒体信息
	public function nns_db_live_media_modify($media_id, $name = null, $type = null, $url = null, $tag = null, $mode = null, $stream = null,$media_caps='LIVE',$nns_timeshift_status=0,
	    $nns_timeshift_delay=0,$nns_storage_status=0,$nns_storage_delay=0,$nns_cast_type=0,$nns_media_type_post=null,$nns_domain='') {
		$result = array ();
		$name=str_replace("'"," ",$name);
		$summary=str_replace("'"," ",$summary);
		//live_id
		if (! isset ( $media_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index modify param error' );
			return $result;
		}
		if (empty ( $media_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index modify param error' );
			return $result;
		}
		$str_sql = " update nns_live_media set nns_name='$name',nns_modify_time=now()";
		if (isset ( $type )) {
			if ($type !== null && $type !== '') {
				$type = ( int ) $type;
				$str_sql .= ",nns_type=$type";
			}
		}
		if (isset ( $url )) {
			if (! empty ( $url )) {
				$str_sql .= ",nns_url='$url'";
			}
		}
		if (isset ( $tag )) {
			if (! empty ( $tag )) {
				$str_sql .= ",nns_tag='$tag'";
			}
		}
		if (isset ( $mode )) {
			if (! empty ( $mode )) {
				$str_sql .= ",nns_mode='$mode'";
			}
		}
		if (isset ( $stream )) {
			if (! empty ( $stream )) {
				$str_sql .= ",nns_kbps='$stream'";
			}
		}
		if (isset ( $media_caps )) {
			if (! empty ( $media_caps )) {
				$str_sql .= ",nns_media_caps='$media_caps'";
			}
		}
		if (isset ( $nns_timeshift_status )) {
			if (strlen( $nns_timeshift_status ) >0) {
				$str_sql .= ",nns_timeshift_status='$nns_timeshift_status'";
			}
		}
		if (isset ( $nns_cast_type )) {
			if (strlen( $nns_cast_type ) >0) {
				$str_sql .= ",nns_cast_type='$nns_cast_type'";
			}
		}
		if (isset ( $nns_timeshift_delay )) {
			if (strlen( $nns_timeshift_delay ) >0) {
				$str_sql .= ",nns_timeshift_delay='$nns_timeshift_delay'";
			}
		}
		if (isset ( $nns_storage_status )) {
			if (strlen( $nns_storage_status ) >0) {
				$str_sql .= ",nns_storage_status='$nns_storage_status'";
			}
		}
		if (isset ( $nns_storage_delay )) {
			if (strlen( $nns_storage_delay ) >0) {
				$str_sql .= ",nns_storage_delay='$nns_storage_delay'";
			}
		}
		if (isset ( $nns_media_type_post )) {
			if (strlen( $nns_media_type_post ) >0) {
				$str_sql .= ",nns_filetype='$nns_media_type_post'";
			}
		}
		$str_sql .= ",nns_domain='$nns_domain'";
		$str_sql .= " where nns_id='$media_id'";
		
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		
		return $result;
	}
	private function nns_db_live_media_modify_deleted($id, $deleted) {
		$result = array ();
		if (! isset ( $id ) || ! isset ( $deleted )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live media delete param error' );
			return $result;
		}
		if (empty ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live media delete param error' );
			return $result;
		}
		$deleted = ( int ) $deleted;
		$str_sql = "update nns_live_media set nns_deleted=" . $deleted . " where nns_id='$id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		
		return $result;
	}
	//媒体删除	
	public function nns_db_live_media_delete($id) {
		//return $this->nns_db_live_media_modify_deleted ( $id, NNS_LIVE_MEDIA_DELETED_TRUE );
		

		global $g_core_url;
		$result = array ();
		$contentid = "";
		//查取$contentid
		$str_sql = "select nns_content_id from nns_live_media where nns_id='$id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows > 0) {
				$contentid = $result ["data"] [0] ["nns_content_id"];
			}
		}
		
		$str_sql = "delete from nns_live_media where nns_id='$id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		/**
		 * 直播模式不删除片源 BY S67 2012-8-21
		 */	
//		if ($result ["ret"] == 0) {
//			if (! empty ( $contentid )) {
//				//调用核心管理平台内容删除
//				//------core start-----------
//				//从config 中获取http——url	
//				$http_url = $g_core_url;
//				//通过CURL抓取返回值
//				$curl_post = 'func=delete_content' . "&id=" . $contentid;
//				nns_db_curl_content_class::nns_db_curl_content ( $http_url, $curl_post );
//			
//		       //------------core end--------------	
//			}
//		}
		return $result;
	}
	
	//恢复媒体
	public function nns_db_live_media_retrieve($id) {
		return $this->nns_db_live_media_modify_deleted ( $id, NNS_LIVE_MEDIA_DELETED_FALSE );
	}

	public function nns_db_live_media_deleted($id)
	{
		return $this->nns_db_live_media_modify_deleted ( $id, NNS_LIVE_MEDIA_DELETED_TRUE );
	}
	
	//媒体审核
	public function nns_db_live_media_check($media_id, $check) {
		$result = array ();
		if (! isset ( $media_id ) || ! isset ( $check )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live modify check param error' );
			return $result;
		}
		if (empty ( $media_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live modify check param error' );
			return $result;
		}
		$check = ( int ) $check;
		$str_sql = "update nns_live_media set nns_check=" . $check . " where nns_id='$media_id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		
		return $result;
	
	}
	//state
	public function nns_db_live_media_state($media_id, $state) {
		$result = array ();
		if (! isset ( $media_id ) || ! isset ( $state )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live modify state param error' );
			return $result;
		}
		if (empty ( $media_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live modify state param error' );
			return $result;
		}
		$state = ( int ) $state;
		$str_sql = "update nns_live_media set nns_state=" . $state . " where nns_id='$media_id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		
		return $result;
	
	}
	//media where
	public function nns_db_live_media_where($min = 0, $num = 0, $live_id = null, $index = null, $index_id = null, $tag = null, $state = NNS_LIVE_MEDIA_STATE_WAITING, $check = NNS_LIVE_MEDIA_CHECK_WAITING, $deleted = NNS_LIVE_MEDIA_DELETED_FALSE) {
		$where = "";
		if (isset ( $live_id )) {
			if (! empty ( $live_id )) {
				if (empty ( $where )) {
					$where .= " where nns_live_id='$live_id'";
				} else {
					$where .= " and nns_live_id='$live_id'";
				}
			}
		}
		if (isset ( $index )) {
			if ($index !== null && $index !== '') {
				if (empty ( $where )) {
					$where .= " where nns_live_index=$index";
				} else {
					$where .= " and nns_live_index=$index";
				}
			}
		}
		if (isset ( $index_id )) {
			if (! empty ( $index_id )) {
				if (empty ( $where )) {
					$where .= " where nns_live_index_id='$index_id'";
				} else {
					$where .= " and nns_live_index_id='$index_id'";
				}
			}
		}
		//state
		if ($state !== null && $state !== '') {
			$state = ( int ) $state;
			if (empty ( $where )) {
				$where .= " where nns_state=$state";
			} else {
				$where .= " and nns_state=$state";
			}
		}
		//check
		if ($check !== null && $check !== '') {
			$check = ( int ) $check;
			if (empty ( $where )) {
				$where .= " where nns_check=$check";
			} else {
				$where .= " and nns_check=$check";
			}
		}
		//deleted
		if ($deleted != "-1") {
			if ($deleted !== null && $deleted !== '') {
				$deleted = ( int ) $deleted;
				if (empty ( $where )) {
					$where .= " where nns_deleted =$deleted";
				} else {
					$where .= " and nns_deleted =$deleted";
				}
			}
		
		}
		
		$where .= " order by nns_modify_time desc ";
		
		if (isset ( $min ) && isset ( $num )) {
			if (! empty ( $num )) {
				$min = ( int ) $min;
				$num = ( int ) $num;
				$where .= " limit $min,$num";
			}
		}
		
		return $where;
	}
	
	//修改播放次数
	public function nns_db_live_media_play_count($media_id) {
		$live_id = "";
		$live_index_id = "";
		$result = array ();
		if (! isset ( $media_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live modify play count param error' );
			return $result;
		}
		if (empty ( $media_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live modify play count param error' );
			return $result;
		}
		//exist
		$str_sql = "select nns_id,nns_live_index_id,nns_live_id from nns_live_media where nns_id='$media_id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows <= 0) {
		    $result = $this->db_obj->nns_db_result ( NNS_ERROR_VOD_MEDIA_NOT_FOUND, 'live modify play count not found' );
			return $result;
		}
		$live_index_id = $result ["data"] [0] ["nns_live_index_id"];
		$live_id = $result ["data"] [0] ["nns_live_id"];
		
		$str_sql = "update nns_live_media set nns_play_count= IFNULL(nns_play_count,0) + 1 where nns_id='$media_id'";
		$result = $this->db_obj->nns_db_execute($str_sql);
		
		$str_sql = "update nns_live_index set nns_play_count= IFNULL(nns_play_count,0) + 1 where nns_id='$live_index_id'";
		$result = $this->db_obj->nns_db_execute($str_sql);
		
		$str_sql = "update nns_live set nns_play_count= IFNULL(nns_play_count,0) + 1 where nns_id='$live_id'";
		$result = $this->db_obj->nns_db_execute($str_sql);
		
		return $result;	
	}
	
	
    //添加媒体源
	/**
	 * 
	 * Enter description here ...
	 * @param string $live_id 点播ID
	 * @param string $index_name 每一集名字
	 * @param string $media_name 片源名字
	 * @param int $index 集数索引
	 * @param string $mode 高清标清流畅
	 * @param string $stream 码率 384
	 * @param string $type pc or iPhone
	 * @param string $tag 输出标记
	 * @param string $url  存储路径
	 * @param string $time_len 时长
	 * @param string $summary 每一集概要
	 * @param string $cdn_policy_id 策略ID
	 * @param string $filetype 文件类型
	 * @param int $live 点播0直播1
	 */
	public function nns_db_live_media_add($live_id, $index_name, $media_name, $index, $mode, $stream = null, $type = null, $tag = null, $url = null,
			 $time_len = null, $summary = null, $cdn_policy_id = null, $filetype = null, $media_caps='LIVE',$nns_timeshift_status=0,$nns_timeshift_delay=0,$nns_storage_status=0,
	    $nns_storage_delay=0,$contentid='',$nns_cast_type=0,$nns_domain='',$nns_media_service='') {
		$result = array ();
		$name=str_replace("'"," ",$name);
		$summary=str_replace("'"," ",$summary);
		global $g_core_url;
		//live_id
		if (! isset ( $live_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index add param error' );
			return $result;
		}
		if (empty ( $live_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index add param error' );
			return $result;
		}
		
		//index
		if (! isset ( $index )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index add param error' );
			return $result;
		}
		if ($index === null || $index === '') {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index add param error' );
			return $result;
		}
		$index = ( int ) $index;
		//mode
		if (! isset ( $mode )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index add param error' );
			return $result;
		}
		if ($mode === null && $mode === '') {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index add param error' );
			return $result;
		}
		//check
		$check = NNS_VOD_MEDIA_CHECK_OK;
		//state
		$state = NNS_VOD_MEDIA_STATE_WAITING;
		//select live state
		$str_sql = "select nns_check,nns_state,nns_cp_id,nns_import_source from nns_live where nns_id='$live_id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows < 0) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_VOD_NOT_FOUND, 'live id not found' );
			return $result;
		}
		$data = $result ["data"];
		$import_source = $data[0]['nns_import_source'];
		$live_check = $data [0] ["nns_check"];
		$nns_cp_id = $data [0] ["nns_cp_id"];
		if ($live_check == NNS_VOD_AND_MEDIA_CHECK_OK) {
			$check = NNS_VOD_MEDIA_CHECK_OK;
		}
		$check = ( int ) $check;
		
		$live_state = $data [0] ["nns_state"];
		if ($live_state == NNS_VOD_AND_MEDIA_STATE_ONLINE) {
			$state = NNS_VOD_MEDIA_STATE_ONLINE;
		}
		$state = ( int ) $state;
		// live_index 索引获取或插入
		$result = $this->nns_db_live_index_add ( $live_id, $index, $index_name, $time_len, $summary ,$import_source);
		if ($result ["ret"] != 0) {
			return $result;
		}
		$index_id = $result ["data"] [0] ["nns_id"];
		
		//调用核心管理平台内容添加
		//------core start-----------
		//从config 中获取http——url	
// 		$http_url = $g_core_url;
// 		//通过CURL抓取返回值
// 		$curl_post = 'func=add_content_by_cdn_policy' . "&name=" . urlencode ( $media_name ) . "&url=" . urlencode ( $url ) . "&cdn_policy_id=" . $cdn_policy_id . "&live=1" . "&filetype=" . $filetype;
// 		$data = nns_db_curl_content_class::nns_db_curl_content ( $http_url, $curl_post );
// 		if (empty ( $data )) {
// 			$result = $this->db_obj->nns_db_result ( NNS_ERROR_CORE_CONTENT_FAILED, 'live media core content return empty' );
// 			return $result;
// 		}
// 		//simple xml 解析
// 		$xml = simplexml_load_string ( $data );
// 		$ret = ( string ) $xml->result ["ret"];
// 		$contentid = ( string ) $xml->content ["id"];
// 		if ($ret != 0) {
// 			$result = $this->db_obj->nns_db_result ( NNS_ERROR_CORE_CONTENT_FAILED, 'live media core content add failed' );
// 			return $result;
// 		}
		//------core end-----------
		$ret=0;
		$http_url='';
		//live media 表的插入	     
		$result = $this->nns_db_live_media_add_impl ( $live_id, $index, $index_id, $media_name, $state, $check, $mode, $tag, $type, $url, $stream, $http_url, $contentid, $ret, $filetype,$media_caps,$nns_cp_id,$nns_timeshift_status,$nns_timeshift_delay,$nns_storage_status,$nns_storage_delay,$nns_cast_type,$import_source,$nns_domain,$nns_media_service);
		
		return $result;
	}
    //绑定
    /**
     * 
     * Enter description here ...
     * @param string $live_id
     * @param string $index_name
     * @param string $media_name 没用到
     * @param int $index
     * @param char $mode
     * @param char $stream
     * @param int $type
     * @param char $tag
     * @param char $url
     * @param int $time_len
     * @param char $summary
     * @param char $contentid 要绑定的核心管理平台的ID
     */
	public function nns_db_live_media_bind($live_id, $index_name, $media_name, $index, $mode, $stream = null, $type = null, $tag = null, $url = null, $time_len = null, $summary = null, $contentid = null,$media_caps='LIVE',
			$nns_timeshift_status=0,$nns_timeshift_delay=0,$nns_storage_status=0,$nns_storage_delay=0,$nns_domain='',$nns_media_service='') {
		$result = array ();
		global $g_core_url;
		//live_id
		if (! isset ( $live_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index add param error' );
			return $result;
		}
		if (empty ( $live_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index add param error' );
			return $result;
		}
		
		//index
		if (! isset ( $index )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index add param error' );
			return $result;
		}
		if ($index === null || $index === '') {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index add param error' );
			return $result;
		}
		$index = ( int ) $index;
		//mode
		if (! isset ( $mode )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index add param error' );
			return $result;
		}
		if ($mode === null && $mode === '') {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index add param error' );
			return $result;
		}
		//check
		$check = NNS_VOD_MEDIA_CHECK_OK;
		//state
		$state = NNS_VOD_MEDIA_STATE_WAITING;
		//select live state
		$str_sql = "select nns_check,nns_state,nns_cp_id,nns_import_source from nns_live where nns_id='$live_id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows < 0) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_VOD_NOT_FOUND, 'live id not found' );
			return $result;
		}
		$data = $result ["data"];
		$live_check = $data [0] ["nns_check"];
		$nns_cp_id = $data [0] ["nns_cp_id"];
		if ($live_check == NNS_VOD_AND_MEDIA_CHECK_OK) {
			$check = NNS_VOD_MEDIA_CHECK_OK;
		}
		$check = ( int ) $check;
		
		$live_state = $data [0] ["nns_state"];
		if ($live_state == NNS_VOD_AND_MEDIA_STATE_ONLINE) {
			$state = NNS_VOD_MEDIA_STATE_ONLINE;
		}
		$state = ( int ) $state;
		
		// live_index 索引获取或插入
		$result = $this->nns_db_live_index_add ( $live_id, $index, $index_name, $time_len, $summary );
		if ($result ["ret"] != 0) {
			return $result;
		}
		$index_id = $result ["data"] [0] ["nns_id"];
		$import_source = $data [0] ["nns_import_source"];
		//调用核心管理平台内容添加
		//------core start-----------
		//从config 中获取http——url	
		$http_url = $g_core_url;
		//通过CURL抓取返回值
		$curl_post = 'func=get_content' . "&id=" . $contentid;
		$data = nns_db_curl_content_class::nns_db_curl_content ( $http_url, $curl_post );
		if (empty ( $data )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_CORE_CONTENT_FAILED, 'live media core content return empty' );
			//return $result;
		}
//		echo $data ;
//		die;
		//simple xml 解析
		//$xml = simplexml_load_string ( $data );
		//$ret = ( string ) $xml->result ["ret"];
// 		if ($ret != 0) {
			//$result = $this->db_obj->nns_db_result ( NNS_ERROR_CORE_CONTENT_FAILED, 'live media core content bind failed' );
			//return $result;
// 		}
		$contentid = ( string ) $contentid;
		$content_name = ( string ) $media_name;
		$file_type = ( string ) 'ts';
		$url = ( string ) $contentid;
		//------core end-----------
		

		//live media 表的插入	     
		$result = $this->nns_db_live_media_add_impl ( $live_id, $index, $index_id, $content_name, $state, $check, $mode, $tag, $type, $url, $stream, $http_url, $contentid, $ret, 
		    $file_type,$media_caps ,$nns_cp_id,$nns_timeshift_status,$nns_timeshift_delay,$nns_storage_status,$nns_storage_delay,$import_source,$nns_domain,$nns_media_service);
		
		return $result;
	}
	
	public function add_media_other_data($media_id, $params = null,$nns_cp_id=null)
	{
	    if (empty($params) || !is_array($params))
	    {
	        return false;
	    }
	    foreach ($params as $key => $v)
	    {
	        $set .= "nns_{$key}='{$v}',";
	    }
	    $str_update = (strlen($nns_cp_id) > 0 ) ? " and nns_cp_id='{$nns_cp_id}' " : " and nns_cp_id='0' ";
	    $set = trim($set, ',');
	    $sql = "update nns_live_media set {$set} where nns_id='{$media_id}' {$str_update}";
	    return $this->db_obj->nns_db_execute ( $sql );
	
	}
	
	//insert live_index
	public function nns_db_live_index_add($live_id, $index, $index_name, $time_len, $summary,$import_source='') {
		$result = array ();
		//live index表的插入
		//live_index exist
		$str_sql = "select nns_id from nns_live_index where nns_live_id='$live_id' and nns_index=$index";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		//已经存在则获取index——id，否则插入
		if ($this->db_obj->num_rows <= 0) {
			//insert live_index
			//生成live index GUID
			$guid = new Guid ();
			$index_id = $guid->toString ();
			
			$str_sql = "insert into nns_live_index";
			$field = "nns_id,nns_name,nns_live_id,nns_index,nns_create_time,nns_modify_time,nns_import_source";
			$values = "'$index_id','$index_name','$live_id',$index,now(),now(),'{$import_source}'";
			//time_len
			if (! empty ( $time_len )) {
				$time_len = ( int ) $time_len;
				$field .= ",nns_time_len";
				$values .= ",$time_len";
			}
			//summary
			if (! empty ( $summary )) {
				$field .= ",nns_summary";
				$values .= ",'$summary'";
			}
			$str_sql .= "(" . $field . ")values(" . $values . ")";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
			if ($result ["ret"] != 0) {
				return $result;
			}
			$result ["data"] [0] ["nns_id"] = $index_id;
		} //end if else		
		return $result;
	}
	
	public function nns_db_live_media_add_impl($live_id, $index, $index_id, $media_name, $state, $check, $mode, $tag, $type, $url, $stream, $http_url, 
	    $contentid, $ret, $file_type,$media_caps='LIVE', $nns_cp_id,$nns_timeshift_status=0,$nns_timeshift_delay=0,$nns_storage_status=0,$nns_storage_delay=0,$nns_cast_type=0,$import_source='',$nns_domain='',$nns_media_service='') {
		//insert live media	
		// live media exist
//		直播片源可以添加多次相同片源
		$str_sql = "select 1 from nns_live_media where nns_live_id='$live_id' and nns_live_index_id='$index_id' and nns_mode='$mode' and nns_media_caps='$media_caps'";
		if ($tag !== null && $tag !== '') {
			$str_sql .= "  and nns_tag='$tag'";
		}
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_VOD_MEDIA_EXIST, 'live media exist' );
			return $result;
		}
		//生成live media GUID
		$guid = new Guid ();
		$media_id = $guid->toString ();
		
		$str_sql = "insert into nns_live_media(nns_id,nns_name,nns_live_id,nns_live_index,nns_live_index_id,nns_state,nns_check,nns_tag,nns_type,nns_url,nns_mode,nns_kbps,nns_create_time,nns_modify_time,nns_deleted,nns_content_id,nns_content_state,nns_filetype,nns_media_caps,nns_cp_id,nns_timeshift_status,nns_timeshift_delay,nns_storage_status,nns_storage_delay,nns_cast_type,nns_import_source,nns_domain,nns_media_service)
		values('$media_id','$media_name','$live_id',$index,'$index_id',$state,$check,'$tag',$type,'$url','$mode','$stream',now(),now()," . NNS_VOD_MEDIA_DELETED_FALSE . ",'$contentid','$ret','$file_type','$media_caps','$nns_cp_id',$nns_timeshift_status,$nns_timeshift_delay,$nns_storage_status,$nns_storage_delay,$nns_cast_type,'$import_source','$nns_domain','{$nns_media_service}')";
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		/**
		 * 直播模式不删除片源 BY S67 2012-8-21
		 */		
//		if ($result ["ret"] != 0) {
//			//live media 没添加成功时，调用核心管理平台，删除已经添加的内容	
//			//------------core start--------------	
//			$curl_post = 'func=delete_content' . "&id=" . $contentid;
//			nns_db_curl_content_class::nns_db_curl_content ( $http_url, $curl_post );
//			//------------core end--------------	
//			return $result;
//		}
		
		//更新第几集
		$str_sql = "update nns_live set nns_new_index=$index where nns_id='$live_id'";
		$this->db_obj->nns_db_execute ( $str_sql );
		$result['nns_id'] = $media_id;
		return $result;
	}
	
	public function get_live_media_id_by_import_id($import_id,$nns_cp_id)
	{
		$str_sql="select nns_id,nns_live_id from nns_live_media where " .

				"nns_content_id='{$import_id}' and nns_deleted!=1";
		//当cp id长度大于1的时候显示原CP ID
		if(strlen($nns_cp_id) > 1)
		{
			$str_sql.=" and nns_cp_id = '{$nns_cp_id}' ";
		}
		else
		{
			$str_sql.=" and nns_cp_id='0' ";
		}
		$result = $this->db_obj->nns_db_query( $str_sql );
		if (count($result['data'])>0){
			return $result['data'][0];
		}else{
			return FALSE;
		}
	}
	

}

?>