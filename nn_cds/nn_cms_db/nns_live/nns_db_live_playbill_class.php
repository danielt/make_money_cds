<?php
$nns_db_live_playbill_dir = dirname ( dirname ( __FILE__ ) );
require_once $nns_db_live_playbill_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
require_once $nns_db_live_playbill_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";

class nns_db_live_playbill_class {
	private $conn;
	private $db_obj;
	
	public function __construct() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
	}
	
	//import
	public function nns_db_live_playbill_import($live_id, $day, $playbill) {
		$result = array ();
		//live_id
		if (! isset ( $live_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live playbill add param error' );
			return $result;
		}
		if (empty ( $live_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live playbill add param error' );
			return $result;
		}
		if (! isset ( $day )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live playbill add param error' );
			return $result;
		}
		if (empty ( $day )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live playbill add param error' );
			return $result;
		}
		if (! isset ( $playbill )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live playbill add param error' );
			return $result;
		}
		if (empty ( $playbill )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live playbill add param error' );
			return $result;
		}
		
		//exist
		$str_sql = "select 1 from nns_live_playbill where nns_live_id='$live_id' and nns_day='$day'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			//update
			$str_sql = "update nns_live_playbill set nns_playbill='$playbill',nns_modify_time=now() where nns_live_id='$live_id' and nns_day='$day'";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
		} else {
			//insert		
			//生成GUID
			$guid = new Guid ();
			$id = $guid->toString ();
			
			$str_sql = "insert into nns_live_playbill(nns_id,nns_live_id,nns_day,nns_playbill,nns_create_time,nns_modify_time)values('$id','$live_id','$day','$playbill',now(),now())";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
		}
		
		return $result;
	}
	
	//export
	public function nns_db_live_playbill_export($live_id, $day) {
		$result = array ();
		//live_id
		if (! isset ( $live_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live playbill add param error' );
			return $result;
		}
		if (empty ( $live_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live playbill add param error' );
			return $result;
		}
		if (! isset ( $day )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live playbill add param error' );
			return $result;
		}
		if (empty ( $day )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live playbill add param error' );
			return $result;
		}
		
		$str_sql = "select * from nns_live_playbill where nns_live_id='$live_id' and nns_day='$day'";
		
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows <= 0) {
			$result = $this->db_obj->nns_db_result ( 'NNS_ERROR_LIVE_PLAYBILL_NOT_FOUND', 'live playbill not found' );
		}
		
		return $result;
	}
	//playbill info
	public function nns_db_live_playbill_info($playbill_id) {
		$result = array ();
		//live_id
		if (! isset ( $playbill_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live playbill delete param error' );
			return $result;
		}
		if (empty ( $playbill_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live playbill delete param error' );
			return $result;
		}
		$str_sql = "select * from nns_live_playbill where nns_id='$playbill_id'";
		
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows <= 0) {
			$result = $this->db_obj->nns_db_result ( 'NNS_ERROR_LIVE_PLAYBILL_NOT_FOUND', 'live playbill not found' );
		}
		
		return $result;
	}
	//playbill delete
	public function nns_db_live_playbill_delete($playbill_id) {
		$result = array ();
		//live_id
		if (! isset ( $playbill_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live playbill delete param error' );
			return $result;
		}
		$str_sql = "delete from nns_live_playbill where nns_id='$playbill_id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		
		return $result;
	}
	//playbill count
	public function nns_db_live_playbill_count($live_id = null, $day = null, $stime = null, $etime = null, $order = 'nns_day') {
		$result = array ();
		$str_sql = "select count(1) as num from nns_live_playbill ";
		$str_sql .= $this->nns_db_live_playbill_where ( 0, 0, $live_id, $day, $stime, $etime, $order );
		$result = $this->db_obj->nns_db_query ( $str_sql );
		
		return $result;
	}
	
	//playbill list
	public function nns_db_live_playbill_list($min = 0, $num = 0, $live_id = null, $day = null, $stime = null, $etime = null, $order = 'nns_day') {
		$result = array ();
		$str_sql = "select * from nns_live_playbill ";
		$str_sql .= $this->nns_db_live_playbill_where ( $min, $num, $live_id, $day, $stime, $etime, $order );
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_LIVE_PLAYBILL_NO_DATA, 'playbill data not find' );
			}
		}
		return $result;
	}
	
	public function nns_db_live_playbill_where($min = 0, $num = 0, $live_id = null, $day = null, $stime = null, $etime = null, $order = 'nns_day') {
		$where = "";
		if (! empty ( $live_id )) {
			if (empty ( $where )) {
				$where .= " where nns_live_id = '$live_id'";
			} else {
				$where .= " and nns_live_id = '$live_id'";
			}
		}
		if (! empty ( $day )) {
			if (empty ( $where )) {
				$where .= " where nns_day = '$day'";
			} else {
				$where .= " and nns_day = '$day'";
			}
		}
		if (! empty ( $stime ) && ! empty ( $etime )) {
			if (empty ( $where )) {
				$where .= " where nns_day >= '$stime' and nns_day<='$etime'";
			} else {
				$where .= " and nns_day >= '$stime' and nns_day<='$etime'";
			}
		}
		//order
		if (isset ( $order )) {
			if (! empty ( $order )) {
				$where .= " order by $order desc";
			}
		}
		//limit
		if (isset ( $min ) && isset ( $num )) {
			if (! empty ( $num )) {
				$min = ( int ) $min;
				$num = ( int ) $num;
				$where .= " limit $min,$num";
			}
		}
		
		return $where;
	
	}

}

?>