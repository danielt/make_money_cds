<?php
/*
 * Created on 2012-8-22
 * by S67
 * 视频参数操作表
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 $nns_db_live_ex_dir = dirname ( dirname ( __FILE__ ) );
require_once $nns_db_live_ex_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
 class nns_db_live_ex_class{
	private $conn;
	private $db_obj;
	public function nns_db_live_ex_class() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
	}
	
	public function nns_db_live_ex_info($video_id,$key){
		$sql_str='select nns_value from nns_live_ex where ' .
				'nns_live_id="'.$video_id.'" and ' .
				'nns_key="'.$key.'" ' ;
				
		$result = $this->db_obj->nns_db_query ( $sql_str );
		if ($result['ret']!=0) return null;
		return $result['data'][0]['nns_value'];
	}
	
	public function nns_db_live_ex_infos($live_id, $key, $val) {
		if (empty($val))
			return false;
		$sql_str = 'select * from nns_live_ex where ' .
				'nns_value="' . $val . '" and ' .
				'nns_key="' . $key . '"  AND ' .
				'nns_live_id<>"' . $live_id . '"';
		$result = $this->db_obj->nns_db_query($sql_str);
		return $result;
	}
	
	public function nns_db_live_ex_infos_v2($live_id=null, $key, $val) {
		if (empty($val))
			return false;
		$sql_str = 'select * from nns_live_ex where ' .
				'nns_value="' . $val . '" and ' .
				'nns_key="' . $key . '"  and ' ;
		if(!empty($live_id))
		{
			$sql_str.=	'nns_live_id<>"' . $live_id . '"';
		}
		$sql_str=rtrim($sql_str,'and ');
		$result = $this->db_obj->nns_db_query($sql_str);
		return $result;
	}
	
	
	public function nns_db_live_ex_arr($video_id){

		$sql_str='select nns_value,nns_key from nns_live_ex where ' .
				'nns_live_id="'.$video_id.'" ' ;
				
		$result = $this->db_obj->nns_db_query ( $sql_str );
		if ($result['ret']!=0) return null;
		return $result['data'];
	}
	
	public function  nns_db_live_set_value($video_id,$key,$value){
		$is_value=$this->nns_db_live_ex_info($video_id,$key);
		if (isset($is_value)){
			$sql_str='update nns_live_ex set ' .
					'nns_value="'.$value.'" ' .
					'where ' .
				'nns_live_id="'.$video_id.'" and ' .
				'nns_key="'.$key.'" ' ;
		}else{
			$sql_str='insert into nns_live_ex ' .
					'(nns_live_id,nns_key,nns_value ) values ' .
			        '("'.$video_id.'",' .
			        '"'.$key.'",' .
			        '"'.$value.'")' ;
		}
		
		$result = $this->db_obj->nns_db_execute ( $sql_str );
		return $result;
	}
	
	public function nns_db_live_ex_clean_by_live_id($video_id){
		$sql_str='delete from nns_live_ex ' .
					'where ' .
				'nns_live_id="'.$video_id.'" ' ;
		$result = $this->db_obj->nns_db_execute ( $sql_str );
		return $result;
	}
 }
?>
