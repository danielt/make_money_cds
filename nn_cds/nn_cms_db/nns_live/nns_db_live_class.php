<?php
$nns_db_live_dir = dirname ( dirname ( __FILE__ ) );
require_once $nns_db_live_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
require_once $nns_db_live_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";
require_once $nns_db_live_dir . DIRECTORY_SEPARATOR . "nns_assist" . DIRECTORY_SEPARATOR . "nns_db_assist_bind_class.php";
require_once $nns_db_live_dir . DIRECTORY_SEPARATOR . "nns_memcached" . DIRECTORY_SEPARATOR . "memcached_manager.class.php";
include_once dirname(dirname ( dirname ( __FILE__ ) )).DIRECTORY_SEPARATOR.'nn_cms_config'
		 				.DIRECTORY_SEPARATOR.'nn_cms_global.php';
include_once dirname(dirname ( dirname ( __FILE__ ) )).DIRECTORY_SEPARATOR.'nn_logic'.DIRECTORY_SEPARATOR.'asset'
		 				.DIRECTORY_SEPARATOR.'asset_item.class.php';
include_once $nns_db_live_dir . DIRECTORY_SEPARATOR . "nns_label" . DIRECTORY_SEPARATOR . "nns_label.class.php";
class nns_db_live_class {
	private $conn;
	private $db_obj;

	public function __construct() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
	}

	//count
	public function nns_db_live_count($name = null, $state = null, $check = null, $view_type = null, $depot_id = null,
	$depot_detail_id = null, $org_type = null, $org_id = null, $tag = null, $live_info = null, $deleted = NNS_LIVE_DELETED_FALSE,
	$except_method=null,$except_detail_id=null,$except_id=null,$begin_sy_date=null,$end_sy_date=null,$begin_create_date=null,
	$end_create_date=null,$begin_copyright_date=null,$end_copyright_date=null,$searchpoint=null,$depot_like_detail_id=null
	,$summary=null,$index_num=null,$min_len=null,$max_len=null,$nns_pinyin=null) {
		$result = array ();
		$str_sql = "select count(1) as num from nns_live ";
		$str_sql .= $this->nns_db_live_where ( $name, $state, $check, $view_type,$depot_id,$depot_detail_id,
		$org_type, $org_id, $tag, $live_info, null, 0, 0, $deleted ,$except_method,$except_detail_id,$except_id,
		$begin_sy_date,$end_sy_date,$begin_create_date,$end_create_date,$begin_copyright_date,$end_copyright_date
		,$searchpoint,$depot_like_detail_id,$summary,$index_num,$min_len,$max_len,$nns_pinyin);

		$result = $this->db_obj->nns_db_query ( $str_sql );
		return $result;
	}
	/**
	 *2012-8-29 bo.chen@starcorcn.com 直播分类 影片统计
	 */
	public function nns_db_live_category_count($sql){
		$result = $this->db_obj->nns_db_query ( $sql );
		return $result;
	}

	//list live
	public function nns_db_live_list($field = null, $name = null, $state = null, $check = null, $view_type = null,
	 $depot_id = null, $depot_detail_id = null, $org_type = null, $org_id = null, $tag = null, $live_info = null,
	 $order = null, $min = 0, $num = 0, $deleted = NNS_LIVE_DELETED_FALSE,$except_method=null,$except_detail_id=null,
	 $except_id=null,$begin_sy_date=null,$end_sy_date=null,$begin_create_date=null,$end_create_date=null,$begin_copyright_date=null,
	 $end_copyright_date=null,$searchpoint=null,$depot_like_detail_id=null,$summary=null,$index_num=null,$min_len=null,
	 $max_len=null,$nns_pinyin=null) {
		$result = array ();
		if (empty ( $field )) {
			$field = " nns_name,nns_state,nns_view_type,nns_depot_id,nns_category_id,nns_org_type,nns_org_id,nns_tag0,nns_director,nns_actor,nns_show_time,nns_area,nns_create_time ";
		}
		$str_sql = " select " . $field . " from nns_live ";
		$str_sql .= $this->nns_db_live_where ( $name, $state, $check, $view_type,$depot_id,$depot_detail_id,
		$org_type, $org_id, $tag, $live_info, $order, $min, $num, $deleted,$except_method,$except_detail_id,
		$except_id,$begin_sy_date,$end_sy_date,$begin_create_date,$end_create_date,$begin_copyright_date,
		$end_copyright_date,$searchpoint,$depot_like_detail_id,$summary,$index_num,$min_len,$max_len,$nns_pinyin );
		
		//die($str_sql);
// echo $str_sql;die;
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_LIVE_NO_DATA, 'live data not find' );
			}
		}
		return $result;
	}
	
	//info
	public function nns_db_live_info($id, $field = null) {
		$result = array ();
		if (! isset ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live info param error' );
			return $result;
		}
		if (empty ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live info param error' );
			return $result;
		}
		if (empty ( $field )) {
			$field = " nns_id,nns_name,nns_state,nns_view_type,nns_depot_id,nns_category_id,nns_org_type,nns_org_id,nns_tag0,nns_director,nns_actor,nns_show_time,nns_create_time,nns_area,nns_summary ";
		}

		$str_sql = "select " . $field . " from nns_live where nns_id='$id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_LIVE_NOT_FOUND, 'live info not find' );
			}
		}
		return $result;
	}

	//add
	public function nns_db_live_add($name, $view_type = 1,$depot_id=null,$depot_detail_id=null, $org_type = 0, $org_id = null,
	$tag = null ,$live_info = null, $summary = null,$point=null, $copyright_date=null,$remark = null,$nns_pinyin=null,$check=null,$nns_cp_id=null,$nns_import_id=null,$nns_pinyin_length = '0') {

		//	修改影片信息输入
		foreach ($live_info as $info_key=>$info_value){
			if ($info_key=='actor' || $info_key=='director'){
				$space=false;
			}else{
				$space=true;
			}
			if (!empty($info_value)){
				$live_info[$info_key]=np_build_split_string($info_value,'/',$space);
			}
		}

		$result = array ();
		$name=str_replace("'"," ",$name);
		$summary=str_replace("'"," ",$summary);
		if (! isset ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live add param error' );
			return $result;
		}
		if (empty ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live add param error' );
			return $result;
		}
		//生成GUID
		$guid = new Guid ();
		$id = $guid->toString ();
		//exist
		$str_sql = "select 1 from nns_live where nns_id='$id' ";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_LIVE_EXIST, 'live name exist' );
			return $result;
		}
		//check

		$check = ( int ) $check;
		//state
		$state = NNS_VOD_STATE_WAITING;
		$state = ( int ) $state;
		//view_type
		$view_type = ( int ) $view_type;
		$point = ( int ) $point;
		$time_value=$time_key='';
		if (!empty($copyright_date)){

			$time_key.=",nns_copyright_date";
			$time_value.=",'$copyright_date'";
		}
		//insert
		$str_sql = "insert into nns_live";
		$field = "nns_id,nns_name,nns_check,nns_state,nns_view_type,nns_depot_id,nns_category_id,nns_point".$time_key;
		$values = "'$id','$name',$check,$state,$view_type,'$depot_id','$depot_detail_id',$point".$time_value;
		//org
		if ($org_type !== null && $org_type !== '') {
			$org_type = ( int ) $org_type;
			$field .= ",nns_org_type";
			$values .= ",$org_type";
		}
		if ($org_id != null) {
			$field .= ",nns_org_id";
			$values .= ",'$org_id'";
		}
		/**
		 * 原tag存储方式废除，改为单个字段存储标识 BY S67
		 */
//		//tag
//		if (! empty ( $tag )) {
//			if (is_array ( $tag )) {
//				for($i = 0; $i < count ( $tag ); $i ++) {
//					$temp = "tag" . $i;
//					if ($tag [$i] !== null && $tag [$i] !== '') {
//						$field .= ",nns_" . $temp;
//						$tag_value = ( int ) $tag [$i];
//						$values .= ",$tag_value";
//					}
//
//				} //for
//			}
//
//		} // tag end if

		/**
		 * 新tag存储方式
		 */
		 if (! empty ( $tag )) {
			if (is_array ( $tag )) {
				$len=count ( $tag );
				$field .= ",nns_tag ";
				$tag_value="";
				for($i = 0; $i < $len; $i ++) {
					if (!empty($tag [$i])){
						$tag_value .= $tag [$i].',';
					}
				} //for
				$values .= ",'$tag_value'";
			}

		}
		//live_info
		if (! empty ( $live_info )) {
			if (is_array ( $live_info )) {
				if ($live_info ["director"] != null) {
					$field .= ",nns_director";
					$values .= ",'" . $live_info ["director"] . "'";
				}
				if ($live_info ["actor"] != null) {
					$field .= ",nns_actor";
					$values .= ",'" . $live_info ["actor"] . "'";
				}
				if ($live_info ["show_time"] != null) {
					$field .= ",nns_show_time";
					$values .= ",'" . $live_info ["show_time"] . "'";
				}
				if ($live_info ["view_len"] != null) {
					$field .= ",nns_view_len";
					$values .= "," . $live_info ["view_len"];
				}
				if ($live_info ["all_index"] != null) {
					$field .= ",nns_all_index";
					$values .= "," . $live_info ["all_index"];
				}
				if (strlen($live_info ["producer"]) >0) {
				    $field .= ",nns_producer";
				    $values .= ",'" . $live_info ["producer"] . "'";
				}
			    if (strlen($live_info ["import_source"]) >0) {
				    $field .= ",nns_import_source";
				    $values .= ",'" . $live_info ["import_source"] . "'";
				}
				if ($live_info ["new_index"] != null) {
					$field .= ",nns_new_index";
					$values .= "," . $live_info ["new_index"];
				}
				if ($live_info ["area"] != null) {
					$field .= ",nns_area";
					$values .= ",'" . $live_info ["area"] . "'";
				}
				if ($live_info ["alias_name"] != null) {
					$field .= ",nns_alias_name";
					$values .= ",'" . $live_info ["alias_name"] . "'";
				}
				if ($live_info ["language"] != null) {
					$field .= ",nns_language";
					$values .= ",'" . $live_info ["language"] . "'";
				}
				if ($live_info ["eng_name"] != null) {
					$field .= ",nns_eng_name";
					$values .= ",'" . $live_info ["eng_name"] . "'";
				}
				if (isset($live_info ["nns_play_count"])) {
					$field .= ",nns_play_count";
					$values .= ",'" . (int)$live_info ["nns_play_count"] . "'";
				}
                //if (isset($live_info['import_id']))
                //{
                //    $field .= ",nns_import_id";
                //    $values .= ",'{$live_info['import_id']}'";
                //}
			}
		} //liveinfo  end if


		if ($summary != null) {
			$field .= ",nns_summary";
			$values .= ",'$summary'";
		}
		if ($remark != null) {
			$field .= ",nns_remark";
			$values .= ",'$remark'";
		}
		if ($nns_pinyin != null) {
			$field .= ",nns_pinyin";
			$values .= ",'$nns_pinyin'";
		}
		if ($nns_cp_id != null) {
			$field .= ",nns_cp_id";
			$values .= ",'$nns_cp_id'";
		}
		if($nns_import_id != null)
		{
			$field .= ",nns_import_id";
			$values .= ",'{$nns_import_id}'";
		}
		$field .= ",nns_pinyin_length";
		$values .= ",'{$nns_pinyin_length}'";

		$field .= ",nns_create_time,nns_modify_time,nns_deleted";
		$values .= ",now(),now()," . NNS_LIVE_DELETED_FALSE;

		$str_sql .= "(" . $field . ")values(" . $values . ")";
		// live
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		if ($result ["ret"] == 0) {
			$data ["id"] = $id;
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_OK, 'live add ok', $data );
			
			//福建移动
							

			//			加入标签
//			新增首字母标签类型
			if($nns_pinyin !=null){
				$vod_info['initial'] = substr($nns_pinyin,0,1);
			}

			$label_inst=new nns_label();
			$label_inst->add_label(
				$id,
				1,
				$live_info
			);
		}

		if ($check==1){
			$this->nns_db_live_check($id,$check);
		}

		return $result;

	}
	public function nns_db_live_move_depot_category($id,$depot_id=null,$depot_detail_id=null){
		$result = array ();
		if (! isset ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live modify param error' );
			return $result;
		}
		if (empty ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live modify param error' );
			return $result;
		}
		//id exist
		$str_sql = "select 1 from nns_live where nns_id='$id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result [ret] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			//update
			$str_sql = "update nns_live set nns_depot_id='$depot_id',nns_category_id='$depot_detail_id' ";
			$str_sql .= " where nns_id='$id'";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
		}
		return $result;
	}
	//modify
	public function nns_db_live_modify($id, $name, $view_type = 1, $depot_id=null, $depot_detail_id=null, $org_type = 0,
	$org_id = null, $tag = null, $live_info = null, $summary = null,$point=null,$copyright_date=null, $remark = null,$nns_pinyin=null,$nns_cp_id=null,$nns_import_id=null,$nns_pinyin_length = '0') {
		//	修改影片信息输入
		foreach ($live_info as $info_key=>$info_value){
			if ($info_key=='actor' || $info_key=='director'){
				$space=false;
			}else{
				$space=true;
			}
			if (!empty($info_value)){
				$live_info[$info_key]=np_build_split_string($info_value,'/',$space);
			}
		}

		$result = array ();
		$name=str_replace("'"," ",$name);
		$summary=str_replace("'"," ",$summary);
		if (! isset ( $id ) || ! isset ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live modify param error' );
			return $result;
		}
		if (empty ( $id ) || empty ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live modify param error' );
			return $result;
		}

//		//name exist
//		$str_sql = "select 1 from nns_live where nns_id != '$id' ";
//		$result = $this->db_obj->nns_db_query ( $str_sql );
//		if ($result [ret] != 0) {
//			return $result;
//		}
//		if ($this->db_obj->num_rows > 0) {
//			$result = $this->db_obj->nns_db_result ( NNS_ERROR_LIVE_EXIST, 'live name exist' );
//			return $result;
//		}
		//view_type
		$view_type = ( int ) $view_type;
		$point = ( int ) $point;
		if (!empty($copyright_date)){
			$time_str.=",nns_copyright_date='$copyright_date' ";
		}
		//id exist
		$str_sql = "select 1 from nns_live where nns_id='$id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );

		if ($result [ret] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			//update
			$str_sql = "update nns_live set nns_name='$name',nns_view_type=$view_type,nns_depot_id='$depot_id',nns_category_id='$depot_detail_id',nns_point=$point".$time_str;
			//org
			if ($org_type !== null && $org_type !== '') {
				$org_type = ( int ) $org_type;
				$str_sql .= ",nns_org_type=$org_type";
			}
			if ($org_id != null) {
				$str_sql .= ",nns_org_id='$org_id'";
			}

		/**
		 * 原tag存储方式废除，改为单个字段存储标识 BY S67
		 */
//			//tag
//			if (! empty ( $tag )) {
//				if (is_array ( $tag )) {
//					for($i = 0; $i < count ( $tag ); $i ++) {
//						$temp = "tag" . $i;
//						if ($tag [$i] !== null && $tag [$i] !== '') {
//							$tag_value = ( int ) $tag [$i];
//							$str_sql .= ",nns_" . $temp . "=" . $tag_value;
//						}
//
//					} //for
//				}
//
//			} // tag end if

		/**
		 * 新tag存储方式 BY S67
		 */
		 if(isset($tag) && !empty($tag) && is_array($tag))
		 {
		     $str_sql .= ",nns_tag ='".implode(',', $tag).",'";
		     $tag_value = implode(',', $tag).",";
		 }
		 else
		 {
		     $str_sql .= ",nns_tag ='26,'";
		     $tag_value = "26,";
		 }
			//live_info
			if (! empty ( $live_info )) {
				if (is_array ( $live_info )) {
					if ($live_info ["director"] != null) {
						$str_sql .= ",nns_director='" . $live_info ["director"] . "'";
					}
					if ($live_info ["actor"] != null) {
						$str_sql .= ",nns_actor='" . $live_info ["actor"] . "'";
					}
					if (!empty($live_info ["show_time"])) {
						$str_sql .= ",nns_show_time='" . $live_info ["show_time"] . "'";
					}
					if ($live_info ["view_len"] != null) {
						$str_sql .= ",nns_view_len=" . $live_info ["view_len"];
					}
					if ($live_info ["all_index"] != null) {
						$str_sql .= ",nns_all_index=" . $live_info ["all_index"];
					}
					if ($live_info ["new_index"] != null) {
						$str_sql .= ",nns_new_index=" . $live_info ["new_index"];
						;
					}
					
					if (strlen($live_info ["producer"]) >0) {
					    $str_sql .= ",nns_producer='" . $live_info ["producer"]."'";
					}
					if (strlen($live_info ["import_source"]) >0) {
					    $str_sql .= ",nns_import_source='" . $live_info ["import_source"]."'";
					}
					
					if ($live_info ["area"] != null) {
						$str_sql .= ",nns_area='" . $live_info ["area"] . "'";
					}
					if ($live_info ["alias_name"] != null) {
						$str_sql .= ",nns_alias_name='" . $live_info ["alias_name"] . "'";
					}
					if ($live_info ["language"] != null) {
						$str_sql .= ",nns_language='" . $live_info ["language"] . "'";
					}
					if ($live_info ["eng_name"] != null) {
						$str_sql .= ",nns_eng_name='" . $live_info ["eng_name"] . "'";
					}
					if (isset($live_info ["nns_play_count"])) {
						$str_sql .= ",nns_play_count='" . (int) $live_info ["nns_play_count"]. "'";
					}
					if (isset($live_info['import_id']))
                    {
                        $str_sql .= ",nns_import_id='{$live_info['import_id']}'";
                    }
				}
			} //liveinfo  end if


			if ($summary != null) {
				$str_sql .= ",nns_summary='$summary'";
			}
			if ($remark != null) {
				$str_sql .= ",nns_remark='$remark'";
			}
			if ($nns_pinyin != null) {
				$str_sql .= ",nns_pinyin='$nns_pinyin'";
			}
			if ($nns_cp_id != null) {
				$str_sql .= ",nns_cp_id='$nns_cp_id'";
			}
			if ($nns_import_id != null) {
				$str_sql .= ",nns_import_id='$nns_import_id'";
			}
			$str_sql .= ",nns_pinyin_length='{$nns_pinyin_length}'";
			$str_sql .= ",nns_create_time=now() ";
			$str_sql .= " where nns_id='$id'";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
//			修改相应媒资包，服务包绑定数据
			if ($result["ret"]==0){
				//			加入标签
				//			新增首字母标签类型
				if($nns_pinyin !=null){
					$vod_info['initial'] = substr($nns_pinyin,0,1);
				}

				$label_inst=new nns_label();
				$label_inst->add_label(
					$id,
					1,
					$live_info
				);

				$str_sql="update nns_assists_item set " .
						"nns_point=$point," .
						"nns_video_name='$name'," .
						"nns_pinyin='$nns_pinyin'," .
						"nns_english_index='".$live_info ["eng_name"]."', " .
						"nns_tag='$tag_value' where nns_video_id='$id'";
				$result = $this->db_obj->nns_db_execute ( $str_sql );
				//$str_sql="update nns_service_item set nns_point=$point,nns_video_name='$name' where nns_video_id='$id'";
				//$result = $this->db_obj->nns_db_execute ( $str_sql );
				//$str_sql="update nns_service_detail_video set nns_video_name='$name',nns_point=$point where nns_video_id='$id'";
				//$result = $this->db_obj->nns_db_execute ( $str_sql );

				$this->clear_memcache_by_asset_service($id);
			}
		}
		;
		return $result;

	}
	private function nns_db_live_modify_deleted($id, $deleted) {
		$result = array ();
		if (! isset ( $id ) || ! isset ( $deleted )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live delete param error' );
			return $result;
		}

		if (empty ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live delete param error' );
			return $result;
		}
		$deleted = ( int ) $deleted;
		$str_sql = "update nns_live_media set nns_deleted=" . $deleted . " where nns_live_id='$id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );

		$str_sql = "update nns_live set nns_deleted=" . $deleted . " where nns_id='$id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );

		return $result;
	}
	public function nns_db_live_real_delete($id) {
		$result = array ();
		if (! isset ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live delete param error' );
			return $result;
		}

		if (empty ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live delete param error' );
			return $result;
		}

		$str_sql = "delete from nns_live_media where nns_live_id='$id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );

		$str_sql = "delete from  nns_live  where nns_id='$id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );

		//		删除标签库
		$label_inst=new nns_label();
		$label_inst->delete_label(
			$id,
			1
		);

		return $result;
	}
	//delete
	public function nns_db_live_delete($id) {
		return $this->nns_db_live_modify_deleted ( $id, NNS_LIVE_DELETED_TRUE );
	}
	//retrieve
	public function nns_db_live_retrieve($id) {
		return $this->nns_db_live_modify_deleted ( $id, NNS_LIVE_DELETED_FALSE );
	}
	//upload images
	public function nns_db_live_upload_images($id, $images) {
		$result = array ();
		if (! isset ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live upload param error' );
			return $result;
		}
		if (empty ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live upload param error' );
			return $result;
		}
		if ($images == null) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live upload param error' );
			return $result;
		}
		if (! is_array ( $images )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live upload param error' );
			return $result;
		}

		//exist
		$str_sql = "select 1 from nns_live where nns_id='$id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			//update
			$str_sql = "";
			$str_sql_asset='';
			for($i = 0; $i < count ( $images ); $i ++) {
				$temp = "image" . $i;
				if ($images [$temp] != null) {
					if (empty ( $str_sql )) {
						$str_sql .= "nns_" . $temp . "='" . $images [$temp] . "'";
						$str_sql_asset .= "nns_video_img" . $i . "='" . $images [$temp] . "'";
					} else {
						$str_sql .= ",nns_" . $temp . "='" . $images [$temp] . "'";
						$str_sql_asset .= ",nns_video_img" . $i . "='" . $images [$temp] . "'";
					}
				}
			} //for
			//sql
			if (!empty($str_sql)){
			$str_sql = "update nns_live set " . $str_sql . " ,nns_modify_time=now() where nns_id='$id'";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
//			修改相应媒资包海报 BY S67 2012-9-9
				if ($result["ret"]==0){
					$str_sql="update nns_assists_item set " . $str_sql_asset . " where nns_video_id='$id'";
					$result = $this->db_obj->nns_db_execute ( $str_sql );
				}
			}else{
				$result["ret"]=0;
				return $result;
			}
		} else {
			//insert
			$str_sql = " insert into nns_live ";
			$filed = "nns_id,nns_state,nns_create_time,nns_modify_time";
			$value = "'$id'," . NNS_LIVE_STATE_WAITING . ",now(),now()";
			for($i = 0; $i < count ( $images ); $i ++) {
				$temp = "image" . $i;
				if ($images [$temp] != null) {
					$filed .= ",nns_" . $temp;
					$value .= ",'" . $images [$temp] . "'";

				} //for
			}
			//sql
			if ($filed == null) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live upload param error' );
				return $result;
			}
			$str_sql .= "( " . $filed . ")values(" . $value . ")";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
		} //end if else
		return $result;
	}

	//edit images
	public function nns_db_live_modify_images($id, $images)
    {
        $result = array();
        if (! isset($id) || empty($id) || ! is_array($images) || empty($images)) 
        {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'live upload param error');
            return $result;
        }
        
        $str_sql = "select 1 from nns_live where nns_id='$id'";
        $result = $this->db_obj->nns_db_query($str_sql);
        if ($result["ret"] != 0) 
        {
            return $result;
        }
        $temp_array = null;
        foreach ($images as $key => $val) 
        {
            if (strpos($key, 'nns_') === FALSE) 
            {
                unset($images[$key]);
                $key = 'nns_' . $key;
            }
            $images[$key] = $val;
        }
        if ($this->db_obj->num_rows > 0) 
        {
            $images['nns_modify_time'] = date("Y-m-d H:i:s");
            foreach ($images as $key => $val) 
            {
                $temp_array[] = "{$key}='{$val}'";
            }
            $str_sql = "update nns_live set " . implode(',', $temp_array) . " where nns_id='$id'";
        } 
        else 
        {
            $images['nns_id'] = $id;
            $images['nns_state'] = NNS_LIVE_STATE_WAITING;
            $images['nns_create_time'] = $images['nns_modify_time'] = date("Y-m-d H:i:s");
            $field_str = $value_str = '';
            foreach ($images as $key => $val) 
            {
                $field_str .= $key . ',';
                $value_str .= "'{$val}',";
            }
            $field_str = rtrim($field_str, ',');
            $value_str = rtrim($value_str, ',');
            $str_sql = "insert into nns_live ({$field_str}) values(($value_str})";
        }
        return $this->db_obj->nns_db_execute($str_sql);
    }
    
	//image info
	public function nns_db_live_images_info($id) {
		$result = array ();
		if (! isset ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live upload param error' );
			return $result;
		}
		if (empty ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live upload param error' );
			return $result;
		}
		$str_sql = "select nns_id,nns_name, nns_image0,nns_image1,nns_image2,nns_image3,nns_image4,nns_image5 from nns_live where nns_id='$id' limit 1";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_LIVE_NOT_FOUND, 'live images info not find' );
			}
		}

		return $result;
	}
	//state
	public function nns_db_live_state($id, $state) {
		$result = array ();
		if (! isset ( $id ) || ! isset ( $state )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live modify state param error' );
			return $result;
		}
		if (empty ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live modify state param error' );
			return $result;
		}
		$state = ( int ) $state;
		if ($state == NNS_LIVE_AND_MEDIA_STATE_ONLINE) {
			$str_sql = "update nns_live_media set nns_state=" . NNS_LIVE_MEDIA_STATE_ONLINE . " where nns_live_id='$id'";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
			if ($result ["ret"] != 0) {
				return $result;
			}
		}

		$str_sql = "update nns_live set nns_state=" . $state . " where nns_id='$id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );

		return $result;
	}
	//check
	public function nns_db_live_check($id, $check) {
		$result = array ();
		if (! isset ( $id ) || ! isset ( $check )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live modify check param error' );
			return $result;
		}
		if (empty ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live modify check param error' );
			return $result;
		}
		if ($check === null && $check === '') {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live modify check param error' );
			return $result;
		}
		$check = ( int ) $check;
		if ($check == NNS_LIVE_AND_MEDIA_CHECK_OK) {
			$str_sql = "update nns_live_media set nns_check=" . NNS_LIVE_MEDIA_CHECK_OK . " where nns_live_id='$id'";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
			if ($result ["ret"] != 0) {
				return $result;
			}
		}

		$str_sql = "update nns_live set nns_check=" . $check . " where nns_id='$id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		if ($result["ret"]==0){
//			$str_sql="update nns_assists_item set nns_check=$check where nns_video_id='$id'";
//			$result = $this->db_obj->nns_db_execute ( $str_sql );

//			同步媒资库绑定栏目
			if ($check==NNS_VOD_AND_MEDIA_CHECK_OK || $check==NNS_VOD_MEDIA_CHECK_OK){
				$result_info_arr=$this->nns_db_live_info($id,"nns_depot_id,nns_category_id,nns_name,nns_point,nns_image0,nns_image1,nns_image2,nns_image3,nns_image4,nns_image5,nns_pinyin,nns_eng_name,nns_play_count,nns_tag");
				if ($result_info_arr["ret"]==0){
					$result_info=$result_info_arr["data"][0];
					$depot_id=$result_info["nns_depot_id"];
					$depot_detail_id=$result_info["nns_category_id"];
					$name=$result_info["nns_name"];
					$point=$result_info["nns_point"];
					$img1=$result_info["nns_image0"];
					$img2=$result_info["nns_image1"];
					$img3=$result_info["nns_image2"];
					$img4=$result_info["nns_image3"];
					$img5=$result_info["nns_image4"];
					$img6=$result_info["nns_image5"];
					$nns_pinyin=$result_info["nns_pinyin"];
					$nns_eng_name=$result_info["nns_eng_name"];
					$nns_play_count=$result_info["nns_play_count"];
					$nns_tag=$result_info["nns_tag"];
				}
				$point=(int)$point;
				$assest_inst=new nns_db_assist_bind_class();
				$assest_result=$assest_inst->nns_db_assest_bind_video_category_list(null,null,$depot_id,$depot_detail_id);
				$assest_inst=null;
				if (count($assest_result["data"])>0){
					foreach ($assest_result["data"] as $assest_item){
						$str_sql = "select count(1) as num from nns_assists_item where nns_category_id='".$assest_item["nns_assest_category_id"]."' and nns_assist_id='".$assest_item["nns_assest_id"]."'";
						$result_count = $this->db_obj->nns_db_query ( $str_sql );

						$this->clear_memcache('asset',$assest_item["nns_assest_id"],$assest_item["nns_assest_category_id"]);

						$num=$result_count["data"][0]["num"];
						$guid = new Guid();
						$assest_item_id = $guid->toString();
						$str_sql="insert into nns_assists_item(nns_id,nns_video_id,nns_video_type,nns_video_name,nns_point,nns_category_id,nns_assist_id,nns_check,nns_order,nns_video_img0,nns_video_img1,nns_video_img2,nns_video_img3,nns_video_img4,nns_video_img5,nns_pinyin,nns_english_index,nns_tag,nns_play_count)values" .
			"('$assest_item_id','$id',1,'$name',$point,'".$assest_item["nns_assest_category_id"]."','".$assest_item["nns_assest_id"]."',$check,$num,'$img1','$img2','$img3','$img4','$img5','$img6','$nns_pinyin','$nns_eng_name','$nns_tag','$nns_play_count')";
						$this->db_obj->nns_db_execute ( $str_sql );
					}
				}
			}
		}
		return $result;
	}
	//where
	public function nns_db_live_where($name = null, $state = NNS_LIVE_MEDIA_STATE_WAITING,
	$check = NNS_LIVE_MEDIA_CHECK_WAITING, $view_type = null, $depot_id = null, $depot_detail_id = null,
	$org_type = null, $org_id = null, $tag = null, $live_info = null, $order = 'nns_modify_time',
	$min = 0, $num = 0, $deleted = NNS_LIVE_DELETED_FALSE,$except_method=null,$except_detail_id=null,
	$except_id=null,$begin_sy_date=null,$end_sy_date=null,$begin_create_date=null,$end_create_date=null,
	$begin_copyright_date=null,$end_copyright_date=null,$searchpoint=null,$depot_like_detail_id=null
	,$summary=null,$index_num=null,$min_len=null,$max_len=null,$nns_pinyin=null) {
		$where = "";

		if ($except_method=="assist"){
			$where .=" where nns_id not in (select nns_video_id from nns_assists_item where nns_assist_id='$except_detail_id' and nns_category_id='$except_id' and nns_video_type=1)";
		}else if ($except_method=="service"){
			$where .=" where nns_id not in (select nns_video_id from nns_service_detail_video where nns_service_category_id='$except_detail_id' and nns_video_type=1)";
		}

		if (!empty($max_len)) {
			$min_len = ( int ) $min_len;
			$max_len = ( int ) $max_len;
			if (empty ( $where )) {
				$where .= " where  nns_view_len > $min_len and nns_view_len < $max_len";
			} else {
				$where .= " and nns_view_len > $min_len and nns_view_len < $max_len";
			}
		}
		if (! empty ( $index_num )) {
			$index_num=(int)$index_num;
			if (empty ( $where )) {
				$where .= " where nns_all_index = $index_num";
			} else {
				$where .= " and nns_all_index = $index_num";
			}
		}

		if (! empty ( $summary )) {
			if (empty ( $where )) {
				$where .= " where nns_summary like '%$summary%'";
			} else {
				$where .= " and nns_summary like '%$summary%'";
			}
		}

		if (! empty ( $name )) {
			
			if (is_array($name)){
				$query=' (';
				foreach ($name as $k=>$v){
					$query.= " {$k} like '%{$v}%' or";
				}
				
				$query=rtrim($query,'or');
				$query.=')';
			}else{
				$query=" nns_name like '%$name%' ";
			}
			
			if (empty ( $where )) {
				$where .= " where {$query}";
			} else {
				$where .= " and {$query}";
			}
		}
		if (! empty ( $nns_pinyin )) {
			if (empty ( $where )) {
				$where .= " where nns_pinyin like '$nns_pinyin%'";
			} else {
				$where .= " and nns_pinyin like '$nns_pinyin%'";
			}
		}
		//state
		if ($state !== null && $state !== '') {
			$state = ( int ) $state;
			if (empty ( $where )) {
				$where .= " where nns_state=$state";
			} else {
				$where .= " and nns_state=$state";
			}
		}
		//check
		if ($check !== null && $check !== '') {
			$check = ( int ) $check;
			if (empty ( $where )) {
				$where .= " where nns_check=$check";
			} else {
				$where .= " and nns_check=$check";
			}
		}
		//view_type
		if ($view_type !== null && $view_type !== '') {
			$view_type = ( int ) $view_type;
			if (empty ( $where )) {
				$where .= " where nns_view_type=$view_type";
			} else {
				$where .= " and nns_view_type=$view_type";
			}
		}
		//depot_id
		if (! empty ( $depot_id )) {
			if (empty ( $where )) {
				$where .= " where nns_depot_id='$depot_id'";
			} else {
				$where .= " and nns_depot_id='$depot_id'";
			}
		}
		//depot_detail_id
		if (! empty ( $depot_detail_id )) {
			if (empty ( $where )) {
				$where .= " where nns_category_id='$depot_detail_id'";
			} else {
				$where .= " and nns_category_id='$depot_detail_id'";
			}
		}
		//depot_detail_id
		if (! empty ( $depot_like_detail_id )) {
			if (empty ( $where )) {
				$where .= " where nns_category_id like '$depot_like_detail_id%'";
			} else {
				$where .= " and nns_category_id like '$depot_like_detail_id%'";
			}
		}

		//org
		if ($org_type !== null && $org_type !== '') {
			$org_type = ( int ) $org_type;
			if (empty ( $where )) {
				$where .= " where nns_org_type=$org_type";
			} else {
				$where .= " and nns_org_type=$org_type";
			}
		}
		//org_id
		if (! empty ( $org_id )) {
			if (empty ( $where )) {
				$where .= " where nns_org_id='$org_id'";
			} else {
				$where .= " and nns_org_id='$org_id'";
			}
		}
/**
 * 废除原TAG查询方式 BY S67
 */
//		//tag
//		if (! empty ( $tag )) {
//			if (is_array ( $tag )) {
//				$tag_str = null;
//				for($i = 0; $i < count ( $tag ); $i ++) {
//					if ($tag [$i] !== null && $tag [$i] !== '') {
//						$temp = "nns_tag" . $tag [$i];
//
//						if (empty ( $tag_str )) {
//							$tag_str .= "  $temp=1";
//						} else {
//							$tag_str .= " or $temp=1";
//						}
//					}
//
//				} //for
//				if (! empty ( $tag_str )) {
//					if (empty ( $where )) {
//						$where .= " where (" . $tag_str . ")";
//					} else {
//						$where .= " and  (" . $tag_str . ")";
//					}
//				}
//			} //end if is array
//		}

/**
 * 新增TAG查询方式 BY S67
 */
 		if (! empty ( $tag )) {
			if (is_array ( $tag )) {
				$tag_str = '';
				for($i = 0; $i < count ( $tag ); $i ++) {
					if (!empty($tag [$i])) {
						$tag_str.=$i.',';
					}

				} //for
				if (! empty ( $tag_str )) {
					$tag_str=','.$tag_str;
					if (empty ( $where )) {
						$where .= " where nns_tag like '%" . $tag_str . "%' ";
					} else {
						$where .= " and nns_tag like '%" . $tag_str . "%' ";
					}
				}
			} //end if is array
		}
		//live_info
		if (! empty ( $live_info )) {
			if (is_array ( $live_info )) {
				if (! empty ( $live_info ["director"] )) {
					if (empty ( $where )) {
						$where .= " where nns_director like '%" . $live_info ["director"] . "%'";
					} else {
						$where .= " and nns_director like '%" . $live_info ["director"] . "%'";
					}
				}
				if (! empty ( $live_info ["actor"] )) {
					if (empty ( $where )) {
						$where .= " where nns_actor like '%" . $live_info ["actor"] . "%'";
					} else {
						$where .= " and nns_actor like '%" . $live_info ["actor"] . "%'";
					}
				}
				if (! empty ( $live_info ["show_time"] )) {
					$temp = date ( 'Y-m-d', strtotime ( $live_info ["show_time"] ) );
					if (empty ( $where )) {
						$where .= " where nns_show_time='" . $temp . "'";
					} else {
						$where .= " and nns_show_time='" . $temp . "'";
					}
				}
				if (! empty ( $live_info ["area"] )) {
					if (empty ( $where )) {
						$where .= " where nns_area like '%" . $live_info ["area"] . "%'";
					} else {
						$where .= " and nns_area like '%" . $live_info ["area"] . "%'";
					}
				}
			}
		}

		if (!empty($begin_sy_date) && !empty($end_sy_date)){
			$where.=" and nns_show_time>='$begin_sy_date' and nns_show_time<='$end_sy_date' ";
		}
		if (!empty($begin_create_date) && !empty($end_create_date)){
			$where.=" and nns_create_time>='$begin_create_date' and nns_create_time<='$end_create_date' ";
		}
		if (!empty($begin_copyright_date) && !empty($end_copyright_date)){
			$where.=" and nns_copyright_date>='$begin_copyright_date' and nns_copyright_date<='$end_copyright_date' ";
		}
		if (!empty($searchpoint)){
			$where.=" and INSTR('$searchpoint',nns_point)>0 ";
		}

		//deleted
		if ($deleted != "-1") {
			$deleted = ( int ) $deleted;
			if (empty ( $where )) {
				$where .= " where nns_deleted =$deleted";
			} else {
				$where .= " and nns_deleted =$deleted";
			}
		}

		//order
		if (isset ( $order )) {
			if (! empty ( $order )) {
				$where .= " order by $order desc";
			} else {
				$where .= " order by nns_modify_time desc";
			}
		}
		//limit
		if (isset ( $min ) && isset ( $num )) {
			if (! empty ( $num )) {
				$min = ( int ) $min;
				$num = ( int ) $num;
				$where .= " limit $min,$num";
			}
		}

		return $where;
	}

	public  function check_bind_state($live_id,$index_num){
		$sql_str="select count(1)  as num  from nns_live_media where nns_live_id='$live_id' group  by nns_live_index";
		$result = $this->db_obj->nns_db_query ( $sql_str );

		if ($index_num>count($result["data"])){
			$bool=false;
		}else{
			$bool=true;
		}
		return $bool;
	}


	public function get_live_by_ids($media_ids){
		if(empty($media_ids)) return false;
		$str_in = '';
		foreach($media_ids as $media_id){
			$str_in .="'$media_id',";
		}
		$str_in = rtrim($str_in,',');
		$str_sql =  "select * from  nns_live where nns_id in($str_in)";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		return isset($result['data'])?$result['data']:false;
	}

	//	清除媒资服务包缓存
	public function clear_memcache_by_asset_service($id){
		$str_sql="select nns_category_id,nns_assist_id from nns_assists_item where nns_video_id='$id'";
		$result = $this->db_obj->nns_db_query( $str_sql );
		$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
			));
		$dc->open();


		foreach ($result['data'] as $item){
			$this->clear_memcache('asset',$item['nns_assist_id'],$item['nns_category_id']);

			nl_asset_item::delete_asset_item_list_by_child_cache($dc,$item['nns_assist_id']);
		}
		/*
		$str_sql="select nns_category_id,nns_service_id from nns_service_item where nns_video_id='$id'";
		$result = $this->db_obj->nns_db_query( $str_sql );
		foreach ($result['data'] as $item){
			$this->clear_memcache('service',$item['nns_service_id'],$item['nns_category_id']);
		}
		*/


	}


	private function clear_memcache($type,$package,$category){
						global $g_mem_cache_enabled;

		 				if ($g_mem_cache_enabled==1){
		 					$mem_inst=memcached_manager::get_instance();
		 					$mem_inst->delete_cache(
										'index_'
		 								.$type.'_'
		 								.$package.'_'
		 								.$category);

		 					$dc=nl_get_dc(array(
								'db_policy'=>NL_DB_WRITE,
								'cache_policy'=>NP_KV_CACHE_TYPE_MEMCACHE
								));
								$dc->open();
		 					nl_asset_item::delete_asset_item_list_by_child_cache($dc,$package);
		 				}
	}
	
	
	
	/***
	 * 批量修改EPG标识
	 */
	 
	 public function edit_epg_tags($videos,$tags){
	 	
	 	$video_ids=implode("','",$videos);
	 	
	 	$str_sql="update nns_live set nns_tag='{$tags}' where nns_id in ('{$video_ids}')";
		$result = $this->db_obj->nns_db_execute( $str_sql );
		if ($result['ret']==0){
			$str_sql="update nns_assists_item set nns_tag='{$tags}' where nns_video_id in ('{$video_ids}') and nns_video_type='1'";
			$this->db_obj->nns_db_execute( $str_sql );
			
			foreach ($videos as $video_id){
				$this->clear_memcache_by_asset_service($video_id);
			}
			
		}
		
		return $result;
		
	 }

	/**
	 * 根据直播频道的注入id查询影片id
	 * @param string $str_import_id
	 * @param mixed $str_cp_id
	 * @return array|bool
	 */
	public function get_live_id_by_import_id($str_import_id, $str_cp_id = 0,$import_source='')
	{
		$str_sql = "select nns_id from nns_live WHERE
						nns_import_id = '{$str_import_id}' and nns_deleted != 1 and nns_cp_id = '{$str_cp_id}'";
		if(strlen($import_source) >0)
		{
		    $str_sql.=" and nns_import_source='{$import_source}' ";
		}
		$result = $this->db_obj->nns_db_query($str_sql);
		if (is_array($result['data'])) {
			if (count($result['data']) > 0) {
				return $result['data'][0]['nns_id'];
			} else {
				return FALSE;
			}
		}
		return FALSE;
	}



}

?>