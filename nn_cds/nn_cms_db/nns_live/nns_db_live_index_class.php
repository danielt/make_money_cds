<?php
$nns_db_live_index_dir = dirname ( dirname ( __FILE__ ) );
require_once $nns_db_live_index_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
require_once $nns_db_live_index_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";

class nns_db_live_index_class {
	private $conn;
	private $db_obj;
	
	public function __construct() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
	}
	
	//索引统计
	public function nns_db_live_index_count($live_id = null, $name = null, $order = null) {
		$result = array ();
		$str_sql = "select count(1) as num from nns_live_index ";
		$where = "";
		//live_id
		if (isset ( $live_id )) {
			if (! empty ( $live_id )) {
				if (empty ( $where )) {
					$where .= " where nns_live_id='$live_id'";
				} else {
					$where .= " or nns_live_id='$live_id'";
				}
			}
		}
		//name
		if (isset ( $name )) {
			if (! empty ( $name )) {
				if (empty ( $where )) {
					$where .= " where nns_name like '%$name%'";
				} else {
					$where .= " or nns_name like '%$name%'";
				}
			}
		}
		//order
		if (isset ( $order )) {
			if (empty ( $order )) {
				$where .= " order by nns_modify_time desc";
			} else {
				$where .= " order by $order desc";
			}
		}
		$str_sql .= $where;
		$result = $this->db_obj->nns_db_query ( $str_sql );
		return $result;
	}
	//索引列表
	public function nns_db_live_index_list($live_id = null, $name = null, $order = null, $min = 0, $num = 0) {
		$result = array ();
		$str_sql = "select * from nns_live_index ";
		$where = "";
		//live_id
		if (isset ( $live_id )) {
			if (! empty ( $live_id )) {
				if (empty ( $where )) {
					$where .= " where nns_live_id='$live_id'";
				} else {
					$where .= " and nns_live_id='$live_id'";
				}
			}
		}
		//live_id
		if (isset ( $name )) {
			if (! empty ( $name )) {
				if (empty ( $where )) {
					$where .= " where nns_name like '%$name%'";
				} else {
					$where .= " and nns_name like '%$name%'";
				}
			}
		}
		if (isset ( $order )) {
			if (empty ( $order )) {
				$where .= " order by nns_modify_time desc";
			} else {
				$where .= " order by $order desc";
			}
		}
		if (isset ( $min ) && isset ( $num )) {
			if (! empty ( $num )) {
				$min = ( int ) $min;
                $num = ( int ) $num;
				$where .= " limit $min,$num";
			}
		}
		$str_sql .= $where;
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_LIVE_INDEX_NOT_FOUND, 'live index data not find' );
			}
		}
		return $result;
	}
	//索引详细信息
	public function nns_db_live_index_info($live_id, $index) {
		$result = array ();
		//live_id
		if (! isset ( $live_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index info param error' );
			return $result;
		}
		if (empty ( $live_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index info param error' );
			return $result;
		}
		//index
		if (! isset ( $index )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index info param error' );
			return $result;
		}
		if ($index === null || $index === '') {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index info param error' );
			return $result;
		}
		$index = ( int ) $index;
		
		$str_sql = "select * from nns_live_index where nns_live_id='$live_id' and nns_index=$index ";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_LIVE_INDEX_NO_DATA, 'live index data not find' );
			}
		}
		return $result;
	
	}
	//索引详细信息
	public function nns_db_live_index_info_by_indexid($index_id){
	    $result = array ();
		//live_id
		if (! isset ( $index_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index info param error' );
			return $result;
		}
		if (empty ( $index_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index info param error' );
			return $result;
		}
		
		$str_sql = "select * from nns_live_index where nns_id='$index_id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_LIVE_INDEX_NO_DATA, 'live index data not find' );
			}
		}
		return $result;
		
	}
	
	//修改index 简介
	public function nns_db_live_index_modify($live_id, $index, $name = null, $time_len = null, $image=null,$summary = null) {
		$result = array ();
		$where = "";
		$name=str_replace("'"," ",$name);
		$summary=str_replace("'"," ",$summary);
		if (! isset ( $live_id ) || ! isset ( $index )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index modify param error' );
			return $result;
		}
		if (empty ( $live_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index modify param error' );
			return $result;
		}
		if ($index === null && $index === '') {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'live index modify param error' );
			return $result;
		}
		
		$index = ( int ) $index;
		$time_len = ( int ) $time_len;
		
		//index exist
		$str_sql = "select 1 from nns_live_index where nns_live_id='$live_id' and nns_index=$index";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows <= 0) {
			//insert into index
			//生成live index GUID
			$guid = new Guid ();
			$index_id = $guid->toString ();
			$str_sql = "insert into nns_live_index(nns_id,nns_live_id,nns_index,nns_name,nns_time_len, nns_image,nns_summary,nns_create_time,nns_modify_time)values('$index_id','$live_id',$index,'$name',$time_len,'$image','$summary',now(),now())";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
		} else {
			if (!empty($image)){
				$img_str=",nns_image='$image'";
			}
			//update
			$str_sql = "update nns_live_index set nns_name='$name',nns_time_len=$time_len ,nns_summary='$summary',nns_modify_time=now()".$img_str." where nns_live_id='$live_id' and nns_index=$index";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
		}
		
		return $result;
	}

}

?>