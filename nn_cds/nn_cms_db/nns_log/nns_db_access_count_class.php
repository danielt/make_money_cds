<?php
/*
 * Created on 2012-6-30
 *
 $access_count  = array(
	 'nns_template_name'=>'',
	 'nns_template_type'=>'',
	 'nns_template_id'=>'',
	 'nns_access_type'=>'',//0:5分钟 1：1小时 2：1天
	 'nns_access_day'=>'',
	 'nns_access_count'=>'',
	 'nns_access_index'=>'',
 );
 */
$nns_db_dir = dirname(dirname(__FILE__));
$nns_cf_dir = dirname($nns_db_dir);
require_once $nns_db_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
require_once $nns_db_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";
require_once $nns_cf_dir. DIRECTORY_SEPARATOR ."nn_cms_config".DIRECTORY_SEPARATOR. "nn_cms_stat_config.php";

class nns_db_access_count_class {
	private $log_file;
	function __construct() {
		$this->db_obj = new nns_db_class(NN_STAT_DB_HOST, NN_STAT_DB_USER, NN_STAT_DB_PWD, NN_STAT_DB_NAME);
		$this->conn = $this->db_obj->nns_db_connect();
		$this->log_file =  dirname(dirname(dirname(__FILE__))).'/data/log/debug_sql.log';
	}
	function add_count($acess_count){
		//
		$nns_access_day = date('Y-m-d');
		$nns_access_day_1 = $nns_access_day." 00:00:00";

		$nns_access_index = floor((time()-strtotime($nns_access_day_1))/(5*60));
		//0:5分钟
		if($this->access_type_exists($nns_access_day,0,$nns_access_index,$acess_count)){
			//update
			$this->_update_count($nns_access_day,0,$nns_access_index,$acess_count);
		}else{
			//insert
			$guid = new Guid ();
			$acess_count['nns_id'] = $guid->toString ();
			$acess_count['nns_access_type'] = 0;
			$acess_count['nns_access_count'] = 1;
			$acess_count['nns_access_day'] = $nns_access_day;
			$acess_count['nns_access_index'] = $nns_access_index;
			$this->_insert_count($acess_count);
		}
		//1：1小时
		$nns_access_index = floor((time()-strtotime($nns_access_day_1))/(60*60));
		if($this->access_type_exists($nns_access_day,1,$nns_access_index,$acess_count)){
			//update
			$this->_update_count($nns_access_day,1,$nns_access_index,$acess_count);
		}else{
			//insert
			$guid = new Guid ();
			$acess_count['nns_id'] = $guid->toString ();
			$acess_count['nns_access_type'] = 1;
			$acess_count['nns_access_count'] = 1;
			$acess_count['nns_access_day'] = $nns_access_day;
			$acess_count['nns_access_index'] = $nns_access_index;
			$this->_insert_count($acess_count);
		}
		//2：1天
		$nns_access_index = floor((time()-strtotime($nns_access_day_1))/(24*60*60));
		if($this->access_type_exists($nns_access_day,2,$nns_access_index,$acess_count)){
			//update
			$this->_update_count($nns_access_day,2,$nns_access_index,$acess_count);
		}else{
			//insert
			$guid = new Guid ();
			$acess_count['nns_id'] = $guid->toString ();
			$acess_count['nns_access_type'] = 2;
			$acess_count['nns_access_count'] = 1;
			$acess_count['nns_access_day'] = $nns_access_day;
			$acess_count['nns_access_index'] = $nns_access_index;
			$this->_insert_count($acess_count);
		}
	}
	function _insert_count($acess_count){
		$sql = "insert into nns_access_count(nns_id," .
			"nns_template_name," .
			"nns_template_type," .
			"nns_template_id," .
			"nns_access_type," .
			"nns_access_day," .
			"nns_access_count," .
			"nns_access_index" .
			") values('".
			$acess_count['nns_id']."','" .
			$acess_count['nns_template_name']."','".
			$acess_count['nns_template_type']."','".
			$acess_count['nns_template_id']."'," .
			$acess_count['nns_access_type'].",'" .
			$acess_count['nns_access_day']."'," .
			$acess_count['nns_access_count']."," .
			$acess_count['nns_access_index'].")";
		$this->db_obj->nns_db_execute($sql);
	}
	function _update_count($nns_access_day,$nns_access_type,$nns_access_index,$acess_count){
		$sql = "update nns_access_count set nns_access_count=nns_access_count+1 " .
			"where nns_access_day='".$nns_access_day .
			"' and nns_access_type='".$nns_access_type .
			"' and nns_access_index='".$nns_access_index .
			"' and nns_template_type='".$acess_count['nns_template_type'].
			"' and nns_template_id='".$acess_count['nns_template_id'].
			"' and nns_template_name='".$acess_count['nns_template_name']."'";

		$this->db_obj->nns_db_execute($sql);
	}

	function access_type_exists($nns_access_day , $nns_access_type,$nns_access_index,$acess_count){
		$sql = "select count(*) as num from nns_access_count " .
			"where nns_access_day ='".$nns_access_day.
			"' and nns_access_type ='".$nns_access_type.
			"' and nns_access_index ='".$nns_access_index.
			"' and nns_template_type ='".$acess_count['nns_template_type'].
			"' and nns_template_id ='".$acess_count['nns_template_id'].
			"' and nns_template_name='".$acess_count['nns_template_name']."'";

		$result = $this->db_obj->nns_db_query($sql);
		if(isset($result['data'][0]['num']) && $result['data'][0]['num'] >0){
			return true;
		}
		return false;
	}
	function get_access_count_5()
	{
		$now = time();
		$nns_access_day = date('Y-m-d', $now);
		$access_index = $this->_get_access_index(time(), 5*60);
		$sql = "select nns_template_type,nns_template_id,nns_template_name ,nns_access_count  from nns_access_count where nns_access_day='$nns_access_day' and nns_access_type=0 and nns_access_index=$access_index";
		//error_log($sql."\r\n",3,$this->log_file);
		$result = $this->db_obj->nns_db_query($sql);
		return isset($result['data'])?$result['data']:null;
	}
	function get_report_data_list($filter){
		$where = '';
		if(!empty($filter['nns_template_type'])){
			$where.= " and nns_template_type='".$filter['nns_template_type']."'";
		}
		if(!empty($filter['nns_template_id'])){
			$where.= " and nns_template_id='".$filter['nns_template_id']."'";
		}
		if(!empty($filter['nns_template_name'])){
			$where.= " and nns_template_name='".$filter['nns_template_name']."'";
		}
		$t_space = ceil(($filter['end_time']-$filter['start_time'])/$filter['x_size']);//时间间隔

		//error_log('t_space='.$tt."\r\n",3,"sql.txt");
		$access_index_mod = null;
		$access_type =0;
		$space_days = ceil(($filter['end_time']-$filter['start_time'])/(24*60*60));

		//error_log($space_days."\r\n",3,$this->log_file);
		if($space_days >= 3){
			$access_type=2;
			$access_index_mod = 24*60*60;

			if($space_days < $filter['x_size']){
				$filter['x_size'] = $space_days;   // x	取值范围：(3,$filter['x_size']);
				$t_space = 24*60*60;
			}
		}else{

			if($t_space <= 5*60){ //
				//
				$access_type=0;
				$access_index_mod = 5*60;
			}else if($t_space >= 24*60*60) {
				$access_type=2;
				$access_index_mod = 24*60*60;
			}else{
				$access_type=1;
				$access_index_mod = 60*60;
			}
		}
		$data_list = array();
		$y_max =0;
		$num = ceil($t_space/$access_index_mod);

		for($i=0 ;  $i<= $filter['x_size']; $i++){

			$tm_t = $filter['start_time']+$i*$t_space;
			$data_item['x_field'] = date('Y-m-d H:i:s',$tm_t);
			//	 error_log('x_field='.$data_item['x_field']."\r\n",3,"sql.txt");
			$total_num = 0;

			//		 error_log(date('Y-m-d H:i:s',$tm_t)."\r\n",3,"sql.txt");
			for($j=0;$j<$num;$j++){


				$tm_time = $tm_t - $access_index_mod*$j;
				$access_index = $this->_get_access_index($tm_time, $access_index_mod);
				$nns_access_day = date('Y-m-d', $tm_time);


				$access_index =  max(0,$access_index);
				$sql = "select nns_access_count as count from nns_access_count where nns_access_day='$nns_access_day' and nns_access_type=$access_type and nns_access_index=$access_index".$where;

				$result = $this->db_obj->nns_db_query($sql);

				$count = isset($result['data'][0]['count'])?$result['data'][0]['count']:0;
				//error_log('count='.$count.'=>sql='.$sql."\r\n",3,$this->log_file);
				$total_num += $count;

				//$access_index--;
			}
			// error_log('total_num='.$total_num."\r\n",3,"sql.txt");
			$data_item['y_field'] = $total_num;
			$y_max = max($y_max,$total_num);

			$data_list[] = $data_item;

		}
		if($y_max<100) $y_max =100;
		$y_degree = floor($y_max/$filter['x_size']);
		if($y_degree<10) $y_degree =10;
		return $this->__format_data($data_list,$y_max,$y_degree);
	}
	function _get_access_index($s_time, $access_index_mod){
		$nns_access_day = date('Y-m-d',$s_time);
		$nns_access_day = $nns_access_day." 00:00:00";
		return floor(($s_time-strtotime($nns_access_day))/$access_index_mod);;
	}
	function __format_data($data_list,$y_max,$y_degree){
		/*
		 *
		 <data x_name="时间" y_name="在线人数" chat_name="在线人数图表" y_max="100" y_min="0" y_degree="10" >
		 <item x_field="06-30 18:59" y_field="48" /><item x_field="06-30 19:47" y_field="47" />
		 <item x_field="06-30 20:35" y_field="60" /><item x_field="06-30 21:23" y_field="53" />
		 <item x_field="06-30 22:11" y_field="46" /><item x_field="06-30 22:59" y_field="34" />
		 <item x_field="06-30 23:47" y_field="28" /><item x_field="07-01 00:35" y_field="18" />
		 <item x_field="07-01 17:23" y_field="23" /><item x_field="07-01 18:11" y_field="44" /></data>
		 *
		 * */
		$data_format = '<data x_name="'.cms_get_lang('time').'" y_name="'.cms_get_lang('visiter').'" chat_name="'.cms_get_lang('visiter_chart').'" y_max="'.$y_max.'" y_min="0" y_degree="'.$y_degree.'" >';
		if(!empty($data_list)){
			foreach($data_list as $item){
				$data_format .= '<item x_field="'.$item['x_field'].'" y_field="'.$item['y_field'].'" />';
			}
		}
		$data_format .= '</data>';
		return $data_format;
	}
}
