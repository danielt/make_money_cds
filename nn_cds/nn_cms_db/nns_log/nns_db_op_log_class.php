<?php
$nns_db_log_dir = dirname(dirname(__FILE__));
require_once $nns_db_log_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";
require_once $nns_db_log_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
//xyl 2013-07-12 17:45:57 导出excel
require_once $nns_db_log_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_excel_class.php";
require_once $nns_db_log_dir.DIRECTORY_SEPARATOR.'nns_debug_log'.DIRECTORY_SEPARATOR.'nns_debug_log_class.php';

class nns_db_op_log_class {
	private $conn;
	private $db_obj;

	public function __construct() {
		$this->db_obj = new nns_db_class(g_db_host, g_db_username, g_db_password, g_db_name);
		$this->conn = $this->db_obj->nns_db_connect();
	}

	/**
	 *
	 * 管理员统计总数 ...
	 * @param string $name
	 * @param string $partner_id
	 * @param string $role_id
	 */
	public function nns_db_op_log_count($op_admin_id = null, $org_type = null, $org_id = null, $op_type = null, $begin_time = null, $end_time = null,$nns_op_desc = null) {
		$begin_time=nns_db_validation_class::nns_db_validation_replace_date($begin_time);
		$end_time=nns_db_validation_class::nns_db_validation_replace_date($end_time);

		$str_sql = "select count(1) as num from nns_op_log ";
		$str_sql .= $this->nns_db_op_log_where($op_admin_id, $org_type, $org_id, $op_type, $begin_time, $end_time, $nns_op_desc);
		//result
		$result = array ();
		$result = $this->db_obj->nns_db_query($str_sql);

		return $result;

	}
	/**
	 *
	 * 管理员列表（模糊查询） ...
	 * @param string $name
	 * @param string $org_id
	 * @param string $role_id
	 * @param int $type
	 * @param string $order 排序字段
	 * @param int $min  每页开始值
	 * @param int $num  返回每页数
	 */
	public function nns_db_op_log_list($op_admin_id = null, $org_type = null, $org_id = null, $op_type = null, $begin_time = null, $end_time = null,$nns_op_desc = null,$min=null,$num=null) {
		$begin_time=nns_db_validation_class::nns_db_validation_replace_date($begin_time);
		$end_time=nns_db_validation_class::nns_db_validation_replace_date($end_time);

		$str_sql = " select * from nns_op_log ";
		$str_sql .= $this->nns_db_op_log_where($op_admin_id, $org_type, $org_id, $op_type, $begin_time, $end_time, $nns_op_desc,$min,$num);
		//result
		$result = array ();
		$result = $this->db_obj->nns_db_query($str_sql);
		if ($result["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result(NNS_ERROR_log_NO_DATA, 'log data not find');
			}
		}
		return $result;

	}

	/**
	 *
	 * 添加log ...
	 * @param string $id
	 * @param string $name
	 * @param string $password
	 * @param string $partner_id
	 * @param string $role_id
	 * @param int $state
	 * @return array
	 */
	public function nns_db_op_log_add($op_admin_id = null, $op_type = null, $op_desc = null, $org_type = null, $org_id = null) {
		$result = array ();

		//生成GUID
		$guid = new Guid();
		$id = $guid->toString();
		$org_type=(int)$org_type;
		//insert
		$str_sql = "insert into nns_op_log(nns_id,nns_op_admin_id,nns_op_type,nns_op_time,nns_op_desc,nns_org_type,nns_org_id)values('$id','$op_admin_id','$op_type',now(),'$op_desc',$org_type,'$org_id')";
		$result = $this->db_obj->nns_db_execute($str_sql);

		return $result;

	}
	//	批量查询影片
	public function nns_db_get_video_name_arr($video_ids,$video_type=0){
		if ($video_type==0){
			$table_name="nns_vod";
		}elseif($video_type==1){
			$table_name="nns_live";
		}
		$video_ids=rtrim($video_ids,',');
		$video_id_arr=explode(',',$video_ids);
		$sql_str="select nns_name,nns_id from $table_name ";
		$where="";
		foreach ($video_id_arr as $id_item){
			if (empty($where)){
				$where.="where nns_id='$id_item' ";
			}else{
				$where.="or nns_id='$id_item' ";
			}
		}
		$sql_str.=$where;
		$result = array ();
		$return_desc="";
		$result = $this->db_obj->nns_db_query($sql_str);
		if ($result["ret"]==0){
			foreach($result["data"] as $result_item){
				$name_str=$result_item["nns_name"];
				if (empty($return_desc)){
					$return_desc=$name_str;
				}else{
					$return_desc.=",".$name_str;
				}
			}
		}
		return $return_desc;
	}
	
	//	批量查询影片
	public function nns_db_get_video_info($video_ids,$video_type=0)
	{
	    $table_name = ($video_type==0) ? 'nns_vod' : 'nns_live';
	    $video_ids=explode(',',rtrim($video_ids,','));
	    $sql_str="select * from {$table_name} where nns_in('".implode("','", $video_ids)."') ";
	    $result = $this->db_obj->nns_db_query($sql_str);
	    return isset($result["data"]) ? $result["data"] : null;
	}
	//	批量查询媒资包内容
	public function nns_db_get_assest_video_name_arr($assest_video_ids,$method="id",$filed='nns_video_name'){
		$assest_video_ids=rtrim($assest_video_ids,',');
		$assest_video_id_arr=explode(',',$assest_video_ids);
		if ($method=="id"){
			$method_id="nns_id";
		}elseif($method=="video"){
			$method_id="nns_video_id";
		}
		$sql_str="select  ".$filed." from nns_assists_item ";
		$where="";
		foreach ($assest_video_id_arr as $id_item){
			if (empty($where)){
				$where.="where $method_id='$id_item' ";
			}else{
				$where.="or $method_id='$id_item' ";
			}
		}
		$sql_str.=$where;
		$sql_str.=' group by '.$method_id;
		$result = array ();
		$return_desc="";
		$result = $this->db_obj->nns_db_query($sql_str);
		if ($result["ret"]==0){
			foreach($result["data"] as $result_item){
				$name_str=$result_item[$filed];
				if ($return_desc==''){
					$return_desc=$name_str;
				}else{
					$return_desc.=",".$name_str;
				}
			}
		}
		return $return_desc;
	}
	//	批量查询服务包内容
	public function nns_db_get_service_name_arr($service_ids,$method="id",$files='nns_name'){
		$service_ids=rtrim($service_ids,',');
		$service_id_arr=explode(',',$service_ids);
		if ($method=="id"){
			$method_id="nns_id";
		}elseif($method=="video"){
			$method_id="nns_video_id";
		}
		$sql_str="select ".$files." from nns_service_item ";
		$where="";
		foreach ($service_id_arr as $id_item){
			if (empty($where)){
				$where.="where ".$method_id."='$id_item' ";
			}else{
				$where.="or ".$method_id."='$id_item' ";
			}
		}
		$sql_str.=$where;
		$result = array ();
		$return_desc="";
		$result = $this->db_obj->nns_db_query($sql_str);
		if ($result["ret"]==0){
			foreach($result["data"] as $result_item){
				$name_str=$result_item[$files];
				if (empty($return_desc)){
					$return_desc=$name_str;
				}else{
					$return_desc.=",".$name_str;
				}
			}
		}
		return $return_desc;
	}
	//	批量查询媒体文件
	public function nns_db_get_media_name_arr($media_ids,$video_type=0){
		if ($video_type==0){
			$table_name="nns_vod_media";
		}elseif($video_type==1){
			$table_name="nns_live_media";
		}
		$media_ids=rtrim($media_ids,',');
		$media_id_arr=explode(',',$media_ids);
		$sql_str="select nns_name,nns_id,nns_content_id from $table_name ";
		$where="";
		foreach ($media_id_arr as $id_item){
			if (empty($where)){
				$where.="where nns_id='$id_item' ";
			}else{
				$where.="or nns_id='$id_item' ";
			}
		}
		$sql_str.=$where;
		$result = array ();
		$return_desc="";
		$result = $this->db_obj->nns_db_query($sql_str);
		if ($result["ret"]==0){
			foreach($result["data"] as $result_item){
				$name_str=$result_item["nns_content_id"];
				if (empty($return_desc)){
					$return_desc=$name_str;
				}else{
					$return_desc.=",".$name_str;
				}
			}
		}
		return $return_desc;
	}
	//	批量查询角色
	public function nns_db_get_role_name_arr($role_ids){
		$role_ids=rtrim($role_ids,',');
		$role_id_arr=explode(',',$role_ids);
		$sql_str="select nns_name,nns_id from nns_role ";
		$where="";
		foreach ($role_id_arr as $id_item){
			if (empty($where)){
				$where.="where nns_id='$id_item' ";
			}else{
				$where.="or nns_id='$id_item' ";
			}
		}
		$sql_str.=$where;
		$result = array ();
		$return_desc="";
		$result = $this->db_obj->nns_db_query($sql_str);
		if ($result["ret"]==0){
			foreach($result["data"] as $result_item){
				$name_str=$result_item["nns_name"];
				if (empty($return_desc)){
					$return_desc=$name_str;
				}else{
					$return_desc.=",".$name_str;
				}
			}
		}
		return $return_desc;
	}
	//	批量查询合作伙伴
	public function nns_db_get_partner_name_arr($partner_ids){
		$partner_ids=rtrim($partner_ids,',');
		$partner_id_arr=explode(',',$partner_ids);
		$sql_str="select nns_name,nns_id from nns_partner ";
		$where="";
		foreach ($partner_id_arr as $id_item){
			if (empty($where)){
				$where.="where nns_id='$id_item' ";
			}else{
				$where.="or nns_id='$id_item' ";
			}
		}
		$sql_str.=$where;
		$result = array ();
		$return_desc="";
		$result = $this->db_obj->nns_db_query($sql_str);
		if ($result["ret"]==0){
			foreach($result["data"] as $result_item){
				$name_str=$result_item["nns_name"];
				if (empty($return_desc)){
					$return_desc=$name_str;
				}else{
					$return_desc.=",".$name_str;
				}
			}
		}
		return $return_desc;
	}
	//	批量查询集团
	public function nns_db_get_group_name_arr($group_ids){
		$group_ids=rtrim($group_ids,',');
		$group_id_arr=explode(',',$group_ids);
		$sql_str="select nns_name,nns_id from nns_group ";
		$where="";
		foreach ($group_id_arr as $id_item){
			if (empty($where)){
				$where.="where nns_id='$id_item' ";
			}else{
				$where.="or nns_id='$id_item' ";
			}
		}
		$sql_str.=$where;
		$result = array ();
		$return_desc="";
		$result = $this->db_obj->nns_db_query($sql_str);
		if ($result["ret"]==0){
			foreach($result["data"] as $result_item){
				$name_str=$result_item["nns_name"];
				if (empty($return_desc)){
					$return_desc=$name_str;
				}else{
					$return_desc.=",".$name_str;
				}
			}
		}
		return $return_desc;
	}
	/**
	 *
	 * 构造sql ...
	 * @param string $name
	 * @param string $partner_id
	 * @param string $role_id
	 * @param string $order
	 * @param int $min
	 * @param int $num
	 */
	public function nns_db_op_log_where($op_admin_id = null, $org_type = null, $org_id = null, $op_type = null, $begin_time = null, $end_time = null,$nns_op_desc = null,$min=null,$num=null) {
		//组合sql
		$where = "";

		if (isset ($op_admin_id)) {
			if (!empty ($op_admin_id)) {
				if (empty ($where)) {
					$where .= " where nns_op_admin_id = '$op_admin_id' ";
				} else {
					$where .= " and nns_op_admin_id = '$op_admin_id' ";
				}
			}
		}
		if (isset ($org_type)) {
			if (!empty ($org_type)) {
				if (empty ($where)) {
					$where .= " where nns_org_type=$org_type";
				} else {
					$where .= " and nns_org_type=$org_type";
				}

			}
		}
		if (isset ($org_id)) {
			if (!empty ($org_id)) {
				if (empty ($where)) {
					$where .= " where nns_org_id='$org_id'";
				} else {
					$where .= " and nns_org_id='$org_id'";
				}

			}
		}
		if (isset ($op_type)) {
			if (!empty ($op_type)) {
				if (empty ($where)) {
					$where .= " where nns_op_type='$op_type'";
				} else {
					$where .= " and nns_op_type='$op_type'";
				}

			}
		}
		if (!empty ($begin_time) && !empty ($end_time)) {
			$end_time=strtotime($end_time."+ 1 days");
			$end_time=date('Y-m-d H:i:s',$end_time);

			if (empty ($where)) {
				$where .= " where nns_op_time >= '$begin_time' and nns_op_time<='$end_time'";
			} else {
				$where .= " and nns_op_time >= '$begin_time' and nns_op_time<='$end_time'";
			}
		}
		if(!empty($nns_op_desc)){
			if (empty ($where)) {
				$where .= " where nns_op_desc like '%$nns_op_desc%'";
			} else {
				$where .= " and nns_op_desc like '%$nns_op_desc%'";
			}
		}
		$where .= " order by nns_op_time desc";
		if (isset ( $min ) && isset ( $num )) {
			if (! empty ( $num )) {
				$min = ( int ) $min;
				$num = ( int ) $num;
				$where .= " limit $min,$num";
			}
		}


		return $where;
	}




	/*
	 * 导出操作日志数据为excel
	 * 2013-07-12 17:44:31
	 * @param:		$op = opera_log 页面跳转
	 * @param:		$data_list	数组
	 * @param:		$count		统计
	 * @param:		$params		参数
	 * */
	public function nns_db_op_log_export_excel($data_list,$count,$params){
		//组装成数组
		$num = 0;

		if ($params['begin_time'] && $params['end_time']) {
			$nns_date = $params['begin_time'].'-'.$params['end_time'];
		}
		$arr = array(
			array(cms_get_lang('total'),$count,cms_get_lang('date'),$nns_date),
			array(cms_get_lang('segnumber'),cms_get_lang('manager_id'),cms_get_lang('log_type'),cms_get_lang('log_detail'),cms_get_lang('log_time'))
		);
		if (!empty($data_list)) {
			foreach ($data_list as $k1=>$v1) {
				$num++;
				$arr[] = array($num,$v1['nns_op_admin_id'],$v1['nns_op_type'],$v1['nns_op_desc'],$v1['nns_op_time']);
			}
		}
		//引入excel.class.php
		$xls = new Excel_XML('UTF-8',false,cms_get_lang('count'));
		$xls->addArray($arr);
		$xls->generateXML('op_log');
		die;
		
	}

}
?>