<?php
$nns_db_log_dir = dirname ( dirname ( __FILE__ ) );
require_once $nns_db_log_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
require_once $nns_db_log_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";
//xyl 2013-05-28 10:50:05
require_once $nns_db_log_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_excel_class.php";
require_once $nns_db_log_dir.DIRECTORY_SEPARATOR.'nns_debug_log'.DIRECTORY_SEPARATOR.'nns_debug_log_class.php';
class nns_db_play_log_class {
	private $conn;
	private $db_obj;
	// private $log_file;
	private $log;
	public function __construct() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
		$this->log =   new nns_debug_log_class();
		$this->log->setlogpath('play_log');

	}


	/**
	 *
	 * 管理员统计总数 ...
	 * @param string $name
	 * @param string $partner_id
	 * @param string $role_id
	 */
	public function nns_db_play_log_count($filter=null ) {

		$str_sql = "select count(1) as num from nns_play_log ";
		$str_sql .= $this->nns_db_play_log_where ($filter);

		//result
		$result = array ();
		$result = $this->db_obj->nns_db_query ( $str_sql );

		return isset($result['data'][0]['num'])?$result['data'][0]['num']:0;

	}
	/**
	 * 5分钟实时统计
	 */
	public function get_play_log_count_5()
	{
		$t_5 = date("Y-m-d H:i:s",time()-5*60);
		$str_sql = "select nns_video_id,nns_video_name, count(*) as total_play_count from nns_play_log where nns_end_position >=  '".$t_5."' group by nns_video_id";

		//error_log($str_sql."\r\n",3,$this->log_file );
		$result = $this->db_obj->nns_db_query ( $str_sql );

		return !empty($result['data'])?$result['data']:null;
	}
	/**
	 *
	 */
	public function nns_db_play_log_list($filter,$start=0,$limit=10) {

		// xyl 2013-05-27 20:13:37 增加播放时间差,总时间统计
		//$str_sql = " select * from nns_play_log ";
		$str_sql = " select *,(UNIX_TIMESTAMP( nns_end_position ) - UNIX_TIMESTAMP( nns_begin_position )) as nns_time_length from nns_play_log ";
		$str_sql .= $this->nns_db_play_log_where ($filter);

		$str_sql .=" limit $start,$limit";

		$this->log->write($str_sql);

		$result = array ();
		$result = $this->db_obj->nns_db_query ( $str_sql );

		return !empty($result['data'])?$result['data']:null;

	}
	/*
	 * 总播放时长
	 * xyl 2013-05-27 20:36:06
	 */
	public function nns_db_play_log_time_count($filter){
		$str_sql = "select sum(UNIX_TIMESTAMP( nns_end_position ) - UNIX_TIMESTAMP( nns_begin_position )) as nns_time_length_sum from nns_play_log ";
		$str_sql .= $this->nns_db_play_log_where ($filter);
		$this->log->write($str_sql);
		$result = array ();
		$result = $this->db_obj->nns_db_query ( $str_sql );
		return !empty($result['data'])?$result['data']:null;
	}


	public function nns_db_play_log_delete($delete_time){
		$str_sql = " delete from nns_play_log where nns_begin_position < '$delete_time' ";
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		return $result;
	}
	public function add_play_log($play_log){

		$guid = new Guid();
		$id = $guid->toString();

		//insert
		$str_sql = "insert into nns_play_log(" .
			"nns_id," .
			"nns_user_id," .
			"nns_product_id," .
			"nns_video_name," .
			"nns_video_id," .
			"nns_video_type," .
			"nns_begin_position," .
			"nns_end_position," .
			"nns_service_asset_id," .
			"nns_media_asset_id," .
			"nns_device_id," .
			"nns_user_agent," .
			"nns_ua_ips," .
			"nns_qam_area_code," .
			"nns_qam_frequence," .
			"nns_qam_ips," .
			"nns_service_type," .
			"nns_nmds_id," .
			"nns_nmds_ips" .
			") values" .
			"('$id'," .
			"'".$play_log['nns_user_id']."'," .
			"'".$play_log['nns_product_id']."'," .
			"'".$play_log['nns_video_name']."'," .
			"'".$play_log['nns_video_id']."'," .
			"'".$play_log['nns_video_type']."'," .
			"'".$play_log['nns_begin_position']."'," .
			"'".$play_log['nns_end_position']."'," .
			"'".$play_log['nns_service_asset_id']."'," .
			"'".$play_log['nns_media_asset_id']."'," .
			"'".$play_log['nns_device_id']."'," .
			"'".$play_log['nns_user_agent']."'," .
			"'".$play_log['nns_ua_ips']."'," .
			"'".$play_log['nns_qam_area_code']."'," .
			"'".$play_log['nns_qam_frequence']."'," .
			"'".$play_log['nns_qam_ips']."'," .
			"'".$play_log['nns_service_type']."'," .
			"'".$play_log['nns_nmds_id']."'," .
			"'".$play_log['nns_nmds_ips']."')";

		//        $this->log->write($str_sql);
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		$result["sql"]=$str_sql;
		return $result;
	}

	/**
	 *
	 * 添加log ...
	 * @param string $id
	 * @param string $name
	 * @param string $password
	 * @param string $partner_id
	 * @param string $role_id
	 * @param int $state
	 * @return array
	 */
	public function nns_db_play_log_add($product_id = null, $user_id = null, $video_id = null,
		$video_type = null,$begin_time=null,$end_time=null,$service_asset_id=null,$media_ass4et_id=null,
		$video_name=null,$device_id=null,$user_agent=null,$ua_ips=null,$qam_area_code=null,$qam_frequence=null,
		$qam_ips=null, $service_type=null) {
			$result = array ();


			$video_type = ( int ) $video_type;
			$guid = new Guid();
			$id = $guid->toString();
			if (empty($begin_time)){
				$begin_time="null";
			}else{
				$begin_time="'$begin_time'";
			}
			if (empty($end_time)){
				$end_time="null";
			}else{
				$end_time="'$end_time'";
			}
			//insert
			$str_sql = "insert into nns_play_log(nns_id,nns_user_id,nns_product_id,nns_video_name,nns_video_id," .
				"nns_video_type,nns_begin_position,nns_end_position,nns_service_asset_id,nns_media_asset_id," .
				"nns_device_id,nns_user_agent,nns_ua_ips,nns_qam_area_code,nns_qam_frequence,nns_qam_ips,nns_service_type) values" .
				"('$id','$user_id','$product_id','$video_name','$video_id','$video_type',$begin_time,$end_time," .
				"'$service_asset_id','$media_asset_id','$device_id','$user_agent','$ua_ips','$qam_area_code','$qam_frequence'" .
				",'$qam_ips','$service_type')";

			$result = $this->db_obj->nns_db_execute ( $str_sql );
			$result["sql"]=$str_sql;
			return $result;

		}



	/**
	 *
	 * 构造sql ...
	 * @param string $name
	 * @param string $partner_id
	 * @param string $role_id
	 * @param string $order
	 * @param int $min
	 * @param int $num
	 */
	public function nns_db_play_log_where($filter) {
		//组合sql
		$where = "";

		if (isset ( $filter['nns_product_id'] )) {
			if (! empty ( $filter['product_id'] )) {

				if ($filter['nns_product_id'] !== false) {
					if (empty ( $where )) {
						$where .= " where nns_product_id = '".$filter['nns_product_id']."' ";
					} else {
						$where .= " and nns_product_id = '".$filter['nns_product_id']."' ";
					}
				}
			}
		}
		if (isset ( $filter['nns_user_id'] )) {
			if (! empty ( $filter['nns_user_id'] )) {
				if ($filter['nns_user_id'] !== false) {
					if (empty ( $where )) {
						$where .= " where nns_user_id='".$filter['nns_user_id']."'";
					} else {
						$where .= " and nns_user_id='".$filter['nns_user_id']."'";
					}
				}

			}
		}
		if (isset ( $filter['nns_video_id'] )) {
			if (! empty ( $filter['nns_video_id'] )) {
				if ($filter['nns_video_id'] !== false) {
					if (empty ( $where )) {
						$where .= " where nns_video_id='".$filter['nns_video_id'] ."'";
					} else {
						$where .= " and nns_video_id='".$filter['nns_video_id'] ."'";
					}
				}

			}
		}
		if (isset ( $filter['nns_service_asset_id']  )) {
			if (! empty ( $filter['nns_service_asset_id'] )) {
				if ($filter['nns_service_asset_id'] !== false) {
					if (empty ( $where )) {
						$where .= " where nns_service_asset_id='".$filter['nns_service_asset_id'] ."'";
					} else {
						$where .= " and nns_service_asset_id='".$filter['nns_service_asset_id'] ."'";
					}
				}

			}
		}
		if (isset ( $filter['nns_media_asset_id']  )) {
			if (! empty ( $filter['nns_media_asset_id'] )) {
				if ($filter['nns_media_asset_id'] !== false) {
					if (empty ( $where )) {
						$where .= " where nns_media_asset_id='".$filter['nns_media_asset_id']."'";
					} else {
						$where .= " and nns_media_asset_id='".$filter['nns_media_asset_id']."'";
					}
				}

			}
		}

		if (isset ( $filter['nns_video_name'] )) {
			if (! empty ( $filter['nns_video_name'] )) {
				if ($filter['nns_video_name'] !== false) {
					if (empty ( $where )) {
						$where .= " where nns_video_name like '%".$filter['nns_video_name']."%'";
					} else {
						$where .= " and nns_video_name like '%".$filter['nns_video_name']."%'";
					}
				}

			}
		}
		if (isset ( $filter['nns_user_agent'] )) {
			if (! empty ( $filter['nns_user_agent'] )) {
				if ($filter['nns_user_agent'] !== false) {
					if (empty ( $where )) {
						$where .= " where nns_user_agent = '".$filter['nns_user_agent']."'";
					} else {
						$where .= " and nns_user_agent = '".$filter['nns_user_agent']."'";
					}
				}

			}
		}
		if (isset ( $filter['nns_ua_ips'] )) {
			if (! empty ( $filter['nns_ua_ips'] )) {
				if ($filter['nns_ua_ips'] !== false) {
					if (empty ( $where )) {
						$where .= " where nns_ua_ips like '%".$filter['nns_ua_ips']."%'";
					} else {
						$where .= " and nns_ua_ips like '%".$filter['nns_ua_ips']."%'";
					}
				}

			}
		}
		if (isset ( $filter['nns_qam_ips'] )) {
			if (! empty ( $filter['nns_qam_ips'] )) {
				if ($filter['nns_qam_ips'] !== false) {
					if (empty ( $where )) {
						$where .= " where nns_qam_ips like '%".$filter['nns_qam_ips']."%'";
					} else {
						$where .= " and nns_qam_ips like '%".$filter['nns_qam_ips']."%'";
					}
				}

			}
		}
		if (isset ( $filter['nns_nmds_ips'] )) {
			if (! empty ( $filter['nns_nmds_ips'] )) {
				if ($filter['nns_nmds_ips'] !== false) {
					if (empty ( $where )) {
						$where .= " where nns_nmds_ips like '%".$filter['nns_nmds_ips']."%'";
					} else {
						$where .= " and nns_nmds_ips like '%".$filter['nns_nmds_ips']."%'";
					}
				}

			}
		}
		if (isset ( $filter['nns_qam_frequence'] )) {
			if (! empty ( $filter['nns_qam_frequence'] )) {
				if ($filter['nns_qam_frequence'] !== false) {
					if (empty ( $where )) {
						$where .= " where nns_qam_frequence = '".$filter['nns_qam_frequence']."'";
					} else {
						$where .= " and nns_qam_frequence = '".$filter['nns_qam_frequence']."'";
					}
				}

			}
		}
		if (isset ( $filter['nns_video_type'] )) {
			if ( $filter['nns_video_type'] !== null &&  $filter['nns_video_type'] !== '') {
				$video_type = ( int )  $filter['nns_video_type'];
				if (empty ( $where )) {
					$where .= " where nns_video_type=". $filter['nns_video_type'];
				} else {
					$where .= " and nns_video_type=". $filter['nns_video_type'];
				}
			}
		}

		if (isset (  $filter['nns_device_id'] )) {
			if ($filter['nns_device_id'] !== null && $filter['nns_device_id'] !== '') {
				if (empty ( $where )) {
					$where .= " where nns_device_id='".$filter['nns_device_id']."'";
				} else {
					$where .= " and nns_device_id='".$filter['nns_device_id']."'";
				}
			}
		}

		if (isset (  $filter['nns_qam_area_code'] )) {
			if ($filter['nns_qam_area_code'] !== null && $filter['nns_qam_area_code'] !== '') {
				if (empty ( $where )) {
					$where .= " where nns_qam_area_code='".$filter['nns_qam_area_code']."'";
				} else {
					$where .= " and nns_qam_area_code='".$filter['nns_qam_area_code']."'";
				}
			}
		}

		if (! empty ( $filter['nns_begin_position'] ) ) {
			if (empty ( $where )) {
				$where .= " where nns_begin_position >= '". $filter['nns_begin_position']."' ";
			} else {
				$where .= " and nns_begin_position >= '". $filter['nns_begin_position']."' ";
			}
		}
		if (! empty ( $filter['nns_end_position']  )) {
			if (empty ( $where )) {
				$where .= " where nns_begin_position<='".$filter['nns_end_position']."'";
			} else {
				$where .= " and  nns_begin_position<='".$filter['nns_end_position']."'";
			}
		}
		if (! empty ( $filter['play_over_time']  )) {
			if (empty ( $where )) {
				$where .= " where nns_end_position>='".$filter['play_over_time']."'";
			} else {
				$where .= " and  nns_end_position>='".$filter['play_over_time']."'";
			}
		}
		$where.=" order by nns_begin_position desc";
		return $where;
	}
	/**
	 * 播放日志总量统计，按5分钟，1小时，1天
	 *
	 *
	 */
	function add_play_log_count(){

		$nns_access_day = date('Y-m-d');
		$nns_access_day_1 = $nns_access_day." 00:00:00";

		$nns_access_index = floor((time()-strtotime($nns_access_day_1))/(5*60));
		//0:5分钟
		if($this->access_type_exists($nns_access_day,0,$nns_access_index)){
			//update
			$this->_update_count($nns_access_day,0,$nns_access_index);
		}else{
			//insert
			$guid = new Guid ();
			$acess_count['nns_id'] = $guid->toString ();
			$acess_count['nns_access_type'] = 0;
			$acess_count['nns_access_count'] = 1;
			$acess_count['nns_access_day'] = $nns_access_day;
			$acess_count['nns_access_index'] = $nns_access_index;
			$this->_insert_count($acess_count);
		}
		//1：1小时
		$nns_access_index = floor((time()-strtotime($nns_access_day_1))/(60*60));
		if($this->access_type_exists($nns_access_day,1,$nns_access_index)){
			//update
			$this->_update_count($nns_access_day,1,$nns_access_index);
		}else{
			//insert
			$guid = new Guid ();
			$acess_count['nns_id'] = $guid->toString ();
			$acess_count['nns_access_type'] = 1;
			$acess_count['nns_access_count'] = 1;
			$acess_count['nns_access_day'] = $nns_access_day;
			$acess_count['nns_access_index'] = $nns_access_index;
			$this->_insert_count($acess_count);
		}
		//2：1天
		$nns_access_index = floor((time()-strtotime($nns_access_day_1))/(24*60*60));
		if($this->access_type_exists($nns_access_day,2,$nns_access_index)){
			//update
			$this->_update_count($nns_access_day,2,$nns_access_index);
		}else{
			//insert
			$guid = new Guid ();
			$acess_count['nns_id'] = $guid->toString ();
			$acess_count['nns_access_type'] = 2;
			$acess_count['nns_access_count'] = 1;
			$acess_count['nns_access_day'] = $nns_access_day;
			$acess_count['nns_access_index'] = $nns_access_index;
			$this->_insert_count($acess_count);
		}
	}
	function _insert_count($acess_count){
		$sql = "insert into nns_play_log_count(nns_id," .
			"nns_access_type," .
			"nns_access_day," .
			"nns_access_count," .
			"nns_access_index" .
			") values('".
			$acess_count['nns_id']."'," .
			$acess_count['nns_access_type'].",'" .
			$acess_count['nns_access_day']."'," .
			$acess_count['nns_access_count']."," .
			$acess_count['nns_access_index'].
			")";

		//	error_log("_insert_count:".$sql."\r\n",3,$this->log_file );
		$this->db_obj->nns_db_execute($sql);
	}
	function _update_count($nns_access_day,$nns_access_type,$nns_access_index){
		$sql = "update nns_play_log_count set nns_access_count=nns_access_count+1 " .
			"where nns_access_day='".$nns_access_day .
			"' and nns_access_type='".$nns_access_type .
			"' and nns_access_index='".$nns_access_index .
			"'";
		//  error_log("_update_count:".$sql."\r\n",3,$this->log_file );
		$this->db_obj->nns_db_execute($sql);
	}

	function access_type_exists($nns_access_day , $nns_access_type,$nns_access_index){
		$sql = "select count(*) as num from nns_play_log_count " .
			"where nns_access_day ='".$nns_access_day.
			"' and nns_access_type ='".$nns_access_type.
			"' and nns_access_index ='".$nns_access_index.
			"'";
		//  error_log("access_type_exists:".$sql."\r\n",3,$this->log_file );
		$result = $this->db_obj->nns_db_query($sql);
		if(isset($result['data'][0]['num']) && $result['data'][0]['num'] >0){
			return true;
		}
		return false;
	}
	function get_report_data_list($filter){
		$where = '';

		$t_space = ceil(($filter['end_time']-$filter['start_time'])/$filter['x_size']);//时间间隔

		//error_log('t_space='.$tt."\r\n",3,"sql.txt");
		$access_index_mod = null;
		$access_type =0;
		$space_days = ceil(($filter['end_time']-$filter['start_time'])/(24*60*60));

		//error_log($space_days."\r\n",3,"sql.txt");
		if($space_days >= 3){
			$access_type=2;
			$access_index_mod = 24*60*60;

			if($space_days < $filter['x_size']){
				$filter['x_size'] = $space_days;   // x	取值范围：(3,$filter['x_size']);
				$t_space = 24*60*60;
			}
		}else{

			if($t_space <= 5*60){ //
				//
				$access_type=0;
				$access_index_mod = 5*60;
			}else if($t_space >= 24*60*60) {
				$access_type=2;
				$access_index_mod = 24*60*60;
			}else{
				$access_type=1;
				$access_index_mod = 60*60;
			}
		}
		$data_list = array();
		$y_max =0;
		$num = ceil($t_space/$access_index_mod);

		for($i=0 ;  $i<= $filter['x_size']; $i++){

			$tm_t = $filter['start_time']+$i*$t_space;
			$data_item['x_field'] = date('Y-m-d H:i:s',$tm_t);
			//	 error_log('x_field='.$data_item['x_field']."\r\n",3,"sql.txt");
			$total_num = 0;

			//		 error_log(date('Y-m-d H:i:s',$tm_t)."\r\n",3,"sql.txt");
			for($j=0;$j<$num;$j++){


				$tm_time = $tm_t - $access_index_mod*$j;
				$access_index = $this->_get_access_index($tm_time, $access_index_mod);
				$nns_access_day = date('Y-m-d', $tm_time);


				$access_index =  max(0,$access_index);
				$sql = "select nns_access_count as count from nns_play_log_count where nns_access_day='$nns_access_day' and nns_access_type=$access_type and nns_access_index=$access_index".$where;

				$result = $this->db_obj->nns_db_query($sql);

				$count = isset($result['data'][0]['count'])?$result['data'][0]['count']:0;
				// error_log('count='.$count.'=>sql='.$sql."\r\n",3,"debug.log");
				$total_num += $count;

				//$access_index--;
			}
			// error_log('total_num='.$total_num."\r\n",3,"sql.txt");
			$data_item['y_field'] = $total_num;
			$y_max = max($y_max,$total_num);

			$data_list[] = $data_item;

		}
		if($y_max<100) $y_max =100;
		$y_degree = floor($y_max/$filter['x_size']);
		if($y_degree<10) $y_degree =10;
		//print_r($data_list);
		return $this->__format_data($data_list,$y_max,$y_degree);
	}
	function _get_access_index($s_time, $access_index_mod){
		$nns_access_day = date('Y-m-d',$s_time);
		$nns_access_day = $nns_access_day." 00:00:00";
		return floor(($s_time-strtotime($nns_access_day))/$access_index_mod);;
	}
	function __format_data($data_list,$y_max,$y_degree){
		/*
		 *
		 <data x_name="时间" y_name="在线人数" chat_name="在线人数图表" y_max="100" y_min="0" y_degree="10" >
		 <item x_field="06-30 18:59" y_field="48" /><item x_field="06-30 19:47" y_field="47" />
		 <item x_field="06-30 20:35" y_field="60" /><item x_field="06-30 21:23" y_field="53" />
		 <item x_field="06-30 22:11" y_field="46" /><item x_field="06-30 22:59" y_field="34" />
		 <item x_field="06-30 23:47" y_field="28" /><item x_field="07-01 00:35" y_field="18" />
		 <item x_field="07-01 17:23" y_field="23" /><item x_field="07-01 18:11" y_field="44" /></data>
		 *
		 * */
		$data_format = '<data x_name="'.cms_get_lang('time').'" y_name="'.cms_get_lang('visiter').'" chat_name="'.cms_get_lang('visiter_chart').'" y_max="'.$y_max.'" y_min="0" y_degree="'.$y_degree.'" >';
		if(!empty($data_list)){
			foreach($data_list as $item){
				$data_format .= '<item x_field="'.$item['x_field'].'" y_field="'.$item['y_field'].'" />';
			}
		}
		$data_format .= '</data>';
		return $data_format;
	}


	/*
	 * 导出为excel表格
	 * xyl 2013-05-27 16:31:32
	 * $data = nns_db_play_log_list_infinite() 和 nns_db_play_time_count(), nns_db_play_log_count() 的结果集
	 * $data_list1  所有日志的返回的数组
	 * $data_list2 播放的总耗时
	 * $count      播放的总条数
	 * $op = play_log
	 */
	public function nns_db_log_export_excel($data_list,$data_list2,$count){
		//组装成数组
		$num = 0;
		if (!empty($data_list2)) {
			foreach ($data_list2 as $k2=>$v2) {
				$nns_time_length_sum = $v2['nns_time_length_sum'];
			}
		}
		$arr = array(
			array(cms_get_lang('total'),$count,cms_get_lang('total').cms_get_lang('time_played_with_second'),$nns_time_length_sum),
			array(cms_get_lang('segnumber'),cms_get_lang('user_id'),cms_get_lang('zdgl_sbid'),cms_get_lang('stat_vod_name'),cms_get_lang('ipqam_dqm'),cms_get_lang('ipqam_addr'),cms_get_lang('ipqam_pd'),'VSIP',cms_get_lang('stat_bf_begin_time'),cms_get_lang('stat_bf_time_s'),cms_get_lang('zdgl_sbid'),cms_get_lang('stat_fw_ip'))
		);
		if (!empty($data_list)) {
			foreach ($data_list as $k1=>$v1) {
				$num++;
				$arr[] = array($num,$v1['nns_user_id'],$v1['nns_product_id'],$v1['nns_video_name'],$v1['nns_qam_area_code'],$v1['nns_qam_ips'],$v1['nns_qam_frequence'],$v1['nns_nmds_ips'],$v1['nns_begin_position'],$v1['nns_time_length'],$v1['nns_device_id'],$v1['nns_ua_ips']);
			}
		}
		//引入excel.class.php
		$xls = new Excel_XML('UTF-8',false,cms_get_lang('count'));
		$xls->addArray($arr);
		$xls->generateXML('play_log_count');
		die;
	}
	//导出excel 查询所有条数 xyl 2013-05-28 12:02:23
	public function nns_db_play_log_list_infinite($filter) {

		//$str_sql = " select * from nns_play_log ";
		$str_sql = " select *,(UNIX_TIMESTAMP( nns_end_position ) - UNIX_TIMESTAMP( nns_begin_position )) as nns_time_length from nns_play_log ";
		$str_sql .= $this->nns_db_play_log_where ($filter);

		$this->log->write($str_sql);

		$result = array ();
		$result = $this->db_obj->nns_db_query ( $str_sql );

		return !empty($result['data'])?$result['data']:null;

	}

	/*
	 * 用户播放统计
	 * xyl 2013-06-04 12:01:08
	 * @param arr	: $filter
	 * @param		: $filter['nns_date'];  XXXX年XX月
	 * @param		: $filter['nns_count_order'] 值 count_desc count_asc
	 * @param		: $filter['nns_time_length_order'] 值 time_length_desc time_length_asc
	 * @param		: $open_limit 开关limit
	 * */
	public function nns_db_play_log_user_list($filter,$start=0,$limit=10,$open_limit=null){
		//拼合日期
		if ($filter['log_year'] && $filter['log_month']) {
			$nns_date = $filter['log_year'].'-'.$filter['log_month'];
			//if ($filter['log_month']==12) {
				//$filter['log_year']+=1;
			//}
			//$nns_date1 = $filter['log_year'].'-'.$filter['log_month'];
		}else {
			$nns_date = '';
		}
		// nns_order 

		//SELECT count(*) as `nns_count`,`nns_user_id`,sum((UNIX_TIMESTAMP( nns_end_position ) - UNIX_TIMESTAMP( nns_begin_position )) )as `nns_time_length`  FROM `nns_play_log` where 1=1 group by `nns_user_id` ORDER BY `nns_count` DESC
		//SELECT COUNT( * ) AS  `nns_count` ,  `nns_user_id` ,  `nns_end_position` , SUM( ( UNIX_TIMESTAMP( nns_end_position ) - UNIX_TIMESTAMP( nns_begin_position ) )) AS  `nns_time_length` FROM  `nns_play_log` WHERE  `nns_end_position` LIKE  '2013-05-%' GROUP BY  `nns_user_id` ORDER BY  `nns_count` DESC 
		$str_sql = "SELECT COUNT(*) AS `nns_count`,`nns_user_id`,`nns_end_position`,SUM((UNIX_TIMESTAMP( nns_end_position ) - UNIX_TIMESTAMP( nns_begin_position ) )) AS `nns_time_length` FROM `nns_play_log` WHERE `nns_user_id` <> '' ";
		//" where 1=1 group by `nns_user_id` ";
		//判断日期范围
		if (!empty($nns_date)) {
			$str_sql.=" AND `nns_end_position` like '".$nns_date."-%'";
		}
		//分组
		$str_sql .=" GROUP BY `nns_user_id` ";
		//排序
		if (!empty($nns_order)) {
			$str_sql.=$nns_order;
		}else {
			$str_sql.=" ORDER BY `nns_count` DESC ";
		}
		//分页
		if ($open_limit=='') {
			$str_sql .=" LIMIT $start,$limit";
		}
		//echo $str_sql;
		$this->log->write($str_sql);
		$result = array ();
		$result = $this->db_obj->nns_db_query ( $str_sql );

		return !empty($result['data'])?$result['data']:null;

	}
	/*
	 * 用户播放统计count分页
	 * 2013-06-04 17:47:05
	 **/
	public function nns_db_play_log_user_count($filter){
		if ($filter['log_year'] && $filter['log_month']) {
			$nns_date = $filter['log_year'].'-'.$filter['log_month'];
			//if ($filter['log_month']==12) {
				//$filter['log_year']+=1;
			//}
			//$nns_date1 = $filter['log_year'].'-'.$filter['log_month'];
		}else {
			$nns_date = '';
		}
		// SELECT COUNT( * ) FROM ( SELECT COUNT( * ) AS  `nns_count` ,  `nns_user_id` , SUM( ( UNIX_TIMESTAMP( nns_end_position ) - UNIX_TIMESTAMP( nns_begin_position ) )) AS  `nns_time_length` FROM  `nns_play_log` WHERE 1 =1 GROUP BY  `nns_user_id`) AS new
		//$str_sql = "SELECT COUNT( * ) AS `num` FROM ( SELECT COUNT( * ) AS  `nns_count` ,  `nns_user_id` , SUM( ( UNIX_TIMESTAMP( nns_end_position ) - UNIX_TIMESTAMP( nns_begin_position ) )) AS  `nns_time_length` FROM  `nns_play_log` WHERE 1 =1 ";
		$str_sql = "SELECT COUNT( * ) AS `num` FROM ( SELECT COUNT( * ) AS  `nns_count` ,  `nns_user_id` , SUM( ( UNIX_TIMESTAMP( nns_end_position ) - UNIX_TIMESTAMP( nns_begin_position ) )) AS  `nns_time_length` FROM  `nns_play_log` WHERE `nns_user_id` <> '' ";
		//GROUP BY  `nns_user_id`) AS `count_user_log`";
		//$this->log->write($str_sql);
		//判断日期范围
		if (!empty($nns_date)) {
			$str_sql.=" AND `nns_end_position` like '".$nns_date."-%'";
		}
		//分组
		$str_sql .=" GROUP BY `nns_user_id`) AS `count_user_log`";
		//echo $str_sql;
		$result = array ();
		$result = $this->db_obj->nns_db_query ( $str_sql );
		return isset($result['data'][0]['num'])?$result['data'][0]['num']:0;
		
	}

	/*
	 * 导出用户月份数据为excel
	 * 2013-06-04 17:47:11
	 * @param:		$op = play_log_count_user 页面跳转
	 * @param:		$data_list	数组
	 * @param:		$count		统计
	 * @param:		$params		参数
	 * */
	public function nns_db_log_user_export_excel($data_list,$count,$params){
		//组装成数组
		$num = 0;

		if ($params['log_year'] && $params['log_month']) {
			$nns_date = $params['log_year'].'-'.$params['log_month'];
		}
		$arr = array(
			array(cms_get_lang('total'),$count,cms_get_lang('date'),$nns_date),
			array(cms_get_lang('segnumber'),cms_get_lang('user_id'),cms_get_lang('stat_bf_time_s'),cms_get_lang('stat_play_times_sum'))
		);
		if (!empty($data_list)) {
			foreach ($data_list as $k1=>$v1) {
				$num++;
				$arr[] = array($num,$v1['nns_user_id'],$v1['nns_time_length'],$v1['nns_count']);
			}
		}
		//引入excel.class.php
		$xls = new Excel_XML('UTF-8',false,cms_get_lang('count'));
		$xls->addArray($arr);
		$xls->generateXML('play_log_count_user');
		die;
		
	}
}

?>