<?php
/*
 * Created on 2012-6-28
 * 用户访问统计
 */
$nns_db_dir = dirname(dirname(__FILE__));
$nns_cf_dir = dirname($nns_db_dir);
require_once $nns_db_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
require_once $nns_db_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";
require_once $nns_cf_dir. DIRECTORY_SEPARATOR ."nn_cms_config".DIRECTORY_SEPARATOR. "nn_cms_stat_config.php";
//xyl 2013-06-05 21:58:50
require_once $nns_db_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_excel_class.php";
class nns_db_view_record_class {
	var $db_obj =null;
	private $log;
	function __construct() {
		$this->db_obj = new nns_db_class(NN_STAT_DB_HOST, NN_STAT_DB_USER, NN_STAT_DB_PWD, NN_STAT_DB_NAME);
		$this->conn = $this->db_obj->nns_db_connect();
        $this->log =  new nns_debug_log_class();
        $this->log->setlogpath('view_record');
	}
	/**
	 * @param array $view_record:
	 * nns_id
	 * nns_template_type
	 * nns_template_id
	 * nns_template_name
	 * nns_asset_id
	 * nns_service_id
	 * nns_category_id
	 * nns_content_id
	 * nns_content_name
	 * nns_create_time
	 * nns_user_id
	 * nns_device_id
	 */
	function save($view_record){
		if(!is_array($view_record)) return;
		//
		$nns_create_time = date('Y-m-d H:i:s',time());//
		$guid = new Guid ();
		$id = $guid->toString ();
		$sql ="insert into nns_view_record(nns_id," .
				"nns_template_type," .
				"nns_template_id," .
				"nns_template_name," .
				"nns_asset_id," .
				"nns_service_id," .
				"nns_category_id," .
				"nns_content_id," .
				"nns_content_name," .
				"nns_create_time," .
				"nns_user_id," .
				"nns_user_ip," .
				"nns_device_id)" .
				" values('".$id."'," .
						"'".$view_record['nns_template_type']."'," .
						"'".$view_record['nns_template_id']."'," .
						"'".$view_record['nns_template_name']."'," .
						"'".$view_record['nns_asset_id']."'," .
						"'".$view_record['nns_service_id']."'," .
						"'".$view_record['nns_category_id']."'," .
						"'".$view_record['nns_content_id']."'," .
						"'".$view_record['nns_content_name']."'," .
						"'".$nns_create_time."'," .
						"'".$view_record['nns_user_id']."'," .
						"'".$view_record['nns_user_ip']."'," .
						"'".$view_record['nns_device_id']."'" .
				")";

        $result=$this->db_obj->nns_db_execute($sql);
        if(!$result){
        	$this->log->write($sql.var_export($result,true));
        }
        return $result;
	}
	function view_record_count($filter = null){
        $where = $this->view_record_list_where($filter);
		$sql = "select count(*) as num from nns_view_record where ".$where;

		$result = $this->db_obj->nns_db_query($sql);

		return isset($result['data'][0]['num'])?$result['data'][0]['num']:0;
	}
	function view_record_list($filter = null, $start=0, $size=30,$open_limit=null){
        $where = $this->view_record_list_where($filter);
		$sql = "select nns_template_type,nns_template_id,nns_template_name,nns_asset_id,nns_service_id,nns_category_id,nns_content_id,nns_content_name,nns_create_time,nns_user_id,nns_device_id,nns_user_ip " .  "from nns_view_record where ".$where." order by nns_create_time desc ";
		//加开关,只要不为空,就不输出
		if ($open_limit=='') {
			$sql .= " limit $start,$size";
		}

		$result = $this->db_obj->nns_db_query($sql);
		$this->log->write('view_record_list | '.$sql);
		return isset($result['data'])?$result['data']:null;
	}
	private function view_record_list_where($filter = null) {
		$where = '1';
		if(!empty($filter['tpl_name'])) $where .=" and nns_template_name like '%".$filter['tpl_name']."%'";
		if(!empty($filter['tpl_type_id'])) $where .=" and nns_template_type='".$filter['tpl_type_id']."'";
		if(!empty($filter['tpl_id'])) $where .=" and nns_template_id='".$filter['tpl_id']."'";
		if(!empty($filter['device_id'])) $where .=" and nns_device_id='".$filter['device_id']."'";
		if(!empty($filter['user_id'])) $where .=" and nns_user_id='".$filter['user_id']."'";
		if(!empty($filter['begin_time'])) $where .=" and nns_create_time >= '".$filter['begin_time']."'";
		if(!empty($filter['end_time'])) $where .=" and nns_create_time <= '".$filter['end_time']."'";
		return $where;
	}
	/*
	 * 导出模板数据为excel
	 * 2013-06-05 21:54:33
	 * @param $data_list	数据
	 * @param $count		统计
	 * @param $params		其他参数
	 * */
	public function view_record_export_excel($data_list,$count,$vod_media_arr=null,$tpl_cn_name_arr=null){
		//组装成数组
		$num = 0;
		$nns_date = date('Y-m-d H:i:s');
		$arr = array(
			array(cms_get_lang('total'),$count,cms_get_lang('date'),$nns_date),
			array(cms_get_lang('segnumber'),cms_get_lang('mblx'),cms_get_lang('mbgl_mbys'),cms_get_lang('mbgl_ymmc'),cms_get_lang('assist_id'),cms_get_lang('service_id'),cms_get_lang('lmgl_nrlmid'),cms_get_lang('stat_vod_name'),cms_get_lang('user_id'),cms_get_lang('zdgl_sbid'),cms_get_lang('user_ip'),cms_get_lang('stat_time'))
		);
		if (!empty($data_list)) {
			foreach ($data_list as $k1=>$v1) {
				$num++;
				//影片名
				$v1['nns_vod_media_name'] = '';
				if (!empty($vod_media_arr)) {
					if (isset($v1['nns_content_id'])) {
						foreach ($vod_media_arr as $v_m) {
							if ($v_m['nns_id']==$v1['nns_content_id']) {
								$v1['nns_vod_media_name'] = $v_m['nns_name'].',id='.$v_m['nns_id'];
							}
						}
					}
				}
				$v1['nns_template_name_detail'] = '';
				if (!empty($tpl_cn_name_arr)) {
					$v1['nns_template_name_detail'] = $tpl_cn_name_arr[$v1["nns_template_type"]][$v1["nns_template_id"]][$v1["nns_template_name"]];
				}
				$arr[]=array($num,$v1['nns_template_type'],$v1['nns_template_id'],$v1['nns_template_name_detail'],$v1['nns_asset_id'],$v1['nns_service_id'],$v1['nns_category_id'],$v1['nns_vod_media_name'],$v1['nns_user_id'],$v1['nns_device_id'],$v1['nns_user_ip'],$v1['nns_create_time']);
			}
		}
		//echo '<pre>';
		//var_dump ($arr);exit;
		//echo '</pre>';
		//引入excel.class.php
		$xls = new Excel_XML('UTF-8',false,cms_get_lang('count'));
		$xls->addArray($arr);
		$xls->generateXML('view_record');
		die;
	}

}