<?php
$nns_db_vod_media_dir = dirname ( dirname ( __FILE__ ) );
$nns_db_vod_media_cms_config = dirname ( dirname ( dirname ( __FILE__ ) ) );
require_once $nns_db_vod_media_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
require_once $nns_db_vod_media_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";
require_once $nns_db_vod_media_cms_config . DIRECTORY_SEPARATOR . "nn_cms_config" . DIRECTORY_SEPARATOR . "nn_cms_global.php";
require_once $nns_db_vod_media_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_curl_content_class.php";
require_once $nns_db_vod_media_dir.DIRECTORY_SEPARATOR.'nns_debug_log'.DIRECTORY_SEPARATOR.'nns_debug_log_class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/models/queue_task_model.php';
class nns_db_vod_media_class {

	private $conn;
	private $db_obj;
    private $log;
	public $is_group = 0;
	public $message_id = '';
	public $conf_info = array();
        public $int_weight = 0;
	
	public function __construct() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
        $this->log =  new nns_debug_log_class();
        $this->log->setlogpath('vod_media');
	}
	/**
	 * 权重设置
	 * @param unknown $int_weight
	 */
	public function _set_weight($int_weight)
	{
	    $int_weight = (int) $int_weight;
	    $this->int_weight = $int_weight >0 ? $int_weight : 0;
	}
	
	/**
	 * 获取权重
	 */
	public function _get_weight()
	{
	    $int_weight = (int) $this->int_weight;
	    return $int_weight >0 ? $int_weight : 0;
	}


	/**
	 * 设置消息中的内容是否为分组消息
	 * @param $int_weight
	 */
	public function _set_is_group($is_group)
	{
		$is_group = (int) $is_group;
		$this->is_group = $is_group >0 ? $is_group : 0;
	}

	/**
	 * 获取消息中内容是否分组消息
	 * @return int
	 */
	public function _get_is_group()
	{
		$is_group = (int) $this->is_group;
		return $is_group >0 ? $is_group : 0;
	}


	//媒体统计
	public function nns_db_vod_media_count($vod_id = null, $index = null, $index_id = null, $tag = null, $state = null, $check = null, $deleted = NNS_VOD_MEDIA_DELETED_FALSE,$filetype=null,$mode=null) {
		$result = array ();
		$str_sql = "select * from nns_vod_media ";
		$str_sql .= $this->nns_db_vod_media_where ( 0, 0, $vod_id, $index, $index_id, $tag, $state, $check, $deleted,$filetype,$mode );
		//$result = $str_sql;
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_VOD_MEDIA_NO_DATA, 'vod media data not find' );
			}
			//tag 赛选
			$data = $result ["data"];
			if (! empty ( $tag )) {
				if (is_array ( $tag )) {
					//遍历结果集，删除不包含tag的项
					for($i = count ( $data ) - 1; $i >= 0; $i --) {
						$flag = 0;
						$str = ( string ) $data [$i] ["nns_tag"];
						for($j = 0; $j < count ( $tag ); $j ++) {
							$find = ( string ) $tag [$j];
							if ( $find != "" && $find != null) {
								if (strstr ( $str, $find ) !== false){
									$flag = 1;
									break;
								}
							}
							if ($flag == 1) {
								break;
							}
						} //for
						//删除数组元素
						if ($flag == 0) {
							array_splice ( $data, $i, 1 );
						}
					} //for
					$result ["data"] = $data;
				} // end if is_array
			} //end if
			//end if tag 赛选
			$count [0] ["num"] = count ( $data );
			$result ["data"] = $count;
		}

		return $result;
	}

	//媒体列表
	public function nns_db_vod_media_list($min = 0, $num = 0, $vod_id = null, $index = null, $index_id = null, $tag = null, $state = null, $check = null, $deleted = NNS_VOD_MEDIA_DELETED_FALSE,$filetype=null,$mode=null) {
		$result = array ();
		$str_sql = "select * from nns_vod_media ";
		$str_sql .= $this->nns_db_vod_media_where ( $min, $num, $vod_id, $index, $index_id, $tag, $state, $check, $deleted ,$filetype,$mode);
		//$result = $str_sql;
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_VOD_MEDIA_NO_DATA, 'vod media data not find' );
			}
			//tag 赛选
			if (! empty ( $tag )) {
				if (is_array ( $tag )) {
					$data = $result ["data"];

					//遍历结果集，删除不包含tag的项
					for($i = count ( $data ) - 1; $i >= 0; $i --) {
						$flag = 0;
						$str = ( string ) $data [$i] ["nns_tag"];
						for($j = 0; $j < count ( $tag ); $j ++) {
							$find = ( string ) $tag [$j];
							if ( $find != "" && $find != null) {
								if (strstr ( $str, $find ) !== false){
									$flag = 1;
									break;
								}
							}
						} //for
						//删除数组元素
						if ($flag == 0) {
							array_splice ( $data, $i, 1 );
						}
					} //for
					$result ["data"] = $data;
				} // end if is_array
			} //end if
		//end if tag 赛选
		}
		return $result;
	}
	//媒体信息
	public function nns_db_vod_media_info($media_id) {
		$result = array ();
		//vod_id
		if (! isset ( $media_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod index info param error' );
			return $result;
		}
		if (empty ( $media_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod index info param error' );
			return $result;
		}
		$str_sql = " select * from nns_vod_media where nns_id='$media_id'";

		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_VOD_MEDIA_NOT_FOUND, 'vod media info not found' );
			}
		}

		return $result;
	}
	
	/**
	 * 修改片源的注入id
	 * @param string $media_file_name 片源名称id
	 * @param string $content_id 第三方内容ID
	 * @return multitype:
	 * @author liangpan
	 * @date 2016-08-27
	 */
	public function nns_db_modify_media_content_id($media_file_name='',$content_id='')
	{
		$media_file_name = strtolower(trim($media_file_name));
		if(strpos($media_file_name, 'mgtv') ===  0)
		{
			$media_file_name=substr($media_file_name, 4);
		}
		if(strlen($media_file_name) < 1)
		{
			return $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 1 ,'empty media file name');
		}
		if(strlen($content_id) < 1)
		{
			return $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 2 ,'empty content id');
		}
		$sql_query="select media.nns_id as nns_id,inde.nns_index as nns_index,vod.nns_name as nns_name,media.nns_mode as nns_mode from " .
				" nns_vod_media as media left join nns_vod_index as inde on media.nns_vod_index_id=inde.nns_id left " . 
				" join nns_vod as vod on vod.nns_id=media.nns_vod_id where media.nns_media_name='{$media_file_name}' and media.nns_deleted=0 ";
		$result_query = $this->db_obj->nns_db_query ( $sql_query );
		if ($result_query ["ret"] != 0) 
		{
			return $this->db_obj->nns_db_result ( NNS_ERROR_DB_SQL_EXEC, 5 ,$result_query);
		}
		if ($this->db_obj->num_rows <= 0) 
		{
			$sql_query="select media.nns_id as nns_id,inde.nns_index as nns_index,vod.nns_name as nns_name,media.nns_mode as nns_mode from " .
					" nns_vod_media as media left join nns_vod_index as inde on media.nns_vod_index_id=inde.nns_id left " .
					" join nns_vod as vod on vod.nns_id=media.nns_vod_id where media.nns_name='{$media_file_name}' and media.nns_deleted=0 ";
			$result_query = $this->db_obj->nns_db_query ( $sql_query );
			if ($result_query ["ret"] != 0)
			{
				return $this->db_obj->nns_db_result ( NNS_ERROR_DB_SQL_EXEC, 5 ,$result_query);
			}
			if ($this->db_obj->num_rows <= 0)
			{
				return $this->db_obj->nns_db_result ( NNS_ERROR_VOD_MEDIA_NOT_FOUND, 3 ,'vod media info not found');
			}
			else if($this->db_obj->num_rows > 1)
			{
				return $this->db_obj->nns_db_result ( NNS_ERROR_VOD_MEDIA_NOT_FOUND, 4 ,'vod media info list > 1');
			}
		}
		else if($this->db_obj->num_rows > 1)
		{
			return $this->db_obj->nns_db_result ( NNS_ERROR_VOD_MEDIA_NOT_FOUND, 4 ,'vod media info list > 1');
		}
		if(!isset($result_query['data'][0]['nns_id']) || empty($result_query['data'][0]['nns_id']))
		{
			return $this->db_obj->nns_db_result ( NNS_ERROR_VOD_MEDIA_NOT_FOUND, 3 ,'vod media info not found');
		}
		$date_time=date("Y-m-d H:i:s");
		$str_sql = "update nns_vod_media set nns_content_id='{$content_id}',nns_modify_time='{$date_time}' where nns_id='{$result_query['data'][0]['nns_id']}'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		if($result['ret'] != '0')
		{
			return $this->db_obj->nns_db_result ( NNS_ERROR_DB_SQL_EXEC, 5 ,$result);
		}
		$g_inject = g_cms_config::get_g_config_value('g_inject_enabled');
		if($g_inject)
		{
			$queue_task_model = new queue_task_model();
        	$queue_task_model->_set_weight($this->_get_weight());
			$queue_task_model->q_add_task_op_mgtv($result_query['data'][0]['nns_id'], BK_OP_MEDIA, BK_OP_ADD);
			unset($queue_task_model);
		}
		$int_index = $result_query['data'][0]['nns_index'] +1;
		$str_mode = $this->get_media_mode($result_query['data'][0]['nns_mode']);
		return $this->db_obj->nns_db_result ( NNS_ERROR_OK, 0,"{$result_query['data'][0]['nns_name']}第{$int_index}集{$str_mode}");
	}
	
	/**
	 * 获取影片清晰度名称
	 * @param string $code 清晰度标示码
	 * @return string 清晰度标示中文描述
	 * @author liangpan
	 * @date 2016-08-27
	 */
	public function get_media_mode($code)
	{
		$code = strtolower(trim($code));
		$return_str = "";
		switch ($code)
		{
			case "std":
				$return_str = "标清";
				break;
			case "hd":
				$return_str = "高清";
				break;
			case "low":
				$return_str = "流畅";
				break;
			case "4k":
				$return_str = "4K";
				break;
			case "sd":
				$return_str = "超高清";
				break;
			default:
				$return_str=$code;
		}
		return $return_str;
	}
	
	//修改媒体信息
	public function nns_db_vod_media_modify($media_id, $name = null, $type = null, $url = null, $tag = null, $mode = null, $stream = null,$media_caps='VOD',$dimens=null,$nns_media_content_id=null,$nns_domain='') {
        //$this->log->write('修改媒体信息');
		$result = array ();
		$media_result=$this->nns_db_vod_media_info($media_id);
		$nns_url=$media_result["data"][0]["nns_url"];
        //$this->log->write('media_id='.$media_id);
		if ($nns_url===$url|| $url == null){
			//vod_id
			if (! isset ( $media_id )) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod index modify param error' );
				//$this->log->write('isset ( $media_id )'.var_export($result,true));
				return $result;
			}
			if (empty ( $media_id )) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod index modify param error' );
				//$this->log->write('empty ( $media_id )'.var_export($result,true));
				return $result;
			}
			$str_sql = " update nns_vod_media set nns_deleted=0,nns_name='$name',nns_modify_time=now()";
			if (isset ( $type )) {
				if ($type !== null && $type !== '') {
					$str_sql .= ",nns_type=$type";
				}
			}
			if (isset ( $url )) {
				if (! empty ( $url )) {
					$str_sql .= ",nns_url='$url'";
				}
			}
			if (isset ( $tag )) {
				if (! empty ( $tag )) {
					$str_sql .= ",nns_tag='$tag'";
				}
			}
			if (isset ( $mode )) {
				if (! empty ( $mode )) {
					$str_sql .= ",nns_mode='$mode'";
				}
			}
			if (isset ( $stream )) {
				if (! empty ( $stream )) {
					$str_sql .= ",nns_kbps='$stream'";
				}
			}
			if (isset ( $media_caps )) {
				if (! empty ( $media_caps )) {
					$str_sql .= ",nns_media_caps='$media_caps'";
				}
			}
			if (isset ( $dimens )) {
				if (! empty ( $dimens )) {
					$str_sql .= ",nns_dimensions='$dimens'";
				}
			}
			if (isset ( $nns_media_content_id )) {
				if(strlen($nns_media_content_id) > 0)
				{
					$str_sql .= ",nns_content_id='$nns_media_content_id'";
				}
			}
			$str_sql .= ",nns_domain='$nns_domain'";
			$str_sql .= " where nns_id='$media_id'";
            //$this->log->write($str_sql);
			$result = $this->db_obj->nns_db_execute ( $str_sql );
			
			$g_inject = g_cms_config::get_g_config_value('g_inject_enabled');
				if($g_inject){
					//$g_bk_mode = g_cms_config::get_g_config_value('g_bk_mode');
					//if(empty($g_bk_mode)){
						//category_model::delete_category_content($id,'index');
						//取消该分集的任务
					//	include_once dirname(dirname(dirname(__FILE__))) . "/mgtv/fjyd/models/clip_task_model.php";
					//	clip_task_model::cancel_task_by_vod_index($id);
					//if($g_bk_mode==1){
						//category_content_model::_task_c2_v2($id, BK_OP_MEDIA, BK_OP_DELETE);
					//}
					$queue_task_model = new queue_task_model();
                    $queue_task_model->_set_weight($this->_get_weight());
					//error_log($id.'-----media-----'.$result_kbps['data'][0]['nns_id'].PHP_EOL,3,'txt.log');
					//$queue_task_model->q_add_task($id, BK_OP_MEDIA, BK_OP_DELETE,null,$result_kbps['data'][0]['nns_id']);
					//$queue_task_model->q_add_task($media_id, BK_OP_MEDIA, BK_OP_ADD,null,$media_id);
					$queue_task_model->q_add_task_op_mgtv($media_id, BK_OP_MEDIA, BK_OP_ADD);
					
					unset($queue_task_model);
				} 
		}else{
//			$vod_id=$media_result["data"][0]["nns_url"];
//			$index_name=$media_result["data"][0]["nns_url"];
//			$index=$media_result["data"][0]["nns_url"];
//			$mode=$media_result["data"][0]["nns_url"];
//			$mode=$media_result["data"][0]["nns_url"];
//			$this->nns_db_vod_media_delete($media_id);
//			$this->nns_db_vod_media_add();
//$this->log->write($nns_url.'---'.$url);
		}
        //$this->log->write('最后返回处'.var_export($result,true));
		return $result;
	}
	private function nns_db_vod_media_modify_deleted($id, $deleted) {
		$result = array ();
		if (! isset ( $id ) || ! isset ( $deleted )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod media delete param error' );
			return $result;
		}
		if (empty ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod media delete param error' );
			return $result;
		}
		$deleted = ( int ) $deleted;
		$str_sql = "update nns_vod_media set nns_deleted=" . $deleted . " where nns_id='$id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );

		return $result;
	}
	//媒体删除
	public function nns_db_vod_media_delete($id,$bool=TRUE,$is_need_message=true) {
		//return $this->nns_db_vod_media_modify_deleted ( $id, NNS_VOD_MEDIA_DELETED_TRUE );
		global $g_core_url;
		$result = array ();
		$contentid = "";
		//查取$contentid
		$str_sql = "select nns_content_id,nns_vod_id,nns_vod_index_id,nns_media_service from nns_vod_media where nns_id='$id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows > 0) {
				$contentid = $result ["data"] [0] ["nns_content_id"];				
				//如果是混合模式调用这个接口
				$vod_id = $result ["data"] [0] ["nns_vod_id"];
				$index_id = $result ["data"] [0] ["nns_vod_index_id"];
				$this->conf_info['import_cdn_mode'] = $result ["data"] [0] ["nns_media_service"];
//				$str_sql_kbps = "select nns_id from nns_vod_media where  nns_vod_id='$vod_id' and nns_vod_index_id='$index_id' and nns_deleted!=1 order by nns_kbps desc limit 1";
//				$result_kbps = $this->db_obj->nns_db_query( $str_sql_kbps );
				//如果最该的码率是要删除的媒体id   就不执行以下接口，走常规接口
				//if ($result_kbps['ret']==0&&count($result_kbps['data'])>0){
					//if($id!=$result_kbps['data'][0]['nns_id']){
						//$queue_task_model_kbps = new queue_task_model();
						
						//error_log($result_kbps['data'][0]['nns_id'].'-----media'.PHP_EOL,3,'txt.log');
						//$queue_task_model_kbps->q_add_task($result_kbps['data'][0]['nns_id'], BK_OP_MEDIA, BK_OP_DELETE);
						//unset($queue_task_model_kbps);
					//}
				//}				
			}
		}
		
		
		
		
		
		
		$g_inject = g_cms_config::get_g_config_value('g_inject_enabled');
				if($g_inject && $bool){
					//$g_bk_mode = g_cms_config::get_g_config_value('g_bk_mode');
					//if(empty($g_bk_mode)){
						//category_model::delete_category_content($id,'index');
						//取消该分集的任务
					//	include_once dirname(dirname(dirname(__FILE__))) . "/mgtv/fjyd/models/clip_task_model.php";
					//	clip_task_model::cancel_task_by_vod_index($id);
					//if($g_bk_mode==1){
						//category_content_model::_task_c2_v2($id, BK_OP_MEDIA, BK_OP_DELETE);
					//}
                    //广西删除逻辑不生成队列
                    $g_delete_enable_center_import_v2 = get_config_v2('g_delete_enable_center_import_v2');
                    if($g_delete_enable_center_import_v2 != 1)
                    {
                        $queue_task_model = new queue_task_model();
                        $queue_task_model->_set_weight($this->_get_weight());
                        $queue_task_model->_set_is_group($this->_get_is_group());
                        //error_log($id.'-----media-----'.$result_kbps['data'][0]['nns_id'].PHP_EOL,3,'txt.log');
                        //$queue_task_model->q_add_task($id, BK_OP_MEDIA, BK_OP_DELETE,null,$result_kbps['data'][0]['nns_id']);
                        $str_message_id = ($is_need_message) ? $this->message_id : '';
                        $queue_task_model->q_add_task_op_mgtv($id, BK_OP_MEDIA, BK_OP_DELETE, $str_message_id, true, $this->conf_info);

                        unset($queue_task_model);
                    }
				} 
		
        if($result){
        	$str_sql = "update nns_vod_media set nns_deleted=1,nns_status=3,nns_action='destroy',nns_modify_time=now() where nns_id='$id'";  
			//$str_sql = "delete from nns_vod_media where nns_id='$id'";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
        }

		/******************************************
		if ($result ["ret"] == 0) {
			if (! empty ( $contentid )  && $bool===TRUE) {
				
				$str_sql = "select count(1) as num from nns_vod_media where nns_content_id='$contentid'";
				$result = $this->db_obj->nns_db_query ( $str_sql );
				//若还存在相同片源 则不调用核心管理平台内容删除
				if ($result['data'][0]['num'] == 0){
					//调用核心管理平台内容删除
					//------core start-----------
					//从config 中获取http——url
					$http_url = $g_core_url;
					//通过CURL抓取返回值
					$curl_post = 'func=delete_content' . "&id=" . $contentid;
					nns_db_curl_content_class::nns_db_curl_content ( $http_url, $curl_post );
				}
		//------------core end--------------
			}
		}*******/
		return $result;

	}

	//恢复媒体
	public function nns_db_vod_media_retrieve($id) {
		return $this->nns_db_vod_media_modify_deleted ( $id, NNS_VOD_MEDIA_DELETED_FALSE );
	}

	//媒体审核
	public function nns_db_vod_media_check($media_id, $check) {
		$result = array ();
		if (! isset ( $media_id ) || ! isset ( $check )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod modify check param error' );
			return $result;
		}
		if (empty ( $media_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod modify check param error' );
			return $result;
		}
		$check = ( int ) $check;
		$str_sql = "update nns_vod_media set nns_check=" . $check . " where nns_id='$media_id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );

		return $result;

	}
	//媒体state
	public function nns_db_vod_media_state($media_id, $state) {
		$result = array ();
		if (! isset ( $media_id ) || ! isset ( $state )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod modify state param error' );
			return $result;
		}
		if (empty ( $media_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod modify state param error' );
			return $result;
		}
		$state = ( int ) $state;
		$str_sql = "update nns_vod_media set nns_state=" . $state . " where nns_id='$media_id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );

		return $result;

	}
	//media where
	public function nns_db_vod_media_where($min = 0, $num = 0, $vod_id = null, $index = null, $index_id = null, $tag = null, $state = NNS_VOD_MEDIA_STATE_WAITING, $check = NNS_VOD_MEDIA_CHECK_WAITING, $deleted = NNS_VOD_MEDIA_DELETED_FALSE,$filetype=null,$mode=null) {
		$where = "";
		if (isset ( $vod_id )) {
			if (! empty ( $vod_id )) {
				if (empty ( $where )) {
					$where .= " where nns_vod_id='$vod_id'";
				} else {
					$where .= " and nns_vod_id='$vod_id'";
				}
			}
		}
		if (isset ( $index )) {
			if ($index !== null && $index !== '') {
				if (empty ( $where )) {
					$where .= " where nns_vod_index=$index";
				} else {
					$where .= " and nns_vod_index=$index";
				}
			}
		}

		if (isset ( $index_id )) {
			if (! empty ( $index_id )) {
				if (empty ( $where )) {
					$where .= " where nns_vod_index_id='$index_id'";
				} else {
					$where .= " and nns_vod_index_id='$index_id'";
				}
			}
		}
		//state
		if ($state !== null && $state !== '') {
			$state = ( int ) $state;
			if (empty ( $where )) {
				$where .= " where nns_state=$state";
			} else {
				$where .= " and nns_state=$state";
			}
		}
		if (isset ( $filetype )) {
			if (! empty ( $filetype )) {
				if (empty ( $where )) {
					$where .= " where nns_filetype='$filetype'";
				} else {
					$where .= " and nns_filetype='$filetype'";
				}
			}
		}
		if (isset ( $mode )) {
			if (! empty ( $mode )) {
				if (empty ( $where )) {
					$where .= " where nns_mode='$mode'";
				} else {
					$where .= " and nns_mode='$mode'";
				}
			}
		}
		//check
		if ($check !== null && $check !== '') {
//			$check = ( int ) $check;
//			if (empty ( $where )) {
//				$where .= " where nns_check=$check";
//			} else {
//				$where .= " and nns_check=$check";
//			}
		}
		//deleted
		if ($deleted != "-1") {
			if ($deleted !== null && $deleted !== '') {
				$deleted = ( int ) $deleted;
				if (empty ( $where )) {
					$where .= " where nns_deleted =$deleted";
				} else {
					$where .= " and nns_deleted =$deleted";
				}
			}
		}

		$where .= " order by nns_modify_time desc ";

		if (isset ( $min ) && isset ( $num )) {
			if (! empty ( $num )) {
				$min = ( int ) $min;
				$num = ( int ) $num;
				$where .= " limit $min,$num";
			}
		}

		return $where;
	}

	//修改播放次数
	public function nns_db_vod_media_play_count($media_id) {
		$vod_id = "";
		$vod_index_id = "";
		$result = array ();
		if (! isset ( $media_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod modify play count param error' );
			return $result;
		}
		if (empty ( $media_id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod modify play count param error' );
			return $result;
		}
		//exist
		$str_sql = "select nns_id,nns_vod_index_id,nns_vod_id from nns_vod_media where nns_id='$media_id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );

		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows <= 0) {
		    $result = $this->db_obj->nns_db_result ( NNS_ERROR_VOD_MEDIA_NOT_FOUND, 'vod modify play count not found' );
			return $result;
		}
		$vod_index_id = $result ["data"] [0] ["nns_vod_index_id"];
		$vod_id = $result ["data"] [0] ["nns_vod_id"];

		$str_sql = "update nns_vod_media set nns_play_count= IFNULL(nns_play_count,0) + 1 where nns_id='$media_id'";
		$result = $this->db_obj->nns_db_execute($str_sql);

		$str_sql = "update nns_vod_index set nns_play_count= IFNULL(nns_play_count,0) + 1 where nns_id='$vod_index_id'";
		$result = $this->db_obj->nns_db_execute($str_sql);

		$str_sql = "update nns_vod set nns_play_count= IFNULL(nns_play_count,0) + 1 where nns_id='$vod_id'";
		$result = $this->db_obj->nns_db_execute($str_sql);

		return $result;
	}

	//添加媒体源
	/**
	 *
	 * Enter description here ...
	 * @param string $vod_id 点播ID
	 * @param string $index_name 每一集名字
	 * @param string $media_name 片源名字
	 * @param int $index 集数索引
	 * @param string $mode 高清标清流畅
	 * @param string $stream 码率 384
	 * @param string $type pc or iPhone
	 * @param string $tag 输出标记
	 * @param string $url  存储路径
	 * @param string $time_len 时长
	 * @param string $summary 每一集概要
	 */
	public function nns_db_vod_media_add($vod_id, $index_name, $media_name, $index, $mode, $stream = null, $type = null, $tag = null, $url = null, $time_len = null, $summary = null, 
	    $cdn_policy_id = null, $filetype = null, $media_caps='VOD',$import_source=NULL,$import_id=NULL,$dimens='0',$nns_cp_id=null,$media_type=1,$original_live_id='',$start_time='',$media_service_type='',$nns_dmain='') 
	{
		global $g_core_url;
		$result = array ();
		$start_time = strlen($start_time) >0 ? $start_time : date('Y-m-d H:i:s'); 
		//$this->log->write('添加媒体源');
		//vod_id
		if (! isset ( $vod_id )) 
		{
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod index add param error' );
			//$this->log->write(var_export($result,true));
			return $result;
		}
		if (empty ( $vod_id )) 
		{
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod index add param error' );
			//$this->log->write($result);
			return $result;
		}

		//index
		if (! isset ( $index )) 
		{
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod index add param error' );
			return $result;
		}
		if ($index === null || $index === '') 
		{
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod index add param error' );
			return $result;
		}
		$index = ( int ) $index;
		//mode
		if (! isset ( $mode )) 
		{
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod index add param error' );
			return $result;
		}
		if ($mode === null && $mode === '') 
		{
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod index add param error' );
			return $result;
		}
		if(strlen($nns_cp_id) <1)
		{
			$nns_cp_id = 0;
		}
		//check
		$check = NNS_VOD_MEDIA_CHECK_WAITING;
		//state
		$state = NNS_VOD_MEDIA_STATE_WAITING;
		//select vod state
		$str_sql = "select nns_check,nns_state,nns_import_source from nns_vod where nns_id='$vod_id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) 
		{
			return $result;
		}
		if ($this->db_obj->num_rows < 0) 
		{
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_VOD_NOT_FOUND, 'vod id not found' );
			return $result;
		}
		$data = $result ["data"];
		$vod_check = $data [0] ["nns_check"];
		if ($vod_check == NNS_VOD_AND_MEDIA_CHECK_OK) 
		{
			$check = NNS_VOD_MEDIA_CHECK_OK;
		}
		$check = ( int ) $check;

		$vod_state = $data [0] ["nns_state"];
		$import_source = $data [0] ["nns_import_source"];
		if ($vod_state == NNS_VOD_AND_MEDIA_STATE_ONLINE) 
		{
			$state = NNS_VOD_MEDIA_STATE_ONLINE;
		}
		$state = ( int ) $state;
		// vod_index 索引获取或插入
		$result = $this->nns_db_vod_index_add ( $vod_id, $index, $index_name, $time_len, $summary,$nns_cp_id,$import_source );
		if ($result ["ret"] != 0) {
			return $result;
		}
		$index_id = $result ["data"] [0] ["nns_id"];

		//调用核心管理平台内容添加
		//------core start-----------
		//从config 中获取http——url
		$http_url = $g_core_url;
		//通过CURL抓取返回值
		$curl_post = 'func=add_content_by_cdn_policy' . "&name=" . urlencode ( $media_name ) . "&url=" . urlencode ( $url ) . "&cdn_policy_id=" . $cdn_policy_id . "&live=0"  . "&filetype=" . $filetype;
//		$data = nns_db_curl_content_class::nns_db_curl_content ( $http_url, $curl_post );
//		if (empty ( $data )) {
//			$result = $this->db_obj->nns_db_result ( NNS_ERROR_CORE_CONTENT_FAILED, 'vod media core content return empty' );
//			return $result;
//		}
		//simple xml 解析
//		$xml = simplexml_load_string ( $data );
//		$ret = ( string ) $xml->result ["ret"];
//		$contentid = ( string ) $xml->content ["id"];
//		if ($ret != 0) {
//			$result = $this->db_obj->nns_db_result ( NNS_ERROR_CORE_CONTENT_FAILED, 'vod media core content add failed' );
//			return $result;
//		}
		//------core end-----------


		//vod media 表的插入
		$result = $this->nns_db_vod_media_add_impl ( $vod_id, $index, $index_id, $media_name, $state, $check, $mode, $tag, $type, $url, $stream, $http_url, 
		    $contentid, $ret, $filetype,$media_caps,$import_source,$import_id,$dimens,$nns_cp_id,$media_type,$original_live_id,$start_time,$media_service_type,$nns_dmain);

		return $result;
	}

	//媒体绑定
	/**
	 *
	 * Enter description here ...
	 * @param unknown_type $vod_id
	 * @param unknown_type $index_name
	 * @param unknown_type $media_name
	 * @param unknown_type $index
	 * @param unknown_type $mode
	 * @param unknown_type $stream
	 * @param unknown_type $type
	 * @param unknown_type $tag
	 * @param unknown_type $url
	 * @param unknown_type $time_len
	 * @param unknown_type $summary
	 * @param unknown_type $contentid
	 */
	public function nns_db_vod_media_bind($vod_id, $index_name, $media_name, $index, $mode, $stream = null, $type = null, $tag = null, $url = null, $time_len = null, $summary = null, 
	    $contentid = null, $media_caps='VOD',$import_source=NULL,$import_id=NULL,$dimens='0',$nns_cp_id = null,$media_type = 1,$original_live_id='',$start_time='',$media_service_type='',$nns_domain='') 
	{
		$result = array ();
		global $g_core_url;
		$start_time = strlen($start_time) >0 ? $start_time : date('Y-m-d H:i:s');
		//vod_id
		if (! isset ( $vod_id )) 
		{
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod index add param error' );
			return $result;
		}
		if (empty ( $vod_id )) 
		{
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod index add param error' );
			return $result;
		}

		//index
		if (! isset ( $index )) 
		{
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod index add param error' );
			return $result;
		}
		if ($index === null || $index === '') 
		{
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod index add param error' );
			return $result;
		}
		if(empty($nns_cp_id))
		{
			$nns_cp_id = intval($nns_cp_id);
		}
		$index = ( int ) $index;
		//mode
		if (! isset ( $mode )) 
		{
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod index add param error' );
			return $result;
		}
		if ($mode === null && $mode === '') 
		{
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'vod index add param error' );
			return $result;
		}
		//check
		$check = NNS_VOD_MEDIA_CHECK_WAITING;
		//state
		$state = NNS_VOD_MEDIA_STATE_WAITING;
		//select vod state
		$str_sql = "select nns_check,nns_state,nns_import_source from nns_vod where nns_id='$vod_id' and nns_deleted!=1";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) 
		{
			return $result;
		}
		if ($this->db_obj->num_rows < 0) 
		{
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_VOD_NOT_FOUND, 'vod id not found' );
			return $result;
		}
		$data = $result ["data"];
		$vod_check = $data [0] ["nns_check"];
		if ($vod_check == NNS_VOD_AND_MEDIA_CHECK_OK) 
		{
			$check = NNS_VOD_MEDIA_CHECK_OK;
		}
		$check = ( int ) $check;

		$vod_state = $data [0] ["nns_state"];
		if ($vod_state == NNS_VOD_AND_MEDIA_STATE_ONLINE) 
		{
			$state = NNS_VOD_MEDIA_STATE_ONLINE;
		}
		$state = ( int ) $state;
        $import_source =  $data [0] ["nns_import_source"];
		// vod_index 索引获取或插入
		$result = $this->nns_db_vod_index_add ( $vod_id, $index, $index_name, $time_len, $summary,$nns_cp_id,$import_source);
		if ($result ["ret"] != 0) 
		{
			return $result;
		}
		$index_id = $result ["data"] [0] ["nns_id"];

		//调用核心管理平台内容添加
		//------core start-----------
		//从config 中获取http——url
		$http_url = $g_core_url;
		//通过CURL抓取返回值
		$curl_post = 'func=get_content' . "&id=" . $contentid;
		$data = nns_db_curl_content_class::nns_db_curl_content ( $http_url, $curl_post );
		if (empty ( $data )) 
		{
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_CORE_CONTENT_FAILED, 'vod media core content return empty' );
			return $result;
		}
		//simple xml 解析
		$xml = simplexml_load_string ( $data );
		$ret = ( string ) $xml->result ["ret"];
		if ($ret != 0) 
		{
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_CORE_CONTENT_FAILED, 'vod media core content add failed' );
			return $result;
		}
		$contentid = ( string ) $xml->content ["id"];
		$content_name = ( string ) $xml->content ["name"];
		$file_type = ( string ) $xml->content ["filetype"];
		$url = ( string ) $xml->content ["url"];
		//------core end-----------


		//vod media 表的插入
		$result = $this->nns_db_vod_media_add_impl ( $vod_id, $index, $index_id, $content_name, $state, $check, $mode, $tag, $type, $url, $stream, $http_url, $contentid, $ret, $file_type ,$media_caps,$import_source,$import_id,$dimens,$nns_cp_id,$media_type,$original_live_id,$start_time,$media_service_type,$nns_domain);

		return $result;
	}

	//insert vod_index
	public function nns_db_vod_index_add($vod_id, $index, $index_name, $time_len, $summary,$nns_cp_id=null,$import_source='') {
		$result = array ();
		//vod index表的插入
		//vod_index exist
		if(empty($nns_cp_id))
		{
			$nns_cp_id = 0;
		}
		$str_sql = "select nns_id from nns_vod_index where nns_vod_id='$vod_id' and nns_index=$index and nns_cp_id='$nns_cp_id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		//已经存在则获取index——id，否则插入
		if ($this->db_obj->num_rows <= 0) {
			//insert vod_index
			//生成vod index GUID
			$guid = new Guid ();
			$index_id = $guid->toString ();

			$str_sql = "insert into nns_vod_index";
			$field = "nns_id,nns_name,nns_vod_id,nns_index,nns_create_time,nns_modify_time,nns_status,nns_action,nns_cp_id,nns_import_source";
			$values = "'$index_id','$index_name','$vod_id',$index,now(),now(),1,'add','$nns_cp_id','{$import_source}'";
			//time_len
			if (! empty ( $time_len )) {
				$time_len = ( int ) $time_len;
				$field .= ",nns_time_len";
				$values .= ",$time_len";
			}
			//summary
			if (! empty ( $summary )) {
				$field .= ",nns_summary";
				$values .= ",'$summary'";
			}
			$str_sql .= "(" . $field . ")values(" . $values . ")";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
			if ($result ["ret"] != 0) {
				return $result;
			}
			$result ["data"] [0] ["nns_id"] = $index_id;
			$g_inject = g_cms_config::get_g_config_value('g_inject_enabled');
			if($g_inject){
			    //category_content_model::_task_c2_v2($media_id, BK_OP_MEDIA, BK_OP_ADD);
			    $queue_task_model = new queue_task_model();
			    $queue_task_model->_set_weight($this->_get_weight());
			    //$queue_task_model->q_add_task($media_id, BK_OP_MEDIA, BK_OP_ADD,null,$old_media_id);
			    $queue_task_model->q_add_task_op_mgtv($index_id, BK_OP_INDEX, BK_OP_ADD);
			     
			    unset($queue_task_model);
			}
		} //end if else
		
		return $result;
	}

	public function check_exsist_and_del($vod_id,$index_id,$mode,$file_type,$dimens,$stream,$nns_cp_id,$tag,$media_type=1,$original_live_id='',$service_type='')
	{
	    if ($dimens=='0' || $dimens=='')
	    {
	        $dimens_str = " and (nns_dimensions='0' or nns_dimensions='') ";
	    }
	    else
	    {
	        $dimens_str = " and nns_dimensions='$dimens' ";
	    }
	    if(strlen($stream) > 0)
	    {
	        $stream_str=" and nns_kbps='$stream' ";
	    }
	    if(strlen($nns_cp_id) <1)
	    {
	        $nns_cp_id = 0;
	    }
	    //片源类型 1 点播  2回看
	    if(strlen($media_type) > 0)
	    {
	    	$m_type_str = " and nns_media_type='{$media_type}' ";
	    }
	    //频道原始ID,回看点播源存在
	    if(strlen($original_live_id) > 0)
	    {
	    	$o_live_str = " and nns_original_live_id='{$original_live_id}' ";
	    }
	    //片源服务类型
	    if(strlen($service_type) > 0)
	    {
	    	$s_type_str = " and nns_media_service='{$service_type}' ";
	    }
	    $str_sql = "select nns_id from nns_vod_media where nns_deleted=0 and nns_vod_id='$vod_id' and nns_vod_index_id='$index_id' and nns_mode='$mode' and nns_filetype='$file_type' and nns_cp_id='$nns_cp_id' {$dimens_str} {$stream_str} {$m_type_str} {$o_live_str} {$s_type_str} ";
	    if ($tag !== null && $tag !== '') 
	    {
	        $str_sql .= "  and nns_tag='$tag'";
	    }
	    
	    $result = $this->db_obj->nns_db_query($str_sql);
	    if ($result ["ret"] != 0) 
	    {
	        return $result;
	    }
	    if(is_array($result['data']) && !empty($result['data']))
	    {
	        foreach ($result['data'] as $val)
	        {
	            $result = $this->nns_db_vod_media_delete($val['nns_id']);
	        }
	    }
	    return $result;
	}
	
	
	public function nns_db_vod_media_add_impl($vod_id, $index, $index_id, $media_name, $state, $check, $mode, $tag, $type, $url, $stream, $http_url,
	    $contentid, $ret, $file_type,$media_caps='VOD',$import_source=NULL,$import_id=NULL,$dimens='0',$nns_cp_id=null,$media_type=1,
	    $original_live_id='',$start_time='',$media_service_type='',$nns_dmain='') 
	{
        $start_time = strlen($start_time) >0 ? $start_time : date('Y-m-d H:i:s');
        //insert vod media
		// vod media exist
		//清晰度验证
		$dimens_str = ($dimens=='0' || $dimens=='') ? " and (nns_dimensions='0' or nns_dimensions='') " : " and nns_dimensions='{$dimens}' ";
		//码率检测
		$stream_str  = (strlen($stream) > 0) ? " and nns_kbps='{$stream}' " : '';
		//片源地址检测
		$url_str  = (strlen($url) > 0) ? " and nns_url='{$url}' " : '';
		//CPID 处理
		$nns_cp_id = (strlen($nns_cp_id) <1) ? 0 : $nns_cp_id;
		//片源类型 1 点播  2回看
		$m_type_str = (strlen($media_type) > 0) ? " and nns_media_type='{$media_type}' " : '';
		//频道原始ID,回看点播源存在
		$o_live_str = (strlen($original_live_id) > 0) ? " and nns_original_live_id='{$original_live_id}' " : '';
		//片源服务类型
		$s_type_str = (strlen($media_service_type) > 0) ? " and nns_media_service='{$media_service_type}' " : '';
		//tag标示验证
		$s_tag_str = ($tag !== null && $tag !== '') ? " and nns_tag='{$tag}' " : '';
		
		$str_sql = "select 1 from nns_vod_media where nns_deleted=0 and nns_vod_id='$vod_id' and nns_vod_index_id='$index_id' and nns_mode='$mode' and nns_filetype='$file_type' and nns_cp_id='$nns_cp_id' {$dimens_str} {$stream_str} {$url_str} {$m_type_str} {$o_live_str} {$s_type_str} {$s_tag_str} ";
		
		$result = $this->db_obj->nns_db_query($str_sql);
        if ($result ["ret"] != 0) 
        {
                return $result;
        }
        if ($this->db_obj->num_rows > 0) 
        {
                $result = $this->db_obj->nns_db_result(NNS_ERROR_VOD_MEDIA_EXIST, 'vod media exist');
                return $result;
        }
		
		//注入前将码率最高的查询放入操作列队 无用，屏蔽 zhiyong.luo 2017-02-23
// 		$old_media_id = '';
// 		$str_sql_kbps = "select * from nns_vod_media where nns_deleted=0 and nns_vod_id='$vod_id' and nns_vod_index_id='$index_id' and nns_deleted!=1 order by nns_kbps desc limit 1";
// 		$result_kbps = $this->db_obj->nns_db_query( $str_sql_kbps );
// 		if ($result_kbps['ret']==0 && count($result_kbps['data']) > 0)
// 		{
// 			$old_media_id = $result_kbps['data'][0]['nns_id'];
// 		}
// 		if(empty($nns_cp_id))
// 		{
// 			$nns_cp_id = 0;
// 		}
		
		
		
		//生成vod media GUID
		$guid = new Guid ();
		$media_id = $guid->toString ();
		
		$ret=(int) $ret;
		$nns_media_name = $this->get_file_name($url);
		$str_sql = "insert into nns_vod_media(nns_id,nns_name,nns_vod_id,nns_vod_index,nns_vod_index_id,nns_state,nns_check,nns_tag,nns_type,nns_url,
		nns_mode,nns_kbps,nns_create_time,nns_modify_time,nns_deleted,nns_content_id,nns_content_state,nns_filetype,nns_media_caps,nns_import_source,
		nns_import_id,nns_dimensions,nns_status,nns_action,nns_cp_id,nns_media_name,nns_media_type,nns_original_live_id,nns_start_time,
		nns_media_service,nns_domain)values('$media_id','$media_name','$vod_id',$index,'$index_id',$state,$check,'$tag',$type,'$url','$mode','$stream',now(),
		now()," . NNS_VOD_MEDIA_DELETED_FALSE . ",'$contentid','$ret','$file_type','$media_caps','$import_source','$import_id','{$dimens}',1,'add',
		'$nns_cp_id','{$nns_media_name}','{$media_type}','{$original_live_id}','{$start_time}','{$media_service_type}','{$nns_dmain}')";
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		if ($result ["ret"] != 0 && !empty($contentid)) 
		{
			//vod media 没添加成功时，调用核心管理平台，删除已经添加的内容
			//------------core start--------------
			$curl_post = 'func=delete_content' . "&id=" . $contentid;
			//fjyd 不需要core
			//nns_db_curl_content_class::nns_db_curl_content ( $http_url, $curl_post );
			//------------core end--------------
			return $result;
		}
		//fjyd有片源更新
		
		$g_inject = g_cms_config::get_g_config_value('g_inject_enabled');
		if($g_inject){
			//category_content_model::_task_c2_v2($media_id, BK_OP_MEDIA, BK_OP_ADD);	
			$queue_task_model = new queue_task_model();
			$queue_task_model->_set_weight($this->_get_weight());
		$queue_task_model->_set_is_group($this->_get_is_group());
			//$queue_task_model->q_add_task($media_id, BK_OP_MEDIA, BK_OP_ADD,null,$old_media_id);
			$queue_task_model->q_add_task_op_mgtv($media_id, BK_OP_MEDIA, BK_OP_ADD,$this->message_id,true,$this->conf_info);
			
			unset($queue_task_model);
		}
		
		
		//更新第几集
		$str_sql = "update nns_vod set nns_new_index=$index,nns_status=2,nns_action='modify',nns_modify_time=now() where nns_id='$vod_id' and nns_cp_id='$nns_cp_id'";
		$this->db_obj->nns_db_execute ( $str_sql );

		$result["nns_id"]=$media_id;
		
		
		return $result;
	}
	
	
	/**
	 * 获取片源路径的文件名称
	 * @param string $url
	 * @return string
	 * @author liangpan
	 * @date 2016-08-27
	 */
	public function get_file_name($url)
	{
		$url = trim($url);
		if(strlen($url) < 1)
		{
			return '';
		}
		$pathinfo_file = pathinfo(basename($url));
		$file_name=$pathinfo_file['filename'];
		$file_name = strtolower(str_replace(array('_','.'), '', $file_name));
		return strlen($file_name)< 1 ? '' : $file_name;
	}
	
	
	/**
 * 通过导入ID 获取CMS平台ID
 * @param String 来源
 * @param String 来源ID
 * @return string ID
 * 			FALSE 无此ID
 */
	public function get_video_media_id_by_import_id($import_source,$import_id,$nns_cp_id=null,$check_enabled=1,$media_service_type=null){
		$str_sql="select nns_id,nns_vod_id from nns_vod_media where " .
				
				"nns_import_id='{$import_id}' and nns_deleted!=1";		
		//当cp id长度大于1的时候显示原CP ID
		if(strlen($nns_cp_id) > 1)
		{
			$str_sql.=" and nns_cp_id = '{$nns_cp_id}' ";
		}
		else
		{
			$str_sql.=" and nns_cp_id='0' ";
		}
		if($check_enabled)
		{
			$str_sql .= " and nns_import_source='{$import_source}'";
		}
		if(!empty($media_service_type))
		{
			$str_sql .= " and nns_media_service='{$media_service_type}'";
		}
		$result = $this->db_obj->nns_db_query( $str_sql );
		if (count($result['data'])>0)
		{
			return $result['data'][0];
		}
		else
		{
			return FALSE;
		}
	}
	
	
	/**
 * 通过导入ID 获取CMS平台ID
 * @param String 来源
 * @param String 来源ID
 * @param array 片源注入参数
 * @return string ID
 * 			FALSE 无此ID
 */
	
	public function get_video_media_id_by_import_id_bk($import_source,$import_id,$params,$cp_id=null)
	{
		$str_sql="select nns_id,nns_vod_id,nns_mode from nns_vod_media where " .
				"nns_import_source='{$import_source}' and " .
				"nns_import_id='{$import_id}' and nns_deleted!=1 ";		

		if(strlen($cp_id) > 0)
		{
			$str_sql.=" and nns_cp_id='{$cp_id}' ";
		}
		if($params['index'])
		{
			$str_sql.=" and nns_vod_index='{$params['index']}' ";
		}
		if($params['asset_id'])
		{
			$str_sql.=" and nns_vod_id='{$params['asset_id']}' ";
		}
		if($params['clip_id'])
		{
			$str_sql.=" and nns_vod_index_id='{$params['clip_id']}' ";
		}
		//片源类型 1 点播源 2 回看源
		if(strlen($params['media_type']) > 0)
		{
			$str_sql.=" and nns_media_type='{$params['media_type']}' ";
		}
		//回看频道原始ID
		if(strlen($params['original_live_id']) > 0)
		{
			$str_sql.=" and nns_original_live_id='{$params['original_live_id']}' ";
		}
		if(strlen($params['media_service_type']) > 0)
		{
			$str_sql.=" and nns_media_service='{$params['media_service_type']}' ";
		}
		$result = $this->db_obj->nns_db_query( $str_sql );
		if ($result['ret'] == 0 && count($result['data']) > 0)
		{			
			$update_sql = "update nns_vod_media set nns_modify_time=now() where nns_id='{$result['data'][0]['nns_id']}'";
			$this->db_obj->nns_db_execute($update_sql);
			return $result['data'][0];
		}
		else
		{
			return FALSE;
		} 
	}
	/**
	 * @param $vod_info array 主媒信息
	 * @param $vod_index_info 分集信息
	 * @param $file_id 片源注入ID
	 * @param $file_definition 高青/标青
	 */
	
	public function get_file_media_check($vod_info_id,$vod_index_info,$file_id,$file_definition,$file_type='ts',$file_dimensions=0,$cp_id=0,$stream=null,$tag=null,$media_type=1,$original_live_id='',$service_type='')
	{
		$vod_id = 	$vod_info_id;
		$vod_index_id = $vod_index_info['nns_id'];
		$dimens_str = "";
		if ($file_dimensions=='0' || $file_dimensions=='')
		{
			$dimens_str.=" and (nns_dimensions='0' or nns_dimensions='') ";
		}
		else
		{
			$dimens_str.=" and nns_dimensions='{$file_dimensions}' ";
		}
		
		if($file_type)
		{
			$dimens_str .=" and nns_filetype='{$file_type}'";
		}
		if(strlen($file_definition) > 0)
		{
			$dimens_str .=" and nns_mode='{$file_definition}'";
		}
		if(strlen($cp_id) > 0)
		{
			$dimens_str .=" and nns_cp_id='{$cp_id}'";
		}
		if(strlen($stream) > 0)
		{
			$dimens_str .=" and nns_kbps='{$stream}' ";
		}
		if(strlen($tag) > 0)
		{
			$dimens_str .=" and nns_tag='{$tag}' ";
		}
		
		//片源类型 1 点播  2回看
		if(strlen($media_type) > 0)
		{
			$dimens_str .= " and nns_media_type='{$media_type}' ";
		}
		//频道原始ID,回看点播源存在
		if(strlen($original_live_id) > 0)
		{
			$dimens_str .= " and nns_original_live_id='{$original_live_id}' ";
		}
		//片源服务类型
		if(strlen($service_type) > 0)
		{
			$dimens_str .= " and nns_media_service='{$service_type}' ";
		}
		$sql = "select * from nns_vod_media where nns_vod_id='$vod_id' and nns_vod_index_id='$vod_index_id' and nns_deleted!=1" . $dimens_str;
		$result = $this->db_obj->nns_db_query( $sql );
		if ($result['ret']==0 && count($result['data']) > 0)
		{
			foreach ($result['data'] as $val) 
			{
				if(strtolower($val['nns_mode']) == strtolower($file_definition) && $val['nns_import_id'] != $file_id)
				{
					$this->nns_db_vod_media_delete($val['nns_id']);
				}
			}			
		}
		return $result['data'];
	}
	public function add_media_other_data($media_id, $params = null,$nns_cp_id=null)
	{
		if (empty($params))
		{
			return false;
		}
		foreach ($params as $key => $v)
		{
			$set .= "nns_{$key}='{$v}',";
		}
		$str_update = (strlen($nns_cp_id) > 0 ) ? " and nns_cp_id='{$nns_cp_id}' " : " and nns_cp_id='0' ";
		$set = trim($set, ',');
		$sql = "update nns_vod_media set {$set} where nns_id='{$media_id}' {$str_update}";
		return $this->db_obj->nns_db_execute ( $sql );
		
	}
	
	public function check_metadata_media($vod_id,$index_id,$media_import_id)
	{
		$check_sql = "select nns_id from nns_vod_media where nns_deleted=0 and nns_vod_id='{$vod_id}' and nns_vod_index_id='{$index_id}' and nns_import_id='{$media_import_id}'";
		$result = $this->db_obj->nns_db_query($check_sql);
		if ($result['ret'] == 0 && count($result['data']) > 0)
		{
			foreach ($result['data'] as $val) 
			{
				$this->nns_db_vod_media_delete($val['nns_id'],false);
			}
		}
	}
	/**
	 * @param $vod_info array 主媒信息
	 * @param $vod_index_info 分集信息
	 * @param $file_id 片源注入ID
	 * @param $file_definition 高青/标青
	 */
	
	public function get_file_media_check_v2($vod_info_id,$vod_index_info,$file_dimensions,$file_type='ts',$cp_id=0,$media_type=1,$original_live_id='',$service_type='')
	{
		$vod_id = 	$vod_info_id;
		$vod_index_id = $vod_index_info['nns_id'];
		$dimens_str = "";
		if ($file_dimensions=='0' || $file_dimensions=='')
		{
			$dimens_str.=" and (nns_dimensions='0' or nns_dimensions='') ";
		}
		else
		{
			$dimens_str.=" and nns_dimensions='{$file_dimensions}' ";
		}
	
		if($file_type)
		{
			$dimens_str .=" and nns_filetype='{$file_type}'";
		}
		if(strlen($cp_id) > 0)
		{
			$dimens_str .=" and nns_cp_id='{$cp_id}'";
		}
		//片源类型 1 点播  2回看
		if(strlen($media_type) > 0)
		{
			$dimens_str .= " and nns_media_type='{$media_type}' ";
		}
		//频道原始ID,回看点播源存在
		if(strlen($original_live_id) > 0)
		{
			$dimens_str .= " and nns_original_live_id='{$original_live_id}' ";
		}
		//片源服务类型
		if(strlen($service_type) > 0)
		{
			$dimens_str .= " and nns_media_service='{$service_type}' ";
		}
		$sql = "select nns_id,nns_mode from nns_vod_media where nns_vod_id='$vod_id' and nns_vod_index_id='$vod_index_id' and nns_deleted!=1" . $dimens_str;
		$result = $this->db_obj->nns_db_query( $sql );
		if ($result['ret']==0 && count($result['data']) > 0)
		{
			return $result['data'][0];
		}
		else
		{
			return false;
		}
	}
}
?>
