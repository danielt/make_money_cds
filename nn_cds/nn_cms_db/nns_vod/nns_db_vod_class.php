<?php
$nns_db_vod_dir = dirname(dirname(__FILE__));
require_once $nns_db_vod_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
require_once $nns_db_vod_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";
require_once $nns_db_vod_dir . DIRECTORY_SEPARATOR . "nns_category" . DIRECTORY_SEPARATOR . "nns_db_category_detail_class.php";
//require_once $nns_db_vod_dir . DIRECTORY_SEPARATOR . "nns_assist" . DIRECTORY_SEPARATOR . "nns_db_assist_item_class.php";
require_once $nns_db_vod_dir . DIRECTORY_SEPARATOR . "nns_assist" . DIRECTORY_SEPARATOR . "nns_db_assist_bind_class.php";
require_once $nns_db_vod_dir . DIRECTORY_SEPARATOR . 'nns_debug_log' . DIRECTORY_SEPARATOR . 'nns_debug_log_class.php';
require_once $nns_db_vod_dir . DIRECTORY_SEPARATOR . "nns_memcached" . DIRECTORY_SEPARATOR . "memcached_manager.class.php";
include_once dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'nn_cms_config'
    . DIRECTORY_SEPARATOR . 'nn_cms_global.php';
include_once dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'nn_logic' . DIRECTORY_SEPARATOR . 'asset'
    . DIRECTORY_SEPARATOR . 'asset_item.class.php';
include_once $nns_db_vod_dir . DIRECTORY_SEPARATOR . "nns_label" . DIRECTORY_SEPARATOR . "nns_label.class.php";


//加载BK配置以及栏目映射
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/models/queue_task_model.php';

class nns_db_vod_class {

    private $conn;
    private $db_obj;
    private $log;

    public $int_weight = 0;
    public $is_group = 0;
    public $message_id = '';

    public function __construct() {
        $this->db_obj = new nns_db_class(g_db_host, g_db_username, g_db_password, g_db_name);
        $this->conn = $this->db_obj->nns_db_connect();
        $this->log = new nns_debug_log_class();
        $this->log->setlogpath('vod');
    }

    /**
     * 权重设置
     * @param unknown $int_weight
     */
    public function _set_weight($int_weight)
    {
        $int_weight = (int) $int_weight;
        $this->int_weight = $int_weight >0 ? $int_weight : 0;
    }

    /**
     * 获取权重
     */
    public function _get_weight()
    {
        $int_weight = (int) $this->int_weight;
        return $int_weight >0 ? $int_weight : 0;
    }

    /**
     * 设置消息中的内容是否为分组消息
     * @param $int_weight
     */
    public function _set_is_group($is_group)
    {
        $is_group = (int) $is_group;
        $this->is_group = $is_group >0 ? $is_group : 0;
    }

    /**
     * 获取消息中内容是否分组消息
     * @return int
     */
    public function _get_is_group()
    {
        $is_group = (int) $this->is_group;
        return $is_group >0 ? $is_group : 0;
    }
    //count
    /**
     *
     * VOD统计.
     * @param string $name 名称
     * @param int $state 上下线
     * @param int $check 审核
     * @param int $view_type 电视剧或电影
     * @param int $org_type 机构类型
     * @param string $org_id 机构ID
     * @param array(31) $tag tag 标记
     * @param array(4) $vod_info 影视信息
     * @param int $deleted 删除标记
     * @param string $order 排序字段
     */
    public function nns_db_vod_count(
        $name = null, $state = null, $check = null, $view_type = null, $depot_id = null, $depot_detail_id = null, $org_type = null, $org_id = null, $tag = null, $vod_info = null, $deleted = NNS_VOD_DELETED_FALSE, $except_method = null, $except_detail_id = null, $except_id = null, $begin_sy_date = null, $end_sy_date = null, $begin_create_date = null, $end_create_date = null, $begin_copyright_date = null, $end_copyright_date = null, $searchpoint = null, $depot_like_detail_id = null, $summary = null, $index_num = null, $min_len = null, $max_len = null, $nns_pinyin = null, $nns_no_pic = null
    ) {

        $result = array();
        $str_sql = "select count(1) as num from nns_vod ";
        $str_sql .= $this->nns_db_vod_where(
            $name, $state, $check, $view_type, $depot_id, $depot_detail_id, $org_type, $org_id, $tag, $vod_info, null, 0, 0, $deleted, $except_method, $except_detail_id, $except_id, $begin_sy_date, $end_sy_date, $begin_create_date, $end_create_date, $begin_copyright_date, $end_copyright_date, $searchpoint, $depot_like_detail_id, $summary, $index_num, $min_len, $max_len, $nns_pinyin, $nns_no_pic
        );
        //echo    $end_create_date;
        //echo $str_sql;exit;

        if (isset($_GET['search_pyerror']) && $_GET['search_pyerror'] == 1) {
            $str_sql = "SELECT COUNT(  'name' ) AS num
FROM (
SELECT CHAR_LENGTH(  `nns_name` ) AS name,  `nns_pinyin_length` 
FROM  `nns_vod`
) AS se
WHERE name !=  `nns_pinyin_length`";
        }
        $result = $this->db_obj->nns_db_query($str_sql);

        return $result;
    }

    /**
     * 2012-8-29 author:bo.chen@starcorcn.com  添加直播栏目树分类的影片数量统计
     */
    public function nns_db_vod_category_count($sql) {
        $result = $this->db_obj->nns_db_query($sql);
        return $result;
    }

    //list vod
    /**
     *
     * VOD列表 ...
     * @param string $field 查询字段
     * @param string $name 名称
     * @param int $state 上下线
     * @param int $check 审核
     * @param int $view_type 电视剧或电影
     * @param int $org_type 机构类型
     * @param string $org_id 机构ID
     * @param array[31] $tag tag 标记
     * @param array(4) $vod_info 影视信息
     * @param string $order 排序字段
     * @param int $min
     * @param int $num
     * @param int $deleted 删除标记
     */
    public function nns_db_vod_list(
        $field = null, $name = null, $state = null, $check = null, $view_type = null, $depot_id = null, $depot_detail_id = null, $org_type = null, $org_id = null, $tag = null, $vod_info = null, $order = null, $min = 0, $num = 0, $deleted = NNS_VOD_DELETED_FALSE, $except_method = null, $except_detail_id = null, $except_id = null, $begin_sy_date = null, $end_sy_date = null, $begin_create_date = null, $end_create_date = null, $begin_copyright_date = null, $end_copyright_date = null, $searchpoint = null, $depot_like_detail_id = null, $summary = null, $index_num = null, $min_len = null, $max_len = null, $nns_pinyin = null, $nns_no_pic = null, $nns_ids_arr = null
    ) {
        $result = array();
        if (empty($field)) {
            $field = " nns_name,nns_state,nns_view_type,nns_depot_id,nns_category_id," .
                "nns_org_type,nns_org_id,nns_pub_category_id,nns_pri_category_id," .
                "nns_pub_category_detail_id,nns_pri_category_detail_id,nns_tag," .
                "nns_director,nns_actor,nns_show_time,nns_area,nns_create_time,nns_pinyin_length ";
        }
        $str_sql = " select " . $field . " from nns_vod ";

        $str_sql .= $this->nns_db_vod_where(
            $name, $state, $check, $view_type, $depot_id, $depot_detail_id, $org_type, $org_id, $tag, $vod_info, $order, $min, $num, $deleted, $except_method, $except_detail_id, $except_id, $begin_sy_date, $end_sy_date, $begin_create_date, $end_create_date, $begin_copyright_date, $end_copyright_date, $searchpoint, $depot_like_detail_id, $summary, $index_num, $min_len, $max_len, $nns_pinyin, $nns_no_pic, $nns_ids_arr
        );

        //die($str_sql);
        if (isset($_GET['search_pyerror']) && $_GET['search_pyerror'] == 1) {
            $str_sql = "SELECT * 
FROM (
SELECT * , CHAR_LENGTH(  `nns_name` ) AS name
FROM  `nns_vod`
) AS se
WHERE name != `nns_pinyin_length` LIMIT $min, $num";
        }
        $result = $this->db_obj->nns_db_query($str_sql);
        if ($result ["ret"] == 0) {
            if ($this->db_obj->num_rows <= 0) {
                $result = $this->db_obj->nns_db_result(NNS_ERROR_VOD_NO_DATA, 'vod data not find');
            }
        }
        return $result;
    }

    //info
    /**
     *
     * VOD信息
     * @param string $id vodid GUID
     * @param string $field 字段
     */
    public function nns_db_vod_info($id, $field = null) {
        $result = array();
        if (!isset($id)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod info param error');
            return $result;
        }
        if (empty($id)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod info param error');
            return $result;
        }
        if (empty($field)) {
            $field = " nns_id,nns_name,nns_state,nns_view_type,nns_depot_id,nns_category_id," .
                "nns_org_type,nns_org_id,nns_tag,nns_director,nns_actor,nns_show_time," .
                "nns_create_time,nns_area,nns_summary ";
        }

        $str_sql = "select " . $field . " from nns_vod where nns_id='$id'";
        $result = $this->db_obj->nns_db_query($str_sql);
        if ($result ["ret"] == 0) {
            if ($this->db_obj->num_rows <= 0) {
                $result = $this->db_obj->nns_db_result(NNS_ERROR_VOD_NOT_FOUND, 'vod info not find');
            }
        }
        return $result;
    }

    //add
    /**
     *
     * 添加VOD
     * @param string $name 名称
     * @param int $view_type 电视剧或电影
     * @param array(4) $catogery 栏目
     * @param int $org_type 机构类型
     * @param string $org_id 机构id
     * @param array(32) $tag CP/SP标记
     * @param array(4) $vod_info 影视信息
     * @param string $summary 简介
     * @param string $remark 备注
     *
     */
    public function nns_db_vod_add(
        $name, $view_type = 1, $depot_id = null, $depot_detail_id = null, $org_type = 0, $org_id = null, $tag = null, $vod_info = null, $summary = null, $point = null, $copyright_date = null, $remark = null, $nns_asset_import_id = null, $nns_pinyin = null, $check = null, $nns_import_source = NULL, $nns_pinyin_length = '0', $nns_cp_id = null,$nns_producer=null
    ) {
        //	修改影片信息输入
        foreach ($vod_info as $info_key => $info_value) {
            if ($info_key == 'actor' || $info_key == 'director') {
                $space = false;
            } else {
                $space = true;
            }
            if (!empty($info_value)) {
                $vod_info[$info_key] = np_build_split_string($info_value, '/', $space);
            }
        }

        //$this->log->write(__LINE__.'nns_db_vod_add');
        $result = array();
        if (!isset($name)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod add param error');
            $this->log->write(__LINE__ . var_export($result, true));
            return $result;
        }
        if (empty($name)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod add param error');
            $this->log->write(__LINE__ . var_export($result, true));
            return $result;
        }
        //生成GUID
        $guid = new Guid ();
        $id = $guid->toString();
        //exist
        //		$str_sql = "select 1 from nns_vod where nns_id='$id' ";
        //		$result = $this->db_obj->nns_db_query ( $str_sql );
        //		if ($result ["ret"] != 0) {
        //			return $result;
        //		}
        //		if ($this->db_obj->num_rows > 0) {
        //			$result = $this->db_obj->nns_db_result ( NNS_ERROR_VOD_EXIST, 'vod name exist' );
        //			$this->log->write(__LINE__.var_export($result,true));
        //			return $result;
        //		}
        //check
        $check = (int) $check;
        //state

        $state = 0;


        //如果是包含媒资注入ID，则表示该影片是媒资注入的
        if ($nns_asset_import_id != null) {
            $state = 2;
        }


        $point = (int) $point;
        $time_key = '';
        $time_value = '';
        if (!empty($copyright_date)) {
            $time_key.=",nns_copyright_date";
            $time_value.=",'$copyright_date'";
        }
        $str_sql = "insert into nns_vod";
        $field = "nns_pinyin_length,nns_id,nns_name,nns_check,nns_state,nns_view_type," .
            "nns_depot_id,nns_category_id,nns_point,nns_asset_import_id" . $time_key;
        $values = "'$nns_pinyin_length','$id','$name',$check,$state,$view_type,'$depot_id'," .
            "'$depot_detail_id',$point,'$nns_asset_import_id'" . $time_value;
        //org
        if ($org_type !== null && $org_type !== '') {
            $org_type = (int) $org_type;
            $field .= ",nns_org_type";
            $values .= ",$org_type";
        }
        if ($org_id != null) {
            $field .= ",nns_org_id";
            $values .= ",'$org_id'";
        }
        if (strlen($nns_cp_id) > 0)
        {
            //$nns_cp_id = intval($nns_cp_id);
            $field .= ",nns_cp_id";
            $values .= ",'$nns_cp_id'";
        }
        else
        {
            $field .= ",nns_cp_id";
            $values .= ",'0'";
        }
        /**
         * 原tag存储方式废除，改为单个字段存储标识 BY S67
         */
        //		//tag
        //		if (! empty ( $tag )) {
        //			if (is_array ( $tag )) {
        //				for($i = 0; $i < count ( $tag ); $i ++) {
        //					$temp = "tag" . $i;
        //					if ($tag [$i] !== null && $tag [$i] !== '') {
        //						$field .= ",nns_" . $temp;
        //						$tag_value = ( int ) $tag [$i];
        //						$values .= ",$tag_value";
        //					}
        //
        //				} //for
        //			}
        //
        //		} // tag end if

        /**
         * 新tag存储方式
         */
        if (!empty($tag)) {
            if (is_array($tag)) {
                $len = count($tag);
                $field .= ",nns_tag ";
                $tag_value = "";
                for ($i = 0; $i < $len; $i++) {
                    if (!empty($tag [$i])) {
                        $tag_value .= $i . ',';
                    }
                } //for
                $values .= ",'$tag_value'";
            }
        }
        //vod_info
        if (!empty($vod_info)) {
            if (is_array($vod_info)) {
                if ($vod_info ["director"] != null)
                {
                    $field .= ",nns_director";
                    $values .= ",'" . $vod_info ["director"] . "'";
                }
                if ($vod_info ["actor"] != null)
                {
                    $field .= ",nns_actor";
                    $values .= ",'" . $vod_info ["actor"] . "'";
                }
                if ($vod_info ["show_time"] != null)
                {
                    $field .= ",nns_show_time";
                    $values .= ",'" . $vod_info ["show_time"] . "'";
                }
                if ($vod_info ["view_len"] != null)
                {
                    $field .= ",nns_view_len";
                    $values .= "," . $vod_info ["view_len"];
                }
                if ($vod_info ["all_index"] != null)
                {
                    $field .= ",nns_all_index";
                    $values .= "," . $vod_info ["all_index"];
                }

                if (!empty($vod_info ["new_index"]))
                {
                    $field .= ",nns_new_index";
                    $values .= "," . $vod_info ["new_index"];
                }
                if (!empty($vod_info ["area"]))
                {
                    $field .= ",nns_area";
                    $values .= ",'" . $vod_info ["area"] . "'";
                }
                if (!empty($vod_info ["vod_part"]))
                {
                    $field .= ",nns_vod_part";
                    $values .= ",'" . $vod_info ["vod_part"] . "'";
                }
                if (!empty($vod_info ["play_role"]))
                {
                    $field .= ",nns_play_role";
                    $values .= ",'" . $vod_info ["play_role"] . "'";
                }
                if (!empty($vod_info ["eng_name"])) {
                    $field .= ",nns_eng_name";
                    $values .= ",'" . $vod_info ["eng_name"] . "'";
                }
                if (!empty($vod_info ["screenwriter"]))
                {
                    $field .= ",nns_screenwriter";
                    $values .= ",'" . $vod_info ["screenwriter"] . "'";
                }
                if (!empty($vod_info ["alias_name"]))
                {
                    $field .= ",nns_alias_name";
                    $values .= ",'" . $vod_info ["alias_name"] . "'";
                }
                if (!empty($vod_info ["producer"]))
                {
                    $field .= ",nns_producer";
                    $values .= ",'" . $vod_info ["producer"] . "'";
                }
                if (!empty($vod_info ["language"]))
                {
                    $field .= ",nns_language";
                    $values .= ",'" . $vod_info ["language"] . "'";
                }
                if (!empty($vod_info ["nns_keyword"]))
                {
                    $keyword = $vod_info ["nns_keyword"];
                }

                if (!empty($vod_info ["kind"]))
                {
                    $field .= ",nns_kind";
                    $values .= ",'" . $vod_info ["kind"] . "'";
                }
                if (!empty($vod_info ["text_lang"]))
                {
                    $field .= ",nns_text_lang";
                    $values .= ",'" . $vod_info ["text_lang"] . "'";
                }
                if (isset($vod_info ["nns_play_count"]))
                {
                    $field .= ",nns_play_count";
                    $values .= ",'" . (int) $vod_info["nns_play_count"] . "'";
                }

                if (isset($vod_info ["copyright"]))
                {
                    $field .= ",nns_copyright";
                    $values .= ",'" . $vod_info ["copyright"] . "'";

                }
                if (isset($vod_info ["copyright_range"]))
                {
                    $field .= ",nns_copyright_range";
                    $values .= ",'" . $vod_info ["copyright_range"] . "'";
                }
                if (isset($vod_info ["copyright_begin_date"]))
                {
                    $field .= ",nns_copyright_begin_date";
                    $values .= ",'" . $vod_info ["copyright_begin_date"] . "'";
                }
                if (isset($vod_info ["score_total"]))
                {
                    $field .= ",nns_score_total";
                    $values .= ",'" . $vod_info ["score_total"] . "'";
                }
                if (isset($vod_info ["ishuaxu"]))
                {
                    $field .= ",nns_ishuaxu";
                    $values .= ",'" . $vod_info ["ishuaxu"] . "'";
                    //$field .= ",nns_positiveoriginalid";
                    //$values .= ",'" . $vod_info ["positiveoriginalid"] . "'";
                }
            }
        } //vodinfo  end if


        if ($summary != null) {
            $field .= ",nns_summary";
            $values .= ",'$summary'";
        }
        if ($remark != null) {
            $field .= ",nns_remark";
            $values .= ",'$remark'";
        }
        if ($nns_import_source != null) {
            $field .= ",nns_import_source";
            $values .= ",'$nns_import_source'";
        }
        if ($nns_producer != null) {
            $field .= ",nns_producer";
            $values .= ",'$nns_producer'";
        }
        $field .= ",nns_create_time,nns_modify_time,nns_deleted,nns_keyword";
        $values .= ",now(),now()," . NNS_VOD_DELETED_FALSE . ",'$keyword'";

        if ($nns_pinyin != null) {
            $field .=",nns_pinyin";
            $values .=",'$nns_pinyin' ";
        }


        //添加状态和动作

        $field .= ",nns_status,nns_action";
        $values .=",1,'add'";

        // vod
        $str_sql .= "(" . $field . ")values(" . $values . ")";

        $result = $this->db_obj->nns_db_execute($str_sql);
        if ($result ["ret"] == 0){
            $data ["id"] = $id;
            $result = $this->db_obj->nns_db_result(NNS_ERROR_OK, 'vod add ok', $data);
            //福建移动
            $g_inject = g_cms_config::get_g_config_value('g_inject_enabled');
            if($g_inject){
                $queue_task_model = new queue_task_model();
                $queue_task_model->_set_weight($this->_get_weight());
                $queue_task_model->q_add_task_op_mgtv($id, BK_OP_VIDEO, BK_OP_ADD,$this->message_id,true,$this->conf_info);
                unset($queue_task_model);
            }


            //			加入标签

            if (!empty($vod_info['show_time'])) {
                $vod_info['year'] = substr(trim($vod_info["show_time"]), 0, 4);
            }

            //			新增首字母标签类型
            if ($nns_pinyin != null) {
                $vod_info['initial'] = substr($nns_pinyin, 0, 1);
            }

            $label_inst = new nns_label();
            $label_inst->add_label(
                $id, 0, $vod_info
            );

            if ($check == 1) {
                $this->nns_db_vod_check($id, $check);
            }
        }



        return $result;
    }

    public function nns_db_asset_line($video_id,$action,$import_id,$cp_id,$category_id,$order='0',$asset_type='video',$bk_cp_id = '',$asset_id)
    {
        $g_inject = g_cms_config::get_g_config_value('g_inject_enabled');
        if($g_inject){
            $queue_task_model = new queue_task_model();
            $queue_task_model->_set_weight($this->_get_weight());
            $queue_task_model->_set_is_group($this->_set_is_group());
            //$order =0;
            return $queue_task_model->q_add_unline_op_mgtv($video_id, $asset_type, $action, $this->message_id,$import_id,$order,$cp_id,$category_id,$asset_type,$bk_cp_id,$asset_id);
            unset($queue_task_model);
        }
        return array(
            "ret" => 0,
            "reason" => "操作成功,但是注入队列开关关闭"
        );
    }

    public function nns_db_vod_move_depot_category($id, $depot_id = null, $depot_detail_id = null) {
        $result = array();
        if (!isset($id)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod modify param error');
            return $result;
        }
        if (empty($id)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod modify param error');
            return $result;
        }
        //id exist
        $str_sql = "select 1 from nns_vod where nns_id='$id'";
        $result = $this->db_obj->nns_db_query($str_sql);
        if ($result [ret] != 0) {
            return $result;
        }
        $point = (int) $point;
        if ($this->db_obj->num_rows > 0) {
            //update
            $str_sql = "update nns_vod set nns_depot_id='$depot_id',nns_category_id='$depot_detail_id' ";
            $str_sql .= " where nns_id='$id'";
            $result = $this->db_obj->nns_db_execute($str_sql);
            //org
        }
        return $result;
    }

    //modify
    /**
     *
     * 修改VOD信息 ...
     * @param string $id vodid GUID
     * @param string $name 名称
     * @param int $view_type 电视剧或电影
     * @param array(4) $catogery 栏目
     * @param int $org_type 机构类型
     * @param string $org_id 机构id
     * @param array(32) $tag CP/SP标记
     * @param array(4) $vod_info 影视信息
     * @param string $summary 简介
     * @param string $remark 备注
     */
    public function nns_db_vod_modify($id, $name, $view_type = 1, $depot_id = null, $depot_detail_id = null, $org_type = 0, $org_id = null, $tag = null, $vod_info = null, $summary = null, $point = null, $copyright_date = null, $remark = null, $nns_pinyin = null, $nns_pinyin_length = '0',$nns_cp_id = null) {
        //	修改影片信息输入
        foreach ($vod_info as $info_key => $info_value) {
            if ($info_key == 'actor' || $info_key == 'director') {
                $space = false;
            } else {
                $space = true;
            }
            if (!empty($info_value)) {
                $vod_info[$info_key] = np_build_split_string($info_value, '/', $space);
            }
        }

        $result = array();
        if (!isset($id) || !isset($name)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod modify param error');
            return $result;
        }
        if (empty($id) || empty($name)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod modify param error');
            return $result;
        }

        //id exist
        $str_sql = "select 1 from nns_vod where nns_id='$id'";
        $result = $this->db_obj->nns_db_query($str_sql);
        if ($result ['ret'] != 0) {
            return $result;
        }
        if (!empty($copyright_date)) {
            $time_str.=",nns_copyright_date='$copyright_date' ";
        }
        $point = (int) $point;
        if ($this->db_obj->num_rows > 0) {
            //update
            $str_sql = "update nns_vod set nns_deleted=0, nns_name='$name',nns_pinyin_length='$nns_pinyin_length',nns_view_type=$view_type,nns_depot_id='$depot_id',nns_category_id='$depot_detail_id',nns_point=$point" . $time_str;
            //org
            if ($org_type !== null && $org_type !== '') {
                $org_type = (int) $org_type;
                $str_sql .= ",nns_org_type=$org_type";
            }
            if ($org_id != null) {
                $str_sql .= ",nns_org_id='$org_id'";
            }
            if(strlen($nns_cp_id) > 0)
            {
                //$nns_cp_id = intval($nns_cp_id);
                $str_sql .= ",nns_cp_id='$nns_cp_id'";
            }
            else
            {
                $str_sql .= ",nns_cp_id='0'";
            }
            /**
             * 原tag存储方式废除，改为单个字段存储标识 BY S67
             */
            //			//tag
            //			if (! empty ( $tag )) {
            //				if (is_array ( $tag )) {
            //					for($i = 0; $i < count ( $tag ); $i ++) {
            //						$temp = "tag" . $i;
            //						if ($tag [$i] !== null && $tag [$i] !== '') {
            //							$tag_value = ( int ) $tag [$i];
            //							$str_sql .= ",nns_" . $temp . "=" . $tag_value;
            //						}
            //
            //					} //for
            //				}
            //
            //			} // tag end if

            /**
             * 新tag存储方式 BY S67
             */
            if (!empty($tag)) {
                if (is_array($tag)) {
                    $len = count($tag);
                    $str_sql .= ",nns_tag = ";
                    $tag_value = '';
                    for ($i = 0; $i < $len; $i++) {
                        if (!empty($tag [$i])) {
                            $tag_value .= $i . ',';
                        }
                    } //for
                    $str_sql .= "'$tag_value'";
                }
            }
            //vod_info
            if (!empty($vod_info)) {
                if (is_array($vod_info)) {
                    if ($vod_info ["director"] !== null) {
                        $str_sql .= ",nns_director='" . $vod_info ["director"] . "'";
                    }
                    if ($vod_info ["actor"] !== null) {
                        $str_sql .= ",nns_actor='" . $vod_info ["actor"] . "'";
                    }
                    if (!empty($vod_info ["show_time"])) {
                        $str_sql .= ",nns_show_time='" . $vod_info ["show_time"] . "'";
                    }
                    if ($vod_info ["view_len"] !== null) {
                        $str_sql .= ",nns_view_len=" . $vod_info ["view_len"];
                    }
                    if ($vod_info ["all_index"] !== null) {
                        $str_sql .= ",nns_all_index=" . $vod_info ["all_index"];
                    }
                    if ($vod_info ["new_index"] !== null) {
                        $str_sql .= ",nns_new_index=" . $vod_info ["new_index"];
                        ;
                    }
                    if ($vod_info ["area"] !== null) {
                        $str_sql .= ",nns_area='" . $vod_info ["area"] . "'";
                    }

                    if ($vod_info ["vod_part"] !== null) {
                        $str_sql .= ",nns_vod_part='" . $vod_info ["vod_part"] . "'";
                    }
                    if ($vod_info ["play_role"] !== null) {

                        $str_sql .= ",nns_play_role='" . $vod_info ["play_role"] . "'";
                    }
                    if ($vod_info ["eng_name"] !== null) {
                        $str_sql .= ",nns_eng_name='" . $vod_info ["eng_name"] . "'";
                    }
                    if ($vod_info ["screenwriter"] !== null) {
                        $str_sql .= ",nns_screenwriter='" . $vod_info ["screenwriter"] . "'";
                    }
                    if ($vod_info ["alias_name"] !== null) {
                        $str_sql .= ",nns_alias_name='" . $vod_info ["alias_name"] . "'";
                    }
                    if ($vod_info ["producer"] !== null) {
                        $str_sql .= ",nns_producer='" . $vod_info ["producer"] . "'";
                    }
                    if ($vod_info ["language"] !== null) {
                        $str_sql .= ",nns_language='" . $vod_info ["language"] . "'";
                    }
                    if ($vod_info ["nns_keyword"] !== null) {
                        $str_sql .= ",nns_keyword='" . $vod_info ["nns_keyword"] . "'";
                    }
                    if (!empty($vod_info ["kind"])) {
                        $str_sql .= ",nns_kind='" . $vod_info ["kind"] . "'";
                    }
                    if (!empty($vod_info ["text_lang"])) {
                        $str_sql .= ",nns_text_lang='" . $vod_info ["text_lang"] . "'";
                    }
                    if (isset($vod_info ["nns_play_count"])) {
                        $str_sql .= ",nns_play_count='" . $vod_info ["nns_play_count"] . "'";
                    }

                    if (isset($vod_info ["copyright"])) {
                        $str_sql .= ",nns_copyright='" . $vod_info ["copyright"] . "'";
                    }
                    if (isset($vod_info ["score_total"])) {
                        $str_sql .= ",nns_score_total='" . $vod_info ["score_total"] . "'";
                    }
                    if (isset($vod_info ["copyright_range"]))
                    {
                        $str_sql .= ",nns_copyright_range='" . $vod_info ["copyright_range"] . "'";
                    }
                    if (isset($vod_info ["copyright_begin_date"]))
                    {
                        $str_sql .= ",nns_copyright_begin_date='" . $vod_info ["copyright_begin_date"] . "'";
                    }
                    if (isset($vod_info ["ishuaxu"]))
                    {
                        $str_sql .= ",nns_ishuaxu='" . $vod_info ["ishuaxu"] . "'";
                        //$str_sql .= ",nns_positiveoriginalid='" . $vod_info ["positiveoriginalid"] . "'";
                    }
                }
            } //vodinfo  end if


            if ($summary != null) {
                $str_sql .= ",nns_summary='$summary'";
            }
            if ($remark != null) {
                $str_sql .= ",nns_remark='$remark'";
            }
            if ($nns_pinyin != null) {
                $str_sql .= ",nns_pinyin='$nns_pinyin'";
            }


            $str_sql .= ",nns_modify_time=now() ";
            $str_sql .= ",nns_state=0 ";
            $str_sql .= ",nns_status=2, nns_action='modify'";
            $str_sql .= " where nns_id='$id'";
            $result = $this->db_obj->nns_db_execute($str_sql);

            //			修改相应媒资包，服务包绑定数据
            if ($result["ret"] == 0) {

                //die('sssssssss');
                //福建移动
                $g_inject = g_cms_config::get_g_config_value('g_inject_enabled');
                if($g_inject){
                    $queue_task_model = new queue_task_model();
                    $queue_task_model->_set_weight($this->_get_weight());
                    $queue_task_model->q_add_task_op_mgtv($id, BK_OP_VIDEO, BK_OP_MODIFY,$this->message_id,true,$this->conf_info);
                    unset($queue_task_model);
                    //category_content_model::_task_c2_v2($id, BK_OP_VIDEO, BK_OP_MODIFY);
                }

                /*                                $str_sql = "update nns_assists_item set " .
                                                        "nns_point=$point" .
                                                        ",nns_video_name='$name'" .
                                                        ",nns_pinyin='$nns_pinyin' " .
                                                        ",nns_english_index='" . $vod_info ["eng_name"] . "' " .
                                                        ",nns_tag='$tag_value' " .
                                                        "where nns_video_id='$id'";

                                                $result = $this->db_obj->nns_db_execute($str_sql);
                                                $str_sql = "update nns_service_item set nns_point=$point,nns_video_name='$name' where nns_video_id='$id'";
                                                $result = $this->db_obj->nns_db_execute($str_sql);
                                                $str_sql = "update nns_service_detail_video set nns_video_name='$name',nns_point=$point where nns_video_id='$id'";
                                                $result = $this->db_obj->nns_db_execute($str_sql);

                                                $this->clear_memcache_by_asset_service($id);*/


                //			加入标签
                if (!empty($vod_info['show_time'])) {
                    $vod_info['year'] = substr(trim($vod_info["show_time"]), 0, 4);
                }
                //	  新增首字母标签类型
                if ($nns_pinyin != null) {
                    $vod_info['initial'] = substr($nns_pinyin, 0, 1);
                }

                $label_inst = new nns_label();
                $label_inst->delete_label(
                    $id, 0
                );
                $label_inst->add_label(
                    $id, 0, $vod_info
                );
                //				die;
            }
        } else {
            //insert
        } //end if
        return $result;
    }

    private function nns_db_vod_modify_deleted($id, $deleted) {
        $result = array();
        if (!isset($id)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'live delete param error');
            return $result;
        }

        if (empty($id)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'live delete param error');
            return $result;
        }
        include_once 'nns_db_vod_index_class.php';
        //		$str_sql = "delete from nns_vod_media where nns_vod_id='$id'";
        //		$result = $this->db_obj->nns_db_execute ( $str_sql );
        //
        //		$str_sql = "delete from nns_vod_index where nns_vod_id='$id'";
        //		$result = $this->db_obj->nns_db_execute ( $str_sql );
        //      循环删除分集
        $bool = true;
        if ($bool) {
            $str_sql = "select nns_id from nns_vod_index where nns_vod_id='$id'";
            $vod_index_data = $this->db_obj->nns_db_query($str_sql);
            if (is_array($vod_index_data['data'])) {
                $vod_index_inst = new nns_db_vod_index_class();
                foreach ($vod_index_data['data'] as $vod_index_item) {
                    $vod_index_inst->nns_db_vod_index_delete($vod_index_item['nns_id']);
                }
                $vod_index_inst = NULL;
            }
        }
        //      删除分集完成，删除影片
        //福建移动
        $g_inject = g_cms_config::get_g_config_value('g_inject_enabled');
        if($g_inject)
        {
            //广西删除逻辑不生成队列
            $g_delete_enable_center_import_v2 = get_config_v2('g_delete_enable_center_import_v2');
            if($g_delete_enable_center_import_v2 != 1)
            {
                $queue_task_model = new queue_task_model();
                $queue_task_model->_set_weight($this->_get_weight());
                $queue_task_model->q_add_task_op_mgtv($id, BK_OP_VIDEO, BK_OP_DELETE, $this->message_id);
                unset($queue_task_model);
            }
        }

        $str_sql = "update nns_vod set nns_deleted=1,nns_status=3,nns_action='destroy',nns_modify_time=now() where nns_id='$id'";
        $result = $this->db_obj->nns_db_execute($str_sql);
        //		删除标签库
        $label_inst = new nns_label();
        $label_inst->delete_label(
            $id, 0
        );

        return $result;
    }

    /**
     * 资源库数据恢复
     * @param string $id guid
     * @return multitype:
     * @author liangpan
     * @date 2016-04-15
     */
    private function nns_db_vod_modify_retrieve($id,$nns_type,$nns_queue_action)
    {
        $result = array ();
        if (!isset($id))
        {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'cod retrieve param error');
            return $result;
        }

        if (empty($id))
        {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod retrieve param error');
            return $result;
        }
        $g_inject = g_cms_config::get_g_config_value('g_inject_enabled');
        //广西删除逻辑不生成队列
        $g_delete_enable_center_import_v2 = get_config_v2('g_delete_enable_center_import_v2');
        $arr_modify = array();
        //添加类型判断 xingcheng.hu
        switch($nns_type)
        {
            case "video":
                $arr_modify[BK_OP_VIDEO][]=$id;
                break;
            case "index":
                $arr_modify[BK_OP_INDEX][]=$id;
                break;
            case "media":
                $arr_modify[BK_OP_MEDIA][]=$id;
                break;
        }
        if($nns_queue_action == 'add' || $nns_queue_action =='modify')
        {
            $str_action_del = " and nns_deleted =1 ";
            $str_state_del = '0';
        }
        else
        {
            $str_action_del = " and nns_deleted !=1 ";
            $str_state_del = '1';
        }
        if (isset($arr_modify[BK_OP_VIDEO]) && !empty($arr_modify[BK_OP_VIDEO]))
        {
            $sql_index = "select nns_id from nns_vod_index where nns_vod_id='{$id}' {$str_action_del} ";
            $vod_index_data = $this->db_obj->nns_db_query($sql_index);
            foreach ($vod_index_data['data'] as $vod_index_item)
            {
                $arr_modify[BK_OP_INDEX][]=$vod_index_item['nns_id'];
            }
        }
        if(isset($arr_modify[BK_OP_INDEX]) && !empty($arr_modify[BK_OP_INDEX]))
        {
            $sql_media = "select nns_id from nns_vod_media where nns_vod_index_id in('".implode("','", $arr_modify[BK_OP_INDEX])."') {$str_action_del}";
            $vod_index_media_data = $this->db_obj->nns_db_query($sql_media);
            if (is_array($vod_index_media_data['data']))
            {
                foreach ($vod_index_media_data['data'] as $vod_index_media_item)
                {
                    $arr_modify[BK_OP_MEDIA][]=$vod_index_media_item['nns_id'];
                }
            }
        }
        $queue_task_model = new queue_task_model();
        foreach ($arr_modify as $key=>$val)
        {
            switch ($key)
            {
                case BK_OP_VIDEO:
                    $str_table ='nns_vod';
                    break;
                case BK_OP_INDEX:
                    $str_table ='nns_vod_index';
                    break;
                case BK_OP_MEDIA:
                    $str_table ='nns_vod_media';
                    break;
            }
            $sql_video_retrieve = "update {$str_table} set nns_deleted='{$str_state_del}',nns_status=0,nns_action='{$nns_queue_action}',nns_modify_time=now() where nns_id in('" . implode("','", $val) . "')";
            $result = $this->db_obj->nns_db_execute($sql_video_retrieve);
            if($g_inject && $g_delete_enable_center_import_v2 != 1)
            {
                foreach ($val as $_v)
                {
                    $queue_task_model->_set_weight($this->_get_weight());
                    $queue_task_model->q_add_task_op_mgtv($_v, $key, $nns_queue_action,null,false);
                }
            }
        }
        return $result;
    }

    public function nns_db_vod_real_delete($id, $bool = TRUE) {
        $result = array();
        if (!isset($id)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'live delete param error');
            return $result;
        }

        if (empty($id)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'live delete param error');
            return $result;
        }
        include_once 'nns_db_vod_index_class.php';
        //		$str_sql = "delete from nns_vod_media where nns_vod_id='$id'";
        //		$result = $this->db_obj->nns_db_execute ( $str_sql );
        //
        //		$str_sql = "delete from nns_vod_index where nns_vod_id='$id'";
        //		$result = $this->db_obj->nns_db_execute ( $str_sql );
        //      循环删除分集
        //if ($bool) {
        $str_sql = "select nns_id from nns_vod_index where nns_vod_id='$id' and nns_deleted!=1";
        $vod_index_data = $this->db_obj->nns_db_query($str_sql);
        if (is_array($vod_index_data['data'])) {
            $vod_index_inst = new nns_db_vod_index_class();
            $vod_index_inst->message_id = $this->message_id;
            if(is_array($vod_index_data['data']))
            {
                foreach ($vod_index_data['data'] as $vod_index_item) {
                    $vod_index_inst->nns_db_vod_index_delete($vod_index_item['nns_id']);
                }
            }
            $vod_index_inst = NULL;
        }
        //}
        //      删除分集完成，删除影片
        //福建移动
        $g_inject = g_cms_config::get_g_config_value('g_inject_enabled');
        if($g_inject){
            //广西删除逻辑不生成队列
            $g_delete_enable_center_import_v2 = get_config_v2('g_delete_enable_center_import_v2');
            if($g_delete_enable_center_import_v2 != 1)
            {
                $queue_task_model = new queue_task_model();
                $queue_task_model->_set_weight($this->_get_weight());
                $queue_task_model->_set_is_group($this->_get_is_group());
                $queue_task_model->q_add_task_op_mgtv($id, BK_OP_VIDEO, BK_OP_DELETE, $this->message_id);
                //error_log($id.'-----video'.PHP_EOL,3,'txt.log');
                unset($queue_task_model);
                //category_content_model::_task_c2_v2($id, BK_OP_VIDEO, BK_OP_DELETE);
            }

        }
        //$str_sql = "delete from  nns_vod  where nns_id='$id'";
        // $result = $this->db_obj->nns_db_execute($str_sql);

        $str_sql = "update nns_vod set nns_deleted=1,nns_status=3,nns_action='destroy',nns_modify_time=now() where nns_id='$id'";
        $result = $this->db_obj->nns_db_execute($str_sql);
        //		删除标签库
        $label_inst = new nns_label();
        $label_inst->delete_label(
            $id, 0
        );

        return $result;
    }

    //delete
    /**
     *
     * 删除VOD
     * @param string $id vodid GUID
     */
    public function nns_db_vod_delete($id) {
        return $this->nns_db_vod_modify_deleted($id, NNS_VOD_DELETED_TRUE);
    }

    //retrieve
    /**
     *
     * 恢复vod/index/media
     * @param string $id vodid GUID
     * @param string $nns_type video index media 恢复媒资类型
     */
    public function nns_db_vod_retrieve($id,$nns_type='video',$nns_queue_action='modify') {
        return $this->nns_db_vod_modify_retrieve($id,$nns_type,$nns_queue_action);
    }

    //upload images
    /**
     *
     * 上传VOD海报
     * @param string $id vodid GUDI
     * @param string $images 海报存储路径
     */
    public function nns_db_vod_upload_images($id, $images) {
        $result = array();
        if (!isset($id)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod upload param error');
            return $result;
        }
        if (empty($id)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod upload param error');
            return $result;
        }
        if ($images == null) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod upload param error');
            return $result;
        }
        if (!is_array($images)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod upload param error');
            return $result;
        }

        //exist
        $str_sql = "select 1 from nns_vod where nns_id='$id'";
        $result = $this->db_obj->nns_db_query($str_sql);
        if ($result ["ret"] != 0) {
            return $result;
        }
        if ($this->db_obj->num_rows > 0) {
            //update
            $str_sql = "";
            $str_sql_asset = '';
            for ($i = 0; $i < count($images); $i++) {
                $temp = "image" . $i;
                if ($images [$temp] != null) {
                    if (empty($str_sql)) {
                        $str_sql .= "nns_" . $temp . "='" . $images [$temp] . "'";
                        $str_sql_asset .= "nns_video_img" . $i . "='" . $images [$temp] . "'";
                    } else {
                        $str_sql .= ",nns_" . $temp . "='" . $images [$temp] . "'";
                        $str_sql_asset .= ",nns_video_img" . $i . "='" . $images [$temp] . "'";
                    }
                }
            } //for
            //sql
            if ($str_sql == null) {
                $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod upload param error');
                return $result;
            }
            $str_sql = "update nns_vod set " . $str_sql . " ,nns_modify_time=now() where nns_id='$id'";

            $result = $this->db_obj->nns_db_execute($str_sql);

            //			修改相应媒资包海报 BY S67 2012-9-9
            if ($result["ret"] == 0) {
                $str_sql = "update nns_assists_item set " . $str_sql_asset . " where nns_video_id='$id'";
                $result = $this->db_obj->nns_db_execute($str_sql);
            }
        } else {
            //insert
            $str_sql = " insert into nns_vod ";
            $filed = "nns_id,nns_state,nns_create_time,nns_modify_time";
            $value = "'$id'," . NNS_VOD_STATE_WAITING . ",now(),now()";
            for ($i = 0; $i < count($images); $i++) {
                $temp = "image" . $i;
                if ($images [$temp] != null) {
                    $filed .= ",nns_" . $temp;
                    $value .= ",'" . $images [$temp] . "'";
                } //for
            }
            //sql
            if ($filed == null) {
                $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod upload param error');
                return $result;
            }
            $str_sql .= "( " . $filed . ")values(" . $value . ")";
            $result = $this->db_obj->nns_db_execute($str_sql);
        } //end if else
        return $result;
    }

    //edit images
    /**
     *
     * 修改VOD海报 ...
     * @param string $id vodid GUDI
     * @param string $images 海报存储路径
     */
    public function nns_db_vod_modify_images($id, $images) {
        $result = array();
        if (!isset($id)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod upload param error');
            return $result;
        }
        if (empty($id)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod upload param error');
            return $result;
        }
        if ($images == null) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod upload param error');
            return $result;
        }
        if (!is_array($images)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod upload param error');
            return $result;
        }

        //exist
        $str_sql = "select 1 from nns_vod where nns_id='$id'";
        $result = $this->db_obj->nns_db_query($str_sql);
        if ($result ["ret"] != 0) {
            return $result;
        }
        if ($this->db_obj->num_rows > 0) {
            //update
            $str_sql = "";
            $str_sql_asset = '';
            for ($i = 0; $i < count($images); $i++) {
                $temp = "image" . $i;
                if ($images [$temp] != null) {
                    if (empty($str_sql)) {
                        $str_sql .= "nns_" . $temp . "='" . $images [$temp] . "'";
                        $str_sql_asset .= "nns_video_img" . $i . "='" . $images [$temp] . "'";
                    } else {
                        $str_sql .= ",nns_" . $temp . "='" . $images [$temp] . "'";
                        $str_sql_asset .= ",nns_video_img" . $i . "='" . $images [$temp] . "'";
                    }
                }
            } //for
            //添加竖图
            if($images['image_v'] != null)
            {
                if (empty ( $str_sql )) {
                    $str_sql .= "nns_image_v='" . $images['image_v'] . "'";
                    $str_sql_asset .= "nns_image_v='" . $images['image_v'] . "'";
                } else {
                    $str_sql .= ",nns_image_v='" . $images['image_v'] . "'";
                    $str_sql_asset .= ",nns_image_v='" . $images['image_v'] . "'";
                }
            }

            //添加横图
            if($images['image_h'] != null)
            {
                if (empty ( $str_sql )) {
                    $str_sql .= "nns_image_h='" . $images['image_h'] . "'";
                    $str_sql_asset .= "nns_image_h='" . $images['image_h'] . "'";
                } else {
                    $str_sql .= ",nns_image_h='" . $images['image_h'] . "'";
                    $str_sql_asset .= ",nns_image_h='" . $images['image_h'] . "'";
                }
            }
            //添加方图
            if($images['image_s'] != null)
            {
                if (empty ( $str_sql )) {
                    $str_sql .= "nns_image_s='" . $images['image_s'] . "'";
                    $str_sql_asset .= "nns_image_s='" . $images['image_s'] . "'";
                } else {
                    $str_sql .= ",nns_image_s='" . $images['image_s'] . "'";
                    $str_sql_asset .= ",nns_image_s='" . $images['image_s'] . "'";
                }
            }
            //sql
            if (!empty($str_sql)) {
                $str_sql = "update nns_vod set " . $str_sql . " ,nns_modify_time=now() where nns_id='$id'";
                $result = $this->db_obj->nns_db_execute($str_sql);
            } else {
                $result["ret"] = 0;
                return $result;
            }
        } else {
            //insert
            $str_sql = " insert into nns_vod ";
            $filed = "nns_id,nns_state,nns_create_time,nns_modify_time";
            $value = "'$id'," . NNS_VOD_STATE_WAITING . ",now(),now()";
            for ($i = 0; $i < count($images); $i++) {
                $temp = "image" . $i;
                if ($images [$temp] != null) {
                    $filed .= ",nns_" . $temp;
                    $value .= ",'" . $images [$temp] . "'";
                } //for
            }
            //sql
            if ($filed == null) {
                $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod upload param error');
                return $result;
            }
            $str_sql .= "( " . $filed . ")values(" . $value . ")";
            $result = $this->db_obj->nns_db_execute($str_sql);
        } //end if else


        return $result;
    }

    //image info
    /**
     *
     * VOD海报信息 ...
     * @param string $id
     */
    public function nns_db_vod_images_info($id) {
        $result = array();
        if (!isset($id)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod upload param error');
            return $result;
        }
        if (empty($id)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod upload param error');
            return $result;
        }
        $str_sql = "select nns_id,nns_name, nns_image0,nns_image1,nns_image2,nns_image3,nns_image4,nns_image5 from nns_vod where nns_id='$id' limit 1";
        $result = $this->db_obj->nns_db_query($str_sql);
        if ($result ["ret"] == 0) {
            if ($this->db_obj->num_rows <= 0) {
                $result = $this->db_obj->nns_db_result(NNS_ERROR_VOD_NOT_FOUND, 'vod images info not find');
            }
        }

        return $result;
    }

    //state
    /**
     *
     * Enter description here ...
     * @param string $id
     * @param int $state
     */
    public function nns_db_vod_state($id, $state) {
        $result = array();
        if (!isset($id) || !isset($state)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod modify state param error');
            return $result;
        }
        if (empty($id)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod modify state param error');
            return $result;
        }
        $state = (int) $state;
        if ($state == NNS_VOD_AND_MEDIA_STATE_ONLINE) {
            $str_sql = "update nns_vod_media set nns_state=" . NNS_VOD_MEDIA_STATE_ONLINE . " where nns_vod_id='$id'";
            $result = $this->db_obj->nns_db_execute($str_sql);
            if ($result ["ret"] != 0) {
                return $result;
            }
        }

        $str_sql = "update nns_vod set nns_state=" . $state . " where nns_id='$id'";
        $result = $this->db_obj->nns_db_execute($str_sql);

        return $result;
    }

    //check
    public function nns_db_vod_check($id, $check) {

        $result = array();
        if (!isset($id) || !isset($check)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod modify check param error');
            return $result;
        }
        if (empty($id)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod modify check param error');
            return $result;
        }
        if ($check === null && $check === '') {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'vod modify check param error');
            return $result;
        }
        $check = (int) $check;
        if ($check == NNS_VOD_AND_MEDIA_CHECK_OK) {
            $str_sql = "update nns_vod_media set nns_check=" . NNS_VOD_MEDIA_CHECK_OK . " where nns_vod_id='$id'";
            $result = $this->db_obj->nns_db_execute($str_sql);
            if ($result ["ret"] != 0) {
                return $result;
            }
        }

        $str_sql = "update nns_vod set nns_check=" . $check . " where nns_id='$id'";
        $result = $this->db_obj->nns_db_execute($str_sql);
        if ($result["ret"] == 0) {

            //			同步媒资库绑定栏目
            if ($check == NNS_VOD_AND_MEDIA_CHECK_OK || $check == NNS_VOD_MEDIA_CHECK_OK) {
                $result_info_arr = $this->nns_db_vod_info($id, "nns_pinyin,nns_eng_name,nns_play_count,nns_tag,nns_depot_id,nns_category_id,nns_name,nns_point,nns_image0,nns_image1,nns_image2,nns_image3,nns_image4,nns_image5");
                if ($result_info_arr["ret"] == 0) {
                    $result_info = $result_info_arr["data"][0];
                    if ($result_info)
                        $depot_id = $result_info["nns_depot_id"];
                    $depot_detail_id = $result_info["nns_category_id"];
                    $name = $result_info["nns_name"];
                    $point = $result_info["nns_point"];
                    $img1 = $result_info["nns_image0"];
                    $img2 = $result_info["nns_image1"];
                    $img3 = $result_info["nns_image2"];
                    $img4 = $result_info["nns_image3"];
                    $img5 = $result_info["nns_image4"];
                    $img6 = $result_info["nns_image5"];
                    $nns_pinyin = $result_info["nns_pinyin"];
                    $nns_eng_name = $result_info["nns_eng_name"];
                    $nns_play_count = $result_info["nns_play_count"];
                    $nns_tag = $result_info["nns_tag"];
                }
                $point = (int) $point;
                $assest_inst = new nns_db_assist_bind_class();
                $assest_result = $assest_inst->nns_db_assest_bind_video_category_list(null, null, $depot_id, $depot_detail_id);
                $assest_inst = null;

                if (count($assest_result["data"]) > 0) {
                    foreach ($assest_result["data"] as $assest_item) {

                        $nns_assist_id = $assest_item["nns_assest_category_id"];
                        if ($nns_assist_id == "0")
                            $nns_assist_id = 1000;
                        //清除媒资包缓存
                        $this->clear_memcache('asset', $assest_item["nns_assest_id"], $nns_assist_id);

                        $str_sql = "select count(1) as num from nns_assists_item where nns_category_id='" . $nns_assist_id . "' and nns_assist_id='" . $assest_item["nns_assest_id"] . "'";
                        $result_count = $this->db_obj->nns_db_query($str_sql);
                        $num = $result_count["data"][0]["num"];
                        $guid = new Guid();
                        $assest_item_id = $guid->toString();

                        $str_sql = "insert into nns_assists_item(nns_id,nns_video_id,nns_video_type,nns_video_name,nns_point,nns_category_id,nns_assist_id,nns_check,nns_order,nns_video_img0,nns_video_img1,nns_video_img2,nns_video_img3,nns_video_img4,nns_video_img5,nns_pinyin,nns_english_index,nns_tag,nns_play_count)values" .
                            "('$assest_item_id','$id',0,'$name',$point,'" . $nns_assist_id . "','" . $assest_item["nns_assest_id"] . "',$check,$num,'$img1','$img2','$img3','$img4','$img5','$img6','$nns_pinyin','$nns_eng_name','$nns_tag','$nns_play_count')";
                        //						检查注入ID是否为空，若为空，跳过插入 BY S67 2013-4-29
                        if (!empty($id))
                            $result = $this->db_obj->nns_db_execute($str_sql);
                    }
                }
            }
        }
        return $result;
    }

    //where
    public function nns_db_vod_where($name = null, $state = NNS_VOD_MEDIA_STATE_WAITING, $check = NNS_VOD_MEDIA_CHECK_WAITING, $view_type = null, $depot_id = null, $depot_detail_id = null, $org_type = null, $org_id = null, $tag = null, $vod_info = null, $order = 'nns_modify_time', $min = 0, $num = 0, $deleted = NNS_VOD_DELETED_FALSE, $except_method = null, $except_detail_id = null, $except_id = null, $begin_sy_date = null, $end_sy_date = null, $begin_create_date = null, $end_create_date = null, $begin_copyright_date = null, $end_copyright_date = null, $searchpoint = null, $depot_like_detail_id = null, $summary = null, $index_num = null, $min_len = null, $max_len = null, $nns_pinyin = null, $nns_no_pic = null, $nns_ids_arr = null) {
        $where = "";

        if ($except_method == "assist") {
            $where .=" where nns_id not in (select nns_video_id from nns_assists_item where nns_assist_id='$except_detail_id' and nns_category_id='$except_id' and nns_video_type=0)";
        } else if ($except_method == "service") {
            $where .=" where nns_id not in (select nns_video_id from nns_service_detail_video where nns_service_category_id='$except_detail_id' and nns_video_type=0)";
        } else if ($except_method == "ad") {
            $where .=" where nns_id not in (select nns_video_id from nns_ad_content_item where nns_ad_id='$except_id' and nns_type='video')";
        } else if ($except_method == "ad_policy") {
            $where .=" where nns_id not in (select nns_video_id from nns_ad_postion_policy where nns_ad_postion_id='$except_id')";
        } else if ($except_method == "weight") { //权重查询VOD 不与 表weight有重复数据
            $where .=" where nns_id not in (select nns_video_id from nns_mgtvbk_op_weight where nns_org_id='$except_id')";
        }

        if (is_array($nns_ids_arr)) {
            $id_str = '';
            foreach ($nns_ids_arr as $item) {
                if ($item)
                    $id_str .= "'" . $item . "',";
            }
            $id_str = rtrim($id_str, ',');
            $in_str = "nns_id in($id_str)";
            if (empty($where)) {
                $where .= "where $in_str";
            } else {
                $where .= " and $in_str";
            }
        }

        if ($nns_no_pic == "1") {
            if (empty($where)) {
                $where .= " where ifnull(nns_image0,'')='' and ifnull(nns_image1,'')='' and ifnull(nns_image2,'')='' and ifnull(nns_image3,'')='' and ifnull(nns_image4,'')='' and ifnull(nns_image5,'')='' ";
            } else {
                $where .= " and  ifnull(nns_image0,'')='' and ifnull(nns_image1,'')='' and ifnull(nns_image2,'')='' and ifnull(nns_image3,'')='' and ifnull(nns_image4,'')='' and ifnull(nns_image5,'')='' ";
            }
        }

        if (!empty($max_len)) {
            $min_len = (int) $min_len;
            $max_len = (int) $max_len;
            if (empty($where)) {
                $where .= " where  nns_view_len > $min_len and nns_view_len < $max_len";
            } else {
                $where .= " and nns_view_len > $min_len and nns_view_len < $max_len";
            }
        }

        if (!empty($name)) {

            if (is_array($name)) {
                $query = ' (';
                foreach ($name as $k => $v) {
                    if($k == 'nns_asset_import_id')
                    {
                        $query.= " nns_asset_import_id = '{$v}' or";
                    }
                    else
                    {
                        $query.= " {$k} like '%{$v}%' or";
                    }
                }

                $query = rtrim($query, 'or');
                $query.=')';
            } else {
                $query = " nns_name like '%$name%' ";
            }

            if (empty($where)) {
                $where .= " where {$query}";
            } else {
                $where .= " and {$query}";
            }
        }
        if (!empty($nns_pinyin)) {
            if (empty($where)) {
                $where .= " where nns_pinyin like '$nns_pinyin%'";
            } else {
                $where .= " and nns_pinyin like '$nns_pinyin%'";
            }
        }
        if (!empty($index_num)) {
            $index_num = (int) $index_num;
            if (empty($where)) {
                $where .= " where nns_all_index = $index_num";
            } else {
                $where .= " and nns_all_index = $index_num";
            }
        }
        //state
        if ($state !== null && $state !== '') {
            $state = (int) $state;
            if (empty($where)) {
                $where .= " where nns_state=$state";
            } else {
                $where .= " and nns_state=$state";
            }
        }
        //check
        if ($check !== null && $check !== '') {
            $check = (int) $check;
            if (empty($where)) {
                $where .= " where nns_check=$check";
            } else {
                $where .= " and nns_check=$check";
            }
        }
        //view_type
        if ($view_type !== null && $view_type !== '') {
            $view_type = (int) $view_type;
            if (empty($where)) {
                $where .= " where nns_view_type=$view_type";
            } else {
                $where .= " and nns_view_type=$view_type";
            }
        }
        //depot_id
        if (!empty($depot_id)) {
            if (empty($where)) {
                $where .= " where nns_depot_id='$depot_id'";
            } else {
                $where .= " and nns_depot_id='$depot_id'";
            }
        }
        //depot_detail_id
        if (!empty($depot_detail_id)) {
            if (empty($where)) {
                $where .= " where nns_category_id like '$depot_detail_id%'";
            } else {
                $where .= " and nns_category_id like '$depot_detail_id%'";
            }
        }

        //depot_detail_id
        if (!empty($depot_like_detail_id)) {
            if (empty($where)) {
                $where .= " where nns_category_id like '$depot_like_detail_id%'";
            } else {
                $where .= " and nns_category_id like '$depot_like_detail_id%'";
            }
        }



        //org
        if ($org_type !== null && $org_type !== '') {
            $org_type = (int) $org_type;
            if (empty($where)) {
                $where .= " where nns_org_type=$org_type";
            } else {
                $where .= " and nns_org_type=$org_type";
            }
        }
        //org_id
        if (!empty($org_id)) {
            if (empty($where)) {
                $where .= " where nns_org_id='$org_id'";
            } else {
                $where .= " and nns_org_id='$org_id'";
            }
        }

        if (!empty($summary)) {
            if (empty($where)) {
                $where .= " where nns_summary like '%$summary%'";
            } else {
                $where .= " and nns_summary like '%$summary%'";
            }
        }
        /**
         * 废除原TAG查询方式 BY S67
         */
        //		//tag
        //		if (! empty ( $tag )) {
        //			if (is_array ( $tag )) {
        //				$tag_str = null;
        //				for($i = 0; $i < count ( $tag ); $i ++) {
        //					if ($tag [$i] !== null && $tag [$i] !== '') {
        //						$temp = "nns_tag" . $tag [$i];
        //
        //						if (empty ( $tag_str )) {
        //							$tag_str .= "  $temp=1";
        //						} else {
        //							$tag_str .= " or $temp=1";
        //						}
        //					}
        //
        //				} //for
        //				if (! empty ( $tag_str )) {
        //					if (empty ( $where )) {
        //						$where .= " where (" . $tag_str . ")";
        //					} else {
        //						$where .= " and  (" . $tag_str . ")";
        //					}
        //				}
        //			} //end if is array
        //		}

        /**
         * 新增TAG查询方式 BY S67
         */
        if (!empty($tag)) {
            if (is_array($tag)) {
                $tag_str = '';
                for ($i = 0; $i < count($tag); $i++) {
                    if (!empty($tag [$i])) {
                        $tag_str.=$i . ',';
                    }
                } //for
                if (!empty($tag_str)) {
                    $tag_str = ',' . $tag_str;
                    if (empty($where)) {
                        $where .= " where nns_tag like '%" . $tag_str . "%' ";
                    } else {
                        $where .= " and nns_tag like '%" . $tag_str . "%' ";
                    }
                }
            } //end if is array
        }

        //vod_info
        if (!empty($vod_info)) {
            if (is_array($vod_info)) {
                if (!empty($vod_info ["director"])) {
                    if (empty($where)) {
                        $where .= " where nns_director like '%" . $vod_info ["director"] . "%'";
                    } else {
                        $where .= " and nns_director like '%" . $vod_info ["director"] . "%'";
                    }
                }
                if (!empty($vod_info ["actor"])) {
                    if (empty($where)) {
                        $where .= " where nns_actor like '%" . $vod_info ["actor"] . "%'";
                    } else {
                        $where .= " and nns_actor like '%" . $vod_info ["actor"] . "%'";
                    }
                }
                if (!empty($vod_info ["show_time"])) {
                    $temp = date('Y-m-d', strtotime($vod_info ["show_time"]));
                    if (empty($where)) {
                        $where .= " where nns_show_time='" . $temp . "'";
                    } else {
                        $where .= " and nns_show_time='" . $temp . "'";
                    }
                }
                if (!empty($vod_info ["area"])) {
                    if (empty($where)) {
                        $where .= " where nns_area like '%" . $vod_info ["area"] . "%'";
                    } else {
                        $where .= " and nns_area like '%" . $vod_info ["area"] . "%'";
                    }
                }
            }
        }
        //deleted
        if ($deleted != "-1") {
            $deleted = (int) $deleted;
            if (empty($where)) {
                $where .= " where nns_deleted =$deleted";
            } else {
                $where .= " and nns_deleted =$deleted";
            }
        }

        if (!empty($begin_sy_date) && !empty($end_sy_date)) {
            $where.=" and nns_show_time>='$begin_sy_date' and nns_show_time<='$end_sy_date' ";
        }

        if (!empty($begin_create_date)){
            $date_str = date('Y-m-d H:i:s',strtotime($begin_create_date));
            $where.=" and nns_create_time>='$date_str'";

        }
        if( !empty($end_create_date)) {
            $date_str1 = date('Y-m-d H:i:s',strtotime($end_create_date));
            $where.="  and nns_create_time<='$date_str1' ";
        }
        if (!empty($begin_copyright_date) && !empty($end_copyright_date)) {
            $where.=" and nns_copyright_date>='$begin_copyright_date' and nns_copyright_date<='$end_copyright_date' ";
        }
        if (!empty($searchpoint)) {
            $where.=" and INSTR('$searchpoint',nns_point)>0 ";
        }

        //order
        if (isset($order)) {
            if (!empty($order)) {
                $where .= " order by $order desc";
            } else {
                $where .= " order by nns_modify_time desc";
            }
        }


        //limit
        if (isset($min) && isset($num)) {
            if (!empty($num)) {
                $min = (int) $min;
                $num = (int) $num;
                $where .= " limit $min,$num";
            }
        }

        return $where;
    }

    public function check_bind_state($vod_id, $index_num) {
        $sql_str = "select count(1)  as num  from nns_vod_media where nns_vod_id='$vod_id' group  by nns_vod_index";
        $result = $this->db_obj->nns_db_query($sql_str);

        if ($index_num > count($result["data"])) {
            $bool = false;
        } else {
            $bool = true;
        }
        return $bool;
    }

    public function get_vod_by_ids($media_ids) {
        if (empty($media_ids))
            return false;
        $str_in = '';
        foreach ($media_ids as $media_id) {
            $str_in .="'$media_id',";
        }
        $str_in = rtrim($str_in, ',');
        $str_sql = "select * from  nns_vod where nns_id in($str_in)";
        $result = $this->db_obj->nns_db_query($str_sql);
        return isset($result['data']) ? $result['data'] : false;
    }

    //	清除媒资服务包缓存
    public function clear_memcache_by_asset_service($id) {
        $str_sql = "select nns_category_id,nns_assist_id from nns_assists_item where nns_video_id='$id'";
        $result = $this->db_obj->nns_db_query($str_sql);
        $dc = nl_get_dc(array(
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
        $dc->open();

        foreach ($result['data'] as $item) {
            $this->clear_memcache('asset', $item['nns_assist_id'], $item['nns_category_id']);
            nl_asset_item::delete_asset_item_list_by_child_cache($dc, $item['nns_assist_id']);
        }
        $str_sql = "select nns_category_id,nns_service_id from nns_service_item where nns_video_id='$id'";
        $result = $this->db_obj->nns_db_query($str_sql);
        foreach ($result['data'] as $item) {
            $this->clear_memcache('service', $item['nns_service_id'], $item['nns_category_id']);
        }
        $dc->close();
    }

    private function clear_memcache($type, $package, $category) {
        global $g_mem_cache_enabled;
        if ($g_mem_cache_enabled == 1) {
            $mem_inst = memcached_manager::get_instance();
            $mem_inst->delete_cache(
                'index_'
                . $type . '_'
                . $package . '_'
                . $category);

            $dc = nl_get_dc(array(
                'db_policy' => NL_DB_WRITE,
                'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
            ));
            $dc->open();
            nl_asset_item::delete_asset_item_list_by_child_cache($dc, $package);
        }
    }

    /**
     * 通过导入ID 获取CMS平台ID
     * @param String 来源
     * @param String 来源ID
     * @return string ID
     * 			FALSE 无此ID
     */
    public function get_video_id_by_import_id($import_source, $import_id,$cp_id=null) {

        $str_sql = "select nns_id from nns_vod where " .
            "nns_import_source='{$import_source}' and " .
            "nns_asset_import_id='{$import_id}' and nns_deleted!=1";
        if (strlen($cp_id) < 1)
        {
            $cp_id = 0;
        }
        $str_sql.=" and nns_cp_id='{$cp_id}'";
        $result = $this->db_obj->nns_db_query($str_sql);
        if (is_array($result['data'])) {
            if (count($result['data']) > 0) {
                return $result['data'][0]['nns_id'];
            } else {
                return FALSE;
            }
        }
        return FALSE;
    }

    /*         * *
     * 批量修改EPG标识
     */

    public function edit_epg_tags($videos, $tags) {

        $video_ids = implode("','", $videos);

        $str_sql = "update nns_vod set nns_tag='{$tags}' where nns_id in ('{$video_ids}')";
        $result = $this->db_obj->nns_db_execute($str_sql);
        if ($result['ret'] == 0) {
            $str_sql = "update nns_assists_item set nns_tag='{$tags}' where nns_video_id in ('{$video_ids}') and nns_video_type='0'";
            $this->db_obj->nns_db_execute($str_sql);

            foreach ($videos as $video_id) {
                $this->clear_memcache_by_asset_service($video_id);
            }
        }

        return $result;
    }

}

?>