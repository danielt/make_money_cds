<?php
/**error define**/

//通用 API error
define('NNS_ERROR_OK',0); //成功
define('NNS_ERROR_UNKNOWN', 1);//未知错误
define('NNS_ERROR_ARGS', 2); //命令的参数有错
define('NNS_ERROR_NOT_IMPL', 3);//命令未实现
define('NNS_ERROR_AUTH', 4);//未认证或是用户名密码错误
define('NNS_ERROR_MEMORY', 5);//没有内容了 比如生成对象失败
define('NNS_ERROR_DB_CONNECT', 6);//DB连接
define('NNS_ERROR_DB_AUTH', 7);////DB认证
define('NNS_ERROR_DB_SQL_EXEC', 8);//DB SQL 执行
define('NNS_ERROR_NOT_CONNECT',9);//执行一个向其他地方连接时，建立连接失败
define('NNS_ERROR_NOT_READY',10); //需要执行的，因为状态未就绪，未得到执行
define('NNS_ERROR_NOT_DB_NAME',11);//指定的数据库不存在
define('NNS_ERROR_DB_CLOSE',12);//关闭连接出错
//8XXX-8999 NNS DB开发使用

//PRI ERROR 8000-80009
define('NNS_ERROR_PRI_NO_DATA',8000); //没有权限记录
define('NNS_ERROR_PRI_NOT_FOUND',8001); //没有权限记录

define('NNS_ERROR_MODULE_NO_DATA',8002); //没有模块记录
define('NNS_ERROR_MODULE_NOT_FOUND',8003); //没有模块记录

//ROLE ERROR 8010-8019
define('NNS_ERROR_ROLE_EXIST',8010); //角色已经存在
define('NNS_ERROR_ROLE_NOT_FOUND',8011); //角色不存在
define('NNS_ERROR_ROLE_NO_DATA',8012); //没有角色记录

//MANAGER ERROR 8020-8029
define('NNS_ERROR_MANAGER_EXIST',8020);//管理员已存在
define('NNS_ERROR_MANAGER_NOT_FOUND',8021);//管理员不存在
define('NNS_ERROR_MANAGER_NO_DATA',8022);//没有管理员记录
define('NNS_ERROR_MANAGER_LOGIN_NOT_FOUND', 8023) ;//管理员ID和PWD验证失败
define('NNS_ERROR_CONFIG_LOGIN_NOT_FOUND', 8024) ;//系统管理员ID和PWD验证失败（config中配置）

define('NNS_ERROR_MANAGER_LOG_EXIST', 8025) ;//管理员日志存在
define('NNS_ERROR_MANAGER_LOG_NO_DATA', 8026) ;//管理员日志不存在

//carrier error 8030-8039
define('NNS_ERROR_CARRIER_EXIST', 8030);//运营商已存在
define('NNS_ERROR_CARRIER_NOT_FOUND', 8031);//运营商不存在
define('NNS_ERROR_CARRIER_NO_DATA', 8032);//运营商已存在


//partner error 8040-8049
define('NNS_ERROR_PARTNER_EXIST', 8040);//合作伙伴已存在
define('NNS_ERROR_PARTNER_NOT_FOUND', 8041);//合作伙伴不存在
define('NNS_ERROR_PARTNER_NO_DATA', 8042);//合作伙伴不存在

//group error 8050-8059
define('NNS_ERROR_GROUP_EXIST', 8050);//集团已存在
define('NNS_ERROR_GROUP_NOT_FOUND', 8051);//集团不存在
define('NNS_ERROR_GROUP_NO_DATA', 8052);//集团不存在

//category_list 8060-8069
define('NNS_ERROR_CATEGORY_EXIST', 8060);//栏目样式已存在
define('NNS_ERROR_CATEGORY_NOT_FOUND', 8061);//栏目样式不存在
define('NNS_ERROR_CATEGORY_NO_DATA', 8062);//栏目样式不存在

//category_detail 8070-8079
define('NNS_ERROR_CATEGORY_DETAIL_EXIST', 8070);//栏目已存在
define('NNS_ERROR_CATEGORY_DETAIL_NOT_FOUND', 8071);//栏目不存在
define('NNS_ERROR_CATEGORY_DETAIL_NO_DATA', 8072);//栏目不存在

//device_type 8080-8089
define('NNS_ERROR_DEVICE_TYPE_EXIST',8080);//终端类型已经存在
define('NNS_ERROR_DEVICE_TYPE_NOT_FOUND',8081);
define('NNS_ERROR_DEVICE_TYPE_NO_DATA',8082);

//vod 8090-8099
define('NNS_ERROR_VOD_EXIST',8090);//点播目录已经存在
define('NNS_ERROR_VOD_NOT_FOUND',8091);
define('NNS_ERROR_VOD_NO_DATA',8092);

define('NNS_ERROR_VOD_INDEX_EXIST',8093);//点播索引已经存在
define('NNS_ERROR_VOD_INDEX_NOT_FOUND',8094);
define('NNS_ERROR_VOD_INDEX_NO_DATA',8095);

define('NNS_ERROR_VOD_MEDIA_EXIST',8096);//点播片源已经存在
define('NNS_ERROR_VOD_MEDIA_NOT_FOUND',8097);
define('NNS_ERROR_VOD_MEDIA_NO_DATA',8098);

//vod 8100-8109
define('NNS_ERROR_LIVE_EXIST',8100);//直播目录已经存在
define('NNS_ERROR_LIVE_NOT_FOUND',8101);
define('NNS_ERROR_LIVE_NO_DATA',8102);

define('NNS_ERROR_LIVE_INDEX_EXIST',8103);//直播索引已经存在
define('NNS_ERROR_LIVE_INDEX_NOT_FOUND',8104);
define('NNS_ERROR_LIVE_INDEX_NO_DATA',8105);

define('NNS_ERROR_LIVE_MEDIA_EXIST',8106);//直播片源已经存在
define('NNS_ERROR_LIVE_MEDIA_NOT_FOUND',8107);
define('NNS_ERROR_LIVE_MEDIA_NO_DATA',8108);

//live_playbill 8110 -8119
define('NNS_ERROR_LIVE_PLAYBILL_EXIST',8110);
define('NNS_ERROR_LIVE_PLAYBILL_NOT_FOUND',8111);
define('NNS_ERROR_LIVE_PLAYBILL_NO_DATA',8112);

//user 8120-8129
define('NNS_ERROR_USER_EXIST', 8120);
define('NNS_ERROR_USER_NOT_FOUND', 8121);
define('NNS_ERROR_USER_NO_DATA', 8122);

//device 8130-8139
define('NNS_ERROR_DEVICE_EXIST', 8120);
define('NNS_ERROR_DEVICE_NOT_FOUND', 8121);
define('NNS_ERROR_DEIVCE_NO_DATA', 8122);


define('NNS_ERROR_SERVICE_EXIST', 8120);

?>