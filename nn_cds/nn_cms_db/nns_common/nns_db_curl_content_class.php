<?php
class nns_db_curl_content_class{
	//cur 调用核心管理平台内容管理
	public static function nns_db_curl_content($url,$curl_post) {
		$data = null;
		try {			
			$curl = curl_init ();
			if (! $curl) {
				return $data;
			}
			if (! curl_setopt ( $curl, CURLOPT_URL, $url )) {
				return $data;
			}
			if (! curl_setopt ( $curl, CURLOPT_HEADER, 0 )) {
				return $data;
			}
			if (! curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, 1 )) {
				return $data;
			}
			if (! curl_setopt ( $curl, CURLOPT_POST, 1 )) {
				return $data;
			}
			if (! curl_setopt ( $curl, CURLOPT_POSTFIELDS, $curl_post )) {
				return $data;
			}
			
			$data = curl_exec ( $curl );
			if ($data !== false) {
				$state = curl_getinfo ( $curl, CURLINFO_HTTP_CODE );
				if ($state == 200) {
					curl_close ( $curl );
					return $data;
				} else {
					$data = null;
				}
			} else {
				$data = null;
			}
		
		} catch ( Exception $ex ) {
			$data = null;
		}
		return $data;
	}
	
	
}

?>