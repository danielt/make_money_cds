<?php
$nns_db_dir = dirname ( dirname ( __FILE__ ) );
require_once $nns_db_dir.DIRECTORY_SEPARATOR.'nns_common'.DIRECTORY_SEPARATOR.'nns_db_validation_class.php';
require_once $nns_db_dir.DIRECTORY_SEPARATOR.'nns_common'.DIRECTORY_SEPARATOR.'nns_db_class.php';
require_once $nns_db_dir.DIRECTORY_SEPARATOR.'nns_debug_log'.DIRECTORY_SEPARATOR.'nns_debug_log_class.php';
class nns_db_common_class {

	protected $db_obj;
    protected $table_name;
    protected $fields;
   // protected $log_file;
    protected $debug = true;
    public $log;
	public function __construct() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->db_obj->nns_db_connect ();
        //$this->log_file =  dirname(dirname(dirname(__FILE__))).'/data/log/debug_sql.log';
        $this->log =  new nns_debug_log_class();
        $this->log->setlogpath('debug_sql');
	    $this->init();
	}
	public function init(){
		//trigger_error('please init table name and fileds!',E_USER_ERROR);
	}
	/**
	 *
	 * Enter description here ...
	 * @param string $str 需要校验的字符串,非0数字即验证
	 * @param int $is_name 是否由字母数字和下划线组成 ,非0数字即验证
	 * @param int $is_alpha_num 是否由字母和数字组成,非0数字即验证
	 * @param int $is_digit是否是纯数字,非0数字即验证
	 * @param int $min 最小长度,非0数字即验证
	 * @param int $max 最大长度,非0数字即验证
	 * @return false/str
	 */
	public static function nns_db_common_validation($str, $min = 1, $max = 0, $is_name = null, $is_alpha_num = null, $is_digit = null, $is_chinese = null) {
		if (empty ( $str )) {
			return false;
		}
		//非法字符串验证
		$str = nns_db_validation_class::nns_db_validation_inject_check ( $str );
		if ($str === false) {
			return false;
		}
		//name
		if (! empty ( $is_name )) {
			if (nns_db_validation_class::nns_db_validation_is_name ( $str ) === false) {
				return false;
			}
		}
		//alpha_num
		if (! empty ( $is_alpha_num )) {
			if (nns_db_validation_class::nns_db_validation_is_alpha_num ( $str ) === false) {
				return false;
			}
		}
		//digit
		if (! empty ( $is_digit )) {
			if (nns_db_validation_class::nns_db_validation_is_digit ( $str ) === false) {
				return false;
			}
		}
		//min max
		if (nns_db_validation_class::nns_db_validation_check_len ( $str, $min, $max ) === false) {
			return false;
		}
		//chinese
		if (! empty ( $is_chinese )) {
			if (nns_db_validation_class::nns_db_validation_is_chinese_char ( $str ) === true) {
				return false;
			}
		}

		return $str;
	}
	/**
	 * db->nns_db_execute 方法别名
	 */
	public function query($sql){

		$rs = $this->db_obj->nns_db_execute($sql);
		if($rs['ret'] == 0) return true;
		return false;
	}
	public function get_all($sql){

		$result = $this->db_obj->nns_db_query($sql);


		return isset($result['data'])?$result['data']:null;
	}
	public function get_one($sql){
		$data_set = $this->get_all($sql);
		return !empty($data_set)?$data_set[0]:null;
	}
	public function get_col($sql){
		$rs = $this->get_one($sql);
		if($rs){
            return $rs[0];
		}
		return false;
	}
    /**
     * 插入
     * add by xsong512 2012-7-6
     */
    public function insert_table($data_arr){
    	if(empty($data_arr)) return false;
    	if(is_string($data_arr)){
    		$sql = $data_arr;
    	}else{
	    	if(empty($this->table_name)) exit(" 没有指定数据表名字");
	    	if(empty($this->fields)) return false;
	    	$sql = "insert into ".$this->table_name."(";
	    	foreach($this->fields as $f_k =>$f_v ) {
	    		//
	            $sql .= $f_k.',';
	    	}
	    	$sql = rtrim($sql,',');
	    	$sql .=') values( ';
	    	foreach($this->fields as  $f_k =>$f_v ) {
	    		//
	    		if(empty($data_arr[$f_k])){
	                $sql .= "'".$f_v."',";
	    		}else{
	    			$sql .= "'".$data_arr[$f_k]."',";
	    		}
	    	}
	    	$sql = rtrim($sql,',');
	    	$sql .=')';
    	}
    	return $this->query($sql);
    }
    /**
     * 更新
     * add by xsong512 2012-7-6
     */
    public function update_table($sets,$wheres){
    	if(empty($this->table_name)) exit(" 没有指定数据表名字");
    	if(empty($this->fields) || empty($sets)) return false;
    	$sql = "update ".$this->table_name." set ";
    	if(is_array($sets)){
	    	foreach($sets as $fd_key => $fd_v){
	    		//
	    		if(array_key_exists($fd_key,$this->fields)){
	    			$sql .= "$fd_key='".$fd_v."',";
	    		}
	    	}
    	}
    	else if(is_string($sets)){
    		$sql .= $sets;
    	}
    	$sql = rtrim($sql,',');
        $sql .= $this->_build_where($wheres);

        return $this->query($sql);
    }
    public function delete_table($nns_ids){
		$where = $this->_build_where($nns_ids);
		$sql = "delete from ".$this->table_name." ".$where;
        $this->query($sql);
    }
    public function count_table($wheres=null){
    	if(empty($this->table_name)) exit(" 没有指定数据表名字");
    	$sql = 'select count(*) as num from '.$this->table_name;;
    	$sql .= $this->_build_where($wheres);

    	$result = $this->db_obj->nns_db_query($sql);

    	return isset($result['data'][0]['num'])?$result['data'][0]['num']:0;
    }
    /**
     * 查询
     * add by xsong512 2012-7-6
     */
    public function select_table($select_fileds=null,$wheres=null,$start=0,$limit=30,$order=null){
    	if(empty($this->table_name)) exit(" 没有指定数据表名字");
    	$sql = 'select ';
    	if($select_fileds ==null){
	    	foreach($this->fields as $f_k=>$f_v) {
	    		//
	            $sql .= $f_k.',';
	    	}
    	}else if(is_string){
    		$sql .= $select_fileds;
    	}
    	else if(is_array($select_fileds)){
            foreach($select_fileds as $fd_key => $fd_v){
            	if(array_key_exists($fd_key,$this->fields)){
            		$sql .= " $fd_key,";
            	}
            }

    	}
    	$sql = rtrim($sql,',').' ';
        $sql .= " from ".$this->table_name;
    	$sql .= $this->_build_where($wheres);

    	if($start!==null && $limit!==null) $sql .= ' limit '.$start.','.$limit;
    	if( !empty($order) && is_string($order)) $sql .= $order;


		$result = $this->db_obj->nns_db_query($sql);


		return isset($result['data'])?$result['data']:null;
    }
    public function create_in($in_arr,$filed=null){
    	$sql = '';
    	foreach($in_arr as $v){
            if(trim($v)) $sql .="'".$v."',";
    	}
    	$sql = rtrim($sql,',');
    	if($filed) return sprintf($filed.' in (%s) ',' '.$sql.' ');
    	return sprintf(' in (%s) ',$sql);
    }
    private function _build_where($wheres=null){
    	if(empty($wheres)) return ' ';
 		$sql = " where 1 ";
 		if(is_string($wheres)){
			$sql .= ' and ' .$wheres;
		}
		else if(is_array($wheres)){

	    	foreach($wheres as $fd_key => $fd_v){
	    		$sql .= "and $fd_key='".$fd_v."'";
	    	}
		}
		return $sql;
    }
}

?>