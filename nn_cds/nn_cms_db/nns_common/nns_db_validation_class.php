<?php
class nns_db_validation_class {
	/**
	 * 
	 * 非法字符串检测 ...
	 * @param string $str
	 */	
	public static function nns_db_validation_inject_check($str) {
		/*$inject_str = "select|insert|update|delete|modify|create|union|into|load_file|outfile";
		if (@eregi ( $inject_str, $str )) {
			return false;
		}
		$str = str_replace("&", "&amp;",$str); 
        $str = str_replace("<", "&lt;",$str);
        $str = str_replace(">", "&gt;",$str);
		// 判断magic_quotes_gpc是否打开
		if (get_magic_quotes_gpc ()) {
			//$str = addslashes ( $str ); // 进行过滤,存在漏洞
			$str = str_replace("\\\"", "&quot;",$str);
            $str = str_replace("\\''", "&#039;",$str);			
		}
		else{
			$str = str_replace("\"", "&quot;",$str);
            $str = str_replace("'", "&#039;",$str);
		}*/
		
		return $str;
	}
	
	/**
	 * 验证登陆帐号  由数字、26个英文字母或者下划线组成的字符串
	 * 
	 * @param string $string 被验证的字符串.
	 * 
	 * @return boolean 验证通过返回真,失败返回假.
	 * 
	 */
	public static function nns_db_validation_is_name($string) {
		$exp_match = '/^\w+$/';
		return preg_match ( $exp_match, $string ) ? true : false;
	}
	
	/**  
	 * 验证是否为数字和26个英文字母组成的字符串。
	 * 
	 * @param string $string 被验证的字符串。  
	 * 
	 * 
	 * 
	 * 
	 * @return boolean 验证成功返回真，失败返回假。  
	 */
	public static function nns_db_validation_is_alpha_num($string) {
		return preg_match ( '/^[A-Za-z0-9]+$/', $string ) ? true : false;
	}
	
     /**  
	 * 验证是否为纯数字。  
	 * 
	 * @param string $string 被验证的字符串。  
	 * 
	 * @return boolean 纯数字返回真，否则返回假。  
	 */
	public static function nns_db_validation_is_digit($string) {
		return preg_match ( '/^[0-9]+$/', $string ) ? true : false;
	}
	/**
	 * 
	 * 验证长度 ，第二个参数是最小长度 ，第三个参数 是最大长度
	 * @param string $str
	 * @param int $min
	 * @param int $max
	 * @return boolean
	 * 
	 */	
	public static function nns_db_validation_check_len($str, $min = 1, $max = 0) {
		$len = strlen ( $str );
		if ($max === 0) {
			return $len >= $min ? true : false;
		}
		return ($len >= $min && $len <= $max) ? true : false;
	}
    /**  
	 * 是否为中文汉字字符串。UTF-8版。  
	 * 
	 * @param string $string  
	 * 
	 * @return boolean  
	 */
	public static function nns_db_validation_is_chinese_char($string) {
		return preg_match ( "/^[\x{4e00}-\x{9fa5}]+$/u", $string ) ? true : false;
	}
//	替换单引号
	public static function nns_db_validation_replace_string($string) {
		$string=str_replace ("'", "\'",$string );
		
		return $string;
	}
	//	替换单引号
	public static function nns_db_validation_replace_date($string) {
		$string=preg_replace ("/[^0-9^\:^\-^\s]/i", "",$string );
		return $string;
	}
}

?>