<?php

/*
 * Created on 2012-6-28
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class nns_db_pager_class {

        var $cur_page;
        var $total_pages;
        var $next_page;
        var $pre_page;
        var $page_size;
        var $url;
        var $total_count;
        var $shorter;
        var $is_num;

        function __construct($count, $page_size = 30, $cur_page = 1, $url = '?', $shorter = false, $is_num = 1) {
                //
                $url = str_replace('?&', '?', $url);
                $this->is_num = $is_num;
                $this->shorter = $shorter;
                $this->total_count = $count;
                $this->startnum = ($cur_page - 1) * $page_size;
                $this->page_size = $page_size;
                if ($page_size < 0)
                        $this->page_size = 1;
                $this->cur_page = $cur_page;
                if ($cur_page < 0)
                        $this->cur_page = 1;

                $this->total_pages = ceil($count / $this->page_size);
                if ($cur_page > $this->total_pages) {
                        $this->cur_page = 1;
                }


                if ($cur_page == 1) {
                        $this->pre_page = 1;
                } else {
                        $this->pre_page = ($cur_page - 1);
                }

                if ($cur_page == $this->total_pages) {
                        $this->next_page = 1;
                } else {
                        $this->next_page = ($cur_page + 1);
                }
                $this->url = $url;
        }

        function nav() {
                return $this->nav_1();
        }
        
        function nav_4() {
        	
                $html = '';
                $html .= '<div class="pagecontrol">';
                if ($this->total_count == 0) {
                        $html .= "<span>" . cms_get_lang('first_page') . "</span>&nbsp;&nbsp;&nbsp;&nbsp;";
                        $html .= "<span>" . cms_get_lang('pre_page') . "</span>&nbsp;&nbsp;&nbsp;&nbsp;";
                        $html .= "<span>" . cms_get_lang('next_page') . "</span>&nbsp;&nbsp;&nbsp;&nbsp;";
                        $html .= "<span>" . cms_get_lang('last_page') . "</span>&nbsp;&nbsp;&nbsp;&nbsp;";
                } else {
                        if (false !== $pos = strpos($this->url, '?')) {
                                $len = strlen($this->url);
                                if ($pos == $len - 1) {
                                        $page = 'page';
                                } else {
                                        $page = '&page';
                                }
                        }
//                        $html .= cms_get_lang('record_amount') . $this->total_count . cms_get_lang('record_one') . '&nbsp;&nbsp;&nbsp;&nbsp;';
                        if ($this->cur_page != 1) {
                                $html .= '<a target="_self" href="' . $this->url . '&page=1">' . cms_get_lang('first_page') . '</a>&nbsp;&nbsp;&nbsp;&nbsp;';
                        } else {
                                $html .= '<span>' . cms_get_lang('first_page') . '</span>&nbsp;&nbsp;&nbsp;&nbsp;';
                        }

                        if ($this->cur_page > 1) {
                                $html .= '<a target="_self" href="' . $this->url . $page . '=' . $this->pre_page . '">' . cms_get_lang('pre_page') . '</a>&nbsp;&nbsp;&nbsp;&nbsp;';
                        } else {
                                $html .= '<span>' . cms_get_lang('pre_page') . '</span>&nbsp;&nbsp;&nbsp;&nbsp;';
                        }
                        if ($this->cur_page != $this->total_pages && $this->total_pages > 1) {
                                $html .= '<a target="_self" href="' . $this->url . $page . "=" . $this->next_page . '">' . cms_get_lang('next_page') . '</a>&nbsp;&nbsp;&nbsp;&nbsp;';
                        } else {
                                $html .= '<span>' . cms_get_lang('next_page') . '</span>&nbsp;&nbsp;&nbsp;&nbsp;';
                        }
                        if ($this->total_pages > 1 && $this->cur_page != $this->total_pages) {
                                $html .= '<a target="_self" href="' . $this->url . $page . "=" . $this->total_pages . '">' . cms_get_lang('last_page') . '</a>&nbsp;&nbsp;&nbsp;&nbsp;';
                        } else {
                                $html .= '<span>' . cms_get_lang('last_page') . '</span>&nbsp;&nbsp;&nbsp;&nbsp;';
                        }
                }
                if (!$this->shorter) {
                        
                        $html .= cms_get_lang('current') . '<input type="text" style="width:20px;" value="' . $this->cur_page . '" id="go_page_num" name="go_page_num">  / <span style="font-weight:bold;color:#ff0000;">' . $this->total_pages . '</span>' . cms_get_lang('page') . '&nbsp;&nbsp;';
                         $html .= '<a href="javascript:go_page_num(\'' . $this->url . '\',' . $this->total_pages . ');">GO&gt;&gt;</a>&nbsp;&nbsp;';
                        
                                $html .= '|&nbsp;&nbsp;' . cms_get_lang('perpagenum') . '&nbsp;';
                                $html .= '<input type="text" style="width:24px;" value="' . $this->page_size . '" id="nns_list_max_num" name="nns_list_max_num">&nbsp;&nbsp;';
                                $html .= '<input type="button" onclick="refresh_prepage_list();" value="' . cms_get_lang('confirm') . '">';
                       
                }
                $html .= '</div>';
                return $html;
        	
        }

        public function nav_1() {

                $html = '';
                $html .= '<div class="pagecontrol">';
                if ($this->total_count == 0) {
                        $html .= "<span>" . cms_get_lang('first_page') . "</span>&nbsp;&nbsp;&nbsp;&nbsp;";
                        $html .= "<span>" . cms_get_lang('pre_page') . "</span>&nbsp;&nbsp;&nbsp;&nbsp;";
                        $html .= "<span>" . cms_get_lang('next_page') . "</span>&nbsp;&nbsp;&nbsp;&nbsp;";
                        $html .= "<span>" . cms_get_lang('last_page') . "</span>&nbsp;&nbsp;&nbsp;&nbsp;";
                } else {
                        if (false !== $pos = strpos($this->url, '?')) {
                                $len = strlen($this->url);
                                if ($pos == $len - 1) {
                                        $page = 'page';
                                } else {
                                        $page = '&page';
                                }
                        }
                        $html .= cms_get_lang('record_amount') . $this->total_count . cms_get_lang('record_one') . '&nbsp;&nbsp;&nbsp;&nbsp;';
                        if ($this->cur_page != 1) {
                                $html .= '<a target="_self" href="' . $this->url . '&page=1">' . cms_get_lang('first_page') . '</a>&nbsp;&nbsp;&nbsp;&nbsp;';
                        } else {
                                $html .= '<span>' . cms_get_lang('first_page') . '</span>&nbsp;&nbsp;&nbsp;&nbsp;';
                        }

                        if ($this->cur_page > 1) {
                                $html .= '<a target="_self" href="' . $this->url . $page . '=' . $this->pre_page . '">' . cms_get_lang('pre_page') . '</a>&nbsp;&nbsp;&nbsp;&nbsp;';
                        } else {
                                $html .= '<span>' . cms_get_lang('pre_page') . '</span>&nbsp;&nbsp;&nbsp;&nbsp;';
                        }
                        if ($this->cur_page != $this->total_pages && $this->total_pages > 1) {
                                $html .= '<a target="_self" href="' . $this->url . $page . "=" . $this->next_page . '">' . cms_get_lang('next_page') . '</a>&nbsp;&nbsp;&nbsp;&nbsp;';
                        } else {
                                $html .= '<span>' . cms_get_lang('next_page') . '</span>&nbsp;&nbsp;&nbsp;&nbsp;';
                        }
                        if ($this->total_pages > 1 && $this->cur_page != $this->total_pages) {
                                $html .= '<a target="_self" href="' . $this->url . $page . "=" . $this->total_pages . '">' . cms_get_lang('last_page') . '</a>&nbsp;&nbsp;&nbsp;&nbsp;';
                        } else {
                                $html .= '<span>' . cms_get_lang('last_page') . '</span>&nbsp;&nbsp;&nbsp;&nbsp;';
                        }
                }
                if (!$this->shorter) {
                        if ($this->is_num) {
                                $html .= cms_get_lang('jump_to') . '<input type="text" style="width:20px;" value="' . $this->cur_page . '" id="go_page_num" name="go_page_num"> ' . cms_get_lang('page') . ' &nbsp;&nbsp;';
                                $html .= '<a href="javascript:go_page_num(\'' . $this->url . '\',' . $this->total_pages . ');">GO&gt;&gt;</a>&nbsp;&nbsp;';
                        }
                        $html .= cms_get_lang('current') . ' <span style="font-weight:bold;color:#ff0000;">' . $this->cur_page . '/' . $this->total_pages . '</span>' . cms_get_lang('page') . '&nbsp;&nbsp;';
                        if ($this->is_num) {
                                $html .= '|&nbsp;&nbsp;' . cms_get_lang('perpagenum') . '&nbsp;';
                                $html .= '<input type="text" style="width:24px;" value="' . $this->page_size . '" id="nns_list_max_num" name="nns_list_max_num">&nbsp;&nbsp;';
                                $html .= '<input type="button" onclick="refresh_prepage_list();" value="' . cms_get_lang('confirm') . '">&nbsp;&nbsp;';
                        }
                }
                $html .= '</div>';
                return $html;
        }

        function nav_2() {

                if (false !== $pos = strpos($this->url, '?')) {
                        $len = strlen($this->url);
                        if ($pos == $len - 1) {
                                $page = 'page';
                        } else {
                                $page = '&page';
                        }
                }
                $nav = '<div class="pagecontrol">';
                $nav .=cms_get_lang('record_sum') . '<span style="font-weight:bold;color:#ff0000;">' . $this->total_count . '</span>' . cms_get_lang('record_one_record') . '&nbsp;' . cms_get_lang('record_one_display') . '<input type="text" style="width:20px;" value="' . $this->page_size . '" id="page_size" name="page_size">' . cms_get_lang('record_one_record');
                if ($this->cur_page != 1) {
                        $nav .= '<a target="_self" href="' . $this->url . $page . '=1">' . cms_get_lang('first_page') . '</a>&nbsp;&nbsp;&nbsp;&nbsp;';

                        $nav .= '<a target="_self" href="' . $this->url . $page . '=' . $this->pre_page . '">' . cms_get_lang('pre_page') . '</a>&nbsp;&nbsp;&nbsp;&nbsp;';
                }
                if ($this->cur_page != $this->total_pages && $this->total_pages > 1)
                        $nav .= '
			<a target="_self" href="' . $this->url . $page . "=" . $this->next_page . '">' . cms_get_lang('next_page') . '</a>&nbsp;&nbsp;&nbsp;&nbsp;';
                if ($this->total_pages > 1)
                        $nav .= '<a target="_self" href="' . $this->url . $page . "=" . $this->total_pages . '">' . cms_get_lang('last_page') . '</a>&nbsp;&nbsp;&nbsp;&nbsp;';

                $nav .= cms_get_lang('current') . ' <span style="font-weight:bold;color:#ff0000;">' . $this->cur_page . '</span>' . cms_get_lang('page') . '&nbsp;&nbsp;
		' . cms_get_lang('total') . '<span style="font-weight:bold;color:#ff0000;">' . $this->total_pages . '</span>页&nbsp;&nbsp;' . cms_get_lang('jump_to') . ' <input type="text" style="width:20px;" value="1" id="go_page_num" name="go_page_num"> ' . cms_get_lang('page') . ' &nbsp;&nbsp;' .
                        '&nbsp;
		<a href="javascript:_go2page(\'' . $this->url . '\',' . $this->total_pages . ');">GO&gt;&gt;</a>&nbsp;&nbsp;
		</div>';

                // $nav .= $this->_javascript();
                return $nav;
        }

        function nav_3() {
                $html = '';
                $html .= '<div class="pagecontrol" style="text-align:center;background:#FFFFFF;height:30px;line-height:30px;">';
                if ($this->total_count == 0) {
                        $html .= '<span><' . cms_get_lang('record_pre_page') . '</span>&nbsp;&nbsp;&nbsp;&nbsp;';
                        $html .= '<span>' . cms_get_lang('record_next_page') . '></span>&nbsp;&nbsp;&nbsp;&nbsp;';
                } else {
                        if (false !== $pos = strpos($this->url, '?')) {
                                $len = strlen($this->url);
                                if ($pos == $len - 1) {
                                        $page = 'page';
                                } else {
                                        $page = '&page';
                                }
                        }

                        if ($this->cur_page > 1) {
                                $html .= '<a target="_self" href="' . $this->url . $page . '=' . $this->pre_page . '"><' . cms_get_lang('record_pre_page') . '</a>&nbsp;&nbsp;&nbsp;&nbsp;';
                        } else {
                                $html .= '<span><' . cms_get_lang('pre_page') . '</span>&nbsp;&nbsp;&nbsp;&nbsp;';
                        }
                        for ($i = 1; $i <= $this->total_pages; $i++) {
                                $html .= '<a target="_self" href="' . $this->url . $page . "=" . $i . '">' . $i . '</a>&nbsp;&nbsp;&nbsp;&nbsp;';
                        }
                        if ($this->cur_page != $this->total_pages && $this->total_pages > 1) {
                                $html .= '<a target="_self" href="' . $this->url . $page . "=" . $this->next_page . '">' . cms_get_lang('record_next_page') . '></a>&nbsp;&nbsp;&nbsp;&nbsp;';
                        } else {
                                $html .= '<span>' . cms_get_lang('record_next_page') . '></span>&nbsp;&nbsp;&nbsp;&nbsp;';
                        }
                        $html.='&nbsp;&nbsp;(' . cms_get_lang('record_sum') . $this->total_count . cms_get_lang('record_pic') . ')';
                }

                $html .= '</div>';
                return $html;
        }

        function _javascript() {
                $js = '';
                $js .='<script> function  _go2page(url,maxpage,key){
			var pagenum=$("#go_page_num").val();
			if (isNaN(pagenum) || pagenum.indexOf(".")>=0){
				alert("<?php echo cms_get_lang(\'msg_3\');?>");
				return;
	}
	if (pagenum>maxpage){
		pagenum=maxpage;
	}
	if (pagenum<1){
		pagenum=1;
	}
	var page_size=$("#page_size").val();
	if (isNaN(pagenum) || pagenum.indexOf(".")>=0){
		alert("<?php echo cms_get_lang(\'record_input_num\');?>");
		return;
	}
	url += "&page_size="+page_size;
	if (key==undefined || key==""){
		window.location.href=url+"&page="+pagenum;
	}else{
		window.location.href=url+"&"+key+"="+pagenum;
	}

	}</script>';
                return $js;
        }

}
