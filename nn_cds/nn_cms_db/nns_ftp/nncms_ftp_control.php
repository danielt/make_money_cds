<?php

/*
 * Created on 2012-8-21
 * BY S67
 * 操作FTP类
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
$nns_ftp_dir = dirname ( dirname ( __FILE__ ) );
require_once $nns_ftp_dir . DIRECTORY_SEPARATOR . "nns_debug_log" . DIRECTORY_SEPARATOR . "nns_debug_log_class.php";
class nncms_ftp_control {
	public static function write_boss_play_log($params) {
		$log=nns_debug_log_class::get_instance();
		$log->setlogpath('ftp_import_file');
		$g_boss_file_path = $params['g_boss_file_path'];
		$record_header = $params['header'];
		$user_id = $params['user_id'];
		$boss_ftp_record = $params['boss_ftp_record'];
		$bool = false;
		if (!empty ($g_boss_file_path)) {
			if (is_writeable($g_boss_file_path)) {
				$bool = true;
			} else {
				if (@ chmod($g_boss_file_path, 0777)) {
					$bool = true;
				}
			}
			if ($bool) {
				list ($usec, $sec) = explode(" ", microtime());
				if(!empty($record_header)) $record_header.='_';
				if(!empty($user_id)) $user_id.='_';
				$ftp_file_name = $record_header . $user_id  . date("YmdHis", $sec) . round($usec * 1000);
				try {
					$log->write('FTP对账文件:' . $g_boss_file_path . $ftp_file_name . ".txt");
					$fp = fopen($g_boss_file_path . $ftp_file_name . ".txt", 'wb');
					if (fwrite($fp, $boss_ftp_record) === FALSE) {
						$state_str = "Write fault!\n";
					}
				} catch (Exception $e) {
					//$viewcode = null;
				}
			} else {
				$state_str = "No permission to write!\n";
			}
			 $log->write($state_str);
		}
	}
}
?>
