<?php
$nns_db_assist_dir = dirname ( dirname ( __FILE__ ) );
require_once $nns_db_assist_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
require_once $nns_db_assist_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";

class nns_db_assist_bind_class{
	private $conn;
	private $db_obj;
	
	public function __construct() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
	}
	
	
	public function nns_db_assest_bind_video_category_list($assest_id=null,$assest_category_id=null,$video_package_id=null,$video_category_id=null){
		$str_sql = "select * from nns_assest_bind_video_category ";
		$result = array ();
		
		$where="";
		if (isset ( $assest_id )) {
			if (! empty ( $assest_id )) {
					if (empty ( $where )) {
						$where .= " where nns_assest_id='$assest_id'";
					} else {
						$where .= " and nns_assest_id='$assest_id'";
					}
			}
		}
		
			if (isset ( $assest_category_id )) {
				$assest_category_id=(int)$assest_category_id;
					if (empty ( $where )) {
						$where .= " where nns_assest_category_id='$assest_category_id'";
					} else {
						$where .= " and nns_assest_category_id='$assest_category_id'";
					}
			}

		if (isset ( $video_package_id )) {
			if (! empty ( $video_package_id )) {
					if (empty ( $where )) {
						$where .= " where nns_video_package_id='$video_package_id'";
					} else {
						$where .= " and nns_video_package_id='$video_package_id'";
					}
			}
		}
		if (isset ( $video_category_id )) {
			if (! empty ( $video_category_id )) {
					if (empty ( $where )) {
						$where .= " where POSITION(nns_video_category_id IN '$video_category_id')=1";
					} else {
						$where .= " and POSITION(nns_video_category_id IN '$video_category_id')=1";
					}
			}
		}
		
		$str_sql.=$where;
			
		
		$result = $this->db_obj->nns_db_query ( $str_sql );
		return $result;
		
	}
	
    public function nns_db_assest_bind_video_category_add($assest_id,$assest_category_id, $video_package_id,$video_category_id,$assist_name,$video_category_name) {
		$result = array ();
		
		//exist
		$str_sql = "select 1 from nns_assest_bind_video_category where nns_assest_id='$assest_id' and nns_assest_category_id='$assest_category_id' and nns_video_package_id='$video_package_id' and nns_video_category_id='$video_category_id'";
	    $result = $this->db_obj->nns_db_query ( $str_sql );
	    if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows > 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_assist_EXIST, 'assist IS EXIST' );
			}else{
				$str_sql = "insert into nns_assest_bind_video_category(nns_assest_id,nns_assest_category_id,nns_video_package_id,nns_video_category_id,nns_assest_name,nns_video_category_name)values('$assest_id','$assest_category_id','$video_package_id','$video_category_id','$assist_name','$video_category_name')";
			}
		}else{
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_assist_EXIST, 'assist IS EXIST' );
		}
		

	
//		echo $str_sql;
		//INSERT
		
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		
		return $result;
	}
	
	public function nns_db_assest_bind_video_category_delete($assest_id,$assest_category_id, $video_package_id,$video_category_id){
		$result = array ();
		
		$str_sql = "delete from nns_assest_bind_video_category where nns_assest_id='$assest_id' and nns_assest_category_id='$assest_category_id' and nns_video_package_id='$video_package_id' and nns_video_category_id='$video_category_id'";
//		echo $str_sql;
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		
		return $result;
		
	}
	
	
}
?>