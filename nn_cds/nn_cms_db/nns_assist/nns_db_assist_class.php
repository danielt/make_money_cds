<?php
$nns_db_assist_dir = dirname ( dirname ( __FILE__ ) );
require_once $nns_db_assist_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
require_once $nns_db_assist_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";

class nns_db_assist_class{
	private $conn;
	private $db_obj;

	public function __construct() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
	}
	/**
	 *
	 *  统计合作伙伴数量 ...
	 */
	public function nns_db_assist_count($org_id,$org_type,$except_product_id=null){
		$result = array();
		$str_sql="select count(1) as num from nns_assists  where nns_org_id='$org_id' and nns_org_type=$org_type";
	    if (!empty($except_product_id)){
	    	$str_sql.=" and nns_id not in (select nns_content_id from nns_product_content where nns_content_type=0 and nns_product_id='$except_product_id' and nns_org_id='$org_id' and nns_org_type=$org_type)";
	    }
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_PARTNER_NO_DATA, 'partner data not find' );
			}
		}
		return $result;

	}

	public function nns_db_assist_list($org_id,$org_type,$except_product_id=null,$min=null,$num=null){
		$str_sql = "select * from nns_assists ";
		$result = array ();

		$where="";
		if (isset ( $org_id )) {
			if (! empty ( $org_id )) {
					if (empty ( $where )) {
						$where .= " where nns_org_id='$org_id'";
					} else {
						$where .= " and nns_org_id='$org_id'";
					}
			}
		}

		if (isset ( $org_type )) {
			if (! empty ( $org_type )) {
					if (empty ( $where )) {
						$where .= " where nns_org_type=$org_type";
					} else {
						$where .= " and nns_org_type=$org_type";
					}
			}
		}

		$str_sql.=$where;

		if (!empty($except_product_id)){
	    	$str_sql.=" and nns_id not in (select nns_content_id from nns_product_content where nns_content_type=0 and nns_product_id='$except_product_id' and nns_org_id='$org_id' and nns_org_type=$org_type)";
	    }

	    $str_sql .= " order by  nns_modify_time desc ";

	    if (isset ( $min ) && isset ( $num )) {
			if (! empty ( $num )) {
				$min = ( int ) $min;
				$num = ( int ) $num;
				$str_sql .= " limit $min,$num";
			}
		}
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_PARTNER_NO_DATA, 'partner data not find' );
			}
		}
		return $result;

	}

    public function nns_db_assist_add($id,$org_id, $org_type ,$content=null, $name="",$image=null,$assist_tree_id=null,$img1=null,$img2=null ) {
		$result = array ();
		//state
		$org_type = (int)$org_type;
		//exist
		$str_sql = "select * from nns_assists where nns_id='$id'";
	    $result = $this->db_obj->nns_db_query ( $str_sql );
	    if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows > 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_assist_EXIST, 'assist IS EXIST' );
			}
		}else{
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_assist_EXIST, 'assist IS EXIST' );
		}
		$str_sql = "insert into nns_assists(nns_id,nns_name,nns_org_id,nns_org_type,nns_category,nns_category_root_id,nns_image,nns_create_time,nns_modify_time,nns_image1,nns_image2)values('$id','$name','$org_id',$org_type,'$content','$assist_tree_id','$image',now(),now(),'$img1','$img2')";
		$result = $this->db_obj->nns_db_execute ( $str_sql );

		return $id;
	}

    public function nns_db_assist_modify($id,$org_id, $org_type ,$name=null,$assist_tree_id=null,$assist_image=null,$img1=null,$img2=null) {
		$result = array ();
		//name exist

			  $str_sql = "select * from nns_assists where nns_id='$id'";
		    $result = $this->db_obj->nns_db_query ( $str_sql );
			if ($result [ret] != 0) {
				return $result;
			}
			if (!empty($assist_image)){
				$img_str=",nns_image='$assist_image'";
			}
			if ($this->db_obj->num_rows > 0) {
				//update
				$str_sql= "update nns_assists set nns_image1='$img1',nns_image2='$img2',nns_org_id='$org_id',nns_org_type='$org_type',nns_name='$name',nns_category_root_id='$assist_tree_id',nns_image='$assist_image',nns_modify_time=now()".$img_str." where nns_id='$id'";
//				echo $str_sql;
				$result = $this->db_obj->nns_db_execute ( $str_sql );
			}



	    //id exist
	  return $result;
	}

	public function nns_db_assist_content_modify($id,$content){
		  $str_sql = "select * from nns_assists where nns_id='$id'";
		    $result = $this->db_obj->nns_db_query ( $str_sql );
			if ($result [ret] != 0) {
				return $result;
			}
			if ($this->db_obj->num_rows > 0) {
				$str_sql= "update nns_assists set nns_category='$content',nns_modify_time=now() where nns_id='$id'";
//				echo $str_sql;
				$result = $this->db_obj->nns_db_execute ( $str_sql );
			}
	}

     public function nns_db_assist_info($id){
	    $result = array ();
		if (! isset ( $id ) ) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'partner info param error' );
			return $result;
		}
		if (empty ( $id ) ) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'partner info param error' );
			return $result;
		}
		$str_sql = "select * from nns_assists where nns_id='$id'";
	    $result = $this->db_obj->nns_db_query ( $str_sql );
	    if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_PARTNER_NOT_FOUND, 'partner info not found' );
			}
		}
		return $result;
	}

//	获取当前影片所包含的媒资包
	public function check_video_belong($video_id){
		$str_sql="select COUNT(*) as num,a.nns_assist_id,b.nns_name from nns_assists_item a left join nns_assists b on a.nns_assist_id=b.nns_id where a.nns_video_id='$video_id' group by a.nns_assist_id";
		$result = $this->db_obj->nns_db_query ( $str_sql );
	    if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_PARTNER_NOT_FOUND, 'partner info not found' );
			}
		}
		return $result;
	}

	public function nns_db_assist_delete($id){
		$result = array ();
		if (! isset ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'partner delete param error' );
			return $result;
		}

		$str_sql = "delete from nns_assists where nns_id='$id'";
//		echo $str_sql;
		$result = $this->db_obj->nns_db_execute ( $str_sql );

		$str_sql = "delete from nns_assists_item where nns_assist_id='$id'";
//		echo $str_sql;
		$result = $this->db_obj->nns_db_execute ( $str_sql );

		return $result;

	}




}
?>