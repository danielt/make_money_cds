<?php
$nns_db_metadata_dir = dirname ( dirname ( __FILE__ ) );
require_once $nns_db_metadata_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
require_once $nns_db_metadata_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";
/*
 * Created on 2012-7-6
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class nns_metadata_class {
	private $conn;
	private $db_obj;
	function nns_metadata_class() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
	}

	public function nns_metadata_list($params){
		$sql_str="select * from nns_metadata ";
		$query=$this->nns_metadata_where($params);
		$sql_str.=$query;
		$result = $this->db_obj->nns_db_query ( $sql_str );
		return $result;
	}
	
	public function nns_metadata_count($params){
		$sql_str="select count(1) as num from nns_metadata ";
		$query=$this->nns_metadata_where($params);
		$sql_str.=$query;
		$result = $this->db_obj->nns_db_query ( $sql_str );
		return $result;
	}
	
	public function nns_metadata_delete($id){
		$sql_str="delete from nns_metadata where nns_id='$id'";
		$result = $this->db_obj->nns_db_execute ( $sql_str );
		return $result;
	}
	public function nns_metadata_add($params){
		$result=array();
		$metadata_key=$params["metadata_key"];
		$metadata_id=$params["metadata_id"];
		$metadata_type=$params["metadata_type"];
		$query_params=array();
		$query_params["metadata_key"]=$metadata_key;
		$query_params["metadata_id"]=$metadata_id;
		$query_params["metadata_type"]=$metadata_type;
		$query_result=$this->nns_metadata_list($query_params);
		if (count($query_result["data"])>0){
			$result["ret"]=1;
		}else{
			$result=$this->nns_metadata_add2($params);
		}
		return $result;
	}
	
	private function nns_metadata_add2($params){
		$guid = new Guid ();
		$id = $guid->toString ();
		
		$sql_str="insert into nns_metadata (nns_id,nns_metadata_id,nns_metadata_type,nns_metadata_key,nns_metadata_value,nns_create_time) values (" .
				"'".$id."','" .
				$params["metadata_id"]."','".
				$params["metadata_type"]."','".
				$params["metadata_key"]."','".
				$params["metadata_value"]."'," .
						"now())";
		$result = $this->db_obj->nns_db_execute ( $sql_str );
		return $result;
	}
	
	public function nns_metadata_modify($params){
		$sql_str="update nns_metadata set nns_metadata_value='".$params["metadata_value"] ."'," .
				" nns_metadata_key='".$params["metadata_key"] ."' where " .
				"nns_id='".$params["id"]."' " ;
		$result = $this->db_obj->nns_db_execute ( $sql_str );
		return $result;
	}
	
	private function nns_metadata_where($params){
		$where="";
		foreach ($params as $key=>$value){
			if ($value!=="" && $value!==null){
				if (empty($where)){
					$where.=" where ";
				}else{
					$where.=" and ";
				}
				$where.=" nns_".$key."='".$value."' ";
			}
		}
		
		$where.=" order by nns_create_time asc";
		return $where;
	}
}
?>