<?php
$nns_db_mem_dir = dirname (dirname ( dirname ( __FILE__ ) ));
require_once $nns_db_mem_dir  . DIRECTORY_SEPARATOR . "nn_cms_db". DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";
require_once $nns_db_mem_dir .DIRECTORY_SEPARATOR."nn_cms_config.php";
require_once $nns_db_mem_dir .DIRECTORY_SEPARATOR . "nn_cms_db". DIRECTORY_SEPARATOR . "nns_debug_log" . DIRECTORY_SEPARATOR . "nns_debug_log_class.php";
class memcached_manager {
	private static $instance;
	public static $NO_KEY="nokey";
	private  $inst;
	private  $connect_flag;
	private $log_inst;
    function memcached_manager() {
    	$this->log_inst=new nns_debug_log_class();
		$this->log_inst->setlogpath("memcache_log");
    }
    
   public static function get_instance(){
   		if (empty(memcached_manager::$instance)){
   			memcached_manager::$instance=new memcached_manager();
   		}
   		return memcached_manager::$instance;
   }
    
//    连接缓存服务器
    private function connect(){
    	$this->inst = new Memcache();
    	$server_arr=explode(";",g_memcache_server);
    	foreach ($server_arr as $server_item){
    		$server_params=explode(":",$server_item);
			$this->connect_flag=$this->inst->addServer($server_params[0], $server_params[1]);
			$log=$server_params[0].":".$server_params[1]." \n".$this->connect_flag." \n";
			$this->log_inst->setlog($log,"link server");
			
    	}
    }
// 根据index插入索引值   
    public function set_cache($index,$value){
    	if (!$this->connect_flag){
    		$this->connect();
    	}
    	if ($this->connect_flag){
    		$value=serialize($value);
    		$real_key=$this->getkey($index);
    		$set_bool=$this->inst->set($real_key,$value,0,3600);
    		//var_dump($set_bool);
    		
    	}
    	$log= var_export($value, true);
    	$log=$index." \n".$log." \n";
		$this->log_inst->setlog($log,"set_cache:$real_key");
    	
    }
//   根据index获取缓存内容
    public function get_cache($index){
    	if (!$this->connect_flag){
    		$this->connect();
    	}
    	if ($this->connect_flag){
    		$real_key=$this->getkey($index);
    		$result=$this->get_value($real_key);
    		if ($result!=memcached_manager::$NO_KEY){
    			$result=unserialize($result);
    		}
    	}
//    	$log= var_export($result, true);
//    	$log=$index." \n".$log." \n";
//		$this->log_inst->setlog($log,"get_cache:$real_key");
    	
    	return $result;
    }
    
    public function delete_cache($index){
    	if (!$this->connect_flag){
    		$this->connect();
    	}
    	if ($this->connect_flag){
    		$real_key=$this->getkey($index);
    		$real_value=$this->get_value($real_key);
    		$this->inst->delete($real_key);
    	}
    	$log=$index." \n";
		$this->log_inst->setlog($log,"delete_cache:$real_value");
    	
    }
    
    
//根据类型获取key值   
    private function getkey($index){
    	$index_arr=explode("_",$index);
    	$method=$index_arr[0];
    	switch ($method){
    		case "page":
//    		获取page缓存标志位
				$media_asset_id='';
				for ($ii=2;$ii<count($index_arr)-4;$ii++){
					$media_asset_id.=$index_arr[$ii].'_';
				}
				$media_asset_id=rtrim($media_asset_id,'_');
    			$category_id=$index_arr[count($index_arr)-4];
    			$index_type=$index_arr[1];
    			$rand_key="index_".$index_type."_".$media_asset_id."_".$category_id;
    			
    			$this->log_inst->setlog($rand_key,'$rand_key');
    			
    			$rand_key=md5($rand_key);
				$sign=$this->get_value($rand_key);
//				若未取到，对标志位进行设置
    			if ($sign==memcached_manager::$NO_KEY){
    				$guid = new Guid ();
    				$rand_value = $guid->toString ();
    				$this->inst->set($rand_key,$rand_value,0,3600);
    				$sign=$rand_value;
    			}
//    			得到列表页的当前key
				$index=$index."_".$sign;
				$this->log_inst->setlog($index,'$realy_key');
    		break;
    	}
    	return md5($index);
    }
//   清除所有缓存
    public function clear_cache(){
    	if (!$this->connect_flag){
    		$this->connect();
    	}
    	if ($this->connect_flag){
    		$this->inst->flush();
    	}
    }
// 获取key值  
    private function get_value($key){
    	$value=$this->inst->get($key);
    	if (!$value){
    		return memcached_manager::$NO_KEY;
    	}else{
    		return $value;
    	}
    }
    
}
?>