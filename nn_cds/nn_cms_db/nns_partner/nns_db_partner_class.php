<?php
$nns_db_partner_dir = dirname ( dirname ( __FILE__ ) );
require_once $nns_db_partner_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
require_once $nns_db_partner_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";
require_once $nns_db_partner_dir . DIRECTORY_SEPARATOR . "nns_depot" . DIRECTORY_SEPARATOR . "nns_db_depot_class.php";
class nns_db_partner_class{
	private $conn;
	private $db_obj;

	public function __construct() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
	}
	/**
	 *
	 *  统计合作伙伴数量 ...
	 */
	public function nns_db_partner_count($name=null){
		$result = array();
		$str_sql="select count(1) as num from nns_partner";
	    if (isset ( $name )) {
			if (! empty ( $name )) {
			    $name = nns_db_common_class::nns_db_common_validation ( $name );
				if ($name !== false) {
					$str_sql .= " where nns_name like '%$name%' ";
				}
			}
		}
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_PARTNER_NO_DATA, 'partner data not find' );
			}
		}
		return $result;

	}

	public function nns_db_partner_list($name=null, $min = 0, $num = 0){
		$str_sql = "select * from nns_partner ";
		$result = array ();
		if (isset ( $name )) {
			if (! empty ( $name )) {
			    $name = nns_db_common_class::nns_db_common_validation ( $name );
				if ($name !== false) {
					$str_sql .= " where nns_name like '%$name%' ";
				}
			}
		}
		if (isset ( $min ) && isset ( $num )) {
			if (! empty ( $num )) {
				$min = ( int ) $min;
                $num = ( int ) $num;
				$str_sql .= " limit $min,$num";
			}
		}

		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_PARTNER_NO_DATA, 'partner data not find' );
			}
		}
		return $result;

	}

    public function nns_db_partner_add($id, $name, $state = 0, $desc = null,$contact=null,$telephone=null,$email=null,$addr=null) {
		$result = array ();
        if (!isset($id) || empty($id)) {
            $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, "Add partner with error:param $id is empty");
            return $result;
        }
		if ( ! isset ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'partner add param error' );
			return $result;
		}
		if ( empty ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'partner add param error' );
			return $result;
		}
    //非法字符串检测
        $id = nns_db_common_class::nns_db_common_validation ( $id );
        if ($id === false) {
			$result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, "Add carrier with error:param:id is illegal");
			return $result;
		}
		$name = nns_db_common_class::nns_db_common_validation ( $name );
		if ($name === false) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier add param error' );
			return $result;
		}
		if (! empty ( $desc )) {
			$desc = nns_db_common_class::nns_db_common_validation ( $desc );
			if ($desc === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier add param error' );
				return $result;
			}
		}
		if (! empty ( $contact )) {
			$contact = nns_db_common_class::nns_db_common_validation ( $contact );
			if ($contact === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier add param error' );
				return $result;
			}
		}
		if (! empty ( $telephone )) {
			$telephone = nns_db_common_class::nns_db_common_validation ( $telephone );
			if ($telephone === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier add param error' );
				return $result;
			}
		}
		if (! empty ( $email )) {
			$email = nns_db_common_class::nns_db_common_validation ( $email );
			if ($email === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier add param error' );
				return $result;
			}
		}
		if (! empty ( $addr )) {
			$addr = nns_db_common_class::nns_db_common_validation ( $addr );
			if ($addr === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier add param error' );
				return $result;
			}
		}
		//state
		$state = (int)$state;
		//exist
		$str_sql = "select 1 from nns_partner where nns_id='$id' or nns_name='$name'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result [ret] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_PARTNER_EXIST, 'partner name exist' );
			return $result;
		}
		//INSERT
		$str_sql = "insert into nns_partner(nns_id,nns_name,nns_state,nns_desc,nns_create_time,nns_modify_time,nns_contact," .
				"nns_telephone,nns_email,nns_addr)values('$id','$name',0,'$desc',now(),now(),'$contact','$telephone'," .
				"'$email','$addr')";
		$result = $this->db_obj->nns_db_execute ( $str_sql );

//		创建仓库
		$depot_inst=new nns_db_depot_class();
		$depot_inst->nns_db_depot_modify($name,"",1,$id,0);
		$depot_inst->nns_db_depot_modify($name,"",1,$id,1);
		$depot_inst=null;

		return $result;
	}

    public function nns_db_partner_modify($id, $name, $state = 0, $desc = null,$contact=null,$telephone=null,$email=null,$addr=null) {
		$result = array ();
		if (! isset ( $id ) || ! isset ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'partner modify param error' );
			return $result;
		}
		if (empty ( $id ) || empty ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'partner modify param error' );
			return $result;
		}
    //非法字符串检测
	    $id = nns_db_common_class::nns_db_common_validation ( $id, 1, 32, '', 1 );
		if($id === false){
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier modify param error' );
			return $result;
		}
		$name = nns_db_common_class::nns_db_common_validation ( $name );
		if ($name === false) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier modify param error' );
			return $result;
		}
		if (! empty ( $desc )) {
			$desc = nns_db_common_class::nns_db_common_validation ( $desc );
			if ($desc === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier modify param error' );
				return $result;
			}
		}
		if (! empty ( $contact )) {
			$contact = nns_db_common_class::nns_db_common_validation ( $contact );
			if ($contact === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier modify param error' );
				return $result;
			}
		}
		if (! empty ( $telephone )) {
			$telephone = nns_db_common_class::nns_db_common_validation ( $telephone );
			if ($telephone === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier modify param error' );
				return $result;
			}
		}
		if (! empty ( $email )) {
			$email = nns_db_common_class::nns_db_common_validation ( $email );
			if ($email === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier modify param error' );
				return $result;
			}
		}
		if (! empty ( $addr )) {
			$addr = nns_db_common_class::nns_db_common_validation ( $addr );
			if ($addr === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier modify param error' );
				return $result;
			}
		}
		$state = (int)$state;
		//name exist
		$str_sql = "select 1 from nns_partner where nns_id != '$id' and nns_name='$name'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result [ret] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_PARTNER_EXIST, 'partner name exist' );
			return $result;
		}
	    //id exist
	    $str_sql = "select * from nns_partner where nns_id='$id'";
	    $result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result [ret] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			//update
			$str_sql= "update nns_partner set nns_name='$name',nns_state='$state',nns_desc='$desc',nns_contact='$contact',nns_telephone='$telephone',nns_email='$email',nns_addr='$addr',nns_modify_time=now() where nns_id='$id'";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
		}
		else{
			//insert
			$str_sql = "insert into nns_partner(nns_id,nns_name,nns_state,nns_desc,nns_create_time,nns_modify_time,nns_contact,nns_telephone,nns_email,nns_addr)values('$id','$name',0,'$desc',now(),now(),'$contact','$telephone','$email','$addr')";
		    $result = $this->db_obj->nns_db_execute ( $str_sql );
		}
		return $result;
	}

     public function nns_db_partner_info($id){
	    $result = array ();
		if (! isset ( $id ) ) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'partner info param error' );
			return $result;
		}
		if (empty ( $id ) ) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'partner info param error' );
			return $result;
		}
		$str_sql = "select * from nns_partner where nns_id='$id'";
	    $result = $this->db_obj->nns_db_query ( $str_sql );
	    if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_PARTNER_NOT_FOUND, 'partner info not found' );
			}
		}
		return $result;
	}

	public function nns_db_partner_delete($id){
		$result = array ();
		if (! isset ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'partner delete param error' );
			return $result;
		}

		$str_sql = "delete from nns_partner where nns_id='$id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );

		//		删除仓库
		$depot_inst=new nns_db_depot_class();
		$depot_inst->nns_db_depot_delete("",1,$id);
		$depot_inst=null;

		return $result;

	}


}
?>
