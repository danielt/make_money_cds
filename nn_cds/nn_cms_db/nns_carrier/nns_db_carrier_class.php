<?php
$nns_db_carrier_dir = dirname ( dirname ( __FILE__ ) );
require_once $nns_db_carrier_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
require_once $nns_db_carrier_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";
require_once $nns_db_carrier_dir . DIRECTORY_SEPARATOR . "nns_depot" . DIRECTORY_SEPARATOR . "nns_db_depot_class.php";

class nns_db_carrier_class {
	private $conn;
	private $db_obj;
	
	public function __construct() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
	}
	/**
	 * 
	 * 统计运营商数量 ...
	 */
	public function nns_db_carrier_count($name = null) {
		$str_sql = "select count(1) as num from nns_carrier ";
		$result = array ();
		if (isset ( $name )) {
			if (! empty ( $name )) {
				$name = nns_db_common_class::nns_db_common_validation ( $name );
				if ($name !== false) {
					$str_sql .= " where nns_name like '%$name%' ";
				}
			}
		}
		$result = $this->db_obj->nns_db_query ( $str_sql );
		return $result;
	}
	/** 
	 * 
	 * 运营商列表
	 * @param string $name
	 * @param int $min
	 * @param int $num
	 * @return 
	 */
	public function nns_db_carrier_list($name = null, $min = 0, $num = 0) {
		$result = array ();
		$str_sql = "select * from nns_carrier ";
		if (isset ( $name )) {
			if (! empty ( $name )) {
				$name = nns_db_common_class::nns_db_common_validation ( $name );
				if ($name !== false) {
					$str_sql .= " where nns_name like '%$name%' ";
				}
			}
		}
		if (isset ( $min ) && isset ( $num )) {
			if (! empty ( $num )) {
				$min = ( int ) $min;
				$num = ( int ) $num;
				$str_sql .= " limit $min,$num";
			}
		}
		
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_CARRIER_NO_DATA, 'carrier data not find' );
			}
		}
		return $result;
	}
	/**
	 * 
	 * 添加运营商
	 * @param string $id
	 * @param string $name
	 * @param int $state
	 * @param string $desc
	 */
	public function nns_db_carrier_add($name, $state = 0, $desc = null, $contact = null, $telephone = null, $email = null, $addr = null) {
		$result = array ();
		if (! isset ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier add param error' );
			return $result;
		}
		if (empty ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier add param error' );
			return $result;
		}
		//非法字符串检测
		$name = nns_db_common_class::nns_db_common_validation ( $name );
		if ($name === false) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier add param error' );
			return $result;
		}
		if (! empty ( $desc )) {
			$desc = nns_db_common_class::nns_db_common_validation ( $desc );
			if ($desc === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier add param error' );
				return $result;
			}
		}
		if (! empty ( $contact )) {
			$contact = nns_db_common_class::nns_db_common_validation ( $contact );
			if ($contact === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier add param error' );
				return $result;
			}
		}
		if (! empty ( $telephone )) {
			$telephone = nns_db_common_class::nns_db_common_validation ( $telephone );
			if ($telephone === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier add param error' );
				return $result;
			}
		}
		if (! empty ( $email )) {
			$email = nns_db_common_class::nns_db_common_validation ( $email );
			if ($email === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier add param error' );
				return $result;
			}
		}
		if (! empty ( $addr )) {
			$addr = nns_db_common_class::nns_db_common_validation ( $addr );
			if ($addr === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier add param error' );
				return $result;
			}
		}
		$state = ( int ) $state;
		//生成GUID
		$guid = new Guid ();
		$id = $guid->toString ();
		//exist
		$str_sql = "select 1 from nns_carrier where nns_id='$id' or nns_name='$name'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_CARRIER_EXIST, 'carrier name exist' );
			return $result;
		}
		//INSERT
		$str_sql = "insert into nns_carrier(nns_id,nns_name,nns_state,nns_desc,nns_create_time,nns_modify_time,nns_contact,nns_telephone,nns_email,nns_addr)values('$id','$name',0,'$desc',now(),now(),'$contact','$telephone','$email','$addr')";
		$result = $this->db_obj->nns_db_execute ( $str_sql );
	    if ($result ["ret"] == 0) {
			$data ["id"] = $id;
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_OK, 'carrier add ok', $data );
		}
		
		//		创建仓库
		$depot_inst=new nns_db_depot_class();
		$depot_inst->nns_db_depot_modify($name,"",0,$id,0);
		$depot_inst->nns_db_depot_modify($name,"",0,$id,1);
		$depot_inst=null;
		
		return $result;
	}
	/**
	 * 
	 * 修改运营商
	 * @param string $id
	 * @param string $name
	 * @param int $state
	 * @param string $desc
	 */
	public function nns_db_carrier_modify($id, $name, $state = 0, $desc = null, $contact = null, $telephone = null, $email = null, $addr = null) {
		$result = array ();
		if (! isset ( $id ) || ! isset ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier modify param error' );
			return $result;
		}
		if (empty ( $id ) || empty ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier modify param error' );
			return $result;
		}
		//非法字符串检测
	    $id = nns_db_common_class::nns_db_common_validation ( $id, 1, 32, '', 1 );
		if($id === false){
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier modify param error' );
			return $result;
		}
		$name = nns_db_common_class::nns_db_common_validation ( $name );
		if ($name === false) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier modify param error' );
			return $result;
		}
		if (! empty ( $desc )) {
			$desc = nns_db_common_class::nns_db_common_validation ( $desc );
			if ($desc === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier modify param error' );
				return $result;
			}
		}
		if (! empty ( $contact )) {
			$contact = nns_db_common_class::nns_db_common_validation ( $contact );
			if ($contact === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier modify param error' );
				return $result;
			}
		}
		if (! empty ( $telephone )) {
			$telephone = nns_db_common_class::nns_db_common_validation ( $telephone );
			if ($telephone === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier modify param error' );
				return $result;
			}
		}
		if (! empty ( $email )) {
			$email = nns_db_common_class::nns_db_common_validation ( $email );
			if ($email === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier modify param error' );
				return $result;
			}
		}
		if (! empty ( $addr )) {
			$addr = nns_db_common_class::nns_db_common_validation ( $addr );
			if ($addr === false) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier modify param error' );
				return $result;
			}
		}
		//state
		$state = ( int ) $state;
		//name exist
		$str_sql = "select 1 from nns_carrier where nns_id != '$id' and nns_name='$name'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_CARRIER_EXIST, 'carrier name exist' );
			return $result;
		}
		//id exist
		$str_sql = "select * from nns_carrier";
//where nns_id='$id'		
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			//update
			$str_sql = "update nns_carrier set nns_name='$name',nns_state='$state',nns_desc='$desc',nns_contact='$contact',nns_telephone='$telephone',nns_email='$email',nns_addr='$addr',nns_modify_time=now() where nns_id='$id'";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
			
		} else {
			//insert
			$str_sql = "insert into nns_carrier(nns_id,nns_name,nns_state,nns_desc,nns_create_time,nns_modify_time,nns_contact,nns_telephone,nns_email,nns_addr)values('$id','$name',0,'$desc',now(),now(),'$contact','$telephone','$email','$addr')";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
		}
		return $result;
	}
	
	/**
	 * 
	 * 运营商信息...
	 * @param string $id
	 */
	public function nns_db_carrier_info($id) {
		$result = array ();
		if (! isset ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier info param error' );
			return $result;
		}
		if (empty ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier info param error' );
			return $result;
		}
		//非法字符串检测
		$id = nns_db_common_class::nns_db_common_validation ( $id, 1, 32, '', 1 );
		if($id === false){
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'carrier info param error' );
			return $result;
		}
		$str_sql = "select * from nns_carrier where nns_id='$id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_CARRIER_NOT_FOUND, 'carrier info not found' );
			}
		}
		return $result;
	}

}

?>