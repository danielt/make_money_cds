<?php
$nns_db_category_detail_dir = dirname ( dirname ( __FILE__ ) );
require_once $nns_db_category_detail_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";

class nns_db_category_detail_class {
	private $conn;
	private $db_obj;
	
	public function __construct() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
	}
    public function nns_db_category_detail_count($name = null, $category_list_id = null, $parent_id = null, $order = null) {
		$result = array ();
		$str_sql = "select count(1) as num from nns_category_detail ";
		$str_sql .= $this->nns_db_category_detail_where ( $name, $category_list_id, $parent_id, $order );
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_CATEGORY_DETAIL_NO_DATA, 'category detail data not find' );
			}
		}
		return $result;
	}
	public function nns_db_category_detail_list($name = null, $category_list_id = null, $parent_id = null, $order = null, $min = 0, $num = 0) {
		$result = array ();
		//$str_sql = "select * from nns_category_detail ";
		$str_sql = "SELECT nns_category_detail.*,b.parent_name FROM nns_category_detail  LEFT JOIN (SELECT nns_id,nns_name AS parent_name FROM nns_category_detail) b ON nns_category_detail.nns_parent_id = b.nns_id ";
		$str_sql .= $this->nns_db_category_detail_where ( $name, $category_list_id, $parent_id, $order, $min, $num );
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_CATEGORY_DETAIL_NO_DATA, 'category detail data not find' );
			}
		}
		return $result;
	
	}
	
	public function nns_db_category_detail_list_byid($id) {
		$result = array ();
		$str_sql = "select nns_parent_id from nns_category_detail where nns_id='$id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if($result["ret"]!=0){
			return $result;
		}
		if(count($result["data"])>0){
			$parent_id = $result["data"][0]["nns_parent_id"];		
	     	$result = $this->nns_db_category_detail_list_all($id,$parent_id,'');
		}		
		return $result;
	
	}
	
	public function nns_db_category_detail_info($id) {
		$result = array ();
		if (! isset ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'category info param error' );
			return $result;
		}
		if (empty ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'category info param error' );
			return $result;
		}
		$str_sql = "select * from nns_category_detail where nns_id='$id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_CATEGORY_DETAIL_NOT_FOUND, 'category info not found' );
			}
		}
		return $result;
	
	}
	public function nns_db_category_detail_add($id,$name, $state = 0, $category_list_id, $parent_id = '0', $desc = null) {
		$result = array ();
	    if (! isset ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'category detail add param error' );
			return $result;
		}
		if (empty ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'category detail add param error' );
			return $result;
		}
		$id = (int)$id;
		
		if($id>999 || $id<100){
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'category detail add param error' );
			return $result;
		}
		
		if (! isset ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'category detail add param error' );
			return $result;
		}
		if (empty ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'category detail add param error' );
			return $result;
		}
		
		$state = ( int ) $state;
		$category_list_id = (int)$category_list_id;
		$id = $category_list_id.$id;
	
		//exist 
		$str_sql = "select 1 from nns_category_detail where nns_category_list_id='$category_list_id' and ( nns_id='$id' or nns_name='$name')";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_CATEGORY_DETAIL_EXIST, 'category detail id or name exist' );
			return $result;
		}
		//INSERT
		$str_sql = "insert into nns_category_detail(nns_id,nns_name,nns_state,nns_category_list_id,nns_parent_id,nns_desc,nns_create_time,nns_modify_time)values('$id','$name',$state,'$category_list_id','$parent_id','$desc',now(),now()) ";
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		
		return $result;
	
	}
	
	public function nns_db_category_detail_modify($id, $name, $state = 0, $category_list_id, $parent_id = '0', $desc = null) {
		$result = array ();
		if (! isset ( $id ) || ! isset ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'category detail add param error' );
			return $result;
		}
		if (empty ( $id ) || empty ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'category detail add param error' );
			return $result;
		}
		$state = ( int ) $state;
		//name exist
		$str_sql = "select 1 from nns_category_detail where nns_category_list_id='$category_list_id' and (nns_id != '$id' and nns_name='$name')";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_CARRIER_EXIST, 'category name exist' );
			return $result;
		}
		//id exist
		$str_sql = "select * from nns_category_detail where nns_id='$id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			//update
			$str_sql = "update nns_category_detail set nns_name='$name',nns_state='$state',nns_category_list_id='$category_list_id',nns_parent_id='$parent_id',nns_desc='$desc',nns_modify_time=now() where nns_id='$id'";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
		} else {
			//insert
			$str_sql = "insert into nns_category_detail(nns_id,nns_name,nns_state,nns_category_list_id,nns_parent_id,nns_desc,nns_create_time,nns_modify_time)values('$id','$name',$state,'$category_list_id','$parent_id','$desc',now(),now()) ";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
		}
		return $result;
	
	}
	
	public function nns_db_category_detail_delete($id) {
		$result = array ();
		if (! isset ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'category detail delete param error' );
			return $result;
		}
		
		$str_sql = "delete from nns_category_detail where nns_id='$id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		
		return $result;
	}
	
	public function nns_db_category_detail_where($name = null, $category_list_id = null, $parent_id = null, $order = null, $min = 0, $num = 0) {
		$where = "";
		
		if (isset ( $name )) {
			if (! empty ( $name )) {
				if (empty ( $where )) {
					$where .= " where nns_name like '%$name%' ";
				} else {
					$where .= " and nns_name like '%$name%'";
				}
			}
		}
		if (isset ( $category_list_id )) {
			if (! empty ( $category_list_id )) {
				if (empty ( $where )) {
					$where .= " where nns_category_list_id = '$category_list_id' ";
				} else {
					$where .= " and nns_category_list_id = '$category_list_id'";
				}
			}
		}
		if (isset ( $parent_id )) {
			if ($parent_id !== null && $parent_id !== '') {
				if (empty ( $where )) {
					$where .= " where nns_parent_id = '$parent_id' ";
				} else {
					$where .= " and nns_parent_id = '$parent_id'";
				}
			}
		}
		
		if (isset ( $order )) {
			if (! empty ( $order )) {
				$where .= " order by nns_category_detail.$order desc";
			} else {
				$where .= " order by nns_category_detail.nns_id + 0 ";
			}
		}
		if (isset ( $min ) && isset ( $num )) {
			if (! empty ( $num )) {
				$min = (int)$min;
				$num = (int)$num;
				$where .= " limit $min,$num";
			}
		}
		return $where;
	}
	
	public function nns_db_category_detail_list_all($id,$parent_id = null,$data=null){
		$result = array();
		if(!isset($parent_id)){
			return $result;
		} 
		/*if($parent_id !== null && $parent_id !== ''){
			return $result;
		}
		*/
		$str_sql = "select * from nns_category_detail where nns_parent_id='$parent_id'";
		if(!empty($id)){
			$str_sql .= " and nns_id='$id'";
		}
		
		
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}		
		if ($this->db_obj->num_rows > 0) {
			if(!empty($data)){
				$data["data"] = array_merge($data["data"],$result["data"]);
			}
			else{
				$data = $result;
			}
			
			$id = $result ["data"][0]["nns_id"];			
			/*if ($id !== null && $id !== '') {
				$data=$this->nns_db_category_detail_list_all($id,$data);
			}*/
			$data=$this->nns_db_category_detail_list_all('',$id,$data);
		}	
		
		return $data;
	}

}

?>
