<?php
$nns_db_category_list_dir = dirname ( dirname ( __FILE__ ) );
require_once $nns_db_category_list_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";

class nns_db_category_list_class {
	private $conn;
	private $db_obj;
	
	public function __construct() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
	}
	public function nns_db_category_list_count($name = null, $type = null, $org_id = null, $order = null, $live = null) {
		$result = array ();
		$str_sql = "select count(1) as num from nns_category_list ";
		$str_sql .= $this->nns_db_category_list_where ( $name, $type, $org_id, $order, 0, 0, $live );
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_CATEGORY_NO_DATA, 'category list data not find' );
			}
		}
		return $result;
	}
	public function nns_db_category_list_list($name = null, $type = null, $org_id = null, $order = null, $min = 0, $num = 0, $live = null) {
		$result = array ();
		$str_sql = "select * from nns_category_list ";
		$str_sql .= $this->nns_db_category_list_where ( $name, $type, $org_id, $order, $min, $num, $live );
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_CATEGORY_NO_DATA, 'category list data not find' );
			}
		}
		return $result;
	
	}
	public function nns_db_category_list_info($id) {
		$result = array ();
		if (! isset ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'category info param error' );
			return $result;
		}
		if (empty ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'category info param error' );
			return $result;
		}
		$str_sql = "select * from nns_category_list where nns_id='$id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_CATEGORY_NOT_FOUND, 'category info not found' );
			}
		}
		return $result;
	
	}
	public function nns_db_category_list_add($name, $type, $state = 0, $org_id = null, $desc = null, $live = 0) {
		$result = array ();
		if (! isset ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'category list add param error' );
			return $result;
		}
		if (empty ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'category list add param error' );
			return $result;
		}
		$type = ( int ) $type;
		$state = ( int ) $state;
		$live = ( int ) $live;
		//$id
		$str_sql = "select max(nns_id+1) as id from nns_category_list";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		
		$id = '100000';
		if ($this->db_obj->num_rows > 0) {
			$data = $result [data];
			$id = $data [0] ["id"];
			if (empty ( $id )) {
				$id = '100000';
			}
		}
		//exist 
		$str_sql = "select 1 from nns_category_list where nns_id='$id' or nns_name='$name'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_CARRIER_EXIST, 'category id or name exist' );
			return $result;
		}
		//INSERT
		$str_sql = "insert into nns_category_list(nns_id,nns_name,nns_state,nns_type,nns_org_id,nns_desc,nns_create_time,nns_modify_time,nns_live)values('$id','$name',$state,$type,'$org_id','$desc',now(),now(),$live )";
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		
		return $result;
	
	}
	
	public function nns_db_category_list_modify($id, $name, $type, $state = 0, $org_id = null, $desc = null, $live = null) {
		$result = array ();
		if (! isset ( $id ) || ! isset ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'category add param error' );
			return $result;
		}
		if (empty ( $id ) || empty ( $name )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'category add param error' );
			return $result;
		}
		//name exist
		$str_sql = "select 1 from nns_category_list where nns_id != '$id' and nns_name='$name'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_CARRIER_EXIST, 'category name exist' );
			return $result;
		}
		$type = ( int ) $type;
		$state = ( int ) $state;
		$live = ( int ) $live;
		//id exist
		$str_sql = "select * from nns_category_list where nns_id='$id'";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			//update
			$str_sql = "update nns_category_list set nns_name='$name',nns_state='$state',nns_type=$type,nns_org_id='$org_id',nns_desc='$desc',nns_modify_time=now(),nns_live=$live where nns_id='$id'";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
		} else {
			//insert
			$str_sql = "insert into nns_category_list(nns_id,nns_name,nns_state,nns_type,nns_org_id,nns_desc,nns_create_time,nns_modify_time,nns_live)values('$id','$name',$state,$type,'$org_id','$desc',now(),now(),$live) ";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
		}
		return $result;
	
	}
	
	public function nns_db_category_list_delete($id) {
		$result = array ();
		if (! isset ( $id )) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_ARGS, 'category delete param error' );
			return $result;
		}
		
		$str_sql = "delete from nns_category_list where nns_id='$id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		
		$str_sql = "delete from nns_category_detail where nns_category_list_id='$id'";
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		
		return $result;
	}
	
	public function nns_db_category_list_where($name = null, $type = null, $org_id = null, $order = null, $min = 0, $num = 0, $live = null) {
		$where = "";
		
		if (isset ( $name )) {
			if (! empty ( $name )) {
				if (empty ( $where )) {
					$where .= " where nns_name like '%$name%' ";
				} else {
					$where .= " and nns_name like '%$name%'";
				}
			}
		}
		if (isset ( $type )) {
			if ($type !== null && $type !== '' && $type != - 1) {
				$type = ( int ) $type;
				if (empty ( $where )) {
					$where .= " where nns_type = $type ";
				} else {
					$where .= " and nns_type = $type";
				}
			}
		}
		if (isset ( $org_id )) {
			if (! empty ( $org_id )) {
				if (empty ( $where )) {
					$where .= " where nns_org_id = '$org_id' ";
				} else {
					$where .= " and nns_org_id = '$org_id'";
				}
			}
		}
		if (isset ( $live )) {
			if ($live !== null && $live !== '') {
				if (empty ( $where )) {
					$where .= " where nns_live = $live ";
				} else {
					$where .= " and nns_live = $live";
				}
			}
		}
		if (isset ( $order )) {
			if (! empty ( $order )) {
				$where .= " order by $order desc";
			} else {
				$where .= " order by nns_id + 0 desc";
			}
		}
		if (isset ( $min ) && isset ( $num )) {
			if (! empty ( $num )) {
				$min = ( int ) $min;
				$num = ( int ) $num;
				$where .= " limit $min,$num";
			}
		}
		return $where;
	}

}

?>
