<?php
$nns_db_manager_log_dir = dirname ( dirname ( __FILE__ ) );
require_once $nns_db_manager_log_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
require_once $nns_db_manager_log_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";

class nns_db_manager_log_class {
	private $conn;
	private $db_obj;
	
	public function __construct() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
	}
	
	public function nns_db_manager_log_add($op_id,$op_type,$op_time,$op_desc){
		$result = array();
		//生成GUID
		$guid = new Guid ();
		$id = $guid->toString ();		
		//exist
		$str_sql = "select 1 from nns_op_log where nns_id='$id'";
	    $result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result [ret] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_MANAGER_LOG_EXIST, 'manager log id exist' );
			return $result;
		}
		//insert
		$str_sql = "insert into nns_op_log(nns_id,nns_op_id,nns_op_type,nns_op_time,nns_op_desc)values('$id','$op_id','$op_type','$op_time','$op_desc')";
		$result = $this->db_obj->nns_db_execute($str_sql);
		return  $result;
		
	}
	
	public function nns_db_manager_log_count($op_id = '', $stime = '', $etime = ''){
		$result = array ();
		$str_sql = "select count(1)as num from nns_op_log ";
		$str_sql .= $this->nns_db_manager_log_where($op_id, $stime, $etime, 0, 0);
		//result
		$result = $this->db_obj->nns_db_query ( $str_sql );
		return $result;
	}
	
	public function nns_db_manager_log_list($op_id = '', $stime = '', $etime = '', $min = 0, $num = 0) {
		$result = array ();
		$str_sql = "select * from nns_op_log ";
		$str_sql .= $this->nns_db_manager_log_where($op_id, $stime, $etime, $min, $num);
		//result
		$result = $this->db_obj->nns_db_query ( $str_sql );
		
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_MANAGER_LOG_NO_DATA, 'manager log not find' );
			}
		}
		return $result;
	}
	
	public function nns_db_manager_log_where($op_id = '', $stime = '', $etime = '', $min = 0, $num = 0) {
		$where = "";
		if (isset ( $op_id )) {
			if (! empty ( $op_id )) {
				if (empty ( $where )) {
					$where .= " where nns_op_id='$op_id'";
				} else {
					$where .= " and nns_op_id='$op_id'";
				}
			}
		} //end if
		if (isset ( $stime ) && isset ( $etime )) {
			if (! empty ( $stime ) && empty ( $etime )) {
				if (empty ( $where )) {
					$where = " where nns_op_time >= '$stime'";
				} else {
					$where = " and nns_op_time >= '$stime'";
				}
			} else if (empty ( $etime ) && ! empty ( $etime )) {
				if (empty ( $where )) {
					$where = " where nns_op_time <= '$etime'";
				} else {
					$where = " and nns_op_time <= '$etime'";
				}
			} else {
				if (empty ( $where )) {
					$where = " where nns_op_time>='$stime' and nns_op_time <= '$etime'";
				} else {
					$where = " and nns_op_time>='$stime' and nns_op_time <= '$etime'";
				}
			}
		} //end if
		

		$where .= " order by nns_op_time desc ";
		
		if (isset ( $min ) && isset ( $num )) {
			if (! empty ( $num )) {
				$where .= " limit '$min','$num'";
			}
		}
		
		return $where;
	}
	
	public function __destruct() {
		unset ( $this->db_obj );
		unset ( $this->conn );
	}

}

?>