<?php

$nns_db_manager_dir = dirname(dirname(__FILE__));
require_once $nns_db_manager_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";

class nns_db_manager_class {

        private $conn;
        private $db_obj;

        public function __construct() {
                $this->db_obj = new nns_db_class(g_db_host, g_db_username, g_db_password, g_db_name);
                $this->conn = $this->db_obj->nns_db_connect();
        }

        public function nns_db_manager_modify_pwd($id, $old_pwd, $new_pwd) {
                $result = array();
                if (!isset($id) || !isset($old_pwd) || !isset($new_pwd)) {
                        $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'manager modify pwd param error');
                        return $result;
                }
                if (empty($id) || empty($old_pwd) || empty($new_pwd)) {
                        $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'manager modify pwd param error');
                        return $result;
                }
                //非法字符串检测
                $id = nns_db_common_class::nns_db_common_validation($id, 1, 32, 1);
                $old_pwd = nns_db_common_class::nns_db_common_validation($old_pwd);
                $new_pwd = nns_db_common_class::nns_db_common_validation($new_pwd, 1, 32, '', '', '', 1);
                if ($id === false || $old_pwd === false || $new_pwd === false) {
                        $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'manager modify pwd param error');
                        return $result;
                }

                //exist
                $str_sql = "select nns_id from nns_manager where nns_id='$id' and nns_password='$old_pwd'";
                $result = $this->db_obj->nns_db_query($str_sql);
                if ($result ["ret"] == 0) {
                        if ($this->db_obj->num_rows <= 0) {
                                $result = $this->db_obj->nns_db_result(NNS_ERROR_MANAGER_NOT_FOUND, 'manager id or pwd error');
                                return $result;
                        }
                }

                $str_sql = "update nns_manager set nns_password='$new_pwd' where nns_id='$id'";
                $result = $this->db_obj->nns_db_execute($str_sql);

                return $result;
        }

        /**
         * 
         * 管理员登录 ...
         * @param string $id
         * @param string $password
         */
        public function nns_db_manager_login($id, $password) {
                $result = array();
                if (!isset($id) || !isset($password)) {
                        $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, cms_get_lang('PASSWORD_ERROR'));
                        return $result;
                }
                if (empty($id) || empty($password)) {
                        $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, cms_get_lang('PASSWORD_ERROR'));
                        return $result;
                }

                //非法字符串检测
                $id = nns_db_common_class::nns_db_common_validation($id);
                $password = nns_db_common_class::nns_db_common_validation($password);

                if ($id === false || $password === false) {
                        $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, cms_get_lang('PASSWORD_ERROR'));
                        return $result;
                }

                //config system login 
                if ($id == g_sys_loginid) {
                        if ($password == g_sys_loginpwd) {
                                $result = $this->db_obj->nns_db_result(NNS_ERROR_OK, cms_get_lang('LOGIN_SUCCESS'));
                                return $result;
                        } else {
                                $result = $this->db_obj->nns_db_result(NNS_ERROR_CONFIG_LOGIN_NOT_FOUND, cms_get_lang('PASSWORD_ERROR'));
                                return $result;
                        }
                }

                //数据库管理员
                //$str_sql = "select a.* ,b.nns_pri_id,b.nns_name as role_name from nns_manager a left join nns_role b on a.nns_role_id=b.nns_id where a.nns_state=0 and a.nns_id='$id' and a.nns_password='$password'";
                $str_sql = " SELECT a.* , b.nns_pri_id,b.nns_name AS role_name FROM  (SELECT * FROM nns_manager WHERE nns_state=0 AND nns_id='$id' AND nns_password='$password' LIMIT 1) AS  a  LEFT JOIN nns_role b ON a.nns_role_id = b.nns_id";
                $result = $this->db_obj->nns_db_query($str_sql);
                if ($result ["ret"] == 0) {
                        if ($this->db_obj->num_rows <= 0) {
                                $result = $this->db_obj->nns_db_result(NNS_ERROR_MANAGER_LOGIN_NOT_FOUND, cms_get_lang('PASSWORD_ERROR'));
                        } else {
                                //login success,and modify login time,login count
                                $str_sql = "update nns_manager set nns_login_count=nns_login_count+1, nns_login_time=now() where nns_id='$id'";
                                $this->db_obj->nns_db_execute($str_sql);
                        }
                }
                return $result;
        }

        /**
         * 
         * 管理员统计总数 ...
         * @param string $name
         * @param string $partner_id
         * @param string $role_id
         */
        public function nns_db_manager_count($name = null, $org_id = null, $role_id = null, $type = null, $order = null) {

                $str_sql = "select count(1) as num from nns_manager ";
                $str_sql .= $this->nns_db_manager_where($name, $org_id, $role_id, $type, $order, 0, 0);
                //result
                $result = array();
                $result = $this->db_obj->nns_db_query($str_sql);

                return $result;
        }

        /**
         * 
         * 管理员列表（模糊查询） ...
         * @param string $name
         * @param string $org_id
         * @param string $role_id
         * @param int $type
         * @param string $order 排序字段
         * @param int $min  每页开始值
         * @param int $num  返回每页数
         */
        public function nns_db_manager_list($name = null, $org_id = null, $role_id = null, $type = null, $order = null, $min = 0, $num = 0) {
                $table = " select * from nns_manager ";
                $table .= $this->nns_db_manager_where($name, $org_id, $role_id, $type, $order, $min, $num);
                $str_sql = "select a.*,b.nns_pri_id,b.nns_name AS role_name from (" . $table . ") as a LEFT JOIN nns_role b ON a.nns_role_id = b.nns_id";
                //result
                $result = array();
                $result = $this->db_obj->nns_db_query($str_sql);
                if ($result ["ret"] == 0) {
                        if ($this->db_obj->num_rows <= 0) {
                                $result = $this->db_obj->nns_db_result(NNS_ERROR_MANAGER_NO_DATA, 'manager data not find');
                        }
                }
                return $result;
        }

        /**
         * 
         * 管理员详细信息 ...
         * @param string $id
         */
        public function nns_db_manager_info($id) {
                $result = array();
                if (!isset($id)) {
                        $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'manager info param error');
                        return $result;
                }
                if (empty($id)) {
                        $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'manager info param error');
                        return $result;
                }
                //非法字符串检测
                $id = nns_db_common_class::nns_db_common_validation($id, 1, 32, 1);

                $str_sql = "select * from nns_manager where nns_id='$id'";
                $result = $this->db_obj->nns_db_query($str_sql);
                if ($result ["ret"] == 0) {
                        if ($this->db_obj->num_rows <= 0) {
                                $result = $this->db_obj->nns_db_result(NNS_ERROR_MANAGER_NOT_FOUND, 'manager info not find');
                        }
                }

                return $result;
        }

        /**
         * 
         * 添加管理员 ...
         * @param string $id
         * @param string $name
         * @param string $password
         * @param string $partner_id
         * @param string $role_id
         * @param int $state
         * @return array
         */
        public function nns_db_manager_add($id, $name, $password, $type, $org_id, $role_id, $state = 0) {
                $result = array();
                if (!isset($id)) {
                        $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'add manager param error');
                        return $result;
                }
                if (empty($id)) {
                        $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'add manager param error');
                        return $result;
                }
                //非法字符串检测
                $id = nns_db_common_class::nns_db_common_validation($id, 1, 32, 1);
                $name = nns_db_common_class::nns_db_common_validation($name);
                $password = nns_db_common_class::nns_db_common_validation($password, 1, 32, '', '', '', 1);
                $org_id = nns_db_common_class::nns_db_common_validation($org_id, 1, 32, '', 1);
                $role_id = nns_db_common_class::nns_db_common_validation($role_id, 1, 32, '', 1);
                if ($id === false || $name === false || $password === false || $org_id === false || $role_id === false) {
                        $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'add manager param error');
                        return $result;
                }
                $type = (int) $type;
                $state = (int) $state;

                //exist
                $str_sql = "select 1 from nns_manager where nns_id='$id'";
                $result = $this->db_obj->nns_db_query($str_sql);
                if ($result ["ret"] != 0) {
                        return $result;
                }
                if ($this->db_obj->num_rows > 0) {
                        $result = $this->db_obj->nns_db_result(NNS_ERROR_ROLE_NO_DATA, 'manager id exist');
                        return $result;
                }
                //insert	
                $str_sql = "insert into nns_manager(nns_id,nns_name,nns_password,nns_org_id,nns_role_id,nns_state,nns_create_time,nns_modify_time,nns_type)values('$id','$name','$password','$org_id','$role_id',$state,now(),now(),$type)";
                $result = $this->db_obj->nns_db_execute($str_sql);

                return $result;
        }

        /**
         * 
         * 修改管理员信息...
         * @param string $id
         * @param string $name
         * @param string $password
         * @param string $partner_id
         * @param string $role_id
         * @param int $state
         */
        public function nns_db_manager_modify($id, $name, $password, $type, $org_id, $role_id, $state = 0) {
                $result = array();
                if (!isset($id)) {
                        $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'modifiy manager param error');
                        return $result;
                }
                if (empty($id)) {
                        $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'modify manager param error');
                        return $result;
                }
                //非法字符串检测
                $id = nns_db_common_class::nns_db_common_validation($id, 1, 32, 1);
                $name = nns_db_common_class::nns_db_common_validation($name);
                $password = nns_db_common_class::nns_db_common_validation($password, 1, 32, '', '', '', 1);
                $org_id = nns_db_common_class::nns_db_common_validation($org_id, 1, 32, '', 1);
                $role_id = nns_db_common_class::nns_db_common_validation($role_id, 1, 32, '', 1);
                if ($id === false || $name === false || $password === false || $org_id === false || $role_id === false) {
                        $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'add manager param error');
                        return $result;
                }
                $type = (int) $type;
                $state = (int) $state;

                //exist
                $str_sql = " select nns_password from nns_manager where nns_id='$id'";
                $result = $this->db_obj->nns_db_query($str_sql);
                if ($result ["ret"] != 0) {
                        return $result;
                }
                if (empty($password)) {
                        $password = $result["data"][0]["nns_password"];
                }
                if ($this->db_obj->num_rows <= 0) {
                        //insert
                        $str_sql = "insert into nns_manager(nns_id,nns_name,nns_password,nns_org_id,nns_role_id,nns_state,nns_create_time,nns_modify_time,nns_type)values('$id','$name','$password','$org_id','$role_id',$state,now(),now(),$type)";
                        $result = $this->db_obj->nns_db_execute($str_sql);
                }

                //modify
                $str_sql = "update nns_manager set nns_name='$name',nns_password='$password',nns_org_id='$org_id',nns_role_id='$role_id',nns_state=$state,nns_modify_time=now(),nns_type=$type where nns_id='$id'";
                $result = $this->db_obj->nns_db_execute($str_sql);

                return $result;
        }

        /**
         * 
         * 删除管理员..
         * @param string $id
         */
        public function nns_db_manager_delete($id) {
                $result = array();
                if (!isset($id)) {
                        $result = $this->db_obj->nns_db_result(NNS_ERROR_ARGS, 'delete manager param error');
                        return $result;
                }

                $str_sql = "delete from nns_manager where nns_id='$id'";
                $result = $this->db_obj->nns_db_execute($str_sql);

                return $result;
        }

        /**
         * 
         * 构造sql ...
         * @param string $name
         * @param string $partner_id
         * @param string $role_id
         * @param string $order
         * @param int $min
         * @param int $num
         */
        public function nns_db_manager_where($name, $org_id, $role_id, $type, $order, $min, $num) {
                //组合sql
                $where = "";

                if (isset($name)) {
                        if (!empty($name)) {
                                $name = nns_db_common_class::nns_db_common_validation($name);
                                if ($name !== false) {
                                        if (empty($where)) {
                                                $where .= " where nns_name like '%$name%' ";
                                        } else {
                                                $where .= " and nns_name like '%$name%'";
                                        }
                                }
                        }
                }
                if (isset($org_id)) {
                        if (!empty($org_id)) {
                                $org_id = nns_db_common_class::nns_db_common_validation($org_id, 1, 32, '', 1);
                                if ($org_id !== false) {
                                        if (empty($where)) {
                                                $where .= " where nns_org_id='$org_id'";
                                        } else {
                                                $where .= " and nns_org_id='$org_id'";
                                        }
                                }
                        }
                }
                if (isset($role_id)) {
                        if (!empty($role_id)) {
                                $role_id = nns_db_common_class::nns_db_common_validation($role_id, 1, 32, '', 1);
                                if ($role_id !== false) {
                                        if (empty($where)) {
                                                $where .= " where nns_role_id='$role_id'";
                                        } else {
                                                $where .= " and nns_role_id='$role_id'";
                                        }
                                }
                        }
                }
                if (isset($type)) {
                        if ($type !== null && $type !== '') {
                                $type = (int) $type;
                                if (empty($where)) {
                                        $where .= " where nns_type=$type";
                                } else {
                                        $where .= " and nns_type=$type";
                                }
                        }
                }
                if (isset($order)) {
                        if (!empty($order)) {
                                $where .= " order by $order desc";
                        } else {
                                $where .= " order by nns_modify_time desc";
                        }
                }
                if (isset($min) && isset($num)) {
                        if (!empty($num)) {
                                $min = (int) $min;
                                $num = (int) $num;
                                $where .= " limit $min,$num";
                        }
                }

                return $where;
        }

}

?>