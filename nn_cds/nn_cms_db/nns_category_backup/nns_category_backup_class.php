<?php
$nns_db_category_backup_dir = dirname ( dirname ( __FILE__ ) );
require_once $nns_db_category_backup_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";
require_once $nns_db_category_backup_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
/*
 * Created on 2012-7-6
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class nns_category_backup_class {
	private $conn;
	private $db_obj;
	function nns_category_backup_class() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
	}

	public function backup_categoty($content,$type,$category_id=null,$org_type=null,$org_id=null){
		$guid = new Guid ();
		$id = $guid->toString ();
		$org_type=(int)$org_type;
		$sql_str="insert into nns_category_backup ";
		$sql_str.="(nns_id,nns_category_type,nns_category,nns_category_id,nns_org_type,nns_org_id,nns_backup_time) values ";
		$sql_str.="('$id','$type','$content','$category_id',$org_type,'$org_id',now()) ";
		
		$result = $this->db_obj->nns_db_execute ( $sql_str );
		return $result;
	}
	
	public function backup_count(){
		$sql_str="select count(1) as num from nns_category_backup ";
		$result = $this->db_obj->nns_db_query ( $sql_str );
		return $result;
	}
}
?>
