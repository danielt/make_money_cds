<?php
$nns_db_depot_dir = dirname ( dirname ( __FILE__ ) );
require_once $nns_db_depot_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";
require_once $nns_db_depot_dir . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";

class nns_db_depot_class {
	private $conn;
	private $db_obj;
	
	public function __construct() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
	}
	
	public function nns_db_depot_count($name, $org_type, $org_id, $depot_type) {
		$result = array ();
		$str_sql = "select count(1) as num from nns_depot ";
		$str_sql .= $this->nns_db_depot_where ( 0, 0, $name, $org_type, $org_id, $depot_type );
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_DEPOT_NO_DATA, 'depot list data not find' );
			}
		}
		return $result;
	}
	
	public function nns_db_depot_list($name, $org_type, $org_id, $depot_type) {
		$result = array ();
		$str_sql = "select * from nns_depot ";
		$str_sql .= $this->nns_db_depot_where ( 0, 0, $name, $org_type, $org_id, $depot_type );
		//echo ($str_sql);exit;
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] == 0) {
			if ($this->db_obj->num_rows <= 0) {
				$result = $this->db_obj->nns_db_result ( NNS_ERROR_DEPOT_NO_DATA, 'depot list data not find' );
			}
		}
		return $result;
	}
	
	public function nns_db_depot_info($org_type, $org_id, $depot_type){
		$str_sql = "select * from nns_depot where nns_type=$depot_type and nns_org_type=$org_type";
		if (!empty($org_id)){
			$str_sql.=" and nns_org_id='$org_id'";
		}
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows <= 0) {
			$result = $this->db_obj->nns_db_result ( NNS_ERROR_DEPOT_NO_DATA, 'depot list data not find' );
		}
		
		return $result;		
	}
	
	public function nns_db_depot_modify($name, $content, $org_type, $org_id, $depot_type) {
		$result = array ();
		//select 
		$str_sql = "select * from nns_depot where nns_type=$depot_type and nns_org_type=$org_type and nns_org_id='$org_id' ";
		$result = $this->db_obj->nns_db_query ( $str_sql );
		if ($result ["ret"] != 0) {
			return $result;
		}
		if ($this->db_obj->num_rows > 0) {
			//update
			$str_sql = "update nns_depot set nns_name='$name',nns_category='$content'  where nns_type=$depot_type and nns_org_type=$org_type and nns_org_id='$org_id' ";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
		} else {
			//insert
			$guid = new Guid ();
			$id = $guid->toString ();
			if (empty($content)){
				$content='<?xml version="1.0"?><depot/>';
			}
			$str_sql = "insert into nns_depot(nns_id,nns_name,nns_category,nns_org_type,nns_org_id,nns_type,nns_create_time,nns_modify_time)values('$id','$name','$content',$org_type,'$org_id',$depot_type,now(),now())";
			$result = $this->db_obj->nns_db_execute ( $str_sql );
		}
		
		return $result;
	}
	
	public function nns_db_depot_delete($id,$org_type, $org_id){
		if (empty($id)){
			$str_sql="delete from nns_depot where nns_org_type=$org_type and nns_org_id='$org_id'";
		}else{
			$str_sql="delete from nns_depot where nns_id='$id'";
		}
		$result = $this->db_obj->nns_db_execute ( $str_sql );
		
		return $result;
	}
	
	public function nns_db_depot_where($min, $num, $name, $org_type, $org_id, $depot_type) {
		$where = "";
		
		if (isset ( $name )) {
			if (! empty ( $name )) {
				if (empty ( $where )) {
					$where .= " where nns_name like '%$name%' ";
				} else {
					$where .= " and nns_name like '%$name%'";
				}
			}
		}
		if (isset ( $org_type )) {
			if ($org_type !== null && $org_type !== '' && $org_type != - 1) {
				$org_type = ( int ) $org_type;
				if (empty ( $where )) {
					$where .= " where nns_org_type = $org_type ";
				} else {
					$where .= " and nns_org_type = $org_type";
				}
			}
		}
		if (isset ( $org_id )) {
			if (! empty ( $org_id )) {
				if (empty ( $where )) {
					$where .= " where nns_org_id = '$org_id' ";
				} else {
					$where .= " and nns_org_id = '$org_id'";
				}
			}
		}
		if (isset ( $depot_type )) {
			if ($depot_type !== null && $depot_type !== '' && $depot_type != - 1) {
				$depot_type = ( int ) $depot_type;
				if (empty ( $where )) {
					$where .= " where nns_type = $depot_type ";
				} else {
					$where .= " and nns_type = $depot_type";
				}
			}
		}
		if (isset ( $min ) && isset ( $num )) {
			if (! empty ( $num )) {
				$min = ( int ) $min;
				$num = ( int ) $num;
				$where .= " limit $min,$num";
			}
		}
		return $where;
	}

}

?>