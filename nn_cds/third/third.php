<?php
set_time_limit(0);
ini_set('display_errors', 0);
header("Content-type:text/html; charset=utf-8");
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/mgtv_init.php';
$arr_func_dir = array(
    'get_third_playurl'=>array(
        'dir'=>dirname(__FILE__).'/playurl.php',
        'class'=>'ip_ipqam_playurl',
        'func'=>'init',
    ),
);
function return_data($resultCode=7, $resultMsg='', $onDemandURL='')
{
    return array (
        'resultCode' => $resultCode,
        'resultMsg' => $resultMsg,
        'onDemandURL' => $onDemandURL
    );
}
$ip = i_get_ip();
$str_func = $_REQUEST['nns_func'] = (isset($_REQUEST['nns_func']) && strlen($_REQUEST['nns_func'])>0) ? $_REQUEST['nns_func']:'default';
unset($_REQUEST['nns_func']);
if(!in_array($str_func, array_keys($arr_func_dir)))
{
    echo json_encode(return_data(7,'nns_func参数错误'));die;
}
if(!file_exists($arr_func_dir[$str_func]['dir']))
{
    echo json_encode(return_data(7,'内部错误'));die;
}
include_once $arr_func_dir[$str_func]['dir'];
$obj_class = new $arr_func_dir[$str_func]['class']($_REQUEST);
$arr_function = get_class_methods($obj_class);
$arr_function = is_array($arr_function) ? $arr_function : array();
if(!in_array($arr_func_dir[$str_func]['func'], $arr_function))
{
    $result = return_data(7,'内部错误');
}
else
{
    $result = $obj_class->$arr_func_dir[$str_func]['func']();
}
echo json_encode($result);die;