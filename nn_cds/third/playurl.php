<?php
class ip_ipqam_playurl
{
    public $str_ip_url = 'http://172.31.183.124/getRequestPlayUrl';
    
    public $arr_params = null;
    
    public $arr_ipqam_params = array(
        'need'=>array(
            'nns_user_code'=>array(
                'name'=>'用户标示',
                'param'=>'userCode',    
            ),
            'nns_asset_id'=>array(
                'name'=>'资产标识',
                'param'=>'assetID',    
            ),
            'nns_sp_code'=>array(
                'name'=>'SP编号',
                'param'=>'spCode',    
            ),
            'nns_target_system_id'=>array(
                'name'=>'门户编号',
                'param'=>'targetSystemID',    
            ),
        ),
        'no_need'=>array(
            'nns_start_time'=>array(
                'name'=>'开始播放时间',
                'param'=>'starttime',    
            ),
            'nns_end_time'=>array(
                'name'=>'结束播放时间',
                'param'=>'endtime',    
            ),
            'nns_is_ad'=>array(
                'name'=>'是否请求广告系统',
                'param'=>'isAd',    
            ),
            'nns_stb_id'=>array(
                'name'=>'机顶盒序列号',
                'param'=>'stbid',    
            ),
            'nns_device_type'=>array(
                'name'=>'请求设备类型',
                'param'=>'devicetype',    
            ),
            'nns_extend1'=>array(
                'name'=>'扩展字段1',
                'param'=>'extend1',    
            ),
            'nns_extend2'=>array(
                'name'=>'扩展字段2',
                'param'=>'extend2',    
            ),
            'nns_extend3'=>array(
                'name'=>'扩展字段3',
                'param'=>'extend3',    
            ),
            'nns_content_name'=>array(
                'name'=>'影片名称',
                'param'=>'contentname',    
            ),
            'nns_try_flag'=>array(
                'name'=>'试用标记',
                'param'=>'tryFlag',    
            ),
        ),
    );
    
    public function __construct($params)
    {
        $this->arr_params = $params;
    }
    
    public function public_get_curl_content($play_url , $time = 30)
    {
        //curl抓取 远程文件的内容
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $play_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $time);
        $content = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        unset($ch);
        //如果内容为空  或者根本没这个xml文件
        if (empty($content) || !$content || $http_code >= 400)
        {
            return $this->return_data(8, "curl失败地址:" . $play_url . '超时时间:' . $time);
        }
        if(!$this->is_json($content))
        {
            return $this->return_data(9, "curl失败地址:" . $play_url . '未知错误');
        }
        $content = json_decode($content,true);
        $resultCode=(isset($content['resultCode']) && strlen($content['resultCode']) >0) ? $content['resultCode'] : 9;
        $resultMsg=(isset($content['resultMsg']) && strlen($content['resultMsg']) >0) ? $content['resultMsg'] : "curl失败地址:" . $play_url . '未知错误';
        $onDemandURL=(isset($content['onDemandURL']) && strlen($content['onDemandURL']) >0) ? $content['onDemandURL'] : '';
        return $this->return_data($resultCode,$resultMsg."请求地址{$play_url}", $onDemandURL);
    }
    
    
    public function get_ipqam_url()
    {
        if(!isset($this->arr_params['nns_asset_id']) || strlen($this->arr_params['nns_asset_id'])>32)
        {
            return $this->return_data(7,$this->arr_ipqam_params['need']['nns_asset_id'].'参数长度>32或者为空');
        }
        return $this->return_data(0,'OK', 'http://172.31.183.124/getRequestPlayUrl?nns_asset_id='.$this->arr_params['nns_asset_id']);
    }
    
    public function init()
    {
        $nns_type = $this->arr_params['nns_type'] = (isset($this->arr_params['nns_type']) && strlen($this->arr_params['nns_type'])>0) ? $this->arr_params['nns_type'] : '0';
        unset($this->arr_params['nns_type']);
        $nns_type = in_array($nns_type, array('0','1')) ? $nns_type : '0';
        if($nns_type == '1')
        {
            return $this->get_ipqam_url();
        }
        return $this->get_ip_url();
    }
    
    public function return_data($resultCode=7, $resultMsg='', $onDemandURL='')
    {
        return array (
            'resultCode' => $resultCode,
            'resultMsg' => $resultMsg,
            'onDemandURL' => $onDemandURL
        );
    }
    
    public function get_ip_url()
    {
        $last_params=null;
        foreach ($this->arr_ipqam_params['need'] as $key=>$value)
        {
            if(!isset($this->arr_params[$key]) || strlen($this->arr_params[$key])<1)
            {
                return $this->return_data(7,$value['name'].'参数不符合规范');
            }
            $last_params[$key] = $this->arr_params[$key];
        }
        foreach ($this->arr_ipqam_params['no_need'] as $key=>$value)
        {
            if(isset($this->arr_params[$key]) && strlen($this->arr_params[$key])>0)
            {
                $last_params[$key] = $this->arr_params[$key];
            }
        }
        if(!isset($last_params['nns_user_code']) || strlen($last_params['nns_user_code'])>32)
        {
            return $this->return_data(7,$this->arr_ipqam_params['need']['nns_user_code'].'参数长度>32或者为空');
        }
        if(!isset($last_params['nns_asset_id']) || strlen($last_params['nns_asset_id'])>20)
        {
            return $this->return_data(7,$this->arr_ipqam_params['need']['nns_asset_id'].'参数长度>20或者为空');
        }
        if(!isset($last_params['nns_sp_code']) || strlen($last_params['nns_sp_code'])>32)
        {
            return $this->return_data(7,$this->arr_ipqam_params['need']['nns_sp_code'].'参数长度>32或者为空');
        }
        if(!isset($last_params['nns_target_system_id']) || strlen($last_params['nns_target_system_id'])>32)
        {
            return $this->return_data(7,$this->arr_ipqam_params['need']['nns_target_system_id'].'参数长度>32或者为空');
        }
        if(isset($last_params['nns_is_ad']) && strlen($last_params['nns_is_ad'])>0)
        {
            $last_params['nns_is_ad'] = in_array($last_params['nns_is_ad'], array('0','1')) ? $last_params['nns_is_ad'] : '0';
        }
        if(isset($last_params['nns_stb_id']) && strlen($last_params['nns_stb_id'])>0 && strlen($last_params['nns_stb_id'])>64 && isset($last_params['nns_is_ad']) && $last_params['nns_is_ad']=='1')
        {
            return $this->return_data(7,$this->arr_ipqam_params['no_need']['nns_stb_id'].'参数长度>64');
        }
        if(!isset($last_params['nns_device_type']) || !in_array($last_params['nns_device_type'], array('0','1','2','3','4','5','6')))
        {
            $last_params['nns_device_type'] = 1;
        }
        if(isset($last_params['nns_try_flag']) && strlen($last_params['nns_try_flag'])>0)
        {
            $last_params['nns_try_flag'] = in_array($last_params['nns_try_flag'], array('0','1')) ? $last_params['nns_try_flag'] : '0';
        }
        $play_url=$this->str_ip_url;
        $play_url = rtrim($play_url,'?').'?';
        foreach ($last_params as $key=>$value)
        {
            if(strlen($value)<1)
            {
                continue;
            }
            if(isset($this->arr_ipqam_params['need'][$key]))
            {
                $play_url.="{$this->arr_ipqam_params['need'][$key]['param']}={$value}&";
            }
            else if(isset($this->arr_ipqam_params['no_need'][$key]))
            {
                $play_url.="{$this->arr_ipqam_params['no_need'][$key]['param']}={$value}&";
            }
        }
        $play_url = rtrim($play_url,'&');
        return $this->public_get_curl_content($play_url);
    }
    
    public function is_json($string)
    {
        if(!is_string($string))
        {
            return false;
        }
        $string = strlen($string) <1 ? '' : $string;
        if(strlen($string)<1)
        {
            return false;
        }
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}