<?php
/*
 * Created on 2012-7-10
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
  // BOSS计费模式
$g_boss_mode="0";
 //打开直播
$g_live_enabled = "1";
//打开直播节目单
$g_live_bill_enabled = "1";

//web模式 打开模板统计和模板管理
$g_web_mode = "0";

//合作伙伴
$g_partner_enabled=0;
//集团
$g_group_enabled=0;

$g_seekpoint_enabled=1;

//元数据功能
$g_metadata_enabled=1;


//data数据同步目录,分号隔开
$g_syn_data_path="";


//语言
$g_current_language = 'zh_CN';
$g_extend_language = 'en_US';


$g_image_host='';

$g_playbill_exists_day=3;



$g_inject_enabled = 0;


$g_bk_enabled = 1;//福建移动播控
//多语言配置 以cms根目录为准
$g_lang_path = array(
	'zh_CN'=>'/nn_cms_manager/languages/default/lang_zh_CN.ini',
	'en_US'=>'/nn_cms_manager/languages/default/lang_en_US.ini',
);

$g_lang_order = array(
	'zh_CN',
	'en_US',
);


$g_stills_enabled=0;
include "child_config/nn_cms_config_mgtv_fjyd.php";
 class g_cms_config{
	static public function get_g_boss_mode(){
		return self::get_g_config_value('g_boss_mode');
	}
	static public function get_g_partner_enabled(){
		return self::get_g_config_value('g_partner_enabled');
	}
	static public function get_g_comment_enabled(){
		return self::get_g_config_value('g_comment_enabled');
	}
	static public function get_g_current_language(){
		return self::get_g_config_value('g_current_language');
	}

	static public function get_g_extend_language(){
		return self::get_g_config_value('g_extend_language');
	}
	static public function get_g_mall_service_enabled(){
		return self::get_g_config_value('g_mall_service_enabled');
	}
	static public function get_g_stills_enabled(){
		return self::get_g_config_value('g_stills_enabled');
	}
	static public function get_g_web_ex_enabled(){
		return self::get_g_config_value('g_web_ex_enabled');
	}

	static public function get_g_config_value($key){
		global $$key;
		if (empty($$key)) $$key=0;
		return $$key;
	}

}
?>