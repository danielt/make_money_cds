<?php
//定时器日志清理的相关配置
$g_clear_log = array
(
    //全局错误日志清理配置
    'global_error_log'=>array(
        //是否开启日志清理0开启,否则关闭
        'enable'=>0,
        //清理多少天前的日志
        'clear_day_before_now'=>180
    ),
    //EPG注入日志清理配置
    'import_epg_log'=>array(
        'enable'=>0,
        //清理消息类型 -1失败消息 1成功消息 其它或者未配置清理所有消息
        'clear_data_fail_or_suc' => 999,
        'clear_day_before_now'=>180
    ),
    //CDN注入日志清理配置
    'c2_log'=>array(
        'enable'=>0,
        //清理消息类型 -1失败消息 1成功消息 2未通知消息 其它或者未配置清理所有消息
        'clear_data_fail_or_suc' => 999,
        'clear_day_before_now'=>180
    ),
    //消息注入日志清理配置
    'import_log'=>array(
        'enable'=>0,
        //清理消息类型 -1失败消息 1成功消息 其它或者未配置清理所有消息
        'clear_data_fail_or_suc' => 999,
        'clear_day_before_now'=>180
    ),
    //中心同步指令日志清理配置
    'op_log'=>array(
        'enable'=>0,
        //清理多少天前的日志
        'clear_day_before_now'=>180
    ),
    'clip_task_log'=>array(
        'enable'=>1,
        'clear_day_before_now'=>180
    ),
);

//定时器message表相关清理
$g_clear_message = array
(
    0 => array(
        //是否开启日志清理0开启,否则关闭
        'enable'=>0,
         //清理所属cp,多个以,分隔,未配置为清理所有cp
        'cp_id'=>"CNTV",
        //清理消息类型 -1失败消息 0其它状态消息 1成功消息  其它或者未配置清理所有消息
        'clear_data_fail_or_suc' =>999,
        'clear_day_before_now'=>180),
    1 => array(
        //是否开启日志清理0开启,否则关闭
        'enable'=>0,
        //清理所属cp,多个以,分隔,未配置为清理所有cp
        'cp_id'=>"YANHUACMS",
        //清理消息类型 -1失败消息 0其它状态消息 1成功消息  其它或者未配置清理所有消息
        'clear_data_fail_or_suc' =>999,
        'clear_day_before_now'=>180)
);

/**
 * 广西异步反馈消息地址
 * array(
 * )
 */
$gx_notify_url = array(
    'http://127.0.0.1/index.php?m=api&c=index'
);
//播控资源库栏目走新版v2,0否，1是
$g_category_v2 = 1;
