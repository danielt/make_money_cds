<?php
/*
 * Created on 2012-6-28
 * 访问统计数据库配置
 */
include_once dirname(dirname(__FILE__) ).DIRECTORY_SEPARATOR."nn_cms_config.php";
define( "NN_STAT_DB_HOST", g_db_host );
define( "NN_STAT_DB_USER", g_db_username );
define( "NN_STAT_DB_PWD", g_db_password );
define( "NN_STAT_DB_NAME", g_db_name );
