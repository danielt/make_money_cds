<?php
/**
 * CP消息注入队列过滤
 */
//媒资消息过滤
//112-IPTV轮播   9-财经  83-电视剧  84-电影  88-动漫  92-广告  85-纪实    97-教育     113-快乐购    102-品牌专区
//100-生活       98-体育    101-微电影  86-音乐  87-综艺
$g_cp_video_filter = array(
	//'view_type' => array(112,100,86),
);
//分集消息过滤
$g_cp_index_filter = array(
	
);

//片源消息过滤
$g_cp_media_filter = array(
	//'media_mode' => array('sd','4'),//片源模式
	//'videoformat' => array('h265'),//片源格式
);
// 透传队列操作类型
$g_pass_queue_action_type = array(
		'栏目同步',
		'栏目推荐',
		'CP同步',
		'栏目图片同步',
		'媒资融合',
		'媒资打包同步',
		'录制节目单',
        '点播锁定',
    '98' => '节目单同步',
    '99' => '直播锁定',
    '94' => '花絮预告绑定',
    '104'=>'明星库同步',
);
//注入状态查询状态清单
$g_import_status = array(
		'注入完成',
		'等待注入',
		'正在注入',
		'等待注入下游',
		'正在注入CDN',
		'注入CDN失败',
		'等待注入EPG',
		'注入EPG失败',
		'等待下载',
		'正在下载',
		'下载失败',
		'正在切片',
		'切片失败',
);
//媒资类型
$g_video_main_type = array (
      0 => '电影',
      1 => '电视剧',
      2 => '综艺',
      3 => '动漫',
      4 => '音乐',
      5 => '纪实',
      6 => '教育',
      7 => '体育',
      8 => '生活',
      9 => '财经',
      10 => '微电影',
      11 => '品牌',
      12 => '广告',
      13 => '新闻',
      14 => '公开课',
      15 => '外语节目',
      16 => '青少年',
      17 => '博客',
      18 => '游戏',
      19 => '原创',
      20 => '公益',
      21 => '政治',
      22 => '军事',
      23 => '法律',
      24 => '文教卫生体育',
      25 => '文艺',
      26 => '科技人文',
      27 => '其他',
      28 => '舞蹈',
      29 => '三农',
      30 => '少儿',
);
/**
 * 老版本V2注入来源清单
 * array(
 * 	'来源ID' => array('来源目录名称','类名称'),
 * )
 * 如来源文件为：nn_cds/api_v2/control/wenguang/sync_source.php  CLASS类名称为：wenguang_sync_source
 */
$g_import_sourcce_v2 = array(
		'0'     =>     array('wenguang','wenguang_sync_source'),                //文广注入模式
		'1'     =>     array('zhongguang','comm_sync_source'),                  //中广注入模式/视达科通用注入模式
		'2'     =>     array('adi_packet','adi_packet_sync_source'),            //ADI报文模式（贵州广电使用）
		'3'     =>     array('metadata_packet','metadata_packet_sync_source'),  //同洲ADI报文模式（贵州广电使用）
		'4'     =>     array('cntv','cntv_packet_sync_source'),                 //爱上-SOAP模式（央视国际标准注入模式）
		'5'     =>     array('ftp_packet','ftp_packet_sync_source'),            //FTP-ADI扫描模式，类似于ADI1.1（贵州广电使用）
		'6'     =>     array('unicom','unicom_sync_source'),                    //联通解耦测试使用模式
		'7'     =>     array('bk_inner','bk_sync_source'),                      //播控注入播控使用模式
		'8'     =>     array('telecom','telecom_sync_source'),                  //电信测试电信模式
		'9'     =>     array('telecom_yueme','telecom_yueme_sync_source'),      //电信测试悦me模式
		'10'    =>     array('unicom_d3','unicom_sync_source'),                 //通集约化测试使用模式
		'11'    =>     array('standard','standard_sync_source'),                //视达科上游注入标准模式
		'12'    =>     array('cdv','cdv_sync_source'),                          //广西新奥特-SOAP模式(央视国际标准注入模式)
		'13'    =>     array('standard_v1','standard_v1_sync_source'),          //视达科上游注入标准模式_v1版本（贵州广电在用）
        '14'    =>     array('telecom_fenghuo','telecom_fenghuo_sync_source'),  //电信测试烽火模式
        '500'   =>     array('hljiptv', 'hljiptv_sync_source'),                 //黑龙江IPTV模式（IPTV标准C2模式）
        '501'   =>     array('sobey', 'sobey_sync_source'),                     //索贝媒资注入模式
		'991'   =>     array('adi_v2_packet','adi_v2_packet_sync_source'),      //ADI1.1 与CMS的ADI注入内容一致
		'992'   =>     array('ngb_packet','ngb_packet_sync_source'),            //NGB注入（贵州广电使用）
		'993'   =>     array('huan_packet','huan_packet_sync_source'),          //欢网录制节目单注入（贵州广电使用）
		'994'   =>     array('mgtv_v2','mgtv_v2_sync_source'),                  //芒果运营商-MQ标准注入模式
		'995'   =>     array('mgtv_cdn_fuse','mgtv_cdn_fuse_sync_source'),      //芒果运营商-湖南电信（融合双CDN模式）
		'996'   =>     array('mgtv_cdn_fusion_feedback','mgtv_cdn_fusion_feedback_sync_source'),//芒果运营商-湖南电信（CMS融合后反馈播控生成队列模式）
        '998'   =>     array('bk_inner','bk_sync_source'),                      //播控注入播控使用模式
		'999'   =>     array('mgtv','mgtv_sync_source'),                        //芒果运营商-老模式
);

/**
 * CDN注入分派方法
 * array(
 * 	'分发ID' => '分发文件',
 * )
 */
$g_cdn_assign = array(
		'1' => 'unicom',//中国联通集采
		'2' => 'msp',//视达科MSP注入模式
		'3' => 'jscn_homed',//江苏广电茁壮注入模式
		'4' => 'standard',//视达科标准注入CDN模式
		'5' => 'ftp_push',//江苏昆山注入模式
		'6' => 'qx_operator',//江西有线-趣享
        '7' => 'gx_unify',//广西-注入新奥特FTP ADI1.1文件格式
        '8' => 'all_operative',//趣享cdn注入模式
	    '9' => 'qx_gxmovement',	//趣享-广西移动CDN注入
	    '10' => 'jscn_suzhou',	//江苏苏州OTT-CDN注入
	    '11' => 'jscn_sihua',	//江苏思华OTT-CDN注入
	    '12' => 'jscn_kukai',	//江苏酷开下游CMS业务平台注入
	    '13' => 'jscn_sihua_vod_middle',	//江苏思华VOD-CDN 中间过度版本注入
	    '14' => 'jscn_sihua_old',	//江苏OTT-CDN注入（老版本）
	    '15' => 'jscn_sihua_new',	//江苏vod-OTT-CDN注入（新版本）
	    '16' => 'jscn_sihua_vod',	//江苏vod-CDN注入（新版本）
		'17' => 'all_operative_v2', //趣享运营商云平台统一分发注入
        '18'=> 'qx_jxmovement',//趣享-江西移动CDN注入
);