<?php

/**
 * 消息开关
 * @var string
 */
#消息开关
define("MS_MESSAGE_ENABLE", true);
#sql日志开关
define("MS_SQL_ENABLE", true);
#debug日志追踪
define("MS_DEBUG_ENABLE", true);
/**
 * 模块媒资类型部分
 * @var string
 */
#分集
define("VIDEO_TYPE_INDEX", 'index');
#片源
define("VIDEO_TYPE_MEDIA", 'media');
#主媒资
define("VIDEO_TYPE_VIDEO", 'video');
/**
 * 主模块部分
 * @var string
 */
#MSP发送模块
define("LOG_MODEL_MSP", 'msp_log');
#cdn发送模块
define("LOG_MODEL_CDN", 'cdn_log');
#cdn->bk消息发送模块
define("LOG_MODEL_CDN_BK_SEND", 'cdn_bk_send_log');
#cdn->bk消息响应模块
define("LOG_MODEL_CDN_BK_NOTIFY", 'cdn_bk_notify_log');
#bk->cdn消息发送模块
define("LOG_MODEL_BK_CDN_SEND", 'bk_cdn_send_log');
#bk->cdn消息反馈模块
define("LOG_MODEL_BK_CDN_NOTIFY", 'bk_cdn_notify_log');
#bk->cdn消息发布反馈模块
define("LOG_MODEL_BK_CDN_DELIVERY_NOTIFY", 'bk_cdn_delivery_notify_log');
#bk->cdn消息上下线反馈模块
define("LOG_MODEL_BK_CDN_LINE_NOTIFY", 'bk_cdn_line_notify_log');
#cms模块
define("LOG_MODEL_CMS", 'cms_log');
#媒资注入播控模块
define("LOG_MODEL_ASSET_IMPORT", 'asset_import_log');
#消息队列模块
define("LOG_MODEL_MESSAGE", 'message_log');
#运营商下发模块
define("LOG_MODEL_SP", 'sp_import_log');
#内容分发模块
define("LOG_MODEL_DIST", 'distribute_log');
#注入队列模块
define("LOG_MODEL_OP", 'op_log');
#中心注入指令模块
define("LOG_MODEL_QUEUE", 'op_queue_log');
#切片模块
define("LOG_MODEL_CLIP", 'clip_log');
#上下线模块
define("LOG_MODEL_UNLINE", 'unline_log');
#消息反馈模块
define("LOG_MODEL_NOTIFY", 'callback_log');
#透传队列反馈模块
define("LOG_PASS_QUEUE_MODEL", 'pass_queue');