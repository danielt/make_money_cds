<?php
/**
 * 播控链接队列池
 * @var unknown
 */
$g_arr_queue_pool = array(
    array(
        'name' => 'thrid_playbill',
        'desc' => '第三方获取节目单',
        'ext_info' => array(
            array(
                'name' => 'max_day',
                'desc' => '获取多少天后的节目单信息(当天开始)',
                'rule' => 'int',
                'default_val' => '3',
            ),
            array(
                'name' => 'channel_url',
                'desc' => '获取第三方频道信息url地址',
                'rule' => 'noempty',
                'default_val' => '',
            ),
            array(
                'name' => 'playbill_url',
                'desc' => '获取第三方节目单信息url地址',
                'rule' => 'noempty',
                'default_val' => '',
            ),
            array(
                'name' => 'deep_playbill_url',
                'desc' => '获取第三方节目单深度信息url地址',
                'rule' => 'noempty',
                'default_val' => '',
            )
        )
    ),
    array(
        'name' => 'del_thrid_playbill',
        'desc' => '删除节目单',
        'ext_info' => array(
            array(
                'name' => 'max_day',
                'desc' => '节目单默认删除基准天+多少天的数据',
                'rule' => 'int',
                'default_val' => '3',
            ),
            array(
                'name' => 'base_day',
                'desc' => '节目单默认删除录制基准为多少天的数据',
                'rule' => 'int',
                'default_val' => '7',
            ),
         )
    ),
    array(
        'name' => 'del_playbill_task',
        'desc' => '删除节目单队列及日志',
        'ext_info' => array(
            array(
                'name' => 'base_day',
                'desc' => '节目单删除默认基准为多少天队列及日志',
                'rule' => 'int',
                'default_val' => '20',
            ),
        )
    ),
    array(
        'name' => 'thrid_full_assests',
        'desc' => '第三方媒资全量数据注入',
        'ext_info' => array(
            array(
                'name' => 'data_num',
                'desc' => '获取数据条数',
                'rule' => 'int',
                'default_val' => '30',
            ),
            array(
                'name' => 'execute_num',
                'desc' => '执行的最大次数',
                'rule' => 'int',
                'default_val' => '5',
            ),
            array(
                'name' => 'asset_site_num',
                'desc' => '获取最多多少个厂家的URL',
                'rule' => 'int',
                'default_val' => '3',
            ),
            array(
                'name' => 'asset_info_url',
                'desc' => '第三方媒资全量数据基本信息url地址',
                'rule' => 'noempty',
                'default_val' => '',
            ),
            array(
                'name' => 'asset_play_info_url',
                'desc' => '第三方媒资全量数据播放信息url地址',
                'rule' => 'noempty',
                'default_val' => '',
            ),
        )
    ),
    array(
        'name' => 'message',
        'desc' => '消息分发队列',
        'ext_info' => null,
    ),
    array(
        'name' => 'import',
        'desc' => '中心注入指令队列',
        'ext_info' => null,
    ),
    array(
        'name' => 'clip_file',
        'desc' => '切片队列',
        'ext_info' => null,
    ),
    array(
        'name' => 'file_encode',
        'desc' => '转码队列',
        'ext_info' => null,
    ),
    array(
        'name' => 'clip_to_encode',
        'desc' => '切片队列到转码队列',
        'ext_info' => null,
    ),
    array(
        'name' => 'queue_to_encode',
        'desc' => '中心同步队列到转码队列',
        'ext_info' => null,
    ),
    array(
        'name' => 'cdn_video',
        'desc' => '注入CDN点播队列',
        'ext_info' => null,
    ),
    array(
        'name' => 'cdn_index',
        'desc' => '注入CDN点播分集队列',
        'ext_info' => null,
    ),
    array(
        'name' => 'cdn_media',
        'desc' => '注入CDN点播片源队列',
        'ext_info' => null,
    ),
    array(
        'name' => 'cdn_live',
        'desc' => '注入CDN直播频道队列',
        'ext_info' => null,
    ),
    array(
        'name' => 'cdn_live_media',
        'desc' => '注入CDN直播源源队列',
        'ext_info' => null,
    ),
    array(
        'name' => 'cdn_playbill',
        'desc' => '注入CDN节目单队列',
        'ext_info' => null,
    ),
    array(
        'name' => 'cdn_file',
        'desc' => '注入CDN文件包队列',
        'ext_info' => null,
    ),
    array(
        'name' => 'epg_video',
        'desc' => '注入EPG点播队列',
        'ext_info' => null,
    ),
    array(
        'name' => 'epg_index',
        'desc' => '注入EPG点播分集队列',
        'ext_info' => null,
    ),
    array(
        'name' => 'epg_media',
        'desc' => '注入EPG点播片源队列',
        'ext_info' => null,
    ),
    array(
        'name' => 'epg_live',
        'desc' => '注入EPG直播频道队列',
        'ext_info' => null,
    ),
    array(
        'name' => 'epg_live_media',
        'desc' => '注入EPG直播源源队列',
        'ext_info' => null,
    ),
    array(
        'name' => 'epg_playbill',
        'desc' => '注入EPG节目单队列',
        'ext_info' => null,
    ),
    array(
        'name' => 'thrid_full_assests_v2',
        'desc' => '酷控媒资数据注入到第三方数据表',
        'ext_info' => array(
            array(
                'name' => 'asset_site_num',
                'desc' => '切割成多少个xml文件',
                'rule' => 'int',
                'default_val' => '100',
            ),
            array(
                'name' => 'asset_info_url',
                'desc' => '酷控媒资全量数据基本信息url地址',
                'rule' => 'noempty',
                'default_val' => '',
            ),
        )
    ),

    array(
        'name' => 'thrid_full_assests_v3',
        'desc' => '酷控媒资数据注入到第三方数据表V3',
        'ext_info' => array(
            array(
                'name' => 'cp_id',
                'desc' => 'CP ID',
                'rule' => 'noempty',
                'default_val' => 'TV_sou',
            ),
            array(
                'name' => 'asset_site_num',
                'desc' => '获取最多多少个厂家的URL',
                'rule' => 'int',
                'default_val' => '3',
            ),
        )
    ),
    array(
        'name' => 'thrid_full_assests_to_vod',
        'desc' => '酷控媒资数据注入到主媒资表',
        'ext_info' => array(
            array(
                'name' => 'cp_id',
                'desc' => 'CP ID',
                'rule' => 'noempty',
                'default_val' => 'TV_sou',
            ),
            array(
                'name' => 'data_num',
                'desc' => '获取数据条数',
                'rule' => 'int',
                'default_val' => '30',
            ),
            array(
                'name' => 'execute_num',
                'desc' => '执行的最大次数',
                'rule' => 'int',
                'default_val' => '5',
            ),
        )
    ),
    array(
        'name' => 'ftp_media_to_db',
        'desc' => 'ftp扫描片源文件入媒资库',
        'ext_info' => array(
            array(
                'name' => 'ftp_url',
                'desc' => 'FTP地址',
                'rule' => 'noempty',
                'default_val' => '',
            ),
            array(
                'name' => 'media_tag',
                'desc' => '影片终端呈现标示',
                'rule' => '',
                'default_val' => '26,',
            ),
            array(
                'name' => 'media_mode',
                'desc' => '影片清晰度(0 标清|1 高清|2 超高清|3 4K)',
                'rule' => '',
                'default_val' => '1',
            ),
            array(
                'name' => 'cp_id',
                'desc' => 'CP ID',
                'rule' => 'noempty',
                'default_val' => 'xiaole',
            ),
            array(
                'name' => 'bitrate',
                'desc' => '扫描的原始文件码率',
                'rule' => 'int',
                'default_val' => '0',
            ),
            array(
                'name' => 'resolution',
                'desc' => '扫描的原始文件分辨率',
                'rule' => 'noempty',
                'default_val' => '1024*768',
            ),
        )
    ),
    array(
        'name' => 'ftp_media_to_db_nmxl',
        'desc' => 'ftp扫描片源文件入媒资库--内蒙孝乐',
        'ext_info' => array(
            array(
                'name' => 'ftp_url',
                'desc' => 'FTP地址',
                'rule' => 'noempty',
                'default_val' => '',
            ),
            array(
                'name' => 'media_tag',
                'desc' => '影片终端呈现标示',
                'rule' => '',
                'default_val' => '26,',
            ),
            array(
                'name' => 'media_mode',
                'desc' => '影片清晰度(0 标清|1 高清|2 超高清|3 4K)',
                'rule' => '',
                'default_val' => '1',
            ),
            array(
                'name' => 'cp_id',
                'desc' => 'CP ID',
                'rule' => 'noempty',
                'default_val' => 'xlzt',
            ),
            array(
                'name' => 'bitrate',
                'desc' => '扫描的原始文件码率',
                'rule' => 'int',
                'default_val' => '0',
            ),
            array(
                'name' => 'resolution',
                'desc' => '扫描的原始文件分辨率',
                'rule' => 'noempty',
                'default_val' => '1024*768',
            ),
        )
    ),
    array(
        'name' => 'playbill_to_localfile',
        'desc' => '节目单文件生成（江苏华博大电视使用）',
        'ext_info' => array(
            array(
                'name' => 'max_day',
                'desc' => '获取节目单最大天数',
                'rule' => 'int',
                'default_val' => '7',
            ),
        )
    ),
    array(
        'name' => 'sync_third_party',
        'desc' => '新疆电信注入CDN后内容要同步给福富也要注入我们的CDN（要注入两个CMS平台）',
        'ext_info' => null,
    ),
    array(
        'name' => 'delivery_import_message',
        'desc' => '预发布获取媒资信息(FTP、MQ、REDIS扫描注入的定时器)',
        'ext_info' => null,
    ),
);