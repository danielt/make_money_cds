<?php
ini_set("soap.wsdl_cache_enabled", "0");
include dirname(dirname(__FILE__)).'/utils.func.php';
$arr_dir = explode('|', str_replace(array ('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
class OTT_SOAP
{
	public function ContentDeployReq($CMSID,$SOPID,$CorrelateID,$ContentMngXMLURL)
	{
		$arg_list = func_get_args();
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '服务端执行参数:' . var_export($arg_list, true).';CMSID:'.$CMSID.';SOPID:'.$SOPID, ORG_ID);
		return array('ResultCode'=>0,'ErrorDescription'=>'ok');
	}
}
$server = new SOAPServer('ContentDeployReq.wsdl');
$server->setClass('OTT_SOAP');
$server->handle();
