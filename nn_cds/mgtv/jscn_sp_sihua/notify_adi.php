<?php
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('display_errors', 0);
set_time_limit(0);
$arr_dir = explode('|', str_replace(array ('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/sp/sp.class.php';
$ip = i_get_ip();
$postdata = file_get_contents("php://input");

// $postdata = '<?xml version = "1.0" encoding="utf-8"? >
// <SyncContentsResult  Time_Stamp="2012-09-11 20:00:00" System_ID="ICMSxxxxxx">  
// 	<Assets>
// 		<Asset ID="资产ID"  Current_ID="15035715362883497889" Type="Movie"  Status="0" Desc="ok">
// </Asset>
// 	</Assets>
// </SyncContentsResult>';

$file_init_dir = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/' . ORG_ID . '/init.php';
$file_init_define = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/' . ORG_ID . '/define.php';
if(!file_exists($file_init_define) || !file_exists($file_init_dir))
{
	nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '初始化文件不存在:[' . $file_init_dir . ']['.$file_init_define.']', 'public');
	echo "1|inner error";
	exit();
}
include_once $file_init_dir;
include_once $file_init_define;
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------CDN到BK消息反馈开始--------------', ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '执行文件:' . __FILE__, ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '收到来自CDN的消息为空,IP:' . $ip . ';收到来自CDN的内容:' . $postdata, 'public');
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '收到来自CDN的消息为空,IP:' . $ip . ';收到来自CDN的POST内容:' . var_export($_REQUEST,true), 'public');
class notify_adi
{
	public function ContentDeployResult($CMSID, $SOPID, $CorrelateID, $ResultCode,$ErrorDescription ,$ResultContent)
	{
		$arg_list = func_get_args();
		$c2_notify = array (
			'nns_id' => $CorrelateID, 
			'nns_notify_content' => $ResultContent, 
			'nns_notify_result' => $ResultCode,
	        'nns_error_description'=> $ErrorDescription,
	        'nns_notify_result_url'=>'',
		);
		$result = c2_task_model::save_c2_notify_v2($c2_notify);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '方法参数：' . var_export($arg_list, true), ORG_ID);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '修改C2表参数：' . var_export($c2_notify, true), ORG_ID);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '结果：' . var_export($result, true), ORG_ID);
		return $result;
	}
}
// $dom = new DOMDocument('1.0', 'utf-8');
// $dom->loadXML($postdata);
// $asset = $dom->getElementsByTagName('Asset');
// if($asset->length <1)
// {
//     echo "1|no data params";
//     exit();
// }
// $arr_asset = null;
// foreach ($asset as $asset_val)
// {
//     $Current_ID = $asset_val->getAttribute('Current_ID');
//     $Status = $asset_val->getAttribute('Status');
//     $Desc = $asset_val->getAttribute('Desc');
//     if(strlen($Current_ID) <1)
//     {
//         continue;
//     }
//     $arr_asset[] = array(
//         'nns_id'=>$Current_ID,
//         'nns_notify_result'=>($Status == '0') ? 0 : '-1',
//         'nns_notify_content'=>$postdata,
//         'nns_error_description'=>$Desc,
//     );
// }
// if(!is_array($arr_asset) || empty($arr_asset))
// {
//     echo "1|no data params";
//     exit();
// }
// foreach ($arr_asset as $asset)
// {
//     $obj_notify = new notify();
//     $result = $obj_notify->ContentDeployResult('starcor', ORG_ID, $asset['nns_id'], $asset['nns_notify_result'], $asset['nns_error_description'], $asset['nns_notify_content']);
//     nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '处理结果:'.var_export($result,true), ORG_ID);
//     unset($obj_notify);
//     $obj_notify = null;
// }
echo "0|ok";
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------CDN到BK消息反馈结束--------------', ORG_ID);