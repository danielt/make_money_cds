<?php
include_once dirname(dirname(__FILE__)) . '/nn_logic/nl_common.func.php';
class nn_db {
    static $db = null;
    public static function get_db($db_policy = NL_DB_READ) {	
        if (!isset(self::$db[$db_policy]) || self::$db[$db_policy] == null) {
			$db = nl_get_db($db_policy);
			$db->open(); 
            self::$db[$db_policy] = $db;
        }
        return self::$db[$db_policy];
    }
}
function nn_get_db($db_policy = NL_DB_READ) {
        return nn_db::get_db($db_policy);
}

?>