<?php
ini_set('display_errors',1);
ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv_v2/mgtv_init.php';

//include_once dirname(__FILE__).'/init.php';
//$ip = i_get_ip();
//i_write_log('notify from '.$ip,'c2');
//$postdata = file_get_contents("php://input");
//i_write_log('notify data '.$postdata,'c2');
class nmgu_notify{
	public function ContentPublishResult($CSPID,$LSPID,$CorrelateID,$CmdResult,$ErrorDescription,$ResultFileURL)
    {
		//$arg_list = func_get_args();

		$c2_notify = array(
		'nns_id'=>$CorrelateID,
		'nns_notify_result_url'=>$ResultFileURL,
		'nns_notify_result'=>$CmdResult,
		);
        //Do something。。。。。。。。
		c2_task_model::save_c2_notify($c2_notify);

		i_write_log_core(var_export($c2_notify,true),'c2_notify');
		return $this->ContentPublishResultResponse(0,'成功');
	}
    //ContentPublishResultResponse返回
	private function ContentPublishResultResponse($ResultCode,$ErrorDescription){
		$ContentPublishResultResponse = new stdClass();
		$ContentPublishResultResponse ->ResultCode=$ResultCode;
		$ContentPublishResultResponse ->ErrorDescription=$ErrorDescription;
		return $ContentPublishResultResponse;
	}
}
//ob_start();
$server = new SOAPServer('ContentPublishResult.wsdl');
$server->setClass('nmgu_notify');
$server->handle();
//$out = ob_get_contents();
//i_write_log($out);
//ob_end_flush();
//echo $out;
