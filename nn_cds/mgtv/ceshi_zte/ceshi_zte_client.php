<?php
ini_set('display_errors',1);
set_time_limit(0);
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/mgtv_init.php';
define('ORG_ID', 'ceshi_zte');
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/'.ORG_ID.'/init.php';
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/'.ORG_ID.'/define.php';
define('CSPID','KKTVITVCMS');
define('LSPID','ZTEAdapter');
ini_set("soap.wsdl_cache_enabled", "0");
class ceshi_zte_client{
	const WSDL = 'http://127.0.0.1:57880/nn_mgtvbk/mgtv/ceshi_zte/ceshi_zte_service.php?wsdl';
	static public function ExecCmd($CorrelateID,$CmdFileURL){
		$arg_list = func_get_args();
		i_write_log_core('ExecCmd request args:'.var_export(array(CSPID,LSPID,$CorrelateID,$CmdFileURL),true),ORG_ID.'/c2');
		$client = self::get_soap_client();
		$ret = $client->ExecCmd(CSPID,LSPID,$CorrelateID,$CmdFileURL);
		i_write_log_core('ExecCmd return:'.var_export($ret,true),ORG_ID.'/c2');
		i_write_log_core('ExecCmd return:'.json_encode($ret),ORG_ID.'/c2json');
		//print_r(json_encode($ret));
		return $ret;
	}
	public static function get_soap_client(){
		return new SOAPClient(self::WSDL,array('trace'=>true));
	}
}
