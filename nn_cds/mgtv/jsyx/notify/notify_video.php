<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors',1);
set_time_limit(0);
include_once  dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv_v2/mgtv_init.php';
define('ORG_ID', 'jsyx');
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/'.ORG_ID.'/init.php';
include_once  dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv_v2/'.ORG_ID.'/define.php';

include_once  dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv_v2/models/queue_task_model.php';


$db = nl_get_db(NL_DB_WRITE);
$db->open();
$postdata = file_get_contents("php://input");
i_write_log_core('notify data '.$postdata,'notify');


$xml = $postdata;
$dom = new DOMDocument('1.0', 'utf-8');
$dom -> loadXML($xml);

$xpath = new DOMXPath($dom);




$entries = $xpath -> query('/PLAYLIST/URL');
$asset_list_all = array();
foreach ($entries as $item) {	
	$asset_lists = array(
	'videoID' => $item -> getAttribute('videoID'), 
	'STATUS' => $item -> getAttribute('STATUS'), 
	'url' => trim($item->nodeValue), 
	);
	$asset_list_all[]=$asset_lists;
}


$dt = date('Y-m-d H:i:s',time());
foreach ($asset_list_all as $asset_list) {
	$result = -1;
	if($asset_list['STATUS']=='Ready'){ 
		$sql="update nns_mgtvbk_c2_task set nns_status=0,nns_ex_url='{$asset_list['url']}' , nns_modify_time='".$dt."'  where nns_org_id='".ORG_ID."' and nns_type='media' and  nns_ref_id='{$asset_list['videoID']}'";	
		$result = 0;
	}
	elseif($asset_list['STATUS']=='Failed')
	{
		$sql="update nns_mgtvbk_c2_task set nns_status=-1,nns_ex_url='{$asset_list['url']}' , nns_modify_time='".$dt."'  where nns_org_id='".ORG_ID."' and nns_type='media' and  nns_ref_id='{$asset_list['videoID']}'";
	}
	$bool = nl_execute_by_db($sql, $db);
	i_write_log_core('notify data_sql '.$sql,'notify');
	i_write_log_core('notify data_bool '.var_export($bool,true),'notify');
	
	
	$sql_c2_task = "select nns_id  from nns_mgtvbk_c2_task where nns_org_id='".ORG_ID."' and nns_type='media' and  nns_ref_id='{$asset_list['videoID']}'";
	$c2_task_id = nl_db_get_col($sql_c2_task, $db);
	
	
	$sql = "select  nns_id from nns_mgtvbk_c2_log where nns_org_id='".ORG_ID."' and  nns_task_id='".$c2_task_id."' and nns_task_type='Movie'  and nns_notify_result is null order by nns_create_time desc limit 1 ";
	$c2_task_log_id = nl_db_get_col($sql, $db);
	
	$array = array('nns_id'=>$c2_task_log_id,'nns_notify_result'=>$result,'nns_notify_result_url'=>false);
	c2_task_model::save_c2_notify($array);
}
