<?php
ini_set('display_errors',1);
ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/mgtv_init.php';
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/core/define.php';
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/core/init.php';
$ip = i_get_ip();
i_write_log_core('notify from '.$ip,'c2');
$postdata = file_get_contents("php://input"); 
i_write_log_core('notify data '.$postdata,'c2');
class core_notify{
	public function ResultNotify($CSPID,$LSPID,$CorrelateID,$CmdResult,$ResultFileURL){
		$arg_list = func_get_args();
		
		$c2_notify = array(
		'nns_id'=>$CorrelateID,
		'nns_notify_result_url'=>$ResultFileURL,
		'nns_notify_result'=>$CmdResult,
		);
		c2_task_model::save_c2_notify($c2_notify);
		i_write_log_core('ResultNotify params:'.var_export($arg_list,true),'c2');
		return array('Result'=>0,'ErrorDescription'=>'notify ok');
	}
}
//ob_start();
$server = new SOAPServer('fjyd_notify.wsdl');
$server->setClass('core_notify');
$server->handle();
//$out = ob_get_contents();
//i_write_log($out);
//ob_end_flush();
//echo $out;
