<?php
ini_set('display_errors',1);
ini_set("soap.wsdl_cache_enabled", "1");
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/mgtv_init.php';
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/core/define.php';
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/utils.func.php';
//die;
define('CSPID','hntv');
define('LSPID','hntv');
class core_client{
	//http://218.207.213.7:35100/cms/services/ctms?wsdl
	//http://127.0.0.1/work/nn_inject/mgtv/fjyd/fjyd_service.php?wsdl
	const WSDL = 'http://192.168.90.120/mgtvbk/mgtv/core/core_service.php?wsdl';

	static public function ExecCmd($CorrelateID,$CmdFileURL){
		$arg_list = func_get_args();
		//$CorrelateID = fjyd_guid_rand();
		i_write_log_core('ExecCmd request args:'.var_export($arg_list,true),'fjyd/c2');
		$client = self::get_soap_client();
		$ret = $client->ExecCmd(CSPID,LSPID,$CorrelateID,$CmdFileURL);
		i_write_log_core('ExecCmd return:'.var_export($ret,true),'fjyd/c2');
		//i_write_log('LastRequest:'.$client->__getLastRequest());
		//i_write_log('LastResponse:'.$client->__getLastResponse());
		return $ret;
	}
	
	
	
	public static function get_soap_client(){
		return new SOAPClient(self::WSDL,array('trace'=>true));
	}
}


/*
$client = new fjyd_client();

$CmdFileURL='ftp://dfdfd';
$client->ExecCmd($CmdFileURL);

echo 'game over!';
*/
