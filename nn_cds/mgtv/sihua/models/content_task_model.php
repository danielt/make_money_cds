<?php
ini_set('display_errors',1);

class content_task_model{
	//
	static public function run($type) {		
		return self::$type();
	}
	/**
	 * 注入分集
	 */
	static public function run_vod_index_to_c2(){
		self::vod(null,'index');
	}
	
	static public function run_vod_media_to_c2(){
		self::vod(null,'media');
	}
	/**
	 * 从内容任务表取任务执行
	 */
	static public function vod($new_id_c2=null,$c2_type=null,$return_xml=false){
		//die(11111111);
		DEBUG && i_write_log('--------开始注入vod---------');
		$db = nn_get_db(NL_DB_READ);
        $db_w = nn_get_db(NL_DB_WRITE);
		if($new_id_c2){
			$sql  = "select * from nns_mgtvbk_c2_task where nns_id='$new_id_c2'";
		}else{
			if($c2_type=='index'){//注入分集的条件是:影片信息已经注入成功
				$sql  = "select t1.* from nns_mgtvbk_c2_task t1,nns_mgtvbk_c2_task t2 where t1.nns_status in(1,2,3) and t1.nns_type='index' and t1.nns_src_id=t2.nns_ref_id and t2.nns_type='vod' and t2.nns_status=0 and t1.nns_org_id='sihua' and t2.nns_org_id='sihua'  order by t1.nns_create_time desc limit 30";
			}elseif($c2_type=='media'){
				$sql  = "select t1.* from nns_mgtvbk_c2_task t1 where t1.nns_status in(1,2,3) and t1.nns_type='media' and t1.nns_org_id='sihua' and nns_clip_task_id is not null  order by t1.nns_create_time desc limit 1";				
			}else{
				$sql  = "select * from nns_mgtvbk_c2_task where nns_status in(1,2,3) and nns_type!='index' and nns_type!='media' and nns_org_id='sihua'  order by nns_create_time desc limit 30";
			}
			
		}			
		
DEBUG && i_write_log('sql-->'.$sql);
		$result = nl_db_get_all($sql,$db);unset($sql);
		if(empty($result)) {
			DEBUG && i_write_log('--------没有C2任务---------');return;
		}
		foreach($result as $val){
			
			//判断是否按照常规注入片源
			//if(IMPROT_MEDIA===false&&$val['nns_type']=='media'){
			//	return ;
			//}
			//查询分集数量，大于1按照连续剧处理等于1按照电影处理				
			if($val['nns_type']=='vod'){
				$sql = "select nns_all_index from nns_vod where nns_id='{$val['nns_ref_id']}'";
				$all_index = nl_db_get_col($sql,$db);unset($sql);
			}elseif($val['nns_type']=='index'){
				$sql = "select nns_all_index from nns_vod where nns_id='{$val['nns_src_id']}'";
				$all_index = nl_db_get_col($sql,$db);unset($sql);
			}
			//推送vod基本信息(添加或则修改)
			if($val['nns_action']=='destroy'){
				//删除分片或则影片
				if($val['nns_type']=='vod'){
					DEBUG && i_write_log('--------删除vod操作---------');
					//删除分集
					$result_index =  nl_db_get_all("select * from nns_mgtvbk_c2_task where nns_type='index' and nns_src_id='{$val['nns_ref_id']}'",$db);
					foreach($result_index as $val_index){
						$nns_status = nl_db_get_col("select nns_status from nns_mgtvbk_c2_task where nns_id='".$val_index['nns_id']."'",$db);
					    if(intval($nns_status) > 0){//可能已经删除，为了不产生重复删除C2指令
						    $index_name = nl_db_get_col("select nns_name from nns_vod_index where nns_id='".$val_index['nns_ref_id']."'",$db);
						    //c2_task_model::delete_vod(array('nns_id'=>$val_index['nns_ref_id'],'nns_task_id'=>$val['nns_id'],'nns_task_name'=>$index_name));//删除影片
						    nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='".$val_index['nns_id']."'", $db_w);
						}
					    unset($nns_status);
					}
					$vod_name = nl_db_get_col("select nns_name from nns_vod where nns_id='".$val['nns_ref_id']."'",$db);
					if($all_index>1){											
						 c2_task_model::delete_series(array('nns_id'=>$val['nns_ref_id'],'nns_task_id'=>$val['nns_id'],'nns_task_name'=>$vod_name));//删除影片
					}else{
						 //c2_task_model::delete_vod(array('nns_id'=>$val['nns_ref_id'],'nns_task_id'=>$val['nns_id'],'nns_task_name'=>$vod_name));//删除影片
					}
					
					nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='".$val['nns_id']."'", $db_w);
				}elseif($val['nns_type']=='index'){
					DEBUG && i_write_log('--------删除index操作---------');
					$nns_status = nl_db_get_col("select nns_status from nns_mgtvbk_c2_task where nns_id='".$val['nns_id']."'",$db);
					if(intval($nns_status) > 0){//可能已经删除，为了不产生重复删除C2指令
						$index_name = nl_db_get_col("select nns_name from nns_vod_index where nns_id='".$val['nns_ref_id']."'",$db);
						c2_task_model::delete_vod(array('nns_id'=>$val['nns_ref_id'],'nns_task_id'=>$val['nns_id'],'nns_task_name'=>$index_name));//删除影片					
						nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='".$val['nns_id']."'", $db_w);						
					}
                    unset($nns_status);
				}elseif($val['nns_type']=='live'){//直播炒作
					i_write_log('--------删除live操作---------');
					$result_index =  nl_db_get_all("select * from nns_mgtvbk_c2_task where nne_type='playbill' and nns_src_id='{$val['nns_ref_id']}'",$db);
					foreach($result_index as $val_index){
						$index_name = nl_db_get_col("select nns_name from nns_live_playbill_item where nns_id='".$val_index['nns_ref_id']."'",$db);
						$r = c2_task_model::delete_playbill(array('nns_id'=>$val_index['nns_ref_id'],'nns_task_id'=>$val['nns_id'],'nns_task_name'=>$index_name));//删除影片
						if($r)
						nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='".$val_index['nns_id']."'", $db_w);
					}
					$vod_name = nl_db_get_col("select nns_name from nns_live where nns_id='".$val['nns_ref_id']."'",$db);
					$r = c2_task_model::delete_live(array('nns_id'=>$val['nns_ref_id'],'nns_task_id'=>$val['nns_id'],'nns_task_name'=>$vod_name));//删除影片
					if($r)
					nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='".$val['nns_id']."'", $db_w);
				}elseif($val['nns_type']=='playbill'){
					i_write_log('--------删除playbill操作---------');
					$index_name = nl_db_get_col("select nns_name from nns_live_playbill_item where nns_id='".$val['nns_ref_id']."'",$db);
					$r = c2_task_model::delete_playbill(array('nns_id'=>$val['nns_ref_id'],'nns_task_id'=>$val['nns_id'],'nns_task_name'=>$index_name));//删除
					if($r)
					nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='".$val['nns_id']."'", $db_w);					
				}elseif($val['nns_type']=='media'){
					i_write_log('------------删除media操作-----------------');
					$val['nns_action'] = 'destroy';
					self::__push_vod_media($val);
					//$r = c2_task_model::delete_movies(array('nns_ids'=>array($val['nns_ref_id']),'nns_task_id'=>$val['nns_id'],'nns_task_name'=>$val['nns_name']));//删除
					//if($r)
					//nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='".$val['nns_id']."'", $db_w);
				}
			}else{
				DEBUG && i_write_log('-----------'.$val['nns_action'].'操作---------'.$val['nns_type']."\n".var_export($val,true));
					if($val['nns_type']=='vod'||$val['nns_type']=='index'){//影片
						$re_xml = self::__push_vod_info($db, $val['nns_ref_id'],$val['nns_action'],$val['nns_type'],$val['nns_src_id'],$val['nns_id'],$val['nns_org_id'],$return_xml);
					if($return_xml===true){
							return $re_xml;
						}
					}elseif($val['nns_type']=='media'){
						
						self::__push_vod_media($val);
					}else{
						//self::__push_live_info($val);//直播
					}
				}
			//return $result_vod;
		}
		return true;
	}
	
	static public function __push_vod_media($val){
		
		$db = nn_get_db(NL_DB_READ);
		$db_w = nn_get_db(NL_DB_WRITE);
		$sp_id = $val['nns_org_id'];
		$sp_info = sp_model::get_sp_info($sp_id);
		$vod_id_arr = nl_db_get_one("select nns_vod_id,nns_id from nns_vod_index where nns_id='{$val['nns_src_id']}'", $db);
		
		$clip = false;
		//$clip = strstr(trim($sp_info['nns_rule']),'100000');
		if($sp_info['nns_rule']=='100000'){
			$clip = true;
		}
		
		$vod_id = $vod_id_arr['nns_vod_id'];
		$vod_index_id = $vod_id_arr['nns_id'];
		
		$sp_sql = "select nns_org_id from nns_mgtvbk_c2_task where nns_id='{$val['nns_id']}'";
		$sp_id = nl_db_get_col($sp_sql, $db);
		
		//查询苯教记录中有没有注入分片的
		$sql_vod_index_sql="select * from nns_mgtvbk_c2_task where nns_org_id='$sp_id' and nns_type='index' and nns_ref_id='$vod_index_id' and nns_src_id='$vod_id'";
		$c2_vod_index = nl_db_get_one($sql_vod_index_sql, $db);		
		//如没有记录该文件
		if(empty($c2_vod_index)){			
			$depot_info = category_model::__get_category_all($vod_index_id, 'index');
			$category = category_model::get_category_id_by_content_all($depot_info['depot_info'], 0,$sp_id);
			if (is_array($category) && count($category) > 0) {
				$re = category_content_model::_task_c2_all($category, $vod_index_id, 'index');
				//if($re===true){
				//	return ;
				//}
			} else {
				i_write_log_core('分集'.$vod_index_id, 'sync/index/'.$sp_id);
				//return ;
			}
		}		
		//查询苯教记录中有没有注入的
		$sql_vod_sql="select * from nns_mgtvbk_c2_task where nns_org_id='$sp_id' and nns_type='vod' and nns_ref_id='$vod_id' ";
		$c2_vod_tmp = nl_db_get_one($sql_vod_sql, $db);
		if(empty($c2_vod_tmp)){
			$depot_info = category_model::__get_category_all($vod_id, 'vod');
			$category = category_model::get_category_id_by_content_all($depot_info['depot_info'], 0,$sp_id);
			if (is_array($category) && count($category) > 0) {
				$re = category_content_model::_task_c2_all($category, $vod_id, 'vod');
				//if($re===true){
				//	return ;
				//}
			} else {
				i_write_log_core('媒体资源'.$vod_id, 'sync/index/'.$sp_id);
				//return ;
			}
		}
		
		$action = "REGIST";
		if($val['nns_action']=='add'){
			$action = "REGIST";
		}elseif($val['nns_action']=='modify'){
			$action = "UPDATE";
		}elseif($val['nns_action']=='destroy'){
			$action = "DELETE";
		}
		
		
		
		
		$data = array(
			'nns_video_id'=>$vod_id,
			'nns_video_index_id'=>$val['nns_src_id'],
			'nns_task_id'=>$val['nns_clip_task_id'],
			'nns_clip_date'=>$val['nns_clip_date'],
			'nns_media_id'=>$val['nns_ref_id'],
			'nns_new_dir'=>true,
			'nns_date'=>$val['nns_clip_date'],
			'clip'=>$clip,
			'nns_id'=>$val['nns_id'],
			'action'=>$action,
		);
		
		
		
		$result = c2_task_model::add_movies_by_vod_index($data);
		
		if($result){
			nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='{$val['nns_id']}'", $db_w);
		}
		
		return true;
	}
	
		
	static private function __push_vod_info($db, $nns_id,$action,$type,$src_id,$c2_task_id,$org_id,$return_xml){
		if($type=='vod'){	
			$sql = "select * from nns_vod where nns_id='$nns_id'";
		}elseif($type=='index'){
			$sql = "select * from nns_vod where nns_id='$src_id'";
		}
		$result = nl_db_get_one($sql,$db);unset($sql);
		if($result===false) return ;
		for ($i=0; $i <=31 ; $i++) { 
			unset($result["nns_tag$i"]);
		}
		return self::__push_vod($db,$result,$nns_id,$action,$c2_task_id,$org_id,$type,$src_id,$return_xml);
	}
	
	
		//
	static private function __push_live_info($params){
		$db = nn_get_db(NL_DB_READ);
		$db_w = nn_get_db(NL_DB_WRITE);
		if($params['nns_type']=='live'){
			$nns_id_tmp = $params['nns_ref_id'];
		}elseif($params['nns_type']=='playbill'){
			$nns_id_tmp = $params['nns_src_id'];
		}
		$sql="select * from nns_live where nns_id='$nns_id_tmp'";
		$result = nl_db_get_one($sql,$db);unset($sql);
		for ($i=0; $i <=31 ; $i++) { 
			unset($result["nns_tag$i"]);
		}
		$org_id = $params['nns_org_id'];
		$action = $params['nns_action'];
		$sp_category = array();	
		
		$sql = "select nns_sp_category_id from nns_mgtvbk_category_content content left join nns_mgtvbk_category category on (content.nns_org_id=category.nns_org_id and content.nns_category_id=category.nns_id) where content.nns_org_id='$org_id' and content.nns_video_id='$vod_id'  and content.nns_video_type=0 group by category.nns_id";
		$sp_category1 = array();
		$result_category = nl_db_get_all($sql,$db);unset($sql);
		foreach ($result_category as  $value) {
			$sp_category1[] = $value['nns_sp_category_id'];
		}
		$sp_category2=array();
		$sql = "select nns_sp_category_id from nns_mgtvbk_category where nns_org_id='$org_id' and bind_vod_item='{$result['nns_category_id']}'";
		$result_category = nl_db_get_all($sql,$db);unset($sql);
		foreach ($result_category as  $value) {
			$sp_category2[] = $value['nns_sp_category_id'];
		}			
		$sp_category = array_merge($sp_category1,$sp_category2);		
		$sp_category = array_unique($sp_category);
		
		
		if($params['nns_type']=='playbill'){
			i_write_log('--------当前注入的是节目单--------');
			$c2_index_sql = "select * from nns_mgtvbk_c2_task where nns_type = 'live' and nns_org_id = '$org_id'  and  nns_ref_id='$nns_id_tmp'";
			$result_index_task = nl_db_get_one($c2_index_sql,$db);unset($c2_index_sql);
			if($result_index_task['nns_status']>0&&$result_index_task['nns_status']!=5){//如果基本信息没有注入则先注入基本信息
				i_write_log('--------如果直播基本信息没有注入则先注入基本信息--------');
				$result['nns_task_id'] = $result_index_task['nns_id'];
				$result['nns_task_name'] = $result['nns_name'];
				$result['nns_category_id'] = $sp_category;
				if($action=='add'){
					$result_vod_rs =  c2_task_model::add_live($result);
				}elseif($action=='modify'){					
					$result_vod_rs =  c2_task_model::update_live($result);
				}					
				if($result_vod_rs){
					nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='{$result_index_task['nns_id']}'", $db_w);
				}
			}else{
				i_write_log('--------基本信息已经注入--------');
			}
			//注入节目单信息
			i_write_log('--------注入节目单信息--------');
			$c2_index_sql = "select * from nns_mgtvbk_c2_task where nns_type = 'playbill' and nns_org_id = '$org_id'  and  nns_ref_id='{$params['nns_ref_id']}'";
			$result_index_task = nl_db_get_one($c2_index_sql,$db);unset($c2_index_sql);
			if($result_index_task['nns_status']>0&&$result_index_task['nns_status']!=5){
				i_write_log('--------如果节目单基本信息没有注入则先注入基本信息--------');
				
				$sql = "select * from nns_live_playbill_item where nns_id='{$params['nns_ref_id']}'";
				$result_live_task = nl_db_get_one($sql,$db);unset($sql);				
				$result['nns_task_id'] = $result_index_task['nns_id'];
				$result['nns_task_name'] = $result_index_task['nns_name'];
				$result['nns_live_id'] = $result_live_task['nns_live_id'];
				$result['nns_begin_time'] = $result_live_task['nns_begin_time'];
				$result['nns_time_len'] = $result_live_task['nns_time_len'];
				$result['nns_live_id'] = $result['nns_id'];
				$result['nns_name'] = $result_live_task['nns_name'];
				$result['nns_id'] = $result_live_task['nns_id'];
				if($action=='add'){
					$result_vod_rs =  c2_task_model::add_playbill($result);
				}elseif($action=='modify'){					
					$result_vod_rs =  c2_task_model::update_playbill($result);
				}				
				if($result_vod_rs){
					nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='{$result_index_task['nns_id']}'", $db_w);
				}				
			}else{
				i_write_log('--------基本信息已经注入--------');
			}			
		}elseif($params['nns_type']=='live'){
			
			i_write_log('--------当前注入的是直播--------');
			$c2_index_sql = "select * from nns_mgtvbk_c2_task where nns_type = 'live' and nns_org_id = '$org_id'  and  nns_ref_id='$nns_id_tmp'";
			
			$result_index_task = nl_db_get_one($c2_index_sql,$db);unset($c2_index_sql);
			if($result_index_task['nns_status']>0&&$result_index_task['nns_status']!=5){//如果基本信息没有注入则先注入基本信息
				i_write_log('--------如果直播基本信息没有注入则先注入基本信息--------');
				$result['nns_task_id'] = $result_index_task['nns_id'];
				$result['nns_task_name'] = $result['nns_name'];
				$result['nns_category_id'] = $sp_category;
				if($action=='add'){
					$result_vod_rs =  c2_task_model::add_live($result);
				}elseif($action=='modify'){					
					$result_vod_rs =  c2_task_model::update_live($result);
				}
				//var_dump($result_vod_rs);
				if($result_vod_rs){
					nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='{$result_index_task['nns_id']}'", $db_w);
				}
			}else{
				i_write_log('--------基本信息已经注入--------');
			}
		}
		
		return true;
		
	}
	static private function __push_vod($db,$result,$nns_id,$action,$c2_task_id,$org_id,$type,$src_id,$return_xml){
			
			
		
		$db_w = nn_get_db(NL_DB_WRITE);
		
		if(empty($result)) {
			nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=4,nns_modify_time=now() where nns_id= '".$c2_task_id."'",$db_w);
			return ;
		}		
		$sp_category = array();	
		//查询改片属于那个栏目		
		if($type=='index'){
			$vod_id = $src_id;
		}elseif($type=='vod'){
			$vod_id = $nns_id;
		}	
		$sql = "select nns_sp_category_id from nns_mgtvbk_category_content content left join nns_mgtvbk_category category on (content.nns_org_id=category.nns_org_id and content.nns_category_id=category.nns_id) where content.nns_org_id='$org_id' and content.nns_video_id='$vod_id'  and content.nns_video_type=0 group by category.nns_id";
		$sp_category1 = array();
		$result_category = nl_db_get_all($sql,$db);unset($sql);
		foreach ($result_category as  $value) {
			$sp_category1[] = $value['nns_sp_category_id'];
		}
		$sp_category2=array();
		$sql = "select nns_sp_category_id from nns_mgtvbk_category where nns_org_id='$org_id' and bind_vod_item='{$result['nns_category_id']}'";
		$result_category = nl_db_get_all($sql,$db);unset($sql);
		foreach ($result_category as  $value) {
			$sp_category2[] = $value['nns_sp_category_id'];
		}			
		$sp_category = array_merge($sp_category1,$sp_category2);
		$sp_category = array_unique($sp_category);
			
		$result_vod = $result;
		$is_movie = true;
		
		
		if($result['nns_all_index']>1&&$type=='index'){//如果是连续剧并且是分集标识的先检查连续剧基本信息有没有注入		
		    DEBUG && i_write_log('--------当前注入的是连续剧分集--------');
			$is_movie = false;
			$c2_index_sql = "select * from nns_mgtvbk_c2_task where nns_type = 'vod' and nns_org_id = '$org_id'  and  nns_ref_id='$src_id'";
			$result_index_task = nl_db_get_one($c2_index_sql,$db);unset($c2_index_sql);
			if($result_index_task['nns_status']>0&&$result_index_task['nns_status']!=5){//如果基本信息没有注入则先注入基本信息
			    DEBUG && i_write_log('--------基本信息没有注入则先注入基本信息---------');
				$result['nns_category_id'] = $sp_category;
				$result['nns_series_flag'] = 1;
				$result['nns_task_id'] = $result_index_task['nns_id'];
				$result['nns_task_name'] = $result['nns_name'];
				
				if($return_xml===true){
					return $result;
				}
				if($action=='add'){
					$result_vod_rs =  c2_task_model::add_series($result);
				}elseif($action=='modify'){					
					$result_vod_rs =  c2_task_model::update_series($result);
				}					
				if($result_vod_rs){
					nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='{$result_index_task['nns_id']}'", $db_w);
				}
			}else{
				 DEBUG && i_write_log('--------基本信息已经注入--------');
				 
			}
		}elseif($result['nns_all_index']==1&&$type=='index'){//如果分集等于1并且是index，则当影片处理
		   // DEBUG && i_write_log('--------当前注入的是影片(分集等于1并且是index，则当影片处理)--------');	
			$is_movie = true;
			//分集信息不再注入???

		}elseif($result['nns_all_index']>1&&$type=='vod'){//如果分集>1并且是vod，则当连续剧
			//var_dump($result);die;
		    DEBUG && i_write_log('--------当前注入的是连续剧--------');	
			$is_movie = false;
			$c2_index_sql = "select * from nns_mgtvbk_c2_task where nns_type = 'vod' and nns_org_id = '$org_id'  and  nns_ref_id='$nns_id'";
			$result_index_task = nl_db_get_one($c2_index_sql,$db);unset($c2_index_sql);
			
			$result['nns_category_id'] = $sp_category;
				$result['nns_series_flag'] = 1;
				$result['nns_task_id'] = $result_index_task['nns_id'];
				$result['nns_task_name'] = $result['nns_name'];
			
			if($return_xml===true){
					return $result;
				}
			if($result_index_task['nns_status']>0&&$result_index_task['nns_status']!=5){//如果基本信息没有注入则先注入基本信息
				//var_dump($result);die;
				$result['nns_category_id'] = $sp_category;
				$result['nns_series_flag'] = 1;
				$result['nns_task_id'] = $result_index_task['nns_id'];
				$result['nns_task_name'] = $result['nns_name'];
				
				
				if($action=='add'){
					$result_vod_rs =  c2_task_model::add_series($result);
				}elseif($action=='modify'){					
					$result_vod_rs =  c2_task_model::update_series($result);
				}
				if($result_vod_rs){
					nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='{$result_index_task['nns_id']}'", $db_w);
				    DEBUG && i_write_log('--------连续剧注入成功---------');
				}else{
					DEBUG && i_write_log('--------连续剧已经注入---------');
				}
			}
		}elseif($result['nns_all_index']==1&&$type=='vod'){//如果是电影并且分集只有1，则是电影
		    DEBUG && i_write_log('--------当前注入的是影片--------');	
			$is_movie = true;
		}	
		
		//初始化基本信息
		$result_vod['nns_category_id'] = $sp_category;
		$nns_alias_name = $result_vod['nns_alias_name'];
		$nns_area = $result_vod['nns_area'];
		$nns_language = $result_vod['nns_language'];
		$nns_show_time = $result_vod['nns_show_time'];
		$nns_kind = $result_vod['nns_kind'];
		$nns_keyword = $result_vod['nns_keyword'];
		//print_r($result_vod);
		//die;
		//插入分集或则影片的信息
		if($type=='vod'){
			if($is_movie){
				//查询分集的id作为影片的新ID
				$sql = "select * from nns_vod_index where  nns_vod_id='$nns_id'";
				$result_num = nl_db_get_one($sql,$db);unset($sql);
				if($result_num != false && is_array($result_num)){
					$result_vod['nns_series_flag'] = 0;
					$result_vod['nns_task_id'] = $c2_task_id;
					$result_vod['nns_id'] = $result_num['nns_id'];
					$index_sql = "select * from nns_mgtvbk_c2_task where nns_type = 'index' and nns_org_id = '$org_id'  and  nns_ref_id='{$result_vod['nns_id']}' and nns_src_id='$nns_id'";
					$index = nl_db_get_one($index_sql,$db);unset($index_sql);
					if($index===false||$index===null){						
						$depot_info = category_model::__get_category_all($result_vod['nns_id'], 'index');						
						$category = category_model::get_category_id_by_content_all($depot_info['depot_info'], 0,$org_id);
						if (is_array($category) && count($category) > 0) {
							$re = category_content_model::_task_c2_all($category, $result_vod['nns_id'], 'index');							
							//if($re===true){
							//	return ;
							//}
						} else {
							i_write_log_core('分集'.$result_vod['nns_id'], 'sync/index/'.$org_id);
							return ;
						}
					}
					$result_vod['nns_task_id'] = $index['nns_id'];
					$result_vod['nns_task_name'] = $result_vod['nns_name'];					
					//查看分集的图片					
					if($index['nns_status']>0){				
					//查询苯教记录中有没有注入的
					$sql_vod_sql="select * from nns_mgtvbk_c2_task where nns_org_id='$org_id' and nns_type='vod' and nns_ref_id='$vod_id' ";
					$c2_vod_tmp = nl_db_get_one($sql_vod_sql, $db);
					if(empty($c2_vod_tmp)){
						$depot_info = category_model::__get_category_all($nns_id, 'vod');
						$category = category_model::get_category_id_by_content_all($depot_info['depot_info'], 0,$org_id);
						if (is_array($category) && count($category) > 0) {
							$re = category_content_model::_task_c2_all($category, $nns_id, 'vod');
							//if($re===true){
							//	return ;
							//}
						} else {
							i_write_log_core('媒体资源'.$nns_id, 'sync/vod/'.$org_id);
							return ;
						}
					}
						
						if($return_xml===true){
							return $result_vod;
						}
											
						if($action=='add'){
							DEBUG && i_write_log('---vod-----添加VOD操作---------');
							$rs =  c2_task_model::add_vod($result_vod);
						}elseif($action=='modify'){	
							DEBUG && i_write_log('----vod----修改VOD操作---------');
							$rs =  c2_task_model::update_vod($result_vod);
						}
						if($rs){
							DEBUG && i_write_log('----vod----C2注入成功操作，更新nns_mgtvbk_c2_task状态为0---------');
							nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='{$c2_task_id}'", $db_w);
							nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='{$index['nns_id']}'", $db_w);
						}else{
							DEBUG && i_write_log('----vod----C2注入成功操作失败--------');
						}
					}
				}else{
					//没有分集
					DEBUG && i_write_log('---查询分集的id作为影片的新ID失败---------');
					//如果此处不做处理，将重复取该任务执行，如果这样的任务太多，将可能导致取不到其他任务
					nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=4,nns_modify_time=now() where nns_id= '".$c2_task_id."'",$db_w);
				}
			}
		}elseif($type=='index'){
			
			
			$sql = "select * from nns_vod_index where  nns_id='$nns_id'";
			$vod = nl_db_get_one($sql,$db);unset($sql);
			$index_sql = "select * from nns_mgtvbk_c2_task where nns_type = 'index' and nns_org_id = '$org_id'  and  nns_ref_id='{$vod['nns_id']}'";
			
			
			$index = nl_db_get_one($index_sql,$db);unset($index_sql);
			
			if($is_movie){//电影
				$result_vod['nns_series_flag'] = 0;
			}else{//连续剧
				$result_vod['nns_series_flag'] = 1;
				$result_vod['nns_vod_id'] = $vod['nns_vod_id'];
				$result_vod['nns_name'] =  $vod['nns_name'];
				if(empty($vod['nns_image'])){
					if(!empty($result_vod['nns_image2'])){
						$result_vod['nns_image'] = $result['nns_image2'];
					}
				}
			}
			$result_vod['nns_index'] = 	$vod['nns_index'];
			$vod_id = $vod['nns_vod_id'];
			if(!empty($vod['nns_name'])) $result_vod['nns_name'] =  $vod['nns_name'];
			$result_vod['nns_task_id'] = $c2_task_id;
			$result_vod['nns_task_name'] = $result_vod['nns_name'];
			$result_vod['nns_id'] = $nns_id;
			$result_vod['nns_image'] = $vod['nns_image'];//分集图片
			$result_vod['nns_index_show_time'] = $vod['nns_create_time'];
			
			//var_dump($result_vod);
			//die;
			
			if($return_xml===true){
					return $result_vod;
				}
			
			//$result_vod['nns_modify_time'] = $vod['nns_modify_time'];
			if($index['nns_status']>0&&$index['nns_status']!=5){
						
					
				//查询苯教记录中有没有注入的
					$sql_vod_sql="select * from nns_mgtvbk_c2_task where nns_org_id='$org_id' and nns_type='vod' and nns_ref_id='{$index['nns_src_id']}' ";
					$c2_vod_tmp = nl_db_get_one($sql_vod_sql, $db);
					if(empty($c2_vod_tmp)){
						$depot_info = category_model::__get_category_all($index['nns_src_id'], 'vod');
						$category = category_model::get_category_id_by_content_all($depot_info['depot_info'], 0,$org_id);
						if (is_array($category) && count($category) > 0) {
							$re = category_content_model::_task_c2_all($category, $index['nns_src_id'], 'vod');
							//if($re===true){
							//	return ;
							//}
						} else {
							i_write_log_core('媒体资源'.$index['nns_src_id'], 'sync/vod/'.$org_id);
							return ;
						}
					}	
								
				
							
						
					
				
				if($action=='add'){
					DEBUG && i_write_log('----index----添加index操作---------');
					$rs =  c2_task_model::add_vod($result_vod);
				}elseif($action=='modify'){			
					DEBUG && i_write_log('----index----修改index操作---------');
					$rs =  c2_task_model::update_vod($result_vod);
				}
				if($rs){
					DEBUG && i_write_log('----index----C2注入成功操作，更新nns_mgtvbk_c2_task状态为0---------');
					nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='{$c2_task_id}'", $db_w);
					if($result_vod['nns_series_flag']==0){
						nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_type = 'vod' and nns_org_id = '$org_id'  and  nns_ref_id='{$index['nns_src_id']}'", $db_w);	
						//nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='{$index['nns_id']}'", $db_w);
					}
				}else{
					DEBUG && i_write_log('-----index---C2注入失败---------');
				}
			}
		}else{
			DEBUG && i_write_log('-----未知类型:'.$type);
		}
		return true;
	}	
	/*添加切片任务*/
	static public function add_task_by_id($nns_id){
		$db = nn_get_db(NL_DB_READ);	
		$sql="select * from nns_mgtvbk_c2_task where nns_id='$nns_id'";
		$re= nl_db_get_one($sql, $db);unset($sql);
		$nnd_index_id = $re['nns_ref_id'];
		$sql = "select * from nns_vod_index where nns_id='$nnd_index_id'";
		$re_index= nl_db_get_one($sql, $db);unset($sql);
		$task_info = array(
			'nns_task_type'=>'std',
			'nns_org_id'=>$re['nns_org_id'],
			'nns_video_id'=>$re['nns_src_id'],
			'nns_video_index_id'=>$re_index['nns_id'],
			'nns_video_type'=>0,
			'nns_index'=>$re_index['nns_index'],
			'nns_create_time'=>date('Y-m-d H:i:s')
		);
		return clip_task_model::add_task($task_info);
	}
	/*取消切片任务*/
	static public function stop_task_by_id($nns_id){
		$db_w = nn_get_db(NL_DB_WRITE);
		$db = nn_get_db(NL_DB_READ);
		$sql="select * from nns_mgtvbk_c2_task where nns_id='$nns_id'";
		$re= nl_db_get_one($sql, $db);unset($sql);
		$nnd_index_id = $re['nns_ref_id'];
		//
		$sql ="update nns_mgtvbk_clip_task set nns_state='cancel' where nns_org_id='{$re['nns_org_id']}' and nns_video_id='{$re['nns_src_id']}' 
		and nns_video_index_id='{$nnd_index_id}' and  nns_video_type=0";
		return nl_execute_by_db($sql, $db_w);
	}

	static public function rsync($nns_id){		
		$db_w = nn_get_db(NL_DB_WRITE);
		$db = nn_get_db(NL_DB_READ);
		$sql="select * from nns_mgtvbk_c2_task where nns_id='$nns_id'";
		$re= nl_db_get_one($sql, $db);unset($sql);
		
		$sql = "select nns_id,nns_status,nns_action from nns_mgtvbk_c2_task where nns_type='vod' and nns_ref_id='{$re['nns_src_id']}' ".
			"and nns_org_id='{$re['nns_org_id']}'";
		
		$id = nl_db_get_one($sql, $db);unset($sql);
		$status = 0;
		$status2 = 0;
		if($id['nns_action']=='add'){
			$status = 1;
		}elseif($id['nns_action']=='modify'){
			$status = 2;
		}elseif($id['nns_action']=='destroy'){
			$status = 3;
		}
		
		if($re['nns_action']=='add'){
			$status2 = 1;
		}elseif($re['nns_action']=='modify'){
			$status2 = 2;
		}elseif($re['nns_action']=='destroy'){
			$status2 = 3;
		}
		
		nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status='$status' where nns_id='{$id['nns_id']}'", $db_w);
		nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status='$status2' where nns_id='{$re['nns_id']}'", $db_w);
		
		content_task_model::vod($re['nns_id']);
		return content_task_model::vod($id['nns_id']);

	}	
    /**
     * 删除单个分集
     * @param nns_id (nns_mgtvbk_c2_task)
     */
	static public function delete_task_vod_index($nns_id){	
		$db = nn_get_db(NL_DB_READ);
		$db_w = nn_get_db(NL_DB_WRITE);
		
		//取消切片任务
		//self::stop_task_by_id($nns_id);
		$sql="select * from nns_mgtvbk_c2_task where  nns_type='index' and nns_id='$nns_id'";
		$re= nl_db_get_one($sql, $db);unset($sql);
		if($re==false) return false;
		$nns_index_id = $re['nns_ref_id'];

		$sql ="update nns_mgtvbk_clip_task set nns_state='cancel' where nns_org_id='{$re['nns_org_id']}' and nns_video_id='{$re['nns_src_id']}' 
		and nns_video_index_id='{$nns_index_id}' and  nns_video_type=0";
		return nl_execute_by_db($sql, $db_w);
		
		/*****************删除媒体
		$sql = "select * from nns_vod_index where nns_id='$nns_index_id'";
		$vod_index = nl_db_get_one($sql,$db);		
		
		$sql = "select nns_id from nns_vod_media where nns_vod_index_id='$nns_index_id'";
		$vod_medias = nl_db_get_all($sql,$db);
		//删除媒体
		$media_ids = array();
		foreach($vod_medias as $item){
			$media_ids[] = $item['nns_id'];
		}
		c2_task_model::delete_movies(array('nns_ids'=>$media_ids,'nns_task_id'=>$vod_index['nns_id'],'nns_task_name'=>$vod_index['nns_name']));	
		***************/
		
		//删除分集
		$sql = "update nns_mgtvbk_c2_task set nns_action='destroy', nns_status=3 where nns_type='index' and nns_id='$nns_id'";
		nl_execute_by_db($sql,$db_w);			
		return true;
	} 
	/**
	 * 删除整个影片
	 * 1)取消切片任务
	 * 2)删除媒体
	 * 3)删除所有分集
	 * 4)删除影片
	 * @param array(
	 * nns_id :(nns_mgtvbk_c2_task)
	 * 
	 * )
	 */
	static public function delete_task_vod($nns_id,$nns_type){		
		$db = nn_get_db(NL_DB_READ);
		$db_w = nn_get_db(NL_DB_WRITE);	
		$sql = "select * from nns_mgtvbk_c2_task where nns_type='$nns_type' and  nns_id='".$nns_id."'";
		$result = nl_db_get_one($sql,$db);
		if($result==false) return false;
	    if($nns_type=='vod'){
		    //取消切片任务
			$sql ="update nns_mgtvbk_clip_task set nns_state='cancel' where nns_org_id='".$result['nns_org_id']."' and nns_video_id='".$result['nns_ref_id']."'  and  nns_video_type=0";
			nl_execute_by_db($sql, $db_w);			
			/**************删除媒体
			$sql = "select * from nns_vod_index where nns_vod_id='".$result['nns_ref_id']."'";
			$index_all = nl_db_get_all($sql,$db);
			foreach($index_all as $index_item){
				$sql = "select nns_id from nns_vod_media where nns_vod_index_id='".$index_item['nns_id']."'";
				$vod_medias = nl_db_get_all($sql,$db);
				//删除媒体
				$media_ids = array();
				foreach($vod_medias as $item){
					$media_ids[] = $item['nns_id'];
				}
				c2_task_model::delete_movies(array('nns_ids'=>$media_ids,'nns_task_id'=>$index_item['nns_id'],'nns_task_name'=>$index_item['nns_name']));
			}		
			************/
			//删除所有分集
			$sql = "update nns_mgtvbk_c2_task set nns_action='destroy', nns_status=3 where nns_type='index' and nns_src_id='".$result['nns_ref_id']."' and nns_org_id='".$result['nns_org_id']."'";
		    nl_execute_by_db($sql,$db_w);
		    
		    //删除影片基本信息
			$sql = "update nns_mgtvbk_c2_task set nns_action='destroy', nns_status=3 where nns_id='".$nns_id."'";
		    nl_execute_by_db($sql,$db_w);
	    }elseif($nns_type=='live'){
	    	//删除媒体数据
	    	$sql = "select * from nns_live_media where nns_live_id='".$result['nns_ref_id']."'";
			$live_all = nl_db_get_all($sql,$db);
			foreach ($live_all as  $value) {
				c2_task_model::delete_live_media(array(
		    		'nns_id'=>$value['nns_id'],
		    		'nns_task_name'=>$result['nns_name'].'-直播流',
		    	),'DELETE');
			}
			
			//删除节目单
			$sql = "select * from nns_mgtvbk_c2_task where nns_type='playbill' and  nns_org_id='".$result['nns_org_id']."' and nns_src_id='".$result['nns_ref_id']."'";
			$playbill_all = nl_db_get_all($sql,$db);
			if(count($playbill_all)>0){
	    	foreach ($live_all as  $value) {
				c2_task_model::delete_playbill(array(
		    		'nns_id'=>$value['nns_ref_id'],
		    		'nns_task_name'=>$result['nns_name'],
		    	));
				$sql = "update nns_mgtvbk_c2_task set nns_action='destroy', nns_status=3 where nns_type='playbill' and nns_src_id='".$result['nns_ref_id']."' and nns_org_id='".$result['nns_org_id']."'";
	   			nl_execute_by_db($sql,$db_w);
			}
			}
			
			//删除直播
			
			c2_task_model::delete_live(array(
				'nns_id'=>$result['nns_ref_id'],
				'nns_task_name'=>$result['nns_name']
			));
			
			//删除影片基本信息
			$sql = "update nns_mgtvbk_c2_task set nns_action='destroy', nns_status=0 where nns_id='".$nns_id."'";
		    nl_execute_by_db($sql,$db_w);
			
	    	
	    }
	    return true; 
	    
	    
	}	
	
	static public function get_vod_task_list($sp_id,$filter=null,$start=0,$size=100){
		$db = nn_get_db(NL_DB_READ);

        $sql = "select * " .
        		"from nns_mgtvbk_c2_task  where nns_org_id='$sp_id'";		
		if($filter != null){
			$wh_arr = array();
			if(!empty($filter['nns_name'])){
				$wh_arr[] = " nns_name like '%".$filter['nns_name']."%'";
			}
			if(!empty($filter['day_picker_start'])){
				$wh_arr[] = " nns_create_time>='".$filter['day_picker_start']."'";
			}
			if(!empty($filter['day_picker_end'])){
				$wh_arr[] = " nns_create_time<='".$filter['day_picker_end']."'";
			}
			if(!empty($filter['nns_type'])){
				$wh_arr[] = " nns_type='".$filter['nns_type']."'";
			}			
			if(!empty($filter['nns_action'])){
				$wh_arr[] = " nns_action='".$filter['nns_action']."'";
			}			
			if(!empty($filter['nns_state'])){
				//$wh_arr[] = " nns_id='".$filter['nns_id']."'";
				$state = $filter['nns_state'];
				switch($state){
					case 'content_wait':
					$wh_arr[] = " nns_status in(1,2,3,5)";
					break;
					case 'content_ok':
					$wh_arr[] = " nns_status=0";
					break;
					case 'content_fail':
					$wh_arr[] = " nns_status=-1";
					break;
					case 'epg_ok':
					$wh_arr[] = " nns_status=99";
					break;
				}
			}			
			if(!empty($wh_arr))	$sql .= " and ".implode(" and ",$wh_arr);	
		}
		
        $count_sql = $sql;
        $count_sql = str_replace('*',' count(*) as temp_count ',$count_sql);
 		$sql .= " order by nns_create_time desc ";
		$sql .= " limit $start,$size";
        $data = nl_db_get_all($sql,$db);
    	$rows = nl_db_get_col($count_sql,$db);
    	return array('data'=>$data,'rows'=>$rows);
	}
	
	static public function get_vod_name($vod_id){
		$db = nn_get_db(NL_DB_READ);
		return nl_db_get_col("select nns_name from nns_vod where nns_id='".$vod_id."'",$db);
	}
	static public function get_vod_index_name($vod_index_id){
		$db = nn_get_db(NL_DB_READ);
		return nl_db_get_col("select nns_name from nns_vod_index where nns_id='".$vod_index_id."'",$db);
	}
	static public function update_c2_task_name($nns_id,$nns_name){
		$db = nn_get_db(NL_DB_WRITE);
		return nl_execute_by_db("update nns_mgtvbk_c2_task set nns_name='".$nns_name."' where nns_id='".$nns_id."'", $db);
	}
	
	static public function resend_c2_task_all($action,$ids){
		$status = 0;	
		switch($action){
			case 'add':
				$status = 1;
				break;
			case 'modify':
				$status = 2;
                break;
            case 'destroy':
                $status = 3;
                break;
        }
		$arr = explode(',', $ids);
		foreach ($arr as  $value) {
			$tmp = array('c2_task_id'=>$value,'status'=>$status,'action'=>$action);
			self::resend_c2_task($tmp);
		}
		return 1;
	}
		
	static public function resend_c2_task($query){
		$nns_id = $query['c2_task_id'];	
		$db_w = nn_get_db(NL_DB_WRITE);
		$db = nn_get_db(NL_DB_READ);
		$sql = "select * from nns_mgtvbk_c2_task where nns_id='".$nns_id."'";
		$c2_task_info = nl_db_get_one($sql,$db);
		if($c2_task_info == false)return false;		
		if(!empty($query['action'])&&!empty($query['status'])){
			$status = $query['status'];
			$action = $query['action'];
			nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=$status,nns_action='".$action."' where nns_id='".$nns_id."'",$db_w);			
		}else{
			$status = 1;
			$action = 'add';
			if( ($c2_task_info['nns_action']=='add' || $c2_task_info['nns_action']=='modify') && $c2_task_info['nns_status']==0){//注入有成功，则修改
				$status = 2;
				$action= 'modify';
			}
			nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=$status,nns_action='".$action."' where nns_id='".$nns_id."'",$db_w);
		}
		if($query['action']=='add'){
			$log_summary="对【".$c2_task_info['nns_name']."】执行【添加命令】";
		}elseif($query['action']=='modify'){
			$log_summary="对【".$c2_task_info['nns_name']."】执行【修改命令】";
		}elseif($query['action']=='destroy'){
			$log_summary="对【".$c2_task_info['nns_name']."】执行【删除命令】";
		}
		log_model::log_write($log_summary);
		content_task_model::vod($nns_id);
		return true;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	//点播
	static public function copy_vod(){
		$db = nn_get_db(NL_DB_READ);

		$sql="select * from nns_vod";
		$result = nl_db_get_all($sql,$db);unset($sql);
		foreach ($result as $value) {
			$id = $value['nns_id'];
			$type = 'vod';
			category_model::category_content($id, $type);
			$sql_index = "select * from nns_vod_index where nns_vod_id='$id'";
			$result_index = nl_db_get_all($sql_index,$db);unset($sql_index);
			foreach ($result_index as $value_index) {
				$id = $value_index['nns_id'];
				$type = 'index';
				category_model::category_content($id, $type);
			}
		}
		
		//self::vod();
	}
//epg注入日志
	static public function get_mgtvbk_import_epg_log($sp_id,$filter=null,$start=0,$size=100){
		 
		$db = nn_get_db(NL_DB_READ);
		$sql = "select * " .
        		"from nns_mgtvbk_import_epg_log  where nns_org_id='$sp_id'";		
		if($filter != null){
			$wh_arr = array();
			 
			if(!empty($filter['day_picker_start'])){
				$wh_arr[] = " nns_create_time>='".$filter['day_picker_start']."'";
			}
			if(!empty($filter['day_picker_end'])){
				$wh_arr[] = " nns_create_time<='".$filter['day_picker_end']."'";
			}
			if(!empty($filter['nns_category'])){
				$wh_arr[] = " nns_video_type='".$filter['nns_category']."'";
			}
			if(!empty($filter['nns_name'])){
				$wh_arr[] = " nns_data like '%".$filter['nns_name']."%'";
			}
			if(!empty($filter['nns_id'])){
			   $wh_arr[] = " nns_id='".$filter['nns_id']."'";
				 
			}
			if(!empty($wh_arr))	$sql .= " and ".implode(" and ",$wh_arr);
		} 
		$count_sql = $sql;
		$count_sql = str_replace('*',' count(*) as temp_count ',$count_sql);
		$sql .= " order by nns_create_time desc ";
		$sql .= " limit $start,$size";  
		$data = nl_db_get_all($sql,$db); 
		//$count_sql  = "SELECT FOUND_ROWS() "; 
		$rows = nl_db_get_col($count_sql,$db);
		return array('data'=>$data,'rows'=>$rows);
	}
	//直播
	static public function copy_live(){
		$db = nn_get_db(NL_DB_READ);
		$sql="select * from nns_live where  nns_deleted=0 limit 2";
		$result = nl_db_get_all($sql,$db);unset($sql);
		foreach ($result as $value) {
			$id = $value['nns_id'];
			$type = 'live';//直播标识
			category_model::category_content($id, $type,1);//添加直播频道
		}	
	}
	
	static public function import_epg($query){
		include_once dirname(dirname(dirname(__FILE__))) . '/import/import.class.php';
		$db_r = nn_get_db(NL_DB_READ);
		$db_w = nn_get_db(NL_DB_WRITE);
		$url = IMPROT_EPG_URL;
		$log = IMPROT_EPG_LOG;
		$import_obj = new import($url, $log);
		$sql="select * from nns_mgtvbk_c2_task where nns_id='".$query['nns_id']."'";		
		$c2_info = nl_db_get_one($sql, $db_r);unset($sql);
		$xml = '';
		switch ($c2_info['nns_type']) {
			case 'vod':
				$sql = "select * from nns_vod where nns_id='{$c2_info['nns_ref_id']}'";
				$info = nl_db_get_one($sql, $db_r);
				if($query['action']=='destroy'){
					$result = $import_obj -> delete_assets($info['nns_id']);
				}else{
					$xml = '<video assets_id="' . $info['nns_id'] . '" ';
					$sql = "select * from nns_depot where nns_id='{$info['nns_depot_id']}'";
					$depot = nl_db_get_one($sql, $db_r);
					$nns_asset_import_id = $info['nns_asset_import_id'];
					$sql = "SELECT * FROM `nns_vod_ex` where nns_vod_id='{$nns_asset_import_id}'";
					$vod_ex = nl_db_get_all($sql, $db_r);					
					$category_name = '';
					$dom = new DOMDocument('1.0', 'utf-8');
					$depot_info = $depot['nns_category'];
					$nns_category_id = $info['nns_category_id'];
					$dom -> loadXML($depot_info);
					$categorys = $dom -> getElementsByTagName('category');
					foreach ($categorys as $category) {
						if ($category -> getAttribute('id') == (int)$nns_category_id) {
							$category_name = $category -> getAttribute('name');
							break;
						}
					}
					$xml .= 'assets_category="' . $category_name . '" video_type="0" ';
					$xml .= 'name="' . $info['nns_name'] . '" ';
					$xml .= 'sreach_key="' . str_replace("/", " ", $info['nns_keyword']) . '" ';
					$xml .= 'view_type="' . $info['nns_view_type'] . '" ';
					$xml .= 'director="' . str_replace("/", " ", $info['nns_director']) . '" ';
					$xml .= 'player="' . str_replace("/", " ", $info['nns_actor']) . '" ';
					$xml .= 'tag="' . str_replace("/", " ", $info['nns_kind']) . '" ';
					$xml .= 'region="' . str_replace("/", " ", $info['nns_area']) . '" ';
					$xml .= 'release_time="' . $info['nns_show_time'] . '" ';
					$xml .= 'totalnum="' . $info['nns_all_index'] . '" ';
					foreach ($vod_ex as $value) {
						$xml .= $value['nns_key'] . '="' . $value['nns_value'] . '" ';
					}
					$xml .= 'smallpic="' . $info['nns_image2'] . '" ';
					$xml .= 'bigpic="' . $info['nns_image0'] . '" ';
					$xml .= 'intro="' . $info['nns_summary'] . '" ';
					$xml .= 'producer="' . str_replace("/", " ", $info['nns_producer']) . '" ';
					$xml .= 'play_role="' . $info['nns_play_role'] . '" ';
					$xml .= 'subordinate_name="' . str_replace("/", " ", $info['nns_alias_name']) . '" ';
					$xml .= 'english_name="' . str_replace("/", " ", $info['nns_eng_name']) . '" ';
					$xml .= 'language="' . str_replace("/", " ", $info['nns_language']) . '" ';
					$xml .= 'caption_language="' . str_replace("/", " ", $info['nns_text_lang']) . '" ';
					$xml .= 'copyright_range="' . str_replace("/", " ", $info['nns_remark']) . '" ';
					$xml .= 'copyright="' . str_replace("/", " ", $info['nns_copyright']) . '" ';
					$xml .= 'total_clicks="' . $info['nns_play_count'] . '" ';
					$xml .= 'day_clicks="0" ';
					$xml .= '></video>';
					
					$result = $import_obj -> import_assets($xml);
				}
				$re = json_decode($result, true);
				if ($re['ret'] == '0') {
					$data = array(
					'nns_id' => np_guid_rand('vod'), 
					'nns_video_id' => $info['nns_id'], 
					'nns_video_type' => 'vod', 
					'nns_org_id' => SP_ORG_ID, );
					nl_db_insert($db_w, 'nns_mgtvbk_import_epg', $data);
				}
				$log_data = array(
					'nns_id' => np_guid_rand('vod'),
					'nns_video_id' => $info['nns_id'],
					'nns_video_type' => 'vod', 
					'nns_org_id' => SP_ORG_ID,
					'nns_create_time'=>date('Y-m-d H:i:s'),
					'nns_action'=>$query['action'],
					'nns_status'=>$re['ret'],
					'nns_reason'=>$re['reason'],
					'nns_data'=>$xml,
				);
				nl_db_insert($db_w, 'nns_mgtvbk_import_epg_log', $data);
				break;
			case 'index':
				$sql = "select * from nns_vod_index where nns_id='{$c2_info['nns_ref_id']}'";
				$info = nl_db_get_one($sql, $db_r);				
				if($query['action']=='destroy'){
					$result = $import_obj -> delete_video_clip($info['nns_id']);
				}else{
					$xml = '<index clip_id="' . $info['nns_id'] . '" ';
					$xml .= 'name="' . $info['nns_name'] . '" ';
					$xml .= 'index="' . $info['nns_index'] . '" ';
					$xml .= 'time_len="' . $info['nns_time_len'] . '" ';
					$xml .= 'summary="' . $info['nns_summary'] . '" ';
					$xml .= 'pic="' . $info['nns_image'] . '" ';
					$xml .= 'director="' . str_replace("/", " ", $info['nns_director']) . '" ';
					$xml .= 'player="' . str_replace("/", " ", $info['nns_actor']) . '" ';
					$xml .= 'total_clicks="' . $info['nns_play_count'] . '" ';
					$xml .= 'release_time="' . $info['nns_create_time'] . '" ';
					$xml .= 'update_time="' . $info['nns_modify_time'] . '" ';
					$xml .= 'month_clicks="" ';
					$xml .= 'week_clicks="" ';
					$xml .= 'day_clicks="" ';
					$xml .= '></index>';
					$result = $import_obj -> import_video_clip($info['nns_vod_id'], $info['nns_id'], $xml);
				}
				$re = json_decode($result, true);
				if ($re['ret'] == '0') {
					$data = array(
					'nns_id' => np_guid_rand('index'), 
					'nns_video_id' => $info['nns_id'], 
					'nns_video_type' => 'index', 
					'nns_org_id' => SP_ORG_ID, );
					nl_db_insert($db_w, 'nns_mgtvbk_import_epg', $data);
				}
				$log_data = array(
					'nns_id' => np_guid_rand('index'),
					'nns_video_id' => $info['nns_id'],
					'nns_video_type' => 'index', 
					'nns_org_id' => SP_ORG_ID,
					'nns_create_time'=>date('Y-m-d H:i:s'),
					'nns_action'=>$query['action'],
					'nns_status'=>$re['ret'],
					'nns_reason'=>$re['reason'],
					'nns_data'=>$xml,
				);
				nl_db_insert($db_w, 'nns_mgtvbk_import_epg_log', $data);
				break;
			case 'media':
				$sql = "select * from nns_vod_media where nns_id='{$c2_info['nns_ref_id']}'";
				$info = nl_db_get_one($sql, $db_r);
				if ($query['action'] == 'destroy') {
					$result = $import_obj -> delete_video_file($info['nns_id']);
				} else {
					$xml = '<media file_id="' . $info['nns_id'] . '" file_type="' . $info['nns_filetype'] . '" ';
					$xml .= 'file_path="' . $info['nns_url'] . '" file_definition="' . $info['nns_mode'] . '" ';
					$xml .= 'file_resolution="" file_size="" file_bit_rate="' . $info['nns_kbps'] . '" file_desc="" ';
					if ($info['nns_dimensions'] == '0') {
						$xml .= 'file_3d="0" file_3d_type=""></media>';
					} elseif ($info['nns_dimensions'] == '100001') {
						$xml .= 'file_3d="1" file_3d_type="0"></media>';
					} elseif ($info['nns_dimensions'] == '100002') {
						$xml .= 'file_3d="1" file_3d_type="1"></media>';
					}
					$result = $import_obj -> import_video_file($info['nns_vod_id'], $info['nns_vod_index_id'], $info['nns_id'], $xml);
				}
				$re = json_decode($result, true);
				if ($re['ret'] == '0') {
					$data = array('nns_id' => np_guid_rand('media'), 'nns_video_id' => $info['nns_id'], 'nns_video_type' => 'media', 'nns_org_id' => SP_ORG_ID, );
					nl_db_insert($db_w, 'nns_mgtvbk_import_epg', $data);
				}		
				$log_data = array(
					'nns_id' => np_guid_rand('media'),
					'nns_video_id' => $info['nns_id'],
					'nns_video_type' => 'media', 
					'nns_org_id' => SP_ORG_ID,
					'nns_create_time'=>date('Y-m-d H:i:s'),
					'nns_action'=>$query['action'],
					'nns_status'=>$re['ret'],
					'nns_reason'=>$re['reason'],
					'nns_data'=>$xml,
				);
				nl_db_insert($db_w, 'nns_mgtvbk_import_epg_log', $data);
				break;
		}
		return true;		
	}
}


