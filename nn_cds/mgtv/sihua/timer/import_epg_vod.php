<?php
header("Content-type: text/html; charset=utf-8");
error_reporting(E_ALL);
ini_set('display_errors',1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
include_once dirname(dirname(__FILE__)) . '/init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/import/import.class.php';

$db_r = nn_get_db(NL_DB_READ);
$db_w = nn_get_db(NL_DB_WRITE);
$url = IMPROT_EPG_URL;
$log = IMPROT_EPG_LOG;

$num = 1500;
$sql = "select * from nns_mgtvbk_c2_task where nns_status=0 and nns_type='vod' and nns_org_id='sihua' order by nns_create_time desc limit $num";

$list = nl_db_get_all($sql, $db_r);
if (count($list) > 0) {
	echo "媒资总数".count($list)."\r\n";
	$vod_i = 0;
	$vod_j = 0;
	foreach ($list as $value) {
		$action = $value['nns_action'];
		$import_obj = new import($url, $log);
		$sql = "select * from nns_vod where nns_id='{$value['nns_ref_id']}'";
		$info = nl_db_get_one($sql, $db_r);		
		if(empty($info)){
			$sql = "delete from nns_mgtvbk_c2_task where nns_org_id='sihua' and nns_type='vod' and nns_ref_id='{$value['nns_ref_id']}'";
			//echo $sql;
			nl_execute_by_db($sql, $db_w);
			$vod_j++;
			continue;
		}		
		if ($action == 'destroy') {
			$result = $import_obj -> delete_assets($info['nns_id']);
		}else{		
			$xml = '<video assets_id="' . $info['nns_id'] . '" ';
			$sql = "select * from nns_depot where nns_id='{$info['nns_depot_id']}'";
			$depot = nl_db_get_one($sql, $db_r);
	
			$nns_asset_import_id = $info['nns_asset_import_id'];
			$sql = "SELECT * FROM `nns_vod_ex` where nns_vod_id='{$nns_asset_import_id}'";
			$vod_ex = nl_db_get_all($sql, $db_r);
	
			$category_name = '';
			$dom = new DOMDocument('1.0', 'utf-8');
			$depot_info = $depot['nns_category'];
			$nns_category_id = $info['nns_category_id'];
			$dom -> loadXML($depot_info);
			$categorys = $dom -> getElementsByTagName('category');
			foreach ($categorys as $category) {
				if ($category -> getAttribute('id') == (int)$nns_category_id) {
					$category_name = $category -> getAttribute('name');
					break;
				}
			}
			$xml .= 'assets_category="' . $category_name . '" video_type="0" ';
			$xml .= 'name="' . $info['nns_name'] . '" ';
			$xml .= 'sreach_key="' . str_replace("/", ",", $info['nns_keyword']) . '" ';
			$xml .= 'view_type="' . $info['nns_view_type'] . '" ';
			$xml .= 'director="' . str_replace("/", ",", $info['nns_director']) . '" ';
			$xml .= 'player="' . str_replace("/", ",", $info['nns_actor']) . '" ';
			$xml .= 'tag="' . str_replace("/", ",", $info['nns_kind']) . '" ';
			$xml .= 'region="' . str_replace("/", ",", $info['nns_area']) . '" ';
			$xml .= 'release_time="' . $info['nns_show_time'] . '" ';
			$xml .= 'totalnum="' . $info['nns_all_index'] . '" ';
			foreach ($vod_ex as $v) {
				$xml .= $v['nns_key'] . '="' . $v['nns_value'] . '" ';
			}
			$xml .= 'smallpic="' . $info['nns_image2'] . '" ';
			$xml .= 'bigpic="' . $info['nns_image0'] . '" ';
			$xml .= 'intro="' . $info['nns_summary'] . '" ';
			$xml .= 'producer="' . str_replace("/", ",", $info['nns_producer']) . '" ';
			$xml .= 'play_role="' . $info['nns_play_role'] . '" ';
			$xml .= 'subordinate_name="' . str_replace("/", ",", $info['nns_alias_name']) . '" ';
			$xml .= 'english_name="' . str_replace("/", ",", $info['nns_eng_name']) . '" ';
			$xml .= 'language="' . str_replace("/", ",", $info['nns_language']) . '" ';
			$xml .= 'caption_language="' . str_replace("/", ",", $info['nns_text_lang']) . '" ';
			$xml .= 'copyright_range="' . str_replace("/", ",", $info['nns_remark']) . '" ';
			$xml .= 'copyright="' . str_replace("/", ",", $info['nns_copyright']) . '" ';
			$xml .= 'total_clicks="' . $info['nns_play_count'] . '" ';
			$xml .= 'day_clicks="0" ';
			$xml .= 'original_id="'.$info['nns_asset_import_id'].'" ';
			$xml .= '></video>';
			error_log($action."\r\n",3,$log.'/'.date('Ymd').'.txt');
			//媒资注入
	
			$result = $import_obj -> import_assets($xml);
			error_log($xml."\r\n",3,$log.'/'.date('Ymd').'.txt');			
		}
		$re = json_decode($result, true);		
		error_log(var_export($re,true)."\r\n",3,$log.'/'.date('Ymd').'.txt');
		error_log("=============vod=============vod==============vod==================="."\r\n",3,$log.'/'.date('Ymd').'.txt');
		if ($re['ret'] == '0') {
			//$data = array('nns_id' => np_guid_rand('vod'), 'nns_video_id' => $info['nns_id'], 'nns_video_type' => 'vod', 'nns_org_id' => 'sihua', );
			//nl_db_insert($db_w, 'nns_mgtvbk_import_epg', $data);
			$sql ="update nns_mgtvbk_c2_task set nns_status=99 where nns_id='{$value['nns_id']}'";
			nl_execute_by_db($sql,$db_w);
			$vod_i++;
		}else{
			$vod_j++;
			echo var_export($re,true);
		}
		$log_data = array(
			'nns_id' => np_guid_rand('vod'),
			'nns_video_id' => $info['nns_id'],
			'nns_video_type' => 'vod', 
			'nns_org_id' => 'sihua',
			'nns_create_time'=>date('Y-m-d H:i:s'),
			'nns_action'=>$action,
			'nns_status'=>$re['ret'],
			'nns_reason'=>$re['reason'],
			'nns_data'=>$xml,
			'nns_name'=>$value['nns_name'],
		);
		nl_db_insert($db_w, 'nns_mgtvbk_import_epg_log', $log_data);
	}

	echo "OK：".$vod_i."\r\n";
	echo "Fail：".$vod_j."\r\n";
	echo "======================================================";
}
?>
