<?php
header("Content-type: text/html; charset=utf-8");
error_reporting(E_ALL);
ini_set('display_errors',1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
include_once dirname(dirname(__FILE__)) . '/init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/import/import.class.php';

$db_r = nn_get_db(NL_DB_READ);
$db_w = nn_get_db(NL_DB_WRITE);
$url = IMPROT_EPG_URL;
$log = IMPROT_EPG_LOG;


$index_num = 3000;

$sql = "select * from nns_mgtvbk_c2_task where nns_type='index' and nns_org_id='sihua' and nns_status=0  order by nns_create_time desc  limit $index_num";
$list = nl_db_get_all($sql, $db_r);

if (is_array($list) && count($list) > 0) {
	echo "分集总数".count($list)."\r\n";
	$index_i = 0;
	$index_j = 0;
	foreach ($list as $value) {
		$action = $value['nns_action'];
		$import_obj = new import($url, $log);
		$sql = "select * from nns_vod_index where nns_id='{$value['nns_ref_id']}'";
		
		$info = nl_db_get_one($sql, $db_r);		
		if(empty($info)){//分集不存在，执行删除操作
			$sql = "delete from nns_mgtvbk_c2_task where nns_org_id='sihua' and nns_type='index' and nns_ref_id='{$value['nns_ref_id']}'";
			//echo $sql;
			nl_execute_by_db($sql, $db_w);
			$index_j++;
			$action = 'destroy';
		}
		if ($action == 'destroy') {
			$result = $import_obj -> delete_video_clip($info['nns_id']);
		} else {		
			//判断该分集的所属的主媒资是否注入		
			$sql = "select * from nns_mgtvbk_c2_task where nns_type='vod' and nns_org_id='sihua' and nns_ref_id='{$value['nns_src_id']}' and nns_status=99 and nns_action in('add','modify')";
			$vod_ok = nl_db_get_one($sql, $db_r);	
			if(empty($vod_ok)){//主媒资没有注入，则不往下执行注入分集
				continue;
			}
				
			
	
			$xml = '<index clip_id="' . $info['nns_id'] . '" ';
			$xml .= 'name="' . $info['nns_name'] . '" ';
			$xml .= 'index="' . $info['nns_index'] . '" ';
			$xml .= 'time_len="' . $info['nns_time_len'] . '" ';
			$xml .= 'summary="' . $info['nns_summary'] . '" ';
			$xml .= 'pic="' . $info['nns_image'] . '" ';
			$xml .= 'director="' . str_replace("/", ",", $info['nns_director']) . '" ';
			$xml .= 'player="' . str_replace("/", ",", $info['nns_actor']) . '" ';
			$xml .= 'total_clicks="' . $info['nns_play_count'] . '" ';
			$xml .= 'release_time="' . $info['nns_create_time'] . '" ';
			$xml .= 'update_time="' . $info['nns_modify_time'] . '" ';
			$xml .= 'month_clicks="" ';
			$xml .= 'week_clicks="" ';
			$xml .= 'day_clicks="" ';
			$xml .= 'original_id="'.$info['nns_import_id'].'" ';
			$xml .= '></index>';
			error_log($action."\r\n",3,$log.'/'.date('Ymd').'.txt');

				$result = $import_obj -> import_video_clip($info['nns_vod_id'], $info['nns_id'], $xml);
				error_log("nns_vod_id=".$info['nns_vod_id']."\r\n",3,$log.'/'.date('Ymd').'.txt');
				error_log("nns_id=".$info['nns_id']."\r\n",3,$log.'/'.date('Ymd').'.txt');
				error_log($xml."\r\n",3,$log.'/'.date('Ymd').'.txt');
		}
		
		$re = json_decode($result, true);
		
		error_log(var_export($re,true)."\r\n",3,$log.'/'.date('Ymd').'.txt');
		error_log("--------index--------------index--------------------index----------------------------------"."\r\n",3,$log.'/'.date('Ymd').'.txt');
		
		if ($re['ret'] == '0') {
			//$data = array('nns_id' => np_guid_rand('index'), 'nns_video_id' => $info['nns_id'], 'nns_video_type' => 'index', 'nns_org_id' => 'sihua', );
			//nl_db_insert($db_w, 'nns_mgtvbk_import_epg', $data);
			$sql ="update nns_mgtvbk_c2_task set nns_status=99 where nns_id='{$value['nns_id']}'";
			nl_execute_by_db($sql,$db_w);			
			$index_i++;
		}else{
			$index_j++;
			echo var_export($re,true);
		}
		$log_data = array(
			'nns_id' => np_guid_rand('index'),
			'nns_video_id' => $info['nns_id'],
			'nns_video_type' => 'index', 
			'nns_org_id' => 'sihua',
			'nns_create_time'=>date('Y-m-d H:i:s'),
			'nns_action'=>$action,
			'nns_status'=>$re['ret'],
			'nns_reason'=>$re['reason'],
			'nns_data'=>$xml,
			'nns_name'=>$value['nns_name'],
		);
		nl_db_insert($db_w, 'nns_mgtvbk_import_epg_log', $log_data);
		
		unset($info);
	}

	echo "OK：".$index_i."\r\n";
	echo "Fail：".$index_j."\r\n";
	echo "======================================================";
}

?>
