<?php
ini_set('display_errors',0);
set_time_limit(0);
include_once  dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv_v2/mgtv_init.php';
define('ORG_ID', 'sihua');
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/'.ORG_ID.'/init.php';
include_once  dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv_v2/'.ORG_ID.'/define.php';

include_once  dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv_v2/models/queue_task_model.php';


$db = nl_get_db(NL_DB_WRITE);
$db->open();
$ip = i_get_ip();
i_write_log_core('notify from '.$ip,'sihua/c2');
$postdata = file_get_contents("php://input");

i_write_log_core('notify data '.$postdata,'sihua/c2');
$xml = $postdata;
$dom = new DOMDocument('1.0', 'utf-8');
$dom -> loadXML($xml);
$xpath = new DOMXPath($dom);


$log_path_temp = '/data/log/mgtv/'.ORG_ID.'/notify/'.date('Ymd');
$log_path = dirname(dirname(dirname(dirname(__FILE__)))).$log_path_temp;
if(!is_dir($log_path)){
	mkdir($log_path,0777,true);
}

$obj = @simplexml_load_string($postdata);
$asset_info = json_decode(json_encode($obj), true);
$entries = $xpath -> query('/message/body/tasks/task');
$result = 0;
$asset_list_all = array();
foreach ($entries as $item) {	
	$asset_lists = array(
	'id' => $item -> getAttribute('id'), 
	'content-id' => $item -> getAttribute('content-id'), 
	'subcontent-id' => $item -> getAttribute('subcontent-id'), 
	'status' => $item -> getAttribute('status'), 
	'target-system-id' => $item -> getAttribute('target-system-id'), 
	'begin-time' => $item -> getAttribute('begin-time'), 
	'end-time' => $item -> getAttribute('end-time'), 
	'url' => trim($item->nodeValue), 
	'timestamp'=>$asset_info['header']['@attributes']['timestamp'],
	'sequence'=>$asset_info['header']['@attributes']['sequence'],
	'component-id'=>$asset_info['header']['@attributes']['component-id'],
	'component-type'=>$asset_info['header']['@attributes']['component-type'],
	'action'=>$asset_info['header']['@attributes']['action'],
	'command'=>$asset_info['header']['@attributes']['command'],
	);
	$asset_list_all[]=$asset_lists;
}


$xml='';
$dt = date('Y-m-d H:i:s');
foreach ($asset_list_all as $asset_list) {
if($asset_list['status']==6){
    
    
    if(stripos($asset_list['subcontent-id'],'live_')==0){
        $live_id = str_replace('live_', '', $asset_list['subcontent-id']);
        $sql_live_ex = "select * from nns_live_ex where nns_live_id='{$live_id}' and nns_key='sihua_live'";
        $live_ex_info = nl_db_get_one($sql_live_ex, $db);
        if(is_array($live_ex_info)&&$live_ex_info!=null){
            $sql_live_sql_ex = "update  nns_live_ex set nns_value='{$asset_list['url']}' where nns_live_id='{$live_id}' and nns_key='sihua_live'";
        }else{
            $sql_live_sql_ex = "insert into  nns_live_ex (nns_live_id,nns_key,nns_value) values ('{$live_id}','sihua_live','{$asset_list['url']}') ";
        }
        nl_execute_by_db($sql_live_sql_ex, $db);
    }
    
    
    
    
    
	$sql="update nns_mgtvbk_c2_task set nns_status=0,nns_ex_url='{$asset_list['url']}' , nns_modify_time='".$dt."'  where nns_org_id='sihua' and nns_type='media' and  nns_ref_id='{$asset_list['subcontent-id']}'";
	nl_execute_by_db($sql, $db);//unset($sql);
	
	$sqltask = $sql="select * from  nns_mgtvbk_c2_task  where nns_org_id='sihua' and nns_type='media' and  nns_ref_id='{$asset_list['subcontent-id']}'";
		$c2_task_info = nl_db_get_one($sqltask, $db);
		if(is_array($c2_task_info)){
			$update = "update  nns_mgtvbk_c2_log set nns_notify_result='0' , nns_notify_time='".$dt."'  where nns_org_id='sihua' and nns_task_type='Movie' and nns_task_id='{$c2_task_info['nns_id']}' and nns_notify_result is null ";
			nl_execute_by_db($update, $db);
            $queue_task_model = new queue_task_model();
            $queue_task_model->q_report_task($c2_task_info['nns_id']);
		}	

	i_write_log_core('notify data '.$sql,'sihua/c2');
//echo $sql;
	//$sql = "update nns_vod_media set nns_ext_url='{$asset_list['url']}' where nns_id='{$asset_list['subcontent-id']}'";
//echo $sql;
	//i_write_log('notify data '.$sql,'c2');
	nl_execute_by_db($sql, $db);
		$xml = '<message module="iCMS" version="1.0"><header timestamp="'.date('Y-m-d').'T'.date('H:i:s').'.0Z" sequence="'.$asset_list['sequence'].'" component-id="'.$asset_list['component-id'].'" component-type="'.$asset_list['component-type'].'" action="RESPONSE" command="ONLINE_TASK_DONE" /><body><result code="1" description="OK" /></body></message>';
	}else{
		$xml = '<message module="iCMS" version="1.0"><header timestamp="'.date('Y-m-d').'T'.date('H:i:s').'.0Z" sequence="'.$asset_list['sequence'].'" component-id="'.$asset_list['component-id'].'" component-type="'.$asset_list['component-type'].'" action="RESPONSE" command="ONLINE_TASK_DONE" /><body><result code="0" description="OK" /></body></message>';
		break;
	}
}
echo $xml;
