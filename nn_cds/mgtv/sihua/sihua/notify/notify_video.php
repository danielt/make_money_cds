<?php

/**
 * 通知xml信息是否发送success
 * 
 */
ini_set('display_errors',1);
include_once dirname(dirname(dirname(__FILE__))).'/mgtv_init.php';
include_once dirname(dirname(__FILE__)).'/init.php';

$ip = i_get_ip();
i_write_log('notify from '.$ip,'c2');
$postdata = file_get_contents("php://input");
i_write_log('notify data '.$postdata,'c2');
$xml = $postdata;
$dom = new DOMDocument('1.0', 'utf-8');
$dom -> loadXML($xml);
$xpath = new DOMXPath($dom);
$asset_list = array();
$nns_guid = $xpath->query('/SyncContentsResult')->item(0)->getAttribute('serialNo');
$entries = $xpath -> query('/SyncContentsResult/Assets/Asset');
foreach ($entries as $item) {
	$array = array(
	'ID' => $item -> getAttribute('ID'), 
	'currentID' => $item -> getAttribute('currentID'), 
	'type' => $item -> getAttribute('type'), 
	'op' => $item -> getAttribute('op'), 
	'result' => $item -> getAttribute('result'), 
	'desc' => $item -> getAttribute('desc'), 
	);
	$asset_list[] = $array;
}
$array = array('guid'=>$nns_guid,'data'=>$asset_list);
i_write_log('notify data OK '.$array,'c2');