<?php

class nn_upload_class {

    public function __construct($config=array())
    {
    	$this->_init($config);
    }
    private function _init($config=array())
    {
    	$defaults = array(
    	'max_size' =>0,
    	'allowed_types'=>'*',
    	'upload_path'=>'',
    	);
		foreach ($defaults as $key => $val)
		{
			if (isset($config[$key]))
			{
				$method = 'set_'.$key;
				if (method_exists($this, $method))
				{
					$this->$method($config[$key]);
				}
				else
				{
					$this->$key = $config[$key];
				}
			}
			else
			{
				$this->$key = $val;
			}
		}
    }

    public function do_upload($field = 'upfile')
    {
		// Was the file able to be uploaded? If not, determine the reason why.
		if ( ! is_uploaded_file($_FILES[$field]['tmp_name']))
		{
			$error = ( ! isset($_FILES[$field]['error'])) ? 4 : $_FILES[$field]['error'];

			switch($error)
			{
				case 1:	// UPLOAD_ERR_INI_SIZE
					$this->set_error('upload_file_exceeds_limit');
					break;
				case 2: // UPLOAD_ERR_FORM_SIZE
					$this->set_error('upload_file_exceeds_form_limit');
					break;
				case 3: // UPLOAD_ERR_PARTIAL
					$this->set_error('upload_file_partial');
					break;
				case 4: // UPLOAD_ERR_NO_FILE
					$this->set_error('upload_no_file_selected');
					break;
				case 6: // UPLOAD_ERR_NO_TMP_DIR
					$this->set_error('upload_no_temp_directory');
					break;
				case 7: // UPLOAD_ERR_CANT_WRITE
					$this->set_error('upload_unable_to_write_file');
					break;
				case 8: // UPLOAD_ERR_EXTENSION
					$this->set_error('upload_stopped_by_extension');
					break;
				default :   $this->set_error('upload_no_file_selected');
					break;
			}

			return FALSE;
		}
		// Set the uploaded data as class variables
		$this->file_temp = $_FILES[$field]['tmp_name'];
		$this->file_size = $_FILES[$field]['size'];
		$fix = explode('.',$_FILES[$field]['name']);
		$this->_file_mime_type($_FILES[$field]);
		$this->file_type = preg_replace("/^(.+?);.*$/", "\\1", $this->file_type);
		$this->file_type = strtolower(trim(stripslashes($this->file_type), '"'));
		$this->file_name = md5($this->_prep_filename(rand(100,999).'_'.$_FILES[$field]['name'])).'.'.$fix[1];

		$this->file_ext	 = $this->get_extension($this->file_name);
		$this->client_name = $this->file_name;
    	//
    	if( ! $this->is_allowed_filetype())
    	{
    		$this->set_error('upload_invalid_filetype');
			return FALSE;
    	}
		// Convert the file size to kilobytes
		if ($this->file_size > 0)
		{
			$this->file_size = round($this->file_size/1024, 2);
		}

		// Is the file size within the allowed maximum?
		if ( ! $this->is_allowed_filesize())
		{
			$this->set_error('upload_invalid_filesize');
			return FALSE;
		}
		if ( ! @copy($this->file_temp, $this->upload_path.$this->file_name))
		{
			if ( ! @move_uploaded_file($this->file_temp, $this->upload_path.$this->file_name))
			{
				$this->set_error('upload_destination_error');
				return FALSE;
			}
		}
		return TRUE;
    }
	public function set_allowed_types($types)
	{
		if ( ! is_array($types) && $types == '*')
		{
			$this->allowed_types = '*';
			return;
		}
		$this->allowed_types = explode('|', $types);
	}
	public function is_allowed_filetype()
	{
		if(!isset($this->allowed_types))
		{
			return TRUE;
		}
		if ($this->allowed_types == '*')
		{
			return TRUE;
		}
        /*
		if (count($this->allowed_types) == 0 OR ! is_array($this->allowed_types))
		{
			$this->set_error('upload_no_file_types');
			return FALSE;
		}*/

		$ext = strtolower(ltrim($this->file_ext, '.'));

		if ( ! in_array($ext, $this->allowed_types))
		{
			return FALSE;
		}

		// Images get some additional checks
		$image_types = array('gif', 'jpg', 'jpeg', 'png', 'jpe');

		if (in_array($ext, $image_types))
		{
			if (getimagesize($this->file_temp) === FALSE)
			{
				return FALSE;
			}
		}
		return TRUE;
	}
	public function is_allowed_filesize()
	{
		if ($this->max_size != 0  AND  $this->file_size > $this->max_size)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
    public function set_error($msg)
    {
    	$this->error_msg[] = $msg;
    }
	public function get_extension($filename)
	{
		$x = explode('.', $filename);
		return '.'.end($x);
	}
	protected function _prep_filename($filename)
	{
		return preg_replace("/\s+/", "_", $filename);//文件名中的空格将被替换为下划线
	}
	protected function _file_mime_type($file)
	{
		// Use if the Fileinfo extension, if available (only versions above 5.3 support the FILEINFO_MIME_TYPE flag)
		if ( (float) substr(phpversion(), 0, 3) >= 5.3 && function_exists('finfo_file'))
		{
			$finfo = new finfo(FILEINFO_MIME_TYPE);
			if ($finfo !== FALSE) // This is possible, if there is no magic MIME database file found on the system
			{
				$file_type = $finfo->file($file['tmp_name']);

				/* According to the comments section of the PHP manual page,
				 * it is possible that this function returns an empty string
				 * for some files (e.g. if they don't exist in the magic MIME database)
				 */
				if (strlen($file_type) > 1)
				{
					$this->file_type = $file_type;
					return;
				}
			}
		}

		// Fall back to the deprecated mime_content_type(), if available
		if (function_exists('mime_content_type'))
		{
			$this->file_type = @mime_content_type($file['tmp_name']);
			return;
		}

		/* This is an ugly hack, but UNIX-type systems provide a native way to detect the file type,
		 * which is still more secure than depending on the value of $_FILES[$field]['type'].
		 *
		 * Notes:
		 *	- a 'W' in the substr() expression bellow, would mean that we're using Windows
		 *	- many system admins would disable the exec() function due to security concerns, hence the function_exists() check
		 */
		if (DIRECTORY_SEPARATOR !== '\\' && function_exists('exec'))
		{
			$output = array();
			@exec('file --brief --mime-type ' . escapeshellarg($file['tmp_path']), $output, $return_code);
			if ($return_code === 0 && strlen($output[0]) > 0) // A return status code != 0 would mean failed execution
			{
				$this->file_type = rtrim($output[0]);
				return;
			}
		}

		$this->file_type = $file['type'];
	}
}
?>