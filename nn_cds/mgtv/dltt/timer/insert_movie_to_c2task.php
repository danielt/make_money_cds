<?php

/**
 * 
 * 将福建移动成功注入的片源写入c2_tak
 */


error_reporting(E_ALL);
ini_set('display_errors',1);
set_time_limit(0);  
include_once dirname(dirname(dirname(__FILE__))).'/mgtv_init.php';
include_once dirname(dirname(__FILE__)).'/init.php';

i_echo('------start movie:------');
$sql = "select * from nns_mgtvbk_clip_task where nns_org_id='dltt' and nns_state='c2_ok' order by nns_create_time asc  limit 0,20000";
$db = nl_get_db(NL_DB_READ);
$db->open();
$db_w = nl_get_db(NL_DB_WRITE);
$db_w->open();
$vod_arr = nl_db_get_all($sql,$db);
$i = 0;
$j = 0;
foreach ($vod_arr as $key => $value) {
	$xml = htmlspecialchars_decode ($value['nns_content'], ENT_QUOTES);
	$dom = new DOMDocument('1.0', 'utf-8');
	$dom -> loadXML($xml);
	$xpath = new DOMXPath($dom);	
	$nns_vod_id = $xpath->query('/task/video')->item(0)->getAttribute('id');
	$nns_vod_index_id = $xpath->query('/task/video/index_list/index')->item(0)->getAttribute('id');
	$nns_media_ids = array();
	$entries = $xpath->query('/task/video/index_list/index/media_list/media');
	foreach($entries as $item){
		$nns_media_ids []= "'".$item->getAttribute('id')."'";
	}
	$nns_media_ids_str = implode(',', $nns_media_ids);	
	$sql_media  = "select * from nns_vod_media where nns_deleted!=1 and nns_vod_id='$nns_vod_id' and nns_vod_index_id='$nns_vod_index_id' and nns_id in ($nns_media_ids_str) order by  nns_kbps desc limit 1";	
	$media_arr = nl_db_get_one($sql_media, $db);	
	if(is_array($media_arr)&&$media_arr!=null){		
		//check c2 task		
		$c2_sql = "select count(*) from nns_mgtvbk_c2_task where nns_org_id='dltt' and nns_type='media' and nns_ref_id='{$media_arr['nns_id']}' and nns_src_id='{$nns_vod_index_id}'";		
		$count = nl_db_get_col($c2_sql, $db);
		if($count==0){
			$sql_index = "select * from nns_vod_index where nns_id='$nns_vod_index_id'";
			$vod_index_arr = nl_db_get_one($sql_index, $db);
			if(is_array($vod_index_arr)&&$vod_index_arr!=null){				
				$c2_id = np_guid_rand();
				$data = array(
					'nns_id'=>$c2_id,
					'nns_name'=>$vod_index_arr['nns_name'],
					'nns_type'=>'media',
					'nns_ref_id'=>$media_arr['nns_id'],
					'nns_action'=>'add',
					'nns_status'=>0,
					'nns_create_time'=>date('Y-m-d H:i:s'),
					'nns_org_id'=>'dltt',
					'nns_src_id'=>$nns_vod_index_id,		
				);
				$re = nl_db_insert($db_w, 'nns_mgtvbk_c2_task', $data);
				if($re){
					$i++;
				}else{
					$j++;
				}				
			}
		}				
	}	
}
echo "OK:".$i."\r\n";
echo "FAIL:".$j."\r\n";

?>
