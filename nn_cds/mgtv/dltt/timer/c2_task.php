<?php
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/nl_common.func.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_cms_db/nns_common/nns_db_guid_class.php';
include_once dirname(dirname(dirname(__FILE__))).'/models/category_model.php';
include_once dirname(dirname(__FILE__)).'/utils.func.php'; 
include_once dirname(dirname(__FILE__)).'/define.php';
include_once dirname(dirname(__FILE__)).'/models/c2_task_model.php';
class c2_task{
	//
	static public function run($type) {		
		self::$type();
	}
	static public function vod($new_id_c2=null){
		$dc = nl_get_dc(array(
			'db_policy' => NL_DB_WRITE,
			'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
		));
		$dc->open();
		if($new_id_c2){
			$sql  = "select * from nns_mgtvbk_c2_task where nns_id='$new_id_c2'";
		}else{
			$sql  = "select * from nns_mgtvbk_c2_task where nns_status > 0 limit 10";
		}		
		$result = nl_db_get_all($sql,$dc->db());unset($sql);
		foreach($result as $val){
			$category_id = $val['nns_category_id'];
			$all_index = $val['nns_all_index'];
			//推送vod基本信息(添加或则修改)
			if($val['nns_action']=='destroy'){
				//删除分片或则影片
				if($val['nns_type']=='vod'){
					$result_index =  nl_db_get_all("select * from nns_mgtvbk_c2_task where nns_src_id='{$val['nns_ref_id']}'",$dc->db());
						foreach($result_index as $val_index){
							$result_index = c2_task_model::delete_vod(array('nns_id'=>$val_index['nns_ref_id'],'nns_task_id'=>$val['nns_id']));//删除影片
							if($result_index){
								nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0 where nns_id='{$val_index['nns_id']}'", $dc -> db());
							}
						}
					if($all_index>1){					
						$result_vod = c2_task_model::delete_series(array('nns_id'=>$val['nns_ref_id'],'nns_task_id'=>$val['nns_id']));//删除影片
					}else{
						$result_vod = c2_task_model::delete_vod(array('nns_id'=>$val['nns_ref_id'],'nns_task_id'=>$val['nns_id']));//删除影片
					}
					if($result_vod){
						nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0 where nns_id='{$val['nns_id']}'", $dc -> db());
					}
				}elseif($val['nns_type']=='index'){
					$result_vod = c2_task_model::delete_vod(array('nns_id'=>$val['nns_ref_id'],'nns_task_id'=>$val['nns_id']));//删除影片
					if($result_vod){
						nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0 where nns_id='{$val['nns_id']}'", $dc -> db());
					}
				}
			}else{
				$result_vod = self::__push_vod_info($dc,$val['nns_ref_id'],$category_id,$val['nns_action'],$val['nns_type'],$val['nns_src_id'],$val['nns_id'],$val['nns_org_id']);
			}
			return $result_vod;
		}
	}
	static private function __push_vod_info($dc,$nns_id,$category_id,$action,$type,$src_id,$c2_task_id,$org_id){
		if($type=='vod'){	
			$sql = "select * from nns_vod where nns_id='$nns_id'";
		}elseif($type=='index'){
			$sql = "select * from nns_vod where nns_id='$src_id'";
		}
		$result = nl_db_get_one($sql,$dc->db());unset($sql);
		for ($i=0; $i <=31 ; $i++) { 
			unset($result["nns_tag$i"]);
		}
		return self::__push_vod($dc,$result,$category_id,$nns_id,$action,$c2_task_id,$org_id,$type,$src_id);
	}
	static private function __push_vod($dc,$result,$category_id,$nns_id,$action,$c2_task_id,$org_id,$type,$src_id){
		$result_vod = $result;
		$new_category_id = nl_db_get_col("select nns_sp_category_id from nns_mgtvbk_category where nns_org_id='$org_id' and nns_id='$category_id'",$dc->db());
		$is_movie = true;
		if($result['nns_all_index']>1&&$type=='index'){//如果是连续剧并且是分集标识的先检查连续剧基本信息有没有注入				
			$is_movie = false;
			$c2_index_sql = "select * from nns_mgtvbk_c2_task where nns_type = 'vod' and nns_org_id = '$org_id' and nns_category_id='$category_id' and  nns_ref_id='$src_id'";
			$result_index_task = nl_db_get_one($c2_index_sql,$dc->db());unset($c2_index_sql);
			if($result_index_task['nns_status']>0){//如果基本信息没有注入则先注入基本信息
				$result['nns_category_id'] = $new_category_id;
				$result['nns_series_flag'] = 1;
				$result['nns_task_id'] = $result_index_task['nns_id'];
				if($action=='add'){
					$result_vod_rs =  c2_task_model::add_series($result);
				}elseif($action=='modify'){					
					$result_vod_rs =  c2_task_model::update_series($result);
				}					
				if($result_vod_rs){
					nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0 where nns_id='{$result_index_task['nns_id']}'", $dc -> db());
				}
			}
		}elseif($result['nns_all_index']==1&&$type=='index'){//如果分集等于1并且是index，则当影片处理
			$is_movie = true;
		}elseif($result['nns_all_index']>1&&$type=='vod'){//如果分集>1并且是vod，则当连续剧
			$is_movie = false;
			$c2_index_sql = "select * from nns_mgtvbk_c2_task where nns_type = 'vod' and nns_org_id = '$org_id' and nns_category_id='$category_id' and  nns_ref_id='$nns_id'";
			$result_index_task = nl_db_get_one($c2_index_sql,$dc->db());unset($c2_index_sql);
			if($result_index_task['nns_status']>0){//如果基本信息没有注入则先注入基本信息
				$result['nns_category_id'] = $new_category_id;
				$result['nns_series_flag'] = 1;
				$result['nns_task_id'] = $result_index_task['nns_id'];
				if($action=='add'){
					$result_vod_rs =  c2_task_model::add_series($result);
				}elseif($action=='modify'){					
					$result_vod_rs =  c2_task_model::update_series($result);
				}
				if($result_vod_rs){
					nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0 where nns_id='{$result_index_task['nns_id']}'", $dc -> db());
				}
			}
		}elseif($result['nns_all_index']==1&&$type=='vod'){//如果是电影并且分集只有1，则是电影
			$is_movie = true;
		}	
		
		//初始化基本信息
		$result_vod['nns_category_id'] = $new_category_id;
		$nns_alias_name = $result_vod['nns_alias_name'];
		$nns_area = $result_vod['nns_area'];
		$nns_language = $result_vod['nns_language'];
		$nns_show_time = $result_vod['nns_show_time'];
		$nns_kind = $result_vod['nns_kind'];
		$nns_keyword = $result_vod['nns_keyword'];
		//print_r($result_vod);
		//die;
		//插入分机或则影片的信息
		if($type=='vod'){
			if($is_movie){
				//查询分集的id作为影片的新ID
				$sql = "select * from nns_vod_index where  nns_vod_id='$nns_id'";
				$result_num = nl_db_get_one($sql,$dc->db());unset($sql);
				if($result_num!=null&&is_array($result_num)){
					$result_vod['nns_series_flag'] = 0;
					$result_vod['nns_task_id'] = $c2_task_id;
					$result_vod['nns_id'] = $result_num['nns_id'];
					$index_sql = "select * from nns_mgtvbk_c2_task where nns_type = 'index' and nns_org_id = '$org_id' and nns_category_id='$category_id' and  nns_ref_id='{$result_vod['nns_id']}' and nns_src_id='$nns_id'";
					$index = nl_db_get_one($index_sql,$dc->db());unset($index_sql);
					$result_vod['nns_task_id'] = $index['nns_id'];
					if($action=='add'){
						$rs =  c2_task_model::add_vod($result_vod);
					}elseif($action=='modify'){					
						$rs =  c2_task_model::update_vod($result_vod);
					}
					if($rs){
						nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0 where nns_id='{$c2_task_id}'", $dc -> db());
						nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0 where nns_id='{$index['nns_id']}'", $dc -> db());
					}
				}
			}
		}elseif($type=='index'){
			$sql = "select * from nns_vod_index where  nns_id='$nns_id'";
			$vod = nl_db_get_one($sql,$dc->db());unset($sql);
			$index_sql = "select * from nns_mgtvbk_c2_task where nns_type = 'vod' and nns_org_id = '$org_id' and nns_category_id='$category_id' and  nns_ref_id='{$vod['nns_vod_id']}'";
			$index = nl_db_get_one($index_sql,$dc->db());unset($index_sql);	
			if($is_movie){//电影
				$result_vod['nns_series_flag'] = 0;
			}else{//连续剧
				$result_vod['nns_series_flag'] = 1;
				$result_vod['nns_vod_id'] = $vod['nns_vod_id'];
			}
			$result_vod['nns_index'] = 	$vod['nns_index'];
			$vod_id = $vod['nns_vod_id'];
			$result_vod['nns_task_id'] = $c2_task_id;
			$result_vod['nns_id'] = $nns_id;
			if($action=='add'){
				$rs =  c2_task_model::add_vod($result_vod);
			}elseif($action=='modify'){					
				$rs =  c2_task_model::update_vod($result_vod);
			}
			if($rs){
				nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0 where nns_id='{$c2_task_id}'", $dc -> db());
				if($result_vod['nns_series_flag']==0){
					nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0 where nns_id='{$index['nns_id']}'", $dc -> db());
				}
			}
		}
		return true;
	}
	//点播
	static public function copy_vod(){
		$dc = nl_get_dc(array(
			'db_policy' => NL_DB_WRITE,
			'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
		));
		$dc->open();
		$sql="select * from nns_vod limit 100";
		$result = nl_db_get_all($sql,$dc->db());unset($sql);
		foreach ($result as $value) {
			$id = $value['nns_id'];
			$type = 'vod';
			category_model::category_content($id, $type);
			$sql_index = "select * from nns_vod_index where nns_vod_id='$id'";
			$result_index = nl_db_get_all($sql_index,$dc->db());unset($sql_index);
			foreach ($result_index as $value_index) {
				$id = $value_index['nns_id'];
				$type = 'index';
				category_model::category_content($id, $type);
			}
		}
		
		//self::vod();
	}
	//直播
	static public function copy_live(){
		$dc = nl_get_dc(array(
			'db_policy' => NL_DB_WRITE,
			'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
		));
		$dc->open();
		$sql = "select * from nns_live where nns_deleted = 0";
		$result_live = nl_db_get_all($sql,$dc->db());unset($sql);
		foreach ($result_live as  $value) {			
			$spid = SP_ORG_ID;
			$sql_validate = "select * from nns_mgtvbk_c2_task where nns_type='live' and nns_ref_id='{$value['nns_id']}' and nns_org_id='$spid'";
			$result_validate = nl_db_get_one($sql_validate,$dc->db());unset($sql_validate);
			if(is_array($result_validate)){
				nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=2,nns_action='modify' where nns_id='{$result_validate['nns_id']}'", $dc -> db());
			}else{
				$now_time = date('Y-m-d H:i:s');
				$guid = new Guid();
				$id = $guid -> toString();
				
				$data = array(
					'nns_id' => $id, 
					'nns_type' => 'live',
				 	'nns_ref_id' => $value['nns_id'], 
				 	'nns_action' => 'add', 
				 	'nns_status' => 1, 
				 	'nns_create_time' => $now_time, 
				 	'nns_modify_time' => $now_time, 
				 	'nns_org_id' => SP_ORG_ID, 
				 	'nns_category_id' => "",
				 	'nns_src_id'=>"",
				);
				nl_db_insert($dc -> db(), 'nns_mgtvbk_c2_task', $data);
			}
		}
	}

	static public function copy_playbill(){
		$dc = nl_get_dc(array(
			'db_policy' => NL_DB_WRITE,
			'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
		));
		$dc->open();
		$sql = "select * from nns_live_playbill_item";
		$playbill = nl_db_get_all($sql,$dc->db());unset($sql);
		foreach ($playbill as  $value) {
			//节目单只做增加，不做修改	
			$sql = "select count(*) from nns_mgtvbk_c2_task 
			where nns_org_id='".SP_ORG_ID."' and nns_src_id='{$value['nns_live_id']}' and nns_ref_id='{$value['nns_id']}' and nns_type='playbill'";
			$num = nl_db_get_col($sql,$dc->db());unset($sql);
			if($num>0){
				continue;
			}
			$guid = new Guid();
			$id = $guid -> toString();
			$now_time = date('Y-m-d H:i:s');
			$data = array(
					'nns_id' => $id, 
					'nns_type' => 'playbill',
				 	'nns_ref_id' => $value['nns_id'], 
				 	'nns_action' => 'add', 
				 	'nns_status' => 1, 
				 	'nns_create_time' => $now_time, 
				 	'nns_modify_time' => $now_time, 
				 	'nns_org_id' => SP_ORG_ID, 
				 	'nns_category_id' => "",
				 	'nns_src_id'=>$value['nns_live_id'],
				);
				nl_db_insert($dc -> db(), 'nns_mgtvbk_c2_task', $data);
		}
	}
}


