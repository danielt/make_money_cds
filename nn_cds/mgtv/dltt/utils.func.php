<?php
function i_date(){
	//
}
function i_write_log($str,$subpath=null){
	//
	$log_path = dirname(dirname(dirname(__FILE__))).'/data/log/mgtv/dltt';
	if($subpath != null){
		$log_path .= '/'.$subpath;
	}
	if(!is_dir($log_path)){
		mkdir($log_path,0777,true);
	}
	$log_file = $log_path.'/'.date('Y-m-d').'.txt';
	$msg = '['.date('H:i:s').']    '.$str ."\n";
	error_log($msg,3,$log_file);
}
function i_echo($msg=''){
    echo date('Y-m-d H:i:s')."   ".$msg."\n";
}
function i_get_ip(){
    if (isset($_SERVER))
    {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);

            /* 取X-Forwarded-For中第一个非unknown的有效IP字符串 */
            foreach ($arr AS $ip)
            {
                $ip = trim($ip);

                if ($ip != 'unknown')
                {
                    $realip = $ip;

                    break;
                }
            }
        }
        elseif (isset($_SERVER['HTTP_CLIENT_IP']))
        {
            $realip = $_SERVER['HTTP_CLIENT_IP'];
        }
        else
        {
            if (isset($_SERVER['REMOTE_ADDR']))
            {
                $realip = $_SERVER['REMOTE_ADDR'];
            }
            else
            {
                $realip = '0.0.0.0';
            }
        }
    }
    else
    {
        if (getenv('HTTP_X_FORWARDED_FOR'))
        {
            $realip = getenv('HTTP_X_FORWARDED_FOR');
        }
        elseif (getenv('HTTP_CLIENT_IP'))
        {
            $realip = getenv('HTTP_CLIENT_IP');
        }
        else
        {
            $realip = getenv('REMOTE_ADDR');
        }
    }

    preg_match("/[\d\.]{7,15}/", $realip, $onlineip);
    $realip = !empty($onlineip[0]) ? $onlineip[0] : '0.0.0.0';

    return $realip;	
}
