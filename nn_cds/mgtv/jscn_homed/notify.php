<?php
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('display_errors', 0);
set_time_limit(0);
$flag = true;
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/np/np_xml2array.class.php';
$ip = i_get_ip();
define('ORG_ID', 'jscn_homed');
$postdata = file_get_contents("php://input");
if(strlen($postdata) <1)
{
	nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '收到来自CDN的消息为空,IP:' . $ip . ';收到来自CDN的内容:' . $postdata, ORG_ID);
	exit();
}

$file_init_dir = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/'.ORG_ID.'/init.php';
$file_init_define = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/'.ORG_ID.'/init.php';
if(!file_exists($file_init_define) || !file_exists($file_init_dir))
{
	nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '初始化文件不存在:[' . $file_init_dir . ']['.$file_init_define.']', ORG_ID);
	exit();
}
include_once $file_init_dir;
include_once $file_init_define;
$file_wsdl_file_name = 'ContentDeployResult.wsdl';
if(!file_exists(dirname(__FILE__).'/'.$file_wsdl_file_name))
{
	nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '初始化WSDL文件不存在:[' . dirname(__FILE__).'/'.$file_wsdl_file_name . ']', ORG_ID);
	exit();
}
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------BK到CDN消息反馈开始--------------', ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '执行文件:' . __FILE__, ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '加载WSDL文件:' . dirname(__FILE__).'/'.$file_wsdl_file_name, ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '收到来自CDN的消息,IP:' . $ip . ';收到来自CDN的内容:' . $postdata, ORG_ID);
class notify_outer
{
	public function IngestNotify($LSPID, $AMSID, $Sequence,$AssetId,$PlayId)
	{
		$arg_list = func_get_args();
		$c2_notify = array (
				'nns_id' => $arg_list[0]->Sequence, 
				'nns_notify_result_url' => '', 
				'nns_notify_result' => $arg_list[0]->ResultCode,
		        'nns_notify_PlayId' => $arg_list[0]->PlayId
		);
		c2_task_model::save_c2_notify($c2_notify);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '方法参数：' . var_export($arg_list, true), ORG_ID);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '修改C2表参数：' . var_export($c2_notify, true), ORG_ID);
		return array (
				'LSPID' => $arg_list[0]->LSPID, 
				'AMSID' => $arg_list[0]->AMSID,
		        'Sequence' => $arg_list[0]->Sequence,
		);
	}
}
if($flag)
{
    $dom = new DOMDocument('1.0', 'utf-8');
    $dom->loadXML($postdata);
    $xml = $dom->saveXML();
    $xml_arr = np_xml2array::getArrayData($xml);
    nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '解析后的数组：' . var_export($xml_arr, true), ORG_ID);
    if($xml_arr['ret'] !=1)
    {
        nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '解析xml：' . var_export($xml_arr, true), ORG_ID);
    }
    $xml_arr_1 = (isset($xml_arr['data']['children'][0]['children'][0]['children'])) ? $xml_arr['data']['children'][0]['children'][0]['children'] : null;
    if(!is_array($xml_arr_1) || empty($xml_arr_1))
    {
        nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '解析xml：' . var_export($xml_arr, true), ORG_ID);
        return ;
    }
    $data = array();
    foreach ($xml_arr_1 as $val)
    {
        if(!isset($val['name']) || !isset($val['content']))
        {
            continue;
        }
        $data[$val['name']] = $val['content'];
    }
    $c2_notify = array (
			'nns_id' => $data['Sequence'], 
			'nns_notify_result_url' => '', 
			'nns_notify_result' => isset($data['ResultCode']) ? $data['ResultCode'] : 0,
	        'nns_notify_PlayId' => $data['PlayId'],
            'nns_notify_content' =>  $xml,
	);
	c2_task_model::save_c2_notify($c2_notify);
	nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '方法参数：' . var_export($xml_arr, true), ORG_ID);
	nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '修改C2表参数：' . var_export($c2_notify, true), ORG_ID);
	ob_clean();
	$str_xml = '<?xml version="1.0" encoding="UTF-8"?>';
	$str_xml.= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
	$str_xml.=     '<soapenv:Body>';
	$str_xml.=         '<IngestNotifyResponse xmlns="http://IngestNotifyService.homed.ipanel.cn/">';
	$str_xml.=             '<LSPID xmlns="">STARCOR</LSPID>';
	$str_xml.=             '<AMSID xmlns="">Homed</AMSID>';
	$str_xml.=             '<Sequence xmlns="">'.$data['Sequence'].'</Sequence>';
	$str_xml.=         '</IngestNotifyResponse>';
	$str_xml.=     '</soapenv:Body>';
	$str_xml.= '</soapenv:Envelope>';
	echo $str_xml;die;
}
else
{
    $server = new SOAPServer($file_wsdl_file_name);
    $server->setClass('notify_outer');
    $server->addFunction( "IngestNotify" ); 
    $server->handle();
}