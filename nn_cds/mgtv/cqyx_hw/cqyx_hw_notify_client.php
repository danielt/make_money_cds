<?php
ini_set('display_errors',1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
define('ORG_ID', 'cqyx');
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/' . ORG_ID . '/init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/' . ORG_ID . '/define.php';
define('CSPID','mgtv');
define('LSPID','mgtv');
class cqyx_notify_client{
	const WSDL = 'http://127.0.0.1/BK_2.4.X/nn_mgtvbk/mgtv/cqyx_hw/cqyx_hw_notify.php?wsdl';
	static public function ResultNotify($CorrelateID,$CmdResult,$ResultFileURL){
		$arg_list = func_get_args();
		i_write_log_core('ExecCmd request args:'.var_export(array(CSPID,LSPID,$CorrelateID,$CmdResult,$ResultFileURL),true),ORG_ID.'/c2');
		$client = self::get_soap_client();
		$ret = $client->ResultNotify(CSPID,LSPID,$CorrelateID,$CmdResult,$ResultFileURL);
		i_write_log_core('ExecCmd return:'.var_export($ret,true),ORG_ID.'/c2');
		i_write_log_core('ExecCmd return:'.json_encode($ret),ORG_ID.'/c2json');
//		print_r(json_encode($ret));
		return $ret;
	}
	public static function get_soap_client(){
		return new SOAPClient(self::WSDL,array('trace'=>true));
	}
}
