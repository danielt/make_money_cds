<?php
ini_set('display_errors',1);
set_time_limit(0);
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__)))."/v2/common.php";
\ns_core\m_load::load("ns_core.m_config");
$arr_dir = explode('|', str_replace(array ('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/'.ORG_ID.'/init.php';
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/'.ORG_ID.'/define.php';
define('CMSID','souhu');
define('SOPID','CENTER');
ini_set("soap.wsdl_cache_enabled", "0");
class client
{
	public static  $wsdl = null;
	//soap 请求配置
	public static $soap_config = array ('trace' => true);
	
	static public function ExecCmd($CorrelateID,$ContentMngXMLURL,$bk_web_url,$video_type=VIDEO_TYPE_VIDEO)
	{
		$arg_list = func_get_args();
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '--------------BK到CDN消息发送开始--------------', ORG_ID, $video_type);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '执行文件:' . __FILE__ , ORG_ID, $video_type);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '执行方法:' . __FUNCTION__, ORG_ID, $video_type);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '客户端请求传入参数:' . var_export($arg_list, true).';CMSID:'.CMSID.';SOPID:'.SOPID, ORG_ID, $video_type);
		
		$xml_post_string = '<?xml version="1.0" encoding="utf-8"?>';
		$xml_post_string.= '<soapenv:Envelope xmlns:iptv="iptv" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">';
		$xml_post_string.=    '<soapenv:Header/>';
		$xml_post_string.=    '<soapenv:Body>';
		$xml_post_string.=        '<iptv:ExecCmd soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">';
		$xml_post_string.=            '<CSPID xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">'.CMSID.'</CSPID>';
		$xml_post_string.=            '<LSPID xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">'.SOPID.'</LSPID>';
		$xml_post_string.=            '<CorrelateID xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">'.$CorrelateID.'</CorrelateID>';
		$xml_post_string.=            '<CmdFileURL xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">'.$ContentMngXMLURL.'</CmdFileURL>';
		$xml_post_string.=        '</iptv:ExecCmd>';
		$xml_post_string.=    '</soapenv:Body>';
		$xml_post_string.= '</soapenv:Envelope>';
		
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '内容:' . var_export($xml_post_string, true), ORG_ID, $video_type);
// 		$client = self::get_soap_client($bk_web_url,$video_type);
// 		$ret = $client->__doRequest($xml_post_string,$bk_web_url,'ExecCmd',1);
// 		unset($client);
// 		if(empty($ret))
// 		{
// 		    nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '---------重试机制开启-----', ORG_ID, $video_type);
// 		    $client = self::get_soap_client($bk_web_url,$video_type);
// 		    $ret = $client->__doRequest($xml_post_string,$bk_web_url,'ExecCmd',1,0);
// 		}
		$client = self::get_soap_client($bk_web_url,$video_type);
		$ret = $client->ExecCmd(CMSID,SOPID,$CorrelateID,$ContentMngXMLURL);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '客户端输出结果:' . var_export($ret, true), ORG_ID, $video_type);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '--------------BK到CDN消息发送结束--------------', ORG_ID, $video_type);
		return $ret;
	}
	public static function get_soap_client($bk_web_url,$video_type)
	{
	    global $g_bk_web_url;
	    $bk_web_url = $g_bk_web_url;
	    unset($g_bk_web_url);
	    $bk_web_url = rtrim(rtrim($bk_web_url,'/'),'\\');
		self::$wsdl = $bk_web_url.'/mgtv/'.ORG_ID.'/service.php?wsdl';
//         self::$wsdl = $bk_web_url;
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '客户端请求wsdl地址:' . self::$wsdl . ',wsdl配置参数:' . var_export(self::$soap_config,true), ORG_ID, $video_type);
		return new SOAPClient(self::$wsdl, self::$soap_config);
	}
}