<?php
set_time_limit(0);
ini_set('display_errors', 0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
$ip = i_get_ip();
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
$postdata = file_get_contents("php://input");
if(strlen($postdata) <1)
{
	nn_log::write_log_message(LOG_MODEL_BK_CDN_DELIVERY_NOTIFY, '收到来自CDN的消息为空,IP:' . $ip . ';收到来自CDN的内容:' . $postdata, ORG_ID);
}
$postdata = json_decode($postdata,true);
$request_data = $_REQUEST;
$file_init_dir = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/'.ORG_ID.'/init.php';
if(!file_exists($file_init_dir))
{
	nn_log::write_log_message(LOG_MODEL_BK_CDN_DELIVERY_NOTIFY, '初始化文件不存在:[' . $file_init_dir . ']', ORG_ID);
	exit();
}
include_once $file_init_dir;
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/c2_task/c2_task.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/c2_log/c2_log.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/sp/sp.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/vod_media/vod_media.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/mgtv_v2/models/queue_task_model.php';
nn_log::write_log_message(LOG_MODEL_BK_CDN_DELIVERY_NOTIFY, '--------------BK到CDN消息反馈开始--------------', ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_DELIVERY_NOTIFY, '执行文件:' . __FILE__, ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_DELIVERY_NOTIFY, '收到来自CDN的消息,IP:' . $ip . ';收到来自CDN的内容input:' . var_export($postdata,true), ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_DELIVERY_NOTIFY, '收到来自CDN的消息,IP:' . $ip . ';收到来自CDN的内容REQUEST:' . var_export($request_data,true), ORG_ID);
class playurl
{
    /**
     * 传入的参数
     * @var unknown
     */
    private $mix_params = null;
    
    public $obj_dc = null;
    
    /**
     * 初始化数据
     * @param string $params
     */
    public function __construct($params=null)
    {
        $this->mix_params = $params;
        $this->obj_dc = nl_get_dc(array (
            "db_policy" => NL_DB_WRITE,
            "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
        ));
    }
    
    /**
     * 消息反馈执行的方法
     */
	public function notify()
	{
        nn_log::write_log_message(LOG_MODEL_BK_CDN_DELIVERY_NOTIFY, __FUNCTION__ ."传入参数为：".var_export($_REQUEST,true), ORG_ID);
		$media_id = (isset($this->mix_params['mediaId'])) ? $this->mix_params['mediaId'] : '';
		$str_playurl = (isset($this->mix_params['srcFiles'][0]['playUrl'])) ? $this->mix_params['srcFiles'][0]['playUrl'] : '';
		if(isset($media_id) && strlen($media_id) >0 && isset($str_playurl) && strlen($str_playurl) >0)
		{
		    $result_query_c2_task = nl_c2_task::query_by_condition($this->obj_dc, array('nns_org_id'=>ORG_ID,'nns_type'=>'media','nns_ref_id'=>$media_id));
		    if($result_query_c2_task['ret'] !=0)
		    {
		        nn_log::write_log_message(LOG_MODEL_BK_CDN_DELIVERY_NOTIFY, __FUNCTION__ . '查询C2表无数据：' . var_export($result_query_c2_task, true), ORG_ID);
		        return $result_query_c2_task;
		    }
		    if(!isset($result_query_c2_task['data_info'][0]) || empty($result_query_c2_task['data_info'][0]) || !is_array($result_query_c2_task['data_info'][0]))
		    {
		        $result_query_c2_task['ret'] = 1;
		        nn_log::write_log_message(LOG_MODEL_BK_CDN_DELIVERY_NOTIFY, __FUNCTION__ . '查询C2表无数据1：' . var_export($result_query_c2_task, true), ORG_ID);
		        return $result_query_c2_task;
		    }
		    $arr_playurl = array(
		        'play_url'=>$str_playurl,
		    );
		    $str_playurl = json_encode($arr_playurl);
		    $sp_config = nl_sp::query_by_id($this->obj_dc,ORG_ID);
		    $sp_config = (isset($sp_config['data_info'][0]['nns_config']) && strlen($sp_config['data_info'][0]['nns_config'])) ? json_decode($sp_config['data_info'][0]['nns_config'],true) : null;
		    $result = nl_vod_media_v2::edit($this->obj_dc,array('nns_ext_url'=>$str_playurl,'nns_media_service'=>$sp_config['media_service_type']), $media_id);
		    if($result['ret'] !=0)
		    {
		        nn_log::write_log_message(LOG_MODEL_BK_CDN_DELIVERY_NOTIFY, __FUNCTION__ . '修改片源数据：' . var_export($result, true), ORG_ID);
		        return $result;
		    }
		    $result_edit_c2_task = nl_c2_task::edit($this->obj_dc, array('nns_status'=>0,), $result_query_c2_task['data_info'][0]['nns_id']);
		    nn_log::write_log_message(LOG_MODEL_BK_CDN_DELIVERY_NOTIFY, __FUNCTION__ . '修改C2_task表参数：' . var_export($result_edit_c2_task, true), ORG_ID);
		    if($result_edit_c2_task['ret'] == '0')
		    {
		        $queue_task_model = new queue_task_model();
		        $queue_task_model->q_report_task($result_query_c2_task['data_info'][0]['nns_id']);
		        unset($queue_task_model);
		        $queue_task_model=null;
		    }
		    return $result_edit_c2_task;
		}
		nn_log::write_log_message(LOG_MODEL_BK_CDN_DELIVERY_NOTIFY, '--------------BK到CDN消息反馈结束--------------', ORG_ID);
		return $this->return_data(1,'error');
	}
    
	/**
	 * 检验字符串是否是json格式
	 * @param string $string
	 */
	private function is_json($string=null) 
	{
	    $string = strlen($string) <1 ? '' : $string;
	    json_decode($string);
	    return (json_last_error() == JSON_ERROR_NONE);
	}
	
	private function return_data($ret,$reason=null,$data_info=null)
	{
	    return array(
	        'ret'=>$ret,
	        'reason'=>($reason === null) ? '' : $reason,
	        'data_info'=>($data_info === null) ? '' : $data_info,
	    );
	}
	
}
$obj_http_notify = new playurl($postdata);
$result = $obj_http_notify->notify();
$result = $result['ret'] =='0' ? array('code'=>1) : array('code'=>0);
echo json_encode($result);