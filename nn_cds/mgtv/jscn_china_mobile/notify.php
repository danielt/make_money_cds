<?php
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('display_errors', 1);
set_time_limit(0);
$arr_dir = explode('|', str_replace(array ('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/sp/sp.class.php';
$ip = i_get_ip();
$postdata = file_get_contents("php://input");
if(strlen($postdata) <1)
{
	nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '收到来自CDN的消息为空,IP:' . $ip . ';收到来自CDN的内容:' . $postdata, 'public');
	exit();
}

$request_data = $_REQUEST;
$file_init_dir = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/' . ORG_ID . '/init.php';
$file_init_define = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/' . ORG_ID . '/define.php';
if(!file_exists($file_init_define) || !file_exists($file_init_dir))
{
	nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '初始化文件不存在:[' . $file_init_dir . ']['.$file_init_define.']', 'public');
	exit();
}
include_once $file_init_dir;
include_once $file_init_define;
$file_wsdl_file_name = 'ContentDeployResult.wsdl';
if(!file_exists(dirname(__FILE__).'/'.$file_wsdl_file_name))
{
	nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '初始化WSDL文件不存在:[' . dirname(__FILE__).'/'.$file_wsdl_file_name . ']', 'public');
	exit();
}
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------CDN到BK消息反馈开始--------------', ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '执行文件:' . __FILE__, ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '加载WSDL文件:' . dirname(__FILE__).'/'.$file_wsdl_file_name, ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '收到来自CDN的消息,IP:' . $ip . ';收到来自CDN的内容:' . $postdata, ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '收到来自CDN的消息,IP:' . $ip . ';收到来自CDN的内容REQUEST:' . var_export($request_data,true), ORG_ID);
class notify
{
	public function ContentDeployResult($CMSID, $SOPID, $CorrelateID, $ResultCode,$ErrorDescription ,$ResultFileURL)
	{
		$arg_list = func_get_args();
		$c2_notify = array (
				'nns_id' => $CorrelateID, 
				'nns_notify_result_url' => $ResultFileURL, 
				'nns_notify_result' => $ResultCode
		);
		c2_task_model::save_c2_notify_v2($c2_notify);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '方法参数：' . var_export($arg_list, true), ORG_ID);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '修改C2表参数：' . var_export($c2_notify, true), ORG_ID);
		return array (
				'ResultCode' => 0, 
				'ErrorDescription' => 'notify ok'
		);
	}
}
$server = new SOAPServer($file_wsdl_file_name);
$server->setClass('notify');
$server->handle();
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------CDN到BK消息反馈结束--------------', ORG_ID);