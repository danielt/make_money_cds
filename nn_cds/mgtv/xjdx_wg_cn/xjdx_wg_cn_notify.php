<?php
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('display_errors',1);
set_time_limit(0);
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/mgtv_init.php';
define('ORG_ID', 'xjdx_wg_cn');
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/'.ORG_ID.'/init.php';
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/'.ORG_ID.'/define.php';
//include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/models/c2_task_model.php';
$ip = i_get_ip();
i_write_log_core('notify from '.$ip,ORG_ID.'/c2notify');
$postdata = file_get_contents("php://input"); 
i_write_log_core('notify data '.$postdata,ORG_ID.'/c2notify');
class xjdx_wg_cn_notify{
	public function ResultNotify($CSPID,$LSPID,$CorrelateID,$CmdResult,$ResultFileURL){
		$arg_list = func_get_args();
		
		
		if($CmdResult!='0')
		{
			$CmdResult = '-1';
		}
		
		
		$c2_notify = array(
		'nns_id'=>$CorrelateID,
		'nns_notify_result_url'=>$ResultFileURL,
		'nns_notify_result'=>$CmdResult,
		);
		c2_task_model::save_c2_notify($c2_notify);
		i_write_log_core('ResultNotify params:'.var_export($arg_list,true),ORG_ID.'/c2notify');
		i_write_log_core('ResultNotify params:'.var_export($c2_notify,true),ORG_ID.'/c2notify');
		return array('Result'=>0,'ErrorDescription'=>'notify ok');
	}
}
//ob_start();
$server = new SOAPServer('xjdx_wg_cn_notify.wsdl');
$server->setClass('xjdx_wg_cn_notify');
$server->handle();
