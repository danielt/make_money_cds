<?php
ini_set('display_errors',1);
set_time_limit(0);
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/mgtv_init.php';
define('ORG_ID', 'hnlt');
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/'.ORG_ID.'/init.php';
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/'.ORG_ID.'/define.php';
define('CSPID','MGSP');
define('LSPID','MGSP');
class hnlt_client{
	const WSDL = 'http://127.0.0.1/nn_mgtvbk/mgtv/hnlt/hnlt_service.php?wsdl';
	static public function ExecCmd($CorrelateID,$CmdFileURL){
		$arg_list = func_get_args();
		i_write_log_core('ExecCmd request args:'.var_export($arg_list,true),ORG_ID.'/c2');
		$client = self::get_soap_client();
		$ret = $client->ExecCmd(CSPID,LSPID,$CorrelateID,$CmdFileURL);
		i_write_log_core('ExecCmd return:'.var_export($ret,true),ORG_ID.'/c2');
		return $ret;
	}
	public static function get_soap_client(){
		return new SOAPClient(self::WSDL,array('trace'=>true));
	}
}
