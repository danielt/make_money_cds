<?php
include_once dirname(__FILE__).'/define.php';

function fjyd_model_autoload($classname){
	$file = dirname(__FILE__).'/models/'.$classname.'.php';
	if(file_exists($file)){
		include_once $file;
		return true;
	}	
	return false;
}
spl_autoload_register('fjyd_model_autoload');


function i_write_log($str,$subpath=null){
	//
	$log_path = dirname(dirname(dirname(__FILE__))).'/data/log/mgtv/'.SP_ORG_ID;
	if($subpath != null){
		$log_path .= '/'.$subpath;
	}
	if(!is_dir($log_path)){
		mkdir($log_path,0777,true);
	}
	$log_file = $log_path.'/'.date('Y-m-d').'.txt';
	$msg = '['.date('H:i:s').']    '.$str ."\n";
	error_log($msg,3,$log_file);
}