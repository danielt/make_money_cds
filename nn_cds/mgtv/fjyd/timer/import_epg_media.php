<?php
header("Content-type: text/html; charset=utf-8");
error_reporting(E_ALL);
ini_set('display_errors',1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
include_once dirname(dirname(__FILE__)) . '/init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/import/import.class.php';

$db_r = nn_get_db(NL_DB_READ);
$db_w = nn_get_db(NL_DB_WRITE);
$url = IMPROT_EPG_URL;
$log = IMPROT_EPG_LOG;

$media_num = 200;

$sql = "select * from nns_mgtvbk_c2_task  where nns_org_id='fjyd' and  nns_type='media' and nns_status=0  order by nns_create_time desc  limit $media_num";
$list = nl_db_get_all($sql, $db_r);
if (is_array($list) && count($list) > 0) {
	echo "片源总数".count($list)."\r\n";
	$media_i = 0;
	$media_j = 0;
	foreach ($list as $value) {
		$action = $value['nns_action'];
		$import_obj = new import($url, $log);
		$sql = "select * from nns_vod_media where nns_id='{$value['nns_ref_id']}' and nns_deleted!=1";
		$info = nl_db_get_one($sql, $db_r);
		
		if(empty($info)){
			$sql = "update nns_mgtvbk_c2_task set nns_status=99,nns_action='destroy',nns_modify_time=now() where nns_org_id='fjyd' and nns_type='media' and nns_ref_id='{$value['nns_ref_id']}'";
			//echo $sql;
			nl_execute_by_db($sql, $db_w);
			//$media_j++;
			//$action = 'destroy';
			$result = $import_obj -> delete_video_file($value['nns_ref_id']);
			continue;
		}
		if ($action == 'destroy') {
			$result = $import_obj -> delete_video_file($value['nns_ref_id']);
			$xml="";
		} else {		
			//查询所属分集是否下发epg
			$sql = "select * from nns_mgtvbk_c2_task where nns_org_id='fjyd' and nns_type='index' and nns_ref_id='{$info['nns_vod_index_id']}' and nns_status=99 and nns_action in('add','modify')";
			$index_ok = nl_db_get_one($sql, $db_r);
			
			if(empty($index_ok)){
				continue;
			}
			
			$xml = '<media file_id="' . $info['nns_id'] . '" file_type="' . $info['nns_filetype'] . '" ';
			$xml .= 'file_path="' . $info['nns_url'] . '" file_definition="' . $info['nns_mode'] . '" ';
			$xml .= 'file_resolution="" file_size="" file_bit_rate="' . $info['nns_kbps'] . '" file_desc="" ';
			$xml .= 'original_id="'.$info['nns_import_id'].'" ';
			if ($info['nns_dimensions'] == '0') {
				$xml .= 'file_3d="0" file_3d_type=""></media>';
			} elseif ($info['nns_dimensions'] == '100001') {
				$xml .= 'file_3d="1" file_3d_type="0"></media>';
			} elseif ($info['nns_dimensions'] == '100002') {
				$xml .= 'file_3d="1" file_3d_type="1"></media>';
			}
			error_log($action."\r\n",3,$log.'/'.date('Ymd').'.txt');

			$result = $import_obj -> import_video_file($info['nns_vod_id'], $info['nns_vod_index_id'], $info['nns_id'], $xml);				
		}

		error_log("nns_vod_id=".$info['nns_vod_id']."\r\n",3,$log.'/'.date('Ymd').'.txt');
		error_log("nns_vod_index_id=".$info['nns_vod_index_id']."\r\n",3,$log.'/'.date('Ymd').'.txt');
		error_log("nns_id=".$info['nns_id']."\r\n",3,$log.'/'.date('Ymd').'.txt');
		error_log($xml."\r\n",3,$log.'/'.date('Ymd').'.txt');
		
		$re = json_decode($result, true);
		
		error_log(var_export($re,true)."\r\n",3,$log.'/'.date('Ymd').'.txt');
		error_log("+++++++++media++++++++++++++++++++++++++media+++++++++++++++++++++++++++media++++++++++++++++++++++++++++++++"."\r\n",3,$log.'/'.date('Ymd').'.txt');
		//print_r($re);
		
		$set = "";
			if($action == 'destroy'){
				$set = ",nns_status='destroy'";
			}
		if ($re['ret'] == '0') {
			
			//$data = array('nns_id' => np_guid_rand('media'), 'nns_video_id' => $info['nns_id'], 'nns_video_type' => 'media', 'nns_org_id' => 'fjyd', );
			//nl_db_insert($db_w, 'nns_mgtvbk_import_epg', $data);
			$sql ="update nns_mgtvbk_c2_task set nns_status=99,nns_modify_time=now() $set where nns_id='{$value['nns_id']}'";
			nl_execute_by_db($sql,$db_w);
			$media_i++;
		}else{
			$media_j++;
			echo var_export($re,true);
		if($re['reason']=="[update_file] db execute error , ret->8096"){
                                $sql ="update nns_mgtvbk_c2_task set nns_status=99,nns_modify_time=now() $wh where nns_id='{$value['nns_id']}'";
                                nl_execute_by_db($sql,$db_w);
                        }
		}		
		$log_data = array(
			'nns_id' => np_guid_rand('media'),
			'nns_video_id' => $info['nns_id'],
			'nns_video_type' => 'media', 
			'nns_org_id' => 'fjyd',
			'nns_create_time'=>date('Y-m-d H:i:s'),
			'nns_action'=>$action,
			'nns_status'=>$re['ret'],
			'nns_reason'=>$re['reason'],
			'nns_data'=>$xml,
			'nns_name'=>$value['nns_name'],
		);
		nl_db_insert($db_w, 'nns_mgtvbk_import_epg_log', $log_data);

	}
	echo "OK：".$media_i."\r\n";
	echo "Fail：".$media_j."\r\n";
	echo "======================================================";
}
?>
