<?php
ini_set('display_errors',1);
set_time_limit(0);
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/mgtv_init.php';
$arr_dir = explode('|', str_replace(array ('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/'.ORG_ID.'/init.php';
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/'.ORG_ID.'/define.php';
define('CMSID','STARCOR');
define('SOPID','Homed');
class client
{
	public static  $wsdl = null;
	//soap 请求配置
	public static $soap_config = array ('trace' => true,'cache_wsdl' =>WSDL_CACHE_NONE);
	
	public function ContentDeployReq($CorrelateID,$FtpPath,$AdiFileName,$bk_web_url,$video_type=VIDEO_TYPE_VIDEO)
	{
		$arg_list = func_get_args();
		$NotifyUrl = 'http://10.4.254.216/nn_ksgdbk/nn_bk/mgtv/'.ORG_ID.'/notify.php';
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '--------------BK到CDN消息发送开始--------------', ORG_ID, $video_type);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '执行文件:' . __FILE__ , ORG_ID, $video_type);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '执行方法:' . __FUNCTION__, ORG_ID, $video_type);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '客户端请求传入参数:' . var_export($arg_list, true).';CMSID:'.CMSID.';SOPID:'.SOPID, ORG_ID, $video_type);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '客户端接收消息响应地址:' . var_export($arg_list, true).';CMSID:'.CMSID.';SOPID:'.SOPID, ORG_ID, $video_type);
		
		$xml_post_string = '<?xml version="1.0" encoding="utf-8"?>';
        $xml_post_string.= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ing="http://IngestAssetService.homed.ipanel.cn">';
        $xml_post_string.=      '<soapenv:Header/>';
        $xml_post_string.=      '<soapenv:Body>';
        $xml_post_string.=          '<ing:IngestAsset>';
        $xml_post_string.=              '<LSPID>'.CMSID.'</LSPID>';
        $xml_post_string.=              '<AMSID>'.SOPID.'</AMSID>';
        $xml_post_string.=              '<Sequence>'.$CorrelateID.'</Sequence>';
        $xml_post_string.=              '<FtpPath>'.$FtpPath.'</FtpPath>';
        $xml_post_string.=              '<AdiFileName>'.$AdiFileName.'</AdiFileName>';
        $xml_post_string.=              '<NotifyUrl>'.$NotifyUrl.'</NotifyUrl>';
        $xml_post_string.=          '</ing:IngestAsset>';
        $xml_post_string.=      '</soapenv:Body>';
        $xml_post_string.= '</soapenv:Envelope>';
		$url =  "http://10.65.255.110:35006/ingest_asset_service?wsdl";
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '内容:' . var_export($xml_post_string, true), ORG_ID, $video_type);
		
		$client = self::get_soap_client($bk_web_url,$video_type);
		
		$ret = $client->__doRequest($xml_post_string,$url,'IngestAssetDeal',1,0);
// 		$ret = $client->IngestAssetDeal(
// 		    new SoapParam(CMSID, "LSPID"),
// 		    new SoapParam(SOPID, "AMSID"),
// 		    new SoapParam($CorrelateID, "Sequence"),
// 		    new SoapParam($FtpPath, "FtpPath"),
// 		    new SoapParam($AdiFileName, "AdiFileName"),
// 		    new SoapParam($NotifyUrl, "NotifyUrl")
// 		    );
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '客户端输出结果:' . var_export($ret, true), ORG_ID, $video_type);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '--------------BK到CDN消息发送结束--------------', ORG_ID, $video_type);
		return $ret;
	}
	public static function get_soap_client($bk_web_url,$video_type)
	{
		self::$wsdl = $bk_web_url.'/mgtv/'.ORG_ID.'/service.php?wsdl';
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '客户端请求wsdl地址:' . self::$wsdl . ',wsdl配置参数:' . var_export(self::$soap_config,true), ORG_ID, $video_type);
		return new SOAPClient(self::$wsdl, self::$soap_config);
	}
}