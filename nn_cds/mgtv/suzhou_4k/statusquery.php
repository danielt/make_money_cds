<?php
header('Content-Type: text/xml; charset=utf-8');
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('display_errors', 0);
set_time_limit(0);
$arr_dir = explode('|', str_replace(array ('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/sp/sp.class.php';
$ip = i_get_ip();
$obj_dc = nl_get_dc(array (
    "db_policy" => NL_DB_WRITE,
    "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));
$postdata = file_get_contents("php://input");
nn_log::write_log_message(LOG_MODEL_CDN_BK_NOTIFY, '收到来自CDN的消息为空,IP:' . $ip . ';收到来自CDN的内容:' . $postdata, ORG_ID);
nn_log::write_log_message(LOG_MODEL_CDN_BK_NOTIFY, '收到来自CDN的消息为空,IP:' . $ip . ';收到来自CDN的POST内容:' . var_export($_REQUEST,true), ORG_ID);
$objectcode = $_REQUEST['objectcode'];
$sql_index="select * from nns_vod_index where nns_cp_id='JSCN_ys4kzq' and nns_import_id='{$objectcode}' order by nns_create_time asc";
$result_index = nl_query_by_db($sql_index, $obj_dc->db());
if(!is_array($result_index) || empty($result_index))
{
    $str_xml = '<?xml version="1.0" encoding="UTF-8"?><response><errorcode>1</errorcode><errormsg>查询失败</errormsg><program id="'.$objectcode.'" name="未知分集"><program id="视达科" status="分集查询失败" /></program ></response>';
    nn_log::write_log_message(LOG_MODEL_CDN_BK_NOTIFY, '反馈的消息:' . $str_xml, ORG_ID);
    echo $str_xml;die;
}
$sql_media="select * from nns_vod_media where nns_cp_id='JSCN_ys4kzq' and nns_vod_index_id='{$result_index[0]['nns_id']}' order by nns_create_time asc";
$result_media = nl_query_by_db($sql_media, $obj_dc->db());
if(!is_array($result_media) || empty($result_media))
{
    $str_xml = '<?xml version="1.0" encoding="UTF-8"?><response><errorcode>1</errorcode><errormsg>查询失败</errormsg><program id="'.$objectcode.'" name="'.$result_index[0]['nns_name'].'"><program id="视达科" status="片源查询失败" /></program ></response>';
    nn_log::write_log_message(LOG_MODEL_CDN_BK_NOTIFY, '反馈的消息:' . $str_xml, ORG_ID);
    echo $str_xml;die;
}
$sql_c2="select * from nns_mgtvbk_c2_task where nns_org_id='".ORG_ID."' and nns_type='media' and nns_ref_id='{$result_media[0]['nns_id']}' order by nns_create_time desc";
$result_c2 = nl_query_by_db($sql_c2, $obj_dc->db());
if(!is_array($result_c2) || empty($result_c2))
{
    $str_xml = '<?xml version="1.0" encoding="UTF-8"?><response><errorcode>1</errorcode><errormsg>查询成功</errormsg><program id="'.$objectcode.'" name="'.$result_index[0]['nns_name'].'"><program id="视达科" status="队列处理中" /></program ></response>';
    nn_log::write_log_message(LOG_MODEL_CDN_BK_NOTIFY, '反馈的消息:' . $str_xml, ORG_ID);
    echo $str_xml;die;
}
if($result_c2[0]['nns_status'] == '0')
{
    $str_xml = '<?xml version="1.0" encoding="UTF-8"?><response><errorcode>1</errorcode><errormsg>查询成功</errormsg><program id="'.$objectcode.'" name="'.$result_index[0]['nns_name'].'"><program id="视达科" status="已上架" /></program ></response>';
    nn_log::write_log_message(LOG_MODEL_CDN_BK_NOTIFY, '反馈的消息:' . $str_xml, ORG_ID);
    echo $str_xml;die;
}
$str_xml = '<?xml version="1.0" encoding="UTF-8"?><response><errorcode>1</errorcode><errormsg>查询成功</errormsg><program id="'.$objectcode.'" name="'.$result_index[0]['nns_name'].'"><program id="视达科" status="处理中" /></program ></response>';
nn_log::write_log_message(LOG_MODEL_CDN_BK_NOTIFY, '反馈的消息:' . $str_xml, ORG_ID);
echo $str_xml;die;