<?php
ini_set('display_errors', 0);
set_time_limit(0);
$arr_dir = explode('|', str_replace(array ('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/c2_task/c2_task.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/models/public_model_exec.class.php';
$ip = i_get_ip();
$postdata = file_get_contents("php://input");
class increment
{
    public $obj_dc = null;
    public $arr_params = null;
    public $int_limit = 500;
    public function __construct()
    {
        $this->obj_dc = nl_get_dc(array (
                'db_policy' => NL_DB_WRITE,
                'cache_policy' => NP_KV_CACHE_TYPE_NULL
            ));
        $this->arr_params = $_REQUEST;
    }
    
    public function init()
    {
        if(!isset($this->arr_params['token']) || strlen($this->arr_params['token'])<1)
        {
            return public_model_exec::return_data(1,'params token error');
        }
        if(!isset($this->arr_params['time']) || strlen($this->arr_params['time'])<1)
        {
            $this->arr_params['time'] = date("Y-m-d H:i:s");
        }
        if(!isset($this->arr_params['type']) || strlen($this->arr_params['type'])<1 || !in_array($this->arr_params['type'], array('0','1','2')))
        {
            return public_model_exec::return_data(1,'params type error');
        }
        return public_model_exec::return_data(0,'ok');
    }
    
    public function action()
    {
        $result_init = $this->init();
        if($result_init['ret'] !=0)
        {
            return $result_init;
        }
        switch ($this->arr_params['type'])
        {
            case '0':
                $sql="select nns_ref_id as id,nns_modify_time as modifyTime from nns_mgtvbk_c2_task where nns_org_id='".ORG_ID."' and nns_type='video' and nns_modify_time<='{$this->arr_params['time']}' and nns_status='0' order by nns_modify_time desc limit {$this->int_limit}";
                break;
            case '1':
                $sql="select nns_ref_id as id,nns_modify_time as modifyTime from nns_mgtvbk_c2_task where nns_org_id='".ORG_ID."' and nns_type='index' and nns_modify_time<='{$this->arr_params['time']}' and nns_status='0' order by nns_modify_time desc limit {$this->int_limit}";
                break;
            case '2':
                $sql="select nns_ref_id as id,nns_modify_time as modifyTime from nns_mgtvbk_c2_task where nns_org_id='".ORG_ID."' and nns_type='media' and nns_modify_time<='{$this->arr_params['time']}' and nns_status='0' order by nns_modify_time desc limit {$this->int_limit}";
                break;
            default:
                return public_model_exec::return_data(1,'params type error');
        }
        $result = nl_query_by_db($sql, $this->obj_dc->db());
        if(!$result)
        {
            return public_model_exec::return_data(7,'inner error');
        }
        if(empty($result) || !is_array($result))
        {
            return public_model_exec::return_data(2,'no data');
        }
        return public_model_exec::return_data(0,'ok',$result);
    }
}
$obj_increment = new increment();
$result = $obj_increment->action();
$last_result = array(
    'code'=>'C0000'.$result['ret'],
    'data'=>$result['data_info'],
);
echo json_encode($last_result);die;

