<?php 
header("Content-type:text/html;charset=utf-8");
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/nl_common.func.php';
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/sp/sp.class.php';
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_READ,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
if(isset($_REQUEST['action']) && $_REQUEST['action']=='increment')
{
    include_once dirname(__FILE__).'/increment.php';
    die;
}
else if(isset($_REQUEST['action']) && $_REQUEST['action']=='increment_info')
{
    include_once dirname(__FILE__).'/increment_info.php';
    die;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>中心注入指令</title>
    </head>
    <body>
             拉取数据 验证脚本：<hr/><br>
        <form method="post" action="./test.php">
            <input name="action" type='hidden' value='increment'><br/>
            <br>
                    输入TOKEN：
            <input name="token" type='text' value=''><br/>
                    输入开始时间：
            <input name="begintime" type='text' value=''><br/>
                    输入结束时间：
            <input name="endtime" type='text' value=''><br/>
                    输入分页条数：
            <input name="page" type='text' value=''><br/>
            <input type="submit" value="提交"/>
        </form>
        <hr/>
              拉取详情脚本：<hr/><br>
        <form method="post" action="./test.php">
            <input name="action" type='hidden' value='increment_info'><br/>
            <br>
                    选择影片类型:
            <select name="type">
                <option value="0">主媒资</option>
                <option value="1">分集</option>
                <option value="2">片源</option>
            </select><br/><br>
                    输入TOKEN：
            <input name="token" type='text' value=''><br/>
                    输入id：
            <input name="id" type='text' value=''><br/>
            <input type="submit" value="提交"/>
        </form>
    </body>
</html>