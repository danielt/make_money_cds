<?php
ini_set('display_errors', 0);
set_time_limit(0);
$arr_dir = explode('|', str_replace(array ('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/c2_task/c2_task.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/vod_media/vod_media.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/video/vod_index.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/video/vod.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/depot/depot.class.php';
include_once dirname(__FILE__) . '/get_increment_info_xml.php';
$ip = i_get_ip();
$postdata = file_get_contents("php://input");
class increment_info
{
    public $obj_dc = null;
    public $arr_params = null;
    public $str_sp_id = null;
    public $arr_sp_config = null;
    public $arr_cp_config = null;
    public $get_increment_info_xml = null;

    public function __construct()
    {
        $this->obj_dc = nl_get_dc(array (
                'db_policy' => NL_DB_WRITE,
                'cache_policy' => NP_KV_CACHE_TYPE_NULL
            ));
        $this->arr_params = $_REQUEST;
        $this->str_sp_id = ORG_ID;
        $this->get_increment_info_xml = new get_increment_info_xml();
    }

    public function init()
    {
        if(!isset($this->arr_params['token']) || strlen($this->arr_params['token'])<1)
        {
            return public_model_exec::return_data(1,'params token error');
        }
        if(!isset($this->arr_params['type']) || strlen($this->arr_params['type'])<1 || !in_array($this->arr_params['type'], array('0','1','2')))
        {
            return public_model_exec::return_data(1,'params type error');
        }
        if(!isset($this->arr_params['id']) || strlen($this->arr_params['id'])<1)
        {
            return public_model_exec::return_data(1,'params id error');
        }
        $this->arr_sp_config = sp_model::get_sp_config($this->str_sp_id);
        return public_model_exec::return_data(0,'ok');
    }

    public function do_video()
    {
        $sql_task ="select nns_ref_id,nns_modify_time from nns_mgtvbk_c2_task where nns_org_id='".ORG_ID."' and nns_type='video' and nns_ref_id='{$this->arr_params['id']}' and nns_status='0' order by nns_modify_time desc";
        $result_c2_task = nl_query_by_db($sql_task, $this->obj_dc->db());
        if(!$result_c2_task)
        {
            return public_model_exec::return_data(1,'inner error');
        }
        if(empty($result_c2_task) || !is_array($result_c2_task))
        {
            return public_model_exec::return_data(7,'no data');
        }
        $result_c2_task = $result_c2_task[0];
        $arr_push = $this->get_increment_info_xml->make_vod_arr($this->obj_dc,$result_c2_task['nns_id'],$this->arr_params['id'],$this->arr_sp_config,$this->arr_cp_config,$this->str_sp_id);
        if(isset($arr_push['ret']) && $arr_push['ret'] != 0 )
        {
            return $arr_push;
        }
//        $sql_vod = "select * from nns_vod where nns_id ='{$result_c2_task['nns_ref_id']}'";
//        $result_video  = nl_query_by_db($sql_vod, $this->obj_dc->db());
//        if(!$result_video)
//        {
//            return public_model_exec::return_data(1,'inner error');
//        }
//        if(empty($result_video) || !is_array($result_video))
//        {
//            return public_model_exec::return_data(7,'no data');
//        }
//        $result_video = $result_video[0];
//
//        $result_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_video['nns_id'],$result_video,$this->arr_sp_config,'cdn');
//        if($result_import_info['ret'] !=0 || !isset($result_import_info['data_info']) || strlen($result_import_info['data_info']) <1)
//        {
//            return $result_import_info;
//        }
//        $result_video['nns_cdn_video_guid'] = $result_import_info['data_info'];
//        $result_category = $this->get_category_info($result_video);
//        if($result_category['ret'] !=0)
//        {
//            return $result_category;
//        }
//        $str_img_ftp = (isset($this->arr_sp_config['img_ftp']) && strlen($this->arr_sp_config['img_ftp'])>0) ? trim($this->arr_sp_config['img_ftp'],'/').'/' : '';
//        $this->_get_cp_info($result_video['nns_cp_id']);
//
//        $str_modifyTime = $result_c2_task['nns_modify_time'];
//        $arr_push = array(
//            'type'=>0,
//            'modifyTime'=>$str_modifyTime,
//            'data'=>array(
//                'cp' => 'coocaa',
//                'originalId' => $result_video['nns_cdn_video_guid'],
//                'id' => $result_video['nns_id'],
//                'name' => $result_video['nns_name'],
//                'alias' => $result_video['nns_alias_name'],
//                'sname' => $result_video['nns_pinyin'],
//                'focus' => $result_video['nns_keyword'],
//                'picV' => strlen($result_video['nns_image_v']) >0 ? $str_img_ftp.trim($result_video['nns_image_v'],'/') : '',
//                'picH' => strlen($result_video['nns_image_h']) >0 ? $str_img_ftp.trim($result_video['nns_image_h'],'/') : '',
//                'score' => $result_video['nns_point'],
//                'playCount' => $result_video['nns_play_count'],
//                'desc' => $result_video['nns_summary'],
//                'seriesType' => 0,
//                'cname' => $result_category['data_info']['category_name'],
//                'cid' => $result_video['nns_category_id'],
//                'tag' => str_replace('/', ',', $result_video['nns_kind']),
//                'persons' => array(
//                    'composer' => '',
//                    'dubber' => '',
//                    'maker' => str_replace('/', ',', $result_video['nns_screenwriter']),
//                    'producer' => '',
//                    'guest' => '',
//                    'actor' => str_replace('/', ',', $result_video['nns_actor']),
//                    'writer' => str_replace('/', ',', $result_video['nns_screenwriter']),
//                    'director' => str_replace('/', ',', $result_video['nns_director']),
//                ),
//                'total' => $result_video['nns_all_index'],
//                'publishTime' => str_replace('-', '', $result_video['nns_show_time']),
//                'currCount' => $result_video['nns_new_index']+1,
//                'area' => $result_video['nns_area'],
//                'year' => substr(str_replace('-', '', $result_video['nns_show_time']), 0,4),
//                'streams' => '高清',
//                'issueTime' => $result_video['nns_create_time'],
//                'contentType' => 1,
//                'effective' => 1,
//                'payStatus' => 0,
//                'tryTime' => 0,
//                'strategy' => '',
//                'platform' => 'HB',
//                'platformAbility' => 'INTERNET',
//                'provider' => $result_video['nns_cp_id'],
//                'providerName' => $this->arr_cp_config[$result_video['nns_cp_id']]['nns_name'],
//            )
//        );
        return public_model_exec::return_data(0,'OK',$arr_push['push']);
    }

    public function do_index()
    {
        $sql_task ="select nns_ref_id,nns_modify_time from nns_mgtvbk_c2_task where nns_org_id='".ORG_ID."' and nns_type='index' and nns_ref_id='{$this->arr_params['id']}' and nns_status='0' order by nns_modify_time desc";
        $result_c2_task = nl_query_by_db($sql_task, $this->obj_dc->db());
        if(!$result_c2_task)
        {
            return public_model_exec::return_data(1,'inner error');
        }
        if(empty($result_c2_task) || !is_array($result_c2_task))
        {
            return public_model_exec::return_data(7,'no data');
        }
        $result_c2_task = $result_c2_task[0];
        $arr_push = $this->get_increment_info_xml->make_index_arr($this->obj_dc,$result_c2_task['nns_id'],$this->arr_params['id'],$this->arr_sp_config,$this->arr_cp_config,$this->str_sp_id);
        if(isset($arr_push['ret']) && $arr_push['ret'] != 0 )
        {
            return $arr_push;
        }
//        $sql_index = "select * from nns_vod_index where nns_id ='{$result_c2_task['nns_ref_id']}'";
//        $result_index  = nl_query_by_db($sql_index, $this->obj_dc->db());
//        if(!$result_index)
//        {
//            return public_model_exec::return_data(1,'inner error');
//        }
//        if(empty($result_index) || !is_array($result_index))
//        {
//            return public_model_exec::return_data(7,'no data');
//        }
//        $result_index = $result_index[0];
//        $result_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $result_index['nns_id'],$result_index,$this->arr_sp_config,'cdn');
//        if($result_import_info['ret'] !=0 || !isset($result_import_info['data_info']) || strlen($result_import_info['data_info']) <1)
//        {
//            return $result_import_info;
//        }
//        $result_index['nns_cdn_index_guid'] = $result_import_info['data_info'];
//        $sql_vod = "select * from nns_vod where nns_id ='{$result_index['nns_vod_id']}'";
//        $result_video  = nl_query_by_db($sql_vod, $this->obj_dc->db());
//        if(!$result_video)
//        {
//            return public_model_exec::return_data(1,'inner error');
//        }
//        if(empty($result_video) || !is_array($result_video))
//        {
//            return public_model_exec::return_data(7,'no data');
//        }
//        $result_video = $result_video[0];
//        $result_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_video['nns_id'],$result_video,$this->arr_sp_config,'cdn');
//        if($result_import_info['ret'] !=0 || !isset($result_import_info['data_info']) || strlen($result_import_info['data_info']) <1)
//        {
//            return $result_import_info;
//        }
//        $result_video['nns_cdn_video_guid'] = $result_import_info['data_info'];
//
//        $result_category = $this->get_category_info($result_video);
//        if($result_category['ret'] !=0)
//        {
//            return $result_category;
//        }
//        $str_img_ftp = (isset($this->arr_sp_config['img_ftp']) && strlen($this->arr_sp_config['img_ftp'])>0) ? trim($this->arr_sp_config['img_ftp'],'/').'/' : '';
//        $this->_get_cp_info($result_video['nns_cp_id']);
//        $str_modifyTime = $result_c2_task['nns_modify_time'];
//        $arr_push = array(
//	        'type'=>1,
//	        'modifyTime'=>$str_modifyTime,
//	        'data'=>array(
//    	        'cp' => 'coocaa',
//    	        'originalId' => $result_video['nns_cdn_video_guid'],
//    	        'id' => $result_video['nns_id'],
//    	        'name' => $result_video['nns_name'],
//    	        'alias' => $result_video['nns_alias_name'],
//    	        'sname' => $result_video['nns_pinyin'],
//    	        'focus' => $result_video['nns_keyword'],
//    	        'picV' => strlen($result_video['nns_image_v']) >0 ? $str_img_ftp.trim($result_video['nns_image_v'],'/') : '',
//    	        'picH' => strlen($result_video['nns_image_h']) >0 ? $str_img_ftp.trim($result_video['nns_image_h'],'/') : '',
//    	        'score' => $result_video['nns_point'],
//    	        'playCount' => $result_video['nns_play_count'],
//    	        'desc' => $result_video['nns_summary'],
//    	        'seriesType' => 0,
//    	        'cname' => $result_category['data_info']['category_name'],
//    	        'cid' => $result_video['nns_category_id'],
//    	        'tag' => str_replace('/', ',', $result_video['nns_kind']),
//    	        'persons' => array(
//    	            'composer' => '',
//    	            'dubber' => '',
//    	            'maker' => str_replace('/', ',', $result_video['nns_screenwriter']),
//    	            'producer' => '',
//    	            'guest' => '',
//    	            'actor' => str_replace('/', ',', $result_video['nns_actor']),
//    	            'writer' => str_replace('/', ',', $result_video['nns_screenwriter']),
//    	            'director' => str_replace('/', ',', $result_video['nns_director']),
//    	        ),
//    	        'total' => $result_video['nns_all_index'],
//    	        'publishTime' => str_replace('-', '', $result_video['nns_show_time']),
//    	        'currCount' => $result_video['nns_new_index']+1,
//    	        'area' => $result_video['nns_area'],
//    	        'year' => substr(str_replace('-', '', $result_video['nns_show_time']), 0,4),
//    	        'streams' => '高清',
//    	        'issueTime' => $result_video['nns_create_time'],
//    	        'contentType' => 1,
//    	        'effective' => 1,
//    	        'payStatus' => 0,
//    	        'tryTime' => 0,
//    	        'strategy' => '',
//    	        'platform' => 'HB',
//    	        'platformAbility' => 'INTERNET',
//    	        'provider' => $result_video['nns_cp_id'],
//    	        'providerName' => $this->arr_cp_config[$result_video['nns_cp_id']]['nns_name'],
//                'episodes' =>array(
//                    array(
//                        'originalId'=>$result_index['nns_cdn_index_guid'],
//                        'id'=>$result_index['nns_id'],
//                        'epId' => $result_index['nns_cdn_index_guid'],
//                        'epName' => "{$result_video['nns_name']}第{$result_index['nns_index']}集",
//                        'epSname' => "{$result_video['nns_name']}第{$result_index['nns_index']}集",
//                        'epFocus' => $result_index['nns_summary'],
//                        'persons' => array(
//                            'actor' => str_replace('/', ',', $result_index['nns_actor']),
//                            'writer' => str_replace('/', ',', $result_index['nns_director']),
//                        ),
//                        'length' => $result_index['nns_time_len'],
//                        'period' => '',
//                        'order' => $result_index['nns_index'],
//                        'epType' => 0,
//                        'pic' => strlen($result_index['nns_image']) >0 ? $str_img_ftp.trim($result_index['nns_image'],'/') : '',
//                        'desc' => $result_index['nns_summary'],
//                        'isPay' => 0,
//                        'tryTime' => 0,
//                        'effective' => 1,
//                    ),
//                ),
//    	    )
//	    );
        return public_model_exec::return_data(0,'OK',$arr_push['push']);
    }

    public function do_media()
    {
        $sql_task ="select nns_ref_id,nns_modify_time from nns_mgtvbk_c2_task where nns_org_id='".ORG_ID."' and nns_type='media' and nns_ref_id='{$this->arr_params['id']}' and nns_status='0' order by nns_modify_time desc";
        $result_c2_task = nl_query_by_db($sql_task, $this->obj_dc->db());
        if(!$result_c2_task)
        {
            return public_model_exec::return_data(1,'inner error');
        }
        if(empty($result_c2_task) || !is_array($result_c2_task))
        {
            return public_model_exec::return_data(7,'no data');
        }
        $result_c2_task = $result_c2_task[0];
        $arr_push = $this->get_increment_info_xml->make_media_arr($this->obj_dc,$result_c2_task['nns_id'],$this->arr_params['id'],$this->arr_sp_config,$this->arr_cp_config,$this->str_sp_id,$this->arr_params['nns_file_path']);
        if(isset($arr_push['ret']) && $arr_push['ret'] != 0 )
        {
            return $arr_push;
        }
//        $sql_media = "select * from nns_vod_media where nns_id='{$result_c2_task['nns_ref_id']}'";
//        $result_media  = nl_query_by_db($sql_media, $this->obj_dc->db());
//        if(!$result_media)
//        {
//            return public_model_exec::return_data(1,'inner error');
//        }
//        if(empty($result_media) || !is_array($result_media))
//        {
//            return public_model_exec::return_data(7,'no data');
//        }
//        $result_media = $result_media[0];
//        $result_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $result_media['nns_id'],$result_media,$this->arr_sp_config,'cdn');
//
//        if($result_import_info['ret'] !=0 || !isset($result_import_info['data_info']) || strlen($result_import_info['data_info']) <1)
//        {
//            return $result_import_info;
//        }
//        $result_media['nns_cdn_media_guid'] = $result_import_info['data_info'];
//
//        $sql_index = "select * from nns_vod_index where nns_id ='{$result_media['nns_vod_index_id']}'";
//        $result_index  = nl_query_by_db($sql_index, $this->obj_dc->db());
//        if(!$result_index)
//        {
//            return public_model_exec::return_data(1,'inner error');
//        }
//        if(empty($result_index) || !is_array($result_index))
//        {
//            return public_model_exec::return_data(7,'no data');
//        }
//        $result_index = $result_index[0];
//        $result_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $result_index['nns_id'],$result_index,$this->arr_sp_config,'cdn');
//        if($result_import_info['ret'] !=0 || !isset($result_import_info['data_info']) || strlen($result_import_info['data_info']) <1)
//        {
//            return $result_import_info;
//        }
//        $result_index['nns_cdn_index_guid'] = $result_import_info['data_info'];
//        $sql_vod = "select * from nns_vod where nns_id ='{$result_index['nns_vod_id']}'";
//        $result_video  = nl_query_by_db($sql_vod, $this->obj_dc->db());
//        if(!$result_video)
//        {
//            return public_model_exec::return_data(1,'inner error');
//        }
//        if(empty($result_video) || !is_array($result_video))
//        {
//            return public_model_exec::return_data(7,'no data');
//        }
//        $result_video = $result_video[0];
//        $result_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_video['nns_id'],$result_video,$this->arr_sp_config,'cdn');
//        if($result_import_info['ret'] !=0 || !isset($result_import_info['data_info']) || strlen($result_import_info['data_info']) <1)
//        {
//            return $result_import_info;
//        }
//        $result_video['nns_cdn_video_guid'] = $result_import_info['data_info'];
//
//        $result_category = $this->get_category_info($result_video);
//        if($result_category['ret'] !=0)
//        {
//            return $result_category;
//        }
//        $str_img_ftp = (isset($this->arr_sp_config['img_ftp']) && strlen($this->arr_sp_config['img_ftp'])>0) ? trim($this->arr_sp_config['img_ftp'],'/').'/' : '';
//        $this->_get_cp_info($result_video['nns_cp_id']);
//        $str_modifyTime = $result_c2_task['nns_modify_time'];
//        $arr_push = array(
//	        'type'=>2,
//		    'modifyTime'=>$str_modifyTime,
//	        'data'=>array(
//                'cp' => 'coocaa',
//                'originalId' => $result_video['nns_cdn_video_guid'],
//                'id' => $result_video['nns_id'],
//                'name' => $result_video['nns_name'],
//                'alias' => $result_video['nns_alias_name'],
//                'sname' => $result_video['nns_pinyin'],
//                'focus' => $result_video['nns_keyword'],
//                'picV' => strlen($result_video['nns_image_v']) >0 ? $str_img_ftp.trim($result_video['nns_image_v'],'/') : '',
//                'picH' => strlen($result_video['nns_image_h']) >0 ? $str_img_ftp.trim($result_video['nns_image_h'],'/') : '',
//                'score' => $result_video['nns_point'],
//                'playCount' => $result_video['nns_play_count'],
//                'desc' => $result_video['nns_summary'],
//                'seriesType' => 0,
//    	        'cname' => $result_category['data_info']['category_name'],
//    	        'cid' => $result_video['nns_category_id'],
//                'tag' => str_replace('/', ',', $result_video['nns_kind']),
//                'persons' => array(
//                    'composer' => '',
//                    'dubber' => '',
//                    'maker' => str_replace('/', ',', $result_video['nns_screenwriter']),
//                    'producer' => '',
//                    'guest' => '',
//                    'actor' => str_replace('/', ',', $result_video['nns_actor']),
//                    'writer' => str_replace('/', ',', $result_video['nns_screenwriter']),
//                    'director' => str_replace('/', ',', $result_video['nns_director']),
//                ),
//                'total' => $result_video['nns_all_index'],
//                'publishTime' => str_replace('-', '', $result_video['nns_show_time']),
//                'currCount' => $result_video['nns_new_index']+1,
//                'area' => $result_video['nns_area'],
//                'year' => substr(str_replace('-', '', $result_video['nns_show_time']), 0,4),
//                'streams' => '高清',
//                'issueTime' => $result_video['nns_create_time'],
//                'contentType' => 1,
//                'effective' => 1,
//    	        'payStatus' => 0,
//                'tryTime' => 0,
//                'strategy' => '',
//                'platform' => 'HB',
//                'platformAbility' => 'INTERNET',
//                'provider' => $result_video['nns_cp_id'],
//                'providerName' => $this->arr_cp_config[$result_video['nns_cp_id']]['nns_name'],
//                'episodes' =>array(
//                    array(
//                            'originalId'=>$result_index['nns_cdn_index_guid'],
//                            'id'=>$result_index['nns_id'],
//                            'epId' => $result_index['nns_cdn_index_guid'],
//                            'epName' => "{$result_video['nns_name']}第{$result_index['nns_index']}集",
//                            'epSname' => "{$result_video['nns_name']}第{$result_index['nns_index']}集",
//                            'epFocus' => $result_index['nns_summary'],
//                            'persons' => array(
//                                'actor' => str_replace('/', ',', $result_index['nns_actor']),
//                                'writer' => str_replace('/', ',', $result_index['nns_director']),
//                            ),
//                            'length' => $result_index['nns_time_len'],
//                            'period' => '',
//                            'order' => $result_index['nns_index'],
//                            'epType' => 0,
//                            'pic' => strlen($result_index['nns_image']) >0 ? $str_img_ftp.trim($result_index['nns_image'],'/') : '',
//                            'desc' => $result_index['nns_summary'],
//                            'isPay' => 0,
//                            'tryTime' => 0,
//                            'effective' => 1,
//                            'mediaUrl' => '',
//                            'mediaoriginalId'=>$result_media['nns_cdn_media_guid'],
//                            'mediaid'=>$result_media['nns_id'],
//                    ),
//                ),
//            )
//        );
//        /*
//        * 下面是根据不同的类型媒资进行原始ID等参数修正
//        * song.wang 2018-07-30
//        */
//        //1、爱奇艺、优酷、腾讯等互联网媒资
//        if(in_array($result_video['nns_cp_id'],array('aiqiyi')))
//        {
//            $media_url_info = explode('_',$result_media['nns_url']);
//            $arr_push['data']['episodes'][0]['originalId'] = $media_url_info['2'];          //分集原始ID
//            $arr_push['data']['episodes'][0]['mediaoriginalId'] = $media_url_info['3'];     //片源原始ID
//        }
//        //2、播控中心OTT内容   IP流
//        if(in_array($result_video['nns_cp_id'],array('fuse_JSCN')))
//        {
//            //IP流获取cpcode+systemid
//            $this->init_cp_info($result_video['nns_cp_id']);
//            $arr_push['data']['platformAbility'] = 'IP';            //平台标识
//            $arr_push['data']['episodes'][0]['mediaoriginalId'] = $result_media['nns_content_id'];                  //片源原始ID
//            $arr_push['data']['episodes'][0]['CPcode'] =  $this->str_cmsid;             //CPcode
//            $arr_push['data']['episodes'][0]['SystemID'] =  $this->str_target_system_id;    //SystemID
//        }
//        //3、播控中心VOD内容   IPQAM
//        if(in_array($result_video['nns_cp_id'],array('cds_ipqam')))
//        {
//            $arr_push['data']['platformAbility'] = 'IPQAM';            //平台标识
//            $arr_push['data']['episodes'][0]['mediaoriginalId'] = $result_media['nns_content_id'];                  //片源原始ID
//        }
////		if(!in_array($result_media['nns_cp_id'], array('aiqiyi')))
////		{
////		    if((int)$this->arr_sp_config['disabled_clip'] !== 2 && (int)$this->arr_sp_config['disabled_clip'] !== 3)
////		    {
////		        $result_media['nns_url'] = ltrim((trim($this->arr_params['nns_file_path'])),'/');
////		        if(isset($this->arr_sp_config['media_ftp']) && !empty($this->arr_sp_config['media_ftp']))
////		        {
////		            $this->arr_sp_config['media_ftp'] = rtrim((trim($this->arr_sp_config['media_ftp'])),'/');
////		            if (stripos($result_media['nns_url'], 'http://') === FALSE && stripos($result_media['nns_url'], 'ftp://') === FALSE)
////		            {
////		                $result_media['nns_url'] = $this->arr_sp_config['media_ftp'].'/'.$result_media['nns_url'];
////		            }
////		        }
////		    }
////		    else
////		    {
////		        $result_media['nns_url'] = ltrim((trim($result_media['nns_url'])),'/');
////		        if(isset($this->arr_sp_config['media_ftp']) && !empty($this->arr_sp_config['media_ftp']))
////		        {
////		            $this->arr_sp_config['media_ftp'] = rtrim((trim($this->arr_sp_config['media_ftp'])),'/');
////		            if (stripos($result_media['nns_url'], 'http://') === FALSE && stripos($result_media['nns_url'], 'ftp://') === FALSE)
////		            {
////		                $result_media['nns_url'] = $this->arr_sp_config['media_ftp'].'/'.$result_media['nns_url'];
////		            }
////		        }
////		    }
////		    $arr_push['data']['episodes'][0]['mediaUrl'] = $result_media['nns_url'];
////		}
//        if(!in_array($result_media['nns_cp_id'], array('aiqiyi')))
//        {
//            if((int)$this->arr_sp_config['disabled_clip'] !== 2 && (int)$this->arr_sp_config['disabled_clip'] !== 3)
//            {
//                $result_media['nns_url'] = ltrim((trim($this->arr_params['nns_file_path'])),'/');
//            }
//            else
//            {
//                $result_media['nns_url'] = ltrim((trim($result_media['nns_url'])),'/');
//            }
//            if(isset($this->arr_sp_config['media_ftp']) && !empty($this->arr_sp_config['media_ftp']))
//            {
//                $this->arr_sp_config['media_ftp'] = rtrim((trim($this->arr_sp_config['media_ftp'])),'/');
//                if (stripos($result_media['nns_url'], 'http://') === FALSE && stripos($result_media['nns_url'], 'ftp://') === FALSE)
//                {
//                    $result_media['nns_url'] = $this->arr_sp_config['media_ftp'].'/'.$result_media['nns_url'];
//                }
//            }
//            $arr_push['data']['episodes'][0]['mediaUrl'] = $result_media['nns_url'];
//        }
        return public_model_exec::return_data(0,'OK',$arr_push['push']);
    }

    public function action()
    {
        if($this->arr_params['type'] == '0')
        {
            return $this->do_video();
        }
        else if($this->arr_params['type'] == '1')
        {
            return $this->do_index();
        }
        else if($this->arr_params['type'] == '2')
        {
            return $this->do_media();
        }
        return public_model_exec::return_data(1,'params type error');
    }

    public function action_v2()
    {
        $result_init = $this->init();
        $str_img_ftp = (isset($this->arr_sp_config['img_ftp']) && strlen($this->arr_sp_config['img_ftp'])>0) ? trim($this->arr_sp_config['img_ftp'],'/').'/' : '';
        if($result_init['ret'] !=0)
        {
            return $result_init;
        }
        if($this->arr_params['type'] == '0')
        {
            $sql_task ="select nns_ref_id,nns_modify_time from nns_mgtvbk_c2_task where nns_org_id='".ORG_ID."' and nns_src_id in (select nns_id from nns_vod_index where nns_vod_id='{$this->arr_params['id']}') and nns_status='0'";
            $sql_task ="select nns_ref_id,nns_modify_time from nns_mgtvbk_c2_task where nns_org_id='".ORG_ID."' and nns_src_id in (select nns_id from nns_vod_index where nns_vod_id='{$this->arr_params['id']}')";
        }
        else if($this->arr_params['type'] == '1')
        {
            $sql_task ="select nns_ref_id,nns_modify_time from nns_mgtvbk_c2_task where nns_org_id='".ORG_ID."' and nns_src_id ='{$this->arr_params['id']}' and nns_status='0'";
            $sql_task ="select nns_ref_id,nns_modify_time from nns_mgtvbk_c2_task where nns_org_id='".ORG_ID."' and nns_src_id ='{$this->arr_params['id']}'";
        }
        else
        {
            $sql_task ="select nns_ref_id,nns_modify_time from nns_mgtvbk_c2_task where nns_org_id='".ORG_ID."' and nns_ref_id ='{$this->arr_params['id']}' and nns_status='0'";
            $sql_task ="select nns_ref_id,nns_modify_time from nns_mgtvbk_c2_task where nns_org_id='".ORG_ID."' and nns_ref_id ='{$this->arr_params['id']}'";
        }
        $result_c2_task = nl_query_by_db($sql_task, $this->obj_dc->db());
        if(!$result_c2_task)
        {
            return public_model_exec::return_data(1,'inner error');
        }
        if(empty($result_c2_task) || !is_array($result_c2_task))
        {
            return public_model_exec::return_data(7,'no data');
        }
        $c2_task = $last_data = $arr_vod = $arr_index = $arr_media = null;
        foreach ($result_c2_task as $value)
        {
            $c2_task[$value['nns_ref_id']] = $value['nns_modify_time'];
        }
        $sql_media = "select * from nns_vod_media where nns_id in('".implode("','", array_keys($c2_task))."')";
        $result_media  = nl_query_by_db($sql_media, $this->obj_dc->db());
        if(!$result_media)
        {
            return public_model_exec::return_data(1,'inner error');
        }
        if(empty($result_media) || !is_array($result_media))
        {
            return public_model_exec::return_data(7,'no data');
        }
        foreach ($result_media as $media_info)
        {
            $result_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $media_info['nns_id'],$media_info,$this->arr_sp_config,'cdn');
            if($result_import_info['ret'] !=0 || !isset($result_import_info['data_info']) || strlen($result_import_info['data_info']) <1)
            {
                return $result_import_info;
            }
            $media_info['nns_cdn_media_guid'] = $result_import_info['data_info'];
            $arr_media[$media_info['nns_id']] = $media_info;
            if(!isset($arr_index[$media_info['nns_vod_index_id']]))
            {
                $arr_index[$media_info['nns_vod_index_id']] = $media_info['nns_vod_index_id'];
            }
            if(!isset($arr_index[$media_info['nns_vod_id']]))
            {
                $arr_vod[$media_info['nns_vod_id']] = $media_info['nns_vod_id'];
            }
        }
        $sql_index = "select * from nns_vod_index where nns_id in('".implode("','", array_keys($arr_index))."')";
        $result_index  = nl_query_by_db($sql_index, $this->obj_dc->db());
        $sql_vod = "select * from nns_vod where nns_id in('".implode("','", array_keys($arr_vod))."')";
        $result_vod  = nl_query_by_db($sql_vod, $this->obj_dc->db());
        if(!$result_vod)
        {
            return public_model_exec::return_data(1,'inner error');
        }
        if(empty($result_vod) || !is_array($result_vod))
        {
            return public_model_exec::return_data(7,'no data');
        }
        if(!$result_index)
        {
            return public_model_exec::return_data(1,'inner error');
        }
        if(empty($result_index) || !is_array($result_index))
        {
            return public_model_exec::return_data(7,'no data');
        }
        foreach ($result_index as $index_info)
        {
            $result_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $index_info['nns_id'],$index_info,$this->arr_sp_config,'cdn');
            if($result_import_info['ret'] !=0 || !isset($result_import_info['data_info']) || strlen($result_import_info['data_info']) <1)
            {
                return $result_import_info;
            }
            $index_info['nns_cdn_index_guid'] = $result_import_info['data_info'];
            $arr_index[$index_info['nns_id']] = $index_info;
        }
        foreach ($result_vod as $vod_info)
        {
            $result_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $vod_info['nns_id'],$vod_info,$this->arr_sp_config,'cdn');
            if($result_import_info['ret'] !=0 || !isset($result_import_info['data_info']) || strlen($result_import_info['data_info']) <1)
            {
                return $result_import_info;
            }
            $vod_info['nns_cdn_video_guid'] = $result_import_info['data_info'];
    
            $result_depot = nl_depot::get_depot_info($this->obj_dc,array('nns_id'=>$vod_info['nns_depot_id']),'video');
            if($result_depot['ret'] !=0 || !isset($result_depot['data_info'][0]) || empty($result_depot['data_info'][0]) || !is_array($result_depot['data_info'][0]))
            {
                return $result_depot;
            }
            $result_depot = $result_depot['data_info'][0];
            $dom = new DOMDocument('1.0', 'utf-8');
            $depot_info = $result_depot['nns_category'];
            if(!$this->is_xml($depot_info))
            {
                return $result_depot;
            }
            $nns_category_id = $vod_info['nns_category_id'];
            $dom->loadXML($depot_info);
            $categorys = $dom->getElementsByTagName('category');
            $arr_category_all = array();
            //默认父级栏目id 为0
            $parent_category_id = 0;
            foreach ($categorys as $category)
            {
                if ((string)$category->getAttribute('id') == (string)$nns_category_id)
                {
                    $category_name = $category->getAttribute('name');
                    //查询父级栏目id 如果为0 则为0  如果不为0 则是该父级的id
                    $parent_category_id =  ((string)$category->getAttribute('parent') == '0') ? 0 : (string)$category->getAttribute('parent');
                    break;
                }
                //所有栏目id的集合
                $arr_category_all[(string)$category->getAttribute('id')] = $category->getAttribute('name');
            }
            $vod_info['category_name'] = $category_name;
    
            $arr_vod[$vod_info['nns_id']] = $vod_info;
        }
        foreach ($arr_media as $media_info)
        {
            if(!isset($arr_vod[$media_info['nns_vod_id']]) || !is_array($arr_vod[$media_info['nns_vod_id']]))
            {
                continue;
            }
            if(!isset($arr_index[$media_info['nns_vod_index_id']]) || !is_array($arr_index[$media_info['nns_vod_index_id']]))
            {
                continue;
            }
            $this->_get_cp_info($arr_vod[$media_info['nns_vod_id']]['nns_cp_id']);
            if(!isset($last_data[$media_info['nns_vod_id']]['cp']))
            {
                $last_data[$media_info['nns_vod_id']] = array(
                    'cp' => 'coocaa',
                    'originalId' => $arr_vod[$media_info['nns_vod_id']]['nns_cdn_video_guid'],
                    'id' => $arr_vod[$media_info['nns_vod_id']]['nns_id'],
                    'name' => $arr_vod[$media_info['nns_vod_id']]['nns_name'],
                    'alias' => $arr_vod[$media_info['nns_vod_id']]['nns_alias_name'],
                    'sname' => $arr_vod[$media_info['nns_vod_id']]['nns_pinyin'],
                    'focus' => $arr_vod[$media_info['nns_vod_id']]['nns_keyword'],
                    'picV' => strlen($arr_vod[$media_info['nns_vod_id']]['nns_image_v']) > 0 ? $str_img_ftp.trim($arr_vod[$media_info['nns_vod_id']]['nns_image_v'],'/') : '',
                    'picH' => strlen($arr_vod[$media_info['nns_vod_id']]['nns_image_h']) > 0 ? $str_img_ftp.trim($arr_vod[$media_info['nns_vod_id']]['nns_image_h'],'/') : '',
                    'score' => $arr_vod[$media_info['nns_vod_id']]['nns_point'],
                    'playCount' => $arr_vod[$media_info['nns_vod_id']]['nns_play_count'],
                    'desc' => $arr_vod[$media_info['nns_vod_id']]['nns_summary'],
                    'seriesType' => 0,
                    'cname' => $arr_vod[$media_info['nns_vod_id']]['category_name'],
                    'cid' => $arr_vod[$media_info['nns_vod_id']]['nns_category_id'],
                    'tag' => str_replace('/', ',', $arr_vod[$media_info['nns_vod_id']]['nns_kind']),
                    'persons' => array(
                        'composer' => '',
                        'dubber' => '',
                        'maker' => str_replace('/', ',', $arr_vod[$media_info['nns_vod_id']]['nns_screenwriter']),
                        'producer' => '',
                        'guest' => '',
                        'actor' => str_replace('/', ',', $arr_vod[$media_info['nns_vod_id']]['nns_actor']),
                        'writer' => str_replace('/', ',', $arr_vod[$media_info['nns_vod_id']]['nns_screenwriter']),
                        'director' => str_replace('/', ',', $arr_vod[$media_info['nns_vod_id']]['nns_director']),
                    ),
                    'total' => $arr_vod[$media_info['nns_vod_id']]['nns_all_index'],
                    'publishTime' => str_replace('-', '', $arr_vod[$media_info['nns_vod_id']]['nns_show_time']),
                    'currCount' => $arr_vod[$media_info['nns_vod_id']]['nns_new_index']+1,
                    'area' => $arr_vod[$media_info['nns_vod_id']]['nns_area'],
                    'year' => substr(str_replace('-', '', $arr_vod[$media_info['nns_vod_id']]['nns_show_time']), 0,4),
                    'streams' => '高清',
                    'issueTime' => $arr_vod[$media_info['nns_vod_id']]['nns_create_time'],
                    'contentType' => 1,
                    'effective' => 1,
                    'payStatus' => '',
                    'tryTime' => 0,
                    'strategy' => '',
                    'platform' => 'HB',
                    'platformAbility' => 'INTERNET',
                    'provider' => $arr_vod[$media_info['nns_vod_id']]['nns_cp_id'],
                    'providerName' => $this->arr_cp_config[$arr_vod[$media_info['nns_vod_id']]['nns_cp_id']]['nns_name'],
                );
            }
            $arr_index[$media_info['nns_vod_index_id']]['nns_index']++;
            $last_data[$media_info['nns_vod_id']]['episodes'][] = array(
                'originalId'=>$arr_index[$media_info['nns_vod_index_id']]['nns_cdn_index_guid'],
                'id'=>$arr_index[$media_info['nns_vod_index_id']]['nns_id'],
                'epId' => $arr_index[$media_info['nns_vod_index_id']]['nns_cdn_index_guid'],
                'epName' => "{$arr_vod[$media_info['nns_vod_id']]['nns_name']}第{$arr_index[$media_info['nns_vod_index_id']]['nns_index']}集",
                'epSname' => "{$arr_vod[$media_info['nns_vod_id']]['nns_name']}第{$arr_index[$media_info['nns_vod_index_id']]['nns_index']}集",
                'epFocus' => $arr_index[$media_info['nns_vod_index_id']]['nns_summary'],
                'persons' => array(
                    'actor' => str_replace('/', ',', $arr_index[$media_info['nns_vod_index_id']]['nns_actor']),
                    'writer' => str_replace('/', ',', $arr_index[$media_info['nns_vod_index_id']]['nns_director']),
                ),
                'length' => $arr_index[$media_info['nns_vod_index_id']]['nns_time_len'],
                'period' => '',
                'order' => $arr_index[$media_info['nns_vod_index_id']]['nns_index'],
                'epType' => 0,
                'pic' => strlen($arr_index[$media_info['nns_vod_index_id']]['nns_image']) > 0 ? $str_img_ftp.trim($arr_index[$media_info['nns_vod_index_id']]['nns_image'],'/') : '',
                'desc' => $arr_index[$media_info['nns_vod_index_id']]['nns_summary'],
                'isPay' => 0,
                'tryTime' => 0,
                'effective' => 1,
                'mediaUrl' => '',
                'mediaoriginalId'=>$media_info['nns_cdn_media_guid'],
                'mediaid'=>$media_info['nns_id'],
            );
        }
        $last_data = is_array($last_data) ? array_values($last_data) : null;
        return public_model_exec::return_data(0,'ok',$last_data);
    }

}
$obj_increment_info = new increment_info();
$result = $obj_increment_info->action();
$result = ($result['ret']=='0' && isset($result['data_info']) && is_array($result['data_info']) && !empty($result['data_info'])) ? $result['data_info'] : array();
echo json_encode($result);die;
