<?php
/**
 * Created by PhpStorm.
 * User: song.wang
 * Date: 2018/7/31
 * Time: 22:16
 */
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/video/vod.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/depot/depot.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/vod_media/vod_media.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/video/vod_index.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/models/public_model_exec.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/c2_task/c2_task.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/file_encode/file_encode.class.php';
class get_increment_info_xml
{
    private $arr_restrict_cp = array(
        'INTERNET' => array(
            'none' => array(
                'aiqiyi'
            )
        ),
        'IP' => array(
            'jscn_sihua' => array(
                'fuse_JSCN',
                'fuse_xiaole',
                'fuse_xiaole_encode',
                'xiaole',
                'xiaole_encode'
            )
        ),
        'IPQAM' => array(
            'sihua_vod_new' => array(
                'fuse_CCTV_GQ',
                'fuse_imba',
                'fuse_SiTV',
                'fuse_SiTVOTT',
                'fuse_tenstar4k',
            ),
            'none' => array(
                'cds_ipqam',
            )
        ),
        'file' => array(
            'none' => array(
                'fuse_SiTV',
                'fuse_tenstarsohu',
                'fuse_youku',
                'fuse_nmsx',
            )
        )
    );
    
    
    private $str_cmsid = 'JSB12028';
    private $str_header = 'JSB12028';
    private $str_target_system_id = '30-S';
    private $str_target_site_id = "nanjing";
    private $str_sihua_sp_id = "SP1N02M00_04_063";

    /**
     * 获取片源注入CDN的状态
     * @param unknown $type
     * @param unknown $cp_id
     * @param unknown $data
     * @param unknown $obj_dc
     */
    private function get_sp_c2_state($type,$cp_id,$data,$obj_dc)
    {
        $flag = false;
        if(!isset($this->arr_restrict_cp[$type]) || !is_array($this->arr_restrict_cp[$type]) || empty($this->arr_restrict_cp[$type]))
        {
            return $flag;
        }
        foreach ($this->arr_restrict_cp[$type] as $key=>$value)
        {
            if(!is_array($value) || empty($value))
            {
                continue;
            }
            if(!in_array($cp_id, $value))
            {
                continue;
            }
            if($key == 'none')
            {
                return true;
            }
            $result = nl_c2_task::query_by_condition_v2($obj_dc, array('nns_org_id'=>$key,'nns_type'=>'media','nns_ref_id'=>$data['nns_id']));
            if($result['ret'] !=0)
            {
                return $flag;
            }
            if(!isset($result['data_info'][0]) || !is_array($result['data_info'][0]) || empty($result['data_info'][0]))
            {
                $resultl_new_media = nl_file_encode::query_by_condition($obj_dc,array('nns_sp_id'=>$key,'nns_in_media_id'=>$data['nns_id']));
                if($result['ret'] !=0)
                {
                    return $flag;
                }
                if(!isset($resultl_new_media['data_info'][0]) || !is_array($resultl_new_media['data_info'][0]) || empty($resultl_new_media['data_info'][0]))
                {
                    return $flag;
                }
                $result = nl_c2_task::query_by_condition_v2($obj_dc, array('nns_org_id'=>$key,'nns_type'=>'media','nns_ref_id'=>$resultl_new_media['data_info'][0]['nns_out_media_id']));
                if($result['ret'] !=0)
                {
                    return $flag;
                }
                if(!isset($result['data_info'][0]) || !is_array($result['data_info'][0]) || empty($result['data_info'][0]))
                {
                    return $flag;
                }
            }
            $result = $result['data_info'][0];
            if(strlen($result['nns_status']) >0 && $result['nns_status'] == '0')
            {
                return true;
            }
            return $flag;
        }
        return $flag;
    }
    
    
    /**
     * 获取所有通过的cp数组
     */
    private function get_all_pass_cp($type=null)
    {
        $data = array();
        if(isset($type) && $type !== null && strlen($type) >0)
        {
            if(!isset($this->arr_restrict_cp[$type]) || !is_array($this->arr_restrict_cp[$type]) || empty($this->arr_restrict_cp[$type]))
            {
                return $data;
            }
            foreach ($this->arr_restrict_cp[$type] as $value)
            {
                if(!is_array($value) || empty($value))
                {
                    continue;
                }
                foreach ($value as $_v)
                {
                    if(in_array($_v, $data))
                    {
                        continue;
                    }
                    $data[] = $_v;
                }
            }
            return $data;
        }
        if(!is_array($this->arr_restrict_cp) || empty($this->arr_restrict_cp))
        {
            return $data;
        }
        foreach ($this->arr_restrict_cp as $value)
        {
            if(!is_array($value) || empty($value))
            {
                continue;
            }
            foreach ($value as $_v)
            {
                if(!is_array($_v) || empty($_v))
                {
                    continue;
                }
                foreach ($_v as $__v)
                {
                    if(in_array($__v, $data))
                    {
                        continue;
                    }
                    $data[] = $__v;
                }
            }
        }
        return $data;
    }
    
    
    private function init_cp_info($obj_dc,$cp_id,$str_sp_id)
    {
        $result_project_key = nl_project_key::query_by_key($obj_dc, $cp_id);
        if($result_project_key['ret'] !=0 || !isset($result_project_key['data_info']) || empty($result_project_key['data_info']) || !is_array($result_project_key['data_info']))
        {
            return $result_project_key;
        }
        $result_project_key = $result_project_key['data_info'];
        $nns_key_value = isset($result_project_key['nns_key_value']) ? $result_project_key['nns_key_value'] : '';
        if(!$this->is_json($nns_key_value))
        {
            return $result_project_key;
        }
        $nns_key_value = json_decode($nns_key_value,true);
        if(isset($nns_key_value[$str_sp_id]) && strlen($nns_key_value[$str_sp_id]))
        {
            $this->str_cmsid  = $nns_key_value[$str_sp_id];
            $this->str_header = $nns_key_value[$str_sp_id];
        }
        if(isset($nns_key_value['target_system_id']) && strlen($nns_key_value['target_system_id'])>0)
        {
            $this->str_target_system_id = $nns_key_value['target_system_id'];
        }
        if(isset($nns_key_value['target_site_id']) && strlen($nns_key_value['target_site_id'])>0)
        {
            $this->str_target_site_id = $nns_key_value['target_site_id'];
        }
        if(isset($nns_key_value['sihua_sp_id']) && strlen($nns_key_value['sihua_sp_id'])>0)
        {
            $this->str_sihua_sp_id = $nns_key_value['sihua_sp_id'];
        }
        return $result_project_key;
    }

    public function make_vod_arr($obj_dc,$nns_id,$nns_ref_id,$arr_sp_config,$arr_cp_config,$str_sp_id)
    {
        $arr_push = array();
        $arr_push['ret'] = 0;
        $result_video = nl_vod::get_video_info_by_id($obj_dc, $nns_ref_id);
        if($result_video['ret'] !=0)
        {
            return $result_video;
        }
        if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
        {
            nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$nns_id}'", $obj_dc->db());
            $arr_push['ret'] = 1;
            $arr_push['reason'] = "基础数据为空";
            return $arr_push;
        }
        $result_video = $result_video['data_info'][0];
        $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($str_sp_id, 'video', $result_video['nns_id'],$result_video,null,'cdn');
        if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
        {
            return $result_video_import_info;
        }
        $result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
        $result_category = $this->get_category_info($obj_dc,$result_video);
        if($result_category['ret'] !=0)
        {
            nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$nns_id}'", $obj_dc->db());
            return $result_category;
        }

        if(!in_array($result_video['nns_cp_id'],$this->get_all_pass_cp()))
        {
            $arr_push['ret'] = 1;
            $arr_push['reason'] = "CP不在范围内";
            return $arr_push;
        }

        $str_img_ftp = (isset($arr_sp_config['img_ftp']) && strlen($arr_sp_config['img_ftp'])>0) ? trim($arr_sp_config['img_ftp'],'/').'/' : '';
        $arr_cp_config = $this->_get_cp_info($obj_dc,$result_video['nns_cp_id'],$arr_cp_config);
        $str_modifyTime = date("Y-m-d H:i:s");
        $arr_push['push'] = array(
            'type'=>0,
            'modifyTime'=>$str_modifyTime,
            'data'=>array(
                'cp' => 'coocaa',
                'originalId' => $result_video['nns_cdn_video_guid'],
                'id' => $result_video['nns_id'],
                'name' => $result_video['nns_name'],
                'alias' => $result_video['nns_alias_name'],
                'sname' => $result_video['nns_pinyin'],
                'focus' => $result_video['nns_keyword'],
                'picV' => strlen($result_video['nns_image_v']) >0 ? $str_img_ftp.trim($result_video['nns_image_v'],'/') : '',
                'picH' => strlen($result_video['nns_image_h']) >0 ? $str_img_ftp.trim($result_video['nns_image_h'],'/') : '',
                'score' => $result_video['nns_point'],
                'playCount' => $result_video['nns_play_count'],
                'desc' => $result_video['nns_summary'],
                'seriesType' => 0,
                'cname' => $result_category['data_info']['category_name'],
                'cid' => $result_video['nns_category_id'],
                'platform' => 'HB',
                'platformAbility' => 'INTERNET',
                'provider' => $result_video['nns_cp_id'],
                'providerName' => $arr_cp_config[$result_video['nns_cp_id']]['nns_name'],
                'tag' => str_replace('/', ',', $result_video['nns_kind']),
                'persons' => array(
                    'composer' => '',
                    'dubber' => '',
                    'maker' => str_replace('/', ',', $result_video['nns_screenwriter']),
                    'producer' => '',
                    'guest' => '',
                    'actor' => str_replace('/', ',', $result_video['nns_actor']),
                    'writer' => str_replace('/', ',', $result_video['nns_screenwriter']),
                    'director' => str_replace('/', ',', $result_video['nns_director']),
                ),
                'total' => $result_video['nns_all_index'],
                'publishTime' => str_replace('-', '', $result_video['nns_show_time']),
                'currCount' => $result_video['nns_new_index']+1,
                'area' => $result_video['nns_area'],
                'year' => substr(str_replace('-', '', $result_video['nns_show_time']), 0,4),
                'streams' => '高清',
                'issueTime' => $result_video['nns_create_time'],
                'contentType' => 1,
                'effective' => 1,
                'payStatus' => 0,
                'tryTime' => 0,
                'strategy' => '',
            )
        );

        $arr_push['reasult_video'] = $result_video;
        //1、互联网
        if(in_array($result_video['nns_cp_id'],$this->get_all_pass_cp('INTERNET')))
        {
            //不做操作
        }
        //2、IP流
        else if(in_array($result_video['nns_cp_id'],$this->get_all_pass_cp('IP')))
        {
            $arr_push['push']['data']['platformAbility'] = 'IP';
        }
        //3、IPQAM
        else if(in_array($result_video['nns_cp_id'],$this->get_all_pass_cp('IPQAM')))
        {
            $arr_push['push']['data']['platformAbility'] = 'IPQAM';
        }
        //4、文件类型
        else if(in_array($result_video['nns_cp_id'], $this->get_all_pass_cp('file')))
        {
            $arr_pushv['data']['platform'] = 'starcor';
            $arr_push['push']['data']['platformAbility'] = 'file';
        }
        return $arr_push;
    }

    public function make_index_arr($obj_dc,$nns_id,$nns_ref_id,$arr_sp_config,$arr_cp_config,$str_sp_id)
    {
        $arr_push = array();
        $arr_push['ret'] = 0;
        $result_index = nl_vod_index::get_video_index_info_by_id($obj_dc, $nns_ref_id);
        if($result_index['ret'] !=0)
        {
            return $result_index;
        }
        if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
        {
            nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$nns_id}'", $obj_dc->db());
            $arr_push['ret'] = 1;
            $arr_push['reason'] = "分集基础数据为空";
            return $arr_push;
        }
        $result_index = $result_index['data_info'][0];
        $result_index_import_info = public_model_exec::get_asset_id_by_sp_id($str_sp_id, 'index', $result_index['nns_id'],$result_index,null,'cdn');

        if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
        {
            return $result_index_import_info;
        }
        $result_index['nns_cdn_index_guid'] = $result_index_import_info['data_info'];
        $result_video = nl_vod::get_video_info_by_id($obj_dc, $result_index['nns_vod_id']);
        if($result_video['ret'] !=0)
        {
            $arr_push['ret'] = 1;
            $arr_push['reason'] = "分集基础数据查询失败";
            return $arr_push;
        }
        if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
        {
            nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$nns_id}'", $obj_dc->db());
            $arr_push['ret'] = 1;
            $arr_push['reason'] = "媒资基础数据为空";
            return $arr_push;
        }
        $result_video = $result_video['data_info'][0];
        $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($str_sp_id, 'video', $result_video['nns_id'],$result_video,null,'cdn');

        if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
        {
            return $result_video_import_info;
        }
        $result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
        $result_category = $this->get_category_info($obj_dc,$result_video);
        if($result_category['ret'] !=0)
        {
            nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$nns_id}'", $obj_dc->db());
            return $result_category;
        }
        $result_index['nns_index']++;
        $str_img_ftp = (isset($arr_sp_config['img_ftp']) && strlen($arr_sp_config['img_ftp'])>0) ? trim($arr_sp_config['img_ftp'],'/').'/' : '';
        $arr_cp_config = $this->_get_cp_info($obj_dc,$result_video['nns_cp_id'],$arr_cp_config);
        $str_modifyTime = date("Y-m-d H:i:s");

        if(!in_array($result_video['nns_cp_id'],$this->get_all_pass_cp()))
        {
            $arr_push['ret'] = 1;
            $arr_push['reason'] = "CP不在范围内";
            return $arr_push;
        }

        $arr_push['push'] = array(
            'type'=>1,
            'modifyTime'=>$str_modifyTime,
            'data'=>array(
                'cp' => 'coocaa',
                'originalId' => $result_video['nns_cdn_video_guid'],
                'id' => $result_video['nns_id'],
                'name' => $result_video['nns_name'],
                'alias' => $result_video['nns_alias_name'],
                'sname' => $result_video['nns_pinyin'],
                'focus' => $result_video['nns_keyword'],
                'picV' => strlen($result_video['nns_image_v']) >0 ? $str_img_ftp.trim($result_video['nns_image_v'],'/') : '',
                'picH' => strlen($result_video['nns_image_h']) >0 ? $str_img_ftp.trim($result_video['nns_image_h'],'/') : '',
                'score' => $result_video['nns_point'],
                'playCount' => $result_video['nns_play_count'],
                'desc' => $result_video['nns_summary'],
                'seriesType' => 0,
                'cname' => $result_category['data_info']['category_name'],
                'cid' => $result_video['nns_category_id'],
                'tag' => str_replace('/', ',', $result_video['nns_kind']),
                'persons' => array(
                    'composer' => '',
                    'dubber' => '',
                    'maker' => str_replace('/', ',', $result_video['nns_screenwriter']),
                    'producer' => '',
                    'guest' => '',
                    'actor' => str_replace('/', ',', $result_video['nns_actor']),
                    'writer' => str_replace('/', ',', $result_video['nns_screenwriter']),
                    'director' => str_replace('/', ',', $result_video['nns_director']),
                ),
                'total' => $result_video['nns_all_index'],
                'publishTime' => str_replace('-', '', $result_video['nns_show_time']),
                'currCount' => $result_video['nns_new_index']+1,
                'area' => $result_video['nns_area'],
                'year' => substr(str_replace('-', '', $result_video['nns_show_time']), 0,4),
                'streams' => '高清',
                'issueTime' => $result_video['nns_create_time'],
                'contentType' => 1,
                'effective' => 1,
                'payStatus' => 0,
                'tryTime' => 0,
                'strategy' => '',
                'platform' => 'HB',
                'platformAbility' => 'INTERNET',
                'provider' => $result_video['nns_cp_id'],
                'providerName' => $arr_cp_config[$result_video['nns_cp_id']]['nns_name'],
                'episodes' =>array(
                    array(
                        'originalId'=>$result_index['nns_cdn_index_guid'],
                        'id'=>$result_index['nns_id'],
                        'epId' => $result_index['nns_cdn_index_guid'],
                        'epName' => "{$result_video['nns_name']}第{$result_index['nns_index']}集",
                        'epSname' => "{$result_video['nns_name']}第{$result_index['nns_index']}集",
                        'epFocus' => $result_index['nns_summary'],
                        'persons' => array(
                            'actor' => str_replace('/', ',', $result_index['nns_actor']),
                            'writer' => str_replace('/', ',', $result_index['nns_director']),
                        ),
                        'length' => $result_index['nns_time_len'],
                        'period' => '',
                        'order' => $result_index['nns_index'],
                        'epType' => 0,
                        'pic' => strlen($result_index['nns_image']) >0 ? $str_img_ftp.trim($result_index['nns_image'],'/') : '',
                        'desc' => $result_index['nns_summary'],
                        'isPay' => 0,
                        'tryTime' => 0,
                        'effective' => 1,
                    ),
                ),
            )
        );
        //1、互联网
        if(in_array($result_video['nns_cp_id'],$this->get_all_pass_cp('INTERNET')))
        {
            $media_info = nl_vod_media_v2::get_vod_media($obj_dc,$result_index['nns_vod_id'],($result_index['nns_index']-1));
            $media_url_info = $media_info[0]['nns_url'];
            $media_url_info = explode('_',$media_url_info);
            $arr_push['push']['data']['episodes'][0]['originalId'] = $media_url_info['2'];
        }
        //2、IP流
        else if(in_array($result_video['nns_cp_id'],$this->get_all_pass_cp('IP')))
        {
            $arr_push['push']['data']['platformAbility'] = 'IP';
        }
        //3、IPQAM
        else if(in_array($result_video['nns_cp_id'],$this->get_all_pass_cp('IPQAM')))
        {
            $arr_push['push']['data']['platformAbility'] = 'IPQAM';
        }
        //4、文件类型
        else if(in_array($result_video['nns_cp_id'],$this->get_all_pass_cp('file')))
        {
            $arr_pushv['data']['platform'] = 'starcor';
            $arr_push['push']['data']['platformAbility'] = 'file';
        }
        $arr_push['result_video'] = $result_video;
        $arr_push['result_index'] = $result_index;
        return $arr_push;
    }

    public function make_media_arr($obj_dc,$nns_id,$nns_ref_id,$arr_sp_config,$arr_cp_config,$str_sp_id,$file_path)
    {
        $arr_push = array();
        $arr_push['ret'] = 0;
        $result_media = nl_vod_media_v2::query_by_id($obj_dc, $nns_ref_id);
        if($result_media['ret'] !=0)
        {
            return $result_media;
        }
        if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
        {
            nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$nns_id}'", $obj_dc->db());
            $arr_push['ret'] = 1;
            $arr_push['reason'] = "片源基础数据为空";
            return $arr_push;
        }
        $result_media = $result_media['data_info'];
        //通过sp_id获取媒资ID
        $result_media_import_info = public_model_exec::get_asset_id_by_sp_id($str_sp_id, 'media', $result_media['nns_id'],$result_media,null,'cdn');

        if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
        {
            return $result_media_import_info;
        }
        $result_media['nns_cdn_media_guid'] = $result_media_import_info['data_info'];

        if(!in_array($result_media['nns_cp_id'], array('aiqiyi')))
        {
            $result_media['nns_cdn_media_guid'] = $result_media['nns_content_id'];
        }
        //通过这个nns_vod_index_id是 nns_vod_index表中的nns_id
        $result_index = nl_vod_index::get_video_index_info_by_id($obj_dc, $result_media['nns_vod_index_id']);
        if($result_index['ret'] !=0)
        {
            return $result_index;
        }
        if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
        {
            nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$nns_id}'", $obj_dc->db());
            $arr_push['ret'] = 1;
            $arr_push['reason'] = "分集基础数据为空";
            return $arr_push;
        }
        $result_index = $result_index['data_info'][0];
        $result_index_import_info = public_model_exec::get_asset_id_by_sp_id($str_sp_id, 'index', $result_index['nns_id'],$result_index,null,'cdn');

        if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
        {
            return $result_index_import_info;
        }
        $result_index['nns_cdn_index_guid'] = $result_index_import_info['data_info'];

        $result_video = nl_vod::get_video_info_by_id($obj_dc, $result_media['nns_vod_id']);
        if($result_video['ret'] !=0)
        {
            return $result_video;
        }
        if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
        {
            nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$nns_id}'", $obj_dc->db());
            $arr_push['ret'] = 1;
            $arr_push['reason'] = "媒资基础数据为空";
            return $arr_push;
        }
        $result_video = $result_video['data_info'][0];
        $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($str_sp_id, 'video', $result_video['nns_id'],$result_video,null,'cdn');

        if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
        {
            return $result_video_import_info;
        }
        $result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
        //IP流获取cpcode+systemid
        $this->init_cp_info($obj_dc,$result_video['nns_cp_id'],$str_sp_id);

        $result_category = $this->get_category_info($obj_dc,$result_video);
        if($result_category['ret'] !=0)
        {
            nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$nns_id}'", $obj_dc->db());
            return $result_category;
        }
        $result_index['nns_index']++;       //播控保存分集号从0开始，下发是从1开始

        if(!in_array($result_video['nns_cp_id'],$this->get_all_pass_cp()))
        {
            $arr_push['ret'] = 1;
            $arr_push['reason'] = "CP不在范围内";
            return $arr_push;
        }

        $str_img_ftp = (isset($arr_sp_config['img_ftp']) && strlen($arr_sp_config['img_ftp'])>0) ? trim($arr_sp_config['img_ftp'],'/').'/' : '';
        $arr_cp_config = $this->_get_cp_info($obj_dc,$result_video['nns_cp_id'],$arr_cp_config);
        $str_modifyTime = date("Y-m-d H:i:s");
        $arr_push['push'] = array(
            'type'=>2,
            'modifyTime'=>$str_modifyTime,
            'data'=>array(
                'cp' => 'coocaa',
                'originalId' => $result_video['nns_cdn_video_guid'],
                'id' => $result_video['nns_id'],
                'name' => $result_video['nns_name'],
                'alias' => $result_video['nns_alias_name'],
                'sname' => $result_video['nns_pinyin'],
                'focus' => $result_video['nns_keyword'],
                'picV' => strlen($result_video['nns_image_v']) >0 ? $str_img_ftp.trim($result_video['nns_image_v'],'/') : '',
                'picH' => strlen($result_video['nns_image_h']) >0 ? $str_img_ftp.trim($result_video['nns_image_h'],'/') : '',
                'score' => $result_video['nns_point'],
                'playCount' => $result_video['nns_play_count'],
                'desc' => $result_video['nns_summary'],
                'seriesType' => 0,
                'cname' => $result_category['data_info']['category_name'],
                'cid' => $result_video['nns_category_id'],
                'tag' => str_replace('/', ',', $result_video['nns_kind']),
                'persons' => array(
                    'composer' => '',
                    'dubber' => '',
                    'maker' => str_replace('/', ',', $result_video['nns_screenwriter']),
                    'producer' => '',
                    'guest' => '',
                    'actor' => str_replace('/', ',', $result_video['nns_actor']),
                    'writer' => str_replace('/', ',', $result_video['nns_screenwriter']),
                    'director' => str_replace('/', ',', $result_video['nns_director']),
                ),
                'total' => $result_video['nns_all_index'],
                'publishTime' => str_replace('-', '', $result_video['nns_show_time']),
                'currCount' => $result_video['nns_new_index']+1,
                'area' => $result_video['nns_area'],
                'year' => substr(str_replace('-', '', $result_video['nns_show_time']), 0,4),
                'streams' => '高清',
                'issueTime' => $result_video['nns_create_time'],
                'contentType' => 1,
                'effective' => 1,
                'payStatus' => 0,
                'tryTime' => 0,
                'strategy' => '',
                'platform' => 'HB',
                'platformAbility' => 'INTERNET',
                'provider' => $result_video['nns_cp_id'],
                'providerName' => $arr_cp_config[$result_video['nns_cp_id']]['nns_name'],
                'episodes' =>array(
                    array(
                        'originalId'=>$result_index['nns_cdn_index_guid'],
                        'id'=>$result_index['nns_id'],
                        'epId' => $result_index['nns_cdn_index_guid'],
                        'epName' => "{$result_video['nns_name']}第{$result_index['nns_index']}集",
                        'epSname' => "{$result_video['nns_name']}第{$result_index['nns_index']}集",
                        'epFocus' => $result_index['nns_summary'],
                        'persons' => array(
                            'actor' => str_replace('/', ',', $result_index['nns_actor']),
                            'writer' => str_replace('/', ',', $result_index['nns_director']),
                        ),
                        'length' => $result_index['nns_time_len'],
                        'period' => '',
                        'order' => $result_index['nns_index'],
                        'epType' => 0,
                        'pic' => strlen($result_index['nns_image']) >0 ? $str_img_ftp.trim($result_index['nns_image'],'/') : '',
                        'desc' => $result_index['nns_summary'],
                        'isPay' => 0,
                        'tryTime' => 0,
                        'effective' => 1,
                        'mediaUrl' => '',
                        'mediaoriginalId'=>$result_media['nns_id'],
                        'mediaid'=>$result_media['nns_id'],
                        'issueTime'=>$result_media['nns_modify_time'],  //最新修改时间
                        'mediaMode'=>$result_media['nns_mode'],      //清晰度
                        'mediaKbps'=>$result_media['nns_kbps'],     //视频码率
                        'mediaResolution'=>$result_media['nns_file_resolution'],        //分辨率
                    ),
                ),
            )
        );
        $arr_push['reasult_video'] = $result_video;
        $arr_push['reasult_index'] = $result_index;
        $arr_push['reasult_media'] = $result_media;
        /*
         * 下面是根据不同的类型媒资进行原始ID等参数修正
         * song.wang 2018-07-30
         */
        //1、爱奇艺、优酷、腾讯等互联网媒资
        if(in_array($result_video['nns_cp_id'],$this->get_all_pass_cp('INTERNET')))
        {
            $media_url_info = explode('_',$result_media['nns_url']);
            $arr_push['push']['data']['episodes'][0]['originalId'] = $media_url_info['2'];          //分集原始ID
            $arr_push['push']['data']['episodes'][0]['mediaoriginalId'] = $media_url_info['3'];     //片源原始ID
        }
        //2、播控中心OTT内容   IP流
        else if(in_array($result_video['nns_cp_id'],$this->get_all_pass_cp('IP')))
        {
            if(!$this->get_sp_c2_state('IP', $result_video['nns_cp_id'], $result_media, $obj_dc))
            {
                #TODO
                return array(
                    'ret'=>1,
                    'reason'=>'注入CDN还未成功',
                );
            }
            if(strlen($result_media['nns_content_id'])<1)
            {
                return array(
                    'ret'=>1,
                    'reason'=>'nns_content_id 为空',
                );
            }
            $arr_push['push']['data']['platformAbility'] = 'IP';            //平台标识
            $arr_push['push']['data']['episodes'][0]['mediaoriginalId'] = $result_media['nns_content_id'];      //片源原始ID
            $arr_push['push']['data']['episodes'][0]['CPcode'] =  $this->str_cmsid;             //CPcode
            $arr_push['push']['data']['episodes'][0]['SystemID'] =  $this->str_target_system_id;    //SystemID
        }
        //3、播控中心VOD内容   IPQAM
        else if(in_array($result_video['nns_cp_id'],$this->get_all_pass_cp('IPQAM')))
        {
            if(!$this->get_sp_c2_state('IPQAM', $result_video['nns_cp_id'], $result_media, $obj_dc))
            {
                #TODO
                return array(
                    'ret'=>1,
                    'reason'=>'注入CDN还未成功',
                );
            }
            if(strlen($result_media['nns_content_id'])<1)
            {
                return array(
                    'ret'=>1,
                    'reason'=>'nns_content_id 为空',
                );
            }
            $arr_push['push']['data']['platformAbility'] = 'IPQAM';            //平台标识
            $arr_push['push']['data']['episodes'][0]['mediaoriginalId'] = $result_media['nns_content_id'];      //片源原始ID
        }
        else if(in_array($result_video['nns_cp_id'], $this->get_all_pass_cp('file')))
        {
            //4、文件类型
            $arr_pushv['data']['platform'] = 'starcor';
            $arr_push['push']['data']['platformAbility'] = 'file';
            if((int)$arr_sp_config['disabled_clip'] !== 2 && (int)$arr_sp_config['disabled_clip'] !== 3)
            {
                $result_media['nns_url'] = ltrim((trim($file_path)),'/');
            }
            else
            {
                $result_media['nns_url'] = ltrim((trim($result_media['nns_url'])),'/');
            }
            if(isset($arr_sp_config['media_ftp']) && !empty($arr_sp_config['media_ftp']))
            {
                $arr_sp_config['media_ftp'] = rtrim((trim($arr_sp_config['media_ftp'])), '/');
                if (stripos($result_media['nns_url'], 'http://') === FALSE && stripos($result_media['nns_url'], 'ftp://') === FALSE)
                {
                    $result_media['nns_url'] = $arr_sp_config['media_ftp'] . '/' . $result_media['nns_url'];
                }
            }
            $arr_push['push']['data']['episodes'][0]['mediaUrl'] = $result_media['nns_url'];
        }
        return $arr_push;
    }

    public function get_category_info($obj_dc,$result_video)
    {
        $result_depot = nl_depot::get_depot_info($obj_dc,array('nns_id'=>$result_video['nns_depot_id']),'video');
        if($result_depot['ret'] !=0 || !isset($result_depot['data_info'][0]) || empty($result_depot['data_info'][0]) || !is_array($result_depot['data_info'][0]))
        {
            $result_depot['ret']=1;
            return $result_depot;
        }
        $result_depot = $result_depot['data_info'][0];
        $dom = new DOMDocument('1.0', 'utf-8');
        $depot_info = $result_depot['nns_category'];
        if(!$this->is_xml($depot_info))
        {
            return public_model_exec::return_data(1,"error");
        }
        $nns_category_id = $result_video['nns_category_id'];
        $dom->loadXML($depot_info);
        $categorys = $dom->getElementsByTagName('category');
        $category_name=$result_video['nns_all_index'] >1 ? "电视剧" : "电影";
        foreach ($categorys as $category)
        {
            if ((string)$category->getAttribute('id') == (string)$nns_category_id)
            {
                $category_name = $category->getAttribute('name');
                break;
            }
        }
        return public_model_exec::return_data(0,"ok",array('category_name'=>$category_name));
    }

    public function is_xml($string)
    {
        if(!is_string($string))
        {
            return false;
        }
        $string = strlen($string) < 1 ? '' : trim($string);
        if(strlen($string) <1)
        {
            return false;
        }
        $xml_parser = xml_parser_create();
        if (! xml_parse($xml_parser, $string, true))
        {
            xml_parser_free($xml_parser);
            return false;
        }
        return true;
    }

    public function _get_cp_info($obj_dc,$cp_id,$arr_cp_config)
    {
        if(strlen($cp_id) <1)
        {
            return false;
        }
        if(isset($arr_cp_config[$cp_id]))
        {
            return true;
        }
        $result_cp = nl_cp::query_by_id($obj_dc,$cp_id);
        if($result_cp['ret'] !=0)
        {
            return false;
        }
        if(!isset($result_cp['data_info']) || !is_array($result_cp['data_info']) || empty($result_cp['data_info']))
        {
            $arr_cp_config[$cp_id] = null;
            return true;
        }
        if($this->is_json($result_cp['data_info']['nns_config']))
        {
            $result_cp['data_info']['nns_config'] = json_decode($result_cp['data_info']['nns_config'],true);
        }
        $arr_cp_config[$cp_id]=$result_cp['data_info'];
        return $arr_cp_config;
    }

    public function is_json($string)
    {
        if(!is_string($string))
        {
            return false;
        }
        $string = strlen($string) <1 ? '' : $string;
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}