<?php
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
$arr_dir = explode('|', str_replace(array ('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/' . ORG_ID . '/init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/' . ORG_ID . '/define.php';
$ip = i_get_ip();
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------BK到CDN消息反馈开始--------------', ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '执行文件:' . __FILE__, ORG_ID);
$postdata = file_get_contents("php://input");
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '收到来自CDN的消息,IP:' . $ip . ';收到来自CDN的内容:' . $postdata, ORG_ID);
class huawei_notify
{
	public function ContentDeployResult($CMSID, $SOPID, $CorrelateID, $ResultCode, $ErrorDescription, $ResultFileURL)
	{
		$arg_list = func_get_args();
		$c2_notify = array (
				'nns_id' => $CorrelateID, 
				'nns_notify_result_url' => $ResultFileURL, 
				'nns_notify_result' => $ResultCode
		);
		c2_task_model::save_c2_notify($c2_notify);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '方法参数：' . var_export($arg_list, true), ORG_ID);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '修改C2表参数：' . var_export($c2_notify, true), ORG_ID);
		return array (
				'ResultCode' => 0, 
				'ErrorDescription' => 'notify ok'
		);
	}
}
//ob_start();
$server = new SOAPServer('ContentDeployResult.wsdl');
$server->setClass('huawei_notify');
$server->handle();