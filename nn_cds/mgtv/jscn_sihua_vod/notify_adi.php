<?php
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('display_errors', 0);
set_time_limit(0);
$arr_dir = explode('|', str_replace(array ('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/sp/sp.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/models/public_model_exec.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/models/video_model.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/c2_log/c2_log.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/c2_task/c2_task.class.php';
$ip = i_get_ip();
$postdata = file_get_contents("php://input");

// $postdata = '<?xml version = "1.0" encoding="utf-8"? >
// <SyncContentsResult  Time_Stamp="2012-09-11 20:00:00" System_ID="ICMSxxxxxx">  
// 	<Assets>
// 		<Asset ID="资产ID"  Current_ID="15035715362883497889" Type="Movie"  Status="0" Desc="ok">
// </Asset>
// 	</Assets>
// </SyncContentsResult>';


// $postdata = <<<XML
// <?xml version="1.0" encoding="UTF-8"? ><ADI><Metadata><AMS Verb="" Asset_Class="packages" Asset_ID="testcms0PAPT01810700" Asset_Name="女乒世界杯：丁宁、朱雨玲会师决赛 中国队提前锁定女单冠亚军" Provider_ID="testcms"  Provider="testcms" Creation_Date="2018-10-25" Description="女乒世界杯半决赛，丁宁4:0石川佳纯，朱雨玲4：1郑怡静，丁宁、朱雨玲会师决赛，中国队提前锁定女单冠亚军。" Version_Major="1"  Version_Minor="0" Product="MOD"/><App_Data Value="CableLabsVOD 1.1" Name="Metadata_Spec_Version" App="MOD"/></Metadata><Asset><Metadata><AMS Verb="" Asset_Class="title" Asset_ID="testcms0PA0000181070" Asset_Name="女乒世界杯：丁宁、朱雨玲会师决赛 中国队提前锁定女单冠亚军" Provider_ID="testcms"  Provider="testcms" Creation_Date="2018-10-25" Description="女乒世界杯半决赛，丁宁4:0石川佳纯，朱雨玲4：1郑怡静，丁宁、朱雨玲会师决赛，中国队提前锁定女单冠亚军。" Version_Major="1"  Version_Minor="0" Product="MOD"/><App_Data Value="program" Name="Show_Type" App="MOD"/><App_Data Value="0" Name="Issue_Number" App="MOD"/><App_Data Value="testcms0PA0000181070" Name="Original_Asset_ID" App="MOD"/><App_Data Value="1" Name="Original_System_ID" App="MOD"/><App_Data Value="1" Name="License_Type" App="MOD"/><App_Data Value="Culture" Name="Genre" App="MOD"/><App_Data Value="女乒世界杯：丁宁、朱雨玲会师决赛 中国队提前锁定女单冠亚军" Name="Proper_Title" App="MOD"/><App_Data Value="女乒世界杯：丁宁、朱雨玲会师决赛 中国队提前锁定女单冠亚军" Name="Parallel_Proper_Title" App="MOD"/><App_Data Value="女乒世界杯：丁宁、朱雨玲会师决赛 中国队提前锁定女单冠亚军" Name="Subordinate_Title" App="MOD"/><App_Data Value="女乒世界杯：丁宁、朱雨玲会师决赛 中国队提前锁定女单冠亚军" Name="Alternative_ Title" App="MOD"/><App_Data Value="女乒世界杯：丁宁、朱雨玲会师决赛 中国队提前锁定女单冠亚军" Name="Title_Description" App="MOD"/><App_Data Value="11" Name="Version_Description" App="MOD"/><App_Data Value="" Name="Key_Words" App="MOD"/><App_Data Value="女乒世界杯半决赛，丁宁4:0石川佳纯，朱雨玲4：1郑怡静，丁宁、朱雨玲会师决赛，中国队提前锁定女单冠亚军。" Name="Description" App="MOD"/><App_Data Value="" Name="Impressive_Dialogue" App="MOD"/><App_Data Value="" Name="Impressive_Plot" App="MOD"/><App_Data Value="" Name="Hot_Comments" App="MOD"/><App_Data Value="" Name="Sentence_Review" App="MOD"/><App_Data Value="" Name="Behind_Scenes" App="MOD"/><App_Data Value="6.8" Name="IMDB_Rating" App="MOD"/><App_Data Value="G" Name="MPAA_Rating" App="MOD"/><App_Data Value="Y" Name="Season_Premiere" App="MOD"/><App_Data Value="N" Name="Season_Finale" App="MOD"/><App_Data Value="00:00:00" Name="Preview_Period" App="MOD"/><App_Data Value="5" Name="Asset_Recommend_Class" App="MOD"/><App_Data Value="0" Name="Chapter" App="MOD"/><App_Data Value="2018" Name="Year" App="MOD"/><App_Data Value="0.0" Name="Suggested_Price" App="MOD"/><App_Data Value="Adult" Name="Audience" App="MOD"/><App_Data Value="2056-01-01 20:00:00" Name="Subscriber_View_Limit" App="MOD"/><App_Data Value="" Name="Region" App="MOD"/></Metadata><Asset><Metadata><AMS Verb="" Asset_Class="language" Asset_ID="testcms0LAPT01810700" Asset_Name="女乒世界杯：丁宁、朱雨玲会师决赛 中国队提前锁定女单冠亚军" Provider_ID="testcms"  Provider="testcms" Creation_Date="2018-10-25" Description="女乒世界杯：丁宁、朱雨玲会师决赛 中国队提前锁定女单冠亚军" Version_Major="1"  Version_Minor="0" Product="MOD"/><App_Data Value="zh" Name="Type" App="MOD"/><App_Data Value="CH" Name="Description" App="MOD"/><App_Data Value="zh_CN" Name="Tag" App="MOD"/><App_Data Value="99" Name="Regin" App="MOD"/><App_Data Value="南京一中" Name="Producers" App="MOD"/><App_Data Value="" Name="Actors" App="MOD"/><App_Data Value="" Name="Director" App="MOD"/><App_Data Value="女乒世界杯：丁宁、朱雨玲会师决赛 中国队提前锁定女单冠亚军" Name="Title" App="MOD"/><App_Data Value="女乒世界杯：丁宁、朱雨玲会师决赛 中国队提前锁定女单冠亚军" Name="Title_Brief" App="MOD"/></Metadata></Asset><Asset><Metadata><AMS Verb="" Asset_Class="movie" Asset_ID="testcms0MV0001429478" Asset_Name="女乒世界杯：丁宁、朱雨玲会师决赛 中国队提前锁定女单冠亚军第1集" Provider_ID="testcms"  Provider="testcms" Creation_Date="2018-10-25" Description="女乒世界杯：丁宁、朱雨玲会师决赛 中国队提前锁定女单冠亚军第1集" Version_Major="1"  Version_Minor="0" Product="MOD"/><App_Data Value="testcms0MV0001429478" Name="Original_Asset_ID" App="MOD"/><App_Data Value="1" Name="Original_System_ID" App="MOD"/><App_Data Value="2" Name="Service_Type" App="MOD"/><App_Data Value="1" Name="Screen_Format" App="MOD"/><App_Data Value="Y" Name="HD_Content" App="MOD"/><App_Data Value="" Name="3D_Content" App="MOD"/><App_Data Value="2" Name="Video_Bit_Rate" App="MOD"/><App_Data Value="1080" Name="Frame_Height" App="MOD"/><App_Data Value="1920" Name="Frame_Width" App="MOD"/><App_Data Value="N" Name="Encryption" App="MOD"/><App_Data Value="10:42:19" Name="Run_Time" App="MOD"/><App_Data Value="7002000" Name="Audio_Bit_Rate" App="MOD"/><App_Data Value="313264f1a2a553d1443ba90d4b1b940d" Name="Content_Check_Sum" App="MOD"/><App_Data Value="1" Name="Security_Classification" App="MOD"/><App_Data Value="" Name="Remarks" App="MOD"/><App_Data Value="235409840" Name="Content_File_Size" App="MOD"/><App_Data Value="1" Name="Encryption_Reason" App="MOD"/><App_Data Value="" Name="Audio_Language" App="MOD"/><App_Data Value="zh_CN" Name="Subtitle_Language" App="MOD"/><App_Data Value="" Name="Audio_Bit_Depth" App="MOD"/><App_Data Value="" Name="Audio_Sampling_Frequency" App="MOD"/><App_Data Value="0" Name="Audio_Type" App="MOD"/><App_Data Value="" Name="Audio_Coding_Format" App="MOD"/><App_Data Value="" Name="Video_Sampling_Type" App="MOD"/><App_Data Value="" Name="Video_Coding_Format" App="MOD"/><App_Data Value="PAL" Name="System" App="MOD"/><App_Data Value="" Name="Aspect_Ratio" App="MOD"/><App_Data Value="5" Name="Video_Quality" App="MOD"/><App_Data Value="1" Name="Audio_Channel_Format" App="MOD"/><App_Data Value="1" Name="Color" App="MOD"/><App_Data Value="" Name="Starting_Point" App="MOD"/><App_Data Value="HD" Name="MediaMode" App="MOD"/><App_Data Value="4K" Name="FileType" App="MOD"/></Metadata><Content Value="ftp://fuse_starcor:fuse_starcor@172.26.2.18:21/local_cache/mgtv/20181023/5bcee47b2a56c6eaa6a1a3b6927bce49/57b1bb90cea03b6810feb6542a1ce3b0.ts"/></Asset></Asset></ADI>
// XML;

$file_init_dir = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/' . ORG_ID . '/init.php';
$file_init_define = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/' . ORG_ID . '/define.php';
if(!file_exists($file_init_define) || !file_exists($file_init_dir))
{
	nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '初始化文件不存在:[' . $file_init_dir . ']['.$file_init_define.']', 'public');
	echo "1|inner error";
	exit();
}
include_once $file_init_dir;
include_once $file_init_define;
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------CDN到BK消息ADI反馈开始--------------', ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '执行文件:' . __FILE__, ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '收到来自CDN的消息为空,IP:' . $ip . ';收到来自CDN的内容:' . $postdata, 'public');
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '收到来自CDN的消息为空,IP:' . $ip . ';收到来自CDN的POST内容:' . var_export($_REQUEST,true), 'public');
class notify_adi
{
	public function ContentDeployResult($CMSID, $SOPID, $CorrelateID, $ResultCode,$ErrorDescription,$ResultContent,$media_id)
	{
	    $result_task_id = $this->get_source_id($media_id);
	    $arg_list = func_get_args();
	    nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '方法参数：' . var_export($arg_list, true), ORG_ID);
	    if($result_task_id !== false)
	    {
    		$c2_notify = array (
    			'nns_id' => $result_task_id, 
    			'nns_notify_content' => $ResultContent, 
    			'nns_notify_result' => $ResultCode,
    	        'nns_error_description'=> $ErrorDescription,
    	        'nns_notify_result_url'=>'',
    		    'nns_notify_PlayId' => $media_id,
    		);
    		$result = c2_task_model::save_c2_notify_v2($c2_notify);
    		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '修改C2表参数：' . var_export($c2_notify, true), ORG_ID);
    		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . 'log结果：' . var_export($result, true), ORG_ID);
	    }
	    else 
	    {
	        $result = array('ret'=>0,'reason'=>'ok');
	    }
	    return $result;
	}

	/**
	 * 获取原始日志ID
	 * @param unknown $media_id
	 */
	public function get_source_id($media_id)
	{
	    $dc = nl_get_dc(array (
                'db_policy' => NL_DB_WRITE,
                'cache_policy' => NP_KV_CACHE_TYPE_NULL
            ));
	    $result = public_model_exec::get_asset_id_by_sp_import_id(ORG_ID, 'media', $media_id);
	    if(!isset($result['data_info']['data_info']) || !is_array($result['data_info']['data_info']) || empty($result['data_info']['data_info']))
	    {
	        nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '通过片源注入ID反查片源信息失败：' . var_export($result, true), ORG_ID);
	        return false;
	    }
	    $result = $result['data_info']['data_info'];
	    $result_c2_task = nl_c2_task::query_c2_exsist($dc, 'media', $result['nns_id'], ORG_ID);
	    if(!isset($result_c2_task['data_info'][0]) || empty($result_c2_task['data_info'][0]) || !is_array($result_c2_task['data_info'][0]))
	    {
	        nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '通过片源查询C2_task表信息失败：' . var_export($result_c2_task, true), ORG_ID);
	        return false;
	    }
	    $result_c2_task = $result_c2_task['data_info'][0];
	    $result_c2_task_log = nl_c2_log::query_by_task_id($dc, $result_c2_task['nns_id']);
	    if(!isset($result_c2_task_log['data_info'][0]) || empty($result_c2_task_log['data_info'][0]) || !is_array($result_c2_task_log['data_info'][0]))
	    {
	        nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '通过片源查询C2_task_log表信息失败：' . var_export($result_c2_task_log, true), ORG_ID);
	        return false;
	    }
	    $result_c2_task_log = $result_c2_task_log['data_info'][0];
	    return (isset($result_c2_task_log['nns_id']) && strlen($result_c2_task_log['nns_id']) >0) ? $result_c2_task_log['nns_id'] : false;
	}
}
$dom = new DOMDocument('1.0', 'utf-8');
$dom->loadXML($postdata);
$AMS = $dom->getElementsByTagName('AMS');
if($AMS->length <1)
{
    echo "1|no data params";
    exit();
}
$Asset_ID = '';
foreach ($AMS as $AMS_val)
{
    $Asset_Class = $AMS_val->getAttribute('Asset_Class');
    if(strlen($Asset_Class) <1)
    {
        continue;
    }
    if($Asset_Class == 'movie')
    {
        $Asset_ID = $AMS_val->getAttribute('Asset_ID');
        break;
    }
}
if(strlen($Asset_ID) <1)
{
    nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '初始化文件不存在:[' . $file_init_dir . ']['.$file_init_define.']', 'public');
    echo "1|Asset_ID empty";
    exit();
}
$obj_notify = new notify_adi();
$result = $obj_notify->ContentDeployResult('starcor', ORG_ID, $Asset_ID, 0, "ok", $postdata,$Asset_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '处理结果:'.var_export($result,true), ORG_ID);
unset($obj_notify);
$obj_notify = null;
echo $result['ret'] =='0' ?  "0|ok" : "1|error";
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------CDN到BK消息反馈结束--------------', ORG_ID);