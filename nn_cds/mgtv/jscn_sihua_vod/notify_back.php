<?php
header('Content-Type: text/xml; charset=utf-8');
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('display_errors', 0);
set_time_limit(0);
$arr_dir = explode('|', str_replace(array ('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/sp/sp.class.php';
$ip = i_get_ip();
$postdata = file_get_contents("php://input");
$file_init_dir = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/' . ORG_ID . '/init.php';
$file_init_define = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/' . ORG_ID . '/define.php';
if(!file_exists($file_init_define) || !file_exists($file_init_dir))
{
	nn_log::write_log_message(LOG_MODEL_BK_CDN_DELIVERY_NOTIFY, '初始化文件不存在:[' . $file_init_dir . ']['.$file_init_define.']', 'public');
	echo '<?xml version="1.0" encoding="UTF-8" ?><response><errorcode>1</errorcode><errormsg>内部错误</errormsg></response>';
	exit();
}
include_once $file_init_dir;
include_once $file_init_define;
nn_log::write_log_message(LOG_MODEL_BK_CDN_DELIVERY_NOTIFY, '--------------CDN到BK消息反馈开始--------------', ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_DELIVERY_NOTIFY, '执行文件:' . __FILE__, ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_DELIVERY_NOTIFY, '收到来自CDN的消息为空,IP:' . $ip . ';收到来自CDN的内容:' . $postdata, ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_DELIVERY_NOTIFY, '收到来自CDN的消息为空,IP:' . $ip . ';收到来自CDN的POST内容:' . var_export($_REQUEST,true), ORG_ID);
class notify_adi
{
    /**
     * 传入的参数
     * @var unknown
     */
    private $mix_params = null;
    
    public $obj_dc = null;
    
    /**
     * 初始化数据
     * @param string $params
     */
    public function __construct($params=null)
    {
        $this->mix_params = $params=== null ? '' : $params;
        $this->obj_dc = nl_get_dc(array (
            "db_policy" => NL_DB_WRITE,
            "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
        ));
    }
    
    /**
     * 消息反馈执行的方法
     */
    public function notify()
    {
        $asseturl = isset($_REQUEST['asseturl']) ? $_REQUEST['asseturl'] : '';
        $arr_asseturl = explode('/', $_REQUEST['asseturl']);
        if(!is_array($arr_asseturl) || empty($arr_asseturl))
        {
            return array('ret'=>1,'查无数据');
        }
        array_pop($arr_asseturl);
        $asset_id = array_pop($arr_asseturl);
        if(strlen($asset_id) !=20)
        {
            return array('ret'=>1,'查无数据2');
        }
        $result_c2_log = nl_c2_log::query_data_by_id($this->obj_dc, $asset_id);
        if($result_c2_log['ret'] !=0 || !isset($result_c2_log['data_info']) || empty($result_c2_log['data_info']) || !is_array($result_c2_log['data_info']))
        {
            nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '查询日志信息sql失败或者无数据'.var_export($result_c2_log,true), ORG_ID);
            nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------BK到CDN消息反馈结束--------------', ORG_ID);
            return array('ret'=>1,'查无数据1');
        }
        $result_c2_log = $result_c2_log['data_info'];
        nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '修改C2_log表参数：' . var_export($result_c2_log, true), ORG_ID);
        $result_edit_c2_task = nl_c2_task::edit($this->obj_dc, array('nns_status'=>0,), $result_c2_log['nns_task_id']);
        nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '修改C2_task表参数：' . var_export($result_edit_c2_task, true), ORG_ID);
        
        
        $result_query_c2_task = nl_c2_task::query_by_id($this->obj_dc, $result_c2_log['nns_task_id']);
        if(isset($result_query_c2_task['data_info']) && is_array($result_query_c2_task['data_info']) && !empty($result_query_c2_task['data_info']))
        {
            $sql_serice="select * from nns_vod_media where nns_id='{$result_query_c2_task['data_info']['nns_ref_id']}' and nns_cp_id='{$result_query_c2_task['data_info']['nns_cp_id']}' limit 1";
            $result_serice = nl_query_by_db($sql_serice,$this->obj_dc->db());
            if(isset($result_serice[0]) && is_array($result_serice[0]) && !empty($result_serice[0]))
            {
                $result_serice[0]['nns_content_id']=$asset_id;
                $result_serice[0]['nns_import_id']=$result_serice[0]['nns_import_id'];
//                 $result_serice[0]['nns_cp_id']="JSCN";
                $sp_config = array(
                    'site_callback_url'=> 'ftp://source_notify:source_notify@172.26.2.17',
                );
                $result_c2_task_info = array(
                    'nns_message_id'=>$result_query_c2_task['data_info']['nns_message_id'],
                );
                $str_action = $result_serice[0]['nns_deleted'] == '1' ? 'delete' : 'add';
                $result = c2_task_model::notify_upstream($result_serice[0], $str_action, array('ret'=>0,'reason'=>'ok'), $sp_config, $result_c2_task_info, $this->obj_dc,'media','',true);
            }
        }
        if($result_edit_c2_task['ret'] == '0')
        {
            $queue_task_model = new queue_task_model();
            $queue_task_model->q_report_task($result_c2_log['nns_task_id']);
            unset($queue_task_model);
            $queue_task_model=null;
        }
        return array('ret'=>0,'reason'=>'元数据同步成功');
    }
}
$notify_adi = new notify_adi();
$result = $notify_adi->notify();
$str='<?xml version="1.0" encoding="UTF-8" ?><response><errorcode>'.$result['ret'].'</errorcode><errormsg>'.$result['reason'].'</errormsg></response>';
echo $str;
nn_log::write_log_message(LOG_MODEL_BK_CDN_DELIVERY_NOTIFY, '--------------CDN到BK消息反馈结束--------------', ORG_ID);