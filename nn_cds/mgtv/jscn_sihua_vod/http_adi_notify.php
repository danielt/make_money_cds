<?php
set_time_limit(0);
ini_set('display_errors', 0);
header("Content-type:text/xml;charset=utf-8");
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
$ip = i_get_ip();
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
$file_init_dir = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/'.ORG_ID.'/init.php';
if(!file_exists($file_init_dir))
{
	nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '初始化文件不存在:[' . $file_init_dir . ']', ORG_ID);
	exit();
}
include_once $file_init_dir;
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/c2_task/c2_task.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/c2_log/c2_log.class.php';
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------BK到CDN消息反馈开始--------------', ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '执行文件:' . __FILE__, ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '收到来自CDN的消息,IP:' . $ip . ';收到来自CDN的内容:' . var_export($_REQUEST,true), ORG_ID);
class http_adi_notify extends nn_public_return
{
    /**
     * 传入的参数
     * @var unknown
     */
    private $mix_params = null;
    
    public $obj_dc = null;
    
    /**
     * 初始化数据
     * @param string $params
     */
    public function __construct($params=null)
    {
        $this->mix_params = $params=== null ? '' : $params;
        $this->obj_dc = nl_get_dc(array (
            "db_policy" => NL_DB_WRITE,
            "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
        ));
    }
    
    /**
     * 消息反馈执行的方法
     */
	public function notify()
	{
        if(!isset($this->mix_params['adicontent']) || strlen($this->mix_params['adicontent']) <1 || !$this->is_xml($this->mix_params['adicontent']))
        {
            $result_edit_c2_log = $this->__return_data(1,'error');
            return $this->make_return_data($result_edit_c2_log);
        }
	    $dom = new DOMDocument('1.0', 'utf-8');
        $dom->loadXML($this->mix_params['adicontent']);
        $AMS = $dom->getElementsByTagName('AMS');
        if($AMS->length <1)
        {
            return ;
        }
        $str_asset_id = $str_log_id='';
        foreach ($AMS as $AMS_val)
        {
            $Asset_Class = $AMS_val->getAttribute('Asset_Class');
            $Asset_Class = ($Asset_Class === null || strlen($Asset_Class)<1) ? '' : strtolower($Asset_Class);
            if(strlen($Asset_Class) <1 || ($Asset_Class != 'package' && $Asset_Class != 'movie'))
            {
                continue;
            }
            $Asset_ID = $AMS_val->getAttribute('Asset_ID');
            $Asset_ID = ($Asset_ID === null || strlen($Asset_ID)<1) ? '' : $Asset_ID;
            if(strlen($Asset_ID) <1)
            {
                continue;
            }
            if($Asset_Class == 'package' && strlen($str_log_id) <1)
            {
                $str_log_id  = $Asset_ID;
            }
            else if($Asset_Class == 'movie' && strlen($str_asset_id) <1)
            {
                $str_asset_id  = $Asset_ID;
            }
            if(strlen($str_log_id) >0 && strlen($str_asset_id)>0)
            {
                break;
            }
        }
        if(strlen($str_asset_id)<1 || strlen($str_log_id)<1 )
        {
            $result_edit_c2_log = $this->__return_data(1,'error');
            return $this->make_return_data($result_edit_c2_log);
        }
        $c2_notify = array (
            'nns_id' => $str_log_id,
            'nns_notify_content' => $this->mix_params['adicontent'],
            'nns_notify_result' => 0,
            'nns_error_description'=> 'ok',
            'nns_notify_result_url'=>'',
            'nns_notify_PlayId' => $str_asset_id,
        );
        $result = c2_task_model::save_c2_notify_v2($c2_notify);
        nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '修改C2表参数：' . var_export($c2_notify, true), ORG_ID);
        nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . 'log结果：' . var_export($result, true), ORG_ID);
		$result_edit_c2_log = $this->__return_data(0,'success');
		return $this->make_return_data($result_edit_c2_log);
	}
	
	/**
	 * 生成xml
	 * @param unknown $result
	 */
	private function make_return_data($result)
	{
	    $this->obj_dom = new DOMDocument("1.0", 'utf-8');
	    $syncInjStatus = $this->_make_xml('syncInjStatus');
	    $this->_make_xml('errorcode',$result['ret'],null,$syncInjStatus);
	    $this->_make_xml('errormsg',$result['reason'],null,$syncInjStatus);
	    return $this->obj_dom->saveXML();
	}
}
$obj_http_notify = new http_adi_notify($_REQUEST);
$result = $obj_http_notify->notify();
echo $result;