<?php
ini_set('display_errors',1);
set_time_limit(0);
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/mgtv_init.php';
define('ORG_ID', 'xjcbc_dx_hw');
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/'.ORG_ID.'/init.php';
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/'.ORG_ID.'/define.php';
$postdata = file_get_contents("php://input");
if(strpos($postdata, 'soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"') === false)
{
	$url = 'http://218.31.255.68/nn_starcor/mgtv/xjcbc_dx_hw/hw_notify.php?wsdl';//接收XML地址
	header("Content-type:text/xml;charset=utf-8");
	$header[] = 'Content-type: text/xml;charset=UTF-8';//定义content-type为xml
	i_write_log_core('notify exchange before '.$postdata,ORG_ID.'/c2notify');
	$postdata = str_replace('soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/http"', 'soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"', $postdata);
	$ch = curl_init(); //初始化curl
	curl_setopt($ch, CURLOPT_URL, $url);//设置链接
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//设置是否返回信息
	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);//设置HTTP头
	curl_setopt($ch, CURLOPT_POST, 1);//设置为POST方式
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);//POST数据
	$response = curl_exec($ch);//接收返回信息
	echo $response;die;
}
ini_set("soap.wsdl_cache_enabled", "0");
header("Content-type:text/xml;charset=utf-8");
$ip = i_get_ip();
i_write_log_core('notify from '.$ip,ORG_ID.'/c2notify');
//$postdata = file_get_contents("php://input");
i_write_log_core('notify data '.$postdata,ORG_ID.'/c2notify');
class hw_notify{
	public function ResultNotify($CSPID,$LSPID,$CorrelateID,$CmdResult,$ResultFileURL){
		$arg_list = func_get_args();

		if($CmdResult!='0')
		{
			$CmdResult = '-1';
		}

		$c2_notify = array(
				'nns_id'=>$CorrelateID,
				'nns_notify_result_url'=>$ResultFileURL,
				'nns_notify_result'=>$CmdResult,
		);
		c2_task_model::save_c2_notify($c2_notify);
		i_write_log_core('ResultNotify params:'.var_export($arg_list,true),ORG_ID.'/c2notify');
		i_write_log_core('ResultNotify params:'.var_export($c2_notify,true),ORG_ID.'/c2notify');
		return array('Result'=>0,'ErrorDescription'=>'notify ok');
	}
}
//ob_start();
$server = new SOAPServer('hw_notify.wsdl');
$server->setClass('hw_notify');
$server->handle();
