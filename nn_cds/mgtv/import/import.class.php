<?php
/*
 * 
 ***************本文档实现以下方法************
 1、媒资信息注入  ，注入接口包含修改媒资
 2、媒体信息删除
 3、媒资分集注入
 4、媒资分集删除
 5、媒资片源注入
 6、媒资片源删除
 * 
 * 返回状态码
 * 自定义状态码
 * 10000    未定义CURL封装类
 * 10001	必要参数不能为空
 * 10002	CURL调用接口失败
 * 
 * 接口返回状态码
 * 0      	成功
 * 非0 		失败，详情参考媒资注入接口文档
 */
//页面设置
date_default_timezone_set('PRC'); 
define("IMPORT_LOG", TRUE);
ini_set("max_execution_time", '0');

//日志类
include_once 'log.class.php';

class import
{
	
	private $import_url = null;   				//媒资注入接口地址
	private $log        = null;   				//媒资注入日志
	private $curl       = null;					//CURL 封装类对象
	private $result     = array();    			//接口处理结果,返回状态   
	private $log_file   = null;
	
	/**
	 * $import_url 
	 * $import_log
	 * $interface_type
	 * $curl_path                           //NP库 CURL路径
	 * 
	 */
	public function __construct($import_url,$log_path='.')
	{
		$this->import_url = $import_url;
		$this->log_file = $log_path.DIRECTORY_SEPARATOR.date('Y_m_d').'.txt';
		
		//CURL 实例化
		$curl_path = dirname(dirname(dirname(dirname(__FILE__)))).'/np/np_http_curl.class.php';
		//$curl_path = dirname(__FILE__).'/np_http_curl.class.php';
		if(file_exists($curl_path))
		{
			include_once $curl_path;
			$this->curl = new np_http_curl_class();
		}else{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:s:i')."[未定义CURL封装类]\n";
				$this->log->_write($this->log_file,$err_msg);
			
			}
			$this->result  = array(
								'code' => 10000,
			            		'reason' => '未定义CURL封装类',
			            		"id" => ''
							);
			return $this->return_xml($this->result);
		}
		//LOG 实例化
		$this->log = new nl_log($log_path,'cms_api','import');
		//$this->log_file = $this->log->path.DIRECTORY_SEPARATOR.date('Y_m_d').'.log';
	}
	
	/**
	 * 媒资信息注入
	 * $assets_xml 媒资信息XML结构
	 * 
	 */
	public function import_assets($assets_xml)
	{
		$asset_info = json_decode(json_encode(@simplexml_load_string($assets_xml)),true);
		//检查必要参数
		$check = $this->assets_info_check_params($asset_info);

		if($check === false)
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:s:i').'[媒资注入开始,接口地址]:'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[错误码]：'.$this->result['code']."\n"
						  .date('Y-m-d H:s:i')."[错误原因]：".$this->result['reason']."\n"
						  .date('Y-m-d H:s:i')."[媒资来源ID]：".$this->result['data']."\n\n";
				$this->log->_write($this->log_file,$err_msg);
			}
			
			return $this->return_xml($this->result);
		}
		
		//媒资注入接口数据结构
		$data['func'] = 'import_assets';
		$data['assets_content']     = $assets_xml;
		
		
		for ($i = 0; $i < 2; $i++) {
        	//访问媒资注入接口
			$asset_ret = $this->curl->post($this->import_url,$data);		
			//判断CURL状态，如果不为200记录错误信息
			$curl_info = $this->curl->curl_getinfo();
			$http_code = $curl_info['http_code'];
            if ($http_code != 200) {
            	continue;
            } else {
            	break;
            }
       }
		
		
		//访问媒资注入接口
		//$asset_ret = $this->curl->post($this->import_url,$data);
		
		//判断CURL状态，如果不为200记录错误信息
		//$curl_info = $this->curl->curl_getinfo();
		if($curl_info['http_code'] != '200')
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:s:i').'[访问媒资信息注入失败,CURL接口地址]：'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[HTTP状态码]：'.$curl_info['http_code']."\n"
						  .date('Y-m-d H:s:i').' [HTTP错误]：'.$this->curl->curl_error()."\n\n";
				$this->log->_write($this->log_file,$err_msg);
			
			}
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问媒资信息注入失败,HTTP状态码'.$curl_info['http_code'];
			$this->result['data'] 	= $asset_info['@attributes']['assets_id'];

			return $this->return_xml($this->result);
			
		}
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($asset_ret)),true);

		
		//成功记录接口返回数据
		if(IMPORT_LOG)
		{
			if($result_info['@attributes']['ret'] == '0')
			{
				$err_msg = date('Y-m-d H:s:i').'[访问媒资注入成功,CURL接口地址]：'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[接口返回结果]：'.$asset_ret;
			}else
			{
				$err_msg = date('Y-m-d H:s:i').'[访问媒资注入失败,CURL接口地址]：'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[接口返回结果]：'.$asset_ret;
			}
			//记录日志
			$this->log->_write($this->log_file,$err_msg);
		}
		
		$this->result['code']   = $result_info['@attributes']['ret'];
		$this->result['reason'] = $result_info['@attributes']['reason'];
		$this->result['data']   = $result_info['@attributes']['id'];
		unset($this->curl);
		return $this->return_xml($this->result);
		
	}


	/**
	 * 删除媒资
	 * $assets_id 媒资来源ID
	 * $video_type 媒资类型  0 点播,1 直播 默认为0
	 */
	public function delete_assets($assets_id,$assets_video_type = 0)
	{
		//检查必要参数
		if(empty($assets_id))
		{
			$this->result = array(
								'code' => 10001,
								'reason' => '媒资ID不能为空',
            					'data' => ''
								);
			if(IMPORT_LOG)
			{	
				$err_msg = date('Y-m-d H:s:i').'[访问媒资删除接口地址]：'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[错误码]：'.$this->result['code']."\n"
						  .date('Y-m-d H:s:i')."[错误原因]：".$this->result['reason']."\n"
						  .date('Y-m-d H:s:i')."[媒资来源ID]：".$this->result['id']."\n\n";
						  
				$this->log->_write($this->log_file,$err_msg);
			}
			
			return $this->return_xml($this->result);
		}
		
		//媒资删除接口数据结构
		$data['func']                   = 'delete_asset';
		$data['assets_id']              = $assets_id;
		$data['assets_video_type']      = $assets_video_type;
		
		//$asset_ret = $this->curl->post($this->import_url,$data);
		
		//判断CURL状态，如果不为200记录错误信息
		//$curl_info = $this->curl->curl_getinfo();
		
		
		for ($i = 0; $i < 2; $i++) {
        	//访问媒资注入接口
			$asset_ret = $this->curl->post($this->import_url,$data);		
			//判断CURL状态，如果不为200记录错误信息
			$curl_info = $this->curl->curl_getinfo();
			$http_code = $curl_info['http_code'];
            if ($http_code != 200) {
            	continue;
            } else {
            	break;
            }
       }
		
		
		if($curl_info['http_code'] != '200')
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:s:i').'[访问媒资删除失败,CURL接口地址]：'.$curl_info['url']."\n"
						  .date('Y-m-d H:s:i').'[HTTP状态码]：'.$curl_info['http_code']."\n"
						  .date('Y-m-d H:s:i').'[HTTP错误]：'.$this->curl->curl_error()."\n\n";
						  
				$this->log->_write($this->log_file,$err_msg);
			
			}
			$this->result['code'] = 10002;
			$this->result['reason'] = '访问媒资删除失败,HTTP状态码'.$curl_info['http_code'];
			$this->result['data'] = $assets_id;

			return $this->return_xml($this->result);
			
		}
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($asset_ret)),true);
		
		//记录日志
		if(IMPORT_LOG)
		{
			//成功
			if($result_info['@attributes']['ret'] == '0')
			{
				$err_msg = date('Y-m-d H:s:i').'[访问媒资删除成功,CURL接口地址]：'.$curl_info['url']."\n"
						  .date('Y-m-d H:s:i').'[接口返回结果]：'.$asset_ret."\n";
			//失败
			}else{
				$err_msg = date('Y-m-d H:s:i').'[访问媒资删除失败,CURL接口地址]：'.$curl_info['url']."\n"
						  .date('Y-m-d H:s:i').'[接口返回结果]：'.$asset_ret."\n";
			}
			$this->log->_write($this->log_file,$err_msg);
		
		}
		$this->result['code']   = $result_info['@attributes']['ret'];
		$this->result['reason'] = $result_info['@attributes']['reason'];
		$this->result['data']   = $result_info['@attributes']['id'];
		
		return $this->return_xml($this->result);
	}
	
	/**
	 * 分集信息注入
	 * $assets_id         媒资来源ID 
	 * $assets_clip_id    分集来源ID
	 * $assets_clip_info  分集信息
	 * $assets_video_type   媒资类型  0 点播,1 直播 默认为0
	 * 
	 */
	 public function import_video_clip($assets_id,$assets_clip_id,$assets_clip_info,$assets_video_type = 0)
	 {
	 	//验证参数
	 	
	 	if(empty($assets_id) || empty($assets_clip_id) || empty($assets_clip_info) )
		{
			$this->result['code']   = 10001;
			$this->result['reason'] = '媒资ID或者媒资分片ID或者分片信息不能为空';
			$this->result['data']   = $assets_clip_id;
			
			//记录日志
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:s:i').'[访问媒资分片注入接口地址]：'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[错误码]：'.$this->result['code']."\n"
						  .date('Y-m-d H:s:i')."[错误原因]：".$this->result['reason']."\n"
						  .date('Y-m-d H:s:i')."[媒资来源ID]：".$assets_id."\n"
						  .date('Y-m-d H:s:i')."[分片来源ID]：".$assets_clip_id."\n\n";
				
				$this->log->_write($this->log_file,$err_msg);
				return $this->return_xml($this->result);
			}
		}
	 	//解析XML数据，
		$clip_info = json_decode(json_encode(@simplexml_load_string($assets_clip_info)),true);
		$check = $this->clip_info_check_params($clip_info);
		if($check === false)
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:s:i').'[媒资分片注入开始,接口地址]:'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[错误码]：'.$this->result['code']."\n"
						  .date('Y-m-d H:s:i')."[错误原因]：".$this->result['reason']."\n"
						  .date('Y-m-d H:s:i')."[媒资来源ID]：".$assets_id."\n"
						  .date('Y-m-d H:s:i')."[分片来源ID]：".$this->result['data']."\n\n";
				$this->log->_write($this->log_file,$err_msg);
			}
			
			return $this->return_xml($this->result);
		}
		//分片注入数据结构
		$data['func'] 				= 'import_video_clip';
		$data['assets_id'] 			= $assets_id;
		$data['assets_video_type']  = $assets_video_type;
		$data['assets_clip_id']    = $assets_clip_id;
		$data['assets_clip_info']   = $assets_clip_info;
		
		//$clip_ret = $this->curl->post($this->import_url,$data);
		
		//判断CURL状态，如果不为200记录错误信息
		//$curl_info = $this->curl->curl_getinfo();
		
		for ($i = 0; $i < 2; $i++) {
        	//访问媒资注入接口
			$clip_ret = $this->curl->post($this->import_url,$data);		
			//判断CURL状态，如果不为200记录错误信息
			$curl_info = $this->curl->curl_getinfo();
			$http_code = $curl_info['http_code'];
            if ($http_code != 200) {
            	continue;
            } else {
            	break;
            }
        }
		
		if($curl_info['http_code'] != '200')
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:s:i').'[访问分片注入失败,CURL接口地址]：'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[HTTP状态码]：'.$curl_info['http_code']."\n"
						  .date('Y-m-d H:s:i').'[HTTP错误]：'.$this->curl->curl_error()."\n\n";
						  
				$this->log->_write($this->log_file,$err_msg);
			
			}
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问分片注入失败,HTTP状态码'.$curl_info['http_code'];
			$this->result['data'] 	= $assets_clip_id;

			return $this->return_xml($this->result);
			
		}
		
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($clip_ret)),true);
		
		//记录日志
		if(IMPORT_LOG)
		{
			//成功
			if($result_info['@attributes']['ret'] == '0')
			{
				$err_msg = date('Y-m-d H:s:i').'[访问分片注入成功,CURL接口地址]：'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[接口返回结果]：'.$clip_ret."\n";
			//失败
			}else{
				$err_msg = date('Y-m-d H:s:i').'[访问分片注入失败,CURL接口地址]：'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[接口返回结果]：'.$clip_ret."\n";
			}
			$this->log->_write($this->log_file,$err_msg);
		
		}
		$this->result['code']   = $result_info['@attributes']['ret'];
		$this->result['reason'] = $result_info['@attributes']['reason'];
		$this->result['data']   = $result_info['@attributes']['id'];
		
		return $this->return_xml($this->result);
	 }
	 
	 /**
	  *   分集信息删除
	  * $assets_clip_id      分集来源ID
	  * $assets_video_type   媒资类型  0 点播,1 直播 默认为0
	  * 
	  */
	public function delete_video_clip($assets_clip_id,$assets_video_type = 0)
	{
		if(empty($assets_clip_id))
		{
			$this->result['code'] = 10001;
			$this->result['reason']  = '分片来源ID不能为空';
			$this->result['data'] = '';
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:s:i').'[访问删除分片失败,CURL接口地址]：'.$this->import_url."\n"
						   .date('Y-m-d H:s:i').'[错误码]：'.$this->result['code'."\n"]
						   .date('Y-m-d H:s:i').'[错误原因]：'.$this->result['reason']."\n";
				$this->log->_write($this->log_file,$err_msg);
			}
			
			return $this->return_xml($this->result);
		}
		//分片删除数据结构
		$data['func'] 				= 'delete_video_clip';
		$data['assets_video_type'] 	= $assets_video_type;
		$data['index_clip_id'] 	= $assets_clip_id;
		
		//$clip_ret = $this->curl->post($this->import_url,$data);
		//获取CURL信息,如果不为200记录错误信息
		//$curl_info = $this->curl->curl_getinfo();
		
		for ($i = 0; $i < 2; $i++) {
        	//访问媒资注入接口
			$clip_ret = $this->curl->post($this->import_url,$data);		
			//判断CURL状态，如果不为200记录错误信息
			$curl_info = $this->curl->curl_getinfo();
			$http_code = $curl_info['http_code'];
            if ($http_code != 200) {
            	continue;
            } else {
            	break;
            }
        }
		

		if($curl_info['http_code'] != '200')
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:s:i').'[访问分片删除失败,CURL接口地址]：'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[HTTP状态码]：'.$curl_info['http_code']."\n"
						  .date('Y-m-d H:s:i').'[HTTP错误]：'.$this->curl->curl_error()."\n\n";
						  
				$this->log->_write($this->log_file,$err_msg);
			
			}
			$this->result['code'] = 10002;
			$this->result['reason'] = '访问分片删除失败,HTTP状态码：'.$curl_info['http_code'];
			$this->result['data'] = $assets_clip_id;

			return $this->return_xml($this->result);
			
		}
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($clip_ret)),true);
		
		//记录日志
		if(IMPORT_LOG)
		{
			//成功
			if($result_info['@attributes']['ret'] == '0')
			{
				$err_msg = date('Y-m-d H:s:i').'[访问分片删除成功,CURL接口地址]：'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[接口返回结果]：'.$clip_ret."\n";
			//失败
			}else{
				$err_msg = date('Y-m-d H:s:i').'[访问分片注入失败,CURL接口地址]：'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[接口返回结果]：'.$clip_ret."\n";
			}
			$this->log->_write($this->log_file,$err_msg);
		
		}
		$this->result['code']   = $result_info['@attributes']['ret'];
		$this->result['reason'] = $result_info['@attributes']['reason'];
		$this->result['data']   = $result_info['@attributes']['id'];
		
		return $this->return_xml($this->result);
	}
	
	/**
	 * 片源信息注入
	 * $assets_id    			媒资来源ID
	 * $assets_clip_id 			分集来源ID
	 * $assets_file_id 			片源来源ID
	 * $assets_file_info 		片源信息
	 * $assets_video_type 		媒资类型   0 点播,1 直播 默认为0
	 * 
	 */
	public function import_video_file($assets_id,$assets_clip_id,$assets_file_id,$assets_file_info,$assets_video_type = 0)
	{
		if(empty($assets_id) || empty($assets_clip_id) || empty($assets_file_id) || empty($assets_file_info))
		{
			$this->result['code'] = 10001;
			$this->result['reason']  = '媒资来源ID或者分片来源ID或者片源来源ID或者片源信息为空';
			$this->result['data'] = '$assets_file_id';
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:s:i').'[访问删除分片失败,CURL接口地址]：'.$this->import_url."\n"
						   .date('Y-m-d H:s:i').'[错误码]：'.$this->result['code'."\n"]
						   .date('Y-m-d H:s:i').'[错误原因]：'.$this->result['reason']."\n";
				$this->log->_write($this->log_file,$err_msg);
			}
			
			return $this->return_xml($this->result);
		}
		$file_info = json_decode(json_encode(@simplexml_load_string($assets_file_info)),true);
		
		$check = $this->file_info_check_params($file_info);
		if($check === false)
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:s:i').'[媒资片源注入开始,接口地址]:'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[错误码]：'.$this->result['code']."\n"
						  .date('Y-m-d H:s:i')."[错误原因]：".$this->result['reason']."\n"
						  .date('Y-m-d H:s:i')."[媒资来源ID]：".$assets_id."\n"
						  .date('Y-m-d H:s:i')."[分片来源ID]：".$assets_clip_id."\n"
						  .date('Y-m-d H:s:i')."[片源来源ID]：".$this->result['data']."\n\n";
				$this->log->_write($this->log_file,$err_msg);
			}
			
			return $this->return_xml($this->result);
		}
		
		//片源注入数据结构
		$data['func'] 				= 'import_video_file';
		$data['assets_id'] 			= $assets_id;
		$data['assets_video_type'] 	= $assets_video_type;
		$data['assets_clip_id'] 	= $assets_clip_id;
		$data['assets_file_id'] 	= $assets_file_id;
		$data['assets_file_info'] 	= $assets_file_info;
		
		//$file_ret = $this->curl->post($this->import_url,$data);
		//获取CURL信息,如果不为200记录错误信息
		//$curl_info = $this->curl->curl_getinfo();
		
		
		
		for ($i = 0; $i < 2; $i++) {
        	//访问媒资注入接口
			$file_ret = $this->curl->post($this->import_url,$data);	
			//判断CURL状态，如果不为200记录错误信息
			$curl_info = $this->curl->curl_getinfo();
			$http_code = $curl_info['http_code'];
            if ($http_code != 200) {
            	continue;
            } else {
            	break;
            }
        }
		
		if($curl_info['http_code'] != '200')
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:s:i').'[访问片源注入失败,CURL接口地址]：'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[HTTP状态码]：'.$curl_info['http_code']."\n"
						  .date('Y-m-d H:s:i').'[HTTP错误]：'.$this->curl->curl_error()."\n\n";
						  
				$this->log->_write($this->log_file,$err_msg);
			
			}
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问片源注入失败,HTTP状态码：'.$curl_info['http_code'];
			$this->result['data'] 	= $assets_clip_id;

			return $this->return_xml($this->result);
			
		}
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($file_ret)),true);
		
		//记录日志
		if(IMPORT_LOG)
		{
			//成功
			if($result_info['@attributes']['ret'] == '0')
			{
				$err_msg = date('Y-m-d H:s:i').'[访问片源注入成功,CURL接口地址]：'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[接口返回结果]：'.$file_ret."\n";
			//失败
			}else{
				$err_msg = date('Y-m-d H:s:i').'[访问片源注入失败,CURL接口地址]：'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[接口返回结果]：'.$file_ret."\n";
			}
			$this->log->_write($this->log_file,$err_msg);
		
		}
		$this->result['code']   = $result_info['@attributes']['ret'];
		$this->result['reason'] = $result_info['@attributes']['reason'];
		$this->result['data']   = $result_info['@attributes']['id'];
		
		return $this->return_xml($this->result);
		
	}
	
	/**
	 * 片源删除
	 * $$assets_file_id     片源来源ID
	 * $assets_video_type   媒资类型   0 点播,1 直播 默认为0
	 * 
	 */
	public function delete_video_file($assets_file_id,$assets_video_type = 0)
	{
		if(empty($assets_file_id))
		{
			$this->result['code'] = 10001;
			$this->result['reason']  = '片源来源ID不能为空';
			$this->result['data'] = '';
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:s:i').'[访问删除片源失败,CURL接口地址]：'.$this->import_url."\n"
						   .date('Y-m-d H:s:i').'[错误码]：'.$this->result['code']."\n"
						   .date('Y-m-d H:s:i').'[错误原因]：'.$this->result['reason']."\n\n";
				$this->log->_write($this->log_file,$err_msg);
			}
			
			return $this->return_xml($this->result);
		}
		//片源删除数据结构
		$data['func'] 				= 'delete_video_file';
		$data['assets_video_type'] 	= $assets_video_type;
		$data['assets_file_id'] 	= $assets_file_id;
		
		$file_ret = $this->curl->post($this->import_url,$data);
		//获取CURL信息,如果不为200记录错误信息
		$curl_info = $this->curl->curl_getinfo();
		
		for ($i = 0; $i < 2; $i++) {
        	//访问媒资注入接口
			$file_ret = $this->curl->post($this->import_url,$data);	
			//判断CURL状态，如果不为200记录错误信息
			$curl_info = $this->curl->curl_getinfo();
			$http_code = $curl_info['http_code'];
            if ($http_code != 200) {
            	continue;
            } else {
            	break;
            }
        }
		
		
		
		if($curl_info['http_code'] != '200')
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:s:i').'[访问片源删除失败,CURL接口地址]：'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[HTTP状态码]：'.$curl_info['http_code']."\n"
						  .date('Y-m-d H:s:i').'[HTTP错误]：'.$this->curl->curl_error()."\n\n";
						  
				$this->log->_write($this->log_file,$err_msg);
			
			}
			$this->result['code']   = 10002;
			$this->result['reason'] = '访问片源删除失败,HTTP状态码：'.$curl_info['http_code'];
			$this->result['data']   = $assets_file_id;
			
			return $this->return_xml($this->result);
			
		}
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($file_ret)),true);
		
		//记录日志
		if(IMPORT_LOG)
		{
			//成功
			if($result_info['@attributes']['ret'] == '0')
			{
				$err_msg = date('Y-m-d H:s:i').'[访问片源删除成功,CURL接口地址]：'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[接口返回结果]：'.$file_ret."\n";
			//失败
			}else{
				$err_msg = date('Y-m-d H:s:i').'[访问片源注入失败,CURL接口地址]：'.$this->import_url."\n"
						  .date('Y-m-d H:s:i').'[接口返回结果]：'.$file_ret."\n";
			}
			$this->log->_write($this->log_file,$err_msg);
		
		}
		$this->result['code']   = $result_info['@attributes']['ret'];
		$this->result['reason'] = $result_info['@attributes']['reason'];
		$this->result['data']   = $result_info['@attributes']['id'];
		
		return $this->return_xml($this->result);
	}
	
	/**
	 * 检查媒资信息参数
	 * $asset_info 媒资信息
	 */
	private function assets_info_check_params($asset_info)
	{		
		if (empty($asset_info['@attributes']['assets_id']) || empty($asset_info['@attributes']['name'])) 
		{
			$this->result['code'] 	= 10001;
			$this->result['reason'] = '媒资id或者媒资名称为空';
			$this->result['data'] 	= $asset_info['@attributes']['assets_id'];
            return false;
         }
		return true;
	}
	
	/**
	 * 检查分片信息参数
	 * $clip_info  分片信息
	 */
	private function clip_info_check_params($clip_info)
	{

		if (empty($clip_info['@attributes']['clip_id']) || (empty($clip_info['@attributes']['index']) && $clip_info['@attributes']['index'] != 0)) 
		{
			$this->result['code'] 	= 10001;
			$this->result['reason'] = '分片来源ID或者分片序号不能为空';
			$this->result['data'] 	= $clip_info['@attributes']['clip_id'];
            return false;
         }
		return true;
	}
	
	/**
	 * 检查片源信息参数
	 * $file_info 	片源信息
	 */
	private function file_info_check_params($file_info)
	{
		if(empty($file_info['@attributes']['file_id']) || empty($file_info['@attributes']['file_type']))
		{
			$this->result['code'] 	= 10001;
			$this->result['reason'] = '片源来源ID或者片源类型不能为空';
			$this->result['data']   = $file_info['@attributes']['file_id'];
            return false;
		}
		return true;
	}
	
	/**
     * 传入参数返回xml
     * @param array $params:代码数组
     * @return string(xml)
     */
    private function return_xml($params = array()) {
            $array = array('ret'=>$params['code'],'reason'=>$params['reason'],'data'=>$params['data']);
            return json_encode($array);
            //return "<result  ret=\"{$params['code']}\"  reason=\"{$params['reason']}\" data='{$params['data']}'></result>";
    }
}

