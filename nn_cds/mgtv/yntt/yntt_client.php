<?php
//ini_set('display_errors',1);
//ini_set("soap.wsdl_cache_enabled", "0");
include_once dirname(dirname(__FILE__)).'/utils.func.php';
include_once dirname(__FILE__).'/init.php';
define('CSPID','mangotv');
define('LSPID','mangotv');
class yntt_client{
	//http://218.207.213.7:35100/cms/services/ctms?wsdl
	//http://127.0.0.1/work/nn_inject/mgtv/fjyd/fjyd_service.php?wsdl
	const WSDL = 'http://192.168.8.232/nn_mgtvbk/mgtv/yntt/yntt_service.php?wsdl';

	static public function ExecCmd($CorrelateID,$CmdFileURL){
		$arg_list = func_get_args();
		//$CorrelateID = fjyd_guid_rand();
		i_write_log('ExecCmd request args:'.var_export($arg_list,true),'c2');
		$client = self::get_soap_client();
		$ret = $client->ExecCmd(CSPID,LSPID,$CorrelateID,$CmdFileURL);
		i_write_log('ExecCmd return:'.var_export($ret,true),'c2');
		//i_write_log('LastRequest:'.$client->__getLastRequest());
		//i_write_log('LastResponse:'.$client->__getLastResponse());
		return $ret;
	}
	public static function get_soap_client(){
		return new SOAPClient(self::WSDL,array('trace'=>true));
	}
}


/*
$client = new fjyd_client();

$CmdFileURL='ftp://dfdfd';
$client->ExecCmd($CmdFileURL);

echo 'game over!';
*/
