<?php
/**
 * 黑龙江IPTV，注入到电信CDN（中兴C2）配置信息
 * @author chunyang.shu
 * @date 2017-09-23
 */

// 定义ORG_ID
if (!defined('ORG_ID'))
{
    define('ORG_ID', 'hljdx_hw');
}

// 加载文件
include_once  dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/' . ORG_ID . '/init.php';
include_once  dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/' . ORG_ID . '/define.php';

// 定义上下游标识
if (!defined('CSPID'))
{
    define('CSPID', 'HLJ_STARCOR');
}
if (!defined('LSPID'))
{
    define('LSPID', 'HLJ_HUAWEI');
}

// 定义接口地址
if (!defined('HLJDX_HW_CDN_WSDL_URL'))
{
    define('HLJDX_HW_CDN_WSDL_URL', 'http://10.20.200.59/bk/nn_cds/mgtv/hljdx_hw/hw.wsdl');
}