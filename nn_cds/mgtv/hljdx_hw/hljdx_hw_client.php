<?php
ini_set('display_errors',1);
set_time_limit(0);
ini_set("soap.wsdl_cache_enabled", "0");
include_once dirname(__FILE__) . '/config.php';

/**
 * 请求CDN注入的客户端类
 * @author chunyang.shu
 * @date 2018-09-28
 */
class hljdx_hw_client
{
    /**
     * 请求C2消息接口
     * @param string $message_id 消息ID
     * @param string $data_xml_url 内容xml地址
     * @return mixed
     * @author chunyang.shu
     * @date 2018-09-28
     */
    public static function ExecCmd($message_id, $data_xml_url)
    {
        // 记录日志
        $str_log = "开始CDN注入，请求C2消息接口，播控消息ID为：{$message_id}，消息内容文件xml地址为：{$data_xml_url}";
        nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, $str_log, ORG_ID);

        // 创建SOAP客户端，请求接口
        $arr_config = array('trace' => true);
        $obj_soap_client = new SoapClient(HLJDX_HW_CDN_WSDL_URL, $arr_config);
        $mixed_result = $obj_soap_client->ExecCmd(CSPID, LSPID, $message_id, $data_xml_url);
        if (empty($mixed_result))
        {
            // 请求失败，进行一次重试
            $str_log = "请求C2消息接口失败，返回结果为：" . var_export($mixed_result, true) . "，进行一次重试";
            nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, $str_log, ORG_ID);
            $mixed_result = $obj_soap_client->ExecCmd(CSPID, LSPID, $message_id, $data_xml_url);
            if (empty($mixed_result))
            {
                $str_log = "重试请求C2消息接口失败，返回结果为：" . var_export($mixed_result, true);
                nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, $str_log, ORG_ID);
                return $mixed_result;
            }
        }

        // 返回结果
        $str_log = "请求C2消息接口成功，返回结果为：" . var_export($mixed_result, true);
        nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, $str_log, ORG_ID);
        return $mixed_result;
    }
}

