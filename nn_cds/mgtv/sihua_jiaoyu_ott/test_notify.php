<?php
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('display_errors', 0);
set_time_limit(0);
$arr_dir = explode('|', str_replace(array ('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/sp/sp.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/models/public_model_exec.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/models/video_model.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/c2_log/c2_log.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/c2_task/c2_task.class.php';
$file_init_dir = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/' . ORG_ID . '/init.php';
$file_init_define = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/' . ORG_ID . '/define.php';
if(!file_exists($file_init_define) || !file_exists($file_init_dir))
{
    nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '初始化文件不存在:[' . $file_init_dir . ']['.$file_init_define.']', 'public');
    echo "1|inner error";
    exit();
}
$dc = nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$sp_config = nl_sp::query_by_id($dc, ORG_ID);
$sp_config = (isset($sp_config['data_info'][0]['nns_config']) && strlen($sp_config['data_info'][0]['nns_config'])) ? json_decode($sp_config['data_info'][0]['nns_config'],true) : null;

include_once $file_init_dir;
include_once $file_init_define;
$result_media = nl_query_by_db("select * from nns_vod_media where nns_id='{}'",$dc->db());
$result_media = $result_media[0];

$result_media['nns_content_id'] = 'OTHE6220180710000514';
$action='add';
$result = array('ret'=>0,'reason'=>ok);
$result_c2_task_info = array(
    'nns_message_id'=>'TXSP_REGIST_201807000001',
);;
$type='media';
$sp_id='';
$flag = true;
$result_notify = c2_task_model::notify_upstream($result_media,$action,$result,$sp_config,$result_c2_task_info,$dc,$type,$sp_id,$flag);