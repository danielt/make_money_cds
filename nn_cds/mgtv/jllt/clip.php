<?php
/**
 * 切片接口入口
 */

@ini_set('display_errors',1);
include_once dirname(dirname(__FILE__)).'/mgtv_init.php';
include_once dirname(__FILE__).'/init.php';
$func = isset($_GET['nns_func'])?$_GET['nns_func']:null;
if($func==null){
	//die('param nns_func is empty');
	//$data =  '<result state="0" reason="OK" />';
	_bulid_result('500','param nns_func is empty',null);
}
if(function_exists($func)){
	$func();
}else{
	_bulid_result('500',$func.' not found',null);
}
//获取片源
function get_clip_task(){
	DEBUG && i_write_log('------get_clip_task------','clip');
	$nns_new_dir = isset($_GET['nns_new_dir']) ? (empty($_GET['nns_new_dir']) ? false : true) : false;
	//test
	/*
	$data= '<task id="51ff32dc1c461e89e9818a21c5fdd43a"><video id="b253852f6afe1369089f72fe6a40c09d" ><index_list><index id="3d83b7ee8b1941a728d462514f6c2998" index="0"><media_list>' .
			'<media id="0010e3fe0bb295f5e1526f10f0823d70" file_id="415068" content_id="419925" content_url="/internettv/init/Movie_20111206172739314122.ts" kbps="1464" kbps_index="01"/>' .
			'<media id="0010e3fe0bb295f5e1526f10f0823d70" file_id="415068" content_id="295296" content_url="/internettv/init/Movie_20111206172739314122.ts" kbps="1464" kbps_index="02" /></media_list></index></index_list></video></task>';
	_bulid_result('200','OK',$data);
	*/
	//
	$task_info = clip_task_model::get_task();
	//
	if(!is_array($task_info)){
		DEBUG && i_write_log('------no task------','clip');
	    _bulid_result('404','no task',null);
		exit;
	}
    
	
	$dt = date('Y-m-d H:i:s');
	//更新任务状态:任务已被取走
	$set_state = array(
	'nns_state' => clip_task_model::TASK_C_STATE_HANDLE,
	'nns_desc' => clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_HANDLE],
	'nns_start_time'=> $dt,
	'nns_modify_time'=>$dt,
	);
	if($nns_new_dir){
		$set_state['nns_new_dir']=1;
	}else{
		$set_state['nns_new_dir']=0;
	}
	clip_task_model::update_task($task_info['nns_id'], $set_state);
	
	
	//任务日志	
	$task_log = array(
	'nns_task_id' => $task_info['nns_id'],
	'nns_state'   => clip_task_model::TASK_C_STATE_HANDLE,
	'nns_desc'    => clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_HANDLE],
	);	
	$rs = clip_task_model::add_task_log($task_log);	
	//var_dump($rs);
	//echo $task_info['nns_content'];
	_bulid_result('200','OK',$task_info['nns_content']);
}
function get_media_url(){
	DEBUG && i_write_log('------get_media_url------','clip');
	//	$data = '<media id="0010e3fe0bb295f5e1526f10f0823d70" url="http://113.247.251.12/iscsi4/mediafiles/dest_movie/Movie_20130517163636373145.ts" state="0" reason="ok"/>';

	//	_bulid_result('200','OK',$data);	
	//
	$task_id = isset($_GET['nns_task_id'])?$_GET['nns_task_id']:null;
	$video_id = isset($_GET['nns_video_id'])?$_GET['nns_video_id']:null;
	$video_type = isset($_GET['nns_video_type'])?$_GET['nns_video_type']:0;
	$nns_index_id = isset($_GET['nns_index_id'])?$_GET['nns_index_id']:null;
	$nns_media_id = isset($_GET['nns_media_id'])?$_GET['nns_media_id']:null;
	$nns_new_dir = isset($_GET['nns_new_dir']) ? (empty($_GET['nns_new_dir']) ? false : true) : false;
	
	//判断任务状态
	$task_info = clip_task_model::get_task_by_id($task_id);
	if($task_info == false){
		DEBUG && i_write_log('未找到该任务(task id:'.$task_id.')','clip');
		
		
		_bulid_result('404','get task info fail',null);	
	}
	if($task_info['nns_state'] == clip_task_model::TASK_C_STATE_CANCEL){
		DEBUG && i_write_log('任务已取消(task id:'.$task_id.')','clip');
		
		$data = '<media id="'.$nns_media_id.'" url="" state="1" reason="task cancel"/>';

		_bulid_result('200','OK',$data);	
	}
	//取影片
	$_params = array(
	'nns_video_id'=>$video_id,
	'nns_video_type'=>$video_type,
	'nns_video_index_id'=>$nns_index_id,
	'nns_media_id'=>$nns_media_id
	);
	//i_write_log('params:'.var_export($_params,true));
	$media_info = clip_task_model::get_media_info($_params);
	if($media_info==false){
		DEBUG && i_write_log('取片源信息失败','clip');
		
		_bulid_result('404','get media info fail',null);
	}
	//取下载地址
	
	include_once dirname(dirname(dirname(dirname(__FILE__)))).'/np/np_http_curl.class.php';
	$http_client = new np_http_curl_class();
	$url = MGTV_MEDIA_DOWNLOAD_URL.'?'.'file_id='.$media_info['nns_import_id'];
	//$url = 'http://58.83.191.25/OTTBossApi/GetDownLoadUrl.ashx?file_id=419925';
	DEBUG && i_write_log('从接口取影片下载地址'."\n".$url,'clip');
	$result = $http_client->get($url);
	$ret_arr = json_decode($result, true);
	DEBUG && i_write_log('response:'.$result."\n".var_export($ret_arr,true),'clip');
	$dt = date('Y-m-d H:i:s');
	if($ret_arr['err']=='0'){
		$down_url = str_replace('113.247.251.12','112.124.57.211:92',$ret_arr['msg']['download_url']);	
		$down_url = $ret_arr['msg']['download_url'];	
		$status = $ret_arr['status'];	
		$reason = $ret_arr['reason'];	
		$data = '<media id="'.$nns_media_id.'" url="'.$down_url.'" state="0" reason="'.$reason.'"/>';
		$set_state = array(
		'nns_desc'=> clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_GET_MEDIA_URL_SUCC].'['.$url.'][media_url:'.$down_url.']',
		'nns_state' => clip_task_model::TASK_C_STATE_GET_MEDIA_URL_SUCC,
		'nns_modify_time' => $dt,
		'nns_alive_time' => $dt,
		);
		if($nns_new_dir){
			$set_state['nns_new_dir']=1;
		}else{
			$set_state['nns_new_dir']=0;
		}
		clip_task_model::update_task($task_id, $set_state);
		
		//任务日志	
		$task_log = array(
		'nns_task_id'=>$task_id,
		'nns_state'=> clip_task_model::TASK_C_STATE_GET_MEDIA_URL_SUCC,
		'nns_desc'=> clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_GET_MEDIA_URL_SUCC].'['.$url.'][media_url:'.$down_url.']',
		'nns_create_time' => $dt,
		//'nns_modify_time'=>$dt,
		);
		clip_task_model::add_task_log($task_log);		
		//
		_bulid_result('200','OK',$data);	
	}else{
		$set_state = array(
		'nns_desc'=> clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_GET_MEDIA_URL_FAIL].'['.$url.']',
		'nns_state' => clip_task_model::TASK_C_STATE_GET_MEDIA_URL_FAIL,
		'nns_modify_time' => $dt,
		'nns_alive_time' => $dt,
		);
		if($nns_new_dir){
			$set_state['nns_new_dir']=1;
		}else{
			$set_state['nns_new_dir']=0;
		}
		clip_task_model::update_task($task_id, $set_state);	
		//任务日志	
		$task_log = array(
		'nns_task_id'=>$task_id,
		'nns_state'=> clip_task_model::TASK_C_STATE_GET_MEDIA_URL_FAIL,
		'nns_desc'=> clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_GET_MEDIA_URL_FAIL].'['.$url.']',
		'nns_create_time' => $dt,
		//'nns_modify_time'=>$dt,
		);
		clip_task_model::add_task_log($task_log);			
		//	
		_bulid_result('404','get meida url fail',null);
	}

}
//上报切片状态
function report_clip_status(){
        DEBUG && i_write_log('------report_clip_status------'."\n params:".var_export($_GET,true),'clip');
/*
	$data =  '<result state="0" reason="OK" />';
	_bulid_result('200','OK',$data);
*/	
	//
	$task_id = isset($_GET['nns_task_id'])?$_GET['nns_task_id']:null;
	$state = isset($_GET['nns_state'])?$_GET['nns_state']:null;
	$desc = isset($_GET['nns_desc'])?urldecode($_GET['nns_desc']):null;
	//$report_data = '<media id="" url=""  state="" desc=""/>';
	$nns_new_dir = isset($_GET['nns_new_dir']) ? (empty($_GET['nns_new_dir']) ? false : true) : false;
    $dt = date('Y-m-d H:i:s');
	
	$set_state = array(
	'nns_desc'=>$desc,
	'nns_state' => $state,
	'nns_modify_time' => $dt,
	'nns_alive_time' => $dt,
	);	
	$task_info = clip_task_model::get_task_by_id($task_id);
	if($task_info == false){
		
		DEBUG && i_write_log('未找到该任务(task id:'.$task_id.')','clip');
		_bulid_result('404','get task info fail',null);
		return false;
	}	
	if($task_info['nns_state'] == clip_task_model::TASK_C_STATE_CANCEL){
		DEBUG && i_write_log('任务已取消(task id:'.$task_id.')','clip');
		
		$data = '<result state="1" reason="task cancel" />';

		_bulid_result('200','OK',$data);	
	}			
	//如果切片完成，生成注入xml文件，执行影片注入命令
	$state = trim($state);
	if(strtolower($state) == clip_task_model::TASK_C_STATE_OK){
		DEBUG && i_write_log('切片完成，生成注入xml文件，执行影片注入命令','clip');
		dirname(dirname(__FILE__)).'/models/category_content_model.php';
		
       /*
		if(empty($task_info['nns_content'])){
			$task_info['nns_content'] = clip_task_model::_build_task_content($task_info);
		}
		
		$dom	= new DOMDocument('1.0', 'utf-8');
		$dom -> loadXML($task_info['nns_content']);		
		$xpath = new DOMXPath($dom);

	    $nns_vod_id = $xpath->query('/task/video')->item(0)->getAttribute('id');
	    $nns_vod_index_id = $xpath->query('/task/video/index_list/index')->item(0)->getAttribute('id');
		
		$nns_media_ids = array();
		$entries = $xpath->query('/task/video/index_list/index/media_list/media');
		foreach($entries as $item){
			$nns_media_ids[] = $item->getAttribute('id');
		}
		$media_info['nns_video_id'] = $nns_vod_id;
		$media_info['nns_video_index_id'] = $nns_vod_index_id;
		$media_info['nns_media_ids'] = $nns_media_ids;
		$media_info['nns_task_id'] = $task_id;
		
		$nns_date = str_replace('.','',$_GET['nns_date']);
		$nns_date = str_replace('/','',$nns_date);
		$media_info['nns_date'] = $nns_date;
		
		include_once dirname(__FILE__).'/models/c2_task_model.php';
		c2_task_model::add_movies_by_vod_index($media_info);
		*/
		
		$nns_date = str_replace('.','',$_GET['nns_date']);
		$nns_date = str_replace('/','',$nns_date);
				
		//$set_state['nns_end_time'] = $dt;
		$set_state['nns_date'] = $nns_date;
		category_content_model::add_media_by_clip($task_id,$nns_date);
	}
	//更新任务状态
	if($nns_new_dir){
		$set_state['nns_new_dir']=1;
	}else{
		$set_state['nns_new_dir']=0;
	}
	clip_task_model::update_task($task_id, $set_state);	
	//任务日志	
	$task_log = array(
	'nns_task_id'=>$task_id,
	'nns_state'=>$state,
	'nns_desc'=>$desc,
	'nns_create_time' => $dt,
	//'nns_modify_time'=>$dt,
	);
	clip_task_model::add_task_log($task_log);
	
	$data =  '<result state="0" reason="OK" />';
	_bulid_result('200','OK',$data);
}
/**
 * 任务不存在或者任务已经注入成功，则可以删除
 */
function delete_clip_file(){
	//
	$task_id = isset($_GET['nns_task_id'])?$_GET['nns_task_id']:null;
	$nns_new_dir = isset($_GET['nns_new_dir']) ? (empty($_GET['nns_new_dir']) ? false : true) : false;
	if($nns_new_dir){
		$result = clip_task_model::can_delete_clip_file($task_id);
	}else{
		$result = clip_task_model::can_delete_clip_file_new($task_id);//判断分集ID所在的所有任务ID是否可以删除
	}
	
	if($result){
		$data = '<result state="0" reason="allow delete" />';
		_bulid_result('200','OK',$data);
	}else{
		$data =  '<result state="1" reason="not allowed delete" />';
		_bulid_result('200','OK',$data);
	}
	//echo $data;
}
function _bulid_result($http_code,$resson,$data=null){
	//return 0;	
	header('HTTP/1.1 '.$http_code.' '.$resson);
    header('Status: '.$http_code.' '.$resson);
    header ( "Content-Type:text/xml;charset=utf-8" ); 
    if($data!= null){
    echo '<?xml version="1.0" encoding="UTF-8"?>';
    DEBUG && i_write_log('返回结果'."\n".$data,'clip');
    echo 	$data;    	
    }

    exit;
}
