<?php
ini_set("soap.wsdl_cache_enabled", "0");
include dirname(dirname(__FILE__)) . '/utils.func.php';
/**
 * soap服务端消息体反馈
 * @author liangpan
 * @date 2016-09-10
 */
class content_deploy_service
{
	public function ResultNotify($CMSID, $SOPID, $CorrelateID, $ResultCode, $ErrorDescription, $ResultFileURL)
	{
		$arg_list = func_get_args();
		nn_log::write_log_message(LOG_MODEL_NOTIFY, '服务端请求传入参数:' . var_export($arg_list, true) . ',执行文件:' . __FILE__ . ',执行方法:' . __FUNCTION__, $CMSID, null, 'message', $SOPID);
		return array (
				'ResultCode' => 0, 
				'ErrorDescription' => 'ok'
		);
	}
}
$server = new SOAPServer('hw_notify.wsdl'); //unicom_zte.wsdl
$server->setClass('content_deploy_service');
$server->handle();
