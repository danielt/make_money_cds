<?php
ini_set('display_errors', 7);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
ini_set("soap.wsdl_cache_enabled", "0");
define("IS_LOG_TIMER_OPERATION", true);
$arr_dir = explode('|', str_replace(array ('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
/**
 * soap类客户端请求
 * @author liangpan
 * @date 2016-09-10
 */
class content_deploy_client
{
	public $wsdl = null;
	public $cp_id = null;
	public $sp_id = null;
	public $video_type = null;
	//soap 请求配置
	public $soap_config = array ('trace' => true);
	/**
	 * CDN消息反馈初始化设置
	 * @param string $sp_id sp_id
	 * @param string $cp_id cp_id
	 * @param string $video_type 影片类型
	 * @author liangpan
	 * @date 2016-09-10
	*/
	public function __construct($sp_id = null, $cp_id = null, $video_type = null)
	{
		
		// 		$this->wsdl = 'http://www.cms.com/bk_2_4_X/nn_mgtvbk/mgtv/' . ORG_ID . '/content_deploy_service.php?wsdl';
		if (strlen($cp_id) < 1)
		{
			nn_log::write_log_message(LOG_MODEL_CDN_BK_SEND, 'CP ID 为空传入参数：' . var_export(func_get_args(), true) . ',执行文件:' . __FILE__ . ',执行方法:' . __FUNCTION__, 'default', $video_type);
		}
		if (strlen($sp_id) < 1)
		{
			nn_log::write_log_message(LOG_MODEL_CDN_BK_SEND, 'SP ID 为空传入参数：' . var_export(func_get_args(), true) . ',执行文件:' . __FILE__ . ',执行方法:' . __FUNCTION__, 'default', $video_type);
		}
		$this->wsdl = 'http://202.107.186.204:57880/nn_mgtvbk/mgtv/iptv/'.$sp_id.'_notify_service.php?wsdl';
		$this->cp_id = $cp_id;
		$this->sp_id = $sp_id;
		$this->video_type = $video_type;

        http://202.107.186.204:57880/nn_mgtvbk/
	}

	/**
	 * CDN消息反馈方法
	 * @param string $CorrelateID  消息体ID 对方的guid
	 * @param int $ResultCode 状态码
	 * @param string $ErrorDescription 错误描述
	 * @param string $ResultFileURL 消息反馈的xml ftp路径
	 * @return array
	 * @author liangpan
	 * @date 2016-09-10
	 */
	public function ContentDeployResult($CorrelateID, $ResultCode, $ErrorDescription, $ResultFileURL)
	{
		$arg_list = func_get_args();
		nn_log::write_log_message(LOG_MODEL_CDN_BK_SEND, '--------------CDN消息反馈开始--------------', $this->sp_id, $this->video_type, 'message', $this->cp_id);
		nn_log::write_log_message(LOG_MODEL_CDN_BK_SEND, '执行文件:' . __FILE__ . ',执行方法:' . __FUNCTION__, $this->sp_id, $this->video_type, 'message', $this->cp_id);
		nn_log::write_log_message(LOG_MODEL_CDN_BK_SEND, '客户端请求传入参数:' . var_export($arg_list, true), $this->sp_id, $this->video_type, 'message', $this->cp_id);
		$client = $this->get_soap_client();
		$ret = $client->ResultNotify($this->sp_id, $this->cp_id, $CorrelateID, $ResultCode, $ErrorDescription, $ResultFileURL);
		nn_log::write_log_message(LOG_MODEL_CDN_BK_SEND, '客户端输出结果:' . var_export($ret, true), $this->sp_id, $this->video_type, 'message', $this->cp_id);
		nn_log::write_log_message(LOG_MODEL_CDN_BK_SEND, '--------------CDN消息反馈结束--------------', $this->sp_id, $this->video_type, 'message', $this->cp_id);
		return $ret;
	}
	/**
	 * SOAP客户端类方法
	 * @return SOAPClient
	 * @author liangpan
	 * @date 2016-09-10
	 */
	public function get_soap_client()
	{
		nn_log::write_log_message(LOG_MODEL_CDN_BK_SEND, '客户端请求wsdl地址:' . $this->wsdl . ',wsdl配置参数:' . var_export($this->soap_config,true), $this->sp_id, $this->video_type, 'message', $this->cp_id);
		return new SOAPClient($this->wsdl, $this->soap_config);
	}
}
