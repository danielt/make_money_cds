<?php
header("Content-type: text/html; charset=utf-8");
error_reporting(E_ALL);
ini_set('display_errors',1);
set_time_limit(0);
include_once dirname(dirname(__FILE__)).'/mgtv_init.php';

$db_r = nn_get_db(NL_DB_READ);
$db_w = nn_get_db(NL_DB_WRITE);



$sql = "select * from nns_vod_media where nns_status=3 and nns_action='destroy' order by nns_modify_time desc limit 100";


i_write_log_core($sql);

$medias = nl_db_get_all($sql, $db_r);
if(count($medias)>0){
	foreach ($medias as $media) {
		$vod_index = nl_db_get_one("select * from nns_vod_index where nns_id='{$media['nns_vod_index_id']}'", $db_r);
		$sp_list = sp_model::get_sp_list();
		i_write_log_core(var_export($sp_list,true));
		foreach ($sp_list as $sp) {			
			$sql_c2 = "select * from nns_mgtvbk_c2_task where nns_org_id='{$sp['nns_id']}' and nns_type='media' and nns_ref_id='{$media['nns_id']}' and nns_src_id='{$media['nns_vod_index_id']}'";
			i_write_log_core($sql_c2);
			$sp_c2_task = nl_db_get_one($sql_c2, $db_r);
			i_write_log_core(var_export($sp_c2_task,true));
			if(is_array($sp_c2_task)&&$sp_c2_task!=null){
				$re = nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=0,nns_modify_time=now(),nns_status='destroy' where nns_id='{$sp_c2_task['nns_id']}'", $db_w);
				i_write_log_core("update nns_mgtvbk_c2_task  set nns_status=0,nns_modify_time=now(),nns_status='destroy' where nns_id='{$sp_c2_task['nns_id']}'");
				
				if($re===false){
					continue;
				}
			}else{
				$data = array(
					'nns_id'=>np_guid_rand($sp['nns_id']),
					'nns_type'=>'media',
					'nns_name'=>$vod_index['nns_name'],
					'nns_ref_id'=>$media['nns_id'],
					'nns_org_id'=>$sp['nns_id'],
					'nns_action'=>'destroy',
					'nns_status'=>0,
					'nns_create_time'=>date('Y-m-d H:i:s'),
					'nns_src_id'=>$media['nns_vod_index_id'],
				);
				i_write_log_core(var_export($data,true));
				$re = nl_db_insert($db_w, 'nns_mgtvbk_c2_task', $data);
				if($re===false){
					continue;
				}
			}			
		}
		nl_execute_by_db("update nns_vod_media set nns_status=0 where nns_id='{$media['nns_id']}'", $db_w);		
	}
}
