<?php
//是否开启日志
define('DEBUG',1);
//SP_ID  部署时询问研发人员
define('SP_ORG_ID','jllt');
//C2下载xml地址
define('C2_FTP','ftp://jllt_xml:jllt_xml@221.8.110.91:21/');
//华为下载片源地址
define('MOVIE_FTP','ftp://starcor:starcorm3u8@221.8.109.91:21/');
//芒果下载片源接口
define('MGTV_MEDIA_DOWNLOAD_URL','http://auth.imgo.tv/OTTBossApi/GetDownLoadUrl.ashx');
//分片片源注入时间间隔（秒）,大于次间隔的分片才加入任务中
define('MEDIA_TIME_OUT',60*5);
//华为下载图片方式:http/ftp
define('HW_DOANLOAD_IMG_METHOD','ftp');
//华为FTP方式下载图片的FTP地址
define('HW_DOANLOAD_IMG_FTP','ftp://jllt_img:jllt_img@221.8.110.91:21/');
//切片开关
define('CLIP', true);
//注入片源
define('IMPROT_MEDIA', false);
//是否下发EPG
define('IMPROT_EPG', true);
//import日志地址
define('IMPROT_EPG_LOG', dirname(dirname(dirname(__FILE__))) . '/data/log/import');
//epg请求地址
define('IMPROT_EPG_URL', 'http://10.70.1.3/nn_cms/nn_cms_manager/service/asset_import/mgtv/asset_api.php');
