<?php
/*
 * 注入片源测试
 * Created on 2013-8-12
 *
 */
ini_set('display_errors',1);
include_once dirname(dirname(__FILE__)).'/mgtv_init.php';
include_once dirname(dirname(__FILE__)).'/fjyd/init.php';
//取最新的一条任务测试
$sql = "select *  from nns_mgtvbk_clip_task where nns_state='ok' order by nns_alive_time desc limit 1";
$db = nl_get_db(NL_DB_READ);
$db->open();

$task_info = nl_db_get_one($sql,$db);//clip_task_model::get_task_by_id($task_id);
if(empty($task_info['nns_content'])){
	$task_info['nns_content'] = clip_task_model::_build_task_content($task_info);
}

$dom	= new DOMDocument('1.0', 'utf-8');
$dom -> loadXML($task_info['nns_content']);		
$xpath = new DOMXPath($dom);

$nns_vod_id = $xpath->query('/task/video')->item(0)->getAttribute('id');
$nns_vod_index_id = $xpath->query('/task/video/index_list/index')->item(0)->getAttribute('id');

$nns_media_ids = array();
$entries = $xpath->query('/task/video/index_list/index/media_list/media');
foreach($entries as $item){
	$nns_media_ids[] = $item->getAttribute('id');
}
$media_info['nns_video_id'] = $nns_vod_id;
$media_info['nns_video_index_id'] = $nns_vod_index_id;
$media_info['nns_media_ids'] = $nns_media_ids;
$media_info['nns_task_id'] = $task_info['nns_id'];

//c2注入
c2_task_model::test_add_movies_by_vod_index($media_info);
echo 'ok';
