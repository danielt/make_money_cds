<?php
set_time_limit(0);
ini_set('display_errors', 0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
$ip = i_get_ip();
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
$postdata = file_get_contents("php://input");
if(strlen($postdata) <1)
{
	nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '收到来自CDN的消息为空,IP:' . $ip . ';收到来自CDN的内容:' . $postdata, ORG_ID);
}
$file_init_dir = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/'.ORG_ID.'/init.php';
if(!file_exists($file_init_dir))
{
	nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '初始化文件不存在:[' . $file_init_dir . ']', ORG_ID);
	exit();
}
include_once $file_init_dir;
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/c2_task/c2_task.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/c2_log/c2_log.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/mgtv_v2/models/queue_task_model.php';
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------BK到CDN消息反馈开始--------------', ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '执行文件:' . __FILE__, ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '收到来自CDN的消息,IP:' . $ip . ';收到来自CDN的内容:' . $postdata, ORG_ID);
class http_notify
{
    /**
     * 传入的参数
     * @var unknown
     */
    private $mix_params = null;
    
    public $obj_dc = null;
    
    /**
     * 初始化数据
     * @param string $params
     */
    public function __construct($params=null)
    {
        $this->mix_params = $params=== null ? '' : $params;
        $this->obj_dc = nl_get_dc(array (
            "db_policy" => NL_DB_WRITE,
            "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
        ));
    }
    
    /**
     * 消息反馈执行的方法
     */
	public function notify()
	{
		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ ."传入参数为：".var_export($_REQUEST,true), ORG_ID);
		$c2_log_id = (isset($_REQUEST['id'])) ? $_REQUEST['id'] : '';
		if(isset($_REQUEST['state']) && strlen($_REQUEST['state']) >0 && strlen($c2_log_id) >0 && in_array($_REQUEST['state'], array('0','1')))
		{
		    $result_c2_log = nl_c2_log::query_data_by_id($this->obj_dc, $c2_log_id);
		    if($result_c2_log['ret'] !=0 || !isset($result_c2_log['data_info']) || empty($result_c2_log['data_info']) || !is_array($result_c2_log['data_info']))
		    {
		        nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '查询日志信息sql失败或者无数据'.var_dump($result_c2_log,true), ORG_ID);
		        nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------BK到CDN消息反馈结束--------------', ORG_ID);
		        return $result_c2_log;
		    }
		    $result_c2_log = $result_c2_log['data_info'];
		    $result_edit_c2_log = $result_edit_c2_task = null;
		    if($_REQUEST['state'] == '0')
		    {
		        $result_edit_c2_log = nl_c2_log::edit($this->obj_dc, array('nns_notify_result'=>0,'nns_notify_time'=>date("Y-m-d H:i:s"),'nns_notify_fail_reason'=>isset($_REQUEST['command']) ? $_REQUEST['command'] : ''), $c2_log_id);
		        $result_edit_c2_task = nl_c2_task::edit($this->obj_dc, array('nns_status'=>6,), $result_c2_log['nns_task_id']);
		    }
		    else if ($_REQUEST['state'] == '1')
		    {
		        $result_edit_c2_log = nl_c2_log::edit($this->obj_dc, array('nns_notify_result'=>-1,'nns_notify_time'=>date("Y-m-d H:i:s"),'nns_notify_fail_reason'=>isset($_REQUEST['command']) ? $_REQUEST['command'] : ''), $c2_log_id);
		        $result_edit_c2_task = nl_c2_task::edit($this->obj_dc, array('nns_status'=>-1,), $result_c2_log['nns_task_id']);
		    }
		    nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '修改C2_log表参数：' . var_export($result_edit_c2_log, true), ORG_ID);
		    nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '修改C2_task表参数：' . var_export($result_edit_c2_task, true), ORG_ID);
		}
		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------BK到CDN消息反馈结束--------------', ORG_ID);
		return (isset($result_edit_c2_log['ret']) && $result_edit_c2_log['ret'] == 0) ? $this->return_data(0,'ok') : $this->return_data(1,'error');
	}
    
	/**
	 * 检验字符串是否是json格式
	 * @param string $string
	 */
	private function is_json($string=null) 
	{
	    $string = strlen($string) <1 ? '' : $string;
	    json_decode($string);
	    return (json_last_error() == JSON_ERROR_NONE);
	}
	
	private function return_data($ret,$reason=null,$data_info=null)
	{
	    return array(
	        'ret'=>$ret,
	        'reason'=>($reason === null) ? '' : $reason,
	        'data_info'=>($data_info === null) ? '' : $data_info,
	    );
	}
	
}
$obj_http_notify = new http_notify($postdata);
$result = $obj_http_notify->notify();
echo json_encode($result);