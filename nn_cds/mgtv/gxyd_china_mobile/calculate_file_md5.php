<?php
set_time_limit(0);
ini_set('display_errors', 0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/sp/sp.class.php';
$ip = i_get_ip();
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
$obj_dc = nl_get_dc(array (
    "db_policy" => NL_DB_WRITE,
    "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));
$sql_media="select * from nns_vod_media where nns_media_name is null or nns_media_name='' order by nns_modify_time asc limit 50";
$result = nl_query_by_db($sql_media, $obj_dc->db());
if(!$result || empty($result) || !is_array($result))
{
    echo "no data";die;
}
$sp_config = nl_sp::query_by_id($obj_dc,ORG_ID);
$sp_config = (isset($sp_config['data_info'][0]['nns_config']) && strlen($sp_config['data_info'][0]['nns_config'])) ? json_decode($sp_config['data_info'][0]['nns_config'],true) : null;

$local_file_path = isset($sp_config['c2_import_push_media_base_url']) ? trim(trim($sp_config['c2_import_push_media_base_url'],'/'),'\\') : '';
foreach ($result as $value)
{
    $temp_media_source_url = trim(trim($value['nns_url'],'/'),"\\");
    $temp_media_source_url = (strlen($local_file_path) >0) ? '/'.$local_file_path.'/'.$temp_media_source_url : '/'.$temp_media_source_url;
    if(!file_exists($temp_media_source_url))
    {
        $obj_dc = nl_get_dc(array (
            "db_policy" => NL_DB_WRITE,
            "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
        ));
        $date = date("Y-m-d H:i:s");
        nl_execute_by_db("update nns_vod_media set nns_modify_time='{$date}' where nns_id='{$value['nns_id']}'", $obj_dc->db());
        continue;
    }
    $str_md5 = md5_file($temp_media_source_url);
    if(!$str_md5 || strlen($str_md5) !=32)
    {
        $obj_dc = nl_get_dc(array (
            "db_policy" => NL_DB_WRITE,
            "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
        ));
        $date = date("Y-m-d H:i:s");
        nl_execute_by_db("update nns_vod_media set nns_modify_time='{$date}' where nns_id='{$value['nns_id']}'", $obj_dc->db());
        continue;
    }
    nl_execute_by_db("update nns_vod_media set nns_media_name='{$str_md5}',nns_modify_time='{$date}' where nns_id='{$value['nns_id']}'", $obj_dc->db());
}
echo "ok";die;