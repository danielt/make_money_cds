<?php 
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('display_errors', 0);
set_time_limit(0);
$arr_dir = explode('|', str_replace(array ('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/sp/sp.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/models/public_model_exec.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/models/video_model.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/c2_log/c2_log.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/c2_task/c2_task.class.php';

$dc = nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_NULL
));

$sp_config = nl_sp::query_by_id($dc, ORG_ID);
$sp_config = (isset($sp_config['data_info'][0]['nns_config']) && strlen($sp_config['data_info'][0]['nns_config'])) ? json_decode($sp_config['data_info'][0]['nns_config'],true) : null;


c2_task_model::notify_cp_info($c2_info, $sp_config, $dc, $c2_notify,$sql_c2_task);