<?php
/**
 * soap 客户端
 * @author pan.liang
 */
class Qzgzz_notify
{
    private $WDSL_URL=null;
    private $CMSID=null;
    private $SOPID=null;
    private $CorrelateID=null;
    private $ResultCode=null;
    private $ErrorDescription =null;
    private $ContentMngXMLURL=null;
    private $soap_config = array ('trace' => true);
    
    public function __construct($WDSL_URL,$CMSID, $SOPID, $CorrelateID,$ResultCode,$ErrorDescription,$ContentMngXMLURL=null,$soap_config=null)
    {
        $this->WDSL_URL = $WDSL_URL;
        $this->CMSID = $CMSID;
        $this->SOPID = $SOPID;
        $this->CorrelateID = $CorrelateID;
        $this->ContentMngXMLURL = $ContentMngXMLURL;
        $this->ResultCode = $ResultCode;
        $this->ErrorDescription = $ErrorDescription;
        if(!empty($soap_config) && is_array($soap_config))
        {
            $this->soap_config = $soap_config;
        }
    }
	/**
	 * 发送 SOAP消息
	 */
	public function ExecCmd()
	{
		$client = $this->get_soap_client();
		if($this->CMSID == 'JSCN' || $this->CMSID == 'CSKJ' || $this->CMSID == 'zhongguangchuanbo' || $this->CMSID == 'shidake' || $this->CMSID == 'Qzgzz')
		{
		    return $client->ResultNotify($this->CMSID,$this->SOPID,$this->CorrelateID,$this->ResultCode,$this->ContentMngXMLURL);
		}
		else
		{
		    return $client->ContentDeployResult($this->CMSID,$this->SOPID,$this->CorrelateID,$this->ResultCode,$this->ErrorDescription,$this->ContentMngXMLURL);
		}
	}
	/**
	 * 初始化 SOAP
	 */
	private function get_soap_client()
	{
		return new SOAPClient($this->WDSL_URL, $this->soap_config);
	}
}