<?php
/**
 * soap 客户端
 * @author pan.liang
 */
class ff_notify
{
    private $WDSL_URL=null;
    private $CMSID=null;
    private $SOPID=null;
    private $CorrelateID=null;
    private $CmdFileURL=null;
    private $soap_config = array ('trace' => true);
    
    public function __construct($WDSL_URL,$CMSID, $SOPID, $CorrelateID,$CmdFileURL=null,$soap_config=null)
    {
        $this->WDSL_URL = $WDSL_URL;
        $this->CMSID = $CMSID;
        $this->SOPID = $SOPID;
        $this->CorrelateID = $CorrelateID;
        $this->CmdFileURL = $CmdFileURL;
        if(!empty($soap_config) && is_array($soap_config))
        {
            $this->soap_config = $soap_config;
        }
    }
	/**
	 * 发送 SOAP消息
	 */
	public function ExecCmd()
	{
		$client = $this->get_soap_client();
		return $client->ExecCmd($this->CMSID,$this->SOPID,$this->CorrelateID,$this->CmdFileURL);
	}
	/**
	 * 初始化 SOAP
	 */
	private function get_soap_client()
	{
		return new SOAPClient($this->WDSL_URL, $this->soap_config);
	}
}