<?php
include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/np/np_string.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv_v2/models/child_models/public_model.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/cp/cp.class.php';
class nl_make_ff_notify_file extends nn_public_model
{
	private $sp_id = null;
	private $cp_id = null;
	private $obj_dom = null;
	/**
	 * @return the $error_info
	*/
	public function get_error_info()
	{
		return $this->error_info;
	}
	
	/**
	 * @param field_type $error_info
	 */
	private function set_error_info($error_info)
	{
		$this->error_info[] = $error_info;
		return $error_info;
	}
	
	public function __construct($sp_id = null, $cp_id = null,$obj_dc=null,$sp_config=null)
	{
		$this->sp_id = $sp_id;
		$this->cp_id = $cp_id;
		$this->obj_dc = $obj_dc;
		$this->arr_sp_config = $sp_config;
	}
	
	public function check_cp_is_import($cp_id)
	{
	    if(strlen($cp_id) <1)
	    {
	        return $this->_return_data(1,'cp_id 为空不注入');
	    }
	    $result = nl_cp::get_cp_config($this->obj_dc,$cp_id);
	    if($result['ret'] !=0)
	    {
	        return $result;
	    }
	    if(isset($result['data_info']['nns_config']['cdn_notify_third_party_enable']) && $result['data_info']['nns_config']['cdn_notify_third_party_enable'] == '1')
	    {
	        return $this->_return_data(1,'cp 不允许注入上游第三方');
	    }
	    return $this->_return_data(0,'Ok');
	}
	
	
	/**
	 * 注入口
	 * @param unknown $task_id
	 * @param unknown $sp_config
	 */
	public function init($task_id)
	{
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/c2_task/c2_task.class.php';
	    $result_c2_task = nl_c2_task::query_by_id($this->obj_dc, $task_id);
	    if($result_c2_task['ret'] !=0 || !isset($result_c2_task['data_info']) || empty($result_c2_task['data_info']) || !is_array($result_c2_task['data_info']) || $result_c2_task['data_info']['nns_status'] !=0)
	    {
	        return $result_c2_task;
	    }
	    $this->arr_params['bk_c2_task_info'] = $result_c2_task['data_info'];
	    $this->str_sp_id = $result_c2_task['data_info']['nns_org_id'];
	    switch ($this->arr_params['bk_c2_task_info']['nns_type'])
	    {
	        case 'video':
	            $result_xml = $this->do_video();
	            break;
	        case 'index':
	            $result_xml = $this->do_index();
	            break;
	        case 'media':
	            $result_xml = $this->do_media();
	            break;
	        default:
	            return $this->_return_data(1,'task 不支持此队列模式');
	    }
	    if($result_xml['ret'] !=0 || !isset($result_xml['data_info']) || strlen($result_xml['data_info']) <1)
	    {
	        return $result_xml;
	    }
	    return $this->make_notify_file($result_xml['data_info'],$this->arr_params['bk_c2_task_info']['nns_type'],$this->arr_params['bk_c2_task_info']['nns_action']);
	}
	/**
	 * 返回数据数组
	 * @param number $ret
	 * @param string $str_desc 英文描述
	 * @param string $error_info 错误描述
	 * @return multitype:number string
	 * @author liangpan
	 * @date 2015-12-25
	 */
	private function _return_data($ret=1,$str_desc='',$arr_data_info=null)
	{
	    return array(
	        'ret'=>$ret,
	        'reason'=>$str_desc,
	        'data_info'=>$arr_data_info,
	    );
	}
	
	/**
	 * 生成CDN反馈文件到本地服务器
	 * @param string $content_id CDNid
	 * @param int $ResultCode 状态结果
	 * @param string $Description 错误描述
	 * @param string $video_type 影片类型
	 * @param string $action 行为动作  REGIST 注册 | UPDATE 更新 | DELETE 删除
	 * @return Ambigous <multitype:number, multitype:number string string >
	 * @return object
	 * @date 2015-05-04
	 */
	public function make_notify_file($str_xml_content,$video_type='media',$action='add')
	{
		if(strlen($this->cp_id) < 1)
		{
			return $this->_make_return_data(1,"CP ID为空：".$this->cp_id);
		}
		if(strlen($this->sp_id) < 1)
		{
			return $this->_make_return_data(1,"SP ID为空：".$this->sp_id);		
		}
		$base_dir = dirname(dirname(dirname(dirname(__FILE__)))).'/data/notify';
		$file_name = np_guid_rand();
		$str_date = date("Ymd");
		$file_dir = '/'.$this->sp_id.'/'.$this->cp_id.'/'.$video_type.'/'.$str_date.'/'.$action.'/';
		$result_make_dir = $this->make_dir($base_dir.$file_dir);
		if(!$result_make_dir)
		{
			return $this->_make_return_data(1,"文件路径生成失败".var_export($this->get_error_info(),true));
		}
		$result_make_file = $this->write_file($str_xml_content,$base_dir.$file_dir.$file_name.'.xml');
		if(!$result_make_dir)
		{
			return $this->_make_return_data(1,"文件生成失败".var_export($this->get_error_info(),true));
		}
		return $this->_make_return_data(0,"success",$file_dir.$file_name.'.xml');
	}
	
	/**
	 * 组装基础dom对象类
	 * @param string $key 键
	 * @param string $val 值
	 * @param array $arr_attr attr值
	 * @param object $parent 父级对象
	 * @return object
	 * @date 2015-05-04
	 */
	private function make_xml($key, $val = null, $arr_attr = null,$parent=null)
	{
		if(is_null($parent))
		{
			$parent=$this->obj_dom;
		}
		$$key = isset($val) ? $this->obj_dom->createElement($key, $val) : $this->obj_dom->createElement($key);
		if (!empty($arr_attr) && is_array($arr_attr))
		{
			foreach ($arr_attr as $attr_key => $attr_val)
			{
				$domAttribute = $this->obj_dom->createAttribute($attr_key);
				$domAttribute->value = $attr_val;
				$$key->appendChild($domAttribute);
				$this->obj_dom->appendChild($$key);
			}
		}
		$parent->appendChild($$key);
		//unset($dom);
		unset($parent);
		return $$key;
	}
	
	/**
	 * 创建文件路径
	 * @param string $xml 文件内容
	 * @return boolean
	 * @author liangpan
	 * @date 2016-08-24
	 */
	private function make_dir($base_file_dir)
	{
		if (is_dir($base_file_dir))
		{
			return true;
		}
		$result=mkdir($base_file_dir, 0777, true);
		if(!$result)
		{
			$this->set_error_info("创建文件夹失败，路径为：".$base_file_dir);
			return false;
		}
		return true;
	}
	
	/**
	 * 写入文件内容
	 * @param string $xml 文件内容
	 * @return boolean
	 * @author liangpan
	 * @date 2016-08-24
	 */
	private function write_file($xml,$base_file)
	{
		if(strlen($xml) < 1)
		{
			$this->set_error_info("文件内容为空不写入");
			return false;
		}
		$result = file_put_contents($base_file, $xml, LOCK_EX);
		if($result === false)
		{
			$this->set_error_info("写入文件失败，文件为：".$base_file);
			return false;
		}
		return true;
	}
	
	/**
	 * 组装主媒资信息
	 * @param unknown $arr_info 主媒资基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 */
	private function make_vod_xml($arr_info,$str_Action,$arr_info_ex=null)
	{
	    $arr_info['nns_kind'] = trim(str_replace(';;', ';',str_replace(';;', ';',str_replace('/', ';', $arr_info['nns_kind']))));
	    $arr_info['nns_kind'] = strlen($arr_info['nns_kind']) >0 ? $arr_info['nns_kind'].';' : '';
	    $arr_info['nns_alias_name'] = trim($arr_info['nns_alias_name']);
	    $arr_info['nns_alias_name'] = strlen($arr_info['nns_alias_name']) > 0 ? $arr_info['nns_alias_name'] : $arr_info['nns_name'];
	    $str_xml = '<Object ElementType="Series" ID="'.$arr_info['nns_cdn_video_guid'].'" Action="'.$str_Action.'" Code="'.$arr_info['nns_cdn_video_guid'].'">';
	    $str_xml.=     '<Property Name="Name"><![CDATA['.$arr_info['nns_name'].']]></Property>';
	    $str_xml.=     '<Property Name="OrderNumber" />';
	    $str_xml.=     '<Property Name="OriginalName"><![CDATA['.$arr_info['nns_alias_name'].']]></Property>';
	    $str_xml.=     '<Property Name="SortName"><![CDATA['.$arr_info['nns_pinyin'].']]></Property>';
	    $str_xml.=     '<Property Name="SearchName"><![CDATA['.$arr_info['nns_pinyin'].']]></Property>';
	    $str_xml.=     '<Property Name="ActorDisplay"><![CDATA['.$arr_info['nns_actor'].']]></Property>';
	    $str_xml.=     '<Property Name="WriterDisplay"><![CDATA['.$arr_info['nns_screenwriter'].']]></Property>';
	    $str_xml.=     '<Property Name="OrgAirDate" />';
	    $str_xml.=     '<Property Name="LicensingWindowStart">20161212141408</Property>';
	    $str_xml.=     '<Property Name="LicensingWindowEnd">20261212141408</Property>';
	    $str_xml.=     '<Property Name="DisplayAsNew">7</Property>';
	    $str_xml.=     '<Property Name="DisplayAsLastChance" />';
	    $str_xml.=     '<Property Name="Macrovision">1</Property>';
	    $str_xml.=     '<Property Name="Price">0.00</Property>';
	    $str_xml.=     '<Property Name="VolumnCount">'.$arr_info['nns_all_index'].'</Property>';
	    $str_xml.=     '<Property Name="Status">0</Property>';
	    $str_xml.=     '<Property Name="Description"><![CDATA['.$arr_info['nns_summary'].']]></Property>';
	    $str_xml.=     '<Property Name="Type">电视剧</Property>';
	    $str_xml.=     '<Property Name="Keywords" />';
	    $str_xml.=     '<Property Name="Tags">'.$arr_info['nns_kind'].'</Property>';
	    $str_xml.=     '<Property Name="Reserve1" />';
	    $str_xml.=     '<Property Name="Reserve2" />';
	    $str_xml.=     '<Property Name="Reserve3" />';
	    $str_xml.=     '<Property Name="Reserve4" />';
	    $str_xml.=     '<Property Name="Reserve5" />';
	    $str_xml.=     '<Property Name="Result" />';
	    $str_xml.=     '<Property Name="ErrorDescription" />';
	    $str_xml.= '</Object>';
	    return $str_xml;
	}
	
	/**
	 * 组装分集信息
	 * @param unknown $arr_info 片源基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 */
	private function make_vod_index_xml($arr_info,$str_Action,$arr_info_ex=null)
	{
	    $str_xml ='<Object ElementType="Program" ID="'.$arr_info['nns_cdn_index_guid'].'" Code="'.$arr_info['nns_cdn_index_guid'].'" Action="'.$str_Action.'">';
	    $str_xml.=	'<Property Name="Name">'.$arr_info['nns_name'].'</Property>';
	    $str_xml.=	'<Property Name="CPContentID">'.$arr_info['nns_cp_id'].'</Property>';
	    $str_xml.= 	'<Property Name="OrderNumber">1</Property>';//0: No DRM 1: BES DRM
	    $str_xml.= 	'<Property Name="OriginalName">'.$arr_info['nns_name'].'</Property>';//0: No DRM  1: BES DRM
	    $str_xml.= 	'<Property Name="SortName">1</Property>';
	    $str_xml.= 	'<Property Name="Sequence">'.$arr_info['nns_index'].'</Property>';
	    $str_xml.= 	'<Property Name="SortName">1</Property>';
	    $str_xml.= 	'<Property Name="SearchName">'.$arr_info['nns_name'].'</Property>';
	    $str_xml.= 	'<Property Name="ActorDisplay">'.$arr_info['nns_actor'].'</Property>'; //0: 4x3   1: 16x9(Wide)
	    $str_xml.= 	'<Property Name="OriginalCountry"></Property>';//0:无字幕 1:有字幕
	    $str_xml.=	'<Property Name="Language"></Property>';
	    $str_xml.=	'<Property Name="ReleaseYear"></Property>';
	    $str_xml.=	'<Property Name="OrgAirDate">'.$arr_info['nns_kbps'].'</Property>';
	    $str_xml.=	'<Property Name="LicensingWindowStart"></Property>';
	    $str_xml.=	'<Property Name="LicensingWindowEnd"></Property>';
	    $str_xml.=	'<Property Name="DisplayAsNew"></Property>';
	    $str_xml.=	'<Property Name="DisplayAsLastChance"></Property>';
	    $str_xml.=	'<Property Name="Macrovision">1</Property>';
	    $str_xml.=	'<Property Name="Description">'.$arr_info['nns_summary'].'</Property>';
	    $str_xml.=	'<Property Name="PriceTaxIn"></Property>';
	    $str_xml.=	'<Property Name="Status">1</Property>';
	    $str_xml.=	'<Property Name="SourceType">1</Property>';
	    $str_xml.=	'<Property Name="SeriesFlag">1</Property>';
	    $str_xml.=	'<Property Name="ContentProvider">'.$arr_info['nns_import_source'].'</Property>';
	    $str_xml.=	'<Property Name="KeyWords">'.$arr_info['nns_summary'].'</Property>';
	    $str_xml.=	'<Property Name="Tags"></Property>';
	    $str_xml.=	'<Property Name="ViewPoint">'.$arr_info['nns_watch_focus'].'</Property>';
	    $str_xml.=	'<Property Name="StarLevel"></Property>';
	    $str_xml.=	'<Property Name="Rating"></Property>';
	    $str_xml.=	'<Property Name="Awards"></Property>';
	    $str_xml.=	'<Property Name="Duration">'.$arr_info['nns_time_len'].'</Property>';
	    $str_xml.=	'<Property Name="Reserve1"></Property>';
	    $str_xml.=	'<Property Name="Reserve2"></Property>';
	    $str_xml.=	'<Property Name="Reserve3"></Property>';
	    $str_xml.=	'<Property Name="Reserve4"></Property>';
	    $str_xml.=	'<Property Name="Reserve5"></Property>';
	    $str_xml.='</Object>';
	    return $str_xml;
	}
	
	/**
	 * 组装片源信息
	 * @param unknown $arr_info 片源基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 * @var string FileURL
	 *     说明： 媒体文件 URL ftp://username:password@ip:port/...  标准 FTP协议 或者 http:// ip:port/...
	 *     注释： 对托管内容注入方式下填写，支持 FTP 和 HTTP 协议获取内容 对非托管内容预注入方式下 则 为 空 ， 填 写ServiceURL 属性 当FileURL和ServiceURL均有值时，
	 *	               首选采用该属性值去获取内容。注入 HLS 内容时，如果属性值为 m3u8 文件结尾，CDN 获取内容时，获取整个 m3u8 的上级目录
	 * @var string ServiceURL
	 *     说明： 内容服务 URL
	 *     注释： 需要指定 CDN 服务 URL 的内容注入/预注入内容接入或实时回源接入内容删除时填写。为 CDN 提供服务的内容服务调用 URL，当 ServiceURL 有值时，该URL 前缀信息应和 CDN 服务配置
	 *           表中对应域内容提供 商 的PlaybackURLprefix 保持一致，用户需要基于本URL 访问注入内容
	 * @var string CPContentID
	 *     说明： CP 对于媒体文件的标识
	 * @var string SourceDRMType
	 *     说明： 0: No DRM 1: BES DRM
	 * @var string DestDRMType
	 *     说明： 0: No DRM 1: BES DRM
	 * @var string AudioType
	 *     说明： 0: 其他 1: Monaural 单声道 2: Stereo 多声道 3: Two-nation monaural 双单声道 4: Two-nation stereo 双多声道  5: AC3(5:1 channel) AC3声道
	 * @var string ScreenFormat
	 *     说明： 0: 4x3   1: 16x9(Wide)
	 * @var string ClosedCaptioning
	 *     说明： 字幕标志 0:无字幕 1:有字幕
	 * @var string Duration
	 *     说明： 播放时长 HHMISSFF （时分秒帧）
	 * @var string FileSize
	 *     说明： 文件大小，单位为 Byte
	 * @var string BitRateType
	 *     说明： 码流 1: 400k 2: 700K 3:  1.3M 4: 2M 5: 2.5M 6:  8M 7: 10M  8：15M 9：20M 10：30M
	 * @var string VideoType
	 *     说明： 编码格式： 1. H.264 2. MPEG4 3. AVS 4. MPEG2 5. MP3 6. WMV 7. H.265
	 * @var string AudioEncodingType
	 *     说明： 编码格式： 1. MP2 2. AAC 3. AMR 4. Mp3
	 * @var string Resolution
	 *     说明： 分辨率类型 1. QCIF 2. QVGA 3. 2/3 D1 4. 3/4 D1 5. D1 6. 720P 7. 1080i 8. 1080P 9. 2k 11. 4k 13. 8k
	 * @var string VideoProfile
	 *     说明： 1. Simple 2. Advanced Simple 3. Baseline 4. Main 5. High 6. JiZhun
	 * @var string SystemLayer
	 *     说明： 1. TS 2. 3GP
	 * @var string Domain
	 *     说明： 发布到融合 CDN 后的服务域和服务协议
	 *     注释： 包含 ServiceDomain 和ProtocolType 两层含义，为 4 字节整数，其中最高三 个 字 节 为ServiceDomain，表示预分发网络域，最低 3 位取值如下：
	 *               0x000001-IPTV 网络
	 *               0x000002-互联网网络
	 *               0x000004-移动网络
	 *               0x000008~0x000080-预留
	 *            最 低 一 个 字 节 为ProtocolType，表示服务协议，最低 4 位取值如下：
	 *               0x01-IPTV TS RTSP，
	 *               0x02-HPD
	 *               0x04-ISMA RTSP
	 *               0x08-HLS。
	 *            如果 Domain 为空，则采用该 CMSID 缺省分发域，缺省分发域可配置
	 * @var string Hotdegree
	 *     说明： 发布到融合 CDN 时使用热度表示
	 *     注释： 0（普通），1（高） 缺省为 0，该字段值越大，表示优先级越高。CDN 应根据优先级安排处理顺序
	 * @var string deliverTime
	 *     说明： 内容分发时间
	 *     注释： 北京时间，采用十四位的字 符 串 格 式YYYYMMDDHHMMSS，例如： 20040101123000 ， 代 表 2004 年 1 月 1 日 12点 30 分 00 秒。说明 如果不携带该字段，表示立即分发
	 */
	private function make_vod_media_xml($arr_info,$str_Action,$arr_info_ex=null,$arr_index_info,$arr_video_info)
	{
	    $arr_video_info['nns_show_time'] = strlen($arr_video_info['nns_show_time']) >0  ?  date("Y",strtotime($arr_video_info['nns_show_time'])) : date("Y");
	    $arr_video_info['nns_kind'] = trim(str_replace(';;', ';',str_replace(';;', ';',str_replace('/', ';', $arr_video_info['nns_kind']))));
	    $arr_video_info['nns_kind'] = strlen($arr_video_info['nns_kind']) >0 ? $arr_video_info['nns_kind'].';' : '';
	    $SeriesFlag = $arr_video_info['nns_all_index'] > 0 ?  1 : 0 ;
	    if($arr_video_info['nns_all_index'] > 1)
	    {
	        $arr_index_info['nns_index'] ++;
	        $SeriesFlag =  1;
	        $Type =  '电视剧';
	        $name = " 第{$arr_index_info['nns_index']}集";
	    }
	    else
	    {
	        $SeriesFlag =  0;
	        $Type =  '电影';
	        $name = '';
	    }

	    $arr_video_info['nns_alias_name'] = trim($arr_video_info['nns_alias_name']);
	    $arr_video_info['nns_alias_name'] = strlen($arr_video_info['nns_alias_name']) > 0 ? $arr_video_info['nns_alias_name'] : $arr_video_info['nns_name'];
	    $str_xml = '<Object ElementType="Program" ID="'.$arr_index_info['nns_cdn_index_guid'].'" Action="'.$str_Action.'" Code="'.$arr_index_info['nns_cdn_index_guid'].'">';
	    $str_xml.=     '<Property Name="Name"><![CDATA['.$arr_video_info['nns_name'].$name.']]></Property>';
	    $str_xml.=     '<Property Name="OriginalName"><![CDATA['.$arr_video_info['nns_alias_name'].$name.']]></Property>';
	    $str_xml.=     '<Property Name="SortName"><![CDATA['.$arr_video_info['nns_pinyin'].']]></Property>';
	    $str_xml.=     '<Property Name="SearchName"><![CDATA['.$arr_video_info['nns_pinyin'].']]></Property>';
	    $str_xml.=     '<Property Name="Genre"><![CDATA['.$arr_video_info['nns_kind'].']]></Property>';
	    $str_xml.=     '<Property Name="ActorDisplay"><![CDATA['.$arr_video_info['nns_actor'].']]></Property>';
	    $str_xml.=     '<Property Name="WriterDisplay"><![CDATA['.$arr_video_info['nns_screenwriter'].']]></Property>';
	    $str_xml.=     '<Property Name="OriginalCountry"><![CDATA['.$arr_video_info['nns_area'].']]></Property>';
	    $str_xml.=     '<Property Name="Language"><![CDATA['.$arr_video_info['nns_language'].']]></Property>';
	    $str_xml.=     '<Property Name="ReleaseYear"><![CDATA['.$arr_video_info['nns_show_time'].']]></Property>';
	    $str_xml.=     '<Property Name="OrgAirDate" />';
	    $str_xml.=     '<Property Name="LicensingWindowStart">20170316112435</Property>';
	    $str_xml.=     '<Property Name="LicensingWindowEnd">20270316112435</Property>';
	    $str_xml.=     '<Property Name="DisplayAsNew"></Property>';
	    $str_xml.=     '<Property Name="DisplayAsLastChance" />';
	    $str_xml.=     '<Property Name="Macrovision">1</Property>';
	    $str_xml.=     '<Property Name="Description"><![CDATA['.$arr_video_info['nns_summary'].']]></Property>';
	    $str_xml.=     '<Property Name="PriceTaxIn">0</Property>';
	    $str_xml.=     '<Property Name="Status">0</Property>';
	    $str_xml.=     '<Property Name="SourceType" />';
	    $str_xml.=     '<Property Name="SeriesFlag">'.$SeriesFlag.'</Property>';
	    $str_xml.=     '<Property Name="Type">'.$Type.'</Property>';
	    $str_xml.=     '<Property Name="Keywords" />';
	    $str_xml.=     '<Property Name="Tags" />';
	    $str_xml.=     '<Property Name="Reserve1" />';
	    $str_xml.=     '<Property Name="Reserve2" />';
	    $str_xml.=     '<Property Name="Reserve3" />';
	    $str_xml.=     '<Property Name="Reserve4" />';
	    $str_xml.=     '<Property Name="Reserve5" />';
	    $str_xml.=     '<Property Name="EX_Tag" />';
	    $str_xml.=     '<Property Name="OrderNumber" />';
	    $str_xml.= '</Object>';
	    $str_xml.= '<Object ElementType="Movie" ID="'.$arr_info['nns_cdn_media_guid'].'" Action="'.$str_Action.'" Code="'.$arr_info['nns_cdn_media_guid'].'">';
	    $str_xml.=	   '<Property Name="Type">1</Property>';
	    $str_xml.=	   '<Property Name="FileURL"><![CDATA['.$arr_info['nns_url'].']]></Property>';
	    $str_xml.=	   '<Property Name="SourceDRMType">0</Property>';
	    $str_xml.=	   '<Property Name="DestDRMType">0</Property>';
	    $str_xml.=	   '<Property Name="AudioType">0</Property>';
	    $str_xml.=     '<Property Name="ScreenFormat">0</Property>';
	    $str_xml.=     '<Property Name="ClosedCaptioning">0</Property>';
	    $str_xml.='</Object>';
	    return $str_xml;
	}
	
	/**
	 * 点播主媒资CDN
	 * @param unknown $movie_id
	 */
	public function do_video()
	{
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_video_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $video_id = $this->arr_params['bk_c2_task_info']['nns_ref_id'];
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	
	    $result_video = nl_vod::get_video_info_by_id($this->obj_dc, $video_id);
	    if($result_video['ret'] !=0)
	    {
	        return $result_video;
	    }
	    if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
	    {
	        return $result_video;
	    }
	    $result_video = $result_video['data_info'][0];
	    if($result_video['nns_all_index'] <= 1)
	    {
	        return $this->_make_return_data(0,'主媒资为单集不注入');
	    }
	    $check_cp_is_import = $this->check_cp_is_import($result_video['nns_cp_id']);
	    if($check_cp_is_import['ret'] !=0)
	    {
	        return $check_cp_is_import;
	    }
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $video_id,$result_video);
	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        return $result_video_import_info;
	    }
	    $result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
	     
	    $action = ($this->arr_params['bk_c2_task_info']['nns_action'] !='destroy') ? ($this->arr_params['bk_c2_task_info']['nns_action'] == 'update') ? 'UPDATE' : 'REGIST' : 'DELETE';
	    // 	    $action = ($result_video['nns_deleted'] !=1) ? ($result_video['nns_modify_time'] > $result_video['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
	    $result_picture = $this->make_image($result_video, 'Series', $result_video['nns_cdn_video_guid'], $action);
	    $xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
	    $xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
	    $xml_str .= 	'<Objects>';
	    $xml_str .=     $this->make_vod_xml($result_video,$action);
	    if(isset($result_picture['picture']) && strlen($result_picture['picture']) >0)
	    {
	        $xml_str .= $result_picture['picture'];
	    }
	    $xml_str .= 	'</Objects>';
	    if(isset($result_picture['mapping']) && strlen($result_picture['mapping']) >0)
	    {
	        $xml_str .= 	'<Mappings>';
	        $xml_str .= $result_picture['mapping'];
	        $xml_str .= 	'</Mappings>';
	    }
	    $xml_str .= '</ADI>';
	    return $this->_return_data(0,'ok',$xml_str);
	}
	
	
	/**
	 * 点播分集CDN
	 * @param unknown $movie_id
	 */
	public function do_index()
	{
	    return $this->_make_return_data(0,'分集不单独注入');
	}
	
	
	/**
	 * 片源CDN
	 * @param unknown $movie_id
	 */
	public function do_media()
	{
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_media_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $movie_id = $this->arr_params['bk_c2_task_info']['nns_ref_id'];
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	    $result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $movie_id);
	    if($result_media['ret'] !=0)
	    {
	        return $result_media;
	    }
	    if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
	    {
	        return $result_media;
	    }
	    $result_media = $result_media['data_info'];
	    $check_cp_is_import = $this->check_cp_is_import($result_media['nns_cp_id']);
	    if($check_cp_is_import['ret'] !=0)
	    {
	        return $check_cp_is_import;
	    }
	    $result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $movie_id,$result_media);
	    if((int)$this->arr_sp_config['disabled_clip'] !== 2 && (int)$this->arr_sp_config['disabled_clip'] !== 3)
	    {
            $result_media['nns_url'] = '';
	    }
	    else
	    {
            $result_media['nns_url'] = '';
	    }
	
	    if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
	    {
	        return $result_media_import_info;
	    }
	    $result_media['nns_cdn_media_guid'] = $result_media_import_info['data_info'];
	
	    $result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $result_media['nns_vod_index_id']);
	    if($result_index['ret'] !=0)
	    {
	        return $result_index;
	    }
	    if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
	    {
	        return $result_index;
	    }
	    $result_index = $result_index['data_info'][0];
	    $result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $result_media['nns_vod_index_id'],$result_index);
	    if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
	    {
	        return $result_index_import_info;
	    }
	    $result_index['nns_cdn_index_guid'] = $result_index_import_info['data_info'];
	    	
	    	
	    $result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_media['nns_vod_id']);
	    if($result_video['ret'] !=0)
	    {
	        return $result_video;
	    }
	    if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
	    {
	        return $result_video;
	    }
	    $result_video = $result_video['data_info'][0];
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_index['nns_vod_id'],$result_video);
	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        return $result_video_import_info;
	    }
	    $result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
	    $action = ($this->arr_params['bk_c2_task_info']['nns_action'] !='destroy') ? ($this->arr_params['bk_c2_task_info']['nns_action'] == 'update') ? 'UPDATE' : 'REGIST' : 'DELETE';
	    
	    $flag = ($result_video['nns_all_index'] > 1) ? false : true;
	    $xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
	    $xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
	    $xml_str .= 	'<Objects>';
	    $xml_str .=       $this->make_vod_media_xml($result_media,$action,null,$result_index,$result_video);
	    if($flag)
	    {
	        $result_picture = $this->self_make_image($result_video, 'Program', $result_index['nns_cdn_index_guid'], $action);
	        if(isset($result_picture['picture']) && strlen($result_picture['picture']) >0)
	        {
	            $xml_str .= $result_picture['picture'];
	        }
	    }
	    $xml_str .= 	'</Objects>';
	    $xml_str .= 	'<Mappings>';
	    $xml_str .=     $this->make_mapping('Program','Movie',$result_index['nns_cdn_index_guid'],$result_media['nns_cdn_media_guid'],$action);
	    if($flag)
	    {
	        if(isset($result_picture['mapping']) && strlen($result_picture['mapping']) >0)
	        {
	           $xml_str .= $result_picture['mapping'];
	        }
	    }
	    else 
	    {
	        $int_index_number = $result_index["nns_index"] + 1;
	        $xml_str .=     $this->make_mapping('Series','Program',$result_video['nns_cdn_video_guid'],$result_index['nns_cdn_index_guid'],$action, array("Sequence"=>$int_index_number));
	    }
	    $xml_str .= 	'</Mappings>';
	    $xml_str .= '</ADI>';
	    return $this->_return_data(0,'ok',$xml_str);
	}
	
	/**
	 * C2
	 * @param unknown $c2_info
	 */
	public function execute_fufu_c2($c2_info)
	{
	    $c2_info['nns_type'] = 'std';
	    $c2_info['nns_org_id'] = $this->str_sp_id;
	    $c2_id = np_guid_rand();
	    $c2_info['nns_id'] = $c2_id;
	    //加载soap客户端文件
	    $file_soap_client_file = dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv/' . $this->str_sp_id . '/client.php';
	    if(!file_exists($file_soap_client_file))
	    {
	        return $this->_make_return_data(1,'加载soap客户端文件不存在,文件路径:['.$file_soap_client_file.']');
	    }
	    global $g_bk_web_url;
	    $bk_web_url = (isset($g_bk_web_url) && strlen($g_bk_web_url)>0) ? $g_bk_web_url : '';
	    unset($g_bk_web_url);
	    $bk_web_url = ltrim(ltrim(trim($bk_web_url),'/'),'\\');
	    if(strlen($bk_web_url) <1)
	    {
	        return $this->_make_return_data(1,'播控后台系统配置管理里面后台基本地址未配置:['.$bk_web_url.']');
	    }
	    include_once $file_soap_client_file;
	    //检查时间范围是否有效执行
	    if ($this->exists_c2_commond($c2_info))
	    {
	        return $this->_make_return_data(1,'该task在一小时内执行过或则task查询无数据,task信息:'.var_export($c2_info,true));
	    }
	    $sub_path = date('Y-m-d');
	    $file = $c2_info['nns_url'];
	    $c2_info['nns_url'] = $sub_path . '/' . $file;
	    //生成soap xml文件 路径
	    $file_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/mgtv/fufu/inject_xml/' . $sub_path . '/';
	    if (!is_dir($file_path))
	    {
	        mkdir($file_path, 0777, true);
	    }
	    $file_path .= $file;
	    file_put_contents($file_path, $c2_info['nns_content']);
	    $CmdFileURL = trim(rtrim($this->arr_sp_config['xml_ftp'],'/')) . '/' . trim(ltrim($c2_info['nns_url'],'/'));
	    try
	    {
	        $result = null;
	        $obj_soap = new client();
	        $result = $obj_soap->ContentDeployReq($c2_info['nns_id'], $CmdFileURL,$bk_web_url,$c2_info['nns_task_type']);
	        $result = isset($result->Result) ? $result->Result : isset($result->ResultCode) ? $result->ResultCode : $result->Result;
	        $result = ($result === null) ? 0 : $result;
	        $c2_info['nns_result'] = '[' . $result . ']';
	        nl_c2_log::add($this->obj_dc, $c2_info);
	        $str_ret = ($result != 0) ? 1 : 0;
	        return $this->_make_return_data($str_ret,var_export($result,true));
	    }
	    catch (Exception $e)
	    {
	        $c2_info['nns_result'] = '[-1]';
	        $c2_info['nns_notify_result'] = -1;
	        nl_c2_log::add($this->obj_dc, $c2_info);
	        return $this->_make_return_data(1,"soap客户端类文件获取异常:[{$file_soap_client_file}]");
	    }
	}
	
	
	/**
	 * 组装img
	 * @param unknown $arr_video
	 * @param unknown $type
	 */
	public function self_make_image($arr_video,$parent_type,$parent_id,$action)
	{
	    $arr_map = null;
	    $str_picture_obj = $str_picture_mapping = '';
	    $img_url = isset($this->arr_sp_config['img_ftp']) ? $this->arr_sp_config['img_ftp'] : '';
	    $img_url = rtrim(rtrim(trim($img_url),'/'),'\\');
	    switch ($parent_type)
	    {
	        case 'Series':
	            $arr_map = $this->arr_vod_video_img;
	            break;
	        case 'Program':
	            $arr_map = $this->arr_vod_video_img;
	            break;
	        case 'Channel':
	            $arr_map = $this->arr_channel_img;
	            break;
	        case 'ScheduleRecord':
	            $arr_map = $this->arr_playbill_img;
	            break;
	    }
	    if(!is_array($arr_map) || empty($arr_map))
	    {
	        return null;
	    }
	    foreach ($arr_map as $key=>$val)
	    {
	        if(isset($arr_video[$key]) && strlen($arr_video[$key]) >0)
	        {
	            $id = md5($key.$arr_video[$key]);
	            $picture = array(
	                'FileURL'=>$img_url.'/'.ltrim(ltrim(trim($arr_video[$key]),'/'),'\\'),
	                'Type'=>$val,
	                'Description'=>'',
	            );
	            $str_picture_obj .= $this->make_object('Picture', $id,$action,$picture);
	            if(isset($this->arr_sp_config['c2_import_cdn_mapping_model']) && $this->arr_sp_config['c2_import_cdn_mapping_model'] == '1')
	            {
	                $str_picture_mapping .= $this->make_mapping($parent_type, 'Picture', $parent_id, $id, $action);
	            }
	            else
	            {
	                $str_picture_mapping .= $this->make_mapping('Picture', $parent_type, $id, $parent_id, $action);
	            }
	        }
	    }
	    return array(
	        'picture'=>$str_picture_obj,
	        'mapping'=>$str_picture_mapping,
	    );
	}
}
