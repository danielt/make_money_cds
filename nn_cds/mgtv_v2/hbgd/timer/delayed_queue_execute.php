<?php
/**
 * Created by PhpStorm.
 * User: song.wang
 * Date: 2018/10/29
 * Time: 22:54
 */
header('Content-type: text/html; charset=utf-8');
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
$arr_dir = explode('|',str_replace(array('/','\\'), '|', dirname(__DIR__)));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/' . ORG_ID . '/init.php';
include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/np/np_redis_check.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/c2_task/c2_task.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_model/message/message_explain.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_core/m_const.php';
class delayed_queue_execute extends nn_timer
{
    private $int_execute_data_max_num = 1000;
    /**
     * 执行入口
     */
    public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->do_run();
        $this->msg('执行结束...');
    }
    /**
     * 真正开始的地方
     */
    public function do_run()
    {
        $message_explain = new \ns_model\message\message_explain();
        // 获取延迟队列中的数据
        $arr_redis_config = array(
            'queue_name'        => 'hbgd',
            'command_lose_time' => 86400
        );
        $obj_redis = nl_get_redis();
        $obj_redis_check = new np_redis_check_class($obj_redis, $arr_redis_config);
        $arr_list = $obj_redis_check->get_command($this->int_execute_data_max_num);
        if (empty($arr_list))
        {
            $this->msg('延迟队列中没有数据');
            return true;
        }
        foreach ($arr_list as $value)
        {
            $command = json_decode($value['command'],1);
            $temp_value = array();
            $temp_value['base_info'] = $command['base_info'];
            $temp_value['ex_info'] = $command['ex_info'];
            //判断类型
            if(in_array('vod',$command['type']))
            {
                $result = $message_explain->vod_action('add',$temp_value);
            }
            elseif(in_array('index',$command['type']))
            {
                $result = $message_explain->index_action('add',$temp_value);
            }
            elseif(in_array('media',$command['type']))
            {
                $result = $message_explain->media_action('add',$temp_value);
            }
            //注入失败，再放回去。
            if($result['ret'] != 0)
            {
                $obj_redis_check->add_command($value['command']);
            }
        }
        return true;
    }
}
$obj_delayed_queue_execute = new delayed_queue_execute('delayed_queue_execute', 'public', __FILE__);
$obj_delayed_queue_execute->run();
echo 'ok';