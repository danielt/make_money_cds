<?php
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
define('ORG_ID', 'shanghai_mobile');
include_once dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/init.php';

class cdn_query_mediaurl_execute extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        do_timer_action();
        $this->msg('执行结束...');
    }
}
$cdn_query_mediaurl_execute = new cdn_query_mediaurl_execute("cdn_query_mediaurl_execute", ORG_ID);
$cdn_query_mediaurl_execute->run();

function do_timer_action()
{
    //从任务表取内容，调用C2执行
    i_echo('start');
    $sp_config = sp_model::get_sp_config(ORG_ID);
    if (isset($sp_config['disabled_cdn']) && (int)$sp_config['disabled_cdn'] === 1) {
        i_echo('close cdn');
    } else {
        $re = content_task_model::do_query_media_url(ORG_ID);
        i_echo('end');
    }
}

