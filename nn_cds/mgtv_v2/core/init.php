<?php
include_once dirname(__FILE__).'/define.php';
function core_model_autoload($classname){
$file = dirname(dirname(__FILE__))."/common/models/".$classname.".php";
if(file_exists($file)){
include_once $file;
return true;
}
return false;
}
spl_autoload_register("core_model_autoload");
