<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors',1);
set_time_limit(0); 
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_init.php';
define('ORG_ID', 'fjyd');
include_once  dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/init.php';
include_once dirname(dirname(dirname(__FILE__))) .'/models/queue_task_model.php';
$sp_config = sp_model::get_sp_config(ORG_ID);
if(isset($sp_config['op_pre_max'])&&(int)$sp_config['op_pre_max']>0){
	$max_num = $sp_config['op_pre_max'];
}else{
	$max_num = 30;
}
if($max_num>$num){
	$queue_task_model = new queue_task_model();	
	$queue_task_model->q_get_task(ORG_ID,$max_num);
}
unset($queue_task_model,$sp_config);