<?php
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
define('ORG_ID', 'sihua');
define('SEEKPOINT_MODEL', 'import_seekpoint');
define('SEEKPOINT_BEGIN', 0);
define('SEEKPOINT_LIMIT', 2);
include_once dirname(dirname(dirname(__FILE__))) . '/' . ORG_ID . '/init.php';
//从任务表取内容，调用C2执行
//i_echo('start');
class epg_index_seekpoint_execute extends nn_timer
{

	public function action($params = null)
	{
		$this->msg('开始执行...');
		do_timer_action();
		$this->msg('执行结束...');
	}
}
$epg_index_seekpoint_execute = new epg_index_seekpoint_execute("epg_index_seekpoint_execute", ORG_ID);
$epg_index_seekpoint_execute->run();

function do_timer_action()
{
	$sp_config = sp_model::get_sp_config(ORG_ID);
	if (isset($sp_config['disabled_epg']) && (int)$sp_config['disabled_epg'] === 1)
	{
		i_echo('close epg');
		die();
	}
	$dc = nl_get_dc(array (
			'db_policy' => NL_DB_READ
	));
	include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_logic/seekpoint/seekpoint.class.php";
	$control_file = dirname(dirname(dirname(dirname(__FILE__)))) . "/data/log/nn_timer/" . ORG_ID . "/" . SEEKPOINT_MODEL . ".txt";
	$result_vod_count = nl_seekpoint::get_seekpoint_info_group_by_video_id($dc, false);
	if (!$result_vod_count || !is_array($result_vod_count))
	{
		i_echo('select count db error');
		i_echo('end');
		die();
	}
	if ($result_vod_count[0]['num'] <= 0)
	{
		i_echo('no data');
		i_echo('end');
		die();
	}
	$file_info = get_running_flag($control_file);
	$count = ceil($result_vod_count[0]['num'] / SEEKPOINT_LIMIT);
	if (empty($file_info))
	{
		set_running_flag(0, $count, $control_file);
		$begin = SEEKPOINT_BEGIN;
	}
	else
	{
		$arr_flag = explode(',', $file_info);
		$arr_flag[0]++;
		$begin = $arr_flag[0];
		if ($arr_flag[1] == $arr_flag[0])
		{
			set_running_flag(0, $count, $control_file);
			$begin = SEEKPOINT_BEGIN;
		}
		else
		{
			set_running_flag($arr_flag[0], $count, $control_file);
		}
	}
	$result_vod = nl_seekpoint::get_seekpoint_info_group_by_video_id($dc, true, $begin, SEEKPOINT_LIMIT);
	if (!$result_vod)
	{
		i_echo('query vod id db error');
		i_echo('end');
		die();
	}
	if (!is_array($result_vod))
	{
		i_echo('end');
		die();
	}
	foreach ($result_vod as $val)
	{
		$result_index = nl_seekpoint::get_seekpoint_info_by_video_id($dc, $val['nns_video_id']);
		if (!$result_index)
		{
			i_echo('query vod index db error');
			i_echo('end');
			die();
		}
		if (!is_array($result_index))
		{
			continue;
		}
		foreach ($result_index as $_v)
		{
			$result_val = nl_seekpoint::get_seekpoint_info_by_video_index_id($dc, $val['nns_video_id'], $_v['nns_video_index']);
			if (!$result_val)
			{
				i_echo('query guid index id db error');
				i_echo('end');
				die();
			}
			if (!is_array($result_val))
			{
				continue;
			}
			if (empty($result_val[0]['nns_index_id']))
			{
				continue;
			}
			import_model::import_clip_seekpoint($result_val[0]['nns_index_id'], $result_val);
		}
	}
	i_echo('end');
}
function set_running_flag($begin, $end, $control_file)
{
	$str_log_dir = dirname($control_file);
	if (!is_dir($str_log_dir))
	{
		mkdir($str_log_dir, 0777, true);
	}
	$controls = $begin . ',' . $end;
	file_put_contents($control_file, $controls);
}

function get_running_flag($control_file)
{
	$str_log_dir = dirname($control_file);
	if (!is_dir($str_log_dir))
	{
		mkdir($str_log_dir, 0777, true);
	}
	return @file_get_contents($control_file);
}