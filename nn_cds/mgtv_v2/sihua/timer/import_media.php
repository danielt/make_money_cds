<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
define('ORG_ID', 'sihua');
define('OLD_ORG_ID', 'shanghai_mobile');
include_once dirname(dirname(dirname(__FILE__))) . '/' . ORG_ID . '/init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/models/queue_task_model.php';
class import_media extends nn_timer
{

	public function action($params = null)
	{
		$this->msg('开始执行...');
		do_timer_action();
		$this->msg('执行结束...');
	}
}
$import_media = new import_media("import_media", ORG_ID);
$import_media->run();

function do_timer_action()
{
	$queue_task = new queue_task_model();
	
	$db = nn_get_db(NL_DB_WRITE);
	$db->open();
	
	$since = 0;
	$num = 1;
	while ($result = get_c2_task($db, $since))
	{
		$since += $num;
		//检测思华的状态
		if (check_c2_task($db, $result[0]['nns_ref_id']))
		{
			continue;
		}
		$queue_task->q_add_task($result[0]['nns_ref_id'], $result[0]['nns_type'], $result[0]['nns_action'], ORG_ID);
	}
}
function get_c2_task($db, $num)
{
	$sql = "select * from nns_mgtvbk_c2_task where nns_type='media' and nns_org_id='" . OLD_ORG_ID . "' limit {$num},1";
	$result = nl_query_by_db($sql, $db);
	if (!is_array($result))
	{
		return false;
	}
	return $result;
}

function check_c2_task($db, $media_id)
{
	//查询是否存在此片源注入成功的
	$sql = "select count(1) as num from nns_mgtvbk_c2_task where nns_type='media' and nns_org_id'" . ORG_ID . " 
			and nns_ref_id='{$media_id}' and nns_status = 0";
	$result = nl_query_by_db($sql, $db);
	//没有数据
	if ($result[0]['num'] < 1)
	{
		return false;
	}
	return true;
}