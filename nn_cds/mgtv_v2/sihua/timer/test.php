<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors',1);
set_time_limit(0); 
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_init.php';
define('ORG_ID', 'sihua');
include_once  dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/init.php';
include_once dirname(dirname(dirname(__FILE__))) .'/models/queue_task_model.php';

$op_queue =new op_queue();

$sql = "select * from nns_mgtvbk_op_queue where nns_org_id='sihua' 
and nns_type='media' and nns_action!='destroy' and nns_status=0 
order by nns_weight desc,nns_release_time desc,nns_video_id asc,nns_op_mtime asc limit 0,5000
";
$db = nn_get_db(NL_DB_WRITE);
$db->open();

$infos = nl_db_get_all($sql, $db);

foreach ($infos as $info) {
    
    $sql_task = "select * from nns_mgtvbk_c2_task where nns_org_id='sihua' and nns_type='media' and nns_ref_id='{$info['nns_media_id']}' and nns_src_id='{$info['nns_index_id']}'";
    $task_info = nl_db_get_one($sql_task, $db);    
    if(!is_array($task_info)){
        unset($sql_task,$task_info);    
        continue;
    }
    $update = "update nns_mgtvbk_c2_task set nns_op_id='{$info['nns_id']}',nns_status=1,nns_epg_status=97 where nns_id='{$task_info['nns_id']}'";
    nl_execute_by_db($update, $db);
    unset($update);
    
    $op_queue->task_progress($db, $info['nns_id']);
    
}


?>