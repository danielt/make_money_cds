<?php
/*
 * Created on 2013-8-24
 *
 * 下载失败的图片重新下载
 *
 */
error_reporting(E_ALL);
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
include_once dirname(dirname(__FILE__)) . '/init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/models/curl.class.php';
include_once (NPDIR . DIRECTORY_SEPARATOR . 'np_ftp.php');
define('ORG_ID', 'sihua');
// 图片保留路径
define("IMPORT_IMG_CATEGORY", 0);
// 上传图片后，是否删除本地图片
define("IMPORT_DEL_LOCAL_IMG", true);

//print_r($g_ftp_conf);
//die;
class download_img_execute extends nn_timer
{

	public function action($params = null)
	{
		$this->msg('开始执行...');
		do_timer_action();
		$this->msg('执行结束...');
	}
}
$download_img_execute = new download_img_execute("download_img_execute", ORG_ID);
$download_img_execute->run();

function do_timer_action()
{
	i_echo('------start download images:------');
	//
	$sql = "select *  from nns_vod  limit 1";
	$db = nl_get_db(NL_DB_READ);
	$db->open();
	
	$db_w = nl_get_db(NL_DB_WRITE);
	$db_w->open();
	
	$vod_arr = nl_db_get_all($sql, $db);
	//$root_dir = dirname(dirname(dirname(dirname(__FILE__)))).'/data/downimg/';
	//$dowload_img_url = "http://218.207.214.121/nn_mgtvbk/data/downimg/";
	$i = 0;
	$j = 0;
	foreach ($vod_arr as $vod_item)
	{
		if (!empty($vod_item['nns_image2']))
		{
			$img_save_name = str_replace('.JPG', '.jpg', $vod_item['nns_image2']);
			//echo $img_save_name;
			img_handel($img_save_name);
		}
	}
}

function img_handel($img_from = "")
{
	global $g_ftp_conf;
	if (empty($img_from))
	{
		die(1);
		return false;
	}
	//组装保存在本地的临时图片名称
	$img_name = basename($img_from);
	//print_r($img_name);
	$img_dir = $g_ftp_conf["down_img_dir"] . '/' . $img_name;
	
	//die($img_dir);
	//配置在子配置文件里面
	if (!empty($g_ftp_conf["down_img_dir"]) && !is_dir($g_ftp_conf["down_img_dir"]))
	{
		$flg = mkdir($g_ftp_conf["down_img_dir"], 0777, true);
		//递归创建
		if (!$flg)
		{
			die(2);
			return false;
		}
	}
	else
	{
		@chmod($g_ftp_conf["down_img_dir"], 0777);
	}
	
	if (stripos($img_from, 'http://') === FALSE && stripos($img_from, 'https://') === FALSE)
	{
		if (!isset($g_ftp_conf["domain"]) || empty($g_ftp_conf["domain"]))
		{
			die(3);
			return false;
		}
		$down_img = $g_ftp_conf["domain"] . $img_from;
	}
	else
	{
		$down_img = $img_from;
	}
	$setopt = array (
			'port' => '80'
	);
	
	$cu = new curl($setopt);
	$contents = $cu->get($down_img);
	$getinfo = $cu->getInfo();
	$infocode = $getinfo['after']['http_code'];
	if ($contents === false || $infocode != '200')
	{
		$error = $cu->error();
		$error_no = $cu->errno();
		die(4);
		return false;
	}
	
	$handle = fopen($img_dir, "w");
	$re = fwrite($handle, $contents);
	fclose($handle);
	if (empty($re))
	{
		die(5);
		return false;
	}
	
	$remote_path = '';
	
	if (defined("ROOT_IMG_CATEGORY") && ROOT_IMG_CATEGORY)
	{
		$remote_path = ROOT_IMG_CATEGORY;
	}
	
	if (defined("IMPORT_IMG_CATEGORY") && IMPORT_IMG_CATEGORY)
	{
		$img_extend_dir = dirname($img_from);
		for ($num = 0; $num < IMPORT_IMG_CATEGORY; $num++)
		{
			$img_extend_dir = dirname($img_extend_dir);
		}
		
		$img_extend_dir = str_replace($img_extend_dir, "", dirname($img_from));
		
		$remote_path = $remote_path . '/' . $img_extend_dir;
	}
	
	$img_url = false;
	//print_r($remote_path);
	//die;
	$ftp_result = __import_up_ftp_img(1, $img_name, $img_dir, $remote_path);
	//var_dump($ftp_result);
	if ($ftp_result === FALSE)
	{
		die(6);
		return FALSE;
	}
	$img_url = true;
	//if (defined("IMPORT_DEL_LOCAL_IMG") && IMPORT_DEL_LOCAL_IMG) {
	//	unlink($img_dir);
	//}
	return $img_url;
}

function __import_up_ftp_img($num, $img_name, $img_dir, $extend_dir = '')
{
	global $g_ftp_conf;
	//print_r($g_ftp_conf);die;
	$ftp_key = 'ftp_to' . $num;
	if (!$g_ftp_conf[$ftp_key])
	{
		die(7);
		return false;
	}
	$passive = isset($g_ftp_conf[$ftp_key]['passive']) && $g_ftp_conf[$ftp_key]['passive'] === false ? false : true;
	//实例化np_ftp类
	$ftp_to1 = new nns_ftp($g_ftp_conf[$ftp_key]["address"], $g_ftp_conf[$ftp_key]["user"], $g_ftp_conf[$ftp_key]["password"], $g_ftp_conf[$ftp_key]["port"]);
	$re = $ftp_to1->connect(30, $passive);
	if (!$ftp_to1->connect(30, $passive))
	{
		die(8);
		return false;
	}
	$remote_path = rtrim($g_ftp_conf[$ftp_key]["img_dir"], '/');
	if ($extend_dir != '')
	{
		$remote_path = $remote_path . '/' . $extend_dir;
	}
	$up_img1 = $ftp_to1->up($remote_path, $img_name, $img_dir);
	if (!$up_img1)
	{
		die(10);
		return false;
	}
	return true;
}