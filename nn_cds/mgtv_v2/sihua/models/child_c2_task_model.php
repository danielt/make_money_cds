<?php

// 图片保留路径
define("IMPORT_IMG_CATEGORY", 0);
// 上传图片后，是否删除本地图片
define("IMPORT_DEL_LOCAL_IMG", true);


class child_c2_task_model {
    
    static $providerID = 'HNBB';
    static $providerType = 2;

    /**
     *内地=1,港台=2,韩日=3,欧美=4, 东南亚=5，其他=99
     */
    static $country_map = array(
        array('key'=>'香港','val'=>2),
        array('key'=>'中国','val'=>1),
        array('key'=>'台湾','val'=>2),
        array('key'=>'其他','val'=>99),
        array('key'=>'韩国','val'=>3),
        array('key'=>'美国','val'=>4),
        array('key'=>'法国','val'=>4),
        array('key'=>'英国','val'=>4),
        array('key'=>'日本','val'=>3),
        array('key'=>'内地','val'=>1),
        array('key'=>'德国','val'=>4),
    );
    
    static public function get_image_url($img_path,$sp_id){
        include dirname(dirname(__FILE__)).'/define.php';
        if(defined('HW_DOANLOAD_IMG_METHOD') && HW_DOANLOAD_IMG_METHOD=='ftp'){
            $img_url = HW_DOANLOAD_IMG_FTP.$img_path;
        }else{
            $img_url = nl_image::epg_video_image_url($img_path);
        }
        return $img_url;
    }
    
    /**
     * 获取国家的映射
     */

    static public function get_country($country){
        $result = 99;
        foreach(self::$country_map as $key=>$val){
            if($val['key']==$country){
                $result = $val['val'];
                break;
            }
        }
        return $result;
    }
    
    
    
    
    /**
     * 新增
     */
    
    static public function add_live($info,$sp_id){
        $action = "REGIST";
        $program_id = $info['nns_id'];
        $xml_str = '<adi:ADI2 xmlns="http://www.cablelabs.com/VODSchema/default" xmlns:adi="http://www.cablelabs.com/VODSchema/adi" xmlns:vod="http://www.cablelabs.com/VODSchema/vod">';
        $c2_id = np_guid_rand();
        //print_r($info);die;
        $xml_str .= self::child_do_live($info, $action, $c2_id,$sp_id);
        $xml_str .= '</adi:ADI2>';
        $file_name = 'live_'.$program_id.'_'.$action.'.xml';
        $c2_info = array(
        'nns_id'=>$c2_id,
        'nns_task_type'=>'Channel',
        'nns_task_id'=> $info['nns_id'],
        'nns_task_name'=> $info['nns_name'],
        'nns_action'=>  $action,
        'nns_url' => $file_name,
        'nns_content' => $xml_str,
        'nns_desc' => 'Channel,'.$action,
        );
        
        $re =  self::execute_c2_child($c2_info,$sp_id);
        
        //sleep(1);
        
        //self::add_live_media($info, $sp_id);
        return $re;
        
    }
    
    /**
     * 更新
     */
    static public function update_live($info,$sp_id){
        $action = "UPDATE";
        $program_id = $info['nns_id'];
        $xml_str = '<adi:ADI2 xmlns="http://www.cablelabs.com/VODSchema/default" xmlns:adi="http://www.cablelabs.com/VODSchema/adi" xmlns:vod="http://www.cablelabs.com/VODSchema/vod">';
        $c2_id = np_guid_rand();
        $xml_str .= self::child_do_live($info, $action, $c2_id,$sp_id);
        
        $xml_str .= '</adi:ADI2>';
        $file_name = 'live_'.$program_id.'_'.$action.'.xml';
        $c2_info = array(
        'nns_id'=>$c2_id,
        'nns_task_type'=>'Channel',
        'nns_task_id'=> $info['nns_id'],
        'nns_task_name'=> $info['nns_name'],
        'nns_action'=>  $action,
        'nns_url' => $file_name,
        'nns_content' => $xml_str,
        'nns_desc' => 'Channel,'.$action,
        );
        
        return self::execute_c2_child($c2_info,$sp_id);
    }
    
    /**
     * 删除
     */
    static public function delete_live($info,$sp_id){
        $action = "DELETE";
        $program_id = $info['nns_id'];
        $xml_str = '<adi:ADI2 xmlns="http://www.cablelabs.com/VODSchema/default" xmlns:adi="http://www.cablelabs.com/VODSchema/adi" xmlns:vod="http://www.cablelabs.com/VODSchema/vod">';
        $c2_id = np_guid_rand();
        $xml_str .= self::child_do_live($info, $action, $c2_id,$sp_id);
        $xml_str .= '</adi:ADI2>';
        $file_name = 'live_'.$program_id.'_'.$action.'.xml';
        $c2_info = array(
        'nns_id'=>$c2_id,
        'nns_task_type'=>'Channel',
        'nns_task_id'=> $info['nns_id'],
        'nns_task_name'=> $info['nns_name'],
        'nns_action'=>  $action,
        'nns_url' => $file_name,
        'nns_content' => $xml_str,
        'nns_desc' => 'Channel,'.$action,
        );
        
        return self::execute_c2_child($c2_info,$sp_id);
    }
    
    static public function child_do_live($info,$action,$c2_id,$sp_id){
        
        if($action=='REGIST'){
            $group_asset = 'OpenGroupAsset';
            $MetadataAsset = 'AddMetadataAsset';
            $ContentAsset = 'AcceptContentAsset';
        }elseif($action=='UPDATE'){
            $group_asset = 'ReplaceGroupAsset';
            $MetadataAsset = 'ReplaceMetadataAsset';
            $ContentAsset = 'ReplaceContentAsset';
        }elseif($action=='DELETE'){
            $group_asset = 'DropGroupAsset';
            $MetadataAsset = 'RemoveMetadataAsset';
            $ContentAsset = 'DestroyContentAsset';
        }
        $program_id = $info['nns_id'];
        
        $db = nn_get_db(NL_DB_READ);
        $sql = "select * from nns_live_ex where nns_live_id='{$info['nns_id']}' and nns_key='pid'";
        $live_ex_info = nl_db_get_one($sql, $db);
        
        
        //live_pindao_order
        
        //source_url
        
        $sql = "select * from nns_live_ex where nns_live_id='{$info['nns_id']}' and nns_key='live_pindao_order'";
        $live_ex_pindao_order = nl_db_get_one($sql, $db);
        
        
        $sql = "select * from nns_live_ex where nns_live_id='{$info['nns_id']}' and nns_key='source_url'";
        $live_ex_source_url = nl_db_get_one($sql, $db);
        
        //$c2_id = np_guid_rand();
        $xml_str = '';
        $xml_str .='<adi:'.$group_asset.' type="VODRelease" product="VOD">';
        $xml_str .='<vod:VODRelease providerID="'.self::$providerID.'" providerType="'.self::$providerType.'" assetID="'.$program_id.'" updateNum="" groupAsset="Y" serialNo="'.$c2_id.'"></vod:VODRelease>';
        $xml_str .='</adi:'.$group_asset.'>';
        
        $xml_str .='<adi:'.$MetadataAsset.' groupProviderID="'.self::$providerID.'" groupAssetID="'.$program_id.'" type="Title" product="VOD">';
        
        $xml_str .='<vod:Title providerID="'.self::$providerID.'" assetID="'.$program_id.'" updateNum="1">';
        
        $xml_str .='<vod:TitleFull><![CDATA['.$info['nns_name'].']]></vod:TitleFull>';          
        $xml_str .='<vod:ShowType>Channel</vod:ShowType>';
        $xml_str .='<vod:ChannelNumber>'.$live_ex_pindao_order['nns_value'].'</vod:ChannelNumber>';
        $xml_str .='</vod:Title>';
        
        $xml_str .='</adi:'.$MetadataAsset.'>';
        //添加媒体xml
        $xml_str .='<adi:'.$ContentAsset.' type="PhysicalChannel">';
        
        $xml_str .='<vod:PhysicalChannel providerID="'.self::$providerID.'" assetID="live_'.$program_id.'" type="Title" product="VOD">';
            
        $xml_str .= '<vod:URL><![CDATA['.$live_ex_source_url['nns_value'].']]></vod:URL><vod:SrcType><![CDATA[1]]></vod:SrcType><vod:TSsupport><![CDATA[1]]></vod:TSsupport><vod:HDFlag><![CDATA[1]]></vod:HDFlag><vod:BitRate><![CDATA[3000000]]></vod:BitRate>';    
        $xml_str .='</vod:PhysicalChannel>';
        
        $xml_str .='</adi:'.$ContentAsset.'>';
        $xml_str .= '<adi:AssociateContent type="PhysicalChannel" effectiveDate="" groupProviderID="'.self::$providerID.'" groupAssetID="'.$program_id.'" providerID="'.self::$providerID.'" assetID="live_'.$program_id.'"/>';
        
        return $xml_str;
        //return self::execute_c2($c2_info);
    }

    
    
    static public function add_live_media($info,$sp_id){
        $action = "REGIST";
        $xml_str = '<adi:ADI2 xmlns="http://www.cablelabs.com/VODSchema/default" xmlns:adi="http://www.cablelabs.com/VODSchema/adi" xmlns:vod="http://www.cablelabs.com/VODSchema/vod">'.PHP_EOL;
        $c2_id = np_guid_rand();
        //$xml_str .= self::child_do_live($info, 'REGIST', $c2_id);
        $xml_str .= self::child_do_live($info, $action, $c2_id,$sp_id);
        $program_id = $info['nns_id'];
        
        $xml_str .= self::do_live_media($info, $action, $c2_id);
        
        $xml_str .= '</adi:ADI2>'.PHP_EOL;
        $file_name = 'live_media_'.$program_id.'_'.$action.'.xml';
        $c2_info = array(
        'nns_id'=>$c2_id,
        'nns_task_type'=>'PhysicalChannel',
        'nns_task_id'=> $info['nns_id'],
        'nns_task_name'=> $info['nns_name'].'-直播流',
        'nns_action'=>  $action,
        'nns_url' => $file_name,
        'nns_content' => $xml_str,
        'nns_desc' => 'PhysicalChannel,'.$action,
        );
        
        return self::execute_c2_child($c2_info,$sp_id);
    }
    
    
    static public function update_live_media($info,$sp_id){
        $action = "UPDATE";
        $xml_str = '<adi:ADI2 xmlns="http://www.cablelabs.com/VODSchema/default" xmlns:adi="http://www.cablelabs.com/VODSchema/adi" xmlns:vod="http://www.cablelabs.com/VODSchema/vod">'.PHP_EOL;
        $c2_id = np_guid_rand();
        $xml_str .= self::do_live($info, $action, $c2_id);
        $program_id = $info['nns_id'];
        
        $xml_str .= self::do_live_media($info, $action, $c2_id);
        
        $xml_str .= '</adi:ADI2>'.PHP_EOL;
        $file_name = 'live_media_'.$program_id.'_'.$action.'.xml';
        $c2_info = array(
        'nns_id'=>$c2_id,
        'nns_task_type'=>'PhysicalChannel',
        'nns_task_id'=> $info['nns_id'],
        'nns_task_name'=> $info['nns_name'].'-直播流',
        'nns_action'=>  $action,
        'nns_url' => $file_name,
        'nns_content' => $xml_str,
        'nns_desc' => 'PhysicalChannel,'.$action,
        );
        
        return self::execute_c2_child($c2_info,$sp_id);
    }
    
    static public function delete_live_media($info,$sp_id){
        $action = "DELETE";
        $xml_str = '<adi:ADI2 xmlns="http://www.cablelabs.com/VODSchema/default" xmlns:adi="http://www.cablelabs.com/VODSchema/adi" xmlns:vod="http://www.cablelabs.com/VODSchema/vod">'.PHP_EOL;
        $c2_id = np_guid_rand();
        $xml_str .= self::do_live($info, $action, $c2_id);
        $program_id = $info['nns_id'];
        
        $xml_str .= self::do_live_media($info, $action, $c2_id);
        
        $xml_str .= '</adi:ADI2>'.PHP_EOL;
        $file_name = 'live_media_'.$program_id.'_'.$action.'.xml';
        $c2_info = array(
        'nns_id'=>$c2_id,
        'nns_task_type'=>'PhysicalChannel',
        'nns_task_id'=> $info['nns_id'],
        'nns_task_name'=> $info['nns_name'].'-直播流',
        'nns_action'=>  $action,
        'nns_url' => $file_name,
        'nns_content' => $xml_str,
        'nns_desc' => 'PhysicalChannel,'.$action,
        );
        
        return self::execute_c2_child($c2_info,$sp_id);
    }
    
    static public function do_live_media($info,$action,$c2_id){
        $xml_str = '';
        if($action=='REGIST'){
            $group_asset = 'OpenGroupAsset';
            $MetadataAsset = 'AddMetadataAsset';
            $ContentAsset = 'AcceptContentAsset';
        }elseif($action=='UPDATE'){
            $group_asset = 'ReplaceGroupAsset';
            $MetadataAsset = 'ReplaceMetadataAsset';
            $ContentAsset = 'ReplaceContentAsset';
        }elseif($action=='DELETE'){
            $group_asset = 'DropGroupAsset';
            $MetadataAsset = 'RemoveMetadataAsset';
            $ContentAsset = 'DestroyContentAsset';
        }
        $program_id = $info['nns_id'].'_media';
        
        $db = nn_get_db(NL_DB_READ);
        $sql = "select * from nns_live_ex where nns_live_id='{$info['nns_id']}' and nns_key='pid'";
        $live_ex_info = nl_db_get_one($sql, $db);
        
        
        $xml_str .='<adi:'.$group_asset.' type="VODRelease" product="VOD">'.PHP_EOL;
        $xml_str .='<vod:VODRelease providerID="'.self::$providerID.'" providerType="'.self::$providerType.'" assetID="'.$program_id.'" updateNum="" groupAsset="Y" serialNo="'.$c2_id.'"></vod:VODRelease>'.PHP_EOL;
        $xml_str .='</adi:'.$group_asset.'>'.PHP_EOL;
        /*$xml_str .='<adi:'.$MetadataAsset.' groupProviderID="'.self::$providerID.'" groupAssetID="'.$program_id.'" type="Title" product="VOD" assetID="'.$program_id.'" updateNum="1" HDFlag="0">'.PHP_EOL;
        
        $xml_str .='<vod:Title providerID="'.self::$providerID.'" assetID="'.$program_id.'" updateNum="1">'.PHP_EOL;
        $xml_str .='<vod:ShowType>PhysicalChannel</vod:ShowType>'.PHP_EOL;
        //$xml_str .='<vod:encodingProfile>PhysicalChannel</vod:ShowType>'.PHP_EOL;
        $xml_str .='<vod:videoCodec>h264</vod:ShowType>'.PHP_EOL;
        $xml_str .='<vod:audioCodec>aac</vod:ShowType>'.PHP_EOL;
        $xml_str .='<vod:HDFlag>1</vod:HDFlag>'.PHP_EOL;
        
        
        $xml_str .='<vod:URL>http://172.25.0.5:80/nn_live.m3u8?id=HNWSHD</vod:URL>'.PHP_EOL;
        $xml_str .='<vod:SrcType>1</vod:SrcType>'.PHP_EOL;
        $xml_str .='<vod:ServiceType>10</vod:ServiceType>'.PHP_EOL;
        $xml_str .='<vod:MimeType>45</vod:MimeType>'.PHP_EOL;
        $xml_str .='</vod:Title>'.PHP_EOL;
        $xml_str .='</adi:'.$MetadataAsset.'>'.PHP_EOL;
        
        
        */
        
        
        $xml_str .= '<adi:'.$ContentAsset.' type="Video" metadataOnly="N">'.PHP_EOL;
        $xml_str .= '<vod:Video providerID="'.self::$providerID.'" assetID="'.$program_id.'" updateNum="1"  videoCodec="h264" audioCodec="aac" HDFlag="1" >'.PHP_EOL;
        
        $xml_str .='<vod:URL>http://172.25.0.5:80/nn_live.m3u8?id=HNWSHD</vod:URL>'.PHP_EOL;
        $xml_str .='<vod:SrcType>1</vod:SrcType>'.PHP_EOL;
        $xml_str .='<vod:ServiceType>10</vod:ServiceType>'.PHP_EOL;
        $xml_str .='<vod:MimeType>45</vod:MimeType>'.PHP_EOL;
        $xml_str .='<vod:BitRateType>3</vod:BitRateType>'.PHP_EOL;
        $xml_str .='<vod:BitRate>3072</vod:BitRate>'.PHP_EOL;
        
        
        
        $xml_str .= '</vod:Video></adi:'.$ContentAsset.'>'.PHP_EOL;
        
        $xml_str .= '<adi:AssociateContent type="Video" effectiveDate="" groupProviderID="'.self::$providerID.'" groupAssetID="'.$info['nns_id'].'" providerID="'.self::$providerID.'" assetID="'.$program_id.'"/>'.PHP_EOL;
        
        
        return $xml_str;
            
    }
    
    
    //删除连续剧
    
    static public function delete_series($vod_info,$sp_id){
        $db = nn_get_db(NL_DB_READ);
        $vod_id = $vod_info['nns_id'];
        $sql="select * from nns_vod where nns_id='$vod_id'";
        $vodinfo = nl_db_get_one($sql, $db);        
        $action = "DELETE";     
        $c2_id = np_guid_rand();        
        $xml_str = self::build_series($vodinfo, $action, $c2_id, $sp_id);
        $file_name = 'Series_'.$vod_id.'_'.$action.'.xml';  
            $c2_info = array(
                'nns_id' => $c2_id,
                'nns_task_type'=>'Series',
                'nns_task_id'=> $vod_info['nns_task_id'],
                'nns_task_name'=> $vod_info['nns_task_name'],
                'nns_action'=>  $action,
                'nns_url' => $file_name,
                'nns_content' => $xml_str,
                'nns_desc' => 'Series,'.$action,
            );
        return self::execute_c2_child($c2_info,$sp_id);
    }


    
    //删除分集
    
    static public function delete_vod($vod_info,$sp_id){
        $db = nn_get_db(NL_DB_READ);
        $vod_id = $vod_info['nns_id'];
        $sql="select * from nns_vod_index where nns_id='$vod_id'";
        $vodinfo = nl_db_get_one($sql, $db);        
        $action = "DELETE";     
        $c2_id = np_guid_rand();        
        $xml_str = self::build_vod($vodinfo, $action, $c2_id, $sp_id);
        $file_name = 'Program_'.$vod_id.'_'.$action.'.xml'; 
            $c2_info = array(
                'nns_id' => $c2_id,
                'nns_task_type'=>'Program',
                'nns_task_id'=> $vod_info['nns_task_id'],
                'nns_task_name'=> $vod_info['nns_task_name'],
                'nns_action'=>  $action,
                'nns_url' => $file_name,
                'nns_content' => $xml_str,
                'nns_desc' => 'Program,'.$action,
            );
        return self::execute_c2_child($c2_info,$sp_id);
    }



    //构造xml
    static public function build_series($vod_info,$action,$c2_id,$sp_id){
        $db = nn_get_db(NL_DB_READ);
        $vod_id = $vod_info['nns_id'];
        $program_id = $vod_id;
        if($action=='REGIST'){
            $group_asset = 'OpenGroupAsset';
            $MetadataAsset = 'AddMetadataAsset';
            $ContentAsset = 'AcceptContentAsset';
        }elseif($action=='UPDATE'){
            $group_asset = 'ReplaceGroupAsset';
            $MetadataAsset = 'ReplaceMetadataAsset';
            $ContentAsset = 'ReplaceContentAsset';
        }elseif($action=='DELETE'){
            $group_asset = 'DropGroupAsset';
            $MetadataAsset = 'RemoveMetadataAsset';
            $ContentAsset = 'DestroyContentAsset';
        }
        //$c2_id = np_guid_rand();
        $xml_str = '';
        $xml_str .='<adi:'.$group_asset.' type="VODRelease" product="VOD">'.PHP_EOL;
        $xml_str .='<vod:VODRelease providerID="'.self::$providerID.'" providerType="'.self::$providerType.'" assetID="'.$program_id.'" updateNum="" groupAsset="Y" serialNo="'.$c2_id.'"></vod:VODRelease>'.PHP_EOL;
        $xml_str .='</adi:'.$group_asset.'>'.PHP_EOL;
        $xml_str .='<adi:'.$MetadataAsset.' groupProviderID="'.self::$providerID.'" groupAssetID="'.$program_id.'" type="Title" product="VOD">'.PHP_EOL;
        $xml_str .='<vod:Title providerID="'.self::$providerID.'" assetID="'.$program_id.'" updateNum="1">'.PHP_EOL;
        $xml_str .='<vod:TitleFull><![CDATA['.$vod_info['nns_name'].']]></vod:TitleFull>'.PHP_EOL;//电视剧名称
        $xml_str .='<vod:ShowType>Series</vod:ShowType>'.PHP_EOL;//电视剧类型标识
        if($vod_info['nns_eng_name']){
        $xml_str .='<vod:EnglishName><![CDATA['.$vod_info['nns_eng_name'].']]></vod:EnglishName>'.PHP_EOL;//英文名称
        }
        if($vod_info['nns_summary']){
        $xml_str .='<vod:SummaryMedium><![CDATA['.$vod_info['nns_summary'].']]></vod:SummaryMedium>'.PHP_EOL;//中文描述
        $xml_str .='<vod:SummaryShort><![CDATA['.$vod_info['nns_summary'].']]></vod:SummaryShort>'.PHP_EOL;//看点
        }
        if(!empty($vod_info['nns_area'])){
            $country_code = self::get_country($vod_info['nns_area']);
            $xml_str .='<vod:CountryOfOrigin><![CDATA['.$country_code.']]></vod:CountryOfOrigin>'.PHP_EOL;//国家地区 内地=1,港台=2,韩日=3,欧美=4, 东南亚=5，其他=99
        }
        if(!empty($vod_info['nns_actor'])){
            $xml_str .='<vod:Actor type="Actor">'.PHP_EOL;//演员
            $xml_str .='<vod:LastNameFirst><![CDATA['.$vod_info['nns_actor'].']]></vod:LastNameFirst>'.PHP_EOL;
            $xml_str .='</vod:Actor>'.PHP_EOL;
        }
        //导演
        if(!empty($vod_info['nns_director'])){
            $xml_str .='<vod:Director><vod:LastNameFirst><![CDATA['.$vod_info['nns_director'].']]></vod:LastNameFirst></vod:Director>'.PHP_EOL;
        }
        //year
        if(!empty($vod_info['nns_show_time'])){
            $xml_str .= '<vod:Year><![CDATA['.date('Y',strtotime($vod_info['nns_show_time'])).']]></vod:Year>'.PHP_EOL;
        }
        //language
        if(!empty($vod_info['nns_language'])){
            $xml_str .= '<vod:Language><![CDATA['.$vod_info['nns_language'].']]></vod:Language>'.PHP_EOL;
        }
        //all_index
        if(!empty($vod_info['nns_all_index'])){
            $xml_str .='<vod:Items><![CDATA['.$vod_info['nns_all_index'].']]></vod:Items>'.PHP_EOL;
        }
        //nns_keyword
        if(!empty($vod_info['nns_keyword'])){
            $xml_str .='<vod:Keyword><![CDATA['.str_replace('/', ' ', $vod_info['nns_keyword']).']]></vod:Keyword>'.PHP_EOL;
        }
        //$xml_str .='<vod:IsAdvertise>0</vod:IsAdvertise>';//0：非广告内容; 1：广告内容

        $xml_str .='</vod:Title>'.PHP_EOL;
        $xml_str .= '</adi:'.$MetadataAsset.'>'.PHP_EOL;
        
        
        $category = '';
        $array_category = array(
            '10000001'=>'电影',
            '10000002'=>'电视剧',
            '10000003'=>'综艺',
            '10000004'=>'动漫',
            '10000005'=>'音乐',
            '10000006'=>'纪实',
            '10000010'=>'纪录片',
            '10000021'=>'广告',
        );
        $category_id = nl_db_get_col("select nns_category_id from nns_vod where nns_id='$program_id'",$db);
        //
        $category = $array_category[$category_id];
        $xml_str .= '<adi:'.$MetadataAsset.' groupProviderID="'.self::$providerID.'" groupAssetID="'.$program_id.'" type="CategoryPath" product="VOD">
        <vod:CategoryPath mso="" providerID="'.self::$providerID.'" assetID="'.$program_id.'" updateNum="1">
        <adi:AssetLifetime startDateTime="2012-09-04" endDateTime="2062-09-04"/>
        <vod:Classification>
        <![CDATA['.$category.']]>
        </vod:Classification>
        </vod:CategoryPath>
        </adi:'.$MetadataAsset.'>';
        
        
        $img_save_name = str_replace('.JPG','.jpg',$vod_info['nns_image2']);
        $file_img = dirname(dirname(dirname(dirname(__FILE__)))).'/data/downimg/'.$img_save_name;
        
        //if(file_exists($file_img)){
            $img = self::img_handel($img_save_name);
            if(!empty($vod_info['nns_image2'])&&$img===true){
                $xml_str .= '<adi:'.$ContentAsset.' type="Image" metadataOnly="N">'.PHP_EOL;
                //图片的实体ID取用图片的名称 
                //$image_asset_id = str_replace('/', '', $vod_info['nns_image2']);
                $image_asset_id = $program_id.'image2';
                $file_fix = substr($vod_info['nns_image2'],  -3,3);
                $img_save_name = str_replace('.JPG','.jpg',$vod_info['nns_image2']);
                $pic_url = self::get_image_url($img_save_name,$sp_id);
                $xml_str .= '<vod:Image providerID="'.self::$providerID.'" assetID="'.$image_asset_id.'" updateNum="1" transferContentURL="'.$pic_url.'" imageEncodingProfile="'.$file_fix.'"    fileName="'.$image_asset_id.'">'.PHP_EOL;
                $xml_str .='<vod:FileType><![CDATA[3]]></vod:FileType>'.PHP_EOL;
                $xml_str .='<vod:Usage><![CDATA[15]]></vod:Usage>'.PHP_EOL;
                $xml_str .='</vod:Image>'.PHP_EOL;
                $xml_str .= '</adi:'.$ContentAsset.'>'.PHP_EOL;
                $xml_str .= '<adi:AssociateContent type="Image"  groupProviderID="'.self::$providerID.'" groupAssetID="'.$program_id.'" providerID="'.self::$providerID.'" assetID="'.$image_asset_id.'"/>'.PHP_EOL;
            }       
        //}
        return $xml_str;
    }




    static public function build_vod($vod_info,$action,$c2_id,$sp_id){
        $db = nn_get_db(NL_DB_READ);
        

        $vod_id = $vod_info['nns_id'];
        $program_id = $vod_id;
        if($action=='REGIST'){
            $group_asset = 'OpenGroupAsset';
            $MetadataAsset = 'AddMetadataAsset';
            $ContentAsset = 'AcceptContentAsset';
        }elseif($action=='UPDATE'){
            $group_asset = 'ReplaceGroupAsset';
            $MetadataAsset = 'ReplaceMetadataAsset';
            $ContentAsset = 'ReplaceContentAsset';
        }elseif($action=='DELETE'){
            $group_asset = 'DropGroupAsset';
            $MetadataAsset = 'RemoveMetadataAsset';
            $ContentAsset = 'DestroyContentAsset';
        }
        //$c2_id = np_guid_rand();
        //$xml_str = '<adi:ADI2 xmlns="http://www.cablelabs.com/VODSchema/default" xmlns:adi="http://www.cablelabs.com/VODSchema/adi" xmlns:vod="http://www.cablelabs.com/VODSchema/vod">';
        $xml_str = '';
        if($vod_info['nns_series_flag']==1){//连续剧   
        
            $xml_str .='<adi:'.$group_asset.' type="VODRelease" product="VOD">'.PHP_EOL;
            $xml_str .='<vod:VODRelease providerID="'.self::$providerID.'" providerType="'.self::$providerType.'" assetID="'.$program_id.'" updateNum="" groupAsset="Y" serialNo="'.$c2_id.'"></vod:VODRelease>'.PHP_EOL;
            $xml_str .='</adi:'.$group_asset.'>'.PHP_EOL;
            
            
            $xml_str .='<adi:'.$MetadataAsset.' groupProviderID="'.self::$providerID.'" groupAssetID="'.$program_id.'" type="Title" product="VOD">'.PHP_EOL;
            $xml_str .='<vod:Title providerID="'.self::$providerID.'" assetID="'.$program_id.'" updateNum="1"><vod:ShowType>Series</vod:ShowType>'.PHP_EOL;
            if($vod_info['nns_name']){
            $xml_str .='<vod:TitleFull><![CDATA['.$vod_info['nns_name'].']]></vod:TitleFull>'.PHP_EOL;
            }
            $xml_str .='<vod:TVIdentifier authority="CST" seriesID="'.$vod_info['nns_vod_id'].'"></vod:TVIdentifier>'.PHP_EOL;
            $xml_str .='<vod:RunTime><![CDATA['.$vod_info['nns_view_len'].']]></vod:RunTime>'.PHP_EOL;
            $xml_str .='<vod:EpisodeID><![CDATA['.intval($vod_info['nns_index']+1).']]></vod:EpisodeID>'.PHP_EOL;
            if($vod_info['nns_summary']){
            $xml_str .='<vod:SummaryMedium><![CDATA['.$vod_info['nns_summary'].']]></vod:SummaryMedium>'.PHP_EOL;
            }
            $xml_str .='<vod:IsAdvertise>0</vod:IsAdvertise>'.PHP_EOL;
            $xml_str .='</vod:Title>'.PHP_EOL;
            $xml_str .='</adi:'.$MetadataAsset.'>'.PHP_EOL;

            
            $img_save_name = str_replace('.JPG','.jpg',$vod_info['nns_image2']);
            $file_img = dirname(dirname(dirname(dirname(__FILE__)))).'/data/downimg/'.$img_save_name;
            $img = self::img_handel($img_save_name);
            //if(file_exists($file_img)){           
                if(!empty($vod_info['nns_image2'])&&$img===true){
                    $xml_str .= '<adi:'.$ContentAsset.' type="Image" metadataOnly="N">'.PHP_EOL;
                    //图片的实体ID取用图片的名称
                    //$image_asset_id = str_replace('/', '', $vod_info['nns_image2']);
                    $image_asset_id = $program_id.'image2';
                    $file_fix = substr($vod_info['nns_image2'],  -3,3);
                    $img_save_name = str_replace('.JPG','.jpg',$vod_info['nns_image2']);
                    $pic_url = self::get_image_url($img_save_name);
                    $xml_str .= '<vod:Image providerID="'.self::$providerID.'" assetID="'.$image_asset_id.'" updateNum="1" transferContentURL="'.$pic_url.'" imageEncodingProfile="'.$file_fix.'"   fileName="'.$image_asset_id.'">'.PHP_EOL;
                    $xml_str .='<vod:FileType><![CDATA[3]]></vod:FileType>'.PHP_EOL;
                    $xml_str .='<vod:Usage><![CDATA[15]]></vod:Usage>'.PHP_EOL;
                    $xml_str .='</vod:Image>'.PHP_EOL;
                    $xml_str .= '</adi:'.$ContentAsset.'>'.PHP_EOL;
                    $xml_str .= '<adi:AssociateContent type="Image"  groupProviderID="'.self::$providerID.'" groupAssetID="'.$program_id.'" providerID="'.self::$providerID.'" assetID="'.$image_asset_id.'"/>'.PHP_EOL;
                }
            //}
            $xml_str .='<adi:AssociateGroup  groupProviderID="'.self::$providerID.'" providerID="'.self::$providerID.'" assetID="'.$program_id.'" sourceGroupAssetID="'.$program_id.'" targetGroupAssetID="'.$vod_info['nns_vod_id'].'"/>'.PHP_EOL;
        }else{
            $xml_str .='<adi:'.$group_asset.' type="VODRelease" product="VOD">'.PHP_EOL;
            $xml_str .='<vod:VODRelease providerID="'.self::$providerID.'" providerType="'.self::$providerType.'" assetID="'.$program_id.'" updateNum="" groupAsset="Y" serialNo="'.$c2_id.'"></vod:VODRelease>'.PHP_EOL;
            $xml_str .='</adi:'.$group_asset.'>'.PHP_EOL;
            $xml_str .='<adi:'.$MetadataAsset.' groupProviderID="'.self::$providerID.'" groupAssetID="'.$program_id.'" type="Title" product="VOD">'.PHP_EOL;
            $xml_str .='<vod:Title providerID="'.self::$providerID.'" assetID="'.$program_id.'" updateNum="1">'.PHP_EOL;
            $xml_str .='<vod:TitleFull><![CDATA['.$vod_info['nns_name'].']]></vod:TitleFull>'.PHP_EOL;//名称
            $xml_str .='<vod:ShowType>Movie</vod:ShowType>'.PHP_EOL;//类型标识
            if($vod_info['nns_eng_name']){
            $xml_str .='<vod:EnglishName><![CDATA['.$vod_info['nns_eng_name'].']]></vod:EnglishName>'.PHP_EOL;//英文名称
            }
            if($vod_info['nns_summary']){
            $xml_str .='<vod:SummaryMedium><![CDATA['.$vod_info['nns_summary'].']]></vod:SummaryMedium>'.PHP_EOL;//中文描述
            $xml_str .='<vod:SummaryShort><![CDATA['.$vod_info['nns_summary'].']]></vod:SummaryShort>'.PHP_EOL;//看点
            }
            if(!empty($vod_info['nns_area'])){
                $country_code = self::get_country($vod_info['nns_area']);
                $xml_str .='<vod:CountryOfOrigin><![CDATA['.$country_code.']]></vod:CountryOfOrigin>'.PHP_EOL;//国家地区 内地=1,港台=2,韩日=3,欧美=4, 东南亚=5，其他=99
            }
            if(!empty($vod_info['nns_actor'])){
                $xml_str .='<vod:Actor type="Actor">'.PHP_EOL;//演员
                $xml_str .='<vod:LastNameFirst><![CDATA['.$vod_info['nns_actor'].']]></vod:LastNameFirst>'.PHP_EOL;
                $xml_str .='</vod:Actor>'.PHP_EOL;
            }
            //导演
            if(!empty($vod_info['nns_director'])){
                $xml_str .='<vod:Director><vod:LastNameFirst><![CDATA['.$vod_info['nns_director'].']]></vod:LastNameFirst></vod:Director>'.PHP_EOL;
            }
            //year
            if(!empty($vod_info['nns_show_time'])){
                $xml_str .= '<vod:Year><![CDATA['.date('Y',strtotime($vod_info['nns_show_time'])).']]></vod:Year>'.PHP_EOL;
            }
            //language
            if(!empty($vod_info['nns_language'])){
                $xml_str .= '<vod:Language><![CDATA['.$vod_info['nns_language'].']]></vod:Language>'.PHP_EOL;
            }
            //nns_keyword
            if(!empty($vod_info['nns_keyword'])){
                $xml_str .='<vod:Keyword><![CDATA['.str_replace('/', ' ', $vod_info['nns_keyword']).']]></vod:Keyword>'.PHP_EOL;
            }
            $xml_str .='</vod:Title>'.PHP_EOL;
            $xml_str .='</adi:'.$MetadataAsset.'>'.PHP_EOL;

            //category
            /*$xml_str .= '<adi:'.$MetadataAsset.' groupProviderID="'.self::$providerID.'" groupAssetID="'.$program_id.'" type="CategoryPath" product="VOD">';
            $xml_str .= '<vod:CategoryPath providerID="'.self::$providerID.'" assetID="'.$program_id.'" updateNum="1">'.PHP_EOL;
            if(is_array($vod_info['nns_category_id'])){
                foreach ($vod_info['nns_category_id'] as  $category_id) {
                    $sql = "select nns_name from nns_mgtvbk_category where nns_sp_category_id='$category_id' and nns_org_id='".SP_ORG_ID."'";
                    $category_name = nl_db_get_col($sql, $db);
                    $xml_str .= '<vod:Classification><![CDATA['.$category_name.']]></vod:Classification>'.PHP_EOL;
                }
            }
            $xml_str .= '</vod:CategoryPath>';*/
            //$xml_str .= '</adi:'.$MetadataAsset.'>'.PHP_EOL;
            //images
            $img_save_name = str_replace('.JPG','.jpg',$vod_info['nns_image2']);
            $file_img = dirname(dirname(dirname(dirname(__FILE__)))).'/data/downimg/'.$img_save_name;
            //if(file_exists($file_img)){
                $img = self::img_handel($img_save_name);
                if(!empty($vod_info['nns_image2'])&&$img===true){
                    $xml_str .= '<adi:'.$ContentAsset.' type="Image" metadataOnly="N">'.PHP_EOL;
                    //图片的实体ID取用图片的名称
                    //$image_asset_id = str_replace('/', '', $vod_info['nns_image2']);
                    $image_asset_id = $program_id.'image2';
                    $file_fix = substr($vod_info['nns_image2'],  -3,3);
                    $img_save_name = str_replace('.JPG','.jpg',$vod_info['nns_image2']);
                    $pic_url = self::get_image_url($img_save_name,$sp_id);
                    $xml_str .= '<vod:Image providerID="'.self::$providerID.'" assetID="'.$image_asset_id.'" updateNum="1" transferContentURL="'.$pic_url.'" imageEncodingProfile="'.$file_fix.'"    fileName="'.$image_asset_id.'">'.PHP_EOL;
                    $xml_str .='<vod:FileType><![CDATA[3]]></vod:FileType>'.PHP_EOL;
                    $xml_str .='<vod:Usage><![CDATA[15]]></vod:Usage>'.PHP_EOL;
                    $xml_str .='</vod:Image>'.PHP_EOL;
                    $xml_str .= '</adi:'.$ContentAsset.'>'.PHP_EOL;
                    $xml_str .= '<adi:AssociateContent type="Image"  groupProviderID="'.self::$providerID.'" groupAssetID="'.$program_id.'" providerID="'.self::$providerID.'" assetID="'.$image_asset_id.'"/>'.PHP_EOL;
                }
            //}
            $xml_str .='<adi:AssociateGroup  groupProviderID="'.self::$providerID.'" providerID="'.self::$providerID.'" assetID="'.$program_id.'" sourceGroupAssetID="'.$program_id.'" targetGroupAssetID="'.$vod_info['nns_vod_id'].'"/>'.PHP_EOL;
        }
        $xml_str .= '';
        
        return $xml_str;
    }
    
    
    
    static public function add_movies_by_vod_index($index_media,$sp_id){
        
        
        //print_r($index_media);
                
        $db = nn_get_db(NL_DB_WRITE);
        $db ->open();
        //获取这个影片的主媒资ID,查询这个媒资是电影还是连续剧
        $is_series = false;
        $sql_series = "select * from nns_vod where nns_id='{$index_media['nns_video_id']}'";
        $info = nl_db_get_one($sql_series, $db);
        
        $sql_index = "select * from nns_vod_index where nns_id='{$index_media['nns_video_index_id']}'";
        $info_index = nl_db_get_one($sql_index, $db);
        
        $nns_task_name = $index_media['nns_task_name'];
        
        
        $c2_id = np_guid_rand();
        unset($sql_series,$sql_index);
        
        $info_index['nns_series_flag'] = 0;
        if($info['nns_all_index']>1){           
            $is_series = true;
            $info_index['nns_series_flag'] = 1;
        }else{
            $info_index['nns_name'] = $info['nns_name'];
            $info_index['nns_series_flag'] = 0;
        }
        
        $sp_config = sp_model::get_sp_config($sp_id);
        if(isset($sp_config['cdn_import_serise'])&&(int)$sp_config['cdn_import_serise']===1){
            $is_series = true;
            $info_index['nns_series_flag'] = 1;
        }
        
        $xml_str = '<adi:ADI2 xmlns="http://www.cablelabs.com/VODSchema/default" xmlns:adi="http://www.cablelabs.com/VODSchema/adi" xmlns:vod="http://www.cablelabs.com/VODSchema/vod">'.PHP_EOL;
        if($is_series===true){
//              $xml_str .=self::build_series($info,'REGIST',$c2_id,$sp_id);
            $xml_str .=self::build_movie($info,'REGIST',$c2_id,$sp_id);
        }
        
        
        
        $xml_str .=self::build_vod($info_index, 'REGIST', $c2_id, $sp_id);
        //print_r($info_index);die;
        
        $action_tmp = $index_media['nns_action'];
        
        $sql = "select * from nns_vod_media where  nns_vod_id='{$index_media['nns_video_id']}' and nns_vod_index_id='{$index_media['nns_video_index_id']}'  and nns_id='{$index_media['nns_media_id']}'   order by nns_kbps desc limit 1";
        $medias = nl_db_get_all($sql,$db);
        $kbps = $medias[0]['nns_kbps'];
        $nns_deleted = $medias[0]['nns_deleted'];
        if($nns_deleted==1){
            //$action_tmp = 'destroy';
        }
            
        if($action_tmp=='add'){
            $action = 'REGIST';
        }elseif($action_tmp=='modify'){
            $action = 'UPDATE';
        }elseif($action_tmp=='destroy'){
            $action = 'DELETE';
        }

        $file_size = $medias[0]['nns_file_size'] * 1024 * 1024;


        if($action=='REGIST'){
            $ContentAsset = 'AcceptContentAsset';
        }elseif($action=='UPDATE'){
            //$ContentAsset = 'ReplaceContentAsset';
            $ContentAsset = 'AcceptContentAsset';
        }elseif($action=='DELETE'){
            $ContentAsset = 'DestroyContentAsset';
        }
        
        
        if(is_array($medias) && count($medias)>0){
            
            $media = array();
            $media_arr = array();

            $media_url = "";
            foreach($medias as $media_item){
                $mode = strtolower($media_item['nns_mode']);
                if($mode == '4' || $mode == 'sd')
                {
                	$mode = 'hd';
                	$media_item['nns_mode'] = 'hd';
                }
                $media_arr[$mode] = $media_item;

            }
            if(!empty($media_arr['hd'])){
                $media = $media_arr['hd'];
            }else if(!empty($media_arr['std'])){
                $media = $media_arr['std'];
            }else if(!empty($media_arr['low'])){
                $media = $media_arr['low'];
            }
            if(empty($media)) return false;
            
            $video_profile =1;
                $mode = strtolower($media['nns_mode']);
                switch($mode){
                    case 'std':
                    $video_profile = 1;
                    break;
                    case 'hd':
                    $video_profile = 5;
                    break;
                    case 'low':
                    $video_profile = 3;
                    break;
            }



            
            if(!empty($index_media['nns_file_path'])){                      
                $ftp_url_tmp = 'ftp://ftp_m3u8:ftp_m3u8@221.181.100.156:52114/'.$index_media['nns_file_path'];
                $url = MOVIE_FTP.'/'.$index_media['nns_file_path'];
            }else{
                $url = MOVIE_FTP.'/'.$index_media['nns_date'].'/'.$index_media['nns_task_id'].'/'.$index_media['nns_media_id'].'.ts';
                $ftp_url_tmp = 'ftp://ftp_m3u8:ftp_m3u8@221.181.100.156:52114/'.$index_media['nns_date'].'/'.$index_media['nns_task_id'].'/'.$index_media['nns_media_id'].'.ts';
            }
            
            //$url = $index_media['nns_file_path'];
        /*if(empty($url)){
                $sql_sp = "select * from nns_mgtvbk_c2_task where nns_org_id='shanghai_mobile' and nns_type='media' and nns_ref_id='{$index_media['nns_media_id']}'";
            $sp_qask = nl_db_get_one($sql_sp, $db);
                if(is_array($sp_qask)){
                    $url = $sp_qask['nns_ex_url'];
                }
            }*/
            /*if(empty($url)){
                nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=7,nns_epg_status=100 where nns_id='{$index_media['nns_task_id']}'", $db);
                return false;
            }*/
        $bool = true;//self::child_check_ftp_file($ftp_url_tmp);
            if($action=='DELETE'){
                $bool = true;
            }
            
            $bool=true;
                if($bool===false){
                    
                    $re = nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=-1 where nns_id='{$index_media['nns_task_id']}'", $db);
                    //var_dump($re);
                    //die('ddddddddddddddddddd');
                    include_once dirname(dirname(dirname(__FILE__))).'/models/queue_task_model.php';
                    $queue_task_model = new queue_task_model();
                    DEBUG && i_write_log_core('path_fail----'.$url,$sp_id.'/path_fail');
                    //$queue_task_model->q_report_task($index_media['nns_task_id']);
                    //$queue_task_model->q_add_task($index_media['nns_media_id'], 'media', 'add',$sp_id,null,true);
                    $queue_task_model->q_re_clip($index_media['nns_task_id']);
                    $queue_task_model=null;
                    return false;
            }
            //$size = isset($medias[0]['nns_file_size']) ? $medias[0]['nns_file_size'] : 20;   
            //$size = $size.'000000';
            $ini_file = array('size'=>$file_size,'md5'=>'',);
            
            // if(!empty($index_media['nns_file_path'])){
                // $url = MOVIE_FTP.'/'.$index_media['nns_file_path'];
                // $ini_file = array('size'=>$index_media['nns_file_size'],'md5'=>$index_media['nns_file_md5'],);
            // }else{
                // $url_ini="ftp://ftp_m3u8:ftp_m3u8@221.181.100.156:52114//".$index_media['nns_date'].'/'.$index_media['nns_task_id'].'/'.$index_media['nns_media_id'].'.ini';
                // $url_arr = parse_url($url_ini);
                // $notify_save_path = dirname(dirname(dirname(dirname(__FILE__)))).'/data/mgtv/ini/notify'.$url_arr["path"];
                // if(!file_exists($notify_save_path)){
                    // $port = isset($url_arr["port"])?$url_arr["port"]:21;
                    // include_once NPDIR . '/np_ftp.php';
                    // $ftp1=new nns_ftp($url_arr['host'],$url_arr["user"],$url_arr["pass"],$port);
                    // $ftpcon1=$ftp1->connect();
                    // if (empty($ftpcon1)) {
                        // $ini_file = array('size'=>'','md5'=>'',);
                    // }
                    // $path_parts = pathinfo($notify_save_path);
                    // if (!is_dir($path_parts['dirname'])){
                        // mkdir($path_parts['dirname'], 0777, true);
                    // }
                    // $rs = $ftp1->get(ltrim($url_arr["path"],'/'),$notify_save_path);
                    // if($rs==false){
                        // $ini_file = array('size'=>'','md5'=>'',);
                    // }
                // }
                // if(file_exists($notify_save_path)){
                    // $ini_file = parse_ini_file($notify_save_path);
                // }else{
                    // $ini_file = array('size'=>'','md5'=>'',);
                // }
            // }

            $xml_str .= '<adi:'.$ContentAsset.' type="Video" metadataOnly="N" fileName="'.$nns_task_name.'" fileSize="'.$ini_file['size'].'" mD5CheckSum="'.$ini_file['md5'].'">'.PHP_EOL;
                //if($action!='DELETE'){
                $xml_str .= '<vod:Video providerID="'.self::$providerID.'" assetID="'.$index_media['nns_media_id'].'" updateNum="1" fileName="'.$nns_task_name.'"  transferContentURL="'.$url.'" encodingProfile="application/octet-stream">'.PHP_EOL;
                $xml_str .= '<vod:Duration>
<![CDATA['.$medias[0]['nns_file_time_len'].']]>
</vod:Duration>
<vod:Bitrate>
<![CDATA['.intval($kbps*1000).']]>
</vod:Bitrate>
<vod:FrameRate>
<![CDATA[5]]>
</vod:FrameRate>
<vod:FileType>
<![CDATA[1]]>
</vod:FileType>
<vod:Usage>
<![CDATA[1]]>
</vod:Usage>
<vod:ServiceType>
<![CDATA[1]]>
</vod:ServiceType>
<vod:MimeType>
<![CDATA[46]]>
</vod:MimeType>
<vod:HDFlag>0</vod:HDFlag></vod:Video>';
                $xml_str .= '</adi:'.$ContentAsset.'>'.PHP_EOL;
                $xml_str .= '<adi:AssociateContent type="Video" effectiveDate="" groupProviderID="'.self::$providerID.'" groupAssetID="'.$index_media['nns_video_index_id'].'" providerID="'.self::$providerID.'" assetID="'.$index_media['nns_media_id'].'"/>'.PHP_EOL;
                $xml_str .= '</adi:ADI2>'.PHP_EOL;
                $file_name = 'vod_media_'.$index_media['nns_video_index_id'].'_'.$index_media['nns_media_id'].'_'.$action.'.xml';
                
                //die($xml_str);
                $c2_info = array(
                    'nns_id' => $c2_id,
                    'nns_task_type'=>'Movie',
                    'nns_task_id'=> $index_media['nns_task_id'],
                    'nns_task_name'=> $nns_task_name,
                    'nns_action'=>  $action,
                    'nns_url' => $file_name,
                    'nns_content' => $xml_str,
                    'nns_desc' => 'Movie,'.$action,
                );
            $result =  self::execute_c2_child($c2_info,$sp_id);
            if(!$result)
            {
            	return self::add_movies_by_vod_index_v2($index_media,$sp_id);
            }
            return $result;
        }   
    }
    
    
    static public function add_movies_by_vod_index_v2($index_media,$sp_id){
    
    
    	//print_r($index_media);
    
    	$db = nn_get_db(NL_DB_WRITE);
    	$db ->open();
    	//获取这个影片的主媒资ID,查询这个媒资是电影还是连续剧
    	$is_series = false;
    	$sql_series = "select * from nns_vod where nns_id='{$index_media['nns_video_id']}'";
    	$info = nl_db_get_one($sql_series, $db);
    
    	$sql_index = "select * from nns_vod_index where nns_id='{$index_media['nns_video_index_id']}'";
    	$info_index = nl_db_get_one($sql_index, $db);
    
    	$nns_task_name = $index_media['nns_task_name'];
    
    
    	$c2_id = np_guid_rand();
    	unset($sql_series,$sql_index);
    
    	$info_index['nns_series_flag'] = 0;
    	if($info['nns_all_index']>1){
    		$is_series = true;
    		$info_index['nns_series_flag'] = 1;
    	}else{
    		$info_index['nns_name'] = $info['nns_name'];
    		$info_index['nns_series_flag'] = 0;
    	}
    
    	$sp_config = sp_model::get_sp_config($sp_id);
    	if(isset($sp_config['cdn_import_serise'])&&(int)$sp_config['cdn_import_serise']===1){
    		$is_series = true;
    		$info_index['nns_series_flag'] = 1;
    	}
    
    	$xml_str = '<adi:ADI2 xmlns="http://www.cablelabs.com/VODSchema/default" xmlns:adi="http://www.cablelabs.com/VODSchema/adi" xmlns:vod="http://www.cablelabs.com/VODSchema/vod">'.PHP_EOL;
    	if($is_series===true){
    		$xml_str .=self::build_series($info,'REGIST',$c2_id,$sp_id);
//     		$xml_str .=self::build_movie($info,'REGIST',$c2_id,$sp_id);
    	}
    
    
    
    	$xml_str .=self::build_vod($info_index, 'REGIST', $c2_id, $sp_id);
    	//print_r($info_index);die;
    
    	$action_tmp = $index_media['nns_action'];
    
    	$sql = "select * from nns_vod_media where  nns_vod_id='{$index_media['nns_video_id']}' and nns_vod_index_id='{$index_media['nns_video_index_id']}'  and nns_id='{$index_media['nns_media_id']}'   order by nns_kbps desc limit 1";
    	$medias = nl_db_get_all($sql,$db);
    	$kbps = $medias[0]['nns_kbps'];
    	$nns_deleted = $medias[0]['nns_deleted'];
    	if($nns_deleted==1){
    		//$action_tmp = 'destroy';
    	}
    
    	if($action_tmp=='add'){
    		$action = 'REGIST';
    	}elseif($action_tmp=='modify'){
    		$action = 'UPDATE';
    	}elseif($action_tmp=='destroy'){
    		$action = 'DELETE';
    	}
    
    	$file_size = $medias[0]['nns_file_size'] * 1024 * 1024;
    
    
    	if($action=='REGIST'){
    		$ContentAsset = 'AcceptContentAsset';
    	}elseif($action=='UPDATE'){
    		//$ContentAsset = 'ReplaceContentAsset';
    		$ContentAsset = 'AcceptContentAsset';
    	}elseif($action=='DELETE'){
    		$ContentAsset = 'DestroyContentAsset';
    	}
    
    
    	if(is_array($medias) && count($medias)>0){
    
    		$media = array();
    		$media_arr = array();
    
    		$media_url = "";
    		foreach($medias as $media_item){
    			$mode = strtolower($media_item['nns_mode']);
    			if($mode == '4' || $mode == 'sd')
    			{
    				$mode = 'hd';
    				$media_item['nns_mode'] = 'hd';
    			}
    			$media_arr[$mode] = $media_item;
    
    		}
    		if(!empty($media_arr['hd'])){
    			$media = $media_arr['hd'];
    		}else if(!empty($media_arr['std'])){
    			$media = $media_arr['std'];
    		}else if(!empty($media_arr['low'])){
    			$media = $media_arr['low'];
    		}
    		if(empty($media)) return false;
    
    		$video_profile =1;
    		$mode = strtolower($media['nns_mode']);
    		switch($mode){
    			case 'std':
    				$video_profile = 1;
    				break;
    			case 'hd':
    				$video_profile = 5;
    				break;
    			case 'low':
    				$video_profile = 3;
    				break;
    		}
    
    
    
    
    		if(!empty($index_media['nns_file_path'])){
    			$ftp_url_tmp = 'ftp://ftp_m3u8:ftp_m3u8@221.181.100.156:52114/'.$index_media['nns_file_path'];
    			$url = MOVIE_FTP.'/'.$index_media['nns_file_path'];
    		}else{
    			$url = MOVIE_FTP.'/'.$index_media['nns_date'].'/'.$index_media['nns_task_id'].'/'.$index_media['nns_media_id'].'.ts';
    			$ftp_url_tmp = 'ftp://ftp_m3u8:ftp_m3u8@221.181.100.156:52114/'.$index_media['nns_date'].'/'.$index_media['nns_task_id'].'/'.$index_media['nns_media_id'].'.ts';
    		}
    
    		//$url = $index_media['nns_file_path'];
    		/*if(empty($url)){
    		 $sql_sp = "select * from nns_mgtvbk_c2_task where nns_org_id='shanghai_mobile' and nns_type='media' and nns_ref_id='{$index_media['nns_media_id']}'";
    		$sp_qask = nl_db_get_one($sql_sp, $db);
    		if(is_array($sp_qask)){
    		$url = $sp_qask['nns_ex_url'];
    		}
    		}*/
    		/*if(empty($url)){
    		 nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=7,nns_epg_status=100 where nns_id='{$index_media['nns_task_id']}'", $db);
    		return false;
    		}*/
    		$bool = true;//self::child_check_ftp_file($ftp_url_tmp);
    		if($action=='DELETE'){
    			$bool = true;
    		}
    
    		$bool=true;
    		if($bool===false){
    
    			$re = nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=-1 where nns_id='{$index_media['nns_task_id']}'", $db);
    			//var_dump($re);
    			//die('ddddddddddddddddddd');
    			include_once dirname(dirname(dirname(__FILE__))).'/models/queue_task_model.php';
    			$queue_task_model = new queue_task_model();
    			DEBUG && i_write_log_core('path_fail----'.$url,$sp_id.'/path_fail');
    			//$queue_task_model->q_report_task($index_media['nns_task_id']);
    			//$queue_task_model->q_add_task($index_media['nns_media_id'], 'media', 'add',$sp_id,null,true);
    			$queue_task_model->q_re_clip($index_media['nns_task_id']);
    			$queue_task_model=null;
    			return false;
    		}
    		//$size = isset($medias[0]['nns_file_size']) ? $medias[0]['nns_file_size'] : 20;
    		//$size = $size.'000000';
    		$ini_file = array('size'=>$file_size,'md5'=>'',);
    
    		// if(!empty($index_media['nns_file_path'])){
    		// $url = MOVIE_FTP.'/'.$index_media['nns_file_path'];
    		// $ini_file = array('size'=>$index_media['nns_file_size'],'md5'=>$index_media['nns_file_md5'],);
    		// }else{
    		// $url_ini="ftp://ftp_m3u8:ftp_m3u8@221.181.100.156:52114//".$index_media['nns_date'].'/'.$index_media['nns_task_id'].'/'.$index_media['nns_media_id'].'.ini';
    		// $url_arr = parse_url($url_ini);
    		// $notify_save_path = dirname(dirname(dirname(dirname(__FILE__)))).'/data/mgtv/ini/notify'.$url_arr["path"];
    		// if(!file_exists($notify_save_path)){
    		// $port = isset($url_arr["port"])?$url_arr["port"]:21;
    		// include_once NPDIR . '/np_ftp.php';
    		// $ftp1=new nns_ftp($url_arr['host'],$url_arr["user"],$url_arr["pass"],$port);
    		// $ftpcon1=$ftp1->connect();
    		// if (empty($ftpcon1)) {
    		// $ini_file = array('size'=>'','md5'=>'',);
    		// }
    		// $path_parts = pathinfo($notify_save_path);
    		// if (!is_dir($path_parts['dirname'])){
    		// mkdir($path_parts['dirname'], 0777, true);
    		// }
    		// $rs = $ftp1->get(ltrim($url_arr["path"],'/'),$notify_save_path);
    		// if($rs==false){
    		// $ini_file = array('size'=>'','md5'=>'',);
    		// }
    		// }
    		// if(file_exists($notify_save_path)){
    		// $ini_file = parse_ini_file($notify_save_path);
    		// }else{
    		// $ini_file = array('size'=>'','md5'=>'',);
    		// }
    		// }
    
    		$xml_str .= '<adi:'.$ContentAsset.' type="Video" metadataOnly="N" fileName="'.$nns_task_name.'" fileSize="'.$ini_file['size'].'" mD5CheckSum="'.$ini_file['md5'].'">'.PHP_EOL;
    		//if($action!='DELETE'){
    		$xml_str .= '<vod:Video providerID="'.self::$providerID.'" assetID="'.$index_media['nns_media_id'].'" updateNum="1" fileName="'.$nns_task_name.'"  transferContentURL="'.$url.'" encodingProfile="application/octet-stream">'.PHP_EOL;
    		$xml_str .= '<vod:Duration>
<![CDATA['.$medias[0]['nns_file_time_len'].']]>
</vod:Duration>
<vod:Bitrate>
<![CDATA['.intval($kbps*1000).']]>
</vod:Bitrate>
<vod:FrameRate>
<![CDATA[5]]>
</vod:FrameRate>
<vod:FileType>
<![CDATA[1]]>
</vod:FileType>
<vod:Usage>
<![CDATA[1]]>
</vod:Usage>
<vod:ServiceType>
<![CDATA[1]]>
</vod:ServiceType>
<vod:MimeType>
<![CDATA[46]]>
</vod:MimeType>
<vod:HDFlag>0</vod:HDFlag></vod:Video>';
    		$xml_str .= '</adi:'.$ContentAsset.'>'.PHP_EOL;
    		$xml_str .= '<adi:AssociateContent type="Video" effectiveDate="" groupProviderID="'.self::$providerID.'" groupAssetID="'.$index_media['nns_video_index_id'].'" providerID="'.self::$providerID.'" assetID="'.$index_media['nns_media_id'].'"/>'.PHP_EOL;
    		$xml_str .= '</adi:ADI2>'.PHP_EOL;
    		$file_name = 'vod_media_'.$index_media['nns_video_index_id'].'_'.$index_media['nns_media_id'].'_'.$action.'.xml';
    
    		//die($xml_str);
    		$c2_info = array(
    				'nns_id' => $c2_id,
    				'nns_task_type'=>'Movie',
    				'nns_task_id'=> $index_media['nns_task_id'],
    				'nns_task_name'=> $nns_task_name,
    				'nns_action'=>  $action,
    				'nns_url' => $file_name,
    				'nns_content' => $xml_str,
    				'nns_desc' => 'Movie,'.$action,
    		);
    		return self::execute_c2_child($c2_info,$sp_id);
    	}
    }
    
    static public function add_movies_by_vod_index_muli($index_media,$sp_id)
    {
        $db = nn_get_db(NL_DB_WRITE);
        $db ->open();
        //获取这个影片的主媒资ID,查询这个媒资是电影还是连续剧
        $is_series = false;
        $sql_series = "select * from nns_vod where nns_id='{$index_media['nns_video_id']}'";
        $info = nl_db_get_one($sql_series, $db);
        $sql_task = "select task.* from nns_mgtvbk_c2_task as task left join nns_vod_index as inde on inde.nns_id=task.nns_src_id where inde.nns_vod_id='{$index_media['nns_video_id']}' and task.nns_type='media' and task.nns_status in('1','5') order by task.nns_src_id limit 4";
        $result_task = nl_query_by_db($sql_task, $db);
        unset($sql_series,$sql_task);
        if(!is_array($result_task) || empty($result_task))
        {
            return false;
        }
        $c2_id = np_guid_rand();
        $nns_series_flag = 0;
        if($info['nns_all_index']>1){
            $is_series = true;
            $nns_series_flag = 1;
        }else{
            $info_index['nns_name'] = $info['nns_name'];
            $nns_series_flag = 0;
        }
        $sp_config = sp_model::get_sp_config($sp_id);
        if(isset($sp_config['cdn_import_serise'])&&(int)$sp_config['cdn_import_serise']===1){
            $is_series = true;
            $nns_series_flag = 1;
        }
        $xml_str = '<adi:ADI2 xmlns="http://www.cablelabs.com/VODSchema/default" xmlns:adi="http://www.cablelabs.com/VODSchema/adi" xmlns:vod="http://www.cablelabs.com/VODSchema/vod">'.PHP_EOL;
        if($is_series===true)
        {
            $xml_str .=self::build_movie($info,'REGIST',$c2_id,$sp_id);
        }
        $array_task = null;
        $nns_task_name = '';
        $temp_arr_index_pre = array();
        foreach ($result_task as $task_val)
        {
            $array_task[$task_val['nns_id']]=array();
            $sql_index = "select * from nns_vod_index where nns_id='{$task_val['nns_src_id']}'";
            $info_index = nl_db_get_one($sql_index, $db);
            if(!is_array($info_index) || empty($info_index))
            {
                continue;
            }
            $info_index['nns_series_flag'] = $nns_series_flag;
            $action_tmp = $task_val['nns_action'];
            $sql = "select * from nns_vod_media where nns_vod_id='{$index_media['nns_video_id']}' and nns_vod_index_id='{$task_val['nns_src_id']}'  and nns_id='{$task_val['nns_ref_id']}' order by nns_kbps desc limit 1";
            $medias = nl_db_get_all($sql,$db);
            $kbps = $medias[0]['nns_kbps'];
            $nns_deleted = $medias[0]['nns_deleted'];
            if($action_tmp=='add'){
                $action = 'REGIST';
            }elseif($action_tmp=='modify'){
                $action = 'UPDATE';
            }elseif($action_tmp=='destroy'){
                $action = 'DELETE';
            }
            $file_size = $medias[0]['nns_file_size'] * 1024 * 1024;
            if($action=='REGIST'){
                $ContentAsset = 'AcceptContentAsset';
            }elseif($action=='UPDATE'){
                $ContentAsset = 'AcceptContentAsset';
            }elseif($action=='DELETE'){
                $ContentAsset = 'DestroyContentAsset';
            }
            if(!is_array($medias) || count($medias)<1)
            {
                continue;
            }
            $media_arr = $media = array();
            $media_url = "";
            foreach($medias as $media_item)
            {
                $mode = strtolower($media_item['nns_mode']);
                if($mode == '4' || $mode == 'sd')
                {
                    $mode = 'hd';
                    $media_item['nns_mode'] = 'hd';
                }
                $media_arr[$mode] = $media_item;
            }
            if(!empty($media_arr['hd'])){
                $media = $media_arr['hd'];
            }else if(!empty($media_arr['std'])){
                $media = $media_arr['std'];
            }else if(!empty($media_arr['low'])){
                $media = $media_arr['low'];
            }
            if(empty($media))
            {
                continue;
            }
            $video_profile =1;
            $mode = strtolower($media['nns_mode']);
            switch($mode){
                case 'std':
                    $video_profile = 1;
                    break;
                case 'hd':
                    $video_profile = 5;
                    break;
                case 'low':
                    $video_profile = 3;
                    break;
            }
            if(!empty($task_val['nns_file_path'])){
                $ftp_url_tmp = 'ftp://ftp_m3u8:ftp_m3u8@221.181.100.156:52114/'.$task_val['nns_file_path'];
                $url = MOVIE_FTP.'/'.$task_val['nns_file_path'];
            }else{
                $url = MOVIE_FTP.'/'.$task_val['nns_clip_date'].'/'.$task_val['nns_id'].'/'.$task_val['nns_ref_id'].'.ts';
                $ftp_url_tmp = 'ftp://ftp_m3u8:ftp_m3u8@221.181.100.156:52114/'.$task_val['nns_clip_date'].'/'.$task_val['nns_id'].'/'.$task_val['nns_ref_id'].'.ts';
            }
            $ini_file = array('size'=>$file_size,'md5'=>'',);
            $array_task[$task_val['nns_id']] = array(
                'video'=>$index_media['nns_video_id'],
                'index'=>$task_val['nns_src_id'],
                'media'=>$task_val['nns_ref_id'],
            );
            if(!in_array($task_val['nns_src_id'], $temp_arr_index_pre))
            {
                $temp_arr_index_pre[]=$task_val['nns_src_id'];
                $xml_str .=self::build_vod($info_index, 'REGIST', $c2_id, $sp_id);
            }
            $xml_str .= '<adi:'.$ContentAsset.' type="Video" metadataOnly="N" fileName="'.$task_val['nns_task_name'].'" fileSize="'.$ini_file['size'].'" mD5CheckSum="'.$ini_file['md5'].'">'.PHP_EOL;
            $xml_str .= '<vod:Video providerID="'.self::$providerID.'" assetID="'.$task_val['nns_ref_id'].'" updateNum="1" fileName="'.$task_val['nns_task_name'].'"  transferContentURL="'.$url.'" encodingProfile="application/octet-stream">'.PHP_EOL;
            $xml_str .= '<vod:Duration><![CDATA['.$medias[0]['nns_file_time_len'].']]></vod:Duration><vod:Bitrate><![CDATA['.intval($kbps*1000).']]></vod:Bitrate><vod:FrameRate><![CDATA[5]]></vod:FrameRate>';
            $xml_str .= '<vod:FileType><![CDATA[1]]></vod:FileType><vod:Usage><![CDATA[1]]></vod:Usage><vod:ServiceType><![CDATA[1]]></vod:ServiceType><vod:MimeType><![CDATA[46]]></vod:MimeType><vod:HDFlag>0</vod:HDFlag></vod:Video>';
            $xml_str .= '</adi:'.$ContentAsset.'>'.PHP_EOL;
            $xml_str .= '<adi:AssociateContent type="Video" effectiveDate="" groupProviderID="'.self::$providerID.'" groupAssetID="'.$task_val['nns_src_id'].'" providerID="'.self::$providerID.'" assetID="'.$task_val['nns_ref_id'].'"/>'.PHP_EOL;
        }
        $xml_str .= '</adi:ADI2>'.PHP_EOL;
        
        $file_name = 'vod_media_'.$index_media['nns_task_id'].'_'.$index_media['nns_media_id'].'_1_'.$action.'.xml';
        $c2_info = array(
            'nns_id' => $c2_id,
            'nns_task_type'=>'Movie',
            'nns_task_id'=> $index_media['nns_task_id'],
            'nns_task_name'=> $index_media['nns_task_name'],
            'nns_action'=>  $action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            'nns_desc' => 'Movie,'.$action,
            'nns_ex_info'=> (is_array($array_task) && !empty($array_task)) ? json_encode($array_task) : '',
        );
        $result =  self::execute_c2_child($c2_info,$sp_id);
        if(!$result)
        {
            return self::add_movies_by_vod_index_muli_v2($index_media,$sp_id);
        }
        return $result;
    }
    
    
    
    
    static public function add_movies_by_vod_index_muli_v2($index_media,$sp_id)
    {
        $db = nn_get_db(NL_DB_WRITE);
        $db ->open();
        //获取这个影片的主媒资ID,查询这个媒资是电影还是连续剧
        $is_series = false;
        $sql_series = "select * from nns_vod where nns_id='{$index_media['nns_video_id']}'";
        $info = nl_db_get_one($sql_series, $db);
        $sql_task = "select task.* from nns_mgtvbk_c2_task as task left join nns_vod_index as inde on inde.nns_id=task.nns_src_id where inde.nns_vod_id='{$index_media['nns_video_id']}' and task.nns_type='media' and task.nns_status in('1','5') order by task.nns_src_id limit 4";
        $result_task = nl_query_by_db($sql_task, $db);
        unset($sql_series,$sql_task);
        if(!is_array($result_task) || empty($result_task))
        {
            return false;
        }
        $c2_id = np_guid_rand();
        $nns_series_flag = 0;
        if($info['nns_all_index']>1){
            $is_series = true;
            $nns_series_flag = 1;
        }else{
            $info_index['nns_name'] = $info['nns_name'];
            $nns_series_flag = 0;
        }
        $sp_config = sp_model::get_sp_config($sp_id);
        if(isset($sp_config['cdn_import_serise'])&&(int)$sp_config['cdn_import_serise']===1){
            $is_series = true;
            $nns_series_flag = 1;
        }
        $xml_str = '<adi:ADI2 xmlns="http://www.cablelabs.com/VODSchema/default" xmlns:adi="http://www.cablelabs.com/VODSchema/adi" xmlns:vod="http://www.cablelabs.com/VODSchema/vod">'.PHP_EOL;
        if($is_series===true)
        {
            $xml_str .=self::build_series($info,'REGIST',$c2_id,$sp_id);
        }
        $array_task = null;
        $nns_task_name = '';
        $temp_arr_index_pre=array();
        foreach ($result_task as $task_val)
        {
            $array_task[$task_val['nns_id']]=array();
            $sql_index = "select * from nns_vod_index where nns_id='{$task_val['nns_src_id']}'";
            $info_index = nl_db_get_one($sql_index, $db);
            if(!is_array($info_index) || empty($info_index))
            {
                continue;
            }
            $info_index['nns_series_flag'] = $nns_series_flag;
            $action_tmp = $task_val['nns_action'];
            $sql = "select * from nns_vod_media where nns_vod_id='{$index_media['nns_video_id']}' and nns_vod_index_id='{$task_val['nns_src_id']}'  and nns_id='{$task_val['nns_ref_id']}' order by nns_kbps desc limit 1";
            $medias = nl_db_get_all($sql,$db);
            $kbps = $medias[0]['nns_kbps'];
            $nns_deleted = $medias[0]['nns_deleted'];
            if($action_tmp=='add'){
                $action = 'REGIST';
            }elseif($action_tmp=='modify'){
                $action = 'UPDATE';
            }elseif($action_tmp=='destroy'){
                $action = 'DELETE';
            }
            $file_size = $medias[0]['nns_file_size'] * 1024 * 1024;
            if($action=='REGIST'){
                $ContentAsset = 'AcceptContentAsset';
            }elseif($action=='UPDATE'){
                $ContentAsset = 'AcceptContentAsset';
            }elseif($action=='DELETE'){
                $ContentAsset = 'DestroyContentAsset';
            }
            if(!is_array($medias) || count($medias)<1)
            {
                continue;
            }
            $media_arr = $media = array();
            $media_url = "";
            foreach($medias as $media_item)
            {
                $mode = strtolower($media_item['nns_mode']);
                if($mode == '4' || $mode == 'sd')
                {
                    $mode = 'hd';
                    $media_item['nns_mode'] = 'hd';
                }
                $media_arr[$mode] = $media_item;
            }
            if(!empty($media_arr['hd'])){
                $media = $media_arr['hd'];
            }else if(!empty($media_arr['std'])){
                $media = $media_arr['std'];
            }else if(!empty($media_arr['low'])){
                $media = $media_arr['low'];
            }
            if(empty($media))
            {
                continue;
            }
            $video_profile =1;
            $mode = strtolower($media['nns_mode']);
            switch($mode){
                case 'std':
                    $video_profile = 1;
                    break;
                case 'hd':
                    $video_profile = 5;
                    break;
                case 'low':
                    $video_profile = 3;
                    break;
            }
            if(!empty($task_val['nns_file_path'])){
                $ftp_url_tmp = 'ftp://ftp_m3u8:ftp_m3u8@221.181.100.156:52114/'.$task_val['nns_file_path'];
                $url = MOVIE_FTP.'/'.$task_val['nns_file_path'];
            }else{
                $url = MOVIE_FTP.'/'.$task_val['nns_clip_date'].'/'.$task_val['nns_id'].'/'.$task_val['nns_ref_id'].'.ts';
                $ftp_url_tmp = 'ftp://ftp_m3u8:ftp_m3u8@221.181.100.156:52114/'.$task_val['nns_clip_date'].'/'.$task_val['nns_id'].'/'.$task_val['nns_ref_id'].'.ts';
            }
            $ini_file = array('size'=>$file_size,'md5'=>'',);
            $array_task[$task_val['nns_id']] = array(
                'video'=>$index_media['nns_video_id'],
                'index'=>$task_val['nns_src_id'],
                'media'=>$task_val['nns_ref_id'],
            );
            if(!in_array($task_val['nns_src_id'], $temp_arr_index_pre))
            {
                $temp_arr_index_pre[]=$task_val['nns_src_id'];
                $xml_str .=self::build_vod($info_index, 'REGIST', $c2_id, $sp_id);
            }
            $xml_str .= '<adi:'.$ContentAsset.' type="Video" metadataOnly="N" fileName="'.$task_val['nns_task_name'].'" fileSize="'.$ini_file['size'].'" mD5CheckSum="'.$ini_file['md5'].'">'.PHP_EOL;
            $xml_str .= '<vod:Video providerID="'.self::$providerID.'" assetID="'.$task_val['nns_ref_id'].'" updateNum="1" fileName="'.$task_val['nns_task_name'].'"  transferContentURL="'.$url.'" encodingProfile="application/octet-stream">'.PHP_EOL;
            $xml_str .= '<vod:Duration><![CDATA['.$medias[0]['nns_file_time_len'].']]></vod:Duration><vod:Bitrate><![CDATA['.intval($kbps*1000).']]></vod:Bitrate><vod:FrameRate><![CDATA[5]]></vod:FrameRate>';
            $xml_str .= '<vod:FileType><![CDATA[1]]></vod:FileType><vod:Usage><![CDATA[1]]></vod:Usage><vod:ServiceType><![CDATA[1]]></vod:ServiceType><vod:MimeType><![CDATA[46]]></vod:MimeType><vod:HDFlag>0</vod:HDFlag></vod:Video>';
            $xml_str .= '</adi:'.$ContentAsset.'>'.PHP_EOL;
            $xml_str .= '<adi:AssociateContent type="Video" effectiveDate="" groupProviderID="'.self::$providerID.'" groupAssetID="'.$task_val['nns_src_id'].'" providerID="'.self::$providerID.'" assetID="'.$task_val['nns_ref_id'].'"/>'.PHP_EOL;
        }
        $xml_str .= '</adi:ADI2>'.PHP_EOL;
        
        $file_name = 'vod_media_'.$index_media['nns_task_id'].'_'.$index_media['nns_media_id'].'_2_'.$action.'.xml';
        $c2_info = array(
            'nns_id' => $c2_id,
            'nns_task_type'=>'Movie',
            'nns_task_id'=> $index_media['nns_task_id'],
            'nns_task_name'=> $index_media['nns_task_name'],
            'nns_action'=>  $action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            'nns_desc' => 'Movie,'.$action,
        );
        return self::execute_c2_child($c2_info,$sp_id);
    }
    
    
    
    
    static public function execute_c2_child($c2_info,$sp_id){
    	$flag = false;
        $c2_id = $c2_info['nns_id'];
        $sub_path = date('Y-m-d');
        $file = $c2_info['nns_url'];
        $c2_info['nns_url'] = $sub_path.'/'.$file;
        $xml_str = $c2_info['nns_content'];
        $file_path = dirname(dirname(dirname(dirname(__FILE__)))).'/data/mgtv/'.$sp_id.'/inject_xml/'.$sub_path.'/';
        
        if(!is_dir($file_path)) mkdir($file_path,0777,true);
        $file_path .= $file;
        //die($file_path);
        file_put_contents($file_path,$xml_str);
        if(!self::exists_c2_commond_child($c2_info,$sp_id)){
            
            if($c2_info['nns_task_type']=='Channel'){
                $result= self::__gateway_run('http://172.25.0.1/nn_gateway/shanghai/cms_to_sihua_live.php',null,'post',$xml_str,null);
            }else{
                $result= self::__gateway_run('http://172.25.0.1/nn_gateway/shanghai/cms_to_sihua.php',null,'post',$xml_str,null);
            }
            $c2_info_temp = $c2_info;
            unset($c2_info_temp['nns_content']);
            //DEBUG && i_write_log_core('--adi注入思华数据为--'.var_export($xml_str,true).'类型为：'.$c2_info['nns_task_type']);
            DEBUG && i_write_log_core('--adi注入思华cdn返回的结果为--task_info:'.var_export($c2_info_temp,true).'；结果：'.var_export($result,true).'类型为：'.$c2_info['nns_task_type']);
        //$result['data'] = '0|1';
        $c2_info['nns_id'] = $c2_id;
        $result_arr = explode('|', $result['data']);
        //$result_arr[0] = 0;
        $c2_info['nns_result'] = '['.$result_arr[0].']';
        
        if($result_arr[0]!=0){
            //$c2_info['nns_again']=1; 
            $c2_info['nns_notify_result'] = -1;
        }
        else
        {
        	$flag = true;
        }
        //$c2_info['nns_result'] = 0;
        self::save_c2_log_child($c2_info,$sp_id);
        return $flag;
        }
        
        return $flag;
    }
    
    
    static public function exists_c2_commond_child($c2_info,$sp_id){
        $db_r = nn_get_db(NL_DB_READ);
        $nns_org_id = $sp_id;
        $nns_task_type = $c2_info['nns_task_type'];
        $nns_task_id = $c2_info['nns_task_id'];
        $nns_action = $c2_info['nns_action'];
        
        if(empty($nns_task_id)) return false;
        $start_time = date('Y-m-d').' 00:00:00';
        $end_time = date('Y-m-d').' 23:59:59';
        $count = "select count(1) as num from nns_mgtvbk_c2_log where nns_org_id='{$sp_id}' and nns_task_id='{$nns_task_id}' and nns_task_type='{$nns_task_type}' and nns_action='{$nns_action}' and nns_create_time>='{$start_time}' and nns_create_time<='{$end_time}' and nns_notify_result is null";
        $c = nl_db_get_col($count, $db_r);
        if($c>=2){
            return true;
        }
    }
    
    
    static public function save_c2_log_child($c2_log,$sp_id){
        $db = nn_get_db(NL_DB_WRITE);
        //$db->open();
        $c2_log['nns_type'] = 'std';
        $c2_log['nns_org_id'] = $sp_id;
        $dt = date('Y-m-d H:i:s');
        $c2_log['nns_create_time'] = $dt;
        $c2_log['nns_send_time'] = $dt;
        return nl_db_insert($db,'nns_mgtvbk_c2_log',$c2_log);
    }



    static public function __gateway_run($url,$log=NULL,$method='get',$params=NULL,$header=NULL){
        
        //return true;
    
        if ($method=='get'){
            $data = http_build_query($params);
            if (strpos('?',$url)>0){
                $url .='&'.$data;
            }else{
                $url .='?'.$data;
            }
        }       
        $ch = curl_init();      
        curl_setopt($ch, CURLOPT_HEADER, 0);        
        if (is_array($header)){
            curl_setopt($ch,CURLOPT_HTTPHEADER,$header);
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        if ($method=='post'){
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10); //超时时间
        $output = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $reason = curl_error($ch);
        curl_close($ch);        
        $return=array(
            'data'=>$output,
            'code'=>$code,
            'reason'=>$reason
        ); 
        return $return;
    }


    static public function img_handel($img_from = "") {
        //return true;
        include_once dirname(dirname(dirname(__FILE__))).'/models/curl.class.php';
        global $g_ftp_conf;
        if (empty($img_from)){
            return false;
        }
        //组装保存在本地的临时图片名称
        $img_name = basename($img_from);
        $img_dir = $g_ftp_conf["down_img_dir"] . '/' . $img_name;
        if (!empty($g_ftp_conf["down_img_dir"]) && !is_dir($g_ftp_conf["down_img_dir"])) {
            $flg = mkdir($g_ftp_conf["down_img_dir"], 0777, true);
            //递归创建
            if (!$flg) {
                return false;
            }
        } else {
            @chmod($g_ftp_conf["down_img_dir"], 0777);
        }   
        if (stripos($img_from, 'http://') === FALSE && stripos($img_from, 'https://') === FALSE) {
            if (!isset($g_ftp_conf["domain"]) || empty($g_ftp_conf["domain"])) {
                return false;
            }
            $down_img = $g_ftp_conf["domain"] . $img_from;
        } else {
            $down_img = $img_from;
        }
        $setopt = array('port' => '80');
        $cu = new curl($setopt);
        $contents = $cu -> get($down_img);
        $getinfo = $cu -> getInfo();
        $infocode = $getinfo['after']['http_code'];
        if ($contents === false || $infocode != '200') {
            $error = $cu -> error();
            $error_no = $cu -> errno();
            return false;
        }   
        $handle = fopen($img_dir, "w");
        $re = fwrite($handle, $contents);
        fclose($handle);
        if (empty($re)) {
            return false;
        }   
        $remote_path = '';  
        if (defined("ROOT_IMG_CATEGORY") && ROOT_IMG_CATEGORY) {
            $remote_path = ROOT_IMG_CATEGORY;
        }   
        if (defined("IMPORT_IMG_CATEGORY") && IMPORT_IMG_CATEGORY) {
            $img_extend_dir = dirname($img_from);
            for ($num = 0; $num < IMPORT_IMG_CATEGORY; $num++) {
                $img_extend_dir = dirname($img_extend_dir);
            }   
            $img_extend_dir = str_replace($img_extend_dir, "", dirname($img_from)); 
            $remote_path = $remote_path . '/' . $img_extend_dir;
        }
        $img_url = false;
        $ftp_result = self::__import_up_ftp_img(1, $img_name, $img_dir, $remote_path);  
        if ($ftp_result === FALSE) {
            return FALSE;
        }
        $img_url = true;    
        return $img_url;
    }
    static public function __import_up_ftp_img($num, $img_name, $img_dir, $extend_dir = '') {
        
        //return true;
        global $g_ftp_conf;
        $ftp_key = 'ftp_to' . $num;
        if (!$g_ftp_conf[$ftp_key]) {
            return false;
        }
        include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/np/np_ftp.php';
        $passive = isset($g_ftp_conf[$ftp_key]['passive']) && $g_ftp_conf[$ftp_key]['passive'] === false ? false : true;
        //实例化np_ftp类
        $ftp_to1 = new nns_ftp($g_ftp_conf[$ftp_key]["address"], $g_ftp_conf[$ftp_key]["user"], $g_ftp_conf[$ftp_key]["password"], $g_ftp_conf[$ftp_key]["port"]);
    
        if (!$ftp_to1 -> connect(30, false)) {
            return false;
        }
        $remote_path = rtrim($g_ftp_conf[$ftp_key]["img_dir"], '/');        
        if ($extend_dir != '') {
            $remote_path = $remote_path . '/' . $extend_dir;
        }
        $up_img1 = $ftp_to1 -> up($remote_path, $img_name, $img_dir);
        if (!$up_img1) {
            return false;
        }   
        return true;
    }
    
    
    
    static  public function child_check_ftp_file($url){
        //return false;
        $url_arr = parse_url($url);
        $port = isset($url_arr["port"]) ? $url_arr["port"] : 21;
        $time = 10;
        $connect_no=ftp_connect($url_arr['host'],$port,$time);
        if ($connect_no===FALSE) return FALSE;
        
        @ftp_login(
        $connect_no,
        $url_arr["user"],
        $url_arr["pass"]
        );
        @ftp_pasv($connect_no,true);
        $contents = ftp_nlist($connect_no, $url_arr['path']);
        DEBUG && i_write_log_core('path_fail----'.$url.var_export($contents,true),'sihua/path_fail_ftp');
        if($contents===false){
            return FALSE;
        }else{
            if(empty($contents)){
                return FALSE;
            }
            return true;
        }
    }
    
    //思华删除片源模式，若是删除分集第一集，则发出2条删除指令
    static public function delete_movie($params,$sp_id)
    {
    	$db = nn_get_db(NL_DB_WRITE);
    	$c2_id = np_guid_rand();
    	$c2_id_movie = np_guid_rand();
    	$action = 'DELETE';
    	$xml_str = '<adi:ADI2 xmlns="http://www.cablelabs.com/VODSchema/default" xmlns:adi="http://www.cablelabs.com/VODSchema/adi" xmlns:vod="http://www.cablelabs.com/VODSchema/vod">'.PHP_EOL;
    	$xml_movie_str = '';
    	$sql_index = "select * from nns_vod_index where nns_id='{$params['nns_video_index_id']}'";
        $info_index = nl_db_get_one($sql_index, $db);
        
        $sql_series = "select * from nns_vod where nns_id='{$params['nns_video_id']}'";
        $info = nl_db_get_one($sql_series, $db);
        //若删除分集是第一集，则按需要按电影模式发出一条删除指令
//         if($info_index['nns_index'] < 1)
//         {
        	$xml_movie_str .= '<adi:ADI2 xmlns="http://www.cablelabs.com/VODSchema/default" xmlns:adi="http://www.cablelabs.com/VODSchema/adi" xmlns:vod="http://www.cablelabs.com/VODSchema/vod">'.PHP_EOL;
        	$xml_movie_str .= self::build_movie($info, 'REGIST', $c2_id_movie, $sp_id);
        	$xml_movie_str .= self::build_vod($info_index, 'REGIST', $c2_id_movie, $sp_id);
//         }
        $info_index['nns_series_flag'] = 1;
        $xml_str .= self::build_series($info,'REGIST',$c2_id,$sp_id);
        $xml_str .= self::build_vod($info_index, 'REGIST', $c2_id, $sp_id);
        $sql = "select * from nns_vod_media where  nns_vod_id='{$params['nns_video_id']}' and nns_vod_index_id='{$params['nns_video_index_id']}'  and nns_id='{$params['nns_media_id']}'   order by nns_kbps desc limit 1";
        $medias = nl_db_get_all($sql,$db);
        $kbps = $medias[0]['nns_kbps'];
        $nns_deleted = $medias[0]['nns_deleted'];
        $file_size = $medias[0]['nns_file_size'] * 1024 * 1024;
        
        $ContentAsset = 'DestroyContentAsset';
        if(is_array($medias) && count($medias)>0)
        {           
            $media = array();
            $media_arr = array();

            $media_url = "";
            foreach($medias as $media_item)
            {
                $mode = strtolower($media_item['nns_mode']);
                $media_arr[$mode] = $media_item;
            }
            if(!empty($media_arr['hd']))
            {
                $media = $media_arr['hd'];
            }
            else if(!empty($media_arr['std']))
            {
                $media = $media_arr['std'];
            }
            else if(!empty($media_arr['low']))
            {
                $media = $media_arr['low'];
            }
            if(empty($media)) return false;
            
            $video_profile =1;
            $mode = strtolower($media['nns_mode']);
            switch($mode){
            	case 'std':
                    $video_profile = 1;
                    break;
                case 'hd':
                    $video_profile = 5;
                    break;
                case 'low':
                    $video_profile = 3;
                    break;
            }
     
            if(!empty($params['nns_file_path']))
            {                      
                $ftp_url_tmp = 'ftp://ftp_m3u8:ftp_m3u8@221.181.100.156:52114/'.$params['nns_file_path'];
                $url = MOVIE_FTP.'/'.$params['nns_file_path'];
            }
            else
            {
                $url = MOVIE_FTP.'/'.$params['nns_date'].'/'.$params['nns_task_id'].'/'.$params['nns_media_id'].'.ts';
                $ftp_url_tmp = 'ftp://ftp_m3u8:ftp_m3u8@221.181.100.156:52114/'.$params['nns_date'].'/'.$params['nns_task_id'].'/'.$params['nns_media_id'].'.ts';
            }            

	        $ini_file = array('size'=>$file_size,'md5'=>'',);
	        $xml_media_str = '<adi:'.$ContentAsset.' type="Video" metadataOnly="N" fileName="'.$nns_task_name.'" fileSize="'.$ini_file['size'].'" mD5CheckSum="'.$ini_file['md5'].'">'.PHP_EOL;
	        $xml_media_str .= '<vod:Video providerID="'.self::$providerID.'" assetID="'.$params['nns_media_id'].'" updateNum="1" fileName="'.$nns_task_name.'"  transferContentURL="'.$url.'" encodingProfile="application/octet-stream">'.PHP_EOL;
	        $xml_media_str .= '<vod:Duration>
<![CDATA['.$medias[0]['nns_file_time_len'].']]>
</vod:Duration>
<vod:Bitrate>
<![CDATA['.intval($kbps*1000).']]>
</vod:Bitrate>
<vod:FrameRate>
<![CDATA[5]]>
</vod:FrameRate>
<vod:FileType>
<![CDATA[1]]>
</vod:FileType>
<vod:Usage>
<![CDATA[1]]>
</vod:Usage>
<vod:ServiceType>
<![CDATA[1]]>
</vod:ServiceType>
<vod:MimeType>
<![CDATA[46]]>
</vod:MimeType>
<vod:HDFlag>0</vod:HDFlag></vod:Video>';
	        $xml_media_str .= '</adi:'.$ContentAsset.'>'.PHP_EOL;
	        $xml_media_str .= '<adi:AssociateContent type="Video" effectiveDate="" groupProviderID="'.self::$providerID.'" groupAssetID="'.$params['nns_video_index_id'].'" providerID="'.self::$providerID.'" assetID="'.$params['nns_media_id'].'"/>'.PHP_EOL;
	        $xml_media_str .= '</adi:ADI2>'.PHP_EOL;

	        $flag = false;
	        $flag_1 = false;
	        
	        if(!empty($xml_movie_str))
	        {
	        	$xml_movie_str .= $xml_media_str;
	        	$file_name_movie = 'movie_vod_media_'.$params['nns_video_index_id'].'_'.$params['nns_media_id'].'_'.$action.'.xml';
	        	$c2_info_movie = array(
	                    'nns_id' => $c2_id_movie,
	                    'nns_task_type'=>'Movie',
	                    'nns_task_id'=> $params['nns_task_id'],
	                    'nns_task_name'=> $params['nns_task_name'],
	                    'nns_action'=>  $action,
	                    'nns_url' => $file_name_movie,
	                    'nns_content' => $xml_movie_str,
	                    'nns_desc' => 'Movie,'.$action,
	        	);
	        	$flag = self::execute_c2_child($c2_info_movie,$sp_id);	        	
	        }
	        if($flag)
	        {
	        	return true;
	        }
	        $xml_str .= $xml_media_str;
	        $file_name = 'vod_media_'.$params['nns_video_index_id'].'_'.$params['nns_media_id'].'_'.$action.'.xml';
	        $c2_info = array(
	                    'nns_id' => $c2_id,
	                    'nns_task_type'=>'Movie',
	                    'nns_task_id'=> $params['nns_task_id'],
	                    'nns_task_name'=> $params['nns_task_name'],
	                    'nns_action'=>  $action,
	                    'nns_url' => $file_name,
	                    'nns_content' => $xml_str,
	                    'nns_desc' => 'Movie,'.$action,
	         );
	         $flag_1 =  self::execute_c2_child($c2_info,$sp_id);
	         if($flag || $flag_1)
	         {
	         	return true;
	         }
	         return false;
// 	         return self::execute_c2_child($c2_info,$sp_id);
        }
    }
    /**
	 * 创建 电影媒资
     */
    static public function build_movie($vod_info,$action,$c2_id,$sp_id)
    {
        $db = nn_get_db(NL_DB_READ);
        $vod_id = $vod_info['nns_id'];
        $program_id = $vod_id;
        if($action=='REGIST'){
            $group_asset = 'OpenGroupAsset';
            $MetadataAsset = 'AddMetadataAsset';
            $ContentAsset = 'AcceptContentAsset';
        }elseif($action=='UPDATE'){
            $group_asset = 'ReplaceGroupAsset';
            $MetadataAsset = 'ReplaceMetadataAsset';
            $ContentAsset = 'ReplaceContentAsset';
        }elseif($action=='DELETE'){
            $group_asset = 'DropGroupAsset';
            $MetadataAsset = 'RemoveMetadataAsset';
            $ContentAsset = 'DestroyContentAsset';
        }
        //$c2_id = np_guid_rand();
        $xml_str = '';
        $xml_str .='<adi:'.$group_asset.' type="VODRelease" product="VOD">'.PHP_EOL;
        $xml_str .='<vod:VODRelease providerID="'.self::$providerID.'" providerType="'.self::$providerType.'" assetID="'.$program_id.'" updateNum="" groupAsset="Y" serialNo="'.$c2_id.'"></vod:VODRelease>'.PHP_EOL;
        $xml_str .='</adi:'.$group_asset.'>'.PHP_EOL;
        $xml_str .='<adi:'.$MetadataAsset.' groupProviderID="'.self::$providerID.'" groupAssetID="'.$program_id.'" type="Title" product="VOD">'.PHP_EOL;
        $xml_str .='<vod:Title providerID="'.self::$providerID.'" assetID="'.$program_id.'" updateNum="1">'.PHP_EOL;
        $xml_str .='<vod:TitleFull><![CDATA['.htmlspecialchars($vod_info['nns_name']).']]></vod:TitleFull>'.PHP_EOL;//电影名称
        $xml_str .='<vod:ShowType>Movie</vod:ShowType>'.PHP_EOL;//电影类型标识
        if($vod_info['nns_eng_name']){
        $xml_str .='<vod:EnglishName><![CDATA['.$vod_info['nns_eng_name'].']]></vod:EnglishName>'.PHP_EOL;//英文名称
        }
        if($vod_info['nns_summary']){
        $xml_str .='<vod:SummaryMedium><![CDATA['.$vod_info['nns_summary'].']]></vod:SummaryMedium>'.PHP_EOL;//中文描述
        $xml_str .='<vod:SummaryShort><![CDATA['.$vod_info['nns_summary'].']]></vod:SummaryShort>'.PHP_EOL;//看点
        }
        if(!empty($vod_info['nns_area'])){
            $country_code = self::get_country($vod_info['nns_area']);
            $xml_str .='<vod:CountryOfOrigin><![CDATA['.$country_code.']]></vod:CountryOfOrigin>'.PHP_EOL;//国家地区 内地=1,港台=2,韩日=3,欧美=4, 东南亚=5，其他=99
        }
        if(!empty($vod_info['nns_actor'])){
            $xml_str .='<vod:Actor type="Actor">'.PHP_EOL;//演员
            $xml_str .='<vod:LastNameFirst><![CDATA['.$vod_info['nns_actor'].']]></vod:LastNameFirst>'.PHP_EOL;
            $xml_str .='</vod:Actor>'.PHP_EOL;
        }
        //导演
        if(!empty($vod_info['nns_director'])){
            $xml_str .='<vod:Director><vod:LastNameFirst><![CDATA['.$vod_info['nns_director'].']]></vod:LastNameFirst></vod:Director>'.PHP_EOL;
        }
        //year
        if(!empty($vod_info['nns_show_time'])){
            $xml_str .= '<vod:Year><![CDATA['.date('Y',strtotime($vod_info['nns_show_time'])).']]></vod:Year>'.PHP_EOL;
        }
        //language
        if(!empty($vod_info['nns_language'])){
            $xml_str .= '<vod:Language><![CDATA['.$vod_info['nns_language'].']]></vod:Language>'.PHP_EOL;
        }
        //nns_keyword
        if(!empty($vod_info['nns_keyword'])){
            $xml_str .='<vod:Keyword><![CDATA['.str_replace('/', ' ', $vod_info['nns_keyword']).']]></vod:Keyword>'.PHP_EOL;
        }
        //$xml_str .='<vod:IsAdvertise>0</vod:IsAdvertise>';//0：非广告内容; 1：广告内容

        $xml_str .='</vod:Title>'.PHP_EOL;
        $xml_str .= '</adi:'.$MetadataAsset.'>'.PHP_EOL;
        
        
        $category = '';
        $array_category = array(
            '10000001'=>'电影',
            '10000002'=>'电视剧',
            '10000003'=>'综艺',
            '10000004'=>'动漫',
            '10000005'=>'音乐',
            '10000006'=>'纪实',
            '10000010'=>'纪录片',
            '10000021'=>'广告',
        );
        $category_id = nl_db_get_col("select nns_category_id from nns_vod where nns_id='$program_id'",$db);
        //
        $category = $array_category[$category_id];
        $xml_str .= '<adi:'.$MetadataAsset.' groupProviderID="'.self::$providerID.'" groupAssetID="'.$program_id.'" type="CategoryPath" product="VOD">
        <vod:CategoryPath mso="" providerID="'.self::$providerID.'" assetID="'.$program_id.'" updateNum="1">
        <adi:AssetLifetime startDateTime="2012-09-04" endDateTime="2062-09-04"/>
        <vod:Classification>
        <![CDATA['.$category.']]>
        </vod:Classification>
        </vod:CategoryPath>
        </adi:'.$MetadataAsset.'>';
        
        
        $img_save_name = str_replace('.JPG','.jpg',$vod_info['nns_image2']);
        $file_img = dirname(dirname(dirname(dirname(__FILE__)))).'/data/downimg/'.$img_save_name;
        
        //if(file_exists($file_img)){
            $img = self::img_handel($img_save_name);
            if(!empty($vod_info['nns_image2'])&&$img===true){
                $xml_str .= '<adi:'.$ContentAsset.' type="Image" metadataOnly="N">'.PHP_EOL;
                //图片的实体ID取用图片的名称 
                //$image_asset_id = str_replace('/', '', $vod_info['nns_image2']);
                $image_asset_id = $program_id.'image2';
                $file_fix = substr($vod_info['nns_image2'],  -3,3);
                $img_save_name = str_replace('.JPG','.jpg',$vod_info['nns_image2']);
                $pic_url = self::get_image_url($img_save_name,$sp_id);
                $xml_str .= '<vod:Image providerID="'.self::$providerID.'" assetID="'.$image_asset_id.'" updateNum="1" transferContentURL="'.$pic_url.'" imageEncodingProfile="'.$file_fix.'"    fileName="'.$image_asset_id.'">'.PHP_EOL;
                $xml_str .='<vod:FileType><![CDATA[3]]></vod:FileType>'.PHP_EOL;
                $xml_str .='<vod:Usage><![CDATA[15]]></vod:Usage>'.PHP_EOL;
                $xml_str .='</vod:Image>'.PHP_EOL;
                $xml_str .= '</adi:'.$ContentAsset.'>'.PHP_EOL;
                $xml_str .= '<adi:AssociateContent type="Image"  groupProviderID="'.self::$providerID.'" groupAssetID="'.$program_id.'" providerID="'.self::$providerID.'" assetID="'.$image_asset_id.'"/>'.PHP_EOL;
            }       
        return $xml_str;
    }
}