<?php



function i_date($msg = '') {
	echo date('Y-m-d H:i:s') . "   " . $msg . "\n";
}

function i_echo($msg = '') {
	echo date('Y-m-d H:i:s') . "   " . $msg . "\n";
}

function __gateway_run($url, $log = NULL, $method = 'get', $params = NULL, $header = NULL) {

	if ($method == 'get') {
		$data = http_build_query($params);
		if (strpos('?', $url) > 0) {
			$url .= '&' . $data;
		} else {
			$url .= '?' . $data;
		}
	}

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_HEADER, 0);

	if (is_array($header)) {
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	}
	curl_setopt($ch, CURLOPT_URL, $url);
	if ($method == 'post') {
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
	}
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	//超时时间
	$output = curl_exec($ch);
	$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$reason = curl_error($ch);
	curl_close($ch);

	$return = array('data' => $output, 'code' => $code, 'reason' => $reason);

	return $return;

}

function i_write_log_core($str, $subpath = null) {
	//
	$log_path = dirname(dirname(__FILE__)) . '/data/log/mgtv';

	if ($subpath != null) {
		$log_path .= '/' . $subpath;
	}
	if (!is_dir($log_path)) {
		mkdir($log_path, 0777, true);
	}
	$log_file = $log_path . '/' . date('Y-m-d') . '.txt';
	$msg = '[' . date('H:i:s') . ']    ' . $str . "\n";
	@error_log($msg, 3, $log_file);
}

function i_get_ip() {
	if (isset($_SERVER)) {
		if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);

			/* 取X-Forwarded-For中第一个非unknown的有效IP字符串 */
			foreach ($arr AS $ip) {
				$ip = trim($ip);

				if ($ip != 'unknown') {
					$realip = $ip;

					break;
				}
			}
		} elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
			$realip = $_SERVER['HTTP_CLIENT_IP'];
		} else {
			if (isset($_SERVER['REMOTE_ADDR'])) {
				$realip = $_SERVER['REMOTE_ADDR'];
			} else {
				$realip = '0.0.0.0';
			}
		}
	} else {
		if (getenv('HTTP_X_FORWARDED_FOR')) {
			$realip = getenv('HTTP_X_FORWARDED_FOR');
		} elseif (getenv('HTTP_CLIENT_IP')) {
			$realip = getenv('HTTP_CLIENT_IP');
		} else {
			$realip = getenv('REMOTE_ADDR');
		}
	}

	preg_match("/[\d\.]{7,15}/", $realip, $onlineip);
	$realip = !empty($onlineip[0]) ? $onlineip[0] : '0.0.0.0';

	return $realip;
}

function i_substr($string, $length) {
	if (strlen($string) <= $length)
		return $string;
	//utf8编码
	$n = 0;
	$tn = 0;
	$noc = 0;
	while ($n < strlen($string)) {
		$t = ord($string[$n]);
		if ($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
			$tn = 1;
			$n++;
			$noc++;
		} elseif (194 <= $t && $t <= 223) {
			$tn = 2;
			$n += 2;
			$noc += 2;
		} elseif (224 <= $t && $t < 239) {
			$tn = 3;
			$n += 3;
			$noc += 2;
		} elseif (240 <= $t && $t <= 247) {
			$tn = 4;
			$n += 4;
			$noc += 2;
		} elseif (248 <= $t && $t <= 251) {
			$tn = 5;
			$n += 5;
			$noc += 2;
		} elseif ($t == 252 || $t == 253) {
			$tn = 6;
			$n += 6;
			$noc += 2;
		} else {
			$n++;
		}
		if ($noc >= $length) {
			break;
		}
	}
	if ($noc > $length) {
		$n -= $tn;
	}
	return substr($string, 0, $n);
}
function i_object_guid( $long = 32,$something = "rand" )
{
	$result = dechex(  time() );
	list ( $usec, $sec ) = explode ( ' ', microtime () );
	$i_millisecond = intval( substr ( $usec, 2, 3 ) );
	$result = $result.dechex( $i_millisecond );

	$a = "";
	if( isset( $_ENV ["COMPUTERNAME"] ) )
		$a .= $_ENV ["COMPUTERNAME"];
	if( isset( $_SERVER ["SERVER_ADDR"] ) )
		$a .= $_SERVER ["SERVER_ADDR"];
	if( isset( $_SERVER ["REMOTE_ADDR"] ) )
		$a .= $_SERVER ["REMOTE_ADDR"];

	//echo $a;

	$a = $a.rand(0,10000);
	$a = $a.rand(0,10000);
	$a = $a.rand(0,10000);
    $a = $a.microtime ();


	$result = $result.md5( $a.$something );
	return substr( $result, 0, $long );
}