<?php
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
array_pop($arr_dir);
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/init.php';

class timer_execute extends nn_timer
{
    public function action($params = null)
    {
        $this->msg('开始执行...');
        do_timer_action();
        $this->msg('执行结束...');
    }
}
$arr_files = pathinfo(__FILE__);
$file_name = $arr_files['filename'];
$timer_execute = new timer_execute($file_name, ORG_ID,__FILE__);
$timer_execute->run();

function do_timer_action()
{
    //从任务表取内容，调用C2执行
    i_echo('start');
    $sp_config = sp_model::get_sp_config(ORG_ID);
    if(isset($sp_config['disabled_epg'])&&(int)$sp_config['disabled_epg']===1){
        i_echo('close epg');
    }else{
        $queue_task_model = new queue_task_model();
        $sql = $queue_task_model->get_epg_sql(ORG_ID, 'video');
        echo $sql;
    
        if($sql!==null){
            $db = nn_get_db(NL_DB_READ);
            $c2_list = nl_db_get_all($sql, $db);
            if(is_array($c2_list)&&count($c2_list)>0){
                if(isset($sp_config['epg_import_model']) && $sp_config['epg_import_model'] == '1')
                {
                    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv_v2/models/import_model_v2.php';
                    $arr_task_id = null;
                    foreach($c2_list as $c2_info)
                    {
                        $arr_task_id[] = $c2_info['nns_id'];
                    }
                    $obj_import_model = new import_model_v2();
                    $re=$obj_import_model->import_epg($arr_task_id);
                }
                else
                {
                    foreach($c2_list as $c2_info)
                    {
                        import_model::import_asset($c2_info['nns_id']);
                    }
                }
            }
        }
        i_echo('end');
    }
}



