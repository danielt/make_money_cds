<?php

class np_http_curl_class {

	public $callback = false;
	public $ch;
	public $curl_getinfo;
	public $curl_error;
    public function __construct(){
    	$this->ch = curl_init ();
    }
	public function set_callback($func_name) {
		$this->callback = $func_name;
	}

	public function do_request($method, $url, $vars=null) {
		//$ch = curl_init ();
		curl_setopt ( $this->ch, CURLOPT_URL, $url );
		curl_setopt ( $this->ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ( $this->ch, CURLOPT_HEADER, 0 );
		//curl_setopt ( $this->ch, CURLOPT_FOLLOWLOCATION, 1 );
		
		if ($method == 'post') {
			curl_setopt ( $this->ch, CURLOPT_POST, 1 );
			curl_setopt ( $this->ch, CURLOPT_POSTFIELDS, $vars );
		}
		$data = curl_exec ( $this->ch );
		if(false !== $data){
		    $this->curl_getinfo = curl_getinfo($this->ch);
		}else{
			$this->curl_error = curl_error($this->ch);
		}

		curl_close ( $this->ch );

		if ($this->callback) {
			$callback = $this->callback;
			$this->callback = false;
			return call_user_func ( $callback, $data );
		} else {
			return $data;
		}

	}
	public function curl_getinfo(){
		return $this->curl_getinfo;
	}
	public function curl_error(){
		return $this->curl_error;
	}

	public function get($url) {
		return $this->do_request ( 'get', $url );
	}

	public function post($url, $vars) {
		return $this->do_request ( 'post', $url, $vars );
	}
}
