<?php

class nl_log {
//	主控制日志输出，竖线分割
	static public $main_rule='INFO|WARNING|ERROR|DEBUG';
//	子控制日志输出，竖线分割
	private $rule='INFO|WARNING|ERROR|DEBUG';
//	日志类型，如：是前端接口api，还是后台接口interface
	private $type;
//	日志模块，如：后台接口标识是user模块 还是product模块，前端API接口标识是哪个第三方接入，如：huawei
	private $model;
//	日志存储地址
	private $path='.';
	
	function nl_log($root_path,$type,$model){
		$this->model = $model;
		$this->type = $type;
		$this->path = rtrim($root_path,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.$this->type.DIRECTORY_SEPARATOR.$this->model;
	}
	
	public function __get($property_name){
		
		if(isset($this->$property_name)){
			return($this->$property_name);
		}else{
			return(NULL);
		}
	}
	
	public function __set($property_name, $value){
		
		$this->$property_name = $value;
	}
	
//	检查是否支持输出该类日志
	private function _check($log_type){
		if (empty(self::$main_rule) || empty($this->rule) || empty($log_type)){
			return FALSE;
		}
		
		if (strpos(self::$main_rule,strtoupper($log_type))===FALSE){
			return FALSE;
		}
		
		if (strpos($this->rule,strtoupper($log_type))===FALSE){
			return FALSE;
		}
		
		return TRUE;
	}
//	拼装日志路径和文件名
	private function _build($log_type){
		if ($this->_check($log_type)===FALSE){
			return FALSE;
		}
		
		$log_path = $this->path.DIRECTORY_SEPARATOR.$log_type.DIRECTORY_SEPARATOR.date('Ymd');
		if(!is_dir($log_path)){
			mkdir($log_path,0777,true);
		}
		
		$log_file = np_get_ip().'.log.txt';
		
		@touch($log_path.DIRECTORY_SEPARATOR.$log_file);
		@chmod($log_path,0777);
		
		return $log_path.DIRECTORY_SEPARATOR.$log_file;
	}
//	写入日志
	public function _write($log_file,$msg){
		$content="[".date("Y-m-d H:i:s")."]\n";
		
		$content.="[REQUEST]\n";
		$content.=$_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING']."\n";
		
		if (!empty($_POST)){
			$content.="[POST]\n";
			$content.=var_export($_POST,true)."\n";
		}

		$content.="[MESSAGE]\n";
		$content.=$msg."\n";
		$content.="--------------------------------------------------------------------------------------------\n\n";

		//创建文件目录
		if (!file_exists($log_file))
		{
			mkdir(pathinfo($log_file, PATHINFO_DIRNAME), 0777, true);
		}

		error_log($content,3,$log_file);
	}
	
	
//	输出信息日志
	public function info($msg){
		$path=$this->_build('info');
		if ($path===FALSE) {
			return;
		}
		$this->_write($path,$msg);
	}
//	输出警告日志
	public function warning($msg){
		$path=$this->_build('warning');
		if ($path===FALSE) {
			return;
		}
		$this->_write($path,$msg);
	}
//	输出调试日志
	public function debug($msg){
		$path=$this->_build('debug');
		if ($path===FALSE) {
			return;
		}
		$this->_write($path,$msg);
	}
	
//	输出错误日志
	public function error($msg){
		$path=$this->_build('error');
		if ($path===FALSE) {
			return;
		}
		$this->_write($path,$msg);
	}
		
}
?>