<?php
/*
 * 
 ***************本文档实现以下方法************
 1、媒资信息注入  ，注入接口包含修改媒资
 2、媒体信息删除
 3、媒资分集注入
 4、媒资分集删除
 5、媒资片源注入
 6、媒资片源删除
 * 
 * 返回状态码
 * 自定义状态码
 * 10000    未定义CURL封装类
 * 10001	必要参数不能为空
 * 10002	CURL调用接口失败
 * 
 * 接口返回状态码
 * 0      	成功
 * 非0 		失败，详情参考媒资注入接口文档
 */
//页面设置
date_default_timezone_set('PRC'); 
define("IMPORT_LOG", TRUE);
ini_set("max_execution_time", '0');

//日志类
include_once 'log.class.php';

class import
{

	private $import_url = null;   				//媒资注入接口地址
	private $log        = null;   				//媒资注入日志
	private $curl       = null;					//CURL 封装类对象
	private $result     = array();    			//接口处理结果,返回状态   
	private $log_file   = null;
	private $sp_id  = null;
	public $cp_id = null; //cp_id
	private $obj_dom = null;

	/**
	 * $import_url 
	 * $import_log
	 * $interface_type
	 * $curl_path                           //NP库 CURL路径
	 * 
	 */
	public function __construct($import_url,$log_path='.',$sp_id='',$cp_id=null)
	{
		$this->import_url = $import_url;
		$this->log_file = $log_path.DIRECTORY_SEPARATOR.date('Y_m_d').'.txt';
		$this->sp_id = $sp_id;
		$this->cp_id = $cp_id;
		//CURL 实例化
		$curl_path = dirname(dirname(dirname(dirname(__FILE__)))).'/np/np_http_curl.class.php';
		//	$curl_path = dirname(__FILE__).'/np_http_curl.class.php';
		if(file_exists($curl_path))
		{
			include_once $curl_path;
			$this->curl = new np_http_curl_class();
		}else{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:i:s')."[未定义CURL封装类]\n";
				$this->log->_write($this->log_file,$err_msg);

			}
			$this->result  = array(
					'code' => 10000,
					'reason' => '未定义CURL封装类',
					"id" => ''
					);
			return $this->return_xml($this->result);
		}
		//LOG 实例化
		$this->log = new nl_log($log_path,'cms_api','import');
		//$this->log_file = $this->log->path.DIRECTORY_SEPARATOR.date('Y_m_d').'.log';
	}

	/**
	 * 发送上下线xml到下游
	 * @param string $assets_xml xml数据
	 * @param string $message_id 消息id
	 * @return Ambigous <string(xml), string>
	 * @author liangpan
	 * @date 2015-11-27
	 */
	public function import_unline($assets_xml,$message_id,$ex_params=null,$asset_unline_model=0)
	{
		$data['cmspost'] = $assets_xml;
		if(is_array($ex_params) && !empty($ex_params))
		{
			foreach ($ex_params as $key=>$val)
			{
				$data[$key]=$val;
			}
			unset($ex_params);
		}
		if($asset_unline_model==0){
			$asset_ret = $this->curl->post($this->import_url, $data);
			nn_log::write_log_message(LOG_MODEL_UNLINE, "原因" . var_export($asset_ret, true), $this->sp_id, VIDEO_TYPE_VIDEO);
		}
		if($asset_unline_model==1)
		{
			$asset_ret = $this->curl->post($this->import_url, $data);
			nn_log::write_log_message(LOG_MODEL_UNLINE, "原因" . var_export($asset_ret, true), $this->sp_id, VIDEO_TYPE_VIDEO);
			$arr = simplexml_load_string($asset_ret);
			$asset_ret = $arr['ret']==0?1:0;
		}
		//判断CURL状态，如果不为200记录错误信息
		$curl_info = $this->curl->curl_getinfo();
		$this->result['data'] 	= $message_id;
		if($curl_info['http_code'] != '200')
		{
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问媒资信息注入失败,HTTP状态码'.$curl_info['http_code'];
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:i:s').'[访问媒资信息注入失败,CURL接口地址]：'.$this->import_url."\n".date('Y-m-d H:i:s').'[HTTP状态码]：'.$curl_info['http_code']."\n".date('Y-m-d H:i:s').' [HTTP错误]：'.$this->curl->curl_error()."\n\n";
				nn_log::write_log_message(LOG_MODEL_UNLINE,"curl返回错误：原因".var_export($err_msg,true),$this->sp_id,VIDEO_TYPE_VIDEO);
			}
			nn_log::write_log_message(LOG_MODEL_UNLINE,"原因".var_export($this->result,true),$this->sp_id,VIDEO_TYPE_VIDEO);
			return $this->return_xml($this->result);
		}
		//解析CURL返回结果
		if($asset_ret == '1')
		{
			$err_msg = date('Y-m-d H:i:s').'[访问媒资注入成功,CURL接口地址]：'.$this->import_url."\n".date('Y-m-d H:i:s').'[接口返回结果]：'.$asset_ret;
			$this->result['reason'] = '访问媒资注入成功';
			$this->result['code'] = 0;
		}
		else
		{
			$err_msg = date('Y-m-d H:i:s').'[访问媒资注入失败,CURL接口地址]：'.$this->import_url."\n".date('Y-m-d H:i:s').'[接口返回结果]：'.$asset_ret;
			$this->result['reason'] = '访问媒资注入失败';
			$this->result['code'] = 1;
		}
		//成功记录接口返回数据
		if(IMPORT_LOG)
		{
			//记录日志
			nn_log::write_log_message(LOG_MODEL_UNLINE,"原因".var_export($this->result,true),$this->sp_id,VIDEO_TYPE_VIDEO);
		}
		$this->result['data'] 	= $message_id;
		return $this->return_xml($this->result);
	}
	
	
	/**
	 * 媒资信息注入
	 * $assets_xml 媒资信息XML结构
	 */
	public function import_assets($assets_xml,$asset_id,$cp_id=null,$message_id='')
	{
		$asset_info = json_decode(json_encode(@simplexml_load_string($assets_xml)),true);
		//检查必要参数
		$check = $this->assets_info_check_params($asset_info);

		if($check === false)
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:i:s').'[媒资注入开始,接口地址]:'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[错误码]：'.$this->result['code']."\n"
					.date('Y-m-d H:i:s')."[错误原因]：".$this->result['reason']."\n"
					.date('Y-m-d H:i:s')."[媒资来源ID]：".$this->result['data']."\n\n";
//				$this->log->_write($this->log_file,$err_msg);
				$this->result['err_msg'] = isset($err_msg)&&strlen($err_msg>0)?$err_msg:NULL;
			}

			return $this->return_xml($this->result);
		}

		//媒资注入接口数据结构
		$data['func'] = 'modify_video';
		$data['assets_content']     = $assets_xml;
		$data['assets_video_info']     = $assets_xml;
		$data['asset_source_id'] =$asset_id;
		$data['assets_id'] =$asset_id;
		$data['bk_message_id'] =$message_id;
		$data['bk_cp_id'] =(strlen($cp_id) < 1) ? 0 : $cp_id;
		
		//for ($i = 0; $i < 1; $i++) {
			//访问媒资注入接口
			$asset_ret = $this->curl->post($this->import_url,$data);
			//判断CURL状态，如果不为200记录错误信息
			$curl_info = $this->curl->curl_getinfo();
//			$http_code = $curl_info['http_code'];
			//if ($http_code != 200) {
			//	continue;
			//} else {
			//	break;
			//}
		//}
		//访问媒资注入接口
		//$asset_ret = $this->curl->post($this->import_url,$data);
		//判断CURL状态，如果不为200记录错误信息
		//$curl_info = $this->curl->curl_getinfo();
		if($curl_info['http_code'] != '200')
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:i:s').'[访问媒资信息注入失败,CURL接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[HTTP状态码]：'.$curl_info['http_code']."\n"
					.date('Y-m-d H:i:s').' [HTTP错误]：'.$this->curl->curl_error()."\n\n";
//				$this->log->_write($this->log_file,$err_msg);
				$this->result['err_msg'] = isset($err_msg)&&strlen($err_msg>0)?$err_msg:NULL;

			}
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问媒资信息注入失败,HTTP状态码'.$curl_info['http_code'];
			$this->result['data'] 	= $asset_info['@attributes']['assets_id'];

			return $this->return_xml($this->result);

		}
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($asset_ret)),true);

		//成功记录接口返回数据
		if(IMPORT_LOG)
		{
			if($result_info['@attributes']['ret'] == '0')
			{
				$err_msg = date('Y-m-d H:i:s').'[访问媒资注入成功,CURL接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[接口返回结果]：'.$asset_ret;
			}else
			{
				$err_msg = date('Y-m-d H:i:s').'[访问媒资注入失败,CURL接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[接口返回结果]：'.$asset_ret;
			}
			//记录日志
			$this->result['err_msg'] = isset($err_msg)&&strlen($err_msg>0)?$err_msg:NULL;
//			$this->log->_write($this->log_file,$err_msg);
		}

		$this->result['code']   = $result_info['@attributes']['ret'];
		$this->result['reason'] = $result_info['@attributes']['reason'];
		$this->result['data']   = $result_info['@attributes']['id'];
		unset($this->curl);
		return $this->return_xml($this->result);

	}


	
	/**
	 * 媒资上下线epg注入
	 * @param unknown $assets_xml
	 * @param unknown $asset_id
	 * @return Ambigous <string(xml), string>
	 * @author liangpan
	 * @date 2016-04-19
	 */
	public function unline_assets($assets_xml,$asset_id)
	{
		$asset_info = json_decode(json_encode(@simplexml_load_string($assets_xml)),true);
		//检查必要参数
		$check = $this->assets_info_check_params($asset_info);
	
		if($check === false)
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:s:i').'[媒资注入开始,接口地址]:'.$this->import_url."\n"
						.date('Y-m-d H:s:i').'[错误码]：'.$this->result['code']."\n"
								.date('Y-m-d H:s:i')."[错误原因]：".$this->result['reason']."\n"
										.date('Y-m-d H:s:i')."[媒资来源ID]：".$this->result['data']."\n\n";
				$this->log->_write($this->log_file,$err_msg);
			}
	
			return $this->return_xml($this->result);
		}
	
		//媒资注入接口数据结构
		$data['func'] = 'unline_assets';
		$data['assets_content']     = $assets_xml;
		$data['assets_video_info']     = $assets_xml;
		$data['asset_source_id'] =$asset_id;
		$data['assets_id'] =$asset_id;
	
		//for ($i = 0; $i < 1; $i++) {
		//访问媒资注入接口
		$asset_ret = $this->curl->post($this->import_url,$data);
		//判断CURL状态，如果不为200记录错误信息
		$curl_info = $this->curl->curl_getinfo();
		$http_code = $curl_info['http_code'];
		//if ($http_code != 200) {
		//	continue;
		//} else {
		//	break;
		//}
		//}
		$this->log->_write($this->log_file,var_export($asset_ret,true));
		//访问媒资注入接口
		//$asset_ret = $this->curl->post($this->import_url,$data);
	
		//判断CURL状态，如果不为200记录错误信息
		//$curl_info = $this->curl->curl_getinfo();
		if($curl_info['http_code'] != '200')
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:s:i').'[访问媒资信息注入失败,CURL接口地址]：'.$this->import_url."\n"
						.date('Y-m-d H:s:i').'[HTTP状态码]：'.$curl_info['http_code']."\n"
								.date('Y-m-d H:s:i').' [HTTP错误]：'.$this->curl->curl_error()."\n\n";
				$this->log->_write($this->log_file,$err_msg);
	
			}
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问媒资信息注入失败,HTTP状态码'.$curl_info['http_code'];
			$this->result['data'] 	= $asset_info['@attributes']['assets_id'];
	
			return $this->return_xml($this->result);
	
		}
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($asset_ret)),true);
	
		//成功记录接口返回数据
		if(IMPORT_LOG)
		{
			if($result_info['@attributes']['ret'] == '0')
			{
				$err_msg = date('Y-m-d H:s:i').'[访问媒资注入成功,CURL接口地址]：'.$this->import_url."\n"
						.date('Y-m-d H:s:i').'[接口返回结果]：'.$asset_ret;
			}else
			{
				$err_msg = date('Y-m-d H:s:i').'[访问媒资注入失败,CURL接口地址]：'.$this->import_url."\n"
						.date('Y-m-d H:s:i').'[接口返回结果]：'.$asset_ret;
			}
			//记录日志
			$this->log->_write($this->log_file,$err_msg);
		}
	
		$this->result['code']   = $result_info['@attributes']['ret'];
		$this->result['reason'] = $result_info['@attributes']['reason'];
		$this->result['data']   = $result_info['@attributes']['id'];
		unset($this->curl);
		return $this->return_xml($this->result);
	
	}
	
	
	
	/**
	 * 删除媒资
	 * $assets_id 媒资来源ID
	 * $video_type 媒资类型  0 点播,1 直播 默认为0
	 */
	public function delete_assets($assets_id,$assets_video_type = 0,$cp_id=null,$message_id='')
	{
		//检查必要参数
		if(empty($assets_id))
		{
			$this->result = array(
					'code' => 10001,
					'reason' => '媒资ID不能为空',
					'data' => ''
					);
			if(IMPORT_LOG)
			{	
				$err_msg = date('Y-m-d H:i:s').'[访问媒资删除接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[错误码]：'.$this->result['code']."\n"
					.date('Y-m-d H:i:s')."[错误原因]：".$this->result['reason']."\n"
					.date('Y-m-d H:i:s')."[媒资来源ID]：".$this->result['id']."\n\n";

				$this->log->_write($this->log_file,$err_msg);
			}

			return $this->return_xml($this->result);
		}

		//媒资删除接口数据结构
		$data['func']                   = 'delete_asset';
		$data['assets_id']              = $assets_id;
		$data['assets_video_type']      = $assets_video_type;
		$data['bk_message_id']      = $message_id;
		$data['bk_cp_id'] =(strlen($cp_id) < 1) ? 0 : $cp_id;
		if($this->cp_id !== null) 
		{
			$data['asset_source'] = $this->cp_id;
		}
		//$asset_ret = $this->curl->post($this->import_url,$data);

		//判断CURL状态，如果不为200记录错误信息
		//$curl_info = $this->curl->curl_getinfo();


		//for ($i = 0; $i < 1; $i++) {
			//访问媒资注入接口
			$asset_ret = $this->curl->post($this->import_url,$data);		
			//判断CURL状态，如果不为200记录错误信息
			$curl_info = $this->curl->curl_getinfo();
			$http_code = $curl_info['http_code'];
			//if ($http_code != 200) {
			//	continue;
			//} else {
			//	break;
			//}
		//}


		if($curl_info['http_code'] != '200')
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:i:s').'[访问媒资删除失败,CURL接口地址]：'.$curl_info['url']."\n"
                    . '要删除的媒资ID为：' . $assets_id . '，媒资类型为:' . $assets_video_type . "\n"
					.date('Y-m-d H:i:s').'[HTTP状态码]：'.$curl_info['http_code']."\n"
					.date('Y-m-d H:i:s').'[HTTP错误]：'.$this->curl->curl_error()."\n\n";

				$this->log->_write($this->log_file,$err_msg);

			}
			$this->result['code'] = 10002;
			$this->result['reason'] = '访问媒资删除失败,HTTP状态码'.$curl_info['http_code'];
			$this->result['data'] = $assets_id;

			return $this->return_xml($this->result);

		}
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($asset_ret)),true);

		//记录日志
		if(IMPORT_LOG)
		{
			//成功
			if($result_info['@attributes']['ret'] == '0')
			{
				$err_msg = date('Y-m-d H:i:s').'[访问媒资删除成功,CURL接口地址]：'.$curl_info['url']."\n"
                    . '删除的媒资ID为：' . $assets_id . '，媒资类型为:' . $assets_video_type . "\n"
					.date('Y-m-d H:i:s').'[接口返回结果]：'.$asset_ret."\n";
				//失败
			}else{
				$err_msg = date('Y-m-d H:i:s').'[访问媒资删除失败,CURL接口地址]：'.$curl_info['url']."\n"
                    . '需要删除的媒资ID为：' . $assets_id . '，媒资类型为:' . $assets_video_type . "\n"
					.date('Y-m-d H:i:s').'[接口返回结果]：'.$asset_ret."\n";
			}
			$this->log->_write($this->log_file,$err_msg);

		}
		$this->result['code']   = $result_info['@attributes']['ret'];
		$this->result['reason'] = $result_info['@attributes']['reason'];
		$this->result['data']   = $result_info['@attributes']['id'];

		return $this->return_xml($this->result);
	}

	/**
	 * 分集信息注入
	 * $assets_id         媒资来源ID 
	 * $assets_clip_id    分集来源ID
	 * $assets_clip_info  分集信息
	 * $assets_video_type   媒资类型  0 点播,1 直播 默认为0
	 * 
	 */
	public function import_video_clip($assets_id,$assets_clip_id,$assets_clip_info,$assets_video_type = 0,$assets_seekpoint_info=null,$cp_id,$message_id='')
	{
		//验证参数
		if(empty($assets_clip_id) || empty($assets_clip_info) )
		{
			$this->result['code']   = 10001;
			$this->result['reason'] = '媒资ID或者媒资分片ID或者分片信息不能为空';
			$this->result['data']   = $assets_clip_id;

			//记录日志
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:i:s').'[访问媒资分片注入接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[错误码]：'.$this->result['code']."\n"
					.date('Y-m-d H:i:s')."[错误原因]：".$this->result['reason']."\n"
					.date('Y-m-d H:i:s')."[媒资来源ID]：".$assets_id."\n"
					.date('Y-m-d H:i:s')."[分片来源ID]：".$assets_clip_id."\n\n";

//				$this->log->_write($this->log_file,$err_msg);
				$this->result['err_msg'] = isset($err_msg)&&strlen($err_msg>0)?$err_msg:NULL;
				return $this->return_xml($this->result);
			}
		}
		//解析XML数据，
		$clip_info = json_decode(json_encode(@simplexml_load_string($assets_clip_info)),true);
		$check = $this->clip_info_check_params($clip_info);
		if($check === false)
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:i:s').'[媒资分片注入开始,接口地址]:'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[错误码]：'.$this->result['code']."\n"
					.date('Y-m-d H:i:s')."[错误原因]：".$this->result['reason']."\n"
					.date('Y-m-d H:i:s')."[媒资来源ID]：".$assets_id."\n"
					.date('Y-m-d H:i:s')."[分片来源ID]：".$this->result['data']."\n\n";
//				$this->log->_write($this->log_file,$err_msg);
				$this->result['err_msg'] = isset($err_msg)&&strlen($err_msg>0)?$err_msg:NULL;
			}

			return $this->return_xml($this->result);
		}
		//分片注入数据结构
		$data['func'] 				= 'import_video_clip';
	    $data['assets_id'] = $assets_id;
		$data['assets_video_type']  = $assets_video_type;
		$data['assets_clip_id']    = $assets_clip_id;
		$data['assets_clip_info']   = $assets_clip_info;
		$data['bk_message_id'] =$message_id;
		$data['bk_cp_id']   = (strlen($cp_id) <1) ? 0 : $cp_id;
		//梁攀   这儿加的是分集信息的打点信息 加入到cms 媒资库
		if(!empty($assets_seekpoint_info))
		{
			$data['seekpoint'] = $assets_seekpoint_info;
		}
		//$clip_ret = $this->curl->post($this->import_url,$data);

		//判断CURL状态，如果不为200记录错误信息
		//$curl_info = $this->curl->curl_getinfo();
		//for ($i = 0; $i < 1; $i++) {
			//访问媒资注入接口
			$clip_ret = $this->curl->post($this->import_url,$data);
			//判断CURL状态，如果不为200记录错误信息
			$curl_info = $this->curl->curl_getinfo();
//			$http_code = $curl_info['http_code'];
			//if ($http_code != 200) {
			//	continue;
			//} else {
			//	break;
			//}
		//}
		
//		$this->log->_write($this->log_file,var_export($clip_ret,true));

		if($curl_info['http_code'] != '200')
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:i:s').'[访问分片注入失败,CURL接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[HTTP状态码]：'.$curl_info['http_code']."\n"
					.date('Y-m-d H:i:s').'[HTTP错误]：'.$this->curl->curl_error()."\n\n";
				$this->result['err_msg'] = isset($err_msg)&&strlen($err_msg>0)?$err_msg:NULL;

//				$this->log->_write($this->log_file,$err_msg);

			}
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问分片注入失败,HTTP状态码'.$curl_info['http_code'];
			$this->result['data'] 	= $assets_clip_id;

			return $this->return_xml($this->result);

		}

		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($clip_ret)),true);

		//记录日志
		if(IMPORT_LOG)
		{
			//成功
			if($result_info['@attributes']['ret'] == '0')
			{
				$err_msg = date('Y-m-d H:i:s').'[访问分片注入成功,CURL接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[接口返回结果]：'.$clip_ret."\n";
				//失败
			}else{
				$err_msg = date('Y-m-d H:i:s').'[访问分片注入失败,CURL接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[接口返回结果]：'.$clip_ret."\n";
			}
			$this->result['err_msg'] = isset($err_msg)&&strlen($err_msg>0)?$err_msg:NULL;
//			$this->log->_write($this->log_file,$err_msg);

		}
		$this->result['code']   = $result_info['@attributes']['ret'];
		$this->result['reason'] = $result_info['@attributes']['reason'];
		$this->result['data']   = $result_info['@attributes']['id'];

		return $this->return_xml($this->result);
	}

	
	/**
	 * 分集打点信息注入
	 * $assets_id         媒资来源ID
	 * $assets_clip_id    分集来源ID
	 * $assets_clip_info  分集信息
	 * $assets_video_type   媒资类型  0 点播,1 直播 默认为0
	 *
	 */
	public function import_video_clip_seekpoint($index_id, $seekpoint_xml)
	{
		//验证参数
		if(empty($index_id) || empty($seekpoint_xml) )
		{
			$this->result['code']   = 10001;
			$this->result['reason'] = '分集ID或者打点信息不能为空';
			$this->result['data']   = $index_id;
	
			//记录日志
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:i:s').'[访问媒资分片打点信息注入接口地址]：'.$this->import_url."\n"
						.date('Y-m-d H:i:s').'[错误码]：'.$this->result['code']."\n"
								.date('Y-m-d H:i:s')."[错误原因]：".$this->result['reason']."\n"
										.date('Y-m-d H:i:s')."[分片来源ID]：".$index_id."\n\n";
	
				$this->log->_write($this->log_file,$err_msg);
				return $this->return_xml($this->result);
			}
		}
		$data =array();
		//分片注入数据结构
		$data['func'] 					 = 'import_seekpoint_data';
		$data['assets_clip_id'] 		 = $index_id;
		$data['assets_seekpoint_info']   = $seekpoint_xml;
		//访问媒资注入接口
		//header("Content-type:text/html;charset=utf-8");
		//var_dump($data);die;
		@sleep(1);
		$clip_ret = $this->curl->post($this->import_url,$data);
		//判断CURL状态，如果不为200记录错误信息
		$curl_info = $this->curl->curl_getinfo();
		$http_code = $curl_info['http_code'];
		$this->log->_write($this->log_file,var_export($clip_ret,true));
	
		if($curl_info['http_code'] != '200')
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:i:s').'[访问分片注入打点信息失败,CURL接口地址]：'.$this->import_url."\n"
						.date('Y-m-d H:i:s').'[HTTP状态码]：'.$curl_info['http_code']."\n"
								.date('Y-m-d H:i:s').'[HTTP错误]：'.$this->curl->curl_error()."\n\n";
	
				$this->log->_write($this->log_file,$err_msg);
	
			}
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问分片注入打点信息失败,HTTP状态码'.$curl_info['http_code'];
			$this->result['data'] 	= $index_id;
	
			return $this->return_xml($this->result);
	
		}
	
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($clip_ret)),true);
	
		//记录日志
		if(IMPORT_LOG)
		{
			//成功
			if($result_info['@attributes']['ret'] == '0')
			{
				$err_msg = date('Y-m-d H:i:s').'[访问分片打点信息注入成功,CURL接口地址]：'.$this->import_url."\n"
						.date('Y-m-d H:i:s').'[接口返回结果]：'.$clip_ret."\n";
				//失败
			}else{
				$err_msg = date('Y-m-d H:i:s').'[访问分片打点信息注入失败,CURL接口地址]：'.$this->import_url."\n"
						.date('Y-m-d H:i:s').'[接口返回结果]：'.$clip_ret."\n";
			}
			$this->log->_write($this->log_file,$err_msg);
	
		}
		$this->result['code']   = $result_info['@attributes']['ret'];
		$this->result['reason'] = $result_info['@attributes']['reason'];
		$this->result['data']   = $result_info['@attributes']['id'];
	
		return $this->return_xml($this->result);
	}
	
	
	/**
	 *   分集信息删除
	 * $assets_clip_id      分集来源ID
	 * $assets_video_type   媒资类型  0 点播,1 直播 默认为0
	 * 
	 */
	public function delete_video_clip($assets_clip_id,$assets_video_type = 0,$import_source=null,$cp_id=null,$message_id='',$assets_id_type=0)
	{
		if(empty($assets_clip_id))
		{
			$this->result['code'] = 10001;
			$this->result['reason']  = '分片来源ID不能为空';
			$this->result['data'] = '';
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:i:s').'[访问删除分片失败,CURL接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[错误码]：'.$this->result['code'."\n"]
					.date('Y-m-d H:i:s').'[错误原因]：'.$this->result['reason']."\n";
				$this->log->_write($this->log_file,$err_msg);
			}

			return $this->return_xml($this->result);
		}
		//分片删除数据结构
		$data['func'] 				= 'delete_video_clip';
		$data['assets_video_type'] 	= $assets_video_type;
		$data['assets_clip_id'] 	= $assets_clip_id;
		$data['bk_cp_id'] 	= (strlen($cp_id) < 1) ? 0 : $cp_id;
		$data['bk_message_id'] =$message_id;
		$data['assets_id_type'] = $assets_id_type;
		if($this->cp_id !== null) 
		{
			$data['asset_source'] = $this->cp_id;
		}
		if(!empty($import_source))
		{
			$data['import_source'] = $import_source;
		}
		//$clip_ret = $this->curl->post($this->import_url,$data);
		//获取CURL信息,如果不为200记录错误信息
		//$curl_info = $this->curl->curl_getinfo();

		//for ($i = 0; $i < 1; $i++) {
			//访问媒资注入接口
			$clip_ret = $this->curl->post($this->import_url,$data);		
			//判断CURL状态，如果不为200记录错误信息
			$curl_info = $this->curl->curl_getinfo();
			$http_code = $curl_info['http_code'];
			//if ($http_code != 200) {
			//	continue;
			//} else {
			//	break;
			//}
		//}


		if($curl_info['http_code'] != '200')
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:i:s').'[访问分片删除失败,CURL接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[HTTP状态码]：'.$curl_info['http_code']."\n"
					.date('Y-m-d H:i:s').'[HTTP错误]：'.$this->curl->curl_error()."\n\n";

				$this->log->_write($this->log_file,$err_msg);

			}
			$this->result['code'] = 10002;
			$this->result['reason'] = '访问分片删除失败,HTTP状态码：'.$curl_info['http_code'];
			$this->result['data'] = $assets_clip_id;

			return $this->return_xml($this->result);

		}
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($clip_ret)),true);

		//记录日志
		if(IMPORT_LOG)
		{
			//成功
			if($result_info['@attributes']['ret'] == '0')
			{
				$err_msg = date('Y-m-d H:i:s').'[访问分片删除成功,CURL接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[接口返回结果]：'.$clip_ret."\n";
				//失败
			}else{
				$err_msg = date('Y-m-d H:i:s').'[访问分片注入失败,CURL接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[接口返回结果]：'.$clip_ret."\n";
			}
			$this->log->_write($this->log_file,$err_msg);

		}
		$this->result['code']   = $result_info['@attributes']['ret'];
		$this->result['reason'] = $result_info['@attributes']['reason'];
		$this->result['data']   = $result_info['@attributes']['id'];

		return $this->return_xml($this->result);
	}

	/**
	 * 片源信息注入
	 * $assets_id    			媒资来源ID
	 * $assets_clip_id 			分集来源ID
	 * $assets_file_id 			片源来源ID
	 * $assets_file_info 		片源信息
	 * $assets_video_type 		媒资类型   0 点播,1 直播 默认为0
	 * 
	 */
	public function import_video_file($assets_id,$assets_clip_id,$assets_file_id,$assets_file_info,$assets_video_type = 0,$policy="",$assets_core_org_id=0,$cp_id=null,$message_id='')
	{
		if(empty($assets_clip_id) || empty($assets_file_id) || empty($assets_file_info))
		{
			$this->result['code'] = 10001;
			$this->result['reason']  = '媒资来源ID或者分片来源ID或者片源来源ID或者片源信息为空';
			$this->result['data'] = '$assets_file_id';
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:i:s').'[访问删除分片失败,CURL接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[错误码]：'.$this->result['code'."\n"]
					.date('Y-m-d H:i:s').'[错误原因]：'.$this->result['reason']."\n";
				$this->result['err_msg'] = isset($err_msg)&&strlen($err_msg>0)?$err_msg:NULL;
//				$this->log->_write($this->log_file,$err_msg);
			}

			return $this->return_xml($this->result);
		}
		$file_info = json_decode(json_encode(@simplexml_load_string($assets_file_info)),true);

		$check = $this->file_info_check_params($file_info);
		if($check === false)
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:i:s').'[媒资片源注入开始,接口地址]:'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[错误码]：'.$this->result['code']."\n"
					.date('Y-m-d H:i:s')."[错误原因]：".$this->result['reason']."\n"
					.date('Y-m-d H:i:s')."[媒资来源ID]：".$assets_id."\n"
					.date('Y-m-d H:i:s')."[分片来源ID]：".$assets_clip_id."\n"
					.date('Y-m-d H:i:s')."[片源来源ID]：".$this->result['data']."\n\n";
				$this->result['err_msg'] = isset($err_msg)&&strlen($err_msg>0)?$err_msg:NULL;

//				$this->log->_write($this->log_file,$err_msg);
			}

			return $this->return_xml($this->result);
		}

		//片源注入数据结构
		$data['func'] 				= 'import_video_file';
		$data['assets_video_type'] 	= $assets_video_type;
		$data['assets_id'] 			= $assets_id;
		$data['assets_clip_id'] 	= $assets_clip_id;
		$data['assets_file_id'] 	= $assets_file_id;
		$data['assets_file_info'] 	= $assets_file_info;
		$data['bk_message_id'] =$message_id;
		$data['bk_cp_id'] 	= (strlen($cp_id) <1) ? 0 : $cp_id;
		$data['policy'] 	= $policy;
		$data['assets_core_org_id'] 	= $assets_core_org_id;
//		$this->log->_write($this->log_file,var_export($data,true));

		//$file_ret = $this->curl->post($this->import_url,$data);
		//获取CURL信息,如果不为200记录错误信息
		//$curl_info = $this->curl->curl_getinfo();


		//for ($i = 0; $i < 1; $i++) {
			//访问媒资注入接口
			$file_ret = $this->curl->post($this->import_url,$data);	
			//判断CURL状态，如果不为200记录错误信息
			$curl_info = $this->curl->curl_getinfo();
			$http_code = $curl_info['http_code'];
			//if ($http_code != 200) {
			//	continue;
			//} else {
			//	break;
			//}
		//}
//		$this->log->_write($this->log_file,var_export($file_ret,true));
		if($curl_info['http_code'] != '200')
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:i:s').'[访问片源注入失败,CURL接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[HTTP状态码]：'.$curl_info['http_code']."\n"
					.date('Y-m-d H:i:s').'[HTTP错误]：'.$this->curl->curl_error()."\n\n";
				$this->result['err_msg'] = isset($err_msg)&&strlen($err_msg>0)?$err_msg:NULL;

//				$this->log->_write($this->log_file,$err_msg);

			}
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问片源注入失败,HTTP状态码：'.$curl_info['http_code'];
			$this->result['data'] 	= $assets_clip_id;

			return $this->return_xml($this->result);

		}
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($file_ret)),true);

		//记录日志
		if(IMPORT_LOG)
		{
			//成功
			if($result_info['@attributes']['ret'] == '0')
			{
				$err_msg = date('Y-m-d H:i:s').'[访问片源注入成功,CURL接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[接口返回结果]：'.$file_ret."\n";
				//失败
			}else{
				$err_msg = date('Y-m-d H:i:s').'[访问片源注入失败,CURL接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[接口返回结果]：'.$file_ret."\n";
			}
//			$this->log->_write($this->log_file,$err_msg);
			$this->result['err_msg'] = isset($err_msg)&&strlen($err_msg>0)?$err_msg:NULL;


		}
		$this->result['code']   = $result_info['@attributes']['ret'];
		$this->result['reason'] = $result_info['@attributes']['reason'];
		$this->result['data']   = $result_info['@attributes']['id'];

		return $this->return_xml($this->result);

	}
	
	
	/**
	 * 直播信息注入MSP
	 * $xml    			媒资来源ID
	 * $overwrite 			分集来源ID
	 */
	public function import_live_info($xml,$content_id,$overwrite=1)
	{
		$data = $xml;
		//访问媒资注入接口
		$file_ret = $this->curl->post($this->import_url."?func=add_live_content_by_template&overwrite={$overwrite}",$data);
		//判断CURL状态，如果不为200记录错误信息
		$curl_info = $this->curl->curl_getinfo();
		$http_code = $curl_info['http_code'];
		if($curl_info['http_code'] != '200')
		{
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问片源注入失败,HTTP状态码：'.$curl_info['http_code'];
			$this->result['data'] 	= $data;
			$this->result['err_msg'] = '[访问片源注入失败,CURL接口地址]：'.$this->import_url.'[HTTP状态码]：'.$curl_info['http_code'].'[HTTP错误]：'.$this->curl->curl_error().'；参数：'.var_export($data,true);
			return $this->return_xml($this->result);
		}
		//解析CURL返回结果
		
		$result_info = json_decode(json_encode(@simplexml_load_string($file_ret)),true);
		//成功
		if($result_info['result']['@attributes']['ret'] == '0')
		{
			$err_msg = '[访问片源注入成功,CURL接口地址]：'.$this->import_url.'[接口返回结果]：'.$file_ret.'；参数：'.var_export($data,true);
			//失败
		}
		else
		{
			$err_msg = '[访问片源注入失败,CURL接口地址]：'.$this->import_url.'[接口返回结果]：'.$file_ret.'；参数：'.var_export($data,true);
		}
		$this->result['err_msg'] = $err_msg;
		$this->result['code']   = $result_info['result']['@attributes']['ret'];
		$this->result['reason'] = $result_info['result']['@attributes']['reason'];
		$this->result['data']   = isset($result_info['result']['@attributes']['id']) ? $result_info['result']['@attributes']['id'] : $content_id;
		$this->result['message'] = $file_ret;
		$this->result['send_message'] = $xml;
		return $this->return_xml($this->result);
	}
	
	/**
	 * 直播信息注入MSP
	 * $xml    			媒资来源ID
	 * $overwrite 		分集来源ID
	 */
	public function import_msp_video_media($arr_video_media,$content_id,$overwrite=1)
	{
		$data = array(
				'func'=>'add_content_by_cdn_policy',
				'id'=>$arr_video_media['nns_id'],
				'name'=>urlencode($arr_video_media['nns_name']),
				'url'=>urlencode($arr_video_media['nns_url']),
				'live'=>'0',
				'filetype'=>$arr_video_media['nns_filetype'],
				'impl'=>'',
				'cdn_policy_id'=>$arr_video_media['cdn_policy_id'],
				'cp'=>$arr_video_media['nns_cp_id'],
				'from'=>'',
				'cdnv2_policy_id'=>'',
				'cdn_ext_source_id'=>'',
				'cdnv2_predistribute_id'=>'',
				'bitrate'=>$arr_video_media['nns_cp_id'],
				'asset_id'=>$arr_video_media['nns_id'],
				'transfer_rate'=>'0',
				'ingress_capacity'=>'0',
		);
		//访问媒资注入接口
		$file_ret = $this->curl->post($this->import_url."?func=add_live_content_by_template&overwrite={$overwrite}",$data);
		//判断CURL状态，如果不为200记录错误信息
		$curl_info = $this->curl->curl_getinfo();
		$http_code = $curl_info['http_code'];
		if($curl_info['http_code'] != '200')
		{
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问片源注入失败,HTTP状态码：'.$curl_info['http_code'];
			$this->result['data'] 	= $data;
			$this->result['err_msg'] = '[访问片源注入失败,CURL接口地址]：'.$this->import_url.'[HTTP状态码]：'.$curl_info['http_code'].'[HTTP错误]：'.$this->curl->curl_error().'；参数：'.var_export($data,true);
			return $this->return_xml($this->result);
		}
		//解析CURL返回结果
	
		$result_info = json_decode(json_encode(@simplexml_load_string($file_ret)),true);
		// 		var_dump($result_info);die;
		//成功
		if($result_info['result']['@attributes']['ret'] == '0')
		{
			$err_msg = '[访问片源注入成功,CURL接口地址]：'.$this->import_url.'[接口返回结果]：'.$file_ret.'；参数：'.var_export($data,true);
			//失败
		}
		else
		{
			$err_msg = '[访问片源注入失败,CURL接口地址]：'.$this->import_url.'[接口返回结果]：'.$file_ret.'；参数：'.var_export($data,true);
		}
		$this->result['err_msg'] = $err_msg;
		$this->result['code']   = $result_info['result']['@attributes']['ret'];
		$this->result['reason'] = $result_info['result']['@attributes']['reason'];
		$this->result['data']   = isset($result_info['result']['@attributes']['id']) ? $result_info['result']['@attributes']['id'] : $content_id;
		return $this->return_xml($this->result);
	}
	
	
	/**
	 * 直播信息注入MSP
	 * $xml    			媒资来源ID
	 * $overwrite 			分集来源ID
	 */
	public function import_cms_live_info($xml)
	{
		$data = array(
				'func'=>'modify_live',
				'assets_content'=>$xml,
		);
		//访问媒资注入接口
		$file_ret = $this->curl->post($this->import_url,$data);
		//判断CURL状态，如果不为200记录错误信息
		$curl_info = $this->curl->curl_getinfo();
		$http_code = $curl_info['http_code'];
		if($curl_info['http_code'] != '200')
		{
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问片源注入失败,HTTP状态码：'.$curl_info['http_code'];
			$this->result['data'] 	= $data;
			$this->result['err_msg'] = '[访问片源注入失败,CURL接口地址]：'.$this->import_url.'[HTTP状态码]：'.$curl_info['http_code'].'[HTTP错误]：'.$this->curl->curl_error().'；参数：'.var_export($data,true);
			return $this->return_xml($this->result);
		}
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($file_ret)),true);
		
// 				var_dump($result_info);die;
		//成功
		if($result_info['@attributes']['ret'] == '0')
		{
			$err_msg = '[访问片源注入成功,CURL接口地址]：'.$this->import_url.'[接口返回结果]：'.$file_ret.'；参数：'.var_export($data,true);
			//失败
		}
		else
		{
			$err_msg = '[访问片源注入失败,CURL接口地址]：'.$this->import_url.'[接口返回结果]：'.$file_ret.'；参数：'.var_export($data,true);
		}
		$this->result['err_msg'] = $err_msg;
		$this->result['code']   = $result_info['@attributes']['ret'];
		$this->result['reason'] = $result_info['@attributes']['reason'];
		$this->result['data']   = isset($result_info['@attributes']['id']) ? $result_info['@attributes']['id'] : '';
		return $this->return_xml($this->result);
	}
	
	/**
	 * 节目单信息注入MSP
	 * $xml    			媒资来源ID
	 * $overwrite 			分集来源ID
	 */
	public function import_cms_playbill_info($xml)
	{
		$data = array(
				'func'=>'modify_playbill',
				'assets_content'=>$xml,
		);
		//访问媒资注入接口
		$file_ret = $this->curl->post($this->import_url,$data);
		//判断CURL状态，如果不为200记录错误信息
		$curl_info = $this->curl->curl_getinfo();
		$http_code = $curl_info['http_code'];
		if($curl_info['http_code'] != '200')
		{
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问片源注入失败,HTTP状态码：'.$curl_info['http_code'];
			$this->result['data'] 	= $data;
			$this->result['err_msg'] = '[访问片源注入失败,CURL接口地址]：'.$this->import_url.'[HTTP状态码]：'.$curl_info['http_code'].'[HTTP错误]：'.$this->curl->curl_error().'；参数：'.var_export($data,true);
			return $this->return_xml($this->result);
		}
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($file_ret)),true);
// 				var_dump($result_info);die;
		//成功
		if($result_info['@attributes']['ret'] == '0')
		{
			$err_msg = '[访问片源注入成功,CURL接口地址]：'.$this->import_url.'[接口返回结果]：'.$file_ret.'；参数：'.var_export($data,true);
			//失败
		}
		else
		{
			$err_msg = '[访问片源注入失败,CURL接口地址]：'.$this->import_url.'[接口返回结果]：'.$file_ret.'；参数：'.var_export($data,true);
		}
		$this->result['err_msg'] = $err_msg;
		$this->result['code']   = $result_info['@attributes']['ret'];
		$this->result['reason'] = $result_info['@attributes']['reason'];
		$this->result['data']   = isset($result_info['@attributes']['id']) ? $result_info['@attributes']['id'] : '';
		return $this->return_xml($this->result);
	}
	
	
	public function import_cms_playbill_info_batch($xml)
	{
	    $data = array(
	        'func'=>'import_batch_playbill',
	        'playbill_info'=>$xml,
	    );
	    //访问媒资注入接口
	    $file_ret = $this->curl->post($this->import_url,$data);
	    //判断CURL状态，如果不为200记录错误信息
	    $curl_info = $this->curl->curl_getinfo();
	    $http_code = $curl_info['http_code'];
	    if($curl_info['http_code'] != '200')
	    {
	        $this->result['code'] 	= 10002;
	        $this->result['reason'] = '访问片源注入失败,HTTP状态码：'.$curl_info['http_code'];
	        $this->result['data'] 	= $data;
	        $this->result['err_msg'] = '[访问片源注入失败,CURL接口地址]：'.$this->import_url.'[HTTP状态码]：'.$curl_info['http_code'].'[HTTP错误]：'.$this->curl->curl_error().'；参数：'.var_export($data,true);
	        return $this->return_xml($this->result);
	    }
	    include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/np/np_xml2array.class.php';
	    $result = np_xml2array::getArrayData($file_ret);
    	$data_ret = null;
	    if(isset($result['data']['children'][0]['children']) && is_array($result['data']['children'][0]['children']))
        {
             foreach ($result['data']['children'][0]['children'] as $value)
             {
                 if(!is_array($value['children']) || empty($value['children']))
                 {
                     continue;
                 }
                 $temp = null;
                 if(!isset($value['attributes']['ContentID']) || strlen($value['attributes']['ContentID']) <1)
                 {
                     continue;
                 }
                 $temp['ContentID'] = $value['attributes']['ContentID'];
                 foreach ($value['children'] as $_v)
                 {
                     $temp[$_v['attributes']['Name']] = $_v['content'];
                 }
                 $data_ret[] = $temp;
             }
        }
	    //解析CURL返回结果
	    $result_info = json_decode(json_encode(@simplexml_load_string($file_ret)),true);
	    // 				var_dump($result_info);die;
	    //成功
	    
        $err_msg = '[访问片源注入成功,CURL接口地址]：'.$this->import_url.'[接口返回结果]：'.$file_ret.'；参数：'.var_export($data,true);
	        //失败
	    $this->result['err_msg'] = $err_msg;
	    $this->result['code']   = 0;
	    $this->result['reason'] = 'ok';
	    $this->result['data']   = $data_ret;
	    return $this->return_xml($this->result);
	}
	
	/**
	 * 删除直播数据
	 * @param unknown $content_id
	 * @param unknown $user
	 * @param string $cp_id 目前暂时不要设置为NUll
	 * @param number $live
	 * @param number $is_del_file
	 * @return Ambigous <string(xml), string>
	 */
	public function delete_live_info($content_id,$user,$cp_id='',$live=1,$is_del_file=0,$arr_producer=null)
	{
		$data = array();
		//片源注入数据结构
		$data['func'] 				= 'delete_content';
		$data['id'] 	= $content_id;
		$data['user'] 			= $user;
		$data['live'] 			= $live;
		$data['is_del_file'] 			= $is_del_file;
		if(isset($arr_producer['nns_import_source']))
		{
		    $data['cp_id'] = $arr_producer['nns_import_source'];
		    $data['cms_id'] = $cp_id;
		    $data['domain'] = $arr_producer['nns_domain'];
		    $data['nmds_id'] = $arr_producer['nns_node_id'];
		    $data['import_from'] = $cp_id;
		}
		//访问媒资注入接口
		$file_ret = $this->curl->post($this->import_url,$data);
		//判断CURL状态，如果不为200记录错误信息
		$curl_info = $this->curl->curl_getinfo();
		$http_code = $curl_info['http_code'];
		if($curl_info['http_code'] != '200')
		{
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问片源注入失败,HTTP状态码：'.$curl_info['http_code'];
			$this->result['data'] 	= $data;
			$this->result['err_msg'] = '[访问片源注入失败,CURL接口地址]：'.$this->import_url.'[HTTP状态码]：'.$curl_info['http_code'].'[HTTP错误]：'.$this->curl->curl_error().'；参数：'.var_export($data,true);
			return $this->return_xml($this->result);
		}
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($file_ret)),true);
		//成功
		if($result_info['result']['@attributes']['ret'] == '0')
		{
			$err_msg = '[访问片源删除成功,CURL接口地址]：'.$this->import_url.'[接口返回结果]：'.$file_ret.'；参数：'.var_export($data,true);
		}
		else
		{
			$err_msg = '[访问片源删除失败,CURL接口地址]：'.$this->import_url.'[接口返回结果]：'.$file_ret.'；参数：'.var_export($data,true);
		}
		
		
		
		$this->obj_dom = new DOMDocument("1.0", 'utf-8');
		$data['core_url'] = $this->import_url;
		$this->result['err_msg'] = $err_msg;
		$this->result['code']   = $result_info['result']['@attributes']['ret'];
		$this->result['reason'] = $result_info['result']['@attributes']['reason'];
		$this->result['data']   = isset($result_info['result']['@attributes']['id']) ? $result_info['result']['@attributes']['id'] : $content_id;

		$this->result['message'] = $file_ret;
		$this->result['send_message'] = $this->_make_array_xml($data);
		return $this->return_xml($this->result);
	}

	
	/**
	 * 节目单注入
	 * @param unknown $xml
	 * @param number $overwrite
	 * @return Ambigous <string(xml), string>
	 */
	public function import_playbill_info($live_import_id,$media_import_id,$arr_playbill,$action,$arr_producer=null,$arr_message_request_data=null)
	{
		$data = array();
		//片源注入数据结构
		$data['func'] 				= 'add_schedule_record';
		$data['action'] 	= ($action == 'modify') ? 'update' : 'regist';
		$data['physical_content_id'] 			= $arr_playbill['nns_playbill_import_id'];
		$data['schedule_id'] 			= $arr_playbill['nns_playbill_import_id'];
		$data['schedule_name'] 			= $arr_playbill['nns_name'];
		$data['physical_channel_id'] 			= $media_import_id;
		$data['start_date'] 			= date("YmdHis",strtotime($arr_playbill['nns_begin_time']));
		$data['duration'] 			= $arr_playbill['nns_time_len'];
		$data['cp_content_id'] 			= $arr_playbill['nns_cp_id'];
		$data['description'] 			= $arr_playbill['nns_summary'];
		$data['domain'] 			= $arr_playbill['nns_domain'];
		$data['hot_dgree'] 			= $arr_playbill['nns_hot_dgree'];
		if(isset($arr_producer['nns_cp_id']))
		{
		    $data['cp_id'] = $arr_playbill['nns_import_source'];
		    $data['cms_id'] = $arr_playbill['nns_cp_id'];
		    $data['nmds_id'] = $arr_playbill['nns_node_id'];
		    $data['dst_pop'] = $arr_playbill['nns_list_ids'];
		}
		if(isset($arr_message_request_data) && is_array($arr_message_request_data) && !empty($arr_message_request_data))
		{
		    foreach ($arr_message_request_data as $key=>$val)
		    {
		        $data[$key] = $val;
		    }
		}
		//访问媒资注入接口
		$file_ret = $this->curl->post($this->import_url,$data);
		//判断CURL状态，如果不为200记录错误信息
		$curl_info = $this->curl->curl_getinfo();
		$http_code = $curl_info['http_code'];
		if($curl_info['http_code'] != '200')
		{
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问片源注入失败,HTTP状态码：'.$curl_info['http_code'];
			$this->result['data'] 	= $data;
			$this->result['err_msg'] = '[访问片源注入失败,CURL接口地址]：'.$this->import_url.'[HTTP状态码]：'.$curl_info['http_code'].'[HTTP错误]：'.$this->curl->curl_error().'；参数：'.var_export($data,true);
			return $this->return_xml($this->result);
		}
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($file_ret)),true);
		//成功
		if($result_info['result']['@attributes']['ret'] == '0')
		{
			$err_msg = '[访问片源注入成功,CURL接口地址]：'.$this->import_url.'[接口返回结果]：'.$file_ret.'；参数：'.var_export($data,true);
			//失败
		}
		else
		{
			$err_msg = '[访问片源注入失败,CURL接口地址]：'.$this->import_url.'[接口返回结果]：'.$file_ret.'；参数：'.var_export($data,true);
		}
		$this->obj_dom = new DOMDocument("1.0", 'utf-8');
		$data['core_url'] = $this->import_url;
		$this->result['err_msg'] = $err_msg;
		$this->result['code']   = $result_info['result']['@attributes']['ret'];
		$this->result['reason'] = $result_info['result']['@attributes']['reason'];
		$this->result['data']   = isset($result_info['result']['@attributes']['id']) ? $result_info['result']['@attributes']['id'] : $arr_playbill['nns_playbill_import_id'];
		$this->result['message'] = $file_ret;
		$this->result['send_message'] = $this->_make_array_xml($data);
		return $this->return_xml($this->result);
	}
	
	
	
	/**
	 * 删除节目单数据
	 * @param unknown $content_id
	 * @param unknown $user
	 * @param string $cp_id 目前暂时不要设置为NUll
	 * @param number $live
	 * @param number $is_del_file
	 * @return Ambigous <string(xml), string>
	 */
	public function delete_playbill_info($live_import_id,$media_import_id,$arr_playbill,$str_core_userid,$arr_producer=null,$arr_message_request_data=null)
	{
		$data = array();
		//片源注入数据结构
		$data['func'] 				= 'delete_schedule_record';
		$data['physical_content_id'] 	= $arr_playbill['nns_playbill_import_id'];
		$data['schedule_id'] 			= $arr_playbill['nns_playbill_import_id'];
		$data['physical_channel_id'] 			= $media_import_id;
		$data['cp_content_id'] 			= $arr_playbill['nns_cp_id'];
		$data['user'] 			= $str_core_userid;
		if(isset($arr_producer['nns_cp_id']))
		{
		    $data['cp_id'] = $arr_playbill['nns_import_source'];
		    $data['cms_id'] = $arr_playbill['nns_cp_id'];
		    $data['nmds_id'] = $arr_playbill['nns_node_id'];
		    $data['dst_pop'] = $arr_playbill['nns_list_ids'];
		}
		if(isset($arr_message_request_data) && is_array($arr_message_request_data) && !empty($arr_message_request_data))
		{
		    foreach ($arr_message_request_data as $key=>$val)
		    {
		        $data[$key] = $val;
		    }
		}
		//访问媒资注入接口
		$file_ret = $this->curl->post($this->import_url,$data);
		//判断CURL状态，如果不为200记录错误信息
		$curl_info = $this->curl->curl_getinfo();
		$http_code = $curl_info['http_code'];
		if($curl_info['http_code'] != '200')
		{
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问片源注入失败,HTTP状态码：'.$curl_info['http_code'];
			$this->result['data'] 	= $data;
			$this->result['err_msg'] = '[访问片源注入失败,CURL接口地址]：'.$this->import_url.'[HTTP状态码]：'.$curl_info['http_code'].'[HTTP错误]：'.$this->curl->curl_error().'；参数：'.var_export($data,true);
			return $this->return_xml($this->result);
		}
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($file_ret)),true);
		
		//成功
		if($result_info['result']['@attributes']['ret'] == '0')
		{
			$err_msg = '[访问节目单删除成功,CURL接口地址]：'.$this->import_url.'[接口返回结果]：'.$file_ret.'；参数：'.var_export($data,true);
		}
		else
		{
			$err_msg = '[访问节目单删除失败,CURL接口地址]：'.$this->import_url.'[接口返回结果]：'.$file_ret.'；参数：'.var_export($data,true);
		}
		$this->obj_dom = new DOMDocument("1.0", 'utf-8');
		$data['core_url'] = $this->import_url;
		$this->result['err_msg'] = $err_msg;
		$this->result['code']   = $result_info['result']['@attributes']['ret'];
		$this->result['reason'] = $result_info['result']['@attributes']['reason'];
		$this->result['data']   = isset($result_info['result']['@attributes']['id']) ? $result_info['result']['@attributes']['id'] : $arr_playbill['nns_playbill_import_id'];
		$this->result['message'] = $file_ret;
		$this->result['send_message'] = $this->_make_array_xml($data);
		return $this->return_xml($this->result);
	}

	/**
	 * 点播媒资片源注入msp
	 * @param array  $arr_media_info	片源信息
	 * @param string $str_cp_id			cp
	 * @return string
	 */
	public function import_msp_vod_media($arr_media_info = array(), $str_cp_id,$arr_message_request_data=null)
	{
		$data = array();
		//片源注入数据结构
		$data['func'] = 'add_content_by_cdn_policy';
		$data['name'] = $arr_media_info['nns_name'];
		$data['url'] = $arr_media_info['file_path'];
		$data['cdn_policy_id'] = $arr_media_info['cdn_policy_id'];
		$data['id'] = urlencode($arr_media_info['nns_media_import_id']);
		$data['live'] = 0;
		$data['filetype'] = $arr_media_info['nns_filetype'];
		$data['asset_id'] = $arr_media_info['nns_import_id'];
		$data['cp'] = $arr_media_info['nns_cp_id'];
		$data['transfer_rate'] = 0;
		$data['ingress_capacity'] = 0;
		$data['bitrate'] = $arr_media_info['nns_kbps'];
		$data['cdn_ext_source_id'] = isset($arr_media_info["clip_custom_origin_id"]) ? $arr_media_info["clip_custom_origin_id"] : "";
		$data["domain"] = isset($arr_media_info["nns_domain"]) ? $arr_media_info["nns_domain"] : 257;
		$data['cms_id'] = $arr_media_info['nns_cp_id'];
		$arr_ext_info = array();
		if(isset($arr_media_info['nns_ext_info']) && strlen($arr_media_info['nns_ext_info'])>0)
		{
		    $arr_ext_info = json_decode($arr_media_info['nns_ext_info'],true);
		}
        if(isset($arr_media_info['nns_ext_url']) && strlen($arr_media_info['nns_ext_url'])>0)
        {
            $arr_ext_url = json_decode($arr_media_info['nns_ext_url'],true);
        }
		if(isset($arr_ext_info['node_id']))
		{
			//nns_list_ids
		   // $data['node_id'] = isset($arr_ext_info['node_id']) ? $arr_ext_info['node_id'] : '';
		    $data['nmds_id'] = isset($arr_ext_info['list_ids']) ? $arr_ext_info['list_ids'] : '';
		    $data['cp'] = $arr_media_info['nns_import_source'];
			$data["org_download_path"] = $arr_media_info["nns_url"];
			$data["business_type"] = isset($arr_ext_url['business_type']) ? $arr_ext_url['business_type'] : "iptv";
		}
        if(isset($arr_message_request_data) && is_array($arr_message_request_data) && !empty($arr_message_request_data))
        {
            foreach ($arr_message_request_data as $key=>$val)
            {
                $data[$key] = $val;
            }
        }
        $obj_curl = new np_http_curl_class();
        $arr_header = array(
            "Content-type: multipart/form-data;charset=\"utf-8\"",
        );
        $file_ret = $obj_curl->do_request('post',$this->import_url,$data,$arr_header,60);
		$curl_info = $obj_curl->curl_getinfo();
		unset($obj_curl);
		$http_code = $curl_info['http_code'];
		if($curl_info['http_code'] != '200')
		{
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问片源注入失败,HTTP状态码：'.$curl_info['http_code'];
			$this->result['data'] 	= $data;
			$this->result['err_msg'] = '[访问片源注入失败,CURL接口地址]：'.$this->import_url.'[HTTP状态码]：'.$curl_info['http_code'].'[HTTP错误]：'.$this->curl->curl_error().'；参数：'.var_export($data,true);
			return $this->return_xml($this->result);
		}
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($file_ret)),true);
		//成功
		if($result_info['result']['@attributes']['ret'] == '0')
		{
			$err_msg = '[访问片源注入成功,CURL接口地址]：'.$this->import_url.'[接口返回结果]：'.$file_ret.'；参数：'.var_export($data,true);
			//失败
		}
		else
		{
			$err_msg = '[访问片源注入失败,CURL接口地址]：'.$this->import_url.'[接口返回结果]：'.$file_ret.'；参数：'.var_export($data,true);
		}
		$this->obj_dom = new DOMDocument("1.0", 'utf-8');
		$data['core_url'] = $this->import_url;
		$this->result['err_msg'] = $err_msg;
		$this->result['code']   = $result_info['result']['@attributes']['ret'];
		$this->result['reason'] = $result_info['result']['@attributes']['reason'];
		$this->result['message'] = $file_ret;
		$this->result['send_message'] = $this->_make_array_xml($data);
		return $this->return_xml($this->result);
	}
	/**
	 * 删除msp平台的点播片源信息
	 * @param string $media_content_id msp平台点播片源的content_id
	 * @param string $str_core_user_id msp平台用户id
	 * @return string
	 */
	public function delete_msp_vod_media($media_content_id, $str_core_user_id,$arr_media_info=null,$arr_message_request_data=null)
	{
		$data = array();
		$data['func'] = 'delete_content';
		$data['id'] = $media_content_id;
		$data['live'] = 0;
		$data['user'] = $str_core_user_id;
		$data['is_del_file'] = 1;

	    $arr_ext_info = array();
		if(isset($arr_media_info['nns_ext_info']) && strlen($arr_media_info['nns_ext_info'])>0)
		{
		    $arr_ext_info = json_decode($arr_media_info['nns_ext_info'],true);
		}
		if(isset($arr_ext_info['node_id']))
		{
		    $data['node_id'] = isset($arr_ext_info['node_id']) ? $arr_ext_info['node_id'] : '';
		    $data['list_ids'] = isset($arr_ext_info['list_ids']) ? $arr_ext_info['list_ids'] : '';
		    $data['cms_id'] = $arr_media_info['nns_cp_id'];
		    $data['cp'] = $arr_media_info['nns_import_source'];
		}
		if(isset($arr_message_request_data) && is_array($arr_message_request_data) && !empty($arr_message_request_data))
		{
		    foreach ($arr_message_request_data as $key=>$val)
		    {
		        $data[$key] = $val;
		    }
		}

		$obj_curl = new np_http_curl_class();
		$arr_header = array(
		    "Content-type: multipart/form-data;charset=\"utf-8\"",
		);
		$file_ret = $obj_curl->do_request('post',$this->import_url,$data,$arr_header,60);
		$curl_info = $obj_curl->curl_getinfo();
		unset($obj_curl);
		$http_code = $curl_info['http_code'];
		if($curl_info['http_code'] != '200')
		{
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问片源注入失败,HTTP状态码：'.$curl_info['http_code'];
			$this->result['data'] 	= $data;
			$this->result['err_msg'] = '[访问片源注入失败,CURL接口地址]：'.$this->import_url.'[HTTP状态码]：'.$curl_info['http_code'].'[HTTP错误]：'.$this->curl->curl_error().'；参数：'.var_export($data,true);
			return $this->return_xml($this->result);
		}
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($file_ret)),true);
		//成功
		if($result_info['result']['@attributes']['ret'] == '0')
		{
			$err_msg = '[访问片源注入成功,CURL接口地址]：'.$this->import_url.'[接口返回结果]：'.$file_ret.'；参数：'.var_export($data,true);
			//失败
		}
		else
		{
			$err_msg = '[访问片源注入失败,CURL接口地址]：'.$this->import_url.'[接口返回结果]：'.$file_ret.'；参数：'.var_export($data,true);
		}
		$this->obj_dom = new DOMDocument("1.0", 'utf-8');
		$data['core_url'] = $this->import_url;
		$this->result['err_msg'] = $err_msg;
		$this->result['code']   = $result_info['result']['@attributes']['ret'];
		$this->result['reason'] = $result_info['result']['@attributes']['reason'];
		$this->result['message'] = $file_ret;
		$this->result['send_message'] = $this->_make_array_xml($data);
		
		
		return $this->return_xml($this->result);
	}
	/**
	 * 片源删除
	 * $$assets_file_id     片源来源ID
	 * $assets_video_type   媒资类型   0 点播,1 直播 默认为0
	 */
	public function delete_video_file($assets_file_id,$assets_video_type = 0,$import_source=null,$cp_id=null,$message_id='')
	{
		if(empty($assets_file_id))
		{
			$this->result['code'] = 10001;
			$this->result['reason']  = '片源来源ID不能为空';
			$this->result['data'] = '';
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:i:s').'[访问删除片源失败,CURL接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[错误码]：'.$this->result['code']."\n"
					.date('Y-m-d H:i:s').'[错误原因]：'.$this->result['reason']."\n\n";
				$this->log->_write($this->log_file,$err_msg);
			}

			return $this->return_xml($this->result);
		}
		//片源删除数据结构
		$data['func'] 				= 'delete_video_file';
		$data['assets_video_type'] 	= $assets_video_type;
		$data['assets_file_id'] 	= $assets_file_id;
		$data['bk_cp_id'] 	= (strlen($cp_id) <1) ? 0 : $cp_id;
		$data['bk_message_id'] =$message_id;
		if($this->cp_id !== null) 
		{
			$data['asset_source'] = $this->cp_id;
		}
		if(!empty($import_source))
		{
			$data['import_source'] 	= $import_source;
		}	
			
		//$file_ret = $this->curl->post($this->import_url,$data);
		//获取CURL信息,如果不为200记录错误信息
		//$curl_info = $this->curl->curl_getinfo();

		//for ($i = 0; $i < 1; $i++) {
			//访问媒资注入接口
			$file_ret = $this->curl->post($this->import_url,$data);	
			//判断CURL状态，如果不为200记录错误信息
			$curl_info = $this->curl->curl_getinfo();
			$http_code = $curl_info['http_code'];
			//if ($http_code != 200) {
			//	continue;
			//} else {
			//	break;
			//}
		//}



		if($curl_info['http_code'] != '200')
		{
			if(IMPORT_LOG)
			{
				$err_msg = date('Y-m-d H:i:s').'[访问片源删除失败,CURL接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[HTTP状态码]：'.$curl_info['http_code']."\n"
					.date('Y-m-d H:i:s').'[HTTP错误]：'.$this->curl->curl_error()."\n\n";

				$this->log->_write($this->log_file,$err_msg);

			}
			$this->result['code']   = 10002;
			$this->result['reason'] = '访问片源删除失败,HTTP状态码：'.$curl_info['http_code'];
			$this->result['data']   = $assets_file_id;

			return $this->return_xml($this->result);

		}
		//解析CURL返回结果
		$result_info = json_decode(json_encode(@simplexml_load_string($file_ret)),true);

		//记录日志
		if(IMPORT_LOG)
		{
			//成功
			if($result_info['@attributes']['ret'] == '0')
			{
				$err_msg = date('Y-m-d H:i:s').'[访问片源删除成功,CURL接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[接口返回结果]：'.$file_ret."\n";
				//失败
			}else{
				$err_msg = date('Y-m-d H:i:s').'[访问片源注入失败,CURL接口地址]：'.$this->import_url."\n"
					.date('Y-m-d H:i:s').'[接口返回结果]：'.$file_ret."\n";
			}
			$this->log->_write($this->log_file,$err_msg);

		}
		$this->result['code']   = $result_info['@attributes']['ret'];
		$this->result['reason'] = $result_info['@attributes']['reason'];
		$this->result['data']   = $result_info['@attributes']['id'];

		return $this->return_xml($this->result);
	}

	/**
	 * 检查媒资信息参数
	 * $asset_info 媒资信息
	 */
	private function assets_info_check_params($asset_info)
	{		
		if (empty($asset_info['@attributes']['assets_id']) || empty($asset_info['@attributes']['name'])) 
		{
			$this->result['code'] 	= 10001;
			$this->result['reason'] = '媒资id或者媒资名称为空';
			$this->result['data'] 	= $asset_info['@attributes']['assets_id'];
			return false;
		}
		return true;
	}

	/**
	 * 检查分片信息参数
	 * $clip_info  分片信息
	 */
	private function clip_info_check_params($clip_info)
	{

		if (empty($clip_info['@attributes']['clip_id']) || (empty($clip_info['@attributes']['index']) && $clip_info['@attributes']['index'] != 0)) 
		{
			$this->result['code'] 	= 10001;
			$this->result['reason'] = '分片来源ID或者分片序号不能为空';
			$this->result['data'] 	= $clip_info['@attributes']['clip_id'];
			return false;
		}
		return true;
	}

	/**
	 * 检查片源信息参数
	 * $file_info 	片源信息
	 */
	private function file_info_check_params($file_info)
	{
		if(empty($file_info['@attributes']['file_id']) || empty($file_info['@attributes']['file_type']))
		{
			$this->result['code'] 	= 10001;
			$this->result['reason'] = '片源来源ID或者片源类型不能为空';
			$this->result['data']   = $file_info['@attributes']['file_id'];
			return false;
		}
		return true;
	}

	/**
	 * 传入参数返回xml
	 * @param array $params:代码数组
	 * @return string(xml)
	 */
	private function return_xml($params = array()) {
		$array = array('ret'=>$params['code'],'reason'=>$params['reason'],
		    'data'=>$params['data'],
		    'err_msg'=>isset($params['err_msg']) ? $params['err_msg'] : '',
		    'message'=>isset($params['message']) ? $params['message'] : '',
		    'send_message'=>isset($params['send_message']) ? $params['send_message'] : ''
		);
		return json_encode($array);
		//return "<result  ret=\"{$params['code']}\"  reason=\"{$params['reason']}\" data='{$params['data']}'></result>";
	}

	public function __destruct() {
		unset($this->curl);
	}
	
	/**
	 * 生成xml
	 * @param unknown $data
	 * @param string $obj_parent
	 * @param string $flag
	 */
    private function _make_array_xml($data,$obj_parent=null,$flag=true)
    {
        $obj_parent = $obj_parent === null ? $this->_make_xml($this->obj_dom, 'msp')  : $obj_parent;
        if(empty($data) || !is_array($data))
        {
            return $flag ? $this->obj_dom->saveXML() : true;
        }
        foreach ($data as $key => $val)
        {
            if($key == 'content_mng_xml')
            {
                $val = base64_encode($val);
            }
            if(!is_array($val))
            {
                 $this->_make_xml($obj_parent, $key,$val);
            }
            else if(!empty($val) && is_array($val))
            {
                $obj_parent = $this->_make_xml($obj_parent, $key);
                $this->_make_array_xml($val,$obj_parent,false);
            }
        }
        return $flag ? $this->obj_dom->saveXML() : true;
    }
    
    
    /**
     * 组装基础dom对象类
     * @param object $dom   dom对象
     * @param object $parent 父级对象
     * @param string $key 键
     * @param string $val 值
     * @return object
     * @date 2015-05-04
     */
    private function _make_xml($parent, $key, $val = null, $arr_attr = null)
    {
        $$key = isset($val) ? $this->obj_dom->createElement($key) : $this->obj_dom->createElement($key);
        if (!empty($arr_attr) && is_array($arr_attr))
        {
            foreach ($arr_attr as $attr_key => $attr_val)
            {
                $domAttribute = $this->obj_dom->createAttribute($attr_key);
                $domAttribute->value = $attr_val;
                $$key->appendChild($domAttribute);
                $this->obj_dom->appendChild($$key);
            }
        }
        if(isset($val) && strlen($val) >0)
        {
            $$key->appendChild($this->obj_dom->createCDATASection($val));
        }
        $parent->appendChild($$key);
        unset($parent);
        return $$key;
    }
}

