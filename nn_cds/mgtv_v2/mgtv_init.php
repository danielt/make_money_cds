<?php
$org_id = isset($_GET['sp_id']) ? $_GET['sp_id'] : null;
define('NX_DEBUG_TYPE_XDEBUGER', 'xdebug');
//DEBUGER模式为数据库DEBUG
define('NX_DEBUG_TYPE_DB_DEBUGER', 'db_debug');

//自动 加载model
function mgtv_model_autoload($classname){
	$file = dirname(__FILE__).'/models/'.$classname.'.php';
	if(file_exists($file)){
		include_once $file;
		return true;
	}
	return false;
}
spl_autoload_register('mgtv_model_autoload');
include_once dirname(dirname(__FILE__)).'/nn_cms_config/nn_cms_function.php';
include_once dirname(__FILE__).'/nn_db.class.php';
include_once dirname(__FILE__).'/mgtv_const.php';
include_once dirname(__FILE__).'/utils.func.php';
include_once dirname(__FILE__).'/nn_timer.class.php';
include_once dirname(dirname(__FILE__)).'/nn_logic/log_model/nn_log.class.php';
include_once dirname(dirname(__FILE__)).'/nn_class/public_return.class.php';
include_once dirname(dirname(__FILE__)).'/nn_logic/global_error_log/global_error_log.class.php';
//加载模块自己的初始化文件
if($org_id){
	$sp_list = sp_model::get_sp_list();
	foreach($sp_list as $sp){
		if($sp['nns_id']==$org_id){
			$file_init =  dirname(__FILE__).'/'.$org_id.'/init.php';
			if(file_exists($file_init)){
				include_once $file_init;
			}
			break;
		}
	}
}

function nx_dispatch_debuger()
{
	include_once dirname(dirname(__FILE__)) . '/nn_logic/nl_constant.php';
	include_once dirname(dirname(__FILE__)) . '/nn_logic/debug/debug_factory.class.php';
	error_reporting(E_ALL ^ E_NOTICE);
	$debug = isset($_GET['debug']) ? $_GET['debug'] : '';
	$debug = trim($debug);
	if (empty($debug))
	{
		return;
	}
	else
	{
		echo ($_SERVER['QUERY_STRING']);
		ini_set('display_errors', 1);
	}

	$debugs = explode(',', $debug);
	if (in_array('db', $debugs))
	{
		include_once dirname(dirname(__FILE__)) . '/nn_logic/debug/debug_factory.class.php';

		nl_debug_factory::create(NX_DEBUG_TYPE_DB_DEBUGER);
	}

	if (in_array('xdebug', $debugs))
	{
		include_once dirname(dirname(__FILE__)) . '/nn_logic/debug/debug_factory.class.php';
		nl_debug_factory::create(NX_DEBUG_TYPE_XDEBUGER);
	}
	//扩展xhprof调试


	if (in_array('xhprof', $debugs))
	{
		include_once dirname(dirname(__FILE__)) . '/nn_logic/debug/debug_factory.class.php';
		//开始xhprof调试
		nl_debug_factory::xhprof_start();
	}
}

function nx_output_debug_info()
{
// 	global $g_debug_mode_enabled;
// 	if ($g_debug_mode_enabled != 1)
// 	{
// 		unset($g_debug_mode_enabled);
// 		return;
// 	}
// 	unset($g_debug_mode_enabled);
	$debug_xdebug_mode = isset($_GET['debug_xdebug_mode']) ? $_GET['debug_xdebug_mode'] : 0;
	$debug_db_mode = isset($_GET['debug_db_mode']) ? $_GET['debug_db_mode'] : 0;

	$debug_modes = array ();
	$debug_modes[NX_DEBUG_TYPE_XDEBUGER] = $debug_xdebug_mode;
	$debug_modes[NX_DEBUG_TYPE_DB_DEBUGER] = $debug_db_mode;
	$debug_output = nl_debug_factory::output($debug_modes);
	$output = '';
	if (is_array($debug_output) && count($debug_output) > 0)
	{
		foreach ($debug_output as $key => $info)
		{
			$output .= "<h1>{$key}</h1>";
			$output .= $info;
			$output .= "<hr/>";
		}
	}
	echo $output;
	//扩展xhprof调试
	//输出调试报告
	nl_debug_factory::xhprof_output();
}