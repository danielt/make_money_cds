<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
array_pop($arr_dir);
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/init.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) .'/nn_class/file_encode/file_encode_execute.php';

class timer_execute extends nn_timer
{
    public function action($params = null)
    {
        $this->msg('开始执行...');
        do_timer_action($this);
        $this->msg('执行结束...');
    }
}
$arr_files = pathinfo(__FILE__);
$file_name = $arr_files['filename'];
$timer_execute = new timer_execute($file_name, ORG_ID,__FILE__);
$timer_execute->run();

function do_timer_action($obj_message_log)
{
    $obj_file_encode_execute = new nl_file_encode_execute(ORG_ID,$obj_message_log);
    $obj_file_encode_execute->init();
}