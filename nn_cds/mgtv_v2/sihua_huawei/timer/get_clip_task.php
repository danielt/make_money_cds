<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors',1);
set_time_limit(0); 
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_init.php';
define('ORG_ID', 'sihua_huawei');
include_once  dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/init.php';
include_once dirname(dirname(dirname(__FILE__))) .'/models/queue_task_model.php';

class get_clip_task extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        do_timer_action();
        $this->msg('执行结束...');
    }
}
$get_clip_task = new get_clip_task("get_clip_task", ORG_ID);
$get_clip_task->run();

function do_timer_action()
{
    i_echo('start');
    $sp_config = sp_model::get_sp_config(ORG_ID);
    if(isset($sp_config['clip_pre_max'])&&(int)$sp_config['clip_pre_max']>0){
        $max_num = $sp_config['clip_pre_max'];
    }else{
        $max_num = 30;
    }
    $db = nn_get_db(NL_DB_READ);
    $sql = "select count(nns_id) as num from nns_mgtvbk_clip_task where nns_org_id='".ORG_ID."' and (ISNULL(nns_state) or nns_state='' or nns_state='add')";
    $num = nl_db_get_col($sql, $db);
    
    if($max_num>$num){
        $queue_task_model = new queue_task_model();
        $queue_task_model->q_get_clip_task(ORG_ID,$max_num);
    }
    unset($db,$queue_task_model,$sp_config);
    i_echo('end');
}
