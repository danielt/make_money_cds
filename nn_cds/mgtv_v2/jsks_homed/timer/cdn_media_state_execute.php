<?php
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
array_pop($arr_dir);
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/init.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/sp/sp.class.php';

class timer_execute extends nn_timer
{
    public function action($params = null)
    {
        $this->msg('开始执行...');
        do_timer_action($this);
        $this->msg('执行结束...');
    }
}
$arr_files = pathinfo(__FILE__);
$file_name = $arr_files['filename'];
$timer_execute = new timer_execute($file_name, ORG_ID,__FILE__);
$timer_execute->run();

function do_timer_action($obj_message)
{
    $obj_dc = nl_get_dc(array (
        'db_policy' => NL_DB_WRITE,
        'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
    ));
    $sp_config = nl_sp::get_sp_config($obj_dc,ORG_ID);
    if($sp_config['ret'] !=0)
    {
        nn_log::write_log_message(LOG_MODEL_CDN, 'CDN SP['.ORG_ID.']配置查询失败'.var_export($sp_config,true), ORG_ID);
        return ;
    }
    $sp_config = isset($sp_config['data_info']['nns_config']) ? $sp_config['data_info']['nns_config'] : null;
    if (isset($sp_config['disabled_cdn']) && (int)$sp_config['disabled_cdn'] === 1) 
    {
        nn_log::write_log_message(LOG_MODEL_CDN, 'CDN SP['.ORG_ID.']配置开关关闭', ORG_ID);
        return ;
    }
    if(!isset($sp_config['c2_get_third_play_url']) || strlen($sp_config['c2_get_third_play_url']) <1)
    {
        nn_log::write_log_message(LOG_MODEL_CDN, '获取第三方CDN播放串SP配置c2_get_third_play_url地址未配置', ORG_ID);
        return ;
    }
    $re = content_task_model::vod(null, 'media', ORG_ID,false,$sp_config,$obj_dc);
    nn_log::write_log_message(LOG_MODEL_CDN, '执行结果：'.var_export($re,true), ORG_ID);
    nn_log::write_log_message(LOG_MODEL_CDN, '错误信息：'.var_export(content_task_model::_get_error_data(),true), ORG_ID);
}

