<?php
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
array_pop($arr_dir);
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/init.php';

class timer_execute extends nn_timer
{
    public function action($params = null)
    {
        $this->msg('开始执行...');
        do_timer_action();
        $this->msg('执行结束...');
    }
}
$arr_files = pathinfo(__FILE__);
$file_name = $arr_files['filename'];
$timer_execute = new timer_execute($file_name, ORG_ID,__FILE__);
$timer_execute->run();

function do_timer_action()
{
	//从任务表取内容，调用C2执行
	i_echo('start');
	$sp_config = sp_model::get_sp_config(ORG_ID);
	if (!isset($sp_config['online_disabled']) || (int)$sp_config['online_disabled'] == 0)
	{
		i_echo('close epg');
	}
	else
	{
		$queue_task_model = new queue_task_model();
		$infos = $queue_task_model->get_import_asset_online_list(ORG_ID);
		if(rand(1,500) == 100)
		{
			$queue_task_model->set_delete_asset_online();
		}
		$asset_unline_model = (isset($sp_config['asset_unline_model']) && $sp_config['asset_unline_model'] == '1') ? 1 : 0;
		$send_way = (isset($sp_config['online_send_way']) && $sp_config['online_send_way'] == '1') ? 'http' : 'ftp';
		$third_category_enabled = (isset($sp_config['third_category_enabled']) && $sp_config['third_category_enabled'] == '1') ? true : false;
		$check_video_statuc_enabled = (isset($sp_config['check_video_statuc_enabled']) && $sp_config['check_video_statuc_enabled'] == '1') ? false : true;
		if (is_array($infos) && !empty($infos))
		{
			foreach ($infos as $info_list)
			{
				import_model::import_unline($info_list,$send_way,$third_category_enabled,$check_video_statuc_enabled,$sp_config,$asset_unline_model);
			}
		}
		i_echo('end');
	}
}



