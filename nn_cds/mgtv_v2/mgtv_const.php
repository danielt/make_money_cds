<?php
define('DEBUG',1);


//定义系统常量
define('SP_CONFIG_VIDEO_ADD', 'video_add');//获取SP 添加的系统配置
define('SP_CONFIG_VIDEO_MODIFY', 'video_modify');//获取SP 修改的系统配置
define('SP_CONFIG_VIDEO_DELETE', 'video_destroy');//获取SP 删除的系统配置

define('SP_CONFIG_INDEX_ADD', 'index_add');//获取SP 添加的系统配置
define('SP_CONFIG_INDEX_MODIFY', 'index_modify');//获取SP 修改的系统配置
define('SP_CONFIG_INDEX_DELETE', 'index_destroy');//获取SP 删除的系统配置

define('SP_CONFIG_MEDIA_ADD', 'media_add');//获取SP 添加的系统配置
define('SP_CONFIG_MEDIA_MODIFY', 'media_modify');//获取SP 修改的系统配置
define('SP_CONFIG_MEDIA_DELETE', 'media_destroy');//获取SP 删除的系统配置




define('SP_C2_WAIT', 1);//c2等待执行
define('SP_C2_OK', 0);//c2执行ok
define('SP_C2_FAIL', -1);//c2执行fail
define('SP_C2_DOING', 5);//c2执行ing
define('SP_C2_WAIT_CDI', 6);//等待获取播放串
define('SP_C2_CANCEL', 7);//取消
define('SP_C2_CDI_DO', 8);//正在获取播放串
define('SP_C2_CDI_FAIL', 9);//获取播放串失败
define('SP_EPG_OK', 99);//epg执行ok
define('SP_EPG_DOING', 98);//epg执行ing
define('SP_EPG_WAIT', 97);//epg执行wait

?>