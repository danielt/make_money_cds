<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
set_time_limit(0);  
include_once dirname(dirname(dirname(__FILE__))).'/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) .'/models/queue_task_model.php';
define('ORG_ID', 'dltt');

class import_index extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        do_timer_action();
        $this->msg('执行结束...');
    }
}
$import_index = new import_index("import_index", ORG_ID);
$import_index->run();

function do_timer_action()
{
    set_time_limit(0);
    $db = nn_get_db(NL_DB_WRITE);
    $db->open();
    $sp_config = sp_model::get_sp_config(ORG_ID);
    $cdn_import_media_mode = false;//默认单独模式
    if(isset($sp_config['cdn_import_media_mode'])&&(int)$sp_config['cdn_import_media_mode']===1){
        $cdn_import_media_mode = true;//混合模式
    }
    $queue_task_model = new queue_task_model();
    $file = fopen("index.csv","r");
    while(! feof($file)){
        $arr = (fgetcsv($file));
        $index_arr = explode(',', $arr[1]);
        foreach ($index_arr as $index) {
            $nns_index = (int)($index-1);
            $sql_index = "select * from nns_vod_index where nns_vod_id='{$arr[2]}' and nns_index='$nns_index' and nns_deleted!=1";
            $index_info = nl_db_get_all($sql_index, $db);
            //print_r($index_info);
            if(is_array($index_info)&&count($index_info)>0){
                foreach ($index_info as $index_value) {
                    $queue_task_model->q_add_task($index_value['nns_id'], 'index', 'add',ORG_ID);
                    $sql_media = "select * from nns_vod_media where nns_vod_id='{$arr[2]}' and nns_vod_index_id='{$index_value['nns_id']}' and nns_deleted!=1";
                    if($cdn_import_media_mode){
                        $sql_media .=" order by nns_kbps desc limit 1";
                    }
                    $media_infos = nl_db_get_all($sql_media, $db);
                    if(is_array($media_infos)){
                        foreach($media_infos as $media){
                            $queue_task_model->q_add_task($media['nns_id'], 'media', 'add',ORG_ID);
                        }
                    }
                }
                echo "\033[32m";
                echo $arr[0]." 第".$index."集".PHP_EOL;
            }else{
                echo "\033[31m";
                echo $arr[0]." 第".$index."集  播控缺集".PHP_EOL;
            }
        }
        //print_r($arr);
        //die;
    }
    fclose($file);
}


