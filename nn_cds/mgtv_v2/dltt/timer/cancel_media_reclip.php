<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors',1);
set_time_limit(0); 
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_init.php';
define('ORG_ID', 'dltt');
include_once  dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/init.php';
//include_once dirname(dirname(dirname(__FILE__))) .'/models/queue_task_model.php';

class cancel_media_reclip extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        do_timer_action();
        $this->msg('执行结束...');
    }
}
$cancel_media_reclip = new cancel_media_reclip("cancel_media_reclip", ORG_ID);
$cancel_media_reclip->run();

function do_timer_action()
{
    $db = nn_get_db(NL_DB_WRITE);
    i_echo('start');
    
    //查询媒体文件被取消的状态
    $sql = "select * from nns_mgtvbk_c2_task where nns_org_id='".ORG_ID."' and nns_type='media' and nns_action!='destroy' and ((nns_status=7 and nns_epg_status=100)  or nns_status=-1)";
    
    $infos = nl_db_get_all($sql, $db);
    
    unset($sql);
    $cancel_int_no = 0;
    $fail_int_no = 0;
    $cancel_int = 0;
    $fail_int = 0;
    foreach ($infos as  $info) {
        $type = "";
        if($info['nns_status']==7){
            $type = 'cancel';
            //查询取消的有多少是正在注入
            $sql="select * from nns_mgtvbk_op_queue where nns_org_id='".ORG_ID."' and nns_type='media' and   nns_status=1 and nns_media_id='{$info['nns_ref_id']}'";
        }else{
            $type = 'fail';
            //查询失败的多少正在注入
            $sql="select * from nns_mgtvbk_op_queue where nns_org_id='".ORG_ID."' and nns_type='media' and   nns_status=1 and nns_id='{$info['nns_op_id']}'";
        }
    
        $op_info = nl_db_get_one($sql, $db);
        if(empty($op_info)){
            if($type=='cancel'){
                $cancel_int_no++;
            }else{
                $fail_int_no++;
            }
        }else{
            if($type=='cancel'){
                //$cancel_int++;
                nl_execute_by_db("update nns_mgtvbk_op_queue set nns_status=0 where nns_id='{$op_info['nns_id']}'", $db);
            }else{
                //$fail_int++;
                nl_execute_by_db("update nns_mgtvbk_op_queue set nns_status=0 where nns_id='{$op_info['nns_id']}'", $db);
            }
            nl_execute_by_db("update nns_mgtvbk_c2_task set nns_op_id={$op_info['nns_id']} where nns_id='{$info['nns_id']}'", $db);
        }
    }
    echo PHP_EOL."cancel_int_no-".$cancel_int_no;
    echo PHP_EOL."fail_int_no-".$fail_int_no;
    echo PHP_EOL."cancel_int-".$cancel_int;
    echo PHP_EOL."fail_int-".$fail_int;
    i_echo('end');
}


