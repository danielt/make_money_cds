<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
set_time_limit(0);  
include_once dirname(dirname(dirname(__FILE__))).'/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) .'/models/queue_task_model.php';
define('ORG_ID', 'dltt');

class import_media extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        do_timer_action();
        $this->msg('执行结束...');
    }
}
$import_media = new import_media("import_media", ORG_ID);
$import_media->run();

function do_timer_action()
{
    set_time_limit(0);
    $db = nn_get_db(NL_DB_WRITE);
    $db->open();
    $queue_task_model = new queue_task_model();
    $file = fopen("media.csv","r");
    while(! feof($file))
    {
        $arr = (fgetcsv($file));
        $arr[0] = explode(',', $arr[0]);
        $indexs = "";
        foreach ($arr[0] as  $value) {
            if(intval($value-1)>=0){
                $indexs .= intval($value-1).',';
            }
        }
        $indexs = substr($indexs, 0,-1);
        $sql="select * from nns_vod_media where nns_vod_id='{$arr[1]}' and nns_vod_index in ($indexs) and nns_deleted!=1";
        $medias = nl_db_get_all($sql, $db);
        unset($sql);
        foreach ($medias as $media){
            $queue_task_model->q_add_task($media['nns_id'], 'media', 'add','dltt',null,true);
        }
        unset($arr);
        echo $arr[1].PHP_EOL;
    }
    fclose($file);
    unset($db,$queue_task_model,$file);
}

