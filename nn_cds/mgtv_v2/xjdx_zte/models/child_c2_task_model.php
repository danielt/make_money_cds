<?php
class child_c2_task_model {
    static public function get_BizDomain($sp_id)
    {
        if($sp_id=='hndx' || $sp_id=='hnlt_zx' || $sp_id=='hndx_hw')
        {
            return 520;
        }
        elseif($sp_id=='xjcbc_dx_hw' || $sp_id=='xjdx_hw' || $sp_id=='cbczq_hw' )
        {
            return 0;
        }
        elseif($sp_id=='xjcbc_dx_zte' || $sp_id=='xjdx_zte' || $sp_id=='cbczq_zte' )
        {
            return 257;
        }
        else
        {
            return 2;
        }
    }
    
    static public function add_new_maping($item,$xml_str,$sp_id){
        $action='REGIST';
        //$item = array();
        //$item['nns_task_name'] = $title."-".$start_time."-[节目单]";
    
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'maping' .'_'. $action . '_' . $three_rand. '.xml';
    
        $c2_info = array();
        $c2_info['c2_id'] = np_guid_rand();
        $c2_info['file_name'] = $file_name;
        $c2_info['xml_content'] = $xml_str;
        $c2_info = array(
            'nns_task_type'=>'Maping',
            'nns_task_id'=> null,
            'nns_task_name'=> isset($item['nns_task_name'])?$item['nns_task_name']:null,
            'nns_action'=>	$action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            'nns_desc' => 'Maping,'.$action,
        );
        //return self::execute_c2($c2_info,$sp_id);
    }
    
    static public function add_new_playbill($live_id,$xml_str,$start_time,$sp_id){
        $db = nn_get_db(NL_DB_READ);
        $action = 'REGIST';
        $sql="select nns_name from nns_live where nns_id='$live_id'";
        $title = nl_db_get_col($sql, $db);
        $item = array();
        $item['nns_task_name'] = $title."-".$start_time."-[节目单]";
    
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'schedule' .'_'. $action . '_' . $three_rand. '.xml';
    
        $c2_info = array();
        $c2_info['c2_id'] = np_guid_rand();
        $c2_info['file_name'] = $file_name;
        $c2_info['xml_content'] = $xml_str;
        $c2_info = array(
            //'nns_id' => $c2_info['c2_id'],
            'nns_task_type'=>'Schedule',
            'nns_task_id'=> np_guid_rand(),
            'nns_task_name'=> isset($item['nns_task_name'])?$item['nns_task_name']:null,
            'nns_action'=>	$action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            //'nns_result' =>$result,
            'nns_desc' => 'Schedule,'.$action,
        );
        return self::execute_c2($c2_info,$sp_id);
    
    }
    
    static public function do_package($package,$action,$sp_id){
        $package_num='';
    
        if($package=='VOD'){
            $package_num=0;
        }
        else if($package=='Channel')
        {
            $package_num=2;
        }
        else if($package=='TVOD')
        {
            $package_num=3;
        }
        else if($package=='SVOD')
        {
            $package_num=4;
        }
        else if($package=='PVOD')
        {
            $package_num=5;
        }
    
        $BizDomain = self::get_BizDomain($sp_id);
        $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="'.$BizDomain.'" Priority="1">';
        $xml_str .= '<Objects>';
        $xml_str .= '<Object ElementType="Package" ID="' .$package. '" Action="' . $action . '" Code="' .$package. '">';
        $xml_str .= '<Property Name="Name">'.$package.'</Property>';
        $xml_str .= '<Property Name="Type">'.$package_num.'</Property>';
        $xml_str .= '<Property Name="OrderNumber">'.$package.'</Property>';
        $xml_str .= '<Property Name="LicensingWindowStart">20140101000000</Property>';
        $xml_str .= '<Property Name="LicensingWindowEnd">20540101000000</Property>';
        $xml_str .= '<Property Name="Price">1.00</Property>';
        $xml_str .= '<Property Name="Status">1</Property>';
        $xml_str .= '</Object>';
        $xml_str .= '</Objects>';
        $xml_str .= '</ADI>';
    
        //zgcms_时间_媒资类型_动作_3位随机数.xml
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . strtolower($package) .'_'. $action . '_' . $three_rand. '.xml';
    
        $c2_info = array(
            'nns_task_type'=>'Package',
            'nns_task_id'=> $package,
            'nns_task_name'=> $package,
            'nns_action'=> $action,
            'nns_url' =>$file_name,
            'nns_content' => $xml_str,
            'nns_desc' => 'Package,'.$action,
        );
    
        return  self::execute_c2($c2_info,$sp_id);
    }
    
    /**
     * 添加内容栏目
     */
    static public function add_categoy($category_info,$sp_id){
        if(file_exists(dirname(dirname(__FILE__)).'/'.$sp_id.'/models/child_c2_task_model.php')){
            include_once dirname(dirname(__FILE__)).'/'.$sp_id.'/models/child_c2_task_model.php';
            if(method_exists('child_c2_task_model','add_categoy')){
                $re = child_c2_task_model::add_categoy($category_info,$sp_id);
                return $re;
            }
        }
        $action = 'REGIST';
        return self::do_category($category_info,$action,$sp_id);
    }
    /**
     * 添加内容栏目
     */
    static public function update_category($category_info,$sp_id){
        if(file_exists(dirname(dirname(__FILE__)).'/'.$sp_id.'/models/child_c2_task_model.php')){
            include_once dirname(dirname(__FILE__)).'/'.$sp_id.'/models/child_c2_task_model.php';
            if(method_exists('child_c2_task_model','update_category')){
                $re = child_c2_task_model::update_category($category_info,$sp_id);
                return $re;
            }
        }
        $action = 'UPDATE';
        return self::do_category($category_info,$action,$sp_id);
    }
    /**
     * $param array(
     * nns_id
     * nns_task_id
     * )
     */
    static public function delete_category($param,$sp_id){
    
        if(file_exists(dirname(dirname(__FILE__)).'/'.$sp_id.'/models/child_c2_task_model.php')){
            include_once dirname(dirname(__FILE__)).'/'.$sp_id.'/models/child_c2_task_model.php';
            if(method_exists('child_c2_task_model','delete_category')){
                $re = child_c2_task_model::delete_category($param,$sp_id);
                return $re;
            }
        }
    
        $category_id = $param['nns_id'];
        $action = "DELETE";
        $BizDomain = self::get_BizDomain($sp_id);
        $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="'.$BizDomain.'" Priority="1">';
        $xml_str .= '<Objects>';
        $xml_str .= '<Object ElementType="Category" ID="' .$category_id. '" Action="' . $action . '" Code="' .$category_id. '">';
    
        $xml_str .= '</Object>';
        $xml_str .= '</Objects>';
        $xml_str .= '</ADI>';
    
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'category' .'_'. $action . '_' . $three_rand. '.xml';
    
    
        $c2_info = array(
            'nns_task_type'=>'Category',
            'nns_task_id'=> isset($param['nns_task_id'])?$param['nns_task_id']:null,
            'nns_task_name'=> isset($param['nns_task_name'])?$param['nns_task_name']:null,
            'nns_action'=>	$action,
            'nns_url' =>$file_name,
            'nns_content' => $xml_str,
            'nns_desc' => 'Category,'.$action,
        );
        return  self::execute_c2($c2_info,$sp_id);
    
        /*
         self::save_c2_log($c2_log);
         if($result['ret']==0){
         return true;
         }else{
         return false;
        }*/
    }
    /**
     * 内容栏目
     * $category_info array(
     * 'nns_category_id'
     * 'nns_parent_id'
     * 'nns_name'
     * )
     */
    static public function do_category($category_info,$action,$sp_id){
        $sort = empty($category_info['nns_sort'])?0:$category_info['nns_sort'];
        $BizDomain = self::get_BizDomain($sp_id);
        $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="'.$BizDomain.'" Priority="1">';
        $xml_str .= '<Objects>';
        $xml_str .= '<Object ElementType="Category" ID="' .$category_info['nns_category_id']. '" Action="' . $action . '" Code="' .$category_info['nns_category_id']. '" ParentCode="'.$category_info['nns_parent_id'].'">';
        $xml_str .= '<Property Name="Name">'.$category_info['nns_name'].'</Property>';
        $xml_str .= '<Property Name="ParentID">'.$category_info['nns_parent_id'].'</Property>';
        $xml_str .= '<Property Name="Sequence">'.$sort.'</Property>';
        $xml_str .= '<Property Name="Status">1</Property>';
        $xml_str .= '<Property Name="Description"></Property>';
        $xml_str .= '</Object>';
        $xml_str .= '</Objects>';
        $xml_str .= '</ADI>';
    
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'category' .'_'. $action . '_' . $three_rand. '.xml';
    
    
        $c2_info = array(
            'nns_task_type'=>'Category',
            'nns_task_id'=> isset($category_info['nns_task_id'])?$category_info['nns_task_id']:null,
            'nns_task_name'=> isset($category_info['nns_task_name'])?$category_info['nns_task_name']:null,
            'nns_action'=>	$action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
    
            'nns_desc' => 'Category,'.$action,
        );
        return self::execute_c2($c2_info,$sp_id);
    }
    /**
     * 本方法可用于测试，不建议其他地方使用
     */
    static public function delete_series_by_ids($params){
        $action = "DELETE";
    
        $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
        $xml_str .= '<Objects>';
        foreach($params['nns_ids'] as $id){
            $xml_str .= '<Object ElementType="Series" ID="' .$id. '" Action="' . $action . '" Code="' .$id. '">';
            $xml_str .= '</Object>';
        }
    
        $xml_str .= '</Objects>';
        $xml_str .= '</ADI>';
    
        $guid = np_guid_rand();
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'series' .'_'. $action . '_' . $three_rand. '.xml';
    
        $c2_info = array(
            //'nns_id' => $c2_info['c2_id'],
            'nns_task_type'=>'Series',
            'nns_task_id'=>  isset($params['nns_task_id'])?$params['nns_task_id']:null,
            'nns_task_name'=>  isset($params['nns_task_name'])?$params['nns_task_name']:null,
            'nns_action'=>	$action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            //'nns_result' => '['.$result['ret'].']'.$result['desc'],
            'nns_desc' => 'Series,'.$action,
        );
    
        return self::execute_c2($c2_info);
    
    }
    /**
     * @params array(
     * nns_id
     * nns_task_id
     * )
     */
    static public function delete_series($params,$sp_id){
        include_once dirname(dirname(__FILE__)).'/define.php';
        $sp_config = sp_model::get_sp_config($sp_id);
        if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0){
            $video_info = video_model::get_vod_info($params['nns_id']);
            switch ($sp_config['import_id_mode']) {
                case '1':
                    $params['nns_id'] = '000000'.$video_info['nns_integer_id'];
                    $params['nns_id'] = str_pad($params['nns_id'],32,"0");
                    break;
                case '2':
                    $params['nns_id'] = $video_info['nns_asset_import_id'];
                    break;
                case '3':
                    $params['nns_id'] = CSP_ID . '0000' . $video_info['nns_integer_id'];
                    $params['nns_id'] = str_pad($params['nns_id'],32,"0");
                    break;
                case '4':
                    $arr_csp_id = explode("|", CSP_ID);
                    $str_lenth = strlen($arr_csp_id[0])+strlen($arr_csp_id[1])+1;
                    $lest_num = 32-$str_lenth;
                    $params['nns_id'] = $arr_csp_id[0].'1'.str_pad($video_info['nns_integer_id'], $lest_num, "0", STR_PAD_LEFT).$arr_csp_id[1];
                    break;
            }
        }
    
        $action = "DELETE";
        $BizDomain = self::get_BizDomain($sp_id);
        $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="'.$BizDomain.'" Priority="1">';
        $xml_str .= '<Objects>';
        $xml_str .= '<Object ElementType="Series" ID="' .$params['nns_id']. '" Action="' . $action . '" Code="' .$params['nns_id']. '">';
    
        $xml_str .= '</Object>';
        $xml_str .= '</Objects>';
        $xml_str .= '</ADI>';
    
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'series' .'_'. $action . '_' . $three_rand. '.xml';
    
        $c2_info = array(
            'nns_task_type'=>'Series',
            'nns_task_id'=> isset($params['nns_task_id'])?$params['nns_task_id']:null,
            'nns_task_name'=> isset($params['nns_task_name'])?$params['nns_task_name']:null,
            'nns_action'=> $action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            'nns_desc' => 'Series,'.$action,
        );
        return self::execute_c2($c2_info,$sp_id);
    
    }
    
    
    
    //如果没有收缩到任务名称
    
    static public function get_task_name($task_id){
        $db = nn_get_db(NL_DB_READ);
        $sql = "select nns_name from nns_mgtvbk_c2_task where nns_id='$task_id'";
        return nl_db_get_col($sql, $db);
    }
    
    //修改连续剧
    static public function update_series($vod_info,$sp_id){
    
        $action = 'UPDATE';
        return self::do_series($vod_info,$action,$sp_id);
    }
    
    //添加连续剧
    static public function add_series($vod_info,$sp_id){
        $action = 'REGIST';
        return self::do_series($vod_info,$action,$sp_id);
    }
    
    //连续剧
    static public function do_series($vod_info,$action,$sp_id){
    
        $vod_id = $vod_info['nns_id'];
    
        $program_id = $vod_id;
        include dirname(dirname(__FILE__)).'/define.php';
    
        if(empty($vod_info['nns_task_name'])){
            $vod_info['nns_task_name'] = self::get_task_name($vod_info['nns_task_id']);
        }
        if(empty($vod_info['nns_name'])){
            $vod_info['nns_name'] = $vod_info['nns_task_name'];
        }
        $BizDomain = self::get_BizDomain($sp_id);
        $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="'.$BizDomain.'" Priority="1">';
        $xml_str .= '<Objects>';
    
        $sp_config = sp_model::get_sp_config($sp_id);
        if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0){
            $video_info = video_model::get_vod_info($program_id);
            switch ($sp_config['import_id_mode']) {
                case '1':
                    $vod_id = '000000'.$video_info['nns_integer_id'];
                    $vod_id = str_pad($vod_id,32,"0");
                    break;
                case '2':
                    $vod_id = $video_info['nns_asset_import_id'];
                    break;
                case '3':
                    $vod_id = CSP_ID . '0000' . $video_info['nns_integer_id'];
                    $vod_id = str_pad($vod_id,32,"0");
                    break;
                case '4':
                    $arr_csp_id = explode("|", CSP_ID);
                    $str_lenth = strlen($arr_csp_id[0])+strlen($arr_csp_id[1])+1;
                    $lest_num = 32-$str_lenth;
                    $vod_id = $arr_csp_id[0].'1'.str_pad($video_info['nns_integer_id'], $lest_num, "0", STR_PAD_LEFT).$arr_csp_id[1];
                    break;
            }
            	
        }
        $temp_program_id = $vod_id;
        $xml_str .= '<Object ElementType="Series" ID="' .$vod_id. '" Action="' . $action . '" Code="' .$vod_id. '">';
    
        //到中兴或者华为name为别名
        if ($sp_id == 'xjcbc_dx_hw' || $sp_id == 'xjcbc_dx_zte' || $sp_id == 'xjdx_hw' || $sp_id == 'xjdx_zte' || $sp_id=='cbczq_hw'  || $sp_id=='cbczq_zte')
        {
            
            $vod_info['nns_alias_name']  = strlen($vod_info['nns_alias_name']) <1 ? $vod_info['nns_name'] : $vod_info['nns_alias_name'];
			$xml_str .= '<Property Name="Name">'.htmlspecialchars(mb_substr($vod_info['nns_alias_name'],0,32,'utf-8'),ENT_QUOTES).'</Property>';
        }
        else
        {
            $xml_str .= '<Property Name="Name">'.htmlspecialchars($vod_info['nns_name'],ENT_QUOTES).'</Property>';
        }
    
        $xml_str .= '<Property Name="OrderNumber"></Property>';//节目订购编号
        $xml_str .= '<Property Name="OriginalName">'.htmlspecialchars(mb_substr($vod_info['nns_alias_name'],0,32,'utf-8'),ENT_QUOTES).'</Property>';//原名
//         $xml_str .= '<Property Name="OriginalName">'.htmlspecialchars($vod_info['nns_alias_name'],ENT_QUOTES).'</Property>';//原名
        $xml_str .= '<Property Name="SortName"></Property>';//索引名称供界面排序
        $xml_str .= '<Property Name="SearchName">'.$vod_info['nns_pinyin'].'</Property>';//搜索名称供界面搜索
    
        $str_date = ($vod_info['nns_show_time'] == '0000-00-00' || empty($vod_info['nns_show_time'])) ? date('Ymd') : date('Ymd',strtotime($vod_info['nns_show_time']));
        $xml_str .= '<Property Name="OrgAirDate">' . $str_date .'</Property>';//首播日期(YYYYMMDD)
        $xml_str .= '<Property Name="LicensingWindowStart">'. date('YmdHis').'</Property>';//有效开始时间(YYYYMMDDHH24MiSS)
        $xml_str .= '<Property Name="LicensingWindowEnd">'. date('YmdHis',time()+ 3*365*24*60*60).'</Property>';//有效结束时间(YYYYMMDDHH24MiSS)
        $xml_str .= '<Property Name="DisplayAsNew">0</Property>';//新到天数
        $xml_str .= '<Property Name="DisplayAsLastChance">300</Property>';//剩余天数
        $xml_str .= '<Property Name="Macrovision">0</Property>';//拷贝保护标志0:无拷贝保护	1:有拷贝保护
        $xml_str .= '<Property Name="Price">0</Property>';//含税定价
        $xml_str .= '<Property Name="VolumnCount">'.$vod_info['nns_all_index'].'</Property>';//总集数
        $xml_str .= '<Property Name="Status">1</Property>';//状态标志,0:失效 1:生效
        if($sp_id =='xjcbc_dx_zte' || $sp_id =='xjdx_zte'  || $sp_id=='cbczq_zte')
        {
            $xml_str .= '<Property Name="Description">'.mb_substr(htmlspecialchars($vod_info['nns_summary'],ENT_QUOTES),0,256,'utf-8').'</Property>';//描述信息
        }
        else
        {
            $xml_str .= '<Property Name="Description">'.htmlspecialchars($vod_info['nns_summary'],ENT_QUOTES).'</Property>';//描述信息
        }
        if($sp_id =='xjcbc_dx_zte' || $sp_id =='xjdx_zte'  || $sp_id=='cbczq_zte')
        {
            $xml_str .= '<Property Name="Type">MV</Property>';//演职角色名称
        }
        else
        {
            if(empty($vod_info['nns_kind']))
            {
                $vod_info['nns_kind'] = $vod_info['nns_keyword'];
            }
            $xml_str .= '<Property Name="Type">'.htmlspecialchars($vod_info['nns_kind'],ENT_QUOTES).'</Property>';//节目内容类型,字符串，表示内容类型，例如：电影，连续剧，新闻，体育…
        }
        // 		$xml_str .= '<Property Name="Type">'.htmlspecialchars($vod_info['nns_kind'],ENT_QUOTES).'</Property>';//节目内容类型,字符串，表示内容类型，例如：电影，连续剧，新闻，体育…
        $xml_str .= '<Property Name="Keywords">'.htmlspecialchars($vod_info['nns_keyword'],ENT_QUOTES).'</Property>';//关键字
        $xml_str .= '<Property Name="Tags">0</Property>';//关联标签
        $xml_str .= '<Property Name="Reserve1"></Property>';//保留字段
        $xml_str .= '<Property Name="Reserve2"></Property>';//保留字段
        $xml_str .= '<Property Name="Reserve3"></Property>';//保留字段
        $xml_str .= '<Property Name="Reserve4"></Property>';//保留字段
        $xml_str .= '<Property Name="Reserve5"></Property>';//保留字段
        if($sp_id!='hndx' && $sp_id!='hnlt_zx' && $sp_id!='hndx_hw' && $sp_id!='xjcbc_dx_hw' && $sp_id!='xjcbc_dx_zte' && $sp_id!='xjdx_hw' && $sp_id!='cbczq_hw' && $sp_id!='xjdx_zte' && $sp_id!='cbczq_zte')
        {
            $xml_str .= '<Property Name="RMediaCode"></Property>';//关联内容唯一标识 ,为了支持表示不同屏的同一个内容关系
        }
        $xml_str .= '</Object>';
    
        if($sp_id!='hndx' && $sp_id!='hnlt_zx' && $sp_id!='hndx_hw' && $sp_id!='xjcbc_dx_hw' && $sp_id!='xjcbc_dx_zte' && $sp_id!='xjdx_hw' && $sp_id!='cbczq_hw' && $sp_id!='xjdx_zte' && $sp_id!='cbczq_zte')
        {
            //演员
            $db = nn_get_db(NL_DB_READ);
            //$db->open();
            $sql = "select l.* from nns_video_label l ,nns_video_label_bind_1006 b where l.nns_id=b.nns_label_id and b.nns_video_id='".$program_id."' ";
            $yy_result = nl_db_get_all($sql,$db);
            if(is_array($yy_result)){
                foreach($yy_result as $label){
                    $cast_name = $label['nns_name'];
                    $first_name = $label['nns_name'];
                    if(strlen($label['nns_name']) > 64){
                        $cast_name = i_substr($label['nns_name'],30);
                    }
                    if(strlen($label['nns_name']) > 32){
                        $first_name = i_substr($label['nns_name'],30);
                    }
                    $xml_str .= '<Object ElementType="Cast" ID="' .$label['nns_id']. '" Action="REGIST" Code="' .$label['nns_id']. '">';
                    $xml_str .= '<Property Name="Name">'.$cast_name.'</Property>';//
                    $xml_str .= '<Property Name="FirstName">'.$first_name.'</Property>';//
                    $xml_str .= '</Object>';
    
                    //我方没有演员角色关系表，这里将角色的id设置为 演员标签id+节目id，角色名称设为演员名
                    $xml_str .= '<Object ElementType="CastRoleMap" ID="' .$label['nns_id'].$program_id. '" Action="REGIST" Code="' .$label['nns_id'].$program_id. '">';
                    $xml_str .= '<Property Name="CastRole">演员</Property>';//演职角色名称
                    $xml_str .= '<Property Name="CastID">'.$label['nns_id'].'</Property>';//人物ID
                    $xml_str .= '<Property Name="CastCode">'.$label['nns_id'].'</Property>';//人物Code
                    $xml_str .= '</Object>';
                    	
                }
            }
            //导演
            $sql = "select l.* from nns_video_label l ,nns_video_label_bind_1005 b where l.nns_id=b.nns_label_id and b.nns_video_id='".$program_id."' ";
            $dy_result = nl_db_get_all($sql,$db);
            if(is_array($dy_result)){
                foreach($dy_result as $label){
                    $cast_name = $label['nns_name'];
                    $first_name = $label['nns_name'];
                    if(strlen($label['nns_name']) > 64){
                        $cast_name = i_substr($label['nns_name'],30);
                    }
                    if(strlen($label['nns_name']) > 32){
                        $first_name = i_substr($label['nns_name'],30);
                    }
                    $xml_str .= '<Object ElementType="Cast" ID="' .$label['nns_id']. '" Action="REGIST" Code="' .$label['nns_id']. '">';
                    $xml_str .= '<Property Name="Name">'.htmlspecialchars($cast_name,ENT_QUOTES).'</Property>';//
                    $xml_str .= '<Property Name="FirstName">'.htmlspecialchars($first_name,ENT_QUOTES).'</Property>';//
                    $xml_str .= '</Object>';
    
                    //我方没有演员角色关系表，这里将角色的id设置为 演员标签id+节目id，角色名称设为演员名
                    $xml_str .= '<Object ElementType="CastRoleMap" ID="' .$label['nns_id'].$program_id. '" Action="REGIST" Code="' .$label['nns_id'].$program_id. '">';
                    $xml_str .= '<Property Name="CastRole">导演</Property>';//演职角色名称
                    $xml_str .= '<Property Name="CastID">'.$label['nns_id'].'</Property>';//人物ID
                    $xml_str .= '<Property Name="CastCode">'.$label['nns_id'].'</Property>';//人物Code
                    $xml_str .= '</Object>';
                    	
                }
            }
    
            //图片
            /*if(!empty($vod_info['nns_image0'])){
             $img_save_name = str_replace('.JPG','.jpg',$vod_info['nns_image0']);
             $pic_url = self::get_image_url($img_save_name,$sp_id);
             $xml_str .= '<Object ElementType="Picture" ID="' .$program_id.'_image0'. '" Action="' . $action . '" Code="' .$program_id.'_image0'. '">';
             $xml_str .= '<Property Name="FileURL">'.$pic_url.'</Property>';//图片文件URL, 此路径格式支持ftp、http两种方式
             $xml_str .= '<Property Name="Description"></Property>';//描述
             $xml_str .= '</Object>';
             }*/
            if(!empty($vod_info['nns_image2'])){
                if($action=='REGIST'){
                    $img_save_name = str_replace('.JPG','.jpg',$vod_info['nns_image2']);
                    $file_img = dirname(dirname(dirname(__FILE__))).'/data/downimg/'.$img_save_name;
                    if(file_exists($file_img)){
                        $pic_url = self::get_image_url($img_save_name,$sp_id);
                        $xml_str .= '<Object ElementType="Picture" ID="' .$program_id.'_image2'. '" Action="' . $action . '" Code="' .$program_id.'_image2'. '">';
                        $xml_str .= '<Property Name="FileURL">'.$pic_url.'</Property>';//图片文件URL, 此路径格式支持ftp、http两种方式
                        $xml_str .= '<Property Name="Description"></Property>';//描述
                        $xml_str .= '</Object>';
                    }
                }
    
            }
        }
        $xml_str .= '</Objects>';
    
        if($sp_id!='hndx' && $sp_id!='hnlt_zx' && $sp_id!='hndx_hw' && $sp_id!='xjcbc_dx_hw' && $sp_id!='xjcbc_dx_zte'){
            $xml_str .= '<Mappings>';
            //图片
            /*if(!empty($vod_info['nns_image0'])){
             $xml_str .= '<Mapping ParentType="Picture" ParentID="'.$program_id.'_image0'.'" ElementType="Series" ElementID="'.$program_id.'" ParentCode="'.$program_id.'_image0'.'" ElementCode="'.$program_id.'" Action="' . $action . '">';
             //Picture时：0: 缩略图 1: 海报 2: 剧照 3: 图标 4: 标题图 5: 广告图 6: 草图 7: 背景图 9: 频道图片
             //10: 频道黑白图片 11: 频道Logo 12: 频道名字图片99: 其他
             $xml_str .= '<Property Name="Type">1</Property>';
             $xml_str .= '<Property Name="Sequence">0</Property>';//
             $xml_str .= '</Mapping>';
             }*/
            if($sp_id !='xjdx_hw' && $sp_id !='xjdx_zte' && $sp_id !='cbczq_hw'  && $sp_id!='cbczq_zte')
            {
                if(!empty($vod_info['nns_image2'])){
                    if($action=='REGIST'){
                        $img_save_name = str_replace('.JPG','.jpg',$vod_info['nns_image2']);
                        $file_img = dirname(dirname(dirname(__FILE__))).'/data/downimg/'.$img_save_name;
                        if(file_exists($file_img)){
                            $xml_str .= '<Mapping ParentType="Picture" ParentID="'.$program_id.'_image2'.'" ElementType="Series" ElementID="'.$vod_id.'" ParentCode="'.$program_id.'_image2'.'" ElementCode="'.$vod_id.'" Action="' . $action . '">';
                            $xml_str .= '<Property Name="Type">1</Property>';
                            $xml_str .= '<Property Name="Sequence">1</Property>';
                            $xml_str .= '</Mapping>';
                        }
                    }
                }
            }
    
            if($sp_id == 'xjdx_zte' || $sp_id=='cbczq_zte')
            {
                $xml_str .= '<Mapping ParentType="Category" ParentID="003" ElementType="Series" ElementID="'.$temp_program_id.'" ParentCode="003" ElementCode="'.$temp_program_id.'" Action="REGIST">';
                $xml_str .= '</Mapping>';
            }
    
    
            //栏目
            if(!empty($vod_info['nns_category_id'])){
                if(is_array($vod_info['nns_category_id'])){
                    foreach($vod_info['nns_category_id'] as $nns_category_id){
                        $xml_str .= '<Mapping ParentType="Category" ParentID="'.$nns_category_id.'" ElementType="Series" ElementID="'.$vod_id.'" ParentCode="'.$nns_category_id.'" ElementCode="'.$vod_id.'" Action="REGIST">';
                        $xml_str .= '</Mapping>';
                    }
                }else if(is_string($vod_info['nns_category_id'])){
                    $xml_str .= '<Mapping ParentType="Category" ParentID="'.$vod_info['nns_category_id'].'" ElementType="Series" ElementID="'.$vod_id.'" ParentCode="'.$vod_info['nns_category_id'].'" ElementCode="'.$vod_id.'" Action="REGIST">';
                    $xml_str .= '</Mapping>';
                }
            }
            //演员
            if(is_array($yy_result)){
                foreach($yy_result as $label){
                    $xml_str .= '<Mapping ParentType="Series" ParentID="'.$vod_id.'" ElementType="CastRoleMap" ElementID="'.$label['nns_id'].$program_id.'" ParentCode="'.$vod_id.'" ElementCode="'.$label['nns_id'].$program_id.'" Action="REGIST">';
                    $xml_str .= '</Mapping>';
                }
            }
            //导演
            if(is_array($dy_result)){
                foreach($dy_result as $label){
                    $xml_str .= '<Mapping ParentType="Series" ParentID="'.$vod_id.'" ElementType="CastRoleMap" ElementID="'.$label['nns_id'].$program_id.'" ParentCode="'.$vod_id.'" ElementCode="'.$label['nns_id'].$program_id.'" Action="REGIST">';
                    $xml_str .= '</Mapping>';
                }
            }
    
            if($sp_id=='jllt'  || $sp_id=='nmgutl' || $sp_id=='xjcbc_dx_hw' || $sp_id=='xjcbc_dx_zte' || $sp_id=='xjdx_hw' || $sp_id=='cbczq_hw'){
                //绑定服务包
                $str_temp_vod = 'VOD';
                if($sp_id=='xjdx_hw' || $sp_id=='cbczq_hw')
                {
                    $str_temp_vod='KKTV';
                }
                else
                {
                    $temp_program_id = $program_id;
                }
                $xml_str .= '<Mapping ParentType="Package" ParentID="'.$str_temp_vod.'" ElementType="Series" ElementID="'.$temp_program_id.'" Action="REGIST" ParentCode="'.$str_temp_vod.'" ElementCode="'.$temp_program_id.'">';
                $xml_str .= '<Property Name="ValidStart">20140101000000</Property>';
                $xml_str .= '<Property Name="ValidEnd">20540101000000</Property>';
                $xml_str .= '</Mapping>';
            }
    
            $xml_str .= '</Mappings>';
        }
    
    
        $xml_str .= '</ADI>';
    
        //echo $xml_str;die;
        $guid = np_guid_rand();
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'series' .'_'. $action . '_' . $three_rand. '.xml';
    
        //
        $c2_info = array(
            'nns_task_type'=>'Series',
            'nns_task_id'=> isset($vod_info['nns_task_id'])?$vod_info['nns_task_id']:null,
            'nns_task_name'=> isset($vod_info['nns_task_name'])?$vod_info['nns_task_name']:null,
            'nns_action'=> $action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            'nns_desc' => 'Series,'.$action,
        );
    
        return self::execute_c2($c2_info,$sp_id);
    }
    /**
     * 点播内容/点播分集
     * $vod_info array(
     * 'nns_id'   影片id/分集id
     * 'nns_vod_id' 分集属于影片id
     * 'nns_category_id'  影片的栏目id
     * 'nns_series_flag' 连续剧分集标识
     * )
     * 目前实现上，给对方的影片id都是我方的分集id，因为片源是和分集关联的；
     * 所以在删除影片时，也是用分集id删除
     */
    static public function do_vod($vod_info,$action,$sp_id){
        //$action = 'REGIST';
        include dirname(dirname(__FILE__)).'/define.php';
        $program_id = $vod_info['nns_id'];//vod nns_vod_id index nns_vod_index_id
        $vod_id = $program_id;
        $sp_config = sp_model::get_sp_config($sp_id);
        if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0){
            $video_info = video_model::get_vod_index_info($program_id);
            switch ($sp_config['import_id_mode']) {
                case '1':
                    $vod_id = '001000'.$video_info['nns_integer_id'];
                    $vod_id = str_pad($vod_id,32,"0");
                    break;
                case '2':
                    $vod_id = $video_info['nns_import_id'];
                    break;
                case '3':
                    $vod_id = CSP_ID . '0001' . $video_info['nns_integer_id'];
                    $vod_id = str_pad($vod_id,32,"0");
                    break;
                case '4':
                    $arr_csp_id = explode("|", CSP_ID);
                    $str_lenth = strlen($arr_csp_id[0])+strlen($arr_csp_id[1])+1;
                    $lest_num = 32-$str_lenth;
                    $vod_id = $arr_csp_id[0].'2'.str_pad($video_info['nns_integer_id'], $lest_num, "0", STR_PAD_LEFT).$arr_csp_id[1];
                    break;
            }
        }
        $temp_program_id = $vod_id;
        $BizDomain = self::get_BizDomain($sp_id);
        $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="'.$BizDomain.'" Priority="1">';
        $xml_str .= '<Objects>';
    
        $xml_str .= '<Object ElementType="Program" ID="' .$vod_id. '" Action="' . $action . '" Code="' .$vod_id. '">';
    
        if(empty($vod_info['nns_task_name'])){
            $vod_info['nns_task_name'] = self::get_task_name($vod_info['nns_task_id']);
        }
        if(empty($vod_info['nns_name'])){
            $vod_info['nns_name'] = $vod_info['nns_task_name'];
        }
    
        //到中兴或者华为name为别名
        if ($sp_id == 'xjcbc_dx_hw' || $sp_id == 'xjcbc_dx_zte' || $sp_id == 'xjdx_hw' || $sp_id == 'xjdx_zte' || $sp_id=='cbczq_hw' || $sp_id=='cbczq_zte')
        {
            $vod_info['nns_alias_name']  = strlen($vod_info['nns_alias_name']) <1 ? $vod_info['nns_name'] : $vod_info['nns_alias_name'];
			$xml_str .= '<Property Name="Name">'.htmlspecialchars(mb_substr($vod_info['nns_alias_name'],0,32,'utf-8'),ENT_QUOTES).'</Property>';
//             $xml_str .= '<Property Name="Name">'.htmlspecialchars($vod_info['nns_alias_name'],ENT_QUOTES).'</Property>';
        }
        else
        {
            $xml_str .= '<Property Name="Name">'.htmlspecialchars($vod_info['nns_name'],ENT_QUOTES).'</Property>';
        }
    
        $xml_str .= '<Property Name="OrderNumber"></Property>';//节目订购编号
        $xml_str .= '<Property Name="OriginalName">'.htmlspecialchars(mb_substr($vod_info['nns_alias_name'],0,32,'utf-8'),ENT_QUOTES).'</Property>';//原名
        $xml_str .= '<Property Name="SortName"></Property>';//索引名称供界面排序
        $xml_str .= '<Property Name="SearchName">'.$vod_info['nns_pinyin'].'</Property>';//搜索名称供界面搜索
        $xml_str .= '<Property Name="Genre">Genre</Property>';//Program的默认类别（Genre）
        $xml_str .= '<Property Name="ActorDisplay">'.htmlspecialchars($vod_info['nns_actor'],ENT_QUOTES).'</Property>';//演员列表(只供显示)
        $xml_str .= '<Property Name="WriterDisplay">'.htmlspecialchars($vod_info['nns_director'],ENT_QUOTES).'</Property>';//作者列表(只供显示)
        $xml_str .= '<Property Name="OriginalCountry">'.htmlspecialchars($vod_info['nns_area'],ENT_QUOTES).'</Property>';//国家地区
        $xml_str .= '<Property Name="Language">'.htmlspecialchars($vod_info['nns_language'],ENT_QUOTES).'</Property>';//语言
        $show_time = null;
        if($vod_info['nns_index_show_time']=='0000-00-00 00:00:00' || empty($vod_info['nns_index_show_time'])){
            $show_time = $vod_info['nns_show_time'];//影片基本信息上的上映时间
        }else{
            $show_time = $vod_info['nns_index_show_time'];//分集的上映时间
        }
    
        $xml_str .= '<Property Name="ReleaseYear">' .  date('Y',strtotime($show_time)).'</Property>';//上映年份(YYYY)
        $xml_str .= '<Property Name="OrgAirDate">' . date('Ymd',strtotime($show_time)).'</Property>';//首播日期(YYYYMMDD)
        $xml_str .= '<Property Name="LicensingWindowStart">'. date('YmdHis').'</Property>';//有效开始时间(YYYYMMDDHH24MiSS)
        $xml_str .= '<Property Name="LicensingWindowEnd">'. date('YmdHis',time()+ 3*365*24*60*60).'</Property>';//有效结束时间(YYYYMMDDHH24MiSS)
        $xml_str .= '<Property Name="DisplayAsNew">0</Property>';//新到天数
        $xml_str .= '<Property Name="DisplayAsLastChance">300</Property>';//剩余天数
        $xml_str .= '<Property Name="Macrovision">0</Property>';//拷贝保护标志0:无拷贝保护	1:有拷贝保护
        //$xml_str .= '<Property Name="Description">'.htmlspecialchars($vod_info['nns_summary'],ENT_QUOTES).'</Property>';//节目描述
        if($sp_id =='xjcbc_dx_zte' || $sp_id =='xjdx_zte' || $sp_id=='cbczq_zte')
        {
            $xml_str .= '<Property Name="Description">'.mb_substr(htmlspecialchars($vod_info['nns_summary'],ENT_QUOTES),0,256,'utf-8').'</Property>';//描述信息
        }
        else
        {
            $xml_str .= '<Property Name="Description">'.htmlspecialchars($vod_info['nns_summary'],ENT_QUOTES).'</Property>';//描述信息
        }
        $xml_str .= '<Property Name="PriceTaxIn"></Property>';//列表定价
        $xml_str .= '<Property Name="Status">1</Property>';//状态标志 0:失效 1:生效
        $xml_str .= '<Property Name="SourceType">1</Property>';//1: VOD 5: Advertisement
        $xml_str .= '<Property Name="SeriesFlag">'.$vod_info['nns_series_flag'].'</Property>';//0: 普通VOD 1: 连续剧剧集
        // 		if(empty($vod_info['nns_kind']))
            // 		{
            // 			$vod_info['nns_kind'] = $vod_info['nns_keyword'];
            // 		}
        // 		$xml_str .= '<Property Name="Type">'.htmlspecialchars($vod_info['nns_kind'],ENT_QUOTES).'</Property>';//节目内容类型 字符串，表示内容类型，例如：电影，连续剧，新闻，体育…
    
        if($sp_id =='xjcbc_dx_zte' || $sp_id =='xjdx_zte' || $sp_id=='cbczq_zte' )
        {
            $xml_str .= '<Property Name="Type">MV</Property>';//演职角色名称
        }
        else
        {
            if(empty($vod_info['nns_kind']))
            {
                $vod_info['nns_kind'] = $vod_info['nns_keyword'];
            }
            $xml_str .= '<Property Name="Type">'.htmlspecialchars($vod_info['nns_kind'],ENT_QUOTES).'</Property>';//节目内容类型,字符串，表示内容类型，例如：电影，连续剧，新闻，体育…
        }
    
        $xml_str .= '<Property Name="Keywords">'.htmlspecialchars($vod_info['nns_keyword'],ENT_QUOTES).'</Property>';//关键字 多个关键字之间使用分号分隔
        $xml_str .= '<Property Name="Tags">1</Property>';//关联标签 多个标签之间使用分号分隔
        $xml_str .= '<Property Name="Reserve1"></Property>';//保留字段
        $xml_str .= '<Property Name="Reserve2"></Property>';//保留字段
        $xml_str .= '<Property Name="Reserve3"></Property>';//保留字段
        $xml_str .= '<Property Name="Reserve4"></Property>';//保留字段
        $xml_str .= '<Property Name="Reserve5"></Property>';//保留字段
        if($sp_id!='hndx' && $sp_id!='hnlt_zx' && $sp_id!='hndx_hw' && $sp_id!='xjcbc_dx_hw' && $sp_id!='xjcbc_dx_zte' && $sp_id!='xjdx_hw' && $sp_id!='cbczq_hw' && $sp_id!='xjdx_zte' && $sp_id!='cbczq_zte')
        {
            $xml_str .= '<Property Name="StorageType">1</Property>';//存储分发策略要求
    
            $xml_str .= '<Property Name="RMediaCode"></Property>';//关联内容唯一标识 ,为了支持表示不同屏的同一个内容关系
        }
    
        $xml_str .= '</Object>';
        if($sp_id!='hndx' && $sp_id!='hnlt_zx' && $sp_id!='hndx_hw' && $sp_id!='xjcbc_dx_hw' && $sp_id!='xjcbc_dx_zte' && $sp_id!='xjdx_hw' && $sp_id!='cbczq_hw' && $sp_id!='xjdx_zte' && $sp_id!='cbczq_zte')
        {
            //演员
            $db = nn_get_db(NL_DB_READ);
            //$db->open();
            $sql = "select l.* from nns_video_label l ,nns_video_label_bind_1006 b where l.nns_id=b.nns_label_id and b.nns_video_id='".$program_id."' ";
            $yy_result = nl_db_get_all($sql,$db);
            if(is_array($yy_result)){
                foreach($yy_result as $label){
                    $cast_name = $label['nns_name'];
                    $first_name = $label['nns_name'];
                    if(strlen($label['nns_name']) > 64){
                        $cast_name = i_substr($label['nns_name'],30);
                    }
                    if(strlen($label['nns_name']) > 32){
                        $first_name = i_substr($label['nns_name'],30);
                    }
                    $xml_str .= '<Object ElementType="Cast" ID="' .$label['nns_id']. '" Action="REGIST" Code="' .$label['nns_id']. '">';
                    $xml_str .= '<Property Name="Name">'.htmlspecialchars($cast_name,ENT_QUOTES).'</Property>';//
                    $xml_str .= '<Property Name="FirstName">'.htmlspecialchars($first_name,ENT_QUOTES).'</Property>';//
                    $xml_str .= '</Object>';
    
                    //我方没有演员角色关系表，这里将角色的id设置为 演员标签id+节目id，角色名称设为演员名
                    $xml_str .= '<Object ElementType="CastRoleMap" ID="' .$label['nns_id'].$program_id. '" Action="REGIST" Code="' .$label['nns_id'].$program_id. '">';
                    $xml_str .= '<Property Name="CastRole">演员</Property>';//演职角色名称
                    $xml_str .= '<Property Name="CastID">'.$label['nns_id'].'</Property>';//人物ID
                    $xml_str .= '<Property Name="CastCode">'.$label['nns_id'].'</Property>';//人物Code
                    $xml_str .= '</Object>';
                    	
                }
            }
            //导演
            $sql = "select l.* from nns_video_label l ,nns_video_label_bind_1005 b where l.nns_id=b.nns_label_id and b.nns_video_id='".$program_id."' ";
            $dy_result = nl_db_get_all($sql,$db);
            if(is_array($dy_result)){
                foreach($dy_result as $label){
                    $cast_name = $label['nns_name'];
                    $first_name = $label['nns_name'];
                    if(strlen($label['nns_name']) > 64){
                        $cast_name = i_substr($label['nns_name'],30);
                    }
                    if(strlen($label['nns_name']) > 32){
                        $first_name = i_substr($label['nns_name'],30);
                    }
                    	
                    $xml_str .= '<Object ElementType="Cast" ID="' .$label['nns_id']. '" Action="REGIST" Code="' .$label['nns_id']. '">';
                    $xml_str .= '<Property Name="Name">'.htmlspecialchars($cast_name,ENT_QUOTES).'</Property>';//
                    $xml_str .= '<Property Name="FirstName">'.htmlspecialchars($first_name,ENT_QUOTES).'</Property>';//
                    $xml_str .= '</Object>';
    
                    //我方没有演员角色关系表，这里将角色的id设置为 演员标签id+节目id，角色名称设为演员名
                    $xml_str .= '<Object ElementType="CastRoleMap" ID="' .$label['nns_id'].$program_id. '" Action="REGIST" Code="' .$label['nns_id'].$program_id. '">';
                    $xml_str .= '<Property Name="CastRole">导演</Property>';//演职角色名称
                    $xml_str .= '<Property Name="CastID">'.$label['nns_id'].'</Property>';//人物ID
                    $xml_str .= '<Property Name="CastCode">'.$label['nns_id'].'</Property>';//人物Code
                    $xml_str .= '</Object>';
                    	
                }
            }
            if( isset($vod_info['nns_series_flag']) && $vod_info['nns_series_flag']==1){
                //图片
                /*if(!empty($vod_info['nns_image'])){
                 $img_save_name = str_replace('.JPG','.jpg',$vod_info['nns_image']);
                 $pic_url = self::get_image_url($img_save_name);
                 $xml_str .= '<Object ElementType="Picture" ID="' .$program_id.'_index_image'. '" Action="' . $action . '" Code="' .$program_id.'_index_image'. '">';
                 $xml_str .= '<Property Name="FileURL">'.$pic_url.'</Property>';//图片文件URL, 此路径格式支持ftp、http两种方式
                 $xml_str .= '<Property Name="Description"></Property>';//描述
                 $xml_str .= '</Object>';
                 }*/
            }else{
                //图片
                if(!empty($vod_info['nns_image0'])){
                    if($action=='REGIST'){
                        $img_save_name = str_replace('.JPG','.jpg',$vod_info['nns_image0']);
                        //$img_save_name = str_replace('.JPG','.jpg',$vod_info['nns_image0']);
                        $file_img = dirname(dirname(dirname(__FILE__))).'/data/downimg/'.$img_save_name;
                        if(file_exists($file_img)){
                            $pic_url = self::get_image_url($img_save_name,$sp_id);
                            $xml_str .= '<Object ElementType="Picture" ID="' .$program_id.'_image0'. '" Action="' . $action . '" Code="' .$program_id.'_image0'. '">';
                            $xml_str .= '<Property Name="FileURL">'.$pic_url.'</Property>';//图片文件URL, 此路径格式支持ftp、http两种方式
                            $xml_str .= '<Property Name="Description"></Property>';//描述
                            $xml_str .= '</Object>';
                        }
                    }
                }
                /*if(!empty($vod_info['nns_image2'])){
                 $img_save_name = str_replace('.JPG','.jpg',$vod_info['nns_image2']);
                 $pic_url = self::get_image_url($img_save_name,$sp_id);
                 $xml_str .= '<Object ElementType="Picture" ID="' .$program_id.'_image2'. '" Action="' . $action . '" Code="' .$program_id.'_image2'. '">';
                 $xml_str .= '<Property Name="FileURL">'.$pic_url.'</Property>';//图片文件URL, 此路径格式支持ftp、http两种方式
                 $xml_str .= '<Property Name="Description"></Property>';//描述
                 $xml_str .= '</Object>';
                 }*/
            }
        }
    
        $xml_str .= '</Objects>';
        //if($sp_id!='hndx'){
        $xml_str .= '<Mappings>';
    
    
        if($sp_id!='hndx' && $sp_id!='hnlt_zx' && $sp_id!='hndx_hw' && $sp_id!='xjcbc_dx_hw' && $sp_id!='xjcbc_dx_zte' && $sp_id!='xjdx_hw' && $sp_id!='cbczq_hw' && $sp_id!='xjdx_zte' && $sp_id!='cbczq_zte')
        {
            //演员
            if(is_array($yy_result)){
                foreach($yy_result as $label){
                    $xml_str .= '<Mapping ParentType="Program" ParentID="'.$vod_id.'" ElementType="CastRoleMap" ElementID="'.$label['nns_id'].$program_id.'" ParentCode="'.$vod_id.'" ElementCode="'.$label['nns_id'].$program_id.'" Action="REGIST">';
                    $xml_str .= '</Mapping>';
                }
            }
            //导演
            if(is_array($dy_result)){
                foreach($dy_result as $label){
                    $xml_str .= '<Mapping ParentType="Program" ParentID="'.$vod_id.'" ElementType="CastRoleMap" ElementID="'.$label['nns_id'].$program_id.'" ParentCode="'.$vod_id.'" ElementCode="'.$label['nns_id'].$program_id.'" Action="REGIST">';
                    $xml_str .= '</Mapping>';
                }
            }
        }
        //如果是连续剧分集，绑定到连续剧
        if( isset($vod_info['nns_series_flag']) && $vod_info['nns_series_flag']==1)
        {
            $video_id = $vod_info['nns_vod_id'];
            	
            if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0){
                $video_info = video_model::get_vod_info($video_id);
                switch ($sp_config['import_id_mode']) {
                    case '1':
                        $video_id = '000000'.$video_info['nns_integer_id'];
                        $video_id = str_pad($video_id,32,"0");
                        break;
                    case '2':
                        $video_id = $video_info['nns_asset_import_id'];
                        break;
                    case '3':
                        $video_id = CSP_ID . '0000' . $video_info['nns_integer_id'];
                        $video_id = str_pad($video_id,32,"0");
                        break;
                    case '4':
                        $arr_csp_id = explode("|", CSP_ID);
                        $str_lenth = strlen($arr_csp_id[0])+strlen($arr_csp_id[1])+1;
                        $lest_num = 32-$str_lenth;
                        $video_id = $arr_csp_id[0].'1'.str_pad($video_info['nns_integer_id'], $lest_num, "0", STR_PAD_LEFT).$arr_csp_id[1];
                        break;
                }
            }
    
            $sequence = $vod_info['nns_index']+1;
            if($sequence > 999){
                if($sp_id != 'fjyd')
                {
                    $sequence = 999;
                }
            }
    
            if($action=='REGIST'){
                $xml_str .= '<Mapping ParentType="Series" ParentID="'.$video_id.'" ElementType="Program" ElementID="'.$vod_id.'" ParentCode="'.$video_id.'" ElementCode="'.$vod_id.'" Action="REGIST">';
                $xml_str .= '<Property Name="Sequence">'.$sequence.'</Property>';//
                $xml_str .= '</Mapping>';
            }
    
            /*/图片
             if(!empty($vod_info['nns_image'])){
            $xml_str .= '<Mapping ParentType="Picture" ParentID="'.$program_id.'_index_image'.'" ElementType="Program" ElementID="'.$program_id.'" ParentCode="'.$program_id.'_index_image'.'" ElementCode="'.$program_id.'" Action="' . $action . '">';
            //Picture时：0: 缩略图 1: 海报 2: 剧照 3: 图标 4: 标题图 5: 广告图 6: 草图 7: 背景图 9: 频道图片
            //10: 频道黑白图片 11: 频道Logo 12: 频道名字图片99: 其他
            $xml_str .= '<Property Name="Type">1</Property>';
            $xml_str .= '<Property Name="Sequence">0</Property>';//
            $xml_str .= '</Mapping>';
            }*/
        }else{
            if($sp_id!='hndx' && $sp_id!='hnlt_zx' && $sp_id!='hndx_hw' && $sp_id!='xjcbc_dx_hw' && $sp_id!='xjcbc_dx_zte' && $sp_id!='xjdx_hw' && $sp_id!='cbczq_hw' && $sp_id!='xjdx_zte' && $sp_id!='cbczq_zte')
            {
                //栏目
                if(!empty($vod_info['nns_category_id']))
                {
                    if(is_array($vod_info['nns_category_id'])){
                        foreach($vod_info['nns_category_id'] as $nns_category_id){
                            $xml_str .= '<Mapping ParentType="Category" ParentID="'.$nns_category_id.'" ElementType="Program" ElementID="'.$program_id.'" ParentCode="'.$nns_category_id.'" ElementCode="'.$program_id.'" Action="REGIST">';
                            $xml_str .= '</Mapping>';
                        }
                    }else if(is_string($vod_info['nns_category_id'])){
                        $xml_str .= '<Mapping ParentType="Category" ParentID="'.$vod_info['nns_category_id'].'" ElementType="Program" ElementID="'.$program_id.'" ParentCode="'.$vod_info['nns_category_id'].'" ElementCode="'.$program_id.'" Action="REGIST">';
                        $xml_str .= '</Mapping>';
                    }
                }
                //图片
                if(!empty($vod_info['nns_image0']))
                {
                    if($action=='REGIST'){
                        $img_save_name = str_replace('.JPG','.jpg',$vod_info['nns_image0']);
                        //$img_save_name = str_replace('.JPG','.jpg',$vod_info['nns_image0']);
                        $file_img = dirname(dirname(dirname(__FILE__))).'/data/downimg/'.$img_save_name;
                        if(file_exists($file_img)){
                            $xml_str .= '<Mapping ParentType="Picture" ParentID="'.$program_id.'_image0'.'" ElementType="Program" ElementID="'.$program_id.'" ParentCode="'.$program_id.'_image0'.'" ElementCode="'.$program_id.'" Action="' . $action . '">';
                            //Picture时：0: 缩略图 1: 海报 2: 剧照 3: 图标 4: 标题图 5: 广告图 6: 草图 7: 背景图 9: 频道图片
                            //10: 频道黑白图片 11: 频道Logo 12: 频道名字图片99: 其他
                            $xml_str .= '<Property Name="Type">1</Property>';
                            $xml_str .= '<Property Name="Sequence">0</Property>';//
                            $xml_str .= '</Mapping>';
                        }
                    }
                }
                /*if(!empty($vod_info['nns_image2'])){
                 $xml_str .= '<Mapping ParentType="Picture" ParentID="'.$program_id.'_image2'.'" ElementType="Program" ElementID="'.$program_id.'" ParentCode="'.$program_id.'_image2'.'" ElementCode="'.$program_id.'" Action="' . $action . '">';
                 $xml_str .= '<Property Name="Type">1</Property>';
                 $xml_str .= '<Property Name="Sequence">1</Property>';//
                 $xml_str .= '</Mapping>';
                 }*/
    
                //添加服务包绑定
                /*$xml_str .= '<Mapping ParentType="Package" ParentID="VOD" ElementType="Program" ElementID="'.$program_id.'" Action="REGIST" ParentCode="VOD" ElementCode="'.$program_id.'" >';
                 $xml_str .= '<Property Name="ValidStart">20140101000000</Property>';
                 $xml_str .= '<Property Name="ValidEnd">20540101000000</Property>';
                 $xml_str .= '</Mapping>';*/
            }
        }
    
        if($sp_id=='jllt'  || $sp_id=='nmgutl' || $sp_id=='xjcbc_dx_hw' || $sp_id=='xjcbc_dx_zte'  || $sp_id=='xjdx_hw' || $sp_id=='cbczq_hw')
        {
            $str_temp_vod = 'VOD';
            if($sp_id=='xjdx_hw' || $sp_id=='cbczq_hw')
            {
                $str_temp_vod='KKTV';
            }
            else
            {
                $temp_program_id = $program_id;
            }
            $xml_str .= '<Mapping ParentType="Package" ParentID="'.$str_temp_vod.'" ElementType="Program" ElementID="'.$temp_program_id.'" Action="REGIST" ParentCode="'.$str_temp_vod.'" ElementCode="'.$temp_program_id.'" >';
            $xml_str .= '<Property Name="ValidStart">20140101000000</Property>';
            $xml_str .= '<Property Name="ValidEnd">20540101000000</Property>';
            $xml_str .= '</Mapping>';
        }
    
        $xml_str .= '</Mappings>';
        //}
        $xml_str .= '</ADI>';
        $guid = np_guid_rand();
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'program' .'_'. $action . '_' . $three_rand. '.xml';
        //self::execute_c2($file_name, $xml_str);
    
        $c2_info = array(
            //'nns_id' => $c2_info['c2_id'],
            'nns_task_type'=>'Program',
            'nns_task_id'=> isset($vod_info['nns_task_id'])?$vod_info['nns_task_id']:null,
            'nns_task_name'=> isset($vod_info['nns_task_name'])?$vod_info['nns_task_name']:null,
            'nns_action'=> $action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            //'nns_result' => '['.$result['ret'].']'.$result['desc'],
            'nns_desc' => 'Program,'.$action,
        );
    
        return self::execute_c2($c2_info,$sp_id);
    }
    
    
    static public function get_image_url($img_path,$sp_id){
        include dirname(dirname(__FILE__)).'/define.php';
        if(defined('HW_DOANLOAD_IMG_METHOD') && HW_DOANLOAD_IMG_METHOD=='ftp'){
            $img_url = HW_DOANLOAD_IMG_FTP.$img_path;
        }else{
            $img_url = nl_image::epg_video_image_url($img_path);
        }
        return $img_url;
    }
    //修改点播
    static public function update_vod($vod_info,$sp_id)
    {
        
        $action = 'UPDATE';
        return self::do_vod($vod_info,$action,$sp_id);
    }
    //添加点播
    static public function add_vod($vod_info,$sp_id){
        $action = 'REGIST';
        return self::do_vod($vod_info,$action,$sp_id);
    }
    /**
     * params
     */
    static public function delete_vod($params,$sp_id){
        include_once dirname(dirname(__FILE__)).'/define.php';
        
        $action = 'DELETE';
        $program_id = $params['nns_id'];
    
        $sp_config = sp_model::get_sp_config($sp_id);
        if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0){
            $video_info = video_model::get_vod_index_info($program_id);
            switch ($sp_config['import_id_mode']) {
                case '1':
                    $program_id = '001000'.$video_info['nns_integer_id'];
                    $program_id = str_pad($program_id,32,"0");
                    break;
                case '2':
                    $program_id = $video_info['nns_import_id'];
                    break;
                case '3':
                    $program_id = CSP_ID . '0001' . $video_info['nns_integer_id'];
                    $program_id = str_pad($program_id,32,"0");
                    break;
                case '4':
                    $arr_csp_id = explode("|", CSP_ID);
                    $str_lenth = strlen($arr_csp_id[0])+strlen($arr_csp_id[1])+1;
                    $lest_num = 32-$str_lenth;
                    $program_id = $arr_csp_id[0].'2'.str_pad($video_info['nns_integer_id'], $lest_num, "0", STR_PAD_LEFT).$arr_csp_id[1];
                    break;
            }
            	
        }
    
        $BizDomain = self::get_BizDomain($sp_id);
        $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="'.$BizDomain.'" Priority="1">';
        $xml_str .= '<Objects>';
        $xml_str .= '<Object ElementType="Program" ID="' .$program_id. '" Action="' . $action . '" Code="' .$program_id. '">';
        $xml_str .= '</Object>';
        $xml_str .= '</Objects>';
        $xml_str .= '</ADI>';
    
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'program' .'_'. $action . '_' . $three_rand. '.xml';
    
        $c2_info = array(
            'nns_task_type'=>'Program',
            'nns_task_id'=> isset($params['nns_task_id'])?$params['nns_task_id']:null,
            'nns_task_name'=> isset($params['nns_task_name'])?$params['nns_task_name']:null,
            'nns_action'=>	$action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            'nns_desc' => 'Program,'.$action,
        );
        return self::execute_c2($c2_info,$sp_id);
    
    }
    /**
     * 此方法不建议使用
     * $params array('nns_ids')
     */
    static public function delete_vods($params){
        $action = 'DELETE';
    
        //$program_id = $params['nns_id'];
        //self::do_vod($vod_info,$action);
    
        $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
        $xml_str .= '<Objects>';
        foreach($params['nns_ids'] as $id){
            $xml_str .= '<Object ElementType="Program" ID="' .$id. '" Action="' . $action . '" Code="' .$id. '">';
            $xml_str .= '</Object>';
        }
    
        $xml_str .= '</Objects>';
        $xml_str .= '</ADI>';
    
        $guid = np_guid_rand();
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'program' .'_'. $action . '_' . $three_rand. '.xml';
        	
        $c2_info = array(
            'nns_id' => $c2_info['c2_id'],
            'nns_task_type'=>'Program',
            'nns_task_id'=> isset($params['nns_task_id'])?$params['nns_task_id']:null,
            'nns_task_name'=> isset($params['nns_task_name'])?$params['nns_task_name']:null,
            'nns_action'=>	$action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            //'nns_result' =>  '['.$result['ret'].']'.$result['desc'],
            'nns_desc' => 'Program,'.$action,
        );
        return self::execute_c2($c2_info);
    
    }
    /**
     * 注入片源（以分集为单位,切片完成后调用）
     * $index_media array(
     * 'nns_video_id'
     * 'nns_video_index_id'
     * 'nns_task_id'
     * 'nns_new_dir'
     * 'nns_media_ids'
     * )
     */
    static public function add_movies_by_vod_index($index_media,$sp_id,$action=null)
    {
        include dirname(dirname(__FILE__)).'/define.php';
        
        //die;
        $db = nn_get_db(NL_DB_WRITE);
        
        
        
        //$sql = "select nns_name from nns_vod_index where nns_id='".$index_media['nns_video_index_id']."'";
        //$nns_task_name= nl_db_get_col($sql,$db);
    
        $sql = "select nns_pinyin from nns_vod where nns_id='".$index_media['nns_video_id']."'";
        $pinyin = nl_db_get_col($sql,$db);
    
        //
        $action = ($action === null) ? 'REGIST' : 'DELETE';
        
        $str_deleted = ($action == 'REGIST') ? 'nns_deleted != 1' : 'nns_deleted = 1';
    
        $sql = "select * from nns_vod_media where {$str_deleted} and nns_vod_id='{$index_media['nns_video_id']}' and nns_vod_index_id='{$index_media['nns_video_index_id']}' and nns_id='{$index_media['nns_media_id']}' order by nns_kbps desc limit 1";
    
        $medias = nl_db_get_all($sql,$db);
        
        
        if(is_array($medias) && count($medias)>0){
            $BizDomain = self::get_BizDomain($sp_id);
            $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="'.$BizDomain.'" Priority="1">';
            	
            	
            $sp_config = sp_model::get_sp_config($sp_id);
            $program_id = $index_media['nns_media_id'];
            if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0){
                $video_info = video_model::get_vod_media_info($index_media['nns_media_id']);
                switch ($sp_config['import_id_mode']) {
                    case '1':
                        $program_id = '002000'.$video_info['nns_integer_id'];
                        $program_id = str_pad($program_id,32,"0");
                        break;
                    case '2':
                        $program_id = $video_info['nns_import_id'];
                        break;
                    case '3':
                        $program_id = CSP_ID . '0002' . $video_info['nns_integer_id'];
                        $program_id = str_pad($program_id,32,"0");
                        break;
                    case '4':
                        $arr_csp_id = explode("|", CSP_ID);
                        $str_lenth = strlen($arr_csp_id[0])+strlen($arr_csp_id[1])+1;
                        $lest_num = 32-$str_lenth;
                        $program_id = $arr_csp_id[0].'3'.str_pad($video_info['nns_integer_id'], $lest_num, "0", STR_PAD_LEFT).$arr_csp_id[1];
                        break;
                }
            }
            $xml_str .= '<Objects>';
            if($sp_id!='hndx' && $sp_id!='hnlt_zx' && $sp_id!='hndx_hw' && $sp_id!='xjcbc_dx_hw' && $sp_id!='xjcbc_dx_zte' && $sp_id!='xjdx_hw' && $sp_id!='cbczq_hw' && $sp_id!='xjdx_zte' && $sp_id!='cbczq_zte')
            {
                	
                $xml_str .= '<Object ElementType="Movie" ID="' .$program_id. '" Action="DELETE" Code="' .$program_id. '">';
                $xml_str .= '</Object>';
            }
            
            
            
            
            $nns_video_index_id = $index_media['nns_video_index_id'];
            if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0){
                $video_info = video_model::get_vod_index_info($index_media['nns_video_index_id']);
                switch ($sp_config['import_id_mode']) {
                    case '1':
                        $nns_video_index_id = '001000'.$video_info['nns_integer_id'];
                        $nns_video_index_id = str_pad($nns_video_index_id,32,"0");
                        break;
                    case '2':
                        $nns_video_index_id = $video_info['nns_import_id'];
                        break;
                    case '3':
                        $nns_video_index_id = CSP_ID . '0001' . $video_info['nns_integer_id'];
                        $nns_video_index_id = str_pad($nns_video_index_id,32,"0");
                        break;
                    case '4':
                        $arr_csp_id = explode("|", CSP_ID);
                        $str_lenth = strlen($arr_csp_id[0])+strlen($arr_csp_id[1])+1;
                        $lest_num = 32-$str_lenth;
                        $nns_video_index_id = $arr_csp_id[0].'2'.str_pad($video_info['nns_integer_id'], $lest_num, "0", STR_PAD_LEFT).$arr_csp_id[1];
                        break;
                }
                	
            }
            
            
            $vod_info = video_model::get_vod_info($index_media['nns_video_id']);
            $xml_str .= '<Object ElementType="Program" ID="' .$nns_video_index_id. '" Action="' . $action . '" Code="' .$nns_video_index_id. '">';
            $xml_str .= '<Property Name="Name">'.htmlspecialchars($vod_info['nns_alias_name'],ENT_QUOTES).'</Property>';
            $xml_str .= '<Property Name="OrderNumber"></Property>';//节目订购编号
            $xml_str .= '<Property Name="OriginalName">'.htmlspecialchars($vod_info['nns_alias_name'],ENT_QUOTES).'</Property>';//原名
            $xml_str .= '<Property Name="SortName"></Property>';//索引名称供界面排序
            $xml_str .= '<Property Name="SearchName">'.$vod_info['nns_pinyin'].'</Property>';//搜索名称供界面搜索
            $xml_str .= '<Property Name="Genre">Genre</Property>';//Program的默认类别（Genre）
            $xml_str .= '<Property Name="ActorDisplay">'.htmlspecialchars($vod_info['nns_actor'],ENT_QUOTES).'</Property>';//演员列表(只供显示)
            $xml_str .= '<Property Name="WriterDisplay">'.htmlspecialchars($vod_info['nns_director'],ENT_QUOTES).'</Property>';//作者列表(只供显示)
            $xml_str .= '<Property Name="OriginalCountry">'.htmlspecialchars($vod_info['nns_area'],ENT_QUOTES).'</Property>';//国家地区
            $xml_str .= '<Property Name="Language">'.htmlspecialchars($vod_info['nns_language'],ENT_QUOTES).'</Property>';//语言
            $show_time = null;
            if($vod_info['nns_index_show_time']=='0000-00-00 00:00:00' || empty($vod_info['nns_index_show_time'])){
                $show_time = $vod_info['nns_show_time'];//影片基本信息上的上映时间
                $show_time = strtotime($vod_info['nns_show_time']);
                if(!$show_time || $show_time==-1)
                {
                    $show_time = date("Y-m-d H:i:s", strtotime("-1 year"));
                }
            }else{
                $show_time = $vod_info['nns_index_show_time'];//分集的上映时间
            }
            $xml_str .= '<Property Name="ReleaseYear">' .  date('Y',strtotime($show_time)).'</Property>';//上映年份(YYYY)
            $xml_str .= '<Property Name="OrgAirDate">' . date('Ymd',strtotime($show_time)).'</Property>';//首播日期(YYYYMMDD)
            $xml_str .= '<Property Name="LicensingWindowStart">'. date('YmdHis').'</Property>';//有效开始时间(YYYYMMDDHH24MiSS)
            $xml_str .= '<Property Name="LicensingWindowEnd">'. date('YmdHis',time()+ 3*365*24*60*60).'</Property>';//有效结束时间(YYYYMMDDHH24MiSS)
            $xml_str .= '<Property Name="DisplayAsNew">0</Property>';//新到天数
            $xml_str .= '<Property Name="DisplayAsLastChance">300</Property>';//剩余天数
            $xml_str .= '<Property Name="Macrovision">0</Property>';//拷贝保护标志0:无拷贝保护	1:有拷贝保护
            $xml_str .= '<Property Name="Description">'.mb_substr(htmlspecialchars($vod_info['nns_summary'],ENT_QUOTES),0,256,'utf-8').'</Property>';//描述信息
            $xml_str .= '<Property Name="PriceTaxIn"></Property>';//列表定价
            $xml_str .= '<Property Name="Status">1</Property>';//状态标志 0:失效 1:生效
            $xml_str .= '<Property Name="SourceType">1</Property>';//1: VOD 5: Advertisement
            $xml_str .= '<Property Name="SeriesFlag">'.$vod_info['nns_series_flag'].'</Property>';//0: 普通VOD 1: 连续剧剧集
            $xml_str .= '<Property Name="Type">MV</Property>';//演职角色名称
            $xml_str .= '<Property Name="Keywords">'.htmlspecialchars($vod_info['nns_keyword'],ENT_QUOTES).'</Property>';//关键字 多个关键字之间使用分号分隔
            $xml_str .= '<Property Name="Tags">1</Property>';//关联标签 多个标签之间使用分号分隔
            $xml_str .= '<Property Name="Reserve1"></Property>';//保留字段
            $xml_str .= '<Property Name="Reserve2"></Property>';//保留字段
            $xml_str .= '<Property Name="Reserve3"></Property>';//保留字段
            $xml_str .= '<Property Name="Reserve4"></Property>';//保留字段
            $xml_str .= '<Property Name="Reserve5"></Property>';//保留字段
            $xml_str .= '</Object>';
            
            
            
            $media = array();
            $media_arr = array();
            	
            $media_url = "";
            foreach($medias as $media_item)
            {
                $mode = strtolower($media_item['nns_mode']);
                if(($sp_id == 'hndx' || $sp_id == 'hnlt' || $sp_id == 'hnlt_zx' || $sp_id!='hndx_hw') && ($mode == '4' || $mode == 'sd'))
                {
                    $mode = 'hd';
                    $media_item['nns_mode'] = 'hd';
                }
                $media_arr[$mode] = $media_item;
    
            }
            	
            //print_r($media_arr);
            if(!empty($media_arr['hd'])){
                $media = $media_arr['hd'];
            }else if(!empty($media_arr['std'])){
                $media = $media_arr['std'];
            }else if(!empty($media_arr['low'])){
                $media = $media_arr['low'];
            }
            if(empty($media)) return false;
            	
            $video_profile =1;
            $mode = strtolower($media['nns_mode']);
            switch($mode){
                case 'std':
                    $video_profile = 1;
                    break;
                case 'hd':
                    $video_profile = 5;
                    break;
                case 'low':
                    $video_profile = 3;
                    break;
            }

           
            if((int)$sp_config['disabled_clip'] !== 2)
            {
                //判断片源的URL
                if(empty($index_media['nns_date'])){
                    $index_media['nns_date'] = nl_db_get_col("select nns_date from nns_mgtvbk_clip_task where nns_id='{$index_media['nns_clip_task_id']}'", $db);
                }
                $url = MOVIE_FTP.$index_media['nns_date'].'/'.$index_media['nns_clip_task_id'].'/'.$pinyin.'-'.$media['nns_vod_index'].'.m3u8';
                if(!empty($index_media['nns_file_path'])){
                    $url = MOVIE_FTP.$index_media['nns_file_path'];
                }
                $bool = ($action == 'REGIST') ? self::check_ftp_file($url) : true;
                $bool = true;
                if($bool===false){
                    DEBUG && i_write_log_core('path_fail----'.$url,$sp_id.'/path_fail');
                    nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=-1 where nns_id='{$index_media['nns_task_id']}'", $db);
                    include_once 'queue_task_model.php';
                    $queue_task_model = new queue_task_model();
    
                    $queue_task_model->q_re_clip($index_media['nns_task_id']);
                    //$queue_task_model->q_add_task($index_media['nns_media_id'], 'media', 'add',$sp_id,null,true);
                    $queue_task_model=null;
                    return false;
                }
            }
            else
            {
                $url = $medias[0]['nns_url'];
            }
            $xml_str .= '<Object ElementType="Movie" ID="' .$program_id. '" Action="' . $action . '" Code="' .$program_id. '" >';
            $xml_str .= '<Property Name="Type">1</Property>';//媒体类型1:正片 2:预览片
            $xml_str .= '<Property Name="FileURL">' .$url. '</Property>';//媒体文件URL,ftp://username:password@ip:port/...  标准FTP协议
            $xml_str .= '<Property Name="SourceDRMType">1</Property>';//0: No DRM 1: BES DRM
            $xml_str .= '<Property Name="DestDRMType">1</Property>';//
            $xml_str .= '<Property Name="AudioType">1</Property>';//0: 其他 1: Monaural 单声道 2: Stereo 多声道 3: Two-nation monaural 双单声道 4: Two-nation stereo 双多声道  5: AC3(5:1 channel) AC3声道
            $xml_str .= '<Property Name="ClosedCaptioning">1</Property>';//0:无字幕 1:有字幕
            if($sp_id!='hndx' && $sp_id!='hnlt_zx' && $sp_id!='hndx_hw' && $sp_id!='xjcbc_dx_hw' && $sp_id!='xjcbc_dx_zte' && $sp_id!='xjdx_hw' && $sp_id!='cbczq_hw' && $sp_id!='xjdx_zte' && $sp_id!='cbczq_zte')
            {
                $xml_str .= '<Property Name="OCSURL"></Property>';//在海量存储中的视频URL，类似Rtsp://ip:port/1/2/3.ts
                $xml_str .= '<Property Name="Duration"></Property>';//播放时长HHMISSFF （时分秒帧）
                $xml_str .= '<Property Name="FileSize">1024</Property>';//文件大小，单位为Byte
                $xml_str .= '<Property Name="BitRateType">1</Property>';//码流;1:1.6Mbps 2:2.1Mbps 3:8Mbps 4:60kbps 5:150kbps 6:420kbps 7:750kbps
                $xml_str .= '<Property Name="VideoType">1</Property>';//编码格式： 1:H.264 2:MPEG4 3:AVS 4:MPEG2 5:MP3 6:WMV
                $xml_str .= '<Property Name="AudioFormat">1</Property>';//编码格式：1.	MP2 2.	AAC 3.	AMR
                $xml_str .= '<Property Name="Resolution">1</Property>';//分辨率类型：1:QCIF 2:QVGA 3:2/3 D1 4:3/4 D1 5:D1 6:720P 7:1080i 8:1080P
                $xml_str .= '<Property Name="VideoProfile">'.$video_profile.'</Property>';//1:Simple 2:Advanced Simple 3:Baseline 4:Main 5:High 6:JiZhun
                $xml_str .= '<Property Name="SystemLayer">1</Property>';//1:TS 2:3GP
                $xml_str .= '<Property Name="ServiceType">0x01</Property>';//服务类型:0x01：在线播放(默认) 0x10：支持下载 0X11:  在线播放+下载
            }
            $xml_str .= '</Object>';
            // break;
            //}
            $xml_str .= '</Objects>';
            	
            //foreach($medias as $media){
            
            //if($sp_id!='hndx'){
            $xml_str .= '<Mappings>';
            $xml_str .= '<Mapping ParentType="Program" ParentID="'.$nns_video_index_id.'" ElementType="Movie" ElementID="'.$program_id.'" ParentCode="'.$nns_video_index_id.'" ElementCode="'.$program_id.'" Action="'.$action.'">';
            $xml_str .= '</Mapping>';
            //}
            $xml_str .= '</Mappings>';
            //}
            $xml_str .= '</ADI>';
            	
            //die($xml_str);
            $guid = np_guid_rand();
            $time = date('YmdHis') . rand(0, 10000);
            $three_rand = rand(100, 999);
            $file_name = 'zgcms_'. $time .'_' . 'vod_media' .'_'. $action . '_' . $three_rand. '.xml';
    
    
            $c2_info = array(
                //'nns_id' => $c2_info['c2_id'],
                'nns_task_type'=>'Movie',
                'nns_task_id'=> isset($index_media['nns_task_id'])?$index_media['nns_task_id']:null,
                'nns_task_name'=> isset($index_media['nns_task_name'])?$index_media['nns_task_name']:null,
                'nns_action'=>	$action,
                'nns_url' => $file_name,
                'nns_content' => $xml_str,
                //'nns_result' =>  '['.$result['ret'].']'.$result['desc'],
                'nns_desc' => 'Movie,'.$action,
            );
            	
            if((int)$sp_config['disabled_clip'] !== 2)
            {
                //修改切片的状态
    
                $clip_task_id = $index_media['nns_clip_task_id'];
    
    
    
                //更改状态
                $dt = date('Y-m-d H:i:s');
                $set_state = array(
                    'nns_state' => clip_task_model::TASK_C_STATE_C2_HANDLE,
                    'nns_desc' => clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_C2_HANDLE],
                    'nns_modify_time'=>$dt,
                );
                clip_task_model::update_task($clip_task_id, $set_state);
    
    
                //任务日志
                $task_log = array(
                    'nns_task_id' => $clip_task_id,
                    'nns_state'   => clip_task_model::TASK_C_STATE_C2_HANDLE,
                    'nns_desc'	=> clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_C2_HANDLE],
                );
                clip_task_model::add_task_log($task_log);
            }
            	
            	
            return self::execute_c2($c2_info,$sp_id);
    
        }else{
            DEBUG && i_write_log_core('--没有片源--');
            nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$index_media['nns_task_id']}'", $db);
        }
    
    }
    
    static public function _do_movie($index_media,$sp_id,$action,$db)
    {
        $str_mapping = $str_object='';
        $sql = "select nns_pinyin from nns_vod where nns_id='".$index_media['nns_video_id']."'";
        $pinyin = nl_db_get_col($sql,$db);
        $sql = "select * from nns_vod_media where nns_deleted != 1 and nns_vod_id='{$index_media['nns_video_id']}' and nns_vod_index_id='{$index_media['nns_video_index_id']}' and nns_id='{$index_media['nns_media_id']}' order by nns_kbps desc limit 1";
        $medias = nl_db_get_all($sql,$db);
    
        if(!is_array($medias) || count($medias)<1){
            nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$index_media['nns_task_id']}'", $db);
            return array(
                'str_object'=>$str_object,
                'str_mapping'=>$str_mapping
            );
        }
        $sp_config = sp_model::get_sp_config($sp_id);
        $program_id = $index_media['nns_media_id'];
        if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0){
            $video_info = video_model::get_vod_media_info($index_media['nns_media_id']);
            switch ($sp_config['import_id_mode']) {
                case '1':
                    $program_id = '002000'.$video_info['nns_integer_id'];
                    $program_id = str_pad($program_id,32,"0");
                    break;
                case '2':
                    $program_id = $video_info['nns_import_id'];
                    break;
                case '3':
                    $program_id = CSP_ID . '0002' . $video_info['nns_integer_id'];
                    $program_id = str_pad($program_id,32,"0");
                    break;
                case '4':
                    $arr_csp_id = explode("|", CSP_ID);
                    $str_lenth = strlen($arr_csp_id[0])+strlen($arr_csp_id[1])+1;
                    $lest_num = 32-$str_lenth;
                    $program_id = $arr_csp_id[0].'3'.str_pad($video_info['nns_integer_id'], $lest_num, "0", STR_PAD_LEFT).$arr_csp_id[1];
                    break;
            }
        }
    
        	
        $media = array();
        $media_arr = array();
        $media_url = "";
        foreach($medias as $media_item)
        {
            $mode = strtolower($media_item['nns_mode']);
            if(($sp_id == 'hndx' || $sp_id == 'hnlt' || $sp_id == 'hnlt_zx' || $sp_id!='hndx_hw') && ($mode == '4' || $mode == 'sd'))
            {
                $mode = 'hd';
                $media_item['nns_mode'] = 'hd';
            }
            $media_arr[$mode] = $media_item;
        }
        if(!empty($media_arr['hd'])){
            $media = $media_arr['hd'];
        }else if(!empty($media_arr['std'])){
            $media = $media_arr['std'];
        }else if(!empty($media_arr['low'])){
            $media = $media_arr['low'];
        }
        if(empty($media))
        {
            return array(
                'str_object'=>$str_object,
                'str_mapping'=>$str_mapping
            );
        }
        $video_profile =1;
        $mode = strtolower($media['nns_mode']);
        switch($mode){
            case 'std':
                $video_profile = 1;
                break;
            case 'hd':
                $video_profile = 5;
                break;
            case 'low':
                $video_profile = 3;
                break;
        }
        if((int)$sp_config['disabled_clip'] !== 2)
        {
            //判断片源的URL
            if(empty($index_media['nns_date'])){
                $index_media['nns_date'] = nl_db_get_col("select nns_date from nns_mgtvbk_clip_task where nns_id='{$index_media['nns_clip_task_id']}'", $db);
            }
            $url = MOVIE_FTP.$index_media['nns_date'].'/'.$index_media['nns_clip_task_id'].'/'.$pinyin.'-'.$media['nns_vod_index'].'.m3u8';
            if(!empty($index_media['nns_file_path'])){
                $url = MOVIE_FTP.$index_media['nns_file_path'];
            }
            $bool = self::check_ftp_file($url);
            if($bool===false)
            {
                DEBUG && i_write_log_core('path_fail----'.$url,$sp_id.'/path_fail');
                nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=-1 where nns_id='{$index_media['nns_task_id']}'", $db);
                include_once 'queue_task_model.php';
                $queue_task_model = new queue_task_model();
                $queue_task_model->q_re_clip($index_media['nns_task_id']);
                $queue_task_model=null;
                return array(
                    'str_object'=>$str_object,
                    'str_mapping'=>$str_mapping
                );
            }
        }
        else
        {
            $url = $medias[0]['nns_url'];
        }
        $str_object .= '<Object ElementType="Movie" ID="' .$program_id. '" Action="' . $action . '" Code="' .$program_id. '" >';
        $str_object .= '<Property Name="Type">1</Property>';//媒体类型1:正片 2:预览片
        $str_object .= '<Property Name="FileURL">' .$url. '</Property>';//媒体文件URL,ftp://username:password@ip:port/...  标准FTP协议
        $str_object .= '<Property Name="SourceDRMType">1</Property>';//0: No DRM 1: BES DRM
        $str_object .= '<Property Name="DestDRMType">1</Property>';//
        $str_object .= '<Property Name="AudioType">1</Property>';//0: 其他 1: Monaural 单声道 2: Stereo 多声道 3: Two-nation monaural 双单声道 4: Two-nation stereo 双多声道  5: AC3(5:1 channel) AC3声道
        $str_object .= '<Property Name="ClosedCaptioning">1</Property>';//0:无字幕 1:有字幕
        $str_object .= '</Object>';
    
        $nns_video_index_id = $index_media['nns_video_index_id'];
        if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0){
            $video_info = video_model::get_vod_index_info($index_media['nns_video_index_id']);
            switch ($sp_config['import_id_mode']) {
                case '1':
                    $nns_video_index_id = '001000'.$video_info['nns_integer_id'];
                    $nns_video_index_id = str_pad($nns_video_index_id,32,"0");
                    break;
                case '2':
                    $nns_video_index_id = $video_info['nns_import_id'];
                    break;
                case '3':
                    $nns_video_index_id = CSP_ID . '0001' . $video_info['nns_integer_id'];
                    $nns_video_index_id = str_pad($nns_video_index_id,32,"0");
                    break;
                case '4':
                    $arr_csp_id = explode("|", CSP_ID);
                    $str_lenth = strlen($arr_csp_id[0])+strlen($arr_csp_id[1])+1;
                    $lest_num = 32-$str_lenth;
                    $nns_video_index_id = $arr_csp_id[0].'2'.str_pad($video_info['nns_integer_id'], $lest_num, "0", STR_PAD_LEFT).$arr_csp_id[1];
                    break;
            }
            	
        }
        $str_mapping .= '<Mapping ParentType="Program" ParentID="'.$nns_video_index_id.'" ElementType="Movie" ElementID="'.$program_id.'" ParentCode="'.$nns_video_index_id.'" ElementCode="'.$program_id.'" Action="'.$action.'">';
        $str_mapping .= '</Mapping>';
        return array(
            'str_object'=>$str_object,
            'str_mapping'=>$str_mapping
        );
    }
    
    static public function  add_movies_by_clip_task($task_id){
    
        $db = nn_get_db(NL_DB_READ);
        $task_info = clip_task_model::get_task_by_id($task_id);
        if(empty($task_info['nns_content'])){
            $task_info['nns_content'] = clip_task_model::_build_task_content($task_info);
        }
    
        $dom	= new DOMDocument('1.0', 'utf-8');
        $dom -> loadXML($task_info['nns_content']);
        $xpath = new DOMXPath($dom);
    
        $nns_vod_id = $xpath->query('/task/video')->item(0)->getAttribute('id');
        $nns_vod_index_id = $xpath->query('/task/video/index_list/index')->item(0)->getAttribute('id');
    
        $nns_media_ids = array();
        $entries = $xpath->query('/task/video/index_list/index/media_list/media');
        foreach($entries as $item){
            $nns_media_ids[] = $item->getAttribute('id');
        }
        $media_info['nns_video_id'] = $nns_vod_id;
        $media_info['nns_video_index_id'] = $nns_vod_index_id;
        $media_info['nns_media_ids'] = $nns_media_ids;
        $media_info['nns_task_id'] = $task_id;
        $media_info['nns_new_dir'] = isset($task_info['nns_new_dir']) ? (empty($task_info['nns_new_dir']) ? false : true) : false;
        $media_info['nns_date'] = empty($task_info['nns_date'])?'':$task_info['nns_date'];
    
        $index_name = nl_db_get_col("select nns_name from nns_vod_index where nns_id='".$nns_vod_index_id."'",$db);
        //判断是否曾注入成功，是则先删除后重新注入
        $sql = "select * from nns_mgtvbk_clip_task_log where nns_video_index_id='".$nns_vod_index_id."' and nns_state='c2_ok' limit 1";
        $result = nl_query_by_db($sql,$db);
        if(!is_bool($result)){
            DEBUG && i_write_log_core('--注入片源前，先执行删除指令--');
            //先删除再重新注入
            $_params_del = array(
                'nns_ids'=>$nns_media_ids,
                'nns_task_id'=> $task_id,
                'nns_task_name'=> $index_name,
            );
            self::delete_movies($_params_del);
        }
        return self::add_movies_by_vod_index($media_info);
    }
    /**
     *   该方法仅供测试用
     */
    static public function test_add_movies_by_vod_index($index_media){
        DEBUG && i_write_log_core('--注入片源--');
        $action = 'REGIST';
        $db = nn_get_db(NL_DB_READ);
        //$db->open();
        //先注入vod及index;否则，注入的片源将没有绑定任何影片
        $sql = "select nns_id from nns_mgtvbk_c2_task " .
            "where nns_ref='".$index_media['nns_video_id']."'" .
            " and nns_action='".SP_ORG_ID."'" .
            " and nns_type='vod' and nns_status>0 limit 1";
        $result = nl_db_get_col($sql,$db);
        if($result != false){
            DEBUG && i_write_log_core('--注入片源时，注入影片内容--');
            include_once dirname(__FILE__).'/content_task_model.php';
            content_task_model::vod($result);
        }
        $sql = "select nns_pinyin from nns_vod where nns_id='".$index_media['nns_video_id']."'";
        $pinyin = nl_db_get_col($sql,$db);
    
        $sql = "select * from nns_vod_media where nns_deleted != 1 and ".nl_db_in($index_media['nns_media_ids'],'nns_id');
        $medias = nl_db_get_all($sql,$db);
        if($medias === false ) {
            DEBUG && i_write_log_core('--取片源出错--'."\n".var_export($medias,true)."\n".$sql);
            return false;
        }
        if(is_array($medias) && count($medias)>0){
            $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
            $xml_str .= '<Objects>';
            foreach($medias as $media){
                //$url = 'ftp://username:password@ip:port/'.$index_media['nns_vod_index_id'].'/'.$media['nns_id'].'.m3u8';
                $video_profile =1;
                $mode = strtolower($media['nns_mode']);
                switch($mode){
                    case 'std':
                        $video_profile = 1;
                        break;
                    case 'hd':
                        $video_profile = 5;
                        break;
                    case 'low':
                        $video_profile = 3;
                        break;
                }
                //$kbps_index = clip_task_model::get_kbps_index($media['nns_mode']);
                if($pinyin){
                    $url = MOVIE_FTP.$index_media['nns_date'].'/'.$media['nns_vod_index_id'].'/'.$pinyin.'-'.$media['nns_vod_index'].'.m3u8';
                }else{
                    $url = MOVIE_FTP.$index_media['nns_date'].'/'.$media['nns_vod_index_id'].'/index.m3u8';
                }
                $xml_str .= '<Object ElementType="Movie" ID="' .$media['nns_id']. '" Action="' . $action . '" Code="' .$media['nns_id']. '" >';
                $xml_str .= '<Property Name="Type">1</Property>';//媒体类型1:正片 2:预览片
                $xml_str .= '<Property Name="FileURL">' .$url. '</Property>';//媒体文件URL,ftp://username:password@ip:port/...  标准FTP协议
                $xml_str .= '<Property Name="SourceDRMType">1</Property>';//0: No DRM 1: BES DRM
                $xml_str .= '<Property Name="DestDRMType">1</Property>';//
                $xml_str .= '<Property Name="AudioType">1</Property>';//0: 其他 1: Monaural 单声道 2: Stereo 多声道 3: Two-nation monaural 双单声道 4: Two-nation stereo 双多声道  5: AC3(5:1 channel) AC3声道
                $xml_str .= '<Property Name="ClosedCaptioning">1</Property>';//0:无字幕 1:有字幕
                $xml_str .= '<Property Name="OCSURL"></Property>';//在海量存储中的视频URL，类似Rtsp://ip:port/1/2/3.ts
                $xml_str .= '<Property Name="Duration"></Property>';//播放时长HHMISSFF （时分秒帧）
                $xml_str .= '<Property Name="FileSize">1024</Property>';//文件大小，单位为Byte
                $xml_str .= '<Property Name="BitRateType">1</Property>';//码流;1:1.6Mbps 2:2.1Mbps 3:8Mbps 4:60kbps 5:150kbps 6:420kbps 7:750kbps
                $xml_str .= '<Property Name="VideoType">1</Property>';//编码格式： 1:H.264 2:MPEG4 3:AVS 4:MPEG2 5:MP3 6:WMV
                $xml_str .= '<Property Name="AudioFormat">1</Property>';//编码格式：1.	MP2 2.	AAC 3.	AMR
                $xml_str .= '<Property Name="Resolution">1</Property>';//分辨率类型：1:QCIF 2:QVGA 3:2/3 D1 4:3/4 D1 5:D1 6:720P 7:1080i 8:1080P
                $xml_str .= '<Property Name="VideoProfile">'.$video_profile.'</Property>';//1:Simple 2:Advanced Simple 3:Baseline 4:Main 5:High 6:JiZhun
                $xml_str .= '<Property Name="SystemLayer">1</Property>';//1:TS 2:3GP
                $xml_str .= '<Property Name="ServiceType">0x01</Property>';//服务类型:0x01：在线播放(默认) 0x10：支持下载 0X11:  在线播放+下载
                $xml_str .= '</Object>';
                break;
            }
            $xml_str .= '</Objects>';
            /*
             $xml_str .= '<Mappings>';
             foreach($medias as $media){
             //$xml_str .= '<Mapping ParentType="Program" ParentID="'.$index_media['nns_video_index_id'].'" ElementType="Movie" ElementID="'.$media['nns_id'].'" ParentCode="'.$index_media['nns_video_index_id'].'" ElementCode="'.$media['nns_id'].'" Action="'.$action.'">';
             $xml_str .= '<Mapping ParentType="Program" ParentID="123" ElementType="Movie" ElementID="'.$media['nns_id'].'" ParentCode="123" ElementCode="'.$media['nns_id'].'" Action="'.$action.'">';
             $xml_str .= '</Mapping>';
             }
             $xml_str .= '</Mappings>';
             */
            $xml_str .= '</ADI>';
    
            $time = date('YmdHis') . rand(0, 10000);
            $three_rand = rand(100, 999);
            $file_name = 'zgcms_'. $time .'_' . 'vod_media' .'_'. $action . '_' . $three_rand. '.xml';
    
            //
            $c2_info = array(
                //'nns_id' => $c2_info['c2_id'],
                'nns_task_type'=>'Movie',
                'nns_task_id'=> isset($index_media['nns_task_id'])?$index_media['nns_task_id']:null,
                'nns_task_name'=> isset($index_media['nns_task_name'])?$index_media['nns_task_name']:null,
                'nns_action'=>	$action,
                'nns_url' => $file_name,
                'nns_content' => $xml_str,
                'nns_result' =>  '['.$result['ret'].']'.$result['desc'],
                'nns_desc' => 'Movie,'.$action,
            );
            return self::execute_c2($c2_info);
    
        }else{
            DEBUG && i_write_log_core('--没有片源--');
        }
    
    }
    /**
     * 添加影片
     */
    static public function add_movie($media){
        $action = 'REGIST';
        return self::do_movie($media,$action);
    }
    /**
     * 删除影片
     * params array(
     * nns_id,
     * nns_task_id
     * )
     */
    static public function delete_movie($val,$sp_id)
    {
        $db = nn_get_db(NL_DB_READ);
        //检查列队中现在有多少空余的可以注入
        
        $sp_info = sp_model::get_sp_config($sp_id);
        $task_arr = nl_db_get_one("select * from nns_mgtvbk_c2_task where nns_id='{$val['nns_task_id']}'", $db);
        $vod_id_arr = nl_db_get_one("select nns_vod_id,nns_id from nns_vod_index where nns_id='{$task_arr['nns_src_id']}'", $db);
		$clip = true;
		$vod_id = $vod_id_arr['nns_vod_id'];
		$vod_index_id = $vod_id_arr['nns_id'];
        $data = array(
            'nns_video_id'=>$vod_id,
            'nns_video_index_id'=>$task_arr['nns_src_id'],
            'nns_clip_task_id'=>$task_arr['nns_clip_task_id'],
            'nns_task_id' => $task_arr['nns_id'],
            'nns_clip_date'=>$task_arr['nns_clip_date'],
            'nns_media_id'=>$task_arr['nns_ref_id'],
            'nns_date'=>$task_arr['nns_clip_date'],
            'nns_file_path'=>$task_arr['nns_file_path'],
            'nns_file_size'=>$task_arr['nns_file_size'],
            'nns_file_md5'=>$task_arr['nns_file_md5'],
            'clip'=>$clip,
            'nns_task_name'=>$task_arr['nns_name'],
            'nns_action'=>$task_arr['nns_action'],
        );
        return self::add_movies_by_vod_index($data, $sp_id,'DELETE');
    }
    /**
     * 使用本方法时，请以分集为单位
     * $params array(
     * nns_ids
     * )
     */
    static public function delete_movies($params,$sp_id){
        $action = 'DELETE';
    
        $BizDomain = self::get_BizDomain($sp_id);
        //
        //return self::do_movie($media,$action);
        $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="'.$BizDomain.'" Priority="1">';
        $xml_str .= '<Objects>';
        if(empty($params['nns_ids'] )) return ;
        foreach($params['nns_ids']  as $nns_id)	{
            $xml_str .= '<Object ElementType="Movie" ID="' .$nns_id. '" Action="' . $action . '" Code="' .$nns_id. '">';
            $xml_str .= '</Object>';
        }
    
        $xml_str .= '</Objects>';
        $xml_str .= '</ADI>';
    
        $guid =  np_guid_rand();
    
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'movie' .'_'. $action . '_' . $three_rand. '.xml';
    
    
        $c2_info = array(
            //'nns_id' => $c2_info['c2_id'],
            'nns_task_type'=>'Movie',
            'nns_task_id'=>  isset($params['nns_task_id'])?$params['nns_task_id']:null,
            'nns_task_name'=>  isset($params['nns_task_name'])?$params['nns_task_name']:null,
            'nns_action'=>	$action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            //'nns_result' =>  '['.$result['ret'].']'.$result['desc'],
            'nns_desc' => 'Movie,'.$action,
        );
        return self::execute_c2($c2_info,$sp_id);
    
    }
    
    //直播
    static public function add_live($live_info,$sp_id){
    
        if(file_exists(dirname(dirname(__FILE__)).'/'.$sp_id.'/models/child_c2_task_model.php')){
            include_once dirname(dirname(__FILE__)).'/'.$sp_id.'/models/child_c2_task_model.php';
            if(method_exists('child_c2_task_model','add_live')){
                $re = child_c2_task_model::add_live($live_info,$sp_id);
                return $re;
            }
        }
    
    
        $action = 'REGIST';
        //
        return self::do_live($live_info,$action,$sp_id);
    }
    static public function update_live($live_info,$sp_id){
    
        if(file_exists(dirname(dirname(__FILE__)).'/'.$sp_id.'/models/child_c2_task_model.php')){
            include_once dirname(dirname(__FILE__)).'/'.$sp_id.'/models/child_c2_task_model.php';
            if(method_exists('child_c2_task_model','update_live')){
                $re = child_c2_task_model::update_live($live_info,$sp_id);
                return $re;
            }
        }
    
        $action = 'UPDATE';
        //
        return self::do_live($live_info,$action,$sp_id);
    }
    /**
     * params
     */
    static public function delete_live($params,$sp_id){
    
    
        if(file_exists(dirname(dirname(__FILE__)).'/'.$sp_id.'/models/child_c2_task_model.php')){
            include_once dirname(dirname(__FILE__)).'/'.$sp_id.'/models/child_c2_task_model.php';
            if(method_exists('child_c2_task_model','delete_live')){
                $re = child_c2_task_model::delete_live($live_info,$sp_id);
                return $re;
            }
        }
    
        $action = 'DELETE';
        $live_id = $params['nns_id'];
        //
        //return self::do_movie($media,$action);
        $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
        $xml_str .= '<Objects>';
        $xml_str .= '<Object ElementType="Channel" ID="' .$live_id. '" Action="' . $action . '" Code="' .$live_id. '">';
        $xml_str .= '</Object>';
        $xml_str .= '</Objects>';
        $xml_str .= '</ADI>';
    
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'channel' .'_'. $action . '_' . $three_rand. '.xml';
    
    
        $c2_info = array(
            //'nns_id' => $c2_info['c2_id'],
            'nns_task_type'=>'Channel',
            'nns_task_id'=> isset($params['nns_task_id'])?$params['nns_task_id']:null,
            'nns_task_name'=> isset($params['nns_task_name'])?$params['nns_task_name']:null,
            'nns_action'=>	$action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            //'nns_result' =>  '['.$result['ret'].']'.$result['desc'],
            'nns_desc' => 'Channel,'.$action,
        );
        $db = nn_get_db(NL_DB_READ);
        $sql = "select * from nns_live_media where   nns_live_id='{$live_id}' and nns_deleted=0";
        $live_media = nl_db_get_all($sql, $db);
        foreach ($live_media as $key => $value) {
            $value['nns_task_name'] = $params['nns_task_name'];
            $value['nns_task_id'] = $params['nns_id'];
            self::delete_live_media($value,$action,$sp_id);
        }
        return self::execute_c2($c2_info,$sp_id);
    }
    
    static public function do_live($live_info,$action,$sp_id){
        $db = nn_get_db(NL_DB_READ);
        //查询直播ID
        $sql_str='select nns_value from nns_live_ex where nns_live_id="'.$live_info['nns_id'].'" and  nns_key="pid"';
        $live_info['nns_name_pid'] = nl_db_get_col($sql_str, $db);
        $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
        $xml_str .= '<Objects>';
        $xml_str .= '<Object ElementType="Channel" ID="' .$live_info['nns_id']. '" Action="' . $action . '" Code="' .$live_info['nns_id']. '">';
        $xml_str .= '<Property Name="ChannelNumber"></Property>';//建议频道号,可以不填
        $xml_str .= '<Property Name="Name">'.$live_info['nns_name'].'</Property>';//频道名称
        $xml_str .= '<Property Name="CallSign">'.$live_info['nns_name_pid'].'</Property>';//台标名称
        $xml_str .= '<Property Name="TimeShift">1</Property>';//时移标志,0:不生效 1:生效
        $xml_str .= '<Property Name="StorageDuration">1</Property>';
        $xml_str .= '<Property Name="TimeShiftDuration">1</Property>';
        $xml_str .= '<Property Name="Description">1</Property>';//描述信息
        $xml_str .= '<Property Name="Country">1</Property>';
        $xml_str .= '<Property Name="State"></Property>';
        $xml_str .= '<Property Name="City"></Property>';
        $xml_str .= '<Property Name="ZipCode"></Property>';
        $xml_str .= '<Property Name="Type">1</Property>';//1:直播频道
        $xml_str .= '<Property Name="SubType"></Property>';
        $xml_str .= '<Property Name="Language"></Property>';
        $xml_str .= '<Property Name="Status">1</Property>';//状态标志,0:失效 1:生效
        $xml_str .= '<Property Name="StartTime">0001</Property>';
        $xml_str .= '<Property Name="EndTime">2359</Property>';
        $xml_str .= '<Property Name="Macrovision"></Property>';
        $xml_str .= '<Property Name="VideoType"></Property>';
        $xml_str .= '<Property Name="AudioType"></Property>';
        $xml_str .= '<Property Name="StreamType"></Property>';
        $xml_str .= '<Property Name="Bilingual"></Property>';
        $xml_str .= '<Property Name="URL"></Property>';//Web频道入口地址,当type=2时，这个属性必填
        $xml_str .= '</Object>';
        $xml_str .= '</Objects>';
        if($sp_id=='jllt'){
            $live_info['nns_category_id'] = array(
                '30000008',
                '30000007'
            );
        }
        if(!empty($live_info['nns_category_id'])){
            $xml_str .= '<Mappings>';
            if(is_array($live_info['nns_category_id'])){
                foreach($live_info['nns_category_id'] as $nns_category_id){
                    $xml_str .= '<Mapping ParentType="Category" ParentID="'.$nns_category_id.'" ElementType="Channel" ElementID="'.$live_info['nns_id'].'" ParentCode="'.$nns_category_id.'" ElementCode="'.$live_info['nns_id'].'" Action="REGIST">';
                    $xml_str .= '</Mapping>';
                }
            }else if(is_string($live_info['nns_category_id'])){
                $xml_str .= '<Mapping ParentType="Category" ParentID="'.$live_info['nns_category_id'].'" ElementType="Channel" ElementID="'.$live_info['nns_id'].'" ParentCode="'.$live_info['nns_category_id'].'" ElementCode="'.$live_info['nns_id'].'" Action="REGIST">';
                $xml_str .= '</Mapping>';
            }
            if($sp_id=='jllt'  || $sp_id=='nmgutl' ){
                $xml_str .= '<Mapping ParentType="Package" ParentID="Channel" ElementType="Channel" ElementID="'.$live_info['nns_id'].'" Action="REGIST" ParentCode="Channel" ElementCode="'.$live_info['nns_id'].'" >';
                $xml_str .= '<Property Name="ValidStart">20140101000000</Property>';
                $xml_str .= '<Property Name="ValidEnd">20540101000000</Property>';
                $xml_str .= '</Mapping>';
            }
    
            $xml_str .= '</Mappings>';
        }
        $xml_str .= '</ADI>';
    
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'live' .'_'. $action . '_' . $three_rand. '.xml';
    
        $c2_info = array(
            //'nns_id' => $c2_info['c2_id'],
            'nns_task_type'=>'Channel',
            'nns_task_id'=> isset($live_info['nns_task_id'])?$live_info['nns_task_id']:null,
            'nns_task_name'=> isset($live_info['nns_task_name'])?$live_info['nns_task_name']:null,
            'nns_action'=>	$action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            //'nns_result' =>$result,
            'nns_desc' => 'Channel,'.$action,
        );
    
        /*
         * @调用live_media//直接进华为命令
        */
        self::execute_c2($c2_info,$sp_id);
        $sql = "select * from nns_live_media where   nns_live_id='{$live_info['nns_id']}' and nns_deleted=0";
        $live_media = nl_db_get_all($sql, $db);
        if(count($live_media)>0&&is_array($live_media)){
            self::add_live_media($live_media, $action,$live_info['nns_id'],$live_info['nns_name'],$sp_id);
        }
        return true;
    
    }
    /*
     * 节目单
     * @params array(
     * 'nns_live_id',
     * 'nns_task_id'
     * )
     */
    static public function add_playbill($item){
        $action = 'REGIST';
    
        $live_id = $item['nns_live_id'];
        $item['nns_task_name'] = $item['nns_name'];
        //$db = nn_get_db(NL_DB_WRITE);
        //$db->open();
        //$sql = "select * from nns_live_playbill_item where nns_live_id='$live_id'";
        //$result = nl_db_get_all($sql,$db);
        //if(is_array($result) && count($result)>0){
        $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
        $xml_str .= '<Objects>';
        //foreach($result as $item){
        $xml_str .= '<Object ElementType="Schedule" ID="' .$item['nns_id']. '" Action="' . $action . '" Code="' .$item['nns_id']. '">';
        $xml_str .= '<Property Name="ChannelID">'.$item['nns_live_id'].'</Property>';
        $xml_str .= '<Property Name="ChannelCode">'.$item['nns_live_id'].'</Property>';
        $xml_str .= '<Property Name="ProgramName">'.$item['nns_name'].'</Property>';
    
        $xml_str .= '<Property Name="SearchName">'.$item['nns_name'].'</Property>';
        $xml_str .= '<Property Name="Genre">Genre</Property>';
        $xml_str .= '<Property Name="SourceType"></Property>';
        $xml_str .= '<Property Name="StartDate">'.date('Ymd',strtotime($item['nns_begin_time'])).'</Property>';
        $xml_str .= '<Property Name="StartTime">'.date('His',$item['nns_begin_time']).'</Property>';
        $xml_str .= '<Property Name="Duration">'.date('His',$item['nns_time_len']).'</Property>';
        $xml_str .= '<Property Name="Status">1</Property>';
        $xml_str .= '<Property Name="Description">'.$item['nns_summary'].'</Property>';
    
        $xml_str .= '<Property Name="ObjectType"></Property>';
        $xml_str .= '<Property Name="ObjectCode"></Property>';
    
        $xml_str .= '</Object>';
        //}
        $xml_str .= '</Objects>';
        $xml_str .= '</ADI>';
    
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'schedule' .'_'. $action . '_' . $three_rand. '.xml';
    
        $c2_info = array();
        $c2_info['c2_id'] = np_guid_rand();
        $c2_info['file_name'] = $file_name;
        $c2_info['xml_content'] = $xml_str;
    
        $c2_info = array(
            //'nns_id' => $c2_info['c2_id'],
            'nns_task_type'=>'Schedule',
            'nns_task_id'=> isset($live_info['nns_task_id'])?$live_info['nns_task_id']:null,
            'nns_task_name'=> isset($live_info['nns_task_name'])?$live_info['nns_task_name']:null,
            'nns_action'=>	$action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            //'nns_result' =>$result,
            'nns_desc' => 'Schedule,'.$action,
        );
        return self::execute_c2($c2_info);
        //}else{
        //	DEBUG && i_write_log_core('直播(id:'.$live_id.')没有节目单');
        //}
        //return false;
    }
    static public function update_playbill($playbill){
        $action = 'UPDATE';
        //
        return self::do_playbill($playbill,$action);
    }
    
    static public function delete_playbill($playbill){
        $action = 'DELETE';
        //
        return self::do_delete_playbill($playbill,$action);
    }
    
    /**
     * params
     */
    static public function do_delete_playbill($params){
        $action = 'DELETE';
        $playbill_id = $params['nns_id'];
        //return self::do_movie($media,$action);
        $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
        $xml_str .= '<Objects>';
        $xml_str .= '<Object ElementType="Schedule" ID="' .$playbill_id. '" Action="' . $action . '" Code="' .$playbill_id. '">';
        $xml_str .= '</Object>';
        $xml_str .= '</Objects>';
        $xml_str .= '</ADI>';
    
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'schedule' .'_'. $action . '_' . $three_rand. '.xml';
    
        $c2_info = array(
            //'nns_id' => $c2_info['c2_id'],
            'nns_task_type'=>'Schedule',
            'nns_task_id'=> isset($params['nns_task_id'])?$params['nns_task_id']:null,
            'nns_task_name'=> isset($params['nns_task_name'])?$params['nns_task_name']:null,
            'nns_action'=>	$action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            //'nns_result' =>  '['.$result['ret'].']'.$result['desc'],
            'nns_desc' => 'Schedule,'.$action,
        );
        return self::execute_c2($c2_info);
    }
    
    static public function do_playbill($playbill,$action){
        $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
        $xml_str .= '<Objects>';
    
        $xml_str .= '<Object ElementType="Schedule" ID="' .$playbill['nns_id']. '" Action="' . $action . '" Code="' .$playbill['nns_id']. '">';
        $xml_str .= '<Property Name="ChannelID"></Property>';
        $xml_str .= '<Property Name="ChannelCode"></Property>';
        $xml_str .= '<Property Name="ProgramName"></Property>';
    
        $xml_str .= '<Property Name="SearchName"></Property>';
        $xml_str .= '<Property Name="Genre"></Property>';
        $xml_str .= '<Property Name="SourceType"></Property>';
        $xml_str .= '<Property Name="StartDate"></Property>';
        $xml_str .= '<Property Name="StartTime"></Property>';
        $xml_str .= '<Property Name="Duration"></Property>';
        $xml_str .= '<Property Name="Status">1</Property>';
        $xml_str .= '<Property Name="Description"></Property>';
    
        $xml_str .= '<Property Name="ObjectType"></Property>';
        $xml_str .= '<Property Name="ObjectCode"></Property>';
    
        $xml_str .= '</Object>';
        /*
         $xml_str .= '<Mappings>';
    
         $xml_str .= '<Mapping ParentType="Program" ParentID="'.$vod_api_id.'" ElementType="Movie" ElementID="'.$media_api_id.'" ParentCode="'.$nns_vod_id.'" ElementCode="'.$nns_media_id.'" Action="'.$action.'">';
         $xml_str .= '</Mapping>';
         	
         $xml_str .= '</Mappings>';
         */
        $xml_str .= '</Objects>';
        $xml_str .= '</ADI>';
    
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'schedule' .'_'. $action . '_' . $three_rand. '.xml';
    
    
        $c2_info = array(
            //'nns_id' => $c2_info['c2_id'],
            'nns_task_type'=>'Schedule',
            'nns_task_id'=> isset($playbill['nns_task_id'])?$playbill['nns_task_id']:null,
            'nns_task_name'=> isset($playbill['nns_task_name'])?$playbill['nns_task_name']:null,
            'nns_action'=>	$action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            //'nns_result' =>$result,
            'nns_desc' => 'Schedule,'.$action,
        );
    
        return self::execute_c2($c2_info);
    }
    
    /**
     * 直播源
     */
    static public function add_live_media($params,$action,$live_id,$live_name,$sp_id){
        $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
        $xml_str .= '<Objects>';
        if(is_array($params) && count($params)>0){
            foreach ($params as $live_media) {
                $xml_str .= '<Object ElementType="PhysicalChannel" ID="' .$live_media['nns_id']. '" Action="' . $action . '" Code="' .$live_media['nns_id']. '">';
                $xml_str .= '<Property Name="ChannelID">'.$live_media['nns_live_id'].'</Property>';//频道ID
                $xml_str .= '<Property Name="ChannelCode">'.$live_media['nns_live_id'].'</Property>';//频道Code
                //$xml_str .= '<Property Name="BitRateType">'.$live_media['nns_kbps'].'</Property>';//码流2: 2M 4: 4M
    
                if(strtoupper($live_media['nns_mode'])=="HD"){
                    $xml_str .= '<Property Name="BitRateType">4</Property>';//码流2: 2M 4: 4M
                }else{
                    $xml_str .= '<Property Name="BitRateType">2</Property>';//码流2: 2M 4: 4M
                }
    
                $xml_str .= '<Property Name="MultiCastIP">239.111.111.111</Property>';
                $xml_str .= '<Property Name="MultiCastPort">4001</Property>';
                $xml_str .= '<Property Name="BitrateCount">1</Property>';
                $xml_str .= '</Object>';
            }
        }
        $xml_str .= '</Objects>';
        $xml_str .= '</ADI>';
    
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'live_media' .'_'. $action . '_' . $three_rand. '.xml';
    
        $c2_info = array(
            //'nns_id' => $c2_info['c2_id'],
            'nns_task_type'=>'PhysicalChannel',
            'nns_task_id'=> isset($params['nns_task_id'])?$params['nns_task_id']:null,
            'nns_task_name'=> isset($params['nns_task_name'])?$params['nns_task_name']:$live_name.'-直播流',
            'nns_action'=>	$action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            //'nns_result' =>$result,
            'nns_desc' => 'PhysicalChannel,'.$action,
        );
    
        return self::execute_c2($c2_info,$sp_id);
    }
    /**
     * @params array(
     * nns_id
     * nns_task_id
     * )
     */
    static public function delete_live_media($params,$action,$sp_id){
        //$action = 'REGIST';
        $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
        $xml_str .= '<Objects>';
        $xml_str .= '<Object ElementType="PhysicalChannel" ID="' .$params['nns_id']. '" Action="' . $action . '" Code="' .$params['nns_id']. '">';
        $xml_str .= '</Object>';
        $xml_str .= '</Objects>';
        $xml_str .= '</ADI>';
    
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'live_media' .'_'. $action . '_' . $three_rand. '.xml';
    
        $c2_info = array(
            //'nns_id' => $c2_info['c2_id'],
            'nns_task_type'=>'PhysicalChannel',
            'nns_task_id'=> isset($params['nns_task_id'])?$params['nns_task_id']:null,
            'nns_task_name'=> isset($params['nns_task_name'])?$params['nns_task_name']:null,
            'nns_action'=>	$action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            //'nns_result' =>$result,
            'nns_desc' => 'PhysicalChannel,'.$action,
        );
    
        return self::execute_c2($c2_info,$sp_id);
    }
    
    static public function do_content_query($task_info,$sp_id){
        $xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
        $xml_str .= '<Objects>';
        $xml_str .= '<Object ElementType="ContentQuery" ID="' .$task_info['nns_ref_id']. '"  Code="' .$task_info['nns_ref_id']. '">';
        $xml_str .= '<Property Name="ObjectType">Movie</Property>';
        $xml_str .= '</Object>';
        $xml_str .= '</Objects>';
        $xml_str .= '</ADI>';
        $guid =  np_guid_rand();
    
        $time = date('YmdHis') . rand(0, 10000);
        $three_rand = rand(100, 999);
        $file_name = 'zgcms_'. $time .'_' . 'content_query' .'_'. $action . '_' . $three_rand. '.xml';
    
        $action = 'REGIST';
        $c2_info = array(
            'nns_task_type'=>'QueryPlay',
            'nns_task_id'=> $task_info['nns_id'],
            'nns_task_name'=> $task_info['nns_name'],
            'nns_action'=>	$action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            'nns_desc' => 'QueryPlay,'.$action,
        );
        return self::execute_c2($c2_info,$sp_id);
    }
    
    /**
     * 华为没有处理的指令，再次重发
     */
    static public function resend_c2_again($sp_id){
        $db = nn_get_db(NL_DB_READ);
        /************超时没返回重发************/
        $time_out = 3600;
        $sql = "select * from nns_mgtvbk_c2_log where nns_org_id='$sp_id' and nns_again=0 and nns_create_time < NOW() - INTERVAL ".$time_out." SECOND  and nns_notify_result is null  order by nns_create_time asc  limit 90";
        $all = nl_db_get_all($sql,$db);
        if(is_array($all)) {
            foreach($all as $item0){
                self::execute_c2_again($item0['nns_id'],$sp_id);
            }
        }
    
        /************以下为失败重发************/
        //连续剧
        $sql = "select * from nns_mgtvbk_c2_log where nns_org_id='$sp_id' and	(nns_notify_result='-1' and (nns_notify_result_url=''  or nns_notify_content like '%Entity queue is full%') or nns_result like '%[-1]soap info is full%') and nns_again=0 order by nns_again asc,nns_create_time asc  limit 90";
        $all = nl_db_get_all($sql,$db);
        if(is_array($all)) {
            foreach($all as $item0){
                self::execute_c2_again($item0['nns_id'],$sp_id);
            }
        }
    }
    
    static public function execute_c2_again($nns_id,$sp_id){
        $db = nn_get_db(NL_DB_READ);
        $db_w = nn_get_db(NL_DB_WRITE);
        $sql = "select * from nns_mgtvbk_c2_log where nns_id='$nns_id'";
        $c2_info = nl_db_get_one($sql,$db);
    
        /*$count = "select count(1) as num from nns_mgtvbk_c2_log where nns_org_id='{$sp_id}' and nns_task_id='{$c2_info['nns_task_id']}' and nns_task_type='{$c2_info['nns_task_type']}'";
         $c = nl_db_get_col($count, $db);
         if($c>=2){
         return false;
        }*/
        // include_once dirname(dirname(dirname(__FILE__))).'/mgtv/'.$sp_id.'/'.$sp_id.'_client.php';
        // $CmdFileURL = C2_FTP.$c2_info['nns_url'];
        // $c2_info['nns_id'] = np_guid_rand();
        // $c2_info['nns_again'] = 0;
        // unset($c2_info['nns_result'],$c2_info['nns_notify_result_url'],$c2_info['nns_notify_result'],$c2_info['nns_notify_time'],$c2_info['nns_modify_time'],$c2_info['nns_notify_content']);
        // $dt = date('Y-m-d H:i:s');
        // $c2_info['nns_create_time'] = $dt;
        // $c2_info['nns_send_time'] = $dt;
        // $result = call_user_func_array(array($sp_id.'_client', 'ExecCmd'), array($c2_info['nns_id'], $CmdFileURL));
        // $c2_info['nns_result'] = '['.$result->Result.']'.$result->ErrorDescription;
        //$result = jllt_client::ExecCmd($nns_id,$CmdFileURL);
        $sql = "update nns_mgtvbk_c2_log set nns_again=1 ,nns_notify_result='-1',nns_modify_time=now() where nns_id='".$nns_id."'";
        nl_execute_by_db($sql,$db_w);
    
        if($c2_info['nns_task_id']){
            content_task_model::vod($c2_info['nns_task_id'],null,$sp_id);
        }
    
        return true;
    }
    /**
     * 执行C2注入命令
     * @param $c2_info array(
     * 'c2_id'
     * 'file_name'
     * 'xml_content'
     * )
     */
    static public function execute_c2($c2_info,$sp_id){
        global $g_webdir;
        $c2_id = np_guid_rand();
        $c2_info['nns_id'] = $c2_id;
//         if(file_exists(dirname(dirname(__FILE__)).'/'.$sp_id.'/models/child_c2_task_model.php')){
//             include_once dirname(dirname(__FILE__)).'/'.$sp_id.'/models/child_c2_task_model.php';
//             if(method_exists('child_c2_task_model','execute_c2')){
//                 $re = child_c2_task_model::execute_c2($c2_info,$sp_id);
//                 return $re;
//             }
//         }
        include_once dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv/'.$sp_id.'/'.$sp_id.'_client.php';
        if(!self::exists_c2_commond($c2_info,$sp_id)){
            $sub_path = date('Y-m-d');
            $file = $c2_info['nns_url'];
            $c2_info['nns_url'] = $sub_path.'/'.$file;
            $xml_str = $c2_info['nns_content'];
            $file_path = dirname(dirname(dirname(dirname(__FILE__)))).'/data/mgtv/'.$sp_id.'/inject_xml/'.$sub_path.'/';
            if(!is_dir($file_path)) mkdir($file_path,0777,true);
            $file_path .= $file;
            file_put_contents($file_path,$xml_str);
            $CmdFileURL = C2_FTP.$c2_info['nns_url'];
            try {
                np_runtime_ex($c2_id,false);
                DEBUG && i_write_log_core('---------execute c2----------'.var_export($c2_info,true),'soap');
                $result = call_user_func_array(array($sp_id.'_client', 'ExecCmd'), array($c2_id, $CmdFileURL));
                /*$c2_info['nns_result'] = '['.$result->Result.']';//$result->ErrorDescription;
                 $bool = false;
                 if($result->Result != 0 ){
                */
                //var_dump($result);die;
                $result = isset($result->Result) ? $result->Result : $result->result;
                $result = ($result === null) ? 0 : $result;
                $c2_info['nns_result'] = '['.$result.']';//$result->ErrorDescription;
                $bool = false;
                if($result != 0 ){
                    //$c2_info['nns_notify_result'] = -1;
                    $bool = true;
                }
                DEBUG && i_write_log_core('---------execute soap time----------'.np_runtime_ex($c2_id,true),'soap');
                self::save_c2_log($c2_info,$sp_id);
                	
                if($bool===true){
                    return false;
                }
                return true;
            } catch (Exception $e) {
                $c2_info['nns_result'] = '[-1]';
                $c2_info['nns_notify_result'] = -1;
                self::save_c2_log($c2_info,$sp_id);
                return false;
            }
        }
        return false;
    }
    
    static public function exists_c2_commond($c2_info,$sp_id){
        set_time_limit(0);
        $db_r = nn_get_db(NL_DB_READ);
        $nns_org_id = $sp_id;
        $nns_task_type = $c2_info['nns_task_type'];
        $nns_task_id = $c2_info['nns_task_id'];
        $nns_action = $c2_info['nns_action'];
    
        if(empty($nns_task_id)) return false;
        /*$count = "select count(1) as num from nns_mgtvbk_c2_log where nns_org_id='{$sp_id}' and nns_task_id='{$nns_task_id}' and nns_task_type='{$nns_task_type}'";
         $c = nl_db_get_col($count, $db);
         if($c>=2){
         return true;
         }*/
    
        usleep(5000);
        //if(empty($nns_task_id)) return false;
        $nowtime=date("Y-m-d H:i:s");
        $now_data = date('Y-m-d H:i:s',strtotime("$nowtime-1 hour"));
        $sql = "select 1 from nns_mgtvbk_c2_log where nns_org_id='$nns_org_id' and nns_task_type='$nns_task_type' and nns_task_id='$nns_task_id' and nns_action='$nns_action' and nns_notify_result is null and nns_create_time > NOW() - INTERVAL 3600 SECOND";
    
        //$sql = "select nns_task_id,count(1) as num from nns_mgtvbk_c2_log where nns_create_time > '".$now_data."' and nns_task_type='Movie' group by nns_task_id,nns_action having count(1)> 2;";
        $data = nl_db_get_all($sql,$db_r);
    
        if(is_array($data) && count($data)>0){
            return true;
        }
        return false;
    
        /*
         switch ($nns_task_type) {
         case 'Series':
         $sql = "select * from nns_mgtvbk_c2_task where nns_id='{$nns_task_id}' and nns_org_id='{$nns_org_id}'";
         break;
         case 'Program':
         $sql = "select * from nns_mgtvbk_c2_task where nns_id='{$nns_task_id}' and nns_org_id='{$nns_org_id}'";
         break;
         case 'Movie':
         $sql = "select * from nns_mgtvbk_c2_task where nns_id='{$nns_task_id}' and nns_org_id='{$nns_org_id}'";
         break;
         default:
         return false;
         break;
         }
         $data = nl_db_get_one($sql,$db_r);
         if(is_array($data)&&$data['nns_status']==5){
         return true;
         }
         return false;*/
    }
    
    
    /**
     * 保存C2命令日志
     * $c2_log array(
     * 'nns_id',
     * 'nns_type',
     * 'nns_org_id',
     * 'nns_video_id',
     * 'nns_video_type',
     * 'nns_video_index_id',
     * 'nns_media_id',
     * 'nns_url',
     * 'nns_content',
     * 'nns_result',
     * 'nns_desc',
     * 'nns_create_time'
     * )
     */
    static public function save_c2_log($c2_log,$sp_id){
        $db = nn_get_db(NL_DB_WRITE);
        //$db->open();
        $c2_log['nns_type'] = 'std';
        $c2_log['nns_org_id'] = $sp_id;
        $dt = date('Y-m-d H:i:s');
        $c2_log['nns_create_time'] = $dt;
        $c2_log['nns_send_time'] = $dt;
        return nl_db_insert($db,'nns_mgtvbk_c2_log',$c2_log);
    }
    /**
     * C2通知命令日志
     * $c2_notify array(
     * 'nns_id',
     * 'nns_notify_result_url',
     * 'nns_notify_result',
     *
     * )
     */
    static public function save_c2_notify($c2_notify){
        DEBUG && i_write_log_core('---------save_c2_notify----------');
        $db = nn_get_db(NL_DB_WRITE);
        $dt = date('Y-m-d H:i:s');
        $wh_content ="";
        if($c2_notify['nns_notify_result']!='-1'){
            $wh_content = "nns_content='',";
        }
        $sql = "update nns_mgtvbk_c2_log set " .
            "nns_notify_result_url='".$c2_notify['nns_notify_result_url']."' ," .$wh_content.
            "nns_notify_result='".$c2_notify['nns_notify_result']."', nns_notify_time='".$dt."'," .
            "nns_modify_time='".$dt."' " .
            "where nns_id='".$c2_notify['nns_id']."'";
        nl_execute_by_db($sql,$db);
    
        $db_r = nn_get_db(NL_DB_READ);
        $sql = "select * from nns_mgtvbk_c2_log where nns_id='".$c2_notify['nns_id']."'";
        $c2_info = nl_db_get_one($sql,$db_r);
        if($c2_info==false) return false;
        //如果回馈的消息是重发的消息
        if($c2_info['nns_again']>=1){
            return false;
        }
        if($c2_info['nns_task_type']=='Series' || $c2_info['nns_task_type']=='Program'||$c2_info['nns_task_type']=='Movie'){
            if($c2_notify['nns_notify_result']=='-1'){
                $sql_update = "update nns_mgtvbk_c2_task set nns_status=-1,nns_modify_time=now(),nns_epg_status=97 where nns_id='".$c2_info['nns_task_id']."'";//注入媒体时，再次发送；否则媒体注入没有绑定节目
                /*if($c2_info['nns_action']=='REGIST'){
                 $sql_update = "update nns_mgtvbk_c2_task set nns_status=1,nns_action='modify',nns_modify_time=now(),nns_epg_status=97 where nns_id='".$c2_info['nns_task_id']."'";//注入媒体时，再次发送；否则媒体注入没有绑定节目
                 }elseif($c2_info['nns_action']=='UPDATE'){
                 $sql_update = "update nns_mgtvbk_c2_task set nns_status=1,nns_action='add',nns_modify_time=now(),nns_epg_status=97 where nns_id='".$c2_info['nns_task_id']."'";//注入媒体时，再次发送；否则媒体注入没有绑定节目
                 }*/
            }else{
                $sql_update = "update nns_mgtvbk_c2_task set nns_status=0,nns_modify_time=now(),nns_epg_status=97 where nns_id='".$c2_info['nns_task_id']."'";
    
                //是否需要异步获取播放穿
    
                $sp_config = sp_model::get_sp_config($c2_info['nns_org_id']);
                $cdn_query_mediaurl_bool = false;
                if(isset($sp_config['cdn_query_mediaurl'])&&(int)$sp_config['cdn_query_mediaurl']===1){
                    $cdn_query_mediaurl_bool = true;
                }
                if($cdn_query_mediaurl_bool&&$c2_info['nns_task_type']=='Movie'){
                    	
                    $sql_update = "update nns_mgtvbk_c2_task set nns_status=6,nns_modify_time=now(),nns_epg_status=97 where nns_id='".$c2_info['nns_task_id']."'";
                    $sql_info = "select * from nns_mgtvbk_c2_task where nns_id='{$c2_info['nns_task_id']}'";
                    $c2__taskinfo = nl_db_get_one($sql_info,$db_r);
                    	
                    if(is_array($c2__taskinfo)){
                        if($c2__taskinfo['nns_status']=='0'){
                            $sql_update = "update nns_mgtvbk_c2_task set nns_status=0,nns_modify_time=now(),nns_epg_status=97 where nns_id='".$c2_info['nns_task_id']."'";
                        }
                    }
                    	
                    if($c2_info['nns_action']=='DELETE'){
                        $sql_update = "update nns_mgtvbk_c2_task set nns_status=0,nns_modify_time=now(),nns_epg_status=97 where nns_id='".$c2_info['nns_task_id']."'";
                    }
                }
            }
            nl_execute_by_db($sql_update,$db);
        }
    
        if($c2_info['nns_task_type']=='Movie' && (int)$sp_config['disabled_clip'] !== 2){
            $sql_c2_task = nl_db_get_one("select * from  nns_mgtvbk_c2_task  where nns_id='".$c2_info['nns_task_id']."'", $db);
            if($c2_info['nns_action']=='DELETE'){
                if($c2_notify['nns_notify_result']=='-1'){
                    $nns_state = clip_task_model::TASK_C_STATE_C2_DELETE_FAIL;
                    $nns_desc = clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_C2_DELETE_FAIL];
                    //$sql = "update nns_mgtvbk_c2_task set nns_status=-1 where nns_id='".$c2_info['nns_task_id']."'";
                    //nl_execute_by_db($sql,$db);
                }else{
                    $nns_state = clip_task_model::TASK_C_STATE_C2_DELETE_SUCC;
                    $nns_desc = clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_C2_DELETE_SUCC];
                    //$sql = "update nns_mgtvbk_c2_task set nns_status=0 where nns_id='".$c2_info['nns_task_id']."'";
                    //nl_execute_by_db($sql,$db);
                    	
                }
            }else{
                if($c2_notify['nns_notify_result']=='-1'){
                    $nns_state = clip_task_model::TASK_C_STATE_C2_FAIL;
                    $nns_desc = clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_C2_FAIL];
                    //$sql = "update nns_mgtvbk_c2_task set nns_status=-1 where nns_id='".$c2_info['nns_task_id']."'";
                    //nl_execute_by_db($sql,$db);
                }else{
                    $nns_state = clip_task_model::TASK_C_STATE_C2_SUCC;
                    $nns_desc = clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_C2_SUCC];
                    //$sql = "update nns_mgtvbk_c2_task set nns_status=0 where nns_id='".$c2_info['nns_task_id']."'";
                    //if($c2_info['nns_org_id']=='sihua'){
                    //	$sql = "update nns_mgtvbk_c2_task set nns_status=6 where nns_id='".$c2_info['nns_task_id']."'";
                    //}
                    //nl_execute_by_db($sql,$db);
                    	
                }
            }
    
            //更改切片状态
            $dt = date('Y-m-d H:i:s');
            $set_state = array(
                'nns_state' => $nns_state,
                'nns_desc' => $nns_desc,
                'nns_modify_time'=>$dt,
            );
            clip_task_model::update_task($sql_c2_task['nns_clip_task_id'], $set_state);
            //任务日志
            $task_log = array(
                'nns_task_id' => $sql_c2_task['nns_clip_task_id'],
                'nns_state' => $nns_state,
                'nns_desc' => $nns_desc,
            );
            clip_task_model::add_task_log($task_log);
        }
    
        // if($c2_info['nns_task_type']=='QueryPlay'){
        // if($c2_notify['nns_notify_result']=='-1'){
        // $sql_update = "update nns_mgtvbk_c2_task set nns_status=9,nns_modify_time=now(),nns_epg_status=97 where nns_id='".$c2_info['nns_task_id']."'";
        // }else{
        // $sql_update = "update nns_mgtvbk_c2_task set nns_status=0,nns_modify_time=now(),nns_epg_status=97,nns_ex_url='".time()."' where nns_id='".$c2_info['nns_task_id']."'";
        // }
        // nl_execute_by_db($sql_update,$db);
        // }
    
        //通知列队
        if($c2_notify['nns_notify_result']!='-1'){
            //$c2_info['nns_task_id']
            include_once 'queue_task_model.php';
            $queue_task_model = new queue_task_model();
            // 			print_r($c2_info);die;
            $queue_task_model->q_report_task($c2_info['nns_task_id']);
            $q_model=null;
        }
    
        //下载结果
        if(!empty($c2_notify['nns_notify_result_url'])){
            	
            $url_arr = parse_url($c2_notify['nns_notify_result_url']);
            $port = isset($url_arr["port"])?$url_arr["port"]:21;
            include_once NPDIR . '/np_ftp.php';
            $ftp1=new nns_ftp($url_arr['host'],$url_arr["user"],$url_arr["pass"],$port);
            $ftpcon1=$ftp1->connect();
            if (empty($ftpcon1)) {
                return FALSE;
            }
            $notify_save_path = dirname(dirname(dirname(__FILE__))).'/data/mgtv/'.$c2_info['nns_org_id'].'/notify'.$url_arr["path"];
            $path_parts = pathinfo($notify_save_path);
            if (!is_dir($path_parts['dirname'])){
                mkdir($path_parts['dirname'], 0777, true);
            }
            $rs = $ftp1->get($url_arr["path"],$notify_save_path);
            if($rs==false){
                DEBUG && i_write_log_core('---------下载 notify 失败----------');
                return FALSE;
            }
            DEBUG && i_write_log_core('---------下载 notify 成功----------');
        }
    
        return true;
    }
    
    
    static public function _save_img_from_url($img_file,$img_url){
        $ret_data = array();
        $json_data = null;
        $http_code = null;
        for ($i = 0; $i < 3; $i++) {
            $http_client = new np_http_curl_class();
            $json_data = $http_client->get($img_url);
            $http_code = $http_client->curl_getinfo['http_code'];
            if ($http_code != 200) {
                //i_echo('-----下载失败:'.$img_url);
                continue;
            } else {
                break;
            }
        }
        if ($json_data && $http_code && $http_code == 200) {
            return  file_put_contents($img_file,$json_data);
        }
        return false;
    }
    
    static public function update_notify_content($nns_id,$nns_notify_content){
        $db = nn_get_db(NL_DB_WRITE);
        $sql = "update nns_mgtvbk_c2_log set nns_notify_content='".$nns_notify_content."' where nns_id='".$nns_id."'";
        return nl_execute_by_db($sql,$db);
    }
    
    static public function get_c2_log_list($sp_id,$filter=null,$start=0,$size=100){
        $db = nn_get_db(NL_DB_READ);
        //$db->open();
        $sql = "select  * from nns_mgtvbk_c2_log where nns_org_id='".$sp_id."' ";
        $sql_count = "select  count(*) from nns_mgtvbk_c2_log where nns_org_id='".$sp_id."' ";
        if($filter != null){
            $wh_arr = array();
            if(!empty($filter['nns_task_name'])){
                $wh_arr[] = " nns_task_name like '%".$filter['nns_task_name']."%'";
            }
            if(!empty($filter['nns_id'])){
                $wh_arr[] = " nns_id='".$filter['nns_id']."'";
            }
            if(!empty($filter['nns_task_type'])){
                $wh_arr[] = " nns_task_type='".$filter['nns_task_type']."'";
            }
            if(!empty($filter['nns_action'])){
                $wh_arr[] = " nns_action='".$filter['nns_action']."'";
            }
    
            if(!empty($filter['day_picker_start'])){
                $wh_arr[] = " nns_create_time >='".$filter['day_picker_start']."' ";
            }
            if(!empty($filter['day_picker_end'])){
                $wh_arr[] = " nns_create_time <='".$filter['day_picker_end']."' ";
            }
            if(!empty($filter['notify_time_start'])){
                $wh_arr[] = "  nns_notify_time >='".$filter['notify_time_start']."'";
            }
            if(!empty($filter['notify_time_end'])){
                $wh_arr[] = "  nns_notify_time <='".$filter['notify_time_end']."'";
            }
            if(!empty($filter['nns_state'])){
                $state = $filter['nns_state'];
                switch($state){
                    case 'request_state_succ':
                        $wh_arr[] = " nns_result like '[0]%'";
                        break;
                    case 'request_state_fail':
                        $wh_arr[] = " nns_result like '[-1]%'";
                        break;
                    case 'notify_state_wait':
                        $wh_arr[] = " nns_notify_result is null ";
                        break;
                    case 'notify_state_succ':
                        $wh_arr[] = " nns_notify_result='0'";
                        break;
                    case 'notify_state_fail':
                        $wh_arr[] = " nns_notify_result='-1'";
                        break;
                    case 'resend_fail':
                        $wh_arr[] = " nns_again=-1";
                        break;
                }
    
            }
            if(!empty($wh_arr))	$sql .= " and ".implode(" and ",$wh_arr);
            if(!empty($wh_arr))	$sql_count .= " and ".implode(" and ",$wh_arr);
        }
        $sql .= " order by nns_create_time desc ";
        $sql .= " limit $start,$size";
        // echo $sql;
        $data = nl_db_get_all($sql,$db);
        $rows = nl_db_get_col($sql_count,$db);
        return array('data'=>$data,'rows'=>$rows);
    }
    
    static public function delete_c2($ids=array()){
        $db = nn_get_db(NL_DB_WRITE);
        $db_r = nn_get_db(NL_DB_READ);
        $sql = "select nns_task_name from nns_mgtvbk_c2_log where ".nl_db_in($ids,'nns_id');
        $c2_info = nl_db_get_all($sql,$db_r);
        $sql = "delete from nns_mgtvbk_c2_log where ".nl_db_in($ids,'nns_id');
        return nl_execute_by_db($sql,$db);
    }
    
    static  public function check_ftp_file($url){
        $url_arr = parse_url($url);
        $port = isset($url_arr["port"]) ? $url_arr["port"] : 21;
        $time = 10;
        $connect_no=ftp_connect($url_arr['host'],$port,$time);
        if ($connect_no===FALSE) return FALSE;
    
        @ftp_login(
            $connect_no,
            $url_arr["user"],
            $url_arr["pass"]
        );
        //吉林联通要开启被动模式
        @ftp_pasv($connect_no,true);
        $contents = @ftp_nlist($connect_no, $url_arr['path']);
        if($contents===false){
            return FALSE;
        }else{
            if(empty($contents)){
                return FALSE;
            }
            return true;
        }
    }
}