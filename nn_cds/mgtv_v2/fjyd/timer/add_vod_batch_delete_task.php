<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
set_time_limit(0);
define('ORG_ID', 'fjyd');
define('OP_WEIGHT', 99999);
//每次读取的数据条数
define('OP_DATA_NUM_EACH_TIME',50);
class add_vod_batch_delete_task extends nn_timer
{

	public function action($params = null)
	{
		$this->msg('开始执行...');
		do_timer_action();
		$this->msg('执行结束...');
	}
}
$add_vod_batch_delete_task = new add_vod_batch_delete_task("add_vod_batch_delete_task", ORG_ID);
$add_vod_batch_delete_task->run();

function do_timer_action()
{
	$data_file_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/log/op_queue/delete_vod_task.txt';
	$dc = nl_get_dc(array (
			'db_policy' => NL_DB_WRITE
	));
	$dc->open();
	if (!file_exists($data_file_path))
	{
		//echo '数据文件不存在';
		return ;
	}
	// 逐行读取数据
	$data_file = fopen($data_file_path, 'r');
	$count = 1;
	$is_left_empty = true;
	$locked = flock($data_file, LOCK_EX + LOCK_NB);
	while (!feof($data_file))
	{
		if ($count <= OP_DATA_NUM_EACH_TIME)
		{
			
			$data_item = fgets($data_file);
			if (!$data_item)
			{
				continue;
			}
			
			if ($count == OP_DATA_NUM_EACH_TIME)
			{
				// 最后一条时，记录剩余内容
				ob_start();
				fpassthru($data_file);
				$is_left_empty = false;
			}
			
			$data_item = preg_replace("/\r\n/", '', rtrim($data_item));
			$data_item_arr = explode(':', $data_item);
			$sp_id = $data_item_arr[0];
			$data_id = $data_item_arr[1];
			$data_type = $data_item_arr[2];
			$op_id = np_guid_rand();
			$result = add_c2_task($dc, $sp_id, $data_type, $data_id, $op_id);
			if ($result === false)
			{
				//echo "数据库执行失败！.运营商id为：".$sp_id.",数据类型为：".$data_type.",数据id为:".$data_id.",op id 为：".$op_id."<br/>";
				break;
			}
			if ($result === 0)
			{
				continue;
			}
			$count++;
		}
		else
		{
			break;
		}
	}
	// 如果最后数据不为空，将剩余内容重新写入到文件中
	if ($is_left_empty)
	{
		$left_content = '';
	}
	else
	{
		$left_content = ob_get_clean();
	}
	flock($data_file, LOCK_EX + LOCK_NB);
	fclose($data_file);
	$result = file_put_contents($data_file_path, $left_content);
}
/**
 * 添加epg队列   原因由于b28的数据清理了一次 导致nns_mgtvbk_c2_task 数据丢失  
 * 执行删除的时候发现没有nns_mgtvbk_c2_task的数据则忽略这样会产生很多垃圾数据
 * 所以后面发现没有数据则  创建一条已经执行到最终状态的数据  
 * @param object $dc 数据库操作对象
 * @param string $sp_id 运营商id
 * @param string $type 类型 video|index|media
 * @param string $mix_id 混合id   可能是 分集id  主媒资id  片源id
 * @param string $op_id op_queue id
 * @return boolean|number
 * @author liangpan
 * @date 2015-09-01
 */
function add_c2_task($dc, $sp_id, $type, $mix_id, $op_id)
{
	$str_src_id = 0;
	$str_task_id = '';
	$vod_id = '';
	$index_id = '';
	$media_id = '';
	switch ($type)
	{
		case 'media':
			$sql = "select nns_id from nns_mgtvbk_clip_task where nns_video_media_id='$mix_id' limit 1";
			$result = nl_query_by_db($sql, $dc->db());
			if (!$result)
			{
				//echo "查询切片数据库失败sql:".$sql."<br/>";
				return false;
			}
			$str_task_id = $result[0]['nns_id'];
			$sql = "select nns_mode,nns_vod_index_id from nns_vod_media where nns_id='{$mix_id}' limit 1";
			$result = nl_query_by_db($sql, $dc->db());
			if (!$result)
			{
				//echo "查询片源信息数据库失败sql:".$sql."<br/>";
				return false;
			}
			if (!is_array($result))
			{
				//echo "查询片源信息0<br/>";
				return 0;
			}
			$str_src_id = $result[0]['nns_vod_index_id'];
			$name = $result[0]['nns_mode'];
			$sql = "select nns_name,nns_vod_id from nns_vod_index where nns_id='{$result[0]['nns_vod_index_id']}' limit 1";
			$index_id = $result[0]['nns_vod_index_id'];
			$result = nl_query_by_db($sql, $dc->db());
			if (!$result)
			{
				//echo "查询分集信息数据库失败sql:".$sql."<br/>";
				return false;
			}
			if (!is_array($result))
			{
				//echo "查询分集信息0<br/>";
				return 0;
			}
			$name = '[' . $result[0]['nns_name'] . '] ' . $name;
			$sql = "select nns_name from nns_vod where nns_id='{$result[0]['nns_vod_id']}' limit 1";
			$vod_id = $result[0]['nns_vod_id'];
			$result = nl_query_by_db($sql, $dc->db());
			if (!$result)
			{
				//echo "查询主媒资信息数据库失败sql:".$sql."<br/>";
				return false;
			}
			if (!is_array($result))
			{
				//echo "查询主媒资0<br/>";
				return 0;
			}
			$media_id = $mix_id;
			$name = '[' . $result[0]['nns_name'] . '] ' . $name;
			break;
		case 'index':
			$sql = "select nns_name,nns_vod_id from nns_vod_index where nns_id='{$mix_id}' limit 1";
			$result = nl_query_by_db($sql, $dc->db());
			if (!$result)
			{
				//echo "查询分集信息数据库失败sql:".$sql."<br/>";
				return false;
			}
			if (!is_array($result))
			{
				//echo "查询分集0<br/>";
				return 0;
			}
			$str_src_id = $result[0]['nns_vod_id'];
			$name = '[' . $result[0]['nns_name'] . ']';
			$sql = "select nns_name from nns_vod where nns_id='{$result[0]['nns_vod_id']}' limit 1";
			$vod_id = $result[0]['nns_vod_id'];
			$result = nl_query_by_db($sql, $dc->db());
			if (!$result)
			{
				//echo "查询主媒资信息数据库失败sql:".$sql."<br/>";
				return false;
			}
			if (!is_array($result))
			{
				//echo "查询主媒资0<br/>";
				return 0;
			}
			$index_id = $mix_id;
			$name = '[' . $result[0]['nns_name'] . '] ' . $name;
			break;
		case 'video':
			$sql = "select nns_name from nns_vod where nns_id='{$mix_id}' limit 1";
			$result = nl_query_by_db($sql, $dc->db());
			if (!$result)
			{
				//echo "查询主媒资信息数据库失败sql:".$sql."<br/>";
				return false;
			}
			if (!is_array($result))
			{
				//echo "查询主媒资0<br/>";
				return 0;
			}
			$vod_id = $mix_id;
			$name = '[' . $result[0]['nns_name'] . ']';
			break;
		default:
			return 0;
	}
	$sql_query = $sql = "select count(*) as num from nns_mgtvbk_c2_task where nns_org_id='{$sp_id}' and nns_type='{$type}' and nns_ref_id='{$mix_id}'";
	$result_query = nl_query_by_db($sql_query, $dc->db());
	if (!$result_query || !is_array($result_query) || empty($result_query))
	{
		//echo "查询epg c2 的数量信息数据库失败sql:".$sql_query."<br/>";
		return false;
	}
	$date = date("Y-m-d H:i:s");
	$date_int = date("Ymd");
	$result_add_op = add_op($dc, $date, $type, $sp_id, $name, $op_id, $vod_id, $index_id, $media_id);
	if (!$result_add_op)
	{
		//echo "添加 op 的信息数据库失败<br/>";
	}
	if (isset($result_query[0]['num']) && $result_query[0]['num'] > 0)
	{
		return true;
	}
	$guid = np_guid_rand($sp_id . $type . $mix_id);
	$sql_insert = "insert into nns_mgtvbk_c2_task(nns_id,nns_type,nns_name,nns_ref_id,nns_action,nns_status,nns_create_time,nns_modify_time,
				nns_org_id,nns_category_id,nns_src_id,nns_all_index,nns_clip_task_id,nns_clip_date,nns_op_id,nns_epg_status,nns_ex_url,
				nns_file_path,nns_file_size,nns_file_md5,nns_cdn_policy) values
				('{$guid}','{$type}','{$name}','{$mix_id}','add','0','{$date}','{$date}',
				'{$sp_id}','','{$str_src_id}','','{$str_task_id}','{$date_int}','{$op_id}','99','',
				'','','','')";
	$result_insert = nl_execute_by_db($sql_insert, $dc->db());
// 	if (!$result_insert)
// 	{
// 		//echo "添加epg c2 信息数据库失败sql:".$sql_insert."<br/>";
// 	}
	return $result_insert ? true : false;
}

/**
 * 添加中心同步队列
 * @param object $dc 数据库操作对象
 * @param string $date 日期
 * @param string $type 类型   video|index|media
 * @param string $sp_id 运营商id
 * @param string $name 名称
 * @param string $op_id op_queue id
 * @param string $vod_id 主媒资id
 * @param string $index_id 分集id
 * @param string $media_id 片源id
 * @return boolean
 * @author liangpan
 * @date 2015-09-01
 */
function add_op($dc, $date, $type, $sp_id, $name, $op_id, $vod_id, $index_id = null, $media_id = null)
{
	$sql = "select nns_show_time from nns_vod where nns_id='{$vod_id}'";
	$re = nl_query_by_db($sql, $dc->db());
	$release_time = '';
	if (is_array($re))
	{
		$release_time = $re[0]['nns_show_time'];
	}
	$index_id = empty($index_id) ? '' : $index_id;
	$media_id = empty($media_id) ? '' : $media_id;
	$op_weight = OP_WEIGHT >= 0 ? OP_WEIGHT : 0;
	$op_mtime = round(np_millisecond_f() * 1000);
	$sql_insert = "insert into nns_mgtvbk_op_queue(nns_id,nns_type,nns_org_id,nns_create_time,nns_action,nns_status,nns_name,nns_video_id,
				nns_weight,nns_index_id,nns_media_id,nns_op_mtime,nns_release_time,nns_state,nns_ex_data,nns_from,nns_message_id) values(
				'{$op_id}','{$type}','{$sp_id}','{$date}','destroy','0','{$name}','{$vod_id}',
				'{$op_weight}','{$index_id}','{$media_id}','{$op_mtime}','{$release_time}','0','','','')";
	$result = nl_execute_by_db($sql_insert, $dc->db());
	//echo "添加 op 的信息数据库sql 为：".$sql_insert.",结果为:".var_export($result,true)."<br/>";
	return $result ? true : false;
}