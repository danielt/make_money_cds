<?php
/*
 * Created on 2013-8-24
 *
 * 下载失败的图片重新下载
 * 
 */
error_reporting(E_ALL);
ini_set('display_errors',1);
set_time_limit(0);  
include_once dirname(dirname(dirname(__FILE__))).'/mgtv_init.php';
include_once dirname(dirname(__FILE__)).'/init.php';
include_once(NPDIR . DIRECTORY_SEPARATOR . 'np_http_curl.class.php');
define('ORG_ID', 'fjyd');

class download_img_execute extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        do_timer_action();
        $this->msg('执行结束...');
    }
}
$download_img_execute = new download_img_execute("download_img_execute", ORG_ID);
$download_img_execute->run();

function do_timer_action()
{
    i_echo('------start download images:------');
    //
    $sql = "select *  from nns_vod where (nns_image2 != '' and nns_image2 is not null) or (nns_image0 != '' and nns_image0 is not null)";
    $db = nl_get_db(NL_DB_READ);
    $db->open();
    
    $db_w = nl_get_db(NL_DB_WRITE);
    $db_w->open();
    
    $vod_arr = nl_db_get_all($sql,$db);
    $root_dir = dirname(dirname(dirname(dirname(__FILE__)))).'/data/downimg';
    $dowload_img_url = "http://img.hifuntv.com/ImageServer/image_v1.5.5/";
    
    $i=0;
    $j=0;
    foreach($vod_arr as $vod_item){
        if(!empty($vod_item['nns_image2'])){
            $img_save_name = str_replace('.JPG','.jpg',$vod_item['nns_image2']);
            $img_file = $root_dir.$img_save_name;
            if(!file_exists($img_file) || filesize($img_file)==0){
                $img = str_replace("/prev/KsImg/", "", $vod_item['nns_image2']);
                $result = _save_img_from_url($img_file,$dowload_img_url.$img);
                $i++;
            }
        }
        if(!empty($vod_item['nns_image0'])){
            $img_save_name = str_replace('.JPG','.jpg',$vod_item['nns_image0']);
            $img_file = $root_dir.$img_save_name;
            if(!file_exists($img_file) || filesize($img_file)==0){
                $img = str_replace("/prev/KsImg/", "", $vod_item['nns_image0']);
                $result = _save_img_from_url($img_file,$dowload_img_url.$img);
                $j++;
            }
        }
    }
    unset($vod_arr);
    //
    
    $sql = "select *  from nns_vod_index where (nns_image != '' and nns_image is not null)";
    $db = nl_get_db(NL_DB_READ);
    $db->open();
    $vod_index_arr = nl_db_get_all($sql,$db);
    
    $k=0;
    
    foreach($vod_index_arr as $index_item){
        if(!empty($index_item['nns_image'])){
            $img_save_name = str_replace('.JPG','.jpg',$index_item['nns_image']);
            $img_file = $root_dir.$img_save_name;
            if(!file_exists($img_file) || filesize($img_file)==0){
                $img = str_replace("/prev/KsImg/", "", $index_item['nns_image']);
                $result = _save_img_from_url($img_file,$dowload_img_url.$img);
                $k++;
            }
        }
    }
    unset($vod_index_arr);
    i_echo('nns_image2:'.$i);
    i_echo('nns_image0:'.$j);
    i_echo('nns_image:'.$k);
    
    i_echo('------ download images end------');
}


function _save_img_from_url($img_file,$img_url){
	$ret_data = array();
	$json_data = null;
	$http_code = null;
	for ($i = 0; $i < 2; $i++) {
		    $http_client = new np_http_curl_class();
	        $json_data = $http_client->get($img_url);
	        $http_code = $http_client->curl_getinfo['http_code'];
	        if ($http_code != 200) {
	                i_echo('-----下载失败:'.$img_url);
	                continue;
	        } else {
	                break;
	        }
	}
	
	$root_dir = dirname(dirname(dirname(__FILE__))).'/data/downimg';
	if ($json_data && $http_code && $http_code == 200) {
	      return  file_put_contents($img_file,$json_data);
	}
	i_echo('-----重试三次失败------');
	return false;	
}

