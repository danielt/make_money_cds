<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors',1);
set_time_limit(0); 
include_once dirname(dirname(dirname(dirname(__FILE__)))) . DIRECTORY_SEPARATOR . 'nn_cms_manager' . DIRECTORY_SEPARATOR . 'nncms_manager_inc.php';
include_once LOGIC_DIR . 'log' . DIRECTORY_SEPARATOR . 'manager_log.class.php';
include_once LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'vod_media.class.php';
include_once LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'vod_index.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) .'/models/queue_task_model.php';
define('ORG_ID', 'jllt');

class add_vod_batch_delete_task extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        do_timer_action();
        $this->msg('执行结束...');
    }
}
$add_vod_batch_delete_task = new add_vod_batch_delete_task("add_vod_batch_delete_task", ORG_ID);
$add_vod_batch_delete_task->run();

function do_timer_action()
{
    $data_file_path = $nncms_config_path . 'data' . DIRECTORY_SEPARATOR . 'log' . DIRECTORY_SEPARATOR . 'op_queue' . DIRECTORY_SEPARATOR . 'delete_vod_task.txt';
    $oper_data_num_each_time = 50; //每次读取的数据条数
    $queue_task_model = new queue_task_model();
    $dc = nl_get_dc(array (
        'db_policy' => NL_DB_READ,
    ));
    $dc->open();
    
    if (!file_exists($data_file_path))
    {
        echo '数据文件不存在';exit;
    }
    
    // 逐行读取数据
    $data_file = fopen($data_file_path, 'r');
    $count = 1;
    $is_left_empty = true;
    $locked = flock($data_file, LOCK_EX+LOCK_NB);
    while (!feof($data_file))
    {
        if ($count <= $oper_data_num_each_time)
        {
    
            $data_item = fgets($data_file);
            if (!$data_item)
            {
                continue;
            }
    
            if ($count == $oper_data_num_each_time)
            {
                // 最后一条时，记录剩余内容
                ob_start();
                fpassthru($data_file);
                $is_left_empty = false;
            }
    
            $data_item = preg_replace("/\r\n/", '', rtrim($data_item));
            $data_item_arr = explode(':', $data_item);
            $sp_id = $data_item_arr[0];
            $data_id = $data_item_arr[1];
            $data_type = $data_item_arr[2];
    
            $eval_str = '$queue_task_model->q_add_task($data_id, $data_type, "destroy", $sp_id';
            if ($data_type == 'media')
            {
                $eval_str .= ', $data_id);';
            }
            else
            {
                $eval_str .= ');';
            }
            eval($eval_str);
            $count++;
        }
        else
        {
            break;
        }
    }
    
    // 如果最后数据不为空，将剩余内容重新写入到文件中
    if ($is_left_empty)
    {
        $left_content = '';
    }
    else
    {
        $left_content = ob_get_clean();
    }
    
    flock($data_file, LOCK_EX+LOCK_NB);
    fclose($data_file);
    $result = file_put_contents($data_file_path, $left_content);
}

