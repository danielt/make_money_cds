<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
array_pop($arr_dir);
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/init.php';
include_once dirname(dirname(dirname(__FILE__))) .'/models/queue_task_model.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) .'/nn_logic/sp/sp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) .'/nn_logic/clip_task/clip_task.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) .'/nn_logic/clip_task/clip_task_log.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_class/file_encode/file_encode.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/op_queue/op_queue.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_class/ftp/ftp.class.php';
include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/np/np_http_curl.class.php';

class timer_execute extends nn_timer
{
    public function action($params = null)
    {
        $this->msg('开始执行...');
        do_timer_action();
        $this->msg('执行结束...');
    }
}
$arr_files = pathinfo(__FILE__);
$file_name = $arr_files['filename'];
$timer_execute = new timer_execute($file_name, ORG_ID,__FILE__);
$timer_execute->run();

function do_timer_action($obj_message_log)
{
    $dc = nl_get_dc(array (
        'db_policy' => NL_DB_WRITE,
        'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
    ));
    $sp_config = nl_sp::get_sp_config($dc, ORG_ID);
    if($sp_config['ret'] !=0)
    {
        $obj_message_log->msg("获取SP_ID为[".ORG_ID."]的基本配置信息失败".var_export($sp_config['reason'],true));
        unset($dc,$sp_config);
        return ;
    }
    $sp_config = isset($sp_config['data_info']['nns_config']) ? $sp_config['data_info']['nns_config'] : null;
    if(!isset($sp_config['clip_file_encode_enable']) || $sp_config['clip_file_encode_enable'] !=1)
    {
        $obj_message_log->msg("获取SP_ID为[".ORG_ID."]的基本配置信息不需要文件转码逻辑，跳出循环");
        unset($dc,$sp_config);
        return ;
    }
    $max_num = (isset($sp_config['clip_encode_wait_max']) && (int)$sp_config['clip_encode_wait_max']>0) ? (int)$sp_config['clip_encode_wait_max'] : 5;
    $result_num = nl_clip_task::check_state_num_by_sp($dc, ORG_ID,'clip_encode_loading');
    if($result_num['ret'] !=0)
    {
        $obj_message_log->msg("获取SP_ID为[".ORG_ID."]的状态为等待转码的文件[clip_encode_loading]数据库执行失败".var_export($result_num['reason'],true));
        unset($dc,$sp_config,$result_num);
        return ;
    }
    $queue_task_model = new queue_task_model($dc,ORG_ID,$sp_config);
    if($max_num > $result_num['data_info'])
    {
        $obj_message_log->msg("队列没有空闲，不在放入数据，等待队列空闲后再进行");
        $num_wait = $max_num - $result_num['data_info'];
        
        //获取等待转码的数据
        $obj_message_log->msg("--------获取等待转码的数据开始------------");
        $result_queue_wait = $queue_task_model->get_encode_clip_task($num_wait);
        if($result_queue_wait['ret'] !=0)
        {
            $obj_message_log->msg($result_queue_wait['reason']);
        }
        $obj_message_log->msg("--------获取等待转码的数据结束------------");
    }
    
    $num_fail = (isset($sp_config['clip_encode_fail_max']) && (int)$sp_config['clip_encode_fail_max']>0) ? (int)$sp_config['clip_encode_fail_max'] : 0;
    
    //获取正在转码的数据进度
    $obj_message_log->msg("--------获取正在转码的数据进度开始------------");    
    $result_queue_load = $queue_task_model->get_encode_clip_load_task();
    if($result_queue_load['ret'] !=0)
    {
        $obj_message_log->msg($result_queue_load['reason']);
    }
    $obj_message_log->msg("--------获取正在转码的数据进度结束------------");
    if($num_fail >0)
    {
        //获取失败转码的数据
        $obj_message_log->msg("--------获取失败转码的数据开始------------");
        $result_queue_fail = $queue_task_model->get_encode_clip_fail_task($num_fail);
        if($result_queue_fail['ret'] !=0)
        {
            $obj_message_log->msg($result_queue_fail['reason']);
        }
        $obj_message_log->msg("--------获取失败转码的数据结束------------");
    }
    unset($queue_task_model,$sp_config,$dc);
    i_echo('end');
}

