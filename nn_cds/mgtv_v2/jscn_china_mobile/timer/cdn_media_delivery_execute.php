<?php
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
array_pop($arr_dir);
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/init.php';

class timer_execute extends nn_timer
{
    public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->init($params);
        $this->msg('执行结束...');
    }
    
    public function init($params=null)
    {
        $this->obj_dc = nl_get_dc(array (
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_NULL
        ));
        $result_sp = $this->_get_sp_info(ORG_ID);
        if($result_sp['ret'] !='0')
        {
            $this->msg(var_export($result_sp,true));
            return $result_sp;
        }
        if(!is_array($this->arr_sp_config) || empty($this->arr_sp_config))
        {
            return ;
        }
        foreach ($this->arr_sp_config as $sp_key=>$sp_val)
        {
            if (isset($sp_val['nns_config']['disabled_cdn']) && (int)$sp_val['nns_config']['disabled_cdn'] === 1) 
            {
                continue;
            }
//            $re = content_task_model::delivery_vod($new_id_c2 = null, 'media', $sp_key,false,$sp_val['nns_config'],$this->obj_dc);
            $re = content_task_model::delivery_vod($this->obj_dc, $sp_val['nns_config'], $sp_key,'media',null);
        }
    }
}
$arr_files = pathinfo(__FILE__);
$file_name = $arr_files['filename'];
$timer_execute = new timer_execute($file_name, ORG_ID,__FILE__);
$timer_execute->run();