<?php
class child_c2_task_model
{
	/**
	 * 获取图片FTP、http地址
	 * @param 图片路径 $img_path
	 * @param SPid $sp_id
	 */
	static public function get_image_url($img_path,$sp_id)
	{
		include_once dirname(dirname(dirname(__FILE__))).'/'.$sp_id.'/define.php';
		if(defined('HW_DOANLOAD_IMG_METHOD') && HW_DOANLOAD_IMG_METHOD=='ftp' && !empty($img_path))
		{
			$img_url = HW_DOANLOAD_IMG_FTP.$img_path;
		}
		else
		{
			$img_url = nl_image::epg_video_image_url($img_path);
		}
		return $img_url;
	}
	/**
	 * 添加连续剧
	 */
	static public function add_series($vod_info,$sp_id)
	{
		$action = 'REGIST';
		return self::do_series($vod_info,$action,$sp_id);
	}

	/**
	 * 修改连续剧
	 */
	static public function update_series($vod_info,$sp_id)
	{
		$action = 'UPDATE';
		return self::do_series($vod_info,$action,$sp_id);
	}

	/**
	 * 连续剧剧头注入
	 * @param $vod_info 连续剧信息
	 * @param $action 操作
	 * @param $sp_id SPID
	 */
	static public function do_series($vod_info,$action,$sp_id)
	{
		$db_r = nn_get_db(NL_DB_READ);
		$vod_id = $vod_info['nns_id'];
		$sp_config = sp_model::get_sp_config($sp_id);
		$info = video_model::get_vod_info($vod_id);
		if(isset($sp_config['import_id_mode']) && strlen($sp_config['import_id_mode']) > 0 && (int)$sp_config['import_id_mode'] > 0)
		{
			switch ($sp_config['import_id_mode'])
			{
				case '1':
					$vod_id = '000000'.$info['nns_integer_id'];
					$vod_id = str_pad($vod_id,32,"0");
					break;
				case '2':
					$vod_id = $info['nns_asset_import_id'];
					break;
				case '3':
					$vod_id = CSP_ID.'000'.$info['nns_integer_id'];
					$vod_id = str_pad($vod_id,32,"0");
					break;
			}
		}
		$sql = "select * from nns_depot where nns_id='{$info['nns_depot_id']}'";
		$depot = nl_db_get_one($sql, $db_r);
		unset($sql);
		$category_name = '';
		$dom = new DOMDocument('1.0', 'utf-8');
		$depot_info = $depot['nns_category'];
		$nns_category_id = $info['nns_category_id'];
		$dom->loadXML($depot_info);
		$categorys = $dom->getElementsByTagName('category');
		foreach ($categorys as $category)
		{
			if ($category->getAttribute('id') == (int)$nns_category_id)
			{
				$category_name = $category->getAttribute('name');
				break;
			}
		}
		//容错，保证横竖方图每一张都有
		if(empty($info['nns_image2']) && !empty($info['nns_image_v']))
		{
			$info['nns_image2'] = $info['nns_image_v'];
		}
		if(empty($info['nns_image1']) && !empty($info['nns_image_v']))
		{
			$info['nns_image1'] = $info['nns_image_v'];
		}
		if(!empty($info['nns_image2']) && empty($info['nns_image_v']))
		{
			$info['nns_image_v'] = $info['nns_image2'];
		}
		if(empty($info['nns_image_h']))
		{
			$info['nns_image_h'] = $info['nns_image_v'];
		}
		if(empty($info['nns_image_s']))
		{
			$info['nns_image_s'] = $info['nns_image_v'];
		}
		$info['nns_summary'] = str_replace(array('"',"&","《","》"), '', $info['nns_summary']);
		$params = array(
			'assets_id' => $vod_id,
			'assets_category' => $category_name,
			'video_type' => 0,
			'name' => htmlspecialchars($info['nns_name'], ENT_QUOTES),
			'sreach_key' => htmlspecialchars(str_replace("/", ",", $info['nns_keyword']), ENT_QUOTES),
			'view_type' => $info['nns_view_type'],
			'director' => htmlspecialchars(str_replace("/", ",", $info['nns_director']), ENT_QUOTES),
		 	'player' => htmlspecialchars(str_replace("/", ",", $info['nns_actor']), ENT_QUOTES),
			'tag' => htmlspecialchars(str_replace("/", ",", $info['nns_kind']), ENT_QUOTES),
			'region' => htmlspecialchars(str_replace("/", ",", $info['nns_area']), ENT_QUOTES),
			'release_time' => $info['nns_show_time'],
			'totalnum' => $info['nns_all_index'],
			'smallpic' => self::get_image_url(str_replace("JPG", "jpg", $info['nns_image2']),$sp_id),
			'midpic' => self::get_image_url(str_replace("JPG", "jpg", $info['nns_image1']),$sp_id),
			'bigpic' => self::get_image_url(str_replace("JPG", "jpg", $info['nns_image0']),$sp_id),
			'verticality_img' => self::get_image_url(str_replace("JPG", "jpg", $info['nns_image_v']),$sp_id),
			'horizontal_img' => self::get_image_url(str_replace("JPG", "jpg", $info['nns_image_h']),$sp_id),
			'square_img' => self::get_image_url(str_replace("JPG", "jpg", $info['nns_image_s']),$sp_id),
			'intro' => htmlspecialchars($info['nns_summary'], ENT_QUOTES),
			'producer' => htmlspecialchars(str_replace("/", ",", $info['nns_producer']), ENT_QUOTES),
			'play_role' => htmlspecialchars($info['nns_play_role'], ENT_QUOTES),
			'subordinate_name' => htmlspecialchars(str_replace("/", ",", $info['nns_alias_name']), ENT_QUOTES),
			'english_name' => htmlspecialchars(str_replace("/", ",", $info['nns_eng_name']), ENT_QUOTES),
			'language' => htmlspecialchars(str_replace("/", ",", $info['nns_language']), ENT_QUOTES),
			'caption_language' => htmlspecialchars(str_replace("/", ",", $info['nns_text_lang']), ENT_QUOTES),
			'copyright_range' => str_replace("/", ",", $info['nns_remark']),
			'copyright' => str_replace("/", ",", $info['nns_copyright']),
			'total_clicks' => $info['nns_play_count'],
			'view_len' => $info['nns_view_len'],
			'asset_source' => $info['nns_cp_id'],
			'day_clicks' => 0,
			'original_id' => $info['nns_asset_import_id'],
		);
		$sql = "SELECT * FROM `nns_vod_ex` where nns_vod_id='{$info['nns_asset_import_id']}'";
		$vod_ex = nl_db_get_all($sql, $db_r);
		if(is_array($vod_ex))
		{
			foreach ($vod_ex as $value)
			{
				$params[$value['nns_key']] = htmlspecialchars($value['nns_value'], ENT_QUOTES);
			}
		}
		$xml_str = self::build_c2_xml($params,'video');
		$time = date('YmdHis') . rand(0, 10000);
		$three_rand = rand(100, 999);
		$file_name = 'Series_'.$time.'_'.$vod_id.'_'.$three_rand.'.xml';
		$c2_info = array(
			'nns_task_type'=>'Series',
			'nns_task_id'=> isset($vod_info['nns_task_id']) ? $vod_info['nns_task_id'] : null,
			'nns_task_name'=> isset($vod_info['nns_task_name']) ? $vod_info['nns_task_name'] : null,
			'nns_action'=> $action,
			'nns_url' => $file_name,
			'nns_content' => $xml_str,
			'nns_desc' => 'Series,'.$action,
		);

		$cp_id = isset($info['nns_cp_id']) ? $info['nns_cp_id'] : null;
		return self::execute_c2($c2_info,$cp_id,$sp_id,$vod_id);
	}

	/**
	 * 删除连续剧剧头
	 * 
	 * @param array $vod_info 主媒资信息
	 * @param string $sp_id 运营商ID
	 */
	static public function delete_series($vod_info,$sp_id)
	{
		$action = 'DELETE';
		$vod_id = $vod_info['nns_id'];
		$sp_config = sp_model::get_sp_config($sp_id);
		$info = video_model::get_vod_info($vod_id);

		if(isset($sp_config['import_id_mode']) && strlen($sp_config['import_id_mode']) > 0 && (int)$sp_config['import_id_mode'] > 0)
		{
			switch ($sp_config['import_id_mode'])
			{
				case '1':
					$vod_id = '000000'.$info['nns_integer_id'];
					$vod_id = str_pad($vod_id,32,"0");
					break;
				case '2':
					$vod_id = $info['nns_asset_import_id'];
					break;
				case '3':
					$vod_id = CSP_ID.'000'.$info['nns_integer_id'];
					$vod_id = str_pad($vod_id,32,"0");
					break;
			}
		}

		//删除也要用来存放通知结果
		$time = date('YmdHis') . rand(0, 10000);
		$three_rand = rand(100, 999);
		$file_name = 'Series_delete_'.$time.'_'.$vod_id.'_'.$three_rand.'.xml';
		$c2_info = array(
			'nns_task_type'=>'Series',
			'nns_task_id'=> isset($vod_info['nns_task_id']) ? $vod_info['nns_task_id'] : null,
			'nns_task_name'=> isset($vod_info['nns_task_name']) ? $vod_info['nns_task_name'] : null,
			'nns_action'=> $action,
			'nns_url' => $file_name,
			'nns_content' => '',
			'nns_desc' => 'Series,'.$action,
		);

		$cp_id = isset($info['nns_cp_id']) ? $info['nns_cp_id'] : null;
		return self::execute_c2($c2_info,$cp_id,$sp_id,$vod_id);
	}

	/**
	 * 添加分集
	 */
	static public function add_vod($index_info,$sp_id)
	{
		$action = 'REGIST';
		return self::do_vod($index_info,$action,$sp_id);
	}

	/**
	 * 修改分集
	 */
	static public function update_vod($index_info,$sp_id)
	{
		$action = 'UPDATE';
		return self::do_vod($index_info,$action,$sp_id);
	}

	static public function do_vod($index_info,$action,$sp_id)
	{
		$db_r = nn_get_db(NL_DB_READ);
		$index_id = $index_info['nns_id'];

		$info = video_model::get_vod_index_info($index_id);
		$vod_id = $info['nns_vod_id'];
		$vod_index = $info['nns_index'];

		//查询打点信息
		$sql_seekpoint="select * from nns_vod_index_seekpoint where nns_video_id='{$vod_id}' and nns_video_index='{$vod_index}'";
		$result_seekpoint=nl_query_by_db($sql_seekpoint, $db_r);
		$seekpoint_xml = null;
		if(is_array($result_seekpoint))
		{
			$seekpoint_xml='<metadata>';
			foreach ($result_seekpoint as $value)
			{
				$seekpoint_xml.='<item begin="'.$value['nns_begin'] .'" end="' . $value['nns_end'] . '" type="'.$value['nns_type'].'" name="'.$value['nns_name'].'" img="'.$value['nns_image'].'" ></item>';
			}
			$seekpoint_xml.='</metadata>';
		}

		$sp_config = sp_model::get_sp_config($sp_id);
		if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0){
			switch ($sp_config['import_id_mode']) {
				case '1':
					$index_id = '001000'.$info['nns_integer_id'];
					$index_id = str_pad($index_id,32,'0');
					break;
				case '2':
					$index_id = $info['nns_import_id'];
					break;
				case '3':
					$index_id = CSP_ID.'001'.$info['nns_integer_id'];
					$index_id = str_pad($index_id,32,'0');
					break;
			}
		}

		if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0){
			$video_info = video_model::get_vod_info($info['nns_vod_id']);
			switch ($sp_config['import_id_mode']) {
				case '1':
					$vod_id = '000000'.$video_info['nns_integer_id'];
					$vod_id = str_pad($vod_id,32,"0");
					break;
				case '2':
					$vod_id = $video_info['nns_asset_import_id'];
					break;
				case '3':
					$vod_id = CSP_ID.'000'.$video_info['nns_integer_id'];
					$vod_id = str_pad($vod_id,32,"0");
					break;
			}
		}

		$info['nns_summary'] = str_replace('"', '', $info['nns_summary']);
		$info['nns_summary'] = str_replace("&", '', $info['nns_summary']);
		$info['nns_summary'] = str_replace("《", '', $info['nns_summary']);
		$info['nns_summary'] = str_replace("》", '', $info['nns_summary']);
		$info['nns_actor'] = htmlspecialchars(str_replace("&", '', $info['nns_actor']), ENT_QUOTES);
		
		$params = array(
			'clip_id' => $index_id,
			'name' => htmlspecialchars($info['nns_name'], ENT_QUOTES),
			'index' => $info['nns_index'],
			'time_len' => $info['nns_time_len'],
			'summary' => htmlspecialchars($info['nns_summary'], ENT_QUOTES),
			'pic' => self::get_image_url(str_replace("JPG", "jpg", $info['nns_image']),$sp_id),
			'director' => htmlspecialchars(str_replace("/", ",", $info['nns_director']), ENT_QUOTES),
			'player' => htmlspecialchars(str_replace("/", ",", $info['nns_actor']), ENT_QUOTES),
			'total_clicks' => $info['nns_play_count'],
			'release_time' => $info['nns_release_time'],
			'update_time' => $info['nns_update_time'],
			'asset_source' => $info['nns_cp_id'],
			'month_clicks' => '',
			'week_clicks' => '',
			'day_clicks' => '',
			'original_id' => $info['nns_import_id'],
		);

		$sql_ex = "SELECT * FROM `nns_vod_index_ex` where nns_vod_index_id='{$info['nns_import_id']}'";
		$index_ex = nl_db_get_all($sql_ex, $db_r);
		if(is_array($index_ex))
		{
			foreach ($index_ex as $value)
			{
				if($value['nns_key'] === 'isintact')
				{
					$params['isintact'] = 1;
				}
				else
				{
					$params[$value['nns_key']] = htmlspecialchars($value['nns_value'], ENT_QUOTES);
				}
			}
		}	

		$xml_str = self::build_c2_xml($params,'index');
		$time = date('YmdHis') . rand(0, 10000);
		$three_rand = rand(100, 999);
		$file_name = 'Program_'.$time.'_'.$index_id.'_'.$three_rand.'.xml';
		$c2_info = array(
			'nns_task_type'=>'Program',
			'nns_task_id'=> isset($index_info['nns_task_id']) ? $index_info['nns_task_id'] : null,
			'nns_task_name'=> isset($index_info['nns_task_name']) ? $index_info['nns_task_name'] : null,
			'nns_action'=> $action,
			'nns_url' => $file_name,
			'nns_content' => $xml_str,
			'nns_desc' => 'Program,'.$action,
		);

		$cp_id = isset($info['nns_cp_id']) ? $info['nns_cp_id'] : null;
		return self::execute_c2($c2_info,$cp_id,$sp_id,$vod_id,$index_id,null,$seekpoint_xml);
	}

	/**
	 * 删除分集
	 */
	static public function delete_vod($index_info,$sp_id)
	{
		$action = 'DELETE';
		$db_w = nn_get_db(NL_DB_READ);
		$index_id = $index_info['nns_id'];

		$info = video_model::get_vod_index_info($index_id);
		$sp_config = sp_model::get_sp_config($sp_id);
		if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0){
			switch ($sp_config['import_id_mode']) {
				case '1':
					$index_id = '001000'.$info['nns_integer_id'];
					$index_id = str_pad($index_id,32,'0');
					break;
				case '2':
					$index_id = $info['nns_import_id'];
					break;
				case '3':
					$index_id = CSP_ID.'001'.$info['nns_integer_id'];
					$index_id = str_pad($index_id,32,'0');
					break;
			}
		}
		$time = date('YmdHis') . rand(0, 10000);
		$three_rand = rand(100, 999);
		$file_name = 'Program_delete_'.$time.'_'.$index_id.'_'.$three_rand.'.xml';
		$c2_info = array(
			'nns_task_type'=>'Program',
			'nns_task_id'=> isset($index_info['nns_task_id']) ? $index_info['nns_task_id'] : null,
			'nns_task_name'=> isset($index_info['nns_task_name']) ? $index_info['nns_task_name'] : null,
			'nns_action'=> $action,
			'nns_url' => $file_name,
			'nns_content' => '',
			'nns_desc' => 'Program,'.$action,
		);

		$cp_id = isset($info['nns_cp_id']) ? $info['nns_cp_id'] : null;
		return self::execute_c2($c2_info, $cp_id, $sp_id, null, $index_id);
	}

	/**
	 * 注入片源（以分集为单位,切片完成后调用）
	 * $index_media array(
	 * 			'nns_video_id'
	 * 			'nns_video_index_id'
	 * 			'nns_task_id'
	 * 			'nns_new_dir'
	 * 			'nns_media_ids'
	 * )
	 */
	static public function add_movies_by_vod_index($index_media,$sp_id)
	{
		include_once dirname(dirname(__FILE__)) . '/define.php';
		$db_r = nn_get_db(NL_DB_READ);
		
		$media_id = $index_media['nns_media_id'];
		$info = video_model::get_vod_media_info($media_id);
		$index_id = $info['nns_vod_index_id'];
		$vod_id = $info['nns_vod_id'];
		$sp_config = sp_model::get_sp_config($sp_id);
		if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0){
			switch ($sp_config['import_id_mode']) {
				case '1':
					$media_id = '002000'.$info['nns_integer_id'];
					$media_id = str_pad($media_id,32,"0");
					break;
				case '2':
					$media_id = $info['nns_import_id'];
					break;
				case '3':
					$media_id = CSP_ID.'002'.$info['nns_integer_id'];
					$media_id = str_pad($media_id,32,"0");
					break;
			}
		}

		if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0){
			$index_info = video_model::get_vod_index_info($info['nns_vod_index_id']);
			switch ($sp_config['import_id_mode']) {
				case '1':
					$index_id = '001000'.$index_info['nns_integer_id'];
					$index_id = str_pad($index_id,32,"0");
					break;
				case '2':
					$index_id = $index_info['nns_import_id'];
					break;
				case '3':
					$index_id = CSP_ID.'001'.$index_info['nns_integer_id'];
					$index_id = str_pad($index_id,32,"0");
					break;
			}
		}

		if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0){
			$video_info = video_model::get_vod_info($info['nns_vod_id']);
			switch ($sp_config['import_id_mode']) {
				case '1':
					$vod_id = '000000'.$video_info['nns_integer_id'];
					$vod_id = str_pad($vod_id,32,"0");
					break;
				case '2':
					$vod_id = $video_info['nns_asset_import_id'];
					break;
				case '3':
					$vod_id = CSP_ID.'000'.$video_info['nns_integer_id'];
					$vod_id = str_pad($vod_id,32,"0");
					break;
			}
		}
		//获取c2
		$c2_sql = "select * from nns_mgtvbk_c2_task where nns_id='{$index_media['nns_task_id']}'";
		$c2_task_info = nl_db_get_one($c2_sql, $db_r);
		if (empty($c2_task_info))
		{
			return false;
		}
		
		$drm_enabled = get_drm_config("g_drm_enable");
		if (!isset($sp_config['import_srouce_url']) || (isset($sp_config['import_srouce_url']) && (int)$sp_config['import_srouce_url'] === 1))
		{
			$info['nns_url'] = $c2_task_info['nns_file_path'];
		}
		$params = array(
			'file_id' => $info['nns_id'],
			'file_type' => $info['nns_filetype'],
			'file_name' => htmlspecialchars($c2_task_info['nns_name'], ENT_QUOTES),
			'file_path' => $info['nns_url'],
			'file_definition' => $info['nns_mode'],
			'file_resolution' => $info['nns_file_resolution'],
			'file_size' => $info['nns_file_size'],
			'file_bit_rate' => $info['nns_kbps'],
			'file_desc' => '',
			'original_id' => $info['nns_import_id'],
			'ex_url' => $c2_task_info['nns_ex_url'],
			'policy' => $c2_task_info['nns_cdn_policy'],
			'asset_source' => $info['nns_cp_id'],
		);

		//增加DRM加密及方案
// 		if((!isset($sp_config['main_drm_enabled']) || intval($sp_config['main_drm_enabled']) == 1) && (!isset($sp_config['q_disabled_drm']) || intval($sp_config['q_disabled_drm']) == 0) && $drm_enabled == 1)
		if(isset($sp_config['main_drm_enabled']) && intval($sp_config['main_drm_enabled']) == 1)
		{
			//开启DRM
			$params['drm_flag'] = $drm_enabled;
			$params['drm_encrypt_solution'] = $sp_config['q_drm_identify'];
		}
		else
		{
			$params['drm_flag'] = 0;
			$params['drm_encrypt_solution'] = '';
		}

		$sql_ex = "SELECT * FROM `nns_vod_media_ex` where nns_vod_media_id='{$info['nns_import_id']}'";
		$media_ex = nl_db_get_all($sql_ex, $db_r);
		if(is_array($media_ex))
		{
			foreach ($media_ex as $value)
			{
				$params[$value['nns_key']] = htmlspecialchars($value['nns_value'], ENT_QUOTES);
			}
		}

		$file_3d = null;
		$file_3d_type = null;
		if ($info['nns_dimensions'] == '0')
		{
			$file_3d = 0;
			$file_3d_type = '';
		}
		elseif ($info['nns_dimensions'] == '100001')
		{
			$file_3d = 1;
			$file_3d_type = 0;
		}
		elseif ($info['nns_dimensions'] == '100002')
		{
			$file_3d = 1;
			$file_3d_type = 1;
		}
		else
		{
			$file_3d = 0;
			$file_3d_type = '';
		}
		$params['file_3d'] = $file_3d;
		$params['file_3d_type'] = $file_3d_type;

		$xml_str = self::build_c2_xml($params,'media');
		
		$time = date('YmdHis') . rand(0, 10000);
		$three_rand = rand(100, 999);
		$file_name = 'Movie_'.$time.'_'.$media_id.'_'.$three_rand.'.xml';
		$action = 'REGIST';
		$c2_info = array(
			'nns_task_type'=> 'Movie',
			'nns_task_id'=> isset($index_media['nns_task_id']) ? $index_media['nns_task_id'] : null,
			'nns_task_name'=> isset($c2_task_info['nns_name']) ? $c2_task_info['nns_name'] : null,
			'nns_action'=> $action,
			'nns_url' => $file_name,
			'nns_content' => $xml_str,
			'nns_desc' => 'Movie,'.$action,
		);

		$cp_id = isset($info['nns_cp_id']) ? $info['nns_cp_id'] : null;
		return self::execute_c2($c2_info,$cp_id,$sp_id,$vod_id,$index_id,$media_id,null,$c2_task_info['nns_cdn_policy']);
	}

	/**
	 * 删除影片
	 * params array(
	 * nns_id,
	 * nns_task_id
	 * )
	 */
	static public function delete_movie($params,$sp_id)
	{
		$action = 'DELETE';
		$media_id = $params['nns_id'];
		//兼容下
		$media_id = $media_id ? $media_id : $params['nns_media_id'];

		$info = video_model::get_vod_media_info($media_id);
		$sp_config = sp_model::get_sp_config($sp_id);
		if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0){
			switch ($sp_config['import_id_mode']) {
				case '1':
					$media_id = '002000'.$info['nns_integer_id'];
					$media_id = str_pad($media_id,32,"0");
					break;
				case '2':
					$media_id = $info['nns_import_id'];
					break;
				case '3':
					$media_id = CSP_ID.'002'.$info['nns_integer_id'];
					$media_id = str_pad($media_id,32,"0");
					break;
			}
		}
		$time = date('YmdHis') . rand(0, 10000);
		$three_rand = rand(100, 999);
		$file_name = 'Movie_delete_'.$time.'_'.$media_id.'_'.$three_rand.'.xml';
		$c2_info = array(
			'nns_task_type'=>'Movie',
			'nns_task_id'=> isset($params['nns_task_id'])?$params['nns_task_id']:null,
			'nns_task_name'=> isset($params['nns_task_name'])?$params['nns_task_name']:null,
			'nns_action'=> $action,
			'nns_url' => $file_name,
			'nns_content' => '',
			'nns_desc' => 'Movie,'.$action,
		);

		$cp_id = isset($info['nns_cp_id']) ? $info['nns_cp_id'] : null;
		return self::execute_c2($c2_info,$cp_id,$sp_id,null,null,$media_id);
	}

	/**
	 * 执行C2注入命令
	 */
	static public function execute_c2($c2_info,$cp_id=null,$sp_id,$vod_id=null,$index_id=null,$media_id=null,$seekpoint_xml=null,$cdn_policy=null)
	{
		include_once dirname(dirname(dirname(__FILE__))).'/import/import.class.php';
		include_once dirname(dirname(dirname(__FILE__))).'/models/queue_task_model.php';
		include_once dirname(dirname(__FILE__)) . '/define.php';
		$db_w = nn_get_db(NL_DB_WRITE);
		$url = CDN_SEND_MODE_HTTP_URL;
		$log = IMPROT_EPG_LOG;
		$import_obj = new import($url, $log, '', $cp_id);
		$c2_id = np_guid_rand();
		$sub_path = date('Y-m-d');
		$file = $c2_info['nns_url'];
		$c2_info['nns_url'] = $sub_path.'/'.$file;
		$xml_str = $c2_info['nns_content'];
		$file_path = dirname(dirname(dirname(dirname(__FILE__)))).'/data/mgtv/'.$sp_id.'/inject_xml/'.$sub_path.'/';
		$notify_path = dirname(dirname(dirname(dirname(__FILE__)))).'/data/mgtv/'.$sp_id.'/notify/'.$sub_path.'/';
		if(!is_dir($file_path)) 
		{
			mkdir($file_path,0777,true);
		}
		if(!is_dir($notify_path)) 
		{
			mkdir($notify_path,0777,true);
		}
		$file_path .= $file;
		file_put_contents($file_path,$xml_str);
		$c2_info['nns_id'] = $c2_id;

		try {
			np_runtime_ex($c2_id,false);

			switch ($c2_info['nns_task_type']) 
			{
				case 'Series':
					if($c2_info['nns_action'] != 'DELETE')
					{
						$result = $import_obj->import_assets($xml_str, $vod_id);
					}
					else 
					{
						$result = $import_obj->delete_assets($vod_id);
					}
					break;
				case 'Program':
					if($c2_info['nns_action'] != 'DELETE')
					{
						$result = $import_obj->import_video_clip($vod_id, $index_id, $xml_str,0,$seekpoint_xml);
					}
					else 
					{
						$result = $import_obj->delete_video_clip($index_id);
					}
					break;
				case 'Movie':
					if($c2_info['nns_action'] != 'DELETE')
					{
						$result = $import_obj->import_video_file($vod_id, $index_id, $media_id, $xml_str, 0, $cdn_policy);
					}
					else 
					{
						$result = $import_obj->delete_video_file($media_id);
					}
					break;
			}
			$re = json_decode($result, true);
			//$re = array('ret' => 0,'reason' => 'success');
			$notify_xml = self::build_c2_xml($re,'result');
			$notify_path .= $file;
			file_put_contents($notify_path,$notify_xml);	
			$c2_info['nns_notify_content'] = $c2_info['nns_url'];
			$bool = false;
			if ((int)$re['ret'] === 0)
			{
				$c2_info['nns_result'] = '[0]';
				$c2_info['nns_notify_result'] = 0;
				$bool = true;
			}
			elseif ((int)$re['ret'] === 7)
			{
				$c2_info['nns_result'] = '[0]';
				$c2_info['nns_notify_result'] = 0;
				$bool = true;
			}
			else
			{
				//发送成功,但是通知失败
				$c2_info['nns_result'] = '[0]';
				$c2_info['nns_notify_result'] = -1;
			}

			$c2_info['nns_notify_time'] = date('Y-m-d H:i:s',time());
			if($bool)
			{
				$sql = "update nns_mgtvbk_c2_task set nns_status=0,nns_modify_time=now()  where nns_id='{$c2_info['nns_task_id']}'";
				nl_execute_by_db($sql, $db_w);
			}
			$queue_task_model = new queue_task_model();
			$queue_task_model->q_report_task($c2_info['nns_task_id']);
			$queue_task_model = null;
			self::save_c2_log($c2_info,$sp_id);
			return $bool;
		}
		catch (Exception $e) 
		{
			DEBUG && i_write_log_core(var_export($e,true),$sp_id.'/c2');
			$c2_info['nns_result'] = '[-1]';
			self::save_c2_log($c2_info,$sp_id);
			return false;
		}
	}

	/**
	 * 保存C2命令日志
	 * $c2_log array(
	 * 			'nns_id',
	 * 			'nns_type',
	 * 			'nns_org_id',
	 * 			'nns_video_id',
	 * 			'nns_video_type',
	 * 			'nns_video_index_id',
	 * 			'nns_media_id',
	 * 			'nns_url',
	 * 			'nns_content',
	 * 			'nns_result',
	 * 			'nns_desc',
	 * 			'nns_create_time'
	 * )
	 */
	static public function save_c2_log($c2_log,$sp_id)
	{
		$db = nn_get_db(NL_DB_WRITE);
		$c2_log['nns_type'] = 'std';
		$c2_log['nns_org_id'] = $sp_id;
		$dt = date('Y-m-d H:i:s');
		$c2_log['nns_create_time'] = $dt;
		$c2_log['nns_send_time'] = $dt;
		return nl_db_insert($db,'nns_mgtvbk_c2_log',$c2_log);
	}

	static public function build_c2_xml($params,$type='result')
	{
		if(!is_array($params))
		{
			return false;
		}
		$dom = new DOMDocument('1.0', 'utf-8');
		$xml_header = "<" . $type . "/>";
		$dom->loadXML($xml_header);
		foreach ($params as $key=>$val)
		{
			$dom->firstChild->setAttribute($key, $val);
		}		
		return $dom->saveXML();
	}
}
