<?php
header("Content-Type:text/html;charset=utf-8");
set_time_limit(0);
ini_set('display_errors', 1);
include_once dirname(dirname(__FILE__)) . '/nn_logic/nl_common.func.php';
if (isset($_POST['anction_flag']) && $_POST['anction_flag'] == 'anction_flag')
{
	include_once dirname(__FILE__) . '/mgtv_init.php';
	include_once dirname(__FILE__).'/mgtv_const.php';
	include_once dirname(dirname(__FILE__)).'/op/op_queue.const.php';
	include_once dirname(dirname(__FILE__)).'/nn_logic/c2_task/c2_task.class.php';
	if(!isset($_POST['sp_id']) && strlen($_POST['sp_id']) <1)
	{
		echo "<script>alert('SP ID 传入为空');history.go(-1);</script>";
		die();
	}
	$nns_task_id = @$_POST['nns_task_id'];
	if(empty($nns_task_id))
	{
		echo "<script>alert('task id 为空');history.go(-1);</script>";
		die();
	}
	$sp_id=$_POST['sp_id'];
	if(!file_exists(dirname(__FILE__) . '/'.$sp_id.'/init.php'))
	{
		echo "<script>alert('不存在SP 配置定义文件内容');history.go(-1);</script>";
		die();
	}
	$dc = nl_get_dc(array (
			'db_policy' => NL_DB_WRITE,
			'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
	));
	include_once dirname(__FILE__) . '/'.$sp_id.'/init.php';
	$sp_config = sp_model::get_sp_config($sp_id);
	if (isset($sp_config['disabled_cdn']) && (int)$sp_config['disabled_cdn'] === 1) {
		echo "<script>alert('cdn 注入开关关闭不允许注入cdn!')</script>";
		die();
	}
	switch (@$_POST['task_type'])
	{
		case 'task_id':
			break;
		case 'log_id':
			$result_task_log = nl_c2_log::query_data_by_id($dc, $_POST['nns_task_id']);
			if($result_task_log['ret'] !=0)
			{
				echo "<script>alert('" . $result_task_log['reason'] . "');history.go(-1);</script>";
				die();
			}
			if(!isset($result_task_log['data_info'][0]) || !is_array($result_task_log['data_info'][0]) || empty($result_task_log['data_info'][0]))
			{
				echo "<script>alert('查无task数据，请检查');history.go(-1);</script>";
				die();
			}
			$nns_task_id = $result_task_log['data_info'][0]['nns_task_id'];
			break;
		default:
			echo "<script>alert('id 类型错误')</script>";
			die();
	}
	$result_task = nl_c2_task::query_data_by_id($dc, $nns_task_id);
	if($result_task['ret'] !=0)
	{
		echo "<script>alert('" . $result_task['reason'] . "');history.go(-1);</script>";
		die();
	}
	if(!isset($result_task['data_info'][0]) || !is_array($result_task['data_info'][0]) || empty($result_task['data_info'][0]))
	{
		echo "<script>alert('查无task数据，请检查');history.go(-1);</script>";
		die();
	}
	$result_task = $result_task['data_info'][0];
	$rand_num = rand(1000,9999);
	$file_name = 'testcdn_'. date('YmdHis') .'_'. $rand_num.'_' . $_POST['nns_task_type'] .'_'. $_POST['nns_action'] .'_'.$result_task['nns_id']. '.xml';
	$c2_info = array(
			'nns_task_type'=>$_POST['nns_task_type'],
			'nns_task_id'=>$result_task['nns_id'],
			'nns_task_name'=>$result_task['nns_name'],
			'nns_action'=>$_POST['nns_action'],
			'nns_url' => $file_name,
			'nns_content' =>$_POST['nns_content'],
			'nns_desc' => $_POST['nns_task_type'].','.$_POST['nns_action'],
	);
	$re = c2_task_model::execute_c2($c2_info,$sp_id);
	var_dump($re);die;
}
else
{
	include_once dirname(dirname(__FILE__)) . '/nn_logic/sp/sp.class.php';
	$dc = nl_get_dc(array (
			'db_policy' => NL_DB_READ, 
			'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
	));
	$result_sp = nl_sp::query_all($dc);
	if ($result_sp['ret'] != 0)
	{
		echo "<script>alert('" . $result_sp['reason'] . "')</script>";
		die();
	}
	?>
<html>
<head>
<meta http-equiv=Content-Type content="text/html;charset=utf-8">
</head>
<body>
	CDN 模拟数据注入
	<hr>
	<form action="./cdn_test_import.php" method="post">
		<input type="hidden" name="anction_flag" value="anction_flag"> 选择SP商： <select name="sp_id">
    <?php
	
		if (isset($result_sp['data_info']) && is_array($result_sp['data_info']) && !empty($result_sp['data_info']))
		{
			foreach ($result_sp['data_info'] as $cp_val)
			{
	?>
				<option value='<?php echo $cp_val['nns_id'];?>'><?php echo $cp_val['nns_name']; ?></option>
	<?php
			}
		}
	?>
    </select> <br /> 选择注入类型： <select name="nns_task_type">
			<option value="Series">Series</option>
			<option value="Program">Program</option>
			<option value="Movie">Movie</option>
			<option value="Category">Category</option>
		</select> <br /> 选择注入行为： <select name="nns_action">
			<option value="REGIST">注册</option>
			<option value="UPDATE">更新</option>
			<option value="DELETE">删除</option>
		</select> <br /> 
		
		选择上传id类型：
		<input type="radio" name="task_type" value="log_id" checked/> CDN注入日志GUID
		<input type="radio" name="task_type" value="task_id" /> C2 task GUID
		<br/>
		ID值：<input type="text" name="nns_task_id" /> <br>
		注入XML结构： <br>
		<textarea name="nns_content" rows="40" cols="150">
        	<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
				BizDomain="0" Priority="1">
				<Objects>
					<Object ElementType="Series" ID="Umai:SERI/1000002681@STARCOR.SMG"
				Action="REGIST" Code="Umai:SERI/1000002681@STARCOR.SMG">
						<Property Name="Name">恐怖角</Property>
						<Property Name="OrderNumber" />
						<Property Name="OriginalName">恐怖角</Property>
						<Property Name="SortName" />
						<Property Name="SearchName" />
						<Property Name="OrgAirDate">19610101</Property>
						<Property Name="LicensingWindowStart">20160513134201</Property>
						<Property Name="LicensingWindowEnd">20190513134201</Property>
						<Property Name="DisplayAsNew">0</Property>
						<Property Name="DisplayAsLastChance">300</Property>
						<Property Name="Macrovision">0</Property>
						<Property Name="Price">0</Property>
						<Property Name="VolumnCount">1</Property>
						<Property Name="Status">1</Property>
						<Property Name="Description">
						فىلىمدە ئەمدى تۈرمىدىن چىققان جىنايەتچى سەككىز يىل بۇرۇن ئۆزىنى باسقۇنچىلىق قىلىش جىنايىتى بىلەن قارلىغان ئادۇكاتنى ئىزدەپ ئۇنىڭدىن قىساس ئالماقچى بولىدۇ،بۇ جىنايەتچىنىڭ ئادۇكاتقا ۋە ئۇنىڭ ئائىلىسىدىكىلەرگە قانداق قەست قىلدىغانلىقىغا،شۇنداقلا ئۇنىڭ رەزىل نىيتىنىڭ ئىشقا ئاشىدىغان ئاشمايدىغانلىقىغا قىززىقسىڭىز نەزىرىڭىز فىلىمنىڭ نادىر مەزمۇنلىرىدا بولسۇن...
						</Property>
						<Property Name="Type">ياۋرو-ئامېرىكا</Property>
						<Property Name="Keywords" />
						<Property Name="Tags">0</Property>
						<Property Name="Reserve1" />
						<Property Name="Reserve2" />
						<Property Name="Reserve3" />
						<Property Name="Reserve4" />
						<Property Name="Reserve5" />
					</Object>
				</Objects>
			<Mappings>
				<Mapping ParentType="Package" ParentID="KKTV" ElementType="Series"
				ElementID="Umai:SERI/1000002681@STARCOR.SMG" Action="REGIST"
				ParentCode="KKTV" ElementCode="Umai:SERI/1000002681@STARCOR.SMG">
					<Property Name="ValidStart">20140101000000</Property>
					<Property Name="ValidEnd">20540101000000</Property>
				</Mapping>
			</Mappings>
		</ADI>
    </textarea>
		<button teyp="submit">提交</button>
	</form>
</body>
</html>
<?php }?>