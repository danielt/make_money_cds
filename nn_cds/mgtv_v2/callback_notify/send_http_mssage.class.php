<?php
class send_http_mssage
{
	private $import_url = null;   				//媒资注入接口地址
	private $log        = null;   				//媒资注入日志
	private $curl       = null;					//CURL 封装类对象
	private $result     = array();    			//接口处理结果,返回状态
	private $log_file   = null;
	private $sp_id  = null;
	
	public function __construct($import_url,$sp_id='')
	{
		$this->import_url = $import_url;
		$this->sp_id = $sp_id;
		//CURL 实例化
		$curl_path = dirname(dirname(dirname(dirname(__FILE__)))).'/np/np_http_curl.class.php';
		if(file_exists($curl_path))
		{
			include_once $curl_path;
			$this->curl = new np_http_curl_class();
		}
		else
		{
			$this->result  = array(
					'code' => 10000,
					'reason' => '未定义CURL封装类',
					"id" => ''
			);
			return $this->return_xml($this->result);
		}
	}
	
	/**
	 * curl媒资上下线反馈给上游
	 * @param string $str_xml xml数据
	 * @param string $message_id 消息id
	 * @author liangpan
	 * @date 2015-11-26
	 */
	public function up_callback_notify($str_xml,$message_id)
	{
		$data = array(
			'cmsresult'=>$str_xml,
		);
		$asset_ret = $this->curl->post($this->import_url,$data);
		//判断CURL状态，如果不为200记录错误信息
		$curl_info = $this->curl->curl_getinfo();
		$this->result['data'] 	= $message_id;
		if($curl_info['http_code'] != '200')
		{
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问媒资上下线反馈失败,HTTP状态码'.$curl_info['http_code'];
			$err_msg = '[访问媒资信息注入失败,CURL接口地址]：'.$this->import_url."\r\n".'[HTTP状态码]：'.$curl_info['http_code']."\r\n".' [HTTP错误]：'.$this->curl->curl_error();
			nn_log::write_log_message(LOG_MODEL_NOTIFY,$err_msg,$this->sp_id,VIDEO_TYPE_VIDEO);
			nn_log::write_log_message(LOG_MODEL_NOTIFY,"原因".var_export($this->result,true),$this->sp_id,VIDEO_TYPE_VIDEO);
			return $this->return_xml($this->result);
        }

		//解析CURL返回结果
		if($asset_ret == '1')
		{
			$err_msg = date('Y-m-d H:s:i').'[访问媒资上下线反馈成功,CURL接口地址]：'.$this->import_url."\n".date('Y-m-d H:s:i').'[接口返回结果]：'.$asset_ret;
			$this->result['reason'] = '访问媒资上下线反馈成功';
			$this->result['code'] = 0;
		}
		else
		{
			$err_msg = date('Y-m-d H:s:i').'[访问媒资上下线反馈失败,CURL接口地址]：'.$this->import_url."\n".date('Y-m-d H:s:i').'[接口返回结果]：'.$asset_ret;
			$this->result['reason'] = '访问媒资上下线反馈失败';
			$this->result['code'] = 1;
		}
		//记录日志
		//nn_log::write_log_message(LOG_MODEL_NOTIFY,"原因".var_export($asset_ret,true),$this->sp_id,VIDEO_TYPE_VIDEO);
		$this->result['data'] 	= $message_id;
		return $this->return_xml($this->result);
	}
	/**
	 * 传入参数返回xml
	 * @param array $params:代码数组
	 * @return string(xml)
	 */
	private function return_xml($params = array()) {
		$array = array('ret'=>$params['code'],'reason'=>$params['reason'],'data'=>$params['data']);
		return json_encode($array);
		//return "<result  ret=\"{$params['code']}\"  reason=\"{$params['reason']}\" data='{$params['data']}'></result>";
	}

	/**
	 * curl媒资上下线反馈给上游
	 * @param string $str_xml xml数据
	 * @param string $message_id 消息id
	 * @author liangpan
	 * @date 2015-11-26
	 */
	public function up_callback_notify_v2($str_xml,$message_id,$format='xml')
	{
		if($format == 'json')
		{
//			$data = array
//			(
//				'cmsresult'=>$str_xml,
//			);
			$data = "cmsresult=".$str_xml;
		}
		else
		{
			$data = array(
				'cmsresult'=>$str_xml,
			);
		}
//		echo $data;die;
		$asset_ret = $this->curl->post($this->import_url,$data);
		$asset_ret = json_decode($asset_ret,true);
		//判断CURL状态，如果不为200记录错误信息
		$curl_info = $this->curl->curl_getinfo();
		$this->result['data'] 	= $message_id;
		if($curl_info['http_code'] != '200')
		{
			$this->result['code'] 	= 10002;
			$this->result['reason'] = '访问媒资上下线反馈失败,HTTP状态码'.$curl_info['http_code'];
			$err_msg = '[访问媒资信息注入失败,CURL接口地址]：'.$this->import_url."\r\n".'[HTTP状态码]：'.$curl_info['http_code']."\r\n".' [HTTP错误]：'.$this->curl->curl_error();
			nn_log::write_log_message(LOG_MODEL_NOTIFY,$err_msg,$this->sp_id,LOG_PASS_QUEUE_MODEL);
			nn_log::write_log_message(LOG_MODEL_NOTIFY,"原因".var_export($this->result,true),$this->sp_id,LOG_PASS_QUEUE_MODEL);
			return $this->return_xml($this->result);
		}

		//解析CURL返回结果
		if($asset_ret['result'] == '1')
		{
			$err_msg = date('Y-m-d H:s:i').'[访问媒资上下线反馈成功,CURL接口地址]：'.$this->import_url."\n".date('Y-m-d H:s:i').'[接口返回结果]：'.$asset_ret;
			$this->result['reason'] = '访问媒资上下线反馈成功';
			$this->result['code'] = 0;
		}
		else
		{
			$err_msg = date('Y-m-d H:s:i').'[访问媒资上下线反馈失败,CURL接口地址]：'.$this->import_url."\n".date('Y-m-d H:s:i').'[接口返回结果]：'.$asset_ret;
			$this->result['reason'] = '访问媒资上下线反馈失败';
			$this->result['code'] = 1;
		}
		//记录日志
		nn_log::write_log_message(LOG_MODEL_NOTIFY,"原因".var_export($asset_ret,true),$this->sp_id,LOG_PASS_QUEUE_MODEL);
		$this->result['data'] 	= $message_id;
		return $this->return_xml($this->result);
	}
}
