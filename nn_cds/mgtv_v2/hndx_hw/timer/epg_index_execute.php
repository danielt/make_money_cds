<?php
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
define('ORG_ID', 'hndx_hw');
include_once dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/init.php';

class epg_index_execute extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        if($this->check_linux_course_v2())
        {
            do_timer_action();
        }
        $this->msg('执行结束...');
    }
    /**
     * 判断其他sp有相同定时器在执行
     * @param string $str_course_file_name 定时器脚本  进程查询的 相对|绝对路径
     * @return boolean true | false
     * @author liangpan
     * @date 2015-08-05
     */
    public function check_linux_course_v2()
    {
        $str_course_file_name = dirname(dirname(dirname(__FILE__))) . '/hndx/timer/epg_index_execute.php';

        $this->msg("-------进程查询处理开始-------");
        $str_course_file_name = "ps -ef | grep " . $str_course_file_name . " | grep -v grep | awk '{print $2}'";
        $this->msg("进程查询的命令为：" . $str_course_file_name);
        @exec($str_course_file_name,$arr_course,$exec_result);
        $this->msg("进程查询的结果为：" . var_export($arr_course,true));
        if($exec_result != 0)
        {
            global $g_ignore_exec_error;
            $return = $g_ignore_exec_error ? false : true;
            unset($g_ignore_exec_error);
            $str_desc = $return ? "程序忽略定时器进程控制，继续执行" : "程序需要定时器进程控制，停止执行";
            $this->msg("进程查询php报错，可能是关闭了exec，也可能没在linux环境运行....".$str_desc);
            return $return;
        }
        if (!empty($arr_course))
        {
            $count_course =count($arr_course);

            $this->msg("运行的进程有".$count_course."个");
            return false;
        }
        else
        {
            $this->msg("一个进程都没开启，继续执行");
            return true;
        }
        $this->msg("-------进程查询处理结束-------");
    }
}
$epg_index_execute = new epg_index_execute("epg_index_execute", ORG_ID, __FILE__);
$epg_index_execute->run();

function do_timer_action()
{
    //从任务表取内容，调用C2执行
    i_echo('start');
    $sp_config = sp_model::get_sp_config(ORG_ID);
    if(isset($sp_config['disabled_epg'])&&(int)$sp_config['disabled_epg']===1){
        i_echo('close epg');
    }else{
        $queue_task_model = new queue_task_model();
        $sql = $queue_task_model->get_epg_sql(ORG_ID, 'index');
        echo $sql;
        if($sql!==null){
            $db = nn_get_db(NL_DB_READ);
            $c2_list = nl_db_get_all($sql, $db);
            if(is_array($c2_list)&&count($c2_list)>0){
                foreach($c2_list as $c2_info){
                    import_model::import_clip($c2_info['nns_id']);
                }
            }
        }
        i_echo('end');
    }
}