<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors',1);
set_time_limit(0); 
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_init.php';
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
array_pop($arr_dir);
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once  dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/init.php';
include_once dirname(dirname(dirname(__FILE__))) .'/models/queue_task_model.php';

class get_task extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        do_timer_action();
        $this->msg('执行结束...');
    }
}
$get_task = new get_task("get_task", ORG_ID);
$get_task->run();

function do_timer_action()
{
    i_echo('start');
    $sp_config = sp_model::get_sp_config(ORG_ID);
    if(isset($sp_config['op_pre_max'])&&(int)$sp_config['op_pre_max']>0){
        $max_num = $sp_config['op_pre_max'];
    }else{
        $max_num = 30;
    }
    
    echo $max_num;
    $db = nn_get_db(NL_DB_READ);
    //begin
    $new_str = "";    
    if(!isset($sp_config['disabled_cdn']) || (int)$sp_config['disabled_cdn'] === 0)//开启CDN注入
    {
    	$c2_count_model = explode(',', $sp_config['c2_count_mode']);//c2计算模式，多个以逗号分隔
    	if (!empty($c2_count_model))
    	{
	    	foreach ($c2_count_model as $val)
	    	{
	    	//0:等待执行;1:执行失败;2:正在执行;3:等待获取CDN播放串;4:注入取消;5:正在获取CDN播放串;6:获取播放传失败(多个以逗号,分隔)
	    		switch ($val)
	    		{
	    			case '0':
	    				$new_str .= 1 . ',';
	    				break;
	    			case '1':
	    				$new_str .= -1 . ',';
	    				break;
	    			case '2':
	    				$new_str .= 5 . ',';
	    				break;
	    			case '3':
	    				$new_str .= 6 . ',';
	    				break;
	    			case '4':
	    				$new_str .= 7 . ',';
	    				break;
	    			case '5':
	    				$new_str .= 8 . ',';
	    				break;
	    			case '6':
	    				$new_str .= 9 . ',';
	    				break;
	    		}
	    	}
	    	$query_str = "and nns_status in (" . rtrim($new_str,',') .")  ";
    	}
    	else 
    	{
    		$query_str = "and nns_epg_status=97 ";
    	}
    }
    else 
    {
    	$query_str = "and nns_epg_status=97 ";
    }
    $sql_cont = "select count(*) as num from nns_mgtvbk_c2_task where nns_org_id='".ORG_ID."'    $query_str";
    $num = nl_db_get_col($sql_cont, $db);
    echo $num;
    //end
    if($max_num>$num){
        $queue_task_model = new queue_task_model();
        $query_num = $max_num-$num;
        if($query_num>0){
            $queue_task_model->q_get_task(ORG_ID,$query_num);
        }
    }
    unset($queue_task_model,$sp_config);
    i_echo('end');
}
