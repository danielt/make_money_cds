<?php
ini_set('display_errors',1);
include_once dirname(dirname(__FILE__)) . '/mgtv_init.php';
include_once dirname(__FILE__).'/models/child_c2_task_model.php';

$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORGID', $str_dir);
if(isset($_POST['action']) && !empty($_POST['action']))
{
	$bind_vod_id = $_POST['parentcode'];	
	$bind_index_ids = $_POST['childcode'];
	child_c2_task_model::add_index_bind_vod($bind_vod_id,$bind_index_ids,ORGID);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MAPPING关系</title>
</head>
<body>
<div>
	<form action="" method="post">
	<input type="hidden" name="action" value="bind" />
	<table align="left" border="1" cellspacing="0">
		<tr>
			<td colspan="2" align="center"><b>子集绑定</b></td>
		</tr>
		<tr>
			<td bgcolor="#00F5FF">父集<font color="red">外部</font>CODE：</td>
			<td><input type="text" name="parentcode" value="" style="width:330px;" /></td>
		</tr>
		<tr>
			<td bgcolor="#00F5FF" rowspan="2">子集<font color="red">外部</font>CODE：</td>
			<td>多个子集CODE使用","分隔</td>
		</tr>
		<tr>
			<td><textarea name="childcode" cols="40" rows="10"></textarea></td>
		</tr>
		<tr>
			<td colspan="2" align="center"><input type="submit" value="提交" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="重置" /></td>
		</tr>
	</table>
	</form>
</div>
</body>
</html>