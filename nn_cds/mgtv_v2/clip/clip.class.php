<?php
include_once dirname(dirname(__FILE__)) . '/models/public_model_exec.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/clip_task/clip_servicers.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/v2/ns_core/m_mediainfo.class.php';
/**
 * 切片公共类
 * @author liangpan
 * @date 2015-12-25
 */
class nl_clip
{
	private $sp_id = null;
	private $cp_id = null;
	private $sp_config = null;
	private $arr_params = null;
	public $obj_dc = null;
	private $function = null;
	private $obj_dom = null;
	private $program_name = '';
	private $content_description = '';
	private $drm_ext_info = '';
	private $cp_config = null;
	private $error_info = null;
	private $mode_index = array(
			'std'=>'01',
			'hd'=>'02',
			'low'=>'03',
			'4k'=>'04',
	);
	public $redis_lock_timeout = 5;//redis锁默认5S
	
	/**
	 * @return the $error_info
	 */
	public function get_error_info()
	{
		return $this->error_info;
	}

	/**
	 * @param field_type $error_info
	 */
	private function set_error_info($error_info)
	{
		$this->error_info[] = $error_info;
		return $error_info;
	}

	public function __construct($sp_id = null, $arr_params = null,$function = null,$cp_id = null)
	{
		$this->sp_id = $sp_id;
		$this->cp_id = $cp_id;
		$this->arr_params = $arr_params;
		$this->obj_dc = nl_get_dc(array (
				'db_policy' => NL_DB_WRITE, 
				'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
		));
		$this->function = $function;
	}

	/**
	 * 返回数据数组
	 * @param number $ret
	 * @param string $str_desc 英文描述
	 * @param string $error_info 错误描述
	 * @return multitype:number string 
	 * @author liangpan
	 * @date 2015-12-25
	 */
	private function _return_data($ret=200,$str_desc='',$error_info=null)
	{
		return array(
				'ret'=>$ret,
				'reason'=>$str_desc,
				'error_info'=>$error_info,
		);
	}
	
	/**
	 * 获取CP配置（单个CP）
	 * @param string $cp_id
	 * @return boolean
	 * @author liangpan
	 * @date 2015-12-25
	 */
	private function _get_cp_config($cp_id=null)
	{
		if(strlen($cp_id) < 1)
		{
			$this->set_error_info($this->_return_data('404','CP ID is empty',"cp id 错误，传入参数为：".var_export($cp_id,true)));
			return false;
		}
		$result_info=nl_cp::query_by_id($this->obj_dc, $cp_id);
		if($result_info['ret'] !=0)
		{
			$this->set_error_info($this->_return_data('404','sql query cp error',"cp id 查询 sql 错误：".var_export($result_info['reason'],true)));
			return false;
		}
		if(isset($result_info['data_info']['nns_config']) && strlen($result_info['data_info']['nns_config']) >0)
		{
			$result_info_config = json_decode($result_info['data_info']['nns_config'],true);
			if(is_array($result_info_config) && !empty($result_info_config))
			{
				$this->cp_config = $result_info_config;
				$this->cp_config['base_cp_name'] = $result_info['data_info']['nns_name'];
			}
		}
		return true;
	}
	
	/**
	 * 获取SP配置（单个SP）
	 * @param string $sp_id
	 * @return boolean
	 * @author liangpan
	 * @date 2015-12-25
	 */
	private function _get_sp_config($sp_id=null)
	{
		if(strlen($sp_id) < 1)
		{
			$this->set_error_info($this->_return_data('404','SP ID is empty',"sp id 错误，传入参数为：".var_export($sp_id,true)));
			return false;
		}
		$result_info=nl_sp::query_by_id($this->obj_dc, $sp_id);
		if($result_info['ret'] !=0)
		{
			$this->set_error_info($this->_return_data('404','sql query sp error',"sp id 查询 sql 错误：".var_export($result_info['reason'],true)));
			return false;
		}
		if(isset($result_info['data_info'][0]['nns_config']) && strlen($result_info['data_info'][0]['nns_config']) >0)
		{
			$result_info_config = json_decode($result_info['data_info'][0]['nns_config'],true);
			if(is_array($result_info_config) && !empty($result_info_config))
			{
				$this->sp_config = $result_info_config;
				$temp_bind_cp_conf = explode(',', $result_info['data_info'][0]['nns_bind_cp']);
				if(is_array($temp_bind_cp_conf) && !empty($temp_bind_cp_conf))
				{
					foreach ($temp_bind_cp_conf as $val)
					{
						if(strlen($val) <1)
						{
							continue;
						}
						$this->sp_config['bind_cp_conf'][]=$val;
					}
				}
				unset($temp_bind_cp_conf);
			}
		}
		return true;
	}
	
	
	/**
	 * 公共反馈错误信息
	 * @param string $function
	 * @param string $is_need_header
	 * @author liangpan
	 * @date 2015-12-25
	 */
	private function _return_auto_error_info( $function = 'public', $is_need_header = true)
	{
		$http_code = '404';
		$resson = '';
		$data = null;
		if(isset($this->error_info) && is_array($this->error_info))
		{
			foreach ($this->error_info as $val)
			{
				$resson.="{$val['reason']};";
			}
		}
		$resson = trim($resson,';');
		$this->_bulid_result($http_code, $resson, $data, $function , $is_need_header);
	}
	
	
	/**
	 * 获取单条切片任务数据
	 * @author liangpan
	 * @date 2015-12-25
	 */
	public function get_clip_task()
	{
		nn_log::write_log_message('clip_log', '------' . $this->function . '------', $this->sp_id, $this->function);
		$flag=$this->_get_sp_config($this->sp_id);
		if(!$flag)
		{
			$this->_return_auto_error_info($this->function);
		}
		if(isset($this->sp_config['clip_import_model']) && $this->sp_config['clip_import_model'] == 1)
		{
			nn_log::write_log_message('clip_log', '新模式切片开始', $this->sp_id, $this->function);
			$this->get_clip_task_v2();
		}
		//切片获取任务REDIS模式
		if(isset($this->sp_config['clip_import_model']) && $this->sp_config['clip_import_model'] == 2)
		{
			nn_log::write_log_message('clip_log', 'REDIS获取切片任务开始', $this->sp_id, $this->function);
			$this->get_clip_task_v3();
		}
		nn_log::write_log_message('clip_log', '老模式切片开始', $this->sp_id, $this->function);
		$nns_new_dir = isset($this->arr_params['nns_new_dir']) ? (int)$this->arr_params['nns_new_dir'] : 0;
		$tom3u8_id = isset($this->arr_params['tom3u8_id']) ? $this->arr_params['tom3u8_id'] : '';
		$task_info = clip_task_model::get_task($this->sp_id);
		if (!is_array($task_info))
		{
			$this->_bulid_result('404', 'no task', null, $this->function);
			exit();
		}
		$dt = date('Y-m-d H:i:s');
		//更新任务状态:任务已被取走
		$set_state = array (
				'nns_state' => clip_task_model::TASK_C_STATE_HANDLE, 
				'nns_desc' => clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_HANDLE], 
				'nns_start_time' => $dt, 
				'nns_modify_time' => $dt,
                'nns_tom3u8_id'   =>$tom3u8_id
		);
		//查询此服务器id是否存在 不存在自动添加
        $result = nl_clip_servicers::query_by_id($this->obj_dc, $tom3u8_id);
        if (!isset($result['data_info'])&&empty($result['data_info']))
        {
            $params['nns_id']=np_guid_rand();
            $params['nns_tom3u8_id']=$tom3u8_id;
            nl_clip_servicers::add($this->obj_dc, $params);
        }
		//if($nns_new_dir){
		$set_state['nns_new_dir'] = $nns_new_dir;
		//}else{
		//	$set_state['nns_new_dir']=0;
		//}
		
		nn_log::write_log_message('clip_log', '更新内容：' . var_export($set_state, true), $this->sp_id, $this->function);
		
		clip_task_model::update_task($task_info['nns_id'], $set_state);
		//任务日志
		$task_log = array (
				'nns_task_id' => $task_info['nns_id'], 
				'nns_state' => clip_task_model::TASK_C_STATE_HANDLE, 
				'nns_desc' => clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_HANDLE]
		);
		nn_log::write_log_message('clip_log', '任务日志内容：' . var_export($task_log, true), $this->sp_id, $this->function);
		$rs = clip_task_model::add_task_log($task_log);
		$this->_bulid_result('200', 'OK', $task_info['nns_content'], $this->function);
	}

	/**
	 * 获取单条切片任务数据（片源获取切片信息）
	 * @author liangpan
	 * @date 2015-12-25
	 */
	private function get_clip_task_v2()
	{
		include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/vod_media/vod_media.class.php';
		include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/clip_task/clip_task.class.php';
		if(!is_array($this->sp_config['bind_cp_conf']) || empty($this->sp_config['bind_cp_conf']))
		{
			nn_log::write_log_message('clip_log', '获取单条切片任务数据（片源获取切片信息）cp为空：' . var_export($this->sp_config['bind_cp_conf'], true), $this->sp_id, $this->function);
			$this->_bulid_result('404', 'bind_cp_conf no query', null, $this->function);
			exit();
		}
		$this->cp_id = $this->sp_config['bind_cp_conf'][array_rand($this->sp_config['bind_cp_conf'])];
		$flag=$this->_get_cp_config($this->cp_id);
		if(!$flag)
		{
			$this->_return_auto_error_info($this->function);
		}
		if(isset($this->sp_config['message_import_time_control']) && !empty($this->sp_config['message_import_time_control']))
		{
			$check_allow_download = $this->check_allow_download($this->sp_config['message_import_time_control']);
			if(!$check_allow_download)
			{
				$this->_bulid_result('404', 'no task allow this time download', null, $this->function);
				exit();
			}
		}
		$result = nl_vod_media_v2::query_wait_clip($this->obj_dc,$this->cp_id);
		if($result['ret'] !=0)
		{
			nn_log::write_log_message('clip_log', '查询切片数据内部错误：' . var_export($result, true), $this->sp_id, $this->function);
			$this->_bulid_result('404', 'bk inner error', null, $this->function);
			exit();
		}
		if(!is_array($result['data_info']) || empty($result['data_info']))
		{
			$this->_bulid_result('404', 'no task', null, $this->function);
			exit();
		}
		$result=$result['data_info'];
		$result_check = nl_clip_task::check_exsist($this->obj_dc,$result['nns_id']);
		if($result_check['ret'] !=0)
		{
			nn_log::write_log_message('clip_log', '切片查询是否存在片源内部错误：' . var_export($result_check, true), $this->sp_id, $this->function);
			$this->_bulid_result('404', 'bk inner error', null, $this->function);
			exit();
		}
		$task_id = empty($result_check['data_info']) ? np_guid_rand() : $result_check['data_info'];
		$flag_media_type = ($result['nns_filetype'] == 'hls') ? 'hls' : 'ts';
		$this->obj_dom = new DOMDocument("1.0", 'utf-8');
		//task xml attr 数组
		$task_array = array(
				'id'=>$task_id,
				'force' => 0,
				'model' => 1,
		);
		$obj_task=$this->make_xml('task',null,$task_array);
		//video xml attr 数组
		$video_array = array(
				'id'=>$result['nns_vod_id'],
				'name' => htmlspecialchars($result['vod_name'], ENT_QUOTES),
				'video_type' => 0,
				'pinyin'=> $result['vod_pinyin'],
		);
		$obj_video=$this->make_xml('video',null,$video_array,$obj_task);
		$obj_index_list=$this->make_xml('index_list',null,null,$obj_video);
		//index xml attr 数组
		$index_array = array(
				'id'=>$result['nns_vod_index_id'],
				'index' => $result['index_index'],
				'name' => htmlspecialchars($result['index_name'], ENT_QUOTES),
				'media_type' => $flag_media_type,
		);
		$obj_index=$this->make_xml('index',null,$index_array,$obj_index_list);
		$obj_media_list=$this->make_xml('media_list',null,null,$obj_index);
		$media_array = array(
				'id'=>$result['nns_id'],
				'name' => $result['nns_name'],
				'file_id' => $result['nns_import_id'],
				'content_id'=> $result['index_import_id'],
				'content_url'=>$result['nns_url'],
				'kbps_index'=>'0'.$this->_get_kbps_index($result['nns_mode']),
				'kbps'=>$result['nns_kbps'],
				'md5'=>clip_task_model::get_media_hash($result['nns_import_id'],$this->cp_id),
				'drm_enabled'=>0,
				'cp_id'=>$this->cp_id,
				'media_type'=>$flag_media_type,
		);
		$this->make_xml('media',null,$media_array,$obj_media_list);
		$str_xml_content = $this->obj_dom->saveXML();
		$add_array = array (
				'nns_id' => $task_id,
				'nns_state' => clip_task_model::TASK_C_STATE_HANDLE,
				'nns_desc' => clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_HANDLE],
				'nns_video_id' => $result['nns_vod_id'],
				'nns_video_index_id' => $result['nns_vod_index_id'],
				'nns_video_media_id' => $result['nns_id'],
				'nns_org_id' => $this->sp_id,
				'nns_task_type' => 'std',
				'nns_video_type' => 0,
				'nns_priority' => 0,
				'nns_force' => 0,
				'nns_new_dir' => 0,
				'nns_task_name' => $result['nns_name'],
				'nns_content' =>htmlspecialchars($str_xml_content, ENT_QUOTES),
		);
		if(empty($result_check['data_info']))
		{
			$result_add = nl_clip_task::add($this->obj_dc, $add_array);
		}
		else
		{
			unset($add_array['nns_id']);
			$result_add = nl_clip_task::edit($this->obj_dc, $add_array,$task_id);
		}
		if($result_add['ret'] !=0)
		{
			nn_log::write_log_message('clip_log', '切片数据添加内部错误：' . var_export($result_add, true), $this->sp_id, $this->function);
			$this->_bulid_result('404', 'bk inner error', null, $this->function);
			exit();
		}
		$result_edit_meida = nl_vod_media_v2::edit($this->obj_dc,array('nns_clip_state'=>2),$result['nns_id']);
		if($result_edit_meida['ret'] !=0)
		{
			nn_log::write_log_message('clip_log', '修改片源切片状态数据内部错误：' . var_export($result_edit_meida, true), $this->sp_id, $this->function);
			$this->_bulid_result('404', 'bk inner error', null, $this->function);
			exit();
		}
		//任务日志
		$task_log = array (
				'nns_task_id' => $task_id,
				'nns_state' => clip_task_model::TASK_C_STATE_HANDLE,
				'nns_desc' => clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_HANDLE]
		);
		nn_log::write_log_message('clip_log', '任务日志内容：' . var_export($task_log, true), $this->sp_id, $this->function);
		clip_task_model::add_task_log($task_log);
		$this->_bulid_result('200', 'OK', $str_xml_content, $this->function);
	}
	/**
	 * 从redis中获取切片任务
	 * @return boolean|unknown
	 */
	private function get_clip_task_v3()
	{
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/np/np_redis_lock.class.php';
		$redis_obj = nl_get_redis();
		$redis_lock = new np_redis_lock_class($redis_obj);
		//获取锁
		$identifier = $redis_lock->lock($this->sp_id,$this->redis_lock_timeout);
		if($identifier === false)
		{
			nn_log::write_log_message('clip_log', '队列已经被锁', $this->sp_id, $this->function);
			$this->_bulid_result('404', 'no task', null, $this->function);
		}
		$result = $redis_obj->lpop($this->sp_id);
		nn_log::write_log_message('clip_log', '任务内容为：' . var_export($result, true), $this->sp_id, $this->function);
		if($result === false)
		{
			nn_log::write_log_message('clip_log', '没有任务', $this->sp_id, $this->function);
			$this->_bulid_result('404', 'no task', null, $this->function);
		}
		//解锁
		$redis_lock->unlock($this->sp_id,$identifier);
		
		$this->_bulid_result('200', 'OK', $result, $this->function);
	}
	/**
	 * 判断时间范围是否允许
	 * @param string $str_time
	 * @return boolean true 允许 | false 不允许
	 * @authorliangpan
	 * @date 2015-12-25
	 */
	private function check_allow_download($str_time)
	{
		$arr_time = explode("|", $str_time);
		if(empty($arr_time) || !is_array($arr_time))
		{
			return true;
		}
		$now_time = intval(str_replace(array(':'), '',date('G:i')));
		foreach ($arr_time as $val)
		{
			$arr_val = explode('-', trim($val));
			if(empty($arr_val) || !is_array($arr_val) || count($arr_val) !=2 || !isset($arr_val[0]) || !isset($arr_val[1]))
			{
				continue;
			}
			$str_begin = intval(str_replace(array(':',"：",' ',' '), '', preg_replace("/^0*/", '',$arr_val[0])));
			$str_end = intval(str_replace(array(':',"：",' ',' '), '', preg_replace("/^0*/", '',$arr_val[1])));
			if($str_begin > $str_end)
			{
				$str_begin=$str_begin^$str_end;
				$str_end=$str_end^$str_begin;
				$str_begin=$str_begin^$str_end;
			}
			if($now_time >= $str_begin && $now_time <= $str_end)
			{
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * 获取清晰度对应
	 * @param string $mode
	 * @return Ambigous <string, multitype:string >
	 * @authorliangpan
	 * @date 2015-12-25
	 */
	private function _get_kbps_index($mode)
	{
		$mode= strtolower($mode);
		$mode_index = '01';
		if(strlen($mode) > 0 && isset($this->mode_index[$mode]))
		{
			$mode_index = $this->mode_index[$mode];
		}
		return $mode_index;
	}
	
	/**
	 * 获取片源下载地址
	 * @authorliangpan
	 * @date 2015-12-25
	 */
	public function get_media_url()
	{
		nn_log::write_log_message('clip_log', '------' . $this->function . '------', $this->sp_id, $this->function);
		$task_id = isset($this->arr_params['nns_task_id']) ? $this->arr_params['nns_task_id'] : null;
		$video_id = isset($this->arr_params['nns_video_id']) ? $this->arr_params['nns_video_id'] : null;
		$video_type = isset($this->arr_params['nns_video_type']) ? $this->arr_params['nns_video_type'] : 0;
		$nns_index_id = isset($this->arr_params['nns_index_id']) ? $this->arr_params['nns_index_id'] : null;
		$nns_media_id = isset($this->arr_params['nns_media_id']) ? $this->arr_params['nns_media_id'] : null;
		$nns_new_dir = isset($this->arr_params['nns_new_dir']) ? $this->arr_params['nns_new_dir'] : 0;
        $tom3u8_id = isset($this->arr_params['tom3u8_id']) ? $this->arr_params['tom3u8_id'] : '';
		//判断任务状态
		$task_info = clip_task_model::get_task_by_id($task_id);
		if ($task_info == false)
		{
			$nns_link_file=nn_log::write_log_message('clip_log', '未找到该任务(task id:' . $task_id . ')', $this->sp_id, $this->function);
			nl_global_error_log::add($this->obj_dc, array('nns_desc'=>'未找到该任务(task id:' . $task_id . ')','nns_model'=>'clip_log','nns_sp_id'=>$this->sp_id,'nns_link_file'=>$nns_link_file,'nns_bind_id'=>$task_id));
			$this->_bulid_result('404', 'get task info fail', null, $this->function);
		}
		if ($task_info['nns_state'] == clip_task_model::TASK_C_STATE_CANCEL)
		{
			nn_log::write_log_message('clip_log', '任务已取消(task id:' . $task_id . ')', $this->sp_id, $this->function);
			$data = '<media id="' . $nns_media_id . '" url="" state="1" reason="task cancel"/>';
			$this->_bulid_result('200', 'OK', $data, $this->function);
		}
		$flag=$this->_get_sp_config($this->sp_id);
		if(!$flag)
		{
		    $nns_link_file=nn_log::write_log_message('clip_log', '获取SP配置失败任务(task id:' . $task_id . '，spid:'.$this->sp_id.')'.var_export($this->error_info,true), $this->sp_id, $this->function);
			nl_global_error_log::add($this->obj_dc, array('nns_desc'=>'获取SP配置失败任务(task id:' . $task_id . '，spid:'.$this->sp_id.')','nns_model'=>'clip_log','nns_sp_id'=>$this->sp_id,'nns_link_file'=>$nns_link_file,'nns_bind_id'=>$task_id));
			$this->_return_auto_error_info($this->function);
		}
		$this->cp_id = $task_info['nns_cp_id'];
        $this->_get_cp_config($this->cp_id);
// 		if(isset($this->sp_config['import_id_mode']) && $this->sp_config['import_id_mode'] == '5')
// 		{
// 		    $nns_media_id_temp = ltrim(substr($nns_media_id, 10),'0');
// 		    include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/vod_media/vod_media.class.php';
// 		    $nns_media_id_temp=nl_vod_media_v2::query_by_integer_id($this->obj_dc, $nns_media_id_temp);
// 		    $nns_media_id = isset($nns_media_id_temp['data_info']['nns_id']) ? $nns_media_id_temp['data_info']['nns_id'] : $nns_media_id;
// 		}
// 		$video_type = $video_type !=0 ? 'live_media' : 'media';
		switch ($video_type)
		{
		    case '0':
		        $video_type = 'media';
		        break;
		    case '1':
		        $video_type = 'live_media';
		        break;
		    case '2':
		        $video_type = 'file';
		        break;
		    default:
		        $video_type = 'media';
		}
		$result = public_model_exec::get_asset_id_by_sp_import_id($this->sp_id, $video_type, $nns_media_id,$this->cp_id,$this->sp_config);
		if($result['ret'] !=0)
		{
		    $nns_link_file=nn_log::write_log_message('clip_log', '获取GUID转换(task id:' . $task_id . '，spid:'.$this->sp_id.')'.var_export($result,true), $this->sp_id, $this->function);
		    nl_global_error_log::add($this->obj_dc, array('nns_desc'=>'获取GUID转换(task id:' . $task_id . '，spid:'.$this->sp_id.')失败','nns_model'=>'clip_log','nns_sp_id'=>$this->sp_id,'nns_link_file'=>$nns_link_file,'nns_bind_id'=>$task_id));
		    $this->_return_auto_error_info($this->function);
		}
		if(!isset($result['data_info']['data_info']) || empty($result['data_info']['data_info']) || !is_array($result['data_info']['data_info']))
		{
		    $nns_link_file=nn_log::write_log_message('clip_log', '取片源信息失败:'.var_export($result,true), $this->sp_id, $this->function);
		    nl_global_error_log::add($this->obj_dc, array('nns_desc'=>'取片源信息失败','nns_model'=>'clip_log','nns_sp_id'=>$this->sp_id,'nns_link_file'=>$nns_link_file,'nns_bind_id'=>$task_id));
		    $this->_bulid_result('404', 'get media info fail', null, $this->function);
		}
		$media_info = $result['data_info']['data_info'];
		$str_get_url='';
		include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/np/np_http_curl.class.php';
		$http_client = new np_http_curl_class();
		if(isset($media_info['nns_cp_id']) && strlen($media_info['nns_cp_id']) > 0 )
		{
			$str_get_url="&cp_id={$media_info['nns_cp_id']}";
		}
		if(!isset($media_info['nns_import_id']) || strlen($media_info['nns_import_id']) <1 )
		{
		    $str_get_url.="&media_id={$media_info['nns_id']}";
		}
		$url = MGTV_MEDIA_DOWNLOAD_URL . 'file_id=' . $media_info['nns_import_id'].$str_get_url;
		nn_log::write_log_message('clip_log', '从接口取影片下载地址：' . $url, $this->sp_id, $this->function);
		$result = $http_client->get($url);
		$ret_arr = json_decode($result, true);
		nn_log::write_log_message('clip_log', 'response:' . $result . ',json_decode数据：' . var_export($ret_arr, true), $this->sp_id, $this->function);
		$dt = date('Y-m-d H:i:s');
// 		$nns_media_type = (empty($media_info['nns_filetype']) || $media_info['nns_filetype'] == 'ts') ? 'ts' : 'hls';
		if ($ret_arr['err'] == '0')
		{
			$down_url = $ret_arr['msg']['download_url'];
			$status = $ret_arr['status'];
			$reason = $ret_arr['reason'];
// 			$data = '<media id="' . $nns_media_id . '" url="' . $down_url . '" state="0" reason="' . $reason . '" media_type="' . $nns_media_type . '"/>';
			$data = '<media id="' . $nns_media_id . '" url="' . $down_url . '" state="0" reason="' . $reason . '"/>';
			$set_state = array (
					'nns_desc' => clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_GET_MEDIA_URL_SUCC] . '[' . $url . '][media_url:' . $down_url . ']', 
					'nns_state' => clip_task_model::TASK_C_STATE_GET_MEDIA_URL_SUCC, 
					'nns_modify_time' => $dt, 
					'nns_alive_time' => $dt
			);
			//if($nns_new_dir){
			$set_state['nns_new_dir'] = $nns_new_dir;
            $set_state['nns_tom3u8_id'] = $tom3u8_id;
			//}else{
			//	$set_state['nns_new_dir']=0;
			//}
			nn_log::write_log_message('clip_log', '更新内容：' . var_export($set_state, true), $this->sp_id, $this->function);
			clip_task_model::update_task($task_id, $set_state);
			//任务日志
			$task_log = array (
					'nns_task_id' => $task_id, 
					'nns_state' => clip_task_model::TASK_C_STATE_GET_MEDIA_URL_SUCC, 
					'nns_desc' => clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_GET_MEDIA_URL_SUCC] . '[' . $url . '][media_url:' . $down_url . ']', 
					'nns_create_time' => $dt
			);
			clip_task_model::add_task_log($task_log);
			nn_log::write_log_message('clip_log', '任务日志：' . var_export($task_log, true), $this->sp_id, $this->function);
			$this->_bulid_result('200', 'OK', $data, $this->function);
		}
		else
		{
			$set_state = array (
					'nns_desc' => clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_GET_MEDIA_URL_FAIL] . '[' . $url . ']', 
					'nns_state' => clip_task_model::TASK_C_STATE_GET_MEDIA_URL_FAIL, 
					'nns_modify_time' => $dt, 
					'nns_alive_time' => $dt
			);
			//if($nns_new_dir){
			$set_state['nns_new_dir'] = $nns_new_dir;
			//}else{
			//$set_state['nns_new_dir']=0;
			//}
			$nns_link_file=nn_log::write_log_message('clip_log', '更新内容：' . var_export($set_state, true), $this->sp_id, $this->function);
			nl_global_error_log::add($this->obj_dc, array('nns_desc'=>'更新内容失败','nns_model'=>'clip_log','nns_sp_id'=>$this->sp_id,'nns_link_file'=>$nns_link_file,'nns_bind_id'=>$task_id));
			clip_task_model::update_task($task_id, $set_state);
            //查询此服务器id是否存在 不存在自动添加
            $result = nl_clip_servicers::query_by_id($this->obj_dc, $tom3u8_id);
            if (!isset($result['data_info'])&&empty($result['data_info']))
            {
                $params['nns_id']=np_guid_rand();
                $params['nns_tom3u8_id']=$tom3u8_id;
                nl_clip_servicers::add($this->obj_dc, $params);
            }
			//任务日志
			$task_log = array (
					'nns_task_id' => $task_id, 
					'nns_state' => clip_task_model::TASK_C_STATE_GET_MEDIA_URL_FAIL, 
					'nns_desc' => clip_task_model::$task_state_arr[clip_task_model::TASK_C_STATE_GET_MEDIA_URL_FAIL] . '[' . $url . ']', 
					'nns_create_time' => $dt, 
					'nns_modify_time' => $dt
			);
			clip_task_model::add_task_log($task_log);
			nn_log::write_log_message('clip_log', '任务日志：' . var_export($task_log, true), $this->sp_id, $this->function);
			$this->_bulid_result('404', 'get meida url fail', null, $this->function);
		}
	}

	/**
	 * cdn切片状态上报
	 * @return boolean
	 * @authorliangpan
	 * @date 2015-12-25
	 */
	public function report_clip_status()
	{
        include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/vod_media/vod_media.class.php';
		nn_log::write_log_message('clip_log', '------' . $this->function . '------', $this->sp_id, $this->function);
		nn_log::write_log_message('clip_log', '-----请求参数为：' . var_export($this->arr_params, true). '------', $this->sp_id, $this->function);

		$task_id = isset($this->arr_params['nns_task_id']) ? $this->arr_params['nns_task_id'] : null;
		$state = isset($this->arr_params['nns_state']) ? $this->arr_params['nns_state'] : null;
		//$desc = isset($this->arr_params['nns_desc']) ? urldecode($this->arr_params['nns_desc']) : null;
		$desc = isset($this->arr_params['nns_desc']) ? htmlspecialchars($this->arr_params['nns_desc'], ENT_QUOTES) : null;
		$nns_new_dir = isset($this->arr_params['nns_new_dir']) ? (int)$this->arr_params['nns_new_dir'] : 0;
		$nns_path = isset($this->arr_params['nns_path']) ? $this->arr_params['nns_path'] : null;
		$nns_file_size = isset($this->arr_params['nns_file_size']) ? $this->arr_params['nns_file_size'] : null;
		$nns_file_md5 = isset($this->arr_params['nns_file_md5']) ? $this->arr_params['nns_file_md5'] : null;
		$nns_cdn_policy = isset($this->arr_params['nns_cdn_policy']) ? $this->arr_params['nns_cdn_policy']:null;
		$nns_file_hit = isset($this->arr_params['nns_file_hit']) ? $this->arr_params['nns_file_hit']:null;
		$nns_node_id = isset($this->arr_params['nns_node_id']) ? $this->arr_params['nns_node_id']:null;
		$nns_list_ids = isset($this->arr_params['nns_list_ids']) ? $this->arr_params['nns_list_ids']:null;
        $tom3u8_id = isset($this->arr_params['tom3u8_id']) ? $this->arr_params['tom3u8_id'] : '';
		$dt = date('Y-m-d H:i:s');
		$set_state = array (
				'nns_desc' => $desc,
				'nns_state' => $state,
				'nns_modify_time' => $dt,
				'nns_alive_time' => $dt
		);
		$task_info = clip_task_model::get_task_by_id($task_id);
		if ($task_info == false)
		{
			$nns_link_file=nn_log::write_log_message('clip_log', '未找到该任务(task id:' . $task_id . ')', $this->sp_id, $this->function);
			nl_global_error_log::add($this->obj_dc, array('nns_desc'=>'切片上报失败：未找到该任务(task id:' . $task_id . ')','nns_model'=>'clip_log','nns_sp_id'=>$this->sp_id,'nns_link_file'=>$nns_link_file,'nns_bind_id'=>$task_id));
			$this->_bulid_result('404', 'get task info fail', null, $this->function);
		}
		//获取媒资信息
		$import_info = clip_task_model::get_import_id_by_task_id($this->obj_dc, array($task_id), $this->sp_id);
        $this->cp_id = $import_info[0]['media_cp_id'];
        
        $this->_get_cp_config($import_info[0]['media_cp_id']);
        
        
		if ($task_info['nns_state'] == clip_task_model::TASK_C_STATE_CANCEL)
		{
			$nns_link_file=nn_log::write_log_message('clip_log', '任务已取消(task id:' . $task_id . ')', $this->sp_id, $this->function);
			$log_params = array(
				'nns_desc'=>'切片上报失败：未找到该任务(task id:' . $task_id . ')',
				'nns_model'=>'clip_log',
				'nns_sp_id'=>$this->sp_id,
				'nns_link_file'=>$nns_link_file,
				'nns_bind_id'=>$task_id,
				'nns_video_import_id' => $import_info[0]['video_import_id'],
				'nns_index_import_id' => $import_info[0]['index_import_id'],
				'nns_media_import_id' => $import_info[0]['media_import_id'],
				'nns_name' => $import_info[0]['nns_name'],
				'nns_cp_id' => $import_info[0]['nns_cp_id'],
				'nns_type' => 'media'
			);
			nl_global_error_log::add($this->obj_dc, $log_params);
			$data = '<result state="1" reason="task cancel" />';
			//任务日志
			$task_log = array (
					'nns_task_id' => $task_id,
					'nns_state' => $state,
					'nns_desc' => $desc,
					'nns_create_time' => $dt
			);
			clip_task_model::add_task_log($task_log);
			$this->_bulid_result('200', 'OK', $data, $this->function);
		}
		//如果切片完成，生成注入xml文件，执行影片注入命令
		$state = strtolower(trim($state));
		//对失败的记录全局日志
		if($state == clip_task_model::TASK_C_STATE_CLIP_DRM_FAIL || $state == clip_task_model::TASK_C_STATE_GET_MEDIA_URL_FAIL || $state == clip_task_model::TASK_C_STATE_DOWNLOAD_FAIL || $state == clip_task_model::TASK_C_STATE_CLIP_FAIL || $state == clip_task_model::TASK_C_STATE_C2_DELETE_FAIL|| $state == clip_task_model::TASK_C_STATE_CLIP_UPLOAD_FAIL)
		{
			switch ($state)
			{
				case clip_task_model::TASK_C_STATE_GET_MEDIA_URL_FAIL:
					$errors = "获取下载地址失败";
					break;
				case clip_task_model::TASK_C_STATE_CLIP_DRM_FAIL:
					$errors = "DRM加密失败";
					break;
				case clip_task_model::TASK_C_STATE_DOWNLOAD_FAIL:
					$errors = "片源下载失败";
					break;
				case clip_task_model::TASK_C_STATE_CLIP_FAIL:
					$errors = "切片失败";
					break;
				case clip_task_model::TASK_C_STATE_C2_DELETE_FAIL:
					$errors = "删除切片服务器片源失败";
					break;
                case clip_task_model::TASK_C_STATE_CLIP_UPLOAD_FAIL:
                    $errors = "上传失败";
                    break;
			}
			$nns_link_file = nn_log::write_log_message('clip_log', $errors . '(task id:' . $task_id . ')', $this->sp_id, $this->function);			
			$log_params = array(
				'nns_desc'=>'切片上报失败：' . $errors . '(task id:' . $task_id . ')',
				'nns_model'=>'clip_log',
				'nns_sp_id'=>$this->sp_id,
				'nns_link_file'=>$nns_link_file,
				'nns_bind_id'=>$task_id,
				'nns_video_import_id' => $import_info[0]['video_import_id'],
				'nns_index_import_id' => $import_info[0]['index_import_id'],
				'nns_media_import_id' => $import_info[0]['media_import_id'],
				'nns_name' => $import_info[0]['nns_name'],
				'nns_cp_id' => $import_info[0]['nns_cp_id'],
				'nns_type' => 'media'
			);
			nl_global_error_log::add($this->obj_dc, $log_params);
		}
		//drm 加密失败  且为切片后配置
		if (($state == clip_task_model::TASK_C_STATE_CLIP_DRM_FAIL || $state == clip_task_model::TASK_C_STATE_GET_MEDIA_URL_FAIL || $state == clip_task_model::TASK_C_STATE_DOWNLOAD_FAIL 
				|| $state == clip_task_model::TASK_C_STATE_CLIP_FAIL || $state == clip_task_model::TASK_C_STATE_FAIL || $state == clip_task_model::TASK_C_STATE_C2_FAIL || $state == clip_task_model::TASK_C_STATE_C2_DELETE_FAIL||$state == clip_task_model::TASK_C_STATE_CLIP_UPLOAD_FAIL)
				&& 1 === $this->check_drm_action())
		{
			//加载切片后消息反馈文件
			if (!file_exists(dirname(dirname(dirname(__FILE__))) . '/api/' . $this->sp_id . '/' . $this->sp_id . '_sync_source.php'))
			{
				nn_log::write_log_message('clip_log', '未查询到反馈操作类文件，文件路径为:' . dirname(dirname(dirname(__FILE__))) . '/api/' . $this->sp_id . '/' . $this->sp_id . '_sync_source.php', $this->sp_id, __FUNCTION__);
				nl_global_error_log::add($this->obj_dc, array('nns_desc'=>'切片上报：未查询到反馈操作类文件，文件路径为:' . dirname(dirname(dirname(__FILE__))) . '/api/' . $this->sp_id . '/' . $this->sp_id . '_sync_source.php','nns_model'=>'clip_log','nns_sp_id'=>$this->sp_id,'nns_bind_id'=>$task_id));
				$data = '<result state="1" reason="async feedback file not find" />';
				$this->_bulid_result('1', 'OK', $data, $this->function);
			}
			//加载反馈上游消息 drm类文件
			include_once dirname(dirname(dirname(__FILE__))) . '/api/' . $this->sp_id . '/' . $this->sp_id . '_sync_source.php';
			$fuc = $this->sp_id . '_sync_source';
			$sync_source = new $fuc();
			$arr_function = get_class_methods($sync_source);
			$arr_function = is_array($arr_function) ? $arr_function : array ();
			//没有方法时
			if (!in_array('async_feedback', $arr_function))
			{
				nn_log::write_log_message('clip_log', '未查询到类方法' . $fuc, $this->sp_id, $this->function);
				nl_global_error_log::add($this->obj_dc, array('nns_desc'=>'切片上报：未查询到类方法' . $fuc ,'nns_model'=>'clip_log','nns_sp_id'=>$this->sp_id,'nns_bind_id'=>$task_id));
				$data = '<result state="1" reason="get op queue info fail" />';
				$this->_bulid_result('1', 'OK', $data, $this->function);
			}
			$op_queue_info = nl_op_queue::query_op_queue_info_by_id($this->obj_dc, $task_info['nns_op_id']);
			if ($op_queue_info['ret'] != 0)
			{
				nn_log::write_log_message('clip_log', $op_queue_info['reason'], $this->sp_id, $this->function);
				nl_global_error_log::add($this->obj_dc, array('nns_desc'=>'切片上报：'.$op_queue_info['reason'] ,'nns_model'=>'clip_log','nns_sp_id'=>$this->sp_id,'nns_bind_id'=>$task_id));
				$data = '<result state="1" reason="bk inside fail" />';
				$this->_bulid_result('1', 'OK', $data, $this->function);
			}
			if (!isset($op_queue_info['data_info'][0]) || !is_array($op_queue_info['data_info'][0]))
			{
				nn_log::write_log_message('clip_log', 'op id 为' . $task_info['nns_op_id'] . '已删除', $this->sp_id, $this->function);
				nl_global_error_log::add($this->obj_dc, array('nns_desc'=>'切片上报：op id 为' . $task_info['nns_op_id'] . '已删除' ,'nns_model'=>'clip_log','nns_sp_id'=>$this->sp_id,'nns_bind_id'=>$task_id));
				$data = '<result state="1" reason="bk inside fail" />';
				$this->_bulid_result('1', 'OK', $data, $this->function);
			}
			$return_flag = $state == clip_task_model::TASK_C_STATE_CLIP_DRM_FAIL ? false : true;
			$sync_source->async_feedback($op_queue_info['data_info'][0], $return_flag);
		}
		if(!$this->_get_sp_config($this->sp_id))
		{
			nn_log::write_log_message('clip_log', '片源状态上报获取sp配置数据内部错误：' . var_export($this->get_error_info(), true), $this->sp_id, $this->function);
			$this->_bulid_result('404', 'bk inner error', null, $this->function);
			exit();
		}
		$clip_file_encode_enable_flag = true;
		if ($state == clip_task_model::TASK_C_STATE_OK)
		{
			nn_log::write_log_message('clip_log', '切片完成，生成注入xml文件，执行影片注入命令', $this->sp_id, $this->function);
			$nns_date = str_replace('.', '', $_GET['nns_date']);
			$nns_date = str_replace('/', '', $nns_date);
			$set_state['nns_date'] = $nns_date;
			$set_state['nns_end_time']=$dt;
			$set_state['nns_file_hit']=$nns_file_hit;
			$set_state['nns_priority']=0;
			//在此进行切片任务的上报和如库
			if(isset($this->sp_config['clip_file_encode_enable']) && $this->sp_config['clip_file_encode_enable'] == 1 && in_array($task_info['nns_video_type'], array('0','1')))
			{
			    $video_info = video_model::get_back_info_one(($task_info['nns_video_type'] == '0') ? 'media' : 'live_media',array('nns_id'=>$task_info['nns_video_media_id']));
			    if (is_array($video_info) && !empty($video_info))
			    {
			        if($this->_get_cp_config($video_info['nns_cp_id']) && isset($this->cp_config['cp_transcode_file_enabled']) && $this->cp_config['cp_transcode_file_enabled'] == '1')
			        {
			            $set_state['nns_state'] = clip_task_model::TASK_C_STATE_CLIP_ENCODE_WAIT;
			            $clip_file_encode_enable_flag = false;
			        }
			    }
			}
		}
		if($state == clip_task_model::TASK_C_STATE_DOWNLOAD_FAIL || $state == clip_task_model::TASK_C_STATE_DOWNLOAD_SUCC)
		{
			$nns_clip_state = ($state == clip_task_model::TASK_C_STATE_DOWNLOAD_SUCC) ? 3 : 4;
			$result_edit_meida = nl_vod_media_v2::edit($this->obj_dc,array('nns_clip_state'=>$nns_clip_state),$task_info['nns_video_media_id']);
			if($result_edit_meida['ret'] !=0)
			{
				nn_log::write_log_message('clip_log', '修改片源切片状态数据内部错误：' . var_export($result_edit_meida, true), $this->sp_id, $this->function);
				$this->_bulid_result('404', 'bk inner error', null, $this->function);
				exit();
			}
		}
		$set_state['nns_tom3u8_id'] = $tom3u8_id;
        $set_state['nns_new_dir'] = $nns_new_dir;
        if($state == clip_task_model::TASK_C_STATE_DOWNLOAD_FAIL || $state == clip_task_model::TASK_C_STATE_GET_MEDIA_URL_FAIL || $state == clip_task_model::TASK_C_STATE_CLIP_FAIL 
         || $state == clip_task_model::TASK_C_STATE_C2_FAIL || $state == clip_task_model::TASK_C_STATE_C2_DELETE_FAIL || $state == clip_task_model::TASK_C_STATE_GET_DRM_KEY_FAIL 
             || $state == clip_task_model::TASK_C_STATE_CLIP_DRM_FAIL || $state == clip_task_model::TASK_C_STATE_CLIP_ENCODE_FAIL|| $state == clip_task_model::TASK_C_STATE_CLIP_UPLOAD_FAIL )
        {
            $this->message_feedback($task_info['nns_op_id'],$import_info,1,clip_task_model::$task_state_arr[$state]);
        }

        if(($state == clip_task_model::TASK_C_STATE_DOWNLOAD_FAIL || $state == clip_task_model::TASK_C_STATE_GET_MEDIA_URL_FAIL || $state == clip_task_model::TASK_C_STATE_CLIP_FAIL 
         || $state == clip_task_model::TASK_C_STATE_C2_FAIL || $state == clip_task_model::TASK_C_STATE_C2_DELETE_FAIL || $state == clip_task_model::TASK_C_STATE_GET_DRM_KEY_FAIL 
             || $state == clip_task_model::TASK_C_STATE_CLIP_DRM_FAIL || $state == clip_task_model::TASK_C_STATE_CLIP_ENCODE_FAIL || $state == clip_task_model::TASK_C_STATE_OK ||$state == clip_task_model::TASK_C_STATE_CLIP_UPLOAD_FAIL ) &&
            isset($this->cp_config['cp_playbill_to_media_enabled']) && $this->cp_config['cp_playbill_to_media_enabled'] == '1' && isset($this->cp_config['source_notify_url']) && strlen($this->cp_config['source_notify_url']) >0)
        {
            nn_log::write_log_message('clip_log', '直播转点播切片状态反馈给epg开始：状态为：' . $state . ',task数据为：' . var_export($task_info, true), $this->sp_id, $this->function);
            $nns_mgtvbk_op_queue = nl_query_by_db("select * from nns_mgtvbk_op_queue where nns_id ='{$task_info['nns_op_id']}'", $this->obj_dc->db());
            nn_log::write_log_message('clip_log', '直播转点播切片状态反馈给epg开始：获取对列数据为：' . var_export($nns_mgtvbk_op_queue, true), $this->sp_id, $this->function);
            if(isset($nns_mgtvbk_op_queue[0]['nns_message_id']) && strlen($nns_mgtvbk_op_queue[0]['nns_message_id']) >0)
            {
                if($state == clip_task_model::TASK_C_STATE_OK)
                {
                    $result_encode = array(
                        'data_info'=>$nns_mgtvbk_op_queue[0]['nns_message_id'],
                        'ret'=>0,
                        'reason'=>clip_task_model::TASK_C_STATE_OK,
                    );
                }
                else
                {
                    $result_encode = array(
                        'data_info' => $nns_mgtvbk_op_queue[0]['nns_message_id'],
                        'ret' => 1,
                        'reason' => $state,
                    );
                    nn_log::write_log_message('clip_log', '直播转点播切片失败状态直接反馈给epg开始：参数为：' . var_export($result_encode, true), $this->sp_id, $this->function);
                    $re_feedback = $this->notify_cp_sp_playbill_to_video($result_encode, $this->cp_config['source_notify_url']);
                    nn_log::write_log_message('clip_log', '直播转点播切片失败状态直接反馈给epg完成：状态' . var_export($re_feedback, true), $this->sp_id, $this->function);
                }
            }
        }
        /*****先处理失败反馈*****/
        //else if($state == clip_task_model::TASK_C_STATE_OK)
        //{
        //    $this->message_feedback($task_info['nns_op_id'],$import_info);
        //}
		clip_task_model::update_task($task_id, $set_state);
        //查询此服务器id是否存在 不存在自动添加
        $result = nl_clip_servicers::query_by_id($this->obj_dc, $tom3u8_id);
        if (!isset($result['data_info'])&&empty($result['data_info']))
        {
            $params['nns_id']=np_guid_rand();
            $params['nns_tom3u8_id']=$tom3u8_id;
            nl_clip_servicers::add($this->obj_dc, $params);
        }
		nn_log::write_log_message('clip_log', '更新内容：' . var_export($set_state, true), $this->sp_id, $this->function);
		if ($state == clip_task_model::TASK_C_STATE_OK)
		{
		    $queue_task = new queue_task_model();
		    $arr_nns_path = pathinfo($nns_path);
		    $media_arr_ex = null;
		    if(isset($this->sp_config['media_ftp']) && strlen($this->sp_config['media_ftp']) >0 && isset($this->cp_config['is_need_real_media_info']) && $this->cp_config['is_need_real_media_info'] =='1' && 
		        isset($arr_nns_path['extension']) && strlen($arr_nns_path['extension'])>0 && strtolower($arr_nns_path['extension']) !='m3u8')
		    {
		        $media_arr_ex = $queue_task->get_media_info($this->sp_config['media_ftp'], $nns_path);
		    }
		    
			$queue_task->q_clip_ok_push($task_id, $nns_date, $nns_new_dir, $nns_path, $nns_file_size, $nns_file_md5,$nns_cdn_policy,$nns_node_id,$nns_list_ids,$this->sp_config,$media_arr_ex);
			if($clip_file_encode_enable_flag)
			{
			    $queue_task->q_clip_ok($task_id);
			}
		}
		//任务日志
		$task_log = array (
				'nns_task_id' => $task_id, 
				'nns_state' => $state, 
				'nns_desc' => $desc, 
				'nns_create_time' => $dt
		);
		clip_task_model::add_task_log($task_log);
		$data = '<result state="0" reason="OK" />';
		nn_log::write_log_message('clip_log', '任务日志：' . var_export($task_log, true), $this->sp_id, $this->function);
		$this->_bulid_result('200', 'OK', $data, $this->function);
	}

	
	/**
	 * 任务不存在或者任务已经注入成功，则可以删除
	 * @authorliangpan
	 * @date 2015-12-25
	 */
	public function delete_clip_file()
	{
		nn_log::write_log_message('clip_log', '------' . $this->function . '------', $this->sp_id, $this->function);
		$task_id = isset($_GET['nns_task_id']) ? $_GET['nns_task_id'] : null;
		$nns_new_dir = isset($_GET['nns_new_dir']) ? (empty($_GET['nns_new_dir']) ? false : true) : false;
		//if($nns_new_dir){
		//	$result = clip_task_model::can_delete_clip_file($task_id);
		//}else{
		$result = clip_task_model::can_delete_clip_file_new($task_id); //判断分集ID所在的所有任务ID是否可以删除
		//}
		if ($result)
		{
			$data = '<result state="0" reason="allow delete" />';
			$this->_bulid_result('200', 'OK', $data, $this->function);
		}
		else
		{
			$data = '<result state="1" reason="not allowed delete" />';
			$this->_bulid_result('200', 'OK', $data, $this->function);
		}
	}

	/**
	 * 获取DRM密钥
	 * @return xml
	 * @authorliangpan
	 * @date 2015-12-25
	 */
	public function get_drm_key()
	{
		nn_log::write_log_message('clip_log', '------' . $this->function . '------', $this->sp_id, $this->function);
		$content_id = isset($_GET['content_id']) ? $_GET['content_id'] : null;
		$type = isset($_GET['nns_type']) ? $_GET['nns_type'] : null;
		$par = isset($_GET['nns_par']) ? $_GET['nns_par'] : null;
		$cp_id = isset($_GET['nns_cp_id']) ? trim($_GET['nns_cp_id']) : null;
		$this->cp_id = $cp_id = (strlen($cp_id) > 0) ? $cp_id : 0;
		$video_type= (isset($_GET['nns_video_type']) && strlen(trim($_GET['nns_video_type'])) >0) ? trim($_GET['nns_video_type']) : 0;
		$video_type = in_array($video_type, array(0,1)) ? $video_type : 0;
		$array_attr_result = array (
				'state' => 1, 
				'drm_key' => '', 
				'reason' => '', 
				'type' => $type, 
				'par' => $par
		);
		$this->obj_dom = new DOMDocument("1.0", 'utf-8');
		if (strlen($content_id) <1 || empty($type))
		{
			nn_log::write_log_message('clip_log', '取DRM密钥key失败:内容ID、类型不能为空', $this->sp_id,  $this->function);
			nl_global_error_log::add($this->obj_dc, array('nns_desc'=>'获取DRM密钥：取DRM密钥key失败:内容ID、类型不能为空' ,'nns_model'=>'clip_log','nns_sp_id'=>$this->sp_id));
			$array_attr_result['reason'] = 'params error';
			$this->make_xml('result', null, $array_attr_result);
			$this->_bulid_result('200', 'OK', $$this->obj_dom->saveXML(), $this->function, false);
		}
		//查询drm配置
		$is_drm_config = $this->check_drm_action();
		//播控服务器内部失败
		if ($is_drm_config === false)
		{
			$array_attr_result['reason'] = 'bk inside fail';
			$this->make_xml('result', null, $array_attr_result);
			$this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
		}
		//播控DRM 开关关闭
		else if ($is_drm_config === true)
		{
			$array_attr_result['reason'] = 'drm config turn off';
			$nns_link_file=nn_log::write_log_message('clip_log', '获取DRM密钥：drm config turn off', $this->sp_id, $this->function);
			nl_global_error_log::add($this->obj_dc, array('nns_desc'=>'获取DRM密钥：drm config turn off' ,'nns_model'=>'clip_log','nns_sp_id'=>$this->sp_id,'nns_link_file'=>$nns_link_file));
			$this->make_xml('result', null, $array_attr_result);
			$this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
		}
		//切片前开关打开  走以前drm老逻辑  （Verimatrix DRM加密）
		if ($is_drm_config === 0)
		{
			$array_attr_result['drm_type'] = 0;
			include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/np/np_http_curl.class.php';
			$curl = new np_http_curl_class();
			
			$str_drm_key_url = defined('DRM_KEY_URL') ? DRM_KEY_URL : '';
			if(strlen($str_drm_key_url) < 1)
			{
			    $str_drm_key_url = (isset($this->sp_config['q_drm_key_file_url']) && strlen($this->sp_config['q_drm_key_file_url']) > 0) ? : '';
			}
			if(strlen($str_drm_key_url) < 1)
			{
			    $str_drm_key_url = (isset($this->cp_config['q_drm_key_file_url']) && strlen($this->cp_config['q_drm_key_file_url']) > 0) ? : '';
			}
			if(strlen($str_drm_key_url) < 1)
			{
			    $nns_link_file=nn_log::write_log_message('clip_log', '取DRM密钥key失败:q_drm_key_file_url参数为空：' . var_export($str_drm_key_url, true), $this->sp_id, $this->function);
			    nl_global_error_log::add($this->obj_dc, array('nns_desc'=>'取DRM密钥key失败:q_drm_key_file_url参数为空：' . var_export($str_drm_key_url, true) ,'nns_model'=>'clip_log','nns_sp_id'=>$this->sp_id,'nns_link_file'=>$nns_link_file,'nns_media_import_id'=>$content_id));
			    $array_attr_result['reason'] = 'get drm key url not set';
			    $this->make_xml('result', null, $array_attr_result);
			    $this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
			}
			$url = $str_drm_key_url . 't=' . $type . '&p=' . $par . '&r=' . $content_id;
			$re = $curl->get($url);
			if (!$re)
			{
				$nns_link_file=nn_log::write_log_message('clip_log', '取DRM密钥key失败:CURL失败' . var_export($re, true), $this->sp_id, $this->function);
				nl_global_error_log::add($this->obj_dc, array('nns_desc'=>'获取DRM密钥：取DRM密钥key失败:CURL失败' ,'nns_model'=>'clip_log','nns_sp_id'=>$this->sp_id,'nns_link_file'=>$nns_link_file,'nns_media_import_id'=>$content_id));
				$array_attr_result['reason'] = 'get drm key fail';
				$this->make_xml('result', null, $array_attr_result);
				$this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
			}
			//$re = pack("H16",strtolower($re));var_export($re);die;
			$re = bin2hex($re);
			$array_attr_result['state'] = 0;
			$array_attr_result['drm_key'] = $re;
			$array_attr_result['drm_type'] = 0;
			$array_attr_result['reason'] = 'OK';
			$result = $this->make_xml('result', null, $array_attr_result);
			$this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
		}
		//切片后开关打开   新逻辑 （ 数字太和 DRM 加密）
		else if($is_drm_config === 1)
		{
			$this->get_data_info($array_attr_result,$video_type,$content_id, $cp_id);
			$array_attr_result['drm_type'] = 1;
			$flag_sp_config = (!isset($this->sp_config['clip_drm_config_params']) || !is_array($this->sp_config['clip_drm_config_params'])) ? false : true;
			$flag_cp_config = (!isset($this->cp_config['clip_drm_config_params']) || !is_array($this->cp_config['clip_drm_config_params'])) ? false : true;
			if ($flag_sp_config === false && $flag_cp_config === false)
			{
			    if($flag_sp_config === false)
			    {
    				$nns_link_file=nn_log::write_log_message('clip_log', '获取SP数字太和配置文件配置失败' . var_export($this->sp_config['clip_drm_config_params'], true), $this->sp_id, $this->function);
    				nl_global_error_log::add($this->obj_dc, array('nns_desc'=>'获取DRM密钥：获取SP数字太和配置文件配置失败'. var_export($this->sp_config['clip_drm_config_params'], true) ,'nns_model'=>'clip_log','nns_sp_id'=>$this->sp_id,'nns_link_file'=>$nns_link_file,'nns_media_import_id'=>$content_id));
			    }
			    if($flag_cp_config === false)
			    {
			        $nns_link_file=nn_log::write_log_message('clip_log', '获取CP数字太和配置文件配置失败' . var_export($this->cp_config['clip_drm_config_params'], true), $this->sp_id, $this->function);
			        nl_global_error_log::add($this->obj_dc, array('nns_desc'=>'获取DRM密钥：获取CP数字太和配置文件配置失败'. var_export($this->cp_config['clip_drm_config_params'], true) ,'nns_model'=>'clip_log','nns_sp_id'=>$this->sp_id,'nns_link_file'=>$nns_link_file,'nns_media_import_id'=>$content_id));
			    }
				$array_attr_result['reason'] = 'clip drm config error';
				$array_attr_result['drm_key'] = '';
				$this->make_xml('result', null, $array_attr_result);
				$this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
			}
			$array_attr_result['state'] = 0;
			$array_attr_result['reason'] = 'OK';
			$array_attr_result['drm_key'] = '';
			$result = $this->make_xml('result', null, $array_attr_result);
			$drm_param_list = $this->make_xml('drm_param_list',null,null,$result);
			$this->make_xml('program_name', $this->program_name,null,$drm_param_list);
			$this->make_xml('content_description', $this->content_description,null,$drm_param_list);
			if($flag_sp_config)
			{
    			foreach ($this->sp_config['clip_drm_config_params'] as $key => $val)
    			{
    				$this->make_xml($key, $val,null,$drm_param_list);
    			}
			}
			else
			{
			    foreach ($this->cp_config['clip_drm_config_params'] as $key => $val)
			    {
			        $this->make_xml($key, $val,null,$drm_param_list);
			    }
			}
			$this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
		}
		//Marlin DRM 加密
		else if($is_drm_config === 2)
		{
		    #TODO
		    $this->get_data_info($array_attr_result,$video_type,$content_id, $cp_id);
		    $array_attr_result['drm_type'] = 2;
		    if(strlen($this->drm_ext_info) <1)
		    {
		        $array_attr_result['reason'] = 'clip media make ext info error';
		        $array_attr_result['drm_key'] = '';
		        $nns_link_file=nn_log::write_log_message('clip_log', '获取片源扩展信息失败' . var_export($this->drm_ext_info, true), $this->sp_id, $this->function);
		        nl_global_error_log::add($this->obj_dc, array('nns_desc'=>'获取片源扩展信息失败' . var_export($this->drm_ext_info, true) ,'nns_model'=>'clip_log','nns_sp_id'=>$this->sp_id,'nns_link_file'=>$nns_link_file,'nns_media_import_id'=>$content_id));
		        $this->make_xml('result', null, $array_attr_result);
		        $this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
		    }
		    $arr_drm_ext_data = json_decode($this->drm_ext_info,true);
		    if(empty($arr_drm_ext_data) || !is_array($arr_drm_ext_data))
		    {
		        $array_attr_result['reason'] = 'clip media make ext info empty';
		        $array_attr_result['drm_key'] = '';
		        $nns_link_file=nn_log::write_log_message('clip_log', '获取片源扩展信息失败empty' . var_export($$arr_drm_ext_data, true), $this->sp_id, $this->function);
		        nl_global_error_log::add($this->obj_dc, array('nns_desc'=>'获取片源扩展信息失败empty' . var_export($arr_drm_ext_data, true) ,'nns_model'=>'clip_log','nns_sp_id'=>$this->sp_id,'nns_link_file'=>$nns_link_file,'nns_media_import_id'=>$content_id));
		        $this->make_xml('result', null, $array_attr_result);
		        $this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
		    }
		    $array_attr_result['state'] = 0;
		    $array_attr_result['reason'] = 'OK';
		    $arr_drm_ext_data_keys = array_keys($arr_drm_ext_data);
		    $str_key = $arr_drm_ext_data_keys[array_rand($arr_drm_ext_data_keys)];
		    $str_val = $arr_drm_ext_data[$str_key];
		    $array_attr_result['drm_key'] = $str_val;
		    $array_attr_result['par'] = $str_key;
		    $this->make_xml('result', null, $array_attr_result);
		    $this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
		}
		else
		{
		    nn_log::write_log_message('clip_log', '不存在此drm加密模式'.var_export($is_drm_config,true), $this->sp_id,  $this->function);
		    nl_global_error_log::add($this->obj_dc, array('nns_desc'=>'不存在此drm加密模式'.var_export($is_drm_config,true) ,'nns_model'=>'clip_log','nns_sp_id'=>$this->sp_id));
		    $array_attr_result['reason'] = 'no such drm model';
		    $this->make_xml('result', null, $array_attr_result);
		    $this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
		}
	}

	/**
	 * 获取直播、点播名称/描述等信息
	 * @param array $array_attr_result 组装xml的数据数组
	 * @param int $video_type 影片类型  0 点播 | 1 直播
	 * @param string $content_id 注入id
	 * @param string $cp_id cp_id
	 * @authorliangpan
	 * @date 2015-12-25
	 */
	private function get_data_info($array_attr_result,$video_type,$content_id, $cp_id)
	{
		switch ($video_type)
		{
			case 0:
				include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/video/vod_index.class.php';
				include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/video/vod_media.class.php';
				include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/video/vod.class.php';
				$result_media = nl_vod_media_v2::get_video_index_info_by_import_id($this->obj_dc, $content_id, $cp_id);
				if ($result_media['ret'] != 0)
				{
					nn_log::write_log_message('clip_log', $result_media['reason'], $this->sp_id, $this->function);
					$array_attr_result['reason'] = 'bk inside fail';
					$this->make_xml('result', null, $array_attr_result);
					$this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
				}
				if (!isset($result_media['data_info'][0]) || !is_array($result_media['data_info'][0]))
				{
					nn_log::write_log_message('clip_log', '未找到该片源信息,上传参数为：' . var_export($_GET, true), $this->sp_id, $this->function);
					$array_attr_result['reason'] = 'no media find';
					$this->make_xml('result', null, $array_attr_result);
					$this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
				}
				$result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $result_media['data_info'][0]['nns_vod_index_id']);
				$result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_media['data_info'][0]['nns_vod_id']);
				if ($result_index['ret'] != 0)
				{
					nn_log::write_log_message('clip_log', $result_index['reason'], $this->sp_id, $this->function);
					$array_attr_result['reason'] = 'bk inside fail';
					$this->make_xml('result', null, $array_attr_result);
					$this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
				}
				if (!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]))
				{
					nn_log::write_log_message('clip_log', '未找到该分集信息,分集id为：' . $result_media['data_info'][0]['nns_vod_index_id'], $this->sp_id, $this->function);
					$array_attr_result['reason'] = 'no index find';
					$this->make_xml('result', null, $array_attr_result);
					$this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
				}
				if ($result_video['ret'] != 0)
				{
					nn_log::write_log_message('clip_log', $result_video['reason'], $this->sp_id, $this->function);
					$array_attr_result['reason'] = 'bk inside fail';
					$this->make_xml('result', null, $array_attr_result);
					$this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
				}
				if (!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]))
				{
					nn_log::write_log_message('clip_log', '未找到该主媒资信息,主媒资id为：' . $result_media['data_info'][0]['nns_vod_id'], $this->sp_id, $this->function);
					$array_attr_result['reason'] = 'no vod find';
					$this->make_xml('result', null, $array_attr_result);
					$this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
				}
				$result_index['data_info'][0]['nns_index']++;
				$this->program_name = "[点播] [{$result_video['data_info'][0]['nns_name']}] [第{$result_index['data_info'][0]['nns_index']}集] {$result_media['data_info'][0]['nns_mode']}";
				$this->content_description =  $result_index['data_info'][0]['nns_summary'];
				$this->drm_ext_info = $result_media['data_info'][0]['nns_drm_ext_info'];
				break;
			case 1:
// 				include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/video/live_index.class.php';
				include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/video/live_media.class.php';
				include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/video/live.class.php';
				$result_media=nl_live_media::get_live_media_info_by_import_id($this->obj_dc, $content_id, $cp_id);
				if ($result_media['ret'] != 0)
				{
					nn_log::write_log_message('clip_log', $result_media['reason'], $this->sp_id, $this->function);
					$array_attr_result['reason'] = 'bk inside fail';
					$this->make_xml('result', null, $array_attr_result);
					$this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
				}
				if (!isset($result_media['data_info'][0]) || !is_array($result_media['data_info'][0]))
				{
					nn_log::write_log_message('clip_log', '未找到该片源信息,上传参数为：' . var_export($_GET, true), $this->sp_id, $this->function);
					$array_attr_result['reason'] = 'no live media find';
					$this->make_xml('result', null, $array_attr_result);
					$this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
				}
				$result_live = nl_live::get_live_info_by_id($this->obj_dc, $result_media['data_info'][0]['nns_live_id'],$cp_id);
				if ($result_live['ret'] != 0)
				{
					nn_log::write_log_message('clip_log', $result_live['reason'], $this->sp_id, $this->function);
					$array_attr_result['reason'] = 'bk inside fail';
					$this->make_xml('result', null, $array_attr_result);
					$this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
				}
				if (!isset($result_live['data_info'][0]) || !is_array($result_live['data_info'][0]))
				{
					nn_log::write_log_message('clip_log', '未找到该主媒资信息,主媒资id为：' . $result_live['data_info'][0]['nns_live_id'], $this->sp_id, $this->function);
					$array_attr_result['reason'] = 'no live find';
					$this->make_xml('result', null, $array_attr_result);
					$this->_bulid_result('200', 'OK', $this->obj_dom->saveXML(), $this->function, false);
				}
				$this->program_name = "[直播] [{$result_live['data_info'][0]['nns_name']}] {$result_media['data_info'][0]['nns_mode']}";
				$this->content_description =  $result_live['data_info'][0]['nns_summary'];
				$this->drm_ext_info = $result_media['data_info'][0]['nns_drm_ext_info'];
				break;
			default:
				break;
		}
		return ;
	}
	
	
	/**
	 * 组装基础dom对象类
	 * @param string $key 键
	 * @param string $val 值
	 * @param array $arr_attr attr值
	 * @param object $parent 父级对象
	 * @return object
	 * @date 2015-05-04
	 */
	private function make_xml($key, $val = null, $arr_attr = null,$parent=null)
	{
		if(is_null($parent))
		{
			$parent=$this->obj_dom;
		}
		$$key = isset($val) ? $this->obj_dom->createElement($key, $val) : $this->obj_dom->createElement($key);
		if (!empty($arr_attr) && is_array($arr_attr))
		{
			foreach ($arr_attr as $attr_key => $attr_val)
			{
				$domAttribute = $this->obj_dom->createAttribute($attr_key);
				$domAttribute->value = $attr_val;
				$$key->appendChild($domAttribute);
				$this->obj_dom->appendChild($$key);
			}
		}
		$parent->appendChild($$key);
		//unset($dom);
		unset($parent);
		return $$key;
	}

	/**
	 * 检查切片中是否需要drm验证
	 * @param string $function
	 * @author liangpan
	 * @date 2015-12-25
	 */
	private function check_drm_action()
	{
		if (empty($this->sp_id))
		{
			nn_log::write_log_message('clip_log', '检查运营商id为空：' . var_export($this->sp_id, true), $this->sp_id, $this->function);
			if(strlen($this->cp_id) <1)
			{
			    nn_log::write_log_message('clip_log', '检查CP_id为空：' . var_export($this->cp_id, true), $this->sp_id, $this->function);
			    return false;
			}
		}
		include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/sp/sp.class.php';
		$result_sp = nl_sp::query_by_id($this->obj_dc, $this->sp_id);
		if ($result_sp['ret'] != 0)
		{
			nn_log::write_log_message('clip_log', '查询sp配置信息数据库错误：'.var_export($result_sp, true), $this->sp_id, $this->function);
			return false;
		}
		if (!isset($result_sp['data_info'][0]))
		{
			nn_log::write_log_message('clip_log', 'SP查询没有运营商信息：' . var_export($result_sp, true), $this->sp_id, $this->function);
			return false;
		}
		$this->sp_config = (isset($result_sp['data_info'][0]['nns_config']) && !empty($result_sp['data_info'][0]['nns_config'])) ? json_decode($result_sp['data_info'][0]['nns_config'], true) : array ();
		if (!isset($this->sp_config['main_drm_enabled']) || $this->sp_config['main_drm_enabled'] != 1)
		{
			nn_log::write_log_message('clip_log', 'SP配置drm主开关关闭', $this->sp_id, $this->function);
// 			return true;
		}
		if (isset($this->sp_config['q_disabled_drm']) && $this->sp_config['q_disabled_drm'] == 1)
		{
			return 0;
		}
		if (isset($this->sp_config['clip_drm_enabled']) && $this->sp_config['clip_drm_enabled'] == 1)
		{
			return 1;
		}
		include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/cp/cp.class.php';
		$result_cp= nl_cp::query_by_id($this->obj_dc, $this->cp_id);
		if ($result_cp['ret'] != 0)
		{
		    nn_log::write_log_message('clip_log', '查询CP配置信息数据库错误，可能是此播控版本cp表不存在：cp为'.$this->cp_id . var_export($result_cp, true), $this->sp_id, $this->function);
		    return true;
		}
		if (!isset($result_cp['data_info']))
		{
		    nn_log::write_log_message('clip_log', '查询没有CP信息：cp为'.$this->cp_id . var_export($result_cp, true), $this->sp_id, $this->function);
		    return true;
		}
		$this->cp_config = (isset($result_cp['data_info']['nns_config']) && !empty($result_cp['data_info']['nns_config'])) ? json_decode($result_cp['data_info']['nns_config'], true) : array ();
		if (!isset($this->cp_config['main_drm_enabled']) || $this->cp_config['main_drm_enabled'] != 1)
		{
		    nn_log::write_log_message('clip_log', 'CP配置drm主开关关闭', $this->sp_id, $this->function);
		    return true;
		}
		if (isset($this->cp_config['q_disabled_drm']) && $this->cp_config['q_disabled_drm'] == 1)
		{
		    return 0;
		}
		if (isset($this->cp_config['clip_drm_enabled']) && $this->cp_config['clip_drm_enabled'] == 1)
		{
		    return 1;
		}
		if (isset($this->cp_config['q_marlin_drm_enable']) && $this->cp_config['q_marlin_drm_enable'] == 1)
		{
		    return 2;
		}
		nn_log::write_log_message('clip_log', 'drm开关关闭', $this->sp_id, $this->function);
		return true;
	}

	/**
	 * 组装返回的xml文件 
	 * @param int $http_code http状态码
	 * @param string $resson 原因描述
	 * @param string $data xml格式的字符串
	 * @authorliangpan
	 * @date 2015-12-25
	 */
	public function _bulid_result($http_code, $resson, $data = null, $function = 'public', $is_need_header = true)
	{
		header('HTTP/1.1 ' . $http_code . ' ' . $resson);
		header('Status: ' . $http_code . ' ' . $resson);
		header("Content-Type:text/xml;charset=utf-8");
		$str_header = $is_need_header ? '<?xml version="1.0" encoding="UTF-8"?>' : '';
 		$str_header = strpos($data,'<?xml') === false ? '<?xml version="1.0" encoding="UTF-8"?>' : '';
		$data = ($data != null) ? $str_header . $data : '';
		nn_log::write_log_message('clip_log', '原因描述：' . var_export($resson, true), $this->sp_id, $function);
		nn_log::write_log_message('clip_log', '返回结果：' . $data, $this->sp_id, $function);
		echo $data;
		die();
	}
	
	/**
	 * 切片信息消息反馈
	 * @param $op_queue_id
     * @param $import_info
	 * @param number $state 0 成功 1 失败 [注入平台统一0成功 1失败]
	 * @param string $str_message
	 * @authorliangpan
	 * @date 2017-02-20
	 */
	public function message_feedback($op_queue_id,$import_info,$state=1,$str_message='download fail')
	{
	    nn_log::write_log_message('clip_log', "---切片消息反馈开始---参数：".var_export(func_get_args(),true), $this->sp_id, $this->function);
	    include_once dirname(dirname(dirname(__FILE__))).'/op/op_queue.class.php';
	    include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/project_group/project_group.class.php';
	    include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/op_queue/op_queue.class.php';
	    include_once dirname(dirname(__FILE__)).'/models/video_model.php';
	    include_once dirname(dirname(__FILE__)).'/models/public_model_exec.class.php';
	    include_once dirname(dirname(dirname(__FILE__))) . '/v2/common.php';

	    if(strlen($this->cp_id) < 1)
	    {
	        nn_log::write_log_message('clip_log', "CP id 错误，传入参数为：".var_export($this->cp_id,true), $this->sp_id, $this->function);
	        return ;
	    }
        $cp_flag = $this->_get_cp_config($this->cp_id);
        if(!$cp_flag)
        {
            nn_log::write_log_message('clip_log', "获取CP配置失败：".var_export($this->error_info,true), $this->sp_id, $this->function);
            return ;
        }

	    $sp_flag = $this->_get_sp_config($this->sp_id);
	    if(!$sp_flag)
	    {
	       nn_log::write_log_message('clip_log', "获取SP配置失败：".var_export($this->error_info,true), $this->sp_id, $this->function);
	       return ;
	    }
	    /*****通用配置全部转移到API_V2或者API_V4里面*****/
//	    if(!isset($this->sp_config['global_config_bind_key']) || empty($this->sp_config['global_config_bind_key']))
//	    {
//	        nn_log::write_log_message('clip_log', "获取SP配置不需要消息反馈!!!!!", $this->sp_id, $this->function);
//	        return ;
//	    }
//	    $str_need_feedback = explode(',', $this->sp_config['global_config_bind_key']);
//	    $result_group = nl_project_group::query_by_ids($this->obj_dc, $str_need_feedback);
//	    if($result_group['ret'] !=0)
//	    {
//	        nn_log::write_log_message('clip_log', "查询消息反馈分组信息:".$result_group['reason'], $this->sp_id, $this->function);
//	        return ;
//	    }
//	    if(!is_array($result_group['data_info']) || empty($result_group['data_info']))
//	    {
//	        nn_log::write_log_message('clip_log', "查询消息反馈分组信息为空，未配置", $this->sp_id, $this->function);
//	        return ;
//	    }
	    $result_op_queue = nl_op_queue::query_op_queue_info_by_id($this->obj_dc,$op_queue_id);
	    if($result_op_queue['ret'] !=0)
	    {
	        nn_log::write_log_message('clip_log', "查询OP_QUEUE队列信息:".$result_op_queue['reason'], $this->sp_id, $this->function);
	        return ;
	    }
	    if(!is_array($result_op_queue['data_info'][0]) || empty($result_op_queue['data_info'][0]))
	    {
	        nn_log::write_log_message('clip_log', "查询OP_QUEUE队列信息为空，错误", $this->sp_id, $this->function);
	        return ;
	    }
        $item = array(
            'nns_message_id' => $result_op_queue['data_info'][0]['nns_message_id'],
            'nns_action' => $result_op_queue['data_info'][0]['nns_action'],
            'nns_org_id' => $result_op_queue['data_info'][0]['nns_org_id'],
        );

        /*****通用配置全部转移到API_V2或者API_V4里面*****/
//	    $result_meta_info = public_model_exec::get_asset_id_by_sp_id($this->sp_id, 'media', $result_op_queue['data_info'][0]['nns_media_id'],null,$this->sp_config);
//	    if($result_meta_info['ret'] !=0)
//	    {
//	        nn_log::write_log_message('clip_log', "查询片源注入CDN/EPG ID错误：".$result_meta_info['reason'], $this->sp_id, $this->function);
//	        return ;
//	    }
        /*****此处切片并未注入CDN，故还没有CDNID*****/
//	    $cdn_id = $result_meta_info['data_info'];
//	    $return_feedback_url = array();
//	    foreach ($result_group['data_info'] as $group_val)
//	    {
//	        $return_feedback_url[] = isset($group_val['nns_value']) && !empty($group_val['nns_value']) ? json_decode($group_val['nns_value']) : array();
//	        if(!isset($group_val['nns_value']) || empty($group_val['nns_value']))
//	        {
//	            continue;
//	        }
//	        $group_val['nns_value'] = json_decode($group_val['nns_value']);
//	        if(!is_array($group_val['nns_value']) || empty($group_val['nns_value']))
//	        {
//	            continue;
//	        }
//	        foreach ($group_val['nns_value'] as $group_value)
//	        {
//	            if(isset($group_value['cp']) && $group_value['cp'] == $cp_id)
//	            {
//	                $return_feedback_url[] = $group_value;
//	            }
//	        }
//	    }
//	    if(!is_array($return_feedback_url) || empty($return_feedback_url))
//	    {
//	        nn_log::write_log_message('clip_log', "查询消息反馈分组信息为空，配置了，但是没找到匹配项", $this->sp_id, $this->function);
//	        return ;
//	    }
//	    foreach ($return_feedback_url as $feedback_val)
//	    {
//	        $obj_feedback = new op_queue();
//	        $mg_file_id=$mg_part_id=$mg_asset_id=$video_info=$cp_config=null;
//	        $item = array('nns_message_id'=>$message_id);
//	        $obj_feedback->message_feedback($feedback_val, $item, $cdn_id, 'media', $video_info, $mg_asset_id, $mg_part_id, $mg_file_id, $cp_config,$state,$str_message);
//	    }

        $flag_message_feedback_enabled = (!isset($this->sp_config['message_feedback_enabled']) || $this->sp_config['message_feedback_enabled'] != 1) ? true : false;
        if(strlen($item['nns_message_id']) > 0 && $flag_message_feedback_enabled)
        {
            $obj_feedback = new op_queue();
            if(isset($this->sp_config['message_feedback_mode']) && $this->sp_config['message_feedback_mode'] == 1)
            {
                $obj_feedback->message_feedback($this->cp_config,$item,'',3,$import_info,$import_info[0]['video_import_id'],$import_info[0]['index_import_id'],$import_info[0]['media_import_id'],$this->cp_config,$state,$str_message);
            }
            else
            {
                /********新版未迁移切片，但反馈走新版 xinxin.deng 2018/7/9 15:45   start********/
                global $g_bk_version_number;
                $bk_version_number = $g_bk_version_number == '1' ? true : false;
                unset($g_bk_version_number);
                $project = evn::get("project");
                $classname = \ns_core\m_load::load("ns_api.{$project}.message.{$this->cp_id}.message");
                $obj_feedback_v2 = new message();
                if ($bk_version_number && $classname)
                {
                    $arr_info = array(
                        'cdn_id' => '',
                        'site_id' => $this->sp_config['site_id'],
                        'mg_asset_type' => 3,
                        'mg_asset_id' => $import_info[0]['video_import_id'],
                        'mg_part_id' => $import_info[0]['index_import_id'],
                        'mg_file_id' => $import_info[0]['media_import_id'],
                    );
                    //兼容湖南电信二级消息反馈，1表示成功，0表示失败。
                    //$code = $state === 1 ? 0 : 1;
                    $obj_feedback_v2->is_ok($item['nns_message_id'], $state, $str_message, $arr_info, $this->sp_id);
                    unset($obj_feedback_v2);
                }
                /********新版未迁移切片，但反馈走新版 xinxin.deng 2018/7/9 15:45   end********/
                else
                {
                    $obj_feedback->message_feedback($this->sp_config,$item,'',3,$import_info,$import_info[0]['video_import_id'],$import_info[0]['index_import_id'],$import_info[0]['media_import_id'],$this->cp_config,$state,$str_message);
                }
            }
        }
	    return ;
	}
	
	
	
	/**
	 * 消息反馈
	 * @param unknown $cp_id
	 * @param unknown $result_encode
	 * @param unknown $encode_info
	 */
	public function notify_cp_sp_playbill_to_video($result_encode,$url)
	{

	    $playbill_info = array(
	        'id'=>$result_encode['data_info'],
	        'state'=>$result_encode['ret'] == 0 ? 2 : 3,
	        'summary'=>$result_encode['reason'],
	    );
	    return $this->_execute_curl($url,'post',array('a'=>'update_vod_playbill_transcode_state','playbill_info'=>json_encode(array($playbill_info))));
	}
	
	
	/**
	 * 执行curl
	 * @param unknown $url
	 * @param unknown $data
	 * @return multitype:NULL unknown string
	 */
	public function _execute_curl($send_url,$func='get',$content=null,$arr_header=null,$time=60)
	{
	    $obj_curl = new np_http_curl_class();
	    $curl_data = $obj_curl->do_request($func,$send_url,$content,$arr_header,60);
	    $curl_info = $obj_curl->curl_getinfo();
	    $http_code = $curl_info['http_code'];
	    if($curl_info['http_code'] != '200')
	    {
	        return $this->_return_data(1,'[访问第三方接口,CURL接口地址]：'.$send_url.'[HTTP状态码]：'.$curl_info['http_code'].'[HTTP错误]：'.$obj_curl->curl_error(),$curl_data);
	    }
	    else
	    {
	        return $this->_return_data(0,'[访问第三方接口,CURL接口地址]：'.$send_url,$curl_data);
	    }
	}
}