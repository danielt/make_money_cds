<?php
/**
 * @description:爱上项目，cms媒资上下线通过透传通知cdn
 * @author:xinxin.deng
 * @date: 2018/1/9 11:56
 */
ini_set('display_errors', 0);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
$arr_dir = explode('|',str_replace(array('/','\\'), '|', dirname(__DIR__)));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
#是否是定时器
define("IS_LOG_TIMER_OPERATION", true);
include_once dirname(dirname(dirname(__FILE__))) . '/' . ORG_ID . '/init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/callback_notify/import_queue.class.php';

class epg_unline_execute extends nn_timer
{

	public function action($params = null)
	{
		$this->msg('开始执行...');
		do_timer_action();
		$this->msg('执行结束...');
	}
}
$epg_unline_execute = new epg_unline_execute("pass_queue_execute", ORG_ID);
$epg_unline_execute->run();

function do_timer_action()
{
	$dc = nl_get_dc(array (
			'db_policy' => NL_DB_WRITE,
			'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
	));
	$dc->open();
	//从任务表取内容，调用C2执行
	i_echo('start');
	$sp_config = sp_model::get_sp_config(ORG_ID);
	$sp_config['nns_id'] = ORG_ID;
	if (!isset($sp_config['pass_queue_disabled']) || (int)$sp_config['pass_queue_disabled'] == 0)
	{
		i_echo('close pass queue');
	}
	else
	{
		$params = array(
				'nns_org_id' => ORG_ID,
				'nns_audit' => 1,//审核通过
				'nns_pause' => 0,//运行状态
				'nns_status' => 1,//正在注入
		);
		$pass_queue = nl_pass_queue::get_pass_queue_for_timer($dc,$params,100);
		if ($pass_queue['ret'] != 0 || empty($pass_queue['data_info']))
		{
			i_echo('no pass queue');
		}
		$import_queue = new import_queue($dc);
		foreach ($pass_queue['data_info'] as $info_list)
		{
			$import_queue->import_pass_queue($info_list,$sp_config);
		}
		i_echo('end');
	}
}



