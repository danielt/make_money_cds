<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors',1);
set_time_limit(0); 
include_once dirname(dirname(__FILE__)).'/mgtv_init.php';
$db = nn_get_db(NL_DB_WRITE);
$db->open();

//var_dump($db);die;

$sql="truncate table nns_mgtvbk_c2_log;
truncate table nns_mgtvbk_c2_task;
truncate table nns_mgtvbk_category_content;
truncate table nns_mgtvbk_clip_task;
truncate table nns_mgtvbk_clip_task_log;
truncate table nns_mgtvbk_op_log;
truncate table nns_mgtvbk_op_queue;
truncate table nns_mgtvbk_op_weight;

truncate table nns_mgtvbk_import_epg_log;";

if (nl_execute_by_db($sql, $db)){
	echo 'ok';
}else {
	echo 'no';
}