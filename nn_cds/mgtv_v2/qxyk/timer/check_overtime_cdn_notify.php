<?php
/**
 * 趣享辽宁兼容服务器性能差，ngnix常出现502的情况导致反馈失败
 * 获取片源任务、正在注入CDN的任务、超过1小时未反馈的任务进行手动反馈，防止之后注入任务堵塞
 */
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
array_pop($arr_dir);
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/define.php';
include_once dirname(dirname(dirname(__FILE__))) . '/models/child_models/c2_task_assign.class.php';

class check_overtime_cdn_notify extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->do_timer_action();
        $this->msg('执行结束...');
    }

    public function do_timer_action()
    {
        $obj_dc = nl_get_dc(
            array(
                'db_policy' =>  NL_DB_READ | NL_DB_WRITE,
                'cache_policy' => NP_KV_CACHE_TYPE_NULL
            )
        );
        //获取超时未反馈的C2任务，获取片源任务、正在注入CDN的任务、超过1小时未反馈的任务
        $overtime_sql = "select nns_ref_id from nns_mgtvbk_c2_task where nns_type='media' and nns_status=5 and nns_modify_time < (NOW() - INTERVAL 7200 SECOND)";
        $overtime_re = nl_query_by_db($overtime_sql,$obj_dc->db());
        if(!is_array($overtime_re))
        {
            echo 'no overtime task';
            return ;
        }
        $sp_sql = "select * from nns_mgtvbk_sp where nns_id='".ORG_ID."'";
        $sp_re = nl_query_by_db($sp_sql,$obj_dc->db());
        if(!is_array($sp_re))
        {
            echo 'query sp failed:'.ORG_ID;
            return ;
        }
        $sp_config = json_decode($sp_re[0]['nns_config'],true);
        $obj_c2_task_assign = new c2_task_assign();
        foreach ($overtime_re as $value)
        {
            $this->msg('处理注入ID：'.$value['nns_ref_id']);
            $assign_re = $obj_c2_task_assign->auto_load_task_model($sp_config['c2_import_cdn_model'], 'save_c2_notify',$obj_dc,ORG_ID,$sp_config,array('id'=>$value['nns_ref_id']));
            $this->msg('处理结果为：'.var_export($assign_re,true));
            var_dump($assign_re) . "\r\n";
        }
        unset($obj_c2_task_assign);
        unset($obj_dc);
    }
}
$check_overtime_cdn_notify = new check_overtime_cdn_notify("check_overtime_cdn_notify", ORG_ID,__FILE__);
$check_overtime_cdn_notify->run();

