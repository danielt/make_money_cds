<?php
/**
 * 趣享辽宁的切片服务器有问题，有时上传失败或链接失败，故增加上传重试
 */
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
array_pop($arr_dir);
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/define.php';


class restart_upload_fail_task extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->do_timer_action();
        $this->msg('执行结束...');
    }

    public function do_timer_action()
    {
        $obj_dc = nl_get_dc(
            array(
                'db_policy' =>  NL_DB_READ | NL_DB_WRITE,
                'cache_policy' => NP_KV_CACHE_TYPE_NULL
            )
        );

        //获取5分钟之前，等待获取播放串的C2片源任务
        $sql = "select c2.nns_id from nns_mgtvbk_c2_task as c2 left join nns_mgtvbk_c2_log as l on c2.nns_id=l.nns_task_id where c2.nns_status = -1 and (l.nns_result like '[-1]上传FTP%' or l.nns_result like '[-1]FTP链接失败%') and l.nns_notify_result != 0 group by c2.nns_id";
        $re = nl_query_by_db($sql,$obj_dc->db());
        if(!is_array($re))
        {
            echo 'no xml_upload_fail task';
            return ;
        }
        $ids = "";
        //处理XML上传失败的数据
        foreach ($re as $value)
        {
            $ids .= "'{$value['nns_id']}',";
        }
        $ids = rtrim($ids,",");
        $update_sql = "update nns_mgtvbk_c2_task set nns_status = 1 where nns_id in ({$ids})";
        nl_execute_by_db($update_sql,$obj_dc->db());
        unset($obj_dc);
    }
}
$restart_upload_fail_task = new restart_upload_fail_task("restart_upload_fail_task", ORG_ID,__FILE__);
$restart_upload_fail_task->run();