<?php
/**
 * 趣享辽宁通过CDN鉴权接口验证注入CDN是否成功
 * 获取等待获取播放串、5分钟之前的任务进行判断
 */
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
array_pop($arr_dir);
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/define.php';
include_once dirname(dirname(dirname(__FILE__))) . '/models/child_models/c2_task_assign.class.php';
include_once dirname(dirname(__FILE__)) . '/models/child_c2_task_notify_model.php';

class check_import_cdn extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->do_timer_action();
        $this->msg('执行结束...');
    }

    public function do_timer_action()
    {
        $obj_dc = nl_get_dc(
            array(
                'db_policy' =>  NL_DB_READ | NL_DB_WRITE,
                'cache_policy' => NP_KV_CACHE_TYPE_NULL
            )
        );
        //获取5分钟之前，等待获取播放串的C2片源任务
        $sql = "select nns_id,nns_ref_id from nns_mgtvbk_c2_task where nns_type='media' and nns_status=6 and nns_modify_time < (NOW() - INTERVAL 300 SECOND)";
        $re = nl_query_by_db($sql,$obj_dc->db());
        if(!is_array($re))
        {
            echo 'no cdn_query_mediaurl task';
            return ;
        }

        $notify_model = new child_c2_task_notify_model(ORG_ID);

        //处理等待获取播放串的数据
        foreach ($re as $value)
        {
            nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '根据CDN鉴权接口判断是否注入成功,注入ID为：'.$value['nns_ref_id'], ORG_ID);
            $is_import = $notify_model->notify_explain($value['nns_ref_id']);
            nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '判断结果为：' . var_export($is_import,true), ORG_ID);

            if($is_import === false)
            {
                //根据C2任务ID查询c2片源注入日志
                $log_query_params = array(
                    'nns_task_id' => $value['nns_id'],
                    'nns_task_type' => 'Movie',
                    'nns_org_id' => ORG_ID,
                );
                //可能会查询出多个C2日志ID
                $c2_log_re = nl_c2_log::query_by_condition($obj_dc, $log_query_params);
                if($c2_log_re['ret'] != 0 || !is_array($c2_log_re['data_info']) || empty($c2_log_re['data_info']))
                {
                    nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '查询c2任务注入日志失败,查询参数：'.var_export($log_query_params,true), ORG_ID);
                }
                $c2_id = array();
                foreach ($c2_log_re['data_info'] as $re_c2_log)
                {
                    $c2_id[] = $re_c2_log['nns_id'];
                }
                //修改c2日志为失败
                $c2_log_info = array(
                    'nns_notify_fail_reason' => '',
                    'nns_notify_result' => -1,
                    //'nns_notify_time' => date("Y-m-d H:i:s"),
                );
                $edit_re = nl_c2_log::edit($obj_dc, $c2_log_info, $c2_id);//修改C2日志
                if($edit_re['ret'] != 0)
                {
                    nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '修改c2任务注入日志失败,修改参数：' . var_export($c2_log_info,true), ORG_ID);
                }
            }

            $c2_edit_info = array(
                'nns_status' => $is_import ? 0 : -1,
            );
            $c2_re = nl_c2_task::edit($obj_dc, $c2_edit_info, $value['nns_id']);//修改C2任务

            if($c2_re['ret'] != 0)
            {
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '修改c2任务失败,修改参数：' . var_export($c2_edit_info,true), ORG_ID);
            }
            continue;
        }
        unset($notify_model);
        unset($obj_dc);
    }
}
$check_import_cdn = new check_import_cdn("check_import_cdn", ORG_ID,__FILE__);
$check_import_cdn->run();