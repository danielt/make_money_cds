<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
define('ORG_ID', 'twsx');
include_once dirname(dirname(dirname(__FILE__))) . '/' . ORG_ID . '/init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/models/queue_task_model.php';
include_once dirname(dirname(dirname(__FILE__))) . '/shanghai_mobile/models/child_c2_task_model.php';
include_once NPDIR . '/np_ftp.php';

class check_play extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        do_timer_action();
        $this->msg('执行结束...');
    }
}
$check_play = new check_play("check_play", ORG_ID);
$check_play->run();

function do_timer_action()
{
    $db = nn_get_db(NL_DB_WRITE);
    $sql = "SELECT * FROM `nns_mgtvbk_c2_log` WHERE `nns_org_id` = 'shanghai_mobile' AND `nns_task_type` = 'QueryPlay' AND `nns_action` = 'REGIST' AND `nns_result` = '[0]' AND `nns_notify_result` = '0' AND `nns_again` = '0' ORDER BY `nns_notify_time` desc ";
    $infos = nl_db_get_all($sql, $db);
    
    
    $queue_task_model = new queue_task_model();
    
    
    if (count($infos) == 0) {
        die ;
    }
    foreach ($infos as $info) {
        $nns_notify_result_url = $info['nns_notify_result_url'];
        $bool = true;
        $url_arr = parse_url($info['nns_notify_result_url']);
        $port = isset($url_arr["port"]) ? $url_arr["port"] : 21;
        $ftp1 = new nns_ftp($url_arr['host'], $url_arr["user"], $url_arr["pass"], $port);
        $ftpcon1 = $ftp1 -> connect();
        if (empty($ftpcon1)) {
            $bool = false;
        }
        $notify_save_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/mgtv/' . ORG_ID . '/notify' . $url_arr["path"];
        $path_parts = pathinfo($notify_save_path);
        if (!is_dir($path_parts['dirname'])) {
            mkdir($path_parts['dirname'], 0777, true);
        }
        if ($bool) {
            if (!file_exists($notify_save_path)) {
                $rs = $ftp1 -> get(ltrim($url_arr["path"], '/'), $notify_save_path);
                if ($rs == false) {
                    i_write_log_core('---------下载 notify 失败----------');
                    $bool = false;
                }
            }
        }
        if ($bool) {
    
            if (strlen(trim($notify_save_path)) > 0 && file_exists($notify_save_path)) {
                $xml_origin = file_get_contents($notify_save_path);
                $obj = simplexml_load_string($xml_origin);
                $xml_info = json_decode(json_encode($obj), true);
                $status = $xml_info['Objects']['Object']['Property'][0];
                $url = $xml_info['Objects']['Object']['Property'][1];
                i_write_log_core(var_export($url, true), 'test');
                i_write_log_core(var_export($xml_origin, true), 'test');
                if (strlen(trim($url)) > 0) {
                    if ($status == '0') {
                        $sql_update = "update nns_mgtvbk_c2_task set nns_status=0,nns_modify_time=now(),nns_epg_status=97,nns_ex_url='" . $url . "' where nns_id='" . $info['nns_task_id'] . "'";
                    } else {
                        $sql_update = "update nns_mgtvbk_c2_task set nns_status=9,nns_modify_time=now(),nns_epg_status=97,nns_ex_url='" . $url . "' where nns_id='" . $info['nns_task_id'] . "'";
    
                    }
                    nl_execute_by_db($sql_update, $db);
    
                    if ($status == '0') {
                        $queue_task_model->q_report_task($info['nns_task_id']);
                        nl_execute_by_db("update nns_mgtvbk_c2_log set nns_again='1' where nns_id='{$info['nns_id']}'", $db);
                    }
                }
            }
        }
    }
}

