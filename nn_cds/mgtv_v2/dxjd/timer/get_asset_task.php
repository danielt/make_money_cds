<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors',1);
set_time_limit(0); 
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_init.php';
define('ORG_ID', 'dxjd');
include_once  dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/init.php';
include_once dirname(dirname(dirname(__FILE__))) .'/models/queue_task_model.php';

class get_asset_task extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        do_timer_action();
        $this->msg('执行结束...');
    }
}
$get_asset_task = new get_asset_task("get_asset_task", ORG_ID);
$get_asset_task->run();

function do_timer_action()
{
    $time = time();
    
    define('Time', 15640000);
    
    $queue_task_model = new queue_task_model();
    $initime = intval($time-Time);
    $datetime = date("Y-m-d H:i:s",$initime);
    $db = nn_get_db(NL_DB_READ);
    $sql_asset = "select * from nns_vod where (nns_create_time >= '$datetime'  or nns_modify_time >= '$datetime') and nns_deleted!=1";
    $assets = nl_db_get_all($sql_asset, $db);
    if(is_array($assets)){
        foreach ($assets as $val) {
            $queue_task_model->q_add_task($val['nns_id'], BK_OP_VIDEO, BK_OP_ADD,ORG_ID,null,true);
        }
    }
    
    
    
    $sql_index = "select * from nns_vod_index where (nns_create_time >= '$datetime'  or nns_modify_time >= '$datetime') and nns_deleted!=1";
    $assets_index = nl_db_get_all($sql_index, $db);
    if(is_array($assets_index)){
        foreach ($assets_index as $val_index) {
            $queue_task_model->q_add_task($val_index['nns_id'], BK_OP_INDEX, BK_OP_ADD,ORG_ID,null,true);
        }
    }
    
    
    
    $sql_media = "select * from nns_vod_media where (nns_create_time >= '$datetime'  or nns_modify_time >= '$datetime') and nns_deleted!=1";
    $assets_media = nl_db_get_all($sql_media, $db);
    if(is_array($assets_media)){
        foreach ($assets_media as $val_media) {
            $queue_task_model->q_add_task($val_media['nns_id'], BK_OP_MEDIA, BK_OP_ADD,ORG_ID,null,true);
        }
    }
}