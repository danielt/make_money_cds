<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
define('ORG_ID', 'dxjd');
include_once dirname(dirname(dirname(__FILE__))) . '/' . ORG_ID . '/init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/models/queue_task_model.php';


class cdn_check_task extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        do_timer_action();
        $this->msg('执行结束...');
    }
}
$cdn_check_task = new cdn_check_task("cdn_check_task", ORG_ID);
$cdn_check_task->run();

function do_timer_action()
{
    $db = nn_get_db(NL_DB_WRITE);
    $sql = "select * from nns_mgtvbk_c2_task where nns_org_id='" . ORG_ID . "' and nns_type='media'  and nns_epg_status=97   and nns_action!='destroy'";
    $infos = nl_db_get_all($sql, $db);
    
    
    if ($infos!=false) {
    
        foreach ($infos as $info) {
            $vod_id = $info['nns_src_id'];
            $info_video = video_model::get_vod_index_info($vod_id);
    
            if($info_video==null){
                continue;
            }
    
            $id = np_guid_rand();
            $data = array(
                'nns_id' => $id,
                'nns_type' => 'index',
                'nns_name' => $info_video['nns_name'],
                'nns_ref_id' => $vod_id,
                'nns_action' => 'add',
                'nns_status' => 1,
                'nns_epg_status' => 97,
                'nns_create_time' => date('Y-m-d H:i:s'),
                'nns_org_id' => ORG_ID,
                'nns_src_id' => $info_video['nns_vod_id'],
            );
            nl_db_insert($db, 'nns_mgtvbk_c2_task', $data);
        }
    
    }
    $sql_index = "select * from nns_mgtvbk_c2_task where nns_org_id='" . ORG_ID . "' and nns_type='index' and nns_epg_status=97 and nns_action!='destroy'";
    
    $infoindexs = nl_db_get_all($sql_index, $db);
    
    if ($infoindexs!=false) {
        foreach($infoindexs as $infos){
            $vod_id = $infos['nns_src_id'];
            $info_video = video_model::get_vod_info($vod_id);
            //没有数据
            if($info_video==null){
                continue;
            }
            $id = np_guid_rand();
            $data = array(
                'nns_id'=>$id,
                'nns_type'=>'video',
                'nns_name'=>$info_video['nns_name'],
                'nns_ref_id'=>$vod_id,
                'nns_action'=>'add',
                'nns_status'=>1,
                'nns_epg_status'=>97,
                'nns_create_time'=>date('Y-m-d H:i:s'),
                'nns_org_id'=>ORG_ID,
            );
            nl_db_insert($db, 'nns_mgtvbk_c2_task', $data);
        }
    }
}