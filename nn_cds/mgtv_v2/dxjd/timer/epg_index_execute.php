<?php
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_init.php';
define('ORG_ID', 'dxjd');
include_once dirname(dirname(dirname(__FILE__))) . '/'.ORG_ID.'/init.php';

class epg_index_execute extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        do_timer_action();
        $this->msg('执行结束...');
    }
}
$epg_index_execute = new epg_index_execute("epg_index_execute", ORG_ID);
$epg_index_execute->run();

function do_timer_action()
{
    //从任务表取内容，调用C2执行
    i_echo('start');
    $sp_config = sp_model::get_sp_config(ORG_ID);
    if(isset($sp_config['disabled_epg'])&&(int)$sp_config['disabled_epg']===1){
        i_echo('close epg');
    }else{
        $queue_task_model = new queue_task_model();
        $sql = $queue_task_model->get_epg_sql(ORG_ID, 'index');
        echo $sql;
        if($sql!==null){
            $db = nn_get_db(NL_DB_READ);
            $c2_list = nl_db_get_all($sql, $db);
            if(is_array($c2_list)&&count($c2_list)>0){
                foreach($c2_list as $c2_info){
                    import_model::import_clip($c2_info['nns_id']);
                }
            }
        }
        i_echo('end');
    }
}