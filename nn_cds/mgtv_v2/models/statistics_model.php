<?php
class statistics_model {

	static public function get_statistics($sp_id, $params,$db) {
		set_time_limit(0);
		//$db = nn_get_db(NL_DB_READ);
		//切片失败
		$clip_arr = array();
		$clip_sql = "";
		$c2_sql = "";
		$c2_task_arr = array();
		if (!empty($params['begin'])) {
			$clip_arr[] = " nns_create_time>='" . $params['begin'] . "'";
			$c2_task_arr[] = " nns_modify_time>='" . $params['begin'] . "'";
		}

		if (!empty($params['end'])) {
			$clip_arr[] = " nns_create_time<='" . $params['end'] . "'";
			$c2_task_arr[] = " nns_modify_time<='" . $params['end'] . "'";
		}
		

		if (!empty($clip_arr))
			$clip_sql = " and " . implode(" and ", $clip_arr);

		$clip_fail = nl_db_get_col("select count(nns_id) as num from nns_mgtvbk_clip_task where nns_org_id='$sp_id' $clip_sql and nns_state in ('get_media_url_fail','download_fail','clip_fail')", $db);
		$clip_succ = nl_db_get_col("select count(nns_id) as num from nns_mgtvbk_clip_task where nns_org_id='$sp_id' $clip_sql and nns_state in ('c2_ok','ok','c2_handle','c2_fail')", $db);
		$clip_wait = nl_db_get_col("select count(nns_id) as num from nns_mgtvbk_clip_task where nns_org_id='$sp_id' $clip_sql and (ISNULL(nns_state) or nns_state='' or nns_state='add')", $db);
							
		//var_dump($clip_wait);			
		$arr_tmp = array();
		$arr_tmp['fail'] = (int)$clip_fail;
		$arr_tmp['ok'] = (int)$clip_succ;
		$arr_tmp['wait'] = (int)$clip_wait;
		$arr_tmp['do'] = 5;
		if($arr_tmp['do']>$arr_tmp['wait']){
			$arr_tmp['do'] = $arr_tmp['wait'];
		}
		unset($clip_succ, $clip_fail, $clip_arr);
		$type = array('video', 'index', 'media');
		$action = array('add', 'modify', 'destroy');
		$impory = array('cdn', 'epg');

		if (!empty($c2_task_arr))
			$c2_sql = " and " . implode(" and ", $c2_task_arr);

		foreach ($type as $value) {
			foreach ($action as $cvalue) {
				foreach ($impory as $ivalue) {
					if ($ivalue == 'cdn') {
						$sql_ok = "select count(nns_id) from nns_mgtvbk_c2_task  where nns_org_id='$sp_id' and nns_type='$value' and nns_action='$cvalue' $c2_sql and nns_status=0";
						$sql_fail = "select count(nns_id) from nns_mgtvbk_c2_task  where nns_org_id='$sp_id' and nns_type='$value' and nns_action='$cvalue' $c2_sql and nns_status=-1";
						$sql_wait = "select count(nns_id) from nns_mgtvbk_c2_task  where nns_org_id='$sp_id' and nns_type='$value' and nns_action='$cvalue' $c2_sql and nns_status=1";
						$sql_do = "select count(nns_id) from nns_mgtvbk_c2_task  where nns_org_id='$sp_id' and nns_type='$value' and nns_action='$cvalue' $c2_sql and nns_status=5";
					} elseif ($ivalue == 'epg') {
						$sql_ok = "select count(nns_id) from nns_mgtvbk_c2_task  where nns_org_id='$sp_id'  and nns_type='$value' and nns_action='$cvalue' $c2_sql and nns_epg_status=99";
						$sql_fail = "";
						$sql_wait = "select count(nns_id) from nns_mgtvbk_c2_task  where nns_org_id='$sp_id'  and nns_type='$value' and nns_action='$cvalue' $c2_sql and nns_epg_status=97";
						$sql_do = "";
					}
					
					//die($sql_ok);
					if ($sql_ok) {
						$re_ok = nl_db_get_col($sql_ok, $db);
						$arr_tmp[$ivalue.'_'.$value.'_'.$cvalue.'_ok'] = (int)$re_ok;
						unset($re_ok,$sql_ok);
					}
					if ($sql_fail) {
						$re_fail = nl_db_get_col($sql_fail, $db);
						$arr_tmp[$ivalue.'_'.$value.'_'.$cvalue.'_fail'] = (int)$re_fail;
						unset($re_fail,$sql_fail);
					} else {
						$arr_tmp[$ivalue.'_'.$value.'_'.$cvalue.'_fail'] = 0;
					}
					if ($sql_wait) {
						$re_wait = nl_db_get_col($sql_wait, $db);
						$arr_tmp[$ivalue.'_'.$value.'_'.$cvalue.'_wait'] = (int)$re_wait;
						unset($re_wait,$sql_wait);
					}
					if ($sql_do) {
						$re_do =  nl_db_get_col($sql_do, $db);
						$arr_tmp[$ivalue.'_'.$value.'_'.$cvalue.'_do'] = (int)$re_do;
						unset($re_do,$sql_do);
					} else {
						$arr_tmp[$ivalue.'_'.$value.'_'.$cvalue.'_do'] = 0;
			
					}
				}
			}
		}
		
		return $arr_tmp;
	}
	static public function get_statistics_num($sp_id, $params,$db) 
	{
		$sql_ok = "select count(nns_id) from nns_mgtvbk_c2_task  where 
		nns_org_id='$sp_id' and nns_action='add' and nns_epg_status=99";
		$num = nl_db_get_col($sql_ok, $db);
		return $num;
	}

}
?>