<?php
include_once dirname(dirname(__FILE__)) . '/import/import.class.php';
include_once dirname(__FILE__) . '/public_model_exec.class.php';
include_once dirname(__FILE__) . '/c2_task_model.php';
include_once dirname(__FILE__) . '/queue_task_model.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/c2_task/c2_task.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/video/vod.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/video/vod_index.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/vod_media/vod_media.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/live/live.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/live/live_index.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/live/live_media.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/playbill/playbill_item.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/file_package/file_package.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/sp/sp.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/depot/depot.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/video/vod_index_seekpoint.class.php';
class import_model_v2
{
    
    public $obj_dc = null;
	public $arr_sp_model = array();
	
	public $arr_error_data = array();
	
	public $arr_cp_config = null;
	
	public $arr_sp_config = null;
	
	public $error_data = null;
	
	public $query_base_data_type = array(
            	    'video',
            	    'index',
            	    'media',
	);
	public $str_date_time = null;
	
	public $arr_category = null;
	public $str_video_import_id = '';
	public $str_index_import_id = '';
	public $str_media_import_id = '';
	
	public function __construct()
	{
	    $this->obj_dc = nl_get_dc(array(
		    'db_policy' =>  NL_DB_READ | NL_DB_WRITE,
		    'cache_policy' => NP_KV_CACHE_TYPE_NULL
		   )
		);
	    $this->str_date_time = date("Y-m-d H:i:s");
	}
	
	/**
	 * 反馈错误信息
	 * @param unknown $data_info
	 */
	public function return_error_data()
	{
		$data = array(
				'ret'=>1,
				'reason'=>var_export($this->error_data,true),
		);
		return $data;
	}
	
	/**
	 * 反馈错误信息
	 * @param unknown $data_info
	 */
	public function return_data($ret,$reason,$data_info=null)
	{
	    $data = array(
	        'ret'=>$ret,
	        'reason'=>$reason,
	        'data_info'=>$data_info,
	    );
	    return $data;
	}
	
	/**
	 * 反馈正确信息
	 * @param unknown $data_info
	 * @return multitype:number unknown mixed
	 */
	public function return_right_data($data_info)
	{
		$data = array(
				'ret'=>0,
				'reason'=>'sucess',
				'data_info'=>$data_info
		);
		return $data;
	}

	/**
	 * @下发epg 
	 */
	public function import_epg($task_id)
	{
	    $task_id = is_array($task_id) ? $task_id : array($task_id);
		if (empty($task_id))
		{
		    nn_log::write_log_message(LOG_MODEL_CMS,"执行的所有错误结果汇总：task_id为空",'error_info');
			return $this->return_data(1, '$task_id为空');
		}
		$result_c2_task = nl_c2_task::query_data_by_id($this->obj_dc,$task_id);
		if($result_c2_task['ret'] !=0)
		{
		    nn_log::write_log_message(LOG_MODEL_CMS,"执行的所有错误结果汇总：" . var_export($result_c2_task,true),'error_info');
		    return $result_c2_task;
		}
		if(!isset($result_c2_task['data_info']) || !is_array($result_c2_task['data_info']))
		{
		    nn_log::write_log_message(LOG_MODEL_CMS,"执行的所有错误结果汇总：" . var_export($result_c2_task,true),'error_info');
		    return $result_c2_task;
		}
		$result_c2_task = $result_c2_task['data_info'];
		$arr_edit_data_error = $arr_sucsess_data = $next_ex_data = null;
		
		foreach ($result_c2_task as $c2_task_info)
		{
		    if(empty($c2_task_info['nns_ref_id']))
		    {
		        $this->arr_error_data[] = "task ID[{$c2_task_info['nns_id']}] nns_ref_id为空";
		        $result_ex = nl_c2_task::edit($this->obj_dc, array('nns_epg_status'=>100,'nns_modify_time'=>date("Y-m-d H:i:s")), $c2_task_info['nns_id']);
		        if($result_ex['ret']!=0)
		        {
		            $this->arr_error_data[] = "数据入库：".$result_ex['reason'];
		        }
		        $arr_edit_data_error[] = $c2_task_info['nns_id'];
		        continue;
		    }
		    $result_sp = $this->get_sp_info($c2_task_info['nns_org_id']);
		    if($result_sp['ret'] !=0)
		    {
		        $this->arr_error_data[] = "task ID[{$c2_task_info['nns_id']}]".$result_sp['reason'];
		        $arr_edit_data_error[] = $c2_task_info['nns_id'];
		        continue;
		    }
		    if(!in_array($c2_task_info['nns_type'], $this->query_base_data_type))
		    {
		        $this->arr_error_data[] = "task ID[{$c2_task_info['nns_id']}] 类型[{$c2_task_info['nns_type']}]没有在数组中:".var_export($this->query_base_data_type,true);
		        $arr_edit_data_error[] = $c2_task_info['nns_id'];
		        continue;
		    }
		    $str_func = "get_{$c2_task_info['nns_type']}_info";
		    $str_ex_func = "import_{$c2_task_info['nns_type']}";
		    $arr_function = get_class_methods($this);
		    $arr_function = is_array($arr_function) ? $arr_function : array();
		    if(!in_array($str_func, $arr_function))
		    {
		        $this->arr_error_data[] = "task ID[{$c2_task_info['nns_id']}] 类型[{$c2_task_info['nns_type']}] 查询基本数据方法[{$str_func}]没有在方法数组中:".var_export($arr_function,true);
		        $arr_edit_data_error[] = $c2_task_info['nns_id'];
		        continue;
		    }
		    if(!in_array($str_ex_func, $arr_function))
		    {
		        $this->arr_error_data[] = "task ID[{$c2_task_info['nns_id']}] 类型[{$c2_task_info['nns_type']}] 查询基本数据方法[{$str_ex_func}]没有在执行方法数组中:".var_export($arr_function,true);
		        $arr_edit_data_error[] = $c2_task_info['nns_id'];
		        continue;
		    }
		    $data_info = $this->$str_func($c2_task_info['nns_ref_id'],$c2_task_info);
		    if($data_info['ret'] !=0)
		    {
		        $this->arr_error_data[] = "task ID[{$c2_task_info['nns_id']}] 类型[{$c2_task_info['nns_type']}] 查询基本数据执行方法[{$str_func}]失败:".$data_info['reason'];
		        $arr_edit_data_error[] = $c2_task_info['nns_id'];
		        continue;
		    }
		    if(!isset($data_info['data_info']) || empty($data_info['data_info']) || !is_array($data_info['data_info']))
		    {
		        $result_ex = nl_c2_task::edit($this->obj_dc, array('nns_epg_status'=>100,'nns_modify_time'=>date("Y-m-d H:i:s")), $c2_task_info['nns_id']);
		        $this->arr_error_data[] = "task ID[{$c2_task_info['nns_id']}] 类型[{$c2_task_info['nns_type']}] nns_ref_id[{$c2_task_info['nns_ref_id']}]  查询数据不完整:".$data_info['reason']."数据入库：".$result_ex['reason'];
		        $arr_edit_data_error[] = $c2_task_info['nns_id'];
		        continue;
		    }

		    $data_info = $this->make_insert_data($data_info['data_info'], $c2_task_info['nns_type']);
		    if(!is_array($data_info))
		    {
		        $arr_edit_data_error[] = $c2_task_info['nns_id'];
		        continue;
		    }
		    foreach ($data_info as $data_info_val)
		    {
    		    $result = $this->$str_ex_func($data_info_val,$c2_task_info);
    		    if($result['ret'] !=0)
    		    {
    		        if($result['ret'] == '999')
    		        {
    		            $result_ex = nl_c2_task::edit($this->obj_dc, array('nns_epg_status'=>'100','nns_modify_time'=>date("Y-m-d H:i:s")), $c2_task_info['nns_id']);
    		        }
    		        else
    		        {
    		            $result_ex = nl_c2_task::edit($this->obj_dc, array('nns_epg_fail_time'=>$c2_task_info['nns_epg_fail_time']+1,'nns_modify_time'=>date("Y-m-d H:i:s")), $c2_task_info['nns_id']);
    		        }
    		        $this->arr_error_data[] = "task ID[{$c2_task_info['nns_id']}] 类型[{$c2_task_info['nns_type']}] 查询基本数据执行注入方法[{$str_ex_func}]失败:".$data_info['reason']."数据入库：".$result_ex['reason'];
    		    }
    		    else
    		    {
        		    $result_ex = nl_c2_task::edit($this->obj_dc, array('nns_epg_status'=>'99','nns_modify_time'=>date("Y-m-d H:i:s")), $c2_task_info['nns_id']);
        	        $this->arr_error_data[] = "task ID[{$c2_task_info['nns_id']}] 类型[{$c2_task_info['nns_type']}]执行注入方法[{$str_ex_func}]成功:".$data_info['reason']."数据入库：".$result_ex['reason'];
        	        if (!empty($c2_task_info['nns_op_id']))
        	        {
        	            $queue_task_model = new queue_task_model();
        	            $r = $queue_task_model->q_report_task($c2_task_info['nns_id']);
        	            $queue_task_model = null;
        	            $this->arr_error_data[] = "检查是否是最终状态执行结果:".var_export($r,true);
        	        }
        	        else
        	        {
        	            $this->arr_error_data[] = "nns_op_id为空,不在检查是否是最终状态";
        	        }
    		    }
		    }
		}
		if(is_array($arr_edit_data_error) && !empty($arr_edit_data_error))
		{
		    nn_log::write_log_message(LOG_MODEL_CMS,"-----收集continue循环信息-----",'error_info');
		    $result_ex = nl_c2_task::edit($this->obj_dc, array('nns_epg_fail_time'=>$c2_task_info['nns_epg_fail_time']+1,'nns_modify_time'=>date("Y-m-d H:i:s")),$arr_edit_data_error);
		    if($result_ex['ret'] !=0)
		    {
		          $this->arr_error_data[] = "task ID[".implode('],[', $arr_edit_data_error)."] 数据执行失败：".$result_ex['reason'];
		    }
		}
		if(!empty($this->arr_error_data))
		{
		     nn_log::write_log_message(LOG_MODEL_CMS,"执行的所有错误结果汇总：" . var_export($this->arr_error_data,true),'error_info');
		}
		return true;
	}
	
	/**
	 * 组装数据
	 * @param unknown $data_info
	 * @param unknown $type
	 */
	public function make_insert_data($data_info,$type)
	{
	    $last_data = null;
	    if(in_array($type, array('media')) && isset($data_info[$type]['info']['nns_ext_url']) && $this->is_json($data_info[$type]['info']['nns_ext_url']) 
	        && isset($this->arr_cp_config[$data_info[$type]['info']['nns_cp_id']]['nns_config']['original_cp_enabled']) 
	        && (int)$this->arr_cp_config[$data_info[$type]['info']['nns_cp_id']]['nns_config']['original_cp_enabled'] === 3)
	    {
	        $data_info[$type]['info']['bk_change_cp_id'] = $data_info[$type]['info']['nns_cp_id'];
	        $temp_data = $data_info;
	        $arr_ext_url = json_decode($data_info[$type]['info']['nns_ext_url'],true);
	        $i=0;
	        $arr_model = array(
	            'hd',
	            'stb',
	            '4k',
	            'low',
	            'sd'
	        );
	        foreach ($arr_ext_url as $ex_key=>$ex_val)
	        {
	            $i++;
	            if($i >=6)
	            {
	                break;
	            }
                $temp_data[$type]['info']['nns_ext_url'] = $ex_key.'|'.$ex_val;
                $temp_data[$type]['info']['nns_mode'] = array_shift($arr_model);
	            $temp_data[$type]['info']['bk_changed_import_id'] = md5($data_info[$type]['info']['bk_changed_import_id']."_{$ex_key}");
	            $temp_data[$type]['info']['nns_import_id'] = $temp_data[$type]['info']['bk_changed_import_id'];
                $temp_data[$type]['info']['bk_change_cp_id'] = $data_info[$type]['info']['nns_cp_id'];
	            $last_data[] = $temp_data;
	        }
	    }
	    else
	    {
	        $data_info[$type]['info']['bk_change_cp_id'] = $data_info[$type]['info']['nns_cp_id'];
	        $last_data[] = $data_info;
	    }
	    return $last_data;
	}
	
	
	
	/**
	 * 获取栏目列表
	 * @param unknown $video_type
	 */
	public function get_category($video_type)
	{
	    if(strlen($video_type) <1)
	    {
	        return $this->return_data(1, '$video_type为空');
	    }
	    if(isset($this->arr_category[$video_type]))
	    {
	        if(empty($this->arr_category[$video_type]))
	        {
	            return $this->return_data(1, '获取资源库栏目无数据');
	        }
	        return $this->return_data(0, 'ok');
	    }
	    $result_depot = nl_depot::get_depot_info($this->obj_dc,null,$video_type);
	    if($result_depot['ret'] !=0)
	    {
	        return $result_depot;
	    }
	    $result_depot = isset($result_depot['data_info'][0]['nns_category']) ? $result_depot['data_info'][0]['nns_category'] : '';
	    if(strlen($result_depot) <1)
	    {
	        $this->arr_category[$video_type] = null;
	        return $this->return_data(1, '获取资源库栏目无数据');
	    }
	    $dom = new DOMDocument('1.0', 'utf-8');
	    $dom->loadXML($result_depot);
	    $categorys = $dom->getElementsByTagName('category');
	    foreach ($categorys as $category)
	    {
	        $this->arr_category[$video_type][(string)$category->getAttribute('id')] = array(
	            'id'=>(string)$category->getAttribute('id'),
	            'name'=>$category->getAttribute('name'),
	            'parent_id'=>((string)$category->getAttribute('parent') == '0') ? 0 : (string)$category->getAttribute('parent'),
	        );
	    }
	    return $this->return_data(0, 'ok');
	}
	
	/**
	 * 是否是json字符串
	 * @param unknown $string
	 */
	public function is_json($string)
	{
	    if(!is_string($string))
	    {
	        return false;
	    }
	    $string = strlen($string) <1 ? '' : $string;
	    $string = trim($string);
	    if(strlen($string) <1)
	    {
	        return false;
	    }
	    json_decode($string);
	    return (json_last_error() == JSON_ERROR_NONE);
	}
	
	/**
	 * 获取CP信息
	 * @param unknown $cp_id
	 */
	public function get_cp_info($cp_id)
	{
	    if(strlen($cp_id) <1)
	    {
	        return $this->return_data(1, '$cp_id为空');
	    }
	    if(isset($this->arr_cp_config[$cp_id]))
	    {
	        return $this->return_data(0, 'ok');
	    }
	    $result_cp = nl_cp::query_by_id($this->obj_dc,$cp_id);
	    if($result_cp['ret'] !=0)
	    {
	        return $result_cp;
	    }
	    if(!isset($result_cp['data_info']) || !is_array($result_cp['data_info']) || empty($result_cp['data_info']))
	    {
	        $this->arr_cp_config[$cp_id] = null;
	        return $result_cp;
	    }
	    if($this->is_json($result_cp['data_info']['nns_config']))
	    {
	        $result_cp['data_info']['nns_config'] = json_decode($result_cp['data_info']['nns_config'],true);
	    }
	    $this->arr_cp_config[$cp_id]=$result_cp['data_info'];
	    return $result_cp;
	}
	
	/**
	 * 获取CP信息
	 * @param unknown $cp_id
	 */
	public function get_sp_info($sp_id)
	{
	    if(strlen($sp_id) <1)
	    {
	        return $this->return_data(1, '$sp_id为空');
	    }
	    if(isset($this->arr_sp_config[$sp_id]))
	    {
	        if(!isset($this->arr_sp_config[$sp_id]['nns_config']['epg_url']) || strlen($this->arr_sp_config[$sp_id]['nns_config']['epg_url']) <1)
	        {
	            return $this->return_data(1, 'EPG注入点播接口未配置');
	        }
	        return $this->return_data(0, 'ok');
	    }
	    $result_sp = nl_sp::query_by_id($this->obj_dc,$sp_id);
	    if($result_sp['ret'] !=0)
	    {
	        return $result_sp;
	    }
	    if(!isset($result_sp['data_info'][0]) || !is_array($result_sp['data_info'][0]) || empty($result_sp['data_info'][0]))
	    {
	        $this->arr_sp_config[$sp_id] = null;
	        return $result_sp;
	    }
	    if($this->is_json($result_sp['data_info'][0]['nns_config']))
	    {
	        $result_sp['data_info'][0]['nns_config'] = json_decode($result_sp['data_info'][0]['nns_config'],true);
	    }
	    $this->arr_sp_config[$sp_id]=$result_sp['data_info'][0];
	    if(!isset($this->arr_sp_config[$sp_id]['nns_config']['epg_url']) || strlen($this->arr_sp_config[$sp_id]['nns_config']['epg_url']) <1)
	    {
	        return $this->return_data(1, 'EPG注入点播接口未配置');
	    }
	    return $result_sp;
	}
	
	/**
	 * 获取主媒资信息
	 * @param unknown $video_id
	 */
	public function get_video_info($video_id,$c2_task_info,$is_need_ex_info=true)
	{
	    $result = null;
	    $result_cate = $this->get_category('video');
	    if($result_cate['ret'] !=0)
	    {
	        return $result_cate;
	    }
	    $result_vod = nl_vod::query_by_id($this->obj_dc, $video_id);
	    if($result_vod['ret'] !=0)
	    {
	        return $result_vod;
	    }
	    if (!isset($result_vod['data_info']) || !is_array($result_vod['data_info']) || empty($result_vod['data_info']))
	    {
	        return $this->return_data(0, '查询主媒资数据不存在：'.$result_vod['reason']);
	    }
	    $result_vod = $result_vod['data_info'];
	    $result_cp  = $this->get_cp_info($result_vod['nns_cp_id']);
	    if($result_cp['ret'] !=0)
	    {
	        return $result_cp;
	    }
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'video', $result_vod['nns_id'],$result_vod,$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config'],'epg');
	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        $result_video_import_info['ret'] = 1;
	        return $result_video_import_info;
	    }
	    $result_vod['bk_changed_import_id'] = $result_video_import_info['data_info'];
	    $result['video']['info'] = $result_vod;
	    if($is_need_ex_info)
	    {
    	    $result_vod_ex = nl_vod::query_ex_by_id($this->obj_dc, $result_vod['nns_asset_import_id'],$result_vod['nns_cp_id']);
    	    if($result_vod_ex['ret'] !=0)
    	    {
    	        return $result_vod_ex;
    	    }
    	    $arr_vod_ex = null;
    	    if(isset($result_vod_ex['data_info']) && !empty($result_vod_ex['data_info']) && is_array($result_vod_ex['data_info']))
    	    {
    	        foreach ($result_vod_ex['data_info'] as $ex_val)
    	        {
    	            $arr_vod_ex[$ex_val['nns_key']] = $ex_val['nns_value'];
    	        }
    	    }
    	    $result['video']['ex_info'] = $arr_vod_ex;
	    }
	    return $this->return_data(0,'ok',$result);
	}
	
	/**
	 * 获取分集、主媒资信息
	 * @param unknown $index_id
	 * @param string $is_need_parent
	 */
	public function get_index_info($index_id,$c2_task_info,$is_need_parent=true,$is_need_ex_info=true)
	{
	    $result = null;
	    $result_index = nl_vod_index::query_by_id($this->obj_dc, $index_id);
	    if($result_index['ret'] !=0)
	    {
	        return $result_index;
	    }
	    if (!isset($result_index['data_info']) || !is_array($result_index['data_info']) || empty($result_index['data_info']))
	    {
	        return $this->return_data(0, '查询分集数据不存在：'.$result_index['reason']);
	    }
	    $result_index = $result_index['data_info'];
	    $result_cp  = $this->get_cp_info($result_index['nns_cp_id']);
	    if($result_cp['ret'] !=0)
	    {
	        return $result_cp;
	    }
	    $result_index_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'index', $result_index['nns_id'],$result_index,$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config'],'epg');
	    if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
	    {
	        $result_index_import_info['ret'] = 1;
	        return $result_index_import_info;
	    }
	    $result_index['bk_changed_import_id'] = $result_index_import_info['data_info'];
	    $result['index']['info'] = $result_index;
	    if($is_need_ex_info)
	    {
    	    $result_index_ex = nl_vod_index::query_ex_by_id($this->obj_dc, $result_index['nns_import_id'],$result_index['nns_cp_id']);
    	    if($result_index_ex['ret'] !=0)
    	    {
    	        return $result_index_ex;
    	    }
    	    $arr_index_ex = null;
    	    if(isset($result_index_ex['data_info']) && !empty($result_index_ex['data_info']) && is_array($result_index_ex['data_info']))
    	    {
    	        foreach ($result_index_ex['data_info'] as $ex_val)
    	        {
    	            $arr_index_ex[$ex_val['nns_key']] = $ex_val['nns_value'];
    	        }
    	    }
    	    $result['index']['ex_info'] = $arr_index_ex;
	    }
	    if($is_need_parent)
	    {
	        $result_vod = $this->get_video_info($result_index['nns_vod_id'],$c2_task_info,false);
	        if($result_vod['ret'] !=0)
	        {
	            return $result_vod;
	        }
	        if(!isset($result_vod['data_info']['video']) || empty($result_vod['data_info']['video']) || !is_array($result_vod['data_info']['video']))
	        {
	            return $this->return_data(0, '查询主媒资数据不存在：'.$result_vod['reason']);
	        }
	        $result['video'] = $result_vod['data_info']['video'];
	    }
	    return $this->return_data(0,'ok',$result);
	}
	
	/**
	 * 获取片源信息
	 * @param unknown $media_id
	 * @param string $is_need_parent
	 */
	public function get_media_info($media_id,$c2_task_info,$is_need_parent=true,$is_need_ex_info=true)
	{
	    $result = null;
	    $result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $media_id);
	    if($result_media['ret'] !=0)
	    {
	        return $result_media;
	    }
	    if (!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
	    {
	        return $this->return_data(0, '查询片源数据不存在：'.$result_media['reason']);
	    }
	    $result_media = $result_media['data_info'];
	    $result_cp  = $this->get_cp_info($result_media['nns_cp_id']);
	    if($result_cp['ret'] !=0)
	    {
	        return $result_cp;
	    }
	    $result_media_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'media', $result_media['nns_id'],$result_media,$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config'],'epg');
	    if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
	    {
	        $result_media_import_info['ret'] = 1;
	        return $result_media_import_info;
	    }
	    $result_media['bk_changed_import_id'] = $result_media_import_info['data_info'];
	    $result['media']['info'] = $result_media;
	    if($is_need_ex_info)
	    {
    	    $result_media_ex = nl_vod_media_v2::query_ex_by_id($this->obj_dc, $result_media['nns_import_id'],$result_media['nns_cp_id']);
    	    if($result_media_ex['ret'] !=0)
    	    {
    	        return $result_media_ex;
    	    }
    	    $arr_media_ex = null;
    	    if(isset($result_media_ex['data_info']) && !empty($result_media_ex['data_info']) && is_array($result_media_ex['data_info']))
    	    {
    	        foreach ($result_media_ex['data_info'] as $ex_val)
    	        {
    	            $arr_media_ex[$ex_val['nns_key']] = $ex_val['nns_value'];
    	        }
    	    }
    	    $result['media']['ex_info'] = $arr_media_ex;
	    }
	    if($is_need_parent)
	    {
	        $result_index = $this->get_index_info($result_media['nns_vod_index_id'],$c2_task_info,true,false);
	        if($result_index['ret'] !=0)
	        {
	            return $result_index;
	        }
	        if(!isset($result_index['data_info']['index']) || empty($result_index['data_info']['index']) || !is_array($result_index['data_info']['index']))
	        {
	            return $this->return_data(0, '查询分集数据不存在：'.$result_index['reason']);
	        }
	        if(!isset($result_index['data_info']['video']) || empty($result_index['data_info']['video']) || !is_array($result_index['data_info']['video']))
	        {
	            return $this->return_data(0, '查询主媒资数据不存在：'.$result_index['reason']);
	        }
	        $result['video'] = $result_index['data_info']['video'];
	        $result['index'] = $result_index['data_info']['index'];
	    }
	    return $this->return_data(0,'ok',$result);
	}
	
	/**
	 * 获取直播频道信息
	 * @param unknown $video_id
	 */
	public function get_live_info($video_id,$c2_task_info,$is_need_ex_info=true)
	{
	    $result = null;
	    $result_cate = $this->get_category('live');
	    if($result_cate['ret'] !=0)
	    {
	        return $result_cate;
	    }
	    $result_live = nl_live::query_by_id($this->obj_dc, $video_id);
	    if($result_live['ret'] !=0)
	    {
	        return $result_live;
	    }
	    if (!isset($result_live['data_info']) || !is_array($result_live['data_info']) || empty($result_live['data_info']))
	    {
	        return $this->return_data(0, '查询直播频道数据不存在：'.$result_live['reason']);
	    }
	    $result_live = $result_live['data_info'];
	    $result_cp  = $this->get_cp_info($result_live['nns_cp_id']);
	    if($result_cp['ret'] !=0)
	    {
	        return $result_cp;
	    }
	    $result_live_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'live', $result_live['nns_id'],$result_live,$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config'],'epg');
	    if($result_live_import_info['ret'] !=0 || !isset($result_live_import_info['data_info']) || strlen($result_live_import_info['data_info']) <1)
	    {
	        $result_live_import_info['ret'] = 1;
	        return $result_live_import_info;
	    }
	    $result_live['nns_live_import_id'] = $result_live_import_info['data_info'];
	    $result['live']['info'] = $result_live;
	    if($is_need_ex_info)
	    {
	        $result_live_ex = nl_live::query_ex_by_id($this->obj_dc, $result_live['nns_id']);
	        if($result_live_ex['ret'] !=0)
	        {
	            return $result_live_ex;
	        }
	        $arr_live_ex = null;
	        if(isset($result_live_ex['data_info']) && !empty($result_live_ex['data_info']) && is_array($result_live_ex['data_info']))
	        {
	            foreach ($result_live_ex['data_info'] as $ex_val)
	            {
	                $arr_live_ex[$ex_val['nns_key']] = $ex_val['nns_value'];
	            }
	        }
	        $result['live']['ex_info'] = $arr_live_ex;
	    }
	    return $this->return_data(0,'ok',$result);
	}
	
	/**
	 * 获取直播频道分集、直播频道信息
	 * @param unknown $index_id
	 * @param string $is_need_parent
	 */
	public function get_live_index_info($index_id,$c2_task_info,$is_need_parent=true,$is_need_ex_info=true)
	{
	    $result = null;
	    $result_index = nl_live_index::query_by_id($this->obj_dc, $index_id);
	    if($result_index['ret'] !=0)
	    {
	        return $result_index;
	    }
	    if (!isset($result_index['data_info']) || !is_array($result_index['data_info']) || empty($result_index['data_info']))
	    {
	        return $this->return_data(0, '查询直播分集数据不存在：'.$result_index['reason']);
	    }
	    $result_index = $result_index['data_info'];
	    $result_cp  = $this->get_cp_info($result_index['nns_cp_id']);
	    if($result_cp['ret'] !=0)
	    {
	        return $result_cp;
	    }
	    $result_index_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'live_index', $result_index['nns_id'],$result_index,$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config'],'epg');
	    if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
	    {
	        $result_index_import_info['ret'] = 1;
	        return $result_index_import_info;
	    }
	    $result_index['nns_live_index_import_id'] = $result_index_import_info['data_info'];
	    $result['live_index']['info'] = $result_index;
	    $result['live_index']['ex_info'] = null;
	    #TODO  目前没有频道分集扩展信息
// 	    if($is_need_ex_info)
// 	    {
// 	        $result_index_ex = nl_live_index::query_ex_by_id($this->obj_dc, $result_index['nns_import_id'],$result_index['nns_cp_id']);
// 	        if($result_index_ex['ret'] !=0)
// 	        {
// 	            return $result_index_ex;
// 	        }
// 	        $arr_index_ex = null;
// 	        if(isset($result_index_ex['data_info']) && !empty($result_index_ex['data_info']) && is_array($result_index_ex['data_info']))
// 	        {
// 	            foreach ($result_index_ex['data_info'] as $ex_val)
// 	            {
// 	                $arr_index_ex[$ex_val['nns_key']] = $ex_val['nns_value'];
// 	            }
// 	        }
// 	        $result['index']['ex_info'] = $arr_index_ex;
// 	    }
	    if($is_need_parent)
	    {
	        $result_live = $this->get_live_info($result_index['nns_live_id'],$c2_task_info,true);
	        if($result_live['ret'] !=0)
	        {
	            return $result_live;
	        }
	        if(!isset($result_live['data_info']['live']) || empty($result_live['data_info']['live']) || !is_array($result_live['data_info']['live']))
	        {
	            return $this->return_data(0, '查询直播频道数据不存在：'.$result_live['reason']);
	        }
	        $result['live'] = $result_live['data_info']['live'];
	    }
	    return $this->return_data(0,'ok',$result);
	}
	
	
	/**
	 * 获取直播源信息
	 * @param unknown $media_id
	 * @param string $is_need_parent
	 */
	public function get_live_media_info($media_id,$c2_task_info,$is_need_parent=true,$is_need_ex_info=true)
	{
	    $result = null;
	    $result_media = nl_live_media::query_by_id($this->obj_dc, $media_id);
	    if($result_media['ret'] !=0)
	    {
	        return $result_media;
	    }
	    if (!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
	    {
	        return $this->return_data(0, '查询直播源数据不存在：'.$result_media['reason']);
	    }
	    $result_media = $result_media['data_info'];
	    
	    $result_cp  = $this->get_cp_info($result_media['nns_cp_id']);
	    if($result_cp['ret'] !=0)
	    {
	        return $result_cp;
	    }
	    $result_media_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'live_media', $result_media['nns_id'],$result_media,$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config'],'epg');
	    if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
	    {
	        $result_media_import_info['ret'] = 1;
	        return $result_media_import_info;
	    }
	    $result_media['nns_live_media_import_id'] = $result_media_import_info['data_info'];
	    $result['live_media']['info'] = $result_media;
	    #TODO 目前没有直播源的扩展信息
	    $result['live_media']['ex_info'] = null;
// 	    if($is_need_ex_info)
// 	    {
// 	        $result_media_ex = nl_vod_media_v2::query_ex_by_id($this->obj_dc, $result_media['nns_import_id'],$result_media['nns_cp_id']);
// 	        if($result_media_ex['ret'] !=0)
// 	        {
// 	            return $result_media_ex;
// 	        }
// 	        $arr_media_ex = null;
// 	        if(isset($result_media_ex['data_info']) && !empty($result_media_ex['data_info']) && is_array($result_media_ex['data_info']))
// 	        {
// 	            foreach ($result_media_ex['data_info'] as $ex_val)
// 	            {
// 	                $arr_media_ex[$ex_val['nns_key']] = $ex_val['nns_value'];
// 	            }
// 	        }
// 	        $result['media']['ex_info'] = $arr_media_ex;
// 	    }
	    if($is_need_parent)
	    {
	        $result_index = $this->get_live_index_info($result_media['nns_live_index_id'],$c2_task_info,true,false);
	        if($result_index['ret'] !=0)
	        {
	            return $result_index;
	        }
	        if(!isset($result_index['data_info']['live_index']) || empty($result_index['data_info']['live_index']) || !is_array($result_index['data_info']['live_index']))
	        {
	            return $this->return_data(0, '查询直播分集数据不存在：'.$result_index['reason']);
	        }
	        if(!isset($result_index['data_info']['live']) || empty($result_index['data_info']['live']) || !is_array($result_index['data_info']['live']))
	        {
	            return $this->return_data(0, '查询直播频道数据不存在：'.$result_index['reason']);
	        }
	        $result['live'] = $result_index['data_info']['live'];
	        $result['live_index'] = $result_index['data_info']['live_index'];
	    }
	    return $this->return_data(0,'ok',$result);
	}
	
	/**
	 * 获取节目单信息
	 * @param unknown $media_id
	 * @param string $is_need_parent
	 */
	public function get_playbill_info($media_id,$c2_task_info,$is_need_parent=true,$is_need_ex_info=true)
	{
	    $result = null;
	    $result_playbill = nl_playbill::query_by_id($this->obj_dc, $media_id);
	    if($result_playbill['ret'] !=0)
	    {
	        return $result_playbill;
	    }
	    if (!isset($result_playbill['data_info']) || !is_array($result_playbill['data_info']) || empty($result_playbill['data_info']))
	    {
	        return $this->return_data(0, '查询节目单数据不存在：'.$result_playbill['reason']);
	    }
	    $result_playbill = $result_playbill['data_info'];
	    $result_playbill['nns_deleted'] = $result_playbill['nns_state'];
	    $result_cp  = $this->get_cp_info($result_playbill['nns_cp_id']);
	    if($result_cp['ret'] !=0)
	    {
	        return $result_cp;
	    }
	    $result_playbill_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'playbill', $result_playbill['nns_id'],$result_playbill,$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config'],'epg');
	    if($result_playbill_import_info['ret'] !=0 || !isset($result_playbill_import_info['data_info']) || strlen($result_playbill_import_info['data_info']) <1)
	    {
	        $result_playbill_import_info['ret'] = 1;
	        return $result_playbill_import_info;
	    }
	    $result_playbill['nns_playbill_import_id'] = $result_playbill_import_info['data_info'];
	    $result['playbill']['info'] = $result_playbill;
	    #TODO 目前没有节目单的扩展信息
	    $result['playbill']['ex_info'] = null;
// 	    if($is_need_ex_info)
// 	    {
// 	        $result_media_ex = nl_vod_media_v2::query_ex_by_id($this->obj_dc, $result_media['nns_import_id'],$result_media['nns_cp_id']);
// 	        if($result_media_ex['ret'] !=0)
// 	        {
// 	            return $result_media_ex;
// 	        }
// 	        $arr_media_ex = null;
// 	        if(isset($result_media_ex['data_info']) && !empty($result_media_ex['data_info']) && is_array($result_media_ex['data_info']))
// 	        {
// 	            foreach ($result_media_ex['data_info'] as $ex_val)
// 	            {
// 	                $arr_media_ex[$ex_val['nns_key']] = $ex_val['nns_value'];
// 	            }
// 	        }
// 	        $result['media']['ex_info'] = $arr_media_ex;
// 	    }
	    if($is_need_parent)
	    {
	        $result_media = $this->get_live_media_info($result_playbill['nns_live_media_id'],$c2_task_info,false,false);
	        if($result_media['ret'] !=0)
	        {
	            return $result_media;
	        }
	        if(!isset($result_media['data_info']['live_media']) || empty($result_media['data_info']['live_media']) || !is_array($result_media['data_info']['live_media']))
	        {
	            return $this->return_data(0, '查询直播源数据不存在：'.$result_media['reason']);
	        }
	        if(!isset($result_media['data_info']['live_index']) || empty($result_media['data_info']['live_index']) || !is_array($result_media['data_info']['live_index']))
	        {
	            return $this->return_data(0, '查询直播分集数据不存在：'.$result_media['reason']);
	        }
	        if(!isset($result_media['data_info']['live']) || empty($result_media['data_info']['live']) || !is_array($result_media['data_info']['live']))
	        {
	            return $this->return_data(0, '查询主媒资数据不存在：'.$result_media['reason']);
	        }
	        $result['live_media'] = $result_media['data_info']['live_media'];
	        $result['live_index'] = $result_media['data_info']['live_index'];
	        $result['live'] = $result_media['data_info']['live'];
	    }
	    return $this->return_data(0,'ok',$result);
	}
	/**
	 * 组装XMl
	 * @param unknown $obj_dom
	 * @param unknown $key
	 * @param string $val
	 * @param string $arr_attr
	 * @param string $parent
	 */
	public function make_xml($obj_dom,$key, $val = null, $arr_attr = null,$parent=null)
	{
		if(is_null($parent))
		{
			$parent=$obj_dom;
		}
		$$key = isset($val) ? $obj_dom->createElement($key, $val) : $obj_dom->createElement($key);
		if (!empty($arr_attr) && is_array($arr_attr))
		{
			foreach ($arr_attr as $attr_key => $attr_val)
			{
				$domAttribute = $obj_dom->createAttribute($attr_key);
				$domAttribute->value = $attr_val;
				$$key->appendChild($domAttribute);
				$obj_dom->appendChild($$key);
			}
		}
		$parent->appendChild($$key);
		unset($parent);
		return $$key;
	}
	
	/**
	 * 注入主媒资
	 * @param unknown $data_info
	 * @param unknown $c2_task_info
	 * @return Ambigous|multitype:unknown unknown
	 */
	public function import_video($data_info,$c2_task_info)
	{
	    $import_obj = new import($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['epg_url'], dirname(dirname(dirname(__FILE__))).'/data/log/import/'.$c2_task_info['nns_org_id'],'',$data_info['video']['info']['nns_cp_id']);
	    if ($data_info['video']['info']['nns_deleted'] == 1 && $c2_task_info['nns_action'] !='destroy')
	    {
	        $c2_task_info['nns_action'] = 'destroy';
	        $result_c2_ex = nl_c2_task::edit($this->obj_dc, array('nns_action'=>'destroy','nns_epg_status'=>'97'), $c2_task_info['nns_id']);
	        if($result_c2_ex['ret'] !=0)
	        {
	            return $result_c2_ex;
	        }
	    }
	    $info = $data_info['video']['info'];
	    //单剧集注入
        if($info['nns_all_index'] >= 2 && (int)$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['epg_single_index_import_model'] === 1)
        {
            return $this->return_data(0, 'success');
        }
	    if(isset($this->arr_cp_config[$info['nns_cp_id']]['nns_config']['original_cp_enabled']) && (int)$this->arr_cp_config[$info['nns_cp_id']]['nns_config']['original_cp_enabled'] === 1)
	    {
	        $new_cp_id = $import_obj->cp_id = $data_info['video']['info']['nns_producer'];
	    }
	    else if(isset($this->arr_cp_config[$info['nns_cp_id']]['nns_config']['original_cp_enabled']) && (int)$this->arr_cp_config[$info['nns_cp_id']]['nns_config']['original_cp_enabled'] === 2)
	    {
	        $new_cp_id = $import_obj->cp_id = 0;
	    }
	    else if(isset($this->arr_cp_config[$info['nns_cp_id']]['nns_config']['original_cp_enabled']) && (int)$this->arr_cp_config[$info['nns_cp_id']]['nns_config']['original_cp_enabled'] === 3)
	    {
	        $new_cp_id = $import_obj->cp_id = $info['bk_change_cp_id'];
	    }
        else
        {
            $new_cp_id = $info['nns_cp_id'];
        }
	    nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG主媒资注入开始==============================",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	    nn_log::write_log_message(LOG_MODEL_CMS,"task_id[{$c2_task_info['nns_id']}]注入行为：".$c2_task_info['nns_action']."注入epg地址:[{$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['epg_url']}]",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);

        $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'video', $info['nns_id'],$info,$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config'],'epg');
        if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
        {
            return $this->return_data(1, '注入EPG的import_id获取失败');
        }
        //单剧集注入
        $this->str_video_import_id = $result_video_import_info['data_info'];
	    //媒资注入
	    if ($c2_task_info['nns_action'] == 'destroy')
	    {
	        $result = $import_obj->delete_assets($this->str_video_import_id,0,$info['nns_cp_id'],(isset($c2_task_info['nns_message_id']) && strlen($c2_task_info['nns_message_id']) >0) ? $c2_task_info['nns_message_id'] : '');
	        $xml = "";
	    }
	    else
	    {
	        //容错，保证横竖方图每一张都有
	        if(empty($info['nns_image2']) && !empty($info['nns_image_v']))
	        {
	            $info['nns_image2'] = $info['nns_image_v'];
	        }
	        if(empty($info['nns_image1']) && !empty($info['nns_image_v']))
	        {
	            $info['nns_image1'] = $info['nns_image_v'];
	        }
	        if(!empty($info['nns_image2']) && empty($info['nns_image_v']))
	        {
	            $info['nns_image_v'] = $info['nns_image2'];
	        }
	        if(empty($info['nns_image_h']))
	        {
	            $info['nns_image_h'] = $info['nns_image_v'];
	        }
	        if(empty($info['nns_image_s']))
	        {
	            $info['nns_image_s'] = $info['nns_image_v'];
	        }
	        $info['nns_summary'] = str_replace("》", '',str_replace("《", '',str_replace("&", '',str_replace('"', '', $info['nns_summary']))));
	        $category_parent_id = isset($this->arr_category['video'][$info['nns_category_id']]['parent_id']) ? $this->arr_category['video'][$info['nns_category_id']]['parent_id'] : 0;
	        $parent_assets_category = isset($this->arr_category['video'][$category_parent_id]['name']) ? $this->arr_category['video'][$category_parent_id]['name'] : '';
	        $arr_attr = array(
	            'assets_id'=>$this->str_video_import_id,
	            'parent_assets_category'=>$parent_assets_category,
	            'parent_assets_category_id'=>$category_parent_id,
	            'assets_category'=>isset($this->arr_category['video'][$info['nns_category_id']]['name']) ? $this->arr_category['video'][$info['nns_category_id']]['name'] : '',
	            'video_type'=>'0',
	            'name'=>htmlspecialchars($info['nns_name'], ENT_QUOTES),
	            'sreach_key'=>htmlspecialchars($info['nns_keyword'], ENT_QUOTES),
	            'view_type'=>$info['nns_view_type'],
	            'director'=>htmlspecialchars($info['nns_director'], ENT_QUOTES),
	            'player'=>htmlspecialchars($info['nns_actor'], ENT_QUOTES),
	            'tag'=>htmlspecialchars($info['nns_kind'], ENT_QUOTES),
	            'Tags'=>$info['nns_tag'],
	            'region'=>htmlspecialchars($info['nns_area'], ENT_QUOTES),
	            'release_time'=>$info['nns_show_time'],
	            'totalnum'=>$info['nns_all_index'],
	            'smallpic'=>str_replace("JPG", "jpg", $info['nns_image2']),
	            'midpic'=>str_replace("JPG", "jpg", $info['nns_image1']),
	            'bigpic'=>str_replace("JPG", "jpg", $info['nns_image0']),
	            'verticality_img'=>str_replace("JPG", "jpg", $info['nns_image_v']),
	            'horizontal_img'=>str_replace("JPG", "jpg", $info['nns_image_h']),
	            'square_img'=>str_replace("JPG", "jpg", $info['nns_image_s']),
	            'intro'=>htmlspecialchars($info['nns_summary'], ENT_QUOTES),
	            'producer'=>htmlspecialchars($info['nns_producer'], ENT_QUOTES),
	            'play_role'=>htmlspecialchars($info['nns_play_role']),
	            'subordinate_name'=>htmlspecialchars($info['nns_alias_name'], ENT_QUOTES),
	            'english_name'=>htmlspecialchars(str_replace(",", "/", $info['nns_eng_name']), ENT_QUOTES),
	            'language'=>htmlspecialchars(str_replace(",", "/", $info['nns_language']), ENT_QUOTES),
	            'caption_language'=>htmlspecialchars(str_replace(",", "/", $info['nns_text_lang']), ENT_QUOTES),
	            'copyright_range'=>str_replace(",", "/", $info['nns_remark']),
	            'copyright'=>str_replace(",", "/", $info['nns_copyright']),
	            'total_clicks'=>$info['nns_play_count'],
	            'view_len'=>$info['nns_view_len'],
	            'asset_source'=>$new_cp_id,
	            'day_clicks'=>0,
	            'original_id'=>$info['nns_asset_import_id'],
	        );
	        if(isset($info['bk_re_import_cp_id']) && strlen($info['bk_re_import_cp_id'])>0 && isset($info['bk_re_import_id']) && strlen($info['bk_re_import_id'])>0 )
	        {
	            $arr_attr['cite_id'] = $info['bk_re_import_id'];
	            $arr_attr['cite_cp_id'] = $info['bk_re_import_cp_id'];
	        }
	        if(is_array($data_info['video']['ex_info']))
	        {
	            foreach ($data_info['video']['ex_info'] as $ex_key=>$ex_val)
	            {
	                if(isset($arr_attr[$ex_key]))
	                {
	                    continue;
	                }
	                $arr_attr[$ex_key] = htmlspecialchars($ex_val, ENT_QUOTES);
	            }
	        }
	        $obj_dom = new DOMDocument('1.0', 'utf-8');
	        $this->make_xml($obj_dom,'video',null,$arr_attr);
	        $xml = $obj_dom->saveXML();
	        $data['func'] = 'modify_video';
	        $data['assets_content']     = $xml;
	        $data['assets_video_info']     = $xml;
	        $data['asset_source_id'] =$this->str_video_import_id;
	        $data['assets_id'] =$this->str_video_import_id;
	        nn_log::write_log_message(LOG_MODEL_CMS,'[EPG主媒资注入参数：]'."\n".var_export($data,true),$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	        unset($data);
	        $result = $import_obj->import_assets($xml, $this->str_video_import_id,$info['nns_cp_id'],(isset($c2_task_info['nns_message_id']) && strlen($c2_task_info['nns_message_id']) >0) ? $c2_task_info['nns_message_id'] : '');
	    }
	    $re = json_decode($result, true);
	    nn_log::write_log_message(LOG_MODEL_CMS,'[注入结果：]'."\n".var_export($re,true),$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	    nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG主媒资注入结束==============================",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	    
	    $log_data = array (
	        'nns_id' => np_guid_rand('vod'),
	        'nns_video_id' => $info['nns_id'],
	        'nns_video_type' => 'vod',
	        'nns_org_id' => $c2_task_info['nns_org_id'],
	        'nns_create_time' => date('Y-m-d H:i:s'),
	        'nns_action' => $c2_task_info['nns_action'],
	        'nns_status' => $re['ret'],
	        'nns_reason' => addslashes($re['reason']),
	        'nns_data' => $xml,
	        'nns_name' => $c2_task_info['nns_name']
	    );
	    nl_db_insert($this->obj_dc->db(), 'nns_mgtvbk_import_epg_log', $log_data);
	    if ($re['ret'] == '0' || (int)$re['ret'] === 7)
	    {
	        return $this->return_data(0, var_export($re,true));
	    }
	    else
	    {
	        return $this->return_data(1, var_export($re,true));
	    }
	}
	
	/**
	 * 注入分集
	 * @param unknown $data_info
	 * @param unknown $c2_task_info
	 */
	public function import_index($data_info,$c2_task_info)
	{
	    $import_obj = new import($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['epg_url'], dirname(dirname(dirname(__FILE__))).'/data/log/import/'.$c2_task_info['nns_org_id'],'',$data_info['video']['info']['nns_cp_id']);
	    if ($data_info['index']['info']['nns_deleted'] == 1 && $c2_task_info['nns_action'] !='destroy')
	    {
	        $c2_task_info['nns_action'] = 'destroy';
	        $result_c2_ex = nl_c2_task::edit($this->obj_dc, array('nns_action'=>'destroy','nns_epg_status'=>'97'), $c2_task_info['nns_id']);
	        if($result_c2_ex['ret'] !=0)
	        {
	            return $result_c2_ex;
	        }
	    }
	    $info = $data_info['index']['info'];

		if (isset($this->arr_cp_config[$info['nns_cp_id']]['nns_config']['original_cp_enabled']) && (int)$this->arr_cp_config[$info['nns_cp_id']]['nns_config']['original_cp_enabled'] === 1)
		{
			$new_cp_id = $import_obj->cp_id = $this->arr_cp_config[$info['nns_cp_id']]['nns_config']['nns_producer'];
		}
		else if (isset($this->arr_cp_config[$info['nns_cp_id']]['nns_config']['original_cp_enabled']) && (int)$this->arr_cp_config[$info['nns_cp_id']]['nns_config']['original_cp_enabled'] === 2)
		{
			$new_cp_id = $import_obj->cp_id = 0;
		}
	    else if(isset($this->arr_cp_config[$info['nns_cp_id']]['nns_config']['original_cp_enabled']) && (int)$this->arr_cp_config[$info['nns_cp_id']]['nns_config']['original_cp_enabled'] === 3)
	    {
	        $new_cp_id = $import_obj->cp_id = $info['bk_change_cp_id'];
	    }
	    else
	    {
	        $new_cp_id = $info['nns_cp_id'];
	    }
		nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG分集注入开始==============================",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
		nn_log::write_log_message(LOG_MODEL_CMS,"task_id[{$c2_task_info['nns_id']}]注入行为：".$c2_task_info['nns_action']."注入epg地址:[{$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['epg_url']}]",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
        //video
		$result_video_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'video', $data_info['video']['info']['nns_id'],$data_info['video']['info'],$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config'],'epg');
        if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
        {
            return $this->return_data(1, '注入EPG的主媒资import_id获取失败');
        }
		//单剧集注入
        if($data_info['video']['info']['nns_all_index'] >= 2 && (int)$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['epg_single_index_import_model'] === 1)
        {
            $this->str_video_import_id = '';
        }
        else
        {
            $this->str_video_import_id = $result_video_import_info['data_info'];
        }
        //index
        $result_index_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'index', $info['nns_id'],$info,$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config'],'epg');
        if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
        {
            return $this->return_data(1, '注入EPG的分集import_id获取失败');
        }
        $this->str_index_import_id = $result_index_import_info['data_info'];
		if ($c2_task_info['nns_action'] == 'destroy')
		{
			if(!empty($info['nns_vod_id']) && $info['nns_index'] !='')
			{
    			$result_seekpoint = nl_vod_index_seekpoint::delete_by_vod_index($this->obj_dc,$info['nns_vod_id'],$info['nns_index']);
    	        if($result_seekpoint['ret']!=0)
    	        {
    	            return $result_seekpoint;
    	        }
			}
			if(!$this->arr_cp_config[$info['nns_cp_id']]['nns_config']['check_metadata_enabled'])//下发注入的随机ID
			{
				$result = $import_obj->delete_video_clip($this->str_index_import_id,0,$info['nns_import_source'],$info['nns_cp_id'],(isset($c2_task_info['nns_message_id']) && strlen($c2_task_info['nns_message_id']) >0) ? $c2_task_info['nns_message_id'] : '');
			}
			else 
			{
				$result = $import_obj->delete_video_clip($this->str_index_import_id,0,null,$info['nns_cp_id'],(isset($c2_task_info['nns_message_id']) && strlen($c2_task_info['nns_message_id']) >0) ? $c2_task_info['nns_message_id'] : '');
			}
		}
		else
		{
		    $seekpoint_xml = null;
		    $result_seekpoint = nl_vod_index_seekpoint::query_by_vod_index($this->obj_dc,$info['nns_vod_id'],$info['nns_index']);
		    if($result_seekpoint['ret']!=0)
		    {
		        return $result_seekpoint;
		    }
		    if(isset($result_seekpoint['data_info']) && is_array($result_seekpoint['data_info']) && !empty($result_seekpoint['data_info']))
		    {
		        $obj_dom = new DOMDocument('1.0', 'utf-8');
		        $obj_metadata = $this->make_xml($obj_dom,'metadata');
		        foreach ($result_seekpoint['data_info'] as $seekpoint_val)
		        {
		            $arr_seekpoint = array(
		                'begin'=>$seekpoint_val['nns_begin'],
		                'end'=>$seekpoint_val['nns_end'],
		                'type'=>$seekpoint_val['nns_type'],
		                'name'=>htmlspecialchars($info['nns_name'], ENT_QUOTES),
		                'img'=>$seekpoint_val['nns_image'],
		            );
		            $this->make_xml($obj_dom, 'item',null,$arr_seekpoint,$obj_metadata);
		        }
		        $seekpoint_xml = $obj_dom->saveXML();
		    }
		    $info['nns_summary'] = str_replace("》", '',str_replace("《", '',str_replace("&", '',str_replace('"', '', $info['nns_summary']))));
		    $arr_attr = array(
		        'clip_id'=>$this->str_index_import_id,
		        'name'=>htmlspecialchars($info['nns_name'], ENT_QUOTES),
		        'index'=>$info['nns_index'],
		        'time_len'=>$info['nns_time_len'],
		        'summary'=>htmlspecialchars($info['nns_summary'], ENT_QUOTES),
		        'pic'=>str_replace("JPG", "jpg", $info['nns_image']),
		        'director'=>htmlspecialchars($info['nns_director'], ENT_QUOTES),
		        'player'=>htmlspecialchars(str_replace("&", '', $info['nns_actor']), ENT_QUOTES),
		        'total_clicks'=>$info['nns_play_count'],
		        'release_time'=>$info['nns_release_time'],
		        'update_time'=>$info['nns_update_time'],
		        'asset_source'=>$new_cp_id,
		        'month_clicks'=>'',
		        'week_clicks'=>'',
		        'day_clicks'=>'',
		        'original_id'=>$info['nns_import_id'],
		    );
		    if(!$this->arr_cp_config[$info['nns_cp_id']]['nns_config']['check_metadata_enabled'])//下发注入的随机ID
		    {
		        $arr_attr['import_source']=$info['nns_import_source'];
		    }
		    if(is_array($data_info['index']['ex_info']))
		    {
		        foreach ($data_info['index']['ex_info'] as $ex_key=>$ex_val)
		        {
		            if(isset($arr_attr[$ex_key]))
		            {
		                continue;
		            }
		            $arr_attr[$ex_key] = htmlspecialchars($ex_val, ENT_QUOTES);
		        }
		    }
		    $obj_dom = new DOMDocument('1.0', 'utf-8');
		    $this->make_xml($obj_dom,'index',null,$arr_attr);
		    $xml = $obj_dom->saveXML();
			$data['func'] 				= 'import_video_clip';
			$data['assets_id'] 			= $this->str_video_import_id;
			$data['assets_video_type']  = 0;
			$data['assets_clip_id']    =  $this->str_index_import_id;
			$data['assets_clip_info']   = $xml;
			nn_log::write_log_message(LOG_MODEL_CMS,'[EPG分集注入参数：]'."\n".var_export($data,true),$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	        unset($data);
			$result = $import_obj->import_video_clip($this->str_video_import_id, $this->str_index_import_id, $xml,0,$seekpoint_xml,$info['nns_cp_id'],(isset($c2_task_info['nns_message_id']) && strlen($c2_task_info['nns_message_id']) >0) ? $c2_task_info['nns_message_id'] : '');
		}
		$re = json_decode($result,true);
		nn_log::write_log_message(LOG_MODEL_CMS,'[注入结果：]'."\n".var_export($re,true),$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	    nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG主媒资注入结束==============================",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	    
	    $log_data = array (
	        'nns_id' => np_guid_rand('index'),
	        'nns_video_id' => $info['nns_id'],
	        'nns_video_type' => 'index',
	        'nns_org_id' => $c2_task_info['nns_org_id'],
	        'nns_create_time' => date('Y-m-d H:i:s'),
	        'nns_action' => $c2_task_info['nns_action'],
	        'nns_status' => $re['ret'],
	        'nns_reason' => addslashes($re['reason']),
	        'nns_data' => $xml,
	        'nns_name' => $c2_task_info['nns_name']
	    );
	    nl_db_insert($this->obj_dc->db(), 'nns_mgtvbk_import_epg_log', $log_data);
	    if ($re['ret'] == '0' || (int)$re['ret'] === 7)
	    {
	        return $this->return_data(0, var_export($re,true));
	    }
	    else
	    {
	        return $this->return_data(1, var_export($re,true));
	    }
	}
	
	/**
	 * 注入片源
	 * @param unknown $data_info
	 * @param unknown $c2_task_info
	 */
	public function import_media($data_info,$c2_task_info)
	{
	    $import_obj = new import($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['epg_url'], dirname(dirname(dirname(__FILE__))).'/data/log/import/'.$c2_task_info['nns_org_id'],'',$data_info['video']['info']['nns_cp_id']);
	    if ($data_info['media']['info']['nns_deleted'] == 1 && $c2_task_info['nns_action'] !='destroy')
	    {
	        $c2_task_info['nns_action'] = 'destroy';
	        $result_c2_ex = nl_c2_task::edit($this->obj_dc, array('nns_action'=>'destroy','nns_epg_status'=>'97'), $c2_task_info['nns_id']);
	        if($result_c2_ex['ret'] !=0)
	        {
	            return $result_c2_ex;
	        }
	    }
	    $info = $data_info['media']['info'];

	    //定制化贵州同洲逻辑
	    if($c2_task_info['nns_org_id'] == 'gzgd_tz_sp' && $c2_task_info['nns_action'] != 'destroy')
	    {
	        nn_log::write_log_message(LOG_MODEL_CMS,"========同洲定制化检查开始=========检查片源ID为:".$c2_task_info['nns_ref_id'],$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	        //查询其他CP的片源是否注入EPG
	        //step one 获取其他cp的片源GUID
	        $media_sql = "select nns_id from nns_vod_media where nns_import_id='{$info['nns_import_id']}' and nns_cp_id!='{$info['nns_cp_id']}'";
	        nn_log::write_log_message(LOG_MODEL_CMS,"获取其他CP的片源SQL为:" . $media_sql,$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	        $media_id_info = nl_query_by_db($media_sql, $this->obj_dc->db());
	        if(is_array($media_id_info))
	        {
	            $ids_info = array();
	            foreach ($media_id_info as $ids_i)
	            {
	                $ids_info[] = $ids_i['nns_id'];
	            }
	            if(count($ids_info) > 1)
	            {
	                $ids_info = implode("','", $ids_info);
	            }
	            else
	            {
	                $ids_info = $ids_info[0];
	            }
	            //step two 查询C2中是否存在
	            $check_c2_sql = "select nns_epg_status,nns_file_path,nns_cdn_policy from nns_mgtvbk_c2_task where nns_type='media' and nns_ref_id in ('{$ids_info}')";
	            nn_log::write_log_message(LOG_MODEL_CMS,"获取C2中是否存在的SQL为:" . $check_c2_sql,$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	            $other_c2 = nl_db_get_one($check_c2_sql, $this->obj_dc->db());
	            if(!is_array($other_c2))//C2中不存在
	            {
	                nn_log::write_log_message(LOG_MODEL_CMS,"C2中没有数据",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	                //step three c2中没有数据，到同步指令中去查
	                $check_sync_sql = "select nns_id from nns_mgtvbk_op_queue where nns_media_id in ('{$ids_info}') and nns_type='media'";
	                nn_log::write_log_message(LOG_MODEL_CMS,"中心同步指令中是否存在的查询SQL为:".$check_sync_sql,$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	                $check_sync = nl_db_get_one($check_sync_sql, $this->obj_dc->db());
	                if(is_array($check_sync))
	                {
	                    nn_log::write_log_message(LOG_MODEL_CMS,"中心同步指令中有数据".var_export($check_sync,true),$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	                    return false;
	                }
	                else
	                {
	                    //step four 中心同步指令中没有，检查中心注入指令
	                    $check_op_sql = "select * from nns_mgtvbk_import_op where nns_video_id in ('{$ids_info}') and nns_video_type='media'";
	                    nn_log::write_log_message(LOG_MODEL_CMS,"中心注入指令令中是否存在的查询SQL为:".$check_op_sql,$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	                    $check_op = nl_db_get_one($check_op_sql, $this->obj_dc->db());
	                    if(is_array($check_op))
	                    {
	                        nn_log::write_log_message(LOG_MODEL_CMS,"中心同步指令中有数据".var_export($check_op,true),$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	                        return false;
	                    }
	                    nn_log::write_log_message(LOG_MODEL_CMS,"中心同步指令中没有数据,则是存量数据可直接注入EPG",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	                }
	            }
	            elseif(is_array($other_c2) && $other_c2['nns_epg_status'] != '99')//C2中存在且则EPG还未注入成功
	            {
	                nn_log::write_log_message(LOG_MODEL_CMS,"C2中有数据，但还未注入EPG",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	                return false;
	            }
	            elseif (is_array($other_c2) && $other_c2['nns_epg_status'] == '99')//EPG注入成功,则同洲的下发
	            {
	                $c2_task_info['nns_file_path'] = $other_c2['nns_file_path'];
	                $c2_task_info['nns_cdn_policy'] = $other_c2['nns_cdn_policy'];
	            }
	        }
	        else
	        {
	            //其他CP没有数据，则可能是同洲的数据，属于存量
	            nn_log::write_log_message(LOG_MODEL_CMS,"注入的是存量任务",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	        }
	        nn_log::write_log_message(LOG_MODEL_CMS,"========同洲定制化检查结束=========检查片源ID为:".$c2_task_info['nns_ref_id'],$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	    }
        $result_media_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'media', $info['nns_id'],$info,$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config'],'epg');
        if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
        {
            $result_media_import_info['ret'] = 1;
            return $result_media_import_info;
        }
        $this->str_media_import_id = $result_media_import_info['data_info'];

	    $result_index_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'index', $info['nns_vod_index_id'],$data_info['index']['info'],$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config'],'epg');
	    if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
	    {
	        $result_index_import_info['ret'] = 1;
	        return $result_index_import_info;
	    }
	    $this->str_index_import_id = $result_index_import_info['data_info'];
	     
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'video', $info['nns_vod_id'],$data_info['video']['info'],$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config'],'epg');
	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        $result_video_import_info['ret'] = 1;
	        return $result_video_import_info;
	    }
        //单剧集注入
        if($data_info['video']['info']['nns_all_index'] >= 2 && (int)$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['epg_single_index_import_model'] === 1)
        {
            $this->str_video_import_id = '';
        }
        else
        {
            $this->str_video_import_id = $result_video_import_info['data_info'];
        }

	    if (isset($this->arr_cp_config[$info['nns_cp_id']]['nns_config']['original_cp_enabled']) && (int)$this->arr_cp_config[$info['nns_cp_id']]['nns_config']['original_cp_enabled'] === 1)
	    {
	        $new_cp_id = $import_obj->cp_id = $data_info['video']['info']['nns_producer'];
	    }
	    elseif (isset($this->arr_cp_config[$info['nns_cp_id']]['nns_config']['original_cp_enabled']) && (int)$this->arr_cp_config[$info['nns_cp_id']]['nns_config']['original_cp_enabled'] === 2)
	    {
	        $new_cp_id = $import_obj->cp_id = 0;
	    }
	    else if(isset($this->arr_cp_config[$info['nns_cp_id']]['nns_config']['original_cp_enabled']) && (int)$this->arr_cp_config[$info['nns_cp_id']]['nns_config']['original_cp_enabled'] === 3)
	    {
	        $new_cp_id = $import_obj->cp_id = $info['bk_change_cp_id'];
	    }
        else
        {
            $new_cp_id =  $info['nns_cp_id'];
        }
	    nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG分集注入开始==============================",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
		nn_log::write_log_message(LOG_MODEL_CMS,"task_id[{$c2_task_info['nns_id']}]注入行为：".$c2_task_info['nns_action']."注入epg地址:[{$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['epg_url']}]",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
		$assets_core_org_id = (isset($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['media_cdn_model']) && $this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['media_cdn_model'] == '1') ? 1 : 0;
	    if ($c2_task_info['nns_action'] == 'destroy')
	    {
	        $str_del_media_id = (isset($this->str_media_import_id) && !empty($this->str_media_import_id)) ? $this->str_media_import_id : $c2_task_info['nns_ref_id'];
	        if(!$this->arr_cp_config[$info['nns_cp_id']]['nns_config']['check_metadata_enabled'])//下发注入的随机ID
	        {
	            $result = $import_obj->delete_video_file($str_del_media_id,0,$info['nns_import_source'],$info['nns_cp_id'],(isset($c2_task_info['nns_message_id']) && strlen($c2_task_info['nns_message_id']) >0) ? $c2_task_info['nns_message_id'] : '');
	        }
	        else
	        {
	            $result = $import_obj->delete_video_file($str_del_media_id,0,null,$info['nns_cp_id'],(isset($c2_task_info['nns_message_id']) && strlen($c2_task_info['nns_message_id']) >0) ? $c2_task_info['nns_message_id'] : '');
	        }
	    }
	    else
	    {
	        if (!isset($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['import_srouce_url']) || (isset($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['import_srouce_url']) && (int)$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['import_srouce_url'] === 1))
	        {
	            $info['nns_url'] = $c2_task_info['nns_file_path'];
	            if(empty($info['nns_url']))
	            {
	                $ext_data = json_decode($info['nns_ext_info'],true);
	                $info['nns_url'] = $ext_data['path'];
	            }
	            if(empty($info['nns_url']))
	            {
	                return $this->return_data(1, '片源最终nns_url结果为空,不允许下发');
	            }
	        }
	        
	        if((isset($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['media_cdn_model']) && $this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['media_cdn_model'] == '1') 
	            || (isset($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['asset_import_cms_model']) && $this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['asset_import_cms_model'] == '1'))
	        {
	            $info['nns_url'] = $c2_task_info['nns_file_path'];
	        }
	        $info['nns_file_size'] = (int)$info['nns_file_size'] >0 ? (int)$info['nns_file_size'] : 8081;
	        
	        $arr_attr = array(
	            'file_id'=>$this->str_media_import_id,
	            'file_type'=>$info['nns_filetype'],
	            'file_name'=>htmlspecialchars($c2_task_info['nns_name'], ENT_QUOTES),
	            'file_path'=>$info['nns_url'],
	            'file_definition'=>$info['nns_mode'],
	            'file_time_len'=>$info['nns_file_time_len'],
	            'Tags'=>$info['nns_tag'],
	            'file_resolution'=>$info['nns_file_resolution'],
	            'file_size'=>$info['nns_file_size'],
	            'file_bit_rate'=>$info['nns_kbps'],
	            'file_desc'=>'',
	            'original_id'=>$info['nns_import_id'],
	            'asset_source'=>$new_cp_id,
	        );
	        if(isset($info['nns_ext_url']) && !empty($info['nns_ext_url']) && self::is_json($info['nns_ext_url']))
	        {
                $arr_ex_media_url = json_decode($info['nns_ext_url'],true);
                if(isset($arr_ex_media_url['play_url']) && strlen($arr_ex_media_url['play_url']) >0)
                {
                    $arr_attr['ex_url'] = $arr_ex_media_url['play_url'];
                }
	        }
	        else if(isset($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['asset_import_cms_model']) && $this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['asset_import_cms_model'] == '2' && isset($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['c2_play_url_ip']) && strlen($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['c2_play_url_ip']) >0)
	        {
	            $str_c2_play_url_ip = trim(trim(trim($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['c2_play_url_ip'],'/'),'\\'));
	            $str_file_path = trim(trim(trim($c2_task_info['nns_file_path'],'/'),'\\'));
	            $arr_attr['ex_url'] = $str_c2_play_url_ip.'/'.$str_file_path;
	        }
	        else
	        {
	            $arr_attr['ex_url'] = (strlen($c2_task_info['nns_ex_url'])<1 && $c2_task_info['nns_org_id']=='cqyx') ? $info['nns_content_id'] : $c2_task_info['nns_ex_url'];
	        }
	        
	        
	        if(isset($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['media_service_type']) && strlen($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['media_service_type']) >0 )
	        {
	            $arr_attr['media_service_type'] = $this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['media_service_type'];
	        }
	        else if(isset($info['nns_media_service']) && strlen($info['nns_media_service']) >0)
	        {
	            $arr_attr['media_service_type'] = $info['nns_media_service'];
	        }
	        
	        if(isset($info['nns_content_id']) && strlen($info['nns_content_id']) >0)
	        {
	            $arr_attr['file_core_id'] = $info['nns_content_id'];
	        }
	        if(isset($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['media_cdn_model']) && $this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['media_cdn_model'] == '1')
	        {
	            $arr_attr['policy'] = $info['nns_cp_id'];
	        }
	        else
	        {
	            $arr_attr['policy'] = $c2_task_info['nns_cdn_policy'];
	        }
	        if(!$this->arr_cp_config[$info['nns_cp_id']]['nns_config']['check_metadata_enabled'])//下发注入的随机ID
	        {
	            $arr_attr['import_source'] = $info['nns_import_source'];
	        }
	        //增加DRM加密及方案
	        if(isset($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['main_drm_enabled']) && intval($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['main_drm_enabled']) == 1)
	        {
	            //开启DRM
	            $arr_attr['drm_flag'] = $info['nns_drm_enabled'];
	            $arr_attr['drm_encrypt_solution'] = $this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['q_drm_identify'];
	            $arr_attr['drm_ext_info'] = '';
	        }
	        else if (isset($this->arr_cp_config[$info['nns_cp_id']]['nns_config']['main_drm_enabled']) && intval($this->arr_cp_config[$info['nns_cp_id']]['nns_config']['main_drm_enabled']) == 1)
	        {
	            //开启DRM
	            $arr_attr['drm_flag'] = $info['nns_drm_enabled'];
	            $arr_attr['drm_encrypt_solution'] = $info['nns_drm_encrypt_solution'];
	            $arr_attr['drm_ext_info'] = htmlspecialchars($info['nns_drm_ext_info']);
	        }
	        else
	        {
	            $arr_attr['drm_flag'] = 0;
	            $arr_attr['drm_encrypt_solution'] = '';
	            $arr_attr['drm_ext_info'] = '';
	        }
	        if (isset($this->arr_cp_config[$info['nns_cp_id']]['nns_config']["clip_custom_origin_id"]))
	        {
	            $arr_attr['cdn_ext_source_id'] = $this->arr_cp_config[$info['nns_cp_id']]['nns_config']["clip_custom_origin_id"];
	        }
	        if ($info['nns_dimensions'] == '100001')
	        {
	            $arr_attr['file_3d'] = 1;
	            $arr_attr['file_3d_type'] = 0;
	        }
	        elseif ($info['nns_dimensions'] == '100002')
	        {
	            $arr_attr['file_3d'] = 1;
	            $arr_attr['file_3d_type'] = 1;
	        }
	        else
	        {
	            $arr_attr['file_3d'] = 0;
	            $arr_attr['file_3d_type'] = '';
	        }
		    if(is_array($data_info['media']['ex_info']))
		    {
		        foreach ($data_info['media']['ex_info'] as $ex_key=>$ex_val)
		        {
		            if(isset($arr_attr[$ex_key]))
		            {
		                continue;
		            }
		            $arr_attr[$ex_key] = htmlspecialchars($ex_val, ENT_QUOTES);
		        }
		    }
		    $obj_dom = new DOMDocument('1.0', 'utf-8');
		    $this->make_xml($obj_dom,'media',null,$arr_attr);
		    $xml = $obj_dom->saveXML();
	        $data['func'] 				= 'import_video_file';
	        $data['assets_video_type'] 	= 0;
	        $data['assets_id'] 			= $this->str_video_import_id;
	        $data['assets_clip_id'] 	= $this->str_index_import_id;
	        $data['assets_file_id'] 	= $this->str_media_import_id;
	        $data['assets_file_info'] 	= $xml;
	        $policy = (isset($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['media_cdn_model']) && $this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['media_cdn_model'] == '1') ? $info['nns_cp_id'] : $c2_task_info['nns_cdn_policy'];
	        $data['policy'] 	= $policy;
	        nn_log::write_log_message(LOG_MODEL_CMS,'[EPG片源注入参数：]'."\n".var_export($data,true),$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	        unset($data);
			$result = $import_obj->import_video_file($this->str_video_import_id, $this->str_index_import_id, $this->str_media_import_id, $xml, 0, $policy,$assets_core_org_id,$info['nns_cp_id'],(isset($c2_task_info['nns_message_id']) && strlen($c2_task_info['nns_message_id']) >0) ? $c2_task_info['nns_message_id'] : '');
	    }
	    $re = json_decode($result, true);
        $video_type = "media";
	    nn_log::write_log_message(LOG_MODEL_CMS,'[注入结果：]'."\n".var_export($re,true),$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	    nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG主媒资注入结束,哈哈==============================",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	    nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG主媒资注入结束==============================,cp配置为：".var_export($this->arr_cp_config,true),$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);

        //香港直播转点播反馈
        if (isset($this->arr_cp_config['cp_playbill_to_media_enabled']) && $this->arr_cp_config['cp_playbill_to_media_enabled'] == '1' && isset($this->arr_cp_config['source_notify_url']) && strlen($this->arr_cp_config['source_notify_url']) >0)
        {
            nn_log::write_log_message(LOG_MODEL_CMS,'注入报错:cp配置为：'.var_export($this->arr_cp_config,true),$c2_task_info['nns_org_id'],$video_type);
            $bool = false;
            if ($re['ret'] == '0')
            {
                $bool = true;
                $result_encode = array(
                    'data_info' => $c2_task_info['nns_message_id'],
                    'ret' => 0,
                    'reason' => $re['reason'],
                );
            }
            elseif ((int)$re['ret'] === 7)
            {
                $bool = true;
                $result_encode = array(
                    'data_info' => $c2_task_info['nns_message_id'],
                    'ret' => 0,
                    'reason' => $re['reason'],
                );
            }
            elseif ((int)$re['ret'] === 6)
            {
                $bool = true;
                $result_encode = array(
                    'data_info' => $c2_task_info['nns_message_id'],
                    'ret' => 0,
                    'reason' => $re['reason'],
                );
            }
            else
            {
                nn_log::write_log_message(LOG_MODEL_CMS,'注入报错:'.var_export($re,true),$c2_task_info['nns_org_id'],$video_type);
                $result_encode = array(
                    'data_info' => $c2_task_info['nns_message_id'],
                    'ret' => 1,
                    'reason' => $re['reason'],
                );
                nn_log::write_log_message(LOG_MODEL_CMS,'注入反馈开始,参数数据为：'.var_export($result_encode,true),$c2_task_info['nns_org_id'],$video_type);
                $re_feedback = $this->notify_cp_sp_playbill_to_video($result_encode, $this->arr_cp_config['source_notify_url']);
                nn_log::write_log_message(LOG_MODEL_CMS, '直播转点播注入epg失败状态直接反馈给epg完成：状态' . var_export($re_feedback, true), $c2_task_info['nns_org_id'], $video_type);
            }

            if ($bool === true)
            {
                $re_feedback = $this->notify_cp_sp_playbill_to_video($result_encode, $this->arr_cp_config['source_notify_url']);
                nn_log::write_log_message(LOG_MODEL_CMS, '直播转点播注入epg成功状态反馈给epg完成：状态' . var_export($re_feedback, true), $c2_task_info['nns_org_id'], $video_type);
            }
        }

	    $str_cdn_notify = '';
	    if(isset($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['media_cdn_model']) && $this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['media_cdn_model'] == '1')
	    {
	        $sql_query_op_id="select nns_id,nns_message_id from nns_mgtvbk_op_queue where nns_id = '{$c2_task_info['nns_op_id']}' limit 1";
	        $info_query_op_id = nl_query_by_db($sql_query_op_id, $this->obj_dc->db());
	        $str_notify_cms_message_id = (isset($info_query_op_id[0]['nns_message_id'])) ? $info_query_op_id[0]['nns_message_id'] : $c2_task_info['nns_message_id'];
	        $asset_cdn_share_addr = isset($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['asset_cdn_share_addr']) ? trim(trim($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['asset_cdn_share_addr'],'/'),'\\') : '';
	        $result_notify_cms = $this->notify_cms_data($asset_cdn_share_addr, $info, $c2_task_info['nns_org_id'], $c2_task_info['nns_action'], $c2_task_info['nns_type'], $str_notify_cms_message_id, ((int)$re['ret'] === 0 || (int)$re['ret'] === 7 || (int)$re['ret'] === 6) ? true : false);
	        if($result_notify_cms['ret'] !=0)
	        {
	            return $result_notify_cms;
	        }
	        $str_cdn_notify = isset($result_notify_cms['data_info']) ? $result_notify_cms['data_info'] : '';
	    }
	    if($info['nns_cp_id'] == 'JSCN' || (strlen($info['nns_cp_id']) >4 && substr($info['nns_cp_id'], 0,4) == 'JSCN'))
	    {
	        $info_notify = $info;
	        $info_notify['nns_cp_id'] = 'JSCN';
	        $sp_config = array(
	            'site_callback_url'=> 'ftp://source_notify:source_notify@172.26.2.17',
	        );
	        $result = c2_task_model::notify_upstream($info, $c2_task_info['nns_action'], $re, $sp_config, $c2_task_info, $this->obj_dc,'media','',true);
	    }
	    $log_data = array (
	        'nns_id' => np_guid_rand('media'),
	        'nns_video_id' => $info['nns_id'],
	        'nns_video_type' => 'media',
	        'nns_org_id' => $c2_task_info['nns_org_id'],
	        'nns_create_time' => date('Y-m-d H:i:s'),
	        'nns_action' => $c2_task_info['nns_action'],
	        'nns_status' => $re['ret'],
	        'nns_reason' => addslashes($re['reason']),
	        'nns_data' => $xml,
	        'nns_name' => $c2_task_info['nns_name'],
	        'nns_file_dir' => $str_cdn_notify,
	    );
	    nl_db_insert($this->obj_dc->db(), 'nns_mgtvbk_import_epg_log', $log_data);
	    if ($re['ret'] == '0' || (int)$re['ret'] === 7 || (int)$re['ret'] === 6)
	    {
	        return $this->return_data(0, var_export($re,true));
	    }
	    else
	    {
	        if($c2_task_info['nns_org_id'] == 'gzgd_tz_sp')
	        {
	            return $this->return_data(999, var_export($re,true));
	        }
	        return $this->return_data(1, var_export($re,true));
	    }
	}
	
	/**
	 * 注入直播频道
	 * @param unknown $data_info
	 * @param unknown $c2_task_info
	 */
	public function import_live($data_info,$c2_task_info)
	{
	    $import_obj = new import($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['epg_url'], dirname(dirname(dirname(__FILE__))).'/data/log/import/'.$c2_task_info['nns_org_id'],'',$data_info['video']['info']['nns_cp_id']);
	    if ($data_info['live']['info']['nns_deleted'] == 1 && $c2_task_info['nns_action'] !='destroy')
	    {
	        $c2_task_info['nns_action'] = 'destroy';
	        $result_c2_ex = nl_c2_task::edit($this->obj_dc, array('nns_action'=>'destroy','nns_epg_status'=>'97'), $c2_task_info['nns_id']);
	        if($result_c2_ex['ret'] !=0)
	        {
	            return $result_c2_ex;
	        }
	    }
	    $info = $data_info['live']['info'];
	    $info['nns_category_name'] = isset($this->arr_category['live'][$info['nns_category_id']]['name']) ? $this->arr_category['live'][$info['nns_category_id']]['name'] : '卫视台';
	    nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG直播频道注入开始==============================",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	    nn_log::write_log_message(LOG_MODEL_CMS,"task_id[{$c2_task_info['nns_id']}]注入行为：".$c2_task_info['nns_action']."注入epg地址:[{$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['epg_url']}]",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	    $xml = $this->make_import_cms_xml('live',$info,null,null,$data_info['live']['ex_info']);
	    if(strlen($xml) <1)
	    {
	        nn_log::write_log_message(LOG_MODEL_CMS,'xml数据直播频道组装失败',$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']); 
	        return $this->return_data(1, 'xml数据直播频道组装失败');
	    }
	    else
	    {
	        nn_log::write_log_message(LOG_MODEL_CMS,'[EPG直播频道注入参数：]'."\n".var_export($xml,true),$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']); 
	    }
	    $result = $import_obj->import_cms_live_info($xml);
	    $re = json_decode($result, true);
	    nn_log::write_log_message(LOG_MODEL_CMS,'[注入结果：]'."\n".var_export($re,true),$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	    nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG直播频道注入结束==============================",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	    
	    $log_data = array (
	        'nns_id' => np_guid_rand('live'),
	        'nns_video_id' => $info['nns_id'],
	        'nns_video_type' => 'live',
	        'nns_org_id' => $c2_task_info['nns_org_id'],
	        'nns_create_time' => date('Y-m-d H:i:s'),
	        'nns_action' => $c2_task_info['nns_action'],
	        'nns_status' => $re['ret'],
	        'nns_reason' => addslashes($re['reason']),
	        'nns_data' => $xml,
	        'nns_name' => $c2_task_info['nns_name'],
	        'nns_file_dir' => '',
	    );
	    nl_db_insert($this->obj_dc->db(), 'nns_mgtvbk_import_epg_log', $log_data);
	    if ($re['ret'] != '0')
	    {
	        return $this->return_data(1, var_export($re,true));
	    }
	    else
	    {
	        return $this->return_data(0, var_export($re,true));
	    }
	}
	
	/**
	 * 注入直播源
	 * @param unknown $data_info
	 * @param unknown $c2_task_info
	 */
	public function import_live_media($data_info,$c2_task_info)
	{
	    $import_obj = new import($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['epg_url'], dirname(dirname(dirname(__FILE__))).'/data/log/import/'.$c2_task_info['nns_org_id'],'',$data_info['video']['info']['nns_cp_id']);
	    if ($data_info['live_media']['info']['nns_deleted'] == 1 && $c2_task_info['nns_action'] !='destroy')
	    {
	        $c2_task_info['nns_action'] = 'destroy';
	        $result_c2_ex = nl_c2_task::edit($this->obj_dc, array('nns_action'=>'destroy','nns_epg_status'=>'97'), $c2_task_info['nns_id']);
	        if($result_c2_ex['ret'] !=0)
	        {
	            return $result_c2_ex;
	        }
	    }
	    $info = $data_info['live_media']['info'];
	    nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG直播源注入开始==============================",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	    nn_log::write_log_message(LOG_MODEL_CMS,"task_id[{$c2_task_info['nns_id']}]注入行为：".$c2_task_info['nns_action']."注入epg地址:[{$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['epg_url']}]",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	    $data_info['live']['info']['nns_category_name'] = isset($this->arr_category['live'][$data_info['live']['info']['nns_category_id']]['name']) ? $this->arr_category['live'][$data_info['live']['info']['nns_category_id']]['name'] : '卫视台';
	     
	    $xml = $this->make_import_cms_xml('live_media',$data_info['live']['info'],$data_info['live_index']['info'],$info,$data_info['live']['ex_info']);
	    if(strlen($xml) <1)
	    {
	        nn_log::write_log_message(LOG_MODEL_CMS,'xml数据直播源组装失败',$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']); 
	        return $this->return_data(1, 'xml数据直播源组装失败');
	    }
	    else
	    {
	        nn_log::write_log_message(LOG_MODEL_CMS,'[EPG直播源注入参数：]'."\n".var_export($xml,true),$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);  
	    }
	    $result = $import_obj->import_cms_live_info($xml);
	    $re = json_decode($result, true);
	    nn_log::write_log_message(LOG_MODEL_CMS,'[注入结果：]'."\n".var_export($re,true),$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	    nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG直播源注入结束==============================",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	     
	    $log_data = array (
	        'nns_id' => np_guid_rand('live_media'),
	        'nns_video_id' => $info['nns_id'],
	        'nns_video_type' => 'live_media',
	        'nns_org_id' => $c2_task_info['nns_org_id'],
	        'nns_create_time' => date('Y-m-d H:i:s'),
	        'nns_action' => $c2_task_info['nns_action'],
	        'nns_status' => $re['ret'],
	        'nns_reason' => addslashes($re['reason']),
	        'nns_data' => $xml,
	        'nns_name' => $c2_task_info['nns_name'],
	        'nns_file_dir' => '',
	    );
	    nl_db_insert($this->obj_dc->db(), 'nns_mgtvbk_import_epg_log', $log_data);
	    if ($re['ret'] != '0')
	    {
	        return $this->return_data(1, var_export($re,true));
	    }
	    else
	    {
	        return $this->return_data(0, var_export($re,true));
	    }
	}
	
	
	/**
	 * 注入节目单
	 * @param unknown $data_info
	 * @param unknown $c2_task_info
	 */
	public function import_playbill($data_info,$c2_task_info)
	{
	    $import_obj = new import($this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['epg_url'], dirname(dirname(dirname(__FILE__))).'/data/log/import/'.$c2_task_info['nns_org_id'],'',$data_info['video']['info']['nns_cp_id']);
	    if ($data_info['playbill']['info']['nns_deleted'] == 1 && $c2_task_info['nns_action'] !='destroy')
	    {
	        $c2_task_info['nns_action'] = 'destroy';
	        $result_c2_ex = nl_c2_task::edit($this->obj_dc, array('nns_action'=>'destroy','nns_epg_status'=>'97'), $c2_task_info['nns_id']);
	        if($result_c2_ex['ret'] !=0)
	        {
	            return $result_c2_ex;
	        }
	    }
	    $info = $data_info['playbill']['info'];
	    nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG节目单注入开始==============================",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	    nn_log::write_log_message(LOG_MODEL_CMS,"task_id[{$c2_task_info['nns_id']}]注入行为：".$c2_task_info['nns_action']."注入epg地址:[{$this->arr_sp_config[$c2_task_info['nns_org_id']]['nns_config']['epg_url']}]",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	    $info['nns_live_media_import_id'] = $data_info['live_media']['info']['nns_live_media_import_id'];
	    $info['nns_flag'] = (isset($this->arr_cp_config[$info['nns_cp_id']]['nns_config']['live_playbill_cms_model']) && $this->arr_cp_config[$info['nns_cp_id']]['nns_config']['live_playbill_cms_model'] == '1') ? 'live' : 'live_media';
	    $info['nns_live_import_id'] = $data_info['live']['info']['nns_live_import_id'];
	    $xml = $this->make_import_cms_xml('playbill',$info);
	    if(strlen($xml) <1)
	    {
	        nn_log::write_log_message(LOG_MODEL_CMS,'xml数据节目单组装失败',$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']); 
	        return $this->return_data(1, 'xml数据节目单组装失败');
	    }
	    else
	    {
	        nn_log::write_log_message(LOG_MODEL_CMS,'[EPG节目单注入参数：]'."\n".var_export($xml,true),$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);  
	    }
	    $result = $import_obj->import_cms_playbill_info($xml);
	    $re = json_decode($result, true);
	    nn_log::write_log_message(LOG_MODEL_CMS,'[注入结果：]'."\n".var_export($re,true),$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	    nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG节目单注入结束==============================",$info['nns_cp_id'],$c2_task_info['nns_type'],'message',$c2_task_info['nns_org_id']);
	
	    $log_data = array (
	        'nns_id' => np_guid_rand('playbill'),
	        'nns_video_id' => $info['nns_id'],
	        'nns_video_type' => 'playbill',
	        'nns_org_id' => $c2_task_info['nns_org_id'],
	        'nns_create_time' => date('Y-m-d H:i:s'),
	        'nns_action' => $c2_task_info['nns_action'],
	        'nns_status' => $re['ret'],
	        'nns_reason' => addslashes($re['reason']),
	        'nns_data' => $xml,
	        'nns_name' => $c2_task_info['nns_name'],
	        'nns_file_dir' => '',
	    );
	    nl_db_insert($this->obj_dc->db(), 'nns_mgtvbk_import_epg_log', $log_data);
	    if ($re['ret'] != '0')
	    {
	        return $this->return_data(1, var_export($re,true));
	    }
	    else
	    {
	        return $this->return_data(0, var_export($re,true));
	    }
	}
	
	/**
	 * 点播、直播、节目单 xml组装
	 * @param string $type
	 * @param string $data_vod
	 * @param string $data_index
	 * @param string $data_media
	 * @param string $data_vod_ex
	 * @param string $data_index_ex
	 * @param string $data_media_ex
	 * @return string
	 */
	public function make_import_cms_xml($type='live',$data_vod=null,$data_index=null,$data_media=null,$data_vod_ex=null,$data_index_ex=null,$data_media_ex=null)
	{
	    switch ($type)
	    {
	        case 'live':
	            $str_assettype=2;
	            break;
	        case 'video':
	            $str_assettype=1;
	            break;
	        case 'playbill':
	            $str_assettype=2;
	            break;
	        default:
	            return '';
	    }
	    $str_xml  = '<?xml version="1.0" encoding="UTF-8"?>';
	    $str_xml .= '<assetcontent>';
	    $str_xml .= 	'<assettype>'.$str_assettype.'</assettype>';
	    $str_xml .= 	'<assetdesc>4</assetdesc>';
	    if(!empty($data_vod) && is_array($data_vod))
	    {
	        $str_xml .= 	'<'.$type.'>';
	        $str_operation = ($data_vod['nns_deleted'] !=1) ? 1 : 2;
	        $str_xml .= 		'<assetoperation>'.$str_operation.'</assetoperation>';
	        $str_xml .= 		'<base_info>';
	        foreach ($data_vod as $key=>$val)
	        {
	            if (in_array($key, array('nns_id','nns_create_time','nns_modify_time','nns_org_id','nns_state','nns_check','nns_deleted','nns_depot_id','nns_domain')))
	            {
	                continue;
	            }
	            $key = str_replace('nns_', '', $key);
	            $val = str_replace(array('&','<','>','"',"'"), '',$val);
	            $str_xml .= 		'<'.$key.'>'.$val.'</'.$key.'>';
	        }
	        $str_xml .= 		'</base_info>';
	        $str_xml .= 		'<ex_info>';
	        if(!empty($data_vod_ex) && is_array($data_vod_ex))
	        {
	            foreach ($data_vod_ex as $vod_ex_val)
	            {
	                $str_xml .= 		'<'.$vod_ex_val['nns_key'].'>'.$vod_ex_val['nns_value'].'</'.$vod_ex_val['nns_key'].'>';
	            }
	        }
	        $str_xml .= 		'</ex_info>';
	        $str_xml .= 	'</'.$type.'>';
	    }
	    if(!empty($data_index) && is_array($data_index))
	    {
	        $str_xml .= 	'<'.$type.'_index>';
	        $str_operation = ($data_index['nns_deleted'] !=1) ? 1 : 2;
	        $str_xml .= 		'<assetoperation>'.$str_operation.'</assetoperation>';
	        $str_xml .= 		'<base_info>';
	        foreach ($data_index as $key=>$val)
	        {
	            if (in_array($key, array('nns_id','nns_create_time','nns_modify_time','nns_deleted')))
	            {
	                continue;
	            }
	            $key = str_replace('nns_', '', $key);
	            $str_xml .= 		'<'.$key.'>'.$val.'</'.$key.'>';
	        }
	        $str_xml .= 		'</base_info>';
	        $str_xml .= 		'<ex_info>';
	        if(!empty($data_index_ex) && is_array($data_index_ex))
	        {
	            foreach ($data_index_ex as $index_ex_val)
	            {
	                $str_xml .= 		'<'.$index_ex_val['nns_key'].'>'.$index_ex_val['nns_value'].'</'.$index_ex_val['nns_key'].'>';
	            }
	        }
	        $str_xml .= 		'</ex_info>';
	        $str_xml .= 	'</'.$type.'_index>';
	    }
	    if(!empty($data_media) && is_array($data_media))
	    {
	        $str_xml .= 	'<'.$type.'_media>';
	        $str_operation = ($data_media['nns_deleted'] !=1) ? 1 : 2;
	        $str_xml .= 		'<assetoperation>'.$str_operation.'</assetoperation>';
	        $str_xml .= 		'<base_info>';
	        foreach ($data_media as $key=>$val)
	        {
	            if (in_array($key, array('nns_id','nns_create_time','nns_modify_time','nns_deleted','nns_domain','nns_drm_flag','nns_ext_url')))
	            {
	                continue;
	            }
	            $key = str_replace('nns_', '', $key);
	            if($key == 'drm_ext_info')
	            {
	                $val = htmlspecialchars($val);
	            }
	            $str_xml .= 	'<'.$key.'>'.$val.'</'.$key.'>';
	        }
	        if(strlen($data_media['nns_ext_url']) <1)
	        {
	            $str_xml .= 	'<ex_url>'.$data_media['nns_url'].'</ex_url>';
	        }
	        else
	        {
	            $str_xml .= 	'<ex_url>'.$data_media['nns_ext_url'].'</ex_url>';
	        }
	        $str_xml .= 		'</base_info>';
	        $str_xml .= 		'<ex_info>';
	        if(!empty($data_media_ex) && is_array($data_media_ex))
	        {
	            foreach ($data_media_ex as $media_ex_val)
	            {
	                $str_xml .= 		'<'.$media_ex_val['nns_key'].'>'.$media_ex_val['nns_value'].'</'.$media_ex_val['nns_key'].'>';
	            }
	        }
	        $str_xml .= 		'</ex_info>';
	        $str_xml .= 	'</'.$type.'_media>';
	    }
	    $str_xml .= '</assetcontent>';
	    return $str_xml;
	}
	
	
	/**
	 * 消息反馈上游
	 * @param unknown $asset_cdn_share_addr
	 * @param unknown $info
	 * @param unknown $sp_id
	 * @param unknown $action
	 * @param unknown $video_type
	 * @param unknown $str_message_id
	 * @param unknown $sp_config
	 * @param unknown $bool
	 * @return multitype:number mixed |Ambigous <multitype:number, multitype:number unknown mixed >
	 */
	public function notify_cms_data($asset_cdn_share_addr,$info,$sp_id,$action,$video_type,$str_message_id,$bool,$arr_data=null)
	{
	    $str_cdn_notify = '';
	    $asset_cdn_share_addr = isset($asset_cdn_share_addr) ? trim(trim($asset_cdn_share_addr,'/'),'\\') : '';
	    if(empty($asset_cdn_share_addr))
	    {
	        nn_log::write_log_message(LOG_MODEL_CMS,'媒资CDN共享地址未配置:'.$asset_cdn_share_addr,$info['nns_cp_id'],$video_type,'message',$sp_id);
	        return $this->return_data(1, '媒资CDN共享地址未配置:'.$asset_cdn_share_addr);
	    }
	    $str_sp_model = (isset($this->arr_sp_config[$sp_id]['nns_config']['cdn_message_feedback_model'])) ? $this->arr_sp_config[$sp_id]['nns_config']['cdn_message_feedback_model'] : "soap";
	    $str_sp_model = ($str_sp_model == "soap") ? "unicom_zte" : "http";
	    include_once dirname(dirname(__FILE__)).'/cdn_notify/cdn_notify.class.php';
	    $obj_cdn_notify = new nn_cdn_notify($sp_id,$info['nns_cp_id']);
	    $str_ResultCode= ($bool === true) ? 0 : -1;
	    $str_Description = ($bool === true) ? 'success' : 'error';
	    if($str_sp_model == 'http')
	    {
	        $result_cdn_notify = $obj_cdn_notify->make_unicom_d3_cdn_file($info,$str_ResultCode,$str_Description,$video_type,$action,$arr_data);
	    }
	    else
	    {
	        $result_cdn_notify = $obj_cdn_notify->make_cdn_file($info,$str_ResultCode,$str_Description,$video_type,$action,$arr_data);
	    }
	    if($result_cdn_notify['ret'] !=0 || !isset($result_cdn_notify['data_info']) || empty($result_cdn_notify['data_info']))
	    {
	        nn_log::write_log_message(LOG_MODEL_CMS,"生成CDN放FTP下拉文件失败".var_export($result_cdn_notify,true),$info['nns_cp_id'],$video_type,'message',$sp_id);
	        return $this->return_data(1,"生成CDN放FTP下拉文件失败".var_export($result_cdn_notify,true));
	    }
	    //根据不同的反馈模式，执行反馈
	    if($str_sp_model == "http")
	    {
	        global $g_bk_web_url;
	        $asset_cdn_share_addr = (isset($g_bk_web_url) && strlen($g_bk_web_url) >0) ? trim(rtrim(rtrim($g_bk_web_url,'/'),'\\')) : '';
	        unset($g_bk_web_url);
	        if(strlen($asset_cdn_share_addr) <1)
	        {
	            nn_log::write_log_message(LOG_MODEL_CMS,"g_bk_web_url全局后台基本地址(数据库配置文件父级目录)未配置",$info['nns_cp_id'],$video_type,'message',$sp_id);
	            return $this->return_data(1,"g_bk_web_url全局后台基本地址(数据库配置文件父级目录)未配置");
	        }
	        $data = file_get_contents($asset_cdn_share_addr.'/'.$result_cdn_notify['data_info'], true);
	        $arr_message_info = nl_message::get_message_info($this->obj_dc,array("nns_message_id"=>$str_message_id));
	        $arr_ext_info = json_decode($arr_message_info["data_info"][0]["nns_ext_info"], true);
	        $obj_curl = new np_http_curl_class();
	        $result_notify = $obj_curl->do_request("post",$arr_ext_info["feedback_url"], $data);
	        $curl_info = $obj_curl->curl_getinfo();
	        $http_code = $curl_info['http_code'];
	        if($http_code["http_code"] != "200")
	        {
	            nn_log::write_log_message(LOG_MODEL_CMS,'消息反馈结果：'.var_export($result_notify,true),$info['nns_cp_id'],$video_type,'message',$sp_id);
	            return $this->return_data(1,'消息反馈结果：'.var_export($result_notify,true));
	        }
	    }
	    else
	    {
	        global $g_project_name;
	        $str_sp_model = (isset($g_project_name) && strlen($g_project_name) >0) ? trim($g_project_name) : '';
	        unset($g_project_name);
	        if(strlen($str_sp_model) <1)
	        {
	            nn_log::write_log_message(LOG_MODEL_CMS,"g_project_name全局项目ID未配置",$info['nns_cp_id'],$video_type,'message',$sp_id);
	            return $this->return_data(1,"g_project_name全局项目ID未配置");
	        }
	        $cdn_include_file = dirname(dirname(dirname(__FILE__))).'/mgtv/'.$str_sp_model.'/content_deploy_client.php';
	        if(!file_exists($cdn_include_file))
	        {
	            nn_log::write_log_message(LOG_MODEL_CMS,'CDN消息反馈文件不存在,文件路径:'.$cdn_include_file,$info['nns_cp_id'],$video_type,'message',$sp_id);
	            return $this->return_data(1,'CDN消息反馈文件不存在,文件路径:'.$cdn_include_file);
	        }
	        include_once $cdn_include_file;
	        $content_deploy_obj = new content_deploy_client($info['nns_cp_id'],'starcor');
	        $result_notify = $content_deploy_obj->ContentDeployResult($str_message_id, 0, 'success', $asset_cdn_share_addr.$result_cdn_notify['data_info']);
	        unset($content_deploy_obj);
	        unset($str_obj_name);
	        if($result_notify->ResultCode != 0)
	        {
	            nn_log::write_log_message(LOG_MODEL_CMS,'消息反馈结果：'.var_export($result_notify,true),$info['nns_cp_id'],$video_type,'message',$sp_id);
	            return $this->return_data(1,'消息反馈结果：'.var_export($result_notify,true));
	        }
	    }
	    return $this->return_right_data(0,'ok',$result_cdn_notify['data_info']);
	}

    /**
     * 消息反馈
     * @param $result_encode
     * @param $url
     * @return array
     */
    public function notify_cp_sp_playbill_to_video($result_encode,$url)
    {

        $playbill_info = array(
            'id'=>$result_encode['data_info'],
            'state'=>$result_encode['ret'] == 0 ? 2 : 3,
            'summary'=>$result_encode['reason'],
        );
        return $this->_execute_curl($url,'post',array('a'=>'update_vod_playbill_transcode_state','playbill_info'=>json_encode(array($playbill_info))));
    }

    /**
     * 执行curl
     * @param $send_url
     * @param string $func
     * @param null $content
     * @param null $arr_header
     * @param int $time
     * @return array
     */
    public function _execute_curl($send_url,$func='get',$content=null,$arr_header=null,$time=60)
    {
        $obj_curl = new np_http_curl_class();
        $curl_data = $obj_curl->do_request($func,$send_url,$content,$arr_header,60);
        $curl_info = $obj_curl->curl_getinfo();
        $http_code = $curl_info['http_code'];
        if($curl_info['http_code'] != '200')
        {
            return $this->_return_data(1,'[访问第三方接口,CURL接口地址]：'.$send_url.'[HTTP状态码]：'.$curl_info['http_code'].'[HTTP错误]：'.$obj_curl->curl_error(),$curl_data);
        }
        else
        {
            return $this->_return_data(0,'[访问第三方接口,CURL接口地址]：'.$send_url,$curl_data);
        }
    }

    /**
     * 返回数据数组
     * @param int $ret
     * @param string $str_desc //英文描述
     * @param null $error_info //错误描述
     * @return array
     */
    private function _return_data($ret=200,$str_desc='',$error_info=null)
    {
        return array(
            'ret'=>$ret,
            'reason'=>$str_desc,
            'error_info'=>$error_info,
        );
    }
}