<?php
class public_model_exec
{
	/**
     * @param $sp_id    SP_ID
     * @param $video_type   媒资类型
     * @param $nns_id   媒资ID
     * @return array    返回信息s4
     * @author xingcheng.hu
     * @date 2016/04/29
     */
	public static function get_asset_id_by_sp_id($sp_id, $video_type, $nns_id, $video_info=null ,$sp_config = null, $mode='cdn',$str_header=null)
	{
		if (strlen($sp_id) < 1)
		{
			return self::return_data('1', 'sp_id为空', '');
		}
		if (strlen($video_type) < 1)
		{
			return self::return_data('1', 'video_type为空', '');
		}
		if (strlen($nns_id) < 1)
		{
			return self::return_data('1', 'nns_id为空', '');
		}
		if (!isset($sp_config) || empty($sp_config) || !is_array($sp_config))
		{
			$sp_config = sp_model::get_sp_config($sp_id);
		}
		//谁加的这个，整的CDN与EPG的注入ID没有分离了。update by zhiyong.luo 2017-09-19
//		if (!isset($sp_config['import_id_mode']) || strlen($sp_config['import_id_mode']) < 1 || (int)$sp_config['import_id_mode'] < 1)
//		{
//			return self::return_data(0, 'ok', $nns_id);
//		}
        //兼容begin
        if($mode === 'epg')
        {
            $config_action = $mode . "_" . $video_type . "_import_id_mode";

        }
        else
        {
            $config_action = "import_id_mode";
        }
        if (!isset($sp_config[$config_action]) || strlen($sp_config[$config_action]) < 1 || (int)$sp_config[$config_action] < 1)
		{
			return self::return_data(0, 'ok', $nns_id);
		}
        //兼容结束
		switch ($video_type)
		{
			case 'video':
				$str_func = 'get_vod_info';
				$str_integer_id = 'nns_integer_id';
				$str_import_id = 'nns_asset_import_id';
                $cp_id = 'nns_cp_id';
				$import_id_model_flags = '1';
				$import_id_model_flags_1 = '000000';
				$import_id_model_flags_2 = '0000';
				$import_id_model_flags_3 = '000001';
// 				$import_id_model_flags_4 = 'PT';
				$import_id_model_flags_4 = 'PA';
				break;
			case 'index':
				$str_func = 'get_vod_index_info';
				$str_integer_id = 'nns_integer_id';
				$str_import_id = 'nns_import_id';
                $cp_id = 'nns_cp_id';
				$import_id_model_flags = '2';
				$import_id_model_flags_1 = '001000';
				$import_id_model_flags_2 = '0001';
				$import_id_model_flags_3 = '000002';
// 				$import_id_model_flags_4 = 'TI';
				$import_id_model_flags_4 = 'TE';
				break;
			case 'media':
				$str_func = 'get_vod_media_info';
				$str_integer_id = 'nns_integer_id';
				$str_import_id = 'nns_import_id';
                $cp_id = 'nns_cp_id';
				$import_id_model_flags = '3';
				$import_id_model_flags_1 = '002000';
                if($sp_id == 'hndx')
                {
                    $import_id_model_flags_1 = '001000';
                }
				$import_id_model_flags_2 = '0002';
				$import_id_model_flags_3 = '000003';
// 				$import_id_model_flags_4 = 'MO';
				$import_id_model_flags_4 = 'MV';
				break;
			case 'live':
				$str_func = 'get_live_info';
				$str_integer_id = 'nns_integer_id';
				$str_import_id = 'nns_import_id';
                $cp_id = 'nns_cp_id';
				$import_id_model_flags = '4';
				$import_id_model_flags_1 = '004000';
				$import_id_model_flags_2 = '0004';
				$import_id_model_flags_3 = '010001';
				$import_id_model_flags_4 = 'CH';
				break;
			case 'live_index':
				$str_func = 'get_live_index_info';
				$str_integer_id = 'nns_integer_id';
				$str_import_id = 'nns_import_id';
                $cp_id = 'nns_cp_id';
				$import_id_model_flags = '5';
				$import_id_model_flags_1 = '005000';
				$import_id_model_flags_2 = '0005';
				$import_id_model_flags_3 = '010002';
				$import_id_model_flags_4 = 'LI';
				break;
			case 'live_media':
				$str_func = 'get_live_media_info';
				$str_integer_id = 'nns_integer_id';
				$str_import_id = 'nns_content_id';
                $cp_id = 'nns_cp_id';
				$import_id_model_flags = '6';
				$import_id_model_flags_1 = '006000';
				$import_id_model_flags_2 = '0006';
				$import_id_model_flags_3 = '010003';
				$import_id_model_flags_4 = 'PC';
				break;
			case 'playbill':
				$str_func = 'get_playbill_info';
				$str_integer_id = 'nns_integer_id';
				$str_import_id = 'nns_playbill_import_id';
                $cp_id = 'nns_cp_id';
				$import_id_model_flags = '7';
				$import_id_model_flags_1 = '007000';
				$import_id_model_flags_2 = '0007';
				$import_id_model_flags_3 = '020001';
				$import_id_model_flags_4 = 'SD';
				break;
			case 'file':
			    $str_func = 'get_file_package_info';
			    $str_integer_id = 'nns_integer_id';
			    $str_import_id = 'nns_import_id';
                $cp_id = 'nns_cp_id';
			    $import_id_model_flags = '8';
			    $import_id_model_flags_1 = '000000';
			    $import_id_model_flags_2 = '0000';
			    $import_id_model_flags_3 = '030001';
				$import_id_model_flags_4 = 'FI';
			    break;
			default:
				return self::return_data('1', '媒资类型错误！');
		}
		if(!isset($video_info) || !is_array($video_info) || empty($video_info))
		{
			$video_info = video_model::$str_func($nns_id);
		}
		$str_csp_id = defined('CSP_ID') ? CSP_ID : (isset($sp_config['import_csp_id']) ? $sp_config['import_csp_id'] : '');

		switch ($sp_config[$config_action])
		{
			case '1': //数字ID
				$asset_id = $import_id_model_flags_1 . $video_info[$str_integer_id];
				$asset_id = str_pad($asset_id, 32, "0");
				break;
			case '2': //原始ID
				$asset_id = $video_info[$str_import_id];
				break;
			case '3': //CSPID+数字ID
				$asset_id = $str_csp_id . $import_id_model_flags_2 . $video_info[$str_integer_id];
				$asset_id = str_pad($asset_id, 32, "0");
				break;
			case '4':
				$arr_csp_id = explode("|", $str_csp_id);
				$str_length = strlen($arr_csp_id[0]) + strlen($arr_csp_id[1]) +1;
				$lest_num = 32 - $str_length;
				$asset_id = $arr_csp_id[0] . $import_id_model_flags . str_pad($video_info[$str_integer_id], $lest_num, "0", STR_PAD_LEFT) . $arr_csp_id[1];
				break;
			case '5':
				$str_length = strlen($import_id_model_flags_3);
				$lest_num = 32 - $str_length;
				$asset_id = $import_id_model_flags_3 . str_pad($video_info[$str_integer_id], $lest_num, "0", STR_PAD_LEFT);
				break;
			case '6':
			    $str_length = strlen($import_id_model_flags_1);
			    $lest_num = 32 - $str_length;
			    $asset_id = $import_id_model_flags_1 . str_pad($video_info[$str_integer_id], $lest_num, "0", STR_PAD_LEFT);
			    break;
		    case '7':
		        $str_header = (strlen($str_header) < 8) ? str_pad($str_header, 8, "0", STR_PAD_RIGHT) : substr($str_header, 0,8);
		        $asset_id = $str_header.$import_id_model_flags_4 . str_pad($video_info[$str_integer_id], 10, "0", STR_PAD_LEFT);
		        break;
            case '8': //CPID+import_id_model_flags+数字ID
                $str_length = strlen($video_info[$cp_id]) + strlen($import_id_model_flags);
                $lest_num = 32 - $str_length;
                $asset_id = $video_info[$cp_id] . $import_id_model_flags . str_pad($video_info[$str_integer_id], $lest_num, "0", STR_PAD_LEFT);
                break;
            case '9': //md5(CPID+import_id)
                $asset_id = md5($video_info[$cp_id] . $video_info[$str_import_id]);
                break;
            case '10':
                $str_header = (strlen($str_header) < 8) ? str_pad($str_header, 8, "0", STR_PAD_RIGHT) : substr($str_header, 0,8);
                $asset_id = $str_header.$import_id_model_flags_4 . str_pad($video_info[$str_integer_id], 10, "0", STR_PAD_LEFT);
                $asset_id = strtolower($asset_id);
                break;
            case '12'://生成20位注入ID  import_id_model_flags+补全0+integer_id
                $str_length = strlen($import_id_model_flags);
                $lest_num = 20 - $str_length;
                $asset_id = $import_id_model_flags . str_pad($video_info[$str_integer_id], $lest_num, "0", STR_PAD_LEFT);
                break;
			default:
				$asset_id = $nns_id;
				break;
		}

		return self::return_data(0, 'ok', $asset_id);
	}
	
	/**
	 * 第三方注入id  反解析
	 * @param unknown $sp_id
	 * @param unknown $video_type
	 * @param unknown $nns_id
	 * @param string $sp_config
	 */
	public static function get_asset_id_by_sp_import_id($sp_id, $video_type, $nns_id,$cp_id=null,$sp_config = null,$mode = "cdn")
	{
	    if (strlen($sp_id) < 1)
	    {
	        return self::return_data('1', 'sp_id为空', '');
	    }
	    if (strlen($video_type) < 1)
	    {
	        return self::return_data('1', 'video_type为空', '');
	    }
	    if (strlen($nns_id) < 1)
	    {
	        return self::return_data('1', 'nns_id为空', '');
	    }
	    if (!isset($sp_config) || empty($sp_config) || !is_array($sp_config))
	    {
	        $sp_config = sp_model::get_sp_config($sp_id);
	    }
	    $array_data = null;
        //谁加的这个，整的CDN与EPG的注入ID没有分离了。update by zhiyong.luo 2017-09-19
//        if (!isset($sp_config['import_id_mode']) || strlen($sp_config['import_id_mode']) < 1 || (int)$sp_config['import_id_mode'] < 1)
//        {
//            $array_data['nns_id'] = $nns_id;
//            $video_info = video_model::get_back_info_one($video_type,$array_data);
//            $last_data=array(
//                'query_info'=>$array_data,
//                'data_info'=>$video_info
//            );
//            return self::return_data(0, 'ok', $last_data);
//        }
        //兼容-begin
        if($mode === 'epg')
        {
            $config_action = $mode . "_" . $video_type . "_import_id_mode";
        }
        else
        {
            $config_action = "import_id_mode";
        }
	    if (!isset($sp_config[$config_action]) || strlen($sp_config[$config_action]) < 1 || (int)$sp_config[$config_action] < 1)
	    {
	        $array_data['nns_id'] = $nns_id;
	        $video_info = video_model::get_back_info_one($video_type,$array_data);
	        $last_data=array(
	            'query_info'=>$array_data,
	            'data_info'=>$video_info
	        );
	        return self::return_data(0, 'ok', $last_data);
	    } //兼容-end
	    else
	    {
    	    switch ($video_type)
    	    {
    	        case 'video':
    	            $str_func = 'get_vod_info';
    	            $str_integer_id = 'nns_integer_id';
    	            $str_import_id = 'nns_asset_import_id';
    	            $import_id_model_flags = '1';
    	            $import_id_model_flags_1 = '000000';
    	            $import_id_model_flags_2 = '0000';
    	            $import_id_model_flags_3 = '000001';
    	            break;
    	        case 'index':
    	            $str_func = 'get_vod_index_info';
    	            $str_integer_id = 'nns_integer_id';
    	            $str_import_id = 'nns_import_id';
    	            $import_id_model_flags = '2';
    	            $import_id_model_flags_1 = '001000';
    	            $import_id_model_flags_2 = '0001';
    	            $import_id_model_flags_3 = '000002';
    	            break;
    	        case 'media':
    	            $str_func = 'get_vod_media_info';
    	            $str_integer_id = 'nns_integer_id';
    	            $str_import_id = 'nns_import_id';
    	            $import_id_model_flags = '3';
    	            $import_id_model_flags_1 = '002000';
    	            $import_id_model_flags_2 = '0002';
    	            $import_id_model_flags_3 = '000003';
    	            break;
    	        case 'live':
    	            $str_func = 'get_live_info';
    	            $str_integer_id = 'nns_integer_id';
    	            $str_import_id = 'nns_import_id';
    	            $import_id_model_flags = '4';
    	            $import_id_model_flags_1 = '004000';
    	            $import_id_model_flags_2 = '0004';
    	            $import_id_model_flags_3 = '010001';
    	            break;
    	        case 'live_index':
    	            $str_func = 'get_live_index_info';
    	            $str_integer_id = 'nns_integer_id';
    	            $str_import_id = 'nns_import_id';
    	            $import_id_model_flags = '5';
    	            $import_id_model_flags_1 = '005000';
    	            $import_id_model_flags_2 = '0005';
    	            $import_id_model_flags_3 = '010002';
    	            break;
    	        case 'live_media':
    	            $str_func = 'get_live_media_info';
    	            $str_integer_id = 'nns_integer_id';
    	            $str_import_id = 'nns_content_id';
    	            $import_id_model_flags = '6';
    	            $import_id_model_flags_1 = '006000';
    	            $import_id_model_flags_2 = '0006';
    	            $import_id_model_flags_3 = '010003';
    	            break;
    	        case 'playbill':
    	            $str_func = 'get_playbill_info';
    	            $str_integer_id = 'nns_integer_id';
    	            $str_import_id = 'nns_playbill_import_id';
    	            $import_id_model_flags = '7';
    	            $import_id_model_flags_1 = '007000';
    	            $import_id_model_flags_2 = '0007';
    	            $import_id_model_flags_3 = '020001';
    	            break;
	            case 'file':
	                $str_func = 'get_file_package_info';
	                $str_integer_id = 'nns_integer_id';
	                $str_import_id = 'nns_import_id';
	                $import_id_model_flags = '8';
	                $import_id_model_flags_1 = '000000';
	                $import_id_model_flags_2 = '0000';
	                $import_id_model_flags_3 = '030001';
	                break;
    	        default:
    	            return self::return_data('1', '媒资类型错误！');
    	    }
    	    $str_csp_id = defined('CSP_ID') ? CSP_ID : isset($sp_config['import_csp_id']) ? $sp_config['import_csp_id'] : '';
			
    	    switch ($sp_config[$config_action])
    	    {
    	        case '1':
    	            //return self::return_data('1', '不支持此类型！');
    	        	$array_data['nns_id'] = $nns_id;
    	            break;
    	        case '2':
    	            $array_data[$str_import_id] = $nns_id;
    	            break;
    	        case '3':
    	            //return self::return_data('1', '不支持此类型！');
    	        	$array_data['nns_id'] = $nns_id;
    	            break;
    	        case '4':
    	            $arr_csp_id = explode("|", $str_csp_id);
    	            $num_begin = strlen($arr_csp_id[0] . $import_id_model_flags);
    	            $num_begin = $num_begin>0 ? $num_begin : 0;
    	            $num_end = strlen($arr_csp_id[1]);
    	            $num_end = $num_end>0 ? $num_end : 0;
    	            $nns_id = substr($nns_id, $num_begin);
    	            $array_data[$str_integer_id] = ($num_end >0) ? substr($nns_id, 0,-$num_end) : $nns_id;
    	            break;
    	        case '5':
    	            $num_begin = strlen($import_id_model_flags_3);
    	            $num_begin = $num_begin>0 ? $num_begin : 0;
    	            $array_data[$str_integer_id] = preg_replace('/^0*/', '',substr($nns_id, $num_begin));
    	            break;
	            case '7':
	                $array_data[$str_integer_id] = preg_replace('/^0*/', '',substr($nns_id, 10));
	                break;
                case '8': //cpid+import_id_model_flags+数字ID
                    $num_begin = strlen($cp_id) + strlen($import_id_model_flags);
                    $array_data[$str_integer_id] =  preg_replace('/^0*/', '',substr($nns_id, $num_begin));
                    break;
                case '12':
                    $num_begin = strlen($import_id_model_flags);
                    $num_begin = $num_begin > 0 ? $num_begin : 0;
                    $array_data[$str_integer_id] = preg_replace('/^0*/', '',substr($nns_id, $num_begin));
                    break;
    	        default:
    	            $array_data['nns_id'] = $nns_id;
    	    }
	    }
        if(isset($cp_id) && strlen($cp_id)>0)
        {
            $array_data['nns_cp_id']=$cp_id;
        }
        $array_data['nns_deleted'] = '0';
        $video_info = video_model::get_back_info_one($video_type,$array_data);
	    $last_data=array(
	       'query_info'=>$array_data,
	       'data_info'=>$video_info
	    );
	    return self::return_data(0, 'ok', $last_data);
	}

	/**
	 * 公共获取栏目信息
	 * @param unknown $xml
	 * @param string $category_id
	 * @param string $category_name
	 * @return Ambigous <multitype:, multitype:int string Ambigous <NULL, null> >
	 */
	public static function get_category_info($xml,$category_id=null,$category_name=null)
	{
		$xml = self::trim_xml_header($xml);
		if(strlen($xml) <1)
		{
			return self::return_data(0, 'xml为空');
		}
		$obj_dom = new DOMDocument("1.0", 'utf-8');
		
		$obj_dom->loadXML($xml);
		$category_info = array();
		foreach($obj_dom->getElementsByTagName('category') as $category)
		{
			if(strlen($category->getAttribute('id')) < 1)
			{
				continue;
			}
			$category_info[$category->getAttribute('id')] = array('id' => $category->getAttribute('id'), 'name' => $category->getAttribute('name'), 'parent' => $category->getAttribute('parent'));
		}
		return self::return_data(0, 'ok',$category_info);
	}
	
	
	/**
	 * xml 编码转换为UTF-8
	 * @param string $xml xml内容
	 * @param string $encode 内容编码
	 * @return string|mixed
	 * @author liangpan
	 * @date 2016-03-12
	 */
	public static function trim_xml_header($xml,$encode=null)
	{
		if(strlen($xml) < 1)
		{
			return '';
		}
		if(strlen($encode) > 0 )
		{
			$xml = mb_convert_encoding($xml,'UTF-8', $encode);
		}
		$xml = preg_replace('/\<\?\s*xml\s+version\s*\=\s*(\"|\').*(\"|\')\s+encoding\s*\=\s*(\"|\')[a-z0-9-]+(\"|\')\s*\?\>/i', '', $xml);
		$xml = preg_replace('/\<\?\s*xml\s+encoding\s*\=\s*(\"|\')[a-z0-9-]+(\"|\')\s*\?\>/i', '', $xml);
		$xml = preg_replace('/\<\?\s*xml\s+version\s*\=\s*(\"|\').*(\"|\')\s*\?\>/i', '', $xml);
		//去除XML中的单引号
		// 		$xml = str_replace(array("\'"), "’", $xml);
		return $xml;
	}
	
	/**
     * @param int $ret 状态码
     * @param string $reason 状态描述
     * @param null $data 返回信息
     * @return array
     * @author xingcheng.hu
     * @date 2016/04/29
     */
	public static function return_data($ret = 1, $reason = "unknown error", $data = NULL)
	{
		return array (
				'ret' => $ret, 
				'reason' => $reason, 
				'data_info' => ($data != NULL) ? $data : NULL
		);
	}	
}
