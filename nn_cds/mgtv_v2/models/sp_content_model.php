<?php

class sp_content_model{
	static public function get_category_id_by_content_all($depot,$video_type=0,$sp_id=null){
		$array = array();
		$all_sp = sp_model::get_sp_list($sp_id);//获取所有要同步的运营商
		$db = nn_get_db(NL_DB_READ);		
		$content_category_id = $depot['category'];//cmd资源库ID
		foreach ($all_sp as  $value) {				
			$sp_org_id = $value['nns_id'];
			if($sp_id==$sp_org_id){
				$categoty = nl_db_get_all("select nns_id,nns_name from nns_mgtvbk_category where nns_org_id='{$sp_id}' and nns_video_type='$video_type' and bind_vod_item='{$content_category_id}'", $db);
				foreach ($categoty as  $categotyvalue) {
					$category_info = array('nns_id'=>$categotyvalue['nns_id'],
					'nns_org_id'=>$sp_id,'nns_name'=>$categotyvalue['nns_name']);
					$array[] = $category_info;
				}				
			}
		}
		return $array;
	}
	
		
	/**
	 * 根据注入栏目取注入到sp的栏目id
	 */
	static public function get_category_id_by_content($depot,$video_type=0,$now_sp_id=false){
		$array = array();
		$all_sp = sp_model::get_sp_list();//获取所有要同步的运营商
		$db = nn_get_db(NL_DB_READ);
		
		if($now_sp_id){
			$sp_org_id = $now_sp_id;
		}else{
			$sp_org_id = SP_ORG_ID;
		}
		$content_category_id = $depot['category'];//cmd资源库ID
		foreach ($all_sp as  $value) {				
			$sp_id = $value['nns_id'];
			if($sp_id==$sp_org_id){
				$categoty = nl_db_get_all("select nns_id,nns_name from nns_mgtvbk_category where nns_org_id='{$sp_id}' and nns_video_type='$video_type' and bind_vod_item='{$content_category_id}'", $db);
				foreach ($categoty as  $categotyvalue) {
					$category_info = array('nns_id'=>$categotyvalue['nns_id'],
					'nns_org_id'=>$sp_id,'nns_name'=>$categotyvalue['nns_name']);
					$array[] = $category_info;
				}				
			}
		}
		return $array;

	}
	/**
	 * 添加栏目
	 * $category_info = array(
		 'category_id'=>栏目ID,'category_name'=>栏目名称,'sp_id'=>运营商ID);
	 */
	static public function get_category($category_info){
		$db = nn_get_db(NL_DB_READ);
		//查询是否存在如果不存在则添加
		$sql = "select nns_id,nns_sp_category_id,nns_name,nns_org_id from nns_mgtvbk_category where nns_id='{$category_info['nns_id']}'
			and nns_sp_category_id='{$category_info['nns_sp_category_id']}' and nns_org_id='{$category_info['nns_org_id']}'";
		$result = nl_db_get_one($sql,$db);unset($sql);
		if(is_array($result) && !empty($result))
			return $result;
		$category_info['nns_create_time'] = date('Y-m-d H:i:s');
		unset($result);
		$result = nl_db_insert($db,'nns_mgtvbk_category',$category_info);
		if($result){
			return $category_info;
		}
	}


	static public function __get_category_all($id,$type){
		$db = nn_get_db(NL_DB_READ);	
		if(empty($type)||empty($id))
			return false;
		if($type=='vod'){
			$sql = "select * from nns_vod where nns_id='$id'";
			$result = nl_db_get_one($sql,$db);unset($sql);
			$org_id = $result['nns_org_id'];
			$category_id = $result['nns_category_id'];
			unset($result);
			$sql = "select * from nns_depot where nns_org_id='$org_id' and nns_org_type='0' and nns_type='0'";
			$result = nl_db_get_one($sql,$db);unset($sql);
			$depot_info = array('id'=>$result['nns_id'], 
				'category' => $category_id, 'org_id' => $org_id
			);
			$dom = new DOMDocument('1.0', 'utf-8');
			$dom->loadXML($result['nns_category']);
			$categorys = $dom->getElementsByTagName('category');
			foreach ($categorys as $category) {
				if ($category->getAttribute('id') === (string)$category_id) {
					$category_name = $category->getAttribute('name');
					$depot_info['name'] = $category_name;
					break;
				}
			}

			//$sql = "select group_concat(nns_id) from nns_vod_index where nns_vod_id='$id'";
			//$index = nl_db_get_col($sql,$db);
			//$index_arr = explode(',', $index);
			//$arr_tmp =  array('vod'=>$id,'index'=>$index_arr,'type'=>$type);
		}elseif($type=='index'){
			$sql = "select b.* from nns_vod_index a left join nns_vod b on a.nns_vod_id=b.nns_id  where a.nns_id='$id'";
			//die($sql);
			$result = nl_db_get_one($sql,$db);unset($sql);
			$org_id = $result['nns_org_id'];
			$category_id = $result['nns_category_id'];
			unset($result);
			$sql = "select * from nns_depot where nns_org_id='$org_id' and nns_org_type='0' and nns_type='0'";
			$result = nl_db_get_one($sql,$db);unset($sql);
			$depot_info = array('id'=>$result['nns_id'], 
				'category' => $category_id, 'org_id' => $org_id			
			);
			$dom = new DOMDocument('1.0', 'utf-8');
			$dom->loadXML($result['nns_category']);
			$categorys = $dom->getElementsByTagName('category');
			foreach ($categorys as $category) {
				if ($category->getAttribute('id') === (string)$category_id) {
					$category_name = $category->getAttribute('name');
					$depot_info['name'] = $category_name;
					break;
				}
			}
			//$arr_tmp =  array('index'=>$id,'type'=>$type);
		}elseif($type=='live'){
			$sql = "select * from nns_live where nns_id='$id'";
			$result = nl_db_get_one($sql,$db);unset($sql);
			$org_id = $result['nns_org_id'];
			$category_id = $result['nns_category_id'];
			unset($result);
			$sql = "select * from nns_depot where nns_org_id='$org_id' and nns_org_type='0' and nns_type='1'";
			$result = nl_db_get_one($sql,$db);unset($sql);
			$depot_info = array('id'=>$result['nns_id'], 
				'category' => $category_id, 'org_id' => $org_id
			);
			$dom = new DOMDocument('1.0', 'utf-8');
			$dom->loadXML($result['nns_category']);
			$categorys = $dom->getElementsByTagName('category');
			foreach ($categorys as $category) {
				if ($category->getAttribute('id') === (string)$category_id) {
					$category_name = $category->getAttribute('name');
					$depot_info['name'] = $category_name;
					break;
				}
			}			
			//$arr_tmp =  array();
		}elseif($type=='playbill'){
			$sql = "select b.* from nns_live_playbill_item a left join nns_live b on a.nns_live_id=b.nns_id  where a.nns_id='$id'";
			$result = nl_db_get_one($sql,$db);unset($sql);
			$org_id = $result['nns_org_id'];
			$category_id = $result['nns_category_id'];
			unset($result);
			$sql = "select * from nns_depot where nns_org_id='$org_id' and nns_org_type='0' and nns_type='1'";
			$result = nl_db_get_one($sql,$db);unset($sql);
			$depot_info = array('id'=>$result['nns_id'], 
				'category' => $category_id, 'org_id' => $org_id			
			);
			$dom = new DOMDocument('1.0', 'utf-8');
			$dom->loadXML($result['nns_category']);
			$categorys = $dom->getElementsByTagName('category');
			foreach ($categorys as $category) {
				if ($category->getAttribute('id') === (string)$category_id) {
					$category_name = $category->getAttribute('name');
					$depot_info['name'] = $category_name;
					break;
				}
			}
			//$arr_tmp =  array();
		}
		return array('depot_info'=>$depot_info);
	}

	/**
	 * $id 视频ID或则分集ID
	 * $type (vod,index)
	 */
	static public function category_content($id,$type,$video_type=0)
	{
			
		$depot_info = self::__get_category_all($id,$type);
		$category_all = self::get_category_id_by_content($depot_info['depot_info'],$video_type);
		if(!is_array($category_all)&&count($category_all)==0)
			return false;
		category_content_model::_task_c2($category_all,$id,$type);
		return true;
	}

	/**
	 * 删除操作
	 * $id 视频ID或则分集ID
	 * $type (vod,index)
	 */
	static public function delete_category_content($id,$type){
		$depot_info = self::__get_category_all($id,$type);
		$category_all = self::get_category_id_by_content($depot_info['depot_info']);
		if(!is_array($category_all)&&count($category_all)==0)
			return false;
		category_content_model::_task_c2_del($category_all,$id,$type);
		return true;
	}

	
	/**
	 * 获取栏目的条数
	 * $filter = array(
		'sp_id'=>$sp_id,
		'category_id'=>$category_id,
		'search'=>$search,
		'day_picker_start'=>$day_picker_start,
		'day_picker_end'=>$day_picker_end,
	);
	 **/
	static public function explode_vod($filter){
		$db = nn_get_db(NL_DB_READ);
		$sp_id = $filter['sp_id'];
		$category_id = $filter['category_id'];
		$search = $filter['search'];
		$day_picker_start = $filter['day_picker_start'];
		$day_picker_end = $filter['day_picker_end'];
		if($category_id==10000){//root栏目
			$sql = "select * from nns_mgtvbk_category_content where nns_video_type=0 and nns_status=1 and nns_org_id='$sp_id' ";
		}else{
			$sql = "select * from nns_mgtvbk_category_content where nns_video_type=0 and nns_status=1 and nns_org_id='$sp_id' and nns_category_id like '$category_id%'";
		}
		if($search){
			$sql .= " and (nns_video_name like '%$search%' or nns_pinyin like '%$search%')";
		}
		if($day_picker_start){
			$sql .= " and nns_create_time>='{$day_picker_start}'";
		}
		if($day_picker_end){
			$sql .= " and nns_create_time<='{$day_picker_end}'";
		}
		$re = nl_db_get_all($sql,$db);
		if(count($re)>1){			
			$str = "ID,TITLE,CATEGORY,DIRECTOR,ACTOR,AREA,KEYWORD,SUMMARY\r\n";
			foreach ($re as $item) {				
				$category_id = $item['nns_category_id'];
				$nns_id = $item["nns_video_id"];				
				$sql = "select nns_name from nns_mgtvbk_category where nns_id='$category_id'";
				$category_name = nl_db_get_col($sql, $db);unset($sql);	
				$sql = "select * from nns_vod where nns_id='$nns_id'";
				$vod = nl_db_get_one($sql, $db);unset($sql);
				$str .= "$nns_id,".$vod['nns_name'].",$category_name,".$vod['nns_director'].",".$vod['nns_actor'].",".$vod['nns_area'].",".$vod['nns_keyword'].",".$vod['nns_summary']."\r\n";
			}
			$sub_path = date('Y-m-d');
			$file = "影片注入".date('His').".csv";
			$file_path = dirname(dirname(dirname(__FILE__))).'/data/mgtv/fjyd/csv/'.$sub_path.'/';
			if(!is_dir($file_path)) mkdir($file_path,0777,true);
			$file_path .= $file;
			file_put_contents($file_path,$str);
			return array('url'=>'../../../data/mgtv/fjyd/csv/'.$sub_path.'/'.$file);
		}
		
	}
	
	/**
	 * 获取栏目的条数
	 * $filter = array(
		'sp_id'=>$sp_id,
		'category_id'=>$category_id,
		'search'=>$search,
		'day_picker_start'=>$day_picker_start,
		'day_picker_end'=>$day_picker_end,
	);
	 **/
	static public function get_category_count($filter){
		$db = nn_get_db(NL_DB_READ);
		$sp_id = $filter['sp_id'];
		$category_id = $filter['category_id'];
		$search = $filter['search'];
		$day_picker_start = $filter['day_picker_start'];
		$day_picker_end = $filter['day_picker_end'];
		if($category_id==10000){//root栏目
			$sql = "select count(*) from nns_mgtvbk_category_content where nns_org_id='$sp_id' ";
		}else{
			$sql = "select count(*) from nns_mgtvbk_category_content where nns_org_id='$sp_id' and nns_category_id like '$category_id%'";
		}
		if($search){
			$sql .= " and (nns_video_name like '%$search%' or nns_pinyin like '%$search%')";
		}
		if($day_picker_start){
			$sql .= " and nns_create_time>='{$day_picker_start}'";
		}
		if($day_picker_end){
			$sql .= " and nns_create_time<='{$day_picker_end}'";
		}		
		return nl_db_get_col($sql,$db);
	}
	
	
	/**
	 * $filter = array(
		'sp_id'=>$sp_id,
		'category_id'=>$category_id,
		'search'=>$search,
		'day_picker_start'=>$day_picker_start,
		'day_picker_end'=>$day_picker_end,
		'offset'=>$offset,
		'limit'=>$limit,
	);
	 */
	static public function get_vodlist($filter)
	{			
		$sp_id = $filter['sp_id'];
		$category_id = $filter['category_id'];
		$search = $filter['search'];
		$day_picker_start = $filter['day_picker_start'];
		$day_picker_end = $filter['day_picker_end'];
		$offset = $filter['offset'];
		$limit = $filter['limit'];
			
		$db = nn_get_db(NL_DB_READ);
		if($category_id==10000){//root栏目
			$sql = "select * from nns_mgtvbk_category_content where nns_org_id='$sp_id'";
		}else{
			$sql = "select * from nns_mgtvbk_category_content where nns_org_id='$sp_id' and nns_category_id like '$category_id%'";
		}		
		if($search){
			$sql .= " and (nns_video_name like '%$search%' or nns_pinyin like '%$search%')";
		}
		if($day_picker_start){
			$sql .= " and nns_create_time>='{$day_picker_start}'";
		}
		if($day_picker_end){
			$sql .= " and nns_create_time<='{$day_picker_end}'";
		}
		$sql .= " order by nns_create_time desc limit $offset,$limit";
		$re =  nl_db_get_all($sql,$db);
		if(count($re)>0&&is_array($re)){
			foreach ($re as $key => $value) {
				//nns_all_index
				if($value['nns_video_type']=='0'){
					$nns_all_index = nl_db_get_col("select nns_all_index from nns_vod where nns_id='{$value['nns_video_id']}'",$db);
					if(empty($nns_all_index)){
						$nns_all_index=0;
					}
				}else{
					$nns_all_index = 1;
				}
				$re[$key]['nns_all_index'] = $nns_all_index;
			}
		}
		return $re;
	}
	
	
	
	/*在详细页面获取分机和切片的状态*/
	
	static public function get_status_in_vod_detail($vod_id,$vod_index_id,$spid)
	{
		$db = nn_get_db(NL_DB_READ);
		$sql="select * from nns_mgtvbk_c2_task where nns_type='index' and nns_ref_id='$vod_index_id' and nns_src_id='$vod_id' and nns_org_id='$spid'";
		$re= nl_db_get_one($sql, $db);unset($sql);
		if(empty($re)){//如果没有先在数据库中插入数据
			self::category_content($vod_index_id, 'index');
			$sql="select * from nns_mgtvbk_c2_task where nns_type='index' and nns_ref_id='$vod_index_id' and nns_src_id='$vod_id' and nns_org_id='$spid'";
			$re= nl_db_get_one($sql, $db);unset($sql);
		}		
		$id = $re['nns_id'];
		$rs = category_content_model::get_status($id);
		$rs['nns_id'] = $id;
		return $rs;
	}
	
	
	static public function get_content_info($filter,$offset,$limit){
		$db = nn_get_db(NL_DB_READ);
		$data = array();
		$nns_id = $filter['nns_id'];
		$sql = "select * from nns_mgtvbk_category_content where nns_id='{$nns_id}'";
		$data['info'] = nl_db_get_one($sql, $db);unset($sql);//影片的基本信息		
		$index_num = $filter['nns_index_all'];
		$org_id = $filter['sp_id'];
		$vod_id = $filter['nns_video_id'];
		$sql = "select SQL_CALC_FOUND_ROWS * from nns_mgtvbk_c2_task where nns_type='index' and nns_org_id='$org_id' and nns_src_id='$vod_id' limit $offset,$limit";
		//echo $sql;
		$task_all = nl_db_get_all($sql, $db);unset($sql);
		$count_sql  = "SELECT FOUND_ROWS() ";
    	$rows = nl_db_get_col($count_sql,$db);
		if(is_array($task_all)&&count($task_all)>0){			
			foreach ($task_all as $key => $value) {
				//查询切片完成情况	
				$sql="select nns_state,nns_id from nns_mgtvbk_clip_task where nns_org_id='$org_id' and nns_video_id='$vod_id' and nns_video_index_id='{$value['nns_ref_id']}' order by nns_create_time desc limit 1";
				$nns_state = nl_db_get_one($sql, $db);unset($sql);				
				//查询切片数据同步状态
				$sql="select nns_result,nns_notify_result,nns_again from nns_mgtvbk_c2_log where nns_org_id='$org_id' and nns_task_id='{$nns_state['nns_id']}' and nns_task_type='Movie' order by nns_create_time desc limit 1";
				$state = nl_db_get_one($sql, $db);unset($sql);
				$task_all[$key]['clip_state']=$nns_state['nns_state'];
				$task_all[$key]['movie_nns_result']=$state['nns_result'];
				$task_all[$key]['movie_nns_notify_result']=$state['nns_notify_result'];
				$task_all[$key]['movie_nns_again']=$state['nns_again'];
			}			
		}
		$data['index_list'] = $task_all;
		$data['rows'] = $rows;
		return $data;
	}
	
	//查询单片的注入情况
	static public function get_import_log($sp_id,$method,$vod_id,$index_num=0,$index_id=0){
		$db = nn_get_db(NL_DB_READ);
		if($method=='vod'){
			if($index_num==1){
				$nns_type='index';
				$obj = "Program";
			}elseif($index_num>1){
				$nns_type='vod';
				$obj = "Series";
			}			
			$sql="select * from nns_mgtvbk_c2_task where nns_type = '$nns_type' and nns_org_id = '$sp_id'  and  ";
			if($nns_type=='index'){
				$sql.="nns_src_id='$vod_id'";
			}elseif($nns_type=='vod'){
				$sql.="nns_ref_id='$vod_id'";
			}			
		}elseif($method=='index'){
			$nns_type='index';
			$obj = "Program";
			$sql="select * from nns_mgtvbk_c2_task where nns_type = '$nns_type' and nns_org_id = '$sp_id'  and  nns_ref_id='$index_id' and nns_src_id='$vod_id'";
		}
		$c2_task = nl_db_get_one($sql, $db);unset($sql);
		$task_id = $c2_task['nns_id'];
		$sql="select * from nns_mgtvbk_c2_log where nns_org_id='$sp_id' and nns_task_type='$obj' and nns_task_id='$task_id' order by nns_create_time desc";
		return  nl_db_get_all($sql, $db);
	}
	
	
	
	
	static public function move_vod_arr($params) {
		$vod = explode(',', $_GET['nns_id']);
		$vad = array_filter($vod);
		foreach ($vad as $value) {
			$params['nns_id'] = $value;
			self::move_vod($params);
		}
		return 1;
	}

	/**
	 * $params = array(
	 'sp_id'=>$_GET['sp_id'],
	 'nns_id'=>$_GET['nns_id'],
	 'category_id'=>$_GET['category_id'],
	 'category_name'=>$_GET['category_name'],
	 );
	 */
	static public function move_vod($params) {
		$db = nn_get_db(NL_DB_WRITE);
		$db_r = nn_get_db(NL_DB_READ);
		$sql = "select * from nns_mgtvbk_category_content where nns_id in ('{$params['nns_id']}')";
		$result = nl_db_get_one($sql, $db);
		unset($sql);
		$id = np_guid_rand();		
		if(empty($result['nns_all_index'])){			
			$result['nns_all_index'] = 0;
		}
		$now_time = date('Y-m-d H:i:s');
		$sql = "insert into nns_mgtvbk_category_content (nns_id,nns_org_id,nns_category_id,nns_category_name,nns_video_id,nns_video_name,nns_create_time,nns_modify_time,nns_all_index,nns_pinyin,nns_video_type)" . "values ('{$id}','{$params['sp_id']}','{$params['category_id']}','{$params['category_name']}','{$result['nns_video_id']}','{$result['nns_video_name']}','{$now_time}','{$now_time}','{$result['nns_all_index']}','{$result['nns_pinyin']}','{$result['nns_video_type']}')";		
		$re = nl_execute_by_db($sql, $db);
		unset($sql);
		$vod_id = $result['nns_video_id'];
		$sp_id = $params['sp_id'];
		$sql = "select * from nns_mgtvbk_c2_task where nns_type='vod' and nns_org_id='$sp_id' and nns_ref_id='$vod_id'";
		$result_c2_task = nl_db_get_all($sql, $db);
		unset($sql);
		foreach ($result_c2_task as $value) {			
			if ($value['nns_action'] == 'add') {
				$nns_action = 'add';
				$nns_status = SP_C2_WAIT;
			} elseif ($value['nns_action'] == 'modify') {
				$nns_action = 'modify';
				$nns_status = SP_C2_WAIT;
			} elseif ($value['nns_action'] == 'destroy') {
				$nns_action = 'modify';
				$nns_status = SP_C2_WAIT;
			}
			$sql = "update nns_mgtvbk_c2_task set nns_action='$nns_action',nns_status=$nns_status where nns_id='{$value['nns_id']}'";
			nl_execute_by_db($sql, $db);
			unset($sql);
		}
		return true;
	}
	
	
	
	
	
	
	
	
	
	

}
