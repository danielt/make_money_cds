<?php
include_once dirname(dirname(__FILE__)) . '/import/import.class.php';
include_once dirname(__FILE__) . '/public_model_exec.class.php';
include_once dirname(__FILE__) . '/queue_task_model.php';
class import_model
{
	static public $arr_sp_model = array();
	
	static public $arr_error_data = array();
	
	static public $arr_image_playbill_type = array(
	    'nns_image0'=>1,
	    'nns_image1'=>2,
	    'nns_image2'=>3,
	    'nns_image_v'=>5,
	    'nns_image_s'=>4,
	    'nns_image_h'=>6,
	);
	
	/**
	 * 反馈错误信息
	 * @param unknown $data_info
	 */
	static public function return_error_data()
	{
		$data = array(
				'ret'=>1,
				'reason'=>var_export(self::$arr_error_data,true),
		);
		self::$arr_error_data=array();
		return $data;
	}
	
	/**
	 * 反馈正确信息
	 * @param unknown $data_info
	 * @return multitype:number unknown mixed
	 */
	static public function return_right_data($data_info)
	{
		$data = array(
				'ret'=>0,
				'reason'=>'sucess',
				'data_info'=>$data_info
		);
		return $data;
	}
	
	static public function log_write($nns_func, $data, $log_summary, $nns_state)
	{
		$db = nn_get_db(NL_DB_WRITE);
		$table = 'nns_mgtvbk_import_log';
		$import_data = "<import_assets/>";
		if ($nns_func == 'import_assets')
		{
			$import_data = $data['assets_content'];
		}
		elseif ($nns_func == 'modify_video' || $nns_func == 'modify_asset' || $nns_func == 'line_asset')
		{
			$import_data = $data['assets_video_info'];
		}
		elseif ($nns_func == 'import_video_clip')
		{
			$import_data = $data['assets_clip_info'];
		}
		elseif ($nns_func == 'modify_video_clip')
		{
			$import_data = $data['assets_clip_info'];
		}
		elseif ($nns_func == 'import_video_file')
		{
			$import_data = $data['assets_file_info'];
		}
		//print_r($nns_func);
		//print_r($import_data);
		//print_r($data);
		

		$video_name = "";
		if ($nns_state == 0)
		{
			if ($data['assets_id'])
			{
				$sql = "select nns_name from nns_vod where nns_asset_import_id='{$data['assets_id']}'";
				$video_name = nl_db_get_col($sql, $db);
			}
			elseif ($data['assets_clip_id'])
			{
				$sql = "select nns_name from nns_vod_index where nns_import_id='{$data['assets_clip_id']}'";
				$video_name = nl_db_get_col($sql, $db);
				if(isset($data['seekpoint']))
				{
					$import_data .= $data['seekpoint'];
				}
			}
			elseif ($data['assets_file_id'])
			{
				$sql = "select b.nns_name from nns_vod_media a  left join nns_vod_index b on a.nns_vod_index_id=b.nns_id where a.nns_import_id='{$data['assets_file_id']}'";
				$video_name = nl_db_get_col($sql, $db);
			}
		}
		else
		{
			if ($data['assets_id'])
			{
				$obj = simplexml_load_string($import_data);
				$asset_info = json_decode(json_encode($obj), true);
				$video_name = $asset_info["@attributes"]["name"];
			}
			elseif ($data['assets_clip_id'])
			{
				$obj = simplexml_load_string($import_data);
				$asset_info = json_decode(json_encode($obj), true);
				$video_name = $asset_info["@attributes"]["name"];
				if(isset($data['seekpoint']))
				{
					$import_data .= $data['seekpoint'];
				}
			}
			elseif ($data['assets_file_id'])
			{
				$sql = "select nns_name from nns_vod_index where nns_import_id='{$data['assets_clip_id']}'";
				//@error_log($sql,3,'e:\log.txt');
				$video_name = nl_db_get_col($sql, $db);
			}
		}
		
		$xml_data = $import_data = str_replace("\\", "", $import_data);
		if(strlen($import_data) >0)
		{
		    $dom = new DOMDocument('1.0', 'utf-8');
		    $dom->loadXML($import_data);
		    $xml_data = $dom->saveXML();
		}
		$data = array (
				'nns_id' => np_guid_rand(), 
				//htmlspecialchars($string, ENT_QUOTES) 
				'nns_func' => $nns_func, 
				'nns_import_data' => addslashes($xml_data), 
				'nns_state' => $nns_state, 
				'nns_summary' => addslashes($log_summary), 
				'nns_assets_id' => $data['assets_id'], 
				'nns_assets_video_type' => $data['assets_video_type'], 
				'nns_assets_clip_id' => $data['assets_clip_id'], 
				'nns_assets_file_id' => $data['assets_file_id'], 
				'nns_svc_item_id' => $data['svc_item_id'], 
				'nns_create_time' => date('Y-m-d H:i:s'), 
				'nns_org_id' => 'fjyd', 
				'nns_name' => $video_name
		);
		nl_db_insert($db, $table, $data);
	}

	static public function get_log_list($filter, $start, $size)
	{
		$db = nn_get_db(NL_DB_READ);
		$sql = "select  * from nns_mgtvbk_import_log where nns_org_id='fjyd'";
		if ($filter != null)
		{
			$wh_arr = array ();
			if (!empty($filter['nns_state']))
			{
				if ($filter['nns_state'] == '1')
				{
					$wh_arr[] = " nns_state!=0";
				}
				elseif ($filter['nns_state'] == '2')
				{
					$wh_arr[] = " nns_state=0";
				}
			}
			if (!empty($filter['nns_func']))
			{
				$wh_arr[] = " nns_func='" . $filter['nns_func'] . "'";
			}
			if (!empty($filter['nns_name']))
			{
				$wh_arr[] = " nns_name like '%" . $filter['nns_name'] . "%' ";
			}
			if (!empty($filter['nns_assets_id']))
			{
				$wh_arr[] = " nns_assets_id='" . $filter['nns_assets_id'] . "' ";
			}
			if (!empty($filter['nns_assets_clip_id']))
			{
				$wh_arr[] = " nns_assets_clip_id='" . $filter['nns_assets_clip_id'] . "' ";
			}
			if (!empty($filter['nns_assets_file_id']))
			{
				$wh_arr[] = " nns_assets_file_id='" . $filter['nns_assets_file_id'] . "' ";
			}
			if (!empty($filter['day_picker_start']))
			{
				$wh_arr[] = " nns_create_time >='" . $filter['day_picker_start'] . "' ";
			}
			if (!empty($filter['day_picker_end']))
			{
				$wh_arr[] = " nns_create_time <='" . $filter['day_picker_end'] . "' ";
			}
			if (!empty($wh_arr))
				$sql .= " and " . implode(" and ", $wh_arr);
		}
		
		$count_sql = str_replace('*', ' count(*) as temp_count ', $sql);
		$sql .= " order by nns_create_time desc ";
		$sql .= " limit $start,$size";
		$data = nl_db_get_all($sql, $db);
		//$count_sql  = "SELECT FOUND_ROWS() ";
		$rows = nl_db_get_col($count_sql, $db);
		return array (
				'data' => $data, 
				'rows' => $rows
		);
	}

	static public function get_log_detail_by_id($id)
	{
		$db = nn_get_db(NL_DB_READ);
		$sql = "select nns_import_data from nns_mgtvbk_import_log where nns_id='" . $id . "'";
		return nl_db_get_col($sql, $db);
	}
	
	//自动注入
	static public function auto_import_epg()
	{
	/***********
		$db = nn_get_db(NL_DB_READ);
		$num = 300;
		$sql = "select nns_id,nns_ref_id,nns_src_id,nns_action,nns_status from nns_mgtvbk_c2_task where nns_type='media' and nns_org_id='jllt' and nns_status=0 order by nns_create_time asc limit $num";
		$c2_task = nl_db_get_all($sql, $db);
		if(count($c2_task)==0){
			die('fail');
		}
		$query = array('nns_action'=>'add');
		foreach ($c2_task as $key => $value) {
			$vod_id = nl_db_get_col("select nns_vod_id from nns_vod_index where nns_id='{$value['nns_src_id']}'", $db);
			if(empty($vod_id)){
				continue;
			}
			$sql = "select nns_id,nns_action from nns_mgtvbk_c2_task where nns_type='vod' and nns_org_id='jllt' and nns_ref_id='{$vod_id}'";
			$c2_task_vod = nl_db_get_one($sql, $db);
			if(is_array($c2_task_vod)&&!empty($c2_task_vod)){
				//先执行主媒体注入
				if($c2_task_vod['nns_action']!='destroy'){
					$c2_task_vod['nns_action'] = 'add';
				}
				$re_vod = self::import_asset($c2_task_vod['nns_id'],$c2_task_vod['nns_action']);
				if($re_vod){
					//注入分集
					$sql = "select nns_id,nns_action from nns_mgtvbk_c2_task where nns_type='index' and nns_org_id='jllt' and nns_src_id='{$vod_id}' and nns_ref_id='{$value['nns_src_id']}'";
					$c2_task_index = nl_db_get_one($sql, $db);
					if(is_array($c2_task_index)&&!empty($c2_task_index)){
						if($c2_task_index['nns_action']!='destroy'){
							$c2_task_index['nns_action'] = 'add';
						}
						$re_index = self::import_clip($c2_task_index['nns_id'],$c2_task_index['nns_action']);
						if($re_index){
							if($value['nns_action']!='destroy'){
								$value['nns_action'] = 'add';
							}
							$re_vod_media = self::import_file($value['nns_id'],$value['nns_action']);
						}
					}
				}
			}
		}***/
	}

	/**
	 * @下发epg 
	 */
	static public function import_epg($query)
	{

		$db = nn_get_db(NL_DB_READ);
		
		if (empty($query))
		{
			return false;
		}
		$nns_id = array_filter(explode(',', $query['nns_id']));
		$re = false;
		foreach ($nns_id as $id)
		{
			$sql = "select nns_type from nns_mgtvbk_c2_task where nns_id='$id'";
			$info = nl_db_get_one($sql, $db);
			if (empty($info))
			{
				continue;
			}
			if ($info['nns_type'] == 'video')
			{
				$re = self::import_asset($id);
			}
			elseif ($info['nns_type'] == 'index')
			{
				$re = self::import_clip($id);
			}
			elseif ($info['nns_type'] == 'media')
			{
				$re = self::import_file($id);
			}
			elseif ($info['nns_type'] == 'live')
			{
				$re = self::import_channel($id);
			}
			elseif ($info['nns_type'] == 'live_media')
			{
				$re = self::import_live($id);
			}
			elseif ($info['nns_type'] == 'playbill')
			{
				$re = self::import_playbill($id);
			}
			else
			{
				continue;
			}
		}
		return $re;
	}

	static public function import_file($nns_id, $action_epg = null)
	{
//		$db_r = nn_get_db(NL_DB_READ);
//		$db_w = nn_get_db(NL_DB_WRITE);
		$dc = nl_get_dc(array(
		    'db_policy' =>  NL_DB_READ | NL_DB_WRITE,
		    'cache_policy' => NP_KV_CACHE_TYPE_NULL
		   )
		);
		$action = 'add';
		$sql = "select * from nns_mgtvbk_c2_task where nns_id='$nns_id'";
		$c2_task_info = nl_db_get_one($sql, $dc->db());
		if (empty($c2_task_info))
		{
			return false;
		}
		if ($c2_task_info['nns_type'] != 'media')
		{
			return false;
		}
		if (!empty($c2_task_info))
		{
			$action = $c2_task_info['nns_action'];
		}
		$sp_id = $c2_task_info['nns_org_id'];
		
		//注入片源之前   先注入分集
		

		$index_c2_info = nl_db_get_one("select * from nns_mgtvbk_c2_task where nns_org_id='$sp_id' and  nns_type='index' and nns_ref_id='{$c2_task_info['nns_src_id']}'", $dc->db());
		if ($index_c2_info['nns_action'] != 'destroy')
		{
			//self::import_clip($index_c2_info['nns_id']);
		}
		
		include_once dirname(dirname(__FILE__)) . '/' . $sp_id . '/define.php';
		$url = IMPROT_EPG_URL;
		$log = IMPROT_EPG_LOG;
			
		$sql = "select * from nns_vod_media where nns_id='{$c2_task_info['nns_ref_id']}'";
		$info = nl_db_get_one($sql, $dc->db());
		$cp_id = isset($info['nns_cp_id']) ? $info['nns_cp_id'] : null;
		// 获取CP配置
		$result_cp = nl_cp::query_by_id($dc, $cp_id);
		$cp_config = json_decode($result_cp['data_info']['nns_config'], true);
		
		$import_obj = new import($url, $log,'',$cp_id);
		//if(isset($info['nns_deleted'])&& (int)$info['nns_deleted']==1){
		//	$action = 'destroy';
		//}


		if (empty($info) || $info['nns_deleted'] == 1)
		{
			$action == 'destroy';
			nl_execute_by_db("update nns_mgtvbk_c2_task set nns_action='destroy',nns_epg_status=97 where nns_id='$nns_id'", $dc->db());
		}
		
		if (empty($c2_task_info['nns_ref_id']))
		{
			//nl_execute_by_db("delete from nns_mgtvbk_c2_task where nns_id='$nns_id'", $db_w);
			return false;
		}
		
		@error_log($action . "\r\n", 3, $log . '/' . date('Ymd') . '.txt');
		
		
		
		$sp_config = sp_model::get_sp_config($sp_id);
		$video_type = "media";
		//定制化贵州同洲逻辑
		if($sp_id == 'gzgd_tz_sp' && $action != 'destroy')
		{
			$video_info = video_model::get_vod_media_info($c2_task_info['nns_ref_id']);
			nn_log::write_log_message(LOG_MODEL_CMS,"========同洲定制化检查开始=========检查片源ID为:".$c2_task_info['nns_ref_id'],$sp_id,$video_type,'');
			//查询其他CP的片源是否注入EPG
			//step one 获取其他cp的片源GUID
			$media_sql = "select nns_id from nns_vod_media where nns_import_id='{$video_info['nns_import_id']}' and nns_cp_id!='{$info['nns_cp_id']}'";
			nn_log::write_log_message(LOG_MODEL_CMS,"获取其他CP的片源SQL为:" . $media_sql,$sp_id,$video_type,'');
			$media_id_info = nl_query_by_db($media_sql, $dc->db());
			if(is_array($media_id_info))
			{
			$ids_info = array();
			foreach ($media_id_info as $ids_i)
			{
				$ids_info[] = $ids_i['nns_id'];
			}
			if(count($ids_info) > 1) 
			{
				$ids_info = implode("','", $ids_info);
			}
			else 
			{
				$ids_info = $ids_info[0];
			}
			//step two 查询C2中是否存在
			$check_c2_sql = "select nns_epg_status,nns_file_path,nns_cdn_policy from nns_mgtvbk_c2_task where nns_type='media' and nns_ref_id in ('{$ids_info}')";
			nn_log::write_log_message(LOG_MODEL_CMS,"获取C2中是否存在的SQL为:" . $check_c2_sql,$sp_id,$video_type,'');
			$other_c2 = nl_db_get_one($check_c2_sql, $dc->db());
			if(!is_array($other_c2))//C2中不存在
			{
				nn_log::write_log_message(LOG_MODEL_CMS,"C2中没有数据",$sp_id,$video_type,'');
				//step three c2中没有数据，到同步指令中去查
				$check_sync_sql = "select nns_id from nns_mgtvbk_op_queue where nns_media_id in ('{$ids_info}') and nns_type='media'";
				nn_log::write_log_message(LOG_MODEL_CMS,"中心同步指令中是否存在的查询SQL为:".$check_sync_sql,$sp_id,$video_type,'');
				$check_sync = nl_db_get_one($check_sync_sql, $dc->db());
				if(is_array($check_sync))
				{
					nn_log::write_log_message(LOG_MODEL_CMS,"中心同步指令中有数据".var_export($check_sync,true),$sp_id,$video_type,'');
					return false;
				}
				else 
				{
					//step four 中心同步指令中没有，检查中心注入指令
					$check_op_sql = "select * from nns_mgtvbk_import_op where nns_video_id in ('{$ids_info}') and nns_video_type='media'";
					nn_log::write_log_message(LOG_MODEL_CMS,"中心注入指令令中是否存在的查询SQL为:".$check_op_sql,$sp_id,$video_type,'');
					$check_op = nl_db_get_one($check_op_sql, $dc->db());
					if(is_array($check_op))
					{
						nn_log::write_log_message(LOG_MODEL_CMS,"中心同步指令中有数据".var_export($check_op,true),$sp_id,$video_type,'');
						return false;
					}
					nn_log::write_log_message(LOG_MODEL_CMS,"中心同步指令中没有数据,则是存量数据可直接注入EPG",$sp_id,$video_type,'');
				}
			}
			elseif(is_array($other_c2) && $other_c2['nns_epg_status'] != '99')//C2中存在且则EPG还未注入成功
			{
				nn_log::write_log_message(LOG_MODEL_CMS,"C2中有数据，但还未注入EPG",$sp_id,$video_type,'');
				return false;
			}
			elseif (is_array($other_c2) && $other_c2['nns_epg_status'] == '99')//EPG注入成功,则同洲的下发 
			{
				$c2_task_info['nns_file_path'] = $other_media['nns_file_path'];
				$c2_task_info['nns_cdn_policy'] = $other_media['nns_cdn_policy'];
			}
			}
			else
			{
				//其他CP没有数据，则可能是同洲的数据，属于存量
				nn_log::write_log_message(LOG_MODEL_CMS,"注入的是存量任务",$sp_id,$video_type,'');
			}
			nn_log::write_log_message(LOG_MODEL_CMS,"========同洲定制化检查结束=========检查片源ID为:".$c2_task_info['nns_ref_id'],$sp_id,$video_type,'');
		}
		//media
		$arr_media_info = video_model::get_vod_media_info($c2_task_info['nns_ref_id']);
		$result_media_import_info = public_model_exec::get_asset_id_by_sp_id($sp_id, 'media', $c2_task_info['nns_ref_id'],$arr_media_info,$sp_config,'epg');
		if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
		{
		    return false;
		}
		$info['nns_id'] = $result_media_import_info['data_info'];
		//index
		$arr_index_info = video_model::get_vod_index_info($info['nns_vod_index_id']);
		$result_index_import_info = public_model_exec::get_asset_id_by_sp_id($sp_id, 'index', $info['nns_vod_index_id'],$arr_index_info,$sp_config,'epg');
		if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
		{
		    return false;
		}
		$info['nns_vod_index_id'] = $result_index_import_info['data_info'];
        $arr_video_info = video_model::get_vod_info($info['nns_vod_id']);
        //单剧集注入  判断是否为花絮、预告片也当作单剧集注入
        if(($arr_video_info['nns_all_index'] == -1 && (int)$sp_config['epg_single_index_import_model'] === 1)||(isset($arr_video_info['nns_ishuaxu']) && $arr_video_info['nns_ishuaxu'] != 0 && $arr_video_info['nns_ishuaxu'] != 2 && $arr_video_info['nns_ishuaxu'] != 3))
        {
            $info['nns_vod_id'] = '';
        }
        else
        {
			//vod
			$result_video_import_info = public_model_exec::get_asset_id_by_sp_id($sp_id, 'video', $info['nns_vod_id'],$arr_video_info,$sp_config,'epg');
			if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
			{
				return false;
			}
            $info['nns_vod_id'] = $result_video_import_info['data_info'];
        }
		
        $index_nns_ext_url = (isset($arr_index_info['nns_ext_url']) && strlen($arr_index_info['nns_ext_url'])>0) ? json_decode($arr_index_info['nns_ext_url'],true) : null;
        if(strlen($index_nns_ext_url['sequence']) > 0 )
        {
            $info['nns_vod_id'] = '';
        }
// 		if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0)
// 		{
// 			$video_info = video_model::get_vod_media_info($c2_task_info['nns_ref_id']);
// 			switch ($sp_config['import_id_mode']) {				
// 				case '1':					
// 					$vod_id = '002000'.$video_info['nns_integer_id'];
// 					$info['nns_id'] = str_pad($vod_id,32,"0");
// 					break;
// 				case '2':
// 					$info['nns_id'] = $video_info['nns_import_id'];
// 					break;
// 				case '3':					
// 					$vod_id = CSP_ID.'002'.$video_info['nns_integer_id'];
// 					$info['nns_id'] = str_pad($vod_id,32,"0");
// 					break;
// 				case '4':
// 					$arr_csp_id = explode("|", CSP_ID);
// 					$str_lenth = strlen($arr_csp_id[0])+strlen($arr_csp_id[1])+1;
// 					$lest_num = 32-$str_lenth;
// 					$info['nns_id'] = $arr_csp_id[0].'3'.str_pad($video_info['nns_integer_id'], $lest_num, "0", STR_PAD_LEFT).$arr_csp_id[1];
// 					break;
// 			}
			
// 		}
// 		//clip
// 		if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0)
// 		{
// 			$video_info = video_model::get_vod_index_info($info['nns_vod_index_id']);
// 			switch ($sp_config['import_id_mode']) 
// 			{				
// 				case '1':					
// 					$vod_id = '001000'.$video_info['nns_integer_id'];
// 					$info['nns_vod_index_id'] = str_pad($vod_id,32,"0");
// 					break;
// 				case '2':
// 					$info['nns_vod_index_id'] = $video_info['nns_import_id'];
// 					break;
// 				case '3':					
// 					$vod_id = CSP_ID.'001'.$video_info['nns_integer_id'];
// 					$info['nns_vod_index_id'] = str_pad($vod_id,32,"0");
// 					break;
// 				case '4':
// 					$arr_csp_id = explode("|", CSP_ID);
// 					$str_lenth = strlen($arr_csp_id[0])+strlen($arr_csp_id[1])+1;
// 					$lest_num = 32-$str_lenth;
// 					$info['nns_vod_index_id'] = $arr_csp_id[0].'2'.str_pad($video_info['nns_integer_id'], $lest_num, "0", STR_PAD_LEFT).$arr_csp_id[1];
// 					break;
// 			}
// 		}
// 		//vod
// 		$vod_info = video_model::get_vod_info($info['nns_vod_id']);
// 		if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0)
// 		{		
// 			switch ($sp_config['import_id_mode']) 
// 			{				
// 				case '1':					
// 					$vod_id = '000000'.$vod_info['nns_integer_id'];
// 					$info['nns_vod_id'] = str_pad($vod_id,32,"0");
// 					break;
// 				case '2':
// 					$info['nns_vod_id'] = $vod_info['nns_asset_import_id'];
// 					break;
// 				case '3':					
// 					$vod_id = CSP_ID.'000'.$vod_info['nns_integer_id'];
// 					$info['nns_vod_id'] = str_pad($vod_id,32,"0");
// 					break;
// 				case '4':
// 					$arr_csp_id = explode("|", CSP_ID);
// 					$str_lenth = strlen($arr_csp_id[0])+strlen($arr_csp_id[1])+1;
// 					$lest_num = 32-$str_lenth;
// 					$info['nns_vod_id'] = $arr_csp_id[0].'1'.str_pad($vod_info['nns_integer_id'], $lest_num, "0", STR_PAD_LEFT).$arr_csp_id[1];
// 					break;
// 			}
			
// 		}
		if (isset($cp_config['original_cp_enabled']) && (int)$cp_config['original_cp_enabled'] === 1)
		{
			$import_obj->cp_id = $arr_video_info['nns_producer'];
		}
		elseif (isset($cp_config['original_cp_enabled']) && (int)$cp_config['original_cp_enabled'] === 2)
		{
			$import_obj->cp_id = 0;
		}
		//-->媒资注入前日志 xingcheng.hu
		$video_type = "media";
		nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG片源注入开始==============================",$sp_id,$video_type,'');
		nn_log::write_log_message(LOG_MODEL_CMS,$action,$sp_id,$video_type,'');
		$assets_core_org_id = (isset($sp_config['media_cdn_model']) && $sp_config['media_cdn_model'] == '1') ? 1 : 0;
		if ($action == 'destroy')
		{
			/**
			 * 吉林联通、福建移动遇上的bug 在播控上查询不到$info['nns_id']的数据 但是cms存在该注入片源  
			 * 现在加个逻辑判断 如果 $info['nns_id']设置了且不为空则 用 $info['nns_id'] 否则用 $c2_task_info['nns_ref_id']
			 * @author liangpan
			 * @date 2015-07-06
			 */
			$str_del_media_id = (isset($info['nns_id']) && !empty($info['nns_id'])) ? $info['nns_id'] : $c2_task_info['nns_ref_id'];
            $str_arr_to_str = var_export($str_del_media_id,true);
            $str_import_assets_message = date('Y-m-d H:i:s').'[EPG片源注入接口URL：]'.$url."\n".
                date('Y-m-d H:i:s').'[EPG片源注入参数：]'."\n".$str_arr_to_str;
            nn_log::write_log_message(LOG_MODEL_CMS,$str_import_assets_message,$sp_id,$video_type,'');
			//$result = $import_obj->delete_video_file($info['nns_id']);
			if(!$cp_config['check_metadata_enabled'])//下发注入的随机ID
			{
				$result = $import_obj->delete_video_file($str_del_media_id,0,$info['nns_import_source'],$cp_id,(isset($c2_task_info['nns_message_id']) && strlen($c2_task_info['nns_message_id']) >0) ? $c2_task_info['nns_message_id'] : '');
			}
			else 
			{
				$result = $import_obj->delete_video_file($str_del_media_id,0,null,$cp_id,(isset($c2_task_info['nns_message_id']) && strlen($c2_task_info['nns_message_id']) >0) ? $c2_task_info['nns_message_id'] : '');
			}
			$xml = "";
		}
		else
		{
			//$drm_enabled = get_drm_config("g_drm_enable");
			if (!isset($sp_config['import_srouce_url']) || (isset($sp_config['import_srouce_url']) && (int)$sp_config['import_srouce_url'] === 1))
			{
				$info['nns_url'] = $c2_task_info['nns_file_path'];
				if(empty($info['nns_url']))
				{
					$ext_data = json_decode($info['nns_ext_info'],true);
					$info['nns_url'] = $ext_data['path'];
				}
				if(empty($info['nns_url']))
				{
					return false;
				}
			}
			$xml = '<media file_id="' . $info['nns_id'] . '" file_type="' . $info['nns_filetype'] . '" ';
			$xml .= ' file_name="' . htmlspecialchars($c2_task_info['nns_name'], ENT_QUOTES) . '" ';
			
			if((isset($sp_config['media_cdn_model']) && $sp_config['media_cdn_model'] == '1') || (isset($sp_config['asset_import_cms_model']) && $sp_config['asset_import_cms_model'] == '1'))
			{
				$xml .= 'file_path="' . $c2_task_info['nns_file_path'] . '" file_definition="' . $info['nns_mode'] . '" ';
			}
			else
			{
				$xml .= 'file_path="' . $info['nns_url'] . '" file_definition="' . $info['nns_mode'] . '" ';
			}

			$xml .= 'file_time_len="' . $info['nns_file_time_len'] . '" ';
			$xml .= 'Tags="' . $info['nns_tag'] . '" ';
			$info['nns_file_size'] = (int)$c2_task_info['nns_file_size'] > 0 ? $c2_task_info['nns_file_size'] : ((int)$info['nns_file_size'] > 0 ? (int)$info['nns_file_size'] : 8081);
			$xml .= 'file_resolution="' . $info['nns_file_resolution'] . '" file_size="' . $info['nns_file_size'] . '" file_bit_rate="' . $info['nns_kbps'] . '" file_desc="" ';
			$xml .= 'original_id="' . $info['nns_import_id'] . '" ';

			if(isset($sp_config['media_service_type']) && strlen($sp_config['media_service_type']) >0)
			{
			    $xml .= 'media_service_type="' . $sp_config['media_service_type'] . '" ';
			}
			else if(isset($info['nns_media_service']) && strlen($info['nns_media_service']) >0)
			{
			    $xml .= 'media_service_type="' . $info['nns_media_service'] . '" ';
			}
//			$xml .= 'ex_url="' . $c2_task_info['nns_ex_url'] . '" ';

			if(strlen($c2_task_info['nns_ex_url'])<1 && $sp_id=='cqyx')
			{
				$xml .= 'ex_url="' . $info['nns_content_id'] . '" ';
			}
			else
			{
			    if(isset($info['nns_ext_url']) && !empty($info['nns_ext_url']) && self::is_json($info['nns_ext_url']))
			    {
			        $arr_ex_media_url = json_decode($info['nns_ext_url'],true);
			        if(isset($arr_ex_media_url['play_url']) && strlen($arr_ex_media_url['play_url']) >0)
			        {
			            $xml .= 'ex_url="' . $arr_ex_media_url['play_url'] . '" ';
			        }
			    }
			    else if(isset($sp_config['asset_import_cms_model']) && $sp_config['asset_import_cms_model'] == '2' && isset($sp_config['c2_play_url_ip']) && strlen($sp_config['c2_play_url_ip']) >0)
			    {
			        $str_c2_play_url_ip = trim(trim(trim($sp_config['c2_play_url_ip'],'/'),'\\'));
			        $str_file_path = trim(trim(trim($c2_task_info['nns_file_path'],'/'),'\\'));
			        $xml .= 'ex_url="' . $str_c2_play_url_ip.'/'.$str_file_path . '" ';
			    }
			    else
			    {
				    $xml .= 'ex_url="' . $c2_task_info['nns_ex_url'] . '" ';
			    }
			}
//			if(strlen($info['nns_cp_id']) > 0)
//			{
//				$sql_cp = "select * from nns_cp where nns_id = '{$info['nns_cp_id']}' limit 1";
//				$result_cp = nl_db_get_one($sql_cp, $dc->db());
//				if(is_array($result_cp) && isset($result_cp['nns_config']) && !empty($result_cp['nns_config']))
//				{
//					$arr_cp_config = json_decode($result_cp['nns_config'],true);
//					if(is_array($arr_cp_config) && isset($arr_cp_config['message_import_enable']) && $arr_cp_config['message_import_enable'] == '1' && isset($arr_cp_config['message_import_mode']) && $arr_cp_config['message_import_mode'] == '1')
//					{
//						$xml .= 'file_core_id="' . $info['nns_content_id'] . '" ';
//					}
//			}
//			}
            //epg注入是否下发绑定的COREID
            if(isset($sp_config['import_core_bind_id_enabled']) && (int)$sp_config['import_core_bind_id_enabled'] === 1)
            {
                if(isset($info['nns_content_id']) && strlen($info['nns_content_id']) >0)
                {
                    $xml .= 'file_core_id="' . $info['nns_content_id'] . '" ';
                }
                else
                {
                    $xml .= 'file_core_id="' . $info['nns_import_id'] . '" ';
                }
            }

            if(isset($sp_config['media_cdn_model']) && $sp_config['media_cdn_model'] == '1')
			{
				$xml .= 'policy="' . $cp_id . '" ';
			}
			else
			{
				$xml .= 'policy="' . $c2_task_info['nns_cdn_policy'] . '" ';
			}
			if(isset($cp_config['original_cp_enabled']) && (int)$cp_config['original_cp_enabled'] === 1)
			{
				$new_cp_id = $arr_video_info['nns_producer'];
			}
			elseif(isset($cp_config['original_cp_enabled']) && (int)$cp_config['original_cp_enabled'] === 2)
			{
				$new_cp_id = 0;
			}
			else
			{
				$new_cp_id =  $info['nns_cp_id'];
			}
			$xml .= 'asset_source="' . $new_cp_id . '" ';
			if(!$cp_config['check_metadata_enabled'])//下发注入的随机ID
			{
				$xml .= 'import_source="' . $info['nns_import_source'] . '" ';
			}
			//增加DRM加密及方案
// 			if((!isset($sp_config['main_drm_enabled']) || intval($sp_config['main_drm_enabled']) == 1) && (!isset($sp_config['q_disabled_drm']) || intval($sp_config['q_disabled_drm']) == 0) && $drm_enabled == 1)
			if(isset($sp_config['main_drm_enabled']) && intval($sp_config['main_drm_enabled']) == 1)
			{
				//开启DRM
				$xml .= 'drm_flag="' . $info['nns_drm_enabled'] . '" ';
				$xml .= 'drm_encrypt_solution="' . $sp_config['q_drm_identify'] . '" ';
				$xml .= 'drm_ext_info="" ';
			}
			else if (isset($cp_config['main_drm_enabled']) && intval($cp_config['main_drm_enabled']) == 1)
			{
			    //开启DRM
			    $xml .= 'drm_flag="' . $info['nns_drm_enabled'] . '" ';
			    $xml .= 'drm_encrypt_solution="' . $info['nns_drm_encrypt_solution'] . '" ';
			    $xml .= 'drm_ext_info="' . htmlspecialchars($info['nns_drm_ext_info']) . '" ';
			}
			else 
			{
				$xml .= 'drm_flag="0" ';
				$xml .= 'drm_encrypt_solution="" ';
				$xml .= 'drm_ext_info="" ';
			}
			
			$sql_ex = "SELECT * FROM `nns_vod_media_ex` where nns_vod_media_id='{$info['nns_import_id']}'";
			$media_ex = nl_db_get_all($sql_ex, $dc->db());
			if(is_array($media_ex))
			{
				foreach ($media_ex as $value)
				{
					$xml .= $value['nns_key'] . '="' . htmlspecialchars($value['nns_value'], ENT_QUOTES) . '" ';
				}
			}
			
			if (isset($cp_config["clip_custom_origin_id"]) && !empty($cp_config["clip_custom_origin_id"]))
			{
				$xml .= 'cdn_ext_source_id="' . $cp_config["clip_custom_origin_id"] . '" ';
			}
			if ($info['nns_dimensions'] == '0')
			{
				$xml .= 'file_3d="0" file_3d_type=""></media>';
			}
			elseif ($info['nns_dimensions'] == '100000')
			{
				$xml .= 'file_3d="1" file_3d_type="3D"></media>';
			}
			elseif ($info['nns_dimensions'] == '100001')
			{
				$xml .= 'file_3d="1" file_3d_type="0"></media>';
			}
			elseif ($info['nns_dimensions'] == '100002')
			{
				$xml .= 'file_3d="1" file_3d_type="1"></media>';
			}
			elseif ($info['nns_dimensions'] == '100003')
			{
				$xml .= 'file_3d="1" file_3d_type="2"></media>';
			}
			else
			{
				$xml .= 'file_3d="0" file_3d_type=""></media>';
			}

			//-->主媒资注入前日志 xingcheng.hu
			$data['func'] 				= 'import_video_file';
			$data['assets_video_type'] 	= 0;
			
			$data['assets_id'] 			= $info['nns_vod_id'];
			$data['assets_clip_id'] 	= $info['nns_vod_index_id'];
			$data['assets_file_id'] 	= $info['nns_id'];
			$data['assets_file_info'] 	= $xml;
			$data['policy'] 	= (isset($sp_config['media_cdn_model']) && $sp_config['media_cdn_model'] == '1') ? $cp_id : $c2_task_info['nns_cdn_policy'];
			$str_arr_to_str = var_export($data,true);
			$str_import_assets_message = date('Y-m-d H:i:s').'[EPG片源注入接口URL：]'.$url."\n".
				date('Y-m-d H:i:s').'[EPG片源注入参数：]'."\n".$str_arr_to_str;
			nn_log::write_log_message(LOG_MODEL_CMS,$str_import_assets_message,$sp_id,$video_type,'');
			unset($data);
			unset($str_arr_to_str);
			unset($str_import_assets_message);
			$result = $import_obj->import_video_file($info['nns_vod_id'], $info['nns_vod_index_id'], $info['nns_id'], $xml, 0, (isset($sp_config['media_cdn_model']) && $sp_config['media_cdn_model'] == '1') ? $cp_id : $c2_task_info['nns_cdn_policy'],$assets_core_org_id,$cp_id,(isset($c2_task_info['nns_message_id']) && strlen($c2_task_info['nns_message_id']) >0) ? $c2_task_info['nns_message_id'] : '');
		}
//		@error_log("nns_vod_id=" . $info['nns_vod_id'] . "\r\n", 3, $log . '/' . date('Ymd') . '.txt');
//		@error_log("nns_vod_index_id=" . $info['nns_vod_index_id'] . "\r\n", 3, $log . '/' . date('Ymd') . '.txt');
//		@error_log("nns_id=" . $info['nns_id'] . "\r\n", 3, $log . '/' . date('Ymd') . '.txt');
//		@error_log($xml . "\r\n", 3, $log . '/' . date('Ymd') . '.txt');
		$re = json_decode($result, true);
		//-->片源注入后日志
		nn_log::write_log_message(LOG_MODEL_CMS,$re,$sp_id,$video_type,'');
		nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG片源注入结束==============================",$sp_id,$video_type,'');
//		@error_log(var_export($re, true) . "\r\n", 3, $log . '/' . date('Ymd') . '.txt');
//		@error_log("++++++by hand++++media+++++++++++by hand++++media+++++++++++++++++++++++++++by hand++++media++++++++++++++++++" . "\r\n", 3, $log . '/' . date('Ymd') . '.txt');
//
		$bool = false;
		//if ((int)$re['ret'] === 0 || stripos($re['reason'], '8096'))
		if ($re['ret'] == '0')
		{
			$bool = true;
		}
		//elseif ((int)$re['ret'] === 7 || stripos($re['reason'], '8097'))
		elseif ((int)$re['ret'] === 7)
		{
			$bool = true;
		}
		elseif ((int)$re['ret'] === 6)
		{
			$bool = true;
		}
		else
		{
			nn_log::write_log_message(LOG_MODEL_CMS,'注入报错:'.var_export($re,true),$sp_id,$video_type);
		}

        //香港直播转点播反馈
        if (isset($cp_config['cp_playbill_to_media_enabled']) && $cp_config['cp_playbill_to_media_enabled'] == '1' && isset($cp_config['source_notify_url']) && strlen($cp_config['source_notify_url']) >0)
        {
            if ($bool === true)
            {
                $result_encode = array(
                    'data_info' => $c2_task_info['nns_message_id'],
                    'ret' => 0,
                    'reason' => $re['reason'],
                );
            }
            else
            {
                $result_encode = array(
                    'data_info' => $c2_task_info['nns_message_id'],
                    'ret' => 1,
                    'reason' => $re['reason'],
                );
           }
            nn_log::write_log_message(LOG_MODEL_CMS,'注入反馈开始,参数数据为：'.var_export($result_encode,true),$c2_task_info['nns_org_id'],$video_type);
            $re_feedback = self::notify_cp_sp_playbill_to_video($result_encode, $cp_config['source_notify_url']);
            nn_log::write_log_message(LOG_MODEL_CMS, '直播转点播注入epg状态直接反馈给epg完成：状态' . var_export($re_feedback, true), $c2_task_info['nns_org_id'], $video_type);

        }


        $str_cdn_notify = '';
		if(isset($sp_config['media_cdn_model']) && $sp_config['media_cdn_model'] == '1')
		{
			$bool = true;
			$sql_query_op_id="select nns_id,nns_message_id from nns_mgtvbk_op_queue where nns_id = '{$c2_task_info['nns_op_id']}' limit 1";
			$info_query_op_id = nl_query_by_db($sql_query_op_id, $dc->db());
			$str_notify_cms_message_id = (isset($info_query_op_id[0]['nns_message_id'])) ? $info_query_op_id[0]['nns_message_id'] : $c2_task_info['nns_message_id'];
			$asset_cdn_share_addr = isset($sp_config['asset_cdn_share_addr']) ? trim(trim($sp_config['asset_cdn_share_addr'],'/'),'\\') : '';
			$result_notify_cms = self::notify_cms_data($asset_cdn_share_addr, $info, $sp_id, $action, $video_type, $str_notify_cms_message_id, $bool);
			if($result_notify_cms['ret'] !=0)
			{
				$bool = false;
			}
			$str_cdn_notify = isset($result_notify_cms['data_info']) ? $result_notify_cms['data_info'] : '';
		}
		if ($bool === true)
		{
			if ($c2_task_info['nns_epg_status'] == 97 || $c2_task_info['nns_epg_status'] == 100)
			{
				$sql = "update nns_mgtvbk_c2_task set nns_epg_status=99,nns_modify_time=now(),nns_action='$action' where nns_id='{$c2_task_info['nns_id']}'";
				nl_execute_by_db($sql, $dc->db());
			}
			if ($c2_task_info['nns_epg_status'] == 99 && !empty($c2_task_info['nns_op_id']))
			{
				$queue_task_model = new queue_task_model();
				$queue_task_model->q_report_task($c2_task_info['nns_id']);
				$queue_task_model = null;
			}
		}
		else
		{
			$now = date('Y-m-d H:i:s');
			if($sp_id == 'gzgd_tz_sp')
			{
				$sql = "update nns_mgtvbk_c2_task set nns_epg_status=100,nns_modify_time='{$now}',nns_action='$action' where nns_id='{$c2_task_info['nns_id']}'";
			}
			else
			{
				$sql = "update nns_mgtvbk_c2_task set nns_epg_status=97,nns_modify_time='{$now}',nns_action='$action',nns_epg_fail_time=nns_epg_fail_time+1 where nns_id='{$c2_task_info['nns_id']}'";
			}
			if($sp_id == 'jscn_sihua' || $sp_id == 'sihua')
			{
    			$sql_index = "update nns_mgtvbk_c2_task set nns_epg_status=97,nns_modify_time='{$now}' where nns_org_id='{$c2_task_info['nns_org_id']}' and nns_ref_id='{$c2_task_info['nns_src_id']}' and nns_type='index' limit 1";
    			nl_execute_by_db($sql_index, $dc->db());
			}
			nl_execute_by_db($sql, $dc->db());
			
			//天威视讯注入失败 异步反馈片源信息,后面需转移至配置文件中
			if(isset($sp_config['message_feedback_mode']) && $sp_config['message_feedback_mode'] == 1 && isset($sp_config['site_callback_url']) && strlen($sp_config['site_callback_url']) > 0 && file_exists(dirname(dirname(dirname(__FILE__))).'/api/'.$sp_id.'/'.$sp_id.'_sync_source.php'))
			{
				include_once dirname(dirname(dirname(__FILE__))).'/api/'.$sp_id.'/'.$sp_id.'_sync_source.php';
				if(method_exists($sp_id.'_sync_source','async_feedback'))
				{
					//根据OP_queue_id查询消息ID
					$op_sql = "select nns_message_id from nns_mgtvbk_op_queue where nns_id='{$c2_task_info['nns_op_id']}'";
					$op_queue_re = nl_db_get_one($op_sql, $dc->db());
					if(is_array($op_queue_re))
					{
						$movie_info = array(
								'nns_action' => $action,
								'nns_message_id' => $op_queue_re['nns_message_id'],
								'nns_org_id' => $sp_id
						);
						$class_name = $sp_id.'_sync_source';
						$sync_source = new $class_name();
						$sync_source->async_feedback($movie_info,false,false,$info['nns_import_id']);
					}
				}
			}
		}
		$log_data = array (
				'nns_id' => np_guid_rand('media'),
				'nns_video_id' => $info['nns_id'],
				'nns_video_type' => 'media',
				'nns_org_id' => $sp_id,
				'nns_create_time' => date('Y-m-d H:i:s'),
				'nns_action' => $action,
				'nns_status' => $re['ret'],
				'nns_reason' => addslashes($re['reason']),
				'nns_data' => $xml,
				'nns_name' => $c2_task_info['nns_name'],
				'nns_file_dir' => $str_cdn_notify,
		);
		@error_log(var_export($log_data, true) . "\r\n", 3, $log . '/' . date('Ymd') . '.txt');
		nl_db_insert($dc->db(), 'nns_mgtvbk_import_epg_log', $log_data);
		$queue_task_model = new queue_task_model();
		$queue_task_model->q_report_task($c2_task_info['nns_id']);
		$queue_task_model = null;
		//nn_close_db($db_r);
		//nn_close_db($db_w);
		return $bool;
	}

	
	static public function is_json($string) 
	{
	    $string = strlen($string) <1 ? '' : $string;
	    json_decode($string);
	    return (json_last_error() == JSON_ERROR_NONE);
	}
	
	/**
	 * 直播源注入
	 * @param unknown $nns_id
	 * @return Ambigous <Ambigous, array('ret'=>'状态码','reason'=>'原因','data'=>'数据'), multitype:NULL number string , number, mixed, multitype:NULL multitype:a r y s t i n g  array multitype:number  >|Ambigous <multitype:, multitype:int string Ambigous <NULL, null> >|multitype:number mixed |mixed
	 */
	static public function import_live($nns_id)
	{
	    include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/c2_task/c2_task.class.php';
		include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/live/live.class.php';
		include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/live/live_index.class.php';
		include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/live/live_media.class.php';
		include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/depot/depot.class.php';
		$dc = nl_get_dc(array(
				'db_policy' =>  NL_DB_READ | NL_DB_WRITE,
				'cache_policy' => NP_KV_CACHE_TYPE_NULL
			)
		);
		$c2_task_info=nl_c2_task::query_by_id($dc, $nns_id);
		if ($c2_task_info['ret'] !=0 || !isset($c2_task_info['data_info']) || empty($c2_task_info['data_info']) || !is_array($c2_task_info['data_info']))
		{
			return $c2_task_info;
		}
		$c2_task_info = $c2_task_info['data_info'];
		nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG直播注入开始==============================",$c2_task_info['nns_org_id'],'live');
		$sp_config = sp_model::get_sp_config($c2_task_info['nns_org_id']);
		//获取片源信息
		$result_live_media=nl_live_media::query_by_id($dc, $c2_task_info['nns_ref_id']);
		if($result_live_media['ret'] !=0 || !isset($result_live_media['data_info']) || empty($result_live_media['data_info']) || !is_array($result_live_media['data_info']))
		{
			nn_log::write_log_message(LOG_MODEL_CMS,"获取片源信息失败：".var_export($result_live_media,true),$c2_task_info['nns_org_id'],'live');
			return $result_live_media;
		}
		$result_live_media = $result_live_media['data_info'];
		//获取片源 注入ID
		$result_media_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'live_media', $result_live_media['nns_id'],$result_live_media,$sp_config,'epg');
		if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
		{
			nn_log::write_log_message(LOG_MODEL_CMS,"获取片源注入id信息失败：".var_export($result_media_import_info,true),$c2_task_info['nns_org_id'],'live');
			return $result_media_import_info;
		}
		$result_live_media['nns_live_media_import_id'] = $result_media_import_info['data_info'];
		//获取直播分集信息
		$result_live_index=nl_live_index::query_by_id($dc, $result_live_media['nns_live_index_id']);
		if($result_live_index['ret'] !=0 || !isset($result_live_index['data_info']) || empty($result_live_index['data_info']) || !is_array($result_live_index['data_info']))
		{
			nn_log::write_log_message(LOG_MODEL_CMS,"获取分集信息失败：".var_export($result_live_index,true),$c2_task_info['nns_org_id'],'live');
			return $result_live_index;
		}
		$result_live_index = $result_live_index['data_info'];
		//获取分集 注入ID
		$result_index_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'live_index', $result_live_index['nns_id'],$result_live_index,$sp_config,'epg');
		
		if($result_index_import_info['ret'] !=0)
		{
			nn_log::write_log_message(LOG_MODEL_CMS,"获取分集注入ID信息失败：".var_export($result_index_import_info,true),$c2_task_info['nns_org_id'],'live');
			return $result_index_import_info;
		}
		$result_live_index['nns_live_index_import_id'] = $result_index_import_info['data_info'];
		//获取直播频道信息
		$result_live=nl_live::query_by_id($dc, $result_live_media['nns_live_id']);
		if($result_live['ret'] !=0 || !isset($result_live['data_info']) || empty($result_live['data_info']) || !is_array($result_live['data_info']))
		{
			nn_log::write_log_message(LOG_MODEL_CMS,"获取频道信息失败：".var_export($result_live,true),$c2_task_info['nns_org_id'],'live');
			return $result_live;
		}
		$result_live = $result_live['data_info'];
		$result_live_ex=nl_live::query_ex_by_id($dc, $result_live_media['nns_live_id']);
		if($result_live_ex['ret'] !=0)
		{
			nn_log::write_log_message(LOG_MODEL_CMS,"获取频道扩展信息失败：".var_export($result_live_ex,true),$c2_task_info['nns_org_id'],'live');
			return $result_live_ex;
		}
		$result_live_ex = (isset($result_live_ex['data_info']) && !empty($result_live_ex['data_info']) && is_array($result_live_ex['data_info'])) ? $result_live_ex['data_info'] : null;
		$str_live_operation = ($result_live['nns_deleted'] !=1) ? 1 : 2;
		$result_live_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'live', $result_live['nns_id'],$result_live,$sp_config,'epg');
		if($result_live_import_info['ret'] !=0 || !isset($result_live_import_info['data_info']) || strlen($result_live_import_info['data_info']) <1)
		{
			nn_log::write_log_message(LOG_MODEL_CMS,"获取频道注入id信息失败：".var_export($result_live_import_info,true),$c2_task_info['nns_org_id'],'live');
			return $result_live_import_info;
		}
		$result_live['nns_live_import_id'] = $result_live_import_info['data_info'];
		$result_depot = nl_depot::get_depot_info($dc,array('nns_id'=>$result_live['nns_depot_id']),'live');
		//获取栏目信息
		if($result_depot['ret'] !=0)
		{
			nn_log::write_log_message(LOG_MODEL_CMS,"获取频道栏目信息失败：".var_export($result_depot,true),$c2_task_info['nns_org_id'],'live');
			return $result_live_ex;
		}
		$arr_depot = public_model_exec::get_category_info($result_depot['data_info'][0]['nns_category']);
		$result_live['nns_category_name'] = (isset($arr_depot['data_info'][$result_live['nns_category_id']]['name']) && strlen($arr_depot['data_info'][$result_live['nns_category_id']]['name']) > 0) ? $arr_depot['data_info'][$result_live['nns_category_id']]['name'] : '卫视台';
		
		$result_live['nns_alias_name']  = (isset($result_live['nns_alias_name']) && strlen($result_live['nns_alias_name']) >0) ? $result_live['nns_alias_name'] : $result_live['nns_name'];
		if($c2_task_info['nns_org_id'] == 'bestv')
		{
		    $result_live_media['nns_media_service'] ='3aproxy';
		    $result_live_media['nns_media_proto'] ='http';
		    $result_live_media['nns_live_media_import_id'] = $result_live['nns_live_import_id'];
		}
		$str_xml = self::make_import_cms_xml('live',$result_live,$result_live_index,$result_live_media,$result_live_ex);
		if(strlen($str_xml) <1)
		{
			self::$arr_error_data[] = 'xml数据组装失败';
			nn_log::write_log_message(LOG_MODEL_CMS,"xml数据组装失败",$c2_task_info['nns_org_id'],'live');
			return self::return_error_data();
		}
		$import_obj = new import($sp_config['epg_url'], dirname(dirname(dirname(__FILE__))).'/data/log/import/'.$c2_task_info['nns_org_id'],$c2_task_info['nns_org_id'],$result_live_media['nns_cp_id']);
		$result = $import_obj->import_cms_live_info($str_xml);
		$re = json_decode($result, true);
		//-->片源注入后日志
		nn_log::write_log_message(LOG_MODEL_CMS,"注入cms结果".var_export($re,true),$c2_task_info['nns_org_id'],'live');
		nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG直播注入结束==============================",$c2_task_info['nns_org_id'],'live');
		$now = date("Y-m-d H:i:s");
		$bool = false;
		if($re['ret'] == 0 )
		{
			$bool = true;
			$sql = "update nns_mgtvbk_c2_task set nns_epg_status=99,nns_modify_time='{$now}',nns_action='{$c2_task_info['nns_action']}' where nns_id='{$c2_task_info['nns_id']}'";
			nl_execute_by_db($sql, $dc->db());
		    $queue_task_model = new queue_task_model();
		    $queue_task_model->q_report_task($c2_task_info['nns_id']);
		    $queue_task_model = null;
		}
		else
		{
			$sql = "update nns_mgtvbk_c2_task set nns_epg_status=97,nns_modify_time='{$now}',nns_action='{$c2_task_info['nns_action']}' where nns_id='{$c2_task_info['nns_id']}'";
			nl_execute_by_db($sql, $dc->db());
		}
		$log_data = array (
				'nns_id' => np_guid_rand('live_media'),
				'nns_video_id' => $result_live_media['nns_id'],
				'nns_video_type' => 'live_media',
				'nns_org_id' => $c2_task_info['nns_org_id'],
				'nns_create_time' => $now,
				'nns_action' => $c2_task_info['nns_action'],
				'nns_status' => $re['ret'],
				'nns_reason' => addslashes($re['reason']),
				'nns_data' => $str_xml,
				'nns_name' => $c2_task_info['nns_name'],
				'nns_file_dir' => '',
		);
		nl_db_insert($dc->db(), 'nns_mgtvbk_import_epg_log', $log_data);
		return $bool;
	}
	
	
	/**
	 * 直播频道注入
	 * @param unknown $nns_id
	 * @return Ambigous <Ambigous, array('ret'=>'状态码','reason'=>'原因','data'=>'数据'), multitype:NULL number string , number, mixed, multitype:NULL multitype:a r y s t i n g  array multitype:number  >|Ambigous <multitype:, multitype:int string Ambigous <NULL, null> >|multitype:number mixed |mixed
	 */
	static public function import_channel($nns_id)
	{
	    include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/c2_task/c2_task.class.php';
	    include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/live/live.class.php';
	    include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/depot/depot.class.php';
	    $dc = nl_get_dc(array(
	        'db_policy' =>  NL_DB_READ | NL_DB_WRITE,
	        'cache_policy' => NP_KV_CACHE_TYPE_NULL
	    )
	    );
	    $c2_task_info=nl_c2_task::query_by_id($dc, $nns_id);
	    if ($c2_task_info['ret'] !=0 || !isset($c2_task_info['data_info']) || empty($c2_task_info['data_info']) || !is_array($c2_task_info['data_info']))
	    {
	        return $c2_task_info;
	    }
	    $c2_task_info = $c2_task_info['data_info'];
	    nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG直播频道注入开始==============================",$c2_task_info['nns_org_id'],'live');
	    $sp_config = sp_model::get_sp_config($c2_task_info['nns_org_id']);
	    
	    //获取直播频道信息
	    $result_live=nl_live::query_by_id($dc, $c2_task_info['nns_ref_id']);
	    if($result_live['ret'] !=0 || !isset($result_live['data_info']) || empty($result_live['data_info']) || !is_array($result_live['data_info']))
	    {
	        nn_log::write_log_message(LOG_MODEL_CMS,"获取频道信息失败：".var_export($result_live,true),$c2_task_info['nns_org_id'],'live');
	        return $result_live;
	    }
	    $result_live = $result_live['data_info'];
	    $result_live_ex=nl_live::query_ex_by_id($dc, $result_live['nns_id']);
	    if($result_live_ex['ret'] !=0)
	    {
	        nn_log::write_log_message(LOG_MODEL_CMS,"获取频道扩展信息失败：".var_export($result_live_ex,true),$c2_task_info['nns_org_id'],'live');
	        return $result_live_ex;
	    }
	    $result_live_ex = (isset($result_live_ex['data_info']) && !empty($result_live_ex['data_info']) && is_array($result_live_ex['data_info'])) ? $result_live_ex['data_info'] : null;
	    $str_live_operation = ($result_live['nns_deleted'] !=1) ? 1 : 2;
	    $result_live_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'live', $result_live['nns_id'],$result_live,$sp_config,'epg');
	    if($result_live_import_info['ret'] !=0 || !isset($result_live_import_info['data_info']) || strlen($result_live_import_info['data_info']) <1)
	    {
	        nn_log::write_log_message(LOG_MODEL_CMS,"获取频道注入id信息失败：".var_export($result_live_import_info,true),$c2_task_info['nns_org_id'],'live');
	        return $result_live_import_info;
	    }
	    $result_live['nns_live_import_id'] = $result_live_import_info['data_info'];
	    $result_depot = nl_depot::get_depot_info($dc,array('nns_id'=>$result_live['nns_depot_id']),'live');
	    //获取栏目信息
	    if($result_depot['ret'] !=0)
	    {
	        nn_log::write_log_message(LOG_MODEL_CMS,"获取频道栏目信息失败：".var_export($result_depot,true),$c2_task_info['nns_org_id'],'live');
	        return $result_live_ex;
	    }
	    $arr_depot = public_model_exec::get_category_info($result_depot['data_info'][0]['nns_category']);
	    $result_live['nns_category_name'] = (isset($arr_depot['data_info'][$result_live['nns_category_id']]['name']) && strlen($arr_depot['data_info'][$result_live['nns_category_id']]['name']) > 0) ? $arr_depot['data_info'][$result_live['nns_category_id']]['name'] : '卫视台';
	    $str_xml = self::make_import_cms_xml('live',$result_live,null,null,$result_live_ex);
	    if(strlen($str_xml) <1)
	    {
	        self::$arr_error_data[] = 'xml数据组装失败';
	        nn_log::write_log_message(LOG_MODEL_CMS,"xml数据组装失败",$c2_task_info['nns_org_id'],'live');
	        return self::return_error_data();
	    }
	    $import_obj = new import($sp_config['epg_url'], dirname(dirname(dirname(__FILE__))).'/data/log/import/'.$c2_task_info['nns_org_id'],$c2_task_info['nns_org_id'],$result_live['nns_cp_id']);
	    $result = $import_obj->import_cms_live_info($str_xml);
	    $re = json_decode($result, true);
	    //-->片源注入后日志
	    nn_log::write_log_message(LOG_MODEL_CMS,"注入cms结果".var_export($re,true),$c2_task_info['nns_org_id'],'live');
	    nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG直播频道注入结束==============================",$c2_task_info['nns_org_id'],'live');
	    $now = date("Y-m-d H:i:s");
	    $bool = false;
	    if($re['ret'] == 0 )
	    {
	        $bool = true;
	        $sql = "update nns_mgtvbk_c2_task set nns_epg_status=99,nns_modify_time='{$now}',nns_action='{$c2_task_info['nns_action']}' where nns_id='{$c2_task_info['nns_id']}'";
	        nl_execute_by_db($sql, $dc->db());
	        $queue_task_model = new queue_task_model();
	        $queue_task_model->q_report_task($c2_task_info['nns_id']);
	        $queue_task_model = null;
	    }
	    else
	    {
	        $sql = "update nns_mgtvbk_c2_task set nns_epg_status=97,nns_modify_time='{$now}',nns_action='{$c2_task_info['nns_action']}' where nns_id='{$c2_task_info['nns_id']}'";
	        nl_execute_by_db($sql, $dc->db());
	    }
	    $log_data = array (
	        'nns_id' => np_guid_rand('live_media'),
	        'nns_video_id' => $result_live['nns_id'],
	        'nns_video_type' => 'live',
	        'nns_org_id' => $c2_task_info['nns_org_id'],
	        'nns_create_time' => $now,
	        'nns_action' => $c2_task_info['nns_action'],
	        'nns_status' => $re['ret'],
	        'nns_reason' => addslashes($re['reason']),
	        'nns_data' => $str_xml,
	        'nns_name' => $c2_task_info['nns_name'],
	        'nns_file_dir' => '',
	    );
	    nl_db_insert($dc->db(), 'nns_mgtvbk_import_epg_log', $log_data);
	    return $bool;
	}
	
	static public function epg_live($dc,$live_info,$action,$sp_id)
	{
	    $sp_config = sp_model::get_sp_config($sp_id);
	    if(!isset($live_info['nns_channel_id']) && !empty($live_info['nns_channel_id']))
	    {
	        return array(
	            'ret'=>1,
	            'reason'=>'直播片源传入参数无nns_channel_id字段,参数为:'.var_export($live_info,true),
	        );
	    }
	    $channel_id = $live_info['nns_channel_id'];
	    include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/live/live.class.php';
	    $result_channel = nl_live::query_by_id($dc, $channel_id);
	    if($result_channel['ret'] !=0 || !isset($result_channel['data_info']) || !is_array($result_channel['data_info']) || empty($result_channel['data_info']))
	    {
	        return $result_channel;
	    }
	    $result_channel = $result_channel['data_info'];
	    include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/live/live_media.class.php';
	    $result_media = nl_live_media::query_by_channel_id($dc, $channel_id);
	    if($result_media['ret'] !=0 || !isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
	    {
	        return $result_media;
	    }
	    $result_media = $result_media['data_info'];
	    include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/c2_task/c2_task.class.php';
	    foreach ($result_media as $media_list)
	    {
	        $str_task_id = np_guid_rand();
	        $result_c2_exsist=nl_c2_task::query_c2_exsist($dc,'live_media',$media_list['nns_id'],$sp_id);
	        if($result_c2_exsist['ret'] !=0)
	        {
	            return $result_c2_exsist;
	        }
	        $result_c2_exsist = isset($result_c2_exsist['data_info'][0]) ? $result_c2_exsist['data_info'][0] : null;
	        $str_action = ($media_list['nns_deleted'] !=1) ? ($media_list['nns_modify_time'] > $media_list['nns_create_time']) ? 'modify' : 'add' : 'destroy';
	        if(is_array($result_c2_exsist) && !empty($result_c2_exsist))
	        {
	            $str_task_id = $result_c2_exsist['nns_id'];
	            $result_c2_edit=nl_c2_task::edit($dc, array('nns_action'=>$str_action,'nns_status'=>0,'nns_epg_status'=>97),$str_task_id);
	            if($result_c2_edit['ret'] !=0)
	            {
	                return $result_c2_edit;
	            }
	        }
	        else
	        {
	            $arr_c2_add = array(
	                'nns_id'=>$str_task_id,
	                'nns_type'=>'live_media',
	                'nns_name'=>"[{$result_channel['nns_name']}] 1 [{$media_list['nns_mode']}]-直播流",
	                'nns_ref_id'=>'',
	                'nns_action'=>$str_action,
	                'nns_status'=>0,
	                'nns_org_id'=>$sp_id,
	                'nns_category_id'=>'',
	                'nns_src_id'=>$media_list['nns_id'],
	                'nns_all_index'=>1,
	                'nns_clip_task_id'=>'',
	                'nns_clip_date'=>'',
	                'nns_op_id'=>'',
	                'nns_epg_status'=>97,
	                'nns_ex_url'=>'',
	                'nns_file_path'=>'',
	                'nns_file_size'=>'',
	                'nns_file_md5'=>'',
	                'nns_cdn_policy'=>'',
	                'nns_epg_fail_time'=>0,
	                'nns_message_id'=>'',
	            );
	            $result_c2_edit=nl_c2_task::add($dc, $arr_c2_add);
	            if($result_c2_edit['ret'] !=0)
	            {
	                return $result_c2_edit;
	            }
	        }
	    }
	    return $result_c2_edit;
	}
	
	
	/**
	 * 节目单注入
	 * @param unknown $nns_id
	 * @return Ambigous <Ambigous, array('ret'=>'状态码','reason'=>'原因','data'=>'数据'), multitype:NULL number string , number, mixed, multitype:NULL multitype:a r y s t i n g  array multitype:number  >|Ambigous <multitype:, multitype:int string Ambigous <NULL, null> >|multitype:number mixed |mixed
	 */
	static public function import_playbill($nns_id)
	{
		include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/c2_task/c2_task.class.php';
		include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/live/playbill.class.php';
		include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/live/live_media.class.php';
		include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/live/live.class.php';
		$dc = nl_get_dc(array(
				'db_policy' =>  NL_DB_READ | NL_DB_WRITE,
				'cache_policy' => NP_KV_CACHE_TYPE_NULL
			)
		);
		
		$c2_task_info=nl_c2_task::query_by_id($dc, $nns_id);
		if ($c2_task_info['ret'] !=0 || !isset($c2_task_info['data_info']) || empty($c2_task_info['data_info']) || !is_array($c2_task_info['data_info']))
		{
			return $c2_task_info;
		}
		$c2_task_info = $c2_task_info['data_info'];
		nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG节目单注入开始==============================",$c2_task_info['nns_org_id'],'playbill');
		$sp_config = sp_model::get_sp_config($c2_task_info['nns_org_id']);
		if (isset($sp_config['playbill']['enabled']) && $sp_config['playbill']['enabled'] == '1')
		{
		    return self::import_playbill_v2($dc,$c2_task_info,$sp_config,$c2_task_info['nns_org_id']);
		}
		//获取节目单信息
		$result_playbill=nl_playbill::query_by_id($dc, $c2_task_info['nns_ref_id']);
		if($result_playbill['ret'] !=0 || !isset($result_playbill['data_info']) || empty($result_playbill['data_info']) || !is_array($result_playbill['data_info']))
		{
			nn_log::write_log_message(LOG_MODEL_CMS,"获取节目单信息失败：".var_export($result_playbill,true),$c2_task_info['nns_org_id'],'playbill');
			return $result_playbill;
		}
		$result_playbill = $result_playbill['data_info'];
		$result_playbill['nns_deleted'] = $result_playbill['nns_state'];
		//获取节目单 注入ID
		$result_playbill_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'playbill', $result_playbill['nns_id'],$result_playbill,$sp_config,'epg');
		if($result_playbill_import_info['ret'] !=0 || !isset($result_playbill_import_info['data_info']) || strlen($result_playbill_import_info['data_info']) <1)
		{
			nn_log::write_log_message(LOG_MODEL_CMS,"获取节目单注入id信息失败：".var_export($result_playbill_import_info,true),$c2_task_info['nns_org_id'],'playbill');
			return $result_playbill_import_info;
		}
		$result_playbill['nns_playbill_import_id'] = $result_playbill_import_info['data_info'];
		//获取片源信息
		$result_live_media=nl_live_media::query_by_id($dc, $result_playbill['nns_live_media_id']);
		if($result_live_media['ret'] !=0 || !isset($result_live_media['data_info']) || empty($result_live_media['data_info']) || !is_array($result_live_media['data_info']))
		{
			nn_log::write_log_message(LOG_MODEL_CMS,"获取片源信息失败：".var_export($result_live_media,true),$c2_task_info['nns_org_id'],'live');
			return $result_live_media;
		}
		$result_live_media = $result_live_media['data_info'];
		//获取片源 注入ID
		$result_media_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'live_media', $result_live_media['nns_id'],$result_live_media,$sp_config,'epg');
		if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
		{
			nn_log::write_log_message(LOG_MODEL_CMS,"获取片源注入id信息失败：".var_export($result_media_import_info,true),$c2_task_info['nns_org_id'],'live');
			return $result_media_import_info;
		}
		$result_playbill['nns_live_media_import_id'] = $result_media_import_info['data_info'];
		
		
		
		//获取频道信息
		$result_live=nl_live::query_by_id($dc, $result_live_media['nns_live_id']);
		if($result_live['ret'] !=0 || !isset($result_live['data_info']) || empty($result_live['data_info']) || !is_array($result_live['data_info']))
		{
		    nn_log::write_log_message(LOG_MODEL_CMS,"获取片源信息失败：".var_export($result_live,true),$c2_task_info['nns_org_id'],'live');
		    return $result_live_media;
		}
		$result_live = $result_live['data_info'];
		//获取片源 注入ID
		$result_live_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'live', $result_live['nns_id'],$result_live,$sp_config,'epg');
		if($result_live_import_info['ret'] !=0 || !isset($result_live_import_info['data_info']) || strlen($result_live_import_info['data_info']) <1)
		{
		    nn_log::write_log_message(LOG_MODEL_CMS,"获取片源注入id信息失败：".var_export($result_live_import_info,true),$c2_task_info['nns_org_id'],'live');
		    return $result_live_import_info;
		}
		$result_playbill['nns_live_import_id'] = $result_live_import_info['data_info'];
		
		if($c2_task_info['nns_org_id'] == 'bestv')
		{
		    $result_playbill['nns_live_media_import_id'] = $result_playbill['nns_live_import_id'];
		}
		$result_cp = nl_cp::query_by_id($dc, $result_live['nns_cp_id']);
		$cp_config = json_decode($result_cp['data_info']['nns_config'], true);
		
		$result_playbill['nns_flag'] = $cp_config['live_playbill_cms_model'] == '1' ? 'live' : 'live_media';
		
		$str_xml = self::make_import_cms_xml('playbill',$result_playbill);
		if(strlen($str_xml) <1)
		{
			self::$arr_error_data[] = 'xml数据组装失败';
			nn_log::write_log_message(LOG_MODEL_CMS,"xml数据组装失败",$c2_task_info['nns_org_id'],'playbill');
			return self::return_error_data();
		}
		$import_obj = new import($sp_config['epg_url'], dirname(dirname(dirname(__FILE__))).'/data/log/import/'.$c2_task_info['nns_org_id'],$c2_task_info['nns_org_id'],$result_playbill['nns_cp_id']);
		$result = $import_obj->import_cms_playbill_info($str_xml);
		$re = json_decode($result, true);
		//-->节目单注入后日志
		nn_log::write_log_message(LOG_MODEL_CMS,"注入cms结果".var_export($re,true),$c2_task_info['nns_org_id'],'playbill');
		nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG节目单注入结束==============================",$c2_task_info['nns_org_id'],'playbill');
		$now = date("Y-m-d H:i:s");
		$bool = false;
		if($re['ret'] == 0 )
		{
			$bool = true;
			$sql = "update nns_mgtvbk_c2_task set nns_epg_status=99,nns_modify_time='{$now}',nns_action='{$c2_task_info['nns_action']}' where nns_id='{$c2_task_info['nns_id']}'";
			nl_execute_by_db($sql, $dc->db());
			$queue_task_model = new queue_task_model();
			$queue_task_model->q_report_task($c2_task_info['nns_id']);
			$queue_task_model = null;
		}
		else
		{
			$sql = "update nns_mgtvbk_c2_task set nns_epg_status=97,nns_modify_time='{$now}',nns_action='{$c2_task_info['nns_action']}',nns_epg_fail_time=nns_epg_fail_time+1 where nns_id='{$c2_task_info['nns_id']}'";
			nl_execute_by_db($sql, $dc->db());
		}
		$log_data = array (
				'nns_id' => np_guid_rand('playbill'),
				'nns_video_id' => $result_live_media['nns_id'],
				'nns_video_type' => 'playbill',
				'nns_org_id' => $c2_task_info['nns_org_id'],
				'nns_create_time' => $now,
				'nns_action' => $c2_task_info['nns_action'],
				'nns_status' => $re['ret'],
				'nns_reason' => addslashes($re['reason']),
				'nns_data' => $str_xml,
				'nns_name' => $c2_task_info['nns_name'],
				'nns_file_dir' => '',
		);
		nl_db_insert($dc->db(), 'nns_mgtvbk_import_epg_log', $log_data);
		return $bool;
	}
	
	
	public static function import_playbill_v2($obj_dc,$c2_task_info,$arr_sp_config,$sp_id)
	{
	    $arr_temp_task = $arr_temp_task_1 =$arr_temp_task_2 =  null;
	    $now = date("Y-m-d H:i:s");
	    $group_num = isset($arr_sp_config['playbill']['group_num']) ? (int)$arr_sp_config['playbill']['group_num'] : 100;
	    $group_num = $group_num >0 ? $group_num : 100;
	    $retry_time = isset($arr_sp_config['epg_retry_time']) ? (int)$arr_sp_config['epg_retry_time'] : 1;
	    $retry_time = $retry_time >=1 ? $retry_time : 1;
	    $sql_query_temp = "((nns_action='add' and nns_epg_fail_time<={$retry_time}) or (nns_action='modify' and nns_epg_fail_time<={$retry_time}) or (nns_action='destroy' and nns_epg_fail_time<={$retry_time}))";
	    $sql_group="select * from nns_mgtvbk_c2_task where nns_status='0' and nns_epg_status='97' and nns_type='playbill' and nns_src_id='{$c2_task_info['nns_src_id']}' and nns_org_id='{$sp_id}' ".
	        " and {$sql_query_temp} order by nns_modify_time asc limit {$group_num} ";
	    $result_group = nl_query_by_db($sql_group, $obj_dc->db());
	    if(!is_array($result_group) || empty($result_group))
	    {
	        return true;
	    }
	    foreach ($result_group as $val)
	    {
	        $arr_temp_task[$val['nns_id']]=$val['nns_ref_id'];
	        $arr_temp_task_1[$val['nns_ref_id']][] = $val['nns_id'];
	    }
	    $arr_group_playbill_info = nl_playbill::query_by_ids($obj_dc, array_values($arr_temp_task));
	    if($arr_group_playbill_info['ret']!=0 || !isset($arr_group_playbill_info['data_info']) || empty($arr_group_playbill_info['data_info']) || !is_array($arr_group_playbill_info['data_info']))
	    {
	        $sql = "update nns_mgtvbk_c2_task set nns_epg_status=97,nns_modify_time='{$now}',nns_action='{$c2_task_info['nns_action']}',nns_epg_fail_time=nns_epg_fail_time+1 where nns_id in('".implode("','", array_keys($arr_temp_task))."')";
	        nl_execute_by_db($sql, $obj_dc->db());
	        return true;
	    }
	    
	    
	    //获取片源信息
	    $result_live_media=nl_live_media::query_by_id($obj_dc, $c2_task_info['nns_src_id']);
	    if($result_live_media['ret'] !=0 || !isset($result_live_media['data_info']) || empty($result_live_media['data_info']) || !is_array($result_live_media['data_info']))
	    {
	        nn_log::write_log_message(LOG_MODEL_CMS,"获取片源信息失败：".var_export($result_live_media,true),$c2_task_info['nns_org_id'],'live');
	        $sql = "update nns_mgtvbk_c2_task set nns_epg_status=97,nns_modify_time='{$now}',nns_action='{$c2_task_info['nns_action']}',nns_epg_fail_time=nns_epg_fail_time+1 where nns_id in('".implode("','", array_keys($arr_temp_task))."')";
	        nl_execute_by_db($sql, $obj_dc->db());
	        return false;
	    }
	    $result_live_media = $result_live_media['data_info'];
	    //获取片源 注入ID
	    $result_media_import_info = public_model_exec::get_asset_id_by_sp_id($c2_task_info['nns_org_id'], 'live_media', $result_live_media['nns_id'],$result_live_media,$arr_sp_config,'epg');
	    if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
	    {
	        nn_log::write_log_message(LOG_MODEL_CMS,"获取片源注入id信息失败：".var_export($result_media_import_info,true),$c2_task_info['nns_org_id'],'live');
	        $sql = "update nns_mgtvbk_c2_task set nns_epg_status=97,nns_modify_time='{$now}',nns_action='{$c2_task_info['nns_action']}',nns_epg_fail_time=nns_epg_fail_time+1 where nns_id in('".implode("','", array_keys($arr_temp_task))."')";
	        nl_execute_by_db($sql, $obj_dc->db());
	        return false;
	    }
	    $result_playbill['nns_live_media_import_id'] = $result_media_import_info['data_info'];
	    if($sp_id == 'bestv')
	    {
	        $sql_live = "select * from nns_live where nns_id ='{$result_live_media['nns_live_id']}'";
	        $result_live = nl_query_by_db($sql_live, $obj_dc->db());
	        if(!is_array($result_live) || empty($result_live))
	        {
	            return false;
	        }
	        $result_playbill['nns_live_media_import_id'] = $result_live['0']['nns_import_id'];
	    }
	    
	    $str_pic = $str_mapping = '';
	    $str_xml  ='<?xml version="1.0" encoding="UTF-8"?>';
	    $str_xml .='<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
	    $str_xml .=   '<Objects>';
	    $arr_temp_data = null;
	    foreach ($arr_group_playbill_info['data_info'] as $playbill_value)
	    {
	        // 组装C2注入的节目单工单xml信息
	        $int_begin_time = strtotime($playbill_value['nns_begin_time']);
	        $str_start_date = date('Ymd', $int_begin_time);
	        $str_start_time = date('His', $int_begin_time);
	        $str_duration = date('His', strtotime('today') + $playbill_value['nns_time_len']);
	        $arr_playbill_import_info = public_model_exec::get_asset_id_by_sp_id($sp_id, 'playbill', $playbill_value['nns_id'], $playbill_value);
	        if ($arr_playbill_import_info['ret'] != 0 || !isset($arr_playbill_import_info['data_info']) || strlen($arr_playbill_import_info['data_info']) == 0)
	        {
	            $sql = "update nns_mgtvbk_c2_task set nns_epg_status=97,nns_modify_time='{$now}',nns_action='{$c2_task_info['nns_action']}',nns_epg_fail_time=nns_epg_fail_time+1 where nns_id in('".implode("','", array_keys($arr_temp_task))."')";
	            nl_execute_by_db($sql, $obj_dc->db());
	            return false;
	        }
            $str_Action = ($playbill_value['nns_state'] !=1) ? ($playbill_value['nns_modify_time'] > $playbill_value['nns_create_time']) ? 'REGIST' : 'REGIST' : 'DELETE';
// 	        $str_Action = ($i==0) ? 'DELETE' : 'REGIST';
	        $str_xml .='<Object ElementType="ScheduleRecord" ContentID="' . $arr_playbill_import_info['data_info'] . '" PhysicalChannelID="'.$result_playbill['nns_live_media_import_id'].'" Action="' . $str_Action . '" Code="' . $arr_playbill_import_info['data_info'] . '">';
	        $str_xml .=   '<Property Name="Name">' . $playbill_value['nns_name'] . '</Property>';
	        $str_xml .=   '<Property Name="StartDate">' . $str_start_date . '</Property>';
	        $str_xml .=   '<Property Name="StartTime">' . $str_start_time . '</Property>';
	        $str_xml .=   '<Property Name="Duration">' . $str_duration . '</Property>';
	        $str_xml .=   '<Property Name="CPContentID">' . $playbill_value['nns_cp_id'] . '</Property>';
	        $str_xml .=   '<Property Name="Description">' . $playbill_value['nns_summary'] . '</Property>';
	        $str_xml .=   '<Property Name="Domain"></Property>';
	        $str_xml .=   '<Property Name="HotDgree">0</Property>';
	        $str_xml .='</Object>';
            $arr_temp_task_2[$arr_playbill_import_info['data_info']] = $playbill_value['nns_id'];
	        $obj_image = self::make_playbill_img($playbill_value, $str_Action,$arr_playbill_import_info['data_info'],$arr_sp_config);
	        if(strlen($obj_image['object_xml'])<1)
	        {
	            continue;
	        }
            $str_pic .=$obj_image['object_xml'];
            $str_mapping .=$obj_image['mapping_xml'];
	    }
	    if(strlen($str_mapping) >0)
	    {
	        $str_xml .=$str_pic;
	    }
	    $str_xml .='</Objects>';
	    if(strlen($str_mapping) >0)
	    {
	        $str_xml .='<Mappings>'.$str_mapping.'</Mappings>';
	    }
	    $str_xml .='</ADI>';
	    $import_obj = new import($arr_sp_config['epg_url'], dirname(dirname(dirname(__FILE__))).'/data/log/import/'.$c2_task_info['nns_org_id'],$c2_task_info['nns_org_id'],$result_live_media['nns_cp_id']);

	    $result = $import_obj->import_cms_playbill_info_batch($str_xml);
	    $re = json_decode($result, true);
	    if($re['code'] !=0)
	    {
	        $sql = "update nns_mgtvbk_c2_task set nns_epg_status=97,nns_modify_time='{$now}',nns_action='{$c2_task_info['nns_action']}',nns_epg_fail_time=nns_epg_fail_time+1 where nns_id in('".implode("','", array_keys($arr_temp_task))."')";
			nl_execute_by_db($sql, $obj_dc->db());
            return false;
	    }
        if(!isset($re['data']) && !is_array($re['data']))
        {
            $sql = "update nns_mgtvbk_c2_task set nns_epg_status=97,nns_modify_time='{$now}',nns_action='{$c2_task_info['nns_action']}',nns_epg_fail_time=nns_epg_fail_time+1 where nns_id in('".implode("','", array_keys($arr_temp_task))."')";
            nl_execute_by_db($sql, $obj_dc->db());
            return false;
        }
        $right_data = $error_data  =array();
        foreach ($re['data'] as $value)
        {
            if($value['ResultCode'] == 0)
            {
                if(isset($arr_temp_task_1[$arr_temp_task_2[$value['ContentID']]]) && is_array($arr_temp_task_1[$arr_temp_task_2[$value['ContentID']]]) && !empty($arr_temp_task_1[$arr_temp_task_2[$value['ContentID']]]))
                {
                    $right_data = array_merge($right_data,$arr_temp_task_1[$arr_temp_task_2[$value['ContentID']]]);
                }
            }
            else
            {
                if(isset($arr_temp_task_1[$arr_temp_task_2[$value['ContentID']]]) && is_array($arr_temp_task_1[$arr_temp_task_2[$value['ContentID']]]) && !empty($arr_temp_task_1[$arr_temp_task_2[$value['ContentID']]]))
                {
                    $error_data = array_merge($error_data,$arr_temp_task_1[$arr_temp_task_2[$value['ContentID']]]);
                }
            }
        }
        if(is_array($error_data) && !empty($error_data))
        {
            $sql = "update nns_mgtvbk_c2_task set nns_epg_status=97,nns_modify_time='{$now}',nns_action='{$c2_task_info['nns_action']}',nns_epg_fail_time=nns_epg_fail_time+1 where nns_id in('".implode("','", $error_data)."')";
            nl_execute_by_db($sql, $obj_dc->db());
        }
        if(is_array($right_data) && !empty($right_data))
        {
            $sql = "update nns_mgtvbk_c2_task set nns_epg_status=99,nns_modify_time='{$now}',nns_action='{$c2_task_info['nns_action']}' where nns_id in('".implode("','", $right_data)."')";
            nl_execute_by_db($sql, $obj_dc->db());
            foreach ($right_data as $value)
            {
                $queue_task_model = new queue_task_model();
                $queue_task_model->q_report_task($value);
                unset($queue_task_model);
                $queue_task_model = null;
            }
        }
        include_once dirname(dirname(dirname(__FILE__))) . "/v2/common.php";
        \ns_core\m_load::load("ns_core.m_config");
        $str_log_guid = np_guid_rand('playbill');
        $file_url = "epg_log/{$c2_task_info['nns_org_id']}/".date("Ymd")."/".date("H")."/";
        $result_dir = m_config::make_dir($file_url);
        if($result_dir['ret'] ==0)
        {
            file_put_contents($result_dir['data_info']['absolute_dir']."/{$str_log_guid}.xml", $str_xml);
            $file_url.="{$str_log_guid}.xml";
        }
        else
        {
            $file_url='';
        }
        
        $log_data = array (
            'nns_id' => $str_log_guid,
            'nns_video_id' => $result_live_media['nns_id'],
            'nns_video_type' => 'playbill',
            'nns_org_id' => $c2_task_info['nns_org_id'],
            'nns_create_time' => $now,
            'nns_action' => $c2_task_info['nns_action'],
            'nns_status' => $re['ret'],
            'nns_reason' => addslashes($re['reason']),
            'nns_data' => '',
            'nns_name' => $c2_task_info['nns_name'],
            'nns_file_dir' => $file_url,
        );
        nl_db_insert($obj_dc->db(), 'nns_mgtvbk_import_epg_log', $log_data);
        return true;
	}
	
	public static function make_playbill_img($playbill_value,$str_Action,$id,$arr_sp_config)
	{
	    $str_mapping = $str_xml='';
	    $img_ftp = trim(trim(trim($arr_sp_config['img_ftp'],'/'),'\\'));
	    foreach (self::$arr_image_playbill_type as $key=>$value)
	    {
	        $playbill_value[$key] = trim(trim(trim($playbill_value[$key],'/'),'\\'));
	        if(!isset($playbill_value[$key]) || strlen($playbill_value[$key]) <1)
	        {
	            continue;
	        }
	        $temp_str_id = md5("{$playbill_value['nns_id']}_{$key}_{$playbill_value[$key]}");
	        $str_xml .='<Object ElementType="Picture" ContentID="'.$temp_str_id.'" Action="'.$str_Action.'" Code="'.$temp_str_id.'">';
	        
// 	        $str_xml .=    '<Property Name="FileURL">'.$playbill_value[$key].'</Property>';
	        $str_xml .=    '<Property Name="FileURL">'.$img_ftp.'/'.$playbill_value[$key].'</Property>';
	        $str_xml .=    '<Property Name="Type">'.$value.'</Property>';
	        $str_xml .=    '<Property Name="Description"/>';
	        $str_xml .='</Object>';
	        
	        $str_mapping.='<Mapping ParentType="ScheduleRecord" ParentID="'.$id.'" ElementType="Picture" ElementID="'.$temp_str_id.'"	ParentCode="'.$id.'" ElementCode="'.$temp_str_id.'" Action="'.$str_Action.'">';
			$str_mapping.=   '<Property Name="Type">'.$value.'</Property>';
		    $str_mapping.='</Mapping>';
	    }
	    unset($playbill_value);
	    return array(
	        'object_xml'=>$str_xml,
	        'mapping_xml'=>$str_mapping,
	    );
	}
	
	
	/**
	 * 点播、直播、节目单 xml组装
	 * @param string $type
	 * @param string $data_vod
	 * @param string $data_index
	 * @param string $data_media
	 * @param string $data_vod_ex
	 * @param string $data_index_ex
	 * @param string $data_media_ex
	 * @return string
	 */
	static public function make_import_cms_xml($type='live',$data_vod=null,$data_index=null,$data_media=null,$data_vod_ex=null,$data_index_ex=null,$data_media_ex=null)
	{
		switch ($type)
		{
			case 'live':
				$str_assettype=2;
				break;
			case 'video':
				$str_assettype=1;
				break;
			case 'playbill':
				$str_assettype=2;
				break;
			default:
				return '';
		}
		$str_xml  = '<?xml version="1.0" encoding="UTF-8"?>';
		$str_xml .= '<assetcontent>';
		$str_xml .= 	'<assettype>'.$str_assettype.'</assettype>';
		$str_xml .= 	'<assetdesc>4</assetdesc>';
		if(!empty($data_vod) && is_array($data_vod))
		{
			$str_xml .= 	'<'.$type.'>';
			$str_operation = ($data_vod['nns_deleted'] !=1) ? 1 : 2;
			$str_xml .= 		'<assetoperation>'.$str_operation.'</assetoperation>';
			$str_xml .= 		'<base_info>';
			foreach ($data_vod as $key=>$val)
			{
				if (in_array($key, array('nns_id','nns_create_time','nns_modify_time','nns_org_id','nns_state','nns_check','nns_deleted','nns_depot_id','nns_domain')))
				{
					continue;
				}
				$key = str_replace('nns_', '', $key);
				$val = str_replace(array('&','<','>','"',"'"), '',$val);
				$str_xml .= 		'<'.$key.'>'.$val.'</'.$key.'>';
			}
			$str_xml .= 		'</base_info>';
			$str_xml .= 		'<ex_info>';
			if(!empty($data_vod_ex) && is_array($data_vod_ex))
			{
				foreach ($data_vod_ex as $vod_ex_val)
				{
					$str_xml .= 		'<'.$vod_ex_val['nns_key'].'>'.$vod_ex_val['nns_value'].'</'.$vod_ex_val['nns_key'].'>';
				}
			}
			$str_xml .= 		'</ex_info>';
			$str_xml .= 	'</'.$type.'>';
		}
		if(!empty($data_index) && is_array($data_index))
		{
			$str_xml .= 	'<'.$type.'_index>';
			$str_operation = ($data_index['nns_deleted'] !=1) ? 1 : 2;
			$str_xml .= 		'<assetoperation>'.$str_operation.'</assetoperation>';
			$str_xml .= 		'<base_info>';
			foreach ($data_index as $key=>$val)
			{
				if (in_array($key, array('nns_id','nns_create_time','nns_modify_time','nns_deleted')))
				{
					continue;
				}
				$key = str_replace('nns_', '', $key);
				$str_xml .= 		'<'.$key.'>'.$val.'</'.$key.'>';
			}
			$str_xml .= 		'</base_info>';
			$str_xml .= 		'<ex_info>';
			if(!empty($data_index_ex) && is_array($data_index_ex))
			{
				foreach ($data_index_ex as $index_ex_val)
				{
					$str_xml .= 		'<'.$index_ex_val['nns_key'].'>'.$index_ex_val['nns_value'].'</'.$index_ex_val['nns_key'].'>';
				}
			}
			$str_xml .= 		'</ex_info>';
			$str_xml .= 	'</'.$type.'_index>';
		}
		if(!empty($data_media) && is_array($data_media))
		{
			$str_xml .= 	'<'.$type.'_media>';
			$str_operation = ($data_media['nns_deleted'] !=1) ? 1 : 2;
			$str_xml .= 		'<assetoperation>'.$str_operation.'</assetoperation>';
			$str_xml .= 		'<base_info>';
			foreach ($data_media as $key=>$val)
			{
				if (in_array($key, array('nns_id','nns_create_time','nns_modify_time','nns_deleted','nns_domain','nns_drm_flag','nns_ext_url','nns_message_id')))
				{
					continue;
				}
				$key = str_replace('nns_', '', $key);
				if($key == 'drm_ext_info')
				{
				    $val = htmlspecialchars($val);
				}
				$str_xml .= 	'<'.$key.'>'.$val.'</'.$key.'>';
			}
			if(strlen($data_media['nns_ext_url']) <1)
			{
			    $str_xml .= 	'<ex_url>'.$data_media['nns_url'].'</ex_url>';
			}
			else
			{
			    $str_xml .= 	'<ex_url>'.$data_media['nns_ext_url'].'</ex_url>';
			}
			$str_xml .= 		'</base_info>';
			$str_xml .= 		'<ex_info>';
			if(!empty($data_media_ex) && is_array($data_media_ex))
			{
				foreach ($data_media_ex as $media_ex_val)
				{
					$str_xml .= 		'<'.$media_ex_val['nns_key'].'>'.$media_ex_val['nns_value'].'</'.$media_ex_val['nns_key'].'>';
				}
			}
			$str_xml .= 		'</ex_info>';
			$str_xml .= 	'</'.$type.'_media>';
		}
		$str_xml .= '</assetcontent>';
		return $str_xml;
	}
	
	
	/**
	 * 消息反馈上游
	 * @param unknown $asset_cdn_share_addr
	 * @param unknown $info
	 * @param unknown $sp_id
	 * @param unknown $action
	 * @param unknown $video_type
	 * @param unknown $str_message_id
	 * @param unknown $sp_config
	 * @param unknown $bool
	 * @return multitype:number mixed |Ambigous <multitype:number, multitype:number unknown mixed >
	 */
	static public function notify_cms_data($asset_cdn_share_addr,$info,$sp_id,$action,$video_type,$str_message_id,$bool,$arr_data=null)
	{
		$str_cdn_notify = '';
		$asset_cdn_share_addr = isset($asset_cdn_share_addr) ? trim(trim($asset_cdn_share_addr,'/'),'\\') : '';
		if(empty($asset_cdn_share_addr))
		{
			nn_log::write_log_message(LOG_MODEL_CMS,'媒资CDN共享地址未配置:'.$asset_cdn_share_addr,$sp_id,$video_type);
			self::$arr_error_data[]='媒资CDN共享地址未配置:'.$asset_cdn_share_addr;
			return self::return_error_data();
		}

		//获取sp的配置文件，检查反馈cdn的消息模式,feijian.gao 2017年4月5日18:12:52  -->start
		$dc = nl_get_dc(array (
				'db_policy' => NL_DB_READ,
				'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
		));
		$sp_config = nl_sp::get_sp_config($dc, $sp_id);
		$sp_config = isset($sp_config['data_info']['nns_config']) ? $sp_config['data_info']['nns_config'] : null;
		$str_sp_model = (isset($sp_config['cdn_message_feedback_model'])) ? $sp_config['cdn_message_feedback_model'] : "soap";
		$str_sp_model = ($str_sp_model == "soap") ? "unicom_zte" : $str_sp_model;
		unset($sp_config);
		//-->end

		include_once dirname(dirname(__FILE__)).'/cdn_notify/cdn_notify.class.php';
		$obj_cdn_notify = new nn_cdn_notify($sp_id,$info['nns_cp_id']);

		//soap返回值的描述 <feijian.gao@starcor.com>
		if(is_array($bool) && count($bool) !=0)
        {
            $str_ResultCode= ($bool === true) ? 0 : -1;
            $str_Description = ($bool === true) ? 'success' : 'error';
        }
        else
        {
            $str_ResultCode= $bool["ResultCode"];
            $str_Description = $bool["ErrorDescription"];
        }

		if($str_sp_model == 'http')
		{
			$result_cdn_notify = $obj_cdn_notify->make_unicom_d3_cdn_file($info,$str_ResultCode,$str_Description,$video_type,$action,$arr_data);
		}
		else
		{
			$result_cdn_notify = $obj_cdn_notify->make_cdn_file($info,$str_ResultCode,$str_Description,$video_type,$action,$arr_data);
		}
		if($result_cdn_notify['ret'] !=0 || !isset($result_cdn_notify['data_info']) || empty($result_cdn_notify['data_info']))
		{
			nn_log::write_log_message(LOG_MODEL_CMS,"生成CDN放FTP下拉文件失败".var_export($result_cdn_notify,true),$sp_id,$video_type);
			self::$arr_error_data[]="生成CDN放FTP下拉文件失败".var_export($result_cdn_notify,true);
			return self::return_error_data();
		}
		//根据不同的反馈模式，执行反馈
		if($str_sp_model == "http")
		{
			$data = file_get_contents($asset_cdn_share_addr.$result_cdn_notify['data_info'], true);
			$arr_message_info = nl_message::get_message_info($dc,array("nns_message_id"=>$str_message_id));
			$arr_ext_info = json_decode($arr_message_info["data_info"][0]["nns_ext_info"], true);
			$obj_curl = new np_http_curl_class();
			$result_notify = $obj_curl->do_request("post",$arr_ext_info["feedback_url"], $data);
			$curl_info = $obj_curl->curl_getinfo();
			$http_code = $curl_info['http_code'];
			if($http_code["http_code"] != "200")
			{
				nn_log::write_log_message(LOG_MODEL_CMS,'消息反馈结果：'.var_export($result_notify,true),$info['nns_cp_id'],$video_type);
				self::$arr_error_data[]='消息反馈结果：'.var_export($result_notify,true);
				return self::return_error_data();
			}
		}
		else
		{
			$cdn_include_file = dirname(dirname(dirname(__FILE__))).'/mgtv/'.$str_sp_model.'/content_deploy_client.php';
			if(!file_exists($cdn_include_file))
			{
				nn_log::write_log_message(LOG_MODEL_CMS,'CDN消息反馈文件不存在,文件路径:'.$cdn_include_file,$sp_id,$video_type);
				self::$arr_error_data[]='CDN消息反馈文件不存在,文件路径:'.$cdn_include_file;
				return self::return_error_data();
			}
			include_once $cdn_include_file;
			$content_deploy_obj = new content_deploy_client($info['nns_cp_id'],'starcor');
			$result_notify = $content_deploy_obj->ContentDeployResult($str_message_id, 0, 'success', $asset_cdn_share_addr.$result_cdn_notify['data_info']);
			unset($content_deploy_obj);
			unset($str_obj_name);
			if($result_notify->ResultCode != 0)
			{
				nn_log::write_log_message(LOG_MODEL_CMS,'消息反馈结果：'.var_export($result_notify,true),$info['nns_cp_id'],$video_type);
				self::$arr_error_data[]='消息反馈结果：'.var_export($result_notify,true);
				return self::return_error_data();
			}
		}

		$str_cdn_notify = $result_cdn_notify['data_info'];
		return self::return_right_data($str_cdn_notify);
	}
	
	
	
	
	/**
	 * 直播片源注入CDN主入口
	 * @param unknown $dc
	 * @param unknown $mix_live_media
	 * @param string $cdn_action
	 * @return Ambigous <Ambigous, array('ret'=>'状态码','reason'=>'原因','data'=>'数据'), multitype:NULL number string , number, mixed, multitype:NULL multitype:a r y s t i n g  array multitype:number  >|Ambigous <boolean, mixed, string, number, string>|multitype:number string
	 */
	static public function import_cdn_live_media($dc,$mix_live_media,$cdn_action='cdn')
	{
		$arr_id = (is_string($mix_live_media)) ? array($mix_live_media) : $mix_live_media;
		#TODO
		$asset_cdn_share_addr='ftp://cdn_ftp:cdn_ftp@10.100.106.13:21/';
		foreach ($arr_id as $val)
		{
			//查询消息队列
			$queue_info = nl_cdn_live_media_queue::query_by_id($dc, $val);
			if($queue_info['ret'] !=0)
			{
				nn_log::write_log_message(LOG_MODEL_MSP,'行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".var_export($queue_info,true),CDN_ORG_ID,'live_media','message',ORG_ID);
				return $queue_info;
			}
			if(!isset($queue_info['data_info']) || !is_array($queue_info['data_info']) || empty($queue_info['data_info']))
			{
				nn_log::write_log_message(LOG_MODEL_MSP,'行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".'查询队列ID信息已不存在,队列ID：'.$val,CDN_ORG_ID,'live_media','message',ORG_ID);
				continue;
			}
			$queue_info = $queue_info['data_info'];
			$arr_queue_log = array(
					'nns_name'=>$queue_info['nns_name'],
					'nns_cdn_queue_id'=>$queue_info['nns_id'],
					'nns_action'=>$queue_info['nns_action'],
					'nns_cp_id'=>$queue_info['nns_cp_id'],
			);
			$arr_queue_edit = array();
			$op_action = 'regist';
			if($queue_info['nns_action'] == '1')
			{
				$op_action = 'update';
			}
			else if($queue_info['nns_action'] == '2')
			{
				$op_action = 'delete';
			}
			//如果是消息响应上游   且  是等待响应或则正在响应 或则响应失败
			if($cdn_action == 'notify' && in_array($queue_info['nns_import_notify_state'], array('0','1','3')))
			{
				$array_media = nl_live_media::query_by_id($dc, $queue_info['nns_live_media_id']);
				if($array_media['ret'] !=0)
				{
					nl_cdn_live_media_queue::edit($dc, array('nns_import_cdn_state'=>0), $queue_info['nns_id']);
					$arr_queue_log['nns_cdn_send'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".var_export($array_media,true);
					nn_log::write_log_message(LOG_MODEL_MSP,$arr_queue_log['nns_cdn_send'],$queue_info['nns_cp_id'],'live_media','message',ORG_ID);
					nl_cdn_live_media_queue_log::add($dc,$arr_queue_log);
					return $array_media;
				}
				if(!isset($array_media['data_info']) || !is_array($array_media['data_info']) || empty($array_media['data_info']))
				{
					nl_cdn_live_media_queue::edit($dc, array('nns_import_cdn_state'=>0), $queue_info['nns_id']);
					$arr_queue_log['nns_cdn_send'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".'查直播片源无数据,频道ID:'.$queue_info['nns_live_media_id'];
					nn_log::write_log_message(LOG_MODEL_MSP,$arr_queue_log['nns_cdn_send'],$queue_info['nns_cp_id'],'live_media','message',ORG_ID);
					nl_cdn_live_media_queue_log::add($dc,$arr_queue_log);
					continue;
				}
				$info = array(
						'nns_cp_id'=>$array_media['nns_cp_id'],
						'nns_import_id'=>$array_media['nns_content_id'],
				);
				if(!in_array($queue_info['nns_import_cdn_state'], array(2,3)))
				{
					continue;
				}
				$bool = ($queue_info['nns_import_cdn_state'] =='2') ? false : true;
				//消息反馈响应
				$re_notify = self::notify_cms_data($asset_cdn_share_addr,$info,'starcor',$op_action,'live_media',$queue_info['nns_message_id'],$bool);
				nn_log::write_log_message(LOG_MODEL_MSP,'消息反馈结果：'.var_export($re_notify,true),$queue_info['nns_cp_id'],'live_media','message',ORG_ID);
				$arr_queue_log['nns_firm_notify'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";信息:".var_export($re_notify,true);
				$arr_queue_edit['nns_import_notify_state'] = (isset($re_notify['ret']) && $re_notify['ret'] ==0) ? 2 : 3;
				$result_queue=nl_cdn_live_media_queue::edit($dc, $arr_queue_edit, $queue_info['nns_id']);
				nn_log::write_log_message(LOG_MODEL_MSP,'更新队列数据信息：'.var_export($result_queue,true),$queue_info['nns_cp_id'],'live_media','message',ORG_ID);
				nl_cdn_live_media_queue_log::add($dc, $arr_queue_log);
				return $result_queue;
			}
			//如果是CDN注入   且  是等待注入或则正在注入 或则注入失败
			else if($cdn_action == 'cdn' && in_array($queue_info['nns_import_cdn_state'], array('0','1','3')))
			{
				$array_live = nl_live::query_by_id($dc, $queue_info['nns_live_id']);
				if($array_live['ret'] !=0)
				{
					nl_cdn_live_media_queue::edit($dc, array('nns_import_cdn_state'=>0), $queue_info['nns_id']);
					$arr_queue_log['nns_cdn_send'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".var_export($array_live,true);
					nn_log::write_log_message(LOG_MODEL_MSP,$arr_queue_log['nns_cdn_send'],$queue_info['nns_cp_id'],'live_media','message',ORG_ID);
					nl_cdn_live_media_queue_log::add($dc,$arr_queue_log);
					return $array_live;
				}
				if(!isset($array_live['data_info']) || !is_array($array_live['data_info']) || empty($array_live['data_info']))
				{
					nl_cdn_live_media_queue::edit($dc, array('nns_import_cdn_state'=>0), $queue_info['nns_id']);
					$arr_queue_log['nns_cdn_send'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".'查直播频道无数据,频道ID:'.$queue_info['nns_live_id'];
					nn_log::write_log_message(LOG_MODEL_MSP,$arr_queue_log['nns_cdn_send'],$queue_info['nns_cp_id'],'live_media','message',ORG_ID);
					nl_cdn_live_media_queue_log::add($dc,$arr_queue_log);
					continue;
				}
				$array_live = $array_live['data_info'];
				$array_media = nl_live_media::query_by_id($dc, $queue_info['nns_live_media_id']);
				if($array_media['ret'] !=0)
				{
					nl_cdn_live_media_queue::edit($dc, array('nns_import_cdn_state'=>0), $queue_info['nns_id']);
					$arr_queue_log['nns_cdn_send'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".var_export($array_media,true);
					nn_log::write_log_message(LOG_MODEL_MSP,$arr_queue_log['nns_cdn_send'],$queue_info['nns_cp_id'],'live_media','message',ORG_ID);
					nl_cdn_live_media_queue_log::add($dc,$arr_queue_log);
					return $array_media;
				}
				if(!isset($array_media['data_info']) || !is_array($array_media['data_info']) || empty($array_media['data_info']))
				{
					nl_cdn_live_media_queue::edit($dc, array('nns_import_cdn_state'=>0), $queue_info['nns_id']);
					$arr_queue_log['nns_cdn_send'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".'查直播片源无数据,频道ID:'.$queue_info['nns_live_media_id'];
					nn_log::write_log_message(LOG_MODEL_MSP,$arr_queue_log['nns_cdn_send'],$queue_info['nns_cp_id'],'live_media','message',ORG_ID);
					nl_cdn_live_media_queue_log::add($dc,$arr_queue_log);
					continue;
				}
				$array_media = $array_media['data_info'];
				$arr_cp_config = nl_cp::query_by_id($dc, $array_media['nns_cp_id']);
				if($arr_cp_config['ret'] !=0)
				{
					nl_cdn_live_media_queue::edit($dc, array('nns_import_cdn_state'=>0), $queue_info['nns_id']);
					$arr_queue_log['nns_cdn_send'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".var_export($arr_cp_config,true);
					nn_log::write_log_message(LOG_MODEL_MSP,$arr_queue_log['nns_cdn_send'],$queue_info['nns_cp_id'],'live_media','message',ORG_ID);
					nl_cdn_live_media_queue_log::add($dc,$arr_queue_log);
					return $array_media;
				}
				$arr_cp_config = (isset($arr_cp_config['data_info']['nns_config']) && strlen($arr_cp_config['data_info']['nns_config']) > 0) ? json_decode($arr_cp_config['data_info']['nns_config'],true) : null;
				
				//MSP注入
				$re_msp = self::import_cdn_live_data($array_live,$array_media,$op_action,$arr_cp_config);
				nn_log::write_log_message(LOG_MODEL_MSP,'MSP注入结果：'.var_export($re_msp,true),$queue_info['nns_cp_id'],'live_media','message',ORG_ID);
				$arr_queue_log['nns_cdn_notify'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";信息:".var_export($re_msp,true);
				$arr_queue_edit['nns_import_cdn_state'] = (isset($re_msp['ret']) && $re_msp['ret'] == 0) ? 2 : 3;
				$info = array(
						'nns_cp_id'=>$array_media['nns_cp_id'],
						'nns_import_id'=>$array_media['nns_content_id'],
				);
				//消息反馈响应
				$re_notify = self::notify_cms_data($asset_cdn_share_addr,$info,'starcor',$op_action,'live_media',$queue_info['nns_message_id'],(isset($re_msp['ret']) && $re_msp['ret'] == 0) ? true : false);
				nn_log::write_log_message(LOG_MODEL_MSP,'消息反馈结果：'.var_export($re_notify,true),$queue_info['nns_cp_id'],'live_media','message',ORG_ID);
				$arr_queue_log['nns_firm_notify'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";信息:".var_export($re_notify,true);
				$arr_queue_edit['nns_import_notify_state'] = (isset($re_notify['ret']) && $re_notify['ret'] == 0) ? 2 : 3;
				//CMS注入信息
				#TODO
				$result_queue=nl_cdn_live_media_queue::edit($dc, $arr_queue_edit, $queue_info['nns_id']);
				nn_log::write_log_message(LOG_MODEL_MSP,'更新队列数据信息：'.var_export($result_queue,true),$queue_info['nns_cp_id'],'live_media','message',ORG_ID);
				nl_cdn_live_media_queue_log::add($dc, $arr_queue_log);
				return $result_queue;
			}
			//如果是CMS注入   且  是等待注入或则正在注入 或则注入失败
			else if($cdn_action == 'cms' && in_array($queue_info['nns_import_cms_state'], array('0','1','3')))
			{
				#TODO
			}
			else
			{
				self::$arr_error_data[]='无此操作方式';
				nn_log::write_log_message(LOG_MODEL_MSP,'行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".var_export(self::$arr_error_data,true),$queue_info['nns_cp_id'],'live_media','message',ORG_ID);
				return self::return_error_data();
			}
		}
		return self::return_right_data();
	}
	
	/**
	 * MSP 平台直播片源消息注入
	 * @param unknown $live_info
	 * @param unknown $media_list
	 * @param unknown $action
	 * @return multitype:number string |mixed
	 */
	static public function import_cdn_live_data($live_info,$media_list,$action,$arr_cp_config=null,$arr_producer=null,$arr_message_request_data=null)
	{
		global $g_core_url;
		$str_core_url = $g_core_url;
		unset($g_core_url);
		$import_obj = new import($str_core_url);
		if(!is_array($media_list) || empty($media_list) || !is_array($live_info) || empty($live_info))
		{
			return array(
					'ret'=>1,
					'reason'=>"数据为空：".var_export(array('live_info'=>$live_info,'media'=>$media_list,))
			);
		}
		$str_template_id = (isset($arr_cp_config['cdn_handele_live_media_template'])) ? $arr_cp_config['cdn_handele_live_media_template'] : 'live_trans_cdn_module';
		if($action != 'destroy' && $action !='delete')
		{
			$obj_dom = new DOMDocument("1.0", 'utf-8');
			$obj_live_list = self::make_xml($obj_dom, 'live_list');
			$temp_time_shift = strpos(strtolower($media_list['nns_media_caps']), 'tstv') === false ? 0 : 1;
			$temp_timeshift_delay = isset($media_list['nns_timeshift_delay']) ? $media_list['nns_timeshift_delay'] : 0;
			$temp_timeshift_delay = $temp_timeshift_delay *60;
			$temp_storage_status = (isset($media_list['nns_storage_status']) && $media_list['nns_storage_status'] == 0) ? 0 : 1;
			$temp_storage_delay = (isset($media_list['nns_storage_delay']) && $media_list['nns_storage_delay'] >0) ? floor($media_list['nns_storage_delay']/3600) : 7*24;
			$obj_live=self::make_xml($obj_dom, 'live',null,null,$obj_live_list);
			self::make_xml($obj_dom, 'live_id',urlencode($media_list['nns_content_id']),null,$obj_live);
			self::make_xml($obj_dom, 'live_name',$live_info['nns_name'],null,$obj_live);
			self::make_xml($obj_dom, 'live_address',$media_list['nns_url'],null,$obj_live);
			self::make_xml($obj_dom, 'time_shift',$temp_time_shift,null,$obj_live);
			self::make_xml($obj_dom, 'time_shift_duration',$temp_timeshift_delay,null,$obj_live);
			self::make_xml($obj_dom, 'tvod_status',$temp_storage_status,null,$obj_live);
			self::make_xml($obj_dom, 'storage_duration',$temp_storage_delay,null,$obj_live);
			self::make_xml($obj_dom, 'template_id',$str_template_id,null,$obj_live);
			self::make_xml($obj_dom, 'status',$media_list['nns_state'],null,$obj_live);
			self::make_xml($obj_dom, 'bitrate',$media_list['nns_kbps'],null,$obj_live);
			if(isset($arr_producer['nns_import_source']))
			{
			    self::make_xml($obj_dom, 'cp_id',$media_list['nns_import_source'],null,$obj_live);
			    self::make_xml($obj_dom, 'import_from',$media_list['nns_cp_id'],null,$obj_live);
			    self::make_xml($obj_dom, 'domain',$media_list['nns_domain'],null,$obj_live);
			    self::make_xml($obj_dom, 'cms_id',$media_list['nns_cp_id'],null,$obj_live);
			    self::make_xml($obj_dom, 'voda_status',$temp_storage_status,null,$obj_live);
			    self::make_xml($obj_dom, 'nmds_id',$arr_producer['nns_node_id'],null,$obj_live);
			}
			else
			{
			    self::make_xml($obj_dom, 'import_from',$media_list['nns_cp_id'],null,$obj_live);
			    self::make_xml($obj_dom, 'cms_id',$media_list['nns_cp_id'],null,$obj_live);
			}
			$str_xml_content = $obj_dom->saveXML();
			$result=$import_obj->import_live_info($str_xml_content,$media_list['nns_content_id']);
			$result = json_decode($result,true);
			return $result;
		}
		global $g_core_userid;
		$str_core_userid = $g_core_userid;
		unset($g_core_userid);
		$result=$import_obj->delete_live_info($media_list['nns_content_id'], $str_core_userid,$media_list['nns_cp_id'],1,1,$arr_producer,$arr_message_request_data);
		$result = json_decode($result,true);
		return $result;
	}
	
	/**
	 * CDN注入节目单主入口
	 * @param unknown $dc
	 * @param unknown $mix_playbill
	 * @param string $cdn_action
	 * @return Ambigous <Ambigous, array('ret'=>'状态码','reason'=>'原因','data'=>'数据'), multitype:NULL number string , number, mixed, multitype:NULL multitype:a r y s t i n g  array multitype:number  >|multitype:number mixed
	 */
	static public function import_cdn_playbill_v2($dc,$mix_playbill,$cdn_action='cdn')
	{
		$arr_id = (is_string($mix_playbill)) ? array($mix_playbill) : $mix_playbill;
		#TODO
		$asset_cdn_share_addr='ftp://cdn_ftp:cdn_ftp@10.100.106.13:21/';
		foreach ($arr_id as $val)
		{
			//查询消息队列
			$queue_info = nl_cdn_playbill_queue::query_by_id($dc, $val);
			if($queue_info['ret'] !=0)
			{
				nn_log::write_log_message(LOG_MODEL_MSP,'行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".var_export($queue_info,true),CDN_ORG_ID,'playbill','message',ORG_ID);
				return $queue_info;
			}
			if(!isset($queue_info['data_info']) || !is_array($queue_info['data_info']) || empty($queue_info['data_info']))
			{
				nn_log::write_log_message(LOG_MODEL_MSP,'行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".'查询队列ID信息已不存在,队列ID：'.$val,CDN_ORG_ID,'playbill','message',ORG_ID);
				continue;
			}
			$queue_info = $queue_info['data_info'];
			$arr_queue_log = array(
					'nns_name'=>$queue_info['nns_name'],
					'nns_cdn_queue_id'=>$queue_info['nns_id'],
					'nns_action'=>$queue_info['nns_action'],
					'nns_cp_id'=>$queue_info['nns_cp_id'],
			);
			$arr_queue_edit = array();
			$op_action = 'regist';
			if($queue_info['nns_action'] == '1')
			{
				$op_action = 'update';
			}
			else if($queue_info['nns_action'] == '2')
			{
				$op_action = 'delete';
			}
			//如果是消息响应上游   且  是等待响应或则正在响应 或则响应失败
			if($cdn_action == 'notify' && in_array($queue_info['nns_import_notify_state'], array('0','1','3')))
			{
				$array_media = nl_live_media::query_by_id($dc, $queue_info['nns_live_media_id']);
				if($array_media['ret'] !=0)
				{
					nl_cdn_live_index_queue::edit($dc, array('nns_import_cdn_state'=>0), $queue_info['nns_id']);
					$arr_queue_log['nns_cdn_send'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".var_export($array_media,true);
					nn_log::write_log_message(LOG_MODEL_MSP,$arr_queue_log['nns_cdn_send'],$queue_info['nns_cp_id'],'playbill','message',ORG_ID);
					nl_cdn_playbill_queue_log::add($dc,$arr_queue_log);
					return $array_media;
				}
				if(!isset($array_media['data_info']) || !is_array($array_media['data_info']) || empty($array_media['data_info']))
				{
					nl_cdn_live_index_queue::edit($dc, array('nns_import_cdn_state'=>0), $queue_info['nns_id']);
					$arr_queue_log['nns_cdn_send'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".'查直播片源无数据,频道ID:'.$queue_info['nns_live_media_id'];
					nn_log::write_log_message(LOG_MODEL_MSP,$arr_queue_log['nns_cdn_send'],$queue_info['nns_cp_id'],'playbill','message',ORG_ID);
					nl_cdn_playbill_queue_log::add($dc,$arr_queue_log);
					continue;
				}
				$info = array(
						'nns_cp_id'=>$array_media['nns_cp_id'],
						'nns_import_id'=>$array_media['nns_content_id'],
				);
				if(!in_array($queue_info['nns_import_cdn_state'], array(2,3)))
				{
					continue;
				}
				$bool = ($queue_info['nns_import_cdn_state'] =='2') ? false : true;
				//消息反馈响应
				$re_notify = self::notify_cms_data($asset_cdn_share_addr,$info,'starcor',$op_action,'live_media',$queue_info['nns_message_id'],$bool);
				nn_log::write_log_message(LOG_MODEL_MSP,'消息反馈结果：'.var_export($re_notify,true),$queue_info['nns_cp_id'],'playbill','message',ORG_ID);
				$arr_queue_log['nns_firm_notify'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";信息:".var_export($re_notify,true);
				$arr_queue_edit['nns_import_notify_state'] = (isset($re_notify['ret']) && $re_notify['ret'] ==0) ? 2 : 3;
				$result_queue=nl_cdn_playbill_queue::edit($dc, $arr_queue_edit, $queue_info['nns_id']);
				nn_log::write_log_message(LOG_MODEL_MSP,'更新队列数据信息：'.var_export($result_queue,true),$queue_info['nns_cp_id'],'playbill','message',ORG_ID);
				nl_cdn_playbill_queue_log::add($dc, $arr_queue_log);
				return $result_queue;
			}
			//如果是CDN注入   且  是等待注入或则正在注入 或则注入失败
			else if($cdn_action == 'cdn' && in_array($queue_info['nns_import_cdn_state'], array('0','1','3')))
			{
				//查询节目单
				$array_playbill = nl_playbill::query_by_id($dc, $queue_info['nns_playbill_id']);
				if($array_playbill['ret'] !=0)
				{
					nl_cdn_live_index_queue::edit($dc, array('nns_import_cdn_state'=>0), $queue_info['nns_id']);
					$arr_queue_log['nns_cdn_send'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".var_export($array_playbill,true);
					nn_log::write_log_message(LOG_MODEL_MSP,$arr_queue_log['nns_cdn_send'],$queue_info['nns_cp_id'],'playbill','message',ORG_ID);
					nl_cdn_playbill_queue_log::add($dc,$arr_queue_log);
					return $array_playbill;
				}
				if(!isset($array_playbill['data_info']) || !is_array($array_playbill['data_info']) || empty($array_playbill['data_info']))
				{
					nl_cdn_live_index_queue::edit($dc, array('nns_import_cdn_state'=>0), $queue_info['nns_id']);
					$arr_queue_log['nns_cdn_send'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".'查节目单无数据,节目单ID:'.$queue_info['nns_playbill_id'];
					nn_log::write_log_message(LOG_MODEL_MSP,$arr_queue_log['nns_cdn_send'],$queue_info['nns_cp_id'],'playbill','message',ORG_ID);
					nl_cdn_playbill_queue_log::add($dc,$arr_queue_log);
					continue;
				}
				$array_playbill = $array_playbill['data_info'];
				//查询直播频道   目前没用  （预留）
				$array_live = nl_live::query_by_id($dc, $array_playbill['nns_live_id']);
				if($array_live['ret'] !=0)
				{
					nl_cdn_live_index_queue::edit($dc, array('nns_import_cdn_state'=>0), $queue_info['nns_id']);
					$arr_queue_log['nns_cdn_send'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".var_export($array_live,true);
					nn_log::write_log_message(LOG_MODEL_MSP,$arr_queue_log['nns_cdn_send'],$queue_info['nns_cp_id'],'playbill','message',ORG_ID);
					nl_cdn_playbill_queue_log::add($dc,$arr_queue_log);
					return $array_live;
				}
				if(!isset($array_live['data_info']) || !is_array($array_live['data_info']) || empty($array_live['data_info']))
				{
					nl_cdn_live_index_queue::edit($dc, array('nns_import_cdn_state'=>0), $queue_info['nns_id']);
					$arr_queue_log['nns_cdn_send'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".'查直播频道无数据,频道ID:'.$array_playbill['nns_live_id'];
					nn_log::write_log_message(LOG_MODEL_MSP,$arr_queue_log['nns_cdn_send'],$queue_info['nns_cp_id'],'playbill','message',ORG_ID);
					nl_cdn_playbill_queue_log::add($dc,$arr_queue_log);
					continue;
				}
				$array_live = $array_live['data_info'];
				$array_media = nl_live_media::query_by_id($dc, $array_playbill['nns_live_media_id']);
				if($array_media['ret'] !=0)
				{
					nl_cdn_live_index_queue::edit($dc, array('nns_import_cdn_state'=>0), $queue_info['nns_id']);
					$arr_queue_log['nns_cdn_send'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".var_export($array_media,true);
					nn_log::write_log_message(LOG_MODEL_MSP,$arr_queue_log['nns_cdn_send'],$queue_info['nns_cp_id'],'playbill','message',ORG_ID);
					nl_cdn_playbill_queue_log::add($dc,$arr_queue_log);
					return $array_media;
				}
				if(!isset($array_media['data_info']) || !is_array($array_media['data_info']) || empty($array_media['data_info']))
				{
					nl_cdn_live_index_queue::edit($dc, array('nns_import_cdn_state'=>0), $queue_info['nns_id']);
					$arr_queue_log['nns_cdn_send'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".'查直播片源无数据,频道ID:'.$array_playbill['nns_live_media_id'];
					nn_log::write_log_message(LOG_MODEL_MSP,$arr_queue_log['nns_cdn_send'],$queue_info['nns_cp_id'],'playbill','message',ORG_ID);
					nl_cdn_playbill_queue_log::add($dc,$arr_queue_log);
					continue;
				}
				$array_media = $array_media['data_info'];
				//MSP注入
				$re_msp = self::import_cdn_playbill_data($array_live['nns_import_id'],$array_media['nns_content_id'],$array_playbill,$op_action);
				nn_log::write_log_message(LOG_MODEL_MSP,'MSP注入结果：'.var_export($re_msp,true),$queue_info['nns_cp_id'],'playbill','message',ORG_ID);
				$arr_queue_log['nns_cdn_notify'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";信息:".var_export($re_msp,true);
				$arr_queue_edit['nns_import_cdn_state'] = (isset($re_msp['ret']) && $re_msp['ret'] == 0) ? 2 : 3;
				$info = array(
						'nns_cp_id'=>$array_playbill['nns_cp_id'],
						'nns_import_id'=>$array_playbill['nns_playbill_import_id'],
				);
				//消息反馈响应
				$re_notify = self::notify_cms_data($asset_cdn_share_addr,$info,'starcor',$op_action,'playbill',$queue_info['nns_message_id'],(isset($re_msp['ret']) && $re_msp['ret'] == 0) ? true : false);
				nn_log::write_log_message(LOG_MODEL_MSP,'消息反馈结果：'.var_export($re_notify,true),$queue_info['nns_cp_id'],'playbill','message',ORG_ID);
				$arr_queue_log['nns_firm_notify'] = '行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";信息:".var_export($re_notify,true);
				$arr_queue_edit['nns_import_notify_state'] = (isset($re_notify['ret']) && $re_notify['ret'] == 0) ? 2 : 3;
				//CMS注入信息
				#TODO
				$result_queue=nl_cdn_playbill_queue::edit($dc, $arr_queue_edit, $queue_info['nns_id']);
				nn_log::write_log_message(LOG_MODEL_MSP,'更新队列数据信息：'.var_export($result_queue,true),$queue_info['nns_cp_id'],'playbill','message',ORG_ID);
				nl_cdn_playbill_queue_log::add($dc, $arr_queue_log);
				return $result_queue;
			}
			//如果是CMS注入   且  是等待注入或则正在注入 或则注入失败
			else if($cdn_action == 'cms' && in_array($queue_info['nns_import_cms_state'], array('0','1','3')))
			{
				#TODO
			}
			else
			{
				self::$arr_error_data[]='无此操作方式';
				nn_log::write_log_message(LOG_MODEL_MSP,'行号：'.__LINE__.";方法：".__FUNCTION__.";文件路径：".__DIR__.";错误信息:".var_export(self::$arr_error_data,true),$queue_info['nns_cp_id'],'playbill','message',ORG_ID);
				return self::return_error_data();
			}
		}
		return self::return_right_data();
	}
	
	/**
	 * 节目单注入MSP
	 * @param unknown $cp_id
	 * @param unknown $live_import_id
	 * @param unknown $media_import_id
	 * @param unknown $arr_playbill
	 * @param unknown $action
	 * @return multitype:number string |mixed
	 */
	static public function import_cdn_playbill_data($live_import_id,$media_import_id,$arr_playbill,$action,$arr_producer=null,$arr_message_request_data=null)
	{
		global $g_core_url;
		$str_core_url = $g_core_url;
		unset($g_core_url);
		$import_obj = new import($str_core_url);
		if(!is_array($arr_playbill) || empty($arr_playbill))
		{
			return array(
					'ret'=>1,
					'reason'=>"数据为空：".var_export(array('playbill_info'=>$arr_playbill))
			);
		}
		if($action != 'destroy' && $action != 'delete')
		{
			$result=$import_obj->import_playbill_info($live_import_id,$media_import_id,$arr_playbill,$action,$arr_producer,$arr_message_request_data);
			$result = json_decode($result,true);
		}
		else
		{
			global $g_core_userid;
			$str_core_userid = $g_core_userid;
			unset($g_core_userid);
			$result=$import_obj->delete_playbill_info($live_import_id,$media_import_id,$arr_playbill,$str_core_userid,$arr_producer,$arr_message_request_data);
			$result = json_decode($result,true);
		}
		return $result;
	}

	/**
	 * 点播片源注入msp（添加、删除）
	 * @param $str_import_id
	 * @param $arr_media_info
	 * @param $str_cp_id
	 * @param $action
	 * @return array|mixed|string
	 */
	public static function import_cdn_vod_media($arr_media_info, $str_cp_id, $action,$arr_message_request_data=null)
	{
		global $g_core_url;
		$str_core_url = $g_core_url;
		unset($g_core_url);
		$import_obj = new import($str_core_url);
		if(!is_array($arr_media_info) || empty($arr_media_info))
		{
			return array(
					'ret'=>1,
					'reason'=>"数据为空：".var_export(array('media_info'=>$arr_media_info))
			);
		}
		if(!in_array(strtolower($action), array('destroy','delete')))
		{
		    if(strtolower($action) == "update")
		    {
        	    global $g_core_userid;
        		$str_core_userid = $g_core_userid;
        		unset($g_core_userid);
        		$result=$import_obj->delete_msp_vod_media($arr_media_info['nns_media_import_id'], $str_core_userid,$arr_media_info,$arr_message_request_data);
        		$result = json_decode($result,true);
		    }
			$result=$import_obj->import_msp_vod_media($arr_media_info, $str_cp_id,$arr_message_request_data);
			$result = json_decode($result,true);
		}
		else
		{
			global $g_core_userid;
			$str_core_userid = $g_core_userid;
			unset($g_core_userid);
			$result=$import_obj->delete_msp_vod_media($arr_media_info['nns_media_import_id'], $str_core_userid,$arr_media_info,$arr_message_request_data);
			$result = json_decode($result,true);
		}
		return $result;
	}
	
	
	public static function make_xml($obj_dom,$key, $val = null, $arr_attr = null,$parent=null)
	{
		if(is_null($parent))
		{
			$parent=$obj_dom;
		}
		$$key = isset($val) ? $obj_dom->createElement($key, $val) : $obj_dom->createElement($key);
		if (!empty($arr_attr) && is_array($arr_attr))
		{
			foreach ($arr_attr as $attr_key => $attr_val)
			{
				$domAttribute = $obj_dom->createAttribute($attr_key);
				$domAttribute->value = $attr_val;
				$$key->appendChild($domAttribute);
				$obj_dom->appendChild($$key);
			}
		}
		$parent->appendChild($$key);
		//unset($dom);
		unset($parent);
		return $$key;
	}
	
	
	/**
	 * 分集打点信息注入cms
	 * @param string $index_id 分集guid
	 * @param array $data 打点信息数组
	 * @author liangpan
	 * @date 2015-07-08
	 */
	static public function import_clip_seekpoint($index_id , $data)
	{
		if(empty($index_id) ||  empty($data))
		{
			return;
		}
		$str_temp='';
		foreach ($data as $value)
		{
			$str_temp.='<item begin="'.$value['nns_begin'] .'" end="' . $value['nns_end'] . '" type="'.$value['nns_type'].'" name="'.$value['nns_name'].'" img="'.$value['nns_image'].'" ></item>';
		}
		$seekpoint_xml=empty($str_temp) ? null : '<metadata>' . $str_temp . '</metadata>';
		$url = IMPROT_EPG_URL;
		$log = IMPROT_EPG_LOG;
		$import_obj = new import($url, $log);
		return  $import_obj->import_video_clip_seekpoint($index_id, $seekpoint_xml);
		
	}
	
	static public function import_clip($nns_id, $action_epg = null)
	{

		include_once dirname(dirname(__FILE__)) . '/import/import.class.php';
//		$db_r = nn_get_db(NL_DB_READ);
//		$db_w = nn_get_db(NL_DB_WRITE);
		$dc = nl_get_dc(array(
		    'db_policy' =>  NL_DB_READ | NL_DB_WRITE,
		    'cache_policy' => NP_KV_CACHE_TYPE_NULL
		   )
		);
		$action = 'add';
		if (!empty($action_epg))
		{
			$action = $action_epg;
		}
		$sql = "select * from nns_mgtvbk_c2_task where nns_id='$nns_id'";
		$c2_task_info = nl_db_get_one($sql, $dc->db());
		if (empty($c2_task_info))
		{
			return false;
		}
		if ($c2_task_info['nns_type'] != 'index')
		{
			return false;
		}
		$sp_id = $c2_task_info['nns_org_id'];
		
		//注入片源之前   先注入分集

		$video_c2_info = nl_db_get_one("select * from nns_mgtvbk_c2_task where nns_org_id='$sp_id' and  nns_type='video' and nns_ref_id='{$c2_task_info['nns_src_id']}'", $dc->db());
		if ($video_c2_info['nns_action'] != 'destroy')
		{
			//self::import_asset($video_c2_info['nns_id']);
		}
		
		include_once dirname(dirname(__FILE__)) . '/' . $sp_id . '/define.php';
		$url = IMPROT_EPG_URL;
		$log = IMPROT_EPG_LOG;
		
		
		if (!empty($c2_task_info))
		{
			$action = $c2_task_info['nns_action'];
		}
		
		$sql = "select * from nns_vod_index where nns_id='{$c2_task_info['nns_ref_id']}'";
		$info = nl_db_get_one($sql, $dc->db());
		$cp_id = isset($info['nns_cp_id']) ? $info['nns_cp_id'] : null;
		// 获取CP配置
		$result_cp = nl_cp::query_by_id($dc, $cp_id);
		$cp_config = json_decode($result_cp['data_info']['nns_config'], true);
		
		$import_obj = new import($url, $log,'',$cp_id);
		//if(isset($info['nns_deleted'])&& (int)$info['nns_deleted']==1){
		//	$action = 'destroy';
		//}
		$seekpoint_xml=null;
		$str_vod_id = null;
		$str_vod_index =null;
		if (empty($info) || $info['nns_deleted'] == 1)
		{
			$action == 'destroy';
			nl_execute_by_db("update nns_mgtvbk_c2_task set nns_action='destroy',nns_epg_status=97 where nns_id='$nns_id'", $dc->db());
			
		}
		else
		{
			$str_vod_id = $info['nns_vod_id'];
			$str_vod_index = $info['nns_index'];
			//梁攀   这儿是查询分集的打点信息
			$sql_seekpoint="select * from nns_vod_index_seekpoint where nns_video_id='{$str_vod_id}' and nns_video_index='{$str_vod_index}'";
			$result_seekpoint=nl_query_by_db($sql_seekpoint, $dc->db());
			if(!$result_seekpoint)
			{
				@error_log("model_query_seekpoint,sql=" . $sql_seekpoint . ",nns_vod_id=" . $str_vod_id . " ,nns_vod_index= " . $str_vod_index . "\r\n", 3, $log . '/' . date('Ymd') . '.txt');
			}
			if(is_array($result_seekpoint))
			{
				$seekpoint_xml='<metadata>';
				foreach ($result_seekpoint as $value)
				{
					$seekpoint_xml.='<item begin="'.$value['nns_begin'] .'" end="' . $value['nns_end'] . '" type="'.$value['nns_type'].'" name="'.$value['nns_name'].'" img="'.$value['nns_image'].'" ></item>';
				}
				$seekpoint_xml.='</metadata>';
			}
		}
		
		if (empty($c2_task_info['nns_ref_id']))
		{
			//nl_execute_by_db("delete from nns_mgtvbk_c2_task where nns_id='$nns_id'", $db_w);
			return false;
		}
		@error_log($action . "\r\n", 3, $log . '/' . date('Ymd') . '.txt');
		
		
		$sp_config = sp_model::get_sp_config($sp_id);		
		//clip
		$index_info = video_model::get_vod_index_info($c2_task_info['nns_ref_id']);
		$result_index_import_info = public_model_exec::get_asset_id_by_sp_id($sp_id, 'index', $c2_task_info['nns_ref_id'],$index_info,$sp_config,'epg');
		if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
		{
		    return false;
		}
		$info['nns_id'] = $result_index_import_info['data_info'];
		
		
// 		if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0){			
// 			$video_info = video_model::get_vod_index_info($c2_task_info['nns_ref_id']);
// 			switch ($sp_config['import_id_mode']) {				
// 				case '1':					
// 					$vod_id = '001000'.$video_info['nns_integer_id'];
// 					$info['nns_id'] = str_pad($vod_id,32,"0");
// 					break;
// 				case '2':
// 					$info['nns_id'] = $video_info['nns_import_id'];
// 					break;
// 				case '3':					
// 					$vod_id = CSP_ID.'001'.$video_info['nns_integer_id'];
// 					$info['nns_id'] = str_pad($vod_id,32,"0");
// 					break;
// 				case '4':
// 					$arr_csp_id = explode("|", CSP_ID);
// 					$str_lenth = strlen($arr_csp_id[0])+strlen($arr_csp_id[1])+1;
// 					$lest_num = 32-$str_lenth;
// 					$info['nns_id'] = $arr_csp_id[0].'2'.str_pad($video_info['nns_integer_id'], $lest_num, "0", STR_PAD_LEFT).$arr_csp_id[1];
// 					break;
// 			}
			
// 		}
        $video_info = video_model::get_vod_info($info['nns_vod_id']);
        /*//判断是否为花絮、预告片的主媒资 查找正媒资
        if(isset($video_info['nns_ishuaxu']) && $video_info['nns_ishuaxu'] != 0 && $video_info['nns_ishuaxu'] != 2 && $video_info['nns_ishuaxu'] != 3)
        {
            $video_info = video_model::get_vod_info($video_info['nns_positiveoriginalid']);
            $info['nns_vod_id']= $video_info['nns_id'];
        }*/
		//单剧集注入 如果是花絮、预告片也当作单剧集注入
		if(($video_info['nns_all_index'] == -1 && (int)$sp_config['epg_single_index_import_model'] === 1)||(isset($video_info['nns_ishuaxu']) && $video_info['nns_ishuaxu'] != 0 && $video_info['nns_ishuaxu'] != 2 && $video_info['nns_ishuaxu'] != 3))
        {
            $info['nns_vod_id'] = '';
        }
        else
        {
			//vod
			$result_video_import_info = public_model_exec::get_asset_id_by_sp_id($sp_id, 'video', $info['nns_vod_id'],$video_info,$sp_config,'epg');
			if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
			{
				return false;
			}
            $info['nns_vod_id'] = $result_video_import_info['data_info'];
        }

// 		if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0)
// 		{		
// 			switch ($sp_config['import_id_mode']) 
// 			{				
// 				case '1':					
// 					$vod_id = '000000'.$vod_info['nns_integer_id'];
// 					$info['nns_vod_id'] = str_pad($vod_id,32,"0");
// 					break;
// 				case '2':
// 					$info['nns_vod_id'] = $vod_info['nns_asset_import_id'];
// 					break;
// 				case '3':
// 					$vod_id = CSP_ID.'000'.$vod_info['nns_integer_id'];
// 					$info['nns_vod_id'] = str_pad($vod_id,32,"0");
// 					break;
// 				case '4':
// 					$arr_csp_id = explode("|", CSP_ID);
// 					$str_lenth = strlen($arr_csp_id[0])+strlen($arr_csp_id[1])+1;
// 					$lest_num = 32-$str_lenth;
// 					$info['nns_vod_id'] = $arr_csp_id[0].'1'.str_pad($vod_info['nns_integer_id'], $lest_num, "0", STR_PAD_LEFT).$arr_csp_id[1];
// 					break;
// 			}
				
// 		}
		if (isset($cp_config['original_cp_enabled']) && (int)$cp_config['original_cp_enabled'] === 1)
		{
			$import_obj->cp_id = $video_info['nns_producer'];
		}
		elseif (isset($cp_config['original_cp_enabled']) && (int)$cp_config['original_cp_enabled'] === 2)
		{
			$import_obj->cp_id = 0;
		}
		//-->分集注入前日志 xingcheng.hu
		$video_type = "index";
		nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG分集注入开始==============================",$sp_id,$video_type,'');
		nn_log::write_log_message(LOG_MODEL_CMS,$action,$sp_id,$video_type,'');
		
		$nns_ext_url = (isset($info['nns_ext_url']) && strlen($info['nns_ext_url'])>0) ? json_decode($info['nns_ext_url'],true) : null;

		$temp_type = true;
		if ($action == 'destroy')
		{
			if(!empty($str_vod_id) && $str_vod_index !='')
			{
				$sql_del="delete from nns_vod_index_seekpoint where nns_video_id='{$str_vod_id}' and nns_video_index='{$str_vod_index}'";
				$result_del=nl_execute_by_db($sql_del, $dc->db());
				if(!$result_del)
				{
					@error_log("model_delete_seekpoint,sql=" . $sql_del . ",nns_vod_id=" . $str_vod_id . " ,nns_vod_index= " . $str_vod_index . "\r\n", 3, $log . '/' . date('Ymd') . '.txt');
				}
			}
			if(!$cp_config['check_metadata_enabled'])//下发注入的随机ID
			{
				$result = $import_obj->delete_video_clip($info['nns_id'],0,$info['nns_import_source'],$cp_id,(isset($c2_task_info['nns_message_id']) && strlen($c2_task_info['nns_message_id']) >0) ? $c2_task_info['nns_message_id'] : '');
			}
			else if (isset($sp_config['epg_index_delete_id_mode']) && $sp_config['epg_index_delete_id_mode'] == '1')
            {
                $result = $import_obj->delete_video_clip($info['nns_import_id'],0,null,$cp_id,(isset($c2_task_info['nns_message_id']) && strlen($c2_task_info['nns_message_id']) >0) ? $c2_task_info['nns_message_id'] : '',1);
            }
			else
			{
				$result = $import_obj->delete_video_clip($info['nns_id'],0,null,$cp_id,(isset($c2_task_info['nns_message_id']) && strlen($c2_task_info['nns_message_id']) >0) ? $c2_task_info['nns_message_id'] : '');
			}
			$xml = "";
		}
		else
		{
			$info['nns_summary'] = str_replace('"', '', $info['nns_summary']);
			
			$xml = '<index clip_id="' . $info['nns_id'] . '" ';			
			$xml .= 'name="' . htmlspecialchars($info['nns_name'], ENT_QUOTES) . '" ';
			if(isset($nns_ext_url['sequence']))
			{
			    $xml .= 'index="' . $nns_ext_url['sequence'] . '" ';
			    $info['nns_vod_id']='';
			}
			else
			{
			    $xml .= 'index="' . $info['nns_index'] . '" ';
			}
			$xml .= 'time_len="' . $info['nns_time_len'] . '" ';
			//$info['nns_summary'] = str_replace("&", '', $info['nns_summary']);
			//$info['nns_summary'] = str_replace("《", '', $info['nns_summary']);
			//$info['nns_summary'] = str_replace("》", '', $info['nns_summary']);
			$xml .= 'summary="' . htmlspecialchars($info['nns_summary'], ENT_QUOTES) . '" ';
			

			//$info['nns_watch_focus'] = str_replace("&", '', $info['nns_watch_focus']);
			//$info['nns_watch_focus'] = str_replace("《", '', $info['nns_watch_focus']);
			//$info['nns_watch_focus'] = str_replace("》", '', $info['nns_watch_focus']);
            if ((int)$sp_config['watch_focus_eq_summary_enabled'] === 1)
            {
                $xml .= 'watch_focus="' . htmlspecialchars($info['nns_summary'], ENT_QUOTES) . '" ';
            }
            else{
                $xml .= 'watch_focus="' . htmlspecialchars($info['nns_watch_focus'], ENT_QUOTES) . '" ';
            }

			$xml .= 'pic="' . str_replace("JPG", "jpg", $info['nns_image']) . '" ';
			$xml .= 'director="' . htmlspecialchars($info['nns_director'], ENT_QUOTES) . '" ';
			$info['nns_actor'] = htmlspecialchars(str_replace("&", '', $info['nns_actor']), ENT_QUOTES);
			$xml .= 'player="' . htmlspecialchars($info['nns_actor'], ENT_QUOTES) . '" ';
			$xml .= 'total_clicks="' . $info['nns_play_count'] . '" ';
			$xml .= 'release_time="' . $info['nns_release_time'] . '" ';
			$xml .= 'update_time="' . $info['nns_update_time'] . '" ';
			if(isset($cp_config['original_cp_enabled']) && (int)$cp_config['original_cp_enabled'] === 1)
			{
				$new_cp_id = $video_info['nns_producer'];
			}
			elseif(isset($cp_config['original_cp_enabled']) && (int)$cp_config['original_cp_enabled'] === 2)
			{
				$new_cp_id = 0;
			}
			else
			{
				$new_cp_id = $info['nns_cp_id'];
			}
			$xml .= 'asset_source="' . $new_cp_id . '" ';
			$xml .= 'month_clicks="" ';
			$xml .= 'week_clicks="" ';
			$xml .= 'day_clicks="" ';
			if(!$cp_config['check_metadata_enabled'])//下发注入的随机ID
			{
				$xml .= 'import_source="' . $info['nns_import_source'] . '" ';
			}
			$sql_ex = "SELECT * FROM `nns_vod_index_ex` where nns_vod_index_id='{$info['nns_import_id']}'";
			$index_ex = nl_db_get_all($sql_ex, $dc->db());
			if(is_array($index_ex))
			{
				foreach ($index_ex as $value)
				{
					if($value['nns_key'] === 'isintact')
					{
						$xml .= 'isintact="' . htmlspecialchars($value['nns_value'], ENT_QUOTES) . '" ';
					}
					else
					{
						$xml .= $value['nns_key'] . '="' . htmlspecialchars($value['nns_value'], ENT_QUOTES) . '" ';
					}
				}
			}
			
			$xml .= 'original_id="' . $info['nns_import_id'] . '" ';
			$xml .= '></index>';

			//-->主媒资注入前日志 xingcheng.hu
			$data['func'] 				= 'import_video_clip';

			
		    $data['assets_id'] = $info['nns_vod_id'];
			$data['assets_video_type']  = 0;
			$data['assets_clip_id']    =  $info['nns_id'];
			$data['assets_clip_info']   = $xml;
			$str_arr_to_str = var_export($data,true);
			$str_import_assets_message = date('Y-m-d H:i:s').'[EPG分集注入接口URL：]'.$url."\n".
			date('Y-m-d H:i:s').'[EPG分集注入参数：]'."\n".$str_arr_to_str;
			nn_log::write_log_message(LOG_MODEL_CMS,$str_import_assets_message,$sp_id,$video_type,'');
			unset($data);
			unset($str_arr_to_str);
			unset($str_import_assets_message);
			//<--
						
			$result = $import_obj->import_video_clip($info['nns_vod_id'], $info['nns_id'], $xml,0,$seekpoint_xml,$cp_id,(isset($c2_task_info['nns_message_id']) && strlen($c2_task_info['nns_message_id']) >0) ? $c2_task_info['nns_message_id'] : '');
		}
		$re = json_decode($result,true);
		//-->分集注入后日志
		nn_log::write_log_message(LOG_MODEL_CMS,$re,$sp_id,$video_type,'');
		nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG分集注入结束==============================",$sp_id,$video_type,'');
		$bool = false;
		//if ((int)$re['ret'] === 0 || stripos($re['reason'], '8093'))
		if($re['ret'] == '0')
		{
			$bool = true;
		}
		//elseif ((int)$re['ret'] === 7 || stripos($re['reason'], '8094'))
		elseif ((int)$re['ret'] === 7)
		{
			$bool = true;
		}
		if ($bool === true)
		{
			if ($c2_task_info['nns_epg_status'] == 97 || $c2_task_info['nns_epg_status'] == 100)
			{
				$sql = "update nns_mgtvbk_c2_task set nns_epg_status=99,nns_modify_time=now(),nns_action='$action'  where nns_id='{$c2_task_info['nns_id']}'";
				nl_execute_by_db($sql, $dc->db());
			}
			
			if ($c2_task_info['nns_epg_status'] == 99 && !empty($c2_task_info['nns_op_id']))
			{
				$queue_task_model = new queue_task_model();
				$r = $queue_task_model->q_report_task($c2_task_info['nns_id']);
				$queue_task_model = null;
				@error_log(var_export($r, true) . "\r\n", 3, $log . '/' . date('Ymd') . '.txt');
			}

            //百视通兼容开发支持主媒资内容反馈 feijian.gao --->start
            $str_cdn_notify = '';
            if(isset($sp_config['index_cdn_model']) && $sp_config['index_cdn_model'] == '1') {
                $bool = true;
                $sql_query_op_id = "select nns_id,nns_message_id from nns_mgtvbk_op_queue where nns_id = '{$c2_task_info['nns_op_id']}' limit 1";
                $info_query_op_id = nl_query_by_db($sql_query_op_id, $dc->db());
                $str_notify_cms_message_id = (isset($info_query_op_id[0]['nns_message_id'])) ? $info_query_op_id[0]['nns_message_id'] : $c2_task_info['nns_message_id'];
                $asset_cdn_share_addr = isset($sp_config['asset_cdn_share_addr']) ? trim(trim($sp_config['asset_cdn_share_addr'], '/'), '\\') : '';
                $result_notify_cms = self::notify_cms_data($asset_cdn_share_addr, $info, $sp_id, $action, $video_type, $str_notify_cms_message_id, $bool);
                if ($result_notify_cms['ret'] != 0) {
                    $bool = false;
                }
                $str_cdn_notify = isset($result_notify_cms['data_info']) ? $result_notify_cms['data_info'] : '';
            }
                //百视通兼容开发支持主媒资内容反馈 feijian.gao --->end
		}
		else
		{
			$now = date('Y-m-d H:i:s');
			$sql = "update nns_mgtvbk_c2_task set nns_epg_status=97,nns_epg_fail_time=nns_epg_fail_time+1,nns_modify_time='{$now}',nns_action='$action' where nns_id='{$c2_task_info['nns_id']}'";
			nl_execute_by_db($sql, $dc->db());
			//天威视讯注入失败 异步反馈片源信息,后面需转移至配置文件中
			if(isset($sp_config['message_feedback_mode']) && $sp_config['message_feedback_mode'] == 1 && isset($sp_config['site_callback_url']) && strlen($sp_config['site_callback_url']) > 0 && file_exists(dirname(dirname(dirname(__FILE__))).'/api/'.$sp_id.'/'.$sp_id.'_sync_source.php'))
			{
				include_once dirname(dirname(dirname(__FILE__))).'/api/'.$sp_id.'/'.$sp_id.'_sync_source.php';
				if(method_exists($sp_id.'_sync_source','async_feedback'))
				{
					//根据OP_queue_id查询消息ID
					$op_sql = "select nns_message_id from nns_mgtvbk_op_queue where nns_id='{$c2_task_info['nns_op_id']}'";
					$op_queue_re = nl_db_get_one($op_sql, $dc->db());
					if(is_array($op_queue_re))
					{
						$vod_clip_info = array(
								'nns_action' => $action,
								'nns_message_id' => $op_queue_re['nns_message_id'],
								'nns_org_id' => $sp_id,
						);
						$class_name = $sp_id.'_sync_source';
						$sync_source = new $class_name();
						$sync_source->async_feedback($vod_clip_info,false,false,$info['nns_import_id']);
					}
				}
			}
		}

		$log_data = array (
				'nns_id' => np_guid_rand('index'), 
				'nns_video_id' => $info['nns_id'], 
				'nns_video_type' => 'index', 
				'nns_org_id' => $sp_id, 
				'nns_create_time' => date('Y-m-d H:i:s'), 
				'nns_action' => $action, 
				'nns_status' => $re['ret'], 
				'nns_reason' => addslashes($re['reason']), 
				'nns_data' => $xml, 
				'nns_name' => $c2_task_info['nns_name']
		);
		nl_db_insert($dc->db(), 'nns_mgtvbk_import_epg_log', $log_data);
		$queue_task_model = new queue_task_model();
		$queue_task_model->q_report_task($c2_task_info['nns_id']);
		$queue_task_model = null;
		//nn_close_db($db_r);
		//nn_close_db($db_w);
		return $bool;
	}
	
	/**
	 * 媒资上下线逻辑处理
	 * @param array $info_list 
	 * @param string $send_way
	 * @param bool $third_category_enabled
	 * @param bool $check_video_statuc_enabled
	 * @param num $asset_unline_model  0|未配置  redis消息队列上下线控制  |  1 epg注入上下线控制
	 * @author liangpan
	 * @date 2015-11-27
	 */
	static public function import_unline($info_list,$send_way,$third_category_enabled,$check_video_statuc_enabled,$sp_config,$asset_unline_model=0)
	{
		include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/asset_online/asset_online_log.class.php';
		include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/asset_online/asset_online.class.php';
		include_once dirname(dirname(__FILE__)) . '/' . $info_list['nns_org_id'] . '/define.php';
		$nns_asset_online_id = $info_list['nns_id'];
		unset($info_list['nns_id']);
		$dc = nl_get_dc(array (
				'db_policy' => NL_DB_WRITE,
				'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
		));
		$time=time();
		$nns_id = np_guid_rand();
		//$filename = $info_list['nns_message_id'].".xml";
		$filename = $nns_id.".xml";
		$date_dir = date('Ymd').'/'.date('H');
		//检查注入媒资是否为最终状态
		if($check_video_statuc_enabled && $info_list['nns_action'] == 'online')
		{
			$flag_result=self::check_is_import_epg($dc,$info_list['nns_vod_id'], $info_list['nns_org_id']);
			if($flag_result['ret'] !=0)
			{
				nn_log::write_log_message(LOG_MODEL_UNLINE,$flag_result['reason'],$info_list['nns_org_id'],VIDEO_TYPE_VIDEO);
				$other_params = null;
				if($flag_result['ret'] == 1)
				{
					$nns_state = 5;
				}
				else if($flag_result['ret'] == 2)
				{
					$nns_state = 6;
				}
				else if($flag_result['ret'] == 3)
				{
					$nns_state = 7;
				}
				else
				{
					$nns_state = 2;
					$other_params='nns_fail_time=nns_fail_time+1';
				}
				$result=nl_asset_online::update($dc, array('nns_status'=>$nns_state), array('nns_id'=>$nns_asset_online_id),$other_params);
				if($result['ret'] !=0)
				{
					nn_log::write_log_message(LOG_MODEL_UNLINE,$result['reason'],$info_list['nns_org_id'],VIDEO_TYPE_VIDEO);
				}
				return ;
			}
		}
		$put_str = '';
		$url = IMPROT_EPG_ONLINE_URL;
		$log = IMPROT_EPG_LOG;
		$import_obj = new import($url, $log,$info_list['nns_org_id']);
		if($asset_unline_model == 1)
		{
			$ex_params =null;
			$data['func'] = 'modify_video';
			$data['assets_content']     = $assets_xml;
			$data['assets_video_info']     = $assets_xml;
			$data['asset_source_id'] =$asset_id;
			$data['assets_id'] =$asset_id;
			switch ($info_list['nns_type'])
			{
				case 'video':
					$func                = 'get_vod_info';
					$import_func         = 'unline_assets';
					$str_id_key          = 'assets_id';
					$str_unline          = 'assets_unline';
					$import_id_mode_flag = 1;
					break;
				case 'index':
					$func 				 = 'get_vod_index_info';
					$import_func 		 = 'unline_video_clip';
					$str_unline 		 = 'assets_clip_unline';
					$str_id_key          = 'assets_clip_id';
					$import_id_mode_flag = 2;
					break;
				case 'media':
					$func        		 = 'get_vod_media_info';
					$import_func 		 = 'unline_video_file';
					$str_unline 		 = 'assets_file_unline';
					$str_id_key  		 = 'assets_file_id';
					$import_id_mode_flag = 3;
					break;
			}
			$video_info = video_model::$func($info_list['nns_vod_id']);
			$vod_id = $video_info['nns_id'];
			if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0)
			{
				switch ($sp_config['import_id_mode']) {
					case '1':
						$vod_id = '000000'.$video_info['nns_integer_id'];
						$vod_id = str_pad($vod_id,32,"0");
						break;
					case '2':
						$vod_id = ($info_list['nns_type'] == 'video') ? $video_info['nns_asset_import_id'] : $video_info['nns_import_id'];
						break;
					case '3':
						$vod_id = CSP_ID.'000'.$video_info['nns_integer_id'];
						$vod_id = str_pad($vod_id,32,"0");
						break;
					case '4':
						$arr_csp_id = explode("|", CSP_ID);
						$str_lenth = strlen($arr_csp_id[0])+strlen($arr_csp_id[1])+1;
						$lest_num = 32-$str_lenth;
						$vod_id = $arr_csp_id[0].$import_id_mode_flag.str_pad($video_info['nns_integer_id'], $lest_num, "0", STR_PAD_LEFT).$arr_csp_id[1];
						break;
				}
			}
			$ex_params[$str_id_key] = $vod_id;
			$ex_params['func'] = $import_func;
			$ex_params['asset_source'] = (strlen($info_list['nns_cp_id']) > 0) ? $info_list['nns_cp_id'] : 0;
			$ex_params[$str_unline] = ($info_list['nns_action'] == 'unline') ? 1 : 0;
			if($info_list['nns_type'] == "video")
			{
				$ex_params["assets_category"] = $info_list["nns_asset_id"] . "|" . $info_list["nns_category_id"];
				$ex_params["cp_id"] = (strlen($info_list['nns_cp_id']) > 0) ? $info_list['nns_cp_id'] : 0;
				$ex_params["assets_video_type"] = "0";
			}
			//分集绑定 feijian.gao 2017年6月13日13:54:15 ->start
			if($info_list["nns_type"] == "index" && in_array($info_list['nns_action'], array('binding','unbinding')))
			{
				//主媒资与分集的绑定关系
				$ex_params = array();
				$info_list["index_index"] = $video_info["nns_index"];
				$ex_params = self::do_vod_index_bind_action($info_list);
			}
			//->end
			$result = $import_obj->import_unline($put_str,$nns_id,$ex_params,$asset_unline_model);
		}
		else
		{
			//将临时文件存储到data/tmp/xml
			$g_epg_send_way_conf = g_cms_config::get_g_config_value("g_epg_send_way_conf");
			if (!is_array($g_epg_send_way_conf))
			{
				nn_log::write_log_message(LOG_MODEL_UNLINE,"上下线配置文件".var_export($g_epg_send_way_conf,true),$info_list['nns_org_id'],VIDEO_TYPE_VIDEO);
				return ;
			}
			//根路径
			$root_dir = dirname(dirname(dirname(__FILE__)));
			//扩展路径
			$exp_dir = trim($g_epg_send_way_conf['http']["xml_path"], '/').'/'.$date_dir;
			if (!is_dir($root_dir . '/' . $exp_dir))
			{
				mkdir($root_dir . '/' . $exp_dir, 0777, true);
			}
			//本地临时文件路径
			$local_file_path = $root_dir . '/' . $exp_dir . '/' .  $filename;
			
			$operation = ($info_list['nns_action'] == 'unline') ? 2 : 1;
			$cp_id = (strlen($info_list['nns_cp_id']) <1 || $info_list['nns_cp_id'] == 0) ? '' : $info_list['nns_cp_id'];
			// 获取CP配置
			$result_cp = nl_cp::query_by_id($dc, $cp_id);
			$cp_config = json_decode($result_cp['data_info']['nns_config'], true);
			$vod_id = $info_list['nns_vod_id'];
			$video_info = video_model::get_vod_info($vod_id);
			if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0)
			{
				switch ($sp_config['import_id_mode']) 
				{				
					case '1':					
						$vod_id = '000000'.$video_info['nns_integer_id'];
						$vod_id = str_pad($vod_id,32,"0");
						break;
					case '2':
						$vod_id = $video_info['nns_asset_import_id'];
						break;
					case '3':					
						$vod_id = CSP_ID.'000'.$video_info['nns_integer_id'];
						$vod_id = str_pad($vod_id,32,"0");
						break;
					case '4':
						$arr_csp_id = explode("|", CSP_ID);
						$str_lenth = strlen($arr_csp_id[0])+strlen($arr_csp_id[1])+1;
						$lest_num = 32-$str_lenth;
						$vod_id = $arr_csp_id[0].'1'.str_pad($video_info['nns_integer_id'], $lest_num, "0", STR_PAD_LEFT).$arr_csp_id[1];
						break;
				}
			}
			$contents  = '<assetcontent>';
			$contents .= 	'<assettype>1</assettype>';
			$contents .= 	'<operation>'.$operation.'</operation>';
			$contents .= 	'<assetdesc>5</assetdesc>';
			$contents .=	'<content>';
			$contents .= 		'<extensionfield1>'.$info_list['nns_category_id'].'</extensionfield1>';
			$contents .= 		'<oriassetid>'.$vod_id.'</oriassetid>';
			if($third_category_enabled)
			{
				$contents .= 	'<categorytype>1</categorytype>';
			}
			$contents .= 		'<order>'.$info_list['nns_order'].'</order>';
			$contents .= 	'</content>';
			$contents .= 	'<info>';
			if(isset($cp_config['original_cp_enabled']) && (int)$cp_config['original_cp_enabled'] === 1)
			{
				$new_cp_id = $video_info['nns_producer'];		
			}
			elseif(isset($cp_config['original_cp_enabled']) && (int)$cp_config['original_cp_enabled'] === 2)
			{
				$new_cp_id = 0;
			}
			else
			{
				$new_cp_id = $cp_id;
			}
			$contents .= 		'<contentProvider>'.$new_cp_id.'</contentProvider>';
			$contents .= 	'</info>';
			$contents .= '</assetcontent>';
			$remote_path = rtrim($g_epg_send_way_conf['http']["xml_path"], '/');
			$remote_path = str_replace("//", "/", $remote_path);
			$result=file_put_contents($local_file_path, $contents);
			if(!$result)
			{
				nn_log::write_log_message(LOG_MODEL_UNLINE,"写入本地文件失败",$info_list['nns_org_id'],VIDEO_TYPE_VIDEO);
				return ;
			}
			if($send_way == 'http')
			{
				//本地文件存放内容,应该是http文件的内容
				$put_str = '<?xml version="1.0" encoding="utf-8"?><xmlpost><time>'. $time .'</time><msgid>'. $nns_id .'</msgid><url>'. rtrim($g_epg_send_way_conf['http']["base_host_url"],'/') . '/' . $exp_dir . '/' .  $filename .'</url></xmlpost>';
			}
			else
			{
				include_once dirname(dirname(dirname(dirname(__FILE__)))).'/np/np_ftp.php';
				//如果是下游只支持ftp还要ftp兼容
				$ftp = new nns_ftp($g_epg_send_way_conf['ftp']["address"], $g_epg_send_way_conf['ftp']["user"], $g_epg_send_way_conf['ftp']["password"], $g_epg_send_way_conf['ftp']["port"]);
				if (!$ftp->connect(30, $g_epg_send_way_conf['ftp']["mode"]))
				{
					nn_log::write_log_message(LOG_MODEL_UNLINE,"ftp连接失败：原因".var_export($ftp->error(),true),$info_list['nns_org_id'],VIDEO_TYPE_VIDEO);
					return ;
				}
				//上传文件
				$up_file = $ftp->up($remote_path . '/' .$date_dir, $filename, $local_file_path);
				@sleep(1);
				if (!$up_file)
				{
					nn_log::write_log_message(LOG_MODEL_UNLINE,"ftp文件上传失败：原因".var_export($ftp->error(),true),$info_list['nns_org_id'],VIDEO_TYPE_VIDEO);
					return ;
				}
				//ftp文件的内容
				$put_str = '<?xml version="1.0" encoding="utf-8"?><xmlpost><time>'. $time .'</time><msgid>'. $nns_id .'</msgid><url>'. $remote_path . '/' .$date_dir . '/' .  $filename .'</url></xmlpost>';
			}
			$result=$import_obj->import_unline($put_str,$nns_id);
		}
		$info_list['nns_id'] = $nns_id;
		$info_list['nns_import_content'] = $put_str;
		$info_list['nns_asset_online_id'] = $nns_asset_online_id;
		$result = json_decode($result,true);
		$info_list['nns_reason'] = $result['reason'];
		$info_list['nns_status'] = ($result['ret'] == 0) ? '0' : '1';
		//-->兼容新的epg上下线模式 by xingcheng.hu
		if($asset_unline_model==0)
		{
			if($info_list['nns_status'] == 0)
			{
				$result=nl_asset_online::update($dc, array('nns_status'=>'3'), array('nns_id'=>$nns_asset_online_id));
			}
			else
			{
				$result=nl_asset_online::update($dc, array('nns_status'=>'2'), array('nns_id'=>$nns_asset_online_id),'nns_fail_time=nns_fail_time+1');
			}
		}
		elseif($asset_unline_model==1)
		{
			if($info_list['nns_status'] == 0)
			{
				$result=nl_asset_online::update($dc, array('nns_status'=>'0'), array('nns_id'=>$nns_asset_online_id));
			}
			else
			{
				$result=nl_asset_online::update($dc, array('nns_status'=>'2'), array('nns_id'=>$nns_asset_online_id),'nns_fail_time=nns_fail_time+1');
			}
		}
		if($result['ret'] !=0)
		{
			nn_log::write_log_message(LOG_MODEL_UNLINE,$result['reason'],$info_list['nns_org_id'],VIDEO_TYPE_VIDEO);
		}
		$result_log=nl_asset_online_log::add($dc, $info_list);
		if($result_log['ret'] !=0)
		{
			nn_log::write_log_message(LOG_MODEL_UNLINE,$result_log['reason'],$info_list['nns_org_id'],VIDEO_TYPE_VIDEO);
		}
		unset($result);
		unset($result_log);
		return ;
	}
	
	/**
	 * 检查主媒资、分集、片源  添加修改是否是最终状态   如果不是  不再向下执行媒资上下线操作
	 * @param unknown $dc
	 * @param unknown $video_id
	 * @param unknown $sp_id
	 * @return multitype:number Ambigous <NULL, number, string, NULL, multitype:a r y s t i n g , array, multitype:number > |multitype:number string
	 * @author liangpan
	 * @date 2015-11-27
	 */
	static public function check_is_import_epg($dc,$video_id,$sp_id)
	{
		$media_arr = array();
		include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/c2_task/c2_task.class.php';
		$result_vod = nl_c2_task::query_end_state($dc, $video_id, $sp_id, 'video');
		if($result_vod['ret'] !=0)
		{
			nn_log::write_log_message(LOG_MODEL_UNLINE,$result_vod['reason'],$sp_id,VIDEO_TYPE_VIDEO);
			return array('ret'=>4,'reason'=>$result_vod['reason']);
		}
		if(!isset($result_vod['data_info']) || !is_array($result_vod['data_info']) || empty($result_vod['data_info']))
		{
			return array('ret'=>1,'reason'=>'主媒资添加或则修改不是最终状态');
		}
		$result_index = nl_c2_task::query_end_state($dc, $video_id, $sp_id, 'index');
		if($result_index['ret'] !=0)
		{
			nn_log::write_log_message(LOG_MODEL_UNLINE,$result_index['reason'],$sp_id,VIDEO_TYPE_VIDEO);
			return array('ret'=>4,'reason'=>$result_index['reason']);
		}
		if(!isset($result_index['data_info']) || !is_array($result_index['data_info']) || empty($result_index['data_info']))
		{
			return array('ret'=>2,'reason'=>'分集添加或则修改不是最终状态');
		}
		foreach ($result_index['data_info'] as $val)
		{
			$media_arr[]=$val['nns_ref_id'];
		}
		$result_media = nl_c2_task::query_end_state($dc, $media_arr, $sp_id, 'media');
		if($result_media['ret'] !=0)
		{
			nn_log::write_log_message(LOG_MODEL_UNLINE,$result_media['reason'],$sp_id,VIDEO_TYPE_VIDEO);
			return array('ret'=>4,'reason'=>$result_media['reason']);
		}
		if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
		{
			return array('ret'=>3,'reason'=>'片源添加或则修改不是最终状态');
		}
		return array('ret'=>0,'reason'=>'ok');
	}
	
	
	
	//媒资ID   注入媒资
	static public function import_asset($nns_id, $action_epg = null)
	{

		$dc = nl_get_dc(array(
		    'db_policy' =>  NL_DB_READ | NL_DB_WRITE,
		    'cache_policy' => NP_KV_CACHE_TYPE_NULL
		   )
		);
		$action = 'add';
		if (!empty($action_epg))
		{
			$action = $action_epg;
		}
		$sql = "select * from nns_mgtvbk_c2_task where nns_id='$nns_id'";
		$c2_task_info = nl_db_get_one($sql, $dc->db());

		if (empty($c2_task_info))
		{
			return false;
		}
		if ($c2_task_info['nns_type'] != 'video')
		{
			return false;
		}
		$sp_id = $c2_task_info['nns_org_id'];
		include_once dirname(dirname(__FILE__)) . '/' . $sp_id . '/define.php';
		$url = IMPROT_EPG_URL;
		$log = IMPROT_EPG_LOG;
		
		if (!empty($c2_task_info))
		{
			$action = $c2_task_info['nns_action'];
		}
		$sql = "select * from nns_vod where nns_id='{$c2_task_info['nns_ref_id']}'";
		$info = nl_db_get_one($sql, $dc->db());

		$cp_id = isset($info['nns_cp_id']) ? $info['nns_cp_id'] : null;
		// 获取CP配置
		$result_cp = nl_cp::query_by_id($dc, $cp_id);
		$cp_config = json_decode($result_cp['data_info']['nns_config'], true);

		$import_obj = new import($url, $log,'',$cp_id);
		if (empty($info) || $info['nns_deleted'] == 1)
		{
			$action == 'destroy';
			nl_execute_by_db("update nns_mgtvbk_c2_task set nns_action='destroy',nns_epg_status=97 where nns_id='$nns_id'", $dc->db());
		}

		$sp_config = sp_model::get_sp_config($sp_id);
		$video_info = video_model::get_vod_info($c2_task_info['nns_ref_id']);
		//单剧集注入 或者是花絮、预告的主媒资不注入epg
		if(((int)$sp_config['epg_single_index_import_model'] === 1 && $video_info['nns_all_index'] == -1)||(isset($info['nns_ishuaxu'])&&$info['nns_ishuaxu']!=0 && $info['nns_ishuaxu']!=2 && $info['nns_ishuaxu']!=3))
        {
            $sql = "update nns_mgtvbk_c2_task set nns_epg_status=99,nns_modify_time=now()  where nns_id='{$c2_task_info['nns_id']}'";
            nl_execute_by_db($sql, $dc->db());
            $queue_task_model = new queue_task_model();
            $queue_task_model->q_report_task($c2_task_info['nns_id']);
            $queue_task_model = null;
            return true;
        }

		$result_video_import_info = public_model_exec::get_asset_id_by_sp_id($sp_id, 'video', $c2_task_info['nns_ref_id'],$video_info,$sp_config,'epg');
		if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
		{
		    return false;
		}
		$info['nns_id'] = $result_video_import_info['data_info'];
		
// 		if(isset($sp_config['import_id_mode'])&&strlen($sp_config['import_id_mode'])>0&&(int)$sp_config['import_id_mode']>0){
// 			$video_info = video_model::get_vod_info($c2_task_info['nns_ref_id']);
// 			switch ($sp_config['import_id_mode']) {				
// 				case '1':					
// 					$vod_id = '000000'.$video_info['nns_integer_id'];
// 					$info['nns_id'] = str_pad($vod_id,32,"0");
// 					break;
// 				case '2':
// 					$info['nns_id'] = $video_info['nns_asset_import_id'];
// 					break;
// 				case '3':
// 					$vod_id = CSP_ID.'000'.$video_info['nns_integer_id'];
// 					$info['nns_id'] = str_pad($vod_id,32,"0");
// 					break;
// 				case '4':
// 					$arr_csp_id = explode("|", CSP_ID);
// 					$str_lenth = strlen($arr_csp_id[0])+strlen($arr_csp_id[1])+1;
// 					$lest_num = 32-$str_lenth;
// 					$info['nns_id'] = $arr_csp_id[0].'1'.str_pad($video_info['nns_integer_id'], $lest_num, "0", STR_PAD_LEFT).$arr_csp_id[1];
// 					break;
// 			}
			
// 		}
		if(isset($cp_config['original_cp_enabled']) && (int)$cp_config['original_cp_enabled'] === 1) 
		{
			$import_obj->cp_id = $info['nns_producer'];
		}
		elseif(isset($cp_config['original_cp_enabled']) && (int)$cp_config['original_cp_enabled'] === 2)
		{
			$import_obj->cp_id = 0;
		}
		//-->媒资注入前日志 xingcheng.hu
		$video_type = "video";
		nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG主媒资注入开始==============================",$sp_id,$video_type,'');
		nn_log::write_log_message(LOG_MODEL_CMS,$action,$sp_id,$video_type,'');
		//媒资注入
		if ($action == 'destroy')
		{
			$result = $import_obj->delete_assets($info['nns_id'],0,$cp_id,(isset($c2_task_info['nns_message_id']) && strlen($c2_task_info['nns_message_id']) >0) ? $c2_task_info['nns_message_id'] : '');
			$xml = "";
		}
		else
		{


			$xml = '<video assets_id="' . $info['nns_id'] . '" ';
			$sql = "select * from nns_depot where nns_id='{$info['nns_depot_id']}'";
			$depot = nl_db_get_one($sql, $dc->db());
			$nns_asset_import_id = $info['nns_asset_import_id'];
			$sql = "SELECT * FROM `nns_vod_ex` where nns_vod_id='{$nns_asset_import_id}'";
			$vod_ex = nl_db_get_all($sql, $dc->db());

			$category_name = '';
			$dom = new DOMDocument('1.0', 'utf-8');
			$depot_info = $depot['nns_category'];
			$nns_category_id = $info['nns_category_id'];
			$dom->loadXML($depot_info);
			$categorys = $dom->getElementsByTagName('category');
			/**
			 * liangpan   2016-03-22 添加父级栏目下发数据（注：现在只支持二级栏目，等cms 4.29  支持无限级栏目）
			 */
			//所有栏目id的集合   栏目id为KEY => 栏目名称为value   
			$arr_category_all = array();
			//默认父级栏目id 为0
			$parent_category_id = 0;
			foreach ($categorys as $category)
			{
				if ((string)$category->getAttribute('id') == (string)$nns_category_id)
				{
					$category_name = $category->getAttribute('name');
					//查询父级栏目id 如果为0 则为0  如果不为0 则是该父级的id
					$parent_category_id =  ((string)$category->getAttribute('parent') == '0') ? 0 : (string)$category->getAttribute('parent');
					//break;
				}
				//所有栏目id的集合
				$arr_category_all[(string)$category->getAttribute('id')] = $category->getAttribute('name');
			}

			//父级栏目的名称   如果 $parent_category_id 为0 则 父级名称为空
			$parent_assets_category = isset($arr_category_all[$parent_category_id]) ? $arr_category_all[$parent_category_id] : '';
			$xml .= 'parent_assets_category="' . $parent_assets_category . '" ';
			//父级栏目的id
			$xml .= 'parent_assets_category_id="' . $parent_category_id . '" ';
			if (strlen($parent_assets_category) > 0 && $sp_id == 'xjdx_zte')
			{
				$xml .= 'assets_category="' . $parent_assets_category . '/' . $category_name . '" video_type="0" ';
			}
			else
			{
                $xml .= 'assets_category="' . $category_name . '" video_type="0" ';
			}
			$xml .= 'name="' . htmlspecialchars($info['nns_name'], ENT_QUOTES) . '" ';
			//下发至下游CMS，都需要以/分隔，不能使用其他的，由于前面已经转换成了/ 所以此处关闭/转成,  2016-11-2
			$xml .= 'sreach_key="' . htmlspecialchars($info['nns_keyword'], ENT_QUOTES) . '" ';
			$xml .= 'view_type="' . $info['nns_view_type'] . '" ';
			$xml .= 'director="' . htmlspecialchars($info['nns_director'], ENT_QUOTES) . '" ';
			$xml .= 'player="' . htmlspecialchars($info['nns_actor'], ENT_QUOTES) . '" ';
            $xml .= 'user_score_arg="' . floatval($info['nns_score_total']) . '" ';
			$xml .= 'tag="' . htmlspecialchars($info['nns_kind'], ENT_QUOTES) . '" ';
			$xml .= 'Tags="' . $info['nns_tag'] . '" ';
            $xml .= 'etag="' . $info['nns_tag'] . '" ';
			$xml .= 'region="' . htmlspecialchars($info['nns_area'], ENT_QUOTES) . '" ';
			$xml .= 'release_time="' . $info['nns_show_time'] . '" ';
			$xml .= 'totalnum="' . $info['nns_all_index'] . '" ';
			if(is_array($vod_ex))
			{
				foreach ($vod_ex as $value)
				{
					$xml .= $value['nns_key'] . '="' . htmlspecialchars($value['nns_value'], ENT_QUOTES) . '" ';
				}
			}

			//容错，保证横竖方图每一张都有
			if(empty($info['nns_image2']) && !empty($info['nns_image_v']))
			{
				$info['nns_image2'] = $info['nns_image_v'];
			}
			if(empty($info['nns_image1']) && !empty($info['nns_image_v']))
			{
				$info['nns_image1'] = $info['nns_image_v'];
			}
			if(!empty($info['nns_image2']) && empty($info['nns_image_v']))
			{
				$info['nns_image_v'] = $info['nns_image2'];
			}
			if(empty($info['nns_image_h']))
			{
				$info['nns_image_h'] = $info['nns_image_v'];
			}
			if(empty($info['nns_image_s']))
			{
				$info['nns_image_s'] = $info['nns_image_v'];
			}
			$xml .= 'smallpic="' . str_replace("JPG", "jpg", $info['nns_image2']) . '" ';
			$xml .= 'midpic="' . str_replace("JPG", "jpg", $info['nns_image1']) . '" ';
			$xml .= 'bigpic="' . str_replace("JPG", "jpg", $info['nns_image0']) . '" ';
			$xml .= 'verticality_img="' . str_replace("JPG", "jpg", $info['nns_image_v']) . '" ';
			$xml .= 'horizontal_img="' . str_replace("JPG", "jpg", $info['nns_image_h']) . '" ';
			$xml .= 'square_img="' . str_replace("JPG", "jpg", $info['nns_image_s']) . '" ';

			//$info['nns_summary'] = str_replace('"', '', $info['nns_summary']);
			//$info['nns_summary'] = str_replace("&", '', $info['nns_summary']);
			//$info['nns_summary'] = str_replace("《", '', $info['nns_summary']);
			//$info['nns_summary'] = str_replace("》", '', $info['nns_summary']);
			$xml .= 'intro="' . htmlspecialchars($info['nns_summary'], ENT_QUOTES) . '" ';
			$xml .= 'producer="' . htmlspecialchars($info['nns_producer'], ENT_QUOTES) . '" ';
			$xml .= 'play_role="' . htmlspecialchars($info['nns_play_role']) . '" ';
			$xml .= 'subordinate_name="' . htmlspecialchars($info['nns_alias_name'], ENT_QUOTES) . '" ';
			$xml .= 'english_name="' . htmlspecialchars(str_replace(",", "/", $info['nns_eng_name']), ENT_QUOTES) . '" ';
			$xml .= 'language="' . htmlspecialchars(str_replace(",", "/", $info['nns_language']), ENT_QUOTES) . '" ';
			$xml .= 'caption_language="' . htmlspecialchars(str_replace(",", "/", $info['nns_text_lang']), ENT_QUOTES) . '" ';
            $xml .= 'copyright_begin_date="' . str_replace(",", "/", $info['nns_copyright_begin_date']) . '" ';//版权开始时间
			$xml .= 'copyright_range="' . str_replace(",", "/", $info['nns_copyright_range']) . '" ';//版权过期时间
			$xml .= 'copyright="' . str_replace(",", "/", $info['nns_copyright']) . '" ';
			$xml .= 'total_clicks="' . $info['nns_play_count'] . '" ';
			$xml .= 'view_len="' . $info['nns_view_len'] . '" ';
			if(isset($cp_config['original_cp_enabled']) && (int)$cp_config['original_cp_enabled'] === 1)
			{
				$new_cp_id = $info['nns_producer'];
			}
			elseif(isset($cp_config['original_cp_enabled']) && (int)$cp_config['original_cp_enabled'] === 2)
			{
				$new_cp_id = 0;
			}
			else
			{
				$new_cp_id = $info['nns_cp_id'];
			}
			$xml .= 'asset_source="' . $new_cp_id . '" ';
			$xml .= 'day_clicks="0" ';
			$xml .= 'original_id="' . $info['nns_asset_import_id'] . '" ';
			$xml .= '></video>';
			//-->主媒资注入前日志 xingcheng.hu
			$data['func'] = 'modify_video';
			$data['assets_content']     = $xml;
			$data['assets_video_info']     = $xml;
			$data['asset_source_id'] =$info['nns_id'];
			$data['assets_id'] =$info['nns_id'];
			$str_arr_to_str = var_export($data,true);
			$str_import_assets_message = date('Y-m-d H:i:s').'[EPG主媒资注入接口URL：]'.$url."\n".
			date('Y-m-d H:i:s').'[EPG主媒资注入参数：]'."\n".$str_arr_to_str;
			nn_log::write_log_message(LOG_MODEL_CMS,$str_import_assets_message,$sp_id,$video_type,'');
			unset($data);
			unset($str_arr_to_str);
			unset($str_import_assets_message);
			//<--
			$result = $import_obj->import_assets($xml, $info['nns_id'],$cp_id,(isset($c2_task_info['nns_message_id']) && strlen($c2_task_info['nns_message_id']) >0) ? $c2_task_info['nns_message_id'] : '');
		}
		//-->主媒资注入结果日志 xingcheng.hu
		$re = json_decode($result, true);
		nn_log::write_log_message(LOG_MODEL_CMS,$re,$sp_id,$video_type,'');
		nn_log::write_log_message(LOG_MODEL_CMS,"=============================EPG主媒资注入结束==============================",$sp_id,$video_type,'');
		//<--
		$bool = false;
		//if ((int)$re['ret'] === 0 || stripos($re['reason'], '8090'))
		if ($re['ret'] == '0')
		{
			$bool = true;
		}
		//elseif ((int)$re['ret'] === 7 || stripos($re['reason'], '8091'))
		elseif ((int)$re['ret'] === 7)
		{
			$bool = true;
		}
		if ($bool === true)
		{
			if ($c2_task_info['nns_epg_status'] == 97 || $c2_task_info['nns_epg_status'] == 100)
			{
				$sql = "update nns_mgtvbk_c2_task set nns_epg_status=99,nns_modify_time=now()  where nns_id='{$c2_task_info['nns_id']}'";
				nl_execute_by_db($sql, $dc->db());
			}
			
			if ($c2_task_info['nns_epg_status'] == 99 && !empty($c2_task_info['nns_op_id']))
			{
				$queue_task_model = new queue_task_model();
				$r = $queue_task_model->q_report_task($c2_task_info['nns_id']);
				$queue_task_model = null;
				@error_log(var_export($r, true) . "\r\n", 3, $log . '/' . date('Ymd') . '.txt');
			}
			//百视通兼容开发支持主媒资内容反馈 feijian.gao --->start
            $str_cdn_notify = '';
            if(isset($sp_config['video_cdn_model']) && $sp_config['video_cdn_model'] == '1') {
                $bool = true;
                $sql_query_op_id = "select nns_id,nns_message_id from nns_mgtvbk_op_queue where nns_id = '{$c2_task_info['nns_op_id']}' limit 1";
                $info_query_op_id = nl_query_by_db($sql_query_op_id, $dc->db());
                $str_notify_cms_message_id = (isset($info_query_op_id[0]['nns_message_id'])) ? $info_query_op_id[0]['nns_message_id'] : $c2_task_info['nns_message_id'];
                $asset_cdn_share_addr = isset($sp_config['asset_cdn_share_addr']) ? trim(trim($sp_config['asset_cdn_share_addr'], '/'), '\\') : '';
                $result_notify_cms = self::notify_cms_data($asset_cdn_share_addr, $info, $sp_id, $action, $video_type, $str_notify_cms_message_id, $bool);
                if ($result_notify_cms['ret'] != 0) {
                    $bool = false;
                }
                $str_cdn_notify = isset($result_notify_cms['data_info']) ? $result_notify_cms['data_info'] : '';
            }
			//百视通兼容开发支持主媒资内容反馈 feijian.gao --->end
		}
		else
		{
			$now = date('Y-m-d H:i:s');
			$sql = "update nns_mgtvbk_c2_task set nns_epg_status=97,nns_epg_fail_time=nns_epg_fail_time+1,nns_modify_time='{$now}' where nns_id='{$c2_task_info['nns_id']}'";
			nl_execute_by_db($sql, $dc->db());
			//天威视讯注入失败 异步反馈片源信息,后面需转移至配置文件中
			if(isset($sp_config['message_feedback_mode']) && $sp_config['message_feedback_mode'] == 1 && isset($sp_config['site_callback_url']) && strlen($sp_config['site_callback_url']) > 0 && file_exists(dirname(dirname(dirname(__FILE__))).'/api/'.$sp_id.'/'.$sp_id.'_sync_source.php'))
			{
				include_once dirname(dirname(dirname(__FILE__))).'/api/'.$sp_id.'/'.$sp_id.'_sync_source.php';
				if(method_exists($sp_id.'_sync_source','async_feedback'))
				{
					//根据OP_queue_id查询消息ID
					$op_sql = "select nns_message_id from nns_mgtvbk_op_queue where nns_id='{$c2_task_info['nns_op_id']}'";
					$op_queue_re = nl_db_get_one($op_sql, $dc->db());
					if(is_array($op_queue_re))
					{
						$tile_info = array(
								'nns_action' => $action,
								'nns_message_id' => $op_queue_re['nns_message_id'],
								'nns_org_id' => $sp_id,
						);
						$class_name = $sp_id.'_sync_source';
						$sync_source = new $class_name();
						$sync_source->async_feedback($tile_info,false,false,$info['nns_asset_import_id']);
					}
				}
			}
		}

		$log_data = array (
				'nns_id' => np_guid_rand('vod'), 
				'nns_video_id' => $info['nns_id'], 
				'nns_video_type' => 'vod', 
				'nns_org_id' => $sp_id, 
				'nns_create_time' => date('Y-m-d H:i:s'), 
				'nns_action' => $action, 
				'nns_status' => $re['ret'], 
				'nns_reason' => addslashes($re['reason']), 
				'nns_data' => $xml, 
				'nns_name' => $c2_task_info['nns_name']
		);
		nl_db_insert($dc->db(), 'nns_mgtvbk_import_epg_log', $log_data);
		$queue_task_model = new queue_task_model();
		$queue_task_model->q_report_task($c2_task_info['nns_id']);
		$queue_task_model = null;
		//nn_close_db($db_r);
		//nn_close_db($db_w);
		return $bool;
	}
	
	//epg注入日志
	static public function get_mgtvbk_import_epg_log($sp_id, $filter = null, $start = 0, $size = 100)
	{
		$db = nn_get_db(NL_DB_READ);
		$sql = "select * " . "from nns_mgtvbk_import_epg_log  where nns_org_id='$sp_id'";
		if ($filter != null)
		{
			$wh_arr = array ();
			
			if (!empty($filter['day_picker_start']))
			{
				$wh_arr[] = " nns_create_time>='" . $filter['day_picker_start'] . "'";
			}
			if (!empty($filter['day_picker_end']))
			{
				$wh_arr[] = " nns_create_time<='" . $filter['day_picker_end'] . "'";
			}
			if (!empty($filter['nns_category']))
			{
				$wh_arr[] = " nns_video_type='" . $filter['nns_category'] . "'";
			}
			if (!empty($filter['nns_name']))
			{
				$wh_arr[] = " nns_name like '%" . $filter['nns_name'] . "%'";
			}
			if (!empty($filter['nns_id']))
			{
				$wh_arr[] = " nns_id='" . $filter['nns_id'] . "'";
			}
			if (!empty($filter['nns_status']))
			{
				if ($filter['nns_status'] == '1')
				{
					$wh_arr[] = " nns_status!=0";
				}
				elseif ($filter['nns_status'] == '2')
				{
					$wh_arr[] = " nns_status=0";
				}
			}
			if (!empty($filter['nns_action']))
			{
				$wh_arr[] = " nns_action='" . $filter['nns_action'] . "'";
			}
			if (!empty($wh_arr))
				$sql .= " and " . implode(" and ", $wh_arr);
		}
		$count_sql = $sql;
		$count_sql = str_replace('*', ' count(*) as temp_count ', $count_sql);
		$sql .= " order by nns_create_time desc ";
		$sql .= " limit $start,$size";
		$data = nl_db_get_all($sql, $db);
		//$count_sql  = "SELECT FOUND_ROWS() "; 
		$rows = nl_db_get_col($count_sql, $db);
		return array (
				'data' => $data, 
				'rows' => $rows
		);
	}

	static public function import_cdn_playbill($sp_id, $live_ids, $begin_day, $end_day)
	{
		set_time_limit(0);
		$db = nn_get_db(NL_DB_READ);
		$db->open();
		$live_id_arr = explode(',', $live_ids);
		$xml_str = '';
		foreach ($live_id_arr as $live_id)
		{
			$sql = "select nns_name from nns_live   where nns_id='{$live_id}'";
			$task_name = nl_db_get_col($sql, $db);
			unset($sql);
			$task_name .= $begin_day . '-' . $end_day;
			
			$begin_days = $begin_day . ' 00:00:00';
			$end_days = $end_day . ' 23:59:59';
			
			$sql = "SELECT nns_id,nns_name,	nns_begin_time,	nns_time_len,nns_pinyin,nns_live_id FROM `nns_live_playbill_item` 
where nns_live_id='$live_id' and nns_begin_time>='$begin_days' and nns_begin_time<='$end_days' ";
			
			$playbills = nl_db_get_all($sql, $db);
			unset($sql);
			if (count($playbills) == 0 || empty($playbills))
			{
				continue;
			}
			
			$xml_str = '<?xml version="1.0" encoding="UTF-8"?><ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
			$xml_str .= '<Objects>';
			foreach ($playbills as $live_item)
			{
				$xml_str .= '<Object ElementType="Schedule" ID="' . $live_item['nns_id'] . '" Action="REGIST" Code="' . $live_item['nns_id'] . '">';
				$xml_str .= '<Property Name="ChannelID">' . $live_id . '</Property>';
				$xml_str .= '<Property Name="ChannelCode">' . $live_id . '</Property>';
				
				$item_nns_name = str_replace('<', '(', $live_item['nns_name']);
				$item_nns_name = str_replace('>', ')', $item_nns_name);
				$item_nns_name = htmlspecialchars($item_nns_name, ENT_QUOTES);
				$xml_str .= '<Property Name="ProgramName">' . $item_nns_name . '</Property>';
				$xml_str .= '<Property Name="SearchName">' . htmlspecialchars($live_item['nns_pinyin'], ENT_QUOTES) . '</Property>';
				$xml_str .= '<Property Name="Genre">Genre</Property>';
				$xml_str .= '<Property Name="StartDate">' . date('Ymd', strtotime($live_item['nns_begin_time'])) . '</Property>';
				$xml_str .= '<Property Name="StartTime">' . date('His', strtotime($live_item['nns_begin_time'])) . '</Property>';
				$time_to = self::timediff($live_item['nns_time_len']);
				$xml_str .= '<Property Name="Duration">' . $time_to . '</Property>';
				$xml_str .= '<Property Name="Status">1</Property>';
				$xml_str .= '</Object>';
			}
			$xml_str .= '</Objects>';
			$xml_str .= '</ADI>';
			//header ( "Content-Type:text/xml;charset=utf-8" ); 
			//die($xml_str);
			c2_task_model::add_new_playbill($live_id, $xml_str, $begin_day . '-' . $end_day, $sp_id);
		}
		return true;
	}

	static public function timediff($timediff)
	{
		$days = intval($timediff / 86400);
		$remain = $timediff % 86400;
		$hours = intval($remain / 3600);
		$remain = $remain % 3600;
		$mins = intval($remain / 60);
		$secs = $remain % 60;
		if ($days > 0)
		{
			$hours = $days * 24;
		}
		if (strlen($hours) == 1)
		{
			$hours = "0" . $hours;
		}
		if (strlen($mins) == 1)
		{
			$mins = "0" . $mins;
		}
		if (strlen($secs) == 1)
		{
			$secs = "0" . $secs;
		}
		return $hours . $mins . $secs;
	}

	/**
	 * @param 
	 */
	static public function import_epg_playbill($sp_id, $live_id, $begin_day, $end_day)
	{
		$db = nn_get_db(NL_DB_READ);
		//echo $live_id;die;
		$sql = "SELECT nns_id,nns_name,	nns_begin_time,	nns_time_len,	nns_live_id,DATE_FORMAT(nns_begin_time, '%Y%m%d') as nns_date 
		FROM `nns_live_playbill_item` where nns_live_id='$live_id' and 
		nns_begin_time>='$begin_day' and nns_begin_time<='$end_day'  group by nns_date";
		$all_playbill = nl_db_get_all($sql, $db);
		//echo $sql.'<pre>';print_r($all_playbill);die;
		if (!is_array($all_playbill) && empty($all_playbill))
		{
			return false;
		}
		$array = array ();
		foreach ($all_playbill as $value)
		{
			$sql_tmp['date'] = $value['nns_date'];
			$sql_child = "SELECT nns_id,nns_name,	nns_begin_time,	nns_time_len,	nns_live_id,DATE_FORMAT(nns_begin_time, '%Y%m%d') as nns_date 
			FROM `nns_live_playbill_item` where nns_live_id='$live_id' and 
			nns_begin_time>='$begin_day' and nns_begin_time<='$end_day'  and DATE_FORMAT(nns_begin_time, '%Y%m%d')='{$value['nns_date']}'";
			$child = nl_db_get_all($sql_child, $db);
			$sql_tmp['playbill'] = array ();
			foreach ($child as $childvalue)
			{
				$sql_tmp['playbill'][] = array (
						'begin_time' => date('His', strtotime($childvalue['nns_begin_time'])), 
						'end_time' => date('His', strtotime($childvalue['nns_begin_time']) + $childvalue['nns_time_len']), 
						'name' => $childvalue['nns_name']
				);
			}
			$array[] = $sql_tmp;
		}
		$channel_info = nl_db_get_one("select * from nns_live_ex where nns_live_id='$live_id' and nns_key='pid'", $db);
		if (!is_array($channel_info) && empty($channel_info))
		{
			return false;
		}
		
		$data = 'channel_id=' . $channel_info['nns_value'] . '&data=' . json_encode($array);
		//$data['data'] = $array;
		$sp_info = sp_model::get_sp_info($sp_id);
		$sp_config = json_decode($sp_info['nns_config'], true);
		if (!isset($sp_config['epg_playbill_url']))
		{
			return false;
		}
		include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/np/np_http_curl.class.php';
		$http_client = new np_http_curl_class();
		
		$result = $http_client->post($sp_config['epg_playbill_url'], $data);
		$ret_arr = json_decode($result, true);
		return $ret_arr['reason'];
	}

	/**
	 * 主媒资分集绑定关系操作
	 * @author  <feijian.gao@starcor.cn>
	 * @date    2017年6月12日19:47:46
	 *
	 * @param array $arr_binding_info  绑定的数据信息
	 * @return array $arr_post_params  返回的注入参数
	 */
	static public function do_vod_index_bind_action($arr_binding_info)
	{
		//拼接xml参数
		$str_index_state_info = "";
		$str_index_state_info .= "<vod_index_list>";
		$str_index_state_info .= "<assets_id>{$arr_binding_info['nns_asset_id']}</assets_id>";
		$str_index_state_info .= "<cp_id>{$arr_binding_info['nns_cp_id']}</cp_id>";
		$str_index_state_info .= "<index_info>{$arr_binding_info['nns_import_id']}|{$arr_binding_info['nns_index']}|{$arr_binding_info['nns_category_id']}</index_info>";
		$str_index_state_info .= "</vod_index_list>";

		//post的参数
		$arr_post_params = array(
			"func" => "bind_vod_index",
			"assets_id" => $arr_binding_info["nns_asset_id"],
			"vod_index_list" => $str_index_state_info,
		);

		unset($str_index_state_info);

		return $arr_post_params;
	}


    /**
     * 消息反馈
     * @param $result_encode
     * @param $url
     * @return array
     */
    static public function notify_cp_sp_playbill_to_video($result_encode,$url)
    {

        $playbill_info = array(
            'id'=>$result_encode['data_info'],
            'state'=>$result_encode['ret'] == 0 ? 2 : 3,
            'summary'=>$result_encode['reason'],
        );
        return self::_execute_curl($url,'post',array('a'=>'update_vod_playbill_transcode_state','playbill_info'=>json_encode(array($playbill_info))));
    }

    /**
     * 执行curl
     * @param $send_url
     * @param string $func
     * @param null $content
     * @param null $arr_header
     * @param int $time
     * @return array
     */
    static public function _execute_curl($send_url,$func='get',$content=null,$arr_header=null,$time=60)
    {
        $obj_curl = new np_http_curl_class();
        $curl_data = $obj_curl->do_request($func,$send_url,$content,$arr_header,60);
        $curl_info = $obj_curl->curl_getinfo();
        $http_code = $curl_info['http_code'];
        if($curl_info['http_code'] != '200')
        {
            return self::_return_data(1,'[访问第三方接口,CURL接口地址]：'.$send_url.'[HTTP状态码]：'.$curl_info['http_code'].'[HTTP错误]：'.$obj_curl->curl_error(),$curl_data);
        }
        else
        {
            return self::_return_data(0,'[访问第三方接口,CURL接口地址]：'.$send_url,$curl_data);
        }
    }

    /**
     * 返回数据数组
     * @param int $ret
     * @param string $str_desc //英文描述
     * @param null $error_info //错误描述
     * @return array
     */
    static public function _return_data($ret=200,$str_desc='',$error_info=null)
    {
        return array(
            'ret'=>$ret,
            'reason'=>$str_desc,
            'error_info'=>$error_info,
        );
    }
}
?>