<?php
$nns_db_clear_log_dir = dirname(dirname ( dirname ( __FILE__ ) ));
require_once $nns_db_clear_log_dir . DIRECTORY_SEPARATOR . "nn_cms_db" . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_class.php";

/**
 * 清理日志
 */
class clear_log_model{
	private $conn;
	private $db_obj;
	
	function clear_log_model() {
		$this->db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
		$this->conn = $this->db_obj->nns_db_connect ();
	}
	
	public function count_op_log(){
		$sql_str="select count(1) as num from nns_mgtvbk_op_log ";
		$result = $this->db_obj->nns_db_query ( $sql_str );
		return $result;
	}
	
	public function count_epg_log(){
		$sql_str="select count(1) as num from nns_mgtvbk_import_epg_log ";
		$result = $this->db_obj->nns_db_query ( $sql_str );
		return $result;
	}
	
	public function count_cp_log(){
		$sql_str="select count(1) as num from nns_mgtvbk_import_log ";
		$result = $this->db_obj->nns_db_query ( $sql_str );
		return $result;
	}

	public function count_global_error_log(){
		$sql_str = "select count(1) as num from nns_global_error_log";
		$result = $this->db_obj->nns_db_query($sql_str);
		return $result;
	}
	
	/**
	 * 清理日志
	 * @param  $action 执行操作（op,epg及cp）
	 * @param  $before_date 日期
	 * @return multitype:
	 */
	public function clear_log($user_id,$action,$before_date){
		if(isset($action) && isset($before_date)){
			switch ($action){
				case "op":
					$str_sql = "delete from nns_op_log where nns_op_admin_id='$user_id' and nns_op_time < '$before_date'";
					break;
				case "zxtb":
					$str_sql = "delete from nns_mgtvbk_op_log where nns_create_time < '$before_date'";
					break;
				case "epg":
					$str_sql = "delete from nns_mgtvbk_import_epg_log where nns_create_time < '$before_date'";
					break;
				case "cp":
					$str_sql = "delete from nns_mgtvbk_import_log where nns_create_time < '$before_date'";
					break;
				case "global_error":
					$str_sql = "delete from nns_global_error_log where nns_create_time < '$before_date'";
			}
			//die($str_sql);
			if (!empty($str_sql)){
				$result=$this->db_obj->nns_db_execute($str_sql);
				return $result;
			}
		}
		
	}
	
}