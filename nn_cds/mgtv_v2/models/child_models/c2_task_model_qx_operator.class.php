<?php
/**
 * 趣享运营商
 * @author LZ
 *
 */
class c2_task_model_v2 extends nn_public_model
{
	private $category_id = '';
	
	/**
	 * 主媒资注入
	 * 连续剧剧头注入package+title+poster  电影不需要注入
	 */
	public function do_video()
	{
		$c2_task_info = $this->arr_params['bk_c2_task_info'];
		if(empty($c2_task_info['nns_ref_id']))
		{
			return $this->_make_return_data(1,'点播传入缺失参数,参数为:'.var_export($this->arr_params,true));
		}
		$package_info = array();//package层信息
		$title_info = array();//title层信息
		$poster_info = array();//海报信息

		//根据vod_id获取主媒资信息
		$result_video = nl_vod::get_video_info_by_id($this->obj_dc, $c2_task_info['nns_ref_id']);
		if($result_video['ret'] != 0)
		{
			return $result_video;
		}
		$video_info = $result_video['data_info'][0];
		
		$this->str_cp_id = $video_info['nns_cp_id'];
        $this->_get_cp_info($this->str_cp_id);
        $this->category_id = $this->arr_cp_config[$this->str_cp_id]['nns_config']['import_cdn_category'];
		$result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $video_info['nns_id'],$video_info,$this->arr_sp_config);
		if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) < 1)
		{
			return $result_video_import_info;
		}			
		$title_info['id'] = $result_video_import_info['data_info'];
		$title_info['name'] = $video_info['nns_name'];
		$title_info['video_id'] = '';
		$title_info['summary'] = $video_info['nns_summary'];
		$title_info['director'] = $video_info['nns_director'];
		$title_info['actor'] = $video_info['nns_actor'];
		$title_info['index'] = $video_info['nns_all_index'];
		$title_info['provider'] = $video_info['nns_cp_id'];
		$title_info['pinyin'] = $video_info['nns_pinyin'];
		$title_info['language'] = empty($video_info['nns_language']) ? '中文' : $video_info['nns_language'];
		$title_info['area'] = empty($video_info['nns_area']) ? '无' : $video_info['nns_area'];
		
// 		if(empty($video_info['nns_image2']) && !empty($video_info['nns_image_v']))
// 		{
// 			$video_info['nns_image2'] = $video_info['nns_image_v'];
// 		}
// 		if(!empty($video_info['nns_image2']))
// 		{
// 			$img_save_name = str_replace('.JPG','.jpg',$video_info['nns_image2']);
// 			$file_img = dirname(dirname(dirname(dirname(__FILE__)))).'/data/downimg/'.$img_save_name;
// 			if(file_exists($file_img))
// 			{
// 				$poster_info['path'] = $this->get_image_url($img_save_name);
// 				$poster_info['provider'] = $video_info['nns_cp_id'];
// 			}
// 		}
		//package信息
		$package_info['id'] = $title_info['id'];
		$package_info['name'] = $title_info['name'];
		$package_info['provider'] = $video_info['nns_cp_id'];
		$package_info['summary'] = $package_info['name'];
		
		$xml = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml .= '<ADI>';
		if ($c2_task_info['nns_action'] != 'destroy')
		{
			$action = '';
			$task_action = $c2_task_info['nns_action'] == 'add' ? 'REGIST' : 'UPDATE';
			$xml .= $this->__build_package($package_info,$action);
			$xml .= '<Asset>';
			$xml .= $this->__build_title($title_info,1);
			$xml .= $this->__build_poster($poster_info);
			$xml .= '</Asset>';	
		}
		else 
		{
			$action = 'DELETE';
			$task_action = 'DELETE';
			$xml .= $this->__build_package($package_info,$action);
		}
		$xml .= '</ADI>';
		
		$file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'vod' . '_' . $task_action . '_' .  rand(100, 999) . '.xml';
		$c2_info = array(
				'nns_task_type'=>'Series',
				'nns_task_id'=> isset($c2_task_info['nns_id']) ? $c2_task_info['nns_id'] : $this->arr_params['nns_id'],
				'nns_task_name'=> isset($c2_task_info['nns_name']) ? $c2_task_info['nns_name'] : $this->arr_params['nns_name'],
				'nns_action'=>	$task_action,
				'nns_url' => $file_name,
				'nns_content' => $xml,
				'nns_desc' => 'Series,'.$task_action,
		);
		return $this->execute_c2_model($c2_info);
	}

	/**
	 * 片源注入
	 * 连续剧子集注入与电影注入时，携带分集title信息+movie信息
	 */
	public function do_media()
	{
		$c2_task_info = $this->arr_params['bk_c2_task_info'];
		if(empty($c2_task_info['nns_ref_id']) || empty($c2_task_info['nns_src_id']))
		{
			return $this->_make_return_data(1,'点播传入缺失参数,参数为:'.var_export($this->arr_params,true));
		} 

		$package_info = array();//package层信息
		$title_info = array();//title层信息
		$movie_info = array();//movie层信息
		$poster_info = array();//海报信息
		//根据media_id获取media信息
		$result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $c2_task_info['nns_ref_id']);
		if($result_media['ret'] !=0)
		{
			return $result_media;
		}
		$media_info = $result_media['data_info'];
		$result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $c2_task_info['nns_ref_id'],$media_info,$this->arr_sp_config);
		if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
		{
			return $result_media_import_info;
		}
		//根据index_id获取分集信息
		$result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $c2_task_info['nns_src_id']);
		if($result_index['ret'] != 0)
		{
		    return $result_index;
		}
		$index_info = $result_index['data_info'][0];
		$result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $index_info['nns_id'],$index_info,$this->arr_sp_config);
		if($result_index_import_info['ret'] != 0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) < 1)
		{
			return $result_index_import_info;
		}
		//根据vod_id获取主媒资信息
		$result_video = nl_vod::get_video_info_by_id($this->obj_dc, $index_info['nns_vod_id']);
		if($result_video['ret'] != 0)
		{
		    return $result_video;
		}
		$video_info = $result_video['data_info'][0];
		$result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $video_info['nns_id'],$video_info,$this->arr_sp_config);
		if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) < 1)
		{
			return $result_video_import_info;
		}
		
		$this->str_cp_id = $video_info['nns_cp_id'];
        $this->_get_cp_info($this->str_cp_id);
        $this->category_id = $this->arr_cp_config[$this->str_cp_id]['nns_config']['import_cdn_category'];
		if($video_info['nns_all_index'] > 1)//连续剧的单集片源
		{
			$title_info['id'] = $result_index_import_info['data_info'];
			$title_info['name'] = $index_info['nns_name'];
			$title_info['video_id'] = $result_video_import_info['data_info'];
			$title_info['summary'] = $index_info['nns_summary'];
			$title_info['director'] = $index_info['nns_director'];
			$title_info['actor'] = $index_info['nns_actor'];
			$title_info['index'] = $index_info['nns_index'] + 1;
			$series_type = 2;
// 			if(!empty($index_info['nns_image']))
// 			{
// 				$img_save_name = str_replace('.JPG','.jpg',$index_info['nns_image']);
// 				$file_img = dirname(dirname(dirname(dirname(__FILE__)))).'/data/downimg/'.$img_save_name;
// 				if(file_exists($file_img))
// 				{
// 					$poster_info['path'] = $this->get_image_url($img_save_name);
// 					$poster_info['provider'] = $video_info['nns_cp_id'];
// 				}
// 			}
		}
		else //电影片源
		{	
			$title_info['id'] = $result_video_import_info['data_info'];
			$title_info['name'] = $video_info['nns_name'];
			$title_info['video_id'] = '';
			$title_info['summary'] = $video_info['nns_summary'];
			$title_info['director'] = $video_info['nns_director'];
			$title_info['actor'] = $video_info['nns_actor'];
			$title_info['index'] = 1;
			$series_type = 0;
// 			if(empty($video_info['nns_image2']) && !empty($video_info['nns_image_v']))
// 			{
// 				$video_info['nns_image2'] = $video_info['nns_image_v'];
// 			}
// 			if(!empty($video_info['nns_image2']))
// 			{
// 				$img_save_name = str_replace('.JPG','.jpg',$video_info['nns_image2']);
// 				$file_img = dirname(dirname(dirname(dirname(__FILE__)))).'/data/downimg/'.$img_save_name;
// 				if(file_exists($file_img))
// 				{
// 					$poster_info['path'] = $this->get_image_url($img_save_name);
// 					$poster_info['provider'] = $video_info['nns_cp_id'];
// 				}
// 			}
		}
		
		//package信息
		$package_info['id'] = $result_media_import_info['data_info'];
		$package_info['name'] = $title_info['name'];
		$package_info['provider'] = $video_info['nns_cp_id'];
		$package_info['summary'] = $package_info['name'];
		
		$xml = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml .= '<ADI>';
		if ($c2_task_info['nns_action'] != 'destroy')
		{
			$action = '';
			$title_info['provider'] = $package_info['provider'];	
			$title_info['pinyin'] = $video_info['nns_pinyin'];
			$title_info['language'] = empty($video_info['nns_language']) ? '中文' : $video_info['nns_language'];
			$title_info['area'] = empty($video_info['nns_area']) ? '无' : $video_info['nns_area'];
			
			$movie_info['id'] = $result_media_import_info['data_info'];
			$movie_info['provider'] = $package_info['provider'];
			$movie_info['name'] = $media_info['nns_name'];
			$movie_info['file_bite'] = $media_info['nns_kbps'];
			$movie_info['time_len'] = $media_info['nns_file_time_len'];
			if(!empty($c2_task_info['nns_file_path']))
			{
				$movie_info['file_path'] = $c2_task_info['nns_file_path'];
			}
			else 
			{
				$movie_info['file_path'] = $media_info['nns_url'];
			}
			if($media_info['nns_mode'] != 'std' && $media_info['nns_mode'] != 'low')
			{
				$movie_info['file_mode'] = 'Y';
			}
			else 
			{
				$movie_info['file_mode'] = 'N';
			}
				
			$xml .= $this->__build_package($package_info,$action);
			$xml .= '<Asset>';
			$xml .= $this->__build_title($title_info,$series_type);
			$xml .= $this->__build_movie($movie_info);
			$xml .= $this->__build_poster($poster_info);
			$xml .= '</Asset>';
		}
		else
		{
			$action = 'DELETE';
			$xml .= $this->__build_package($package_info,$action);
		}
		
		$xml .= '</ADI>';

		$task_action = $c2_task_info['nns_action'] == 'destroy' ? 'DELETE' : 'REGIST';
		$file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'vod_media' . '_' . $task_action . '_' .  rand(100, 999) . '.xml';
		$c2_info = array(
				'nns_task_type'=>'Movie',
				'nns_task_id'=> isset($c2_task_info['nns_id']) ? $c2_task_info['nns_id'] : $this->arr_params['nns_task_id'],
				'nns_task_name'=> isset($c2_task_info['nns_name']) ? $c2_task_info['nns_name'] : $this->arr_params['nns_task_name'],
				'nns_action'=>	$task_action,
				'nns_url' => $file_name,
				'nns_content' => $xml,
				'nns_desc' => 'Movie,'.$task_action,
		);
		return $this->execute_c2_model($c2_info);
	}
	/**
	 * 组装package层信息
	 * @param array $params
	 * array(
	 * 	id=>注入ID,
	 *  name=>名称
	 *  provider=>提供商
	 *  summary=>简介
	 * )
	 * @param string $action 操作
	 */
	private function __build_package($params,$action = '')
	{
		$xml = '<Metadata>';
		$xml .= '<AMS Asset_Name="'.$params['name'].'" Provider="'.$params['provider'].'" Product="" Version_Major="1" Version_Minor="0" Description="'.$params['summary'].'" Creation_Date="'.date('Y-m-d').'" Provider_ID="'.$params['provider'].'" Asset_ID="'.$params['id'].'" Asset_Class="package" Verb="'.$action.'" />';
		$xml .= '</Metadata>';
		return $xml;
	}
	/**
	 * 组装TITLE层信息
	 * @param $params
	 * array(
	 * 	id=>注入ID,
	 * 	video_id=>主媒资注入ID,//当为子集时，才有此值
	 *  name=>名称,
	 *  pinyin=>拼音首字母,
	 *  summary=>简介,
	 *  provider=>内容提供商ID,
	 *  language=>语言,
	 *  director=>导演,//多个以逗号分隔
	 *  actor=>演员,//多个以逗号分隔
	 *  index=>集数,//子集为子集数，连续剧剧头为总集数
	 *  area=>地区
	 * )
	 * @param $series_type 1-连续剧父集 0-电影 2子集
	 * @author zhiyong.luo
	 */
	private function __build_title($params,$series_type = 0)
	{
		if(empty($params))
		{
			return;
		}
		$xml = '<Metadata>';
		$xml .= '<AMS Asset_Name="'.$params['name'].'" Provider="'.$params['provider'].'" Product="MOD" Version_Major="1" Version_Minor="0" Description="'.$params['name'].'" Creation_Date="'.date('Y-m-d').'" Provider_ID="'.$params['provider'].'" Asset_ID="'.$params['id'].'" Asset_Class="title" />';
		$xml .= '<App_Data App="MOD" Name="Title" Value="'.$params['name'].'" />';//名称
		$xml .= '<App_Data App="BMS" Name="Title_FG" Value="" />';
		$xml .= '<App_Data App="MOD" Name="Subject_IDs" Value="'.$this->category_id.'" />';
		$xml .= '<App_Data App="BMS" Name="TitleSearchCode" Value="'.$params['pinyin'].'" />';//拼音检索
		$xml .= '<App_Data App="MOD" Name="Summary_Medium" Value="'.mb_strcut($params['summary'],0,256,'utf-8').'" />';//简介
		$xml .= '<App_Data App="MOD" Name="Summary_Medium_FG" Value="" />';
		$xml .= '<App_Data App="BMS" Name="SupplyLang" Value="'.$params['language'].'" />';//节目语种
		$xml .= '<App_Data App="BMS" Name="ProduceDate" Value="" />';//影视公司出品日期
		$xml .= '<App_Data App="BMS" Name="ControlLevel" Value="0" />';
		$xml .= '<App_Data App="MOD" Name="DirectorName" Value="'.$params['director'].'" />';
		$xml .= '<App_Data App="BMS" Name="DirectorName_FG" Value="" />';
		$xml .= '<App_Data App="BMS" Name="DirectorSearchCode" Value="" />';
		$xml .= '<App_Data App="MOD" Name="Actors_Display" Value="'.mb_strcut($params['actor'],0,128,'utf-8').'" />';//演员
		$xml .= '<App_Data App="BMS" Name="Actors_Display_FG" Value="" />';
		$xml .= '<App_Data App="BMS" Name="ActorSearchCode" Value="" />';
		$xml .= '<App_Data App="MOD" Name="Propagation_Priority" Value="" />';
		$xml .= '<App_Data App="MOD" Name="VODProvider" Value="'.$params['provider'].'" />';//节目制片商
		$xml .= '<App_Data App="BMS" Name="VODProvider_FG" Value="" />';
		$xml .= '<App_Data App="BMS" Name="SuperVodID" Value="'.$params['video_id'].'" />';//影片类型为连续剧子集时，传入父集的package的Asset_ID
		$xml .= '<App_Data App="BMS" Name="IsSitcom" Value="'.$series_type.'" />';//0-影片 1-连续剧父集 2-连续剧子集
		//只有在IsSitcom取值为2时, SitcomNum 才意义
		$xml .= '<App_Data App="BMS" Name="SitcomNum" Value="'.$params['index'].'" />';//集数
		$xml .= '<App_Data App="BMS" Name="IsRecommend" Value="N" />';
		$xml .= '<App_Data App="BMS" Name="National" Value="'.$params['area'].'" />';//节目产地描述
		$xml .= '<App_Data App="BMS" Name="National_FG" Value="" />';
		$xml .= '<App_Data App="BMS" Name="NewFlag" Value="N" />';
		$xml .= '<App_Data App="BMS" Name="Licensing_Window_Start" Value="" />';
		$xml .= '<App_Data App="BMS" Name="Licensing_Window_End" Value="" />';
		$xml .= '<App_Data App="MOD" Name="Suggested_Price" Value="0" />';
		$xml .= '</Metadata>';
		return $xml;
	}
	/**
	 * 组装MOVIE层信息
	 * @param array $movie_params
	 * array(
	 * 	
	 * )
	 */
	private function __build_movie($movie_params)
	{
		if(empty($movie_params))
		{
			return;
		}
		$file_bit_rate = $movie_params['file_bite'] > 0 ? $movie_params['file_bite'] : 1;
		$asset_id = $movie_params['provider'].$movie_params['id'];//由于注入时，packageid为movieid，用于播放，MOVIE层的id不能与packageid相同，故注入CDN的加上CP前缀 用于区分
		$xml = '<Asset>';
		$xml .= '<Metadata>';
		$xml .= '<AMS Asset_Name="'.$movie_params['name'].'" Provider="'.$movie_params['provider'].'" Product="" Version_Major="1" Version_Minor="0" Description="'.$movie_params['name'].'" Creation_Date="'.date('Y-m-d').'" Provider_ID="'.$movie_params['provider'].'" Asset_ID="'.$asset_id.'"  Asset_Class="movie" />';
		$xml .= '<App_Data App="MOD" Name="Run_Time" Value="'.gmstrftime('%H:%M:%S',$movie_params['time_len']).'" />';
		$xml .= '<App_Data App="MOD" Name="HDContent" Value="'.$movie_params['file_mode'].'" />';
		$xml .= '<App_Data App="MOD" Name="Bit_Rate" Value="'. $file_bit_rate .'" />';
		$xml .= '<App_Data App="MOD" Name="FileFormat" Value="MPEG_2_TRANSPORT" />';
		$xml .= '<App_Data App="MOD" Name="CodeFormat" Value="H.264" />';
		$xml .= '<App_Data App="MOD" Name="Encryption" Value="N" />';
		$xml .= '</Metadata>';
		$xml .= '<Content Value="'.$movie_params['file_path'].'" />';
		$xml .= '</Asset>';
		return $xml;
	}
	/**
	 * 组装POSTER海报信息
	 * @param array $poster_params
	 * array(
	 * 	provider=>提供商ID,
	 * 	path=>海报地址
	 * )
	 * @return string
	 */
	private function __build_poster($poster_params)
	{
		if(empty($poster_params))
		{
			return;
		}
		$xml = '<Asset>';
		$guid = np_guid_rand();
		$xml .= '<Metadata>';
		$xml .= '<AMS Asset_Name="" Provider="'.$poster_params['provider'].'" Product="" Version_Major="1" Version_Minor="0" Description="" Creation_Date="'.date('Y-m-d').'" Provider_ID="'.$poster_params['provider'].'" Asset_ID="'.$guid.'" Asset_Class="poster"/>';
		$xml .= '</Metadata>';
		$xml .= '<Content Value="'.$poster_params['path'].'" />';
		$xml .= '</Asset>';
		return $xml;
	}
	/**
	 * 趣享华为C2注入
	 * @param $c2_info array
	 * @return Ambigous <multitype:number , multitype:number string >
	 */
	public function execute_c2_model($c2_info)
	{
		$c2_info['nns_type'] = 'std';
		$c2_info['nns_org_id'] = $this->str_sp_id;
		$c2_info['nns_id'] = np_guid_rand();
		$c2_info['nns_send_time'] = date('Y-m-d H:i:s');
		if($c2_info['nns_action'] === 'DELETE')//如果是重发的删除，直接置为成功状态，因为华为VOD系统需要手动再删除，然后从另外一个接口通知回来
		{
			$c2_info['nns_notify_result'] = '0';
			$task_c2_info = array(
					'nns_status' => 0,
					'nns_modify_time' => date('Y-m-d H:i:s'),
			);
		}
		else
		{
			$c2_info['nns_notify_result'] = '';
			$task_c2_info = array(
					'nns_status' => 5,
					'nns_modify_time' => date('Y-m-d H:i:s'),
			);
		}
		//加载soap客户端文件
		$file_soap_client_file = dirname(dirname(dirname(dirname(__FILE__)))) . '/import_service/'.$this->str_sp_id.'/client.php';
		if(!file_exists($file_soap_client_file))
		{
			return $this->_make_return_data(1,'加载soap客户端文件不存在,文件路径:['.$file_soap_client_file.']');
		}
		global $g_bk_web_url;
		$bk_web_url = (isset($g_bk_web_url) && strlen($g_bk_web_url)>0) ? $g_bk_web_url : '';
		unset($g_bk_web_url);
		$bk_web_url = ltrim(ltrim(trim($bk_web_url),'/'),'\\');
		if(strlen($bk_web_url) <1)
		{
			return $this->_make_return_data(1,'播控后台系统配置管理里面后台基本地址未配置:['.$bk_web_url.']');
		}
		include_once $file_soap_client_file;
		//检查时间范围是否有效执行
//		if ($this->exists_c2_commond($c2_info))
//		{
//			return $this->_make_return_data(1,'该task在一小时内执行过或则task查询无数据,task信息:'.var_export($c2_info,true));
//		}
		$sub_path = date('Y-m-d');
		$file = $c2_info['nns_url'];
		$c2_info['nns_url'] = $sub_path . '/' . $file;
		//生成soap xml文件 路径
		$file_path = $this->c2_xml_path . '/inject_xml/' . $sub_path . '/';
		if (!is_dir($file_path))
		{
			mkdir($file_path, 0777, true);
		}
		$file_path .= $file;
		file_put_contents($file_path, $c2_info['nns_content']);
		//上传到M3U8的FTP服务器上
		$up_re = $this->up_to_ftp($file_path,MOVIE_FTP,false,"./");
		//$ftpPath = trim(rtrim($this->arr_sp_config['xml_ftp'],'/')).'/'.$sub_path.'/';
		if($up_re['ret'] != 0)
		{
			//删除本地文件
			unlink($file_path);
			$c2_info['nns_result'] = '[-1]' . $up_re['reason'];
			$c2_info['nns_notify_result'] = -1;
			nl_c2_log::add($this->obj_dc, $c2_info);
			return $this->_make_return_data(1,'ADI文件上传至M3U8失败:' . $up_re['reason']);
		}
		
		try
		{
			$result = null;
			$detail_params = '<Provider_ID>' . $this->str_cp_id . '</Provider_ID>';
			$result = client::publish(MOVIE_FTP,$file,$detail_params,$bk_web_url,$c2_info['nns_task_type']);
			
			$bool = true;
			if($result->publishReturn->returnCode != 0)
			{
				$c2_info['nns_result'] = '[-1]'.'('.$result->publishReturn->returnCode.')'.$result->publishReturn->message;
				$bool = false;
			}
			else 
			{
				$c2_info['nns_result'] = '[0]';
			}
			//CDN单日志模式开启
			$exist_result = nl_c2_log::check_task_exist($this->obj_dc, array('nns_task_id' => $c2_info['nns_task_id'],'nns_action' => $c2_info['nns_action']));
					
			if($this->arr_sp_config['cdn_single_log_mode'] == '0' && $exist_result['ret'] == 0)
			{
				$c2_id = $exist_result['data_info'][0]['nns_id'];
				$c2_info['nns_id'] = $c2_id;
				$c2_info['nns_notify_fail_reason'] = '';
				$c2_info['nns_cdn_send_time'] = $exist_result['data_info'][0]['nns_cdn_send_time'] + 1;
				$c2_info['nns_create_time'] = $exist_result['data_info'][0]['nns_create_time'];
				$c2_info['nns_notify_content'] = "";				
				nl_c2_log::edit($this->obj_dc, $c2_info, $c2_id);
			}
			else 
			{
				nl_c2_log::add($this->obj_dc, $c2_info);
			}
			if($bool)
			{
				$c2_re = nl_c2_task::edit($this->obj_dc, $task_c2_info, $c2_info['nns_task_id']);//修改C2任务
				return $this->_make_return_data(0,'发送C2成功'.var_export($result,true));
			}
			else
			{
				$task_c2_info['nns_status'] = '-1';
				$c2_re = nl_c2_task::edit($this->obj_dc, $task_c2_info, $c2_info['nns_task_id']);//修改C2任务
				return $this->_make_return_data(1,'发送C2失败'.var_export($result,true));
			}
		
		} catch (Exception $e)
		{
			$task_c2_info['nns_status'] = '-1';
			$c2_re = nl_c2_task::edit($this->obj_dc, $task_c2_info, $c2_info['nns_task_id']);//修改C2任务
			$c2_info['nns_result'] = '[-1]';
			$c2_info['nns_notify_result'] = -1;
			nl_c2_log::add($this->obj_dc, $c2_info);
			return $this->_make_return_data(1,'发送C2失败');
		}
	}
	/**
	 * 注入反馈回调
	 */
	public function save_c2_notify()
	{
		//根据注入ID获取相关信息，此ID不是主媒资就是片源的
		$package_id = $this->arr_params['id'];
		$result_video_import_info = public_model_exec::get_asset_id_by_sp_import_id($this->str_sp_id, 'video', $package_id,null,$this->arr_sp_config);
		if($result_video_import_info['ret'] != 0)
		{
			return $result_video_import_info;
		}
		
		//ID是片源
		if(!is_array($result_video_import_info['data_info']['data_info']) || empty($result_video_import_info['data_info']['data_info']))
		{
			$result_media_import_info = public_model_exec::get_asset_id_by_sp_import_id($this->str_sp_id, 'media', $package_id,null,$this->arr_sp_config);
			if($result_media_import_info['ret'] != 0 || !is_array($result_media_import_info['data_info']['data_info']) || empty($result_media_import_info['data_info']['data_info']))
			{
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '通过注入ID查询主媒资/片源的GUID时，查询失败：' . $package_id, $this->str_sp_id);
				return $this->_make_return_data(1,'入参参数有误');
			}
			$package_info = $result_media_import_info['data_info']['data_info'];
			$check_video_bool = false;
		}
		else 
		{
			$package_info = $result_video_import_info['data_info']['data_info'];
			$check_video_bool = true;
		}
		
		if($check_video_bool) //主媒资
		{
            nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . 'CDN通知反馈的是主媒资', $this->str_sp_id);
			$query_params = array(
					'nns_type' => 'video',//主媒资
					//'nns_status' => '5',//正在注入
					'nns_ref_id' => $package_info['nns_id'],
					'nns_org_id' => $this->str_sp_id
			);
			$task_type = "Series";
		}
		else
		{
            nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . 'CDN通知反馈的是片源', $this->str_sp_id);
			$query_params = array(
					'nns_type' => 'media',//片源
					//'nns_status' => '5',//正在注入
					'nns_ref_id' => $package_info['nns_id'],
					'nns_org_id' => $this->str_sp_id
			);
			$task_type = "Movie";
		}
		//根据注入ID获取c2任务ID
		$c2_task_info = nl_c2_task::query_by_condition($this->obj_dc,$query_params,0,1);
		if($c2_task_info['ret'] != 0 || !is_array($c2_task_info['data_info']))
		{
            nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '查询c2任务失败,查询参数：'.var_export($query_params,true), $this->str_sp_id);
			return $c2_task_info;
		}
		$log_query_params = array(
				'nns_task_id' => $c2_task_info['data_info'][0]['nns_id'],
				'nns_task_type' => $task_type,
				'nns_org_id' => $this->str_sp_id,
		);
		//根据c2id查询c2注入日志
        //可能会查询出多个
		$c2_log_re = nl_c2_log::query_by_condition($this->obj_dc, $log_query_params);
		if($c2_log_re['ret'] != 0 || !is_array($c2_log_re['data_info']) || empty($c2_log_re['data_info']))
		{
            nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '查询c2任务注入日志失败,查询参数：'.var_export($log_query_params,true), $this->str_sp_id);
			return $c2_log_re;
		}
		//修改c2日志
		$c2_id = array();
        foreach ($c2_log_re['data_info'] as $re_c2_log)
        {
            $c2_id[] = $re_c2_log['nns_id'];
        }
        /************通过鉴权 方式判断是否注入成功*************/
        $is_import = true;
        if(!$check_video_bool && (!isset($this->arr_sp_config['cdn_query_mediaurl']) || (int)$this->arr_sp_config['cdn_query_mediaurl'] === 0)) //片源注入才会走此逻辑
        {
            $notify_file = dirname(dirname(dirname(__FILE__))) . "/" . $this->str_sp_id . "/models/child_c2_task_notify_model.php";
            if (file_exists($notify_file))
            {
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '根据CDN鉴权接口判断是否注入成功', $this->str_sp_id);
                $notify_model = new child_c2_task_notify_model($this->str_sp_id);
                $is_import = $notify_model->notify_explain($package_id);
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '判断结果为：' . var_export($is_import,true), $this->str_sp_id);
            }
        }
        /*************************************************/
        $c2_log_info = array(
            'nns_notify_fail_reason' => '',
            'nns_notify_result' => $is_import ? 0 : -1,
            'nns_notify_time' => date("Y-m-d H:i:s"),
        );

		$edit_re = nl_c2_log::edit($this->obj_dc, $c2_log_info, $c2_id);//修改C2日志
		if($edit_re['ret'] != 0)
		{
            nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '修改c2任务注入日志失败,修改参数：' . var_export($c2_log_info,true), $this->str_sp_id);
			return $edit_re;
		}
        $c2_edit_info = array(
            'nns_status' => $is_import ? 0 : -1,
        );
        if(!$check_video_bool && isset($this->arr_sp_config['cdn_query_mediaurl']) && (int)$this->arr_sp_config['cdn_query_mediaurl'] !== 0)
        {
            $c2_edit_info = array(
                'nns_status' => 6, //等待获取CDN播放串
            );
        }
		$c2_re = nl_c2_task::edit($this->obj_dc, $c2_edit_info, $c2_task_info['data_info'][0]['nns_id']);//修改C2任务
		if($c2_re['ret'] != 0)
		{
            nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '修改c2任务失败,修改参数：' . var_export($c2_edit_info,true), $this->str_sp_id);
			return $c2_re;
		}
		return $c2_re;
	}

    /**
     * 江西注入反馈回调
     */
    public function save_c2_jx_notify()
    {
        //根据注入ID获取相关信息，此ID不是主媒资就是片源的
        $package_id = $this->arr_params['Asset_ID'];
        $result = $this->arr_params['result'];
            $result_video_import_info = public_model_exec::get_asset_id_by_sp_import_id($this->str_sp_id, 'video', $package_id, null, $this->arr_sp_config);
            if ($result_video_import_info['ret'] != 0)
            {
                return $result_video_import_info;
            }

            //ID是片源
            if (!is_array($result_video_import_info['data_info']['data_info']) || empty($result_video_import_info['data_info']['data_info']))
            {
                $result_media_import_info = public_model_exec::get_asset_id_by_sp_import_id($this->str_sp_id, 'media', $package_id, null, $this->arr_sp_config);
                if ($result_media_import_info['ret'] != 0 || !is_array($result_media_import_info['data_info']['data_info']) || empty($result_media_import_info['data_info']['data_info']))
                {
                    nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '通过注入ID查询主媒资/片源的GUID时，查询失败：' . $package_id, $this->str_sp_id);
                    return $this->_make_return_data(1, '入参参数有误');
                }
                $package_info = $result_media_import_info['data_info']['data_info'];
                $check_video_bool = false;
            }
            else
            {
                $package_info = $result_video_import_info['data_info']['data_info'];
                $check_video_bool = true;
            }

            if ($check_video_bool) //主媒资
            {
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . 'CDN通知反馈的是主媒资', $this->str_sp_id);
                $query_params = array('nns_type' => 'video',//主媒资
                    'nns_status' => '5',//正在注入
                    'nns_ref_id' => $package_info['nns_id'], 'nns_org_id' => $this->str_sp_id);
                $task_type = "Series";
            }
            else
            {
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . 'CDN通知反馈的是片源', $this->str_sp_id);
                $query_params = array('nns_type' => 'media',//片源
                    'nns_status' => '5',//正在注入
                    'nns_ref_id' => $package_info['nns_id'], 'nns_org_id' => $this->str_sp_id);
                $task_type = "Movie";
            }
            //根据注入ID获取c2任务ID
            $c2_task_info = nl_c2_task::query_by_condition($this->obj_dc, $query_params, 0, 1);
            if ($c2_task_info['ret'] != 0 || !is_array($c2_task_info['data_info']))
            {
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '查询c2任务失败,查询参数：' . var_export($query_params, true), $this->str_sp_id);
                return $c2_task_info;
            }
            $log_query_params = array('nns_task_id' => $c2_task_info['data_info'][0]['nns_id'], 'nns_task_type' => $task_type, 'nns_org_id' => $this->str_sp_id,);
            //根据c2id查询c2注入日志
            //可能会查询出多个
            $c2_log_re = nl_c2_log::query_by_condition($this->obj_dc, $log_query_params);
            if ($c2_log_re['ret'] != 0 || !is_array($c2_log_re['data_info']) || empty($c2_log_re['data_info']))
            {
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '查询c2任务注入日志失败,查询参数：' . var_export($log_query_params, true), $this->str_sp_id);
                return $c2_log_re;
            }
            //修改c2日志
            $c2_id = array();
            foreach ($c2_log_re['data_info'] as $re_c2_log)
            {
                $c2_id[] = $re_c2_log['nns_id'];
            }
            /************通过鉴权 方式判断是否注入成功*************/
            $is_import = true;
            if (!$check_video_bool) //片源注入才会走此逻辑
            {
                $notify_file = dirname(dirname(dirname(__FILE__))) . "/" . $this->str_sp_id . "/models/child_c2_task_notify_model.php";
                if (file_exists($notify_file))
                {
                    nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '根据CDN鉴权接口判断是否注入成功', $this->str_sp_id);
                    $notify_model = new child_c2_task_notify_model($this->str_sp_id);
                    $is_import = $notify_model->notify_explain($package_id);
                    nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '判断结果为：' . var_export($is_import, true), $this->str_sp_id);
                }
            }
            /*************************************************/
            if($result=='0')
            {
            $c2_log_info = array('nns_notify_fail_reason' => '', 'nns_notify_result' => $is_import ? 0 : -1, 'nns_notify_time' => date("Y-m-d H:i:s"),);
            $edit_re = nl_c2_log::edit($this->obj_dc, $c2_log_info, $c2_id);//修改C2日志
            if ($edit_re['ret'] != 0)
            {
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '修改c2任务注入日志失败,修改参数：' . var_export($c2_log_info, true), $this->str_sp_id);
                return $edit_re;
            }
            $c2_edit_info = array('nns_status' => $is_import ? 0 : -1,);
            $c2_re = nl_c2_task::edit($this->obj_dc, $c2_edit_info, $c2_task_info['data_info'][0]['nns_id']);//修改C2任务
            if ($c2_re['ret'] != 0)
            {
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '修改c2任务失败,修改参数：' . var_export($c2_edit_info, true), $this->str_sp_id);
                return $c2_re;
            }
            return $c2_re;
            }
            else
            {
                $c2_log_info = array('nns_notify_fail_reason' => '', 'nns_notify_result' =>  -1, 'nns_notify_time' => date("Y-m-d H:i:s"),);
                $edit_re = nl_c2_log::edit($this->obj_dc, $c2_log_info, $c2_id);//修改C2日志
                if ($edit_re['ret'] != 0)
                {
                    nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '修改c2任务注入日志失败,修改参数：' . var_export($c2_log_info, true), $this->str_sp_id);
                    return $edit_re;
                }
                $c2_edit_info = array('nns_status' =>  -1,);
                $c2_re = nl_c2_task::edit($this->obj_dc, $c2_edit_info, $c2_task_info['data_info'][0]['nns_id']);//修改C2任务
                if ($c2_re['ret'] != 0)
                {
                    nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '修改c2任务失败,修改参数：' . var_export($c2_edit_info, true), $this->str_sp_id);
                    return $c2_re;
                }
                return $c2_re;
            }
    }
	/**
	 * VOD系统主动删除通知
	 */
	public function notice_delete_notify()
	{
		//根据注入ID获取相关信息，此ID不是主媒资就是片源的
		$package_id = $this->arr_params['id'];
		$result_video_import_info = public_model_exec::get_asset_id_by_sp_import_id($this->str_sp_id, 'video', $package_id,null,$this->arr_sp_config);
		if($result_video_import_info['ret'] != 0)
		{
			return $result_video_import_info;
		}
		
		//ID是分集
		if(!is_array($result_video_import_info['data_info']['data_info']) || empty($result_video_import_info['data_info']['data_info']))
		{
			$result_media_import_info = public_model_exec::get_asset_id_by_sp_import_id($this->str_sp_id, 'media', $package_id,null,$this->arr_sp_config);
			if($result_media_import_info['ret'] != 0 || !is_array($result_media_import_info['data_info']['data_info']) || empty($result_media_import_info['data_info']['data_info']))
			{
				return $this->_make_return_data(1,"入参参数有误");
			}
			$package_info = $result_media_import_info['data_info']['data_info'];
			$check_video_bool = false;
		}
		else
		{
			$package_info = $result_video_import_info['data_info']['data_info'];
			$check_video_bool = true;
		}
		if($check_video_bool) //主媒资
		{
			$query_params = array(
					'nns_type' => 'video',//主媒资
					//'nns_status' => '0',//注入成功
					'nns_ref_id' => $package_info['nns_id'],
					'nns_org_id' => $this->str_sp_id
			);
		}
		else
		{
			$query_params = array(
					'nns_type' => 'media',//主媒资
					//'nns_status' => '0',//注入成功
					'nns_ref_id' => $package_info['nns_id'],
					'nns_org_id' => $this->str_sp_id
			);
		}
		//根据注入ID获取c2任务ID
		$c2_task_info = nl_c2_task::query_by_condition($this->obj_dc,$query_params,0,1);
		if($c2_task_info['ret'] != 0 || !is_array($c2_task_info['data_info']))
		{
			return $c2_task_info;
		}
		//改为删除操作
		$c2_edit_info = array(
				'nns_status' => 0,
				'nns_epg_status' => '97',
				'nns_action' => 'destroy',
		);
		$c2_re = nl_c2_task::edit($this->obj_dc, $c2_edit_info, $c2_task_info['data_info'][0]['nns_id']);
		return $c2_re;
	}
	/**
	 * 把ADIxml文件上传至与M3U8相同的FTP上
	 * @param string $local_path 本地文件路径
	 * @param string $up_ftp  上传FTP
	 * @param bool $passive 主被动模式，默认被动模式 
	 * @param string $path 上传后FTP路径，默认当前目录
	 * @return Ambigous <multitype:number , multitype:number string >
	 */
	public function up_to_ftp($local_path,$up_ftp,$passive = true,$path=".")
	{
		$arr_ftp = parse_url($up_ftp);
		if(!isset($arr_ftp['port']))
		{
			$arr_ftp['port'] = 21;
		}
		$ftp = new nns_ftp($arr_ftp['host'], $arr_ftp['user'], $arr_ftp['pass'], $arr_ftp['port']);
		$ftpcon = $ftp->connect(10, $passive);
		if(!$ftpcon)
		{
			return $this->_make_return_data(1,'FTP链接失败' . $up_ftp);
		}
		$path_arr = pathinfo($local_path);
		$ftp_re = $ftp->up($path, $path_arr['basename'], $local_path);
		if(!$ftp_re)
		{
			$error_msg = '上传FTP'.$up_ftp.'失败,上传路径:'.$path.',上传文件名:'.$path_arr['basename'].',上传的本地文件:'.$local_path;
			return $this->_make_return_data(1,$error_msg);
		}
		return $this->_make_return_data(0,"上传成功");
	}
}