<?php
/**
 * starcor 江苏思华（老版本）规范ADI CDN注入
 * @author liangpan
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/v2/ns_core/m_mediainfo.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv/kukai/get_increment_info_xml.php';
class c2_task_model_v2 extends nn_public_model
{
    public $get_increment_info_xml = null;

    /*
     *  主媒资CDN
     */
	public function do_video()
	{
        $this->get_increment_info_xml = new get_increment_info_xml();
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
            return $this->_make_return_data(1, '点播传入参数无nns_ref_id字段,参数为:' . var_export($this->arr_params));
        }
        $arr_push = $this->get_increment_info_xml->make_vod_arr($this->obj_dc,$this->arr_params['bk_c2_task_info']['nns_id'],$this->arr_params['bk_c2_task_info']['nns_ref_id'],$this->arr_sp_config,$this->arr_cp_config,$this->str_sp_id);
        if(isset($arr_push['ret']) && $arr_push['ret'] != 0)
        {
            return $arr_push;
        }
        $result_video = $arr_push['result_video'];
        $arr_push = $arr_push['push'];
        $str_push = json_encode($arr_push);
	    $action = ($result_video['nns_deleted'] !=1) ? ($result_video['nns_modify_time'] > $result_video['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
	    $c2_info = array(
	        'nns_id'=>np_guid_rand(),
	        'nns_task_type'=>'Series',
	        'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	        'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
	        'nns_action'=>	$action,
	        'nns_content'=>	$str_push,
	    );

		if($result_video['nns_all_index'] >1000)
		{
		    return $this->_make_return_data(1,"分集>1000");
		}
	    $result = $this->execute_self_c2($c2_info);
	    return $result;
	}
	/*
	 *  分集CDN
	 */
	public function do_index()
	{
        $this->get_increment_info_xml = new get_increment_info_xml();
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_ref_id字段,参数为:'.var_export($this->arr_params));
	    }
        $arr_push = $this->get_increment_info_xml->make_index_arr($this->obj_dc,$this->arr_params['bk_c2_task_info']['nns_id'],$this->arr_params['bk_c2_task_info']['nns_ref_id'],$this->arr_sp_config,$this->arr_cp_config,$this->str_sp_id);
        if(isset($arr_push['ret']) && $arr_push['ret'] != 0)
        {
            return $arr_push;
        }
        $result_video = $arr_push['result_video'];
        $result_index = $arr_push['result_index'];
        //1、爱奇艺、优酷、腾讯等互联网  ——  查找当前分集下片源ID，为了修改分集原始ID，为了爱奇艺
        $arr_push = $arr_push['push'];
	    $str_push = json_encode($arr_push);
	    $action = ($result_video['nns_deleted'] !=1) ? ($result_video['nns_modify_time'] > $result_video['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
	    $c2_info = array(
	        'nns_id'=>np_guid_rand(),
	        'nns_task_type'=>'Program',
	        'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	        'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
	        'nns_action'=>	$action,
	        'nns_content'=>	$str_push,
	    );
		if($result_video['nns_all_index'] >2000)
		{
		    return $this->_make_return_data(1,"分集>2000");
		}
	    $result = $this->execute_self_c2($c2_info);
	    return $result;
	}
	/*
	 *  片源CDN
	 */
	public function do_media()
	{
        $this->get_increment_info_xml = new get_increment_info_xml();
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
		if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_ref_id字段,参数为:'.var_export($this->arr_params));
	    }
        $arr_push = $this->get_increment_info_xml->make_media_arr($this->obj_dc,$this->arr_params['bk_c2_task_info']['nns_id'],$this->arr_params['bk_c2_task_info']['nns_ref_id'],$this->arr_sp_config,$this->arr_cp_config,$this->str_sp_id,$this->arr_params['nns_file_path']);
        if(isset($arr_push['ret']) && $arr_push['ret'] != 0)
        {
            return $arr_push;
        }
        $result_video = $arr_push['result_video'];
        $result_index = $arr_push['result_index'];
        $result_media = $arr_push['result_media'];
        $arr_push = $arr_push['push'];
		$str_push = json_encode($arr_push);
		$action = ($result_media['nns_deleted'] !=1) ? ($result_media['nns_modify_time'] > $result_media['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
		$c2_info = array(
		        'nns_id'=>np_guid_rand(),
				'nns_task_type'=>'Movie',
				'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	            'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
				'nns_action'=>	$action,
		        'nns_content'=>	$str_push,
		);
		if($result_video['nns_all_index'] >1000)
		{
		    return $this->_make_return_data(1,"分集>1000");
		}
		$result = $this->execute_self_c2($c2_info);
		return $result;
	}
	/**
	 *
	 * @param unknown $c2_info
	 */
	public function execute_self_c2($c2_info,$flag=true)
	{
	    $c2_info['nns_type'] = 'std';
	    $c2_info['nns_org_id'] = $this->str_sp_id;
	    $c2_info['nns_desc'] ='';
// 	    $c2_info['nns_id'] = np_guid_rand();
	    include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/np/np_http_curl.class.php';
	    //检查时间范围是否有效执行
	    if ($this->exists_c2_commond($c2_info))
	    {
	        return $this->_make_return_data(1,'该task在一小时内执行过或则task查询无数据,task信息:'.var_export($c2_info,true));
	    }
	    $sub_path = date('Y-m-d');
	    $file = $this->str_sp_id . '_' .$c2_info['nns_action'].'_'. $c2_info['nns_id'] . '.txt';
	    $c2_info['nns_url'] = $sub_path . '/'.strtolower($c2_info['nns_task_type']).'/' . $file;
	    //生成soap xml文件 路径
	    $file_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/mgtv/' . $this->str_sp_id . '/inject_xml/' . $sub_path . '/' .strtolower($c2_info['nns_task_type']).'/';
	    if (!is_dir($file_path))
	    {
	        mkdir($file_path, 0777, true);
	    }
	    $file_path .= $file;
	    file_put_contents($file_path, $c2_info['nns_content']);
	    $cdn_send_mode = $this->arr_sp_config['cdn_send_mode'];
	    if($cdn_send_mode != '1')
	    {
	        $c2_info['nns_desc'] = "注入下游模式非HTTP";
	        nl_c2_log::add($this->obj_dc, $c2_info);
	        return $this->_make_return_data(1,"注入下游模式非HTTP");
	    }
	    $cdn_send_mode_url = $this->arr_sp_config['cdn_send_mode_url'];
	    if(strlen($cdn_send_mode_url) <1)
	    {
	        $c2_info['nns_desc'] = "注入下游媒资基本信息请求url为空";
	        nl_c2_log::add($this->obj_dc, $c2_info);
	        return $this->_make_return_data(1,"注入下游请求url为空");
	    }
	    $obj_curl = new np_http_curl_class();
	   $arr_header = array("Content-Type: application/json");
	   $request_ret = $obj_curl->do_request('post',$cdn_send_mode_url,$c2_info['nns_content'],$arr_header,30);

            nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, ';收到来自CDN的内容:' . $request_ret."发送内容:{$c2_info['nns_content']}", $this->str_sp_id);
	    $curl_info = $obj_curl->curl_getinfo();
	    if($curl_info['http_code'] != '200')
	    {
	        $c2_info['nns_result'] = '[' . 1 . ']';
	        nl_c2_log::add($this->obj_dc, $c2_info);
	        nl_c2_task::edit_cdn_fail_time_v2($this->obj_dc,$c2_info['nns_task_id']);
	        return $this->_make_return_data(1,"CMS应答非200状态，[HTTP状态码]:{$curl_info['http_code']}[HTTP错误]".$obj_curl->curl_error());
	    }
	    $request_ret = json_decode($request_ret,true);
	    if((isset($request_ret['code']) && $request_ret['code'] == '0'))
	    {
	        $c2_info['nns_result'] = '[0]';
	        $c2_info['nns_status'] = '0';
	        $c2_info['nns_desc'] = $request_ret['msg'];
	        nl_c2_task::edit($this->obj_dc, array('nns_status'=>'0'), $c2_info['nns_task_id']);
	    }
	    else
	    {
	        $c2_info['nns_result'] = '[1]';
	        $c2_info['nns_status'] = '-1';
            $c2_info['nns_desc'] = $request_ret['msg'];
	        nl_c2_task::edit_cdn_fail_time_v2($this->obj_dc,$c2_info['nns_task_id']);
	    }
	   $c2_info['nns_content']='';
	    nl_c2_log::add($this->obj_dc, $c2_info);
        if((isset($request_ret['code']) && $request_ret['code'] == '0'))
        {
            $queue_task_model = new queue_task_model();
            $queue_task_model->q_report_task($c2_info['nns_task_id']);
            unset($queue_task_model);
            $queue_task_model=null;
         }
         return $this->_make_return_data(0,"OK".var_export($request_ret,true));
	}
    /**
     * 思华-转换成Java的时间格式
     * @author <feijian.gao@starcor.com>
     * @date    2017年9月1日14:57:55
     * @return false|mixed|string
     */
	private function get_java_datetime()
    {
        $time = time();
        $date = date('c',time());
        $date_zone_replace = date('O',time());
        $date_zone_search = date('P',time());
        $date = str_replace($date_zone_search,$date_zone_replace,$date);
        $date = str_replace("+",". ",$date);
        return $date;
    }

}
