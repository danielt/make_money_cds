<?php
/**
 * starcor CDN标准规范ADI CDN注入
 * @author liangpan
 */
class c2_task_model_v2 extends nn_public_model
{
	/**
	 * 组装主媒资信息
	 * @param unknown $arr_info 主媒资基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 */
	private function make_vod_xml($arr_info,$str_Action,$arr_info_ex=null)
	{
	    $arr_info['nns_kind'] = trim(str_replace(';;', ';',str_replace(';;', ';',str_replace('/', ';', $arr_info['nns_kind']))));
	    $arr_info['nns_kind'] = strlen($arr_info['nns_kind']) >0 ? $arr_info['nns_kind'].';' : '';
	    $str_xml = '<Object ElementType="Series" ID="'.$arr_info['nns_cdn_video_guid'].'" Action="'.$str_Action.'" Code="'.$arr_info['nns_cdn_video_guid'].'">';
	    $str_xml.=     '<Property Name="Name"><![CDATA['.$arr_info['nns_name'].']]></Property>';
	    $str_xml.=     '<Property Name="OrderNumber" />';
	    $str_xml.=     '<Property Name="OriginalName"><![CDATA['.$arr_info['nns_name'].']]></Property>';
	    $str_xml.=     '<Property Name="SortName"><![CDATA['.$arr_info['nns_pinyin'].']]></Property>';
	    $str_xml.=     '<Property Name="SearchName"><![CDATA['.$arr_info['nns_pinyin'].']]></Property>';
	    $str_xml.=     '<Property Name="OrgAirDate" />';
	    $str_xml.=     '<Property Name="LicensingWindowStart">20161212141408</Property>';
	    $str_xml.=     '<Property Name="LicensingWindowEnd">20261212141408</Property>';
	    $str_xml.=     '<Property Name="DisplayAsNew">7</Property>';
	    $str_xml.=     '<Property Name="DisplayAsLastChance" />';
	    $str_xml.=     '<Property Name="Macrovision">1</Property>';
	    $str_xml.=     '<Property Name="Price">0.00</Property>';
	    $str_xml.=     '<Property Name="VolumnCount">'.$arr_info['nns_all_index'].'</Property>';
	    $str_xml.=     '<Property Name="Status">0</Property>';
	    $str_xml.=     '<Property Name="Description"><![CDATA['.$arr_info['nns_summary'].']]></Property>';
	    $str_xml.=     '<Property Name="Type">电视剧</Property>';
	    $str_xml.=     '<Property Name="Keywords" />';
	    $str_xml.=     '<Property Name="Tags">'.$arr_info['nns_kind'].'</Property>';
	    $str_xml.=     '<Property Name="Reserve1" />';
	    $str_xml.=     '<Property Name="Reserve2" />';
	    $str_xml.=     '<Property Name="Reserve3" />';
	    $str_xml.=     '<Property Name="Reserve4" />';
	    $str_xml.=     '<Property Name="Reserve5" />';
	    $str_xml.=     '<Property Name="Result" />';
	    $str_xml.=     '<Property Name="ErrorDescription" />';
	    $str_xml.= '</Object>';
	    return $str_xml;
	}
	
	/**
	 * 组装片源信息
	 * @param unknown $arr_info 片源基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 * @var string FileURL
	 *     说明： 媒体文件 URL ftp://username:password@ip:port/...  标准 FTP协议 或者 http:// ip:port/... 
	 *     注释： 对托管内容注入方式下填写，支持 FTP 和 HTTP 协议获取内容 对非托管内容预注入方式下 则 为 空 ， 填 写ServiceURL 属性 当FileURL和ServiceURL均有值时，
	 *	               首选采用该属性值去获取内容。注入 HLS 内容时，如果属性值为 m3u8 文件结尾，CDN 获取内容时，获取整个 m3u8 的上级目录
	 * @var string ServiceURL
	 *     说明： 内容服务 URL
	 *     注释： 需要指定 CDN 服务 URL 的内容注入/预注入内容接入或实时回源接入内容删除时填写。为 CDN 提供服务的内容服务调用 URL，当 ServiceURL 有值时，该URL 前缀信息应和 CDN 服务配置
     *           表中对应域内容提供 商 的PlaybackURLprefix 保持一致，用户需要基于本URL 访问注入内容
     * @var string CPContentID
	 *     说明： CP 对于媒体文件的标识 
	 * @var string SourceDRMType 
	 *     说明： 0: No DRM 1: BES DRM
	 * @var string DestDRMType  
	 *     说明： 0: No DRM 1: BES DRM
	 * @var string AudioType   
	 *     说明： 0: 其他 1: Monaural 单声道 2: Stereo 多声道 3: Two-nation monaural 双单声道 4: Two-nation stereo 双多声道  5: AC3(5:1 channel) AC3声道
	 * @var string ScreenFormat    
	 *     说明： 0: 4x3   1: 16x9(Wide)
	 * @var string ClosedCaptioning     
	 *     说明： 字幕标志 0:无字幕 1:有字幕
	 * @var string Duration      
	 *     说明： 播放时长 HHMISSFF （时分秒帧）
	 * @var string FileSize       
	 *     说明： 文件大小，单位为 Byte
	 * @var string BitRateType        
	 *     说明： 码流 1: 400k 2: 700K 3:  1.3M 4: 2M 5: 2.5M 6:  8M 7: 10M  8：15M 9：20M 10：30M 
	 * @var string VideoType         
	 *     说明： 编码格式： 1. H.264 2. MPEG4 3. AVS 4. MPEG2 5. MP3 6. WMV 7. H.265 
	 * @var string AudioEncodingType          
	 *     说明： 编码格式： 1. MP2 2. AAC 3. AMR 4. Mp3 
	 * @var string Resolution           
	 *     说明： 分辨率类型 1. QCIF 2. QVGA 3. 2/3 D1 4. 3/4 D1 5. D1 6. 720P 7. 1080i 8. 1080P 9. 2k 11. 4k 13. 8k 
	 * @var string VideoProfile           
	 *     说明： 1. Simple 2. Advanced Simple 3. Baseline 4. Main 5. High 6. JiZhun 
	 * @var string SystemLayer           
	 *     说明： 1. TS 2. 3GP  
	 * @var string Domain            
	 *     说明： 发布到融合 CDN 后的服务域和服务协议 
	 *     注释： 包含 ServiceDomain 和ProtocolType 两层含义，为 4 字节整数，其中最高三 个 字 节 为ServiceDomain，表示预分发网络域，最低 3 位取值如下： 
	 *               0x000001-IPTV 网络 
	 *               0x000002-互联网网络 
	 *               0x000004-移动网络 
	 *               0x000008~0x000080-预留 
	 *            最 低 一 个 字 节 为ProtocolType，表示服务协议，最低 4 位取值如下：
	 *               0x01-IPTV TS RTSP， 
	 *               0x02-HPD 
	 *               0x04-ISMA RTSP 
	 *               0x08-HLS。 
	 *            如果 Domain 为空，则采用该 CMSID 缺省分发域，缺省分发域可配置
	 * @var string Hotdegree             
	 *     说明： 发布到融合 CDN 时使用热度表示
	 *     注释： 0（普通），1（高） 缺省为 0，该字段值越大，表示优先级越高。CDN 应根据优先级安排处理顺序
	 * @var string deliverTime              
	 *     说明： 内容分发时间
	 *     注释： 北京时间，采用十四位的字 符 串 格 式YYYYMMDDHHMMSS，例如： 20040101123000 ， 代 表 2004 年 1 月 1 日 12点 30 分 00 秒。说明 如果不携带该字段，表示立即分发
	 */
	private function make_vod_media_xml($arr_info,$str_Action,$arr_info_ex=null,$arr_index_info,$arr_video_info)
	{
	    $arr_video_info['nns_show_time'] = strlen($arr_video_info['nns_show_time']) >0  ?  date("Y",strtotime($arr_video_info['nns_show_time'])) : date("Y");
	    $arr_video_info['nns_kind'] = trim(str_replace(';;', ';',str_replace(';;', ';',str_replace('/', ';', $arr_video_info['nns_kind']))));
	    $arr_video_info['nns_kind'] = strlen($arr_video_info['nns_kind']) >0 ? $arr_video_info['nns_kind'].';' : '';
	    $SeriesFlag = $arr_video_info['nns_all_index'] > 0 ?  1 : 0 ;
	    if($arr_video_info['nns_all_index'] > 1)
	    {
	        $arr_index_info['nns_index'] ++;
	        $SeriesFlag =  1;
	        $Type =  '电视剧';
	        $name = " 第{$arr_index_info['nns_index']}集";
	    }
	    else
	    {
	        $SeriesFlag =  0;
	        $Type =  '电影';
	        $name = '';
	    }
	    $str_xml = '<Object ElementType="Program" ID="'.$arr_index_info['nns_cdn_index_guid'].'" Action="'.$str_Action.'" Code="'.$arr_index_info['nns_cdn_index_guid'].'">';
	    $str_xml.=     '<Property Name="Name"><![CDATA['.$arr_video_info['nns_name'].$name.']]></Property>';
	    $str_xml.=     '<Property Name="OriginalName"><![CDATA['.$arr_video_info['nns_name'].$name.']]></Property>';
	    $str_xml.=     '<Property Name="SortName"><![CDATA['.$arr_video_info['nns_pinyin'].']]></Property>';
	    $str_xml.=     '<Property Name="SearchName"><![CDATA['.$arr_video_info['nns_pinyin'].']]></Property>';
	    $str_xml.=     '<Property Name="Genre"><![CDATA['.$arr_video_info['nns_kind'].']]></Property>';
	    $str_xml.=     '<Property Name="ActorDisplay"><![CDATA['.$arr_video_info['nns_actor'].']]></Property>';
	    $str_xml.=     '<Property Name="WriterDisplay"><![CDATA['.$arr_video_info['nns_screenwriter'].']]></Property>';
	    $str_xml.=     '<Property Name="OriginalCountry"><![CDATA['.$arr_video_info['nns_area'].']]></Property>';
	    $str_xml.=     '<Property Name="Language"><![CDATA['.$arr_video_info['nns_language'].']]></Property>';
	    $str_xml.=     '<Property Name="ReleaseYear"><![CDATA['.$arr_video_info['nns_show_time'].']]></Property>';
	    $str_xml.=     '<Property Name="OrgAirDate" />';
	    $str_xml.=     '<Property Name="LicensingWindowStart">20170316112435</Property>';
	    $str_xml.=     '<Property Name="LicensingWindowEnd">20270316112435</Property>';
	    $str_xml.=     '<Property Name="DisplayAsNew"></Property>';
	    $str_xml.=     '<Property Name="DisplayAsLastChance" />';
	    $str_xml.=     '<Property Name="Macrovision">1</Property>';
	    $str_xml.=     '<Property Name="Description"><![CDATA['.$arr_video_info['nns_summary'].']]></Property>';
	    $str_xml.=     '<Property Name="PriceTaxIn">0</Property>';
	    $str_xml.=     '<Property Name="Status">0</Property>';
	    $str_xml.=     '<Property Name="SourceType" />';
	    $str_xml.=     '<Property Name="SeriesFlag">'.$SeriesFlag.'</Property>';
	    $str_xml.=     '<Property Name="Type">电影</Property>';
	    $str_xml.=     '<Property Name="Keywords" />';
	    $str_xml.=     '<Property Name="Tags" />';
	    $str_xml.=     '<Property Name="Reserve1" />';
	    $str_xml.=     '<Property Name="Reserve2" />';
	    $str_xml.=     '<Property Name="Reserve3" />';
	    $str_xml.=     '<Property Name="Reserve4" />';
	    $str_xml.=     '<Property Name="Reserve5" />';
	    $str_xml.=     '<Property Name="EX_Tag" />';
	    $str_xml.=     '<Property Name="OrderNumber" />';
	    $str_xml.= '</Object>';
	    $str_xml.= '<Object ElementType="Movie" ID="'.$arr_info['nns_cdn_media_guid'].'" Action="'.$str_Action.'" Code="'.$arr_info['nns_cdn_media_guid'].'">';
	    $str_xml.=	   '<Property Name="Type">1</Property>';
	    $str_xml.=	   '<Property Name="FileURL"><![CDATA['.$arr_info['nns_url'].']]></Property>';
	    $str_xml.=	   '<Property Name="SourceDRMType">0</Property>';
	    $str_xml.=	   '<Property Name="DestDRMType">0</Property>';
	    $str_xml.=	   '<Property Name="AudioType">0</Property>';
	    $str_xml.=     '<Property Name="ScreenFormat">0</Property>';
	    $str_xml.=     '<Property Name="ClosedCaptioning">0</Property>';
	    $str_xml.='</Object>';
		return $str_xml;
	}
	
	/**
	 * 点播主媒资CDN
	 * @param unknown $movie_id
	 */
	public function do_video()
	{
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_video_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $video_id = $this->arr_params['bk_c2_task_info']['nns_ref_id'];
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	     
	    $result_video = nl_vod::get_video_info_by_id($this->obj_dc, $video_id);
	    if($result_video['ret'] !=0)
	    {
	        return $result_video;
	    }
	    if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['bk_c2_task_info']['nns_id']}'", $this->obj_dc->db());
	        return $result_video;
	    }
	    $result_video = $result_video['data_info'][0];
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $video_id,$result_video);
	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        return $result_video_import_info;
	    }
	    $result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
	    if($result_video['nns_all_index'] <1)
	    {
	        return $this->_make_return_data(0,'不是电视剧，不注入video');
	    }
	    $action = ($this->arr_params['bk_c2_task_info']['nns_action'] !='destroy') ? ($this->arr_params['bk_c2_task_info']['nns_action'] == 'update') ? 'UPDATE' : 'REGIST' : 'DELETE';
	    $result_picture = $this->self_make_image($result_video, 'Series', $result_video['nns_cdn_video_guid'], $action);
	    $xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
	    $xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
	    $xml_str .= 	'<Objects>';
	    $xml_str .=     $this->make_vod_xml($result_video,$action);
	    if(isset($result_picture['picture']) && strlen($result_picture['picture']) >0)
	    {
	        $xml_str .= $result_picture['picture'];
	    }
	    $xml_str .= 	'</Objects>';
	    if(isset($result_picture['mapping']) && strlen($result_picture['mapping']) >0)
	    {
	        $xml_str .= 	'<Mappings>';
	        $xml_str .= $result_picture['mapping'];
	        $xml_str .= 	'</Mappings>';
	    }
	    $xml_str .= '</ADI>';
	    $file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'vod_video' . '_' . $action . '_' .  rand(100, 999) . '.xml';
		$c2_info = array(
	        'nns_task_type'=>'Series',
	        'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	        'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
	        'nns_action'=>	$action,
	        'nns_url' => $file_name,
	        'nns_content' => $xml_str,
	        'nns_desc' => 'Series,'.$action,
	    );
	    return $this->execute_fufu_c2($c2_info);
	}
	
	
	/**
	 * 点播分集CDN
	 * @param unknown $movie_id
	 */
	public function do_index()
	{
	    return $this->_make_return_data(0,'分集不单独注入');
	}
	
	
	/**
	 * 片源CDN
	 * @param unknown $movie_id
	 */
	public function do_media()
	{
		if(!isset($this->arr_params['nns_media_id']) || empty($this->arr_params['nns_media_id']))
		{
			return $this->_make_return_data(1,'点播传入参数无nns_media_id字段,参数为:'.var_export($this->arr_params));
		}
		$movie_id = $this->arr_params['nns_media_id'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
		$result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $movie_id);
		if($result_media['ret'] !=0)
		{
			return $result_media;
		}
		if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
		{
			nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
			return $result_media;
		}
		$result_media = $result_media['data_info'];
		$result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $movie_id,$result_media);
// 		if((int)$this->arr_sp_config['disabled_clip'] !== 2 && (int)$this->arr_sp_config['disabled_clip'] !== 3)
// 		{
// 		    $result_media['nns_url'] = ltrim((trim($this->arr_params['nns_file_path'])),'/');
// 		    if(isset($this->arr_sp_config['media_ftp']) && !empty($this->arr_sp_config['media_ftp']))
// 		    {
// 		        $this->arr_sp_config['media_ftp'] = rtrim((trim($this->arr_sp_config['media_ftp'])),'/');
// 		        if (stripos($result_media['nns_url'], 'http://') === FALSE && stripos($result_media['nns_url'], 'ftp://') === FALSE)
// 		        {
// 		            $result_media['nns_url'] = $this->arr_sp_config['media_ftp'].'/'.$result_media['nns_url'];
// 		        }
// 		    }
// 		}
// 		else
// 		{
// 		    $result_media['nns_url'] = ltrim((trim($result_media['nns_url'])),'/');
// 		    if(isset($this->arr_sp_config['media_ftp']) && !empty($this->arr_sp_config['media_ftp']))
// 		    {
// 		        $this->arr_sp_config['media_ftp'] = rtrim((trim($this->arr_sp_config['media_ftp'])),'/');
// 		        if (stripos($result_media['nns_url'], 'http://') === FALSE && stripos($result_media['nns_url'], 'ftp://') === FALSE)
// 		        {
// 		        	 $result_media['nns_url'] = $this->arr_sp_config['media_ftp'].'/'.$result_media['nns_url'];
// 		        }
// 		    }
// 		}
		if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
		{
			return $result_media_import_info;
		}
		$result_media['nns_cdn_media_guid'] = $result_media_import_info['data_info'];
		
		$result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $result_media['nns_vod_index_id']);
		if($result_index['ret'] !=0)
		{
		    return $result_index;
		}
		if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
		{
		    nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
		    return $result_index;
		}
		$result_index = $result_index['data_info'][0];
		$result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $result_media['nns_vod_index_id'],$result_index);
		if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
		{
		    return $result_index_import_info;
		}
		$result_index['nns_cdn_index_guid'] = $result_index_import_info['data_info'];
		 
		 
		$result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_media['nns_vod_id']);
		if($result_video['ret'] !=0)
		{
		    return $result_video;
		}
		if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
		{
		    nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
		    return $result_video;
		}
		$result_video = $result_video['data_info'][0];
		$result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_index['nns_vod_id'],$result_video);
		if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
		{
		    return $result_video_import_info;
		}
		$result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
		$action = ($this->arr_params['bk_c2_task_info']['nns_action'] !='destroy') ? ($this->arr_params['bk_c2_task_info']['nns_action'] == 'update') ? 'UPDATE' : 'REGIST' : 'DELETE';
		$flag = ($result_video['nns_all_index'] > 1) ? false : true;
// 		$action = ($result_media['nns_deleted'] !=1) ? ($result_media['nns_modify_time'] > $result_media['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
		$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
		$xml_str .= 	'<Objects>';
		$xml_str .=       $this->make_vod_media_xml($result_media,$action,null,$result_index,$result_video);
		if($flag)
		{
		    $result_picture = $this->self_make_image($result_video, 'Program', $result_index['nns_cdn_index_guid'], $action);
		    if(isset($result_picture['picture']) && strlen($result_picture['picture']) >0)
		    {
		        $xml_str .= $result_picture['picture'];
		    }
		}
		$xml_str .= 	'</Objects>';
		$xml_str .= 	'<Mappings>';
		$xml_str .=     $this->make_mapping('Program','Movie',$result_index['nns_cdn_index_guid'],$result_media['nns_cdn_media_guid'],$action);
		if($flag && isset($result_picture['mapping']) && strlen($result_picture['mapping']) >0)
		{
    	        $xml_str .= $result_picture['mapping'];
		}
// 		if($action != 'DELETE' && $flag)
// 		{
// 		      $xml_str .=     $this->make_mapping('Series','Program',$result_video['nns_cdn_video_guid'],$result_index['nns_cdn_index_guid'],$action);
// 		}
		$xml_str .= 	'</Mappings>';
		$xml_str .= '</ADI>';
		
		$file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'vod_media' . '_' . $action . '_' .  rand(100, 999) . '.xml';
		$c2_info = array(
				'nns_task_type'=>'Movie',
				'nns_task_id'=> isset($this->arr_params['nns_task_id'])?$this->arr_params['nns_task_id']:null,
				'nns_task_name'=> isset($this->arr_params['nns_task_name'])?$this->arr_params['nns_task_name']:null,
				'nns_action'=>	$action,
				'nns_url' => $file_name,
				'nns_content' => $xml_str,
				'nns_desc' => 'Movie,'.$action,
		);
		return $this->execute_fufu_c2($c2_info);
	}
	
	
	/**
	 * C2
	 * @param unknown $c2_info
	 */
	public function execute_fufu_c2($c2_info)
	{
	    $c2_info['nns_type'] = 'std';
	    $c2_info['nns_org_id'] = $this->str_sp_id;
	    $c2_id = np_guid_rand();
	    $c2_info['nns_id'] = $c2_id;
	    //加载soap客户端文件
	    $file_soap_client_file = dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv/' . $this->str_sp_id . '/client.php';
	    if(!file_exists($file_soap_client_file))
	    {
	        return $this->_make_return_data(1,'加载soap客户端文件不存在,文件路径:['.$file_soap_client_file.']');
	    }
	    global $g_bk_web_url;
	    $bk_web_url = (isset($g_bk_web_url) && strlen($g_bk_web_url)>0) ? $g_bk_web_url : '';
	    unset($g_bk_web_url);
	    $bk_web_url = ltrim(ltrim(trim($bk_web_url),'/'),'\\');
	    if(strlen($bk_web_url) <1)
	    {
	        return $this->_make_return_data(1,'播控后台系统配置管理里面后台基本地址未配置:['.$bk_web_url.']');
	    }
	    include_once $file_soap_client_file;
	    //检查时间范围是否有效执行
	    if ($this->exists_c2_commond($c2_info))
	    {
	        return $this->_make_return_data(1,'该task在一小时内执行过或则task查询无数据,task信息:'.var_export($c2_info,true));
	    }
	    $sub_path = date('Y-m-d');
	    $file = $c2_info['nns_url'];
	    $c2_info['nns_url'] = $sub_path . '/' . $file;
	    //生成soap xml文件 路径
	    $file_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/mgtv/fufu/inject_xml/' . $sub_path . '/';
	    if (!is_dir($file_path))
	    {
	        mkdir($file_path, 0777, true);
	    }
	    $file_path .= $file;
	    file_put_contents($file_path, $c2_info['nns_content']);
	    $CmdFileURL = trim(rtrim($this->arr_sp_config['xml_ftp'],'/')) . '/' . trim(ltrim($c2_info['nns_url'],'/'));
	    try
	    {
	        $result = null;
	        $obj_soap = new client();
	        $result = $obj_soap->ContentDeployReq($c2_info['nns_id'], $CmdFileURL,$bk_web_url,$c2_info['nns_task_type']);
	        $result = isset($result->Result) ? $result->Result : isset($result->ResultCode) ? $result->ResultCode : $result->Result;
	        $result = ($result === null) ? 0 : $result;
	        $c2_info['nns_result'] = '[' . $result . ']';
	        nl_c2_log::add($this->obj_dc, $c2_info);
	        $str_ret = ($result != 0) ? 1 : 0;
	        return $this->_make_return_data($str_ret,var_export($result,true));
	    } 
	    catch (Exception $e)
	    {
	        $c2_info['nns_result'] = '[-1]';
	        $c2_info['nns_notify_result'] = -1;
	        nl_c2_log::add($this->obj_dc, $c2_info);
	        return $this->_make_return_data(1,"soap客户端类文件获取异常:[{$file_soap_client_file}]");
	    }
	}
	
	
	/**
	 * 组装img
	 * @param unknown $arr_video
	 * @param unknown $type
	 */
	public function self_make_image($arr_video,$parent_type,$parent_id,$action)
	{
	    $arr_map = null;
	    $str_picture_obj = $str_picture_mapping = '';
	    $img_url = isset($this->arr_sp_config['img_ftp']) ? $this->arr_sp_config['img_ftp'] : '';
	    $img_url = rtrim(rtrim(trim($img_url),'/'),'\\');
	    switch ($parent_type)
	    {
	        case 'Series':
	            $arr_map = $this->arr_vod_video_img;
	            break;
	        case 'Program':
	            $arr_map = $this->arr_vod_video_img;
	            break;
	        case 'Channel':
	            $arr_map = $this->arr_channel_img;
	            break;
	        case 'ScheduleRecord':
	            $arr_map = $this->arr_playbill_img;
	            break;
	    }
	    if(!is_array($arr_map) || empty($arr_map))
	    {
	        return null;
	    }
	    foreach ($arr_map as $key=>$val)
	    {
	        if(isset($arr_video[$key]) && strlen($arr_video[$key]) >0)
	        {
	            $id = md5($key.$arr_video[$key]);
	            $picture = array(
	                'FileURL'=>$img_url.'/'.ltrim(ltrim(trim($arr_video[$key]),'/'),'\\'),
	                'Type'=>$val,
	                'Description'=>'',
	            );
	            $str_picture_obj .= $this->make_object('Picture', $id,$action,$picture);
	            if(isset($this->arr_sp_config['c2_import_cdn_mapping_model']) && $this->arr_sp_config['c2_import_cdn_mapping_model'] == '1')
	            {
	                $str_picture_mapping .= $this->make_mapping($parent_type, 'Picture', $parent_id, $id, $action);
	            }
	            else
	            {
	                $str_picture_mapping .= $this->make_mapping('Picture', $parent_type, $id, $parent_id, $action);
	            }
	        }
	    }
	    return array(
	        'picture'=>$str_picture_obj,
	        'mapping'=>$str_picture_mapping,
	    );
	}
}
