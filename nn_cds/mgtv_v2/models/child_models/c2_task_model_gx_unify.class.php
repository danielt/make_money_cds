<?php
/**
 * 趣享运营商
 * @author LZ
 *
 */
class c2_task_model_v2 extends nn_public_model
{
	/**
	 * 片源注入
	 * package+title信息+movie信息
	 */
	public function do_media()
	{
		$c2_task_info = $this->arr_params['bk_c2_task_info'];
		if(empty($c2_task_info['nns_ref_id']) || empty($c2_task_info['nns_src_id']))
		{
			return $this->_make_return_data(1,'点播传入缺失参数,参数为:'.var_export($this->arr_params,true));
		} 

		$package_info = array();//package层信息
		$title_info = array();//title层信息
		$movie_info = array();//movie层信息
		$poster_info = array();//海报信息
		//根据media_id获取media信息
		$result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $c2_task_info['nns_ref_id']);
		if($result_media['ret'] !=0)
		{
			return $result_media;
		}
		$media_info = $result_media['data_info'];
		$result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $c2_task_info['nns_ref_id'],$media_info,$this->arr_sp_config);
		if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
		{
			return $result_media_import_info;
		}
		//根据index_id获取分集信息
		$result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $c2_task_info['nns_src_id']);
		if($result_index['ret'] != 0)
		{
		    return $result_index;
		}
		$index_info = $result_index['data_info'][0];
		$result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $index_info['nns_id'],$index_info,$this->arr_sp_config);
		if($result_index_import_info['ret'] != 0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) < 1)
		{
			return $result_index_import_info;
		}
		//根据vod_id获取主媒资信息
		$result_video = nl_vod::get_video_info_by_id($this->obj_dc, $index_info['nns_vod_id']);
		if($result_video['ret'] != 0)
		{
		    return $result_video;
		}
		$video_info = $result_video['data_info'][0];
		$result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $video_info['nns_id'],$video_info,$this->arr_sp_config);
		if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) < 1)
		{
			return $result_video_import_info;
		}
		
		$this->str_cp_id = $video_info['nns_cp_id'];
        $title_info['id'] = $result_index_import_info['data_info'];

		if($video_info['nns_all_index'] > 1)//连续剧的单集片源
		{
            $title_info['asset_type'] = 1;
            $title_info['video_id'] = $result_video_import_info['data_info'];
            $title_info['summary'] = $index_info['nns_summary'];
            $title_info['director'] = $index_info['nns_director'];
            $title_info['actor'] = $index_info['nns_actor'];
            $title_info['index'] = $index_info['nns_index'] + 1;
            $title_info['name'] = $video_info['nns_name']."_".$title_info['index'];
            $title_info['season'] = 0;
		}
		else //电影片源
		{
			$title_info['name'] = $video_info['nns_name'];
			$title_info['video_id'] = '';
			$title_info['summary'] = $video_info['nns_summary'];
			$title_info['director'] = $video_info['nns_director'];
			$title_info['actor'] = $video_info['nns_actor'];
			$title_info['index'] = 1;
			$title_info['season'] = 1;
            $title_info['asset_type'] = 0;
		}
        if(empty($video_info['nns_image2']) && !empty($video_info['nns_image_v'])) //竖图
        {
            $video_info['nns_image2'] = $video_info['nns_image_v'];
        }
        if(!empty($video_info['nns_image2']) && $title_info['index'] <= 1)
        {
            $img_save_name = str_replace('.JPG','.jpg',$video_info['nns_image2']);
            $file_img = dirname(dirname(dirname(dirname(__FILE__)))).'/data/downimg/'.$img_save_name;
            if(file_exists($file_img))
            {
                $img_name = end(explode("/",$img_save_name));
                $str_length = strlen($video_info['nns_cp_id']) + 1;
                $lest_num = 32 - $str_length;
                $poster_id = $video_info['nns_cp_id'] . 'V' . str_pad($video_info['nns_integer_id'], $lest_num, "0", STR_PAD_LEFT);
                $poster_info[] = array(
                    'id' => $poster_id,
                    'path' => $img_name,
                    'provider' => $video_info['nns_cp_id'],
                    'provider_id' => $this->arr_sp_config['import_csp_id'],
                    'name' => $video_info['nns_name']."_竖海报",
                    'mode' => 0
                );
            }
        }
        if(!empty($video_info['nns_image_h']) && $title_info['index'] <= 1)//横图
        {
            $img_save_name = str_replace('.JPG','.jpg',$video_info['nns_image_h']);
            $file_img = dirname(dirname(dirname(dirname(__FILE__)))).'/data/downimg/'.$img_save_name;
            if(file_exists($file_img))
            {
                $img_name = end(explode("/",$img_save_name));
                $str_length = strlen($video_info['nns_cp_id']) + 1;
                $lest_num = 32 - $str_length;
                $poster_id = $video_info['nns_cp_id'] . 'H' . str_pad($video_info['nns_integer_id'], $lest_num, "0", STR_PAD_LEFT);
                $poster_info[] = array(
                    'id' => $poster_id,
                    'path' => $img_name,
                    'provider' => $video_info['nns_cp_id'],
                    'provider_id' => $this->arr_sp_config['import_csp_id'],
                    'name' => $video_info['nns_name']."_横海报",
                    'mode' => 1,
                );
            }
        }
		//package信息
		$package_info['id'] = $result_video_import_info['data_info'];
		$package_info['name'] = $video_info['nns_name'];
		$package_info['provider'] = $video_info['nns_cp_id'];
        $package_info['provider_id'] = $this->arr_sp_config['import_csp_id'];
		$package_info['summary'] = $package_info['name'];
        $package_info['director'] = $video_info['nns_director'];
        $package_info['actor'] = $video_info['nns_actor'];
        $package_info['kind'] = $video_info['nns_kind'];
        $view_type = get_config_v2('g_video_main_type');
        $package_info['view'] = $view_type[$video_info['nns_view_type']];
        $package_info['keyword'] = $video_info['nns_keyword'];
        $package_info['area'] = empty($video_info['nns_area']) ? '' : $video_info['nns_area'];
        $package_info['language'] = $video_info['nns_language'];
        $package_info['all_index'] = $video_info['nns_all_index'];



		$xml = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml .= '<ADI>';
		if ($c2_task_info['nns_action'] != 'destroy')
		{
			$action = '';
			$title_info['provider'] = $package_info['provider'];
            $title_info['provider_id'] = $this->arr_sp_config['import_csp_id'];
			$title_info['language'] = $video_info['nns_language'];
			$title_info['area'] = empty($video_info['nns_area']) ? '' : $video_info['nns_area'];
			
			$movie_info['id'] = $result_media_import_info['data_info'];
			$movie_info['provider'] = $package_info['provider'];
            $movie_info['provider_id'] = $this->arr_sp_config['import_csp_id'];
			$movie_info['name'] = $title_info['name'];
			$movie_info['file_bite'] = empty($media_info['nns_kbps']) ? 1 : $media_info['nns_kbps'];
			$movie_info['time_len'] = $media_info['nns_file_time_len'];

            $title_info['view'] = $package_info['view'];
            $title_info['time_len'] = $media_info['nns_file_time_len'];
            $title_info['all_index'] = $video_info['nns_all_index'];
            $title_info['keyword'] = $video_info['nns_keyword'];

			if(!empty($c2_task_info['nns_file_path']))
			{
				$movie_info['file_path'] = $c2_task_info['nns_file_path'];
			}
			else 
			{
				$movie_info['file_path'] = $media_info['nns_url'];
			}
			switch ($media_info['nns_mode'])
            {
                case 'low':
                    $movie_info['file_name'] = '低清';
                    break;
                case 'std':
                    $movie_info['file_name'] = '标清';
                    break;
                case 'hd':
                    $movie_info['file_name'] = '高清';
                    break;
                case 'sd':
                    $movie_info['file_name'] = '超清';
                    break;
                case '4':
                    $movie_info['file_name'] = '4K';
                    break;
            }
            $movie_info['file_mode'] = strtoupper($media_info['nns_mode']);
            $movie_info['file_size'] = empty($media_info['nns_file_size']) ? 1 : $media_info['nns_file_size'];
			$xml .= $this->__build_package($package_info,$action);
			$xml .= '<Asset>';
			$xml .= $this->__build_title($title_info);
			$xml .= $this->__build_movie($movie_info);
			if(!empty($poster_info))
            {
                foreach ($poster_info as $post)
                {
                    $xml .= $this->__build_poster($post);
                }
            }

			$xml .= '</Asset>';
		}
		else
		{
			$action = 'DELETE';
			$xml .= $this->__build_package($package_info,$action);
		}
		
		$xml .= '</ADI>';
		$task_action = $c2_task_info['nns_action'] == 'destroy' ? 'DELETE' : 'REGIST';
		$file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'vod_media' . '_' . $task_action . '_' .  rand(100, 999) . '.xml';
		$c2_info = array(
				'nns_task_type'=>'Movie',
				'nns_task_id'=> isset($c2_task_info['nns_id']) ? $c2_task_info['nns_id'] : $this->arr_params['nns_task_id'],
				'nns_task_name'=> isset($c2_task_info['nns_name']) ? $c2_task_info['nns_name'] : $this->arr_params['nns_task_name'],
				'nns_action'=>	$task_action,
				'nns_url' => $file_name,
				'nns_content' => $xml,
				'nns_desc' => 'Movie,'.$task_action,
		);
		return $this->execute_c2_model($c2_info);
	}
	/**
	 * 组装package层信息
	 * @param array $params
	 * array(
	 * 	id=>注入ID,
	 *  name=>名称
	 *  provider=>提供商
	 *  summary=>简介
	 * )
	 * @param string $action 操作
	 */
	private function __build_package($params,$action = '')
	{
		$xml = '<Metadata>';
		$xml .= '<AMS Asset_Name="'.$params['name'].'" Provider="'.$params['provider'].'" Product="VOD" Version_Major="1" Version_Minor="0" Description="'.$params['summary'].'" Creation_Date="'.date('Y-m-d').'" Provider_ID="'.$params['provider_id'].'" Asset_ID="'.$params['id'].'" Asset_Class="package" Verb="'.$action.'" />';
        $xml .= '<App_Data App="MOD" Name="Metadata_Spec_Version" Value="CableLabsVOD1.1"/>';
		$xml .= '<App_Data App="MOD" Name="Classify" Value="' . $params['view'] . '"/>';
		$xml .= '<App_Data App="MOD" Name="Genre" Value="' . $params['kind'] . '"/>';
		$xml .= '<App_Data App="MOD" Name="Keyword" Value="' . $params['keyword'] . '"/>';
		$xml .= '<App_Data App="MOD" Name="Year" Value=""/>';
		$xml .= '<App_Data App="MOD" Name="Actors" Value="' . $params['actor'] . '"/>';
		$xml .= '<App_Data App="MOD" Name="Director" Value="' . $params['director'] . '"/>';
		$xml .= '<App_Data App="MOD" Name="Summary_Long" Value="' . $params['summary'] . '"/>';
		$xml .= '<App_Data App="MOD" Name="Country_Of_Origin" Value="' . $params['area'] . '"/>';
		$xml .= '<App_Data App="MOD" Name="Language" Value="' . $params['language'] . '"/>';
		$xml .= '<App_Data App="MOD" Name="Licensing_Window_Start" Value=""/>';
		$xml .= '<App_Data App="MOD" Name="Licensing_Window_End" Value=""/>';
		$xml .= '<App_Data App="MOD" Name="TotalNumber" Value="' . $params['all_index'] . '"/>';
		$xml .= '<App_Data App="MOD" Name="VodResourceType" Value="pc"/>';
		$xml .= '</Metadata>';
		return $xml;
	}
	/**
	 * 组装TITLE层信息
	 * @param $params
	 * array(
	 * 	id=>注入ID,
	 * 	video_id=>主媒资注入ID,//当为子集时，才有此值
	 *  name=>名称,
	 *  pinyin=>拼音首字母,
	 *  summary=>简介,
	 *  provider=>内容提供商ID,
	 *  language=>语言,
	 *  director=>导演,//多个以逗号分隔
	 *  actor=>演员,//多个以逗号分隔
	 *  index=>集数,//子集为子集数，连续剧剧头为总集数
	 *  area=>地区
	 * )
	 * @param $series_type 1-连续剧父集 0-电影 2子集
	 * @author zhiyong.luo
	 */
	private function __build_title($params)
	{
		if(empty($params))
		{
			return;
		}
		$xml = '<Metadata>';
        $xml .= '<AMS Asset_Class="title" Product="VOD" Version_Major="1" Version_Minor="0" Verb="" Asset_ID="' . $params['id'] . '" Asset_Name="' . $params['name'] . '" Creation_Date="' . date('Y-m-d') . '" Description="" Provider="' . $params['provider'] . '" Provider_ID="' . $params['provider_id'] . '"/>';
        $xml .= '<App_Data App="MOD" Name="Title" Value="' . $params['name'] . '"/>';
        $xml .= '<App_Data App="MOD" Name="Title_Sec" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="Title_Brief" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="AssetType" Value="'.$params['asset_type'].'"/>';
        $xml .= '<App_Data App="MOD" Name="Run_Time" Value="'.gmstrftime('%H:%M:%S',$params['time_len']).'"/>';
        $xml .= '<App_Data App="MOD" Name="Show_Type" Value="' . $params['view'] . '"/>';
        $xml .= '<App_Data App="MOD" Name="Genre" Value="' . $params['kind'] . '"/>';
        $xml .= '<App_Data App="MOD" Name="Keyword" Value="' . $params['keyword'] . '"/>';
        $xml .= '<App_Data App="MOD" Name="Season" Value="'.$params['season'].'"/>';
        $xml .= '<App_Data App="MOD" Name="TotalNumber" Value="' . $params['all_index'] . '"/>';
        $xml .= '<App_Data App="MOD" Name="Chapter" Value="' . $params['index'] . '"/>';
        $xml .= '<App_Data App="MOD" Name="Summary_Long" Value="' . str_replace(array("'","\"","&"),"’",$params['summary']) . '"/>';
        $xml .= '<App_Data App="MOD" Name="Summary_Long_Sec" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="Language" Value="' . $params['language'] . '"/>';
        $xml .= '<App_Data App="MOD" Name="Subtitle_Language" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="Director" Value="'.$params['director'].'"/>';
        $xml .= '<App_Data App="MOD" Name="Director_Sec" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="Actors" Value="'.$params['actor'].'"/>';
        $xml .= '<App_Data App="MOD" Name="Actors_Sec" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="Writers" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="Writers_Sec" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="Provider" Value="' . $params['provider'] . '"/>';
        $xml .= '<App_Data App="MOD" Name="Provider_Sec" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="Play_Date" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="Premiere_Date" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="Year" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="Country_Of_Origin" Value="' . $params['area'] . '"/>';
        $xml .= '<App_Data App="MOD" Name="Country_Of_Origin_Sec" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="Column" value=""/>';
        $xml .= '<App_Data App="MOD" Name="Audience" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="Classify" Value="' . $params['view'] . '"/>';
        $xml .= '<App_Data App="MOD" Name="Licensing_Window_Start" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="Licensing_Window_End" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="Rate" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="Suggested_Price" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="HighLight" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="Awards" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="Screen_Writer" Value=""/>';
		$xml .= '</Metadata>';
		return $xml;
	}
	/**
	 * 组装MOVIE层信息
	 * @param array $movie_params
	 * array(
	 * 	
	 * )
	 */
	private function __build_movie($movie_params)
	{
		if(empty($movie_params))
		{
			return;
		}
		$asset_id = $movie_params['id'];
		$xml = '<Asset>';
		$xml .= '<Metadata>';
		$xml .= '<AMS Asset_Name="'.$movie_params['name'].'" Provider="'.$movie_params['provider'].'" Product="VOD" Version_Major="1" Version_Minor="0" Description="'.$movie_params['name'].'" Creation_Date="'.date('Y-m-d').'" Provider_ID="'.$movie_params['provider_id'].'" Asset_ID="'.$asset_id.'"  Asset_Class="movie" />';
        $xml .= '<App_Data App="MOD" Name="Bit_Rate" Value="'.$movie_params['file_bite'].'"/>';
        $xml .= '<App_Data App="MOD" Name="Content_FileSize" Value="'.$movie_params['file_size'].'"/>';
        $xml .= '<App_Data App="MOD" Name="Run_Time" Value="'.gmstrftime('%H:%M:%S',$movie_params['time_len']).'"/>';
        $xml .= '<App_Data App="MOD" Name="FileFormat" Value="MPEG-TS"/>';
        $xml .= '<App_Data App="MOD" Name="CodeFormat" Value="AVC"/>';
        $xml .= '<App_Data App="MOD" Name="Format" Value="'.$movie_params['file_name'].'"/>';
        $xml .= '<App_Data App="MOD" Name="HD_Format" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="SD_Format" Value=""/>';
        $xml .= '<App_Data App="MOD" Name="Strean_Size" Value=""/>';
        $xml .= ' <App_Data App="MOD" Name="Asset_Tag" Value="STB"/>';
        $xml .= '<App_Data App="MOD" Name="Asset_Profile" Value="'.$movie_params['file_mode'].'"/>';
		$xml .= '</Metadata>';
		$xml .= '<Content Value="'.$movie_params['file_path'].'" />';
		$xml .= '</Asset>';
		return $xml;
	}
	/**
	 * 组装POSTER海报信息
	 * @param array $poster_params
	 * array(
	 * 	provider=>提供商ID,
	 * 	path=>海报地址
	 * )
	 * @return string
	 */
	private function __build_poster($poster_params)
	{
		if(empty($poster_params))
		{
			return;
		}
		$xml = '<Asset>';
		$xml .= '<Metadata>';
		$xml .= '<AMS Asset_Name="'.$poster_params['name'].'" Provider="'.$poster_params['provider'].'" Product="" Version_Major="1" Version_Minor="0" Description="" Creation_Date="'.date('Y-m-d').'" Provider_ID="'.$poster_params['provider_id'].'" Asset_ID="'.$poster_params['id'].'" Asset_Class="poster"/>';
        $xml .= '<App_Data App="MOD" Name="Ui_Style" Value="' . $poster_params['mode'] . '"/>';
        $xml .= '<App_Data App="MOD" Name="Title" Value="'.$poster_params['name'].'"/>';
        $xml .= '<App_Data App="MOD" Name="Summary" Value=""/>';
		$xml .= '</Metadata>';
		$xml .= '<Content Value="'.$poster_params['path'].'" />';
		$xml .= '</Asset>';
		return $xml;
	}
	/**
	 * 趣享华为C2注入
	 * @param $c2_info array
	 * @return Ambigous <multitype:number , multitype:number string >
	 */
	public function execute_c2_model($c2_info)
	{
		$c2_info['nns_type'] = 'std';
		$c2_info['nns_org_id'] = $this->str_sp_id;
		$c2_info['nns_id'] = np_guid_rand();
		$c2_info['nns_send_time'] = date('Y-m-d H:i:s');
        if(!isset($this->arr_sp_config['ftp_cdn_send_mode_url']) || empty($this->arr_sp_config['ftp_cdn_send_mode_url']))
        {
            nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, "未配置FTP上传地址", $this->str_sp_id);
            return $this->_make_return_data(1,'未配置FTP上传地址');
        }
		//检查时间范围是否有效执行
//		if ($this->exists_c2_commond($c2_info))
//		{
//			return $this->_make_return_data(1,'该task在一小时内执行过或则task查询无数据,task信息:'.var_export($c2_info,true));
//		}
		$sub_path = date('Y-m-d');
		$file = $c2_info['nns_url'];
		$c2_info['nns_url'] = $sub_path . '/' . $file;
		//生成soap xml文件 路径
		$file_path = $this->c2_xml_path . '/inject_xml/' . $sub_path . '/';
		if (!is_dir($file_path))
		{
            $flg = mkdir($file_path, 0777, true);
            if (!$flg)
            {
                nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, "本地创建路径失败".$file_path, $this->str_sp_id);
                return $this->_make_return_data(1,'本地创建路径失败'.$file_path);
            }
		}
		$file_path .= $file;
		$put_re = file_put_contents($file_path, $c2_info['nns_content']);
		if(!$put_re)
        {
            nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, "写入本地ADI文件失败".$file_path, $this->str_sp_id);
            return $this->_make_return_data(1,'写入本地ADI文件失败'.$file_path);
        }
        nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, "上传FTP参数：本地文件[{$file_path}]上传至[{$this->arr_sp_config['ftp_cdn_send_mode_url']}]", $this->str_sp_id);
		//FTP模式。上传到指定的FTP服务器上
		$up_re = $this->up_to_ftp($file_path,$this->arr_sp_config['ftp_cdn_send_mode_url'],false);
        nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, "上传结果：".var_export($up_re,true), $this->str_sp_id);
		if($up_re['ret'] != 0)
		{
			//删除本地文件
            unlink($file_path);
            $c2_info['nns_result'] = '[-1]' . $up_re['reason'];
            $c2_info['nns_notify_result'] = -1;
            nl_c2_log::add($this->obj_dc, $c2_info);
			return $this->_make_return_data(1,'ADI文件上传至FTP失败:' . $up_re['reason']);
		}
		else
        {
            //CDN单日志模式开启
            $exist_result = nl_c2_log::check_task_exist($this->obj_dc, array('nns_task_id' => $c2_info['nns_task_id'],'nns_action' => $c2_info['nns_action']));
            if($this->arr_sp_config['cdn_single_log_mode'] == '0' && $exist_result['ret'] == 0)
            {
                $c2_id = $exist_result['data_info'][0]['nns_id'];
                $c2_info['nns_id'] = $c2_id;
                $c2_info['nns_notify_fail_reason'] = '';
                $c2_info['nns_cdn_send_time'] = $exist_result['data_info'][0]['nns_cdn_send_time'] + 1;
                $c2_info['nns_create_time'] = $exist_result['data_info'][0]['nns_create_time'];
                $c2_info['nns_notify_content'] = "";
                nl_c2_log::edit($this->obj_dc, $c2_info, $c2_id);
            }
            else
            {
                $c2_info['nns_notify_result'] = '';
                $c2_info['nns_result'] = '[0]';
                nl_c2_log::add($this->obj_dc, $c2_info);
            }
            return $this->_make_return_data(0,'发送C2成功');
        }
	}
	/**
	 * 注入反馈回调
	 */
	public function save_c2_notify()
	{
	    $back_info = array();
        foreach ($this->arr_params as $params)
        {
            nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, 'C2处理反馈开始', $this->str_sp_id);
            $result_media_import_info = public_model_exec::get_asset_id_by_sp_import_id($this->str_sp_id, 'media', $params['original_id'],$params['cp_id'],$this->arr_sp_config);
            if($result_media_import_info['ret'] != 0 || !is_array($result_media_import_info['data_info']['data_info']) || empty($result_media_import_info['data_info']['data_info']))
            {
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, "根据片源注入ID未查询到片源信息:{$params['original_id']}---{$params['cp_id']}", $this->str_sp_id);
                $back_info[] = array(
                    'msg_id' => $params['msg_id'],
                    'status' => 1,
                );
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, 'C2处理反馈结束', $this->str_sp_id);
                continue;
            }
            $package_info = $result_media_import_info['data_info']['data_info'];

            $query_params = array(
                'nns_type' => 'media',//片源
                //'nns_status' => '5',//正在注入
                'nns_ref_id' => $package_info['nns_id'],
                'nns_org_id' => $this->str_sp_id
            );
            $task_type = "Movie";
            nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, "根据片源信息查询C2任务信息:".var_export($query_params,true), $this->str_sp_id);
            //根据注入ID获取c2任务ID
            $c2_task_info = nl_c2_task::query_by_condition($this->obj_dc,$query_params,0,1);
            if($c2_task_info['ret'] != 0 || !is_array($c2_task_info['data_info']))
            {
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, "未查询到C2任务信息", $this->str_sp_id);
                $back_info[] = array(
                    'msg_id' => $params['msg_id'],
                    'status' => 1,
                );
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, 'C2处理反馈结束', $this->str_sp_id);
                continue;
            }
            $log_query_params = array(
                'nns_task_id' => $c2_task_info['data_info'][0]['nns_id'],
                'nns_task_type' => $task_type,
                'nns_org_id' => $this->str_sp_id,
            );
            nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, "根据C2任务查询C2注入日志信息:".var_export($log_query_params,true), $this->str_sp_id);
            //根据c2id查询c2注入日志
            $c2_log_re = nl_c2_log::check_task_exist($this->obj_dc, $log_query_params);
            if($c2_log_re['ret'] != 0 || !is_array($c2_log_re['data_info']))
            {
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, "未查询到C2注入日志信息", $this->str_sp_id);
                $back_info[] = array(
                    'msg_id' => $params['msg_id'],
                    'status' => 1,
                );
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, 'C2处理反馈结束', $this->str_sp_id);
                continue;
            }
            //修改c2日志
            $c2_id = $c2_log_re['data_info'][0]['nns_id'];
            $c2_log_info = array(
                'nns_notify_fail_reason' => '',
                'nns_notify_result' => 0,
            );
            $edit_re = nl_c2_log::edit($this->obj_dc, $c2_log_info, $c2_id);//修改C2日志
            if($edit_re['ret'] != 0)
            {
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, "修改C2注入日志失败", $this->str_sp_id);
                $back_info[] = array(
                    'msg_id' => $params['msg_id'],
                    'status' => 1,
                );
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, 'C2处理反馈结束', $this->str_sp_id);
                continue;
            }
            $c2_re = nl_c2_task::edit($this->obj_dc, array('nns_status' => 0), $c2_task_info['data_info'][0]['nns_id']);//修改C2任务
            if($c2_re['ret'] != 0)
            {
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, "修改C2任务失败", $this->str_sp_id);
                $back_info[] = array(
                    'msg_id' => $params['msg_id'],
                    'status' => 1,
                );
                nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, 'C2处理反馈结束', $this->str_sp_id);
                continue;
            }
            $back_info[] = array(
                'msg_id' => $params['msg_id'],
                'status' => 0,
            );
        }
		return $back_info;
	}

	/**
	 * 把ADIxml文件上传至与FTP上
	 * @param string $local_path 本地文件路径
	 * @param string $up_ftp  上传FTP
	 * @param bool $passive 主被动模式，默认被动模式
	 * @return Ambigous <multitype:number , multitype:number string >
	 */
	public function up_to_ftp($local_path,$up_ftp,$passive = true)
	{
		$arr_ftp = parse_url($up_ftp);
		if(!isset($arr_ftp['port']))
		{
			$arr_ftp['port'] = 21;
		}
		$ftp = new nns_ftp($arr_ftp['host'], $arr_ftp['user'], $arr_ftp['pass'], $arr_ftp['port']);
		$ftpcon = $ftp->connect(20, $passive);
		if(!$ftpcon)
		{
			return $this->_make_return_data(1,'FTP链接失败' . $up_ftp);
		}
		$path_arr = pathinfo($local_path);
		$path = empty($arr_ftp['path']) ? "./" : "." . $arr_ftp['path'];
		$ftp_re = $ftp->up($path, $path_arr['basename'], $local_path);
		if(!$ftp_re)
		{
            nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, "上传失败：".var_export($ftp->error(),true), $this->str_sp_id);
			$error_msg = '上传FTP'.$up_ftp.'失败,上传路径:'.$arr_ftp['path'].',上传文件名:'.$path_arr['basename'].',上传的本地文件:'.$local_path;
			return $this->_make_return_data(1,$error_msg);
		}
		return $this->_make_return_data(0,"上传成功");
	}
}