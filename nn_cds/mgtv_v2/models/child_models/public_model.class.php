<?php
/**
 * 公共CDN解析模块
 * @author liangpan
 */
class nn_public_model
{
	public $obj_dc = null;
	//传入参数
	public $arr_params = null;
	//SP ID
	public $str_sp_id = null;
	//SP config
	public $arr_sp_config = null;
	//CP ID
	public $str_cp_id = null;
	//CP config
	public $arr_cp_config = null;
	//DEBUG 数据
	public $arr_debug_data=null;
	
	public $arr_category = null;
	
	
	public $arr_vod_video_img = array(
	    'nns_image0'=>3,
	    'nns_image1'=>2,
	    'nns_image2'=>1,
	    'nns_image_v'=>5,
	    'nns_image_s'=>6,
	    'nns_image_h'=>4,
	);
	
	public $arr_vod_index_img = array(
	    'nns_image'=>1,
	);
	
	public $arr_channel_img = array(
	    'nns_image0'=>3,
	    'nns_image1'=>2,
	    'nns_image2'=>1,
	    'nns_image_v'=>5,
	    'nns_image_s'=>6,
	    'nns_image_h'=>4,
	    'nns_image_logo'=>7,
	);
	
	public $arr_playbill_img = array(
	    'nns_image0'=>3,
	    'nns_image1'=>2,
	    'nns_image2'=>1,
	    'nns_image3'=>4,
	    'nns_image4'=>5,
	    'nns_image5'=>6,
	);
	
	/**
	 * XML存放路径
	 */
	public $c2_xml_path = '';
	
	/**
	 * @return the $arr_debug_data
	 */
	public function get_arr_debug_data()
	{
		return $this->arr_debug_data;
	}
	
	/**
	 * @param field_type $arr_debug_data
	 */
	public function set_arr_debug_data($arr_debug_data)
	{
		$this->arr_debug_data[] = $arr_debug_data;
	}
	/**
	 * 初始化数据
	 * @param string $obj_dc
	 * @param string $str_func
	 * @param string $arr_params
	 * @param string $sp_id
	 * @param string $sp_config
	 * @param string $cp_id
	 * @param string $cp_config
	 * @return boolean
	 */
	public function __construct($obj_dc=null,$sp_id=null,$sp_config=null,$arr_params=null,$cp_id=null,$cp_config=null)
	{
		if($obj_dc === null)
		{
			$obj_dc = nl_get_dc(array(
			    'db_policy' =>  NL_DB_READ | NL_DB_WRITE,
			    'cache_policy' => NP_KV_CACHE_TYPE_NULL
			   )
			);
		}
		$this->obj_dc = $obj_dc;
		$this->str_sp_id = $sp_id;
		$this->str_cp_id = $cp_id;
		$this->arr_sp_config = $sp_config;
		$this->arr_cp_config = $cp_config;
		$this->arr_params = $arr_params;
		$this->c2_xml_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/mgtv/' . $this->str_sp_id;
		$sp_config['cdn_report_task_model'] = isset($sp_config['cdn_report_task_model']) ? trim($sp_config['cdn_report_task_model']) : '';
		if(strlen($sp_config['cdn_report_task_model']) < 1)
		{
			return $this->_make_return_data(2,'还是以老逻辑执行CDN注入');
		}
		return $this->_make_return_data(0,'加载成功');
	}

	/**
	 * 组装反馈数据
	 * @param number $ret
	 * @param string $reason
	 * @param string $reason_en
	 * @param string $data
	 * @return multitype:number string
	 */
	public function _make_return_data($ret=1,$reason='error',$data=null)
	{
		return array(
				'ret'=>$ret,
				'reason'=>$reason,
				'data_info'=>$data,
		);
	}
	
	
	/**
	 * 获取栏目列表
	 * @param unknown $video_type
	 */
	public function get_category($video_type)
	{
	    if(strlen($video_type) <1)
	    {
	        return $this->_make_return_data(1, '$video_type为空');
	    }
	    if(isset($this->arr_category[$video_type]))
	    {
	        if(empty($this->arr_category[$video_type]))
	        {
	            return $this->_make_return_data(1, '获取资源库栏目无数据');
	        }
	        return $this->_make_return_data(0, 'ok');
	    }
	    $result_depot = nl_depot::get_depot_info($this->obj_dc,null,$video_type);
	    if($result_depot['ret'] !=0)
	    {
	        return $result_depot;
	    }
	    $result_depot = isset($result_depot['data_info'][0]['nns_category']) ? $result_depot['data_info'][0]['nns_category'] : '';
	    if(strlen($result_depot) <1)
	    {
	        $this->arr_category[$video_type] = null;
	        return $this->_make_return_data(1, '获取资源库栏目无数据');
	    }
	    $dom = new DOMDocument('1.0', 'utf-8');
	    $dom->loadXML($result_depot);
	    $categorys = $dom->getElementsByTagName('category');
	    foreach ($categorys as $category)
	    {
	        $this->arr_category[$video_type][(string)$category->getAttribute('id')] = array(
	            'id'=>(string)$category->getAttribute('id'),
	            'name'=>$category->getAttribute('name'),
	            'parent_id'=>((string)$category->getAttribute('parent') == '0') ? 0 : (string)$category->getAttribute('parent'),
	        );
	    }
	    return $this->_make_return_data(0, 'ok');
	}
	
	
	/**
	 * 组装MAPPING 关系
	 * @param unknown $str_parent_type
	 * @param unknown $op_action
	 * @param unknown $parent_info
	 * @param unknown $child_info
	 */
	public function auto_make_mapping($str_parent_type,$str_element_type,$op_action,$arr_info)
	{
		$str_xml='';
		foreach ($arr_info as $arr_list)
		{
			if(!isset($arr_list['parent_id']) || strlen($arr_list['parent_id']) >0 || isset($arr_list['child_id']) || strlen($arr_list['child_id']) >0)
			{
				continue;
			}
			$str_xml .= '<Mapping ParentType="'.$str_parent_type.'" ParentID="'.$arr_list['parent_id'].'" ElementType="'.$str_element_type.'" ElementID="'.$arr_list['child_id'].'" ParentCode="'.$arr_list['parent_id'].'" ElementCode="'.$arr_list['child_id'].'" Action="'.$op_action.'">';
			if(is_array($arr_list) && !empty($arr_list))
			{
				foreach ($arr_list as $ex_name_key=>$ex_name_val)
				{
					if(!is_string($ex_name_val) || !is_numeric($ex_name_val))
					{
							continue;
					}
					$str_xml .=  '<Property Name="'.$ex_name_key.'"><![CDATA['.$ex_name_val.']]></Property>';
				}
			}
			$str_xml .= '</Mapping>';
		}
		return $str_xml;
	}
	
	/**
	 * 中国联通标准C2
	 * @param unknown $c2_info
	 */
	public function execute_c2($c2_info)
	{
		$c2_info['nns_type'] = 'std';
		$c2_info['nns_org_id'] = $this->str_sp_id;
		if ($this->str_sp_id == 'ZTE_CDN')
		{
			$c2_id = np_guid_rand_v2();
		}
		else
		{
			$c2_id = np_guid_rand();
		}
		$c2_info['nns_id'] = $c2_id;
		//加载soap客户端文件
		$file_soap_client_file = dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv/' . $this->str_sp_id . '/' . $this->str_sp_id . '_client.php';
		if(!file_exists($file_soap_client_file))
		{
			return $this->_make_return_data(1,'加载soap客户端文件不存在,文件路径:['.$file_soap_client_file.']');
		}
		include_once $file_soap_client_file;
		//检查时间范围是否有效执行
		if ($this->exists_c2_commond($c2_info))
		{
			return $this->_make_return_data(1,'该task在一小时内执行过或则task查询无数据,task信息:'.var_export($c2_info,true));
		}
		$sub_path = date('Y-m-d');
		$file = $c2_info['nns_url'];
		$c2_info['nns_url'] = $sub_path . '/' . $file;
		//生成soap xml文件 路径
		$file_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/mgtv/' . $this->str_sp_id . '/inject_xml/' . $sub_path . '/';
		if (!is_dir($file_path))
		{
			mkdir($file_path, 0777, true);
		}
		$file_path .= $file;
		file_put_contents($file_path, $c2_info['nns_content']);
		$CmdFileURL = trim(rtrim($this->arr_sp_config['xml_ftp'],'/')) . '/' . trim(ltrim($c2_info['nns_url'],'/'));
		try
		{
			$result = null;
			$result = call_user_func_array(array($this->str_sp_id.'_client', 'ContentDeployReq'), array($c2_id, $CmdFileURL));
			$result = isset($result->Result) ? $result->Result : isset($result->ResultCode) ? $result->ResultCode : $result->Result;
			$result = ($result === null) ? 0 : $result;
			$c2_info['nns_result'] = '[' . $result . ']';
			nl_c2_log::add($this->obj_dc, $c2_info);
			$str_ret = ($result != 0) ? 1 : 0;
			return $this->_make_return_data($str_ret,var_export($result,true));
		} catch (Exception $e)
		{
			$c2_info['nns_result'] = '[-1]';
			$c2_info['nns_notify_result'] = -1;
			nl_c2_log::add($this->obj_dc, $c2_info);
			return $this->_make_return_data(1,"soap客户端类文件获取异常:[{$file_soap_client_file}]");
		}
	}
	
	/**
	 * 中国电信标准C2
	 * @param unknown $c2_info
	 */
	public function execute_telecom_c2($c2_info)
	{
	    $c2_info['nns_type'] = 'std';
	    $c2_info['nns_org_id'] = $this->str_sp_id;
	    if ($this->str_sp_id == 'ZTE_CDN')
	    {
	        $c2_id = np_guid_rand_v2();
	    }
	    else
	    {
	        $c2_id = np_guid_rand();
	    }
	    $c2_info['nns_id'] = $c2_id;
	    //加载soap客户端文件
	    $file_soap_client_file = dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv/' . $this->str_sp_id . '/' . $this->str_sp_id . '_client.php';
	    if(!file_exists($file_soap_client_file))
	    {
	        return $this->_make_return_data(1,'加载soap客户端文件不存在,文件路径:['.$file_soap_client_file.']');
	    }
	    include_once $file_soap_client_file;
	    //检查时间范围是否有效执行
	    if ($this->exists_c2_commond($c2_info))
	    {
	        return $this->_make_return_data(1,'该task在一小时内执行过或则task查询无数据,task信息:'.var_export($c2_info,true));
	    }
	    $sub_path = date('Y-m-d');
	    $file = $c2_info['nns_url'];
	    $c2_info['nns_url'] = $sub_path . '/' . $file;
	    //生成soap xml文件 路径
	    $file_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/mgtv/' . $this->str_sp_id . '/inject_xml/' . $sub_path . '/';
	    if (!is_dir($file_path))
	    {
	        mkdir($file_path, 0777, true);
	    }
	    $file_path .= $file;
	    file_put_contents($file_path, $c2_info['nns_content']);
	    $CmdFileURL = trim(rtrim($this->arr_sp_config['xml_ftp'],'/')) . '/' . trim(ltrim($c2_info['nns_url'],'/'));
	    try
	    {
	        $result = null;
	        $result = call_user_func_array(array($this->str_sp_id.'_client', 'ContentDeployReq'), array($c2_id, $CmdFileURL));
	        $result = isset($result->Result) ? $result->Result : isset($result->ResultCode) ? $result->ResultCode : $result->Result;
	        $result = ($result === null) ? 0 : $result;
	        $c2_info['nns_result'] = '[' . $result . ']';
	        nl_c2_log::add($this->obj_dc, $c2_info);
	        $str_ret = ($result != 0) ? 1 : 0;
	        return $this->_make_return_data($str_ret,var_export($result,true));
	    } catch (Exception $e)
	    {
	        $c2_info['nns_result'] = '[-1]';
	        $c2_info['nns_notify_result'] = -1;
	        nl_c2_log::add($this->obj_dc, $c2_info);
	        return $this->_make_return_data(1,"soap客户端类文件获取异常:[{$file_soap_client_file}]");
	    }
	}
	
	
	/**
	 * 中国电信标准C2
	 * @param unknown $c2_info
	 */
	public function execute_standard_c2($c2_info)
	{
	    $c2_info['nns_type'] = 'std';
	    $c2_info['nns_org_id'] = $this->str_sp_id;
	    $c2_id = np_guid_rand();
	    $c2_info['nns_id'] = $c2_id;
	    //加载soap客户端文件
	    $file_soap_client_file = dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv/' . $this->str_sp_id . '/client.php';
	    if(!file_exists($file_soap_client_file))
	    {
	        return $this->_make_return_data(1,'加载soap客户端文件不存在,文件路径:['.$file_soap_client_file.']');
	    }
	    global $g_bk_web_url;
	    $bk_web_url = (isset($g_bk_web_url) && strlen($g_bk_web_url)>0) ? $g_bk_web_url : '';
	    unset($g_bk_web_url);
	    $bk_web_url = ltrim(ltrim(trim($bk_web_url),'/'),'\\');
	    if(strlen($bk_web_url) <1)
	    {
	        return $this->_make_return_data(1,'播控后台系统配置管理里面后台基本地址未配置:['.$bk_web_url.']');
	    }
	    include_once $file_soap_client_file;
	    //检查时间范围是否有效执行
	    if ($this->exists_c2_commond($c2_info))
	    {
	        return $this->_make_return_data(1,'该task在一小时内执行过或则task查询无数据,task信息:'.var_export($c2_info,true));
	    }
	    $sub_path = date('Y-m-d');
	    $file = $c2_info['nns_url'];
	    $c2_info['nns_url'] = $sub_path . '/' . $file;
	    //生成soap xml文件 路径
	    $file_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/mgtv/' . $this->str_sp_id . '/inject_xml/' . $sub_path . '/';
	    if (!is_dir($file_path))
	    {
	        mkdir($file_path, 0777, true);
	    }
	    $file_path .= $file;
	    file_put_contents($file_path, $c2_info['nns_content']);
	    $c2_info['nns_content'] = mysql_escape_string($c2_info['nns_content']);
	    $CmdFileURL = trim(rtrim($this->arr_sp_config['xml_ftp'],'/')) . '/' . trim(ltrim($c2_info['nns_url'],'/'));
	    try
	    {
	        $result = null;
            $obj_soap = new client();
            $result = $obj_soap->ContentDeployReq($c2_info['nns_id'], $CmdFileURL,$bk_web_url,$c2_info['nns_task_type']);
	        $result = isset($result->Result) ? $result->Result : isset($result->ResultCode) ? $result->ResultCode : $result->Result;
	        $result = ($result === null) ? 0 : $result;
	        $c2_info['nns_result'] = '[' . $result . ']';
	        $result_log = nl_c2_log::add($this->obj_dc, $c2_info);
	        $str_ret = ($result != 0) ? 1 : 0;
	        return $this->_make_return_data($str_ret,var_export($result,true));
	    } catch (Exception $e)
	    {
	        $c2_info['nns_result'] = '[-1]';
	        $c2_info['nns_notify_result'] = -1;
	        nl_c2_log::add($this->obj_dc, $c2_info);
	        return $this->_make_return_data(1,"soap客户端类文件获取异常:[{$file_soap_client_file}]");
	    }
	}
	
	/**
	 * 内部CDN内部自定义C2
	 * @param unknown $c2_info
	 * @param unknown $result
	 */
	public function execute_c2_msp($c2_info,$result,$info,$result_media=null)
	{
	    //检查时间范围是否有效执行
		if ($this->exists_c2_commond($c2_info))
		{
			//return $this->_make_return_data(1,'该task在一小时内执行过或则task查询无数据,task信息:'.var_export($c2_info,true));
		}
		$sub_path = date('Y-m-d');
		$file = $c2_info['nns_url'];
		$c2_info['nns_url'] = strtolower($c2_info['nns_task_type']).'/'.$sub_path . '/' . $file;
		$c2_info['nns_content'] = $result['send_message'];
		//生成soap xml文件 路径
		$file_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/mgtv/' . $this->str_sp_id . '/inject_xml/'.strtolower($c2_info['nns_task_type']).'/' . $sub_path . '/';
		
		$file_path_1 = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/mgtv/' . $this->str_sp_id . '/notify/'.strtolower($c2_info['nns_task_type']).'/' . $sub_path . '/';
		if (!is_dir($file_path))
		{
			mkdir($file_path, 0777, true);
		}
		if (!is_dir($file_path_1))
		{
		    mkdir($file_path_1, 0777, true);
		}
		$file_path .= $file;
		$file_path_1 .= $c2_info['nns_notify_result_url'];
		file_put_contents($file_path_1, json_encode($result,JSON_UNESCAPED_UNICODE));
		file_put_contents($file_path, $c2_info['nns_content']);
        $c2_info['nns_notify_time'] = date('Y-m-d H:i:s');
		$c2_info['nns_notify_fail_reason'] =$result['reason'];
		$c2_info['nns_result'] = '[0]';
		$c2_info['nns_notify_result'] = ($result['ret'] == '0') ? 0 : '-1';
		$c2_info['nns_notify_content'] = $sub_path . '/' . $c2_info['nns_notify_result_url'];
		if(isset($this->arr_sp_config['media_cdn_model']) && $this->arr_sp_config['media_cdn_model'] == '1')
		{
		    include_once dirname(dirname(__FILE__)).'/import_model.php';
		    if($this->arr_params['bk_c2_task_info']['nns_is_group'] !=1)
		    {
// 		        $queue_task_model = new queue_task_model();
// 		        $queue_task_model->q_report_task($this->arr_params['bk_c2_task_info']['nns_id']);
    		    $bool = ($result['ret'] == '0') ? true : false;
    		    $sql_c2_task_id = "select * from nns_mgtvbk_c2_task where nns_id='{$this->arr_params['nns_task_id']}' limit 1";
    		    $info_c2_task_op_id = nl_query_by_db($sql_c2_task_id, $this->obj_dc->db());
    		    $sql_query_op_id="select nns_id,nns_message_id from nns_mgtvbk_op_queue where nns_id = '{$info_c2_task_op_id[0]['nns_op_id']}' limit 1";
    		    $info_query_op_id = nl_query_by_db($sql_query_op_id, $this->obj_dc->db());
    		    $str_notify_cms_message_id = (isset($info_query_op_id[0]['nns_message_id'])) ? $info_query_op_id[0]['nns_message_id'] : $info_c2_task_op_id[0]['nns_message_id'];
    		    $asset_cdn_share_addr = isset($this->arr_sp_config['asset_cdn_share_addr']) ? trim(trim($this->arr_sp_config['asset_cdn_share_addr'],'/'),'\\') : '';
    		    $result_notify_cms = import_model::notify_cms_data($asset_cdn_share_addr, $info, $this->str_sp_id, $c2_info['nns_action'], $info_c2_task_op_id[0]['nns_type'], $str_notify_cms_message_id, $bool);
    		    if($result_notify_cms['ret'] !=0)
    		    {
    		        $bool = false;
    		    }
    		    $str_cdn_notify = isset($result_notify_cms['data_info']) ? $result_notify_cms['data_info'] : '';
		    }
		    else
		    {
				$bool = ($result['ret'] == '0') ? true : false;
				$sql_c2_task_id = "select * from nns_mgtvbk_c2_task where nns_id='{$this->arr_params['nns_task_id']}' limit 1";
				$info_c2_task_op_id = nl_query_by_db($sql_c2_task_id, $this->obj_dc->db());
		        $queue_task_model = new queue_task_model();
		        $queue_task_model->q_report_task($this->arr_params['bk_c2_task_info']['nns_id']);
		        if($queue_task_model->is_need_notify_data && $queue_task_model->arr_nedd_notify_data['ret'] ==0 &&
		            isset($queue_task_model->arr_nedd_notify_data['data_info']) && is_array($queue_task_model->arr_nedd_notify_data['data_info'])
		            && !empty($queue_task_model->arr_nedd_notify_data['data_info']))
		        {
		            $str_notify_cms_message_id = (isset($info_query_op_id[0]['nns_message_id'])) ? $info_query_op_id[0]['nns_message_id'] : $info_c2_task_op_id[0]['nns_message_id'];
		            $asset_cdn_share_addr = isset($this->arr_sp_config['asset_cdn_share_addr']) ? trim(trim($this->arr_sp_config['asset_cdn_share_addr'],'/'),'\\') : '';
		            $result_notify_cms = import_model::notify_cms_data($asset_cdn_share_addr, $info, $this->str_sp_id, $c2_info['nns_action'], $info_c2_task_op_id[0]['nns_type'], $str_notify_cms_message_id, $bool,$queue_task_model->arr_nedd_notify_data['data_info']);
		            if($result_notify_cms['ret'] !=0)
		            {
		                $bool = false;
		            }
		            $str_cdn_notify = isset($result_notify_cms['data_info']) ? $result_notify_cms['data_info'] : '';
		        }
		        
		    }
		}
		else if(isset($this->arr_sp_config['media_cdn_model']) && $this->arr_sp_config['media_cdn_model'] == '3')
		{
		    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/c2_task/c2_task.class.php';
	        $queue_task_model = new queue_task_model();
	        $queue_task_model->q_report_task($this->arr_params['bk_c2_task_info']['nns_id']);
	        $queue_c2_task_info = nl_c2_task::query_by_id($this->obj_dc, $this->arr_params['bk_c2_task_info']['nns_id']);
	        if(isset($queue_c2_task_info['data_info']) && is_array($queue_c2_task_info['data_info']) && !empty($queue_c2_task_info['data_info']))
	        {
	            $sql_message = "select * from nns_mgtvbk_message where nns_message_id='{$queue_c2_task_info['data_info']['nns_message_id']}' and nns_cp_id='{$info['nns_cp_id']}' limit 1";
	            $queue_info_message = nl_query_by_db($sql_message, $this->obj_dc->db());
	            if(isset($queue_info_message[0]) && is_array($queue_info_message[0]) && !empty($queue_info_message[0]) && strlen($queue_info_message[0]['nns_encrypt']) >0)
	            {
	                if($queue_c2_task_info['data_info']['nns_type'] == 'media')
	                {
	                    $send_data = $this->query_vod_media_info($info);
	                }
	                else 
	                {
	                    $send_data = $this->query_live_media_info($info);
	                }
	                if(isset($send_data['data_info']) && strlen($send_data['data_info']) >0)
	                {	                    
	                    $obj_curl = new np_http_curl_class();
                        $arr_header = array(
                            "Content-type: text/xml;charset=\"utf-8\"",
                            "Cache-Control: no-cache",
                        );
                        $file_ret = $obj_curl->do_request('post',$queue_info_message[0]['nns_encrypt'],$send_data['data_info'],$arr_header,60);
                		$curl_info = $obj_curl->curl_getinfo();
		                unset($obj_curl);
	                }
	            }
	        }
	        if($result_media['nns_deleted'] !='1' && isset($result['message']) && $this->is_xml($result['message']))
	        {
	            include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/np/np_xml_to_array.class.php';
	            $dom = new DOMDocument('1.0', 'utf-8');
	            $dom->loadXML($result['message']);
	            $cmspost = $dom->saveXML();
	            $xml_arr_data = np_xml_to_array::parse2($cmspost);
	            if(isset($xml_arr_data['content']['@attributes']['id']) && strlen($xml_arr_data['content']['@attributes']['id'])>0)
	            {
	                $result_edit_media = nl_vod_media_v2::edit($this->obj_dc,array('nns_content_id'=>$xml_arr_data['content']['@attributes']['id'],'nns_media_service'=>'msp'),$result_media['nns_id']); 
	            }
	            else
	            {
	                $result_edit_media = nl_vod_media_v2::edit($this->obj_dc,array('nns_content_id'=>md5($result_media['nns_cp_id'].$result_media['nns_import_id']),'nns_media_service'=>'msp'),$result_media['nns_id']); 
	            }
	            if($result_edit_media['ret'] !='0')
	            {
	                return $result_edit_media;
	            }
	        }
	        
		}
		else if(!in_array($c2_info['nns_task_type'], array('PhysicalChannel','Schedule')))
		{
		    if(isset($this->arr_sp_config['media_cdn_model']) && $this->arr_sp_config['media_cdn_model'] == '2')
		    {
		        include_once dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv_v2/models/c2_task_model.php';
		        c2_task_model::notify_org_platfrom($this->obj_dc,$c2_info,$this->arr_sp_config);
		    }
		    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/c2_task/c2_task.class.php';
		    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
		    $result_edit_c2 = nl_c2_task::edit($this->obj_dc, array('nns_status'=>'0'), $c2_info['nns_task_id']);
		    if($result_edit_c2['ret'] !='0')
		    {
		        return $result_edit_c2;
		    }
		    $result_edit_media = nl_vod_media_v2::edit($this->obj_dc,array('nns_content_id'=>$result_media['nns_media_import_id'],'nns_media_service'=>'msp'),$result_media['nns_id']);
		    if($result_edit_media['ret'] !='0')
		    {
		        return $result_edit_media;
		    }
		}
	    return nl_c2_log::add($this->obj_dc, $c2_info);
	}
	
	/**
	 * 
	 * @param unknown $string
	 */
	public function is_xml($string)
	{
	    if(!is_string($string))
	    {
	        return false;
	    }
	    $string = strlen($string) < 1 ? '' : trim($string);
	    if(strlen($string) <1)
	    {
	        return false;
	    }
	    $xml_parser = xml_parser_create();
	    if (! xml_parse($xml_parser, $string, true))
	    {
	        xml_parser_free($xml_parser);
	        return false;
	    }
	    return true;
	}
	
	/**
	 * 点播片源查询
	 * @param unknown $val
	 */
	public function query_vod_media_info($val)
	{
	    $data_media = nl_query_by_db("select * from nns_vod_media where nns_import_id = '{$val['nns_import_id']}' and nns_cp_id='{$val['nns_cp_id']}'", $this->obj_dc->db());
	    if(empty($data_media) || !is_array($data_media))
	    {
	        return $this->_make_return_data(1,'error');
	    }
	    $arr_media_info = null;
	    foreach ($data_media as $arr_media_data_info)
	    {
	        $arr_media_info[$arr_media_data_info['nns_id']] = $arr_media_data_info;
	    }
	    #TODO
	    $result_c2_info = nl_c2_task::query_by_condition_v2($this->obj_dc, array('nns_org_id'=>'starcor','nns_type'=>'media','nns_ref_id'=>array_keys($arr_media_info)));
	    if($result_c2_info['ret'] !=0)
	    {
	        return $this->_make_return_data(1,'queue query error');
	    }
	    $result_c2_info['data_info'] = (isset($result_c2_info['data_info']) && is_array($result_c2_info['data_info']) && !empty($result_c2_info['data_info'])) ? $result_c2_info['data_info'] : null;
	    $temp_c2_info = null;
	    if(is_array($result_c2_info['data_info']) && !empty($result_c2_info['data_info']))
	    {
	        foreach ($result_c2_info['data_info'] as $c2_info)
	        {
	            $temp_c2_info[$c2_info['nns_ref_id']] = $c2_info;
	        }
	    }
	    foreach ($arr_media_info as $value)
	    {
	        $nns_ext_info = (isset($value['nns_ext_info']) && strlen($value['nns_ext_info'])) ? json_decode($value['nns_ext_info'],true) : null;
	        $nns_ext_url = (isset($value['nns_ext_url']) && strlen($value['nns_ext_url'])) ? json_decode($value['nns_ext_url'],true) : null;
	        $str_providerID = isset($nns_ext_url['providerID']) ? $nns_ext_url['providerID'] : '';
	        $str_percentComplete = (isset($temp_c2_info[$value['nns_id']]['nns_status']) && $temp_c2_info[$value['nns_id']]['nns_status'] == '0') ? 100 : 60;
	        if($value['nns_deleted'] == '1')
	        {
	            $str_state = $str_percentComplete == 100 ? 'Canceled' : 'Transfer';
	        }
	        else
	        {
	            $str_state = $str_percentComplete == 100 ? 'Complete' : 'Transfer';
	        }
	        $int_contentSize = (isset($nns_ext_info['size']) && (int)$nns_ext_info['size'] >0) ? (int)$nns_ext_info['size'] : 0;
	        $str_md5Checksum = isset($nns_ext_info['file_md5']) ? $nns_ext_info['file_md5'] : '';
	        $str_playUrl = isset($nns_ext_info['path']) ? $nns_ext_info['path'] : '';
	        $str_md5DateTime = (isset($temp_c2_info[$value['nns_id']]['nns_modify_time']) && strlen($temp_c2_info[$value['nns_id']]['nns_modify_time']) >0) ? $temp_c2_info[$value['nns_id']]['nns_modify_time'] : date("Y-m-d H:i:s");
	        $int_time = strtotime($str_md5DateTime);
	        $str_md5DateTime = date("Y-m-d",$str_md5DateTime).'T'.date("H:i:s",$str_md5DateTime).'Z';
	        $value['nns_import_id'] = explode('|',$value['nns_import_id']);
	        $xml  = '<?xml version="1.0" encoding="UTF-8"?>';
	        $xml .= '<TransferStatus ';
	        $xml .=' providerID="'.$str_providerID.'" ';
	        $xml .=' assetID="'.$value['nns_import_id'][0].'" ';
	        $xml .=' volumeName="'.$value['nns_name'].'" ';
	        $xml .=' state="'.$str_state.'" ';
	        $xml .=' percentComplete="'.$str_percentComplete.'" ';
	        $xml .=' contentSize="'.$int_contentSize.'" ';
	        $xml .=' md5Checksum="'.$str_md5Checksum.'" ';
	        $xml .=' md5DateTime="'.$str_md5DateTime.'" ';
	        $xml .=' avgBitRate ="'.$value['nns_kbps'].'" ';
	        $xml .=' playUrl="'.$str_playUrl.'" ';
	        $xml .= '/>';
	        break;
	    }
	    return $this->_make_return_data(0,'queue ok',$xml);
	}
	
	/**
	 * 直播片源查询
	 * @param unknown $val
	 */
	public function query_live_media_info($val)
	{
	    $data_media = nl_query_by_db("select * from nns_live_media where nns_content_id = '{$val['nns_import_id']}' and nns_cp_id='{$val['nns_cp_id']}'", $this->obj_dc->db());
	    if(empty($data_media) || !is_array($data_media))
	    {
	        return $this->_make_return_data(1,'error');
	    }
	    $arr_media_info = null;
	    foreach ($data_media as $arr_media_data_info)
	    {
	        $arr_media_info[$arr_media_data_info['nns_id']] = $arr_media_data_info;
	    }
	    #TODO
	    $result_c2_info = nl_c2_task::query_by_condition_v2($this->obj_dc, array('nns_org_id'=>'starcor','nns_type'=>'live_media','nns_ref_id'=>array_keys($arr_media_info)));
	    if($result_c2_info['ret'] !=0)
	    {
	        return $this->_make_return_data(1,'queue query error');
	    }
	    $result_c2_info['data_info'] = (isset($result_c2_info['data_info']) && is_array($result_c2_info['data_info']) && !empty($result_c2_info['data_info'])) ? $result_c2_info['data_info'] : null;
	    $temp_c2_info = null;
	    if(is_array($result_c2_info['data_info']) && !empty($result_c2_info['data_info']))
	    {
	        foreach ($result_c2_info['data_info'] as $c2_info)
	        {
	            $temp_c2_info[$c2_info['nns_ref_id']] = $c2_info;
	        }
	    }
	    foreach ($arr_media_info as $value)
	    {
	        $nns_ext_info = (isset($value['nns_ext_info']) && strlen($value['nns_ext_info'])) ? json_decode($value['nns_ext_info'],true) : null;
	        $nns_ext_url = (isset($value['nns_ext_url']) && strlen($value['nns_ext_url'])) ? json_decode($value['nns_ext_url'],true) : null;
	        $str_providerID = isset($nns_ext_url['providerID']) ? $nns_ext_url['providerID'] : '';
	        $str_percentComplete = (isset($temp_c2_info[$value['nns_id']]['nns_status']) && $temp_c2_info[$value['nns_id']]['nns_status'] == '0') ? 100 : 60;
	        if($value['nns_deleted'] == '1')
	        {
	            $str_state = $str_percentComplete == 100 ? 'Canceled' : 'Transfer';
	        }
	        else
	        {
	            $str_state = $str_percentComplete == 100 ? 'Complete' : 'Transfer';
	        }
	        $int_contentSize = (isset($nns_ext_info['size']) && (int)$nns_ext_info['size'] >0) ? (int)$nns_ext_info['size'] : 0;
	        $str_md5Checksum = isset($nns_ext_info['file_md5']) ? $nns_ext_info['file_md5'] : '';
	        $str_playUrl = isset($nns_ext_info['path']) ? $nns_ext_info['path'] : '';
	        $str_md5DateTime = (isset($temp_c2_info[$value['nns_id']]['nns_modify_time']) && strlen($temp_c2_info[$value['nns_id']]['nns_modify_time']) >0) ? $temp_c2_info[$value['nns_id']]['nns_modify_time'] : date("Y-m-d H:i:s");
	        $int_time = strtotime($str_md5DateTime);
	        $str_md5DateTime = date("Y-m-d",$str_md5DateTime).'T'.date("H:i:s",$str_md5DateTime).'Z';
	        $value['nns_content_id'] = explode('|',$value['nns_content_id']);
	        $xml  = '<?xml version="1.0" encoding="UTF-8"?>';
	        $xml .= '<TransferStatus ';
	        $xml .=' providerID="'.$str_providerID.'" ';
	        $xml .=' assetID="'.$value['nns_import_id'][0].'" ';
	        $xml .=' volumeName="'.$value['nns_name'].'" ';
	        $xml .=' state="'.$str_state.'" ';
	        $xml .=' percentComplete="'.$str_percentComplete.'" ';
	        $xml .=' contentSize="'.$int_contentSize.'" ';
	        $xml .=' md5Checksum="'.$str_md5Checksum.'" ';
	        $xml .=' md5DateTime="'.$str_md5DateTime.'" ';
	        $xml .=' avgBitRate ="'.$value['nns_kbps'].'" ';
	        $xml .=' playUrl="'.$str_playUrl.'" ';
	        $xml .= '/>';
	        break;
	    }
	    return $this->_make_return_data(0,'queue ok',$xml);
	}
	
	
	
	/**
	 * 检查C2表在一段时间内是否存在，如果不存在返回false 存在反馈true
	 * @param unknown $c2_info
	 * @return boolean
	 */
	public function exists_c2_commond($c2_info)
	{
		return false;
		set_time_limit(0);
		$nns_org_id = $this->str_sp_id;
		$nns_task_type = $c2_info['nns_task_type'];
		$nns_task_id = $c2_info['nns_task_id'];
		$nns_action = $c2_info['nns_action'];
		if (empty($nns_task_id))
		{
			return false;
		}
		usleep(5000);
		$sql = "select 1 from nns_mgtvbk_c2_log where nns_org_id='{$nns_org_id}' and nns_task_type='{$nns_task_type}' and nns_task_id='{$nns_task_id}' and nns_action='{$nns_action}' and nns_notify_result is null and nns_create_time > NOW() - INTERVAL 3600 SECOND";
		$data = nl_db_get_all($sql, $this->obj_dc->db());
		return (is_array($data) && count($data) > 0) ? true : false;
	}
	
	/**
	 * 获取消息队列的数据
	 * @param unknown $cp_id
	 * @param unknown $sp_id
	 */
    public function query_message_info($cp_id)
    {
        $data = array(
            'sop_id'=>$this->str_sp_id,
            'xml_url_qc'=>'',
            'encrypt'=>'',
            'content_mng_xml'=>'',
            'correlate_id'=>''
        );
        $sql_c2_task_id = "select * from nns_mgtvbk_c2_task where nns_id='{$this->arr_params['nns_task_id']}' limit 1";
	    $info_c2_task_op_id = nl_query_by_db($sql_c2_task_id, $this->obj_dc->db());
	    $sql_query_op_id="select nns_id,nns_message_id from nns_mgtvbk_op_queue where nns_id = '{$info_c2_task_op_id[0]['nns_op_id']}' limit 1";
	    $info_query_op_id = nl_query_by_db($sql_query_op_id, $this->obj_dc->db());
	    $str_notify_cms_message_id = (isset($info_query_op_id[0]['nns_message_id'])) ? $info_query_op_id[0]['nns_message_id'] : $info_c2_task_op_id[0]['nns_message_id'];
        $result_messgae = nl_message::query_message_by_message_id($this->obj_dc, $str_notify_cms_message_id, $cp_id);
        if(!isset($result_messgae['data_info']) || empty($result_messgae['data_info']) || !is_array($result_messgae['data_info']))
        {
            return $data;
        }
        $data['xml_url_qc'] = $result_messgae['data_info']['nns_xmlurlqc'];
        $data['encrypt'] = $result_messgae['data_info']['nns_xmlurlqc'];
        $data['content_mng_xml'] = $result_messgae['data_info']['nns_message_content'];
        $data['correlate_id'] = $result_messgae['data_info']['nns_message_id'];
        return $data;
    }
    
    
    /**
     * 茁壮C2
     * @param unknown $c2_info
     */
    public function execute_c2_homed($c2_info)
    {
        $c2_info['nns_type'] = 'std';
        $c2_info['nns_org_id'] = $this->str_sp_id;
        $c2_info['nns_id'] = np_guid_rand();
        //加载soap客户端文件
        $file_soap_client_file = dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv/' . $this->str_sp_id . '/client.php';
        if(!file_exists($file_soap_client_file))
        {
            return $this->_make_return_data(1,'加载soap客户端文件不存在,文件路径:['.$file_soap_client_file.']');
        }
        global $g_bk_web_url;
        $bk_web_url = (isset($g_bk_web_url) && strlen($g_bk_web_url)>0) ? $g_bk_web_url : '';
        unset($g_bk_web_url);
        $bk_web_url = ltrim(ltrim(trim($bk_web_url),'/'),'\\'); 
        if(strlen($bk_web_url) <1)
        {
            return $this->_make_return_data(1,'播控后台系统配置管理里面后台基本地址未配置:['.$bk_web_url.']');
        }
        include_once $file_soap_client_file;
        //检查时间范围是否有效执行
        if ($this->exists_c2_commond($c2_info))
        {
            return $this->_make_return_data(1,'该task在一小时内执行过或则task查询无数据,task信息:'.var_export($c2_info,true));
        }
        $sub_path = date('Y-m-d');
        $file = $c2_info['nns_url'];
        $c2_info['nns_url'] = $sub_path . '/' . $file;
        //生成soap xml文件 路径
        $file_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/mgtv/' . $this->str_sp_id . '/inject_xml/' . $sub_path . '/';
        if (!is_dir($file_path))
        {
            mkdir($file_path, 0777, true);
        }
        $file_path .= $file;
        file_put_contents($file_path, $c2_info['nns_content']);
        $FtpPath = trim(rtrim($this->arr_sp_config['xml_ftp'],'/')).'/';
        $AdiFileName = '/'.trim(ltrim($c2_info['nns_url'],'/'));
        try
        {
            $result = null;
            $obj_soap = new client();
            $result = $obj_soap->ContentDeployReq($c2_info['nns_id'], $FtpPath,$AdiFileName,$bk_web_url,$c2_info['nns_task_type']);
            $result = isset($result->Result) ? $result->Result : isset($result->ResultCode) ? $result->ResultCode : $result->Result;
            $result = ($result === null) ? 0 : $result;
            $c2_info['nns_result'] = '[' . $result . ']';
            nl_c2_log::add($this->obj_dc, $c2_info);
            $str_ret = ($result != 0) ? 1 : 0;
            return $this->_make_return_data($str_ret,var_export($result,true));
        } catch (Exception $e)
        {
            $c2_info['nns_result'] = '[-1]';
            $c2_info['nns_notify_result'] = -1;
            nl_c2_log::add($this->obj_dc, $c2_info);
            return $this->_make_return_data(1,"soap客户端类文件获取异常:[{$file_soap_client_file}]");
        }
    }
    
    /**
     * 组装mapping
     * @param unknown $parent_type
     * @param unknown $type
     * @param unknown $parent_id
     * @param unknown $id
     * @param unknown $action
     * @param string $arr_params
     */
    public function make_mapping($parent_type,$type,$parent_id,$id,$action,$arr_params=null,$mapping_type=1)
    {
        $str_xml ='<Mapping ParentType="'.$parent_type.'" ParentID="'.$parent_id.'" ElementType="'.$type.'" ElementID="'.$id.'" ParentCode="'.$parent_id.'" ElementCode="'.$id.'" Action="'.$action.'" Type="'.$mapping_type.'">';
        if(is_array($arr_params) && !empty($arr_params))
        {
            foreach ($arr_params as $key=>$val)
            {
                $str_xml.=	'<Property Name="'.$key.'"><![CDATA['.$val.']]></Property>';
            }
        }
        $str_xml.='</Mapping>';
        return $str_xml;
    }
    
    /**
     * 组装mapping
     * @param unknown $parent_type
     * @param unknown $type
     * @param unknown $parent_id
     * @param unknown $id
     * @param unknown $action
     * @param string $arr_params
     */
    public function make_object($type,$id,$action,$arr_params=null)
    {
        $str_xml ='<Object ElementType="'.$type.'" ContentID="'.$id.'" Action="'.$action.'" ElementID="'.$id.'" Code="'.$id.'">';
        if(is_array($arr_params) && !empty($arr_params))
        {
            foreach ($arr_params as $key=>$val)
            {
                $str_xml.=	'<Property Name="'.$key.'"><![CDATA['.$val.']]></Property>';
            }
        }
        $str_xml.='</Object>';
        return $str_xml;
    }
    
    /**
     * 组装img
     * @param unknown $arr_video
     * @param unknown $type
     */
    public function make_image($arr_video,$parent_type,$parent_id,$action)
    {
        $arr_map = null;
        $str_picture_obj = $str_picture_mapping = '';
        $img_url = isset($this->arr_sp_config['img_ftp']) ? $this->arr_sp_config['img_ftp'] : '';
        $img_url = rtrim(rtrim(trim($img_url),'/'),'\\');
        switch ($parent_type)
        {
            case 'Series':
                $arr_map = $this->arr_vod_video_img;
                break;
            case 'Program':
                $arr_map = $this->arr_vod_video_img;
                break;
            case 'Channel':
                $arr_map = $this->arr_channel_img;
                break;
            case 'ScheduleRecord':
                $arr_map = $this->arr_playbill_img;
                break;
        }
        if(!is_array($arr_map) || empty($arr_map))
        {
            return null;
        }
        foreach ($arr_map as $key=>$val)
        {
            if(isset($arr_video[$key]) && strlen($arr_video[$key]) >0)
            {
                $id = md5($key.$arr_video[$key]);
                $picture = array(
                    'FileURL'=>$img_url.'/'.ltrim(ltrim(trim($arr_video[$key]),'/'),'\\'),
                    'Type'=>$val,
                    'Description'=>'',
                );
                $str_picture_obj .= $this->make_object('Picture', $id,$action,$picture);
                $arr_params = array('Type'=>$val);
                if(isset($this->arr_sp_config['c2_import_cdn_mapping_model']) && $this->arr_sp_config['c2_import_cdn_mapping_model'] == '1')
                {
                    $str_picture_mapping .= $this->make_mapping($parent_type, 'Picture', $parent_id, $id, $action,$arr_params);
                }
                else
                {
                    $str_picture_mapping .= $this->make_mapping('Picture', $parent_type, $id, $parent_id, $action,$arr_params);
                }
            }
        }
        return array(
            'picture'=>$str_picture_obj,
            'mapping'=>$str_picture_mapping,
        );
    }
    
    /**
     * 时间转为时分秒
     * @param unknown $seconds
     * @return string
     */
    public function seconds_to_hour($seconds)
    {
        $seconds = intval($seconds) >0 ? intval($seconds) : 0;
        if ($seconds < 60)
        {
            $tt = "00:00:" . sprintf("%02d", intval($seconds % 60));
        }
        if ($seconds >= 60)
        {
            $h = sprintf("%02d", intval($seconds / 60));
            $s = sprintf("%02d", intval($seconds % 60));
            if ($s == 60)
            {
                $s = sprintf("%02d", 0);
                ++ $h;
            }
            $t = "00";
            if ($h == 60)
            {
                $h = sprintf("%02d", 0);
                ++ $t;
            }
            if ($t)
            {
                $t = sprintf("%02d", $t);
            }
            $tt = $t . ":" . $h . ":" . $s;
        }
        if ($seconds >= 60 * 60)
        {
            $t = sprintf("%02d", intval($seconds / 3600));
            $h = sprintf("%02d", intval($seconds / 60) - $t * 60);
            $s = sprintf("%02d", intval($seconds % 60));
            if ($s == 60)
            {
                $s = sprintf("%02d", 0);
                ++ $h;
            }
            if ($h == 60)
            {
                $h = sprintf("%02d", 0);
                ++ $t;
            }
            if ($t)
            {
                $t = sprintf("%02d", $t);
            }
            $tt = $t . ":" . $h . ":" . $s;
        }
        return $seconds > 0 ? $tt : '00:00:00';
    }
    
    
    /**
     * 是否是json字符串
     * @param unknown $string
     */
    public function is_json($string)
    {
        if(!is_string($string))
        {
            return false;
        }
        $string = strlen($string) <1 ? '' : $string;
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
    
    
    /**
     * 获取CP信息
     * @param unknown $cp_id
     */
    public function _get_cp_info($cp_id)
    {
        if(strlen($cp_id) <1)
        {
            return $this->return_data(1, '$cp_id为空');
        }
        if(isset($this->arr_cp_config[$cp_id]))
        {
            return $this->return_data(0, 'ok');
        }
        $result_cp = nl_cp::query_by_id($this->obj_dc,$cp_id);
        if($result_cp['ret'] !=0)
        {
            return $result_cp;
        }
        if(!isset($result_cp['data_info']) || !is_array($result_cp['data_info']) || empty($result_cp['data_info']))
        {
            $this->arr_cp_config[$cp_id] = null;
            return $result_cp;
        }
        if($this->is_json($result_cp['data_info']['nns_config']))
        {
            $result_cp['data_info']['nns_config'] = json_decode($result_cp['data_info']['nns_config'],true);
        }
        $this->arr_cp_config[$cp_id]=$result_cp['data_info'];
        return $result_cp;
    }
    
    /**
     * 获取SP信息
     * @param unknown $sp_id
     */
    public function _get_sp_info($sp_id)
    {
        if(strlen($sp_id) <1)
        {
            return $this->return_data(1, '$sp_id为空');
        }
        if(isset($this->arr_sp_config[$sp_id]))
        {
            return $this->return_data(0, 'ok');
        }
        $result_sp = nl_sp::query_by_id($this->obj_dc,$sp_id);
        if($result_sp['ret'] !=0)
        {
            return $result_sp;
        }
        if(!isset($result_sp['data_info'][0]) || !is_array($result_sp['data_info'][0]) || empty($result_sp['data_info'][0]))
        {
            $this->arr_sp_config[$sp_id] = null;
            return $result_sp;
        }
        if($this->is_json($result_sp['data_info'][0]['nns_config']))
        {
            $result_sp['data_info'][0]['nns_config'] = json_decode($result_sp['data_info'][0]['nns_config'],true);
        }
        $this->arr_sp_config[$sp_id]=$result_sp['data_info'][0];
        return $result_sp;
    }
    
    
    /**
     * 获取所有sP信息
     */
    public function _get_all_sp_info()
    {
        $result_sp = nl_sp::query_all($this->obj_dc);
        if($result_sp['ret'] !=0 || !isset($result_sp['data_info']) || !is_array($result_sp['data_info']) || empty($result_sp['data_info']))
        {
            return $result_sp;
        }
        foreach ($result_sp['data_info'] as $sp_val)
        {
            if($this->is_json($sp_val['nns_config']))
            {
                $sp_val['nns_config'] = json_decode($sp_val['nns_config'],true);
            }
            $this->arr_sp_config[$sp_val['nns_id']] = $sp_val;
        }
        return $result_sp;
    }
    
    
    /**
     * 获取所有CP信息
     * @param unknown $cp_id
     */
    public function _get_all_cp_info()
    {
        $result_cp = nl_cp::query_all($this->obj_dc);
        if($result_cp['ret'] !=0 || !isset($result_cp['data_info']) || !is_array($result_cp['data_info']) || empty($result_cp['data_info']))
        {
            return $result_cp;
        }
        foreach ($result_cp['data_info'] as $cp_val)
        {
            if($this->is_json($cp_val['nns_config']))
            {
                $cp_val['nns_config'] = json_decode($cp_val['nns_config'],true);
            }
            $this->arr_cp_config[$cp_val['nns_id']] = $cp_val;
        }
        return $result_cp;
    }
}