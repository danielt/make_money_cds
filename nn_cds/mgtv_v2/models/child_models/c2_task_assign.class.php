<?php
include_once dirname(dirname(__FILE__)).'/public_model_exec.class.php';
include_once dirname(__FILE__).'/public_model.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/c2_log/c2_log.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
include_once dirname(dirname(__FILE__)).'/queue_task_model.php';
include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/np/np_ftp.php";
/**
 * 公共CDN解析模块
 * @author liangpan
 */
class c2_task_assign
{
	/**
	 * 组装反馈数据
	 * @param number $ret
	 * @param string $reason
	 * @param string $reason_en
	 * @param string $data
	 * @return multitype:number string
	 */
	public function _make_return_data($ret=1,$reason='error',$data=null)
	{
		return array(
				'ret'=>$ret,
				'reason'=>$reason,
				'data_info'=>$data,
		);
	}
	/**
	 * 自动加载模块文件
	 * @param int $task_model CDN上报模块加载方式
	 * @param string $str_func 加载使用方法
	 * @param object $obj_dc DC
	 * @param string $sp_id SPID
	 * @param array  $sp_config SP配置
	 */
	public function auto_load_task_model($task_model,$str_func,$obj_dc=null,$sp_id=null,$sp_config=null,$arr_params=null)
	{
		$cdn_assign = get_config_v2('g_cdn_assign');
		if(!isset($cdn_assign[$task_model]) || empty($cdn_assign[$task_model]))
		{
			return $this->_make_return_data(2,'未查询到CDN注入模式，还是以老逻辑执行CDN注入');
		}
		
		$file_name = 'c2_task_model_' . $cdn_assign[$task_model];
		$file_base_file = dirname(__FILE__) . '/' . $file_name . '.class.php';
		if(!file_exists($file_base_file))
		{
			return $this->_make_return_data(1,$file_name . '方法类文件不存在,路径：' . $file_base_file);
		}
// 		switch ($task_model)
// 		{
// 			case '1':
// 				$file_base_file = $file_base_dir.'/c2_task_model_unicom.class.php';
// 				if(!file_exists($file_base_file))
// 				{
// 					return $this->_make_return_data(1,'c2_task_model_unicom方法类文件不存在,路径：'.$file_base_file);
// 				}
// 				break;
// 			case '2':
// 				$file_base_file = $file_base_dir.'/c2_task_model_msp.class.php';
// 				if(!file_exists($file_base_file))
// 				{
// 					return $this->_make_return_data(1,'c2_task_model_msp方法类文件不存在,路径：'.$file_base_file);
// 				}
// 				break;
// 			case '3':
// 			    $file_base_file = $file_base_dir.'/c2_task_model_jscn_homed.class.php';
// 			    if(!file_exists($file_base_file))
// 			    {
// 			        return $this->_make_return_data(1,'c2_task_model_msp方法类文件不存在,路径：'.$file_base_file);
// 			    }
// 			    break;
// 		    case '4':
// 		        $file_base_file = $file_base_dir.'/c2_task_model_standard.class.php';
// 		        if(!file_exists($file_base_file))
// 		        {
// 		            return $this->_make_return_data(1,'c2_task_model_standard方法类文件不存在,路径：'.$file_base_file);
// 		        }
// 		        break;
// 	        case '5':
// 	            $file_base_file = $file_base_dir.'/c2_task_model_ftp_push.class.php';
// 	            if(!file_exists($file_base_file))
// 	            {
// 	                return $this->_make_return_data(1,'c2_task_model_ftp_push方法类文件不存在,路径：'.$file_base_file);
// 	            }
// 	            break;
// 			default:
// 				return $this->_make_return_data(2,'未查询到CDN注入模式，还是以老逻辑执行CDN注入');
// 		}
		include_once $file_base_file;
		$obj_c2_task = new c2_task_model_v2($obj_dc,$sp_id,$sp_config,$arr_params);
		if(!method_exists($obj_c2_task,$str_func))
		{
			return $this->_make_return_data(1,'C2方法类方法不存在,方法:['.$str_func.']文件路径:'.$file_base_file);
		}
		return $obj_c2_task->$str_func();
	}
}