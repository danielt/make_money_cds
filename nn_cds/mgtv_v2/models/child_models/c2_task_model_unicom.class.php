<?php
/**
 * 中国联通CDN标准规范ADI CDN注入
 * @author liangpan
 */
class c2_task_model_v2 extends nn_public_model
{

	/**
	 * 组装主媒资信息
	 * @param unknown $arr_info 主媒资基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 */
	private function make_vod_xml($arr_info,$arr_info_ex=null)
	{
		$str_Action = $arr_info['nns_deleted'] == 0 ? "REGIST or UPDATE" : "DELETE";
		$str_Domain = $arr_info['nns_filetype'] == 'ts' ? 257 : 264;
		$str_xml ='<Object ElementType="Program" ContentID="'.$arr_info['nns_import_id'].'" UniContentId="'.$arr_info['nns_import_id'].'" Action="'.$str_Action.'">';
		$str_xml.=	'<Property Name="Name">1</Property>';
		$str_xml.= 	'<Property Name="OrderNumber">1</Property>';//0: No DRM 1: BES DRM
		$str_xml.= 	'<Property Name="OriginalName">1</Property>';//0: No DRM  1: BES DRM
		$str_xml.= 	'<Property Name="SortName">1</Property>';//0: 其他 1: Monaural 单声道 2: Stereo 多声道 3: Two-nation monaural 双单声道 4: Two-nation stereo 双多声道  5: AC3(5:1 channel) AC3声道
		$str_xml.= 	'<Property Name="SearchName">1</Property>';//媒体类型1:正片 2:预览片
		$str_xml.= 	'<Property Name="OrgAirDate">1</Property>'; //0: 4x3   1: 16x9(Wide)
		$str_xml.= 	'<Property Name="LicensingWindowStart">1</Property>';//0:无字幕 1:有字幕
		$str_xml.=	'<Property Name="LicensingWindowEnd">'.$arr_info['nns_file_time_len'].'</Property>';
		$str_xml.=	'<Property Name="CPContentID">'.$arr_info['nns_file_size'].'</Property>';
		$str_xml.=	'<Property Name="DisplayAsNew">'.$arr_info['nns_kbps'].'</Property>';
		$str_xml.=	'<Property Name="DisplayAsLastChance"></Property>';
		$str_xml.=	'<Property Name="Macrovision"></Property>';
		$str_xml.=	'<Property Name="Price"></Property>';
		$str_xml.=	'<Property Name="VolumnCount"></Property>';
		$str_xml.=	'<Property Name="Status"></Property>';
		$str_xml.=	'<Property Name="Description">'.$str_Domain.'</Property>';
		$str_xml.=	'<Property Name="ContentProvider"></Property>';
		$str_xml.=	'<Property Name="KeyWords"></Property>';
		$str_xml.=	'<Property Name="SeriesFlag"></Property>';
		$str_xml.=	'<Property Name="ContentProvider"></Property>';
		$str_xml.=	'<Property Name="KeyWords"></Property>';
		$str_xml.=	'<Property Name="Tags"></Property>';
		$str_xml.=	'<Property Name="ViewPoint">'.$str_Domain.'</Property>';
		$str_xml.=	'<Property Name="StarLevel"></Property>';
		$str_xml.=	'<Property Name="Rating"></Property>';
		$str_xml.=	'<Property Name="Awards"></Property>';
		$str_xml.=	'<Property Name="Length"></Property>';
		$str_xml.=	'<Property Name="Reserve1"></Property>';
		$str_xml.=	'<Property Name="Reserve2"></Property>';
		$str_xml.=	'<Property Name="UniContentId"></Property>';
		$str_xml.='</Object>';
		return $str_xml;
	}
	
	/**
	 * 组装分集信息
	 * @param unknown $arr_info 片源基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 */
	private function make_vod_index_xml($arr_info,$arr_info_ex=null)
	{
		$str_Action = $arr_info['nns_deleted'] == 0 ? "REGIST or UPDATE" : "DELETE";
		$str_Domain = $arr_info['nns_filetype'] == 'ts' ? 257 : 264;
		$str_xml ='<Object ElementType="Program" ContentID="'.$arr_info['nns_import_id'].'" Action="'.$str_Action.'">';
		$str_xml.=	'<Property Name="Name">1</Property>';
		$str_xml.=	'<Property Name="CPContentID">'.$arr_info['nns_url'].'</Property>';
		$str_xml.= 	'<Property Name="OrderNumber">1</Property>';//0: No DRM 1: BES DRM
		$str_xml.= 	'<Property Name="OriginalName">1</Property>';//0: No DRM  1: BES DRM
		$str_xml.= 	'<Property Name="SortName">1</Property>';//0: 其他 1: Monaural 单声道 2: Stereo 多声道 3: Two-nation monaural 双单声道 4: Two-nation stereo 双多声道  5: AC3(5:1 channel) AC3声道
		$str_xml.= 	'<Property Name="SearchName">1</Property>';//媒体类型1:正片 2:预览片
		$str_xml.= 	'<Property Name="ActorDisplay">1</Property>'; //0: 4x3   1: 16x9(Wide)
		$str_xml.= 	'<Property Name="OriginalCountry">1</Property>';//0:无字幕 1:有字幕
		$str_xml.=	'<Property Name="Language">'.$arr_info['nns_file_time_len'].'</Property>';
		$str_xml.=	'<Property Name="ReleaseYear">'.$arr_info['nns_file_size'].'</Property>';
		$str_xml.=	'<Property Name="OrgAirDate">'.$arr_info['nns_kbps'].'</Property>';
		$str_xml.=	'<Property Name="LicensingWindowStart"></Property>';
		$str_xml.=	'<Property Name="LicensingWindowEnd"></Property>';
		$str_xml.=	'<Property Name="DisplayAsLastChance"></Property>';
		$str_xml.=	'<Property Name="Macrovision"></Property>';
		$str_xml.=	'<Property Name="Description"></Property>';
		$str_xml.=	'<Property Name="PriceTaxIn">'.$str_Domain.'</Property>';
		$str_xml.=	'<Property Name="Status"></Property>';
		$str_xml.=	'<Property Name="SourceType"></Property>';
		$str_xml.=	'<Property Name="SeriesFlag"></Property>';
		$str_xml.=	'<Property Name="ContentProvider"></Property>';
		$str_xml.=	'<Property Name="KeyWords"></Property>';
		$str_xml.=	'<Property Name="Tags"></Property>';
		$str_xml.=	'<Property Name="ViewPoint">'.$str_Domain.'</Property>';
		$str_xml.=	'<Property Name="StarLevel"></Property>';
		$str_xml.=	'<Property Name="Rating"></Property>';
		$str_xml.=	'<Property Name="Awards"></Property>';
		$str_xml.=	'<Property Name="Length"></Property>';
		$str_xml.=	'<Property Name="ProgramType"></Property>';
		$str_xml.=	'<Property Name="Reserve1"></Property>';
		$str_xml.=	'<Property Name="Reserve2"></Property>';
		$str_xml.=	'<Property Name="Reserve3"></Property>';
		$str_xml.=	'<Property Name="Reserve4"></Property>';
		$str_xml.=	'<Property Name="Reserve5"></Property>';
		$str_xml.=	'<Property Name="UniContentId"></Property>';
		$str_xml.='</Object>';
		return $str_xml;
	}
	
	/**
	 * 组装片源信息
	 * @param unknown $arr_info 片源基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 */
	private function make_vod_media_xml($arr_info,$arr_info_ex=null)
	{
		$str_Action = $arr_info['nns_deleted'] == 0 ? "REGIST or UPDATE" : "DELETE";
		$str_Domain = $arr_info['nns_filetype'] == 'ts' ? 257 : 264;
		$str_xml ='<Object ElementType="Movie" PhysicalContentID="'.$arr_info['nns_cdn_media_guid'].'" Action="'.$str_Action.'" Type="1">';
		$str_xml.=	'<Property Name="Type">1</Property>';
		$str_xml.=	'<Property Name="FileURL">'.$arr_info['nns_url'].'</Property>';
		$str_xml.=	'<Property Name="CPContentID">'.$arr_info['nns_cp_id'].'</Property>';
		$str_xml.= 	'<Property Name="SourceDRMType">1</Property>';//0: No DRM 1: BES DRM
		$str_xml.= 	'<Property Name="DestDRMType">1</Property>';//0: No DRM  1: BES DRM
		$str_xml.= 	'<Property Name="AudioType">1</Property>';//0: 其他 1: Monaural 单声道 2: Stereo 多声道 3: Two-nation monaural 双单声道 4: Two-nation stereo 双多声道  5: AC3(5:1 channel) AC3声道
		$str_xml.= 	'<Property Name="ScreenFormat">1</Property>'; //0: 4x3   1: 16x9(Wide)
		$str_xml.= 	'<Property Name="ClosedCaptioning">1</Property>';//0:无字幕 1:有字幕
		$str_xml.=	'<Property Name="Duration">'.$arr_info['nns_file_time_len'].'</Property>';
		$str_xml.=	'<Property Name="FileSize">'.$arr_info['nns_file_size'].'</Property>';
		$str_xml.=	'<Property Name="BitRateType">'.$arr_info['nns_kbps'].'</Property>';
		$str_xml.=	'<Property Name="VideoType">1</Property>';
		$str_xml.=	'<Property Name="AudioEncodingType"></Property>';
		$str_xml.=	'<Property Name="Resolution"></Property>';
		$str_xml.=	'<Property Name="VideoProfile"></Property>';
		$str_xml.=	'<Property Name="SystemLayer"></Property>';
		$str_xml.=	'<Property Name="Domain">'.$str_Domain.'</Property>';
		$str_xml.=	'<Property Name="Hotdegree"></Property>';
		$str_xml.='</Object>';
		return $str_xml;
	}
	
	
	/**
	 * 组装直播频道信息
	 * @param unknown $arr_info 片源基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 */
	private function make_live_xml($arr_info,$arr_info_ex=null)
	{
		$str_Action = $arr_info['nns_deleted'] == 0 ? "REGIST or UPDATE" : "DELETE";
		$str_Domain = $arr_info['nns_filetype'] == 'ts' ? 257 : 264;
		$str_xml ='<Object ElementType="Program" ContentID="'.$arr_info['nns_import_id'].'" Action="'.$str_Action.'">';
		$str_xml.=	'<Property Name="ChannelNumber">1</Property>';
		$str_xml.=	'<Property Name="Name">'.$arr_info['nns_url'].'</Property>';
		$str_xml.= 	'<Property Name="CallSign">1</Property>';//0: No DRM 1: BES DRM
		$str_xml.= 	'<Property Name="TimeShift">1</Property>';//0: No DRM  1: BES DRM
		$str_xml.= 	'<Property Name="Type">1</Property>';//0: 其他 1: Monaural 单声道 2: Stereo 多声道 3: Two-nation monaural 双单声道 4: Two-nation stereo 双多声道  5: AC3(5:1 channel) AC3声道
		$str_xml.= 	'<Property Name="Status">1</Property>';//媒体类型1:正片 2:预览片
		$str_xml.= 	'<Property Name="StartTime">1</Property>'; //0: 4x3   1: 16x9(Wide)
		$str_xml.= 	'<Property Name="EndTime">1</Property>';//0:无字幕 1:有字幕
		$str_xml.=	'<Property Name="CPContentID">'.$arr_info['nns_file_time_len'].'</Property>';
		$str_xml.=	'<Property Name="StorageDuration">'.$arr_info['nns_file_size'].'</Property>';
		$str_xml.=	'<Property Name="TimeShiftDuration">'.$arr_info['nns_kbps'].'</Property>';
		$str_xml.=	'<Property Name="Country"></Property>';
		$str_xml.=	'<Property Name="State"></Property>';
		$str_xml.=	'<Property Name="City"></Property>';
		$str_xml.=	'<Property Name="ZipCode"></Property>';
		$str_xml.=	'<Property Name="URL"></Property>';
		$str_xml.=	'<Property Name="SubType">'.$str_Domain.'</Property>';
		$str_xml.=	'<Property Name="Language"></Property>';
		$str_xml.=	'<Property Name="Macrovision"></Property>';
		$str_xml.=	'<Property Name="VideoType"></Property>';
		$str_xml.=	'<Property Name="AudioType"></Property>';
		$str_xml.=	'<Property Name="StreamType"></Property>';
		$str_xml.=	'<Property Name="Bilingual"></Property>';
		$str_xml.=	'<Property Name="URL">'.$str_Domain.'</Property>';
		$str_xml.=	'<Property Name="UniContentId"></Property>';
		$str_xml.='</Object>';
		return $str_xml;
	}
	
	/**
	 * 组装片源信息
	 * @param unknown $arr_live_media 片源基本信息
	 * @param string $arr_live_media_ex 扩展数据
	 */
	private function make_live_media_xml($arr_live_media,$arr_live_media_ex=null)
	{
		$str_Action = $arr_live_media['nns_deleted'] == 0 ? "REGIST or UPDATE" : "DELETE";
		$str_DestCastType = $str_SrcCastType = ($arr_live_media['nns_cast_type'] == 1) ? 'multicast' : 'unicast';
		$str_MultiCastIP = '';
		$str_MultiCastPort = '';
		$str_UnicastUrl = '';
		if($str_SrcCastType == 'multicast')
		{
			$arr_live_media['nns_url'] = trim($arr_live_media['nns_url']);
			$udp_url = preg_replace('/^udp/i', '', $arr_live_media['nns_url']);
			$udp_url = trim(ltrim(ltrim(ltrim(ltrim($udp_url,':'),'/'),'/'),'@'));
			$arr_udp = explode(':',$udp_url);
			$str_MultiCastIP = $arr_udp[0];
			$str_MultiCastPort = $arr_udp[1];
		}
		else
		{
			$str_UnicastUrl = $arr_live_media['nns_url'];
		}
// 		$str_Domain = ($arr_live_media['nns_cast_type'] == 1 || $arr_live_media['nns_cast_type'] == 0) ? 264 : 264;
		$str_Domain = $arr_live_media['nns_filetype'] == 'ts' ? 257 : 264;
		$str_BitRateType = (strtoupper($arr_live_media['nns_mode'])=="HD") ? 4 : 2;
		$str_xml ='<Object ElementType="PhysicalChannel" PhysicalContentID="'.$arr_live_media['nns_cdn_media_guid'].'" Action="'.$str_Action.'">';
		$str_xml.=	'<Property Name="DestCastType">'.$str_DestCastType.'</Property>';
		$str_xml.=	'<Property Name="SrcCastType">'.$str_SrcCastType.'</Property>';
		$str_xml.= 	'<Property Name="ChannelID">'.$arr_live_media_ex['nns_cdn_channel_guid'].'</Property>';//0: No DRM 1: BES DRM
		$str_xml.= 	'<Property Name="BitRateType">'.$str_BitRateType.'</Property>';//0: No DRM  1: BES DRM
		$str_xml.= 	'<Property Name="CPContentID">'.$arr_live_media['nns_cp_id'].'</Property>';//0: 其他 1: Monaural 单声道 2: Stereo 多声道 3: Two-nation monaural 双单声道 4: Two-nation stereo 双多声道  5: AC3(5:1 channel) AC3声道
		$str_xml.= 	'<Property Name="MultiCastIP">'.$str_MultiCastIP.'</Property>';//媒体类型1:正片 2:预览片
		$str_xml.= 	'<Property Name="MultiCastPort">'.$str_MultiCastPort.'</Property>'; //0: 4x3   1: 16x9(Wide)
		if($str_SrcCastType == 'multicast')
		{
		    $str_xml.= 	'<Property Name="UnicastUrl"/>';//0:无字幕 1:有字幕
		}
		else
		{
		    $str_xml.= 	'<Property Name="UnicastUrl"><![CDATA['.$str_UnicastUrl.']]></Property>';//0:无字幕 1:有字幕
		}
		$str_xml.=	'<Property Name="VideoType">8</Property>';
		$str_xml.=	'<Property Name="AudioType"></Property>';
		$str_xml.=	'<Property Name="Resolution">'.$arr_live_media['nns_kbps'].'</Property>';
		$str_xml.=	'<Property Name="VideoProfile"></Property>';
		$str_xml.=	'<Property Name="SystemLayer"></Property>';
		$str_xml.=	'<Property Name="Domain">'.$str_Domain.'</Property>';
		$str_xml.=	'<Property Name="Hotdegree"></Property>';
		$str_xml.= 	'<Property Name="TimeShift">'.$arr_live_media['nns_timeshift_status'].'</Property>';//0: No DRM  1: BES DRM
		$str_xml.= 	'<Property Name="TimeShiftDuration">'.$arr_live_media['nns_timeshift_delay'].'</Property>';//0: 其他 1: Monaural 单声道 2: Stereo 多声道 3: Two-nation monaural 双单声道 4: Two-nation stereo 双多声道  5: AC3(5:1 channel) AC3声道
		$str_xml.= 	'<Property Name="TvodStatus">'.$arr_live_media['nns_storage_status'].'</Property>';//媒体类型1:正片 2:预览片
		$str_xml.= 	'<Property Name="StorageDuration">'.$arr_live_media['nns_storage_delay'].'</Property>'; //0: 4x3   1: 16x9(Wide)
		$str_xml.='</Object>';
		return $str_xml;
	}
	
	/**
	 * 组装节目单信息
	 * @param unknown $arr_live_media 片源基本信息
	 * @param string $arr_live_media_ex 扩展数据
	 */
	private function make_playbill_xml($arr_playbill,$arr_playbill_ex=null)
	{
		$str_Action = $arr_playbill['nns_state'] == 0 ? "REGIST or UPDATE" : "DELETE";
		$str_StartDate = str_replace('-', '', trim(substr($arr_playbill['nns_begin_time'], 0,10)));
		$str_StartTime = str_replace(':', '', trim(substr($arr_playbill['nns_begin_time'], 11,8)));
		$str_time_len = gmstrftime("%H%M%S",$arr_playbill['nns_time_len']);
		$str_xml ='<Object ElementType="ScheduleRecord" PhysicalContentID="'.$arr_playbill['nns_cdn_playbill_guid'].'" ScheduleId="'.$arr_playbill['nns_cdn_playbill_guid'].'" PhysicalChannelID="'.$arr_playbill['nns_cdn_live_media_guid'].'" Action="'.$str_Action.'">';
		$str_xml.=	'<Property Name="StartDate">'.$str_StartDate.'</Property>';
		$str_xml.=	'<Property Name="StartTime">'.$str_StartTime.'</Property>';
		$str_xml.= 	'<Property Name="Duration">'.$str_time_len.'</Property>';//0: No DRM 1: BES DRM
		$str_xml.= 	'<Property Name="CPContentID">'.$arr_playbill['nns_cp_id'].'</Property>';//0: No DRM  1: BES DRM
		$str_xml.= 	'<Property Name="Description">'.$arr_playbill['nns_summary'].'</Property>';//0: 其他 1: Monaural 单声道 2: Stereo 多声道 3: Two-nation monaural 双单声道 4: Two-nation stereo 双多声道  5: AC3(5:1 channel) AC3声道
		$str_xml.= 	'<Property Name="Domain">257</Property>';//媒体类型1:正片 2:预览片
		$str_xml.= 	'<Property Name="HotDgree"></Property>'; //0: 4x3   1: 16x9(Wide)
		$str_xml.='</Object>';
		return $str_xml;
	}
	
	/**
	 * 片源CDN
	 * @param unknown $movie_id
	 */
	public function do_media()
	{
		if(!isset($this->arr_params['nns_media_id']) || empty($this->arr_params['nns_media_id']))
		{
			return $this->_make_return_data(1,'点播传入参数无nns_media_id字段,参数为:'.var_export($this->arr_params));
		}
		$movie_id = $this->arr_params['nns_media_id'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
		$result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $movie_id);
		if($result_media['ret'] !=0)
		{
			return $result_media;
		}
		if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
		{
			nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
			return $result_media;
		}
		$result_media = $result_media['data_info'];
		$result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $movie_id,$result_media);
		if(isset($this->arr_sp_config['media_ftp']) && !empty($this->arr_sp_config['media_ftp']))
		{
			$this->arr_sp_config['media_ftp'] = rtrim((trim($this->arr_sp_config['media_ftp'])),'/');
			$result_media['nns_url'] = ltrim((trim($result_media['nns_url'])),'/');
			if (stripos($result_media['nns_url'], 'http://') === FALSE && stripos($result_media['nns_url'], 'ftp://') === FALSE)
			{
				$result_media['nns_url'] = $this->arr_sp_config['media_ftp'].'/'.$result_media['nns_url'];
			}
		}
		if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
		{
			return $result_media_import_info;
		}
		$result_media['nns_cdn_media_guid'] = $result_media_import_info['data_info'];
		$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
		$xml_str .= 	'<Objects>';
		$xml_str .=     $this->make_vod_media_xml($result_media);
		$xml_str .= 	'</Objects>';
		$xml_str .= '</ADI>';
		$action = ($result_media['nns_deleted'] !=1) ? ($result_media['nns_modify_time'] > $result_media['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
		$file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'vod_media' . '_' . $action . '_' .  rand(100, 999) . '.xml';
		$c2_info = array(
				'nns_task_type'=>'Movie',
				'nns_task_id'=> isset($this->arr_params['nns_task_id'])?$this->arr_params['nns_task_id']:null,
				'nns_task_name'=> isset($this->arr_params['nns_task_name'])?$this->arr_params['nns_task_name']:null,
				'nns_action'=>	$action,
				'nns_url' => $file_name,
				'nns_content' => $xml_str,
				'nns_desc' => 'Movie,'.$action,
		);
		return $this->execute_c2($c2_info);
	}
	
	/**
	 * 直播片源CDN
	 * @param unknown $movie_id
	 */
	public function do_live_media()
	{
		if(!isset($this->arr_params['nns_channel_id']) && !empty($this->arr_params['nns_channel_id']))
		{
			return array(
					'ret'=>1,
					'reason'=>'直播片源传入参数无nns_channel_id字段,参数为:'.var_export($this->arr_params),
			);
		}
		$channel_id = $this->arr_params['nns_channel_id'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/live.class.php';
		$result_channel = nl_live::query_by_id($this->obj_dc, $channel_id);
		if($result_channel['ret'] !=0 || !isset($result_channel['data_info']) || !is_array($result_channel['data_info']) || empty($result_channel['data_info']))
		{
			return $result_channel;
		}
		$result_channel = $result_channel['data_info'];
		$result_channel_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'live', $channel_id,$result_channel);
		if($result_channel_import_info['ret'] !=0 || !isset($result_channel_import_info['data_info']) || strlen($result_channel_import_info['data_info']) <1)
		{
			return $result_channel_import_info;
		}
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/live_media.class.php';
		$result_media = nl_live_media::query_by_channel_id($this->obj_dc, $channel_id);
		if($result_media['ret'] !=0 || !isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
		{
			return $result_media;
		}
		$result_media = $result_media['data_info'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/c2_task/c2_task.class.php';
		foreach ($result_media as $media_list)
		{
			$result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'live_media', $media_list['nns_id'],$media_list);
			if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
			{
				return $result_media_import_info;
			}
			$media_list['nns_cdn_media_guid'] = $result_media_import_info['data_info'];
			$xml_str='';
			$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
			$xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
			$xml_str .= 	'<Objects>';
			$xml_str .=     $this->make_live_media_xml($media_list,array('nns_cdn_channel_guid'=>$result_channel_import_info['data_info']));
			$xml_str .= 	'</Objects>';
			$xml_str .= '</ADI>';
			$action = ($result_media['nns_deleted'] !=1) ? ($result_media['nns_modify_time'] > $result_media['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
			$str_action = ($result_media['nns_deleted'] !=1) ? ($result_media['nns_modify_time'] > $result_media['nns_create_time']) ? 'modify' : 'add' : 'destroy';
			$file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'live_media' . '_' . $action . '_' .  rand(100, 999) . '.xml';
			
			$str_task_id = np_guid_rand();
			$result_c2_exsist=nl_c2_task::query_c2_exsist($this->obj_dc,'live_media',$media_list['nns_id'],$this->str_sp_id);
			if($result_c2_exsist['ret'] !=0)
			{
				return $result_c2_exsist;
			}
			$result_c2_exsist = isset($result_c2_exsist['data_info'][0]) ? $result_c2_exsist['data_info'][0] : null;
			if(is_array($result_c2_exsist) && !empty($result_c2_exsist))
			{
				$str_task_id = $result_c2_exsist['nns_id'];
				$result_c2_edit=nl_c2_task::edit($this->obj_dc, array('nns_action'=>$str_action,'nns_status'=>1,'nns_epg_status'=>97),$str_task_id);
				if($result_c2_edit['ret'] !=0)
				{
					return $result_c2_edit;
				}
			}
			else
			{
				$arr_c2_add = array(
						'nns_id'=>$str_task_id,
						'nns_type'=>'live_media',
						'nns_name'=>"[{$result_channel['nns_name']}] 1 [{$media_list['nns_mode']}]-直播流",
						'nns_ref_id'=>'',
						'nns_action'=>$str_action,
						'nns_status'=>1,
						'nns_org_id'=>$this->str_sp_id,
						'nns_category_id'=>'',
						'nns_src_id'=>$media_list['nns_id'],
						'nns_all_index'=>1,
						'nns_clip_task_id'=>'',
						'nns_clip_date'=>'',
						'nns_op_id'=>'',
						'nns_epg_status'=>97,
						'nns_ex_url'=>'',
						'nns_file_path'=>'',
						'nns_file_size'=>'',
						'nns_file_md5'=>'',
						'nns_cdn_policy'=>'',
						'nns_epg_fail_time'=>0,
						'nns_message_id'=>'',
				);
				$result_c2_add=nl_c2_task::add($this->obj_dc, $arr_c2_add);
				if($result_c2_edit['ret'] !=0)
				{
					return $result_c2_add;
				}
			}
			
			$c2_info = array(
					'nns_task_type'=>'PhysicalChannel',
					'nns_task_id'=> $str_task_id,
					'nns_task_name'=> "[{$result_channel['nns_name']}] 1 [{$media_list['nns_mode']}]-直播流",
					'nns_action'=>	$action,
					'nns_url' => $file_name,
					'nns_content' => $xml_str,
					'nns_desc' => 'PhysicalChannel,'.$action,
			);
			return $this->execute_c2($c2_info);
		}
	}
	
	
	/**
	 * 直播节目单CDN
	 * @param unknown $movie_id
	 */
	public function do_playbill()
	{
		if(!isset($this->arr_params['nns_playbill_id']) && !empty($this->arr_params['nns_playbill_id']))
		{
			return array(
					'ret'=>1,
					'reason'=>'节目单传入参数无nns_playbill_id字段,参数为:'.var_export($this->arr_params),
			);
		}
		$playbill_id = $this->arr_params['nns_playbill_id'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/live_media.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/playbill.class.php';
		$result_playbill = nl_playbill::query_by_id($this->obj_dc, $playbill_id);
		if($result_playbill['ret'] !=0 || !isset($result_playbill['data_info']) || !is_array($result_playbill['data_info']) || empty($result_playbill['data_info']))
		{
			return $result_playbill;
		}
		$result_playbill = $result_playbill['data_info'];
		$result_live_media = nl_live_media::query_by_id($this->obj_dc, $result_playbill['nns_live_media_id']);
		if($result_live_media['ret'] !=0 || !isset($result_live_media['data_info']) || !is_array($result_live_media['data_info']) || empty($result_live_media['data_info']))
		{
			return $result_live_media;
		}
		$result_live_media = $result_live_media['data_info'];
		$result_playbill_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'playbill', $playbill_id,$result_playbill);
		if($result_playbill_import_info['ret'] !=0 || !isset($result_playbill_import_info['data_info']) || strlen($result_playbill_import_info['data_info']) <1)
		{
			return $result_playbill_import_info;
		}
		$result_live_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'live_media', $playbill_id,$result_live_media);
		if($result_live_media_import_info['ret'] !=0 || !isset($result_live_media_import_info['data_info']) || strlen($result_live_media_import_info['data_info']) <1)
		{
			return $result_live_media_import_info;
		}
		$result_playbill['nns_cdn_playbill_guid'] = $result_playbill_import_info['data_info'];
		$result_playbill['nns_cdn_live_media_guid'] = $result_live_media_import_info['data_info'];
		$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
		$xml_str .= 	'<Objects>';
		$xml_str .=     $this->make_playbill_xml($result_playbill);
		$xml_str .= 	'</Objects>';
		$xml_str .= '</ADI>';
		
		$action = ($result_playbill['nns_state'] !=1) ? ($result_playbill['nns_modify_time'] > $result_playbill['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
		$str_action = ($result_playbill['nns_state'] !=1) ? ($result_playbill['nns_modify_time'] > $result_playbill['nns_create_time']) ? 'modify' : 'add' : 'destroy';
		$file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'playbill' . '_' . $action . '_' .  rand(100, 999) . '.xml';
		$str_task_id = np_guid_rand();
		$result_c2_exsist=nl_c2_task::query_c2_exsist($this->obj_dc,'playbill',$result_playbill['nns_id'],$this->str_sp_id);
		if($result_c2_exsist['ret'] !=0)
		{
			return $result_c2_exsist;
		}
		$result_c2_exsist = isset($result_c2_exsist['data_info'][0]) ? $result_c2_exsist['data_info'][0] : null;
		if(is_array($result_c2_exsist) && !empty($result_c2_exsist))
		{
			$str_task_id = $result_c2_exsist['nns_id'];
			$result_c2_edit=nl_c2_task::edit($this->obj_dc, array('nns_action'=>$str_action,'nns_status'=>1,'nns_epg_status'=>97),$str_task_id);
			if($result_c2_edit['ret'] !=0)
			{
				return $result_c2_edit;
			}
		}
		else
		{
			$arr_c2_add = array(
					'nns_id'=>$str_task_id,
					'nns_type'=>'playbill',
					'nns_name'=>"[{$result_playbill['nns_name']}]-节目单",
					'nns_ref_id'=>$result_playbill['nns_live_media_id'],
					'nns_action'=>$str_action,
					'nns_status'=>1,
					'nns_org_id'=>$this->str_sp_id,
					'nns_category_id'=>'',
					'nns_src_id'=>$result_playbill['nns_id'],
					'nns_all_index'=>1,
					'nns_clip_task_id'=>'',
					'nns_clip_date'=>'',
					'nns_op_id'=>'',
					'nns_epg_status'=>97,
					'nns_ex_url'=>'',
					'nns_file_path'=>'',
					'nns_file_size'=>'',
					'nns_file_md5'=>'',
					'nns_cdn_policy'=>'',
					'nns_epg_fail_time'=>0,
					'nns_message_id'=>'',
			);
			$result_c2_add=nl_c2_task::add($this->obj_dc, $arr_c2_add);
			if($result_c2_edit['ret'] !=0)
			{
				return $result_c2_add;
			}
		}
			
		$c2_info = array(
				'nns_task_type'=>'Schedule',
				'nns_task_id'=> $str_task_id,
				'nns_task_name'=> "[{$result_playbill['nns_name']}]-节目单",
				'nns_action'=>	$action,
				'nns_url' => $file_name,
				'nns_content' => $xml_str,
				'nns_desc' => 'Schedule,'.$action,
		);
		return $this->execute_c2($c2_info);
	}
}
