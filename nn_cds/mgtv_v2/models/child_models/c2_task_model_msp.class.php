<?php
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/message/nl_message.class.php';
/**
 * 视达科MSP注入
 * @author feijian.gao
 */
class c2_task_model_v2 extends nn_public_model
{
    public $obj_dom = null;
	/**
	 * 片源CDN
	 * @param unknown $movie_id
	 */
	public function do_media()
	{
		if(!isset($this->arr_params['nns_media_id']) || empty($this->arr_params['nns_media_id']))
		{
			return $this->_make_return_data(1,'点播传入参数无nns_media_id字段,参数为:'.var_export($this->arr_params));
		}
		if(!isset($this->arr_params['nns_task_id']) || empty($this->arr_params['nns_task_id']))
		{
		    return $this->_make_return_data(1,'点播传入参数无nns_task_id字段,参数为:'.var_export($this->arr_params));
		}
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/c2_task/c2_task.class.php';
		$result_c2_task=nl_c2_task::query_data_by_id($this->obj_dc, $this->arr_params['nns_task_id']);
		if($result_c2_task['ret'] !=0)
		{
		    return $result_c2_task;
		}
		if(!isset($result_c2_task['data_info'][0]) || empty($result_c2_task['data_info'][0]) || !is_array($result_c2_task['data_info'][0]))
		{
		    return $this->_make_return_data(1,'未查询到c2标数据'.var_export($result_c2_task['data_info'],true));
		}
		$movie_id = $this->arr_params['nns_media_id'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
		$result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $movie_id);
		if($result_media['ret'] !=0)
		{
			return $result_media;
		}
		if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
		{
			nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
			return $result_media;
		}
		$result_media = $result_media['data_info'];
		
		//获取片源 注入ID
		$result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $result_media['nns_id'],$result_media,$this->arr_sp_config);
		if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
		{
		    return $result_media_import_info;
		}
		$result_media['nns_media_import_id'] = $result_media_import_info['data_info'];
		
		$result_media['name'] = $result_c2_task['data_info'][0]['nns_name'];
		$result_media['cdn_policy_id'] = strlen($result_c2_task['data_info'][0]['nns_cdn_policy']) >0 ? $result_c2_task['data_info'][0]['nns_cdn_policy'] : 'cdn1';
		
		$result_media['file_path'] = $result_c2_task['data_info'][0]['nns_file_path'];
		$str_action = ($result_media['nns_deleted'] == 0) ? 'regist' : 'delete';
		$str_action_1 = ($result_media['nns_deleted'] ==0) ? ($result_media['nns_modify_time'] > $result_media['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
		
		$arr_message_request_data = $this->query_message_info($result_media['nns_cp_id']);

		//获取cp配置里的外部源id   -->start
		$arr_cp_config = nl_cp::get_cp_config($this->obj_dc,$result_media['nns_cp_id']);
		if(strlen($arr_cp_config['data_info']['nns_config']["clip_custom_origin_id"])>0)
		{
			$result_media["clip_custom_origin_id"] = $arr_cp_config['data_info']['nns_config']["clip_custom_origin_id"];
		}
		if(isset($arr_cp_config['data_info']['nns_config']["import_msp_cdn_policy"]) && strlen($arr_cp_config['data_info']['nns_config']["import_msp_cdn_policy"]) >0)
		{
		    $temp_import_msp_cdn_policy = trim($arr_cp_config['data_info']['nns_config']["import_msp_cdn_policy"]);
		    $result_media['cdn_policy_id'] = strlen($temp_import_msp_cdn_policy) > 0 ? $temp_import_msp_cdn_policy : $result_media['cdn_policy_id'];
		}
		else if(evn::get("project") == 'telecom' && $result_media['cdn_policy_id'] == 'cdn1')
		{
		    $result_media['cdn_policy_id'] = '';
		}
		unset($arr_cp_config);
		//获取cp配置里的外部源id   -->end
		$result = import_model::import_cdn_vod_media($result_media,$this->str_sp_id,$str_action_1,$arr_message_request_data);
		if($result['ret'] !=0)
		{
		    $result['ret'] =1;
		    nl_c2_task::edit($this->obj_dc, array('nns_status'=>'-1'), $this->arr_params['nns_task_id']);
		}
		else
		{
		    $result['ret'] =0;
		    nl_c2_task::edit($this->obj_dc, array('nns_status'=>'0'), $this->arr_params['nns_task_id']);
		}
		$three_rand = np_guid_rand();
		$c2_task_log = array(
		    'nns_id' => $three_rand,
		    'nns_type' => 'std',
		    'nns_org_id' => $this->str_sp_id,
		    'nns_task_type' => 'Movie',
		    'nns_task_id' => $this->arr_params['nns_task_id'],
		    'nns_task_name' => $result_c2_task['data_info'][0]['nns_name'],
		    'nns_action' => $str_action_1,
		    'nns_notify_result_url' => $three_rand.'.txt',
		    'nns_desc' => 'Movie,'.$str_action_1,
		    'nns_again' => 0,
		    'nns_url' => $three_rand. '.xml',
		);
		
		$base_info = array(
		    'nns_cp_id'=>$result_media['nns_cp_id'],
		    'nns_import_id'=>$result_media['nns_import_id'],
		    'nns_url'=>$result_media['nns_url'],
		    'nns_ext_url'=>$result_media['nns_ext_url'],
		);

		$this->execute_c2_msp($c2_task_log,$result,$base_info,$result_media);
		return $result;
	}

	/**
	 * 直播片源CDN
	 * @param unknown $movie_id
	 */
	public function do_live_media()
	{
		if(!isset($this->arr_params['nns_media_id']) && !empty($this->arr_params['nns_media_id']))
		{
		    nn_log::write_log_message(LOG_MODEL_MSP,'直播片源传入参数无nns_media_id字段,参数为:'.var_export($this->arr_params,true),$this->str_sp_id,'live_media');
			return array(
					'ret'=>1,
					'reason'=>'直播片源传入参数无nns_channel_id字段,参数为:'.var_export($this->arr_params),
			);
		}
		
		$array_media = nl_live_media::query_by_id($this->obj_dc, $this->arr_params['nns_media_id']);
		if($array_media['ret'] !=0)
		{
		    return $array_media;
		}
		if(!isset($array_media['data_info']) || !is_array($array_media['data_info']) || empty($array_media['data_info']))
		{
		    return $array_media;
		}
		$media_val = $array_media['data_info'];
		
		$array_live = nl_live::query_by_id($this->obj_dc, $media_val['nns_live_id']);
		if($array_live['ret'] !=0)
		{
		    return $array_live;
		}
		if(!isset($array_live['data_info']) || !is_array($array_live['data_info']) || empty($array_live['data_info']))
		{
		    return $this->_make_return_data(1,'点播传入参数无nns_task_id字段,参数为:'.var_export($this->arr_params));
		}
		$array_live = $array_live['data_info'];
		
		$arr_all_index = nl_live_index::query_by_ids($this->obj_dc, $media_val['nns_live_index_id']);
		if($arr_all_index['ret'] !=0)
		{
		    return $array_live;
		}
		if(!isset($arr_all_index['data_info']) || !is_array($arr_all_index['data_info']) || empty($arr_all_index['data_info']))
		{
		    return $this->_make_return_data(1,'点播传入参数无nns_task_id字段,参数为:'.var_export($this->arr_params));
		}
		$arr_all_index = $arr_all_index['data_info'];
		
	    $result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'live_media', $media_val['nns_id'],$media_val,$this->arr_sp_config);
	    if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
	    {
	        return $result_media_import_info;
	    }
	    $media_val['nns_content_id'] = $result_media_import_info['data_info'];
	    $str_action = ($media_val['nns_deleted'] ==0) ? ($media_val['nns_modify_time'] > $media_val['nns_create_time']) ? 'modify' : 'add' : 'destroy';
	    $str_action_1 = ($media_val['nns_deleted'] ==0) ? ($media_val['nns_modify_time'] > $media_val['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
	    $arr_all_index['nns_index']++;
	    $arr_c2_info = array(
	        'nns_name'=>"[{$array_live['nns_name']}] [第" . $arr_all_index['nns_index'] . "集] [{$media_val['nns_mode']}]",
	    );
	    $arr_cp_config = nl_cp::query_by_id($this->obj_dc, $media_val['nns_cp_id']);
	    $arr_cp_config = json_decode($arr_cp_config['data_info']['nns_config'],true);
	    
	    $result_producer = nl_producer::query_by_cp_source_domain_id($this->obj_dc, 'live_media', $media_val['nns_cp_id'], $media_val['nns_import_source'], $media_val['nns_domain']);
	    
	    $arr_producer = (isset($result_producer['data_info']) && is_array($result_producer['data_info']) && !empty($result_producer['data_info'])) ? $result_producer['data_info'] : null;
	    
	    $arr_message_request_data = $this->query_message_info($media_val['nns_cp_id']);
	    
	    //MSP注入
	    $re_msp = import_model::import_cdn_live_data($array_live,$media_val,$str_action,$arr_cp_config,$arr_producer,$arr_message_request_data);
	    if($re_msp['ret'] !=0)
	    {
	        $re_msp['ret'] =1;
	        nl_c2_task::edit($this->obj_dc, array('nns_status'=>'-1'), $this->arr_params['nns_task_id']);
	    }
	    else
	    {
	        nl_c2_task::edit($this->obj_dc, array('nns_status'=>'0'), $this->arr_params['nns_task_id']);
	    }
	    $three_rand = np_guid_rand();
	    $c2_task_log = array(
	        'nns_id' => $three_rand,
	        'nns_type' => 'std',
	        'nns_org_id' => $this->str_sp_id,
	        'nns_task_type' => 'PhysicalChannel',
	        'nns_task_id' => $this->arr_params['nns_task_id'],
	        'nns_task_name' => $arr_c2_info['nns_name'],
	        'nns_action' => $str_action_1,
	        'nns_notify_result_url' => $three_rand.'.txt',
	        'nns_desc' => 'PhysicalChannel,'.$str_action_1,
	        'nns_again' => 0,
	        'nns_url' => $three_rand. '.xml',
	    );
	    
	    $base_info = array(
	        'nns_cp_id'=>$media_val['nns_cp_id'],
	        'nns_import_id'=>$media_val['nns_content_id'],
	        'nns_url'=>$media_val['nns_url'],
	        'nns_ext_url'=>$media_val['nns_ext_url'],
	    );
	    $this->execute_c2_msp($c2_task_log,$re_msp,$base_info);
	    return $re_msp;
	}


	/**
	 * 直播节目单CDN
	 * @param unknown $movie_id
	 */
	public function do_playbill()
	{
		if(!isset($this->arr_params['nns_playbill_id']) && !empty($this->arr_params['nns_playbill_id']))
		{
			return array(
					'ret'=>1,
					'reason'=>'节目单传入参数无nns_playbill_id字段,参数为:'.var_export($this->arr_params),
			);
		}
		$playbill_id = $this->arr_params['nns_playbill_id'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/live_media.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/playbill.class.php';
		
		//查询节目单
	    $array_playbill = nl_playbill::query_by_id($this->obj_dc, $this->arr_params['nns_playbill_id']);
	    if($array_playbill['ret'] !=0)
	    {
	        return $array_playbill;
	    }
	    if(!isset($array_playbill['data_info']) || !is_array($array_playbill['data_info']) || empty($array_playbill['data_info']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_task_id字段1,参数为:'.var_export($this->arr_params));
	    }
	    $array_playbill = $array_playbill['data_info'];
	    $result_playbill_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'playbill', $array_playbill['nns_id'],$array_playbill,$this->arr_sp_config);
	    if($result_playbill_import_info['ret'] !=0 || !isset($result_playbill_import_info['data_info']) || strlen($result_playbill_import_info['data_info']) <1)
	    {
	        // 		    nn_log::write_log_message(LOG_MODEL_CMS,"获取片源注入id信息失败：".var_export($result_media_import_info,true),$this->str_sp_id,'media');
	        return $result_playbill_import_info;
	    }
	    $array_playbill['nns_playbill_import_id'] = $result_playbill_import_info['data_info'];
	    //查询直播频道   目前没用  （预留）
	    $array_live = nl_live::query_by_id($this->obj_dc, $array_playbill['nns_live_id']);
	    if($array_live['ret'] !=0)
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_task_id字段2,参数为:'.var_export($this->arr_params));
	    }
	    if(!isset($array_live['data_info']) || !is_array($array_live['data_info']) || empty($array_live['data_info']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_task_id字段3,参数为:'.var_export($this->arr_params));
	    }
	    $array_live = $array_live['data_info'];
	    $result_live_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'live', $array_live['nns_id'],$array_live,$this->arr_sp_config);
	    if($result_live_import_info['ret'] !=0 || !isset($result_live_import_info['data_info']) || strlen($result_live_import_info['data_info']) <1)
	    {
	        return $result_live_import_info;
	    }
	    $array_live['nns_import_id'] = $result_live_import_info['data_info'];
	    
	    $array_media = nl_live_media::query_by_id($this->obj_dc, $array_playbill['nns_live_media_id']);
	    if($array_media['ret'] !=0)
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_task_id字段4,参数为:'.var_export($this->arr_params));
	    }
	    if(!isset($array_media['data_info']) || !is_array($array_media['data_info']) || empty($array_media['data_info']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_task_id字段5,参数为:'.var_export($this->arr_params));
	    }
	    $array_media = $array_media['data_info'];
	    $result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'live_media', $array_media['nns_id'],$array_media,$this->arr_sp_config);
	    if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
	    {
	        return $result_media_import_info;
	    }
	    $array_media['nns_content_id'] = $result_media_import_info['data_info'];
	    $str_action = ($array_playbill['nns_state'] !=1) ? ($array_playbill['nns_modify_time'] > $array_playbill['nns_create_time']) ? 'modify' : 'add' : 'destroy';
	    $str_action_1 = ($array_playbill['nns_state'] !=1) ? ($array_playbill['nns_modify_time'] > $array_playbill['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
	    $arr_c2_info = array(
	        'nns_name'=>$array_playbill['nns_name'],
	    );
	    
	    $result_producer = nl_producer::query_by_cp_source_domain_id($this->obj_dc, 'playbill', $array_playbill['nns_cp_id'], $array_playbill['nns_import_source'], $array_playbill['nns_domain']);
	     
	    $arr_producer = (isset($result_producer['data_info']) && is_array($result_producer['data_info']) && !empty($result_producer['data_info'])) ? $result_producer['data_info'] : null;
	     
	    $arr_message_request_data = $this->query_message_info($array_playbill['nns_cp_id']);
	    //MSP注入
	    $re_msp = import_model::import_cdn_playbill_data($array_live['nns_import_id'],$array_media['nns_content_id'],$array_playbill,$str_action,$arr_producer,$arr_message_request_data);
	    if($re_msp['ret'] !=0)
	    {
	        $re_msp['ret'] =1;
	        nl_c2_task::edit($this->obj_dc, array('nns_status'=>'-1'), $this->arr_params['nns_task_id']);
	    }
	    else
	    {
	        nl_c2_task::edit($this->obj_dc, array('nns_status'=>'0'), $this->arr_params['nns_task_id']);
	    }
	    
	    
	    $three_rand = np_guid_rand();
	    $c2_task_log = array(
	        'nns_id' => $three_rand,
	        'nns_type' => 'std',
	        'nns_org_id' => $this->str_sp_id,
	        'nns_task_type' => 'Schedule',
	        'nns_task_id' => $this->arr_params['nns_task_id'],
	        'nns_task_name' => $arr_c2_info['nns_name'],
	        'nns_action' => $str_action_1,
	        'nns_notify_result_url' => $three_rand.'.txt',
	        'nns_desc' => 'Schedule,'.$str_action_1,
	        'nns_again' => 0,
	        'nns_url' => $three_rand. '.xml',
	    );
	    
	    $base_info = array(
	        'nns_cp_id'=>$array_playbill['nns_cp_id'],
	        'nns_import_id'=>$array_playbill['nns_import_id'],
	        'nns_url'=>$array_playbill['nns_url'],
	        'nns_ext_url'=>$array_playbill['nns_ext_url'],
	    );
	    $this->execute_c2_msp($c2_task_log,$re_msp,$base_info);
	    return $re_msp;
	}
	
	/**
	 * 文件包CDN
	 * @param unknown $movie_id
	 */
	public function do_file()
	{
	    if(!isset($this->arr_params['nns_file_id']) && !empty($this->arr_params['nns_file_id']))
	    {
	        return array(
	            'ret'=>1,
	            'reason'=>'文件包传入参数无nns_file_id字段,参数为:'.var_export($this->arr_params),
	        );
	    }
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/file_package/file_package.class.php';
	    //查询文件包
	    $array_file_package = nl_file_package::query_by_id($this->obj_dc, $this->arr_params['nns_file_id']);
	    if($array_file_package['ret'] !=0)
	    {
	        return $array_file_package;
	    }
	    if(!isset($array_file_package['data_info']) || !is_array($array_file_package['data_info']) || empty($array_file_package['data_info']))
	    {
	        return $this->_make_return_data(1,'文件包传查询无数据'.$this->arr_params['nns_file_id']);
	    }
	    $array_file_package = $array_file_package['data_info'];
	    $result_file_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'file',$this->arr_params['nns_file_id'],$array_file_package,$this->arr_sp_config);
	    $array_file_package['nns_import_id'] = $result_file_info['data_info'];
	    $str_action_1 = ($array_file_package['nns_deleted'] ==0) ? ($array_file_package['nns_modify_time'] > $array_file_package['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
	    $result_producer = nl_producer::query_by_cp_source_domain_id($this->obj_dc, 'file', $array_file_package['nns_cp_id'], $array_file_package['nns_import_source'], $array_file_package['nns_domain']);
	     
	    $arr_message_request_data = $this->query_message_info($array_file_package['nns_cp_id']);
	    //MSP注入
// 	    $re_msp = import_model::import_cdn_playbill_data($array_file_package['nns_import_id'],$array_media['nns_content_id'],$array_file_package,$str_action,$result_producer,$arr_message_request_data);
	    $re_msp = array(
	        'ret'=>0,
	        'reason'=>'import ok'
	    );
	    if($re_msp['ret'] !=0)
	    {
	        $re_msp['ret'] =1;
	        nl_c2_task::edit($this->obj_dc, array('nns_status'=>'-1'), $this->arr_params['nns_task_id']);
	    }
	    else
	    {
	        nl_c2_task::edit($this->obj_dc, array('nns_status'=>'0'), $this->arr_params['nns_task_id']);
	    }
	    
	    $time = date('YmdHis') . rand(0, 10000);
	    $three_rand = rand(100, 999);
	    $c2_task_log = array(
	        'nns_id' => np_guid_rand(),
	        'nns_type' => 'std',
	        'nns_org_id' => $this->str_sp_id,
	        'nns_task_type' => 'File',
	        'nns_task_id' => $this->arr_params['nns_task_id'],
	        'nns_task_name' => $array_file_package['nns_name'],
	        'nns_action' => $str_action_1,
	        'nns_notify_result_url' => '',
	        'nns_desc' => 'File,'.$str_action_1,
	        'nns_again' => 0,
	        'nns_url' => 'zgcms_'. $time .'_' . 'File' .'_'. $str_action_1 . '_' . $three_rand. '.xml',
	    );
	    $base_info = array(
	        'nns_cp_id'=>$array_file_package['nns_cp_id'],
	        'nns_import_id'=>$array_file_package['nns_import_id'],
	        'nns_url'=>$array_file_package['nns_service_url'],
	        'nns_ext_url'=>$array_file_package['nns_ext_url'],
	    );
	    $this->execute_c2_msp($c2_task_log,$re_msp,$base_info);
	    return $re_msp;
	}
	
	
	private function _add_c2($type,$mix_id,$str_action,$c2_info=null)
	{
	    $next_string='';
        switch ($type)
        {
            case 'live_media':
                $next_string='-直播流';
                break;
            case 'playbill':
                $next_string='-节目单';
                break;
            default:
                return self::return_data(1,'没有此类型');
        }
	    $str_task_id = np_guid_rand();
	    $result_c2_exsist=nl_c2_task::query_c2_exsist($this->obj_dc,$type,$mix_id,$this->str_sp_id);
	    if($result_c2_exsist['ret'] !=0)
	    {
	        return $result_c2_exsist;
	    }
	    $result_c2_exsist = isset($result_c2_exsist['data_info'][0]) ? $result_c2_exsist['data_info'][0] : null;
	    if(is_array($result_c2_exsist) && !empty($result_c2_exsist))
	    {
	        $str_task_id = $result_c2_exsist['nns_id'];
	        $result = nl_c2_task::edit($this->obj_dc, array('nns_action'=>$str_action,'nns_status'=>1,'nns_epg_status'=>97),$str_task_id);
	    }
	    else
	    {
            $arr_c2_add = array(
                'nns_id'=>$str_task_id,
                'nns_type'=>$type,
                'nns_name'=>"[{$c2_info['nns_name']}]{$next_string}",
                'nns_ref_id'=>$c2_info['nns_ref_id'],
                'nns_action'=>$str_action,
                'nns_status'=>1,
                'nns_org_id'=>$this->str_sp_id,
                'nns_category_id'=>'',
                'nns_src_id'=>$c2_info['nns_src_id'],
                'nns_all_index'=>1,
                'nns_clip_task_id'=>'',
                'nns_clip_date'=>'',
                'nns_op_id'=>'',
                'nns_epg_status'=>97,
                'nns_ex_url'=>'',
                'nns_file_path'=>'',
                'nns_file_size'=>'',
                'nns_file_md5'=>'',
                'nns_cdn_policy'=>'',
                'nns_epg_fail_time'=>0,
                'nns_message_id'=>'',
            );
            $result = nl_c2_task::add($this->obj_dc, $arr_c2_add);
	    }
	    $result['data_info'] = $str_task_id;
        return $result;
	}
	
	
	private function _make_xml_string($arr_data,$obj_parent=null)
	{
	    if(empty($arr_data) || !is_array($arr_data))
	    {
	        return ;
	    }
	    $this->obj_dom = new DOMDocument("1.0", 'utf-8');
	    foreach ($arr_data as $key=>$val)
	    {
	        if(is_string($val))
	        {
	            $this->make_xml($key,$val);
	        }
	        else if(is_array($val) && !empty($val))
	        {
	            $obj_parent = $this->make_xml($key);
	            $this->_make_xml_string($val,$obj_parent);
	        }
	        else
	        {
	            $this->make_xml($key);
	        }
	    }
	    return ;
	}
	
	/**
	 * 组装基础dom对象类
	 * @param string $key 键
	 * @param string $val 值
	 * @param array $arr_attr attr值
	 * @param object $parent 父级对象
	 * @return object
	 * @date 2015-05-04
	 */
	private function make_xml($key, $val = null, $arr_attr = null,$parent=null)
	{
	    if(is_null($parent))
	    {
	        $parent=$this->obj_dom;
	    }
	    $$key = isset($val) ? $this->obj_dom->createElement($key, $val) : $this->obj_dom->createElement($key);
	    if (!empty($arr_attr) && is_array($arr_attr))
	    {
	        foreach ($arr_attr as $attr_key => $attr_val)
	        {
	            $domAttribute = $this->obj_dom->createAttribute($attr_key);
	            $domAttribute->value = $attr_val;
	            $$key->appendChild($domAttribute);
	            $this->obj_dom->appendChild($$key);
	        }
	    }
	    $parent->appendChild($$key);
	    //unset($dom);
	    unset($parent);
	    return $$key;
	}
}
