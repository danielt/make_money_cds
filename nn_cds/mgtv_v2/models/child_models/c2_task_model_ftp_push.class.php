<?php
/**
 * starcor 昆山茁壮规范ADI CDN注入
 * @author liangpan
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_class/ftp/ftp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/c2_task/c2_task.class.php';
include_once dirname(dirname(__FILE__)).'/queue_task_model.php';
class c2_task_model_v2 extends nn_public_model
{
    
    public function make_media_delivery_xml($result_video,$result_index,$result_media,$action)
    {
        $result_index['nns_index']++;
        $str_xml = '';
        $str_xml.= '<Objects>';
        $str_xml.=      '<Object Code="'.$result_video['nns_cdn_video_guid'].'" Action="'.$action.'" ID="'.$result_video['nns_cdn_video_guid'].'" ElementType="Series"  ObjectID="" ParentCode="">';
        $str_xml.=          '<Property Name="Name"><![CDATA['.$result_video['nns_name'].']]></Property>';
        $str_xml.=          '<Property Name="EnName"><![CDATA['.$result_video['nns_eng_name'].']]></Property>';
        $str_xml.=          '<Property Name="Pinyin"><![CDATA['.$result_video['nns_pinyin'].']]></Property>';
        $str_xml.=          '<Property Name="Code">'.$result_video['nns_cdn_video_guid'].'</Property>';
        $str_xml.=          '<Property Name="ActorDisplay"><![CDATA['.$result_video['nns_actor'].']]></Property>';
        $str_xml.=          '<Property Name="WriterDisplay"><![CDATA['.$result_video['nns_director'].']]></Property>';
        $str_xml.=          '<Property Name="OrgAirDate"></Property>';
        $str_xml.=          '<Property Name="IsCharge">0</Property>';
        $str_xml.=          '<Property Name="ChargeDesc"></Property>';
        $str_xml.=          '<Property Name="Score">8.5</Property>';
        $str_xml.=          '<Property Name="VolumnCount">'.$result_video['nns_all_index'].'</Property>';
        $str_xml.=          '<Property Name="Description"><![CDATA['.$result_video['nns_summary'].']]></Property>';
        $str_xml.=          '<Property Name="Tag"></Property>';
        $str_xml.=          '<Property Name="StarLevel">6</Property>';
        $str_xml.=          '<Property Name="PictureURL1"></Property>';
        $str_xml.=          '<Property Name="ProgramType"><![CDATA['.$result_video['category_name'].']]></Property>';
        $str_xml.=          '<Property Name="ProgramType2"><![CDATA['.$result_video['p_category_name'].']]></Property>';
        $str_xml.=          '<Property Name="OriginalCountry"><![CDATA['.$result_video['nns_area'].']]></Property>';
        $str_xml.=          '<Property Name="Language"><![CDATA['.$result_video['nns_language'].']]></Property>';
        $str_xml.=          '<Property Name="ReleaseYear"><![CDATA['.date('Y',strtotime($result_video['nns_show_time'])).']]></Property>';
        $str_xml.=          '<Property Name="Duration"><![CDATA['.$result_video['nns_view_len'].']]></Property>';
        $str_xml.=          '<Property Name="Definition">SD</Property>';
        $str_xml.=          '<Property Name="Is3d">0</Property>';
        $str_xml.=          '<Property Name="Compere"><![CDATA['.$result_video['nns_actor'].']]></Property>';
        $str_xml.=          '<Property Name="Guest"></Property>';
        $str_xml.=          '<Property Name="OnlineTime"><![CDATA['.date("Y-m-d H:i:s").']]></Property>';
        $str_xml.=          '<Property Name="Recommendation"><![CDATA['.$result_video['nns_keyword'].']]></Property>';
        $str_xml.=          '<Property Name="HorizontalPoster"></Property>';
        $str_xml.=      '</Object>';
        $str_xml.=      '<Object Code="'.$result_index['nns_cdn_index_guid'].'" Action="'.$action.'" ID="'.$result_index['nns_cdn_index_guid'].'" ElementType="Program" ObjectID="" ParentCode="'.$result_video['nns_cdn_video_guid'].'">';
        $str_xml.=          '<Property Name="Name"><![CDATA['.$result_index['nns_name'].']]></Property>';
        $str_xml.=          '<Property Name="Code">'.$result_index['nns_cdn_index_guid'].'</Property>';
        $str_xml.=          '<Property Name="ActorDisplay"><![CDATA['.$result_index['nns_actor'].']]></Property>';
        $str_xml.=          '<Property Name="WriterDisplay"><![CDATA['.$result_index['nns_director'].']]></Property>';
        $str_xml.=          '<Property Name="OrgAirDate"></Property>';
        $str_xml.=          '<Property Name="VolumnCount">'.$result_video['nns_all_index'].'</Property>';
        $str_xml.=          '<Property Name="Description"><![CDATA['.$result_index['nns_summary'].']]></Property>';
        $str_xml.=          '<Property Name="Tag"></Property>';
        $str_xml.=          '<Property Name="OriginalCountry"><![CDATA['.$result_video['nns_area'].']]></Property>';
        $str_xml.=          '<Property Name="Language"><![CDATA['.$result_video['nns_language'].']]></Property>';
        $str_xml.=          '<Property Name="ReleaseYear"><![CDATA['.date('Y',strtotime($result_video['nns_show_time'])).']]></Property>';
        $str_xml.=          '<Property Name="Duration"><![CDATA['.$result_index['nns_time_len'].']]></Property>';
        $str_xml.=          '<Property Name="ProgramType"><![CDATA[综艺]]></Property>';
        $str_xml.=          '<Property Name="ProgramType2"><![CDATA[访谈]]></Property>';
        $str_xml.=          '<Property Name="EnName"></Property>';
        $str_xml.=          '<Property Name="Definition">SD</Property>';
        $str_xml.=          '<Property Name="Is3d">0</Property>';
        $str_xml.=          '<Property Name="PictureURL1"></Property>';
        $str_xml.=      '</Object>';
        $str_xml.=      '<Object Code="'.$result_media['nns_cdn_media_guid'].'" Action="'.$action.'" ID="'.$result_media['nns_cdn_media_guid'].'" ElementType="Movie" ObjectID="" ParentCode="'.$result_index['nns_cdn_index_guid'].'">';
        $str_xml.=          '<Property Name="Code">'.$result_media['nns_cdn_media_guid'].'</Property>';
        $str_xml.=          '<Property Name="FileURL">'.$result_media['nns_url'].'</Property>';
        $str_xml.=          '<Property Name="PlayURL">'.$result_media['nns_url'].'</Property>';
        $str_xml.=          '<Property Name="CdnPlatform">23</Property>';
        $str_xml.=          '<Property Name="Definition">1</Property>';
        $str_xml.=          '<Property Name="BitRateId">1</Property>';
        $str_xml.=          '<Property Name="Dispatch">1</Property>';
        $str_xml.=          '<Property Name="Md5">'.$result_media['nns_file_md5'].'</Property>';
        $str_xml.=          '<Property Name="IsTranscode">1</Property>';
        $str_xml.=      '</Object>';
        $str_xml.= '</Objects>';
        $str_xml.= '<Mappings>';
        $str_xml.=      '<Mapping ElementCode="'.$result_index['nns_cdn_index_guid'].'" ParentCode="'.$result_video['nns_cdn_video_guid'].'" ElementID="'.$result_index['nns_cdn_index_guid'].'" ParentID="'.$result_video['nns_cdn_video_guid'].'" ElementType="Program" ParentType="Series" Action="'.$action.'" ID="'.$result_video['nns_cdn_video_guid'].$result_index['nns_cdn_index_guid'].'">';
        $str_xml.=          '<Property Name="Type">1</Property>';
        $str_xml.=          '<Property Name="Sequence">'.$result_index['nns_index'].'</Property>';
        $str_xml.=          '<Property Name="ValidStart"></Property>';
        $str_xml.=          '<Property Name="ValidEnd"></Property>';
        $str_xml.=      '</Mapping>';
        $str_xml.=      '<Mapping ElementCode="'.$result_media['nns_cdn_media_guid'].'" ParentCode="'.$result_index['nns_cdn_index_guid'].'" ElementID="'.$result_media['nns_cdn_media_guid'].'" ParentID="'.$result_index['nns_cdn_index_guid'].'" ElementType="Movie" ParentType="Program" Action="'.$action.'" ID="'.$result_index['nns_cdn_index_guid'].$result_media['nns_cdn_media_guid'].'">';
        $str_xml.=          '<Property Name="Type">1</Property>';
        $str_xml.=          '<Property Name="Sequence">'.$result_index['nns_index'].'</Property>';
        $str_xml.=          '<Property Name="ValidStart"></Property>';
        $str_xml.=          '<Property Name="ValidEnd"></Property>';
        $str_xml.=      '</Mapping>';
        $str_xml.= '</Mappings>';
        return $str_xml;
    }
    
    
    public function do_media_delivery_del()
    {
        $this->do_media_delivery_add();
    }
    
    /**
     * 发布操作
     */
    public function do_media_delivery_add()
    {
        if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
        {
            return $this->_make_return_data(1,'点播传入参数无nns_ref_id字段,参数为:'.var_export($this->arr_params));
        }
        $movie_id = $this->arr_params['bk_c2_task_info']['nns_ref_id'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_class/ftp/ftp_push.class.php';
		$result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $movie_id);
		if($result_media['ret'] !=0)
		{
			return $result_media;
		}
		if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
		{
			nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=7,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
			return $result_media;
		}
		
		$result_media = $result_media['data_info'];
// 		$temp_media_source_url = trim(trim($result_media['nns_url'],'/'),"\\");
// 		$local_file_path = isset($this->arr_sp_config['c2_import_push_media_base_url']) ? trim(trim($this->arr_sp_config['c2_import_push_media_base_url'],'/'),'\\') : '';
// 		$result_media['nns_temp_url'] = (strlen($local_file_path) >0) ? '/'.$local_file_path.'/'.$temp_media_source_url : '/'.$temp_media_source_url;
		
		$result_media['nns_file_md5'] = $result_media['nns_media_name'];
		if(strlen($result_media['nns_file_md5'])!=32)
		{
		    return null;
		}
		$result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $movie_id,$result_media);
		$str_media_ftp = rtrim((trim($this->arr_sp_config['media_ftp'])),'/');
		if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
		{
			return $result_media_import_info;
		}
		$result_media['nns_cdn_media_guid'] = $result_media_import_info['data_info'];
		$result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $result_media['nns_vod_index_id']);
		if($result_index['ret'] !=0)
		{
		    return $result_index;
		}
		if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
		{
		    nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=7,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
		    return $result_index;
		}
		$result_index = $result_index['data_info'][0];
		$result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $result_media['nns_vod_index_id'],$result_index);
		
		if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
		{
		    return $result_index_import_info;
		}
		$result_index['nns_cdn_index_guid'] = $result_index_import_info['data_info'];
		
		$result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_media['nns_vod_id']);
		if($result_video['ret'] !=0)
		{
		    return $result_video;
		}
		if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
		{
		    nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=7,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
		    return $result_video;
		}
		$result_video = $result_video['data_info'][0];
		
		$this->get_category('video');
		if(isset($result_video['nns_category_id']) && strlen($result_video['nns_category_id']) >0 
		    && isset($this->arr_category['video'][$result_video['nns_category_id']]['name']) && strlen($this->arr_category['video'][$result_video['nns_category_id']]['name'])>0)
		{
		    $result_video['category_name'] = $this->arr_category['video'][$result_video['nns_category_id']]['name'];
		}
		else
		{
		    $result_video['category_name'] = $result_video['nns_all_index'] >1 ? "电视剧" : '电影';
		}
		$result_video['p_category_name'] = $result_video['category_name'];
		
		$result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_media['nns_vod_id'],$result_video);
		
		if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
		{
		    return $result_video_import_info;
		}
		$result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
		
		$result_cp_config = nl_cp::query_by_id($this->obj_dc, $result_video['nns_cp_id']);
		if($result_cp_config['ret'] !=0)
		{
		    return $result_cp_config;
		}
		$this->arr_cp_config = (isset($result_cp_config['data_info']['nns_config']) && strlen($result_cp_config['data_info']['nns_config']) >0) ? json_decode($result_cp_config['data_info']['nns_config'],true) : null;
		
		
		$temp_file_path = pathinfo($result_media['nns_url']);
		if(!isset($temp_file_path['extension']) || strlen($temp_file_path['extension']) <1)
		{
		    nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=7,nns_epg_status=100 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
		    return $this->_make_return_data(1,'temp set');
		}
		$temp_file_path['extension'] = strtolower($temp_file_path['extension']);
		$result_media['nns_filetype'] = $temp_file_path['extension'];
		if(strtolower($result_media['nns_filetype']) !='ts' || !isset($temp_file_path['extension']) || strtolower($temp_file_path['extension']) !='ts')
		{
		    nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=7,nns_epg_status=100 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
		    return $this->_make_return_data(1,'temp set');
		}
		$result_c2_log = nl_c2_log::query_by_task_id($this->obj_dc, $this->arr_params['bk_c2_task_info']['nns_id'],array('REGIST',"UPDATE"));
		if($result_c2_log['ret'] !=0 || !isset($result_c2_log['data_info'][0]) || !is_array($result_c2_log['data_info'][0]) || empty($result_c2_log['data_info'][0]))
		{
		    return $this->_make_return_data(1,'temp set');
		}
		
		$result_media['nns_url'] = $str_media_ftp.'/'.$result_media['nns_cp_id'].'/'.$result_c2_log['data_info'][0]['nns_id'].'/'.$result_c2_log['data_info'][0]['nns_id'].'.'.$temp_file_path['extension'];
		$action = ($this->arr_params['bk_c2_task_info']['nns_action'] !='destroy') ? ($this->arr_params['bk_c2_task_info']['nns_action'] == 'update') ? 'UPDATE' : 'REGIST' : 'DELETE';
		$task_log_id = np_guid_rand();
        $xml_str = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml_str.= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
        $xml_str.=      $this->make_media_delivery_xml($result_video,$result_index,$result_media,$action);
        $xml_str.= '</ADI>';
        $action = ($result_media['nns_deleted'] !=1) ? 'DELIVERY_ADD' : 'DELIVERY_DEL';
        $str_guid = np_guid_rand();
        $file_name = $this->str_sp_id . '_' . date('His') . '_' . $str_guid . '_' . 'vod_media' . '_' . $action . '.xml';
        $c2_info = array(
            'nns_task_type'=>'Movie',
            'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
            'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
            'nns_action'=>	$action,
            'nns_url' => $file_name,
            'nns_content' => $xml_str,
            'nns_desc' => 'Movie,'.$action,
            'nns_id'=>$str_guid,
        );
        $result = $this->execute_self_bk_c3($c2_info);
        return $result;
    }
    
    /**
     * 组装昆山片源信息
     * @param unknown $arr_info 片源基本信息
     * @param string $arr_info_ex 扩展数据
     * @return string
     */
	private function make_kunshan_vod_media_xml_v2($arr_info,$arr_info_ex=null)
	{
	    $str_Action = ($arr_info['nns_deleted'] !=1) ? '' : 'DELETE';
	    $str_date = date("Y-m-d");
	    $str_xml  ='<ADI>';
	    $str_xml .=    '<Metadata>';
	    $str_xml .=        '<AMS Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Asset_ID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" Asset_Class="package" Provider="STARCOR" Provider_ID="www.starcor.com" Product="MOD" Version_Minor="0" Version_Major="1" Description="MODpackage" Creation_Date="'.$str_date.'" Verb="'.$str_Action.'" />';
	    $str_xml .=        '<App_Data App="MOD" Name="Provider_Content_Tier" Value="STARCOR" />';
	    $str_xml .=        '<App_Data App="MOD" Name="Metadata_Spec_Version" Value="CableLabsVOD1.1" />';
	    $str_xml .=        '<App_Data App="MOD" Name="Default_Language" Value="zh_CN" />';
	    $str_xml .=        '<App_Data App="MOD" Name="Languages" Value="英语" />';
	    $str_xml .=    '</Metadata>';
	    $str_xml .=    '<Asset>';
	    $str_xml .=        '<Metadata>';
	    $str_xml .=            '<AMS Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Asset_ID="'.$arr_info_ex['index']['base']['nns_cdn_index_guid'].'" Asset_Class="title" Provider="STARCOR" Provider_ID="www.starcor.com" Product="MOD" Version_Minor="0" Version_Major="1" Description="MODtitle" Creation_Date="2016-03-18" Verb="'.$str_Action.'" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Type" Value="title" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Show_Type" Value="Movie" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Title" Value="'.$arr_info_ex['video']['base']['nns_name'].'" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Rating" Value="" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Run_Time" Value="'.$this->seconds_to_hour($arr_info['nns_file_time_len']).'" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Display_Run_Time" Value="01:35" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Year" Value="'.$arr_info_ex['video']['base']['nns_show_time'].'" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Country_of_Origin" Value="'.$arr_info_ex['video']['base']['nns_area'].'" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Preview_Period" Value="0" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Provider_QA_Contact" Value="" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Genre" Value="-" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Category" Value="" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Billing_ID" Value="0" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Licensing_Window_Start" Value="2015-01-01" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Licensing_Window_End" Value="2066-07-22" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Suggested_Price" Value="0.0" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Maximum_Viewing_Length" Value="00:00:00" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Title_Brief" Value="" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Episode_Name" Value="'.$arr_info_ex['video']['base']['nns_name'].'" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Episode_ID" Value="1" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Summary_Short" Value="'.$arr_info_ex['video']['base']['nns_summary'].'" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Actors" Value="'.$arr_info_ex['video']['base']['nns_actor'].'" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Director" Value="'.$arr_info_ex['video']['base']['nns_director'].'" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Producers" Value="STARCOR" />';
	    $str_xml .=            '<App_Data App="MOD" Name="IsSeries" Value="N" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Chapter" Value="1" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Region" Value="'.$arr_info_ex['video']['base']['nns_area'].'" />';
	    $str_xml .=            '<App_Data App="MOD" Name="Provider" Value="STARCOR" />';
	    $str_xml .=        '</Metadata>';
	    $str_xml .=        '<Asset>';
	    $str_xml .=            '<Metadata>';
	    $str_xml .=                '<AMS Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Asset_ID="'.$arr_info['nns_cdn_media_guid'].'" Asset_Class="movie" Provider="STARCOR" Provider_ID="www.starcor.com" Product="MOD" Version_Minor="0" Version_Major="1" Description="MODmovie" Creation_Date="2016-03-18" Verb="'.$str_Action.'" />';
	    $str_xml .=                '<App_Data App="MOD" Name="Encryption" Value="N" />';
	    $str_xml .=                '<App_Data App="MOD" Name="Type" Value="movie" />';
	    $str_xml .=                '<App_Data App="MOD" Name="HDContent" Value="Y" />';
	    $str_xml .=                '<App_Data App="MOD" Name="Content_FileSize" Value="'.$arr_info['nns_file_size'].'" />';
	    $str_xml .=                '<App_Data App="MOD" Name="Content_CheckSum" Value="49f66848492573c878a7b06727696807" />';
	    $str_xml .=                '<App_Data App="MOD" Name="Content_Format" Value="H264" />';
	    $str_xml .=                '<App_Data App="MOD" Name="Bit_Rate" Value="'.$arr_info['nns_kbps'].'" />';
	    $str_xml .=                '<App_Data App="MOD" Name="Audio_Type" Value="0" />';
	    $str_xml .=             '</Metadata>';
	    $str_xml .=             '<Content Value="'.$arr_info['nns_cdn_media_guid'].'.ts" />';
	    $str_xml .=        '</Asset>';
	    $str_xml .=        '<Asset>';
	    $str_xml .=            '<Metadata>';
	    $str_xml .=                '<AMS Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Asset_ID="'.$arr_info['nns_cdn_media_guid'].'" Asset_Class="poster" Provider="STARCOR" Provider_ID="www.starcor.com" Product="MOD" Version_Minor="0" Version_Major="1" Description="MOD poster" Creation_Date="2016-03-18" Verb="'.$str_Action.'" />';
	    $str_xml .=                '<App_Data App="MOD" Name="Type" Value="poster" />';
	    $str_xml .=                '<App_Data App="MOD" Name="Content_FileSize" Value="29763" />';
	    $str_xml .=                '<App_Data App="MOD" Name="Content_CheckSum" Value="20bcd05a4915a0973b5f0a87c2e7f71b" />';
	    $str_xml .=            '</Metadata>';
	    $str_xml .=            '<Content Value="'.$arr_info['nns_cdn_media_guid'].'.jpg"/>';
	    $str_xml .=        '</Asset>';
	    $str_xml .=    '</Asset>';
	    $str_xml .='</ADI>';
	    return $str_xml;
	}
	
	/**
	 * 组装主媒资信息
	 * @param unknown $arr_info 主媒资基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 */
	private function make_vod_xml($arr_info,$str_Action,$arr_info_ex=null)
	{
	    $str_xml ='<Object ElementType="Series" ID="'.$arr_info['nns_cdn_video_guid'].'" Action="'.$str_Action.'">';
	    $str_xml.=	'<Property Name="Name">'.$arr_info['nns_name'].'</Property>';
	    $str_xml.=	'<Property Name="OrderNumber">1</Property>';
	    $str_xml.=	'<Property Name="OriginalName">'.$arr_info['nns_name'].'</Property>';
	    $str_xml.=	'<Property Name="AliasName">'.$arr_info['nns_alias_name'].'</Property>';
	    $str_xml.=	'<Property Name="EnglishName">'.$arr_info['nns_eng_name'].'</Property>';
	    $str_xml.=	'<Property Name="SortName">'.$arr_info['nns_name'].'</Property>';
	    $str_xml.=	'<Property Name="SearchName">'.$arr_info['nns_name'].'</Property>';
	    $str_xml.=	'<Property Name="OrgAirDate">'.$arr_info['nns_copyright_date'].'</Property>';
	    $str_xml.=	'<Property Name="ReleaseYear">'.$arr_info['nns_show_time'].'</Property>';
	    $str_xml.=	'<Property Name="LicensingWindowStart"></Property>';
	    $str_xml.=	'<Property Name="LicensingWindowEnd"></Property>';
	    $str_xml.=	'<Property Name="CPContentID">'.$arr_info['nns_cp_id'].'</Property>';
	    $str_xml.=	'<Property Name="DisplayAsNew">0</Property>';
	    $str_xml.=	'<Property Name="DisplayAsLastChance">0</Property>';
	    $str_xml.=	'<Property Name="Macrovision">1</Property>';
	    $str_xml.=	'<Property Name="Price">1</Property>';
	    $str_xml.=	'<Property Name="VolumnCount">'.$arr_info['nns_all_index'].'</Property>';
	    $str_xml.=	'<Property Name="NewCount">'.$arr_info['nns_new_index'].'</Property>';
	    $str_xml.=	'<Property Name="Status">1</Property>';
	    $str_xml.=	'<Property Name="Description">'.$arr_info['nns_summary'].'</Property>';
	    $str_xml.=	'<Property Name="ContentProvider">'.$arr_info['nns_import_source'].'</Property>';
	    $str_xml.=	'<Property Name="KeyWords">'.$arr_info['nns_keyword'].'</Property>';
	    $str_xml.=	'<Property Name="OriginalCountry">'.$arr_info['nns_area'].'</Property>';
	    $str_xml.=	'<Property Name="ActorDisplay">'.$arr_info['nns_actor'].'</Property>';
	    $str_xml.=	'<Property Name="DirectorDisplay">'.$arr_info['nns_director'].'</Property>';
	    $str_xml.=	'<Property Name="WriterDisplay">'.$arr_info['nns_screenwriter'].'</Property>';
	    $str_xml.=	'<Property Name="Language">'.$arr_info['nns_language'].'</Property>';
	    $str_xml.=	'<Property Name="Kind">'.$arr_info['nns_kind'].'</Property>';
	    $str_xml.=	'<Property Name="Duration">'.$arr_info['nns_view_len'].'</Property>';
	    $str_xml.=	'<Property Name="CategoryName">'.$arr_info['nns_cdn_category_name'].'</Property>';
	    $str_xml.=	'<Property Name="CategoryID">'.$arr_info['nns_cdn_category_id'].'</Property>';
	    $str_xml.=	'<Property Name="PlayCount">0</Property>';
	    $str_xml.=	'<Property Name="CategorySort">0</Property>';
	    $str_xml.=	'<Property Name="Tags">'.$arr_info['nns_tag'].'</Property>';
	    $str_xml.=	'<Property Name="ViewPoint">'.$arr_info['nns_point'].'</Property>';
	    $str_xml.=	'<Property Name="StarLevel">6</Property>';
	    $str_xml.=	'<Property Name="Rating"/>';
	    $str_xml.=	'<Property Name="Awards"/>';
	    $str_xml.=	'<Property Name="Sort">0</Property>';
	    $str_xml.=	'<Property Name="Hotdegree">0</Property>';
	    $str_xml.=	'<Property Name="Reserve1"/>';
	    $str_xml.=	'<Property Name="Reserve1"/>';
	    $str_xml.='</Object>';
	    return $str_xml;
	}
	
	/**
	 * 组装分集信息
	 * @param unknown $arr_info 片源基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 */
	private function make_vod_index_xml($arr_info,$str_Action,$arr_info_ex=null)
	{
	    $str_xml ='<Object ElementType="Program" ID="'.$arr_info['nns_cdn_index_guid'].'" Action="'.$str_Action.'">';
	    $str_xml.=	'<Property Name="Name">'.$arr_info['nns_name'].'</Property>';
	    $str_xml.=	'<Property Name="CPContentID">'.$arr_info['nns_cp_id'].'</Property>';
	    $str_xml.= 	'<Property Name="OrderNumber">1</Property>';//0: No DRM 1: BES DRM
	    $str_xml.= 	'<Property Name="OriginalName">'.$arr_info['nns_name'].'</Property>';//0: No DRM  1: BES DRM
	    $str_xml.= 	'<Property Name="SortName">1</Property>';
	    $str_xml.= 	'<Property Name="Sequence">'.$arr_info['nns_index'].'</Property>';
	    $str_xml.= 	'<Property Name="SortName">1</Property>';
	    $str_xml.= 	'<Property Name="SearchName">'.$arr_info['nns_name'].'</Property>';
	    $str_xml.= 	'<Property Name="ActorDisplay">'.$arr_info['nns_actor'].'</Property>'; //0: 4x3   1: 16x9(Wide)
	    $str_xml.= 	'<Property Name="OriginalCountry"></Property>';//0:无字幕 1:有字幕
	    $str_xml.=	'<Property Name="Language"></Property>';
	    $str_xml.=	'<Property Name="ReleaseYear"></Property>';
	    $str_xml.=	'<Property Name="OrgAirDate">'.$arr_info['nns_kbps'].'</Property>';
	    $str_xml.=	'<Property Name="LicensingWindowStart"></Property>';
	    $str_xml.=	'<Property Name="LicensingWindowEnd"></Property>';
	    $str_xml.=	'<Property Name="DisplayAsNew"></Property>';
	    $str_xml.=	'<Property Name="DisplayAsLastChance"></Property>';
	    $str_xml.=	'<Property Name="Macrovision">1</Property>';
	    $str_xml.=	'<Property Name="Description">'.$arr_info['nns_summary'].'</Property>';
	    $str_xml.=	'<Property Name="PriceTaxIn"></Property>';
	    $str_xml.=	'<Property Name="Status">1</Property>';
	    $str_xml.=	'<Property Name="SourceType">1</Property>';
	    $str_xml.=	'<Property Name="SeriesFlag">1</Property>';
	    $str_xml.=	'<Property Name="ContentProvider">'.$arr_info['nns_import_source'].'</Property>';
	    $str_xml.=	'<Property Name="KeyWords">'.$arr_info['nns_summary'].'</Property>';
	    $str_xml.=	'<Property Name="Tags"></Property>';
	    $str_xml.=	'<Property Name="ViewPoint">'.$arr_info['nns_watch_focus'].'</Property>';
	    $str_xml.=	'<Property Name="StarLevel"></Property>';
	    $str_xml.=	'<Property Name="Rating"></Property>';
	    $str_xml.=	'<Property Name="Awards"></Property>';
	    $str_xml.=	'<Property Name="Duration">'.$arr_info['nns_time_len'].'</Property>';
	    $str_xml.=	'<Property Name="Reserve1"></Property>';
	    $str_xml.=	'<Property Name="Reserve2"></Property>';
	    $str_xml.=	'<Property Name="Reserve3"></Property>';
	    $str_xml.=	'<Property Name="Reserve4"></Property>';
	    $str_xml.=	'<Property Name="Reserve5"></Property>';
	    $str_xml.='</Object>';
	    return $str_xml;
	}
	
	/**
	 * 组装片源信息
	 * @param unknown $arr_info 片源基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 * @var string FileURL
	 *     说明： 媒体文件 URL ftp://username:password@ip:port/...  标准 FTP协议 或者 http:// ip:port/...
	 *     注释： 对托管内容注入方式下填写，支持 FTP 和 HTTP 协议获取内容 对非托管内容预注入方式下 则 为 空 ， 填 写ServiceURL 属性 当FileURL和ServiceURL均有值时，
	 *	               首选采用该属性值去获取内容。注入 HLS 内容时，如果属性值为 m3u8 文件结尾，CDN 获取内容时，获取整个 m3u8 的上级目录
	 * @var string ServiceURL
	 *     说明： 内容服务 URL
	 *     注释： 需要指定 CDN 服务 URL 的内容注入/预注入内容接入或实时回源接入内容删除时填写。为 CDN 提供服务的内容服务调用 URL，当 ServiceURL 有值时，该URL 前缀信息应和 CDN 服务配置
	 *           表中对应域内容提供 商 的PlaybackURLprefix 保持一致，用户需要基于本URL 访问注入内容
	 * @var string CPContentID
	 *     说明： CP 对于媒体文件的标识
	 * @var string SourceDRMType
	 *     说明： 0: No DRM 1: BES DRM
	 * @var string DestDRMType
	 *     说明： 0: No DRM 1: BES DRM
	 * @var string AudioType
	 *     说明： 0: 其他 1: Monaural 单声道 2: Stereo 多声道 3: Two-nation monaural 双单声道 4: Two-nation stereo 双多声道  5: AC3(5:1 channel) AC3声道
	 * @var string ScreenFormat
	 *     说明： 0: 4x3   1: 16x9(Wide)
	 * @var string ClosedCaptioning
	 *     说明： 字幕标志 0:无字幕 1:有字幕
	 * @var string Duration
	 *     说明： 播放时长 HHMISSFF （时分秒帧）
	 * @var string FileSize
	 *     说明： 文件大小，单位为 Byte
	 * @var string BitRateType
	 *     说明： 码流 1: 400k 2: 700K 3:  1.3M 4: 2M 5: 2.5M 6:  8M 7: 10M  8：15M 9：20M 10：30M
	 * @var string VideoType
	 *     说明： 编码格式： 1. H.264 2. MPEG4 3. AVS 4. MPEG2 5. MP3 6. WMV 7. H.265
	 * @var string AudioEncodingType
	 *     说明： 编码格式： 1. MP2 2. AAC 3. AMR 4. Mp3
	 * @var string Resolution
	 *     说明： 分辨率类型 1. QCIF 2. QVGA 3. 2/3 D1 4. 3/4 D1 5. D1 6. 720P 7. 1080i 8. 1080P 9. 2k 11. 4k 13. 8k
	 * @var string VideoProfile
	 *     说明： 1. Simple 2. Advanced Simple 3. Baseline 4. Main 5. High 6. JiZhun
	 * @var string SystemLayer
	 *     说明： 1. TS 2. 3GP
	 * @var string Domain
	 *     说明： 发布到融合 CDN 后的服务域和服务协议
	 *     注释： 包含 ServiceDomain 和ProtocolType 两层含义，为 4 字节整数，其中最高三 个 字 节 为ServiceDomain，表示预分发网络域，最低 3 位取值如下：
	 *               0x000001-IPTV 网络
	 *               0x000002-互联网网络
	 *               0x000004-移动网络
	 *               0x000008~0x000080-预留
	 *            最 低 一 个 字 节 为ProtocolType，表示服务协议，最低 4 位取值如下：
	 *               0x01-IPTV TS RTSP，
	 *               0x02-HPD
	 *               0x04-ISMA RTSP
	 *               0x08-HLS。
	 *            如果 Domain 为空，则采用该 CMSID 缺省分发域，缺省分发域可配置
	 * @var string Hotdegree
	 *     说明： 发布到融合 CDN 时使用热度表示
	 *     注释： 0（普通），1（高） 缺省为 0，该字段值越大，表示优先级越高。CDN 应根据优先级安排处理顺序
	 * @var string deliverTime
	 *     说明： 内容分发时间
	 *     注释： 北京时间，采用十四位的字 符 串 格 式YYYYMMDDHHMMSS，例如： 20040101123000 ， 代 表 2004 年 1 月 1 日 12点 30 分 00 秒。说明 如果不携带该字段，表示立即分发
	 */
	private function make_vod_media_xml($arr_info,$str_Action,$arr_info_ex=null)
	{
	    $str_xml ='<Object ElementType="Movie" ID="'.$arr_info['nns_cdn_media_guid'].'" Action="'.$str_Action.'" Type="1">';
	    // 		$str_xml.=	'<Property Name="FileURL">http://211.156.185.107:10000/ott/运营上传勿动/月光男孩.BD.720p.中英双字幕.mkv</Property>';
	    $str_xml.=	'<Property Name="FileURL">'.$arr_info['nns_url'].'</Property>';
	    $str_xml.=	'<Property Name="CPContentID">'.$arr_info['nns_cp_id'].'</Property>';
	    $str_xml.= 	'<Property Name="SourceDRMType">0</Property>';
	    $str_xml.= 	'<Property Name="DestDRMType">0</Property>';
	    $str_xml.= 	'<Property Name="AudioType">1</Property>';
	    $str_xml.= 	'<Property Name="ScreenFormat">1</Property>';
	    $str_xml.= 	'<Property Name="ClosedCaptioning">1</Property>';
	    $str_xml.= 	'<Property Name="Tags">'.$arr_info['nns_tag'].'</Property>';
	    $str_xml.=	'<Property Name="Duration">'.$arr_info['nns_file_time_len'].'</Property>';
	    $str_xml.=	'<Property Name="FileSize">'.$arr_info['nns_file_size'].'</Property>';
	    $str_xml.=	'<Property Name="BitRateType">'.$arr_info['nns_kbps'].'</Property>';
	    $str_xml.=	'<Property Name="VideoType">1</Property>';
	    $str_xml.=	'<Property Name="FileType">'.$arr_info['nns_filetype'].'</Property>';
	    $str_xml.=	'<Property Name="FrameRate">'.$arr_info['nns_file_frame_rate'].'</Property>';
	    $str_xml.=	'<Property Name="Dimensions">'.$arr_info['nns_dimensions'].'</Property>';
	    $str_xml.=	'<Property Name="AudioEncodingType">4</Property>';
	    $str_xml.=	'<Property Name="Resolution">'.$arr_info['nns_file_resolution'].'</Property>';
	    $str_xml.=	'<Property Name="MediaMode">1</Property>';
	    $str_xml.=	'<Property Name="SystemLayer">1</Property>';
	    $str_xml.=	'<Property Name="ServiceType">'.$arr_info['nns_media_service'].'</Property>';
	    $str_xml.=	'<Property Name="Domain">'.$arr_info['nns_domain'].'</Property>';
	    $str_xml.=	'<Property Name="Hotdegree">0</Property>';
	    $str_xml.='</Object>';
	    return $str_xml;
	}
	
	
	private function do_media_self($result_video,$result_index,$result_media)
	{
	    $date = date("Ymd");
	    $temp_guid = np_guid_rand();
	    $action = ($this->arr_params['bk_c2_task_info']['nns_action'] !='destroy') ? ($this->arr_params['bk_c2_task_info']['nns_action'] == 'update') ? 'UPDATE' : 'REGIST' : 'DELETE';
	    $xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
	    $xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
	    $xml_str .= 	'<Objects>';
	    $xml_str .=     $this->make_vod_media_xml($result_media,$action);
	    $xml_str .= 	'</Objects>';
	    $xml_str .= 	'<Mappings>';
	    $xml_str .=     $this->make_mapping('Program','Movie',$result_index['nns_cdn_index_guid'],$result_media['nns_cdn_media_guid'],$action);
	    $xml_str .=     $this->make_mapping('Series','Program',$result_video['nns_cdn_video_guid'],$result_index['nns_cdn_index_guid'],$action);
	    $xml_str .= 	'</Mappings>';
	    $xml_str .= '</ADI>';
	    
	    $file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'vod_media' . '_' . $action . '_' .  rand(100, 999) . '.xml';
	    $c2_info = array(
	        'nns_task_type'=>'Movie',
	        'nns_task_id'=> isset($this->arr_params['nns_task_id'])?$this->arr_params['nns_task_id']:null,
	        'nns_task_name'=> isset($this->arr_params['nns_task_name'])?$this->arr_params['nns_task_name']:null,
	        'nns_action'=>	$action,
	        'nns_url' => $file_name,
	        'nns_content' => $xml_str,
	        'nns_desc' => 'Movie,'.$action,
	        'nns_id'=>np_guid_rand(),
	    );
	    $result = $this->execute_self_bk_c2($c2_info);
	    if($result['ret'] !=0)
	    {
	        return $result;
	    }
	    $data_ftp = null;
	    $data_ftp[] = array(
	        'source_file'=>$result['data_info'],
	        'remote_file'=>$result_media['nns_cp_id'].'/'.$date.'/'.$temp_guid.'/'.$temp_guid.'.xml',
	    );
	    $ftp_url = isset($this->arr_sp_config['c2_import_push_dst_url']) ? $this->arr_sp_config['c2_import_push_dst_url'] : '';
	    $result_ftp = $this->push_xml_file($ftp_url,$data_ftp);
// 	    c2_task_model::save_c2_notify(array('nns_id'=>$c2_info['nns_id'],'nns_notify_result_url'=>'','nns_notify_result'=>$result_ftp['ret']));
	    $result_ftp['data_info'] = $date.'/'.$temp_guid;
	    return $result_ftp;
	}
	
	
	/**
	 * 片源CDN
	 * @param unknown $movie_id
	 */
	public function do_media()
	{
	    if(!isset($this->arr_params['nns_media_id']) || empty($this->arr_params['nns_media_id']))
		{
			return $this->_make_return_data(1,'点播传入参数无nns_media_id字段,参数为:'.var_export($this->arr_params));
		}
		$movie_id = $this->arr_params['nns_media_id'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_class/ftp/ftp_push.class.php';
		$result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $movie_id);
		if($result_media['ret'] !=0)
		{
			return $result_media;
		}
		if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
		{
			nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=7,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
			return $result_media;
		}
		$result_media = $result_media['data_info'];
		$result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $movie_id,$result_media);
		if((int)$this->arr_sp_config['disabled_clip'] !== 2 && (int)$this->arr_sp_config['disabled_clip'] !== 3)
		{
		    $result_media['nns_url'] = ltrim((trim($this->arr_params['nns_file_path'])),'/');
		    if(isset($this->arr_sp_config['media_ftp']) && !empty($this->arr_sp_config['media_ftp']) && $this->str_sp_id !='jscn_china_mobile')
		    {
		        $this->arr_sp_config['media_ftp'] = rtrim((trim($this->arr_sp_config['media_ftp'])),'/');
		        if (stripos($result_media['nns_url'], 'http://') === FALSE && stripos($result_media['nns_url'], 'ftp://') === FALSE)
		        {
		            $result_media['nns_url'] = $this->arr_sp_config['media_ftp'].'/'.$result_media['nns_url'];
		        }
		    }
		}
		else
		{
		    $result_media['nns_url'] = ltrim((trim($result_media['nns_url'])),'/');
		    if(isset($this->arr_sp_config['media_ftp']) && !empty($this->arr_sp_config['media_ftp'])  && $this->str_sp_id !='jscn_china_mobile')
		    {
		        $this->arr_sp_config['media_ftp'] = rtrim((trim($this->arr_sp_config['media_ftp'])),'/');
		        if (stripos($result_media['nns_url'], 'http://') === FALSE && stripos($result_media['nns_url'], 'ftp://') === FALSE)
		        {
		            $result_media['nns_url'] = $this->arr_sp_config['media_ftp'].'/'.$result_media['nns_url'];
		        }
		    }
		}
		if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
		{
			return $result_media_import_info;
		}
		$result_media['nns_cdn_media_guid'] = $result_media_import_info['data_info'];
		$arr_nns_file_resolution = explode('*', $result_media['nns_file_resolution']);
		$result_media['FrameHeight'] = (isset($arr_nns_file_resolution[0]) && (int)$arr_nns_file_resolution[0] >0) ? $arr_nns_file_resolution[0] : 1080;
		$result_media['FrameWidth'] = (isset($arr_nns_file_resolution[1]) && (int)$arr_nns_file_resolution[1] >0) ? $arr_nns_file_resolution[1] : 1920;
		$result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $result_media['nns_vod_index_id']);
		if($result_index['ret'] !=0)
		{
		    return $result_index;
		}
		if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
		{
		    nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=7,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
		    return $result_index;
		}
		$result_index = $result_index['data_info'][0];
		$result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $result_media['nns_vod_index_id'],$result_index);
		
		if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
		{
		    return $result_index_import_info;
		}
		$result_index['nns_cdn_index_guid'] = $result_index_import_info['data_info'];
		
		$result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_media['nns_vod_id']);
		if($result_video['ret'] !=0)
		{
		    return $result_video;
		}
		if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
		{
		    nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=7,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
		    return $result_video;
		}
		$result_video = $result_video['data_info'][0];
		$result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_media['nns_vod_id'],$result_video);
		
		if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
		{
		    return $result_video_import_info;
		}
		$result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
		
		$result_cp_config = nl_cp::query_by_id($this->obj_dc, $result_video['nns_cp_id']);
		if($result_cp_config['ret'] !=0)
		{
		    return $result_cp_config;
		}
		$this->arr_cp_config = (isset($result_cp_config['data_info']['nns_config']) && strlen($result_cp_config['data_info']['nns_config']) >0) ? json_decode($result_cp_config['data_info']['nns_config'],true) : null;
		
		$date = date("Ymd");
		$temp_guid = np_guid_rand();
		if($this->str_sp_id == 'sichuan_xiaole')
		{
		    $temp_guid = $this->arr_params['nns_task_id'];
		}
		
		
		if(((strlen($result_media['nns_cp_id']) <= 5 || substr($result_media['nns_cp_id'], 0,5) != 'fuse_')) && $this->is_json($result_media['nns_ext_info']))
		{
		    $nns_ext_info = json_decode($result_media['nns_ext_info'],true);
		    if(isset($nns_ext_info['path']) && strlen($nns_ext_info['path']) >0)
		    {
		        $temp_media_source_url = trim(trim($nns_ext_info['path'],'/'),"\\");
		    }
		    else
		    {
		        $temp_media_source_url = trim(trim($result_media['nns_url'],'/'),"\\");
		    }
		}
		else
		{
		    $temp_media_source_url = trim(trim($result_media['nns_url'],'/'),"\\");
		}
		$temp_file_path = pathinfo($temp_media_source_url);
		if(!isset($temp_file_path['extension']) || strlen($temp_file_path['extension']) <1)
		{
		    nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=7,nns_epg_status=100 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
		    return $this->_make_return_data(1,'temp set');
		}
		$temp_file_path['extension'] = strtolower($temp_file_path['extension']);
		$result_media['nns_filetype'] = $temp_file_path['extension'];
// 		if(strtolower($result_media['nns_filetype']) !='ts' || !isset($temp_file_path['extension']) || strtolower($temp_file_path['extension']) !='ts')
// 		{
// 		    nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=7,nns_epg_status=100 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
// 		    return $this->_make_return_data(1,'temp set');
// 		}
		if(isset($this->arr_sp_config['cdn_media_playurl_by_csv']) && $this->arr_sp_config['cdn_media_playurl_by_csv'])
		{
		      $temp_desc_url = $result_media['nns_cp_id'] .'/' . $temp_guid . '/' . $temp_guid.'.'.$temp_file_path['extension'];
		}
		else
		{
		      $temp_desc_url = $result_media['nns_cp_id'].'/'.$date .'/' . $temp_guid . '/' . $temp_guid.'.'.$temp_file_path['extension'];
		}
		$result_media['nns_url'] = $temp_desc_url;
		$action = ($this->arr_params['bk_c2_task_info']['nns_action'] !='destroy') ? ($this->arr_params['bk_c2_task_info']['nns_action'] == 'update') ? 'UPDATE' : 'REGIST' : 'DELETE';
		$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
		$xml_str .= 	'<Objects>';
		$xml_str .=     $this->make_vod_media_xml($result_media,$action);
		$xml_str .= 	'</Objects>';
		$xml_str .= 	'<Mappings>';
		$xml_str .=     $this->make_mapping('Program','Movie',$result_index['nns_cdn_index_guid'],$result_media['nns_cdn_media_guid'],$action);
		$xml_str .=     $this->make_mapping('Series','Program',$result_video['nns_cdn_video_guid'],$result_index['nns_cdn_index_guid'],$action);
		$xml_str .= 	'</Mappings>';
		$xml_str .= '</ADI>';

		$str_guid = $temp_guid;
		$file_name = $this->str_sp_id . '_' . date('His') . '_' . $str_guid . '_' . 'vod_media' . '_' . $action . '.xml';
	    $c2_info = array(
	        'nns_task_type'=>'Movie',
	        'nns_task_id'=> isset($this->arr_params['nns_task_id'])?$this->arr_params['nns_task_id']:null,
	        'nns_task_name'=> isset($this->arr_params['nns_task_name'])?$this->arr_params['nns_task_name']:null,
	        'nns_action'=>	$action,
	        'nns_url' => $file_name,
	        'nns_content' => $xml_str,
	        'nns_desc' => 'Movie,'.$action,
	        'nns_id'=>$str_guid,
	    );
	    $result = $this->execute_self_bk_c2($c2_info);
		if($result['ret'] !=0)
	    {
	        return $result;
	    }
	    $data_ftp = null;
	    if(isset($this->arr_sp_config['cdn_media_playurl_by_csv']) && $this->arr_sp_config['cdn_media_playurl_by_csv'])
	    {
	        $data_ftp[] = array(
    	        'source_file'=>$result['data_info'],
    	        'remote_file'=>$result_media['nns_cp_id'].'/'.$temp_guid.'/'.$temp_guid.'.xml',
    	    );
	    }
	    else
	    {
    	    $data_ftp[] = array(
    	        'source_file'=>$result['data_info'],
    	        'remote_file'=>$result_media['nns_cp_id'].'/'.$date.'/'.$temp_guid.'/'.$temp_guid.'.xml',
    	    );
	    }
	    
	    if($result_media['nns_cp_id'] == 'xiaole_mp3')
	    {
	        $local_file_path = '/mnt/'.$temp_media_source_url;
	    }
	    else
	    {
            $local_file_path = isset($this->arr_sp_config['c2_import_push_media_base_url']) ? trim(trim($this->arr_sp_config['c2_import_push_media_base_url'],'/'),'\\') : '';
            $local_file_path = (strlen($local_file_path) >0) ? '/'.$local_file_path.'/'.$temp_media_source_url : '/'.$temp_media_source_url;
	    }
        $file_params = array(
            'des_file_path'=>$temp_desc_url,
            'local_file_path'=>$local_file_path,
            'queue_id'=>$c2_info['nns_id'],
            'request_url'=>$this->arr_sp_config['c2_import_push_go_url'],
            'org_id'=>$this->str_sp_id,
        );

        $ftp_url = isset($this->arr_sp_config['c2_import_push_dst_url']) ? $this->arr_sp_config['c2_import_push_dst_url'] : '';
        if(isset($this->arr_sp_config['cdn_media_playurl_by_csv']) && $this->arr_sp_config['cdn_media_playurl_by_csv'])
        {
            $result_ftp = $this->create_file_dir($ftp_url,$data_ftp);
            if($result_ftp['ret'] !=0)
            {
                $edit_c2_info = array(
                    'nns_result'=>"[-1]",
                    'nns_notify_result'=>-1,
                    'nns_notify_fail_reason'=>$result_ftp['reason'],
                );
                nl_c2_log::edit($this->obj_dc, $edit_c2_info,$c2_info['nns_id']);
                return $result_ftp;
            }
        }
        else
        {
            $result_ftp = $this->push_xml_file($ftp_url,$data_ftp);
            if($result_ftp['ret'] !=0)
            {
                $edit_c2_info = array(
                    'nns_result'=>"[-1]",
                    'nns_notify_result'=>-1,
                    'nns_notify_fail_reason'=>$result_ftp['reason'],
                );
                nl_c2_log::edit($this->obj_dc, $edit_c2_info,$c2_info['nns_id']);
                return $result_ftp;
            }
        }
        $obj_ftp_push = new ftp_push();
        $result_ftp_push  =  $obj_ftp_push->go_push_ts_media($file_params);
        if($result_ftp_push['ret'] !=0)
        {
            $edit_c2_info = array(
                'nns_result'=>"[-1]",
                'nns_notify_result'=>-1,
                'nns_notify_fail_reason'=>$result_ftp_push['reason'],
            );
            nl_c2_log::edit($this->obj_dc, $edit_c2_info,$c2_info['nns_id']);
        }
        return $result_ftp_push;
	}
	
	
	/**
	 * 片源CDN
	 * @param unknown $movie_id
	 */
	public function do_media_v2()
	{
	    if(!isset($this->arr_params['nns_media_id']) || empty($this->arr_params['nns_media_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_media_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $movie_id = $this->arr_params['nns_media_id'];
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_class/ftp/ftp_push.class.php';
	    $result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $movie_id);
	    if($result_media['ret'] !=0)
	    {
	        return $result_media;
	    }
	    if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_media;
	    }
	
	    $result_c2_task_log = nl_c2_log::query_by_task_id($this->obj_dc, $this->arr_params['nns_task_id']);
	    if($result_c2_task_log['ret'] !=0)
	    {
	        return $result_c2_task_log;
	    }
	    $result_c2_task_log['data_info'] = (isset($result_c2_task_log['data_info'][0]) && is_array($result_c2_task_log['data_info'][0]) && !empty($result_c2_task_log['data_info'][0])) ? $result_c2_task_log['data_info'][0] : null;
	    if(isset($result_c2_task_log['data_info']['nns_notify_result']) && strlen($result_c2_task_log['data_info']['nns_notify_result']) <1)
	    {
	        return $result_c2_task_log;
	    }
	    $result_media = $result_media['data_info'];
	    $result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $movie_id,$result_media);
	    if((int)$this->arr_sp_config['disabled_clip'] !== 2 && (int)$this->arr_sp_config['disabled_clip'] !== 3)
	    {
	        $result_media['nns_url'] = ltrim((trim($this->arr_params['nns_file_path'])),'/');
	        if(isset($this->arr_sp_config['media_ftp']) && !empty($this->arr_sp_config['media_ftp']))
	        {
	            $this->arr_sp_config['media_ftp'] = rtrim((trim($this->arr_sp_config['media_ftp'])),'/');
	            if (stripos($result_media['nns_url'], 'http://') === FALSE && stripos($result_media['nns_url'], 'ftp://') === FALSE)
	            {
	                $result_media['nns_url'] = $this->arr_sp_config['media_ftp'].'/'.$result_media['nns_url'];
	            }
	        }
	    }
	    else
	    {
	        $result_media['nns_url'] = ltrim((trim($result_media['nns_url'])),'/');
	        if(isset($this->arr_sp_config['media_ftp']) && !empty($this->arr_sp_config['media_ftp']))
	        {
	            $this->arr_sp_config['media_ftp'] = rtrim((trim($this->arr_sp_config['media_ftp'])),'/');
	            if (stripos($result_media['nns_url'], 'http://') === FALSE && stripos($result_media['nns_url'], 'ftp://') === FALSE)
	            {
	                $result_media['nns_url'] = $this->arr_sp_config['media_ftp'].'/'.$result_media['nns_url'];
	            }
	        }
	    }
	    if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
	    {
	        return $result_media_import_info;
	    }
	    $result_media['nns_cdn_media_guid'] = $result_media_import_info['data_info'];
	    $action = ($this->arr_params['bk_c2_task_info']['nns_action'] !='destroy') ? ($this->arr_params['bk_c2_task_info']['nns_action'] == 'update') ? 'UPDATE' : 'REGIST' : 'DELETE';
	    
	    if(isset($result_c2_task_log['data_info']['nns_notify_result']) && $result_c2_task_log['data_info']['nns_notify_result']!='0')
	    {
	        $date = date("Ymd");
	        $temp_guid = np_guid_rand();
	        $temp_media_source_url = trim(trim($result_media['nns_url'],'/'),"\\");
	        $ftp_url = isset($this->arr_sp_config['c2_import_push_dst_url']) ? $this->arr_sp_config['c2_import_push_dst_url'] : '';
	        $arr_ftp_config = parse_url($ftp_url);
	        $path_ftp = (isset($arr_ftp_config["path"]) && strlen($arr_ftp_config["path"]) > 0) ? $arr_ftp_config["path"] : '';
	        $path_ftp = trim(trim($path_ftp,'/'));
	        $path_ftp = (strlen($path_ftp) >0) ? '/'.$path_ftp : '/';
	        $temp_desc_url = $path_ftp.'/'.$date .'/' . $temp_guid . '/' . $temp_guid.'.ts';
	        $temp_desc_url = trim(trim($temp_desc_url,'/'));
	        $temp_desc_url_xml = $date .'/' . $temp_guid . '/' . $temp_guid.'.xml';
            $local_file_path = isset($this->arr_sp_config['c2_import_push_media_base_url']) ? trim(trim($this->arr_sp_config['c2_import_push_media_base_url'],'/'),'\\') : '';
            $local_file_path = (strlen($local_file_path) >0) ? '/'.$local_file_path.'/'.$temp_media_source_url : '/'.$temp_media_source_url;
            
            $file_params = array(
                'des_file_path'=>$temp_desc_url,
                'local_file_path'=>$local_file_path,
                'queue_id'=>$temp_guid,
                'request_url'=>$this->arr_sp_config['c2_import_push_go_url'],
            );
            $obj_ftp_push = new ftp_push();
            $result = $obj_ftp_push->go_push_ts_media($file_params);
            $c2_info = array(
                'nns_task_type'=>'Movie',
                'nns_task_id'=> isset($this->arr_params['nns_task_id'])?$this->arr_params['nns_task_id']:null,
                'nns_task_name'=> isset($this->arr_params['nns_task_name'])?$this->arr_params['nns_task_name']:null,
                'nns_action'=>	$action,
                'nns_url' => '',
                'nns_content' => '',
                'nns_desc' => 'Movie,'.$action,
                'nns_id'=>$temp_guid,
                'nns_ex_info'=>json_encode(array('xml'=>$temp_desc_url_xml,'file'=>$temp_desc_url)),
                'nns_type'=>'std',
                'nns_org_id'=>$this->str_sp_id,
                'nns_notify_result'=>'',
                'nns_result'=>"[{$result['ret']}]",
            );
            return nl_c2_log::add($this->obj_dc, $c2_info);
	    }
	    $result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $result_media['nns_vod_index_id']);
	    if($result_index['ret'] !=0)
	    {
	        return $result_index;
	    }
	    if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_index;
	    }
	    $result_index = $result_index['data_info'][0];
	    $result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $result_media['nns_vod_index_id'],$result_index);
	
	    if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
	    {
	        return $result_index_import_info;
	    }
	    $result_index['nns_cdn_index_guid'] = $result_index_import_info['data_info'];
	
	    $result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_media['nns_vod_id']);
	    if($result_video['ret'] !=0)
	    {
	        return $result_video;
	    }
	    if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_video;
	    }
	    $result_video = $result_video['data_info'][0];
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_media['nns_vod_id'],$result_video);
	
	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        return $result_video_import_info;
	    }
	    $result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
	
	    $result_cp_config = nl_cp::query_by_id($this->obj_dc, $result_video['nns_cp_id']);
	    if($result_cp_config['ret'] !=0)
	    {
	        return $result_cp_config;
	    }
	    $result_c2_task_log['data_info']['nns_ex_info'] = json_decode($result_c2_task_log['data_info']['nns_ex_info'],true);
	    $this->arr_cp_config = (isset($result_cp_config['data_info']['nns_config']) && strlen($result_cp_config['data_info']['nns_config']) >0) ? json_decode($result_cp_config['data_info']['nns_config'],true) : null;
	    $result_media['nns_url'] = $result_c2_task_log['data_info']['nns_ex_info']['file'];
	    $action = ($this->arr_params['bk_c2_task_info']['nns_action'] !='destroy') ? ($this->arr_params['bk_c2_task_info']['nns_action'] == 'update') ? 'UPDATE' : 'REGIST' : 'DELETE';
	    $xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
	    $xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
	    $xml_str .= 	'<Objects>';
	    $xml_str .=     $this->make_vod_media_xml($result_media,$action);
	    $xml_str .= 	'</Objects>';
	    $xml_str .= 	'<Mappings>';
	    $xml_str .=     $this->make_mapping('Program','Movie',$result_index['nns_cdn_index_guid'],$result_media['nns_cdn_media_guid'],$action);
	    $xml_str .=     $this->make_mapping('Series','Program',$result_video['nns_cdn_video_guid'],$result_index['nns_cdn_index_guid'],$action);
	    $xml_str .= 	'</Mappings>';
	    $xml_str .= '</ADI>';
	
	    $file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'vod_media' . '_' . $action . '_' .  rand(100, 999) . '.xml';
	    $sub_path = date('Y-m-d');
	    //生成soap xml文件 路径
	    $file_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/mgtv/' . $this->str_sp_id . '/inject_xml/' . $sub_path . '/';
	    if (!is_dir($file_path))
	    {
	        mkdir($file_path, 0777, true);
	    }
	    $file_path .= $file_name;
	    $result = @file_put_contents($file_path, $xml_str);
	    if(!$result)
	    {
	        return nl_c2_log::edit($this->obj_dc, array('nns_result'=>'[-1]'), $result_c2_task_log['data_info']['nns_id']); 
	    }
	    $data_ftp = null;
	    $data_ftp[] = array(
	        'source_file'=>$file_path,
	        'remote_file'=>$result_c2_task_log['data_info']['nns_ex_info']['xml'],
	    );
	    $ftp_url = isset($this->arr_sp_config['c2_import_push_dst_url']) ? $this->arr_sp_config['c2_import_push_dst_url'] : '';
	    $result_ftp = $this->push_xml_file($ftp_url,$data_ftp);
	    if($result_ftp['ret'] !=0)
	    {
	        return nl_c2_log::edit($this->obj_dc, array('nns_result'=>'[-1]'), $result_c2_task_log['data_info']['nns_id']);
	    }
	    nl_c2_log::edit($this->obj_dc, array('nns_url'=>$sub_path . '/' . $file_name,'nns_content'=>$xml_str,'nns_result'=>'[0]'), $result_c2_task_log['data_info']['nns_id']);
	    nl_c2_task::edit($this->obj_dc, array('nns_status'=>0), $this->arr_params['nns_task_id']);
	    $queue_task_model = new queue_task_model();
	    return $queue_task_model->q_report_task($this->arr_params['nns_task_id']);
	}
	
	
	/**
	 * 点播主媒资CDN
	 * @param unknown $movie_id
	 */
	public function do_video()
	{
	    $date = date("Ymd");
	    $temp_guid = np_guid_rand();
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_video_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $video_id = $this->arr_params['bk_c2_task_info']['nns_ref_id'];
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/depot/depot.class.php';
	
	    $result_video = nl_vod::get_video_info_by_id($this->obj_dc, $video_id);
	    if($result_video['ret'] !=0)
	    {
	        return $result_video;
	    }
	    if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['bk_c2_task_info']['nns_id']}'", $this->obj_dc->db());
	        return $result_video;
	    }
	    $result_video = $result_video['data_info'][0];
	    
	    $result_cp_config = nl_cp::query_by_id($this->obj_dc, $result_video['nns_cp_id']);
	    if($result_cp_config['ret'] !=0)
	    {
	        return $result_cp_config;
	    }
	    $this->arr_cp_config = (isset($result_cp_config['data_info']['nns_config']) && strlen($result_cp_config['data_info']['nns_config']) >0) ? json_decode($result_cp_config['data_info']['nns_config'],true) : null;
	    
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $video_id,$result_video);
	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        return $result_video_import_info;
	    }
	    $result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
	    
	    $result_depot = nl_depot::get_depot_info($this->obj_dc, null,'video');
	    
	    if($result_depot['ret'] !=0)
	    {
	        return $result_depot;
	    }
	    $result_video['nns_cdn_category_name'] = (isset($result_video['nns_all_index']) && (int)$result_video['nns_all_index'] >1) ? '电视剧' : '电影';
	    $result_video['nns_cdn_category_id'] = '';
	    if(isset($result_depot['data_info'][0]['nns_category']) && strlen($result_depot['data_info'][0]['nns_category']) >0 && $this->is_xml($result_depot['data_info'][0]['nns_category']))
	    {
	        $dom = new DOMDocument('1.0', 'utf-8');
	        $dom->loadXML($result_depot['data_info'][0]['nns_category']);
	        $categorys = $dom->getElementsByTagName('category');
	        foreach ($categorys as $category)
	        {
	            $nns_cdn_category_id = (string)$category->getAttribute('id');
	            if($nns_cdn_category_id == $result_video['nns_category_id'])
	            {
	                $result_video['nns_cdn_category_id'] = $nns_cdn_category_id;
	                $result_video['nns_cdn_category_name'] = $category->getAttribute('name');
	                break;
	            }
	        }
	    }
	    
	    $action = ($this->arr_params['bk_c2_task_info']['nns_action'] !='destroy') ? ($this->arr_params['bk_c2_task_info']['nns_action'] == 'update') ? 'UPDATE' : 'REGIST' : 'DELETE';
	    // 	    $action = ($result_video['nns_deleted'] !=1) ? ($result_video['nns_modify_time'] > $result_video['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
	    $result_picture = $this->make_image_self($result_video, 'Series', $result_video['nns_cdn_video_guid'], $action,$date,$temp_guid);
	    $xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
	    $xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
	    $xml_str .= 	'<Objects>';
	    $xml_str .=     $this->make_vod_xml($result_video,$action);
	    if(isset($result_picture['picture']) && strlen($result_picture['picture']) >0)
	    {
	        $xml_str .= $result_picture['picture'];
	    }
	    $xml_str .= 	'</Objects>';
	    if(isset($result_picture['mapping']) && strlen($result_picture['mapping']) >0)
	    {
	        $xml_str .= 	'<Mappings>';
	        $xml_str .= $result_picture['mapping'];
	        $xml_str .= 	'</Mappings>';
	    }
	    $xml_str .= '</ADI>';
	    $str_guid = $temp_guid;
	    $file_name = $this->str_sp_id . '_' . date('His') . '_' . $str_guid . '_' . 'vod_video' . '_' . $action . '.xml';
	    $c2_info = array(
	        'nns_task_type'=>'Series',
	        'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	        'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
	        'nns_action'=>	$action,
	        'nns_url' => $file_name,
	        'nns_content' => $xml_str,
	        'nns_desc' => 'Series,'.$action,
	        'nns_id'=>$str_guid,
	    );
	    $result = $this->execute_self_bk_c2($c2_info);
	    if($result['ret'] !=0)
	    {
	        return $result;
	    }
	    $data_ftp = null;
	    $data_ftp[] = array(
	        'source_file'=>$result['data_info'],
	        'remote_file'=>$result_video['nns_cp_id'].'/'.$date.'/'.$temp_guid.'/'.$temp_guid.'.xml',
	    );
	    if(isset($result_picture['base_path']) && is_array($result_picture['base_path']) && !empty($result_picture['base_path']))
	    {
	        foreach ($result_picture['base_path'] as $file_temp_val)
	        {
	            $data_ftp[] = $file_temp_val;
	        }
	    }
	    $ftp_url = isset($this->arr_sp_config['c2_import_push_dst_url']) ? $this->arr_sp_config['c2_import_push_dst_url'] : '';
	    $result_ftp = $this->push_xml_file($ftp_url,$data_ftp);
	    
	    c2_task_model::save_c2_notify(array('nns_id'=>$c2_info['nns_id'],'nns_notify_result_url'=>'','nns_notify_result'=>$result_ftp['ret']));
	    return $result_ftp;
	}
	
	/**
	 * 点播分集CDN
	 * @param unknown $movie_id
	 */
	public function do_index()
	{
	    $date = date("Ymd");
	    $temp_guid = np_guid_rand();
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_index_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $index_id = $this->arr_params['bk_c2_task_info']['nns_ref_id'];
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	    $result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $index_id);
	    if($result_index['ret'] !=0)
	    {
	        return $result_index;
	    }
	    if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['bk_c2_task_info']['nns_id']}'", $this->obj_dc->db());
	        return $result_index;
	    }
	    $result_index = $result_index['data_info'][0];
	    $result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $index_id,$result_index);
	    if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
	    {
	        return $result_index_import_info;
	    }
	    $result_index['nns_cdn_index_guid'] = $result_index_import_info['data_info'];
	     
	     
	    $result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_index['nns_vod_id']);
	    if($result_video['ret'] !=0)
	    {
	        return $result_video;
	    }
	    if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['bk_c2_task_info']['nns_id']}'", $this->obj_dc->db());
	        return $result_video;
	    }
	    $result_video = $result_video['data_info'][0];
	    
	    $result_cp_config = nl_cp::query_by_id($this->obj_dc, $result_video['nns_cp_id']);
	    if($result_cp_config['ret'] !=0)
	    {
	        return $result_cp_config;
	    }
	    $this->arr_cp_config = (isset($result_cp_config['data_info']['nns_config']) && strlen($result_cp_config['data_info']['nns_config']) >0) ? json_decode($result_cp_config['data_info']['nns_config'],true) : null;
	    
	    
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_index['nns_vod_id'],$result_video);
	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        return $result_video_import_info;
	    }
	    $result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
	    $action = ($this->arr_params['bk_c2_task_info']['nns_action'] !='destroy') ? ($this->arr_params['bk_c2_task_info']['nns_action'] == 'update') ? 'UPDATE' : 'REGIST' : 'DELETE';
	    // 	    $action = ($result_index['nns_deleted'] !=1) ? ($result_index['nns_modify_time'] > $result_index['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
	    $result_picture = $this->make_image_self($result_index, 'Program', $result_index['nns_cdn_video_guid'], $action,$date,$temp_guid);
	    $result_index['nns_index'] = (isset($result_index['nns_index']) && $result_index['nns_index'] >=0) ? $result_index['nns_index']+1 : 1;
	    $xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
	    $xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
	    $xml_str .= 	'<Objects>';
	    $xml_str .=     $this->make_vod_index_xml($result_index,$action);
	    if(isset($result_picture['picture']) && strlen($result_picture['picture']) >0)
	    {
	        $xml_str .= $result_picture['picture'];
	    }
	    $xml_str .= 	'</Objects>';
	    $xml_str .= 	'<Mappings>';
	    if(isset($result_picture['mapping']) && strlen($result_picture['mapping']) >0)
	    {
	        $xml_str .= $result_picture['mapping'];
	    }
	    $xml_str .=     $this->make_mapping('Series','Program',$result_video['nns_cdn_video_guid'],$result_index['nns_cdn_index_guid'],$action,array('Sequence'=>$result_index['nns_index']));
	    $xml_str .= 	'</Mappings>';
	    $xml_str .= '</ADI>';

	    $str_guid = $temp_guid;
	    $file_name = $this->str_sp_id . '_' . date('His') . '_' . $str_guid . '_' . 'vod_index' . '_' . $action . '.xml';
	    $c2_info = array(
	        'nns_task_type'=>'Program',
	        'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	        'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
	        'nns_action'=>	$action,
	        'nns_url' => $file_name,
	        'nns_content' => $xml_str,
	        'nns_desc' => 'Program,'.$action,
	        'nns_id'=>$str_guid,
	    );
	    $result = $this->execute_self_bk_c2($c2_info);
	    if($result['ret'] !=0)
	    {
	        return $result;
	    }
	    $data_ftp = null;
	    $data_ftp[] = array(
	        'source_file'=>$result['data_info'],
	        'remote_file'=>$result_index['nns_cp_id'].'/'.$date.'/'.$temp_guid.'/'.$temp_guid.'.xml',
	    );
	    if(isset($result_picture['base_path']) && is_array($result_picture['base_path']) && !empty($result_picture['base_path']))
	    {
	        foreach ($result_picture['base_path'] as $file_temp_val)
	        {
	            $data_ftp[] = $file_temp_val;
	        }
	    }
	    $ftp_url = isset($this->arr_sp_config['c2_import_push_dst_url']) ? $this->arr_sp_config['c2_import_push_dst_url'] : '';
	    $result_ftp = $this->push_xml_file($ftp_url,$data_ftp);
	    c2_task_model::save_c2_notify(array('nns_id'=>$c2_info['nns_id'],'nns_notify_result_url'=>'','nns_notify_result'=>$result_ftp['ret']));
	    return $result_ftp;
	}
	
	/**
     *推送文件信息给下游
	 */
	private function push_xml_file($ftp_url,$arr_file)
	{
	    $ftp_url = trim($ftp_url,'/');
	    if(strlen($ftp_url) <1)
	    {
	        return $this->_make_return_data(1,'sp c2_import_push_dst_url not set');
	    }
	    $arr_ftp_config = parse_url($ftp_url);
	    $host_ftp = (isset($arr_ftp_config["host"]) && strlen($arr_ftp_config["host"]) > 0) ? $arr_ftp_config["host"] : '';
	    $user_ftp = (isset($arr_ftp_config["user"]) && strlen($arr_ftp_config["user"]) > 0) ? $arr_ftp_config["user"] : '';
	    $pass_ftp = (isset($arr_ftp_config["pass"]) && strlen($arr_ftp_config["pass"]) > 0) ? $arr_ftp_config["pass"] : '';
	    $path_ftp = (isset($arr_ftp_config["path"]) && strlen($arr_ftp_config["path"]) > 0) ? $arr_ftp_config["path"] : '';
	    $port_ftp = (isset($arr_ftp_config["port"]) && strlen($arr_ftp_config["port"]) > 0) ? $arr_ftp_config["port"] : 21;
	    
	    $path_ftp = trim(trim($path_ftp,'/'));
	    $path_ftp = (strlen($path_ftp) >0) ? '/'.$path_ftp : '/'; 
	    foreach ($arr_file as $file_val)
	    {
	        $obj_ftp = new nl_ftp($host_ftp, $user_ftp, $pass_ftp,$port_ftp,30);
	        $result_upload = $obj_ftp->upload_file($path_ftp.'/'.$file_val['remote_file'],$file_val['source_file']);
	        $obj_ftp->my_destruct();
	        unset($obj_ftp);
	        if($result_upload['ret'] !=0)
	        {
	            for ($i=0;$i<2;$i++)
	            {
	                $obj_ftp = new nl_ftp($host_ftp, $user_ftp, $pass_ftp,$port_ftp,30);
	                $result_upload = $obj_ftp->upload_file($path_ftp.'/'.$file_val['remote_file'],$file_val['source_file']);
	                $obj_ftp->my_destruct();
	                unset($obj_ftp);
	                if($result_upload['ret'] == '0')
	                {
	                    break;
	                }
	            }
	            if($result_upload['ret'] !='0')
	            {
	                return $result_upload;
	            }
	        }
	    }
	    return $this->_make_return_data(0,'ftp upload ok');
	}
	
	/**
	 *推送文件信息给下游
	 */
	private function create_file_dir($ftp_url,$arr_file)
	{
	    $ftp_url = trim($ftp_url,'/');
	    if(strlen($ftp_url) <1)
	    {
	        return $this->_make_return_data(1,'sp c2_import_push_dst_url not set');
	    }
	    $arr_ftp_config = parse_url($ftp_url);
	    $host_ftp = (isset($arr_ftp_config["host"]) && strlen($arr_ftp_config["host"]) > 0) ? $arr_ftp_config["host"] : '';
	    $user_ftp = (isset($arr_ftp_config["user"]) && strlen($arr_ftp_config["user"]) > 0) ? $arr_ftp_config["user"] : '';
	    $pass_ftp = (isset($arr_ftp_config["pass"]) && strlen($arr_ftp_config["pass"]) > 0) ? $arr_ftp_config["pass"] : '';
	    $path_ftp = (isset($arr_ftp_config["path"]) && strlen($arr_ftp_config["path"]) > 0) ? $arr_ftp_config["path"] : '';
	    $port_ftp = (isset($arr_ftp_config["port"]) && strlen($arr_ftp_config["port"]) > 0) ? $arr_ftp_config["port"] : 21;
	     
	    $path_ftp = trim(trim($path_ftp,'/'));
	    $path_ftp = (strlen($path_ftp) >0) ? '/'.$path_ftp : '/';
	    foreach ($arr_file as $file_val)
	    {
	        $obj_ftp = new nl_ftp($host_ftp, $user_ftp, $pass_ftp,$port_ftp,30);
	        $result_upload = $obj_ftp->create_file_dir($path_ftp.'/'.$file_val['remote_file'],$file_val['source_file']);
	        $obj_ftp->my_destruct();
	        unset($obj_ftp);
	        if($result_upload['ret'] !=0)
	        {
	            for ($i=0;$i<2;$i++)
	            {
	                $obj_ftp = new nl_ftp($host_ftp, $user_ftp, $pass_ftp,$port_ftp,30);
	                $result_upload = $obj_ftp->create_file_dir($path_ftp.'/'.$file_val['remote_file'],$file_val['source_file']);
	                $obj_ftp->my_destruct();
	                unset($obj_ftp);
	                if($result_upload['ret'] == '0')
	                {
	                    break;
	                }
	            }
	            if($result_upload['ret'] !='0')
	            {
	                return $result_upload;
	            }
	        }
	    }
	    return $this->_make_return_data(0,'ftp file dir create ok');
	}
	
	
	/**
	 * 中国电信标准C2
	 * @param unknown $c2_info
	 */
	public function execute_self_bk_c2($c2_info)
	{
	    $c2_info['nns_type'] = 'std';
	    $c2_info['nns_org_id'] = $this->str_sp_id;
	    
	    $sub_path = date('Y-m-d');
	    $file = $c2_info['nns_url'];
	    $c2_info['nns_url'] = $sub_path . '/' . $file;
	    //生成soap xml文件 路径
	    $file_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/mgtv/' . $this->str_sp_id . '/inject_xml/' . $sub_path . '/';
	    if (!is_dir($file_path))
	    {
	        mkdir($file_path, 0777, true);
	    }
	    $file_path .= $file;
	    $result = @file_put_contents($file_path, $c2_info['nns_content']);
	    $CmdFileURL = trim(rtrim($this->arr_sp_config['xml_ftp'],'/')) . '/' . trim(ltrim($c2_info['nns_url'],'/'));
	    
	    $result = $result ? 0 : -1;
        $c2_info['nns_result'] = "[{$result}]";
        $c2_info['nns_notify_result'] = $result;
        $c2_info['nns_content'] = '';
        nl_c2_log::add($this->obj_dc, $c2_info);
	    return ($result== 0) ? $this->_make_return_data(0,'ok',$file_path) :  $this->_make_return_data(1,'error',$file_path);
	}
	
	
	/**
	 * 中国电信标准C2
	 * @param unknown $c2_info
	 */
	public function execute_self_bk_c3($c2_info)
	{
	    $c2_info['nns_type'] = 'std';
	    $c2_info['nns_org_id'] = $this->str_sp_id;
	     
	    $sub_path = date('Y-m-d');
	    $file = $c2_info['nns_url'];
	    $c2_info['nns_url'] = $sub_path . '/' . $file;
	    //生成soap xml文件 路径
	    $file_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/mgtv/' . $this->str_sp_id . '/inject_xml/' . $sub_path . '/';
	    if (!is_dir($file_path))
	    {
	        mkdir($file_path, 0777, true);
	    }
	    $file_path .= $file;
	    $result = @file_put_contents($file_path, $c2_info['nns_content']);
	    
	    $data_ftp = null;
	    $str_guid = np_guid_rand();
	    $str_path = date("Ymd").'/'.$str_guid.'.xml';
	    $data_ftp[] = array(
	        'source_file'=>$file_path,
	        'remote_file'=>$str_path,
	    );
	    
	    $ftp_url = isset($this->arr_sp_config['c2_import_push_dst_url']) ? $this->arr_sp_config['c2_import_push_dst_url'] : '';
        $result_ftp = $this->push_xml_file($ftp_url,$data_ftp);
        if($result_ftp['ret'] !=0)
        {
            $edit_c2_info = array(
                'nns_result'=>"[-1]",
                'nns_notify_result'=>-1,
                'nns_notify_fail_reason'=>$result_ftp['reason'],
            );
            nl_c2_log::edit($this->obj_dc, $edit_c2_info,$c2_info['nns_id']);
            return $result_ftp;
        }
	    $CmdFileURL = trim(rtrim($ftp_url,'/')) . '/' . trim(ltrim($str_path,'/'));
	    //加载soap客户端文件
	    $file_soap_client_file = dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv/' . $this->str_sp_id . '/client.php';
	    if(!file_exists($file_soap_client_file))
	    {
	        return $this->_make_return_data(1,'加载soap客户端文件不存在,文件路径:['.$file_soap_client_file.']');
	    }
	    include_once $file_soap_client_file;
	    //检查时间范围是否有效执行
	    if ($this->exists_c2_commond($c2_info))
	    {
	        return $this->_make_return_data(1,'该task在一小时内执行过或则task查询无数据,task信息:'.var_export($c2_info,true));
	    }
	    $c2_import_asset_url = $this->arr_sp_config['c2_import_asset_url'];
        try {
            $result = client::ExecCmd($c2_info['nns_id'], $CmdFileURL, $c2_import_asset_url,'media');
            $result = isset($result->Result) ? $result->Result : $result->result;
            $result = ($result === null) ? 0 : $result;
        } catch (Exception $e) {
            $result = false;
        }
	    $result = ($result || $result == '0') ? 0 : -1;
	    $c2_info['nns_result'] = "[{$result}]";
	    $c2_info['nns_notify_result'] = $result;
	    $c2_info['nns_content'] = '';
	    nl_c2_log::add($this->obj_dc, $c2_info);
	    return ($result== 0) ? $this->_make_return_data(0,'ok',$file_path) :  $this->_make_return_data(1,'error',$file_path);
	}
	
	/**
	 * 组装img
	 * @param unknown $arr_video
	 * @param unknown $type
	 */
	private function make_image_self($arr_video,$parent_type,$parent_id,$action,$date,$temp_guid)
	{
	    $str_base_img_url = isset($this->arr_cp_config['g_ftp_conf']['down_img_dir']) ? trim($this->arr_cp_config['g_ftp_conf']['down_img_dir']) : '';
	    $str_base_img_url = rtrim(rtrim($str_base_img_url,'/'),'\\');
	    $base_path_img = $arr_map = null;
	    $str_picture_obj = $str_picture_mapping = '';
	    $img_url = isset($this->arr_sp_config['img_ftp']) ? $this->arr_sp_config['img_ftp'] : '';
	    $img_url = rtrim(rtrim(trim($img_url),'/'),'\\');
	    switch ($parent_type)
	    {
	        case 'Series':
	            $arr_map = $this->arr_vod_video_img;
	            break;
	        case 'Program':
	            $arr_map = $this->arr_vod_index_img;
	            break;
	        case 'Channel':
	            $arr_map = $this->arr_channel_img;
	            break;
	        case 'ScheduleRecord':
	            $arr_map = $this->arr_playbill_img;
	            break;
	    }
	    if(!is_array($arr_map) || empty($arr_map))
	    {
	        return null;
	    }
	    foreach ($arr_map as $key=>$val)
	    {
	        if(isset($arr_video[$key]) && strlen($arr_video[$key]) >0)
	        {
	            $arr_video[$key] = ltrim(ltrim(trim($arr_video[$key]),'/'),'\\');
	            $id = md5($key.$arr_video[$key]);
	            $temp_file_ex = pathinfo($arr_video[$key]);
	            $source_file = $str_base_img_url.'/'.$arr_video[$key];
	            $remote_file = $arr_video['nns_cp_id'].'/'.$date.'/'.$temp_guid.'/'.$id.'.'.$temp_file_ex['extension'];
	            
	            $picture = array(
	                'FileURL'=>$remote_file,
	                'Type'=>$val,
	                'Description'=>'',
	            );
	            $base_path_img[]=array(
	                'source_file'=>$source_file,
	                'remote_file'=>$remote_file,
	            );
	            $str_picture_obj .= $this->make_object('Picture', $id,$action,$picture);
	            //                 $str_picture_mapping .= $this->make_mapping($parent_type, 'Picture', $parent_id, $id, $action);
	            $str_picture_mapping .= $this->make_mapping('Picture', $parent_type, $id, $parent_id, $action);
	        }
	    }
	    return array(
	        'picture'=>$str_picture_obj,
	        'mapping'=>$str_picture_mapping,
	        'base_path'=>$base_path_img,
	    );
	}
}
