<?php
/**
 * starcor 江苏思华（老版本）规范ADI CDN注入
 * @author liangpan
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_class/ftp/ftp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_class/ftp/ftp_push.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/c2_task/c2_task.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/c2_log/c2_log.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/v2/ns_core/m_mediainfo.class.php';
include_once dirname(dirname(__FILE__)).'/queue_task_model.php';
class c2_task_model_v2 extends nn_public_model
{
	private $str_cmsid = 'cctv';
	private $str_header = 'cctv';
	private $str_target_system_id = array(
	    "4000"=>'30-S',
	    "8000"=>'30-H',
    );
	private $str_target_site_id = "nanjing";
	private $str_sihua_sp_id = "SP1N02M00_04_063";
	
	
	
	/**
	 * 初始化第三方的CP 信息
	 * @param unknown $cp_id
	 */
	private function init_cp_info($cp_id)
	{
	    $result_project_key = nl_project_key::query_by_key($this->obj_dc, $cp_id);
	    if($result_project_key['ret'] !=0 || !isset($result_project_key['data_info']) || empty($result_project_key['data_info']) || !is_array($result_project_key['data_info']))
	    {
	        return $result_project_key;
	    }
	    $result_project_key = $result_project_key['data_info'];
	    $nns_key_value = isset($result_project_key['nns_key_value']) ? $result_project_key['nns_key_value'] : '';
	    if(!$this->is_json($nns_key_value))
	    {
	        return $result_project_key;
	    }
	    $nns_key_value = json_decode($nns_key_value,true);
	    if(isset($nns_key_value[$this->str_sp_id]) && strlen($nns_key_value[$this->str_sp_id]))
	    {
	        $this->str_cmsid  = $nns_key_value[$this->str_sp_id];
	        $this->str_header = $nns_key_value[$this->str_sp_id];
	    }
	    return $result_project_key;
	}
	
	private function my_sub_str($string,$str_len=28)
	{
	    return mb_substr($string, 0,$str_len,'UTF-8');
	}
	
	
	/**
	 * 组装片源信息
	 * @param unknown $arr_info 片源基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 */
	private function make_media_movie_xml($task_log_id,$arr_info,$arr_info_ex=null)
	{
	    $str_date = date("Y-m-d");
	    $str_Action = ($arr_info['nns_deleted'] !=1) ? '' : 'DELETE';
	    $str_img_ftp = (isset($this->arr_sp_config['img_ftp']) && strlen($this->arr_sp_config['img_ftp'])>0) ? trim($this->arr_sp_config['img_ftp'],'/').'/' : '';
	    $str_index_name = $arr_info_ex['video']['base']['nns_name'] = $this->my_sub_str($arr_info_ex['video']['base']['nns_name'],33);
		$str_xml = '';
        $str_xml.= '<Metadata>';
        $str_xml.=      '<AMS Verb="'.$str_Action.'" Asset_Class="packages" Asset_ID="'.$task_log_id.'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Provider="'.$this->str_cmsid.'" Provider_ID="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="asset package" Version_Major="1" Version_Minor="0" Product="MOD"/>';
        $str_xml.=      '<App_Data Value="CableLabsVOD1.1" Name="Metadata_Spec_Version" App="MOD"/>';
        $str_xml.= '</Metadata>';
        $str_xml.= '<Asset>';
        $str_xml.=      '<Metadata>';
        $str_xml.=          '<AMS Verb="'.$str_Action.'" Asset_Class="title" Asset_ID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Provider="'.$this->str_cmsid.'" Provider_ID="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_summary'].'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
        $str_xml.=          '<App_Data Value="program" Name="Show_Type" App="MOD"/>';
        $str_xml.=          '<App_Data Value="cctv" Name="Title" App="MOD"/>';
        $str_xml.=          '<App_Data Value="01:26:16" Name="Run_Time" App="MOD"/>';
        $str_xml.=          '<App_Data Value="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" Name="Original_Asset_ID" App="MOD"/>';
        $str_xml.=          '<App_Data Value="0" Name="Original_System_ID" App="MOD"/>';
        $str_xml.=          '<App_Data Value="1" Name="License_Type" App="MOD"/>';
        $str_xml.=          '<App_Data Value="Movie" Name="Genre" App="MOD"/>';
        $str_xml.=          '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Proper_Title" App="MOD"/>';
        $str_xml.=          '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Parallel_Proper_Title" App="MOD"/>';
        $str_xml.=          '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Subordinate_Title" App="MOD"/>';
        $str_xml.=          '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Alternative_ Title" App="MOD"/>';
        $str_xml.=          '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Title_Description" App="MOD"/>';
        $str_xml.=          '<App_Data Value="2" Name="Version_Description" App="MOD"/>';
        $str_xml.=          '<App_Data Value="'.$arr_info_ex['video']['base']['nns_keyword'].'" Name="Key_Words" App="MOD"/>';
        $str_xml.=          '<App_Data Value="'.$arr_info_ex['video']['base']['nns_summary'].'" Name="Description" App="MOD"/>';
        $str_xml.=          '<App_Data Value="" Name="Impressive_Dialogue" App="MOD"/>';
        $str_xml.=          '<App_Data Value="" Name="Impressive_Plot" App="MOD"/>';
        $str_xml.=          '<App_Data Value="" Name="Hot_Comments" App="MOD"/>';
        $str_xml.=          '<App_Data Value="" Name="Sentence_Review" App="MOD"/>';
        $str_xml.=          '<App_Data Value="" Name="Behind_Scenes" App="MOD"/>';
        $str_xml.=          '<App_Data Value="6.8" Name="IMDB_Rating" App="MOD"/>';
        $str_xml.=          '<App_Data Value="G" Name="MPAA_Rating" App="MOD"/>';
        $str_xml.=          '<App_Data Value="Movie" Name="Category" App="MOD"/>';
        $str_xml.=          '<App_Data Value="Y" Name="Season_Premiere" App="MOD"/>';
        $str_xml.=          '<App_Data Value="N" Name="Season_Finale" App="MOD"/>';
        $str_xml.=          '<App_Data Value="00:00:00" Name="Preview_Period" App="MOD"/>';
        $str_xml.=          '<App_Data Value="5" Name="Asset_Recommend_Class" App="MOD"/>';
        $str_xml.=          '<App_Data Value="0" Name="Chapter" App="MOD"/>';
        $str_xml.=          '<App_Data Value="2017" Name="Year" App="MOD"/>';
        $str_xml.=          '<App_Data Value="0.0" Name="Suggested_Price" App="MOD"/>';
        $str_xml.=          '<App_Data Value="Adult" Name="Audience" App="MOD"/>';
        $str_xml.=          '<App_Data Value="2056-01-01 20:00:00" Name="Subscriber_View_Limit" App="MOD"/>';
        $str_xml.=          '<App_Data Value="1" Name="Region" App="MOD"/>';
        $str_xml.=      '</Metadata>';
        $str_xml.=      '<Asset>';
        $str_xml.=          '<Metadata>';
        $str_xml.=              '<AMS Verb="'.$str_Action.'" Asset_Class="movie" Asset_ID="'.$arr_info['nns_cdn_media_guid'].'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Provider="'.$this->str_cmsid.'" Provider_ID="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_name'].'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
        $str_xml.=              '<App_Data Value="'.$arr_info['nns_cdn_media_guid'].'" Name="Original_Asset_ID" App="MOD"/>';
        $str_xml.=              '<App_Data Value="" Name="Original_System_ID" App="MOD"/>';
        $str_xml.=              '<App_Data Value="1" Name="Screen_Format" App="MOD"/>';
        $str_xml.=              '<App_Data Value="Y" Name="HDContent" App="MOD"/>';
        $str_xml.=              '<App_Data Value="" Name="3DContent" App="MOD"/>';
        $str_xml.=              '<App_Data Value="'.$arr_info['nns_kbps'].'" Name="Video_Bit_Rate" App="MOD"/>';
        $str_xml.=              '<App_Data Value="'.$arr_info['FrameHeight'].'" Name="Frame_Height" App="MOD"/>';
        $str_xml.=              '<App_Data Value="'.$arr_info['FrameWidth'].'" Name="Frame_Width" App="MOD"/>';
        $str_xml.=              '<App_Data Value="N" Name="Encryption" App="MOD"/>';
        $str_xml.=              '<App_Data Value="'.$arr_info['nns_kbps'].'" Name="Bit_Rate" App="MOD"/>';
        $str_xml.=              '<App_Data Value="1204000" Name="Audio_Bit_Rate" App="MOD"/>';
        $str_xml.=              '<App_Data Value="68cfcf7da9a4e14b171395110514c33c" Name="Content_Check_Sum" App="MOD"/>';
        $str_xml.=              '<App_Data Value="1" Name="Security_Classification" App="MOD"/>';
        $str_xml.=              '<App_Data Value="" Name="Remarks" App="MOD"/>';
        $str_xml.=              '<App_Data Value="'.$arr_info['nns_file_size'].'" Name="Content_File_Size" App="MOD"/>';
        $str_xml.=              '<App_Data Value="'.$arr_info['nns_file_size'].'" Name="Content_FileSize" App="MOD"/>';
        $str_xml.=              '<App_Data Value="1" Name="Encryption_Reason" App="MOD"/>';
        $str_xml.=              '<App_Data Value="zh" Name="Audio_Language" App="MOD"/>';
        $str_xml.=              '<App_Data Value="zh_CN" Name="Subtitle_Language" App="MOD"/>';
        $str_xml.=              '<App_Data Value="" Name="Audio_Bit_Depth" App="MOD"/>';
        $str_xml.=              '<App_Data Value="" Name="Audio_Sampling_Frequency" App="MOD"/>';
        $str_xml.=              '<App_Data Value="1" Name="Audio_Type" App="MOD"/>';
        $str_xml.=              '<App_Data Value="1" Name="Audio_Coding_Format" App="MOD"/>';
        $str_xml.=              '<App_Data Value="" Name="Video_Sampling_Type" App="MOD"/>';
        $str_xml.=              '<App_Data Value="1" Name="Video_Coding_Format" App="MOD"/>';
        $str_xml.=              '<App_Data Value="PAL" Name="System" App="MOD"/>';
        $str_xml.=              '<App_Data Value="" Name="Aspect_Ratio" App="MOD"/>';
        $str_xml.=              '<App_Data Value="5" Name="Video_Quality" App="MOD"/>';
        $str_xml.=              '<App_Data Value="1" Name="Audio_Channel_Format" App="MOD"/>';
        $str_xml.=              '<App_Data Value="1" Name="Color" App="MOD"/>';
        $str_xml.=              '<App_Data Value="" Name="Starting_Point" App="MOD"/>';
        $str_xml.=          '</Metadata>';
        $str_xml.=          '<Content Value="'.$arr_info['nns_url'].'"/>';
        $str_xml.=      '</Asset>';
        $str_xml.= '</Asset>';
		return $str_xml;
	}
	
	
	
	
	/**
	 * 组装片源信息
	 * @param unknown $arr_info 片源基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 */
	private function make_media_teleplay_xml($task_log_id,$arr_info,$arr_info_ex=null)
	{
	    $str_date = date("Y-m-d");
	    $str_Action = ($arr_info['nns_deleted'] !=1) ? '' : 'DELETE';
	    $str_img_ftp = (isset($this->arr_sp_config['img_ftp']) && strlen($this->arr_sp_config['img_ftp'])>0) ? trim($this->arr_sp_config['img_ftp'],'/').'/' : '';
	    $arr_info_ex['index']['base']['nns_index']++;
	    
	    $arr_info_ex['video']['base']['nns_name'] = $this->my_sub_str($arr_info_ex['video']['base']['nns_name']);
	    $str_index_name = $arr_info_ex['video']['base']['nns_name']."0{$arr_info_ex['index']['base']['nns_index']}";
	    
	    $arr_info_ex['video']['base']['nns_summary'] = $arr_info_ex['video']['base']['nns_name'];
	    
	    $str_xml = '';
        $str_xml.= '<Metadata>';
        $str_xml.=      '<AMS Verb="'.$str_Action.'" Asset_Class="packages" Asset_ID="'.$task_log_id.'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Provider="'.$this->str_cmsid.'" Provider_ID="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="asset package" Version_Major="1" Version_Minor="0" Product="MOD"/>';
        $str_xml.=      '<App_Data Value="CableLabsVOD1.1" Name="Metadata_Spec_Version" App="MOD"/>';
        $str_xml.= '</Metadata>';
        $str_xml.= '<Asset>';
        $str_xml.=      '<Metadata>';
        $str_xml.=          '<AMS Verb="'.$str_Action.'" Asset_Class="title" Asset_ID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" Asset_Name="'.$str_index_name.'" Provider="'.$this->str_cmsid.'" Provider_ID="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_summary'].'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
        $str_xml.=          '<App_Data Value="Series" Name="Show_Type" App="MOD"/>';
        $str_xml.=          '<App_Data Value="cctv" Name="Title" App="MOD"/>';
        $str_xml.=          '<App_Data Value="01:26:16" Name="Run_Time" App="MOD"/>';
        $str_xml.=          '<App_Data Value="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" Name="Original_Asset_ID" App="MOD"/>';
        $str_xml.=          '<App_Data Value="0" Name="Original_System_ID" App="MOD"/>';
        $str_xml.=          '<App_Data Value="1" Name="License_Type" App="MOD"/>';
        $str_xml.=          '<App_Data Value="Movie" Name="Genre" App="MOD"/>';
        $str_xml.=          '<App_Data Value="'.$str_index_name.'" Name="Proper_Title" App="MOD"/>';
        $str_xml.=          '<App_Data Value="'.$str_index_name.'" Name="Parallel_Proper_Title" App="MOD"/>';
        $str_xml.=          '<App_Data Value="'.$str_index_name.'" Name="Subordinate_Title" App="MOD"/>';
        $str_xml.=          '<App_Data Value="'.$str_index_name.'" Name="Alternative_ Title" App="MOD"/>';
        $str_xml.=          '<App_Data Value="'.$str_index_name.'" Name="Title_Description" App="MOD"/>';
        $str_xml.=          '<App_Data Value="2" Name="Version_Description" App="MOD"/>';
        $str_xml.=          '<App_Data Value="'.$arr_info_ex['video']['base']['nns_keyword'].'" Name="Key_Words" App="MOD"/>';
        $str_xml.=          '<App_Data Value="'.$arr_info_ex['video']['base']['nns_summary'].'" Name="Description" App="MOD"/>';
        $str_xml.=          '<App_Data Value="" Name="Impressive_Dialogue" App="MOD"/>';
        $str_xml.=          '<App_Data Value="" Name="Impressive_Plot" App="MOD"/>';
        $str_xml.=          '<App_Data Value="" Name="Hot_Comments" App="MOD"/>';
        $str_xml.=          '<App_Data Value="" Name="Sentence_Review" App="MOD"/>';
        $str_xml.=          '<App_Data Value="" Name="Behind_Scenes" App="MOD"/>';
        $str_xml.=          '<App_Data Value="6.8" Name="IMDB_Rating" App="MOD"/>';
        $str_xml.=          '<App_Data Value="G" Name="MPAA_Rating" App="MOD"/>';
        $str_xml.=          '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Category" App="MOD"/>';
        $str_xml.=          '<App_Data Value="Y" Name="Season_Premiere" App="MOD"/>';
        $str_xml.=          '<App_Data Value="N" Name="Season_Finale" App="MOD"/>';
        $str_xml.=          '<App_Data Value="00:00:00" Name="Preview_Period" App="MOD"/>';
        $str_xml.=          '<App_Data Value="'.$arr_info_ex['index']['base']['nns_index'].'" Name="Asset_Recommend_Class" App="MOD"/>';
        $str_xml.=          '<App_Data Value="0" Name="Chapter" App="MOD"/>';
        $str_xml.=          '<App_Data Value="2017" Name="Year" App="MOD"/>';
        $str_xml.=          '<App_Data Value="0.0" Name="Suggested_Price" App="MOD"/>';
        $str_xml.=          '<App_Data Value="Adult" Name="Audience" App="MOD"/>';
        $str_xml.=          '<App_Data Value="2056-01-01 20:00:00" Name="Subscriber_View_Limit" App="MOD"/>';
        $str_xml.=          '<App_Data Value="1" Name="Region" App="MOD"/>';
        $str_xml.=      '</Metadata>';
        $str_xml.=      '<Asset>';
        $str_xml.=          '<Metadata>';
        $str_xml.=              '<AMS Verb="'.$str_Action.'" Asset_Class="movie" Asset_ID="'.$arr_info['nns_cdn_media_guid'].'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Provider="'.$this->str_cmsid.'" Provider_ID="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_name'].'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
        $str_xml.=              '<App_Data Value="'.$arr_info['nns_cdn_media_guid'].'" Name="Original_Asset_ID" App="MOD"/>';
        $str_xml.=              '<App_Data Value="" Name="Original_System_ID" App="MOD"/>';
        $str_xml.=              '<App_Data Value="1" Name="Screen_Format" App="MOD"/>';
        $str_xml.=              '<App_Data Value="Y" Name="HDContent" App="MOD"/>';
        $str_xml.=              '<App_Data Value="" Name="3DContent" App="MOD"/>';
        $str_xml.=              '<App_Data Value="'.$arr_info['nns_kbps'].'" Name="Video_Bit_Rate" App="MOD"/>';
        $str_xml.=              '<App_Data Value="'.$arr_info['FrameHeight'].'" Name="Frame_Height" App="MOD"/>';
        $str_xml.=              '<App_Data Value="'.$arr_info['FrameWidth'].'" Name="Frame_Width" App="MOD"/>';
        $str_xml.=              '<App_Data Value="N" Name="Encryption" App="MOD"/>';
        $str_xml.=              '<App_Data Value="'.$arr_info['nns_kbps'].'" Name="Bit_Rate" App="MOD"/>';
        $str_xml.=              '<App_Data Value="1204000" Name="Audio_Bit_Rate" App="MOD"/>';
        $str_xml.=              '<App_Data Value="68cfcf7da9a4e14b171395110514c33c" Name="Content_Check_Sum" App="MOD"/>';
        $str_xml.=              '<App_Data Value="1" Name="Security_Classification" App="MOD"/>';
        $str_xml.=              '<App_Data Value="" Name="Remarks" App="MOD"/>';
        $str_xml.=              '<App_Data Value="'.$arr_info['nns_file_size'].'" Name="Content_File_Size" App="MOD"/>';
        $str_xml.=              '<App_Data Value="'.$arr_info['nns_file_size'].'" Name="Content_FileSize" App="MOD"/>';
        $str_xml.=              '<App_Data Value="1" Name="Encryption_Reason" App="MOD"/>';
        $str_xml.=              '<App_Data Value="zh" Name="Audio_Language" App="MOD"/>';
        $str_xml.=              '<App_Data Value="zh_CN" Name="Subtitle_Language" App="MOD"/>';
        $str_xml.=              '<App_Data Value="" Name="Audio_Bit_Depth" App="MOD"/>';
        $str_xml.=              '<App_Data Value="" Name="Audio_Sampling_Frequency" App="MOD"/>';
        $str_xml.=              '<App_Data Value="1" Name="Audio_Type" App="MOD"/>';
        $str_xml.=              '<App_Data Value="1" Name="Audio_Coding_Format" App="MOD"/>';
        $str_xml.=              '<App_Data Value="" Name="Video_Sampling_Type" App="MOD"/>';
        $str_xml.=              '<App_Data Value="1" Name="Video_Coding_Format" App="MOD"/>';
        $str_xml.=              '<App_Data Value="PAL" Name="System" App="MOD"/>';
        $str_xml.=              '<App_Data Value="" Name="Aspect_Ratio" App="MOD"/>';
        $str_xml.=              '<App_Data Value="5" Name="Video_Quality" App="MOD"/>';
        $str_xml.=              '<App_Data Value="1" Name="Audio_Channel_Format" App="MOD"/>';
        $str_xml.=              '<App_Data Value="1" Name="Color" App="MOD"/>';
        $str_xml.=              '<App_Data Value="" Name="Starting_Point" App="MOD"/>';
        $str_xml.=          '</Metadata>';
        $str_xml.=          '<Content Value="'.$arr_info['nns_url'].'"/>';
        $str_xml.=      '</Asset>';
        $str_xml.= '</Asset>';
		return $str_xml;
	}
	
	
	
	/**
	 * 组装片源信息
	 * @param unknown $arr_info 片源基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 */
	private function make_media_variety_xml($task_log_id,$arr_info,$arr_info_ex=null)
	{
	    $str_date = date("Y-m-d");
	    $str_Action = ($arr_info['nns_deleted'] !=1) ? '' : 'DELETE';
	    $str_img_ftp = (isset($this->arr_sp_config['img_ftp']) && strlen($this->arr_sp_config['img_ftp'])>0) ? trim($this->arr_sp_config['img_ftp'],'/').'/' : '';
	    $str_language_id = $this->make_other_guid($arr_info_ex['video']['base']['nns_integer_id'],'video',0,'LA');
	    $arr_info_ex['index']['base']['nns_index']++;
	    $str_index_name = $arr_info_ex['video']['base']['nns_name'].'第'.$arr_info_ex['index']['base']['nns_index'].'集';
	    
	    $str_xml = '';
	    $str_xml.= '<Metadata>';
	    $str_xml.=      '<AMS Verb="'.$str_Action.'" Asset_Class="packages" Asset_ID="'.$task_log_id.'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Provider_ID="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="asset package" Version_Major="1" Version_Minor="0" Product="MOD"/>';
	    $str_xml.=      '<App_Data Value="CableLabsVOD1.1" Name="Metadata_Spec_Version" App="MOD"/>';
	    $str_xml.= '</Metadata>';
	    
	    $str_xml.= '<Asset>';
	    $str_xml.=     '<Metadata>';
	    $str_xml.=         '<AMS Verb="'.$str_Action.'" Asset_Class="title" Asset_ID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Provider_ID="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_summary'].'" Version_Major="1" Version_Minor="0" Product="MOD"/>';
	    $str_xml.=         '<App_Data Value="series" Name="Show_Type" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Series" Name="Genre" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" Name="Original_Asset_ID" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" Name="Original_System_ID" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_all_index'].'" Name="Volumn_Count" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="0" Name="Chapter" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="1" Name="License_Type" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Proper_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_summary'].'" Name="Description" App="MOD"/>';
	    $str_xml.=         '<App_Data App="MOD" Name="Category" Value="Series"/>';
	    $str_xml.=         '<App_Data Value="Y" Name="Season_Premiere" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Y" Name="Season_Finale" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="00:00:00" Name="Preview_Period" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="2017" Name="Year" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="5.0" Name="Suggested_Price" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Adult" Name="Audience" App="MOD"/>';
	    $str_xml.=     '</Metadata>';
	    $str_xml.=     '<Asset>';
	    $str_xml.=         '<Metadata>';
	    $str_xml.=             '<AMS Verb="'.$str_Action.'" Asset_Class="movie" Asset_ID="'.$arr_info['nns_cdn_media_guid'].'" Asset_Name="'.$str_index_name.'" Provider_ID="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="" Version_Major="1" Version_Minor="0" Product="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$arr_info['nns_cdn_media_guid'].'" Name="Original_Asset_ID" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$arr_info['nns_cdn_media_guid'].'" Name="Original_System_ID" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="Y" Name="HDContent" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$arr_info['nns_kbps'].'" Name="Video_Bit_Rate" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="2000" Name="Audio_Bit_Rate" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$arr_info['FrameHeight'].'" Name="Frame_Height" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$arr_info['FrameWidth'].'" Name="Frame_Width" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="00:07:50" Name="Run_Time" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="7ea975704f7c100d547d41f6ac37f9b3" Name="Content_Check_Sum" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="1" Name="Security_Classification" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="1" Name="Encryption_Reason" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="加密备注" Name="Remarks" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="N" Name="Encryption" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$arr_info['nns_file_size'].'" Name="Content_File_Size" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="zh" Name="Audio_Language" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="zh" Name="Subtitle_Language" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Audio_Bit_Depth" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Audio_Sampling_Frequency" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Audio_Coding_Format" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Video_Sampling_Type" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Video_Coding_Format" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="PAL" Name="System" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="16:9" Name="Aspect_Ratio" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="5" Name="Video_Quality" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="5" Name="Audio_Quality" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="1" Name="Audio_Channel_Format" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="1" Name="Color" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Starting_Point" App="MOD"/>';
	    $str_xml.=         '</Metadata>';
	    $str_xml.=         '<Content Value="'.$arr_info['nns_url'].'"/>';
	    $str_xml.=     '</Asset>';
	    $str_xml.= '</Asset>';
	    return $str_xml;
	}
	
	
	/**
	 * 生成图片的GUID
	 */
	private function make_other_guid($str_integer_id,$video_type='',$index=0,$str_type='PO')
	{
	    $str_header = (strlen($this->str_header) < 8) ? str_pad($this->str_header, 8, "0", STR_PAD_RIGHT) : substr($this->str_header, 0,8);
	    $str_type = strlen($str_type) == 2 ? $str_type : 'PO';
	    $index = strlen($index) == 1 ? $index : 0;
	    switch ($video_type)
	    {
	        case 'video':
	            $video_type = 'PT';
	            break;
	        case 'index':
	            $video_type = 'TI';
	            break;
	        case 'media':
	            $video_type = 'MO';
	            break;
	        case 'live':
	            $video_type = 'CH';
	            break;
	        case 'live_index':
	            $video_type = 'LI';
	            break;
	        case 'live_media':
	            $video_type = 'PC';
	            break;
	        case 'playbill':
	            $video_type = 'SD';
	            break;
	        case 'file':
	            $video_type = 'FI';
	            break;
	        default:
	            $video_type = 'PU';
	    }
	    return $str_header.$str_type .$video_type. str_pad($str_integer_id, 7, "0", STR_PAD_LEFT).$index;
	}
	
	/**
	 * 片源CDN
	 * @param unknown $movie_id
	 */
	public function do_media()
	{
		if(!isset($this->arr_params['nns_media_id']) || empty($this->arr_params['nns_media_id']))
		{
			return $this->_make_return_data(1,'点播传入参数无nns_media_id字段,参数为:'.var_export($this->arr_params));
		}
		$movie_id = $this->arr_params['nns_media_id'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
		$result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $movie_id);
		if($result_media['ret'] !=0)
		{
			return $result_media;
		}
		if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
		{
			nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
			return $result_media;
		}
		$result_media = $result_media['data_info'];
		$temp_media_source_url='';
		if($this->is_json($result_media['nns_ext_info']))
		{
		    $nns_ext_info = json_decode($result_media['nns_ext_info'],true);
		    if(isset($nns_ext_info['path']) && strlen($nns_ext_info['path']) >0)
		    {
		        $temp_media_source_url = trim(trim($nns_ext_info['path'],'/'),"\\");
		    }
		    else
		    {
		        $temp_media_source_url = trim(trim($result_media['nns_url'],'/'),"\\");
		    }
		}
		else
		{
		    $temp_media_source_url = trim(trim($result_media['nns_url'],'/'),"\\");
		}
		$media_info_guid = dirname(dirname(dirname(dirname(__FILE__)))).'/data/temp/mediainfo/temp';
		if (!is_dir($media_info_guid))
		{
		    mkdir($media_info_guid, 0777, true);
		}
		$media_info_guid.=np_guid_rand().'.xml';
		$result = m_mediainfo::exec_ftp("ftp://suzhou4k:suzhou4k123@172.26.2.18:21/local_cache/".$temp_media_source_url,$media_info_guid);
		$arr_media_info = null;
		if($result['ret'] == '0' && strlen($result['data_info']) >0)
		{
		    $arr_media_info = simplexml_load_string($result['data_info']);
		    $arr_media_info = json_decode(json_encode($arr_media_info),true);
		}
// 		ob_clean();
// 		echo json_encode($arr_media_info);die;
		$int_bit_rate = isset($arr_media_info['File']['track'][0]['Overall_bit_rate'][0]) ? (int)$arr_media_info['File']['track'][0]['Overall_bit_rate'][0] : 27*1024*1024;
		
		$media_formart = isset($arr_media_info['File']['track'][0]['Overall_bit_rate_mode'][0]) ? $arr_media_info['File']['track'][0]['Overall_bit_rate_mode'][0] : '';
		
		$media_formart = is_string($media_formart) ? strtolower($media_formart) : '';
		if($media_formart !='cbr')
		{
		    return $this->_make_return_data(1,'片源非CBR不允许注入',$media_formart);
		}
		$result_media['nns_file_size'] = isset($arr_media_info['File']['track'][0]['File_size'][0]) ? (int)$arr_media_info['File']['track'][0]['File_size'][0] : 384061252;
		$result_media['FrameHeight'] = (isset($arr_media_info['File']['track'][1]['Sampled_Height']) && (int)$arr_media_info['File']['track'][1]['Sampled_Height']>0) ? (int)$arr_media_info['File']['track'][1]['Sampled_Height'] : 1080;
		$result_media['FrameWidth'] = (isset($arr_media_info['File']['track'][1]['Sampled_Width']) && (int)$arr_media_info['File']['track'][1]['Sampled_Width']>0) ? (int)$arr_media_info['File']['track'][1]['Sampled_Width'] : 1920;
		
		$result_media['nns_kbps'] = $int_bit_rate;
// 		$temp_media_source_url = 'test/CCTV3520180522000355.ts';
		$arr_nns_file_resolution = explode('*', $result_media['nns_file_resolution']);
// 		$result_media['FrameHeight'] = (isset($arr_nns_file_resolution[0]) && (int)$arr_nns_file_resolution[0] >0) ? $arr_nns_file_resolution[0] : 1080;
// 		$result_media['FrameWidth'] = (isset($arr_nns_file_resolution[1]) && (int)$arr_nns_file_resolution[1] >0) ? $arr_nns_file_resolution[1] : 1920;
		$result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $result_media['nns_vod_index_id']);
		if($result_index['ret'] !=0)
		{
		    return $result_index;
		}
		if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
		{
		    nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
		    return $result_index;
		}
		$result_index = $result_index['data_info'][0];
		
		
		
		$result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_media['nns_vod_id']);
		if($result_video['ret'] !=0)
		{
		    return $result_video;
		}
		if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
		{
		    nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
		    return $result_video;
		}
		$result_video = $result_video['data_info'][0];
		$arr_pathinfo = pathinfo($temp_media_source_url);
		if(!isset($arr_pathinfo['extension']) || strlen($arr_pathinfo['extension'])<1)
		{
		    $this->_make_return_data(1,'error');
		}
		$temp_guid = $this->get_guid_rand("MO", $result_media['nns_integer_id']);
		$result_media['nns_url'] = $temp_guid.'.'.$arr_pathinfo['extension'];
		
		$result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $result_index['nns_id'],$result_index,null,'cdn',$this->str_header);
		if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
		{
		    return $result_media_import_info;
		}
		$result_media['nns_cdn_media_guid'] = $result_media_import_info['data_info'];
// 		var_dump($result_media);die;
// 		$result_media['nns_cdn_media_guid'] = $this->get_content_guid("MO", $result_media['nns_integer_id']);
		
		$result_video['nns_cdn_video_guid'] = $this->get_content_guid("VI", $result_media['nns_integer_id']);
		$result_index['nns_cdn_index_guid'] = $this->get_content_guid("IN", $result_media['nns_integer_id']);
		$temp_desc_url = $temp_guid.'/'.$temp_guid.'.'.$arr_pathinfo['extension'];
		$arr_info_ex=array(
		    'video'=>array(
		        'base'=>$result_video,
		    ),
		    'index'=>array(
		        'base'=>$result_index,
		    ),
		);
		$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml_str .= '<ADI>';
		if($arr_info_ex['video']['base']['nns_all_index'] <=1)
		{
		    $xml_str .=   $this->make_media_movie_xml($temp_guid,$result_media,$arr_info_ex);
		}
		else
		{
		    $xml_str .=   $this->make_media_teleplay_xml($temp_guid,$result_media,$arr_info_ex);
		}
		$xml_str .= '</ADI>';
		
		$action = ($result_media['nns_deleted'] !=1) ? 'REGIST' : 'DELETE';
		
		$file_name = $this->str_sp_id . '_' . date('His') . '_' . $temp_guid . '_' . 'vod_media' . '_' . $action . '.xml';
		$c2_info = array(
		    'nns_task_type'=>'Movie',
		    'nns_task_id'=> isset($this->arr_params['nns_task_id'])?$this->arr_params['nns_task_id']:null,
		    'nns_task_name'=> isset($this->arr_params['nns_task_name'])?$this->arr_params['nns_task_name']:null,
		    'nns_action'=>	$action,
		    'nns_url' => $file_name,
		    'nns_content' => $xml_str,
		    'nns_desc' => 'Movie,'.$action,
		    'nns_id'=>$temp_guid,
		);
		$result = $this->execute_self_bk_c2($c2_info);
		if($result['ret'] !=0)
		{
		    return $result;
		}
		if($action == 'DELETE')
		{
		    return $result;
		}
		$data_ftp = null;
	    $data_ftp[] = array(
	        'source_file'=>$result['data_info'],
	        'remote_file'=>$temp_guid.'/adi.xml',
	    );
	    $local_file_path = isset($this->arr_sp_config['c2_import_push_media_base_url']) ? trim(trim($this->arr_sp_config['c2_import_push_media_base_url'],'/'),'\\') : '';
	    $local_file_path = (strlen($local_file_path) >0) ? '/'.$local_file_path.'/'.$temp_media_source_url : '/'.$temp_media_source_url;
		
		
		$ftp_url = isset($this->arr_sp_config['c2_import_push_dst_url']) ? $this->arr_sp_config['c2_import_push_dst_url'] : '';
	    $result_ftp = $this->push_xml_file($ftp_url,$data_ftp);
	    if($result_ftp['ret'] !=0)
	    {
	        $edit_c2_info = array(
	            'nns_result'=>"[-1]",
	            'nns_notify_result'=>-1,
	            'nns_notify_fail_reason'=>$result_ftp['reason'],
	        );
	        nl_c2_log::edit($this->obj_dc, $edit_c2_info,$c2_info['nns_id']);
	        return $result_ftp;
	    }
	    $file_params = array(
	        'des_file_path'=>$result_ftp['data_info'].'/'.$temp_desc_url,
	        'local_file_path'=>$local_file_path,
	        'queue_id'=>$c2_info['nns_id'],
	        'request_url'=>$this->arr_sp_config['c2_import_push_go_url'],
	        'org_id'=>$this->str_sp_id,
	    );
		$obj_ftp_push = new ftp_push();
		$result_ftp_push  =  $obj_ftp_push->go_push_ts_media($file_params);
		if($result_ftp_push['ret'] !=0)
		{
		    $edit_c2_info = array(
		        'nns_result'=>"[-1]",
		        'nns_notify_result'=>-1,
		        'nns_notify_fail_reason'=>$result_ftp_push['reason'],
		    );
		    nl_c2_log::edit($this->obj_dc, $edit_c2_info,$c2_info['nns_id']);
		}
		return $result_ftp_push;
	}
	
	/**
	 *推送文件信息给下游
	 */
	private function push_xml_file($ftp_url,$arr_file)
	{
	    $ftp_url = trim($ftp_url,'/');
	    if(strlen($ftp_url) <1)
	    {
	        return $this->_make_return_data(1,'sp c2_import_push_dst_url not set');
	    }
	    $arr_ftp_config = parse_url($ftp_url);
	    $host_ftp = (isset($arr_ftp_config["host"]) && strlen($arr_ftp_config["host"]) > 0) ? $arr_ftp_config["host"] : '';
	    $user_ftp = (isset($arr_ftp_config["user"]) && strlen($arr_ftp_config["user"]) > 0) ? $arr_ftp_config["user"] : '';
	    $pass_ftp = (isset($arr_ftp_config["pass"]) && strlen($arr_ftp_config["pass"]) > 0) ? $arr_ftp_config["pass"] : '';
	    $path_ftp = (isset($arr_ftp_config["path"]) && strlen($arr_ftp_config["path"]) > 0) ? $arr_ftp_config["path"] : '';
	    $port_ftp = (isset($arr_ftp_config["port"]) && strlen($arr_ftp_config["port"]) > 0) ? $arr_ftp_config["port"] : 21;
	     
	    $path_ftp = trim(trim($path_ftp,'/'));
	    $path_ftp = (strlen($path_ftp) >0) ? '/'.$path_ftp : '/';
	    foreach ($arr_file as $file_val)
	    {
	        $obj_ftp = new nl_ftp($host_ftp, $user_ftp, $pass_ftp,$port_ftp,30);
	        $result_upload = $obj_ftp->upload_file($path_ftp.'/'.$file_val['remote_file'],$file_val['source_file']);
	        $obj_ftp->my_destruct();
	        unset($obj_ftp);
	        if($result_upload['ret'] !=0)
	        {
	            for ($i=0;$i<2;$i++)
	            {
	                $obj_ftp = new nl_ftp($host_ftp, $user_ftp, $pass_ftp,$port_ftp,30);
	                $result_upload = $obj_ftp->upload_file($path_ftp.'/'.$file_val['remote_file'],$file_val['source_file']);
	                $obj_ftp->my_destruct();
	                unset($obj_ftp);
	                if($result_upload['ret'] == '0')
	                {
	                    break;
	                }
	            }
	            if($result_upload['ret'] !='0')
	            {
	                return $result_upload;
	            }
	        }
	    }
	    return $this->_make_return_data(0,'ftp upload ok',$path_ftp);
	}

	
	/**
	 * 获取  GUID
	 */
	private function get_guid_rand($str_type,$str_integer_id,$time=0)
	{
	    $str_header = (strlen($this->str_header) < 4) ? str_pad($this->str_header, 4, "0", STR_PAD_RIGHT) : substr($this->str_header, 0,4);
	    $str_integer_id = base_convert($str_integer_id,10,36);
	    $str_guid = $str_header.'P'.$str_type.$this->get_rand_str(5).str_pad($str_integer_id, 8, "0", STR_PAD_LEFT);
	    $result_log = nl_c2_log::query_data_by_id($this->obj_dc, $str_guid);
	    if ($time >= 3 && isset($result_log['data_info']) && is_array($result_log['data_info']) && !empty($result_log['data_info']))
	    {
	        return np_guid_rand();
	    }
	    if(isset($result_log['data_info']) && is_array($result_log['data_info']) && !empty($result_log['data_info']))
	    {
	        $time++;
	        $str_guid = $this->get_guid_rand($str_type,$str_integer_id,$time);
	    }
	    return $str_guid;
	}
	
	/**
	 * 获取  GUID
	 */
	private function get_content_guid($str_type,$str_integer_id)
	{
	    $str_header = (strlen($this->str_header) < 4) ? str_pad($this->str_header, 4, "0", STR_PAD_RIGHT) : substr($this->str_header, 0,4);
	    $str_integer_id = base_convert($str_integer_id,10,36);
	    $str_guid = $str_header.'C'.$str_type.str_pad($str_integer_id, 13, "0", STR_PAD_LEFT);
	    return $str_guid;
	}
	
	/**
	 * 中国电信标准C2
	 * @param unknown $c2_info
	 */
	public function execute_self_bk_c2($c2_info)
	{
	    $c2_info['nns_type'] = 'std';
	    $c2_info['nns_org_id'] = $this->str_sp_id;
	     
	    $sub_path = date('Y-m-d');
	    $file = $c2_info['nns_url'];
	    $c2_info['nns_url'] = $sub_path . '/' . $file;
	    //生成soap xml文件 路径
	    $file_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/mgtv/' . $this->str_sp_id . '/inject_xml/' . $sub_path . '/';
	    if (!is_dir($file_path))
	    {
	        mkdir($file_path, 0777, true);
	    }
	    $file_path .= $file;
	    $result = @file_put_contents($file_path, $c2_info['nns_content']);
	    $CmdFileURL = trim(rtrim($this->arr_sp_config['xml_ftp'],'/')) . '/' . trim(ltrim($c2_info['nns_url'],'/'));
	     
	    $result = $result ? 0 : -1;
	    $c2_info['nns_result'] = "[{$result}]";
	    $c2_info['nns_notify_result'] = $result;
	    $c2_info['nns_content'] = '';
	    nl_c2_log::add($this->obj_dc, $c2_info);
	    return ($result== 0) ? $this->_make_return_data(0,'ok',$file_path) :  $this->_make_return_data(1,'error',$file_path);
	}
	
	
	
	
	
	/**
	 * 发布操作
	 */
	public function do_media_delivery_add()
	{
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_id']) || empty($this->arr_params['bk_c2_task_info']['nns_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $result_c2_log = nl_c2_log::query_by_task_id($this->obj_dc, $this->arr_params['bk_c2_task_info']['nns_id'],array('REGIST',"UPDATE"));
	    if($result_c2_log['ret'] !=0)
	    {
	        return $result_c2_log;
	    }
	    if(!isset($result_c2_log['data_info'][0]) || empty($result_c2_log['data_info'][0]) || !is_array($result_c2_log['data_info'][0]))
	    {
	        return $this->_make_return_data(0,'查询无tasklog数据');
	    }
	    $asset_id = $result_c2_log['data_info'][0]['nns_id'];
	    $ftp_cdn_send_mode_url = $this->arr_sp_config['c2_import_asset_url'];
	    if(strlen($ftp_cdn_send_mode_url) <1)
	    {
	        return $this->_make_return_data(0,'ftp_cdn_send_mode_url配置为空');
	    }
	    $ftp_cdn_send_mode_url.="/ingest?cpcode=".$this->str_cmsid."&packageassetid={$asset_id}";
	    $obj_curl = new np_http_curl_class();
	    $arr_header = array(
	        "Content-type: application/xml;charset=\"utf-8\"",
	        "Accept: text/xml",
	        "Cache-Control: no-cache",
	        "Pragma: no-cache",
	        "SOAPAction: \"run\"",
	    );
	    $str_guid = np_guid_rand();
	    $request_ret = $obj_curl->do_request('post',$ftp_cdn_send_mode_url,null,$arr_header,60);
	    $curl_info = $obj_curl->curl_getinfo();
	    $sub_path = date('Y-m-d');
	    $file = $this->str_sp_id . '_DELIVERY_ADD_'. $str_guid . '.xml';
	    $str_url= $sub_path . '/'.strtolower('Movie').'/' . $file;
	    $c2_info = array(
	        'nns_id'=>$str_guid,
	        'nns_task_type'=>'Movie',
	        'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	        'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
	        'nns_action'=>	'DELIVERY_ADD',
	        'nns_content'=>	'',
	        'nns_type'=>'std',
	        'nns_org_id'=>$this->str_sp_id,
	        'nns_url'=>$str_url,
	    );
	    if(is_string($request_ret) && strlen($request_ret)>0)
	    {
    	    //生成soap xml文件 路径
    	    $file_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/mgtv/' . $this->str_sp_id . '/inject_xml/' . $sub_path . '/' .strtolower($c2_info['nns_task_type']).'/';
    	    if (!is_dir($file_path))
    	    {
    	        mkdir($file_path, 0777, true);
    	    }
    	    $file_path .= $file;
    	    file_put_contents($file_path, $request_ret);
    	    $request_result = simplexml_load_string($request_ret);
    	    $request_result = json_decode(json_encode($request_result),true);
	    }
	    if($curl_info['http_code'] != '200')
	    {
	        $c2_info['nns_result'] = '[' . 1 . ']';
	        $c2_info['nns_notify_result'] = '1';
	        $c2_info['nns_notify_fail_reason'] = "curl非200,URL{$ftp_cdn_send_mode_url}";
	        nl_c2_task::edit($this->obj_dc, array('nns_status'=>'9'), $this->arr_params['bk_c2_task_info']['nns_id']);
	    }
	    else
	    {
	        $c2_info['nns_result'] = '[' . 0 . ']';
	        $errormsg= isset($request_result['errormsg']) ? $request_result['errormsg'] : '未知';
	        if(isset($request_result['errorcode']) && $request_result['errorcode'] == '0')
	        {
	            $c2_info['nns_notify_result'] = '0';
	        }
            else
            {
                $c2_info['nns_notify_result'] = '1';
            }
	        
	        $c2_info['nns_notify_fail_reason'] = "错误信息:{$errormsg};请求URL地址:{$ftp_cdn_send_mode_url}";
	        nl_c2_task::edit($this->obj_dc, array('nns_status'=>'8'), $this->arr_params['bk_c2_task_info']['nns_id']);
	    }
	    return nl_c2_log::add($this->obj_dc, $c2_info);
	}
	
	
	
	/**
	 * 取消发布操作
	 */
	public function do_media_delivery_del()
	{
	    global $g_bk_web_url;
	    $bk_web_url = strlen($g_bk_web_url) > 0 ? $g_bk_web_url : '';
	    unset($g_bk_web_url);
	    if(strlen($bk_web_url) <1)
	    {
	        return $this->_make_return_data(1,"全局g_bk_web_url配置为空");
	    }
	    $bk_web_url= $bk_web_url.'/mgtv/'.$this->str_sp_id.'/notify_adi.php';
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_id']) || empty($this->arr_params['bk_c2_task_info']['nns_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $result_c2_log = nl_c2_log::query_by_task_id($this->obj_dc, $this->arr_params['bk_c2_task_info']['nns_id'],array('REGIST',"UPDATE"));
	    if($result_c2_log['ret'] !=0)
	    {
	        return $result_c2_log;
	    }
	    if(!isset($result_c2_log['data_info'][0]) || empty($result_c2_log['data_info'][0]) || !is_array($result_c2_log['data_info'][0]))
	    {
	        return $this->_make_return_data(0,'查询无tasklog数据');
	    }
	    $asset_id = $result_c2_log['data_info'][0]['nns_id'];
	    $ftp_cdn_send_mode_url = $this->arr_sp_config['c2_import_asset_url'];
	    if(strlen($ftp_cdn_send_mode_url) <1)
	    {
	        return $this->_make_return_data(0,'ftp_cdn_send_mode_url配置为空');
	    }
	    $ftp_cdn_send_mode_url.="/deleteAsset?appid=269&packageassetid={$asset_id}";
	    $bk_web_url = $bk_web_url."?asseturl={$asset_id}/adi.xml";
	    $obj_curl = new np_http_curl_class();
	    $arr_header = array(
	        "Content-type: application/xml;charset=\"utf-8\"",
	        "Accept: text/xml",
	        "Cache-Control: no-cache",
	        "Pragma: no-cache",
	        "SOAPAction: \"run\"",
	    );
	    $str_guid = np_guid_rand();
	    $request_ret = $obj_curl->do_request('post',$ftp_cdn_send_mode_url,null,$arr_header,60);
	    $curl_info = $obj_curl->curl_getinfo();
	    $sub_path = date('Y-m-d');
	    $file = $this->str_sp_id . '_DELIVERY_DEL_'. $str_guid . '.xml';
	    $str_url= $sub_path . '/'.strtolower('Movie').'/' . $file;
	    $c2_info = array(
	        'nns_id'=>$str_guid,
	        'nns_task_type'=>'Movie',
	        'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	        'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
	        'nns_action'=>	'DELIVERY_DEL',
	        'nns_content'=>	'',
	        'nns_type'=>'std',
	        'nns_org_id'=>$this->str_sp_id,
	        'nns_url'=>$str_url,
	    );
	    if(is_string($request_ret) && strlen($request_ret)>0)
	    {
    	    //生成soap xml文件 路径
    	    $file_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/mgtv/' . $this->str_sp_id . '/inject_xml/' . $sub_path . '/' .strtolower($c2_info['nns_task_type']).'/';
    	    if (!is_dir($file_path))
    	    {
    	        mkdir($file_path, 0777, true);
    	    }
    	    $file_path .= $file;
    	    file_put_contents($file_path, $request_ret);
    	    $request_result = simplexml_load_string($request_ret);
    	    $request_result = json_decode(json_encode($request_result),true);
	    }
	    
	    if($curl_info['http_code'] != '200')
	    {
	        $c2_info['nns_result'] = '[' . 1 . ']';
	        $c2_info['nns_notify_result'] = '1';
	        $c2_info['nns_notify_fail_reason'] = "curl非200,URL{$ftp_cdn_send_mode_url}";
	        nl_c2_task::edit($this->obj_dc, array('nns_status'=>'9'), $this->arr_params['bk_c2_task_info']['nns_id']);
	    }
	    else
	    {
	        $c2_info['nns_result'] = '[' . 0 . ']';
	        $errormsg= isset($request_result['errormsg']) ? $request_result['errormsg'] : '未知';
	        if(isset($request_result['errorcode']) && $request_result['errorcode'] == '0')
	        {
	            $c2_info['nns_notify_result'] = '0';
	        }
            else
            {
                $c2_info['nns_notify_result'] = '1';
            }
	        
	        $c2_info['nns_notify_fail_reason'] = "错误信息:{$errormsg};请求URL地址:{$ftp_cdn_send_mode_url}";
	        nl_c2_task::edit($this->obj_dc, array('nns_status'=>'8'), $this->arr_params['bk_c2_task_info']['nns_id']);
	    }	    
	    $result = nl_c2_log::add($this->obj_dc, $c2_info);
	    usleep(5000);
	    $obj_curl = new np_http_curl_class();
	    $arr_header = array(
	        "Content-type: application/xml;charset=\"utf-8\"",
	        "Accept: text/xml",
	        "Cache-Control: no-cache",
	        "Pragma: no-cache",
	        "SOAPAction: \"run\"",
	    );
	    $request_ret = $obj_curl->do_request('post',$bk_web_url,null,$arr_header,60);
	    return $result;
	}
	
	
	
	/**
	 * 上线操作
	 */
	public function do_media_online()
	{
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_ref_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $movie_id = $this->arr_params['bk_c2_task_info']['nns_ref_id'];
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	    $result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $movie_id);
	    if($result_media['ret'] !=0)
	    {
	        return $result_media;
	    }
	    if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_media;
	    }
	    $result_media = $result_media['data_info'];
		$this->init_cp_info($result_media['nns_cp_id']);
	    $result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $movie_id,$result_media,null,'cdn',$this->str_header);
	    if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
	    {
	        return $result_media_import_info;
	    }
	    
	    $result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $result_media['nns_vod_index_id']);
	    if($result_index['ret'] !=0)
	    {
	        return $result_index;
	    }
	    if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_index;
	    }
	    $result_index = $result_index['data_info'][0];
	    $result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $result_media['nns_vod_index_id'],$result_index,null,'cdn',$this->str_header);
	    
	    if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
	    {
	        return $result_index_import_info;
	    }
	    
	    $result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_media['nns_vod_id']);
	    if($result_video['ret'] !=0)
	    {
	        return $result_video;
	    }
	    if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_video;
	    }
	    $result_video = $result_video['data_info'][0];
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_media['nns_vod_id'],$result_video,null,'cdn',$this->str_header);
	    
	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        return $result_video_import_info;
	    }

        //>>构建上线请求的报文信息
        $time=$task_log_id=$str_target_system_id=$str_sp_id=$str_target_system_id="";
        //时间戳
        $time = $this->get_java_datetime();
        //消息id
        $task_log_id = $this->get_guid_rand('UN',$result_media['nns_integer_id']);
        //目标系统标识 指26-S(标清系统)或者26-H(高清系统)
        $str_target_system_id = '30-S';
        //思华方配置的spid
        $str_sp_id = $this->str_sihua_sp_id;
        $str_target_site_id = $this->str_target_site_id;
        $int_sequence = time();
        //发送的报文实体
/*	    $xml_str = '<?xml version="1.0" encoding="utf-8"?>';*/
//	    $xml_str.= '<message module="iCMS" version="1.0">';
//	    $xml_str.=     '<header timestamp="'.$time.'" sequence="'.$int_sequence.'" component-id="iCMS_01" component-type="iCMS" action="REQUEST" command="ONLINE_TASK_DONE"/>';
//	    $xml_str.=     '<body>';
//	    $xml_str.=         '<tasks>';
//	    $xml_str.=             '<task sp-id="'.$str_sp_id.'" content-id="'.$result_index_import_info['data_info'].'" subcontent-id="'.$result_media_import_info['data_info'].'" target-system-id="'.$str_target_system_id.'" target-site-id="'.$str_target_site_id.'" status="6"/>';
//        $xml_str.=         '</tasks>';
//        $xml_str.=     '</body>';
//	    $xml_str.= '</message>';

        $xml_str =<<<XML
<?xml version="1.0" encoding="utf-8"?>
<message module="iCMS" version="1.0"> 
  <header action="REQUEST" command="ONLINE_TASK_DONE" component-id="iCMS_01" component-type="iCMS" sequence="{$int_sequence}" timestamp="{$time}"/>  
  <body> 
    <tasks> 
      <task page-id="3" sp-id="{$str_sp_id}" status="6" content-id="{$result_index_import_info['data_info']}" subcontent-id="{$result_media_import_info['data_info']}" target-site-id="{$str_target_site_id}" target-system-id="{$str_target_system_id}"/> 
    </tasks> 
  </body> 
</message>
XML;
	    $action = ($result_media['nns_deleted'] !=1) ? 'ONLINE' : 'OFFLINE';
	    $c2_info = array(
	        'nns_id'=>$task_log_id,
	        'nns_task_type'=>'Movie',
	        'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	        'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
	        'nns_action'=>	$action,
	        'nns_content'=>	$xml_str,
	    );
	    $result = $this->execute_self_c2($c2_info,false);
	    return $result;
	}
	
	
	
	
	
	/**
	 * 上线操作
	 */
	public function do_media_unline()
	{
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_ref_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $movie_id = $this->arr_params['bk_c2_task_info']['nns_ref_id'];
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	    $result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $movie_id);
	    if($result_media['ret'] !=0)
	    {
	        return $result_media;
	    }
	    if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_media;
	    }
	    $result_media = $result_media['data_info'];
		$this->init_cp_info($result_media['nns_cp_id']);
	    $result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $movie_id,$result_media,null,'cdn',$this->str_header);
	    if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
	    {
	        return $result_media_import_info;
	    }
	     
	    $result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $result_media['nns_vod_index_id']);
	    if($result_index['ret'] !=0)
	    {
	        return $result_index;
	    }
	    if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_index;
	    }
	    $result_index = $result_index['data_info'][0];
	    $result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $result_media['nns_vod_index_id'],$result_index,null,'cdn',$this->str_header);
	     
	    if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
	    {
	        return $result_index_import_info;
	    }
	     
	    $result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_media['nns_vod_id']);
	    if($result_video['ret'] !=0)
	    {
	        return $result_video;
	    }
	    if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_video;
	    }
	    $result_video = $result_video['data_info'][0];
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_media['nns_vod_id'],$result_video,null,'cdn',$this->str_header);
	     
	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        return $result_video_import_info;
	    }

	    //>>构建上线请求的报文信息
        $time=$task_log_id=$str_target_system_id=$str_target_site_id=$str_sp_id='';
        //时间戳
	    $time = $this->get_java_datetime();
	    //消息id
	    $task_log_id = $this->get_guid_rand('UN',$result_media['nns_integer_id']);
	    //目标系统标识 指26-S(标清系统)或者26-H(高清系统)
	    $str_target_system_id = '30-S';
	    $str_target_site_id = $this->str_target_site_id;
        $str_sp_id = $this->str_sihua_sp_id;
        $int_sequence = time();
	    //报文实体内容xml
/*	    $xml_str = '<?xml version="1.0" encoding="utf-8"?>';*/
//	    $xml_str.= '<message module="iCMS" version="1.0">';
//	    $xml_str.=     '<header timestamp="'.$time.'" sequence="'.$int_sequence.'" component-id="CLPS_01" component-type="CLPS" action="REQUEST" command="CONTENT_OFFLINE"/>';
//	    $xml_str.=     '<body>';
//	    $xml_str.=         '<contents>';
//	    $xml_str.=             '<content sp-id="'.$str_sp_id.'" content-id="'.$result_index_import_info['data_info'].'" subcontent-id="'.$result_media_import_info['data_info'].'" target-system-id="'.$str_target_system_id.'" target-site-id="'.$str_target_site_id.'" status="6"/>';
//	    $xml_str.=         '</contents>';
//	    $xml_str.=     '</body>';
//	    $xml_str.= '</message>';

        $xml_str = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<message module="iCMS" version="1.0"> 
  <header action="REQUEST" command="CONTENT_OFFLINE" component-id="CLPS_01" component-type="CLPS" sequence="{$int_sequence}" timestamp="{$time}"/>  
  <body> 
    <contents> 
      <content content-id="{$result_index_import_info['data_info']}" sp-id="{$str_sp_id}" status="6" subcontent-id="{$result_media_import_info['data_info']}" target-site-id="{$str_target_site_id}" target-system-id="{$str_target_system_id}"/> 
    </contents> 
  </body> 
</message>

XML;

	    
	    $action = ($result_media['nns_deleted'] !=1) ? 'ONLINE' : 'OFFLINE';
	    $c2_info = array(
	        'nns_id'=>$task_log_id,
	        'nns_task_type'=>'Movie',
	        'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	        'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
	        'nns_action'=>	$action,
	        'nns_content'=>	$xml_str,
	    );
	    $result = $this->execute_self_c2($c2_info,false);
	    return $result;
	}
	
	
	/**
	 * 获取随机字符串
	 * @param unknown $len
	 */
	private function get_rand_str($len) 
	{  
        $chars = array(  
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9');  
        $charsLen = count($chars) - 1;
        $output = '';  
        for ($i = 0; $i < $len; $i++) 
        {  
            $output .= $chars[rand(0, $charsLen)];  
        }  
        return $output;  
    }  
	
	
	public function do_notify()
	{
	    return $this->notify_upstream(array('nns_cp_id'=>$this->arr_params['nns_cp_id']),$this->arr_params['action'],$this->arr_params['result'],'video');
	}
	
	
	/**
	 * 
	 * @param unknown $result_media
	 * @param unknown $action
	 * @param unknown $result
	 * @param string $type
	 */
	private function notify_upstream($result_media,$action,$result,$type = 'media')
	{
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/notify/make_notify_file.class.php';
	    global $g_project_name;
	    $project_name = (isset($g_project_name) && strlen($g_project_name)>0) ? $g_project_name : '';
	    if(strlen($project_name) <1)
	    {
	        return $this->_make_return_data(1,"全局项目ID配置为空:[{$project_name}];line:[".__LINE__."];file[".__FILE__."]");
	    }
	    global $g_bk_web_url;
	    $bk_web_url = (isset($g_bk_web_url) && strlen($g_bk_web_url)>0) ? $g_bk_web_url : '';
	    $bk_web_url = rtrim(rtrim(trim($bk_web_url),'/'),'\\');
	    if(strlen($bk_web_url) <1)
	    {
	        return $this->_make_return_data(1,"后台基本地址(数据库配置文件父级目录)配置为空:[{$bk_web_url}];line:[".__LINE__."];file[".__FILE__."]");
	    }
	    unset($g_project_name);
	    unset($g_bk_web_url);
	    $str_include_file = dirname(dirname(dirname(dirname(__FILE__)))).'/notify/soap/'.$project_name.'/notify.php';
	    if(!file_exists($str_include_file))
	    {
	        return $this->_make_return_data(1,"加载消息反馈文件不存在:[{$str_include_file}];line:[".__LINE__."];file[".__FILE__."]");
	    }
	    include_once $str_include_file;
	    $ResultCode = (isset($result['ret']) && $result['ret'] == 0) ? 0 : -1;
	    $Description = ($ResultCode == 0) ? 'success' : 'error';
	    #TODO
 	    $result_media['nns_content_id'] = '1111111';
        if($type == 'media')
        {
	        $obj_make_file = new nl_make_notify_file($this->str_sp_id,$result_media['nns_cp_id']);
            $result_notify_file = $obj_make_file->make_notify_file($result_media,$ResultCode,$Description,'media',$action);
            if($result_notify_file['ret'] !=0)
            {
                return $result_notify_file;
            }
            $site_callback_url = rtrim(rtrim(trim($this->arr_sp_config['site_callback_url']),'/'),'\\');
            if(strlen($site_callback_url) <1)
            {
                return $this->_make_return_data(1,"消息反馈上游地址为空:[{$site_callback_url}];line:[".__LINE__."];file[".__FILE__."]");
            }
        }
        else
        {
            $site_callback_url='';
            $result_notify_file=array();
            $result_notify_file['data_info']='';
        }
	    $WDSL_URL = $bk_web_url.'/notify/soap/'.$project_name.'/'.$result_media['nns_cp_id'].'/service.php?wsdl';
	    $obj_soap = new notify($WDSL_URL,$result_media['nns_cp_id'],'starcor',$this->arr_params['bk_c2_task_info']['nns_message_id'],(int)$ResultCode,$Description,$site_callback_url.$result_notify_file['data_info']);
	    $soap_result = $obj_soap->ExecCmd();
        if(isset($soap_result->ResultCode))
        {
            $ResultCode = ($soap_result->ResultCode == 0) ? 0 : 1;
        }
        else
        {
            $ResultCode = ($soap_result->Result == 0) ? 0 : 1;
        }
	    return  $this->_make_return_data($ResultCode,isset($soap_result->ErrorDescription) ? $soap_result->ErrorDescription : '');
	}
	
	
	/**
	 * 直播片源CDN
	 * @param unknown $movie_id
	 */
	public function do_live_media()
	{
		if(!isset($this->arr_params['nns_channel_id']) && !empty($this->arr_params['nns_channel_id']))
		{
			return array(
					'ret'=>1,
					'reason'=>'直播片源传入参数无nns_channel_id字段,参数为:'.var_export($this->arr_params),
			);
		}
		$channel_id = $this->arr_params['nns_channel_id'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/live.class.php';
		$result_channel = nl_live::query_by_id($this->obj_dc, $channel_id);
		if($result_channel['ret'] !=0 || !isset($result_channel['data_info']) || !is_array($result_channel['data_info']) || empty($result_channel['data_info']))
		{
			return $result_channel;
		}
		$result_channel = $result_channel['data_info'];
		$result_channel_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'live', $channel_id,$result_channel,null,'cdn',$this->str_header);
		if($result_channel_import_info['ret'] !=0 || !isset($result_channel_import_info['data_info']) || strlen($result_channel_import_info['data_info']) <1)
		{
			return $result_channel_import_info;
		}
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/live_media.class.php';
		$result_media = nl_live_media::query_by_channel_id($this->obj_dc, $channel_id);
		if($result_media['ret'] !=0 || !isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
		{
			return $result_media;
		}
		$result_media = $result_media['data_info'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/c2_task/c2_task.class.php';
		foreach ($result_media as $media_list)
		{
			$result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'live_media', $media_list['nns_id'],$media_list,null,'cdn',$this->str_header);
			if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
			{
				return $result_media_import_info;
			}
			$media_list['nns_cdn_media_guid'] = $result_media_import_info['data_info'];
			$xml_str='';
			$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
			$xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
			$xml_str .= 	'<Objects>';
			$xml_str .=     $this->make_live_media_xml($media_list,array('nns_cdn_channel_guid'=>$result_channel_import_info['data_info']));
			$xml_str .= 	'</Objects>';
			$xml_str .= '</ADI>';
			$action = ($result_media['nns_deleted'] !=1) ? ($result_media['nns_modify_time'] > $result_media['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
			$str_action = ($result_media['nns_deleted'] !=1) ? ($result_media['nns_modify_time'] > $result_media['nns_create_time']) ? 'modify' : 'add' : 'destroy';
			$file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'live_media' . '_' . $action . '_' .  rand(100, 999) . '.xml';
			
			$str_task_id = np_guid_rand();
			$result_c2_exsist=nl_c2_task::query_c2_exsist($this->obj_dc,'live_media',$media_list['nns_id'],$this->str_sp_id);
			if($result_c2_exsist['ret'] !=0)
			{
				return $result_c2_exsist;
			}
			$result_c2_exsist = isset($result_c2_exsist['data_info'][0]) ? $result_c2_exsist['data_info'][0] : null;
			if(is_array($result_c2_exsist) && !empty($result_c2_exsist))
			{
				$str_task_id = $result_c2_exsist['nns_id'];
				$result_c2_edit=nl_c2_task::edit($this->obj_dc, array('nns_action'=>$str_action,'nns_status'=>1,'nns_epg_status'=>97),$str_task_id);
				if($result_c2_edit['ret'] !=0)
				{
					return $result_c2_edit;
				}
			}
			else
			{
				$arr_c2_add = array(
						'nns_id'=>$str_task_id,
						'nns_type'=>'live_media',
						'nns_name'=>"[{$result_channel['nns_name']}] 1 [{$media_list['nns_mode']}]-直播流",
						'nns_ref_id'=>'',
						'nns_action'=>$str_action,
						'nns_status'=>1,
						'nns_org_id'=>$this->str_sp_id,
						'nns_category_id'=>'',
						'nns_src_id'=>$media_list['nns_id'],
						'nns_all_index'=>1,
						'nns_clip_task_id'=>'',
						'nns_clip_date'=>'',
						'nns_op_id'=>'',
						'nns_epg_status'=>97,
						'nns_ex_url'=>'',
						'nns_file_path'=>'',
						'nns_file_size'=>'',
						'nns_file_md5'=>'',
						'nns_cdn_policy'=>'',
						'nns_epg_fail_time'=>0,
						'nns_message_id'=>'',
				);
				$result_c2_add=nl_c2_task::add($this->obj_dc, $arr_c2_add);
				if($result_c2_edit['ret'] !=0)
				{
					return $result_c2_add;
				}
			}
			
			$c2_info = array(
					'nns_task_type'=>'PhysicalChannel',
					'nns_task_id'=> $str_task_id,
					'nns_task_name'=> "[{$result_channel['nns_name']}] 1 [{$media_list['nns_mode']}]-直播流",
					'nns_action'=>	$action,
					'nns_url' => $file_name,
					'nns_content' => $xml_str,
					'nns_desc' => 'PhysicalChannel,'.$action,
			);
			return $this->execute_self_c2($c2_info);
		}
	}
	
	
	/**
	 * 直播节目单CDN
	 * @param unknown $movie_id
	 */
	public function do_playbill()
	{
		if(!isset($this->arr_params['nns_playbill_id']) && !empty($this->arr_params['nns_playbill_id']))
		{
			return array(
					'ret'=>1,
					'reason'=>'节目单传入参数无nns_playbill_id字段,参数为:'.var_export($this->arr_params),
			);
		}
		$playbill_id = $this->arr_params['nns_playbill_id'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/live_media.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/playbill.class.php';
		$result_playbill = nl_playbill::query_by_id($this->obj_dc, $playbill_id);
		if($result_playbill['ret'] !=0 || !isset($result_playbill['data_info']) || !is_array($result_playbill['data_info']) || empty($result_playbill['data_info']))
		{
			return $result_playbill;
		}
		$result_playbill = $result_playbill['data_info'];
		$result_live_media = nl_live_media::query_by_id($this->obj_dc, $result_playbill['nns_live_media_id']);
		if($result_live_media['ret'] !=0 || !isset($result_live_media['data_info']) || !is_array($result_live_media['data_info']) || empty($result_live_media['data_info']))
		{
			return $result_live_media;
		}
		$result_live_media = $result_live_media['data_info'];
		$result_playbill_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'playbill', $playbill_id,$result_playbill,null,'cdn',$this->str_header);
		if($result_playbill_import_info['ret'] !=0 || !isset($result_playbill_import_info['data_info']) || strlen($result_playbill_import_info['data_info']) <1)
		{
			return $result_playbill_import_info;
		}
		$result_live_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'live_media', $playbill_id,$result_live_media,null,'cdn',$this->str_header);
		if($result_live_media_import_info['ret'] !=0 || !isset($result_live_media_import_info['data_info']) || strlen($result_live_media_import_info['data_info']) <1)
		{
			return $result_live_media_import_info;
		}
		$result_playbill['nns_cdn_playbill_guid'] = $result_playbill_import_info['data_info'];
		$result_playbill['nns_cdn_live_media_guid'] = $result_live_media_import_info['data_info'];
		$result_playbill_ex['nns_filetype'] = $result_live_media['nns_filetype'];
		$result_playbill_ex['nns_import_source'] = $result_live_media['nns_import_source'];
		$result_playbill_ex['nns_domain'] = $result_live_media['nns_domain'];
		$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
		$xml_str .= 	'<Objects>';
		$xml_str .=     $this->make_playbill_xml($result_playbill,$result_playbill_ex);
		$xml_str .= 	'</Objects>';
		$xml_str .= '</ADI>';
		
		$action = ($result_playbill['nns_state'] !=1) ? ($result_playbill['nns_modify_time'] > $result_playbill['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
		$str_action = ($result_playbill['nns_state'] !=1) ? ($result_playbill['nns_modify_time'] > $result_playbill['nns_create_time']) ? 'modify' : 'add' : 'destroy';
		$file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'playbill' . '_' . $action . '_' .  rand(100, 999) . '.xml';
		$str_task_id = np_guid_rand();
		$result_c2_exsist=nl_c2_task::query_c2_exsist($this->obj_dc,'playbill',$result_playbill['nns_id'],$this->str_sp_id);
		if($result_c2_exsist['ret'] !=0)
		{
			return $result_c2_exsist;
		}
		$result_c2_exsist = isset($result_c2_exsist['data_info'][0]) ? $result_c2_exsist['data_info'][0] : null;
		if(is_array($result_c2_exsist) && !empty($result_c2_exsist))
		{
			$str_task_id = $result_c2_exsist['nns_id'];
			$result_c2_edit=nl_c2_task::edit($this->obj_dc, array('nns_action'=>$str_action,'nns_status'=>1,'nns_epg_status'=>97),$str_task_id);
			if($result_c2_edit['ret'] !=0)
			{
				return $result_c2_edit;
			}
		}
		else
		{
			$arr_c2_add = array(
					'nns_id'=>$str_task_id,
					'nns_type'=>'playbill',
					'nns_name'=>"[{$result_playbill['nns_name']}]-节目单",
					'nns_ref_id'=>$result_playbill['nns_live_media_id'],
					'nns_action'=>$str_action,
					'nns_status'=>1,
					'nns_org_id'=>$this->str_sp_id,
					'nns_category_id'=>'',
					'nns_src_id'=>$result_playbill['nns_id'],
					'nns_all_index'=>1,
					'nns_clip_task_id'=>'',
					'nns_clip_date'=>'',
					'nns_op_id'=>'',
					'nns_epg_status'=>97,
					'nns_ex_url'=>'',
					'nns_file_path'=>'',
					'nns_file_size'=>'',
					'nns_file_md5'=>'',
					'nns_cdn_policy'=>'',
					'nns_epg_fail_time'=>0,
					'nns_message_id'=>'',
			);
			$result_c2_add=nl_c2_task::add($this->obj_dc, $arr_c2_add);
			if($result_c2_edit['ret'] !=0)
			{
				return $result_c2_add;
			}
		}
		$c2_info = array(
				'nns_task_type'=>'Schedule',
				'nns_task_id'=> $str_task_id,
				'nns_task_name'=> "[{$result_playbill['nns_name']}]-节目单",
				'nns_action'=>	$action,
				'nns_url' => $file_name,
				'nns_content' => $xml_str,
				'nns_desc' => 'Schedule,'.$action,
		);
		return $this->execute_self_c2($c2_info);
	}
	
	
	/**
	 * 
	 * @param unknown $c2_info
	 */
	public function execute_self_c2($c2_info,$flag=true)
	{
	    $c2_info['nns_type'] = 'std';
	    $c2_info['nns_org_id'] = $this->str_sp_id;
	    $c2_info['nns_desc'] ='';
// 	    $c2_info['nns_id'] = np_guid_rand();
	    include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/np/np_http_curl.class.php';
	    //检查时间范围是否有效执行
	    if ($this->exists_c2_commond($c2_info))
	    {
	        return $this->_make_return_data(1,'该task在一小时内执行过或则task查询无数据,task信息:'.var_export($c2_info,true));
	    }
	    $sub_path = date('Y-m-d');
	    $file = $this->str_sp_id . '_' .$c2_info['nns_action'].'_'. $c2_info['nns_id'] . '.xml';
	    $c2_info['nns_url'] = $sub_path . '/'.strtolower($c2_info['nns_task_type']).'/' . $file;
	    //生成soap xml文件 路径
	    $file_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/mgtv/' . $this->str_sp_id . '/inject_xml/' . $sub_path . '/' .strtolower($c2_info['nns_task_type']).'/';
	    if (!is_dir($file_path))
	    {
	        mkdir($file_path, 0777, true);
	    }
	    $file_path .= $file;
	    file_put_contents($file_path, $c2_info['nns_content']);
	    $cdn_send_mode = $this->arr_sp_config['cdn_send_mode'];
	    if($cdn_send_mode != '1')
	    {
	        $c2_info['nns_desc'] = "注入下游模式非HTTP";
	        nl_c2_log::add($this->obj_dc, $c2_info);
	        return $this->_make_return_data(1,"注入下游模式非HTTP");
	    }
	    if($flag)
	    {
    	    $cdn_send_mode_url = $this->arr_sp_config['cdn_send_mode_url'];
    	    if(strlen($cdn_send_mode_url) <1)
    	    {
    	        $c2_info['nns_desc'] = "注入下游媒资基本信息请求url为空";
    	        nl_c2_log::add($this->obj_dc, $c2_info);
    	        return $this->_make_return_data(1,"注入下游请求url为空");
    	    }
	    }
	    else 
	    {
	        $cdn_send_mode_url = $this->arr_sp_config['cdn_send_cdi_mode_url'];
	        if(strlen($cdn_send_mode_url) <1)
	        {
	            $c2_info['nns_desc'] = "注入下游上下线信息请求url为空";
	            nl_c2_log::add($this->obj_dc, $c2_info);
	            return $this->_make_return_data(1,"注入下游请求url为空");
	        }
	    }
	    if(strlen($c2_info['nns_id']) !=32)
	    {
    	    $obj_curl = new np_http_curl_class();
    	    $arr_header = array(
    	        "Content-type: application/xml;charset=\"utf-8\"",
    	        "Accept: text/xml",
    	        "Cache-Control: no-cache",
    	        "Pragma: no-cache",
    	        "SOAPAction: \"run\"",
    	    );
    	    $request_ret = $obj_curl->do_request('post',$cdn_send_mode_url,$c2_info['nns_content'],$arr_header,60);
            nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, ';收到来自CDN的内容:' . $request_ret."发送内容:{$c2_info['nns_content']}", $this->str_sp_id);
    	    $curl_info = $obj_curl->curl_getinfo();
    	    if($curl_info['http_code'] != '200')
    	    {
    	        $c2_info['nns_result'] = '[' . 1 . ']';
    	        nl_c2_log::add($this->obj_dc, $c2_info);
    	        nl_c2_task::edit_cdn_fail_time_v2($this->obj_dc,$c2_info['nns_task_id']);
    	        return $this->_make_return_data(1,"CMS应答非200状态，[HTTP状态码]:{$curl_info['http_code']}[HTTP错误]".$obj_curl->curl_error());
    	    }
    	    if($c2_info['nns_action'] != 'OFFLINE' && $c2_info['nns_action'] != 'ONLINE')
    	    {
    	        $request_ret = explode('|', $request_ret);
    	        $str_code = (isset($request_ret[0]) && $request_ret[0] == '0') ? 1 : '-1';
    	        $str_description = (isset($request_ret[1])) ? $request_ret[1] : 'unkown error';
    	    }
    	    else
    	    {
    	        $request_ret = simplexml_load_string($request_ret);
    	        $request_ret = json_decode(json_encode($request_ret),true);
    	        $str_code = $request_ret["body"]["result"]["@attributes"]["code"];
    	        $str_description = $request_ret["body"]["result"]["@attributes"]["description"];
    	    }
	    }
	    else 
	    {
	        $str_code = '-1';
	        $str_description = "循环了3次都不能生成唯一ID,SB的思华";
	    }
	    if((isset($str_code) && $str_code == '1'))
	    {
	        $c2_info['nns_result'] = '[0]';
	        $c2_info['nns_status'] = '5';
	        $c2_info['nns_desc'] = $str_description;
	        nl_c2_task::edit($this->obj_dc, array('nns_status'=>'6'), $c2_info['nns_task_id']);
	    }
	    else
	    {
	        $c2_info['nns_result'] = '[1]';
	        $c2_info['nns_status'] = '-1';
            $c2_info['nns_desc'] = $str_description;
	        nl_c2_task::edit_cdn_fail_time_v2($this->obj_dc,$c2_info['nns_task_id']);
	    }
	    $c2_info['nns_content']='';
	    nl_c2_log::add($this->obj_dc, $c2_info);
	    return $this->_make_return_data(0,"OK".var_export($request_ret,true));
	}

    /**
     * 思华-转换成Java的时间格式
     * @author <feijian.gao@starcor.com>
     * @date    2017年9月1日14:57:55
     * @return false|mixed|string
     */
	private function get_java_datetime()
    {
        $time = time();
        $date = date('c',time());
        $date_zone_replace = date('O',time());
        $date_zone_search = date('P',time());
        $date = str_replace($date_zone_search,$date_zone_replace,$date);
        $date = str_replace("+",". ",$date);
        return $date;
    }
}
