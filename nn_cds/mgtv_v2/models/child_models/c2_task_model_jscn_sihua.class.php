<?php
/**
 * starcor 江苏思华（老版本）规范ADI CDN注入
 * @author liangpan
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/v2/ns_core/m_mediainfo.class.php';
class c2_task_model_v2 extends nn_public_model
{
	private $str_cmsid = 'JSB12028';
	private $str_header = 'JSB12028';
	private $str_target_system_id = '30-S';
	private $str_target_site_id = "Nanjing";
	private $str_sihua_sp_id = "SP1N02M00_04_063";
	private $arr_cp = null;
	private $str_system_id = '';
	private $str_adi_url = '';
	private $str_cdi_url = '';
	
	/**
	 * 初始化第三方的CP 信息
	 * @param unknown $cp_id
	 */
	private function init_cp_info($cp_id)
	{
	    $result_cp = nl_cp::query_by_id($this->obj_dc, $cp_id);
	    if($result_cp['ret'] !=0 || !isset($result_cp['data_info']) || empty($result_cp['data_info']) || !is_array($result_cp['data_info']))
	    {
	        return $result_cp;
	    }
	    $result_cp = $result_cp['data_info'];
	    $result_cp['nns_config'] = $this->is_json($result_cp['nns_config']) ? json_decode($result_cp['nns_config'],true) : null;
	    $this->arr_cp = $result_cp;
	    $result_project_key = nl_project_key::query_by_key($this->obj_dc, $cp_id);
	    if($result_project_key['ret'] !=0 || !isset($result_project_key['data_info']) || empty($result_project_key['data_info']) || !is_array($result_project_key['data_info']))
	    {
	        return $result_project_key;
	    }
	    $result_project_key = $result_project_key['data_info'];
	    $nns_key_value = isset($result_project_key['nns_key_value']) ? $result_project_key['nns_key_value'] : '';
	    if(!$this->is_json($nns_key_value))
	    {
	        return $result_project_key;
	    }
	    $nns_key_value = json_decode($nns_key_value,true);
	    if(isset($nns_key_value[$this->str_sp_id]) && strlen($nns_key_value[$this->str_sp_id])>0)
	    {
	        $this->str_cmsid  = $nns_key_value[$this->str_sp_id];
	        $this->str_header = $nns_key_value[$this->str_sp_id];
	    }
	    if(isset($nns_key_value['target_system_id']) && strlen($nns_key_value['target_system_id'])>0)
	    {
	        $this->str_target_system_id = $nns_key_value['target_system_id'];
	    }
	    if(isset($nns_key_value['target_site_id']) && strlen($nns_key_value['target_site_id'])>0)
	    {
	        $this->str_target_site_id = $nns_key_value['target_site_id'];
	    }
	    if(isset($nns_key_value['sihua_sp_id']) && strlen($nns_key_value['sihua_sp_id'])>0)
	    {
	        $this->str_sihua_sp_id = $nns_key_value['sihua_sp_id'];
	    }
	    if(isset($nns_key_value['systemid']) && strlen($nns_key_value['systemid'])>0)
	    {
	        $this->str_system_id = $nns_key_value['systemid'];
	    }
	    if(isset($nns_key_value['cdi_url']) && strlen($nns_key_value['cdi_url'])>0)
	    {
	        $this->str_cdi_url = $nns_key_value['cdi_url'];
	    }
	    if(isset($nns_key_value['adi_url']) && strlen($nns_key_value['adi_url'])>0)
	    {
	        $this->str_adi_url = $nns_key_value['adi_url'];
	    }
	    return $result_project_key;
	}
	
	private function my_sub_str($string,$str_len=28)
	{
	    return mb_substr($string, 0,$str_len,'UTF-8');
	}
	
	
	
	
	private function make_video_xml($task_log_id,$arr_info_ex)
	{
	    $task_log_id = $this->make_other_guid($arr_info_ex['video']['base']['nns_integer_id'],'video',0,'PA');
	    $arr_info_ex['video']['base']['nns_summary'] = $this->my_sub_str($arr_info_ex['video']['base']['nns_summary'],100);
	    $str_category = isset($this->arr_cp['nns_name']) ? $this->arr_cp['nns_name'] : '未知';
	    $str_date = date("Y-m-d");
	    $str_year = date("Y");
	    $str_Action = ($arr_info_ex['video']['base']['nns_deleted'] !=1) ? '' : 'DELETE';
	    $arr_img = null;
	    $str_img_ftp = (isset($this->arr_sp_config['img_ftp']) && strlen($this->arr_sp_config['img_ftp'])>0) ? trim($this->arr_sp_config['img_ftp'],'/').'/' : '';
	    $arr_img_index = array('nns_image_v','nns_image_h','nns_image_s');
	    foreach ($arr_img_index as $key=>$val)
	    {
	        if(isset($arr_info_ex['video']['base'][$val]) && strlen($arr_info_ex['video']['base'][$val])>0)
	        {
	            $arr_info_ex['video']['base'][$val] = trim($arr_info_ex['video']['base'][$val],'/');
	            $md5_img = $this->make_other_guid($arr_info_ex['video']['base']['nns_integer_id'],'video',$key);
	            $arr_img[$md5_img] = rtrim($str_img_ftp,'/').'/'.trim($arr_info_ex['video']['base'][$val],'/');
	        }
	    }
	    $str_language_id = $this->make_other_guid($arr_info_ex['video']['base']['nns_integer_id'],'video',0,'LA');
	    $arr_info_ex['index']['base']['nns_index']++;
	    $arr_info_ex['video']['base']['nns_new_index']++;
	    $str_index_name = $arr_info_ex['video']['base']['nns_name'].'第'.$arr_info_ex['index']['base']['nns_index'].'集';
	    $str_xml = '';
	    //packages信息, 其中Asset_ID取值为栏目title的Asset_ID
	    $str_xml.= '<Metadata>';
	    $str_xml.=     '<AMS Verb="'.$str_Action.'" Asset_Class="packages" Asset_ID="'.$task_log_id.'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_summary'].'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	    $str_xml.=     '<App_Data Value="CableLabsVOD 1.1" Name="Metadata_Spec_Version" App="MOD"/>';
	    $str_xml.= '</Metadata>';
        //栏目资产
	    $str_xml.= '<Asset>';
        //栏目title信息, 即栏目元数据信息
	    $str_xml.=     '<Metadata>';
	    $str_xml.=         '<AMS Verb="'.$str_Action.'" Asset_Class="title" Asset_ID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_summary'].'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	    $str_xml.=         '<App_Data Value="series" Name="Show_Type" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="0" Name="Issue_Number" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_all_index'].'" Name="Volumn_Count" App="MOD"/>';
        $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_new_index'].'" Name="New_Count" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" Name="Original_Asset_ID" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="1" Name="Original_System_ID" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="1" Name="License_Type" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Culture" Name="Genre" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Proper_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Parallel_Proper_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Subordinate_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Alternative_ Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Title_Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="11" Name="Version_Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Key_Words" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_summary'].'" Name="Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Impressive_Dialogue" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Impressive_Plot" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Hot_Comments" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Sentence_Review" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Behind_Scenes" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="6.8" Name="IMDB_Rating" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="G" Name="MPAA_Rating" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_category.'" Name="Category" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Y" Name="Season_Premiere" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="N" Name="Season_Finale" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="00:00:00" Name="Preview_Period" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="5" Name="Asset_Recommend_Class" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="0" Name="Chapter" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_year.'" Name="Year" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="0.0" Name="Suggested_Price" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Adult" Name="Audience" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="2056-01-01 20:00:00" Name="Subscriber_View_Limit" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Region" App="MOD"/>';
	    $str_xml.=     '</Metadata>';
	    //栏目资产下的asset(language信息)
        $str_xml.=     '<Asset>';
        //language信息
//         $language_name = $arr_info_ex['video']['base']['nns_name'];
        $language_name = $this->my_sub_str($arr_info_ex['video']['base']['nns_name'],30);
        $str_xml.=          '<Metadata>';
        $str_xml.=              '<AMS Verb="'.$str_Action.'" Asset_Class="language" Asset_ID="'.$str_language_id.'" Asset_Name="'.$language_name.'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$language_name.'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
        $str_xml.=              '<App_Data Value="zh" Name="Type" App="MOD"/>';
        $str_xml.=              '<App_Data Value="CH" Name="Description" App="MOD"/>';
        $str_xml.=              '<App_Data Value="zh_CN" Name="Tag" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="99" Name="Regin" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="南京一中" Name="Producers" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="" Name="Actors" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="" Name="Director" App="MOD"/>';
        $str_xml.=              '<App_Data Value="'.$language_name.'" Name="Title" App="MOD"/>';
        $str_xml.=              '<App_Data Value="'.$language_name.'" Name="Title_Brief" App="MOD"/>';
        $str_xml.=          '</Metadata>';
        $str_xml.=     '</Asset>';
	    //栏目资产下的asset(poster信息)
        if (is_array($arr_img) && !empty($arr_img))
        {
            foreach ($arr_img as $img_key=>$img_val)
            {
                $Content_Check_Sum = md5_file($img_val);
                $Content_Check_Sum = $Content_Check_Sum === false ? md5($img_val) : $Content_Check_Sum;

                $size = @getimagesize($img_val);
                $Horizontal_Pixels = (isset($size[0]) && (int)$size[0] >0 && is_array($size) && !empty($size)) ? (int)$size[0] : 320;
                $Vertical_Pixels = (isset($size[1]) && (int)$size[1] >0 && is_array($size) && !empty($size)) ? (int)$size[1] : 240;
                $str_xml.=     '<Asset>';
                $str_xml.=         '<Metadata>';
                $str_xml.=             '<AMS Verb="'.$str_Action.'" Asset_Class="poster" Asset_ID="'.$img_key.'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'海报" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_name'].'海报" Version_Major="1" Version_Minor="0" Product="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'海报" Name="Description" App="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$img_key.'" Name="Original_Asset_ID" App="MOD"/>';
                $str_xml.=             '<App_Data Value="1" Name="Original_System_ID" App="MOD"/>';
                $str_xml.=             '<App_Data Value="1" Name="Screen_Format" App="MOD"/>';
                $str_xml.=             '<App_Data Value="1" Name="Usage" App="MOD"/>';
                $str_xml.=             '<App_Data Value="Y" Name="Encryption" App="MOD"/>';
                $str_xml.=             '<App_Data Value="52856" Name="Content_File_Size" App="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$Horizontal_Pixels.'*'.$Vertical_Pixels.'" Name="Image_Aspect_Ratio" App="MOD"/>';
                $str_xml.=             '<App_Data Value="SD" Name="VideoType" App="MOD"/>';
                $str_xml.=             '<App_Data Value="RGB" Name="Color_Type" App="MOD"/>';
                $str_xml.=             '<App_Data Value="jpg" Name="Image_Encoding_Profile" App="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$Horizontal_Pixels.'" Name="Horizontal_Pixels" App="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$Vertical_Pixels.'" Name="Vertical_Pixels" App="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$Content_Check_Sum.'" Name="Content_Check_Sum" App="MOD"/>';
                $str_xml.=         '</Metadata>';
                $str_xml.=         '<Content Value="'.$img_val.'"/>';
                $str_xml.=     '</Asset>';
            }
        }
	    $str_xml.= '</Asset>';
	    return $str_xml;
	}
	
	
	private function make_video_movie_xml($task_log_id,$arr_info_ex)
	{
	    $task_log_id = $this->make_other_guid($arr_info_ex['video']['base']['nns_integer_id'],'video',0,'PA');
	    $arr_info_ex['video']['base']['nns_summary'] = $this->my_sub_str($arr_info_ex['video']['base']['nns_summary'],100);
	    $str_category = isset($this->arr_cp['nns_name']) ? $this->arr_cp['nns_name'] : '未知';
	    $str_date = date("Y-m-d");
	    $str_year = date("Y");
	    $str_Action = ($arr_info_ex['video']['base']['nns_deleted'] !=1) ? '' : 'DELETE';
	    $arr_img = null;
	    $str_img_ftp = (isset($this->arr_sp_config['img_ftp']) && strlen($this->arr_sp_config['img_ftp'])>0) ? trim($this->arr_sp_config['img_ftp'],'/').'/' : '';
	    $arr_img_index = array('nns_image_v','nns_image_h','nns_image_s');
	    foreach ($arr_img_index as $key=>$val)
	    {
	        if(isset($arr_info_ex['video']['base'][$val]) && strlen($arr_info_ex['video']['base'][$val])>0)
	        {
	            $arr_info_ex['video']['base'][$val] = trim($arr_info_ex['video']['base'][$val],'/');
	            $md5_img = $this->make_other_guid($arr_info_ex['video']['base']['nns_integer_id'],'video',$key);
	            $arr_img[$md5_img] = rtrim($str_img_ftp,'/').'/'.trim($arr_info_ex['video']['base'][$val],'/');
	        }
	    }
	    $str_language_id = $this->make_other_guid($arr_info_ex['video']['base']['nns_integer_id'],'video',0,'LA');
	    $arr_info_ex['index']['base']['nns_index']++;
	    $arr_info_ex['video']['base']['nns_new_index']++;
	    $str_index_name = $arr_info_ex['video']['base']['nns_name'].'第'.$arr_info_ex['index']['base']['nns_index'].'集';
	    $str_xml = '';
	    //packages信息, 其中Asset_ID取值为栏目title的Asset_ID
	    $str_xml.= '<Metadata>';
	    $str_xml.=     '<AMS Verb="'.$str_Action.'" Asset_Class="packages" Asset_ID="'.$task_log_id.'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_summary'].'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	    $str_xml.=     '<App_Data Value="CableLabsVOD 1.1" Name="Metadata_Spec_Version" App="MOD"/>';
	    $str_xml.= '</Metadata>';
	    //栏目资产
	    $str_xml.= '<Asset>';
	    //栏目title信息, 即栏目元数据信息
	    $str_xml.=     '<Metadata>';
	    $str_xml.=         '<AMS Verb="'.$str_Action.'" Asset_Class="title" Asset_ID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_summary'].'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	    $str_xml.=         '<App_Data Value="program" Name="Show_Type" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="0" Name="Issue_Number" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_all_index'].'" Name="Volumn_Count" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_new_index'].'" Name="New_Count" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" Name="Original_Asset_ID" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="1" Name="Original_System_ID" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="1" Name="License_Type" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Culture" Name="Genre" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Proper_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Parallel_Proper_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Subordinate_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Alternative_ Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Title_Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="11" Name="Version_Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Key_Words" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_summary'].'" Name="Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Impressive_Dialogue" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Impressive_Plot" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Hot_Comments" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Sentence_Review" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Behind_Scenes" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="6.8" Name="IMDB_Rating" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="G" Name="MPAA_Rating" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_category.'" Name="Category" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Y" Name="Season_Premiere" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="N" Name="Season_Finale" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="00:00:00" Name="Preview_Period" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="5" Name="Asset_Recommend_Class" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="0" Name="Chapter" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_year.'" Name="Year" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="0.0" Name="Suggested_Price" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Adult" Name="Audience" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="2056-01-01 20:00:00" Name="Subscriber_View_Limit" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Region" App="MOD"/>';
	    $str_xml.=     '</Metadata>';
	    //栏目资产下的asset(language信息)
	    $str_xml.=     '<Asset>';
	    //language信息
        $language_name = $this->my_sub_str($arr_info_ex['video']['base']['nns_name'],30);
	    $str_xml.=          '<Metadata>';
	    $str_xml.=              '<AMS Verb="'.$str_Action.'" Asset_Class="language" Asset_ID="'.$str_language_id.'" Asset_Name="'.$language_name.'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$language_name.'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	    $str_xml.=              '<App_Data Value="zh" Name="Type" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="CH" Name="Description" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="zh_CN" Name="Tag" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="99" Name="Regin" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="南京一中" Name="Producers" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="" Name="Actors" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="" Name="Director" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="'.$language_name.'" Name="Title" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="'.$language_name.'" Name="Title_Brief" App="MOD"/>';
	    $str_xml.=          '</Metadata>';
	    $str_xml.=     '</Asset>';
	    //栏目资产下的asset(poster信息)
	    if (is_array($arr_img) && !empty($arr_img))
	    {
	        foreach ($arr_img as $img_key=>$img_val)
	        {
	            $Content_Check_Sum = md5_file($img_val);
	            $Content_Check_Sum = $Content_Check_Sum === false ? md5($img_val) : $Content_Check_Sum;
	
	            $size = @getimagesize($img_val);
	            $Horizontal_Pixels = (isset($size[0]) && (int)$size[0] >0 && is_array($size) && !empty($size)) ? (int)$size[0] : 320;
	            $Vertical_Pixels = (isset($size[1]) && (int)$size[1] >0 && is_array($size) && !empty($size)) ? (int)$size[1] : 240;
	            $str_xml.=     '<Asset>';
	            $str_xml.=         '<Metadata>';
	            $str_xml.=             '<AMS Verb="'.$str_Action.'" Asset_Class="poster" Asset_ID="'.$img_key.'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'海报" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_name'].'海报" Version_Major="1" Version_Minor="0" Product="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'海报" Name="Description" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$img_key.'" Name="Original_Asset_ID" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="1" Name="Original_System_ID" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="1" Name="Screen_Format" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="1" Name="Usage" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="Y" Name="Encryption" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="52856" Name="Content_File_Size" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$Horizontal_Pixels.'*'.$Vertical_Pixels.'" Name="Image_Aspect_Ratio" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="SD" Name="VideoType" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="RGB" Name="Color_Type" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="jpg" Name="Image_Encoding_Profile" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$Horizontal_Pixels.'" Name="Horizontal_Pixels" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$Vertical_Pixels.'" Name="Vertical_Pixels" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$Content_Check_Sum.'" Name="Content_Check_Sum" App="MOD"/>';
	            $str_xml.=         '</Metadata>';
	            $str_xml.=         '<Content Value="'.$img_val.'"/>';
	            $str_xml.=     '</Asset>';
	        }
	    }
	    $str_xml.= '</Asset>';
	    return $str_xml;
	}
	
	
	private function make_index_xml($task_log_id,$arr_info_ex)
	{
	    $task_log_id = $this->make_other_guid($arr_info_ex['video']['base']['nns_integer_id'],'video',0,'PA');
	    $arr_info_ex['video']['base']['nns_summary'] = $this->my_sub_str($arr_info_ex['video']['base']['nns_summary'],100);
	    $str_category = isset($this->arr_cp['nns_name']) ? $this->arr_cp['nns_name'] : '未知';
	    $str_date = date("Y-m-d");
	    $str_year = date("Y");
	    $str_Action = ($arr_info_ex['index']['base']['nns_deleted'] !=1) ? '' : 'DELETE';
	    $arr_img_in = $arr_img = null;
	    $str_img_ftp = (isset($this->arr_sp_config['img_ftp']) && strlen($this->arr_sp_config['img_ftp'])>0) ? trim($this->arr_sp_config['img_ftp'],'/').'/' : '';
	    $arr_img_video = array('nns_image_v','nns_image_h','nns_image_s');
	    $arr_img_index = array('nns_image');
	    foreach ($arr_img_video as $key=>$val)
	    {
	        if(isset($arr_info_ex['video']['base'][$val]) && strlen($arr_info_ex['video']['base'][$val])>0)
	        {
	            $arr_info_ex['video']['base'][$val] = trim($arr_info_ex['video']['base'][$val],'/');
	            $md5_img = $this->make_other_guid($arr_info_ex['video']['base']['nns_integer_id'],'video',$key);
	            $arr_img[$md5_img] = rtrim($str_img_ftp,'/').'/'.trim($arr_info_ex['video']['base'][$val],'/');
	        }
	    }
	    foreach ($arr_img_index as $key=>$val)
	    {
	        if(isset($arr_info_ex['index']['base'][$val]) && strlen($arr_info_ex['index']['base'][$val])>0)
	        {
	            $arr_info_ex['index']['base'][$val] = trim($arr_info_ex['index']['base'][$val],'/');
	            $md5_img = $this->make_other_guid($arr_info_ex['index']['base']['nns_integer_id'],'index',$key);
	            $arr_img[$md5_img] = rtrim($str_img_ftp,'/').'/'.trim($arr_info_ex['index']['base'][$val],'/');
	        }
	    }
	    $str_language_id = $this->make_other_guid($arr_info_ex['video']['base']['nns_integer_id'],'video',0,'LA');
	    $str_language_id_index = $this->make_other_guid($arr_info_ex['index']['base']['nns_integer_id'],'index',0,'LA');
	    $arr_info_ex['index']['base']['nns_index']++;
	    $arr_info_ex['video']['base']['nns_new_index']++;
	    $str_index_name = $arr_info_ex['video']['base']['nns_name'].'第'.$arr_info_ex['index']['base']['nns_index'].'集';
	    $str_xml = '';
	    //packages信息, 其中Asset_ID取值为栏目title的Asset_ID
	    $str_xml.= '<Metadata>';
	    $str_xml.=     '<AMS Verb="" Asset_Class="packages" Asset_ID="'.$task_log_id.'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_summary'].'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	    $str_xml.=     '<App_Data Value="CableLabsVOD 1.1" Name="Metadata_Spec_Version" App="MOD"/>';
	    $str_xml.= '</Metadata>';
        //栏目资产
	    $str_xml.= '<Asset>';
        //栏目title信息, 即栏目元数据信息
	    $str_xml.=     '<Metadata>';
	    $str_xml.=         '<AMS Verb="" Asset_Class="title" Asset_ID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_summary'].'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	    $str_xml.=         '<App_Data Value="series" Name="Show_Type" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="0" Name="Issue_Number" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_all_index'].'" Name="Volumn_Count" App="MOD"/>';
        $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_new_index'].'" Name="New_Count" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" Name="Original_Asset_ID" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="1" Name="Original_System_ID" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="1" Name="License_Type" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Culture" Name="Genre" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Proper_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Parallel_Proper_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Subordinate_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Alternative_ Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Title_Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="11" Name="Version_Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Key_Words" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_summary'].'" Name="Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Impressive_Dialogue" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Impressive_Plot" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Hot_Comments" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Sentence_Review" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Behind_Scenes" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="6.8" Name="IMDB_Rating" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="G" Name="MPAA_Rating" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_category.'" Name="Category" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Y" Name="Season_Premiere" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="N" Name="Season_Finale" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="00:00:00" Name="Preview_Period" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="5" Name="Asset_Recommend_Class" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="0" Name="Chapter" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_year.'" Name="Year" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="0.0" Name="Suggested_Price" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Adult" Name="Audience" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="2056-01-01 20:00:00" Name="Subscriber_View_Limit" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Region" App="MOD"/>';
	    $str_xml.=     '</Metadata>';
	    //栏目资产下的asset(language信息)
        $str_xml.=     '<Asset>';
        //language信息
        $language_name = $this->my_sub_str($arr_info_ex['video']['base']['nns_name'],30);
        $str_xml.=          '<Metadata>';
        $str_xml.=              '<AMS Verb="" Asset_Class="language" Asset_ID="'.$str_language_id.'" Asset_Name="'.$language_name.'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$language_name.'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
        $str_xml.=              '<App_Data Value="zh" Name="Type" App="MOD"/>';
        $str_xml.=              '<App_Data Value="CH" Name="Description" App="MOD"/>';
        $str_xml.=              '<App_Data Value="zh_CN" Name="Tag" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="99" Name="Regin" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="南京一中" Name="Producers" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="" Name="Actors" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="" Name="Director" App="MOD"/>';
        $str_xml.=              '<App_Data Value="'.$language_name.'" Name="Title" App="MOD"/>';
        $str_xml.=              '<App_Data Value="'.$language_name.'" Name="Title_Brief" App="MOD"/>';
        $str_xml.=          '</Metadata>';
        $str_xml.=     '</Asset>';
	    //栏目资产下的asset(poster信息)
        if (is_array($arr_img) && !empty($arr_img))
        {
            foreach ($arr_img as $img_key=>$img_val)
            {
                $Content_Check_Sum = md5_file($img_val);
                $Content_Check_Sum = $Content_Check_Sum === false ? md5($img_val) : $Content_Check_Sum;
                
                $size = @getimagesize($img_val);
                $Horizontal_Pixels = (isset($size[0]) && (int)$size[0] >0 && is_array($size) && !empty($size)) ? (int)$size[0] : 320;
                $Vertical_Pixels = (isset($size[1]) && (int)$size[1] >0 && is_array($size) && !empty($size)) ? (int)$size[1] : 240;
                $str_xml.=     '<Asset>';
                $str_xml.=         '<Metadata>';
                $str_xml.=             '<AMS Verb="" Asset_Class="poster" Asset_ID="'.$img_key.'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'海报" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_name'].'海报" Version_Major="1" Version_Minor="0" Product="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'海报" Name="Description" App="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$img_key.'" Name="Original_Asset_ID" App="MOD"/>';
                $str_xml.=             '<App_Data Value="1" Name="Original_System_ID" App="MOD"/>';
                $str_xml.=             '<App_Data Value="1" Name="Screen_Format" App="MOD"/>';
                $str_xml.=             '<App_Data Value="1" Name="Usage" App="MOD"/>';
                $str_xml.=             '<App_Data Value="Y" Name="Encryption" App="MOD"/>';
                $str_xml.=             '<App_Data Value="52856" Name="Content_File_Size" App="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$Horizontal_Pixels.'*'.$Vertical_Pixels.'" Name="Image_Aspect_Ratio" App="MOD"/>';
                $str_xml.=             '<App_Data Value="SD" Name="VideoType" App="MOD"/>';
                $str_xml.=             '<App_Data Value="RGB" Name="Color_Type" App="MOD"/>';
                $str_xml.=             '<App_Data Value="jpg" Name="Image_Encoding_Profile" App="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$Horizontal_Pixels.'" Name="Horizontal_Pixels" App="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$Vertical_Pixels.'" Name="Vertical_Pixels" App="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$Content_Check_Sum.'" Name="Content_Check_Sum" App="MOD"/>';
                $str_xml.=         '</Metadata>';
                $str_xml.=         '<Content Value="'.$img_val.'"/>';
                $str_xml.=     '</Asset>';
            }
        }
	    $str_xml.= '</Asset>';
	    
	    //<!-- 栏目第1集资产 -->
	    $str_xml.= '<Asset>';
	    //<!-- 栏目第1集title信息, 即栏目第1集的元数据信息 -->
	    $str_xml.=     '<Metadata>';
	    $str_xml.=         '<AMS Verb="'.$str_Action.'" Asset_Class="title" Asset_ID="'.$arr_info_ex['index']['base']['nns_cdn_index_guid'].'" Asset_Name="'.$str_index_name.'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$str_index_name.'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	    $str_xml.=         '<App_Data Value="series" Name="Show_Type" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="1" Name="Issue_Number" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['index']['base']['nns_cdn_index_guid'].'" Name="Original_Asset_ID" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="1" Name="Original_System_ID" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['index']['base']['nns_index'].'" Name="Chapter"/>';
	    $str_xml.=         '<App_Data Value="1" Name="License_Type" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_index_name.'" Name="Proper_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_index_name.'" Name="Parallel_Proper_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_index_name.'" Name="Subordinate_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_index_name.'" Name="Alternative_ Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_index_name.'" Name="Title_Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="11" Name="Version_Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Key_Words" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['index']['base']['nns_summary'].'" Name="Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Impressive_Dialogue" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Impressive_Plot" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Hot_Comments" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Sentence_Review" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Behind_Scenes" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="6.8" Name="IMDB_Rating" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="G" Name="MPAA_Rating" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_category.'" Name="Category" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Y" Name="Season_Premiere" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="N" Name="Season_Finale" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="00:00:00" Name="Preview_Period" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="5" Name="Asset_Recommend_Class" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_year.'" Name="Year" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="0.0" Name="Suggested_Price" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Adult" Name="Audience" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="2056-01-01 20:00:00" Name="Subscriber_View_Limit" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Region" App="MOD"/>';
	    $str_xml.=     '</Metadata>';

	    if (is_array($arr_img_in) && !empty($arr_img_in))
	    {
	        foreach ($arr_img_in as $img_key=>$img_val)
	        {
	            $Content_Check_Sum = md5_file($img_val);
	            $Content_Check_Sum = $Content_Check_Sum === false ? md5($img_val) : $Content_Check_Sum;
	            
	            $size = @getimagesize($img_val);
	            $Horizontal_Pixels = (isset($size[0]) && (int)$size[0] >0 && is_array($size) && !empty($size)) ? (int)$size[0] : 320;
	            $Vertical_Pixels = (isset($size[1]) && (int)$size[1] >0 && is_array($size) && !empty($size)) ? (int)$size[1] : 240;
                $str_xml.=     '<Asset>';
                $str_xml.=         '<Metadata>';
                $str_xml.=             '<AMS Verb="'.$str_Action.'" Asset_Class="poster" Asset_ID="'.$img_key.'" Asset_Name="'.$str_index_name.'海报" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$str_index_name.'海报" Version_Major="1" Version_Minor="0" Product="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$str_index_name.'海报" Name="Description" App="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$img_key.'" Name="Original_Asset_ID" App="MOD"/>';
                $str_xml.=             '<App_Data Value="1" Name="Original_System_ID" App="MOD"/>';
                $str_xml.=             '<App_Data Value="1" Name="Screen_Format" App="MOD"/>';
                $str_xml.=             '<App_Data Value="1" Name="Usage" App="MOD"/>';
                $str_xml.=             '<App_Data Value="Y" Name="Encryption" App="MOD"/>';
                $str_xml.=             '<App_Data Value="52856" Name="Content_File_Size" App="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$Horizontal_Pixels.'*'.$Vertical_Pixels.'" Name="Image_Aspect_Ratio" App="MOD"/>';
                $str_xml.=             '<App_Data Value="SD" Name="VideoType" App="MOD"/>';
                $str_xml.=             '<App_Data Value="RGB" Name="Color_Type" App="MOD"/>';
                $str_xml.=             '<App_Data Value="jpg" Name="Image_Encoding_Profile" App="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$Horizontal_Pixels.'" Name="Horizontal_Pixels" App="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$Vertical_Pixels.'" Name="Vertical_Pixels" App="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$Content_Check_Sum.'" Name="Content_Check_Sum" App="MOD"/>';
                $str_xml.=         '</Metadata>';
                $str_xml.=         '<Content Value="'.$img_val.'"/>';
                $str_xml.=     '</Asset>';
	        }
	    }
	    if(isset($arr_info_ex['index']['ex']['seekpoint']) && is_array($arr_info_ex['index']['ex']['seekpoint']) && !empty($arr_info_ex['index']['ex']['seekpoint']))
	    {
	        foreach ($arr_info_ex['index']['ex']['seekpoint'] as $seek_key=>$seek_value)
	        {
	            // <!-- 栏目第1集资产下的asset(SeekPoint信息) -->
	            $str_xml.=     '<Asset>';
	            // <!-- SeekPoint信息 -->
	            $str_xml.=         '<Metadata>';
	            $str_xml.=             '<AMS Verb="'.$str_Action.'" Asset_Class="SeekPoint" Asset_ID="'.$seek_key.'" Asset_Name="'.$str_index_name.'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$str_index_name.'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	            //<!-- DATE:2017-08-22 打点开始时间 例：00:00:24 必填 （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="SeekName" Value="'.$seek_value['nns_name'].'"/>';
	            //<!-- DATE:2017-08-22 打点开始时间 例：00:00:24 必填 （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="BeginTime" Value="hh:mi:ss"/>';
	            //<!-- DATE:2017-08-22 打点结束时间 例：00:00:48  非必填（FragmentType 为 剧头、片尾 必填） （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="EndTime" Value="hh:mi:ss"/>';
	            //<!-- DATE:2017-08-22 打点片段类型 （head 剧头  medium 片中 end 片尾 interact 互动） 必填  （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="FragmentType" Value="head"/>';
	            //<!-- DATE:2017-08-22 展示类型（Web 打开web  Text 文本展示） 必填 （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="OperateType" Value="Web"/>';
	            //<!-- DATE:2017-08-22 网页链接地址  当OperateType 为 web时 必填  （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="ServiceUrl" Value="head"/>';
	            //<!-- DATE:2017-08-22 打点开始时间 例：00:00:24 非必填 （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="SeekDesc" Value="看点信息描述"/>';
	            //<!-- DATE:2017-08-22 MD5 文件校验 必填 （视达科） -->
	            $str_xml.=             '<App_Data Value="29808A45AAED7C4635CB57EFB40EC64E" Name="Content_Check_Sum" App="MOD"/>';
	            $str_xml.=             '<App_Data App="MOD" Name="SeekPicUrl" Value="ftp://username:pswd@ip:port/path/filename1.jpg"/>';
	            $str_xml.=         '</Metadata>';
	            $str_xml.=     '</Asset>';
	        }
	    }
        // <!-- 栏目第1集资产下的asset(language信息) -->
	    $str_xml.=     '<Asset>';
        //<!-- language信息 -->
        $language_name = $this->my_sub_str($str_index_name,30);
	    $str_xml.=         '<Metadata>';
	    $str_xml.=             '<AMS Verb="'.$str_Action.'" Asset_Class="language" Asset_ID="'.$str_language_id_index.'" Asset_Name="'.$language_name.'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$language_name.'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	    $str_xml.=             '<App_Data Value="0" Name="Type" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="CH" Name="Description" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="zh_CN" Name="Tag" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="99" Name="Regin" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="南京一中" Name="Producers" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Actors" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Director" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$language_name.'" Name="Title" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$language_name.'" Name="Title_Brief" App="MOD"/>';
	    $str_xml.=         '</Metadata>';
	    $str_xml.=     '</Asset>';
	    $str_xml.= '</Asset>';
	    return $str_xml;
	}
	
	
	/**
	 * 生成图片的GUID
	 */
	private function make_other_guid($str_integer_id,$video_type='',$index=0,$str_type='PO')
	{
	    $str_header = (strlen($this->str_header) < 8) ? str_pad($this->str_header, 8, "0", STR_PAD_RIGHT) : substr($this->str_header, 0,8);
	    $str_type = strlen($str_type) == 2 ? $str_type : 'PO';
	    $index = strlen($index) == 1 ? $index : 0;
	    switch ($video_type)
	    {
	        case 'video':
	            $video_type = 'PT';
	            break;
	        case 'index':
	            $video_type = 'TI';
	            break;
	        case 'media':
	            $video_type = 'MO';
	            break;
	        case 'live':
	            $video_type = 'CH';
	            break;
	        case 'live_index':
	            $video_type = 'LI';
	            break;
	        case 'live_media':
	            $video_type = 'PC';
	            break;
	        case 'playbill':
	            $video_type = 'SD';
	            break;
	        case 'file':
	            $video_type = 'FI';
	            break;
	        default:
	            $video_type = 'PU';
	    }
	    return $str_header.$str_type .$video_type. str_pad($str_integer_id, 7, "0", STR_PAD_LEFT).$index;
	}
	
	private function make_media_xml($task_log_id,$arr_info_ex)
	{
	    $task_log_id = $this->make_other_guid($arr_info_ex['video']['base']['nns_integer_id'],'video',0,'PA');
	    $arr_info_ex['video']['base']['nns_summary'] = $this->my_sub_str($arr_info_ex['video']['base']['nns_summary'],100);
	    $str_category = isset($this->arr_cp['nns_name']) ? $this->arr_cp['nns_name'] : '未知';
	    $str_date = date("Y-m-d");
	    $str_year = date("Y");
	    $str_Action = ($arr_info_ex['media']['base']['nns_deleted'] !=1) ? '' : 'DELETE';
	    $arr_img_in = $arr_img = null;
	    $str_img_ftp = (isset($this->arr_sp_config['img_ftp']) && strlen($this->arr_sp_config['img_ftp'])>0) ? trim($this->arr_sp_config['img_ftp'],'/').'/' : '';
	    $arr_img_video = array('nns_image_v','nns_image_h','nns_image_s');
	    $arr_img_index = array('nns_image');
	    foreach ($arr_img_video as $key=>$val)
	    {
	        if(isset($arr_info_ex['video']['base'][$val]) && strlen($arr_info_ex['video']['base'][$val])>0)
	        {
	            $arr_info_ex['video']['base'][$val] = trim($arr_info_ex['video']['base'][$val],'/');
	            $md5_img = $this->make_other_guid($arr_info_ex['video']['base']['nns_integer_id'],'video',$key);
	            $arr_img[$md5_img] = rtrim($str_img_ftp,'/').'/'.trim($arr_info_ex['video']['base'][$val],'/');
	        }
	    }
	    foreach ($arr_img_index as $key=>$val)
	    {
	        if(isset($arr_info_ex['index']['base'][$val]) && strlen($arr_info_ex['index']['base'][$val])>0)
	        {
	            $arr_info_ex['index']['base'][$val] = trim($arr_info_ex['index']['base'][$val],'/');
	            $md5_img = $this->make_other_guid($arr_info_ex['index']['base']['nns_integer_id'],'index',$key);
	            $arr_img[$md5_img] = rtrim($str_img_ftp,'/').'/'.trim($arr_info_ex['index']['base'][$val],'/');
	        }
	    }
	    $str_language_id = $this->make_other_guid($arr_info_ex['video']['base']['nns_integer_id'],'video',0,'LA');
	    $str_language_id_index = $this->make_other_guid($arr_info_ex['index']['base']['nns_integer_id'],'index',0,'LA');
	    $arr_info_ex['index']['base']['nns_index']++;
	    $arr_info_ex['video']['base']['nns_new_index']++;
	    $str_index_name = $arr_info_ex['video']['base']['nns_name'].'第'.$arr_info_ex['index']['base']['nns_index'].'集';
	    $str_xml = '';
	    //packages信息, 其中Asset_ID取值为栏目title的Asset_ID
	    $str_xml.= '<Metadata>';
	    $str_xml.=     '<AMS Verb="" Asset_Class="packages" Asset_ID="'.$task_log_id.'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_summary'].'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	    $str_xml.=     '<App_Data Value="CableLabsVOD 1.1" Name="Metadata_Spec_Version" App="MOD"/>';
	    $str_xml.= '</Metadata>';
	    //栏目资产
	    $str_xml.= '<Asset>';
	    //栏目title信息, 即栏目元数据信息
	    $str_xml.=     '<Metadata>';
	    $str_xml.=         '<AMS Verb="" Asset_Class="title" Asset_ID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_summary'].'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	    $str_xml.=         '<App_Data Value="series" Name="Show_Type" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="0" Name="Issue_Number" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_all_index'].'" Name="Volumn_Count" App="MOD"/>';
        $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_new_index'].'" Name="New_Count" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" Name="Original_Asset_ID" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="1" Name="Original_System_ID" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="1" Name="License_Type" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Culture" Name="Genre" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Proper_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Parallel_Proper_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Subordinate_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Alternative_ Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Title_Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="11" Name="Version_Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Key_Words" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_summary'].'" Name="Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Impressive_Dialogue" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Impressive_Plot" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Hot_Comments" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Sentence_Review" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Behind_Scenes" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="6.8" Name="IMDB_Rating" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="G" Name="MPAA_Rating" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_category.'" Name="Category" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Y" Name="Season_Premiere" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="N" Name="Season_Finale" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="00:00:00" Name="Preview_Period" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="5" Name="Asset_Recommend_Class" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="0" Name="Chapter" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_year.'" Name="Year" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="0.0" Name="Suggested_Price" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Adult" Name="Audience" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="2056-01-01 20:00:00" Name="Subscriber_View_Limit" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Region" App="MOD"/>';
	    $str_xml.=     '</Metadata>';
	    //栏目资产下的asset(language信息)
	    $str_xml.=     '<Asset>';
	    //language信息
        $language_name = $this->my_sub_str($arr_info_ex['video']['base']['nns_name'],30);
	    $str_xml.=          '<Metadata>';
	    $str_xml.=              '<AMS Verb="" Asset_Class="language" Asset_ID="'.$str_language_id.'" Asset_Name="'.$language_name.'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$language_name.'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	    $str_xml.=              '<App_Data Value="zh" Name="Type" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="CH" Name="Description" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="zh_CN" Name="Tag" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="99" Name="Regin" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="南京一中" Name="Producers" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="" Name="Actors" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="" Name="Director" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="'.$language_name.'" Name="Title" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="'.$language_name.'" Name="Title_Brief" App="MOD"/>';
	    $str_xml.=          '</Metadata>';
	    $str_xml.=     '</Asset>';
	    //栏目资产下的asset(poster信息)
	    if (is_array($arr_img) && !empty($arr_img))
	    {
	        foreach ($arr_img as $img_key=>$img_val)
	        {
	            $Content_Check_Sum = md5_file($img_val);
	            $Content_Check_Sum = $Content_Check_Sum === false ? md5($img_val) : $Content_Check_Sum;
	            
	            $size = @getimagesize($img_val);
	            $Horizontal_Pixels = (isset($size[0]) && (int)$size[0] >0 && is_array($size) && !empty($size)) ? (int)$size[0] : 320;
	            $Vertical_Pixels = (isset($size[1]) && (int)$size[1] >0 && is_array($size) && !empty($size)) ? (int)$size[1] : 240;
	            $str_xml.=     '<Asset>';
	            $str_xml.=         '<Metadata>';
	            $str_xml.=             '<AMS Verb="" Asset_Class="poster" Asset_ID="'.$img_key.'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'海报" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_name'].'海报" Version_Major="1" Version_Minor="0" Product="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'海报" Name="Description" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$img_key.'" Name="Original_Asset_ID" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="1" Name="Original_System_ID" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="1" Name="Screen_Format" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="1" Name="Usage" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="Y" Name="Encryption" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="52856" Name="Content_File_Size" App="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$Horizontal_Pixels.'*'.$Vertical_Pixels.'" Name="Image_Aspect_Ratio" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="SD" Name="VideoType" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="RGB" Name="Color_Type" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="jpg" Name="Image_Encoding_Profile" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$Horizontal_Pixels.'" Name="Horizontal_Pixels" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$Vertical_Pixels.'" Name="Vertical_Pixels" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$Content_Check_Sum.'" Name="Content_Check_Sum" App="MOD"/>';
	            $str_xml.=         '</Metadata>';
	            $str_xml.=         '<Content Value="'.$img_val.'"/>';
	            $str_xml.=     '</Asset>';
	        }
	    }
	    $str_xml.= '</Asset>';
	     
	    //<!-- 栏目第1集资产 -->
	    $str_xml.= '<Asset>';
	    //<!-- 栏目第1集title信息, 即栏目第1集的元数据信息 -->
	    $str_xml.=     '<Metadata>';
	    $str_xml.=         '<AMS Verb="" Asset_Class="title" Asset_ID="'.$arr_info_ex['index']['base']['nns_cdn_index_guid'].'" Asset_Name="'.$str_index_name.'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$str_index_name.'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	    $str_xml.=         '<App_Data Value="series" Name="Show_Type" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="1" Name="Issue_Number" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['index']['base']['nns_cdn_index_guid'].'" Name="Original_Asset_ID" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="1" Name="Original_System_ID" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['index']['base']['nns_index'].'" Name="Chapter"/>';
	    $str_xml.=         '<App_Data Value="1" Name="License_Type" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_index_name.'" Name="Proper_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_index_name.'" Name="Parallel_Proper_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_index_name.'" Name="Subordinate_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_index_name.'" Name="Alternative_ Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_index_name.'" Name="Title_Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="11" Name="Version_Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Key_Words" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['index']['base']['nns_summary'].'" Name="Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Impressive_Dialogue" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Impressive_Plot" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Hot_Comments" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Sentence_Review" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Behind_Scenes" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="6.8" Name="IMDB_Rating" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="G" Name="MPAA_Rating" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_category.'" Name="Category" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Y" Name="Season_Premiere" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="N" Name="Season_Finale" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="00:00:00" Name="Preview_Period" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="5" Name="Asset_Recommend_Class" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_year.'" Name="Year" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="0.0" Name="Suggested_Price" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Adult" Name="Audience" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="2056-01-01 20:00:00" Name="Subscriber_View_Limit" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Region" App="MOD"/>';
	    $str_xml.=     '</Metadata>';
	
	    if (is_array($arr_img_in) && !empty($arr_img_in))
	    {
	        foreach ($arr_img_in as $img_key=>$img_val)
	        {
	            $Content_Check_Sum = md5_file($img_val);
	            $Content_Check_Sum = $Content_Check_Sum === false ? md5($img_val) : $Content_Check_Sum;
	            
	            $size = @getimagesize($img_val);
	            $Horizontal_Pixels = (isset($size[0]) && (int)$size[0] >0 && is_array($size) && !empty($size)) ? (int)$size[0] : 320;
	            $Vertical_Pixels = (isset($size[1]) && (int)$size[1] >0 && is_array($size) && !empty($size)) ? (int)$size[1] : 240;
	            $str_xml.=     '<Asset>';
	            $str_xml.=         '<Metadata>';
	            $str_xml.=             '<AMS Verb="" Asset_Class="poster" Asset_ID="'.$img_key.'" Asset_Name="'.$str_index_name.'海报" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$str_index_name.'海报" Version_Major="1" Version_Minor="0" Product="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$str_index_name.'海报" Name="Description" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$img_key.'" Name="Original_Asset_ID" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="1" Name="Original_System_ID" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="1" Name="Screen_Format" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="1" Name="Usage" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="Y" Name="Encryption" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="52856" Name="Content_File_Size" App="MOD"/>';
                $str_xml.=             '<App_Data Value="'.$Horizontal_Pixels.'*'.$Vertical_Pixels.'" Name="Image_Aspect_Ratio" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="SD" Name="VideoType" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="RGB" Name="Color_Type" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="jpg" Name="Image_Encoding_Profile" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$Horizontal_Pixels.'" Name="Horizontal_Pixels" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$Vertical_Pixels.'" Name="Vertical_Pixels" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$Content_Check_Sum.'" Name="Content_Check_Sum" App="MOD"/>';
	            $str_xml.=         '</Metadata>';
	            $str_xml.=         '<Content Value="'.$img_val.'"/>';
	            $str_xml.=     '</Asset>';
	        }
	    }
	    if(isset($arr_info_ex['index']['ex']['seekpoint']) && is_array($arr_info_ex['index']['ex']['seekpoint']) && !empty($arr_info_ex['index']['ex']['seekpoint']))
	    {
	        foreach ($arr_info_ex['index']['ex']['seekpoint'] as $seek_key=>$seek_value)
	        {
	            // <!-- 栏目第1集资产下的asset(SeekPoint信息) -->
	            $str_xml.=     '<Asset>';
	            // <!-- SeekPoint信息 -->
	            $str_xml.=         '<Metadata>';
	            $str_xml.=             '<AMS Verb="" Asset_Class="SeekPoint" Asset_ID="JSB12035SP0001271070" Asset_Name="'.$str_index_name.'打点" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$str_index_name.'打点" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	            //<!-- DATE:2017-08-22 打点开始时间 例：00:00:24 必填 （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="SeekName" Value="打点测试"/>';
	            //<!-- DATE:2017-08-22 打点开始时间 例：00:00:24 必填 （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="BeginTime" Value="00:00:24"/>';
	            //<!-- DATE:2017-08-22 打点结束时间 例：00:00:48  非必填（FragmentType 为 剧头、片尾 必填） （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="EndTime" Value="00:00:26"/>';
	            //<!-- DATE:2017-08-22 打点片段类型 （head 剧头  medium 片中 end 片尾 interact 互动） 必填  （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="FragmentType" Value="head"/>';
	            //<!-- DATE:2017-08-22 展示类型（Web 打开web  Text 文本展示） 必填 （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="OperateType" Value="Web"/>';
	            //<!-- DATE:2017-08-22 网页链接地址  当OperateType 为 web时 必填  （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="ServiceUrl" Value="head"/>';
	            //<!-- DATE:2017-08-22 打点开始时间 例：00:00:24 非必填 （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="SeekDesc" Value="看点信息描述"/>';
	            //<!-- DATE:2017-08-22 MD5 文件校验 必填 （视达科） -->
	            $str_xml.=             '<App_Data Value="29808A45AAED7C4635CB57EFB40EC64E" Name="Content_Check_Sum" App="MOD"/>';
	            $str_xml.=             '<App_Data App="MOD" Name="SeekPicUrl" Value="ftp://image_jscn:image_jscn@172.26.2.17:21/prev/KsImg/cp_fuse_sitvott_neweast/20180817/5b7693d63494589c436760b013093c36.jpg"/>';
	            $str_xml.=         '</Metadata>';
	            $str_xml.=     '</Asset>';
	        }
	    }
	    // <!-- 栏目第1集资产下的asset(language信息) -->
	    $str_xml.=     '<Asset>';
	    //<!-- language信息 -->
        $language_name = $this->my_sub_str($str_index_name,30);
	    $str_xml.=         '<Metadata>';
	    $str_xml.=             '<AMS Verb="" Asset_Class="language" Asset_ID="'.$str_language_id_index.'" Asset_Name="'.$language_name.'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$language_name.'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	    $str_xml.=             '<App_Data Value="0" Name="Type" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="CH" Name="Description" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="zh_CN" Name="Tag" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="99" Name="Regin" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="南京一中" Name="Producers" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Actors" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Director" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$language_name.'" Name="Title" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$language_name.'" Name="Title_Brief" App="MOD"/>';
	    $str_xml.=         '</Metadata>';
	    $str_xml.=     '</Asset>';
	    
// 	    $Content_Check_Sum = md5_file($arr_info_ex['media']['base']['nns_url']);
	    $Content_Check_Sum = false;
        $Content_Check_Sum = $Content_Check_Sum === false ? md5($arr_info_ex['media']['base']['nns_url']) : $Content_Check_Sum;
	    // <!-- 电视剧第1集资产下的asset(movie信息)-高清 -->
	    $str_xml.=     '<Asset>';
	    // <!-- movie信息 -->
	    $str_xml.=         '<Metadata>';
	    $str_xml.=             '<AMS Verb="'.$str_Action.'" Asset_Class="movie" Asset_ID="'.$arr_info_ex['media']['base']['nns_cdn_media_guid'].'" Asset_Name="'.$str_index_name.'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$str_index_name.'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$arr_info_ex['media']['base']['nns_cdn_media_guid'].'" Name="Original_Asset_ID" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="1" Name="Original_System_ID" App="MOD"/>';
	    //1 IP | 2 IpQAM
	    $str_xml.=             '<App_Data Value="1" Name="Service_Type" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="1" Name="Screen_Format" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="Y" Name="HD_Content" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="3D_Content" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="2" Name="Video_Bit_Rate" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$arr_info_ex['media']['base']['FrameHeight'].'" Name="Frame_Height" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$arr_info_ex['media']['base']['FrameWidth'].'" Name="Frame_Width" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="N" Name="Encryption" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$arr_info_ex['media']['base']['RunTime'].'" Name="Run_Time" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$arr_info_ex['media']['base']['BeatRate'].'" Name="Audio_Bit_Rate" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$Content_Check_Sum.'" Name="Content_Check_Sum" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="1" Name="Security_Classification" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Remarks" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$arr_info_ex['media']['base']['FileSize'].'" Name="Content_File_Size" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="1" Name="Encryption_Reason" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Audio_Language" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="zh_CN" Name="Subtitle_Language" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Audio_Bit_Depth" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Audio_Sampling_Frequency" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="0" Name="Audio_Type" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Audio_Coding_Format" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Video_Sampling_Type" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Video_Coding_Format" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="PAL" Name="System" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Aspect_Ratio" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="5" Name="Video_Quality" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="1" Name="Audio_Channel_Format" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="1" Name="Color" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Starting_Point" App="MOD"/>';
	    // <!-- DATE:2017-08-22 片源清晰度（HD高清、STD标清、low流畅、SD超高清）（视达科） -->
	    $str_xml.=             '<App_Data Value="HD" Name="MediaMode" App="MOD"/>';
	    // <!-- DATE:2017-08-22 片源格式（3D  4K  dubi normal）（视达科） -->
	    $str_xml.=             '<App_Data Value="4K" Name="FileType" App="MOD"/>';
	    $str_xml.=         '</Metadata>';
	    $str_xml.=         '<Content Value="'.$arr_info_ex['media']['base']['nns_url'].'"/>';
	    if(isset($arr_info_ex['index']['ex']['drawingframe']) && is_array($arr_info_ex['index']['ex']['drawingframe']) && !empty($arr_info_ex['index']['ex']['drawingframe']))
	    {
	        foreach ($arr_info_ex['media']['ex']['drawingframe'] as $drawingframe_key=>$drawingframe_value)
	        {
        	    // <!-- movie信息的asset(DrawingFrame信息)  需要新增Asset_Class的枚举值DrawingFrame-->
        	    $str_xml.=         '<Asset>';
        	    // <!-- DrawingFrame信息(字段可根据实际需要再修订) -->
        	    $str_xml.=             '<Metadata>';
        	    $str_xml.=                 '<AMS Verb="'.$str_Action.'" Asset_Class="DrawingFrame" Asset_ID="JSB12035DF0001271070" Asset_Name="'.$str_index_name.'抽帧" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$str_index_name.'抽帧" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
        	    //<!-- DATE:2017-08-22 抽帧时间 例 10 单位秒 代表 10秒一帧 （压缩后的文件名称为 000010.jpg 代表第10秒的抽帧图片  002000 代表第20分钟的抽帧图片，压缩后文件里面不在含有任何文件夹） 必填 （视达科） -->
        	    $str_xml.=                 '<App_Data App="MOD" Name="DrawingFrameInt" Value="10"/>';
        	    // <!-- DATE:2017-08-22 MD5 文件校验 必填 （视达科） -->
        	    $str_xml.=                 '<App_Data Value="29808A45AAED7C4635CB57EFB40EC64F" Name="Content_Check_Sum" App="MOD"/>';
        	    //<!-- DATE:2017-08-22 抽帧压缩文件格式（jpg、gif、png）（视达科） -->
        	    $str_xml.=                 '<App_Data App="MOD" Name="DrawingFrameFileType" Value="jpg"/>';
        	    // <!-- 抽帧描述信息，非必填 -->
        	    $str_xml.=                 '<App_Data App="MOD" Name="FrameDesc" Value="抽帧信息描述1"/>';
        	    $str_xml.=             '</Metadata>';
        	    // <!-- 抽帧文件绝对路径，必填 -->
        	    $str_xml.=             '<Content Value="ftp://image_jscn:image_jscn@172.26.2.17:21/cvideo/test/media/nmsx/2017-09-19/0890b60dab86e1494e525f0a84fb9b77/320X320.zip"/>';
        	    $str_xml.=         '</Asset>';
	        }
	    }
	    $str_xml.=     '</Asset>'; 
	    $str_xml.= '</Asset>';
	    return $str_xml;
	}
	
	
	private function make_movie_media_xml($task_log_id,$arr_info_ex)
	{
	    $task_log_id = $this->make_other_guid($arr_info_ex['video']['base']['nns_integer_id'],'video',0,'PA');
	    $arr_info_ex['video']['base']['nns_summary'] = $this->my_sub_str($arr_info_ex['video']['base']['nns_summary'],100);
	    $str_category = isset($this->arr_cp['nns_name']) ? $this->arr_cp['nns_name'] : '未知';
	    $str_date = date("Y-m-d");
	    $str_year = date("Y");
	    $str_Action = ($arr_info_ex['media']['base']['nns_deleted'] !=1) ? '' : 'DELETE';
	    $arr_img_in = $arr_img = null;
	    $str_img_ftp = (isset($this->arr_sp_config['img_ftp']) && strlen($this->arr_sp_config['img_ftp'])>0) ? trim($this->arr_sp_config['img_ftp'],'/').'/' : '';
	    $arr_img_video = array('nns_image_v','nns_image_h','nns_image_s');
	    $arr_img_index = array('nns_image');
	    foreach ($arr_img_video as $key=>$val)
	    {
	        if(isset($arr_info_ex['video']['base'][$val]) && strlen($arr_info_ex['video']['base'][$val])>0)
	        {
	            $arr_info_ex['video']['base'][$val] = trim($arr_info_ex['video']['base'][$val],'/');
	            $md5_img = $this->make_other_guid($arr_info_ex['video']['base']['nns_integer_id'],'video',$key);
	            $arr_img[$md5_img] = rtrim($str_img_ftp,'/').'/'.trim($arr_info_ex['video']['base'][$val],'/');
	        }
	    }
	    foreach ($arr_img_index as $key=>$val)
	    {
	        if(isset($arr_info_ex['index']['base'][$val]) && strlen($arr_info_ex['index']['base'][$val])>0)
	        {
	            $arr_info_ex['index']['base'][$val] = trim($arr_info_ex['index']['base'][$val],'/');
	            $md5_img = $this->make_other_guid($arr_info_ex['index']['base']['nns_integer_id'],'index',$key);
	            $arr_img[$md5_img] = rtrim($str_img_ftp,'/').'/'.trim($arr_info_ex['index']['base'][$val],'/');
	        }
	    }
	    $str_language_id = $this->make_other_guid($arr_info_ex['video']['base']['nns_integer_id'],'video',0,'LA');
	    $str_language_id_index = $this->make_other_guid($arr_info_ex['index']['base']['nns_integer_id'],'index',0,'LA');
	    $arr_info_ex['index']['base']['nns_index']++;
	    $arr_info_ex['video']['base']['nns_new_index']++;
	    $str_index_name = $arr_info_ex['video']['base']['nns_name'].'第'.$arr_info_ex['index']['base']['nns_index'].'集';
	    $str_xml = '';
	    //packages信息, 其中Asset_ID取值为栏目title的Asset_ID
	    $str_xml.= '<Metadata>';
	    $str_xml.=     '<AMS Verb="" Asset_Class="packages" Asset_ID="'.$task_log_id.'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_summary'].'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	    $str_xml.=     '<App_Data Value="CableLabsVOD 1.1" Name="Metadata_Spec_Version" App="MOD"/>';
	    $str_xml.= '</Metadata>';
	    //栏目资产
	    $str_xml.= '<Asset>';
	    //栏目title信息, 即栏目元数据信息
	    $str_xml.=     '<Metadata>';
	    $str_xml.=         '<AMS Verb="" Asset_Class="title" Asset_ID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_summary'].'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	    $str_xml.=         '<App_Data Value="program" Name="Show_Type" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="0" Name="Issue_Number" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_all_index'].'" Name="Volumn_Count" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_new_index'].'" Name="New_Count" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" Name="Original_Asset_ID" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="1" Name="Original_System_ID" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="1" Name="License_Type" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Culture" Name="Genre" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Proper_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Parallel_Proper_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Subordinate_Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Alternative_ Title" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'" Name="Title_Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="11" Name="Version_Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Key_Words" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['video']['base']['nns_summary'].'" Name="Description" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Impressive_Dialogue" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Impressive_Plot" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Hot_Comments" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Sentence_Review" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Behind_Scenes" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="6.8" Name="IMDB_Rating" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="G" Name="MPAA_Rating" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_category.'" Name="Category" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Y" Name="Season_Premiere" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="N" Name="Season_Finale" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="00:00:00" Name="Preview_Period" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="5" Name="Asset_Recommend_Class" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="0" Name="Chapter" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="'.$str_year.'" Name="Year" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="0.0" Name="Suggested_Price" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="Adult" Name="Audience" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="2056-01-01 20:00:00" Name="Subscriber_View_Limit" App="MOD"/>';
	    $str_xml.=         '<App_Data Value="" Name="Region" App="MOD"/>';
	    $str_xml.=     '</Metadata>';
	    //栏目资产下的asset(language信息)
	    $str_xml.=     '<Asset>';
	    //language信息
        $language_name = $this->my_sub_str($arr_info_ex['video']['base']['nns_name'],30);
	    $str_xml.=          '<Metadata>';
	    $str_xml.=              '<AMS Verb="" Asset_Class="language" Asset_ID="'.$str_language_id.'" Asset_Name="'.$language_name.'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$language_name.'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	    $str_xml.=              '<App_Data Value="zh" Name="Type" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="CH" Name="Description" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="zh_CN" Name="Tag" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="99" Name="Regin" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="南京一中" Name="Producers" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Actors" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Director" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="'.$language_name.'" Name="Title" App="MOD"/>';
	    $str_xml.=              '<App_Data Value="'.$language_name.'" Name="Title_Brief" App="MOD"/>';
	    $str_xml.=          '</Metadata>';
	    $str_xml.=     '</Asset>';
	    //栏目资产下的asset(poster信息)
	    if (is_array($arr_img) && !empty($arr_img))
	    {
	        foreach ($arr_img as $img_key=>$img_val)
	        {
	            $Content_Check_Sum = md5_file($img_val);
	            $Content_Check_Sum = $Content_Check_Sum === false ? md5($img_val) : $Content_Check_Sum;
	             
	            $size = @getimagesize($img_val);
	            $Horizontal_Pixels = (isset($size[0]) && (int)$size[0] >0 && is_array($size) && !empty($size)) ? (int)$size[0] : 320;
	            $Vertical_Pixels = (isset($size[1]) && (int)$size[1] >0 && is_array($size) && !empty($size)) ? (int)$size[1] : 240;
	            $str_xml.=     '<Asset>';
	            $str_xml.=         '<Metadata>';
	            $str_xml.=             '<AMS Verb="" Asset_Class="poster" Asset_ID="'.$img_key.'" Asset_Name="'.$arr_info_ex['video']['base']['nns_name'].'海报" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$arr_info_ex['video']['base']['nns_name'].'海报" Version_Major="1" Version_Minor="0" Product="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$arr_info_ex['video']['base']['nns_name'].'海报" Name="Description" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$img_key.'" Name="Original_Asset_ID" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="1" Name="Original_System_ID" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="1" Name="Screen_Format" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="1" Name="Usage" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="Y" Name="Encryption" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="52856" Name="Content_File_Size" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$Horizontal_Pixels.'*'.$Vertical_Pixels.'" Name="Image_Aspect_Ratio" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="SD" Name="VideoType" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="RGB" Name="Color_Type" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="jpg" Name="Image_Encoding_Profile" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$Horizontal_Pixels.'" Name="Horizontal_Pixels" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$Vertical_Pixels.'" Name="Vertical_Pixels" App="MOD"/>';
	            $str_xml.=             '<App_Data Value="'.$Content_Check_Sum.'" Name="Content_Check_Sum" App="MOD"/>';
	            $str_xml.=         '</Metadata>';
	            $str_xml.=         '<Content Value="'.$img_val.'"/>';
	            $str_xml.=     '</Asset>';
	        }
	    }
// 	    $str_xml.= '</Asset>';
	
	    //<!-- 栏目第1集资产 -->
// 	    $str_xml.= '<Asset>';
	    //<!-- 栏目第1集title信息, 即栏目第1集的元数据信息 -->
// 	    $str_xml.=     '<Metadata>';
// 	    $str_xml.=         '<AMS Verb="" Asset_Class="title" Asset_ID="'.$arr_info_ex['index']['base']['nns_cdn_index_guid'].'" Asset_Name="'.$str_index_name.'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$str_index_name.'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="column" Name="Show_Type" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="1" Name="Issue_Number" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['index']['base']['nns_cdn_index_guid'].'" Name="Original_Asset_ID" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="1" Name="Original_System_ID" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="1" Name="License_Type" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="'.$str_index_name.'" Name="Proper_Title" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="'.$str_index_name.'" Name="Parallel_Proper_Title" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="'.$str_index_name.'" Name="Subordinate_Title" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="'.$str_index_name.'" Name="Alternative_ Title" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="'.$str_index_name.'" Name="Title_Description" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="2" Name="Version_Description" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="" Name="Key_Words" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="'.$arr_info_ex['index']['base']['nns_summary'].'" Name="Description" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="" Name="Impressive_Dialogue" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="" Name="Impressive_Plot" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="" Name="Hot_Comments" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="" Name="Sentence_Review" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="" Name="Behind_Scenes" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="6.8" Name="IMDB_Rating" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="G" Name="MPAA_Rating" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="'.$str_category.'" Name="Category" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="Y" Name="Season_Premiere" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="N" Name="Season_Finale" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="00:00:00" Name="Preview_Period" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="5" Name="Asset_Recommend_Class" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="0" Name="Chapter" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="'.$str_year.'" Name="Year" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="0.0" Name="Suggested_Price" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="Adult" Name="Audience" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="2056-01-01 20:00:00" Name="Subscriber_View_Limit" App="MOD"/>';
// 	    $str_xml.=         '<App_Data Value="" Name="Region" App="MOD"/>';
// 	    $str_xml.=     '</Metadata>';
	
// 	    if (is_array($arr_img_in) && !empty($arr_img_in))
// 	    {
// 	        foreach ($arr_img_in as $img_key=>$img_val)
// 	        {
// 	            $Content_Check_Sum = md5_file($img_val);
// 	            $Content_Check_Sum = $Content_Check_Sum === false ? md5($img_val) : $Content_Check_Sum;
	             
// 	            $size = @getimagesize($img_val);
// 	            $Horizontal_Pixels = (isset($size[0]) && (int)$size[0] >0 && is_array($size) && !empty($size)) ? (int)$size[0] : 320;
// 	            $Vertical_Pixels = (isset($size[1]) && (int)$size[1] >0 && is_array($size) && !empty($size)) ? (int)$size[1] : 240;
// 	            $str_xml.=     '<Asset>';
// 	            $str_xml.=         '<Metadata>';
// 	            $str_xml.=             '<AMS Verb="" Asset_Class="poster" Asset_ID="'.$img_key.'" Asset_Name="'.$str_index_name.'海报" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$str_index_name.'海报" Version_Major="1" Version_Minor="0" Product="MOD"/>';
// 	            $str_xml.=             '<App_Data Value="'.$str_index_name.'海报" Name="Description" App="MOD"/>';
// 	            $str_xml.=             '<App_Data Value="'.$img_key.'" Name="Original_Asset_ID" App="MOD"/>';
// 	            $str_xml.=             '<App_Data Value="1" Name="Original_System_ID" App="MOD"/>';
// 	            $str_xml.=             '<App_Data Value="1" Name="Screen_Format" App="MOD"/>';
// 	            $str_xml.=             '<App_Data Value="1" Name="Usage" App="MOD"/>';
// 	            $str_xml.=             '<App_Data Value="Y" Name="Encryption" App="MOD"/>';
// 	            $str_xml.=             '<App_Data Value="52856" Name="Content_File_Size" App="MOD"/>';
// 	            $str_xml.=             '<App_Data Value="'.$Horizontal_Pixels.'*'.$Vertical_Pixels.'" Name="Image_Aspect_Ratio" App="MOD"/>';
// 	            $str_xml.=             '<App_Data Value="SD" Name="VideoType" App="MOD"/>';
// 	            $str_xml.=             '<App_Data Value="RGB" Name="Color_Type" App="MOD"/>';
// 	            $str_xml.=             '<App_Data Value="jpg" Name="Image_Encoding_Profile" App="MOD"/>';
// 	            $str_xml.=             '<App_Data Value="'.$Horizontal_Pixels.'" Name="Horizontal_Pixels" App="MOD"/>';
// 	            $str_xml.=             '<App_Data Value="'.$Vertical_Pixels.'" Name="Vertical_Pixels" App="MOD"/>';
// 	            $str_xml.=             '<App_Data Value="'.$Content_Check_Sum.'" Name="Content_Check_Sum" App="MOD"/>';
// 	            $str_xml.=         '</Metadata>';
// 	            $str_xml.=         '<Content Value="'.$img_val.'"/>';
// 	            $str_xml.=     '</Asset>';
// 	        }
// 	    }
	    if(isset($arr_info_ex['index']['ex']['seekpoint']) && is_array($arr_info_ex['index']['ex']['seekpoint']) && !empty($arr_info_ex['index']['ex']['seekpoint']))
	    {
	        foreach ($arr_info_ex['index']['ex']['seekpoint'] as $seek_key=>$seek_value)
	        {
	            // <!-- 栏目第1集资产下的asset(SeekPoint信息) -->
	            $str_xml.=     '<Asset>';
	            // <!-- SeekPoint信息 -->
	            $str_xml.=         '<Metadata>';
	            $str_xml.=             '<AMS Verb="" Asset_Class="SeekPoint" Asset_ID="'.$seek_key.'" Asset_Name="'.$str_index_name.'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$str_index_name.'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	            //<!-- DATE:2017-08-22 打点开始时间 例：00:00:24 必填 （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="SeekName" Value="'.$seek_value['nns_name'].'"/>';
	            //<!-- DATE:2017-08-22 打点开始时间 例：00:00:24 必填 （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="BeginTime" Value="hh:mi:ss"/>';
	            //<!-- DATE:2017-08-22 打点结束时间 例：00:00:48  非必填（FragmentType 为 剧头、片尾 必填） （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="EndTime" Value="hh:mi:ss"/>';
	            //<!-- DATE:2017-08-22 打点片段类型 （head 剧头  medium 片中 end 片尾 interact 互动） 必填  （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="FragmentType" Value="head"/>';
	            //<!-- DATE:2017-08-22 展示类型（Web 打开web  Text 文本展示） 必填 （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="OperateType" Value="Web"/>';
	            //<!-- DATE:2017-08-22 网页链接地址  当OperateType 为 web时 必填  （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="ServiceUrl" Value="head"/>';
	            //<!-- DATE:2017-08-22 打点开始时间 例：00:00:24 非必填 （视达科） -->
	            $str_xml.=             '<App_Data App="MOD" Name="SeekDesc" Value="看点信息描述"/>';
	            //<!-- DATE:2017-08-22 MD5 文件校验 必填 （视达科） -->
	            $str_xml.=             '<App_Data Value="29808A45AAED7C4635CB57EFB40EC64E" Name="Content_Check_Sum" App="MOD"/>';
	            $str_xml.=             '<App_Data App="MOD" Name="SeekPicUrl" Value="ftp://username:pswd@ip:port/path/filename1.jpg"/>';
	            $str_xml.=         '</Metadata>';
	            $str_xml.=     '</Asset>';
	        }
	    }
	    // <!-- 栏目第1集资产下的asset(language信息) -->
// 	    $str_xml.=     '<Asset>';
// 	    //<!-- language信息 -->
// 	    $str_xml.=         '<Metadata>';
// 	    $str_xml.=             '<AMS Verb="" Asset_Class="language" Asset_ID="'.$str_language_id_index.'" Asset_Name="'.$str_index_name.'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$str_index_name.'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
// 	    $str_xml.=             '<App_Data Value="0" Name="Type" App="MOD"/>';
// 	    $str_xml.=             '<App_Data Value="CH" Name="Description" App="MOD"/>';
// 	    $str_xml.=             '<App_Data Value="zh_CN" Name="Tag" App="MOD"/>';
// 	    $str_xml.=             '<App_Data Value="'.$str_index_name.'" Name="Title" App="MOD"/>';
// 	    $str_xml.=             '<App_Data Value="'.$str_index_name.'" Name="Title_Brief" App="MOD"/>';
// 	    $str_xml.=         '</Metadata>';
// 	    $str_xml.=     '</Asset>';
	     
	    // 	    $Content_Check_Sum = md5_file($arr_info_ex['media']['base']['nns_url']);
	    $Content_Check_Sum = false;
	    $Content_Check_Sum = $Content_Check_Sum === false ? md5($arr_info_ex['media']['base']['nns_url']) : $Content_Check_Sum;
	    // <!-- 电视剧第1集资产下的asset(movie信息)-高清 -->
	    $str_xml.=     '<Asset>';
	    // <!-- movie信息 -->
	    $str_xml.=         '<Metadata>';
	    $str_xml.=             '<AMS Verb="'.$str_Action.'" Asset_Class="movie" Asset_ID="'.$arr_info_ex['media']['base']['nns_cdn_media_guid'].'" Asset_Name="'.$str_index_name.'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$str_index_name.'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$arr_info_ex['media']['base']['nns_cdn_media_guid'].'" Name="Original_Asset_ID" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="1" Name="Original_System_ID" App="MOD"/>';
	    //1 IP | 2 IpQAM
	    $str_xml.=             '<App_Data Value="1" Name="Service_Type" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="1" Name="Screen_Format" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="Y" Name="HD_Content" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="3D_Content" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="2" Name="Video_Bit_Rate" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$arr_info_ex['media']['base']['FrameHeight'].'" Name="Frame_Height" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$arr_info_ex['media']['base']['FrameWidth'].'" Name="Frame_Width" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="N" Name="Encryption" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$arr_info_ex['media']['base']['RunTime'].'" Name="Run_Time" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$arr_info_ex['media']['base']['BeatRate'].'" Name="Audio_Bit_Rate" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$Content_Check_Sum.'" Name="Content_Check_Sum" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="1" Name="Security_Classification" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Remarks" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="'.$arr_info_ex['media']['base']['FileSize'].'" Name="Content_File_Size" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="1" Name="Encryption_Reason" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Audio_Language" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="zh_CN" Name="Subtitle_Language" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Audio_Bit_Depth" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Audio_Sampling_Frequency" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="0" Name="Audio_Type" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Audio_Coding_Format" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Video_Sampling_Type" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Video_Coding_Format" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="PAL" Name="System" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Aspect_Ratio" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="5" Name="Video_Quality" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="1" Name="Audio_Channel_Format" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="1" Name="Color" App="MOD"/>';
	    $str_xml.=             '<App_Data Value="" Name="Starting_Point" App="MOD"/>';
	    // <!-- DATE:2017-08-22 片源清晰度（HD高清、STD标清、low流畅、SD超高清）（视达科） -->
	    $str_xml.=             '<App_Data Value="HD" Name="MediaMode" App="MOD"/>';
	    // <!-- DATE:2017-08-22 片源格式（3D  4K  dubi normal）（视达科） -->
	    $str_xml.=             '<App_Data Value="4K" Name="FileType" App="MOD"/>';
	    $str_xml.=         '</Metadata>';
	    $str_xml.=         '<Content Value="'.$arr_info_ex['media']['base']['nns_url'].'"/>';
	    if(isset($arr_info_ex['index']['ex']['seekpoint']) && is_array($arr_info_ex['index']['ex']['seekpoint']) && !empty($arr_info_ex['index']['ex']['seekpoint']))
	    {
	        foreach ($arr_info_ex['media']['ex']['drawingframe'] as $drawingframe_key=>$drawingframe_value)
	        {
	            // <!-- movie信息的asset(DrawingFrame信息)  需要新增Asset_Class的枚举值DrawingFrame-->
	            $str_xml.=         '<Asset>';
	            // <!-- DrawingFrame信息(字段可根据实际需要再修订) -->
	            $str_xml.=             '<Metadata>';
	            $str_xml.=                 '<AMS Verb="'.$str_Action.'" Asset_Class="DrawingFrame" Asset_ID="'.$drawingframe_key.'" Asset_Name="'.$str_index_name.'" Provider_ID="'.$this->str_cmsid.'"  Provider="'.$this->str_cmsid.'" Creation_Date="'.$str_date.'" Description="'.$str_index_name.'" Version_Major="1"  Version_Minor="0" Product="MOD"/>';
	            //<!-- DATE:2017-08-22 抽帧时间 例 10 单位秒 代表 10秒一帧 （压缩后的文件名称为 000010.jpg 代表第10秒的抽帧图片  002000 代表第20分钟的抽帧图片，压缩后文件里面不在含有任何文件夹） 必填 （视达科） -->
	            $str_xml.=                 '<App_Data App="MOD" Name="DrawingFrameInt" Value="10"/>';
	            // <!-- DATE:2017-08-22 MD5 文件校验 必填 （视达科） -->
	            $str_xml.=                 '<App_Data Value="29808A45AAED7C4635CB57EFB40EC64E" Name="Content_Check_Sum" App="MOD"/>';
	            //<!-- DATE:2017-08-22 抽帧压缩文件格式（jpg、gif、png）（视达科） -->
	            $str_xml.=                 '<App_Data App="MOD" Name="DrawingFrameFileType" Value="jpg"/>';
	            // <!-- 抽帧描述信息，非必填 -->
	            $str_xml.=                 '<App_Data App="MOD" Name="FrameDesc" Value="抽帧信息描述1"/>';
	            $str_xml.=             '</Metadata>';
	            // <!-- 抽帧文件绝对路径，必填 -->
	            $str_xml.=             '<Content Value="ftp://username:pswd@ip:port/path/filename.zip"/>';
	            $str_xml.=         '</Asset>';
	        }
	    }
	    $str_xml.=     '</Asset>';
	    $str_xml.= '</Asset>';
	    return $str_xml;
	}
	
	
	public function do_video()
	{
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_ref_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $nns_ref_id = $this->arr_params['bk_c2_task_info']['nns_ref_id'];
	    $result_video = nl_vod::get_video_info_by_id($this->obj_dc, $nns_ref_id);
	    if($result_video['ret'] !=0)
	    {
	        return $result_video;
	    }
	    if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
	    {
	        nl_c2_task::edit($this->obj_dc, array('nns_status'=>'1','nns_action'=>'destroy','nns_epg_status'=>'97'), $this->arr_params['bk_c2_task_info']['nns_id']);
	        return $result_video;
	    }
	    $result_video = $result_video['data_info'][0];
		$this->init_cp_info($result_video['nns_cp_id']);
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_video['nns_id'],$result_video,null,'cdn',$this->str_header);
	    
	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        return $result_video_import_info;
	    }
	    $result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
	    
	    $arr_info_ex=array(
	        'video'=>array(
	            'base'=>$result_video,
	        ),
	    );
	    $task_log_id= $this->get_guid_rand('VI',$result_video['nns_integer_id']);
	    $xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
	    $xml_str .= '<ADI>';
	    if($arr_info_ex['video']['base']['nns_all_index'] <=1 && !in_array($arr_info_ex['video']['base']['nns_cp_id'], array('fuse_JSCN_xes','fuse_JSCN_ych','fuse_SiTVOTT_NEWEAST')))
	    {
	        $xml_str .=   $this->make_video_movie_xml($task_log_id, $arr_info_ex);
	    }
	    else
	    {
	        $xml_str .=   $this->make_video_xml($task_log_id,$arr_info_ex);
	    }
//         $xml_str .=   $this->make_video_xml($task_log_id,$arr_info_ex);
	    $xml_str .= '</ADI>';
// 	    ob_clean();
// 	    header("Content-Type:text/xml;charset=utf-8");
// 	    echo $xml_str;die;
	    $action = ($result_video['nns_deleted'] !=1) ? ($result_video['nns_modify_time'] > $result_video['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
	    $c2_info = array(
	        'nns_id'=>$task_log_id,
	        'nns_task_type'=>'Series',
	        'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	        'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
	        'nns_action'=>	$action,
	        'nns_content'=>	$xml_str,
	    );
		if($arr_info_ex['video']['base']['nns_all_index'] >200)
		{
		    return $this->_make_return_data(1,"分集>200");
		}
	    $result = $this->execute_self_c2($c2_info);
	    return $result;
	}
	
	
	
	public function do_index()
	{
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_ref_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $nns_ref_id = $this->arr_params['bk_c2_task_info']['nns_ref_id'];
	    
	    $result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $nns_ref_id);
	    if($result_index['ret'] !=0)
	    {
	        return $result_index;
	    }
	    if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
	    {
	        nl_c2_task::edit($this->obj_dc, array('nns_status'=>'1','nns_action'=>'destroy','nns_epg_status'=>'97'), $this->arr_params['bk_c2_task_info']['nns_id']);
	        return $result_index;
	    }
	    $result_index = $result_index['data_info'][0];
		$this->init_cp_info($result_index['nns_cp_id']);
	    $result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $result_index['nns_id'],$result_index,null,'cdn',$this->str_header);
	    
	    if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
	    {
	        return $result_index_import_info;
	    }
	    $result_index['nns_cdn_index_guid'] = $result_index_import_info['data_info'];
	    
	    $result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_index['nns_vod_id']);
	    if($result_video['ret'] !=0)
	    {
	        return $result_video;
	    }
	    if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
	    {
	        nl_c2_task::edit($this->obj_dc, array('nns_status'=>'1','nns_action'=>'destroy','nns_epg_status'=>'97'), $this->arr_params['bk_c2_task_info']['nns_id']);
	        return $result_video;
	    }
	    $result_video = $result_video['data_info'][0];
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_video['nns_id'],$result_video,null,'cdn',$this->str_header);
	     
	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        return $result_video_import_info;
	    }
	    $result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
	     
	    $arr_info_ex=array(
	        'video'=>array(
	            'base'=>$result_video,
	        ),
	        'index'=>array(
	            'base'=>$result_index,
	        ),
	    );
	    $task_log_id= $this->get_guid_rand('IN',$result_index['nns_integer_id']);
	    $xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
	    $xml_str .= '<ADI>';
	    if($arr_info_ex['video']['base']['nns_all_index'] <=1  && !in_array($arr_info_ex['video']['base']['nns_cp_id'], array('fuse_JSCN_xes','fuse_JSCN_ych','fuse_SiTVOTT_NEWEAST')))
	    {
	        nl_c2_task::edit($this->obj_dc, array('nns_status'=>0), $this->arr_params['bk_c2_task_info']['nns_id']);
	        return $this->_make_return_data(0,'OK');
	    }
	    else
	    {
	        $xml_str .=   $this->make_index_xml($task_log_id,$arr_info_ex);
	    }
// 	    $xml_str .=   $this->make_index_xml($task_log_id,$arr_info_ex);
	    $xml_str .= '</ADI>';
// 	    ob_clean();
// 	    header("Content-Type:text/xml;charset=utf-8");
// 	    echo $xml_str;die;
	    $action = ($result_video['nns_deleted'] !=1) ? ($result_video['nns_modify_time'] > $result_video['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
	    $c2_info = array(
	        'nns_id'=>$task_log_id,
	        'nns_task_type'=>'Program',
	        'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	        'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
	        'nns_action'=>	$action,
	        'nns_content'=>	$xml_str,
	    );
		if($arr_info_ex['video']['base']['nns_all_index'] >200)
		{
		    return $this->_make_return_data(1,"分集>200");
		}
	    $result = $this->execute_self_c2($c2_info);
	    return $result;
	}
	
	private function make_time($time)
	{
	    return date("H:i:s",$time);
	}
	
	/**
	 * 片源CDN
	 * @param unknown $movie_id
	 */
	public function do_media()
	{
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
		if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_ref_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $nns_ref_id = $this->arr_params['bk_c2_task_info']['nns_ref_id'];
		$result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $nns_ref_id);
		if($result_media['ret'] !=0)
		{
			return $result_media;
		}
		if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
		{
			nl_c2_task::edit($this->obj_dc, array('nns_status'=>'1','nns_action'=>'destroy','nns_epg_status'=>'97'), $this->arr_params['bk_c2_task_info']['nns_id']);
	        return $result_media;
		}
		$result_media = $result_media['data_info'];
		$this->init_cp_info($result_media['nns_cp_id']);
		$result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $result_media['nns_id'],$result_media,null,'cdn',$this->str_header);
	    if((int)$this->arr_sp_config['disabled_clip'] !== 2 && (int)$this->arr_sp_config['disabled_clip'] !== 3)
		{
		    $result_media['nns_url'] = ltrim((trim($this->arr_params['nns_file_path'])),'/');
		    if(isset($this->arr_sp_config['media_ftp']) && !empty($this->arr_sp_config['media_ftp']))
		    {
		        $this->arr_sp_config['media_ftp'] = rtrim((trim($this->arr_sp_config['media_ftp'])),'/');
		        if (stripos($result_media['nns_url'], 'http://') === FALSE && stripos($result_media['nns_url'], 'ftp://') === FALSE)
		        {
		            $result_media['nns_url'] = $this->arr_sp_config['media_ftp'].'/'.$result_media['nns_url'];
		        }
		    }
		}
		else
		{
		    $result_media['nns_url'] = ltrim((trim($result_media['nns_url'])),'/');
		    if(isset($this->arr_sp_config['media_ftp']) && !empty($this->arr_sp_config['media_ftp']))
		    {
		        $this->arr_sp_config['media_ftp'] = rtrim((trim($this->arr_sp_config['media_ftp'])),'/');
		        if (stripos($result_media['nns_url'], 'http://') === FALSE && stripos($result_media['nns_url'], 'ftp://') === FALSE)
		        {
		        	 $result_media['nns_url'] = $this->arr_sp_config['media_ftp'].'/'.$result_media['nns_url'];
		        }
		    }
		}
		
		if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
		{
			return $result_media_import_info;
		}
		$result_media['nns_cdn_media_guid'] = $result_media_import_info['data_info'];
		
		
		$media_info_guid = dirname(dirname(dirname(dirname(__FILE__)))).'/data/temp/mediainfo/temp';
		if (!is_dir($media_info_guid))
		{
		    mkdir($media_info_guid, 0777, true);
		}
		$media_info_guid.=np_guid_rand().'.xml';
		$result = m_mediainfo::exec_ftp($result_media['nns_url'],$media_info_guid);
		$arr_media_info = null;
		if($result['ret'] == '0' && strlen($result['data_info']) >0)
		{
		    $arr_media_info = simplexml_load_string($result['data_info']);
		    $arr_media_info = json_decode(json_encode($arr_media_info),true);
		}
		$Duration = isset($arr_media_info['File']['track'][0]['Duration'][0]) ? (int)$arr_media_info['File']['track'][0]['Duration'][0] : 45*60*60;
		$result_media['RunTime'] = $this->make_time($Duration);
		$result_media['BeatRate'] = isset($arr_media_info['File']['track'][0]['Overall_bit_rate'][0]) ? (int)$arr_media_info['File']['track'][0]['Overall_bit_rate'][0] : 2.3*1024*1024;
		$result_media['FileSize'] = isset($arr_media_info['File']['track'][0]['File_size'][0]) ? (int)$arr_media_info['File']['track'][0]['File_size'][0] : 384061252;
		$arr_nns_file_resolution = explode('*', $result_media['nns_file_resolution']);
		$result_media['FrameHeight'] = (isset($arr_media_info['File']['track'][1]['Sampled_Height']) && (int)$arr_media_info['File']['track'][1]['Sampled_Height']>0) ? (int)$arr_media_info['File']['track'][1]['Sampled_Height'] : 1080;
		$result_media['FrameWidth'] = (isset($arr_media_info['File']['track'][1]['Sampled_Width']) && (int)$arr_media_info['File']['track'][1]['Sampled_Width']>0) ? (int)$arr_media_info['File']['track'][1]['Sampled_Width'] : 1920;
		
		$media_formart = isset($arr_media_info['File']['track'][0]['Overall_bit_rate_mode'][0]) ? $arr_media_info['File']['track'][0]['Overall_bit_rate_mode'][0] : '';
		$media_formart = is_string($media_formart) ? strtoupper($media_formart) : '';
        if($media_formart != 'VBR')
        {
            $c2_info = array(
                'nns_id'=>np_guid_rand(),
                'nns_task_type'=>'Movie',
                'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
                'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
                'nns_action'=>	'UPDATE',
                'nns_content'=>	'',
                'nns_type' => 'std',
                'nns_org_id'=> $this->str_sp_id,
                'nns_desc' =>'片源非VBR',
                'nns_url'=>'',
                'nns_result' => '[1]',
                'nns_status' => '-1',
            );
	        nl_c2_log::add($this->obj_dc, $c2_info);
            nl_c2_task::edit($this->obj_dc, array('nns_status'=>'-1'), $this->arr_params['bk_c2_task_info']['nns_id']);
            return $this->_make_return_data(1,"片源非VBR");
        }
        if($result_media['BeatRate'] >= 3.5*1024*1024)
        {
            $c2_info = array(
                'nns_id'=>np_guid_rand(),
                'nns_task_type'=>'Movie',
                'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
                'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
                'nns_action'=>	'UPDATE',
                'nns_content'=>	'',
                'nns_type' => 'std',
                'nns_org_id'=> $this->str_sp_id,
                'nns_desc' =>'片源码率过大',
                'nns_url'=>'',
                'nns_result' => '[1]',
                'nns_status' => '-1',
            );
            nl_c2_log::add($this->obj_dc, $c2_info);
            nl_c2_task::edit($this->obj_dc, array('nns_status'=>'-1'), $this->arr_params['bk_c2_task_info']['nns_id']);
            return $this->_make_return_data(1,"片源码率过大");
        }
		$result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $result_media['nns_vod_index_id']);
		if($result_index['ret'] !=0)
		{
		    return $result_index;
		}
		if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
		{
	        nl_c2_task::edit($this->obj_dc, array('nns_status'=>'1','nns_action'=>'destroy','nns_epg_status'=>'97'), $this->arr_params['bk_c2_task_info']['nns_id']);
	        return $result_index;
		}
		$result_index = $result_index['data_info'][0];
		$result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $result_index['nns_id'],$result_index,null,'cdn',$this->str_header);
		
		if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
		{
		    return $result_index_import_info;
		}
		$result_index['nns_cdn_index_guid'] = $result_index_import_info['data_info'];
		
		$result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_media['nns_vod_id']);
		if($result_video['ret'] !=0)
		{
		    return $result_video;
		}
		if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
		{
	        nl_c2_task::edit($this->obj_dc, array('nns_status'=>'1','nns_action'=>'destroy','nns_epg_status'=>'97'), $this->arr_params['bk_c2_task_info']['nns_id']);
	        return $result_video;
		}
		$result_video = $result_video['data_info'][0];
		$result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_video['nns_id'],$result_video,null,'cdn',$this->str_header);
		
		if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
		{
		    return $result_video_import_info;
		}
		$result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
		
		$arr_info_ex=array(
		    'video'=>array(
		        'base'=>$result_video,
		    ),
		    'index'=>array(
		        'base'=>$result_index,
		    ),
		    'media'=>array(
		        'base'=>$result_media,
		    ),
		);
		
		$task_log_id= $this->get_guid_rand('MO',$result_media['nns_integer_id']);
		$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml_str .= '<ADI>';
// 	    $xml_str .=   $this->make_media_xml($task_log_id,$arr_info_ex);
	    if($arr_info_ex['video']['base']['nns_all_index'] <=1  && !in_array($arr_info_ex['video']['base']['nns_cp_id'], array('fuse_JSCN_xes','fuse_JSCN_ych','fuse_SiTVOTT_NEWEAST')))
	    {
	        $xml_str .=   $this->make_movie_media_xml($task_log_id,$arr_info_ex);
	    }
	    else
	    {
	        $xml_str .=   $this->make_media_xml($task_log_id,$arr_info_ex);
	    }
		$xml_str .= '</ADI>';
// 	    ob_clean();
// 	    header("Content-Type:text/xml;charset=utf-8");
// 	    echo $xml_str;die;
		$action = ($result_media['nns_deleted'] !=1) ? ($result_media['nns_modify_time'] > $result_media['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
		$c2_info = array(
		        'nns_id'=>$task_log_id,
				'nns_task_type'=>'Movie',
				'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	            'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
				'nns_action'=>	$action,
		        'nns_content'=>	$xml_str,
		);
		if($arr_info_ex['video']['base']['nns_all_index'] >200)
		{
		    return $this->_make_return_data(1,"分集>200");
		}
		$result = $this->execute_self_c2($c2_info);
		return $result;
	}

	/**
	 * 发布操作
	 */
	public function do_media_delivery_add()
	{
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_ref_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $movie_id = $this->arr_params['bk_c2_task_info']['nns_ref_id'];
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	    $result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $movie_id);
	    if($result_media['ret'] !=0)
	    {
	        return $result_media;
	    }
	    if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_media;
	    }
	    $result_media = $result_media['data_info'];
		$this->init_cp_info($result_media['nns_cp_id']);
	    $result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $movie_id,$result_media,null,'cdn',$this->str_header);
	    if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
	    {
	        return $result_media_import_info;
	    }

	    $result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $result_media['nns_vod_index_id']);
	    if($result_index['ret'] !=0)
	    {
	        return $result_index;
	    }
	    if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_index;
	    }
	    $result_index = $result_index['data_info'][0];
	    $result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $result_media['nns_vod_index_id'],$result_index,null,'cdn',$this->str_header);

	    if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
	    {
	        return $result_index_import_info;
	    }

	    $result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_media['nns_vod_id']);
	    if($result_video['ret'] !=0)
	    {
	        return $result_video;
	    }
	    if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_video;
	    }
	    $result_video = $result_video['data_info'][0];
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_media['nns_vod_id'],$result_video,null,'cdn',$this->str_header);

	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        return $result_video_import_info;
	    }

	    $time = microtime(true)*10000;
	    $task_log_id = $this->get_guid_rand('DE',$result_media['nns_integer_id']);
	    $xml_str = '<?xml version="1.0" encoding="utf-8"?>';
	    $xml_str.= '<message module="iCMS" version="1.0">';
	    $xml_str.=     '<header timestamp="'.$time.'" sequence="'.$task_log_id.'" component-id="iCMS_01" component-type="iCMS" action="REQUEST" command="DELIVERY_TASK_ADD"/>';
	    $xml_str.=     '<body>';
	    $xml_str.=         '<tasks>';
	    $xml_str.=             '<task content-id="'.$result_index_import_info['data_info'].'" subcontent-id="'.$result_media_import_info['data_info'].'" target-system-id="02" target-site-id="Nanjing" priority="1" begin-time="'.date("Y-m-d").'T'.date("H:i:s").'.0Z"/>';
	    $xml_str.=         '</tasks>';
	    $xml_str.=     '</body>';
	    $xml_str.= '</message>';

	    $action = ($result_media['nns_deleted'] !=1) ? 'DELIVERY_ADD' : 'DELIVERY_DEL';
	    $c2_info = array(
	        'nns_id'=>$task_log_id,
	        'nns_task_type'=>'Movie',
	        'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	        'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
	        'nns_action'=>	$action,
	        'nns_content'=>	$xml_str,
	    );
	    $result = $this->execute_self_c2($c2_info,false);
	    return $result;
	}
	
	
	
	/**
	 * 取消发布操作
	 */
	public function do_media_delivery_del()
	{
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_ref_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $movie_id = $this->arr_params['bk_c2_task_info']['nns_ref_id'];
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	    $result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $movie_id);
	    if($result_media['ret'] !=0)
	    {
	        return $result_media;
	    }
	    if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_media;
	    }
	    $result_media = $result_media['data_info'];
		$this->init_cp_info($result_media['nns_cp_id']);
	    $result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $movie_id,$result_media,null,'cdn',$this->str_header);
	    if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
	    {
	        return $result_media_import_info;
	    }
	     
	    $result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $result_media['nns_vod_index_id']);
	    if($result_index['ret'] !=0)
	    {
	        return $result_index;
	    }
	    if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_index;
	    }
	    $result_index = $result_index['data_info'][0];
	    $result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $result_media['nns_vod_index_id'],$result_index,null,'cdn',$this->str_header);
	     
	    if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
	    {
	        return $result_index_import_info;
	    }
	     
	    $result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_media['nns_vod_id']);
	    if($result_video['ret'] !=0)
	    {
	        return $result_video;
	    }
	    if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_video;
	    }
	    $result_video = $result_video['data_info'][0];
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_media['nns_vod_id'],$result_video,null,'cdn',$this->str_header);
	     
	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        return $result_video_import_info;
	    }
	     
	    $time = microtime(true)*10000;
	    $task_log_id = $this->get_guid_rand('DE',$result_media['nns_integer_id']);
	    $xml_str = '<?xml version="1.0" encoding="utf-8"?>';
	    $xml_str.= '<message module="iCMS" version="1.0">';
	    $xml_str.=     '<header timestamp="'.$time.'" sequence="'.$task_log_id.'" component-id="iCMS_01" component-type="iCMS" action="REQUEST" command="DELIVERY_TASK_DELETE"/>';
	    $xml_str.=     '<body>';
	    $xml_str.=         '<tasks>';
	    $xml_str.=             '<task content-id="'.$result_index_import_info['data_info'].'" subcontent-id="'.$result_media_import_info['data_info'].'" target-system-id="02" target-site-id="Nanjing" priority="1" begin-time="'.date("Y-m-d").'T'.date("H:i:s").'.0Z"/>';
	    $xml_str.=         '</tasks>';
	    $xml_str.=     '</body>';
	    $xml_str.= '</message>';
	     
	    $action = ($result_media['nns_deleted'] !=1) ? 'DELIVERY_ADD' : 'DELIVERY_DEL';
	    $c2_info = array(
	        'nns_id'=>$task_log_id,
	        'nns_task_type'=>'Movie',
	        'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	        'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
	        'nns_action'=>	$action,
	        'nns_content'=>	$xml_str,
	    );
	    $result = $this->execute_self_c2($c2_info,false);
	    return $result;
	}
	
	
	
	/**
	 * 上线操作
	 */
	public function do_media_online()
	{
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_ref_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $movie_id = $this->arr_params['bk_c2_task_info']['nns_ref_id'];
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	    $result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $movie_id);
	    if($result_media['ret'] !=0)
	    {
	        return $result_media;
	    }
	    if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_media;
	    }
	    $result_media = $result_media['data_info'];
		$this->init_cp_info($result_media['nns_cp_id']);
	    $result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $movie_id,$result_media,null,'cdn',$this->str_header);
	    if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
	    {
	        return $result_media_import_info;
	    }
	    
	    $result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $result_media['nns_vod_index_id']);
	    if($result_index['ret'] !=0)
	    {
	        return $result_index;
	    }
	    if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_index;
	    }
	    $result_index = $result_index['data_info'][0];
	    $result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $result_media['nns_vod_index_id'],$result_index,null,'cdn',$this->str_header);
	    
	    if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
	    {
	        return $result_index_import_info;
	    }
	    
	    $result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_media['nns_vod_id']);
	    if($result_video['ret'] !=0)
	    {
	        return $result_video;
	    }
	    if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_video;
	    }
	    $result_video = $result_video['data_info'][0];
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_media['nns_vod_id'],$result_video,null,'cdn',$this->str_header);
	    
	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        return $result_video_import_info;
	    }

        //>>构建上线请求的报文信息
        $time=$task_log_id=$str_target_system_id=$str_sp_id=$str_target_system_id="";
        //时间戳
        $time = $this->get_java_datetime();
        //消息id
        $task_log_id = $this->get_guid_rand('UN',$result_media['nns_integer_id']);
        //目标系统标识 指26-S(标清系统)或者26-H(高清系统)
        $str_target_system_id = $this->str_target_system_id;
        //思华方配置的spid
        $str_sp_id = $this->str_sihua_sp_id;
        $str_target_site_id = $this->str_target_site_id;
        $int_sequence = time();
        //发送的报文实体
/*	    $xml_str = '<?xml version="1.0" encoding="utf-8"?>';*/
//	    $xml_str.= '<message module="iCMS" version="1.0">';
//	    $xml_str.=     '<header timestamp="'.$time.'" sequence="'.$int_sequence.'" component-id="iCMS_01" component-type="iCMS" action="REQUEST" command="ONLINE_TASK_DONE"/>';
//	    $xml_str.=     '<body>';
//	    $xml_str.=         '<tasks>';
//	    $xml_str.=             '<task sp-id="'.$str_sp_id.'" content-id="'.$result_index_import_info['data_info'].'" subcontent-id="'.$result_media_import_info['data_info'].'" target-system-id="'.$str_target_system_id.'" target-site-id="'.$str_target_site_id.'" status="6"/>';
//        $xml_str.=         '</tasks>';
//        $xml_str.=     '</body>';
//	    $xml_str.= '</message>';

        $xml_str =<<<XML
<?xml version="1.0" encoding="utf-8"?>
<message module="iCMS" version="1.0"> 
  <header action="REQUEST" command="ONLINE_TASK_DONE" component-id="iCMS_01" component-type="iCMS" sequence="{$int_sequence}" timestamp="{$time}"/>  
  <body> 
    <tasks> 
      <task page-id="3" sp-id="{$str_sp_id}" status="6" content-id="{$result_index_import_info['data_info']}" subcontent-id="{$result_media_import_info['data_info']}" target-site-id="{$str_target_site_id}" target-system-id="{$str_target_system_id}"/> 
    </tasks> 
  </body> 
</message>
XML;
	    $action = ($result_media['nns_deleted'] !=1) ? 'ONLINE' : 'OFFLINE';
	    $c2_info = array(
	        'nns_id'=>$task_log_id,
	        'nns_task_type'=>'Movie',
	        'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	        'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
	        'nns_action'=>	$action,
	        'nns_content'=>	$xml_str,
	    );
	    $result = $this->execute_self_c2($c2_info,false);
	    return $result;
	}
	
	
	
	
	
	/**
	 * 上线操作
	 */
	public function do_media_unline()
	{
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_ref_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $movie_id = $this->arr_params['bk_c2_task_info']['nns_ref_id'];
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	    $result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $movie_id);
	    if($result_media['ret'] !=0)
	    {
	        return $result_media;
	    }
	    if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_media;
	    }
	    $result_media = $result_media['data_info'];
		$this->init_cp_info($result_media['nns_cp_id']);
	    $result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $movie_id,$result_media,null,'cdn',$this->str_header);
	    if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
	    {
	        return $result_media_import_info;
	    }
	     
	    $result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $result_media['nns_vod_index_id']);
	    if($result_index['ret'] !=0)
	    {
	        return $result_index;
	    }
	    if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_index;
	    }
	    $result_index = $result_index['data_info'][0];
	    $result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $result_media['nns_vod_index_id'],$result_index,null,'cdn',$this->str_header);
	     
	    if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
	    {
	        return $result_index_import_info;
	    }
	     
	    $result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_media['nns_vod_id']);
	    if($result_video['ret'] !=0)
	    {
	        return $result_video;
	    }
	    if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
	        return $result_video;
	    }
	    $result_video = $result_video['data_info'][0];
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_media['nns_vod_id'],$result_video,null,'cdn',$this->str_header);
	     
	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        return $result_video_import_info;
	    }

	    //>>构建上线请求的报文信息
        $time=$task_log_id=$str_target_system_id=$str_target_site_id=$str_sp_id='';
        //时间戳
	    $time = $this->get_java_datetime();
	    //消息id
	    $task_log_id = $this->get_guid_rand('UN',$result_media['nns_integer_id']);
	    //目标系统标识 指26-S(标清系统)或者26-H(高清系统)
	    $str_target_system_id = $this->str_target_system_id;
	    $str_target_site_id = $this->str_target_site_id;
        $str_sp_id = $this->str_sihua_sp_id;
        $int_sequence = time();
	    //报文实体内容xml
/*	    $xml_str = '<?xml version="1.0" encoding="utf-8"?>';*/
//	    $xml_str.= '<message module="iCMS" version="1.0">';
//	    $xml_str.=     '<header timestamp="'.$time.'" sequence="'.$int_sequence.'" component-id="CLPS_01" component-type="CLPS" action="REQUEST" command="CONTENT_OFFLINE"/>';
//	    $xml_str.=     '<body>';
//	    $xml_str.=         '<contents>';
//	    $xml_str.=             '<content sp-id="'.$str_sp_id.'" content-id="'.$result_index_import_info['data_info'].'" subcontent-id="'.$result_media_import_info['data_info'].'" target-system-id="'.$str_target_system_id.'" target-site-id="'.$str_target_site_id.'" status="6"/>';
//	    $xml_str.=         '</contents>';
//	    $xml_str.=     '</body>';
//	    $xml_str.= '</message>';

        $xml_str = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<message module="iCMS" version="1.0"> 
  <header action="REQUEST" command="CONTENT_OFFLINE" component-id="CLPS_01" component-type="CLPS" sequence="{$int_sequence}" timestamp="{$time}"/>  
  <body> 
    <contents> 
      <content content-id="{$result_index_import_info['data_info']}" sp-id="{$str_sp_id}" status="6" subcontent-id="{$result_media_import_info['data_info']}" target-site-id="{$str_target_site_id}" target-system-id="{$str_target_system_id}"/> 
    </contents> 
  </body> 
</message>

XML;

	    
	    $action = ($result_media['nns_deleted'] !=1) ? 'ONLINE' : 'OFFLINE';
	    $c2_info = array(
	        'nns_id'=>$task_log_id,
	        'nns_task_type'=>'Movie',
	        'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	        'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
	        'nns_action'=>	$action,
	        'nns_content'=>	$xml_str,
	    );
	    $result = $this->execute_self_c2($c2_info,false);
	    return $result;
	}
	
	
	/**
	 * 获取  GUID
	 */
	private function get_guid_rand($str_type,$str_integer_id,$time=0)
	{
	    $str_header = (strlen($this->str_header) < 8) ? str_pad($this->str_header, 8, "0", STR_PAD_RIGHT) : substr($this->str_header, 0,8);
	    $str_integer_id = base_convert($str_integer_id,10,36);
	    $str_guid = $str_header.'P'.$str_type.$this->get_rand_str(3).str_pad($str_integer_id, 6, "0", STR_PAD_LEFT);
	    $result_log = nl_c2_log::query_data_by_id($this->obj_dc, $str_guid);
	    if ($time >= 3 && isset($result_log['data_info']) && is_array($result_log['data_info']) && !empty($result_log['data_info']))
	    {
	        return np_guid_rand();
	    }
	    if(isset($result_log['data_info']) && is_array($result_log['data_info']) && !empty($result_log['data_info']))
	    {
	        $time++;
	        $str_guid = $this->get_guid_rand($str_type,$str_integer_id,$time);
	    }
	    return $str_guid;
	}
	
	/**
	 * 获取随机字符串
	 * @param unknown $len
	 */
	private function get_rand_str($len) 
	{  
        $chars = array(  
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9');  
        $charsLen = count($chars) - 1;
        $output = '';  
        for ($i = 0; $i < $len; $i++) 
        {  
            $output .= $chars[rand(0, $charsLen)];  
        }  
        return $output;  
    }  
	
	
	public function do_notify()
	{
	    return $this->notify_upstream(array('nns_cp_id'=>$this->arr_params['nns_cp_id']),$this->arr_params['action'],$this->arr_params['result'],'video');
	}
	
	
	/**
	 * 
	 * @param unknown $result_media
	 * @param unknown $action
	 * @param unknown $result
	 * @param string $type
	 */
	private function notify_upstream($result_media,$action,$result,$type = 'media')
	{
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/notify/make_notify_file.class.php';
	    global $g_project_name;
	    $project_name = (isset($g_project_name) && strlen($g_project_name)>0) ? $g_project_name : '';
	    if(strlen($project_name) <1)
	    {
	        return $this->_make_return_data(1,"全局项目ID配置为空:[{$project_name}];line:[".__LINE__."];file[".__FILE__."]");
	    }
	    global $g_bk_web_url;
	    $bk_web_url = (isset($g_bk_web_url) && strlen($g_bk_web_url)>0) ? $g_bk_web_url : '';
	    $bk_web_url = rtrim(rtrim(trim($bk_web_url),'/'),'\\');
	    if(strlen($bk_web_url) <1)
	    {
	        return $this->_make_return_data(1,"后台基本地址(数据库配置文件父级目录)配置为空:[{$bk_web_url}];line:[".__LINE__."];file[".__FILE__."]");
	    }
	    unset($g_project_name);
	    unset($g_bk_web_url);
	    $str_include_file = dirname(dirname(dirname(dirname(__FILE__)))).'/notify/soap/'.$project_name.'/notify.php';
	    if(!file_exists($str_include_file))
	    {
	        return $this->_make_return_data(1,"加载消息反馈文件不存在:[{$str_include_file}];line:[".__LINE__."];file[".__FILE__."]");
	    }
	    include_once $str_include_file;
	    $ResultCode = (isset($result['ret']) && $result['ret'] == 0) ? 0 : -1;
	    $Description = ($ResultCode == 0) ? 'success' : 'error';
	    #TODO
 	    $result_media['nns_content_id'] = '1111111';
        if($type == 'media')
        {
	        $obj_make_file = new nl_make_notify_file($this->str_sp_id,$result_media['nns_cp_id']);
            $result_notify_file = $obj_make_file->make_notify_file($result_media,$ResultCode,$Description,'media',$action);
            if($result_notify_file['ret'] !=0)
            {
                return $result_notify_file;
            }
            $site_callback_url = rtrim(rtrim(trim($this->arr_sp_config['site_callback_url']),'/'),'\\');
            if(strlen($site_callback_url) <1)
            {
                return $this->_make_return_data(1,"消息反馈上游地址为空:[{$site_callback_url}];line:[".__LINE__."];file[".__FILE__."]");
            }
        }
        else
        {
            $site_callback_url='';
            $result_notify_file=array();
            $result_notify_file['data_info']='';
        }
	    $WDSL_URL = $bk_web_url.'/notify/soap/'.$project_name.'/'.$result_media['nns_cp_id'].'/service.php?wsdl';
	    $obj_soap = new notify($WDSL_URL,$result_media['nns_cp_id'],'starcor',$this->arr_params['bk_c2_task_info']['nns_message_id'],(int)$ResultCode,$Description,$site_callback_url.$result_notify_file['data_info']);
	    $soap_result = $obj_soap->ExecCmd();
        if(isset($soap_result->ResultCode))
        {
            $ResultCode = ($soap_result->ResultCode == 0) ? 0 : 1;
        }
        else
        {
            $ResultCode = ($soap_result->Result == 0) ? 0 : 1;
        }
	    return  $this->_make_return_data($ResultCode,isset($soap_result->ErrorDescription) ? $soap_result->ErrorDescription : '');
	}
	
	
	/**
	 * 直播片源CDN
	 * @param unknown $movie_id
	 */
	public function do_live_media()
	{
		if(!isset($this->arr_params['nns_channel_id']) && !empty($this->arr_params['nns_channel_id']))
		{
			return array(
					'ret'=>1,
					'reason'=>'直播片源传入参数无nns_channel_id字段,参数为:'.var_export($this->arr_params),
			);
		}
		$channel_id = $this->arr_params['nns_channel_id'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/live.class.php';
		$result_channel = nl_live::query_by_id($this->obj_dc, $channel_id);
		if($result_channel['ret'] !=0 || !isset($result_channel['data_info']) || !is_array($result_channel['data_info']) || empty($result_channel['data_info']))
		{
			return $result_channel;
		}
		$result_channel = $result_channel['data_info'];
		$result_channel_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'live', $channel_id,$result_channel,null,'cdn',$this->str_header);
		if($result_channel_import_info['ret'] !=0 || !isset($result_channel_import_info['data_info']) || strlen($result_channel_import_info['data_info']) <1)
		{
			return $result_channel_import_info;
		}
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/live_media.class.php';
		$result_media = nl_live_media::query_by_channel_id($this->obj_dc, $channel_id);
		if($result_media['ret'] !=0 || !isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
		{
			return $result_media;
		}
		$result_media = $result_media['data_info'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/c2_task/c2_task.class.php';
		foreach ($result_media as $media_list)
		{
			$result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'live_media', $media_list['nns_id'],$media_list,null,'cdn',$this->str_header);
			if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
			{
				return $result_media_import_info;
			}
			$media_list['nns_cdn_media_guid'] = $result_media_import_info['data_info'];
			$xml_str='';
			$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
			$xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
			$xml_str .= 	'<Objects>';
			$xml_str .=     $this->make_live_media_xml($media_list,array('nns_cdn_channel_guid'=>$result_channel_import_info['data_info']));
			$xml_str .= 	'</Objects>';
			$xml_str .= '</ADI>';
			$action = ($result_media['nns_deleted'] !=1) ? ($result_media['nns_modify_time'] > $result_media['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
			$str_action = ($result_media['nns_deleted'] !=1) ? ($result_media['nns_modify_time'] > $result_media['nns_create_time']) ? 'modify' : 'add' : 'destroy';
			$file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'live_media' . '_' . $action . '_' .  rand(100, 999) . '.xml';
			
			$str_task_id = np_guid_rand();
			$result_c2_exsist=nl_c2_task::query_c2_exsist($this->obj_dc,'live_media',$media_list['nns_id'],$this->str_sp_id);
			if($result_c2_exsist['ret'] !=0)
			{
				return $result_c2_exsist;
			}
			$result_c2_exsist = isset($result_c2_exsist['data_info'][0]) ? $result_c2_exsist['data_info'][0] : null;
			if(is_array($result_c2_exsist) && !empty($result_c2_exsist))
			{
				$str_task_id = $result_c2_exsist['nns_id'];
				$result_c2_edit=nl_c2_task::edit($this->obj_dc, array('nns_action'=>$str_action,'nns_status'=>1,'nns_epg_status'=>97),$str_task_id);
				if($result_c2_edit['ret'] !=0)
				{
					return $result_c2_edit;
				}
			}
			else
			{
				$arr_c2_add = array(
						'nns_id'=>$str_task_id,
						'nns_type'=>'live_media',
						'nns_name'=>"[{$result_channel['nns_name']}] 1 [{$media_list['nns_mode']}]-直播流",
						'nns_ref_id'=>'',
						'nns_action'=>$str_action,
						'nns_status'=>1,
						'nns_org_id'=>$this->str_sp_id,
						'nns_category_id'=>'',
						'nns_src_id'=>$media_list['nns_id'],
						'nns_all_index'=>1,
						'nns_clip_task_id'=>'',
						'nns_clip_date'=>'',
						'nns_op_id'=>'',
						'nns_epg_status'=>97,
						'nns_ex_url'=>'',
						'nns_file_path'=>'',
						'nns_file_size'=>'',
						'nns_file_md5'=>'',
						'nns_cdn_policy'=>'',
						'nns_epg_fail_time'=>0,
						'nns_message_id'=>'',
				);
				$result_c2_add=nl_c2_task::add($this->obj_dc, $arr_c2_add);
				if($result_c2_edit['ret'] !=0)
				{
					return $result_c2_add;
				}
			}
			
			$c2_info = array(
					'nns_task_type'=>'PhysicalChannel',
					'nns_task_id'=> $str_task_id,
					'nns_task_name'=> "[{$result_channel['nns_name']}] 1 [{$media_list['nns_mode']}]-直播流",
					'nns_action'=>	$action,
					'nns_url' => $file_name,
					'nns_content' => $xml_str,
					'nns_desc' => 'PhysicalChannel,'.$action,
			);
			return $this->execute_self_c2($c2_info);
		}
	}
	
	
	/**
	 * 直播节目单CDN
	 * @param unknown $movie_id
	 */
	public function do_playbill()
	{
		if(!isset($this->arr_params['nns_playbill_id']) && !empty($this->arr_params['nns_playbill_id']))
		{
			return array(
					'ret'=>1,
					'reason'=>'节目单传入参数无nns_playbill_id字段,参数为:'.var_export($this->arr_params),
			);
		}
		$playbill_id = $this->arr_params['nns_playbill_id'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/live_media.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/playbill.class.php';
		$result_playbill = nl_playbill::query_by_id($this->obj_dc, $playbill_id);
		if($result_playbill['ret'] !=0 || !isset($result_playbill['data_info']) || !is_array($result_playbill['data_info']) || empty($result_playbill['data_info']))
		{
			return $result_playbill;
		}
		$result_playbill = $result_playbill['data_info'];
		$result_live_media = nl_live_media::query_by_id($this->obj_dc, $result_playbill['nns_live_media_id']);
		if($result_live_media['ret'] !=0 || !isset($result_live_media['data_info']) || !is_array($result_live_media['data_info']) || empty($result_live_media['data_info']))
		{
			return $result_live_media;
		}
		$result_live_media = $result_live_media['data_info'];
		$result_playbill_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'playbill', $playbill_id,$result_playbill,null,'cdn',$this->str_header);
		if($result_playbill_import_info['ret'] !=0 || !isset($result_playbill_import_info['data_info']) || strlen($result_playbill_import_info['data_info']) <1)
		{
			return $result_playbill_import_info;
		}
		$result_live_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'live_media', $playbill_id,$result_live_media,null,'cdn',$this->str_header);
		if($result_live_media_import_info['ret'] !=0 || !isset($result_live_media_import_info['data_info']) || strlen($result_live_media_import_info['data_info']) <1)
		{
			return $result_live_media_import_info;
		}
		$result_playbill['nns_cdn_playbill_guid'] = $result_playbill_import_info['data_info'];
		$result_playbill['nns_cdn_live_media_guid'] = $result_live_media_import_info['data_info'];
		$result_playbill_ex['nns_filetype'] = $result_live_media['nns_filetype'];
		$result_playbill_ex['nns_import_source'] = $result_live_media['nns_import_source'];
		$result_playbill_ex['nns_domain'] = $result_live_media['nns_domain'];
		$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
		$xml_str .= 	'<Objects>';
		$xml_str .=     $this->make_playbill_xml($result_playbill,$result_playbill_ex);
		$xml_str .= 	'</Objects>';
		$xml_str .= '</ADI>';
		
		$action = ($result_playbill['nns_state'] !=1) ? ($result_playbill['nns_modify_time'] > $result_playbill['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
		$str_action = ($result_playbill['nns_state'] !=1) ? ($result_playbill['nns_modify_time'] > $result_playbill['nns_create_time']) ? 'modify' : 'add' : 'destroy';
		$file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'playbill' . '_' . $action . '_' .  rand(100, 999) . '.xml';
		$str_task_id = np_guid_rand();
		$result_c2_exsist=nl_c2_task::query_c2_exsist($this->obj_dc,'playbill',$result_playbill['nns_id'],$this->str_sp_id);
		if($result_c2_exsist['ret'] !=0)
		{
			return $result_c2_exsist;
		}
		$result_c2_exsist = isset($result_c2_exsist['data_info'][0]) ? $result_c2_exsist['data_info'][0] : null;
		if(is_array($result_c2_exsist) && !empty($result_c2_exsist))
		{
			$str_task_id = $result_c2_exsist['nns_id'];
			$result_c2_edit=nl_c2_task::edit($this->obj_dc, array('nns_action'=>$str_action,'nns_status'=>1,'nns_epg_status'=>97),$str_task_id);
			if($result_c2_edit['ret'] !=0)
			{
				return $result_c2_edit;
			}
		}
		else
		{
			$arr_c2_add = array(
					'nns_id'=>$str_task_id,
					'nns_type'=>'playbill',
					'nns_name'=>"[{$result_playbill['nns_name']}]-节目单",
					'nns_ref_id'=>$result_playbill['nns_live_media_id'],
					'nns_action'=>$str_action,
					'nns_status'=>1,
					'nns_org_id'=>$this->str_sp_id,
					'nns_category_id'=>'',
					'nns_src_id'=>$result_playbill['nns_id'],
					'nns_all_index'=>1,
					'nns_clip_task_id'=>'',
					'nns_clip_date'=>'',
					'nns_op_id'=>'',
					'nns_epg_status'=>97,
					'nns_ex_url'=>'',
					'nns_file_path'=>'',
					'nns_file_size'=>'',
					'nns_file_md5'=>'',
					'nns_cdn_policy'=>'',
					'nns_epg_fail_time'=>0,
					'nns_message_id'=>'',
			);
			$result_c2_add=nl_c2_task::add($this->obj_dc, $arr_c2_add);
			if($result_c2_edit['ret'] !=0)
			{
				return $result_c2_add;
			}
		}
		$c2_info = array(
				'nns_task_type'=>'Schedule',
				'nns_task_id'=> $str_task_id,
				'nns_task_name'=> "[{$result_playbill['nns_name']}]-节目单",
				'nns_action'=>	$action,
				'nns_url' => $file_name,
				'nns_content' => $xml_str,
				'nns_desc' => 'Schedule,'.$action,
		);
		return $this->execute_self_c2($c2_info);
	}
	
	
	/**
	 * 
	 * @param unknown $c2_info
	 */
	public function execute_self_c2($c2_info,$flag=true)
	{
	    $c2_info['nns_type'] = 'std';
	    $c2_info['nns_org_id'] = $this->str_sp_id;
	    $c2_info['nns_desc'] ='';
// 	    $c2_info['nns_id'] = np_guid_rand();
	    include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/np/np_http_curl.class.php';
	    //检查时间范围是否有效执行
	    if ($this->exists_c2_commond($c2_info))
	    {
	        return $this->_make_return_data(1,'该task在一小时内执行过或则task查询无数据,task信息:'.var_export($c2_info,true));
	    }
	    $sub_path = date('Y-m-d');
	    $file = $this->str_sp_id . '_' .$c2_info['nns_action'].'_'. $c2_info['nns_id'] . '.xml';
	    $c2_info['nns_url'] = $sub_path . '/'.strtolower($c2_info['nns_task_type']).'/' . $file;
	    //生成soap xml文件 路径
	    $file_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/mgtv/' . $this->str_sp_id . '/inject_xml/' . $sub_path . '/' .strtolower($c2_info['nns_task_type']).'/';
	    if (!is_dir($file_path))
	    {
	        mkdir($file_path, 0777, true);
	    }
	    $file_path .= $file;
	    file_put_contents($file_path, $c2_info['nns_content']);
	    $cdn_send_mode = $this->arr_sp_config['cdn_send_mode'];
	    if($cdn_send_mode != '1')
	    {
	        $c2_info['nns_desc'] = "注入下游模式非HTTP";
	        nl_c2_log::add($this->obj_dc, $c2_info);
	        return $this->_make_return_data(1,"注入下游模式非HTTP");
	    }
	    if($flag)
	    {
    	    $cdn_send_mode_url = $this->arr_sp_config['cdn_send_mode_url'];
    	    if(strlen($cdn_send_mode_url) <1)
    	    {
    	        $c2_info['nns_desc'] = "注入下游媒资基本信息请求url为空";
    	        nl_c2_log::add($this->obj_dc, $c2_info);
    	        return $this->_make_return_data(1,"注入下游请求url为空");
    	    }
    	    if(isset($this->str_adi_url) && strlen($this->str_adi_url) >0)
    	    {
    	        $cdn_send_mode_url = $this->str_adi_url;
    	    }
	    }
	    else 
	    {
	        $cdn_send_mode_url = $this->arr_sp_config['cdn_send_cdi_mode_url'];
	        if(strlen($cdn_send_mode_url) <1)
	        {
	            $c2_info['nns_desc'] = "注入下游上下线信息请求url为空";
	            nl_c2_log::add($this->obj_dc, $c2_info);
	            return $this->_make_return_data(1,"注入下游请求url为空");
	        }
    	    if(isset($this->str_cdi_url) && strlen($this->str_cdi_url) >0)
    	    {
    	        $cdn_send_mode_url = $this->str_cdi_url;
    	    }
	    }
	    if(isset($this->str_system_id) && strlen($this->str_system_id) >0)
	    {
	        $cdn_send_mode_url = preg_replace('/systemId\=[a-z0-9_]+/i', "systemId={$this->str_system_id}", $cdn_send_mode_url);
	    }
	    if(strlen($c2_info['nns_id']) !=32)
	    {
    	    $obj_curl = new np_http_curl_class();
    	    $arr_header = array(
    	        "Content-type: application/xml;charset=\"utf-8\"",
    	        "Accept: text/xml",
    	        "Cache-Control: no-cache",
    	        "Pragma: no-cache",
    	        "SOAPAction: \"run\"",
    	    );
    	    $request_ret = $obj_curl->do_request('post',$cdn_send_mode_url,$c2_info['nns_content'],$arr_header,60);
//     	    ob_clean();
    	    
//     	    header("Content-Type:text/xml;charset=utf-8");
//     	    echo $c2_info['nns_content'];die;
//     	    var_dump($request_ret,$cdn_send_mode_url,$c2_info['nns_content']);die;
            nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, ';收到来自CDN的内容:' . $request_ret."发送内容:{$c2_info['nns_content']}", $this->str_sp_id);
    	    $curl_info = $obj_curl->curl_getinfo();
    	    if($curl_info['http_code'] != '200')
    	    {
    	        $c2_info['nns_result'] = '[' . 1 . ']';
    	        nl_c2_log::add($this->obj_dc, $c2_info);
    	        nl_c2_task::edit_cdn_fail_time_v2($this->obj_dc,$c2_info['nns_task_id']);
    	        return $this->_make_return_data(1,"CMS应答非200状态，[HTTP状态码]:{$curl_info['http_code']}[HTTP错误]".$obj_curl->curl_error());
    	    }
    	    if($c2_info['nns_action'] != 'OFFLINE' && $c2_info['nns_action'] != 'ONLINE')
    	    {
    	        $request_ret = explode('|', $request_ret);
    	        $str_code = (isset($request_ret[0]) && $request_ret[0] == '0') ? 1 : '-1';
    	        $str_description = (isset($request_ret[1])) ? $request_ret[1] : 'unkown error';
    	    }
    	    else
    	    {
    	        $request_ret = simplexml_load_string($request_ret);
    	        $request_ret = json_decode(json_encode($request_ret),true);
    	        $str_code = $request_ret["body"]["result"]["@attributes"]["code"];
    	        $str_description = $request_ret["body"]["result"]["@attributes"]["description"];
    	    }
	    }
	    else 
	    {
	        $str_code = '-1';
	        $str_description = "循环了3次都不能生成唯一ID,SB的思华";
	    }
	    if((isset($str_code) && $str_code == '1'))
	    {
	        $c2_info['nns_result'] = '[0]';
	        $c2_info['nns_status'] = '5';
	        $c2_info['nns_desc'] = $str_description;
	        $temp_nns_status =  ($c2_info['nns_task_type'] == 'Series' || $c2_info['nns_task_type'] == 'Program') ? 0 : 6;
	        nl_c2_task::edit($this->obj_dc, array('nns_status'=>$temp_nns_status), $c2_info['nns_task_id']);
	        
	    }
	    else
	    {
	        $c2_info['nns_result'] = '[1]';
	        $c2_info['nns_status'] = '-1';
            $c2_info['nns_desc'] = $str_description;
	        nl_c2_task::edit_cdn_fail_time_v2($this->obj_dc,$c2_info['nns_task_id']);
	    }
	    $c2_info['nns_content']='';
	    nl_c2_log::add($this->obj_dc, $c2_info);
	    return $this->_make_return_data(0,"OK".var_export($request_ret,true));
	}

    /**
     * 思华-转换成Java的时间格式
     * @author <feijian.gao@starcor.com>
     * @date    2017年9月1日14:57:55
     * @return false|mixed|string
     */
	private function get_java_datetime()
    {
        $time = time();
        $date = date('c',time());
        $date_zone_replace = date('O',time());
        $date_zone_search = date('P',time());
        $date = str_replace($date_zone_search,$date_zone_replace,$date);
        $date = str_replace("+",". ",$date);
        return $date;
    }
}
