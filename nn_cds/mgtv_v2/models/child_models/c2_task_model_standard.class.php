<?php
/**
 * starcor CDN标准规范ADI CDN注入
 * @author liangpan
 */
class c2_task_model_v2 extends nn_public_model
{
	/**
	 * 组装主媒资信息
	 * @param unknown $arr_info 主媒资基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 */
	private function make_vod_xml($arr_info,$str_Action,$arr_info_ex=null)
	{
		$str_xml ='<Object ElementType="Series" ContentID="'.$arr_info['nns_cdn_video_guid'].'" Action="'.$str_Action.'">';
		$str_xml.=	'<Property Name="Name"><![CDATA['.$arr_info['nns_name'].']]></Property>';
		$str_xml.=	'<Property Name="OrderNumber"><![CDATA[1]]></Property>';
		$str_xml.=	'<Property Name="OriginalName"><![CDATA['.$arr_info['nns_name'].']]></Property>';
		$str_xml.=	'<Property Name="AliasName"><![CDATA['.$arr_info['nns_alias_name'].']]></Property>';
		$str_xml.=	'<Property Name="EnglishName"><![CDATA['.$arr_info['nns_eng_name'].']]></Property>';
		$str_xml.=	'<Property Name="SortName"><![CDATA['.$arr_info['nns_name'].']]></Property>';
		$str_xml.=	'<Property Name="SearchName"><![CDATA['.$arr_info['nns_name'].']]></Property>';
		$str_xml.=	'<Property Name="OrgAirDate"><![CDATA['.$arr_info['nns_copyright_date'].']]></Property>';
		$str_xml.=	'<Property Name="ReleaseYear"><![CDATA['.$arr_info['nns_show_time'].']]></Property>';
		$str_xml.=	'<Property Name="LicensingWindowStart"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="LicensingWindowEnd"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="CPContentID"><![CDATA['.$arr_info['nns_cp_id'].']]></Property>';
		$str_xml.=	'<Property Name="DisplayAsNew"><![CDATA[0]]></Property>';
		$str_xml.=	'<Property Name="DisplayAsLastChance"><![CDATA[0]]></Property>';
		$str_xml.=	'<Property Name="Macrovision"><![CDATA[1]]></Property>';
		$str_xml.=	'<Property Name="Price"><![CDATA[1]]></Property>';
		$str_xml.=	'<Property Name="VolumnCount"><![CDATA['.$arr_info['nns_all_index'].']]></Property>';
		$str_xml.=	'<Property Name="NewCount"><![CDATA['.$arr_info['nns_new_index'].']]></Property>';
		$str_xml.=	'<Property Name="Status"><![CDATA[1]]></Property>';
		$str_xml.=	'<Property Name="Description"><![CDATA['.$arr_info['nns_summary'].']]></Property>';
		$str_xml.=	'<Property Name="ContentProvider"><![CDATA['.$arr_info['nns_import_source'].']]></Property>';
		$str_xml.=	'<Property Name="KeyWords"><![CDATA['.$arr_info['nns_keyword'].']]></Property>';
		$str_xml.=	'<Property Name="OriginalCountry"><![CDATA['.$arr_info['nns_area'].']]></Property>';
		$str_xml.=	'<Property Name="ActorDisplay"><![CDATA['.$arr_info['nns_actor'].']]></Property>';
		$str_xml.=	'<Property Name="WriterDisplay"><![CDATA['.$arr_info['nns_screenwriter'].']]></Property>';
		$str_xml.=	'<Property Name="Language"><![CDATA['.$arr_info['nns_language'].']]></Property>';
		$str_xml.=	'<Property Name="Kind"><![CDATA['.$arr_info['nns_kind'].']]></Property>';
		$str_xml.=	'<Property Name="Duration"><![CDATA['.$arr_info['nns_view_len'].']]></Property>';
		$str_xml.=	'<Property Name="CategoryName"><![CDATA[电视剧]]></Property>';
		$str_xml.=	'<Property Name="CategoryID"><![CDATA[10000140142]]></Property>';
		$str_xml.=	'<Property Name="PlayCount"><![CDATA[0]]></Property>';
		$str_xml.=	'<Property Name="CategorySort"><![CDATA[0]]></Property>';
		$str_xml.=	'<Property Name="Tags"><![CDATA['.$arr_info['nns_tag'].']]></Property>';
		$str_xml.=	'<Property Name="ViewPoint"><![CDATA['.$arr_info['nns_summary'].']]></Property>';
		$str_xml.=	'<Property Name="StarLevel"><![CDATA[6]]></Property>';
		$str_xml.=	'<Property Name="Rating"/>';
		$str_xml.=	'<Property Name="Awards"/>';
		$str_xml.=	'<Property Name="Sort"><![CDATA[0]]></Property>';
		$str_xml.=	'<Property Name="Hotdegree"><![CDATA[0]]></Property>';
		$str_xml.=	'<Property Name="Reserve1"/>';
		$str_xml.=	'<Property Name="Reserve1"/>';
		$str_xml.='</Object>';
		return $str_xml;
	}
	
	/**
	 * 组装分集信息
	 * @param unknown $arr_info 片源基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 */
	private function make_vod_index_xml($arr_info,$str_Action,$arr_info_ex=null)
	{
		$str_xml ='<Object ElementType="Program" ContentID="'.$arr_info['nns_cdn_index_guid'].'" Action="'.$str_Action.'">';
		$str_xml.=	'<Property Name="Name"><![CDATA['.$arr_info['nns_name'].']]></Property>';
		$str_xml.=	'<Property Name="CPContentID"><![CDATA['.$arr_info['nns_cp_id'].']]></Property>';
		$str_xml.= 	'<Property Name="OrderNumber"><![CDATA[1]]></Property>';//0: No DRM 1: BES DRM
		$str_xml.= 	'<Property Name="OriginalName"><![CDATA['.$arr_info['nns_name'].']]></Property>';//0: No DRM  1: BES DRM
		$str_xml.= 	'<Property Name="SortName"><![CDATA[1]]></Property>';
		$str_xml.= 	'<Property Name="Sequence"><![CDATA['.$arr_info['nns_index'].']]></Property>';
		$str_xml.= 	'<Property Name="SortName"><![CDATA[1]]></Property>';
		$str_xml.= 	'<Property Name="SearchName"><![CDATA['.$arr_info['nns_name'].']]></Property>';
		$str_xml.= 	'<Property Name="ActorDisplay"><![CDATA['.$arr_info['nns_actor'].']]></Property>'; //0: 4x3   1: 16x9(Wide)
		$str_xml.= 	'<Property Name="OriginalCountry"><![CDATA[]]></Property>';//0:无字幕 1:有字幕
		$str_xml.=	'<Property Name="Language"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="ReleaseYear"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="OrgAirDate"><![CDATA['.$arr_info['nns_kbps'].']]></Property>';
		$str_xml.=	'<Property Name="LicensingWindowStart"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="LicensingWindowEnd"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="DisplayAsNew"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="DisplayAsLastChance"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="Macrovision"><![CDATA[1]]></Property>';
		$str_xml.=	'<Property Name="Description"><![CDATA['.$arr_info['nns_summary'].']]></Property>';
		$str_xml.=	'<Property Name="PriceTaxIn"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="Status"><![CDATA[1]]></Property>';
		$str_xml.=	'<Property Name="SourceType"><![CDATA[1]]></Property>';
		$str_xml.=	'<Property Name="SeriesFlag"><![CDATA[1]]></Property>';
		$str_xml.=	'<Property Name="ContentProvider"><![CDATA['.$arr_info['nns_import_source'].']]></Property>';
		$str_xml.=	'<Property Name="KeyWords"><![CDATA['.$arr_info['nns_summary'].']]></Property>';
		$str_xml.=	'<Property Name="Tags"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="ViewPoint"><![CDATA['.$arr_info['nns_watch_focus'].']]></Property>';
		$str_xml.=	'<Property Name="StarLevel"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="Rating"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="Awards"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="Duration"><![CDATA['.$arr_info['nns_time_len'].']]></Property>';
		$str_xml.=	'<Property Name="Reserve1"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="Reserve2"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="Reserve3"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="Reserve4"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="Reserve5"><![CDATA[]]></Property>';
		$str_xml.='</Object>';
		return $str_xml;
	}
	
	/**
	 * 组装片源信息
	 * @param unknown $arr_info 片源基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 * @var string FileURL
	 *     说明： 媒体文件 URL ftp://username:password@ip:port/...  标准 FTP协议 或者 http:// ip:port/... 
	 *     注释： 对托管内容注入方式下填写，支持 FTP 和 HTTP 协议获取内容 对非托管内容预注入方式下 则 为 空 ， 填 写ServiceURL 属性 当FileURL和ServiceURL均有值时，
	 *	               首选采用该属性值去获取内容。注入 HLS 内容时，如果属性值为 m3u8 文件结尾，CDN 获取内容时，获取整个 m3u8 的上级目录
	 * @var string ServiceURL
	 *     说明： 内容服务 URL
	 *     注释： 需要指定 CDN 服务 URL 的内容注入/预注入内容接入或实时回源接入内容删除时填写。为 CDN 提供服务的内容服务调用 URL，当 ServiceURL 有值时，该URL 前缀信息应和 CDN 服务配置
     *           表中对应域内容提供 商 的PlaybackURLprefix 保持一致，用户需要基于本URL 访问注入内容
     * @var string CPContentID
	 *     说明： CP 对于媒体文件的标识 
	 * @var string SourceDRMType 
	 *     说明： 0: No DRM 1: BES DRM
	 * @var string DestDRMType  
	 *     说明： 0: No DRM 1: BES DRM
	 * @var string AudioType   
	 *     说明： 0: 其他 1: Monaural 单声道 2: Stereo 多声道 3: Two-nation monaural 双单声道 4: Two-nation stereo 双多声道  5: AC3(5:1 channel) AC3声道
	 * @var string ScreenFormat    
	 *     说明： 0: 4x3   1: 16x9(Wide)
	 * @var string ClosedCaptioning     
	 *     说明： 字幕标志 0:无字幕 1:有字幕
	 * @var string Duration      
	 *     说明： 播放时长 HHMISSFF （时分秒帧）
	 * @var string FileSize       
	 *     说明： 文件大小，单位为 Byte
	 * @var string BitRateType        
	 *     说明： 码流 1: 400k 2: 700K 3:  1.3M 4: 2M 5: 2.5M 6:  8M 7: 10M  8：15M 9：20M 10：30M 
	 * @var string VideoType         
	 *     说明： 编码格式： 1. H.264 2. MPEG4 3. AVS 4. MPEG2 5. MP3 6. WMV 7. H.265 
	 * @var string AudioEncodingType          
	 *     说明： 编码格式： 1. MP2 2. AAC 3. AMR 4. Mp3 
	 * @var string Resolution           
	 *     说明： 分辨率类型 1. QCIF 2. QVGA 3. 2/3 D1 4. 3/4 D1 5. D1 6. 720P 7. 1080i 8. 1080P 9. 2k 11. 4k 13. 8k 
	 * @var string VideoProfile           
	 *     说明： 1. Simple 2. Advanced Simple 3. Baseline 4. Main 5. High 6. JiZhun 
	 * @var string SystemLayer           
	 *     说明： 1. TS 2. 3GP  
	 * @var string Domain            
	 *     说明： 发布到融合 CDN 后的服务域和服务协议 
	 *     注释： 包含 ServiceDomain 和ProtocolType 两层含义，为 4 字节整数，其中最高三 个 字 节 为ServiceDomain，表示预分发网络域，最低 3 位取值如下： 
	 *               0x000001-IPTV 网络 
	 *               0x000002-互联网网络 
	 *               0x000004-移动网络 
	 *               0x000008~0x000080-预留 
	 *            最 低 一 个 字 节 为ProtocolType，表示服务协议，最低 4 位取值如下：
	 *               0x01-IPTV TS RTSP， 
	 *               0x02-HPD 
	 *               0x04-ISMA RTSP 
	 *               0x08-HLS。 
	 *            如果 Domain 为空，则采用该 CMSID 缺省分发域，缺省分发域可配置
	 * @var string Hotdegree             
	 *     说明： 发布到融合 CDN 时使用热度表示
	 *     注释： 0（普通），1（高） 缺省为 0，该字段值越大，表示优先级越高。CDN 应根据优先级安排处理顺序
	 * @var string deliverTime              
	 *     说明： 内容分发时间
	 *     注释： 北京时间，采用十四位的字 符 串 格 式YYYYMMDDHHMMSS，例如： 20040101123000 ， 代 表 2004 年 1 月 1 日 12点 30 分 00 秒。说明 如果不携带该字段，表示立即分发
	 */
	private function make_vod_media_xml($arr_info,$str_Action,$arr_info_ex=null)
	{
		$str_xml ='<Object ElementType="Movie" ContentID="'.$arr_info['nns_cdn_media_guid'].'" Action="'.$str_Action.'" Type="1">';
// 		$str_xml.=	'<Property Name="FileURL"><![CDATA[http://211.156.185.107:10000/ott/运营上传勿动/月光男孩.BD.720p.中英双字幕.mkv]]></Property>';
		$str_xml.=	'<Property Name="FileURL"><![CDATA['.$arr_info['nns_url'].']]></Property>';
		$str_xml.=	'<Property Name="CPContentID"><![CDATA['.$arr_info['nns_cp_id'].']]></Property>';
		$str_xml.= 	'<Property Name="SourceDRMType"><![CDATA[0]]></Property>';
		$str_xml.= 	'<Property Name="DestDRMType"><![CDATA[0]]></Property>';
		$str_xml.= 	'<Property Name="AudioType"><![CDATA[1]]></Property>';
		$str_xml.= 	'<Property Name="ScreenFormat"><![CDATA[1]]></Property>'; 
		$str_xml.= 	'<Property Name="ClosedCaptioning"><![CDATA[1]]></Property>';
		$str_xml.= 	'<Property Name="Tags"><![CDATA['.$arr_info['nns_tag'].']]></Property>';
		$str_xml.=	'<Property Name="Duration"><![CDATA['.$arr_info['nns_file_time_len'].']]></Property>';
		$str_xml.=	'<Property Name="FileSize"><![CDATA['.$arr_info['nns_file_size'].']]></Property>';
		$str_xml.=	'<Property Name="BitRateType"><![CDATA['.$arr_info['nns_kbps'].']]></Property>';
		$str_xml.=	'<Property Name="VideoType"><![CDATA[1]]></Property>';
		$str_xml.=	'<Property Name="AudioEncodingType"><![CDATA[4]]></Property>';
		$str_xml.=	'<Property Name="Resolution"><![CDATA['.$arr_info['nns_file_resolution'].']]></Property>';
		$str_xml.=	'<Property Name="MediaMode"><![CDATA[1]]></Property>';
		$str_xml.=	'<Property Name="SystemLayer"><![CDATA[1]]></Property>';
		$str_xml.=	'<Property Name="ServiceType"><![CDATA['.$arr_info['nns_media_service'].']]></Property>';
		$str_xml.=	'<Property Name="Domain"><![CDATA['.$arr_info['nns_domain'].']]></Property>';
		$str_xml.=	'<Property Name="Hotdegree"><![CDATA[0]]></Property>';
		$str_xml.='</Object>';
		return $str_xml;
	}
	
	
	/**
	 * 组装直播频道信息
	 * @param unknown $arr_info 片源基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 */
	private function make_live_xml($arr_info,$arr_info_ex=null,$str_Action)
	{
		$TimeShift = (isset($arr_info_ex['live_type']) && (strpos(strtolower($arr_info_ex['live_type']),'tstv') !== false || strpos(strtolower($arr_info_ex['live_type']),'playback') !== false)) ? 1 : 0;
		$str_xml ='<Object ElementType="Channel" ContentID="'.$arr_info['nns_cdn_live_guid'].'" Action="'.$str_Action.'">';
		$str_xml.=	'<Property Name="Name"><![CDATA['.$arr_info['nns_name'].']]></Property>';
		$str_xml.=	'<Property Name="ChannelNumber"><![CDATA['.$arr_info_ex['live_pindao_order'].']]></Property>';
		$str_xml.= 	'<Property Name="CallSign"><![CDATA['.$arr_info['nns_name'].']]></Property>';
		$str_xml.= 	'<Property Name="TimeShift"><![CDATA['.$TimeShift.']]></Property>';
		$str_xml.= 	'<Property Name="Type"><![CDATA[2]]></Property>';
		$str_xml.= 	'<Property Name="Status"><![CDATA[1]]></Property>';
		$str_xml.= 	'<Property Name="Tags"><![CDATA['.$arr_info['nns_tags'].']]></Property>';
		$str_xml.= 	'<Property Name="StartTime"><![CDATA[0000]]></Property>';
		$str_xml.= 	'<Property Name="EndTime"><![CDATA[2359]]></Property>';
		$str_xml.=	'<Property Name="CPContentID"><![CDATA['.$arr_info['nns_cp_id'].']]></Property>';
		$str_xml.=	'<Property Name="Country"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="State"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="City"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="ZipCode"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="URL"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="SubType"><![CDATA[1]]></Property>';
		$str_xml.=	'<Property Name="Language"><![CDATA['.$arr_info['nns_language'].']]></Property>';
		$str_xml.=	'<Property Name="Macrovision"><![CDATA[1]]></Property>';
		$str_xml.=	'<Property Name="VideoType"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="AudioType"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="StreamType"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="Bilingual"><![CDATA[]]></Property>';
		$str_xml.='</Object>';
		return $str_xml;
	}
	
	/**
	 * 组装片源信息
	 * @param unknown $arr_live_media 片源基本信息
	 * @param string $arr_live_media_ex 扩展数据
	 * @return string
	 * @var string DestCastType               
	 *     说明： 用户访问改频道使用是单播还是组播
	 *     注释： unicast/multicast
	 * @var string SrcCastType                
	 *     说明： CDN 接收到的频道类型方式，可能是单播，也可能是组播
	 *     注释： unicast/multicast
	 * @var string ChannelID                 
	 *     说明：频 道 ID ， 所 属 的 关 联 的channel 的标识 contentid
	 * @var string Storage                  
	 *     说明：回看标志 0:不生效 1:生效
	 * @var string TimeShift                   
	 *     说明：时移标志 0:不生效 1:生效
	 * @var string Status                    
	 *     说明：状态标志 0:失效 1:生效
	 * @var string StorageDuration                     
	 *     说明：存储时长，单位小时 仅仅对回看有效
	 * @var string TimeShiftDuration                      
	 *     说明：默认时移时长, 单位分钟 (Reserved) 仅仅对时移有效 
	 * @var string BitRateType                       
	 *     说明：码流: 1: 400k 2：700K 3:  1.3M 4：2M 5：2.5M 6:  8M 7：10M 8：15M 9：20M 10：30M 
	 * @var string ServiceURL
	 *     说明： 内容服务 URL
	 *     注释： 需要指定 CDN 服务 URL 的内容注入/预注入内容接入或实时回源接入内容删除时填写。为 CDN 提供服务的内容服务调用 URL，当 ServiceURL 有值时，该URL 前缀信息应和 CDN 服务配置
     *           表中对应域内容提供 商 的PlaybackURLprefix 保持一致，用户需要基于本URL 访问注入内容
	 * @var string CPContentID 
	 *     说明：内容服务商对于该物理频道的唯一标识  
	 * @var string MultiCastIP  
	 *     说明：组播 IP（当 srccasttype 为组播时，必填）    
	 * @var string MultiCastPort   
	 *     说明：组播端口（当 srccasttype 为组播时，必填）   
	 * @var string UnicastUrl    
	 *     说明：当 srccasttype 为单播时，必填。 SrcCastType 为单播且托管内容注入方式下填写，如果注入的是 HLS 直播内容，取值为编码器推送的直播URL： http://ip:port/xxx/index.m3u8 
	 *           对非托管内容预注入方式下 则 为 空 ， 填 写ServiceURL 属性   
	 * @var string VideoType    
	 *     说明：编码格式： 1. H.264 2. MPEG4 3. AVS 4. MPEG2 5.MP3 6.WMV7.H.265 
	 * @var string AudioType     
	 *     说明：编码格式： 1. MP2 2.AAC 3.AMR 4.MP3 
	 * @var string Resolution      
	 *     说明：分辨率类型 1.QCIF 2.QVGA 3.2/3 D1 4.3/4 D1 5.D1 6.720P 7.1080i 8.1080P 9.2K 11.4K 13.8K  
	 * @var string VideoProfile      
	 *     说明：1.Simple 2.Advanced Simple 3.Baseline 4.Main 5.High 6.JiZhun    
	 * @var string SystemLayer      
	 *     说明：1.TS 2.3GP 3.mp4 4.flv 5.rtp   
	 * @var string Domain            
	 *     说明： 发布到融合 CDN 后的服务域和服务协议 
	 *     注释： 包含 ServiceDomain 和ProtocolType 两层含义，为 4 字节整数，其中最高三 个 字 节 为ServiceDomain，表示预分发网络域，最低 3 位取值如下： 
	 *               0x000001-IPTV 网络 
	 *               0x000002-互联网网络 
	 *               0x000004-移动网络 
	 *               0x000008~0x000080-预留 
	 *            最 低 一 个 字 节 为ProtocolType，表示服务协议，最低 4 位取值如下：
	 *               0x01-IPTV TS RTSP， 
	 *               0x02-HPD 
	 *               0x04-ISMA RTSP 
	 *               0x08-HLS。 
	 *            如果 Domain 为空，则采用该 CMSID 缺省分发域，缺省分发域可配置
	 * @var string Hotdegree             
	 *     说明： 发布到融合 CDN 时使用热度表示
	 *     注释： 0（普通），1（高） 缺省为 0，该字段值越大，表示优先级越高。CDN 应根据优先级安排处理顺序
	 */
	private function make_live_media_xml($arr_live_media,$arr_live_media_ex=null,$str_Action)
	{
		$str_DestCastType = $str_SrcCastType = ($arr_live_media['nns_cast_type'] == 1) ? 'multicast' : 'unicast';
		$str_MultiCastIP = '';
		$str_MultiCastPort = '';
		$str_UnicastUrl = '';
		if($str_SrcCastType == 'multicast')
		{
			$arr_live_media['nns_url'] = trim($arr_live_media['nns_url']);
			$udp_url = preg_replace('/^udp/i', '', $arr_live_media['nns_url']);
			$udp_url = trim(ltrim(ltrim(ltrim(ltrim($udp_url,':'),'/'),'/'),'@'));
			$arr_udp = explode(':',$udp_url);
			$str_MultiCastIP = $arr_udp[0];
			$str_MultiCastPort = $arr_udp[1];
		}
		else
		{
			$str_UnicastUrl = $arr_live_media['nns_url'];
		}
		$str_BitRateType = (strtoupper($arr_live_media['nns_mode'])=="HD") ? 4 : 2;		
		
		$str_xml ='<Object ElementType="PhysicalChannel" ContentID="'.$arr_live_media['nns_cdn_media_guid'].'" Action="'.$str_Action.'">';
		$str_xml.=	'<Property Name="DestCastType"><![CDATA['.$str_DestCastType.']]></Property>';
		$str_xml.=	'<Property Name="SrcCastType"><![CDATA['.$str_SrcCastType.']]></Property>';
		$str_xml.= 	'<Property Name="ChannelID"><![CDATA['.$arr_live_media_ex['nns_cdn_channel_guid'].']]></Property>';
		$str_xml.= 	'<Property Name="Storage"><![CDATA['.$arr_live_media['nns_storage_status'].']]></Property>';
		$str_xml.= 	'<Property Name="TimeShift"><![CDATA['.$arr_live_media['nns_timeshift_status'].']]></Property>';
		$str_xml.= 	'<Property Name="Tags"><![CDATA['.$arr_live_media['nns_tags'].']]></Property>';
		$str_xml.= 	'<Property Name="Status"><![CDATA[1]]></Property>';
		$str_xml.= 	'<Property Name="StorageDuration"><![CDATA['.$arr_live_media['nns_storage_delay'].']]></Property>';
		$str_xml.= 	'<Property Name="TimeShiftDuration"><![CDATA['.$arr_live_media['nns_timeshift_delay'].']]></Property>';
		$str_xml.= 	'<Property Name="BitRateType"><![CDATA['.$str_BitRateType.']]></Property>';
		$str_xml.=	'<Property Name="ServiceURL"><![CDATA[]]></Property>';
		$str_xml.= 	'<Property Name="CPContentID"><![CDATA['.$arr_live_media['nns_import_source'].']]></Property>';
		$str_xml.= 	'<Property Name="MultiCastIP"><![CDATA['.$str_MultiCastIP.']]></Property>';
		$str_xml.= 	'<Property Name="MultiCastPort"><![CDATA['.$str_MultiCastPort.']]></Property>';
		if($str_SrcCastType == 'multicast')
		{
		    $str_xml.= 	'<Property Name="UnicastUrl"/>';
		}
		else
		{
		    $str_xml.= 	'<Property Name="UnicastUrl"><![CDATA['.$str_UnicastUrl.']]></Property>';
		}
		$str_xml.=	'<Property Name="VideoType"><![CDATA[1]]></Property>';
		$str_xml.=	'<Property Name="AudioType"><![CDATA[1]]></Property>';
		$str_xml.=	'<Property Name="Resolution"><![CDATA['.$arr_live_media['nns_kbps'].']]></Property>';
		$str_xml.=	'<Property Name="VideoProfile"><![CDATA[1]]></Property>';
		$str_xml.=	'<Property Name="SystemLayer"><![CDATA[]]></Property>';
		$str_xml.=	'<Property Name="Domain"><![CDATA['.$arr_live_media['nns_domain'].']]></Property>';
		$str_xml.=	'<Property Name="Hotdegree"><![CDATA[]]></Property>';
		$str_xml.='</Object>';
		return $str_xml;
	}
	
	/**
	 * 组装节目单信息
	 * @param unknown $arr_live_media 片源基本信息
	 * @param string $arr_live_media_ex 扩展数据
	 * @return string 
	 * @var string StartDate       
	 *     说明：节目开播日期(YYYYMMDD)  
	 * @var string StartTime        
	 *     说明：节目开播时间北京时间 (YYYYMMDDHHMMSS)，如果只有 6 字 节 ， 则 格 式 为HH24MISS ， 需 要 和StartDate 一起拼装成 14字 节 的 北 京 时 间YYYYMMDDHHMMSS 
	 * @var string EndTime         
	 *     说明：节目结束时间北京时间(YYYYMMDDHHMMSS) 悦 me 必选
	 * @var string Duration          
	 *     说明：播放时长（单位：秒） 节目时长(HH24MISS) 采用StartDate属性时必填 
	 * @var string CPContentID           
	 *     说明：CP 对于该段录制内容的唯一标识
	 * @var string Description            
	 *     说明：描述信息
	 * @var string Domain            
	 *     说明： 发布到融合 CDN 后的服务域和服务协议 
	 *     注释： 包含 ServiceDomain 和ProtocolType 两层含义，为 4 字节整数，其中最高三 个 字 节 为ServiceDomain，表示预分发网络域，最低 3 位取值如下： 
	 *               0x000001-IPTV 网络 
	 *               0x000002-互联网网络 
	 *               0x000004-移动网络 
	 *               0x000008~0x000080-预留 
	 *            最 低 一 个 字 节 为ProtocolType，表示服务协议，最低 4 位取值如下：
	 *               0x01-IPTV TS RTSP， 
	 *               0x02-HPD 
	 *               0x04-ISMA RTSP 
	 *               0x08-HLS。 
	 *            如果 Domain 为空，则采用该 CMSID 缺省分发域，缺省分发域可配置
	 * @var string Hotdegree             
	 *     说明： 发布到融合 CDN 时使用热度表示
	 *     注释： 0（普通），1（高） 缺省为 0，该字段值越大，表示优先级越高。CDN 应根据优先级安排处理顺序
	 */
	private function make_playbill_xml($arr_playbill,$arr_playbill_ex=null,$str_Action)
	{
		$str_EndTime = date('YmdHis',(strtotime($arr_playbill['nns_begin_time'])+$arr_playbill['nns_time_len']));
		$str_StartDate = str_replace('-', '', trim(substr($arr_playbill['nns_begin_time'], 0,10)));
		$str_StartTime = str_replace(':', '', trim(substr($arr_playbill['nns_begin_time'], 11,8)));
		$str_time_len = gmstrftime("%H%M%S",$arr_playbill['nns_time_len']);
		$str_xml ='<Object ElementType="ScheduleRecord" ContentID="'.$arr_playbill['nns_cdn_playbill_guid'].'" PhysicalChannelID="'.$arr_playbill['nns_cdn_live_guid'].'" Action="'.$str_Action.'">';
		$str_xml.=	'<Property Name="Name"><![CDATA['.$arr_playbill['nns_name'].']]></Property>';
		$str_xml.=	'<Property Name="StartDate"><![CDATA['.$str_StartDate.']]></Property>';
		$str_xml.=	'<Property Name="StartTime"><![CDATA['.$str_StartTime.']]></Property>';
		$str_xml.=	'<Property Name="EndTime"><![CDATA['.$str_EndTime.']]></Property>';
		$str_xml.= 	'<Property Name="Duration"><![CDATA['.$str_time_len.']]></Property>';
		$str_xml.= 	'<Property Name="CPContentID"><![CDATA['.$arr_playbill['nns_import_source'].']]></Property>';
		$str_xml.= 	'<Property Name="Description"><![CDATA['.$arr_playbill['nns_summary'].']]></Property>';
		$str_xml.= 	'<Property Name="Domain"><![CDATA[]]></Property>';
		$str_xml.= 	'<Property Name="HotDgree"><![CDATA[0]]></Property>';
		$str_xml.='</Object>';
		return $str_xml;
	}
	
	/**
	 * 生成file xml
	 * @param unknown $arr_file
	 * @param string $arr_file_ex
	 * @var string Description            
	 *     说明：描述信息
	 * @var string FileURL
	 *     说明： 媒体文件 URL ftp://username:password@ip:port/...  标准 FTP协议 或者 http:// ip:port/... 
	 *     注释： 对托管内容注入方式下填写，支持 FTP 和 HTTP 协议获取内容 对非托管内容预注入方式下 则 为 空 ， 填 写ServiceURL 属性 当FileURL和ServiceURL均有值时，
	 *	               首选采用该属性值去获取内容。注入 HLS 内容时，如果属性值为 m3u8 文件结尾，CDN 获取内容时，获取整个 m3u8 的上级目录
	 * @var string ServiceURL
	 *     说明： 内容服务 URL
	 *     注释： 需要指定 CDN 服务 URL 的内容注入/预注入内容接入或实时回源接入内容删除时填写。为 CDN 提供服务的内容服务调用 URL，当 ServiceURL 有值时，该URL 前缀信息应和 CDN 服务配置
     *           表中对应域内容提供 商 的PlaybackURLprefix 保持一致，用户需要基于本URL 访问注入内容
	 * @var string FileSize 
	 *     说明： 文件大小，单位为 Byte
	 * @var string Domain            
	 *     说明： 发布到融合 CDN 后的服务域和服务协议 
	 *     注释： 包含 ServiceDomain 和ProtocolType 两层含义，为 4 字节整数，其中最高三 个 字 节 为ServiceDomain，表示预分发网络域，最低 3 位取值如下： 
	 *               0x000001-IPTV 网络 
	 *               0x000002-互联网网络 
	 *               0x000004-移动网络 
	 *               0x000008~0x000080-预留 
	 *            最 低 一 个 字 节 为ProtocolType，表示服务协议，最低 4 位取值如下：
	 *               0x01-IPTV TS RTSP， 
	 *               0x02-HPD 
	 *               0x04-ISMA RTSP 
	 *               0x08-HLS。 
	 *            如果 Domain 为空，则采用该 CMSID 缺省分发域，缺省分发域可配置
	 * @var string Hotdegree             
	 *     说明： 发布到融合 CDN 时使用热度表示
	 *     注释： 0（普通），1（高） 缺省为 0，该字段值越大，表示优先级越高。CDN 应根据优先级安排处理顺序
	 * @var string deliverTime              
	 *     说明： 内容分发时间
	 *     注释： 北京时间，采用十四位的字 符 串 格 式YYYYMMDDHHMMSS，例如： 20040101123000 ， 代 表 2004 年 1 月 1 日 12点 30 分 00 秒。说明 如果不携带该字段，表示立即分发
	 */
	private function make_file_xml($arr_file,$arr_file_ex=null)
	{
	    $str_xml ='<Object ElementType="File" PhysicalContentID="'.$arr_file['nns_cdn_file_guid'].'" Action="'.$arr_file['nns_action'].'">';
	    $str_xml.= 	'<Property Name="Description"><![CDATA['.$arr_file['nns_description'].']]></Property>';
		$str_xml.=	'<Property Name="FileURL"><![CDATA['.$arr_file['nns_file_url'].']]></Property>';
		$str_xml.=	'<Property Name="ServiceURL"><![CDATA['.$arr_file['nns_service_url'].']]></Property>';
		$str_xml.=	'<Property Name="FileSize"><![CDATA['.$arr_file['nns_file_size'].']]></Property>';
	    $str_xml.= 	'<Property Name="Domain"><![CDATA[257]]></Property>';
	    $str_xml.= 	'<Property Name="HotDgree"><![CDATA[0]]></Property>';
		$str_xml.=	'<Property Name="deliverTime"><![CDATA['.date('YmdHis').']]></Property>';
	    $str_xml.='</Object>';
	    return $str_xml;
	}
	
	/**
	 * 栏目发布
	 */
	public function do_category()
	{
	    $action = 'REGIST';
	    $xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
	    $xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
	    $xml_str .=    '<Objects>';
	    $xml_str .=        '<Object ElementType="Category" ContentID="'.$this->arr_params['category'].'" Action="'.$action.'">';
	    $xml_str .= 	       '<Property Name="ParentID"><![CDATA['.$this->arr_params['parent_category'].']]></Property>';
	    $xml_str .=	           '<Property Name="Name"><![CDATA['.$this->arr_params['name'].']]></Property>';
	    $xml_str .=	           '<Property Name="Sequence"><![CDATA[0]]></Property>';
	    $xml_str .=	           '<Property Name="Status"><![CDATA[1]]></Property>';
	    $xml_str .= 	       '<Property Name="Description"><![CDATA[]]></Property>';
	    $xml_str .=        '</Object>';
	    $xml_str .= 	'</Objects>';
	    $xml_str .= '</ADI>';
	    $file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'vod_category' . '_' . $action . '_' .  rand(100, 999) . '.xml';
	    $c2_info = array(
	        'nns_task_type'=>'Category',
	        'nns_task_id'=> np_guid_rand(),
	        'nns_task_name'=> "[Category][{$this->arr_params['category']}][{$this->arr_params['name']}]",
	        'nns_action'=>	$action,
	        'nns_url' => $file_name,
	        'nns_content' => $xml_str,
	        'nns_desc' => 'Category,'.$action,
	    );
	    return $this->execute_standard_c2($c2_info);
	}
	
	
	/**
	 * 点播主媒资CDN
	 * @param unknown $movie_id
	 */
	public function do_video()
	{
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_video_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $video_id = $this->arr_params['bk_c2_task_info']['nns_ref_id'];
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	     
	    $result_video = nl_vod::get_video_info_by_id($this->obj_dc, $video_id);
	    if($result_video['ret'] !=0)
	    {
	        return $result_video;
	    }
	    if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['bk_c2_task_info']['nns_id']}'", $this->obj_dc->db());
	        return $result_video;
	    }
	    $result_video = $result_video['data_info'][0];
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $video_id,$result_video);
	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        return $result_video_import_info;
	    }
	    $result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
	    
	    $action = ($this->arr_params['bk_c2_task_info']['nns_action'] !='destroy') ? ($this->arr_params['bk_c2_task_info']['nns_action'] == 'update') ? 'UPDATE' : 'REGIST' : 'DELETE';
// 	    $action = ($result_video['nns_deleted'] !=1) ? ($result_video['nns_modify_time'] > $result_video['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
	    $result_picture = $this->make_image($result_video, 'Series', $result_video['nns_cdn_video_guid'], $action);
	    $xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
	    $xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
	    $xml_str .= 	'<Objects>';
	    $xml_str .=     $this->make_vod_xml($result_video,$action);
	    if(isset($result_picture['picture']) && strlen($result_picture['picture']) >0)
	    {
	        $xml_str .= $result_picture['picture'];
	    }
	    $xml_str .= 	'</Objects>';
	    if(isset($result_picture['mapping']) && strlen($result_picture['mapping']) >0)
	    {
	        $xml_str .= 	'<Mappings>';
	        $xml_str .= $result_picture['mapping'];
	        $xml_str .= 	'</Mappings>';
	    }
	    $xml_str .= '</ADI>';
	    $file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'vod_video' . '_' . $action . '_' .  rand(100, 999) . '.xml';
		$c2_info = array(
	        'nns_task_type'=>'Series',
	        'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	        'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
	        'nns_action'=>	$action,
	        'nns_url' => $file_name,
	        'nns_content' => $xml_str,
	        'nns_desc' => 'Series,'.$action,
	    );
	    return $this->execute_standard_c2($c2_info);
	}
	
	
	/**
	 * 点播分集CDN
	 * @param unknown $movie_id
	 */
	public function do_index()
	{
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_index_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $index_id = $this->arr_params['bk_c2_task_info']['nns_ref_id'];
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	    $result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $index_id);
	    if($result_index['ret'] !=0)
	    {
	        return $result_index;
	    }
	    if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['bk_c2_task_info']['nns_id']}'", $this->obj_dc->db());
	        return $result_index;
	    }
	    $result_index = $result_index['data_info'][0];
	    $result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $index_id,$result_index);
	    if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
	    {
	        return $result_index_import_info;
	    }
	    $result_index['nns_cdn_index_guid'] = $result_index_import_info['data_info'];
	    
	    
	    $result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_index['nns_vod_id']);
	    if($result_video['ret'] !=0)
	    {
	        return $result_video;
	    }
	    if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['bk_c2_task_info']['nns_id']}'", $this->obj_dc->db());
	        return $result_video;
	    }
	    $result_video = $result_video['data_info'][0];
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_index['nns_vod_id'],$result_video);
	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        return $result_video_import_info;
	    }
	    $result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
	    $action = ($this->arr_params['bk_c2_task_info']['nns_action'] !='destroy') ? ($this->arr_params['bk_c2_task_info']['nns_action'] == 'update') ? 'UPDATE' : 'REGIST' : 'DELETE';
// 	    $action = ($result_index['nns_deleted'] !=1) ? ($result_index['nns_modify_time'] > $result_index['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
	    $result_picture = $this->make_image($result_index, 'Program', $result_index['nns_cdn_video_guid'], $action);
	    $result_index['nns_index'] = (isset($result_index['nns_index']) && $result_index['nns_index'] >=0) ? $result_index['nns_index']+1 : 1;
	    $xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
	    $xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
	    $xml_str .= 	'<Objects>';
	    $xml_str .=     $this->make_vod_index_xml($result_index,$action);
	    if(isset($result_picture['picture']) && strlen($result_picture['picture']) >0)
	    {
	        $xml_str .= $result_picture['picture'];
	    }
	    $xml_str .= 	'</Objects>';
	    $xml_str .= 	'<Mappings>';
	    if(isset($result_picture['mapping']) && strlen($result_picture['mapping']) >0)
	    {
	        $xml_str .= $result_picture['mapping'];
	    }
	    $xml_str .=     $this->make_mapping('Series','Program',$result_video['nns_cdn_video_guid'],$result_index['nns_cdn_index_guid'],$action,array('Sequence'=>$result_index['nns_index']));
	    $xml_str .= 	'</Mappings>';
	    $xml_str .= '</ADI>';
	     
	    $file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'vod_index' . '_' . $action . '_' .  rand(100, 999) . '.xml';
	    $c2_info = array(
	        'nns_task_type'=>'Program',
	        'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	        'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
	        'nns_action'=>	$action,
	        'nns_url' => $file_name,
	        'nns_content' => $xml_str,
	        'nns_desc' => 'Program,'.$action,
	    );
	    return $this->execute_standard_c2($c2_info);
	}
	
	
	/**
	 * 片源CDN
	 * @param unknown $movie_id
	 */
	public function do_media()
	{
		if(!isset($this->arr_params['nns_media_id']) || empty($this->arr_params['nns_media_id']))
		{
			return $this->_make_return_data(1,'点播传入参数无nns_media_id字段,参数为:'.var_export($this->arr_params));
		}
		$movie_id = $this->arr_params['nns_media_id'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
		$result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $movie_id);
		if($result_media['ret'] !=0)
		{
			return $result_media;
		}
		if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
		{
			nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
			return $result_media;
		}
		$result_media = $result_media['data_info'];
		$result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $movie_id,$result_media);
		if((int)$this->arr_sp_config['disabled_clip'] !== 2 && (int)$this->arr_sp_config['disabled_clip'] !== 3)
		{
		    $result_media['nns_url'] = ltrim((trim($this->arr_params['nns_file_path'])),'/');
		    
// 		    if(isset($this->arr_sp_config['clip_file_encode_enable']) && $this->arr_sp_config['clip_file_encode_enable'] == '1')
// 		    {
// 		        $arr_temp_pathinfo = pathinfo($result_media['nns_url']);
// 		        global $g_encode_file_ex_type;
// 		        $g_encode_file_ex_type = (isset($g_encode_file_ex_type) && strlen($g_encode_file_ex_type) >0) ? $g_encode_file_ex_type : 'ts';
// 		        $result_media['nns_url'] = $arr_temp_pathinfo['dirname'].'/'.$arr_temp_pathinfo['filename'].'.'.$g_encode_file_ex_type;
// 		        unset($g_encode_file_ex_type);
// 		    }
		    if(isset($this->arr_sp_config['media_ftp']) && !empty($this->arr_sp_config['media_ftp']))
		    {
		        $this->arr_sp_config['media_ftp'] = rtrim((trim($this->arr_sp_config['media_ftp'])),'/');
		        if (stripos($result_media['nns_url'], 'http://') === FALSE && stripos($result_media['nns_url'], 'ftp://') === FALSE)
		        {
		            $result_media['nns_url'] = $this->arr_sp_config['media_ftp'].'/'.$result_media['nns_url'];
		        }
		    }
		}
		else
		{
		    $result_media['nns_url'] = ltrim((trim($result_media['nns_url'])),'/');
		    if(isset($this->arr_sp_config['media_ftp']) && !empty($this->arr_sp_config['media_ftp']))
		    {
		        $this->arr_sp_config['media_ftp'] = rtrim((trim($this->arr_sp_config['media_ftp'])),'/');
		        if (stripos($result_media['nns_url'], 'http://') === FALSE && stripos($result_media['nns_url'], 'ftp://') === FALSE)
		        {
		        	 $result_media['nns_url'] = $this->arr_sp_config['media_ftp'].'/'.$result_media['nns_url'];
		        }
		    }
		}
		
// 		    var_dump($result_media['nns_url']);die;
		if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
		{
			return $result_media_import_info;
		}
		$result_media['nns_cdn_media_guid'] = $result_media_import_info['data_info'];
		
		$result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $result_media['nns_vod_index_id']);
		if($result_index['ret'] !=0)
		{
		    return $result_index;
		}
		if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
		{
		    nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
		    return $result_index;
		}
		$result_index = $result_index['data_info'][0];
		$result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $result_media['nns_vod_index_id'],$result_index);
		if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
		{
		    return $result_index_import_info;
		}
		$result_index['nns_cdn_index_guid'] = $result_index_import_info['data_info'];
		 
		 
		$result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_media['nns_vod_id']);
		if($result_video['ret'] !=0)
		{
		    return $result_video;
		}
		if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
		{
		    nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
		    return $result_video;
		}
		$result_video = $result_video['data_info'][0];
		$result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_index['nns_vod_id'],$result_video);
		if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
		{
		    return $result_video_import_info;
		}
		$result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
		$action = ($this->arr_params['bk_c2_task_info']['nns_action'] !='destroy') ? ($this->arr_params['bk_c2_task_info']['nns_action'] == 'update') ? 'UPDATE' : 'REGIST' : 'DELETE';
// 		$action = ($result_media['nns_deleted'] !=1) ? ($result_media['nns_modify_time'] > $result_media['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
		$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
		$xml_str .= 	'<Objects>';
		$xml_str .=     $this->make_vod_media_xml($result_media,$action);
		$xml_str .= 	'</Objects>';
		$xml_str .= 	'<Mappings>';
		$xml_str .=     $this->make_mapping('Program','Movie',$result_index['nns_cdn_index_guid'],$result_media['nns_cdn_media_guid'],$action);
		if($action != 'DELETE')
		{
		      $xml_str .=     $this->make_mapping('Series','Program',$result_video['nns_cdn_video_guid'],$result_index['nns_cdn_index_guid'],$action);
		}
		$xml_str .= 	'</Mappings>';
		$xml_str .= '</ADI>';
		
		$file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'vod_media' . '_' . $action . '_' .  rand(100, 999) . '.xml';
		$c2_info = array(
				'nns_task_type'=>'Movie',
				'nns_task_id'=> isset($this->arr_params['nns_task_id'])?$this->arr_params['nns_task_id']:null,
				'nns_task_name'=> isset($this->arr_params['nns_task_name'])?$this->arr_params['nns_task_name']:null,
				'nns_action'=>	$action,
				'nns_url' => $file_name,
				'nns_content' => $xml_str,
				'nns_desc' => 'Movie,'.$action,
		);
		return $this->execute_standard_c2($c2_info);
	}
	
	/**
	 * 直播频道处理
	 */
	public function do_live()
	{
	    if(!isset($this->arr_params['nns_channel_id']) && !empty($this->arr_params['nns_channel_id']))
	    {
	        return array(
	            'ret'=>1,
	            'reason'=>'直播频道传入参数无nns_channel_id字段,参数为:'.var_export($this->arr_params),
	        );
	    }
	    $channel_id = $this->arr_params['nns_channel_id'];
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/live.class.php';
	    $result_channel = nl_live::query_by_id($this->obj_dc, $channel_id);
	    if($result_channel['ret'] !=0 || !isset($result_channel['data_info']) || !is_array($result_channel['data_info']) || empty($result_channel['data_info']))
	    {
	        return $result_channel;
	    }
	    $result_channel = $result_channel['data_info'];
	    $result_channel_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'live', $channel_id,$result_channel);
	    if($result_channel_import_info['ret'] !=0 || !isset($result_channel_import_info['data_info']) || strlen($result_channel_import_info['data_info']) <1)
	    {
	        return $result_channel_import_info;
	    }
	    $result_channel['nns_cdn_live_guid'] = $result_channel_import_info['data_info'];
	    $result_channel_ex = nl_live::query_ex_by_id($this->obj_dc,$result_channel['nns_id']);
	    if($result_channel_ex['ret'] !=0)
	    {
	        return $result_channel_ex;
	    }
	    $result_channel_ex = isset($result_channel_ex['data_info']) ? $result_channel_ex['data_info'] : null;
	    $arr_channel_ex=null;
	    if(is_array($result_channel_ex))
	    {
	        foreach ($result_channel_ex as $channel_ex_val)
	        {
	            $arr_channel_ex[$channel_ex_val['nns_key']] = $channel_ex_val['nns_value'];
	        }
	    }
	    $str_Action = ($result_channel['nns_deleted'] !=1) ? ($result_channel['nns_modify_time'] > $result_channel['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
		$result_picture = $this->make_image($result_channel, 'Channel', $result_channel['nns_cdn_live_guid'], $str_Action);
	    $xml_str='';
	    $xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
	    $xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
	    $xml_str .= 	'<Objects>';
	    $xml_str .=     $this->make_live_xml($result_channel,$arr_channel_ex,$str_Action);
	    if(isset($result_picture['picture']) && strlen($result_picture['picture']) >0)
	    {
	        $xml_str .= $result_picture['picture'];
	    }
	    $xml_str .= 	'</Objects>';
	    if(isset($result_picture['mapping']) && strlen($result_picture['mapping']) >0)
	    {
	        $xml_str .= 	'<Mappings>';
	        $xml_str .= $result_picture['mapping'];
	        $xml_str .= 	'</Mappings>';
	    }
	    $xml_str .= '</ADI>';
	    $file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'live' . '_' . $str_Action . '_' .  rand(100, 999) . '.xml';
	    $c2_info = array(
				'nns_task_type'=>'Channel',
				'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
				'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
				'nns_action'=>	$str_Action,
				'nns_url' => $file_name,
				'nns_content' => $xml_str,
				'nns_desc' => 'Channel,'.$str_Action,
		);
		return $this->execute_standard_c2($c2_info);
	}
	
	
	/**
	 * 直播片源CDN
	 * @param unknown $movie_id
	 */
	public function do_live_media()
	{
		if(!isset($this->arr_params['nns_channel_id']) && !empty($this->arr_params['nns_channel_id']))
		{
			return array(
					'ret'=>1,
					'reason'=>'直播片源传入参数无nns_channel_id字段,参数为:'.var_export($this->arr_params),
			);
		}
		if(!isset($this->arr_params['nns_media_id']) && !empty($this->arr_params['nns_media_id']))
		{
		    return array(
		        'ret'=>1,
		        'reason'=>'直播片源传入参数无nns_media_id字段,参数为:'.var_export($this->arr_params),
		    );
		}
		$channel_id = $this->arr_params['nns_channel_id'];
		$media_id = $this->arr_params['nns_media_id'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/live.class.php';
		$result_channel = nl_live::query_by_id($this->obj_dc, $channel_id);
		if($result_channel['ret'] !=0 || !isset($result_channel['data_info']) || !is_array($result_channel['data_info']) || empty($result_channel['data_info']))
		{
			return $result_channel;
		}
		$result_channel = $result_channel['data_info'];
		$result_channel_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'live', $channel_id,$result_channel);
		if($result_channel_import_info['ret'] !=0 || !isset($result_channel_import_info['data_info']) || strlen($result_channel_import_info['data_info']) <1)
		{
			return $result_channel_import_info;
		}
		$result_channel['nns_cdn_channel_guid'] = $result_channel_import_info['data_info'];
		$result_channel_ex = nl_live::query_ex_by_id($this->obj_dc,$result_channel['nns_id']);
		if($result_channel_ex['ret'] !=0)
		{
		    return $result_channel_ex;
		}
		$result_channel_ex = isset($result_channel_ex['data_info']) ? $result_channel_ex['data_info'] : null;
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/live_media.class.php';
		$result_media = nl_live_media::query_by_id($this->obj_dc, $media_id);
		if($result_media['ret'] !=0 || !isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
		{
			return $result_media;
		}
		$result_media = $result_media['data_info'];
		$result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'live_media', $media_id,$result_media);
		if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
		{
		    return $result_media_import_info;
		}
		$result_media['nns_cdn_media_guid'] = $result_media_import_info['data_info'];
		$str_Action = ($result_media['nns_deleted'] !=1) ? ($result_media['nns_modify_time'] > $result_media['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
		
		$xml_str='';
		$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
		$xml_str .= 	'<Objects>';
		$xml_str .=       $this->make_live_media_xml($result_media,$result_channel,$str_Action);
		$xml_str .= 	'</Objects>';
		$xml_str .= 	'<Mappings>';
		$xml_str .=       $this->make_mapping('Channel','PhysicalChannel',$result_channel['nns_cdn_channel_guid'],$result_media['nns_cdn_media_guid'],$str_Action);
		$xml_str .= 	'</Mappings>';
		$xml_str .= '</ADI>';
		
		$file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'live_media' . '_' . $str_Action . '_' .  rand(100, 999) . '.xml';
		$c2_info = array(
		    'nns_task_type'=>'Channel',
		    'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
		    'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
		    'nns_action'=>	$str_Action,
		    'nns_url' => $file_name,
		    'nns_content' => $xml_str,
		    'nns_desc' => 'Channel,'.$str_Action,
		);
		return $this->execute_standard_c2($c2_info);
	}
	
	
	/**
	 * 直播节目单CDN
	 * @param unknown $movie_id
	 */
	public function do_playbill()
	{
		if(!isset($this->arr_params['nns_playbill_id']) && !empty($this->arr_params['nns_playbill_id']))
		{
			return array(
					'ret'=>1,
					'reason'=>'节目单传入参数无nns_playbill_id字段,参数为:'.var_export($this->arr_params),
			);
		}
		$playbill_id = $this->arr_params['nns_playbill_id'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/live.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/playbill.class.php';
		$result_playbill = nl_playbill::query_by_id($this->obj_dc, $playbill_id);
		if($result_playbill['ret'] !=0 || !isset($result_playbill['data_info']) || !is_array($result_playbill['data_info']) || empty($result_playbill['data_info']))
		{
			nl_c2_task::edit($this->obj_dc, array('nns_state'=>7), $this->arr_params['bk_c2_task_info']['nns_id']);
			$result_playbill['ret'] = 1;
		    return $result_playbill;
		}
		$result_playbill = $result_playbill['data_info'];
		$result_live = nl_live::query_by_id($this->obj_dc, $result_playbill['nns_live_id']);
		if($result_live['ret'] !=0 || !isset($result_live['data_info']) || !is_array($result_live['data_info']) || empty($result_live['data_info']))
		{
			$result_live['ret'] = 1;
			return $result_live;
		}
		$result_live = $result_live['data_info'];
		$result_playbill_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'playbill', $playbill_id,$result_playbill);
		if($result_playbill_import_info['ret'] !=0 || !isset($result_playbill_import_info['data_info']) || strlen($result_playbill_import_info['data_info']) <1)
		{
			return $result_playbill_import_info;
		}
		$result_live_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'live', $result_live['nns_id'],$result_live);
		if($result_live_import_info['ret'] !=0 || !isset($result_live_import_info['data_info']) || strlen($result_live_import_info['data_info']) <1)
		{
			return $result_live_import_info;
		}
		$result_playbill['nns_cdn_playbill_guid'] = $result_playbill_import_info['data_info'];
		$result_playbill['nns_cdn_live_guid'] = $result_live_import_info['data_info'];
		$str_Action = ($result_playbill['nns_state'] !=1) ? ($result_playbill['nns_modify_time'] > $result_playbill['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
		$result_picture = $this->make_image($result_playbill, 'ScheduleRecord', $result_playbill['nns_cdn_playbill_guid'], $str_Action);
		$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
		$xml_str .= 	'<Objects>';
		$xml_str .=       $this->make_playbill_xml($result_playbill,null,$str_Action);
		if(isset($result_picture['picture']) && strlen($result_picture['picture']) >0)
		{
		    $xml_str .= $result_picture['picture'];
		}
		$xml_str .= 	'</Objects>';
		$xml_str .= 	'<Mappings>';
		$xml_str .=       $this->make_mapping('Channel','ScheduleRecord',$result_playbill['nns_cdn_live_guid'],$result_playbill['nns_cdn_playbill_guid'],$str_Action);
		if(isset($result_picture['mapping']) && strlen($result_picture['mapping']) >0)
		{
		    $xml_str .= $result_picture['mapping'];
		}
		$xml_str .= 	'</Mappings>';
		$xml_str .= '</ADI>';
		$file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'playbill' . '_' . $str_Action . '_' .  rand(100, 999) . '.xml';
		$c2_info = array(
		    'nns_task_type'=>'Schedule',
		    'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
		    'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
		    'nns_action'=>	$str_Action,
		    'nns_url' => $file_name,
		    'nns_content' => $xml_str,
		    'nns_desc' => 'Schedule,'.$str_Action,
		);
		return $this->execute_standard_c2($c2_info);
	}
}
