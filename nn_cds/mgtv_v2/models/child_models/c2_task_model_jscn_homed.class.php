<?php
/**
 * starcor 江苏茁壮规范ADI CDN注入
 * @author liangpan
 */
class c2_task_model_v2 extends nn_public_model
{
	private $str_cmsid = 'STARCOR';
    
	/**
	 * 组装主媒资信息
	 * @param unknown $arr_info 主媒资基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 */
	private function make_vod_xml($arr_info_ex,$str_Action)
	{
	    $str_Action = (strtolower($str_Action) == 'delete') ? 'Remove' : 'Replace';
	    $arr_img = null;
	    $str_img_ftp = (isset($this->arr_sp_config['img_ftp']) && strlen($this->arr_sp_config['img_ftp'])>0) ? trim($this->arr_sp_config['img_ftp'],'/').'/' : '';
	    $arr_img_index = array('nns_image0','nns_image1','nns_image2','nns_image3','nns_image4','nns_image5','nns_image_v','nns_image_s','nns_image_h');
	    foreach ($arr_img_index as $val)
	    {
	        if(isset($arr_info_ex['video']['base'][$val]) && strlen($arr_info_ex['video']['base'][$val])>0)
	        {
	            $arr_info_ex['video']['base'][$val] = trim($arr_info_ex['video']['base'][$val],'/');
	            $md5_img = md5($str_img_ftp.$arr_info_ex['video']['base'][$val]);
	            $arr_img[$md5_img] = $str_img_ftp.$arr_info_ex['video']['base'][$val];
	        }
	    }
	    $arr_info_ex['index']['base']['nns_index']++;
	    $str_xml  ='<adi:ADI2 xmlns="http://www.cablelabs.com/VODSchema/default" xmlns:adi="http://www.cablelabs.com/VODSchema/adi" xmlns:vod="http://www.cablelabs.com/VODSchema/vod">';
	    $str_xml .=    '<adi:'.$str_Action.'GroupAsset type="VODRelease" product="VOD">';
	    $str_xml .=       '<vod:VODRelease providerID="'.$this->str_cmsid.'" providerType="2" assetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" updateNum="" groupAsset="Y" serialNo="'.$this->arr_params['bk_c2_task_info']['nns_id'].'">';
	    $str_xml .=       '<adi:AssetLifetime startDateTime="2014-10-19" endDateTime="2064-10-19" />';
	    $str_xml .=       '</vod:VODRelease>';
	    $str_xml .=    '</adi:'.$str_Action.'GroupAsset>';
	    
	    $str_xml .=    '<adi:'.$str_Action.'MetadataAsset groupProviderID="'.$this->str_cmsid.'" groupAssetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" type="Title" product="VOD">';
	    $str_xml .=        '<vod:Title providerID="'.$this->str_cmsid.'" assetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" updateNum="1">';
	    $str_xml .=             '<adi:AssetLifetime startDateTime="2014-01-19" endDateTime="2064-01-19" />';
	    $str_xml .=             '<vod:Format />';
	    $str_xml .=             '<vod:StarLevel>0</vod:StarLevel>';
	    $str_xml .=             '<vod:Keyword>'.$arr_info_ex['video']['base']['nns_keyword'].'</vod:Keyword>';
	    $str_xml .=             '<vod:IsAdvertise>0</vod:IsAdvertise>';
	    $str_xml .=             '<vod:Background />';
	    $str_xml .=             '<vod:Year>'.$arr_info_ex['video']['base']['nns_show_time'].'</vod:Year>';
	    $str_xml .=             '<vod:StudioName />';
	    $str_xml .=             '<vod:Actor type="Actor">';
	    $str_xml .=                 '<vod:LastNameFirst>'.$arr_info_ex['video']['base']['nns_actor'].'</vod:LastNameFirst>';
	    $str_xml .=             '</vod:Actor>';
	    $str_xml .=             '<vod:RunTime />';
	    $str_xml .=             '<vod:Actor type="Actress">';
	    $str_xml .=             '<vod:LastNameFirst />';
	    $str_xml .=             '</vod:Actor>';
	    $str_xml .=             '<vod:Awards />';
	    $str_xml .=             '<vod:Actor type="CoActress">';
	    $str_xml .=             '<vod:LastNameFirst />';
	    $str_xml .=             '</vod:Actor>';
	    $str_xml .=             '<vod:MediaFormat />';
	    $str_xml .=             '<vod:Director>';
	    $str_xml .=                 '<vod:LastNameFirst>'.$arr_info_ex['video']['base']['nns_director'].'</vod:LastNameFirst>';
	    $str_xml .=             '</vod:Director>';
	    $str_xml .=             '<vod:CountryOfOrigin>1</vod:CountryOfOrigin>';
	    $str_xml .=             '<vod:IptvDesc />';
	    $str_xml .=             '<vod:Comments />';
	    $str_xml .=             '<vod:TitleFull>'.$arr_info_ex['video']['base']['nns_name'].'</vod:TitleFull>';
	    $str_xml .=             '<vod:Agent />';
	    $str_xml .=             '<vod:Actor type="CoActor">';
	    $str_xml .=                 '<vod:LastNameFirst />';
	    $str_xml .=             '</vod:Actor>';
	    $str_xml .=             '<vod:ShowType>Series</vod:ShowType>';
	    $str_xml .=             '<vod:Priority>2</vod:Priority>';
	    $str_xml .=             '<vod:Status>0</vod:Status>';
	    $str_xml .=             '<vod:Rating />';
	    $str_xml .=             '<vod:SummaryMedium>'.$arr_info_ex['video']['base']['nns_summary'].'</vod:SummaryMedium>';
	    $str_xml .=             '<vod:SummaryShort>'.$arr_info_ex['video']['base']['nns_summary'].'</vod:SummaryShort>';
	    $str_xml .=             '<vod:Language>'.$arr_info_ex['video']['base']['nns_language'].'</vod:Language>';
	    $str_xml .=             '<vod:Superviser />';
	    $str_xml .=             '<vod:EnglishDesc />';
	    $str_xml .=             '<vod:Extend>';
	    $str_xml .=                 '<vod:Director_en></vod:Director_en>';
	    $str_xml .=                 '<vod:Name_en></vod:Name_en>';
	    $str_xml .=                 '<vod:Actor_en></vod:Actor_en>';
	    $str_xml .=                 '<vod:Description_en></vod:Description_en>';
	    $str_xml .=             '</vod:Extend>';
	    $str_xml .=        '</vod:Title>';
	    $str_xml .=    '</adi:'.$str_Action.'MetadataAsset>';
	    $str_xml .=    '<adi:'.$str_Action.'MetadataAsset groupProviderID="'.$this->str_cmsid.'" groupAssetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" type="CategoryPath" product="VOD">';
	    $str_xml .=       '<vod:CategoryPath providerID="'.$this->str_cmsid.'" assetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" updateNum="1">';
	    $str_xml .=           '<adi:AssetLifetime startDateTime="2014-10-19" endDateTime="2064-10-19" />';
	    $str_xml .=           '<vod:Category>'.$arr_info_ex['category']['base']['nns_name'].'</vod:Category>';
	    $str_xml .=           '<vod:Classification>'.$arr_info_ex['category']['base']['nns_name'].'</vod:Classification>';
	    $str_xml .=       '</vod:CategoryPath>';
	    $str_xml .=    '</adi:'.$str_Action.'MetadataAsset>';
	    $str_xml .=    '<adi:'.$str_Action.'MetadataAsset groupProviderID="'.$this->str_cmsid.'" groupAssetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" type="Copyright" product="VOD">';
	    $str_xml .=       '<vod:Copyright providerID="'.$this->str_cmsid.'" assetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" updateNum="1">';
	    $str_xml .=           '<adi:AssetLifetime startDateTime="2014-10-19" endDateTime="2064-10-19" />';
	    $str_xml .=           '<vod:LicenseType>0</vod:LicenseType>';
	    $str_xml .=           '<vod:TransferAgain>0</vod:TransferAgain>';
	    $str_xml .=           '<vod:CopyType>1</vod:CopyType>';
	    $str_xml .=           '<vod:OriginalLicenseCompany />';
	    $str_xml .=           '<vod:Transfer3rd>0</vod:Transfer3rd>';
	    $str_xml .=           '<vod:CopyName />';
	    $str_xml .=           '<vod:PublishNo />';
	    $str_xml .=           '<vod:AuthorserilNo />';
	    $str_xml .=           '<vod:VideoLicense />';
	    $str_xml .=           '<vod:CopyIdea />';
	    $str_xml .=           '<vod:CountryNo />';
	    $str_xml .=           '<vod:PublicationDate />';
	    $str_xml .=           '<vod:CopyLicense />';
	    $str_xml .=           '<vod:CopySerilNo />';
	    $str_xml .=           '<vod:CopyLicenser />';
	    $str_xml .=           '<vod:TransferLicenseCompany />';
	    $str_xml .=        '</vod:Copyright>';
	    $str_xml .=    '</adi:'.$str_Action.'MetadataAsset>';
	    
	    if(!empty($arr_img) && is_array($arr_img))
	    {
	        foreach ($arr_img as $key=>$val)
	        {
	            $str_xml .=    '<adi:'.$str_Action.'ContentAsset type="Image" metadataOnly="N" fileName="'.$key.'" fileSize="47969" mD5CheckSum="475436d950b1570da08a545c7cd94326" Original_Asset_ID="">';
	            $str_xml .=       '<vod:Image providerID="'.$this->str_cmsid.'" assetID="'.$key.'" updateNum="1" fileName="'.$key.'" fileSize="47969" mD5CheckSum="475436d950b1570da08a545c7cd94326" transferContentURL="'.$val.'" imageEncodingProfile="JPG/JPEG">';
	            $str_xml .=           '<adi:AssetLifetime startDateTime="2014-01-19" endDateTime="2064-01-19" />';
	            $str_xml .=           '<vod:Caption>'.$key.'</vod:Caption>';
	            $str_xml .=           '<vod:MimeType>25</vod:MimeType>';
	            $str_xml .=           '<vod:FileType>3</vod:FileType>';
	            $str_xml .=           '<vod:ServiceType>1</vod:ServiceType>';
	            $str_xml .=           '<vod:ColorType>RGB</vod:ColorType>';
	            $str_xml .=           '<vod:Usage>6</vod:Usage>';
	            $str_xml .=           '<vod:ImagePixels horizontalPixels="410" verticalPixels="543" />';
	            $str_xml .=       '</vod:Image>';
	            $str_xml .=   '</adi:'.$str_Action.'ContentAsset>';
	        }
	        foreach ($arr_img as $key=>$val)
	        {
	            $str_xml .=    '<adi:AssociateContent type="Image" effectiveDate="" groupProviderID="'.$this->str_cmsid.'" groupAssetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" providerID="'.$this->str_cmsid.'" assetID="'.$key.'" />';
	        }
	    }
	    $str_xml .='</adi:ADI2>';
	    return $str_xml;
	}
	
	/**
	 * 组装分集信息
	 * @param unknown $arr_info 片源基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 */
	private function make_vod_index_xml($arr_info_ex,$str_Action)
	{
		$str_Action = (strtolower($str_Action) == 'delete') ? 'Remove' : 'Replace';
	    $arr_img = null;
		$str_img_ftp = (isset($this->arr_sp_config['img_ftp']) && strlen($this->arr_sp_config['img_ftp'])>0) ? trim($this->arr_sp_config['img_ftp'],'/').'/' : '';
		$arr_img_index = array('nns_image0','nns_image1','nns_image2','nns_image3','nns_image4','nns_image5','nns_image_v','nns_image_s','nns_image_h');
		foreach ($arr_img_index as $val)
		{
		    if(isset($arr_info_ex['video']['base'][$val]) && strlen($arr_info_ex['video']['base'][$val])>0)
		    {
		        $arr_info_ex['video']['base'][$val] = trim($arr_info_ex['video']['base'][$val],'/');
		        $md5_img = md5($str_img_ftp.$arr_info_ex['video']['base'][$val]);
		        $arr_img[$md5_img] = $str_img_ftp.$arr_info_ex['video']['base'][$val];
		    }
		}
		$arr_info_ex['index']['base']['nns_index']++;
		$str_xml  ='<adi:ADI2 xmlns="http://www.cablelabs.com/VODSchema/default" xmlns:adi="http://www.cablelabs.com/VODSchema/adi" xmlns:vod="http://www.cablelabs.com/VODSchema/vod">';
		$str_xml .=    '<adi:'.$str_Action.'GroupAsset type="VODRelease" product="VOD">';
		$str_xml .=       '<vod:VODRelease providerID="'.$this->str_cmsid.'" providerType="2" assetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" updateNum="" groupAsset="Y" serialNo="'.$this->arr_params['bk_c2_task_info']['nns_id'].'">';
		$str_xml .=       '<adi:AssetLifetime startDateTime="2014-10-19" endDateTime="2064-10-19" />';
		$str_xml .=       '</vod:VODRelease>';
		$str_xml .=    '</adi:'.$str_Action.'GroupAsset>';
		$str_xml .=    '<adi:'.$str_Action.'GroupAsset type="VODRelease" product="VOD">';
		$str_xml .=        '<vod:VODRelease providerID="'.$this->str_cmsid.'" providerType="2" assetID="'.$arr_info_ex['index']['base']['nns_cdn_index_guid'].'" updateNum="" groupAsset="Y" serialNo="'.$this->arr_params['bk_c2_task_info']['nns_id'].'">';
		$str_xml .=           '<adi:AssetLifetime startDateTime="2014-11-24" endDateTime="2064-11-24" />';
		$str_xml .=        '</vod:VODRelease>';
		$str_xml .=    '</adi:'.$str_Action.'GroupAsset>';
		$str_xml .=    '<adi:'.$str_Action.'MetadataAsset groupProviderID="'.$this->str_cmsid.'" groupAssetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" type="Title" product="VOD">';
		$str_xml .=        '<vod:Title providerID="'.$this->str_cmsid.'" assetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" updateNum="1">';
		$str_xml .=             '<adi:AssetLifetime startDateTime="2014-01-19" endDateTime="2064-01-19" />';
		$str_xml .=             '<vod:Format />';
		$str_xml .=             '<vod:StarLevel>0</vod:StarLevel>';
		$str_xml .=             '<vod:Keyword>'.$arr_info_ex['video']['base']['nns_keyword'].'</vod:Keyword>';
		$str_xml .=             '<vod:IsAdvertise>0</vod:IsAdvertise>';
		$str_xml .=             '<vod:Background />';
		$str_xml .=             '<vod:Year>'.$arr_info_ex['video']['base']['nns_show_time'].'</vod:Year>';
		$str_xml .=             '<vod:StudioName />';
		$str_xml .=             '<vod:Actor type="Actor">';
		$str_xml .=                 '<vod:LastNameFirst>'.$arr_info_ex['video']['base']['nns_actor'].'</vod:LastNameFirst>';
		$str_xml .=             '</vod:Actor>';
		$str_xml .=             '<vod:RunTime />';
		$str_xml .=             '<vod:Actor type="Actress">';
		$str_xml .=             '<vod:LastNameFirst />';
		$str_xml .=             '</vod:Actor>';
		$str_xml .=             '<vod:Awards />';
		$str_xml .=             '<vod:Actor type="CoActress">';
		$str_xml .=             '<vod:LastNameFirst />';
		$str_xml .=             '</vod:Actor>';
		$str_xml .=             '<vod:MediaFormat />';
		$str_xml .=             '<vod:Director>';
		$str_xml .=                 '<vod:LastNameFirst>'.$arr_info_ex['video']['base']['nns_director'].'</vod:LastNameFirst>';
		$str_xml .=             '</vod:Director>';
		$str_xml .=             '<vod:CountryOfOrigin>1</vod:CountryOfOrigin>';
		$str_xml .=             '<vod:IptvDesc />';
		$str_xml .=             '<vod:Comments />';
		$str_xml .=             '<vod:TitleFull>'.$arr_info_ex['video']['base']['nns_name'].'</vod:TitleFull>';
		$str_xml .=             '<vod:Agent />';
		$str_xml .=             '<vod:Actor type="CoActor">';
		$str_xml .=                 '<vod:LastNameFirst />';
		$str_xml .=             '</vod:Actor>';
		$str_xml .=             '<vod:ShowType>Series</vod:ShowType>';
		$str_xml .=             '<vod:Priority>2</vod:Priority>';
		$str_xml .=             '<vod:Status>0</vod:Status>';
		$str_xml .=             '<vod:Rating />';
		$str_xml .=             '<vod:SummaryMedium>'.$arr_info_ex['video']['base']['nns_summary'].'</vod:SummaryMedium>';
		$str_xml .=             '<vod:SummaryShort>'.$arr_info_ex['video']['base']['nns_summary'].'</vod:SummaryShort>';
		$str_xml .=             '<vod:Language>'.$arr_info_ex['video']['base']['nns_language'].'</vod:Language>';
		$str_xml .=             '<vod:Superviser />';
		$str_xml .=             '<vod:EnglishDesc />';
		$str_xml .=             '<vod:Extend>';
		$str_xml .=                 '<vod:Director_en></vod:Director_en>';
		$str_xml .=                 '<vod:Name_en></vod:Name_en>';
		$str_xml .=                 '<vod:Actor_en></vod:Actor_en>';
		$str_xml .=                 '<vod:Description_en></vod:Description_en>';
		$str_xml .=             '</vod:Extend>';
		$str_xml .=        '</vod:Title>';
		$str_xml .=    '</adi:'.$str_Action.'MetadataAsset>';
		$str_xml .=    '<adi:'.$str_Action.'MetadataAsset groupProviderID="'.$this->str_cmsid.'" groupAssetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" type="CategoryPath" product="VOD">';
		$str_xml .=       '<vod:CategoryPath providerID="'.$this->str_cmsid.'" assetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" updateNum="1">';
		$str_xml .=           '<adi:AssetLifetime startDateTime="2014-10-19" endDateTime="2064-10-19" />';
		$str_xml .=           '<vod:Category>'.$arr_info_ex['category']['base']['nns_name'].'</vod:Category>';
		$str_xml .=           '<vod:Classification>'.$arr_info_ex['category']['base']['nns_name'].'</vod:Classification>';
		$str_xml .=       '</vod:CategoryPath>';
		$str_xml .=    '</adi:'.$str_Action.'MetadataAsset>';
		$str_xml .=    '<adi:'.$str_Action.'MetadataAsset groupProviderID="'.$this->str_cmsid.'" groupAssetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" type="Copyright" product="VOD">';
		$str_xml .=       '<vod:Copyright providerID="'.$this->str_cmsid.'" assetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" updateNum="1">';
		$str_xml .=           '<adi:AssetLifetime startDateTime="2014-10-19" endDateTime="2064-10-19" />';
		$str_xml .=           '<vod:LicenseType>0</vod:LicenseType>';
		$str_xml .=           '<vod:TransferAgain>0</vod:TransferAgain>';
		$str_xml .=           '<vod:CopyType>1</vod:CopyType>';
		$str_xml .=           '<vod:OriginalLicenseCompany />';
		$str_xml .=           '<vod:Transfer3rd>0</vod:Transfer3rd>';
		$str_xml .=           '<vod:CopyName />';
		$str_xml .=           '<vod:PublishNo />';
		$str_xml .=           '<vod:AuthorserilNo />';
		$str_xml .=           '<vod:VideoLicense />';
		$str_xml .=           '<vod:CopyIdea />';
		$str_xml .=           '<vod:CountryNo />';
		$str_xml .=           '<vod:PublicationDate />';
		$str_xml .=           '<vod:CopyLicense />';
		$str_xml .=           '<vod:CopySerilNo />';
		$str_xml .=           '<vod:CopyLicenser />';
		$str_xml .=           '<vod:TransferLicenseCompany />';
		$str_xml .=        '</vod:Copyright>';
		$str_xml .=    '</adi:'.$str_Action.'MetadataAsset>';
		$str_xml .=    '<adi:'.$str_Action.'MetadataAsset groupProviderID="'.$this->str_cmsid.'" groupAssetID="'.$arr_info_ex['index']['base']['nns_cdn_index_guid'].'" type="Title" product="VOD">';
		$str_xml .=        '<vod:Title providerID="'.$this->str_cmsid.'" assetID="'.$arr_info_ex['index']['base']['nns_cdn_index_guid'].'" updateNum="1">';
		$str_xml .=             '<adi:AssetLifetime startDateTime="2014-10-19" endDateTime="2064-10-19" />';
		$str_xml .=             '<vod:Status>0</vod:Status>';
		$str_xml .=             '<vod:MetaLanguage />';
		$str_xml .=             '<vod:TitleFull>'.$arr_info_ex['video']['base']['nns_name'].'[第'.$arr_info_ex['index']['base']['nns_index'].'集]'.'</vod:TitleFull>';
		$str_xml .=             '<vod:RunTime>1</vod:RunTime>';
		$str_xml .=             '<vod:SummaryMedium>'.$arr_info_ex['video']['base']['nns_name'].'[第'.$arr_info_ex['index']['base']['nns_index'].'集]'.'</vod:SummaryMedium>';
		$str_xml .=             '<vod:SearchName />';
		$str_xml .=             '<vod:EnglishName />';
		$str_xml .=             '<vod:IsAdvertise>0</vod:IsAdvertise>';
		$str_xml .=             '<vod:ShowType>Series</vod:ShowType>';
		$str_xml .=             '<vod:EpisodeID>'.$arr_info_ex['index']['base']['nns_index'].'</vod:EpisodeID>';
		$str_xml .=        '</vod:Title>';
		$str_xml .=    '</adi:'.$str_Action.'MetadataAsset>';
// 		if(!empty($arr_img) && is_array($arr_img))
// 		{
// 		    foreach ($arr_img as $key=>$val)
// 		    {
//         		$str_xml .=    '<adi:AcceptContentAsset type="Image" metadataOnly="N" fileName="'.$key.'" fileSize="47969" mD5CheckSum="475436d950b1570da08a545c7cd94326" Original_Asset_ID="">';
//         		$str_xml .=       '<vod:Image providerID="'.$this->str_cmsid.'" assetID="'.$key.'" updateNum="1" fileName="'.$key.'" fileSize="47969" mD5CheckSum="475436d950b1570da08a545c7cd94326" transferContentURL="'.$val.'" imageEncodingProfile="JPG/JPEG">';
//         		$str_xml .=           '<adi:AssetLifetime startDateTime="2014-01-19" endDateTime="2064-01-19" />';
//         		$str_xml .=           '<vod:Caption>'.$key.'</vod:Caption>';
//         		$str_xml .=           '<vod:MimeType>25</vod:MimeType>';
//         		$str_xml .=           '<vod:FileType>3</vod:FileType>';
//         		$str_xml .=           '<vod:ServiceType>1</vod:ServiceType>';
//         		$str_xml .=           '<vod:ColorType>RGB</vod:ColorType>';
//         		$str_xml .=           '<vod:Usage>6</vod:Usage>';
//         		$str_xml .=           '<vod:ImagePixels horizontalPixels="410" verticalPixels="543" />';
//         		$str_xml .=       '</vod:Image>';
//         		$str_xml .=   '</adi:AcceptContentAsset>';
// 		    }
// 		}
		$str_xml .=    '<adi:AssociateGroup effectiveDate="" groupProviderID="'.$this->str_cmsid.'" providerID="'.$this->str_cmsid.'" assetID="'.$arr_info_ex['index']['base']['nns_cdn_index_guid'].'" sourceGroupAssetID="'.$arr_info_ex['index']['base']['nns_cdn_index_guid'].'" targetGroupAssetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" />';
// 		if(!empty($arr_img) && is_array($arr_img))
// 		{
// 		    foreach ($arr_img as $key=>$val)
// 		    {
// 		        $str_xml .=    '<adi:AssociateContent type="Image" effectiveDate="" groupProviderID="'.$this->str_cmsid.'" groupAssetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" providerID="'.$this->str_cmsid.'" assetID="'.$key.'" />';
// 		    }
// 		}
		$str_xml .='</adi:ADI2>';
		return $str_xml;
	}
	
	/**
	 * 组装片源信息
	 * @param unknown $arr_info 片源基本信息
	 * @param string $arr_info_ex 扩展数据
	 * @return string
	 */
	private function make_vod_media_xml($arr_info,$arr_info_ex=null)
	{
		$str_Action = ($arr_info['nns_deleted'] !=1) ? 'Accept' : 'Remove';
		$arr_img = null;
		$str_img_ftp = (isset($this->arr_sp_config['img_ftp']) && strlen($this->arr_sp_config['img_ftp'])>0) ? trim($this->arr_sp_config['img_ftp'],'/').'/' : '';
		$arr_img_index = array('nns_image0','nns_image1','nns_image2','nns_image3','nns_image4','nns_image5','nns_image_v','nns_image_s','nns_image_h');
		foreach ($arr_img_index as $val)
		{
		    if(isset($arr_info_ex['video']['base'][$val]) && strlen($arr_info_ex['video']['base'][$val])>0)
		    {
		        $arr_info_ex['video']['base'][$val] = trim($arr_info_ex['video']['base'][$val],'/');
		        $md5_img = md5($str_img_ftp.$arr_info_ex['video']['base'][$val]);
		        $arr_img[$md5_img] = $str_img_ftp.$arr_info_ex['video']['base'][$val];
		    }
		}
		$arr_info_ex['index']['base']['nns_index']++;
		$str_xml  ='<adi:ADI2 xmlns="http://www.cablelabs.com/VODSchema/default" xmlns:adi="http://www.cablelabs.com/VODSchema/adi" xmlns:vod="http://www.cablelabs.com/VODSchema/vod">';
		$str_xml .=    '<adi:OpenGroupAsset type="VODRelease" product="VOD">';
		$str_xml .=       '<vod:VODRelease providerID="'.$this->str_cmsid.'" providerType="2" assetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" updateNum="" groupAsset="Y" serialNo="'.$this->arr_params['bk_c2_task_info']['nns_id'].'">';
		$str_xml .=       '<adi:AssetLifetime startDateTime="2014-10-19" endDateTime="2064-10-19" />';
		$str_xml .=       '</vod:VODRelease>';
		$str_xml .=    '</adi:OpenGroupAsset>';
		$str_xml .=    '<adi:OpenGroupAsset type="VODRelease" product="VOD">';
		$str_xml .=        '<vod:VODRelease providerID="'.$this->str_cmsid.'" providerType="2" assetID="'.$arr_info_ex['index']['base']['nns_cdn_index_guid'].'" updateNum="" groupAsset="Y" serialNo="'.$this->arr_params['bk_c2_task_info']['nns_id'].'">';
		$str_xml .=           '<adi:AssetLifetime startDateTime="2014-11-24" endDateTime="2064-11-24" />';
		$str_xml .=        '</vod:VODRelease>';
		$str_xml .=    '</adi:OpenGroupAsset>';
		$str_xml .=    '<adi:AddMetadataAsset groupProviderID="'.$this->str_cmsid.'" groupAssetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" type="Title" product="VOD">';
		$str_xml .=        '<vod:Title providerID="'.$this->str_cmsid.'" assetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" updateNum="1">';
		$str_xml .=             '<adi:AssetLifetime startDateTime="2014-01-19" endDateTime="2064-01-19" />';
		$str_xml .=             '<vod:Format />';
		$str_xml .=             '<vod:StarLevel>0</vod:StarLevel>';
		$str_xml .=             '<vod:Keyword>'.$arr_info_ex['video']['base']['nns_keyword'].'</vod:Keyword>';
		$str_xml .=             '<vod:IsAdvertise>0</vod:IsAdvertise>';
		$str_xml .=             '<vod:Background />';
		$str_xml .=             '<vod:Year>'.$arr_info_ex['video']['base']['nns_show_time'].'</vod:Year>';
		$str_xml .=             '<vod:StudioName />';
		$str_xml .=             '<vod:Actor type="Actor">';
		$str_xml .=                 '<vod:LastNameFirst>'.$arr_info_ex['video']['base']['nns_actor'].'</vod:LastNameFirst>';
		$str_xml .=             '</vod:Actor>';
		$str_xml .=             '<vod:RunTime />';
		$str_xml .=             '<vod:Actor type="Actress">';
		$str_xml .=             '<vod:LastNameFirst />';
		$str_xml .=             '</vod:Actor>';
		$str_xml .=             '<vod:Awards />';
		$str_xml .=             '<vod:Actor type="CoActress">';
		$str_xml .=             '<vod:LastNameFirst />';
		$str_xml .=             '</vod:Actor>';
		$str_xml .=             '<vod:MediaFormat />';
		$str_xml .=             '<vod:Director>';
		$str_xml .=                 '<vod:LastNameFirst>'.$arr_info_ex['video']['base']['nns_director'].'</vod:LastNameFirst>';
		$str_xml .=             '</vod:Director>';
		$str_xml .=             '<vod:CountryOfOrigin>1</vod:CountryOfOrigin>';
		$str_xml .=             '<vod:IptvDesc />';
		$str_xml .=             '<vod:Comments />';
		$str_xml .=             '<vod:TitleFull>'.$arr_info_ex['video']['base']['nns_name'].'</vod:TitleFull>';
		$str_xml .=             '<vod:Agent />';
		$str_xml .=             '<vod:Actor type="CoActor">';
		$str_xml .=                 '<vod:LastNameFirst />';
		$str_xml .=             '</vod:Actor>';
		$str_xml .=             '<vod:ShowType>Series</vod:ShowType>';
		$str_xml .=             '<vod:Priority>2</vod:Priority>';
		$str_xml .=             '<vod:Status>0</vod:Status>';
		$str_xml .=             '<vod:Rating />';
		$str_xml .=             '<vod:SummaryMedium>'.$arr_info_ex['video']['base']['nns_summary'].'</vod:SummaryMedium>';
		$str_xml .=             '<vod:SummaryShort>'.$arr_info_ex['video']['base']['nns_summary'].'</vod:SummaryShort>';
		$str_xml .=             '<vod:Language>'.$arr_info_ex['video']['base']['nns_language'].'</vod:Language>';
		$str_xml .=             '<vod:Superviser />';
		$str_xml .=             '<vod:EnglishDesc />';
		$str_xml .=             '<vod:Extend>';
		$str_xml .=                 '<vod:Director_en></vod:Director_en>';
		$str_xml .=                 '<vod:Name_en></vod:Name_en>';
		$str_xml .=                 '<vod:Actor_en></vod:Actor_en>';
		$str_xml .=                 '<vod:Description_en></vod:Description_en>';
		$str_xml .=             '</vod:Extend>';
		$str_xml .=        '</vod:Title>';
		$str_xml .=    '</adi:AddMetadataAsset>';
		$str_xml .=    '<adi:AddMetadataAsset groupProviderID="'.$this->str_cmsid.'" groupAssetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" type="CategoryPath" product="VOD">';
		$str_xml .=       '<vod:CategoryPath providerID="'.$this->str_cmsid.'" assetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" updateNum="1">';
		$str_xml .=           '<adi:AssetLifetime startDateTime="2014-10-19" endDateTime="2064-10-19" />';
		$str_xml .=           '<vod:Category>'.$arr_info_ex['category']['base']['nns_name'].'</vod:Category>';
		$str_xml .=           '<vod:Classification>'.$arr_info_ex['category']['base']['nns_name'].'</vod:Classification>';
		$str_xml .=       '</vod:CategoryPath>';
		$str_xml .=    '</adi:AddMetadataAsset>';
		$str_xml .=    '<adi:AddMetadataAsset groupProviderID="'.$this->str_cmsid.'" groupAssetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" type="Copyright" product="VOD">';
		$str_xml .=       '<vod:Copyright providerID="'.$this->str_cmsid.'" assetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" updateNum="1">';
		$str_xml .=           '<adi:AssetLifetime startDateTime="2014-10-19" endDateTime="2064-10-19" />';
		$str_xml .=           '<vod:LicenseType>0</vod:LicenseType>';
		$str_xml .=           '<vod:TransferAgain>0</vod:TransferAgain>';
		$str_xml .=           '<vod:CopyType>1</vod:CopyType>';
		$str_xml .=           '<vod:OriginalLicenseCompany />';
		$str_xml .=           '<vod:Transfer3rd>0</vod:Transfer3rd>';
		$str_xml .=           '<vod:CopyName />';
		$str_xml .=           '<vod:PublishNo />';
		$str_xml .=           '<vod:AuthorserilNo />';
		$str_xml .=           '<vod:VideoLicense />';
		$str_xml .=           '<vod:CopyIdea />';
		$str_xml .=           '<vod:CountryNo />';
		$str_xml .=           '<vod:PublicationDate />';
		$str_xml .=           '<vod:CopyLicense />';
		$str_xml .=           '<vod:CopySerilNo />';
		$str_xml .=           '<vod:CopyLicenser />';
		$str_xml .=           '<vod:TransferLicenseCompany />';
		$str_xml .=        '</vod:Copyright>';
		$str_xml .=    '</adi:AddMetadataAsset>';
		$str_xml .=    '<adi:AddMetadataAsset groupProviderID="'.$this->str_cmsid.'" groupAssetID="'.$arr_info_ex['index']['base']['nns_cdn_index_guid'].'" type="Title" product="VOD">';
		$str_xml .=        '<vod:Title providerID="'.$this->str_cmsid.'" assetID="'.$arr_info_ex['index']['base']['nns_cdn_index_guid'].'" updateNum="1">';
		$str_xml .=             '<adi:AssetLifetime startDateTime="2014-10-19" endDateTime="2064-10-19" />';
		$str_xml .=             '<vod:Status>0</vod:Status>';
		$str_xml .=             '<vod:MetaLanguage />';
		$str_xml .=             '<vod:TitleFull>'.$arr_info_ex['video']['base']['nns_name'].'[第'.$arr_info_ex['index']['base']['nns_index'].'集]'.'</vod:TitleFull>';
		$str_xml .=             '<vod:RunTime>1</vod:RunTime>';
		$str_xml .=             '<vod:SummaryMedium>'.$arr_info_ex['video']['base']['nns_name'].'[第'.$arr_info_ex['index']['base']['nns_index'].'集]'.'</vod:SummaryMedium>';
		$str_xml .=             '<vod:SearchName />';
		$str_xml .=             '<vod:EnglishName />';
		$str_xml .=             '<vod:IsAdvertise>0</vod:IsAdvertise>';
		$str_xml .=             '<vod:ShowType>Series</vod:ShowType>';
		$str_xml .=             '<vod:EpisodeID>'.$arr_info_ex['index']['base']['nns_index'].'</vod:EpisodeID>';
		$str_xml .=        '</vod:Title>';
		$str_xml .=    '</adi:AddMetadataAsset>';
		if(!empty($arr_img) && is_array($arr_img))
		{
		    foreach ($arr_img as $key=>$val)
		    {
        		$str_xml .=    '<adi:AcceptContentAsset type="Image" metadataOnly="N" fileName="'.$key.'" fileSize="47969" mD5CheckSum="475436d950b1570da08a545c7cd94326" Original_Asset_ID="">';
        		$str_xml .=       '<vod:Image providerID="'.$this->str_cmsid.'" assetID="'.$key.'" updateNum="1" fileName="'.$key.'" fileSize="47969" mD5CheckSum="475436d950b1570da08a545c7cd94326" transferContentURL="'.$val.'" imageEncodingProfile="JPG/JPEG">';
        		$str_xml .=           '<adi:AssetLifetime startDateTime="2014-01-19" endDateTime="2064-01-19" />';
        		$str_xml .=           '<vod:Caption>'.$key.'</vod:Caption>';
        		$str_xml .=           '<vod:MimeType>25</vod:MimeType>';
        		$str_xml .=           '<vod:FileType>3</vod:FileType>';
        		$str_xml .=           '<vod:ServiceType>1</vod:ServiceType>';
        		$str_xml .=           '<vod:ColorType>RGB</vod:ColorType>';
        		$str_xml .=           '<vod:Usage>6</vod:Usage>';
        		$str_xml .=           '<vod:ImagePixels horizontalPixels="410" verticalPixels="543" />';
        		$str_xml .=       '</vod:Image>';
        		$str_xml .=   '</adi:AcceptContentAsset>';
		    }
		}
		$str_xml .=    '<adi:'.$str_Action.'ContentAsset type="Video" metadataOnly="N" fileName="'.str_replace("'", '',str_replace('"', '', $this->arr_params['bk_c2_task_info']['nns_name'])).'" fileSize="'.$arr_info['nns_file_size'].'" mD5CheckSum="a2b9844b4c68e634223753ef422f7fd3" Original_Asset_ID="">';
		$str_xml .=       '<vod:Video providerID="'.$this->str_cmsid.'" assetID="'.$arr_info['nns_cdn_media_guid'].'" updateNum="1" fileName="'.str_replace("'", '',str_replace('"', '', $this->arr_params['bk_c2_task_info']['nns_name'])).'" fileSize="'.$arr_info['nns_file_size'].'" mD5CheckSum="a2b9844b4c68e634223753ef422f7fd3" transferContentURL="'.$arr_info['nns_url'].'" encodingProfile="video/H264" encodingCode="84tsh264mp28000700019219201080">';
		$str_xml .=           '<adi:AssetLifetime startDateTime="2014-10-19" endDateTime="2064-10-19" />';
		$str_xml .=           '<vod:AspectRatio>16:9</vod:AspectRatio>';
		$str_xml .=           '<vod:HDFlag>0</vod:HDFlag>';
		$str_xml .=           '<vod:Brightness></vod:Brightness>';
		$str_xml .=           '<vod:FrameHeight>'.$arr_info['FrameHeight'].'</vod:FrameHeight>';
		$str_xml .=           '<vod:IsFinished>1</vod:IsFinished>';
		$str_xml .=           '<vod:MimeType>1</vod:MimeType>';
		$str_xml .=           '<vod:IsEdit>1</vod:IsEdit>';
		$str_xml .=           '<vod:ServiceType>1</vod:ServiceType>';
		$str_xml .=           '<vod:parentAssetID></vod:parentAssetID>';
		$str_xml .=           '<vod:NeedDRM>1</vod:NeedDRM>';
		$str_xml .=           '<vod:Usage>1</vod:Usage>';
		$str_xml .=           '<vod:FrameWidth>'.$arr_info['FrameWidth'].'</vod:FrameWidth>';
		$str_xml .=           '<vod:VideoCodec>video/H264</vod:VideoCodec>';
		$str_xml .=           '<vod:Numberofframes>3</vod:Numberofframes>';
		$str_xml .=           '<vod:AudioCodec>mpeg1layer2</vod:AudioCodec>';
		$str_xml .=           '<vod:IsDRM>1</vod:IsDRM>';
		$str_xml .=           '<vod:Duration>'.$arr_info['nns_file_time_len'].'</vod:Duration>';
		$str_xml .=           '<vod:FileType>1</vod:FileType>';
		$str_xml .=           '<vod:Contrast></vod:Contrast>';
		$str_xml .=           '<vod:Bitrate>'.$arr_info['nns_kbps'].'</vod:Bitrate>';
		$str_xml .=           '<vod:FrameRate>'.$arr_info['nns_file_frame_rate'].'</vod:FrameRate>';
		$str_xml .=       '</vod:Video>';
		$str_xml .=    '</adi:'.$str_Action.'ContentAsset>';
		$str_xml .=    '<adi:AssociateContent type="Video" effectiveDate="" groupProviderID="'.$this->str_cmsid.'" groupAssetID="'.$arr_info_ex['index']['base']['nns_cdn_index_guid'].'" providerID="'.$this->str_cmsid.'" assetID="'.$arr_info['nns_cdn_media_guid'].'" />';
		$str_xml .=    '<adi:AssociateGroup effectiveDate="" groupProviderID="'.$this->str_cmsid.'" providerID="'.$this->str_cmsid.'" assetID="'.$arr_info_ex['index']['base']['nns_cdn_index_guid'].'" sourceGroupAssetID="'.$arr_info_ex['index']['base']['nns_cdn_index_guid'].'" targetGroupAssetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" />';
		if(!empty($arr_img) && is_array($arr_img))
		{
		    foreach ($arr_img as $key=>$val)
		    {
		        $str_xml .=    '<adi:AssociateContent type="Image" effectiveDate="" groupProviderID="'.$this->str_cmsid.'" groupAssetID="'.$arr_info_ex['video']['base']['nns_cdn_video_guid'].'" providerID="'.$this->str_cmsid.'" assetID="'.$key.'" />';
		    }
		}
		$str_xml .='</adi:ADI2>';
		return $str_xml;
	}
	
	/**
	 * 组装节目单信息
	 * @param unknown $arr_live_media 片源基本信息
	 * @param string $arr_live_media_ex 扩展数据
	 * @return string 
	 */
	private function make_playbill_xml($arr_playbill,$arr_playbill_ex=null)
	{
		$str_Action = ($arr_playbill['nns_state'] !=1) ? ($arr_playbill['nns_modify_time'] > $arr_playbill['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
		$str_xml ='<Object ElementType="ScheduleRecord" PhysicalContentID="'.$arr_playbill['nns_cdn_playbill_guid'].'" ScheduleId="'.$arr_playbill['nns_cdn_playbill_guid'].'" PhysicalChannelID="'.$arr_playbill['nns_cdn_live_media_guid'].'" Action="'.$str_Action.'">';
		
		return $str_xml;
	}
	
	/**
	 * 点播主媒资CDN
	 * @param unknown $movie_id
	 */
	public function do_video()
	{
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_video_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $action = ($this->arr_params['bk_c2_task_info']['nns_action'] !='destroy') ? ($this->arr_params['bk_c2_task_info']['nns_action'] == 'modify') ? 'UPDATE' : 'REGIST' : 'DELETE';
	    if($action == 'REGIST')
	    {
	        include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/c2_task/c2_task.class.php';
	        return nl_c2_task::edit($this->obj_dc, array('nns_status'=>0,'nns_epg_status'=>97), $this->arr_params['bk_c2_task_info']['nns_id']);
	    }
	    $video_id = $this->arr_params['bk_c2_task_info']['nns_ref_id'];
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	
	    $result_video = nl_vod::get_video_info_by_id($this->obj_dc, $video_id);
	    if($result_video['ret'] !=0)
	    {
	        return $result_video;
	    }
	    if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['bk_c2_task_info']['nns_id']}'", $this->obj_dc->db());
	        return $result_video;
	    }
	    $result_video = $result_video['data_info'][0];
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $video_id,$result_video);
	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        return $result_video_import_info;
	    }
	    $result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
	     
	    $arr_info_ex=array(
	        'video'=>array(
	            'base'=>$result_video,
	        ),
	    );
	    $xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
	    $xml_str .=     $this->make_vod_xml($arr_info_ex,$action);
	    
	    $file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'vod_video' . '_' . $action . '_' .  rand(100, 999) . '.xml';
	    $c2_info = array(
	        'nns_task_type'=>'Series',
	        'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	        'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
	        'nns_action'=>	$action,
	        'nns_url' => $file_name,
	        'nns_content' => $xml_str,
	        'nns_desc' => 'Series,'.$action,
	    );
		return $this->execute_c2_homed($c2_info);
	}
	
	/**
	 * 点播分集CDN
	 * @param unknown $movie_id
	 */
	public function do_index()
	{
	    if(!isset($this->arr_params['bk_c2_task_info']['nns_ref_id']) || empty($this->arr_params['bk_c2_task_info']['nns_ref_id']))
	    {
	        return $this->_make_return_data(1,'点播传入参数无nns_index_id字段,参数为:'.var_export($this->arr_params));
	    }
	    $action = ($this->arr_params['bk_c2_task_info']['nns_action'] !='destroy') ? ($this->arr_params['bk_c2_task_info']['nns_action'] == 'modify') ? 'UPDATE' : 'REGIST' : 'DELETE';
	    if($action == 'REGIST')
	    {
	        include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/c2_task/c2_task.class.php';
	        return nl_c2_task::edit($this->obj_dc, array('nns_status'=>0,'nns_epg_status'=>97), $this->arr_params['bk_c2_task_info']['nns_id']);
	    }
	    $index_id = $this->arr_params['bk_c2_task_info']['nns_ref_id'];
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
	    $result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $index_id);
	    if($result_index['ret'] !=0)
	    {
	        return $result_index;
	    }
	    if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['bk_c2_task_info']['nns_id']}'", $this->obj_dc->db());
	        return $result_index;
	    }
	    $result_index = $result_index['data_info'][0];
	    $result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $index_id,$result_index);
	    if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
	    {
	        return $result_index_import_info;
	    }
	    $result_index['nns_cdn_index_guid'] = $result_index_import_info['data_info'];
	     
	    $result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_index['nns_vod_id']);
	    if($result_video['ret'] !=0)
	    {
	        return $result_video;
	    }
	    if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
	    {
	        nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['bk_c2_task_info']['nns_id']}'", $this->obj_dc->db());
	        return $result_video;
	    }
	    $result_video = $result_video['data_info'][0];
	    $result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_index['nns_vod_id'],$result_video);
	    if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
	    {
	        return $result_video_import_info;
	    }
	    $result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
	    
	    $arr_info_ex=array(
		    'video'=>array(
		        'base'=>$result_video,
		    ),
		    'index'=>array(
		        'base'=>$result_index,
		    ),
		);
		$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml_str .= $this->make_vod_index_xml($arr_info_ex,$action);
		$file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'vod_media' . '_' . $action . '_' .  rand(100, 999) . '.xml';
		$c2_info = array(
				'nns_task_type'=>'Program',
				'nns_task_id'=> isset($this->arr_params['bk_c2_task_info']['nns_id'])?$this->arr_params['bk_c2_task_info']['nns_id']:null,
	            'nns_task_name'=> isset($this->arr_params['bk_c2_task_info']['nns_name'])?$this->arr_params['bk_c2_task_info']['nns_name']:null,
				'nns_action'=>	$action,
				'nns_url' => $file_name,
				'nns_content' => $xml_str,
				'nns_desc' => 'Program,'.$action,
		);
		return $this->execute_c2_homed($c2_info);
	}
	
	
	/**
	 * 片源CDN
	 * @param unknown $movie_id
	 */
	public function do_media()
	{
		if(!isset($this->arr_params['nns_media_id']) || empty($this->arr_params['nns_media_id']))
		{
			return $this->_make_return_data(1,'点播传入参数无nns_media_id字段,参数为:'.var_export($this->arr_params));
		}
		$movie_id = $this->arr_params['nns_media_id'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/vod_media/vod_media.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod.class.php';
		$result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $movie_id);
		if($result_media['ret'] !=0)
		{
			return $result_media;
		}
		if(!isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
		{
			nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
			return $result_media;
		}
		$result_media = $result_media['data_info'];
		$result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'media', $movie_id,$result_media);
		if((int)$this->arr_sp_config['disabled_clip'] !== 2 && (int)$this->arr_sp_config['disabled_clip'] !== 3)
		{
		    $result_media['nns_url'] = ltrim((trim($this->arr_params['nns_file_path'])),'/');
		    if(isset($this->arr_sp_config['media_ftp']) && !empty($this->arr_sp_config['media_ftp']))
		    {
		        $this->arr_sp_config['media_ftp'] = rtrim((trim($this->arr_sp_config['media_ftp'])),'/');
		        if (stripos($result_media['nns_url'], 'http://') === FALSE && stripos($result_media['nns_url'], 'ftp://') === FALSE)
		        {
		            $result_media['nns_url'] = $this->arr_sp_config['media_ftp'].'/'.$result_media['nns_url'];
		        }
		    }
		}
		else
		{
		    $result_media['nns_url'] = ltrim((trim($result_media['nns_url'])),'/');
		    if(isset($this->arr_sp_config['media_ftp']) && !empty($this->arr_sp_config['media_ftp']))
		    {
		        $this->arr_sp_config['media_ftp'] = rtrim((trim($this->arr_sp_config['media_ftp'])),'/');
		        if (stripos($result_media['nns_url'], 'http://') === FALSE && stripos($result_media['nns_url'], 'ftp://') === FALSE)
		        {
		            $result_media['nns_url'] = $this->arr_sp_config['media_ftp'].'/'.$result_media['nns_url'];
		        }
		    }
		}
		if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
		{
			return $result_media_import_info;
		}
		$result_media['nns_cdn_media_guid'] = $result_media_import_info['data_info'];
		$arr_nns_file_resolution = explode('*', $result_media['nns_file_resolution']);
		$result_media['FrameHeight'] = (isset($arr_nns_file_resolution[0]) && (int)$arr_nns_file_resolution[0] >0) ? $arr_nns_file_resolution[0] : 1080;
		$result_media['FrameWidth'] = (isset($arr_nns_file_resolution[1]) && (int)$arr_nns_file_resolution[1] >0) ? $arr_nns_file_resolution[1] : 1920;
		$result_index = nl_vod_index::get_video_index_info_by_id($this->obj_dc, $result_media['nns_vod_index_id']);
		if($result_index['ret'] !=0)
		{
		    return $result_index;
		}
		if(!isset($result_index['data_info'][0]) || !is_array($result_index['data_info'][0]) || empty($result_index['data_info'][0]))
		{
		    nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
		    return $result_index;
		}
		$result_index = $result_index['data_info'][0];
		$result_index_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'index', $result_media['nns_vod_index_id'],$result_index);
		
		if($result_index_import_info['ret'] !=0 || !isset($result_index_import_info['data_info']) || strlen($result_index_import_info['data_info']) <1)
		{
		    return $result_index_import_info;
		}
		$result_index['nns_cdn_index_guid'] = $result_index_import_info['data_info'];
		
		$result_video = nl_vod::get_video_info_by_id($this->obj_dc, $result_media['nns_vod_id']);
		if($result_video['ret'] !=0)
		{
		    return $result_video;
		}
		if(!isset($result_video['data_info'][0]) || !is_array($result_video['data_info'][0]) || empty($result_video['data_info'][0]))
		{
		    nl_execute_by_db("update nns_mgtvbk_c2_task  set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='{$this->arr_params['nns_task_id']}'", $this->obj_dc->db());
		    return $result_video;
		}
		$result_video = $result_video['data_info'][0];
		$result_video_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'video', $result_media['nns_vod_id'],$result_video);
		
		if($result_video_import_info['ret'] !=0 || !isset($result_video_import_info['data_info']) || strlen($result_video_import_info['data_info']) <1)
		{
		    return $result_video_import_info;
		}
		$result_video['nns_cdn_video_guid'] = $result_video_import_info['data_info'];
		
		$arr_info_ex=array(
		    'video'=>array(
		        'base'=>$result_video,
		    ),
		    'index'=>array(
		        'base'=>$result_index,
		    ),
		);
		$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml_str .= $this->make_vod_media_xml($result_media,$arr_info_ex);
		$action = ($result_media['nns_deleted'] !=1) ? ($result_media['nns_modify_time'] > $result_media['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
		$file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'vod_media' . '_' . $action . '_' .  rand(100, 999) . '.xml';
		$c2_info = array(
				'nns_task_type'=>'Movie',
				'nns_task_id'=> isset($this->arr_params['nns_task_id'])?$this->arr_params['nns_task_id']:null,
				'nns_task_name'=> isset($this->arr_params['nns_task_name'])?$this->arr_params['nns_task_name']:null,
				'nns_action'=>	$action,
				'nns_url' => $file_name,
				'nns_content' => $xml_str,
				'nns_desc' => 'Movie,'.$action,
		);
		$result = $this->execute_c2_homed($c2_info);
// 		$result = array(
// 		    'ret'=>0,
// 		    'reason'=>'ok',
// 		    'data_info'=>'',
// 		);
// 		$result = $this->notify_upstream($result_media, $action,$result);
// 		var_dump($result);die;
		return $result;
	}
	
	public function do_notify()
	{
	    return $this->notify_upstream(array('nns_cp_id'=>$this->arr_params['nns_cp_id']),$this->arr_params['action'],$this->arr_params['result'],'video');
	}
	
	
	/**
	 * 
	 * @param unknown $result_media
	 * @param unknown $action
	 * @param unknown $result
	 * @param string $type
	 */
	private function notify_upstream($result_media,$action,$result,$type = 'media')
	{
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/notify/make_notify_file.class.php';
	    global $g_project_name;
	    $project_name = (isset($g_project_name) && strlen($g_project_name)>0) ? $g_project_name : '';
	    if(strlen($project_name) <1)
	    {
	        return $this->_make_return_data(1,"全局项目ID配置为空:[{$project_name}];line:[".__LINE__."];file[".__FILE__."]");
	    }
	    global $g_bk_web_url;
	    $bk_web_url = (isset($g_bk_web_url) && strlen($g_bk_web_url)>0) ? $g_bk_web_url : '';
	    $bk_web_url = rtrim(rtrim(trim($bk_web_url),'/'),'\\');
	    if(strlen($bk_web_url) <1)
	    {
	        return $this->_make_return_data(1,"后台基本地址(数据库配置文件父级目录)配置为空:[{$bk_web_url}];line:[".__LINE__."];file[".__FILE__."]");
	    }
	    unset($g_project_name);
	    unset($g_bk_web_url);
	    $str_include_file = dirname(dirname(dirname(dirname(__FILE__)))).'/notify/soap/'.$project_name.'/notify.php';
	    if(!file_exists($str_include_file))
	    {
	        return $this->_make_return_data(1,"加载消息反馈文件不存在:[{$str_include_file}];line:[".__LINE__."];file[".__FILE__."]");
	    }
	    include_once $str_include_file;
	    $ResultCode = (isset($result['ret']) && $result['ret'] == 0) ? 0 : -1;
	    $Description = ($ResultCode == 0) ? 'success' : 'error';
	    #TODO
 	    $result_media['nns_content_id'] = '1111111';
        if($type == 'media')
        {
	        $obj_make_file = new nl_make_notify_file($this->str_sp_id,$result_media['nns_cp_id']);
            $result_notify_file = $obj_make_file->make_notify_file($result_media,$ResultCode,$Description,'media',$action);
            if($result_notify_file['ret'] !=0)
            {
                return $result_notify_file;
            }
            $site_callback_url = rtrim(rtrim(trim($this->arr_sp_config['site_callback_url']),'/'),'\\');
            if(strlen($site_callback_url) <1)
            {
                return $this->_make_return_data(1,"消息反馈上游地址为空:[{$site_callback_url}];line:[".__LINE__."];file[".__FILE__."]");
            }
        }
        else
        {
            $site_callback_url='';
            $result_notify_file=array();
            $result_notify_file['data_info']='';
        }
	    $WDSL_URL = $bk_web_url.'/notify/soap/'.$project_name.'/'.$result_media['nns_cp_id'].'/service.php?wsdl';
	    $obj_soap = new notify($WDSL_URL,$result_media['nns_cp_id'],'starcor',$this->arr_params['bk_c2_task_info']['nns_message_id'],(int)$ResultCode,$Description,$site_callback_url.$result_notify_file['data_info']);
	    $soap_result = $obj_soap->ExecCmd();
        if(isset($soap_result->ResultCode))
        {
            $ResultCode = ($soap_result->ResultCode == 0) ? 0 : 1;
        }
        else
        {
            $ResultCode = ($soap_result->Result == 0) ? 0 : 1;
        }
	    return  $this->_make_return_data($ResultCode,isset($soap_result->ErrorDescription) ? $soap_result->ErrorDescription : '');
	}
	
	
	/**
	 * 直播片源CDN
	 * @param unknown $movie_id
	 */
	public function do_live_media()
	{
		if(!isset($this->arr_params['nns_channel_id']) && !empty($this->arr_params['nns_channel_id']))
		{
			return array(
					'ret'=>1,
					'reason'=>'直播片源传入参数无nns_channel_id字段,参数为:'.var_export($this->arr_params),
			);
		}
		$channel_id = $this->arr_params['nns_channel_id'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/live.class.php';
		$result_channel = nl_live::query_by_id($this->obj_dc, $channel_id);
		if($result_channel['ret'] !=0 || !isset($result_channel['data_info']) || !is_array($result_channel['data_info']) || empty($result_channel['data_info']))
		{
			return $result_channel;
		}
		$result_channel = $result_channel['data_info'];
		$result_channel_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'live', $channel_id,$result_channel);
		if($result_channel_import_info['ret'] !=0 || !isset($result_channel_import_info['data_info']) || strlen($result_channel_import_info['data_info']) <1)
		{
			return $result_channel_import_info;
		}
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/live_media.class.php';
		$result_media = nl_live_media::query_by_channel_id($this->obj_dc, $channel_id);
		if($result_media['ret'] !=0 || !isset($result_media['data_info']) || !is_array($result_media['data_info']) || empty($result_media['data_info']))
		{
			return $result_media;
		}
		$result_media = $result_media['data_info'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/c2_task/c2_task.class.php';
		foreach ($result_media as $media_list)
		{
			$result_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'live_media', $media_list['nns_id'],$media_list);
			if($result_media_import_info['ret'] !=0 || !isset($result_media_import_info['data_info']) || strlen($result_media_import_info['data_info']) <1)
			{
				return $result_media_import_info;
			}
			$media_list['nns_cdn_media_guid'] = $result_media_import_info['data_info'];
			$xml_str='';
			$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
			$xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
			$xml_str .= 	'<Objects>';
			$xml_str .=     $this->make_live_media_xml($media_list,array('nns_cdn_channel_guid'=>$result_channel_import_info['data_info']));
			$xml_str .= 	'</Objects>';
			$xml_str .= '</ADI>';
			$action = ($result_media['nns_deleted'] !=1) ? ($result_media['nns_modify_time'] > $result_media['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
			$str_action = ($result_media['nns_deleted'] !=1) ? ($result_media['nns_modify_time'] > $result_media['nns_create_time']) ? 'modify' : 'add' : 'destroy';
			$file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'live_media' . '_' . $action . '_' .  rand(100, 999) . '.xml';
			
			$str_task_id = np_guid_rand();
			$result_c2_exsist=nl_c2_task::query_c2_exsist($this->obj_dc,'live_media',$media_list['nns_id'],$this->str_sp_id);
			if($result_c2_exsist['ret'] !=0)
			{
				return $result_c2_exsist;
			}
			$result_c2_exsist = isset($result_c2_exsist['data_info'][0]) ? $result_c2_exsist['data_info'][0] : null;
			if(is_array($result_c2_exsist) && !empty($result_c2_exsist))
			{
				$str_task_id = $result_c2_exsist['nns_id'];
				$result_c2_edit=nl_c2_task::edit($this->obj_dc, array('nns_action'=>$str_action,'nns_status'=>1,'nns_epg_status'=>97),$str_task_id);
				if($result_c2_edit['ret'] !=0)
				{
					return $result_c2_edit;
				}
			}
			else
			{
				$arr_c2_add = array(
						'nns_id'=>$str_task_id,
						'nns_type'=>'live_media',
						'nns_name'=>"[{$result_channel['nns_name']}] 1 [{$media_list['nns_mode']}]-直播流",
						'nns_ref_id'=>'',
						'nns_action'=>$str_action,
						'nns_status'=>1,
						'nns_org_id'=>$this->str_sp_id,
						'nns_category_id'=>'',
						'nns_src_id'=>$media_list['nns_id'],
						'nns_all_index'=>1,
						'nns_clip_task_id'=>'',
						'nns_clip_date'=>'',
						'nns_op_id'=>'',
						'nns_epg_status'=>97,
						'nns_ex_url'=>'',
						'nns_file_path'=>'',
						'nns_file_size'=>'',
						'nns_file_md5'=>'',
						'nns_cdn_policy'=>'',
						'nns_epg_fail_time'=>0,
						'nns_message_id'=>'',
				);
				$result_c2_add=nl_c2_task::add($this->obj_dc, $arr_c2_add);
				if($result_c2_edit['ret'] !=0)
				{
					return $result_c2_add;
				}
			}
			
			$c2_info = array(
					'nns_task_type'=>'PhysicalChannel',
					'nns_task_id'=> $str_task_id,
					'nns_task_name'=> "[{$result_channel['nns_name']}] 1 [{$media_list['nns_mode']}]-直播流",
					'nns_action'=>	$action,
					'nns_url' => $file_name,
					'nns_content' => $xml_str,
					'nns_desc' => 'PhysicalChannel,'.$action,
			);
			return $this->execute_telecom_c2($c2_info);
		}
	}
	
	
	/**
	 * 直播节目单CDN
	 * @param unknown $movie_id
	 */
	public function do_playbill()
	{
		if(!isset($this->arr_params['nns_playbill_id']) && !empty($this->arr_params['nns_playbill_id']))
		{
			return array(
					'ret'=>1,
					'reason'=>'节目单传入参数无nns_playbill_id字段,参数为:'.var_export($this->arr_params),
			);
		}
		$playbill_id = $this->arr_params['nns_playbill_id'];
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/live_media.class.php';
		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/playbill.class.php';
		$result_playbill = nl_playbill::query_by_id($this->obj_dc, $playbill_id);
		if($result_playbill['ret'] !=0 || !isset($result_playbill['data_info']) || !is_array($result_playbill['data_info']) || empty($result_playbill['data_info']))
		{
			return $result_playbill;
		}
		$result_playbill = $result_playbill['data_info'];
		$result_live_media = nl_live_media::query_by_id($this->obj_dc, $result_playbill['nns_live_media_id']);
		if($result_live_media['ret'] !=0 || !isset($result_live_media['data_info']) || !is_array($result_live_media['data_info']) || empty($result_live_media['data_info']))
		{
			return $result_live_media;
		}
		$result_live_media = $result_live_media['data_info'];
		$result_playbill_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'playbill', $playbill_id,$result_playbill);
		if($result_playbill_import_info['ret'] !=0 || !isset($result_playbill_import_info['data_info']) || strlen($result_playbill_import_info['data_info']) <1)
		{
			return $result_playbill_import_info;
		}
		$result_live_media_import_info = public_model_exec::get_asset_id_by_sp_id($this->str_sp_id, 'live_media', $playbill_id,$result_live_media);
		if($result_live_media_import_info['ret'] !=0 || !isset($result_live_media_import_info['data_info']) || strlen($result_live_media_import_info['data_info']) <1)
		{
			return $result_live_media_import_info;
		}
		$result_playbill['nns_cdn_playbill_guid'] = $result_playbill_import_info['data_info'];
		$result_playbill['nns_cdn_live_media_guid'] = $result_live_media_import_info['data_info'];
		$result_playbill_ex['nns_filetype'] = $result_live_media['nns_filetype'];
		$result_playbill_ex['nns_import_source'] = $result_live_media['nns_import_source'];
		$result_playbill_ex['nns_domain'] = $result_live_media['nns_domain'];
		$xml_str  = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml_str .= '<ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" BizDomain="2" Priority="1">';
		$xml_str .= 	'<Objects>';
		$xml_str .=     $this->make_playbill_xml($result_playbill,$result_playbill_ex);
		$xml_str .= 	'</Objects>';
		$xml_str .= '</ADI>';
		
		$action = ($result_playbill['nns_state'] !=1) ? ($result_playbill['nns_modify_time'] > $result_playbill['nns_create_time']) ? 'UPDATE' : 'REGIST' : 'DELETE';
		$str_action = ($result_playbill['nns_state'] !=1) ? ($result_playbill['nns_modify_time'] > $result_playbill['nns_create_time']) ? 'modify' : 'add' : 'destroy';
		$file_name = $this->str_sp_id . '_cms_' . date('YmdHis') . '_' . rand(1000, 9999) . '_' . 'playbill' . '_' . $action . '_' .  rand(100, 999) . '.xml';
		$str_task_id = np_guid_rand();
		$result_c2_exsist=nl_c2_task::query_c2_exsist($this->obj_dc,'playbill',$result_playbill['nns_id'],$this->str_sp_id);
		if($result_c2_exsist['ret'] !=0)
		{
			return $result_c2_exsist;
		}
		$result_c2_exsist = isset($result_c2_exsist['data_info'][0]) ? $result_c2_exsist['data_info'][0] : null;
		if(is_array($result_c2_exsist) && !empty($result_c2_exsist))
		{
			$str_task_id = $result_c2_exsist['nns_id'];
			$result_c2_edit=nl_c2_task::edit($this->obj_dc, array('nns_action'=>$str_action,'nns_status'=>1,'nns_epg_status'=>97),$str_task_id);
			if($result_c2_edit['ret'] !=0)
			{
				return $result_c2_edit;
			}
		}
		else
		{
			$arr_c2_add = array(
					'nns_id'=>$str_task_id,
					'nns_type'=>'playbill',
					'nns_name'=>"[{$result_playbill['nns_name']}]-节目单",
					'nns_ref_id'=>$result_playbill['nns_live_media_id'],
					'nns_action'=>$str_action,
					'nns_status'=>1,
					'nns_org_id'=>$this->str_sp_id,
					'nns_category_id'=>'',
					'nns_src_id'=>$result_playbill['nns_id'],
					'nns_all_index'=>1,
					'nns_clip_task_id'=>'',
					'nns_clip_date'=>'',
					'nns_op_id'=>'',
					'nns_epg_status'=>97,
					'nns_ex_url'=>'',
					'nns_file_path'=>'',
					'nns_file_size'=>'',
					'nns_file_md5'=>'',
					'nns_cdn_policy'=>'',
					'nns_epg_fail_time'=>0,
					'nns_message_id'=>'',
			);
			$result_c2_add=nl_c2_task::add($this->obj_dc, $arr_c2_add);
			if($result_c2_edit['ret'] !=0)
			{
				return $result_c2_add;
			}
		}
			
		$c2_info = array(
				'nns_task_type'=>'Schedule',
				'nns_task_id'=> $str_task_id,
				'nns_task_name'=> "[{$result_playbill['nns_name']}]-节目单",
				'nns_action'=>	$action,
				'nns_url' => $file_name,
				'nns_content' => $xml_str,
				'nns_desc' => 'Schedule,'.$action,
		);
		return $this->execute_telecom_c2($c2_info);
	}
}
