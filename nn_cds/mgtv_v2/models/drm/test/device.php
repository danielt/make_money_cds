<?php
/**DRM设备管理测试
 * 1、添加设备
 * 1、删除设备
 * Author: 陈波(bo.chen@starcor.cn)
 * Date: 2015/4/1 20:21
 */

include_once dirname(dirname(__DIR__)).'/drm/manager/device.class.php';

//数据库对象实例化
$dc_param = array();
$dc_param['db_policy'] = NL_DB_WRITE;
$dc_param['cache_policy'] = NP_KV_CACHE_TYPE_MEMCACHE;
$dc_param['delay_open_db'] = true;
$dc_param['delay_open_cache'] = true;
$dc = nl_get_dc($dc_param);
$dc->open();

if($_GET['a'] == 'add')
{
	echo "添加开始<br/>";
	$params = array();
	$params['nns_drm_device_id'] = 'uft3YwQltZJSDcWZbIPUaTuokKQ=';
	$params['nns_device_type'] = 'WEB_PC';
	$params['nns_device_id'] = 'uft3YwQltZJSDcWZbIPUaTuokKQ=';
	var_dump(nl_m_device::add_device($dc,$params));
	echo "<br/>添加结束<br/>";
}
if($_GET['a'] == 'delete')
{
	$params = array();
	$params['nns_drm_device_id'] = 'A4PQdmDmkgrfPZPziEURazwGXjk=';
	$params['nns_device_type'] = 'WEB_PC';
	$params['nns_device_id'] = 'A4PQdmDmkgrfPZPziEURazwGXjk=';
	echo "删除开始<br/>";
	var_dump(nl_m_device::delete_device($dc,$params));
	echo "<br/>删除结束<br/>";
}
if($_GET['a'] == 'disable')
{
	$params = array();
	$params['nns_drm_device_id'] = '44444';
	$params['nns_device_type'] = 'WEB_PC';
	$params['nns_device_id'] = '55555';
	echo "禁用设备开始<br/>";
	var_dump(nl_m_device::disable_device($dc,$params));
	echo "<br/>禁用设备结束<br/>";
}
if($_GET['a'] == 'enable')
{
	$params = array();
	$params['nns_drm_device_id'] = '44444';
	$params['nns_device_type'] = 'WEB_PC';
	$params['nns_device_id'] = '55555';
	echo "禁用设备开始<br/>";
	var_dump(nl_m_device::enable_device($dc,$params));
	echo "<br/>禁用设备结束<br/>";
}

//$m_config = new nl_m_config();
//$m_config->add_network($dc,array());


