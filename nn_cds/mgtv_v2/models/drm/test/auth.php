<?php
/**授权关系管理
 * 1、添加授权
 * 2、删除授权
 * Author: 陈波(bo.chen@starcor.cn)
 * Date: 2015/4/1 20:21
 */

include_once dirname(dirname(__DIR__)).'/drm/manager/auth_drm.class.php';

//数据库对象实例化
$dc_param = array();
$dc_param['db_policy'] = NL_DB_WRITE;
$dc_param['cache_policy'] = NP_KV_CACHE_TYPE_MEMCACHE;
$dc_param['delay_open_db'] = true;
$dc_param['delay_open_cache'] = true;
$dc = nl_get_dc($dc_param);
$dc->open();


if($_GET['a'] == 'add_auth')
{
	echo "添加开始<br/>";
	$params = array();
	$params['nns_start_time'] = '2015-04-10T06:56:23+00:00';//date('Y-m-d').'T'.date('H:i:s');
	$params['nns_end_time'] = '2015-04-10T07:56:23+00:00';//date('Y-m-d').'T'.date('H:i:s',time()+3600);
	$params['nns_device_id'] = '44444';
	$params['nns_auth_id'] = '0d804877ae7bd6242dffb1f9b8307628';
	$params['nns_package_id'] = '44444';
	$params['nns_description'] = '测试';
	var_dump(nl_m_auth_drm::add_auth($dc,$params));
	echo "<br/>添加结束<br/>";
}
if($_GET['a'] == 'delete_auth')
{
	echo "删除开始<br/>";
	$params = array();
	$params['nns_start_time'] = '2015-04-10T06:56:23+00:00';//date('Y-m-d').'T'.date('H:i:s');
	$params['nns_end_time'] = '2015-04-10T07:56:23+00:00';//date('Y-m-d').'T'.date('H:i:s',time()+3600);
	$params['nns_device_id'] = '44444';
	$params['nns_auth_id'] = '0d804877ae7bd6242dffb1f9b8307628';
	$params['nns_package_id'] = '44444';
	$params['nns_description'] = '测试';
	var_dump(nl_m_auth_drm::delete_auth($dc,$params));
	echo "<br/>删除结束<br/>";
}

if($_GET['a'] == 'delete_device_auth')
{
	echo "删除开始<br/>";
	$params = array();
	$params['nns_device_id'] = '44444';
	var_dump(nl_m_auth_drm::delete_device_auth($dc,$params));
	echo "<br/>删除结束<br/>";
}

