<?php
/**产品包管理
 * 1、添加产品包
 * 2、删除产品包
 * Author: 陈波(bo.chen@starcor.cn)
 * Date: 2015/4/1 20:21
 */

include_once dirname(dirname(__DIR__)).'/drm/manager/product.class.php';

//数据库对象实例化
$dc_param = array();
$dc_param['db_policy'] = NL_DB_WRITE;
$dc_param['cache_policy'] = NP_KV_CACHE_TYPE_MEMCACHE;
$dc_param['delay_open_db'] = true;
$dc_param['delay_open_cache'] = true;
$dc = nl_get_dc($dc_param);
$dc->open();


if($_GET['a'] == 'add_package')
{
	echo "添加开始<br/>";
	$params = array();
	$params['nns_package_id'] = '44444';
	$params['nns_description'] = 'WEB_PC';
	var_dump(nl_m_product::add_package($dc,$params));
	echo "<br/>添加结束<br/>";
}
if($_GET['a'] == 'delete_package')
{
	echo "删除开始<br/>";
	$params = array();
	$params['nns_package_id'] = '44444';
	$params['nns_description'] = 'WEB_PC';
	var_dump(nl_m_product::delete_package($dc,$params));
	echo "<br/>删除结束<br/>";
}



