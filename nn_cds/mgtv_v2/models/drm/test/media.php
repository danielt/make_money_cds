<?php
/**DRM添加片源
 * 1、添加片源
 * 2、删除片源
 * 3、添加片源内容
 * 4、删除片源内容
 * Author: 陈波(bo.chen@starcor.cn)
 * Date: 2015/4/1 20:21
 */

include_once dirname(dirname(__DIR__)).'/drm/manager/media.class.php';

//数据库对象实例化
$dc_param = array();
$dc_param['db_policy'] = NL_DB_WRITE;
$dc_param['cache_policy'] = NP_KV_CACHE_TYPE_MEMCACHE;
$dc_param['delay_open_db'] = true;
$dc_param['delay_open_cache'] = true;
$dc = nl_get_dc($dc_param);
$dc->open();
//添加片源
if($_GET['a'] == 'add_media')
{
	echo "添加开始<br/>";
	$params = array();
	$params['nns_media_id'] = '44444';
	$params['nns_description'] = 'WEB_PC';
	var_dump(nl_m_media::add_media($dc,$params));
	echo "<br/>添加结束<br/>";
}
//删除片源
if($_GET['a'] == 'delete_media')
{
	echo "删除开始<br/>";
	$params = array();
	$params['nns_media_id'] = '44444';
	$params['nns_description'] = 'WEB_PC';
	var_dump(nl_m_media::delete_media($dc,$params));
	echo "<br/>删除结束<br/>";
}
//添加片源到network
if($_GET['a'] == 'add_media_to_network')
{
	echo "添加开始<br/>";
	$params = array();
	$params['nns_media_id'] = '44444';
	$params['nns_network_id'] = 'test_network_id';
	$params['nns_description'] = 'WEB_PC';
	var_dump(nl_m_media::add_media_to_network($dc,$params));
	echo "<br/>添加结束<br/>";
}

//删除片源与network的绑定关系
if($_GET['a'] == 'delete_media_to_network')
{
	echo "删除开始<br/>";
	$params = array();
	$params['nns_media_id'] = '44444';
	$params['nns_network_id'] = 'test_network_id';
	$params['nns_description'] = 'WEB_PC';
	var_dump(nl_m_media::delete_media_from_network($dc,$params));
	echo "<br/>删除结束<br/>";
}


//添加片源内容
if($_GET['a'] == 'add_media_content')
{
	echo "添加开始<br/>";
	$params = array();
	$params['nns_media_id'] = '44444';
	$params['nns_content_id'] = '44444';
	$params['nns_network_id'] = 'test_network_id';
	$params['nns_start_time'] = date('Y-m-d').'T'.date('H:i:s');
	$params['nns_end_time'] =  date('Y-m-d').'T'.date('H:i:s',time()+3600);
	var_dump(nl_m_media::add_media_content($dc,$params));
	echo "<br/>添加结束<br/>";
}


//删除片源
if($_GET['a'] == 'delete_media_content')
{
	echo "删除开始<br/>";
	$params = array();
	$params['nns_media_id'] = '44444';
	$params['nns_content_id'] = '44444';
	$params['nns_network_id'] = 'test_network_id';
	$params['nns_start_time'] = date('Y-m-d H:i:s');
	$params['nns_end_time'] = date('Y-m-d H:i:s');
	var_dump(nl_m_media::delete_media_content($dc,$params));
	echo "<br/>删除结束<br/>";
}

//添加片源内容到产品包
if($_GET['a'] == 'add_media_content_to_package')
{
	echo "添加开始<br/>";
	$params = array();
	$params['nns_media_id'] = '44444';
	$params['nns_content_id'] = '44444';
	$params['nns_network_id'] = 'test_network_id';
	$params['nns_start_time'] = date('Y-m-d H:i:s');
	$params['nns_end_time'] = date('Y-m-d H:i:s');
	$params['nns_package_id'] = '44444';
	$params['nns_description'] = 'WEB_PC';

	var_dump(nl_m_media::add_media_content_to_package($dc,$params));
	echo "<br/>添加结束<br/>";
}


//删除片源内容所属产品包
if($_GET['a'] == 'delete_media_content_to_package')
{
	echo "删除开始<br/>";
	$params = array();
	$params['nns_media_id'] = '44444';
	$params['nns_content_id'] = '44444';
	$params['nns_network_id'] = 'test_network_id';
	$params['nns_start_time'] = date('Y-m-d').'T'.date('H:i:s');
	$params['nns_end_time'] = date('Y-m-d').'T'.date('H:i:s',time()+3600);
	$params['nns_package_id'] = '44444';
	$params['nns_description'] = 'WEB_PC';
	var_dump(nl_m_media::delete_media_content_to_package($dc,$params));
	echo "<br/>删除结束<br/>";
}


/**
 * '',
'',
'nns_network_id',
'nns_start_time',
'nns_end_time',
'',
'',
 */