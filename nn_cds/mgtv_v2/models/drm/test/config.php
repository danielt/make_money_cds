<?php
/**
 * DRM配置相关测试
 * 1、添加network
 * 2、删除network
 * Author: 陈波(bo.chen@starcor.cn)
 * Date: 2015/4/1 20:21
 */

include_once dirname(dirname(__DIR__)).'/drm/manager/config.class.php';

//数据库对象实例化
$dc_param = array();
$dc_param['db_policy'] = NL_DB_WRITE;
$dc_param['cache_policy'] = NP_KV_CACHE_TYPE_MEMCACHE;
$dc_param['delay_open_db'] = true;
$dc_param['delay_open_cache'] = true;
$dc = nl_get_dc($dc_param);
$dc->open();

if($_GET['a'] == 'add_network')
{
	echo "添加开始<br/>";
	var_dump(nl_m_config::add_network($dc));
	echo "<br/>添加结束<br/>";
}
if($_GET['a'] == 'delete_network')
{
	$param = array();
	echo "删除开始<br/>";
	var_dump(nl_m_config::delete_network($dc,$param));
	echo "<br/>删除结束<br/>";
}

//$m_config = new nl_m_config();
//$m_config->add_network($dc,array());


