<?php
/**
 * 授权相关业务组装
 * Author: 陈波(bo.chen@starcor.cn)
 * Date: 2015/3/20 19:37
 */
include_once dirname(__FILE__).'/auth_drm.class.php';
include_once dirname(__FILE__).'/auth_redis.class.php';
include_once dirname(__FILE__).'/common.class.php';
class nl_m_auth extends common
{
	/**
	 * 获取播放串时授权处理
	 * Author: 陈波(bo.chen@starcor.cn)
	 * $params = array(
		'nns_device_id',
		'nns_user_id',
		'nns_content_id',
	 *  'nns_product_id',
		'nns_user_valid_start_time',
		'nns_user_valid_end_time',
	 * )
	 */
	public static function play_auth($dc,$redis,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		$filed_arr = array(
			'nns_device_id',
			'nns_user_id',
			'nns_content_id',
			'nns_product_id',
			'nns_user_valid_start_time',
			'nns_user_valid_end_time',
			'nns_description'
		);
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		//当前时间戳
		$current_time = time();
		//计算临时授权-开始时间
		if(!isset($params['nns_temp_start_time']))
		{
			$params['nns_temp_start_time'] = date('Y-m-d H:i:s',$current_time);

		}
		//计算临时授权-结束时间
		if(!isset($params['nns_temp_end_time']))
		{
			//获取延期时间，单位：秒
			$temp_auth_time = get_config_v2('g_drm_vcas_temp_auth_time');
			if(empty($temp_auth_time) || $temp_auth_time < 0)
			{
				return get_logic_result(ML_STATE_PARAM_ERROR,"g_drm_vcas_temp_auth_time format is wrong");
			}

			$params['nns_temp_end_time'] = date('Y-m-d H:i:s',$current_time+$temp_auth_time);
		}
		$params['nns_expire_hour'] = date('YmdHi',$current_time);
		$params['nns_update_time'] = date('Y-m-d H:i:s',$current_time);

		//生成授权ID,保证必须唯一
		$params['nns_auth_id'] = self::get_auth_id($params);

		//DRM删除用户的所有授权关系
		$delete_auth_params = array();
		$delete_auth_params['nns_device_id'] = $params['nns_device_id'];
		$drm_delete_re = nl_m_auth_drm::delete_device_auth($dc,$delete_auth_params);
		if(!$drm_delete_re)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"DRM delete failure");
		}
		//DRM添加新的授权关系
		$add_auth_params = array();
		$add_auth_params['nns_start_time'] = $params['nns_temp_start_time'];
		$add_auth_params['nns_end_time'] = $params['nns_temp_end_time'];
		$add_auth_params['nns_device_id'] = $params['nns_device_id'];
		$add_auth_params['nns_auth_id'] = $params['nns_auth_id'];
		$add_auth_params['nns_package_id'] = $params['nns_content_id'];
		$add_auth_params['nns_description'] = $params['nns_description'];

		$drm_add_re = nl_m_auth_drm::add_auth($dc,$add_auth_params);
		if(!$drm_add_re)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"DRM add failure");
		}
		//REDIS删除用户的所有授权关系
		$redis_delete_auth_params = array();
		$redis_delete_re = nl_m_auth_redis::delete_auth($redis,$redis_delete_auth_params);
		if(!$redis_delete_re)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"REDIS delete failure");
		}
		//REDIS添加新的授权关系
		$redis_add_auth_params = array();
		$redis_add_auth_params['user_id'] = $params['nns_user_id'];
		$redis_add_auth_params['product_id'] = $params['nns_product_id'];
		$redis_add_auth_params['media_content_id'] = $params['media_content_id'];
		$redis_add_auth_params['user_valid_start_time'] = $params['nns_user_valid_start_time'];
		$redis_add_auth_params['user_valid_end_time'] = $params['nns_user_valid_end_time'];
		$redis_add_auth_params['temp_valid_start_time'] = $params['nns_start_time'];
		$redis_add_auth_params['temp_valid_end_time'] = $params['nns_end_time'];
		$redis_add_auth_params['expire_hour'] = $params['nns_expire_hour'];
		$redis_add_auth_params['update_time'] = $params['nns_update_time'];
		$redis_add_auth_params['entitlement_id'] = $params['nns_auth_id'];

		$redis_add_re = nl_m_auth_redis::add_auth($redis,$redis_add_auth_params);
		if(!$redis_add_re)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"REDIS add failure");
		}
		return true;
	}

	/**
	 * 维持授权
	 * Author: 陈波(bo.chen@starcor.cn)
	 * $params = array(
	 * 'nns_device_id',
	 * 'nns_session_handle',
	 * 'nns_user_id',
	 * 'nns_product_id',
	 * 'nns_media_content_id',
	 * 'nns_description'
	 *
	 * )
	 */
	public static function maintain_auth($dc,$redis,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		$filed_arr = array(
			'nns_device_id',
			'nns_user_id',
			'nns_product_id',
			'nns_content_id',
			'nns_description'

		);
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}


		//获取用户在redis中的授权信息
		$auth_params = array();
        $auth_params['page_index'] = 1;
        $auth_params['page_size'] = 1;
		$auth_params['#id'] = $auth_id = nl_m_auth_redis::get_redis_auth_id($params);
		$auth_params['page_index'] = 1;
		$auth_paramsp['page_size'] = 1;
		$redis_auth_info = nl_m_auth_redis::get_auth_list($redis,$auth_params);

		if ($redis_auth_info['count'] == 0)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"redis empty");
		}
		$redis_auth_info = $redis_auth_info['data'];
		//获取延期时间，单位：秒
		$temp_auth_time = get_config_v2('g_drm_vcas_temp_auth_time');

		if(empty($temp_auth_time) || $temp_auth_time < 0)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"g_drm_vcas_temp_auth_time format is wrong");
		}
		//计算授权延期
		$params['nns_start_time'] = date('Y-m-d H:i:s',strtotime($redis_auth_info['nns_start_time'])+$temp_auth_time);
		$params['nns_end_time'] = date('Y-m-d H:i:s',strtotime($redis_auth_info['nns_end_time'])+$temp_auth_time);
		//当前时间戳
		$current_time = time();
		$params['nns_expire_hour'] = date('YmdHi',$current_time);
		$params['nns_update_time'] = date('Y-m-d H:i:s',$current_time);
		//修改DRM系统给的授权关系
		$drm_modify_auth_params = array();
		$drm_modify_auth_params['nns_start_time'] = $params['nns_start_time'];
		$drm_modify_auth_params['nns_end_time'] = $params['nns_end_time'];
		$drm_modify_auth_params['nns_device_type'] = isset($params['nns_device_type'])?$params['nns_device_type']:'';
		$drm_modify_auth_params['nns_device_id'] = $params['nns_device_id'];
		$drm_modify_auth_params['nns_authorization_id'] = $redis_auth_info['entitlement_id'];
		$drm_modify_auth_params['nns_package_id'] = $params['nns_product_id'];
		$drm_modify_auth_params['nns_description'] = $params['nns_description'];
		$drm_modify_re = nl_m_auth_drm::modify_auth($dc,$drm_modify_auth_params);
		if($drm_modify_re)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"DRM delete failure");
		}

		//修改REDIS的授权关系
		$redis_auth_info['temp_valid_start_time'] = $params['nns_start_time'];
		$redis_auth_info['temp_valid_end_time'] = $params['nns_end_time'];
		$redis_auth_info['expire_hour'] = $params['nns_expire_hour'];
		$redis_auth_info['update_time'] = $params['nns_update_time'];

		$redis_modify_re = nl_m_auth_redis::add_auth($redis,$redis_auth_info);
		if($redis_modify_re)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"REDIS add failure");
		}
		return true;
	}

	/**
	 * 生成授权唯一ID
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $params = array(
	 *                'nns_user_id'=>'',
	 *                'nns_content_id'=>'',
	 *                'nns_product_id' =>'',
	 * )
	 */
	public static function get_auth_id($params)
	{
		//生成REDIS主键 对用户ID、片源ID、产品包ID进行MD5处理获得
		return  md5($params['nns_user_id'].$params['nns_content_id'].$params['nns_product_id']);
	}
}
