<?php
/**
 * 封装媒资注入和DRM交互的业务组装
 * Author: 陈波(bo.chen@starcor.cn)
 * Date: 2015/3/20 19:36
 */
include_once dirname(__FILE__).'/media.class.php';
include_once dirname(__FILE__).'/user.class.php';
class nl_m_import_drm extends common
{
	/**
	 * 媒资注入DRM
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $dc
	 * @param $params
	 * @return array|bool
	 */
	public static function import_drm($dc,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		$filed_arr = array(
			'nns_media_id',
			'nns_content_id',
			'nns_product_id',
			'nns_description',
		);
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		//登录DRM
		$login_re = nl_m_user::login($dc);
		if(!$login_re)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"Login failure");
		}
		//片源绑定管理域名
		$domain_params = array();
		$domain_params['nns_session'] = $login_re['session'];
		$domain_params['nns_media_id'] = $params['nns_media_id'];
		$domain_params['nns_description'] = $params['nns_description'];
		$domain_re = nl_m_media::add_media_to_network($dc,$domain_params);
		if(!$domain_re)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"Media to network add failure");
		}
		//添加片源内容
		if(!isset($params['nns_network_id']))
		{
			$params['nns_network_id'] = get_config_v2('g_vcas_network_id');
		}
		//如果设置片源有效期，默认10年
		if(!isset($params['nns_start_time']))
		{
			$params['nns_start_time'] = date('Y-m-d H:i:s',time());
		}
		if(!isset($params['nns_end_time']))
		{
			$params['nns_end_time'] = date('Y-m-d H:i:s',strtotime("+10 years",$params['nns_start_time']));
		}
		//注入片源内容
		$content_params = array();
		$content_params['nns_media_id'] = $params['nns_media_id'];
		$content_params['nns_content_id'] = $params['nns_content_id'];
		$content_params['nns_network_id'] = $params['nns_network_id'];
		$content_params['nns_start_time'] = $params['nns_start_time'];
		$content_params['nns_end_time'] = $params['nns_end_time'];
		$content_params['nns_session_handle'] = $login_re['sessionHandle'];
		$content_re = nl_m_media::add_media_content($dc,$content_params);
		if(!$content_re)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"Add  to network add failure");
		}
		//添加产品包
		$product_params = array();
		$product_params['nns_product_id'] = $params['nns_product_id'];
		$product_params['nns_session_handle'] = $login_re['sessionHandle'];
		$product_params['nns_description'] = $params['nns_description'];
		$product_re = nl_m_product::add_package($dc,$product_params);
		if(!$product_re)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"Product add failure");
		}
		//片源内容绑定产品包
		$content_product_params = array();
		$content_product_params['nns_media_id'] = $params['nns_media_id'];
		$content_product_params['nns_content_id'] = $params['nns_content_id'];
		$content_product_params['nns_network_id'] = $params['nns_network_id'];
		$content_product_params['nns_start_time'] = $params['nns_start_time'];
		$content_product_params['nns_end_time'] = $params['nns_end_time'];
		$content_product_params['nns_session_handle'] = $login_re['sessionHandle'];
		$content_product_params['nns_package_id'] = $params['nns_product_id'];
		$content_product_params['nns_description'] = $params['nns_description'];
		$content_product_re = nl_m_media::delete_media_content_to_package($dc,$content_product_params);
		if(!$content_product_re)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"content to product add failure");
		}

		return true;
	}
}