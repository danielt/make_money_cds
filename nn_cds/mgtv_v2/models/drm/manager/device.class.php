<?php
/**
 * 设备相关业务组装
 * Author: 陈波(bo.chen@starcor.cn)
 * Date: 2015/3/20 19:35
 */
include_once dirname(__FILE__).'/common.class.php';
class nl_m_device extends common
{
	//定义日志模块
	private static $str_log_module = 'nl_m_vod';

	public function __construct($wsdl)
	{
		$this->client = $this->get_soap_client($wsdl);
	}

	/**
	 * 向DRM系统添加设备
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $dc
	 * @param $params
	 * @return array
	 */
	public static function add_device($dc, $params)
	{

		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR, "params format is wrong");
		}
		$filed_arr = array (
			'nns_drm_device_id',
			'nns_device_type',
			'nns_device_id',
		);

		$check_res = self::check_params($params, $filed_arr);
		if ($check_res)
		{

			return get_logic_result(ML_STATE_PARAM_ERROR, "$check_res cannot be empty");
		}
		//开启VCAS
		if (self::get_vcas_enable())
		{

			return self::__add_device_by_vcas($dc,$params);
		}


	}

	/**
	 * 删除设备
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $dc
	 * @param $params
	 * @return array|void
	 */
	public static function  delete_device($dc, $params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR, "params format is wrong");
		}
		$filed_arr = array (
			'nns_drm_device_id',
			'nns_device_type',
			'nns_device_id',
		);
		$check_res = self::check_params($params, $filed_arr);
		if ($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR, "$check_res cannot be empty");
		}

		//开启VCAS
		if (self::get_vcas_enable())
		{
			return self::__delete_device_by_vcas($dc,$params);

		}


	}

	/**
	 * 启用设备
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $dc
	 * @param $params
	 */
	public static function enable_device($dc, $params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR, "params format is wrong");
		}
		$filed_arr = array (
			'nns_drm_device_id',
			'nns_device_type',
			'nns_device_id',
		);
		$check_res = self::check_params($params, $filed_arr);
		if ($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR, "$check_res cannot be empty");
		}
		//开启VCAS
		if (self::get_vcas_enable())
		{
			return self::__enable_device_by_vcas($dc,$params);

		}
	}

	/**
	 * 禁用设备
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $dc
	 * @param $params
	 * @return array|void
	 */
	public static function disable_device($dc, $params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR, "params format is wrong");
		}
		$filed_arr = array (
			'nns_drm_device_id',
			'nns_device_type',
			'nns_device_id',
		);
		$check_res = self::check_params($params, $filed_arr);
		if ($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR, "$check_res cannot be empty");
		}

		//开启VCAS
		if (self::get_vcas_enable())
		{
			return self::__disable_device_by_vcas($dc,$params);

		}
	}

	/**
	 * DRM-VCAS 添加设备
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $dc
	 * @param $params
	 * @return array|bool|mixed|void
	 */
	private static function __add_device_by_vcas($dc,$params)
	{
		//获取network ID
		$network_id = get_config_v2('g_drm_vcas_network_id');
		if(empty($network_id))
		{
			nl_log_v2_error('nl_m_device','nns_network_id为空');
			return get_logic_result(ML_STATE_PARAM_ERROR,"nns_network_id为空");
		}
		$params['nns_network_id'] = $network_id;
		//获取SOAP俩连接对象
		$client = self::__get_device_by_vcas_client();
		if($client === false)
		{
			nl_log_v2_error('nl_m_device',' WSDL连接失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"WSDL连接失败");
		}
		//获取登陆session
		$session_ret = self::vcas_login();
		if($session_ret === false || empty($session_ret['session']))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"登陆DRM-VCAS失败");
		}
		$params['nns_session'] = $session_ret['session'];
		//向DRM添加设备
		include_once dirname(__DIR__) . '/vcas/device.class.php';
		$result = nl_vcas_device::add_device($client, $params);
		$data = self::get_vcas_result($result);
		//添加日志
		$params['nns_drm_tag'] = self::$vcas_tag;
		self::add_drm_log($params, $data, 'device', 'add_device', 'add');
		//销毁登陆状态
		self::vcas_logout($session_ret['session']);
		$code = $data['result']['resultCode'];
		//非0，认为失败
		if($code == 0)
		{
			nl_log_v2_error('nl_m_device','添设备成功，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_OK,"添设备成功");
		}
		nl_log_v2_error('nl_m_device','VCAS-addNetwork返回失败，返回参数:'.var_export($result,true));
		return get_logic_result(ML_STATE_PARAM_ERROR,"VCAS-addNetwork返回失败");

	}

	/**
	 * DRM-VCAS 删除设备
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $dc
	 * @param $params
	 * @return array|bool|mixed|void
	 */
	private static function __delete_device_by_vcas($dc,$params)
	{
		//获取network ID
		$network_id = get_config_v2('g_drm_vcas_network_id');
		if(empty($network_id))
		{
			nl_log_v2_error('nl_m_device','nns_network_id为空');
			return get_logic_result(ML_STATE_PARAM_ERROR,"nns_network_id为空");
		}
		$params['nns_network_id'] = $network_id;
		//获取SOAP俩连接对象
		$client = self::__get_device_by_vcas_client();
		if($client === false)
		{
			nl_log_v2_error('nl_m_device',' WSDL连接失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"WSDL连接失败");
		}
		//获取登陆session
		$session_ret = self::vcas_login();
		if($session_ret === false || empty($session_ret['session']))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"登陆DRM-VCAS失败");
		}
		$params['nns_session'] = $session_ret['session'];
		//向DRM添加设备
		include_once dirname(__DIR__) . '/vcas/device.class.php';

		$result = nl_vcas_device::delete_device($client, $params);
		$data = self::get_vcas_result($result);
		$params['nns_drm_tag'] = self::$vcas_tag;
		self::add_drm_log($params, $data, 'device', 'delete_device', 'delete');
		//销毁登陆状态
		self::vcas_logout($session_ret['session']);
		$code = $data['result']['resultCode'];
		//非0，认为失败
		if($code == 0)
		{
			nl_log_v2_error('nl_m_device','删除设备成功，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_OK,"删除设备成功");
		}else if($code == 206)
		{
			nl_log_v2_error('nl_m_device','设备不存在，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_PARAM_ERROR,"设备已存在");
		}
		nl_log_v2_error('nl_m_device','VCAS-deleteNetwork返回失败，返回参数:'.var_export($result,true));
		return get_logic_result(ML_STATE_PARAM_ERROR,"VCAS-deleteNetwork返回失败");

	}

	/**
	 * DRM-VCAS 禁用设备
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $dc
	 * @param $params
	 * @return array|bool|mixed|void
	 */
	private static function __disable_device_by_vcas($dc,$params)
	{
		//获取network ID
		$network_id = get_config_v2('g_drm_vcas_network_id');
		if(empty($network_id))
		{
			nl_log_v2_error('nl_m_device','nns_network_id为空');
			return get_logic_result(ML_STATE_PARAM_ERROR,"nns_network_id为空");
		}
		$params['nns_network_id'] = $network_id;
		//获取SOAP俩连接对象
		$client = self::__get_device_by_vcas_client();
		if($client === false)
		{
			nl_log_v2_error('nl_m_device',' WSDL连接失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"WSDL连接失败");
		}
		//获取登陆session
		$session_ret = self::vcas_login();
		if($session_ret === false || empty($session_ret['session']))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"登陆DRM-VCAS失败");
		}
		$params['nns_session'] = $session_ret['session'];
		//向DRM添加设备
		include_once dirname(__DIR__) . '/vcas/device.class.php';
		$result = nl_vcas_device::disable_device($client, $params);
		$data = self::get_vcas_result($result);
		$params['nns_drm_tag'] = self::$vcas_tag;
		self::add_drm_log($params, $data, 'device', 'disable_device', 'modify');
		//销毁登陆状态
		self::vcas_logout($session_ret['session']);
		$code = $data['result']['resultCode'];
		//非0，认为失败
		if($code == 0)
		{
			nl_log_v2_error('nl_m_device','禁用设备成功，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_OK,"禁用设备成功");
		}else if($code == 206)
		{
			nl_log_v2_error('nl_m_device','设备不存在，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_PARAM_ERROR,"设备不存在");
		}
		nl_log_v2_error('nl_m_device','VCAS-disableDevices返回失败，返回参数:'.var_export($result,true));
		return get_logic_result(ML_STATE_PARAM_ERROR,"VCAS-disableDevices返回失败");
	}

	/**
	 * DRM-VCAS 启用设备
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $dc
	 * @param $params
	 * @return array|bool|mixed|void
	 */
	private static function __enable_device_by_vcas($dc,$params)
	{
		//获取network ID
		$network_id = get_config_v2('g_drm_vcas_network_id');
		if(empty($network_id))
		{
			nl_log_v2_error('nl_m_device','nns_network_id为空');
			return get_logic_result(ML_STATE_PARAM_ERROR,"nns_network_id为空");
		}
		$params['nns_network_id'] = $network_id;
		//获取SOAP俩连接对象
		$client = self::__get_device_by_vcas_client();
		if($client === false)
		{
			nl_log_v2_error('nl_m_device',' WSDL连接失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"WSDL连接失败");
		}
		//获取登陆session
		$session_ret = self::vcas_login();
		if($session_ret === false || empty($session_ret['session']))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"登陆DRM-VCAS失败");
		}
		$params['nns_session'] = $session_ret['session'];
		//向DRM添加设备
		include_once dirname(__DIR__) . '/vcas/device.class.php';
		$result = nl_vcas_device::enable_device($client, $params);
		$data = self::get_vcas_result($result);
		$params['nns_drm_tag'] = self::$vcas_tag;
		self::add_drm_log($params, $data, 'device', 'disable_device', 'modify');
		//销毁登陆状态
		self::vcas_logout($session_ret['session']);
		$code = $data['result']['resultCode'];
		//非0，认为失败
		if($code == 0)
		{
			nl_log_v2_error('nl_m_device','启用设备成功，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_OK,"禁用设备成功");
		}else if($code == 206)
		{
			nl_log_v2_error('nl_m_device','设备不存在，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_PARAM_ERROR,"设备不存在");
		}
		nl_log_v2_error('nl_m_device','VCAS-enableDevices返回失败，返回参数:'.var_export($result,true));
		return get_logic_result(ML_STATE_PARAM_ERROR,"VCAS-enableDevices返回失败");
	}

	/**
	 * 获取设备接口对象
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @return bool|SOAPClient|string
	 */
	private static function __get_device_by_vcas_client()
	{
		$service_url = get_config_v2('g_drm_vcas_device_manager_service_url');
		if(empty($service_url))
		{
			nl_log_v2_error('nl_m_config','g_drm_vcas_device_manager_service_url 为空或者未配置');
			return false;
		}
		$client = self::get_soap_client($service_url);
		if(is_object($client))
		{
			return $client;
		}
		return false;
	}
}
