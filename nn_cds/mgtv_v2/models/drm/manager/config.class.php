<?php
/**
 * DRM 系统配置相关接口封装
 * Author: 陈波(bo.chen@starcor.cn)
 * Date: 2015/4/1 11:44
 */
include_once dirname(__FILE__).'/common.class.php';
class nl_m_config extends common
{
	/**
	 * 添加管理域
	 * Author: 陈波(bo.chen@starcor.cn)
	 * $params = array(
	 *
	 * )
	 */
	public static function add_network($dc,$params =array())
	{
		nl_log_v2_info('nl_m_config', "add_network传入参数:" . var_export($params, true));
		if (!is_array($params))
		{
			nl_log_v2_error('nl_m_config','add_network传入参数非数组');
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		//判断VCAS是否开启状态
		if(self::get_vcas_enable())
		{
			//VCAS 添加network
			return self::__add_network_by_vacs($dc,$params);
		}

	}

	/**
	 * 删除管理域
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $dc
	 * @param $params
	 * @return array|void
	 */
	public static function delete_network($dc,$params)
	{
		nl_log_v2_info('nl_m_config', "delete_network传入参数:" . var_export($params, true));
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}

		//判断VCAS是否开启状态
		if(self::get_vcas_enable())
		{
			return self::delete_network_by_vcas($dc,$params);
		}

	}

	/**
	 * VCAS 添加network
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $params
	 * @return array|bool
	 */
	private static function __add_network_by_vacs($dc,$params=array())
	{
		$network = array();
		$network['nns_network_id'] = get_config_v2('g_drm_vcas_network_id');
		$network['nns_network_type'] = get_config_v2('g_drm_vcas_network_type');
		if(empty($network['nns_network_id']) || empty($network['nns_network_type']))
		{
			nl_log_v2_error('nl_m_config','nns_network_id或者nns_network_type为空');
			return false;
		}
		//获取sessionHandle
		$session_ret = self::vcas_login();
		if($session_ret === false || empty($session_ret['session']))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"登陆DRM-VCAS失败");
		}
		$network['nns_session'] = $session_ret['session'];
		//获取WSDL连接对象
		$client = self::__get_config_by_vcas_client();
		if($client === false)
		{
			nl_log_v2_error('nl_m_config',' WSDL连接失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"WSDL连接失败");
		}
		//把管理域添加到DRM
		include_once dirname(__DIR__).'/vcas/config.class.php';
		$result = nl_vcas_config::add_network($client,$network);
		$data = self::get_vcas_result($result);
		//添加日志
		$network['nns_drm_tag'] = self::$vcas_tag;
		self::add_drm_log($network, $data, 'config', 'add_network', 'add');
		//销毁登陆状态
		self::vcas_logout($session_ret['session']);
		$code = $data['resultCode'];
		//非0，认为失败
		if($code == 0)
		{
			nl_log_v2_info('nl_m_config','添加管理域成功，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_OK,"添加管理域成功");
		}else if($code == 110)
		{
			nl_log_v2_error('nl_m_config','管理域已存在，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_PARAM_ERROR,"管理域已存在");
		}
		nl_log_v2_error('nl_m_config','VCAS-addNetwork返回失败，返回参数:'.var_export($result,true));
		return get_logic_result(ML_STATE_PARAM_ERROR,"VCAS-addNetwork返回失败");
	}

	/**
	 * VCAS 删除network
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param array $params
	 * @return array|bool
	 */
	private static function delete_network_by_vcas($dc,$params = array())
	{
		$network = array();
		$network['nns_network_id'] = get_config_v2('g_drm_vcas_network_id');
		$network['nns_network_type'] = get_config_v2('g_drm_vcas_network_type');
		if(empty($network['nns_network_id']) || empty($network['nns_network_type']))
		{
			nl_log_v2_error('nl_m_config','nns_network_id或者nns_network_type为空');
			return false;
		}
		//获取sessionHandle
		$session_ret = self::vcas_login();
		if($session_ret === false || empty($session_ret['session']))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"登陆DRM-VCAS失败");
		}
		$network['nns_session'] = $session_ret['session'];
		//获取WSDL连接对象
		$client = self::__get_config_by_vcas_client();
		if($client === false)
		{
			nl_log_v2_error('nl_m_config',' WSDL连接失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"WSDL连接失败");
		}
		//把管理域添加到DRM
		include_once dirname(__DIR__).'/vcas/config.class.php';
		$result = nl_vcas_config::delete_network($client,$network);
		$data = self::get_vcas_result($result);
		//添加日志
		$network['nns_drm_tag'] = self::$vcas_tag;
		self::add_drm_log($network, $data, 'config', 'add_network', 'add');
		//销毁登陆状态
		self::vcas_logout($session_ret['session']);
		$code = $data['resultCode'];
		//非0，认为失败
		if($code == 0)
		{
			nl_log_v2_info('nl_m_config','删除管理域成功，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_OK,"删除管理域成功");
		}else if($code == 217)
		{
			nl_log_v2_error('nl_m_config','管理域不存在，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_PARAM_ERROR,"管理域已存在");
		}
		nl_log_v2_error('nl_m_config','VCAS-removeNetwork返回失败，返回参数:'.var_export($result,true));
		return get_logic_result(ML_STATE_PARAM_ERROR,"VCAS-removeNetwork返回失败");
	}

	/**
	 * 获取配置接口对象
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @return bool|SOAPClient|string
	 */
	private static function __get_config_by_vcas_client()
	{
		$service_url = get_config_v2('g_drm_vcas_config_manager_service_url');
		if(empty($service_url))
		{
			nl_log_v2_error('nl_m_config','g_drm_vcas_config_manager_service_url 为空或者未配置');
			return false;
		}
		$client = self::get_soap_client($service_url);
		if(is_object($client))
		{
			return $client;
		}
		return false;
	}

}