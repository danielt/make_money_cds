<?php
/**
 * Author: 陈波(bo.chen@starcor.cn)
 * Date: 2015/3/20
 * Time: 19:35
 */
include_once dirname(__FILE__).'/common.class.php';
class nl_m_product extends common
{
	/**
	 * 添加产品包
	 * Author: 陈波(bo.chen@starcor.cn)
	 * $params = array(
	 *		'nns_package_id' => '产品包ID',
	 * 		'nns_description' => '产品包描述',
	 * )
	 */
	public static function add_package($dc,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		//定义需要检查字段
		$filed_arr = array(
			'nns_package_id',
			'nns_description',
		);
		//检查失败，直接返回错误
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		//判断VCAS是否开启状态
		if(self::get_vcas_enable())
		{
			return self::__add_package_by_vcas($dc,$params);
		}

	}

	/**
	 * 添加产品包
	 * Author: 陈波(bo.chen@starcor.cn)
	 * $params = array(
	 *  	'nns_package_id' => '产品包ID',
	 * 		'nns_description' => '产品包描述',
	 * )
	 */
	public static function delete_package($dc,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		//定义需要检查字段
		$filed_arr = array(
			'nns_package_id',
			'nns_description',
		);
		$check_res = self::check_params($params,$filed_arr);
		//检查失败，直接返回错误
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		//判断VCAS是否开启状态
		if(self::get_vcas_enable())
		{
			return self::__delete_package_by_vcas($dc,$params);
		}
	}

	/**
	 * 获取产品包接口对象
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @return bool|SOAPClient|string
	 */
	private static function __get_package_by_vcas_client()
	{
		$service_url = get_config_v2('g_drm_vcas_entitlement_manager_service_url');
		if(empty($service_url))
		{
			nl_log_v2_error('nl_m_product','g_drm_vcas_entitlement_manager_service_url 为空或者未配置');
			return false;
		}
		include_once dirname(__DIR__).'/vcas/entitlement.class.php';
		$client = self::get_soap_client($service_url);
		if(is_object($client))
		{
			return $client;
		}
		return false;
	}

	/**
	 * 添加产品包
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $dc
	 * @param $params
	 * @return array|bool
	 */
	private static function __add_package_by_vcas($dc,$params)
	{
		//获取登陆session
		$session_ret = self::vcas_login();
		if($session_ret === false)
		{
			nl_log_v2_error('nl_m_product',' 登陆DRM-VCAS失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"登陆DRM-VCAS失败");
		}
		$params['nns_session'] = $session_ret['session'];
		//获取WSDL对象
		$client = self::__get_package_by_vcas_client();
		if($client === false)
		{
			nl_log_v2_error('nl_m_product',' WSDL连接失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"WSDL连接失败");
		}
		//向DRM同步产品包
		$result = nl_vcas_entitlement::add_package($client,$params);
		$data = self::get_vcas_result($result);
		//添加接口交互日志
		$params['nns_drm_tag'] = self::$vcas_tag;
		self::add_drm_log($params, $data, 'media', '__add_package_by_vcas', 'add');
		//销毁登陆状态
		self::vcas_logout($session_ret['session']);
		$code  = $data['result']['resultCode'];
		//非0，认为失败
		if($code === 0)
		{
			nl_log_v2_info('nl_m_product',' 添加产品包成功');
			return get_logic_result(ML_STATE_OK,"添加产品包成功");
		}else if($code == 201)
		{
			nl_log_v2_error('nl_m_product','产品包已存在，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_PARAM_ERROR,"产品包已存在");
		}
		nl_log_v2_error('nl_m_product','VCAS-addPackage，返回参数:'.var_export($result,true));
		return get_logic_result(ML_STATE_PARAM_ERROR,"VCAS-addPackage 返回失败");

	}

	/**
	 * 删除产品包
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $dc
	 * @param $params
	 * @return array|bool
	 */
	private static function __delete_package_by_vcas($dc,$params)
	{
		//获取登陆session
		$session_ret = self::vcas_login();
		if($session_ret === false || empty($session_ret['session']))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"登陆DRM-VCAS失败");
		}
		$params['nns_session'] = $session_ret['session'];
		//获取WSDL对象
		$client = self::__get_package_by_vcas_client();
		if($client === false)
		{
			nl_log_v2_error('nl_m_product',' WSDL连接失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"WSDL连接失败");
		}

		include_once dirname(__DIR__).'/vcas/entitlement.class.php';
		$result = nl_vcas_entitlement::delete_package($client,$params);
		$data = self::get_vcas_result($result);
		$params['nns_drm_tag'] = self::$vcas_tag;
		self::add_drm_log($params, $data, 'media', '__delete_package_by_vcas', 'add');
		//销毁登陆状态
		self::vcas_logout($session_ret['session']);
		$code  = $data['result']['resultCode'];
		//非0，认为失败
		if($code == 0)
		{
			nl_log_v2_info('nl_m_product',' 删除产品包成功');
			return get_logic_result(ML_STATE_OK,"删除产品包成功");
		}else if($code == 202)
		{
			nl_log_v2_error('nl_m_product','产品包不存在，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_PARAM_ERROR,"产品包不存在");
		}
		nl_log_v2_error('nl_m_product','VCAS-removePackage，返回参数:'.var_export($result,true));
		return get_logic_result(ML_STATE_PARAM_ERROR,"VCAS-removePackage 返回失败");

	}
}