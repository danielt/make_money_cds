<?php
/**
 * @author yunping.yang
 * @date   2015-3-26
 */
include_once dirname(__FILE__) . '/common.class.php';
//include_once dirname(dirname(dirname(__FILE__))) . "/common/v2/global.include.php";
include_once NPDIR . 'np_debug.class.php';

class nl_m_drm_log
{
	//定义表名
	private static $vod_table_name = 'nns_drm_log';

	/**
	 * 添加用户
	 * @param $dc     DC对象
	 * @param $params KEY-VALUE键值对
	 *                array(KEY1=>VALUE1,KEY2=>VALUE2)
	 * @return bool
	 *                String 成功,返回数据ID，FALSE 失败
	 */
	public static function add($dc, $params)
	{

		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR, "params format is wrong");
		}

		$params["nns_id"] = np_guid_rand();
		$datetime = date("Y-m-d H:i:s");
		$params["nns_modify_time"] = $datetime;
		$params["nns_create_time"] = $datetime;

		$model = m_factory::model_load(self::$vod_table_name);
		$re = $model->_insert($dc, $params);
		return $re;
	}

	/**
	 * 修改用户信息
	 * @param $dc     DC对象
	 * @param $id     ID
	 * @param $params 需要修改的KEY-VALUE键值对
	 *                array(KEY1=>VALUE1,KEY2=>VALUE2)
	 * @return bool
	 *                TRUE 成功，FALSE 失败
	 */
	public static function modify($dc, $where, $params)
	{
		if (!is_array($params) || !is_array($where))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR, "params format is wrong");
		}
		$model = m_factory::model_load(self::$vod_table_name);
		$datetime = date("Y-m-d H:i:s");
		$params["nns_modify_time"] = $datetime;

		$re = check::modify_before($dc, $where, $params);
		if ($re["code"] !== ML_STATE_OK)
		{
			m_model::rollback_trans($dc);

			return $re;
		}
		$re = $model->modify($dc, $where, $params);

		if ($re["code"] !== ML_STATE_OK)
		{
			m_model::rollback_trans($dc);

			return $re;
		}
	}


	/**
	 * 根据ID组获取VOD信息列表
	 * @param $dc   DC对象
	 * @param $ids  ID 数组
	 * @return bool
	 *              TRUE 成功 FALSE 失败
	 *              array 数据集合，以id为KEY返回
	 */
	public static function get_drm_log_by_ids($dc, $ids)
	{
		if (!is_array($ids) || empty($ids))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR, "params format is wrong");
		}
		$model = m_factory::model_load(self::$vod_table_name);
		$where = array ();
		$where["nns_id"] = $ids;
		$re = $model->query($dc, $where);
		if ($re["code"] === ML_STATE_OK && is_array($re["data"]))
		{
			$data = $re["data"];
			$data = np_array_rekey($data, "nns_id");
			$re["data"] = $data;
		}

		return $re;
	}

	/**
	 * 通用查询方法封装
	 * @param       $dc           DC对象
	 * @param       $limit        边界值
	 *                            array(min,count)
	 * @param       $orderby      排序
	 *                            array(key=>asc|desc)
	 * @param array $wheres       where条件查询 多个条件按and连接
	 *                            array(key1=>array(1,2,3),key2=>2) 表示 key1 in (1,2,3) and key2=2
	 * @param array $likes        like条件查询 多个条件按and连接
	 *                            array(key1=>array(1,2,3),key2=>2) 表示 (key1 like 1 or key1 like 2 or key1 like 3) and key2 like 2
	 * @param array $where_or     where条件查询 多个条件按or连接
	 *                            array(key1=>array(1,2,3),key2=>2) 表示 key1 in (1,2,3) or key2=2
	 * @param array $like_or      like条件查询 多个条件按or连接
	 *                            array(key1=>array(1,2,3),key2=>2) 表示 (key1 like 1 or key1 like 2 or key1 like 3) or key2 like 2
	 * @param array $custom_where 自定义查询条件，多个条件按and连接
	 *                            array(
	 *                            " nns_creat_time > '2014-12-12 14:00:00' and nns_create_time<'2015-1-1 11:00:00'",
	 *                            " nns_modify_time > '2014-12-12 14:00:00' and nns_modify_time<'2015-1-1 11:00:00'",
	 *                            )
	 *                            表示 (nns_creat_time > '2014-12-12 14:00:00' and nns_create_time<'2015-1-1 11:00:00') and (nns_modify_time > '2014-12-12 14:00:00' and nns_modify_time<'2015-1-1 11:00:00')
	 * @return bool | array
	 *                            TRUE 数据为空 FALSE 数据库查询错误  ARRAY 数据结果集
	 */
	public static function get_drm_log_list($dc, $limit, $orderby, $wheres = array (), $likes = array (), $where_or = array (), $like_or = array (), $custom_where = array ())
	{
		$model = m_factory::model_load(self::$vod_table_name);

		$re = $model->query($dc, $limit, $orderby, $wheres, $likes, $where_or, $like_or, $custom_where);

		//        $sql = $db_inst->where($wheres)->where($where_or,"or")->like($likes)->like($like_or,"or")->custom_where($custom_where)->orderby($orderby)->query();

		return $re;
	}

	/**
	 * 通用统计方法封装
	 * @param       $dc       DC对象
	 * @param array $wheres   where条件查询 多个条件按and连接
	 *                        array(key1=>array(1,2,3),key2=>2) 表示 key1 in (1,2,3) and key2=2
	 * @param array $likes    like条件查询 多个条件按and连接
	 *                        array(key1=>array(1,2,3),key2=>2) 表示 (key1 like 1 or key1 like 2 or key1 like 3) and key2 like 2
	 * @param array $where_or where条件查询 多个条件按or连接
	 *                        array(key1=>array(1,2,3),key2=>2) 表示 key1 in (1,2,3) or key2=2
	 * @param array $like_or  like条件查询 多个条件按or连接
	 *                        array(key1=>array(1,2,3),key2=>2) 表示 (key1 like 1 or key1 like 2 or key1 like 3) or key2 like 2
	 * @return bool | int
	 *                        FALSE 数据库查询错误  count数
	 */
	public static function count_drm_log_list($dc, $wheres = array (), $likes = array (), $where_or = array (), $like_or = array (), $custom_where = array ())
	{
		$model = m_factory::model_load(self::$vod_table_name);
		$re = $model->count($dc, $wheres, $likes, $where_or, $like_or, $custom_where);

		return $re;
	}
}
