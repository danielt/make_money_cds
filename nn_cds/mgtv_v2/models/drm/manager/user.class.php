<?php
/**
 * DRM系统登录
 * Author: 陈波(bo.chen@starcor.cn)
 * Date: 2015/3/20 19:34
 */

include_once dirname(__FILE__).'/common.class.php';
include_once dirname(dirname(__FILE__)).'/vcas/admin.class.php';
class nl_m_user extends common
{

	/**
	 * VCAS 登陆
	 * Author: 陈波(bo.chen@starcor.cn)
	 * $params = array(  可以为空，如果为空从配置文件中获取对应的账号密码
	 * 		'nns_user_name'=>'登录用户名',
	 * 		'nns_password'=>'登录密码',
	 * )
	 */

	public static function vcas_login($params = array())
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		include_once dirname(__DIR__).'/vcas/admin.class.php';
		//如果没有传入账号密码，直接从配置文件中获取
		if(!isset($params['nns_user_name']) || !isset($params['nns_password']))
		{
			$params['nns_user_name'] = get_config_v2('g_drm_vcas_manager_user_name');
			$params['nns_password'] = get_config_v2('g_drm_vcas_manager_password');
		}
		$service_url = get_config_v2('d_drm_vcas_admin_manager_service_url');
		if(empty($service_url))
		{
			nl_log_v2_error('nl_config','d_drm_vcas_admin_manager_service_url为空或者未配置');
			return false;
		}
		//SOAP对象
		$client = self::get_soap_client($service_url);
		//登录请求
		$result = nl_vcas_admin::login($client,$params);
		//解析返回结果
		$data = self::get_vcas_result($result);
		//设置日志所属DRM厂商
		$params['nns_drm_tag'] = self::$vcas_tag;
		//添加日志
		self::add_drm_log($params,$result,'admin','signOn','login');
		//非0，认为失败
		if($data['result']['resultCode'] != 0)
		{
			nl_log_v2_error('nl_config','VCAS-login返回失败，返回参数:'.var_export($result,true));
			return false;
		}
		//定义返回结果
		$result = array();
		$result['session'] = $data['sessionHandle']['handle'];
		return $result;
	}


	/**
	 * VCAS注销
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $dc
	 * @param $params
	 * @return array|void
	 */
	public static function vcas_logout($params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		$filed_arr = array(
			'nns_session',
		);
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		$service_url = get_config_v2('d_drm_vcas_admin_manager_service_url');
		if(empty($service_url))
		{
			nl_log_v2_error('nl_config','d_drm_vcas_admin_manager_service_url为空或者未配置');
			return false;
		}
		//SOAP对象
		$client = self::get_soap_client($service_url);

		include_once dirname(__DIR__).'/vcas/admin.class.php';
		$user = new stdClass();
		$user->SessionHandle = $params['nns_session'];
		//DRM注销接口
		$result = nl_vcas_admin::logout($client,$user);
		//解析DRM返回结果
		$data = self::get_vcas_result($result);
		//日志所属DRM厂商
		$params['nns_drm_tag'] = self::$vcas_tag;
		//添加日志
		self::add_drm_log($params,$result,'admin','signOn','login');
		//非0，认为失败
		if($data['result']['resultCode'] != 0)
		{
			return false;
		}
		return true;

	}
}