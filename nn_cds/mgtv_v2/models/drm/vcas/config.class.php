<?php
/**
 * VCAS系统配置相关接口
 * Author: 陈波(bo.chen@starcor.cn)
 * Date: 2015/3/20 18:52
 */

class nl_vcas_config
{
	/**
	 * 添加管理域名
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function add_network($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$network = self::get_vcas_network_object($params);
		$session = self::get_vcas_session_object($params);
//		echo '<pre>';
//		var_dump($network);
//		var_dump($session);exit;
		return $client->addNetwork($network,$session);
	}

	/**
	 * 删除管理域名
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function delete_network($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$network = self::get_vcas_network_object($params);
		$session = self::get_vcas_session_object($params);
		return $client->removeNetwork($network,$session);
	}

	/**
	 * 获取network 列表
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function get_network_list($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$session = self::get_vcas_session_object($params);
		return $client->getNetworkList($session);
	}

	/**
	 * 生成VCAS系统network数据对象
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $params = array(
	 *                'nns_network_id',
	 *                'nns_network_type'
	 * )
	 * @return stdClass
	 */
	static public function get_vcas_network_object($params)
	{
		$network = new stdClass();
		$network->smsNetworkId = $params['nns_network_id'];
		$network->networkType = $params['nns_network_type'];
		return $network;
	}
	/**
	 * 生成VCAS系统session Handle数据对象
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $params = array(
	 *                'nns_session_handle'
	 * )
	 * @return stdClass
	 */
	static public function get_vcas_session_object($params)
	{
		$session_handle = new stdClass();
		$session_handle->handle = $params['nns_session'];
		return $session_handle;
	}
}