<?php
/**
 * VCAS系统的设备相关接口
 * Author: 陈波(bo.chen@starcor.cn)
 * Date: 2015/3/20 18:47
 */
class nl_vcas_device
{
	/**
	 * 添加设备
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function add_device($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$device_list = self::get_vcas_device_object($params);
		$session = self::get_vcas_session_object($params);
		return $client->addDevices($device_list,$session);
	}

	/**
	 * 删除设备
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function delete_device($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$device_list = self::get_vcas_device_object($params);
		$session = self::get_vcas_session_object($params);

		return $client->removeDevices($device_list,$session);
	}

	/**
	 * 启用设备
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function enable_device($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$device_list = self::get_vcas_device_object($params);
		$session = self::get_vcas_session_object($params);
		return $client->enableDevices($device_list,$session);
	}

	/**
	 * 禁用设备
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function disable_device($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$device_list = self::get_vcas_device_object($params);
		$session = self::get_vcas_session_object($params);
		return $client->disableDevices($device_list,$session);
	}

	/**
	 * 生成VCAS系统的设备数据对象
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $params
	 * @return array|stdClass
	 */
	public static function get_vcas_device_object($params)
	{
		$device = new stdClass();
		$device->smsDeviceId = $params['nns_drm_device_id'];
		$device->smsNetworkId = $params['nns_network_id'];
		$device->deviceType = $params['nns_device_type'];
		$device->networkDeviceId = $params['nns_device_id'];
		$device_list = array();
		$device_list[] = $device;

		return $device_list;

	}

	/**
	 * 生成VCAS系统session Handle数据对象
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $params = array(
	 *                'nns_session_handle'
	 * )
	 * @return stdClass
	 */
	static public function get_vcas_session_object($params)
	{
		$session_handle = new stdClass();
		$session_handle->handle = $params['nns_session'];
		return $session_handle;
	}

}