<?php
/**
 * VCAS系统登陆相关接口
 * Author: 陈波(bo.chen@starcor.cn)
 * Date: 2015/3/20 18:46
 */
class nl_vcas_admin
{
	/**
	 * 登录VCAS系统
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function login($client,$user_attributes)
	{
		if(!is_object($client))
		{
			return false;
		}
		if(empty($user_attributes['nns_user_name']) || empty($user_attributes['nns_password']))
		{
			return false;
		}
		//组装登录参数
		$user = new stdClass();
		$user->userName = $user_attributes['nns_user_name'];
		$user->password = $user_attributes['nns_password'];
		return $client->signOn($user);
	}

	/**
	 * 注销VCAS系统
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function logout($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		if(empty($params->handle))
		{
			return false;
		}
		return $client->signOff($params);
	}
}