<?php
/**
 * VCAS系统的片源相关接口
 * Author: 陈波(bo.chen@starcor.cn)
 * Date: 2015/3/20 18:52
 */

class nl_vcas_content
{
	/**
	 * 添加片源
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function add_content($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$content = self::get_content_object($params);
		$session = self::get_vcas_session_object($params);
		return $client->addContent($content,$session);
	}

	/**
	 * 删除片源
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function delete_content($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$content = self::get_content_object($params);
		$session = self::get_vcas_session_object($params);
		return $client->removeContent($content,$session);
	}

	/**
	 * 添加片源到管理域
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function add_content_to_network($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$cton = self::get_content_to_network_object($params);
		$session = self::get_vcas_session_object($params);
		return $client->addContentToNetwork($cton,$session);
	}

	/**
	 * 移除片源和管理域的所属关系
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function delete_content_from_network($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$cton = self::get_content_to_network_object($params);
		$session = self::get_vcas_session_object($params);
		return $client->removeContentFromNetwork($cton,$session);
	}

	/**
	 * 添加片源内容
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function add_event($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$event = self::get_event_object($params);
		$session = self::get_vcas_session_object($params);
//		echo "<pre>";
//		var_dump($event);exit;
		return $client->addEvents($event,$session);
	}

	/**
	 * 删除片源内容
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function delete_event($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$event = self::get_event_object($params);
		$session = self::get_vcas_session_object($params);
		return $client->removeEvents($event,$session);
	}

	/**
	 * 添加片源内容到产品包
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function add_event_to_package($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$package = self::get_event_to_package_object($params);
		$event_list = self::get_event_object($params);
		$session = self::get_vcas_session_object($params);
		return $client->addEventsToPackage($package,$event_list,$session);
	}

	/**
	 * 添加片源内容到产品包
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function delete_event_to_package($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$package = self::get_event_to_package_object($params);
		$event_list = self::get_event_object($params);
		$session = self::get_vcas_session_object($params);
		return $client->removeEventsFromPackage($package,$event_list,$session);
	}

	/**
	 * 生成VCAS系统的片源内容数据对象
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $params
	 * @return stdClass
	 */
	public static function get_event_object($params)
	{

		//片源内容的有效期
		$time_period_rule = new stdClass();
//		$time_period_rule->startTime = $params['nns_start_time'];
//		$time_period_rule->endTime = $params['nns_end_time'];
		//片源内容信息
		$event = new stdClass();
		$event->smsEventId = $params['nns_content_id'];
		$event->smsContentId = $params['nns_media_id'];
		$event->smsNetworkId = $params['nns_network_id'];
		$event->exclusive = 'true';
		$event->timePeriodRule = $time_period_rule;
		$event->preStartDuration = 0;
		//片源内容列表
		$event_list = array();
		$event_list[] = $event;

		return $event_list;
	}


	/**
	 * 生成VCAS系统的片源数据对象
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $params
	 * @return array|stdClass
	 */
	public static function get_content_object($params)
	{

		$media = new stdClass();
		$media->smsContentId = $params['nns_media_id'];
		if(!empty($params['nns_description']))
		{
			$media->description = $params['nns_description'];
		}
		if(!empty($params['nns_tag']))
		{
			$media->tag = $params['nns_tag'];
		}
		$media_list = array();
		$media_list[] = $media;

		return $media_list;

	}

	/**
	 * 生成VCAS系统的片源添加到管理域的数据对象
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $params
	 * @return stdClass
	 */
	public static function get_content_to_network_object($params)
	{
		$network_content = new stdClass();
		$network_content->smsNetworkId = $params['nns_network_id'];
		$network_content->smsContentId = $params['nns_media_id'];
		$network_content->networkContentId = $params['nns_media_id'];
		$network_content->networkContentType = isset($params['nns_media_type'])?$params['nns_media_type']:'VOD'; //DTV或者VOD

		return $network_content;
	}

	/**
	 * 生成VCAS系统的片源内容添加到产品包的数据对象
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $params
	 * @return stdClass
	 */
	public static function get_event_to_package_object($params)
	{
		$package = new stdClass();
		$package->smsPackageId = $params['nns_package_id'];
		$package->description = $params['nns_description'];
		return $package;
	}

	/**
	 * 生成VCAS系统session Handle数据对象
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $params = array(
	 *                'nns_session'
	 * )
	 * @return stdClass
	 */
	static public function get_vcas_session_object($params)
	{
		$session_handle = new stdClass();
		$session_handle->handle = $params['nns_session'];
		return $session_handle;
	}

}