<?php
/**
 * VCAS系统的授权相关接口
 * Author: 陈波(bo.chen@starcor.cn)
 * Date: 2015/3/20 18:41
 */

class nl_vcas_entitlement
{
	/**
	 * 添加产品包
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function add_package($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$package_list = self::get_package_list_object($params);
		$session = self::get_vcas_session_object($params);
		return $client->addPackages($package_list,$session);
	}

	/**
	 * 删除产品包
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function delete_package($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$package_list = self::get_package_list_object($params);
		$session = self::get_vcas_session_object($params);
		return $client->removePackages($package_list,$session);
	}

	/**
	 * 片源内容绑定产品包
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function add_event_to_package($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		return $client->addEventsToPackage($params);
	}

	/**
	 * 片源内容取消绑定产品包
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function delete_event_from_package($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		return $client->removeEventsFromPackage($params);
	}

	/**
	 * 添加授权关系
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function add_entitlement($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$entitlement_list = self::get_entitlement_object($params);
		$session = self::get_vcas_session_object($params);
		return $client->addEntitlements($entitlement_list,$session);
	}

	/**
	 * 删除授权关系
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function delete_entitlement($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$entitlement_list = self::get_entitlement_object($params);
		$session = self::get_vcas_session_object($params);
		return $client->removeEntitlements($entitlement_list,$session);
	}

	/**
	 * 修改授权关系
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function modify_entitlement($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$entitlement_list = self::get_entitlement_object($params);
		$session = self::get_vcas_session_object($params);
		return $client->modifyEntitlements($entitlement_list,$session);
	}

	/**
	 * 删除设备的所有授权
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function delete_devicet_entitlement($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$entitlement_list = self::get_delete_device_entitlement_object($params);
		$session = self::get_vcas_session_object($params);
		return $client->removeAllEntitlements($entitlement_list,$session);
	}

	/**
	 * 获取授权关系列表
	 * @param $client
	 * @param $params
	 * @return bool
	 */
	static public function get_entitlement_list($client,$params)
	{
		if(!is_object($client))
		{
			return false;
		}
		$entitlement_list = self::get_entitlement_object($params);
		$session = self::get_vcas_session_object($params);
		return $client->getEntitlementList($entitlement_list,$session);
	}
	/**
	 * 生成VCAS系统的产品包列表数据对象
	 * @param $params
	 * @return stdClass
	 */
	public static function get_package_list_object($params)
	{

		//产品包信息
		$package = new stdClass();
		$package->smsPackageId = $params['nns_package_id'];
		$package->description = $params['nns_description'];
		$package_list = array();
		$package_list[] = $package;
		return $package_list;
	}
	/**
	 * 生成VCAS系统的产品包数据对象
	 * @param $params
	 * @return stdClass
	 */
	public static function get_package_object($params)
	{

		//产品包信息
		$package = new stdClass();
		$package->smsPackageId = $params['nns_package_id'];
		$package->description = $params['nns_description'];
		return $package;
	}

	/**
	 * 生成VCAS系统的授权数据对象
	 * @param $params
	 * @return stdClass
	 */
	public static function get_entitlement_object($params)
	{

		//授权关系有效期
		$time_period_rule = new stdClass();
		$time_period_rule->startTime = $params['nns_start_time'];
		$time_period_rule->endTime = $params['nns_end_time'];


		//设备信息
		$device = new stdClass();
		$device->entityType = $params['nns_device_type'];//DEVICE、DOMAIN、RETAILER
		$device->entityId = $params['nns_device_id'];
		//片源内容信息
		$package = self::get_package_object($params);
		//授权关系接口
		$entitlement = new stdClass();
		$entitlement->smsEntitlementId = $params['nns_auth_id'];
		$entitlement->package = $package;
		$entitlement->entitledEntity = $device;
		$entitlement->timePeriodRule = $time_period_rule;
		$entitlement_list = array();
		$entitlement_list[] = $entitlement;

		return $entitlement_list;
	}
	/**
	 * 生成删除指定条件的授权关系数据对象
	 * @param $params
	 * @return stdClass
	 */
	public static function get_delete_device_entitlement_object($params)
	{
		//设备信息
		$entitled_entity = new stdClass();
		$entitled_entity->entityType = empty($params['nns_device_type'])?'DEVICE':$params['nns_device_type'];//DEVICE、DOMAIN、RETAILER
		$entitled_entity->entityId = $params['nns_device_id'];
		$entitled_entity_list = array();
		$entitled_entity_list[] = $entitled_entity;
		return $entitled_entity_list;
	}

	/**
	 * 生成查询授权关系数据对象
	 * @param $params
	 * @return stdClass
	 */
	public static function get_query_entitlement_object($params)
	{
		//查询条件
		$entitlement_list_query = new stdClass();
		$entitlement_list_query->smsEntitlementId = $params['nns_device_id'];
		$entitlement_list_query->entityType = $params['nns_device_type']; //DEVICE、DOMAIN、RETAILER
		$entitlement_list_query->entitlementCount = $params['nns_count'];
		return $entitlement_list_query;
	}

	/**
	 * 生成VCAS系统session Handle数据对象
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $params = array(
	 *                'nns_session'
	 * )
	 * @return stdClass
	 */
	static public function get_vcas_session_object($params)
	{
		$session_handle = new stdClass();
		$session_handle->handle = $params['nns_session'];
		return $session_handle;
	}

}