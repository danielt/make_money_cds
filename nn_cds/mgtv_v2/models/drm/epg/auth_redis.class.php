<?php
/**
 * 负责管理REDIS中的授权
 * Author: 陈波(bo.chen@starcor.cn)
 * Date: 2015/3/20 19:37
 */
class nl_m_auth_redis extends common
{
	private static $drm_user_auth_table = 'drm_user_auth_to_redis';
	private static $redis_auth_id = '';

	public function __construct($wsdl)
	{

	}

	/**
	 * 添加REDIS授权关系
	 * Author: 陈波(bo.chen@starcor.cn)
	 * $params = array(
	 * 'user_id'=>'用户ID',
	 * 'product_id'=>'产品包ID',
	 * 'media_content_id'=>'片源内容ID',
	 * 'user_valid_start_time'=>'产品有效开始时间',
	 * 'user_valid_end_time'=>'产品有效结束时间',
	 * 'temp_valid_start_time'=>'临时有效开始时间',
	 * 'temp_valid_end_time'=>'临时有效结束时间',
	 * 'expire_hour'=>'过期小时，用于定时删除过期授权',
	 * 'update_time'=>'修改授权时间',
	 * 'entitlement_id'=>'DRM授权关系ID',
	 * )
	 */
	public static function add_auth($redis,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		$filed_arr = array(
			'user_id',
			'product_id',
			'content_id',
			'user_valid_start_time',
			'user_valid_end_time',
			'temp_valid_start_time',
			'temp_valid_end_time',
			'expire_hour',
			'update_time',
			'entitlement_id',
		);
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		//获取redis 主键
		$str_id = $params['entitlement_id'];

		//判断表是否存在
		$bool_exists = $redis->hash_table_exists(self::$drm_user_auth_table);
		if(!$bool_exists)
		{
			//创建表，指定表字段以及需要创建索引的字段
			$redis->hash_create_table(self::$drm_user_auth_table,$filed_arr,array("user_id","content_id","expire_hour"));
		}
		//如果$str_id存在则修改对应数据，否则添加
		$bool_re = $redis->hash_table(self::$drm_user_auth_table)->hash_update($str_id,$params);
		return $bool_re;

	}

	/**
	 * 删除REDIS授权关系
	 * Author: 陈波(bo.chen@starcor.cn)
	 * $params = array(
	 * '#id'=>'REDIS主键ID',
	 * 'user_id'=>'用户ID',
	 * 'media_content_id'=>'片源内容ID',
	 * 'expire_hour'=>'过期小时，用于定时删除过期授权',
	 * )
	 */
	public static function delete_auth($redis,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		$where = array();
		//根据redis主键ID
		if(!empty($params['#id']))
		{
			$where['#id'] = $params['#id'];
		}
		//根据用户ID
		if(!empty($params['user_id']))
		{
			$where['user_id'] = $params['user_id'];
		}
		//根据片源内容ID
		if(!empty($params['content_id']))
		{
			$where['content_id'] = $params['content_id'];
		}
		//根据小时
		if(!empty($params['expire_hour']))
		{
			$where['expire_hour'] = $params['expire_hour'];
		}
		//如果条件为空返回错误
		if(empty($where))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params cannot be empty");
		}
		$bool_re = $redis->hash_table(self::$drm_user_auth_table)->hash_where($where)->hash_delete();

		return $bool_re;
	}

	/**
	 * 获取授权关系
	 * @param $dc
	 * @param $params
	 * @return array|bool
	 */
	public static function get_auth_list($redis,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		$filed_arr = array(
			'page_index',
			'page_size',
		);
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		$where = array();
		//根据用户ID
		if(!empty($params['user_id']))
		{
			$where['user_id'] = $params['user_id'];
		}
		//根据片源内容ID
		if(!empty($params['content_id']))
		{
			$where['content_id'] = $params['content_id'];
		}

		$limit=array(
			$params['page_index']*$params['page_size'],
			$params['page_size']
		);
		$re = $redis->hash_table(self::$drm_user_auth_table)->hash_orderby("update_time","desc")->hash_where($where)->hash_query("*",$limit,0,true);
		return $re;
	}
}
