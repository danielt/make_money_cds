<?php
/**
 * DRM系统授权-通过接口
 * Author: 陈波(bo.chen@starcor.cn)
 * Date: 2015/3/20 19:37
 */
include_once dirname(__FILE__).'/common.class.php';
class nl_m_auth_drm extends common
{

	public function __construct($wsdl)
	{
		self::$client = $this->get_soap_client($wsdl);
	}

	/**
	 * 添加授权关系
	 * Author: 陈波(bo.chen@starcor.cn)
	 * $params = array(
	 *
	 * )
	 */
	public static function add_auth($dc,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		$filed_arr = array(
			'nns_device_id',
			'nns_auth_id',
			'nns_package_id',
			'nns_description'
		);
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		//判断VCAS是否开启状态
		if(self::get_vcas_enable())
		{
			return self::__add_auth_by_vcas($dc,$params);
		}
	}

	/**
	 * 修改授权关系
	 * Author: 陈波(bo.chen@starcor.cn)
	 * $params = array(
	 *
	 * )
	 */
	public static function modify_auth($dc,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		$filed_arr = array(
			'nns_start_time',
			'nns_end_time',
			'nns_device_id',
			'nns_auth_id',
			'nns_package_id',
			'nns_description'
		);
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		//判断VCAS是否开启状态
		if(self::$vcas_enable)
		{

		}
	}

	/**
	 * 删除授权关系
	 * Author: 陈波(bo.chen@starcor.cn)
	 * $params = array(
	 *
	 * )
	 */
	public static function delete_auth($dc,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		$filed_arr = array(
			'nns_device_id',
			'nns_auth_id',
			'nns_package_id',
			'nns_description'
		);
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		//判断VCAS是否开启状态
		if(self::get_vcas_enable())
		{
			return self::__delete_auth_by_vcas($dc,$params);
		}
	}

	/**
	 * 删除设备授权关系
	 * Author: 陈波(bo.chen@starcor.cn)
	 * $params = array(
	 *  'nns_device_type' => '设备类型', 默认DEVICE
	 *  'nns_device_id' => '设备ID',
	 * )
	 */
	public static function delete_device_auth($dc,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		$filed_arr = array(
			'nns_device_id',
		);
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		//判断VCAS是否开启状态
		if(self::get_vcas_enable())
		{
			return self::__delete_device_auth_by_vcas($dc,$params);

		}
	}

	/**
	 * 获取授权关系
	 * @param $dc
	 * @param $params
	 * @return array|bool
	 */
	public static function get_auth_list($dc,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		$filed_arr = array(
			'nns_device_type',
			'nns_device_id',
			'nn_count',
		);
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		//判断VCAS是否开启状态
		if(self::$vcas_enable)
		{
			include_once dirname(__DIR__).'/vcas/entitlement.class.php';
			$soap_re = nl_vcas_entitlement::get_entitlement_list(self::$client,$params);
			$result = self::get_vcas_result($dc,$soap_re);
			$params['nns_drm_tag'] = self::$vcas_tag;
		}

		//添加日志
		self::add_drm_log($params, $soap_re, 'auth', 'get_auth', 'get');
		if ($result['code'] !== ML_STATE_OK)
		{
			return false;
		}
		return $result;
	}

	/**
	 * 获取授权接口对象
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @return bool|SOAPClient|string
	 */
	private static function __get_auth_by_vcas_client()
	{
		$service_url = get_config_v2('g_drm_vcas_entitlement_manager_service_url');
		if(empty($service_url))
		{
			nl_log_v2_error('nl_m_auth_drm','g_drm_vcas_entitlement_manager_service_url 为空或者未配置');
			return false;
		}
		include_once dirname(__DIR__).'/vcas/entitlement.class.php';
		$client = self::get_soap_client($service_url);
		if(is_object($client))
		{
			return $client;
		}
		return false;
	}

	/**
	 * 添加授权
	 * @param $dc
	 * @param $params
	 * @return array|bool
	 */
	private static function __add_auth_by_vcas($dc,$params)
	{
		//DRM设备类型，默认：DEVICE
		if(!isset($params['nns_device_type']) && empty($params['nns_device_type']))
		{
			$params['nns_device_type'] = 'DEVICE';
		}
		$client = self::__get_auth_by_vcas_client();
		if($client === false)
		{
			nl_log_v2_error('nl_m_auth_drm',' WSDL连接失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"WSDL连接失败");
		}
		//获取登陆session
		$session_ret = self::vcas_login();
		if($session_ret === false || empty($session_ret['session']))
		{
			nl_log_v2_error('nl_m_auth_drm',' 登陆DRM-VCAS失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"登陆DRM-VCAS失败");
		}
		$params['nns_session'] = $session_ret['session'];
		//向DRM添加授权关系
		include_once dirname(__DIR__).'/vcas/entitlement.class.php';
		$result = nl_vcas_entitlement::add_entitlement($client,$params);
		$data = self::get_vcas_result($result);
		$params['nns_drm_tag'] = self::$vcas_tag;
		self::add_drm_log($params, $data, 'auth', '__add_device_auth_by_vcas', 'add');
		//销毁登陆状态
		self::vcas_logout($session_ret['session']);
		$code = $data['result']['resultCode'];
		//非0，认为失败
		if($code == 0)
		{
			nl_log_v2_info('nl_m_auth_drm',' 添加授权成功');
			return get_logic_result(ML_STATE_OK,"添加授权成功");
		}
		nl_log_v2_error('nl_m_auth_drm','VCAS-add_entitlement，返回参数:'.var_export($result,true));
		return get_logic_result(ML_STATE_PARAM_ERROR,"VCAS-add_entitlement 返回失败");

	}

	/**
	 * 删除授权
	 * @param $dc
	 * @param $params
	 * @return array|bool
	 */
	private static function __delete_auth_by_vcas($dc,$params)
	{
		//DRM设备类型，默认：DEVICE
		if(!isset($params['nns_device_type']) && empty($params['nns_device_type']))
		{
			$params['nns_device_type'] = 'DEVICE';
		}
		//获取登陆session
		$session_ret = self::vcas_login();
		if($session_ret === false || empty($session_ret['session']))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"登陆DRM-VCAS失败");
		}
		$params['nns_session'] = $session_ret['session'];
		//WSDL连接对象
		$client = self::__get_auth_by_vcas_client();
		if($client === false)
		{
			nl_log_v2_error('nl_m_auth_drm',' WSDL连接失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"WSDL连接失败");
		}

		include_once dirname(__DIR__).'/vcas/entitlement.class.php';
		$result = nl_vcas_entitlement::delete_entitlement($client,$params);
		$data = self::get_vcas_result($result);
		$params['nns_drm_tag'] = self::$vcas_tag;
		self::add_drm_log($params, $data, 'auth', '__delete_auth_by_vcas', 'delete');
		//销毁登陆状态
		self::vcas_logout($session_ret['session']);
		$code = $data['result']['resultCode'];
		//非0，认为失败
		if($code == 0)
		{
			nl_log_v2_info('nl_m_auth_drm',' 删除授权成功');
			return get_logic_result(ML_STATE_OK,"删除授权成功");
		}else if($code == 223)
		{
			nl_log_v2_error('nl_m_auth_drm','授权不存在，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_PARAM_ERROR,"授权不存在");
		}
		nl_log_v2_error('nl_m_auth_drm','VCAS-delete_entitlement，返回参数:'.var_export($result,true));
		return get_logic_result(ML_STATE_PARAM_ERROR,"VCAS-delete_entitlement 返回失败");

	}

	/**
	 * 删除授权
	 * @param $dc
	 * @param $params
	 * @return array|bool
	 */
	private static function __delete_device_auth_by_vcas($dc,$params)
	{
		//获取登陆session
		$session_ret = self::vcas_login();
		if($session_ret === false || empty($session_ret['session']))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"登陆DRM-VCAS失败");
		}
		$params['nns_session'] = $session_ret['session'];
		//WSDL连接对象
		$client = self::__get_auth_by_vcas_client();
		if($client === false)
		{
			nl_log_v2_error('nl_m_auth_drm',' WSDL连接失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"WSDL连接失败");
		}

		include_once dirname(__DIR__).'/vcas/entitlement.class.php';
		$result = nl_vcas_entitlement::delete_devicet_entitlement($client,$params);
		$data = self::get_vcas_result($result);
		$params['nns_drm_tag'] = self::$vcas_tag;
		self::add_drm_log($params, $data, 'auth', '__delete_device_auth_by_vcas', 'delete');
		//销毁登陆状态
		self::vcas_logout($session_ret['session']);
		$code = $data['result']['resultCode'];
		//非0，认为失败
		if($code == 0)
		{
			nl_log_v2_info('nl_m_auth_drm',' 删除授权成功');
			return get_logic_result(ML_STATE_OK,"删除授权成功");
		}else if($code == 223)
		{
			nl_log_v2_error('nl_m_auth_drm','授权不存在，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_PARAM_ERROR,"授权不存在");
		}
		nl_log_v2_error('nl_m_auth_drm','VCAS-delete_entitlement，返回参数:'.var_export($result,true));
		return get_logic_result(ML_STATE_PARAM_ERROR,"VCAS-delete_entitlement 返回失败");

	}
}