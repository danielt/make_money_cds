<?php
/**
 * 片源相关业务实现
 * Author: 陈波(bo.chen@starcor.cn)
 * Date: 2015/3/20 19:39
 */
include_once dirname(__FILE__).'/common.class.php';
class nl_m_media extends common
{
	/**
	 * 添加片源
	 * Author: 陈波(bo.chen@starcor.cn)
	 * $params = array(
	 * 		'nns_media_id'=>'片源ID',
	 * 		'nns_description'=>'片源描述信息',
	 * )
	 */
	public static function add_media($dc,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		$filed_arr = array(
			'nns_media_id',
			'nns_description',
		);
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		//判断VCAS是否开启状态
		if(self::get_vcas_enable())
		{
			return self::__add_media_by_vcas($dc,$params);
		}


	}

	/**
	 * 删除片源
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $dc
	 * @param $params
	 * @return array|void
	 */
	public static function delete_media($dc,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		$filed_arr = array(
			'nns_media_id',
			'nns_description',
		);
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		//判断VCAS是否开启状态
		if(self::get_vcas_enable())
		{
			return self::__delete_media_by_vcas($dc,$params);

		}
	}
	/**
	 * 片源绑定管理域
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $dc
	 * @param $params
	 * @return array|void
	 */
	public static function add_media_to_network($dc,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		$filed_arr = array(
			'nns_media_id',
			'nns_network_id',
		);
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		//判断VCAS是否开启状态
		if(self::get_vcas_enable())
		{
			return self::__add_media_to_network_by_vcas($dc,$params);

		}
	}

	/**
	 * 删除片源绑定管理域
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $dc
	 * @param $params
	 * @return array|void
	 */
	public static function delete_media_from_network($dc,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		$filed_arr = array(
			'nns_media_id',
			'nns_description',
		);
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		//判断VCAS是否开启状态
		if(self::get_vcas_enable())
		{
			return self::__delete_media_from_network_by_vcas($dc,$params);
		}


	}

	/**
	 * 添加片源内容
	 * Author: 陈波(bo.chen@starcor.cn)
	 * $params = array(
	 * 		'nns_user_name'=>'登录用户名',
	 * 		'nns_password'=>'登录密码',
	 * )
	 */
	public static function add_media_content($dc,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		//验证参数
		$filed_arr = array(
			'nns_media_id',
			'nns_content_id',
			'nns_network_id',
			'nns_start_time',
			'nns_end_time',
		);
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		//判断VCAS是否开启状态
		if(self::get_vcas_enable())
		{
			return self::__add_media_content_by_vcas($dc,$params);

		}
	}


	/**
	 * 删除片源内容
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $dc
	 * @param $params
	 * @return array|void
	 */
	public static function delete_media_content($dc,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		//验证参数
		$filed_arr = array(
			'nns_media_id',
			'nns_content_id',
			'nns_network_id',
			'nns_start_time',
			'nns_end_time',
		);
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		//判断VCAS是否开启状态
		if(self::get_vcas_enable())
		{
			return self::__delete_media_content_by_vcas($dc,$params);

		}

	}
	/**
	 * 添加片源内容到产品包
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $dc
	 * @param $params
	 * @return array|void
	 */
	public static function add_media_content_to_package($dc,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		//验证参数
		$filed_arr = array(
			'nns_media_id',
			'nns_content_id',
			'nns_network_id',
			'nns_start_time',
			'nns_end_time',
			'nns_package_id',
			'nns_description',
		);
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		//判断VCAS是否开启状态
		if(self::get_vcas_enable())
		{
			return self::__add_media_content_to_package($dc,$params);
		}
	}
	/**
	 * 删除片源内容到产品包
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @param $dc
	 * @param $params
	 * @return array|void
	 */
	public static function delete_media_content_to_package($dc,$params)
	{
		if (!is_array($params))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"params format is wrong");
		}
		//验证参数
		$filed_arr = array(
			'nns_media_id',
			'nns_content_id',
			'nns_network_id',
			'nns_start_time',
			'nns_end_time',
			'nns_package_id',
			'nns_description',
		);
		$check_res = self::check_params($params,$filed_arr);
		if($check_res)
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"$check_res cannot be empty");
		}
		//判断VCAS是否开启状态
		if(self::get_vcas_enable())
		{
			return self::__delete_media_content_to_package($dc,$params);
		}
	}


	/**
	 * 添加片源
	 * @param $dc
	 * @param $params
	 * @return array|bool
	 */
	private static function __add_media_by_vcas($dc,$params)
	{
		//获取登陆session
		$session_ret = self::vcas_login();
		if($session_ret === false || empty($session_ret['session']))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"登陆DRM-VCAS失败");
		}
		$params['nns_session'] = $session_ret['session'];
		//获取WSDL对象
		$client = self::__get_content_by_vcas_client();
		if($client === false)
		{
			nl_log_v2_error('nl_m_config',' WSDL连接失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"WSDL连接失败");
		}
		include_once dirname(__DIR__).'/vcas/content.class.php';
		$result = nl_vcas_content::add_content($client,$params);
		$data = self::get_vcas_result($result);
		$params['nns_drm_tag'] = self::$vcas_tag;
		self::add_drm_log($params, $data, 'media', 'add_content', 'add');
//		echo '<pre>';
//		var_dump($data);exit;
		//销毁登陆状态
		self::vcas_logout($session_ret['session']);
		$code = $data['result']['resultCode'];
		//非0，认为失败
		if($code == 0)
		{
			nl_log_v2_error('nl_m_media','VCAS-addContent返回成功，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_OK,"添加片源成功");
		}else if($code == 203)
		{
			nl_log_v2_error('nl_m_media','VCAS-addContent返回成功，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_PARAM_ERROR,"片源已存在");
		}

		nl_log_v2_error('nl_m_media','VCAS-addContent返回失败，返回参数:'.var_export($result,true));
		return get_logic_result(ML_STATE_PARAM_ERROR,"VCAS-addContent返回失败");

	}

	/**
	 * 删除片源
	 * @param $dc
	 * @param $params
	 * @return array|bool
	 */
	private static function __delete_media_by_vcas($dc,$params)
	{
		//获取登陆session
		$session_ret = self::vcas_login();
		if($session_ret === false || empty($session_ret['session']))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"登陆DRM-VCAS失败");
		}
		$params['nns_session'] = $session_ret['session'];
		//获取WSDL对象
		$client = self::__get_content_by_vcas_client();
		if($client === false)
		{
			nl_log_v2_error('nl_m_config',' WSDL连接失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"WSDL连接失败");
		}
		include_once dirname(__DIR__).'/vcas/content.class.php';
		$result = nl_vcas_content::delete_content($client,$params);
		$data = self::get_vcas_result($result);
		$params['nns_drm_tag'] = self::$vcas_tag;
		self::add_drm_log($params, $data, 'media', 'delete_content', 'delete');
//		echo '<pre>';
//		var_dump($data);exit;
		//销毁登陆状态
		self::vcas_logout($session_ret['session']);
		$code = $data['result']['resultCode'];
		//非0，认为失败
		if($code == 0)
		{
			nl_log_v2_info('nl_m_media','删除片源成功，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_OK,"删除片源成功");
		}else if($code == 204)
		{
			nl_log_v2_error('nl_m_media','VCAS-removeContent返回成功，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_PARAM_ERROR,"片源不存在");
		}

		nl_log_v2_error('nl_m_media','VCAS-removeContent返回失败，返回参数:'.var_export($result,true));
		return get_logic_result(ML_STATE_PARAM_ERROR,"VCAS-removeContent返回失败");

	}

	/**
	 * 添加片源到network的绑定关系
	 * @param $dc
	 * @param $params
	 * @return array|bool
	 */
	private static function __add_media_to_network_by_vcas($dc,$params)
	{
		//获取登陆session
		$session_ret = self::vcas_login();
		if($session_ret === false || empty($session_ret['session']))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"登陆DRM-VCAS失败");
		}
		$params['nns_session'] = $session_ret['session'];
		//获取WSDL对象
		$client = self::__get_content_by_vcas_client();
		if($client === false)
		{
			nl_log_v2_error('nl_m_config',' WSDL连接失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"WSDL连接失败");
		}
		//想DRM添加content与network的绑定关系
		include_once dirname(__DIR__).'/vcas/content.class.php';

		$result = nl_vcas_content::add_content_to_network($client,$params);
		$data = self::get_vcas_result($result);
		//添加日志
		$params['nns_drm_tag'] = self::$vcas_tag;
		self::add_drm_log($params, $data, 'media', '__add_media_to_content_by_vcas', 'add');
		//销毁登陆状态
		self::vcas_logout($session_ret['session']);
		$code = $data['resultCode'];
		//非0，认为失败
		if($code == 0)
		{
			nl_log_v2_info('nl_m_media','片源添加到管理域成功，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_OK,"片源添加到管理域成功");
		}

		nl_log_v2_error('nl_m_media','VCAS-addContentToNetwork返回失败，返回参数:'.var_export($result,true));
		return get_logic_result(ML_STATE_PARAM_ERROR,"VCAS-addContentToNetwork返回失败");

	}

	/**
	 * 删除片源到network的绑定关系
	 * @param $dc
	 * @param $params
	 * @return array|bool
	 */
	private static function __delete_media_from_network_by_vcas($dc,$params)
	{
		//获取登陆session
		$session_ret = self::vcas_login();
		if($session_ret === false || empty($session_ret['session']))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"登陆DRM-VCAS失败");
		}
		$params['nns_session'] = $session_ret['session'];
		//获取WSDL对象
		$client = self::__get_content_by_vcas_client();
		if($client === false)
		{
			nl_log_v2_error('nl_m_config',' WSDL连接失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"WSDL连接失败");
		}
		//向DRM添加content与network的绑定关系
		include_once dirname(__DIR__).'/vcas/content.class.php';
		$result = nl_vcas_content::add_content_to_network($client,$params);
		//解析接口返回
		$data = self::get_vcas_result($result);
		//添加日志
		$params['nns_drm_tag'] = self::$vcas_tag;
		self::add_drm_log($params, $data, 'media', '__add_media_to_content_by_vcas', 'add');
		//销毁登陆状态
		self::vcas_logout($session_ret['session']);
		$code = $data['resultCode'];
		//非0，认为失败
		if($code == 0)
		{

			return get_logic_result(ML_STATE_OK,"删除片源与管理域的绑定关系成功");
		}
		nl_log_v2_error('nl_m_media','VCAS-removeContentFromNetwork返回失败，返回参数:'.var_export($result,true));
		return get_logic_result(ML_STATE_PARAM_ERROR,"VCAS-removeContentFromNetwork返回失败");

	}

	/**
	 * 添加片源内容
	 * @param $dc
	 * @param $params
	 * @return array|bool
	 */
	private static function __add_media_content_by_vcas($dc,$params)
	{
		//获取登陆session
		$session_ret = self::vcas_login();
		if($session_ret === false || empty($session_ret['session']))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"登陆DRM-VCAS失败");
		}
		$params['nns_session'] = $session_ret['session'];
		//获取WSDL对象
		$client = self::__get_content_by_vcas_client();
		if($client === false)
		{
			nl_log_v2_error('nl_m_config',' WSDL连接失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"WSDL连接失败");
		}
		include_once dirname(__DIR__).'/vcas/content.class.php';
		$result = nl_vcas_content::add_event($client,$params);
		$data = self::get_vcas_result($result);
		$params['nns_drm_tag'] = self::$vcas_tag;
		self::add_drm_log($params, $data, 'media', '__add_media_content', 'add');
//		echo "<pre>";
//		var_dump($data);exit;
		//销毁登陆状态
		self::vcas_logout($session_ret['session']);
		//非0，认为失败
		$code = $data['result']['resultCode'];
		if($code == 0)
		{
			nl_log_v2_info('nl_m_media','添加片源内容成功，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_OK,"添加片源内容成功");
		}else if($code == 214 )
		{
			nl_log_v2_error('nl_m_media','VCAS-addEvent，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_PARAM_ERROR,"片源内容已存在");
		}
		nl_log_v2_error('nl_m_media','VCAS-addEvent，返回参数:'.var_export($result,true));
		return get_logic_result(ML_STATE_PARAM_ERROR,"VCAS-addEvent返回失败");

	}
	/**
	 * 删除片源内容
	 * @param $dc
	 * @param $params
	 * @return array|bool
	 */
	private static function __delete_media_content_by_vcas($dc,$params)
	{
		//获取登陆session
		$session_ret = self::vcas_login();
		if($session_ret === false || empty($session_ret['session']))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"登陆DRM-VCAS失败");
		}
		$params['nns_session'] = $session_ret['session'];
		//获取WSDL对象
		$client = self::__get_content_by_vcas_client();
		if($client === false)
		{
			nl_log_v2_error('nl_m_config',' WSDL连接失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"WSDL连接失败");
		}
		include_once dirname(__DIR__).'/vcas/content.class.php';
		$result = nl_vcas_content::delete_event($client,$params);
		$data = self::get_vcas_result($result);
		$params['nns_drm_tag'] = self::$vcas_tag;
		self::add_drm_log($params, $data, 'media', '__add_media_content', 'add');
		//销毁登陆状态
		self::vcas_logout($session_ret['session']);
		$code = $data['result']['resultCode'];
		//非0，认为失败
		if($code == 0)
		{
			nl_log_v2_info('nl_m_media','删除片源内容成功，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_OK,"删除片源内容成功");
		}else if($code == 213)
		{
			nl_log_v2_error('nl_m_media','片源内容不存在，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_PARAM_ERROR,"片源内容不存在");
		}

		nl_log_v2_error('nl_m_media','VCAS-removeEvent，返回参数:'.var_export($result,true));
		return get_logic_result(ML_STATE_PARAM_ERROR,"VCAS-removeEvent返回失败");

	}

	/**
	 * 片源绑定产品包
	 * @param $dc
	 * @param $params
	 * @return array|bool
	 */
	private static function __add_media_content_to_package($dc,$params)
	{
		//获取登陆session
		$session_ret = self::vcas_login();
		if($session_ret === false || empty($session_ret['session']))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"登陆DRM-VCAS失败");
		}
		$params['nns_session'] = $session_ret['session'];
		//获取WSDL对象
		$client = self::__get_auth_by_vcas_client();
		if($client === false)
		{
			nl_log_v2_error('nl_m_auth_drm',' WSDL连接失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"WSDL连接失败");
		}
		//向DRM同步片源与产品包的关系
		include_once dirname(__DIR__).'/vcas/content.class.php';
		$result = nl_vcas_content::add_event_to_package($client,$params);
		$data = self::get_vcas_result($result);
		$params['nns_drm_tag'] = self::$vcas_tag;
		self::add_drm_log($params, $data, 'media', '__add_media_content_to_package', 'add');
		//销毁登陆状态
		self::vcas_logout($session_ret['session']);
		$code = $data['result']['resultCode'];
		//非0，认为失败
		if($code == 0)
		{
			nl_log_v2_info('nl_m_media','片源绑定产品包成功，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_OK,"片源绑定产品包成功");
		}else if($code == 215)
		{
			nl_log_v2_error('nl_m_media','绑定关系已存在，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_PARAM_ERROR,"绑定关系已存在");
		}
		nl_log_v2_error('nl_m_media','VCAS-addEventsToPackage，返回参数:'.var_export($result,true));
		return get_logic_result(ML_STATE_PARAM_ERROR,"VCAS-addEventsToPackage 返回失败");

	}

	/**
	 * 取消片源绑定产品包
	 * @param $dc
	 * @param $params
	 * @return array|bool
	 */
	private static function __delete_media_content_to_package($dc,$params)
	{
		//获取登陆session
		$session_ret = self::vcas_login();
		if($session_ret === false || empty($session_ret['session']))
		{
			return get_logic_result(ML_STATE_PARAM_ERROR,"登陆DRM-VCAS失败");
		}
		$params['nns_session'] = $session_ret['session'];
		$client = self::__get_auth_by_vcas_client();
		if($client === false)
		{
			nl_log_v2_error('nl_m_auth_drm',' WSDL连接失败');
			return get_logic_result(ML_STATE_PARAM_ERROR,"WSDL连接失败");
		}

		include_once dirname(__DIR__).'/vcas/content.class.php';
		$result = nl_vcas_content::delete_event_to_package($client,$params);
		$data = self::get_vcas_result($result);
		$params['nns_drm_tag'] = self::$vcas_tag;
		self::add_drm_log($params, $data, 'media', '__delete_media_content_to_package', 'delete');
		//销毁登陆状态
		self::vcas_logout($session_ret['session']);
		$code = $data['result']['resultCode'];
		//var_dump($data);exit;
		//非0，认为失败
		if($code == 0)
		{
			nl_log_v2_info('nl_m_media','取消片源绑定产品包成功，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_OK,"取消片源绑定产品包成功");
		}else if($code == 115)
		{
			nl_log_v2_error('nl_m_media','绑定关系不存在，返回参数:'.var_export($result,true));
			return get_logic_result(ML_STATE_PARAM_ERROR,"绑定关系不存在");
		}

		nl_log_v2_error('nl_m_media','VCAS-deleteEventsToPackage，返回参数:'.var_export($result,true));
		return get_logic_result(ML_STATE_PARAM_ERROR,"VCAS-deleteEventsToPackage 返回失败");

	}


	/**
	 * 获取授权接口对象
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @return bool|SOAPClient|string
	 */
	private static function __get_auth_by_vcas_client()
	{
		$service_url = get_config_v2('g_drm_vcas_entitlement_manager_service_url');
		if(empty($service_url))
		{
			nl_log_v2_error('nl_m_media','g_drm_vcas_entitlement_manager_service_url 为空或者未配置');
			return false;
		}
		$client = self::get_soap_client($service_url);
		if(is_object($client))
		{
			return $client;
		}
		return false;
	}

	//

	/**
	 * 获取片源接口对象
	 * Author: 陈波(bo.chen@starcor.cn)
	 * @return bool|SOAPClient|string
	 */
	private static function __get_content_by_vcas_client()
	{
		$service_url = get_config_v2('g_drm_vcas_content_manager_service_url');
		if(empty($service_url))
		{
			nl_log_v2_error('nl_m_media','g_drm_vcas_content_manager_service_url 为空或者未配置');
			return false;
		}
		$client = self::get_soap_client($service_url);
		if(is_object($client))
		{
			return $client;
		}
		return false;
	}

}