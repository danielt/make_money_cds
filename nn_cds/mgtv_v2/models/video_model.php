<?php

class video_model {

    
	
	static public function show_db_quantity_info($dc,$array,$limit=array()){
		$db=$dc->db();
		if($array['where'] == "nns_modify_time" || $array['where'] == "nns_create_time"){
			$sql="select * from nns_".$array['table']." where " .$array['where']. "   >= '" .$array['begin_time']. "' and " . $array['where'] . "<='" .$array['end_time']. "'";
		}else if($array['where'] == "nns_state"){
			$sql="select * from nns_".$array['table']." where " .$array['where']. " !='0' and ((nns_modify_time  >= '" .$array['begin_time']. "' and nns_modify_time <='" .$array['end_time']. "') or  (nns_create_time  >= '" .$array['begin_time']. "' and nns_create_time <='" .$array['end_time']. "'))" ;
		}else{
			return false;
		}
		if(!empty($limit)){
			$sql.=" limit " .$limit['left']. "," .$limit['right'];
		}
		return nl_query_by_db($sql,$db);
	}
	static public function show_db_quantity_info_count($dc,$array){
		//$db = nn_get_db(NL_DB_READ);
		$db=$dc->db();
		if($array['where'] == "nns_modify_time" || $array['where'] == "nns_create_time"){
			$sql="select count(*) as total from nns_".$array['table']." where " .$array['where']. "   >= '" .$array['begin_time']. "' and " . $array['where'] . "<='" .$array['end_time']. "'";
		}else if($array['where'] == "nns_state"){
			$sql="select count(*) as total from nns_".$array['table']." where " .$array['where']. " !='0' and ((nns_modify_time  >= '" .$array['begin_time']. "' and nns_modify_time <='" .$array['end_time']. "') or  (nns_create_time  >= '" .$array['begin_time']. "' and nns_create_time <='" .$array['end_time']. "'))" ;
		}else{
			return false;
		}
		$res=nl_query_by_db($sql,$db);
		return $res['0']['total'];
	}
	static public function get_vod_info($video_id){
		$db = nn_get_db(NL_DB_READ);
		$sql = "select * from nns_vod where nns_id='$video_id' or nns_asset_import_id='$video_id'";
		return nl_db_get_one($sql, $db);
	}
	
	static public function get_vod_index_info($video_id){
		$db = nn_get_db(NL_DB_READ);
		$sql = "select * from nns_vod_index where nns_id='$video_id'";
		return nl_db_get_one($sql, $db);
	}

    /**
     * 根据主媒资id获取分集列表
     * @param $vod_id
     * @return bool
     */
    static public function get_vod_index_list($vod_id){
        $db = nn_get_db(NL_DB_READ);
        $sql = "select * from nns_vod_index where nns_vod_id='$vod_id'";
        return nl_db_get_all($sql, $db);
    }

	static public function get_vod_media_info($video_id){
		$db = nn_get_db(NL_DB_READ);
		$sql = "select * from nns_vod_media where nns_id='$video_id'";
		return nl_db_get_one($sql, $db);
	}


	static public function get_live_info($video_id){
		$db = nn_get_db(NL_DB_READ);
		$sql = "select * from nns_live where nns_id='$video_id'";
		return nl_db_get_one($sql, $db);
	}
	
	static public function get_file_package_info($video_id){
	    $db = nn_get_db(NL_DB_READ);
	    $sql = "select * from nns_file_package where nns_id='$video_id'";
	    return nl_db_get_one($sql, $db);
	}
	
	static public function get_live_index_info($video_id){
		$db = nn_get_db(NL_DB_READ);
		$sql = "select * from nns_live_index where nns_id='$video_id'";
		return nl_db_get_one($sql, $db);
	}
	
	static public function get_live_media_info($video_id){
		$db = nn_get_db(NL_DB_READ);
		$sql = "select * from nns_live_media where nns_id='$video_id'";
		return nl_db_get_one($sql, $db);
	}
	
	static public function get_playbill_info($video_id){
		$db = nn_get_db(NL_DB_READ);
		$sql = "select * from nns_live_playbill_item where nns_id='$video_id'";
		return nl_db_get_one($sql, $db);
	}
	
	static public function get_vod_medias_by_index($video_index_id){
		$db = nn_get_db(NL_DB_READ);
		$sql = "select * from nns_vod_media where nns_vod_index_id='$video_index_id' and nns_deleted!=1";
		return nl_db_get_all($sql, $db);
	}
	
	/**
	 * 反解析获取数据信息
	 * @param unknown $dc
	 * @param unknown $video_type
	 * @param unknown $array_query
	 */
	static public function get_back_info_one($video_type,$array_query)
	{
	    if(!is_array($array_query) || empty($array_query))
	    {
	        return true;
	    }
	    $db = nn_get_db(NL_DB_READ);
	    switch ($video_type)
	    {
	        case 'video':
	            $table_name="nns_vod";
	            break;
	        case 'index':
	            $table_name="nns_vod_index";
	            break;
	        case 'media':
	            $table_name="nns_vod_media";
	            break;
	        case 'live':
	            $table_name="nns_live";
	            break;
	        case 'live_index':
	            $table_name="nns_live_index";
	            break;
	        case 'live_media':
	            $table_name="nns_live_media";
	            break;
	        case 'playbill':
	            $table_name="nns_live_playbill_item";
	            break;
            case 'file':
                $table_name="nns_file_package";
                break;
	        default:
	            return true;
	    }
	    $sql="select * from {$table_name} where ";
        foreach ($array_query as $key=>$val)
        {
            $sql.=" {$key}='{$val}' and ";
        }
        $sql=substr(trim($sql), 0,-3)." limit 1";
	    $result = nl_query_by_db($sql, $db);
	    if(!$result)
	    {
	        return false;
	    }
	    return (isset($result['0']) && is_array($result['0']) && !empty($result['0'])) ? $result['0'] : true;
	}
	
	static public function get_vod_list($filter=null,$start=0,$size=12){
		set_time_limit(0);
		$db = nn_get_db(NL_DB_READ);
		$sql = "select * " .
        		"from nns_vod  where nns_deleted!=1";
		if($filter != null){
			$wh_arr = array();
			if(!empty($filter['search'])){
				$wh_arr[] = " (nns_name like '%".$filter['search']."%' or   nns_id = '".$filter['search']."'  or nns_pinyin like '%".$filter['search']."%' )";
				
			}
			
			if(!empty($filter['category_id'])&&$filter['category_id']!=10000){
					
						
					
				$wh_arr[] = " nns_category_id='".$filter['category_id']."'";
			}			
					
			
						
			if(!empty($wh_arr))	$sql .= " and ".implode(" and ",$wh_arr);
		}		
        $count_sql = $sql;
        $count_sql = str_replace('*', ' count(*) as temp_count ', $count_sql);
        $sql .= " order by nns_create_time desc";
        $sql .= " limit $start,$size";
        $data = nl_db_get_all($sql, $db);
        $rows = nl_db_get_col($count_sql, $db);
        return array('data' => $data, 'rows' => $rows);
    }

    /**
     * @name 查询主媒资增量
     */

    static public function video_increment($filter, $offset, $limit) {
        set_time_limit(0);
        $db = nn_get_db(NL_DB_READ);
        $sql = "select * " . "from nns_vod ";
        if ($filter != null) {
            $wh_arr = array();
            if (!empty($filter['nns_name'])) {
                $wh_arr[] = " nns_name like '%" . $filter['nns_name'] . "%'";
            }
            if (!empty($filter['day_picker_start'])) {
                $wh_arr[] = " nns_modify_time>='" . $filter['day_picker_start'] . "'";
            }
            if (!empty($filter['day_picker_end'])) {
                $wh_arr[] = " nns_modify_time<='" . $filter['day_picker_end'] . "'";
            }
            if (!empty($filter['nns_action'])) {
                $wh_arr[] = " nns_action='" . $filter['nns_action'] . "'";
            }
            if (!empty($wh_arr))
                $sql .= " where " . implode(" and ", $wh_arr);
        } else {
            $wh_arr = array();
            $date = date('Y-m-d') . ' 00:00:00';
            $wh_arr[] = " nns_modify_time>='" . $date . "'";
            $wh_arr[] = " nns_modify_time<='" . date('Y-m-d H:i:s') . "'";
            if (!empty($wh_arr))
                $sql .= " where " . implode(" and ", $wh_arr);
        }

        $count_sql = str_replace('*', ' count(*) as temp_count ', $sql);
        $sql .= " order by nns_modify_time desc";
        $sql .= " limit $offset, $limit";
        $data = nl_db_get_all($sql, $db);
        $rows = nl_db_get_col($count_sql, $db);
        return array('data' => $data, 'rows' => $rows);

    }



    /**
     * @name 查询主媒资增量
     */

    static public function index_increment($filter, $offset, $limit) {
        set_time_limit(0);
        $db = nn_get_db(NL_DB_READ);
        $sql = "select * " . "from nns_vod_index ";
        if ($filter != null) {
            $wh_arr = array();
            if (!empty($filter['nns_name'])) {
                $wh_arr[] = " nns_name like '%" . $filter['nns_name'] . "%'";
            }
            if (!empty($filter['day_picker_start'])) {
                $wh_arr[] = " nns_modify_time>='" . $filter['day_picker_start'] . "'";
            }
            if (!empty($filter['day_picker_end'])) {
                $wh_arr[] = " nns_modify_time<='" . $filter['day_picker_end'] . "'";
            }
            if (!empty($filter['nns_action'])) {
                $wh_arr[] = " nns_action='" . $filter['nns_action'] . "'";
            }
            if (!empty($wh_arr))
                $sql .= " where " . implode(" and ", $wh_arr);
        } else {
            $wh_arr = array();
            $date = date('Y-m-d') . ' 00:00:00';
            $wh_arr[] = " nns_modify_time>='" . $date . "'";
            $wh_arr[] = " nns_modify_time<='" . date('Y-m-d H:i:s') . "'";
            if (!empty($wh_arr))
                $sql .= " where " . implode(" and ", $wh_arr);
        }

        $count_sql = str_replace('*', ' count(*) as temp_count ', $sql);
        $sql .= " order by nns_modify_time desc";
        $sql .= " limit $offset, $limit";
        $data = nl_db_get_all($sql, $db);
        $rows = nl_db_get_col($count_sql, $db);
        return array('data' => $data, 'rows' => $rows);

    }


    /**
     * @name 查询主媒资增量
     */

    static public function media_increment($filter, $offset, $limit) {
        set_time_limit(0);
        $db = nn_get_db(NL_DB_READ);
        $sql = "select nns_vod_media.*,nns_vod_index.nns_name as nns_index_name " . 
        "from nns_vod_media left join nns_vod_index on nns_vod_media.nns_vod_index_id=nns_vod_index.nns_id ";
        if ($filter != null) {
            $wh_arr = array();
            if (!empty($filter['nns_name'])) {
                $wh_arr[] = " nns_vod_index.nns_name like '%" . $filter['nns_name'] . "%'";
            }
            if (!empty($filter['day_picker_start'])) {
                $wh_arr[] = " nns_vod_media.nns_modify_time>='" . $filter['day_picker_start'] . "'";
            }
            if (!empty($filter['day_picker_end'])) {
                $wh_arr[] = " nns_vod_media.nns_modify_time<='" . $filter['day_picker_end'] . "'";
            }
            if (!empty($filter['nns_action'])) {
                $wh_arr[] = " nns_vod_media.nns_action='" . $filter['nns_action'] . "'";
            }
            if (!empty($wh_arr))
                $sql .= " where " . implode(" and ", $wh_arr);
        } else {
            $wh_arr = array();
            $date = date('Y-m-d') . ' 00:00:00';
            $wh_arr[] = " nns_vod_media.nns_modify_time>='" . $date . "'";
            $wh_arr[] = " nns_vod_media.nns_modify_time<='" . date('Y-m-d H:i:s') . "'";
            if (!empty($wh_arr))
                $sql .= " where " . implode(" and ", $wh_arr);
        }

        $count_sql = str_replace('nns_vod_media.*,nns_vod_index.nns_name as nns_index_name', ' count(*) as temp_count ', $sql);
        $sql .= " order by nns_vod_media.nns_modify_time desc";
        $sql .= " limit $offset, $limit";
        $data = nl_db_get_all($sql, $db);
        $rows = nl_db_get_col($count_sql, $db);
        return array('data' => $data, 'rows' => $rows);

    }
    
	static public function get_vod_ex_info($import_id,$type='vod',$key=null,$cp_id=0){
		$db = nn_get_db(NL_DB_READ);
		switch ($type)
		{
			case 'vod':
				$sql = "select * from nns_vod_ex where nns_vod_id='$import_id' and nns_cp_id='$cp_id'";
				break;
			case 'index':
				$sql = "select * from nns_vod_index_ex where nns_vod_index_id='$import_id' and nns_cp_id='$cp_id'";
				break;
			case 'media':
				$sql = "select * from nns_vod_media_ex where nns_vod_media_id='$import_id' and nns_cp_id='$cp_id'";
				break;
		}
		if(!empty($key))
		{
			$sql .= " and nns_key='$key'";
		}
		$result = nl_db_get_all($sql, $db);
		if(!is_array($result))
		{
			return $result;
		}
		$re = array();
		foreach ($result as $value)
		{
			$re[$value['nns_key']] = $value['nns_value'];
		}
		return $re;
	}

}
