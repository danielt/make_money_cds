<?php
ini_set('display_errors',1);
include_once dirname(dirname(__FILE__)) . '/mgtv_const.php';
include_once dirname(dirname(dirname(__FILE__))) . '/op/op_queue.const.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/producer/producer.class.php';
include_once dirname(__FILE__)  . '/queue_task_model.php';

class content_task_model{
    
    static public $sp_config = null;
    static public $arr_error = null;
    static public $obj_dc = null;
    static public $sp_id = null;
    
    static public function _get_error_data()
    {
        $arr_error = self::$arr_error;
        self::$arr_error = null;
        return (empty($arr_error)) ? null : $arr_error;
    }
    
	static public function run($type) 
	{
		return self::$type();
	}

	/**
	 * 注入分集
	 */
	static public function run_vod_index_to_c2(){
		self::vod(null,'index');
	}

	static public function run_vod_playbill_to_c2(){
		self::vod(null,'playbill');
	}

	/**
	 * 昆山广电注入CDN 获取播放串
	 * @param unknown $arr_c2_task
	 */
	static public function get_cdn_play_url($arr_c2_task)
	{
	    $temp_media_info_arr = $temp_result_arr = null;
	    if(!is_array($arr_c2_task) || empty($arr_c2_task))
	    {
	        return true;
	    }
	    foreach ($arr_c2_task as $task_val)
	    {
	        if($task_val['nns_type'] !='media' || strlen($task_val['nns_ref_id']) <1)
	        {
	            continue;
	        }
	        $resut_cdn_media = public_model_exec::get_asset_id_by_sp_id(self::$sp_id, 'media', $task_val['nns_ref_id'],null,self::$sp_config,'cdn');
	        if($resut_cdn_media['ret'] !=0 || !isset($resut_cdn_media['data_info']) || strlen($resut_cdn_media['data_info']) <1)
	        {
	            self::$arr_error[] = $resut_cdn_media;
	            continue;
	        }
	        if($task_val['nns_action'] == 'destroy')
	        {
	            $temp_result_arr['del'][$task_val['nns_id']] = $resut_cdn_media['data_info'];
	        }
	        else
	        {
	            $temp_result_arr['add'][$task_val['nns_id']] = $resut_cdn_media['data_info'];
	        }
	        $temp_media_info_arr[$resut_cdn_media['data_info']] = $task_val['nns_ref_id'];
	    }
	    
	    if(!is_array($temp_result_arr) || empty($temp_result_arr))
	    {
	        return true;
	    }
	    foreach ($temp_result_arr as $key=>$val)
	    {
	        if(!is_array($val) || empty($val))
	        {
	            continue;
	        }
	        $str_obj_func = "get_{$key}_media_play_url"; 
	        $obj_cdn_query = new get_media(self::$sp_config['c2_get_third_play_url']);
	        $chunk_val = array_chunk($val,15,true);
	        foreach ($chunk_val as $media_val)
	        {
	            if(!is_array($media_val) || empty($media_val))
	            {
	                continue;
	            }
	            $media_arr_val = array_values($media_val);
	            $media_arr_key = array_keys($media_val);
	            $result_ex = $obj_cdn_query->$str_obj_func($media_arr_val);
	            if($result_ex['ret'] !='0')
	            {
	                self::$arr_error[] = $result_ex;
	                continue;
	            }
	            $result_ex = strlen($result_ex['data_info']) ? json_decode($result_ex['data_info'],true) : null;
	            if(!is_array($result_ex) || !isset($result_ex['errorcode']) || $result_ex['errorcode'] != 0 || !isset($result_ex['package_list']) || !is_array($result_ex['package_list']) || empty($result_ex['package_list']))
	            {
	                $result = nl_c2_task::edit_cdn_fail_time(self::$obj_dc,$media_arr_key);
	                if($result['ret'] !=0)
	                {
	                    self::$arr_error[] = $result;
	                }
	                continue;
	            }
	            $sucess_data = $error_data = null;
	            foreach ($result_ex['package_list'] as $media_ex_val)
                {
                    $media_ex_val['packageid'] = isset($media_ex_val['packageid']) ? $media_ex_val['packageid'] : '';
                    if(!in_array($media_ex_val['packageid'], $media_arr_key))
                    {
                        continue;
                    }
                    if(isset($media_ex_val['status']) && in_array($media_ex_val['status'], array('0','2','1')))
                    {
                        if($key == 'add' && $media_ex_val['status'] == '2' && isset($media_ex_val['homed_videoid']) && strlen($media_ex_val['homed_videoid']) >0)
                        {
                            $sucess_data[] = $media_ex_val['packageid'];
                            $result = nl_vod_media_v2::edit(self::$obj_dc,array('nns_content_id'=>$media_ex_val['homed_videoid']), $temp_media_info_arr[$media_ex_val['packageid']]);
                            if($result['ret'] !=0)
                            {
                                self::$arr_error[] = $result;
                            }
                        }
                        else if($key == 'del' && $media_ex_val['status'] == '2')
                        {
                            $sucess_data[]=$media_ex_val['packageid'];
                        }
                    }
                    else
                    {
                        $error_data[]=$media_ex_val['packageid'];
                    }
                }
                if(is_array($sucess_data) && !empty($sucess_data))
                {
                    $result = nl_c2_task::edit(self::$obj_dc,array('nns_state'=>0),$sucess_data);
                    if($result['ret'] !=0)
                    {
                        self::$arr_error[] = $result;
                    }
                }
                if(is_array($error_data) && !empty($error_data))
                {
                    $result = nl_c2_task::edit_cdn_fail_time(self::$obj_dc,$error_data);
                    if($result['ret'] !=0)
                    {
                        self::$arr_error[] = $result;
                    }
                }
	        }
	    }
	    return true;
	}
	
	/**
	 * 从内容任务表取任务执行
	 * @param string $new_id_c2 nns_mgtvbk_c2_task表记录ID
	 * @param string $c2_type 主媒资(video)、分集(index)、片源(media)
	 * @param string $sp_id 运营商ID
	 * @param bool $is_cdn_resend 是否是跑CDN注入失败重新注入脚本
	 * @return boolean
	 */
	static public function line_vod($obj_dc,$sp_confog,$sp_id,$c2_type=null,$new_id_c2=null)
	{
	    include_once dirname(__FILE__).'/child_models/c2_task_assign.class.php';
	    $str_date = date("Y-m-d H:i:s");
	    if(isset($new_id_c2) && strlen($new_id_c2) >0)
	    {
// 	        $sql = "select * from nns_mgtvbk_c2_task where nns_id='{$new_id_c2}' and nns_org_id='{$sp_id}' and nns_type='{$c2_type}' and nns_status in(12,14)";
	        $sql = "select * from nns_mgtvbk_c2_task where nns_id='{$new_id_c2}' and nns_org_id='{$sp_id}' and nns_type='{$c2_type}'";
	    }
	    else
	    {
	        $sql = "select * from nns_mgtvbk_c2_task where nns_org_id='{$sp_id}' and nns_type='{$c2_type}' and nns_status=12 order by nns_modify_time asc limit 30";
	    }
	    $result = nl_query_by_db($sql,$obj_dc->db());
	    if(!is_array($result) || empty($result))
	    {
	        return true;
	    }
	    foreach ($result as $task_info)
	    {
	        $result_ex = $arr_pamas = null;
	        $arr_pamas['bk_c2_task_info'] = $task_info;
	        $obj_c2_task_assign = new c2_task_assign();
	        if ($task_info['nns_type'] == 'media')
	        {
	            $sql="select * from nns_vod_media where nns_id='{$arr_pamas['bk_c2_task_info']['nns_ref_id']}' limit 1";
	            $result_media = nl_query_by_db($sql,$obj_dc->db());
	            if(!isset($result_media[0]) || empty($result_media[0]) || empty($result_media[0]))
	            {
	               continue; 
	            }
	            $str_function = $result_media[0]['nns_deleted'] == '1' ? 'do_media_unline' : 'do_media_online';
// 	            var_dump($result_media[0]['nns_deleted'],$str_function);die;
//                 $str_function = ($arr_pamas['bk_c2_task_info']['nns_action'] == 'destroy') ? 'do_media_unline' : 'do_media_online';
                $result_ex = $obj_c2_task_assign->auto_load_task_model($sp_confog['c2_import_cdn_model'], $str_function,$obj_dc,$sp_id,$sp_confog,$arr_pamas);
	        }
	        if($result_ex === null)
	        {
	            continue;
	        }
	        if(isset($result_ex['ret']) && $result_ex['ret'] == '0')
	        {
	            nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0,nns_modify_time='{$str_date}',nns_epg_status=97 where nns_id='{$task_info['nns_id']}'", $obj_dc->db());
	        }
	        else
	        {
	            nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=-1,nns_modify_time='{$str_date}',nns_epg_status=97 where nns_id='{$task_info['nns_id']}'", $obj_dc->db());
	        }
	    }
	    return true;
	}
	
	
	/**
	 * 从内容任务表取任务执行
	 * @param string $new_id_c2 nns_mgtvbk_c2_task表记录ID
	 * @param string $c2_type 主媒资(video)、分集(index)、片源(media)
	 * @param string $sp_id 运营商ID
	 * @param bool $is_cdn_resend 是否是跑CDN注入失败重新注入脚本
	 * @return boolean
	 */
	static public function delivery_vod($obj_dc,$sp_confog,$sp_id,$c2_type=null,$new_id_c2=null)
	{
	    include_once dirname(__FILE__).'/child_models/c2_task_assign.class.php';
	    $str_date = date("Y-m-d H:i:s");
	    if(isset($new_id_c2) && strlen($new_id_c2) >0)
	    {
	        $sql = "select * from nns_mgtvbk_c2_task where nns_id='{$new_id_c2}' and nns_org_id='{$sp_id}' and nns_type='{$c2_type}' and nns_status in(6,8,9)";
	    }
	    else
	    {
	        $sql = "select * from nns_mgtvbk_c2_task where nns_org_id='{$sp_id}' and nns_type='{$c2_type}' and nns_status=6 order by nns_modify_time asc limit 30";
	    }
	    $result = nl_query_by_db($sql,$obj_dc->db());
	    if(!is_array($result) || empty($result))
	    {
	        return true;
	    }
	    foreach ($result as $task_info)
	    {
	        $result_ex = $arr_pamas = null;
	        $arr_pamas['bk_c2_task_info'] = $task_info;
	        $obj_c2_task_assign = new c2_task_assign();
	        if ($task_info['nns_type'] == 'media')
	        {
	            $str_function = ($arr_pamas['bk_c2_task_info']['nns_action'] == 'destroy') ? 'do_media_delivery_del' : 'do_media_delivery_add';
	            $result_ex = $obj_c2_task_assign->auto_load_task_model($sp_confog['c2_import_cdn_model'], $str_function,$obj_dc,$sp_id,$sp_confog,$arr_pamas);
	        }
	        if($result_ex === null)
	        {
	            continue;
	        }
	        if(($sp_id == 'suzhou_4k' || $sp_id == 'jscn_sihua_vod') && $str_function == 'do_media_delivery_del')
	        {
	            continue;
	        }
	        if(isset($result_ex['ret']) && $result_ex['ret'] == '0')
	        {
	            nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=8,nns_modify_time='{$str_date}',nns_epg_status=97 where nns_id='{$task_info['nns_id']}'", $obj_dc->db());
	        }
	        else
	        {
	            nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=9,nns_modify_time='{$str_date}',nns_epg_status=97 where nns_id='{$task_info['nns_id']}'", $obj_dc->db());
	        }
	    }
	    return true;
	}
	
	
	/**
	 * 从内容任务表取任务执行
	 * @param string $new_id_c2 nns_mgtvbk_c2_task表记录ID
	 * @param string $c2_type 主媒资(video)、分集(index)、片源(media)
	 * @param string $sp_id 运营商ID
	 * @param bool $is_cdn_resend 是否是跑CDN注入失败重新注入脚本
	 * @return boolean
	 */
	static public function playurl_vod($obj_dc,$sp_confog,$sp_id,$c2_type=null,$new_id_c2=null)
	{
	    include_once dirname(__FILE__).'/child_models/c2_task_assign.class.php';
	    $str_date = date("Y-m-d H:i:s");
	    if(isset($new_id_c2) && strlen($new_id_c2) >0)
	    {
	        $sql = "select * from nns_mgtvbk_c2_task where nns_id='{$new_id_c2}' and nns_org_id='{$sp_id}' and nns_type='{$c2_type}' and nns_status=8";
	    }
	    else
	    {
	        $sql = "select * from nns_mgtvbk_c2_task where nns_org_id='{$sp_id}' and nns_type='{$c2_type}' and nns_status=8 order by nns_modify_time asc limit 100";
	    }
	    $result = nl_query_by_db($sql,$obj_dc->db());
	    if(!is_array($result) || empty($result))
	    {
	        return true;
	    }
	    $arr_pamas = null;
        $obj_c2_task_assign = new c2_task_assign();
	    foreach ($result as $task_info)
	    {
	        if ($task_info['nns_type'] == 'media')
	        {
	            $arr_pamas['bk_c2_task_info'][] = array('nns_id'=>$task_info['nns_id'],'nns_ref_id'=>$task_info['nns_ref_id']);
	        }
	    }
	    $result_ex = $obj_c2_task_assign->auto_load_task_model($sp_confog['c2_import_cdn_model'], 'do_playurl',$obj_dc,$sp_id,$sp_confog,$arr_pamas);
	    return true;
	}
	
	/**
	 * 从内容任务表取任务执行
	 * @param string $new_id_c2 nns_mgtvbk_c2_task表记录ID
	 * @param string $c2_type 主媒资(video)、分集(index)、片源(media)
	 * @param string $sp_id 运营商ID
	 * @param bool $is_cdn_resend 是否是跑CDN注入失败重新注入脚本
	 * @return boolean
	 */
	static public function vod($new_id_c2=null, $c2_type=null, $sp_id, $is_cdn_resend = false,$sp_confog=null,$obj_dc=null)
	{
		np_runtime_ex('cdn_task',false);
		set_time_limit(0);
		DEBUG && i_write_log_core('--------开始注入vod---------',$sp_id.'/c2');
		if($obj_dc === null)
		{
		    $db = nn_get_db(NL_DB_READ);
		    $db->open();
		    $db_w = nn_get_db(NL_DB_WRITE);
		    $db_w->open();
		}
		
		else
		{
		    $db_w = $db = $obj_dc->db();
		}
		if($new_id_c2)
		{
			//这里一般是手动注入的时候调用
			$sql = "select * from nns_mgtvbk_c2_task where nns_id='$new_id_c2' and nns_org_id='$sp_id'";
		}
		else
		{
		    if(!isset($sp_config) || !is_array($sp_confog) || empty($sp_config))
		    {
    	        $sp_info = sp_model::get_sp_info($sp_id);
    	        $sp_config = json_decode($sp_info['nns_config'],true);//获取sp的策略
		    }
// 	        if(isset($sp_config['cdn_fail_retry_mode']) && $sp_config['cdn_fail_retry_mode'] == '2' && isset($sp_config['cdn_fail_retry_time']) && $sp_config['cdn_fail_retry_time'] >0)
// 	        {
// 	            self::vod($new_id_c2, $c2_type, $sp_id, true,$sp_confog,$obj_dc);
// 	        }
			$queue_task_model = new queue_task_model();
			$sql = $queue_task_model->get_cdn_sql($sp_id, $c2_type, $is_cdn_resend,$sp_confog);
			if(($sp_id=='sihua' || $sp_id == 'jscn_homed' || $sp_id == 'jsks_homed')&&$c2_type='media')
			{
				if ($is_cdn_resend)
				{
					$sql = "select * from nns_mgtvbk_c2_task where nns_org_id='{$sp_id}' and nns_type='media' and nns_status=-1 order by nns_modify_time asc limit 30";
				}
				else
				{
					$sql = "select * from nns_mgtvbk_c2_task where nns_org_id='{$sp_id}' and nns_type='media' and nns_status=1 order by nns_modify_time asc limit 30";
				}
			}
			DEBUG && i_write_log_core('--------获取任务的sql---------'.$sql,$sp_id.'/cdn_sql');
			if(($sp_id=='kukai_xiaole' || $sp_id=='sichuan_xiaole') && $c2_type='media')
			{
			    $sql_count = "select count(*) as count from nns_mgtvbk_c2_task where  nns_org_id='{$sp_id}' and nns_type='media' and nns_status not in('0','1','7','-1')";
			    $result_temp_count = nl_query_by_db($sql_count, $db);
			    $temp_count = (isset($result_temp_count[0]['count']) && (int)$result_temp_count[0]['count'] >0) ? (int)$result_temp_count[0]['count'] : 0;
			    $cdn_media_max  = (isset($sp_confog['cdn_media_max']) && (int)$sp_confog['cdn_media_max'] >0) ? (int)$sp_confog['cdn_media_max'] : 20;
			    var_dump($temp_count,$cdn_media_max);
			    if($temp_count >=$cdn_media_max)
			    {
			        return false;
			    }
			    $temp_list = $cdn_media_max-$temp_count;
			    $sql = "select * from nns_mgtvbk_c2_task where nns_org_id='{$sp_id}' and nns_type='media' and nns_status=1 order by nns_modify_time asc limit {$temp_list}";
			}
		}
		if($sql === null)
		{
			return false;
		}
		//获取C2任务
		$result = nl_db_get_all($sql,$db);

        echo '查询结果:' . var_export($result,true) . '；SQL语句：' . $sql."<hr>";
        unset($sql);
		DEBUG && i_write_log_core('--------获取任务'.count($result).'个的时间---------'.np_runtime_ex('cdn_task',true),$sp_id.'/cdn_task');
		if(count($result)==0)
		{
			return false;
		}
		if(!is_array($result) || empty($result))
		{
			return false;
		}
		//昆山广电注入模式
		if(isset($sp_confog['c2_get_third_play_url']) && strlen($sp_confog['c2_get_third_play_url']) >0 && isset($sp_confog['c2_import_cdn_model']) && $sp_confog['c2_import_cdn_model'] == '5')
		{
		    self::$sp_config = $sp_confog;
		    self::$obj_dc = $obj_dc;
		    self::$sp_id = $sp_id;
		    return self::get_cdn_play_url($result);
		}
		foreach($result as $val)
		{
			np_runtime_ex($val['nns_id'],false);
			//if($val['nns_org_id']=='sihua'){
			$bool = true;
			//}else{
			$bool = self::__check_cdn_doing_num($val['nns_org_id'],$val['nns_type']);
			//}

			if($bool===false)
			{
				return false;
			}
			//-->start xingcheng.hu 添加片源未注入成功,暂停注入其对应的分集更新操作(cntv_me)
			$sp_config = sp_model::get_sp_config($sp_id);
			if($val['nns_type']=='index' && $val['nns_action']=='modify' && strlen($sp_config['cdn_update_index_message_waiting']) >0 && $sp_config['cdn_update_index_message_waiting']==0 ){
				$sql = "select * from nns_mgtvbk_c2_task where nns_type='media' and nns_ref_id in(select nns_id from nns_vod_media where nns_vod_id='{$val['nns_src_id']}' and nns_vod_index_id='{$val['nns_ref_id']}') and nns_org_id='{$val['nns_org_id']}' and nns_src_id='{$val['nns_ref_id']}' and  nns_action='add' and nns_status=0";
				$result  = nl_db_get_one($sql,$db);
				if(!$result)
				{
					continue;
				}
			}
			//<--end
			$all_index = 0;
			$is_series = false;//默认电影
			//查询分集数量，大于1按照连续剧处理等于1按照电影处理
			if($val['nns_type']=='video')
			{
				$sql = "select nns_all_index from nns_vod where nns_id='{$val['nns_ref_id']}'";
				$all_index = nl_db_get_col($sql,$db);unset($sql);
			}
			elseif($val['nns_type']=='index')
			{
				$sql = "select nns_all_index from nns_vod where nns_id='{$val['nns_src_id']}'";
				$all_index = nl_db_get_col($sql,$db);unset($sql);
			}
			if($all_index > 1)
			{
				$is_series = true;//电视剧
			}
			if(!self::check_import($db_w, $sp_config, $val['nns_id'],$val['nns_type'],$val['nns_action'],$is_series))//true注入 false不注入
			{
				continue;
			}
            $ftp_bool = true;

			//推送vod基本信息(添加或则修改)
			if($val['nns_action'] == 'destroy')
			{
//				$sp_config = sp_model::get_sp_config($sp_id);
				//三层结构，则设置总集数大于1
				if(isset($sp_config['cdn_import_serise']) && (int)$sp_config['cdn_import_serise'] === 1)
				{
					$all_index = 2;
				}
				//删除分片或则影片
				if($val['nns_type']=='video')
				{
					DEBUG && i_write_log_core('--------删除vod操作---------',$sp_id.'/c2');
					$re = false;
					if($all_index > 1) //连续剧删除剧头
					{
					    $temp_delete_vod = array('nns_id'=>$val['nns_ref_id'],'nns_task_id'=>$val['nns_id'],'nns_task_name'=>$val['nns_name'],'nns_type'=>$val['nns_type']);
					    $temp_delete_vod['bk_c2_task_info'] = $val;
						$re = c2_task_model::delete_series($temp_delete_vod,$sp_id);//删除影片
					}
					else //电影删除主媒资，分集放过
					{
						if($sp_id == 'cntv_me')//爱上电影主媒资删除不需要注入CDN
						{
							nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0,nns_modify_time=now(),nns_epg_status=97 where nns_id='{$val['nns_id']}'", $db_w);
							include_once 'queue_task_model.php';
							$queue_task_model = new queue_task_model();
							$queue_task_model->q_report_task($val['nns_id']);
							$queue_task_model = null;
							continue;
						}
						$temp_delete_vod = array('nns_id'=>$val['nns_ref_id'],'nns_task_id'=>$val['nns_id'],'nns_task_name'=>$val['nns_name'],'nns_type'=>$val['nns_type']);
						$temp_delete_vod['bk_c2_task_info'] = $val;
						$re = c2_task_model::delete_vod($temp_delete_vod,$sp_id,$val['nns_type']);//删除影片
					}
					if(isset($sp_config['cdn_import_state_model']) && strlen($sp_config['cdn_import_state_model']) >0 && $sp_config['cdn_import_state_model']=='1')
					{
					    continue;
					}
					if((int)$sp_config['cdn_send_mode'] === 0)
					{
						$nns_status = '';
						if ($re)
						{
							$nns_status = 'nns_status=5,';
						}
						else 
						{
							$nns_status = 'nns_status=-1,';
						}
						nl_execute_by_db("update nns_mgtvbk_c2_task set $nns_status nns_modify_time=now() where nns_id='".$val['nns_id']."'", $db_w);
					}
					elseif ((int)$sp_config['cdn_send_mode'] === 1)
					{
						if(!$re)
						{
							$nns_status = 'nns_status=-1,';
							nl_execute_by_db("update nns_mgtvbk_c2_task set $nns_status nns_modify_time=now() where nns_id='".$val['nns_id']."'", $db_w);
						}
					}
				}
				elseif($val['nns_type']=='index')
				{
					DEBUG && i_write_log_core('--------删除index操作---------',$sp_id.'/c2');
					if($sp_id == 'cntv_me')//无论连续剧还是电影，爱上分集不需要注入CDN
					{
						nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0,nns_modify_time=now(),nns_epg_status=97 where nns_id='{$val['nns_id']}'", $db_w);
						include_once 'queue_task_model.php';
						$queue_task_model = new queue_task_model();
						$queue_task_model->q_report_task($val['nns_id']);
						$queue_task_model = null;
						continue;
					}
					if($all_index <= 1 && ((int)$sp_config['movie_import_cdn_enabled'] === 1 && (int)$sp_config['cdn_import_serise'] !== 1))//电影删除分集,非三层结构且电影分集不需要注入CDN（删除操作）
					{
						nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0,nns_modify_time=now(),nns_epg_status=97 where nns_id='{$val['nns_id']}'", $db_w);
						include_once 'queue_task_model.php';
						$queue_task_model = new queue_task_model();
						$queue_task_model->q_report_task($val['nns_id']);
						$queue_task_model = null;
						continue;
					}
					$temp_delete_vod = array('nns_id'=>$val['nns_ref_id'],'nns_task_id'=>$val['nns_id'],'nns_task_name'=>$val['nns_name'],'nns_type'=>$val['nns_type']);
					$temp_delete_vod['bk_c2_task_info'] = $val;
					$re = c2_task_model::delete_vod($temp_delete_vod,$sp_id,$val['nns_type']);//删除影片
					if(isset($sp_config['cdn_import_state_model']) && strlen($sp_config['cdn_import_state_model']) >0 && $sp_config['cdn_import_state_model']=='1')
					{
					    continue;
					}
					if((int)$sp_config['cdn_send_mode'] === 0)
					{
						$nns_status = '';
						if ($re)
						{
							$nns_status = 'nns_status=5,';
						}
						else 
						{
							$nns_status = 'nns_status=-1,';
						}
						nl_execute_by_db("update nns_mgtvbk_c2_task set $nns_status nns_modify_time=now() where nns_id='".$val['nns_id']."'", $db_w);
					}
					elseif ((int)$sp_config['cdn_send_mode'] === 1)
					{
						if(!$re)
						{
							$nns_status = 'nns_status=-1,';
							nl_execute_by_db("update nns_mgtvbk_c2_task set $nns_status nns_modify_time=now() where nns_id='".$val['nns_id']."'", $db_w);
						}
					}
				}
				elseif($val['nns_type']=='media')
				{
					DEBUG && i_write_log_core('--------删除media操作---------',$sp_id.'/c2');
					if($sp_id=='sihua')
					{
						self::__push_vod_media($val);
					}
					else
					{
					    if(($sp_id=='xjdx_hw' || $sp_id=='xjdx_zte') && $val['nns_cp_id'] == 'cbczq')
					    {
					        $temp_nns_org_id = $sp_id=='xjdx_hw' ? 'cbczq_hw' : 'cbczq_zte';
					        $sql_unique = "select * from nns_mgtvbk_c2_task where nns_type='{$val['nns_type']}' and nns_ref_id='{$val['nns_ref_id']}' and nns_org_id='{$temp_nns_org_id}' and nns_action='{$val['nns_action']}' order by nns_modify_time desc limit 1";
					        $all_unique = nl_query_by_db($sql_unique,$db);
					        if(isset($all_unique[0]['nns_status'])  && $all_unique[0]['nns_status'] != '5')
					        {
					            nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status='{$all_unique[0]['nns_status']}',nns_modify_time=now() where nns_id='".$val['nns_id']."'", $db_w);
					            if($all_unique[0]['nns_status'] == '0')
					            {
					                $queue_task_model = new queue_task_model();
					                $queue_task_model->q_report_task($val['nns_id']);
					                unset($queue_task_model);
					                $queue_task_model=null;
					            }
					        }
					        else
					        {
					            nl_execute_by_db("update nns_mgtvbk_c2_task set nns_modify_time=now() where nns_id='".$val['nns_id']."'", $db_w);
					        }
					        continue;
					    }
					    $temp_movie_del = array('nns_id'=>$val['nns_ref_id'],'nns_clip_task_id'=>$val['nns_clip_task_id'],'nns_task_id'=>$val['nns_id'],'nns_task_name'=>$val['nns_name'],'nns_type'=>$val['nns_type']);
					    $temp_movie_del['bk_c2_task_info'] = $val;
						$re = c2_task_model::delete_movie($temp_movie_del,$sp_id);//删除影片
						if(isset($sp_config['cdn_import_state_model']) && strlen($sp_config['cdn_import_state_model']) >0 && $sp_config['cdn_import_state_model']=='1')
						{
						    continue;
						}
						if((int)$sp_config['cdn_send_mode'] === 0)
						{
							$nns_status = '';
							if ($re)
							{
								$nns_status = 'nns_status=5,';
							}
							else 
							{
								$nns_status = 'nns_status=-1,';
							}
							nl_execute_by_db("update nns_mgtvbk_c2_task set $nns_status nns_modify_time=now() where nns_id='".$val['nns_id']."'", $db_w);
						}
						elseif ((int)$sp_config['cdn_send_mode'] === 1)
						{
							if(!$re)
							{
								$nns_status = 'nns_status=-1,';
								nl_execute_by_db("update nns_mgtvbk_c2_task set $nns_status nns_modify_time=now() where nns_id='".$val['nns_id']."'", $db_w);
							}
						}
					}
				}
				else if($val['nns_type']=='playbill')
				{
					$temp_playbill_info = array('nns_playbill_id'=>$val['nns_ref_id'],'nns_task_id'=>$val['nns_id'],'nns_type'=>$val['nns_type']);
					$temp_playbill_info['bk_c2_task_info'] = $val;
// 					$result = c2_task_model::add_playbill($temp_playbill_info,null,$val['nns_org_id']);
                    $result = c2_task_model::do_playbill_v2($temp_playbill_info, 'DELETE', $val['nns_org_id']);
				}
				else if($val['nns_type']=='live_media')
				{
					$live_id_arr = nl_db_get_one("select * from nns_live_media where nns_id='{$val['nns_ref_id']}'", $db);
					$live_info = array('nns_channel_id'=>$live_id_arr['nns_live_id'],'nns_media_id'=>$val['nns_ref_id'],'nns_task_id'=>$val['nns_id'],'nns_type'=>$val['nns_type']);
					$live_info['bk_c2_task_info'] = $val;
					$result = c2_task_model::do_live($live_info,'DELETE',$val['nns_org_id']);
				}
				else if($val['nns_type']=='live')
				{
				    $live_info = array('nns_channel_id'=>$val['nns_ref_id']);
				    $live_info['bk_c2_task_info'] = $val;
				    $result = c2_task_model::do_channel($live_info,'DELETE',$val['nns_org_id']);
				}
				else if($val['nns_type']=='file')
				{
				    $file_package_arr = nl_db_get_one("select * from nns_file_package where nns_id='{$val['nns_ref_id']}'", $db);
				    $file_package_info = array('nns_file_id'=>$live_id_arr['nns_live_id'],'nns_task_id'=>$val['nns_id'],'nns_type'=>$val['nns_type']);
				    $file_package_info['bk_c2_task_info'] = $val;
				    $result = c2_task_model::do_file($file_package_info,null,$val['nns_org_id']);
				}
			}
			else
			{
				    DEBUG && i_write_log_core('-----------'.$val['nns_action'].'操作---------'.$val['nns_type']."\n".var_export($val,true),$sp_id.'/c2');
					if($val['nns_type'] == 'video' || $val['nns_type'] == 'index')
					{//影片
						//self::__push_vod_info($db, $val['nns_ref_id'],$val['nns_action'],$val['nns_type'],$val['nns_src_id'],$val['nns_id'],$val['nns_org_id']);
					    $val['bk_c2_task_info'] = $val;
					    self::__push_vod_info($val,$sp_id);
					    $ftp_bool = false;
					}
					elseif($val['nns_type']=='media')
					{
					    if(($sp_id=='xjdx_hw' || $sp_id=='xjdx_zte') && $val['nns_cp_id'] == 'cbczq')
					    {
					        $temp_nns_org_id = $sp_id=='xjdx_hw' ? 'cbczq_hw' : 'cbczq_zte';
					        $sql_unique = "select * from nns_mgtvbk_c2_task where nns_type='{$val['nns_type']}' and nns_ref_id='{$val['nns_ref_id']}' and nns_org_id='{$temp_nns_org_id}' and nns_action!='destroy' order by nns_modify_time desc limit 1";
					        $all_unique = nl_query_by_db($sql_unique,$db);
					        if(isset($all_unique[0]['nns_status']) && $all_unique[0]['nns_status'] != '5')
					        {
					            nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status='{$all_unique[0]['nns_status']}',nns_modify_time=now() where nns_id='".$val['nns_id']."'", $db_w);
					            if($all_unique[0]['nns_status'] == '0')
					            {
    					            $queue_task_model = new queue_task_model();
    					            $queue_task_model->q_report_task($val['nns_id']);
    					            unset($queue_task_model);
    					            $queue_task_model=null;
					            }
					        }
					        else
					        {
					            nl_execute_by_db("update nns_mgtvbk_c2_task set nns_modify_time=now() where nns_id='".$val['nns_id']."'", $db_w);
					        }
				            continue;
					    }
					    $val['bk_c2_task_info'] = $val;
						$result = self::__push_vod_media($val);
                        $ftp_bool = true;
					}
					else if($val['nns_type']=='playbill')
					{
						$temp_playbill_info = array('nns_playbill_id'=>$val['nns_ref_id'],'nns_task_id'=>$val['nns_id'],'nns_type'=>$val['nns_type']);
						$temp_playbill_info['bk_c2_task_info'] = $val;
//					    $result = c2_task_model::add_playbill($temp_playbill_info,null,$val['nns_org_id']);
                        $result = c2_task_model::do_playbill_v2($temp_playbill_info, 'REGIST', $val['nns_org_id']);
					}
					else if($val['nns_type']=='live_media')
					{
						$live_id_arr = nl_db_get_one("select * from nns_live_media where nns_id='{$val['nns_ref_id']}'", $db);
						$live_info = array('nns_channel_id'=>$live_id_arr['nns_live_id'],'nns_media_id'=>$val['nns_ref_id'],'nns_task_id'=>$val['nns_id'],'nns_type'=>$val['nns_type']);
						$live_info['bk_c2_task_info'] = $val;
					    $result = c2_task_model::do_live($live_info,'REGIST',$val['nns_org_id']);
					}
					else if($val['nns_type']=='live')
					{
					    $live_info = array('nns_channel_id'=>$val['nns_ref_id']);
					    $live_info['bk_c2_task_info'] = $val;
					    $result = c2_task_model::do_channel($live_info,'REGIST',$val['nns_org_id']);
					}
					else if($val['nns_type']=='file')
					{
					    $file_package_arr = nl_db_get_one("select * from nns_file_package where nns_id='{$val['nns_ref_id']}'", $db);
					    $file_package_info = array('nns_file_id'=>$live_id_arr['nns_live_id'],'nns_task_id'=>$val['nns_id'],'nns_type'=>$val['nns_type']);
					    $file_package_info['bk_c2_task_info'] = $val;
					    $result = c2_task_model::do_file($file_package_info,null,$val['nns_org_id']);
					}
			}
			if(isset($sp_config['cdn_import_state_model']) && strlen($sp_config['cdn_import_state_model']) >0 && $sp_config['cdn_import_state_model']=='1')
			{
			    continue;
			}
			if(in_array($val['nns_type'], array('live','playbill','live_media','file')) && (!isset($sp_config['c2_import_cdn_model']) || $sp_config['c2_import_cdn_model'] !='2'))
			{
                /**
                 * 兼容返回值，有一些CDN注入方法返回数据为true和false，有一些返回的是数组格式
                 * @author chunyang.shu
                 * @date 2017-10-30
                 */
			    if(!isset($sp_config['playbill']['enabled']) || $sp_config['playbill']['enabled'] != '1')
			    {
			        $str_date = date("Y-m-d H:i:s");
			        if((isset($result['ret']) && $result['ret'] == '0') || true === $result)
			        {
			            nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time='{$str_date}',nns_epg_status=97 where nns_id='{$val['nns_id']}'", $db_w);
			        }
			        else
			        {
			            nl_execute_by_db("update nns_mgtvbk_c2_task set nns_modify_time='{$str_date}',nns_epg_status=97,nns_cdn_fail_time=nns_cdn_fail_time+1 where nns_id='{$val['nns_id']}'", $db_w);
			        }
			    }
			    
			}
			if($sp_config['cdn_send_mode'] =='2' && (!isset($sp_config['ftp_cdn_send_mode_status']) || (int)$sp_config['ftp_cdn_send_mode_status'] === 0) && $ftp_bool) //FTP不需要反馈
			{
                if((isset($result['ret']) && $result['ret'] == '0') || (isset($result) && is_bool($result) && $result)  || (isset($re) && $re))
                {
                    nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0,nns_modify_time=now(),nns_epg_status=97 where nns_id='{$val['nns_id']}'", $db_w);
                }
                else
                {
                    nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=-1,nns_modify_time=now(),nns_epg_status=97,nns_cdn_fail_time=nns_cdn_fail_time+1 where nns_id='{$val['nns_id']}'", $db_w);
                }
			    include_once 'queue_task_model.php';
			    $queue_task_model = new queue_task_model();
			    $queue_task_model->q_report_task($val['nns_id']);
			    $queue_task_model = null;
			    continue;
			}
			elseif ($sp_config['cdn_send_mode'] =='2' && isset($sp_config['ftp_cdn_send_mode_status']) && (int)$sp_config['ftp_cdn_send_mode_status'] === 1 && $ftp_bool)
            {
                if((isset($result['ret']) && $result['ret'] == '0') || (isset($result) && is_bool($result) && $result)  || (isset($re) && $re))
                {
                    if($sp_id == 'suzhou_4k' && $val['nns_action'] == 'destroy')
                    {
                        nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=6,nns_modify_time=now(),nns_epg_status=97 where nns_id='{$val['nns_id']}'", $db_w);
                    }
                    else
                    {
                        if(in_array($val['nns_type'], array('video','index')))
                        {
                            nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0,nns_modify_time=now(),nns_epg_status=97 where nns_id='{$val['nns_id']}'", $db_w);
                        }
                        else
                        {
                            nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now(),nns_epg_status=97 where nns_id='{$val['nns_id']}'", $db_w);
                        }
                    }
                }
                else
                {
                    nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=-1,nns_modify_time=now(),nns_epg_status=97,nns_cdn_fail_time=nns_cdn_fail_time+1 where nns_id='{$val['nns_id']}'", $db_w);
                }
            }
			//return $result_vod;
			DEBUG && i_write_log_core('--------执行'.$val['nns_name'].'用时间---------'.np_runtime_ex($val['nns_id'],true),$sp_id.'/c2/'.$val['nns_type']);
		}
		return true;
	}
	
	
	/**
	 * 从内容任务表取任务执行  一个临时的删除cdn的队列控制器
	 * 梁攀 2015-04-30
	 */
	static public function vod_v2($new_id_c2=null,$c2_type=null,$sp_id){
	
		np_runtime_ex('cdn_task',false);
		//echo $new_id_c2
		set_time_limit(0);
		DEBUG && i_write_log_core('--------开始注入vod---------',$sp_id.'/c2');
		$db = nn_get_db(NL_DB_READ);
		$db->open();
		$db_w = nn_get_db(NL_DB_WRITE);
		$db_w->open();
		if($new_id_c2){
			$sql  = "select * from nns_mgtvbk_c2_task where nns_id='$new_id_c2' and nns_org_id='$sp_id'";
		}else{
			$queue_task_model = new queue_task_model();
			$sql = $queue_task_model->get_cdn_sql($sp_id, $c2_type);
			//die($sql);
			//echo $sql;
			if($sp_id=='sihua'&&$c2_type='media'){
				$sql = "select * from nns_mgtvbk_c2_task where nns_org_id='sihua' and nns_type='media' and nns_status=1 and nns_action='destroy' order by nns_modify_time desc limit 30";
			}
			// die($sql);
			DEBUG && i_write_log_core('--------获取任务的sql---------'.$sql,$sp_id.'/cdn_sql');
		}
	
		//echo $sql;
	
		if($sql===null){
			return false;
		}
		$result = nl_db_get_all($sql,$db);unset($sql);
	
	
		DEBUG && i_write_log_core('--------获取任务'.count($result).'个的时间---------'.np_runtime_ex('cdn_task',true),$sp_id.'/cdn_task');
	
		//var_dump($result);die;
		//die('1');
		if(count($result)==0){
			return false;
		}
		//die('5');
		if(!is_array($result)||empty($result)){
			return false;
		}
		//die('2');
		foreach($result as $val){
			np_runtime_ex($val['nns_id'],false);
			//echo 'do--';
			//if($val['nns_org_id']=='sihua'){
			$bool = true;
			//}else{
			$bool = self::__check_cdn_doing_num($val['nns_org_id'],$val['nns_type']);
			//}
			//var_dump($bool);die;
			if($bool===false){
				return false;
			}
			$all_index = 0;
			//查询分集数量，大于1按照连续剧处理等于1按照电影处理
			if($val['nns_type']=='video'){
				$sql = "select nns_all_index from nns_vod where nns_id='{$val['nns_ref_id']}'";
			}elseif($val['nns_type']=='index'){
				$sql = "select nns_all_index from nns_vod where nns_id='{$val['nns_src_id']}'";
			}
			$all_index = nl_db_get_col($sql,$db);unset($sql);
				
			//推送vod基本信息(添加或则修改)
			if($val['nns_action']=='destroy'){
	
				$sp_config = sp_model::get_sp_config($sp_id);
				if(isset($sp_config['cdn_import_serise'])&&(int)$sp_config['cdn_import_serise']===1){
					$all_index = 2;
				}
	
				//删除分片或则影片
				if($val['nns_type']=='video'){
					DEBUG && i_write_log_core('--------删除vod操作---------',$sp_id.'/c2');
					if($all_index>1){
						c2_task_model::delete_series(array('nns_id'=>$val['nns_ref_id'],'nns_task_id'=>$val['nns_id'],'nns_task_name'=>$val['nns_name']),$sp_id);//删除影片
					}else{
						c2_task_model::delete_vod(array('nns_id'=>$val['nns_ref_id'],'nns_task_id'=>$val['nns_id'],'nns_task_name'=>$val['nns_name']),$sp_id);//删除影片
					}
					nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='".$val['nns_id']."'", $db_w);
				}elseif($val['nns_type']=='index'){
					DEBUG && i_write_log_core('--------删除index操作---------',$sp_id.'/c2');
					c2_task_model::delete_vod(array('nns_id'=>$val['nns_ref_id'],'nns_task_id'=>$val['nns_id'],'nns_task_name'=>$val['nns_name']),$sp_id);//删除影片
					nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='".$val['nns_id']."'", $db_w);
					//unset($nns_status);
				}elseif($val['nns_type']=='media'){
					DEBUG && i_write_log_core('--------删除media操作---------',$sp_id.'/c2');
						
					if($sp_id=='sihua'){
						self::__push_vod_media($val);
					}else{
						c2_task_model::delete_movie(array('nns_id'=>$val['nns_ref_id'],'nns_clip_task_id'=>$val['nns_clip_task_id'],'nns_task_id'=>$val['nns_id'],'nns_task_name'=>$val['nns_name']),$sp_id);//删除影片
						nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='".$val['nns_id']."'", $db_w);
	
					}
				}
			}else{
				DEBUG && i_write_log_core('-----------'.$val['nns_action'].'操作---------'.$val['nns_type']."\n".var_export($val,true),$sp_id.'/c2');
				if($val['nns_type']=='video'||$val['nns_type']=='index'){//影片
					//self::__push_vod_info($db, $val['nns_ref_id'],$val['nns_action'],$val['nns_type'],$val['nns_src_id'],$val['nns_id'],$val['nns_org_id']);
					self::__push_vod_info($val,$sp_id);
				}elseif($val['nns_type']=='media'){
					self::__push_vod_media($val);
				}
			}
			//return $result_vod;
			DEBUG && i_write_log_core('--------执行'.$val['nns_name'].'用时间---------'.np_runtime_ex($val['nns_id'],true),$sp_id.'/c2/'.$val['nns_type']);
		}
		return true;
	}

	static public function __push_vod_media($val)
	{
		set_time_limit(0);

		$db = nn_get_db(NL_DB_READ);
		$db_w = nn_get_db(NL_DB_WRITE);
		//检查列队中现在有多少空余的可以注入
		$sp_id = $val['nns_org_id'];

		$sp_info = sp_model::get_sp_config($sp_id);
		$vod_id_arr = nl_db_get_one("select nns_vod_id,nns_id from nns_vod_index where nns_id='{$val['nns_src_id']}'", $db);
		$clip = true;
		$vod_id = $vod_id_arr['nns_vod_id'];
		$vod_index_id = $vod_id_arr['nns_id'];
		$data = array(
			'nns_video_id'=>$vod_id,
			'nns_video_index_id'=>$val['nns_src_id'],
			'nns_clip_task_id'=>$val['nns_clip_task_id'],
			'nns_task_id' => $val['nns_id'],
			'nns_clip_date'=>$val['nns_clip_date'],
			'nns_media_id'=>$val['nns_ref_id'],
			'nns_date'=>$val['nns_clip_date'],
			'nns_file_path'=>$val['nns_file_path'],
			'nns_file_size'=>$val['nns_file_size'],
			'nns_file_md5'=>$val['nns_file_md5'],
			'clip'=>$clip,
			'nns_task_name'=>$val['nns_name'],
			'nns_action'=>$val['nns_action'],
		);
		if(isset($val['bk_c2_task_info']))
		{
		    $data['bk_c2_task_info'] = $val['bk_c2_task_info'];
		}
		//是否需要检查主媒资上游注入
		if(isset($sp_info['cdn_media_check_video_enabled']) && $sp_info['cdn_media_check_video_enabled'] ==1)
		{
    		$re_video = self::__check_video($sp_id, $vod_id);
		}
		//是否需要检查分集上游注入
		if(isset($sp_info['cdn_media_check_index_enabled']) && $sp_info['cdn_media_check_index_enabled'] ==1)
		{
		    $re_video_index = self::__check_video_index($sp_id, $vod_index_id);
		}
		
		//如果是思华且是删除操作
		if($sp_id == 'sihua' && $val['nns_action'] == 'destroy')
		{
			$result = c2_task_model::delete_movie($data,$sp_id);
		}
		else 
		{
			$result = c2_task_model::add_movies_by_vod_index($data,$sp_id);
		}
		if(isset($sp_info['cdn_import_state_model']) && strlen($sp_info['cdn_import_state_model']) >0 && $sp_info['cdn_import_state_model']=='1')
		{
		   return true;
		}
		if((int)$sp_info['cdn_send_mode'] === 0)
		{
			$nns_status = '';
			if($result)
			{
				$nns_status = 'nns_status=5,';
			}
			else
			{
				$nns_status = 'nns_status=-1,';
			}
			nl_execute_by_db("update nns_mgtvbk_c2_task set $nns_status nns_modify_time=now() where nns_id='{$val['nns_id']}'", $db_w);
		}
		elseif ((int)$sp_info['cdn_send_mode'] === 1)//HTTP 注入
        {
            if (!$result) //注入失败，更改状态失败
            {
                $nns_status = 'nns_status=-1,';
                nl_execute_by_db("update nns_mgtvbk_c2_task set $nns_status nns_modify_time=now(),nns_cdn_fail_time=nns_cdn_fail_time+1 where nns_id='{$val['nns_id']}'", $db_w);
            }
            else //注入成功，C2状态更改由C2注入模块自写
            {
                include_once 'queue_task_model.php';
                $queue_task_model = new queue_task_model();
                $queue_task_model->q_report_task($val['nns_id']);
                $queue_task_model = null;
            }
		}
		elseif((int)$sp_info['cdn_send_mode'] === 2 && (!isset($sp_info['ftp_cdn_send_mode_status']) || (int)$sp_info['ftp_cdn_send_mode_status'] === 0))//FTP注入
        {
//            if($result)
//            {
//                $nns_status = 'nns_status=5,';
//            }
//            else
//            {
//                $nns_status = 'nns_status=-1,';
//            }
//            nl_execute_by_db("update nns_mgtvbk_c2_task set $nns_status nns_modify_time=now() where nns_id='{$val['nns_id']}'", $db_w);
        }
		return true;
	}

	
	//static private function __push_vod_info($db, $nns_id,$action,$type,$src_id,$c2_task_id,$org_id){
	/**
	 * 注入影片主媒资或分集
	 * @param unknown $val
	 * @param unknown $sp_id
	 * @return void|Ambigous <void, boolean, unknown>
	 */
	static private function __push_vod_info($val,$sp_id)
	{
		set_time_limit(0);
		$db = nn_get_db(NL_DB_READ);
		if($val['nns_type'] == 'video')
		{
			$sql = "select * from nns_vod where nns_id='{$val['nns_ref_id']}'";
		}
		elseif($val['nns_type']=='index')
		{
			$sql = "select * from nns_vod where nns_id='{$val['nns_src_id']}'";
		}
		$result = nl_db_get_one($sql,$db);unset($sql);
		if($result===false) return ;
		for ($i=0; $i <=31 ; $i++) 
		{ 
			unset($result["nns_tag$i"]);
		}

		//return self::__push_vod($db,$result,$nns_id,$action,$c2_task_id,$org_id,$type,$src_id);
		return self::__push_vod($result,$sp_id,$val);
	}

	//static private function __push_vod($db,$result,$nns_id,$action,$c2_task_id,$org_id,$type,$src_id){
	static private function __push_vod($result,$sp_id,$c2_task_info)
	{
		set_time_limit(0);
		$result_new_cdn_ex = 0;
		$db_w = nn_get_db(NL_DB_WRITE);
		/**
		 * 注入CDN新分发模式
		 * @pan.liang
		 */
		if($c2_task_info['nns_type']=='index')
		{
		    $vod_id = $c2_task_info['nns_src_id'];
		    $result_new_cdn_ex = c2_task_model::do_program($c2_task_info,$sp_id);
		}
		elseif($c2_task_info['nns_type']=='video')
		{
		    $vod_id = $c2_task_info['nns_ref_id'];
		    $result_new_cdn_ex = c2_task_model::do_series_v2($c2_task_info, $sp_id);
		}
		if($result_new_cdn_ex!== 0)
		{
		    $sp_config = sp_model::get_sp_config($sp_id);
		    if(isset($sp_config['cdn_import_state_model']) && strlen($sp_config['cdn_import_state_model']) >0 && $sp_config['cdn_import_state_model']=='1')
		    {
                include_once 'queue_task_model.php';
                $queue_task_model = new queue_task_model();
                $queue_task_model->q_report_task($c2_task_info['nns_id']);
                $queue_task_model = null;
		        return $result_new_cdn_ex;
		    }
		    if((int)$sp_config['cdn_send_mode'] == '2' && (!isset($sp_config['ftp_cdn_send_mode_status']) || (int)$sp_config['ftp_cdn_send_mode_status'] === 0))//FTP不需要异步反馈
		    {
                include_once 'queue_task_model.php';
                $queue_task_model = new queue_task_model();
                $queue_task_model->q_report_task($c2_task_info['nns_id']);
                $queue_task_model = null;
                return $result_new_cdn_ex;
		    }
            if((int)$sp_config['cdn_send_mode'] === 1) //HTTP注入
            {
                if(!$result_new_cdn_ex) //注入失败，更改状态为失败
                {
                    nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=-1,nns_modify_time=now(),nns_cdn_fail_time=nns_cdn_fail_time+1 where nns_id='{$c2_task_info['nns_id']}'", $db_w);
                }
                else  //注入成功，C2状态更改由C2注入模块自写
                {
                    include_once 'queue_task_model.php';
                    $queue_task_model = new queue_task_model();
                    $queue_task_model->q_report_task($c2_task_info['nns_id']);
                    $queue_task_model = null;
                }
                return $result_new_cdn_ex;
            }
		    $nns_status = ($result_new_cdn_ex) ? 'nns_status=5,' : 'nns_status=-1,';
		    nl_execute_by_db("update nns_mgtvbk_c2_task set $nns_status nns_modify_time=now() where nns_id='{$c2_task_info['nns_id']}'", $db_w);
		    return $result_new_cdn_ex;
		}
		
		
		//查询改片属于那个栏目
		$db = nn_get_db(NL_DB_READ);
		$sp_category = array();
		
		$sql = "select nns_sp_category_id from nns_mgtvbk_category_content content left join nns_mgtvbk_category category on (content.nns_org_id=category.nns_org_id and content.nns_category_id=category.nns_id) where content.nns_org_id='$sp_id' and content.nns_video_id='$vod_id'  and content.nns_video_type=0 group by category.nns_id";
		$sp_category1 = array();
		/*$result_category = nl_db_get_all($sql,$db);unset($sql);
		foreach ($result_category as  $value) {
			$sp_category1[] = $value['nns_sp_category_id'];
		}*/
		$sp_category2=array();
		$sql = "select nns_sp_category_id from nns_mgtvbk_category where nns_org_id='$sp_id' and bind_vod_item='{$result['nns_category_id']}'";
		$result_category = nl_db_get_all($sql,$db);unset($sql);
		foreach ($result_category as  $value) 
		{
			$sp_category2[] = $value['nns_sp_category_id'];
		}
		$sp_category = array_unique(array_merge($sp_category1,$sp_category2));

		$result_vod = $result;
		//在此只判断是按照电影来推送消息还是按照连续剧来推送消息
		$is_movie = true;
		if($result['nns_all_index'] > 1 && $c2_task_info['nns_type'] == 'index')
		{//如果是连续剧并且是分集标识的先检查连续剧基本信息有没有注入	
			$is_movie = false;
		}
		elseif($result['nns_all_index'] == 1 && $c2_task_info['nns_type'] == 'index')
		{//如果分集等于1并且是index，则当影片处理
			$is_movie = true;
		}
		elseif($result['nns_all_index'] > 1 && $c2_task_info['nns_type'] == 'video')
		{//如果分集>1并且是vod，则当连续剧
			$is_movie = false;
		}
		elseif($result['nns_all_index'] == 1 && $c2_task_info['nns_type'] == 'video')
		{//如果是电影并且分集只有1，则是电影			
			$is_movie = true;
		}

		if(empty($result['nns_all_index']))
		{
			$count = nl_db_get_col("select nns_index  from nns_vod_index where nns_vod_id='$vod_id' and nns_deleted!=1 order by nns_index desc limit 1", $db);
			$result['nns_all_index'] = intval($count+1);
			$is_movie = false;
		}

		//查看sp配置是否是三层注入 如果是三层注入 则按照连续剧注入
		$sp_config = sp_model::get_sp_config($sp_id);
		if(isset($sp_config['cdn_import_serise']) && (int)$sp_config['cdn_import_serise'] === 1)
		{
			$is_movie = false;
		}
		//如果不需要注入栏目就传空
		if(isset($sp_config['disabled_category']) && (int)$sp_config['disabled_category'] === 1)
		{
			$sp_category = array();
		}

		//全部按照连续剧来注入
		//$is_movie = false;

		//初始化基本信息
		$result_vod['nns_category_id'] = $sp_category;
		$nns_alias_name = $result_vod['nns_alias_name'];
		$nns_area = $result_vod['nns_area'];
		$nns_language = $result_vod['nns_language'];
		$nns_show_time = $result_vod['nns_show_time'];
		$nns_kind = $result_vod['nns_kind'];
		$nns_keyword = $result_vod['nns_keyword'];

		//插入分集或则影片的信息
		if($c2_task_info['nns_type']=='video')
		{
			if($is_movie)//电影
			{
				if($sp_id == 'gxiptv' || $sp_id == 'gx_iptv')//广西新媒体不需要注入CDN
				{
					nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0,nns_modify_time=now(),nns_epg_status=97 where nns_id='{$c2_task_info['nns_id']}'", $db_w);
					include_once 'queue_task_model.php';
					$queue_task_model = new queue_task_model();
					$queue_task_model->q_report_task($c2_task_info['nns_id']);
					$queue_task_model = null;
					return true;
				}
				if((int)$sp_config['movie_import_cdn_enabled'] === 1 && (int)$sp_config['cdn_import_serise'] !== 1)//非三层结构注入模式且电影两层注入方式，注入主媒资，分集不注入
				{
					DEBUG && i_write_log_core('--------注入的是电影主媒资(两层结构)---------',$sp_id.'/c2');
					$result_vod['nns_series_flag'] = 0;
					$result_vod['nns_task_id'] = $c2_task_info['nns_id'];
					$result_vod['nns_task_name'] = $result_vod['nns_name'];
					if($c2_task_info['nns_action']=='add')
					{
						$rs = c2_task_model::add_vod($result_vod,$sp_id);
					}
					elseif($c2_task_info['nns_action']=='modify')
					{
						$rs = c2_task_model::update_vod($result_vod,$sp_id);
					}
				}
				else 
				{
					$sql = "select * from nns_vod_index where  nns_vod_id='{$c2_task_info['nns_ref_id']}' and nns_deleted!=1";
					$result_num = nl_db_get_one($sql,$db);unset($sql);
					if(!is_array($result_num))
					{
						DEBUG && i_write_log_core('注入任务已不存在或者媒资信息已删除',$sp_id.'/c2');
						nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=7,nns_epg_status=100,nns_modify_time=now() where nns_id='{$c2_task_info['nns_id']}'", $db_w);
						include_once 'queue_task_model.php';
						$queue_task_model = new queue_task_model();
						$queue_task_model->set_child_task_cancel($sp_id,$c2_task_info['nns_type'],$c2_task_info['nns_ref_id']);
						$queue_task_model = null;
						return true;
					}
					$result_vod['nns_series_flag'] = 0;
					$result_vod['nns_task_id'] = $c2_task_info['nns_id'];
					$result_vod['nns_id'] = $result_num['nns_id'];
					$result_vod['nns_task_name'] = $result_vod['nns_name'];

					//查询主媒资有没有生成任务
					//self::__check_video($sp_id, $c2_task_info['nns_ref_id']);

					if($c2_task_info['nns_action']=='add')
					{
						if($sp_id == 'cntv_me')//爱上 如果是影片 ，主媒资不需要注入CDN
						{
							DEBUG && i_write_log_core('--------注入的是电影主媒资---------',$sp_id.'/c2');
							nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0,nns_modify_time=now(),nns_epg_status=97 where nns_id='{$c2_task_info['nns_id']}'", $db_w);
							include_once 'queue_task_model.php';
							$queue_task_model = new queue_task_model();
							$queue_task_model->q_report_task($c2_task_info['nns_id']);
							$queue_task_model = null;
							return true;
						}
						$rs = c2_task_model::add_vod($result_vod,$sp_id);
					}
					elseif($c2_task_info['nns_action']=='modify')
					{	
						if($sp_id == 'cntv_me')//爱上 如果是影片且动作是修改，需通过片源重新注入CDN
						{
							$rs = self::__push_modify_vod_info($c2_task_info,$is_movie);
						}
						else 
						{
							$rs = c2_task_model::update_vod($result_vod,$sp_id);
						}
					}
				}
				if((int)$sp_config['cdn_send_mode'] === 0)
				{
					$nns_status = '';
					if($rs && $sp_id!='jsyx')
					{
						$nns_status = 'nns_status=5,';
					}
					elseif (!$rs && $sp_id!='jsyx')
					{
						$nns_status = 'nns_status=-1,';
					}
					nl_execute_by_db("update nns_mgtvbk_c2_task set $nns_status nns_modify_time=now() where nns_id='{$c2_task_info['nns_id']}'", $db_w);
				}
				elseif ((int)$sp_config['cdn_send_mode'] === 1)//HTTP 注入失败 标记任务失败
				{
					if(!$rs)
					{
						$nns_status = 'nns_status=-1,';
						nl_execute_by_db("update nns_mgtvbk_c2_task set $nns_status nns_modify_time=now() where nns_id='{$c2_task_info['nns_id']}'", $db_w);
					}
				}
			}
			else
			{//连续剧
				$sql = "select * from nns_vod where  nns_id='{$c2_task_info['nns_ref_id']}' and nns_deleted!=1";
				$result_num = nl_db_get_one($sql,$db);unset($sql);
				if(!is_array($result_num))
				{
					DEBUG && i_write_log_core('注入任务已不存在或者媒资信息已删除',$sp_id.'/c2');
					nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=7,nns_epg_status=100,nns_modify_time=now() where nns_id='{$c2_task_info['nns_id']}'", $db_w);
					include_once 'queue_task_model.php';
					$queue_task_model = new queue_task_model();
					$queue_task_model->set_child_task_cancel($sp_id,$c2_task_info['nns_type'],$c2_task_info['nns_ref_id']);
					$queue_task_model = null;
					return true;
				}
				$result_vod['nns_series_flag'] = 1;
				$result_vod['nns_task_id'] = $c2_task_info['nns_id'];
				$result_vod['nns_id'] = $result_num['nns_id'];
				$result_vod['nns_task_name'] = $result_vod['nns_name'];
				DEBUG && i_write_log_core('--------注入的是连续剧主媒资---------',$sp_id.'/c2');
				if($c2_task_info['nns_action']=='add')
				{
					$rs = c2_task_model::add_series($result_vod,$sp_id);
				}
				elseif($c2_task_info['nns_action']=='modify')
				{
					$rs = c2_task_model::update_series($result_vod,$sp_id);
				}
				if((int)$sp_config['cdn_send_mode'] === 0)
				{
					$nns_status = '';
					if($rs && $sp_id!='jsyx')
					{
						$nns_status = 'nns_status=5,';
					}
					elseif (!$rs && $sp_id!='jsyx')
					{
						$nns_status = 'nns_status=-1,';
					}
					nl_execute_by_db("update nns_mgtvbk_c2_task set $nns_status nns_modify_time=now() where nns_id='{$c2_task_info['nns_id']}'", $db_w);
				}
				elseif ((int)$sp_config['cdn_send_mode'] === 1)//HTTP 注入失败 标记任务失败
				{
					if(!$rs)
					{
						$nns_status = 'nns_status=-1,';
						nl_execute_by_db("update nns_mgtvbk_c2_task set $nns_status nns_modify_time=now() where nns_id='{$c2_task_info['nns_id']}'", $db_w);
					}
				}
			}
		}
		elseif($c2_task_info['nns_type']=='index')
		{
			if($sp_id == 'gxiptv' || $sp_id == 'gx_iptv')//广西新媒体不需要注入CDN
			{
				nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0,nns_modify_time=now(),nns_epg_status=97 where nns_id='{$c2_task_info['nns_id']}'", $db_w);
				include_once 'queue_task_model.php';
				$queue_task_model = new queue_task_model();
				$queue_task_model->q_report_task($c2_task_info['nns_id']);
				$queue_task_model = null;
				return true;
			}
			$sql = "select * from nns_vod_index where  nns_id='{$c2_task_info['nns_ref_id']}' and nns_deleted!=1";
			$vod = nl_db_get_one($sql,$db);unset($sql);
			if(empty($vod) || $vod === false || $vod===null)
			{
// 				nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=-1,nns_modify_time=now()  where nns_id='{$c2_task_info['nns_id']}'", $db_w);
// 				return false;
				DEBUG && i_write_log_core('注入任务已不存在或者媒资信息已删除',$sp_id.'/c2');
				nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=7,nns_epg_status=100,nns_modify_time=now() where nns_id='{$c2_task_info['nns_id']}'", $db_w);
				include_once 'queue_task_model.php';
				$queue_task_model = new queue_task_model();
				$queue_task_model->set_child_task_cancel($sp_id,$c2_task_info['nns_type'],$c2_task_info['nns_ref_id']);
				$queue_task_model = null;
				return true;
			}
			if($is_movie)
			{//电影
				if((int)$sp_config['movie_import_cdn_enabled'] === 1 && (int)$sp_config['cdn_import_serise'] !== 1)//非三层结构注入模式且电影分集不注入
				{
					DEBUG && i_write_log_core('--------注入的是电影分集且为非三层结构注入模式(不需要注入CDN)---------',$sp_id.'/c2');
					nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0,nns_modify_time=now(),nns_epg_status=97 where nns_id='{$c2_task_info['nns_id']}'", $db_w);
					include_once 'queue_task_model.php';
					$queue_task_model = new queue_task_model();
					$queue_task_model->q_report_task($c2_task_info['nns_id']);
					$queue_task_model = null;
					return true;
				}
				$result_vod['nns_series_flag'] = 0;
			}
			else
			{//连续剧
				$result_vod['nns_series_flag'] = 1;
				$result_vod['nns_vod_id'] = $vod['nns_vod_id'];
				$result_vod['nns_name'] =  $vod['nns_name'];
				if(empty($vod['nns_image']))
				{
					if(!empty($result_vod['nns_image2']))
					{
						$result_vod['nns_image'] = $result['nns_image2'];
					}
				}
			}
			$result_vod['nns_index'] = 	$vod['nns_index'];
			$vod_id = $vod['nns_vod_id'];
			$result_vod['nns_task_id'] = $c2_task_info['nns_id'];
			$result_vod['nns_task_name'] = $result_vod['nns_name'];
			$result_vod['nns_id'] = $c2_task_info['nns_ref_id'];
			$result_vod['nns_image'] = $vod['nns_image'];//分集图片
			$result_vod['nns_index_show_time'] = $vod['nns_create_time'];
			//self::__check_video($sp_id, $vod['nns_vod_id']);
			if($c2_task_info['nns_action'] == 'add')
			{
				if($sp_id == 'cntv_me')//爱上分集不需要注入CDN
				{
					DEBUG && i_write_log_core('--------注入的是添加=>爱上分集(不需要注入CDN)---------',$sp_id.'/c2');
					nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0,nns_modify_time=now(),nns_epg_status=97 where nns_id='{$c2_task_info['nns_id']}'", $db_w);
					include_once 'queue_task_model.php';
					$queue_task_model = new queue_task_model();
					$queue_task_model->q_report_task($c2_task_info['nns_id']);
					$queue_task_model = null;
					return true;
				}
				$rs = c2_task_model::add_vod($result_vod,$sp_id);
			}
			elseif($c2_task_info['nns_action'] == 'modify')
			{
				if($sp_id == 'cntv_me')
				{
					if($is_movie)
					{//爱上电影分集操作是修改，需要片源重新注入CDN
						DEBUG && i_write_log_core('--------注入的是修改=>爱上电影分集(不需要注入CDN)---------',$sp_id.'/c2');
						nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=0,nns_modify_time=now(),nns_epg_status=97 where nns_id='{$c2_task_info['nns_id']}'", $db_w);
						include_once 'queue_task_model.php';
						$queue_task_model = new queue_task_model();
						$queue_task_model->q_report_task($c2_task_info['nns_id']);
						$queue_task_model = null;
						return true;
					}
					//爱上连续剧分集操作是修改，需要片源重新注入CDN
					$rs = self::__push_modify_vod_info($c2_task_info,$is_movie);
				}
				else 
				{	
					$rs = c2_task_model::update_vod($result_vod,$sp_id);
				}
			}
			if((int)$sp_config['cdn_send_mode'] === 0)
			{
				$nns_status = '';
				if($rs && $sp_id!='jsyx')
				{
					$nns_status = 'nns_status=5,';
				}
				elseif (!$rs && $sp_id!='jsyx')
				{
					$nns_status = 'nns_status=-1,';
				}
				nl_execute_by_db("update nns_mgtvbk_c2_task set $nns_status nns_modify_time=now() where nns_id='{$c2_task_info['nns_id']}'", $db_w);
			}
			elseif ((int)$sp_config['cdn_send_mode'] === 1)//HTTP 注入失败 标记任务失败
			{
				if(!$rs)
				{
					$nns_status = 'nns_status=-1,';
					nl_execute_by_db("update nns_mgtvbk_c2_task set $nns_status nns_modify_time=now() where nns_id='{$c2_task_info['nns_id']}'", $db_w);
				}
			}
		}
		return true;
	}

	/*添加切片任务*/
	static public function add_task_by_id($nns_id){
		$db = nn_get_db(NL_DB_READ);	
		$sql="select * from nns_mgtvbk_c2_task where nns_id='$nns_id'";
		$re= nl_db_get_one($sql, $db);unset($sql);
		$nnd_index_id = $re['nns_ref_id'];
		$sql = "select * from nns_vod_index where nns_id='$nnd_index_id'";
		$re_index= nl_db_get_one($sql, $db);unset($sql);
		$task_info = array(
			'nns_task_type'=>'std',
			'nns_org_id'=>$re['nns_org_id'],
			'nns_video_id'=>$re['nns_src_id'],
			'nns_video_index_id'=>$re_index['nns_id'],
			'nns_video_type'=>0,
			'nns_index'=>$re_index['nns_index'],
			'nns_create_time'=>date('Y-m-d H:i:s')
		);
		return clip_task_model::add_task($task_info);
	}

	/*取消切片任务*/
	static public function stop_task_by_id($nns_id){
		$db_w = nn_get_db(NL_DB_WRITE);
		$db = nn_get_db(NL_DB_READ);
		$sql="select * from nns_mgtvbk_c2_task where nns_id='$nns_id'";
		$re= nl_db_get_one($sql, $db);unset($sql);
		$nnd_index_id = $re['nns_ref_id'];
		$sql ="update nns_mgtvbk_clip_task set nns_state='cancel' where nns_org_id='{$re['nns_org_id']}' and nns_video_id='{$re['nns_src_id']}' 
		and nns_video_index_id='{$nnd_index_id}' and  nns_video_type=0";
		return nl_execute_by_db($sql, $db_w);
	}

	static public function rsync($nns_id){
		$db_w = nn_get_db(NL_DB_WRITE);
		$db = nn_get_db(NL_DB_READ);
		$sql="select * from nns_mgtvbk_c2_task where nns_id='$nns_id'";
		$re= nl_db_get_one($sql, $db);unset($sql);

		$sql = "select nns_id,nns_status,nns_action from nns_mgtvbk_c2_task where nns_type='vod' and nns_ref_id='{$re['nns_src_id']}' ".
			"and nns_org_id='{$re['nns_org_id']}'";

		$id = nl_db_get_one($sql, $db);unset($sql);
		$status = 0;
		$status2 = 0;
		if($id['nns_action']=='add'){
			$status = 1;
		}elseif($id['nns_action']=='modify'){
			$status = 2;
		}elseif($id['nns_action']=='destroy'){
			$status = 3;
		}

		if($re['nns_action']=='add'){
			$status2 = 1;
		}elseif($re['nns_action']=='modify'){
			$status2 = 2;
		}elseif($re['nns_action']=='destroy'){
			$status2 = 3;
		}

		nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status='$status' where nns_id='{$id['nns_id']}'", $db_w);
		nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status='$status2' where nns_id='{$re['nns_id']}'", $db_w);

		content_task_model::vod($re['nns_id']);
		return content_task_model::vod($id['nns_id']);
	}

	/**
	 * 删除单个分集
	 * @param nns_id (nns_mgtvbk_c2_task)
	 */
	static public function delete_task_vod_index($nns_id){	
		$db = nn_get_db(NL_DB_READ);
		$db_w = nn_get_db(NL_DB_WRITE);
		
		//取消切片任务
		//self::stop_task_by_id($nns_id);
		$sql="select * from nns_mgtvbk_c2_task where  nns_type='index' and nns_id='$nns_id'";
		$re= nl_db_get_one($sql, $db);unset($sql);
		if($re==false) return false;
		$nns_index_id = $re['nns_ref_id'];

		$sql ="update nns_mgtvbk_clip_task set nns_state='cancel' where nns_org_id='{$re['nns_org_id']}' and nns_video_id='{$re['nns_src_id']}' 
		and nns_video_index_id='{$nns_index_id}' and  nns_video_type=0";
		return nl_execute_by_db($sql, $db_w);
		
		/*****************删除媒体
		$sql = "select * from nns_vod_index where nns_id='$nns_index_id'";
		$vod_index = nl_db_get_one($sql,$db);		
		
		$sql = "select nns_id from nns_vod_media where nns_vod_index_id='$nns_index_id'";
		$vod_medias = nl_db_get_all($sql,$db);
		//删除媒体
		$media_ids = array();
		foreach($vod_medias as $item){
			$media_ids[] = $item['nns_id'];
		}
		c2_task_model::delete_movies(array('nns_ids'=>$media_ids,'nns_task_id'=>$vod_index['nns_id'],'nns_task_name'=>$vod_index['nns_name']));	
		***************/
		
		//删除分集
		$sql = "update nns_mgtvbk_c2_task set nns_action='destroy', nns_status=3 where nns_type='index' and nns_id='$nns_id'";
		nl_execute_by_db($sql,$db_w);
		return true;
	}

	/**
	 * 删除整个影片
	 * 1)取消切片任务
	 * 2)删除媒体
	 * 3)删除所有分集
	 * 4)删除影片
	 * @param array(
	 * nns_id :(nns_mgtvbk_c2_task)
	 * 
	 * )
	 */
	static public function delete_task_vod($nns_id,$nns_type){		
		$db = nn_get_db(NL_DB_READ);
		$db_w = nn_get_db(NL_DB_WRITE);	
		$sql = "select * from nns_mgtvbk_c2_task where nns_type='$nns_type' and  nns_id='".$nns_id."'";
		$result = nl_db_get_one($sql,$db);
		if($result==false) return false;
		if($nns_type=='vod'){
			//取消切片任务
			$sql ="update nns_mgtvbk_clip_task set nns_state='cancel' where nns_org_id='".$result['nns_org_id']."' and nns_video_id='".$result['nns_ref_id']."'  and  nns_video_type=0";
			nl_execute_by_db($sql, $db_w);
			/**************删除媒体
			$sql = "select * from nns_vod_index where nns_vod_id='".$result['nns_ref_id']."'";
			$index_all = nl_db_get_all($sql,$db);
			foreach($index_all as $index_item){
				$sql = "select nns_id from nns_vod_media where nns_vod_index_id='".$index_item['nns_id']."'";
				$vod_medias = nl_db_get_all($sql,$db);
				//删除媒体
				$media_ids = array();
				foreach($vod_medias as $item){
					$media_ids[] = $item['nns_id'];
				}
				c2_task_model::delete_movies(array('nns_ids'=>$media_ids,'nns_task_id'=>$index_item['nns_id'],'nns_task_name'=>$index_item['nns_name']));
			}		
			************/
			//删除所有分集
			$sql = "update nns_mgtvbk_c2_task set nns_action='destroy', nns_status=3 where nns_type='index' and nns_src_id='".$result['nns_ref_id']."' and nns_org_id='".$result['nns_org_id']."'";
			nl_execute_by_db($sql,$db_w);
			
			//删除影片基本信息
			$sql = "update nns_mgtvbk_c2_task set nns_action='destroy', nns_status=3 where nns_id='".$nns_id."'";
			nl_execute_by_db($sql,$db_w);
		}elseif($nns_type=='live'){
			//删除媒体数据
			$sql = "select * from nns_live_media where nns_live_id='".$result['nns_ref_id']."'";
			$live_all = nl_db_get_all($sql,$db);
			foreach ($live_all as  $value) {
				c2_task_model::delete_live_media(array(
					'nns_id'=>$value['nns_id'],
					'nns_task_name'=>$result['nns_name'].'-直播流',
				),'DELETE');
			}
			
			//删除节目单
			$sql = "select * from nns_mgtvbk_c2_task where nns_type='playbill' and  nns_org_id='".$result['nns_org_id']."' and nns_src_id='".$result['nns_ref_id']."'";
			$playbill_all = nl_db_get_all($sql,$db);
			if(count($playbill_all)>0){
			foreach ($live_all as  $value) {
				c2_task_model::delete_playbill(array(
					'nns_id'=>$value['nns_ref_id'],
					'nns_task_name'=>$result['nns_name'],
				));
				$sql = "update nns_mgtvbk_c2_task set nns_action='destroy', nns_status=3 where nns_type='playbill' and nns_src_id='".$result['nns_ref_id']."' and nns_org_id='".$result['nns_org_id']."'";
					nl_execute_by_db($sql,$db_w);
			}
			}

			//删除直播
			c2_task_model::delete_live(array(
				'nns_id'=>$result['nns_ref_id'],
				'nns_task_name'=>$result['nns_name']
			));

			//删除影片基本信息
			$sql = "update nns_mgtvbk_c2_task set nns_action='destroy', nns_status=0 where nns_id='".$nns_id."'";
			nl_execute_by_db($sql,$db_w);
		}

		return true; 
	}

	static public function get_vod_name($vod_id){
		$db = nn_get_db(NL_DB_READ);
		return nl_db_get_col("select nns_name from nns_vod where nns_id='".$vod_id."'",$db);
	}

	static public function get_vod_index_name($vod_index_id){
		$db = nn_get_db(NL_DB_READ);
		return nl_db_get_col("select nns_name from nns_vod_index where nns_id='".$vod_index_id."'",$db);
	}

	static public function update_c2_task_name($nns_id,$nns_name){
		$db = nn_get_db(NL_DB_WRITE);
		return nl_execute_by_db("update nns_mgtvbk_c2_task set nns_name='".$nns_name."' where nns_id='".$nns_id."'", $db);
	}

	/**
	 * @desc  界面重发命令 
	 * @param $query = array('c2_task_id' => $_GET['c2_task_id'], 'action' => $_GET['action'], 'status' => $_GET['status'], );
	 */
	static public function resend_c2_task($query){
		set_time_limit(0);
		$nns_id = $query['c2_task_id'];
		$db_w = nn_get_db(NL_DB_WRITE);
		$db = nn_get_db(NL_DB_READ);
		$sql = "select * from nns_mgtvbk_c2_task where nns_id='".$nns_id."'";
		$c2_task_info = nl_db_get_one($sql,$db);
		if($c2_task_info == false)return false;
		if($c2_task_info['nns_status']==7||$c2_task_info['nns_epg_status']==100) return false;

		include_once dirname(dirname(dirname(__FILE__))).'/nn_cms_db/nns_log/nns_db_op_log_class.php';

		$status = $query['status'];
		$action = $query['action'];
		$sp_id = $c2_task_info['nns_org_id'];

		$log = new nns_db_op_log_class();
		$op_desc = "注入C2 SP:".$sp_id.'   :数据:'.htmlspecialchars(var_export($c2_task_info,true),ENT_QUOTES);
		$org_type = 0;
		$org_id = $_SESSION['nns_org_id'];
		$log->nns_db_op_log_add($_SESSION['nns_mgr_id'], $action, $op_desc, $org_type, $org_id);

		nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=1,nns_action='".$action."',nns_epg_status=97 where nns_id='".$nns_id."'",$db_w);

		content_task_model::vod($nns_id,null,$sp_id);
		return true;
	}
	
	/**
	 * 后台操作上下线
	 * @param unknown $dc
	 * @param unknown $c2_task_id
	 * @param unknown $sp_config
	 * @param unknown $sp_id
	 */
	static public function resend_c2_task_line($dc,$c2_task_id,$sp_config,$sp_id)
	{
	    set_time_limit(0);
	    include_once dirname(dirname(dirname(__FILE__))).'/nn_cms_db/nns_log/nns_db_op_log_class.php';
    	if(strlen($c2_task_id)<1)
    	{
    	    return true;
    	}
	    $log = new nns_db_op_log_class();
	    $log->nns_db_op_log_add($_SESSION['nns_mgr_id'], 'line', "注入C2 SP:".$sp_id.'   :数据task_id:'.$c2_task_id, 0, $_SESSION['nns_org_id']);
	    content_task_model::line_vod($dc,$sp_config,$sp_id,'media',$c2_task_id);
	    return true;
	}
	
	/**
	 * 后台操作发布流程
	 * @param unknown $dc
	 * @param unknown $c2_task_id
	 * @param unknown $sp_config
	 * @param unknown $sp_id
	 */
	static public function resend_c2_task_delivery($dc,$c2_task_id,$sp_config,$sp_id)
	{
	    set_time_limit(0);
	    include_once dirname(dirname(dirname(__FILE__))).'/nn_cms_db/nns_log/nns_db_op_log_class.php';
	    if(strlen($c2_task_id)<1)
	    {
	        return true;
	    }
	    $log = new nns_db_op_log_class();
	    $log->nns_db_op_log_add($_SESSION['nns_mgr_id'], 'delivery', "注入C2 SP:".$sp_id.'   :数据task_id:'.$c2_task_id, 0, $_SESSION['nns_org_id']);
	    content_task_model::delivery_vod($dc,$sp_config,$sp_id,'media',$c2_task_id);
	    return true;
	}

	/**
	 * 后台操作获取URL 流程
	 * @param unknown $dc
	 * @param unknown $c2_task_id
	 * @param unknown $sp_config
	 * @param unknown $sp_id
	 */
	static public function resend_c2_task_playurl($dc,$c2_task_id,$sp_config,$sp_id)
	{
	    set_time_limit(0);
	    include_once dirname(dirname(dirname(__FILE__))).'/nn_cms_db/nns_log/nns_db_op_log_class.php';
	    if(strlen($c2_task_id)<1)
	    {
	        return true;
	    }
	    $log = new nns_db_op_log_class();
	    $log->nns_db_op_log_add($_SESSION['nns_mgr_id'], 'playurl', "注入C2 SP:".$sp_id.'   :数据task_id:'.$c2_task_id, 0, $_SESSION['nns_org_id']);
	    content_task_model::playurl_vod($dc,$sp_config,$sp_id,'media',$c2_task_id);
	    return true;
	}
	
	/**
	 * 重发多个任务
	 * @param $action string 
	 * @param $ids string
	 */
	static public function resend_c2_task_all($action,$ids){
		set_time_limit(0);
		$status = SP_C2_WAIT;
		$arr = explode(',', $ids);
		foreach ($arr as  $value) {
			$all_list = array('c2_task_id'=>$value,'status'=>$status,'action'=>$action);
			self::resend_c2_task($all_list);
		}
		return true;
	}

	/**
	 * 获取任务列表
	 * @param $sp_id string
	 * @param $filter  array
	 */
	static public function get_vod_task_list($sp_id,$filter=null,$start=0,$size=100){
		set_time_limit(0);	
		$db = nn_get_db(NL_DB_READ);
		$str_left_join = '';
		
		$sql = "select task.* from nns_mgtvbk_c2_task as task {$str_left_join} where task.nns_org_id='$sp_id'";
		if($filter != null){
			$wh_arr = array();
			if(!empty($filter['nns_name'])){
			    if(strpos($filter['nns_name'], "|#|,") !== false)
			    {
			        $arr = explode("|#|,", $filter['nns_name']);
			        $str_temp='';
			        foreach ($arr as $arr_temp )
			        {
			           $str_temp.= " task.nns_name like '%".$arr_temp."%' or";
			        }
			        $str_temp = rtrim(trim($str_temp),'or');
			        $wh_arr[] = " ({$str_temp}) ";
			    }
			    else {
				    $wh_arr[] = " task.nns_name like '%".$filter['nns_name']."%'";
			    }
			}
			if(!empty($filter['day_picker_start'])){
				$wh_arr[] = " task.nns_create_time>='".$filter['day_picker_start']."'";
			}
			if(!empty($filter['day_picker_end'])){
				$wh_arr[] = " task.nns_create_time<='".$filter['day_picker_end']."'";
			}
			if(!empty($filter['nns_type'])){
				$wh_arr[] = " task.nns_type='".$filter['nns_type']."'";
			}
			if(!empty($filter['nns_action'])){
				$wh_arr[] = " task.nns_action='".$filter['nns_action']."'";
			}
			if(strlen($filter['nns_cdn_fail_time'])){
				$wh_arr[] = " task.nns_cdn_fail_time='".$filter['nns_cdn_fail_time']."'";
			}
            if(strlen($filter['nns_ref_id'])){
                $wh_arr[] = " task.nns_ref_id='".$filter['nns_ref_id']."'";
            }
            if(strlen($filter['nns_src_id'])){
                $wh_arr[] = " task.nns_src_id='".$filter['nns_src_id']."'";
            }
			if(!empty($filter['nns_state'])){
				$state = $filter['nns_state'];
				switch($state){
					case 'content_wait':
					$wh_arr[] = " task.nns_status=1";
					break;
					case 'content_ok':
					$wh_arr[] = " task.nns_status=0";
					break;
					case 'content_fail':
					$wh_arr[] = " task.nns_status=-1";
					break;
					case 'content_doing':
					$wh_arr[] = " task.nns_status=5";
					break;
					case 'content_wait_cdi':
					$wh_arr[] = " task.nns_status=6";
					break;
					case 'content_cancel':
					$wh_arr[] = " task.nns_status=7";
					break;
					case 'content_wait_cdi_doing':
					$wh_arr[] = " task.nns_status=8";
					break;
					case 'content_wait_cdi_fail':
					$wh_arr[] = " task.nns_status=9";
					break;
					case 'ftp_connet_fail':
						$wh_arr[] = " task.nns_status=10";
						break;
					case 'ftp_clip_get_fail':
						$wh_arr[] = " task.nns_status=11";
						break;
					case 'content_wait_del':
					$wh_arr[] = " task.nns_status=12";
					break;
					case 'content_doing_del':
						$wh_arr[] = " task.nns_status=13";
						break;
					case 'content_fail_del':
						$wh_arr[] = " task.nns_status=14";
						break;
				}
			}

			if(!empty($filter['nns_epg_state'])){
				$epg_state = $filter['nns_epg_state'];
				switch($epg_state){
					case 'content_epg_wait':
					$wh_arr[] = " task.nns_epg_status=97";
					break;
					case 'content_epg_ok':
					$wh_arr[] = " task.nns_epg_status=99";
					break;
					case 'content_epg_doing':
					$wh_arr[] = " task.nns_epg_status=98";
					break;
					case 'content_epg_cancel':
					$wh_arr[] = " task.nns_epg_status=100";
					break;
                    case 'content_epg_fail':
                        $wh_arr[] = " task.nns_epg_status=96";
                    break;
				}
			}
			if(isset($filter['nns_cp_id']) && strlen($filter['nns_cp_id']) >0){
			    $wh_arr[] = " task.nns_cp_id='{$filter['nns_cp_id']}'";
			}
			if(!empty($wh_arr)) $sql .= " and ".implode(" and ",$wh_arr);
		}
		$count_sql = $sql;
		$count_sql = str_replace('task.*',' count(*) as temp_count ',$count_sql);
		if(empty($filter) || !is_array($filter))
		{
		   $sql .= " order by task.nns_create_time desc ";
		}
		$sql .= " limit $start,$size";
		$data = nl_db_get_all($sql,$db);
		$rows = nl_db_get_col($count_sql,$db);
		return array('data'=>$data,'rows'=>$rows);
	}

    /**
     * 获取直播任务列表
     * @param $sp_id string
     * @param $filter  array
     */
    static public function get_live_task_list($sp_id,$filter=null,$start=0,$size=100)
    {
        set_time_limit(0);
        $db = nn_get_db(NL_DB_READ);
        $str_left_join = '';

        $sql = "select task.* from nns_mgtvbk_c2_live_task as task {$str_left_join} where task.nns_org_id='$sp_id'";
        if($filter != null){
            $wh_arr = array();
            if(!empty($filter['nns_name'])){
                if(strpos($filter['nns_name'], "|#|,") !== false)
                {
                    $arr = explode("|#|,", $filter['nns_name']);
                    $str_temp='';
                    foreach ($arr as $arr_temp )
                    {
                        $str_temp.= " task.nns_name like '%".$arr_temp."%' or";
                    }
                    $str_temp = rtrim(trim($str_temp),'or');
                    $wh_arr[] = " ({$str_temp}) ";
                }
                else {
                    $wh_arr[] = " task.nns_name like '%".$filter['nns_name']."%'";
                }
            }
            if(!empty($filter['day_picker_start'])){
                $wh_arr[] = " task.nns_create_time>='".$filter['day_picker_start']."'";
            }
            if(!empty($filter['day_picker_end'])){
                $wh_arr[] = " task.nns_create_time<='".$filter['day_picker_end']."'";
            }
            if(!empty($filter['nns_type'])){
                $wh_arr[] = " task.nns_type='".$filter['nns_type']."'";
            }
            if(!empty($filter['nns_action'])){
                $wh_arr[] = " task.nns_action='".$filter['nns_action']."'";
            }
            if(strlen($filter['nns_cdn_fail_time'])){
                $wh_arr[] = " task.nns_cdn_fail_time='".$filter['nns_cdn_fail_time']."'";
            }
            if(strlen($filter['nns_ref_id'])){
                $wh_arr[] = " task.nns_ref_id='".$filter['nns_ref_id']."'";
            }
            if(strlen($filter['nns_src_id'])){
                $wh_arr[] = " task.nns_src_id='".$filter['nns_src_id']."'";
            }
            if(!empty($filter['nns_state'])){
                $state = $filter['nns_state'];
                switch($state){
                    case 'content_wait':
                        $wh_arr[] = " task.nns_status=1";
                        break;
                    case 'content_ok':
                        $wh_arr[] = " task.nns_status=0";
                        break;
                    case 'content_fail':
                        $wh_arr[] = " task.nns_status=-1";
                        break;
                    case 'content_doing':
                        $wh_arr[] = " task.nns_status=5";
                        break;
                    case 'content_wait_cdi':
                        $wh_arr[] = " task.nns_status=6";
                        break;
                    case 'content_cancel':
                        $wh_arr[] = " task.nns_status=7";
                        break;
                    case 'content_wait_cdi_doing':
                        $wh_arr[] = " task.nns_status=8";
                        break;
                    case 'content_wait_cdi_fail':
                        $wh_arr[] = " task.nns_status=9";
                        break;
                    case 'ftp_connet_fail':
                        $wh_arr[] = " task.nns_status=10";
                        break;
                    case 'ftp_clip_get_fail':
                        $wh_arr[] = " task.nns_status=11";
                        break;
                    case 'content_wait_del':
                        $wh_arr[] = " task.nns_status=12";
                        break;
                    case 'content_doing_del':
                        $wh_arr[] = " task.nns_status=13";
                        break;
                    case 'content_fail_del':
                        $wh_arr[] = " task.nns_status=14";
                        break;
                }
            }

            if(!empty($filter['nns_epg_state'])){
                $epg_state = $filter['nns_epg_state'];
                switch($epg_state){
                    case 'content_epg_wait':
                        $wh_arr[] = " task.nns_epg_status=97";
                        break;
                    case 'content_epg_ok':
                        $wh_arr[] = " task.nns_epg_status=99";
                        break;
                    case 'content_epg_doing':
                        $wh_arr[] = " task.nns_epg_status=98";
                        break;
                    case 'content_epg_cancel':
                        $wh_arr[] = " task.nns_epg_status=100";
                        break;
                    case 'content_epg_fail':
                        $wh_arr[] = " task.nns_epg_status=96";
                        break;
                }
            }
            if(isset($filter['nns_cp_id']) && strlen($filter['nns_cp_id']) >0){
                $wh_arr[] = " task.nns_cp_id='{$filter['nns_cp_id']}'";
            }
            if(!empty($wh_arr)) $sql .= " and ".implode(" and ",$wh_arr);
        }
        $count_sql = $sql;
        $count_sql = str_replace('task.*',' count(*) as temp_count ',$count_sql);
        if(empty($filter) || !is_array($filter))
        {
            $sql .= " order by task.nns_create_time desc ";
        }
        $sql .= " limit $start,$size";
        $data = nl_db_get_all($sql,$db);
        $rows = nl_db_get_col($count_sql,$db);
        return array('data'=>$data,'rows'=>$rows);
    }


	static public function __check_cdn_doing_num($sp_id,$type){
		$db = nn_get_db(NL_DB_READ);
		$sp_config = sp_model::get_sp_config($sp_id);
		$max_num = 100;
		if(isset($sp_config['cdn_'.$type.'_max'])&&(int)$sp_config['cdn_'.$type.'_max'] > 0){
			$max_num = $sp_config['cdn_'.$type.'_max'];
		}
		$sql = null;
		switch ($type) {
			case 'video':
				$sql = "select count(nns_id) as num from nns_mgtvbk_c2_task where nns_org_id='$sp_id' and nns_type='video' and nns_status=5 ";
				break;
			case 'index':
				$sql = "select count(nns_id) as num from nns_mgtvbk_c2_task where nns_org_id='$sp_id' and nns_type='index' and   nns_status=5 ";
				break;
			case 'media':
				$sql = "select count(nns_id) as num from nns_mgtvbk_c2_task where nns_org_id='$sp_id' and nns_type='media'  and nns_status=5 ";
				break;
            case 'playbill':
                $sql = "select count(nns_id) as num from nns_mgtvbk_c2_task where nns_org_id='$sp_id' and nns_type='playbill'  and nns_status=5 ";
                break;
		}
		$num = nl_db_get_col($sql, $db);

		if($num>=$max_num){
			return false;
		}
		return true;
	}

	static public function __check_video($sp_id, $vod_id){
		if($sp_id=='sihua'){
			return ;
		}
		$db = nn_get_db(NL_DB_WRITE);
		$sql = "select * from nns_mgtvbk_c2_task where nns_org_id='$sp_id' and nns_type='video' and nns_ref_id='$vod_id'";
		$info = nl_db_get_one($sql, $db);
		//echo $sql;die;
		if(empty($info)|| $info===false || $info ===null){
			$info_video = video_model::get_vod_info($vod_id);
			//没有数据
			$id = np_guid_rand();
			$data = array(
				'nns_id'=>$id,
				'nns_type'=>'video',
				'nns_name'=>$info_video['nns_name'],
				'nns_ref_id'=>$vod_id,
				'nns_action'=>'add',
				'nns_status'=>1,
				'nns_epg_status'=>97,
				'nns_create_time'=>date('Y-m-d H:i:s'),
				'nns_org_id'=>$sp_id,
			);
			if(!empty($vod_id)){
				nl_db_insert($db, 'nns_mgtvbk_c2_task', $data);
				if($sp_id!='sihua'){
					self::vod($id,null,$sp_id);
				}
			}
			
		}else{
			if((int)$info['nns_status']==1){
				if($sp_id!='sihua'){
					self::vod($info['nns_id'],null,$sp_id);
				}
			}
		}

		return true;
	}

	static public function __check_video_index($sp_id, $vod_id){
		if($sp_id=='sihua'){
			return ;
		}
		$db = nn_get_db(NL_DB_WRITE);
		$sql = "select * from nns_mgtvbk_c2_task where nns_org_id='$sp_id' and nns_type='index' and nns_ref_id='$vod_id'";
		//echo $sql;die;
		$info = nl_db_get_one($sql, $db);
		if(empty($info)|| $info===false || $info ===null){
			$info_video = video_model::get_vod_index_info($vod_id);
			//没有数据
			$id = np_guid_rand();
			$data = array(
				'nns_id'=>$id,
				'nns_type'=>'index',
				'nns_name'=>$info_video['nns_name'],
				'nns_ref_id'=>$vod_id,
				'nns_action'=>'add',
				'nns_status'=>1,
				'nns_epg_status'=>97,
				'nns_create_time'=>date('Y-m-d H:i:s'),
				'nns_org_id'=>$sp_id,
				'nns_src_id'=>$info_video['nns_vod_id'],
			);
			if(!empty($vod_id)){
				nl_db_insert($db, 'nns_mgtvbk_c2_task', $data);
				if($sp_id=='sihua'){
					self::vod($id,null,$sp_id);
				}
			}
			self::vod($id,null,$sp_id);
		}else{
			if((int)$info['nns_status']==1){
				if($sp_id=='sihua'){
					self::vod($info['nns_id'],null,$sp_id);
				}
			}elseif($info['nns_status']==0&&$info['nns_action']=='destroy'){
				$sql_update = "update nns_mgtvbk_c2_task set nns_status=1,nns_action='add',nns_epg_status=97 where nns_id='{$info['nns_id']}'";
				nl_execute_by_db($sql_update, $db);
				self::vod($info['nns_id'],null,$sp_id);
			}
		}

		return true;
	}
	/**
	 * 重新添加队列，队列添加至中心同步指令
	 * @param unknown $ids
	 * @return boolean
	 */
	static public function add_queue($ids){
		set_time_limit(0);
		$arr = explode(',', $ids);
		include_once 'queue_task_model.php';
		$queue_task_model = new queue_task_model();
		$db = nn_get_db(NL_DB_WRITE);
		foreach ($arr as $id) {
			if($id){
				$sql="select * from nns_mgtvbk_c2_task where nns_id='$id'";
				$info = nl_db_get_one($sql, $db);
				nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=7,nns_epg_status=100 where nns_id='$id'", $db);
				$queue_task_model->q_report_task($id);
				$queue_task_model->q_add_task($info['nns_ref_id'],$info['nns_type'] , $info['nns_action'],$info['nns_org_id'],null,true,$info['nns_message_id']);
			}
		}
		return true;
	}

	/**
	 * @异步请求cdn播放串
	 * @sp_id
	 * @c2_task_id
	 * return true or false
	 * 
	 */
	static public function do_query_media_url($sp_id,$c2_task_id=null){
		$db = nn_get_db(NL_DB_WRITE);
		$sp_config = sp_model::get_sp_config($sp_id);
		$cdn_media_max = $sp_config['cdn_media_max'];
		//查询还可以发送多少指令到C2
		$do_count = nl_db_get_col("select count(nns_id) as num from nns_mgtvbk_c2_task where nns_org_id='{$sp_id}' and nns_type='media' and nns_action!='destroy' and nns_status='8'", $db);
		$execute_num = $cdn_media_max-$do_count;
		//已经最大
		if($execute_num<=0) {
			DEBUG && i_write_log_core('============= c2 queue full ===============',$sp_id.'/do_query_media_url/');
			return false;
		}
		$where = "";
		if($c2_task_id){
			$where = "nns_id='{$c2_task_id}' and ";
		}		
		$sql = "select * from nns_mgtvbk_c2_task where $where nns_org_id='{$sp_id}' and nns_type='media' and nns_action!='destroy' and nns_status='6' order by nns_modify_time asc limit $execute_num";		
		$infos = nl_db_get_all($sql, $db);
		if(count($infos)==0) {
			DEBUG && i_write_log_core('============= no task ===============',$sp_id.'/do_query_media_url/');	
			return false;
		}
		foreach ($infos as $info) {
			$re = c2_task_model::do_content_query($info, $sp_id);
			if($re){
				nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status='8',nns_modify_time=now() where nns_id='".$info['nns_id']."'", $db);
			}else{
				DEBUG && i_write_log_core('============= c2 execute fail ==============='.var_export($info,true),$sp_id.'/do_query_media_url/');
			}
		}
		return true;
	}

	/**
	 * 爱上专用，当操作为修改时，主媒资、分集走此方法
	 * @param $val c2数据
	 * @param $is_movie bool true 电影 false连续剧
	 */	
	static public function __push_modify_vod_info($val,$is_movie)
	{
		set_time_limit(0);
		$db = nn_get_db(NL_DB_READ);
		$db_w = nn_get_db(NL_DB_WRITE);
		$sp_id = $val['nns_org_id'];
		if($val['nns_type'] == 'video' && $is_movie)
		{//电影，修改program
			$vod_index_sql = "select nns_id from nns_vod_index where nns_vod_id='{$val['nns_ref_id']}' and nns_deleted!=1 and nns_index=0";
			$vod_index_info = nl_db_get_one($vod_index_sql,$db);unset($vod_index_sql);
			$vod_media_sql = "select nns_id from nns_vod_media where nns_deleted != 1 and nns_vod_id='{$val['nns_ref_id']}' order by nns_kbps desc limit 1";
			$vod_media_info = nl_db_get_one($vod_media_sql,$db);unset($vod_media_sql);
			if(!is_array($vod_index_info) || !is_array($vod_media_info))
			{
				return false;
			}
			$data = array(
				'nns_video_id' => $val['nns_ref_id'],
				'nns_video_index_id' => $vod_index_info['nns_id'],
				'nns_clip_task_id' => $val['nns_clip_task_id'],
				'nns_task_id' => $val['nns_id'],
				'nns_clip_date' => $val['nns_clip_date'],
				'nns_media_id' => $vod_media_info['nns_id'],
				'nns_date' => $val['nns_clip_date'],
				'nns_file_path' => $val['nns_file_path'],
				'nns_file_size' => $val['nns_file_size'],
				'nns_file_md5' => $val['nns_file_md5'],
				'nns_task_name' => $val['nns_name'],
				'nns_action' => $val['nns_action'],
			);
			$result = c2_task_model::update_vod($data,$sp_id);
		}
		elseif ($val['nns_type'] == 'index' && !$is_movie) 
		{//连续剧修改分集
			$vod_media_sql = "select nns_id from nns_vod_media where nns_deleted != 1 and nns_vod_id='{$val['nns_src_id']}' and nns_vod_index_id='{$val['nns_ref_id']}' order by nns_kbps desc limit 1";
			$vod_media_info = nl_db_get_one($vod_media_sql,$db);unset($vod_media_sql);
			if(!is_array($vod_media_info))
			{
				return false;
			}
			$data = array(
				'nns_video_id' => $val['nns_src_id'],
				'nns_video_index_id' => $val['nns_ref_id'],
				'nns_clip_task_id' => $val['nns_clip_task_id'],
				'nns_task_id' => $val['nns_id'],
				'nns_clip_date' => $val['nns_clip_date'],
				'nns_media_id' => $vod_media_info['nns_id'],
				'nns_date' => $val['nns_clip_date'],
				'nns_file_path' => $val['nns_file_path'],
				'nns_file_size' => $val['nns_file_size'],
				'nns_file_md5' => $val['nns_file_md5'],
				'nns_task_name' => $val['nns_name'],
				'nns_action' => $val['nns_action'],
			);
			$result = c2_task_model::update_vod($data,$sp_id);
		}
		else 
		{
			DEBUG && i_write_log_core('--------注入的修改操作既不是电影的主媒资，也不是连续剧的分集---------',$sp_id.'/c2');
			$result = false;
			
		}
//		if($result)
//		{
//			nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=5,nns_modify_time=now() where nns_id='{$val['nns_id']}'", $db_w);
//		}
		return $result;
	}
	/**
	 * 取消C2注入任务
	 * @date 2017-05-18
	 * @author zhiyong.luo
	 */
	static public function cancel_c2_queue($ids)
	{
		set_time_limit(0);
		$arr = explode(',', $ids);
		include_once 'queue_task_model.php';
		$queue_task_model = new queue_task_model();
		$db = nn_get_db(NL_DB_WRITE);
		foreach ($arr as $id) 
		{
			if($id)
			{
				$queue_task_model->cancel_c2_task($db, $id);
			}
		}
		return true;
	}
	/**
	 * 检测C2任务是否执行，不执行时进行上报
	 * @param object $db
	 * @param array $sp_config
	 * @param string $task_id
	 * @param string $video_type
	 * @param string $action
	 * @param $is_series
	 * @return boolean
	 */
	static public function check_import($db,$sp_config,$task_id,$video_type = 'video',$action = 'add',$is_series = true)
	{
		$c2_import = true;//注入CDN
		if($video_type != 'video' && $video_type != 'index' && $video_type != 'media')
		{
			return $c2_import;
		}
		$type = $video_type . "_" . $action;
		if(!isset($sp_config[$type]['cdn_import_model']) || empty($sp_config[$type]['cdn_import_model']))//都要注入
		{
			return $c2_import;
		}
		switch ($sp_config[$type]['cdn_import_model'])
		{
			case '1': //仅电影注入
				if($is_series) //连续剧
				{
					$c2_import = false;
				}
				break;
			case '2'://仅连续剧注入
				if(!$is_series) //电影
				{
					$c2_import = false;
				}
				break;
			case '3':
				$c2_import = false;
				break;
		}
		if(!$c2_import) //连续剧
		{
			$sql = "update nns_mgtvbk_c2_task set nns_status=0,nns_modify_time=now(),nns_epg_status=97 where nns_id='{$task_id}'";
			nl_execute_by_db($sql, $db);
			$queue_task_model = new queue_task_model();
			$queue_task_model->q_report_task($task_id);
			$queue_task_model = null;
		}
		return $c2_import;
	}
}
