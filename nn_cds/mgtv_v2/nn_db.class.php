<?php
include_once dirname(dirname(__FILE__)) . '/nn_logic/nl_common.func.php';
class nn_db 
{
    static $db = null;
    static $dc = null;
	
	public static function get_db($db_policy = NL_DB_READ) 
	{	
        if (!isset(self::$db[$db_policy]) || self::$db[$db_policy] == null) 
		{
			$db = nl_get_db($db_policy);
			$db->open(); 
            self::$db[$db_policy] = $db;
        }
        return self::$db[$db_policy];
    }
	public static function close_db($db)
	{
		self::$db = null;
		$db->close();
	}
	public static function get_dc($db_policy = NL_DB_READ)
	{
		if(!is_object(self::$dc))
		{
			self::$dc = nl_get_dc(array (
				"db_policy" => NL_DB_READ|NL_DB_WRITE,
				"cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
			));
		}
		return self::$dc;
	}
}
function nn_get_db($db_policy = NL_DB_READ) {
	//$dc = nl_get_dc(array (
	//		"db_policy" => NL_DB_READ|NL_DB_WRITE,
	//		"cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
	//));
	$dc = nn_db::get_dc($db_policy);
	return $dc->db();
//         return nn_db::get_db($db_policy);
}

function nn_close_db($db)
{
	return nn_db::close_db($db);
}
?>