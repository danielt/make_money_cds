<?php
include_once dirname(__FILE__).'/define.php';
function sihua_model_autoload($classname){
$file = dirname(__FILE__)."/models/".$classname.".php";
if(file_exists($file)){
include_once $file;
return true;
}
return false;
}
spl_autoload_register("sihua_model_autoload");
