<?php
/**
 * Created by PhpStorm.
 * User: Hushengs
 * Date: 2016/7/18
 * Time: 10:28
 */
header("Content-Type:text/html;charset=utf-8");
date_default_timezone_set("Asia/Hong_Kong");
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_init.php';
class timer_clear_message extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->do_timer_action();
        $this->msg('执行结束...');
    }
    public function do_timer_action()
    {
        $g_clear_message = get_config_v2('g_clear_message');
        $this->dc = nl_get_dc(array(
            "db_policy" => NL_DB_WRITE,
            "cache_policy" => NP_KV_CACHE_TYPE_NULL
        ));
        //清理message表
        foreach ($g_clear_message as $value)
		{
	        if(isset($value['enable']) && $value['enable']===0)
	        {
				$value['clear_day_before_now'] = empty($value['clear_day_before_now']) ? 180 : $value['clear_day_before_now'];
				$time = date('Y-m-d');
	            $sql = "DELETE FROM nns_mgtvbk_message WHERE nns_create_time <= DATE_ADD('{$time}',INTERVAL -{$value['clear_day_before_now']} day) ";
	            $where = '';
	            if(strlen($value['cp_id']) > 0)
	            {
	                $where .= " and nns_cp_id = '{$value['cp_id']}'";
	            }
	            if($value['clear_data_fail_or_suc'] == -1)
	            {
	                $where .= " and nns_message_state='4'";
	            }
	            if($value['clear_data_fail_or_suc'] == 0)
	            {
	                $where .= " and nns_message_state!='4' and nns_message_state!='3'";
	            }
	            if($value['clear_data_fail_or_suc'] == 1)
	            {
	                $where .= " and nns_message_state='3'";
	            }
	            $sql .= $where;
	            $result = nl_execute_by_db($sql,$this->dc->db());
	            if($result)
	            {
	                $this->msg('执行清理message成功'.$sql);
	            }
	            else
	            {
	                $this->msg('执行清理message失败'.$sql);
	            }
	        }
        }
    }
}
$timer_clear_log = new timer_clear_message("clear_message", 'public',__FILE__);
$timer_clear_log->run();

