<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_init.php';

include_once dirname(dirname(__FILE__)) . '/models/queue_task_model.php';
class get_queue_task extends nn_timer
{

	public function action($params = null)
	{
		$this->msg('开始执行...');
		do_timer_action();
		$this->msg('执行结束...');
	}
}
$get_queue_task = new get_queue_task("get_queue_task", 'public',__FILE__);
$get_queue_task->run();

function do_timer_action()
{
	$queue_task_model = new queue_task_model();
	$sp_list = sp_model::get_sp_list();
	if (is_array($sp_list) && $sp_list != null)
	{
		foreach ($sp_list as $sp_info)
		{
			$queue_task_model->q_get_task($sp_info['nns_id'], 200);
		}
	}
	$queue_task_model = null;
	$sp_list = null;
}
