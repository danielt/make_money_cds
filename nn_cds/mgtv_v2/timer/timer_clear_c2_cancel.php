<?php
/**
 * User: Hushengs
 * Date: 2016/7/31
 * Time: 21:10
 */
header("Content-Type:text/html;charset=utf-8");
date_default_timezone_set("Asia/Hong_Kong");
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_init.php';
class timer_clear_c2_cancel extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->do_timer_action();
        $this->msg('执行结束...');
    }
    public function do_timer_action()
    {
        $this->dc = nl_get_dc(array(
            "db_policy" => NL_DB_WRITE,
            "cache_policy" => NP_KV_CACHE_TYPE_NULL
        ));
        //清理c2EPG取消状态的数据
            $sql = "DELETE FROM nns_mgtvbk_c2_task WHERE nns_epg_status = 100";
            $result = nl_execute_by_db($sql,$this->dc->db());
            if($result)
            {
                $this->msg('执行清理c2_task取消状态任务成功'.$sql);
            }
            else
            {
                $this->msg('执行清理c2_task取消状态任务失败'.$sql);
            }
    }
}
$timer_clear_c2_cancel = new timer_clear_c2_cancel("clear_c2_cancel", 'public',__FILE__);
$timer_clear_c2_cancel->run();
