<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/op_queue/op_queue.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/c2_task/c2_task.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/api/twsx/twsx_sync_source.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_cms_db/nns_vod/nns_db_vod_media_class.php';
include_once dirname(dirname(dirname(__FILE__))) . "/nn_logic/sp/sp.class.php";

class check_overtime_queue extends nn_timer
{
	//是否删除播控上的资源内容
	private $is_delete_vod_media = true;
	//是否反馈上游
	private $is_notify = true;
	
	public function action($params = null)
	{
		$this->msg('开始执行...');
		$this->do_timer_action();
		$this->msg('执行结束...');
	}
	/**
	 * 脚本执行体
	 * @author zhiyong.luo
	 */
	private function do_timer_action()
	{
		$dc = nl_get_dc(array (
				"db_policy" => NL_DB_WRITE | NL_DB_READ,
				"cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
		));
		$dc->open();
		//获取SP
		$sp_all = nl_sp::query_all($dc);
		$sp_info = array();
		if($sp_all['ret'] == 0 && !empty($sp_all['data_info']) && is_array($sp_all['data_info']))
		{
			$sp_info = $sp_all['data_info'];
		}
		if (!is_array($sp_info))
		{
			echo "SP未配置\r\n";
			return ;
		}
		foreach ($sp_info as $sp_list)
		{
			$sp_config = isset($sp_list['nns_config']) && !empty($sp_list['nns_config']) ? json_decode($sp_list['nns_config'], true) : null;
			if (!isset($sp_config['message_import_overtime_enabled']) || (int)$sp_config['message_import_overtime_enabled'] === 0)
			{
				echo "SP:" . $sp_list['nns_id'] . "没有启用超时时限功能\r\n";
				continue;
			}
			$overtime = (int)$sp_config['message_import_overtime'];
			if($overtime <= 0)
			{
				echo "SP:" . $sp_list['nns_id'] . "没有配置超时时限的具体时间\r\n";
				continue;
			}
			//获取已超时队列
			$over_time = date('Y-m-d H:i:s',time()-$overtime);
			$params = array(
					'less_than' => array('nns_create_time' => $over_time),
					'nns_org_id' => $sp_list['nns_id'],
			);
			$overtime_queue = nl_op_queue::get_overtime_op_queue_by_condition($dc, $params);
			//获取已经超时的队列
			if($overtime_queue['ret'] != 0 || empty($overtime_queue['data_info']) || !is_array($overtime_queue['data_info']))
			{
				echo "SP没有超时队列";
				continue;
			}
			//取消C2队列任务，删除op_queue队列，删除资源库内容，反馈上游
			$queue_ids = array();
			$media_ids = array();
			foreach ($overtime_queue['data_info'] as $queue)
			{
				$queue_ids[] = $queue['nns_id'];
				if($queue['nns_type'] === 'media')
				{
					$media_ids[] = $queue['nns_media_id'];
				}
			}
			$c2_update_arr = array('in' => array('nns_op_id' => $queue_ids),'nns_org_id' => $sp_list['nns_id']);
			$c2_set_arr = array(
					'nns_epg_status' => 100,
					'nns_status' => 7
			);
			$c2_re = nl_c2_task::cancel_c2_task($dc,$c2_set_arr,$c2_update_arr);
			//$c2_re = array('ret' => 0);
			if($c2_re['ret'] != 0)
			{
				echo $c2_re['reason'];
				continue;
			}
			//删除op_queue队列
			$op_re = nl_op_queue::delete_op_queue($dc,$queue_ids);
			//$op_re = array('ret' => 0);
			if($op_re['ret'] != 0)
			{
				echo $op_re['reason'];
				continue;
			}
			//删除播控资源库内容
			if($this->is_delete_vod && !empty($media_ids))
			{
				$vod_media_inst = new nns_db_vod_media_class();
				foreach ($media_ids as $id)
				{
					$vod_media_inst->nns_db_vod_media_delete($id);
				}
			}
			if($this->is_notify)
			{
				$sync_source = new twsx_sync_source();
				foreach ($overtime_queue['data_info'] as $value)
				{
					$sync_source->async_feedback($value,false);				
				}
			}
		}
	}
}
$check_overtime_queue = new check_overtime_queue("check_overtime_queue", 'public',__FILE__);
$check_overtime_queue->run();