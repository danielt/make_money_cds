<?php
/**
 * 定时器慢查询日志日志
 * @author liangpan
 * @date 2016-03-06
 */
set_time_limit(0);
date_default_timezone_set("Asia/Chongqing");
include_once dirname(dirname(__FILE__)) . '/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/slow_log/slow_log.class.php';
define("GET_RESEND_DAY", 3);
class clear_slow extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        do_timer_action($this);
        $this->msg('执行结束...');
    }
}
$clear_slow = new clear_slow("clear_slow", "public",__FILE__);
$clear_slow->run();

function do_timer_action($obj)
{
    $dc = nl_get_dc(array (
        'db_policy' => NL_DB_WRITE,
        'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
    ));
    $int_resend_day = (defined('GET_RESEND_DAY') && GET_RESEND_DAY >0) ? GET_RESEND_DAY : 3;
    $str_date = date('Y-m-d', strtotime("-{$int_resend_day} days"));
    $result = nl_slow_log::del_date($dc, $str_date);
    $obj->msg(var_export($result,true));
    
}

