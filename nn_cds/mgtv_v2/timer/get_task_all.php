<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/message/message_group.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/message/nl_message.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/message/message_group_list.class.php';
class timer_execute extends nn_timer
{
	public function action($params = null)
	{
		$this->msg('开始执行...');
		$this->init($params);
		$this->msg('执行结束...');
	}
	
	public function init($params = null)
	{
	    if($this->obj_dc === null)
	    {
	        $this->obj_dc = nl_get_dc(array (
	            "db_policy" => NL_DB_WRITE | NL_DB_READ,
	            "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
	        ));
	    }
	    if(isset($params["bk_queue_value"]) && strlen($params["bk_queue_value"]) >0)
	    {
	        $result_sp = $this->_get_sp_info($params["bk_queue_value"]);
	        if($result_sp['ret'] !=0)
	        {
	            $this->msg("sp信息获取spID[{$params["bk_queue_value"]}]失败：".var_export($result_sp,true));
	            return ;
	        }
	    }
	    else
	    {
	        $result_sp = $this->_get_all_sp_info();
	        if($result_sp['ret'] !=0)
	        {
	            $this->msg("sp信息获取所有失败：".var_export($result_sp,true));
	            return ;
	        }
	    }
	    if (is_array($this->arr_sp_config) && !empty($this->arr_sp_config))
	    {
	        $queue_task_model = new queue_task_model();
	        foreach ($this->arr_sp_config as $sp_key=>$sp_val)
	        {
	            $import_num = (isset($sp_val['nns_config']['import_op_pre_max']) && (int)$sp_val['nns_config']['import_op_pre_max'] >0) ? (int)$sp_val['nns_config']['import_op_pre_max'] : 20;
	            $infos = $queue_task_model->get_import_op_list($sp_key,null,0,$import_num);
	            if (!is_array($infos['data']) || empty($infos['data']))
	            {
	                continue;
	            }
	            foreach ($infos['data'] as $val)
	            {
	                $queue_task_model->add_task_to_queue($sp_key, $val['nns_id']);
	            }
	        }
	    }
	    else
	    {
	        $this->msg("中心注入指令往中心同步指令注入无任何SP匹配，没执行任何东西");
	    }
	    return ;
	}
}
$arr_files = pathinfo(__FILE__);
$file_name = $arr_files['filename'];
$timer_execute = new timer_execute($file_name, '',__FILE__,'import');
$timer_execute->run();