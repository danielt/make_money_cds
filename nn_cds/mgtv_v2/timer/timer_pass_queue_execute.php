<?php
ini_set('display_errors', 0);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_init.php';
include_once dirname(dirname(__FILE__)) . '/callback_notify/import_queue.class.php';

class timer_pass_queue_execute extends nn_timer
{
    public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->init($params);
        $this->msg('执行结束...');
    }

    public function init($params = null)
    {
        if($this->obj_dc === null)
        {
            $this->obj_dc = nl_get_dc(array (
                "db_policy" => NL_DB_WRITE | NL_DB_READ,
                "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
            ));
        }
        $result_sp = $this->_get_all_sp_info();
        if($result_sp['ret'] !=0)
        {
            $this->msg("sp信息获取所有失败：".var_export($result_sp,true));
            return ;
        }
        if (is_array($this->arr_sp_config) && !empty($this->arr_sp_config))
        {
            foreach ($this->arr_sp_config as $sp_key=>$sp_val)
            {
                if (!isset($sp_val['nns_config']['pass_queue_disabled']) || (int)$sp_val['nns_config']['pass_queue_disabled'] == 0)
                {
                    $this->msg($sp_key . "透传队列关闭");
                    continue;
                }
                $pass_params = array(
                    'nns_org_id' => $sp_key,
                    'nns_audit' => 1,//审核通过
                    'nns_pause' => 0,//运行状态
                    'nns_status' => 1,//正在注入
                );
                $pass_queue = nl_pass_queue::get_pass_queue_for_timer($this->obj_dc,$pass_params,100);
                if ($pass_queue['ret'] != 0 || empty($pass_queue['data_info']))
                {
                    $this->msg($sp_key . "透传队列没有数据");
                    continue;
                }
                global $g_bk_version_number;
                $bk_version = $g_bk_version_number == '1' ? true : false;
                unset($g_bk_version_number);
                $import_queue = new import_queue($this->obj_dc);
                foreach ($pass_queue['data_info'] as $info_list)
                {
                    if ($bk_version)
                    {
                        $info_list['nns_content'] = html_entity_decode($info_list['nns_content']);
                    }
                    $import_queue->import_pass_queue($info_list,$sp_val['nns_config']);
                }
            }
        }
    }
}
$arr_files = pathinfo(__FILE__);
$file_name = $arr_files['filename'];
$timer_pass_queue_execute = new timer_pass_queue_execute($file_name, '',__FILE__,'import');
$timer_pass_queue_execute->run();


