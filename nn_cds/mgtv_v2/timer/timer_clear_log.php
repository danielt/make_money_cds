<?php
/**
 * 定时器清理日志
 * @author xingcheng.hu
 * Date: 2016/7/12
 * Time: 14:07
 */
header("Content-Type:text/html;charset=utf-8");
date_default_timezone_set("Asia/Hong_Kong");
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_init.php';
class timer_clear_log extends nn_timer
{

    public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->do_timer_action();
        $this->msg('执行结束...');
    }
    public function do_timer_action()
    {
        $config = get_config_v2('g_clear_log');
        $this->dc = nl_get_dc(array(
            "db_policy" => NL_DB_WRITE,
            "cache_policy" => NP_KV_CACHE_TYPE_NULL
        ));
        //清理全局错误日志
        if(isset($config['global_error_log']['enable']) && $config['global_error_log']['enable']===0)
        {
            $config['global_error_log']['clear_day_before_now'] = empty($config['global_error_log']['clear_day_before_now']) ? 180 : $config['global_error_log']['clear_day_before_now'];
            $time = date('Y-m-d');
            $sql = "DELETE FROM nns_global_error_log WHERE nns_create_time <= DATE_ADD('{$time}',INTERVAL -{$config['global_error_log']['clear_day_before_now']} day)";
            $result = nl_execute_by_db($sql,$this->dc->db());
            if($result)
            {
                $this->msg('执行清理global_error_log成功'.$sql);
            }
            else
            {
                $this->msg('执行清理global_error_log失败'.$sql);
            }
            unset($config['global_error_log']);
        }

        //清理EGP注入日志
        if(isset($config['import_epg_log']['enable']) && $config['import_epg_log']['enable']===0)
        {
            $config['import_epg_log']['clear_day_before_now'] = empty($config['import_epg_log']['clear_day_before_now']) ? 180 : $config['import_epg_log']['clear_day_before_now'];
            $time = date('Y-m-d');
            $sql = "DELETE FROM nns_mgtvbk_import_epg_log WHERE nns_create_time <= DATE_ADD('{$time}',INTERVAL -{$config['import_epg_log']['clear_day_before_now']} day)";
            $where = "";
            if($config['import_epg_log']['clear_data_fail_or_suc']===-1)
            {
                $where = " and nns_status!=0";
            }
            if($config['import_epg_log']['clear_data_fail_or_suc']===1)
            {
                $where = " and nns_status=0";
            }
            $sql .= $where;
            $result = nl_execute_by_db($sql,$this->dc->db());
            if($result)
            {
                $this->msg('执行清理import_epg_log成功'.$sql);
            }
            else
            {
                $this->msg('执行清理import_epg_log失败'.$sql);
            }
            unset($config['import_epg_log']);
        }

        //清理CDN注入日志
        if(isset($config['c2_log']['enable']) && $config['c2_log']['enable']===0)
        {
            $config['c2_log']['clear_day_before_now'] = empty($config['c2_log']['clear_day_before_now']) ? 180 : $config['c2_log']['clear_day_before_now'];
            $time = date('Y-m-d');
            $sql = "DELETE FROM nns_mgtvbk_c2_log WHERE nns_create_time <= DATE_ADD('{$time}',INTERVAL -{$config['c2_log']['clear_day_before_now']} day)";
            $where = "";
            if($config['c2_log']['clear_data_fail_or_suc']===-1)
            {
                $where = " and nns_notify_result=-1";
            }
            if($config['c2_log']['clear_data_fail_or_suc']===1)
            {
                $where = " and nns_notify_result=0 and nns_notify_result is not null";
            }
            if($config['c2_log']['clear_data_fail_or_suc']===2)
            {
                $where = " and nns_notify_result is null";
            }
            $sql .= $where;
            $result = nl_execute_by_db($sql,$this->dc->db());
            if($result)
            {
                $this->msg('执行清理c2_log成功'.$sql);
            }
            else
            {
                $this->msg('执行清理c2_log失败'.$sql);
            }
            unset($config['c2_log']);
        }

        //清理消息注入日志
        if(isset($config['import_log']['enable']) && $config['import_log']['enable']===0)
        {
            $config['import_log']['clear_day_before_now'] = empty($config['import_log']['clear_day_before_now']) ? 180 : $config['import_log']['clear_day_before_now'];
            $time = date('Y-m-d');
            $sql = "DELETE FROM nns_mgtvbk_import_log WHERE nns_create_time <= DATE_ADD('{$time}',INTERVAL -{$config['import_log']['clear_day_before_now']} day)";
            $where = "";
            if($config['import_log']['clear_data_fail_or_suc']===-1)
            {
                $where = " and nns_state!=0";
            }
            if($config['import_log']['clear_data_fail_or_suc']===1)
            {
                $where = " and nns_state=0";
            }
            $sql .= $where;
            $result = nl_execute_by_db($sql,$this->dc->db());
            if($result)
            {
                $this->msg('执行清理import_log成功'.$sql);
            }
            else
            {
                $this->msg('执行清理import_log失败'.$sql);
            }
            unset($config['import_log']);
        }

        //清理同步中心注入日志
        if(isset($config['op_log']['enable']) && $config['op_log']['enable']===0)
        {
            $config['op_log']['clear_day_before_now'] = empty($config['op_log']['clear_day_before_now']) ? 180 : $config['op_log']['clear_day_before_now'];
            $time = date("Y-m-d");
            $sql = "DELETE FROM nns_mgtvbk_op_log WHERE nns_create_time <= DATE_ADD('{$time}',INTERVAL -{$config['op_log']['clear_day_before_now']} day)";
            $result = nl_execute_by_db($sql,$this->dc->db());
            if($result)
            {
                $this->msg('执行清理op_log成功'.$sql);
            }
            else
            {
                $this->msg('执行清理op_log失败'.$sql);
            }
            unset($config['op_log']);
        }
        
        //清理切片日志
        if(isset($config['clip_task_log']['enable']) && $config['clip_task_log']['enable']===0)
        {
            $config['clip_task_log']['clear_day_before_now'] = empty($config['clip_task_log']['clear_day_before_now']) ? 180 : $config['clip_task_log']['clear_day_before_now'];
            $time = date("Y-m-d");
        	$sql = "DELETE FROM nns_mgtvbk_clip_task_log WHERE nns_create_time <= DATE_ADD('{$time}',INTERVAL -{$config['clip_task_log']['clear_day_before_now']} day)";
        	$result = nl_execute_by_db($sql,$this->dc->db());
        	if($result)
        	{
        		$this->msg('执行清理clip_task_log成功'.$sql);
        	}
        	else
        	{
        		$this->msg('执行清理clip_task_log失败'.$sql);
        	}
        	unset($config['clip_task_log']);
        }
    }
}
$timer_clear_log = new timer_clear_log("clear_log", 'public',__FILE__);
$timer_clear_log->run();

