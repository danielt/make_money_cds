<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(__FILE__)) .'/nn_class/file_encode/file_encode_execute.php';
class timer_execute extends nn_timer
{
    public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->init($params);
        $this->msg('执行结束...');
    }
    
    /**
     * 初始化
     */
    function init($params =null)
    {
        $str_file_encode_pool = $params["bk_queue_value"];
        if(strlen($str_file_encode_pool) <1)
        {
            $this->msg("[redis转码池中返回数据为空]".$str_file_encode_pool);
            return ;
        }
        $str_mgtv_init = dirname(dirname(__FILE__))."/mgtv_v2/{$str_file_encode_pool}/init.php";
        if(!file_exists($str_mgtv_init))
        {
            $this->msg("[初始化文件不存在]".$str_mgtv_init);
            return ;
        }
        include_once $str_mgtv_init;
        define('ORG_ID', $str_file_encode_pool);
        $obj_file_encode_execute = new nl_file_encode_execute(ORG_ID,$this);
        $result = $obj_file_encode_execute->set_task();
        $this->msg("处理结果：".var_export($result,true));
    }
}
$arr_files = pathinfo(__FILE__);
$file_name = $arr_files['filename'];
$timer_execute = new timer_execute($file_name, $file_name,__FILE__,'file_encode');
$timer_execute->run();



