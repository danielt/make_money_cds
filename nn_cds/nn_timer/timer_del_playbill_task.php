<?php
/**
 * 更新频道绑定的第三方频道节目单信息
 * @author ye.zhong
 * @date 2017-05-13
 */
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(__FILE__)) . '/api_v2/nn_const.php';
include_once dirname(dirname(__FILE__)) . '/api_v2/common.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/c2_task/c2_task.class.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/c2_log/c2_log.class.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/epg_log/epg_log.class.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/live/playbill.class.php';
//引入np类文件
include_np('np_string');
define("IS_LOG_TIMER_OPERATION", true);
class timer_execute extends nn_timer
{
	public function action($params = null)
	{
		$this->msg('开始执行...');
		$this->init($params);
		$this->msg('执行结束...');
	}

    /**
     * 初始化
     * @param string $params
     */
    public function init($params = null)
    {
        $str_third_playbill_pool = $params["bk_queue_value"];
        if(!$this->has_length($str_third_playbill_pool))
        {
            echo "redis节目单池中返回数据为空".'<br/>';
            nn_log::write_log_message(LOG_MODEL_MESSAGE, "[redis节目单池中返回数据为空]".$str_third_playbill_pool, "get_playbill_info");
            return false;
        }
        $arr_guid_time = explode("|#|", $str_third_playbill_pool);
        $str_sp_guid =$arr_guid_time[0];
        $str_type_1 =$arr_guid_time[1];
        $str_type_2 =isset($arr_guid_time[2]) ? $arr_guid_time[2] : '';
        $result_sp = $this->_get_sp_info($str_sp_guid);
        $base_day = (isset($this->arr_params['bk_queue_config']['nns_ext_info']['base_day']) && (int)$this->arr_params['bk_queue_config']['nns_ext_info']['base_day'] >0) ? (int)$this->arr_params['bk_queue_config']['nns_ext_info']['base_day'] : '21';
        $str_date = date("Y-m-d",strtotime("-{$base_day} day"))." 23:59:59";
        if($result_sp['ret'] !=0)
        {
            return $result_sp;
        }
        if(isset($this->arr_sp_config[$str_sp_guid]) && is_array($this->arr_sp_config[$str_sp_guid]) && !empty($this->arr_sp_config[$str_sp_guid]))
        {
            $config_state = isset($this->arr_sp_config[$str_sp_guid]['nns_config']['playbill_add']['status']) ? $this->arr_sp_config[$str_sp_guid]['nns_config']['playbill_add']['status'] : '';
            $flag_cdn = $epg_state=true;
            if($config_state == '1')
            {
                $epg_state = false;
            }
            else if($config_state == '2')
            {
                $flag_cdn = false;
            }
            $result_del_c2 = nl_c2_task::del_by_date_and_state($this->obj_dc, $str_sp_guid, $str_date,$flag_cdn,$epg_state);
            if($result_del_c2['ret'] !=0)
            {
                return $result_del_c2;
            }
        }
        if($str_type_2 == 'cdn' || $str_type_1 =='cdn')
        {
            $result_del_cdn_log = nl_c2_log::del_by_date_and_state($this->obj_dc, $str_sp_guid, $str_date);
            if($result_del_cdn_log['ret'] !=0)
            {
                return $result_del_cdn_log;
            }
        }
        if($str_type_1 == 'epg' || $str_type_2 == 'epg')
        {
            $result_del_epg_log = nl_epg_log::del_by_date_and_state($this->obj_dc, $str_sp_guid, $str_date);
            if($result_del_epg_log['ret'] !=0)
            {
                return $result_del_epg_log;
            }
        }
        return $this->_return_data(0,'OK');
    }
}
$arr_files = pathinfo(__FILE__);
$file_name = $arr_files['filename'];
$timer_execute = new timer_execute($file_name, ORG_ID,__FILE__,'del_playbill_task');
$timer_execute->run();