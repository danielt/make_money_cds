<?php
/**
 * 定时器推入队列池
 * pan.liang
 */
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(__FILE__)) . '/api_v2/nn_const.php';
include_once dirname(dirname(__FILE__)) . '/api_v2/common.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/nl_common.func.php';

class timer_execute extends nn_timer
{
    public $str_date = null;
    public $str_date_time = null;
    
    public $arr_func_array = array(
        'cdn_video'=>array(
            'type'=>'video',
            'func'=>'cdn',
        ),
        'cdn_index'=>array(
            'type'=>'index',
            'func'=>'cdn',
        ),
        'cdn_media'=>array(
            'type'=>'media',
            'func'=>'cdn',
        ),
        'cdn_live'=>array(
            'type'=>'live',
            'func'=>'cdn',
        ),
        'cdn_live_media'=>array(
            'type'=>'live_media',
            'func'=>'cdn',
        ),
        'cdn_playbill'=>array(
            'type'=>'playbill',
            'func'=>'cdn',
        ),
        'cdn_file'=>array(
            'type'=>'file',
            'func'=>'cdn',
        ),
        'epg_video'=>array(
            'type'=>'video',
            'func'=>'epg',
        ),
        'epg_index'=>array(
            'type'=>'index',
            'func'=>'epg',
        ),
        'epg_media'=>array(
            'type'=>'media',
            'func'=>'epg',
        ),
        'epg_live'=>array(
            'type'=>'live',
            'func'=>'epg',
        ),
        'epg_live_media'=>array(
            'type'=>'live_media',
            'func'=>'epg',
        ),
        'epg_playbill'=>array(
            'type'=>'playbill',
            'func'=>'epg',
        ),
    );
    
   
    
    public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->init($params);
        $this->msg('执行结束...');
    }
    
    /**
     * 初始化
     */
    public function init($params=null)
    {
        $this->obj_dc = nl_get_dc(array (
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_NULL
        ));
        $flag_request = false;
        if(isset($params['request']['nns_id']))
        {
            $flag_request = true;
            $nns_id = strlen($params['request']['nns_id']) >0 ? explode(',', $params['request']['nns_id']) : '';
            $result_queue = nl_queue::timer_query_by_ids($this->obj_dc,$nns_id);
        }
        else if(isset($params['request']['nns_queue']))
        {
            $flag_request = true;
            $nns_queue = strlen($params['request']['nns_queue']) >0 ? explode(',', $params['request']['nns_queue']) : '';
            $result_queue = nl_queue::timer_query_by_queue_ids($this->obj_dc,$nns_queue);
        }
        else
        {
            $result_queue = nl_queue::timer_query($this->obj_dc);
        }
        if($result_queue['ret'] !=0)
        {
            $this->msg(var_export($result_queue,true));
            return ;
        }
        if(!isset($result_queue['data_info']) || !is_array($result_queue['data_info']) || empty($result_queue['data_info']))
        {
            $this->msg("后台未配置队列池数据:".var_export($result_queue,true));
            return ;
        }
        $result_queue = $result_queue['data_info'];
        $time = time();
        $this->str_date = date("Y-m-d");
        $this->str_date_time = date('Y-m-d H:i:s');
        global $g_arr_queue_pool;
        if(is_array($g_arr_queue_pool))
        {
            foreach ($g_arr_queue_pool as $val)
            {
                $this->g_arr_queue_pool[$val['name']] = $val;
            }
        }
        unset($g_arr_queue_pool);
        foreach ($result_queue as $queue_val)
        {
            $flag = (isset($this->g_arr_queue_pool[$queue_val['nns_queue']]) && is_array($this->g_arr_queue_pool[$queue_val['nns_queue']]) && !empty($this->g_arr_queue_pool[$queue_val['nns_queue']])) ? true : false;
            $arr_function = get_class_methods($this);
            $arr_function = is_array($arr_function) ? $arr_function : array();
            $queue_val['nns_ext_info'] = (isset($queue_val['nns_ext_info']) && strlen($queue_val['nns_ext_info'])>0 && $this->is_json($queue_val['nns_ext_info'])) ? json_decode($queue_val['nns_ext_info'],true) : null;
            if(isset($this->arr_func_array[$queue_val['nns_queue']]['func']) && strlen($this->arr_func_array[$queue_val['nns_queue']]['func']) >0 && isset($this->arr_func_array[$queue_val['nns_queue']]['type']) && strlen($this->arr_func_array[$queue_val['nns_queue']]['type']) >0)
            {
                $queue_val['type'] = $this->arr_func_array[$queue_val['nns_queue']]['type'];
                $str_pre_fun = $this->arr_func_array[$queue_val['nns_queue']]['func'];
            }
            else
            {
                $str_pre_fun = $queue_val['nns_queue'];
            }
            $func = $str_pre_fun."_queue";
            if(!in_array($func, $arr_function))
            {
                $this->msg('没查询到配置的队列:'.var_export($func,true).';方法数组:'.var_export($arr_function,true));
                continue;
            }
            $result = $this->$func($queue_val,$params);
            $this->msg('推入数据库队列池结果:'.var_export($result,true));
            $ex_time = $queue_val['nns_timer']*60;
            if(!$flag_request && (($time - strtotime($queue_val['nns_modify_time'])) < ($queue_val['nns_timer']*60)) && $queue_val['nns_modify_time'] != $queue_val['nns_create_time'])
            {
                $this->msg("队列时间未到，不执行[{$queue_val['nns_queue']}]时间:[{$queue_val['nns_modify_time']}]等待时间[{$ex_time}];当前时间:[{$this->str_date_time}]");
                continue;
            }
            if($flag)
            {
                $result = $this->push_to_queue_pool_redis($queue_val);
                $this->msg('推入redis队列池结果:'.var_export($result,true));
                nl_queue::edit($this->obj_dc, array('nns_modify_time'=>$this->str_date_time), $queue_val['nns_id']);
            }
            else
            {
                $this->msg('全局配置没查询到该数组:'.var_export($func,true).';全局数组:'.var_export($this->g_arr_queue_pool,true));
            }
        }
        return $this->_return_data(0,'ok');
    }
        
    /**
     * 节目单队列
     */
    public function thrid_playbill_queue($queue_val,$params=null)
    {
        include_once dirname(dirname(__FILE__)) . '/nn_logic/live/third_live.class.php';
        $result_queue_exsist_del = nl_queue_pool::query_by_queue_date_del($this->obj_dc,$queue_val['nns_id'],$this->str_date);
        if($result_queue_exsist_del['ret'] !=0)
        {
            return $result_queue_exsist_del;
        }
        $arr_exsist_del = null;
        $result_queue_exsist_del = (isset($result_queue_exsist_del['data_info']) && is_array($result_queue_exsist_del['data_info']) && !empty($result_queue_exsist_del['data_info'])) ? $result_queue_exsist_del['data_info'] : null;
        if(is_array($result_queue_exsist_del) && !empty($result_queue_exsist_del))
        {
            foreach ($result_queue_exsist_del as $exsist_del_val)
            {
                $arr_exsist_del[] = $exsist_del_val['nns_value'];
            }
            $result_queue_pool = nl_queue_pool::delete_by_value($this->obj_dc, $queue_val['nns_id'], $arr_exsist_del);
            if($result_queue_pool['ret'] !=0)
            {
                return $result_queue_pool;
            }
        }
        $result_queue_exsist = nl_queue_pool::query_by_queue_date($this->obj_dc,$queue_val['nns_id'],$this->str_date);
        if($result_queue_exsist['ret'] !=0)
        {
            return $result_queue_exsist;
        }
        $arr_exsist = null;
        $result_queue_exsist = (isset($result_queue_exsist['data_info']) && is_array($result_queue_exsist['data_info']) && !empty($result_queue_exsist['data_info'])) ? $result_queue_exsist['data_info'] : null;
        if(is_array($result_queue_exsist))
        {
            foreach ($result_queue_exsist as $exsist_val)
            {
                $arr_exsist[$exsist_val['nns_value']] = $exsist_val;
            }
        }
        $result_live = nl_third_live::query_all($this->obj_dc);
        if($result_live['ret'] !=0 || !isset($result_live['data_info']) || empty($result_live['data_info']) || !is_array($result_live['data_info']))
        {
            $this->msg(var_export($result_live,true));
            return $result_live;
        }
        $result_live = $result_live['data_info'];
//         $queue_val['nns_ext_info'] = (isset($queue_val['nns_ext_info']) && strlen($queue_val['nns_ext_info']) >0) ? json_decode($queue_val['nns_ext_info'],true) : null;
        $max_day = (isset($queue_val['nns_ext_info']['max_day']) && $queue_val['nns_ext_info']['max_day'] >0) ? $queue_val['nns_ext_info']['max_day'] : 0;
        $date_arr = array($this->str_date);
        for ($i=1;$i<=$max_day;$i++)
        {
            $date_arr[] = date("Y-m-d",strtotime("+{$i} day"));
        }
        $date_arr = array_filter($date_arr);
        $arr_insert_data = null;
        foreach ($date_arr as $date_val)
        {
            foreach ($result_live as $live_val)
            {
                $temp_val = $live_val['nns_id']."|#|".$date_val;
                if(isset($arr_exsist[$temp_val]) && isset($arr_exsist[$temp_val]['nns_state']))
                {
                    $arr_insert_data[$temp_val]=array(
                        'nns_id'=>np_guid_rand(),
                        'nns_queue_id'=>$queue_val['nns_id'],
                        'nns_value'=>$temp_val,
                        'nns_state'=>$arr_exsist[$temp_val]['nns_state'],
                        'nns_create_time'=>$this->str_date_time,
                        'nns_modify_time'=>$this->str_date_time,
                    );
                }
                else
                {
                    $arr_insert_data[$temp_val]=array(
                        'nns_id'=>np_guid_rand(),
                        'nns_queue_id'=>$queue_val['nns_id'],
                        'nns_value'=>$temp_val,
                        'nns_state'=>0,
                        'nns_create_time'=>$this->str_date_time,
                        'nns_modify_time'=>$this->str_date_time,
                    );
                }
            }
        }
        return $this->_check_all_params($queue_val, $arr_insert_data,$arr_exsist,true);
    }
    
    /**
     * 节目单文件生成（江苏华博大电视使用)
     * @param unknown $queue_val
     * @param string $params
     * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据'), Ambigous>
     */
    public function playbill_to_localfile_queue($queue_val,$params=null)
    {
        $arr_insert_data = null;
        $arr_insert_data['placeholder'] = array(
            'nns_id'=>np_guid_rand(),
            'nns_queue_id'=>$queue_val['nns_id'],
            'nns_value'=>'placeholder',
            'nns_state'=>0,
            'nns_create_time'=>$this->str_date_time,
            'nns_modify_time'=>$this->str_date_time,
        );
        return $this->_check_all_params($queue_val, $arr_insert_data);
    }
    
    /**
     * ftp 扫盘注入媒资
     * @param unknown $queue_val
     * @param string $params
     * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据'), Ambigous>
     */
    public function ftp_media_to_db_queue($queue_val,$params=null)
    {
        $arr_insert_data = null;
        $arr_insert_data['placeholder'] = array(
            'nns_id'=>np_guid_rand(),
            'nns_queue_id'=>$queue_val['nns_id'],
            'nns_value'=>'placeholder',
            'nns_state'=>0,
            'nns_create_time'=>$this->str_date_time,
            'nns_modify_time'=>$this->str_date_time,
        );
        return $this->_check_all_params($queue_val, $arr_insert_data);
    }

    /**
     * ftp 扫盘注入媒资--内蒙古孝乐扫描
     * @param unknown $queue_val
     * @param string $params
     * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据'), Ambigous>
     */
    public function ftp_media_to_db_nmxl_queue($queue_val,$params=null)
    {
        $arr_insert_data = null;
        $arr_insert_data['placeholder'] = array(
            'nns_id'=>np_guid_rand(),
            'nns_queue_id'=>$queue_val['nns_id'],
            'nns_value'=>'placeholder',
            'nns_state'=>0,
            'nns_create_time'=>$this->str_date_time,
            'nns_modify_time'=>$this->str_date_time,
        );
        return $this->_check_all_params($queue_val, $arr_insert_data);
    }
    
    /**
     * 节目单队列
     */
    public function del_playbill_task_queue($queue_val,$params=null)
    {
        $result_sp = $this->_get_all_sp_info();
        if($result_sp['ret'] !=0 || !is_array($this->arr_sp_config) || empty($this->arr_sp_config))
        {
            return $result_sp;
        }
        $arr_insert_data = null;
        foreach ($this->arr_sp_config as $sp_key=>$sp_val)
        {
            $temp_task = '';
            if(isset($sp_val['nns_config']['disabled_epg']) && ($sp_val['nns_config']['disabled_epg'] == '0' || strlen($sp_val['nns_config']['disabled_epg']) <1) && isset($sp_val['nns_config']['epg_assets_enabled']) && is_array($sp_val['nns_config']['epg_assets_enabled']) && in_array('playbill', $sp_val['nns_config']['epg_assets_enabled']))
            {
                $temp_task.="|#|epg";
            }
            if(isset($sp_val['nns_config']['disabled_cdn']) && ($sp_val['nns_config']['disabled_cdn'] == '0' || strlen($sp_val['nns_config']['disabled_cdn']) <1) && isset($sp_val['nns_config']['cdn_assets_enabled']) && is_array($sp_val['nns_config']['cdn_assets_enabled']) && in_array('playbill', $sp_val['nns_config']['cdn_assets_enabled']))
            {
                $temp_task.="|#|cdn";
            }
            if(strlen($temp_task)<1)
            {
                continue;
            }
            $temp_task = $sp_val['nns_id'].$temp_task;
            $arr_insert_data[$temp_task] = array(
                'nns_id'=>np_guid_rand(),
                'nns_queue_id'=>$queue_val['nns_id'],
                'nns_value'=>$temp_task,
                'nns_state'=>0,
                'nns_create_time'=>$this->str_date_time,
                'nns_modify_time'=>$this->str_date_time,
            );
        }
        return $this->_check_all_params($queue_val, $arr_insert_data);
    }
    
    /**
     * 节目单删除队列
     */
    public function del_thrid_playbill_queue($queue_val,$params=null)
    {
        include_once dirname(dirname(__FILE__)) . '/nn_logic/live/live_media.class.php';
        $result_live_media = nl_live_media::query_group_all_live($this->obj_dc);
        if($result_live_media['ret'] !=0 || !isset($result_live_media['data_info']) || empty($result_live_media['data_info']) || !is_array($result_live_media['data_info']))
        {
            return $result_live_media;
        }
        $result_live_media = $result_live_media['data_info'];
        $arr_insert_data = $arr_live = null;
        $del_time_base = (isset($queue_val['nns_ext_info']["base_day"]) && (int)$queue_val['nns_ext_info']["base_day"] >0) ? (int)$queue_val['nns_ext_info']["base_day"] : 7;
        $del_time_max = (isset($queue_val['nns_ext_info']["max_day"]) && (int)$queue_val['nns_ext_info']["max_day"] >0) ? (int)$queue_val['nns_ext_info']["max_day"] : 3;
        foreach ($result_live_media as $live_media_val)
        {
            if(isset($arr_live[$live_media_val['nns_live_id']]))
            {
                continue;
            }
            $temp_del_time_last = (isset($arr_live[$live_media_val['nns_storage_delay']]) && (int)$arr_live[$live_media_val['nns_storage_delay']] >0) ? (int)$arr_live[$live_media_val['nns_storage_delay']] + $del_time_max : $del_time_base + $del_time_max;
            $temp_val = $live_media_val['nns_live_id']."|#|".date("Y-m-d",strtotime("-{$temp_del_time_last} day"))."|#|".date("Y-m-d",strtotime("-{$del_time_base} day"));
            $arr_live[$arr_live[$live_media_val['nns_live_id']]] = $temp_val;
            $arr_insert_data[$temp_val]=array(
                'nns_id'=>np_guid_rand(),
                'nns_queue_id'=>$queue_val['nns_id'],
                'nns_value'=>$temp_val,
                'nns_state'=>0,
                'nns_create_time'=>$this->str_date_time,
                'nns_modify_time'=>$this->str_date_time,
            );
            
        }
        return $this->_check_all_params($queue_val, $arr_insert_data);
    }
    
    /**
     * 媒资全量数据队列
     */
    public function thrid_full_assests_queue($queue_val,$params=null)
    {
        $result_query = nl_queue_pool::timer_query_by_queue_id_v2($this->obj_dc,$queue_val['nns_id']);
        if($result_query['ret'] !=0 || (isset($result_query['data_info']) && is_array($result_query['data_info']) && !empty($result_query['data_info'])))
        {
            return $result_query;
        }
        $arr_insert_data[$queue_val['nns_id']]=array(
            'nns_id'=>np_guid_rand(),
            'nns_queue_id'=>$queue_val['nns_id'],
            'nns_value'=>$queue_val['nns_id'],
            'nns_state'=>0,
            'nns_create_time'=>$this->str_date_time,
            'nns_modify_time'=>$this->str_date_time,
        );
        return $this->_check_all_params($queue_val, $arr_insert_data);
    }

    /**
     * 酷控媒资全量数据队列
     */
    public function thrid_full_assests_v2_queue($queue_val,$params=null)
    {
        $result_query = nl_queue_pool::timer_query_by_queue_id_v2($this->obj_dc,$queue_val['nns_id']);
        if($result_query['ret'] !=0 || (isset($result_query['data_info']) && is_array($result_query['data_info']) && !empty($result_query['data_info'])))
        {
            return $result_query;
        }
        $arr_insert_data[$queue_val['nns_id']]=array(
            'nns_id'=>np_guid_rand(),
            'nns_queue_id'=>$queue_val['nns_id'],
            'nns_value'=>$queue_val['nns_queue'],
            'nns_state'=>0,
            'nns_create_time'=>$this->str_date_time,
            'nns_modify_time'=>$this->str_date_time,
        );
        return $this->_check_all_params($queue_val, $arr_insert_data);
    }
    
    
    /**
     * 酷控媒资全量数据队列
     */
    public function thrid_full_assests_v3_queue($queue_val,$params=null)
    {
        $base_file_dir = dirname(dirname(__FILE__))."/data/split/xml/full_assests/";
        if(!is_dir($base_file_dir))
        {
            return $this->_return_data(1,"路径不存在[{$base_file_dir}]");
        }
        $arr_base_file_dir = scandir($base_file_dir);
        if((!is_array($arr_base_file_dir) || empty($arr_base_file_dir)))
        {
            return $this->_return_data(1,"路径不存在文件[{$base_file_dir}]");
        }
        $arr_base_file_dir = array_diff($arr_base_file_dir, array('.','..'));
        if((!is_array($arr_base_file_dir) || empty($arr_base_file_dir)))
        {
            return $this->_return_data(1,"路径不存在文件[{$base_file_dir}]");
        }
        $arr_temp_base_file_dir = null;
        foreach ($arr_base_file_dir as $afile)
        {
            if (is_dir($base_file_dir . $afile))
            {
                continue;
            }
            $pathinfo_file = pathinfo($afile);
            if(isset($pathinfo_file['extension']) && $pathinfo_file['extension']=="xml" && isset($pathinfo_file['filename']) && strlen($pathinfo_file['filename']) > 0)
            {
                $str_dir = $base_file_dir.$afile;
                $str_dir = str_replace('\\', '/', $str_dir);
                $arr_temp_base_file_dir[md5($str_dir)] = $str_dir;
            }
        }
        if((!is_array($arr_temp_base_file_dir) || empty($arr_temp_base_file_dir)))
        {
            return $this->_return_data(1,"路径不存在文件[{$base_file_dir}]");
        }
        $result_query = nl_queue_pool::delete_by_queue_date($this->obj_dc,$queue_val['nns_id'],date("Y-m-d",strtotime("-1 day")));
        if($result_query['ret'] !=0)
        {
            return $result_query;
        }
        
        foreach ($arr_temp_base_file_dir as $key=>$value)
        {
            $arr_insert_data[$key]=array(
                'nns_id'=>np_guid_rand(),
                'nns_queue_id'=>$queue_val['nns_id'],
                'nns_value'=>$value,
                'nns_state'=>0,
                'nns_create_time'=>$this->str_date_time,
                'nns_modify_time'=>$this->str_date_time,
            );
            
        }
        return $this->_check_all_params($queue_val, $arr_insert_data);
    }

    /**
     * @description:酷控媒资数据队列注入到vod表
     * @author:xinxin.deng
     * @date: 2017/9/20 11:39
     */
    public function thrid_full_assests_to_vod_queue($queue_val,$params=null)
    {
        $result_query = nl_queue_pool::timer_query_by_queue_id_v2($this->obj_dc,$queue_val['nns_id']);
        if($result_query['ret'] !=0 || (isset($result_query['data_info']) && is_array($result_query['data_info']) && !empty($result_query['data_info'])))
        {
            return $result_query;
        }
        $arr_insert_data[$queue_val['nns_id']]=array(
            'nns_id'=>np_guid_rand(),
            'nns_queue_id'=>$queue_val['nns_id'],
            'nns_value'=>$queue_val['nns_queue'],
            'nns_state'=>0,
            'nns_create_time'=>$this->str_date_time,
            'nns_modify_time'=>$this->str_date_time,
        );
        return $this->_check_all_params($queue_val, $arr_insert_data);
    }
    /**
     * 消息队列分发到中心注入指令队列
     */
    public function message_queue($queue_val,$params=null)
    {
        $result_cp = $this->_get_all_cp_info();
        if($result_cp['ret'] !=0 || !is_array($this->arr_cp_config) || empty($this->arr_cp_config))
        {
            return $result_cp;
        }
        $arr_insert_data = null;
        foreach ($this->arr_cp_config as $cp_key=>$cp_val)
        {
            if(isset($cp_val['nns_config']) && is_array($cp_val['nns_config'])>0 && !empty($cp_val['nns_config']))
            {
                $arr_insert_data[$cp_val['nns_id']] = array(
                    'nns_id'=>np_guid_rand(),
                    'nns_queue_id'=>$queue_val['nns_id'],
                    'nns_value'=>$cp_val['nns_id'],
                    'nns_state'=>$cp_val['nns_state'],
                    'nns_create_time'=>$this->str_date_time,
                    'nns_modify_time'=>$this->str_date_time,
                );
            }
            else
            {
                $arr_insert_data[$cp_val['nns_id']] = array(
                    'nns_id'=>np_guid_rand(),
                    'nns_queue_id'=>$queue_val['nns_id'],
                    'nns_value'=>$cp_val['nns_id'],
                    'nns_state'=>1,
                    'nns_create_time'=>$this->str_date_time,
                    'nns_modify_time'=>$this->str_date_time,
                );
            }
        }
        return $this->_check_all_params($queue_val, $arr_insert_data);
    }
    
    /**
     * 中心注入指令队列
     */
    public function import_queue($queue_val,$params=null)
    {
        $result_sp = $this->_get_all_sp_info();
        if($result_sp['ret'] !=0 || !is_array($this->arr_sp_config) || empty($this->arr_sp_config))
        {
            return $result_sp;
        }
        $arr_insert_data = null;
        foreach ($this->arr_sp_config as $sp_key=>$sp_val)
        {
            if(isset($sp_val['nns_config']) && is_array($sp_val['nns_config'])>0 && !empty($sp_val['nns_config']))
            {
                $arr_insert_data[$sp_val['nns_id']] = array(
                    'nns_id'=>np_guid_rand(),
                    'nns_queue_id'=>$queue_val['nns_id'],
                    'nns_value'=>$sp_val['nns_id'],
                    'nns_state'=>$sp_val['nns_state'],
                    'nns_create_time'=>$this->str_date_time,
                    'nns_modify_time'=>$this->str_date_time,
                );
            }
            else
            {
                $arr_insert_data[$sp_val['nns_id']] = array(
                    'nns_id'=>np_guid_rand(),
                    'nns_queue_id'=>$queue_val['nns_id'],
                    'nns_value'=>$sp_val['nns_id'],
                    'nns_state'=>1,
                    'nns_create_time'=>$this->str_date_time,
                    'nns_modify_time'=>$this->str_date_time,
                );
            }
        }
        return $this->_check_all_params($queue_val, $arr_insert_data);
    }
    
    
    /**
     * 中心注入指令队列
     */
    public function clip_file_queue($queue_val,$params=null)
    {
        $result_sp = $this->_get_all_sp_info();
        if($result_sp['ret'] !=0 || !is_array($this->arr_sp_config) || empty($this->arr_sp_config))
        {
            return $result_sp;
        }
        $arr_insert_data = null;
        foreach ($this->arr_sp_config as $sp_key=>$sp_val)
        {
            if(isset($sp_val['nns_config']['disabled_clip']) && ($sp_val['nns_config']['disabled_clip'] == '0' || strlen($sp_val['nns_config']['disabled_clip']) <1))
            {
                $arr_insert_data[$sp_val['nns_id']] = array(
                    'nns_id'=>np_guid_rand(),
                    'nns_queue_id'=>$queue_val['nns_id'],
                    'nns_value'=>$sp_val['nns_id'],
                    'nns_state'=>0,
                    'nns_create_time'=>$this->str_date_time,
                    'nns_modify_time'=>$this->str_date_time,
                );
            }
        }
        return $this->_check_all_params($queue_val, $arr_insert_data);
    }
    
    /**
     * 转码队列
     */
    public function file_encode_queue($queue_val,$params=null)
    {
        $result_sp = $this->_get_all_sp_info();
        if($result_sp['ret'] !=0 || !is_array($this->arr_sp_config) || empty($this->arr_sp_config))
        {
            return $result_sp;
        }
        $arr_insert_data = null;
        foreach ($this->arr_sp_config as $sp_key=>$sp_val)
        {
            if(isset($sp_val['nns_config']['clip_file_encode_enable']) && $sp_val['nns_config']['clip_file_encode_enable'] == '1' 
                && isset($sp_val['nns_config']['clip_file_encode_model']) && ($sp_val['nns_config']['clip_file_encode_model'] =='1' || $sp_val['nns_config']['clip_file_encode_model'] =='2'))
            {
                $arr_insert_data[$sp_val['nns_id']] = array(
                    'nns_id'=>np_guid_rand(),
                    'nns_queue_id'=>$queue_val['nns_id'],
                    'nns_value'=>$sp_val['nns_id'],
                    'nns_state'=>0,
                    'nns_create_time'=>$this->str_date_time,
                    'nns_modify_time'=>$this->str_date_time,
                );
            }
        }
        return $this->_check_all_params($queue_val, $arr_insert_data);
    }
    
    
    /**
     * 切片队列->转码队列
     */
    public function clip_to_encode_queue($queue_val,$params=null)
    {
        $result_sp = $this->_get_all_sp_info();
        if($result_sp['ret'] !=0 || !is_array($this->arr_sp_config) || empty($this->arr_sp_config))
        {
            return $result_sp;
        }
        $arr_insert_data = null;
        foreach ($this->arr_sp_config as $sp_key=>$sp_val)
        {
            if(isset($sp_val['nns_config']['clip_file_encode_enable']) && $sp_val['nns_config']['clip_file_encode_enable'] == '1' && isset($sp_val['nns_config']['clip_file_encode_model']) && $sp_val['nns_config']['clip_file_encode_model'] =='1')
            {
                $arr_insert_data[$sp_val['nns_id']] = array(
                    'nns_id'=>np_guid_rand(),
                    'nns_queue_id'=>$queue_val['nns_id'],
                    'nns_value'=>$sp_val['nns_id'],
                    'nns_state'=>0,
                    'nns_create_time'=>$this->str_date_time,
                    'nns_modify_time'=>$this->str_date_time,
                );
            }
        }
        return $this->_check_all_params($queue_val, $arr_insert_data);
    }
    
    /**
     * 切片队列->转码队列
     */
    public function queue_to_encode_queue($queue_val,$params=null)
    {
        $result_sp = $this->_get_all_sp_info();
        if($result_sp['ret'] !=0 || !is_array($this->arr_sp_config) || empty($this->arr_sp_config))
        {
            return $result_sp;
        }
        $arr_insert_data = null;
        foreach ($this->arr_sp_config as $sp_key=>$sp_val)
        {
            if(isset($sp_val['nns_config']['clip_file_encode_enable']) && $sp_val['nns_config']['clip_file_encode_enable'] == '1' && isset($sp_val['nns_config']['clip_file_encode_model']) && $sp_val['nns_config']['clip_file_encode_model'] =='2')
            {
                $arr_insert_data[$sp_val['nns_id']] = array(
                    'nns_id'=>np_guid_rand(),
                    'nns_queue_id'=>$queue_val['nns_id'],
                    'nns_value'=>$sp_val['nns_id'],
                    'nns_state'=>0,
                    'nns_create_time'=>$this->str_date_time,
                    'nns_modify_time'=>$this->str_date_time,
                );
            }
        }
        return $this->_check_all_params($queue_val, $arr_insert_data);
    }
    /**
     * CDN 注入
     * @param unknown $queue_val
     * @param string $params
     */
    public function cdn_queue($queue_val,$params=null)
    {
        $result_sp = $this->_get_all_sp_info();
        if($result_sp['ret'] !=0 || !is_array($this->arr_sp_config) || empty($this->arr_sp_config))
        {
            return $result_sp;
        }
        $arr_insert_data = null;
        foreach ($this->arr_sp_config as $sp_key=>$sp_val)
        {
            if(isset($sp_val['nns_config']['disabled_cdn']) && ($sp_val['nns_config']['disabled_cdn'] == '0' || strlen($sp_val['nns_config']['disabled_cdn']) <1) && isset($sp_val['nns_config']['cdn_assets_enabled']) && is_array($sp_val['nns_config']['cdn_assets_enabled']) && in_array($queue_val['type'], $sp_val['nns_config']['cdn_assets_enabled']))
            {
                $arr_insert_data[$sp_val['nns_id']] = array(
                    'nns_id'=>np_guid_rand(),
                    'nns_queue_id'=>$queue_val['nns_id'],
                    'nns_value'=>$sp_val['nns_id'],
                    'nns_state'=>0,
                    'nns_create_time'=>$this->str_date_time,
                    'nns_modify_time'=>$this->str_date_time,
                );
            }
        }
        return $this->_check_all_params($queue_val, $arr_insert_data);
    }
    
    /**
     * CDN 注入
     * @param unknown $queue_val
     * @param string $params
     */
    public function epg_queue($queue_val,$params=null)
    {
        $result_sp = $this->_get_all_sp_info();
        if($result_sp['ret'] !=0 || !is_array($this->arr_sp_config) || empty($this->arr_sp_config))
        {
            return $result_sp;
        }
        $arr_insert_data = null;
        foreach ($this->arr_sp_config as $sp_key=>$sp_val)
        {
            if(isset($sp_val['nns_config']['disabled_epg']) && ($sp_val['nns_config']['disabled_epg'] == '0' || strlen($sp_val['nns_config']['disabled_epg']) <1) && isset($sp_val['nns_config']['epg_assets_enabled']) && is_array($sp_val['nns_config']['epg_assets_enabled']) && in_array($queue_val['type'], $sp_val['nns_config']['epg_assets_enabled']))
            {
                $arr_insert_data[$sp_val['nns_id']] = array(
                    'nns_id'=>np_guid_rand(),
                    'nns_queue_id'=>$queue_val['nns_id'],
                    'nns_value'=>$sp_val['nns_id'],
                    'nns_state'=>0,
                    'nns_create_time'=>$this->str_date_time,
                    'nns_modify_time'=>$this->str_date_time,
                );
            }
        }
        return $this->_check_all_params($queue_val, $arr_insert_data);
    }
    
    
    /**
     * CDN 注入
     * @param unknown $queue_val
     * @param string $params
     */
    public function sync_third_party_queue($queue_val,$params=null)
    {
        $result_sp = $this->_get_all_sp_info();
        if($result_sp['ret'] !=0 || !is_array($this->arr_sp_config) || empty($this->arr_sp_config))
        {
            return $result_sp;
        }
        $arr_insert_data = null;
        foreach ($this->arr_sp_config as $sp_key=>$sp_val)
        {
            if(isset($sp_val['nns_config']['cdn_notify_third_party_enable']) && $sp_val['nns_config']['cdn_notify_third_party_enable'] =='1' && isset($sp_val['nns_config']['cdn_notify_third_party_mark']) && strlen($sp_val['nns_config']['cdn_notify_third_party_mark'])>0)
            {
                $arr_insert_data[$sp_val['nns_id']] = array(
                    'nns_id'=>np_guid_rand(),
                    'nns_queue_id'=>$queue_val['nns_id'],
                    'nns_value'=>$sp_val['nns_id'],
                    'nns_state'=>0,
                    'nns_create_time'=>$this->str_date_time,
                    'nns_modify_time'=>$this->str_date_time,
                );
            }
        }
        return $this->_check_all_params($queue_val, $arr_insert_data);
    }
    
    
    /**
     * 消息队列分发到中心注入指令队列
     */
    public function delivery_import_message_queue($queue_val,$params=null)
    {
        $result_cp = $this->_get_all_cp_info();
        if($result_cp['ret'] !=0 || !is_array($this->arr_cp_config) || empty($this->arr_cp_config))
        {
            return $result_cp;
        }
        $arr_insert_data = null;
        foreach ($this->arr_cp_config as $cp_key=>$cp_val)
        {
            if(!isset($cp_val['nns_config']) || !is_array($cp_val['nns_config'])>0 || empty($cp_val['nns_config']))
            {
                continue;
            }
            if((isset($cp_val['nns_config']['message_http_import_enable']) && $cp_val['nns_config']['message_http_import_enable'] == '1') || 
                (isset($cp_val['nns_config']['message_queue_import_enable']) && $cp_val['nns_config']['message_queue_import_enable'] == '1') ||
                (isset($cp_val['nns_config']['message_ftp_import_enable']) && $cp_val['nns_config']['message_ftp_import_enable'] == '1') )
            {
                $arr_insert_data[$cp_val['nns_id']] = array(
                    'nns_id'=>np_guid_rand(),
                    'nns_queue_id'=>$queue_val['nns_id'],
                    'nns_value'=>$cp_val['nns_id'],
                    'nns_state'=>$cp_val['nns_state'],
                    'nns_create_time'=>$this->str_date_time,
                    'nns_modify_time'=>$this->str_date_time,
                );
            }
        }
        return $this->_check_all_params($queue_val, $arr_insert_data);
    }
}
$params = array(
    'request'=>$_REQUEST,
);
$arr_files = pathinfo(__FILE__);
$file_name = $arr_files['filename'];
$timer_execute = new timer_execute($file_name, $file_name,__FILE__);
$timer_execute->run($params);