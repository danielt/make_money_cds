<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/mgtv_init.php';
class timer_execute extends nn_timer
{
    public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->init($params);
        $this->msg('执行结束...');
    }
    
    /**
     * 初始化
     * @param string $params
     */
    function init($params = null)
    {
        $str_third_playbill_pool = $params["bk_queue_value"];
        if(strlen($str_third_playbill_pool) <1)
        {
            $this->msg("[redis节目单池中返回数据为空]".$str_third_playbill_pool);
            return ;
        }
        $str_mgtv_init = dirname(dirname(__FILE__))."/mgtv_v2/{$str_third_playbill_pool}/init.php";
        if(!file_exists($str_mgtv_init))
        {
            $this->msg("[初始化文件不存在]".$str_mgtv_init);
            return ;
        }
        include_once $str_mgtv_init;
        define('ORG_ID', $str_third_playbill_pool);
        $result_sp = $this->_get_sp_info($str_third_playbill_pool);
        if($result_sp['ret'] !=0)
        {
            $this->msg("sp信息获取spID[{$str_third_playbill_pool}]失败：".var_export($result_sp,true));
            return ;
        }
        if(isset($this->arr_sp_config[$str_third_playbill_pool]['nns_config']['disabled_epg']) && $this->arr_sp_config[$str_third_playbill_pool]['nns_config']['disabled_epg'] == '1')
        {
            $this->msg("sp信息获取spID[{$str_third_playbill_pool}]注入EPG开关关闭");
            return ;
        }
        if(!isset($this->arr_sp_config[$str_third_playbill_pool]['nns_config']['epg_assets_enabled']) || 
            !is_array($this->arr_sp_config[$str_third_playbill_pool]['nns_config']['epg_assets_enabled']) || 
            (is_array($this->arr_sp_config[$str_third_playbill_pool]['nns_config']['epg_assets_enabled']) && 
                !in_array('playbill', $this->arr_sp_config[$str_third_playbill_pool]['nns_config']['epg_assets_enabled'])))
        {
            $this->msg("sp信息获取spID[{$str_third_playbill_pool}]注入EPG节目单不允许注入");
            return ;
        }
        $queue_task_model = new queue_task_model();
        $sql = $queue_task_model->get_epg_sql(ORG_ID, 'playbill');
        $this->msg("查询对列数据SQL：{$sql}");
        if(empty($sql))
        {
            $this->msg("SQL 语句为空不在执行");
            return ;
        }
        $c2_list = nl_db_get_all($sql, $this->obj_dc->db());
        if(!is_array($c2_list) || count($c2_list)<1)
        {
            $this->msg("SQL查询无队列数据执行");
            return ;
        }
        if(isset($this->arr_sp_config[$str_third_playbill_pool]['nns_config']['epg_import_model']) && $this->arr_sp_config[$str_third_playbill_pool]['nns_config']['epg_import_model'] == '1')
        {
            include_once dirname(dirname(__FILE__)).'/mgtv_v2/models/import_model_v2.php';
            $arr_task_id = null;
            foreach($c2_list as $c2_info)
            {
                $arr_task_id[] = $c2_info['nns_id'];
            }
            $obj_import_model = new import_model_v2();
            $result=$obj_import_model->import_epg($arr_task_id);
            $this->msg("处理结果：".var_export($result,true));
        }
        else
        {
            foreach($c2_list as $c2_info)
            {
                $result = import_model::import_playbill($c2_info['nns_id']);
                $this->msg("task_id:[{$c2_info['nns_id']}]处理结果：".var_export($result,true));
            }
        }
    }
}
$arr_files = pathinfo(__FILE__);
$file_name = $arr_files['filename'];
$timer_execute = new timer_execute($file_name, ORG_ID,__FILE__,'epg_playbill');
$timer_execute->run();