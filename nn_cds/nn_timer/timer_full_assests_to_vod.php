<?php
/**
 * @description:定时器酷控全量媒资入库vod
 * @author:xinxin.deng
 * @date: 2017/9/20 14:23
 */
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(__FILE__)) . '/api_v2/nn_const.php';
include_once dirname(dirname(__FILE__)) . '/api_v2/common.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/nl_common.func.php';

class timer_execute extends nn_timer
{
    public $str_date = null;
    public $str_date_time = null;
    public $arr_full_assets_site = array();
    /**
     * 定时器初始化
     * @see nn_timer::action()
     */
    public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->init($params);
        $this->msg('执行结束...');
    }
    
    /**
     * 初始化
     */
    public function init($params=null)
    {
        include_once dirname(dirname(__FILE__)) . '/nn_logic/full_assets/full_assets.class.php';
        include_once dirname(dirname(__FILE__)) . '/api_v2/control/standard/sync_source.php';
        $str_third_playbill_pool = $params["bk_queue_value"];
        if(strlen($str_third_playbill_pool) <1 || $str_third_playbill_pool != 'thrid_full_assests_to_vod')
        {
            $this->msg("[redis节目单池中返回数据为空]".$str_third_playbill_pool);
            return ;
        }
        $this->arr_params['bk_queue_config']['nns_ext_info']['data_num'] = 1;
        $result_query = nl_full_assets::timer_query_v2($this->obj_dc, $this->arr_params['bk_queue_config']['nns_ext_info']['data_num'], $this->arr_params['bk_queue_config']['nns_ext_info']['execute_num']);
        if($result_query['ret'] !=0)
        {
            $this->msg(var_export($result_query,true));
            return ;
        }
        if(!isset($result_query['data_info']) || !is_array($result_query['data_info']) || empty($result_query['data_info']))
        {
            $this->msg('sql查询到任何队列需要执行的数据：'.var_export($result_query,true));
            return ;
        }

        $result_query = $result_query['data_info'];
        return $this->add_asset($result_query);
    }

    /**
     * @description:添加媒资
     * @author:xinxin.deng
     * @date: 2017/9/20 14:07
     * @param $result_query
     */
    public function add_asset($result_query)
    {
        $temp_arr_data=array();
        foreach ($result_query as $query_val)
        {
            //添加操作次数
            $result_edit = nl_full_assets::edit_sql($this->obj_dc, "update nns_full_assets set nns_execute_time=nns_execute_time+1,nns_modify_time='".date("Y-m-d H:i:s")."' where nns_id='{$query_val['nns_id']}'");
            if(isset($temp_arr_data[$query_val['nns_cp_id']]))
            {
                if(empty($temp_arr_data[$query_val['nns_cp_id']]) || !is_array($temp_arr_data[$query_val['nns_cp_id']]))
                {
                    continue;
                }
            }
            else
            {
                $result_cp = nl_cp::query_by_id($this->obj_dc, $query_val['nns_cp_id']);
                if($result_cp['ret'] !=0 || !isset($result_cp['data_info']) || !is_array($result_cp['data_info']) || empty($result_cp['data_info']))
                {
                    continue;
                }
                $temp_arr_data[$query_val['nns_cp_id']]['nns_name'] = $result_cp['data_info']['nns_name'];
                $temp_arr_data[$query_val['nns_cp_id']]['nns_config'] = (isset($result_cp['data_info']['nns_config'])
                    && strlen($result_cp['data_info']['nns_config']) >0) ? json_decode($result_cp['data_info']['nns_config'],true) : null;
            }
            //得到媒资数据
            $arr_asset = json_decode(base64_decode($query_val['nns_content']),true);
            if(isset($arr_asset) && !empty($arr_asset) && is_array($arr_asset))
            {
                $temp_array_key = array_keys($arr_asset);
                if(is_array($temp_array_key) && !empty($temp_array_key))
                {
                    $temp_array_key = array_intersect($temp_array_key, array('video','index','media'));
                    if(count($temp_array_key) !=3)
                    {
                        $edit_data = array(
                            'nns_state'=> 1,
                        );
                        $result_edit = nl_full_assets::edit($this->obj_dc, $edit_data, $query_val['nns_id']);
                        continue;
                    }
                }
                $result_indatabase = $this->in_database($arr_asset,$query_val['nns_cp_id'],$temp_arr_data);
                if($result_indatabase['ret'] == 0)
                {
                    $edit_data = array(
                        'nns_state'=> 2,
                    );
                    $result_edit = nl_full_assets::edit($this->obj_dc, $edit_data, $query_val['nns_id']);
                }
                else
                {
                    $this->msg('入库失败，数据有问题啊,媒资id为：' . $query_val['nns_id'] . '错误原因为：' . addslashes($result_indatabase['reason']));
                    $edit_data = array(
                        'nns_state'=>3,
                        'nns_reason'=>addslashes($result_indatabase['reason']),
                    );
                    $result_edit = nl_full_assets::edit($this->obj_dc, $edit_data, $query_val['nns_id']);
                }
            }
            else
            {
                $this->msg('执行结果：数据有问题啊,媒资id为：' . $query_val['nns_id']);
                $edit_data = array(
                    'nns_state' => '3',
                    'nns_reason'=>'数据有问题啊',
                );
                $result_edit = nl_full_assets::edit($this->obj_dc, $edit_data, $query_val['nns_id']);
            }
            if($result_edit['ret'] !=0)
            {
                $this->msg(var_export($result_edit,true));
            }
        }
        return;
    }

    /**
     * @description:数据入库
     * @author:xinxin.deng
     * @date: 2017/9/20 13:54
     * @param $asset_data
     * @param $cp_id
     * @param $temp_arr_data
     * @return array|null
     */
    public function in_database($asset_data,$cp_id,$temp_arr_data)
    {
        foreach ($asset_data as $asset_key=>$asset_val)
        {
            switch ($asset_key)
            {
                case 'video':
                    foreach ($asset_val as $val)
                    {
                        $obj_sync = new standard_sync_source(array('cp_id'=>$cp_id,'cp_name'=>$temp_arr_data[$cp_id]['nns_name']),$temp_arr_data[$cp_id]['nns_config'],'',$cp_id);
                        $obj_sync->import->download_img_enabled = true;//进行图片下载
                        $obj_sync->no_need_change_info = false;
                        $obj_sync->fuse_enabled = true;//不进行补全
                        $re = $obj_sync->modify_asset($val['data'],$val['video_id']);
                        if($re['ret'] !=0)
                        {
                            return $re;
                        }
                        unset($obj_sync);
                    }
                    break;
                case 'index':
                    foreach ($asset_val as $val)
                    {
                        $obj_sync = new standard_sync_source(array('cp_id'=>$cp_id,'cp_name'=>$temp_arr_data[$cp_id]['nns_name']),$temp_arr_data[$cp_id]['nns_config'],'',$cp_id);
                        $obj_sync->import->download_img_enabled = true;
                        $obj_sync->no_need_change_info = false;
                        $obj_sync->fuse_enabled = true;//不进行补全
                        $re = $obj_sync->modify_video_clip($val['data'],$val['video_id'],$val['index_id']);
                        if($re['ret'] !=0)
                        {
                            return $re;
                        }
                        unset($obj_sync);
                    }
                    break;
                case 'media':
                    foreach ($asset_val as $val)
                    {
                        $obj_sync = new standard_sync_source(array('cp_id'=>$cp_id,'cp_name'=>$temp_arr_data[$cp_id]['nns_name']),$temp_arr_data[$cp_id]['nns_config'],'',$cp_id);
                        $obj_sync->import->download_img_enabled = true;
                        $obj_sync->no_need_change_info = false;
                        $obj_sync->fuse_enabled = true;//不进行补全
                        $re = $obj_sync->import_video_file($val['data'],$val['video_id'],$val['index_id'],$val['media_id']);
                        if($re['ret'] !=0)
                        {
                            return $re;
                        }
                        unset($obj_sync);
                    }
                    break;
            }
            
        }
        return $this->_return_data(0,'ok');
    }
}
$arr_files = pathinfo(__FILE__);
$file_name = $arr_files['filename'];
$timer_execute = new timer_execute($file_name, $file_name,__FILE__,'thrid_full_assests_to_vod');
$timer_execute->run();
