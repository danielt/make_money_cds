<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . "/v2/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load_old("nn_logic/queue/queue.class.php");

/**
 * Class timer_execute
 * 后台多进程运行定时器
 */
class timer_execute
{
    private $arr_queue_pool = array();
    private $path_php_exe;

    public function __construct()
    {
        $this->path_php_exe = evn::get("php_execute_path");
        $this->arr_queue_pool = array(
            1 => 'thrid_playbill',
            2 => 'del_thrid_playbill',
            3 => 'del_playbill_task',
            4 => 'thrid_full_assests',
            5 => 'message',
            6 => 'import',
            7 => 'clip_file',
            8 => 'file_encode',
            9 => 'clip_to_encode',
            10 => 'queue_to_encode',
            11 => 'cdn_video',
            12 => 'cdn_index',
            13 => 'cdn_media',
            14 => 'cdn_live',
            15 => 'cdn_live_media',
            16 => 'cdn_playbill',
            17 => 'cdn_file',
            18 => 'epg_video',
            19 => 'epg_index',
            20 => 'epg_media',
            21 => 'epg_live',
            22 => 'epg_live_media',
            23 => 'epg_playbill',
            24 => 'thrid_full_assests_v2',
            25 => 'thrid_full_assests_v3',
            26 => 'thrid_full_assests_to_vod',
            27 => 'ftp_media_to_db',
            28 => 'ftp_media_to_db_nmxl',
            29 => 'playbill_to_localfile',
            30 => 'sync_third_party',
            31 => 'delivery_import_message',
        );
    }

    public function init()
    {
        $result_queue = nl_queue::query_by_condition(m_config::get_dc(), array('nns_state' => 0));

        if ($result_queue['ret'] != 0 || !isset($result_queue['data_info']) || empty($result_queue['data_info']) || !is_array($result_queue['data_info'])) {
            return m_config::return_data(1, '获取对列池数据失败');
        }
        $result_queue = $result_queue['data_info'];

        $str_exe = $this->path_php_exe . ' -f ' . dirname(__FILE__) . '/timer_public.php ';
        foreach ($result_queue as $queue_value) {

            if (!in_array($queue_value['nns_queue'], $this->arr_queue_pool)) {
                m_config::return_data(1, "队列{$queue_value['nns_queue']}不是合法队列");
            }

            $timer_file = dirname(__FILE__) . "timer_{$queue_value['nns_queue']}";

            //定时器文件不存在
            if (!file_exists($timer_file)) {
                m_config::return_data(1, "队列{$queue_value['nns_queue']}的定时器文件未找到");
            }

            if (substr(php_uname(), 0, 7) != "Windows") {
                //LINUX下执行
                $str_order = "{$str_exe}  {$queue_value['nns_queue']} {$timer_file} > /dev/null &";
            } else {
                //windows下执行
                $str_order = "{$str_exe}  {$queue_value['nns_queue']} {$timer_file} ";
            }
            usleep(50000);
            exec($str_order);
        }
        return m_config::return_data(0, "OK");
    }

    /**
     * @param $nns_queue
     * @param $str_base_dir
     * 执行特定定时器
     */
    public function exec($nns_queue, $timer_file)
    {

        $str_exe = $this->path_php_exe . ' -f ' . $timer_file;
        if (substr(php_uname(), 0, 7) != "Windows") {
            //LINUX后台运行(php后台进程),不会堵塞
            $str_order = "{$str_exe}  {$nns_queue}> /dev/null &";
        } else {
            //windows下执行
            $str_order = "{$str_exe} {$nns_queue} ";
        }
        //执行指定定时器
        exec($str_order);
    }
}

//有参数则执行带参数特定定时器,如果没有指定则执行队列相关所有定时器
if (!empty($argv[1]) && !empty($argv[2])) {
    $timer_execute = new timer_execute();
    $result = $timer_execute->exec($argv[1], $argv[2]);
} else {
    $timer_execute = new timer_execute();
    $result = $timer_execute->init();
}

