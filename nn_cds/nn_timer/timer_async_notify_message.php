<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 0);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/notify_message/notify_message.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/np/np_http_curl.class.php';
class timer_async_notify_message extends nn_timer
{
    //反馈条数
    public $notify_num = 100;

    public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->do_timer_action();
        $this->msg('执行结束...');
    }
    /**
     * 脚本执行体
     * @author zhiyong.luo
     */
    private function do_timer_action()
    {
        $dc = nl_get_dc(array (
            "db_policy" => NL_DB_WRITE | NL_DB_READ,
            "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
        ));
        $dc->open();
        //获取反馈消息
        $params = array(
            'nns_status' => 0,
        );
        $notify_list = nl_notify_message::get_nofity_message($dc,$params,0,$this->notify_num);
        if($notify_list['ret'] != 0)
        {
            echo "不存在反馈的消息\r\n";
            return ;
        }
        if(empty($notify_list['data_info']))
        {
            echo "不存在反馈的消息\r\n";
            return ;
        }

        foreach($notify_list['data_info'] as $value){
            //先判断有无sp
            if(empty($value['nns_org_id']))
            {
                $cp_info = nl_cp::query_by_id($dc, $value['nns_cp_id']);
                $cp_config = isset($cp_info['data_info']['nns_config']) && !empty($cp_info['data_info']['nns_config']) ? json_decode($cp_info['data_info']['nns_config'], true) : null;
                if (!isset($cp_config['site_callback_url']) && strlen($cp_config['site_callback_url']) <= 0)
                {
                    nn_log::write_log_message(LOG_MODEL_NOTIFY,"CP:" . $value['nns_cp_id'] . "没有配置反馈地址", $value['nns_cp_id']);
                    continue;
                }
                else
                {
                    $site_callback_url = $cp_config['site_callback_url'];
                }
            }
            else
            {
                $sp_info = nl_sp::query_by_id($dc, $value['nns_org_id']);
                $sp_config = isset($sp_info['data_info'][0]['nns_config']) && !empty($sp_info['data_info'][0]['nns_config']) ? json_decode($sp_info['data_info'][0]['nns_config'], true) : null;
                if (isset($sp_config['message_feedback_mode']) && ($sp_config['message_feedback_mode'] == 1))
                {//取用cp
                    $cp_info = nl_cp::query_by_id($dc, $value['nns_cp_id']);
                    $cp_config = isset($cp_info['data_info']['nns_config']) && !empty($cp_info['data_info']['nns_config']) ? json_decode($cp_info['data_info']['nns_config'], true) : null;
                    if (!isset($cp_config['site_callback_url']) && strlen($cp_config['site_callback_url']) <= 0)
                    {
                        nn_log::write_log_message(LOG_MODEL_NOTIFY,"CP:" . $value['nns_cp_id'] . "没有配置反馈地址", $value['nns_cp_id']);
                        continue;
                    }
                    else
                    {
                        $site_callback_url = $cp_config['site_callback_url'];
                    }
                }
                else
                {
                    if (!isset($sp_config['site_callback_url']) && strlen($sp_config['site_callback_url']) <= 0)
                    {
                        nn_log::write_log_message(LOG_MODEL_NOTIFY,"SP:" . $value['nns_org_id'] . "没有配置反馈地址",  $value['nns_org_id']);
                        continue;
                    }
                    else
                    {
                        $site_callback_url = $sp_config['site_callback_url'];
                    }
                }
            }
            $result = $this->curl_post($site_callback_url,$value['nns_notify']);
            if($result['code'])
            {//删除反馈消息 写入日志
                nl_notify_message::delete_nofity_message($dc,$value['nns_id']);
                nn_log::write_log_message(LOG_MODEL_NOTIFY, '反馈的xml数据为'.$value['nns_notify'].'POST反馈数据返回为：' . var_export($result['curl_info'],true) .'请求的url为' . $site_callback_url, $value['nns_cp_id']);
            }
            else
            {//反馈失败，添加反馈重试次数 修改修改时间
                $where['nns_id']=$value['nns_id'];
                if($value['nns_again']>=5)
                {//超过5次 认定为反馈失败
                    $fail['nns_again']=$value['nns_again'] +1;
                    $fail['nns_status']='-1';
                    $fail['nns_modify_time']=date('Y-m-d H:i:s',time());
                }
                else
                {//低于5次 继续反馈
                    $fail['nns_again']=$value['nns_again'] +1;
                    $fail['nns_modify_time']=date('Y-m-d H:i:s',time());
                }
                nl_notify_message::modify_notify_message($dc,$fail,$where);
            }
        }
    }
    /**
     * 异步回调
     * @param unknown $url
     * @param unknown $content
     */
    private function curl_post($url,$content)
    {
        $http_curl = new np_http_curl_class();
        //$http_header = array("Content-Type: text/xml");
        $response = $http_curl->post($url,array('cmsresult' => $content),null,5);
        $curl_info = $http_curl->curl_getinfo();
        $http_code = $curl_info['http_code'];
        if($http_code != 200)
        {
            $result['code']=false;
            return $result;
        }
        else
        {
            $result['code'] = true;
            $result['curl_info'] = $curl_info;
            return $result;
        }
    }
}
$async_notify_message = new timer_async_notify_message("async_notify_message", 'public',__FILE__);
$async_notify_message->run();