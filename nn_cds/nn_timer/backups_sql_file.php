<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hushengs
 * Date: 2017/10/12
 * Time: 17:29
 */
header("Content-Type:text/html;charset=utf-8");
date_default_timezone_set("Asia/Hong_Kong");
set_time_limit(0);
echo "开始时间".date('Y_m_d h:i:s')."<br/>";
include dirname(dirname(__FILE__)).'/nn_logic/nl_common.func.php';
$str_table_name = "nns_mgtvbk_message,nns_mgtvbk_c2_log,nns_mgtvbk_import_epg_log";
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$arr_table = explode(',',$str_table_name);
if(is_array($arr_table) && !empty($arr_table))
{
    foreach ($arr_table as $value_table_name)
    {
        $flag = 1;
        $table_name = $value_table_name;
        $cut_flag = 1;
        $time = date('Y-m-d');
        $count_total_sql = "select count(*) as count_total from $table_name where nns_create_time <= DATE_ADD('{$time}',INTERVAL - 90 day) ";
        $result = nl_query_by_db($count_total_sql, $dc->db());
        echo "总共应处理".$result[0]['count_total']."条<br/>";
        while($flag==1)
        {
            $sql = "select * from $table_name where nns_create_time <= DATE_ADD('{$time}',INTERVAL - 90 day) limit 5000 ";
            $result = nl_query_by_db($sql, $dc->db());
            if(is_array($result) && !empty($result))
            {
                foreach($result as $val)
                {
                    if(is_array($val))
                    {
                        $count_total = count($val);
                        $count = 1;
                        $val_bu = '';
                        foreach ($val as $key=>$true_value)
                        {
                            if($key == 'nns_id')
                            {
                                $delete_val = $true_value;
                            }
                            if($count!=$count_total)
                            {
                                $val_bu .=addslashes($true_value)."'".','."'";
                            }
                            else
                            {
                                $val_bu .=addslashes($true_value)."'";
                            }
                            $count++;
                        }
                        $insert_sql ="insert into ".$table_name." value('".$val_bu.");";
                        $file_dir = dirname(dirname(__FILE__))."/data/backup_sql/";
                        if(!is_dir($file_dir))
                        {
                            mkdir($file_dir,true);
                        }
                        $file_name = date('Ymd')."_".$table_name."_".$cut_flag.".sql";
                        file_put_contents($file_dir.$file_name,$insert_sql."\n",FILE_APPEND);
                        unset($insert_sql);
                        $delete_sql = "delete from $table_name where nns_id='"."{$delete_val}"."'";
                        nl_execute_by_db($delete_sql,$dc->db());
                    }
                }
            }
            $count_sql = "select count(*) as count_total from $table_name where nns_create_time <= DATE_ADD('{$time}',INTERVAL - 90 day) ";
            $result = nl_query_by_db($count_sql, $dc->db());
            echo "剩余待处理".$result[0]['count_total']."条<br/>";
            if($result[0]['count_total']==0)
            {
                $flag = 2;
            }
            else
            {
                $cut_flag++;
            }
        }
        echo $table_name."备份清理完毕,休眠1秒<br/>";
        sleep(1);
        unset($cut_flag);
    }
}
else
{
    echo "table信息配置错误";
}
echo "结束时间".date('Y_m_d h:i:s');