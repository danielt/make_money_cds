<?php
/**
 * 定时器全量媒资补全
 * pan.liang
 */
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(__FILE__)) . '/api_v2/nn_const.php';
include_once dirname(dirname(__FILE__)) . '/api_v2/common.php';
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/models/queue_task_model.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/clip_task/clip_task.class.php';

class timer_execute extends nn_timer
{
    public $str_date = null;
    public $str_date_time = null;
    public $arr_full_assets_site = array();
    
    /**
     * 定时器初始化
     * @see nn_timer::action()
     */
    public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->init($params);
        $this->msg('执行结束...');
    }
    
    /**
     * 初始化
     */
    public function init($params=null)
    {
        $str_third_playbill_pool = $params["bk_queue_value"];
        if(strlen($str_third_playbill_pool) <1)
        {
            $this->msg("[redis节目单池中返回数据为空]".$str_third_playbill_pool);
            return ;
        }
        $str_mgtv_init = dirname(dirname(__FILE__))."/mgtv_v2/{$str_third_playbill_pool}/init.php";
        if(!file_exists($str_mgtv_init))
        {
            $this->msg("[初始化文件不存在]".$str_mgtv_init);
            return ;
        }
        include_once $str_mgtv_init;
        define('ORG_ID', $str_third_playbill_pool);
        $result_sp = $this->_get_sp_info($str_third_playbill_pool);
        if($result_sp['ret'] !=0)
        {
            $this->msg("sp信息获取spID[{$str_third_playbill_pool}]失败：".var_export($result_sp,true));
            return ;
        }
        $max_num = (isset($this->arr_sp_config[$str_third_playbill_pool]['nns_config']['clip_pre_max']) && (int)$this->arr_sp_config[$str_third_playbill_pool]['nns_config']['clip_pre_max']>0) ? (int)$this->arr_sp_config[$str_third_playbill_pool]['nns_config']['clip_pre_max'] : 30;
        $result_wait = nl_clip_task::timer_get_wait_clip_data($this->obj_dc,$str_third_playbill_pool);
        if($result_wait['ret'] !=0)
        {
            $this->msg("获取等待切片数据数量失败：".var_export($result_wait,true));
            return ;
        }
        $result_wait['data_info'] = (int)$result_wait['data_info'];
        if($max_num > $result_wait['data_info'])
        {
            $ex_num = $max_num-$result_wait['data_info'];
            $ex_num = $ex_num >0 ? $ex_num : 5;
            $this->msg("spID[{$str_third_playbill_pool}],等待切片的数量[{$result_wait['data_info']}],需要下发切片数据的数量[{$max_num}],最终执行的数量[{$ex_num}]");
            $queue_task_model = new queue_task_model();
            $queue_task_model->q_get_clip_task($str_third_playbill_pool,$ex_num);
        }
        else
        {
            $this->msg("spID[{$str_third_playbill_pool}]切片数量达到上限：等待切片的数量[{$result_wait['data_info']}],需要下发切片数据的数量[{$max_num}]");
        }
        return ;
    }
}
$arr_files = pathinfo(__FILE__);
$file_name = $arr_files['filename'];
$timer_execute = new timer_execute($file_name, $file_name,__FILE__,'clip_file');
$timer_execute->run();
