<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(__FILE__)) .'/nn_class/file_encode/make_file_encode.class.php';
class timer_execute extends nn_timer
{
    public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->init($params);
        $this->msg('执行结束...');
    }
    
    /**
     * 初始化
     */
    function init($params =null)
    {
        $obj_dc= nl_get_dc(array (
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
        $str_clip_to_encode_pool = $params["bk_queue_value"];
        if(strlen($str_clip_to_encode_pool) <1)
        {
            $this->msg("[redis转码池中返回数据为空]".$str_clip_to_encode_pool);
            return ;
        }
        $str_mgtv_init = dirname(dirname(__FILE__))."/mgtv_v2/{$str_clip_to_encode_pool}/init.php";
        if(!file_exists($str_mgtv_init))
        {
            $this->msg("[初始化文件不存在]".$str_mgtv_init);
            return ;
        }
        include_once $str_mgtv_init;
        define('ORG_ID', $str_clip_to_encode_pool);
        $obj_make_file_encode = new make_file_encode_queue($obj_dc);
        $task_info = nl_clip_task::query_by_params($this->obj_dc, array('nns_org_id'=>ORG_ID,'nns_state'=>'clip_encode_wait'),30);
        if($task_info['ret'] !=0 || !isset($task_info['data_info']) || empty($task_info['data_info']) || !is_array($task_info['data_info']))
        {
            $this->msg("处理结果：".var_export($task_info,true));
            return ;
        }
        foreach ($task_info['data_info'] as $val)
        {
            $result = $obj_make_file_encode->init(ORG_ID,$val['nns_id']);
            $this->msg("处理结果：".var_export($result,true));
        }
    }
}
$arr_files = pathinfo(__FILE__);
$file_name = $arr_files['filename'];
$timer_execute = new timer_execute($file_name, ORG_ID,__FILE__,'clip_to_encode');
$timer_execute->run();



