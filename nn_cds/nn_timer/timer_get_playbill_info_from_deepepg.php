<?php
/**
 * 更新频道绑定的第三方频道节目单信息
 * @author ye.zhong
 * @date 2017-05-13
 */
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/nn_logic/message/nl_message.class.php';
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(__FILE__)) . '/api_v2/nn_const.php';
include_once dirname(dirname(__FILE__)) . '/api_v2/common.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/cache/view_cache.class.php';
include_once dirname(dirname(__FILE__)) . '/nn_cms_manager/service/asset_import/import_assets_proc.class.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/live/live.class.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/live/live_media.class.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/live/playbill.class.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/live/third_live.class.php';
include_once dirname(dirname(__FILE__)) . '/api_v2/control/standard/sync_source.php';
//引入np类文件
include_np('np_string');
define("IS_LOG_TIMER_OPERATION", true);
class timer_get_playbill_info_from_deepepg extends nn_timer
{
	public function action($params = null)
	{
		$this->msg('开始执行...');
		$this->do_timer_action($params);
		$this->msg('执行结束...');
	}


    public function do_timer_action($params = null)
    {
        $str_third_playbill_pool = $params["bk_queue_value"];
        if(!$this->has_length($str_third_playbill_pool))
        {
            echo "redis节目单池中返回数据为空".'<br/>';
            nn_log::write_log_message(LOG_MODEL_MESSAGE, "[redis节目单池中返回数据为空]".$str_third_playbill_pool, "get_playbill_info");
            return false;
        }
        $arr_guid_time = explode("|#|", $str_third_playbill_pool);
        $str_third_live_guid =$arr_guid_time[0];
        $str_third_live_time =$arr_guid_time[1];



        //>>1 取配置信息以及验证配置信息
        $arr_config_result = $this->validate_config_info();
        if($arr_config_result["ret"] != 0)
        {
            echo $arr_config_result["reason"] ."\n";
            return false;
        }
        $arr_config = $arr_config_result["data"];
        $arr_config["playbill_days"] = $str_third_live_time;

        //>>2 获取回看类型频道
        $dc = nl_get_dc(array(
            "db_policy" => NL_DB_WRITE|NL_DB_READ,
            "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
        ));


        //获取回看频道类型绑定的第三方id
        $arr_res_live = nl_third_live::query_by_id($dc,$str_third_live_guid);
        if ($arr_res_live['ret'] != 0)
        {
            nn_log::write_log_message(LOG_MODEL_MESSAGE, "[查询回看频道失败]".$arr_res_live['reason'], "get_playbill_info");
            return false;
        }
        $arr_third_live = $arr_res_live["data_info"];
        $arr_live_info = nl_live::query_by_id($dc,$arr_third_live["nns_live_id"]);
        if ($arr_live_info['ret'] != 0)
        {
            nn_log::write_log_message(LOG_MODEL_MESSAGE, "[查询绑定频道失败]".$arr_res_live['reason'], "get_playbill_info");
            return false;
        }
        $str_cp_id = $arr_live_info["data_info"]["nns_cp_id"];
        $obj_img_handle = new import_assets_class();
        $obj_img_handle->cp_id = $str_cp_id;
        //根据第三方id更新节目单信息

        $get_playbill_url = $arr_config_result["data"]["main_info_url"] .'&id='.$arr_third_live['nns_third_live_id'];
        $mysql_playbill_by_id = nl_playbill::get_playbill_by_day($dc,$arr_third_live["nns_live_id"],$str_third_live_time);
        if ($mysql_playbill_by_id['ret'] != 0)
        {
            echo $mysql_playbill_by_id['reason'] ."\n";
            nn_log::write_log_message(LOG_MODEL_MESSAGE, $mysql_playbill_by_id['reason'], "get_playbill_info");
        }
        else if ($mysql_playbill_by_id['data_info'] === true)
        {
            $mysql_playbill_by_id = array();
        }
        else
        {
            $mysql_playbill_by_id = np_array_rekey($mysql_playbill_by_id['data_info'], 'nns_playbill_import_id');
        }
        //缓存不存在，需要重新获取节目单信息
        $obj_curl = new public_sync_source();
        $xml_playbill_main_info = $obj_curl->public_get_curl_content($get_playbill_url,30);
        $xml_arr = xml2array::getArrayData($xml_playbill_main_info["data_info"]);
        if ($xml_arr['ret'] != 1)
        {
            nn_log::write_log_message(LOG_MODEL_MESSAGE, $xml_arr['reason'], "get_playbill_info");
            return false;
        }
        $arr_playbill_info = $this->get_third_playbill_list_by_arr_xml($dc, $arr_third_live["nns_live_id"],$xml_arr,$arr_config,$obj_img_handle);
        if ($arr_playbill_info['ret'] != 0)
        {
            echo "频道【{$arr_third_live["nns_live_id"]}】绑定的第三方频道【{$arr_third_live['nns_third_live_id']}】解析节目详情XML失败".'<br/>';
            nn_log::write_log_message(LOG_MODEL_MESSAGE, $arr_playbill_info['reason'], "get_playbill_info");
            return false;
        }
        $arr_playbill_info = $arr_playbill_info['data'];
        $result = $this->compare_mysql_adi_info($dc, $mysql_playbill_by_id, $arr_playbill_info);
        echo "频道【{$arr_third_live["nns_live_id"]}】.绑定的第三方频道【{$arr_third_live['nns_third_live_id']}】".$result['reason'].'<br/>';
        return true;
    }

    /**
     * 判断字符串是否有值（空字符串不算值）
     * @author <feijian.gao@starcpr>
     * @date   2017年7月18日11:19:19
     * @param  string $string     需要检查的字符串
     * @return bool   false|true  返回过期时间
     */
    function has_length($string)
    {
        if(!isset($string))
        {
            return false;
        }
        $string = trim($string);
        if(strlen($string)==0)
        {
            return false;
        }
        return true;
    }

    /**
     * 验证配置信息
     * @author <feijian.gao@starcor>
     * @date   2017年7月18日14:27:41
     * @return array array('ret'=>返回状态码,'reason'=>'返回说明',data=>array('main_info_url'=>'','describe_info_url'=>''))
     */
    public function validate_config_info()
    {
        $g_playbill_info_completion_url = $this->arr_params["bk_queue_config"]['nns_ext_info']["playbill_url"];
        $g_playbill_info_description_url = $this->arr_params["bk_queue_config"]['nns_ext_info']["deep_playbill_url"];
        $g_get_playbill_info_for_days = $this->arr_params["bk_queue_config"]['nns_ext_info']["max_day"];
        $str_main_info_url = '';
        $str_describe_info_url = '';
        $arr_config_info = array();

        //取节目单时间
        $int_playbill_days = isset($g_get_playbill_info_for_days) ? $g_get_playbill_info_for_days : 7;
        $int_playbill_days = intval($int_playbill_days);

        //验证地址有效
        $obj_curl = new public_sync_source();
        $arr_result = $obj_curl->public_get_curl_content($g_playbill_info_completion_url,30);
        if($arr_result["ret"] != 0)
        {
            return $this->build_return(1,"[无效url]{$g_playbill_info_completion_url}");
        }
        $str_main_info_url = $g_playbill_info_completion_url;
        $arr_result = $obj_curl->public_get_curl_content($g_playbill_info_description_url,30);
        if($arr_result["ret"] != 0)
        {
            return $this->build_return(1,"[无效url]{$g_playbill_info_description_url}");
        }
        $str_describe_info_url = $g_playbill_info_description_url;

        unset($g_playbill_info_completion_url,$g_playbill_info_description_url,$g_assets_info_completion_time);
        unset($obj_curl);

        //返回验证信息
        $arr_config_info["main_info_url"] = $str_main_info_url;
        $arr_config_info["describe_info_url"] = $str_describe_info_url;
        $arr_config_info["playbill_days"] = $int_playbill_days;
        return $this->build_return(0,"验证成功",$arr_config_info);
    }

    /**
     * 构建返回参数
     * @author <feijian.gao@starcor>
     * @date	2017年7月18日12:13:12
     * @param int    $int_ret    返回码
     * @param string $str_info	 描述信息
     * @param mixed  $mixed_data 返回数据
     * @return array array("ret"=>返回码0成功其余失败,"reason"=>"返回描述","data"=>"数组或者字符串");
     */
    public function build_return($int_ret = 0, $str_info = "", $mixed_data = "")
    {
        $arr_return_data = array();
        if(intval($int_ret) == 0)
        {
            return array("ret"=>0,"reason"=>"成功","data"=>$mixed_data);
        }
        $arr_return_data["ret"] = intval($int_ret);
        $arr_return_data["reason"] = $str_info;
        $arr_return_data["data"] = $mixed_data;
        return $arr_return_data;
    }

    /**
     * 获取第三方节目单信息
     * @param $dc
     * @param $str_live_id
     * @param $xml_arr
     * @param array $arr_config
     * @param null $obj_img_handle
     * @return array
     */
    public function get_third_playbill_list_by_arr_xml($dc, $str_live_id, $xml_arr, $arr_config=array(),$obj_img_handle=null)
    {
        $arr_list = array();
        $arr_schedules = $xml_arr['data']['document']["children"];
        //循环所有天数的节目单
        foreach ($arr_schedules as $arr_schedule)
        {
            //对一天的节目单信息进行解析
            $date = $arr_schedule['attributes']['date'];
            if(strtotime($date) != strtotime($arr_config["playbill_days"]))
            {
                continue;
            }
            foreach($arr_schedule["children"] as $arr_event)
            {
                $start_time = '00:00';
                $end_time = '00:00';
                $title = '';
                //对一天中每个节目单信息进行处理，包括节目单信息补全
                $title = str_replace("\\", '', $arr_event['children']["title"]["content"]);
                $title = htmlspecialchars($title, ENT_QUOTES);
                //节目唯一id,为了重组节目单注入id。 xinxin.deng 2018/7/12 13:52 start.
                $playbill_unique_id = $arr_event['attributes']['id'];
                //xinxin.deng 2018/7/12 13:52 end
                $program_id = $arr_event['children']["program_id"]["content"];
                $start_time = $arr_event['children']["start_time"]["content"].':00';
                $end_time = $arr_event['children']["end_time"]["content"].':00';
                $date_start = $date.' '.$start_time;
                $date_end = $date.' '.$end_time;
                if (strtotime($start_time) > strtotime($end_time))
                {
                    $date_end = date('Y-m-d H:i:s',strtotime($date_end)+86400);
                }
                $time_len = strtotime($date_end) - strtotime($date_start);
                //获取节目描述信息
                $arr_third_p_info =  array(
                    'nns_name' => $title,
                    'nns_begin_time' => $date_start,
                    'nns_time_len' => $time_len,
                    'nns_live_id' => $str_live_id,
                );

                if (!$this->has_length($program_id))
                {
                    $desc_arr['program_desc'] = '';
                    $desc_arr['pic'] = array();
                    $desc_arr['actor'] = '';
                    $desc_arr['director'] = '';
                    $desc_arr["kind"] = "";

                }
                else
                {
                    $third_playbill_desc_url = $arr_config["describe_info_url"] .'&id='.$program_id;
                    $obj_curl = new public_sync_source();
                    $xml = $obj_curl->public_get_curl_content($third_playbill_desc_url,30);
                    $xml_arr = xml2array::getArrayData($xml["data_info"]);
                    if ($xml_arr['ret'] != 1)
                    {
                        return $this->build_return(1,$xml_arr['reason']);
                    }
                    $desc_arr = $this->get_desc_by_program_arr($xml_arr,$obj_img_handle);
                }
                $arr_temp_image = array();
                $arr_third_p_info['nns_summary'] = isset($desc_arr['program_desc']) ? addslashes($desc_arr['program_desc']) : '';
                if(isset($desc_arr['pic']['horizontal_img']) && strlen($desc_arr['pic']['horizontal_img'])>0)
                {
                    $arr_third_p_info['nns_image0'] = $desc_arr['pic']['horizontal_img'];
                    if(isset($arr_third_p_info['nns_image0']) && strlen($arr_third_p_info['nns_image0']) > 0)
                    {
                        $arr_temp_image[] = $arr_third_p_info['nns_image0'];
                    }
                    unset($desc_arr['pic']['horizontal_img']);
                }
                if(isset($desc_arr['pic']['verticality_img']) && strlen($desc_arr['pic']['verticality_img'])>0)
                {
                    $arr_third_p_info['nns_image2'] = $desc_arr['pic']['verticality_img'];
                    if(isset($arr_third_p_info['nns_image2']) && strlen($arr_third_p_info['nns_image2']) > 0)
                    {
                        $arr_temp_image[] = $arr_third_p_info['nns_image2'];
                    }
                    unset($desc_arr['pic']['verticality_img']);
                }
                if(isset($desc_arr['pic']['square_img']) && strlen($desc_arr['pic']['square_img'])>0)
                {
                    $arr_third_p_info['nns_image1'] = $desc_arr['pic']['square_img'];
                    if(isset($arr_third_p_info['nns_image1']) && strlen($arr_third_p_info['nns_image1']) > 0)
                    {
                        $arr_temp_image[] = $arr_third_p_info['nns_image1'];
                    }
                    unset($desc_arr['pic']['square_img']);
                }
                for ($i = 0;$i<6;$i++)
                {
                    if(isset($arr_third_p_info['nns_image'.$i]))
                    {
                        continue;
                    }
                    $arr_third_p_info['nns_image'.$i] = isset($desc_arr['pic'][$i]) && strlen($desc_arr['pic'][$i]) > 0 ? $desc_arr['pic'][$i] : '';
                    if(count($arr_temp_image) <3 && isset($desc_arr['pic'][$i]) && strlen($desc_arr['pic'][$i]) > 0)
                    {
                        $arr_temp_image[] = $desc_arr['pic'][$i];
                    }
                }
                $count_temp = count($arr_temp_image);
                for ($j = 0;$j<3;$j++)
                {
                    if(!isset($arr_third_p_info['nns_image'.$i]) || strlen($arr_third_p_info['nns_image'.$i]) <1)
                    {
                        if($count_temp >=3)
                        {
                            $arr_third_p_info['nns_image'.$i] = array_pop($arr_temp_image);
                        }
                        else if($count_temp == 2 || $count_temp == 1)
                        {
                            $arr_third_p_info['nns_image'.$i] = $arr_temp_image[0];
                        }
                    }
                }
                $arr_third_p_info['nns_actor'] = isset($desc_arr['actor']) ? rtrim($desc_arr['actor'],'/') : '';
                $arr_third_p_info['nns_director'] = isset($desc_arr['director']) ? rtrim($desc_arr['director'],'/') : '';
                $arr_third_p_info["nns_kind"] = isset($desc_arr["kind"]) ? $desc_arr["kind"] : "";
                //添加节目唯一id,重组节目单注入id。 xinxin.deng 2018/7/12 13:52 start.
                $str_import_id = md5(strtotime($date_start) ."|" . $playbill_unique_id . "|" . $program_id . "|" .strtotime($date_end));
                $arr_third_p_info['nns_playbill_import_id'] = $str_import_id;
                $arr_playbill = $arr_third_p_info;
                $arr_list[$str_import_id] = $arr_playbill;
            }
            break;
        }
        return $this->build_return(0,"successful",$arr_list);
    }

    /**
     * 比较节目单数据库数据和接口数据,更新最新节目单
     * @param $dc
     * @param $arr_mysql
     * @param $arr_adi
     * @return array
     */
    public function compare_mysql_adi_info($dc, $arr_mysql, $arr_adi)
    {
        $params = array(
            'ret' => 0,
            'reason' =>'更新成功'
        );
        if (empty($arr_mysql) && empty($arr_adi))
        {
            return array(
                'ret' => 0,
                'reason' => '数据没改变,无操作'
            );
        }
        else if (empty($arr_mysql) && is_array($arr_adi))
        {
            //从缓存添加到数据库
            $params = $this->construct_params($dc, 'add', $arr_adi);
        }
        else if (is_array($arr_mysql) && empty($arr_adi))
        {
            //删除数据库数据
//            $params = $this->construct_params($dc, 'delete', $arr_mysql);
        }
        else
        {
            $m_key = array_keys($arr_mysql);
            $a_key = array_keys($arr_adi);
            $delete_arr = array();
            foreach ($arr_mysql as $key => $value)
            {
                //数据库里不在缓存,应该删除
                if (!in_array($key, $a_key))
                {
                    $delete_arr[] = $value;
                }
            }
            if (!empty($delete_arr))
            {
                //删除节目单
                $params = $this->construct_params($dc, 'delete',$delete_arr);
            }
            $add_arr = array();
            foreach ($arr_adi as $k => $v)
            {
                //数据库应该添加的
                if (!in_array($k,$m_key))
                {
                    $add_arr[] = $v;
                }
            }
            if (!empty($add_arr))
            {
                $params = $this->construct_params($dc, 'add', $add_arr);
            }
        }
        return $params;
    }

    /**
     * 构造节目单增加或删除的参数结构并执行
     * @param $dc
     * @param $type
     * @param $arr_params
     * @return array
     */
    public function construct_params($dc, $type, $arr_params)
    {
        $obj_import_assets = new import_assets_class();
        if ($type == 'add')
        {
            foreach ($arr_params as $item)
            {
                $params = array();
                $time_tmp = explode(' ',$item['nns_begin_time']);
                $params['playbill_info']['start_date'] = $time_tmp[0];
                $params['playbill_info']['start_time'] = $time_tmp[1];
                $params['playbill_info']['duration'] = gmstrftime('%H%M%S', $item['nns_time_len']);
                $params['playbill_info']['program_name'] = $item['nns_name'];
                $params['playbill_info']['info'] = $item['nns_summary'];
                $params['playbill_info']['image1'] = $item['nns_image1'];
                $params['playbill_info']['image2'] = $item['nns_image2'];
                $params['playbill_info']['image3'] = $item['nns_image3'];
                $params['playbill_info']['image4'] = $item['nns_image4'];
                $params['playbill_info']['image5'] = $item['nns_image5'];
                $params['playbill_info']['image0'] = $item['nns_image0'];
                $params['playbill_info']['actor'] = $item['nns_actor'];
                $params['playbill_info']['director'] = $item['nns_director'];
                $params['playbill_info']['kind'] = $item['nns_kind'];
                $params['live_id'] = $item['nns_live_id'];
                $params['playbill_id'] = $item['nns_playbill_import_id'];
                $arr_where = array(
                    'where' => array(
                        'nns_live_id' => $item['nns_live_id']
                    ),
                    'like' => array(
                        'nns_media_caps' => 'PLAYBACK'
                    ),
                );
                $live_media_info = nl_live_media::query($dc,$arr_where);
                if ($live_media_info['ret'] != 0)
                {
                    return $live_media_info;
                }
                $params['live_media_id'] = $live_media_info['data_info'][0]['nns_id'];
                $params['live_media_source'] = $live_media_info['data_info'][0]['nns_cp_id'];
                $params['playbill_info']['import_source'] = $live_media_info['data_info'][0]['nns_import_source'];
                $params['playbill_info']['domain'] = $live_media_info['data_info'][0]['nns_domain'];
                $params = $this->mix_htmlspecialchars($params);
                $result = $obj_import_assets->import_live_playbill_deal($params);
                if ($result['ret'] != 0)
                {
                    return $result;
                }
            }
        }
        if ($type == 'delete')
        {
            foreach ($arr_params as $item)
            {
                $params = array();
                $params['playbill_id'] = $item['nns_playbill_import_id'];
                $arr_where = array(
                    'where' => array(
                        'nns_live_id' => $item['nns_live_id']
                    ),
                    'like' => array(
                        'nns_media_caps' => 'PLAYBACK'
                    ),
                );
                $live_media_info = nl_live_media::query($dc,$arr_where);
                if ($live_media_info['ret'] != 0)
                {
                    return $live_media_info;
                }
                $params['live_playbill_source'] = $live_media_info['data_info'][0]['nns_cp_id'];
                $result = $obj_import_assets->delete_playbill_deal($params);
                if ($result['ret'] != 0)
                {
                    return $result;
                }
            }
        }
        return array('ret' => 0, 'reason' => '更新成功');
    }
    
    /**
     * 底层实现sql添加bug
     * @param unknown $last_params
     * @return string
     */
    public function mix_htmlspecialchars($last_params)
    {
        $str_pregs = "/\'|\/\*|\#|\"|\--|\ --|\/|\*|\-|\+|\=|\~|\*@|\*!|\$|\%|\^|\&/";
        if(is_array($last_params))
        {
            foreach ($last_params as $key=>$value)
            {
                if(is_array($value))
                {
                    $last_params[$key] = $this->mix_htmlspecialchars($value);
                    continue;
                }
                else if(is_string($value) && preg_match($str_pregs, $value))
                {
                    $last_params[$key] = htmlspecialchars($value, ENT_QUOTES);
                }
            }
        }
        else if(is_string($last_params) && preg_match($str_pregs, $last_params))
        {
            $last_params = htmlspecialchars($last_params, ENT_QUOTES);
        }
        return $last_params;
    }

    /**
     * 根据节目单密钥获得节目的描述.图片.演员.导演
     * @param $arr_desc
     * @param $obj_image_handle
     * @return mixed
     * @internal param $str_cp_id
     */
    public function get_desc_by_program_arr($arr_desc, $obj_image_handle)
    {

        $desc_arr = array();
        //节目单的补全信息
        $arr_program_info = $arr_desc['data']['document']["children"]['program']["children"];
        //节目单简介
        if(isset($arr_program_info["epi_no"]["content"]))
        { //处理节目单分集号的问题，这里是特殊处理。从第三发获取的节目单可能没有分集描述
            $int_epi_no = $arr_program_info["epi_no"]["content"];
            //这里特殊处理 所以才用了‘episode’，xml转换为数组的方法会将第一个子元素key初始化为节点名称，第二个子元素会从0开始递增
            $int_epi_no = (intval($int_epi_no) == 1) ? 'episode' : $int_epi_no - 2;
        }
        if(isset($int_epi_no) && isset($arr_program_info["episodes"]["children"][$int_epi_no]))  //若节目类型为电视剧，就取该集的简介
        {
            $int_epi_no = intval($int_epi_no);
            //如果没有单集描述，就使用全局的描述
            $desc_arr['program_desc'] = $arr_program_info["episodes"]["children"][$int_epi_no]["children"]["description"]["content"];
        }
        else
        {
            $desc_arr['program_desc'] = $arr_program_info["description"]["content"];
        }
        //节目单横竖图片信息
        $arr_picture_info = $arr_program_info["thumbs"]["children"];
        if (is_array($arr_picture_info) && count($arr_picture_info) != 0)
        {
            $int_picture_control = 0;
            foreach ($arr_picture_info as $picture_url)
            {
                if(!$this->has_length($picture_url["content"]))
                {
                    continue;
                }
                if($int_picture_control > 3)
                {
                    $desc_arr['pic'][] = $obj_image_handle->img_handel(preg_replace('/\?.*/i', '', $picture_url['content']));
                    continue;
                }
                if(!isset($picture_url['attributes']['size']) || strlen($picture_url['attributes']['size'])<1)
                {
                    $desc_arr['pic'][] = $obj_image_handle->img_handel(preg_replace('/\?.*/i', '', $picture_url['content']));
                    continue;
                }
                $arr_pic_size = explode('x', $picture_url['attributes']['size']);
                if(!is_array($arr_pic_size) || empty($arr_pic_size) || count($arr_pic_size) != 2)
                {
                    $desc_arr['pic'][] = $obj_image_handle->img_handel(preg_replace('/\?.*/i', '', $picture_url['content']));
                    continue;
                }
                $num = ceil(($arr_pic_size[0]/$arr_pic_size[1])*1000);
                if ($num < 820)
                {
                    $desc_arr['pic']['verticality_img'] = $obj_image_handle->img_handel(preg_replace('/\?.*/i', '', $picture_url['content']));
                    continue;
                }
                else if($num > 1400)
                {
                    $desc_arr['pic']['horizontal_img'] = $obj_image_handle->img_handel(preg_replace('/\?.*/i', '', $picture_url['content']));
                    continue;
                }
                else
                {
                    $desc_arr['pic']['square_img'] = $obj_image_handle->img_handel(preg_replace('/\?.*/i', '', $picture_url['content']));
                }
                $int_picture_control++;
            }
        }
        //节目单图片信息
        $arr_picture_info = $arr_program_info["pictures"]["children"];
        if (is_array($arr_picture_info) && count($arr_picture_info) != 0)
        {
            $int_picture_control = 0;
            foreach ($arr_picture_info as $picture_url)
            {
                if($int_picture_control > 2)
                {
                    break;
                }
                if($this->has_length($picture_url["content"]))
                {
                    $str_img_tmp = strpos($picture_url["content"],"?") === false ? $picture_url["content"] : substr($picture_url["content"],0,strpos($picture_url["content"],"?"));
                    $desc_arr['pic'][] = $obj_image_handle->img_handel($str_img_tmp);
                }
                $int_picture_control++;
            }
        }
        
        //演员信息
        $arr_actor_info = $arr_program_info["cast_info"]["children"];
        if(is_array($arr_actor_info) && count($arr_actor_info) != 0)
        {
            foreach ($arr_actor_info as $actor_item)
            {
                $desc_arr["actor"] .= $actor_item["children"]["star_name"]["content"] . "/";
            }
        }
        //演员编剧主持人
        $arr_crews_info = $arr_program_info["crews"]["children"];
        if(is_array($arr_crews_info) && count($arr_crews_info) != 0)
        {
            foreach($arr_crews_info as $arr_crew_item)
            {
                //导演
                if($arr_crew_item["attributes"]["role"] == "director")
                {
                    $desc_arr['director'] .= $arr_crew_item["children"]["star_name"]['content']."/";
                }
                //主持人/嘉宾
                if($arr_crew_item["attributes"]["role"] == "crews")
                {
                    $desc_arr['actor'] .= $arr_crew_item["children"]["star_name"]['content']."/";
                }
                //编剧（暂时不取该值）
                if($arr_crew_item["attributes"]["role"] == "writer")
                {

                }
            }
        }
        //节目类型
        $arr_kind_info = $arr_program_info["genre"];
        if(isset($arr_kind_info["content"]))
        {
            $desc_arr["kind"] = $arr_kind_info["content"];
        }
        unset($arr_program_info);
        return $desc_arr;
    }
}
$timer = new timer_get_playbill_info_from_deepepg("timer_get_playbill_info_from_deepepg", 'timer_get_playbill_info_from_deepepg',null,"thrid_playbill");
$timer->run();




/**
 * xml转化为数组,支持命名空间
 * @auth yxt
 * 2014年3月10日9:06:29
 */
class xml2array {

    private static $arr_re=array(
        0=>array(
            'ret'=>0,
            'reason'=>'解析失败',
        ),
        1=>array(
            'ret'=>1,
            'reason'=>'成功',
        )
    );
    /**
     * @param string $str_xml xml字符串
     * @return array 转化后的数组array('ret'=>返回编码,'reason'=>编码解释,'data'=>解析后的数组)
     */
    static function getArrayData($str_xml = null) {
        $obj_node = @simplexml_load_string($str_xml, 'SimpleXMLElement', LIBXML_NOCDATA | LIBXML_NOBLANKS);
        if ($obj_node === false) {
            $arr_re = self::$arr_re[0];
        } else {
            $arr_re = self::$arr_re[1];
            $arr_re['data']=self::parseNode($obj_node);
        }
        return $arr_re;
    }

    /**
     * 解析SimpleXMLElement节点对象
     * @param SimpleXMLElement $obj_node 需要解析的几点
     * @param array $arr_parent 本次递归返回的数组
     * @param string $str_namespace 命名空间
     * @param bool $bool_recursive 是否是递归
     * @return array
     */
    private static function parseNode($obj_node, &$arr_parent = null, $str_namespace = '', $bool_recursive = false) {
        $str_content = "{$obj_node}";
        if (strlen($str_content)>0) {
            $arr_children["content"] = $str_content;
        }
        foreach ($obj_node->children() as $k => $v) {
            self::parseNode($v, $arr_children['children'], '', true);
        }
        foreach ($obj_node->attributes() as $k => $v) {
            $arr_children['attributes'][$k] = "{$v}";
        }
        if(is_array($arr_parent) && array_key_exists($obj_node->getName(),$arr_parent))
        {
            $arr_parent[] = &$arr_children;
        }
        else
        {
            $arr_parent[$obj_node->getName()] = &$arr_children;
        }
        return $arr_parent;
    }

}

