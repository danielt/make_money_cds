<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/notify_message/notify_message.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/np/np_http_curl.class.php';
class timer_async_notify_message_by_gx extends nn_timer
{
    //反馈条数
    public $notify_num = 100;

    public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->do_timer_action();
        $this->msg('执行结束...');
    }
    /**
     * 脚本执行体
     * @author zhiyong.luo
     */
    private function do_timer_action()
    {
        $dc = nl_get_dc(array (
            "db_policy" => NL_DB_WRITE | NL_DB_READ,
            "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
        ));
        $dc->open();
        //获取反馈消息
        $params = array(
            'nns_status' => 0,
        );
        $notify_list = nl_notify_message::get_nofity_message($dc,$params,0,$this->notify_num);
        if($notify_list['ret'] != 0)
        {
            echo "不存在反馈的消息\r\n";
            return ;
        }
        if(empty($notify_list['data_info']))
        {
            echo "不存在反馈的消息\r\n";
            return ;
        }

        foreach($notify_list['data_info'] as $value){

            //取出反馈地址循环反馈 存入日志
            include_once dirname(dirname(__FILE__)) . '/nn_cms_config/nn_cms_meta_config_v2.php';
            foreach ($gx_notify_url as $val){
                $result = $this->curl_post($val,$value['nns_notify']);
                if($result['code'])
                {//删除反馈消息 写入日志
                    nl_notify_message::delete_nofity_message($dc,$value['nns_id']);
                    nn_log::write_log_message(LOG_MODEL_NOTIFY, '反馈的xml数据为'.$value['nns_notify'].'POST反馈数据返回为：' . var_export($result['curl_info'],true) .'请求的url为' . $val, $value['nns_cp_id']);
                }
            }
        }
    }
    /**
     * 异步回调
     * @param unknown $url
     * @param unknown $content
     */
    private function curl_post($url,$content)
    {
        $http_curl = new np_http_curl_class();
        $http_header = array("Content-Type: text/xml");
        $response = $http_curl->post($url,$content,$http_header,5);
        $curl_info = $http_curl->curl_getinfo();
        $http_code = $curl_info['http_code'];
        if($http_code > 400)
        {
            $result['code']=false;
            return $result;
        }
        else
        {
            $result['code'] = true;
            $result['curl_info'] = $curl_info;
            return $result;
        }
    }
}
$async_notify_message = new timer_async_notify_message_by_gx("async_notify_message", 'public',__FILE__);
$async_notify_message->run();