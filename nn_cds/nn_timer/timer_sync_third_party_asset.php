<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/mgtv_init.php';
class timer_execute extends nn_timer
{
    public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->init($params);
        $this->msg('执行结束...');
    }
    
    /**
     * 初始化
     * @param string $params
     */
    function init($params = null)
    {
        $str_third_playbill_pool = $params["bk_queue_value"];
        if(strlen($str_third_playbill_pool) <1)
        {
            $this->msg("[redis节目单池中返回数据为空]".$str_third_playbill_pool);
            return ;
        }
        $str_mgtv_init = dirname(dirname(__FILE__))."/mgtv_v2/{$str_third_playbill_pool}/init.php";
        if(!file_exists($str_mgtv_init))
        {
            $this->msg("[初始化文件不存在]".$str_mgtv_init);
            return ;
        }
        include_once $str_mgtv_init;
        define('ORG_ID', $str_third_playbill_pool);
        $result_sp = $this->_get_sp_info($str_third_playbill_pool);
        if($result_sp['ret'] !=0)
        {
            $this->msg("sp信息获取spID[{$str_third_playbill_pool}]失败：".var_export($result_sp,true));
            return ;
        }
        if(!isset($this->arr_sp_config[$str_third_playbill_pool]['nns_config']['cdn_notify_third_party_enable']) || $this->arr_sp_config[$str_third_playbill_pool]['nns_config']['cdn_notify_third_party_enable'] !='1' || !isset($this->arr_sp_config[$str_third_playbill_pool]['nns_config']['cdn_notify_third_party_mark']) || strlen($this->arr_sp_config[$str_third_playbill_pool]['nns_config']['cdn_notify_third_party_mark'])<1)
        {
            $this->msg("sp信息获取spID[{$str_third_playbill_pool}]不再同时往第三方CMS平台注入元数据信息");
            return ;
        }
        include_once dirname(dirname(__FILE__)).'/nn_logic/c2_task/c2_task.class.php';
        include_once dirname(dirname(__FILE__)).'/mgtv_v2/models/c2_task_model.php';
        $params = array(
            'nns_org_id' =>$str_third_playbill_pool,
            'nns_status' =>'0',
            'nns_notify_third_state' =>'0',
        );
        $result_c2_task = nl_c2_task::query_by_condition($this->obj_dc,$params,0,2,"order by nns_create_time asc");
        if($result_c2_task['ret'] !=0 || !isset($result_c2_task['data_info']) || empty($result_c2_task['data_info']) || !is_array($result_c2_task['data_info']))
        {
            $this->msg("查询无数据".var_export($result_c2_task,true));
            return ;
        }
        foreach ($result_c2_task['data_info'] as $data_info)
        {
            $result = c2_task_model::notify_upstream_third($data_info['nns_id'],$this->arr_sp_config[$str_third_playbill_pool]['nns_config'],$this->obj_dc,$data_info['nns_id']);
            $this->msg('消息反馈上游结果:'.var_export($result,true));
            nl_c2_task::edit($this->obj_dc, array('nns_notify_third_state'=>($result['ret'] == '0') ? '1' : '2'), $data_info['nns_id']);
        }
    }
}
$arr_files = pathinfo(__FILE__);
$file_name = $arr_files['filename'];
$timer_execute = new timer_execute($file_name, ORG_ID,__FILE__,'sync_third_party');
$timer_execute->run();