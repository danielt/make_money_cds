<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/mgtv_init.php';
class timer_execute extends nn_timer
{
    public function action($params = null)
    {
        $this->msg('开始执行...');
        $this->init($params);
        $this->msg('执行结束...');
    }
    
    /**
     * 初始化
     * @param string $params
     */
    function init($params = null)
    {
        $str_third_playbill_pool = $params["bk_queue_value"];
        if(strlen($str_third_playbill_pool) <1)
        {
            $this->msg("[redis节目单池中返回数据为空]".$str_third_playbill_pool);
            return ;
        }
        $str_mgtv_init = dirname(dirname(__FILE__))."/mgtv_v2/{$str_third_playbill_pool}/init.php";
        if(!file_exists($str_mgtv_init))
        {
            $this->msg("[初始化文件不存在]".$str_mgtv_init);
            return ;
        }
        include_once $str_mgtv_init;
        define('ORG_ID', $str_third_playbill_pool);
        $result_sp = $this->_get_sp_info($str_third_playbill_pool);
        if($result_sp['ret'] !=0)
        {
            $this->msg("sp信息获取spID[{$str_third_playbill_pool}]失败：".var_export($result_sp,true));
            return ;
        }
        if(isset($this->arr_sp_config[$str_third_playbill_pool]['nns_config']['disabled_cdn']) && $this->arr_sp_config[$str_third_playbill_pool]['nns_config']['disabled_cdn'] == '1')
        {
            $this->msg("sp信息获取spID[{$str_third_playbill_pool}]注入CDN开关关闭");
            return ;
        }
        if(!isset($this->arr_sp_config[$str_third_playbill_pool]['nns_config']['cdn_assets_enabled']) || 
            !is_array($this->arr_sp_config[$str_third_playbill_pool]['nns_config']['cdn_assets_enabled']) || 
            (is_array($this->arr_sp_config[$str_third_playbill_pool]['nns_config']['cdn_assets_enabled']) && 
                !in_array('file', $this->arr_sp_config[$str_third_playbill_pool]['nns_config']['cdn_assets_enabled'])))
        {
            $this->msg("sp信息获取spID[{$str_third_playbill_pool}]注入CDN文件包不允许注入");
            return ;
        }
        $result = content_task_model::vod($new_id_c2 = null, 'file', ORG_ID);
        $this->msg("处理结果：".var_export($result,true));
    }
}
$arr_files = pathinfo(__FILE__);
$file_name = $arr_files['filename'];
$timer_execute = new timer_execute($file_name, ORG_ID,__FILE__,'cdn_file');
$timer_execute->run();



