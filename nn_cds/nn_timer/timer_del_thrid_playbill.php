<?php
/**
 * 更新频道绑定的第三方频道节目单信息
 * @author ye.zhong
 * @date 2017-05-13
 */
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(__FILE__)) . '/api_v2/nn_const.php';
include_once dirname(dirname(__FILE__)) . '/api_v2/common.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/live/playbill.class.php';
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/models/queue_task_model.php';
//引入np类文件
include_np('np_string');
define("IS_LOG_TIMER_OPERATION", true);
class timer_execute extends nn_timer
{
	public function action($params = null)
	{
		$this->msg('开始执行...');
		$this->init($params);
		$this->msg('执行结束...');
	}

    /**
     * 初始化
     * @param string $params
     */
    public function init($params = null)
    {
        $str_third_playbill_pool = $params["bk_queue_value"];
        if(!$this->has_length($str_third_playbill_pool))
        {
            echo "redis节目单池中返回数据为空".'<br/>';
            nn_log::write_log_message(LOG_MODEL_MESSAGE, "[redis节目单池中返回数据为空]".$str_third_playbill_pool, "get_playbill_info");
            return false;
        }
        $arr_guid_time = explode("|#|", $str_third_playbill_pool);
        $str_third_live_guid =$arr_guid_time[0];
        $str_third_live_time =$arr_guid_time[2];
        $str_third_live_del =$arr_guid_time[1];
        $result_del = nl_playbill::playbill_by_del_day($this->obj_dc, $str_third_live_guid,$str_third_live_del.' 23:59:59');
        if($result_del['ret'] !=0)
        {
            return $result_del;
        }
        $result_query = nl_playbill::get_playbill_by_del_day($this->obj_dc, $str_third_live_guid,$str_third_live_time.' 23:59:59');
        if($result_query['ret'] !=0 || !isset($result_query['data_info']) || empty($result_query['data_info']) || !is_array($result_query['data_info']))
        {
            return $result_query;
        }
        $result_query = $result_query['data_info'];
        $edit_data = null;
        foreach ($result_query as $query_val)
        {
            $edit_data[] = $query_val['nns_id'];
        }
        $result_edit = nl_playbill::edit_v2($this->obj_dc, array('nns_state'=>1), array('nns_id'=>$edit_data));
        if($result_edit['ret'] !=0)
        {
            return $result_edit;
        }
        foreach ($edit_data as $del_val)
        {
            $queue_task_model = new queue_task_model();
            $queue_task_model->q_add_task_op_mgtv($del_val, 'playbill', 'destroy','',true,null);
            unset($queue_task_model);
        }
        return $result_edit;
    }
}
$arr_files = pathinfo(__FILE__);
$file_name = $arr_files['filename'];
$timer_execute = new timer_execute($file_name, ORG_ID,__FILE__,'del_thrid_playbill');
$timer_execute->run();