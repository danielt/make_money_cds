<?php 
class nl_public_file_encode extends nn_public_return
{
    /**
     * 负载均衡队列数据
     * @var unknown
     */
    public $arr_transcode_load_balance = null;
    
    /**
     * 
     * @param unknown $sp_id
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')|multitype:number
     */
    public function get_encode_num()
    {
        $result = nl_file_encode::get_encode_doing_num($this->obj_dc,$this->sp_id);
        if($result['ret'] !=0 || !isset($result['data_info']) || !is_array($result['data_info']) || empty($result['data_info']))
        {
            return $result;
        }
        $last_data = null;
        foreach ($result['data_info'] as $data)
        {
            if(empty($data['nns_loadbalance_id']))
            {
                $last_data['empty'] = $data['doing_num'];
            }
            else
            {
                $last_data[$data['nns_loadbalance_id']] = $data['doing_num'];
            }
        }
        return $this->__return_data(0,'ok',$last_data);
    }
    
    
    /**
     * 获取正在转码的数据统计
     * @param unknown $sp_id
     * @param unknown $loadbalance_id
     * @param unknown $model_id
     * @param unknown $data_loadbalance
     * @return Ambigous|multitype:number |array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function get_load_balance_all_num($num,$file_encode=null)
    {
        $result_load_balance = nl_transcode_load_balance::query_sp_info($this->obj_dc,$this->sp_id);
        if($result_load_balance['ret'] !=0 || !isset($result_load_balance['data_info']) || !is_array($result_load_balance['data_info']) || empty($result_load_balance['data_info']))
        {
            return $result_load_balance;
        }
        $arr_video = $arr_live = null;
        $num_video = $num_live = 0;
        foreach ($result_load_balance['data_info'] as $load_balance_val)
        {
            $load_balance_val['nns_max_num'] = (int)$load_balance_val['nns_max_num']>0 ? (int)$load_balance_val['nns_max_num'] : 0;
            if(isset($file_encode[$load_balance_val['nns_id']]))
            {
                $load_balance_val['nns_max_num'] = $load_balance_val['nns_max_num'] - $file_encode[$load_balance_val['nns_id']];
            }
            if($load_balance_val['nns_max_num'] <1)
            {
                continue;
            }
            $this->arr_transcode_load_balance[$load_balance_val['nns_id']] = $load_balance_val;
            if($load_balance_val['nns_type'] == '1')
            {
                $arr_live[] = $load_balance_val;
                $num_live+=$load_balance_val['nns_max_num'];
            }
            else
            {
                $arr_video[] = $load_balance_val;
                $num_video+=$load_balance_val['nns_max_num'];
            }
        }
        $last_num = $num/$num_video;
        $load_balance_num = count($arr_video);
        $last_data_temp = $last_data =null;
        for ($i=0;$i<$load_balance_num;$i++)
        {
            $temp_num = floor($arr_video[$i]['nns_max_num'] *$last_num);
            if($temp_num <1)
            {
                $last_data_temp[] = $arr_video[$i]['nns_id'];
            }
            else
            {
                for ($j=0;$j<$temp_num;$j++)
                {
                    $last_data[] = $arr_video[$i]['nns_id'];
                }
            }
        }
        $is_num = empty($last_data) ? 0 : count($last_data);
        if($num > $is_num)
        {
            $last_data_temp = $this->my_array_rand($last_data_temp,$num-$is_num);
            if(is_array($last_data_temp))
            {
                if(is_array($last_data))
                {
                    $last_data = array_merge($last_data,$last_data_temp);
                }
                else
                {
                    $last_data = $last_data_temp;
                }
            }
        }
        return $last_data;
    }
    
    /**
     * 获取随机的数据
     * @param unknown $last_data_temp
     * @param unknown $num
     * @return NULL|unknown
     */
    public function my_array_rand($last_data_temp,$num)
    {
        $last_data = null;
        if(empty($last_data_temp) || !is_array($last_data_temp))
        {
            return $last_data;
        }
        if($num <1)
        {
            return $last_data;
        }
        $data_count = count($last_data_temp);
        if($data_count > $num)
        {
            $arr = range(0,$num);
            shuffle($arr);
            for ($i=0;$i<$num;$i++)
            {
                $temp_index = array_pop($arr);
                if(isset($last_data_temp[$temp_index]))
                {
                    $last_data[] = $last_data_temp[$temp_index];
                }
            }
            return $last_data;
        }
        return $last_data_temp;
    }
}