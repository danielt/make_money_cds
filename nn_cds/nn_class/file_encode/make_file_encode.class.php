<?php
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(__FILE__)).'/public_return.class.php';
include_once dirname(dirname(__FILE__)).'/ftp/ftp.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/transcode/transcode_manufacturer_model.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/transcode/transcode_manufacturer.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/transcode/transcode_model.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/vod_media/vod_media.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/live/live_media.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/live/live_media_ex.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/op_queue/op_queue.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/clip_task/clip_task.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/clip_task/clip_task_log.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/file_encode/file_encode.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/sp/sp.class.php';
include_once dirname(dirname(dirname(__FILE__))).'/nn_logic/cp/cp.class.php';
class make_file_encode_queue extends nn_public_return
{
    
    /**
     * 类 初始化
     * @param unknown $dc
     */
    public function __construct($dc)
    {
        $this->obj_dc = $dc;
    }
    /**
     * 初始化
     * @param unknown $task_id
     * @return Ambigous|Ambigous <unknown, Ambigous, mixed>|multitype:number
     */
    public function init($sp_id,$task_id)
    {
        $task_info = nl_clip_task::query_by_id($this->obj_dc, $task_id);
        if($task_info['ret'] !=0 || !isset($task_info['data_info']) || !is_array($task_info['data_info']) || empty($task_info['data_info']))
        {
            return $task_info;
        }
        $task_info = $task_info['data_info'];
        if(!in_array($task_info['nns_state'],array('clip_encode_wait','clip_encode_fail','ok')))
        {
            return $this->__return_data(0,'OK');
        }
        $result_sp = $this->_get_sp_info($task_info['nns_org_id']);
        if($result_sp['ret'] !=0)
        {
            return $result_sp;
        }
        if(!isset($this->arr_sp_config[$task_info['nns_org_id']]['nns_config']['clip_file_encode_enable']) || $this->arr_sp_config[$task_info['nns_org_id']]['nns_config']['clip_file_encode_enable'] != 1)
        {
            return $this->__return_data(1,"SPID:{$task_info['nns_org_id']}不做转码处理");
        }
        if(!isset($this->arr_sp_config[$task_info['nns_org_id']]['nns_config']['clip_file_encode_model']) || $this->arr_sp_config[$task_info['nns_org_id']]['nns_config']['clip_file_encode_model'] != 1)
        {
            return $this->__return_data(1,"SPID:{$task_info['nns_org_id']}不走新的转码逻辑");
        }
        if(!in_array($task_info['nns_video_type'], array('0','1')))
        {
            return $this->__return_data(1,'转码的为非直播、或者点播片源');
        }
        $video_info = $this->get_asset_info(($task_info['nns_video_type'] == '0') ? 'media' : 'live_media',array('nns_id'=>$task_info['nns_video_media_id']));
        if($video_info['ret'] !=0 || !isset($video_info['data_info']) || !is_array($video_info['data_info']) || empty($video_info['data_info']))
        {
            return $video_info;
        }
        $video_info = $video_info['data_info'];
        $result_cp = $this->_get_cp_info($video_info['nns_cp_id']);
        if($result_cp['ret'] !=0)
        {
            return $result_cp;
        }
        $result_encode =  ($task_info['nns_video_type'] != '0') ? $this->make_live_encode_queue($task_info, $video_info) : $this->make_video_encode_queue($task_info, $video_info);
        $arr_clip_log = array(
            'nns_id'=>np_guid_rand(),
            'nns_task_id'=>$task_id,
            'nns_video_type'=>$task_info['nns_video_type'],
            'nns_video_id'=>$task_info['nns_video_id'],
            'nns_video_index_id'=>$task_info['nns_video_index_id'],
            'nns_org_id'=>$task_info['nns_org_id'],
        );
        if($result_encode['ret'] !=0)
        {
            $arr_clip_log['nns_state']='file_encode_fail';
            $arr_clip_log['nns_desc']='切片指令转入转码队列失败，原因：'.json_encode($result_encode,JSON_UNESCAPED_UNICODE);
            nl_clip_task_log::add($this->obj_dc, $arr_clip_log);
            $result_edit_clip = nl_clip_task::edit($this->obj_dc, array('nns_state'=>'clip_encode_fail'), $task_info['nns_id']);
        }
        else
        {
            $arr_clip_log['nns_state']='file_encode_ok';
            $arr_clip_log['nns_desc']='切片指令执行完毕，准备转码';
            nl_clip_task_log::add($this->obj_dc, $arr_clip_log);
            $result_edit_clip = nl_clip_task::edit($this->obj_dc, array('nns_state'=>'ok'), $task_info['nns_id']);
        }
        if(isset($this->arr_cp_config[$video_info['nns_cp_id']]['nns_config']['video_source_notify_url']) && strlen($this->arr_cp_config[$video_info['nns_cp_id']]['nns_config']['video_source_notify_url']) >0)
        {
            $data_op_queue = nl_op_queue::query_by_id($this->obj_dc, $task_info['nns_op_id']);
            if(isset($data_op_queue['data_info']) && !empty($data_op_queue['data_info']) && is_array($data_op_queue['data_info']))
            {
                $playbill_info = array(
                    'id'=>$data_op_queue['data_info']['nns_message_id'],
                    'state'=>$result_encode['ret'] == '0' ? 1 : 3,
                    'summary'=>$result_encode['reason'],
                );
                $this->_execute_curl($this->arr_cp_config[$video_info['nns_cp_id']]['nns_config']['video_source_notify_url'],'post',array('a'=>'update_vod_playbill_transcode_state','playbill_info'=>json_encode(array($playbill_info))));
            }
        }
        i_write_log_core("切片指令执行完毕，准备转码,队列信息为".var_export($task_info,true));

        if($result_edit_clip['ret'] !=0)
        {
            return $result_edit_clip;
        }
        if($task_info['nns_video_type'] == '0')
        {
            if(!isset($this->arr_sp_config[$task_info['nns_org_id']]['nns_config']['media_encode_model']) || $this->arr_sp_config[$task_info['nns_org_id']]['nns_config']['media_encode_model'] != 1)
            {
                $arr_clip_log['nns_id']=np_guid_rand();
                $arr_clip_log['nns_state']='file_encode_org';
                $arr_clip_log['nns_desc']='转码后的原片队列继续，不删除';
                nl_clip_task_log::add($this->obj_dc, $arr_clip_log);
            }
            else
            {
                $arr_clip_log['nns_id']=np_guid_rand();
                $arr_clip_log['nns_state']='file_encode_org';
                $arr_clip_log['nns_desc']='转码后的原片队列需删除';
                nl_clip_task_log::add($this->obj_dc, $arr_clip_log);
                nl_op_queue::delete_op_queue($this->obj_dc,array($task_info['nns_op_id']));
            }
        }
        $queue_task = new queue_task_model();
	    $queue_task->q_clip_ok($task_id);
	    return $result_edit_clip;
    }
    
    
    /**
     * 生成直播转码队列
     * @param unknown $task_info
     * @param unknown $video_info
     */
    public function make_live_encode_queue($task_info,$video_info)
    {
        
    }
    
    
    /**
     * 添加直播转点播队列
     * @param unknown $op_queue_info
     * @param unknown $video_info
     */
    public function make_live_to_media_queue($op_queue_info,$video_info)
    {
        if($op_queue_info['nns_type'] != 'media' || $video_info['nns_media_type'] !='2')
        {
            return $this->__return_data(1,'队列非点播片源或者数据非直播转点播模式');
        }
        $result_sp = $this->_get_sp_info($op_queue_info['nns_org_id']);
        if($result_sp['ret'] !=0)
        {
            return $result_sp;
        }
        if(!isset($this->arr_sp_config[$task_info['nns_org_id']]['nns_config']['clip_file_encode_enable']) || $this->arr_sp_config[$op_queue_info['nns_org_id']]['nns_config']['clip_file_encode_enable'] != 1)
        {
            return $this->__return_data(1,"SPID:{$op_queue_info['nns_org_id']}不做转码处理");
        }
        if(!isset($this->arr_sp_config[$task_info['nns_org_id']]['nns_config']['clip_file_encode_model']) || $this->arr_sp_config[$op_queue_info['nns_org_id']]['nns_config']['clip_file_encode_model'] != 1)
        {
            return $this->__return_data(1,"SPID:{$op_queue_info['nns_org_id']}不走新的转码逻辑");
        }
        $result_transcode_model = nl_transcode_model::query_by_cp($this->obj_dc, $video_info['nns_cp_id'],'0');
        if($result_transcode_model['ret'] !=0)
        {
            return $result_transcode_model;
        }
        if(!isset($result_transcode_model['data_info']) || empty($result_transcode_model['data_info']) || !is_array($result_transcode_model['data_info']))
        {
            return $result_transcode_model;
        }
        $result_transcode_model = $result_transcode_model['data_info'];
        $date = date("Ymd");
        unset($video_info['nns_integer_id']);
        $video_info['nns_media_service'] = '';
        $data_media_file_info = $data_media_info = null;
        foreach ($result_transcode_model as $transcode_model_val)
        {
            $temp_drawing_frame = null;
            $temp_media_data = $video_info;
            if(strlen($transcode_model_val['manufacturer_model_file_path']) <1 || strlen($transcode_model_val['manufacturer_mark']) <1)
            {
                continue;
            }
            if(!(strtolower($video_info['nns_mode']) == $transcode_model_val['nns_input_definition'] || $transcode_model_val['nns_input_definition'] == 'default')
                || !(strtolower($video_info['nns_filetype']) == $transcode_model_val['nns_input_file_type'] || $transcode_model_val['nns_input_file_type'] == 'default'))
            {
                continue;
            }
            $temp_media_data['nns_content_id'] = '';
            $temp_media_data['nns_id'] = np_guid_rand('vod_media');
            $temp_media_data['nns_url'] =  $transcode_model_val['manufacturer_mark'].'/'.$transcode_model_val['manufacturer_model_file_path'].'/'.$date.'/'.$transcode_model_val['nns_id'].'/'.$temp_media_data['nns_id'].'.'.$transcode_model_val['nns_output_file_type'];
            $temp_media_data['nns_filetype'] = $transcode_model_val['nns_output_file_type'];
            $temp_media_data['nns_tag'] = $transcode_model_val['nns_output_file_tag'];
            $temp_media_data['nns_encode_flag'] = 1;
            $temp_media_data['nns_mode'] = strtolower($transcode_model_val['nns_output_definition']);
            $temp_media_data['nns_kbps'] = (isset($transcode_model_val['nns_output_bitrate']) && (int)$transcode_model_val['nns_output_bitrate'] >0) ? (int)$transcode_model_val['nns_output_bitrate'] : $temp_media_data['nns_kbps'];
            $temp_media_data['nns_import_id'] =  md5($video_info['nns_id'].'_'.$temp_media_data['nns_filetype'].$temp_media_data['nns_tag'].$temp_media_data['nns_mode'].$temp_media_data['nns_kbps'].'_'.$transcode_model_val['manufacturer_mark'].'_'.$transcode_model_val['manufacturer_model_file_path']);
            
            $temp_ex_data = null;
            $temp_ex_data['url'] = $temp_ex_data['path']=$temp_media_data['nns_url'];
            $temp_media_data['nns_ext_info'] = json_encode($temp_ex_data,JSON_UNESCAPED_UNICODE);
            
            if($transcode_model_val['nns_drawing_frame'] == '1')
            {
                $transcode_model_val['nns_drawing_type'] = json_decode($transcode_model_val['nns_drawing_type'],true);
                if(is_array($transcode_model_val['nns_drawing_type']) && !empty($transcode_model_val['nns_drawing_type']))
                {
                    foreach ($transcode_model_val['nns_drawing_type'] as $drawing_frame_key=>$drawing_frame_val)
                    {
                        if(strlen($drawing_frame_key)<1 || strlen($drawing_frame_val) <1)
                        {
                            continue;
                        }
                        $temp_drawing_frame[$drawing_frame_key]=$transcode_model_val['manufacturer_mark'].'/'.$transcode_model_val['manufacturer_model_file_path'].'/media/'.$temp_media_data['nns_cp_id'].'/'.date("Y-m-d").'/'.$temp_media_data['nns_import_id'].'/'.$drawing_frame_val.'/'.$temp_media_data['nns_id'].'.zip';
                    }
                }
            }
            $result_add_media_and_ex = $this->add_media_and_ex($temp_media_data, $temp_drawing_frame);
            if($result_add_media_and_ex['ret'] !=0)
            {
                return $result_add_media_and_ex;
            }
            $temp_media_data['nns_id'] = $result_add_media_and_ex['data_info'];
            $data_media_info[] = $temp_media_data;
            $data_media_file_info[$temp_media_data['nns_id']]['model_id'] = $transcode_model_val['nns_id'];
            $data_media_file_info[$temp_media_data['nns_id']]['file']['media']['out'] = $temp_media_data['nns_url'];
            $data_media_file_info[$temp_media_data['nns_id']]['file']['media']['in'] = $video_info['nns_ext_info'];
            if(!empty($temp_drawing_frame) && is_array($temp_drawing_frame))
            {
                $data_media_file_info[$temp_media_data['nns_id']]['file']['drawing_frame'] = $temp_drawing_frame;
            }
        }
        $task_info = array(
            'nns_org_id'=>$op_queue_info['nns_org_id'],
            'nns_task_name'=>$op_queue_info['nns_name'],
            'nns_priority'=>$op_queue_info['nns_priority'],
            'nns_video_type'=>'0',
        );
        if(is_array($data_media_info) && !empty($data_media_info))
        {
            foreach ($data_media_info as $media_val)
            {
                $result_add_op_queue = $this->add_op_queue($op_queue_info, $media_val);
                if($result_add_op_queue['ret']!=0)
                {
                    return $result_add_op_queue;
                }
                if(!isset($result_add_op_queue['data_info']) || strlen($result_add_op_queue['data_info']) <1)
                {
                    continue;
                }
                if(!isset($data_media_file_info[$media_val['nns_id']]['model_id']) || !isset($data_media_file_info[$media_val['nns_id']]['file']) || empty($data_media_file_info[$media_val['nns_id']]['file']) || !is_array($data_media_file_info[$media_val['nns_id']]['file']))
                {
                    continue;
                }
                $result_add_file_encode = $this->add_file_encode($media_val, $result_add_op_queue['data_info'], $video_info['nns_id'], $task_info, $data_media_file_info[$media_val['nns_id']]);
                if($result_add_file_encode['ret'] !=0)
                {
                    return $result_add_file_encode;
                }
            }
        }
        return $this->__return_data(0,'OK');
    }
    
    
    /**
     * 生成点播转码队列
     * @param unknown $task_info
     * @param unknown $video_info
     */
    public function make_video_encode_queue($task_info,$video_info)
    {
        $result_transcode_model = nl_transcode_model::query_by_cp($this->obj_dc, $video_info['nns_cp_id'],$task_info['nns_video_type'],$task_info['nns_org_id']);
        if($result_transcode_model['ret'] !=0)
        {
            return $result_transcode_model;
        }
        if(!isset($result_transcode_model['data_info']) || empty($result_transcode_model['data_info']) || !is_array($result_transcode_model['data_info']))
        {
            return $result_transcode_model;
        }
        $result_transcode_model = $result_transcode_model['data_info'];
        $result_op_queue = nl_op_queue::query_by_id($this->obj_dc, $task_info['nns_op_id']);
        if($result_op_queue['ret'] !=0)
        {
            return $result_op_queue;
        }
        if(!isset($result_op_queue['data_info']) || !is_array($result_op_queue['data_info']) || empty($result_op_queue['data_info']))
        {
            $result_op_queue['data_info'] = array(
                'nns_id'=>$task_info['nns_op_id'],
                'nns_type'=>'media',
                'nns_org_id'=>$task_info['nns_org_id'],
                'nns_create_time'=>date("Y-m-d H:i:s"),
                'nns_name'=>$task_info['nns_task_name'],
                'nns_video_id'=>$task_info['nns_video_id'],
                'nns_weight'=>$task_info['nns_priority'],
                'nns_index_id'=>$task_info['nns_video_index_id'],
                'nns_media_id'=>$task_info['nns_video_media_id'],
                'nns_op_mtime'=>round(np_millisecond_f()*1000),
                'nns_release_time'=>date("Y-m-d"),
                'nns_ex_data'=>$video_info['nns_ext_info'],
                'nns_from'=>'',
                'nns_message_id'=>'',
                'nns_encode_flag'=>'',
                'nns_is_group'=>0,
                'nns_cp_id'=>$video_info['nns_cp_id'],
            );
        }
        $result_op_queue = $result_op_queue['data_info'];
        $result_op_queue['nns_action'] = 'add';
        $result_op_queue['nns_status'] = '8';
        $result_op_queue['nns_state'] = '0';
        $nns_ex_data = json_decode($video_info['nns_ext_info'],true);
        if(!isset($nns_ex_data['path']) || strlen($nns_ex_data['path'])<1)
        {
            return $this->__return_data(1,'片源路径为空');
        }
        $nns_ex_data['path'] = trim(trim(trim($nns_ex_data['path'],'/'),'\\'));
        
        
        $output_path = pathinfo($nns_ex_data['path']);
        if(!isset($output_path['filename']) || strlen($output_path['filename']) <1)
        {
            return $this->__return_data(1,'filename为空');
        }
        
        unset($video_info['nns_integer_id']);
        $video_info['nns_media_service'] = '';
        $data_media_file_info = $data_media_info = null;
        foreach ($result_transcode_model as $transcode_model_val)
        {
            $temp_drawing_frame = null;
            $temp_media_data = $video_info;
            //文件路径为空
            if(strlen($transcode_model_val['manufacturer_model_file_path']) <1 || strlen($transcode_model_val['manufacturer_mark']) <1)
            {
                continue;
            }
            //清晰度不匹配  文件类型不匹配
            if(!(strtolower($video_info['nns_mode']) == $transcode_model_val['nns_input_definition'] || $transcode_model_val['nns_input_definition'] == 'default')
                || !(strtolower($video_info['nns_filetype']) == $transcode_model_val['nns_input_file_type'] || $transcode_model_val['nns_input_file_type'] == 'default'))
            {
                continue;
            }
            //开始码率不匹配
            if(isset($transcode_model_val['nns_start_bitrate']) && strlen($transcode_model_val['nns_start_bitrate'])>0 && (int)$transcode_model_val['nns_start_bitrate'] >0 && $video_info['nns_kbps'] < (int)$transcode_model_val['nns_start_bitrate'])
            {
                continue;
            }
            //结束码率不匹配
            if(isset($transcode_model_val['nns_end_bitrate']) && strlen($transcode_model_val['nns_end_bitrate'])>0 && (int)$transcode_model_val['nns_end_bitrate'] >0 && $video_info['nns_kbps'] > (int)$transcode_model_val['nns_end_bitrate'])
            {
                continue;
            }
            
            $temp_media_data['nns_content_id'] = '';
            $temp_media_data['nns_id'] = np_guid_rand('vod_media');
            $temp_media_data['nns_url'] =  $transcode_model_val['manufacturer_mark'].'/'.$transcode_model_val['manufacturer_model_file_path'].'/'.$transcode_model_val['nns_sp_id'].'/'.$transcode_model_val['nns_cp_id'].'/'.$output_path['dirname'].'/'.$temp_media_data['nns_id'].'.'.$transcode_model_val['nns_output_file_type'];
            $temp_media_data['nns_filetype'] = $transcode_model_val['nns_output_file_type'];
            $temp_media_data['nns_tag'] = $transcode_model_val['nns_output_file_tag'];
            $temp_media_data['nns_encode_flag'] = 1;
            $temp_media_data['nns_mode'] = strtolower($transcode_model_val['nns_output_definition']);
            $temp_media_data['nns_kbps'] = (isset($transcode_model_val['nns_output_bitrate']) && (int)$transcode_model_val['nns_output_bitrate'] >0) ? (int)$transcode_model_val['nns_output_bitrate'] : $temp_media_data['nns_kbps'];
//             $temp_media_data['nns_import_id'] =  md5($video_info['nns_id'].'_'.$temp_media_data['nns_filetype'].$temp_media_data['nns_tag'].$temp_media_data['nns_mode'].$temp_media_data['nns_kbps'].'_'.$transcode_model_val['manufacturer_mark'].'_'.$transcode_model_val['manufacturer_model_file_path'].'_'.$transcode_model_val['nns_output_file_type']);
            
            $temp_media_data['nns_import_id'] = md5($this->file_encode_md5($video_info['nns_id'],$temp_media_data,$transcode_model_val).$task_info['nns_org_id']);
            
            $temp_ex_data = $nns_ex_data;
            $temp_ex_data['url'] = $temp_ex_data['path']=$temp_media_data['nns_url'];
            $temp_media_data['nns_ext_info'] = json_encode($temp_ex_data,JSON_UNESCAPED_UNICODE);
            
            if($transcode_model_val['nns_drawing_frame'] == '1')
            {
                $transcode_model_val['nns_drawing_type'] = json_decode($transcode_model_val['nns_drawing_type'],true);
                if(is_array($transcode_model_val['nns_drawing_type']) && !empty($transcode_model_val['nns_drawing_type']))
                {
                    foreach ($transcode_model_val['nns_drawing_type'] as $drawing_frame_key=>$drawing_frame_val)
                    {
                        if(strlen($drawing_frame_key)<1 || strlen($drawing_frame_val) <1)
                        {
                            continue;
                        }
                        $temp_drawing_frame[$drawing_frame_key]=$transcode_model_val['manufacturer_mark'].'/'.$transcode_model_val['manufacturer_model_file_path'].'/media/'.$transcode_model_val['nns_sp_id'].'/'.$transcode_model_val['nns_cp_id'].'/'.date("Y-m-d").'/'.$temp_media_data['nns_import_id'].'/'.$drawing_frame_val.'/'.$temp_media_data['nns_id'].'.zip';
                    }
                }
            }
//             echo json_encode($temp_media_data);die;
            $result_add_media_and_ex = $this->add_media_and_ex($temp_media_data, $temp_drawing_frame);
            if($result_add_media_and_ex['ret'] !=0)
            {
                return $result_add_media_and_ex;
            }
            $temp_media_data['nns_id'] = $result_add_media_and_ex['data_info'];
            $data_media_info[] = $temp_media_data;
            $data_media_file_info[$temp_media_data['nns_id']]['model_id'] = $transcode_model_val['nns_id'];
            $data_media_file_info[$temp_media_data['nns_id']]['file']['media']['out'] = $temp_media_data['nns_url'];
            $data_media_file_info[$temp_media_data['nns_id']]['file']['media']['in'] = $nns_ex_data['path'];
            if(!empty($temp_drawing_frame) && is_array($temp_drawing_frame))
            {
                $data_media_file_info[$temp_media_data['nns_id']]['file']['drawing_frame'] = $temp_drawing_frame;
            }
        }
        if(is_array($data_media_info) && !empty($data_media_info))
        {
            foreach ($data_media_info as $media_val)
            {
                $temp_nns_op_id = np_guid_rand('nns_mgtvbk_op_queue');
                if(!isset($data_media_file_info[$media_val['nns_id']]['model_id']) || !isset($data_media_file_info[$media_val['nns_id']]['file']) || empty($data_media_file_info[$media_val['nns_id']]['file']) || !is_array($data_media_file_info[$media_val['nns_id']]['file']))
                {
                    continue;
                }
                $result_add_file_encode = $this->add_file_encode($media_val, $temp_nns_op_id, $video_info['nns_id'], $task_info, $data_media_file_info[$media_val['nns_id']]);
                if($result_add_file_encode['ret'] ==1)
                {
                    return $result_add_file_encode;
                }
                else if($result_add_file_encode['ret'] ==0)
                {
                    continue;
                }
                $result_op_queue['nns_id'] = $temp_nns_op_id;
                $result_add_op_queue = $this->add_op_queue($result_op_queue, $media_val);
                i_write_log_core("添加新的中心队列信息为".var_export($result_op_queue,true).'片源信息为'.var_export($media_val, true).'执行结果为'.var_export($result_add_op_queue,true));
                if($result_add_op_queue['ret']!=0)
                {
                    return $result_add_op_queue;
                }
            }
        }
        return $this->__return_data(0,'OK');
    }
    
    /**
     * 生成文件的注入ID
     * @param unknown $media_id
     * @param unknown $temp_media_data
     * @param unknown $transcode_model_val
     */
    public function file_encode_md5($media_id,$temp_media_data,$transcode_model_val)
    {
        $arr_media_info =  array(
            'nns_filetype',
            'nns_tag',
            'nns_mode',
            'nns_kbps',
        );
        $arr_transcode_model_info =  array(
            'manufacturer_mark',
            'manufacturer_model_file_path',
            'nns_output_file_type',
            'nns_start_bitrate',
            'nns_end_bitrate',
            'nns_video_config',
            'nns_audio_config',
            'nns_drawing_frame',
            'nns_drawing_type',
            'nns_img_type',
            'nns_drawing_rule',
        );
        $temp_info = $media_id.'_';
        foreach ($arr_media_info as $val)
        {
            if(isset($temp_media_data[$val]))
            {
                $temp_info.=$temp_media_data[$val];
            }
        }
        foreach ($arr_transcode_model_info as $model_val)
        {
            if(isset($transcode_model_val[$model_val]))
            {
                $temp_info.=$transcode_model_val[$model_val];
            }
        }
        return md5($temp_info);
    }
    
    /**
     * 添加转码队列
     * @param unknown $media_info
     * @param unknown $nns_op_id
     * @param unknown $nns_in_media_id
     * @param unknown $task_info
     * @param unknown $transcode_model_val
     */
    private function add_file_encode($media_info,$nns_op_id,$nns_in_media_id,$task_info,$data_media_file_info)
    {
        if($task_info['nns_video_type'] == '0')
        {
            if($media_info['nns_media_type'] == '2')
            {
                $nns_type  = 2;
            }
            else
            {
                $nns_type = 0;
            }
        }
        else
        {
            $nns_type = 1;
        }
        $data_file_encode_info = array(
            'nns_id'=>np_guid_rand('file_encode'),
            'nns_sp_id'=>$task_info['nns_org_id'],
            'nns_cp_id'=>$media_info['nns_cp_id'],
            'nns_op_id'=>$nns_op_id,
            'nns_in_media_id'=>$nns_in_media_id,
            'nns_queue_name'=>$task_info['nns_task_name'],
            'nns_out_media_id'=>$media_info['nns_id'],
            'nns_model_id'=>$data_media_file_info['model_id'],
            'nns_priority'=>$task_info['nns_priority'],
            'nns_media_type'=>$task_info['nns_video_type'],
            'nns_type'=>$nns_type,
            'nns_state'=>0,
            'nns_file_url'=>json_encode($data_media_file_info['file']),
        );
        $result_unique = nl_file_encode::query_unique_by_index_out($this->obj_dc, $data_file_encode_info['nns_sp_id'],$data_file_encode_info['nns_out_media_id']);
        if($result_unique['ret'] !=0)
        {
            return $result_unique;
        }
        
        $media_file_url = isset($this->arr_sp_config[$task_info['nns_org_id']]['nns_config']['clip_file_addr_input']) ? $this->arr_sp_config[$task_info['nns_org_id']]['nns_config']['clip_file_addr_input'] : '';
        $drawing_frame_file_url = isset($this->arr_sp_config[$task_info['nns_org_id']]['nns_config']['drawing_frame_addr_input']) ? $this->arr_sp_config[$task_info['nns_org_id']]['nns_config']['drawing_frame_addr_input'] : '';
        $media_file_url = trim(rtrim(rtrim($media_file_url,'/'),'\\'));
        $drawing_frame_file_url = trim(rtrim(rtrim($drawing_frame_file_url,'/'),'\\'));
        if(strlen($media_file_url) <1)
        {
            return $this->__return_data(1,'SP配置media_ftp为空');
        }
        foreach ($data_media_file_info['file'] as $key=>$val)
        {
            if(!in_array($key, array('media','drawing_frame')))
            {
                continue;
            }
            if($key == 'drawing_frame')
            {
                if(strlen($drawing_frame_file_url) <1)
                {
                    return $this->__return_data(1,'SP配置img_ftp为空');
                }
                foreach ($val as $_v)
                {
                    $_v = trim(ltrim(ltrim($_v,'/'),'\\'));
                    if(strlen($_v) <1)
                    {
                        continue;
                    }
                    $result_make = $this->make_ftp_file_url($drawing_frame_file_url.'/'.$_v);
                    if($result_make['ret'] !=0)
                    {
                        return $result_make;
                    }
                }
            }
            else
            {
                $val = trim(ltrim(ltrim($val['out'],'/'),'\\'));
                if(strlen($val) <1)
                {
                    continue;
                }
                $result_make = $this->make_ftp_file_url($media_file_url.'/'.$val);
                if($result_make['ret'] !=0)
                {
                    return $result_make;
                }
            }
        }
        $result_add = nl_file_encode::add($this->obj_dc, $data_file_encode_info);
        if($result_add['ret'] !=0)
        {
            return $result_add;
        }
        if(isset($result_unique['data_info']) && is_array($result_unique['data_info']) && !empty($result_unique['data_info']))
        {
            nl_file_encode::delete($this->obj_dc, $result_unique['data_info']['nns_id']);
        }
        return $this->__return_data(2,'OK');
    }
    
    /**
     * 创建文件夹
     * @param unknown $ftp_url
     * @return multitype:number
     */
    private function make_ftp_file_url($ftp_url)
    {
        if(strlen($ftp_url) <1)
        {
            return $this->_make_return_data(1,'FTP url 地址为空不创建目录');
        }
        $arr_ftp_config = parse_url($ftp_url);
        $host_ftp = (isset($arr_ftp_config["host"]) && strlen($arr_ftp_config["host"]) > 0) ? $arr_ftp_config["host"] : '';
        $user_ftp = (isset($arr_ftp_config["user"]) && strlen($arr_ftp_config["user"]) > 0) ? $arr_ftp_config["user"] : '';
        $pass_ftp = (isset($arr_ftp_config["pass"]) && strlen($arr_ftp_config["pass"]) > 0) ? $arr_ftp_config["pass"] : '';
        $path_ftp = (isset($arr_ftp_config["path"]) && strlen($arr_ftp_config["path"]) > 0) ? $arr_ftp_config["path"] : '';
        $port_ftp = (isset($arr_ftp_config["port"]) && strlen($arr_ftp_config["port"]) > 0) ? $arr_ftp_config["port"] : 21;
        $obj_ftp = new nl_ftp($host_ftp, $user_ftp, $pass_ftp,$port_ftp,30);
        $arr_pathinfo = pathinfo($path_ftp);
        $remote_path = isset($arr_pathinfo['dirname']) ? trim(trim(trim($arr_pathinfo['dirname'],'/'),'\\')) : '';
        if(strlen($remote_path) <1)
        {
            return $this->__return_data(1,'创建FTP路径地址为空');
        }
        return $obj_ftp->make_dir($remote_path);
    }
    
    /**
     * 添加OP queue队列
     */
    private function add_op_queue($arr_op_queue,$media_info)
    {
//         $arr_op_queue['nns_id'] = np_guid_rand('op_queue');
        $arr_op_queue['nns_action'] = 'add';
        $arr_op_queue['nns_media_id'] = $media_info['nns_id'];
        $arr_op_queue['nns_ex_data'] = $media_info['nns_ext_info'];
        $result_op_queue_exsist = nl_op_queue::query_op_queue_by_unique($this->obj_dc, $arr_op_queue['nns_type'], $arr_op_queue['nns_org_id'], $arr_op_queue['nns_action'], $arr_op_queue['nns_video_id'], $arr_op_queue['nns_index_id'], $arr_op_queue['nns_media_id']);
        if($result_op_queue_exsist['ret'] !=0)
        {
            return $result_op_queue_exsist;
        }
        if(isset($result_op_queue_exsist['data_info']) && is_array($result_op_queue_exsist['data_info']) && !empty($result_op_queue_exsist['data_info']))
        {
            return $this->__return_data(0,'ok',$result_op_queue_exsist['data_info']['nns_id']);
        }
        $arr_op_queue['nns_status'] = 8;
        $arr_op_queue['nns_is_group'] = 0;
        $result = nl_op_queue::add($this->obj_dc, $arr_op_queue);
        if($result['ret'] !=0)
        {
            return $result;
        }
        return $this->__return_data(0,'ok',$arr_op_queue['nns_id']);
    }
    
    /**
     * 添加点播片源及片源扩展信息
     * @param unknown $temp_media_data
     * @param unknown $transcode_model_val
     */
    private function add_media_and_ex($media_data,$data_img_drawing_frame=null)
    {
        $nns_media_guid = $media_data['nns_id'];
        if(!is_array($media_data))
        {
            return $this->__return_data(1,'数据为空');
        }
        $result_media_exsist = nl_vod_media_v2::query_media_org_exsist($this->obj_dc, $media_data['nns_import_id'], $media_data['nns_cp_id'],$media_data['nns_import_source']);
        if($result_media_exsist['ret'] !='0')
        {
            return $result_media_exsist;
        }
        if(!isset($result_media_exsist['data_info']) || !is_array($result_media_exsist['data_info']) || empty($result_media_exsist['data_info']))
        {
            $result_media_add = nl_vod_media_v2::add($this->obj_dc, $media_data);
            if($result_media_add['ret'] !=0)
            {
                return $result_media_add;
            }
        }
        else
        {
            $nns_media_guid = $result_media_exsist['data_info']['nns_id'];
        }
        $result_media_ex = nl_vod_media_v2::query_ex_by_id($this->obj_dc, $media_data['nns_import_id'], $media_data['nns_cp_id']);
        if($result_media_ex['ret'] !='0')
        {
            return $result_media_ex;
        }
        $result_media_ex = isset($result_media_ex['data_info']) ? $result_media_ex['data_info'] : null;
        if(is_array($result_media_ex) && !empty($result_media_ex))
        {
            foreach ($result_media_ex as $media_ex_val)
            {
                if(strlen($media_ex_val['nns_key']) <1 || strpos($media_ex_val['nns_key'], 'drawing_frame') !== false)
                {
                    continue;
                }
                $data_img_drawing_frame[$media_ex_val['nns_key']] = $media_ex_val['nns_value'];
            }
        }
        if(is_array($data_img_drawing_frame) && !empty($data_img_drawing_frame))
        {
            foreach ($data_img_drawing_frame as $img_drawing_frame_key=>$img_drawing_frame_val)
            {
                if(strlen($media_ex_val['nns_key']) <1)
                {
                    continue;
                }
                $result_media_ex_exsist = nl_vod_media_v2::query_ex_by_id_and_key($this->obj_dc, $media_data['nns_import_id'], $media_data['nns_cp_id'],$img_drawing_frame_key);
                if($result_media_exsist['ret'] !='0')
                {
                    return $result_media_exsist;
                }
                $result_execute = $this->__return_data(0,'ok');
                if(isset($result_media_exsist['data_info'][0]['nns_value']) && $result_media_exsist['data_info'][0]['nns_value'] != $img_drawing_frame_val)
                {
                    $result_execute = nl_vod_media_v2::edit_ex($this->obj_dc, $media_data['nns_import_id'], $media_data['nns_cp_id'], $img_drawing_frame_key, $img_drawing_frame_val);
                }
                else if(!isset($result_media_exsist['data_info']) || empty($result_media_exsist['data_info']) || !is_array($result_media_exsist['data_info']))
                {
                    $result_execute = nl_vod_media_v2::add_ex($this->obj_dc, $media_data['nns_import_id'], $media_data['nns_cp_id'], $img_drawing_frame_key, $img_drawing_frame_val);
                }
                if($result_execute['ret'] !=0)
                {
                    return $result_execute;
                }
            }
        }
        return $this->__return_data(0,'ok',$nns_media_guid);
    }
    
    /**
     * 生产ftp文件路径
     * @param unknown $ftp_in
     * @param unknown $ftp_out
     */
    private function make_ftp_dir($path_in,$ftp_out)
    {
        //拆分FTP地址
        $url_arr_out = parse_url($ftp_out);
        $host_out = (isset($url_arr_out["host"]) && strlen($url_arr_out["host"]) > 0) ? $url_arr_out["host"] : '';
        $user_out = (isset($url_arr_out["user"]) && strlen($url_arr_out["user"]) > 0) ? $url_arr_out["user"] : '';
        $pass_out = (isset($url_arr_out["pass"]) && strlen($url_arr_out["pass"]) > 0) ? $url_arr_out["pass"] : '';
        $path_out = (isset($url_arr_out["path"]) && strlen($url_arr_out["path"]) > 0) ? $url_arr_out["path"] : '';
        $port_out = (isset($url_arr_out["port"]) && strlen($url_arr_out["port"]) > 0) ? $url_arr_out["port"] : 21;
        $obj_ftp = new nl_ftp($host_out, $user_out, $pass_out,$port_out);
        $path_out = trim(trim($path_out,'/'));
        $path_base = (strlen($path_out) >0) ? $path_out : '';
        $arr_path_info = pathinfo($path_in);
        if(isset($arr_path_info['dirname']))
        {
            $arr_path_info['dirname'] = trim($arr_path_info['dirname'],'/');
            if(strlen($arr_path_info['dirname']))
            {
                $path_base.='/'.$arr_path_info['dirname'];
            }
        }
        $path_base = trim($path_base,'/');
        $str_ftp_url = "ftp://";
        if(strlen($user_out) > 0 && strlen($pass_out) > 0 )
        {
            $str_ftp_url.="{$user_out}:{$pass_out}@";
        }
        $str_ftp_url.="{$host_out}:{$port_out}/{$path_base}/{$arr_path_info['filename']}";
        $this->arr_inout_put['output']['user']['value'] = $user_out;
        $this->arr_inout_put['output']['pwd']['value'] = $pass_out;
        $this->arr_inout_put['output']['path']['value'] = $str_ftp_url;
        return $obj_ftp->make_dir($path_base);
    }
}