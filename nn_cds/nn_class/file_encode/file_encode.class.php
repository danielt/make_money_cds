<?php
include_once dirname(__FILE__).'/file_encode.func.php';
class nl_file_encode_class
{
	private $arr_inout_put =array (
			'output' => array (
					'user' => array (
							'fullName' => null,
							'name' => 'user',
							'desc' => '',
							'value' => 'trans_file'
					),
					'pwd' => array (
							'fullName' => null,
							'name' => 'pwd',
							'desc' => '',
							'value' => 'starcor1234'
					),
					'path' => array (
							'fullName' => null,
							'name' => 'path',
							'desc' => '转码输出路径',
							'value' => 'ftp://trans_file:starcor1234@192.168.90.79:21/test/mgtv_3a32469c179474f44a8be6c8fdc808f49'
					),
					'nameFormat' => array (
							'fullName' => null,
							'name' => 'nameFormat',
							'desc' => '转码输出命名规则',
							'value' => '$input'
					)
			),
			'input' => array (
					'user' => array (
							'fullName' => null,
							'name' => 'user',
							'desc' => '',
							'value' => 'source_file'
					),
					'pwd' => array (
							'fullName' => null,
							'name' => 'pwd',						
					        'desc' => '',
							'value' => 'starcor1234'
					),
					'path' => array (
							'fullName' => null,
							'name' => 'path',
							'desc' => '转码输入文件路径',
							'value' => 'ftp://source_file:starcor1234@192.168.90.79:21/test/mgtv_3a32469c179474f44a8be6c8fdc808f49.ts'
					),
					'traversal' => array (
							'fullName' => null,
							'name' => 'traversal',
							'desc' => '是否遍历子目录',
							'value' => '0'
					),
					'aterTsc' => array (
							'success' => array (
									'action' => array (
											'fullName' => null,
											'name' => 'action',
											'desc' => '转码成功后，输入文件的处理方法',
											'value' => 'keep'
									),
									'mvToPath' => array (
											'fullName' => null,
											'name' => 'mvToPath',
											'desc' => '转码成功后，移动源文件的目标路径',
											'value' => ''
									)
							),
							'failed' => array (
									'action' => array (
											'fullName' => null,
											'name' => 'action',
											'desc' => '转码失败后，输入文件的处理方法',
											'value' => 'keep'
									),
									'mvToPath' => array (
											'fullName' => null,
											'name' => 'mvToPath',
											'desc' => '转码失败后，移动源文件的目标路径',
											'value' => ''
									),
							),
					),
			),
	);
	//视屏 配置
	private $arr_video_model = array (
			'general' => array (
					'enable' => array (
							'fullName' => null, 
							'name' => 'enable', 
							'desc' => '视频使能', 
							'value' => '1'
					), 
					'codec' => array (
							'fullName' => null, 
							'name' => 'codec', 
							'desc' => '视频编码格式', 
							'value' => 'h264'
					), 
					'frameRate' => array (
							'fullName' => null, 
							'name' => 'frameRate', 
							'desc' => '视频帧率', 
							'value' => '25'
					), 
					'scale' => array (
							'fullName' => null, 
							'name' => 'scale', 
							'desc' => '视频缩放比例', 
							'value' => '0'
					), 
					'width' => array (
							'fullName' => null, 
							'name' => 'width', 
							'desc' => '视频宽度', 
							'value' => '1920'
					), 
					'height' => array (
							'fullName' => null, 
							'name' => 'height', 
							'desc' => '视频高度', 
							'value' => '1080'
					), 
					'aspectRatio' => array (
							'fullName' => null, 
							'name' => 'aspectRatio', 
							'desc' => '视频宽高比', 
							'value' => 'copy'
					), 
					'fillMode' => array (
							'fullName' => null, 
							'name' => 'fillMode', 
							'desc' => '视频宽高比', 
							'value' => 'stretch'
					), 
					'bc' => array (
							'rate' => array (
									'fullName' => null, 
									'name' => 'rate', 
									'desc' => 'CBR时的视频总码率', 
									'value' => '2000'
							), 
							'maxRate' => array (
									'fullName' => null, 
									'name' => 'maxRate', 
									'desc' => 'VBR时的最大视频码率', 
									'value' => ''
							), 
							'minRate' => array (
									'fullName' => null, 
									'name' => 'minRate', 
									'desc' => 'VBR时的最小视频码率', 
									'value' => ''
							), 
							'method' => array (
									'fullName' => null, 
									'name' => 'method', 
									'desc' => '码率控制方法', 
									'value' => 'cbr'
							)
					)
			), 
			'h264' => array (
					'basic' => array (
							'profile' => array (
									'fullName' => null, 
									'name' => 'profile', 
									'desc' => '编码格式的profile', 
									'value' => 'high'
							), 
							'pixelFormat' => array (
									'fullName' => null, 
									'name' => 'pixelFormat', 
									'desc' => '像素格式', 
									'value' => 'yuv420'
							), 
							'level' => array (
									'fullName' => null, 
									'name' => 'level', 
									'desc' => '编码格式的level', 
									'value' => 'auto'
							), 
							'entropyEncoder' => array (
									'fullName' => null, 
									'name' => 'entropyEncoder', 
									'desc' => '使用的熵编码方法，vlc或者ac', 
									'value' => 'ac'
							), 
							'partitions' => array (
									'fullName' => null, 
									'name' => 'partitions', 
									'desc' => '图像编码分区方法', 
									'value' => 'ac'
							), 
							'scanMode' => array (
									'type' => array (
											'fullName' => null, 
											'name' => 'type', 
											'desc' => '输出视频扫描模式', 
											'value' => 'i'
									), 
									'fieldOrder' => array (
											'fullName' => null, 
											'name' => 'fieldOrder', 
											'desc' => '隔行扫描场序', 
											'value' => 'tt'
									)
							)
					), 
					'me' => array (
							'method' => array (
									'fullName' => null, 
									'name' => 'method', 
									'desc' => '运动估计方法', 
									'value' => 'hex'
							), 
							'range' => array (
									'fullName' => null, 
									'name' => 'range', 
									'desc' => '运动搜索范围', 
									'value' => '16'
							), 
							'subMe' => array (
									'fullName' => null, 
									'name' => 'subMe', 
									'desc' => '亚像素运动估计', 
									'value' => '7'
							), 
							'directPred' => array (
									'fullName' => null, 
									'name' => 'directPred', 
									'desc' => 'B帧内采用的运动侦测的方式', 
									'value' => 'spatial'
							)
					), 
					'bc' => array (
							'crf' => array (
									'fullName' => null, 
									'name' => 'crf', 
									'desc' => '恒定质量参数', 
									'value' => '23'
							), 
							'crfMax' => array (
									'fullName' => null, 
									'name' => 'crfMax', 
									'desc' => '最大恒定质量参数', 
									'value' => '24'
							), 
							'qp' => array (
									'fullName' => null, 
									'name' => 'qp', 
									'desc' => '量化参数', 
									'value' => '23'
							)
					), 
					'frame' => array (
							'gop' => array (
									'format' => array (
											'fullName' => null, 
											'name' => 'format', 
											'desc' => 'gop格式', 
											'value' => 'close'
									), 
									'min' => array (
											'fullName' => null, 
											'name' => 'min', 
											'desc' => '最小I帧间隔', 
											'value' => '25'
									), 
									'max' => array (
											'fullName' => null, 
											'name' => 'max', 
											'desc' => '最大I帧间隔', 
											'value' => '25'
									), 
									'maxBFrameCount' => array (
											'fullName' => null, 
											'name' => 'maxBFrameCount', 
											'desc' => '两个非B帧之间允许的最大B帧数', 
											'value' => '3'
									)
							), 
							'refFrame' => array (
									'count' => array (
											'fullName' => null, 
											'name' => 'count', 
											'desc' => '参考帧数量', 
											'value' => '3'
									), 
									'bFrameAsRef' => array (
											'fullName' => null, 
											'name' => 'bFrameAsRef', 
											'desc' => '允许使用B帧作为参考帧的设置', 
											'value' => 'none'
									), 
									'weightB' => array (
											'fullName' => null, 
											'name' => 'weightB', 
											'desc' => '设置是否允许B帧加权预测', 
											'value' => '1'
									), 
									'weightP' => array (
											'fullName' => null, 
											'name' => 'weightP', 
											'desc' => 'P帧加权预测方式', 
											'value' => 'smart'
									), 
									'sceneCut' => array (
											'fullName' => null, 
											'name' => 'sceneCut', 
											'desc' => '开启场景切换检测', 
											'value' => '1'
									)
							)
					)
			), 
			'mp4' => array (
					'basic' => array (
							'profile' => array (
									'fullName' => null, 
									'name' => 'profile', 
									'desc' => '编码格式的profile', 
									'value' => 'simple'
							), 
							'scanMode' => array (
									'type' => array (
											'fullName' => null, 
											'name' => 'type', 
											'desc' => '输出视频扫描模式', 
											'value' => 'p'
									), 
									'fieldOrder' => array (
											'fullName' => null, 
											'name' => 'fieldOrder', 
											'desc' => '隔行扫描场序', 
											'value' => 'auto'
									)
							)
					), 
					'frame' => array (
							'gop' => array (
									'max' => array (
											'fullName' => null, 
											'name' => 'max', 
											'desc' => '最大I帧间隔', 
											'value' => '250'
									), 
									'maxBFrameCount' => array (
											'fullName' => null, 
											'name' => 'maxBFrameCount', 
											'desc' => '两个非B帧之间允许的最大B帧数', 
											'value' => '3'
									)
							), 
							'refFrame' => array (
									'count' => array (
											'fullName' => null, 
											'name' => 'count', 
											'desc' => '参考帧数量', 
											'value' => '3'
									)
							)
					)
			), 
			'mpeg2' => array (
					'basic' => array (
							'profile' => array (
									'fullName' => null, 
									'name' => 'profile', 
									'desc' => '编码格式的profile', 
									'value' => 'main'
							), 
							'pixelFormat' => array (
									'fullName' => null, 
									'name' => 'pixelFormat', 
									'desc' => '像素格式', 
									'value' => 'yuv420'
							), 
							'scanMode' => array (
									'type' => array (
											'fullName' => null, 
											'name' => 'type', 
											'desc' => '输出视频扫描模式', 
											'value' => 'p'
									), 
									'fieldOrder' => array (
											'fullName' => null, 
											'name' => 'fieldOrder', 
											'desc' => '隔行扫描场序', 
											'value' => 'auto'
									)
							)
					), 
					'frame' => array (
							'gop' => array (
									'max' => array (
											'fullName' => null, 
											'name' => 'max', 
											'desc' => '最大I帧间隔', 
											'value' => '250'
									), 
									'maxBFrameCount' => array (
											'fullName' => null, 
											'name' => 'maxBFrameCount', 
											'desc' => '两个非B帧之间允许的最大B帧数', 
											'value' => '3'
									)
							), 
							'refFrame' => array (
									'count' => array (
											'fullName' => null, 
											'name' => 'count', 
											'desc' => '参考帧数量', 
											'value' => '3'
									)
							)
					)
			), 
			'wmv' => array (
					'basic' => array (
							'scanMode' => array (
									'type' => array (
											'fullName' => null, 
											'name' => 'type', 
											'desc' => '输出视频扫描模式', 
											'value' => 'p'
									), 
									'fieldOrder' => array (
											'fullName' => null, 
											'name' => 'fieldOrder', 
											'desc' => '隔行扫描场序', 
											'value' => 'auto'
									)
							)
					), 
					'frame' => array (
							'gop' => array (
									'max' => array (
											'fullName' => null, 
											'name' => 'max', 
											'desc' => '最大I帧间隔', 
											'value' => '250'
									)
							), 
							'refFrame' => array (
									'count' => array (
											'fullName' => null, 
											'name' => 'count', 
											'desc' => '参考帧数量', 
											'value' => '3'
									)
							)
					)
			), 
			'avs' => array (), 
			'h265' => array ()
	);
	//音频 配置
	private $arr_audio_model = array (
			'enable' => array (
					'fullName' => null, 
					'name' => 'enable', 
					'desc' => '音频编码使能', 
					'value' => '1'
			), 
			'codec' => array (
					'fullName' => null, 
					'name' => 'codec', 
					'desc' => '音频编码格式', 
					'value' => 'mp2'
			), 
			'kbps' => array (
					'fullName' => null, 
					'name' => 'kbps', 
					'desc' => '音频码率', 
					'value' => '192'
			), 
			'hz' => array (
					'fullName' => null, 
					'name' => 'hz', 
					'desc' => '音频采样率', 
					'value' => '48000'
			), 
			'channels' => array (
					'fullName' => null, 
					'name' => 'channels', 
					'desc' => '声道', 
					'value' => 'stereo'
			), 
			'tracks' => array (
					'fullName' => null, 
					'name' => 'tracks', 
					'desc' => '音轨', 
					'value' => '0'
			), 
			'multiOutput' => array (array(
					'volume' => array (
							'fullName' => null, 
							'name' => 'volume', 
							'desc' => '音量调整', 
							'value' => '0'
					), 
					'codec' => array (
							'fullName' => null, 
							'name' => 'codec', 
							'desc' => '音频编码格式', 
							'value' => 'mp2'
					), 
					'bitrate' => array (
							'fullName' => null, 
							'name' => 'bitrate', 
							'desc' => '音频码率', 
							'value' => '192'
					), 
					'ar' => array (
							'fullName' => null, 
							'name' => 'ar', 
							'desc' => '音频采样率', 
							'value' => '48000'
					), 
					'channels' => array (
							'fullName' => null, 
							'name' => 'channels', 
							'desc' => '声道', 
							'value' => 'stereo'
					)
					)
			)
	);
	//转码复用 配置
	private $arr_mux_model = array (
			'basic' => array (
					'serviceProvider' => array (
							'fullName' => null, 
							'name' => 'serviceProvider', 
							'desc' => '服务提供商', 
							'value' => ''
					), 
					'serviceName' => array (
							'fullName' => null, 
							'name' => 'serviceName', 
							'desc' => '服务名称', 
							'value' => ''
					), 
					'rate' => array (
							'fullName' => null, 
							'name' => 'rate', 
							'desc' => '复用码率', 
							'value' => ''
					), 
					'extension' => array (
							'fullName' => null, 
							'name' => 'extension', 
							'desc' => '封装格式', 
							'value' => 'ts'
					)
			), 
			'fmt' => array (
					'ts' => array (
							'pmtPid' => array (
									'fullName' => null, 
									'name' => 'pmtPid', 
									'desc' => 'PMT PID', 
									'value' => '0x1000'
							), 
							'programId' => array (
									'fullName' => null, 
									'name' => 'programId', 
									'desc' => '节目ID', 
									'value' => '0x0001'
							), 
							'pcrPid' => array (
									'fullName' => null, 
									'name' => 'pcrPid', 
									'desc' => 'PCR PID', 
									'value' => '0x0064'
							), 
							'videoPid' => array (
									'fullName' => null, 
									'name' => 'videoPid', 
									'desc' => '视频PID', 
									'value' => '0x0064'
							), 
							'audioPid' => array (
									'fullName' => null, 
									'name' => 'audioPid', 
									'desc' => '音频PID', 
									'value' => '0x0065'
							)
					), 
					'tsParam' => array (
							'pmtPid' => array (
									'fullName' => null, 
									'name' => 'pmtPid', 
									'desc' => 'PMT PID', 
									'value' => '0x1000'
							), 
							'videoPid' => array (
									'fullName' => null, 
									'name' => 'videoPid', 
									'desc' => '视频PID', 
									'value' => '0x0064'
							), 
							'audioPid' => array (
									'fullName' => null, 
									'name' => 'audioPid', 
									'desc' => '音频PID', 
									'value' => '0x0065'
							)
					), 
					'hls' => array (
							'pmtPid' => array (
									'fullName' => null, 
									'name' => 'pmtPid', 
									'desc' => 'PMT PID', 
									'value' => '0x1000'
							), 
							'videoPid' => array (
									'fullName' => null, 
									'name' => 'videoPid', 
									'desc' => '视频PID', 
									'value' => '0x0064'
							), 
							'audioPid' => array (
									'fullName' => null, 
									'name' => 'audioPid', 
									'desc' => '音频PID', 
									'value' => '0x0065'
							), 
							'segmentLength' => array (
									'fullName' => null, 
									'name' => 'segmentLength', 
									'desc' => 'HLS分段长度', 
									'value' => ''
							)
					), 
					'segmentLength' => array (
							'fullName' => null, 
							'name' => 'segmentLength', 
							'desc' => 'HLS分段长度', 
							'value' => ''
					)
			)
	);
	//转码扩展 配置
	private $arr_ext_model = array (
			'subtitle' => array (
					'user' => array (
							'fullName' => null, 
							'name' => 'user', 
							'desc' => '', 
							'value' => ''
					), 
					'pwd' => array (
							'fullName' => null, 
							'name' => 'pwd', 
							'desc' => '', 
							'value' => ''
					), 
					'enable' => array (
							'fullName' => null, 
							'name' => 'enable', 
							'desc' => '是否开启字幕叠加功能', 
							'value' => '0'
					), 
					'mode' => array (
							'fullName' => null, 
							'name' => 'mode', 
							'desc' => '字幕加载方式，自动加载会根据源文件名称智能搜索字幕文件，手动加载需要指定字幕文件', 
							'value' => 'manual'
					), 
					'path' => array (
							'fullName' => null, 
							'name' => 'path', 
							'desc' => '字幕机文件存放目录', 
							'value' => ''
					), 
					'intSub' => array (
							'fullName' => null, 
							'name' => 'intSub', 
							'desc' => '使用内置字幕轨道', 
							'value' => ''
					)
			), 
			'logo' => array (
					'enable' => array (
							'fullName' => null, 
							'name' => 'enable', 
							'desc' => '是否开启图片叠加功能', 
							'value' => '0'
					), 
					'user' => array (
							'fullName' => null, 
							'name' => 'user', 
							'desc' => '', 
							'value' => ''
					), 
					'pwd' => array (
							'fullName' => null, 
							'name' => 'pwd', 
							'desc' => '', 
							'value' => ''
					), 
					'x' => array (
							'fullName' => null, 
							'name' => 'x', 
							'desc' => '图片叠加位置左上角的x轴坐标', 
							'value' => '0'
					), 
					'y' => array (
							'fullName' => null, 
							'name' => 'y', 
							'desc' => '图片叠加位置左上角的y轴坐标', 
							'value' => '0'
					), 
					'width' => array (
							'fullName' => null, 
							'name' => 'width', 
							'desc' => '图片宽度', 
							'value' => '0'
					), 
					'height' => array (
							'fullName' => null, 
							'name' => 'height', 
							'desc' => '图片高度', 
							'value' => '0'
					), 
					'file' => array (
							'fullName' => null, 
							'name' => 'file', 
							'desc' => '图片文件url', 
							'value' => ''
					), 
					'mode' => array (
							'fullName' => null, 
							'name' => 'mode', 
							'desc' => '图片叠加到输入或输出上', 
							'value' => 'out'
					), 
					'multiLogo' => null
			), 
			'delogo' => array (
					'enable' => array (
							'fullName' => null, 
							'name' => 'enable', 
							'desc' => '是否开启去台标功能', 
							'value' => '0'
					), 
					'x' => array (
							'fullName' => null, 
							'name' => 'x', 
							'desc' => '台标区域左上角x轴坐标', 
							'value' => '0'
					), 
					'y' => array (
							'fullName' => null, 
							'name' => 'y', 
							'desc' => '台标区域左上角y轴坐标', 
							'value' => '0'
					), 
					'width' => array (
							'fullName' => null, 
							'name' => 'width', 
							'desc' => '台标区域宽度', 
							'value' => '10'
					), 
					'height' => array (
							'fullName' => null, 
							'name' => 'height', 
							'desc' => '台标区域高度', 
							'value' => '10'
					), 
					'band' => array (
							'fullName' => null, 
							'name' => 'band', 
							'desc' => '台标区域的模糊程度', 
							'value' => '2'
					), 
					'multiDelogo' => null
			), 
			'brightness' => array (
					'adjustment' => array (
							'fullName' => null, 
							'name' => 'adjustment', 
							'desc' => '亮度调节值', 
							'value' => ''
					)
			), 
			'contrast' => array (
					'adjustment' => array (
							'fullName' => null, 
							'name' => 'adjustment', 
							'desc' => '对比度调节值', 
							'value' => ''
					)
			), 
			'saturation' => array (
					'adjustment' => array (
							'fullName' => null, 
							'name' => 'adjustment', 
							'desc' => '饱和度调节值', 
							'value' => ''
					)
			), 
			'audioDelay' => array (
					'adjustment' => array (
							'fullName' => null, 
							'name' => 'adjustment', 
							'desc' => '音频延迟', 
							'value' => ''
					)
			)
	);
	//输入输出 配置
	private $arr_inout_config = null;
	//临时数组模板
	private $temp_arr_model = null;
	//转码分发策略
	private $task_distribute = null;

	public function __construct($task_id,$task_distribute ,$arr_inout_put=null, $arr_video = null, $arr_audio = null, $arr_mux = null, $arr_ext = null)
	{
		$this->temp_arr_model = array(
				'report'=>'0',
				'replyKey'=>$task_id,
				'cmdBody'=>array(
						'cmd'=>$task_distribute,
						'task'=>array('extData'=>null,)
				),
		);
		$this->task_distribute = $task_distribute;
		$this->arr_inout_config = $arr_inout_put;
	}

	/**
	 * 解析输入输出参数
	 */
	private function parse_inout()
	{
	    $this->arr_inout_put['input']['user']['value'] = isset($this->arr_inout_config['input']['user']) ? $this->arr_inout_config['input']['user'] : '';
	    $this->arr_inout_put['input']['pwd']['value'] = isset($this->arr_inout_config['input']['pwd']) ? $this->arr_inout_config['input']['pwd'] : '';
	    $this->arr_inout_put['input']['path']['value'] = isset($this->arr_inout_config['input']['path']) ? $this->arr_inout_config['input']['path'] : '';
	    $this->arr_inout_put['output']['user']['value'] = isset($this->arr_inout_config['output']['user']) ? $this->arr_inout_config['output']['user'] : '';
	    $this->arr_inout_put['output']['pwd']['value'] = isset($this->arr_inout_config['output']['pwd']) ? $this->arr_inout_config['output']['pwd'] : '';
	    $this->arr_inout_put['output']['path']['value'] = isset($this->arr_inout_config['output']['path']) ? $this->arr_inout_config['output']['path'] : '';
	}
	
	/**
	 * 生产ftp文件路径
	 * @param unknown $ftp_in
	 * @param unknown $ftp_out
	 */
	private function make_ftp_dir($path_in,$ftp_out)
	{
	    //拆分FTP地址
	    $url_arr_out = parse_url($ftp_out);
	    $host_out = (isset($url_arr_out["host"]) && strlen($url_arr_out["host"]) > 0) ? $url_arr_out["host"] : '';
	    $user_out = (isset($url_arr_out["user"]) && strlen($url_arr_out["user"]) > 0) ? $url_arr_out["user"] : '';
	    $pass_out = (isset($url_arr_out["pass"]) && strlen($url_arr_out["pass"]) > 0) ? $url_arr_out["pass"] : '';
	    $path_out = (isset($url_arr_out["path"]) && strlen($url_arr_out["path"]) > 0) ? $url_arr_out["path"] : '';
	    $port_out = (isset($url_arr_out["port"]) && strlen($url_arr_out["port"]) > 0) ? $url_arr_out["port"] : 21;
	    $obj_ftp = new nl_ftp($host_out, $user_out, $pass_out,$port_out);
	    $path_out = trim(trim($path_out,'/'));
	    $path_base = (strlen($path_out) >0) ? $path_out : '';
	    $arr_path_info = pathinfo($path_in);
	    if(isset($arr_path_info['dirname']))
	    {
	        $arr_path_info['dirname'] = trim($arr_path_info['dirname'],'/');
	        if(strlen($arr_path_info['dirname']))
	        {
	            $path_base.='/'.$arr_path_info['dirname'];
	        }
	    }
	    $path_base = trim($path_base,'/');
	    $str_ftp_url = "ftp://";
	    if(strlen($user_out) > 0 && strlen($pass_out) > 0 )
	    {
	        $str_ftp_url.="{$user_out}:{$pass_out}@";
	    }
	    $str_ftp_url.="{$host_out}:{$port_out}/{$path_base}/{$arr_path_info['filename']}";
	    $this->arr_inout_put['output']['user']['value'] = $user_out;
	    $this->arr_inout_put['output']['pwd']['value'] = $pass_out;
	    $this->arr_inout_put['output']['path']['value'] = $str_ftp_url;
	    return $obj_ftp->make_dir($path_base);
	}
	
	/**
	 * 转码策略分发
	 */
	public function task_distribute()
	{
	    switch ($this->task_distribute)
	    {
	        case TMS_SYS_GET_TASK_STATUS:
	            $str_func = 'get_task_state';
	            break;
	        case TMS_SYS_SET_TASK:
	            $str_func = 'set_task';
	            break;
            case TMS_SYS_DEL_TASK:
                $str_func = 'del_task';
                break;
            default:
                return $this->_return_data(1,'没有此转码方式');
	    }
	    return $this->$str_func();
	}
	
	/**
	 * 方法反馈
	 * @param unknown $ret
	 * @param string $reason
	 * @param string $data_info
	 */
	private function _return_data($ret,$reason='ok',$data_info=null)
	{
	    return array(
	        'ret'=>$ret,
	        'reason'=>$reason,
	        'data_info'=>$data_info
	    );
	}
	
	/**
	 * 上报转码数据
	 */
	private function set_task()
	{
	    $result_parase = $this->parse_inout();
		$this->temp_arr_model['cmdBody']['task']['inout']=$this->arr_inout_put;
		$this->temp_arr_model['cmdBody']['task']['video']=$this->arr_video_model;
		$this->temp_arr_model['cmdBody']['task']['audio']=$this->arr_audio_model;
		$this->temp_arr_model['cmdBody']['task']['mux']=$this->arr_mux_model;
		$this->temp_arr_model['cmdBody']['task']['ext']=$this->arr_ext_model;
		return $this->_return_data(0,'ok',json_encode($this->temp_arr_model));
	}
	/**
	 * 删除转码数据
	 */
	private function del_task()
	{
	    return $this->_return_data(0,'ok',json_encode($this->temp_arr_model));
	}
	
	/**
	 * 获取转码状态
	 */
	private function get_task_state()
	{
		 return $this->_return_data(0,'ok',json_encode($this->temp_arr_model));
	}
}