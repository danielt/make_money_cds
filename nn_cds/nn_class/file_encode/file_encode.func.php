<?php 
//转码 握手
define('TMS_SYS_HANDSHAKE', 'TMS_SYS_HANDSHAKE');
//转码 心跳
define('TMS_SYS_HEARTBEAT', 'TMS_SYS_HEARTBEAT');
//转码 获取系统状态
define('TMS_SYS_GET_STATUS', 'TMS_SYS_GET_STATUS');
//转码 设置普通转码任务
define('TMS_SYS_SET_TASK', 'TMS_SYS_SET_TASK');
//转码 设置多码率单输出 HLS 转码任务
define('TMS_SYS_SET_MULTI_TEMPLATES_HLS_TASK', 'TMS_SYS_SET_MULTI_TEMPLATES_HLS_TASK');
//转码  删除转码任务
define('TMS_SYS_DEL_TASK', 'TMS_SYS_DEL_TASK');
//转码 获取转码状态
define('TMS_SYS_GET_TASK_STATUS', 'TMS_SYS_GET_TASK_STATUS');

//转码  生成片源信息，注入片源队列
define('TMS_SYS_TASK_QUEUE', 'TMS_SYS_TASK_QUEUE');
function _comm_make_return_data($ret,$reason=null,$data=null,$page_data=null,$other_data=null)
{
    $temp_array=array(
        'ret'=>$ret,
        'reason'=>$reason,
        'data_info'=>null,
        'page_info'=>null,
        'ext_data'=>null
    );
    if($data !== null)
    {
        $temp_array['data_info']=$data;
    }
    if(!empty($page_data))
    {
        $temp_array['page_info']=$page_data;
    }
    if(!empty($other_data))
    {
        $temp_array['ext_data']=$other_data;
    }
    return $temp_array;
}