<?php
class nl_public_file_execute
{
    public $out_ex_type = null;
    //sp 配置信息
	public $sp_config = null;
    //参数
	public $arr_params = null;
	//切片shuzu
	public $arr_task = null;
	
	public $op_sp = null;
	
	public $dc = null;
	
	

	public $manufacturer = 'LeoVideo';
	public $manufacturer_api_url = '';
	
	public $in_out_type = array(
	    'ts' => array(
	        'HD' => array(
	            'Mobile-1080P-8Mbps' => array(
	                'name' => 'TS-PC-1080P-4Mbps',
	                'tag' => '16,',
	                'bitrate' => '8192',
	                'filetype' =>'ts',
	            ),
	            'PC-1080P-4Mbps' => array(
	                'name' => 'TS-Mobile-1080P-2Mbps',
	                'tag' => '27,28,29,30,',
	                'bitrate' => '4080',
	                'filetype' =>'ts',
	            ),
	            'Pad-1080P-2Mbps' => array(
	                'name' => 'TS-Mobile-1080P-2Mbps',
	                'tag' => '27,28,29,30,',
	                'bitrate' => '2048',
	                'filetype' =>'ts',
	            ),
	        ),
	        'STD' => array(
	            'Mobile-1080P-8Mbps' => array(
	                'name' => 'TS-PC-1080P-4Mbps',
	                'tag' => '16,',
	                'bitrate' => '8192',
	                'filetype' =>'ts',
	            ),
	            'PC-1080P-4Mbps' => array(
	                'name' => 'TS-Mobile-1080P-2Mbps',
	                'tag' => '27,28,29,30,',
	                'bitrate' => '4080',
	                'filetype' =>'ts',
	            ),
	            'Pad-1080P-2Mbps' => array(
	                'name' => 'TS-Mobile-1080P-2Mbps',
	                'tag' => '27,28,29,30,',
	                'bitrate' => '2048',
	                'filetype' =>'ts',
	            ),
	        ),
	        '4K' => array(
	            'Mobile-1080P-8Mbps' => array(
	                'name' => 'TS-PC-1080P-4Mbps',
	                'tag' => '16,',
	                'bitrate' => '8192',
	                'filetype' =>'ts',
	            ),
	            'PC-1080P-4Mbps' => array(
	                'name' => 'TS-Mobile-1080P-2Mbps',
	                'tag' => '27,28,29,30,',
	                'bitrate' => '4080',
	                'filetype' =>'ts',
	            ),
	            'Pad-1080P-2Mbps' => array(
	                'name' => 'TS-Mobile-1080P-2Mbps',
	                'tag' => '27,28,29,30,',
	                'bitrate' => '2048',
	                'filetype' =>'ts',
	            ),
	        ),
	    ),
	    'mp4' => array(
	        'HD' => array(
	        //                 'STB-32M-TS' => array(
	            //                     'name' => 'STB-32M-TS',
	            //                     'tag' => '26,',
	            //                     'bitrate' => '8192',
	            //                     'filetype' =>'ts',
	            //                 ),
	            'PC-1080P-4Mbps' => array(
	                'name' => 'PC-1080P-4Mbps',
	                'tag' => '31,',
	                'bitrate' => '4096',
	                'filetype' =>'mp4',
	            ),
	            'Pad-1080P-2Mbps' => array(
	                'name' => 'Mobile-1080P-2Mbps',
	                'tag' => '27,28,29,30,',
	                'bitrate' => '2048',
	                'filetype' =>'mp4',
	            ),
	        ),
	        'STD' => array(
	            'PC-1080P-4Mbps' => array(
	                'name' => 'PC-1080P-4Mbps',
	                'tag' => '31,',
	                'bitrate' => '4096',
	                'filetype' =>'mp4',
	            ),
	            'Pad-1080P-2Mbps' => array(
	                'name' => 'Mobile-1080P-2Mbps',
	                'tag' => '27,28,29,30,',
	                'bitrate' => '2048',
	                'filetype' =>'mp4',
	            ),
	        ),
	        '4K' => array(
	        //                 'STB-32M-TS' => array(
	            //                     'name' => 'STB-32M-TS',
	            //                     'tag' => '26,',
	            //                     'bitrate' => '8192',
	            //                     'filetype' =>'ts',
	            //                 ),
	            'PC-1080P-8Mbps' => array(
	                'name' => 'PC-1080P-8Mbps',
	                'tag' => '31,',
	                'bitrate' => '8192',
	                'filetype' =>'mp4',
	            ),
	            'PC-1080P-4Mbps' => array(
	                'name' => 'PC-1080P-4Mbps',
	                'tag' => '27,28,29,30,',
	                'bitrate' => '4096',
	                'filetype' =>'mp4',
	            ),
	        ),
	    )
	);
	
	
	
	public function __construct($dc,$arr_task,$arr_sp_config, $arr_params=null,$op_sp)
	{
	    $this->arr_params = $arr_params;
	    $this->sp_config = $arr_sp_config;
	    $this->arr_task = $arr_task;
	    $this->op_sp = $op_sp;
	    $this->dc = $dc;
	    global $g_encode_file_ex_type;
	    $this->out_ex_type = (isset($g_encode_file_ex_type) && strlen($g_encode_file_ex_type)>0) ? $g_encode_file_ex_type : 'ts';
	    unset($g_encode_file_ex_type);
// 	    $this->_init();
	}
	
	/**
	 * 初始化转码配置
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 */
	public function _init()
	{
	    if(isset($this->arr_params['media_info']['nns_cp_id']))
	    {
	        $result_transcode_model = nl_transcode_model::query_by_cp($this->dc, $this->arr_params['media_info']['nns_cp_id']);
	        if($result_transcode_model['ret'] !=0 || !isset($result_transcode_model['data_info']) || empty($result_transcode_model['data_info']) || !is_array($result_transcode_model['data_info']))
	        {
	            return $result_transcode_model;
	        }
	        foreach ($result_transcode_model['data_info'] as $transcode_model_val)
	        {
	            if(!isset($this->manufacturer_api_url) || strlen($this->manufacturer_api_url) <1)
	            {
	                $this->manufacturer_api_url = $transcode_model_val['manufacturer_api_addr'];
	                $this->manufacturer = $transcode_model_val['manufacturer_mark'];
	                $this->in_out_type = null;
	            }
	            $this->in_out_type[$transcode_model_val['nns_input_file_type']][$transcode_model_val['nns_input_definition']][] = $transcode_model_val;
	        }
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	}
	
	
	public function ex_curl($url,$data)
	{
	    $clip_file_encode_way = (isset($this->sp_config['clip_file_encode_way']) && strlen($this->sp_config['clip_file_encode_way']) >0) ? strtolower($this->sp_config['clip_file_encode_way']) : '';
	    switch ($clip_file_encode_way)
	    {
	        case 'post':
	            $str_request_way = 'post';
	            break;
	        case 'get':
	            $str_request_way = 'get';
	            break;
	        default:
	            $str_request_way = 'post';
	    }
	    $obj_curl = new np_http_curl_class();
	    
	    $curl_data = $obj_curl->$str_request_way($url, $data,array ( "Content-Type: text/plain; charset=utf-8",));

	    $curl_info = $obj_curl->curl_getinfo();
	    $http_code = $curl_info['http_code'];
	    if($curl_info['http_code'] != '200')
	    {
	        return _comm_make_return_data(1,'[访问切片转码失败,CURL接口地址]：'.$url.'[HTTP状态码]：'.$curl_info['http_code'].'[HTTP错误]：'.$obj_curl->curl_error(),'参数：'.var_export($data,true));
	    }
	    else
	    {
	        return _comm_make_return_data(0,'[访问切片转码成功,CURL接口地址]：'.$url.'参数'.var_export($data,true),$curl_data);
	    }
	}
}