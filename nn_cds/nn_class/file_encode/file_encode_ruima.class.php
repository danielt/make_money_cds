<?php
class nl_file_encode_ruima extends nn_public_return
{
    /**
     * 转码负载均衡
     * @var unknown
     */
    public $flag_loadbalance_encode = FALSE;
    /**
     * 队列数据
     * @var unknown
     */
	public $data_queue = null;
	/**
	 * 模板数据  带请求地址
	 * @var unknown
	 */
	public $data_model = null;
	/**
	 * 其他请求参数
	 * @var unknown
	 */
	public $arr_params = null;
	/**
	 * 请求地址  带 点播、直播
	 * @var unknown
	 */
	public $request_url = null;
    /**
     * 转码后的文件地址（片源文件、抽帧图片）
     * @var unknown
     */
	public $encode_file_url = null;
	
	public $curl_way = null;
	
	
	
	//输入输出配置
	private $arr_inout_put =array (
			'output' => array (
					'user' => array (
							'fullName' => null,
							'name' => 'user',
							'desc' => '',
							'value' => 'trans_file'
					),
					'pwd' => array (
							'fullName' => null,
							'name' => 'pwd',
							'desc' => '',
							'value' => 'starcor1234'
					),
					'path' => array (
							'fullName' => null,
							'name' => 'path',
							'desc' => '转码输出路径',
							'value' => 'ftp://trans_file:starcor1234@192.168.90.79:21/test/mgtv_3a32469c179474f44a8be6c8fdc808f49'
					),
					'nameFormat' => array (
							'fullName' => null,
							'name' => 'nameFormat',
							'desc' => '转码输出命名规则',
							'value' => '$input'
					)
			),
			'input' => array (
					'user' => array (
							'fullName' => null,
							'name' => 'user',
							'desc' => '',
							'value' => 'source_file'
					),
					'pwd' => array (
							'fullName' => null,
							'name' => 'pwd',						
					        'desc' => '',
							'value' => 'starcor1234'
					),
					'path' => array (
							'fullName' => null,
							'name' => 'path',
							'desc' => '转码输入文件路径',
							'value' => 'ftp://source_file:starcor1234@192.168.90.79:21/test/mgtv_3a32469c179474f44a8be6c8fdc808f49.ts'
					),
					'traversal' => array (
							'fullName' => null,
							'name' => 'traversal',
							'desc' => '是否遍历子目录',
							'value' => '0'
					),
					'aterTsc' => array (
							'success' => array (
									'action' => array (
											'fullName' => null,
											'name' => 'action',
											'desc' => '转码成功后，输入文件的处理方法',
											'value' => 'keep'
									),
									'mvToPath' => array (
											'fullName' => null,
											'name' => 'mvToPath',
											'desc' => '转码成功后，移动源文件的目标路径',
											'value' => ''
									)
							),
							'failed' => array (
									'action' => array (
											'fullName' => null,
											'name' => 'action',
											'desc' => '转码失败后，输入文件的处理方法',
											'value' => 'keep'
									),
									'mvToPath' => array (
											'fullName' => null,
											'name' => 'mvToPath',
											'desc' => '转码失败后，移动源文件的目标路径',
											'value' => ''
									),
							),
					),
			),
	);
	//视屏 配置
	private $arr_video_model = array (
			'general' => array (
					'enable' => array (
							'fullName' => null, 
							'name' => 'enable', 
							'desc' => '视频使能', 
							'value' => '1'
					), 
					'codec' => array (
							'fullName' => null, 
							'name' => 'codec', 
							'desc' => '视频编码格式', 
							'value' => 'h264'
					), 
					'frameRate' => array (
							'fullName' => null, 
							'name' => 'frameRate', 
							'desc' => '视频帧率', 
							'value' => '25'
					), 
					'scale' => array (
							'fullName' => null, 
							'name' => 'scale', 
							'desc' => '视频缩放比例', 
							'value' => '0'
					), 
					'width' => array (
							'fullName' => null, 
							'name' => 'width', 
							'desc' => '视频宽度', 
							'value' => '1920'
					), 
					'height' => array (
							'fullName' => null, 
							'name' => 'height', 
							'desc' => '视频高度', 
							'value' => '1080'
					), 
					'aspectRatio' => array (
							'fullName' => null, 
							'name' => 'aspectRatio', 
							'desc' => '视频宽高比', 
							'value' => 'copy'
					), 
					'fillMode' => array (
							'fullName' => null, 
							'name' => 'fillMode', 
							'desc' => '视频宽高比', 
							'value' => 'stretch'
					), 
					'bc' => array (
							'rate' => array (
									'fullName' => null, 
									'name' => 'rate', 
									'desc' => 'CBR时的视频总码率', 
									'value' => '2000'
							), 
							'maxRate' => array (
									'fullName' => null, 
									'name' => 'maxRate', 
									'desc' => 'VBR时的最大视频码率', 
									'value' => ''
							), 
							'minRate' => array (
									'fullName' => null, 
									'name' => 'minRate', 
									'desc' => 'VBR时的最小视频码率', 
									'value' => ''
							), 
							'method' => array (
									'fullName' => null, 
									'name' => 'method', 
									'desc' => '码率控制方法', 
									'value' => 'cbr'
							)
					)
			), 
			'h264' => array (
					'basic' => array (
							'profile' => array (
									'fullName' => null, 
									'name' => 'profile', 
									'desc' => '编码格式的profile', 
									'value' => 'high'
							), 
							'pixelFormat' => array (
									'fullName' => null, 
									'name' => 'pixelFormat', 
									'desc' => '像素格式', 
									'value' => 'yuv420'
							), 
							'level' => array (
									'fullName' => null, 
									'name' => 'level', 
									'desc' => '编码格式的level', 
									'value' => 'auto'
							), 
							'entropyEncoder' => array (
									'fullName' => null, 
									'name' => 'entropyEncoder', 
									'desc' => '使用的熵编码方法，vlc或者ac', 
									'value' => 'ac'
							), 
							'partitions' => array (
									'fullName' => null, 
									'name' => 'partitions', 
									'desc' => '图像编码分区方法', 
									'value' => 'ac'
							), 
							'scanMode' => array (
									'type' => array (
											'fullName' => null, 
											'name' => 'type', 
											'desc' => '输出视频扫描模式', 
											'value' => 'i'
									), 
									'fieldOrder' => array (
											'fullName' => null, 
											'name' => 'fieldOrder', 
											'desc' => '隔行扫描场序', 
											'value' => 'tt'
									)
							)
					), 
					'me' => array (
							'method' => array (
									'fullName' => null, 
									'name' => 'method', 
									'desc' => '运动估计方法', 
									'value' => 'hex'
							), 
							'range' => array (
									'fullName' => null, 
									'name' => 'range', 
									'desc' => '运动搜索范围', 
									'value' => '16'
							), 
							'subMe' => array (
									'fullName' => null, 
									'name' => 'subMe', 
									'desc' => '亚像素运动估计', 
									'value' => '7'
							), 
							'directPred' => array (
									'fullName' => null, 
									'name' => 'directPred', 
									'desc' => 'B帧内采用的运动侦测的方式', 
									'value' => 'spatial'
							)
					), 
					'bc' => array (
							'crf' => array (
									'fullName' => null, 
									'name' => 'crf', 
									'desc' => '恒定质量参数', 
									'value' => '23'
							), 
							'crfMax' => array (
									'fullName' => null, 
									'name' => 'crfMax', 
									'desc' => '最大恒定质量参数', 
									'value' => '24'
							), 
							'qp' => array (
									'fullName' => null, 
									'name' => 'qp', 
									'desc' => '量化参数', 
									'value' => '23'
							)
					), 
					'frame' => array (
							'gop' => array (
									'format' => array (
											'fullName' => null, 
											'name' => 'format', 
											'desc' => 'gop格式', 
											'value' => 'close'
									), 
									'min' => array (
											'fullName' => null, 
											'name' => 'min', 
											'desc' => '最小I帧间隔', 
											'value' => '25'
									), 
									'max' => array (
											'fullName' => null, 
											'name' => 'max', 
											'desc' => '最大I帧间隔', 
											'value' => '25'
									), 
									'maxBFrameCount' => array (
											'fullName' => null, 
											'name' => 'maxBFrameCount', 
											'desc' => '两个非B帧之间允许的最大B帧数', 
											'value' => '3'
									)
							), 
							'refFrame' => array (
									'count' => array (
											'fullName' => null, 
											'name' => 'count', 
											'desc' => '参考帧数量', 
											'value' => '3'
									), 
									'bFrameAsRef' => array (
											'fullName' => null, 
											'name' => 'bFrameAsRef', 
											'desc' => '允许使用B帧作为参考帧的设置', 
											'value' => 'none'
									), 
									'weightB' => array (
											'fullName' => null, 
											'name' => 'weightB', 
											'desc' => '设置是否允许B帧加权预测', 
											'value' => '1'
									), 
									'weightP' => array (
											'fullName' => null, 
											'name' => 'weightP', 
											'desc' => 'P帧加权预测方式', 
											'value' => 'smart'
									), 
									'sceneCut' => array (
											'fullName' => null, 
											'name' => 'sceneCut', 
											'desc' => '开启场景切换检测', 
											'value' => '1'
									)
							)
					)
			), 
			'mp4' => array (
					'basic' => array (
							'profile' => array (
									'fullName' => null, 
									'name' => 'profile', 
									'desc' => '编码格式的profile', 
									'value' => 'simple'
							), 
							'scanMode' => array (
									'type' => array (
											'fullName' => null, 
											'name' => 'type', 
											'desc' => '输出视频扫描模式', 
											'value' => 'p'
									), 
									'fieldOrder' => array (
											'fullName' => null, 
											'name' => 'fieldOrder', 
											'desc' => '隔行扫描场序', 
											'value' => 'auto'
									)
							)
					), 
					'frame' => array (
							'gop' => array (
									'max' => array (
											'fullName' => null, 
											'name' => 'max', 
											'desc' => '最大I帧间隔', 
											'value' => '250'
									), 
									'maxBFrameCount' => array (
											'fullName' => null, 
											'name' => 'maxBFrameCount', 
											'desc' => '两个非B帧之间允许的最大B帧数', 
											'value' => '3'
									)
							), 
							'refFrame' => array (
									'count' => array (
											'fullName' => null, 
											'name' => 'count', 
											'desc' => '参考帧数量', 
											'value' => '3'
									)
							)
					)
			), 
			'mpeg2' => array (
					'basic' => array (
							'profile' => array (
									'fullName' => null, 
									'name' => 'profile', 
									'desc' => '编码格式的profile', 
									'value' => 'main'
							), 
							'pixelFormat' => array (
									'fullName' => null, 
									'name' => 'pixelFormat', 
									'desc' => '像素格式', 
									'value' => 'yuv420'
							), 
							'scanMode' => array (
									'type' => array (
											'fullName' => null, 
											'name' => 'type', 
											'desc' => '输出视频扫描模式', 
											'value' => 'p'
									), 
									'fieldOrder' => array (
											'fullName' => null, 
											'name' => 'fieldOrder', 
											'desc' => '隔行扫描场序', 
											'value' => 'auto'
									)
							)
					), 
					'frame' => array (
							'gop' => array (
									'max' => array (
											'fullName' => null, 
											'name' => 'max', 
											'desc' => '最大I帧间隔', 
											'value' => '250'
									), 
									'maxBFrameCount' => array (
											'fullName' => null, 
											'name' => 'maxBFrameCount', 
											'desc' => '两个非B帧之间允许的最大B帧数', 
											'value' => '3'
									)
							), 
							'refFrame' => array (
									'count' => array (
											'fullName' => null, 
											'name' => 'count', 
											'desc' => '参考帧数量', 
											'value' => '3'
									)
							)
					)
			), 
			'wmv' => array (
					'basic' => array (
							'scanMode' => array (
									'type' => array (
											'fullName' => null, 
											'name' => 'type', 
											'desc' => '输出视频扫描模式', 
											'value' => 'p'
									), 
									'fieldOrder' => array (
											'fullName' => null, 
											'name' => 'fieldOrder', 
											'desc' => '隔行扫描场序', 
											'value' => 'auto'
									)
							)
					), 
					'frame' => array (
							'gop' => array (
									'max' => array (
											'fullName' => null, 
											'name' => 'max', 
											'desc' => '最大I帧间隔', 
											'value' => '250'
									)
							), 
							'refFrame' => array (
									'count' => array (
											'fullName' => null, 
											'name' => 'count', 
											'desc' => '参考帧数量', 
											'value' => '3'
									)
							)
					)
			), 
			'avs' => array (), 
			'h265' => array ()
	);
	//音频 配置
	private $arr_audio_model = array (
			'enable' => array (
					'fullName' => null, 
					'name' => 'enable', 
					'desc' => '音频编码使能', 
					'value' => '1'
			), 
			'codec' => array (
					'fullName' => null, 
					'name' => 'codec', 
					'desc' => '音频编码格式', 
					'value' => 'mp2'
			), 
			'kbps' => array (
					'fullName' => null, 
					'name' => 'kbps', 
					'desc' => '音频码率', 
					'value' => '192'
			), 
			'hz' => array (
					'fullName' => null, 
					'name' => 'hz', 
					'desc' => '音频采样率', 
					'value' => '48000'
			), 
			'channels' => array (
					'fullName' => null, 
					'name' => 'channels', 
					'desc' => '声道', 
					'value' => 'stereo'
			), 
			'tracks' => array (
					'fullName' => null, 
					'name' => 'tracks', 
					'desc' => '音轨', 
					'value' => '0'
			), 
			'multiOutput' => array (array(
					'volume' => array (
							'fullName' => null, 
							'name' => 'volume', 
							'desc' => '音量调整', 
							'value' => '0'
					), 
					'codec' => array (
							'fullName' => null, 
							'name' => 'codec', 
							'desc' => '音频编码格式', 
							'value' => 'mp2'
					), 
					'bitrate' => array (
							'fullName' => null, 
							'name' => 'bitrate', 
							'desc' => '音频码率', 
							'value' => '192'
					), 
					'ar' => array (
							'fullName' => null, 
							'name' => 'ar', 
							'desc' => '音频采样率', 
							'value' => '48000'
					), 
					'channels' => array (
							'fullName' => null, 
							'name' => 'channels', 
							'desc' => '声道', 
							'value' => 'stereo'
					)
					)
			)
	);
	//转码复用 配置
	private $arr_mux_model = array (
			'basic' => array (
					'serviceProvider' => array (
							'fullName' => null, 
							'name' => 'serviceProvider', 
							'desc' => '服务提供商', 
							'value' => ''
					), 
					'serviceName' => array (
							'fullName' => null, 
							'name' => 'serviceName', 
							'desc' => '服务名称', 
							'value' => ''
					), 
					'rate' => array (
							'fullName' => null, 
							'name' => 'rate', 
							'desc' => '复用码率', 
							'value' => ''
					), 
					'extension' => array (
							'fullName' => null, 
							'name' => 'extension', 
							'desc' => '封装格式', 
							'value' => 'ts'
					)
			), 
			'fmt' => array (
					'ts' => array (
							'pmtPid' => array (
									'fullName' => null, 
									'name' => 'pmtPid', 
									'desc' => 'PMT PID', 
									'value' => '0x1000'
							), 
							'programId' => array (
									'fullName' => null, 
									'name' => 'programId', 
									'desc' => '节目ID', 
									'value' => '0x0001'
							), 
							'pcrPid' => array (
									'fullName' => null, 
									'name' => 'pcrPid', 
									'desc' => 'PCR PID', 
									'value' => '0x0064'
							), 
							'videoPid' => array (
									'fullName' => null, 
									'name' => 'videoPid', 
									'desc' => '视频PID', 
									'value' => '0x0064'
							), 
							'audioPid' => array (
									'fullName' => null, 
									'name' => 'audioPid', 
									'desc' => '音频PID', 
									'value' => '0x0065'
							)
					), 
					'tsParam' => array (
							'pmtPid' => array (
									'fullName' => null, 
									'name' => 'pmtPid', 
									'desc' => 'PMT PID', 
									'value' => '0x1000'
							), 
							'videoPid' => array (
									'fullName' => null, 
									'name' => 'videoPid', 
									'desc' => '视频PID', 
									'value' => '0x0064'
							), 
							'audioPid' => array (
									'fullName' => null, 
									'name' => 'audioPid', 
									'desc' => '音频PID', 
									'value' => '0x0065'
							)
					), 
					'hls' => array (
							'pmtPid' => array (
									'fullName' => null, 
									'name' => 'pmtPid', 
									'desc' => 'PMT PID', 
									'value' => '0x1000'
							), 
							'videoPid' => array (
									'fullName' => null, 
									'name' => 'videoPid', 
									'desc' => '视频PID', 
									'value' => '0x0064'
							), 
							'audioPid' => array (
									'fullName' => null, 
									'name' => 'audioPid', 
									'desc' => '音频PID', 
									'value' => '0x0065'
							), 
							'segmentLength' => array (
									'fullName' => null, 
									'name' => 'segmentLength', 
									'desc' => 'HLS分段长度', 
									'value' => ''
							)
					), 
					'segmentLength' => array (
							'fullName' => null, 
							'name' => 'segmentLength', 
							'desc' => 'HLS分段长度', 
							'value' => ''
					)
			)
	);
	//转码扩展 配置
	private $arr_ext_model = array (
			'subtitle' => array (
					'user' => array (
							'fullName' => null, 
							'name' => 'user', 
							'desc' => '', 
							'value' => ''
					), 
					'pwd' => array (
							'fullName' => null, 
							'name' => 'pwd', 
							'desc' => '', 
							'value' => ''
					), 
					'enable' => array (
							'fullName' => null, 
							'name' => 'enable', 
							'desc' => '是否开启字幕叠加功能', 
							'value' => '0'
					), 
					'mode' => array (
							'fullName' => null, 
							'name' => 'mode', 
							'desc' => '字幕加载方式，自动加载会根据源文件名称智能搜索字幕文件，手动加载需要指定字幕文件', 
							'value' => 'manual'
					), 
					'path' => array (
							'fullName' => null, 
							'name' => 'path', 
							'desc' => '字幕机文件存放目录', 
							'value' => ''
					), 
					'intSub' => array (
							'fullName' => null, 
							'name' => 'intSub', 
							'desc' => '使用内置字幕轨道', 
							'value' => ''
					)
			), 
			'logo' => array (
					'enable' => array (
							'fullName' => null, 
							'name' => 'enable', 
							'desc' => '是否开启图片叠加功能', 
							'value' => '0'
					), 
					'user' => array (
							'fullName' => null, 
							'name' => 'user', 
							'desc' => '', 
							'value' => ''
					), 
					'pwd' => array (
							'fullName' => null, 
							'name' => 'pwd', 
							'desc' => '', 
							'value' => ''
					), 
					'x' => array (
							'fullName' => null, 
							'name' => 'x', 
							'desc' => '图片叠加位置左上角的x轴坐标', 
							'value' => '0'
					), 
					'y' => array (
							'fullName' => null, 
							'name' => 'y', 
							'desc' => '图片叠加位置左上角的y轴坐标', 
							'value' => '0'
					), 
					'width' => array (
							'fullName' => null, 
							'name' => 'width', 
							'desc' => '图片宽度', 
							'value' => '0'
					), 
					'height' => array (
							'fullName' => null, 
							'name' => 'height', 
							'desc' => '图片高度', 
							'value' => '0'
					), 
					'file' => array (
							'fullName' => null, 
							'name' => 'file', 
							'desc' => '图片文件url', 
							'value' => ''
					), 
					'mode' => array (
							'fullName' => null, 
							'name' => 'mode', 
							'desc' => '图片叠加到输入或输出上', 
							'value' => 'out'
					), 
					'multiLogo' => null
			), 
			'delogo' => array (
					'enable' => array (
							'fullName' => null, 
							'name' => 'enable', 
							'desc' => '是否开启去台标功能', 
							'value' => '0'
					), 
					'x' => array (
							'fullName' => null, 
							'name' => 'x', 
							'desc' => '台标区域左上角x轴坐标', 
							'value' => '0'
					), 
					'y' => array (
							'fullName' => null, 
							'name' => 'y', 
							'desc' => '台标区域左上角y轴坐标', 
							'value' => '0'
					), 
					'width' => array (
							'fullName' => null, 
							'name' => 'width', 
							'desc' => '台标区域宽度', 
							'value' => '10'
					), 
					'height' => array (
							'fullName' => null, 
							'name' => 'height', 
							'desc' => '台标区域高度', 
							'value' => '10'
					), 
					'band' => array (
							'fullName' => null, 
							'name' => 'band', 
							'desc' => '台标区域的模糊程度', 
							'value' => '2'
					), 
					'multiDelogo' => null
			), 
			'brightness' => array (
					'adjustment' => array (
							'fullName' => null, 
							'name' => 'adjustment', 
							'desc' => '亮度调节值', 
							'value' => ''
					)
			), 
			'contrast' => array (
					'adjustment' => array (
							'fullName' => null, 
							'name' => 'adjustment', 
							'desc' => '对比度调节值', 
							'value' => ''
					)
			), 
			'saturation' => array (
					'adjustment' => array (
							'fullName' => null, 
							'name' => 'adjustment', 
							'desc' => '饱和度调节值', 
							'value' => ''
					)
			), 
			'audioDelay' => array (
					'adjustment' => array (
							'fullName' => null, 
							'name' => 'adjustment', 
							'desc' => '音频延迟', 
							'value' => ''
					)
			)
	);
	//输入输出 配置
	private $arr_inout_config = null;
	//临时数组模板
	private $temp_arr_model = null;
	//转码分发策略
	private $task_distribute = null;
	
	private $sp_config_encode_url = null;
	
	/**
	 * 组装数据
	 */
	private function init_make_data()
	{
	    $this->temp_arr_model = array(
	        'report'=>'0',
	        'replyKey'=>$this->arr_task['nns_id'],
	        'cmdBody'=>array(
	            'cmd'=>$this->arr_params['cmd'],
	            'task'=>array('extData'=>null,)
	        ),
	    );
	}
	
	
    
    public function __construct($obj_dc,$data_queue=null,$data_model=null,$arr_sp_config,$sp_id,$arr_params=null)
    {
        if(isset($data_model['nns_video_config']))
        {
            $data_model['nns_video_config'] = $this->is_json($data_model['nns_video_config']) ? json_decode($data_model['nns_video_config'],true) : null;
        }
        if(isset($data_model['nns_audio_config']))
        {
            $data_model['nns_audio_config'] = $this->is_json($data_model['nns_audio_config']) ? json_decode($data_model['nns_audio_config'],true) : null;
        }
        if(isset($data_model['nns_other_config']))
        {
            $data_model['nns_other_config'] = $this->is_json($data_model['nns_other_config']) ? json_decode($data_model['nns_other_config'],true) : null;
        }
        $this->obj_dc = $obj_dc;
        $this->data_queue = $data_queue;
        $this->data_model = $data_model;
        $this->arr_params = $arr_params;
        $this->arr_sp_config = $arr_sp_config;
        $this->sp_id = $sp_id;
    }
    
    /**
     * 初始化转码配置
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    public function init($str_type='0')
    {
        $this->encode_file_url = (isset($this->data_queue['nns_file_url']) && strlen($this->data_queue['nns_file_url'])>0 && $this->is_json($this->data_queue['nns_file_url'])) ? json_decode($this->data_queue['nns_file_url'],true) : null;
        if(isset($this->arr_sp_config[$this->sp_id]['nns_config']['transcode_load_balance_encode']) && $this->arr_sp_config[$this->sp_id]['nns_config']['transcode_load_balance_encode'] == '1' && $str_type =='0' && !empty($this->arr_params) && is_array($this->arr_params))
        {
            $this->request_url['video'] = $this->arr_params['nns_api_url'];
            $this->request_url['loadbalance_id'] = $this->arr_params['nns_id'];
        }
        else
        {
            $this->request_url = (isset($this->data_model['manufacturer_api_addr']) && strlen($this->data_model['manufacturer_api_addr'])>0 && $this->is_json($this->data_model['manufacturer_api_addr'])) ? json_decode($this->data_model['manufacturer_api_addr'],true) : null;
            $this->request_url = (is_array($this->request_url) && !empty($this->request_url)) ? $this->request_url : null;
            $this->request_url['loadbalance_id'] = '59e06461336ef9164ad3ccc852ad7f2f';
        }
        $this->encode_file_url = (is_array($this->encode_file_url) && !empty($this->encode_file_url)) ? $this->encode_file_url : null;
        $clip_file_encode_way = (isset($this->arr_sp_config[$this->sp_id]['nns_config']['clip_file_encode_way']) && strlen($this->arr_sp_config[$this->sp_id]['nns_config']['clip_file_encode_way']) >0) ? strtolower($this->arr_sp_config[$this->sp_id]['nns_config']['clip_file_encode_way']) : '';
        $clip_file_encode_way = strtolower($clip_file_encode_way);
        $this->curl_way = in_array($clip_file_encode_way, array('post','get')) ? $clip_file_encode_way : 'post';
    }
       
    
    public function set_task()
    {
        switch ($this->data_queue['nns_type'])
        {
            case '1':
                $this->init('1');
                return $this->set_task_live();
            case '2':
                $this->init('0');
                return $this->set_task_live_to_media();
            default:
                $this->init('0');
                return $this->set_task_media();                
        }
    }
    
    /**
     * 直播转码
     */
    public function set_task_live()
    {
        if(strlen($this->request_url['live']) <1)
        {
            return $this->__return_data(1,'转码请求地址request_url配置为空:'.$this->request_url);
        }
        $result_media = nl_live_media::query_by_id($this->obj_dc, $this->data_queue['nns_in_media_id']);
        if($result_media['ret'] !=0)
        {
            return $result_media;
        }
        $result_media_out = nl_live_media::query_by_id($this->obj_dc, $this->data_queue['nns_out_media_id']);
        if($result_media_out['ret'] !=0)
        {
            return $result_media_out;
        }
        if(!isset($result_media['data_info']['nns_url']) || strlen($result_media['data_info']['nns_url'])<1)
        {
            return $this->__return_data(1,'查询的原始片源没有片源路径');
        }
        $out_bitrate = (int)$this->data_model['nns_output_bitrate'] > 0 ? (int)$this->data_model['nns_output_bitrate']*1024 : 4000*1024;
        $inout_xml ='<?xml version="1.0" encoding="UTF-8"?>';
        $inout_xml.='<message module="URS" version="1.0">';
        $inout_xml.=    '<header action="REQUEST" command="ENCODER_LIVE_START" />';
        $inout_xml.=    '<body>';
        $inout_xml.=        '<task id="'.$this->data_queue['nns_integer_id'].'" mode="ONLINE" source-type="IP" program-no="'.$result_media_out['nns_integer_id'].'" ca="" ip-source-url="'.$result_media['data_info']['nns_url'].'">';
        $inout_xml.=        '<encode-params>';
        $inout_xml.=            '<system-params system-type="ts" ts-ip="192.168.1.26" ts-port="6001"/>';
        $inout_xml.=            '<video-params video-codec="h264" rcmode="Onepass" video-width="1920" video-height="1080" video-frame-rate="25" video-bitrate="'.$out_bitrate.'" video-b-frame-num="5" video-key-frame-interval="0.2" />';
        $inout_xml.=            '<audio-params audio-codec="aaclc" audio-channels="2" audio-sample-rate="32000" audio-channel-mode="allchannels" audio-bitrate-bps="16000"/>';
        $inout_xml.=            '<video-codec-params h264-profile="main" h264-level="30"/>';
        $inout_xml.=        '</encode-params>';
        if(isset($this->encode_file_url['drawing_frame']) && is_array($this->encode_file_url['drawing_frame']) && !empty($this->encode_file_url['drawing_frame']))
        {
            if(!isset($this->arr_sp_config[$this->sp_id]['nns_config']['drawing_frame_addr_output']) || strlen($this->arr_sp_config[$this->sp_id]['nns_config']['drawing_frame_addr_output']) <1)
            {
                return $this->__return_data(1,'抽帧图片img_ftp输入FTP配置为空:'.$this->arr_sp_config[$this->sp_id]['nns_config']['img_ftp']);
            }
            $ftp_drawing_frame_url = trim(rtrim(rtrim($this->arr_sp_config[$this->sp_id]['nns_config']['drawing_frame_addr_output'],'/'),'\\'));
            $this->data_model['nns_drawing_rule'] = (int)$this->data_model['nns_drawing_rule'] > 0 ? (int)$this->data_model['nns_drawing_rule'] : 20;
            $i = 0;
            foreach ($this->encode_file_url['drawing_frame'] as $drawing_frame_val)
            {
                $i++;
                if($i > 1)
                {
                    break;
                }
                $drawing_frame_val = trim(ltrim(ltrim($drawing_frame_val,'/'),'\\'));
                if(strlen($drawing_frame_val) <1)
                {
                    continue;
                }
                $drawing_frame_url = $ftp_drawing_frame_url.'/'.$drawing_frame_val;
                $drawing_frame_path = pathinfo($drawing_frame_url);
                $size = pathinfo($drawing_frame_path['dirname']);
                $inout_xml.= '<snapshot path="'.$drawing_frame_path['dirname'].'" size="'.strtolower($size['basename']).'" interval="'.$this->data_model['nns_drawing_rule'].'" />';
            }
        }
        $inout_xml.=        '</task>';
        $inout_xml.=    '</body>';
        $inout_xml.='</message>';
        $content_length = strlen($inout_xml);
        $arr_header = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: \"run\"",
            "Content-Length: {$content_length}",
        );
        $result = $this->_execute_curl($this->request_url['live'],$this->curl_way, $inout_xml,$arr_header);
        if($result['ret'] !=0)
        {
            $log_file = $this->make_file_log($inout_xml, '');
            $this->make_file_encode_log($this->data_queue['nns_id'],"转码请求失败，描述[{$result['reason']}]",0,1,$log_file['request'],$log_file['notify']);
            nl_file_encode::edit($this->obj_dc, array('nns_state'=>2), $this->data_queue['nns_id']);
            return $this->__return_data(1,$result['reason']);
        }
        $message_content = (isset($result['data_info']) && strlen($result['data_info'])>0) ? $result['data_info'] : '';
        $log_file = $this->make_file_log($inout_xml, $message_content);
        if(!$this->is_xml($message_content))
        {
            $this->make_file_encode_log($this->data_queue['nns_id'],"转码返回的值非xml",0,1,$log_file['request'],$log_file['notify']);
            nl_file_encode::edit($this->obj_dc, array('nns_state'=>2), $this->data_queue['nns_id']);
            return $this->__return_data(1,"转码返回的值非xml");
        }
        $result_ex = json_decode(json_encode(@simplexml_load_string($message_content)),true);
        $description = isset($result_ex['body']['result']['@attributes']['description']) ? $result_ex['body']['result']['@attributes']['description'] : '';
        if(isset($result_ex['body']['result']['@attributes']['code']) && $result_ex['body']['result']['@attributes']['code'] =='1')
        {
            $result = $this->make_file_encode_log($this->data_queue['nns_id'],"请求转码成功",0,0,$log_file['request'],$log_file['notify']);
            nl_file_encode::edit($this->obj_dc, array('nns_state'=>1), $this->data_queue['nns_id']);
            return $this->__return_data(0,$description);
        }
        $this->make_file_encode_log($this->data_queue['nns_id'],"请求转码失败",0,1,$log_file['request'],$log_file['notify']);
        nl_file_encode::edit($this->obj_dc, array('nns_state'=>1), $this->data_queue['nns_id']);
        return $this->__return_data(1,$description);
    }
    
    /**
     * 直播转点播
     */
    public function set_task_live_to_media()
    {
        if(strlen($this->request_url['video']) <1)
        {
            return $this->__return_data(1,'转码请求地址request_url配置为空:'.$this->request_url);
        }
        if(!isset($this->arr_sp_config[$this->sp_id]['nns_config']['clip_file_addr_output']) || strlen($this->arr_sp_config[$this->sp_id]['nns_config']['clip_file_addr_output']) <1)
        {
            return $this->__return_data(1,'转码文件输入FTP配置为空:'.$this->arr_sp_config[$this->sp_id]['nns_config']['clip_file_addr_output']);
        }
        if(!isset($this->encode_file_url['media']) || strlen($this->encode_file_url['media']) <1)
        {
            return $this->__return_data(1,'转码文件输出为空:'.$this->encode_file_url['media']);
        }
        $result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $this->data_queue['nns_in_media_id']);
        if($result_media['ret'] !=0)
        {
            return $result_media;
        }
        if(!isset($result_media['data_info']['nns_ext_info']) || strlen($result_media['data_info']['nns_ext_info'])<1 || !$this->is_json($result_media['data_info']['nns_ext_info']))
        {
            return $this->__return_data(1,'查询的原始片源没有片源路径');
        }
        $nns_ext_info = json_decode($result_media['data_info']['nns_ext_info'],true);
        if(!isset($nns_ext_info['path']) || strlen($nns_ext_info['path'])<1)
        {
            return $this->__return_data(1,'查询的原始片源没有片源路径path');
        }
        $ftp_url = trim(rtrim(rtrim($this->arr_sp_config[$this->sp_id]['nns_config']['clip_file_addr_output'],'/'),'\\'));
        $in_file_url = trim(ltrim(ltrim($nns_ext_info['path'],'/'),'\\'));
        $out_file_url = trim(ltrim(ltrim($this->encode_file_url['media'],'/'),'\\'));
        
        
        #TODO 直播转点播  请求 MSP
        $ftp_in = $ftp_url.'/'.$in_file_url;
        $in_arr_file_name = pathinfo($ftp_in);
         
        $ftp_out = $ftp_url.'/'.$out_file_url;
        $out_arr_file_name = pathinfo($ftp_out);
         
        $out_bitrate = (int)$this->data_model['nns_output_bitrate'] > 0 ? (int)$this->data_model['nns_output_bitrate']*1024 : 4000*1024;
         
        $inout_xml ='<?xml version="1.0" encoding="UTF-8"?>';
        $inout_xml.='<message module="xCoder" version="1.0.1.1730">';
        $inout_xml.=   '<header action="REQUEST" command="TASK_ASSIGN" />';
        $inout_xml.=   '<body>';
        $inout_xml.=       '<task id="'.$this->data_queue['nns_integer_id'].'">';
        $inout_xml.=           '<input>';
        $inout_xml.=               '<i_path>'.$ftp_in.'</i_path>';
        $inout_xml.=               '<i_file_fmt>'.$in_arr_file_name['extension'].'</i_file_fmt>';
        $inout_xml.=           '</input>';
        $inout_xml.=           '<output>';
        $inout_xml.=               '<o_path>'.$ftp_out.'</o_path>';
        $inout_xml.=               '<system_params>';
        $inout_xml.=                   '<file_format>'.$out_arr_file_name['extension'].'</file_format>';
        $inout_xml.=                   '<drm_encryption_flag>disable</drm_encryption_flag>';
        $inout_xml.=                   '<hls_segmenter>disable</hls_segmenter>';
        
        if(isset($this->encode_file_url['drawing_frame']) && is_array($this->encode_file_url['drawing_frame']) && !empty($this->encode_file_url['drawing_frame']))
        {
            if(!isset($this->arr_sp_config[$this->sp_id]['nns_config']['drawing_frame_addr_output']) || strlen($this->arr_sp_config[$this->sp_id]['nns_config']['drawing_frame_addr_output']) <1)
            {
                return $this->__return_data(1,'抽帧图片img_ftp输入FTP配置为空:'.$this->arr_sp_config[$this->sp_id]['nns_config']['img_ftp']);
            }
            $ftp_drawing_frame_url = trim(rtrim(rtrim($this->arr_sp_config[$this->sp_id]['nns_config']['drawing_frame_addr_output'],'/'),'\\'));
            $this->data_model['nns_drawing_rule'] = (int)$this->data_model['nns_drawing_rule'] > 0 ? (int)$this->data_model['nns_drawing_rule'] : 20;
            $i = 0;
            foreach ($this->encode_file_url['drawing_frame'] as $drawing_frame_val)
            {
                $i++;
                if($i > 1)
                {
                    break;
                }
                $drawing_frame_val = trim(ltrim(ltrim($drawing_frame_val,'/'),'\\'));
                if(strlen($drawing_frame_val) <1)
                {
                    continue;
                }
                $drawing_frame_url = $ftp_drawing_frame_url.'/'.$drawing_frame_val;
                $drawing_frame_path = pathinfo($drawing_frame_url);
                $size = pathinfo($drawing_frame_path['dirname']);
                 
                $inout_xml.=                   '<snapshot>';
                $inout_xml.=                       '<snapshot_flag>enable</snapshot_flag>';
                $inout_xml.=                       '<snapshot_path>'.$drawing_frame_path['dirname'].'</snapshot_path>';
                $inout_xml.=                       '<snapshot_size>'.strtolower($size['basename']).'</snapshot_size>';
                $inout_xml.=                       '<snapshot_interval>'.$this->data_model['nns_drawing_rule'].'</snapshot_interval>';
                $inout_xml.=                   '</snapshot>';
            }
        }
// 	    else
// 	    {
//     	    $inout_xml.=                   '<snapshot>';
//     	    $inout_xml.=                       '<snapshot_flag>disable</snapshot_flag>';
//     	    $inout_xml.=                   '</snapshot>';
// 	    }
        $inout_xml.=               '</system_params>';
        $inout_xml.=               '<video_params>';
        $inout_xml.=                   '<video_aspect>16:9</video_aspect>';
        $inout_xml.=                   '<bframes>3</bframes>';
        $inout_xml.=                   '<iframe_interval>50</iframe_interval>';
        $inout_xml.=                   '<interlaced>disable</interlaced>';
        $inout_xml.=                   '<level>40</level>';
        $inout_xml.=                   '<max_v_bitrate>'.$out_bitrate.'</max_v_bitrate>';
        $inout_xml.=                   '<profile>high</profile>';
        $inout_xml.=                   '<rc_method>vbr</rc_method>';
        $inout_xml.=                   '<ref_count>3</ref_count>';
        $inout_xml.=                   '<v_type>h264</v_type>';
        $inout_xml.=                   '<scenecut_detect>enable</scenecut_detect>';
        $inout_xml.=                   '<v_bitrate>'.$out_bitrate.'</v_bitrate>';
        $inout_xml.=                   '<v_pass>1</v_pass>';
        $inout_xml.=                   '<video_open_gop>disable</video_open_gop>';
        $inout_xml.=                   '<video_quality>normal</video_quality>';
        $inout_xml.=                   '<output_size>1920x1080</output_size>';
        $inout_xml.=                   '<fps>auto</fps>';
        $inout_xml.=               '</video_params>';
        $inout_xml.=               '<audio_params>';
        $inout_xml.=                   '<a_bitrate>192000</a_bitrate>';
        $inout_xml.=                   '<channels>2</channels>';
        $inout_xml.=                   '<a_type>ac3</a_type>';
        $inout_xml.=                   '<sample_rate>48000</sample_rate>';
        $inout_xml.=               '</audio_params>';
        $inout_xml.=               '<subtitle_params></subtitle_params>';
        $inout_xml.=           '</output>';
        $inout_xml.=       '</task>';
        $inout_xml.=   '</body>';
        $inout_xml.='</message>';
        $content_length = strlen($inout_xml);
        $arr_header = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: \"run\"",
            "Content-Length: {$content_length}",
        );
        $result = $this->_execute_curl($this->request_url['video'],$this->curl_way, $inout_xml,$arr_header);
        if($result['ret'] !=0)
        {
            $log_file = $this->make_file_log($inout_xml, '');
            $this->make_file_encode_log($this->data_queue['nns_id'],"转码请求失败，描述[{$result['reason']}]",0,1,$log_file['request'],$log_file['notify']);
            nl_file_encode::edit($this->obj_dc, array('nns_state'=>2), $this->data_queue['nns_id']);
            return $this->__return_data(1,$result['reason']);
        }
        $message_content = (isset($result['data_info']) && strlen($result['data_info'])>0) ? $result['data_info'] : '';
        $log_file = $this->make_file_log($inout_xml, $message_content);
        if(!$this->is_xml($message_content))
        {
            $this->make_file_encode_log($this->data_queue['nns_id'],"转码返回的值非xml",0,1,$log_file['request'],$log_file['notify']);
            nl_file_encode::edit($this->obj_dc, array('nns_state'=>2), $this->data_queue['nns_id']);
            return $this->__return_data(1,"转码返回的值非xml");
        }
        $result_ex = json_decode(json_encode(@simplexml_load_string($message_content)),true);
        $description = isset($result_ex['body']['result']['@attributes']['description']) ? $result_ex['body']['result']['@attributes']['description'] : '';
        if(isset($result_ex['body']['result']['@attributes']['code']) && $result_ex['body']['result']['@attributes']['code'] =='1')
        {
            $result = $this->make_file_encode_log($this->data_queue['nns_id'],"请求转码成功",0,0,$log_file['request'],$log_file['notify']);
            nl_file_encode::edit($this->obj_dc, array('nns_state'=>1), $this->data_queue['nns_id']);
            return $this->__return_data(0,$description);
        }
        $this->make_file_encode_log($this->data_queue['nns_id'],"请求转码失败",0,1,$log_file['request'],$log_file['notify']);
        nl_file_encode::edit($this->obj_dc, array('nns_state'=>1), $this->data_queue['nns_id']);
        return $this->__return_data(1,$description);
    }
    
	/**
	 * 上报点播转码数据
	 */
	public function set_task_media()
	{
	    if(strlen($this->request_url['video']) <1)
	    {
	        return $this->__return_data(1,'转码请求地址request_url配置为空:'.$this->request_url);
	    }
	    if(!isset($this->arr_sp_config[$this->sp_id]['nns_config']['clip_file_addr_output']) || strlen($this->arr_sp_config[$this->sp_id]['nns_config']['clip_file_addr_output']) <1)
	    {
	        return $this->__return_data(1,'转码文件输入FTP配置为空:'.$this->arr_sp_config[$this->sp_id]['nns_config']['clip_file_addr_output']);
	    }
	    if(!isset($this->encode_file_url['media']['in']) || strlen($this->encode_file_url['media']['in']) <1)
	    {
	        return $this->__return_data(1,'转码文件输入文件为空:'.$this->encode_file_url['media']);
	    }
	    if(!isset($this->encode_file_url['media']['out']) || strlen($this->encode_file_url['media']['out']) <1)
	    {
	        return $this->__return_data(1,'转码文件输出文件为空:'.$this->encode_file_url['media']);
	    }
	    $result_media = nl_vod_media_v2::query_by_id($this->obj_dc, $this->data_queue['nns_in_media_id']);
	    if($result_media['ret'] !=0)
	    {
	        return $result_media;
	    }
	    if(!isset($result_media['data_info']['nns_ext_info']) || strlen($result_media['data_info']['nns_ext_info'])<1 || !$this->is_json($result_media['data_info']['nns_ext_info']))
	    {
	        return $this->__return_data(1,'查询的原始片源没有片源路径');
	    }
	    $nns_ext_info = json_decode($result_media['data_info']['nns_ext_info'],true);
	    if(!isset($nns_ext_info['path']) || strlen($nns_ext_info['path'])<1)
	    {
	        return $this->__return_data(1,'查询的原始片源没有片源路径path');
	    }
	    $out_bitrate = (int)$this->data_model['nns_output_bitrate'] > 0 ? (int)$this->data_model['nns_output_bitrate'] : 4000;
	    
	    $ftp_url = trim(rtrim(rtrim($this->arr_sp_config[$this->sp_id]['nns_config']['clip_file_addr_output'],'/'),'\\'));
	    $in_file_url = trim(ltrim(ltrim($this->encode_file_url['media']['in'],'/'),'\\'));
	    $out_file_url = trim(ltrim(ltrim($this->encode_file_url['media']['out'],'/'),'\\'));
	    
	    $ftp_in = $ftp_url.'/'.$in_file_url;
	    $in_arr_file_name = pathinfo($ftp_in);
	    
	    $ftp_out = $ftp_url.'/'.$out_file_url;
	    $out_arr_file_name = pathinfo($ftp_out);
	    
	    
	    if(strpos($ftp_out, 'ftp://') !== false)
	    {
	        $obj_ftp = new nl_ftp(null,null,null,21,90,false,$ftp_out);
	        $temp_data_info  = $obj_ftp->check_file_exsist($obj_ftp->path);
	        if($temp_data_info['ret'] == 0 && $temp_data_info['data_info'] >0)
	        {
	            $obj_ftp->delete_file($obj_ftp->path);
	        }
	    }
	    
	    //拆分FTP地址  输出的ftp
	    $url_arr_in = parse_url($ftp_in);
	    //拆分FTP地址  输出的ftp
	    $url_arr_out = parse_url($ftp_out);
	    
	    //输入ftp地址 和输出ftp地址 配置入模板
	    $this->arr_inout_put['input']['user']['value'] = (isset($url_arr_in["user"]) && strlen($url_arr_in["user"]) > 0) ? $url_arr_in["user"] : '';
	    $this->arr_inout_put['input']['pwd']['value'] = (isset($url_arr_in["pass"]) && strlen($url_arr_in["pass"]) > 0) ? $url_arr_in["pass"] : '';
	    $this->arr_inout_put['input']['path']['value'] = $ftp_in;
	    $this->arr_inout_put['output']['user']['value'] = (isset($url_arr_out["user"]) && strlen($url_arr_out["user"]) > 0) ? $url_arr_out["user"] : '';
	    $this->arr_inout_put['output']['pwd']['value'] = (isset($url_arr_out["pass"]) && strlen($url_arr_out["pass"]) > 0) ? $url_arr_out["pass"] : '';
	    $this->arr_inout_put['output']['path']['value'] = $out_arr_file_name['dirname'].'/'.$out_arr_file_name['filename'];
	    //视频配置入模板
	    $this->arr_video_model['general']['bc']['rate']['value'] = $out_bitrate;
	    
	    if(isset($this->data_model['nns_video_config']) && is_array($this->data_model['nns_video_config']) && !empty($this->data_model['nns_video_config']))
	    {
	        $temp_nns_config  = $this->data_model['nns_video_config'];
	        foreach ($temp_nns_config as $temp_nns_config_key=>$temp_nns_config_value)
	        {
	            $temp_nns_config_key = trim(trim($temp_nns_config_key,'|#|'));
	            if(strlen($temp_nns_config_key) <1)
	            {
	                continue;
	            }
	            $temp_nns_config_key_arr = explode("|#|", $temp_nns_config_key);
                $this->check_multi_level_exsist_and_set_value($this->arr_video_model, $temp_nns_config_key_arr, $temp_nns_config_value['value']);
	        }
	    }
	    if(isset($this->data_model['nns_audio_config']) && is_array($this->data_model['nns_audio_config']) && !empty($this->data_model['nns_audio_config']))
	    {
	        $temp_nns_config  = $this->data_model['nns_audio_config'];
	        foreach ($temp_nns_config as $temp_nns_config_key=>$temp_nns_config_value)
	        {
	            $temp_nns_config_key = trim(trim($temp_nns_config_key,'|#|'));
	            if(strlen($temp_nns_config_key) <1)
	            {
	                continue;
	            }
	            $temp_nns_config_key_arr = explode("|#|", $temp_nns_config_key);
                $this->check_multi_level_exsist_and_set_value($this->arr_audio_model, $temp_nns_config_key_arr, $temp_nns_config_value['value']);
	        }
	    }
	    if(isset($this->data_model['nns_other_config']) && is_array($this->data_model['nns_other_config']) && !empty($this->data_model['nns_other_config']))
	    {
	        $temp_nns_config  = $this->data_model['nns_other_config'];
	        foreach ($temp_nns_config as $temp_nns_config_key=>$temp_nns_config_value)
	        {
	            $temp_nns_config_key = trim(trim($temp_nns_config_key,'|#|'));
	            if(strlen($temp_nns_config_key) <1)
	            {
	                continue;
	            }
	            $temp_nns_config_key_arr = explode("|#|", $temp_nns_config_key);
	            if(count($temp_nns_config_key_arr) <2)
	            {
	                continue;
	            }
	            $temp_key_ex = array_shift($temp_nns_config_key_arr);
	            if($temp_key_ex == 'mux')
	            {
                    $this->check_multi_level_exsist_and_set_value($this->arr_mux_model, $temp_nns_config_key_arr, $temp_nns_config_value['value']);
	            }
	            else if($temp_key_ex == 'ext')
	            {
	                $this->check_multi_level_exsist_and_set_value($this->arr_ext_model, $temp_nns_config_key_arr, $temp_nns_config_value['value']);
	            }
	        }
	    }
	    
	    
	    $this->arr_video_model['general']['width']['value'] = (isset($this->data_model['nns_video_config']['width']['value']) && strlen($this->data_model['nns_video_config']['width']['value']) >0 && (int)$this->data_model['nns_video_config']['width']['value'] >0) ? (int)$this->data_model['nns_video_config']['width']['value'] : 1920;
	    $this->arr_video_model['general']['height']['value'] = (isset($this->data_model['nns_video_config']['height']['value']) && strlen($this->data_model['nns_video_config']['height']['value']) >0 && (int)$this->data_model['nns_video_config']['height']['value'] >0) ? (int)$this->data_model['nns_video_config']['height']['value'] : 1080;
	     
	    $this->arr_video_model['general']['aspectRatio']['value'] = (isset($this->data_model['nns_video_config']['aspectRatio']['value']) && strlen($this->data_model['nns_video_config']['aspectRatio']['value']) >0) ? $this->data_model['nns_video_config']['aspectRatio']['value'] : 'copy';
	    $this->arr_video_model['general']['fillMode']['value'] = (isset($this->data_model['nns_video_config']['fillMode']['value']) && strlen($this->data_model['nns_video_config']['fillMode']['value']) >0) ? $this->data_model['nns_video_config']['fillMode']['value'] : 'stretch';
	    
	    $this->arr_video_model['general']['bc']['maxRate']['value'] = (isset($this->data_model['nns_video_config']['bc-maxRate']['value']) && strlen($this->data_model['nns_video_config']['bc-maxRate']['value']) >0) ? $this->data_model['nns_video_config']['bc-maxRate']['value'] : '';
	    $this->arr_video_model['general']['bc']['minRate']['value'] = (isset($this->data_model['nns_video_config']['bc-minRate']['value']) && strlen($this->data_model['nns_video_config']['bc-minRate']['value']) >0) ? $this->data_model['nns_video_config']['bc-minRate']['value'] : '';
	    $this->arr_video_model['general']['bc']['method']['value'] = (isset($this->data_model['nns_video_config']['bc-method']['value']) && strlen($this->data_model['nns_video_config']['bc-method']['value']) >0) ? $this->data_model['nns_video_config']['bc-method']['value'] : 'cbr';
	     
	    $this->arr_video_model['h264']['basic']['scanMode']['type']['value'] = (isset($this->data_model['nns_video_config']['h264-basic-scanMode-type']['value']) && strlen($this->data_model['nns_video_config']['h264-basic-scanMode-type']['value']) >0) ? $this->data_model['nns_video_config']['h264-basic-scanMode-type']['value'] : 'i';
	     
	    //音频配置入模板
	    $this->arr_audio_model['enable']['value'] = (isset($this->data_model['nns_audio_config']['enable']['value']) && strlen($this->data_model['nns_audio_config']['enable']['value']) >0 
	        && in_array($this->data_model['nns_audio_config']['enable']['value'], array('0','1'))) ? $this->data_model['nns_audio_config']['enable']['value'] : '1';
	    $this->arr_audio_model['codec']['value'] = (isset($this->data_model['nns_audio_config']['codec']['value']) && strlen($this->data_model['nns_audio_config']['codec']['value']) >0) ? $this->data_model['nns_audio_config']['codec']['value'] : 'mp2';
	    $this->arr_audio_model['kbps']['value'] = (isset($this->data_model['nns_audio_config']['kbps']['value']) && strlen($this->data_model['nns_audio_config']['kbps']['value']) >0 && (int)$this->data_model['nns_audio_config']['kbps']['value'] >0) ? (int)$this->data_model['nns_audio_config']['kbps']['value'] : '192';
	    $this->arr_audio_model['hz']['value'] = (isset($this->data_model['nns_audio_config']['hz']['value']) && strlen($this->data_model['nns_audio_config']['hz']['value']) >0 && (int)$this->data_model['nns_audio_config']['hz']['value'] >0) ? (int)$this->data_model['nns_audio_config']['hz']['value'] : '48000';
	    $this->arr_audio_model['channels']['value'] = (isset($this->data_model['nns_audio_config']['channels']['value']) && strlen($this->data_model['nns_audio_config']['channels']['value']) >0) ? $this->data_model['nns_audio_config']['channels']['value'] : 'stereo';
	    $this->arr_audio_model['tracks']['value'] = (isset($this->data_model['nns_audio_config']['tracks']['value']) && strlen($this->data_model['nns_audio_config']['tracks']['value']) >0) ? $this->data_model['nns_audio_config']['tracks']['value'] : '0';
	    
	    
	    $this->arr_audio_model['multiOutput'][0]['volume']['value'] = (isset($this->data_model['nns_audio_config']['multiOutput-volume']['value']) && strlen($this->data_model['nns_audio_config']['multiOutput-volume']['value']) >0) ? $this->data_model['nns_audio_config']['multiOutput-volume']['value'] : '0';
	    $this->arr_audio_model['multiOutput'][0]['codec']['value'] = (isset($this->data_model['nns_audio_config']['multiOutput-codec']['value']) && strlen($this->data_model['nns_audio_config']['multiOutput-codec']['value']) >0) ? $this->data_model['nns_audio_config']['multiOutput-codec']['value'] : 'mp2';
	    $this->arr_audio_model['multiOutput'][0]['bitrate']['value'] = (isset($this->data_model['nns_audio_config']['multiOutput-bitrate']['value']) && strlen($this->data_model['nns_audio_config']['multiOutput-bitrate']['value']) >0) ? $this->data_model['nns_audio_config']['multiOutput-bitrate']['value'] : '192';
	    $this->arr_audio_model['multiOutput'][0]['ar']['value'] = (isset($this->data_model['nns_audio_config']['multiOutput-ar']['value']) && strlen($this->data_model['nns_audio_config']['multiOutput-ar']['value']) >0) ? $this->data_model['nns_audio_config']['multiOutput-ar']['value'] : '48000';
	    $this->arr_audio_model['multiOutput'][0]['channels']['value'] = (isset($this->data_model['nns_audio_config']['multiOutput-channels']['value']) && strlen($this->data_model['nns_audio_config']['multiOutput-channels']['value']) >0) ? $this->data_model['nns_audio_config']['multiOutput-channels']['value'] : 'stereo';
	     
	    
	    
	    //混合配置入模板
	    
	    //扩展信息入模板
	    
	    $arr_data = array(
	        'report'=>'0',
	        'replyKey'=>$this->data_queue['nns_id'],
	        'cmdBody'=>array(
	            'cmd'=>'TMS_SYS_SET_TASK',
	            'task'=>array(
	                'extData'=>null,
	                'inout'=>$this->arr_inout_put,
	                'video'=>$this->arr_video_model,
	                'audio'=>$this->arr_audio_model,
	                'mux'=>$this->arr_mux_model,
	                'ext'=>$this->arr_ext_model,
	            )
	        ),
	    );
	    $str_json_data = json_encode($arr_data);
	    $result = $this->_execute_curl($this->request_url['video'],$this->curl_way, $str_json_data);
        if($result['ret'] !=0)
	    {
	        $log_file = $this->make_file_log($str_json_data, '');
            $this->make_file_encode_log($this->data_queue['nns_id'],"转码请求失败，描述[{$result['reason']}]",0,1,$log_file['request'],$log_file['notify']);
            nl_file_encode::edit($this->obj_dc, array('nns_state'=>2), $this->data_queue['nns_id']);
	        return $this->__return_data(1,$result['reason']);
	    }
	    $message_content = (isset($result['data_info']) && strlen($result['data_info'])>0) ? $result['data_info'] : '';
	    $curl_data = json_decode($message_content,true);
        $log_file = $this->make_file_log($str_json_data, $message_content);
	    if($curl_data['code'] == 0)
	    {
	        $result = $this->make_file_encode_log($this->data_queue['nns_id'],"请求转码成功",0,0,$log_file['request'],$log_file['notify']);
	        nl_file_encode::edit($this->obj_dc, array('nns_state'=>1,'nns_loadbalance_id'=>$this->arr_params['nns_id']), $this->data_queue['nns_id']);
	        return $this->__return_data(0,'sucess');
	    }
        $this->make_file_encode_log($this->data_queue['nns_id'],"请求转码失败",0,1,$log_file['request'],$log_file['notify']);
        nl_file_encode::edit($this->obj_dc, array('nns_state'=>2), $this->data_queue['nns_id']);
        return $this->__return_data(1,$message_content);
	}
	
	
	function check_multi_level_exsist_and_set_value(&$set_data, $array_keys, $value)
	{
	    if(is_array($array_keys) && !empty($array_keys))
	    {
	        $key = array_shift($array_keys);
	    }
	    else
	    {
	        $set_data['value'] = $value;
	        return ;
	    }
	    if (isset($set_data[$key]))
	    {
	        $this->check_multi_level_exsist_and_set_value($set_data[$key],$array_keys,$value);
	    }
	    return ;
	}
	
	/**
	 * 终止转码数据
	 */
	public function cancel_task($task_id)
	{
	    $inout_xml ='<?xml version="1.0" encoding="utf-8"?>';
	    $inout_xml.='<message module="xCoder" version="1.0">';
	    $inout_xml.=   '<header action="REQUEST" command="TASK_CANCEL" />';
	    $inout_xml.=   '<body>';
	    $inout_xml.=   '<task id="'.$task_id.'" />';
	    $inout_xml.=   '</body>';
	    $inout_xml.='</message>';
	    $content_length = strlen($inout_xml);
	    $arr_header = array(
	        "Content-type: text/xml;charset=\"utf-8\"",
	        "Accept: text/xml",
	        "Cache-Control: no-cache",
	        "Pragma: no-cache",
	        "SOAPAction: \"run\"",
	        "Content-Length: {$content_length}",
	    );
	    $result = $this->_execute_curl($this->request_url['video'],$this->curl_way, $inout_xml,$arr_header);
	    if($result['ret'] !=0)
	    {
	        return $result;
	    }
	    $message_content = (isset($result['data_info']) && strlen($result['data_info'])>0) ? $result['data_info'] : '';
	    if(!$this->is_xml($message_content))
	    {
	        return $this->__return_data(1,"反馈的数据非xml");
	    }
	    //解析下载的XML
	    $dom = new DOMDocument('1.0', 'utf-8');
	    $dom->loadXML($message_content);
	    $task = $dom->getElementsByTagName('result');
    	if($task->length <1)
        {
            return $this->__return_data(1,"无队列状态查询到",array('nns_request_state'=>0,'nns_notify_state'=>1,'nns_request_content'=>$inout_xml,'nns_notify_content'=>$message_content));
        }
        $nns_request_content = trim($inout_xml);
        $nns_notify_content = trim($message_content);
        $log_file = $this->make_file_log($nns_request_content, $nns_notify_content);
        $temp_task = null;
	    foreach ($task as $task_val)
        {
            $id = $task_val->getAttribute('task');
            $status = $task_val->getAttribute('code');
            $cause = $task_val->getAttribute('description');
            if(strlen($id) <1 || strlen($status) <1)
            {
                continue;
            }
            $result_exsist = nl_file_encode::query_by_integer_id($this->obj_dc, $id);
            if($result_exsist['ret'] !=0)
            {
                return $result_exsist;
            }
            if(!isset($result_exsist['data_info']) || !is_array($result_exsist['data_info']) || empty($result_exsist['data_info']))
            {
                $temp_task[]=$id;
                continue;
            }
            $result_exsist = $result_exsist['data_info'];
            if($status == '1')
            {
                $this->make_file_encode_log($result_exsist['nns_id'],"转码队列任务终止成功，描述[{$cause}]",0,0,$log_file['request'],$log_file['notify']);
            }
            else
            {
                $this->make_file_encode_log($result_exsist['nns_id'],"转码队列任务终止失败，描述[{$cause}]",0,1,$log_file['request'],$log_file['notify']);
            }
        }
        return $this->__return_data(0,'ok',$temp_task);
	}
	
	/**
	 * 删除转码数据
	 */
	public function del_task($id=NULL)
	{
	    $inout_xml ='<?xml version="1.0" encoding="utf-8"?>';
	    $inout_xml.='<message module="xCoder" version="1.0">';
	    $inout_xml.=   '<header action="REQUEST" command="TASK_DELETE" />';
	    $inout_xml.=   '<body>';
	    if(strlen($id) >0)
	    {
	        $inout_xml.=   '<task id="'.$id.'" />';
	    }
	    else
	    {
	        $inout_xml.=   '<task id="" />';
	    }
	    $inout_xml.=   '</body>';
	    $inout_xml.='</message>';
	    $content_length = strlen($inout_xml);
	    $arr_header = array(
	        "Content-type: text/xml;charset=\"utf-8\"",
	        "Accept: text/xml",
	        "Cache-Control: no-cache",
	        "Pragma: no-cache",
	        "SOAPAction: \"run\"",
	        "Content-Length: {$content_length}",
	    );
	    $result = $this->_execute_curl($this->request_url['video'],$this->curl_way, $inout_xml,$arr_header);
	    if($result['ret'] !=0)
	    {
	        return $result;
	    }
	    $message_content = (isset($result['data_info']) && strlen($result['data_info'])>0) ? $result['data_info'] : '';
	    if(!$this->is_xml($message_content))
	    {
	        return $this->__return_data(1,"反馈的数据非xml");
	    }
	    //解析下载的XML
	    $dom = new DOMDocument('1.0', 'utf-8');
	    $dom->loadXML($message_content);
	    $task = $dom->getElementsByTagName('result');
    	if($task->length <1)
        {
            return $this->__return_data(1,"无队列状态查询到",array('nns_request_state'=>0,'nns_notify_state'=>1,'nns_request_content'=>$inout_xml,'nns_notify_content'=>$message_content));
        }
        $nns_request_content = trim($inout_xml);
        $nns_notify_content = trim($message_content);
        $log_file = $this->make_file_log($nns_request_content, $nns_notify_content);
        $temp_task = null;
	    foreach ($task as $task_val)
        {
            $id = $task_val->getAttribute('task');
            $status = $task_val->getAttribute('code');
            $cause = $task_val->getAttribute('description');
            if(strlen($id) <1 || strlen($status) <1)
            {
                continue;
            }
            $result_exsist = nl_file_encode::query_by_integer_id($this->obj_dc, $id);
            if($result_exsist['ret'] !=0)
            {
                return $result_exsist;
            }
            if(!isset($result_exsist['data_info']) || !is_array($result_exsist['data_info']) || empty($result_exsist['data_info']))
            {
                $temp_task[]=$id;
                continue;
            }
            $result_exsist = $result_exsist['data_info'];
            if($status == '1')
            {
                $this->make_file_encode_log($result_exsist['nns_id'],"转码队列删除成功，描述[{$cause}]",0,0,$log_file['request'],$log_file['notify']);
            }
            else
            {
                $this->make_file_encode_log($result_exsist['nns_id'],"转码队列删除失败，描述[{$cause}]",0,1,$log_file['request'],$log_file['notify']);
            }
        }
        return $this->__return_data(0,'ok',$temp_task);
	}
	
	/**
	 * 获取转码状态
	 */
	public function get_task_state()
	{
	    foreach ($this->data_queue as $val)
	    {
	        $arr_data = array(
	            'report'=>'0',
	            'replyKey'=>$val['nns_id'],
	            'cmdBody'=>array(
	                'cmd'=>'TMS_SYS_GET_TASK_STATUS',
	            ),
	        );
	        $this->init(0);
	        if(strlen($this->request_url['video']) <1)
	        {
	            continue;
	        }
	        $str_json_data = json_encode($arr_data);
	        $result = $this->_execute_curl($this->request_url['video'],$this->curl_way, $str_json_data);
	        if($result['ret'] !=0)
	        {
	            return $result;
	        }
	        $message_content = (isset($result['data_info']) && strlen($result['data_info'])>0) ? $result['data_info'] : '';
	        $curl_data = json_decode($message_content,true);
	        $log_file = $this->make_file_log($str_json_data, $message_content);
	        if(isset($curl_data['code']) && strlen($curl_data['code']) >0 && $curl_data['code'] == '0')
	        {
	            if(isset($curl_data['info']) && strlen($curl_data['info']) >0 && $curl_data['info']=='100')
	            {
	                $this->make_file_encode_log($val['nns_id'],"转码成功，描述[{$curl_data['message']}]",0,0,$log_file['request'],$log_file['notify']);
    	            nl_file_encode::edit($this->obj_dc, array('nns_state'=>3,'nns_priority'=>0), $val['nns_id']);
                    nl_op_queue::edit($this->obj_dc, array('nns_status'=>0), $val['nns_op_id']);

    	            $this->notify_cp_sp(array('ret'=>0,'reason'=>"转码成功，描述[{$curl_data['message']}]"), $val);
	            }
	            else
	            {
	                $this->make_file_encode_log($val['nns_id'],"正在转码，进度{$curl_data['info']}%",0,0,$log_file['request'],$log_file['notify']);
	            }
	        }
	        else
	        {
	            $this->make_file_encode_log($val['nns_id'],"转码失败，描述[{$curl_data['message']}]",0,0,$log_file['request'],$log_file['notify']);
	            nl_file_encode::edit($this->obj_dc, array('nns_state'=>2), $val['nns_id']);
	            $this->notify_cp_sp(array('ret'=>1,'reason'=>"转码失败，描述[{$curl_data['message']}]"), $val);
	        }
	    }
        return $this->__return_data(0,'ok');
	}
	
	/**
	 * 生成日志文件
	 */
	public function make_file_log($nns_request_content='',$nns_notify_content='')
	{
	    $database_url = date("Ymd").'/'.date("H");
	    if(strlen($nns_notify_content)>0)
	    {
	        $dir_file = 'mgtv/file_encode/'.strtolower($this->sp_id).'/notify';
	        $nns_notify_content = $this->_make_log_file($nns_notify_content, $database_url,$dir_file);
	        $nns_notify_content = (isset($nns_notify_content['data_info']) && strlen($nns_notify_content['data_info'])>0) ? $nns_notify_content['data_info'] : '';
	    }
	    else
	    {
	        $nns_notify_content='';
	    }
	    if(strlen($nns_request_content)>0)
	    {
	        $dir_file = 'mgtv/file_encode/'.strtolower($this->sp_id).'/request';
	        $nns_request_content = $this->_make_log_file($nns_request_content, $database_url,$dir_file);
	        $nns_request_content = (isset($nns_request_content['data_info']) && strlen($nns_request_content['data_info'])>0) ? $nns_request_content['data_info'] : '';
	    }
	    else
	    {
	        $nns_request_content='';
	    }
	    return array(
	        'request'=>$nns_request_content,
	        'notify'=>$nns_notify_content,
	    );
	}
	
	/**
	 * 插入转码日志
	 * @param unknown $file_encode_id
	 * @param unknown $nns_request_state
	 * @param string $nns_request_state
	 * @param unknown $nns_notify_state
	 * @param string $nns_notify_content
	 * @param string //$nns_desc
	 */
	public function make_file_encode_log($file_encode_id,$nns_desc,$nns_request_state=0,$nns_notify_state=0,$nns_request_content='',$nns_notify_content='')
	{
	    if(strlen($file_encode_id) <1)
	    {
	        return $this->__return_data(1,'file_encode_id 参数为空不生成转码日志');
	    }
	    $arr_params = array(
	        'nns_id'=>np_guid_rand('file_encode_log'),
	        'nns_file_encode_id'=>$file_encode_id,
	        'nns_desc'=>$nns_desc,
	        'nns_request_state'=>$nns_request_state,
	        'nns_notify_state'=>$nns_notify_state,
	        'nns_request_content'=>$nns_request_content,
	        'nns_notify_content'=>$nns_notify_content,
	    );
	    return nl_file_encode_log::add($this->obj_dc, $arr_params);
	}
	
	/**
	 * 消息反馈
	 * @param unknown $cp_id
	 * @param unknown $result_encode
	 * @param unknown $encode_info
	 */
	public function notify_cp_sp($result_encode,$encode_info)
	{
	    $this->_get_cp_info($encode_info['nns_cp_id']);
	    if(isset($this->arr_cp_config[$encode_info['nns_cp_id']]['nns_config']['video_source_notify_url']) && strlen($this->arr_cp_config[$encode_info['nns_cp_id']]['nns_config']['video_source_notify_url']) >0)
        {
            $data_op_queue = nl_op_queue::query_by_id($this->obj_dc, $encode_info['nns_op_id']);
            if(isset($data_op_queue['data_info']) && !empty($data_op_queue['data_info']) && is_array($data_op_queue['data_info']))
            {
                $playbill_info = array(
                    'id'=>$data_op_queue['data_info']['nns_message_id'],
                    'state'=>$result_encode['ret'] == '0' ? 2 : 3,
                    'summary'=>$result_encode['reason'],
                );
                $this->_execute_curl($this->arr_cp_config[$encode_info['nns_cp_id']]['nns_config']['video_source_notify_url'],'post',array('a'=>'update_vod_playbill_transcode_state','playbill_info'=>json_encode(array($playbill_info))));
            }
        }
	}
}
