<?php
include_once dirname(dirname(__FILE__)).'/public_return.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/np/np_http_curl.class.php';
class ftp_push extends nn_public_return
{
    public $obj_curl = null;
    
    public $push_request_params = array(
        'des_file_path',
        'local_file_path',
        'queue_id',
        'request_url'
    );
    public $state_request_params = array(
        'queue_id',
        'request_url'
    );
    /**
     * 初始化
     */
    public function __construct()
    {
        $this->obj_curl = new np_http_curl_class();
    }
    
    /**
     * 片源推送
     * @param unknown $file_params
     * @return multitype:number
     */
    public function go_push_ts_media($file_params)
    {
        foreach ($this->push_request_params as $val)
        {
            if(!isset($file_params[$val]) || strlen($file_params[$val]) <1)
            {
                return $this->__return_data(1,"传入参数错误或者值为空，需要的参数：".var_export($this->push_request_params,true).";实际传入的参数：".var_export($file_params,true));
            }
        }
        $params = array(
            'action'=>'upload',
            'target'=>$file_params['des_file_path'],
            'source'=>$file_params['local_file_path'],
            'id'=>$file_params['queue_id'],
        );
        $str_params = '';
        foreach ($params as $key=>$val)
        {
            $str_params.="{$key}={$val}&";
        }
        $str_params = trim($str_params,'&');
        $result = $this->obj_curl->post($file_params['request_url'], $str_params);
        $curl_info = $this->obj_curl->curl_getinfo();
        if($curl_info['http_code'] !='200')
        {
            return $this->__return_data(1,'curl 反馈状态码非200:'.var_export($curl_info,true).';请求地址'.$file_params['request_url'].";请求参数:".$str_params);
        }
        $result = json_decode($result,true);
        nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, "请求参数：".var_export($params,true).';输出结果：'.var_export($result,true), $file_params['org_id'], 'media');
        return $this->__return_data(($result['state'] == 0) ? 0 : 1,"请求参数：".var_export($params,true).';输出结果：'.var_export($result,true));
    }
    
    /**
     * 片源查询状态
     * @param unknown $queue_params
     * @return multitype:number
     */
    public function go_get_push_state($queue_params)
    {
        foreach ($this->state_request_params as $val)
        {
            if(!isset($file_params[$val]) || strlen($queue_params[$val]) <1)
            {
                return $this->__return_data(1,"传入参数错误或者值为空，需要的参数：".var_export($this->state_request_params,true).";实际传入的参数：".var_export($queue_params,true));
            }
        }
        $params = array(
            'action'=>'status',
            'id'=>$queue_params['queue_id'],
        );
        $str_params = '';
        foreach ($params as $key=>$val)
        {
            $str_params.="{$key}={$val}&";
        }
        $str_params = trim($str_params,'&');
        $result = $this->obj_curl->post($queue_params['request_url'], $str_params);
        $curl_info = $this->obj_curl->curl_getinfo();
        if($curl_info['http_code'] !='200')
        {
            return $this->__return_data(1,'curl 反馈状态码非200:'.var_export($curl_info,true));
        }
        $result = json_decode($result,true);
        return $this->__return_data(($result['state'] == 0) ? 0 : 1,"请求参数：".var_export($params,true).';输出结果：'.var_export($result,true));
    }
    
    /**
     * 销毁
     */
    public function __destruct()
    {
        unset($this->obj_curl);
    }
}