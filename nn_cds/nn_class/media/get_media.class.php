<?php
include_once dirname(dirname(__FILE__)).'/public_return.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/np/np_http_curl.class.php';
class get_media extends nn_public_return
{
    /**
     * CURL object
     * @var unknown
     */
    public $obj_curl = null;
    
    public $third_part_url = null;
    
    /**
     * 初始化
     */
    public function __construct($third_part_url)
    {
        $this->third_part_url = trim(trim($third_part_url,'?'));
        $this->obj_curl = new np_http_curl_class();
    }
    
    
    /**
     * 添加片源获取播放串
     * @param unknown $arr_media_ids
     * @return multitype:number
     */
    public function get_add_media_play_url($arr_media_ids)
    {
        if(!is_array($arr_media_ids) || empty($arr_media_ids))
        {
            return $this->__return_data(1,'media ID 非数组:'.var_export($arr_media_ids,true));
        }
        $str_media_id = implode(',', $arr_media_ids);
        $this->third_part_url.="?action=sync&packageid={$str_media_id}";
        $result = $this->obj_curl->get($this->third_part_url);
        $curl_info = $this->obj_curl->curl_getinfo();
        if($curl_info['http_code'] !='200')
        {
            return $this->__return_data(1,'curl 反馈状态码非200:'.var_export($curl_info,true));
        }
        return $this->__return_data(0,"请求参数：".var_export($this->third_part_url,true).';输出结果：'.var_export($result,true),$result);
    }
    
    /**
     * 删除片源获取状态
     * @param unknown $arr_media_ids
     * @return multitype:number
     */
    public function get_del_media_play_url($arr_media_ids)
    {
        if(!is_array($arr_media_ids) || empty($arr_media_ids))
        {
            return $this->__return_data(1,'media ID 非数组:'.var_export($arr_media_ids,true));
        }
        $str_media_id = implode(',', $arr_media_ids);
        $this->third_part_url.="?action=delete&packageid={$str_media_id}";
        $result = $this->obj_curl->get($this->third_part_url);
        $curl_info = $this->obj_curl->curl_getinfo();
        if($curl_info['http_code'] !='200')
        {
            return $this->__return_data(1,'curl 反馈状态码非200:'.var_export($curl_info,true));
        }
        return $this->__return_data(0,"请求参数：".var_export($this->third_part_url,true).';输出结果：'.var_export($result,true),$result);
    }
    
    /**
     * 数据销毁
     */
    public function __destruct()
    {
        unset($this->obj_curl);
    }
}