<?php
include_once dirname(dirname(__FILE__)).'/public_return.class.php';
class nn_soap_notify extends nn_public_return
{
    public $arr_drm_params = array(
                'drm_flag'=>0,
                'drm_encrypt_solution'=>'',
                'drm_ext_info'=>'',
            );
    /**
     * 初始化
     * @param string $obj_dc
     * @param string $cp_id
     * @param string $cp_config
     * @author liangpan
     * @date 2016-11-28
     */
    public function __construct($obj_dc=null,$cp_id=NULL,$cp_config=NULL)
    {
        $this->cp_id = $cp_id;
        $this->arr_cp_config = $cp_config;
        if(is_object($obj_dc))
        {
            $this->obj_dc = $obj_dc;
        }
        else
        {
            $this->obj_dc = nl_get_dc(array (
                'db_policy' => NL_DB_WRITE,
                'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
            ));
        }
    }
    
    /**
     * 组装DRM 参数
     * @param array $params
     * @author liangpan
     * @date 2016-11-28
     */
    public function make_drm_params($params=null)
    {
        $cp_config = $this->__get_cp_config();
        if($cp_config['ret'] !=0)
        {
            return $cp_config;
        }
        $result_data = null;
        $cp_config = (isset($cp_config['data_info']['base_config']) && is_array($cp_config['data_info']['base_config']) && !empty($cp_config['data_info']['base_config'])) ? $cp_config['data_info']['base_config'] : null;
        if(isset($cp_config['main_drm_enabled']) && intval($cp_config['main_drm_enabled']) != 1)
        {
            return $this->__return_right_data($this->arr_drm_params);
        }
        if(isset($cp_config['q_marlin_drm_enable']) && intval($cp_config['q_marlin_drm_enable']) == 1)
        {
            return $this->make_marlin_drm($params);
        }
        return $this->__return_right_data($this->arr_drm_params);
    }
    
    /**
     * 组装marlin 参数
     * @param array $params   必要参数[asset_type(媒资类型点播、直播)|nns_id(媒资点播GUID、直播GUID)]
     * @author liangpan
     * @date 2016-11-28
     */
    private function make_marlin_drm($params)
    {
        $this->arr_drm_params['drm_flag'] = 1;
        $this->arr_drm_params['drm_encrypt_solution'] = 'marlin';
        $this->arr_drm_params['drm_ext_info'] = null;
        if(!isset($params['asset_type']) || strlen($params['asset_type']) <1)
        {
            $this->set_error_desc("方法:".__FUNCTION__.";行数：".__LINE__.";传入参数错误，未设置asset_type参数，参数为：".var_export($params,true));
            return $this->__return_error_data();
        }
        if(!isset($params['nns_id']) || strlen($params['nns_id']) <1)
        {
            $this->set_error_desc("方法:".__FUNCTION__.";行数：".__LINE__.";传入参数错误，未设置nns_id参数，参数为：".var_export($params,true));
            return $this->__return_error_data();
        }
        $num = 1;
        $temp_rand_params = '';
        switch ($params['asset_type'])
        {
            case "vod":
                $num = (isset($this->arr_cp_config['base_config']['marlin_drm_config']['marlin_drm_vod_num']) && (int)$this->arr_cp_config['base_config']['marlin_drm_config']['marlin_drm_vod_num'] > 0) ? (int)$this->arr_cp_config['base_config']['marlin_drm_config']['marlin_drm_vod_num'] : 1;
                $temp_rand_params = rand(0,9999)."_";
                break;
            case "live":
                $num = (isset($this->arr_cp_config['base_config']['marlin_drm_config']['marlin_drm_live_num']) && (int)$this->arr_cp_config['base_config']['marlin_drm_config']['marlin_drm_live_num'] > 0) ? (int)$this->arr_cp_config['base_config']['marlin_drm_config']['marlin_drm_live_num'] : 10;
                break;
            default:
                $this->set_error_desc("方法:".__FUNCTION__.";行数：".__LINE__.";传入参数错误，asset_type参数，参数为：".var_export($params,true).";匹配的参数值为:[vod|live]");
                return $this->__return_error_data();
        }
        for ($i=1;$i<=$num;$i++)
        {
            $str_temp = MD5($params['nns_id']."_".$temp_rand_params.$i);
            $this->arr_drm_params['drm_ext_info'][$str_temp] = MD5($str_temp);
        }
        return $this->__return_right_data($this->arr_drm_params);
    }
}