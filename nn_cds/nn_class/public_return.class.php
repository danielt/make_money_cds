<?php
class nn_public_return
{
    public $cp_id = null;
    public $sp_id = null;
    public $arr_cp_config = null;
    public $arr_sp_config = null;
    //错误描述
    public $error_desc = null;
    public $obj_dc = null;
    public $obj_dom = null;
    
    /**
     * @return the $error_desc
     * @author liangpan
     * @date 2016-11-28
     */
    public function __get_error_desc()
    {
        return $this->error_desc;
    }
    
    /**
     * @param field_type $error_desc
     * @author liangpan
     * @date 2016-11-28
     */
    public function __set_error_desc($error_desc)
    {
        $this->error_desc[] = $error_desc;
    }
    
    
    /**
     * 反馈正确数据
     * @param string $data_info
     * @return multitype:number string
     * @author liangpan
     * @date 2016-11-28
     */
    public function __return_right_data($data_info=null)
    {
        return array(
            'ret'=>0,
            'reason'=>'ok',
            'data_info'=>($data_info === null) ? '' : $data_info
        );
    }
    
    /**
     * 反馈错误数据
     * @return multitype:number string
     * @author liangpan
     * @date 2016-11-28
     */
    public function __return_error_data($data_info=null)
    {
        return array(
            'ret'=>1,
            'reason'=>var_export($this->__get_error_desc(),true),
            'data_info'=>($data_info === null) ? '' : $data_info
        );
    }
    
    
    /**
     * 反馈错误数据
     * @return multitype:number string
     * @author liangpan
     * @date 2016-11-28
     */
    public function __return_data($ret,$reason=null,$data_info=null)
    {
        return array(
            'ret'=>$ret,
            'reason'=>($reason === null) ? '' : $reason,
            'data_info'=>($data_info === null) ? '' : $data_info
        );
    }
    
    /**
     * 获取CP配置（单个CP）
     * @param string $cp_id
     * @return boolean
     * @author liangpan
     * @date 2016-11-28
     */
    public function __get_cp_config()
    {
        if(!empty($this->arr_cp_config) && is_array($this->arr_cp_config))
        {
            return $this->__return_right_data($this->arr_cp_config);
        }
        if(strlen($this->cp_id) < 1)
        {
            $this->__set_error_desc("cp id 错误，传入参数为：".var_dump($this->cp_id,true));
            return $this->__return_error_data();
        }
        $result_info=nl_cp::query_by_id($this->obj_dc, $this->cp_id);
        if($result_info['ret'] !=0)
        {
            $this->set_error_info("cp id 查询 sql 错误：".var_dump($result_info['reason'],true));
            return $this->__return_error_data();
        }
        $this->arr_cp_config['base_info'] = $this->arr_cp_config['base_config'] = null;
        if(isset($result_info['data_info']['nns_config']) && strlen($result_info['data_info']['nns_config']) >0)
        {
            $result_info_config = json_decode($result_info['data_info']['nns_config'],true);
            $this->arr_cp_config['base_config'] = (is_array($result_info_config) && !empty($result_info_config)) ? $result_info_config : null;
        }
        if(isset($result_info['data_info']['nns_config']))
        {
            unset($result_info['data_info']['nns_config']);
        }
        if(is_array($result_info['data_info']) && !empty($result_info['data_info']))
        {
            foreach ($result_info['data_info'] as $data_info_key=>$data_info_val)
            {
                $this->arr_cp_config['base_info'][$data_info_key]=$data_info_val;
            }
        }
        return $this->__return_right_data($this->arr_cp_config);
    }
    
    /**
     * 获取SP配置（单个SP）
     * @param string $sp_id
     * @return boolean
     * @author liangpan
     * @date 2016-11-28
     */
    public function __get_sp_config()
    {
        if(!empty($this->arr_sp_config) && is_array($this->arr_sp_config))
        {
            return $this->__return_right_data($this->arr_sp_config);
        }
        if(strlen($this->sp_id) < 1)
        {
            $this->set_error_info("sp id 错误，传入参数为：".var_dump($this->sp_id,true));
            return $this->__return_error_data();
        }
        $result_info=nl_sp::query_by_id($this->obj_dc, $this->sp_id);
        if($result_info['ret'] !=0)
        {
            $this->set_error_info("sp id 查询 sql 错误：".var_dump($result_info['reason'],true));
            return $this->__return_error_data();
        }
        $this->arr_sp_config['base_info'] = $this->arr_sp_config['base_config'] = null;
        if(isset($result_info['data_info'][0]['nns_config']) && strlen($result_info['data_info'][0]['nns_config']) >0)
        {
            $result_info_config = json_decode($result_info['data_info'][0]['nns_config'],true);
            if(is_array($result_info_config) && !empty($result_info_config))
            {
                $this->arr_sp_config['base_config'] = (is_array($result_info_config) && !empty($result_info_config)) ? $result_info_config : null;
            }
        }
        if(isset($result_info['data_info'][0]['nns_config']))
        {
            unset($result_info['data_info'][0]['nns_config']);
        }
        if(isset($result_info['data_info'][0]['nns_bind_cp']))
        {
            $temp_bind_cp_conf = explode(',', $result_info['data_info'][0]['nns_bind_cp']);
            if(is_array($temp_bind_cp_conf) && !empty($temp_bind_cp_conf))
            {
                foreach ($temp_bind_cp_conf as $val)
                {
                    if(strlen($val) <1)
                    {
                        continue;
                    }
                    $this->arr_sp_config['base_info']['bind_cp_conf'][]=$val;
                }
            }
            unset($result_info['data_info'][0]['nns_bind_cp']);
        }
        if(is_array($result_info['data_info'][0]) && !empty($result_info['data_info'][0]))
        {
            foreach ($result_info['data_info'][0] as $data_info_key=>$data_info_val)
            {
                $this->arr_sp_config['base_info'][$data_info_key]=$data_info_val;
            }
        }
        return $this->__return_right_data($this->arr_sp_config);
    }
    
    
    /**
     * 反解析获取数据信息
     * @param unknown $dc
     * @param unknown $video_type
     * @param unknown $array_query
     */
    public function get_asset_info($video_type,$array_query)
    {
        if(!is_array($array_query) || empty($array_query))
        {
            return $this->__return_data(1,"查询条件数组不能为空");
        }
        switch ($video_type)
        {
            case 'video':
                $table_name="nns_vod";
                break;
            case 'index':
                $table_name="nns_vod_index";
                break;
            case 'media':
                $table_name="nns_vod_media";
                break;
            case 'live':
                $table_name="nns_live";
                break;
            case 'live_index':
                $table_name="nns_live_index";
                break;
            case 'live_media':
                $table_name="nns_live_media";
                break;
            case 'playbill':
                $table_name="nns_live_playbill_item";
                break;
            case 'file':
                $table_name="nns_file_package";
                break;
            default:
                return $this->__return_data(1,"未查询到媒资类型");
        }
        $sql="select * from {$table_name} where ";
        foreach ($array_query as $key=>$val)
        {
            $sql.=" {$key}='{$val}' and ";
        }
        $sql=substr(trim($sql), 0,-3)." limit 1";
        $result = nl_query_by_db($sql, $this->obj_dc->db());
        if(!$result)
        {
            return $this->__return_data(1,'error|sql:'.$sql);
        }
        $data = (isset($result['0']) && is_array($result['0']) && !empty($result['0'])) ? $result['0'] : null;
        return $this->__return_data(0,'ok|sql:'.$sql,$data);
    }
    
    
    /**
     * 获取CP信息
     * @param unknown $cp_id
     */
    public function _get_cp_info($cp_id)
    {
        if(strlen($cp_id) <1)
        {
            return $this->__return_data(1, '$cp_id为空');
        }
        if(isset($this->arr_cp_config[$cp_id]))
        {
            return $this->__return_data(0, 'ok');
        }
        $result_cp = nl_cp::query_by_id($this->obj_dc,$cp_id);
        if($result_cp['ret'] !=0)
        {
            return $result_cp;
        }
        if(!isset($result_cp['data_info']) || !is_array($result_cp['data_info']) || empty($result_cp['data_info']))
        {
            $this->arr_cp_config[$cp_id] = null;
            return $result_cp;
        }
        if($this->is_json($result_cp['data_info']['nns_config']))
        {
            $result_cp['data_info']['nns_config'] = json_decode($result_cp['data_info']['nns_config'],true);
        }
        $this->arr_cp_config[$cp_id]=$result_cp['data_info'];
        return $result_cp;
    }
    
    /**
     * 获取SP信息
     * @param unknown $sp_id
     */
    public function _get_sp_info($sp_id)
    {
        if(strlen($sp_id) <1)
        {
            return $this->__return_data(1, '$sp_id为空');
        }
        if(isset($this->arr_sp_config[$sp_id]))
        {
            return $this->__return_data(0, 'ok');
        }
        $result_sp = nl_sp::query_by_id($this->obj_dc,$sp_id);
        if($result_sp['ret'] !=0)
        {
            return $result_sp;
        }
        if(!isset($result_sp['data_info'][0]) || !is_array($result_sp['data_info'][0]) || empty($result_sp['data_info'][0]))
        {
            $this->arr_sp_config[$sp_id] = null;
            return $result_sp;
        }
        if($this->is_json($result_sp['data_info'][0]['nns_config']))
        {
            $result_sp['data_info'][0]['nns_config'] = json_decode($result_sp['data_info'][0]['nns_config'],true);
        }
        $this->arr_sp_config[$sp_id]=$result_sp['data_info'][0];
        return $result_sp;
    }
    
    
    /**
     * 获取所有sP信息
     */
    public function _get_all_sp_info()
    {
        $result_sp = nl_sp::query_all($this->obj_dc);
        if($result_sp['ret'] !=0 || !isset($result_sp['data_info']) || !is_array($result_sp['data_info']) || empty($result_sp['data_info']))
        {
            return $result_sp;
        }
        foreach ($result_sp['data_info'] as $sp_val)
        {
            if($this->is_json($sp_val['nns_config']))
            {
                $sp_val['nns_config'] = json_decode($sp_val['nns_config'],true);
            }
            $this->arr_sp_config[$sp_val['nns_id']] = $sp_val;
        }
        return $result_sp;
    }
    
    
    /**
     * 获取所有CP信息
     * @param unknown $cp_id
     */
    public function _get_all_cp_info()
    {
        $result_cp = nl_cp::query_all($this->obj_dc);
        if($result_cp['ret'] !=0 || !isset($result_cp['data_info']) || !is_array($result_cp['data_info']) || empty($result_cp['data_info']))
        {
            return $result_cp;
        }
        foreach ($result_cp['data_info'] as $cp_val)
        {
            if($this->is_json($cp_val['nns_config']))
            {
                $cp_val['nns_config'] = json_decode($cp_val['nns_config'],true);
            }
            $this->arr_cp_config[$cp_val['nns_id']] = $cp_val;
        }
        return $result_cp;
    }
    
    /**
     * 判断是否是json
     * @param unknown $string
     * @return boolean
     */
    public function is_json($string)
    {
        $string = strlen($string) <1 ? '' : $string;
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
    
    /**
     * 判断是否是xml
     * @param unknown $string
     * @return boolean
     */
    public function is_xml($string)
    {
        $string = strlen($string) < 1 ? '' : $string;
        $xml_parser = xml_parser_create();
        if (! xml_parse($xml_parser, $string, true)) 
        {
            xml_parser_free($xml_parser);
            return false;
        }
        return true;
    }
    
    /**
     * 生成文件
     * @param unknown $content
     * @param unknown $type
     */
    public function _make_log_file($content,$database_url,$dir_url='',$file_name=null)
    {
        if(strlen($content) <1)
        {
            return $this->__return_data(0,'ok','');
        }
        if(!isset($file_name) || strlen($file_name))
        {
            $file_ext = '.txt';
            if($this->is_json($content))
            {
                $file_ext='.txt';
            }
            else if($this->is_xml($content))
            {
                $file_ext='.xml';
            }
            $file_name = np_guid_rand('_make_log_file').$file_ext;
        }
        $dir_url = trim(trim(trim($dir_url,'/'),'\\'));
        $database_url = trim(trim(trim($database_url,'/'),'\\'));
        $dir_file = (strlen($dir_url) <1) ? $database_url.'/'.$file_name : $dir_url.'/'.$database_url.'/'.$file_name;
        $this->_make_log_dir_and_write($dir_file,$content);
        return $this->__return_data(0,'ok',$database_url.'/'.$file_name);
    }
    
    
    /**
     * 创建文件路径
     * @param unknown $file_path 文件相对路径
     */
    public function _make_log_dir_and_write($file_path,$content='')
    {
        if(strlen($content)<1)
        {
            return $this->__return_data(1,'内容为空');
        }
        $file_path = trim(trim(trim($file_path,'/'),'\\'));
        if(strlen($file_path)<1)
        {
            return $this->__return_data(1,'文件路径为空');
        }
        $log_dir = dirname(dirname(__FILE__)).'/data/'.$file_path;
        $arr_pathinfo = pathinfo($log_dir);
        $dirname = isset($arr_pathinfo['dirname']) ? $arr_pathinfo['dirname'] : '';
        $dirname = trim(rtrim(rtrim($dirname,'/'),'\\'));
        if(strlen($dirname)<1)
        {
            return $this->__return_data(1,'文件路径为空');
        }
        if (!is_dir($dirname))
        {
            @mkdir($dirname, 0777, true);
        }
        if(!file_exists($dirname))
        {
            @mkdir($dirname, 0777, true);
        }
        @error_log($content, 3, $log_dir);
        return $this->__return_data(0,'OK');
    }
    
    /**
     * 执行curl
     * @param unknown $url
     * @param unknown $data
     * @return multitype:NULL unknown string
     */
    public function _execute_curl($send_url,$func='get',$content=null,$arr_header=null,$time=60)
    {
        $obj_curl = new np_http_curl_class();
        $curl_data = $obj_curl->do_request($func,$send_url,$content,$arr_header,60);
        $curl_info = $obj_curl->curl_getinfo();
        $http_code = $curl_info['http_code'];
        if($curl_info['http_code'] != '200')
        {
            return $this->__return_data(1,'[访问第三方接口,CURL接口地址]：'.$send_url.'[HTTP状态码]：'.$curl_info['http_code'].'[HTTP错误]：'.$obj_curl->curl_error(),$curl_data);
        }
        else
        {
            return $this->__return_data(0,'[访问第三方接口,CURL接口地址]：'.$send_url,$curl_data);
        }
    }
    
    
    /**
     * 组装基础dom对象类
     * @param string $key 键
     * @param string $val 值
     * @param array $arr_attr attr值
     * @param object $parent 父级对象
     * @return object
     * @date 2015-05-04
     */
    public function _make_xml($key, $val = null, $arr_attr = null,$parent=null)
    {
        if(is_null($parent))
        {
            $parent=$this->obj_dom;
        }
        $$key = isset($val) ? $this->obj_dom->createElement($key, $val) : $this->obj_dom->createElement($key);
        if (!empty($arr_attr) && is_array($arr_attr))
        {
            foreach ($arr_attr as $attr_key => $attr_val)
            {
                $domAttribute = $this->obj_dom->createAttribute($attr_key);
                $domAttribute->value = $attr_val;
                $$key->appendChild($domAttribute);
                $this->obj_dom->appendChild($$key);
            }
        }
        $parent->appendChild($$key);
        //unset($dom);
        unset($parent);
        return $$key;
    }
}