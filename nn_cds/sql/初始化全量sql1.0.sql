/*
SQLyog Ultimate v11.25 (64 bit)
MySQL - 5.5.53 : Database - nn_cds
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`nn_cds` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `nn_cds`;

/*Table structure for table `nns_actor` */

DROP TABLE IF EXISTS `nns_actor`;

CREATE TABLE `nns_actor` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'GUID',
  `nns_import_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '注入ID',
  `nns_import_source` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '注入来源',
  `nns_cp_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'CP ID',
  `nns_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT '明星名字',
  `nns_area` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '国家地区',
  `nns_alias_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '别名',
  `nns_profession` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '职业',
  `nns_pinyin` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '拼音',
  `nns_works` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '代表作',
  `nns_info` varchar(4096) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'JSON描述额外信息字符串',
  `nns_label_id` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '对应标签ID',
  `nns_image_v` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '明星图片竖',
  `nns_image_s` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '明星图片方',
  `nns_image_h` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '明星图片横',
  `nns_create_time` datetime NOT NULL COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL COMMENT '修改时间',
  `nns_old_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '曾用名',
  `nns_english_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '英文名',
  `nns_country` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '国籍',
  `nns_sex` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '性别',
  `nns_full_pinyin` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '名称全拼',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_actor_bind` */

DROP TABLE IF EXISTS `nns_actor_bind`;

CREATE TABLE `nns_actor_bind` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'GUID',
  `nns_bind_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '绑定的资源id',
  `nns_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '类型，0点播|1直播',
  `nns_source_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '绑定资源类型，0主媒资/直播频道|1分集',
  `nns_cp_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'CP ID',
  `nns_create_time` datetime NOT NULL COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_areacode` */

DROP TABLE IF EXISTS `nns_areacode`;

CREATE TABLE `nns_areacode` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '区域码',
  `nns_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT '区域名称',
  `nns_company_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '分公司id',
  `nns_company_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '分公司名称',
  `nns_city_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '地市id',
  `nns_city_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '地市名称',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_assest_bind_video_category` */

DROP TABLE IF EXISTS `nns_assest_bind_video_category`;

CREATE TABLE `nns_assest_bind_video_category` (
  `nns_assest_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_assest_category_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_package_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_category_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_category_name` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_assest_name` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_id` int(4) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_assists_item` */

DROP TABLE IF EXISTS `nns_assists_item`;

CREATE TABLE `nns_assists_item` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_name` char(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_img0` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_img1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_img2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_img3` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_img4` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_img5` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_category_id` char(64) COLLATE utf8_unicode_ci NOT NULL,
  `nns_assist_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_type` int(4) NOT NULL,
  `nns_order` int(4) NOT NULL DEFAULT '0',
  `nns_play_count` int(4) DEFAULT NULL,
  `nns_score` int(4) DEFAULT NULL,
  `nns_score_count` int(4) DEFAULT NULL,
  `nns_point` int(1) DEFAULT '0',
  `nns_check` int(4) DEFAULT '1',
  `nns_play_day_count` int(4) DEFAULT NULL,
  `nns_pinyin` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_pinyin_length` int(3) NOT NULL DEFAULT '0' COMMENT 'æ‹¼éŸ³å­—ç¬¦æ€»é•¿åº¦',
  `nns_tag` varchar(128) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `nns_single_tag` varchar(128) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_english_index` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'è‹±æ–‡æŸ¥è¯¢ç´¢å¼•',
  PRIMARY KEY (`nns_id`),
  KEY `index_order_assest` (`nns_order`),
  KEY `index_pinyin` (`nns_pinyin`),
  KEY `index_category` (`nns_category_id`,`nns_assist_id`),
  KEY `index_name` (`nns_video_name`),
  KEY `index_enname` (`nns_english_index`(255)),
  KEY `nns_video_id` (`nns_video_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_bigtv_bind` */

DROP TABLE IF EXISTS `nns_bigtv_bind`;

CREATE TABLE `nns_bigtv_bind` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_bk_channel_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '播控 频道ID',
  `nns_bk_channel_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '播控频道名称',
  `nns_bigtv_channel_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '大电视频道名称',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `idx_bk_bigtv` (`nns_bk_channel_id`,`nns_bigtv_channel_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_build_media_file` */

DROP TABLE IF EXISTS `nns_build_media_file`;

CREATE TABLE `nns_build_media_file` (
  `nns_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '物理文件GUID',
  `nns_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '物理文件名称',
  `nns_url` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_file_size` int(10) unsigned DEFAULT '0' COMMENT '文件大小',
  `nns_file_ex` varchar(12) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'ts' COMMENT '物理文件后缀名称',
  `nns_delete` tinyint(3) unsigned DEFAULT '0' COMMENT '删除状态 0 未删除 | 1 已删除',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_build_vod` */

DROP TABLE IF EXISTS `nns_build_vod`;

CREATE TABLE `nns_build_vod` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'GUID',
  `nns_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '生产平台主媒资名称',
  `nns_state` tinyint(4) unsigned DEFAULT '0' COMMENT '主媒资审核状态,0 待审核，1审核通过，2审核不通过',
  `nns_view_type` tinyint(4) unsigned DEFAULT '1' COMMENT '影片类型 1 点播',
  `nns_tag` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'tag标志位',
  `nns_director` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '导演',
  `nns_actor` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '演员',
  `nns_show_time` date DEFAULT NULL COMMENT '上映时间',
  `nns_view_len` int(4) unsigned DEFAULT '0' COMMENT '影片时长',
  `nns_all_index` mediumint(4) unsigned DEFAULT '0' COMMENT '分集总集数',
  `nns_new_index` mediumint(4) unsigned DEFAULT '0' COMMENT '最新集数',
  `nns_area` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '上映地区',
  `nns_image0` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '大图',
  `nns_image1` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '中图',
  `nns_image2` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '小图',
  `nns_image3` varchar(512) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_image4` varchar(512) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_image5` varchar(512) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_summary` varchar(4096) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '主媒资描述',
  `nns_remark` varchar(4096) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '标志',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `nns_deleted` tinyint(4) unsigned DEFAULT '0' COMMENT '是否删除 0 未删除| 1 已删除',
  `nns_category_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '栏目id',
  `nns_play_count` int(4) unsigned DEFAULT '0' COMMENT '播放次数',
  `nns_score_total` int(8) unsigned DEFAULT '0' COMMENT '评分',
  `nns_score_count` int(4) unsigned DEFAULT '0' COMMENT '总分数',
  `nns_point` int(1) unsigned DEFAULT '0' COMMENT '观看星级',
  `nns_copyright_date` date DEFAULT NULL COMMENT '版权日期',
  `nns_asset_import_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '主媒资注入id',
  `nns_pinyin` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '拼音',
  `nns_pinyin_length` smallint(3) unsigned NOT NULL COMMENT '拼音长度',
  `nns_alias_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '别名',
  `nns_eng_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '英文名称',
  `nns_language` varchar(16) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '语言',
  `nns_text_lang` varchar(16) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '字幕语言',
  `nns_producer` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '提供商',
  `nns_screenwriter` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '字幕作者',
  `nns_play_role` varchar(4096) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '播放角色',
  `nns_copyright_range` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '版权范围',
  `nns_vod_part` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_keyword` char(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '关键字',
  `nns_import_source` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '注入来源',
  `nns_kind` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '影片类型 如：战争,爱情',
  `nns_copyright` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '版权信息 如芒果TV',
  `nns_clarity` varchar(32) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_integer_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nns_image_v` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '宽图',
  `nns_image_s` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '横图',
  `nns_image_h` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '竖图',
  `nns_cp_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT 'CPID',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `nns_integer_id` (`nns_integer_id`),
  KEY `index_pinyin` (`nns_pinyin`),
  KEY `index_modify_time` (`nns_modify_time`),
  KEY `index_category` (`nns_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_build_vod_ex` */

DROP TABLE IF EXISTS `nns_build_vod_ex`;

CREATE TABLE `nns_build_vod_ex` (
  `nns_vod_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '主媒资扩展注入id',
  `nns_key` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`nns_vod_id`,`nns_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_build_vod_index` */

DROP TABLE IF EXISTS `nns_build_vod_index`;

CREATE TABLE `nns_build_vod_index` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '分集GUID',
  `nns_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '分集名称',
  `nns_vod_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '主媒资id',
  `nns_index` mediumint(4) unsigned DEFAULT '0' COMMENT ' 分集号 默认0开始 0为第一集',
  `nns_time_len` int(4) unsigned DEFAULT '0' COMMENT '分集时长',
  `nns_summary` varchar(4096) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '分集描述',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `nns_image` char(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '分集图片',
  `nns_play_count` int(4) unsigned DEFAULT '0' COMMENT '播放次数',
  `nns_score_total` int(8) unsigned DEFAULT '0' COMMENT '分数',
  `nns_score_count` int(4) unsigned DEFAULT '0' COMMENT '总分数',
  `nns_import_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '分集注入id',
  `nns_import_source` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '注入来源',
  `nns_actor` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '演员',
  `nns_director` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '导演',
  `nns_new_media` char(10) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_state` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '分集审核状态,0 待审核，1审核通过，2审核不通过',
  `nns_new_media_time` datetime DEFAULT NULL,
  `nns_deleted` tinyint(4) unsigned DEFAULT '0' COMMENT '是否删除 0 未删除| 1 已删除',
  `nns_release_time` datetime DEFAULT NULL COMMENT '上映时间',
  `nns_update_time` date DEFAULT NULL COMMENT '更新时间',
  `nns_integer_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nns_watch_focus` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '分集看点信息',
  `nns_cp_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `nns_integer_id` (`nns_integer_id`),
  KEY `index_vod_id` (`nns_vod_id`),
  KEY `index_vod_index` (`nns_index`),
  KEY `nns_vod_id_nns_index` (`nns_vod_id`,`nns_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_build_vod_index_ex` */

DROP TABLE IF EXISTS `nns_build_vod_index_ex`;

CREATE TABLE `nns_build_vod_index_ex` (
  `nns_vod_index_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '分集扩展注入id',
  `nns_key` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`nns_vod_index_id`,`nns_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_build_vod_media` */

DROP TABLE IF EXISTS `nns_build_vod_media`;

CREATE TABLE `nns_build_vod_media` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '片源名称',
  `nns_vod_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '主媒资id',
  `nns_vod_index_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '分集id',
  `nns_type` int(4) DEFAULT NULL,
  `nns_state` tinyint(4) unsigned DEFAULT '0' COMMENT '片源资审核状态,0 待审核，1审核通过，2审核不通过',
  `nns_url` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '片源地址',
  `nns_tag` char(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'tag标志位',
  `nns_mode` char(32) COLLATE utf8_unicode_ci DEFAULT 'std' COMMENT '清晰度 默认std 标清',
  `nns_kbps` int(4) unsigned DEFAULT '0' COMMENT '码率',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'Cid',
  `nns_content_state` int(4) unsigned DEFAULT '0' COMMENT 'Cid状态',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `nns_deleted` tinyint(4) unsigned DEFAULT '0' COMMENT '是否删除 0 未删除| 1 已删除',
  `nns_vod_index` int(4) unsigned DEFAULT '0' COMMENT '分集号',
  `nns_filetype` char(32) COLLATE utf8_unicode_ci DEFAULT 'ts' COMMENT '片源类型',
  `nns_play_count` int(4) unsigned DEFAULT '0' COMMENT '播放次数',
  `nns_score_total` int(8) unsigned DEFAULT '0' COMMENT '分数',
  `nns_score_count` int(4) unsigned DEFAULT '0' COMMENT '分数',
  `nns_media_caps` varchar(16) COLLATE utf8_unicode_ci DEFAULT 'VOD',
  `nns_import_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '片源注入id',
  `nns_import_source` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '注入来源',
  `nns_dimensions` char(8) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ӰƬά�ȣ�Ϊ��Ϊ 2D 0   3D 100001 100002 ',
  `nns_ext_url` text COLLATE utf8_unicode_ci,
  `nns_file_size` int(11) unsigned DEFAULT '0' COMMENT '文件大小',
  `nns_file_time_len` int(6) unsigned DEFAULT '0' COMMENT '片源时长',
  `nns_file_frame_rate` int(6) unsigned DEFAULT '0',
  `nns_file_resolution` varchar(32) COLLATE utf8_unicode_ci DEFAULT '0',
  `nns_integer_id` int(11) NOT NULL AUTO_INCREMENT,
  `nns_cp_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `nns_bind_file` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '片源绑定id',
  `nns_ext_info` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '片源切片扩展信息,json串',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `nns_integer_id` (`nns_integer_id`),
  KEY `index_vod_id_index` (`nns_vod_id`,`nns_vod_index_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_build_vod_media_ex` */

DROP TABLE IF EXISTS `nns_build_vod_media_ex`;

CREATE TABLE `nns_build_vod_media_ex` (
  `nns_vod_media_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_key` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`nns_vod_media_id`,`nns_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_carrier` */

DROP TABLE IF EXISTS `nns_carrier`;

CREATE TABLE `nns_carrier` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_desc` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_state` int(4) DEFAULT NULL,
  `nns_create_time` datetime DEFAULT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  `nns_contact` char(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_telephone` char(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_email` char(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_addr` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_category` */

DROP TABLE IF EXISTS `nns_category`;

CREATE TABLE `nns_category` (
  `nns_category_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '栏目id',
  `nns_cp_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'CPID',
  `nns_parent_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '父级栏目id',
  `nns_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '栏目名称',
  `nns_weight` mediumint(8) unsigned DEFAULT '0' COMMENT '权重',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_category_id`,`nns_cp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_category_backup` */

DROP TABLE IF EXISTS `nns_category_backup`;

CREATE TABLE `nns_category_backup` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_category_type` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_category` mediumtext COLLATE utf8_unicode_ci,
  `nns_org_type` int(2) DEFAULT NULL,
  `nns_org_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_category_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_backup_time` datetime DEFAULT NULL,
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_category_detail` */

DROP TABLE IF EXISTS `nns_category_detail`;

CREATE TABLE `nns_category_detail` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_category_list_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_parent_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_state` int(11) DEFAULT NULL,
  `nns_desc` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_create_time` datetime DEFAULT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_category_list` */

DROP TABLE IF EXISTS `nns_category_list`;

CREATE TABLE `nns_category_list` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_type` int(4) DEFAULT NULL,
  `nns_org_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_desc` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_create_time` datetime DEFAULT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  `nns_state` int(4) DEFAULT NULL,
  `nns_live` int(4) DEFAULT '1',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_cdn_live_index_queue` */

DROP TABLE IF EXISTS `nns_cdn_live_index_queue`;

CREATE TABLE `nns_cdn_live_index_queue` (
  `nns_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_live_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '直播GUID',
  `nns_live_index_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '直播分集GUID',
  `nns_action` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 增加 | 1 修改 | 2 删除',
  `nns_import_cdn_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 等待注入CDN |  1 正在注入CDN | 2 注入CDN 成功 | 3 注入CDN失败',
  `nns_import_cdn_time` mediumint(3) unsigned NOT NULL DEFAULT '0' COMMENT '注入CDN的重试次数',
  `nns_import_cms_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 等待注入EPG |  1 正在注入EPG | 2 注入EPG 成功 | 3 注入EPG失败',
  `nns_import_cms_time` mediumint(3) unsigned NOT NULL DEFAULT '0' COMMENT '注入CMS的重试次数',
  `nns_import_notify_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 等待响应 上游 |  1 正在响应上游 | 2 响应上游 成功 | 3 响应上游失败',
  `nns_import_notify_time` mediumint(3) unsigned NOT NULL DEFAULT '0' COMMENT '响应上游重发次数',
  `nns_message_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '上游注入的消息ID',
  `nns_cp_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'CPID',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  KEY `index_cp_action_index` (`nns_action`,`nns_cp_id`),
  KEY `index_cp_state_index` (`nns_cp_id`,`nns_import_cdn_state`),
  KEY `index_message_index` (`nns_message_id`),
  KEY `index_live_index` (`nns_live_id`),
  KEY `index_live_index_index` (`nns_live_index_id`),
  KEY `index_create_time_index` (`nns_create_time`),
  KEY `index_modify_time_index` (`nns_modify_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_cdn_live_index_queue_log` */

DROP TABLE IF EXISTS `nns_cdn_live_index_queue_log`;

CREATE TABLE `nns_cdn_live_index_queue_log` (
  `nns_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_cdn_queue_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息队列GUID',
  `nns_action` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 增加 | 1 修改 | 2 删除',
  `nns_cdn_send` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '注入CDN发送消息',
  `nns_cdn_notify` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '注入CDN响应消息',
  `nns_cms_send` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '发送CMS消息',
  `nns_cms_notify` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '发送CMS响应消息',
  `nns_firm_send` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '厂商发送数据',
  `nns_firm_notify` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '厂商响应数据',
  `nns_cp_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'CPID',
  `nns_create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_cdn_live_media_queue` */

DROP TABLE IF EXISTS `nns_cdn_live_media_queue`;

CREATE TABLE `nns_cdn_live_media_queue` (
  `nns_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_live_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '直播GUID',
  `nns_live_index_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '直播分集GUID',
  `nns_live_media_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '直播片源GUID',
  `nns_action` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 增加 | 1 修改 | 2 删除',
  `nns_import_cdn_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 等待注入CDN |  1 正在注入CDN | 2 注入CDN 成功 | 3 注入CDN失败',
  `nns_import_cdn_time` mediumint(3) unsigned NOT NULL DEFAULT '0' COMMENT '注入CDN的重试次数',
  `nns_import_cms_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 等待注入EPG |  1 正在注入EPG | 2 注入EPG 成功 | 3 注入EPG失败',
  `nns_import_cms_time` mediumint(3) unsigned NOT NULL DEFAULT '0' COMMENT '注入CMS的重试次数',
  `nns_import_notify_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 等待响应 上游 |  1 正在响应上游 | 2 响应上游 成功 | 3 响应上游失败',
  `nns_import_notify_time` mediumint(3) unsigned NOT NULL DEFAULT '0' COMMENT '响应上游重发次数',
  `nns_message_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '上游注入的消息ID',
  `nns_cp_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'CPID',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  KEY `index_cp_action_media` (`nns_action`,`nns_cp_id`),
  KEY `index_cp_state_media` (`nns_cp_id`,`nns_import_cdn_state`),
  KEY `index_message_media` (`nns_message_id`),
  KEY `index_live_media` (`nns_live_id`),
  KEY `index_live_index_media` (`nns_live_index_id`),
  KEY `index_live_media_media` (`nns_live_media_id`),
  KEY `index_create_time_media` (`nns_create_time`),
  KEY `index_modify_time_media` (`nns_modify_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_cdn_live_media_queue_log` */

DROP TABLE IF EXISTS `nns_cdn_live_media_queue_log`;

CREATE TABLE `nns_cdn_live_media_queue_log` (
  `nns_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_cdn_queue_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息队列GUID',
  `nns_action` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 增加 | 1 修改 | 2 删除',
  `nns_cdn_send` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '注入CDN发送消息',
  `nns_cdn_notify` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '注入CDN响应消息',
  `nns_cms_send` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '发送CMS消息',
  `nns_cms_notify` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '发送CMS响应消息',
  `nns_firm_send` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '厂商发送数据',
  `nns_firm_notify` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '厂商响应数据',
  `nns_cp_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'CPID',
  `nns_create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_cdn_live_queue` */

DROP TABLE IF EXISTS `nns_cdn_live_queue`;

CREATE TABLE `nns_cdn_live_queue` (
  `nns_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_live_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '直播ID',
  `nns_action` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 增加 | 1 修改 | 2 删除',
  `nns_import_cdn_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 等待注入CDN |  1 正在注入CDN | 2 注入CDN 成功 | 3 注入CDN失败',
  `nns_import_cdn_time` mediumint(3) unsigned NOT NULL DEFAULT '0' COMMENT '注入CDN的重试次数',
  `nns_import_cms_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 等待注入EPG |  1 正在注入EPG | 2 注入EPG 成功 | 3 注入EPG失败',
  `nns_import_cms_time` mediumint(3) unsigned NOT NULL DEFAULT '0' COMMENT '注入CMS的重试次数',
  `nns_import_notify_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 等待响应 上游 |  1 正在响应上游 | 2 响应上游 成功 | 3 响应上游失败',
  `nns_import_notify_time` mediumint(3) unsigned NOT NULL DEFAULT '0' COMMENT '响应上游重发次数',
  `nns_message_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '上游注入的消息ID',
  `nns_cp_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'CPID',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  KEY `index_cp_action` (`nns_action`,`nns_cp_id`),
  KEY `index_cp_state` (`nns_cp_id`,`nns_import_cdn_state`),
  KEY `index_message` (`nns_message_id`),
  KEY `index_live` (`nns_live_id`),
  KEY `index_create_time` (`nns_create_time`),
  KEY `index_modify_time` (`nns_modify_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_cdn_live_queue_log` */

DROP TABLE IF EXISTS `nns_cdn_live_queue_log`;

CREATE TABLE `nns_cdn_live_queue_log` (
  `nns_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_cdn_queue_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息队列GUID',
  `nns_action` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 增加 | 1 修改 | 2 删除',
  `nns_cdn_send` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '注入CDN发送消息',
  `nns_cdn_notify` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '注入CDN响应消息',
  `nns_cms_send` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '发送CMS消息',
  `nns_cms_notify` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '发送CMS响应消息',
  `nns_firm_send` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '厂商发送数据',
  `nns_firm_notify` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '厂商响应数据',
  `nns_cp_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'CPID',
  `nns_create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_cdn_playbill_queue` */

DROP TABLE IF EXISTS `nns_cdn_playbill_queue`;

CREATE TABLE `nns_cdn_playbill_queue` (
  `nns_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_live_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '直播GUID',
  `nns_live_index_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '直播分集GUID',
  `nns_live_media_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '直播片源GUID',
  `nns_playbill_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '节目单GUID',
  `nns_action` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 增加 | 1 修改 | 2 删除',
  `nns_import_cdn_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 等待注入CDN |  1 正在注入CDN | 2 注入CDN 成功 | 3 注入CDN失败',
  `nns_import_cdn_time` mediumint(3) unsigned NOT NULL DEFAULT '0' COMMENT '注入CDN的重试次数',
  `nns_import_cms_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 等待注入EPG |  1 正在注入EPG | 2 注入EPG 成功 | 3 注入EPG失败',
  `nns_import_cms_time` mediumint(3) unsigned NOT NULL DEFAULT '0' COMMENT '注入CMS的重试次数',
  `nns_import_notify_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 等待响应 上游 |  1 正在响应上游 | 2 响应上游 成功 | 3 响应上游失败',
  `nns_import_notify_time` mediumint(3) unsigned NOT NULL DEFAULT '0' COMMENT '响应上游重发次数',
  `nns_message_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '上游注入的消息ID',
  `nns_cp_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'CPID',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  KEY `index_cp_action_playbill` (`nns_action`,`nns_cp_id`),
  KEY `index_cp_state_playbill` (`nns_cp_id`,`nns_import_cdn_state`),
  KEY `index_message_playbill` (`nns_message_id`),
  KEY `index_live_playbill` (`nns_live_id`),
  KEY `index_live_index_playbill` (`nns_live_index_id`),
  KEY `index_live_media_playbill` (`nns_live_media_id`),
  KEY `index_playbill` (`nns_playbill_id`),
  KEY `index_create_time_playbill` (`nns_create_time`),
  KEY `index_modify_time_playbill` (`nns_modify_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_cdn_playbill_queue_log` */

DROP TABLE IF EXISTS `nns_cdn_playbill_queue_log`;

CREATE TABLE `nns_cdn_playbill_queue_log` (
  `nns_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_cdn_queue_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息队列GUID',
  `nns_action` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 增加 | 1 修改 | 2 删除',
  `nns_cdn_send` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '注入CDN发送消息',
  `nns_cdn_notify` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '注入CDN响应消息',
  `nns_cms_send` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '发送CMS消息',
  `nns_cms_notify` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '发送CMS响应消息',
  `nns_firm_send` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '厂商发送数据',
  `nns_firm_notify` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '厂商响应数据',
  `nns_cp_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'CPID',
  `nns_create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_cms_global` */

DROP TABLE IF EXISTS `nns_cms_global`;

CREATE TABLE `nns_cms_global` (
  `nns_id` char(32) NOT NULL,
  `nns_module` varchar(128) NOT NULL COMMENT '模块类型',
  `nns_key` varchar(128) NOT NULL COMMENT '参数键',
  `nns_value` varchar(4096) DEFAULT NULL COMMENT '参数值',
  PRIMARY KEY (`nns_id`),
  KEY `inde_module_key` (`nns_module`,`nns_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `nns_command` */

DROP TABLE IF EXISTS `nns_command`;

CREATE TABLE `nns_command` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__0` */

DROP TABLE IF EXISTS `nns_command__0`;

CREATE TABLE `nns_command__0` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__1` */

DROP TABLE IF EXISTS `nns_command__1`;

CREATE TABLE `nns_command__1` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__10` */

DROP TABLE IF EXISTS `nns_command__10`;

CREATE TABLE `nns_command__10` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__11` */

DROP TABLE IF EXISTS `nns_command__11`;

CREATE TABLE `nns_command__11` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__12` */

DROP TABLE IF EXISTS `nns_command__12`;

CREATE TABLE `nns_command__12` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__13` */

DROP TABLE IF EXISTS `nns_command__13`;

CREATE TABLE `nns_command__13` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__14` */

DROP TABLE IF EXISTS `nns_command__14`;

CREATE TABLE `nns_command__14` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__15` */

DROP TABLE IF EXISTS `nns_command__15`;

CREATE TABLE `nns_command__15` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__16` */

DROP TABLE IF EXISTS `nns_command__16`;

CREATE TABLE `nns_command__16` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__17` */

DROP TABLE IF EXISTS `nns_command__17`;

CREATE TABLE `nns_command__17` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__18` */

DROP TABLE IF EXISTS `nns_command__18`;

CREATE TABLE `nns_command__18` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__19` */

DROP TABLE IF EXISTS `nns_command__19`;

CREATE TABLE `nns_command__19` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__2` */

DROP TABLE IF EXISTS `nns_command__2`;

CREATE TABLE `nns_command__2` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__20` */

DROP TABLE IF EXISTS `nns_command__20`;

CREATE TABLE `nns_command__20` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__21` */

DROP TABLE IF EXISTS `nns_command__21`;

CREATE TABLE `nns_command__21` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__22` */

DROP TABLE IF EXISTS `nns_command__22`;

CREATE TABLE `nns_command__22` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__23` */

DROP TABLE IF EXISTS `nns_command__23`;

CREATE TABLE `nns_command__23` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__24` */

DROP TABLE IF EXISTS `nns_command__24`;

CREATE TABLE `nns_command__24` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__25` */

DROP TABLE IF EXISTS `nns_command__25`;

CREATE TABLE `nns_command__25` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__26` */

DROP TABLE IF EXISTS `nns_command__26`;

CREATE TABLE `nns_command__26` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__27` */

DROP TABLE IF EXISTS `nns_command__27`;

CREATE TABLE `nns_command__27` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__28` */

DROP TABLE IF EXISTS `nns_command__28`;

CREATE TABLE `nns_command__28` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__29` */

DROP TABLE IF EXISTS `nns_command__29`;

CREATE TABLE `nns_command__29` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__3` */

DROP TABLE IF EXISTS `nns_command__3`;

CREATE TABLE `nns_command__3` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__30` */

DROP TABLE IF EXISTS `nns_command__30`;

CREATE TABLE `nns_command__30` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__31` */

DROP TABLE IF EXISTS `nns_command__31`;

CREATE TABLE `nns_command__31` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__32` */

DROP TABLE IF EXISTS `nns_command__32`;

CREATE TABLE `nns_command__32` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__33` */

DROP TABLE IF EXISTS `nns_command__33`;

CREATE TABLE `nns_command__33` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__34` */

DROP TABLE IF EXISTS `nns_command__34`;

CREATE TABLE `nns_command__34` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__35` */

DROP TABLE IF EXISTS `nns_command__35`;

CREATE TABLE `nns_command__35` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__36` */

DROP TABLE IF EXISTS `nns_command__36`;

CREATE TABLE `nns_command__36` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__37` */

DROP TABLE IF EXISTS `nns_command__37`;

CREATE TABLE `nns_command__37` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__38` */

DROP TABLE IF EXISTS `nns_command__38`;

CREATE TABLE `nns_command__38` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__39` */

DROP TABLE IF EXISTS `nns_command__39`;

CREATE TABLE `nns_command__39` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__4` */

DROP TABLE IF EXISTS `nns_command__4`;

CREATE TABLE `nns_command__4` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__40` */

DROP TABLE IF EXISTS `nns_command__40`;

CREATE TABLE `nns_command__40` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__41` */

DROP TABLE IF EXISTS `nns_command__41`;

CREATE TABLE `nns_command__41` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__42` */

DROP TABLE IF EXISTS `nns_command__42`;

CREATE TABLE `nns_command__42` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__43` */

DROP TABLE IF EXISTS `nns_command__43`;

CREATE TABLE `nns_command__43` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__44` */

DROP TABLE IF EXISTS `nns_command__44`;

CREATE TABLE `nns_command__44` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__45` */

DROP TABLE IF EXISTS `nns_command__45`;

CREATE TABLE `nns_command__45` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__46` */

DROP TABLE IF EXISTS `nns_command__46`;

CREATE TABLE `nns_command__46` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__47` */

DROP TABLE IF EXISTS `nns_command__47`;

CREATE TABLE `nns_command__47` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__48` */

DROP TABLE IF EXISTS `nns_command__48`;

CREATE TABLE `nns_command__48` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__49` */

DROP TABLE IF EXISTS `nns_command__49`;

CREATE TABLE `nns_command__49` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__5` */

DROP TABLE IF EXISTS `nns_command__5`;

CREATE TABLE `nns_command__5` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__50` */

DROP TABLE IF EXISTS `nns_command__50`;

CREATE TABLE `nns_command__50` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__51` */

DROP TABLE IF EXISTS `nns_command__51`;

CREATE TABLE `nns_command__51` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__52` */

DROP TABLE IF EXISTS `nns_command__52`;

CREATE TABLE `nns_command__52` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__53` */

DROP TABLE IF EXISTS `nns_command__53`;

CREATE TABLE `nns_command__53` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__54` */

DROP TABLE IF EXISTS `nns_command__54`;

CREATE TABLE `nns_command__54` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__55` */

DROP TABLE IF EXISTS `nns_command__55`;

CREATE TABLE `nns_command__55` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__56` */

DROP TABLE IF EXISTS `nns_command__56`;

CREATE TABLE `nns_command__56` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__57` */

DROP TABLE IF EXISTS `nns_command__57`;

CREATE TABLE `nns_command__57` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__58` */

DROP TABLE IF EXISTS `nns_command__58`;

CREATE TABLE `nns_command__58` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__59` */

DROP TABLE IF EXISTS `nns_command__59`;

CREATE TABLE `nns_command__59` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__6` */

DROP TABLE IF EXISTS `nns_command__6`;

CREATE TABLE `nns_command__6` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__60` */

DROP TABLE IF EXISTS `nns_command__60`;

CREATE TABLE `nns_command__60` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__61` */

DROP TABLE IF EXISTS `nns_command__61`;

CREATE TABLE `nns_command__61` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__62` */

DROP TABLE IF EXISTS `nns_command__62`;

CREATE TABLE `nns_command__62` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__63` */

DROP TABLE IF EXISTS `nns_command__63`;

CREATE TABLE `nns_command__63` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__64` */

DROP TABLE IF EXISTS `nns_command__64`;

CREATE TABLE `nns_command__64` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__65` */

DROP TABLE IF EXISTS `nns_command__65`;

CREATE TABLE `nns_command__65` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__66` */

DROP TABLE IF EXISTS `nns_command__66`;

CREATE TABLE `nns_command__66` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__67` */

DROP TABLE IF EXISTS `nns_command__67`;

CREATE TABLE `nns_command__67` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__68` */

DROP TABLE IF EXISTS `nns_command__68`;

CREATE TABLE `nns_command__68` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__69` */

DROP TABLE IF EXISTS `nns_command__69`;

CREATE TABLE `nns_command__69` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__7` */

DROP TABLE IF EXISTS `nns_command__7`;

CREATE TABLE `nns_command__7` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__70` */

DROP TABLE IF EXISTS `nns_command__70`;

CREATE TABLE `nns_command__70` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__71` */

DROP TABLE IF EXISTS `nns_command__71`;

CREATE TABLE `nns_command__71` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__72` */

DROP TABLE IF EXISTS `nns_command__72`;

CREATE TABLE `nns_command__72` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__73` */

DROP TABLE IF EXISTS `nns_command__73`;

CREATE TABLE `nns_command__73` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__74` */

DROP TABLE IF EXISTS `nns_command__74`;

CREATE TABLE `nns_command__74` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__75` */

DROP TABLE IF EXISTS `nns_command__75`;

CREATE TABLE `nns_command__75` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__76` */

DROP TABLE IF EXISTS `nns_command__76`;

CREATE TABLE `nns_command__76` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__77` */

DROP TABLE IF EXISTS `nns_command__77`;

CREATE TABLE `nns_command__77` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__78` */

DROP TABLE IF EXISTS `nns_command__78`;

CREATE TABLE `nns_command__78` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__79` */

DROP TABLE IF EXISTS `nns_command__79`;

CREATE TABLE `nns_command__79` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__8` */

DROP TABLE IF EXISTS `nns_command__8`;

CREATE TABLE `nns_command__8` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__80` */

DROP TABLE IF EXISTS `nns_command__80`;

CREATE TABLE `nns_command__80` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__81` */

DROP TABLE IF EXISTS `nns_command__81`;

CREATE TABLE `nns_command__81` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__82` */

DROP TABLE IF EXISTS `nns_command__82`;

CREATE TABLE `nns_command__82` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__83` */

DROP TABLE IF EXISTS `nns_command__83`;

CREATE TABLE `nns_command__83` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__84` */

DROP TABLE IF EXISTS `nns_command__84`;

CREATE TABLE `nns_command__84` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__85` */

DROP TABLE IF EXISTS `nns_command__85`;

CREATE TABLE `nns_command__85` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__86` */

DROP TABLE IF EXISTS `nns_command__86`;

CREATE TABLE `nns_command__86` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__87` */

DROP TABLE IF EXISTS `nns_command__87`;

CREATE TABLE `nns_command__87` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__88` */

DROP TABLE IF EXISTS `nns_command__88`;

CREATE TABLE `nns_command__88` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__89` */

DROP TABLE IF EXISTS `nns_command__89`;

CREATE TABLE `nns_command__89` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__9` */

DROP TABLE IF EXISTS `nns_command__9`;

CREATE TABLE `nns_command__9` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__90` */

DROP TABLE IF EXISTS `nns_command__90`;

CREATE TABLE `nns_command__90` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__91` */

DROP TABLE IF EXISTS `nns_command__91`;

CREATE TABLE `nns_command__91` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__92` */

DROP TABLE IF EXISTS `nns_command__92`;

CREATE TABLE `nns_command__92` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__93` */

DROP TABLE IF EXISTS `nns_command__93`;

CREATE TABLE `nns_command__93` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__94` */

DROP TABLE IF EXISTS `nns_command__94`;

CREATE TABLE `nns_command__94` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__95` */

DROP TABLE IF EXISTS `nns_command__95`;

CREATE TABLE `nns_command__95` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__96` */

DROP TABLE IF EXISTS `nns_command__96`;

CREATE TABLE `nns_command__96` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__97` */

DROP TABLE IF EXISTS `nns_command__97`;

CREATE TABLE `nns_command__97` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__98` */

DROP TABLE IF EXISTS `nns_command__98`;

CREATE TABLE `nns_command__98` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command__99` */

DROP TABLE IF EXISTS `nns_command__99`;

CREATE TABLE `nns_command__99` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游消息唯一标识ID',
  `nns_source_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游来源ID(原CPID)',
  `nns_type` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容类型，video/index/media等',
  `nns_original_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容原始ID',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '内容ID,主媒资/分集/片源等',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_message_id`,`nns_source_id`,`nns_type`,`nns_original_id`,`nns_content_id`),
  KEY `message` (`nns_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task` */

DROP TABLE IF EXISTS `nns_command_task`;

CREATE TABLE `nns_command_task` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__0` */

DROP TABLE IF EXISTS `nns_command_task__0`;

CREATE TABLE `nns_command_task__0` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__1` */

DROP TABLE IF EXISTS `nns_command_task__1`;

CREATE TABLE `nns_command_task__1` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__10` */

DROP TABLE IF EXISTS `nns_command_task__10`;

CREATE TABLE `nns_command_task__10` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__11` */

DROP TABLE IF EXISTS `nns_command_task__11`;

CREATE TABLE `nns_command_task__11` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__12` */

DROP TABLE IF EXISTS `nns_command_task__12`;

CREATE TABLE `nns_command_task__12` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__13` */

DROP TABLE IF EXISTS `nns_command_task__13`;

CREATE TABLE `nns_command_task__13` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__14` */

DROP TABLE IF EXISTS `nns_command_task__14`;

CREATE TABLE `nns_command_task__14` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__15` */

DROP TABLE IF EXISTS `nns_command_task__15`;

CREATE TABLE `nns_command_task__15` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__16` */

DROP TABLE IF EXISTS `nns_command_task__16`;

CREATE TABLE `nns_command_task__16` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__17` */

DROP TABLE IF EXISTS `nns_command_task__17`;

CREATE TABLE `nns_command_task__17` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__18` */

DROP TABLE IF EXISTS `nns_command_task__18`;

CREATE TABLE `nns_command_task__18` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__19` */

DROP TABLE IF EXISTS `nns_command_task__19`;

CREATE TABLE `nns_command_task__19` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__2` */

DROP TABLE IF EXISTS `nns_command_task__2`;

CREATE TABLE `nns_command_task__2` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__20` */

DROP TABLE IF EXISTS `nns_command_task__20`;

CREATE TABLE `nns_command_task__20` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__21` */

DROP TABLE IF EXISTS `nns_command_task__21`;

CREATE TABLE `nns_command_task__21` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__22` */

DROP TABLE IF EXISTS `nns_command_task__22`;

CREATE TABLE `nns_command_task__22` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__23` */

DROP TABLE IF EXISTS `nns_command_task__23`;

CREATE TABLE `nns_command_task__23` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__24` */

DROP TABLE IF EXISTS `nns_command_task__24`;

CREATE TABLE `nns_command_task__24` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__25` */

DROP TABLE IF EXISTS `nns_command_task__25`;

CREATE TABLE `nns_command_task__25` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__26` */

DROP TABLE IF EXISTS `nns_command_task__26`;

CREATE TABLE `nns_command_task__26` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__27` */

DROP TABLE IF EXISTS `nns_command_task__27`;

CREATE TABLE `nns_command_task__27` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__28` */

DROP TABLE IF EXISTS `nns_command_task__28`;

CREATE TABLE `nns_command_task__28` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__29` */

DROP TABLE IF EXISTS `nns_command_task__29`;

CREATE TABLE `nns_command_task__29` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__3` */

DROP TABLE IF EXISTS `nns_command_task__3`;

CREATE TABLE `nns_command_task__3` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__30` */

DROP TABLE IF EXISTS `nns_command_task__30`;

CREATE TABLE `nns_command_task__30` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__31` */

DROP TABLE IF EXISTS `nns_command_task__31`;

CREATE TABLE `nns_command_task__31` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__32` */

DROP TABLE IF EXISTS `nns_command_task__32`;

CREATE TABLE `nns_command_task__32` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__33` */

DROP TABLE IF EXISTS `nns_command_task__33`;

CREATE TABLE `nns_command_task__33` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__34` */

DROP TABLE IF EXISTS `nns_command_task__34`;

CREATE TABLE `nns_command_task__34` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__35` */

DROP TABLE IF EXISTS `nns_command_task__35`;

CREATE TABLE `nns_command_task__35` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__36` */

DROP TABLE IF EXISTS `nns_command_task__36`;

CREATE TABLE `nns_command_task__36` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__37` */

DROP TABLE IF EXISTS `nns_command_task__37`;

CREATE TABLE `nns_command_task__37` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__38` */

DROP TABLE IF EXISTS `nns_command_task__38`;

CREATE TABLE `nns_command_task__38` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__39` */

DROP TABLE IF EXISTS `nns_command_task__39`;

CREATE TABLE `nns_command_task__39` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__4` */

DROP TABLE IF EXISTS `nns_command_task__4`;

CREATE TABLE `nns_command_task__4` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__40` */

DROP TABLE IF EXISTS `nns_command_task__40`;

CREATE TABLE `nns_command_task__40` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__41` */

DROP TABLE IF EXISTS `nns_command_task__41`;

CREATE TABLE `nns_command_task__41` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__42` */

DROP TABLE IF EXISTS `nns_command_task__42`;

CREATE TABLE `nns_command_task__42` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__43` */

DROP TABLE IF EXISTS `nns_command_task__43`;

CREATE TABLE `nns_command_task__43` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__44` */

DROP TABLE IF EXISTS `nns_command_task__44`;

CREATE TABLE `nns_command_task__44` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__45` */

DROP TABLE IF EXISTS `nns_command_task__45`;

CREATE TABLE `nns_command_task__45` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__46` */

DROP TABLE IF EXISTS `nns_command_task__46`;

CREATE TABLE `nns_command_task__46` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__47` */

DROP TABLE IF EXISTS `nns_command_task__47`;

CREATE TABLE `nns_command_task__47` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__48` */

DROP TABLE IF EXISTS `nns_command_task__48`;

CREATE TABLE `nns_command_task__48` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__49` */

DROP TABLE IF EXISTS `nns_command_task__49`;

CREATE TABLE `nns_command_task__49` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__5` */

DROP TABLE IF EXISTS `nns_command_task__5`;

CREATE TABLE `nns_command_task__5` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__50` */

DROP TABLE IF EXISTS `nns_command_task__50`;

CREATE TABLE `nns_command_task__50` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__51` */

DROP TABLE IF EXISTS `nns_command_task__51`;

CREATE TABLE `nns_command_task__51` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__52` */

DROP TABLE IF EXISTS `nns_command_task__52`;

CREATE TABLE `nns_command_task__52` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__53` */

DROP TABLE IF EXISTS `nns_command_task__53`;

CREATE TABLE `nns_command_task__53` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__54` */

DROP TABLE IF EXISTS `nns_command_task__54`;

CREATE TABLE `nns_command_task__54` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__55` */

DROP TABLE IF EXISTS `nns_command_task__55`;

CREATE TABLE `nns_command_task__55` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__56` */

DROP TABLE IF EXISTS `nns_command_task__56`;

CREATE TABLE `nns_command_task__56` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__57` */

DROP TABLE IF EXISTS `nns_command_task__57`;

CREATE TABLE `nns_command_task__57` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__58` */

DROP TABLE IF EXISTS `nns_command_task__58`;

CREATE TABLE `nns_command_task__58` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__59` */

DROP TABLE IF EXISTS `nns_command_task__59`;

CREATE TABLE `nns_command_task__59` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__6` */

DROP TABLE IF EXISTS `nns_command_task__6`;

CREATE TABLE `nns_command_task__6` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__60` */

DROP TABLE IF EXISTS `nns_command_task__60`;

CREATE TABLE `nns_command_task__60` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__61` */

DROP TABLE IF EXISTS `nns_command_task__61`;

CREATE TABLE `nns_command_task__61` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__62` */

DROP TABLE IF EXISTS `nns_command_task__62`;

CREATE TABLE `nns_command_task__62` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__63` */

DROP TABLE IF EXISTS `nns_command_task__63`;

CREATE TABLE `nns_command_task__63` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__64` */

DROP TABLE IF EXISTS `nns_command_task__64`;

CREATE TABLE `nns_command_task__64` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__65` */

DROP TABLE IF EXISTS `nns_command_task__65`;

CREATE TABLE `nns_command_task__65` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__66` */

DROP TABLE IF EXISTS `nns_command_task__66`;

CREATE TABLE `nns_command_task__66` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__67` */

DROP TABLE IF EXISTS `nns_command_task__67`;

CREATE TABLE `nns_command_task__67` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__68` */

DROP TABLE IF EXISTS `nns_command_task__68`;

CREATE TABLE `nns_command_task__68` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__69` */

DROP TABLE IF EXISTS `nns_command_task__69`;

CREATE TABLE `nns_command_task__69` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__7` */

DROP TABLE IF EXISTS `nns_command_task__7`;

CREATE TABLE `nns_command_task__7` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__70` */

DROP TABLE IF EXISTS `nns_command_task__70`;

CREATE TABLE `nns_command_task__70` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__71` */

DROP TABLE IF EXISTS `nns_command_task__71`;

CREATE TABLE `nns_command_task__71` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__72` */

DROP TABLE IF EXISTS `nns_command_task__72`;

CREATE TABLE `nns_command_task__72` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__73` */

DROP TABLE IF EXISTS `nns_command_task__73`;

CREATE TABLE `nns_command_task__73` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__74` */

DROP TABLE IF EXISTS `nns_command_task__74`;

CREATE TABLE `nns_command_task__74` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__75` */

DROP TABLE IF EXISTS `nns_command_task__75`;

CREATE TABLE `nns_command_task__75` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__76` */

DROP TABLE IF EXISTS `nns_command_task__76`;

CREATE TABLE `nns_command_task__76` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__77` */

DROP TABLE IF EXISTS `nns_command_task__77`;

CREATE TABLE `nns_command_task__77` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__78` */

DROP TABLE IF EXISTS `nns_command_task__78`;

CREATE TABLE `nns_command_task__78` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__79` */

DROP TABLE IF EXISTS `nns_command_task__79`;

CREATE TABLE `nns_command_task__79` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__8` */

DROP TABLE IF EXISTS `nns_command_task__8`;

CREATE TABLE `nns_command_task__8` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__80` */

DROP TABLE IF EXISTS `nns_command_task__80`;

CREATE TABLE `nns_command_task__80` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__81` */

DROP TABLE IF EXISTS `nns_command_task__81`;

CREATE TABLE `nns_command_task__81` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__82` */

DROP TABLE IF EXISTS `nns_command_task__82`;

CREATE TABLE `nns_command_task__82` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__83` */

DROP TABLE IF EXISTS `nns_command_task__83`;

CREATE TABLE `nns_command_task__83` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__84` */

DROP TABLE IF EXISTS `nns_command_task__84`;

CREATE TABLE `nns_command_task__84` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__85` */

DROP TABLE IF EXISTS `nns_command_task__85`;

CREATE TABLE `nns_command_task__85` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__86` */

DROP TABLE IF EXISTS `nns_command_task__86`;

CREATE TABLE `nns_command_task__86` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__87` */

DROP TABLE IF EXISTS `nns_command_task__87`;

CREATE TABLE `nns_command_task__87` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__88` */

DROP TABLE IF EXISTS `nns_command_task__88`;

CREATE TABLE `nns_command_task__88` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__89` */

DROP TABLE IF EXISTS `nns_command_task__89`;

CREATE TABLE `nns_command_task__89` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__9` */

DROP TABLE IF EXISTS `nns_command_task__9`;

CREATE TABLE `nns_command_task__9` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__90` */

DROP TABLE IF EXISTS `nns_command_task__90`;

CREATE TABLE `nns_command_task__90` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__91` */

DROP TABLE IF EXISTS `nns_command_task__91`;

CREATE TABLE `nns_command_task__91` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__92` */

DROP TABLE IF EXISTS `nns_command_task__92`;

CREATE TABLE `nns_command_task__92` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__93` */

DROP TABLE IF EXISTS `nns_command_task__93`;

CREATE TABLE `nns_command_task__93` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__94` */

DROP TABLE IF EXISTS `nns_command_task__94`;

CREATE TABLE `nns_command_task__94` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__95` */

DROP TABLE IF EXISTS `nns_command_task__95`;

CREATE TABLE `nns_command_task__95` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__96` */

DROP TABLE IF EXISTS `nns_command_task__96`;

CREATE TABLE `nns_command_task__96` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__97` */

DROP TABLE IF EXISTS `nns_command_task__97`;

CREATE TABLE `nns_command_task__97` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__98` */

DROP TABLE IF EXISTS `nns_command_task__98`;

CREATE TABLE `nns_command_task__98` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task__99` */

DROP TABLE IF EXISTS `nns_command_task__99`;

CREATE TABLE `nns_command_task__99` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息池ID',
  `nns_sp_id` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令行为动作',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '指令状态',
  `nns_mtime` bigint(16) NOT NULL DEFAULT '0' COMMENT '指令操作毫秒级时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `primary_key` (`nns_command_id`,`nns_sp_id`,`nns_action`),
  KEY `command_id` (`nns_command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log` */

DROP TABLE IF EXISTS `nns_command_task_log`;

CREATE TABLE `nns_command_task_log` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__0` */

DROP TABLE IF EXISTS `nns_command_task_log__0`;

CREATE TABLE `nns_command_task_log__0` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__1` */

DROP TABLE IF EXISTS `nns_command_task_log__1`;

CREATE TABLE `nns_command_task_log__1` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__10` */

DROP TABLE IF EXISTS `nns_command_task_log__10`;

CREATE TABLE `nns_command_task_log__10` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__11` */

DROP TABLE IF EXISTS `nns_command_task_log__11`;

CREATE TABLE `nns_command_task_log__11` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__12` */

DROP TABLE IF EXISTS `nns_command_task_log__12`;

CREATE TABLE `nns_command_task_log__12` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__13` */

DROP TABLE IF EXISTS `nns_command_task_log__13`;

CREATE TABLE `nns_command_task_log__13` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__14` */

DROP TABLE IF EXISTS `nns_command_task_log__14`;

CREATE TABLE `nns_command_task_log__14` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__15` */

DROP TABLE IF EXISTS `nns_command_task_log__15`;

CREATE TABLE `nns_command_task_log__15` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__16` */

DROP TABLE IF EXISTS `nns_command_task_log__16`;

CREATE TABLE `nns_command_task_log__16` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__17` */

DROP TABLE IF EXISTS `nns_command_task_log__17`;

CREATE TABLE `nns_command_task_log__17` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__18` */

DROP TABLE IF EXISTS `nns_command_task_log__18`;

CREATE TABLE `nns_command_task_log__18` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__19` */

DROP TABLE IF EXISTS `nns_command_task_log__19`;

CREATE TABLE `nns_command_task_log__19` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__2` */

DROP TABLE IF EXISTS `nns_command_task_log__2`;

CREATE TABLE `nns_command_task_log__2` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__20` */

DROP TABLE IF EXISTS `nns_command_task_log__20`;

CREATE TABLE `nns_command_task_log__20` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__21` */

DROP TABLE IF EXISTS `nns_command_task_log__21`;

CREATE TABLE `nns_command_task_log__21` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__22` */

DROP TABLE IF EXISTS `nns_command_task_log__22`;

CREATE TABLE `nns_command_task_log__22` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__23` */

DROP TABLE IF EXISTS `nns_command_task_log__23`;

CREATE TABLE `nns_command_task_log__23` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__24` */

DROP TABLE IF EXISTS `nns_command_task_log__24`;

CREATE TABLE `nns_command_task_log__24` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__25` */

DROP TABLE IF EXISTS `nns_command_task_log__25`;

CREATE TABLE `nns_command_task_log__25` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__26` */

DROP TABLE IF EXISTS `nns_command_task_log__26`;

CREATE TABLE `nns_command_task_log__26` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__27` */

DROP TABLE IF EXISTS `nns_command_task_log__27`;

CREATE TABLE `nns_command_task_log__27` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__28` */

DROP TABLE IF EXISTS `nns_command_task_log__28`;

CREATE TABLE `nns_command_task_log__28` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__29` */

DROP TABLE IF EXISTS `nns_command_task_log__29`;

CREATE TABLE `nns_command_task_log__29` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__3` */

DROP TABLE IF EXISTS `nns_command_task_log__3`;

CREATE TABLE `nns_command_task_log__3` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__30` */

DROP TABLE IF EXISTS `nns_command_task_log__30`;

CREATE TABLE `nns_command_task_log__30` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__31` */

DROP TABLE IF EXISTS `nns_command_task_log__31`;

CREATE TABLE `nns_command_task_log__31` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__32` */

DROP TABLE IF EXISTS `nns_command_task_log__32`;

CREATE TABLE `nns_command_task_log__32` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__33` */

DROP TABLE IF EXISTS `nns_command_task_log__33`;

CREATE TABLE `nns_command_task_log__33` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__34` */

DROP TABLE IF EXISTS `nns_command_task_log__34`;

CREATE TABLE `nns_command_task_log__34` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__35` */

DROP TABLE IF EXISTS `nns_command_task_log__35`;

CREATE TABLE `nns_command_task_log__35` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__36` */

DROP TABLE IF EXISTS `nns_command_task_log__36`;

CREATE TABLE `nns_command_task_log__36` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__37` */

DROP TABLE IF EXISTS `nns_command_task_log__37`;

CREATE TABLE `nns_command_task_log__37` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__38` */

DROP TABLE IF EXISTS `nns_command_task_log__38`;

CREATE TABLE `nns_command_task_log__38` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__39` */

DROP TABLE IF EXISTS `nns_command_task_log__39`;

CREATE TABLE `nns_command_task_log__39` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__4` */

DROP TABLE IF EXISTS `nns_command_task_log__4`;

CREATE TABLE `nns_command_task_log__4` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__40` */

DROP TABLE IF EXISTS `nns_command_task_log__40`;

CREATE TABLE `nns_command_task_log__40` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__41` */

DROP TABLE IF EXISTS `nns_command_task_log__41`;

CREATE TABLE `nns_command_task_log__41` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__42` */

DROP TABLE IF EXISTS `nns_command_task_log__42`;

CREATE TABLE `nns_command_task_log__42` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__43` */

DROP TABLE IF EXISTS `nns_command_task_log__43`;

CREATE TABLE `nns_command_task_log__43` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__44` */

DROP TABLE IF EXISTS `nns_command_task_log__44`;

CREATE TABLE `nns_command_task_log__44` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__45` */

DROP TABLE IF EXISTS `nns_command_task_log__45`;

CREATE TABLE `nns_command_task_log__45` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__46` */

DROP TABLE IF EXISTS `nns_command_task_log__46`;

CREATE TABLE `nns_command_task_log__46` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__47` */

DROP TABLE IF EXISTS `nns_command_task_log__47`;

CREATE TABLE `nns_command_task_log__47` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__48` */

DROP TABLE IF EXISTS `nns_command_task_log__48`;

CREATE TABLE `nns_command_task_log__48` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__49` */

DROP TABLE IF EXISTS `nns_command_task_log__49`;

CREATE TABLE `nns_command_task_log__49` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__5` */

DROP TABLE IF EXISTS `nns_command_task_log__5`;

CREATE TABLE `nns_command_task_log__5` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__50` */

DROP TABLE IF EXISTS `nns_command_task_log__50`;

CREATE TABLE `nns_command_task_log__50` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__51` */

DROP TABLE IF EXISTS `nns_command_task_log__51`;

CREATE TABLE `nns_command_task_log__51` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__52` */

DROP TABLE IF EXISTS `nns_command_task_log__52`;

CREATE TABLE `nns_command_task_log__52` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__53` */

DROP TABLE IF EXISTS `nns_command_task_log__53`;

CREATE TABLE `nns_command_task_log__53` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__54` */

DROP TABLE IF EXISTS `nns_command_task_log__54`;

CREATE TABLE `nns_command_task_log__54` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__55` */

DROP TABLE IF EXISTS `nns_command_task_log__55`;

CREATE TABLE `nns_command_task_log__55` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__56` */

DROP TABLE IF EXISTS `nns_command_task_log__56`;

CREATE TABLE `nns_command_task_log__56` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__57` */

DROP TABLE IF EXISTS `nns_command_task_log__57`;

CREATE TABLE `nns_command_task_log__57` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__58` */

DROP TABLE IF EXISTS `nns_command_task_log__58`;

CREATE TABLE `nns_command_task_log__58` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__59` */

DROP TABLE IF EXISTS `nns_command_task_log__59`;

CREATE TABLE `nns_command_task_log__59` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__6` */

DROP TABLE IF EXISTS `nns_command_task_log__6`;

CREATE TABLE `nns_command_task_log__6` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__60` */

DROP TABLE IF EXISTS `nns_command_task_log__60`;

CREATE TABLE `nns_command_task_log__60` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__61` */

DROP TABLE IF EXISTS `nns_command_task_log__61`;

CREATE TABLE `nns_command_task_log__61` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__62` */

DROP TABLE IF EXISTS `nns_command_task_log__62`;

CREATE TABLE `nns_command_task_log__62` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__63` */

DROP TABLE IF EXISTS `nns_command_task_log__63`;

CREATE TABLE `nns_command_task_log__63` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__64` */

DROP TABLE IF EXISTS `nns_command_task_log__64`;

CREATE TABLE `nns_command_task_log__64` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__65` */

DROP TABLE IF EXISTS `nns_command_task_log__65`;

CREATE TABLE `nns_command_task_log__65` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__66` */

DROP TABLE IF EXISTS `nns_command_task_log__66`;

CREATE TABLE `nns_command_task_log__66` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__67` */

DROP TABLE IF EXISTS `nns_command_task_log__67`;

CREATE TABLE `nns_command_task_log__67` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__68` */

DROP TABLE IF EXISTS `nns_command_task_log__68`;

CREATE TABLE `nns_command_task_log__68` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__69` */

DROP TABLE IF EXISTS `nns_command_task_log__69`;

CREATE TABLE `nns_command_task_log__69` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__7` */

DROP TABLE IF EXISTS `nns_command_task_log__7`;

CREATE TABLE `nns_command_task_log__7` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__70` */

DROP TABLE IF EXISTS `nns_command_task_log__70`;

CREATE TABLE `nns_command_task_log__70` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__71` */

DROP TABLE IF EXISTS `nns_command_task_log__71`;

CREATE TABLE `nns_command_task_log__71` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__72` */

DROP TABLE IF EXISTS `nns_command_task_log__72`;

CREATE TABLE `nns_command_task_log__72` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__73` */

DROP TABLE IF EXISTS `nns_command_task_log__73`;

CREATE TABLE `nns_command_task_log__73` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__74` */

DROP TABLE IF EXISTS `nns_command_task_log__74`;

CREATE TABLE `nns_command_task_log__74` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__75` */

DROP TABLE IF EXISTS `nns_command_task_log__75`;

CREATE TABLE `nns_command_task_log__75` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__76` */

DROP TABLE IF EXISTS `nns_command_task_log__76`;

CREATE TABLE `nns_command_task_log__76` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__77` */

DROP TABLE IF EXISTS `nns_command_task_log__77`;

CREATE TABLE `nns_command_task_log__77` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__78` */

DROP TABLE IF EXISTS `nns_command_task_log__78`;

CREATE TABLE `nns_command_task_log__78` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__79` */

DROP TABLE IF EXISTS `nns_command_task_log__79`;

CREATE TABLE `nns_command_task_log__79` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__8` */

DROP TABLE IF EXISTS `nns_command_task_log__8`;

CREATE TABLE `nns_command_task_log__8` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__80` */

DROP TABLE IF EXISTS `nns_command_task_log__80`;

CREATE TABLE `nns_command_task_log__80` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__81` */

DROP TABLE IF EXISTS `nns_command_task_log__81`;

CREATE TABLE `nns_command_task_log__81` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__82` */

DROP TABLE IF EXISTS `nns_command_task_log__82`;

CREATE TABLE `nns_command_task_log__82` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__83` */

DROP TABLE IF EXISTS `nns_command_task_log__83`;

CREATE TABLE `nns_command_task_log__83` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__84` */

DROP TABLE IF EXISTS `nns_command_task_log__84`;

CREATE TABLE `nns_command_task_log__84` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__85` */

DROP TABLE IF EXISTS `nns_command_task_log__85`;

CREATE TABLE `nns_command_task_log__85` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__86` */

DROP TABLE IF EXISTS `nns_command_task_log__86`;

CREATE TABLE `nns_command_task_log__86` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__87` */

DROP TABLE IF EXISTS `nns_command_task_log__87`;

CREATE TABLE `nns_command_task_log__87` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__88` */

DROP TABLE IF EXISTS `nns_command_task_log__88`;

CREATE TABLE `nns_command_task_log__88` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__89` */

DROP TABLE IF EXISTS `nns_command_task_log__89`;

CREATE TABLE `nns_command_task_log__89` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__9` */

DROP TABLE IF EXISTS `nns_command_task_log__9`;

CREATE TABLE `nns_command_task_log__9` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__90` */

DROP TABLE IF EXISTS `nns_command_task_log__90`;

CREATE TABLE `nns_command_task_log__90` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__91` */

DROP TABLE IF EXISTS `nns_command_task_log__91`;

CREATE TABLE `nns_command_task_log__91` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__92` */

DROP TABLE IF EXISTS `nns_command_task_log__92`;

CREATE TABLE `nns_command_task_log__92` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__93` */

DROP TABLE IF EXISTS `nns_command_task_log__93`;

CREATE TABLE `nns_command_task_log__93` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__94` */

DROP TABLE IF EXISTS `nns_command_task_log__94`;

CREATE TABLE `nns_command_task_log__94` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__95` */

DROP TABLE IF EXISTS `nns_command_task_log__95`;

CREATE TABLE `nns_command_task_log__95` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__96` */

DROP TABLE IF EXISTS `nns_command_task_log__96`;

CREATE TABLE `nns_command_task_log__96` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__97` */

DROP TABLE IF EXISTS `nns_command_task_log__97`;

CREATE TABLE `nns_command_task_log__97` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__98` */

DROP TABLE IF EXISTS `nns_command_task_log__98`;

CREATE TABLE `nns_command_task_log__98` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_command_task_log__99` */

DROP TABLE IF EXISTS `nns_command_task_log__99`;

CREATE TABLE `nns_command_task_log__99` (
  `nns_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自动增量',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '指令队列ID',
  `nns_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态码',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `nns_execute_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '日志执行路径',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_cp` */

DROP TABLE IF EXISTS `nns_cp`;

CREATE TABLE `nns_cp` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_guid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `nns_ex_name` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'CP名称 以后拿来生成文件mgtv_v2 文件名用的，不在使用nns_id了',
  `nns_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'CP名称',
  `nns_contactor` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '联系人',
  `nns_telephone` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '电话',
  `nns_email` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Email',
  `nns_addr` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '邮箱',
  `nns_desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '描述',
  `nns_state` tinyint(3) unsigned DEFAULT '0' COMMENT '使用状态  0  开启 | 1 关闭 ',
  `nns_config` text COLLATE utf8_unicode_ci,
  `nns_file_state_enable` tinyint(3) unsigned DEFAULT '0' COMMENT '上游注入的文件包资源库栏目是否展示 0 不展示| 1展示 ',
  `nns_create_time` datetime NOT NULL COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL COMMENT '修改时间',
  `nns_type` tinyint(3) unsigned DEFAULT '0' COMMENT '上游接收的数据的CP 类型  0 CP/SP厂商 | 1 融合厂商',
  `nns_cms_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '与MSP平台的标示ID cms ID',
  PRIMARY KEY (`nns_guid`),
  UNIQUE KEY `nns_id` (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

/*Table structure for table `nns_del_temp` */

DROP TABLE IF EXISTS `nns_del_temp`;

CREATE TABLE `nns_del_temp` (
  `nns_id` char(32) NOT NULL,
  `nns_org_id` char(32) NOT NULL,
  `nns_name` text,
  PRIMARY KEY (`nns_id`,`nns_org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_depot` */

DROP TABLE IF EXISTS `nns_depot`;

CREATE TABLE `nns_depot` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_category` mediumtext COLLATE utf8_unicode_ci,
  `nns_org_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_org_type` int(4) NOT NULL,
  `nns_type` int(4) NOT NULL DEFAULT '0' COMMENT '标识点直播',
  `nns_create_time` datetime NOT NULL,
  `nns_modify_time` datetime NOT NULL,
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_depot_v2` */

DROP TABLE IF EXISTS `nns_depot_v2`;

CREATE TABLE `nns_depot_v2` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '记录ID',
  `nns_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '媒资库栏目名称',
  `nns_category_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT '媒资库栏目ID',
  `nns_parent_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '上一级父栏目',
  `nns_import_category_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '第三方栏目注入id',
  `nns_import_parent_category_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '第三方父级栏目注入id',
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'CP ID',
  `nns_depot_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '媒资库ID',
  `nns_org_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '媒资来源ID',
  `nns_order` int(11) NOT NULL DEFAULT '0' COMMENT '栏目权重(降序)',
  `nns_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '点/直播标志位',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `index_depot_v2_category_type` (`nns_category_id`,`nns_type`),
  KEY `index_depot_v2_parent_id` (`nns_parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_epg_file` */

DROP TABLE IF EXISTS `nns_epg_file`;

CREATE TABLE `nns_epg_file` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_integer_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `nns_epg_file_set_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_import_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '注入ID',
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'CP ID',
  `nns_url` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '文件地址',
  `nns_deleted` tinyint(2) DEFAULT '0' COMMENT '是否删除,0 否,1 是',
  `nns_dest_path` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '厂商在EPG Sever 上存储该文件的相对路径。如果是相对路径的根路径，则用空表示。如：epg/bestv/',
  `nns_dest_file` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '厂商在EPG Sever 上存储该文件的目标文件名，如：index.php',
  `nns_md5` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'MD5验证字符串',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `nns_integer_id` (`nns_integer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000000001 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='file表';

/*Table structure for table `nns_epg_file_set` */

DROP TABLE IF EXISTS `nns_epg_file_set`;

CREATE TABLE `nns_epg_file_set` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_integer_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `nns_import_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '注入ID',
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'CP ID',
  `nns_group` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '分组',
  `nns_deleted` tinyint(2) DEFAULT '0' COMMENT '是否删除,0 否,1 是',
  `nns_system_file` tinyint(2) DEFAULT '0' COMMENT '该文件集是否为系统层文件,0 否,1 是',
  `nns_tar` tinyint(2) DEFAULT '0' COMMENT '文件是否为tar包,0 否,1 是',
  `nns_show_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '文件生效时间',
  `nns_epg_addr` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '目标分发的epg地址,多个逗号分开',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `nns_integer_id` (`nns_integer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='fileset表';

/*Table structure for table `nns_file_encode` */

DROP TABLE IF EXISTS `nns_file_encode`;

CREATE TABLE `nns_file_encode` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_sp_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'SPID',
  `nns_cp_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'CPID',
  `nns_op_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'OP QUEUE UUID',
  `nns_in_media_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '原始片源ID',
  `nns_queue_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '队列名称',
  `nns_out_media_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '转码后的片源ID',
  `nns_file_url` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '转码后的片源和抽帧图片的URL 地址  json存储',
  `nns_model_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '转码模板ID',
  `nns_type` tinyint(3) unsigned DEFAULT '0' COMMENT '转码类型  0 点播转码 |  1 直播转码 | 2 回看转点播 ',
  `nns_priority` smallint(5) unsigned DEFAULT '0' COMMENT '队列优先级',
  `nns_media_type` tinyint(3) unsigned DEFAULT '0' COMMENT '原始片源类型  0 点播 | 1 直播',
  `nns_state` tinyint(3) unsigned DEFAULT '0' COMMENT '转码状态  0 等待转码  |  1 正在转码  |  2 转码失败 |  3 转码成功  |  4 暂停转码 |  5 失效队列',
  `nns_integer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nns_again` tinyint(3) unsigned DEFAULT '0' COMMENT '执行失败的次数',
  `nns_message_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '消息ID',
  `nns_loadbalance_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '负载均衡ID',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `idx_unique` (`nns_integer_id`),
  UNIQUE KEY `idx_sp_media_id` (`nns_sp_id`,`nns_out_media_id`) USING BTREE,
  KEY `idx_state` (`nns_state`),
  KEY `idx_create_time` (`nns_create_time`),
  KEY `idx_op_id` (`nns_op_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_file_encode_log` */

DROP TABLE IF EXISTS `nns_file_encode_log`;

CREATE TABLE `nns_file_encode_log` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_file_encode_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_request_state` tinyint(3) unsigned DEFAULT '0' COMMENT '请求状态 0 成功 | 1 失败',
  `nns_request_content` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '请求内容',
  `nns_notify_state` tinyint(3) unsigned DEFAULT '0' COMMENT '响应状态 0 成功 | 1 失败',
  `nns_notify_content` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '响应内容',
  `nns_desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '描述',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  KEY `idx_file_encode_id` (`nns_file_encode_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_file_handle_queue` */

DROP TABLE IF EXISTS `nns_file_handle_queue`;

CREATE TABLE `nns_file_handle_queue` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'UUID',
  `nns_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '片源名称',
  `nns_action` varchar(12) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '文件的处理方式，目前只有删除',
  `nns_media_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '片源id',
  `nns_message_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '消息id',
  `ns_sp_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'SP_ID',
  `nns_state` tinyint(3) unsigned DEFAULT '0' COMMENT '队列状态',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_file_package` */

DROP TABLE IF EXISTS `nns_file_package`;

CREATE TABLE `nns_file_package` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '文件GUID ',
  `nns_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '描述',
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'CP id ',
  `nns_import_id` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '注入ID',
  `nns_import_source` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '内容提供商',
  `nns_deleted` tinyint(3) unsigned DEFAULT '0' COMMENT '状态 0  未删除| 1 已删除',
  `nns_file_url` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '文件url地址',
  `nns_service_url` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '文件服务地址',
  `nns_file_size` int(10) unsigned DEFAULT '0' COMMENT '文件大小|单位 byte',
  `nns_domain` varchar(8) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'domain域',
  `nns_hot_degree` smallint(5) unsigned DEFAULT '0' COMMENT '热度权重',
  `nns_integer_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增标示',
  `nns_deliver_time` datetime DEFAULT NULL COMMENT '内容分发时间',
  `nns_ext_url` varchar(2048) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '上游方注入url 和其他信息  json存储',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `index_auto` (`nns_integer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_full_assets` */

DROP TABLE IF EXISTS `nns_full_assets`;

CREATE TABLE `nns_full_assets` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'CP_ID',
  `nns_asset_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '注入ID',
  `nns_asset_import_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '原始ID',
  `nns_asset_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `nns_content` longtext COLLATE utf8_unicode_ci,
  `nns_state` tinyint(3) unsigned DEFAULT '0' COMMENT '状态 0 未使用 | 1 未补全 | 2 已补全',
  `nns_execute_time` tinyint(3) unsigned DEFAULT '0' COMMENT '已执行次数',
  `nns_reason` text COLLATE utf8_unicode_ci COMMENT '原因描述',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `idx_asset_id` (`nns_asset_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_full_assets_site` */

DROP TABLE IF EXISTS `nns_full_assets_site`;

CREATE TABLE `nns_full_assets_site` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_siteid` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '站点ID',
  `nns_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '站点名称',
  `nns_state` tinyint(3) unsigned DEFAULT '0' COMMENT '状态 0  启用 | 1 禁用',
  `nns_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '图片URL',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `idx_siteid` (`nns_siteid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_fuse_cp` */

DROP TABLE IF EXISTS `nns_fuse_cp`;

CREATE TABLE `nns_fuse_cp` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_src_cp_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '源端CP ID',
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '转变后的CP ID 全局唯一',
  `nns_cp_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '转变后的CP 名称',
  `nns_state` tinyint(3) unsigned DEFAULT '1' COMMENT '0 有效 | 1 无效',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `idx_cp` (`nns_cp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_global_error_log` */

DROP TABLE IF EXISTS `nns_global_error_log`;

CREATE TABLE `nns_global_error_log` (
  `nns_id` char(32) NOT NULL DEFAULT '',
  `nns_name` varchar(255) DEFAULT NULL COMMENT '名称',
  `nns_desc` text COMMENT '错误描述',
  `nns_type` varchar(32) DEFAULT NULL COMMENT 'video 主媒资 | index 分集 | media 片源',
  `nns_action` varchar(32) DEFAULT NULL COMMENT 'add 添加 | modify 修改 | destroy 删除 | line 上线 | unline 下线',
  `nns_link_file` varchar(255) DEFAULT NULL COMMENT '文件日志连接路径',
  `nns_model` varchar(32) DEFAULT NULL COMMENT '日志模块',
  `nns_cp_id` varchar(32) DEFAULT NULL COMMENT 'CP id',
  `nns_sp_id` varchar(32) DEFAULT NULL COMMENT 'SP id',
  `nns_bind_id` varchar(32) DEFAULT NULL,
  `nns_message_id` varchar(32) DEFAULT NULL COMMENT '消息id',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_video_import_id` varchar(64) DEFAULT NULL COMMENT '主媒资注入id',
  `nns_index_import_id` varchar(64) DEFAULT NULL COMMENT '分集注入id',
  `nns_media_import_id` varchar(64) DEFAULT NULL COMMENT '片源注入id',
  PRIMARY KEY (`nns_id`),
  KEY `index_message` (`nns_message_id`),
  KEY `index_video_id` (`nns_video_import_id`),
  KEY `index_index_id` (`nns_index_import_id`),
  KEY `index_media_id` (`nns_media_import_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_group` */

DROP TABLE IF EXISTS `nns_group`;

CREATE TABLE `nns_group` (
  `nns_id` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_desc` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_state` int(4) DEFAULT NULL,
  `nns_create_time` datetime DEFAULT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  `nns_contact` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_telephone` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_email` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_addr` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_boss_type` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_boss_area_code` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_template_id` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_template_type` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_url_link` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_outside_link` char(1) COLLATE utf8_unicode_ci DEFAULT '0',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_import_asset_online` */

DROP TABLE IF EXISTS `nns_import_asset_online`;

CREATE TABLE `nns_import_asset_online` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'guid',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT '消息ID',
  `nns_video_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '影片名称',
  `nns_import_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '媒资注入id',
  `nns_category_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '媒资栏目',
  `nns_order` int(8) unsigned DEFAULT '0' COMMENT '主媒资序号',
  `nns_action` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT 'online 上线,unline 下线',
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT 'CP默认为0',
  `nns_org_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '运营商id',
  `nns_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '0 成功 | 1 等待注入 | 2 注入失败 | 3 等待反馈 | 4 反馈失败 | 5 主媒资不是最终状态 | 6 分集不是最终状态 | 7 片源不是最终状态',
  `nns_audit` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '1 审核通过，0审核不通过',
  `nns_create_time` datetime NOT NULL,
  `nns_modify_time` datetime NOT NULL,
  `nns_fail_time` int(11) unsigned NOT NULL DEFAULT '0',
  `nns_delete` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '0 未删除 | 1已删除',
  `nns_vod_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_type` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'video' COMMENT '上下线媒资类型',
  `nns_asset_id` char(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '媒资包id',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='播控媒资上下线分发表';

/*Table structure for table `nns_import_asset_online_log` */

DROP TABLE IF EXISTS `nns_import_asset_online_log`;

CREATE TABLE `nns_import_asset_online_log` (
  `nns_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'guid',
  `nns_video_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '影片名称',
  `nns_import_id` varchar(128) NOT NULL DEFAULT '' COMMENT '媒资注入id',
  `nns_category_id` varchar(64) NOT NULL DEFAULT '' COMMENT '媒资栏目',
  `nns_action` varchar(8) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'online 上线,unline 下线',
  `nns_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '结果描述',
  `nns_import_content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '注入XML内容',
  `nns_callback_content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '反馈的xml内容',
  `nns_org_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'sp id',
  `nns_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '0 下发成功 1下发失败 2 反馈成功 3 反馈失败',
  `nns_message_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '消息id',
  `nns_asset_online_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '上下线队列  guid',
  `nns_create_time` datetime NOT NULL COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL COMMENT '修改时间',
  `nns_vod_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nns_type` char(32) NOT NULL DEFAULT 'video' COMMENT '媒资类型',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_init_params_item` */

DROP TABLE IF EXISTS `nns_init_params_item`;

CREATE TABLE `nns_init_params_item` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_init_type` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `nns_packet_type` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `nns_packet_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_category_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_action_type` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `nns_group` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `nns_tag` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_category_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_order` int(2) NOT NULL COMMENT '自定义下行参数排序',
  PRIMARY KEY (`nns_id`),
  KEY `index_tag` (`nns_tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_label_type` */

DROP TABLE IF EXISTS `nns_label_type`;

CREATE TABLE `nns_label_type` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '标签类型id',
  `nns_type` char(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'video' COMMENT '标签类型种类',
  `nns_weight` int(4) NOT NULL COMMENT '标签类型权重',
  `nns_create_time` datetime NOT NULL,
  `nns_summary` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `nns_field` char(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '¶ÔÓ¦×ÊÔ´¿â×Ö¶Î£¬Îª¿ÕÎª²»¶ÔÓ¦×Ö¶Î',
  PRIMARY KEY (`nns_id`),
  KEY `nns_type` (`nns_type`,`nns_weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='分类标签类型表';

/*Table structure for table `nns_live` */

DROP TABLE IF EXISTS `nns_live`;

CREATE TABLE `nns_live` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '直播频道GUID',
  `nns_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '直播频道名称',
  `nns_state` tinyint(3) unsigned DEFAULT '0' COMMENT '直播频道状态',
  `nns_check` tinyint(3) unsigned DEFAULT '0' COMMENT '审核状态 0 已审核 | 1 未审核',
  `nns_view_type` tinyint(3) unsigned DEFAULT '1' COMMENT '直播频道类型',
  `nns_org_type` tinyint(4) DEFAULT NULL,
  `nns_org_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '运营商ID',
  `nns_tag` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '终端tag标示',
  `nns_director` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '导演',
  `nns_actor` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '演员',
  `nns_show_time` date DEFAULT NULL COMMENT '展示时间',
  `nns_view_len` int(10) unsigned DEFAULT '0' COMMENT '时长（秒）',
  `nns_all_index` tinyint(3) unsigned DEFAULT '0' COMMENT '总分集集数',
  `nns_new_index` tinyint(3) unsigned DEFAULT '0' COMMENT '最新分集 0开始',
  `nns_area` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '地区',
  `nns_image0` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '大图',
  `nns_image1` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '中图',
  `nns_image2` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '小图',
  `nns_image3` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '扩展1',
  `nns_image4` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '扩展2',
  `nns_image5` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '扩展3',
  `nns_summary` varchar(4096) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '频道描述',
  `nns_remark` varchar(4096) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '频道标示',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `nns_deleted` tinyint(3) unsigned DEFAULT '0' COMMENT '删除状态  0 未删除 | 1已删除',
  `nns_depot_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '栏目depot GUID',
  `nns_category_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '栏目ID',
  `nns_play_count` int(10) unsigned DEFAULT '0' COMMENT '总播放次数',
  `nns_score` tinyint(3) unsigned DEFAULT '0' COMMENT '评分数',
  `nns_score_count` int(10) unsigned DEFAULT '0' COMMENT '评分次数',
  `nns_point` tinyint(3) unsigned DEFAULT '0' COMMENT '推荐星级',
  `nns_copyright_date` date DEFAULT NULL COMMENT '版权日期',
  `nns_pinyin` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '拼音',
  `nns_pinyin_length` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '拼音长度',
  `nns_eng_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '英文名称',
  `nns_alias_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '直播别名',
  `nns_language` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '直播语种',
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '0',
  `nns_import_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '直播频道的注入id',
  `nns_integer_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增序列号',
  `nns_producer` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '内容提供商名称',
  `nns_import_source` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '注入来源',
  `nns_image_v` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '竖图',
  `nns_image_h` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '横图',
  `nns_image_s` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '方图',
  `nns_image_logo` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '播放器内logo',
  `nns_ext_info` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '扩展参数',
  `nns_image_t` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '频道内容截屏',
  `nns_ex_lang` varchar(2048) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '扩展多语言',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `nns_integer_id` (`nns_integer_id`),
  KEY `index_pinyin` (`nns_pinyin`),
  KEY `index_depot` (`nns_depot_id`,`nns_category_id`),
  KEY `index_modify_time` (`nns_modify_time`),
  KEY `index_org` (`nns_org_type`,`nns_org_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_live_bind_third_platform` */

DROP TABLE IF EXISTS `nns_live_bind_third_platform`;

CREATE TABLE `nns_live_bind_third_platform` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_live_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '本地频道ID',
  `nns_third_live_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '第三方频道ID',
  `nns_third_live_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '第三方频道名称',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `index_live_third_index` (`nns_live_id`,`nns_third_live_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_live_ex` */

DROP TABLE IF EXISTS `nns_live_ex`;

CREATE TABLE `nns_live_ex` (
  `nns_live_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_key` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_value` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_cp_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT 'cp_id',
  PRIMARY KEY (`nns_live_id`,`nns_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_live_index` */

DROP TABLE IF EXISTS `nns_live_index`;

CREATE TABLE `nns_live_index` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_live_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_index` int(4) DEFAULT NULL,
  `nns_time_len` int(4) DEFAULT NULL,
  `nns_summary` varchar(4096) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_create_time` datetime DEFAULT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  `nns_image` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_play_count` int(8) DEFAULT NULL,
  `nns_score` int(64) DEFAULT NULL,
  `nns_score_count` int(64) DEFAULT NULL,
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_integer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nns_import_source` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '注入来源',
  `nns_import_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '直播分集注入id',
  `nns_deleted` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '直播分集是否被删除',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `nns_integer_id` (`nns_integer_id`),
  KEY `index_live_id` (`nns_live_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_live_info_language` */

DROP TABLE IF EXISTS `nns_live_info_language`;

CREATE TABLE `nns_live_info_language` (
  `nns_live_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_language` char(8) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `nns_summary` varchar(4096) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_area` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_alias_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_live_language` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`nns_live_id`,`nns_language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_live_media` */

DROP TABLE IF EXISTS `nns_live_media`;

CREATE TABLE `nns_live_media` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_live_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_live_index_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_type` int(4) DEFAULT NULL,
  `nns_state` int(4) DEFAULT NULL,
  `nns_check` int(4) DEFAULT '1',
  `nns_url` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_tag` char(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_mode` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_kbps` int(4) DEFAULT NULL,
  `nns_content_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_content_state` int(4) DEFAULT NULL,
  `nns_create_time` datetime DEFAULT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  `nns_deleted` int(4) DEFAULT '0',
  `nns_live_index` int(4) DEFAULT NULL,
  `nns_filetype` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_play_count` int(4) DEFAULT NULL,
  `nns_score_total` int(8) DEFAULT NULL,
  `nns_score_count` int(4) DEFAULT NULL,
  `nns_media_caps` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '0',
  `nns_cast_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 默认老逻辑注入 | 1 UDP 注入 | 2 单播 注入',
  `nns_message_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_timeshift_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '时移标识,是否支持时移,1支持0不支持,默认不支持',
  `nns_timeshift_delay` int(11) NOT NULL DEFAULT '0' COMMENT '时移时长(单位分钟),在时移标志生效时,有效',
  `nns_storage_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '录播标识,1生效0不生效',
  `nns_storage_delay` int(11) NOT NULL DEFAULT '0' COMMENT '存储时长(单位小时),在录播标志生效时，有效',
  `nns_integer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nns_drm_enabled` tinyint(3) unsigned DEFAULT '0' COMMENT '是否DRM加密',
  `nns_drm_encrypt_solution` varchar(32) CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL DEFAULT '' COMMENT '加密方案',
  `nns_drm_ext_info` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'drm扩展信息（json存储）',
  `nns_import_source` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '注入来源',
  `nns_domain` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '上游注入的网络域',
  `nns_ext_url` varchar(2048) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '上游方注入url 和其他信息  json存储',
  `nns_media_service` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '片源服务类型',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `nns_integer_id` (`nns_integer_id`),
  KEY `index_live_index_id` (`nns_live_id`,`nns_live_index_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_live_media_ex` */

DROP TABLE IF EXISTS `nns_live_media_ex`;

CREATE TABLE `nns_live_media_ex` (
  `nns_live_media_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '直播片源GUID',
  `nns_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '键',
  `nns_value` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '值',
  PRIMARY KEY (`nns_live_media_id`,`nns_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_live_playbill` */

DROP TABLE IF EXISTS `nns_live_playbill`;

CREATE TABLE `nns_live_playbill` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_live_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_day` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_playbill` varchar(4096) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_create_time` datetime DEFAULT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`nns_id`),
  KEY `index_playbill_video_id` (`nns_live_id`),
  KEY `index_playbill_day` (`nns_day`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_live_playbill_item` */

DROP TABLE IF EXISTS `nns_live_playbill_item`;

CREATE TABLE `nns_live_playbill_item` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `nns_begin_time` datetime NOT NULL COMMENT '节目播出时间',
  `nns_time_len` int(4) NOT NULL COMMENT '节目时长（秒',
  `nns_live_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '直播ID',
  `nns_create_time` datetime NOT NULL,
  `nns_state` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '默认为0，0为有效，1为无效',
  `nns_summary` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_image1` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_image2` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_image3` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_image4` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_image5` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_image0` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_pinyin` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_pinyin_length` int(3) NOT NULL COMMENT '.......',
  `nns_eng_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_keyword` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_kind` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '节目类型:如 战争 综艺等',
  `nns_image6` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_actor` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '演员,以/分隔',
  `nns_director` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '作者,导演,以/分隔',
  `nns_live_media_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '直播片源id',
  `nns_playbill_import_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '节目单的注入ID',
  `nns_domain` varchar(12) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_hot_dgree` varchar(12) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_integer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `nns_import_source` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '注入来源',
  `nns_ext_url` varchar(2048) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '上游方注入url 和其他信息  json存储',
  `nns_image_v` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '竖图',
  `nns_image_h` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '横图',
  `nns_image_s` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '方图',
  `nns_end_time` datetime DEFAULT NULL COMMENT '节目结束时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `nns_integer_id` (`nns_integer_id`),
  UNIQUE KEY `playbill_item_index_import_id_cp_id_import_source` (`nns_playbill_import_id`,`nns_cp_id`,`nns_import_source`),
  KEY `index_live_id` (`nns_live_id`),
  KEY `index_begin_time` (`nns_begin_time`,`nns_live_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_live_playbill_item_language` */

DROP TABLE IF EXISTS `nns_live_playbill_item_language`;

CREATE TABLE `nns_live_playbill_item_language` (
  `nns_playbill_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_language` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `nns_summary` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`nns_playbill_id`,`nns_language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_manager` */

DROP TABLE IF EXISTS `nns_manager`;

CREATE TABLE `nns_manager` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_password` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_org_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_state` int(4) DEFAULT NULL,
  `nns_role_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_create_time` datetime DEFAULT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  `nns_login_time` datetime DEFAULT NULL,
  `nns_login_count` int(4) DEFAULT '0',
  `nns_type` int(4) DEFAULT NULL,
  `nns_platform` varchar(12) COLLATE utf8_unicode_ci DEFAULT 'import' COMMENT '登录平台',
  `nns_cp_id` mediumtext COLLATE utf8_unicode_ci COMMENT 'CPID,分隔',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_media_asset_import_log` */

DROP TABLE IF EXISTS `nns_media_asset_import_log`;

CREATE TABLE `nns_media_asset_import_log` (
  `nns_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nns_import_video_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_import_type` char(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_create_time` datetime DEFAULT NULL,
  `nns_error_desc` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `nns_error_code` char(8) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'img ',
  `nns_state` char(1) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '...0......0....',
  `nns_video_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_modify_time` datetime NOT NULL,
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_media_asset_top` */

DROP TABLE IF EXISTS `nns_media_asset_top`;

CREATE TABLE `nns_media_asset_top` (
  `nns_id` int(8) NOT NULL AUTO_INCREMENT,
  `nns_video_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_type` int(2) DEFAULT NULL,
  `nns_asset_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_asset_category_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_order` int(4) DEFAULT NULL,
  `nns_top_type` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'total为总排名，week周排名，day日排名，month月排名',
  `nns_top_method` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_play_count` int(4) DEFAULT NULL,
  PRIMARY KEY (`nns_id`),
  KEY `index_media_top_order` (`nns_order`),
  KEY `index_top_type` (`nns_top_type`,`nns_top_method`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_medium_library_asset` */

DROP TABLE IF EXISTS `nns_medium_library_asset`;

CREATE TABLE `nns_medium_library_asset` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '主键，唯一标识',
  `nns_cp_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'CP ID',
  `nns_disk_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '磁盘ID',
  `nns_asset_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '分类名称',
  `nns_asset_path` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '分类路径名称',
  `nns_asset_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '分类类型。0单集；1多集',
  `nns_asset_summary` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '分类描述',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `nns_state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态。0启用；1禁用',
  PRIMARY KEY (`nns_id`),
  KEY `index_library_asset_disk_cp` (`nns_disk_id`,`nns_cp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_medium_library_cp` */

DROP TABLE IF EXISTS `nns_medium_library_cp`;

CREATE TABLE `nns_medium_library_cp` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '主键，唯一标识',
  `nns_cp_name` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'CP名称',
  `nns_contactor` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '联系人',
  `nns_telephone` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '电话',
  `nns_email` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Email',
  `nns_addr` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '地址',
  `nns_cp_summary` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'CP描述',
  `nns_image_h` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '横海报',
  `nns_image_v` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '竖海报',
  `nns_image_s` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '方海报',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `nns_state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态。0启用；1禁用',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_medium_library_disk` */

DROP TABLE IF EXISTS `nns_medium_library_disk`;

CREATE TABLE `nns_medium_library_disk` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '主键，唯一标识',
  `nns_server_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '服务器地址',
  `nns_disk_name` varchar(125) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '磁盘名称',
  `nns_disk_path` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT '磁盘地址',
  `nns_disk_extension` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '磁盘支持文件扩展名',
  `nns_summary` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '磁盘描述',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `nns_state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态。0启用；1禁用',
  PRIMARY KEY (`nns_id`),
  KEY `index_library_disk_server_id` (`nns_server_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_medium_library_file` */

DROP TABLE IF EXISTS `nns_medium_library_file`;

CREATE TABLE `nns_medium_library_file` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '主键，唯一标识',
  `nns_cp_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'CP ID',
  `nns_disk_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '磁盘ID',
  `nns_asset_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '文件分类ID',
  `nns_file_index` int(11) NOT NULL DEFAULT '0' COMMENT '文件集数',
  `nns_file_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '文件名称',
  `nns_file_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '文件物理路径',
  `nns_file_size` int(11) NOT NULL DEFAULT '0' COMMENT '文件大小，字节',
  `nns_file_type` char(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '文件格式',
  `nns_file_kbps_mode` char(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '文件码率模式',
  `nns_file_kbps` int(4) NOT NULL DEFAULT '0' COMMENT '文件码率（视频文件）',
  `nns_file_length` int(11) NOT NULL DEFAULT '0' COMMENT '播放时长（视频文件）',
  `nns_file_coding` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '视频编码（视频文件）',
  `nns_ext_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '文件额外地址',
  `nns_base_info` text COLLATE utf8_unicode_ci NOT NULL COMMENT '文件元数据',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `nns_state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态。0启用；1禁用',
  `nns_is_analysis` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否解析。0未解析；1解析成功；2解析失败',
  PRIMARY KEY (`nns_id`),
  KEY `index_library_file_disk_cp` (`nns_disk_id`,`nns_cp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_medium_library_server` */

DROP TABLE IF EXISTS `nns_medium_library_server`;

CREATE TABLE `nns_medium_library_server` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '主键，唯一标识',
  `nns_server_ip` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '服务器IP',
  `nns_server_name` varchar(125) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '服务器名称',
  `nns_server_path` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT '服务器地址',
  `nns_server_extension` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '服务器支持文件扩展名',
  `nns_server_summary` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '服务器描述',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `nns_state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态。0启用；1禁用',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_message_group` */

DROP TABLE IF EXISTS `nns_message_group`;

CREATE TABLE `nns_message_group` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_guid` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID  同message表 GUID一致',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '接收的消息ID',
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_sp_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'SP ID',
  `nns_state` tinyint(3) unsigned zerofill DEFAULT '000' COMMENT '消息包状态  0 等待消息反馈 | 1 消息反馈成功 | 2 消息反馈失败 ',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  KEY `index_message_guid` (`nns_message_guid`),
  KEY `index_create_time` (`nns_create_time`) USING BTREE,
  KEY `index_modify_time` (`nns_modify_time`) USING BTREE,
  KEY `index_sp_cp_message` (`nns_sp_id`,`nns_cp_id`,`nns_message_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_message_group_list` */

DROP TABLE IF EXISTS `nns_message_group_list`;

CREATE TABLE `nns_message_group_list` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_group_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_type` varchar(12) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '媒资类型',
  `nns_import_id` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '注入ID',
  `nns_video_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '各种媒资的  GUID',
  `nns_state` tinyint(3) unsigned DEFAULT '0' COMMENT '状态 0 还在队列中 | 1 成功 | 2 失败',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '描述',
  `nns_ext_url` varchar(2048) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '上游方注入url 和其他信息  json存储',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  KEY `index_message_id` (`nns_message_group_id`) USING BTREE,
  KEY `index_video_id` (`nns_video_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_message_temp` */

DROP TABLE IF EXISTS `nns_message_temp`;

CREATE TABLE `nns_message_temp` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'CPID',
  `nns_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'URL地址',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`,`nns_cp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_mgtvbk_c2_log` */

DROP TABLE IF EXISTS `nns_mgtvbk_c2_log`;

CREATE TABLE `nns_mgtvbk_c2_log` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_type` char(64) COLLATE utf8_unicode_ci NOT NULL,
  `nns_org_id` char(64) COLLATE utf8_unicode_ci NOT NULL,
  `nns_task_type` char(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_task_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_task_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_action` char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nns_content` text COLLATE utf8_unicode_ci NOT NULL,
  `nns_result` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nns_notify_result_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_notify_result` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_notify_time` datetime DEFAULT NULL,
  `nns_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `nns_create_time` datetime NOT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  `nns_again` int(11) DEFAULT '0',
  `nns_notify_content` text COLLATE utf8_unicode_ci,
  `nns_send_time` datetime DEFAULT NULL,
  `nns_ex_info` text COLLATE utf8_unicode_ci,
  `nns_cdn_send_time` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'CDN发送次数',
  `nns_notify_fail_reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '失败原因',
  `nns_ex_group` text COLLATE utf8_unicode_ci COMMENT '扩展分组消息（json存储）',
  PRIMARY KEY (`nns_id`),
  KEY `nns_type` (`nns_type`),
  KEY `nns_again` (`nns_again`),
  KEY `nns_org_id` (`nns_org_id`),
  KEY `nns_task_id` (`nns_task_id`),
  KEY `nns_task_type` (`nns_task_type`),
  KEY `nns_action` (`nns_action`),
  KEY `nns_notify_result` (`nns_notify_result`),
  KEY `nns_create_time` (`nns_create_time`),
  KEY `nns_task_name` (`nns_task_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='c2æŒ‡ä»¤æ‰§è¡Œè®°å½•è¡¨';

/*Table structure for table `nns_mgtvbk_c2_task` */

DROP TABLE IF EXISTS `nns_mgtvbk_c2_task`;

CREATE TABLE `nns_mgtvbk_c2_task` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_type` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nns_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_ref_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT '电影对应电影的ID，分集对应分集ID，直播对应节目单ID',
  `nns_cdn_import_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'cdn注入id',
  `nns_action` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'add，modify，destroy',
  `nns_status` int(4) NOT NULL COMMENT '0(1:已添加，2:已修改,3:已删，4:未知错误)',
  `nns_create_time` datetime NOT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  `nns_org_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_category_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_src_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_all_index` int(4) DEFAULT NULL,
  `nns_clip_task_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_clip_date` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_op_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_epg_status` int(4) DEFAULT '97',
  `nns_ex_url` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_file_path` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_file_size` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_file_md5` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_cdn_policy` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_epg_fail_time` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'EPG注入失败次数',
  `nns_cdn_fail_time` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'CDN失败次数',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '消息id',
  `nns_is_group` tinyint(3) unsigned DEFAULT '0' COMMENT '是否是分组  默认0  0 不是分组消息 | 1是分组消息',
  `nns_weight` smallint(5) unsigned DEFAULT '0' COMMENT '权重',
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'nns_cp_id',
  `nns_queue_execute_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '执行注入的本地文本日志',
  `nns_notify_third_state` tinyint(3) unsigned DEFAULT '0' COMMENT '消息同步注入第三方CMS平台标示  0 原始状态 | 1 成功 | 2 失败',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `idx_c2_task` (`nns_type`,`nns_ref_id`,`nns_org_id`,`nns_src_id`),
  KEY `nns_status` (`nns_status`),
  KEY `nns_create_time` (`nns_create_time`),
  KEY `nns_org_id` (`nns_org_id`),
  KEY `nns_op_id` (`nns_op_id`),
  KEY `nns_name` (`nns_name`),
  KEY `nns_epg_status` (`nns_epg_status`),
  KEY `idx_task` (`nns_org_id`,`nns_type`,`nns_action`),
  KEY `idx_src_id` (`nns_src_id`) USING BTREE,
  KEY `mgtvbk_c2_task_time` (`nns_org_id`,`nns_type`,`nns_modify_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='C2任务表';

/*Table structure for table `nns_mgtvbk_category` */

DROP TABLE IF EXISTS `nns_mgtvbk_category`;

CREATE TABLE `nns_mgtvbk_category` (
  `nns_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ä¸»é”® ä¸€çº§æ ç›®å››ä½æ•°å­—è¡¨ç¤ºï¼šå¦‚1000 äºŒçº§æ ç›®æ·»åŠ 3ä½æ•°å­—ï¼Œå¦‚ï¼š 1000123ï¼Œ',
  `nns_sp_category_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'å¯¹åº”spæ ç›®id',
  `nns_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'æ ç›®åç§°',
  `nns_create_time` datetime NOT NULL,
  `nns_modify_time` datetime NOT NULL,
  `nns_org_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_sort` smallint(6) DEFAULT NULL,
  `bind_vod_item` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_type` int(4) DEFAULT '0',
  UNIQUE KEY `nns_id_nns_sp_category_id_nns_video_type_nns_org_id` (`nns_id`,`nns_sp_category_id`,`nns_video_type`,`nns_org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_mgtvbk_category_content` */

DROP TABLE IF EXISTS `nns_mgtvbk_category_content`;

CREATE TABLE `nns_mgtvbk_category_content` (
  `nns_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `nns_org_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'sp id',
  `nns_category_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT '栏目id',
  `nns_category_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT '栏目name',
  `nns_video_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT '视频id',
  `nns_video_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '视频名称',
  `nns_create_time` datetime NOT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  `nns_all_index` int(4) DEFAULT NULL,
  `nns_pinyin` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'pinyin',
  `nns_video_type` int(4) DEFAULT '0',
  `nns_status` int(4) DEFAULT '0',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `nns_org_id_nns_category_id_nns_video_id_nns_video_type` (`nns_org_id`,`nns_category_id`,`nns_video_id`,`nns_video_type`),
  KEY `nns_category_id` (`nns_category_id`),
  KEY `nns_org_id` (`nns_org_id`),
  KEY `nns_create_time` (`nns_create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='栏目绑定内容表';

/*Table structure for table `nns_mgtvbk_clip_servicers` */

DROP TABLE IF EXISTS `nns_mgtvbk_clip_servicers`;

CREATE TABLE `nns_mgtvbk_clip_servicers` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_tom3u8_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '服务器ID',
  `nns_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '服务器名称',
  `nns_ip` varchar(16) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '服务器ip',
  `nns_create_time` datetime NOT NULL COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL COMMENT '修改时间',
  `nns_desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '服务器描述',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_mgtvbk_clip_task` */

DROP TABLE IF EXISTS `nns_mgtvbk_clip_task`;

CREATE TABLE `nns_mgtvbk_clip_task` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_task_type` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `nns_org_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_type` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_index_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_content` text COLLATE utf8_unicode_ci,
  `nns_state` char(30) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '默认为空，表示未被处理 ok：执行成功 handle：任务已下发（被切片工具取走） download:下载影片 cliping：正在切片 cancel:任务取消 clip_fail: 切片失败 clip_exists:切片存在 unkown:未知错误',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'çŠ¶æ€æè¿°',
  `nns_priority` smallint(4) unsigned DEFAULT '0',
  `nns_alive_time` datetime DEFAULT NULL COMMENT 'ä¸ŠæŠ¥æ—¶é—´',
  `nns_start_time` datetime DEFAULT NULL COMMENT 'ä»»åŠ¡å¼€å§‹æ—¶é—´',
  `nns_end_time` datetime DEFAULT NULL COMMENT 'ä»»åŠ¡ç»“æŸæ—¶é—´',
  `nns_create_time` datetime NOT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  `nns_force` char(10) COLLATE utf8_unicode_ci DEFAULT '0',
  `nns_date` varchar(64) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_new_dir` int(4) DEFAULT '0',
  `nns_tom3u8_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '服务器ID',
  `nns_video_media_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_task_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_op_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_file_hit` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'nns_cp_id',
  PRIMARY KEY (`nns_id`),
  KEY `nns_org_id` (`nns_org_id`),
  KEY `nns_vod_id_nns_vod_index_id` (`nns_video_id`,`nns_video_index_id`),
  KEY `nns_state` (`nns_state`),
  KEY `ids_p_date` (`nns_priority`,`nns_create_time`),
  KEY `nns_task_name` (`nns_task_name`),
  KEY `nns_video_index_id` (`nns_video_index_id`),
  KEY `nns_op_id` (`nns_op_id`),
  KEY `nns_video_media_id` (`nns_video_media_id`),
  KEY `nns_file_hit` (`nns_file_hit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ä»»åŠ¡è¡¨';

/*Table structure for table `nns_mgtvbk_clip_task_bak` */

DROP TABLE IF EXISTS `nns_mgtvbk_clip_task_bak`;

CREATE TABLE `nns_mgtvbk_clip_task_bak` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_task_type` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `nns_org_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_type` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_index_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_content` text COLLATE utf8_unicode_ci,
  `nns_state` char(30) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '默认为空，表示未被处理 ok：执行成功 handle：任务已下发（被切片工具取走） download:下载影片 cliping：正在切片 cancel:任务取消 clip_fail: 切片失败 clip_exists:切片存在 unkown:未知错误',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'çŠ¶æ€æè¿°',
  `nns_priority` smallint(4) DEFAULT '0',
  `nns_alive_time` datetime DEFAULT NULL COMMENT 'ä¸ŠæŠ¥æ—¶é—´',
  `nns_start_time` datetime DEFAULT NULL COMMENT 'ä»»åŠ¡å¼€å§‹æ—¶é—´',
  `nns_end_time` datetime DEFAULT NULL COMMENT 'ä»»åŠ¡ç»“æŸæ—¶é—´',
  `nns_create_time` datetime NOT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  `nns_force` char(10) COLLATE utf8_unicode_ci DEFAULT '0',
  `nns_date` varchar(64) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_new_dir` int(4) DEFAULT '0',
  PRIMARY KEY (`nns_id`),
  KEY `nns_org_id` (`nns_org_id`),
  KEY `nns_vod_id_nns_vod_index_id` (`nns_video_id`,`nns_video_index_id`),
  KEY `nns_state` (`nns_state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ä»»åŠ¡è¡¨';

/*Table structure for table `nns_mgtvbk_clip_task_log` */

DROP TABLE IF EXISTS `nns_mgtvbk_clip_task_log`;

CREATE TABLE `nns_mgtvbk_clip_task_log` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ä»»åŠ¡id',
  `nns_state` char(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ä¸ŠæŠ¥çŠ¶æ€',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ä¸ŠæŠ¥çŠ¶æ€æè¿°',
  `nns_create_time` datetime NOT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  `nns_video_type` char(2) COLLATE utf8_unicode_ci DEFAULT '0',
  `nns_video_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_index_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_org_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_create_mic_time` double(15,0) unsigned DEFAULT '0',
  PRIMARY KEY (`nns_id`),
  KEY `nns_task_id` (`nns_task_id`),
  KEY `nns_video_index_id` (`nns_video_index_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ä»»åŠ¡çŠ¶æ€ä¸ŠæŠ¥è®°å½•è¡¨';

/*Table structure for table `nns_mgtvbk_file_log` */

DROP TABLE IF EXISTS `nns_mgtvbk_file_log`;

CREATE TABLE `nns_mgtvbk_file_log` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `nns_org_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `nns_task_type` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_task_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_task_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_action` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nns_content` text COLLATE utf8_unicode_ci NOT NULL,
  `nns_result` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nns_notify_result_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_notify_result` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_notify_time` datetime DEFAULT NULL,
  `nns_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `nns_create_time` datetime NOT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  `nns_again` int(11) DEFAULT '0',
  `nns_notify_content` text COLLATE utf8_unicode_ci,
  `nns_send_time` datetime DEFAULT NULL,
  `nns_ex_info` text COLLATE utf8_unicode_ci,
  `nns_cdn_send_time` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'CDN发送次数',
  `nns_notify_fail_reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '失败原因',
  `nns_ex_group` text COLLATE utf8_unicode_ci COMMENT '扩展分组消息（json存储）',
  PRIMARY KEY (`nns_id`),
  KEY `nns_type` (`nns_type`),
  KEY `nns_again` (`nns_again`),
  KEY `nns_org_id` (`nns_org_id`),
  KEY `nns_task_id` (`nns_task_id`),
  KEY `nns_task_type` (`nns_task_type`),
  KEY `nns_action` (`nns_action`),
  KEY `nns_notify_result` (`nns_notify_result`),
  KEY `nns_create_time` (`nns_create_time`),
  KEY `nns_task_name` (`nns_task_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='file注入cdn日志表';

/*Table structure for table `nns_mgtvbk_file_task` */

DROP TABLE IF EXISTS `nns_mgtvbk_file_task`;

CREATE TABLE `nns_mgtvbk_file_task` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_type` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nns_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称或者组别',
  `nns_ref_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT '电影对应电影的ID，分集对应分集ID，直播对应节目单ID',
  `nns_cdn_import_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'cdn注入id',
  `nns_action` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'add，modify，destroy',
  `nns_status` int(4) NOT NULL COMMENT '注入cdn状态,0 注入成功,-1 注入失败,1 等待注入,5 正在注入,7 取消注入',
  `nns_create_time` datetime NOT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  `nns_org_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_category_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_src_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_all_index` int(4) DEFAULT NULL,
  `nns_clip_task_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '切片id',
  `nns_clip_date` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '切片日期',
  `nns_op_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '中心同步指令id',
  `nns_epg_status` int(4) DEFAULT NULL COMMENT '注入epg状态,96 注入失败,97 等待注入,98 正在注入,99 注入成功,100 取消注入',
  `nns_ex_url` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_file_path` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_file_size` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_file_md5` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_cdn_policy` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_epg_fail_time` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'EPG注入失败次数',
  `nns_cdn_fail_time` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'CDN失败次数',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '消息id',
  `nns_is_group` tinyint(3) unsigned DEFAULT '0' COMMENT '是否是分组  默认0  0 不是分组消息 | 1是分组消息',
  `nns_weight` smallint(5) unsigned DEFAULT '0' COMMENT '权重',
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'nns_cp_id',
  `nns_queue_execute_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '执行注入的本地文本日志',
  `nns_notify_third_state` tinyint(3) unsigned DEFAULT '0' COMMENT '消息同步注入第三方CMS平台标示  0 原始状态 | 1 成功 | 2 失败',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `idx_file_task` (`nns_type`,`nns_ref_id`,`nns_org_id`,`nns_src_id`),
  KEY `nns_status` (`nns_status`),
  KEY `nns_create_time` (`nns_create_time`),
  KEY `nns_org_id` (`nns_org_id`),
  KEY `nns_op_id` (`nns_op_id`),
  KEY `nns_name` (`nns_name`),
  KEY `nns_epg_status` (`nns_epg_status`),
  KEY `idx_task` (`nns_org_id`,`nns_type`,`nns_action`),
  KEY `idx_src_id` (`nns_src_id`) USING BTREE,
  KEY `mgtvbk_c2_task_time` (`nns_org_id`,`nns_type`,`nns_modify_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='file_task表';

/*Table structure for table `nns_mgtvbk_global` */

DROP TABLE IF EXISTS `nns_mgtvbk_global`;

CREATE TABLE `nns_mgtvbk_global` (
  `nns_id` varchar(32) NOT NULL,
  `nns_key` varchar(64) NOT NULL,
  `nns_value` varchar(256) NOT NULL,
  `nns_type` varchar(32) NOT NULL,
  `nns_sp_id` varchar(32) NOT NULL,
  `nns_create_time` datetime NOT NULL,
  `nns_modify_time` datetime NOT NULL,
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_mgtvbk_import_epg` */

DROP TABLE IF EXISTS `nns_mgtvbk_import_epg`;

CREATE TABLE `nns_mgtvbk_import_epg` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_org_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `nns_video_id_nns_video_type_nns_org_id` (`nns_video_id`,`nns_video_type`,`nns_org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_mgtvbk_import_epg_log` */

DROP TABLE IF EXISTS `nns_mgtvbk_import_epg_log`;

CREATE TABLE `nns_mgtvbk_import_epg_log` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_org_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_create_time` datetime NOT NULL,
  `nns_action` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_status` int(10) NOT NULL,
  `nns_reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_data` text COLLATE utf8_unicode_ci,
  `nns_name` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT '队列名称',
  `nns_file_dir` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'CDN消息反馈文件相对路径',
  PRIMARY KEY (`nns_id`),
  KEY `nns_create_time` (`nns_create_time`),
  KEY `nns_video_type` (`nns_video_type`),
  KEY `nns_name` (`nns_name`),
  KEY `nns_status` (`nns_status`),
  KEY `nns_action` (`nns_action`),
  KEY `nns_org_id` (`nns_org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_mgtvbk_import_fail` */

DROP TABLE IF EXISTS `nns_mgtvbk_import_fail`;

CREATE TABLE `nns_mgtvbk_import_fail` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_org_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_action` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_ref_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_create_time` datetime NOT NULL,
  `nns_modify_time` datetime NOT NULL,
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `nns_type_nns_org_id_nns_ref_id_nns_action` (`nns_type`,`nns_org_id`,`nns_ref_id`,`nns_action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_mgtvbk_import_log` */

DROP TABLE IF EXISTS `nns_mgtvbk_import_log`;

CREATE TABLE `nns_mgtvbk_import_log` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_func` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `nns_import_data` text COLLATE utf8_unicode_ci,
  `nns_state` int(11) DEFAULT NULL,
  `nns_summary` text COLLATE utf8_unicode_ci,
  `nns_assets_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_assets_video_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_assets_clip_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_assets_file_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_svc_item_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_org_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_create_time` datetime NOT NULL,
  `nns_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`nns_id`),
  KEY `nns_func` (`nns_func`),
  KEY `nns_state` (`nns_state`),
  KEY `nns_org_id` (`nns_org_id`),
  KEY `nns_name` (`nns_name`),
  KEY `nns_create_time` (`nns_create_time`),
  KEY `nns_assets_id` (`nns_assets_id`),
  KEY `nns_assets_clip_id` (`nns_assets_clip_id`),
  KEY `nns_assets_file_id` (`nns_assets_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_mgtvbk_import_op` */

DROP TABLE IF EXISTS `nns_mgtvbk_import_op`;

CREATE TABLE `nns_mgtvbk_import_op` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息ID',
  `nns_video_name` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '队列名称',
  `nns_video_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '落地数据GUID',
  `nns_video_type` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '影片类型',
  `nns_action` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '操作行为',
  `nns_create_time` datetime NOT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `nns_op_mtime` bigint(16) unsigned NOT NULL DEFAULT '0' COMMENT '操作时间',
  `nns_op_sp` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '下游平台ID',
  `nns_weight` smallint(5) unsigned DEFAULT '0' COMMENT '权重值越高，权重越高',
  `nns_is_group` smallint(3) unsigned DEFAULT '0' COMMENT '是否是分组  默认0  0 不是分组消息 | 1是分组消息',
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'CP_ID',
  `nns_bk_queue_excute_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '消息队列执行的结果url 所有情况',
  `nns_command_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '消息指令池ID',
  PRIMARY KEY (`nns_id`),
  KEY `nns_video_id` (`nns_video_id`),
  KEY `nns_action` (`nns_action`),
  KEY `nns_video_type` (`nns_video_type`),
  KEY `nns_op_mtime` (`nns_op_mtime`),
  KEY `nns_op_sp` (`nns_op_sp`(255)),
  KEY `nns_video_name` (`nns_video_name`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_mgtvbk_index_img` */

DROP TABLE IF EXISTS `nns_mgtvbk_index_img`;

CREATE TABLE `nns_mgtvbk_index_img` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_org_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_status` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_create_time` datetime DEFAULT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `nns_video_id_nns_org_id` (`nns_video_id`,`nns_org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_mgtvbk_log` */

DROP TABLE IF EXISTS `nns_mgtvbk_log`;

CREATE TABLE `nns_mgtvbk_log` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_text` text COLLATE utf8_unicode_ci NOT NULL,
  `nns_create_time` datetime NOT NULL,
  `nns_org_id` char(16) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`nns_id`),
  KEY `nns_name_nns_create_time` (`nns_name`,`nns_create_time`),
  KEY `nns_org_id` (`nns_org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_mgtvbk_message` */

DROP TABLE IF EXISTS `nns_mgtvbk_message`;

CREATE TABLE `nns_mgtvbk_message` (
  `nns_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_message_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息下发时间',
  `nns_message_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '上游下发的消息ID',
  `nns_message_xml` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '上游下发的原始内容（后期这个字段会删除掉）',
  `nns_message_content` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '上游下发的需要解析的内容信息（后期这个字段会删除掉）',
  `nns_message_original_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '上游下发的原始URL',
  `nns_message_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '下发的内容xml相对路径',
  `nns_bk_queue_excute_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '消息队列执行的结果url 所有情况',
  `nns_message_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '消息队列状态:0等待注入1ftp链接失败2ftp下载失败3注入成功4注入失败',
  `nns_action` tinyint(3) unsigned DEFAULT '0' COMMENT '操作类型',
  `nns_type` tinyint(3) unsigned DEFAULT '1' COMMENT '注入媒资类型:1主媒资2分集3片源',
  `nns_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '注入媒资名称',
  `nns_create_time` datetime NOT NULL COMMENT '创建时间',
  `nns_again` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '失败后重试次数,默认10次',
  `nns_delete` tinyint(3) unsigned DEFAULT '0' COMMENT '0为未删除，1为删除',
  `nns_fail_time` varchar(20) DEFAULT '' COMMENT '失败时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `nns_cp_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT 'cp_id',
  `nns_package_id` varchar(32) DEFAULT '' COMMENT '包ID',
  `nns_xmlurlqc` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '广州电信悦ME MD5摘要',
  `nns_encrypt` varchar(2048) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '广州电信悦ME 加密串',
  `nns_content_number` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'xml文件中包含的内容数量',
  PRIMARY KEY (`nns_id`),
  KEY `nns_create_time` (`nns_create_time`),
  KEY `nns_modify_time` (`nns_modify_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_mgtvbk_op_blacklist` */

DROP TABLE IF EXISTS `nns_mgtvbk_op_blacklist`;

CREATE TABLE `nns_mgtvbk_op_blacklist` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_index_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '0',
  `nns_video_media_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '0',
  `nns_category_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '0',
  `nns_type` smallint(4) NOT NULL,
  `nns_org_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_create_time` datetime NOT NULL,
  `nns_is_black` tinyint(3) NOT NULL,
  PRIMARY KEY (`nns_id`),
  KEY `nns_org_id_nns_video_id_nns_type` (`nns_org_id`,`nns_video_id`,`nns_type`),
  KEY `nns_video_id` (`nns_video_id`),
  KEY `nns_org_id` (`nns_org_id`),
  KEY `nns_create_time` (`nns_create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_mgtvbk_op_log` */

DROP TABLE IF EXISTS `nns_mgtvbk_op_log`;

CREATE TABLE `nns_mgtvbk_op_log` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_type` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_org_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_create_time` datetime NOT NULL,
  `nns_action` char(8) COLLATE utf8_unicode_ci NOT NULL,
  `nns_status` int(1) NOT NULL,
  `nns_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_weight` int(1) NOT NULL DEFAULT '0',
  `nns_index_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_media_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_op_mtime` bigint(16) NOT NULL,
  `nns_release_time` date DEFAULT NULL,
  `nns_op_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '???ID',
  `nns_category_id` char(32) COLLATE utf8_unicode_ci DEFAULT '0',
  `nns_file_len` int(8) DEFAULT '0',
  `nns_file_size` int(8) DEFAULT '0',
  `nns_bk_queue_excute_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '消息队列执行的结果url 所有情况',
  PRIMARY KEY (`nns_id`),
  KEY `index_status` (`nns_status`),
  KEY `index_type` (`nns_type`),
  KEY `index_ex_id` (`nns_video_id`),
  KEY `index_op_id` (`nns_op_id`),
  KEY `index_create_time` (`nns_create_time`),
  KEY `index_task_index` (`nns_org_id`,`nns_type`,`nns_create_time`,`nns_status`,`nns_action`),
  KEY `nns_category_id` (`nns_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_mgtvbk_op_queue` */

DROP TABLE IF EXISTS `nns_mgtvbk_op_queue`;

CREATE TABLE `nns_mgtvbk_op_queue` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_type` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_org_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_create_time` datetime NOT NULL,
  `nns_action` char(8) COLLATE utf8_unicode_ci NOT NULL,
  `nns_status` int(1) NOT NULL,
  `nns_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nns_weight` smallint(1) unsigned NOT NULL DEFAULT '0',
  `nns_index_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_media_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_op_mtime` bigint(16) NOT NULL,
  `nns_release_time` date DEFAULT NULL,
  `nns_state` int(1) NOT NULL DEFAULT '0' COMMENT '????舵?',
  `nns_ex_data` varchar(2048) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_from` varchar(32) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nns_encode_flag` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '转码url标示（MD5）',
  `nns_is_group` tinyint(3) unsigned DEFAULT '0' COMMENT '是否是分组  默认0  0 不是分组消息 | 1是分组消息',
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'nns_cp_id',
  `nns_bk_queue_excute_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '消息队列执行的结果url 所有情况',
  `nns_command_task_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '消息指令状态池ID',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `unique_op` (`nns_type`,`nns_org_id`,`nns_action`,`nns_video_id`,`nns_index_id`,`nns_media_id`),
  KEY `index_status` (`nns_status`),
  KEY `index_type` (`nns_type`),
  KEY `index_mtime` (`nns_op_mtime`),
  KEY `index_task_index` (`nns_org_id`,`nns_status`,`nns_video_id`,`nns_op_mtime`),
  KEY `index_encode_flag` (`nns_org_id`,`nns_status`,`nns_action`,`nns_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_mgtvbk_op_weight` */

DROP TABLE IF EXISTS `nns_mgtvbk_op_weight`;

CREATE TABLE `nns_mgtvbk_op_weight` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_type` char(8) COLLATE utf8_unicode_ci NOT NULL,
  `nns_index_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_media_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_org_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_weight` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '权重',
  PRIMARY KEY (`nns_id`),
  KEY `index_unique` (`nns_video_id`,`nns_type`,`nns_index_id`,`nns_media_id`,`nns_org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_mgtvbk_sp` */

DROP TABLE IF EXISTS `nns_mgtvbk_sp`;

CREATE TABLE `nns_mgtvbk_sp` (
  `nns_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nns_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT 'spåç§°',
  `nns_create_time` datetime NOT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  `nns_telphone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_email` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_contact` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_rule` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_config` text COLLATE utf8_unicode_ci COMMENT '绯荤???疆',
  `nns_bind_cp` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_state` tinyint(3) unsigned DEFAULT '0' COMMENT '使用状态  0  开启 | 1 关闭',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='è¿è¥å•†é…ç½®è¡¨';

/*Table structure for table `nns_mgtvbk_task` */

DROP TABLE IF EXISTS `nns_mgtvbk_task`;

CREATE TABLE `nns_mgtvbk_task` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_task_type` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `nns_org_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_vod_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_vod_index_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_state` char(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'é»˜è®¤ä¸ºç©ºï¼Œè¡¨ç¤ºæœªè¢«å¤„ç† okï¼šæ‰§è¡ŒæˆåŠŸ handleï¼šä»»åŠ¡å·²ä¸‹å‘ï¼ˆè¢«åˆ‡ç‰‡å·¥å…·å–èµ°ï¼‰ download:ä¸‹è½½å½±ç‰‡ clipingï¼šæ­£åœ¨åˆ‡ç‰‡ cancel:ä»»åŠ¡å–æ¶ˆ clip_fail: åˆ‡ç‰‡å¤±è´¥ clip_exists:åˆ‡ç‰‡å­˜åœ¨ unkown:æœªçŸ¥é”™è¯¯',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'çŠ¶æ€æè¿°',
  `nns_priority` int(4) DEFAULT NULL COMMENT 'ä¼˜å…ˆçº§',
  `nns_alive_time` datetime DEFAULT NULL COMMENT 'ä¸ŠæŠ¥æ—¶é—´',
  `nns_start_time` datetime DEFAULT NULL COMMENT 'ä»»åŠ¡å¼€å§‹æ—¶é—´',
  `nns_end_time` datetime DEFAULT NULL COMMENT 'ä»»åŠ¡ç»“æŸæ—¶é—´',
  `nns_create_time` datetime NOT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`nns_id`),
  KEY `nns_org_id` (`nns_org_id`),
  KEY `nns_vod_id_nns_vod_index_id` (`nns_vod_id`,`nns_vod_index_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ä»»åŠ¡è¡¨';

/*Table structure for table `nns_mgtvbk_task_log` */

DROP TABLE IF EXISTS `nns_mgtvbk_task_log`;

CREATE TABLE `nns_mgtvbk_task_log` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_task_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ä»»åŠ¡id',
  `nns_state` char(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ä¸ŠæŠ¥çŠ¶æ€',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ä¸ŠæŠ¥çŠ¶æ€æè¿°',
  `nns_create_time` datetime NOT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`nns_id`),
  KEY `nns_task_id` (`nns_task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ä»»åŠ¡çŠ¶æ€ä¸ŠæŠ¥è®°å½•è¡¨';

/*Table structure for table `nns_module` */

DROP TABLE IF EXISTS `nns_module`;

CREATE TABLE `nns_module` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` char(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_state` int(4) DEFAULT NULL,
  `nns_create_time` datetime DEFAULT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_multiple_cdn_status` */

DROP TABLE IF EXISTS `nns_multiple_cdn_status`;

CREATE TABLE `nns_multiple_cdn_status` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_media_id` char(32) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_index_id` char(32) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_type` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '单位类型',
  `nns_ex_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL COMMENT '前置CDN返回拉流地址',
  `nns_front_cdn` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '前置CDN',
  `nns_back_cdn` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '后置CDN',
  `nns_action` char(16) COLLATE utf8_unicode_ci NOT NULL,
  `nns_create_time` datetime NOT NULL,
  `nns_status` int(1) NOT NULL COMMENT '状态,0等待 1成功 2失败',
  `nns_reason` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '错误原因',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_notify_message` */

DROP TABLE IF EXISTS `nns_notify_message`;

CREATE TABLE `nns_notify_message` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'GUID',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT '消息ID',
  `nns_notify` mediumtext COLLATE utf8_unicode_ci NOT NULL COMMENT '反馈内容',
  `nns_status` int(2) NOT NULL DEFAULT '0' COMMENT '反馈状态 0等待反馈 1正在反馈 2反馈失败 3反馈成功',
  `nns_org_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'SPID',
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '注入平台CPID',
  `nns_create_time` datetime NOT NULL COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_op_log` */

DROP TABLE IF EXISTS `nns_op_log`;

CREATE TABLE `nns_op_log` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_op_admin_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_op_type` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_op_time` datetime DEFAULT NULL,
  `nns_op_desc` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_org_type` int(4) DEFAULT NULL,
  `nns_org_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`nns_id`),
  KEY `index_optime` (`nns_op_time`),
  KEY `index_org` (`nns_org_type`,`nns_org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_partner` */

DROP TABLE IF EXISTS `nns_partner`;

CREATE TABLE `nns_partner` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_desc` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_state` int(4) DEFAULT NULL,
  `nns_create_time` datetime DEFAULT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  `nns_contact` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_telephone` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_email` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_addr` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_pass_queue` */

DROP TABLE IF EXISTS `nns_pass_queue`;

CREATE TABLE `nns_pass_queue` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_action` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0无行为1添加2修改3删除',
  `nns_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '队列类型0栏目同步1栏目推荐2CP同步3栏目图片同步4媒资融合',
  `nns_message_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息ID',
  `nns_org_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'SPID',
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'CPID',
  `nns_audit` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0待审核1审核通过2审核不通过',
  `nns_pause` tinyint(1) NOT NULL DEFAULT '0' COMMENT '队列0正常1暂停',
  `nns_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '队列状态0注入成功1等待注入2正在注入3注入失败4等待反馈',
  `nns_content` text COLLATE utf8_unicode_ci NOT NULL COMMENT '透传内容',
  `nns_result_desc` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '队列任务结果描述',
  `nns_queue_time` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '队列毫秒级创建时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `nns_queue_execute_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '执行注入的本地文本日志',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_play_log` */

DROP TABLE IF EXISTS `nns_play_log`;

CREATE TABLE `nns_play_log` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_user_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_product_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_service_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_begin_position` datetime DEFAULT NULL,
  `nns_end_position` datetime DEFAULT NULL,
  `nns_provider_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_service_asset_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_media_asset_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_video_type` int(2) DEFAULT NULL,
  `nns_device_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_user_agent` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_ua_ips` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_qam_area_code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_qam_frequence` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_qam_ips` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_service_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_nmds_id` char(128) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_nmds_ips` char(128) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`nns_id`),
  KEY `nns_user_id` (`nns_user_id`),
  KEY `nns_device_id` (`nns_device_id`),
  KEY `nns_video_id` (`nns_video_id`),
  KEY `nns_video_name` (`nns_video_name`),
  KEY `nns_qam_area_code` (`nns_qam_area_code`),
  KEY `nns_begin_position` (`nns_begin_position`),
  KEY `nns_end_position` (`nns_end_position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_pri` */

DROP TABLE IF EXISTS `nns_pri`;

CREATE TABLE `nns_pri` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_module_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_name` char(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_state` int(4) DEFAULT NULL,
  `nns_create_time` datetime DEFAULT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_producer` */

DROP TABLE IF EXISTS `nns_producer`;

CREATE TABLE `nns_producer` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_cp_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'nns_cp 表的 nns_id',
  `nns_import_source` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '注入MSP的cpid|内容提供商ID',
  `nns_domain` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'domain',
  `nns_producer` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '内容提供商名称',
  `nns_contactor` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '联系人',
  `nns_telephone` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '电话号码',
  `nns_email` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '邮箱',
  `nns_addr` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '地址',
  `nns_desc` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '描述',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `nns_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '影片类型 media 点播 | live_media 直播 | playbill 节目单 ',
  `nns_node_id` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '节点ID',
  `nns_list_ids` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '节点列表',
  `nns_weight` smallint(1) unsigned NOT NULL DEFAULT '0' COMMENT '权重',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_product` */

DROP TABLE IF EXISTS `nns_product`;

CREATE TABLE `nns_product` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_order_number` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_order_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `nns_order_type` tinyint(2) unsigned NOT NULL COMMENT '产品包类型 0 接收CP/SP所有 1融合后产品包',
  `nns_order_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `nns_order_price` decimal(5,2) NOT NULL DEFAULT '0.00' COMMENT '订购价格',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_product_vod_bind` */

DROP TABLE IF EXISTS `nns_product_vod_bind`;

CREATE TABLE `nns_product_vod_bind` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nns_vod_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '主媒资ID',
  `nns_product_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '产品包ID',
  `nns_asset_import_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '主媒资注入id',
  `nns_import_source` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '注入来源',
  `nns_cp_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_project_group` */

DROP TABLE IF EXISTS `nns_project_group`;

CREATE TABLE `nns_project_group` (
  `nns_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '通用配置组合GUID',
  `nns_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '组合名称',
  `nns_key_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'KEY的自增ID,多个以逗号分隔',
  `nns_value` varchar(4096) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '键值，json串',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_project_key` */

DROP TABLE IF EXISTS `nns_project_key`;

CREATE TABLE `nns_project_key` (
  `nns_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置键GUID',
  `nns_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '配置键名称',
  `nns_model_id` smallint(4) unsigned NOT NULL DEFAULT '0' COMMENT '控制器GUID',
  `nns_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '配置键KEY',
  `nns_key_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0text文本1选择框',
  `nns_key_value` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '当key_type为选择框设置选择值，为json串',
  `nns_is_more` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否支持多个0否1是',
  `nns_example` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'KEY值举例',
  `nns_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_project_model` */

DROP TABLE IF EXISTS `nns_project_model`;

CREATE TABLE `nns_project_model` (
  `nns_id` smallint(4) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置模块GUID',
  `nns_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '播控配置模块名称',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '播控配置模块相关描述',
  `nns_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_queue` */

DROP TABLE IF EXISTS `nns_queue`;

CREATE TABLE `nns_queue` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_queue` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '定时器队列池模板',
  `nns_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '定时器队列池名称',
  `nns_desc` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '描述',
  `nns_timer` smallint(5) unsigned DEFAULT '30' COMMENT '定时器运行时间间隔（单位/分钟）',
  `nns_often_state` tinyint(3) unsigned DEFAULT '0' COMMENT '是否频繁生成队列池：  0 是 | 1 否 ',
  `nns_ext_info` text COLLATE utf8_unicode_ci COMMENT '扩展信息',
  `nns_state` tinyint(3) unsigned DEFAULT '0' COMMENT '0 启用 | 1 禁用',
  `nns_create_time` datetime DEFAULT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `idx_queue` (`nns_queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_queue_pool` */

DROP TABLE IF EXISTS `nns_queue_pool`;

CREATE TABLE `nns_queue_pool` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_queue_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '队列模板id',
  `nns_value` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '值',
  `nns_state` tinyint(3) unsigned DEFAULT '0' COMMENT '状态: 0 启用 | 1 禁用',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `idx_queue_value` (`nns_queue_id`,`nns_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_role` */

DROP TABLE IF EXISTS `nns_role`;

CREATE TABLE `nns_role` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_state` int(4) DEFAULT NULL,
  `nns_pri_id` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_desc` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_create_time` datetime DEFAULT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_send_platform` */

DROP TABLE IF EXISTS `nns_send_platform`;

CREATE TABLE `nns_send_platform` (
  `nns_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nns_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nns_api_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nns_sync_operation` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nns_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1启用，2禁用',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '最后修改时间',
  `nns_language` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nns_cp_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_send_platform_queue` */

DROP TABLE IF EXISTS `nns_send_platform_queue`;

CREATE TABLE `nns_send_platform_queue` (
  `nns_id` varchar(32) NOT NULL DEFAULT '' COMMENT 'ID，主键',
  `nns_other_platform_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nns_queue_type` tinyint(1) NOT NULL COMMENT '1：全平台 , 2：单一平台',
  `nns_assets_type` varchar(16) NOT NULL DEFAULT '' COMMENT '队列媒资类型',
  `nns_operation_type` char(3) NOT NULL DEFAULT '' COMMENT '该队列是由什么操作生成的',
  `nns_cms_video_id` varchar(32) DEFAULT NULL COMMENT 'cms主媒资id',
  `nns_cms_video_index_id` varchar(32) DEFAULT NULL COMMENT 'cms分集id',
  `nns_cms_media_id` varchar(32) DEFAULT NULL COMMENT 'cms片源id',
  `nns_upstream_video_id` varchar(32) DEFAULT NULL COMMENT '上游主媒资id',
  `nns_upstream_video_index_id` varchar(32) DEFAULT NULL COMMENT '上游分集id',
  `nns_upstream_media_id` varchar(32) DEFAULT NULL COMMENT '上游片源id',
  `nns_assets_data_type` char(8) DEFAULT 'adi' COMMENT '媒资数据类型，默认是adi',
  `nns_assets_data_url` varchar(128) DEFAULT NULL COMMENT '队列数据文件地址',
  `nns_cms_admin_name` varchar(32) DEFAULT 'upstream' COMMENT 'cms操作管理员名称',
  `nns_status` tinyint(1) NOT NULL COMMENT '1未同步，2同步成功，3同步失败，4程序取消',
  `nns_other_platform_return_data` varchar(1024) DEFAULT NULL COMMENT '第三方平台返回数据',
  `nns_next_sync_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '下次同步时间',
  `nns_last_sync_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '最后同步时间，第三方系统响应时间',
  `nns_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `nns_ex_message` varchar(600) DEFAULT NULL COMMENT '其他附加信息',
  `nns_depot_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0点播，1 直播',
  `nns_cp_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`nns_id`),
  KEY `index_order_platform_id` (`nns_other_platform_id`),
  KEY `index_cms_video_id` (`nns_cms_video_id`),
  KEY `index_cms_video_index_id` (`nns_cms_video_index_id`),
  KEY `index_cms_media_id` (`nns_cms_media_id`),
  KEY `index_upstream_video_id` (`nns_upstream_video_id`),
  KEY `index_upstream_video_index_id` (`nns_upstream_video_index_id`),
  KEY `index_upstream_media_id` (`nns_upstream_media_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_send_platform_queue_log` */

DROP TABLE IF EXISTS `nns_send_platform_queue_log`;

CREATE TABLE `nns_send_platform_queue_log` (
  `nns_id` varchar(32) NOT NULL,
  `nns_queue_id` varchar(32) NOT NULL COMMENT '队列id',
  `nns_summary` varchar(128) DEFAULT NULL COMMENT '说明',
  `nns_create_time` varchar(32) DEFAULT '0000-00-00 00:00:00.000' COMMENT '创建时间',
  PRIMARY KEY (`nns_id`),
  KEY `index_queue_id` (`nns_queue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_service_type` */

DROP TABLE IF EXISTS `nns_service_type`;

CREATE TABLE `nns_service_type` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_service_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '片源服务类型',
  `nns_service_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '片源服务类型名称',
  `nns_video_type` tinyint(3) unsigned DEFAULT '0' COMMENT '0 点播 | 1 直播',
  `nns_desc` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '描述',
  `nns_state` tinyint(3) unsigned DEFAULT '0' COMMENT '0 启用 | 1 禁用',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `idx_video_type` (`nns_video_type`,`nns_service_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_stills` */

DROP TABLE IF EXISTS `nns_stills`;

CREATE TABLE `nns_stills` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '剧照ID，该表主键，不可修改',
  `nns_video_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '剧照隶属影片ID',
  `nns_video_type` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT '剧照隶属影片类型',
  `nns_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '剧照地址',
  `nns_summary` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '剧照说明',
  `nns_create_time` datetime NOT NULL COMMENT '剧照创建时间',
  `nns_order` int(4) NOT NULL COMMENT '剧照排序字段',
  `nns_width` int(4) DEFAULT NULL COMMENT '剧照宽',
  `nns_height` int(4) DEFAULT NULL COMMENT '剧照高',
  `nns_scale` int(4) DEFAULT NULL COMMENT '高宽比*100',
  PRIMARY KEY (`nns_id`),
  KEY `index_video` (`nns_video_id`,`nns_video_type`),
  KEY `index_width_height` (`nns_width`,`nns_height`),
  KEY `index_scale` (`nns_scale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='剧照表';

/*Table structure for table `nns_transcode_load_balance` */

DROP TABLE IF EXISTS `nns_transcode_load_balance`;

CREATE TABLE `nns_transcode_load_balance` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_type` tinyint(3) unsigned DEFAULT '0' COMMENT 'api 类型   0 点播 | 1直播',
  `nns_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '负载均衡名称',
  `nns_api_url` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'api请求地址',
  `nns_manufacturer_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '转码厂家ID',
  `nns_sp_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'SP ID',
  `nns_state` tinyint(3) unsigned DEFAULT '0' COMMENT '状态 0 启用 | 1 禁用',
  `nns_desc` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '描述',
  `nns_max_num` tinyint(3) unsigned DEFAULT '0' COMMENT '服务器能够承载的线程  配置为0  无限制 ',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_transcode_manufacturer` */

DROP TABLE IF EXISTS `nns_transcode_manufacturer`;

CREATE TABLE `nns_transcode_manufacturer` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '转码厂家名称',
  `nns_mark` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '转码标示',
  `nns_type` tinyint(4) DEFAULT '0' COMMENT '厂家支持转码类型  0 只支持点播  |  1 只支持直播 | 2 支持点播和直播',
  `nns_api_addr` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '转码API请求地址  json 里面可能有直播请求地址，可能有点播请求地址',
  `nns_state` tinyint(3) unsigned DEFAULT '0' COMMENT '0 启用 | 1 禁用',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `idx_mark` (`nns_mark`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_transcode_manufacturer_model` */

DROP TABLE IF EXISTS `nns_transcode_manufacturer_model`;

CREATE TABLE `nns_transcode_manufacturer_model` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_manufacturer_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '转码厂家GUID',
  `nns_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '转码模板名称',
  `nns_mark` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '厂商转码模板标示',
  `nns_file_path` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '输出文件路径,用于创建文件夹',
  `nns_state` tinyint(3) unsigned DEFAULT '0' COMMENT '0 启用 | 1 禁用',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `idx_manufacturer_mark` (`nns_manufacturer_id`,`nns_mark`),
  UNIQUE KEY `idx_manufacturer_filepath` (`nns_manufacturer_id`,`nns_file_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_transcode_model` */

DROP TABLE IF EXISTS `nns_transcode_model`;

CREATE TABLE `nns_transcode_model` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '策略名称',
  `nns_model_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '厂家模板GUID',
  `nns_cp_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '上游CPID',
  `nns_sp_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'SPID',
  `nns_type` tinyint(3) unsigned DEFAULT '0' COMMENT '转码类型  0 点播转码| 1 直播转码',
  `nns_input_definition` varchar(8) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '输入文件清晰度',
  `nns_output_definition` varchar(8) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '输出文件清晰度',
  `nns_input_file_type` varchar(8) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '输入片源文件格式',
  `nns_output_file_type` varchar(8) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '输出片源文件格式',
  `nns_output_file_tag` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '输出文件的tag标示 26 STB,27 IPHONE，28 IPAD，29 ANDROID_PHONE，30 ANDROID_PAD，31 PC，多个用,分隔',
  `nns_start_bitrate` mediumint(8) unsigned DEFAULT '0' COMMENT '码率开始匹配值',
  `nns_end_bitrate` mediumint(8) unsigned DEFAULT '0' COMMENT '码率结束匹配值',
  `nns_output_bitrate` mediumint(8) unsigned DEFAULT '0' COMMENT '转码输出码率',
  `nns_state` tinyint(3) unsigned DEFAULT '0' COMMENT '0 启用 | 禁用',
  `nns_drawing_frame` tinyint(3) unsigned DEFAULT '0' COMMENT '是否启动抽帧  0 否 | 1 是',
  `nns_drawing_type` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '抽帧格式  json 存储',
  `nns_img_type` varchar(6) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '图片类型',
  `nns_drawing_rule` tinyint(3) unsigned DEFAULT '0' COMMENT '抽帧时间规则',
  `nns_video_config` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '视屏输出配置 json存储',
  `nns_audio_config` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '音频输出配置 json存储',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `nns_view_bitrate` mediumint(8) unsigned DEFAULT '0' COMMENT '界面展示码率',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_video_label` */

DROP TABLE IF EXISTS `nns_video_label`;

CREATE TABLE `nns_video_label` (
  `nns_id` char(32) CHARACTER SET utf8 NOT NULL,
  `nns_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT '标签名称',
  `nns_type` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '标签类型ID area 地区 year 年份等',
  `nns_create_time` datetime NOT NULL,
  `nns_times` int(8) NOT NULL DEFAULT '0' COMMENT '标签使用次数',
  `nns_state` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1为启用 默认为启用 0为不启用',
  `nns_source` char(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'main' COMMENT 'main Ö÷Ìå±êÇ© hot ÓÃ»§ÐÐÎªÈÈÃÅ±êÇ©',
  `nns_count` int(11) NOT NULL DEFAULT '0' COMMENT 'åŒ…å«å½±ç‰‡æ€»æ•°',
  PRIMARY KEY (`nns_id`),
  KEY `nns_name` (`nns_name`,`nns_type`,`nns_times`,`nns_state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='影片资源分类标签表';

/*Table structure for table `nns_video_label_bind_1000` */

DROP TABLE IF EXISTS `nns_video_label_bind_1000`;

CREATE TABLE `nns_video_label_bind_1000` (
  `nns_video_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_type` int(1) NOT NULL DEFAULT '0',
  `nns_label_id` char(32) NOT NULL,
  `nns_label_type_id` char(4) NOT NULL DEFAULT '0000',
  KEY `nns_video_id` (`nns_video_id`,`nns_label_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_video_label_bind_1001` */

DROP TABLE IF EXISTS `nns_video_label_bind_1001`;

CREATE TABLE `nns_video_label_bind_1001` (
  `nns_video_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_type` int(1) NOT NULL DEFAULT '0',
  `nns_label_id` char(32) NOT NULL,
  `nns_label_type_id` char(4) NOT NULL DEFAULT '0000',
  KEY `nns_video_id` (`nns_video_id`,`nns_label_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_video_label_bind_1002` */

DROP TABLE IF EXISTS `nns_video_label_bind_1002`;

CREATE TABLE `nns_video_label_bind_1002` (
  `nns_video_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_type` int(1) NOT NULL DEFAULT '0',
  `nns_label_id` char(32) NOT NULL,
  `nns_label_type_id` char(4) NOT NULL DEFAULT '0000',
  KEY `nns_video_id` (`nns_video_id`,`nns_label_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_video_label_bind_1003` */

DROP TABLE IF EXISTS `nns_video_label_bind_1003`;

CREATE TABLE `nns_video_label_bind_1003` (
  `nns_video_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_type` int(1) NOT NULL DEFAULT '0',
  `nns_label_id` char(32) NOT NULL,
  `nns_label_type_id` char(4) NOT NULL DEFAULT '0000',
  KEY `nns_video_id` (`nns_video_id`,`nns_label_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_video_label_bind_1004` */

DROP TABLE IF EXISTS `nns_video_label_bind_1004`;

CREATE TABLE `nns_video_label_bind_1004` (
  `nns_video_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_type` int(1) NOT NULL DEFAULT '0',
  `nns_label_id` char(32) NOT NULL,
  `nns_label_type_id` char(4) NOT NULL DEFAULT '0000',
  KEY `nns_video_id` (`nns_video_id`,`nns_label_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_video_label_bind_1005` */

DROP TABLE IF EXISTS `nns_video_label_bind_1005`;

CREATE TABLE `nns_video_label_bind_1005` (
  `nns_video_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_type` int(1) NOT NULL DEFAULT '0',
  `nns_label_id` char(32) NOT NULL,
  `nns_label_type_id` char(4) NOT NULL DEFAULT '0000',
  KEY `nns_video_id` (`nns_video_id`,`nns_label_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_video_label_bind_1006` */

DROP TABLE IF EXISTS `nns_video_label_bind_1006`;

CREATE TABLE `nns_video_label_bind_1006` (
  `nns_video_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_type` int(1) NOT NULL DEFAULT '0',
  `nns_label_id` char(32) NOT NULL,
  `nns_label_type_id` char(4) NOT NULL DEFAULT '0000',
  KEY `nns_video_id` (`nns_video_id`,`nns_label_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_video_label_bind_1007` */

DROP TABLE IF EXISTS `nns_video_label_bind_1007`;

CREATE TABLE `nns_video_label_bind_1007` (
  `nns_video_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_type` int(1) NOT NULL DEFAULT '0',
  `nns_label_id` char(32) NOT NULL,
  `nns_label_type_id` char(4) NOT NULL DEFAULT '0000',
  KEY `nns_video_id` (`nns_video_id`,`nns_label_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_video_label_bind_1008` */

DROP TABLE IF EXISTS `nns_video_label_bind_1008`;

CREATE TABLE `nns_video_label_bind_1008` (
  `nns_video_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_type` int(1) NOT NULL DEFAULT '0',
  `nns_label_id` char(32) NOT NULL,
  `nns_label_type_id` char(4) NOT NULL DEFAULT '0000',
  KEY `nns_video_id` (`nns_video_id`,`nns_label_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_video_label_bind_1009` */

DROP TABLE IF EXISTS `nns_video_label_bind_1009`;

CREATE TABLE `nns_video_label_bind_1009` (
  `nns_video_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_type` int(1) NOT NULL DEFAULT '0',
  `nns_label_id` char(32) NOT NULL,
  `nns_label_type_id` char(4) NOT NULL DEFAULT '0000',
  KEY `nns_video_id` (`nns_video_id`,`nns_label_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_video_label_bind_1010` */

DROP TABLE IF EXISTS `nns_video_label_bind_1010`;

CREATE TABLE `nns_video_label_bind_1010` (
  `nns_video_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_type` int(1) NOT NULL DEFAULT '0',
  `nns_label_id` char(32) NOT NULL,
  `nns_label_type_id` char(4) NOT NULL DEFAULT '0000',
  KEY `nns_video_id` (`nns_video_id`,`nns_label_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_video_label_bind_1011` */

DROP TABLE IF EXISTS `nns_video_label_bind_1011`;

CREATE TABLE `nns_video_label_bind_1011` (
  `nns_video_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_type` int(1) NOT NULL DEFAULT '0',
  `nns_label_id` char(32) NOT NULL,
  `nns_label_type_id` char(4) NOT NULL DEFAULT '0000',
  KEY `nns_video_id` (`nns_video_id`,`nns_label_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_view_record` */

DROP TABLE IF EXISTS `nns_view_record`;

CREATE TABLE `nns_view_record` (
  `nns_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_template_type` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `nns_template_id` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `nns_template_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_asset_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_service_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_category_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_content_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_content_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_create_time` datetime NOT NULL,
  `nns_user_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_device_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_user_ip` char(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`nns_id`),
  KEY `nns_template_type` (`nns_template_type`),
  KEY `nns_template_id` (`nns_template_id`),
  KEY `nns_template_name` (`nns_template_name`),
  KEY `nns_user_id` (`nns_user_id`),
  KEY `nns_device_id` (`nns_device_id`),
  KEY `nns_create_time` (`nns_create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_virtual_category_content` */

DROP TABLE IF EXISTS `nns_virtual_category_content`;

CREATE TABLE `nns_virtual_category_content` (
  `nns_media_asset_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_category_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_value` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`nns_media_asset_id`,`nns_category_id`,`nns_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_vod` */

DROP TABLE IF EXISTS `nns_vod`;

CREATE TABLE `nns_vod` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '主媒资名称',
  `nns_state` tinyint(3) unsigned DEFAULT '0' COMMENT '状态',
  `nns_view_type` tinyint(3) unsigned DEFAULT '1' COMMENT '影片类型',
  `nns_org_type` tinyint(4) unsigned DEFAULT '0',
  `nns_org_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '运营商ID',
  `nns_tag` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '终端标识',
  `nns_director` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '导演',
  `nns_actor` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '演员',
  `nns_show_time` date DEFAULT NULL COMMENT '发布日期',
  `nns_view_len` int(10) unsigned DEFAULT '0' COMMENT '影片播放时长（秒）',
  `nns_all_index` smallint(5) unsigned DEFAULT '0',
  `nns_new_index` smallint(5) unsigned DEFAULT '0',
  `nns_area` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '上映地区',
  `nns_image0` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '大图',
  `nns_image1` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '中图',
  `nns_image2` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '小图',
  `nns_image3` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '扩展1',
  `nns_image4` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '扩展2',
  `nns_image5` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '扩展3',
  `nns_summary` varchar(4096) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '描述',
  `nns_remark` varchar(4096) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '标志',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `nns_deleted` tinyint(4) unsigned DEFAULT '0' COMMENT '是否删除 0 未删除  |  1删除',
  `nns_check` tinyint(4) unsigned DEFAULT '0' COMMENT '审核状态  0 审核通过 | 1 审核不通过',
  `nns_depot_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '点播、直播GUID',
  `nns_category_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '栏目ID',
  `nns_play_count` int(4) unsigned DEFAULT '0' COMMENT '播放次数',
  `nns_score_total` float(4,2) DEFAULT NULL COMMENT '评分',
  `nns_score_count` int(10) unsigned DEFAULT '0' COMMENT '总分数',
  `nns_point` tinyint(1) unsigned DEFAULT '0' COMMENT '评分',
  `nns_copyright_date` date DEFAULT NULL COMMENT '版权日期',
  `nns_asset_import_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '主媒资注入id',
  `nns_pinyin` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称拼音',
  `nns_pinyin_length` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '拼音长度',
  `nns_alias_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '别名',
  `nns_eng_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '英文名称',
  `nns_language` varchar(16) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '语言',
  `nns_text_lang` varchar(16) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '字幕语言',
  `nns_producer` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '内容发布商',
  `nns_screenwriter` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '字幕作者',
  `nns_play_role` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '角色',
  `nns_copyright_begin_date` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '版权开始时间',
  `nns_copyright_range` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '版权过期时间',
  `nns_vod_part` varchar(4) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '关键字',
  `nns_import_source` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '注入来源',
  `nns_kind` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '影片类型 如：战争,爱情',
  `nns_copyright` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '版权信息 如芒果TV',
  `nns_clarity` varchar(32) COLLATE utf8_unicode_ci DEFAULT '',
  `nns_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '(add:1|modify:2|destroy:3|ok:0|execute:4)',
  `nns_action` char(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'add' COMMENT 'add|modify|destroy|ok|execute',
  `nns_integer_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `nns_image_v` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '横图',
  `nns_image_s` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '方图',
  `nns_image_h` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '竖图',
  `nns_cp_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '注入CPID',
  `nns_conf_info` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '用于存放主媒资扩展配置信息',
  `nns_ext_url` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '上游方注入url 和其他信息  json存储',
  `nns_ishuaxu` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '媒资正片/花絮类型，0【正片】1【花絮】2【回看-正片】3【限免-正片】4【预告】',
  `nns_ex_lang` varchar(2048) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '扩展多语言',
  `nns_positiveoriginalid` varchar(36) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '正片媒资原始ID',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `nns_integer_id` (`nns_integer_id`),
  KEY `index_pinyin` (`nns_pinyin`),
  KEY `index_modify_time` (`nns_modify_time`),
  KEY `index_org` (`nns_org_type`,`nns_org_id`),
  KEY `index_category` (`nns_depot_id`,`nns_category_id`),
  KEY `index_import_id` (`nns_asset_import_id`,`nns_import_source`),
  KEY `nns_status` (`nns_status`,`nns_action`)
) ENGINE=InnoDB AUTO_INCREMENT=29899 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='最后操作动作';

/*Table structure for table `nns_vod_audit` */

DROP TABLE IF EXISTS `nns_vod_audit`;

CREATE TABLE `nns_vod_audit` (
  `nns_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'guid',
  `nns_import_id` varchar(128) NOT NULL DEFAULT '' COMMENT '注入id',
  `nns_name` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nns_video_type` char(12) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '媒资类型  video 主媒资| index 分集| media 片源',
  `nns_action` char(12) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'add 添加 | destroy 删除',
  `nns_check` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 未审核  |1 审核通过 | 2 审核未通过',
  `nns_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 未操作| 1 已上线  | 2 已下线',
  `nns_org_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '运营商id',
  `nns_cp_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT 'cp id',
  `nns_create_time` datetime NOT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `nns_parent_import_id` varchar(128) NOT NULL DEFAULT '' COMMENT '父级注入id',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `nns_import_org_cp_type_id` (`nns_import_id`,`nns_org_id`,`nns_cp_id`,`nns_video_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_vod_audit_log` */

DROP TABLE IF EXISTS `nns_vod_audit_log`;

CREATE TABLE `nns_vod_audit_log` (
  `nns_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nns_operator` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '操作人',
  `nns_vod_name` varchar(1024) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '操作发送内容',
  `nns_cp_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT 'cp id内容标识，默认为0',
  `nns_action` char(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '内容动作（add 新增 | destory 删除）',
  `nns_state` char(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '内容操作动作（pass审核通过|refused不通过|online上线|unline下线',
  `nns_create_time` datetime NOT NULL COMMENT '创建时间',
  `nns_org_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '运营商id',
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_vod_audit_word` */

DROP TABLE IF EXISTS `nns_vod_audit_word`;

CREATE TABLE `nns_vod_audit_word` (
  `nns_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'guid',
  `nns_word` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '审核媒资、分集、片源名称',
  `nns_enabled` tinyint(3) unsigned NOT NULL COMMENT '是否可当敏感词（0 不可用做敏感词 | 1可用做敏感词 ）',
  `nns_org_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '运营商id',
  `nns_create_time` datetime NOT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`nns_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_vod_copy` */

DROP TABLE IF EXISTS `nns_vod_copy`;

CREATE TABLE `nns_vod_copy` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_state` int(4) DEFAULT NULL,
  `nns_view_type` int(4) DEFAULT '1',
  `nns_pub_category_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_pri_category_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_pub_category_detail_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_pri_category_detail_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_org_type` int(4) DEFAULT NULL,
  `nns_org_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_tag` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_tag0` int(4) DEFAULT '0',
  `nns_tag1` int(4) DEFAULT '0',
  `nns_tag2` int(4) DEFAULT '0',
  `nns_tag3` int(4) DEFAULT '0',
  `nns_tag4` int(4) DEFAULT '0',
  `nns_tag5` int(4) DEFAULT '0',
  `nns_tag6` int(4) DEFAULT '0',
  `nns_tag7` int(4) DEFAULT '0',
  `nns_tag8` int(4) DEFAULT '0',
  `nns_tag9` int(4) DEFAULT '0',
  `nns_tag10` int(4) DEFAULT '0',
  `nns_tag11` int(4) DEFAULT '0',
  `nns_tag12` int(4) DEFAULT '0',
  `nns_tag13` int(4) DEFAULT '0',
  `nns_tag14` int(4) DEFAULT '0',
  `nns_tag15` int(4) DEFAULT '0',
  `nns_tag16` int(4) DEFAULT '0',
  `nns_tag17` int(4) DEFAULT '0',
  `nns_tag18` int(4) DEFAULT '0',
  `nns_tag19` int(4) DEFAULT '0',
  `nns_tag20` int(4) DEFAULT '0',
  `nns_tag21` int(4) DEFAULT '0',
  `nns_tag22` int(4) DEFAULT '0',
  `nns_tag23` int(4) DEFAULT '0',
  `nns_tag24` int(4) DEFAULT '0',
  `nns_tag25` int(4) DEFAULT '0',
  `nns_tag26` int(4) DEFAULT '0',
  `nns_tag27` int(4) DEFAULT '0',
  `nns_tag28` int(4) DEFAULT '0',
  `nns_tag29` int(4) DEFAULT '0',
  `nns_tag30` int(4) DEFAULT '0',
  `nns_tag31` int(4) DEFAULT '0',
  `nns_director` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_actor` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_show_time` date DEFAULT NULL,
  `nns_view_len` int(4) DEFAULT NULL,
  `nns_all_index` int(4) DEFAULT '0',
  `nns_new_index` int(4) DEFAULT '0',
  `nns_area` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_image0` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_image1` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_image2` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_image3` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_image4` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_image5` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_summary` varchar(4096) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_remark` varchar(4096) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_create_time` datetime DEFAULT NULL,
  `nns_modify_time` datetime DEFAULT NULL,
  `nns_deleted` int(4) DEFAULT '0',
  `nns_check` int(4) DEFAULT '0',
  `nns_depot_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_category_id` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_play_count` int(4) DEFAULT NULL,
  `nns_score_total` int(8) DEFAULT NULL,
  `nns_score_count` int(4) DEFAULT NULL,
  `nns_point` int(1) DEFAULT '0',
  `nns_copyright_date` date DEFAULT NULL,
  `nns_asset_import_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '主媒资注入id',
  `nns_pinyin` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_pinyin_length` int(3) NOT NULL COMMENT '.......',
  `nns_alias_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_eng_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_language` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_text_lang` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_producer` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_screenwriter` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_play_role` varchar(4096) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_copyright_range` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_vod_part` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '²¿',
  `nns_keyword` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_import_source` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '注入来源',
  `nns_kind` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '影片类型 如：战争,爱情',
  `nns_copyright` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '版权信息 如芒果TV',
  `nns_clarity` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '������',
  `nns_status` int(4) NOT NULL DEFAULT '1' COMMENT '(add:1|modify:2|destroy:3|ok:0|execute:4)',
  `nns_action` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'add' COMMENT 'add|modify|destroy|ok|execute',
  `nns_integer_id` int(11) NOT NULL AUTO_INCREMENT,
  `nns_image_v` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_image_s` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_image_h` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_cp_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `nns_integer_id` (`nns_integer_id`),
  KEY `index_pinyin` (`nns_pinyin`),
  KEY `index_modify_time` (`nns_modify_time`),
  KEY `index_org` (`nns_org_type`,`nns_org_id`),
  KEY `index_category` (`nns_depot_id`,`nns_category_id`),
  KEY `index_import_id` (`nns_asset_import_id`,`nns_import_source`),
  KEY `nns_status` (`nns_status`,`nns_action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='最后操作动作';

/*Table structure for table `nns_vod_ex` */

DROP TABLE IF EXISTS `nns_vod_ex`;

CREATE TABLE `nns_vod_ex` (
  `nns_vod_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '主媒资扩展注入id',
  `nns_key` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_cp_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT 'cp_id',
  PRIMARY KEY (`nns_vod_id`,`nns_key`,`nns_cp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_vod_index` */

DROP TABLE IF EXISTS `nns_vod_index`;

CREATE TABLE `nns_vod_index` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '分集名称',
  `nns_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '分集名称',
  `nns_vod_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '主媒资GUID',
  `nns_index` smallint(5) unsigned DEFAULT '0' COMMENT '分集号   0 开始',
  `nns_time_len` smallint(5) unsigned DEFAULT '0' COMMENT '分集时长（秒）',
  `nns_summary` varchar(4096) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '分集描述',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `nns_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '分集海报',
  `nns_play_count` int(10) unsigned DEFAULT '0' COMMENT '播放次数',
  `nns_score_total` int(10) unsigned DEFAULT '0' COMMENT '评分总数',
  `nns_score_count` int(10) unsigned DEFAULT '0' COMMENT '评分次数',
  `nns_import_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '分集注入id',
  `nns_import_source` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '注入来源',
  `nns_actor` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '演员',
  `nns_director` varchar(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '导演',
  `nns_new_media` char(10) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '最新片源',
  `nns_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '(add:1|modify:2|destroy:3|ok:0|execute:4)',
  `nns_new_media_time` datetime DEFAULT NULL COMMENT '新注入的片源时间',
  `nns_deleted` tinyint(3) unsigned DEFAULT '0' COMMENT '删除状态  0 未删除 | 1 已删除',
  `nns_action` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'add' COMMENT '操作行为 add|modify|destroy|ok|execute',
  `nns_release_time` datetime DEFAULT NULL COMMENT '发布时间',
  `nns_update_time` date DEFAULT NULL COMMENT '更新时间',
  `nns_integer_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增序列号',
  `nns_watch_focus` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '分集看点',
  `nns_state` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `nns_cp_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT 'CP_ID',
  `nns_conf_info` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '用于存放分集扩展配置信息',
  `nns_ext_url` varchar(2048) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '上游方注入url 和其他信息  json存储',
  `nns_ex_lang` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '扩展多语言',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `nns_integer_id` (`nns_integer_id`),
  KEY `index_vod_id` (`nns_vod_id`),
  KEY `index_vod_index` (`nns_index`),
  KEY `index_import_id` (`nns_import_id`,`nns_import_source`),
  KEY `nns_status_nns_action` (`nns_status`,`nns_action`),
  KEY `nns_vod_id_nns_index` (`nns_vod_id`,`nns_index`)
) ENGINE=InnoDB AUTO_INCREMENT=1236 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='最后操作动作';

/*Table structure for table `nns_vod_index_ex` */

DROP TABLE IF EXISTS `nns_vod_index_ex`;

CREATE TABLE `nns_vod_index_ex` (
  `nns_vod_index_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '分集扩展注入id',
  `nns_key` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_cp_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT 'cp id',
  PRIMARY KEY (`nns_vod_index_id`,`nns_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_vod_index_info_language` */

DROP TABLE IF EXISTS `nns_vod_index_info_language`;

CREATE TABLE `nns_vod_index_info_language` (
  `nns_vod_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_language` char(8) COLLATE utf8_unicode_ci NOT NULL,
  `nns_index` int(4) DEFAULT NULL,
  `nns_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_summary` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`nns_vod_id`,`nns_language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_vod_index_seekpoint` */

DROP TABLE IF EXISTS `nns_vod_index_seekpoint`;

CREATE TABLE `nns_vod_index_seekpoint` (
  `nns_id` char(32) NOT NULL,
  `nns_video_index` int(1) NOT NULL COMMENT '分集号',
  `nns_video_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '影片ID',
  `nns_type` int(1) NOT NULL COMMENT '0代表片中 1代表片头 2代表片尾',
  `nns_image` varchar(512) DEFAULT NULL COMMENT '图片地址',
  `nns_begin` int(1) DEFAULT NULL COMMENT '开始时间，秒',
  `nns_end` int(1) DEFAULT NULL COMMENT '结束时间，秒',
  `nns_name` varchar(512) DEFAULT NULL COMMENT '打点信息',
  PRIMARY KEY (`nns_id`),
  KEY `index_id` (`nns_video_index`,`nns_video_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `nns_vod_info_language` */

DROP TABLE IF EXISTS `nns_vod_info_language`;

CREATE TABLE `nns_vod_info_language` (
  `nns_vod_id` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_language` char(8) COLLATE utf8_unicode_ci NOT NULL,
  `nns_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `nns_director` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_actor` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_area` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_summary` varchar(4096) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_alias_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_vod_language` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_producer` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_screenwriter` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_play_role` varchar(4096) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_text_lang` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`nns_vod_id`,`nns_language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `nns_vod_media` */

DROP TABLE IF EXISTS `nns_vod_media`;

CREATE TABLE `nns_vod_media` (
  `nns_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID',
  `nns_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '片源名称',
  `nns_vod_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '主媒资GUID',
  `nns_vod_index_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '分集GUID',
  `nns_type` tinyint(3) unsigned DEFAULT '0',
  `nns_state` int(10) unsigned DEFAULT '0' COMMENT '状态',
  `nns_url` varchar(512) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '片源url地址',
  `nns_tag` char(128) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'tag标示',
  `nns_mode` varchar(12) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '清晰度',
  `nns_kbps` int(10) unsigned DEFAULT '0' COMMENT '码率',
  `nns_content_id` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '播放内容ID',
  `nns_content_state` tinyint(3) unsigned DEFAULT '0' COMMENT '内容状态',
  `nns_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `nns_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `nns_deleted` tinyint(3) unsigned DEFAULT '0' COMMENT '删除状态 0  未删除 | 1 已删除',
  `nns_check` tinyint(3) unsigned DEFAULT '1' COMMENT '审核状态',
  `nns_vod_index` smallint(5) unsigned DEFAULT '0' COMMENT '分集号  0开始',
  `nns_filetype` varchar(8) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '文件类型',
  `nns_play_count` int(4) unsigned DEFAULT '0' COMMENT '播放次数',
  `nns_score_total` int(8) unsigned DEFAULT '0' COMMENT '总分数',
  `nns_score_count` int(4) unsigned DEFAULT '0' COMMENT '评分次数',
  `nns_media_caps` varchar(16) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '影片 输出类型',
  `nns_import_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '片源注入id',
  `nns_import_source` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '注入来源',
  `nns_dimensions` char(8) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '片源类型 2D | 3D | 100001 | 100002 ',
  `nns_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '(add:1|modify:2|destroy:3|ok:0|execute:4)',
  `nns_action` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'add' COMMENT 'add|modify|destroy|ok|execute',
  `nns_ext_url` varchar(2048) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '上游方注入url 和其他信息  json存储',
  `nns_file_size` bigint(20) unsigned DEFAULT '0' COMMENT '文件大小',
  `nns_file_time_len` int(10) unsigned DEFAULT '0' COMMENT '播放时长（秒）',
  `nns_file_frame_rate` int(10) unsigned DEFAULT '0',
  `nns_file_resolution` varchar(12) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '分辨率',
  `nns_integer_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增序列号',
  `nns_cp_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT 'CPID',
  `nns_ext_info` varchar(2048) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '片源切片扩展信息,json串',
  `nns_media_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'ts文件名称',
  `nns_clip_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 原始状态 | 1 等待下载  | 2 正在下载 | 3 下载成功 | 4 下载失败 ',
  `nns_drm_enabled` tinyint(3) unsigned DEFAULT '0' COMMENT '是否DRM加密',
  `nns_drm_encrypt_solution` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '加密方案',
  `nns_drm_ext_info` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'drm扩展信息（json存储）',
  `nns_domain` varchar(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '上游注入的网络域',
  `nns_media_type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '片源类型,1点播片源2回看片源',
  `nns_original_live_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '频道原始id(点播片源时此字段无意义)',
  `nns_start_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '回看开始时间(点播片源时此字段无意义)',
  `nns_media_service` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '片源服务类型',
  `nns_conf_info` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '用于存放片源扩展配置信息',
  `nns_encode_flag` tinyint(3) unsigned DEFAULT '0' COMMENT '是否是转码生成的文件 0 否，1是',
  `nns_live_to_media` tinyint(3) unsigned DEFAULT '0' COMMENT '转码类型  0 点播 | 1直播转点播',
  `nns_media_service_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`nns_id`),
  UNIQUE KEY `nns_integer_id` (`nns_integer_id`),
  KEY `index_vod_id_index` (`nns_vod_id`,`nns_vod_index_id`),
  KEY `index_import_id` (`nns_import_id`,`nns_import_source`),
  KEY `nns_status_nns_action` (`nns_status`,`nns_action`),
  KEY `index_media_name` (`nns_media_name`) USING BTREE,
  KEY `index_name` (`nns_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='æœ€åŽæ“ä½œåŠ¨ä½œ';

/*Table structure for table `nns_vod_media_ex` */

DROP TABLE IF EXISTS `nns_vod_media_ex`;

CREATE TABLE `nns_vod_media_ex` (
  `nns_vod_media_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '片源扩展注入id',
  `nns_key` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `nns_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nns_cp_id` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT 'cp id',
  PRIMARY KEY (`nns_vod_media_id`,`nns_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `system_global_error` */

DROP TABLE IF EXISTS `system_global_error`;

CREATE TABLE `system_global_error` (
  `cms_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cms_stage` varchar(12) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '框架前后台',
  `cms_project` varchar(24) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '项目ID',
  `cms_model` varchar(24) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '模块名称',
  `cms_class` varchar(24) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '类名称',
  `cms_method` varchar(24) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '方法名称',
  `cms_error_type` varchar(12) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '错误消息类型',
  `cms_error_info` text COLLATE utf8_unicode_ci COMMENT '错误日志',
  `cms_error_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '错误文件位置',
  `cms_deleted` tinyint(3) unsigned DEFAULT '0' COMMENT '是否删除 0 未删除 | 1已删除',
  `cms_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `cms_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=307 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `system_global_params` */

DROP TABLE IF EXISTS `system_global_params`;

CREATE TABLE `system_global_params` (
  `cms_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'uuID',
  `cms_global_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '全局参数名称',
  `cms_global_model` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '全局参数模板',
  `cms_project_model` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '项目模板标示',
  `cms_config` text COLLATE utf8_unicode_ci NOT NULL COMMENT '配置json',
  `cms_create_time` datetime NOT NULL COMMENT '创建时间',
  `cms_modify_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `system_manager` */

DROP TABLE IF EXISTS `system_manager`;

CREATE TABLE `system_manager` (
  `cms_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'UUID',
  `cms_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '管理员名称',
  `cms_parent_manager` int(10) unsigned DEFAULT NULL COMMENT '父级管理员',
  `cms_login_account` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '管理员登录账号',
  `cms_login_pass` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '登录密码',
  `cms_role_id` int(10) unsigned NOT NULL COMMENT '角色uuid',
  `cms_login_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `cms_login_time` datetime NOT NULL COMMENT '登录时间',
  `cms_state` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '管理员状态  0 启用 | 1 禁用  默认禁用',
  `cms_deleted` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '0 未删除  | 1 已删除',
  `cms_create_time` datetime NOT NULL COMMENT '创建时间',
  `cms_modify_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `system_menu` */

DROP TABLE IF EXISTS `system_menu`;

CREATE TABLE `system_menu` (
  `cms_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'UUID',
  `cms_project_id` int(10) unsigned DEFAULT NULL COMMENT '项目ID',
  `cms_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cms_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cms_level` tinyint(3) unsigned DEFAULT '0' COMMENT '菜单层级 0 为1级，默认1级',
  `cms_parent_id` int(10) unsigned DEFAULT '0' COMMENT '父级菜单ID',
  `cms_order` float(10,3) unsigned DEFAULT '0.000' COMMENT '菜单排序权重',
  `cms_state` tinyint(3) unsigned DEFAULT '0' COMMENT '菜单状态  0 启用  1 禁用',
  `cms_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `cms_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `system_project` */

DROP TABLE IF EXISTS `system_project`;

CREATE TABLE `system_project` (
  `cms_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '项目ID',
  `cms_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '项目名称',
  `cms_mark` varchar(24) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '项目英文标示',
  `cms_mobilephone_number` char(11) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '手机号码',
  `cms_telphone_number` char(11) COLLATE utf8_unicode_ci DEFAULT '',
  `cms_remark` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '备注',
  `cms_order` float(6,3) unsigned DEFAULT '0.000' COMMENT '排序权重',
  `cms_state` tinyint(3) unsigned DEFAULT '0' COMMENT '状态 0 启用 | 1 禁用',
  `cms_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `cms_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`cms_id`),
  UNIQUE KEY `mark` (`cms_mark`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `system_project_class` */

DROP TABLE IF EXISTS `system_project_class`;

CREATE TABLE `system_project_class` (
  `cms_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'UUID',
  `cms_project_id` int(10) unsigned DEFAULT NULL COMMENT '项目ID',
  `cms_project_model_id` int(10) unsigned DEFAULT NULL COMMENT '模板ID',
  `cms_mark` varchar(24) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '类标示',
  `cms_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '类名称',
  `cms_state` tinyint(3) unsigned DEFAULT '0' COMMENT '状态 0 启用 | 1 禁用',
  `cms_remark` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '备注',
  `cms_order` float(6,3) unsigned DEFAULT '0.000' COMMENT '排序权重',
  `cms_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `cms_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`cms_id`),
  UNIQUE KEY `unique_project_mark_class` (`cms_project_id`,`cms_mark`,`cms_project_model_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `system_project_func` */

DROP TABLE IF EXISTS `system_project_func`;

CREATE TABLE `system_project_func` (
  `cms_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'UUID',
  `cms_project_id` int(10) unsigned DEFAULT NULL COMMENT '项目ID',
  `cms_project_model_id` int(10) unsigned DEFAULT NULL COMMENT '模板ID',
  `cms_project_class_id` int(10) unsigned DEFAULT NULL COMMENT '类ID',
  `cms_mark` varchar(24) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '方法标示',
  `cms_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '方法名称',
  `cms_state` tinyint(3) unsigned DEFAULT '0' COMMENT '状态 0 启用 | 1 禁用',
  `cms_remark` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '备注',
  `cms_order` float(6,3) unsigned DEFAULT '0.000' COMMENT '排序权重',
  `cms_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `cms_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`cms_id`),
  UNIQUE KEY `unique_project_mark_class_func` (`cms_project_id`,`cms_project_model_id`,`cms_project_class_id`,`cms_mark`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `system_project_model` */

DROP TABLE IF EXISTS `system_project_model`;

CREATE TABLE `system_project_model` (
  `cms_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'UUID',
  `cms_project_id` int(10) unsigned DEFAULT NULL COMMENT '项目ID',
  `cms_mark` varchar(24) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '模板标示',
  `cms_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '模板名称',
  `cms_state` tinyint(3) unsigned DEFAULT '0' COMMENT '状态 0 启用 | 1 禁用',
  `cms_remark` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '备注',
  `cms_order` float(6,3) unsigned DEFAULT '0.000' COMMENT '排序权重',
  `cms_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `cms_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`cms_id`),
  UNIQUE KEY `unique_project_mark` (`cms_project_id`,`cms_mark`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `system_project_model_1` */

DROP TABLE IF EXISTS `system_project_model_1`;

CREATE TABLE `system_project_model_1` (
  `cms_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'uuID',
  `cms_stage` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '前后台模板',
  `cms_stage_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '前后台模板名称',
  `cms_stage_desc` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '前后台模板描述',
  `cms_model` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '模块',
  `cms_model_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '模块名称',
  `cms_model_desc` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '模块描述',
  `cms_class` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '类标示',
  `cms_class_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '类名称',
  `cms_class_desc` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '类描述',
  `cms_method` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '方法',
  `cms_method_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '方法名称',
  `cms_method_desc` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '方法描述',
  `cms_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除 0 未删除 | 1已删除',
  `cms_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `cms_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

/*Table structure for table `system_project_url` */

DROP TABLE IF EXISTS `system_project_url`;

CREATE TABLE `system_project_url` (
  `cms_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'UUID',
  `cms_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'URL名称',
  `cms_project_id` int(10) unsigned DEFAULT NULL COMMENT '项目ID',
  `cms_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '项目路径',
  `cms_desc` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '描述',
  `cms_state` tinyint(3) unsigned DEFAULT '0' COMMENT '状态 0 启用 | 1 禁用',
  `cms_create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `cms_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `system_role` */

DROP TABLE IF EXISTS `system_role`;

CREATE TABLE `system_role` (
  `cms_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cms_project_id` int(10) unsigned NOT NULL COMMENT '项目id',
  `cms_role_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '角色名称',
  `cms_roles` text COLLATE utf8_unicode_ci NOT NULL COMMENT '角色权限（json）',
  `cms_desc` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '角色描述',
  `cms_create_time` datetime NOT NULL COMMENT '创建时间',
  `cms_modify_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
