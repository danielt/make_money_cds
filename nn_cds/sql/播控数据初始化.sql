-- ------------------------------------
-- 为避免自增量在拼接唯一数据ID标识时重复，请初始化自增数字的起始值
-- ------------------------------------
ALTER TABLE nns_vod AUTO_INCREMENT=1000000000;
ALTER TABLE nns_vod_index AUTO_INCREMENT=1000000000;
ALTER TABLE nns_vod_media AUTO_INCREMENT=1000000000;
ALTER TABLE nns_epg_file_set AUTO_INCREMENT=1000000000;
ALTER TABLE nns_epg_file AUTO_INCREMENT=1000000000;
-- ------------------------------------
-- 初始化播控数据库
-- ------------------------------------
truncate nns_vod;
truncate nns_vod_ex;
truncate nns_vod_index;
truncate nns_vod_index_ex;
truncate nns_vod_media;
truncate nns_vod_media_ex;
truncate nns_mgtvbk_message;
truncate nns_mgtvbk_c2_task;
truncate nns_mgtvbk_import_log;
truncate nns_mgtvbk_import_op;
truncate nns_mgtvbk_op_log;
truncate nns_mgtvbk_op_queue;
truncate nns_video_label;
truncate nns_video_label_bind_1000;
truncate nns_video_label_bind_1001;
truncate nns_video_label_bind_1002;
truncate nns_video_label_bind_1003;
truncate nns_video_label_bind_1004;
truncate nns_video_label_bind_1005;
truncate nns_video_label_bind_1006;
truncate nns_video_label_bind_1007;
truncate nns_video_label_bind_1008;
truncate nns_video_label_bind_1009;
truncate nns_video_label_bind_1010;
truncate nns_video_label_bind_1011;
truncate nns_mgtvbk_clip_task;
truncate nns_mgtvbk_clip_task_log;
truncate nns_mgtvbk_import_epg_log;
truncate nns_mgtvbk_c2_log;
truncate nns_epg_file_set;
truncate nns_epg_file;
truncate nns_mgtvbk_file_task;
truncate nns_mgtvbk_file_log;

-- ------------------------------------
-- 初始化通用配置 模块与键值
-- ------------------------------------
-- ------------------------------------
-- 通用配置模块 Records of nns_project_model
-- ------------------------------------
INSERT INTO `nns_project_model` (`nns_id`, `nns_name`, `nns_desc`, `nns_create_time`, `nns_modify_time`) VALUES
(1,	'消息模块',	'针对接收上游CP消息与消息生成注入队列的配置',	now(),	now()),
(2,	'中心模块',	'处理中心注入队列与中心同步队列的配置',	now(),	now()),
(3,	'切片模块',	'处理切片队列模块的配置',	now(),	now()),
(4,	'CDN模块',	'处理CDN注入队列模块的配置',	now(),	now()),
(5,	'EPG模块',	'处理EPG注入队列模块的配置',	now(),	now());	
-- ------------------------------------
-- 湖南电信初始化通用配置
-- ------------------------------------
INSERT INTO `nns_project_key` (`nns_id`, `nns_name`, `nns_model_id`, `nns_key`, `nns_key_type`, `nns_key_value`, `nns_is_more`, `nns_example`, `nns_desc`, `nns_create_time`, `nns_modify_time`) VALUES
(1,	'消息生成注入任务指定SP',	1,	'import_cdn_mode',	0,	'',	0,	'zte/hw',	'消息生成注入队列时，按注入标识指定SP进行注入',	'2017-03-07 14:34:17',	'2017-03-07 14:34:17'),
(2,	'消息反馈地址(SP使用)',	1,	'import_notify_url',	0,	'[]',	0,	'http://127.0.0.1/notify_url',	'指定SP反馈地址（与CP的反馈地址配置不同）',	'2017-03-07 14:34:17',	'2017-03-10 14:52:38'),
(3,	'注入HVS携带播放内容ID',	5,	'media_import_core_mode',	1,	'[\"\\u4e0d\\u643a\\u5e26\",\"\\u643a\\u5e26\\u539f\\u59cbID\",\"\\u643a\\u5e26CDN\\u6ce8\\u5165ID\"]',	0,	'',	'当注入HVS携带播放内容ID，0不携带、1携带原始ID、2携带CDN注入ID',	'2017-03-07 14:34:17',	'2017-03-07 14:34:17'),
(4,	'站点ID(SP使用)',	1,	'import_notify_site_id',	0,	'[]',	0,	'',	'芒果二级媒资专用站点ID',	'2017-03-10 11:24:45',	'2017-03-10 13:16:46'),
(5,	'消息异步反馈站点(中兴)',	1,	'message_notify_site_id_zte',	0,	'[]',	0,	'',	'芒果湖南电信二级媒资专用CP消息反馈',	'2017-03-10 13:20:13',	'2017-03-10 14:42:41'),
(6,	'消息异步反馈地址(中兴)',	1,	'message_notify_url_zte',	0,	'[]',	0,	'',	'芒果湖南电信二级媒资CP消息专用',	'2017-03-10 13:21:29',	'2017-03-10 14:51:36'),
(7,	'消息异步反馈站点(华为)',	1,	'message_notify_site_id_hw',	0,	'[]',	0,	'',	'芒果湖南电信二级媒资专用CP消息反馈',	'2017-03-10 13:20:13',	'2017-03-10 14:42:41'),
(8,	'消息异步反馈地址(华为)',	1,	'message_notify_url_hw',	0,	'[]',	0,	'',	'芒果湖南电信二级媒资CP消息专用',	'2017-03-10 13:21:29',	'2017-03-10 14:51:29');

INSERT INTO `nns_project_group` (`nns_id`, `nns_name`, `nns_key_id`, `nns_value`, `nns_desc`, `nns_create_time`, `nns_modify_time`) VALUES
(1,	'湖南电信-中兴',	'1,2',	'{\"import_cdn_mode\":\"zte\",\"import_notify_url\":\"http:\\/\\/10.1.172.45:80\\/cmsfeedback\\/SubCmsApi\\/Feedback.action\"}',	'用于中兴SP',	'2017-03-02 14:51:50',	'2017-03-10 14:53:38'),
(2,	'湖南电信-华为',	'1,4,2',	'{\"import_cdn_mode\":\"hw\",\"import_notify_site_id\":\"bc91cbce36c04fa097bc274821ed949c\",\"import_notify_url\":\"http:\\/\\/10.1.172.45:80\\/cmsfeedback\\/SubCmsApi\\/Feedback.action\"}',	'用于华为SP',	'2017-03-06 00:38:56',	'2017-03-10 14:53:16'),
(3,	'内容注入CP配置',	'6,5,7,8',	'{\"message_notify_url_zte\":\"http:\\/\\/10.1.172.45:80\\/cmsfeedback\\/SubCmsApi\\/Feedback.action\",\"message_notify_site_id_zte\":\"5f925bad6d9847aa9ab65b09d6370e58\",\"message_notify_site_id_hw\":\"bc91cbce36c04fa097bc274821ed949c\",\"message_notify_url_hw\":\"http:\\/\\/10.1.172.45:80\\/cmsfeedback\\/SubCmsApi\\/Feedback.action\"}',	'CP消息通用配置',	'2017-03-10 12:08:25',	'2017-03-10 14:52:12');


