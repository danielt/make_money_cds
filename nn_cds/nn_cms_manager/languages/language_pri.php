<?php
/*
 * Created on 2012-3-5
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 function get_pri_language($code){
	$return_str="";
	switch($code){
		case "100":
			$return_str=cms_get_lang('pri_xxgl');
		break;
		case "101":
			$return_str=cms_get_lang('pri_jsgl');
		break;
		case "102":
			$return_str=cms_get_lang('pri_glygl');
		break;
		case "103":
			$return_str=cms_get_lang('pri_hzhbgl');
		break;
		case "104":
			$return_str=cms_get_lang('pri_jtgl');
		break;
		case "105":
			$return_str=cms_get_lang('pri_lmgl');
		break;
		case "106":
			$return_str=cms_get_lang('pri_dbgl');
		break;
		case "107":
			$return_str=cms_get_lang('pri_zbgl');
		break;
		case "100100":
			$return_str=cms_get_lang('pri_xtgly');
		break;
		case "101100":
			$return_str=cms_get_lang('pri_add_role');
		break;
		case "101101":
			$return_str=cms_get_lang('pri_modify_role');
		break;
		case "101102":
			$return_str=cms_get_lang('pri_del_role');
		break;
		case "101103":
			$return_str=cms_get_lang('pri_show_role');
		break;
		case "102100":
			$return_str=cms_get_lang('pri_add_admin_user');
		break;
		case "102101":
			$return_str=cms_get_lang('pri_modify_admin_user');
		break;
		case "102102":
			$return_str=cms_get_lang('pri_del_admin_user');
		break;
		case "102103":
			$return_str=cms_get_lang('pri_show_admin_user');
		break;
		case "103100":
			$return_str=cms_get_lang('pri_add_partner');
		break;
		case "103101":
			$return_str=cms_get_lang('pri_modify_partner');
		break;
		case "103102":
			$return_str=cms_get_lang('pri_del_partner');
		break;
		case "103103":
			$return_str=cms_get_lang('pri_show_partner');
		break;
		case "104100":
			$return_str=cms_get_lang('pri_add_group');
		break;
		case "104101":
			$return_str=cms_get_lang('pri_modify_group');
		break;
		case "104102":
			$return_str=cms_get_lang('pri_del_group');
		break;
		case "104103":
			$return_str=cms_get_lang('pri_show_group');
		break;
		case "105100":
			$return_str=cms_get_lang('pri_add_lm');
		break;
		case "105101":
			$return_str=cms_get_lang('pri_modify_lm');
		break;
		case "105102":
			$return_str=cms_get_lang('pri_del_lm');
		break;
		case "105103":
			$return_str=cms_get_lang('pri_show_lm');
		break;
		case "106100":
			$return_str=cms_get_lang('pri_del_lm');
		break;
		case "106101":
			$return_str=cms_get_lang('pri_add_vod');
		break;
		case "106102":
			$return_str=cms_get_lang('pri_modify_vod');
		break;
		case "106103":
			$return_str=cms_get_lang('pri_del_vod');
		break;
		case "106104":
			$return_str=cms_get_lang('pri_show_vod');
		break;
		case "107100":
			$return_str=cms_get_lang('pri_add_live');
		break;
		case "107101":
			$return_str=cms_get_lang('pri_modify_live');
		break;
		case "107102":
			$return_str=cms_get_lang('pri_del_live');
		break;
		case "107103":
			$return_str=cms_get_lang('pri_show_live');
		break;
		case "107104":
			$return_str=cms_get_lang('pri_audit_live');
		break;
		case "108":
			$return_str=cms_get_lang('pri_device_manage');
		break;
		case "108103":
			$return_str=cms_get_lang('pri_show_device');
		break;
		case "108100":
			$return_str=cms_get_lang('pri_add_device');
		break;
		case "108101":
			$return_str=cms_get_lang('pri_modify_device');
		break;
		case "108102":
			$return_str=cms_get_lang('pri_del_device');
		break;
		case "109":
			$return_str=cms_get_lang('pri_product_manage');
		break;
		case "109103":
			$return_str=cms_get_lang('pri_show_product');
		break;
		case "109100":
			$return_str=cms_get_lang('pri_add_product');
		break;
		case "109101":
			$return_str=cms_get_lang('pri_modify_product');
		break;
		case "109102":
			$return_str=cms_get_lang('pri_del_product');
		break;
		case "110":
			$return_str=cms_get_lang('pri_service_manage');
		break;
		case "110103":
			$return_str=cms_get_lang('pri_show_service');
		break;
		case "110100":
			$return_str=cms_get_lang('pri_add_service');
		break;
		case "110101":
			$return_str=cms_get_lang('pri_modify_service');
		break;
		case "110102":
			$return_str=cms_get_lang('pri_del_service');
		break;
		case "111":
			$return_str=cms_get_lang('pri_assists_manage');
		break;
		case "111103":
			$return_str=cms_get_lang('pri_show_assists');
		break;
		case "111100":
			$return_str=cms_get_lang('pri_add_assists');
		break;
		case "111101":
			$return_str=cms_get_lang('pri_modify_assists');
		break;
		case "111102":
			$return_str=cms_get_lang('pri_del_assists');
		break;
		case "112":
			$return_str=cms_get_lang('pri_ipqam_manage');
		break;
		case "112103":
			$return_str=cms_get_lang('pri_show_ipqam');
		break;
		case "112100":
			$return_str=cms_get_lang('pri_add_ipqam');
		break;
		case "112101":
			$return_str=cms_get_lang('pri_modify_ipqam');
		break;
		case "112102":
			$return_str=cms_get_lang('pri_del_ipqam');
		break;
		case "113":
			$return_str=cms_get_lang('pri_tpl_manage');
		break;
		case "113101":
			$return_str=cms_get_lang('pri_tpl_manage');
		break;
		case '113102':
			$return_str=cms_get_lang('xtgl_bqgl');
		break;
		case "114":
			$return_str=cms_get_lang('pri_stat_chart_manage');
		break;
		case "114101":
			$return_str=cms_get_lang('pri_show_stat_chart_manage');
		break;
		case "115":
			$return_str=cms_get_lang('pri_show_stat_chart_manage');
		break;
		case "115101":
			$return_str=cms_get_lang('pri_show_stat_chart_manage');
		break;
		case "116":
			$return_str=cms_get_lang('pri_keyword_manage');
		break;
		case "116100":
			$return_str=cms_get_lang('pri_keyword_edit');
		break;
		case "116101":
			$return_str=cms_get_lang('pri_keyword_list');
		break;
		case "117":
			$return_str=cms_get_lang('ad_manager');
		break;
		case "117100":
			$return_str=cms_get_lang('pri_ad_pos_edit');
		break;
		case "117101":
			$return_str=cms_get_lang('pri_ad_content_edit');
		break;
		case "117102":
			$return_str=cms_get_lang('pri_ad_policy_edit');
		break;
		case "117103":
			$return_str=cms_get_lang('pri_ad_list');
		break;
		//added by tom yuliang.xia@starcorcn.com 2012-09-29 09:55:42
		case "118":
			$return_str=cms_get_lang('pri_serve_manage');
		break;
		case "118101":
			$return_str=cms_get_lang('pri_msg_serve_view');
		break;
		case "118102":
			$return_str=cms_get_lang('pri_msg_edit');
		break;
		case "118103":
			$return_str=cms_get_lang('pri_msg_del');
		break;
		case '121':
			$return_str=cms_get_lang('topic_xxbgl');
		break;
		case '121100':
			$return_str=cms_get_lang('pri_add_info_package');
		break;
		case '121101':
			$return_str=cms_get_lang('topic_xxbgl_edit');
		break;
		case '121102':
			$return_str=cms_get_lang('pri_del_info_package');
		 break;
		 case '121103':
			$return_str=cms_get_lang('pri_add_info_package_column');
		 break;
		 case '121104':
			$return_str=cms_get_lang('pri_modify_info_package_column');
		 break;
		 case '121105':
			$return_str=cms_get_lang('pri_del_info_package_column');
		 break;
		 case '121106':
			$return_str=cms_get_lang('pri_info_package_column_order');
		  break;
		  case '121107':
			$return_str=cms_get_lang('pri_add_info_item');
		  break;
		  case '121108':
			$return_str=cms_get_lang('pri_modify_info_item');
		  break;
		  case '121109':
			$return_str=cms_get_lang('pri_del_info_item');
		  break;
		   case '121110':
			$return_str=cms_get_lang('pri_audit_info_item');
		  break;
		   case '121111':
			$return_str=cms_get_lang('pri_order_info_item');
		  break;
		   case '121112':
			$return_str=cms_get_lang('pri_move_info_item');
		  break;
		   case '122':
			$return_str=cms_get_lang('zdgl_yhml');
			case '122101':
			$return_str=cms_get_lang('');
		  break;
		default:
			if (substr($code,-3,3)=="001"){
				$return_str=cms_get_lang('pri_module_invisible');
			}
		break;
	}

	return $return_str;
 }

?>
