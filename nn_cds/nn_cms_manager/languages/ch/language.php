<?php

/*
 * Created on 2012-3-4
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
$language_manager_company="视达科科技有限公司";
$language_manager_titile = "全媒体管理系统";
$language_manager_help = "帮助";
$language_manager_quit = "退出";
$language_manager_xtgl = "系统配置";
$language_manager_log_limit = "播放日志保留天数";
$language_manager_lmgl = "栏目管理";
$language_manager_qxgl = "系统配置";
$language_manager_zdgl = "终端管理";
$language_manager_dbgl = "点播管理";
$language_manager_bkgl = "板块服务包";
$language_manager_bkgllb = "板块服务包列表";
$language_manager_bkgl_bk = "服务包";
$language_manager_bkgl_owner = "被服务包引用";
$language_manager_bkgl_bklb = "板块元数据管理";
$language_manager_zbgl = "直播管理";
$language_manager_jmdgl = "节目单管理";
$language_manager_xtgl_yysgl = "平台运营商";
$language_manager_xtgl_cmsgl = "CMS配置管理";
$language_manager_xtgl_jsgl = "角色管理";
$language_manager_xtgl_hzgl = "CP/SP管理";
$language_manager_xtgl_jtgl = "集团管理";
$language_manager_xtgl_glygl = "管理员管理";
$language_manager_cpgl = "产品计费包";
$language_manager_cpgl_cp = "产品包";
$language_manager_cpgl_zf = "资费包";
$language_manager_cpgl_zflx="资费类型";
$language_manager_cpgl_zflb = "资费包列表";
$language_manager_cpgl_bdcp = "绑定内容";
$language_manager_cpgl_cplb = "产品包列表";
$language_manager_cpgl_cpshex = "生效日期";
$language_manager_cpgl_cpshix = "失效日期";
$language_manager_cpgl_bkjl = "播控记录（片数）";
$language_manager_cpgl_sfsx = "是否失效";
$language_manager_cpgl_cp_id = "产品包ID";
$language_manager_cpgl_cpmc = "产品包名称";
$language_manager_cpgl_cpjg = "产品包价格(单位:元)";
$language_manager_cpgl_jffs = "计费方式";
$language_manager_cpgl_sjzq = "时间周期(天)";
$language_manager_cpgl_sqcpb = "授权产品包";
$language_manager_cpgl_sqyps = "授权数";
$language_manager_cpgl_cpzylx = "产品包资源类型";
$language_manager_cpgl_cpjgfs0 = "按次计费";
$language_manager_cpgl_cpjgfs1 = "包月计费";
$language_manager_cpgl_cpjgfs2 = "包年计费";
$language_manager_cpgl_cpjgfs3 = "按周期计费";
$language_manager_cpgl_cpjgfs4 = "按影片数计费";

$language_manager_global_pt_jkpz="平台接口地址";
$language_manager_global_hxpt_jkpz="MSP接口配置";
$language_manager_global_hxpt_rk="平台入口";
$language_manager_global_hxpt_dlm="平台登录名";
$language_manager_global_hxpt_mm="平台登录密码";
$language_manager_global_hxpt_dd="调度服务器地址";

//0为全部影片，1为所有直播影片，2为所有点播影片，3为按栏目绑定，4为按直播影片绑定，5为按直播片源绑定，6为按点播影片绑定，7为按点播片源绑定，

$language_manager_cpgl_bdyp0 = "绑定全部内容";
$language_manager_cpgl_bdyp1 = "绑定全部直播";
$language_manager_cpgl_bdyp2 = "绑定全部点播";
$language_manager_cpgl_bdyp3 = "按栏目绑定";
$language_manager_cpgl_bdyp4 = "按直播影片绑定";
$language_manager_cpgl_bdyp5 = "按直播片源绑定";
$language_manager_cpgl_bdyp6 = "按点播影片绑定";
$language_manager_cpgl_bdyp7 = "按点播片源绑定";

$language_manager_xtgl_mbgl = "模板管理";
$language_manager_xtgl_xgmm = "修改密码";
$language_manager_yhm = "用户名";
$language_manager_yhlx = "用户等级类型";
$language_manager_yhgl = "用户管理";

$language_manager_zdgl_sbidh = "设备ID号";
$language_manager_zdgl_sbgl = "设备管理";
$language_manager_zdgl_yhgl = "用户管理";
$language_manager_zdgl_sblb = "设备列表";
$language_manager_zdgl_yhfz = "用户分组";
$language_manager_zdgl_yhml = "用户栏目";
$language_manager_zdgl_yhlb = "用户列表";
$language_manager_zdgl_sb = "设备";
$language_manager_zdgl_yh = "用户";
$language_manager_zdgl_sbmc = "设备名称";
//   $language_manager_zdgl_yhmc="用户名称";
$language_manager_zdgl_bdyh = "绑定用户";
$language_manager_zdgl_bdcp = "绑定产品";
$language_manager_zdgl_sbmc = "设备名称";
$language_manager_zdgl_zdlx = "终端类型";
$language_manager_zdgl_zdlxgl = "终端类型管理";
$language_manager_zdgl_zdlxlb = "终端类型列表";
$language_manager_zdgl_zdlxmc = "终端类型名称";
$language_manager_zdgl_zdpt = "输出平台";
$language_manager_zdgl_zdscbj = "输出EPG分组";
$language_manager_zdgl_bbh = "版本号";
$language_manager_zdgl_sjsc = "升级包上传";

$language_manager_xtgl_cmshtpz = "CMS后台配置";
$language_manager_xtgl_qdmbscpz = "前端输出配置";

$language_manager_lmgl_lmmbgl = "栏目模版管理";
$language_manager_lmgl_lmmbgl_yys = "运营商栏目模版";
$language_manager_lmgl_lmmbgl_hzhb = "合作伙伴栏目模版";
$language_manager_lmgl_lmmbgl_jt = "集团栏目模版";
$language_manager_lmgl_nrlmgl = "栏目内容管理";
$language_manager_lmgl_lmmb = "栏目模版";
$language_manager_lmgl_nrlm = "栏目内容";
$language_manager_lmgl_lmmc = "栏目名称";
$language_manager_lmgl_nrlmid = "栏目id";
$language_manager_lmgl_tjzlm = "子栏目";
$language_manager_lmgl_3wszbm = "(三位数字编码)";

$language_manager_jggl_dqjg = "当前机构";
$language_manager_jggl_xzjg = "选择机构";

$language_manager_mbgl_mblx = "模板类型ID";
$language_manager_mbgl_mbys = "模板ID";
$language_manager_mbgl_mbmc = "模板名称";
$language_manager_mbgl_mblb = "模板列表";
$language_manager_mbgl_ckts = "查看图片";

$language_manager_lmgl_lmlbgl_yys = "运营商栏目内容";
$language_manager_lmgl_lmlbgl_hzhb = "合作伙伴栏目内容";
$language_manager_lmgl_lmlbgl_jt = "集团栏目内容";

$language_manager_lmgl_sjlm = "上级栏目";
$language_manager_lmgl_gmu = "根目录";

$language_manager_lmgl_lmnrmc = "栏目内容名称";
$language_manager_lmnr_msg_noselect = "请选择一条栏目内容！";
$language_manager_lmgl_bdyhml = "绑定用户栏目";

$language_manager_lmgl_lmmb = "栏目模版";
$language_manager_lmgl_lmmblx = "栏目模版类型";
$language_manager_lmgl_zblm = "直播栏目";
$language_manager_lmgl_dblm = "点播栏目";
$language_manager_lmgl_lmmbmc = "栏目模版名称";
$language_manager_lmgl_msg_noselect = "请选择一条栏目模版！";

// $language_manager_sbgl="设备管理";
// $language_manager_zbgl="直播管理";
// $language_manager_dbgl="点播管理";
// $language_manager_jfgl="计费管理";

$language_manager_str_group = "集团";
$language_manager_str_partner = "合作伙伴";

$language_manager_lxzc = "联系支持";
$language_manager_gywm = "关于".$language_manager_company."公司";
$language_manager_tktj = "条款及条件";
$language_manager_ys = "隐私";

$language_manager_dlsj = "上次登录时间";
$language_action_lock = "锁定";
$language_action_no_lock = "未锁定";
$language_action_unlock = "解锁";
$language_action_action = "操作";
$language_action_binded = "已绑定";
$language_action_unbinded = "未绑定";
$language_action_success = "成功";
$language_action_fault = "失败";
$language_action_move_category = "转移栏目";
$language_action_add = "添加";
$language_action_delete = "删除";

$language_action_add_assist = "内容上线";
$language_action_delete_assist = "内容下线";

$language_action_real_delete = "彻底删除";
$language_action_edit = "修改";
$language_action_state = "状态";
$language_action_select = "全选";
$language_action_cancel = "取消";
$language_action_audit = "审核";
$language_action_confirm = "确定";
$language_action_search = "查询";
$language_action_back = "返回";
$language_action_upload = "上传";
$language_action_reset = "恢复";
$language_action_bind = "绑定";
$language_action_nobind = "不绑定";
$language_manager_global_kq = "开启";
$language_manager_global_bkq = "不开启";
$language_action_search_mhcx = "模糊查询";
$language_action_search_xz = "选择";
$language_action_search_bx = "不限";
$language_action_search_yx = "有效";
$language_action_search_sx = "失效";
$language_action_search_yjyx = "永久有效";
$language_action_point = "分";
$language_action_yes = "是";
$language_action_no = "否";
$language_action_day = "日";
$language_action_month = "月";
$language_action_advance = "高级";
$language_action_year = "年";
$language_action_perpagenum = "分页条数";

$language_msg_ask_add = "是否进行本次添加？";
$language_msg_ask_edit = "是否进行本次修改？";
$language_msg_ask_delete = "是否进行本次删除？";
$language_msg_ask_offline = "是否确定下线？";
$language_msg_ask_search = "是否进行本次查询？";
$language_msg_ask_audit = "是否进行本次审核？";
$language_msg_ask_reset = "是否进行本次恢复？";
$language_msg_force_xx = "是否强行下线该影片！";
$language_msg_carrier = "未配置平台运营商，请配置平台运营商！";

$language_manager_global_xtdz = "系统地址";
$language_manager_global_hcsj = "缓存过期时间（单位：分钟）";
$language_manager_global_hc = "缓存";
$language_manager_global_jlts = "每页显示记录条数";
$language_manager_global_yysmc = "运营商名称";
$language_manager_global_ms = "描述";
$language_manager_global_yyb = "语言";
$language_manager_global_yzm = "验证码";
$language_manager_global_zdlx = "终端类型";

$language_manager_group_jtmc = "集团名称";
$language_manager_group_jtxx = "集团";
$language_manager_group_jtxx_xx = "集团信息";
$language_manager_group_jtlb = "集团列表";
$language_manager_group_msg_noselect = "请选择一条集团信息！";

$language_manager_partner_hzhbmc = "合作伙伴名称";
$language_manager_partner_hzhbxx = "合作伙伴";
$language_manager_partner_hzhblb = "合作伙伴列表";
$language_manager_partner_hzhbxx_xx = "合作伙伴信息";
$language_manager_partner_msg_noselect = "请选择一条合作伙伴信息！";

$language_manager_table_role_name = "角色名称";
$language_manager_role_jsxx = "角色信息";
$language_manager_role_msg_noselect = "请选择一条角色信息！";

$language_manager_manager_glyxx = "管理员信息";
$language_manager_manager_msg_noselect = "请选择一条管理员信息！";

$language_manager_table_name = "名称";
$language_manager_table_title = "标题";
$language_manager_table_create_time = "创建时间";
$language_manager_table_edit_time = "修改时间";
$language_manager_table_edit_role = "角色";

$language_manager_table_pass = "密码";
$language_manager_table_oldpass = "旧密码";
$language_manager_table_newpass = "新密码";
$language_manager_table_pass_confirm = "密码确认";

$language_manager_table_role_pri = "角色权限";
$language_manager_table_role_manager = "隶属管理员个数";
$language_manager_table_manager_id = "操作帐号";
$language_manager_table_manager_name = "管理员昵称";
$language_manager_table_manager_login = "管理员登录";
$language_manager_table_group = "隶属机构";
$language_manager_table_login_time = "最近登录时间";
$language_manager_table_login_num = "登录次数";
$language_manager_table_segnumber = "序号";
$language_manager_table_lxr = "联系人";
$language_manager_table_lxdh = "联系电话";
$language_manager_table_email = "Email";
$language_manager_table_lxdz = "地址";
$language_manager_table_nc = "昵称";
$language_manager_table_order = "排序";
$language_manager_table_order_up = "置顶";
$language_manager_table_order_down = "置尾";
$language_manager_table_order_pre = "向上";
$language_manager_table_order_next = "向下";
$language_manager_table_zh = "用户ID";
$language_manager_table_ID = "ID";
$language_manager_table_bind_device_id = "BIND设备ID";

$language_manager_table_first_page = "首页";
$language_manager_table_last_page = "末页";
$language_manager_table_next_page = "下一页";
$language_manager_table_pre_page = "上一页";
$language_manager_table_page = "页";
$language_manager_table_current = "当前";
$language_manager_table_total = "总共";
$language_manager_table_jump_to = "跳转到第";


$language_manager_js_msg_1 = "两次输入密码不一致！";
$language_manager_js_msg_2 = "所填内容不能为空!";
$language_manager_js_msg_3 = "所填内容必须为整数!";
$language_manager_js_msg_4 = "请先选择一条数据！";
$language_manager_js_msg_5 = "您没有操作该模块的权限！";
$language_manager_js_msg_6 = "电话号码格式不正确！";
$language_manager_js_msg_7 = "email格式不正确！";
$language_manager_js_msg_9 = "必须输入3位字符！";
$language_manager_js_msg_8 = "栏目列表为空！";
$language_manager_js_msg_10 = "ID已存在！";
$language_manager_js_msg_11 = "失效日期必须大于生效日期！";
$language_manager_js_msg_12 = "栏目内有数据！";
$language_manager_js_msg_13 = "所填内容必须为大于0的正整数！";
//$language_manager_js_msg_12 = "影片已存在！";
$language_media_lm_bslm = "不设栏目";
$language_manager_lmlb="栏目列表";

$language_media_addedit_ysx = "已上线";
$language_media_addedit_yxx = "未上线";//下线
$language_media_addedit_xiax = "下线";//下线
$language_media_addedit_ysh = "已审核";
$language_media_addedit_ddsh = "待审核";
$language_action_access_audit = "通过审核";
$language_action_deleted = "已删除";

$language_service_bknrgl = "板块内容管理";
$language_service_fengmian = "封面";
$language_service_tpnr = "图片内容";
$language_service_czykxzsp = "从资源库选择视频";
$language_service_sp = "视频";
$language_service_mlsgid = "目录树根ID";
$language_service_bdgdmt = "绑定更多媒体";
$language_service_bdgdtp = "绑定更多图片";
$language_service_qhpk = "切换片库";
$language_service_pklx = "片库类型";
$language_service_pk = "片库";
$language_service_syzy = "所有资源";
$language_service_dshzy = "待审核资源";
$language_service_ysczy = "回收站";

$language_media_zykgl = "资源库管理";
$language_media_zyklm = "资源库栏目";
$language_media_zykflgl = "资源库分类管理";
$language_media_gylm = "公有栏目";
$language_media_sylm = "私有栏目";
$language_media_daoyan = "导演";
$language_media_yy = "演员";
$language_media_sysj = "上映时间";
$language_media_playbill_kssj = "开始时间";
$language_media_playbill_drjmd = "导入节目单";
$language_media_playbill_scjmd = "删除节目单";
$language_media_playbill_dcjmd = "导出节目单";
$language_media_playbill_jmdlb = "节目单列表";
$language_media_playbill_jmdrq = "节目单日期";
$language_media_yp_xx = "影片信息";

$language_media_playbill_drms = "导入模式";
$language_media_playbill_jmmc = "节目名称";
$language_media_playbill_import = "导入";
$language_media_playbill_export = "导出";
$language_media_playbill_single_dr = "单天导入";
$language_media_playbill_more_dr = "多天导入";
$language_media_playbill_select_date = "选择日期";
$language_media_playbill_jmdwj = "节目单文件";
$language_media_mjpc = "每集时长(分钟)";
$language_media_zsc = "时长(分钟)";
$language_media_js = "集数";
$language_media_fj = "分集";
$language_media_fps = "分片数";
$language_media_fp = "分片";
$language_media_zjs = "总集数";
$language_media_dqgx = "当前更新";
$language_media_nrjj = "内容简介";
$language_media_dq = "上映地区";
$language_media_dy = "电影";
$language_media_zb = "直播";
$language_media_db = "点播";
$language_media_lxj = "连续剧";
$language_media_dylb = "电影列表";
$language_media_lxjlb = "连续剧列表";
$language_media_zblb = "直播列表";
$language_media_search_zbcx = "直播查询";
$language_media_zbpdmc = "直播频道名称";
$language_media_mtmc = "媒体文件名称";
$language_media_zbymc = "直播源名称";
$language_media_mtlx = "媒体文件类型";
$language_media_index_jj = "简介";
$language_media_index_sc = "时长";
$language_media_control_bjjj = "分集描述";
$language_media_control_bjzbjj = "编辑直播描述";
$language_media_control_tjmt = "添加媒体文件";
$language_media_control_tjzby = "添加直播源";
$language_media_addedit_pymc = "片源名称";
$language_media_addedit_pydz = "片源URL地址";
$language_media_addedit_pyml = "片源码率(kbps)";
$language_media_addedit_mtid = "媒体ID";
$language_media_addedit_pylx = "片源类型";
$language_media_addedit_pylx = "清晰度";
$language_media_addedit_tjfs = "推荐分数";
$language_media_addedit_copyright_date = "版权过期时间";

$language_media_addedit_bjhb = "海报";
$language_media_addedit_slt = "缩略图";
$language_media_addedit_pictrue = "海报";
$language_media_addedit_big = "大";
$language_media_addedit_normal = "中";
$language_media_addedit_small = "小";
$language_action_search_ypcx = "影片查询";

$language_media_addedit_msg_select_dy = "请选择一部未上线电影！";
$language_media_addedit_msg_select_lxj = "请选择一部未上线连续剧！";
$language_media_addedit_msg_select_cp = "请选择一条产品信息！";
$language_media_addedit_msg_select_jm = "请选择一部节目！";
$language_media_addedit_msg_select_jmd = "请选择一个节目单！";
$language_media_mtwjgl = "媒体文件管理";
$language_media_mt = "媒体文件";
$language_media_zbygl = "直播源管理";
$language_media_zbfl = "直播分类";
$language_media_dbfl = "点播分类";

$language_manager_assist = "媒资包";
$language_manager_assist_owner = "被媒资包引用";
$language_manager_assist_gl = "媒资包管理";
$language_manager_assist_nrgl = "媒资包内容管理";
$language_manager_assist_lb = "媒资包列表";
$language_manager_assist_bdnr = "绑定媒资包内容";

$language_manager_service_bdnr = "绑定服务包内容";

$language_epg_n1_a_addr = "前端N1-A入口";
$language_epg_n6_a_addr = "前端N6-A入口";
$language_epg_n1_b_addr = "前端N1-B入口";
$language_epg_cant_modify = "不可修改";
$language_epg_string      = 'EPG';

$language_media_addedit_pygs = "片源格式";
$language_media_addedit_bdcl = "绑定策略";
$language_media_addedit_pymtid = "片源媒体ID";
$language_media_addedit_tjpyfs = "添加片源方式";
$language_media_addedit_new_media = "新增片源";
$language_media_addedit_bind_media = "片源管理";

$language_media_addedit_exist_media="绑定已有片源";
$language_media_addedit_root_skin= "开机皮肤包";
$language_media_select_box="媒体选择框";
$language_play_count="播放次数";
$language_export_csv="导出csv数据";
$language_system_config_rzgl="日志管理";
$language_syytem_config_rz="日志";
$language_action_log_type="操作类型";
$language_action_log_detail="操作详情";
$language_action_log_time= "操作时间";
$language_product_fee_gl= "资费管理";

$language_ipqam_gl= "IPQAM管理";
$language_ipqam_glip= "管理IP";
$language_ipqam_backupglip= "备用管理IP";
$language_ipqam_dqm= "区域码";
$language_ipqam_sjip= "数据IP";
$language_ipqam_sjip_domain= "数据IP网络域";
$language_ipqam_pd= "频点";
$language_ipqam_pdgl= "频点管理";
$language_ipqam_pl= "频率";
$language_ipqam_fhl= "符号率";
$language_ipqam_qsdk= "起始端口";
$language_ipqam_jsdk= "端口数量";
$language_ipqam_qsfwdk= "起始服务端口";
$language_ipqam_jsfwdk= "结束服务端口";
$language_ipqam_ms= "调制模式";
$language_ipqam_select_domain= "域选择框";
$language_media_output_mode="输出模式";
$language_media_video_lx="影片类型";
$language_media_flv_play="预览";
$language_media_pinyin="拼音索引";

$language_manager_stat = "访问统计";
$language_stat_view_record="访问日志";
$language_stat_view_record_tpl_type="模板类型ID";
$language_stat_view_record_tpl_ID="模板ID";
$language_stat_view_record_tpl_name="模板名称";
$language_stat_view_record_content_name="内容名称";
$language_stat_tongji_bb="统计报表";
$language_manager_mzdr="媒资注入";

$language_manager_keyword="分类关键字";
$language_keyword_gjzgl="分类关键字管理";
$language_keyword_gjzlb="分类关键字列表";
$language_keyword_gjztj="分类关键字添加";
$language_keyword_no_gjz_can_using="没有关键字";

$language_catalog_mlbhsj ='目录包含数据';
$language_catalog_fzbhsj ='分组包含数据';
//added by xyl 2012-10-18 18:16:03
$language_manager_yhjf = "酒店计费";
$language_manager_jflb = "账单流水";
$language_manager_jdjf = "房客账单";
$language_manager_kftd = "历史账单";

//added by chenbo 2012-10-19
$language_manager_zdgl_yhfz = "用户分组";
$language_manager_zdgl_tjyh = "添加用户";
$language_manager_zdgl_yhqz = "用户群组";
$language_manager_zdgl_yhml = "用户栏目";
$language_action_move_catalog = '转移栏目';
$language_action_bind_catalog = '绑定栏目';
$language_action_move_albel_group = '绑定群组';
$language_action_delete_albel_group = '取消群组';
$language_action_bind_category = '绑定用户';
$language_action_bind_category_cancel = '取消绑定';
$language_action_undefined_lm = '未定义栏目';
$language_action_undefined_fz = '未定义分组';
$language_manager_lmgl_tjzfl ='子分类';
$language_manager_lmgl_sjfl = '上级分类';
$language_manager_lmgl_flnrmc = '分类内容名称';
$language_manager_qxzfz = '请选择分组';
$langusge_manager_zdgl_lsqz ='隶属群组';
$language_manager_zdgl_yhm = '分组名';
$language_manager_zdgl_yhqid = '用户群ID';
$language_manager_zdgl_user_checkbox = '用户选择框';
$language_manager_adgl_cscw ='参数错误';
// added by chenbo 2012-11-06
$language_topic_xxbgl = "信息包管理";
$language_topic_xxbgl_content = "信息包内容管理";
$language_topic_xxbgl_list = "信息包列表";
$language_topic_xxbgl_add = "添加信息包";
$language_topic_xxbgl_edit = "修改信息包";
$language_topic_xxb = "信息包";
$language_topic_xxbmc = "信息包名称";
$language_topic_xxb_fmtp = "封面图片";
$language_action_lms = "栏目树";

//add by chenbo 2012-11-27
$language_manager_xtgl_bqgl = '标签管理';
$language_manager_bqgl_tjbq = '添加标签';
$language_manager_bqgl_xgbq = '修改标签';
$language_manager_bqgl_bqlb = '标签列表';
$language_manager_bqgl_bqid = '标签ID';
$language_manager_bqgl_bqmc = '标签名称';
$language_manager_bqgl_bqms = '标签描述';
$language_manager_bqgl_bqnr = '标签内容';
$language_manager_bqgl_scbq = '删除标签';
$language_manager_bqgl_zdybqfs = '自定义标签调用方式';

//2013-01-23 12:04:46
$language_scyw_ywddgl = '业务订单管理';
$language_scyw_scbgl = '商城包管理';
$language_scyw_scbnrgl = '商城包内容管理';
//2013-02-04 10:05:10
$language_weather_package = '天气包';
$language_weather = '天气';
$language_weather_tqbgl = '天气包管理';
$language_weather_tjtqb = '添加天气包';
$language_weather_bjtqb = '编辑天气包';
$language_weather_tqblb = '天气包列表';
$language_weather_tjtq = '添加天气';
$language_weather_nrgl = '天气内容管理';

?>