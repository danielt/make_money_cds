<?php

/*
 * Created on 2012-3-5
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

function get_pri_language($code) {
    $return_str = "";
    switch ($code) {
        case "100":
            $return_str = "系统管理";
            break;
        case "101":
            $return_str = "角色管理";
            break;
        case "102":
            $return_str = "管理员管理";
            break;
        case "103":
            $return_str = "合作伙伴管理";
            break;
        case "104":
            $return_str = "集团管理";
            break;
        case "105":
            $return_str = "栏目管理";
            break;
        case "106":
            $return_str = "点播内容管理";
            break;
        case "107":
            $return_str = "直播内容管理";
            break;
        case "100100":
            $return_str = "系统管理员";
            break;
        case "101100":
            $return_str = "添加角色";
            break;
        case "101101":
            $return_str = "修改角色";
            break;
        case "101102":
            $return_str = "删除角色";
            break;
        case "101103":
            $return_str = "查看角色";
            break;
        case "102100":
            $return_str = "添加管理员";
            break;
        case "102101":
            $return_str = "修改管理员";
            break;
        case "102102":
            $return_str = "删除管理员";
            break;
        case "102103":
            $return_str = "查看管理员";
            break;
        case "103100":
            $return_str = "添加合作伙伴";
            break;
        case "103101":
            $return_str = "修改合作伙伴";
            break;
        case "103102":
            $return_str = "删除合作伙伴";
            break;
        case "103103":
            $return_str = "查看合作伙伴";
            break;
        case "104100":
            $return_str = "添加集团";
            break;
        case "104101":
            $return_str = "修改集团";
            break;
        case "104102":
            $return_str = "删除集团";
            break;
        case "104103":
            $return_str = "查看集团";
            break;
        case "105100":
            $return_str = "添加栏目";
            break;
        case "105101":
            $return_str = "修改栏目";
            break;
        case "105102":
            $return_str = "删除栏目";
            break;
        case "105103":
            $return_str = "查看栏目";
            break;
        case "106100":
            $return_str = "添加点播内容";
            break;
        case "106101":
            $return_str = "修改点播内容";
            break;
        case "106102":
            $return_str = "删除点播内容";
            break;
        case "106103":
            $return_str = "查看点播内容";
            break;
        case "106104":
            $return_str = "审查点播内容";
            break;
        case "107100":
            $return_str = "添加直播内容";
            break;
        case "107101":
            $return_str = "修改直播内容";
            break;
        case "107102":
            $return_str = "删除直播内容";
            break;
        case "107103":
            $return_str = "查看直播内容";
            break;
        case "107104":
            $return_str = "审查直播内容";
            break;
        case "108":
            $return_str = "终端管理";
            break;
        case "108103":
            $return_str = "查看终端";
            break;
        case "108100":
            $return_str = "添加终端";
            break;
        case "108101":
            $return_str = "修改终端";
            break;
        case "108102":
            $return_str = "删除终端";
            break;
        case "109":
            $return_str = "产品管理";
            break;
        case "109103":
            $return_str = "查看产品";
            break;
        case "109100":
            $return_str = "添加产品";
            break;
        case "109101":
            $return_str = "修改产品";
            break;
        case "109102":
            $return_str = "删除产品";
            break;
        case "110":
            $return_str = "服务包管理";
            break;
        case "110103":
            $return_str = "查看服务包";
            break;
        case "110100":
            $return_str = "添加服务包";
            break;
        case "110101":
            $return_str = "修改服务包";
            break;
        case "110102":
            $return_str = "删除服务包";
            break;
        case "111":
            $return_str = "媒资包管理";
            break;
        case "111103":
            $return_str = "查看媒资包";
            break;
        case "111100":
            $return_str = "添加媒资包";
            break;
        case "111101":
            $return_str = "修改媒资包";
            break;
        case "111102":
            $return_str = "删除媒资包";
            break;
        case "112":
            $return_str = "IPQAM管理";
            break;
        case "112103":
            $return_str = "查看IPQAM";
            break;
        case "112100":
            $return_str = "添加IPQAM";
            break;
        case "112101":
            $return_str = "修改IPQAM";
            break;
        case "112102":
            $return_str = "删除IPQAM";
            break;
        case "113":
            $return_str = "模板管理";
            break;
        case "113101":
            $return_str = "模板管理";
            break;
        case "114":
            $return_str = "统计报表";
            break;
        case "114101":
            $return_str = "查看统计报表";
            break;
        case "115":
            $return_str = "媒资注入";
            break;
        case "115101":
            $return_str = "媒资注入管理";
            break;
        case "116":
            $return_str = "关键字管理";
            break;
        case "116100":
            $return_str = "编辑关键字";
            break;
        case "116101":
            $return_str = "查看关键字列表";
            break;
        case "117":
            $return_str = "广告管理";
            break;
        case "117100":
            $return_str = "编辑广告位";
            break;
        case "117101":
            $return_str = "编辑广告内容";
            break;
        case "117102":
            $return_str = "编辑广告策略";
            break;
        case "117103":
            $return_str = "查看广告列表";
            break;
        //added by tom yuliang.xia@starcorcn.com 2012-09-29 09:55:42
        case "118":
            $return_str = "服务管理";
            break;
        case "118101":
            $return_str = "消息服务查看";
            break;
        case "118102":
            $return_str = "消息编辑";
            break;
        case "118103":
            $return_str = "消息删除";
            break;
        case '121':
            $return_str = "信息包管理";
            break;
        case '121100':
            $return_str = "增加信息包";
            break;
        case '121101':
            $return_str = "修改信息包";
            break;
        case '121102':
            $return_str = "删除信息包";
            break;
        case '121103':
            $return_str = "增加信息包栏目";
            break;
        case '121104':
            $return_str = "修改信息包栏目";
            break;
        case '121105':
            $return_str = "删除信息包栏目";
            break;
        case '121106':
            $return_str = "信息包栏目排序";
            break;
        case '121107':
            $return_str = "添加信息条目";
            break;
        case '121108':
            $return_str = "修改信息条目";
            break;
        case '121109':
            $return_str = "删除信息条目";
            break;
        case '121110':
            $return_str = "审核信息条目";
            break;
        case '121111':
            $return_str = "排序信息条目";
            break;
        case '121112':
            $return_str = "移动信息条目";
        case '129':
            $return_str = "测试模块";
            break;
        default:
            if (substr($code, -3, 3) == "001") {
                $return_str = "模块不可见";
            }
            break;
    }

    return $return_str;
}

?>
