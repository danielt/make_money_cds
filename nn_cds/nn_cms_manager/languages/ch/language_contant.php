<?php
/*
 * Created on 2012-3-16
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 function get_language_name($code){
 	$return_str="";
 	switch ($code){
 		case "ch":
 			$return_str="中文";
 		break;
 		case "en":
 			$return_str="英文";
 		break;
 	}
 	return $return_str;
 }
 
  function get_contant_language($code){
  	$code=strtoupper($code);
 	$return_str="";
 	switch($code){
 		
 		case "STD":
 			$return_str="标清";
 		break;
 		case "HD":
 			$return_str="高清";
 		break;
 		case "LOW":
 			$return_str="流畅";
 		break;
 		
 		
 	}
 	
 	return $return_str;
 }
// 0:已经分发成功
//#define NN_C_NCDS_STATE_INIT					1   //未处理,初始状态
//#define NN_C_NCDS_STATE_START				2   //已经开始处理
//#define NN_C_NCDS_STATE_DOWNLOAD			3   //下载中
//#define NN_C_NCDS_STATE_ERROR				5   //失败,可以通过reason来看错误信息
//#define NN_C_NCDS_STATE_WAIT_DELETE_DATA	6	//分发系统要求删除内容,等待处理,切换
 
 
 function get_cdn_state($code){
 	$return_str="";
 	switch ($code){
 		case 0:
 			$return_str="分发成功";
 		break;
 		case 1:
 			$return_str="未处理";
 		break;
 		case 2:
 			$return_str="开始处理";
 		break;
 		case 3:
 			$return_str="下载中";
 		break;
 		case 4:
 			$return_str="";
 		break;
 		case 5:
 			$return_str="失败";
 		break;
 		case 6:
 			$return_str="等待删除处理";
 		break;
 	}
 	return $return_str;
 }
// #define NN_C_NCDS_MODE_MANUAL              0   //由上层业务控制,适合于磁盘能存储全部内容,或上
//#define NN_C_NCDS_MODE_AUTO_PUSH           1   //自动控制,适合于磁盘不太够,但较大,且预计会热门的片
//#define NN_C_NCDS_MODE_AUTO_PULL           2   //自动控制,适合于磁盘较小
 
  function get_cdn_mode($code){
 	$return_str="";
 	switch ($code){
 		case 0:
 			$return_str="固定分发";
 		break;
 		case 1:
 			$return_str="自动推模式";
 		break;
 		case 2:
 			$return_str="自动拉模式";
 		break;
 	}
 	return $return_str;
 }
// 
// #define NN_C_NCDS_TYPE_SRC					0	//分发发送方,数据源
//#define NN_C_NCDS_TYPE_DST					1	//分发接收方
   function get_cdn_type($code){
 	$return_str="";
 	switch ($code){
 		case 0:
 			$return_str="发送方";
 		break;
 		case 1:
 			$return_str="接收方";
 		break;
 	}
 	return $return_str;
 }
 
 function get_tag_detail_html($nns_tag_str,$splite='&lt;br&gt;') {
	$nns_tag_str=rtrim($nns_tag_str,',');
	$nns_tags = explode(',', $nns_tag_str);
	$alt_str = '';
	foreach ($nns_tags as $tag_value) {
		switch ($tag_value) {
			case NNS_PLAYER_STD_TAG :
				$alt_str .= 'STB';
				break;
			case NNS_PC_TAG :
				$alt_str .= 'PC';
				break;
			case NNS_ANDROID_PAD_TAG :
				$alt_str .= 'ANDROID_PAD';
				break;
			case NNS_ANDROID_PHONE_TAG :
				$alt_str .= 'ANDROID_PHONE';
				break;
			case NNS_APPLE_PAD_TAG :
				$alt_str .= 'IPAD';
				break;
			case NNS_APPLE_PHONE_TAG :
				$alt_str .= 'IPHONE';
				break;
			default :
				$alt_str .= '自定义EPG分组 ' . $tag_value;
				break;
		}
		$alt_str .= $splite;
	}
	$alt_str = rtrim($alt_str, $splite);
	return  $alt_str;
}
 function get_tag_options($tag,$tag_str) {
 	switch ($tag) {
			case NNS_PLAYER_STD_TAG :
				$alt_str = 'STB';
				break;
			case NNS_PC_TAG :
				$alt_str = 'PC';
				break;
			case NNS_ANDROID_PAD_TAG :
				$alt_str = 'ANDROID_PAD';
				break;
			case NNS_ANDROID_PHONE_TAG :
				$alt_str .= 'ANDROID_PHONE';
				break;
			case NNS_APPLE_PAD_TAG :
				$alt_str = 'IPAD';
				break;
			case NNS_APPLE_PHONE_TAG :
				$alt_str = 'IPHONE';
				break;
			default :
				$alt_str = '自定义EPG分组 ' . $tag;
				break;
		}
		if (strstr(','.$tag_str,','.$tag.',')){
			$selected=' selected ';
		}
		return '<option value="'.$tag.'" '.$selected.' >'.$alt_str.'</option>';
 }
  function get_tag_input_box($tag,$tag_str) {
 	switch ($tag) {
			case NNS_PLAYER_STD_TAG :
				$alt_str = 'STB(<font color="red">'.NNS_PLAYER_STD_TAG.'</font>)';
				break;
			case NNS_PC_TAG :
				$alt_str = 'PC(<font color="red">'.NNS_PC_TAG.'</font>)';
				break;
			case NNS_ANDROID_PAD_TAG :
				$alt_str = 'ANDROID_PAD(<font color="red">'.NNS_ANDROID_PAD_TAG.'</font>)';
				break;
			case NNS_ANDROID_PHONE_TAG :
				$alt_str .= 'ANDROID_PHONE(<font color="red">'.NNS_ANDROID_PHONE_TAG.'</font>)';
				break;
			case NNS_APPLE_PAD_TAG :
				$alt_str = 'IPAD(<font color="red">'.NNS_APPLE_PAD_TAG.'</font>)';
				break;
			case NNS_APPLE_PHONE_TAG :
				$alt_str = 'IPHONE(<font color="red">'.NNS_APPLE_PHONE_TAG.'</font>)';
				break;
			default :
				$alt_str = cms_get_lang('zdgl_zdscbj').' (<font color="red">'.$tag.'</font>)';
				break;
		}
		if (strstr(','.$tag_str,','.$tag.',')){
			$selected=' checked ';
		}
		return '<input value="'.$tag.'" name="nns_tag[]" type="checkbox" '.$selected.' /> '.$alt_str;
 }
 
 function get_video_ex_name($ex_type){
 	switch ($ex_type) {
			case 'custom' :
				$alt_str = '自定义';
				break;
			case 'default' :
				$alt_str = '缺省';
				break;
			default :
				$alt_str = '自动';
				break;
		}
	return $alt_str;
 }
 
 function get_language_name_by_code($code){
 	
 	switch ($code){
 		case 'zh_CN':
 			return '简体中文';
 		break;
 		case 'zh_TW':
 			return '繁体中文';
 		break;
 		case 'en_US':
 			return '美式英文';
 		break;
 	}
/*
af 	     公用荷兰语
af-ZA     公用荷兰语 - 南非
sq     阿尔巴尼亚
sq-AL     阿尔巴尼亚 -阿尔巴尼亚
ar     阿拉伯语
ar-DZ     阿拉伯语 -阿尔及利亚
ar-BH     阿拉伯语 -巴林
ar-EG     阿拉伯语 -埃及
ar-IQ     阿拉伯语 -伊拉克
ar-JO     阿拉伯语 -约旦
ar-KW     阿拉伯语 -科威特
ar-LB     阿拉伯语 -黎巴嫩
ar-LY     阿拉伯语 -利比亚
ar-MA     阿拉伯语 -摩洛哥
ar-OM     阿拉伯语 -阿曼
ar-QA     阿拉伯语 -卡塔尔
ar-SA     阿拉伯语 - 沙特阿拉伯
ar-SY     阿拉伯语 -叙利亚共和国
ar-TN     阿拉伯语 -北非的共和国
ar-AE     阿拉伯语 - 阿拉伯联合酋长国
ar-YE     阿拉伯语 -也门
hy     亚美尼亚
hy-AM     亚美尼亚的 -亚美尼亚
az     Azeri
az-AZ-Cyrl     Azeri-(西里尔字母的) 阿塞拜疆
az-AZ-Latn     Azeri(拉丁文)- 阿塞拜疆
eu     巴斯克
eu-ES     巴斯克 -巴斯克
be     Belarusian
be-BY     Belarusian-白俄罗斯
bg     保加利亚
bg-BG     保加利亚 -保加利亚
ca     嘉泰罗尼亚
ca-ES     嘉泰罗尼亚 -嘉泰罗尼亚
zh-HK     华 - 香港的 SAR
zh-MO     华 - 澳门的 SAR
zh-CN     华 -中国
zh-CHS     华 (单一化)
zh-SG     华 -新加坡
zh-TW     华 -台湾
zh-CHT     华 (传统的)
hr     克罗埃西亚
hr-HR     克罗埃西亚 -克罗埃西亚
cs     捷克
cs-CZ     捷克 - 捷克
da     丹麦文
da-DK     丹麦文 -丹麦
div     Dhivehi
div-MV     Dhivehi-马尔代夫
nl     荷兰
nl-BE     荷兰 -比利时
nl-NL     荷兰 - 荷兰
en     英国
en-AU     英国 -澳洲
en-BZ     英国 -伯利兹
en-CA     英国 -加拿大
en-CB     英国 -加勒比海
en-IE     英国 -爱尔兰
en-JM     英国 -牙买加
en-NZ     英国 - 新西兰
en-PH     英国 -菲律宾共和国
en-ZA     英国 - 南非
en-TT     英国 - 千里达托贝哥共和国
en-GB     英国 - 英国
en-US     英国 - 美国
en-ZW     英国 -津巴布韦
et     爱沙尼亚
et-EE     爱沙尼亚的 -爱沙尼亚
fo     Faroese
fo-FO     Faroese- 法罗群岛
fa     波斯语
fa-IR     波斯语 -伊朗王国
fi     芬兰语
fi-FI     芬兰语 -芬兰
fr     法国
fr-BE     法国 -比利时
fr-CA     法国 -加拿大
fr-FR     法国 -法国
fr-LU     法国 -卢森堡
fr-MC     法国 -摩纳哥
fr-CH     法国 -瑞士
gl     加利西亚
gl-ES     加利西亚 -加利西亚
ka     格鲁吉亚州
ka-GE     格鲁吉亚州 -格鲁吉亚州
de     德国
de-AT     德国 -奥地利
de-DE     德国 -德国
de-LI     德国 -列支敦士登
de-LU     德国 -卢森堡
de-CH     德国 -瑞士
el     希腊
el-GR     希腊 -希腊
gu     Gujarati
gu-IN     Gujarati-印度
he     希伯来
he-IL     希伯来 -以色列
hi     北印度语
hi-IN     北印度的 -印度
hu     匈牙利
hu-HU     匈牙利的 -匈牙利
is     冰岛语
is-IS     冰岛的 -冰岛
id     印尼
id-ID     印尼 -印尼
it     意大利
it-IT     意大利 -意大利
it-CH     意大利 -瑞士
ja     日本
ja-JP     日本 -日本
kn     卡纳达语
kn-IN     卡纳达语 -印度
kk     Kazakh
kk-KZ     Kazakh-哈萨克
kok     Konkani
kok-IN     Konkani-印度
ko     韩国
ko-KR     韩国 -韩国
ky     Kyrgyz
ky-KZ     Kyrgyz-哈萨克
lv     拉脱维亚
lv-LV     拉脱维亚的 -拉脱维亚
lt     立陶宛
lt-LT     立陶宛 -立陶宛
mk     马其顿
mk-MK     马其顿 -FYROM
ms     马来
ms-BN     马来 -汶莱
ms-MY     马来 -马来西亚
mr     马拉地语
mr-IN     马拉地语 -印度
mn     蒙古
mn-MN     蒙古 -蒙古
no     挪威
nb-NO     挪威 (Bokm?l) - 挪威
nn-NO     挪威 (Nynorsk)- 挪威
pl     波兰
pl-PL     波兰 -波兰
pt     葡萄牙
pt-BR     葡萄牙 -巴西
pt-PT     葡萄牙 -葡萄牙
pa     Punjab 语
pa-IN     Punjab 语 -印度
ro     罗马尼亚语
ro-RO     罗马尼亚语 -罗马尼亚
ru     俄国
ru-RU     俄国 -俄国
sa     梵文
sa-IN     梵文 -印度
sr-SP-Cyrl     塞尔维亚 -(西里尔字母的) 塞尔维亚共和国
sr-SP-Latn     塞尔维亚 (拉丁文)- 塞尔维亚共和国
sk     斯洛伐克
sk-SK     斯洛伐克 -斯洛伐克
sl     斯洛文尼亚
sl-SI     斯洛文尼亚 -斯洛文尼亚
es     西班牙
es-AR     西班牙 -阿根廷
es-BO     西班牙 -玻利维亚
es-CL     西班牙 -智利
es-CO     西班牙 -哥伦比亚
es-CR     西班牙 - 哥斯达黎加
es-DO     西班牙 - 多米尼加共和国
es-EC     西班牙 -厄瓜多尔
es-SV     西班牙 - 萨尔瓦多
es-GT     西班牙 -危地马拉
es-HN     西班牙 -洪都拉斯
es-MX     西班牙 -墨西哥
es-NI     西班牙 -尼加拉瓜
es-PA     西班牙 -巴拿马
es-PY     西班牙 -巴拉圭
es-PE     西班牙 -秘鲁
es-PR     西班牙 - 波多黎各
es-ES     西班牙 -西班牙
es-UY     西班牙 -乌拉圭
es-VE     西班牙 -委内瑞拉
sw     Swahili
sw-KE     Swahili-肯尼亚
sv     瑞典
sv-FI     瑞典 -芬兰
sv-SE     瑞典 -瑞典
syr     Syriac
syr-SY     Syriac-叙利亚共和国
ta     坦米尔
ta-IN     坦米尔 -印度
tt     Tatar
tt-RU     Tatar-俄国
te     Telugu
te-IN     Telugu-印度
th     泰国
th-TH     泰国 -泰国
tr     土耳其语
tr-TR     土耳其语 -土耳其
uk     乌克兰
uk-UA     乌克兰 -乌克兰
ur     Urdu
ur-PK     Urdu-巴基斯坦
uz     Uzbek
uz-UZ-Cyrl     Uzbek-(西里尔字母的) 乌兹别克斯坦
uz-UZ-Latn     Uzbek(拉丁文)- 乌兹别克斯坦
vi     越南
vi-VN     越南 -越南*/
 }
?>
