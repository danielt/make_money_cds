<?php
/*
 * Created on 2012-3-5
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 function get_pri_language($code){
 	$return_str="";
 	switch($code){
 		case "100":
 			$return_str="System Management";
 		break;
 		case "101":
 			$return_str="Role Management";
 		break;
 		case "102":
 			$return_str="Administrator Management";
 		break;
 		case "103":
 			$return_str="Partner Management";
 		break;
 		case "104":
 			$return_str="Group Management";
 		break;
 		case "105":
 			$return_str="Category Management";
 		break;
 		case "106":
 			$return_str="VOD Content Management";
 		break;
 		case "107":
 			$return_str="Live Content Management";
 		break;
 		case "100100":
 			$return_str="System Administrator";
 		break;
 		case "101100":
 			$return_str="Add Role";
 		break;
 		case "101101":
 			$return_str="Revise Role";
 		break;
 		case "101102":
 			$return_str="Delete Role";
 		break;
 		case "101103":
 			$return_str="View Role";
 		break;
 		case "102100":
 			$return_str="Add Administrator";
 		break;
 		case "102101":
 			$return_str="Revise Administrator";
 		break;
 		case "102102":
 			$return_str="Delte Administrator";
 		break;
 		case "102103":
 			$return_str="View Administrtor";
 		break;
 		case "103100":
 			$return_str="Add Partner";
 		break;
 		case "103101":
 			$return_str="Revise Partner";
 		break;
 		case "103102":
 			$return_str="Delte Partner";
 		break;
 		case "103103":
 			$return_str="View Partner";
 		break;
 		case "104100":
 			$return_str="Add Group";
 		break;
 		case "104101":
 			$return_str="Revise Group";
 		break;
 		case "104102":
 			$return_str="Delte Group";
 		break;
 		case "104103":
 			$return_str="View Group";
 		break;
 		case "105100":
 			$return_str="Add Category";
 		break;
 		case "105101":
 			$return_str="Edit Category";
 		break;
 		case "105102":
 			$return_str="Delte Category";
 		break;
 		case "105103":
 			$return_str="View Category";
 		break;
 		case "106100":
 			$return_str="Add VOD Content";
 		break;
 		case "106101":
 			$return_str="Edit VOD Content";
 		break;
 		case "106102":
 			$return_str="Delte VOD Content";
 		break;
 		case "106103":
 			$return_str="View VOD Content";
 		break;
 		case "106104":
 			$return_str="Examine VOD Content";
 		break;
 		case "107100":
 			$return_str="Add Live Content";
 		break;
 		case "107101":
 			$return_str="Edit Live Content";
 		break;
 		case "107102":
 			$return_str="Delete Live Content";
 		break;
 		case "107103":
 			$return_str="View Live Content";
 		break;
 		case "107104":
 			$return_str="Examine Live Content";
 		break;
 		case "108":
 			$return_str="Terminal Type Management";
 		break;
 		case "108103":
 			$return_str="View Terminal Type";
 		break;
 		case "108100":
 			$return_str="Add Terminal Type";
 		break;
 		case "108101":
 			$return_str="Edit Terminal Type";
 		break;
 		case "108102":
 			$return_str="Delte Terminal Type";
 		break;
 		case "109":
 			$return_str="Product Management";
 		break;
 		case "109103":
 			$return_str="View Products";
 		break;
 		case "109100":
 			$return_str="Add Products";
 		break;
 		case "109101":
 			$return_str="EditProducts";
 		break;
 		case "109102":
 			$return_str="Delte Products";
 		break;
 		case "110":
 			$return_str="Service Pack Management";
 		break;
 		case "110103":
 			$return_str="View Service Pack";
 		break;
 		case "110100":
 			$return_str="Add Service Pack";
 		break;
 		case "110101":
 			$return_str="Edit Service Pack";
 		break;
 		case "110102":
 			$return_str="Delte Service Pack";
 		break;
 		case "111":
 			$return_str="Media Pack Management";
 		break;
 		case "111103":
 			$return_str="View Media Pack";
 		break;
 		case "111100":
 			$return_str="Add Media Pack";
 		break;
 		case "111101":
 			$return_str="Edit Media Pack";
 		break;
 		case "111102":
 			$return_str="Delete Media Pack";
 		break;
 		case "112":
 			$return_str="IPQAM Management";
 		break;
 		case "112103":
 			$return_str="View IPQAM";
 		break;
 		case "112100":
 			$return_str="Add IPQAM";
 		break;
 		case "112101":
 			$return_str="Edit IPQAM";
 		break;
 		case "112102":
 			$return_str="Delte IPQAM";
 		break;
 		case "113":
 			$return_str="Framework Management";
 		break;
 		case "113101":
 			$return_str="Framework Management";
 		break;
 		case "114":
 			$return_str="Statistical Report";
 		break;
 		case "114101":
 			$return_str="View Statistical Report";
 		break;
 		case "115":
 			$return_str="Media Asset Injection";
 		break;
 		case "115101":
 			$return_str="Media Asset Injection Management";
 		break;
 	}
 	
 	return $return_str;
 }

?>
