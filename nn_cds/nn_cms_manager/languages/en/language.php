<?php
$language_manager_company="Biteer.China";
$language_manager_titile = "Omnimedia";
$language_manager_help = "Help";
$language_manager_quit = "Exit";
$language_manager_xtgl = "System Configuration";
$language_manager_log_limit = "Paly Log Retention Time";
$language_manager_lmgl = "Category Management";
$language_manager_qxgl = "System Configuration";
$language_manager_zdgl = "Terminal Management";
$language_manager_dbgl = "VOD Management";
$language_manager_bkgl = "Plate Service Pack";
$language_manager_bkgllb = "Plate Service Pack List";
$language_manager_bkgl_bk = "Service Pack";
$language_manager_bkgl_owner = "Citied by Service Pack";
$language_manager_bkgl_bklb = "Plate Metadata Management";
$language_manager_zbgl = "Live Management";
$language_manager_jmdgl = "Program Mangement";
$language_manager_xtgl_yysgl = "Plaftform Operator";
$language_manager_xtgl_cmsgl = "CMS Configuration Mangement";
$language_manager_xtgl_jsgl = "Role Management";
$language_manager_xtgl_hzgl = "CP/SP Mangement";
$language_manager_xtgl_jtgl = "Group Mangement";
$language_manager_xtgl_glygl = "Administrator Management";
$language_manager_cpgl = "Product Accounting Pack";
$language_manager_cpgl_cp = "Product Pack";
$language_manager_cpgl_zf = "Tariff Pack";
$language_manager_cpgl_zflx="Tariff Pack";
$language_manager_cpgl_zflb = "Tariff Pcak";
$language_manager_cpgl_bdcp = "Binding Content";
$language_manager_cpgl_cplb = "Product Pack List";
$language_manager_cpgl_cpshex = "Effective Date";
$language_manager_cpgl_cpshix = "Expiration Date";
$language_manager_cpgl_bkjl = "Playing Record";
$language_manager_cpgl_sfsx = "Expired or not";
$language_manager_cpgl_cpmc = "Product Pack Name";
$language_manager_cpgl_cpjg = "Product Pack Price";
$language_manager_cpgl_jffs = "Charging Mode";
$language_manager_cpgl_sjzq = "Cycle Time";
$language_manager_cpgl_sqyps = "Authorization Number";
$language_manager_cpgl_cpzylx = "Product Pack Resource Type";
$language_manager_cpgl_cpjgfs0 = "Charge by Times";
$language_manager_cpgl_cpjgfs1 = "Charge by Month";
$language_manager_cpgl_cpjgfs2 = "Charge by Year";
$language_manager_cpgl_cpjgfs3 = "Charge by Cycle";
$language_manager_cpgl_cpjgfs4 = "Charge by Movies Number";

$language_manager_global_pt_jkpz="Platform interface address";
$language_manager_global_hxpt_jkpz="MSP interface configuration";
$language_manager_global_hxpt_rk="platform Entrance ";
$language_manager_global_hxpt_dlm="platform log name";
$language_manager_global_hxpt_mm="plaftform password";
$language_manager_global_hxpt_dd="Scheduling server address";



$language_manager_cpgl_bdyp0 = "Bind all content";
$language_manager_cpgl_bdyp1 = "Bind all live";
$language_manager_cpgl_bdyp2 = "Bind all VOD";
$language_manager_cpgl_bdyp3 = "Bind by Category";
$language_manager_cpgl_bdyp4 = "Bind by live movie";
$language_manager_cpgl_bdyp5 = "Bind by live source";
$language_manager_cpgl_bdyp6 = "Bind by VOD movie";
$language_manager_cpgl_bdyp7 = "Bind by Vod source";

$language_manager_xtgl_mbgl = "Plate Management";
$language_manager_xtgl_xgmm = "Change password";
$language_manager_yhm = "username";
$language_manager_yhlx = "user type ";

$language_manager_zdgl_sbidh = "Device ID number";
$language_manager_zdgl_sbgl = "Device management";
$language_manager_zdgl_yhgl = "User management";
$language_manager_zdgl_sblb = "Device list";
$language_manager_zdgl_yhlb = "User list";
$language_manager_zdgl_sb = "Device";
$language_manager_zdgl_yh = "User";
$language_manager_zdgl_sbmc = "Device name";
$language_manager_zdgl_bdyh = "Bind user";
$language_manager_zdgl_bdcp = "Bind product";
$language_manager_zdgl_sbmc = "Device name";
$language_manager_zdgl_zdlx = "Terminal type";
$language_manager_zdgl_zdlxgl = "Terminal type management";
$language_manager_zdgl_zdlxlb = "Terminal type list";
$language_manager_zdgl_zdlxmc = "Terminal type name";
$language_manager_zdgl_zdpt = "output plaftform";
$language_manager_zdgl_zdscbj = "Terminal EPG Groups ";
$language_manager_zdgl_bbh = "Version number";
$language_manager_zdgl_sjsc = "Upate pack uploading";

$language_manager_xtgl_cmshtpz = "CMS backstage configuration";
$language_manager_xtgl_qdmbscpz = "Front-end output configuration";

$language_manager_lmgl_lmmbgl = "Category plate management";
$language_manager_lmgl_lmmbgl_yys = "Operator Category plate";
$language_manager_lmgl_lmmbgl_hzhb = "Cooperation partner Category plate";
$language_manager_lmgl_lmmbgl_jt = "group Category plate";
$language_manager_lmgl_nrlmgl = "Category content management";
$language_manager_lmgl_lmmb = "Category plate";
$language_manager_lmgl_nrlm = "Category content";
$language_manager_lmgl_lmmc = "Category name";
$language_manager_lmgl_nrlmid = "Category";
$language_manager_lmgl_tjzlm = "sub Category";
$language_manager_lmgl_3wszbm = "(Three digit code)";

$language_manager_jggl_dqjg = "Present Group";
$language_manager_jggl_xzjg = "Select Group";

$language_manager_mbgl_mblx = "ID Plate Type";
$language_manager_mbgl_mbys = "Plate ID";
$language_manager_mbgl_mblb = "Plate List";
$language_manager_mbgl_ckts = "View pictures";

$language_manager_lmgl_lmlbgl_yys = "Operator Category Content";
$language_manager_lmgl_lmlbgl_hzhb = "Partner Category Content";
$language_manager_lmgl_lmlbgl_jt = "Group Category content";

$language_manager_lmgl_sjlm = "superior Category";
$language_manager_lmgl_gmu = "Root Directory";

$language_manager_lmgl_lmnrmc = "Category content name";
$language_manager_lmnr_msg_noselect = "Please select one Category content";

$language_manager_lmgl_lmmb = "Category plate";
$language_manager_lmgl_lmmblx = "Category plate type";
$language_manager_lmgl_zblm = "live Category";
$language_manager_lmgl_dblm = "VOD Category";
$language_manager_lmgl_lmmbmc = "Category plate name";
$language_manager_lmgl_msg_noselect = "please select one Category plate ";


$language_manager_str_group = "Group";
$language_manager_str_partner = "Partner";

$language_manager_lxzc = "Contact us";
$language_manager_gywm = "About ".$language_manager_company." company";
$language_manager_tktj = "termns and conditions";
$language_manager_ys = "Privacy";

$language_manager_dlsj = "last log time";
$language_action_lock = "lock";
$language_action_no_lock = "no lock";
$language_action_unlock = "unlock";
$language_action_action = "action";
$language_action_binded = "binded";
$language_action_unbinded = "unbinded";
$language_action_success = "success";
$language_action_fault = "failure";
$language_action_move_category = " move Category";
$language_action_add = "add";
$language_action_delete = "delte";
$language_action_real_delete = "real delete";
$language_action_edit = "edit";
$language_action_state = "state";
$language_action_select = "select";
$language_action_cancel = "cancel";
$language_action_audit = "audit";
$language_action_confirm = "confirm";
$language_action_search = "search";
$language_action_back = "back";
$language_action_upload = "ipload";
$language_action_reset = "reset";
$language_action_bind = "bind";
$language_action_nobind = "no bind";
$language_manager_global_kq = "open";
$language_manager_global_bkq = "not open";
$language_action_search_mhcx = "Fuzzy Search";
$language_action_search_xz = "choose";
$language_action_search_bx = "unlimited";
$language_action_search_yx = "effective";
$language_action_search_sx = "invalid";
$language_action_search_yjyx = "Permanent";
$language_action_point = "point";
$language_action_yes = "yes";
$language_action_no = "no";
$language_action_day = "day";
$language_action_month = "month";
$language_action_advance = "advanced";
$language_action_year = "year";
$language_action_perpagenum = "Show the number of records per page";

$language_msg_ask_add = "are you sure to add?";
$language_msg_ask_edit = "are you sure to edit?";
$language_msg_ask_delete = "are you sure to delete?";
$language_msg_ask_search = "are you sure to search?";
$language_msg_ask_audit = "are you sure to audit?";
$language_msg_ask_reset = "are you sure to reset?";
$language_msg_force_xx = "are you sure to force off the movie?";
$language_msg_carrier = "please set platform operator first";

$language_manager_global_xtdz = "system address";
$language_manager_global_hcsj = "Cache expiration time";
$language_manager_global_hc = "Cache ";
$language_manager_global_jlts = "Each page shows the number of records";
$language_manager_global_yysmc = "operator name";
$language_manager_global_ms = "description";
$language_manager_global_yyb = "language";
$language_manager_global_yzm = "Verification code";
$language_manager_global_zdlx = "terminal type";

$language_manager_group_jtmc = "Group name";
$language_manager_group_jtxx = "Group";
$language_manager_group_jtxx_xx = "Group information";
$language_manager_group_jtlb = "Group list";
$language_manager_group_msg_noselect = "Please select a piece of group information";

$language_manager_partner_hzhbmc = "partner name";
$language_manager_partner_hzhbxx = "partner";
$language_manager_partner_hzhblb = "partner list";
$language_manager_partner_hzhbxx_xx = "partner information";
$language_manager_partner_msg_noselect = "Please select a piece of partner information";

$language_manager_table_role_name = "role name";
$language_manager_role_jsxx = "role information";
$language_manager_role_msg_noselect = "Please select a piece of role information";

$language_manager_manager_glyxx = "administrator information";
$language_manager_manager_msg_noselect = "Please select a piece of administrator information";

$language_manager_table_name = "name";
$language_manager_table_title = "title";
$language_manager_table_create_time = "create time";
$language_manager_table_edit_time = "edit time";
$language_manager_table_edit_role = "role";

$language_manager_table_pass = "password";
$language_manager_table_oldpass = "old password";
$language_manager_table_newpass = "new password";
$language_manager_table_pass_confirm = "password confirmation";

$language_manager_table_role_pri = "Role privilege ";
$language_manager_table_role_manager = "attached administrator number";
$language_manager_table_manager_id = "administrator account";
$language_manager_table_manager_name = "administrator name";
$language_manager_table_manager_login = "adminstrator login";
$language_manager_table_group = "attached group";
$language_manager_table_login_time = "recent login name";
$language_manager_table_login_num = "login number";
$language_manager_table_segnumber = "serial number";
$language_manager_table_lxr = "contact person";
$language_manager_table_lxdh = "phone";
$language_manager_table_email = "Email";
$language_manager_table_lxdz = "address";
$language_manager_table_nc = "name";
$language_manager_table_order = "order";
$language_manager_table_order_up = "up";
$language_manager_table_order_down = "down";
$language_manager_table_order_pre = "previous";
$language_manager_table_order_next = "next";
$language_manager_table_zh = "account";
$language_manager_table_ID = "ID";

$language_manager_table_first_page = "first page";
$language_manager_table_last_page = "last page";
$language_manager_table_next_page = "next page";
$language_manager_table_pre_page = "previous page";
$language_manager_table_page = "page";
$language_manager_table_current = "current";
$language_manager_table_total = "total";
$language_manager_table_jump_to = "jump to";


$language_manager_js_msg_1 = "Two times password is not consistent";
$language_manager_js_msg_2 = "Filling contents can't be empty";
$language_manager_js_msg_3 = "All the information must be an integer";
$language_manager_js_msg_4 = "Please select a data！";
$language_manager_js_msg_5 = "you do not have permission to operate the plate";
$language_manager_js_msg_6 = "The phone number is not in the correct format！";
$language_manager_js_msg_7 = "he email format is not correct！";
$language_manager_js_msg_9 = "you must input 3 characters！";
$language_manager_js_msg_8 = "Category list is empty";
$language_manager_js_msg_10 = "ID already exists";
$language_manager_js_msg_11 = "Expiration date must be later than the effective date";
$language_manager_js_msg_12 = "the Category is already with data";
//$language_manager_js_msg_12 = "影片已存在The film exists！";
$language_media_lm_bslm = "No Category";
$language_manager_lmlb="Category list";

$language_media_addedit_ysx = "online";
$language_media_addedit_yxx = "offline";
$language_media_addedit_ysh = "approved";
$language_media_addedit_ddsh = "under approval";
$language_action_access_audit = "pass approval";
$language_action_deleted = "deleted";

$language_service_bknrgl = "plate content management";
$language_service_fengmian = "cover";
$language_service_tpnr = "picture";
$language_service_czykxzsp = "select video from resource library";
$language_service_sp = "video";
$language_service_mlsgid = "dirctory three root ID";
$language_service_bdgdmt = "bind more medias";
$language_service_bdgdtp = "bind more pictures";
$language_service_qhpk = "switch storage";
$language_service_pklx = "storage type";
$language_service_pk = "storage";
$language_service_syzy = "all resources";
$language_service_dshzy = "Pending audit resources";
$language_service_ysczy = "Recycling station";

$language_media_zykgl = "Resources management";
$language_media_zyklm = "Resources category";
$language_media_zykflgl = "Resource classification management";
$language_media_gylm = "pubilc category";
$language_media_sylm = "private category";
$language_media_daoyan = "dirctor";
$language_media_yy = "actor";
$language_media_sysj = "relase time";
$language_media_playbill_kssj = "start time";
$language_media_playbill_drjmd = "import program";
$language_media_playbill_scjmd = "delte program";
$language_media_playbill_dcjmd = "export program";
$language_media_playbill_jmdlb = "prgoram list";
$language_media_playbill_jmdrq = "program date";
$language_media_yp_xx = "movie information";

$language_media_playbill_drms = "import mode";
$language_media_playbill_jmmc = "program name";
$language_media_playbill_import = "import";
$language_media_playbill_export = "export";
$language_media_playbill_single_dr = "single day import";
$language_media_playbill_more_dr = "more days import";
$language_media_playbill_select_date = "select date";
$language_media_playbill_jmdwj = "prgoram document";
$language_media_mjpc = "duration of episode";
$language_media_zsc = "duration";
$language_media_js = "episodes";
$language_media_fps = "numbers";
$language_media_fp = "number";
$language_media_zjs = "all episodes";
$language_media_dqgx = "currect updates";
$language_media_nrjj = "content introduction";
$language_media_dq = "show area";
$language_media_dy = "movie";
$language_media_zb = "live";
$language_media_db = "vod";
$language_media_lxj = "tv series";
$language_media_dylb = "movie list";
$language_media_lxjlb = "tv series list";
$language_media_zblb = "live list";
$language_media_search_zbcx = "live search";
$language_media_zbpdmc = "live channel name";
$language_media_mtmc = "media file name";
$language_media_zbymc = "Live source name ";
$language_media_mtlx = "media file type";
$language_media_index_jj = "introduction";
$language_media_index_sc = "duration";
$language_media_control_bjjj = "edit episode description";
$language_media_control_bjzbjj = "edit live description";
$language_media_control_tjmt = "add media files";
$language_media_control_tjzby = "add live sources";
$language_media_addedit_pymc = "source name";
$language_media_addedit_pydz = "source URL address";
$language_media_addedit_pyml = "source bit rate(kbps)";
$language_media_addedit_mtid = "media ID";
$language_media_addedit_pylx = "source type";
$language_media_addedit_pylx = "resolution";
$language_media_addedit_tjfs = "Recommendation scores";
$language_media_addedit_copyright_date = "copyright expiration date";

$language_media_addedit_bjhb = "Edit poster ";
$language_media_addedit_slt = "Thumbnail";
$language_media_addedit_pictrue = "poster";
$language_media_addedit_big = "big";
$language_media_addedit_normal = "normal";
$language_media_addedit_small = "small";
$language_action_search_ypcx = "movie search";

$language_media_addedit_msg_select_dy = "Please choose a movie not on screen yet！";
$language_media_addedit_msg_select_lxj = "Please choose a tv series not on screen yet！";
$language_media_addedit_msg_select_cp = "Please select a product information！";
$language_media_addedit_msg_select_jm = "Please select a program！";
$language_media_addedit_msg_select_jmd = "Please select a program list！";
$language_media_mtwjgl = "Media file management";
$language_media_mt = "media file";
$language_media_zbygl = "live source management";
$language_media_zbfl = "live clarification";
$language_media_dbfl = "VOD clarification";

$language_manager_assist = "Media resource pack";
$language_manager_assist_owner = "cited by Media resource pack";
$language_manager_assist_gl = "Media resource pack management";
$language_manager_assist_nrgl = "Media resource pack content management";
$language_manager_assist_lb = "Media resource pack list";
$language_manager_assist_bdnr = "bind the content of Media resource pack";

$language_manager_service_bdnr = "bind service pack content";

$language_epg_n1_a_addr = "entrance to front N1-A";
$language_epg_n6_a_addr = "entrance to front N6-A";
$language_epg_n1_b_addr = "entrance to front N1-B";
$language_epg_cant_modify = "can not modify";

$language_media_addedit_pygs = "media format";
$language_media_addedit_bdcl = "bind strategy";
$language_media_addedit_pymtid = "source media ID";
$language_media_addedit_tjpyfs = "add source mode";
$language_media_addedit_new_media = "new media";
$language_media_addedit_bind_media = "media management";
$language_media_addedit_root_skin= "root skin pack";
$language_media_select_box="media select box";
$language_play_count="play times";
$language_export_csv="export CSV data";
$language_system_config_rzgl="log management";
$language_syytem_config_rz="log";
$language_action_log_type="log type";
$language_action_log_detail="log detail";
$language_action_log_time= "log time";
$language_product_fee_gl= "Billing management";

$language_ipqam_gl= "IPQAM management";
$language_ipqam_glip= "Manage IP";
$language_ipqam_backupglip= "Spare management IP";
$language_ipqam_dqm= "area code";
$language_ipqam_sjip= "data IP";
$language_ipqam_sjip_domain= "data IP network domain";
$language_ipqam_pd= "frequency point";
$language_ipqam_pdgl= "freuency list";
$language_ipqam_pl= "frequency";
$language_ipqam_fhl= "bit rate";
$language_ipqam_qsdk= "the start and end port";
$language_ipqam_jsdk= "port number";
$language_ipqam_qsfwdk= "Start service port";
$language_ipqam_jsfwdk= "End service port";
$language_ipqam_ms= "Modulation mode";
$language_ipqam_select_domain= "Domain selection box";
$language_media_output_mode="export mode";
$language_media_video_lx="movie type";
$language_media_flv_play="preview";
$language_media_pinyin="Phonetic abbreviations";

$language_manager_stat = "visit statistics";
$language_stat_view_record="visit record";
$language_stat_view_record_tpl_type="plate type ID";
$language_stat_view_record_tpl_ID="plate ID";
$language_stat_view_record_tpl_name="plate name";
$language_stat_view_record_content_name="content name";
$language_stat_tongji_bb="Statistical Report";
?>