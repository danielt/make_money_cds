<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../nncms_manager_inc.php");
 //导入语言包
  error_reporting(E_ALL ^ E_NOTICE);
 session_start();
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
 $action=$_GET["action"];
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

 //加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

include $nncms_db_path. "nns_common/nns_db_constant.php";
include $nncms_db_path. "nns_service/nns_db_service_class.php";
$service_inst=new nns_db_service_class();
$result=$service_inst->nns_db_service_list($_SESSION["nns_org_id"],$_SESSION["nns_manager_type"]);
?>
<!DOCTYPE html PUBLIC "-//W3C//liD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/liD/xhtml1-transitional.lid">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/leftmenu.js"></script>
<script language="javascript">
//<!--xyl 2013-04-10 18:38:31-->
	//function refresh_category_list(){
		//window.location.href="nncms_left_bkgl.php?action=refresh";
	//}

</script>
</head>

<body>
<div class="leftMenu">
    <ul class="sup_menu" >

        <li><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('bkgl');?></a>
        	<ul class="sub_menu">
            	<li><a href="javascript:void(null);" pos="contentframe/service_pages/nncms_content_servicelist.php"><?php echo cms_get_lang('bkgl_bklb');?></a></li>
                <li><a href="javascript:void(null);" pos="contentframe/service_pages/nncms_content_service_add.php?action=add"><?php echo cms_get_lang('add|bkgl_bk');?></a></li>
            </ul>
        </li>
<!--//xyl 2013-04-10 18:38:20-->
<!--      	<li class="select_menu"><a href="javascript:refresh_category_list();">&middot; <?php echo cms_get_lang('service_bknrgl');?></a>-->
      	<li class="select_menu"><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('service_bknrgl');?></a>
        	<ul class="sub_menu">
            	<?php if ($result["ret"]==0){
            		$data=$result["data"];
            		if ($data!=null){
            			foreach ($data as $item){?>
            			 <li><a href="javascript:void(null);" pos="contentframe/service_pages/nncms_content_service_tree_addedit.php?nns_id=<?php echo $item["nns_id"];?>"><?php echo $item["nns_name"]."(".$item["nns_id"].")";?></a></li>
            		<?php }}}?>
            </ul>
        </li>

    </ul>
</div>
</body>
</html>
