<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include ("../nncms_manager_inc.php");
//导入语言包
session_start();
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//加载多语言
include_once ($nncms_config_path . 'nn_cms_config/nn_cms_global.php');
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
$message_list = '';
$import_mode = g_cms_config::get_g_config_value('g_import_mode');
switch ((string)$import_mode)
{
	case 'cntv_me':
		$message_list = 'nncms_content_cp_cntv_message_list.php';
		break;	
	default:
		$message_list = 'nncms_content_cp_message_list.php?org_id=' . $import_mode;
		break;
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../js/leftmenu.js"></script>
	</head>
	<body>
		<div class="leftMenu">
			<ul class="sup_menu">
				<li>
					<a href="javascript:void(null);">&middot; 下游平台管理</a>
					<ul class="sub_menu">
						<li>
							<a href="javascript:void(null);" pos="contentframe/sp_pages_v2/nncms_content_sp_list.php">下游分发列表</a>
						</li>
<!-- 						<li> -->
<!-- 							<a href="javascript:void(null);" pos="contentframe/sp_pages_v2/nncms_content_sp_addedit.php?action=add">添加SP</a> -->
<!-- 						</li> -->
<!-- 						<li> -->
<!-- 							<a href="javascript:void(null);" pos="contentframe/cp_pages/nncms_content_cp_info_list.php">CP列表</a> -->
<!-- 						</li> -->
					</ul>
				</li>
			</ul>
<!--			<ul class="sup_menu">-->
<!--				<li>-->
<!--					<a href="javascript:void(null);">&middot; CP消息管理</a>-->
<!--					<ul class="sub_menu">-->
<!--						<li>-->
<!--							<a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/--><?php //echo $message_list;?><!--">CP消息列表</a>-->
<!--						</li>-->
<!--					</ul>-->
<!--				</li>-->
<!--			</ul>-->
<!-- 			<ul class="sup_menu"> -->
<!-- 				<li> -->
<!-- 					<a href="javascript:void(null);">&middot; CP信息管理</a> -->
<!-- 					<ul class="sub_menu"> -->
<!-- 						<li> -->
<!-- 							<a href="javascript:void(null);" pos="contentframe/cp_pages/nncms_content_cp_info_list.php">CP信息列表</a> -->
<!-- 						</li> -->
<!-- 						<li> -->
<!-- 							<a href="javascript:void(null);" pos="contentframe/cp_pages/nncms_content_cp_info_addedit.php">添加CP列表</a> -->
<!-- 						</li> -->
<!-- 					</ul> -->
<!-- 				</li> -->
<!-- 			</ul> -->
		</div>
	</body>
</html>
