<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
//导入语言包
session_start();
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../js/leftmenu.js"></script>
        </head>

        <body>
                <div class="leftMenu">
                        <ul class="sup_menu">
                                <?php
                                foreach ($g_epg_data_type as $name => $list) {
                                        ?>
                                
                                        <li><a href="javascript:void(null);">&middot; <?php echo $name; ?>配置</a>
                                                <?php
                                                foreach ($list as $k => $v) {
                                                        ?>
                                                        <ul class="sub_menu">
                                                                <li><a href="javascript:void(null);" pos="mgtv/epg_v2/nns_mgtv_list.php?type=<?php echo $k; ?>"><?php echo $v; ?>列表</a></li>
                                                        </ul>
                                                <?php } ?>

                                        </li>
                                <?php } ?>

                                <li><a href="javascript:void(null);">&middot; 芒果TV日志</a>
                                        <ul class="sub_menu">
                                                <li><a href="javascript:void(null);" pos="contentframe/media_asset_import_log/nns_cms_log_list.php?type=menu">媒资包错误日志</a></li>
                                        </ul>
                                </li>
                                <li><a href="javascript:void(null);">&middot; FAQ管理</a>
                                        <ul class="sub_menu">
                                                <li><a href="javascript:void(null);" pos="mgtv/nns_faq_list.php?type=menu">FAQ管理</a>
                                                </li>
                                        </ul>
                                </li>
                        </ul>
                </div>
        </body>
</html>
