<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../nncms_manager_inc.php");
//导入语言包
session_start();
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}

//加载多语言
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />

<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/leftmenu.js"></script>
</head>

<body>
<div class="leftMenu">
	<ul class="sup_menu">
		<!--li ><a href="javascript:void(null);">&middot; 酒店服务</a>
			<ul class="sub_menu">
				<li><a href="javascript:void(null);"  pos="contentframe/content_pages/nncms_content_list.php?type=1">酒店介绍</a></li>
				<li><a href="javascript:void(null);" pos="contentframe/service_pages/nncms_content_service_category_list.php?type=1000300&service_package_id=jiudian_service&type=hotel">出行提示</a></li>

			</ul>
		</li-->
		<li ><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('xtgl_xxgl');?></a>
			<ul class="sub_menu">
				<li><a href="javascript:void(null);" pos="contentframe/msg_pages/nncms_content_msg_list.php"><?php echo cms_get_lang('xxgl_xxlb');?></a></li>
				<li><a href="javascript:void(null);" pos="contentframe/msg_pages/nncms_content_msg_addedit.php?action=add"><?php echo cms_get_lang('xxgl_fbxx');?></a></li>
<!--				<li><a href="javascript:void(null);" pos="controls/user_select/nncms_controls_user_category_select.php?category_id=<?php echo $g_user_label_group[0]['nns_id'];?>"><?php echo cms_get_lang('xxgl_xxfz');?></a></li>-->
        		<li><a href="javascript:void(null);" pos="contentframe/devices_pages/nncms_content_user_category_addedit2.php?category_id=<?php echo $g_user_label_group[0]['nns_id'];?>"><?php echo cms_get_lang('xxgl_fzgl');?></a></li>
			</ul>
		</li>

	</ul>
</div>
</body>
</html>
