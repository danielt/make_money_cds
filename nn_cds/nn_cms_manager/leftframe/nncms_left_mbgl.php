<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../nncms_manager_inc.php");
 //导入语言包
 session_start();
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

 //加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/leftmenu.js"></script>
</head>

<body>
<div class="leftMenu">
	<ul class="sup_menu">

		<li><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('xtgl_mbgl');?></a>
			<ul class="sub_menu">
				<li><a href="javascript:void(null);" pos="contentframe/skin_pages/nncms_content_skinlist.php"><?php echo cms_get_lang('mbgl_mlcp').cms_get_lang('mbgl_mblb');?></a></li>
				<!--li><a href="javascript:void(null);" pos="contentframe/skin_pages/nncms_content_templatelist.php"><?php echo cms_get_lang('mbgl_kmbpz')?></a></li-->
				<li><a href="javascript:void(null);" pos="contentframe/skin_pages/nncms_content_template_bind_asset.php"><?php echo cms_get_lang('mbgl_mzbkmbbd')?></a></li>
				<li><a href="javascript:void(null);" pos="contentframe/skin_pages/nncms_content_template_bind_topic.php"><?php echo cms_get_lang('mbgl_xxbmbbd')?></a></li>
			</ul>
		</li>
		<li><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('xtgl_bqgl');?></a>
			<ul class="sub_menu">
				<li><a href="javascript:void(null);" pos="contentframe/skin_pages/nncms_content_template_label_list.php"><?php echo cms_get_lang('bqgl_bqlb');?></a></li>
				<li><a href="javascript:void(null);" pos="contentframe/skin_pages/nncms_content_template_label_addedit.php?action=add"><?php echo cms_get_lang('bqgl_tjbq');?></a></li>
			</ul>
		</li>
	</ul>
</div>
</body>
</html>
