<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
// ob_start();
include("../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
//导入语言包
session_start();
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include $nncms_db_path . "nns_carrier/nns_db_carrier_class.php";
$carrier_inst = new nns_db_carrier_class();
$result = $carrier_inst->nns_db_carrier_list();
if ($result["ret"] == 0) {
        $carrier_data = $result["data"][0];
} else if ($result["ret"] == 8032) {
        echo "<script>var bool=false;</script>";
}
// ob_end_clean();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../js/leftmenu.js"></script>
                <script language="javascript">
                        $(document).ready(function() {
                                supSelect($(".sup_menu > li:eq(0)"));
                        });
                </script>
        </head>

        <body>
                <div class="leftMenu">
                        <ul class="sup_menu">
                                <?php if ($_SESSION["nns_manager_type"] == 0) { ?>
                                        <li><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('xtgl_cmsgl'); ?></a>
                                                <ul class="sub_menu">
                                                        <li><a href="javascript:void(null);" pos="contentframe/nncms_content_global.php">&middot; <?php echo cms_get_lang('xtgl_cmsgl'); ?></a></li>
                                                        <li><a href="javascript:void(null);" pos="contentframe/nncms_content_carrier.php">&middot; <?php echo cms_get_lang('xtgl_yysgl'); ?></a></li>
                                                </ul>
                                        </li>

                                        <!--<ul class="sub_menu">
                                                            <li><a href="javascript:void(null);" pos="contentframe/nncms_content_rolelist.php">角色管理</a></li>
                                                            <li><a href="javascript:void(null);" pos="contentframe/nncms_content_userlist.php">管理员管理</a></li>
                                        </ul>-->

                                        <li><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('permission_management'); ?></a>
                                                <ul class="sub_menu">
                                                        <?php if (pub_func_get_module_right('101')) { ?>
                                                                <li><a href="javascript:void(null);" pos="contentframe/manager_pages/nncms_content_managerlist.php">&middot; <?php echo cms_get_lang('xtgl_glygl'); ?></a></li>
                                                        <?php } if (pub_func_get_module_right('102')) { ?>
                                                                <li ><a href="javascript:void(null);"   pos="contentframe/role_pages/nncms_content_rolelist.php">&middot; <?php echo cms_get_lang('xtgl_jsgl'); ?></a></li>
                                                        <?php } ?>
                                                </ul>
                                        </li>	
                                        <li><a href="javascript:void(null);"  >&middot; <?php echo cms_get_lang('action_log'); ?></a>
                                                <ul class="sub_menu">
                                                        <li><a href="javascript:void(null);" pos="contentframe/log_manager_pages/nncms_content_opera_log.php">&middot; <?php echo cms_get_lang('action_log'); ?></a></li>
                                                        <li><a href="javascript:void(null);" pos="contentframe/ckfinder/ckfinder.html">&middot; API日志</a></li>
														<li><a href="javascript:void(null);"  pos="contentframe/log_manager_pages/vod_online.view.php">&middot;  点播缺失</a> </li>
                                                        <li><a href="javascript:void(null);"  pos="contentframe/log_manager_pages/nncms_content_global_error_log.php">&middot;  全局错误日志</a> </li>
                                                        <?php if($_SESSION["nns_mgr_name"] == g_sys_loginid){?>
                                                        <li><a href="javascript:void(null);"  pos="contentframe/log_manager_pages/nncms_content_slow_log.php">&middot;  慢SQL日志</a> </li>
                                                        <li><a href="javascript:void(null);"  pos="contentframe/log_manager_pages/nncms_content_general_log.php">&middot;  全局SQL日志</a> </li>
                                                        <?php }?>
                                                </ul>
                                        </li>
                                        <li><a href="javascript:void(null);"  pos="contentframe/log_manager_pages/nncms_content_log_clean.php">&middot; <?php echo cms_get_lang('xtgl_rzql'); ?></a> </li>
                                        <?php
                                }
                                if (isset($_SESSION['nns_role_name']) && $_SESSION['nns_role_name'] !== "超级管理员") {
                                        ?>
                                        <li><a href="javascript:void(null);"  pos="contentframe/nncms_content_password.php">&middot; <?php echo cms_get_lang('xtgl_xgmm'); ?></a></li>
                                <?php } ?>
                        </ul>
                </div>
        </body>
</html>
