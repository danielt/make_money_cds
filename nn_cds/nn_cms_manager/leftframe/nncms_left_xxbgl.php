<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../nncms_manager_inc.php");
 //导入语言包
 session_start();
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include $nncms_db_path. "nns_common/nns_db_constant.php";

 //加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

//引入 topic logic
include $nncms_config_path.'nn_logic/topic/topic.class.php';

//获取dc
$dc = nl_get_dc(array(
			'db_policy'=>NL_DB_READ,
			'cache_policy'=>NP_KV_CACHE_TYPE_NULL
			)
		);
//连接数据库
$dc->open();

//获取信息包列表总数
$params['nns_type']=" ='std'  ";
$topic_list = nl_topic::get_topic_list($dc,null,null,0,50,NL_DC_AUTO,null,$params);
//echo '<pre>';
//print_r($topic_list);exit;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/leftmenu.js"></script>

</head>

<body>
<div class="leftMenu">
    <ul class="sup_menu">
    	<li><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('topic_xxbgl');?></a>
       		<ul class="sub_menu">
       			<li><a href="javascript:void(null);" pos="contentframe/topic_pages/nncms_content_topic_list.php">&middot; <?php echo cms_get_lang('topic_xxbgl_list');?></a></li>
       			<li><a href="javascript:void(null);" pos="contentframe/topic_pages/nncms_content_topic_addedit.php?action=add">&middot; <?php echo cms_get_lang('topic_xxbgl_add');?></a></li>
       		</ul>
        </li>
      
        	<li class="select_menu"><a href="javascript:void(null);" >&middot; <?php echo cms_get_lang('topic_xxbgl_content');?></a>
        	<ul class="sub_menu">
    			<?php  if(is_array($topic_list)) {foreach($topic_list as $key=>$val){ ?>
                <li><a href="javascript:void(null);" pos="contentframe/topic_pages/nncms_content_topic_tree_addedit.php?topic_id=<?php echo $val['nns_id'];?>"><?php echo $val['nns_name'];?></a></li>
				<?php } }?>
            </ul>
        </li>
    </ul>
</div>
</body>
</html>
