<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * @author txc by 2013-1-22
 * 
 */
 include("../nncms_manager_inc.php");
//加载多语言
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
 //导入语言包
 session_start();
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";
include $nncms_db_path. "nns_common/nns_db_constant.php";
//include_once $nncms_config_path."nn_logic/schedule/schedule.class.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//liD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/liD/xhtml1-transitional.lid">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/leftmenu.js"></script>

</head>

<body>
<div class="leftMenu">
	<ul class="sup_menu" >

		<li><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('ex_package|ysjgl');?></a>
			<ul class="sub_menu">
				<li><a href="javascript:void(null);" pos="contentframe/web_ex_pages/nncms_content_web_ex_list.php"><?php echo cms_get_lang('webkz_ysjlb');?></a></li>            
			</ul>
		</li>

	</ul>
</div>
</body>
</html>
