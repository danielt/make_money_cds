<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include ("../nncms_manager_inc.php");
//导入语言包
session_start();
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//加载多语言
include_once ($nncms_config_path . 'nn_cms_config/nn_cms_global.php');
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . 'nn_logic/cp/cp.class.php';
include $nncms_config_path . 'nn_logic/nl_common.func.php';
//$message_list = '';
//$import_mode = g_cms_config::get_g_config_value('g_import_mode');
//switch ((string)$import_mode)
//{
//	case 'cntv_me':
//		$message_list = 'nncms_content_cp_cntv_message_list.php';
//		break;
//	default:
//		$message_list = 'nncms_content_cp_message_list.php?org_id=' . $import_mode;
//		break;
//}
$dc = nl_get_dc(array (
	'db_policy' => NL_DB_WRITE,
	'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$dc->open();
$result = nl_cp::query_all($dc);
if($result['ret'] !=0)
{
	echo '<script>alert("'.$result['reason'].'");history.go(-1);</script>';die;
}
$result_cp_live = $result_cp_live_index = $result_cp_live_media = $result_cp_playbill = null;

$menu_array = array(
		'live'=>'直播频道',
		'live_index'=>'直播分集',
		'live_media'=>'直播片源',
		'playbill'=>'节目单',
); 
$menu_array_value = array();
if(isset($result['data_info']) && is_array($result['data_info']) && !empty($result['data_info']))
{
	foreach ($result['data_info'] as $val)
	{
		$str_cp_id = $val['nns_id'];
		$cp_config = (strlen($val['nns_config']) >0) ? json_decode($val['nns_config'],true) : null;
		if(!isset($cp_config['cdn_receive_import_enable']) || $cp_config['cdn_receive_import_enable'] != 1)
		{
			continue;
		}
		foreach ($menu_array as $menu_key=>$menu_name)
		{
			
			$temp_key = 'cdn_handele_'.$menu_key.'_enable';
			if(isset($cp_config[$temp_key]) && $cp_config[$temp_key] == 1)
			{
				$menu_array_value[$menu_key][$val['nns_id']] = $val['nns_name'];
			}
		}
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../js/leftmenu.js"></script>
	</head>
	<body>
		<div class="leftMenu">
			<?php if(isset($menu_array_value) && !empty($menu_array_value) && is_array($menu_array_value)){ ?>
				<?php foreach ($menu_array_value as $menu_id=>$menu_val){?>
				<ul class="sup_menu">
					<li>
						<a href="javascript:void(null);">&middot; <?php echo $menu_array[$menu_id]?></a>
						<?php foreach($menu_val as $key=>$val){?>
							<ul class="sub_menu">
								<li>
									<a href="javascript:void(null);" pos="contentframe/cdn_pages/nncms_cdn_<?php echo $menu_id;?>_queue.php?cp_id=<?php echo $key;?>"><?php echo $val;?></a>
								</li>
							</ul>
						<?php } ?>
					</li>
				</ul>
				<ul class="sup_menu">
					<li>
						<a href="javascript:void(null);">&middot; <?php echo $menu_array[$menu_id].'日志'?></a>
						<?php foreach($menu_val as $key=>$val){?>
							<ul class="sub_menu">
								<li>
									<a href="javascript:void(null);" pos="contentframe/cdn_pages/nncms_cdn_<?php echo $menu_id;?>_queue_log.php?cp_id=<?php echo $key;?>"><?php echo $val;?></a>
								</li>
							</ul>
						<?php } ?>
					</li>
				</ul>
				<?php }?>
			<?php }?>
			
		</div>
	</body>
</html>
