<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
  include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
 //导入语言包
 session_start();
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/leftmenu.js"></script>
</head>

<body>
<div class="leftMenu">

	<ul class="sup_menu">
	<?php if ($g_web_mode==1){?>
	   <li><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('mbym|stat');?></a>
		<ul class="sub_menu">
		   <li><a pos="contentframe/log_manager_pages/nncms_content_view_record.php" href="javascript:void(null);" >&middot; <?php echo cms_get_lang('mbym_visit_record');?></a></li>
			<li><a pos="contentframe/log_manager_pages/nncms_content_view_record_5.php" href="javascript:void(null);" >&middot; <?php echo cms_get_lang('play_live_template_page_visit');?></a></li>
		   <li class="select_menu"><a pos="contentframe/log_manager_pages/nncms_content_access_count_5.php" href="javascript:void(null);" >&middot; <?php echo cms_get_lang('stat_ssfwtj');?></a></li>
		   <li class="select_menu"><a pos="contentframe/log_manager_pages/nncms_content_template_select.php" href="javascript:void(null);" >&middot; <?php echo cms_get_lang('mbym_stat_graphic');?></a></li>
	   </ul>
		</li>
		<?php }?>
		<li><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('play_log_sum');?></a>
			<ul class="sub_menu">
				<li><a pos="contentframe/log_manager_pages/nncms_content_play_log.php" href="javascript:void(null);">&middot; <?php echo cms_get_lang('play_history');?></a></li>
				<li><a pos="contentframe/log_manager_pages/nncms_content_play_log_5.php" href="javascript:void(null);">&middot; <?php echo cms_get_lang('play_live_record');?></a></li>
				<li><a pos="contentframe/log_manager_pages/nncms_content_play_log_count_5.php" href="javascript:void(null);">&middot; <?php echo cms_get_lang('stat_ssbftj');?></a></li>
				<li><a pos="contentframe/log_manager_pages/nncms_content_play_log_report.php" href="javascript:void(null);">&middot; <?php echo cms_get_lang('stat_bftjtb');?></a></li>
				<li><a pos="contentframe/log_manager_pages/nncms_content_play_log_count_user.php" href="javascript:void(null);">&middot; <?php echo cms_get_lang('stat_yfbftj');?></a></li>
			</ul>
		</li>
	</ul>
</div>
</body>
</html>
