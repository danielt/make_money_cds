<?php
/**
 * Leftmenu of Weather management
 * 
 * @author wuha
 * @version $Id$
 */
include ("../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
?>
<!DOCTYPE HTML>
<html>
        <head>
                <meta charset="utf-8">
                <title></title>
                <link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../js/leftmenu.js"></script>
        </head>

        <body>
                <div class="leftMenu">
                        <ul class="sup_menu" >

                                <li><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('weather_management'); ?></a>
                                        <ul class="sub_menu">
                                                <li><a href="javascript:void(null);" pos="contentframe/weather_manage_pages/nncms_content_weather_pic_list.php"><?php echo cms_get_lang('weather_pic_management'); ?></a></li>
                                                <li><a href="javascript:void(null);" pos="contentframe/weather_manage_pages/nncms_content_weather_pic_test.php">天气接口测试</a></li>

                                        </ul>
                                </li>
                        </ul>
                </div>
        </body>
</html>