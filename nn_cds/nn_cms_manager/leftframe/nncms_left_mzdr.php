<?php
ob_start();
header("Content-Type:text/html;charset=utf-8");
include("../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
//导入语言包

session_start();
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
$action=$_GET["action"];
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_db_path. "nns_common/nns_db_constant.php";
include $nncms_db_path. "nns_assist/nns_db_assist_class.php";
$assist_inst=new nns_db_assist_class();
$result=$assist_inst->nns_db_assist_list($_SESSION["nns_org_id"],$_SESSION["nns_manager_type"]);
ob_end_clean();
?>
<!DOCTYPE html PUBLIC "-//W3C//liD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/liD/xhtml1-transitional.lid">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/leftmenu.js"></script>
<script language="javascript">
function refresh_category_list(){
	window.location.href="nncms_left_mzgl.php?action=refresh";
}

</script>
</head>
<body>
<div class="leftMenu">
	<ul class="sup_menu" >
		<li><a href="javascript:void(null);"  pos="contentframe/asset_import_pages/nncms_content_asset_import_list.php">&middot; <?php echo cms_get_lang('media_inject_list');?></a> </li>
		<li><a href="javascript:void(null);"  pos="contentframe/asset_import_pages/nncms_content_asset_insert_vod.php">&middot;  <?php echo cms_get_lang('inject_media_manage');?></a>
		<li><a href="javascript:void(null);"  pos="contentframe/asset_import_pages/nncms_content_asset_import_set_list.php">&middot;  <?php echo cms_get_lang('inject_setup');?></a></li>
	</ul>
</div>
</body>
</html>