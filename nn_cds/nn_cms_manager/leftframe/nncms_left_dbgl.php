<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../nncms_manager_inc.php");
 // 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
 //导入语言包
 session_start();
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";
include $nncms_db_path. "nns_common/nns_db_constant.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//liD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/liD/xhtml1-transitional.lid">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/leftmenu.js"></script>

</head>

<body>
<div class="leftMenu">
	<ul class="sup_menu" >

		<li><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('movie_demand_manage');?></a>
			<ul class="sub_menu">
				<li><a href="javascript:void(null);" pos="contentframe/vod_pages/nncms_content_vodlist.php?view_type=0"><?php echo cms_get_lang('media_dylb');?></a></li>
				<li><a href="javascript:void(null);" pos="contentframe/vod_pages/nncms_content_vodlist.php?view_type=0&view_audit=<?php echo NNS_VOD_CHECK_WAITING;?>"><?php echo cms_get_lang('media_ddsh|media_dylb');?></a></li>
				<li><a href="javascript:void(null);" pos="contentframe/vod_pages/nncms_content_vod_search.php?view_type=0"><?php echo cms_get_lang('search|media_dy');?></a></li>
				<li><a href="javascript:void(null);" pos="contentframe/vod_pages/nncms_content_vod_addedit.php?action=add&view_type=0"><?php echo cms_get_lang('add|media_dy');?></a></li>
				<li><a href="javascript:void(null);" pos="contentframe/vod_pages/nncms_content_vodlist.php?view_type=0&view_delete_type=1"><?php echo cms_get_lang('delete|media_dy');?></a></li>
				<li><a href="javascript:void(null);" pos="contentframe/vod_pages/nncms_content_vodlist.php?view_type=0&view_delete=<?php echo NNS_VOD_DELETED_TRUE;?>"><?php echo cms_get_lang('deleted|media_dylb');?></a></li>

			</ul>
		</li>
	   <li><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('movie_demand_manage');?></a>
			<ul class="sub_menu">
				<li><a href="javascript:void(null);" pos="contentframe/vod_pages/nncms_content_vodlist.php?view_type=1"><?php echo cms_get_lang('media_lxjlb');?></a></li>

				<li><a href="javascript:void(null);" pos="contentframe/vod_pages/nncms_content_vodlist.php?view_type=1&view_audit=<?php echo NNS_VOD_CHECK_WAITING;?>"><?php echo cms_get_lang('media_ddsh|media_lxjlb');?></a></li>
				<li><a href="javascript:void(null);" pos="contentframe/vod_pages/nncms_content_vod_search.php?view_type=1"><?php echo cms_get_lang('search|media_lxj');?></a></li>
				<li><a href="javascript:void(null);" pos="contentframe/vod_pages/nncms_content_vod_addedit.php?action=add&view_type=1"><?php echo cms_get_lang('add|media_lxj');?></a></li>
				<li><a href="javascript:void(null);" pos="contentframe/vod_pages/nncms_content_vodlist.php?view_type=1&view_delete_type=1"><?php echo cms_get_lang('delete|media_lxj');?></a></li>
				<li><a href="javascript:void(null);" pos="contentframe/vod_pages/nncms_content_vodlist.php?view_type=1&view_delete=<?php echo NNS_VOD_DELETED_TRUE;?>"><?php echo cms_get_lang('deleted|media_lxjlb');?></a></li>
			</ul>
		</li>

	</ul>
</div>
</body>
</html>
