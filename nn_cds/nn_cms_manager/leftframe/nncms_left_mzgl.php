<?php
 ob_start();
header("Content-Type:text/html;charset=utf-8");
include("../nncms_manager_inc.php");
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
session_start();
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
$action=$_GET["action"];
//导入语言包
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_db_path. "nns_common/nns_db_constant.php";
include $nncms_db_path. "nns_assist/nns_db_assist_class.php";
$assist_inst=new nns_db_assist_class();
$result=$assist_inst->nns_db_assist_list($_SESSION["nns_org_id"],$_SESSION["nns_manager_type"]);
ob_end_clean();
?>
<!DOCTYPE html PUBLIC "-//W3C//liD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/liD/xhtml1-transitional.lid">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript">
	function refresh_category_list(){

		window.location.href="nncms_left_mzgl.php?action=refresh";
	}
</script>
<script language="javascript" src="../js/leftmenu.js"></script>

</head>
<body>
<div class="leftMenu">
	<ul class="sup_menu" >
		<li><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('assist_gl');?></a>
			<ul class="sub_menu">
				<li><a href="javascript:void(null);" pos="contentframe/assist_pages/nncms_content_assistlist.php"><?php echo cms_get_lang('assist_lb');?></a></li>
				<li><a href="javascript:void(null);" pos="contentframe/assist_pages/nncms_content_assist_add.php?action=add"><?php echo cms_get_lang('add|assist');?></a></li>
			</ul>
		</li>
		<li class="select_menu"><a href="javascript:refresh_category_list();">&middot; <?php echo cms_get_lang('assist_nrgl');?></a>
			<ul class="sub_menu">
				<!-- 2012-8-20 陈波 1661507441@qq.com 添加服务包列表没数据时的提示   start -->
				<?php if ($result["ret"]==0){
					$data=$result["data"];
					if ($data!=null){
						foreach ($data as $item){?>
						 <li><a href="javascript:void(null);" pos="contentframe/assist_pages/nncms_content_assist_tree_addedit.php?nns_id=<?php echo $item["nns_id"];?>"><?php echo $item["nns_name"]."(".$item["nns_id"].")";?></a></li>
					<?php }}}else{?>
						<li><?php echo cms_get_lang('assist_ex_1')?></li>
						<?php }?>
					<!-- 2012-8-20 陈波 1661507441@qq.com 添加服务包列表没数据时的提示   end	-->
			</ul>
		</li>

	</ul>
</div>
</body>
</html>