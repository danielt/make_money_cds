<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include ("../nncms_manager_inc.php");
//导入语言包
session_start();
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//加载多语言
include_once ($nncms_config_path . 'nn_cms_config/nn_cms_global.php');
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . 'nn_logic/cp/cp.class.php';
include $nncms_config_path . 'nn_logic/nl_common.func.php';
//$message_list = '';
//$import_mode = g_cms_config::get_g_config_value('g_import_mode');
//switch ((string)$import_mode)
//{
//	case 'cntv_me':
//		$message_list = 'nncms_content_cp_cntv_message_list.php';
//		break;
//	default:
//		$message_list = 'nncms_content_cp_message_list.php?org_id=' . $import_mode;
//		break;
//}
$dc = nl_get_dc(array (
	'db_policy' => NL_DB_WRITE,
	'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$dc->open();
$cp_list = nl_cp::query_all($dc,'0');
$cp_list_data_info = $cp_list['data_info'];


$cp_list = nl_cp::query_all($dc,'1');
$cp_list_data_info_fuse = $cp_list['data_info'];
//var_dump($cp_list_data_info);die;
//-->是否显示临时消息
$bool_display_temp_message = false;
$bool_asyn_feedback_message_enabled = false;
$bool_delayed_message_import_enabled = false;
if(is_array($cp_list_data_info))
{
	foreach($cp_list_data_info as $key=>$cp_list)
	{
		$arr_temp_config = isset($cp_list['nns_config'])?json_decode($cp_list['nns_config'],true):null;
		if(isset($arr_temp_config['temp_message_enabled']) && $arr_temp_config['temp_message_enabled']==1)
		{
			$bool_display_temp_message = true;
		}
		if(isset($arr_temp_config['asyn_feedback_message_enabled']) && $arr_temp_config['asyn_feedback_message_enabled'] == 1)
		{
			$bool_asyn_feedback_message_enabled = true;
		}
		if(isset($arr_temp_config['delayed_message_import_enabled']) && $arr_temp_config['delayed_message_import_enabled'] == 1)
		{
			$bool_delayed_message_import_enabled = true;
		}
	}
}
global $g_fuse_enabled;
//var_dump($bool_display_temp_message);die;
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../js/leftmenu.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {
	            window.parant.resetFrameHeight();
	        });
		</script>
	</head>
	<body>
		<div class="leftMenu">
			<ul class="sup_menu">
				<li>
					<a href="javascript:void(null);">&middot; 上游平台管理</a>
					<ul class="sub_menu">
						<li>
							<a href="javascript:void(null);" pos="contentframe/cp_pages/nncms_content_cp_info_list.php">上游信息列表</a>
						</li>
						<li>
							<a href="javascript:void(null);" pos="contentframe/cp_pages/nncms_cp_childs_list.php">上游子集列表</a>
						</li>
					</ul>
				</li>
			</ul>
			<ul class="sup_menu">
				<li>
					<a href="javascript:void(null);">&middot; 上游提供商管理</a>
					<ul class="sub_menu">
						<li>
							<a href="javascript:void(null);" pos="contentframe/producer/nncms_producer_list.php">上游提供商列表</a>
						</li>
						<li>
							<a href="javascript:void(null);" pos="contentframe/product/nncms_product_list.php">产品包列表</a>
						</li>
					</ul>
				</li>
			</ul>
			<?php if($g_fuse_enabled){?>
            <ul class="sup_menu">
				<li>				
					<a href="javascript:void(null);">&middot; CP融合管理</a>
					<ul class="sub_menu">
                    	<li><a href="javascript:void(null);" pos="contentframe/project_fuse/nncms_fuse_cp_list.php">CP融合列表</a></li>
                    </ul>
				</li>
			</ul>
			<?php }?>
			<?php if(isset($cp_list_data_info)&&$cp_list_data_info!=NUll){ ?>
			<ul class="sup_menu">
					<li>
						<a href="javascript:void(null);">&middot; 上游平台消息管理</a>
						<?php
						if(is_array($cp_list_data_info))foreach($cp_list_data_info as $cp_list){
                        ?>
							<ul class="sub_menu">
								<li>
									<a href="javascript:void(null);" pos="contentframe/cp_pages/nncms_content_cp_message_list.php?org_id=<?php echo $cp_list['nns_id'];?>"><?php echo $cp_list['nns_name'];?></a>
								</li>
							</ul>
						<?php } ?>
					</li>
			</ul>
			<?php } ?>
			<?php if(isset($cp_list_data_info_fuse)&&$cp_list_data_info_fuse!=NUll&&$g_fuse_enabled){ ?>
			<ul class="sup_menu">
					<li>
						<a href="javascript:void(null);">&middot; 上游融合平台消息管理</a>
						<?php
						if(is_array($cp_list_data_info_fuse))foreach($cp_list_data_info_fuse as $cp_list){
                        ?>
							<ul class="sub_menu">
								<li>
									<a href="javascript:void(null);" pos="contentframe/cp_pages/nncms_content_cp_message_list.php?org_id=<?php echo $cp_list['nns_id'];?>"><?php echo $cp_list['nns_name'];?></a>
								</li>
							</ul>
						<?php } ?>
					</li>
			</ul>
			<?php } ?>
			<?php if($bool_display_temp_message){ ?>
			<ul class="sup_menu">
				<li>				
					<a href="javascript:void(null);">&middot; 上游平台临时消息管理</a>
					<?php
					if(is_array($cp_list_data_info))
					{
						foreach($cp_list_data_info as $cp_list){
							$cp_list_config_info = (isset($cp_list['nns_config']) && !empty($cp_list['nns_config'])) ? json_decode($cp_list['nns_config'],true) : null;
							$temp_message_enabled = isset($cp_list_config_info['temp_message_enabled'])?$cp_list_config_info['temp_message_enabled']:NULL;
							if($temp_message_enabled == 1){?>
								<ul class="sub_menu">
									<li>
										<a href="javascript:void(null);" pos="contentframe/cp_pages/nncms_content_cp_temp_message_list.php?org_id=<?php echo $cp_list['nns_id'];?>"><?php echo $cp_list['nns_name'];?></a>
									</li>
								</ul>
							<?php }
						}
					} ?>
				</li>
			</ul>
			<?php }?>
			
			<?php if($bool_asyn_feedback_message_enabled){ ?>
                <ul class="sup_menu">
                    <li>
                        <a href="javascript:void(null);">&middot; 异步反馈消息队列</a>
                        <?php
                        if(is_array($cp_list_data_info))foreach($cp_list_data_info as $cp_list){
							$cp_list_config_info = (isset($cp_list['nns_config']) && !empty($cp_list['nns_config'])) ? json_decode($cp_list['nns_config'],true) : null;
                            $asyn_feedback_message_enabled=isset($cp_list_config_info['asyn_feedback_message_enabled'])?$cp_list_config_info['asyn_feedback_message_enabled']:NULL;
							?>
							<?php if($asyn_feedback_message_enabled==1){ ?>
                                <ul class="sub_menu">
                                <li>
                                    <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_notify_message_queue.php?cp_id=<?php echo $cp_list['nns_id'];?>"><?php echo $cp_list['nns_name'];?></a>
                                </li>
                                </ul>
							<?php } ?>

                        <?php } ?>
                    </li>
                </ul>
			<?php }?>
			
			<?php if($bool_delayed_message_import_enabled){ ?>
			<ul class="sup_menu">
				<li>				
					<a href="javascript:void(null);">&middot; 上游平台消息延时注入队列</a>
					<?php
					if(is_array($cp_list_data_info))
					{
						foreach($cp_list_data_info as $cp_list){
							$cp_list_config_info = (isset($cp_list['nns_config']) && !empty($cp_list['nns_config'])) ? json_decode($cp_list['nns_config'],true) : null;
							$temp_message_enabled = isset($cp_list_config_info['delayed_message_import_enabled'])?$cp_list_config_info['delayed_message_import_enabled']:NULL;
							if($temp_message_enabled == 1){?>
								<ul class="sub_menu">
									<li>
										<a href="javascript:void(null);" pos="contentframe/cp_pages/nncms_content_delayed_message_queue.php?cp_id=<?php echo $cp_list['nns_id'];?>"><?php echo $cp_list['nns_name'];?></a>
									</li>
								</ul>
							<?php }
						}
					} ?>
				</li>
			</ul>
			<?php }?>
		</div>
	</body>
</html>
