<?php
header("Content-Type:text/html;charset=utf-8");
include dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'base.inc.php';
include ("../nncms_manager_inc.php");
// 导入多语言包
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
//导入语言包
session_start();
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
$dc = load_dc();
load_logic('sp_asset','sp_asset');
$data = nl_sp_asset::get_sp_list($dc);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../js/leftmenu.js"></script>
	</head>
	<body>
		<div class="leftMenu">
			<ul class="sup_menu">
				<li>
					<a href="javascript:void(null);">&middot; 任务列表</a>
					<ul class="sub_menu">
						<li>
							<a href="javascript:void(null);" pos="contentframe/mgtv_asset_pages/nncms_category_task.php">C2_TASK</a>
						</li>
					</ul>
				</li>
				<?php
				if(is_array($data)&&!empty($data)){
				?>
				<li>
					<a href="javascript:void(null);">&middot; 栏目内容管理</a>
					<ul class="sub_menu">
						<?php
						foreach ($data as $val) {

						?>
						<li>
							<a href="javascript:void(null);" pos="contentframe/sp_content_pages/nncms_content_bk.php?sp_id=<?php echo $val['nns_id'];?>"><?php echo $val['nns_name'];?></a>
						</li>
						<?php
						}
						?>
					</ul>
				</li>
				<?php
				}
				?>
                <li>
                    <a href="javascript:void(null);">&middot; 栏目树管理</a>
                    <ul class="sub_menu">
                        <li>
                            <a href="javascript:void(null);" pos="contentframe/mgtv_asset_pages/nncms_category_list.php">栏目树管理</a>
                        </li>
                    </ul>
                </li>
			</ul>
		</div>
	</body>
</html>
