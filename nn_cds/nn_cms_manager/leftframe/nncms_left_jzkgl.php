<?php
/**
 * Created by PhpStorm.
 * Use : 介质库管理
 * User: kan.yang@starcor.cn
 * Date: 18-3-26
 * Time: 上午9:40
 */

header("Content-Type:text/html;charset=utf-8");
include("../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//导入语言包
session_start();
if (isset($_SESSION["language_dir"])){
    $language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
    <link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
    <script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
    <script language="javascript" src="../js/leftmenu.js"></script>
</head>

<body>
<div class="leftMenu">
    <ul class="sup_menu">
        <li ><a href="javascript:void(null);" >&middot; 介质库管理</a>
            <ul class="sub_menu">
                <li><a href="javascript:void(null);" pos="contentframe/medium/nncms_content_medium_server.php?nns_manager_type=2">服务器管理</a></li>
                <li><a href="javascript:void(null);" pos="contentframe/medium/nncms_content_medium_disk.php?nns_manager_type=2">磁盘管理</a></li>
                <li><a href="javascript:void(null);" pos="contentframe/medium/nncms_content_medium_cp.php?nns_manager_type=2">CP管理</a></li>
                <li><a href="javascript:void(null);" pos="contentframe/medium/nncms_content_medium_asset.php?nns_manager_type=2">分类管理</a></li>
                <li><a href="javascript:void(null);" pos="contentframe/medium/nncms_content_medium_file.php?nns_manager_type=2">文件管理</a></li>
            </ul>
        </li>
        <li ><a href="javascript:void(null);" >&middot; 介质库杂项管理</a>
            <ul class="sub_menu">
                <li><a href="javascript:void(null);" pos="contentframe/medium/nncms_medium_library_filter_extension_list.php?nns_manager_type=2">片源扩展文件管理</a></li>
                <li><a href="javascript:void(null);" pos="contentframe/medium/nncms_medium_library_type_list.php?nns_manager_type=2">媒资类型管理</a></li>
            </ul>
        </li>
    </ul>
</div>
</body>
</html>
