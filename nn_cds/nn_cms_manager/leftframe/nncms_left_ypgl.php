<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
//导入语言包
session_start();
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
include $nncms_db_path . "nns_common/nns_db_constant.php";
include $nncms_config_path . 'nn_logic/cp/cp.class.php';
include $nncms_config_path . 'nn_logic/queue/queue.class.php';
include $nncms_config_path . 'nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
	'db_policy' => NL_DB_WRITE,
	'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$dc->open();
$cp_list = nl_cp::query_file_pack_state($dc,1);
$cp_list_data_info = $cp_list['data_info'];

$result_queue = nl_queue::query_by_queue_id($dc,'thrid_full_assests');


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />

                <script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../js/leftmenu.js"></script>
        </head>
        <body>
            <div class="leftMenu">
                <ul class="sup_menu">
                    <li class="select_menu">
                        <a href="javascript:void(null);" >&middot; <?php echo cms_get_lang('media_dbgl'); ?></a>
                        <ul class="sub_menu">
                            <li><a href="javascript:void(null);" pos="contentframe/vod_pages/nncms_content_vod.php"><?php echo cms_get_lang('media_playbill_dbnrgl'); ?></a></li>
                            <li><a href="javascript:void(null);" pos="contentframe/vod_pages/nncms_content_vod_tree_addedit.php"><?php echo cms_get_lang('media_dbfl'); ?></a></li>
							<li><a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_index_seekpoint_import.php"><?php echo cms_get_lang('media_index_seekpoint_import'); ?></a></li>
							<li><a href="javascript:void(null);" pos="contentframe/content_import/nncms_content_media_import.php"><?php echo "第三方片源绑定"; ?></a></li>
							<li><a href="javascript:void(null);" pos="contentframe/content_download/nncms_content_download.php"><?php echo "媒资片源下载"; ?></a></li>
                            <li><a href="javascript:void(null);" pos="contentframe/content_import/nncms_content_stock_import.php"><?php echo "存量CSV注入"; ?></a></li>
                        	<li><a href="javascript:void(null);" pos="contentframe/content_import/nncms_content_stock_import_by_csv.php">媒资导入</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(null);">&middot; <?php echo cms_get_lang('media_zbgl'); ?></a>
                        <ul class="sub_menu">
                            <li><a href="javascript:void(null);" pos="contentframe/live_pages/nncms_content_live.php"><?php echo cms_get_lang('media_playbill_zbnrgl'); ?></a></li>
                            <li><a href="javascript:void(null);" pos="contentframe/third_live/nncms_third_live_list.php"><?php echo '第三方频道绑定列表'; ?></a></li>
                            <li><a href="javascript:void(null);" pos="contentframe/third_live/nncms_bigtv_bind_list.php"><?php echo '大电视绑定列表'; ?></a></li>
                            <li><a href="javascript:void(null);" pos="contentframe/live_pages/nncms_content_live_tree_addedit.php"><?php echo cms_get_lang('media_zbfl'); ?></a></li>
                            <li><a href="javascript:void(null);" pos="contentframe/playbill_pages/nncms_content_playbill.php"><?php echo cms_get_lang('media_playbill_jmdlb'); ?></a></li>
                            <li><a href="javascript:void(null);" pos="contentframe/playbill_pages/nncms_content_playbill_import.php"><?php echo cms_get_lang('media_playbill_jmddr'); ?></a></li>
                        </ul>
                    </li>
                    <?php if(isset($result_queue['data_info']) && is_array($result_queue['data_info']) && !empty($result_queue['data_info'])){?>
                    <li>
                        <a href="javascript:void(null);">&middot; 媒资导入</a>
                        <ul class="sub_menu">
                            <li><a href="javascript:void(null);" pos="contentframe/full_assets/nncms_full_assets_list.php">全量媒资列表</a></li>
                            <li><a href="javascript:void(null);" pos="contentframe/full_assets/nncms_full_assets_import.php">媒资文件上传</a></li>
                        </ul>
                    </li>
                    <?php }?>
                    <?php if(is_array($cp_list_data_info) && !empty($cp_list_data_info)){ ?>
                        <li class="select_menu">
                        <a href="javascript:void(null);" >&middot; 文件资源包</a>
                        <ul class="sub_menu">
                            <?php foreach ($cp_list_data_info as $cp_val){?>
                            <li><a href="javascript:void(null);" pos="contentframe/file_package/nncms_file_package_list.php?cp_id=<?php echo $cp_val['nns_id'];?>"><?php echo $cp_val['nns_id'].'/'.$cp_val['nns_name']; ?></a></li>
                            <?php }?>
                        </ul>
                    </li>
                    <?php }?>
                    <li>
                        <a href="javascript:void(null);">&middot; 服务类型配置</a>
                    	<ul class="sub_menu">
                        	<li><a href="javascript:void(null);" pos="contentframe/service_type/nncms_service_type_list.php">服务类型管理</a></li>
                        </ul>
                    </li>
                    <?php if(isset($result_queue['data_info']) && is_array($result_queue['data_info']) && !empty($result_queue['data_info'])){?>
                    <li>
                        <a href="javascript:void(null);">&middot; 媒资导入</a>
                        <ul class="sub_menu">
                            <li><a href="javascript:void(null);" pos="contentframe/full_assets/nncms_full_assets_list.php">全量媒资列表</a></li>
                            <li><a href="javascript:void(null);" pos="contentframe/full_assets/nncms_full_assets_import.php">媒资文件上传</a></li>
                            <li><a href="javascript:void(null);" pos="contentframe/full_assets/nncms_full_assets_site_list.php">站点列表</a></li>
                            <li><a href="javascript:void(null);" pos="contentframe/full_assets/nncms_full_assets_site_import.php">站点文件上传</a></li>
                        </ul>
                    </li>
                    <?php }?>
                </ul>
            </div>

        </body>
</html>
