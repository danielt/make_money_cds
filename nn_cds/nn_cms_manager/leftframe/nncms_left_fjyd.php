<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include ("../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
//导入语言包
session_start();
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
include $nncms_db_path . "nns_common/nns_db_constant.php";
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv/models/sp_model.php';

$sp_id = $_GET['sp_id'];
$data = sp_model::get_sp_list();
$lab_name = "";
foreach ($data as  $value) {
	if($value['nns_id']==$sp_id){
		$lab_name = $value['nns_name'];
		break;
	}
}
?>
<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />

                <script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../js/leftmenu.js"></script>

        </head>

        <body>
                <div class="leftMenu">
                        <ul class="sup_menu">
                                <li class="select_menu">
                                        <a href="javascript:void(null);" >&middot; <?php echo $lab_name;?></a>
                                        <ul class="sub_menu">
                                        	<?php 
                                            $pri_bool6=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"135008");
											if($pri_bool6){
                                            ?>
					                        <li>
					                            <a href="javascript:void(null);" pos="contentframe/sp_content_pages/nncms_mgtv_import_count.php?sp_id=<?php echo $sp_id;?>">数据统计</a>
					                        </li> 
					                        <?php } ?>
                                            <?php 
                                            $pri_bool1=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"135001");											
											if($pri_bool1){
                                            ?>
                                            <!--
                                            <li>
                                                	<a href="javascript:void(null);" pos="contentframe/sp_content_pages/nncms_content_bk.php?sp_id=<?php echo $sp_id;?>">
                                                		内容管理
                                                	</a>
                                              </li>
                                             -->
                                            <?php } ?>
                                            <?php 
                                            $pri_bool2=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"135002");
											if($pri_bool2){
                                            ?>
                                            <li>
                                                	<a href="javascript:void(null);" pos="contentframe/sp_content_pages/nncms_content_task.php?sp_id=<?php echo $sp_id;?>">
                                                		内容注入任务列表
                                                	</a>
                                              </li>
                                               <?php } ?>
                                            <?php 
                                            $pri_bool3=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"135003");
											if($pri_bool3){
                                            ?>
                                            <li>
                                                	<a href="javascript:void(null);" pos="contentframe/sp_content_pages/nncms_content_clip_task.php?sp_id=<?php echo $sp_id;?>">
                                                		切片任务列表
                                                	</a>
                                              </li> 
                                            <?php } ?>
                                            <?php 
                                            $pri_bool4=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"135004");
											if($pri_bool4){
                                            ?>
                                            <li>
                                                	<a href="javascript:void(null);" pos="contentframe/sp_content_pages/nncms_content_c2_log_list.php?sp_id=<?php echo $sp_id;?>">
                                                		注入华为命令列表
                                                	</a>
                                              </li>
                                              <?php } ?>
                                            <?php 
                                            $pri_bool5=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"135005");
											if($pri_bool5){
                                            ?>                                                                                               
					                        <li>
					                            <a href="javascript:void(null);" pos="contentframe/sp_content_pages/nncms_category_list.php?sp_id=<?php echo $sp_id;?>">栏目管理</a>
					                        </li>
					                        <?php } ?>
					                        
					                        <?php 
                                            $pri_bool6=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"135006");
											if($pri_bool6){
                                            ?>                                                                                               
					                        <li>
					                            <a href="javascript:void(null);" pos="contentframe/sp_content_pages/nncms_content_log.php?sp_id=<?php echo $sp_id;?>">内容注入日志</a>
					                        </li>
					                        <?php } ?>
					                        
					                        
					                        <?php 
                                            $pri_bool6=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"135007");
											if($pri_bool6){
                                            ?>                                                                                               
					                        <li>
					                            <a href="javascript:void(null);" pos="contentframe/sp_content_pages/nncms_mgtv_import_log.php?sp_id=<?php echo $sp_id;?>">MGTV注入日志</a>
					                        </li>
					                        <?php } ?>
					                        
					                        <?php 
                                            $pri_bool6=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"135008");
											if($pri_bool6){
                                            ?>                                                                                               
					                        <li>
					                            <a href="javascript:void(null);" pos="contentframe/sp_content_pages/nncms_mgtv_index.php?sp_id=<?php echo $sp_id;?>">首页推荐海报</a>
					                        </li>
					                        <?php } ?>
					                        
					                        
					                        
					                        <?php 
                                            $pri_bool6=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"135010");
											if($pri_bool6){
                                            ?>                                                                                               
					                        <li>
					                            <a href="javascript:void(null);" pos="contentframe/sp_content_pages/nncms_mgtv_epg_log.php?sp_id=<?php echo $sp_id;?>">EPG注入日志</a>
					                        </li>
					                        <?php } ?> 
					                                                                  
                                        </ul>
                                </li>
                        </ul>
                </div>

        </body>
</html>
