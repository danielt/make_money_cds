<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../nncms_manager_inc.php");
 //导入语言包
 session_start();
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";
include_once $nncms_config_path. "nn_cms_manager/nncms_mlang.php";
include $nncms_db_path. "nns_common/nns_db_constant.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/leftmenu.js"></script>
</head>

<body>
<div class="leftMenu">
    <ul class="sup_menu">
    	<li><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('media_zbgl');?></a>
    		<ul class="sub_menu">
            	<li><a href="javascript:void(null);" pos="contentframe/live_pages/nncms_content_live_tree_addedit.php"><?php echo cms_get_lang('contorl_zbpkgl');?></a></li>

                <li><a href="javascript:void(null);" pos="contentframe/live_pages/nncms_content_livelist.php?view_type=0&view_audit=<?php echo NNS_VOD_CHECK_WAITING;?>"><?php echo cms_get_lang('contorl_dbpkgl');?></a></li>

            </ul>
        </li>
        <li><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('media_zbgl');?></a>
        	<ul class="sub_menu">
            	<li><a href="javascript:void(null);" pos="contentframe/live_pages/nncms_content_livelist.php?view_type=0"><?php echo cms_get_lang('media_zblb');?></a></li>

                <li><a href="javascript:void(null);" pos="contentframe/live_pages/nncms_content_livelist.php?view_type=0&view_audit=<?php echo NNS_VOD_CHECK_WAITING;?>"><?php echo cms_get_lang('media_ddsh|media_zblb');?></a></li>
                <li><a href="javascript:void(null);" pos="contentframe/live_pages/nncms_content_live_search.php?view_type=0"><?php echo  cms_get_lang('media_ypcx|media_zbygl');?></a></li>
                <li><a href="javascript:void(null);" pos="contentframe/live_pages/nncms_content_live_addedit.php?action=add&view_type=0"><?php echo cms_get_lang('add|media_zb') ;?></a></li>
                <li><a href="javascript:void(null);" pos="contentframe/live_pages/nncms_content_livelist.php?view_type=0&view_delete_type=1"><?php echo cms_get_lang('delete|media_zb') ;?></a></li>
				<li><a href="javascript:void(null);" pos="contentframe/live_pages/nncms_content_livelist.php?view_type=0&view_delete=<?php echo NNS_VOD_DELETED_TRUE;?>"><?php echo cms_get_lang('delete|media_zblb');?></a></li>
            </ul>
        </li>
       	<li><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('jmdgl');?></a>
        	<ul class="sub_menu">
            	<li><a href="javascript:void(null);" pos="contentframe/playbill_pages/nncms_content_playbill_list.php?action=select"><?php echo cms_get_lang('media_playbill_jmdlb');?></a></li>
                <li><a href="javascript:void(null);" pos="contentframe/playbill_pages/nncms_content_playbill_list.php?action=import"><?php echo cms_get_lang('media_playbill_drjmd');?></a></li>
                <li><a href="javascript:void(null);" pos="contentframe/playbill_pages/nncms_content_playbill_list.php?action=export"><?php echo cms_get_lang('media_playbill_dcjmd');?></a></li>

            </ul>
        </li>

    </ul>
</div>
</body>
</html>
