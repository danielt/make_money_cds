<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//导入语言包
session_start();
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />

<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/leftmenu.js"></script>
</head>

<body>
<div class="leftMenu">
	<ul class="sup_menu">
		<li ><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('hotel|contorl_fw');?></a>
			<ul class="sub_menu">
				<li><a href="javascript:void(null);"  pos="contentframe/content_pages/nncms_content_list.php?type=1"><?php echo cms_get_lang('hotel|introduction');?></a></li>
				<li><a href="javascript:void(null);" pos="contentframe/service_pages/nncms_content_service_category_list.php?type=1000300&service_package_id=jiudian_service&type=hotel"><?php echo cms_get_lang('trip_notice');?></a></li>

			</ul>
		</li>
		<li ><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('xxgl_xxfw');?></a>
			<ul class="sub_menu">
				<li><a href="javascript:void(null);" pos="contentframe/msg_pages/nncms_content_msg_list.php"><?php echo cms_get_lang('xxgl_xx|list');?></a></li>
				<li><a href="javascript:void(null);" pos="contentframe/msg_pages/nncms_content_msg_addedit.php?action=add"><?php echo cms_get_lang('xxgl_xxtj');?></a></li>
			</ul>
		</li>

	</ul>
</div>
</body>
</html>
