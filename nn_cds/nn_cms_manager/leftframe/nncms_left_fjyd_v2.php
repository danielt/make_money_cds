<?php header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include ("../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
//导入语言包
session_start();
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
include $nncms_db_path . "nns_common/nns_db_constant.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/models/sp_model.php';

$sp_id = $_GET['sp_id'];
$data = sp_model::get_sp_list();
$lab_name = "";
foreach ($data as $value) {
	if ($value['nns_id'] == $sp_id) {
		$lab_name = $value['nns_name'];
		break;
	}
}
include $nncms_config_path . 'nn_logic/cp/cp.class.php';
$dc = nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$dc->open();
$sp_config_data = sp_model::get_sp_info($sp_id);


$str_bind_cp = $sp_config_data['nns_bind_cp'];
$str_bind_cp = ltrim($str_bind_cp,',');
$arr_bind_cp = (strlen($str_bind_cp) >0) ? explode(',', $str_bind_cp) : array();
$last_cp_val = array();
if(!empty($arr_bind_cp))
{
    $result_cp = nl_cp::query_by_more_id($dc, $arr_bind_cp);
    if(isset($result_cp['data_info']) && is_array($result_cp['data_info']) && !empty($result_cp['data_info']))
    {
        foreach ($result_cp['data_info'] as $cp_val)
        {
            $cp_val_config = !empty($cp_val['nns_config']) ? json_decode($cp_val['nns_config'],true) : null;
            if(!isset($cp_val_config['message_group_enabled']) || $cp_val_config['message_group_enabled'] != 1)
            {
                continue;
            }
            $last_cp_val[$cp_val['nns_id']] = $cp_val['nns_name'];
        }
    }
}

$sp_config_data['nns_config'] = json_decode($sp_config_data['nns_config'],true);
$config_flag = false;
if((isset($sp_config_data['nns_config']['flow_audit']) || isset($sp_config_data['nns_config']['flow_unline'])) && (!empty($sp_config_data['nns_config']['flow_unline']) || !empty($sp_config_data['nns_config']['flow_audit'])))
{
	$config_flag = true;
}
$config_unline_flag = false;
if(isset($sp_config_data['nns_config']['online_disabled']) && $sp_config_data['nns_config']['online_disabled'] =='1')
{
	$config_unline_flag = true;
}
$config_clip_flag = true;
if(isset($sp_config_data['nns_config']['disabled_clip']) && intval($sp_config_data['nns_config']['disabled_clip']) === 2)
{
	$config_clip_flag = false;
}
$config_file_encode_flag = false;
if(isset($sp_config_data['nns_config']['clip_file_encode_enable']) && intval($sp_config_data['nns_config']['clip_file_encode_enable']) ===1 
    && isset($sp_config_data['nns_config']['clip_file_encode_model']) && in_array(intval($sp_config_data['nns_config']['clip_file_encode_model']), array(1,2)))
{
    $config_file_encode_flag = true;
}
$config_pass_queue_flag = false;
if(isset($sp_config_data['nns_config']['pass_queue_disabled']) && $sp_config_data['nns_config']['pass_queue_disabled'] =='1')
{
	$config_pass_queue_flag = true;
}
$config_epg_flag = true;
if(isset($sp_config_data['nns_config']['disabled_epg']) && $sp_config_data['nns_config']['disabled_epg'] =='1')
{
	$config_epg_flag = false;
}
$config_cdn_flag = true;
if(isset($sp_config_data['nns_config']['disabled_cdn']) && $sp_config_data['nns_config']['disabled_cdn'] =='1')
{
	$config_cdn_flag = false;
}
global $import_epg_playbill_model;
global $g_bk_version_number;
$bk_version_number = $g_bk_version_number == '1' ? true : false;
unset($g_bk_version_number);
$center_import = true;
$g_enable_center_import_v2 = get_config_v2("g_enable_center_import_v2");
$g_playbill_import_cdn_v2 = get_config_v2('g_playbill_import_cdn_v2');
if($bk_version_number && !$g_enable_center_import_v2)
{
    $center_import = false;
}
?>
<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />

                <script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../js/leftmenu.js"></script>

        </head>

        <body>
                <div class="leftMenu">
                        <ul class="sup_menu">
                        	<li class="select_menu">
                        		<a href="javascript:void(null);" >&middot;注入队列管理</a>
                        		 <ul class="sub_menu">
                                     <?php if($center_import){?>
                        		 	<li>
	                             		<a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_import_queue_task.php?sp_id=<?php echo $sp_id; ?>">中心注入指令</a>
					                </li>
                                     <?php } ?>
	                             	<li>
	                             		<a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_queue_task.php?sp_id=<?php echo $sp_id; ?>">中心同步指令</a>
					                </li>
					                <?php if($config_clip_flag){?>
					                <li>
                                       <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_clip_task.php?sp_id=<?php echo $sp_id; ?>">切片指令</a>
                                    </li>
                                    <?php }?>
                                    <?php if($config_file_encode_flag){?>
					                <li>
                                       <a href="javascript:void(null);" pos="contentframe/file_encode/nncms_file_encode_list.php?nns_sp_id=<?php echo $sp_id; ?>">转码指令</a>
                                    </li>
                                    <?php }?>
                                    <li>
                                       <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_task.php?sp_id=<?php echo $sp_id; ?>">CDN/EPG点播注入</a>
                                    </li>
                                    <li>
                                       <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_file_task.php?sp_id=<?php echo $sp_id; ?>">CDN/EPG模板文件注入</a>
                                    </li>



                                     <?php  if($g_playbill_import_cdn_v2 == '1'){
                                         //这里作为黑龙江单独的直播注入页面进行操作
                                         ?>
                                         <li>
                                         <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_live_v2.php?sp_id=<?php echo $sp_id; ?>">CDN/EPG直播注入</a>
                                         </li>
                                     <?php  }else{ ?>
                                         <li>
                                             <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_live.php?sp_id=<?php echo $sp_id; ?>">CDN/EPG直播注入</a>
                                         </li>
                                         <?php if($import_epg_playbill_model ==1){?>
                                             <li>
                                                 <a href="javascript:void(null);" pos="contentframe/playbill_pages/nncms_content_playbill.php?sp_id=<?php echo $sp_id; ?>">CDN/EPG节目单注入</a>
                                             </li>
                                         <?php }?>
                                     <?php } ?>






					                <li>
					                   <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_category_list.php?sp_id=<?php echo $sp_id; ?>">CDN栏目绑定</a>
					                </li>
					                <!--
					                	<li>
                                                	<a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_bk.php?sp_id=<?php echo $sp_id;?>">
                                                		内容管理
                                                	</a>
                                            </li> 
					                	-->                                    
					             </ul>
                        	</li>
                    
                        	
                        	<?php if(!empty($last_cp_val)){?>
                        	  <li class="select_menu">
                        		<a href="javascript:void(null);" >&middot;分组反馈队列</a>
                        		<ul class="sub_menu">
                        		    <?php foreach ($last_cp_val as $cp_key=>$cp_val){?>
                        			<li>
	                             		<a href="javascript:void(null);" pos="contentframe/message_pages/nncms_message_group_list.php?cp_id=<?php echo $cp_key;?>&sp_id=<?php echo $sp_id; ?>"><?php echo $cp_key.'/'.$cp_val;?></a>
					                </li>
					                <?php }?>			                        
					            </ul>
                        	</li>
                        	<?php }?>
                        	<?php if($config_flag){?>
                        	<li class="select_menu">
                        		<a href="javascript:void(null);" >&middot;审核队列</a>
                        		<ul class="sub_menu">
                        			<li>
	                             		<a href="javascript:void(null);" pos="contentframe/vod_audit/nncms_vod_audit.php?sp_id=<?php echo $sp_id; ?>">中心流程指向</a>
					                </li>
					                <li>
	                             		<a href="javascript:void(null);" pos="contentframe/vod_audit/nncms_vod_audit_word_list.php?sp_id=<?php echo $sp_id; ?>">中心流程敏感词库</a>
					                </li>
					                <li>
	                             		<a href="javascript:void(null);" pos="contentframe/vod_audit/nncms_vod_audit_log_list.php?sp_id=<?php echo $sp_id; ?>">中心流程操作日志</a>
					                </li>				                        
					            </ul>
                        	</li>
                        	<?php }?>
                        	<?php if($config_unline_flag){?>
                        	<li class="select_menu">
                        		<a href="javascript:void(null);" >&middot;上下线队列管理</a>
                        		<ul class="sub_menu">
					                <li>
										<a href="javascript:void(null);" pos="contentframe/asset_online/nncms_content_asset_online_list.php?sp_id=<?php echo $sp_id; ?>">上下线消息队列</a>
									</li>
					                <li>
	                             		<a href="javascript:void(null);" pos="contentframe/asset_online/nncms_content_asset_online_log.php?sp_id=<?php echo $sp_id; ?>">上下线队列日志</a>
					                </li>			                        
					            </ul>
                        	</li>
                        	<?php }?>
                        	<?php if($config_pass_queue_flag){?>
                        	<li class="select_menu">
                        		<a href="javascript:void(null);" >&middot;透传队列管理</a>
                        		<ul class="sub_menu">
					                <li>
										<a href="javascript:void(null);" pos="contentframe/pass_queue/nncms_content_pass_queue_list.php?sp_id=<?php echo $sp_id; ?>">透传队列</a>
									</li>			                        
					            </ul>
                        	</li>
                        	<?php }?>
                            <?php if(!$bk_version_number){ ?>
                        	<li class="select_menu">
                        		<a href="javascript:void(null);" >&middot;业务配置</a>
                        		<ul class="sub_menu">
                        			<li>
					                    <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_op_control.php?sp_id=<?php echo $sp_id; ?>">权重设置</a>
					                </li>
	                             	<li>
					                    <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_blacklist.php?sp_id=<?php echo $sp_id; ?>">黑/白名单</a>
					                </li>					                        
					            </ul>
                        	</li>
                            <?php }?>
                        	<li class="select_menu">
                        		<a href="javascript:void(null);" >&middot;日志管理</a>
                        		<ul class="sub_menu">
                                    <?php if(!$bk_version_number){ ?>
                        			<li>
					                   <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_mgtv_import_log.php?sp_id=<?php echo $sp_id; ?>">CP注入日志</a>
					                </li>
					                <?php }if($config_epg_flag){?>
                        			<li>
					                   <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_mgtv_epg_log.php?sp_id=<?php echo $sp_id; ?>">EPG注入日志</a>
					                </li> 
					                <?php }?>
					                <?php if($config_cdn_flag){?>
                        			<li>
                                       <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_c2_log_list.php?sp_id=<?php echo $sp_id; ?>">CDN注入日志</a>
                                    </li>
                                    <!--<li>-->
                                    <!--   <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_file_log_list.php?sp_id=--><?php //echo $sp_id; ?><!--">CDN模板文件注入日志</a>-->
                                    <!--</li>-->
                                    <?php }if(!$bk_version_number){ ?>
	                             	<li>
					                   <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_op_log.php?sp_id=<?php echo $sp_id; ?>">中心同步指令日志</a>
					                </li>
                                    <?php }?>
					            </ul>
                        	</li>
                        	<li class="select_menu">
                        		<a href="javascript:void(null);" >&middot;数据规整</a>
                        		<ul class="sub_menu">
                        			<li>
					                   <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_add_task.php?sp_id=<?php echo $sp_id; ?>">存量注入</a>
					                </li>
					                <li>
					                   <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_clip_amend.php?sp_id=<?php echo $sp_id; ?>">切片指令修正</a>
					                </li>
					                <li>
					                    <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_vod.php?sp_id=<?php echo $sp_id; ?>">点播缺失数据修正</a>
					                </li>	
					                <li>
					                   <a href="javascript:void(null);" pos="../statistics/html/st_task_weight.php?sp_id=<?php echo $sp_id; ?>">缺失数据修正</a>
					                </li>
					                 <li>
					                   <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_db_quantity.php?sp_id=<?php echo $sp_id; ?>">资源库数据存量</a>
					                </li>
                                    <li>
                                        <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_vod_select.php?sp_id=<?php echo $sp_id; ?>">媒资筛选</a>
                                    </li>
					                <li>
					                   <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_vod_batch_delete.php?sp_id=<?php echo $sp_id; ?>">批量删除媒资</a>
					                </li>
					                <li>
					                   <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_vod_batch_delete.php?action=delete_old&sp_id=<?php echo $sp_id; ?>">批量删除重复片源</a>
					                </li>
                                    <?php
                                        if(isset($sp_config_data['nns_config']['cdn_media_playurl_by_csv']) && $sp_config_data['nns_config']['cdn_media_playurl_by_csv'] == 1)
                                        {
                                            echo '<li>' .
                                                    '<a href="javascript:void(null);" pos="../statistics/html/st_task_import_media_playinfo.php?action=import_media_playurl&sp_id=' . $sp_id . '">导入片源播放串数据</a>' .
                                                 '</li>';
                                    }
                                    ?>
					            </ul>
                        	</li>
                        	<li class="select_menu">
                        		<a href="javascript:void(null);" >&middot;统计图表</a>
                        		<ul class="sub_menu">
                        			<li>
					                     <a href="javascript:void(null);" pos="../statistics/html/st_task_stat.php?sp_id=<?php echo $sp_id; ?>">中心同步指令统计</a>
					                </li>
					                <li>
					                     <a href="javascript:void(null);" pos="../statistics/html/st_task_import_state.php?sp_id=<?php echo $sp_id; ?>">CDN/EPG点播注入统计</a>
					                </li>
					                <li>
					                     <a href="javascript:void(null);" pos="../statistics/html/st_task_import_statistics.php?sp_id=<?php echo $sp_id; ?>">CDN/EPG注入统计图表</a>
					                </li>  
                        		</ul>                        		
                        	</li>
                                <!--<li class="select_menu">
                                        <a href="javascript:void(null);" >&middot; <?php echo $lab_name; ?></a>
                                        <ul class="sub_menu">
                                        
                                        	<li>
					                            <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_queue_task.php?sp_id=<?php echo $sp_id; ?>">操作列队</a>
					                        </li>
                                        
                                        
                                        	<?php 
                                            $pri_bool6=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"135008");
											if($pri_bool6){
                                            ?>
					                        <!--<li>
					                            <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_mgtv_import_count.php?sp_id=<?php echo $sp_id;?>">数据统计</a>
					                        </li>-- 
					                        <?php } ?>
                                            <?php 
                                            $pri_bool1=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"135001");											
											if($pri_bool1){
                                            ?>                                            
                                            <li>
                                                	<a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_bk.php?sp_id=<?php echo $sp_id; ?>">
                                                		内容管理
                                                	</a>
                                            </li>                                             
                                            <?php } ?>
                                            <?php 
                                            $pri_bool2=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"135002");
											if($pri_bool2){
                                            ?>
                                            <li>
                                                	<a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_task.php?sp_id=<?php echo $sp_id; ?>">
                                                		内容注入任务列表
                                                	</a>
                                              </li>
                                               <?php } ?>
                                            <?php 
                                            $pri_bool3=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"135003");
											if($pri_bool3){
                                            ?>
                                            <li>
                                                	<a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_clip_task.php?sp_id=<?php echo $sp_id; ?>">
                                                		切片任务列表
                                                	</a>
                                              </li> 
                                            <?php } ?>
                                            
                                            <li>
					                            <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_clip_amend.php?sp_id=<?php echo $sp_id; ?>">切片数据修正</a>
					                        </li>
                                            
                                            
                                            <?php 
                                            $pri_bool4=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"135004");
											if($pri_bool4){
                                            ?>
                                            <li>
                                                	<a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_c2_log_list.php?sp_id=<?php echo $sp_id; ?>">
                                                		注入CDN命令列表
                                                	</a>
                                              </li>
                                              <?php } ?>
                                            <?php 
                                            $pri_bool5=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"135005");
											if($pri_bool5){
                                            ?>                                                                                               
					                        <li>
					                            <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_category_list.php?sp_id=<?php echo $sp_id; ?>">栏目管理</a>
					                        </li>
					                        <?php } ?>
					                        
					                        <?php 
                                            $pri_bool6=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"135007");
											if($pri_bool6){
                                            ?>                                                                                               
					                        <li>
					                            <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_mgtv_import_log.php?sp_id=<?php echo $sp_id; ?>">MGTV注入日志</a>
					                        </li>
					                        <?php } ?>
					                        
					                        <?php 
                                            $pri_bool6=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"135010");
											if($pri_bool6){
                                            ?>                                                                                               
					                        <li>
					                            <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_mgtv_epg_log.php?sp_id=<?php echo $sp_id; ?>">EPG注入日志</a>
					                        </li>
					                        <?php } ?> 
					                        
					                        
					                        <li>
					                            <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_playbill.php?sp_id=<?php echo $sp_id; ?>">节目单注入</a>
					                        </li>
					                        
					                        
					                        <li>
					                            <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_live.php?sp_id=<?php echo $sp_id; ?>">直播注入</a>
					                        </li>
					                        
					                        
                                            
                                            <!--  <li>
					                            <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_op.php?sp_id=<?php echo $sp_id;?>">权重设置</a>
					                        </li>--
                                            
					                        <li>
					                            <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_op_log.php?sp_id=<?php echo $sp_id; ?>">操作队列日志</a>
					                        </li>    
											
											<li>
					                            <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_add_task.php?sp_id=<?php echo $sp_id; ?>">初始化数据</a>
					                        </li>
					                        <li>
					                            <a href="javascript:void(null);" pos="contentframe/sp_content_pages_v2/nncms_content_vod.php?sp_id=<?php echo $sp_id; ?>">点播注入</a>
					                        </li>
					                        
					                        
					                        
					                        
					                        
					                        
					                        
					                        
					                        

                                        </ul>
                             </li>-->
                                <!--
                                <li>
                                    <a href="javascript:void(null);" >&middot; 数据统计</a>
                                    <ul class="sub_menu">   
                                    	<li>
					                     <a href="javascript:void(null);" pos="../statistics/html/st_task_import_state.php?sp_id=<?php echo $sp_id; ?>">中心同步指令统计</a>
					                   </li>
					                   <li>
					                     <a href="javascript:void(null);" pos="../statistics/html/st_task_stat.php?sp_id=<?php echo $sp_id; ?>">CDN/EPG点播注入统计</a>
					                   </li>                                 	
                                       <li>
					                     <a href="javascript:void(null);" pos="../statistics/html/st_task_ok.php?sp_id=<?php echo $sp_id; ?>">数据统计</a>
					                   </li>
					                   <li>
					                     <a href="javascript:void(null);" pos="../statistics/html/st_task_weight.php?sp_id=<?php echo $sp_id; ?>">缺失数据规整</a>
					                   </li>

					                   </ul>
					            </li>-->
                        </ul>
                </div>

        </body>
</html>
