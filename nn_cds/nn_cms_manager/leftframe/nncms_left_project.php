<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../nncms_manager_inc.php");
 //导入语言包
 session_start();
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

 //加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

include $nncms_config_path . 'nn_logic/cp/cp.class.php';
include $nncms_config_path . 'nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$dc->open();
$cp_list = nl_cp::query_all($dc);
$cp_list_data_info = $cp_list['data_info'];
$cp_transcode_file_enabled = false;
if(is_array($cp_list_data_info))
{
    foreach($cp_list_data_info as $key=>$cp_list)
    {
        $arr_temp_config = isset($cp_list['nns_config'])?json_decode($cp_list['nns_config'],true):null;
        if(isset($arr_temp_config['cp_transcode_file_enabled']) && $arr_temp_config['cp_transcode_file_enabled']==1)
        {
            $cp_transcode_file_enabled = true;
            break;
        }
    }
}
global $g_arr_queue_pool;
$arr_queue_pool = (isset($g_arr_queue_pool) && is_array($g_arr_queue_pool) && !empty($g_arr_queue_pool)) ? $g_arr_queue_pool : null;
unset($g_arr_queue_pool);
?>
<!DOCTYPE html PUBLIC "-//W3C//liD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/liD/xhtml1-transitional.lid">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/leftmenu.js"></script>
</head>

<body>
<div class="leftMenu">
    <ul class="sup_menu">

        <li><a href="javascript:void(null);">&middot; 通用配置</a>
        	<ul class="sub_menu">
            	<li><a href="javascript:void(null);" pos="contentframe/project_group/nncms_project_model_list.php">通用模块管理</a></li>
            	<li><a href="javascript:void(null);" pos="contentframe/project_group/nncms_project_group_list.php">通用配置分组</a></li>
            </ul>
        </li>
        <?php if($cp_transcode_file_enabled){?>
        <li><a href="javascript:void(null);">&middot; 转码配置</a>
        	<ul class="sub_menu">
            	<li><a href="javascript:void(null);" pos="contentframe/transcode/nncms_transcode_manufacturer_list.php">转码厂家管理</a></li>
            	<li><a href="javascript:void(null);" pos="contentframe/transcode/nncms_transcode_manufacturer_model_list.php">转码厂家模板管理</a></li>
            	<li><a href="javascript:void(null);" pos="contentframe/transcode/nncms_transcode_model_list.php">转码配置管理</a></li>
            	<li><a href="javascript:void(null);" pos="contentframe/transcode/nncms_transcode_load_balance_list.php">负载均衡配置</a></li>
            </ul>
        </li>
        <?php }?>
        <?php if(!empty($arr_queue_pool)){ ?>
        <li><a href="javascript:void(null);">&middot; 队列配置</a>
        	<ul class="sub_menu">
            	<li><a href="javascript:void(null);" pos="contentframe/queue/nncms_queue_list.php">队列模板管理</a></li>
            </ul>
        </li>    
        <?php }?>
        <li><a href="javascript:void(null);">&middot; 切片服务器配置</a>
            <ul class="sub_menu">
                <li><a href="javascript:void(null);" pos="contentframe/clip/nncms_clip_servicer_list.php">切片服务器管理</a></li>
            </ul>
        </li>
    </ul>
</div>
</body>
</html>
