<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
//导入语言包
session_start();
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../js/leftmenu.js"></script>
        </head>

        <body>
                <div class="leftMenu">
                        <ul class="sup_menu">

                                <li><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('zdgl_zdlxgl'); ?></a>
                                        <ul class="sub_menu">
                                                <li><a href="javascript:void(null);" pos="contentframe/devices_pages/nncms_content_deviceslist.php"><?php echo cms_get_lang('zdgl_zdlxlb'); ?></a></li>
                                                <li><a href="javascript:void(null);" pos="contentframe/devices_pages/nncms_content_devices_addedit.php?action=add"><?php echo cms_get_lang('add|zdgl_zdlx'); ?></a></li>
                                                <li><a href="javascript:void(null);" pos="contentframe/devices_pages/nncms_content_deviceslist.php"><?php echo cms_get_lang('delete|zdgl_zdlx'); ?></a></li>
                                        </ul>
                                </li>
                                <li><a href="javascript:void(null);">&middot; 终端升级管理</a>
                                        <ul class="sub_menu">

                                                <li><a href="javascript:void(null);" pos="contentframe/version/nns_cms_version_list.php">版本列表</a></li>
                                                <li><a href="javascript:void(null);" pos="contentframe/version/nns_cms_version_add.php">添加版本</a></li>
                                        </ul>
                                </li>
                                <li><a href="javascript:void(null);">&middot; 终端定向升级管理</a>
                                        <ul class="sub_menu">

                                                <li><a href="javascript:void(null);" pos="contentframe/version_mac/nns_cms_version_list.php">版本列表</a></li>
                                                <li><a href="javascript:void(null);" pos="contentframe/version_mac/nns_cms_version_add.php">添加版本</a></li>
                                        </ul>
                                </li>
                                <?php if (!g_cms_config::get_g_config_value('g_user_device_disabled')) { ?>
                                        <li><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('zdgl_sbgl'); ?></a>
                                                <ul class="sub_menu">
                                                        <li><a href="javascript:void(null);" pos="contentframe/devices_pages/nncms_content_devices_detail_list.php"><?php echo cms_get_lang('zdgl_sblb'); ?></a></li>
                                                        <?php if ($g_boss_mode == 0) { ?>
                                                                <li><a href="javascript:void(null);" pos="contentframe/devices_pages/nncms_content_devices_detail_addedit.php?action=add"><?php echo cms_get_lang('add|zdgl_sb'); ?></a></li>
                                                                <li><a href="javascript:void(null);" pos="contentframe/devices_pages/nncms_content_devices_detail_list.php"><?php echo cms_get_lang('delete|zdgl_sb'); ?></a></li>
                                                        <?php } ?>
                                                        <li><a href="javascript:void(null);" pos="contentframe/devices_pages/nncms_content_devices_detail_search.php"><?php echo cms_get_lang('search|zdgl_sb'); ?></a></li>
                                                        <?php if ($g_boss_mode == 0) { ?>
                                                                <li><a href="javascript:void(null);" pos="contentframe/devices_pages/nncms_content_devices_csv_import.php"><?php echo cms_get_lang('zdgl_sbxx|pldr'); ?></a></li>
                                                        <?php } ?>           
                                                </ul>
                                        </li>
                                        <li><a href="javascript:void(null);">&middot; <?php echo cms_get_lang('zdgl_yhgl'); ?></a>
                                                <ul class="sub_menu">

                                                        <li><a href="javascript:void(null);" pos="contentframe/devices_pages/nncms_content_user_list.php"><?php echo cms_get_lang('zdgl_yhlb'); ?></a></li>
                                                        <?php if ($g_boss_mode == 0) { ?>
                                                                <li><a href="javascript:void(null);" pos="contentframe/devices_pages/nncms_content_user_addedit.php?action=add"><?php echo cms_get_lang('add|zdgl_yh'); ?></a></li>
                                                                <!--<li><a href="javascript:void(null);" pos="contentframe/devices_pages/nncms_content_user_list.php?"><?php echo cms_get_lang('delete|zdgl_yh'); ?></a></li>-->
                                                        <?php } ?>
                                                        <li><a href="javascript:void(null);" pos="contentframe/devices_pages/nncms_content_user_search.php"><?php echo cms_get_lang('search|zdgl_yh'); ?></a></li>
                                                        <!--<li><a href="javascript:void(null);" pos="contentframe/devices_pages/nncms_content_user_search.php?action=bind"><?php echo cms_get_lang('cpgl_bdcp'); ?></a></li>-->
                                                        <?php if ($g_user_category_enabled == 1) { ?>
                                                                <li><a href="javascript:void(null);" pos="contentframe/devices_pages/nncms_content_user_catalog_addedit.php"><?php echo cms_get_lang('zdgl_yhml'); ?></a></li>
                                                                <li><a href="javascript:void(null);" pos="contentframe/devices_pages/nncms_content_user_category_addedit.php"><?php echo cms_get_lang('zdgl_yhqz'); ?></a></li>

                                                                <li><a href="javascript:void(null);" pos="controls/user_select/nncms_controls_user_category_select.php?category_id=10001"><?php echo cms_get_lang('zdgl_csfz_tree'); ?></a></li>
                                                                <li><a href="javascript:void(null);" pos="contentframe/devices_pages/nncms_content_user_category_addedit2.php?category_id=10001"><?php echo cms_get_lang('zdgl_csfz'); ?></a></li>

                                                        <?php } ?>
                                                </ul>
                                        </li>
                                <?php } ?>

                        </ul>
                </div>
        </body>
</html>
