<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
 //导入语言包
 session_start();
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/leftframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/leftmenu.js"></script>
</head>

<body>
<div class="leftMenu">
	<ul class="sup_menu">
		<li ><a href="javascript:void(null);"   pos="contentframe/role_pages/nncms_content_rolelist.php">&middot; <?php echo cms_get_lang('xtgl_jsgl');?></a>
			<!--<ul class="sub_menu">
				<li><a href="javascript:void(null);" pos="contentframe/nncms_content_rolelist.php">角色管理</a></li>
				<li><a href="javascript:void(null);" pos="contentframe/nncms_content_userlist.php">管理员管理</a></li>
			</ul>-->
		</li>


		<li><a href="javascript:void(null);" pos="contentframe/manager_pages/nncms_content_managerlist.php">&middot; <?php echo cms_get_lang('xtgl_glygl');?></a></li>

	</ul>
</div>
</body>
</html>
