<?php

/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 ob_start();
session_start();
include ("nncms_manager_inc.php");
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
//  include ($nncms_db_path. "nns_common/nn_cms_config.php");
$_SESSION["nns_mgr_id"] = "";
$username = $_POST["username"];
$userpass = $_POST["userpass"];
$logincode = $_POST["logincode"];
$language_dir_post = $_POST["nns_language_id"];

//导入语言包
if (isset ($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
$checkpri = null;
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

if ($username && $userpass) {
	// 	echo $_SESSION["login_code"]. ":". $logincode;
	if (strtoupper($logincode) == strtoupper($_SESSION["login_code"])) {
		include "nncms_manager_inc.php";
		include $nncms_db_path . "nns_manager" . DIRECTORY_SEPARATOR . "nns_db_manager_class.php";
		$manager_inst = new nns_db_manager_class();
		$login_arr = $manager_inst->nns_db_manager_login($username, $userpass);
		if ($login_arr["ret"] != 0) {
			echo "<script>alert('" . $login_arr["reason"] . "');window.location.href='index.php';</script>";
		} else
			if ($login_arr["ret"] == 0) {
				//	 	 session_start();
				if ($login_arr["data"][0]["nns_type"]==2){
					$_SESSION["nns_mgr_name"] = $login_arr["data"][0]["nns_name"];
					$_SESSION["nns_last_login_time"] = $login_arr["data"][0]["nns_login_time"];
					$_SESSION["nns_role_pris"] = $login_arr["data"][0]["nns_pri_id"];
					$_SESSION["nns_mgr_id"] = $login_arr["data"][0]["nns_id"];
					$_SESSION["nns_org_id"] = $login_arr["data"][0]["nns_org_id"];
					$_SESSION["nns_role_id"] = $login_arr["data"][0]["nns_role_id"];
					$_SESSION["nns_manager_type"] = $login_arr["data"][0]["nns_type"];
					$_SESSION["nns_role_name"] = $login_arr["data"][0]["role_name"];
					$_SESSION["language_dir"] = $language_dir_post;
				}
				Header("Location: nncms_manager.php");
				ob_end_flush();
			}
	} 

function getDirFiles($dir) {
	if ($handle = opendir($dir)) {
		/* Because the return type could be false or other equivalent type(like 0),
		 this is the correct way to loop over the directory. */
		while (false !== ($file = readdir($handle))) {
			$files[] = $file;
		}
	}
	closedir($handle);
	if ($files)
		return $files;
	else
		return false;
}
$languages = getDirFiles("languages");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/loginstyle.css" rel="stylesheet" type="text/css" />
<link href="css/allstyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="js/jquery-1.4.3.min.js"></script>
<script>
	$(document).ready(function(){
		$('.login_code img').click(function(){
			window.location.href='index.php';
		});
		
	});
</script>
</head>

<body class="loginbody">
	<div class="login_control">
        <div class="loginlogo"></div>
        <div class="loginbox">
            <div class="login_title"><?php echo $language_manager_table_manager_login;?></div>
             <form action="index.php?XDEBUG_SESSION_START=123" method="post" id="login_form">
            <div class="login_content">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><?php echo $language_manager_yhm?>:</td>
                    <td>
                    <input type="text" name="username" id="username" style="width:150px;"/></td>
                  </tr>
                  <tr>
                    <td><?php echo $language_manager_table_pass?>:</td>
                    <td><input type="password" name="userpass" id="userpass" style="width:150px;"/></td>
                  </tr>
                  <tr>
                    <td><?php echo $language_manager_global_yyb?>:</td>
                    <td><select style="width:90px;"  name="nns_language_id" id="nns_language_id" rule="noempty">
                    <?php

foreach ($languages as $language) {
	if ($language != "." && $language != "..") {
?>
                		 <option value="<?php echo $language;?>" >&nbsp;<?php $language_arr=explode(".",$language); echo $language_arr[0];?></option>
                	<?php }}?>
                    </select></td>
                  </tr>
                  <tr>
                    <td><?php echo $language_manager_global_yzm?>:</td>
                    <td  valign="top"><input type="text" name="logincode" id="logincode" style="width:80px;" /><div class="login_code"><img src="controls/nncms_controls_code_image.php"/></div> </td>
                  </tr>
                </table>
    
            </div>
            </form>
            <div style="background:url(images/login-03.png) 0px 0px repeat-x; height:2px;"/>
        <div class="login_controlbtns">
                <div class="loginbtn"><a href="javascript:document.getElementById('login_form').submit();"><img src="images/login-04.png" width="81" height="34" border="0"/></a></div>
                <!--<div class="forget"><a href="#">忘记密码?</a></div>-->
                <div style="clear:both;"/>
            </div>
        </div>
         <div class="copyinfo">copyright &copy; 2015 北京视达科科技有限公司 版权所有</div>
    </div>
   
</body>
</html>
<?php }?>