<?php
/**
 * 湖南芒果tv的boss对接api
 */
class mgtv_boss{
	//连接等待时间,0不等待
	public $connect_time_out=10;
	//设置curl允许执行的最长秒数
	public $time_out=10;
	//url基础地址
	public $url="";
	//url端口
	public $port="";
	//url页面地址
	public $url_page_url=array("auth"=>"auth.aspx","playauth"=>"playauth.aspx","liveadd"=>"liveadd.aspx");
	//CURL句柄
	private $ch = null;
	public function __construct($url="http://58.83.191.25/OTTBossApi/",$port="80"){
		$this->port=$port;
		$this->url=$url;
		//初始化
		$this->ch= curl_init();
	}
	/**
	 * 认证接口
	 * @param string $mode认证模式，ott_tv 互联网电视
	 * @param array $params，参数组array("tv_id"=>电视ID[设备ID],"net_id"=>入网ID,"mac_id"=>MAC地址,"token"=>token密匙)
	 */
	public function auth($mode="",$params=array()){
		$params["mode"]=$mode;
		$url=rtrim($this->url,'/').'/'.$this->url_page_url["auth"];
		return $this->post($url, $params);
	}
	/**
	 * 获取影片定价信息
	 * @param string $token
	 * @param array $params参数组array("user_id"=>用户id,"tv_id"=>电视di,"content_id"=>内容id,"content_type"=>0点播1直播)
	 */
	public function play_auth($token="",$params=array()){
		$params["token"]=$token;
		$url=rtrim($this->url,'/').'/'.$this->url_page_url["playauth"];
//		var_dump($params);
		return $this->post($url, $params);
	}
	/**
	 * 直播添加修改成功通知计费平台
	 * @param string $live_id直播id
	 * @param string $live_name直播名称
	 * @param string $live_type直播类型TSTV/LIVE
	 */
	public function live_add($live_id,$live_name,$live_type){
		$params["live_id"]=$live_id;
		$params["live_name"]=$live_name;
		$params["live_type"]=$live_type;
		$url=rtrim($this->url,'/').'/'.$this->url_page_url["liveadd"];
//		var_dump($params);
		return $this->post($url, $params);
	}
	/**
	 * 直播删除成功通知计费平台
	 * @param string $live_id:直播id
	 */
	public function live_del($live_id){
		$params["live_id"]=$live_id;
		$url=rtrim($this->url,'/').'/'.$this->url_page_url["liveadd"];
//		var_dump($params);
		return $this->post($url, $params);
	}
	private function post($url,$params){
		//设置CURL连接的端口
		curl_setopt($this->ch, CURLOPT_PORT, $port);
		//设置连接等待时间,0不等待
		curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, $this->connect_time_out);
		//设置curl允许执行的最长秒数
		curl_setopt($this->ch, CURLOPT_TIMEOUT, $this->time_out);
		//发送一个常规的POST请求，类型为：application/x-www-form-urlencoded
		curl_setopt($this->ch, CURLOPT_POST, true) ;
		//设置POST字段值
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, $params);
		//设置请求的URL
		curl_setopt($this->ch, CURLOPT_URL, $url);
		//开始执行请求
		$result = curl_exec($this->ch);
		return $result;
	}
	/**
	 * 得到错误信息
	 * @return string
	 */
	public function error()
	{
		return curl_error($this->ch);
	}

	/**
	 * 得到错误代码
	 * @return int
	 */
	public function errno()
	{
		return curl_errno($this->ch);
	}
	/**
	 * 析构方法
	 */
	public function __destruct(){
		//关闭CURL
		curl_close($this->ch);
	}
}