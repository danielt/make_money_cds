<?php 
/**
 * 
 * @access xml parse
 * @author chenbo@starcorcn.com
 * @deprecated version - 2012-9-18
 * 
 */
class xml_rpc_parse{
	private $doc = null;
	public function __construct(){
		
	}
	/** 
   * xml_encode 
   * 
   * 数组到xml的转化函数(构造函数), 
   *  
   * @param array $array 要转化的数组 
   * @param array $type 区分request response 
   * @param int $deep 当前转化的层次，递归时用 
   * @param String $root 当前的父层xml 
   * @param String $item 键值为数字的数组元素对应的XML名称。例如,如果一个数组的下标是数字， 
   * 假设数组的每个元素存储的是文章的信息，那么我们定义每篇文章在XML中显示的元素标签为param， 
   * 即设置$item='param'，就可以生成了类似如下结构的XML： 
   *<boss_api>
   *  <request>
   *    <param name="request">
   *      <param name="version" value="1.0"/>
   *      <param name="method" value="maintainUserInfo"/>
   *      <param name="params">
   *        <param name="userId" value="user06"/>
   *        <param name="userAlias" value="user06"/>
   *      </param>
   *      <param name="params">
   *        <item>
   *        	<param name="userId" value="user06"/>
   *        	<param name="userAlias" value="user06"/>
   *        </item>
   *      </param>
   *      <param name="auto_key" value="4dthoP4Kq1NRhd1r"/>
   *
   *      <param name="type" value="1"/>
   *    </param>
   *  </request>
   *</boss_api>
   * @return null 
   **/
public function xml_encode($array = array(),$type='response',$deep = 0,$parentNode = 'boss_api',$item = 'param') {
		
		if(!is_array($array) && count($array) == 0) return false ;
		
		if($deep==0) {  
          	$this->doc = new DOMDocument("1.0",'utf-8');
          	$this->doc->formatOutput = true;  
         	$root = $this->doc->createElement( $parentNode );  
         	$this->doc->appendChild( $root );
         	
         	$type = $this->doc->createElement( $type );
         	$root->appendChild($type);
         	
		}
		
		$deep++;
		if(is_array($array)){
	    	foreach($array as $key => $value) {  

	         	if(preg_match ('/^[0-9]+$/',$key)){

	         		$child      = $this->doc->createElement('item');
	         	}else{
	         		$child      = $this->doc->createElement($item);
	         		$attr_name  = $this->doc->createAttribute('name');
	     			$child->appendChild($attr_name);
	     			$attr_key 	= $this->doc->createTextNode($key);
	     			$attr_name->appendChild($attr_key);
	         	}
	     		
	     		if(is_array($value)){
     				
	     			if($key === 'params'){

	     				$val = isset($value[0])?$value[0]:array();

	     				$this->xml_encode($val,$type,$deep,$child,$item);
	     			}else{

						$this->xml_encode($value,$type,$deep,$child,$item);
	     				
	     			}
			 		
	     		}else{
	     			$attr_value = $this->doc->createAttribute('value');
			 		$child->appendChild($attr_value);
			 		$attr_val = $this->doc->createTextNode($value);
			 		$attr_value->appendChild($attr_val);
	     		}
	     		if($parentNode == 'boss_api'){
	     			$type->appendChild($child);      //添加到父层节点
	     		}else{
	     			$parentNode->appendChild($child);
	     		}
	         	  
		 	}
		}
	 	 
	 	return $this->doc->saveXML();
 	}
 	
 /** 
   * xml_decode 
   * 
   * xml到array的转换
   *  
   * @param string $xml 要转换xml字符串 
   * @return array 
   **/
public function xml_decode($xml_str,$type){
		$data = null;
		$xml =simplexml_load_string($xml_str);
		
		$xml_obj = $this->object_array($xml->{$type});
		$array = $this->xml_array($xml_obj['param']);
		return $array;
	}
 /** 
   * xml_array 
   * 
   * xml对象到array的指定格式转换 
   *  
   * @param object $xml 要转换xml对象
   * @param string $key 指定数据的key 
   * @param string  
   * @return array 
   **/
public function xml_array($xml,$key=null,$index=false){
	$data = null;
	foreach($xml as $k=>$val){
		
		if($index){
			if(isset($val['param'])){
				$data[$k] = $this->xml_array($val['param']);
			}else{
				$data[0] = $this->xml_array($val);
			}
			
		}

		if(isset($val['@attributes']['value'])){

			$data[$val['@attributes']['name']] = $val['@attributes']['value'];
			
		}else{

			if(isset($val['item'])){
				$data[$val['@attributes']['name']] = $this->xml_array($val['item'],$val['@attributes']['name'],true);
			}
			if(isset($val['param'])){
				if(isset($val['@attributes']['name'])){
					$data[$val['@attributes']['name']] = $this->xml_array($val['param']);
				}else{
					$data[$k] = $this->xml_array($val['param']);
				}
				
			}
			
		}
		
	}
	return $data;
	
}
   /**
	 +----------------------------------------------------------
	 * 将xml对象转换为数组
	 +----------------------------------------------------------
	 * @access private 
	 +----------------------------------------------------------
	 * @param object $object 
	 +----------------------------------------------------------
	 * @return array
	 +----------------------------------------------------------
	 */
	function object_array($object)
	{
	   if(is_object($object))
	   {
	    $object = (array)$object;
	   }
	   if(is_array($object))
	   {
	    foreach($object as $key=>$value)
	    {
	     $object[$key] = $this->object_array($value);
	    }
	   }
	   return $object;
	}
}
?>