<?php
/**
 * 
 * @access xml client
 * @author chenbo@starcorcn.com
 * @deprecated version - 2012-9-17 client 端封装实现用户接口对接
 * 
 */
include_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'interface_conf.php');
include_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'xmlrpc/xml_rpc_client.php');
class test_user{
	private $xml_client = null;
	
	function __construct(){
		$this->xml_client = new xml_rpc_client(CMS_USER_ACC_URl);
	}
	/**
	 +----------------------------------------------------------
	 * 实现维护用户信息，新增，修改接口 
	 +----------------------------------------------------------
	 * @access public 
	 +----------------------------------------------------------
	 * @param array $user_info
	 +----------------------------------------------------------
	 * @return array
	 +----------------------------------------------------------
	 */
	public function maintainUserInfo($user_info){
		
		$result = $this->xml_client->maintainUserInfo($user_info);
		$result = $this->object_array($result);
		return $result;
	}
	/**
	 +----------------------------------------------------------
	 * 实现删除用户信息接口
	 +----------------------------------------------------------
	 * @access public 
	 +----------------------------------------------------------
	 * @param array $user_info
	 +----------------------------------------------------------
	 * @return array
	 +----------------------------------------------------------
	 */
	public function deleteUser($user_info){
		
		$result = $this->json_client->deleteUser($user_info);
		$result = $this->object_array($result);
		return $result;
	}
	/**
	 +----------------------------------------------------------
	 * 实现用户绑定集团接口
	 +----------------------------------------------------------
	 * @access public 
	 +----------------------------------------------------------
	 * @param array $user_info
	 +----------------------------------------------------------
	 * @return array
	 +----------------------------------------------------------
	 */
	public function maintainUserGroup($user_info){
		$result = $this->json_client->maintainUserGroup($user_info);
		$result = $this->object_array($result);
		return $result;
	}
	/**
	 +----------------------------------------------------------
	 * 实现取消用户绑定的集团接口
	 +----------------------------------------------------------
	 * @access public 
	 +----------------------------------------------------------
	 * @param array $user_info
	 +----------------------------------------------------------
	 * @return array
	 +----------------------------------------------------------
	 */
	public function deleteUserGroup($user_info){
		$result = $this->json_client->deleteUserGroup($user_info);
		$result = $this->object_array($result);
		return $result;
	}
	 /**
	 +----------------------------------------------------------
	 * 将xml对象转换为数组
	 +----------------------------------------------------------
	 * @access private 
	 +----------------------------------------------------------
	 * @param object
	 +----------------------------------------------------------
	 * @return array
	 +----------------------------------------------------------
	 */
    private function object_array($object)
    {
       if(is_object($object))
       {
        $object = (array)$object;
       }
       if(is_array($object))
       {
	        foreach($object as $key=>$value)
	        {
	         $object[$key] = $this->object_array($value);
	        }
       }
       return $object;
    }
}

$user = new test_user();
//维护用户信息
$user_info = array(
array(
			'userId'				=>'user07', 		//  boss端ID
			'userAlias'				=>'user06',     	//  用户名
			'userName'				=>'user06', 		//  用户昵称
			'userPassword'			=>'user06',   		//  用户密码
			'payType'				=>'0',				//  用户付费类型
			'phoneNumber'			=>'9032323',		//  用户电话
			'userRank'				=>'1',				//  用户BOSS类型 1：普通用户2：中级用户3：较高级用户4：高级用户5：VIP用户
			'groupCustomerId'		=>'',				//  用户所属机构，为空为运营商
			'setTopBoxIdBindFlag'	=>'YES',			
			'setTopBoxId'			=>'60000',
			'stbType'				=>'001',
			'userTariffType'		=>'0',
			'ip' 					=>'127.0.0.1',
			'mac'					=>'1111',
			'areaCode'				=>'610000',
			'smartCardId'			=>'12123',
			'smartCardIdBindFlag'   =>'',
			'ipBindFlag'			=>'',
			'macBindFlag'			=>'',
		),array(
			'userId'				=>'user08', 		//  boss端ID
			'userAlias'				=>'user06',     	//  用户名
			'userName'				=>'user06', 		//  用户昵称
			'userPassword'			=>'user06',   		//  用户密码
			'payType'				=>'0',				//  用户付费类型
			'phoneNumber'			=>'9032323',		//  用户电话
			'userRank'				=>'1',				//  用户BOSS类型 1：普通用户2：中级用户3：较高级用户4：高级用户5：VIP用户
			'groupCustomerId'		=>'',				//  用户所属机构，为空为运营商
			'setTopBoxIdBindFlag'	=>'YES',			
			'setTopBoxId'			=>'60000',
			'stbType'				=>'001',
			'userTariffType'		=>'0',
			'ip' 					=>'127.0.0.1',
			'mac'					=>'1111',
			'areaCode'				=>'610000',
			'smartCardId'			=>'12123',
			'smartCardIdBindFlag'   =>'',
			'ipBindFlag'			=>'',
			'macBindFlag'			=>'',
		),array(
			'userId'				=>'user09', 		//  boss端ID
			'userAlias'				=>'user06',     	//  用户名
			'userName'				=>'user06', 		//  用户昵称
			'userPassword'			=>'user06',   		//  用户密码
			'payType'				=>'0',				//  用户付费类型
			'phoneNumber'			=>'9032323',		//  用户电话
			'userRank'				=>'1',				//  用户BOSS类型 1：普通用户2：中级用户3：较高级用户4：高级用户5：VIP用户
			'groupCustomerId'		=>'',				//  用户所属机构，为空为运营商
			'setTopBoxIdBindFlag'	=>'YES',			
			'setTopBoxId'			=>'60000',
			'stbType'				=>'001',
			'userTariffType'		=>'0',
			'ip' 					=>'127.0.0.1',
			'mac'					=>'1111',
			'areaCode'				=>'610000',
			'smartCardId'			=>'12123',
			'smartCardIdBindFlag'   =>'',
			'ipBindFlag'			=>'',
			'macBindFlag'			=>'',
		)
);


print_r($user->maintainUserInfo($user_info));exit;


//删除用户
//$user_info = array(
//			'userId'  	=> 'user06',
//	
//		);
//print_r($user->deleteUser($user_info));exit;

//用户绑定集团
//$user_info = array(
//	'userId'  => '100382752',
//	'groupId' => '98004',
//);
//
//
//print_r($user->maintainUserGroup($user_info));exit;
////用户取消绑定集团
//$user_info = array(
//	'userId'  => '100382752',
//	'groupId' => '98004',
//);
//print_r($user->deleteUserGroup($user_info));


$user_info = array(
	'areaCode'=>'',
	'ip'=>'',
	'mac'=>'',
	'smartCardId'=>'',
	'phoneNumber'=>'',
	'setTopBoxId'=>'',
	'stbType'=>'',
	'userId'=>'',
	'userName'=>'',
	'userPassword'=>'',
	'description'=>'',
	'userTariffType'=>'',
);
print_r(json_encode($user_info));exit;


?>