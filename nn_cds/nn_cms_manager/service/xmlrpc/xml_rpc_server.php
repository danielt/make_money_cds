<?php
/**
 * 
 * @access xml server
 * @author chenbo@starcorcn.com
 * @deprecated version - 2012-9-18
 * 
 */

 class xml_rpc_server{
 	private $server_url;
    private $method;
    private $jsonRequest;
    static  $interfaceSafe;
    private $header;
    private $log_inst;
    private $result;
    private $conf;
    private $data;

    public function __construct($conf) {
    	//加载接口映射关系
    	$this->conf = $conf;
    	
		//获取header 扩展参数
    	$this->header['PARAM_1'] = isset($_SERVER['PARAM_1'])?$_SERVER['PARAM_1']:'';
    	$this->header['PARAM_2'] = isset($_SERVER['PARAM_1'])?$_SERVER['PARAM_2']:'';
    	$this->header['PARAM_3'] = isset($_SERVER['PARAM_1'])?$_SERVER['PARAM_3']:'';
    	$this->header['PARAM_4'] = isset($_SERVER['PARAM_1'])?$_SERVER['PARAM_4']:'';
    	//调用接口地址
        $this->server_url = 'http://'.$_SERVER ['HTTP_HOST'].$_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];
        
    }
    
	public function __call($name,$arguments){
		$this->method = $name;
		$arguments = $arguments[0];
		extract($arguments);
		$this->data = $params[0];

    	if(isset($this->conf[$name])){
    		$file_name = $this->conf[$name];
    	}else{
    		exit('指定方法不存在');
    	}
    	include_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'interface/'.$file_name.'.php');
    	$object = new $file_name();
    	$this->result = $object->$name($this->data);
		//写日志
		return $this->result;
	}

     /**
	 +----------------------------------------------------------
	 * 签名验证
	 +----------------------------------------------------------
	 * @access private 
	 +----------------------------------------------------------
	 * @param null
	 +----------------------------------------------------------
	 * @return true/false 
	 +----------------------------------------------------------
	 */
	 private function sign_verify(){
	 	
	 }
     /**
	 +----------------------------------------------------------
	 * 写日志
	 +----------------------------------------------------------
	 * @access private 
	 +----------------------------------------------------------
	 * @param null
	 +----------------------------------------------------------
	 * @return null
	 +----------------------------------------------------------
	 */
	private function writeLog(){
		//实例化log对象
		$this->log_inst = new nns_debug_log_class();
		$this->log_inst->setlogpath('boss');
		//写日志  入参 + 返回数据
		$this->log_inst->write('url:'.$this->client_url.'-------- input:'.json_encode($this->data).'-------result:'.json_encode($this->result));
	}

	/**
	 +----------------------------------------------------------
	 * 接口适配器，配置接口方法名称对应接口文件
	 +----------------------------------------------------------
	 * @access private 
	 +----------------------------------------------------------
	 * @param null 
	 +----------------------------------------------------------
	 * @return null
	 +----------------------------------------------------------
	 */
	private function interface_conf(){
		
		
	}
 }
?>