<?php
/**
 * 
 * @access API boss接口 总入口
 * @author chenbo@starcorcn.com
 * @deprecated version - 2012-9-10
 * 
 */
 header('Content-Type: application/json;charset=utf-8');
 //只处理PSOT请求
if($_SERVER['REQUEST_METHOD'] == 'POST') {
	$post = object_array(json_decode(file_get_contents('php://input')));
	//json 接口处理

	include_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'jsonrpc/json_rpc_server.php');
	include_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'interface_conf.php');
	$server = new json_rpc_server($conf);
	$method_name = $post['method'];
	echo json_encode($server->{$method_name}($post));
	
}
/**
 +----------------------------------------------------------
 * 将json对象转换为数组
 +----------------------------------------------------------
 * @access private 
 +----------------------------------------------------------
 * @param object $object 
 +----------------------------------------------------------
 * @return array
 +----------------------------------------------------------
 */
function object_array($object)
{
   if(is_object($object))
   {
    $object = (array)$object;
   }
   if(is_array($object))
   {
    foreach($object as $key=>$value)
    {
     $object[$key] = object_array($value);
    }
   }
   return $object;
}
?>