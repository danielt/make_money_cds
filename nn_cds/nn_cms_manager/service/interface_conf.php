<?php

/************************** 常量定义 ***********************/

// debug 开关
define('IS_DEBUG','2'); //1、关闭debug ，2、开启debug
// 日志存在目录
define('ROOL_URL',dirname(dirname(dirname(__FILE__))).'/');


/************************ 接口地址配置 ***********************/

//用户基本信息接口url
define('CMS_USER_UCC_URl','http://test.nncms.com/nn_cms_manager/service/cms_json_api.php');
//用户帐户信息url
define('CMS_USER_ACC_URl','http://test.nncms.com/nn_cms_manager/service/cms_xml_api.php');

//用户帐户信息url
define('CMS_TOPIC_URl','http://test.nnvms.com/nn_cms_manager/service/cms_api.php');


/******************* 接口方法名称 对应文件名称映射 ****************/
global $conf;
//用户
$conf['maintainUserInfo'] 	    = 'cms_user';
$conf['deleteUser'] 			= 'cms_user';
$conf['maintainUserGroup'] 	    = 'cms_user';
$conf['deleteUserGroup'] 		= 'cms_user';
//集团
$conf['maintainGroupInfo'] 	    = 'cms_group';
$conf['deleteGroup'] 			= 'cms_group';
//产品资费
$conf['maintainProductFee'] 	= 'cms_product';
$conf['deleteProductFee'] 	    = 'cms_product';
$conf['authorization'] 	        = 'cms_product';
$conf['deleteAuthorization'] 	= 'cms_product';

//信息包
$conf['api_add_topic_item']     = 'cms_topic';
$conf['api_add_topic_item_image'] = 'cms_topic';
?>