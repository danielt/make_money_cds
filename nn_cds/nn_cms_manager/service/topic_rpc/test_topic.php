<?php
/**
 * 
 * @access json client
 * @author chenbo@starcorcn.com
 * @deprecated version - 2012-9-12 client 端封装实现用户接口对接
 * 
 */

include_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'interface_conf.php');
include_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'topic_rpc/rpc_client.php');
class test_topic{
	private $client;
	private $json_client = null;
	function __construct(){
		$this->json_client = new rpc_client(CMS_TOPIC_URl);
	}
	/**
	 * 新增信息导入接口方法
	 */
	function api_add_topic_item($topic_item_info,$topic_item_files){
		$result = $this->json_client->api_add_topic_item($topic_item_info,$topic_item_files);
		//$result = $this->object_array($result);
		return $result;
	}
	/**
	 * 信息相册图片导入接口
	 */
	function api_add_topic_item_image($topic_item_image,$topic_item_image_files){
		$result = $this->json_client->api_add_topic_item_image($topic_item_image,$topic_item_image_files);
		//$result = $this->object_array($result);
		return $result;
	}
}

$topic = new test_topic();
//增加信息包内容
$param = array(
	'topic_id'=>'123456',
	'category_id'=>'654321',
	'title'=>'信息标题',
	'subtitle'=>'信息副标题',
	'keyword'=>'信息关键字',
	'tag'=>'信息EPG标识',
	'summary'=>'信息摘要',
	'link_url'=>'信息外链地址',
	'link_type'=>'信息是否外链',
	'source'=>'信息来源',
	'content'=>'信息内容',
);
$files = array(
	"cover" => "D:\a.jpg",
	'image' =>array('D:\a.jpg','D:\a.jpg','D:\a.jpg')
);

$result = $topic->api_add_topic_item($param,$files);
echo '<pre>';
print_r($result);exit;
//增加信息包内容相册列表
//$param = array(
//	 'summary'=>'图片描述',
//	 'topic_item_id'=>'50ab1e76232104ec8adad25ebec723c2',
//);
//$files = array('image'=>'D:\a.jpg');
//$result = $topic->api_add_topic_item_image($param,$files);
//echo '<pre>';
//print_r($result);exit;
//ob_end_flush();
?>