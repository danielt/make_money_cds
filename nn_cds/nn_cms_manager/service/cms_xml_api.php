<?php
/**
 * 
 * @access API boss接口 xml 入口
 * @author chenbo@starcorcn.com
 * @deprecated version - 2012-9-14
 * 
 */
 header('Content-Type: application/xml;charset=utf-8');
 
 //只处理PSOT请求
if($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	include_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'xmlrpc/xml_rpc_server.php');
	include_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'xmlrpc/xml_rpc_parse.php');
	include_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'interface_conf.php');
	$xml = new xml_rpc_parse();
	
	$post = $xml->xml_decode(file_get_contents('php://input'),'request');

	$server = new xml_rpc_server($conf);
	$method_name = $post['method'];
	$request = $server->{$method_name}($post);
//	echo '<pre>sss'.$method_name;
//	print_r($request);exit;
	echo $xml->xml_encode($server->{$method_name}($post),'response');
	
}

?>