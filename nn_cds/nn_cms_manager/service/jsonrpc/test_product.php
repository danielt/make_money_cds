<?php
/**
 * 
 * @access json client
 * @author chenbo@starcorcn.com
 * @deprecated version - 2012-9-12 client 端封装实现用户接口对接
 * 
 */
include_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'interface_conf.php');
include_once('./json_rpc_client.php');
class test_product{
	private $client;
	private $json_client = null;
	function __construct(){
		$this->json_client = new json_rpc_client(CMS_USER_UCC_URl);
	}
	/**
	 +----------------------------------------------------------
	 * 实现维护产品资费接口 
	 +----------------------------------------------------------
	 * @access public 
	 +----------------------------------------------------------
	 * @param array $prodcut_fee_info
	 +----------------------------------------------------------
	 * @return array
	 +----------------------------------------------------------
	 */
	public function maintainProductFee($product_fee_info){
		
		$result = $this->json_client->maintainProductFee($product_fee_info);
		$result = $this->object_array($result);
		return $result;
	}
	/**
	 +----------------------------------------------------------
	 * 实现删除产品资费接口 
	 +----------------------------------------------------------
	 * @access public 
	 +----------------------------------------------------------
	 * @param array $prodcut_fee_info
	 +----------------------------------------------------------
	 * @return array
	 +----------------------------------------------------------
	 */
	public function deleteProductFee($product_fee_info){
		
		$result = $this->json_client->deleteProductFee($product_fee_info);
		$result = $this->object_array($result);
		return $result;
	}
	public function authorization($authorizationInfo){
		$result = $this->json_client->authorization($authorizationInfo);
		$result = $this->object_array($result);
		return $result;
	}
	 /**
	 +----------------------------------------------------------
	 * 将json对象转换为数组
	 +----------------------------------------------------------
	 * @access private 
	 +----------------------------------------------------------
	 * @param object
	 +----------------------------------------------------------
	 * @return array
	 +----------------------------------------------------------
	 */
    private function object_array($object)
    {
       if(is_object($object))
       {
        $object = (array)$object;
       }
       if(is_array($object))
       {
        foreach($object as $key=>$value)
        {
         $object[$key] = $this->object_array($value);
        }
       }
       return $object;
    }
}

//测试
$product = new test_product();
//$goodList = array(
//	array(
//		'amount'			=>'5',
//		'userTariffType'	=>1,
//		'beginDateTime'		=>'2012-5-15',
//		'currencyType'		=>'RMB',
//		'endDateTime'		=>'2012-5-30',
//		'explanation'		=>'资费1',
//		'gatheringMode'		=>'PREGATHER',
//		'goodsId'			=>'88888',
//		'goodsName'			=>'资费1',
//		'price'				=>1.5,
//		'priceType'			=>'OneTimeCharge',
//		'priceunit'			=>'部',
//		'unit'				=>'元',
//	),
//	array(
//		'amount'			=>'5',
//		'userTariffType'	=>1,
//		'beginDateTime'		=>'2012-5-15',
//		'currencyType'		=>'RMB',
//		'endDateTime'		=>'2012-5-30',
//		'explanation'		=>'资费2',
//		'gatheringMode'		=>'PREGATHER',
//		'goodsId'			=>'88888',
//		'goodsName'			=>'资费2',
//		'price'				=>1.5,
//		'priceType'			=>'OneTimeCharge',
//		'priceunit'			=>'部',
//		'unit'				=>'元',
//	),
//);
//$prodcut_fee_info = array(
//				'productId'=>'IDC_testPro07',
//				'productFeeInfo'=>$goodList,
//				);
//echo '<pre>';
//print_r($product->maintainProductFee($prodcut_fee_info));

//$product_fee_info = array(
//	'productId' =>'IDC_testPro07',
//	'goodsId'	=> '88888',
//);
//echo '<pre>';
//print_r($product->deleteProductFee($product_fee_info));exit;

//$authorizationInfo['availDateTime'] = "2012-06-08 14:12:00";
//$authorizationInfo['invalidationTime'] = "2013-06-08 14:12:00";
//$authorizationInfo['productId']="sss";
//$authorizationInfo['productNumber']=-1;
//$authorizationInfo['productUnit']="部";
//$authorizationInfo['userId']="2222222222";
//$authorizationInfo['goodsId']="2222222222";
//print_r($product->authorization($authorizationInfo));exit;

//$params['productId']="test";
//$params['productName']="测试";
//$params['serviceId']="BTV";
//$params['organizationId']="11111";
//
//$params['contentId']="testGroup";
//$params['contentType']="testGroup";
//$params['categoryId']="testGroup";
//
//print_r($product->authorization($params));exit;


//$params["userId"]="IDC_20120615";
//$params["in1"]="757674";
//
//$listProdInfo = array(
//	'errorMsg'=>'',
//	'flag'=>'',
//	'prodList'=>array(
//		'effDate'=>'',
//		'expDate'=>'',
//		'isOrder'=>'',
//		'orderDate'=>'',
//		'productId'=>'',
//		'productName'=>'',
//		'sp_name'=>'',
//		'goodsId'=>'',
//	),
//);
//print_r(json_encode($listProdInfo));exit;

//$getPublicAcctResponse = array(
//	'acctOutInfo'=>
//		array(
//			'userId'=>'',
//			'fee'=>'',
//			'prepaidFee'=>'',
//		),
//	'flag'=>'',
//	'errorMsg'=>'',
//);
//
//print_r(json_encode($getPublicAcctResponse));exit;
$prodOrderInfo = array(
	'amount'=>'',
	'productId'=>'',
	'goodsId'=>'',
	'tradelogId'=>'',
	'userId'=>'',
);


$ProdOrderOut = array(
	'doneCode'=>'',
	'flag'=>'',
	'errorMsg'=>'',
);
$acctOutInfo = array(
	'fee'=>'',
	'prepaidFee'=>'',
);

$multiAuthorization= array(
	'userId'=>'',
	'authorizationInfoList'=> array(
		'availDateTime'=>'',
		'invalidationTime'=>'',
		'productId'=>'',
		'productNumber'=>'',
		'productUnit'=>'',
		'userId'=>'',
		'goodsId'=>'',
	),
);

$deleteMultiAuthorization = array(
	'productIds'=>array(
		'productId'=>'',
	),
	'userId'=>'',
);



print_r(json_encode($deleteMultiAuthorization));exit;


?>