<?php
/**
 * 
 * @access json client
 * @author chenbo@starcorcn.com
 * @deprecated version - 2012-9-12  client 端封装实现集团接口对接
 * 
 */
include_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'interface_conf.php');
include_once('./json_rpc_client.php');
class test_group{
	private $client;
	private $json_client = null;
	function __construct(){
		$this->json_client = new json_rpc_client(CMS_USER_UCC_URl);
	}
	/**
	 +----------------------------------------------------------
	 * 实现维护集团信息接口
	 +----------------------------------------------------------
	 * @access public 
	 +----------------------------------------------------------
	 * @param array $group_info
	 +----------------------------------------------------------
	 * @return array
	 +----------------------------------------------------------
	 */
	public function maintainGroupInfo($group_info){
		
		$result = $this->json_client->maintainGroupInfo($group_info);
		$result = $this->object_array($result);
		return $result;
	}
	/**
	 +----------------------------------------------------------
	 * 实现删除集团信息接口
	 +----------------------------------------------------------
	 * @access public 
	 +----------------------------------------------------------
	 * @param array $group_info
	 +----------------------------------------------------------
	 * @return array
	 +----------------------------------------------------------
	 */
	public function deleteGroup($group_info){
		
		$result = $this->json_client->deleteGroup($group_info);
		$result = $this->object_array($result);
		return $result;
	}
	 /**
	 +----------------------------------------------------------
	 * 将json对象转换为数组
	 +----------------------------------------------------------
	 * @access private 
	 +----------------------------------------------------------
	 * @param object
	 +----------------------------------------------------------
	 * @return array
	 +----------------------------------------------------------
	 */
    private function object_array($object)
    {
       if(is_object($object))
       {
        $object = (array)$object;
       }
       if(is_array($object))
       {
        foreach($object as $key=>$value)
        {
         $object[$key] = $this->object_array($value);
        }
       }
       return $object;
    }
}

$group = new test_group();
////集团信息维护
//$group_info = array(
//			'groupId'  		=> '456789',
//			'groupName' 	=> '测试集团01',
//			'areaCode'  	=> '001',
//			'description' 	=> '撒旦撒大的撒旦撒旦撒旦撒',
//			'type'			=> '1',
//);
//print_r($group->maintainGroupInfo($group_info));exit;
//删除集团
$group_info = array(
			'groupId'  	=> 'user06',
		);
print_r($group->deleteGroup($group_info));exit;
?>