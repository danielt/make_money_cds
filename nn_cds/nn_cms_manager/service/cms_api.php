<?php
/**
 * 
 * @access API boss接口 总入口
 * @author chenbo@starcorcn.com
 * @deprecated version - 2012-9-10
 * 
 */
 header('Content-Type: application/json;charset=utf-8');
 date_default_timezone_set('PRC');
 //error_reporting(E_ALL &~ E_STRICT &~ E_NOTICE);
 error_reporting(0);
 //只处理PSOT请求
if($_SERVER['REQUEST_METHOD'] == 'POST') {
	$post = $_POST;
	$files = $_FILES;
	
	include_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'topic_rpc/rpc_server.php');
	include_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'interface_conf.php');
	$server = new rpc_server($conf);
	$method_name = $post['method'];
	$result = $server->{$method_name}($post,$files);

	$xml_result = '<?xml version="1.0" encoding="UTF-8"?><response>';
	foreach($result as $key=>$val){
		 $xml_result.= "<$key>$val</$key>";
	}
	
	$xml_result .= '</response>';
	exit($xml_result);
	
}
/**
 +----------------------------------------------------------
 * 将json对象转换为数组
 +----------------------------------------------------------
 * @access private 
 +----------------------------------------------------------
 * @param object $object 
 +----------------------------------------------------------
 * @return array
 +----------------------------------------------------------
 */
function object_array($object)
{
   if(is_object($object))
   {
    $object = (array)$object;
   }
   if(is_array($object))
   {
    foreach($object as $key=>$value)
    {
     $object[$key] = object_array($value);
    }
   }
   return $object;
}
?>