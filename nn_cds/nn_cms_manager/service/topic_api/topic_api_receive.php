<?php
/**
 * 信息包接收... （##如果要改变目录，修改 23 行定义CMS_PATH的值为正确目录就可以了##）
 * 大概流程: 上传如果有的封面和内容图片 -> 添加信息包内容到数据库(返回信息包内容ID) -> 将图片数据与信息包内容ID关联存到图片表中
 * 来源: 信息网关，字段如果确定，是否需要验证，未验证，待修改
 * 
 * @author wuha
 * @version $Id$
 */

// 定义普通响应信息
$responses	= array(0	=> array('ret'	=> 0, 'reason'	=> '推送成功'),
                    1	=> array('ret'	=> 1, 'reason'	=> '验证失败'),
                    6	=> array('ret'	=> 6, 'reason'	=> '保存数据失败')
                    );

/** ----------------------------- TOKEN检查 -------------------------------------- */
(isset($_POST['token']) && 'starcor_crawler' == $_POST['token']) or response($responses[1]);
unset($_POST['token']);

/** ----------------------------- 相关配置 -------------------------------------- */
// 定义CMS根目录常量，当本文件路径有变，请修改这个路径为正确路径
define('CMS_PATH', dirname(dirname(dirname(dirname(__FILE__)))) . DIRECTORY_SEPARATOR);
// 定义逻辑目录，在后面的LOGIC文件中需要
define('LOGIC_DIR', CMS_PATH . 'nn_logic' . DIRECTORY_SEPARATOR);
// 定义存数据库时加到前面的路径，请保持和 UPIMG_PATH 结构相同，两边不带 / 
$image_url_prefix	= 'data/upimg/topic';
// 定义上传目录总目录，下属有cover(封面目录), images(内容图片目录)，请和上面保持结构相同
define('UPIMG_PATH', CMS_PATH . 'data' . DIRECTORY_SEPARATOR . 'upimg' . DIRECTORY_SEPARATOR . 'topic' . DIRECTORY_SEPARATOR);
// 定义一个存已上传成功文件完整路径的数组，方便在失败后清除无用图片
$uploaded_files	= array();


// 导入GUID类库
require CMS_PATH . DIRECTORY_SEPARATOR . 'nn_cms_db' . DIRECTORY_SEPARATOR . 'nns_common' . DIRECTORY_SEPARATOR . 'nns_db_guid_class.php';

// 如果有图片，先上传
if (isset($_FILES[0]))
{
// 	第一张图片处理为封面，保存到cover目录下
// 	先检查是否出现文件传输错误
    if ($result	= is_file_error($_FILES[0]))
    {
// 		出现文件传输错误，响应，这里的错误由is_file_error 返回
        response($result);
    }
// 	没有错误开始保存图片，如果保存成功，将获取到的图片URI 标识存入数组，后面存入数据库
    if ( ! $image_file_names[] = upload_picture('cover', $_FILES[0]))
    {
// 		保存失败的话，响应错误
        response($responses[7]);
    }
// 	如果有第二张图片，开始循环处理上传为内容图片，保存到images文件夹
    if(isset($_FILES[1])) foreach($_FILES as $key => $_file)
    {
// 		过滤掉第一张，那是封面
        if (0 == $key) continue;
        if ($result = is_file_error($_file))
        {
            response($result);
        }
        if ( ! $image_file_names[] = upload_picture('images', $_file))
        {
            response($responses[7]);
        }
    }
}

// 载入DC操作需要的文件
require CMS_PATH . DIRECTORY_SEPARATOR . 'nn_logic' . DIRECTORY_SEPARATOR . 'nl_common.func.php';

// 准备$dc
$dc_config	= array('db_policy'=> NL_DB_WRITE);
$dc	= nl_get_dc($dc_config);
$dc -> open();

// 载入信息包内容逻辑类库
require LOGIC_DIR . 'topic' . DIRECTORY_SEPARATOR . 'topic_item.class.php';
$guid_inst = new Guid();
// 通过GUID 生成信息包内容ID
$topic_item_id	= $guid_inst -> toString();
$item_data	= $_POST;

/** --------------------------------其他配置项------------------------------------------ */
// 默认审核
$item_data['audit'] = 1;
// 充入到文本信息里一起传递
$item_data['id']	= $topic_item_id;
// 如果存在图片，将第一张存为封面图片
isset($image_file_names[0]) && $item_data['image']	= $image_file_names[0];



// 开始信息包内容添加逻辑
if (nl_topic_item :: add_topic_item($dc, $_POST['topic_id'], $_POST['category_id'], $item_data) )
{
// 	如果没有封面或内容就响应成功
    if ( !isset($image_file_names) )
    {
        response($responses[0]);
    }
}
else
{
// 	添加失败响应添加数据失败
    response($responses[6]);
}

// 载入信息包内容图像逻辑类库
require LOGIC_DIR . 'topic' . DIRECTORY_SEPARATOR . 'topic_item_image.class.php';

// 如果有已上传的图片，循环保存到数据库，对应$topic_item_id
if (isset($image_file_names))
{
    foreach($image_file_names as $image_file_name)
    {
        if ( ! nl_topic_item_image :: add_topic_item_image($dc, $topic_item_id, array('image' => $image_file_name)) )
        {
            response($responses[6]);
        }
    }
    response($responses[0]);
}

/** ----------------------------- 函数定义 -------------------------------------- */

/**
 * 响应JSON，如果响应代码不为
 * 
 * @access global
 * @param array $response 响应代码及信息 array('ret' => 1, 'reason' => '错误信息')
 * @return void 直接输出
 */
function response($response)
{
// 	取得已上传图片集合的引用
    global $uploaded_files;
// 	如果响应的状态码非0（不成功）以及存在已上传文件，开始循环回滚删除文件
    if ($response['ret'] && count($uploaded_files))
    {
        foreach($uploaded_files as $file)
        {
            is_file($file) && unlink($file);
        }
    }
    echo json_encode($response);
    exit;
}
/**
 * 取得包含文件名等信息的错误信息字符
 * 
 * @access global
 * @param mixed $key 要取的错误信息代号，同状态码
 * @param mixed $_file 包含name, tmp_name, type, size 等等的$_FILES 图片子数组
 * @return array 返回包含错误状态码和错误信息的数组
 */
function get_format_error($key, $_file)
{
    $responses	= array(
        2 	=> array('ret'	=> 2, 'reason'	=> "上传文件超出了php配置文件约定的值上传文件名称为：{$file['name']} (文件名)"),
        3	=> array('ret'	=> 3, 'reason'	=> '文件只有部分被上传'),
        4	=> array('ret'	=> 4, 'reason'	=> "上传文件类型错误，上传的文件类型为：{$file['type']} (类型)名称为：{$file['name']} (文件名称)"),
        5	=> array('ret'	=> 5, 'reason'	=> "上传文件大小为0，文件名称为：{$file['name']} (文件名称)"),
        7	=> array('ret'	=> 7, 'reason'	=> '图片保存失败')
    );
    return $response[$key];
}
/**
 * 检查是否出现文件传送失败，并在有错时返回错误数组
 * 
 * @access global
 * @param mixed $_file 包含name, tmp_name, type, size 等等的$_FILES 图片子数组
 * @return mixed 如果有文件错误，返回错误数组，没有错误返回false
 */
function is_file_error($_file)
{
    if (UPLOAD_ERR_OK != $_file['error'])
    {
        switch($_file['error'])
        {
            case UPLOAD_ERR_INI_SIZE:
                return get_format_error(2, $_file);
                break;
            case UPLOAD_ERR_PARTIAL:
                return get_format_error(3, $_file);
                break;
            case UPLOAD_ERR_NO_FILE:
                return get_format_error(5, $_file);
                break;
        }
    }
    else
    {
        return false;
    }
}
/**
 * 上传图片并将已上传图片的完整路径保存到数组中
 * 
 * @access global
 * @param mixed $image_category 因为分封面和内容图片，所以通过这个来创建文件夹分别存放
 * @param mixed $_files 包含name, tmp_name, type, size 等等的$_FILES 图片子数组
 * @return mixed 保存图片成功返回要存入数据库的文件路径字符串，保存图片失败返回false
 */
function upload_picture($image_category, $_files)
{
    global $uploaded_files;
    global $image_url_prefix;
    $upimg_path = UPIMG_PATH . $image_category . DIRECTORY_SEPARATOR;
    if (!is_dir($upimg_path))
    {
        mkdir($upimg_path, 0777, true);
    }
    $guid_inst	= new Guid();
    
    $guid_file_name	= $guid_inst -> toString() . strrchr($_files['name'], '.');
    $destination	= $upimg_path . $guid_file_name;
    if (move_uploaded_file($_files['tmp_name'], $destination))
    {
// 		上传成功后，将已上传文件的完整路径存入$uploaded_files ，方便出错时清除;
        $uploaded_files[]	= $destination;
        return "{$image_url_prefix}/{$image_category}/{$guid_file_name}";
    }
    else
    {
        return false;
    }
}