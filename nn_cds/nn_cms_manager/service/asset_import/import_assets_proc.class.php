<?php

/**
 * 媒资导入接口
 */
include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'db.class.php';
include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'curl.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/video/vod_media.class.php';
//include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/nn_logic/media_asset_import_log/media_asset_import_log.class.php';
error_reporting(0);
class import_assets_class extends db
{

	public $log = "";
	public $vod_id;
	public $dc;
	public $re_code = array();
	public $cp_id = null;
	public $download_img_enabled = true;//是否下载图片
	public $clean_illegal_character_enabled = false;//是否转换非法中文字符

	//  当前保存的图片相对路径
	private $current_img_url = '';

	/**
	 * @param boolean $download_img_enabled
	 */
	public function set_download_img_enabled($download_img_enabled=true)
	{
		$this->download_img_enabled = $download_img_enabled;
	}

	private function update_media_data($media_id, $params = null,$nns_cp_id=null)
	{

		if (empty($params))
		{
			return false;
		}
        $set = '';
		foreach ($params as $key => $v)
		{
			$set .= "nns_{$key}='{$v}',";
		}
		$str_update = (strlen($nns_cp_id) > 0 ) ? " and nns_cp_id='{$nns_cp_id}' " : " and nns_cp_id='0' ";
		$set = trim($set, ',');
		$sql = "update nns_vod_media set {$set} where nns_id='{$media_id}' {$str_update}";
		return nl_execute_by_db($sql, $this->dc->db());

	}

	/**
	 * 将基础函数没有插入的数据入扩展库
	 * @param string $video_id:影片注入id
	 * @param array $params
	 */
	private function insert_vod_data_ex($video_id, $params = array(),$nns_cp_id = null)
	{
		if (empty($params) || empty($video_id))
		{
			return null;
		}
		$nns_cp_id = (strlen($nns_cp_id) > 0 ) ? $nns_cp_id : 0;
		$sql_delete = "delete from nns_vod_ex where nns_vod_id='{$video_id}' and nns_cp_id='{$nns_cp_id}'";
		nl_execute_by_db($sql_delete, $this->dc->db());
		$val = "";
 		foreach ($params as $key => $v)
		{
			if (!empty($v))
			{
				$val .= "('{$video_id}','{$key}','{$v}','{$nns_cp_id}'),";
			}
		}
		if(empty($val)) return;
		$val = trim($val, ',');
		$sql = "insert into nns_vod_ex(nns_vod_id,nns_key,nns_value,nns_cp_id) values{$val}";
		return nl_execute_by_db($sql, $this->dc->db());
	}
	
	/**
	 * 将基础函数扩展库删除
	 * @param string $video_id:影片注入id
	 * @param array $params
	 */
	private function delete_vod_data_ex($video_id,$nns_cp_id = null)
	{
		if (empty($params) || empty($video_id))
		{
			return null;
		}
		$nns_cp_id = (strlen($nns_cp_id) > 0 ) ? $nns_cp_id : 0;
		$sql_delete = "delete from nns_vod_ex where nns_vod_id='{$video_id}' and nns_cp_id='{$nns_cp_id}'";
		return nl_execute_by_db($sql_delete, $this->dc->db());
	}
	
	
	/**
	 * 将基础函数没有插入的数据入分集扩展库
	 * @param string 分集注入ID:影片id
	 * @param array $params
	 */
	private function insert_vod_index_data_ex($index_id, $params = array(),$nns_cp_id=null)
	{
		if (empty($params) || empty($index_id))
		{
			return null;
		}
		$nns_cp_id = (strlen($nns_cp_id) > 0) ? $nns_cp_id : 0;
		$sql_delete = "delete from nns_vod_index_ex where nns_vod_index_id='{$index_id}' and nns_cp_id='{$nns_cp_id}'";
		nl_execute_by_db($sql_delete, $this->dc->db());
		$val = "";
		foreach ($params as $key => $v)
		{
			if (!empty($v))
			{
				$val .= "('{$index_id}','{$key}','{$v}','{$nns_cp_id}'),";
			}
		}
		if(empty($val)) return;
		$val = trim($val, ',');
		$sql = "insert into nns_vod_index_ex(nns_vod_index_id,nns_key,nns_value,nns_cp_id) values{$val}";
		return nl_execute_by_db($sql, $this->dc->db());
	}
	
	/**
	 * 将基础函数扩展库删除
	 * @param string $video_id:影片注入id
	 * @param array $params
	 */
	private function delete_vod_index_data_ex($index_id,$nns_cp_id=null)
	{
		if (empty($params) || empty($index_id))
		{
			return null;
		}
		$nns_cp_id = (strlen($nns_cp_id) > 0) ? $nns_cp_id : 0;
		$sql_delete = "delete from nns_vod_index_ex where nns_vod_index_id='{$index_id}' and nns_cp_id='{$nns_cp_id}'";
		return nl_execute_by_db($sql_delete, $this->dc->db());
	}
	/**
	 * 将基础函数没有插入的数据入片源扩展库
	 * @param string 片源注入ID:片源id
	 * @param array $params
	 */
	private function insert_vod_media_data_ex($media_id, $params = array(),$nns_cp_id=null)
	{
		if (empty($params) || empty($media_id))
		{
			return null;
		}
		$nns_cp_id = (strlen($nns_cp_id) > 0) ? $nns_cp_id : 0;
		$sql_delete = "delete from nns_vod_media_ex where nns_vod_media_id='{$media_id}' and nns_cp_id='{$nns_cp_id}'";
		nl_execute_by_db($sql_delete, $this->dc->db());
		$val = "";
		foreach ($params as $key => $v)
		{
			if (!empty($v))
			{
				$val .= "('{$media_id}','{$key}','{$v}','{$nns_cp_id}'),";
			}
		}
		if(empty($val)) return;
		$val = trim($val, ',');
		$sql = "insert into nns_vod_media_ex(nns_vod_media_id,nns_key,nns_value,nns_cp_id) values{$val}";
		return nl_execute_by_db($sql, $this->dc->db());
	}
	
	/**
	 * 将基础函数扩展库删除
	 * @param string $video_id:影片注入id
	 * @param array $params
	 */
	private function delete_vod_media_data_ex($media_id, $nns_cp_id=null)
	{
		if (empty($params) || empty($media_id))
		{
			return null;
		}
		$nns_cp_id = (strlen($nns_cp_id) > 0) ? $nns_cp_id : 0;
		$sql_delete = "delete from nns_vod_media_ex where nns_vod_media_id='{$media_id}' and nns_cp_id='{$nns_cp_id}'";
		return nl_execute_by_db($sql_delete, $this->dc->db());
	}

	/**
	 * 将基础函数没有插入的数据入vod表
	 * @param string $video_id:影片id
	 * @param array $params
	 */
	private function insert_vod_data($video_id, $params = array(),$nns_cp_id = null)
	{
		if (empty($params) || empty($video_id))
		{
			return null;
		}
		$set = "";
		foreach ($params as $key => $v)
		{
			if(strpos($key , 'image') !== false && empty($v))
			{
				continue;
			}
			else
			{
				$set .= "nns_{$key}='{$v}',";
			}
		}
		$set = trim($set, ',');
		$str_where  = (strlen($nns_cp_id) > 0) ? " and nns_cp_id ='{$nns_cp_id}' " : " and nns_cp_id ='0' ";
		//$sql = "update nns_assists_item as a,nns_vod as v set a.nns_video_img2='{$params['image2']}',a.nns_play_count='{$params['play_count']}' where a.nns_video_id=v.nns_id and (v.nns_id='{$video_id}' or v.nns_asset_import_id='{$video_id}')";
		// nl_execute_by_db($sql, $this->dc->db());
		$sql = "update nns_vod set {$set} where (nns_id='{$video_id}' OR nns_asset_import_id='{$video_id}') {$str_where} ";
		return nl_execute_by_db($sql, $this->dc->db());
	}

	/**
	 * 注入分片
	 * @param string $vod_id:注入影片id
	 * @param array $params_index:分片信息
	 * @param array $params_media:片源信息
	 * @return bool：注入是否成功，有可能是分片注入或者是片源注入的情况
	 */
	private function insert_index_data($vod_id, $params_index = array(), $params_media = array())
	{
		//        $params_clip = array(
		//            "index" => $params_index["index"],
		//            "time_len" => $params_index["time_len"],
		//            "name" => $params_index["name"],
		//            "summary" => $params_index["summary"],
		//            "director" => $params_index["director"],
		//            "player" => $params_index["player"],
		//        );
		$params_clip = array_merge($params_index);
		//调用基础函数插入分片信息
		$insert_clip = $this->update_clip($vod_id, $params_index["clip_id"], $params_clip);
		if ($insert_clip === false)
		{
			//写错误日志
			$this->error_logs($params_index, "影片id为{$vod_id}的分片id为{$params_clip["index"]}的分片注入失败，请重新注入影片");
			$this->re_code = array(
				"ret" => 9,
				"reason" => "影片id为{$vod_id}的分片id为{$params_clip["index"]}的分片注入失败，请重新注入影片",
				"id" => $this->vod_id
			);
			return false;
		}
		else
		{
			//写日志
			if (defined("IMPORT_LOG") && IMPORT_LOG === true)
			{
				$this->write_logs($params_clip, "影片id为{$vod_id}的分片id为{$params_clip["index"]}的分片注入成功");
			}
		}
		//组织剩余的数据
		$diff_index_arr = array(
			"image" => $params_index["pic"],
			"play_count" => $params_index["total_clicks"],
			"create_time" => $params_index["update_time"],
		);
		$sql_update_index = "update nns_vod_index set nns_create_time='{$diff_index_arr['create_time']}',nns_image='{$diff_index_arr["image"]}',nns_play_count='{$diff_index_arr['play_count']}' where nns_id='{$insert_clip}'";
		$rr = nl_execute_by_db($sql_update_index, $this->dc->db());
		if ($rr === false)
		{
			//写错误日志
			$this->error_logs($diff_index_arr, "影片id为{$vod_id}的分片id为{$params_clip["index"]}的分片剩余数据注入失败，请重新注入影片");
			$this->re_code = array(
				"ret" => 9,
				"reason" => "影片id为{$vod_id}的分片id为{$params_clip["index"]}的分片剩余数据注入失败，请重新注入影片",
				"id" => $this->vod_id
			);
			return false;
		}
		else
		{
			//写日志
			if (defined("IMPORT_LOG") && IMPORT_LOG === true)
			{
				$this->write_logs($diff_index_arr, "影片id为{$vod_id}的分片id为{$params_clip["index"]}的分片剩余数据注入成功");
			}
		}
		//插入片源信息
		//如果只有一个片源
		$if_ok_import_file = true;
		if (!empty($params_media))
		{
			if (array_key_exists("@attributes", $params_media))
			{
				$if_ok_import_file = $this->insert_media_data($vod_id, $params_index["clip_id"], $params_media["@attributes"], $params_index["time_len"]);
			}
			else
			{
				//如果只有多个片源
				foreach ($params_media as $key_media => $val_media)
				{
					$if_ok_import_file = $this->insert_media_data($vod_id, $params_index["clip_id"], $val_media["@attributes"], $params_index["time_len"]);
					if ($if_ok_import_file === false)
					{
						break;
					}
				}
			}
		}
		return $if_ok_import_file;
	}

	/**
	 * 备注：芒果的没有片源时间，这里直接使用分片的时间
	 * @param string $asset_import_id:影片注入id
	 * @param string $clip_import_id :分片注入id
	 * @param array $params_media    :片源信息
	 * @param int $index_time_len
	 * @return bool,片源是否注入成功
	 */
	private function insert_media_data($asset_import_id, $clip_import_id, $params_media = array(), $index_time_len = 0)
	{

		if ($params_media["file_3d"])
		{
			if ($params_media["file_3d_type"] == 0)
			{
				$file_dimensions = nl_vod_media::DIMENSIONS_3D_LEFT_RIGHT;
			}
			elseif ($params_media["file_3d_type"] == 1)
			{
				$file_dimensions = nl_vod_media::DIMENSIONS_3D_UP_DOWN;
			}
		}
		else
		{
			$file_dimensions = '0';
		}
		//        $params_file = array(
		//            "file_name" => "", //分片名称
		//            "file_type" => $params_media["file_type"],
		//            "file_path" => $params_media["file_path"],
		//            "file_definition" => $params_media["file_definition"],
		//            "file_bit_rate" => $params_media["file_bit_rate"],
		//            "file_desc" => $params_media["file_desc"],
		//            "file_time_len" => $index_time_len, //这里直接使用分片的时间
		//            "file_dimensions" => $file_dimensions
		//        );

		$params_file = array_merge($params_media["@attributes"], array(
			"file_name" => "",
			"file_time_len" => $index_time_len,
			"file_core_id" => "",
			"file_dimensions" => $file_dimensions
		));
		$insert_file = $this->update_file($asset_import_id, $clip_import_id, $params_media['file_id'], $params_file);
		if ($insert_file === false)
		{
			//写错误日志
			$this->error_logs($params_file, "影片id为{$asset_import_id}的分片id为{$clip_import_id}的分片片源注入失败，分片数据为");
			$this->re_code = array(
				"ret" => 9,
				"reason" => "影片id为{$asset_import_id}的分片id为{$clip_import_id}的分片片源注入失败，请重新注入影片",
				"id" => $this->vod_id
			);
			return false;
		}
		else
		{
			//写日志
			if (defined("IMPORT_LOG") && IMPORT_LOG === true)
			{
				$this->write_logs($params_file, "影片id为{$asset_import_id}的分片id为{$clip_import_id}的分片片源注入成功");
			}
		}
		return $insert_file;
	}
	/**
	 * 媒资上下线队列入口
	 * @param unknown $params
	 * @return multitype:|Ambigous <multitype:, array('ret'=>'状态码','reason'=>'原因','data'=>'数据')>|multitype:number string |Ambigous <multitype:number string , Ambigous, multitype:number string multitype:string  , unknown, array('ret'=>'状态码','reason'=>'原因','data'=>'数据')>
	 */
	public function line_asset($params)
	{
		//判断是否为点播（目前只支持点播）
		if ($params["assets_video_type"] != 0)
		{
			$this->re_code = array(
					"ret" => 1,
					"reason" => "请传递点播类型的影片"
			);
			return $this->re_code;
		}
		//判断是否传递了影片id
		if (empty($params["assets_id"]))
		{
			$this->re_code = array(
					"ret" => 1,
					"reason" => "内容id为空"
			);
			return $this->re_code;
		}
		////将xml解析为数组，检查参数是否正确
		$obj = simplexml_load_string($params['assets_video_info']);
		$asset_info = json_decode(json_encode($obj), true);
		
		//查询主媒资分集映射关系
		$vod_info = nl_asset_incidence_relation::query_asset_relation($this->dc,$asset_info["@attributes"]['asset_type'],$params["assets_id"],$asset_info["@attributes"]['cp_id']);
		if($vod_info['ret'] !=0)
		{
			return $vod_info;
		}
		$exists_id = (isset($vod_info['data_info'][0]['nns_id']) && !empty($vod_info['data_info'][0]['nns_id'])) ? $vod_info['data_info'][0]['nns_id'] : false;
		if($exists_id === FALSE)
		{
			return array(
					"ret" => 1,
					"reason" => '媒资上下线'.$params['asset_type']."查无数据"
			);
		}
// 		$asset_id = $asset_info["@attributes"]["assets_id"];
// 		$action =  $asset_info["@attributes"]["action"];
// 		$cp_id =  $asset_info["@attributes"]["cp_id"];
// 		$category_id =  $asset_info["@attributes"]["category_id"];
		//$params_db = array_merge($asset_info["@attributes"], array("point" => 0));
		return  $this->asset_line($params["assets_id"],$asset_info["@attributes"],$exists_id);
	}
	
	
	
	/**
	 * 影片信息修改
	 */
	public function modify_asset($params)
	{
		//判断是否为点播（目前只支持点播）
		if ($params["assets_video_type"] != 0)
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "请传递点播类型的影片"
			);
			return $this->re_code;
		}
		//判断是否传递了影片id
		if (empty($params["assets_id"]))
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "内容id为空"
			);
			return $this->re_code;
		}
		////将xml解析为数组，检查参数是否正确
		$obj = simplexml_load_string($params['assets_video_info']);
		$asset_info = json_decode(json_encode($obj), true);
		//对参数进行检查
		$check = $this->modify_video_check_params($asset_info);
		// 如果参数有问题
		if ($check["if_ok"] !== true)
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => $check["reason"]
			);
			return $this->re_code;
		}
		
		//     对图像进行处理
		$check_img_small = $this->img_handel($asset_info["@attributes"]['smallpic']);
		$check_img_middle = $this->img_handel($asset_info["@attributes"]['middlepic']);
		$check_img_big = $this->img_handel($asset_info["@attributes"]['bigpic']);
		$check_img_v = $this->img_handel($asset_info["@attributes"]['verticality_img']);
		$check_img_h = $this->img_handel($asset_info["@attributes"]['horizontal_img']);
		$check_img_s = $this->img_handel($asset_info["@attributes"]['square_img']);
		//如果图片处理失败
		if (!$check_img_small || !$check_img_big || !$check_img_v || !$check_img_h || !$check_img_s || !$check_img_middle)
		{
			return $this->re_code;
		}
		if(isset($params['conf_info']) && !empty($params['conf_info']) && is_array($params['conf_info']))
		{
			$this->conf_info = $params['conf_info'];
		}
		$nns_cp_id = (strlen($asset_info["@attributes"]['asset_source']) > 0) ? $asset_info["@attributes"]['asset_source'] : 0 ;
		$check_img_big = $check_img_big === TRUE ? '' : $check_img_big;
		$check_img_small = $check_img_small === TRUE ? '' : $check_img_small;
		$check_img_middle = $check_img_middle === TRUE ? '' : $check_img_middle;
		$check_img_v = $check_img_v === TRUE ? '' : $check_img_v;
		$check_img_h = $check_img_h === TRUE ? '' : $check_img_h;
		$check_img_s = $check_img_s === TRUE ? '' : $check_img_s;
		//获取影片id
		$asset_id = $params["assets_id"];
		$params_db = $asset_info["@attributes"];

		//调用基层函数修改
		$vod_res = $this->update_asset($asset_id, $params_db);
		if ($vod_res === false)
		{
			$this->re_code = array(
				"ret" => 9,
				"reason" => '服务器内部错误'
			);
		}
		else
		{
			$this->re_code = array(
				"ret" => 0,
				"reason" => '修改成功'
			);
		}
		if(strlen($vod_res) >0)
		{
		    $sql_del_image="select * from nns_vod where nns_id='{$vod_res}' limit 1";
		    $dc = nl_get_dc(array(
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
            ));
    	    $dc->open();
    	    $result_del_image  =nl_query_by_db($sql_del_image, $dc->db());
    	    $temp_del_img= null;
    	    if(isset($result_del_image[0]) && is_array($result_del_image[0]) && !empty($result_del_image[0]))
    	    {
    	        $temp_del_img[]  = $result_del_image[0]['nns_image0'];
    	        $temp_del_img[]  = $result_del_image[0]['nns_image1'];
    	        $temp_del_img[]  = $result_del_image[0]['nns_image2'];
    	        $temp_del_img[]  = $result_del_image[0]['nns_image3'];
    	        $temp_del_img[]  = $result_del_image[0]['nns_image4'];
    	        $temp_del_img[]  = $result_del_image[0]['nns_image5'];
    	        $temp_del_img[]  = $result_del_image[0]['nns_image_v'];
    	        $temp_del_img[]  = $result_del_image[0]['nns_image_s'];
    	        $temp_del_img[]  = $result_del_image[0]['nns_image_h'];
    	    }
		    $this->img_del_handel($temp_del_img);
		}
		//调用自定义函数修改
		$play_count = $params_db['play_count'] ? $params_db['play_count'] : 0;
		$diff_vod_arr = array(
//			"image0" => $check_img_small, //大海报
//			"image1" => $check_img_small, //中海报
//			"image2" => $check_img_small, //小海报
			"play_count" => $play_count, //点击数,播放次数
			"tag" => $asset_info["@attributes"]["etag"],//yxt这里写死了，以后如果mgtv要扩展需要修改接口文档
//			"image_v" => $check_img_v, //竖图
//			"image_h" => $check_img_h,//横图
//			"image_s" => $check_img_s,//方图
		);
		if(!empty($check_img_small))
		{
			$diff_vod_arr['image0'] = $check_img_small;
			$diff_vod_arr['image1'] = $check_img_small;
			$diff_vod_arr['image2'] = $check_img_small;
		}
		if(!empty($check_img_middle))
		{
			$diff_vod_arr['image1'] = $check_img_middle;
		}
		if(!empty($check_img_big))
		{
			$diff_vod_arr['image0'] = $check_img_big;
		}
		if(!empty($check_img_v))
		{
			$diff_vod_arr['image_v'] = $check_img_v;
		}
		if(!empty($check_img_h))
		{
			$diff_vod_arr['image_h'] = $check_img_h;
		}
		if(!empty($check_img_s))
		{
			$diff_vod_arr['image_s'] = $check_img_s;
		}
		//存放主媒资扩展配置信息
		if(isset($params['conf_info']) && !empty($params['conf_info']) && is_array($params['conf_info']))
		{
			$diff_vod_arr['conf_info'] = json_encode($params['conf_info']);
		}		
		if(isset($params['thrid_party_info']) && is_array($params['thrid_party_info']) && !empty($params['thrid_party_info']))
		{
		    $diff_vod_arr['ext_url'] = json_encode($params['thrid_party_info']);
		}
		$insert_data_re = self::insert_vod_data($asset_info["@attributes"]["assets_id"], $diff_vod_arr,$nns_cp_id);
		//调用自定义函数扩展信息修改
		$diff_vod_arr_ex = array(
			"svc_item_id" => $asset_info["@attributes"]["svc_item_id"],
			"national" => $asset_info["@attributes"]["national"],
			"initials" => $asset_info["@attributes"]["initials"],
			"month_clicks" => $asset_info["@attributes"]["month_clicks"],
			"week_clicks" => $asset_info["@attributes"]["week_clicks"],
			"base_id" => $asset_info["@attributes"]["base_id"],
			"asset_path" => $asset_info["@attributes"]["asset_path"],
			"ex_tag" => $asset_info["@attributes"]["ex_tag"],
			"full_spell" => $asset_info["@attributes"]["full_spell"],
			"awards" => $asset_info["@attributes"]["awards"],
			"year" => $asset_info["@attributes"]["year"],

            "directornew"=>$asset_info["@attributes"]["directornew"],
            "playernew"=>$asset_info["@attributes"]["playernew"],
            "adaptornew"=>$asset_info["@attributes"]["adaptornew"],

            "play_time" => $asset_info["@attributes"]["play_time"],
			"channel" => $asset_info["@attributes"]["channel"],
			"first_spell" => $asset_info["@attributes"]["first_spell"],
			"assets_definition" => $asset_info["@attributes"]["assets_definition"],
			"online_identify " => $asset_info["@attributes"]["online_identify"],
		);
		$insert_data_re = self::insert_vod_data_ex($asset_info["@attributes"]["assets_id"], $diff_vod_arr_ex,$nns_cp_id);
		return $this->re_code;
	}

	/**
	 * 影片参数基础
	 * @param $params:影片参数数组
	 * @return array $re_arr=("if_ok"=>bool,"reason"=>"错误描述")
	 */
	public function modify_video_check_params($params = array())
	{

		if (is_array($params) && array_key_exists("@attributes", $params))
		{
			if (empty($params["@attributes"]["assets_id"]))
			{
				return array(
					"if_ok" => false,
					"reason" => "来源媒资id为空"
				);
			}
		}
		return array(
			"if_ok" => true,
			"reason" => "参数正确"
		);
	}

	/**
	 * 媒资删除
	 */
	public function delete_asset($params)
	{

		//判断参数
		if (empty($params["assets_id"]))
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "没有内容id参数"
			);
			return $this->re_code;
		}
		if (strlen($params["asset_source"]) < 1)
		{
			$this->re_code = array(
					"ret" => 1,
					"reason" => "没有cp id参数"
			);
			return $this->re_code;
		}
		if ($params["assets_video_type"] != 0)
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "请传递点播类型影片"
			);
			return $this->re_code;
		}
		$res_del = parent::delete_asset(array("id" => $params["assets_id"]),true,$params["asset_source"]);
		if ($res_del)
		{
			self::delete_vod_data_ex($params["assets_id"],$params['asset_source']);
			$this->re_code = array(
				"ret" => 0,
				"reason" => "删除成功"
			);
			return $this->re_code;
		}
		elseif ($res_del === false)
		{
			$this->re_code = array(
				"ret" => 2,
				"reason" => "删除失败"
			);
			return $this->re_code;
		}
		else
		{
			$this->re_code = array(
				"ret" => 7,
				"reason" => "媒资不存在"
			);
			return $this->re_code;
		}
	}

	/**
	 * 删除直播频道
	 */
	public function delete_live_channel($params)
	{
		//判断参数
		if (empty($params["live_id"]))
		{
			$this->re_code = array(
					"ret" => 1,
					"reason" => "没有内容id参数"
			);
			return $this->re_code;
		}
		if (strlen($params["live_source"]) < 1)
		{
			$this->re_code = array(
					"ret" => 1,
					"reason" => "没有cp id参数"
			);
			return $this->re_code;
		}
		$res_del = parent::delete_live(array("id" => $params["assets_id"],'action'=>$params['action']),true,$params["asset_source"]);
		if ($res_del)
		{
			self::delete_vod_data_ex($params["assets_id"],$params['asset_source']);
			$this->re_code = array(
					"ret" => 0,
					"reason" => "删除成功"
			);
			return $this->re_code;
		}
		elseif ($res_del === false)
		{
			$this->re_code = array(
					"ret" => 2,
					"reason" => "删除失败"
			);
			return $this->re_code;
		}
		else
		{
			$this->re_code = array(
					"ret" => 7,
					"reason" => "频道不存在"
			);
			return $this->re_code;
		}
	}

	/**
	 * 修改或者增加直播频道
	 */
	public function modify_live_channel($param)
	{
		if(empty($param['channel_id']))
		{
			$return['ret'] = 1;
			$return['reason'] = '频道注入的id为空';
			return $return;
		}
		if(strlen($param['live_source']) < 1)
		{
			$return['ret'] = 1;
			$return['reason'] = '频道注入的cp为空';
			return $return;
		}
		$result = parent::update_live($param);
		if($result['ret'] == '0')
		{
			$this->re_code=array(
					"ret" => 0,
					"reason" => "修改或增加成功",
					'data'=>$result['data']['id'],
				);
		}
		else
		{
			$this->re_code = array(
				'ret' => '1',
				'reason'=>'修改或增加失败',
			);
		}
		return $this->re_code;
	}
	/**
	 * 分片注入
	 */
	public function import_video_clip($params)
	{

		//判断参数是否传递
		if (empty($params['assets_id']))
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "内容id为空"
			);
			return $this->re_code;
		}
		if (empty($params['assets_clip_id']))
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "内容分片id为空"
			);
			return $this->re_code;
		}
		//将xml解析为数组，检查参数是否正确
		$obj = simplexml_load_string($params['assets_clip_info']);
		$clip_info = json_decode(json_encode($obj), true);
		//打点信息数组
		if(!empty($params['seekpoint']))
		{
			$obj_seekpoint = simplexml_load_string($params['seekpoint']);
			$seekpoint_info = json_decode(json_encode($obj_seekpoint),true);
		}
		else
		{
			$seekpoint_info = null;
		}
		
		$nns_cp_id = (strlen($clip_info["@attributes"]['asset_source']) > 0) ? $clip_info["@attributes"]['asset_source'] : 0;
		//写日志
		if (defined("IMPORT_LOG") && IMPORT_LOG === true)
		{
			$this->write_logs(array_merge($clip_info,$seekpoint_info), "注入分片开始，媒资信息为：");
		}
		//对参数进行检查
		$check = $this->import_video_clip_ckeck_params($clip_info);
		if ($check["if_ok"] !== true)
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => $check["reason"]
			);
			return $this->re_code;
		}

		//对图片ftp处理
		$check_img_index = $this->img_handel($clip_info["@attributes"]['pic']);
		if (!$check_img_index)
		{
			$this->write_logs(var_export($this->re_code, true), "图片处理失败");
			return $this->re_code;
		}

		$check_img_index = $check_img_index === TRUE ? '' : $check_img_index;
		
		//存放分集扩展配置信息
		if(!isset($params['conf_info']) || empty($params['conf_info']) || !is_array($params['conf_info']))
		{
			$params['conf_info'] = null;
		}
		$params_seekpoint = !empty($seekpoint_info) ? $seekpoint_info['item'] : null;
		$params_clip = array_merge($clip_info["@attributes"], array('pic' => $check_img_index));

		if(isset($params['thrid_party_info']) && is_array($params['thrid_party_info']) && !empty($params['thrid_party_info']))
		{
		    $params_clip['thrid_party_info'] = $params['thrid_party_info'];
		}
		$insert_clip = $this->update_clip($params['assets_id'], $params['assets_clip_id'], $params_clip,$params_seekpoint,$params['conf_info']);
		if (!empty($insert_clip))
		{
			$this->re_code = array(
				"ret" => 0,
				"reason" => "注入成功"
			);
			//			MGTV需要取第一个分片的信息当做影片片长来保存 BY S67 2013-04-30
			if ($params_clip['index'] == 0)
			{
				$this->modify_vod_other_data($params['assets_id'], array('view_len' => $params_clip['time_len']));
			}
		    
			//写日志
			if (defined("IMPORT_LOG") && IMPORT_LOG === true)
			{
				$this->write_logs(null, "基础信息注入成功");
			}
			//插入剩余的数据
			
			//调用自定义函数扩展信息修改
			$diff_vod_index_arr_ex = array(
				"isintact" => $clip_info["@attributes"]["isintact"],
				"subordinate_name" => $clip_info["@attributes"]["subordinate_name"],
				"initials" => $clip_info["@attributes"]['initials'],
		        "publisher" => $clip_info["@attributes"]['publisher'],
		        "first_spell" => $clip_info["@attributes"]['first_spell'],
		       	"caption_language" => $clip_info["@attributes"]['caption_language'],
		       	"language" => $clip_info["@attributes"]['language'],
		        "region" => $clip_info["@attributes"]['region'],
		        "adaptor" => $clip_info["@attributes"]['adaptor'],
		        "sreach_key" => $clip_info["@attributes"]['sreach_key'],
		        "event_tag" => $clip_info["@attributes"]['event_tag'],
		        "year" => $clip_info["@attributes"]['year'],
                "directornew"=>$clip_info["@attributes"]["directornew"],
                "playernew"=>$clip_info["@attributes"]["playernew"],
                "adaptornew"=>$clip_info["@attributes"]["adaptornew"],
				"sort_name" => $clip_info["@attributes"]['sort_name'],
			);
			self::insert_vod_index_data_ex($clip_info["@attributes"]["clip_id"], $diff_vod_index_arr_ex,$nns_cp_id);
		}
		elseif ($insert_clip === false)
		{
			$this->re_code = array(
				"ret" => 3,
				"reason" => "注入失败"
			);
			$arr_import_desc = $this->import_desc;
			if(!empty($arr_import_desc) && isset($arr_import_desc['ret']))
			{
				$this->re_code = $arr_import_desc;
				$this->import_desc = null;
			}
			//写日志
			if (defined("IMPORT_LOG") && IMPORT_LOG === true)
			{
				$this->write_logs(null, "基础信息注入失败");
			}
		}
		return $this->re_code;
	}

	/**
	 * 检查分片注入参数是否正确
	 * @param array $params
	 */
	public function import_video_clip_ckeck_params($params = array())
	{

		if (is_array("$params") && array_key_exists("@attributes", $params))
		{
			if (empty($params["@attributes"]["assets_id"]))
			{
				return array(
					"if_ok" => false,
					"reason" => "来源媒资id为空"
				);
			}
		}
		return array(
			"if_ok" => true,
			"reason" => "参数正确"
		);
	}

	/**
	 * 分片修改
	 */
	public function modify_video_clip($params)
	{
		//判断参数是否传递
		if (empty($params['assets_id']))
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "内容id为空"
			);
			return $this->re_code;
		}
		if (empty($params['assets_clip_id']))
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "内容分片id为空"
			);
			return $this->re_code;
		}
		//将xml解析为数组，检查参数是否正确
		$obj = simplexml_load_string($params['assets_clip_info']);
		$clip_info = json_decode(json_encode($obj), true);

		//对参数进行检查
		$check = $this->import_video_clip_ckeck_params($clip_info);
		if ($check["if_ok"] !== true)
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => $check["reason"]
			);
			return $this->re_code;
		}

		//对图片ftp处理
		$check_img_index = $this->img_handel($clip_info["@attributes"]['pic']);
		if (!$check_img_index)
		{
			$this->write_logs(var_export($this->re_code, true), "图片处理失败");
			return $this->re_code;
		}

		$check_img_index = $check_img_index === TRUE ? '' : $check_img_index;
		//组合db需要的数组
		//        $params_clip = array(
		//            "index" => $clip_info["@attributes"]['index'],
		//            "time_len" => $clip_info["@attributes"]["time_len"],
		//            "name" => $clip_info["@attributes"]["name"],
		//            "summary" => $clip_info["@attributes"]["summary"],
		//            "director" => $clip_info["@attributes"]["director"],
		//            "player" => $clip_info["@attributes"]["player"],
		//        );

		$params_clip = array_merge($clip_info["@attributes"], array('pic' => $check_img_index));
		$insert_clip = $this->update_clip($params['assets_id'], $params['assets_clip_id'], $params_clip);

		//			MGTV需要取第一个分片的信息当做影片片长来保存 BY S67 2013-04-30
		if ($params_clip['index'] == 0)
		{
			$this->modify_vod_other_data($params['assets_id'], array('view_len' => $params_clip['time_len']));
		}
		if ($insert_clip)
		{
			$this->re_code = array(
				"ret" => 0,
				"reason" => "修改成功"
			);
			//            $this->unline_clip(array(//注入了就将他设置为上线
			//                'assets_clip_id' => $params['assets_clip_id'],
			//                'unline' => 0,
			//            ));

			// $sql_update_index = "update nns_vod_index set nns_release_time='{$clip_info['@attributes']['release_time']}',nns_update_time='{$clip_info['@attributes']['update_time']}', nns_image='{$check_img_index}',nns_play_count='{$clip_info['@attributes']['total_clicks']}' where nns_id='{$insert_clip}'";
			// $r = nl_execute_by_db($sql_update_index, $this->dc->db());
		}
		elseif ($insert_clip === false)
		{
			$this->re_code = array(
				"ret" => 9,
				"reason" => "服务器内部错误"
			);
		}

		return $this->re_code;
	}

	/**
	 * 检查分片修改参数是否正确
	 * @param array $params
	 */
	public function modify_video_clip_ckeck_params($params = array())
	{
		if (is_array("$params") && array_key_exists("@attributes", $params))
		{
			if (empty($params["@attributes"]["assets_id"]))
			{
				return array(
					"if_ok" => false,
					"reason" => "来源媒资id为空"
				);
			}
		}
		return array(
			"if_ok" => true,
			"reason" => "参数正确"
		);
	}

	/**
	 * 分片删除
	 */
	public function delete_video_clip($params)
	{
		if (empty($params["assets_clip_id"]))
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "内容分片id为空"
			);
			return $this->re_code;
		}
		if (strlen($params["asset_source"]) < 1)
		{
			$this->re_code = array(
					"ret" => 1,
					"reason" => "内容cp id为空"
			);
			return $this->re_code;
		}
		if (!empty($params["assets_video_type"]))
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "请传递点播分片"
			);
			return $this->re_code;
		}
		$re = $this->delete_clip(array("id" => $params["assets_clip_id"]),true,$params["asset_source"]);
		if ($re === TRUE)
		{
			$this->re_code = array(
				"ret" => 0,
				"reason" => "删除成功"
			);
			self::delete_vod_index_data_ex($params["assets_clip_id"],$params['asset_source']);
			//写日志
			if (defined("IMPORT_LOG") && IMPORT_LOG === true)
			{
				$this->write_logs($params, "分片删除成功");
			}
		}
		elseif ($re === false)
		{
			//写错误日志
			$this->error_logs($params, "分片删除失败");
			$this->re_code = array(
				"ret" => 9,
				"reason" => "分片删除失败"
			);
		}
		elseif ($re === -1)
		{
			$this->re_code = array(
				"ret" => 7,
				"reason" => "无此分片"
			);
		}
		return $this->re_code;
	}

	/**
	 * 片源注入
	 */
	public function import_video_file($params)
	{		
		if (empty($params["assets_id"]))
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "内容id为空"
			);
			return $this->re_code;
		}
		if (!empty($params["assets_video_type"]))
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "请传递点播片源"
			);
			return $this->re_code;
		}
		if (empty($params["assets_clip_id"]))
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "内容分片id为空"
			);
			return $this->re_code;
		}
		if (empty($params["assets_file_id"]))
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "内容片源id为空"
			);
			return $this->re_code;
		}
		$obj = simplexml_load_string($params['assets_file_info']);
		$file_info = json_decode(json_encode($obj), true);
		if (empty($file_info["@attributes"]["file_id"]))
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "来源片源id为空"
			);
			return $this->re_code;
		}
		if (empty($file_info["@attributes"]["file_type"]))
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "片源类型为空"
			);
			return $this->re_code;
		}
		if(isset($params['conf_info']) && !empty($params['conf_info']) && is_array($params['conf_info']))
		{
			$this->conf_info = $params['conf_info'];
		}

        $file_dimensions = 0;
		if ($file_info["@attributes"]["file_3d"])
		{
			if (isset($file_info["@attributes"]["file_3d_type"]) && strlen($file_info["@attributes"]["file_3d_type"]) > 1)
            {
                $file_dimensions = (int)$file_info["@attributes"]["file_3d_type"];
            }
			else{
                $file_dimensions = 0;
            }
		}
		$nns_cp_id = (strlen($file_info["@attributes"]["asset_source"]) > 0) ? $file_info["@attributes"]["asset_source"] : 0;
		$params['media_service_type'] = (isset($params['conf_info']['import_cdn_mode']) && !empty($params['conf_info']['import_cdn_mode'])) ? $params['conf_info']['import_cdn_mode'] : '';
		$params_file = array_merge($file_info["@attributes"], array(
			"file_name" => $file_info['@attributes']['file_name'],
			"file_dimensions" => $file_dimensions,
			"media_service_type" => $params['media_service_type'],
		));
		$re = $this->update_file($params["assets_id"], $params["assets_clip_id"], $params["assets_file_id"], $params_file);
		if ($re)
		{
			$this->re_code = array(
				"ret" => 0,
				"reason" => "片源注入成功",
			    'nns_id' =>$re,
			);

			$params_media = array();

			$params_media['file_size'] = isset($file_info['@attributes']['file_size']) ? $file_info['@attributes']['file_size'] : 0;
			$params_media['file_time_len'] = isset($file_info['@attributes']['file_time_len']) ? (int)$file_info['@attributes']['file_time_len'] : 0;
			$params_media['file_resolution'] = isset($file_info['@attributes']['file_resolution']) ? $file_info['@attributes']['file_resolution'] : 0;
			$params_media['file_frame_rate'] = isset($file_info['@attributes']['FrameRate']) ? (int)$file_info['@attributes']['FrameRate'] : 0;
			$params_media['drm_enabled'] = isset($file_info['@attributes']['drm_enabled']) ? (int)$file_info['@attributes']['drm_enabled'] : 0;
			//存放片源扩展配置信息
			if(isset($params['conf_info']) && !empty($params['conf_info']) && is_array($params['conf_info']))
			{
				$params_media['conf_info'] = json_encode($params['conf_info']);
			}
			//在nns_ext_url中存放特殊的分组nns_import_id(特殊使用，消息分组反馈需求使用) <feijian.gao@starcor.cn> -->start
			$arr_ext_url = array();
			if(isset($file_info['@attributes']['task_id']))
			{
				$arr_ext_url["nns_import_id"] = $file_info['@attributes']['task_id'];
			}
			else
			{
				$arr_ext_url["nns_import_id"] = $params["assets_file_id"];
			}
			if(isset($file_info['@attributes']['business_type']))
			{
				$arr_ext_url["business_type"] = $file_info['@attributes']['business_type'];
			}
			if(isset($params['playbill_playurls']) && is_array($params['playbill_playurls']) && !empty($params['playbill_playurls']))
			{
			    $arr_ext_url["playbill_playurls"] = $params['playbill_playurls'];
			}
			if(isset($params['thrid_party_playurls']) && is_array($params['thrid_party_playurls']) && !empty($params['thrid_party_playurls']))
			{
			    $arr_ext_url = $params['thrid_party_playurls'];
			}
			$params_media['ext_url'] = json_encode($arr_ext_url);
			//-->end

			$this->update_media_data($re, $params_media,$nns_cp_id);
			
			//调用自定义函数扩展信息修改
			$diff_media_arr_ex = array(
				'file_hash' => $file_info['@attributes']['file_hash'],
				'file_width' => $file_info['@attributes']['file_width'],
				'file_height' => $file_info['@attributes']['file_height'],
				'file_scale' => $file_info['@attributes']['file_scale'],
				'file_coding' => $file_info['@attributes']['file_coding']
			);
			self::insert_vod_media_data_ex($file_info["@attributes"]["file_id"], $diff_media_arr_ex,$nns_cp_id);
			//写日志
			if (defined("IMPORT_LOG") && IMPORT_LOG === true)
			{
				$this->write_logs($params, "片源注入成功");
			}
		}
		elseif ($re === false)
		{
			//写错误日志
			$this->error_logs($params, "片源注入失败");
			$this->re_code = array(
				"ret" => 9,
				"reason" => "片源注入失败"
			);
			$arr_import_desc = $this->import_desc;
			if(!empty($arr_import_desc) && isset($arr_import_desc['ret']))
			{
				$this->re_code = $arr_import_desc;
				$this->import_desc = null;
			}
		}
		return $this->re_code;
	}

	/**
	 * 删除片源
	 */
	public function delete_video_file($params)
	{
		if (empty($params["assets_file_id"]))
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "内容片源ID为空"
			);
			return $this->re_code;
		}
		if (strlen($params["asset_source"]) < 1)
		{
			$this->re_code = array(
					"ret" => 1,
					"reason" => "内容cp ID为空"
			);
			return $this->re_code;
		}
		if (!empty($params["assets_video_type"]))
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "请传递点播片源"
			);
			return $this->re_code;
		}
		if(isset($params['conf_info']) && !empty($params['conf_info']) && is_array($params['conf_info']))
		{
			$this->conf_info = $params['conf_info'];
		}
		$re = $this->delete_file(array("id" => $params["assets_file_id"]),true,$params["asset_source"],$params['media_service_type'],$params["import_source"]);
		if ($re === TRUE)
		{
			$this->re_code = array(
				"ret" => 0,
				"reason" => "片源删除成功"
			);
			self::delete_vod_media_data_ex($params["assets_file_id"],$params['asset_source']);
			//写日志
			if (defined("IMPORT_LOG") && IMPORT_LOG === true)
			{
				$this->write_logs($params, "片源删除成功");
			}
		}
		elseif ($re === false)
		{
			//写错误日志
			$this->error_logs($params, "片源删除失败");
			$this->re_code = array(
				"ret" => 9,
				"reason" => "片源删除失败"
			);
		}
		elseif ($re === -1)
		{
			$this->re_code = array(
				"ret" => 7,
				"reason" => "无此片源"
			);
		}
		return $this->re_code;
	}

	/**
	 * 删除直播片源
	 * @param 	array $param	 删除片源的相关参数数组（片源注入id,片源cpid）
	 * @return array 			 array('ret'=>'0成功,其余数字均表示失败','reason'=>'返回码的说明')
	 * @author feijian.gao
	 * @data	2016年9月29日16:50:50
	 */
	public function delete_live_file($param)
	{
		//参数检查
		if(strlen($param['live_media_id']) < 1 && strlen($param['live_source']) < 1)
		{
			return array(
				'ret' => '1',
				'reason' => '注入id或cp为空',
			);
		}
		$re = $this->delete_channel_file($param,$param["live_source"]);
		return $re;

	}

	/**
	 * 资费定价标签同步
	 */
	public function update_svc_item_id()
	{
		if (empty($params["assets_video_type"]))
		{
			$params["assets_video_type"] = 0;
		}
		if (empty($params["assets_id"]))
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "内容片源ID为空"
			);
			return $this->re_code;
		}
		if (empty($params["svc_item_id"]))
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "定价标签为空"
			);
			return $this->re_code;
		}
		$sql = "update nns_vod_ex set nns_value='{$params["svc_item_id"]}' where nns_vod_id='{$params["assets_id"]}' and nns_key='svc_item_id'";
		$re = nl_execute_by_db($sql, $this->dc->db());
		if ($re)
		{
			$this->re_code = array(
				"ret" => 0,
				"reason" => "定价标签同步成功"
			);
			//写日志
			if (defined("IMPORT_LOG") && IMPORT_LOG === true)
			{
				$this->write_logs($params, "定价标签同步成功");
			}
		}
		elseif ($re === false)
		{
			//写错误日志
			$this->error_logs($params, "定价标签同步成功");
			$this->re_code = array(
				"ret" => 9,
				"reason" => "定价标签同步失败"
			);
		}
		return $this->re_code;
	}

	/**
	 * 图片处理，当有需要下载和上传的图片的时候都需要调用这个方法来先把图片处理了
	 * 如果图片处理没有成功就不进行后面的操作
	 * @param string $img_from_addr://下载的ftp的完整的图片地址,有目录
	 * @return bool
	 */
	public function img_handel($img_from = "")
	{
        $img_from = trim($img_from);
        //如果参数为空直接反馈数据
        if (empty($img_from)) 
        {
            return true;
        }
        // 不需要下载图片
        if (! $this->download_img_enabled) 
        {
            return $img_from;
        }
        // 载入全局的芒果tv的ftp配置数据,是一个二维数组
        if (isset($this->cp_id) && strlen($this->cp_id) > 0 && isset($this->cp_config['g_ftp_conf']['img_enabled']) && $this->cp_config['g_ftp_conf']['img_enabled'] == 1)
        {
            $g_ftp_conf = $this->cp_config['g_ftp_conf'];
        } 
        else 
        {
            global $g_ftp_conf;
        }
        $ignore_img_dowload_fail = false;
        //广西广电忽略海报下载失败
        if(isset($this->cp_config['g_ftp_conf']['ignore_img_dowload_fail']) && $this->cp_config['g_ftp_conf']['ignore_img_dowload_fail'] == 1)
        {
            $ignore_img_dowload_fail = true;
        }
        // 组装保存在本地的临时图片名称
        $img_name = basename($img_from);
        $img_name = str_replace('JPG', 'jpg', $img_name);
        $save_name = $img_name;
        // 默认无后缀的JPG
        $save_name = (substr($save_name, - 1) === '.') ? $save_name . 'JPG' : $save_name;
        $down_name = $img_name;
        // 判断名字中是否存在非法字符
        if ($this->clean_illegal_character_enabled) 
        {
            $img_arr = $this->__clean_illegal_character($img_name);
            $save_name = $img_arr['save_name'];
            $down_name = $img_arr['down_name'];
            $img_from = str_replace($img_name, $down_name, $img_from);
        }
        $img_dir = $g_ftp_conf["down_img_dir"] . '/' . $save_name;
        
        //播控控制生成文件名
        $temp_img_pathinfo = pathinfo($save_name);
        $temp_img_extension = (isset($temp_img_pathinfo['extension']) && strlen($temp_img_pathinfo['extension']) > 0) ? strtolower($temp_img_pathinfo['extension']) : 'jpg';
        $save_name_v2 = np_guid_rand() . '.' . $temp_img_extension;
        // 配置在子配置文件里面
        
        // 加载np_ftp
        include_once NPDIR . '/np_ftp.php';
        
        // 检查加载文件保存路径是否存在，不存在创建
        if (! empty($g_ftp_conf["down_img_dir"]) && ! is_dir($g_ftp_conf["down_img_dir"])) 
        {
            $flg = mkdir($g_ftp_conf["down_img_dir"], 0777, true);
            // 递归创建
            if (! $flg) 
            {
                //广西广电忽略海报下载失败
                if($ignore_img_dowload_fail)
                {
                    return true;
                }
                $this->re_code = array(
                    "ret" => 3,
                    "reason" => "本地创建路径失败:{$g_ftp_conf["down_img_dir"]}",
                    "id" => $this->vod_id
                );
                return false;
            }
        } 
        else 
        {
            // 如果存在修改权限
            @chmod($g_ftp_conf["down_img_dir"], 0777);
        }
        // 这个配置项在子配置文件里面
        
        if (stripos($img_from, 'http://') === FALSE && stripos($img_from, 'https://') === FALSE && stripos($img_from, 'ftp://') === FALSE) 
        {
            if (! isset($g_ftp_conf["domain"]) || empty($g_ftp_conf["domain"])) 
            {
                return true;
            }
            $down_img = $g_ftp_conf["domain"] . $img_from;
            // 增加下载图片的规则
            if (isset($g_ftp_conf['rule']) && ! empty($g_ftp_conf["rule"]) && isset($g_ftp_conf['rule_domain']) && ! empty($g_ftp_conf["rule_domain"])) 
            {
                if (stripos($img_from, $g_ftp_conf['rule']) !== false) 
                {
                    $down_img = $g_ftp_conf['rule_domain'] . $img_from;
                }
            }
        } 
        else 
        {
            $down_img = $img_from;
        }
        if (defined('IMPORT_CURL_IMG_PORT') && IMPORT_CURL_IMG_PORT) 
        {
            $setopt = array(
                'port' => IMPORT_CURL_IMG_PORT
            );
        } 
        else 
        {
            $setopt = array(
                'port' => '80'
            );
        }
        if (stripos($down_img, 'https://') !== false) 
        {
            $setopt = array(
                'port' => '443'
            );
        }
        if (stripos($down_img, 'ftp://') !== false) 
        {
            $setopt = array(
                'port' => '21'
            );
        }
        // var_dump($setopt);die;
        $arr_url_parse = parse_url($down_img);
        if (isset($arr_url_parse['port']) && ! empty($arr_url_parse['port'])) 
        {
            $setopt = array(
                'port' => $arr_url_parse['port']
            );
        }
        // 下载图片是否需要获取图片重命名之前的图片文件名
        if (defined('IMPORT_RENAME_IMG') && IMPORT_RENAME_IMG && defined('IMPORT_SEPARATOR') && IMPORT_SEPARATOR) 
        {
            $img_orginal_arr = explode(IMPORT_SEPARATOR, $down_name);
            $link_name = end($img_orginal_arr);
            $link_arr = explode('.', $link_name);
            $down_img = str_replace(IMPORT_SEPARATOR . $link_arr[0], '', $down_img);
        }
        $down_img = rtrim($down_img, '.');
        $cu = new curl($setopt);
        $contents = $cu->get($down_img);
        $getinfo = $cu->getInfo();
        $infocode = $getinfo['after']['http_code'];
        if ($contents === false || ($infocode != '200' && $infocode != '226')) 
        {
            //广西广电忽略海报下载失败
            if($ignore_img_dowload_fail)
            {
                return true;
            }
            $error = $cu->error();
            $error_no = $cu->errno();
            $this->re_code = array(
                "ret" => 3,
                "reason" => "curl获取图片失败失败代码：" . $error_no . ",信息：" . $error . "，图片参数为:{$down_img}",
                "id" => $this->vod_id
            );
            return false;
        }
        
        $handle = fopen($img_dir, "w");
        $re = fwrite($handle, $contents);
        fclose($handle);
        if (empty($re)) 
        {
            //广西广电忽略海报下载失败
            if($ignore_img_dowload_fail)
            {
                return true;
            }
            $this->re_code = array(
                "ret" => 3,
                "reason" => "本地保存文件失败",
                "id" => $this->vod_id
            );
            return false;
        }
        
        
        $remote_path = '';
        if (defined("ROOT_IMG_CATEGORY") && ROOT_IMG_CATEGORY) 
        {
            $remote_path = ROOT_IMG_CATEGORY;
        }
        // 兼容FTP全路径的图片
        if (stripos($img_from, 'ftp://') !== FALSE || stripos($img_from, 'http://') !== FALSE || stripos($img_from, 'https://') !== FALSE) 
        {
            $img_from = ltrim($arr_url_parse['path'], '/');
        }
        $img_extend_dir = dirname($img_from);
        if ($img_extend_dir !== '.') 
        {
            $remote_path = $remote_path . '/' . $img_extend_dir;
            $remote_path = str_replace("//", "/", $remote_path);
        }
        
        //播控生成自己的文件名规则了，cp+日期+guid
        $str_date = date("Ymd");
        $str_path_with_cp = (! isset($this->cp_id) || strlen($this->cp_id) < 1) ? 'cp_0/' . $str_date : 'cp_' . strtolower($this->cp_id) . '/' . $str_date;
        $bk_img_store_dir = rtrim(rtrim($g_ftp_conf["down_img_dir"] . "/prev/KsImg/" . $str_path_with_cp, '/'), '\\');
        $bk_img_store_relative_dir = rtrim(rtrim("/prev/KsImg/" . $str_path_with_cp, '/'), '\\');
        
        
        if (! is_dir($bk_img_store_dir)) 
        {
            $flg = mkdir($bk_img_store_dir, 0777, true);
            if (! $flg) 
            {
                //广西广电忽略海报下载失败
                if($ignore_img_dowload_fail)
                {
                    return true;
                }
                $this->re_code = array(
                    "ret" => 3,
                    "reason" => "本地创建路径失败:{$g_ftp_conf["down_img_dir"]}:{$bk_img_store_dir}",
                    "id" => $this->vod_id
                );
                return false;
            }
            @chmod($bk_img_store_dir, 0777);
        }
        
        if (file_exists($bk_img_store_dir . '/' . $save_name_v2)) 
        {
            @unlink($bk_img_store_dir . '/' . $save_name_v2);
        }
        $img = copy($g_ftp_conf["down_img_dir"] . '/' . $save_name, $bk_img_store_dir . '/' . $save_name_v2);
        if (! $img) 
        {
            //广西广电忽略海报下载失败
            if($ignore_img_dowload_fail)
            {
                return true;
            }
            $this->re_code = array(
                "ret" => 3,
                "reason" => "本地创建路径失败:远程文件[{$g_ftp_conf["down_img_dir"]}" . "/" . "{$save_name}];存储文件{$bk_img_store_dir}" . "/" . "{$save_name_v2}",
                "id" => $this->vod_id
            );
            return false;
        }
        unlink($g_ftp_conf["down_img_dir"] . '/' . $save_name);

        //是否将注入的图片上传到指定ftp。
        if (!$g_ftp_conf['upload_img_to_ftp_enabled'])
        {
            return str_replace("//", "/", str_replace("//", "/", $bk_img_store_relative_dir . '/' . $save_name_v2));
        }
        else
        {//上传到ftp
            $img_save_path = str_replace("//", "/", str_replace("//", "/", $bk_img_store_relative_dir . '/' . $save_name_v2));
            $img_dir = $g_ftp_conf['upload_img_to_ftp_url'];
            $result = $this->__import_up_ftp_img($g_ftp_conf["down_img_dir"].$img_save_path, $img_dir);

            if ($result === false)
            {
                //广西广电忽略海报下载失败
                if($ignore_img_dowload_fail)
                {
                    return true;
                }
                return false;
            }
            return $img_save_path;
        }
    }
    
    
    public function img_del_handel($array_img)
    {
        if(!is_array($array_img) || empty($array_img))
        {
            return true;
        }
        // 载入全局的芒果tv的ftp配置数据,是一个二维数组
        if (isset($this->cp_id) && strlen($this->cp_id) > 0 && isset($this->cp_config['g_ftp_conf']['img_enabled']) && $this->cp_config['g_ftp_conf']['img_enabled'] == 1)
        {
            $g_ftp_conf = $this->cp_config['g_ftp_conf'];
        }
        else
        {
            global $g_ftp_conf;
        }
        $down_img_dir = isset($g_ftp_conf["down_img_dir"]) ? $g_ftp_conf["down_img_dir"] : '';
        $down_img_dir = strlen($down_img_dir) >0 ? trim(rtrim(rtrim($down_img_dir,'/'),'\\')) : '';
        if(strlen($down_img_dir)<1)
        {
            return true;
        }
        foreach ($array_img as $val)
        {
            $val = trim(ltrim(ltrim($val,'/'),'\\'));
            if(strlen($val)<1)
            {
                continue;
            }
            @unlink($down_img_dir.'/'.$val);
        }
    }
	/**
	 * 中文符号转换
	 * @param $img_name 图片地址
	 * @return array
	 */
	private function __clean_illegal_character($img_name)
	{
		$new_name = $img_name;
		if(stripos($img_name, "：") !== false)
		{
			$img_name = str_replace("：", "%A3%BA", $img_name);
			$new_name = str_replace("：", "", $new_name);
		}
		if(stripos($img_name, "？") !== false)
		{
			$img_name = str_replace("？", "%A3%BF", $img_name);
			$new_name = str_replace("？", "", $new_name);
		}
		if(stripos($img_name, "“") !== false)
		{
			$img_name = str_replace("“", "%A1%B0", $img_name);
			$new_name = str_replace("“", "", $new_name);
		}
		if(stripos($img_name, "”") !== false)
		{
			$img_name = str_replace("”", "%A1%B1", $img_name);
			$new_name = str_replace("”", "", $new_name);
		}
		if(stripos($img_name, "！") !== false)
		{
			$img_name = str_replace("！", "%A3%A1", $img_name);
			$new_name = str_replace("！", "", $new_name);
		}
		if(stripos($img_name, "。") !== false)
		{
			$img_name = str_replace("。", "%A1%A3", $img_name);
			$new_name = str_replace("。", "", $new_name);
		}
		if(stripos($img_name, "，") !== false)
		{
			$img_name = str_replace("，", "%A3%AC", $img_name);
			$new_name = str_replace("，", "", $new_name);
		}	
		if(stripos($img_name, "、") !== false)
		{
			$img_name = str_replace("、", "%A1%A2", $img_name);
			$new_name = str_replace("、", "", $new_name);
		}
		if(stripos($img_name, " ") !== false)
		{
			$img_name = str_replace(" ", "%20", $img_name);
			$new_name = str_replace(" ", "", $new_name);
		}
		if(stripos($img_name, "（") !== false)
		{
			$img_name = str_replace("（", "%A3%A8", $img_name);
			$new_name = str_replace("（", "", $new_name);
		}
		if(stripos($img_name, "）") !== false)
		{
			$img_name = str_replace("）", "%A3%A9", $img_name);
			$new_name = str_replace("）", "", $new_name);
		}
		if(stripos($img_name, "；") !== false)
		{
			$img_name = str_replace("；", "%A3%BB", $img_name);
			$new_name = str_replace("；", "", $new_name);
		}
		if(stripos($img_name, "《") !== false)
		{
			$img_name = str_replace("《", "%A1%B6", $img_name);
			$new_name = str_replace("《", "", $new_name);
		}
		if(stripos($img_name, "》") !== false)
		{
			$img_name = str_replace("》", "%A1%B7", $img_name);
			$new_name = str_replace("》", "", $new_name);
		}
		if(stripos($img_name, "·") !== false)
		{
			$img_name = str_replace("·", "%A1%A4", $img_name);
			$new_name = str_replace("·", "", $new_name);
		}
		if(stripos($img_name, "—") !== false)
		{
			$img_name = str_replace("—", "%A1%AA", $img_name);
			$new_name = str_replace("—", "", $new_name);
		}
		return array('save_name' => $new_name,'down_name' => $img_name);
	}

    /**
     * @description:上传到ftp
     * @author:xinxin.deng
     * @date: 2017/11/13 10:47
     * @param $img_name 图片本地地址
     * @param $img_dir //ftp带密码地址
     * @return bool|string
     */
	private function __import_up_ftp_img($img_local_path, $img_dir)
	{
	    $img_arr = explode("/",$img_local_path);
	    $img_name = end($img_arr);
		//实例化np_ftp类
        $arr_ftp = parse_url($img_dir);
        if(!isset($arr_ftp['scheme']) || strtolower($arr_ftp['scheme']) != 'ftp')
        {
            $this->re_code = array(
                "ret" => 3,
                "reason" => "[非正常FTP地址]".$img_dir,
                "id" => $this->vod_id
            );
            return false;
        }
        $host = isset($arr_ftp['host']) ? $arr_ftp['host'] : '';
        $port = isset($arr_ftp['port']) ? $arr_ftp['port'] : 21;
        $user = isset($arr_ftp['user']) ? $arr_ftp['user'] : '';
        $pass = isset($arr_ftp['pass']) ? $arr_ftp['pass'] : '';
        $path = isset($arr_ftp['path']) ? $arr_ftp['path'] : '/';
        if(strlen($host) <1)
        {
            $this->re_code = array(
                "ret" => 3,
                "reason" => "[FTP地址为空]".$img_dir,
                "id" => $this->vod_id
            );
            return false;
        }
        if(strlen($port) <1)
        {
            $this->re_code = array(
                "ret" => 3,
                "reason" => "[FTP端口为空]".$img_dir,
                "id" => $this->vod_id
            );
            return false;
        }
        if(strlen($user) <1)
        {
            $this->re_code = array(
                "ret" => 3,
                "reason" => "[FTP账户为空]".$img_dir,
                "id" => $this->vod_id
            );
            return false;
        }
        if(strlen($pass) <1)
        {
            $this->re_code = array(
                "ret" => 3,
                "reason" => "[FTP密码为空]".$img_dir,
                "id" => $this->vod_id
            );
            return false;
        }
        //连接ftp
		$ftp_to1 = new nns_ftp($host, $user, $pass, $port);
		
		if (!$ftp_to1->connect(5))
		{
			$this->re_code = array(
				"ret" => 3,
				"reason" => "上传ftp连接失败,地址:{$host},用户名:{$user},密码:{$pass},端口:{$port}",
				"id" => $this->vod_id
			);
			return false;
		}
		//上传文件
		$up_img1 = $ftp_to1->up($path, $img_name, $img_local_path);
		if (!$up_img1)
		{
			$this->re_code = array(
				"ret" => 3,
				"reason" => "上传文件失败,ftp路径为:" . $host . ":{$port}/" . $path . "/" . $img_name . ",本地路径为:{$img_name}",
				"id" => $this->vod_id
			);
			return false;
		}

		return $path . '/' . $img_name;
	}

	/**
	 * 日志记录函数
	 * @param array $params//日志参数组
	 * @param string $msg//日志记录说明信息
	 * @return null
	 */
	static function write_logs($params = array(), $msg = "")
	{
		$handle = fopen(dirname(dirname(dirname(dirname(__FILE__)))) . '/data/log/assets_import_log_' . date('Ymd') . '.txt', "a");
		fwrite($handle, "################################" . date("Y-m-n H:i:s", time()));
		fwrite($handle, var_export($msg, true) . "\r\n");
		fwrite($handle, var_export($params, true) . "\r\n");
		fclose($handle);
	}

	public function error_logs($params = array(), $msg = "")
	{
		$handle = fopen(dirname(dirname(dirname(dirname(__FILE__)))) . '/data/log/' . "assets_import_error_log.txt", "a");
		fwrite($handle, var_export($msg, true) . "\r\n");
		fwrite($handle, var_export($params, true) . "\r\n");
		fclose($handle);
	}

	/**
	 * 需要改写影片信息 BY S67 2013-04-30
	 * 1. MGTV的影片长度保存在分片信息中,需要重写。
	 * 2. MGTV的影片上映时间？？？？
	 * @param string $video_id:影片id
	 * @param array $params {
	 "view_len":时长，秒
	 }
	 */
	private function modify_vod_other_data($video_id, $params = array())
	{
		if (empty($params) || empty($video_id))
		{
			return null;
		}
		$set = "";
		foreach ($params as $key => $v)
		{
			$set .= "nns_{$key}='{$v}',";
		}
		$set = trim($set, ',');
		$sql = "update nns_vod set {$set} where nns_id='{$video_id}' OR nns_asset_import_id='{$video_id}'";
		return nl_execute_by_db($sql, $this->dc->db());
	}

	/*     * ***********************************新接口上下线***************************************** */

	public function unline_assets($params)
	{

		if (empty($params['assets_id']))
			return array(
				"ret" => 1,
				"reason" => "媒资ID为空"
			);
		$params['assets_unline'] = $params['assets_unline'] != 0 ? 1 : 0;

		$result = $this->unline_asset(array(
			'assets_id' => $params['assets_id'],
			'unline' => $params['assets_unline']
		));

		if ($result === NULL)
			return array(
				"ret" => 7,
				"reason" => "无此媒资"
			);

		if ($result === FALSE)
			return array(
				"ret" => 9,
				"reason" => "服务器内部错误"
			);

		if ($result === TRUE)
			return array(
				"ret" => 0,
				"reason" => "成功"
			);
	}

	public function unline_video_clip($params)
	{

		if (empty($params['assets_clip_id']))
			return array(
				"ret" => 1,
				"reason" => "媒资分片ID为空"
			);
		$params['assets_clip_unline'] = $params['assets_clip_unline'] ? $params['assets_clip_unline'] : 0;

		$result = $this->unline_clip(array(
			'assets_clip_id' => $params['assets_clip_id'],
			'unline' => $params['assets_clip_unline']
		));

		if ($result === NULL)
			return array(
				"ret" => 7,
				"reason" => "无此分片"
			);

		if ($result === FALSE)
			return array(
				"ret" => 9,
				"reason" => "服务器内部错误"
			);

		if ($result === TRUE)
			return array(
				"ret" => 0,
				"reason" => "成功"
			);
	}

	public function unline_video_file($params)
	{

		if (empty($params['assets_file_id']))
			return array(
				"ret" => 1,
				"reason" => "媒资分片ID为空"
			);
		$params['assets_file_unline'] = $params['assets_file_unline'] ? $params['assets_file_unline'] : 0;

		$result = $this->unline_file(array(
			'assets_file_id' => $params['assets_file_id'],
			'unline' => $params['assets_file_unline']
		));

		if ($result === NULL)
			return array(
				"ret" => 7,
				"reason" => "无此分片"
			);

		if ($result === FALSE)
			return array(
				"ret" => 9,
				"reason" => "服务器内部错误"
			);

		if ($result === TRUE)
			return array(
				"ret" => 0,
				"reason" => "成功"
			);
	}

	//更新主媒资的分集总数
	public function modify_asset_index_num($params)
	{
		if (empty($params['assets_id']))
			return array(
				"ret" => 1,
				"reason" => "媒资ID为空"
			);
		if (empty($params['all_index_num']))
			return array(
				"ret" => 1,
				"reason" => "分级总数为空"
			);
		$sql = "select * from nns_vod where nns_import_source='" . IMPORT_SOURCE . "' and nns_asset_import_id='" . $params['assets_id'] . "'";
		$vod_info = nl_db_get_one($sql, $this->dc->db());
		if (is_array($vod_info))
		{
			if ($vod_info['nns_all_index'] < $params['all_index_num'])
			{
				$sql_index = "select * from nns_vod_index where nns_vod_id='{$vod_info['nns_id']}' order by nns_index asc limit 1";
				$index_info = nl_db_get_one($sql_index, $this->dc->db());
				$view_time = 0;
				if (is_array($index_info))
				{
					$view_time = $index_info['nns_time_len'];
				}
				$update_sql = "update nns_vod set nns_all_index='{$params['all_index_num']}',nns_view_len='$view_time' where nns_id='{$vod_info['nns_id']}'";
				$vod = nl_execute_by_db($update_sql, $this->dc->db());
				if ($vod)
				{
					return array(
						"ret" => 0,
						"reason" => "分集总数更新成功"
					);
				}
				else
				{
					return array(
						"ret" => 1,
						"reason" => "分集总数更新失败"
					);
				}
			}
			else
			{
				return array(
					"ret" => 1,
					"reason" => "分集不更新"
				);
			}
		}
		else
		{
			return array(
				"ret" => 1,
				"reason" => "媒资信息获取失败"
			);
		}
		//nl_execute_by_db($sql, $this->dc->db());
	}

	/**
	 * 回写数据
	 * @param string $import_source_id  注入Id
	 * @param array  $params
	 */
	public function modify_media_data($import_source_id, $params)
	{
		if (empty($import_source_id))
			return array(
				"ret" => 1,
				"reason" => "媒资ID为空"
			);
		if (empty($params))
			return array(
				"ret" => 1,
				"reason" => ""
			);
		$sql = "select * from nns_vod_media where nns_import_source='" . IMPORT_SOURCE . "' and nns_import_id='" . $import_source_id . "'";
		$media_info = nl_db_get_one($sql, $this->dc->db());
		if (is_array($media_info))
		{
			$where = "nns_import_source='" . IMPORT_SOURCE . "' and nns_import_id='" . $import_source_id . "'";
			$media = nl_db_update($this->dc->db(), 'nns_vod_media', $params, $where);
			if ($media)
			{
				return array(
					"ret" => 0,
					"reason" => ""
				);
			}
			else
			{
				return array(
					"ret" => 1,
					"reason" => "更新失败"
				);
			}
		}
		else
		{
			return array(
				"ret" => 1,
				"reason" => "没有片源"
			);
		}
	}

	/**
	 * 回写数据
	 * @param string $import_source_id  注入Id
	 * @param array  $params
	 */
	public function modify_vod_index_data($import_source_id, $params)
	{
		if (empty($import_source_id))
			return array(
				"ret" => 1,
				"reason" => "媒资ID为空"
			);
		if (empty($params))
			return array(
				"ret" => 1,
				"reason" => ""
			);
		$sql = "select * from nns_vod_index where nns_import_source='" . IMPORT_SOURCE . "' and nns_import_id='" . $import_source_id . "'";
		$media_info = nl_db_get_one($sql, $this->dc->db());
		if (is_array($media_info))
		{
			$where = "nns_import_source='" . IMPORT_SOURCE . "' and nns_import_id='" . $import_source_id . "'";
			$media = nl_db_update($this->dc->db(), 'nns_vod_index', $params, $where);
			if ($media)
			{
				return array(
					"ret" => 0,
					"reason" => ""
				);
			}
			else
			{
				return array(
					"ret" => 1,
					"reason" => "更新失败"
				);
			}
		}
		else
		{
			return array(
				"ret" => 1,
				"reason" => "没有片源"
			);
		}
	}

	/**
	 * 传入参数返回xml
	 * @param array $params:代码数组
	 * @return string(xml)
	 */
	public function return_xml($params = array())
	{
		/*$error=$this->get_error();
		 if (count($error)>0){
		 $error=$error[0];
		 }else{
		 $error='';
		 }*/
		header('Content-Type: text/xml; charset=utf-8');
		if(strlen(trim($params['ret']))<1){
			$params['ret'] = 1;
			$error='time out';
		}
		return "<result  ret='{$params['ret']}'  reason='{$params['reason']}' id='{$params['id']}' " . "debug='{$error}'" . "></result>";
	}

	/**
	 * 片源入库
	 * @param  array $arr_content
	 * @return array('ret'=>'返回编码,0成功，其余失败','reason'=>'')
	 */
	public function import_live_file_v2($arr_content)
	{
		//单播、组播
		$arr_cast_type = array('unicast','multicast');
		//检查片源id
		if(!isset($arr_content['live_media_id']) || strlen($arr_content['live_media_id']) < 1)
		{
			return array(
				'ret' => '1',
				'reason' => '片源内容id为空',
			);
		}
		//检查组播单播字段的合法性
		if(!in_array($arr_content['live_file_info']['dest_cast_type'], $arr_cast_type) && !in_array($arr_content['live_file_info']['src_cast_type'], $arr_cast_type))
		{
			return array(
					'ret' => '1',
					'reason' => '组播单播字段不合法',
			);
		}
		//返回信息
		$result = $this->insert_live_file($arr_content);
		return $result;
	}

	/**
	 * 节目单入库
	 * @params array $param  节目单信息
	 */
	public function import_live_playbill_deal($param)
	{
		//参数判断 节目开始时间、结束时间、时长都不能为空
		if(empty($param['playbill_info']['start_date']) || empty($param['playbill_info']['start_time']) || empty($param['playbill_info']['duration']))
		{
			return array(
				'ret' => '1',
				'reason' => '播放时间不能为空',
			);
		}
		$result = $this->import_live_playbill($param);
		return $result;
	}

	/**
	 *逻辑删除节目单
	 */
	public function delete_playbill_deal($param)
	{
		//检查节目单注入id
		if(strlen($param['playbill_id']) < 1)
		{
			return array(
				'ret' => 1,
				'reason' => '节目单注入id不合法',
			);
		}
		$result = $this->delete_playbill($param);
		return $result;
	}
	
	
	/**
	 * 透传队列统一入口，即播控不做任何处理，仅生成队列
	 * @param $params
	 * @return array:|Ambigous <multitype:, array('ret'=>'状态码','reason'=>'原因','data'=>'数据')>|multitype:number string |Ambigous <multitype:number string , Ambigous, multitype:number string multitype:string  , unknown, array('ret'=>'状态码','reason'=>'原因','data'=>'数据')>
	 */
	public function pass_queue($params)
	{
		if (strlen($params["type"]) <= 0)
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "透传类型为空"
			);
			return $this->re_code;
		}
		if (strlen($params["cp_id"]) <= 0)
		{
			$this->re_code = array(
				"ret" => 1,
				"reason" => "CPID为空"
			);
			return $this->re_code;
		}
		if(strlen($params["action"]) <= 0)
		{
			$params["action"] = 0;
		}
		//兼容一下读取不到message_id的情况。
        if(strlen($this->message_id) <= 0)
        {
            $message_id = $params['message_id'];
        }
        else
        {
            $message_id = $this->message_id;
        }
		$queue_task_model = new queue_task_model();
		$result = $queue_task_model->q_add_pass_queue_op_mgtv($params['type'],$params['action'],$params["cp_id"],$params['pass_content'],$message_id);
		unset($queue_task_model);
		return  $result;
	}

	/**
	 * 查询融合媒资是否已存在
	 * @param string $import_id  媒资的原始ID （对应于nns_vod表中的nns_asset_import_id）
	 * @return array('ret'=>'返回码','reason'=>'返回状态说明','data'=>array()返回的数据)
	 * @author <feijian.gao@starcor.cn>
	 * @date	2017年2月28日15:19:56
	 */
	public function check_asset_existed($import_id)
	{
		$sql = "select nns_id from nns_vod where nns_asset_import_id = '{$import_id}'";
		$result = nl_query_by_db($sql, $this->dc->db());
		if(is_array($result))
		{
			return array("ret" => 0, "reason" => "媒资已存在", "data" => $result);
		}
		return array("ret" => 1, "reason" => "媒资不存在", "data" => array());
	}
}

//include_once '/data/web/np/np_ftp.php';
//$ftp_to1=new  nns_ftp("58.83.191.23","intertv","hntv*UHB",21);
//$a=$ftp_to1->connect();
//$up_img1=$ftp_to1->up("/intertv/prev/KsImg/","CCCcc.jpg","/data/web/nn_cms/data/a.jpg");
//var_dump($up_img1);die();
//import_assets_class::write_logs(array(1,2,3,4),'fda');
