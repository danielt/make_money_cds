<?php

/**
 * 媒资注入api接口
 * @author yxt2013年1月18日11:17:30
 */
//		echo '<pre>';print_r($_POST);die();
//ini_set('display_errors',1);


include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'db.class.php';
//die;
include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) .  '/mgtv_v2/mgtv_init.php';
//die;
error_reporting(E_ALL ^ E_NOTICE);
define('IMPORT_SOURCE', "mgtv");
date_default_timezone_set("PRC"); //设置时区（中国）
ini_set("max_execution_time", "0");
define("IMPORT_LOG", TRUE); //注入是否开启日志记录
//if(IMPORT_LOG===TRUE) chmod(dirname(__FILE__), 0777);
$func = $_POST['func'];
//post提交的func参数和类的对应关系,如果接口函数改变名字了需要修改这里的对应关系，键名为post提交过来的方法名
$compare = array("import_assets" => "import_assets");
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'mgtv' . DIRECTORY_SEPARATOR . "import_assets_proc.class.php";
if($func=='unline_video_file' || $func =='unline_video_clip' ||  $func =='unline_assets') {
	 header('Content-Type: text/xml; charset=utf-8');	
	echo  '<result  ret="0"  reason="no impl" id=""></result>';exit;
}
if(!is_dir(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/data/log/asset_import/')){
	mkdir(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/data/log/asset_import/',0777,true);
}


$dt = date('Y-m-d');
error_log('['.date('H:i:s').']--------------'.$func."----------------------------\n".var_export($_POST,true)."\n",3,dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/data/log/asset_import/'.$dt.'.txt');
class asset_api extends db {

        static public function run() {
                global $func;
                global $compare;
				$id = np_guid_rand();
                $class_obj = new import_assets_class();
                $dc = nl_get_dc(array("db_policy" => NL_DB_WRITE, "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE));
                $dc->open();
                $class_obj->dc = $dc;
                //这方法返回的都是一个带有三个元素的一维数组
                $re_code = $class_obj->$func();
				import_model::log_write($func, $_POST, $re_code['reason'], $re_code['ret']);
                $result = self::return_xml($re_code);
                //include_once dirname(__FILE__) . "/cov_pinyin.php";
                return $result;
        }

        /**
         * 传入参数返回xml
         * @param array $params:代码数组
         * @return string(xml)
         */
        static private function return_xml($params = array()) {
                header('Content-Type: text/xml; charset=utf-8');
                return "<result  ret=\"{$params['ret']}\"  reason=\"{$params['reason']}\" id='{$params['id']}'></result>";
        }

}

$result = asset_api::run();

error_log('['.date('H:i:s')."]--------------return-------------------\n".$result."\n",3,dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/data/log/asset_import/'.$dt.'.txt');
echo $result;


//end of the file