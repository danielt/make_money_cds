<?php

/**
 * 媒资注入api接口
 * @author yxt2013年1月18日11:17:30
 */
//		echo '<pre>';print_r($_POST);die();
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'db.class.php';
error_reporting(E_ALL ^ E_NOTICE);
error_reporting(E_ALL & ~(E_STRICT | E_NOTICE));
if ($_GET['debug']==1){
ini_set('display_errors',1);
error_reporting(E_ALL);
var_dump($_REQUEST);
}
define('IMPORT_SOURCE', "mgtv");
date_default_timezone_set("PRC"); //设置时区（中国）
ini_set("max_execution_time", "0");

set_time_limit(0);
// 图片保留路径
define("IMPORT_IMG_CATEGORY", 0);
// 上传图片后，是否删除本地图片
define("IMPORT_DEL_LOCAL_IMG", true);
define("IMPORT_LOG", false); //注入是否开启日志记录
//if(IMPORT_LOG===TRUE) chmod(dirname(__FILE__), 0777);
$func = $_POST['func'];
//post提交的func参数和类的对应关系,如果接口函数改变名字了需要修改这里的对应关系，键名为post提交过来的方法名
$compare = array("modify_video" => "modify_asset");


include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) .  '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(__FILE__)) .  DIRECTORY_SEPARATOR . "import_assets_proc.class.php";
if($func=='unline_video_file' || $func =='unline_video_clip' ||  $func =='unline_assets') {
	 header('Content-Type: text/xml; charset=utf-8');	
	echo  '<result  ret="0"  reason="no impl" id=""></result>';exit;
}
class asset_api extends db {

        static public function run() {
                global $func;
                global $compare;
                $class_obj = new import_assets_class();
                $class_obj->check_perfect=true;
//                $class_obj->core_import=true;
//                $class_obj->core_policy_id='test';
                $dc = nl_get_dc(array("db_policy" => NL_DB_WRITE, "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE));
                $dc->open();
                $class_obj->dc = $dc;
                
                $func=isset($compare[$func])?$compare[$func]:$func;
                
                //这方法返回的都是一个带有三个元素的一维数组
                $re_code = $class_obj->$func($_POST);
				
                echo $class_obj->return_xml($re_code);
				
				
				import_model::log_write($func, $_POST, $re_code['reason'], $re_code['ret']);
                
                if (IMPORT_LOG===TRUE){
                	$error=$class_obj->get_error();
					 if (count($error)>0){
                		$class_obj->write_log(var_export($class_obj->get_error(),true),'error');
					 }
                }
				$class_obj->write_log(var_export($_POST,true),'asset');
                $class_obj->write_log(var_export($re_code,true),'asset');
                
//                $_GET['ok'] = 'no';
//                include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/controls/nns_string.php";
        }

        

}

return asset_api::run();