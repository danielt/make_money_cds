<?php

/*
 * Created on 2013-1-16
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_cms_config/nn_cms_global.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_cms_manager/nncms_manager_inc.php";
//include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_cms_db/nns_assist/nns_db_assist_item_class.php";
//include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_cms_db/nns_service/nns_db_service_category_class.php";
// include_once dirname(dirname(dirname(__FILE__))). "/nn_cms_db/nns_live/nns_db_live_class.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_cms_db/nns_vod/nns_db_vod_class.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_cms_db/nns_depot/nns_db_depot_class.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_cms_db/nns_vod/nns_db_vod_index_class.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_cms_db/nns_vod/nns_db_vod_media_class.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_cms_db/nns_live/nns_db_live_class.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_cms_db/nns_live/nns_db_live_ex_class.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_cms_db/nns_live/nns_db_live_media_class.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_cms_db/nns_debug_log/nns_debug_log_class.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_cms_db/nns_common/nns_db_guid_class.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_logic/cdn/cdn_live_queue.class.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_logic/cdn/cdn_live_media_queue.class.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_logic/cdn/cdn_playbill_queue.class.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_logic/live/live.class.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_logic/live/live_media.class.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_logic/live/playbill.class.php";

include_once LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'vod.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_cms_manager/controls/nncms_controls_pinyin_class.php";

include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/models/queue_task_model.php';


class db
{


    //融合标示  true 融合CP | false 非融合CP
    public $fuse_enabled = FALSE;
    // 	日志类
    public $debug_log;
    // 	注入core平台开关
    public $core_import = FALSE;
    // 	注入core平台时的策略ID
    public $core_policy_id = '';
    public $spcial_word = 'default';
    public $int_weight = 0;
    public $is_group = 0;
    public $import_desc = null;

    public $message_id = '';
    public $media_import_module = false;//不同注入ID相同清晰度的片源是否先删后增模式
    public $media_import_comm = false;//注入ID相同，是否先删后增
    public $cp_config = null;
    private $db_obj;
    public $media_import_module_v2 = false;//芒果新的片源替换逻辑
    public $media_mode = array(
        'low' => 1,
        'std' => 2,
        'hd' => 3,
        'sd' =>4,
        '4' => 5,
        '4k' => 5
    );//新片源替换逻辑，清晰度层次关系
    public $conf_info = array();//注入条件配置

    public function __construct($cp_config=null)
    {
        $this->db_obj = nl_get_dc(array (
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
        $this->cp_config = $cp_config;
        $this->debug_log = nns_debug_log_class::get_instance();
        $this->debug_log->setlogpath('import_asset_log');

        $this->media_import_module = $this->cp_config['media_replace_mode_one'];
        $this->media_import_comm = $this->cp_config['media_replace_mode_two'];
        $this->media_import_module_v2 = $this->cp_config['media_replace_mode_three'];
    }

    /**
     * 权重设置
     * @param unknown $int_weight
     */
    public function _set_weight($int_weight)
    {
        $int_weight = (int)$int_weight;
        $this->int_weight = $int_weight > 0 ? $int_weight : 0;
    }

    /**
     * 获取权重
     */
    public function _get_weight()
    {
        $int_weight = (int) $this->int_weight;
        return $int_weight >0 ? $int_weight : 0;
    }

    /**
     * 设置消息中的内容是否为分组消息
     * @param $int_weight
     */
    public function _set_is_group($is_group)
    {
        $is_group = (int) $is_group;
        $this->is_group = $is_group >0 ? $is_group : 0;
    }

    /**
     * 获取消息中内容是否分组消息
     * @return int
     */
    public function _get_is_group()
    {
        $is_group = (int) $this->is_group;
        return $is_group >0 ? $is_group : 0;
    }

    public function write_log($log, $method)
    {

        /*$this->debug_log->setlog(
         $log, $method
         );*/
        if (!is_dir(dirname(dirname(dirname(dirname(__FILE__)))) . '/data/log/mgtv/assets_import/'))
        {
            mkdir(dirname(dirname(dirname(dirname(__FILE__)))) . '/data/log/mgtv/assets_import/', 0777, true);
        }
        $handle = fopen(dirname(dirname(dirname(dirname(__FILE__)))) . '/data/log/mgtv/assets_import/' . date('Y-m-d') . '.txt', "a");
        fwrite($handle, "################################" . date("H:i:s", time()));

        fwrite($handle, $method . "   \n" . $log . "\r\n");
        fclose($handle);
    }

    public function asset_line($assets_id,$params,$exists_id)
    {
        if(!isset($params['asset_type']))
        {
            return array(
                "ret" => 1,
                "reason" => "上下线媒资类型未设置"
            );
        }
        $video_inst = new nns_db_vod_class();
        $video_inst->_set_weight($this->_get_weight());
        $video_inst->_set_is_group($this->_get_is_group());
        $video_inst->message_id = $this->message_id;
        //增加上下线消息可能从其他CP接收到的
        if(!isset($params['bk_cp_id']) || empty($params['bk_cp_id']))
        {
            $params['bk_cp_id'] = $params['cp_id'];
        }
        return $video_inst->nns_db_asset_line($exists_id,$params['action'],$assets_id,$params['cp_id'],$params['category_id'],$params['order'],$params['asset_type'],$params['bk_cp_id'],$params['asset_id']);
    }


    /**
     * 点播影片信息添加、修改
     * @param String 注入来源ID
     * @param array(
     * "assets_category"="分类名"
    svc_item_id=”定价ID” ××××××××××××××基础函数未存
    video_type=”1为直播  0为点播”  *默认为0
    name =”媒资名称”   *必要参数
    sreach_key=” 国医养生馆 青海卫视”  （可以搜索字段)
    view_type =“0为电影 1为连续剧” *默认为0
    director =“导演,导演,….” (可以搜索字段)
    player =“演员,演员,….” (可以搜索字段)
    tag=“情感,动作,……”标签   (可以搜索字段)
    region=“日韩,欧美”地区     (可以搜索字段)
    release_time =“上映时间”    (可以搜索排序字段)
    totalnum =“总集数”  *默认为1
    national =“国别”   (可以搜索字段) ××××××××××××××基础函数未存
    smallpic=”小海报”  ××××××××××××××基础函数未存
    intro =“剧情简介”
    producer =“供片商”  (可以搜索字段)
    adaptor=“编剧”
    point="推荐星级"
    play_role =“角色扮演”  （芒果暂无）
    subordinate_name =“别名”
    english_name =“英文名”
    language =“视频语种”
    caption_language =“字幕语种,字幕语种,”
    initials=“影片名首字母”   (可以搜索字段) ××××××××××××××基础函数未存
    view_len="影片时长（秒）"（芒果暂无）
    copyright_range =“版权期限”
    total_clicks=“总点击” (可以排序字段)  ××××××××××××××基础函数未存
    month_clicks=“月点击”(可以排序字段)  ××××××××××××××基础函数未存
    week_clicks=“周点击”(可以排序字段)  ××××××××××××××基础函数未存
    day_clicks=“日点击”  ××××××××××××××基础函数未存
    copyright=“版权信息”

     *     )
     * @param String 父栏目ID
     * @return video_id 注入后的video_id
     * 			FALSE 注入失败
     */
    public function update_asset($asset_item_id, $params, $parent_category_id = '10000')
    {
        $str_import_source = (isset($params['import_source']) && strlen($params['import_source'])>0) ? $params['import_source'] : IMPORT_SOURCE;
        //首先添加媒资入库
        $video_inst = new nns_db_vod_class();
        //贵州cp权重优先级
        $dc = nl_get_dc(array(
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
        $cp_weight=nl_producer::get_weight($dc,$params["asset_source"],$params["producer"]);
        if($cp_weight['ret']==0 && is_array($cp_weight['data_info']))
        {
            $video_inst->_set_weight($cp_weight['data_info']['weight']);
        }
        else
        {
            if(isset($params['priority']) && !empty($params['priority']))
            {
                $video_inst->_set_weight($params['priority']);
            }
            else
            {
                $video_inst->_set_weight($this->_get_weight());
            }
        }
        $video_inst->message_id = $this->message_id;
        $video_inst->conf_info = $this->conf_info;
        $exists_id = $video_inst->get_video_id_by_import_id($str_import_source, $asset_item_id,$params['asset_source']);


        $params['assets_category'] = trim($params['assets_category']);

        if(strlen($params['assets_category'])<1)
        {
            return false;
        }
        //     获取主媒资的主分类标识
        //     当未传递view_type时根据assets_category一级栏目计算一个
        $params['view_type'] = $this->get_import_vod_view_type($params);

        if(isset($params['bk_cate_id']) && strlen($params['bk_cate_id']) >0 && isset($params['bk_depot_id']) && strlen($params['bk_depot_id']) >0  && isset($params['bk_org_id']) && strlen($params['bk_org_id']) >0 )
        {
            $depot_info = array(
                'id'=>$params['bk_depot_id'],
                'category'=>$params['bk_cate_id'],
                'org_id'=>$params['bk_org_id']
            );
        }
        else
        {
            //引入v2新版
            include_once dirname(dirname(dirname(dirname(__FILE__)))). "/v2/common.php";
            \ns_core\m_load::load("ns_core.m_config");
            global $g_bk_version_number;
            $category_v2 = get_config_v2('g_category_v2');
            $bk_version_number = ($g_bk_version_number == '1' && $category_v2 == 1)? true : false;
            unset($g_bk_version_number);
            if ($bk_version_number)
            {
                \ns_core\m_load::load("ns_data_model.category.m_category_v2_inout");
                $do_category = array(
                    'base_info' => array(
                        'nns_name' => $params['assets_category'],   //栏目名称支持多层目录，多层目录的时候 /分割
                        'nns_import_category_id' => '', //栏目名称支持多层目录，多层目录的时候 /分割
                        'nns_cp_id' => $params['asset_source'],
                        'nns_import_parent_category_id' => '',
                        'nns_video_type' => '0',  //媒资类型  0 点播 | 1 直播
                    ),
                );
                $obj_excute = new \m_category_v2_inout();
                $result = $obj_excute->add($do_category);
                if ($result['ret'] != NS_CDS_SUCCE || !is_array($result['data_info']['base_info']) || empty($result['data_info']['base_info']))
                {
                    return false;
                }
                $depot_info = array(
                    'id' => $result['data_info']['base_info']['nns_depot_id'],
                    'category' => $result['data_info']['base_info']['nns_id'],
                    'org_id' => $result['data_info']['base_info']['nns_org_id'],
                    'name' => $result['data_info']['base_info']['nns_name'],
                );
            }
            else
            {
                if(isset($params['parent_assets_category_id']) && strlen($params['parent_assets_category_id']) > 1)
                {
                    $depot_info_parent = $this->set_depot_category($params['parent_assets_category'], 0, substr($params['parent_assets_category_id'], 0,-3));
                    $parent_category_id = $depot_info_parent['category'];
                }
                $depot_info = $this->set_depot_category($params['assets_category'], 0, $parent_category_id);
            }

        }
        if (strlen($params["initials"]) == 0)
        {
            $pinyin = nns_controls_pinyin_class::get_pinyin_letter($params['name']);
            $pinyin_length = nns_controls_pinyin_class::$length;
        }
        elseif(strstr($params["initials"],'|'))
        {
            $simplespell = explode('|', $params["initials"]);
            $pinyin = $simplespell[0];
            $pinyin_length = strlen($pinyin);
        }
        else
        {
            $pinyin = $params["initials"];
            $pinyin_length = strlen($pinyin);
        }

        if ($exists_id === FALSE)
        {
            $result = $video_inst->nns_db_vod_add($this->change_spcial_word($params['name']), $params['view_type'], $depot_info['id'], $depot_info['category'], 0, $depot_info['org_id'], NULL, array(
                'director' => $this->change_spcial_word($params['director']),
                'actor' => $this->change_spcial_word($params['player']),
                'ishuaxu' => intval($params['ishuaxu']),
                //'positiveoriginalid' => $this->change_spcial_word($params['positiveoriginalid']),
                'show_time' => $params['release_time'],
                'view_len' => intval($params['view_len']),
                'all_index' => intval($params['totalnum']),
                'area' => $this->change_spcial_word($params['region']),
                'eng_name' => $this->change_spcial_word($params['english_name']),
                'screenwriter' => $this->change_spcial_word($params['adaptor']),
                'alias_name' => $this->change_spcial_word($params['subordinate_name']),
                'producer' => $this->change_spcial_word($params['producer']),
                'language' => $this->change_spcial_word($params['language']),
                'nns_keyword' => $this->change_spcial_word($params['keywords']),
                'kind' => $this->change_spcial_word($params['tag']),
                'play_role' => $this->change_spcial_word($params['play_role']),
                'text_lang' => $this->change_spcial_word($params['caption_language']),
                'copyright' => $this->change_spcial_word($params['copyright']),
                'copyright_range' => $params['copyright_range'],
                'copyright_begin_date' => !empty($params['copyright_begin_date']) ? $params['copyright_begin_date'] : date('Y-m-d H:i:s'),
                'score_total' => floatval($params['score_total']),
                'nns_play_count' => intval($params['play_count']),
            ), $this->change_spcial_word($params['intro']), $params['point'], $params['copyright_range'], NULL, $asset_item_id, $pinyin, g_cms_config::get_g_config_value('g_video_audit_enabled'), $str_import_source,$pinyin_length,$params['asset_source']);
            if ($result['ret'] == 0)
            {
                $new_video_id = $result['data']['id'];
            }
            else
            {
                $new_video_id = FALSE;
            }
        }
        else
        {
            $result = $video_inst->nns_db_vod_modify($exists_id, $this->change_spcial_word($params['name']), $params['view_type'], $depot_info['id'], $depot_info['category'], 0, $depot_info['org_id'], NULL, array(
                'director' => $this->change_spcial_word($params['director']),
                'actor' => $this->change_spcial_word($params['player']),
                'ishuaxu' => intval($params['ishuaxu']),
                //'positiveoriginalid' => $this->change_spcial_word($params['positiveoriginalid']),
                'show_time' => $params['release_time'],
                'view_len' => intval($params['view_len']),
                'all_index' => intval($params['totalnum']),
                'area' => $this->change_spcial_word($params['region']),
                'eng_name' => $this->change_spcial_word($params['english_name']),
                'screenwriter' => $this->change_spcial_word($params['adaptor']),
                'alias_name' => $this->change_spcial_word($params['subordinate_name']),
                'producer' => $this->change_spcial_word($params['producer']),
                'language' => $this->change_spcial_word($params['language']),
                'nns_keyword' => $this->change_spcial_word($params['keywords']),
                'kind' => $this->change_spcial_word($params['tag']),
                'play_role' => $this->change_spcial_word($params['play_role']),
                'text_lang' => $this->change_spcial_word($params['caption_language']),
                'copyright' => $this->change_spcial_word($params['copyright']),
                'copyright_range' => $params['copyright_range'],
                'copyright_begin_date' => !empty($params['copyright_begin_date']) ? $params['copyright_begin_date'] : date('Y-m-d H:i:s'),
                'score_total' => floatval($params['score_total']),
                'nns_play_count' => intval($params['play_count']),
            ), $this->change_spcial_word($params['intro']), $params['point'], $params['copyright_range'], NULL, $pinyin,$pinyin_length,$params['asset_source']);
            if ($result['ret'] == 0)
            {
                $dc = nl_get_dc(array(
                    'db_policy' => NL_DB_WRITE,
                    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
                ));
                $dc->open();
                nl_vod::delete_vod_info_by_cache($dc, $exists_id);
                $new_video_id = $exists_id;
            }
            else
            {
                //			错误日志
                $this->write_log('ret:' . $result['ret'] . "\n" . 'asset_id:' . $asset_item_id . "\n" . 'params:' . json_encode($params) . "\n", 'update_asset');
                //$exists_id = $video_inst->get_video_id_by_import_id(IMPORT_SOURCE, $asset_item_id);
                $new_video_id = FALSE;
            }
        }
        return $new_video_id;
    }

    public function sync_category_cdn($arr_category)
    {
        $str_parent_id = substr($arr_category['category'], 0,-3);
        $arr_category['parent_category'] = (strlen($str_parent_id)<5 || $str_parent_id == '10000') ? 0 : $str_parent_id;
        $dc = nl_get_dc(array(
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
        $dc->open();
        include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/sp/sp.class.php';
        include_once dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv_v2/models/child_models/c2_task_assign.class.php';
        include_once dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv_v2/models/child_models/c2_task_model_standard.class.php';
        $result_sp = nl_sp::query_all($dc);
        if($result_sp['ret'] !=0)
        {
            return $result_sp;
        }
        if(!isset($result_sp['data_info']) || !is_array($result_sp['data_info']) || empty($result_sp['data_info']))
        {
            return $result_sp;
        }
        foreach ($result_sp['data_info'] as $sp_val)
        {
            $sp_val['nns_config'] = (isset($sp_val['nns_config']) && strlen($sp_val['nns_config']) >0) ? json_decode($sp_val['nns_config'],true) : null;
            if((!isset($sp_val['nns_config']['op_queue_video_enabled']) || empty($sp_val['nns_config']['op_queue_video_enabled'])
                    || (is_array($sp_val['nns_config']['op_queue_video_enabled']) && in_array('category', $sp_val['nns_config']['op_queue_video_enabled'])))
                && isset($sp_val['nns_config']['c2_import_cdn_model']) && $sp_val['nns_config']['c2_import_cdn_model'] == '4')
            {
                $obj_c2_task = new c2_task_model_v2($dc,$sp_val['nns_id'],$sp_val['nns_config'],$arr_category);
                if(!method_exists($obj_c2_task,'do_category'))
                {
                    return $this->_make_return_data(1,'C2方法类方法不存在,方法:[do_category]文件路径:');
                }
                $obj_c2_task->do_category();
                unset($obj_c2_task);
            }
        }
    }




    /**
     * @param string 平台媒资ID
     * @param EPG，tag串
     * @return FALSE TRUE
     */
    public function update_video_epg_tag($video_id, $tag)
    {
        $dc = nl_get_dc(array(
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
        $dc->open();
        $tag = rtrim($tag, ',');
        $tag .= ',';
        $sql = 'update nns_vod set nns_tag=\'' . $tag . '\' where nns_id=\'' . $video_id . '\'';

        $result = nl_execute_by_db($sql, $dc->db());

        if ($result === FALSE)
            return FALSE;

        $sql = 'update nns_assists_item set nns_tag=\'' . $tag . '\' where nns_video_id=\'' . $video_id . '\'';

        $result = nl_execute_by_db($sql, $dc->db());

        if ($result === FALSE)
            return FALSE;

        //			若成功 清缓存
        // $vod_inst = new nns_db_vod_class();
        // $vod_inst->clear_memcache_by_asset_service($video_id);

        return $result;
    }

    //若存在assets_category一级影片分类，则将影片注入到对应的资源库栏目，
    //若不存在则直接注入到根栏目
    public function get_default_depot_info($assets_category, $depot_type)
    {
        $depot_inst = new nns_db_depot_class();
        $depot_result = $depot_inst->nns_db_depot_info(0, NULL, $depot_type);
        $org_id = $depot_result['data'][0]['nns_org_id'];
        $depot_id = $depot_result['data'][0]['nns_id'];
        if (empty($assets_category))
        {
            $category_id = '10000';
        }
        else
        {
            $depot_info = $depot_result['data'][0]['nns_category'];
            $dom = new DOMDocument('1.0', 'utf-8');
            $dom->loadXML($depot_info);
            $categorys = $dom->getElementsByTagName('category');
            foreach ($categorys as $category)
            {
                if ($category->getAttribute('name') === $assets_category)
                {
                    $category_id = $category->getAttribute('id');
                    break;
                }
            }
        }

        return array(
            'id' => $depot_id,
            'category_id' => $category_id,
            'org_id' => $org_id
        );
    }

    /**
     * 点播影片删除
     * @param array(
     * 	'id'=>asset_id 对方平台注入ID
     * )
     * @param 是否删除底层片源ID
     * @return TRUE 删除成功
     * 		FALSE 删除失败
     *          -1 没有
     */
    public function delete_asset($params, $delete_core = TRUE,$nns_cp_id=null)
    {

        if (empty($params['id']))
            return FALSE;
        //      获取CMS平台ID
        $vod_inst = new nns_db_vod_class();
        $vod_inst->message_id = $this->message_id;
        //贵州cp权重优先级
        //查询主媒资内容提供商
        $dc = nl_get_dc(array(
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
        $vod_result=nl_vod::query_by_id($dc,$params['id']);
        $cp_weight=nl_producer::get_weight($dc,$vod_result['data_info']['nns_cp_id'],$vod_result['data_info']["nns_producer"]);
        if($cp_weight['ret']==0 && is_array($cp_weight['data_info']))
        {
            $vod_inst->_set_weight($cp_weight['data_info']['weight']);
        }
        else
        {
            $vod_inst->_set_weight($this->_get_weight());
        }
        $vod_inst->_set_is_group($this->_get_is_group());
        $nns_cp_id = (strlen($nns_cp_id) > 0) ? $nns_cp_id : 0;
        $nns_id = $vod_inst->get_video_id_by_import_id(IMPORT_SOURCE, $params['id'],$nns_cp_id);
        if ($nns_id !== FALSE)
        {
            //		下线影片
            // $asset_inst = new nns_db_assist_item_class();
            //$service_inst = new nns_db_service_category_class();
            // $result = $asset_inst->nns_db_assist_item_unline($nns_id);
            //$result = $service_inst->nns_db_service_category_unline($nns_id);

            $asset_inst = null;
            $service_inst = null;
            $result = $vod_inst->nns_db_vod_real_delete($nns_id, $delete_core);
            $return = FALSE;
            if ($result["ret"] == 0)
            {
                $return = TRUE;
                //			若成功 清缓存
                //$vod_inst->clear_memcache_by_asset_service($nns_id);
                //福建移动
                //$g_inject = g_cms_config::get_g_config_value('g_inject_enabled');
                //if($g_inject){
                //	$category_arr = category_model::delete_category_content($nns_id,'vod');
                //}
            }
            else
            {
                if ($result['ret'] == NNS_ERROR_VOD_NOT_FOUND)
                    $return = TRUE;

                //			错误日志
                $this->write_log('ret:' . $result['ret'] . "\n" . 'import_id:' . $params['id'] . "\n" . 'nns_id:' . $nns_id . "\n", 'delete_asset');

            }
        }
        else
        {
            $return = true;
        }

        return $return;
    }

    /**
     * 直播频道删除
     */
    public function delete_live($params, $delete_core = TRUE,$nns_cp_id=null)
    {
        if(empty($params['id']))
        {
            return false;
        }

        $live_inst = new nns_db_live_class();
        $nns_cp_id = (strlen($nns_cp_id) > 0) ? $nns_cp_id : 0;
        $nns_id = $live_inst->get_live_id_by_import_id($params['id'],$nns_cp_id);
        if($nns_id !== false)
        {
            $result = $live_inst->nns_db_live_delete($nns_id);

            $result_media = nl_live_media::query_by_channel_id($this->db_obj, $nns_id);
            if(isset($result_media['data_info']) && is_array($result_media['data_info']) && !empty($result_media['data_info']))
            {
                foreach ($result_media['data_info'] as $media_info)
                {
                    $this->_get_cp_config($media_info['nns_cp_id']);
                    if(isset($this->cp_config['message_live_media_import_enable']) && $this->cp_config['message_live_media_import_enable']  == '1')
                    {
                        $queue_task_model = new queue_task_model();
                        $queue_task_model->_set_weight($this->_get_weight());
                        $queue_task_model->_set_is_group($this->_get_is_group());
                        $queue_task_model->q_add_task_op_mgtv($media_info['nns_id'], BK_OP_LIVE_MEDIA, BK_OP_DELETE,$this->message_id);
                        unset($queue_task_model);
                    }
                }
            }
            if($result['ret'] == '0')
            {
                $return = true;
            }
            else
            {
                if ($result['ret'] == NNS_ERROR_VOD_NOT_FOUND)
                    $return = TRUE;
                //			错误日志
                $this->write_log('ret:' . $result['ret'] . "\n" . 'import_id:' . $params['id'] . "\n" . 'nns_id:' . $nns_id . "\n", 'delete_live');
            }
        }
        else
        {
            $return = true;
        }
        return $return;
    }

    /**
     * 更新或者新增频道
     */
    public function update_live($param,$parent_category_id = '10000')
    {
        //引入v2新版
        include_once dirname(dirname(dirname(dirname(__FILE__)))). "/v2/common.php";
        \ns_core\m_load::load("ns_core.m_config");
        global $g_bk_version_number;
        $category_v2 = get_config_v2('g_category_v2');
        $bk_version_number = ($g_bk_version_number == '1' && $category_v2 == 1)? true : false;
        unset($g_bk_version_number);

        //根据注入id和cp查询直播频道是否存在
        $live_inst = new nns_db_live_class();
        $nns_id = $live_inst->get_live_id_by_import_id($param['channel_id'],$param['live_source'],$param['import_source']);
        if($nns_id)
        {//存在修改
            //新增之前，添加栏目

            if ($bk_version_number)
            {
                \ns_core\m_load::load("ns_data_model.category.m_category_v2_inout");
                $do_category = array(
                    'base_info' => array(
                        'nns_name' => $param['live_video_info']['live_category'],   //栏目名称支持多层目录，多层目录的时候 /分割
                        'nns_import_category_id' => '', //栏目名称支持多层目录，多层目录的时候 /分割
                        'nns_cp_id' => $param['live_source'],
                        'nns_import_parent_category_id' => '',
                        'nns_video_type' => '1',  //媒资类型  0 点播 | 1 直播
                    ),
                );
                $obj_excute = new \m_category_v2_inout();
                $result = $obj_excute->add($do_category);
                if ($result['ret'] != NS_CDS_SUCCE || !is_array($result['data_info']['base_info']) || empty($result['data_info']['base_info']))
                {
                    return false;
                }
                $depot_info = array(
                    'id' => $result['data_info']['base_info']['nns_depot_id'],
                    'category' => $result['data_info']['base_info']['nns_id'],
                    'org_id' => $result['data_info']['base_info']['nns_org_id'],
                    'name' => $result['data_info']['base_info']['nns_name'],
                );
            }
            else
            {
                if(isset($param['parent_assets_category_id']) && strlen($param['parent_assets_category_id']) > 1)
                {
                    $depot_info_parent = $this->set_depot_category($param['parent_assets_category'], 0, substr($param['parent_assets_category_id'], 0,-3));
                    $parent_category_id = $depot_info_parent['category'];
                }
                $depot_info = $this->set_depot_category($param['live_video_info']['live_category'],1,$parent_category_id);

            }

            //拼音
            $pinyin = nns_controls_pinyin_class::get_pinyin_letter($param['live_video_info']['name']);
            $pinyin_length = nns_controls_pinyin_class::$length;
            $modify_live = $live_inst->nns_db_live_modify($nns_id,$param['live_video_info']['name'],1,$depot_info['id'],$depot_info['category'],0,$depot_info['org_id'],null,array(
                'cp_id'=>$param['live_source'],
                'import_id'=>$param['channel_id'],
                'import_source'=>$param['live_video_info']['import_source'],
                'producer'=>$param['live_video_info']['producer'],
                'all_index'=>$param['live_video_info']['all_index'],
            ),$this->change_spcial_word($param['live_video_info']['info']),$param['live_video_info']['point'],'',$param['live_video_info']['remark'],$pinyin,
                $param['live_source'],$param['channel_id'],$pinyin_length);

            // 频道信息更新完成，更新频道扩展信息
            $arr_live_ex_result = array('ret' => 0, 'reason' => 'ok');
            if (is_array($param['live_video_ex_info']) && !empty($param['live_video_ex_info']))
            {
                $obj_live_ex = new nns_db_live_ex_class();
                foreach ($param['live_video_ex_info'] as $str_key => $str_value)
                {
                    $arr_live_ex_result = $obj_live_ex->nns_db_live_set_value($nns_id, $str_key, $str_value);
                    if ($arr_live_ex_result['ret'] != '0')
                    {
                        unset($obj_live_ex);
                        break;
                    }
                }
                unset($obj_live_ex);
            }

            //修改队列
            $live_cdn_inst = new nl_cdn_live_queue();
            $param_cdn = array(
                'nns_live_id'=>$nns_id,
                'nns_action'=>$param['action'],
                'nns_message_id'=>$param['message_id'],
                'nns_cp_id'=>$param['live_source'],
            );
            $live_cdn_live =$live_cdn_inst->add($this->db_obj,$param_cdn);
            if($modify_live['ret'] == '0' && $live_cdn_live['ret'] == '0' && $arr_live_ex_result['ret'] == '0')
            {
                $modify_live['data']['id']=$nns_id;
                return $modify_live;
            }
            else
            {
                return array(
                    'ret' => '1',
                    'reason'=>'数据库执行失败',
                    'data'=>'',
                );
            }
        }
        else
        {//不存在新增
            //新增之前，添加栏目

            if ($bk_version_number)
            {
                \ns_core\m_load::load("ns_data_model.category.m_category_v2_inout");
                $do_category = array(
                    'base_info' => array(
                        'nns_name' => $param['live_video_info']['live_category'],   //栏目名称支持多层目录，多层目录的时候 /分割
                        'nns_import_category_id' => '', //栏目名称支持多层目录，多层目录的时候 /分割
                        'nns_cp_id' => $param['live_source'],
                        'nns_import_parent_category_id' => '',
                        'nns_video_type' => '1',  //媒资类型  0 点播 | 1 直播
                    ),
                );
                $obj_excute = new \m_category_v2_inout();
                $result = $obj_excute->add($do_category);
                if ($result['ret'] != NS_CDS_SUCCE || !is_array($result['data_info']['base_info']) || empty($result['data_info']['base_info']))
                {
                    return false;
                }
                $depot_info = array(
                    'id' => $result['data_info']['base_info']['nns_depot_id'],
                    'category' => $result['data_info']['base_info']['nns_id'],
                    'org_id' => $result['data_info']['base_info']['nns_org_id'],
                    'name' => $result['data_info']['base_info']['nns_name'],
                );
            }
            else
            {
                if(isset($param['parent_assets_category_id']) && strlen($param['parent_assets_category_id']) > 1)
                {
                    $depot_info_parent = $this->set_depot_category($param['parent_assets_category'], 0, substr($param['parent_assets_category_id'], 0,-3));
                    $parent_category_id = $depot_info_parent['category'];
                }
                $depot_info = $this->set_depot_category($param['live_video_info']['live_category'],1,$parent_category_id);
            }

            //拼音
            $pinyin = nns_controls_pinyin_class::get_pinyin_letter($param['live_video_info']['name']);
            $pinyin_length = nns_controls_pinyin_class::$length;

            //准备添加频道的数据
            $live_add_result = $live_inst->nns_db_live_add($param['live_video_info']['name'],1,$depot_info['id'],$depot_info['category'],0,$depot_info['org_id'],null,array(
                'cp_id'=>$param['live_source'],
                'import_id'=>$param['channel_id'],
                'import_source'=>$param['live_video_info']['import_source'],
                'producer'=>$param['live_video_info']['producer'],
                'all_index'=>$param['live_video_info']['all_index'],
            ),$this->change_spcial_word($param['live_video_info']['info']),$param['live_video_info']['point'],'',$param['live_video_info']['remark'],$pinyin,$param['live_video_info']['state'],
                $param['live_source'],$param['channel_id'],$pinyin_length);

            // 频道信息更新完成，更新频道扩展信息
            $arr_live_ex_result = array('ret' => 0, 'reason' => 'ok');
            if (is_array($param['live_video_ex_info']) && !empty($param['live_video_ex_info']))
            {
                $obj_live_ex = new nns_db_live_ex_class();
                foreach ($param['live_video_ex_info'] as $str_key => $str_value)
                {
                    $arr_live_ex_result = $obj_live_ex->nns_db_live_set_value($live_add_result['data']['id'], $str_key, $str_value);
                    if ($arr_live_ex_result['ret'] != '0')
                    {
                        unset($obj_live_ex);
                        break;
                    }
                }
                unset($obj_live_ex);
            }

            //修改队列
            $live_cdn_inst = new nl_cdn_live_queue();
            $param_cdn = array(
                'nns_live_id'=>$live_add_result['data']['id'],
                'nns_action'=>$param['action'],
                'nns_message_id'=>$param['message_id'],
                'nns_cp_id'=>$param['live_source'],
            );
            $live_cdn_live =$live_cdn_inst->add($this->db_obj,$param_cdn);
            if($live_add_result['ret'] == '0' && $live_cdn_live['ret'] == '0' && $arr_live_ex_result['ret'] == '0')
            {
                return $live_add_result;
            }
            else
            {
                return array(
                    'ret' => '1',
                    'reason'=>'数据库执行失败',
                    'data'=>'',
                );
            }
        }
    }

    /**
     * 点播分片信息添加、修改
     * @param String 对应影片注入ID
     * @param String 来源分片id clip_id
     * @param Array(
    index=”分片序号 0为第1集”  *必要参数
    time_len=“分片长度(秒)”
    name="分片名"
    summary=”分片剧情简介”
    pic=“分片海报”××××××××××××××基础函数未存
    director =“导演,导演,….” (可以搜索字段)
    player =“演员,演员,….” (可以搜索字段)
    total_clicks=“总点击” ××××××××××××××基础函数未存
    month_clicks=“月点击” ××××××××××××××基础函数未存
    week_clicks=“周点击” ××××××××××××××基础函数未存
    day_clicks=“日点击” ××××××××××××××基础函数未存
    update_time=“更新时间” ××××××××××××××基础函数未存
    release_time="上映时间" ××××××××××××××基础函数未存
    )
     * @return String 注入成功后index_id
     * 			FALSE 注入失败
     */
    public function update_clip($asset_id, $clip_id, $params,$params_seekpoint=null,$conf_info=null)
    {
        //echo '<pre>';print_r($params);die();
        //检查是否影片注入ID已存在
        $dc = nl_get_dc(array(
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
        $dc->open();
        $str_import_source = (isset($params['import_source']) && strlen($params['import_source']) >0) ? $params['import_source'] : IMPORT_SOURCE;
        $params['asset_source'] = (!isset($params['asset_source']) || strlen($params['asset_source']) < 1) ? '0' : $params['asset_source'];
        $video_inst = new nns_db_vod_class();
        $exists_id = $video_inst->get_video_id_by_import_id($str_import_source, $asset_id,$params['asset_source']);
        if ($exists_id === FALSE)
        {
            $this->import_desc =array(
                'ret'=>6,
                'reason'=>'asset id is not exist',
            );
            return FALSE;
        }
        $index = (int)$params['index'];
        //$video_index = new nns_db_vod_index_class();
        $vod_index_inst = new nns_db_vod_index_class();
        //贵州cp权重优先级
        //查询主媒资内容提供商
        $vod_result=nl_vod::query_by_id($dc,$exists_id);
        $cp_weight=nl_producer::get_weight($dc,$vod_result['data_info']['nns_cp_id'],$vod_result['data_info']["nns_producer"]);
        if($cp_weight['ret']==0 && is_array($cp_weight['data_info']))
        {
            $vod_index_inst->_set_weight($cp_weight['data_info']['weight']);
        }
        else
        {
            if(isset($params['priority']) && !empty($params['priority']))
            {
                $vod_index_inst->_set_weight($params['priority']);
            }
            else
            {
                $vod_index_inst->_set_weight($this->_get_weight());
            }
        }
        $vod_index_inst->_set_is_group($this->_get_is_group());
        $vod_index_inst->cp_config = $this->cp_config;
        $this->conf_info = $conf_info;
        $vod_index_inst->conf_info = $this->conf_info;
        $vod_index_inst->message_id = $this->message_id;

        $exists_is = $vod_index_inst->get_video_index_id_by_import_id($str_import_source, $clip_id, null, false,false,$params['asset_source'],1);

        //$vod_index_inst = new nns_db_vod_index_class();
        $vod_media_inst = new nns_db_vod_media_class();
        $queue_task_model = new queue_task_model();
        $index = (int)$params['index'];
        $release_time = strtotime($params['release_time']);
        $release_time = date('Y-m-d H:i:s', $release_time);
        //print_r($params);die;
        //分集修改

        if (is_array($exists_is))
        {
            //查看这个分集的主媒资id是否是和查询出来的id一直,如果一致,则看
            if ($exists_id == $exists_is['nns_vod_id'])
            {
                if ($exists_is['nns_index'] != $index)
                {
                    //判断是否开启分集替换规则
                    if($this->cp_config['clip_replace_mode_one'] == '1')
                    {
                        //			错误日志
                        $this->write_log("分集替换被禁止 \n" . 'asset_id:' . $asset_id . "\n" . 'clip_id:' . $clip_id . "\n" . 'params:' . json_encode($params) . "\n", 'update_clip');
                        return array('ret'=>'14' , 'reason'=>'分集替换被禁止');
                    }
                    else
                    {
                        //如果分集序号不相同,则先要删除原来的分集和片源信息,再添加新的信息
                        $video_index_ary = nl_db_get_one("select * from nns_vod_index where nns_id='{$exists_is['nns_id']}' and nns_deleted!=1", $dc->db());
                        $sql_media = "select * from nns_vod_media where  nns_vod_index_id='{$video_index_ary['nns_id']}' and nns_deleted!=1";
                        $media_ary = nl_db_get_all($sql_media, $dc->db());
                        $data = $video_index_ary;
                        $data['nns_id'] = np_guid_rand();
                        $data['nns_vod_id'] = $exists_id;
                        $data['nns_index'] = $index;
                        $data['nns_release_time'] = $release_time;
                        $data['nns_image'] = $params['pic'];
                        $data['nns_update_time'] = $params['update_time'];
                        $data['nns_status'] = 1;
                        $data['nns_action'] = 'add';
                        $data['nns_name'] = $params['name'];
                        $data['nns_time_len'] = $params['time_len'];
                        $data['nns_modify_time'] = date('Y-m-d H:i:s', time());
                        $data['nns_cp_id'] = (isset($params['asset_source']) && strlen($params['asset_source']) >0) ? $params['asset_source'] : '0';
                        $data['nns_summary'] = $this->change_spcial_word($params['summary']);
                        $data['nns_import_source'] = $str_import_source;
                        unset($data['nns_integer_id']);
                        nl_db_insert($dc->db(), 'nns_vod_index', $data);
                        $vod_index_inst->nns_db_vod_index_delete($exists_is['nns_id']);

                        $queue_task_model->_set_weight($vod_index_inst->_get_weight());
                        //$queue_task_model->q_add_task_op_mgtv($data['nns_id'], BK_OP_INDEX, BK_OP_ADD,$this->message_id,true,$this->conf_info);
                        $queue_task_model->q_add_task_op_mgtv($data['nns_id'], BK_OP_INDEX, BK_OP_ADD,$this->message_id,true);

                        if (is_array($media_ary))
                        {
                            foreach ($media_ary as $media_key => $media_val)
                            {
                                $data_media = $media_val;
                                $data_media['nns_id'] = np_guid_rand();
                                $data_media['nns_vod_id'] = $exists_id;
                                $data_media['nns_vod_index_id'] = $data['nns_id'];
                                $data_media['nns_vod_index'] = $index;
                                $data_media['nns_modify_time'] = date('Y-m-d H:i:s', time());
                                $data_media['nns_status'] = 1;
                                $data_media['nns_action'] = 'add';
                                $data_media['nns_modify_time'] = date('Y-m-d H:i:s', time());
                                $data_media['nns_import_source'] = $str_import_source;
                                $data_media['nns_cp_id'] = empty($params['asset_source']) ? '0' : $params['asset_source'];
                                unset($data_media['nns_integer_id']);
                                nl_db_insert($dc->db(), 'nns_vod_media', $data_media);
                                //$queue_task_model->q_add_task_op_mgtv($data_media['nns_id'], BK_OP_MEDIA, BK_OP_ADD,$this->message_id,true,$this->conf_info);
                                $queue_task_model->q_add_task_op_mgtv($data_media['nns_id'], BK_OP_MEDIA, BK_OP_ADD,$this->message_id,true,array('import_cdn_mode' => $media_val['nns_media_service']));
                                //分集中已有片源删除
                                //$vod_media_inst->nns_db_vod_media_delete($media_val['nns_id']);
                            }
                        }
                    }
                }
                elseif ($exists_is['nns_index'] == $index)
                {
                    $video_index_ary = nl_db_get_one("select * from nns_vod_index where nns_id='{$exists_is['nns_id']}' and nns_deleted!=1", $dc->db());
                    $data = $video_index_ary;
                    //$data['nns_id'] = np_guid_rand();
                    $data['nns_vod_id'] = $exists_id;
                    $data['nns_index'] = $index;
                    $data['nns_release_time'] = $release_time;
                    $data['nns_image'] = $params['pic'];
                    $data['nns_update_time'] = $params['update_time'];
                    $data['nns_status'] = 1;
                    $data['nns_action'] = 'modify';
                    $data['nns_name'] = $params['name'];
                    $data['nns_modify_time'] = date('Y-m-d H:i:s', time());
                    $data['nns_cp_id'] = (isset($params['asset_source']) && strlen($params['asset_source']) >0) ? $params['asset_source'] : '0';
                    $data['nns_summary'] = $this->change_spcial_word($params['summary']);
                    $data['nns_time_len'] = $params['time_len'];
                    $where = "nns_id='{$exists_is['nns_id']}'";
                    nl_db_update($dc->db(), 'nns_vod_index', $data, $where);
                    $queue_task_model->_set_weight($vod_index_inst->_get_weight());
                    $queue_task_model->q_add_task_op_mgtv($data['nns_id'], BK_OP_INDEX, BK_OP_MODIFY,$this->message_id,true,$this->conf_info);
                }
            }
            elseif ($exists_id != $exists_is['nns_vod_id'])
            {
                //echo __LINE__;
                //如果不是相同的主媒资id
                //第一步 需要

                $old_index_ary = nl_db_get_one("select * from nns_vod_index where nns_deleted!=1 and nns_id='{$exists_is['nns_id']}'", $dc->db());
                $odl_media_ary = nl_db_get_all("select * from nns_vod_media where nns_deleted!=1 and nns_vod_id='{$exists_is['nns_vod_id']}' and nns_vod_index_id='{$exists_is['nns_id']}'", $dc->db());
                $vod_index_inst->nns_db_vod_index_delete($exists_is['nns_id']);
                $data = $old_index_ary;
                $data['nns_id'] = np_guid_rand();
                $data['nns_vod_id'] = $exists_id;
                $data['nns_index'] = $index;
                $data['nns_release_time'] = $release_time;
                $data['nns_image'] = $params['pic'];
                $data['nns_update_time'] = $params['update_time'];
                $data['nns_status'] = 1;
                $data['nns_action'] = 'add';
                $data['nns_name'] = $params['name'];
                $data['nns_modify_time'] = date('Y-m-d H:i:s', time());
                $data['nns_time_len'] = $params['time_len'];
                $data['nns_cp_id'] = (isset($params['asset_source']) && strlen($params['asset_source']) >0) ? $params['asset_source'] : '0';
                $data['nns_summary'] = $this->change_spcial_word($params['summary']);
                $data['nns_import_source'] = $str_import_source;
                unset($data['nns_integer_id']);
                nl_db_insert($dc->db(), 'nns_vod_index', $data);
                $queue_task_model->_set_weight($vod_index_inst->_get_weight());
                //$queue_task_model->q_add_task_op_mgtv($data['nns_id'], BK_OP_INDEX, BK_OP_ADD,$this->message_id,true,$this->conf_info);
                $queue_task_model->q_add_task_op_mgtv($data['nns_id'], BK_OP_INDEX, BK_OP_ADD,$this->message_id,true);
                if (is_array($odl_media_ary))
                {
                    foreach ($odl_media_ary as $media_key => $media_val)
                    {
                        $data_media = $media_val;
                        $data_media['nns_id'] = np_guid_rand();
                        $data_media['nns_vod_id'] = $exists_id;
                        $data_media['nns_vod_index_id'] = $data['nns_id'];
                        $data_media['nns_vod_index'] = $index;
                        $data_media['nns_modify_time'] = date('Y-m-d H:i:s', time());
                        $data_media['nns_status'] = 1;
                        $data_media['nns_action'] = 'add';
                        $data_media['nns_modify_time'] = date('Y-m-d H:i:s', time());
                        $data_media['nns_import_source'] = $str_import_source;
                        $data_media['nns_cp_id'] = (isset($params['asset_source']) && strlen($params['asset_source']) >0) ? $params['asset_source'] : '0';
                        unset($data_media['nns_integer_id']);
                        nl_db_insert($dc->db(), 'nns_vod_media', $data_media);
                        $queue_task_model->_set_weight($vod_index_inst->_get_weight());
                        //$queue_task_model->q_add_task_op_mgtv($data_media['nns_id'], BK_OP_MEDIA, BK_OP_ADD,$this->message_id,true,$this->conf_info);
                        $queue_task_model->q_add_task_op_mgtv($data_media['nns_id'], BK_OP_MEDIA, BK_OP_ADD,$this->message_id,true,array('import_cdn_mode' => $media_val['nns_media_service']));
                        //$vod_media_inst->nns_db_vod_media_delete($media_val['nns_id']);//分集中已有片源删除
                    }
                }
            }
            $result = array('ret'=>0);
        }else{

            //检查主媒资是否为虚拟，是则将nns_mgtvbk_import_op中分组状态置为0 <feijian.gao@starcor.cn> -->start
            if($asset_id == $clip_id && $exists_id != false)
            {
                nl_import_op::update($this->db_obj,array("nns_is_group"=>0),array("nns_video_id"=>$exists_id,"nns_video_type"=>"video","nns_action"=>"add"));
            }
            //-->end

            $result = $vod_index_inst->nns_db_vod_index_modify($exists_id, $index, $this->change_spcial_word($params['name']), $params['time_len'], $params['pic'], $this->change_spcial_word($params['summary']), $this->change_spcial_word($params['director']), $this->change_spcial_word($params['player']), $str_import_source, $clip_id, $release_time,$params['asset_source']);

        }
        //var_dump($params_seekpoint);exit;
        /**
         * 梁攀   2015-04-22   分集打点信息添加注入
         */
        //先 删除分集的某一集打点信息
        $this->del_seekpoint($dc, $index, $exists_id);
        if(!empty($params_seekpoint))
        {
            //再添加分集的 打点信息
            if(array_key_exists('1', $params_seekpoint))
            {
                foreach ($params_seekpoint as $value)
                {
                    $this->add_seekpoint($dc, $index, $exists_id, $value["@attributes"]);
                }
            }
            else
            {
                $this->add_seekpoint($dc, $index, $exists_id, $params_seekpoint["@attributes"]);
            }

        }
        if ($result['ret'] == 0)
        {
            $index_info = $vod_index_inst->nns_db_vod_index_info($exists_id, $index);
            $vod_index_other_data = array(
                'nns_watch_focus' => $params['watch_focus'],
                'nns_director' => $params['director'],
                'nns_actor' => $params['player'],
            );
            if(!empty($conf_info))
            {
                $vod_index_other_data['nns_conf_info'] = json_encode($conf_info);
            }
            if(isset($params['thrid_party_info']) && is_array($params['thrid_party_info']) && !empty($params['thrid_party_info']))
            {
                $vod_index_other_data['nns_ext_url'] = json_encode($params['thrid_party_info']);
            }
            $this->__update_other_data($dc, $index_info['data'][0]['nns_id'], $vod_index_other_data, 'index');
            return $index_info['data'][0]['nns_id'];
        }
        else
        {

            //			错误日志
            $this->write_log('ret:' . $result['ret'] . "\n" . 'asset_id:' . $asset_id . "\n" . 'clip_id:' . $clip_id . "\n" . 'params:' . json_encode($params) . "\n", 'update_clip');
            return FALSE;
        }
    }

    /**
     * 打点信息添加
     * @param object $dc 数据库操作对象
     * @param int $index 分集号
     * @param string $exists_id vod_id
     * @param array $params 打点信息添加的数组
     * @return null
     * @author liangpan
     * @date 2015-04-17
     */
    public function add_seekpoint($dc,$index,$exists_id,$params)
    {
        $index = (int)$index;
        $sql_query="select count(*) as total from nns_vod_index_seekpoint where nns_video_index='{$index}' and nns_video_id='{$exists_id}' and nns_type='{$params['type']}' and nns_name='{$params['name']}' ";
        $result_query=nl_query_by_db($sql_query, $dc->db());
        if(!$result_query)
        {
            $this->write_log('ret:8' . " db query seekpoint error " . $sql_query , 'query_seekpoint');
            return ;
        }
        if(is_array($result_query) && $result_query[0]['total'] > 0)
        {
            return ;
        }
        $str_id = np_guid_rand();
        //对打点信息的图片ftp处理
        $check_img_index = $this->img_handel($params['img']);
        if (!$check_img_index)
        {
            $this->write_logs(var_export($this->re_code, true), "打点信息图片处理失败");
            return ;
        }
        $check_img_index = $check_img_index === TRUE ? '' : $check_img_index;
        $sql_insert="insert into nns_vod_index_seekpoint (nns_id,nns_video_index,nns_video_id,nns_type,nns_image,nns_begin,nns_end,nns_name) values " .
            "('{$str_id}','{$index}','{$exists_id}','{$params['type']}','{$check_img_index}','{$params['begin']}','{$params['end']}','{$params['name']}')";
        $result_insert=nl_execute_by_db($sql_insert, $dc->db());
        if(!$result_insert)
        {
            $this->write_log('ret:8' . " db insert seekpoint error " . 'sql:'. $sql_insert , 'insert_seekpoint');
        }
        return ;
    }

    /**
     * 打点信息删除
     * @param object $dc 数据库操作对象
     * @param int $index 分集号
     * @param string $exists_id vod_id
     * @return boolean
     * @author liangpan
     * @date 2015-04-17
     */
    public function del_seekpoint($dc,$index,$vod_id)
    {
        if(empty($vod_id) || $index=='')
        {
            return true;
        }
        $index=(int)$index;
        $sql_del = "delete from nns_vod_index_seekpoint where nns_video_id='{$vod_id}' and nns_video_index='{$index}' ";
        $result_del = nl_execute_by_db($sql_del, $dc->db());
        if(!$result_del)
        {
            $this->write_log('ret:8,reason:delete seekpoint db error,sql:' . $sql_del , 'delete_seekpoint');
            return false;
        }
        return true;
    }

    /**
     * 点播分片删除
     * @param array(
     * 	'id'=>clip_id 对方平台分片注入ID
     * )
     * @return TRUE 删除成功
     * 		FALSE 删除失败
     * 	-1 无此ID
     */
    public function delete_clip($params, $delete_core = TRUE,$nns_cp_id)
    {

        if (empty($params['id']))
            return FALSE;

        //      获取CMS平台ID
        $vod_index_inst = new nns_db_vod_index_class();
        //贵州cp权重优先级
        //查询主媒资内容提供商
        $dc = nl_get_dc(array(
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
        include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_index.class.php';
        $index_result= nl_vod_index::query_by_id($dc,$params['id']);
        $vod_result=nl_vod::query_by_id($dc,$index_result['data_info'][0]['nns_vod_id']);
        $cp_weight=nl_producer::get_weight($dc,$vod_result['data_info'][0]['nns_cp_id'],$vod_result['data_info'][0]["nns_producer"]);
        if($cp_weight['ret']==0 && is_array($cp_weight['data_info']))
        {
            $vod_index_inst->_set_weight($cp_weight['data_info']['weight']);
        }
        else
        {
            $vod_index_inst->_set_weight($this->_get_weight());
        }
        $vod_index_inst->_set_is_group($this->_get_is_group());
        $clip_data = $vod_index_inst->get_video_index_id_by_import_id(IMPORT_SOURCE, $params['id'],null,false,false,$nns_cp_id,$this->cp_config['check_metadata_enabled']);
        if(!is_array($clip_data))
        {
            return true;
        }
        $vod_index_inst->message_id = $this->message_id;
        $result = $vod_index_inst->nns_db_vod_index_delete($clip_data['nns_id']);
        $return = FALSE;
        if ($result["ret"] == 0)
        {
            $return = TRUE;
            $dc = nl_get_dc(array(
                'db_policy' => NL_DB_WRITE,
                'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
            ));
            $dc->open();
            nl_vod::delete_vod_info_by_cache($dc, $clip_data['nns_vod_id']);
        }
        else
        {
            if ($result['ret'] == NNS_ERROR_VOD_INDEX_NOT_FOUND)
                $return = TRUE;
            //			错误日志
            $this->write_log('ret:' . $result['ret'] . "\n" . 'import_id:' . $params['id'] . "\n" . 'nns_id:' . $clip_data['nns_id'] . "\n" . 'vod_id:' . $clip_data['nns_vod_id'] . "\n", 'delete_clip');
        }

        // 		$sql_del = "delete from nns_vod_index_seekpoint where nns_video_id='{$clip_data['nns_vod_id']}' and nns_video_index='{$clip_data['nns_index']}' ";
        // 		$result_del = nl_execute_by_db($sql_del, $dc->db());
        // 		if(!$result_del)
        // 		{
        // 			$this->write_log('ret:8,reason:delete seekpoint db error,sql:' . $sql_del , 'delete_seekpoint');
        // 			$return = false;
        // 		}
        /**
         * 梁攀   这儿是删除分集的打点信息   2015-04-20
         * ***************start************
         */
        //         $sql_del = "delete from nns_vod_index_seekpoint where nns_video_id='{$clip_data['nns_vod_id']}' and nns_video_index='{$clip_data['nns_index']}' ";
        //         $result_del = nl_execute_by_db($sql_del, $dc->db());
        //         if(!$result_del)
        //         {
        //         	$this->write_log('ret:8,reason:delete seekpoint db error,sql:' . $sql_del , 'delete_seekpoint');
        //         	$return = false;
        //         }
        $return = $this->del_seekpoint($dc, $clip_data['nns_index'], $clip_data['nns_vod_id']);
        /**
         * 梁攀   这儿是删除分集的打点信息   2015-04-20
         * ***************end************
         */
        return $return;
    }

    /**
     * 更新或修改片源信息
     * @param String 影片注入ID
     * @param String 分片注入ID
     * @param String 片源注入ID
     * @param Array(
     * 	file_name="片源名称"
    file_type=“片源类型 flv | mp4 | ts” *必要参数
    file_path=“片源物理文件相对路径”
    file_definition=“片源清晰度 hd|std|low”   *默认为std
    file_resolution=“分辨率”800*600  ××××××××××××××基础函数未存
    file_size=“1024”文件大小M  ××××××××××××××基础函数未存
    file_bit_rate=“片源码率”kbps
    file_desc=”文件描述”
    file_time_len="时长"
    file_core_id='core平台ID'
    file_dimensions='2D'  片源维度 标识 2D 3D类型
     * )
     * @return String 注入成功后media_id
     * 			FALSE 注入失败
     */
    public function update_file($asset_id, $clip_id, $file_id, $params)
    {
        //	echo $asset_id,$clip_id,$file_id;
        //echo '<pre>';print_r($params);die();
        //检查是否影片注入ID已存在
        //var_dump($this->message_id);die;
        $video_inst = new nns_db_vod_class();
        $params['asset_source'] = (!isset($params['asset_source']) || strlen($params['asset_source']) < 1) ? '0' : $params['asset_source'];
        $str_import_source = (isset($params['import_source']) && strlen($params['import_source']) >0) ? $params['import_source'] : IMPORT_SOURCE;
        $exists_id = $video_inst->get_video_id_by_import_id($str_import_source, $asset_id,$params['asset_source']);
        if ($exists_id === FALSE)
        {
            $this->import_desc =array(
                'ret'=>6,
                'reason'=>'asset id is not exist',
            );
            return FALSE;
        }

        //检查是否分片注入ID已存在
        $video_index_inst = new nns_db_vod_index_class();
        $exists_index = $video_index_inst->get_video_index_id_by_import_id($str_import_source, $clip_id,null,null,$exists_id,$params['asset_source'],1);
        if ($exists_index === FALSE)
        {
            $this->import_desc =array(
                'ret'=>7,
                'reason'=>'index id is not exist',
            );
            return FALSE;
        }
        //$media_name=mb_substr($params['file_name'],0,128,"utf-8");

        $video_media_inst = new nns_db_vod_media_class();
        $video_media_inst->message_id = $this->message_id;
        //贵州cp权重优先级
        //查询主媒资内容提供商
        $dc = nl_get_dc(array(
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
        $vod_result=nl_vod::query_by_id($dc,$exists_id);
        $cp_weight=nl_producer::get_weight($dc,$vod_result['data_info']['nns_cp_id'],$vod_result['data_info']["nns_producer"]);
        if($cp_weight['ret']==0 && is_array($cp_weight['data_info']))
        {
            $video_media_inst->_set_weight($cp_weight['data_info']['weight']);
        }
        else
        {
            if($params['priority'])
            {
                $video_media_inst->_set_weight($params['priority']);
            }
            else
            {
                $video_media_inst->_set_weight($this->_get_weight());
            }

        }
        $video_media_inst->conf_info = $this->conf_info;
        $params['file_definition'] = empty($params['file_definition']) ? 'std' : $params['file_definition'];
        //片源类型
        $params['media_type'] = (isset($params['media_type']) && !empty($params['media_type'])) ? $params['media_type'] : 1;
        $params['original_live_id'] = (isset($params['original_live_id']) && !empty($params['original_live_id'])) ? $params['original_live_id'] : '';
        //片源服务类型
        $params['media_service_type'] = (isset($params['media_service_type']) && !empty($params['media_service_type'])) ? $params['media_service_type'] : '';
        if($this->media_import_module)//片源检测，删掉老数据
        {
            //不同注入ID
            $video_media_inst->get_file_media_check($exists_id, $exists_index, $file_id, $params['file_definition'], $params['file_type'], $params['file_dimensions'],$params['asset_source'],$params['file_bit_rate'],$params['tag'],$params['media_type'],$params['original_live_id'],$params['media_service_type']);
        }
        //检查是否片源注入ID已存在
        $params['index'] = $exists_index['nns_index'];
        $params['asset_id'] = $exists_index['nns_vod_id'];
        $params['clip_id'] = $exists_index['nns_id'];
        $exists_media = $video_media_inst->get_video_media_id_by_import_id_bk($str_import_source, $file_id, $params,$params['asset_source']);

        if ($exists_media['nns_vod_id'] != $exists_id)
        {
            $exists_media = false;
        }

        //若片源已存在，根据项目情况判断是否反馈注入失败或成功

        if ($exists_media !== FALSE)
        {
            if(defined('NOTIFY_FAIL_ENABLED') && NOTIFY_FAIL_ENABLED)
            {//片源存在反馈注入失败
                $this->import_desc =array(
                    'ret' => 8,
                    'reason'=>'media is already exist',
                );
                return false;
            }
            else if($this->media_import_comm)
            {
                //注入ID存在，进行删除
                $demo = $video_media_inst->nns_db_vod_media_delete($exists_media['nns_id']);
            }
            elseif ($this->media_import_module_v2 && (empty($exists_media['nns_mode']) || $this->media_mode[$params['file_definition']] >= $this->media_mode[$exists_media['nns_mode']]))//芒果新的片源替换逻辑
            {
                $video_media_inst->nns_db_vod_media_delete($exists_media['nns_id'],true,false);
            }
            else
            {
                $this->import_desc =array(
                    'ret' => 8,
                    'reason'=>'media is already exist',
                );
                return $exists_media['nns_id'];
            }
        }
        else if($this->media_import_comm)//包含相同的注入ID
        {
            $video_media_inst->check_exsist_and_del($exists_id, $exists_index['nns_id'], $params['file_definition'], $params['file_type'] , $params['file_dimensions'], $params['file_bit_rate'], $params['asset_source'], $params['tag'],$params['media_type'],$params['original_live_id'],$params['media_service_type']);
        }
        elseif ($this->media_import_module_v2)//芒果新的片源替换逻辑
        {
            $check_module_v2 = $video_media_inst->get_file_media_check($exists_id, $exists_index,'','',$params['file_type'],$params['file_dimensions'],$params['asset_source'],null,null,$params['media_type'],$params['original_live_id'],$params['media_service_type']);
            if(is_array($check_module_v2) && !empty($check_module_v2))
            {
                foreach($check_module_v2 as $check_module_v2_val){
                    if(empty($check_module_v2_val['nns_mode']) || $this->media_mode[$params['file_definition']] >= $this->media_mode[$check_module_v2_val['nns_mode']])
                    {
                        $video_media_inst->nns_db_vod_media_delete($check_module_v2_val['nns_id']);
                    }
                    //存在高于当前清晰度的影片
                    if($this->media_mode[$params['file_definition']] < $this->media_mode[$check_module_v2_val['nns_mode']])
                    {
                        return $check_module_v2_val['nns_id'];
                    }
                }
            }
        }
        //注入片源时，由于注入ID相同，只是import_source不同，则会造成新增失败，需把片源删除但不生成任务
        if((int)$this->cp_config['check_metadata_enabled'] === 0)
        {
            $video_media_inst->check_metadata_media($exists_id,$exists_index['nns_id'],$file_id);
        }

        //检查是否为虚拟的主媒资注入,若是则将虚拟主媒资分集的分组标识置为0<feijian.gao@starcor.cn> -->start
        if($exists_media == false && ($clip_id == $file_id))
        {
            nl_import_op::update($this->db_obj,array("nns_is_group"=>0),array("nns_video_id"=>$exists_id,"nns_video_type"=>"video","nns_action"=>"add"));
            nl_import_op::update($this->db_obj,array("nns_is_group"=>0),array("nns_video_id"=>$exists_index['nns_id'],"nns_video_type"=>"index","nns_action"=>"add"));
        }
        //-->end

        //core平台ID不为空为绑定
        //core平台ID不为空，则为绑定
        $video_media_inst->_set_is_group($this->_get_is_group());
        if (empty($params['file_core_id']) && $this->core_import === TRUE)
        {
            $result = $this->bind_file($video_media_inst, $exists_id, $exists_index, $params, $file_id);
        }
        else
        {
            $result = $this->add_file($video_media_inst, $exists_id, $exists_index, $params, $file_id,$str_import_source);
        }
        if ($result['ret'] == 0)
        {
            $media_info = $video_media_inst->get_video_media_id_by_import_id_bk($str_import_source, $file_id, $params);
            $dc = nl_get_dc(array(
                'db_policy' => NL_DB_WRITE,
                'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
            ));
            $dc->open();
            nl_vod::delete_vod_info_by_cache($dc, $media_info['nns_vod_id']);
            return $media_info['nns_id'];
        }
        else
        {
            //			错误日志
            $this->write_log('ret:' . $result['ret'] . "\n" . 'asset_id:' . $asset_id . "\n" . 'clip_id:' . $clip_id . "\n" . 'media_id:' . $file_id . "\n" . 'params:' . json_encode($params) . "\n", 'update_media');
            $this->import_desc = $result;
            return FALSE;
        }
    }

    /**
     * 测试使用 片源库
     * @param array $live_file_content
     * @return array('ret'=>'返回码,0成功,其余失败','reason'=>'返回结果说明')
     * @author feijian.gao
     * @date	2016年10月17日11:56:14
     */
    public function insert_live_file($live_file_content)
    {
        //>>1 查询片源你是否存在（存在则修改，否则进行更新）
        $where_arr = array(
            'where'=>array(
                'nns_content_id'=>$live_file_content['live_media_id'],
                'nns_cp_id'=>$live_file_content['live_media_source'],
                'nns_import_source' => $live_file_content['live_file_info']['import_source']
            ),
        );
        $check_live_media_exist = nl_live_media::query($this->db_obj,$where_arr);

        //处理参数（处理成符合数据表结构的格式）
        if($live_file_content['live_file_info']['src_cast_type'] == 'multicast')
        { //单播还是组播
            $nns_media_caps = 1;
            $nns_url = "UDP://@{$live_file_content['live_file_info']['multi_cast_ip']}:{$live_file_content['live_file_info']['multi_cast_port']}";
        }
        else if($live_file_content['live_file_info']['src_cast_type'] == 'unicast')
        {
            $nns_media_caps=2;
            $nns_url =$live_file_content['live_file_info']['unicast_url'];
        }
        else
        {
            $nns_media_caps=0;
        }
        //时移标志
        $nns_timeshift = ($live_file_content['live_file_info']['timeshift_status'] == '0') ? 'LIVE' : 'LIVE,TSTV';
        $nns_id = np_guid_rand();
        //节目时长
        $nns_storage_len = (int)$live_file_content['live_file_info']['storage_duration']*3600;

        //>>2 数据入库
        $input_param = array(
            'nns_id'=>$nns_id,
            'nns_media_caps'=>$nns_timeshift,
            'nns_name'=>$live_file_content['live_file_info']['name'],
            'nns_live_id'=>$live_file_content['live_id'],
            'nns_live_index_id'=>$live_file_content['live_index_id'],
            'nns_type'=>$live_file_content['live_video_type'],
            'nns_tag'=>$live_file_content['live_file_info']['tag'],
            'nns_url'=>$nns_url,
            'nns_mode'=>$live_file_content['live_file_info']['definition'],
            'nns_kbps'=>$live_file_content['live_file_info']['bit_rate'],
            'nns_content_id'=>$live_file_content['live_media_id'],
            'nns_filetype'=>$live_file_content['live_file_info']['type'],
            'nns_cast_type'=>$nns_media_caps,
            'nns_cp_id'=>$live_file_content['live_file_info']['live_source'],
            'nns_timeshift_status'=>$live_file_content['live_file_info']['timeshift_status'],
            'nns_timeshift_delay'=>$live_file_content['live_file_info']['timeshit_duration'],
            'nns_storage_status'=>$live_file_content['live_file_info']['tvod_status'],
            'nns_message_id'=>$live_file_content['message_id'],
            'nns_storage_delay'=>$nns_storage_len,
            'nns_deleted' => 0,
            'nns_import_source' => $live_file_content['live_file_info']['import_source'],
            'nns_domain'=>$live_file_content['live_file_info']['domain'],
            'nns_live_index'=>$live_file_content['live_file_info']['live_index'],
            'nns_state' => isset($live_file_content['live_file_info']['state']) ? $live_file_content['live_file_info']['state'] : 1,
        );
        $str_op_action = BK_OP_ADD;
        if(!is_array($check_live_media_exist['data_info']))
        {
            $live_media_result = nl_live_media::add($this->db_obj,$input_param);
        }
        else
        {
            $str_op_action = BK_OP_MODIFY;
            unset($input_param['nns_id']);
            $nns_id = $check_live_media_exist['data_info'][0]['nns_id'];
            $live_media_result = nl_live_media::edit($this->db_obj,$input_param,$nns_id);
        }
        if($live_media_result['ret'] != '0')
        {
            return  array(
                'ret' => '1',
                'reason' => '直播片源入库失败:'.$live_media_result['reason'],
            );
        }
        $this->_get_cp_config($live_file_content['live_file_info']['live_source']);
        if(isset($this->cp_config['message_live_media_import_enable']) && $this->cp_config['message_live_media_import_enable']  == '1')
        {
            $queue_task_model = new queue_task_model();
            $queue_task_model->_set_weight($this->_get_weight());
            $queue_task_model->q_add_task_op_mgtv($nns_id, BK_OP_LIVE_MEDIA, $str_op_action,$this->message_id);
            unset($queue_task_model);
            return array(
                'ret'=>'0'
            );
        }
        //>>3 注入队列
        $input_queue_param = array(
            'nns_live_id'=>$live_file_content['live_id'],
            'nns_live_index_id'=>$live_file_content['live_index_id'],
            'nns_live_media_id'=>$nns_id,
            'nns_action'=>$live_file_content['action'],
            'nns_message_id'=>$live_file_content['message_id'],
            'nns_cp_id'=>$live_file_content['live_media_source'],
        );
        $cdn_live_media_result = nl_cdn_live_media_queue::add($this->db_obj,$input_queue_param);
        if($cdn_live_media_result['ret'] != '0')
        {
            return array(
                'ret' => '1',
                'reason' => '队列添加失败:'.$cdn_live_media_result['reason'],
            );
        }
        return array(
            'ret'=>'0'
        );
    }

    /**
     * 获取CP信息
     * @param unknown $cp_id
     */
    private function _get_cp_config($cp_id)
    {
        if(is_array($this->cp_config) && !empty($this->cp_config))
        {
            return ;
        }
        $result = nl_cp::query_by_id($this->db_obj, $cp_id);
        if(!isset($result['data_info']) || !is_array($result['data_info']) || empty($result['data_info']))
        {
            return ;
        }
        $this->cp_config = isset($result['data_info']['nns_config']) && !empty($result['data_info']['nns_config']) ? json_decode($result['data_info']['nns_config'], true) : null;
        return ;
    }
    /**
     * 节目单入库
     * @param $param
     * @return array
     */
    public function import_live_playbill($param)
    {
        $begin_time = date('Y-m-d H:i:s',strtotime($param['playbill_info']['start_date'].str_pad($param['playbill_info']['start_time'],6,'0',STR_PAD_LEFT)));
        //拼音
        $pinyin = nns_controls_pinyin_class::get_pinyin_letter($param['playbill_info']['program_name']);
        $pinyin_length = nns_controls_pinyin_class::$length;

        //节目单时长
        $nns_time_len = str_pad($param['playbill_info']['duration'],6,'0',STR_PAD_LEFT);
        $time_h=substr($nns_time_len,0,2); $time_i=substr($nns_time_len,2,2);  $time_s = substr($nns_time_len,4,2);
        $nns_time_len = (int)$time_h*3600+(int)$time_i*60+(int)$time_s;

        $playbill_inst = new nl_playbill();
        $nns_id = np_guid_rand();
        $input_param = array(
            'nns_id'=>$nns_id,
            'nns_name'=>$param['playbill_info']['program_name'],
            'nns_begin_time'=>$begin_time,
            'nns_time_len'=>$nns_time_len,
            'nns_live_id'=>$param['live_id'],
            'nns_state'=>'0',
            'nns_summary'=>$param['playbill_info']['info'],
            'nns_image1'=>$param['playbill_info']['image1'],
            'nns_image2'=>$param['playbill_info']['image2'],
            'nns_image3'=>$param['playbill_info']['image3'],
            'nns_image4'=>$param['playbill_info']['image4'],
            'nns_image5'=>$param['playbill_info']['image5'],
            'nns_image0'=>$param['playbill_info']['image0'],
            'nns_actor'=>$param['playbill_info']['actor'],
            'nns_director'=>$param['playbill_info']['director'],
            'nns_kind'=>$param['playbill_info']['kind'],
            'nns_pinyin'=>$pinyin,
            'nns_pinyin_length'=>$pinyin_length,
            'nns_live_media_id'=>$param['live_media_id'],
            'nns_playbill_import_id'=>$param['playbill_id'],
            'nns_domain'=>$param['playbill_info']['domain'],
            'nns_hot_dgree'=>$param['playbill_info']['hot_degree'],
            'nns_cp_id'=>$param['live_media_source'],
            'nns_import_source'=>$param['playbill_info']['import_source'],
        );
        $where_arr = array(
            'where'=>array(
                'nns_playbill_import_id'=>$param['playbill_id'],
                'nns_cp_id'=>$param['live_media_source'],
                'nns_import_source'=>$param['playbill_info']['import_source'],
            ),
        );

        $check_playbill_exist = nl_playbill::query($this->db_obj,$where_arr);
        $str_op_action = BK_OP_ADD;
        if(!is_array($check_playbill_exist['data_info']))
        {
            $live_playbill_result = nl_playbill::add($this->db_obj,$input_param);
        }
        else
        {
            $str_op_action = BK_OP_MODIFY;
            unset($input_param['nns_id']);
            $nns_id = $check_playbill_exist['data_info'][0]['nns_id'];
            $live_playbill_result = nl_playbill::edit($this->db_obj,$input_param,$nns_id);
        }
        if($live_playbill_result['ret'] != '0')
        {
            return $result =array(
                'ret'=>'1',
                'reason'=>'添加节目单失败'.$live_playbill_result['reason'],
                'data'=>$nns_id,
            );
        }
        $this->_get_cp_config($param['live_media_source']);
        if(isset($this->cp_config['message_playbill_import_enable']) && $this->cp_config['message_playbill_import_enable']  == '1')
        {
            $queue_task_model = new queue_task_model();
            $queue_task_model->_set_weight($this->_get_weight());
            $queue_task_model->_set_is_group($this->_get_is_group());
            $queue_task_model->q_add_task_op_mgtv($nns_id, BK_OP_PLAYBILL, $str_op_action,$this->message_id);
            unset($queue_task_model);
            return array(
                'ret'=>'0'
            );
        }
        //注入到队列
        $input_queue_param = array(
            'nns_live_id'=>$param['live_id'],
            'nns_live_index_id'=>$param['live_index_id'],
            'nns_live_media_id'=>$param['live_media_id'],
            'nns_playbill_id'=>$nns_id,
            'nns_action'=>$param['action'],
            'nns_message_id'=>$param['message_id'],
            'nns_cp_id'=>$param['live_media_source'],
        );
        $cdn_playbill_result = nl_cdn_playbill_queue::add($this->db_obj,$input_queue_param);
        if($cdn_playbill_result['ret'] != '0')
        {
            return $result =array(
                'ret'=>'1',
                'reason'=>'添加队列失败'.$cdn_playbill_result['reason'],
                'data'=>$nns_id,
            );
        }
        return array(
            'ret'=>'0',
            'reason' => '节目单添加成功',
        );
    }

    /**
     * 逻辑删除节目单
     */
    public function delete_playbill($param)
    {
        $where_arr = array(
            'where'=>array(
                'nns_playbill_import_id'=>$param['playbill_id'],
                'nns_cp_id'=>$param['live_playbill_source'],
            ),
        );
        $palybill_data = nl_playbill::query($this->db_obj,$where_arr);
        $nns_playbill_id = $palybill_data['data_info'][0]['nns_id'];
        $nns_live_id = $palybill_data['data_info'][0]['nns_live_id'];
        $nns_live_media_id = $palybill_data['data_info'][0]['nns_live_media_id'];
        $playbill_media_data = nl_live_media::query_by_id($this->db_obj,$nns_live_media_id);
        $nns_live_index_id = $playbill_media_data['data_info']['nns_live_index_id'];
        //查询到删除对象
        if(is_array($palybill_data['data_info']))
        {
            $logic_delete_playbill = nl_playbill::edit($this->db_obj,array('nns_state'=>'1'),$nns_playbill_id);
            $this->_get_cp_config($param['live_playbill_source']);
            if(isset($this->cp_config['message_playbill_import_enable']) && $this->cp_config['message_playbill_import_enable']  == '1')
            {
                $queue_task_model = new queue_task_model();
                $queue_task_model->_set_weight($this->_get_weight());
                $queue_task_model->_set_is_group($this->_get_is_group());
                $queue_task_model->q_add_task_op_mgtv($nns_playbill_id, BK_OP_PLAYBILL, BK_OP_DELETE,$this->message_id);
                unset($queue_task_model);
                return array(
                    'ret'=>'0',
                    'reason'=>'操作成功'
                );
            }
            $input_param = array(
                'nns_live_id'=>$nns_live_id,
                'nns_live_index_id'=>$nns_live_index_id,
                'nns_live_media_id'=>$nns_live_media_id,
                'nns_playbill_id'=>$nns_playbill_id,
                'nns_action'=>$param['action'],
                'nns_message_id'=>$param['message_id'],
                'nns_cp_id'=>$param['live_playbill_source'],
            );
            $queue_result = nl_cdn_playbill_queue::add($this->db_obj,$input_param);
            if($queue_result['ret'] == '0' && $logic_delete_playbill['ret'] == '0')
            {
                return array(
                    'ret'=>'0',
                    'reason'=>'操作成功'
                );
            }
        }
        else
        {//没有查询到删除对象
            return array(
                'ret'=>'1',
                'reason'=>'删除对象不存在'
            );
        }
    }


    public function _delete_file_package($params,$where_params)
    {
        foreach ($where_params as $key=>$val)
        {
            if(strlen($val) <1)
            {
                return array(
                    'ret'=>'1',
                    'reason'=>"唯一确定参数为空".$key,
                );
            }
        }
        $result_unique = nl_file_package::query_unique($this->db_obj,$where_params);
        if($result_unique['ret'] !=0)
        {
            return $result_unique;
        }
        if(isset($result_unique['data_info']) && is_array($result_unique['data_info']) && !empty($result_unique['data_info']))
        {
            foreach ($result_unique['data_info'] as $del_val)
            {
                $result_del = nl_file_package::edit($this->db_obj,array('nns_deleted'=>1),$del_val['nns_id']);
                if($result_del['ret'] !=0)
                {
                    return $result_del;
                }
                $queue_task_model = new queue_task_model();
                $queue_task_model->_set_weight($params['nns_hot_degree'] > 0 ? $params['nns_hot_degree'] : 0);
                $queue_task_model->_set_is_group($this->_get_is_group());
                $queue_task_model->q_add_task_op_mgtv($del_val['nns_id'], BK_OP_FILE, BK_OP_DELETE,$this->message_id);
                unset($queue_task_model);
            }
        }
        return $result_del;
    }

    /**
     * 修改file包
     * @param unknown $params
     * @param unknown $where_params
     */
    public function _modify_file_package($params,$where_params)
    {
        foreach ($where_params as $key=>$val)
        {
            if(strlen($val) <1)
            {
                return array(
                    'ret'=>'1',
                    'reason'=>"唯一确定参数为空".$key,
                );
            }
        }
        $result_unique = nl_file_package::query_unique($this->db_obj,$where_params);
        if($result_unique['ret'] !=0)
        {
            return $result_unique;
        }
        if(isset($result_unique['data_info']) && is_array($result_unique['data_info']) && !empty($result_unique['data_info']))
        {
            foreach ($result_unique['data_info'] as $del_val)
            {
                $result_del = nl_file_package::edit($this->db_obj,array('nns_deleted'=>1),$del_val['nns_id']);
                if($result_del['ret'] !=0)
                {
                    return $result_del;
                }
                $queue_task_model = new queue_task_model();
                $queue_task_model->_set_weight($params['nns_hot_degree'] > 0 ? $params['nns_hot_degree'] : 0);
                $queue_task_model->_set_is_group(0);
                $queue_task_model->q_add_task_op_mgtv($del_val['nns_id'], BK_OP_FILE, BK_OP_DELETE,$this->message_id);
                unset($queue_task_model);
            }
        }
        $last_data = array_merge($params,$where_params);
        $params['nns_id'] = np_guid_rand();
        $result = nl_file_package::add($this->db_obj, $params);
        if($result['ret'] !=0)
        {
            return $result;
        }
        $queue_task_model = new queue_task_model();
        $queue_task_model->_set_weight($params['nns_hot_degree'] > 0 ? $params['nns_hot_degree'] : 0);
        $queue_task_model->_set_is_group($this->_get_is_group());
        $queue_task_model->q_add_task_op_mgtv($params['nns_id'], BK_OP_FILE,BK_OP_ADD,$this->message_id);
        unset($queue_task_model);
        return $result;
    }


    private function bind_file($video_media_inst, $exists_id, $exists_index, $params, $file_id,$import_source='')
    {
        $import_source = (isset($import_source) && strlen($import_source)>0) ? $import_source : IMPORT_SOURCE;
        $result = $video_media_inst->nns_db_vod_media_bind($exists_id, NULL, $this->change_spcial_word($params['file_name']), $exists_index['nns_index'], $params['file_definition'], $params['file_bit_rate'], 1, NULL, $params['file_path'], $params['file_time_len'], $this->change_spcial_word($params['file_desc']), $params['file_core_id'], 'VOD', $import_source, $file_id, $params['file_dimensions'],$params['asset_source'],$params['media_type'],$params['original_live_id'],$params['start_time'],$params['media_service_type']);
        return $result;
    }

    private function add_file($video_media_inst, $exists_id, $exists_index, $params, $file_id,$import_source)
    {
        //添加片源信息
        if ($this->core_import === FALSE)
        {
            //		不需要注入core平台则只增加自身平台
            $result = $video_media_inst->nns_db_vod_media_add_impl($exists_id, $exists_index['nns_index'], $exists_index['nns_id'], $this->change_spcial_word($params['file_name']), 0, 1, $params['file_definition'], $params['tag'], 1, $params['file_path'], $params['file_bit_rate'], NULL, $params['file_core_id'], NULL, $params['file_type'], 'VOD', $import_source, $file_id, $params['file_dimensions'],$params['asset_source'],$params['media_type'],$params['original_live_id'],$params['start_time'],$params['media_service_type'],$params['domain']);

            //fjyd有片源更新
            //$str_sql = "update nns_vod_index set nns_new_media='1' where nns_id='".$exists_index['nns_id']."' and nns_vod_id='$exists_id'";
            //include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/mgtv/fjyd/models/clip_task_model.php";
            //clip_task_model::update_new_media_state(array('vod_index_id'=>$exists_index['nns_id'],'vod_id'=>$exists_id,'status'=>1));
            //die;
            //$video_media_inst->db_obj->nns_db_execute ( $str_sql );
        }
        else
        {
            //		需要注入core平台
            $result = $video_media_inst->nns_db_vod_media_add($exists_id, $this->change_spcial_word($params['file_name']), $this->change_spcial_word($params['file_name']), $exists_index['nns_index'], $params['file_definition'], $params['file_bit_rate'], 1, $params['tag'], $params['file_path'], $params['file_time_len'], $this->change_spcial_word($params['file_desc']), $this->core_policy_id, $params['file_type'], 'VOD',$import_source, $file_id, $params['file_dimensions'],$params['asset_source'],$params['media_type'],$params['original_live_id'],$params['start_time'],$params['media_service_type'],$params['domain']);
        }
        return $result;
    }

    /**
     * 点播片源删除
     * @param array(
     * 	'id'=>media_id 对方平台片源注入ID
     * )
     * @return TRUE 删除成功
     * 		FALSE 删除失败
     * 	-1
     */
    public function delete_file($params, $delete_core = TRUE,$nns_cp_id,$media_service_type = null,$str_import_source='')
    {

        if (empty($params['id']))
        {
            return FALSE;
        }
        //      获取CMS平台ID
        $vod_media_inst = new nns_db_vod_media_class();
        //贵州cp权重优先级
        //查询主媒资内容提供商
        $dc = nl_get_dc(array(
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
        include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/video/vod_media.class.php';
        $vod_media_result=nl_vod_media::query_by_id($dc,$params['id']);
        $vod_result=nl_vod::query_by_id($dc,$vod_media_result['data_info']['nns_vod_id']);
        $cp_weight=nl_producer::get_weight($dc,$vod_result['data_info']['nns_cp_id'],$vod_result['data_info']["nns_producer"]);
        if($cp_weight['ret']==0 && is_array($cp_weight['data_info']))
        {
            $vod_media_inst->_set_weight($cp_weight['data_info']['weight']);
        }
        else
        {
            $vod_media_inst->_set_weight($this->_get_weight());
        }
        $vod_media_inst->_set_is_group($this->_get_is_group());
        $vod_media_inst->message_id = $this->message_id;
        $vod_media_inst->conf_info = $this->conf_info;
        $vod_media_inst->import_id = $params['id'];
        $str_import_source = strlen($str_import_source)>1 ? $str_import_source : IMPORT_SOURCE;
        $media_data = $vod_media_inst->get_video_media_id_by_import_id($str_import_source, $params['id'],$nns_cp_id,$this->cp_config['check_metadata_enabled'],$media_service_type);
        if(!is_array($media_data))
        {
            return true;
        }
        $result = $vod_media_inst->nns_db_vod_media_delete($media_data['nns_id']);
        $return = FALSE;
        if ($result["ret"] == 0)
        {
            $return = TRUE;
            $dc = nl_get_dc(array(
                'db_policy' => NL_DB_WRITE,
                'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
            ));
            $dc->open();
            nl_vod::delete_vod_info_by_cache($dc, $media_data['nns_vod_id']);
        }
        else
        {
            if ($result['ret'] == NNS_ERROR_VOD_MEDIA_NOT_FOUND)
                $return = -1;
            //			错误日志
            $this->write_log('ret:' . $result['ret'] . "\n" . 'import_id:' . $params['id'] . "\n" . 'nns_id:' . $media_data['nns_id'] . "\n" . 'vod_id:' . $media_data['nns_vod_id'] . "\n", 'delete_file');
        }

        return $return;
    }

    public function delete_channel_file($params,$nns_cp_id)
    {
        $where_arr = array(
            'where'=>array(
                'nns_content_id'=>$params['live_media_id'],
                'nns_cp_id'=>$nns_cp_id,
            )
        );
        $media_data = nl_live_media::query($this->db_obj,$where_arr);
        $nns_id = $media_data['data_info'][0]['nns_id'];
        $nns_live_id = $media_data['data_info'][0]['nns_live_id'];
        $nns_live_index_id = $media_data['data_info'][0]['nns_live_index_id'];

        if(is_array($media_data) && $media_data !== false)
        {
            $result = nl_live_media::edit($this->db_obj,array('nns_deleted'=>1),$nns_id);
            $this->_get_cp_config($nns_cp_id);
            if(isset($this->cp_config['message_playbill_import_enable']) && $this->cp_config['message_playbill_import_enable']  == '1')
            {
                $queue_task_model = new queue_task_model();
                $queue_task_model->_set_weight($this->_get_weight());
                $queue_task_model->_set_is_group($this->_get_is_group());
                $queue_task_model->q_add_task_op_mgtv($nns_id, BK_OP_LIVE_MEDIA, BK_OP_DELETE,$this->message_id);
                unset($queue_task_model);
                return array(
                    'ret'=>'0',
                    'reason'=>'删除成功'
                );
            }
            //进入队列
            $input_queue = array(
                'nns_live_id'=>$nns_live_id,
                'nns_live_index_id'=>$nns_live_index_id,
                'nns_live_media_id'=>$nns_id,
                'nns_action'=>$params['action'],
                'nns_message_id'=>$params['message_id'],
                'nns_cp_id'=>$nns_cp_id,
            );
            $cdn_live_media_result = nl_cdn_live_media_queue::add($this->db_obj,$input_queue);
            if($cdn_live_media_result['ret'] == '0' && $result['ret'] == '0')
            {
                return $return = array(
                    'ret'=>'0',
                    'reason'=>'删除成功'
                );
            }

        }
        else
        {
            return array(
                'ret'=>'1',
                'reason'=>'未查询到直播片源'
            );
        }

        return array(
            'ret'=>'1',
            'reason'=>'删除失败'
        );

    }


    /**
     * $category_name 栏目名称
     * $parent_id  代码动态创建几级栏目 10000代表一级栏目, 代表二级栏目 ,
     * $$depot_type 0 点播 ,1直播
     */
    public function set_depot_category($category_name, $depot_type = 0, $parent_id = '10000')
    {
        $depot_inst = new nns_db_depot_class();
        $depot_result = $depot_inst->nns_db_depot_info(0, NULL, $depot_type);

        $org_id = $depot_result['data'][0]['nns_org_id'];
        $org_type = $depot_result['data'][0]['nns_org_type'];
        $depot_id = $depot_result['data'][0]['nns_id'];
        $depot_info = $depot_result['data'][0]['nns_category'];
        //加载xml
        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->loadXML($depot_info);
        $category_id_arr = array();
        $categorys = $dom->getElementsByTagName('category');
        $flag = true;
        foreach ($categorys as $category)
        {
            //栏目名称和栏目父节点同时相同才认为存在该栏目，允许不同栏目级别下面的栏目名称相同
            $parent = $parent_id == '10000' ? 0 : $parent_id;
            if($category->getAttribute('name') == $category_name && $category->getAttribute('parent') == (string)$parent)
            {
                $flag = false;
            }
            //echo $category->getAttribute('name').'-----'.$category_name.'---------'.$category->getAttribute('parent').'-------'.$parent.'<br/>';
            if ($category->getAttribute('name') === $category_name && $category->getAttribute('parent') === (string)$parent)
            {
                $category_id = $category->getAttribute('id');
                $arr_cate = array(
                    'id' => $depot_id,
                    'category' => $category_id,
                    'org_id' => $org_id,
                    'name' => $category->getAttribute('name')
                );

                // 				if($depot_type == 0)
                // 				{
                // 				    $this->sync_category_cdn($arr_cate);
                // 				}
                return $arr_cate;
            }
            else
            {
                $category_id_arr[$category->getAttribute('id')] = $category->getAttribute('id');
            }
        }
        //exit('exit');
        $category_parent = $parent_id == 10000 ? 0 : $parent_id;
        $category_id = $parent_id * 1000;
        foreach ($category_id_arr as $key => $val)
        {

            if (strlen($category_id) != strlen($val))
            {
                unset($category_id_arr[$key]);
            }
        }
        //	echo $parent_id;exit;
        for ($i = 1; $i < 1000; $i++)
        {
            $category_id = $category_id + $i;
            if (!array_key_exists((string)$category_id, $category_id_arr))
            {
                break;
            }
        }
        //echo $category_id.'------------';
        //echo $category_parent;exit;
        //$category_parent = substr($category_id,0,strlen($category_id)-3);
        //创建栏目
        $bool = true;

        if ($category_parent != 0)
        {

            foreach ($categorys as $item)
            {
                if ($item->getAttribute("id") === $category_id)
                {
                    $bool = false;
                    break;
                }
            }
            if ($bool)
            {
                //			print_r($category);exit;
                foreach ($categorys as $item)
                {
                    //		 	 	echo $item->getAttribute("id").'---'.$category_parent;
                    //var_dump($item->getAttribute("id"));
                    //var_dump($category_parent);exit;
                    if ($item->getAttribute("id") === (string)$category_parent)
                    {
                        $item_new = $dom->createElement("category");
                        $item_new->setAttribute("id", $category_id);
                        $category_name = str_replace("<", "", $category_name);
                        $category_name = str_replace("/>", "", $category_name);
                        $category_name = str_replace("\"", "", $category_name);
                        $category_name = str_replace("'", "", $category_name);
                        $item_new->setAttribute("name", $category_name);
                        $item_new->setAttribute("parent", $category_parent);
                        //	    	 		echo '<pre>-----';
                        //	    	 		print_r($item_new);exit;
                        $item->appendChild($item_new);
                        break;
                    }
                }
            }
            else
                break;
        }
        else
        {
            foreach ($categorys as $item)
            {
                if ($item->getAttribute("id") === $category_id)
                {
                    $bool = false;
                    break;
                }
            }
            if ($bool)
            {
                $item_new = $dom->createElement("category");
                $item_new->setAttribute("id", $category_id);
                $item_new->setAttribute("name", $category_name);
                $item_new->setAttribute("parent", $category_parent);
                $dom->firstChild->appendChild($item_new);
            }
            else
                break;
        }
        $xml_data = $dom->saveXML();
        //	echo '<pre>';
        //	print_r($xml_data);exit;
        //修改栏目
        $result = $depot_inst->nns_db_depot_modify('', $xml_data, $org_type, $org_id, $depot_type);

        //修改栏目成功
        if ($result['ret'] == '0')
        {
            $arr_cate =  array(
                'id' => $depot_id,
                'category' => $category_id,
                'org_id' => $org_id,
                'name' => $category_name
            );
            if($depot_type == 0 && $flag && $this->fuse_enabled)
            {
                $this->sync_category_cdn($arr_cate);
            }
            return $arr_cate;
        }
        return false;
    }

    function change_spcial_word($param)
    {
        switch ($this->spcial_word)
        {
            default :
                $param = str_replace("'", "\'", $param);
                $param = str_replace('"', '\"', $param);
                return $param;
                break;
        }
    }
    /**
     * 修改媒资注入信息
     * @param $dc
     * @param $id 主键GUID
     * @param $params 修改信息 array()
     * @param $type 修改的类型
     */
    private function __update_other_data($dc, $id, $params, $type = 'video')
    {
        if ($id === FALSE || empty($id))
        {
            return FALSE;
        }
        $exist_key = array(
            'video' => array(
                'nns_cp_id',
                'nns_original_id',
                'nns_image0',
                'nns_image1',
                'nns_image2',
                'nns_image_h',
                'nns_image_s',
                'nns_image_v',
                'nns_play_count',
                'nns_tag',
                'nns_watch_focus',
                'nns_conf_info',
                'nns_ext_url',
            ),
            'index' => array(
                'nns_cp_id',
                'nns_release_time',
                'nns_original_id',
                'nns_update_time',
                'nns_image',
                'nns_play_count',
                'nns_watch_focus',
                'nns_drm_flag',
                'nns_isintact',
                'nns_conf_info',
                'nns_ext_url',
                'nns_director',
                'nns_actor',
            ),
            'media' => array(
                'nns_cp_id',
                'nns_original_id',
                'nns_ex_url',
                'nns_resolution',
                'nns_file_time_len',
                'nns_file_size',
                'nns_file_coding',
                'nns_drm_flag',
                'nns_drm_encrypt_solution',
                'nns_conf_info',
                'nns_ext_url',
            ),
            'live' => array(
                'nns_cp_id',
                'nns_image0',
                'nns_image1',
                'nns_image2',
                'nns_image_h',
                'nns_image_s',
                'nns_image_v',
            ),
            'stream' => array(
                'nns_import_source',
                'nns_asset_import_id',
                'nns_original_id'
            ),
            'asset'=>array(
                'nns_video_img0',
                'nns_video_img1',
                'nns_video_img2',
                'nns_image_h',
                'nns_image_s',
                'nns_image_v',
                'nns_tag'
            )
        );

        switch ($type)
        {
            case 'video':
                $table = 'nns_vod';
                break;
            case 'index':
                $table = 'nns_vod_index';
                break;
            case 'media':
                $table = 'nns_vod_media';
                break;
            case 'live':
                $table = 'nns_live';
                break;
            case 'stream':
                $table = 'nns_live_media';
                break;
            case 'asset':
                $table = 'nns_assists_item';
                break;
            default:
                return;
                break;
        }

        $set = '';
        foreach ($params as $key => $value)
        {
            //if (strlen($value) > 0)
            //{
            if (in_array($key, $exist_key[$type]))
                $set .= " {$key}='{$value}',";
            //}
        }

        if (empty($set))
            return;

        $set = rtrim($set, ',');
        if ($type == 'asset')
        {
            $sql = 'update ' . $table . ' set ' . $set . ' where nns_video_id=\'' . $id . '\'';
        }
        else
        {
            $sql = 'update ' . $table . ' set ' . $set . ' where nns_id=\'' . $id . '\'';
        }
        return nl_execute_by_db($sql, $dc->db());
    }
    /**
     * 当且仅当主媒资没有传递view_type字段时，
     * 根据配置以及主媒资的一级栏目
     * 获取主媒资的主分类，如电影、电视剧等计算一个主分类标识
     * @param $request_data 主媒资数据
     * @return 主媒资的主分类
     *  '电影' => 0,
     *	'电视剧' => 1,
     *	'综艺' => 2,
     *	'动漫' => 3,
     *	'音乐' => 4,
     *	'纪实' => 5
     * @author fuyuan.shuai
     * @date 2014-08-19
     */
    private function get_import_vod_view_type($request_data)
    {
        if (strlen($request_data['view_type']) > 0)
        {
            return $request_data['view_type'];
        }

        $category_label_map = array(
            '电影' => 0,
            '电视剧' => 1,
            '综艺' => 2,
            '动漫' => 3,
            '音乐' => 4,
            '纪实' => 5,
            '教育' => 6,
            '体育' => 7,
            '生活' => 8,
            '财经' => 9,
            '微电影' => 10,
            '品牌专区' => 11,
            '广告' => 12,
        );

        if (!is_array($request_data))
        {
            return 0;
        }

        if (isset($request_data["assets_category"]))
        {
            $category_key = $request_data['assets_category'];
            if (isset($category_label_map[$category_key]))
            {
                return $category_label_map[$category_key];
            }
        }

        return 0;
    }
}
?>
