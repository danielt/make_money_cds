<?php
/**
 * 
 * @access json server
 * @author chenbo@starcorcn.com
 * @deprecated version - 2012-9-12  user接口，提供给boss
 * 
 */
 define('ROOT_URL',dirname(dirname(dirname(dirname(__FILE__)))).DIRECTORY_SEPARATOR);
 include_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'interface_conf.php');
// include_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'json_rpc_server.php');
 include_once(ROOT_URL.'nn_cms_db/nns_debug_log/nns_debug_log_class.php');
 
 class cms_group extends Exception {
 	
	private $log;
 	private $group_inst;
 	function __construct(){
		//引入user DB
		include_once(ROOT_URL.'nn_cms_db/nns_group/nns_db_group_class.php');
		$this->group_inst = new nns_db_group_class();
 	}
	/**
	 +----------------------------------------------------------
	 * 维护集团信息
	 +----------------------------------------------------------
	 * @access public 
	 +----------------------------------------------------------
	 * @param array 集团信息数据
	 +----------------------------------------------------------
	 * @return array 处理结果
	 +----------------------------------------------------------
	 */
	public function maintainGroupInfo($group_info) {
		//验证groupId
		if(isset($group_info['groupId']) && !empty($group_info['groupId'])){
			$groupid = $group_info['groupId'];
		}else{
			$result_code    = '100001';
			$result_message = 'groupId,必选参数不能为空';
		}
		
		if(isset($result_code)){
			return $this->result($result_code,$result_message);
		}
		
		$groupname 			= $group_info['groupName'];
		$groupareacode 		= $group_info['areaCode'];
		$groupdescription 	= $group_info['description'];
		$grouptype 			= $group_info['type'];
	
		$result = $this->group_inst->nns_db_group_modify($groupid, $groupname, 0, $groupdescription, null, null, null, null, $groupareacode, $grouptype);
		$this->group_inst = null;
		if ($result["ret"] == 0) {
			$result_code    = '100000';
			$result_message = '维护集团信息成功';
		} else {
			$result_code 	= '100002';
			$result_message	='参数格式不正确。类型不正确或长度过长';
		}
	
		return $this->result($result_code,$result_message);
	}
	
 	/**
	 +----------------------------------------------------------
	 * 删除集团
	 +----------------------------------------------------------
	 * @access public 
	 +----------------------------------------------------------
	 * @param  array $group_info
	 +----------------------------------------------------------
	 * @return array 
	 +----------------------------------------------------------
	 */
	 public function deleteGroup($group_info) {
		//验证groupId
		if(isset($group_info['groupId']) && !empty($group_info['groupId'])){
			$groupid = $group_info['groupId'];
		}else{
			$result_code    = '100001';
			$result_message = 'groupId,必选参数不能为空';
		}
		
		if(isset($result_code)){
			return $this->result($result_code,$result_message);
		}
		
		$result = $this->group_inst->nns_db_group_delete($groupid);
		$this->group_inst = null;
		if ($result["ret"] == 0) {
			$result_code = "100000";
			$result_message="删除集团信息成功";
		} else {
			$result_code = "100005";
			$result_message="集团不存在";
		}
		
		return $this->result($result_code,$result_message);
	}
 	
 	/**
	 +----------------------------------------------------------
	 * 返回格式构造
	 +----------------------------------------------------------
	 * @access private 
	 +----------------------------------------------------------
	 * @param $code,$message,$data
	 +----------------------------------------------------------
	 * @return array 
	 +----------------------------------------------------------
	 */
 	private function result($code,$message,$data=null){
 		//是否成功
		$result['result'] = $code == '100000' ? 0 : 1;
		//根据是否成功判断返回状态码还是返回数据
		empty($data) ? $result['code'] = $code : $result['data'] = $data;
		//返回接口处理结果提示
		$result['message'] = $message;
		
 		return $result;
 	}
 }
?>