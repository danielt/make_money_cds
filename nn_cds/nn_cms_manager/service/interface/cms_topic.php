<?php
/**
 * 
 * @access interface data model
 * @author bo.chen@starcorcn.com
 * @deprecated version - 2012-11-20  
 */
 
define('ROOT_URL',dirname(dirname(dirname(dirname(__FILE__)))).DIRECTORY_SEPARATOR);
include_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'interface_conf.php');
include ROOT_URL. "nn_logic/topic/topic_item_image.class.php";
include ROOT_URL. "nn_logic/topic/topic_item.class.php";

//引入图片上传
$nncms_config_path = ROOT_URL;
include_once ROOT_URL . "nn_cms_manager/controls/nncms_controls_public_function.php";

class cms_topic extends Exception{
	private $log;
	private $dc;
 	private $topic_inst;
 	function __construct(){
		//实例化DB
		$this->dc = nl_get_dc(array(
			'db_policy'=>NL_DB_READ,
			'cache_policy'=>NP_KV_CACHE_TYPE_NULL
			)
		);
		$this->dc->open();
 	}
   	/**
	 * 新增信息导入接口方法
	 */
	function api_add_topic_item($topic_item_info,$topic_item_files){
		
		//封面图片处理
		$cover_url = '';
		$id=np_guid_rand();
		if(isset($topic_item_files['cover'])){
			
			$cover_url = pub_func_image_upload_by_topic_photo($topic_item_files['cover'],$topic_item_info['topic_id'],$id);
			if(empty($cover_url)){
				//exit('标题为  '.$topic_item_info['title'].' 图片处理失败,请重新上传!');
				$result = array(
					'result' => '0001',
					'message' =>'标题为  '.$topic_item_info['title'].' 图片处理失败,请重新同步数据!',
				);
				return $result;
			}
		}
		
		//销毁封面图片
		unset($topic_item_files['cover']);

		
		//替换内容中的图片地址,暂时
		$content = $topic_item_info['content'];

		//数据入库
		$param = array(
			'id'=>$id,
			'title' => $topic_item_info['title'],
			'subtitle' => $topic_item_info['subtitle'],
			'keyword' => $topic_item_info['keyword'],
			'tag' => $topic_item_info['tag'],
			'summary' => $topic_item_info['summary'],
			'link_url' => $topic_item_info['link_url'],
			'link_type' => $topic_item_info['link_type'],
			'source' => $topic_item_info['source'],
			'author'=>$topic_item_info['author'],
			'content' => $content,
			'image' => $cover_url,
		);
		$topic_id = $topic_item_info['topic_id'];
		$category_id = $topic_item_info['category_id'];
		$topic_item_id = nl_topic_item::add_topic_item($this->dc,$topic_id,$category_id,$param);
		if(empty($topic_item_id)){
			$result = array(
				'result' => '0002',
				'message' => '标题为  '.$topic_item_info['title'].' 数据入库失败，请重新同步数据',
			);
			return $result;
		}
		// 处理相册列表
		if(!empty($topic_item_files['image'])){
			foreach($topic_item_files['image'] as $key=>$val){
				$image_url = pub_func_image_upload_by_topic($val,'image_list');
				if($image_url){
					$param = array(
						'image'=>$image_url,
						'summary'=>'',
						'topic_item_id'=>$topic_item_id,
					);
			
					$topic_item_image_id = nl_topic_item_image::add_topic_item_image($this->dc,$topic_item_id,$param);
					if(empty($topic_item_image_id)){ 
						$result = array(
							'result' => '0004',
							'message' => '标题为  '.$topic_item_info['title'].'相册图片入库失败',
						);
						return $result;
					}
					
				}else{
					$result = array(
						'result' => '0003',
						'message' => '标题为  '.$topic_item_info['title'].' 相册图片处理失败，请重新同步数据',
					);
					return $result;
				}
			}
		}
		
		//处理成功
		$result = array(
			'result' => '0000',
			'message' => '数据同步成功',
		);
		
		return $result;
	}
	/**
	 * 信息相册图片导入接口
	 */
	function api_add_topic_item_image($topic_item_image,$topic_item_image_files){
		
		$topic_item_id = $topic_item_image['topic_item_id'];
		/***
		 * 下面这段代码要改 
		 * pub_func_image_upload_by_topic_photo应传参数 
		 * 1. 图片
		 * 2. 信息包ID
		 * 3. 信息包ITEM ID 
		 * 
		 * BY S67
		 *
		 */
		$image_url = pub_func_image_upload_by_topic_photo($topic_item_image_files['image'],'image',$topic_item_id);
		if(empty($image_url)){
			//exit('标题为  '.$topic_item_info['title'].' 图片处理失败,请重新上传!');
			$result = array(
				'result' => '0001',
				'message' =>'描述为  '.$topic_item_image['summary'].' 图片处理失败,请重新同步数据!',
			);
			return $result;
		}
		$param = array(
			'image'=>$image_url,
			'summary'=>$topic_item_image['summary'],
			'topic_item_id'=>$topic_item_id,
		);

		$topic_item_image_id = nl_topic_item_image::add_topic_item_image($this->dc,$topic_item_id,$param);
		if(empty($topic_item_image_id)){
			$result = array(
				'result' => '0002',
				'message' => '描述为  '.$topic_item_image['summary'].' 数据入库失败，请重新同步数据',
			);
		}else{
			$result = array(
				'result' => '0000',
				'message' => '数据同步成功',
			);
		}
		return $result;
	}
}
?>