<?php
/**
 * 
 * @access json client
 * @author chenbo@starcorcn.com
 * @deprecated version - 2012-9-10  user接口，提供给boss
 * 
 */
 define('ROOT_URL',dirname(dirname(dirname(dirname(__FILE__)))).DIRECTORY_SEPARATOR);
 include_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'interface_conf.php');
 include_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'json_rpc_server.php');
 include_once(ROOT_URL.'nn_cms_db/nns_debug_log/nns_debug_log_class.php');
 include_once(ROOT_URL.'nn_cms_manager/controls/nncms_control_memcache.php');
 
 class cms_product {
 	
	private $log;
 	private $product_inst;
 	private $product_fee_inst;
 	function __construct(){
		//引入product DB
		include_once(ROOT_URL.'nn_cms_db/nns_products/nns_db_products_class.php');
		//引入product fee DB
		include_once(ROOT_URL.'nn_cms_db/nns_product_fee/nns_db_product_fee_class.php');
		
		$this->product_inst = new nns_db_products_class();
		
		$this->product_fee_inst = new nns_db_product_fee_class();
 	}
 	/**
	 +----------------------------------------------------------
	 * 维护产品资费 
	 +----------------------------------------------------------
	 * @access private 
	 +----------------------------------------------------------
	 * @param array 产品资费信息数据
	 +----------------------------------------------------------
	 * @return array 处理结果
	 +----------------------------------------------------------
	 */
 	public function maintainProductFee($product_info){

		$productId = $product_info['productId'];
		$goodList = $product_info['productFeeInfo'];
		$product_result = $this->product_inst->nns_db_products_info($productId);

		if ($product_result["ret"] == 0) {

			$this->product_inst->nns_db_products_check($productId,1);
			
			if (is_array($goodList)) {
				foreach ($goodList as $goodInfo) {

					$amount 			= $goodInfo['amount'];
					$user_tariff_type 	= $goodInfo['userTariffType'];
					$beginDateTime 		= $goodInfo['beginDateTime'];
					$currencyType 		= $goodInfo['currencyType'];
					$endDateTime 		= $goodInfo['endDateTime'];
					$explanation 		= $goodInfo['explanation'];
					$gatheringMode 		= $goodInfo['gatheringMode'];
					$goodsId 			= $goodInfo['goodsId'];
					$goodsName 			= $goodInfo['goodsName'];
					$price 				= $goodInfo['price'];
					$priceType 			= $goodInfo['priceType'];
					$priceunit 			= $goodInfo['priceunit'];
					$unit 				= $goodInfo['unit'];

					$gxgd_product_result = $this->product_fee_inst->nns_db_product_fee_add($productId, $goodsId, $user_tariff_type, $goodsName, $price, $unit, $priceType, $amount, $priceunit, $gatheringMode, $currencyType, $beginDateTime, $endDateTime, $explanation);
					if ($gxgd_product_result["ret"] == 0) {
						$result_code 	= '100000';
						$result_message	='产品资费信息维护成功';
					}else{
						$result_code 	= '100002';
						$result_message	='参数格式不正确。类型不正确或长度过长';
					}
				}
			}
			$this->product_inst = null;

		} else {
			$result_code 	= '100006';
			$result_message	='相关产品不存在';
		}
		$this->product_inst = null;
		return $this->result($result_code,$result_message);
 	}
 	/**
	 +----------------------------------------------------------
	 * 删除产品资费
	 +----------------------------------------------------------
	 * @access public 
	 +----------------------------------------------------------
	 * @param array 产品资费数据
	 +----------------------------------------------------------
	 * @return array 处理结果
	 +----------------------------------------------------------
	 */
	public function deleteProductFee($product_info) {
		//验证productd
		if(isset($product_info['productId']) && !empty($product_info['productId'])){
			$productId 	= $product_info['productId'];	
		}else{
			$result_code    = '100001';
			$result_message = 'productId,必选参数不能为空';
		}
		//验证goodsId
		if(isset($product_info['goodsId']) && !empty($product_info['goodsId'])){
			$goodsId 	= $product_info['goodsId'];	
		}else{
			$result_code    = '100001';
			$result_message = 'goodsId,必选参数不能为空';
		}
		
		if(isset($result_code)){
			return $this->result($result_code,$result_message);
		}
	
		$result = $this->product_fee_inst->nns_db_product_fee_delete($productId, $goodsId);
		if ($result["ret"] == 0) {
			$result_code 	= '100000';
			$result_message = '资费信息删除成功';
		} else {
			$result_code 	= '100007';
			$result_message = '产品Id号格式非法';
		}
	
		//返回格式构造
		return $this->result($result_code,$result_message);
		
	}
	
	/**
	 +----------------------------------------------------------
	 * 维护产品授权接口
	 +----------------------------------------------------------
	 * @access public 
	 +----------------------------------------------------------
	 * @param array 产品授权信息
	 +----------------------------------------------------------
	 * @return array 处理结果
	 +----------------------------------------------------------
	 */
	 
	 public function authorization($authorizationInfo) {
		
		//验证avail_date_time
		if(isset($authorizationInfo['availDateTime']) && !empty($authorizationInfo['availDateTime'])){
			$availDateTime 	= $authorizationInfo['availDateTime'];	
		}else{
			$result_code    = '100001';
			$result_message = 'availDateTime,必选参数不能为空';
		}
		
		//验证invalidation_time
		if(isset($authorizationInfo['invalidationTime']) && !empty($authorizationInfo['invalidationTime'])){
			$invalidationTime 	= $authorizationInfo['invalidationTime'];	
		}else{
			$result_code    = '100001';
			$result_message = 'invalidationTime,必选参数不能为空';
		}
		
		//验证productId
		if(isset($authorizationInfo['productId']) && !empty($authorizationInfo['productId'])){
			$productId 	= $authorizationInfo['productId'];	
		}else{
			$result_code    = '100001';
			$result_message = 'productId,必选参数不能为空';
		}
		
		//验证userId
		if(isset($authorizationInfo['userId']) && !empty($authorizationInfo['userId'])){
			$userId 	= $authorizationInfo['userId'];	
		}else{
			$result_code    = '100001';
			$result_message = 'userId,必选参数不能为空';
		}
		//验证productNumber
		if(isset($authorizationInfo['productNumber']) && !empty($authorizationInfo['productNumber'])){
			$productNumber 	= $authorizationInfo['productNumber'];
		}else{
			$result_code    = '100001';
			$result_message = 'productNumber,必选参数不能为空';
		}
		//验证productUnit
		if(isset($authorizationInfo['productUnit']) && !empty($authorizationInfo['productUnit'])){
			$productUnit 	= $authorizationInfo['productUnit'];
		}else{
			$result_code    = '100001';
			$result_message = 'productUnit,必选参数不能为空';
		}
		//验证goodsId
		if(isset($authorizationInfo['goodsId']) && !empty($authorizationInfo['goodsId'])){
			$goodsId 	= $authorizationInfo['goodsId'];
		}else{
			$result_code    = '100001';
			$result_message = 'goodsId,必选参数不能为空';
		}
		
		//如果参数有错，返回错误
		if(isset($result_code)){
			return $this->result($result_code,$result_message);
		}
		
		require_once(ROOT_URL.'nn_cms_db/nns_user/nns_db_user_class.php');
		$user_inst = new nns_db_user_class();
		$count_inst = $user_inst->nns_db_user_count($userId);
		$user_inst = null;
		if ($count_inst["data"][0]["num"] > 0) {
			
			$count_inst = $this->product_inst->nns_db_products_count(null, $productId);
			$product_inst = null;
			if ($count_inst["data"][0]["num"] > 0) {
				require_once(ROOT_URL.'nn_cms_db/nns_products/nns_db_user_products_class.php');
				$product_user_inst = new nns_db_user_products_class();
				$result = $product_user_inst->nns_db_user_products_add($userId, $productId, $availDateTime, $invalidationTime, 0, 0, $productNumber, $productUnit, $goodsId);
				$product_user_inst = null;
				if ($result["ret"] == 0) {
					delete_cache("product_".$userId);
					$result_code = "100000";
					$result_message = '产品授权成功';
				}else{
					$result_code = "100011";
					$result_message = '产品授权失败';
				}

			} else {
				$result_code = '100006';
				$result_message = '产品不存在';
			}
		} else {
			$result_code = '100004';
			$result_message = '用户不存在';
		}
		//返回处理结果
		return $this->result($result_code,$result_message);
		
	}
	// 	删除产品授权接口
	public function deleteAuthorization($deleteAuthorization) {
		
		//验证productId
		if(isset($deleteAuthorization['productId']) && !empty($deleteAuthorization['productId'])){
			$productId 	= $deleteAuthorization['productId'];	
		}else{
			$result_code    = '100001';
			$result_message = 'productId,必选参数不能为空';
		}
		//验证userId
		if(isset($deleteAuthorization['userId']) && !empty($deleteAuthorization['userId'])){
			$userId 	= $deleteAuthorization['userId'];	
		}else{
			$result_code    = '100001';
			$result_message = 'userId,必选参数不能为空';
		}
		
		//如果参数有错，返回错误
		if(isset($result_code)){
			return $this->result($result_code,$result_message);
		}

		require_once(ROOT_URL.'nn_cms_db/nns_products/nns_db_user_products_class.php');
		$product_user_inst = new nns_db_user_products_class();
		$count_arr = $product_user_inst->nns_db_user_products_count($userId, $productId);
		if ($count_arr["data"][0]["num"] > 0) {
			$result = $product_user_inst->nns_db_user_products_delete(null, $userId, $productId);
			$product_user_inst = null;
			delete_cache("product_".$userId);
			if ($result["ret"] == 0) {

				$result_code = '100000';
				$result_message = '删除产品授权成功';
			}else{
				$result_code = '100012';
				$result_message = '删除产品授权失败';
			}
		} else {
			$result_code = "100013";
			$result_message = '用户没有该产品的授权';
		}
	
		return $this->result($result_code,$result_message);
	}
 	
 	/**
	 +----------------------------------------------------------
	 * 返回格式构造
	 +----------------------------------------------------------
	 * @access private 
	 +----------------------------------------------------------
	 * @param $code,$message,$data
	 +----------------------------------------------------------
	 * @return array 
	 +----------------------------------------------------------
	 */
 	private function result($code,$message,$data=null){
 		//是否成功
		$result['result'] = $code == '100000' ? 0 : 1;
		//根据是否成功判断返回状态码还是返回数据
		empty($data) ? $result['code'] = $code : $result['data'] = $data;
		//返回接口处理结果提示
		$result['message'] = $message;
		
 		return $result;
 	}
 }
?>