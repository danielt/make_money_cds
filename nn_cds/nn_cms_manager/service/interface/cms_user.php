<?php
/**
 * 
 * @access json interface
 * @author chenbo@starcorcn.com
 * @deprecated version - 2012-9-10  user接口，提供给boss
 * 
 */
 define('ROOT_URL',dirname(dirname(dirname(dirname(__FILE__)))).DIRECTORY_SEPARATOR);
 include_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'interface_conf.php');
// include_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'json_rpc_server.php');
 include_once(ROOT_URL.'nn_cms_db/nns_debug_log/nns_debug_log_class.php');
 include_once(ROOT_URL.'nn_cms_manager/controls/nncms_control_memcache.php');
 
 class cms_user extends Exception {
 	
	private $log;
 	private $user_inst;
 	function __construct(){
		//引入user DB
		include_once(ROOT_URL.'nn_cms_db/nns_user/nns_db_user_class.php');
		$this->user_inst = new nns_db_user_class();
 	}
 	/**
	 +----------------------------------------------------------
	 * 维护用户信息
	 +----------------------------------------------------------
	 * @access private 
	 +----------------------------------------------------------
	 * @param array 用户信息数据
	 +----------------------------------------------------------
	 * @return array 处理结果
	 +----------------------------------------------------------
	 */
 	public function maintainUserInfo($user_info){
		//验证userId
		if(isset($user_info['userId']) && !empty($user_info['userId'])){
			$nns_id = $user_info['userId'];	
		}else{
			$result_code    = '100001';
			$result_message = 'userId,必选参数不能为空';
		}
		//验证IP地址
		if(isset($user_info['ip']) && !empty($user_info['ip'])){
			$nns_ip = $user_info['ip'];	
		}else{
			$result_code    = '100001';
			$result_message = 'ip,必选参数不能为空';
		}
		//验证userName
		if(isset($user_info['userName']) && !empty($user_info['userName'])){
			$nns_name = $user_info['userName'];	
		}else{
			$result_code    = '100001';
			$result_message = 'userName,必选参数不能为空';
		}
		//验证setTopBoxId（机顶盒编号）
		if(isset($user_info['setTopBoxId']) && !empty($user_info['setTopBoxId'])){
			$nns_deveice_id = $user_info['setTopBoxId'];
		}else{
			$result_code    = '100001';
			$result_message = 'setTopBoxId,必选参数不能为空';
		}
		if(isset($result_code)){
			return $this->result($result_code,$result_message);
		}
		
		$nns_pass = $user_info['userPassword'];
		$nns_group_id = $user_info['groupCustomerId'];
		$nns_phone = $user_info['phoneNumber'];
		
		$nns_topbox_bind = $user_info['setTopBoxIdBindFlag'];
		$nns_stb_type = empty($user_info['stbType'])?'1000':trim($user_info['stbType']);
		
		$nns_mac = $user_info['mac'];
		$area_code = $user_info['areaCode'];
		$smart_card = $user_info['smartCardId'];
		
		if (empty ($nns_group_id)) {
			require_once (ROOT_URL.'nn_cms_db/nns_carrier/nns_db_carrier_class.php');
			$carrier_inst = new nns_db_carrier_class();
			
			$group_type = 0;
			$carrier_result = $carrier_inst->nns_db_carrier_list();
			$carrier_inst = null;
			$nns_group_id = $carrier_result["data"][0]["nns_id"];
		} else {
			$group_type = 2;
			//绑定集团
			require(ROOT_URL.'nn_cms_db/nns_user/nns_db_user_group_class.php');
			$model = new nns_db_user_group_class();
			$model->bind_group($nns_id,$nns_group_id);

		}

		$result = $this->user_inst->nns_db_user_modify($nns_id, $nns_name, $nns_pass, $group_type, $nns_group_id, 1, null, null, null, null, $nns_phone, null, null, (object)$user_info);
		$this->user_inst = null;
		
		if ($result["ret"] == 0) {
			if (strtolower($nns_topbox_bind) == "yes") {
				include_once(ROOT_URL.'nn_cms_db/nns_device/nns_db_device_class.php');
				$device_inst = new nns_db_device_class();
				$result = $device_inst->nns_db_device_update($nns_deveice_id, $nns_name, $nns_stb_type, $group_type, $nns_group_id, $nns_id, $nns_ip, null, 0, null, null, null, $nns_mac, $area_code, $smart_card);
				delete_cache("device_".$nns_deveice_id);
			}
			$result_code = '100000';
			$result_message="维护用户信息成功";
			delete_cache("user_".$nns_id);
		}else{
			$result_code = '100002';
			$result_message='参数格式不正确。类型不正确或长度过长';
		}
		
		return $this->result($result_code,$result_message);
		
		
		
 	}
 	/**
	 +----------------------------------------------------------
	 * 删除user
	 +----------------------------------------------------------
	 * @access public 
	 +----------------------------------------------------------
	 * @param array 用户信息数据
	 +----------------------------------------------------------
	 * @return array 处理结果
	 +----------------------------------------------------------
	 */
	public function deleteUser($user_info) {
		if(isset($user_info['userId']) && !empty($user_info['userId'])){
			
			$result = $this->user_inst->nns_db_user_delete($user_info['userId']);
			$this->user_inst = null;
			if ($result["ret"] == 0) {
				$result_code 	= '100000';
				$result_message ='销户成功';
			} else {
				$result_code  	= '100004';
				$result_message	='指定用户不存在';
			}
		} else {
			$result_code 	= '100001';
			$result_message	= '用户ID为空,必选参数不能为空';
		}
		//返回格式构造
		return $this->result($result_code,$result_message);
		
	}
	
	/**
	 +----------------------------------------------------------
	 * user 绑定集团
	 +----------------------------------------------------------
	 * @access public 
	 +----------------------------------------------------------
	 * @param array 用户信息数据
	 +----------------------------------------------------------
	 * @return array 处理结果
	 +----------------------------------------------------------
	 */
	public function maintainUserGroup($user_info){;
			//验证userId
			if(isset($user_info['userId']) && !empty($user_info['userId'])){
				$userId = $user_info['userId'];	
			}else{
				$result_code    = '100001';
				$result_message = 'userId,必选参数不能为空';
			}
			//验证hroupId地址
			if(isset($user_info['groupId']) && !empty($user_info['groupId'])){
				$groupId = $user_info['groupId'];	
			}else{
				$result_code    = '100001';
				$result_message = 'groupId,必选参数不能为空';
			}
			//检查用户是否有效
			$result = $this->user_inst->nns_db_user_info($userId);
			if($result['ret'] != 0){
				$result_code  	= '100004';
				$result_message	='指定用户不存在';
			}
			//检查集团是否有效
			require(ROOT_URL.'nn_cms_db/nns_group/nns_db_group_class.php');
			$group_inst = new nns_db_group_class();
			$result = $group_inst->nns_db_group_info($groupId);
			if($result['ret'] != 0){
				$result_code  	= '100008';
				$result_message	='指定集团不存在';
			}
			if(isset($result_code)){
				//返回格式构造
				return $this->result($result_code,$result_message);
			}
			require_once ROOT_URL.'nn_cms_db/nns_user/nns_db_user_group_class.php';
			$user_group_model= new nns_db_user_group_class();
			$result = $user_group_model->bind_group($userId,$groupId);
			if($result['ret'] != 0){
				$result_code  	= '100009';
				$result_message	='用户绑定集团失败';
			}else{
				$result_code  	= '100000';
				$result_message	='用户绑定集团成功';
			}
			//返回格式构造
			return $this->result($result_code,$result_message);
	}
	/**
	 +----------------------------------------------------------
	 * user  解绑集团接口
	 +----------------------------------------------------------
	 * @access public 
	 +----------------------------------------------------------
	 * @param array 用户信息数据
	 +----------------------------------------------------------
	 * @return array 处理结果
	 +----------------------------------------------------------
	 */

	public function deleteUserGroup($user_info){
			//验证userId
			if(isset($user_info['userId']) && !empty($user_info['userId'])){
				$userId = $user_info['userId'];	
			}else{
				$result_code    = '100001';
				$result_message = 'userId,必选参数不能为空';
			}
			//验证groupId地址
			if(isset($user_info['groupId']) && !empty($user_info['groupId'])){
				$groupId = $user_info['groupId'];	
			}else{
				$result_code    = '100001';
				$result_message = 'groupId,必选参数不能为空';
			}
			//判断是否有错误
			if(isset($result_code)){
				
				return $this->result($result_code,$result_message);
			}
			
			require(ROOT_URL.'nn_cms_db/nns_user/nns_db_user_group_class.php');
			$group_inst = new nns_db_user_group_class();
			$result = $group_inst->unbind_group($userId,$groupId);
			
			if($result['ret'] != 0){
				$result_code  	= '100010';
				$result_message	='用户解绑集团失败';
			}else{
				$result_code  	= '100000';
				$result_message	='取消用户绑定集团成功';
			}
			//返回格式构造
			return $this->result($result_code,$result_message);
	}
 	
 	/**
	 +----------------------------------------------------------
	 * 返回格式构造
	 +----------------------------------------------------------
	 * @access private 
	 +----------------------------------------------------------
	 * @param $code,$message,$data
	 +----------------------------------------------------------
	 * @return array 
	 +----------------------------------------------------------
	 */
 	private function result($code,$message,$data=null){
 		//是否成功
		$result['result'] = $code == '100000' ? 0 : 1;
		//根据是否成功判断返回状态码还是返回数据
		empty($data) ? $result['code'] = $code : $result['data'] = $data;
		//返回接口处理结果提示
		$result['message'] = $message;
		
 		return $result;
 	}
 }
?>