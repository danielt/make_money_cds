<?php
include_once($np_path.'np/np_mlang.class.php');
include_once $nncms_config_path. "nn_cms_config/nn_cms_global.php";
function cms_get_lang($key,$lang=null){
	global $nncms_config_path;
	static $mlang = null;
	
	$lang_path = g_cms_config::get_g_config_value('g_lang_path');
	$lang_order = g_cms_config::get_g_config_value('g_lang_order');
	if(empty($lang)){
		$lang = isset($_SESSION["lang_dir"])?$_SESSION["lang_dir"]:'';
	}
	//$lang = 'en_US';
	if(!empty($lang)){
		foreach($lang_order as $k=>$v){
			if($v == $lang ){
				$lang_order[$k] = $lang_order['0'];
				$lang_order['0'] = $v;
			}
		}
	}
	//实例化语言包对象
	if(!$mlang instanceof np_mlang_class){
		$mlang = new np_mlang_class($nncms_config_path);
		$mlang_load = $mlang->load($lang_path,$lang_order);
	}
	$mlang_value = $mlang->get_lang($key,$lang);

	return $mlang_value;
}
?>