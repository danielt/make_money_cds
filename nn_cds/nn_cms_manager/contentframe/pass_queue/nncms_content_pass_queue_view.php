<?php
if($_GET['header_type'] == 'xml')
{
	header("Content-Type:text/xml;charset=utf-8");
}
else 
{
	header("Content-Type:text/html;charset=utf-8");
}
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"]))
{
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
include_once $nncms_config_path . "nn_logic/pass_queue/pass_queue.class.php";

$dc = nl_get_dc(array(
		'db_policy' => NL_DB_READ | NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$dc->open();
$pass_queue = nl_pass_queue::query_by_id($dc, $_GET['id']);
global $g_bk_version_number;
$bk_version = $g_bk_version_number == '1' ? true : false;
unset($g_bk_version_number);
if ($bk_version)
{
    $pass_queue['data_info'][0]['nns_content'] = html_entity_decode($pass_queue['data_info'][0]['nns_content']);
}
echo $pass_queue['data_info'][0]['nns_content'];