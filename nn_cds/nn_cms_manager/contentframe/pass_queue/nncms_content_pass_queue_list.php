<?php 
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) 
{
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
include_once $nncms_config_path . "nn_logic/pass_queue/pass_queue.class.php";
include_once $nncms_config_path . "nn_logic/cp/cp.class.php";
include_once $nncms_config_path . 'mgtv_v2/callback_notify/import_queue.class.php';
include $nncms_config_path . "v2/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_core.m_public");
\ns_core\m_load::load("ns_model.pass_queue.pass_queue");

$dc = nl_get_dc(array(
       	'db_policy' => NL_DB_READ | NL_DB_WRITE,
        'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$dc->open();
$sp_id = $_GET['sp_id'];
global $g_bk_version_number;
$bk_version = $g_bk_version_number == '1' ? true : false;
unset($g_bk_version_number);
//播控v2操作
if($bk_version && isset($_REQUEST['op']) && !empty($_REQUEST['op']))
{
    if($_REQUEST['op'] == 'retry')
    {
        \m_config::$flag_operation = false;
        $log_timer_path = \m_config::return_child_path($nncms_config_path . "v2/ns_timer/delivery/delivery_pass_queue.timer.php", 'ns_timer');

        $ids = array_filter(array_unique(explode(',', isset($_REQUEST['ids']) ? $_REQUEST['ids'] : '')));
        if (!is_array($ids) || empty($ids))
        {
            echo "<script>alert('传入查询ID为空');history.go(-1);</script>";
            die;
        }

        //获取透传队列指令队列执行任务
        $result_task = nl_pass_queue::query_by_condition(\m_config::get_dc(), array('nns_id' => $ids));

        if ($result_task['ret'] != NS_CDS_SUCCE)
        {
            echo "<script>alert('数据库查询数据失败');history.go(-1);</script>";
            die;
        }
        if (empty($result_task['data_info']) || !is_array($result_task['data_info']))
        {
            echo "<script>alert('数据库查询无数据');history.go(-1);</script>";
            die;
        }
        \ns_core\m_load::load("ns_model.delivery.pass_queue_delivery_explain");
        $explain_inst = new \ns_model\delivery\pass_queue_delivery_explain($sp_id);
        foreach ($result_task['data_info'] as $task)
        {
            //实例化底层数据基础类
            $pass_queue = new ns_model\pass_queue\pass_queue($sp_id);
            //过滤注入数据
            $pass_queue_explain = $pass_queue->explain();
            if ($pass_queue_explain['ret'] != NS_CDS_SUCCE)
            {
                \m_config::timer_write_log($log_timer_path, var_export($pass_queue_explain['reason'], true), $task['nns_type']);
                continue;
            }

            //封装注入的消息
            $task['nns_content'] = html_entity_decode($task['nns_content']);//反格式化
            $message_arr['pass_queue_task_info'] = $task;

            m_config::write_pass_queue_execute_log("***********开始执行透传队列指令注入".var_export($message_arr['data_info'][0],true), $sp_id, $task['nns_id']);
            $explain_inst->init($message_arr);
            $explain_result = $explain_inst->explain();
            $explain_inst->destroy_init();
            m_config::write_pass_queue_execute_log("***********执行透传队列结果：".var_export($explain_result,true), $sp_id, $task['nns_id']);
        }
        if(!isset($explain_result['reason']) || empty($explain_result['reason']))
        {
            $explain_result['reason'] = $explain_result['ret'] != NS_CDS_SUCCE ? '操作失败' : '操作成功';
        }
        echo "<script>alert('" . $explain_result['reason'] . "');history.go(-1);</script>";
        die;
    }
}


if(isset($_REQUEST['op']) && !empty($_REQUEST['op']))
{
	if($_REQUEST['op'] == 'retry')
	{
		$arr_id = array_filter(array_unique(explode(',', isset($_REQUEST['ids']) ? $_REQUEST['ids'] : '')));
		if(empty($arr_id) || !is_array($arr_id))
		{
			echo '<script>alert("操作成功");history.go(-1);</script>';die;
		}
		$flag = ($_REQUEST['mssage'] == 'import') ? true : false;
		$arr_sp = nl_sp::query_by_id($dc, $sp_id);
		if(!isset($arr_sp['data_info'][0]['nns_config']) || empty($arr_sp['data_info'][0]['nns_config']))
		{
			echo '<script>alert("操作成功");history.go(-1);</script>';die;
		}
		$sp_config = json_decode($arr_sp['data_info']['0']['nns_config'],true);
		if (!isset($sp_config['pass_queue_disabled']) || (int)$sp_config['pass_queue_disabled'] == 0)
		{
			echo '<script>alert("透传队列开关已关闭");history.go(-1);</script>';die;
		}
		#TODO
		if($flag)
		{
			include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/mgtv_init.php';
			include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/' . $sp_id . '/init.php';
			$sp_config['nns_id'] = $sp_id;
			foreach ($arr_id as $val)
			{
				$result = nl_pass_queue::query_by_id($dc,$val);
				if(!isset($result['data_info'][0]) || $result['data_info'][0]['nns_audit'] != '1' || $result['data_info'][0]['nns_status'] == '2' || $result['data_info'][0]['nns_pause'] == '1')
				{
					continue;
				}
                if ($bk_version)
                {
                    $result['data_info'][0]['nns_content'] = html_entity_decode($result['data_info'][0]['nns_content']);
                }
				$import_queue = new import_queue($dc);
				$import_queue->import_pass_queue($result['data_info'][0],$sp_config);
			}
		}
		else
		{
			#TODO
		}
		echo '<script>alert("操作成功");history.go(-1);</script>';die;
	}
	else if($_REQUEST['op'] == 'do_audit')
	{
		$arr_id = array_filter(array_unique(explode(',', isset($_REQUEST['ids']) ? $_REQUEST['ids'] : '')));
		if(empty($arr_id) || !is_array($arr_id))
		{
			echo '<script>alert("操作成功");history.go(-1);</script>';die;
		}
		if(!in_array($_REQUEST['nns_audit'], array(1,2)))
		{
			echo '<script>alert("参数错误");history.go(-1);</script>';die;
		}
		$result = nl_pass_queue::modify($dc,array('nns_audit'=>$_REQUEST['nns_audit']),array('in'=>array('nns_id' => $arr_id),'nns_status'=>1));
		echo '<script>alert("'.$result['reason'].'");history.go(-1);</script>';die;
	}
	else if($_REQUEST['op'] == 'do_pause')
	{
		$arr_id = array_filter(array_unique(explode(',', isset($_REQUEST['ids']) ? $_REQUEST['ids'] : '')));
		if(empty($arr_id) || !is_array($arr_id))
		{
			echo '<script>alert("操作成功");history.go(-1);</script>';die;
		}
		if(!in_array($_REQUEST['nns_pause'], array(0,1)))
		{
			echo '<script>alert("参数错误");history.go(-1);</script>';die;
		}
		$result = nl_pass_queue::modify($dc,array('nns_pause'=>$_REQUEST['nns_pause']),array('in'=>array('nns_id' => $arr_id),'nns_status'=>1,'nns_audit'=>1));
		echo '<script>alert("'.$result['reason'].'");history.go(-1);</script>';die;
	}
}
$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : (!empty($_COOKIE["page_max_num"]) ?  $_COOKIE["page_max_num"] : 18);
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) 
{
	$page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit = $page_size;
$filter = null;
$filter['nns_org_id'] = $sp_id;
if(isset($_GET['action_op']))
{
	switch ($_GET['action_op'])
	{
		case 'search':
			$filter['nns_message_id'] = $_GET['nns_message_id'];
			$filter['nns_status'] = $_GET['nns_status'];
			$filter['nns_content'] = $_GET['nns_content'];			
			$filter['nns_cp_id'] = $_GET['nns_cp_id'];
			$filter['nns_audit'] = $_GET['nns_audit'];
			$filter['nns_pause'] = $_GET['nns_pause'];
			$filter['nns_type'] = $_GET['nns_type'];
			break;
	}
}
//获取CP

$result = nl_pass_queue::get_pass_queue($dc, $filter, $offset, $limit,true);
$cp_re = nl_cp::query_all($dc);
if($cp_re['ret'] == 0 && is_array($cp_re['data_info']))
{
	$cp_arr = $cp_re['data_info'];
}
$vod_total_num = $result['page_info']['total_count'];
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) 
{
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;

$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);

$data = $result['data_info']; 
$pass_queue_action_type = get_config_v2('g_pass_queue_action_type');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
		<script language="javascript">

			function refresh_vod_page() 
			{
				var num = $("#nns_list_max_num").val();
                setCookie("page_max_num",num);
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) 
			{
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}

				})
				return BoxKey;
			}
			function do_audit(mes)
			{
				var r=confirm("是否进行该操作");
				var nns_audit;
				nns_audit = (mes == 'pass') ? 1 : 2;
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "nncms_content_pass_queue_list.php?op=do_audit&sp_id=<?php echo $sp_id;?>&nns_audit="+nns_audit+"&ids="+ids;
						window.location.href = url;
					}
				}
			}	
			function do_pasue(mes)
			{
				var r=confirm("是否进行该操作");
				var nns_pause;
				nns_pause = (mes == 'stop') ? 1 : 0;
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "nncms_content_pass_queue_list.php?op=do_pause&sp_id=<?php echo $sp_id;?>&nns_pause="+nns_pause+"&ids="+ids;
						window.location.href = url;
					}
				}
			}
			function msg_retry(mes)
			{
				var r=confirm("是否进行该操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "nncms_content_pass_queue_list.php?op=retry&mssage="+mes+"&sp_id=<?php echo $sp_id;?>&ids="+ids;
						window.location.href = url;
					}
				}
			}
		</script>

	</head>
	<body>
		<div class="content">
			<div class="content_position">
				透传队列
			</div>
				<div class="content_table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<form action="" method="get">
							<tbody>
								<tr>
									<td>
										消息ID：&nbsp;&nbsp;<input type="text" name="nns_message_id" id="nns_message_id" value="<?php echo $_GET['nns_message_id']; ?>" style="width:160px;">
										&nbsp;&nbsp;内容：&nbsp;&nbsp;<input type="text" name="nns_content" id="nns_content" value="<?php echo $_GET['nns_content']; ?>" style="width:140px;">
										动作：&nbsp;&nbsp;<select name="nns_action" style="width: 80px;">
											<option value="" >全部</option>
											<option value="0" <?php if($_GET['nns_action']=='0') echo 'selected="selected"'?>>无动作</option>
											<option value="1" <?php if($_GET['nns_action']=='1') echo 'selected="selected"'?>>添加</option>
											<option value="2" <?php if($_GET['nns_action']=='2') echo 'selected="selected"'?>>修改</option>
											<option value="3" <?php if($_GET['nns_action']=='3') echo 'selected="selected"'?>>删除</option>
											<option value="4" <?php if($_GET['nns_action']=='4') echo 'selected="selected"'?>>上线</option>
											<option value="5" <?php if($_GET['nns_action']=='5') echo 'selected="selected"'?>>下线</option>
											<option value="6" <?php if($_GET['nns_action']=='6') echo 'selected="selected"'?>>锁定</option>
											<option value="7" <?php if($_GET['nns_action']=='7') echo 'selected="selected"'?>>解锁</option>
                                    	</select>
                                    	操作类型：&nbsp;&nbsp;<select name="nns_type" style="width: 80px;">
											<option value="" >全部</option>
											<?php if(is_array($pass_queue_action_type)){foreach ($pass_queue_action_type as $k=>$pass){?>
											<option value="<?php echo $k;?>" <?php if(strlen($_GET['nns_type']) > 0 && $_GET['nns_type'] == $k) echo 'selected="selected"'?>><?php echo $pass;?></option>
											<?php }}?>
                                    	</select>
                                    	状态：&nbsp;&nbsp;<select name="nns_status" style="width: 80px;">
											<option value="" >全部</option>
											<option value="0" <?php if($_GET['nns_status']=='0') echo 'selected="selected"'?>>注入成功</option>
											<option value="1" <?php if($_GET['nns_status']=='1') echo 'selected="selected"'?>>等待注入</option>
											<option value="2" <?php if($_GET['nns_status']=='2') echo 'selected="selected"'?>>正在注入</option>
											<option value="3" <?php if($_GET['nns_status']=='3') echo 'selected="selected"'?>>注入失败</option>
											<option value="4" <?php if($_GET['nns_status']=='4') echo 'selected="selected"'?>>等待反馈</option>
                                    	</select>
                                    	审核：&nbsp;&nbsp;<select name="nns_audit" style="width: 80px;">
											<option value="" >全部</option>
											<option value="0" <?php if($_GET['nns_audit']=='0') echo 'selected="selected"'?>>待审核</option>
											<option value="1" <?php if($_GET['nns_audit']=='1') echo 'selected="selected"'?>>审核通过</option>
											<option value="2" <?php if($_GET['nns_audit']=='2') echo 'selected="selected"'?>>审核未通过</option>
                                    	</select>
                                    	运行状态：&nbsp;&nbsp;<select name="nns_pause" style="width: 50px;">
											<option value="" >全部</option>
											<option value="0" <?php if($_GET['nns_pause']=='0') echo 'selected="selected"'?>>正常</option>
											<option value="1" <?php if($_GET['nns_pause']=='1') echo 'selected="selected"'?>>暂停</option>
                                    	</select>
                                    	CP：&nbsp;&nbsp;<select name="nns_cp_id" style="width: 120px;">
											<option value="" >全部</option>
											<?php foreach ($cp_arr as $sp_list){?>
											<option value="<?php echo $sp_list['nns_id'];?>" <?php if($_GET['nns_cp_id']==$sp_list['nns_id']) echo 'selected="selected"'?>><?php echo $sp_list['nns_name'];?></option>
                                    		<?php }?>
                                    	</select>
										<input type="hidden" name="action_op" value="search" />
										<input type="hidden" name="sp_id" value="<?php echo $sp_id;?>" />
										&nbsp;&nbsp;&nbsp;&nbsp;	
										<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  />										
									</td>
								</tr>
							</tbody>
							</form>
						</table>
				</div>			
			<div class="content_table formtable">

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th><input name="" type="checkbox" value="">序号</th>
							<th>消息ID</th>
							<th>CPID</th>	
							<th>状态</th>
							<th>描述</th>
							<th>审核</th>
							<th>操作类型</th>
							<th>动作</th>
							<th>内容</th>
							<th>创建时间</th>
							<th>修改时间</th>
							<th>操作</th>						    
						</tr>
					</thead>
					<tbody>
						<?php
						if($data != null)
						{
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) 
							{
								$num++;
						?>
						<tr>
						<td width="50px"><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>" /><?php echo $num; ?></td>
						<td><?php echo $item['nns_message_id'];?></td>
                        <td><?php echo $item['nns_cp_id'];?></td>
                        <td>
                        <?php 
                        	switch ($item['nns_pause'])
                        	{
                        		case '0':
                        			echo '<font color="green">[正常]</font>';
                        			break;
                        		case '1':
                        			echo '<font color="red">[暂停]</font>';
                        			break;	
                        	}
                        	switch ($item['nns_status'])
                        	{
                        		case '0':
                        			echo '<font color="blue">注入成功</font>';
                        			break;
                        		case '1':
                        			echo '<font color="blue">等待注入</font>';
                        			break;
                        		case '2':
                        			echo '<font color="gray">正在注入</font>';
                        			break;
                        		case '3':
                        			echo '<font color="red">注入失败</font>';
                        			break;
                        		case '4':
                        			echo '<font color="green">等待反馈</font>';
                        			break;
                        	}
                        ?>
                        </td>
						<td><?php echo $item['nns_result_desc'] ?></td>
						<td>
                        <?php 
                        	switch ($item['nns_audit'])
                        	{
                        		case '0':
                        			echo '<font color="gray">待审核</font>';
                        			break;
                        		case '1':
                        			echo '<font color="green">通过</font>';
                        			break;	
                        		case '2':
                        			echo '<font color="red">未通过</font>';
                        			break;
                        	}
                        ?>
                        </td>
                        <td>
                        <?php 
                        	if(!empty($pass_queue_action_type) && is_array($pass_queue_action_type))
                        	{
                        		echo $pass_queue_action_type[$item['nns_type']];
                        	}
                        ?>
                        </td>
                        <td>
                        <?php 
                        	switch ($item['nns_action'])
                        	{
                        		case '0':
                        			echo '<font color="red">无动作</font>';
                        			break;
                        		case '1':
                        			echo '<font color="blue">添加</font>';
                        			break;
                        		case '2':
                        			echo '<font color="blue">修改</font>';
                        			break;  
                        		case '3':
                        			echo '<font color="blue">删除</font>';
                        			break;
								case '4':
                        			echo '<font color="blue">上线</font>';
                        			break;
								case '5':
                        			echo '<font color="blue">下线</font>';
                        			break;
								case '6':
                        			echo '<font color="blue">锁定</font>';
                        			break;
								case '7':
                        			echo '<font color="blue">解锁</font>';
                        			break;
                        	}
                        ?>
                        </td>
                        <td><a target="_blank" href="nncms_content_pass_queue_view.php?id=<?php echo $item['nns_id'];?>&header_type=<?php $arr = json_decode($item['nns_content'],true);if(is_array($arr)){echo 'json';}else{echo 'xml';}?>">查看内容</a></td>
                        <td><?php echo $item['nns_create_time']; ?></td>  
                        <td><?php echo $item['nns_modify_time']; ?></td>
                        <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                        	<!-- 注入 -->
                        	<?php if($item['nns_status'] != '2' && $item['nns_audit'] == '1' && $item['nns_pause'] == '0'){?>
                        		<a href="nncms_content_pass_queue_list.php?op=retry&mssage=import&sp_id=<?php echo $sp_id; ?>&ids=<?php echo $item['nns_id'];?>">注入</a>
                        	<?php }?>
                        	<!-- 审核，等待注入才能设置 -->
                        	<?php if($item['nns_status'] == '1'){?>
	                        	<?php if($item['nns_audit'] == '0'){?>
	                        		<a href="nncms_content_pass_queue_list.php?op=do_audit&nns_audit=1&sp_id=<?php echo $sp_id; ?>&ids=<?php echo $item['nns_id'];?>">审核通过</a>
	                        		<a href="nncms_content_pass_queue_list.php?op=do_audit&nns_audit=2&sp_id=<?php echo $sp_id; ?>&ids=<?php echo $item['nns_id'];?>">审核不通过</a>
	                        	<?php }elseif($item['nns_audit'] == '1'){?>
	                        		<a href="nncms_content_pass_queue_list.php?op=do_audit&nns_audit=2&sp_id=<?php echo $sp_id; ?>&ids=<?php echo $item['nns_id'];?>">审核不通过</a>
	                        	<?php }else{?>
	                        		<a href="nncms_content_pass_queue_list.php?op=do_audit&nns_audit=1&sp_id=<?php echo $sp_id; ?>&ids=<?php echo $item['nns_id'];?>">审核通过</a>
	                        	<?php }?>
                        	<?php }?> 
                        	<!-- 暂停,等待注入与审核通过才能设置 -->
                        	<?php if($item['nns_status'] == '1' && $item['nns_audit'] == '1'){?>
	                        	<?php if($item['nns_pause'] == '0'){?>
	                        		<a href="nncms_content_pass_queue_list.php?op=do_pause&nns_pause=1&sp_id=<?php echo $sp_id; ?>&ids=<?php echo $item['nns_id'];?>">暂停</a>
	                        	<?php }else{?>
	                        		<a href="nncms_content_pass_queue_list.php?op=do_pause&nns_pause=0&sp_id=<?php echo $sp_id; ?>&ids=<?php echo $item['nns_id'];?>">正常</a>
	                        	<?php }?>
                        	<?php }?>
                        </td>                         
                        </tr>
						<?php
							}
						}
						$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) 
						{
						?>
						<tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                     	</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="pagecontrol">
			  共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_pass_queue_list.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_pass_queue_list.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_content_pass_queue_list.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_pass_queue_list.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_pass_queue_list.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

          	</div>
       	<div class="controlbtns">
	    	<div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
	        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
	        <div class="controlbtn move"><a href="javascript:msg_retry('import');">注入</a></div>
			<div class="controlbtn move"><a href="javascript:do_audit('pass');">审核通过</a></div>
			<div class="controlbtn move"><a href="javascript:do_audit('refuse');">审核不通过</a></div>
			<div class="controlbtn move"><a href="javascript:do_pasue('stop');">暂停</a></div>
			<div class="controlbtn move"><a href="javascript:do_pasue('normal');">取消暂停</a></div>
            <div style="clear:both;"></div>                
		</div>
	</body>
</html>
