<?php
/*
 * Created on 2013-1-26
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include_once LOGIC_DIR.'boss_api'.DIRECTORY_SEPARATOR.'boss_api.func.php';
 function notice_boss_live_add($live_id,$live_name,$live_type){
// 	$boss=get_boss_api();
// 	$result=$boss->live_add(
//		array(
//			'live_id'=>$live_id,
//			'live_name'=>$live_name,
//			'live_type'=>$live_type
//		)
//);

	$result=call_boss_api_by_mode(
		g_cms_config::get_g_config_value('g_mode'),
		'live_add',
		array(
			'live_id'=>$live_id,
			'live_name'=>$live_name,
			'live_type'=>$live_type
		));

 	if ($result['ret']==0){
 		return TRUE;
 	}else{
 		//yxt如果添加直播失败写入日志
 		$handle=fopen(dirname(dirname(dirname(dirname(__FILE__)))).'/data/log/mgtv_boss/'."error_addlive.txt","a");
		fwrite($handle, "################################add_time:".date("Y-m-n H:i:s",time())."\r\n");
		fwrite($handle, "to_params:\r\n");
		fwrite($handle, "live_id:{$live_id}\r\n");
		fwrite($handle, "live_name:{$live_name}\r\n");
		fwrite($handle, "live_type:{$live_type}\r\n");
		fwrite($handle, "ret:{$result['ret']}\r\n");
		fwrite($handle, "reason:{$result['reason']}\r\n");
		fclose($handle);
 		return FALSE;
 	}
 }
 
 function notice_boss_live_del($live_id){
// 	$boss=get_boss_api();
// 	$result=$boss->live_del(
//		array(
//			'live_id'=>$live_id,
//		)
//);
	
	$live_id=rtrim($live_id,',');
	$live_ids=explode(',',$live_id);
	
	foreach ($live_ids as $live_item){
		$result=call_boss_api_by_mode(
		g_cms_config::get_g_config_value('g_mode'),
		'live_del',
		array(
			'live_id'=>$live_item
		));
	}
	
	
 	if ($result['ret']==0){
 		return TRUE;
 	}else{
 		//yxt如果添加直播失败写入日志
 		$handle=fopen(dirname(dirname(dirname(dirname(__FILE__)))).'/data/log/mgtv_boss/'."error_dellive.txt","a");
		fwrite($handle, "################################del_time:".date("Y-m-n H:i:s",time())."\r\n");
		fwrite($handle, "to_params:\r\n");
		fwrite($handle, "live_id:{$live_id}\r\n");
		fwrite($handle, "ret:{$result['ret']}\r\n");
		fwrite($handle, "reason:{$result['reason']}\r\n");
		fclose($handle);
 		return FALSE;
 	}
 }
?>
