<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
ob_start();
include("../../nncms_manager_inc.php");
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/language_contant.php";
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";

include LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'live_info_language.class.php';
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
$dc->open();

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();

$action = $_GET["action"];
$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107104");
$pri_edit = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107101");
$checkpri = null;



if (!$pri_bool) {
        Header("Location: ../nncms_content_wrong.php");
} else {

        include ($nncms_db_path . "nns_common/nns_db_constant.php");


        include ($nncms_db_path . "nns_live/nns_db_live_index_class.php");
        include ($nncms_db_path . "nns_live/nns_db_live_media_class.php");

        // echo $nncms_db_path. "nns_role/nns_db_role_class.php";
        // include($nncms_db_path. "nns_pri/nns_db_pri_class.php");


        include $nncms_db_path . "nns_live/nns_db_live_class.php";
        $live_inst = new nns_db_live_class();
        if (!empty($nns_id)) {
                $live_info = $live_inst->nns_db_live_info($nns_id, "*");
                $live_inst = null;
        } else {
                echo "<script>alert('" . $live_info["reason"] . "');self.location='nncms_content_livelist.php';</script>";
        }
        if ($live_info["ret"] != 0) {
                echo "<script>alert('" . $live_info["reason"] . "');self.location='nncms_content_livelist.php';</script>";
        }

        $edit_data = $live_info["data"][0];
        //		$live_inst=null;
        //	$edit_pri=$edit_data["nns_pri_id"];

        /**
         * 获取视频次要参数 BY S67 2012-8-22
         */
        include $nncms_db_path . "nns_live/nns_db_live_ex_class.php";
        $live_ex_inst = new nns_db_live_ex_class();
        $play_bill_type = $live_ex_inst->nns_db_live_ex_info($nns_id, 'play_bill_type');
        $live_type = $live_ex_inst->nns_db_live_ex_info($nns_id, 'live_type');
        $dvb = $live_ex_inst->nns_db_live_ex_info($nns_id, 'dvb');
        $pid = $live_ex_inst->nns_db_live_ex_info($nns_id, 'pid');
        /*         * *
         * 废除 原TAG方式
         */
        //for ($tag_num=0;$tag_num<32;$tag_num++){
        //	if ($edit_data["nns_tag".$tag_num]==1){
        //		$tar_str.=$tag_num.',';
        //	};
        //}

        $tar_str = ltrim($edit_data['nns_tag'], ',');


        $live_indexes_inst = new nns_db_live_index_class();
        $live_index_inst = new nns_db_live_media_class();

        include $nncms_db_path . "nns_carrier/nns_db_carrier_class.php";
        include $nncms_db_path . "nns_partner/nns_db_partner_class.php";
        include $nncms_db_path . "nns_group/nns_db_group_class.php";
        $carrier_inst = new nns_db_carrier_class();
        $partner_inst = new nns_db_partner_class();
        $group_inst = new nns_db_group_class();
        if ($_SESSION["nns_manager_type"] == 0) {

                $carrier_result = $carrier_inst->nns_db_carrier_list();

                if ($carrier_result["ret"] == 0) {
                        $carrier_data = $carrier_result["data"][0];
                }
                if ($action == "edit") {
                        //		 $partner_inst=new nns_db_partner_class();
                        $partner_result = $partner_inst->nns_db_partner_info($edit_data["nns_org_id"]);

                        if ($partner_result["ret"] == 0) {
                                $partner_data = $partner_result["data"][0];
                        }

                        //		 $group_inst=new nns_db_group_class();
                        $group_result = $group_inst->nns_db_group_info($edit_data["nns_org_id"]);

                        if ($group_result["ret"] == 0) {
                                $group_data = $group_result["data"][0];
                        }
                }
        }


        //	类型为合作伙伴
        if ($_SESSION["nns_manager_type"] == 1) {

                $partner_result = $partner_inst->nns_db_partner_info($_SESSION["nns_org_id"]);

                if ($partner_result["ret"] == 0) {
                        $partner_data = $partner_result["data"][0];
                }
                //	类型为集团
        } else if ($_SESSION["nns_manager_type"] == 2) {

                $group_result = $group_inst->nns_db_group_info($_SESSION["nns_org_id"]);

                if ($group_result["ret"] == 0) {
                        $group_data = $group_result["data"][0];
                }
        }
        //include $nncms_db_path . "nns_assist/nns_db_assist_class.php";
        //include $nncms_db_path . "nns_service/nns_db_service_class.php";
        //  include $nncms_db_path. "nns_service/nns_db_service_detail_video_class.php";

        //$assist_inst = new nns_db_assist_class();
       // $service_inst = new nns_db_service_class();
        //  $service_item_inst=new nns_db_service_detail_video_class();
        //$online1 = $assist_inst->check_video_belong($nns_id);
        //$online2 = $service_inst->check_video_belong($nns_id);
        //  $online3=$service_item_inst->nns_db_service_detail_video_list(0,0,null,$nns_id,1);
        $assist_inst = null;
        $service_inst = null;
        //  $service_item_inst=null;
        /*
        if (count($online1["data"]) > 0 || count($online2["data"]) > 0) {
                $is_online = true;
        } else {
                $is_online = false;
        }*/

        /**
         * 扩展语言 BY S67
         */
        $extend_langs_str = g_cms_config::get_g_extend_language();
        if (empty($extend_langs_str)) {
                $extend_langs = array();
        } else {
                $extend_langs = explode('|', $extend_langs_str);
        }
        /**
         * 剧照 BY cb
         */
        //获取剧照数量，判断是否显示剧照iframe
        include LOGIC_DIR . 'stills' . DIRECTORY_SEPARATOR . 'stills.class.php';
        $stills_num = nl_stills::count_stills_num($dc, $edit_data['nns_id'], $edit_data['nns_view_type']);
        $stills_enabled = g_cms_config::get_g_config_value('g_stills_enabled');
        ob_end_clean();
        ?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title></title>
                        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                        <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                        <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
                        <link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
                        <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
                        <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                        <script language="javascript" src="../../js/checkinput.js.php"></script>
                        <script language="javascript" src="../../js/table.js.php"></script>
                        <script language="javascript" src="../../js/rate.js"></script>
                        <script language="javascript">
                                function gotoDelete(nns_id,deleted,nns_cp_id) {
                                        $("#delete_form #nns_id").val(nns_id);
                                        $("#delete_form #nns_deleted").val(deleted);
                                        $("#delete_form #nns_cp_id").val(nns_cp_id);
                                        checkForm('<?php echo cms_get_lang('media_zbgl'); ?>', '<?php echo cms_get_lang('msg_ask_delete'); ?>', $('#delete_form'), '');
                                }
                                function showCDN(id, type) {
                                        window.parent.begin_show_cdn(id, type);
                                }

                                function show_extend_lang(num) {

                                        if ($(".lang_editor:eq(" + num + ")").is(":hidden") == false) {
                                                $(".lang_editor").hide();
                                                $(".lang_btn").html("<?php echo cms_get_lang('spread'); ?>");
                                        } else {

                                                $(".lang_editor").hide();
                                                $(".lang_btn").html("<?php echo cms_get_lang('spread'); ?>");
                                                $(".lang_editor:eq(" + num + ")").show();
                                                $(".lang_btn:eq(" + num + ")").html("<?php echo cms_get_lang('fold'); ?>");
                                        }
                                        window.parent.checkheight();
                                }

                                function set_show_minor(num) {
                                        $obj = $("input[name=moreinfo_chk]:eq(" + num + ")");
                                        if ($obj.attr("checked") == "checked") {
                                                $(".extend_" + num).show();
                                        } else {
                                                $(".extend_" + num).hide();
                                        }
                                        window.parent.checkheight();
                                }

                                $(document).ready(function() {
                                        $(".lang_editor").hide();
                                        show_extend_lang(0);
                                        var len = $("input[name=moreinfo_chk]").length;
                                        for (var i = 0; i < len; i++) {
                                                set_show_minor(i);
                                        }
                                });
                        </script>
                </head>

                <body>
                        <div class="content">
                                            <!--<div class="content_position"><?php echo cms_get_lang('dbgl'); ?> > <?php
                                if ($view_type == 0) {
                                        echo cms_get_lang('media_dy|dbgl');
                                } else if ($view_type == 1) {
                                        echo cms_get_lang('media_lxj|dbgl');
                                }
                                ?></div>-->
                                <div class="content_table">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                        <tr>
                                                                <td width="120"><?php echo cms_get_lang('media_mtid') ?>:</td>
                                                                <td ><?php echo $edit_data["nns_id"]; ?></td>
                                                                <td width="120"><?php echo cms_get_lang('media_ywsy') ?>:</td>
                                                                <td>
        <?php echo $edit_data["nns_eng_name"]; ?>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td width="120"><?php echo cms_get_lang('media_zb|name') ?>:</td>
                                                                <td><?php echo $edit_data["nns_name"]; ?></td>
                                                                <td width="120"><?php echo cms_get_lang('media_pinyin'); ?>:</td>
                                                                <td>
        <?php echo $edit_data["nns_pinyin"]; ?>
                                                                </td>

                                                        </tr>
                                                        <tr>
                                                                <td width="120"><?php echo $lang_name; ?><?php echo cms_get_lang('media_pdid') ?>:</td>
                                                                <td><?php echo $pid; ?>
                                                                </td>
                                                                <td width="120"><?php echo $lang_name; ?><?php echo cms_get_lang('media_zblx') ?>:</td>
                                                                <td><?php
                                                                        if (strstr($live_type, 'LIVE')) {
                                                                                echo cms_get_lang('media_zb');
                                                                        }
                                                                        if (strstr($live_type, 'TSTV')) {
                                                                                echo cms_get_lang('media_sy');
                                                                        }
                                                                        ?>
                                                                </td>

                                                        </tr>

                                                        <tr>
                                                                <td width="120"><?php echo $lang_name; ?><?php echo cms_get_lang('media_dvb_cs') ?>:</td>
                                                                <td><?php echo $dvb; ?>
                                                                </td>
                                                                <td width="120"><?php echo cms_get_lang('jmdgl_jmdcl') ?>:</td>
                                                                <td>
        <?php echo get_video_ex_name($play_bill_type); ?>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td width="120"><?php echo cms_get_lang('lxdq') ?>:</td>
                                                                <td><?php echo $edit_data["nns_area"]; ?></td>

                                                                <td width="120"><?php echo cms_get_lang('group'); ?>:</td>
                                                                <td><?php
                                                                        if ($edit_data["nns_org_type"] == 0) {
                                                                                echo $carrier_data["nns_name"];
                                                                        } else if ($edit_data["nns_org_type"] == 1) {
                                                                                echo $partner_data["nns_name"];
                                                                        } if ($edit_data["nns_org_type"] == 2) {
                                                                                echo $group_data["nns_name"];
                                                                        }
                                                                        ?>
                                                                </td>
                                                        </tr>
                                                        <tr>

                                                                <td width="120"><?php echo cms_get_lang('state'); ?>:</td>
                                                                <td>
                                                                        <?php
                                                                        if ($is_online) {
                                                                                echo "<font style=\"color:#00CC00\">" . cms_get_lang('media_ysx') . "</font>";
                                                                        } else {
                                                                                echo "<font style=\"color:#999999\">" . cms_get_lang('media_wxx') . "</font>";
                                                                        }
                                                                        ?>
                                                                </td>
                                                                <td width="120"><?php echo cms_get_lang('zdgl_zdscbj'); ?>:</td>
                                                                <td>

                                                                        <alt alt="<?php echo get_tag_detail_html($tar_str); ?>"><?php echo $tar_str; ?></alt>


                                                                </td>
                                                        </tr>

                                                        <tr>
                                                                <td class="rightstyle2a"><?php echo cms_get_lang('zyk_play_count'); ?>:</td>
                                                                <td colspan="3">
                                                        <?php echo $edit_data["nns_play_count"]; ?>
                                                                </td>
                                                        </tr>
        <?php if (count($online1["data"]) > 0) { ?>
                                                                <tr>
                                                                        <td width="120"><?php echo cms_get_lang('assist_owner'); ?>:</td>
                                                                        <td colspan="3">
                                                                                <?php
                                                                                foreach ($online1["data"] as $online1_arr_value) {
                                                                                        echo "<span style=\"color:#ff0000\">" . $online1_arr_value["nns_name"] . "</span><span style=\"color:#999999\">(" . $online1_arr_value["nns_assist_id"] . ")</span>&nbsp;&nbsp;&nbsp;&nbsp;";
                                                                                }
                                                                                ?>
                                                                        </td>
                                                                </tr>
        <?php } ?>
        <?php if (count($online2["data"]) > 0 && $g_service_enabled == 1) { ?>
                                                                <tr>
                                                                        <td width="120"><?php echo cms_get_lang('bkgl_owner'); ?>:</td>
                                                                        <td colspan="3">
                                                                                <?php
                                                                                foreach ($online2["data"] as $online2_arr_value) {
                                                                                        echo "<span style=\"color:#ff0000\">" . $online2_arr_value["nns_name"] . "</span><span style=\"color:#999999\">(" . $online2_arr_value["nns_service_id"] . ")</span>&nbsp;&nbsp;&nbsp;&nbsp;";
                                                                                }
                                                                                ?>
                                                                        </td>
                                                                </tr>
        <?php } ?>



                                                        <tr>
                                                                <td width="120">大、中、小图:</td>
                                                                <td colspan="3">
                                                                        <div class="img_content">
        <?php if (!empty($edit_data["nns_image0"])) { ?>
                                                                                        <div class="img_view"><h4><?php echo cms_get_lang('media_bjhb') . "(" . cms_get_lang('media_big') . ")" . cms_get_lang('media_slt'); ?></h4>
                                                                                                <a href="<?php echo pub_func_get_image_url($edit_data["nns_image0"]); ?>?rand=<?php echo rand(); ?>" target="_blank">
                                                                                                        <img src="<?php echo pub_func_get_image_url($edit_data["nns_image0"]); ?>?rand=<?php echo rand(); ?>" style="border:none;" /></a><h4 class="resolution"></h4></div>
                                                                                <?php } ?>

        <?php if (!empty($edit_data["nns_image1"])) { ?>
                                                                                        <div class="img_view"><h4><?php echo cms_get_lang('media_bjhb') . "(" . cms_get_lang('media_normal') . ")" . cms_get_lang('media_slt'); ?></h4>
                                                                                                <a href="<?php echo pub_func_get_image_url($edit_data["nns_image1"]); ?>?rand=<?php echo rand(); ?>" target="_blank">
                                                                                                        <img src="<?php echo pub_func_get_image_url($edit_data["nns_image1"]); ?>?rand=<?php echo rand(); ?>" style="border:none;" /></a><h4 class="resolution"></h4></div>
        <?php } ?>
        <?php if (!empty($edit_data["nns_image2"])) { ?>
                                                                                        <div class="img_view"><h4><?php echo cms_get_lang('media_bjhb') . "(" . cms_get_lang('media_small') . ")" . cms_get_lang('media_slt'); ?></h4>
                                                                                                <a href="<?php echo pub_func_get_image_url($edit_data["nns_image2"]); ?>?rand=<?php echo rand(); ?>" target="_blank">
                                                                                                        <img src="<?php echo pub_func_get_image_url($edit_data["nns_image2"]); ?>?rand=<?php echo rand(); ?>" style="border:none;" /></a><h4 class="resolution"></h4></div>
        <?php } ?>
                                                                        </div>
                                                                </td>

                                                        </tr>
                                                        <tr>
                                                                <td width="120">横、竖、方图:</td>
                                                                <td colspan="3">
                                                                        <div class="img_content">
        <?php if (!empty($edit_data["nns_image_v"])) { ?>
                                                                                        <div class="img_view"><h4>竖图<?php echo cms_get_lang('media_slt'); ?></h4>
                                                                                                <a href="<?php echo pub_func_get_image_url($edit_data["nns_image_v"]); ?>?rand=<?php echo rand(); ?>" target="_blank">
                                                                                                        <img src="<?php echo pub_func_get_image_url($edit_data["nns_image_v"]); ?>?rand=<?php echo rand(); ?>" style="border:none;" /></a><h4 class="resolution"></h4></div>
                                                                                <?php } ?>

        <?php if (!empty($edit_data["nns_image_s"])) { ?>
                                                                                        <div class="img_view"><h4>方图<?php echo cms_get_lang('media_slt'); ?></h4>
                                                                                                <a href="<?php echo pub_func_get_image_url($edit_data["nns_image_s"]); ?>?rand=<?php echo rand(); ?>" target="_blank">
                                                                                                        <img src="<?php echo pub_func_get_image_url($edit_data["nns_image_s"]); ?>?rand=<?php echo rand(); ?>" style="border:none;" /></a><h4 class="resolution"></h4></div>
        <?php } ?>
        <?php if (!empty($edit_data["nns_image_h"])) { ?>
                                                                                        <div class="img_view"><h4>横图<?php echo cms_get_lang('media_slt'); ?></h4>
                                                                                                <a href="<?php echo pub_func_get_image_url($edit_data["nns_image_h"]); ?>?rand=<?php echo rand(); ?>" target="_blank">
                                                                                                        <img src="<?php echo pub_func_get_image_url($edit_data["nns_image_h"]); ?>?rand=<?php echo rand(); ?>" style="border:none;" /></a><h4 class="resolution"></h4></div>
        <?php } ?>
                                                                        </div>
                                                                </td>

                                                        </tr>
                                                        <tr>
                                                                <td width="120">LOGO:</td>
                                                                <td colspan="3">
                                                                        <div class="img_content">
                                                                                <?php if (!empty($edit_data["nns_image_logo"])) { ?>
                                                                                        <div class="img_view"><h4>LOGO<?php echo cms_get_lang('media_slt'); ?></h4>
                                                                                                <a href="<?php echo pub_func_get_image_url($edit_data["nns_image_logo"]); ?>?rand=<?php echo rand(); ?>" target="_blank">
                                                                                                        <img src="<?php echo pub_func_get_image_url($edit_data["nns_image_logo"]); ?>?rand=<?php echo rand(); ?>" style="border:none;" /></a><h4 class="resolution"></h4></div>
                                                                                <?php } ?>
                                                                        </div>
                                                                </td>

                                                        </tr>

                                                        <tr>
                                                                <td width="120"><?php echo cms_get_lang('media_nrjj'); ?>:</td>
                                                                <td colspan="3">
                                                        <?php echo $edit_data["nns_summary"]; ?>
                                                                </td>
                                                        </tr>
        <?php if ($pri_edit) { ?>
                                                                <tr>
                                                                        <td width="120"><?php echo cms_get_lang('action'); ?>:</td>
                                                                        <td  colspan="3">
                                                                                <input name="" type="button" value="<?php echo cms_get_lang('media_yp_xx'); ?>" onclick="self.location.href = 'nncms_content_live_addedit.php?action=edit&view_type=<?php echo $view_type; ?>&nns_id=<?php echo $nns_id; ?>';"/>&nbsp;&nbsp;
                                                                                <input name="" type="button" value="<?php echo cms_get_lang('media_bjhb'); ?>" onclick="self.location.href = 'nncms_content_live_img_addedit.php?nns_id=<?php echo $nns_id; ?>';"/>&nbsp;&nbsp;
                                                                                <?php if (!empty($extend_langs_str)) { ?>
                                                                                        <input name="" type="button" value="<?php echo cms_get_lang('dyz') ?>" onclick="self.location.href = 'nncms_content_live_info_language_extend.php?nns_id=<?php echo $nns_id; ?>';" />&nbsp;&nbsp;
                                                                                <?php } ?>
                                                                                <?php if ($stills_enabled) { ?>
                                                                                        <input name="" type="button" value="<?php echo cms_get_lang('media_jzgl') ?>" onclick="window.location.href = '../stills_pages/nncms_content_stills.php?video_name=<?php echo $edit_data["nns_name"]; ?>&video_id=<?php echo $edit_data["nns_id"]; ?>&video_type=<?php echo $edit_data['nns_view_type'] ?>&url_type=4'"/>
                                                                <?php } ?>
                                                                        </td>
                                                                </tr>
        <?php } ?>
        <?php if ($stills_enabled && $stills_num > 0) { ?>
                                                                <tr >
                                                                        <td colspan="4" style="padding:0px">
                                                                                <div  style="height:200px;">
                                                                                        <iframe scrolling="no" frameborder="0" style="width:100%;" height="410" src="../stills_pages/nncms_content_stills_list.php?video_id=<?php echo $edit_data["nns_id"]; ?>&video_type=<?php echo $edit_data['nns_view_type'] ?>"></iframe>
                                                                                </div>
                                                                        </td>
                                                                </tr>
                                                        <?php } ?>

                                                        <!--语言扩展-->
                                                        <?php
                                                        $num = 0;
                                                        foreach ($extend_langs as $extend_lang) {
                                                                $result = nl_live_info_language::get_live_info_by_language($dc, $nns_id, $extend_lang, NL_DC_DB);
                                                                if ($result !== FALSE || $result !== TRUE) {
                                                                        $data = $result[0];
                                                                } else {
                                                                        $data = NULL;
                                                                }
                                                                ?>
                                                                <tr>
                                                                        <td colspan="4"   style="padding:0px;"><div class="radiolist" style=" border-bottom:1px solid #86B7CC;"><div class="radiogroup">
                                                                                                <h3 style="  background-color:#DCEEF5;">
                                                                                                        <?php
                                                                                                        $lang_name = get_language_name_by_code($extend_lang);
                                                                                                        echo $lang_name, cms_get_lang('yyb|kz');
                                                                                                        ?>
                                                                                                        <div style='padding:2px 5px 2px 20px; float:right; background:url(../../images/add_1.png) 0px 50% no-repeat;'>
                                                                                                                <a href='javascript:show_extend_lang(<?php echo $num; ?>);' class="lang_btn"> <?php echo cms_get_lang('spread'); ?></a></div>
                                                                                                </h3>

                                                                                        </div></div></td>
                                                                </tr>
                                                                <tr class='lang_editor'>
                                                                        <td colspan="4"   style="padding:0px;">
                                                                                <div class="content_table">
                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                <tbody>
                                                                                                        <tr>
                                                                                                                <td width="120"><?php echo $lang_name, cms_get_lang('name'); ?>:</td>
                                                                                                                <td><?php echo $data['nns_name']; ?></td>
                                                                                                                <td width="120"><?php echo $lang_name, cms_get_lang('lxdq'); ?>:</td>
                                                                                                                <td><?php echo $data['nns_area']; ?>
                                                                                                                </td>

                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                                <td width="120"><?php echo $lang_name, cms_get_lang('media_index_jj'); ?>:</td>
                                                                                                                <td colspan="2">

                <?php echo $data['nns_summary']; ?>
                                                                                                                </td>
                                                                                                                <td width="120">
                                                                                                                        <input  name="moreinfo_chk" id="moreinfo_chk" type="checkbox" onclick="set_show_minor(<?php echo $num; ?>);" /> <font style="color:#0000ff"><?php echo cms_get_lang('gdxx'); ?></a>
                                                                                                                </td>
                                                                                                        </tr>
                                                                                                        <tr class="extend_<?php echo $num; ?>">
                                                                                                                <td width="120"><?php echo $lang_name, cms_get_lang('media_bm'); ?>:</td>
                                                                                                                <td><?php echo $data['nns_alias_name']; ?></td>
                                                                                                                <td width="120"><?php echo $lang_name, cms_get_lang('media_spyz'); ?>:</td>
                                                                                                                <td><?php echo $data['nns_live_language']; ?></td>
                                                                                                        </tr>



                                                                                                </tbody>
                                                                                        </table>
                                                                                </div>
                                                                        </td>
                                                                </tr>
                <?php $num++;
        }
        ?>

                                                        <!-----多語言結束-------->



                                                        <?php
                                                        for ($live_current_index = 1; $live_current_index <= $edit_data["nns_all_index"]; $live_current_index++) {

                                                                $num = 0;

                                                                $live_indexes_info = $live_indexes_inst->nns_db_live_index_info($nns_id, $live_current_index - 1);
                                                                $index_intor_data = $live_indexes_info["data"][0];
                                                                ?>

                                                                <tr>
                                                                        <td colspan="4"  style="padding:0px;"><div class="radiolist"><div class="radiogroup"><h3><?php echo cms_get_lang('media_zb'); ?></h3>
                                                                                        </div></div></td>
                                                                </tr>


                                                                <?php
                                                                $live_media_arr = $live_index_inst->nns_db_live_media_list(0, 0, $nns_id, $live_current_index - 1,null,null,null,null,-1);
                                                                if ($live_media_arr["ret"] == 0) {
                                                                        $live_media_data = $live_media_arr["data"];
                                                                } else {
                                                                        $live_media_data = null;
                                                                }
                                                                ?>
                                                                <tr>
                                                                        <td colspan="4">
                                                                                <div class="content_table formtable" style="padding:3px 10px;">
                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                <thead>
                                                                                                        <tr>

                                                                                                                <th><?php echo cms_get_lang('segnumber'); ?></th>
                                                                                                                <th><?php echo cms_get_lang('name'); ?></th>
                                                                                                                <th><?php echo cms_get_lang('media_pydz'); ?></th>
                                                                                                                <th><?php echo cms_get_lang('media_pylx'); ?></th>
                                                                                                                <th><?php echo cms_get_lang('zdgl_zdscbj'); ?></th>
                                                                                                                <th><?php echo cms_get_lang('media_pygs'); ?></th>
                                                                                                                <th><?php echo cms_get_lang('media_mtid'); ?></th>
                                                                                                                <th><?php echo cms_get_lang('media_pyml'); ?></th>
                                                                                                                <th>CP ID</th>
                                                                                                                <th>状态</th>
                                                                                                                <th><?php echo cms_get_lang('action'); ?></th>


                                                                                                        </tr>
                                                                                                </thead>
                                                                                                <tbody>
                                                                                                        <?php
                                                                                                        if ($live_media_data != null) {

                                                                                                                foreach ($live_media_data as $item) {
                                                                                                                        $num++;
                                                                                                                        ?>
                                                                                                                        <tr>

                                                                                                                                <td><?php echo $num; ?></td>
                                                                                                                                <td><?php echo $item["nns_name"]; ?></td>
                                                                                                                                <td><?php echo $item["nns_url"]; ?></td>
                                                                                                                                <td><?php echo get_contant_language($item["nns_mode"]); ?></td>
                                                                                                                                <td><alt alt="<?php echo get_tag_detail_html($item["nns_tag"]); ?>"><?php echo $item["nns_tag"]; ?></alt></td>
                                                                                                                                <td><?php echo $item["nns_filetype"]; ?></td>
                                                                                                                                <td><?php echo $item["nns_content_id"]; ?></td>
                                                                                                                                <td><?php echo $item["nns_kbps"]; ?></td>
                                                                                                                                <td><?php echo $item["nns_cp_id"]; ?></td>
                                                                                                                                <td><?php if($item["nns_deleted"] =='0'){ echo "未删除";}else{ echo '<font color="red">已删除</font>';}; ?></td>
                                                                                                                                <td  class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                                                                                                                                        <?php if ($pri_edit) { ?>
                                                                                                                                                <a href="../live_media_pages/nncms_content_live_media_addedit.php?view_type=<?php echo $view_type; ?>&action=edit&live_id=<?php echo $nns_id; ?>&index=<?php echo $live_current_index - 1; ?>&nns_id=<?php echo $item["nns_id"]; ?>" target="_self"><?php echo cms_get_lang('edit'); ?></a>&nbsp;&nbsp;
                                                                                                                                                
                                                                                                                                                <?php if($item["nns_deleted"] =='0'){?>
                                                                                                                                                <a href="javascript:gotoDelete('<?php echo $item["nns_id"]; ?>','1','<?php echo $item["nns_cp_id"];?>');" target="_self"><?php echo cms_get_lang('delete'); ?></a>&nbsp;&nbsp;
                                                                                                                                                <?php }else{?>
                                                                                                                                                <a href="javascript:gotoDelete('<?php echo $item["nns_id"]; ?>','0','<?php echo $item["nns_cp_id"];?>');" target="_self">恢复</a>&nbsp;&nbsp;
                                                                                                                                                
                                                                                                                                                <?php }?>
                                                                                                                                                <a href="javascript:showCDN('<?php echo $item["nns_content_id"]; ?>',0);" target="_self"><?php echo cms_get_lang('ff|state'); ?></a>&nbsp;&nbsp;
                                                                                                                                        <?php } ?>
                                                                                                                                        <?php
                                                                                                                                        if (strtolower($item["nns_filetype"]) == "flv") {
                                                                                                                                                $core_npss_url = explode(";", $g_core_npss_url);
                                                                                                                                                $flv_url = "http://" . $core_npss_url[0] . "/nn_live.flv?id=" . $item["nns_content_id"] . "&nst=iptv";
                                                                                                                                                $flv_url = urlencode($flv_url);
                                                                                                                                                ?>
                                                                                                                                                <a href="../../models/flashplayer/flashplayer.php?vod_url=<?php echo $flv_url; ?>" target="_blank"><?php echo cms_get_lang('media_flv_play'); ?></a>&nbsp;&nbsp;
                                                                                                                        <?php } ?>
                                                                                                                                </td>

                                                                                                                        </tr>
                        <?php }
                }
                ?>
                                                                                                </tbody>
                                                                                        </table>
                                                                                </div>
                                                                        </td>
                                                                </tr>
                                <?php } ?>
                                                </tbody>
                                        </table>
                                </div>
        <?php if ($pri_edit) { ?>
                                        <form id="delete_form" action="../live_media_pages/nncms_content_live_media_control.php" method="post">
                                                <input name="action" id="action" type="hidden" value="delete" />
                                                <input name="nns_id" id="nns_id" type="hidden" value="" />
                                                <input name="nns_deleted" id="nns_deleted" type="hidden" value="" />
                                                <input name="nns_cp_id" id="nns_cp_id" type="hidden" value="" />
                                                <input name="nns_live_id" id="nns_live_id" type="hidden" value="<?php echo $nns_id; ?>" />
                                        </form>
                                        <?php } ?>
                                <div class="controlbtns">
                                        <?php if ($pri_edit) { ?>
                                                <div class="controlbtn info"><a href="nncms_content_live_addedit.php?action=edit&view_type=<?php echo $view_type; ?>&nns_id=<?php echo $nns_id; ?>"><?php echo cms_get_lang('media_yp_xx'); ?></a></div>
                                                <div class="controlbtn haibao"><a href="nncms_content_live_img_addedit.php?nns_id=<?php echo $nns_id; ?>"><?php echo cms_get_lang('media_bjhb'); ?></a></div>
                                                <?php if ($stills_enabled) { ?>
                                                        <div class="controlbtn stills"><a href="javascript:window.location.href='../stills_pages/nncms_content_stills.php?video_name=<?php echo $edit_data["nns_name"]; ?>&video_id=<?php echo $edit_data["nns_id"]; ?>&video_type=<?php echo $edit_data['nns_view_type'] ?>&url_type=4'"><?php echo cms_get_lang('media_jzgl'); ?></a></div>
                                                <?php } ?>
                                                <?php if (!empty($extend_langs_str)) { ?>
                                                        <div class="controlbtn lang"><a  href="../live_pages/nncms_content_live_info_language_extend.php?nns_id=<?php echo $nns_id; ?>"><?php echo cms_get_lang('dyz'); ?></a></div>
                <?php } ?>
                                                <div class="controlbtn media"><a href="../live_media_pages/nncms_content_live_media_list.php?nns_id=<?php echo $nns_id; ?>&view_type=<?php echo $view_type; ?>"><?php echo cms_get_lang('media_bind_media'); ?></a></div>
        <?php } ?>
                                        <div class="controlbtn back"><a href="javascript:window.parent.refresh_tree_content();"><?php echo cms_get_lang('back'); ?></a></div>
                                        <div style="clear:both;"></div>
                                </div>
                        </div>
                </body>
        </html>
        <script language="javascript" src="../../js/image_loaded_func.js"></script>
<?php } ?>
