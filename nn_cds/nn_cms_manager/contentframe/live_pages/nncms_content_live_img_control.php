<?php
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
include $nncms_config_path . 'nn_cms_manager/controls/nncms_controls_public_function.php';
require_once $nncms_db_path . 'nns_log' . DIRECTORY_SEPARATOR . 'nns_db_op_log_class.php';
$log_inst = new nns_db_op_log_class();

$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE, 
		'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$dc->open();
$params = $_REQUEST;



/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/live/live.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';
$nns_id = $_REQUEST['nns_id'];

$result_query = nl_live::query_by_id($dc, $nns_id);
if($result_query['ret'] !=0 || !isset($result_query['data_info']) || !is_array($result_query['data_info']) || empty($result_query['data_info']))
{
    echo "<script>alert('". $result_query['reason']. "');</script>";
    echo "<script>self.location='../live_media_pages/nncms_content_live_media_list.php?nns_id=".$nns_id ."';</script>";die;
}
$result_query = $result_query['data_info'];
$result_cp = nl_cp::query_by_id($dc, $result_query['nns_cp_id']);
if($result_cp['ret'] !=0 || !isset($result_cp['data_info']) || !is_array($result_cp['data_info']) || empty($result_cp['data_info']))
{
    echo "<script>alert('". $result_cp['reason']. "');</script>";
    echo "<script>self.location='../live_media_pages/nncms_content_live_media_list.php?nns_id=".$nns_id ."';</script>";die;
}
$result_cp = $result_cp['data_info'];
$arr_cp_config = (isset($result_cp['nns_config']) || strlen($result_cp['nns_config']) >0) ? json_decode($result_cp['nns_config'],true) : null;
// 载入全局的芒果tv的ftp配置数据,是一个二维数组
if (isset($arr_cp_config['g_ftp_conf']['img_enabled']) && $arr_cp_config['g_ftp_conf']['img_enabled'] == 1)
{
    $g_ftp_conf = $arr_cp_config['g_ftp_conf'];
}
else
{
    global $g_ftp_conf;
}

$arr_img = array(
    'nns_image0',
    'nns_image1',
    'nns_image2',
    'nns_image3',
    'nns_image4',
    'nns_image5',
    'nns_image_v',
    'nns_image_s',
    'nns_image_h',
    'nns_image_logo',
);
$ex_img_data = array();
foreach ($arr_img as $img_val)
{
    if(!isset($result_query[$img_val]) || strlen($result_query[$img_val]) <1)
    {
        continue;
    }
    pub_func_image_delete($result_query[$img_val],$g_ftp_conf);
}
foreach ($arr_img as $img_val)
{
    if(isset($_FILES[$img_val]))
    {
        $ex_img_data[$img_val] = pub_func_image_upload_by_live($_FILES[$img_val],$nns_id,$g_ftp_conf,$result_query['nns_cp_id']);
    }
    else
    {
        $ex_img_data[$img_val]='';
    }
}
$result = nl_live::edit($dc, $ex_img_data, $nns_id);
if ($result["ret"]!=0)
{
	echo "<script>alert('". $result['reason']. "');</script>";
}
else
{
	$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], 'edit', $_SESSION["nns_mgr_name"] . "修改直播图片:" . $nns_id, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
    echo "<script>alert('修改成功');</script>";
}
unset($log_inst);
$live_inst=null;
echo "<script>self.location='../live_media_pages/nncms_content_live_media_list.php?nns_id=".$nns_id ."';</script>";