<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
ob_start();
include("../../nncms_manager_inc.php");
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/language_contant.php";


//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/producer/producer.class.php';

$action = $_GET["action"];
$nns_id = $_GET["nns_id"];
$depot_id = $_GET["depot_id"];
$depot_detail_id = $_GET["depot_detail_id"];
$pri_bool = false;
if (!empty($action)) {
        switch ($action) {
                case "edit":
                        $action_str = cms_get_lang('save');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107101");
                        break;
                case "add":
                        $action_str = cms_get_lang('add');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107100");
                        break;
                default:
                        break;
        }
}
$checkpri = null;



if (!$pri_bool) {
        Header("Location: ../nncms_content_wrong.php");
} else {

        include ($nncms_db_path . "nns_common/nns_db_constant.php");

        //	 include ($nncms_db_path. "nns_category/nns_db_category_list_class.php");
        // echo $nncms_db_path. "nns_role/nns_db_role_class.php";
        // include($nncms_db_path. "nns_pri/nns_db_pri_class.php");

        if ($action == "edit") {
                include $nncms_db_path . "nns_live/nns_db_live_class.php";
                $live_inst = new nns_db_live_class();
                if (!empty($nns_id)) {
                        $live_info = $live_inst->nns_db_live_info($nns_id, "*");
                        $live_inst = null;
                } else {
                        echo "<script>alert('" . $live_info["reason"] . "');self.location='nncms_content_livelist.php';</script>";
                }
                if ($live_info["ret"] != 0) {
                        echo "<script>alert('" . $live_info["reason"] . "');self.location='nncms_content_livelist.php';</script>";
                }

                $edit_data = $live_info["data"][0];
                $depot_id = $edit_data["nns_depot_id"];
                $depot_detail_id = $edit_data["nns_category_id"];

                /*                 * *
                 * 废除 原TAG方式
                 */
                //for ($tag_num=0;$tag_num<32;$tag_num++){
                //	if ($edit_data["nns_tag".$tag_num]==1){
                //		$tar_str.=$tag_num.',';
                //	};
                //}

                $tar_str = ltrim($edit_data['nns_tag'], ',');

                /**
                 * 获取视频次要参数 BY S67 2012-8-22
                 */
                include $nncms_db_path . "nns_live/nns_db_live_ex_class.php";
                $live_ex_inst = new nns_db_live_ex_class();
                $source_url = $live_ex_inst->nns_db_live_ex_info($nns_id, 'source_url');
                $play_bill_type = $live_ex_inst->nns_db_live_ex_info($nns_id, 'play_bill_type');
                $live_type = $live_ex_inst->nns_db_live_ex_info($nns_id, 'live_type');
                $pindao_name= $live_ex_inst->nns_db_live_ex_info($nns_id, 'live_pindao_order');
                $program_id = $live_ex_inst->nns_db_live_ex_info($nns_id, 'pid');
                $dvb = $live_ex_inst->nns_db_live_ex_info($nns_id, 'dvb');
                //添加直播回看天数 xinxin.deng 2018/8/3 14:53
                $back_record_day = $live_ex_inst->nns_db_live_ex_info($nns_id, 'back_record_day');
                $back_record_day_type = $live_ex_inst->nns_db_live_ex_info($nns_id, 'back_record_day_type');

                $nns_begin = $live_ex_inst->nns_db_live_ex_info($nns_id, 'ts_limit_min');
                $nns_length = $live_ex_inst->nns_db_live_ex_info($nns_id, 'ts_limit_max');
                $nns_length = is_numeric($nns_length) ? $nns_length / 3600 : $nns_length;
                
                $nns_ts_pos = $live_ex_inst->nns_db_live_ex_info($nns_id, 'ts_default_pos');
                
                //		$live_inst=null;
                //	$edit_pri=$edit_data["nns_pri_id"];
        }

        //	$category_list_inst=new nns_db_category_list_class();
        //	$category_list_result=$category_list_inst->nns_db_category_list_list("",0,null,null,0,0,1);
        //	if ($category_list_result["ret"]==0) $category_list_data=$category_list_result["data"];

        include $nncms_db_path . "nns_carrier/nns_db_carrier_class.php";
        include $nncms_db_path . "nns_partner/nns_db_partner_class.php";
        include $nncms_db_path . "nns_group/nns_db_group_class.php";
        $carrier_inst = new nns_db_carrier_class();
        $partner_inst = new nns_db_partner_class();
        $group_inst = new nns_db_group_class();
        if ($_SESSION["nns_manager_type"] == 0) {
                $carrier_result = $carrier_inst->nns_db_carrier_list();

                if ($carrier_result["ret"] == 0) {
                        $carrier_data = $carrier_result["data"][0];
                }
                if ($action == "edit") {
                        //		 $partner_inst=new nns_db_partner_class();
                        $partner_result = $partner_inst->nns_db_partner_info($edit_data["nns_org_id"]);

                        if ($partner_result["ret"] == 0) {
                                $partner_data = $partner_result["data"][0];
                        }

                        //		 $group_inst=new nns_db_group_class();
                        $group_result = $group_inst->nns_db_group_info($edit_data["nns_org_id"]);

                        if ($group_result["ret"] == 0) {
                                $group_data = $group_result["data"][0];
                        }
                }
        } else {

                //	类型为合作伙伴
                if ($_SESSION["nns_manager_type"] == 1) {

                        $partner_result = $partner_inst->nns_db_partner_info($_SESSION["nns_org_id"]);

                        if ($partner_result["ret"] == 0) {
                                $partner_data = $partner_result["data"][0];
                        }
                        //	类型为集团
                } else if ($_SESSION["nns_manager_type"] == 2) {

                        $group_result = $group_inst->nns_db_group_info($_SESSION["nns_org_id"]);

                        if ($group_result["ret"] == 0) {
                                $group_data = $group_result["data"][0];
                        }
                }
        }
        ob_end_clean();

        include_once LOGIC_DIR . 'cp/cp.class.php';
        $dc = nl_get_dc(array(
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
        $dc->open();
        $cp_list = nl_cp::query_all($dc);
        $cp_list = is_array($cp_list['data_info']) ? $cp_list['data_info'] : array();
        ?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title></title>
                        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                        <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                        <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
                        <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
                        <link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
                        <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                        <script language="javascript" src="../../js/table.js.php"></script>
                        <script language="javascript" src="../../js/radiolist.js"></script>
                        <script language="javascript" src="../../js/checkinput.js.php"></script>
                        <script language="javascript" src="../../js/cms_ajax.js"></script>
                        <script language="javascript" src="../../js/cms_live.js.php"></script>
                        <script language="javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
                        <script language="javascript" src="../../js/rate.js"></script>
                        <script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
                        <script language="javascript">
                                $(document).ready(function() {

                                        $(".selectbox").hide();
                                        var $obj = $("#nns_carrier").find("option:selected");
                                        var index = $("#nns_carrier option").index($obj);
                                        selectCarrier(index);
                                        $("#nns_carrier").change(function() {
                                                var $obj = $(this).find("option:selected");
                                                var index = $("#nns_carrier option").index($obj);
                                                selectCarrier(index);
                                        });
                                        get_data();
                                    	$("#nns_cp_id").change(function(){
                                    		get_data();
                                    	});
                                        $(".extends").hide();

                                });

                                function selectCarrier(num) {

                                        $("#nns_partner_control").hide();
                                        $("#nns_group_control").hide();
                                        $("#nns_group_input").attr("rule", "");
                                        $("#nns_partner_input").attr("rule", "");

                                        var val = $("#nns_carrier option:eq(" + num + ")").attr("value");
                                        switch (val) {
                                                case "1":
                                                        $("#nns_partner_input").attr("rule", "noempty");
                                                        $("#nns_partner_control").show();
                                                        break;
                                                case "2":
                                                        $("#nns_group_input").attr("rule", "noempty");
                                                        $("#nns_group_control").show();
                                                        break;
                                        }
                                        window.parent.checkheight();
                                }
                                function checkheight() {
                                        $(".selectbox iframe").height($(".selectbox iframe").contents().find(".selectcontrol").height() + 10);
                                }
                                function begin_select_partner() {
                                        $("#select_frame").attr("src", "../../controls/nncms_controls_select.php?method=partner");
                                        $(".selectbox").show();
                                }

                                function begin_select_group() {
                                        $("#select_frame").attr("src", "../../controls/nncms_controls_select.php?method=group");
                                        $(".selectbox").show();
                                }
                                function close_select() {
                                        $(".selectbox").hide();
                                }

                                function set_partner_id(value, name) {
                                        $("#nns_partner").attr("value", value);
                                        $("#nns_partner_input").attr("value", name);
                                }

                                function set_group_id(value, name) {
                                        $("#nns_group").attr("value", value);
                                        $("#nns_group_input").attr("value", name);
                                }

                                function set_show_minor() {

                                        if ($("#moreinfo_chk").attr("checked")) {
                                                $(".extends").show();
                                        } else {
                                                $(".extends").hide();
                                        }
                                        window.parent.checkheight();
                                }
                                //ajax 动态加载动态框  flag=true(转换控制模式的时候 动态框全部清空再添加动态框)  flag=false(添加动态框的时候)
                                function get_data()
                                {
                                	var str_cp_id= $('#nns_cp_id').val();
                                	str_cp_id = str_cp_id == 'undefined' ? '' : str_cp_id;
                                	$.get("../vod_pages/nncms_content_vod_control.php", { action: "query",nns_cp_id : str_cp_id},
                                		function(data)
                                		{
                                    		$('#nns_import_source').html('');
                                    		if(data.ret !=0)
                                    		{
                                    			alert(data.reason);return;
                                        	}
                                        	var str_html='<option value="">--请选择内容提供商--</option>';
                                        	var exsist_str = '<?php if(isset($edit_data['nns_import_source'])){ echo $edit_data['nns_import_source'];}else{ echo "";}?>';
                                        	if(data.data_info.length > 0)
                                            {
                                            	for(var i=0;i<data.data_info.length;i++)
                                    			{
                                        			var str_selected = exsist_str == data.data_info[i].nns_import_source ? 'selected="selected"' : '';
                                            		str_html+="<option value='"+data.data_info[i].nns_import_source+"' "+str_selected+" >"+data.data_info[i].nns_producer+"</option>";
                                                }
                                            }
                                        	$('#nns_import_source').append(str_html);
                                		},'json');	
                                }
                        </script>
                </head>

                <body>

                        <div class="selectbox">
                                <iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
                        </div>
                        <div class="content">
                                            <!--<div class="content_position"><?php echo cms_get_lang('zbgl'); ?></div>-->
                                <form id="add_form" action="nncms_content_live_control.php" method="post">
                                        <input name="action" id="action" type="hidden" value="<?php echo $action; ?>" />
                                        <input name="nns_id" id="nns_id" type="hidden" value="<?php echo $nns_id; ?>"/>
                                        <input name="nns_tags" id="nns_tags" type="hidden" value=""/>
                                        <input name="depot_id" id="depot_id" type="hidden" value="<?php echo $depot_id; ?>"/>
                                        <input name="depot_detail_id" id="depot_detail_id" type="hidden" value="<?php echo $depot_detail_id; ?>"/>
                                        <input name="nns_partner" id="nns_partner" type="hidden" value="<?php echo $edit_data["nns_org_id"]; ?>"/>
                                        <input name="nns_group" id="nns_group" type="hidden" value="<?php echo $edit_data["nns_org_id"]; ?>"/>
                                        <input name="nns_point" id="nns_point" type="hidden" value=""/>
                                        <div class="content_table">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                                <tr>
                                                                        <td class="rightstyle"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('media_zb|name'); ?>:</td>
                                                                        <td><input name="nns_live_name" id="nns_live_name" type="text" rule="noempty"
                                                                                   value="<?php
                                                                                   if ($action == "edit") {
                                                                                           echo $edit_data["nns_name"];
                                                                                   }
                                                                                   ?>"
                                                                                   /> </td>

                                                                </tr>
                                                                <tr>
                                                                	<td class="rightstyle">直播源地址:</td>
                                                                	<td><input name="nns_source_url" id="nns_source_url" type="text" value="<?php echo $source_url;?>"/></td>
                                                                </tr>
                                                                <tr>
                                                                	 <td class="rightstyle">频道号:</td>
															 		  <td>
															 			<input name="nns_pindao_name" id="nns_pindao_name" rule="noempty|int" type="text" value="<?php echo $pindao_name; ?>" />
															 		  </td>
                                                                </tr>
                                                                <tr>
                                                                        <td class="rightstyle"><?php echo cms_get_lang('media_bm'); ?>:</td>
                                                                        <td>
                                                                                <input name="nns_alias_name" id="nns_alias_name" type="text" value="<?php
                                                                                if ($action == "edit") {
                                                                                        echo $edit_data["nns_alias_name"];
                                                                                }
                                                                                ?>" />
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                        <td class="rightstyle"><?php echo cms_get_lang('media_pdid'); ?>:</td>
                                                                        <td>
                                                                                <input name="pid" id="pid" type="text" value="<?php
                                                                                if ($action == "edit") {
                                                                                        echo $program_id;
                                                                                }
                                                                                ?>" />
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                        <td class="rightstyle"><?php echo cms_get_lang('media_dvb_cs'); ?>:</td>
                                                                        <td>
                                                                                <input name="dvb" id="dvb" type="text" value="<?php
                                                                                if ($action == "edit") {
                                                                                        echo $dvb;
                                                                                }
                                                                                ?>" />
                                                                        </td>
                                                                </tr>
                                                                <!-- R0853 设置频道回看天数 xinxin.deng 2018/8/3 14:51 -->
                                                                <tr>
                                                                    <td class="rightstyle">直播回看天数:</td>
                                                                    <td><select id="look_back_type" style="width: 15%;" name="look_back_type">
                                                                            <option value="0" <?php if($back_record_day_type == 0){echo "selected='selected'";}?>><?php echo '自然天数';?></option>
                                                                            <option value="1" <?php if($back_record_day_type == 1){echo "selected='selected'";}?>><?php echo '相对天数';?></option>
                                                                        </select><input style="width: 10%;" name="look_back" id="look_back" onblur="checkbackrecord()" type="text"
                                                                                        value="<?php
                                                                                        if($action == "edit")
                                                                                        {
                                                                                            echo $back_record_day;
                                                                                        }
                                                                                        ?>"/>&nbsp;&nbsp;<p id="look_back_type_notice" style="width: 40%;display: inline;">自然天数【回看天数内容结尾时间为从请求数据时开始计算前n天】;相对天数【回看天数内容结尾时间为从请求数据时-1天开始计算前n天】</p></td>
                                                                </tr>
                                                                <tr>
                                                                        <td class="rightstyle"><?php echo cms_get_lang('media_zblx'); ?>:</td>
                                                                        <td>
                                                                                <div class="radiolist">

                                                                                        <div class="radiogroup">
                                                                                                <div class="radioitem" >
                                                                                                        <input value="LIVE" name="live_type[]" type="checkbox"
                                                                                                               <?php if ($action == "add" || strstr($live_type, 'LIVE')) { ?>checked<?php } ?>
                                                                                                               /> <?php echo cms_get_lang('media_zb'); ?>
                                                                                                </div>
                                                                                                <div class="radioitem" >
                                                                                                        <input value="TSTV" onclick="return shiyiShow();" class="tstv" name="live_type[]" type="checkbox"
                                                                                                               <?php if ($action == "edit" && strstr($live_type, 'TSTV')) { ?>checked<?php } ?>
                                                                                                               /> <?php echo cms_get_lang('media_sy'); ?>
                                                                                                </div>
																								<div class="radioitem">
																									<input value="PLAYBACK"
																										   name="live_type[]" type="checkbox"
																										<?php if($action == "edit" && strstr($live_type, 'PLAYBACK'))
																										{ ?>
																											checked <?php } ?> /> <?php echo cms_get_lang('media_playback'); ?>
																								</div>
                                                                                        </div>
																				</div>

                                                                        </td>

                                                                </tr>
                                                                <tr class="shiyi">
                                                                        <td class="rightstyle">时移最小偏移值(秒):</td>
                                                                        <td>
                                                                                <input name="begin" id="pid" type="text" value="<?php
                                                                                if ($action == "edit") {
                                                                                        echo $nns_begin;
                                                                                }
                                                                                ?>" />
                                                                        </td>
                                                                </tr>
                                                                <tr class="shiyi">
                                                                        <td class="rightstyle">时移最大偏移值(小时):</td>
                                                                        <td>
                                                                                <input name="length" id="pid" type="text" value="<?php
                                                                                if ($action == "edit") {
                                                                                        echo $nns_length;
                                                                                }
                                                                                ?>" />
                                                                        </td>
                                                                </tr>
                                                                 <tr class="shiyi">
                                                                        <td class="rightstyle">时移默认偏移值(秒):</td>
                                                                        <td>
                                                                                <input name="ts_pos" id="pid" type="text" value="<?php
                                                                                if ($action == "edit") {
                                                                                        echo $nns_ts_pos;
                                                                                }
                                                                                ?>" />
                                                                        </td>
                                                                </tr>
                                                                
                                                                <?php if ($action == "edit") { ?>
                                                                        <tr>
                                                                                <td class="rightstyle"><?php echo cms_get_lang('media_pinyin'); ?>:</td>
                                                                                <td>
                                                                                        <input name="nns_pinyin" id="nns_pinyin" type="text" value="<?php echo $edit_data["nns_pinyin"]; ?>" />
                                                                                </td>
                                                                        </tr>
                                                                <?php } ?>
                                                                <tr>
                                                                        <td class="rightstyle"><?php echo cms_get_lang('media_ywsy'); ?>:</td>
                                                                        <td>
                                                                                <input name="nns_eng_name" id="nns_eng_name" type="text" value="<?php
                                                                                if ($action == "edit") {
                                                                                        echo $edit_data["nns_eng_name"];
                                                                                }
                                                                                ?>" />
                                                                        </td>
                                                                </tr>



                                                                <tr class="select_carrier_class">
                                                                        <td class="rightstyle"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('group'); ?>:</td>
                                                                        <td><select name="nns_carrier" id="nns_carrier"  style="margin:5px 0px;"><br>
                                                                                                <option value="<?php echo $carrier_data["nns_id"]; ?>" <?php if ($action == "edit" && $edit_data["nns_type"] == 0) { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('xtgl_yysgl'), '/', $carrier_data["nns_name"]; ?></option>
                                                                                                <?php if ($g_partner_enabled == 1) { ?>
                                                                                                        <option value="1" <?php if ($action == "edit" && $edit_data["nns_type"] == 1) { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('partner'); ?></option>
                                                                                                <?php }if ($g_group_enabled == 1) { ?>
                                                                                                        <option value="2" <?php if ($action == "edit" && $edit_data["nns_type"] == 2) { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('group'); ?></option>
                                                                                                <?php } ?>
                                                                                </select>
                                                                                <?php if ($g_partner_enabled == 1) { ?>
                                                                                        <div id="nns_partner_control">
                                                                                                <input name="nns_partner_input" id="nns_partner_input" type="text" value="<?php echo $partner_data["nns_name"]; ?>" style="width:30%;" readonly="true" disabled="false"/>&nbsp;&nbsp;<input onclick="begin_select_partner();" name="" type="button" value="<?php echo cms_get_lang('search|partner') ?>"/>
                                                                                        </div>
                                                                                <?php } if ($g_group_enabled == 1) { ?>
                                                                                        <div id="nns_group_control">
                                                                                                <input name="nns_group_input" id="nns_group_input" type="text" value="<?php echo $group_data["nns_name"]; ?>" style="width:30%;" readonly="true"  disabled="false"/>&nbsp;&nbsp;<input name="" type="button" value="<?php echo cms_get_lang('search|group_jtxx') ?>" onclick="begin_select_group();"/>
                                                                                        </div>
                                                                                <?php } ?>


                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                        <td class="rightstyle"><?php echo cms_get_lang('media_tjfs'); ?>:</td>
                                                                        <td>
                                                                                <span class="p_rate" id="p_rate">
                                                                                        <i title="1<?php echo cms_get_lang('point'); ?>"></i>
                                                                                        <i title="2<?php echo cms_get_lang('point'); ?>"></i>
                                                                                        <i title="3<?php echo cms_get_lang('point'); ?>"></i>
                                                                                        <i title="4<?php echo cms_get_lang('point'); ?>"></i>
                                                                                        <i title="5<?php echo cms_get_lang('point'); ?>"></i>
                                                                                </span>
                                                                                <script>
                                var Rate = new pRate("p_rate", function() {
                                        $("#nns_point").val(Rate.Index);
                                });
        <?php if ($action == "edit") { ?>
                                        Rate.setindex(<?php echo $edit_data["nns_point"]; ?>);
        <?php } ?>
                                                                                </script>
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                        <td class="rightstyle"><?php echo cms_get_lang('media_jmdsz'); ?>:</td>
                                                                        <td>
                                                                                <select name="play_bill_type" id="play_bill_type"  style="margin:5px 0px;">
                                                                                        <option value="auto" <?php if ($action == "edit" && $play_bill_type == 'auto') { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('new_default'); ?></option>
                                                                                        <option value="custom" <?php if ($action == "edit" && $play_bill_type == 'custom') { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('new_custom'); ?></option>
                                                                                        <option value="default" <?php if ($action == "edit" && $play_bill_type == 'default') { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('new_auto'); ?></option>

                                                                                </select>
                                                                        </td>

                                                                </tr>
                                                                <tr>
                                                                        <td class="rightstyle"><?php echo cms_get_lang('zdgl_zdscbj'); ?>:</td>
                                                                        <td>
                                                                                <div class="radiolist" id="tag_list">

                                                                                        <div class="radiogroup">
                                                                                                <?php if ($g_mode != "xhpm") { ?>
                                                                                                        <div class="radioitem" >
                                                                                                                <?php echo get_tag_input_box(NNS_PLAYER_STD_TAG, $tar_str); ?>
                                                                                                        </div>
                                                                                                        <div class="radioitem" >
                                                                                                                <?php echo get_tag_input_box(NNS_PC_TAG, $tar_str); ?>
                                                                                                        </div>
                                                                                                        <div class="radioitem" >
                                                                                                                <?php echo get_tag_input_box(NNS_ANDROID_PAD_TAG, $tar_str); ?>
                                                                                                        </div>
                                                                                                        <div class="radioitem" >
                                                                                                                <?php echo get_tag_input_box(NNS_ANDROID_PHONE_TAG, $tar_str); ?>
                                                                                                        </div>
                                                                                                        <div class="radioitem" >
                                                                                                                <?php echo get_tag_input_box(NNS_APPLE_PAD_TAG, $tar_str); ?>
                                                                                                        </div>
                                                                                                        <div class="radioitem" >
                                                                                                                <?php echo get_tag_input_box(NNS_APPLE_PHONE_TAG, $tar_str); ?>
                                                                                                        </div>

                                                                                                <?php } ?>

                                                                                                <?php for ($device_num = 0; $device_num < $g_manager_list_device_num; $device_num++) { ?>

                                                                                                        <div class="radioitem">
                                                                                                                <?php echo get_tag_input_box($device_num, $tar_str); ?>
                                                                                                        </div>
                                                                                                <?php } ?>

                                                                                                <div style="clear:both;"/>
                                                                                        </div>
                                                                                </div>
                                                                        </td>
                                                                </tr>
                                                                <!--<tr>
                                                                                    <td class="rightstyle"><?php echo cms_get_lang('media_copyright_date'); ?></td>
                                                                                    <td>
                                                                                                        <input name="nns_copyright_date" id="nns_copyright_date" type="text" value="<?php
                                                                if ($action == "edit") {
                                                                        echo $edit_data["nns_copyright_date"];
                                                                }
                                                                ?>" value="2006-1-1" readonly="readonly"/>
                                                                                    </td>
                                                                </tr>-->


                                                                                                                                                                                                                                    <!--直播分镜功能<tr>
                                                                                                                                                                                                                                                        <td width="120"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
                                                                                                                                                                                                                                                        <td>
                                                                                                                                                                                                                                                                            <input name="nns_live_view_len" id="nns_live_view_len" type="text" rule="noempty|int"  value="1"/>
                                                                                                                                                                                                                                                        </td>
                                                                                                                                                                                                                                    </tr>-->

                                                                <tr>
                                                                        <td class="rightstyle"><?php echo cms_get_lang('media_dq'); ?>:</td>
                                                                        <td>
                                                                                <input name="nns_live_area" id="nns_live_area" type="text" value="<?php
                                                                                if ($action == "edit") {
                                                                                        echo $edit_data["nns_area"];
                                                                                }
                                                                                ?>"/>
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                        <td class="rightstyle"><?php echo cms_get_lang('zyk_play_count'); ?>:</td>
                                                                        <td>
                                                                                <input name="nns_play_count" id="nns_play_count" type="text" value="<?php
                                                                                if ($action == "edit") {
                                                                                        echo $edit_data["nns_play_count"];
                                                                                }
                                                                                ?>"/>
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                        <td class="rightstyle"><?php echo cms_get_lang('media_zbyz'); ?>:</td>
                                                                        <td>
                                                                                <input name="nns_language" id="nns_language" type="text" value="<?php
                                                                                if ($action == "edit") {
                                                                                        echo $edit_data["nns_language"];
                                                                                }
                                                                                ?>"/>
                                                                        </td>
                                                                </tr>
                                                                <?php if(!empty($cp_list)){?>
                                                                <tr>
                                                                        <td class="rightstyle">内容提供商ID:</td>
                                                                        <td>
                                                                            <select name="nns_cp_id" id="nns_cp_id" style="margin:5px 0px;">
                                                                            <?php foreach ($cp_list as $cp){
                                                                                    $str = '';
                                                                                    if ($action == "edit" && $edit_data['nns_cp_id'] == $cp['nns_id']) {
                                                                                        $str = 'selected="selected"';
                                                                                    }
                                                                                    echo '<option value="'. $cp['nns_id'] .'" '. $str .'>'. $cp['nns_name'] .'</option>';
                                                                                }
                                                                            ?>
                                                                            </select>
                                                                        </td>
                                                                </tr>
                                                                <?php }else{
                                                                	
                                                                	?>
                                                                		<input name="nns_cp_id" id="nns_cp_id" type="text" value='0'/>
                                                                	<?php 
                                                                }?>
                                                                <tr>
                                                    					  <td class="rightstyle" width="120">选择内容提供商</td>
                                                    					  <td>
                                                    						  <select name="nns_import_source" id="nns_import_source">
                                                    						      
                                                    							  
                                                    						  </select>
                                                    					  </td>
                                                    				  </tr>
                                                    			<tr>
                                                                <tr>
                                                                    <td class="rightstyle" width="120">注入ID</td>
                                                                    <td>
                                                                        <input type="text" name="nns_import_id" id="nns_import_id" value="<?php if($action=='edit'){echo $edit_data['nns_import_id'];} ?>" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                        <td class="rightstyle"><?php echo cms_get_lang('media_nrjj'); ?>:</td>
                                                                        <td>
                                                                                <textarea name="nns_live_summary" id="nns_live_summary" cols="" rows=""><?php
                                                                                        if ($action == "edit") {
                                                                                                echo $edit_data["nns_summary"];
                                                                                        }
                                                                                        ?></textarea>&nbsp;&nbsp;
                                                                                <!--<input name="moreinfo_chk" id="moreinfo_chk" type="checkbox" onclick="set_show_minor();" /> <font style="color:#0000ff">更多信息</a>-->
                                                                        </td>
                                                                </tr>



                                                        </tbody>
                                                </table>
                                        </div>
                                </form>
                                <div class="controlbtns">
                                        <div class="controlbtn <?php echo $action; ?>"><a href="javascript:$('#nns_tags').attr('value',getCheckedData(true,$('#tag_list')));checkForm('<?php echo $language_manager_dbgl; ?>','<?php echo cms_get_lang('msg_ask_change'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
                                        <div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back'); ?></a></div>
                                        <div style="clear:both;"></div>
                                </div>
                        </div>
                        <script>
                                shiyiShow();
                                function shiyiShow() {

                                        if ($('.tstv').attr('checked') == 'checked') {
                                                $('.shiyi').show();
                                        } else {
                                                $('.shiyi').hide();
                                        }
                                         window.parent.checkheight();
                                        window.parent.resetFrameHeight();
                                }
                                function checkbackrecord() {
                                    var nubmer = $('#look_back').val();
                                    if (nubmer == '') {
                                        return true;
                                    }
                                    if($('#look_back_type').val() == 1)
                                    {
                                        var reg = /^\d+\.?\d{0,1}$/;
                                        var notice = '<?php echo '请输入大于0的数,小数只能一位';?>';
                                    }
                                    else
                                    {
                                        var reg = /^\d+$/;
                                        var notice = '<?php echo '请输入自然数';?>';
                                    }
                                    if (!reg.test(nubmer)) {
                                        alert(notice);
                                        return false;
                                    }
                                }
                                $("#look_back_type").change(function(){
                                    var type = $(this).val();
                                    var natural_days_notice = "<?php echo '回看天数内容结尾时间为从请求数据时开始计算前n天';?>";
                                    var relative_days_notice = "<?php echo '回看天数内容结尾时间为从请求数据时-1天开始计算前n天';?>";

                                    var obj = $("#look_back_type_notice");
                                    if(type == 1)
                                    {
                                        obj.html(relative_days_notice);
                                    }
                                    else
                                    {
                                        obj.html(natural_days_notice);
                                    }
                                });
                        </script>
                </body>
        </html>
<?php } ?>


