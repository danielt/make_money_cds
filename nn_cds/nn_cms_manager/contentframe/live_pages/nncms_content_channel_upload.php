<?php
set_time_limit(0);
error_reporting(0);
header("Content-Type:text/html;charset=utf-8");
//ob_start();
include("../../nncms_manager_inc.php");

include_once LOGIC_DIR . 'playbill' . DIRECTORY_SEPARATOR . 'playbill_item.class.php';
include_once LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'live_bo.class.php';
include_once LOGIC_DIR . 'log' . DIRECTORY_SEPARATOR . 'manager_log.class.php';

//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language_media.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_pinyin_class.php";
include $nncms_db_path . "nns_live/nns_db_live_class.php";
include $nncms_db_path . "nns_live/nns_db_live_ex_class.php";

if ($_GET["nns_org_id"]=="" || $_GET["nns_org_type"]==""){
	$nns_org_id=$_SESSION["nns_org_id"];
	$nns_org_type=$_SESSION["nns_manager_type"];
}else{
	$nns_org_id=$_GET["nns_org_id"];
	$nns_org_type=$_GET["nns_org_type"];
}
include($nncms_db_path. "nns_depot/nns_db_depot_class.php");
$depot_inst=new nns_db_depot_class();
$depot_array=$depot_inst->nns_db_depot_list("",$nns_org_type,$nns_org_id,1);
// var_dump($partner_array);
if ($depot_array["ret"]!=0){
	$data=null;
	echo "<script>alert(". $depot_array["reason"].");</script>";
}
$depot_id=$depot_array["data"][0]["nns_id"];
// Define a destination
$targetFolder = dirname(__FILE__); // Relative to the root
$times = isset($_POST['timestamp']) ? $_POST['timestamp'] : '';
$verifyToken = md5('unique_salt' . $times);
$fileTypes = array('csv'); // File extensions
$arr_reason = array();
if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
        $targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
        //$targetPath=$targetFolder;
        $targetFile = rtrim($targetPath, '/') . '/' . $_FILES['Filedata']['name'];
        // Validate the file type
        if(!in_array(get_extension($_FILES['Filedata']['name']), $fileTypes))
        {
        	exit('file is not csv');
        }
        $file = fopen($_FILES['Filedata']['tmp_name'],'r');
        $i=0;
        while ($data = fgetcsv($file)) { //每次读取CSV里面的一行内容
        	//print_r($data); //此为一个数组，要获得每一个数据，访问数组下标即可
        	$i++;
        	if($i===1)
        	{
        		continue;
        	}
        	$arr_tag=array_filter(explode(',', $data[6]));
        	if(empty($arr_tag))
        	{
        		$arr_tag=array('26');
        	}
        	$live_info_array = array(
        			'director'=>null,
        			"actor" => null,
        			"show_time" => null,
        			"view_len" => null,
        			"all_index" => null,
        			"new_index" => null,
        			"area" => null,
        			"alias_name" => $data[2],
        			'nns_play_count' => 0,
        			"language" => null,
        			"eng_name" => null,
        	);
        	$res=add_channel($data[0],$data[1],$depot_id,$data[8],$data[4],$data[7],$data[3],$arr_tag,$live_info_array,$data[5]);
        	if($res['ret'] != '0')
        	{
        		$arr_reason[]=$res['reason'];
        	}
        }
        fclose($file);
        if(empty($arr_reason))
        {
        	exit('import file ok !');
        }
        exit("部分文件导入成功！<br/>".implode("<br/>", $arr_reason));
}
else
{
	exit('file uplode error');
}
function get_extension($file)
{
	return trim(pathinfo($file, PATHINFO_EXTENSION));
}

function add_channel($nns_live_name,$pindao_name,$depot_id,$depot_detail_id,$nns_play_bill_type,$nns_live_type,$nns_pid,$tag_array,$live_info,$org_id)
{
	
	$live_ex_inst = new nns_db_live_ex_class();
	$live_inst = new nns_db_live_class();
	//检查频道号
	$live_pindao = $live_ex_inst->nns_db_live_ex_infos_v2(null, 'live_pindao_order', $pindao_name);
	if (count($live_pindao['data']) > 0) {//频道号存在
		$live_name = $live_inst->nns_db_live_info($live_pindao['data'][0]['nns_live_id'], 'nns_name');
		$live_pindao_name = $live_name['data'][0]['nns_name'];
		return array(
				'ret'=>1,
				'reason'=>"频道号($pindao_name)已被($live_pindao_name)绑定"
		);
	}
	$pinyin = nns_controls_pinyin_class::get_pinyin_letter($nns_live_name);
	$result = $live_inst->nns_db_live_add($nns_live_name, 1, $depot_id, $depot_detail_id, 0, $org_id, $tag_array, $live_info, null, null,
			 null, null, $pinyin,1);
	$nns_id = $result["data"]["id"];
	$live_ex_inst->nns_db_live_set_value($nns_id, 'play_bill_type', $nns_play_bill_type);
	$live_ex_inst->nns_db_live_set_value($nns_id, 'live_pindao_order', $pindao_name);
	$live_ex_inst->nns_db_live_set_value($nns_id, 'live_type', $nns_live_type);
	$live_ex_inst->nns_db_live_set_value($nns_id, 'pid', $nns_pid);
	return array(
			'ret'=>0,
			'reason'=>'ok'
	);
}