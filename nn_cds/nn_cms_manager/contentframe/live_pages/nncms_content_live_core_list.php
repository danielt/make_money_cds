<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
ob_start();
include("../../nncms_manager_inc.php");
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
// 导入多语言包
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/language_contant.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
include "../../../nn_logic/common/pager.class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107104");
$checkpri = null;
if (!$pri_bool)
        Header("Location: ../nncms_content_wrong.php");
$length = 12;

function getList($start = 1) {
        global $length, $g_core_url;
        $searchName = isset($_GET['nns_product_search']) ? $_GET['nns_product_search'] : '';
        $url = $g_core_url . '?func=get_live_content_list&page_index=' . $start . '&page_size=' . $length . '&live=1&id=&name=' . $searchName;
        $xml = @file_get_contents($url);
        $p = xml_parser_create();
        xml_parse_into_struct($p, $xml, $list);
        xml_parser_free($p);
        $newlist = array();
        foreach ($list as $v) {
                if (is_array($v['attributes']))
                        $newlist[$v['tag']][] = $v['attributes'];
                else
                        $newlist[$v['tag']][] = $v;
        }
        $count = $newlist['CONTENT_LIST'][0]['COUNT'];
        $list = $newlist['CONTENT'];
        if (!is_array($list))
                $list = array();
        $data['count'] = $count;
        $data['list'] = $list;
        return $data;
}

$list = getList();
$page = isset($_GET['page']) && $_GET['page'] > 1 ? $_GET['page'] : "1";
$url = "?nns_product_search={$searchName}";
$page = new nl_pager($list['count'], $length, $page, $url);
$list = getList($page->cur_page);
$list = $list['list'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../../js/cms_cookie.js"></script>

                <script language="javascript" src="../../js/checkinput.js.php"></script>

                <script language="javascript" src="../../js/table.js.php"></script>
                <script language="javascript" src="../../js/checkinput.js.php"></script>
                <script language="javascript" src="../../js/radiolist.js"></script>
                <script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
        </head>
        <body>
                <div class="content">
                        <div class="closebtn"><img src="../../images/topicon_08-topicon.png" /></div>
                        <div class="content_table">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <form action="nncms_content_live_core_list.php" class="searchform" method="get">
                                                <tr>
                                                        <td width="120">根据名称模糊查询:</td>
                                                        <td><input name="nns_product_search" id="nns_product_search" type="text"  value="<?php echo $_GET['nns_product_search'] ?>"   />&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" value="查询"/><input type="submit" class="backlist"  value="返回列表"/></td>
                                                </tr>
                                        </form>
                                        <tr>
                                                <td colspan="4">
                                                        <div class="content_table formtable">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <thead>
                                                                                <tr>

                                                                                        <th>直播ID</th>
                                                                                        <th>直播名称</th>
                                                                                        <th>文件类型</th>
                                                                                </tr>
                                                                        </thead>
                                                                        <tbody>
<?php foreach ($list as $v) { ?>
                                                                                        <tr class="">  
                                                                                                <td><input name="<?php echo $v['NAME'] ?>" ids="<?php echo $v['ID'] ?>"  type="hidden" value="" /><?php echo $v['ID'] ?></td><td><?php echo $v['NAME'] ?></td><td><?php echo $v['FILETYPE'] ?></td></tr>
<?php } ?>
                                                                                <script language="javascript">
                                                                                        create_empty_tr($(".formtable"), 0, <?php echo 12 - count($list) ?>);
                                                                                </script>
                                                                        </tbody>
                                                                </table>
                                                        </div>
                                                </td>
                                        </tr>
                                </table>
                        </div>
<?php echo $page->nav_1(); ?>

                </div>
                <script>
                        $('.backlist').click(function() {
                                $('#nns_product_search').val('');
                        })
                        $('input[type=submit]').click(function (){
                                
                                $('.searchform').submit();
                                
                        })

                        $('.closebtn').click(function() {
                                window.parent.close_select();
                        });

                        $('tr').click(function() {
                                var t = $(this).first('td').find('input');
                                var id = t.attr('ids');
                                var name = t.attr('name');
                                if (id == null) {
                                        return false;
                                }
                                window.parent.setLiveId(id, name);
                                return false;
                        });
                </script>
        </body>
</html>
