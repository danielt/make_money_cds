<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
    $language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
//引入v2新版
include $nncms_config_path . "v2/common.php";
\ns_core\m_load::load("ns_core.m_config");
global $g_bk_version_number;
$category_v2 = get_config_v2('g_category_v2');
$bk_version_number = ($g_bk_version_number == '1' && $category_v2 == 1)? true : false;
unset($g_bk_version_number);

$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107105");
$checkpri=null;

if ($_GET["nns_org_id"]=="" || $_GET["nns_org_type"]==""){
    $nns_org_id=$_SESSION["nns_org_id"];
    $nns_org_type= 1;
}else{
    $nns_org_id=$_GET["nns_org_id"];
    $nns_org_type= 1;
}
$nns_id=$_GET["nns_id"];
$action=$_GET["action"];
if (!$pri_bool){
    Header("Location: ../nncms_content_wrong.php");
}else{
    include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
    // echo $nncms_db_path. "nns_role/nns_db_role_class.php";
    include($nncms_db_path. "nns_depot/nns_db_depot_class.php");
    if ($bk_version_number)
    {
        $params = array(
            'nns_org_id' => $nns_org_id,
            'nns_type' => 1,
        );
        $depot_array = \nl_depot_v2::get_depot_list(\m_config::get_dc(), $params, null, null, false);
        if ($depot_array["ret"] != 0) {
            $data = null;
            echo "<script>alert('" . $depot_array["reason"] . "');</script>";
        }
        $data = $depot_array["data_info"];
    }
    else
    {
        $depot_inst=new nns_db_depot_class();
        $depot_array = $depot_inst->nns_db_depot_list("", $nns_org_type, $nns_org_id, 1);
        if ($depot_array["ret"] != 0) {
            $data = null;
            echo "<script>alert('" . $depot_array["reason"] . "');</script>";
        }
        $data = $depot_array["data"];
    }
    $depot_inst=null;
    $base_name=cms_get_lang('media_zb|service_pk');
    $base_id=10000;

    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
        <script language="javascript" src="../../js/dtree.js"></script>
        <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
        <script language="javascript" src="../../js/table.js.php"></script>
        <script language="javascript" src="../../js/checkinput.js.php"></script>
        <script language="javascript" src="../../js/category_count.js"></script>
        <script language="javascript">
            $(document).ready(function(){
                <?php if ($bk_version_number) {?>
                category.count('live',<?php echo '\'' . $data[0]['nns_depot_id'] . '\'' ?>);
                <?php }else{ ?>
                category.count('live',<?php echo '\'' . $data[0]['nns_id'] . '\'' ?>);
                <?php }?>
                $(".category_editbox").width($(".content").width()-400);
                $(window).resize(function(){
                    $(".category_editbox").width($(".content").width()-400);
                });

            });


            function select_tree_item(item_id,item_name,parent_id){
                if (item_id==<?php echo $base_id?>){
                    item_value=0;
                    $(".category_edit_box").find("#nns_category_detail_name").attr("disabled",true);
                    $(".category_edit_box").find("input[type='button']:eq(0)").attr("disabled",true);
                    $(".category_edit_box").find("input[type='button']:eq(1)").attr("disabled",true);
                    $(".category_edit_box").find("input[type='button']:eq(2)").attr("disabled",true);
                    $(".category_edit_box").find("input[type='button']:eq(3)").attr("disabled",true);
                }else{
                    item_value=item_id;
                    $(".category_edit_box").find("#nns_category_detail_name").attr("disabled",false);
                    $(".category_edit_box").find("input[type='button']:eq(0)").attr("disabled",false);
                    $(".category_edit_box").find("input[type='button']:eq(1)").attr("disabled",false);
                    $(".category_edit_box").find("input[type='button']:eq(2)").attr("disabled",false);
                    $(".category_edit_box").find("input[type='button']:eq(3)").attr("disabled",false);
                }
                $(".category_edit_box").show();
                $(".category_edit_box").find("#category_parent").val(parent_id);
                $(".category_edit_box").find("#nns_category_id").val(item_value);

                $(".category_edit_box").find(".category_edit_id").html(item_id);
                $(".category_edit_box").find("#nns_category_detail_name").val(item_name);
                $(".category_add_box").hide();
            }

            function add_category_command(t){


                if ($(".category_add_box").is(":hidden")){
                    $(".category_add_box").show();

                    $(".category_add_box").find("#category_parent").val($(".category_edit_box").find("#nns_category_id").val());
                    $(".category_add_box").find(".parent_category_box").html($(".category_edit_box").find("#nns_category_detail_name").val());
                    inputVal=$(t).val();
                    $(t).val("<?php echo cms_get_lang('cancel'); ?>");
                }else{
                    $(t).val(inputVal);
                    $(".category_add_box").hide();
                }
            }

            function order_node(action_str){

                $(".category_edit_box").find("#action").val(action_str);
                checkForm('<?php echo cms_get_lang('lmgl');?>','<?php echo cms_get_lang('msg_ask_change');?>',$('#edit_form'),"edit");
            }

            function edit_node(){

                $(".category_edit_box").find("#action").val("edit");
                checkForm('<?php echo cms_get_lang('media_zykgl');?>','<?php echo cms_get_lang('msg_ask_change');?>',$('#edit_form'),"edit");
            }
            function delete_node(){

                $(".category_edit_box").find("#action").val("delete");
                checkForm('<?php echo cms_get_lang('media_zykgl');?>','<?php echo cms_get_lang('msg_ask_change');?>',$('#edit_form'),"delete");
            }
        </script>
    </head>

    <body>
    <div class="content">
        <div class="content_position"><?php echo cms_get_lang('media_zykgl'),' > ',cms_get_lang('lmgl_nrlmgl');?></div>
        <div style="background-color:#FFF;">
            <div class="category_tree" style="height:400px;">

                <?php
                if ($data != null) {
                    $assist_content = $bk_version_number === true ? $data : $data[0]["nns_category"];
                } else {
                    $assist_content = null;
                }
                if ($bk_version_number)
                {
                    echo pub_func_get_tree_by_array($assist_content, $base_name, $base_id);
                }
                else
                {
                    echo pub_func_get_tree_by_html($assist_content, $base_name, $base_id);
                }
                ?>

            </div>
            <div class="category_editbox" >
                <div class="category_edit_box">
                    <form id="edit_form" action="nncms_content_live_tree_control.php" method="post">
                        <input name="action" id="action" type="hidden" value="" />
                        <input name="category_parent" id="category_parent" type="hidden" value="" />
                        <input name="nns_category_id" id="nns_category_id" type="hidden" value="" />
                        <input name="org_id" id="org_id" type="hidden" value="<?php echo $nns_org_id;?>" />
                        <input name="org_type" id="org_type" type="hidden" value="<?php echo $nns_org_type;?>" />

                        <div class="content_table">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td class="rightstyle2"><?php echo cms_get_lang('lmgl_nrlmid');?>:</td>
                                    <td class="category_edit_id"><</td>

                                </tr>
                                <tr>
                                    <td class="rightstyle2"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('lmgl_lmnrmc');?>:</td>
                                    <td><input name="nns_category_detail_name" id="nns_category_detail_name" type="text" rule="noempty"
                                               value=""
                                        /></td>

                                </tr>
                                <tr>
                                    <td class="rightstyle2"><?php echo cms_get_lang('action');?> </td>
                                    <td>

                                        <input name="" type="button" value="<?php echo cms_get_lang('edit');?>" onclick="edit_node();"/>&nbsp;&nbsp;
                                        <input name="" type="button" value="<?php echo cms_get_lang('delete');?>" onclick="delete_node();"/>&nbsp;&nbsp;
                                        <!--<input name="" type="button" value="--><?php //echo cms_get_lang('order_pre');?><!--" onclick="order_node('up');"/>&nbsp;&nbsp;-->
                                        <!--<input name="" type="button" value="--><?php //echo cms_get_lang('order_next');?><!--" onclick="order_node('down');"/>&nbsp;&nbsp;-->
                                        <input name="" type="button" value="<?php echo cms_get_lang('add|lmgl_tjzlm');?>" onclick="add_category_command(this);"/>&nbsp;&nbsp;


                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
                <div class="category_add_box">
                    <form id="add_form" action="nncms_content_live_tree_control.php" method="post">
                        <input name="action" id="action" type="hidden" value="add" />
                        <input name="baseid" id="baseid" type="hidden" value="<?php echo $base_id;?>" />
                        <input name="category_parent" id="category_parent" type="hidden" value="" />
                        <input name="org_id" id="org_id" type="hidden" value="<?php echo $nns_org_id;?>" />
                        <input name="org_type" id="org_type" type="hidden" value="<?php echo $nns_org_type;?>" />
                        <div class="content_table">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td class="rightstyle2"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('lmgl_sjlm');?>:</td>
                                    <td class="parent_category_box">

                                    </td>

                                </tr>
                                <tr>
                                    <td class="rightstyle2"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('lmgl_nrlmid');?>:</td>
                                    <td><input name="nns_category_detail_id" id="nns_category_detail_id" type="text" rule="noempty|int" len="3" style="width:20%" maxlength="3"
                                               value=""
                                        /></td>

                                </tr>
                                <tr>
                                    <td class="rightstyle2"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('lmgl_lmnrmc');?>:</td>
                                    <td><input name="nns_category_detail_name" id="nns_category_detail_name" type="text" rule="noempty"
                                               value=""
                                        /></td>

                                </tr>


                                <tr>
                                    <td class="rightstyle2"><?php echo cms_get_lang('action');?>:</td>
                                    <td>
                                        <input name="" type="button" value="<?php echo cms_get_lang('add');?>" onclick="checkForm('<?php echo cms_get_lang('lmgl');?>','<?php echo cms_get_lang('msg_ask_change');?>',$('#add_form'),'add');"/>&nbsp;&nbsp;
                                    </td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
            <div style="clear:both;"/>
        </div>


    </div>
    <script>
        var node=d.selectedNode;
        if (!node) node=0;
        var node_id=d.aNodes[node].id;
        var node_parent_id=d.aNodes[node].pid;
        var node_name=d.aNodes[node].name;
        if (node_id==0){
            node_id="10000";
        }
        if (node_parent_id==-1){
            node_parent_id=0;
        }
        //alert(node_id+":"+node_parent_id+":"+node_name);
        select_tree_item(node_id,node_name,node_parent_id);
    </script>
    </body>
    </html>
<?php }?>
