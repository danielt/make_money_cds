<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";


 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();

$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107107");
if ($_SESSION["nns_manager_type"]!=0) $pri_bool=false;
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");



 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
<link href="../../css/uploadify.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/radiolist.js"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<!--<script language="javascript" src="../../js/cms_datepicker.js"></script>-->
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/swfobject.js"></script>
<script language="javascript" src="../../js/muti_upload/jquery.uploadify.min.js"></script>

<script language="javascript">
var stauts_log='';
	$(document).ready(function(){
		<?php $timestamp = time();?>
			$('#playbill_import_file').uploadify({
				'formData'     : {
					'timestamp' : '<?php echo $timestamp;?>',
					'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',	
					'session_id':'<?php echo $_SESSION['nns_mgr_id'];?>'
				},
				'buttonText':'<?php echo cms_get_lang('media_xzdrwj');?>',
				'scriptAccess':'always',
				'fileDesc':'<?php echo cms_get_lang('media_qxzxmltxtwj');?>',
				'swf'      : 'uploadify.swf',
				'auto'         : true,
				'successTimeout': 99999,
				'uploader' : 'nncms_content_channel_upload.php',
				'onQueueComplete':function(data){
                                   
					$('#stauts_log').html(stauts_log);
					window.parant.resetFrameHeight();
				},
				'onUploadSuccess':function(file,data,response){
					var stauts_str='';
						stauts_str=file.name+' ----- ';
						stauts_str+='<span>'+data+'</span>';
					stauts_str+='<br>';
					stauts_log+=stauts_str;
			  
				}
			});
		
	});
	

</script>
</head>

<body>


<div class="content" style="background:#fff;">
	<div class="content_position"><?php echo cms_get_lang('jmdgl');?> > <?php echo cms_get_lang('media_playbill_drjmd');?></div>
	<form id="import_form" action="nncms_content_live_control.php" method="post"  enctype="multipart/form-data">
	<input type="hidden" name="nns_id" id="nns_id" value="<?php echo $nns_id;?>"/>
	<input type="hidden" name="action" id="action" value="import"/>
	<div class="content_table" style="overflow:auto; height:450px;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			  <tr>
				<td class="rightstyle"><?php echo cms_get_lang('media_channel_jmdwj');?></td>
				<td> <div style="padding-top:10px;"><input name="playbill_import_file" id="playbill_import_file" type="file" size="60%" multiple="true"  /></div> </td>
			</tr>
			<tr>
				<td class="rightstyle"><?php echo cms_get_lang('upload_state');?></td>
				<td ><div id="stauts_log"  style="min-height:40px;"></div></td>
			</tr>
		  </tbody>
		</table>
	</div>
	</form>
	<div class="controlbtns">
		<div class="controlbtn back"><a href="javascript:$('#playbill_import_file').uploadify('stop');"><?php echo cms_get_lang('cancel_upload');?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>
</body>
</html>
