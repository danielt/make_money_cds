<?php

/*
 * Created on 2012-3-2
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
@header("Content-Type:text/html;charset=utf-8");
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}

// 获取cache模块
include LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'live.class.php';
include LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'live_bo.class.php';
include LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'live_info_language.class.php';

//芒果TV专用接口
//include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'mgtv_live.php';

$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
$dc->open();
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//require_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_clear_cache.php";
require_once $nncms_db_path . "nns_log" . DIRECTORY_SEPARATOR . "nns_db_op_log_class.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/models/queue_task_model.php';
$log_inst = new nns_db_op_log_class();

$action = $_POST["action"];
$nns_id = $_POST["nns_id"];
$audit = $_POST["audit"];
$nns_url = $_POST["nns_url"];

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
if (!empty($action)) {
        switch ($action) {
                case "edit":
                        $action_str = cms_get_lang('edit|media_zb');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107101");
                        break;
                case "add":
                        $action_str = cms_get_lang('add|media_zb');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107100");
                        break;
                case "delete":
                        $action_str = cms_get_lang('delete|media_zb');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107102");
                        break;
                case "realdelete":
                        $action_str = cms_get_lang('real_delete|media_zb');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107102");
                        break;
                case "audit":
                        $action_str = cms_get_lang('audit|media_zb');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107103");
                        break;
                case "reset":
                        $action_str = cms_get_lang('reset|media_zb');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107102");
                        break;
                case "move_category":
                        $action_str = cms_get_lang('move_catalog|media_zb');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107101");
                        break;
                case "unline":
                        $action_str = cms_get_lang('media_wxx');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107101");
                        break;
                case "ctag":
                        $action_str = cms_get_lang('edit_epg_more');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107101");
                        break;
                default:
                        break;
        }
}
$checkpri = null;



include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/producer/producer.class.php';




if (!$pri_bool) {
        Header("Location: ../nncms_content_wrong.php");
} else {
        $nns_live_name = $_POST["nns_live_name"];
        $nns_source_url = $_POST['nns_source_url'];
        $nns_carrier = $_POST["nns_carrier"];
        $nns_partner_input = $_POST["nns_partner"];
        $nns_group_input = $_POST["nns_group"];
        $nns_live_category_public = $_POST["nns_live_category_public"];
        $nns_live_category_public_id = $_POST["nns_live_category_public_id"];
        $nns_live_category_private = $_POST["nns_live_category_private"];
        $nns_live_category_private_id = $_POST["nns_live_category_private_id"];
        $nns_live_directory = $_POST["nns_live_directory"];
        $nns_live_actor = $_POST["nns_live_actor"];
        $nns_live_show_time = $_POST["nns_live_show_time"];
        $nns_live_view_len = $_POST["nns_live_view_len"] * 60;
//	$nns_live_episode=$_POST["nns_live_episode"];
        $nns_live_episode = 1;
        $nns_live_area = $_POST["nns_live_area"];
//         $nns_tags = $_POST["nns_tags"];
        $_POST['nns_tag'] = isset($_POST['nns_tag']) ? $_POST['nns_tag'] : null;
        $tag_array = (is_array($_POST['nns_tag']) && !empty($_POST['nns_tag'])) ? $_POST['nns_tag'] : array('26');

        $nns_live_summary = $_POST["nns_live_summary"];
        $depot_id = $_POST["depot_id"];
        $depot_detail_id = $_POST["depot_detail_id"];
        $nns_point = $_POST["nns_point"];
        $nns_pinyin = $_POST["nns_pinyin"];
        $nns_copyright_date = $_POST["nns_copyright_date"];
        $nns_play_bill_type = $_POST["play_bill_type"];
        $nns_alias_name = $_POST["nns_alias_name"];
        $nns_language = $_POST["nns_language"];
        $nns_eng_name = $_POST["nns_eng_name"];
        $nns_dvb = $_POST["dvb"];
        $nns_back_record_day = $_POST['look_back'];
        $nns_back_record_day_type = $_POST['look_back_type'];
        $nns_pid = $_POST["pid"];
        $nns_begin = $_POST['begin'];
        $pindao_name = $_POST['nns_pindao_name'];
        $nns_length = $_POST['length'];
        $nns_ts_pos = $_POST['ts_pos'];
        $nns_cp_id = $_POST['nns_cp_id'];
        if (is_array($_POST["live_type"]))
                $nns_live_type = implode(',', $_POST["live_type"]);
//	$nns_live_new_episode=$_POST["nns_live_new_episode"];
        $nns_live_new_episode = 1;

        require_once $nncms_config_path . "nn_cms_manager/controls/nncms_control_memcache.php";

        //	判断管理类型
        if ($_SESSION["nns_manager_type"] == 0) {
                if ($nns_carrier == 1) {
                        $nns_org_id = $nns_partner_input;
                        $nns_org_type = $nns_carrier;
                } else if ($nns_carrier == 2) {
                        $nns_org_id = $nns_group_input;
                        $nns_org_type = $nns_carrier;
                } else {
                        $nns_org_id = $nns_carrier;
                        $nns_org_type = 0;
                }
        } else {
                $nns_org_id = $_SESSION["nns_org_id"];
                $nns_org_type = $_SESSION["nns_manager_type"];
        }

        switch ($nns_org_type) {
                case 0:
                        $nns_live_category_private = "";
                        $nns_live_category_private_id = "";
                        break;
                case 1:
                        if ($nns_live_category_private == 0) {
                                $nns_live_category_private = "";
                                $nns_live_category_private_id = "";
                        } else if ($nns_live_category_public == 0) {
                                $nns_live_category_public = "";
                                $nns_live_category_public_id = "";
                        }
                        break;
                case 2:
                        $nns_live_category_public = "";
                        $nns_live_category_public_id = "";
                        break;
        }

        $category_array = array("pub_category_id" => $nns_live_category_public,
            "pri_category_id" => $nns_live_category_private,
            "pub_category_detail_id" => $nns_live_category_public_id,
            "pri_category_detail_id" => $nns_live_category_private_id);
//         $tag_array = explode(",", $nns_tags);

        $live_info_array = array("director" => $nns_live_directory,
            "actor" => $nns_live_actor,
            "show_time" => $nns_live_show_time,
            "view_len" => $nns_live_view_len,
            "all_index" => $nns_live_episode,
            "new_index" => $nns_live_new_episode,
            "area" => $nns_live_area,
            "alias_name" => $nns_alias_name,
            "language" => $nns_language,
            "eng_name" => $nns_eng_name,
            'nns_play_count' => isset($_POST["nns_play_count"]) ? $_POST["nns_play_count"] : 0,
            'import_id' => isset($_POST['nns_import_id']) ? $_POST['nns_import_id'] : ''
        );

        include $nncms_db_path . "nns_common/nns_db_constant.php";
        include $nncms_db_path . "nns_live/nns_db_live_class.php";
        include $nncms_db_path . "nns_live/nns_db_live_ex_class.php";
        require_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_pinyin_class.php";
        $live_inst = new nns_db_live_class();
        $live_ex_inst = new nns_db_live_ex_class();
        switch ($action) {
                case "unline":
                        require_once $nncms_db_path . "nns_assist/nns_db_assist_item_class.php";
                        require_once $nncms_db_path . "nns_service/nns_db_service_category_class.php";
                        $asset_inst = new nns_db_assist_item_class();
                        $service_inst = new nns_db_service_category_class();
                        $asset_inst->nns_db_assist_item_unline($nns_id);
                        $service_inst->nns_db_service_category_unline($nns_id);
                        $asset_inst = null;
                        $service_inst = null;
                        $nns_names = $log_inst->nns_db_get_video_name_arr($nns_id);
                        if ($result["ret"] != 0) {
                                echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
                        } else {
                                $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_names, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                                $live_inst->clear_memcache_by_asset_service($nns_id);
                                echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
                        }
                        break;
                case "edit":
                	
	                	$live_pindao = $live_ex_inst->nns_db_live_ex_infos($nns_id, 'live_pindao_order', $pindao_name);
	                	if (count($live_pindao['data']) > 0) {//频道号存在
	                		$live_name = $live_inst->nns_db_live_info($live_pindao['data'][0]['nns_live_id'], 'nns_name');
	                		$live_pindao_name = $live_name['data'][0]['nns_name'];
	                		echo "<script>alert('频道号($pindao_name)已被($live_pindao_name)绑定');</script>";
	                		$result["ret"] = 1;
	                		break;
	                	}
	               
	                	//检查直播源地址
	                	$live_source_url = $live_ex_inst->nns_db_live_ex_infos($nns_id, 'source_url', $nns_source_url);
	                	if (count($live_source_url['data'])>0) {
	                		$live_name = $live_inst->nns_db_live_info($live_source_url['data'][0]['nns_live_id'], 'nns_name');
	                		$live_source_name = $live_name['data'][0]['nns_name'];
	                		echo "<script>alert('直播源地址($nns_source_url)已被($live_source_name)绑定');</script>";
	                		$result['ret'] = 1;
	                		break;
	                	}
	                	$live_ex_inst->nns_db_live_set_value($nns_id, 'live_pindao_order', $pindao_name);
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'back_record_day_type', $nns_back_record_day_type);
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'back_record_day', $nns_back_record_day);
/* 	                	$live_ex_inst->nns_db_live_set_value($nns_id, ' live_types ', $nns_live_types);
	                	$live_ex_inst->nns_db_live_set_value($nns_id, ' live_video_pid ', $nns_video_pid);
	                	$live_ex_inst->nns_db_live_set_value($nns_id, ' live_audio_pid ', $nns_audio_pid); */
                	
                        if (empty($nns_pinyin)) {
                                $pinyin = nns_controls_pinyin_class::get_pinyin_letter($nns_live_name);
                        } else {
                                $pinyin = $nns_pinyin;
                        }
                        $nns_import_source = $_POST['nns_import_source'];
                        if(strlen($nns_import_source) <1)
                        {
                            echo "<script language=javascript>alert('内容提供商不能为空');history.go(-1);</script>";die;
                        }
                        $result_producer = nl_producer::query_by_cp_source_id($dc, $nns_cp_id,$nns_import_source);
                        if($result_producer['ret'] !=0)
                        {
                            echo "<script language=javascript>alert('".$result_producer['reason']."');history.go(-1);</script>";die;
                        }
                        if(!isset($result_producer['data_info']) || empty($result_producer['data_info']) || !is_array($result_producer['data_info']))
                        {
                            echo "<script language=javascript>alert('查询内容发布商不存在');history.go(-1);</script>";die;
                        }
                        $live_info_array['import_source'] = $nns_import_source;
                        $live_info_array['producer'] = $result_producer['data_info']['nns_producer'];
                        $result = $live_inst->nns_db_live_modify($nns_id, $nns_live_name, 1, $depot_id, $depot_detail_id, $nns_org_type, $nns_org_id, $tag_array, $live_info_array, $nns_live_summary, $nns_point, $nns_copyright_date, null, $pinyin, $nns_cp_id);

                        $live_ex_inst->nns_db_live_set_value($nns_id, 'source_url', $nns_source_url);
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'play_bill_type', $nns_play_bill_type);
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'live_type', $nns_live_type);
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'dvb', $nns_dvb);
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'pid', $nns_pid);
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'ts_limit_min', $nns_begin);
                        if ($nns_length != '') {
                                $nns_length = $nns_length * 3600;
                        }
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'ts_limit_max', $nns_length);
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'ts_default_pos', $nns_ts_pos);
                        if ($result["ret"] != 0) {
                                echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
                        } else {
                                //	芒果TV专用BOSS接口
                                //notice_boss_live_add($nns_id, $nns_live_name, $nns_live_type);
//                                 $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_live_name, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
//                                 $live_inst->clear_memcache_by_asset_service($nns_id);
//                                 nl_live_bo::epg_delete_live_list_by_pid_cache($dc, $nns_pid);
//                                 nl_live::delete_live_info_by_cache($dc, $nns_id);
//                                 delete_cache("detail_$nns_id");
                                echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
                        }
                        break;
                case "add":
                	
                		//检查频道号
	                	$live_pindao = $live_ex_inst->nns_db_live_ex_infos($nns_id, 'live_pindao_order', $pindao_name);
	                	if (count($live_pindao['data']) > 0) {//频道号存在
	                		$live_name = $live_inst->nns_db_live_info($live_pindao['data'][0]['nns_live_id'], 'nns_name');
	                		$live_pindao_name = $live_name['data'][0]['nns_name'];
	                		echo "<script>alert('频道号($pindao_name)已被($live_pindao_name)绑定');</script>";
	                		$result["ret"] = 1;
	                		break;
	                	}
	                	//检查直播源地址
	                	$live_source_url = $live_ex_inst->nns_db_live_ex_infos($nns_id, 'source_url', $nns_source_url);
	                	if (count($live_source_url['data'])>0) {
	                		$live_name = $live_inst->nns_db_live_info($live_source_url['data'][0]['nns_live_id'], 'nns_name');
	                		$live_source_name = $live_name['data'][0]['nns_name'];
	                		echo "<script>alert('直播源地址($nns_source_url)已被($live_source_name)绑定');</script>";
	                		$result['ret'] = 1;
	                		break;
	                	}
                	
                        $pinyin = nns_controls_pinyin_class::get_pinyin_letter($nns_live_name);
                        $nns_import_source = $_POST['nns_import_source'];
                        if(strlen($nns_import_source) <1)
                        {
                            echo "<script language=javascript>alert('内容提供商不能为空');history.go(-1);</script>";die;
                        }
                        $result_producer = nl_producer::query_by_cp_source_id($dc, $nns_cp_id,$nns_import_source);
                        if($result_producer['ret'] !=0)
                        {
                            echo "<script language=javascript>alert('".$result_producer['reason']."');history.go(-1);</script>";die;
                        }
                        if(!isset($result_producer['data_info']) || empty($result_producer['data_info']) || !is_array($result_producer['data_info']))
                        {
                            echo "<script language=javascript>alert('查询内容发布商不存在');history.go(-1);</script>";die;
                        }
                        $live_info_array['import_source'] = $nns_import_source;
                        $live_info_array['producer'] = $result_producer['data_info']['nns_producer'];
                        $result = $live_inst->nns_db_live_add($nns_live_name, 1, $depot_id, $depot_detail_id, $nns_org_type, $nns_org_id, $tag_array, $live_info_array, $nns_live_summary, $nns_point, $nns_copyright_date, null, $pinyin, $g_video_audit_enabled, $nns_cp_id);
                        $nns_id = $result["data"]["id"];
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'source_url', $nns_source_url);
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'play_bill_type', $nns_play_bill_type);
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'live_pindao_order', $pindao_name);
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'live_type', $nns_live_type);
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'dvb', $nns_dvb);
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'pid', $nns_pid);
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'ts_limit_min', $nns_begin);
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'back_record_day_type', $nns_back_record_day_type);
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'back_record_day', $nns_back_record_day);
                        
                        if ($nns_length != '') {
                                $nns_length = $nns_length * 3600;
                        }
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'ts_limit_max', $nns_length);
                        $live_ex_inst->nns_db_live_set_value($nns_id, 'ts_default_pos', $nns_ts_pos);
                        if ($result["ret"] != 0) {
//                                 nl_live_bo::epg_delete_live_list_by_pid_cache($dc, $nns_pid);


                                echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
                                echo "<script>window.parent.refresh_tree_content();</script>";
                        } else {
//                                 $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_live_name, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);

                                //	芒果TV专用BOSS接口
//                                 notice_boss_live_add($nns_id, $nns_live_name, $nns_live_type);

                                echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
                        }
                        break;
                case "delete":
                        $delete_bool = true;
                        //	$role_inst->
                        $nns_ids = explode(",", $nns_id);
                        $nns_names = $log_inst->nns_db_get_video_name_arr($nns_id, 1);
                        $num = 0;
                        foreach ($nns_ids as $nnsid) {
                                if (!empty($nnsid)) {
                                        $num++;
                                        $resultarr = $live_inst->nns_db_live_delete($nnsid);
                                        if ($resultarr["ret"] != 0 && $num == 1) {
                                                $delete_bool = false;
                                                break;
                                        }
                                }
                        }
                        if (!$delete_bool) {
                                echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
                        } else 
                        {
                            $import_flag = true;
                            foreach ($nns_ids as $nnsid)
                            {
                                if (empty($nnsid)) 
                                {
                                    continue;
                                }
                                $result_media_info = nl_live_media::query_by_channel_id($dc, $nnsid);
                                if(!isset($result_media_info['data_info']) || empty($result_media_info['data_info']) || !is_array($result_media_info['data_info']))
                                {
                                    continue;
                                }
                                foreach ($result_media_info['data_info'] as $del_medis_id)
                                {
                                    if($import_flag)
                                    {
                                        $result_info_cp=nl_cp::query_by_id($dc, $del_medis_id['nns_cp_id']);
                                        $arr_cp_config['base_info'] = $arr_cp_config['base_config'] = null;
                                        if(isset($result_info_cp['data_info']['nns_config']) && strlen($result_info_cp['data_info']['nns_config']) >0)
                                        {
                                            $result_info_config = json_decode($result_info_cp['data_info']['nns_config'],true);
                                            $arr_cp_config['base_config'] = (is_array($result_info_config) && !empty($result_info_config)) ? $result_info_config : null;
                                        }
                                        if(isset($result_info_cp['data_info']['nns_config']))
                                        {
                                            unset($result_info_cp['data_info']['nns_config']);
                                        }
                                        if(is_array($result_info_cp['data_info']) && !empty($result_info['data_info']))
                                        {
                                            foreach ($result_info_cp['data_info'] as $data_info_key=>$data_info_val)
                                            {
                                                $arr_cp_config['base_info'][$data_info_key]=$data_info_val;
                                            }
                                        }
                                        $import_flag = false;
                                        if(!isset($arr_cp_config['base_config']['message_live_media_import_enable']) || $arr_cp_config['base_config']['message_live_media_import_enable']  != '1')
                                        {
                                            break;
                                        }
                                    }
                                    $queue_task_model = new queue_task_model($dc);
                                    $queue_task_model->q_add_task_op_mgtv($del_medis_id['nns_id'], BK_OP_LIVE_MEDIA, BK_OP_DELETE);
                                    unset($queue_task_model);
                                }
                            }
                            $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_names, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                            echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
                        }   
                        break;
                case "realdelete":
                        $delete_bool = true;
                        //	$role_inst->
                        $nns_ids = explode(",", $nns_id);
                        $nns_names = $log_inst->nns_db_get_video_name_arr($nns_id, 1);
                        $num = 0;
                        foreach ($nns_ids as $nnsid) {
                                if (!empty($nnsid)) {
                                        $num++;
                                        $resultarr = $live_inst->nns_db_live_real_delete($nnsid);
                                        if ($resultarr["ret"] != 0 && $num == 1) {
                                                $delete_bool = false;
                                                break;
                                        }
                                        $live_ex_inst->nns_db_live_ex_clean_by_live_id($nnsid);

                                        //	芒果TV专用BOSS接口
                                        notice_boss_live_del($nns_id);

                                        nl_live_info_language::delete_live_info_by_language($dc, $nnsid);
                                }
                        }

                        if (!$delete_bool) {
                                echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
                        } else {
                            $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_names, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                            echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
                        }
                        break;
                case "reset":
                        $reset_bool = true;
                        //	$role_inst->
                        $nns_ids = explode(",", $nns_id);
                        $nns_names = $log_inst->nns_db_get_video_name_arr($nns_id, 1);
                        $num = 0;
                        foreach ($nns_ids as $nnsid) {
                                if (!empty($nnsid)) {
                                        $num++;
                                        $resultarr = $live_inst->nns_db_live_retrieve($nnsid);
                                        if ($resultarr["ret"] != 0 && $num == 1) {
                                                $reset_bool = false;
                                                break;
                                        }
                                }
                        }
                        if (!$reset_bool) {
                                echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
                        } else {
                            $import_flag = true;
                            foreach ($nns_ids as $nnsid)
                            {
                                if (empty($nnsid))
                                {
                                    continue;
                                }
                                $result_media_info = nl_live_media::query_by_channel_id($dc, $nnsid);
                                if(!isset($result_media_info['data_info']) || empty($result_media_info['data_info']) || !is_array($result_media_info['data_info']))
                                {
                                    continue;
                                }
                                foreach ($result_media_info['data_info'] as $del_medis_id)
                                {
                                    if($import_flag)
                                    {
                                        $result_info_cp=nl_cp::query_by_id($dc, $del_medis_id['nns_cp_id']);
                                        $arr_cp_config['base_info'] = $arr_cp_config['base_config'] = null;
                                        if(isset($result_info_cp['data_info']['nns_config']) && strlen($result_info_cp['data_info']['nns_config']) >0)
                                        {
                                            $result_info_config = json_decode($result_info_cp['data_info']['nns_config'],true);
                                            $arr_cp_config['base_config'] = (is_array($result_info_config) && !empty($result_info_config)) ? $result_info_config : null;
                                        }
                                        if(isset($result_info_cp['data_info']['nns_config']))
                                        {
                                            unset($result_info_cp['data_info']['nns_config']);
                                        }
                                        if(is_array($result_info_cp['data_info']) && !empty($result_info['data_info']))
                                        {
                                            foreach ($result_info_cp['data_info'] as $data_info_key=>$data_info_val)
                                            {
                                                $arr_cp_config['base_info'][$data_info_key]=$data_info_val;
                                            }
                                        }
                                        $import_flag = false;
                                        if(!isset($arr_cp_config['base_config']['message_live_media_import_enable']) || $arr_cp_config['base_config']['message_live_media_import_enable']  != '1')
                                        {
                                            break;
                                        }
                                    }
                                    $queue_task_model = new queue_task_model($dc);
                                    $str_action = ($del_medis_id['nns_modify_time'] > $del_medis_id['nns_create_time']) ? BK_OP_MODIFY : BK_OP_ADD;
                                    $queue_task_model->q_add_task_op_mgtv($del_medis_id['nns_id'], BK_OP_LIVE_MEDIA, $str_action);
                                    unset($queue_task_model);
                                }
                            }
                            $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_names, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                            echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
                        }
                        break;
                case "audit":
                        $audit_bool = true;
                        //	$role_inst->
                        $nns_ids = explode(",", $nns_id);
                        $nns_names = $log_inst->nns_db_get_video_name_arr($nns_id, 1);
                        $num = 0;
                        foreach ($nns_ids as $nnsid) {
                                if (!empty($nnsid)) {
                                        $num++;

                                        $resultarr = $live_inst->nns_db_live_check($nnsid, $audit);

                                        if ($resultarr["ret"] != 0 && $num == 1) {
                                                $audit_bool = false;
                                                break;
                                        }
                                }
                        }
                        if (!$audit) {
                                $action_str = cms_get_lang('cancel') . $action_str;
                        }
                        if (!$audit_bool) {
                                echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
                        } else {
                                $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_names, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                                echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
                        }
                        break;
                case "move_category":
                        $depot_id = $_POST["move_depot_id"];
                        $category_id = $_POST["move_category_id"];
                        $delete_bool = true;
                        //	$role_inst->
                        $nns_ids = explode(",", $nns_id);
                        $nns_names = $log_inst->nns_db_get_video_name_arr($nns_id, 1);
                        $num = 0;
                        foreach ($nns_ids as $nnsid) {
                                if (!empty($nnsid)) {
                                        $num++;
                                        $resultarr = $live_inst->nns_db_live_move_depot_category($nnsid, $depot_id, $category_id);
                                        if ($resultarr["ret"] != 0 && $num == 1) {
                                                $delete_bool = false;
                                                break;
                                        }
                                }
                        }
                        if (!$delete_bool) {
                                echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
                        } else {
                                $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_names, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                                echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
                        }
                        break;
                //		批量修改EPG标识 BY S67 2013/5/13
                case 'ctag':
                        $nns_tag = $_POST["nns_tags"];
                        //	$role_inst->
                        $nns_ids = explode(",", $nns_id);
                        $nns_names = $log_inst->nns_db_get_video_name_arr($nns_id);
                        $num = 0;
                        $result = $live_inst->edit_epg_tags($nns_ids, $nns_tag);

                        if ($result['ret'] != 0) {
                                echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
                        } else {
                                $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], "edit", $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_names, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                                echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
                        }
                        break;
                default:

                        break;
        }
        //var_dump($result);
        $category_inst = null;
        $log_inst = null;
        if ($action == "delete" || $action == "audit" || $action == "reset" || $action == "realdelete" || $action == "move_category" || $action == "unline" || $action == "edit" || $action == 'ctag') {
                echo "<script>window.parent.refresh_tree_content();</script>";
        } else {
                echo "<script>self.location='../live_media_pages/nncms_content_live_media_list.php?nns_id=" . $nns_id . "';</script>";
        }
}
?>
