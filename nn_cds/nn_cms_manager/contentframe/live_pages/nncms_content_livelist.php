<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
ob_start();
include("../../nncms_manager_inc.php");
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
// 导入多语言包
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/language_contant.php";

//op=cdn&sp_id=<?php echo g_cms_config::get_g_config_value('g_sp_org_id');\>&video_id=<?php echo $item["nns_id"]; >&op_action=add

/**
 * 
 * 
 * 注入到epg==============================================
 * 
 * 
 * 
 */
// include_once $nncms_config_path . 'mgtv/mgtv_init.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/models/import_model.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/log_model/nn_log.class.php';

if(isset($_GET['op'])){
	include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cdn/cdn_live_media_queue.class.php';
	include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cdn/cdn_playbill_queue.class.php';
	include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/live/live.class.php';
	include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/live/live_index.class.php';
	include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/live/live_media.class.php';
	include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/live/playbill.class.php';
	$db = nn_get_db(NL_DB_READ);
	$live_id = $_GET['video_id'];
	if(empty($live_id)||$_GET['sp_id']!=g_cms_config::get_g_config_value('g_sp_org_id')){
		$re_str = '操作失败';
		echo '<script>alert("'.$re_str.'");history.go(-1);</script>';die;
	}
	$sql = "select * from nns_live where nns_id='$live_id' and nns_deleted!=1";
	$live_info = nl_db_get_one($sql, $db);
	if($live_info===false||$live_info===null){
		$re_str = '操作失败';
		echo '<script>alert("'.$re_str.'");history.go(-1);</script>';die;
	}
	$dc = nl_get_dc(array (
			'db_policy' => NL_DB_WRITE,
			'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
	));
	unset($live_info['nns_category_id']);
	if($_GET['op']=='cdn'){
		$live_info['nns_task_name'] = $live_info['nns_name'];
		$re = false;
		switch ($_GET['op_action']) {
			case 'add':
				$re = c2_task_model::add_live($live_info);
			break;
			case 'modify':
				$re = c2_task_model::update_live($live_info);
			break;
			case 'destroy':
				$re = c2_task_model::delete_live($live_info);
			break;
		}
		
		if($re){
			$re_str = '操作成功';
		}else{
			$re_str = '操作失败';
		}
		echo '<script>alert("'.$re_str.'");history.go(-1);</script>';
		
	}
	else if($_GET['op']=='cdn_live')
	{
		define('ORG_ID', 'unicom_zte');
		$live_id = $_GET['video_id'];
		$arr_live = nl_live::query_by_id($dc, $live_id);
		if($arr_live['ret'] !=0)
		{
			echo '<script>alert("'.$arr_live['reason'].'");history.go(-1);</script>';die;
		}
		if(!isset($arr_live['data_info']) || empty($arr_live['data_info']) || !is_array($arr_live['data_info']))
		{
			echo '<script>alert("查无直播数据:'.$live_id.'");history.go(-1);</script>';die;
		}
		$arr_live = $arr_live['data_info'];
		$arr_index = nl_live_index::query_by_live_id($dc, $arr_live['nns_id']);
		if($arr_index['ret'] !=0)
		{
			echo '<script>alert("'.$arr_index['reason'].'");history.go(-1);</script>';die;
		}
		if(!isset($arr_index['data_info']) || empty($arr_index['data_info']) || !is_array($arr_index['data_info']))
		{
			echo '<script>alert("查无直播分集数据:'.$arr_live['nns_id'].'");history.go(-1);</script>';die;
		}
		$arr_index = $arr_index['data_info'];
		$last_queue_info=array();
		foreach ($arr_index as $index_val)
		{
			$arr_media = nl_live_media::query_by_live_index_id($dc, $arr_live['nns_id'],$index_val['nns_id']);
			if($arr_media['ret'] !=0)
			{
				echo '<script>alert("'.$arr_media['reason'].'");history.go(-1);</script>';die;
			}
			if(!isset($arr_media['data_info']) || empty($arr_media['data_info']) || !is_array($arr_media['data_info']))
			{
				continue;
			}
			$index_val_index = $index_val['nns_index']+1;
			$arr_media = $arr_media['data_info'];
			foreach ($arr_media as $media_val)
			{
				$arr_queue=nl_cdn_live_media_queue::query_queue_is_exsist($dc,$media_val['nns_live_id'],$media_val['nns_live_index_id'],$media_val['nns_id'],$media_val['nns_cp_id']);
				if($arr_queue['ret'] !=0)
				{
					echo '<script>alert("'.$arr_queue['reason'].'");history.go(-1);</script>';die;
				}
				if(isset($arr_queue['data_info']) && is_array($arr_queue['data_info']) && !empty($arr_queue['data_info']))
				{
					$array_edit_queue = array();
					$arr_queue = $arr_queue['data_info'];
					if(($arr_queue['nns_action'] == 0 || $arr_queue['nns_action'] == 1) && $media_val['nns_import_cdn_state'] == 0)
					{
						$array_edit_queue['nns_import_cdn_state'] = 0;
						$array_edit_queue['nns_import_cms_state'] = 0;
						$array_edit_queue['nns_import_notify_state'] = 0;
						$array_edit_queue['nns_action'] = 1;
						$last_queue_info[]=$arr_queue['nns_id'];
					}
					else if($arr_queue['nns_action'] == 2 && $media_val['nns_deleted'] != 0)
					{
						$array_edit_queue['nns_import_cdn_state'] = 0;
						$array_edit_queue['nns_import_cms_state'] = 0;
						$array_edit_queue['nns_import_notify_state'] = 0;
						$array_edit_queue['nns_action'] = 2;
						$last_queue_info[]=$arr_queue['nns_id'];
					}
					else
					{
						continue;
					}
					$result_edit_queue=nl_cdn_live_media_queue::edit($dc, $array_edit_queue,$arr_queue['nns_id']);
				}
				else
				{
					if($media_val['nns_deleted'] !=0)
					{
						continue;
					}
					$queue_guid = np_guid_rand();
					$array_add_queue = array(
							'nns_id'=>$queue_guid,
							'nns_name'=>"[{$live_info['nns_name']}] [第{$index_val_index}集] [{$media_val['nns_mode']}]",
							'nns_live_id'=>$media_val['nns_live_id'],
							'nns_live_index_id'=>$media_val['nns_live_index_id'],
							'nns_live_media_id'=>$media_val['nns_id'],
							'nns_action'=>0,
							'nns_import_cdn_state'=>0,
							'nns_import_cdn_time'=>0,
							'nns_import_cms_state'=>0,
							'nns_import_cms_time'=>0,
							'nns_import_notify_state'=>0,
							'nns_import_notify_time'=>0,
							'nns_message_id'=>$media_val['nns_message_id'],
							'nns_cp_id'=>$media_val['nns_cp_id'],
					);
					$result_add_queue=nl_cdn_live_media_queue::add($dc, $array_add_queue);
					if($result_add_queue['ret'] !=0)
					{
						echo '<script>alert("'.$result_add_queue['reason'].'");history.go(-1);</script>';die;
					}
					$last_queue_info[]=$queue_guid;
				}
			}
		}
		if(is_array($last_queue_info) && !empty($last_queue_info))
		{
// 			$result=import_model::import_cdn_live_media($dc,$last_queue_info,'cdn');
// 			if($result['ret'] !=0)
// 			{
// 				echo '<script>alert("'.$result['reason'].'");history.go(-1);</script>';die;
// 			}
		}
		echo '<script>alert("成功");history.go(-1);</script>';die;
	}
	else if($_GET['op']=='cdn_playbill')
	{
		define('ORG_ID', 'unicom_zte');
		$live_id = $_GET['video_id'];
	
		$arr_live = nl_live::query_by_id($dc, $live_id);
		if($arr_live['ret'] !=0)
		{
			echo '<script>alert("'.$arr_live['reason'].'");history.go(-1);</script>';die;
		}
		if(!isset($arr_live['data_info']) || empty($arr_live['data_info']) || !is_array($arr_live['data_info']))
		{
			echo '<script>alert("查无直播数据:'.$live_id.'");history.go(-1);</script>';die;
		}
		$arr_live = $arr_live['data_info'];
		$arr_index = nl_live_index::query_by_live_id($dc, $arr_live['nns_id']);
		if($arr_index['ret'] !=0)
		{
			echo '<script>alert("'.$arr_index['reason'].'");history.go(-1);</script>';die;
		}
		if(!isset($arr_index['data_info']) || empty($arr_index['data_info']) || !is_array($arr_index['data_info']))
		{
			echo '<script>alert("查无直播分集数据:'.$arr_live['nns_id'].'");history.go(-1);</script>';die;
		}
		$arr_index = $arr_index['data_info'];
		$last_queue_info=array();
		foreach ($arr_index as $index_val)
		{
			$arr_media = nl_live_media::query_by_live_index_id($dc, $arr_live['nns_id'],$index_val['nns_id']);
			if($arr_media['ret'] !=0)
			{
				echo '<script>alert("'.$arr_media['reason'].'");history.go(-1);</script>';die;
			}
			if(!isset($arr_media['data_info']) || empty($arr_media['data_info']) || !is_array($arr_media['data_info']))
			{
				continue;
			}
			$index_val_index = $index_val['nns_index']+1;
			$arr_media = $arr_media['data_info'];
			foreach ($arr_media as $media_val)
			{
				$arr_playbill = nl_playbill::query_playbill_is_exsist($dc, $media_val['nns_live_id'],$media_val['nns_id'],$media_val['nns_cp_id']);
				if($arr_playbill['ret'] !=0)
				{
					echo '<script>alert("'.$arr_playbill['reason'].'");history.go(-1);</script>';die;
				}
				if(!isset($arr_playbill['data_info']) || empty($arr_playbill['data_info']) || !is_array($arr_playbill['data_info']))
				{
					continue;
				}
				$arr_playbill = $arr_playbill['data_info'];
				foreach ($arr_playbill as $playbill_val)
				{
					$arr_queue=nl_cdn_playbill_queue::query_queue_is_exsist($dc,$media_val['nns_live_id'],$media_val['nns_id'],$playbill_val['nns_id'],$playbill_val['nns_cp_id']);
					if($arr_queue['ret'] !=0)
					{
						echo '<script>alert("'.$arr_queue['reason'].'");history.go(-1);</script>';die;
					}
					if(isset($arr_queue['data_info']) && is_array($arr_queue['data_info']) && !empty($arr_queue['data_info']))
					{
						$array_edit_queue = array();
						$arr_queue = $arr_queue['data_info'];
						if(($arr_queue['nns_action'] == 0 || $arr_queue['nns_action'] == 1) && $playbill_val['nns_state'] == 0)
						{
							$array_edit_queue['nns_import_cdn_state'] = 0;
							$array_edit_queue['nns_import_cms_state'] = 0;
							$array_edit_queue['nns_import_notify_state'] = 0;
							$array_edit_queue['nns_action'] = 1;
							$last_queue_info[]=$arr_queue['nns_id'];
						}
						else if($arr_queue['nns_action'] == 2 && $playbill_val['nns_state'] != 0)
						{
							$array_edit_queue['nns_import_cdn_state'] = 0;
							$array_edit_queue['nns_import_cms_state'] = 0;
							$array_edit_queue['nns_import_notify_state'] = 0;
							$array_edit_queue['nns_action'] = 2;
							$last_queue_info[]=$arr_queue['nns_id'];
						}
						else
						{
							continue;
						}
						$result_edit_queue=nl_cdn_playbill_queue::edit($dc, $array_edit_queue,$arr_queue['nns_id']);
					}
					else
					{
						if($media_val['nns_state'] !=0)
						{
							continue;
						}
						$queue_guid = np_guid_rand();
						$array_add_queue = array(
								'nns_id'=>$queue_guid,
								'nns_name'=>"[{$live_info['nns_name']}] [{$playbill_val['nns_name']}]",
								'nns_live_id'=>$media_val['nns_live_id'],
								'nns_live_index_id'=>$media_val['nns_live_index_id'],
								'nns_live_media_id'=>$media_val['nns_id'],
								'nns_playbill_id'=>$playbill_val['nns_id'],
								'nns_action'=>0,
								'nns_import_cdn_state'=>0,
								'nns_import_cdn_time'=>0,
								'nns_import_cms_state'=>0,
								'nns_import_cms_time'=>0,
								'nns_import_notify_state'=>0,
								'nns_import_notify_time'=>0,
								'nns_message_id'=>$playbill_val['nns_message_id'],
								'nns_cp_id'=>$playbill_val['nns_cp_id'],
						);
						$result_add_queue=nl_cdn_playbill_queue::add($dc, $array_add_queue);
						if($result_add_queue['ret'] !=0)
						{
							echo '<script>alert("'.$result_add_queue['reason'].'");history.go(-1);</script>';die;
						}
						$last_queue_info[]=$queue_guid;
					}
				}
			}
		}
		if(is_array($last_queue_info) && !empty($last_queue_info))
		{
// 			$result=import_model::import_cdn_live_media($dc,$last_queue_info,'cdn');
// 			if($result['ret'] !=0)
// 			{
// 				echo '<script>alert("'.$result['reason'].'");history.go(-1);</script>';die;
// 			}
		}
		echo '<script>alert("成功");history.go(-1);</script>';die;
	}
	die;
}
/**
 * 注入到epg    结束================================================
 * 
 */

if (count($_POST) == 3) {
        include $nncms_config_path . "nn_logic/mgtv/func_mgtv_boss.php";
        $list = notice_boss_live_add($_POST['vid'], $_POST['vname'], $_POST['type']);
        if ($list)
                exit("1");
        exit("0");
}

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107103");
$checkpri = null;
if (!$pri_bool)
        Header("Location: ../nncms_content_wrong.php");



$view_delete = $_GET["view_delete"];
$nns_live_search = $_GET["nns_live_search"];
$view_audit = $_GET["view_audit"];
$view_type_delete = $_GET["view_delete_type"];
$depot_id = $_GET["depot_id"];
$depot_detail_id = $_GET["depot_detail_id"];
$view_list_max_num = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : (!empty($_COOKIE["page_max_num"]) ?  $_COOKIE["page_max_num"] : 18);

//高级查询
$search_py = $_GET["search_py"];
$search_yy = $_GET["search_yy"];
$search_dy = $_GET["search_dy"];
$search_point = $_GET["search_point"];
$search_sy_begin_date = $_GET["search_sy_begin_date"];
$search_sy_end_date = $_GET["search_sy_end_date"];
$search_rk_begin_date = $_GET["search_rk_begin_date"];
$search_rk_end_date = $_GET["search_rk_end_date"];
$search_copyright_begin_date = $_GET["search_copyright_begin_date"];
$search_copyright_end_date = $_GET["search_copyright_end_date"];
$search_type = $_GET["search_type"];
$search_episode = $_GET["search_episode"];
$search_dq = $_GET["search_dq"];
$search_len_min = $_GET["search_len_min"];
$search_len_max = $_GET["search_len_max"];
$search_summary = $_GET["search_summary"];


$searchinfo = array();
$searchinfo["director"] = $search_dy;
$searchinfo["actor"] = $search_yy;
$searchinfo["area"] = $search_dq;
$search_py = strtolower($search_py);

$search_url = "&search_py=$search_py&nns_live_search=$nns_live_search&search_yy=$search_yy&search_dy=$search_dy&search_point=$search_point&search_sy_begin_date=$search_sy_begin_date&search_sy_end_date=$search_sy_end_date&search_rk_begin_date=$search_rk_begin_date" .
        "&search_rk_end_date=$search_rk_end_date&search_copyright_begin_date=$search_copyright_begin_date&search_copyright_end_date=$search_copyright_end_date" .
        "&search_type=$search_type&search_episode=$search_episode&search_dq=$search_dq&search_len_min=$search_len_min&search_len_max=$search_len_max&search_summary=$search_summary";

$nns_url = "&depot_id=$depot_id&depot_detail_id=$depot_detail_id&view_delete=" . $view_delete . "&view_audit=" . $view_audit . "&view_delete_type=" . $view_type_delete . "&view_list_max_num=$view_list_max_num";

// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
include($nncms_db_path . "nns_live/nns_db_live_class.php");
include $nncms_db_path . "nns_common/nns_db_constant.php";
include $nncms_db_path . "nns_carrier/nns_db_carrier_class.php";
include $nncms_db_path . "nns_partner/nns_db_partner_class.php";
include $nncms_db_path . "nns_group/nns_db_group_class.php";
//include $nncms_db_path . "nns_assist/nns_db_assist_item_class.php";
//include $nncms_db_path . "nns_service/nns_db_service_category_class.php";
//  include $nncms_db_path. "nns_service/nns_db_service_detail_video_class.php";

//$assist_inst = new nns_db_assist_item_class();
//$service_inst = new nns_db_service_category_class();
//  $service_item_inst=new nns_db_service_detail_video_class();
$g_manager_list_max_num = $view_list_max_num;

if (!empty($depot_id)) {
        $live_inst = new nns_db_live_class();
        $countArr = $live_inst->nns_db_live_count($nns_live_search, "", $view_audit, $search_type, $depot_id, $depot_detail_id, null, null, null, $searchinfo, $view_delete, null, null, null, $search_sy_begin_date, $search_sy_end_date, $search_rk_begin_date, $search_rk_end_date, $search_copyright_begin_date, $search_copyright_end_date, $search_point, null, $search_summary, $search_episode, $search_len_min, $search_len_max, $search_py);
        if ($countArr["ret"] == 0)
                $live_total_num = $countArr["data"][0]["num"];

        //ajax 取总数（所有资源，待审核资源，回收站）
        if (isset($_GET['ajax']) && $_GET['ajax'] == 'count_live') {
                //wuha: 将下面实参$view_audit(第三个参数)改为了传入空字符串，修复点击待审核资源后 所有资源里数量跟审核相同 - 2013-4-11
                $countArr = $live_inst->nns_db_live_count($nns_live_search, "", '', $search_type, $depot_id, $depot_detail_id, null, null, null, $searchinfo, null, null, null, null, $search_sy_begin_date, $search_sy_end_date, $search_rk_begin_date, $search_rk_end_date, $search_copyright_begin_date, $search_copyright_end_date, $search_point, null, $search_summary, $search_episode, $search_len_min, $search_len_max, $search_py);
                $count_all = 0;
                if ($countArr["ret"] == 0) {
                        $count_all = $countArr["data"][0]["num"];
                }
                //待审核资源 view_audit=0
                $countArr = $live_inst->nns_db_live_count($nns_live_search, "", 0, $search_type, $depot_id, $depot_detail_id, null, null, null, $searchinfo, null, null, null, null, $search_sy_begin_date, $search_sy_end_date, $search_rk_begin_date, $search_rk_end_date, $search_copyright_begin_date, $search_copyright_end_date, $search_point, null, $search_summary, $search_episode, $search_len_min, $search_len_max, $search_py);
                $count_check = 0;
                if ($countArr["ret"] == 0) {
                        $count_check = $countArr["data"][0]["num"];
                }
                //回收站 view_delete=1
                $countArr = $live_inst->nns_db_live_count($nns_live_search, "", null, $search_type, $depot_id, $depot_detail_id, null, null, null, $searchinfo, 1, null, null, null, $search_sy_begin_date, $search_sy_end_date, $search_rk_begin_date, $search_rk_end_date, $search_copyright_begin_date, $search_copyright_end_date, $search_point, null, $search_summary, $search_episode, $search_len_min, $search_len_max, $search_py);
                $count_deleted = 0;
                if ($countArr["ret"] == 0) {
                        $count_deleted = $countArr["data"][0]["num"];
                }
                echo json_encode(array('count_all' => $count_all, 'count_check' => $count_check, 'count_deleted' => $count_deleted));
                exit;
        }
        $live_pages = ceil($live_total_num / $g_manager_list_max_num);
        $currentpage = 1;
        if (!empty($_GET["page"]))
                $currentpage = $_GET["page"];
        $live_array = $live_inst->nns_db_live_list("*", $nns_live_search, "", $view_audit, $view_type, $depot_id, $depot_detail_id, "", "", "", $searchinfo, "", ($currentpage - 1) * $g_manager_list_max_num, $g_manager_list_max_num, $view_delete, null, null, null, $search_sy_begin_date, $search_sy_end_date, $search_rk_begin_date, $search_rk_end_date, $search_copyright_begin_date, $search_copyright_end_date, $search_point, null, $search_summary, $search_episode, $search_len_min, $search_len_max, $search_py);

// var_dump($manager_array);
        if ($live_array["ret"] != 0) {
                $data = null;
                echo "<script>alert(" . $live_array["reason"] . ");</script>";
        }
        $data = $live_array["data"];

        
}

$carrier_inst = new nns_db_carrier_class();
$partner_inst = new nns_db_partner_class();
$group_inst = new nns_db_group_class();

/**
 * 扩展语言 BY S67
 */
$extend_langs_str = g_cms_config::get_g_extend_language();
$extend_langs_str = trim($extend_langs_str);

/**
 * 剧照 by cb
 */
$stills_enabled = g_cms_config::get_g_config_value('g_stills_enabled');

ob_end_clean();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../../js/cms_cookie.js"></script>
                <script language="javascript" src="../../js/table.js.php"></script>
                <script language="javascript" src="../../js/checkinput.js.php"></script>
                <script language="javascript" src="../../js/rate.js"></script>
                <script language="javascript" src="../../js/image_loaded_func.js"></script>
                <script language="javascript">
                        function checkhiddenInput() {
                                var par = getAllCheckBoxSelect("online");
                                $("#action").attr("value", "delete");
                                if (par == "") {

                                        alert("<?php echo cms_get_lang('msg_select_lm'); ?>");

                                } else {
                                        $('#nns_id').attr('value', getAllCheckBoxSelect("online"));
                                        checkForm('<?php echo cms_get_lang('media_zbgl'); ?>', '<?php echo cms_get_lang('msg_ask_delete'); ?>', $('#delete_form'));
                                }

                        }

                        function checkhiddenBox(type) {
                                BoxKey = false;
                                $("input.checkhiddenInput:checked").each(function() {
                                        if ($(this).attr('rel') == type) {

                                                BoxKey = true;
                                                return false;
                                        }


                                })
                                return BoxKey;
                        }

                        function checkhiddenInput_audit(bool) {

                                if (bool) {
                                        var checkType = 1;
                                } else {
                                        var checkType = 0;
                                }
                                var ifBoxKey = checkhiddenBox(checkType);//验证是否已经通过审核
                                if (ifBoxKey && checkType) {
                                        alert('<?php echo cms_get_lang('js_msg_vodlist_audit'); ?>');
                                        return false;
                                } else if (ifBoxKey && !checkType) {

                                        alert('<?php echo cms_get_lang('js_msg_vodlist_audit_cancel'); ?>');
                                        return false;
                                } else {

                                }

                                var par = getAllCheckBoxSelect();
                                $("#action").attr("value", "audit");
                                if (bool) {
                                        $("#audit").attr("value",<?php echo NNS_LIVE_CHECK_OK; ?>);
                                } else {
                                        $("#audit").attr("value",<?php echo NNS_LIVE_CHECK_WAITING; ?>);
                                }
                                if (par == "") {
                                        alert("<?php echo cms_get_lang('media_select_jm'); ?>");

                                } else {
                                        $('#nns_id').attr('value', getAllCheckBoxSelect());
                                        checkForm('<?php echo cms_get_lang('media_zbgl'); ?>', '<?php echo cms_get_lang('msg_ask_audit'); ?>', $('#delete_form'));
                                }

                        }

                        function reset_live_delete() {
                                var par = getAllCheckBoxSelect("online");
                                $("#action").attr("value", "reset");
                                if (par == "") {

                                        alert("<?php echo cms_get_lang('media_select_jm'); ?>");

                                } else {
                                        $('#nns_id').attr('value', getAllCheckBoxSelect("online"));
                                        checkForm('<?php echo cms_get_lang('media_zbgl'); ?>', '<?php echo cms_get_lang('msg_ask_reset'); ?>', $('#delete_form'));
                                }
                        }
                        function live_real_delete() {
                                var par = getAllCheckBoxSelect("online");
                                $("#action").attr("value", "realdelete");
                                if (par == "") {

                                        alert("<?php echo cms_get_lang('media_select_jm'); ?>");

                                } else {
                                        $('#nns_id').attr('value', getAllCheckBoxSelect("online"));
                                        checkForm('<?php echo cms_get_lang('media_zbgl'); ?>', '<?php echo cms_get_lang('msg_ask_delete'); ?>', $('#delete_form'));
                                }
                        }
                        function set_move_category_id(value, depot_id) {
                                var par = getAllCheckBoxSelect();
                                $("#delete_form").find("#move_category_id").val(value);
                                $("#delete_form").find("#move_depot_id").val(depot_id);
                                $("#delete_form").find("#action").val("move_category");
                                if (par == "") {
                                        alert("<?php echo cms_get_lang('media_select_jm'); ?>");
                                } else {
                                        $('#nns_id').attr('value', getAllCheckBoxSelect());
                                        checkForm('<?php echo cms_get_lang('media_zbgl'); ?>', '<?php echo cms_get_lang('msg_ask_change'); ?>', $('#delete_form'));
                                }
                        }
                        function set_vod_unline(value) {
                                if (confirm("<?php echo cms_get_lang('msg_force_xx'); ?>")) {
                                        $("#delete_form").find("#nns_id").val(value);
                                        $("#delete_form").find("#action").val("unline");
                                        checkForm('<?php echo cms_get_lang('media_zbgl'); ?>', '<?php echo cms_get_lang('msg_ask_change'); ?>', $('#delete_form'));
                                }
                        }
                        function begin_select_category() {
                                var par = getAllCheckBoxSelect();
                                if (par == "") {
                                        alert("<?php echo cms_get_lang('msg_select_one_data'); ?>");
                                } else {
                                        window.parent.begin_select_category();
                                }
                        }

                        $(document).ready(function() {
                                                $('.boosup').click(function() {
                                var vtype = $(this).attr('vtype');
                                var vname = $(this).attr('vname');
                                var vid = $(this).attr('vid');
                                $.post('', {vtype: vtype, vname: vname, vid: vid}, function(type) {
                                        if (type == '1') {
                                                alert("同步成功");
                                        } else {
                                                alert("同步失败");
                                        }
                                });
                        })
                                window.parent.now_frame_url = window.location.href;
                        });

                        function get_tags_edit(tags) {
                                $("#action").attr("value", "ctag");
                                $("#nns_tags").attr("value", tags);
                                var par = getAllCheckBoxSelect();
                                if (par == "") {
<?php if ($view_type == 0) { ?>
                                                alert("<?php echo cms_get_lang('media_select_dy'); ?>");
<?php } else if ($view_type == 1) { ?>
                                                alert("<?php echo cms_get_lang('media_select_lxj'); ?>");
<?php } ?>
                                } else {
                                        $('#nns_id').attr('value', par);
                                        checkForm('<?php echo cms_get_lang('dbgl'); ?>', '<?php echo cms_get_lang('msg_ask_ctag'); ?>', $('#delete_form'));
                                }
                        }

                </script>
        </head>

        <body>
                <div class="content">
                                    <!--<div class="content_position"><?php echo cms_get_lang('zbgl'); ?></div>-->

                        <div class="content_table formtable">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <thead>
                                                <tr>
                                                        <th><input name="" type="checkbox" value="" /></th>
                                                        <th><?php echo cms_get_lang('segnumber'); ?></th>
                                                        <th><?php echo cms_get_lang('name'); ?></th>
                                                        <th><?php echo cms_get_lang('media_pdh')?></th>
                                                        <th><?php echo cms_get_lang('group'); ?></th>
                                                        <th><?php echo cms_get_lang('jmdgl_jmdcl'); ?></th>
                                                        <th>CP ID</th>
                                                        <!--<th>分镜数</th>-->
														<th>播放类型</th>
                                                        <th><?php echo cms_get_lang('audit'); ?></th>
                                                        <th><?php echo cms_get_lang('state'); ?></th>
                                                        <!--<th><?php echo cms_get_lang('create_time'); ?></th>-->
                                                        <th><?php echo cms_get_lang('action'); ?></th>

                                                </tr>
                                        </thead>
                                        <tbody>
                                                <?php
                                                if ($data != null) {
                                                        include $nncms_db_path . "nns_live/nns_db_live_ex_class.php";
                                                        $num = ($currentpage - 1) * $g_manager_list_max_num;
                                                        $db = nn_get_db(NL_DB_READ);
                                                        foreach ($data as $item) {
																$sql_media = "select * from nns_live_media where nns_live_id='{$item['nns_id']}' and nns_deleted ='0'";
																$arr_media = nl_query_by_db($sql_media, $db);
																$arr_media = (is_array($arr_media) && !empty($arr_media) && isset($arr_media[0])) ? $arr_media : null;
                                                                $num++;
//          计算片源是否都已经绑定
                                                                $is_bind = $live_inst->check_bind_state($item["nns_id"], $item["nns_all_index"]);
                                                               // $count1 = $assist_inst->nns_db_assist_item_count(null, null, $item["nns_id"], 1);
                                                               // $count2 = $service_inst->nns_db_service_category_count(null, null, null, $item["nns_id"]);
                                                                /**
                                                                 * 获取视频次要参数 BY S67 2012-8-22
                                                                 */
                                                                $play_bill_type = '';
                                                                $live_ex_inst = new nns_db_live_ex_class();
                                                                $play_bill_type = $live_ex_inst->nns_db_live_ex_info($item["nns_id"], 'play_bill_type');
                                                                $live_type = $live_ex_inst->nns_db_live_ex_info($item["nns_id"], 'live_type');
                                                                $live_type = empty($live_type) ? "LIVE,TSTV" : $live_type;
//            $count3=$service_item_inst->nns_db_service_detail_video_count(null,$item["nns_id"],1);
                                                                /*if ($count1["data"][0]["num"] > 0 || $count2["data"][0]["num"] > 0) {
                                                                        $is_online = true;
                                                                } else {
                                                                        $is_online = false;
                                                                }*/
                                                                ?>
                                                                <tr>
                                                                        <td><input name="input" type="checkbox" class="checkhiddenInput" rel="<?php echo $item["nns_check"]; ?>" value="<?php echo $item["nns_id"]; ?>" <?php
                                                                                if ($is_online == true) {
                                                                                        echo "online='true'";
                                                                                }
                                                                                ?>/></td>
                                                                        <td><?php echo $num; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $item["nns_id"]; ?></td>
                                                                        <td><alt alt="<?php echo pub_func_get_video_alt($item); ?>"><a href="../live_pages/nncms_content_live_detail.php?nns_id=<?php echo $item["nns_id"]; ?>" target="_self" class="link">
                                                                                                <?php echo $item["nns_name"]; ?></a></alt></td>
                                                                        <td> <?php  echo $live_ex_inst->nns_db_live_ex_info($item["nns_id"], 'live_pindao_order'); ?></td>
                                                                        <td><?php
                                                                                switch ($item["nns_org_type"]) {
                                                                                        case 0:
                                                                                                $info_result = $carrier_inst->nns_db_carrier_info($item["nns_org_id"]);
                                                                                                break;
                                                                                        case 1:
                                                                                                $info_result = $partner_inst->nns_db_partner_info($item["nns_org_id"]);
                                                                                                break;
                                                                                        case 2:
                                                                                                $info_result = $group_inst->nns_db_group_info($item["nns_org_id"]);
                                                                                                break;
                                                                                }
                                                                                if ($info_result["ret"] == 0) {
                                                                                        echo $info_result["data"][0]["nns_name"];
                                                                                }
                                                                                ?></td>
                                                                                
                                                                        <td>
                                                                                <?php echo get_video_ex_name($play_bill_type); ?>
                                                                        </td>
                                                                        <td>
                                                                                <?php echo $item['nns_cp_id']; ?>
                                                                        </td>
																		<td>
                                                                                <?php 
																					if(count($arr_media) == 1 && isset($arr_media[0]["nns_cast_type"]) && $arr_media[0]["nns_cast_type"] == '1')
																					{
																						echo "组播";
																					}
                                                                                	else if(count($arr_media) == 1 && isset($arr_media[0]["nns_cast_type"]) && $arr_media[0]["nns_cast_type"] == '2')
																					{
																						echo "单播";
																					}
																					else
																					{
																						echo "";
																					}
                                                                                ?>
                                                                        </td>
                                                                        <td><?php
                                                                                switch ($item["nns_check"]) {
                                                                                        case NNS_VOD_CHECK_OK:
                                                                                                echo "<font style=\"color:#00CC00\">", cms_get_lang('audited'), "</font>";
                                                                                                break;
                                                                                        case NNS_VOD_CHECK_WAITING:
                                                                                                echo "<font style=\"color:#999999\">", cms_get_lang('media_dsh'), "</font>";
                                                                                                break;
//                	case NNS_live_STATE_OFFLINE:
//                	echo "<font style=\"color:#ff0000\">". $language_live_media_addedit_yxx. "</font>";
//                	break;
                                                                                }
                                                                                ?> </td>
                                                                        <td><?php
                                                                                if ($is_online) {
                                                                                        echo "<font style=\"color:#00CC00\">" . cms_get_lang('media_ysx') . "</font>";
                                                                                } else {
                                                                                        echo "<font style=\"color:#999999\">" . cms_get_lang('media_wxx') . "</font>";
                                                                                }
                                                                                ?> </td>
                                                                        
                                                                                                                                                                                                                                                                                                                                                                                                                                                <!--<td><?php echo $item["nns_create_time"]; ?> </td>-->
                                                                        <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                                                                                 <a class="boosup" href="###" vtype="<?php echo $live_type; ?>" vid="<?php echo $item["nns_id"]; ?>" vname="<?php echo $item["nns_name"]; ?>" >Boss同步</a>
                                                                                <a href="nncms_content_live_img_addedit.php?nns_id=<?php echo $item["nns_id"]; ?>" target="_self" ><?php echo cms_get_lang('media_bjhb'); ?></a>
                                                                                <a href="../live_media_pages/nncms_content_live_media_list.php?nns_id=<?php echo $item["nns_id"]; ?>&view_type=<?php echo $item["nns_view_type"]; ?>" target="_self"
                                                                                   <?php if (!$is_bind) { ?> class="nobind"<?php } ?>><?php echo cms_get_lang('media_bind_media'); ?></a>

                                                                                <?php if (!empty($extend_langs_str)) { ?>
                                                                                        <a href="nncms_content_vod_info_language_extend.php?nns_id=<?php echo $item["nns_id"]; ?>" target="_self" ><?php echo cms_get_lang('dyz'); ?></a>
                                                                                <?php } ?>

                                                                                <?php if ($is_online) { ?>

                                                                                        <a href="javascript:set_vod_unline('<?php echo $item["nns_id"]; ?>');" target="_self" ><?php echo cms_get_lang('media_xiax'); ?></a>
                                                                                        <!--<span class="disable"><?php echo cms_get_lang('media_bind_media'); ?></span>-->
                                                                                <?php } ?>
                                                                                <!--&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <a href="../live_pages/nncms_content_live_index_edit.php?nns_id=<?php echo $item["nns_id"]; ?>&view_type=<?php echo $item["nns_view_type"]; ?>" target="_self" ><?php echo cms_get_lang('media_control_bjjj'); ?></a>
                                                                                 </td>-->
                                                                                <?php if ($stills_enabled) { ?>
                                                                                        <a href="../stills_pages/nncms_content_stills.php?video_id=<?php echo $item["nns_id"]; ?>&video_name=<?php echo $item['nns_name'] ?>&video_type=1&url_type=3&depot_detail_id=<?php echo $_GET['depot_detail_id'] ?>&depot_id=<?php echo $_GET['depot_id'] ?>&view_list_max_num=<?php echo $_GET['view_list_max_num'] ?>"><?php echo cms_get_lang('media_jzgl'); ?> </a>
                                                                                <?php } ?>
                                                                                
                                                                                
                                                                                <?php
                                                                                if ($is_bind) {
                                                                                	?>
                                                                                	<a href="./nncms_content_livelist.php?op=cdn&sp_id=<?php echo g_cms_config::get_g_config_value('g_sp_org_id');?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=add">添加直播CND</a>
                                                                                <a href="./nncms_content_livelist.php?op=cdn&sp_id=<?php echo g_cms_config::get_g_config_value('g_sp_org_id');?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=modify">修改直播CND</a>
                                                                                <a href="./nncms_content_livelist.php?op=cdn&sp_id=<?php echo g_cms_config::get_g_config_value('g_sp_org_id');?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=destroy">删除直播CND</a>
                                                                                	<?php
                                                                                }
                                                                                ?>
                                                                           		<?php 
	                                                                           		if(count($arr_media) == 1 && isset($arr_media[0]["nns_cast_type"]) && $arr_media[0]["nns_cast_type"] == '1')
	                                                                           		{
	                                                                           			?>
	                                                                           			<a href="./nncms_content_livelist.php?op=cdn_live&cp_id=<?php echo $item['nns_cp_id'];?>&media_id=<?php echo $arr_media[0]['nns_id'];?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=add">添加组播CND</a>
		                                                                                <a href="./nncms_content_livelist.php?op=cdn_live&cp_id=<?php echo $item['nns_cp_id'];?>&media_id=<?php echo $arr_media[0]['nns_id'];?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=modify">修改组播CND</a>
		                                                                                <a href="./nncms_content_livelist.php?op=cdn_live&cp_id=<?php echo $item['nns_cp_id'];?>&media_id=<?php echo $arr_media[0]['nns_id'];?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=destroy">删除组播CND</a>
	                                                                           			<a href="./nncms_content_livelist.php?op=cdn_playbill&cp_id=<?php echo $item['nns_cp_id'];?>&media_id=<?php echo $arr_media[0]['nns_id'];?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=add">添加节目单CND</a>
		                                                                                <a href="./nncms_content_livelist.php?op=cdn_playbill&cp_id=<?php echo $item['nns_cp_id'];?>&media_id=<?php echo $arr_media[0]['nns_id'];?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=modify">修改节目单CND</a>
		                                                                                <a href="./nncms_content_livelist.php?op=cdn_playbill&cp_id=<?php echo $item['nns_cp_id'];?>&media_id=<?php echo $arr_media[0]['nns_id'];?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=destroy">删除节目单CND</a>
	                                                                           			<?php 
	                                                                           		}
	                                                                           		else if(count($arr_media) == 1 && isset($arr_media[0]["nns_cast_type"]) && $arr_media[0]["nns_cast_type"] == '2')
	                                                                           		{
	                                                                           			?>
	                                                                           			<a href="./nncms_content_livelist.php?op=cdn_live&cp_id=<?php echo $item['nns_cp_id'];?>&media_id=<?php echo $arr_media[0]['nns_id'];?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=add">添加单播CND</a>
		                                                                                <a href="./nncms_content_livelist.php?op=cdn_live&cp_id=<?php echo $item['nns_cp_id'];?>&media_id=<?php echo $arr_media[0]['nns_id'];?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=modify">修改单播CND</a>
		                                                                                <a href="./nncms_content_livelist.php?op=cdn_live&cp_id=<?php echo $item['nns_cp_id'];?>&media_id=<?php echo $arr_media[0]['nns_id'];?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=destroy">删除单播CND</a>
	                                                                           			<a href="./nncms_content_livelist.php?op=cdn_playbill&cp_id=<?php echo $item['nns_cp_id'];?>&media_id=<?php echo $arr_media[0]['nns_id'];?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=add">添加节目单CND</a>
		                                                                                <a href="./nncms_content_livelist.php?op=cdn_playbill&cp_id=<?php echo $item['nns_cp_id'];?>&media_id=<?php echo $arr_media[0]['nns_id'];?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=modify">修改节目单CND</a>
		                                                                                <a href="./nncms_content_livelist.php?op=cdn_playbill&cp_id=<?php echo $item['nns_cp_id'];?>&media_id=<?php echo $arr_media[0]['nns_id'];?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=destroy">删除节目单CND</a>
	                                                                           			<?php 
	                                                                           		}
                                                                           		?>
                                                                </tr>


                                                                <?php
                                                        }
                                                } $least_num = $g_manager_list_max_num - count($data);
                                                for ($i = 0; $i < $least_num; $i++) {
                                                        ?>
                                                        <tr>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>                                                                                                                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                        </tr>

                                                <?php } ?>
                                        </tbody>
                                </table>
                        </div>
                        <div class="pagecontrol">
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_livelist.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_livelist.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $live_pages) { ?>
                                        <a href="nncms_content_livelist.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_livelist.php?page=<?php echo $live_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_livelist.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $live_pages; ?>);">GO>></a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;
                                <?php echo cms_get_lang('total'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $live_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_prepage_list();"/>

                        </div>
                        <div class="controlbtns">
                                <div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);"><?php echo cms_get_lang('select'); ?></a></div>
                                <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);"><?php echo cms_get_lang('cancel'); ?></a></div>
                                <?php
                                if ($view_delete == "" && $view_audit == "") {
                                        if ($view_type_delete != 1) {
                                                ?>
                                                <div class="controlbtn add"><a href="javascript:void(null);" pos="nncms_content_live_addedit.php?depot_id=<?php echo $depot_id; ?>&depot_detail_id=<?php echo $depot_detail_id; ?>" action="add"><?php echo cms_get_lang('add'); ?></a></div>
                                                <div class="controlbtn edit"><a href="javascript:void(null);" pos="nncms_content_live_addedit.php?depot_id=<?php echo $depot_id; ?>&depot_detail_id=<?php echo $depot_detail_id; ?>" action="edit"><?php echo cms_get_lang('edit'); ?></a></div>
                                                <div class="controlbtn move"><a href="javascript:begin_select_category();" action="edit"><?php echo cms_get_lang('move_catalog'); ?></a></div>
                                        <?php } ?>

                                <?php }if ($view_delete == "" && $view_type_delete != 1) { ?>
                                        <div class="controlbtn delete"><a href="javascript:checkhiddenInput();"  action="delete"><?php echo cms_get_lang('delete'); ?></a></div>
                                        <div class="controlbtn audit"><a href="###" onclick="return checkhiddenInput_audit(true);"  action="audit"><?php echo cms_get_lang('access_audit'); ?></a></div>
                                        <div class="controlbtn cancel_audit"><a href="###" onclick="return checkhiddenInput_audit(false);"  action="audit"><?php echo cms_get_lang('cancel|audit'); ?></a></div>
                                <?php } ?>
                                <?php if ($view_delete != "" && $view_type_delete != 1) { ?>
                                        <div class="controlbtn back"><a href="javascript:reset_live_delete();"  action="reset"><?php echo cms_get_lang('reset'); ?></a></div>
                                        <div class="controlbtn real_delete"><a href="javascript:live_real_delete();"  action="delete"><?php echo cms_get_lang('real_delete'); ?></a></div>
                                <?php } ?>
                                <div class="controlbtn edit"><a href="javascript:window.parent.begin_show_tags();" action="edit"><?php echo cms_get_lang('edit_epg'); ?></a></div>
                                <div style="clear:both;"></div>
                        </div>
                        <form id="delete_form" action="nncms_content_live_control.php" method="post">
                                <input name="action" id="action" type="hidden" value="delete" />
                                <input name="nns_tags" id="nns_tags" type="hidden" value="" />
                                <input name="nns_id" id="nns_id" type="hidden" value="" />
                                <input name="audit" id="audit" type="hidden" value="" />
                                <input name="nns_url" id="nns_url" type="hidden" value="<?php echo $nns_url; ?>"/>
                                <input name="move_category_id" id="move_category_id" type="hidden" value=""/>
                                <input name="move_depot_id" id="move_depot_id" type="hidden" value=""/>
                        </form>
                </div>
        </body>
</html>
<?php
$carrier_inst = null;
$partner_inst = null;
$group_inst = null;
$assist_inst = null;
$service_inst = null;
$live_inst = null;
//   $service_item_inst=null;
?>

