<?php
/*
 * Created on 2012-3-2
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
@header("Content-Type:text/html;charset=utf-8");
include("../../nncms_manager_inc.php");
include ($nncms_config_path."nn_cms_config/nn_cms_global.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
    $language_dir=$_SESSION["language_dir"];
}
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
require_once $nncms_db_path. "nns_log".DIRECTORY_SEPARATOR . "nns_db_op_log_class.php";
$log_inst=new nns_db_op_log_class();
//引入v2新版
include $nncms_config_path . "v2/common.php";
\ns_core\m_load::load("ns_core.m_config");
global $g_bk_version_number;
$category_v2 = get_config_v2('g_category_v2');
$bk_version_number = ($g_bk_version_number == '1' && $category_v2 == 1)? true : false;
unset($g_bk_version_number);

$action=$_POST["action"];
$nns_id=$_POST["nns_id"];


//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
if (!empty($action)){
    switch($action){
        case "edit":
            $action_str=cms_get_lang('edit|lmgl_zblm');
            $pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107101");
            break;

        case "add":
            $action_str=cms_get_lang('add|lmgl_zblm');
            $pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107100");
            break;

        case "delete":
            $action_str=cms_get_lang('delete|lmgl_zblm');
            $pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107102");
            break;
        case "up":
            $action_str=cms_get_lang('order|lmgl_zblm');
            $pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107101");
            break;
        case "down":
            $action_str=cms_get_lang('order|lmgl_zblm');
            $pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107101");
            break;
        default:
            break;
    }
}
$checkpri=null;

if (!$pri_bool){
    Header("Location: ../nncms_content_wrong.php");
}else{
    require_once $nncms_db_path."nns_category_backup".DIRECTORY_SEPARATOR."nns_category_backup_class.php";
    require_once $nncms_db_path. "nns_debug_log".DIRECTORY_SEPARATOR . "nns_debug_log_class.php";

    $nns_debug_log=new nns_debug_log_class();
    $nns_debug_log->setlogpath("treexml_log.txt");

    $nns_category_backup_inst=new nns_category_backup_class();

    $nns_category_detail_name=$_POST["nns_category_detail_name"];
    $nns_category_detail_id=$_POST["nns_category_detail_id"];
    $category_parent=$_POST["category_parent"];
    $baseid=$_POST["baseid"];
    $category_id=$_POST["nns_category_id"];
    $org_id=$_POST["org_id"];
    $org_type=$_POST["org_type"];
    include($nncms_db_path. "nns_depot/nns_db_depot_class.php");
    include($nncms_config_path. "nn_logic/depot/depot_v2.class.php");
    if ($bk_version_number)
    {
        $params = array(
            'nns_org_id' => $org_id,
            'nns_type' => $org_type,
        );
        $depot_array = \nl_depot_v2::get_depot_list(\m_config::get_dc(), $params, null, null, false);
        if ($depot_array['ret'] != NS_CDS_SUCCE)
        {
            echo "<script>alert('". $action_str,cms_get_lang('fault'), "');</script>";
        }
        else
        {
            $data = $depot_array['data_info'];
            if ($category_parent != 0)
            {
                $nns_category_detail_id = $category_parent. $nns_category_detail_id;
            }
            else
            {
                $nns_category_detail_id = $baseid. $nns_category_detail_id;
            }
            switch ($action)
            {
                case 'add':
                    //备份
                    $nns_category_backup_inst->backup_categoty(json_encode($data),"depot",$data[0]["nns_depot_id"],$org_type,$org_id);

                    foreach ($data as $items)
                    {
                        if ($items['nns_category_id'] == $nns_category_detail_id)
                        {
                            echo "<script>alert('".cms_get_lang('msg_10')."');</script>";
                            break;
                        }
                    }
                    $add_params = array(
                        'name'                      => $nns_category_detail_name,
                        'category_id'               => $nns_category_detail_id,
                        'parent_id'                 => $category_parent,
                        'depot_id'                  => $data[0]["nns_depot_id"],
                        'org_id'                    => $org_id,
                        'type'                      => $org_type,
                    );

                    $result = \nl_depot_v2::add(\m_config::get_dc(), $add_params);
                    if ($result["ret"]!=0){
                        echo "<script>alert('". $action_str,cms_get_lang('fault'), "');</script>";
                    }else{
                        $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$nns_category_detail_id,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
                        echo "<script>alert('". $action_str,cms_get_lang('success'), "');</script>";
                    }
                    break;
                case 'edit':
                    //备份
                    $nns_category_backup_inst->backup_categoty(json_encode($data),"depot",$data[0]["nns_depot_id"], $org_type, $org_id);
                    //先查询是否有这个
                    $query_params = array(
                        'category_id'               => $category_id,
                        'parent_id'                 => $category_parent,
                        'org_id'                    => $org_id,
                        'type'                      => $org_type,
                    );
                    $exist_data = \nl_depot_v2::query_by_condition(\m_config::get_dc(), $query_params);
                    if ($exist_data['ret'] != NS_CDS_SUCCE)
                    {
                        echo "<script>alert('新版查询存在此栏目失败');</script>";
                        break;
                    }
                    //进行修改
                    $result = \nl_depot_v2::modify(\m_config::get_dc(), array('nns_name' => $nns_category_detail_name), array('nns_id' => $exist_data['data_info'][0]['nns_id']));

                    if ($result["ret"] != NS_CDS_SUCCE){
                        echo "<script>alert('". $action_str,cms_get_lang('fault'), "');</script>";
                    }else{
                        $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$category_id,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
                        echo "<script>alert('". $action_str,cms_get_lang('success'), "');</script>";
                    }
                    break;
                case 'delete':
                    //备份
                    $nns_category_backup_inst->backup_categoty(json_encode($data),"depot",$data[0]["nns_depot_id"],$org_type,$org_id);
                    //统计当前栏目下是否有媒资
                    require_once $nncms_db_path. "nns_live/nns_db_live_class.php";
                    $live_inst=new nns_db_live_class();
                    $count_arr=$live_inst->nns_db_live_count(null,null,null,null,$data[0]["nns_depot_id"],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,$category_id);
                    if ((int)$count_arr["data"][0]["num"] > 0 || $count_arr['ret'] != 0)
                    {
                        echo "<script>alert('". $action_str. cms_get_lang('fault'). "');</script>";
                        break;
                    }

                    $del_params = array(
                        'nns_category_id' => $category_id,
                        'nns_org_id' => $org_id,
                        'nns_type' => $org_type,
                        'nns_depot_id' => $data[0]["nns_depot_id"],
                    );
                    $result = \nl_depot_v2::delete(\m_config::get_dc(), $del_params);

                    if ($result['ret'] != NS_CDS_SUCCE)
                    {
                        echo "<script>alert('". $action_str,cms_get_lang('fault'), "');</script>";
                    }
                    else
                    {
                        $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$category_id,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
                        echo "<script>alert('". $action_str.cms_get_lang('success'). "');</script>";
                    }

                    break;
                case 'up'://排序向上
                    break;
                case 'down'://排序向下
                    break;
            }
        }
    }
    else
    {
        $depot_inst=new nns_db_depot_class();
        $depot_array=$depot_inst->nns_db_depot_list("",$org_type,$org_id,1);
        // var_dump($partner_array);
        if ($depot_array["ret"]==0){
            $data=$depot_array["data"][0];
        }
        //	 $service_inst=null;

        if ($data["nns_category"]!=""){
            $depot_content=$data["nns_category"];
        }else{
            $depot_content="<depot></depot>";
        }
        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->loadXML($depot_content);
        $category = $dom->getElementsByTagName('category');

        $nns_debug_log->setlog($depot_content,"live_tree");

        switch($action){
            case "edit":
                $nns_category_backup_inst->backup_categoty($depot_content,"depot",$data["nns_id"],$org_type,$org_id);
                if ($data!=null){

                    foreach($category as $item){

                        if ($item->getAttribute("id")===$category_id){
                            $nns_category_detail_name=str_replace("<","",$nns_category_detail_name);
                            $nns_category_detail_name=str_replace("/>","",$nns_category_detail_name);
                            $nns_category_detail_name=str_replace("\"","",$nns_category_detail_name);
                            $nns_category_detail_name=str_replace("'","",$nns_category_detail_name);
                            $item->setAttribute("name",$nns_category_detail_name);
                            break;
                        }
                    }
                    $xml_data=$dom->saveXML();
                    $result=$depot_inst->nns_db_depot_modify("",$xml_data,$org_type,$org_id,1);

                }

                if ($result["ret"]!=0){
                    echo "<script>alert('". $action_str,cms_get_lang('fault'), "');</script>";
                }else{
                    $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$category_id,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
                    echo "<script>alert('". $action_str,cms_get_lang('success'), "');</script>";
                }
                break;


            case "add":
                $nns_category_backup_inst->backup_categoty($depot_content,"depot",$data["nns_id"],$org_type,$org_id);
                $bool=true;

                if ($category_parent!=0){
                    $nns_category_detail_id=$category_parent. $nns_category_detail_id;
                    foreach($category as $item){
                        $item_id=$item->getAttribute("id");
                        $item_name=$item->getAttribute("name");
                        if ($item_id===$nns_category_detail_id){
                            echo "<script>alert('".cms_get_lang('msg_10')."');</script>";
                            $bool=false;
                            break;
                        }
                    }
                    if ($bool){
                        foreach($category as $item){
                            if ($item->getAttribute("id")===$category_parent){
                                $item_new = $dom->createElement("category");
                                $item_new->setAttribute("id",$nns_category_detail_id);
                                $nns_category_detail_name=str_replace("<","",$nns_category_detail_name);
                                $nns_category_detail_name=str_replace("/>","",$nns_category_detail_name);
                                $nns_category_detail_name=str_replace("\"","",$nns_category_detail_name);
                                $nns_category_detail_name=str_replace("'","",$nns_category_detail_name);
                                $item_new->setAttribute("name",$nns_category_detail_name);
                                $item_new->setAttribute("parent",$category_parent);
                                $item->appendChild($item_new);
                                break;
                            }
                        }

                    }else break;
                }else{
                    $nns_category_detail_id=$baseid. $nns_category_detail_id;
                    foreach($category as $item){
                        if ($item->getAttribute("id")===$nns_category_detail_id){
                            echo "<script>alert('".cms_get_lang('msg_10')."');</script>";
                            $bool=false;
                            break;
                        }
                    }
                    if($bool){
                        $item_new = $dom->createElement("category");
                        $item_new->setAttribute("id",$nns_category_detail_id);
                        $item_new->setAttribute("name",$nns_category_detail_name);
                        $item_new->setAttribute("parent",$category_parent);
                        $dom->firstChild->appendChild($item_new);
                    }else break;

                }
                $xml_data=$dom->saveXML();
                $result=$depot_inst->nns_db_depot_modify("",$xml_data,$org_type,$org_id,1);


                if ($result["ret"]!=0){
                    echo "<script>alert('". $action_str.cms_get_lang('fault'). "');</script>";
                }else{
                    $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$nns_category_detail_id,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
                    echo "<script>alert('". $action_str.cms_get_lang('success'). "');</script>";
                }
                break;
            case "delete":
                $nns_category_backup_inst->backup_categoty($depot_content,"depot",$data["nns_id"],$org_type,$org_id);
                if ($data!=null){
                    $bool=true;

                    foreach($category as $item){
                        if ($item->getAttribute("id")===$category_id){
                            require_once $nncms_db_path. "nns_live/nns_db_live_class.php";
                            $live_inst=new nns_db_live_class();
                            $count_arr=$live_inst->nns_db_live_count(null,null,null,null,$data["nns_id"],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,$category_id);
                            if ($count_arr["data"][0]["num"]>0){
                                $bool=false;
                            }else{
                                $item->parentNode->removeChild($item);
                                $xml_data=$dom->saveXML();
                                $result=$depot_inst->nns_db_depot_modify("",$xml_data,$org_type,$org_id,1);
                            }
                        }
                    }


                }

                if (!$bool){
                    if (empty($count_arr)){
                        echo "<script>alert('". $action_str,cms_get_lang('fault'), "');</script>";
                    }else{
                        echo "<script>alert('". $action_str,cms_get_lang('fault'),",".cms_get_lang('msg_12') ."');</script>";
                    }
                }else{
                    $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$category_id,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
                    echo "<script>alert('". $action_str,cms_get_lang('success'), "');</script>";
                }
                break;
            case "up":
                $nns_category_backup_inst->backup_categoty($depot_content,"depot",$data["nns_id"],$org_type,$org_id);
                if ($data!=null){
                    $bool=true;

                    $count_category=$category->length;
                    $true_index=0;
                    for ($index=0;$index<$count_category;$index++){
                        $value=$category->item($index)->getAttribute("id");
                        if ($value===$category_id){
                            $true_index=$index;
                            break;
                        }
                    }
                    if ($true_index>0){
                        $item=$category->item($true_index);
                        $true_index_before=$true_index-1;
                        while ($true_index_before>=0){
                            $item_before=$category->item($true_index_before);
                            if ($item_before->getAttribute("parent")===$category->item($true_index)->getAttribute("parent")){
                                if ($true_index_before>=0){
                                    $item->parentNode->removeChild($item);
                                    $item_before->parentNode->insertBefore($item,$item_before);
                                    $xml_data=$dom->saveXML();
                                    $result=$depot_inst->nns_db_depot_modify(null,$xml_data,$org_type,$org_id,1);
                                    $bool=true;
                                    break;
                                }
                            }else{
                                $bool=false;
                            }
                            $true_index_before--;
                        }
                    }else $bool=false;
                }

                if (!$bool){
                    echo "<script>alert('". $action_str,cms_get_lang('fault'), "');</script>";
                }else{
                    $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],"order",$_SESSION["nns_mgr_name"].$action_str.":".$category_id,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
                    echo "<script>alert('". $action_str,cms_get_lang('success'), "');</script>";
                }
                break;
            case "down":
                $nns_category_backup_inst->backup_categoty($depot_content,"depot",$data["nns_id"],$org_type,$org_id);
                if ($data!=null){
                    $bool=true;

                    $count_category=$category->length;
                    $true_index=0;
                    for ($index=0;$index<$count_category;$index++){
                        $value=$category->item($index)->getAttribute("id");
                        if ($value===$category_id){
                            $true_index=$index;
                            break;
                        }
                    }
                    if ($true_index<$count_category-1){
                        $item=$category->item($true_index);
                        $true_index_after=$true_index+1;
                        while ($true_index_after<$count_category){
                            $item_after=$category->item($true_index_after);
                            if ($item_after->getAttribute("parent")===$category->item($true_index)->getAttribute("parent")){
                                if ($true_index_after<$count_category){
                                    $item_after->parentNode->removeChild($item_after);
                                    $item->parentNode->insertBefore($item_after,$item);
                                    $xml_data=$dom->saveXML();
                                    $result=$depot_inst->nns_db_depot_modify(null,$xml_data,$org_type,$org_id,1);
                                    $bool=true;
                                    break;
                                }
                            }else $bool=false;
                            $true_index_after++;
                        }

                    }else $bool=false;
                }

                if (!$bool){
                    echo "<script>alert('". $action_str,cms_get_lang('fault'), "');</script>";
                }else{
                    $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],"order",$_SESSION["nns_mgr_name"].$action_str.":".$category_id,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
                    echo "<script>alert('". $action_str,cms_get_lang('success'), "');</script>";
                }
                break;
            default:

                break;
        }
    }


    //var_dump($result);
    $depot_inst=null;
    $log_inst=null;
    echo "<script>self.location='nncms_content_live_tree_addedit.php?nns_org_id=$org_id&nns_org_type=$org_type';</script>";

}
?>
