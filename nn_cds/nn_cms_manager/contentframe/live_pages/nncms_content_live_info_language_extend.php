<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
ob_start();
include("../../nncms_manager_inc.php");
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/language_contant.php";
include_once $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php";

include LOGIC_DIR.'video'.DIRECTORY_SEPARATOR.'live_info_language.class.php';
$dc = nl_get_dc(array (
	'db_policy' => NL_DB_WRITE,
	'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$dc->open();
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();

$action=$_GET["action"];
$nns_id=$_GET["nns_id"];
$view_type=$_GET["view_type"];
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107101");
$checkpri=null;


if (!$pri_bool){
	Header("Location: ../nncms_content_wrong.php");exit;
}

include ($nncms_db_path. "nns_common/nns_db_constant.php");


include ($nncms_db_path. "nns_live/nns_db_live_index_class.php");
include ($nncms_db_path. "nns_live/nns_db_live_media_class.php");

// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");


include $nncms_db_path. "nns_live/nns_db_live_class.php";
$live_inst=new nns_db_live_class();
if (!empty($nns_id)){
	$live_info=$live_inst->nns_db_live_info($nns_id,"*");
	$live_inst=null;
}else{
	echo "<script>alert('".$live_info["reason"] ."');self.location='nncms_content_livelist.php';</script>";
}
if ($live_info["ret"]!=0){
	echo "<script>alert('".$live_info["reason"] ."');self.location='nncms_content_livelist.php';</script>";
}

$edit_data=$live_info["data"][0];
//		$live_inst=null;
//	$edit_pri=$edit_data["nns_pri_id"];

/**
 * 获取视频次要参数 BY S67 2012-8-22
 */
include $nncms_db_path. "nns_live/nns_db_live_ex_class.php";
$live_ex_inst=new nns_db_live_ex_class();
$play_bill_type=$live_ex_inst->nns_db_live_ex_info($nns_id,'play_bill_type');

/***
 * 废除 原TAG方式
 */
//for ($tag_num=0;$tag_num<32;$tag_num++){
//	if ($edit_data["nns_tag".$tag_num]==1){
//		$tar_str.=$tag_num.',';
//	};
//}

$tar_str=ltrim($edit_data['nns_tag'],',');


$live_indexes_inst=new nns_db_live_index_class();
$live_index_inst=new nns_db_live_media_class();

include $nncms_db_path. "nns_carrier/nns_db_carrier_class.php";
include $nncms_db_path. "nns_partner/nns_db_partner_class.php";
include $nncms_db_path. "nns_group/nns_db_group_class.php";
$carrier_inst=new nns_db_carrier_class();
$partner_inst=new nns_db_partner_class();
$group_inst=new nns_db_group_class();
if ($_SESSION["nns_manager_type"]==0){

	$carrier_result=$carrier_inst->nns_db_carrier_list();

	if ($carrier_result["ret"]==0){
		$carrier_data=$carrier_result["data"][0];
	}
	if ($action=="edit"){
		//		 $partner_inst=new nns_db_partner_class();
		$partner_result=$partner_inst->nns_db_partner_info($edit_data["nns_org_id"]);

		if ($partner_result["ret"]==0){
			$partner_data=$partner_result["data"][0];
		}

		//		 $group_inst=new nns_db_group_class();
		$group_result=$group_inst->nns_db_group_info($edit_data["nns_org_id"]);

		if ($group_result["ret"]==0){
			$group_data=$group_result["data"][0];
		}
	}
}


//	类型为合作伙伴
if ($_SESSION["nns_manager_type"]==1){

	$partner_result=$partner_inst->nns_db_partner_info($_SESSION["nns_org_id"]);

	if ($partner_result["ret"]==0){
		$partner_data=$partner_result["data"][0];
	}
	//	类型为集团
}else if ($_SESSION["nns_manager_type"]==2){

	$group_result=$group_inst->nns_db_group_info($_SESSION["nns_org_id"]);

	if ($group_result["ret"]==0){
		$group_data=$group_result["data"][0];
	}
}
include $nncms_db_path. "nns_assist/nns_db_assist_class.php";
include $nncms_db_path. "nns_service/nns_db_service_class.php";
//  include $nncms_db_path. "nns_service/nns_db_service_detail_video_class.php";

$assist_inst=new nns_db_assist_class();
$service_inst=new nns_db_service_class();
//  $service_item_inst=new nns_db_service_detail_video_class();
$online1=$assist_inst->check_video_belong($nns_id);
$online2=$service_inst->check_video_belong($nns_id);
//  $online3=$service_item_inst->nns_db_service_detail_video_list(0,0,null,$nns_id,1);
$assist_inst=null;
$service_inst=null;
//  $service_item_inst=null;
if (count($online1["data"])>0 || count($online2["data"])>0 ){
	$is_online=true;
}else{
	$is_online=false;
}

/**
 * 扩展语言 BY S67
 */
$extend_langs_str=g_cms_config::get_g_extend_language();
$extend_langs=explode('|',$extend_langs_str);
ob_end_clean();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/rate.js"></script>
<script language="javascript">
function gotoDelete(nns_id){
	$("#delete_form #nns_id").val(nns_id);
	checkForm('<?php echo cms_get_lang('media_zbgl');?>','<?php echo cms_get_lang('msg_ask_delete'); ?>',$('#delete_form'),'');
}
function showCDN(id,type){
	window.parent.begin_show_cdn(id,type);
}

function show_extend_lang(num){

	if ($(".lang_editor:eq("+num+")").is(":hidden")==false){
		$(".lang_editor").hide();
		$(".lang_btn").html("<?php echo cms_get_lang('spread');?>");
	}else{

		$(".lang_editor").hide();
		$(".lang_btn").html("<?php echo cms_get_lang('spread');?>");
		$(".lang_editor:eq("+num+")").show();
		$(".lang_btn:eq("+num+")").html("<?php echo cms_get_lang('fold');?>");
	}
	window.parent.checkheight();
}

function set_show_minor(num){
	$obj=$("input[name=moreinfo_chk]:eq("+num+")");
	if ($obj.attr("checked")=="checked"){
		$(".extend_"+num).show();
	}else{
		$(".extend_"+num).hide();
	}
	window.parent.checkheight();
}

$(document).ready(function(){
	$(".lang_editor").hide();
	show_extend_lang(0);
	var len=$("input[name=moreinfo_chk]").length;
	for(var i=0;i<len;i++){
		set_show_minor(i);
	}
});
</script>
</head>

<body>
<div class="content">
<form id="add_form" action="nncms_content_live_info_language_control.php" method="post">
	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
	<input name="live_id" id="live_id" type="hidden" value="<?php echo $nns_id;?>" />
	<!--<div class="content_position"><?php echo cms_get_lang('dbgl');?> > <?php if ($view_type==0){ echo cms_get_lang('media_dy|dbgl');} else if ($view_type==1){echo cms_get_lang('media_lxj|dbgl');}?></div>-->
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td width="120"><?php echo cms_get_lang('media_mtid');?>:</td>
				<td ><?php echo $edit_data["nns_id"];?></td>
			   <td width="120"><?php echo cms_get_lang('media_ywsy');?>:</td>
				<td>
					<?php echo $edit_data["nns_eng_name"];?>
				</td>
			</tr>
			 <tr>
				<td width="120"><?php echo cms_get_lang('media_zb|name');?>:</td>
				<td><?php echo $edit_data["nns_name"];?></td>
				 <td width="120"><?php echo cms_get_lang('media_pinyin');?>:</td>
				<td>
					<?php echo $edit_data["nns_pinyin"];?>
				</td>

			 </tr>
			 <tr>
				<td width="120"><?php echo cms_get_lang('lxdq')?>:</td>
				<td><?php echo $edit_data["nns_area"];?></td>

				<td width="120"><?php echo cms_get_lang('group');?>:</td>
				<td><?php if ($edit_data["nns_org_type"]==0){
					echo $carrier_data["nns_name"];
				}else if ($edit_data["nns_org_type"]==1){
					echo $partner_data["nns_name"];
				} if ($edit_data["nns_org_type"]==2){
					echo $group_data["nns_name"];
				}?>
				</td>
			 </tr>
			<tr>

				 <td width="120"><?php echo cms_get_lang('state');?>:</td>
				<td>
<?php if ($is_online){
	echo "<font style=\"color:#00CC00\">". cms_get_lang('media_ysx'). "</font>";
}else{
	echo "<font style=\"color:#999999\">". cms_get_lang('media_wxx'). "</font>";
}?>
				</td>
				<td width="120"><?php echo cms_get_lang('zdgl_zdscbj');?>:</td>
				<td>

						<alt alt="<?php echo get_tag_detail_html($tar_str);?>"><?php echo $tar_str;?></alt>


				</td>
			</tr>

			<?php if (count($online1["data"])>0){?>
			<tr>
				<td width="120"><?php echo cms_get_lang('assist_owner');?>:</td>
				<td colspan="3">
<?php
	foreach ($online1["data"] as $online1_arr_value){
		echo "<span style=\"color:#ff0000\">".$online1_arr_value["nns_name"]."</span><span style=\"color:#999999\">(".$online1_arr_value["nns_assist_id"].")</span>&nbsp;&nbsp;&nbsp;&nbsp;";
	}
?>
				</td>
			</tr>
			<?php }?>
			<?php if (count($online2["data"])>0  && $g_service_enabled==1){?>
			<tr>
				<td width="120"><?php echo cms_get_lang('bkgl_owner');?>:</td>
				<td colspan="3">
<?php
foreach ($online2["data"] as $online2_arr_value){
	echo "<span style=\"color:#ff0000\">".$online2_arr_value["nns_name"]."</span><span style=\"color:#999999\">(".$online2_arr_value["nns_service_id"].")</span>&nbsp;&nbsp;&nbsp;&nbsp;";
}

?>
				</td>
			</tr>
			<?php }?>
			<tr class="select_carrier_class">
				<td width="120"><?php echo cms_get_lang('media_tjfs');?>:</td>
				<td>
					<span class="p_rate" id="p_rate">
					<i title="1<?php echo cms_get_lang('point');?>"></i>
					<i title="2<?php echo cms_get_lang('point');?>"></i>
					<i title="3<?php echo cms_get_lang('point');?>"></i>
					<i title="4<?php echo cms_get_lang('point');?>"></i>
					<i title="5<?php echo cms_get_lang('point');?>"></i>
					</span>
<script>
var Rate = new pRate("p_rate",function(){
	$("#nns_point").val(Rate.Index);
},true);
Rate.setindex(<?php echo $edit_data["nns_point"];?>);

</script>
				</td>
				<td width="120"><?php echo cms_get_lang('jmdgl_jmdcl')?>:</td>
				<td>
					<?php echo get_video_ex_name($play_bill_type);?>
				</td>
			</tr>


			<tr>
				<td width="120"><?php echo cms_get_lang('media_bjhb')."(".$language_media_addedit_big.")";?>:</td>
				<td colspan="3">
				<div class="img_content">
					 <?php if (!empty($edit_data["nns_image0"])){?>
						<div class="img_view"><h4><?php echo cms_get_lang('media_bjhb')."(".cms_get_lang('media_big').")".cms_get_lang('media_slt');?></h4>
						<a href="<?php echo pub_func_get_image_url($edit_data["nns_image0"]);?>?rand=<?php echo rand();?>" target="_blank">
						<img src="<?php echo pub_func_get_image_url($edit_data["nns_image0"]);?>?rand=<?php echo rand();?>" style="border:none;" /></a><h4 class="resolution"></h4></div>
					<?php }?>

					 <?php if (!empty($edit_data["nns_image1"])){?>
						<div class="img_view"><h4><?php echo cms_get_lang('media_bjhb')."(".cms_get_lang('media_normal').")".cms_get_lang('media_slt');?></h4>
						<a href="<?php echo pub_func_get_image_url($edit_data["nns_image1"]);?>?rand=<?php echo rand();?>" target="_blank">
						<img src="<?php echo pub_func_get_image_url($edit_data["nns_image1"]);?>?rand=<?php echo rand();?>" style="border:none;" /></a><h4 class="resolution"></h4></div>
					<?php }?>
					 <?php if (!empty($edit_data["nns_image2"])){?>
						<div class="img_view"><h4><?php echo cms_get_lang('media_bjhb')."(".cms_get_lang('media_small').")".cms_get_lang('media_slt');?></h4>
						<a href="<?php echo pub_func_get_image_url($edit_data["nns_image2"]);?>?rand=<?php echo rand();?>" target="_blank">
						<img src="<?php echo pub_func_get_image_url($edit_data["nns_image2"]);?>?rand=<?php echo rand();?>" style="border:none;" /></a><h4 class="resolution"></h4></div>
					<?php }?>
				</div>
			   </td>

			</tr>


			<tr>
				<td width="120"><?php echo cms_get_lang('media_nrjj');?>:</td>
				<td colspan="3">
					<?php echo $edit_data["nns_summary"];?>
				</td>
			</tr>
		<!--语言扩展-->
<?php
$num=0;
foreach ($extend_langs as $extend_lang){
	$result=nl_live_info_language::get_live_info_by_language($dc,$nns_id,$extend_lang,NL_DC_DB);
	if ($result!==FALSE || $result!==TRUE){
		$data=$result[0];
	} else{
		$data=NULL;
	}
?>
			<tr>
			<td colspan="4"   style="padding:0px;"><div class="radiolist" style=" border-bottom:1px solid #86B7CC;"><div class="radiogroup">
			<h3 style="  background-color:#DCEEF5;">
<?php
	$lang_name=get_language_name_by_code($extend_lang);
	echo $lang_name,cms_get_lang('yyb|kz');
?>
				<div style='padding:2px 5px 2px 20px; float:right; background:url(../../images/add_1.png) 0px 50% no-repeat;'>
				<a href='javascript:show_extend_lang(<?php echo $num;?>);' class="lang_btn"> <?php echo cms_get_lang('spread');?></a></div>
			</h3>

			</div></div></td>
		  </tr>
			<tr class='lang_editor'>
			<td colspan="4"   style="padding:0px;">
				<div class="content_table">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td width="120"><?php echo $lang_name,cms_get_lang('name');?> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
							<td><input name="nns_name[]" type="text" rule="noempty"
									value="<?php echo $data['nns_name'];?>" rules="noempty"
							 />:</td>
						</tr>


						<tr>
							<td width="120"><?php echo $lang_name,cms_get_lang('lxdq');?>:</td>
							<td><input name="nns_area[]" type="text"
									value="<?php echo $data['nns_area'];?>"
							 /> </td>
						</tr>
						<tr>
							<td width="120"><?php echo $lang_name,cms_get_lang('media_index_jj');?>:</td>
							<td>
								<textarea name="nns_summary[]" cols="" rows=""><?php echo $data['nns_summary'];?></textarea>
								<input  name="moreinfo_chk" id="moreinfo_chk" type="checkbox" onclick="set_show_minor(<?php echo $num;?>);" /> <font style="color:#0000ff"><?php echo cms_get_lang('gdxx');?></a>
							</td>
						</tr>
						<tr class="extend_<?php echo $num;?>">
							<td width="120"><?php echo $lang_name,cms_get_lang('media_bm');?>:</td>
							<td><input name="nns_alias_name[]" type="text"
									value="<?php echo $data['nns_alias_name'];?>"
							 /> </td>
						</tr>

						<tr  class="extend_<?php echo $num;?>">
							<td width="120"><?php echo $lang_name,cms_get_lang('media_spyz');?>:</td>
							<td><input name="nns_live_language[]" type="text"
									value="<?php echo $data['nns_live_language'];?>"
							 /> </td>
						</tr>


					</tbody>
				</table>
				</div>
			</td>
		  </tr>
		  <?php $num++; }?>
		  </tbody>
		</table>
	</div>
	</form>
	<div class="controlbtns">
		<div class="controlbtn edit"><a href="javascript:checkForm('<?php echo cms_get_lang('dbgl');?>','<?php echo cms_get_lang('msg_ask_change'); ?>',$('#add_form'),'edit');"><?php echo cms_get_lang('edit');?></a></div>
		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>
</body>
</html>