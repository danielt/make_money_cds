<?php
header("Content-Type:text/html; charset=UTF-8");
include("../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
//include ($nncms_config_path. "nn_cms_config/nn_cms_config_path.php");
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";

$checkpri=new nns_db_pri_class();
if (!$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"100100")){
	Header("Location: nncms_content_wrong.php");
}
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
$checkpri=null;
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";


if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit')
{
	require_once $nncms_db_path. "nns_log".DIRECTORY_SEPARATOR . "nns_db_op_log_class.php";
	$log_inst=new nns_db_op_log_class();
	$configcode="<?php \r\n";
	
	foreach ($_REQUEST as $key=>$val)
	{
	    $key = strtolower($key);
	    if(substr($key, 0,2) != 'g_')
	    {
	        continue;
	    }
	    $configcode.="\$".$key."=\"". $val. "\";\r\n";
	}
	$configcode.="include_once \"nn_cms_function.php\";";
	$fp1 = @fopen( $nncms_config_path. "nn_cms_config/nn_cms_global.php","w");
	@fwrite($fp1,$configcode);
	@fclose($fp1);
	$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],"edit",$_SESSION["nns_mgr_name"].cms_get_lang('edit|xtgl'),$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
	$log_inst=null;
	echo "<script>alert('".cms_get_lang('edit|success')."');self.location='nncms_content_global.php';</script>";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/table.js.php"></script>
<script language="javascript" src="../js/checkinput.js.php"></script>
<script language="javascript">
function checkForm(formobj){
	_formobj=formobj;
	if (checkInputRules()){
		window.parent.cmsalert.showAlert("<?php echo cms_get_lang('xtgl_cmsgl');?>","<?php echo cms_get_lang('msg_ask_change');?>",submitForm);
	}
}
</script>
</head>

<body>
<div class="content">
	<div class="content_position"><?php echo cms_get_lang('xtgl');?> > <?php echo cms_get_lang('xtgl_cmsgl');?></div>
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
		
		    <tr style="display:none;">
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>全局web地址(老项目使用):</td>
				<td><?php echo $g_webdir;?></td>
			</tr>
			<tr style="display:none;">
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>缓存开关(0关闭|1开启):</td>
				<td><?php echo $g_cache_enabled;?></td>
			</tr>
			<tr style="display:none;">
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>播放日志存储天数:</td>
				<td><?php echo $g_play_log_limit;?></td>
			</tr>
			<tr>
				<td colspan="2" style="padding:0px;">
					<div class="radiolist">
						<div class="radiogroup">
							<h3>播控后台配置</h3>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>项目全局中文名称:</td>
				<td><?php echo strlen($g_prject_ch_name) <1 ? "播控子平台-媒资注入平台" : $g_prject_ch_name;?></td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>播控版本切换:</td>
				<td><?php echo $g_bk_version_number == '1' ? "播控V2版本" : "播控老版本";?></td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>融合平台开关:</td>
				<td><?php echo $g_fuse_enabled ? cms_get_lang('kq') : cms_get_lang('not_open');?></td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>介质库平台开关:</td>
				<td><?php echo $g_medium_enabled ? cms_get_lang('kq') : cms_get_lang('not_open');?></td>
			</tr>
			<tr>
				<td class="rightstyle">PHP环境执行地址:<br>linux:[/usr/local/php/bin/php]<br/>windows:[E:\php\php.exe]</td>
				<td><?php echo $g_evn_operation_bin;?></td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>全局项目ID:</td>
				<td><?php echo $g_project_name;?></td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>后台基本地址(数据库配置文件父级目录):</td>
				<td><?php echo $g_bk_web_url;?></td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>后台日志地址(/nn_mgtvbk/data/):</td>
				<td><?php echo $g_bk_log_web_url;?></td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>后台默认数据展示条数:</td>
				<td><?php echo $g_manager_list_max_num;?></td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>memcache缓存开关:</td>
				<td><?php echo $g_mem_cache_enabled ? cms_get_lang('kq') : cms_get_lang('not_open');?></td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>媒资审核开关:</td>
				<td><?php echo $g_video_audit_enabled ? cms_get_lang('kq') : cms_get_lang('not_open');?></td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>登录验证码开关:</td>
				<td><?php echo $g_login_code_enabled ? cms_get_lang('kq') : cms_get_lang('not_open');?></td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>登录验证码开关:</td>
				<td><?php echo $g_login_code_enabled ? cms_get_lang('kq') : cms_get_lang('not_open');?></td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>媒资信息补全开关:</td>
				<td><?php echo $g_assets_info_completion_enabled ? cms_get_lang('kq') : cms_get_lang('not_open');?></td>
			</tr>
			<tr>
				<td class="rightstyle">媒资信息补全接口:</td>
				<td><?php echo $g_assets_info_completion_url;?></td>
			</tr>
			<tr>
				<td class="rightstyle">媒资信息补全结果MEM缓存时间（秒）:</td>
				<td><?php echo $g_assets_info_completion_time;?></td>
			</tr>
			<tr>
				<td colspan="2" style="padding:0px;">
					<div class="radiolist">
						<div class="radiogroup">
							<h3>MSP配置</h3>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="rightstyle">请求地址:</td>
				<td><?php echo $g_core_url;?></td>
			</tr>
            <tr>
				<td class="rightstyle">账号:</td>
				<td><?php echo $g_core_userid;?></td>
			</tr>
			<tr>
				<td class="rightstyle">密码:</td>
				<td><?php echo $g_core_password;?></td>
			</tr>
			<tr>
				<td class="rightstyle">播放地址:</td>
				<td><?php echo $g_core_npss_url;?></td>
			</tr>
			<tr>
				<td colspan="2" style="padding:0px;">
					<div class="radiolist">
						<div class="radiogroup">
							<h3>转码配置</h3>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="rightstyle">转出统一片源后缀(未配置默认ts):</td>
				<td><?php echo $g_encode_file_ex_type;?></td>
			</tr>
			<tr>
				<td colspan="2" style="padding:0px;">
					<div class="radiolist">
						<div class="radiogroup">
							<h3>测试桩配置</h3>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="rightstyle">SOAP模拟ftp相对地址:</td>
				<td><?php echo $g_soap_test_ftp_url;?></td>
			</tr>
			<tr>
				<td colspan="2" style="padding:0px;">
					<div class="radiolist">
						<div class="radiogroup">
							<h3>DEBUG配置</h3>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="rightstyle">SQL语句收集开关:</td>
				<td><?php echo $g_debug_sql_enable ? cms_get_lang('kq') : cms_get_lang('not_open');?></td>
			</tr>
			<tr>
				<td class="rightstyle">SQL语句收集执行大于时间<br>(如果为0全部收集 单位/秒):</td>
				<td><?php echo $g_debug_sql_ex_second;?></td>
			</tr>
			<tr>
				<td colspan="2" style="padding:0px;">
					<div class="radiolist">
						<div class="radiogroup">
							<h3>自定义配置</h3>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="rightstyle">选择DB:</td>
				<td><?php echo $g_db_configer_enable;?></td>
			</tr>
		</tbody>
		</table>
	</div>
	<div class="controlbtns">
		<div class="controlbtn sure"><a href="nncms_content_global_edit.php"><?php echo cms_get_lang('edit');?></a></div>
		<div style="clear:both"></div>
	</div>
</div>
</body>
</html>
