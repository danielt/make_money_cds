<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();

$action=$_GET["action"];
$nns_id=$_GET["nns_id"];
$pri_bool=false;
if (!empty($action)){
	switch($action){
	case "edit":
		$action_str=cms_get_lang('save');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"104101");
		break;
	case "add":
		$action_str=cms_get_lang('add');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"104100");
		break;
	default:
		break;
	}
}
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");

if ($action=="edit"){
	include $nncms_db_path. "nns_group/nns_db_group_class.php";
	$group_inst=new nns_db_group_class();
	if (!empty($nns_id)){
		$group_info=$group_inst->nns_db_group_info($nns_id);
		$group_inst=null;
	}else{
		echo "<script>alert('".$group_info["reason"] ."');self.location='nncms_content_grouplist.php';</script>";
	}
	if ($group_info["ret"]!=0){
		echo "<script>alert('".$partner_info["reason"] ."');self.location='nncms_content_grouplist.php';</script>";
	}
	$edit_data=$group_info["data"][0];
}

function getDirFiles($dir)
{
	if ($handle = opendir($dir)){
		while (false !== ($file = readdir($handle))) {
			$files[]=$file;
		}
	}
	closedir($handle);
	if($files) return $files;
	else return false;
}

$sup_dir_arr=getDirFiles($nncms_config_path. "data/template");
$json_str="{";
foreach($sup_dir_arr as $sup_dir){
	if ($sup_dir!="." && $sup_dir!=".." && is_numeric($sup_dir)){
		$json_str.="'".$sup_dir."':'";
		$sub_dir_arr=getDirFiles($nncms_config_path. "data/template/". $sup_dir);
		foreach($sub_dir_arr as $sub_dir){
			if ($sub_dir!="." && $sub_dir!=".." && is_numeric($sub_dir) ){
				$json_str.=$sub_dir."|";
			}
		}
		$json_str=substr($json_str,0,-1);
		$json_str.="',";
	}
}
$json_str=substr($json_str,0,-1);
$json_str.="}";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/radiolist.js"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript">
var temp_id="<?php echo $edit_data["nns_template_id"];?>";
var temp_type="<?php echo $edit_data["nns_template_type"];?>";
var skin_list=<?php echo $json_str;?>;
$(document).ready(function(){
	var list_html="";
	for (var key in skin_list){
		if (key==temp_type){
			set_id_select(key);
			list_html+="<option value='"+key+"' selected >"+key+"</option>";
		}else{
			list_html+="<option value='"+key+"'  >"+key+"</option>";
		}

	}
	//		$("#enableoutside_no2").attr("checked",true);
	$("#nns_template_type").html(list_html);
	$("#nns_template_type").change(function(){
		var $obj=$(this).find("option:selected");
		var index=$("#nns_template_type option").index($obj);
		selecttemplate(index);
	});
	if (!temp_type){
		selecttemplate(0);
	}
	$("#enableoutside_yes").click(function(){
		enableoutside();
	});
	$("#enableoutside_no").click(function(){
		enableoutside();
	});
	$("#enableoutside_no2").click(function(){
		enableoutside();
	});

	enableoutside();
});

function enableoutside(){
	var bool=$("#enableoutside_yes").attr("checked");
	var bool1=$("#enableoutside_no").attr("checked");
	var bool2=$("#enableoutside_no2").attr("checked");
	if (bool){
		$("#nns_url_link").parent().parent().show();
		$("#nns_template_type").parent().parent().hide();
		$("#nns_template_id").parent().parent().hide();
	}
	if (bool1){
		$("#nns_url_link").parent().parent().hide();
		$("#nns_template_type").parent().parent().show();
		$("#nns_template_id").parent().parent().show();
	}
	if (bool2){
		$("#nns_url_link").parent().parent().hide();
		$("#nns_template_type").parent().parent().hide();
		$("#nns_template_id").parent().parent().hide();
	}
}

function selecttemplate(index){
	set_id_select($("#nns_template_type option:eq("+index+")").attr("value"));
}

function set_id_select(id){
	var list_id_html="";
	var skin_list_str=skin_list[id];
	var skin_arr=skin_list[id].split("|");
	for(var i=0; i< skin_arr.length;i++){
		if (skin_arr[i]==temp_id){
			list_id_html+="<option value='"+skin_arr[i]+"' selected >"+skin_arr[i]+"</option>";
		}else{
			list_id_html+="<option value='"+skin_arr[i]+"' >"+skin_arr[i]+"</option>";
		}
	}
	$("#nns_template_id").html(list_id_html);
}
</script>
</head>

<body>


<div class="content">
	<div class="content_position"><?php echo cms_get_lang('xtgl_jtgl');?> > <?php echo cms_get_lang('add|group_jtxx');?></div>
	<form id="add_form" action="nncms_content_group_control.php" method="post">
	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
	<?php if ($action=="edit"){?>
	<input name="nns_id" id="nns_id" type="hidden" value="<?php echo $nns_id;?>"/>
	<?php } ?>
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			  <tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('group_jtmc');?> </td>
				<td><input name="nns_group_name" id="nns_group_name" type="text" rule="noempty"
					value="<?php if($action=="edit"){echo $edit_data["nns_name"];}?>" <?php if($action=="edit"){echo "style='background:#F3F3F3; border:1px solid #D1D1D1; color:#8E8E8E' readonly";}?>
				/></td>

			</tr>
			 <tr>
				<td class="rightstyle"><?php echo cms_get_lang('rkms')?></td>
				<td>
					<input type="radio" name="enableoutside" value="2" id="enableoutside_no2" <?php if ($edit_data["nns_outside_link"]==2 || $action=="edit"){?>checked="true"<?php } ?>/><?php echo cms_get_lang('yysmr')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" name="enableoutside" value="0" id="enableoutside_no" <?php if ($edit_data["nns_outside_link"]==0){?>checked="true"<?php }?>/><?php echo cms_get_lang('zdmb')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" name="enableoutside" value="1" id="enableoutside_yes" <?php if ($edit_data["nns_outside_link"]==1){?>checked="true"<?php }?>/><?php echo cms_get_lang('jtzjfwqwl')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>

			</tr>
			<tr>
				<td class="rightstyle"><?php echo cms_get_lang('jtzjfwqwl')?>:</td>
				<td><input name="nns_url_link" id="nns_url_link" type="text" value="<?php echo $edit_data["nns_url_link"];?>"/>
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><?php echo cms_get_lang('mblx')?>:</td>
				<td>
				<select name="nns_template_type" id="nns_template_type" rule="noempty">



					</select>
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><?php echo cms_get_lang('mbgl_mbys')?>:</td>
				<td>
				<select name="nns_template_id" id="nns_template_id" rule="noempty">


					</select>
				</td>
			</tr>
			 <tr>
				<td class="rightstyle"><?php echo cms_get_lang('lxr');?>:</td>
				<td><input name="nns_group_contact" id="nns_group_contact" type="text" value="<?php echo $edit_data["nns_contact"];?>"/>
				</td>
		  </tr>
		  <tr>
				<td class="rightstyle"><?php echo cms_get_lang('lxdh');?>:</td>
				<td><input name="nns_group_phone" id="nns_group_phone" type="text" rule='phone' value="<?php echo $edit_data["nns_telephone"];?>"/>
				</td>
		  </tr>
		  <tr>
				<td class="rightstyle"><?php echo cms_get_lang('email');?>:</td>
				<td><input name="nns_group_email" id="nns_group_email" type="text" rule='email' value="<?php echo $edit_data["nns_email"];?>"/>
				</td>
		  </tr>
		  <tr>
				<td class="rightstyle"><?php echo cms_get_lang('lxdz');?>:</td>
				<td><input name="nns_group_addr" id="nns_group_addr" type="text" value="<?php echo $edit_data["nns_addr"];?>"/>
				</td>
		  </tr>
		  <tr>
				<td class="rightstyle"><?php echo cms_get_lang('ms');?>:</td>
				<td><textarea name="nns_group_desc" id="nns_group_desc" cols="" rows=""><?php echo $edit_data["nns_desc"];?></textarea>
				</td>
		  </tr>

		  </tbody>
		</table>
	</div>
	</form>
	<div class="controlbtns">
		<div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','<?php echo cms_get_lang('msg_ask_edit'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>
</body>
</html>