<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

 //加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"104103");
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");

 $nns_group_search=$_GET["nns_group_search"];
  if (!$nns_group_search){
 	$nns_group_search="";
 	$nns_url_query="";
 }else{
 	$nns_url_query="&nns_group_search=". urlencode($nns_group_search);
 }
 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
 if (!empty($_COOKIE["page_max_num"])){
		$g_manager_list_max_num=$_COOKIE["page_max_num"];
	}
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
 include($nncms_db_path. "nns_group/nns_db_group_class.php");
 $group_inst=new nns_db_group_class();
  $currentpage=1;
 if ($_SESSION["nns_manager_type"]!=2){
	 $countArr=$group_inst->nns_db_group_count($nns_group_search);
	 if ($countArr["ret"]==0)
	 $group_total_num=$countArr["data"][0]["num"];
	 $group_pages=ceil($group_total_num/$g_manager_list_max_num);

	 if (!empty($_GET["page"])) $currentpage=$_GET["page"];

	 $group_array=$group_inst->nns_db_group_list($nns_group_search,($currentpage-1)*$g_manager_list_max_num,$g_manager_list_max_num);
	// var_dump($partner_array);
		$group_inst=null;
	 if ($group_array["ret"]!=0){
	 	$data=null;
	 	echo "<script>alert(". $group_array["reason"].");</script>";
	 }
	 $data=$group_array["data"];
 }else{
 	$group_array=$group_inst->nns_db_group_info($_SESSION["nns_org_id"]);
 	if ($group_array["ret"]!=0){
	 	$data=null;
	 	echo "<script>alert('". $group_array["reason"]."');</script>";
	 }
	  $data=$group_array["data"];
 }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript">
function checkhiddenInput(){
	var par=getAllCheckBoxSelect();
	if (par==""){
		alert("<?php echo cms_get_lang('group_msg_noselect');?>");
	}else{
		$('#nns_id').attr('value',getAllCheckBoxSelect());
		checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','<?php echo cms_get_lang('msg_ask_delete');?>',$('#delete_form'));
	}

}
</script>
</head>

<body>
<div class="content">
	<div class="content_position"><?php echo cms_get_lang('xtgl_jtgl');?> > <?php echo cms_get_lang('group_jtlb');?></div>
    <div class="content_table formtable">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
        	<tr>
            	<th><input name="" type="checkbox" value="" /></th>
                <th><?php echo cms_get_lang('segnumber');?></th>
                <th><?php echo cms_get_lang('jtid')?></th>
                <th><?php echo cms_get_lang('name');?></th>
                <th><?php echo cms_get_lang('create_time');?></th>
                <th><?php echo cms_get_lang('edit_time');?></th>
            </tr>
        </thead>
        <tbody>
        <?php
        	if ($data!=null){
        	 $num=($currentpage-1)*$g_manager_list_max_num; foreach($data as $item){
        	$num++;

        	?>
              <tr>
                <td><input name="input" type="checkbox" value="<?php echo  $item["nns_id"];?>" /></td>
                <td><?php echo $num;?></td>
                <td><?php echo $item["nns_id"];?></td>
                <td><a href="nncms_content_group_detail.php?nns_id=<?php echo $item["nns_id"];?>"  class="link"><?php echo $item["nns_name"];?></a></td>
                <td><?php echo $item["nns_create_time"];?> </td>
                <td><?php echo $item["nns_modify_time"];?> </td>
              </tr>
          <?php }} ?>
          	<script language="javascript">
          	create_empty_tr($(".formtable"),<?php echo count($data);?>,<?php echo $nncms_ui_min_list_item_count;?>);
          </script>
          </tbody>
        </table>
    </div>
     <div class="pagecontrol">
     	<?php if ($currentpage>1){?>
     	<a href="nncms_content_grouplist.php?page=1<?php echo $nns_url_query;?>" target="_self"><?php echo cms_get_lang('first_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
    	<a href="nncms_content_grouplist.php?page=<?php echo $currentpage-1;?><?php echo $nns_url_query;?>" target="_self"><?php echo cms_get_lang('pre_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
    	<?php }else{?>
    		<span><?php echo cms_get_lang('first_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
    		<span><?php echo cms_get_lang('pre_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
    	<?php } if ($currentpage<$group_pages){?>
        <a href="nncms_content_grouplist.php?page=<?php echo $currentpage+1;?><?php echo $nns_url_query;?>" target="_self"><?php echo cms_get_lang('next_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="nncms_content_grouplist.php?page=<?php echo $group_pages;?><?php echo $nns_url_query;?>" target="_self"><?php echo cms_get_lang('last_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
       <?php }else{?>
      		<span><?php echo cms_get_lang('next_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
    		<span><?php echo cms_get_lang('last_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
      	<?php }?>

        <?php echo cms_get_lang('jump_to');?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage;?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page');?> &nbsp;&nbsp;
        <a href="javascript:go_page_num('nncms_content_grouplist.php?ran=1<?php echo $nns_url_query;?>',<?php echo $group_pages;?>);">GO>></a>&nbsp;&nbsp;
        <?php echo cms_get_lang('current');?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage."/".$group_pages;?></span><?php echo cms_get_lang('page');?>&nbsp;&nbsp;|&nbsp;&nbsp;
        <?php echo cms_get_lang('perpagenum');?>&nbsp;
         <input name="nns_list_max_num" id="nns_list_max_num" type="text"
         value="<?php echo $g_manager_list_max_num;?>" style="width:24px;"/>&nbsp;&nbsp;
         <input type="button" value="<?php echo cms_get_lang('confirm');?>"
         onclick="refresh_prepage_list();"/>

    </div>
    <div class="controlbtns">
        <div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);"><?php echo cms_get_lang('select');?></a></div>
        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);"><?php echo cms_get_lang('cancel');?></a></div>
        <?php if ($_SESSION["nns_manager_type"]!=2 && $g_boss_mode!=1){?>
        <div class="controlbtn add"><a href="javascript:void(null);" pos="nncms_content_group_addedit.php" action="add"><?php echo cms_get_lang('add');?></a></div>
        <?php }?>
    	<div class="controlbtn edit"><a href="javascript:void(null);" pos="nncms_content_group_addedit.php" action="edit"><?php echo cms_get_lang('edit');?></a></div>
    	<?php if ($_SESSION["nns_manager_type"]!=2 && $g_boss_mode!=1){?>
        <div class="controlbtn delete"><a href="javascript:checkhiddenInput();"  action="delete"><?php echo cms_get_lang('delete');?></a></div>
        <?php }?>
        <div style="clear:both;"></div>
    </div>
     <form id="delete_form" action="nncms_content_group_control.php" method="post">
      <input name="action" id="action" type="hidden" value="delete" />
      <input name="nns_id" id="nns_id" type="hidden" value="" />
     </form>
</div>
</body>
</html>
