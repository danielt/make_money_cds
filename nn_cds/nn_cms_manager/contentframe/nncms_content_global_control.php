<?php
/*
 * Created on 2012-6-1
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include ($nncms_config_path. "nn_cms_config/nn_cms_config_path.php");
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";

$checkpri=new nns_db_pri_class();
if (!$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"100100")){
	Header("Location: nncms_content_wrong.php");
}
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
 $checkpri=null;
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

 $webdomain=$_POST["webdomain"];
 $cachetime=$_POST["cachetime"];
 $listpagenum=$_POST["listpagenum"];
 $devicenum=$_POST["devicenum"];
 $partner_enabled=$_POST["partner_enabled"];
 $group_enabled=$_POST["group_enabled"];
 $cache_enabled=$_POST["cache_enabled"];
 $product_enabled=$_POST["product_enabled"];
$core_url=$_POST["core_url"];
$core_userid=$_POST["core_userid"];
$core_password=$_POST["core_password"];
$core_npss_url=$_POST["core_npss_url"];
// var_dump($result);
// echo $_SERVER['PHP_SELF'];
 if (!empty($webdomain) && !empty($cachetime) && !empty($listpagenum)){
 	if (empty($devicenum)) $devicenum=32;
 	require_once $nncms_db_path. "nns_log".DIRECTORY_SEPARATOR . "nns_db_op_log_class.php";
	$log_inst=new nns_db_op_log_class();
			
 	$configcode="<?php \r\n";
 	$configcode.="\$g_webdir=\"". $webdomain. "\";\r\n";
 	$configcode.="\$g_temptime=". $cachetime. ";\r\n";
 	$configcode.="\$g_manager_list_max_num=". $listpagenum. ";\r\n";
 	$configcode.="\$g_manager_list_device_num=". $devicenum. ";\r\n";
 	$configcode.="\$g_partner_enabled=". $partner_enabled. ";\r\n";
 	$configcode.="\$g_group_enabled=". $group_enabled. ";\r\n";
 	$configcode.="\$g_product_enabled=". $product_enabled. ";\r\n";
 	$configcode.="\$g_core_url = \"".$core_url."\";\r\n";
 	$configcode.="\$g_core_userid = \"".$core_userid."\";\r\n";
 	$configcode.="\$g_core_password = \"".$core_password."\";\r\n";
 	$configcode.="\$g_core_npss_url = \"".$core_npss_url."\";\r\n";
 	$configcode.="\$g_cache_enabled = \"".$cache_enabled."\";\r\n";
 	$configcode.="\$g_live_enabled = \"".$g_live_enabled."\";\r\n";
 	$configcode.="\$g_ipqam_enabled = \"".$g_ipqam_enabled."\";\r\n";
 	$configcode.="?>";
 	$path1=$config_path1. "nn_cms_config/nn_cms_global.php";
 	$path2=$config_path2. "nn_cms_config/nn_cms_global.php";
 	if (is_dir($config_path1)){
	 	$fp1 = @fopen( $path1,"w");
		@fwrite($fp1,$configcode);
		@fclose($fp1);
 	}
 	if (is_dir($config_path2)){
		$fp2 = @fopen( $path2,"w");
		@fwrite($fp2,$configcode);
		@fclose($fp2);
 	}
	$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],"edit",$_SESSION["nns_mgr_name"].cms_get_lang('edit|xtgl'),$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
	$log_inst=null;
	echo "<script>alert('".cms_get_lang('edit'). cms_get_lang('success') ."');self.location='nncms_content_global.php';</script>";
 }
?>