<?php
error_reporting(7);
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cdn/cdn_live_queue_log.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
//导入语言包
if (isset($_SESSION["language_dir"]))
{
	$language_dir = $_SESSION["language_dir"];
}
include_once $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include_once $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool)
{
	Header("Location: ../nncms_content_wrong.php");
	exit();
}
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE, 
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$cp_id = $_REQUEST['cp_id'];
$cp_data = nl_cp::query_by_id($dc,$cp_id);
$cp_data = isset($cp_data['data_info']) ? $cp_data['data_info'] : array();
$params_query = array(
		'where'=>array(
				'nns_cp_id'=>$cp_id,
		),
);
function pub_func_get_desc_alt($item) {
	if(!empty($item))
	{
		$array['substr'] = (mb_strlen($item) > 12) ? mb_substr( $item, 0, 12, 'utf-8' ).'...' : $item;
	}
	$array['html'] =  $item;
	return $array;
}
if(isset($_REQUEST['op']) && !empty($_REQUEST['op']))
{
	if($_REQUEST['op'] == 'do_delete')
	{
		$arr_id = array_filter(array_unique(explode(',', isset($_REQUEST['ids']) ? $_REQUEST['ids'] : '')));
		if(empty($arr_id) || !is_array($arr_id))
		{
			echo '<script>alert("操作成功");history.go(-1);</script>';die;
		}
		if(!in_array($_REQUEST['nns_delete'], array(0,1)))
		{
			echo '<script>alert("参数错误");history.go(-1);</script>';die;
		}
		$result=nl_cdn_live_queue_log::delete($dc,$arr_id);
		echo '<script>alert("'.$result['reason'].'");history.go(-1);</script>';die;
	}
}
$page_info = $params = array ();
$params['where']['nns_cp_id'] = $cp_id;
$page_info['page_num'] = isset($_GET['page']) ? $_GET['page'] : 1;
$page_info['page_size'] = isset($_GET['page_size']) ? $_GET['page_size'] : (isset($_COOKIE['page_max_num']) && $_COOKIE['page_max_num'] > 0) ? $_COOKIE['page_max_num'] : 18;
if (isset($_REQUEST['nns_video_name']) && strlen($_REQUEST['nns_video_name']) > 0)
{
	$params['like']['nns_name'] = $_REQUEST['nns_video_name'];
}
if (isset($_REQUEST['nns_cdn_queue_id']) && strlen($_REQUEST['nns_cdn_queue_id']) > 0)
{
	$params['where']['nns_cdn_queue_id'] = $_REQUEST['nns_cdn_queue_id'];
}
if (isset($_REQUEST['nns_action']) && strlen($_REQUEST['nns_action']) > 0)
{
	$params['where']['nns_action'] = $_REQUEST['nns_action'];
}
if (isset($_REQUEST['create_begin_time']) && strlen($_REQUEST['create_begin_time']) > 0)
{
	$params['where']['create_begin_time'] = $_REQUEST['create_begin_time'];
}
if (isset($_REQUEST['create_end_time']) && strlen($_REQUEST['create_end_time']) > 0)
{
	$params['where']['create_end_time'] = $_REQUEST['create_end_time'];
}
$url = 'nncms_cdn_live_queue_log.php?flag_url=1';
foreach ($_REQUEST as $k => $v)
{
	if ($k != 'flag_url' && $k != 'page')
	{
		$url .= '&' . $k . '=' . $v;
	}
}

$result = nl_cdn_live_queue_log::query($dc, $params, $page_info);
$pager = new nl_pager($result['page_info']['total_count'], $page_info['page_size'], $page_info['page_num'], $url);
if ($result['ret'] != 0)
{
	echo '<script>alert("' . $result['reason'] . '");</script>';
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
<link href="../../css/comm.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/rate.js"></script>
<script language="javascript" src="../../js/image_loaded_func.js"></script>
<script language="javascript" src="../../js/cms_datepicker.js"></script>
<script language="javascript" src="../../js/alertbox.js"></script>
<script language="javascript">
			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				})
				return BoxKey;
			}
			function do_delete(mes)
			{
				var r=confirm("是否进行该操作");
				var nns_audit;
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "nncms_cdn_live_queue_log.php?op=do_delete&cp_id=<?php echo $cp_id;?>&ids="+ids;
						window.location.href = url;
					}
				}
			}
			$(document).ready(function() {
				$('#clear_time').click(function(){
					$('#create_begin_time').val('');
					$('#create_end_time').val('');

				});
				window.parent.now_frame_url = window.location.href;
			});

			$(document).ready(function() {
				$('#clear_time_modify').click(function(){
					$('#nns_begin_time').val('');
					$('#nns_end_time').val('');

				});
				window.parent.now_frame_url = window.location.href;
			});
		</script>
</head>
<body>
	<div class="content">
		<div class="content_position">CDN直播频道日志列表</div>
		<div class="content_table">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<form action="nncms_cdn_live_queue_log.php" method="get">
					<tbody>
						<tr>
						<td>
							 &nbsp;&nbsp;&nbsp;&nbsp;队列ID：&nbsp;&nbsp;<input type="text" name="nns_cdn_queue_id"
								id="nns_cdn_queue_id" style="width:200px;"
								value="<?php echo $_GET['nns_cdn_queue_id']; ?>"
								>
							 &nbsp;&nbsp;&nbsp;&nbsp;影片名称：&nbsp;&nbsp;<input type="text" name="nns_video_name"
								id="nns_video_name" style="width:200px;"
								value="<?php echo $_GET['nns_video_name']; ?>"
								>
							 &nbsp;&nbsp;&nbsp;&nbsp;操作类型：&nbsp;&nbsp; <select name="nns_action" style="width: 120px;">
									<option value="">全部</option>
									<option value="online"
										<?php if($_GET['nns_action'] == '0'){ echo "selected";}?>>添加</option>
									<option value="unline"
										<?php if($_GET['nns_action'] == '1'){ echo "selected";}?>>修改</option>
									<option value="unline"
										<?php if($_GET['nns_action'] == '2'){ echo "selected";}?>>删除</option>
							</select>
							<br/>
							 &nbsp;&nbsp;&nbsp;&nbsp;选择创建时间段：&nbsp;&nbsp;&nbsp;<input name="create_begin_time" id="create_begin_time" type="text"  value="<?php
										if (isset($_GET['day_picker_start']))
											echo $_GET['day_picker_start'];
									?>" style="width:120px;" class="datetimepicker" callback="test" />
										 - <input name="create_end_time" id="create_end_time" type="text"  value="<?php
											if (isset($_GET['create_end_time']))
												echo $_GET['create_end_time'];
										?>" style="width:120px;" class="datetimepicker" callback="test" />
										 &nbsp;&nbsp;<input type="button" id="clear_time" name="clear_time" value="清除时间"/>
							<input type="hidden" value="<?php echo $cp_id; ?>" name="cp_id" />
							<input type="submit" value="查询" id="inquery" name="inquery" />
						</td>
						</tr>
					</tbody>
				</form>
			</table>
		</div>
		<div class="content_table formtable">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th style="width: 50px;"><input name="" type="checkbox" value="">序号</th>
						<th style="width: 100px;">队列ID</th>
						<th style="width: 120px;">影片名称</th>
						<th style="width: 50px;">操作类型</th>
						<th style="width: 50px;">CND发送消息</th>
						<th style="width: 50px;">CND反馈消息</th>
						<th style="width: 50px;">响应发送消息</th>
						<th style="width: 50px;">响应反馈消息</th>
						<th style="width: 50px;">CMS发送消息</th>
						<th style="width: 50px;">CMS反馈消息</th>
						<th style="width: 155px;">创建时间</th>
						<th style="width: 50px;">操作</th>
					</tr>
				</thead>
				<tbody>
						<?php
						if (!empty($result['data_info']) && is_array($result['data_info']))
						{
							$num = ($page_info['page_num'] - 1) * $page_info['page_size'];
							foreach ($result['data_info'] as $item)
							{
								$num++;
								?>
						<tr>
						<td id="is_delete"><input name="input" type="checkbox"
							value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
						<td><?php echo $item['nns_message_id'];?></td>
						<td><?php echo $item['nns_name'];?></td>
						<td><?php if($item['nns_action'] == '0'){ echo "添加";}
								  else if($item['nns_action'] == '1'){ echo "修改";}
								  else if($item['nns_action'] == '2'){ echo "删除";}
								  else{ echo "<font style=\"color:#FF0000\">未知</font>";};?>
						<td>
							<?php $arr_desc_info= pub_func_get_desc_alt($item['nns_cdn_send']);?>
							<alt alt="<?php echo $arr_desc_info['html']; ?>">
								<?php echo "<font style='color: #000000'>{$arr_desc_info['substr']}</font>";?>
							</alt>
						</td>
						<td>
							<?php $arr_desc_info= pub_func_get_desc_alt($item['nns_cdn_notify']);?>
							<alt alt="<?php echo $arr_desc_info['html']; ?>">
								<?php echo "<font style='color: #000000'>{$arr_desc_info['substr']}</font>";?>
							</alt>
						</td>
						<td>
							<?php $arr_desc_info= pub_func_get_desc_alt($item['nns_cms_send']);?>
							<alt alt="<?php echo $arr_desc_info['html']; ?>">
								<?php echo "<font style='color: #000000'>{$arr_desc_info['substr']}</font>";?>
							</alt>
						</td>
						<td>
							<?php $arr_desc_info= pub_func_get_desc_alt($item['nns_cms_notify']);?>
							<alt alt="<?php echo $arr_desc_info['html']; ?>">
								<?php echo "<font style='color: #000000'>{$arr_desc_info['substr']}</font>";?>
							</alt>
						</td>
						<td>
							<?php $arr_desc_info= pub_func_get_desc_alt($item['nns_firm_send']);?>
							<alt alt="<?php echo $arr_desc_info['html']; ?>">
								<?php echo "<font style='color: #000000'>{$arr_desc_info['substr']}</font>";?>
							</alt>
						</td>
						<td>
							<?php $arr_desc_info= pub_func_get_desc_alt($item['nns_firm_notify']);?>
							<alt alt="<?php echo $arr_desc_info['html']; ?>">
								<?php echo "<font style='color: #000000'>{$arr_desc_info['substr']}</font>";?>
							</alt>
						</td>
						<td><?php echo $item['nns_create_time'];?></td>

						<td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                        	<a href="nncms_cdn_live_queue_log.php?op=do_delete&cp_id=<?php echo $cp_id; ?>&ids=<?php echo $item['nns_id'];?>">删除日志</a>
                        </td>
					</tr>
						<?php
							}
						}$least_num = $page_info['page_size'] - count($result['data_info']);
						for ($i = 0; $i < $least_num; $i++) {
						?>
						<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
						<?php } ?>
					</tbody>
			</table>
		</div>
			<?php echo $pager->nav();?>
		<div class="controlbtns">
			<div class="controlbtn allselect">
				<a href="javascript:selectAllCheckBox(true);">全选</a>
			</div>
			<div class="controlbtn cancel">
				<a href="javascript:selectAllCheckBox(false);">取消</a>
			</div>
			<div class="controlbtn move">
				<a href="javascript:do_delete('delete');">删除日志</a>
			</div>
			<div style="clear: both;"></div>
		</div>

	</div>
</body>
</html>