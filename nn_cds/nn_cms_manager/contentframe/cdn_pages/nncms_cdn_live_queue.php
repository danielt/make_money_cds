<?php
error_reporting(7);
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cdn/cdn_live_queue.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
//导入语言包
if (isset($_SESSION["language_dir"]))
{
	$language_dir = $_SESSION["language_dir"];
}
include_once $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include_once $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool)
{
	Header("Location: ../nncms_content_wrong.php");
	exit();
}
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE, 
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$cp_id = $_REQUEST['cp_id'];
$cp_data = nl_cp::query_by_id($dc,$cp_id);
$cp_data = isset($cp_data['data_info']) ? $cp_data['data_info'] : array();
$params_query = array(
		'where'=>array(
				'nns_cp_id'=>$cp_id,
		),
);
if(isset($_REQUEST['op']) && !empty($_REQUEST['op']))
{
	if($_REQUEST['op'] == 'retry')
	{
		include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/models/import_model.php';
		$arr_id = array_filter(array_unique(explode(',', isset($_REQUEST['ids']) ? $_REQUEST['ids'] : '')));
		if(empty($arr_id) || !is_array($arr_id))
		{
			echo '<script>alert("操作成功");history.go(-1);</script>';die;
		}
		define('ORG_ID', 'unicom_zte');
		foreach ($arr_id as $val)
		{
			$queue_info = nl_query_by_db("select * from nns_cdn_live_queue where nns_id='{$val}'", $dc->db());
			if(!isset($queue_info[0]) || !is_array($queue_info[0]) || empty($queue_info[0]))
			{
				continue;
			}
			$queue_info = $queue_info[0];
			if($_REQUEST['mssage'] == 'notify')
			{
				if($queue_info['nns_import_cdn_state'] !='2' || $queue_info['nns_import_cdn_state'] !='3' )
				{
					continue;
				}
				$op_action = 'regist';
				if($queue_info['nns_action'] == '1')
				{
					$op_action = 'update';
				}
				else if($queue_info['nns_action'] == '2')
				{
					$op_action = 'delete';
				}
				$array_media = nl_query_by_db("select * from nns_live_media where nns_id='{$queue_info['nns_live_media_id']}'", $dc->db());
				if(!isset($array_media[0]) || !is_array($array_media[0]) || empty($array_media[0]))
				{
					echo '<script>alert("查无频道数据");history.go(-1);</script>';die;
				}
				$info = array(
						'nns_cp_id'=>$array_media[0]['nns_cp_id'],
						'nns_import_id'=>$array_media[0]['nns_content_id'],
				);
				$c2_task_info = array(
						'nns_message_id'=>$queue_info['nns_message_id']
				);
				$bool = ($queue_info['nns_import_cdn_state'] =='2') ? false : true;
				$sp_config = array(
						'media_cdn_model'=>1,
						'asset_cdn_share_addr'=>'ftp://cdn_ftp:cdn_ftp@10.100.106.13:21/'
				);
				$re = import_model::notify_cms_data($info,'starcor',$op_action,'live',$c2_task_info,$sp_config,$bool);
				if($re === false)
				{
					echo '<script>alert("消息反馈失败");history.go(-1);</script>';die;
				}
			}
			else if($_REQUEST['message'] == 'cdn')
			{
				$array_live = nl_query_by_db("select * from nns_live where nns_id='{$queue_info['nns_live_id']}'", $dc->db());
				if(!is_array($array_live) || empty($array_live))
				{
					echo '<script>alert("查无直播数据");history.go(-1);</script>';die;
				}
				$array_media = nl_query_by_db("select * from nns_live_media where nns_id='{$queue_info['nns_live_media_id']}'", $dc->db());
				if(!is_array($array_media) || empty($array_media))
				{
					echo '<script>alert("查无频道数据");history.go(-1);</script>';die;
				}
				$array_live=$array_live[0];
				$op_action = 'regist';
				if($queue_info['nns_action'] == '1')
				{
					$op_action = 'update';
				}
				else if($queue_info['nns_action'] == '2')
				{
					$op_action = 'delete';
				}
				$re = import_model::import_cdn_live_data($array_live,$array_media,$op_action,ORG_ID);
				if($re['ret'] !=0)
				{
					echo '<script>alert("'.var_export($re,true).'");history.go(-1);</script>';die;
				}
			}
			else if($_REQUEST['mssage'] == 'cms')
			{
				
			}
			else
			{
				echo '<script>alert("无此操作方式");history.go(-1);</script>';die;
			}
		}
		echo '<script>alert("操作成功");history.go(-1);</script>';die;
	}
	else if($_REQUEST['op'] == 'do_delete')
	{
		$arr_id = array_filter(array_unique(explode(',', isset($_REQUEST['ids']) ? $_REQUEST['ids'] : '')));
		if(empty($arr_id) || !is_array($arr_id))
		{
			echo '<script>alert("操作成功");history.go(-1);</script>';die;
		}
		if(!in_array($_REQUEST['nns_delete'], array(0,1)))
		{
			echo '<script>alert("参数错误");history.go(-1);</script>';die;
		}
		$result=nl_cdn_live_queue::delete($dc,$arr_id);
		echo '<script>alert("'.$result['reason'].'");history.go(-1);</script>';die;
	}
}
$page_info = $params = array ();
$params['where']['nns_cp_id'] = $cp_id;
$page_info['page_num'] = isset($_GET['page']) ? $_GET['page'] : 1;
$page_info['page_size'] = isset($_GET['page_size']) ? $_GET['page_size'] : (isset($_COOKIE['page_max_num']) && $_COOKIE['page_max_num'] > 0) ? $_COOKIE['page_max_num'] : 18;
if (isset($_REQUEST['nns_video_name']) && strlen($_REQUEST['nns_video_name']) > 0)
{
	$params['like']['nns_name'] = $_REQUEST['nns_video_name'];
}
if (isset($_REQUEST['nns_message_id']) && strlen($_REQUEST['nns_message_id']) > 0)
{
	$params['where']['nns_message_id'] = $_REQUEST['nns_message_id'];
}
if (isset($_REQUEST['nns_import_cdn_state']) && strlen($_REQUEST['nns_import_cdn_state']) > 0)
{
	$params['where']['nns_import_cdn_state'] = $_REQUEST['nns_import_cdn_state'];
}
if (isset($_REQUEST['nns_import_cms_state']) && strlen($_REQUEST['nns_import_cms_state']) > 0)
{
	$params['where']['nns_import_cms_state'] = $_REQUEST['nns_import_cms_state'];
}
if (isset($_REQUEST['nns_import_notify_state']) && strlen($_REQUEST['nns_import_notify_state']) > 0)
{
	$params['where']['nns_import_notify_state'] = $_REQUEST['nns_import_notify_state'];
}
if (isset($_REQUEST['nns_action']) && strlen($_REQUEST['nns_action']) > 0)
{
	$params['where']['nns_action'] = $_REQUEST['nns_action'];
}
if (isset($_REQUEST['create_begin_time']) && strlen($_REQUEST['create_begin_time']) > 0)
{
	$params['where']['create_begin_time'] = $_REQUEST['create_begin_time'];
}
if (isset($_REQUEST['create_end_time']) && strlen($_REQUEST['create_end_time']) > 0)
{
	$params['where']['create_end_time'] = $_REQUEST['create_end_time'];
}
$url = 'nncms_cdn_live_queue.php?flag_url=1';
foreach ($_REQUEST as $k => $v)
{
	if ($k != 'flag_url' && $k != 'page')
	{
		$url .= '&' . $k . '=' . $v;
	}
}

$result = nl_cdn_live_queue::query($dc, $params, $page_info);
$pager = new nl_pager($result['page_info']['total_count'], $page_info['page_size'], $page_info['page_num'], $url);
if ($result['ret'] != 0)
{
	echo '<script>alert("' . $result['reason'] . '");</script>';
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
<link href="../../css/comm.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/rate.js"></script>
<script language="javascript" src="../../js/image_loaded_func.js"></script>
<script language="javascript" src="../../js/cms_datepicker.js"></script>
<script language="javascript" src="../../js/alertbox.js"></script>
<script language="javascript">
			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				})
				return BoxKey;
			}
			function retry(mes)
			{
				var r=confirm("是否进行该操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						if(mes == 'notify')
						{
							var url = "nncms_cdn_live_queue.php?op=retry&mssage="+mes+"&cp_id=<?php echo $cp_id;?>&ids="+ids;
						}
						else if(mes == 'cdn')
						{
							var url = "nncms_cdn_live_queue.php?op=retry&mssage="+mes+"&cp_id=<?php echo $cp_id;?>&ids="+ids;
						}
						else if(mes == 'cms')
						{
							var url = "nncms_cdn_live_queue.php?op=retry&mssage="+mes+"&cp_id=<?php echo $cp_id;?>&ids="+ids;
						}
						else
						{
							alert("没有此方法");history.go(-1);
						}
						window.location.href = url;
					}
				}
			}
			function do_delete(mes)
			{
				var r=confirm("是否进行该操作");
				var nns_audit;
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "nncms_cdn_live_queue.php?op=do_delete&cp_id=<?php echo $cp_id;?>&ids="+ids;
						window.location.href = url;
					}
				}
			}
			$(document).ready(function() {
				$('#clear_time').click(function(){
					$('#create_begin_time').val('');
					$('#create_end_time').val('');

				});
				window.parent.now_frame_url = window.location.href;
			});

			$(document).ready(function() {
				$('#clear_time_modify').click(function(){
					$('#nns_begin_time').val('');
					$('#nns_end_time').val('');

				});
				window.parent.now_frame_url = window.location.href;
			});
		</script>
</head>
<body>
	<div class="content">
		<div class="content_position">CDN频道队列列表</div>
		<div class="content_table">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<form action="nncms_cdn_live_queue.php" method="get">
					<tbody>
						<tr>
						<td>
							 &nbsp;&nbsp;&nbsp;&nbsp;消息ID：&nbsp;&nbsp;<input type="text" name="nns_message_id"
								id="nns_message_id" style="width:200px;"
								value="<?php echo $_GET['nns_message_id']; ?>"
								>
							 &nbsp;&nbsp;&nbsp;&nbsp;影片名称：&nbsp;&nbsp;<input type="text" name="nns_video_name"
								id="nns_video_name" style="width:200px;"
								value="<?php echo $_GET['nns_video_name']; ?>"
								>
							 &nbsp;&nbsp;&nbsp;&nbsp;频道id：&nbsp;&nbsp;<input type="text" name="nns_live_media_id"
								id="nns_live_media_id" style="width:200px;"
								value="<?php echo $_GET['nns_live_media_id']; ?>"
								>
							<br/>
							 &nbsp;&nbsp;&nbsp;&nbsp;操作类型：&nbsp;&nbsp; <select name="nns_action" style="width: 120px;">
									<option value="">全部</option>
									<option value="online"
										<?php if($_GET['nns_action'] == '0'){ echo "selected";}?>>添加</option>
									<option value="unline"
										<?php if($_GET['nns_action'] == '1'){ echo "selected";}?>>修改</option>
									<option value="unline"
										<?php if($_GET['nns_action'] == '2'){ echo "selected";}?>>删除</option>
							</select>
							
							&nbsp;&nbsp;&nbsp;&nbsp;注入CDN状态：&nbsp;&nbsp; <select name="nns_import_cdn_state" style="width: 120px;">
									<option value="">全部</option>
									<option value="online"
										<?php if($_GET['nns_import_cdn_state'] == '0'){ echo "selected";}?>>等待注入CDN</option>
									<option value="unline"
										<?php if($_GET['nns_import_cdn_state'] == '1'){ echo "selected";}?>>正在注入CDN</option>
									<option value="unline"
										<?php if($_GET['nns_import_cdn_state'] == '2'){ echo "selected";}?>>注入CDN成功</option>
									<option value="unline"
										<?php if($_GET['nns_import_cdn_state'] == '3'){ echo "selected";}?>>注入CDN失败</option>
							</select>
							&nbsp;&nbsp;&nbsp;&nbsp;响应上游状态：&nbsp;&nbsp; <select name="nns_import_cdn_state" style="width: 120px;">
									<option value="">全部</option>
									<option value="online"
										<?php if($_GET['nns_import_notify_state'] == '0'){ echo "selected";}?>>等待响应</option>
									<option value="unline"
										<?php if($_GET['nns_import_notify_state'] == '1'){ echo "selected";}?>>正在响应</option>
									<option value="unline"
										<?php if($_GET['nns_import_notify_state'] == '3'){ echo "selected";}?>>响应成功</option>
									<option value="unline"
										<?php if($_GET['nns_import_notify_state'] == '4'){ echo "selected";}?>>响应失败</option>
							</select>
							&nbsp;&nbsp;&nbsp;&nbsp;注入CMS状态：&nbsp;&nbsp; <select name="nns_import_cdn_state" style="width: 120px;">
									<option value="">全部</option>
									<option value="online"
										<?php if($_GET['nns_import_cdn_state'] == '0'){ echo "selected";}?>>等待注入CMS</option>
									<option value="unline"
										<?php if($_GET['nns_import_cdn_state'] == '1'){ echo "selected";}?>>正在注入CMS</option>
									<option value="unline"
										<?php if($_GET['nns_import_cdn_state'] == '2'){ echo "selected";}?>>注入CMS成功</option>
									<option value="unline"
										<?php if($_GET['nns_import_cdn_state'] == '3'){ echo "selected";}?>>注入CMS失败</option>
							</select>
							<br/>
							 &nbsp;&nbsp;&nbsp;&nbsp;选择创建时间段：&nbsp;&nbsp;&nbsp;<input name="create_begin_time" id="create_begin_time" type="text"  value="<?php
										if (isset($_GET['day_picker_start']))
											echo $_GET['day_picker_start'];
									?>" style="width:120px;" class="datetimepicker" callback="test" />
										 - <input name="create_end_time" id="create_end_time" type="text"  value="<?php
											if (isset($_GET['create_end_time']))
												echo $_GET['create_end_time'];
										?>" style="width:120px;" class="datetimepicker" callback="test" />
										 &nbsp;&nbsp;<input type="button" id="clear_time" name="clear_time" value="清除时间"/>
							<input type="hidden" value="<?php echo $cp_id; ?>" name="cp_id" />
							<input type="submit" value="查询" id="inquery" name="inquery" />
						</td>
						</tr>
					</tbody>
				</form>
			</table>
		</div>
		<div class="content_table formtable">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th style="width: 50px;"><input name="" type="checkbox" value="">序号</th>
						<th style="width: 100px;">消息ID</th>
						<th style="width: 100px;">频道ID</th>
						<th style="width: 120px;">影片名称</th>
						<th style="width: 50px;">操作类型</th>
						<th style="width: 50px;">CDN状态</th>
						<th style="width: 50px;">注入CDN次数</th>
						<th style="width: 50px;">响应上游状态</th>
						<th style="width: 50px;">响应上游次数</th>
						<th style="width: 50px;">CMS状态</th>
						<th style="width: 50px;">CMS次数</th>
						<th style="width: 155px;">创建时间</th>
						<th style="width: 155px;">修改时间</th>
						<th style="width: 50px;">操作</th>
					</tr>
				</thead>
				<tbody>
						<?php
						if (!empty($result['data_info']) && is_array($result['data_info']))
						{
							$num = ($page_info['page_num'] - 1) * $page_info['page_size'];
							foreach ($result['data_info'] as $item)
							{
								$num++;
								?>
						<tr>
						<td id="is_delete"><input name="input" type="checkbox"
							value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
						<td><?php echo $item['nns_message_id'];?></td>
						<td><?php echo $item['nns_live_media_id'];?></td>
						<td><?php echo $item['nns_name'];?></td>
						<td><?php if($item['nns_action'] == '0'){ echo "添加";}
								  else if($item['nns_action'] == '1'){ echo "修改";}
								  else if($item['nns_action'] == '2'){ echo "删除";}
								  else{ echo "<font style=\"color:#FF0000\">未知</font>";};?>
						</td>
						<td><?php if($item['nns_import_cdn_state'] == '0'){ echo "等待注入CDN";}
								  else if($item['nns_import_cdn_state'] == '1'){ echo "正在注入CDN";}
								  else if($item['nns_import_cdn_state'] == '2'){ echo "注入CDN成功";}
								  else if($item['nns_import_cdn_state'] == '3'){ echo "<font style=\"color:#FF0000\">注入CDN失败</font>";}
								  else{ echo "<font style=\"color:#FF0000\">未知</font>";};?>
						</td>
						<td><?php echo $item['nns_import_cdn_time'];?></td>
						<td><?php if($item['nns_import_notify_state'] == '0'){ echo "等待响应上游";}
								  else if($item['nns_import_notify_state'] == '1'){ echo "正在响应上游";}
								  else if($item['nns_import_notify_state'] == '2'){ echo "响应成功";}
								  else if($item['nns_import_notify_state'] == '3'){ echo "<font style=\"color:#FF0000\">响应失败</font>";}
								  else{ echo "<font style=\"color:#FF0000\">未知</font>";};?>
						</td>
						<td><?php echo $item['nns_import_notify_time'];?></td>
						<td><?php if($item['nns_import_cms_state'] == '0'){ echo "等待注入CMS";}
								  else if($item['nns_import_cms_state'] == '1'){ echo "正在注入CMS";}
								  else if($item['nns_import_cms_state'] == '2'){ echo "注入CMS成功";}
								  else if($item['nns_import_cms_state'] == '3'){ echo "<font style=\"color:#FF0000\">注入CMS失败</font>";}
								  else{ echo "<font style=\"color:#FF0000\">未知</font>";};?>
						</td>
						<td><?php echo $item['nns_import_cms_time'];?></td>
						<td><?php echo $item['nns_create_time'];?></td>
						<td><?php echo $item['nns_modify_time'];?></td>

						<td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                        	<?php if($item['nns_import_cdn_state'] == '2' || $item['nns_import_cdn_state'] == '1' ){?>
                        		<a href="nncms_cdn_live_queue.php?op=retry&mssage=cdn&cp_id=<?php echo $cp_id; ?>&ids=<?php echo $item['nns_id'];?>">重新注入CDN</a>
                        	<?php }else if($item['nns_import_cdn_state'] == '0'){?>
                        		<a href="nncms_cdn_live_queue.php?op=retry&mssage=cdn&cp_id=<?php echo $cp_id; ?>&ids=<?php echo $item['nns_id'];?>">注入CDN</a>
                        	<?php }?>
                        	<?php if($item['nns_import_notify_state'] == '2' || $item['nns_import_notify_state'] == '1' ){?>
                        		<a href="nncms_cdn_live_queue.php?op=retry&mssage=notify&cp_id=<?php echo $cp_id; ?>&ids=<?php echo $item['nns_id'];?>">重新反馈上游</a>
                        	<?php }else if($item['nns_import_notify_state'] == '0'){?>
                        		<a href="nncms_cdn_live_queue.php?op=retry&mssage=notify&cp_id=<?php echo $cp_id; ?>&ids=<?php echo $item['nns_id'];?>">反馈上游</a>
                        	<?php }?>
                        	<?php if($item['nns_import_cms_state'] == '2'  || $item['nns_import_cms_state'] == '1' ){?>
                        		<a href="nncms_cdn_live_queue.php?op=retry&mssage=cms&cp_id=<?php echo $cp_id; ?>&ids=<?php echo $item['nns_id'];?>">重新注入CMS</a>
                        	<?php }else if($item['nns_import_cms_state'] == '0'){?>
                        		<a href="nncms_cdn_live_queue.php?op=retry&mssage=cms&cp_id=<?php echo $cp_id; ?>&ids=<?php echo $item['nns_id'];?>">注入CMS</a>
                        	<?php }?>
                        	<a href="nncms_cdn_live_queue.php?op=do_delete&cp_id=<?php echo $cp_id; ?>&ids=<?php echo $item['nns_id'];?>">删除队列</a>
                        </td>
					</tr>
						<?php
							}
						}$least_num = $page_info['page_size'] - count($result['data_info']);
						for ($i = 0; $i < $least_num; $i++) {
						?>
						<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
						<?php } ?>
					</tbody>
			</table>
		</div>
			<?php echo $pager->nav();?>
		<div class="controlbtns">
			<div class="controlbtn allselect">
				<a href="javascript:selectAllCheckBox(true);">全选</a>
			</div>
			<div class="controlbtn cancel">
				<a href="javascript:selectAllCheckBox(false);">取消</a>
			</div>
			<div class="controlbtn move">
				<a href="javascript:retry('cdn');">注入CDN</a>
			</div>
			<div class="controlbtn move">
				<a href="javascript:retry('notify');">反馈上游</a>
			</div>
			<div class="controlbtn move">
				<a href="javascript:retry('cms');">注入CMS</a>
			</div>
			<div class="controlbtn move">
				<a href="javascript:do_delete('delete');">删除</a>
			</div>
			<div style="clear: both;"></div>
		</div>

	</div>
</body>
</html>