<?php

/**
 * @author txc by 2012-11-21
 */
header("Content-Type:text/html;charset=utf-8");
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
require_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_clear_cache.php";
require_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";


$video_id = isset($_POST["video_id"]) ? $_POST["video_id"] : $_GET['video_id']; //视频ID
$video_name = isset($_POST['video_name']) ? $_POST['video_name'] : $_GET['video_name'];
$action = isset($_POST["action"]) ? $_POST["action"] : $_GET["action"]; //操作类型
$image_id = isset($_POST["image_id"]) ? $_POST["image_id"] : $_GET['image_id']; //图片ID
$video_type = isset($_POST['video_type']) ? $_POST['video_type'] : $_GET['video_type'];
//echo '<pre>';
//print_r($_POST);exit;
$pri_bool = true;

if (!empty($action)) {
        //获取权限检查类
        include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
        $checkpri = new nns_db_pri_class();
        switch ($action) {
                case "add":
                        $action_str = cms_get_lang('add|topic_xxb');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "121107");
                        break;
                case "edit":
                        $action_str = cms_get_lang('add|topic_xxb');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "121108");
                        break;
                case "delete":
                        $action_str = cms_get_lang('add|topic_xxb');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "121109");
                        break;
                case "up":
                        $action_str = cms_get_lang('add|topic_xxb');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "121109");
                        break;
                case "down":
                        $action_str = cms_get_lang('add|topic_xxb');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "121109");
                        break;
                case "first":
                        $action_str = cms_get_lang('add|topic_xxb');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "121109");
                        break;
                case "last":
                        $action_str = cms_get_lang('add|topic_xxb');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "121109");
                        break;
        }
        $checkpri = null;
}
if ($pri_bool === false) {
        Header("Location: ../nncms_content_wrong.php");
        exit;
}


include $nncms_config_path . 'nn_logic/stills/stills.class.php';
include $nncms_config_path . 'nn_logic/log/manager_log.class.php';
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        )
);
$dc->open();

$list = nl_stills::get_stills_list_by_scale($dc, $video_id, '72');
//var_dump($list);
//exit;
switch ($action) {
        //添加图片
        case 'add':
                if ($_FILES["error"] > 0) {
                        echo "<script language='javascript'>alert('", cms_get_lang('upload|fault'), "')</script>";
                        echo "<script>location='nncms_content_stills.php?video_name={$video_name}&video_id={$video_id}&video_type={$video_type}'</script>";
                        break;
                }
                $image_type = $_FILES["image"]["type"];
                $image_types = explode("/", $image_type);
                if ($image_types[0] != 'image') {
                        echo "<script language='javascript'>alert('", cms_get_lang('upload_img_file'), "')</script>";
                        echo "<script>location='nncms_content_stills.php?video_name={$video_name}&video_id={$video_id}&video_type={$video_type}'</script>";
                        break;
                }
                $file = $_FILES["image"]["tmp_name"];
                $file_name = $_FILES["image"]["name"];

                $im = getimagesize($_FILES['image']['tmp_name']);

                $width = $im[0];
                $height = $im[1];
                $scale = floor(($height / $width) * 100); //计算图片长宽比    
                $url = pub_func_image_upload_by_vod_stills($_FILES["image"], $video_id);
                $params["nns_image"] = $url;
                $params["nns_scale"] = $scale;
                $params["nns_width"] = $width;
                $params['nns_height'] = $height;
                $params["nns_summary"] = $_POST["summary"];
                $params['nns_create_time'] = date('Y-m-d H:i:s');
                $params['nns_order'] = 0;
                $video_type = $_POST['video_type'];
                $result = nl_stills::add_stills($dc, $video_id, $video_type, $params);
                if (!$result) {
                        echo "<script>alert('", cms_get_lang('add_picture_fail'), "');</script>";
                } else {
                        $log = $_SESSION["nns_mgr_id"] . cms_get_lang('add|media_jz') . ',' . cms_get_lang('video_id') . '：' . $video_id;
                        nl_manager_log::add($dc, $log, $action);
                        echo "<script>alert('", cms_get_lang('add_picture_success'), "')</script>";
                }

                echo "<script>location='nncms_content_stills.php?video_name={$video_name}&video_id={$video_id}&video_type={$video_type}'</script>";
                break;
        //修改
        case 'edit':
                $params['nns_summary'] = $_POST["summary"];
                $result = nl_stills::modify_stills($dc, $image_id, $params);
                if ($result) {

                        $log = $_SESSION["nns_mgr_id"] . cms_get_lang('treat|topic_xxb') . $item_name . cms_get_lang('edit|picture') . ',' . cms_get_lang('picture') . 'ID：' . $image_id;
                        nl_manager_log::add($dc, $log, 'edit');
                        echo "<script>alert('", cms_get_lang('update_success'), "')</script>";
                }
                else
                        echo "<script>alert('", cms_get_lang('update_fault'), "')</script>";
                echo "<script>window.history.go(-1);</script>";
                break;
        //删除
        case 'delete':
                $result = nl_stills:: delete_stills($dc, $image_id);
                if ($result) {
                        $log = $_SESSION["nns_mgr_id"] . cms_get_lang('delete|media_jz|picture') . "ID：" . $image_id;
                        nl_manager_log::add($dc, $log, 'delete');
                }
                echo $result;
                break;
        //向上排序
        case 'up':
                $result = nl_stills::update_stills_to_order_up($dc, $image_id);
                if ($result) {
                        $log = $_SESSION["nns_mgr_id"] . cms_get_lang('treat|media_jz|order') . '：' . cms_get_lang('order_pre') . ',' . cms_get_lang('picture') . "ID：" . $image_id;
                        nl_manager_log::add($dc, $log, 'order');
                }
                echo $result;

                break;
        //向下排序
        case 'down':
                $result = nl_stills::update_stills_to_order_down($dc, $image_id);
                if ($result) {
                        $log = $_SESSION["nns_mgr_id"] . cms_get_lang('treat|media_jz|order|picture') . ':' . cms_get_lang('order_next') . ',' . cms_get_lang('picture') . "ID：" . $image_id;
                        nl_manager_log::add($dc, $log, 'order');
                }
                echo $result;

                break;
        //置顶
        case 'first':
                $result = nl_stills::update_stills_to_order_first($dc, $image_id);
                if ($result) {
                        $log = $_SESSION["nns_mgr_id"] . cms_get_lang('treat|media_jz|order|picture') . ':' . cms_get_lang('order_up') . ',' . cms_get_lang('picture') . "ID：" . $image_id;
                        nl_manager_log::add($dc, $log, 'order');
                }
                echo $result;
                break;
        //置尾
        case 'last':

                $result = nl_stills::update_stills_to_order_last($dc, $image_id);
                if ($result) {
                        $log = $_SESSION["nns_mgr_id"] . cms_get_lang('treat|media_jz|order|picture') . ':' . cms_get_lang('order_down') . ',' . cms_get_lang('picture') . "ID：" . $image_id;
                        nl_manager_log::add($dc, $log, 'order');
                }
                echo $result;
                break;
}
?>
