<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
include_once LOGIC_DIR	.'image/image.class.php';

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106103");
$checkpri = null;

if ($_GET["nns_org_id"] == "" || $_GET["nns_org_type"] == "") {
        $nns_org_id = $_SESSION["nns_org_id"];
        $nns_org_type = $_SESSION["nns_manager_type"];
} else {
        $nns_org_id = $_GET["nns_org_id"];
        $nns_org_type = $_GET["nns_org_type"];
}

$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$group_name = $_GET["group_name"];
if (!$pri_bool) {
        Header("Location: ../nncms_content_wrong.php");
        exit;
}

require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
if (!empty($_COOKIE["page_max_num"])) {
        $g_manager_list_max_num = $_COOKIE["page_max_num"];
}

include $nncms_config_path . "nn_logic/stills/stills.class.php";
//获取DC
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        )
);
$dc->open();

//获取视频ID,视频名称
$video_id = $_GET['video_id'];
$video_name = $_GET['video_name'];
$video_type = $_GET['video_type'];
//分页设置
$total_num = nl_stills::count_stills_num($dc, $video_id, $video_type);
$pages = ceil($total_num / $g_manager_list_max_num);
if (!empty($_GET["page"]))
        $currentpage = $_GET["page"];
else
        $currentpage = 1;
$since = $currentpage * $g_manager_list_max_num - $g_manager_list_max_num;
$url = "?video_id=$video_id&video_name=$video_name&video_type=$video_type";
include $nncms_db_path . "nns_common/nns_db_pager_class.php";
$pager = new nns_db_pager_class($total_num, $g_manager_list_max_num, $currentpage, $url);
//获取剧照list
$stills_list = nl_stills::get_stills_list($dc, $video_id, $video_type, $since, $g_manager_list_max_num, $order = NL_ORDER_NUM_DESC);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                <link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
                <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
                <link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../../js/dtree.js"></script>
                <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
                <script language="javascript" src="../../js/table.js.php"></script>
                <script language="javascript" src="../../js/checkinput.js.php"></script>
                <script language="javascript" src="../../js/tabs.js"></script>
                <script language="javascript" src="../../js/cms_datepicker.js"></script>
                <script language="javascript" src="../../js/cms_cookie.js"></script>
                <script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
                <script language="javascript" src="../../js/category_count.js"></script>
                <script language="javascript" src="../../js/image_loaded_func.js"></script>
                <script language="javascript">

                        function checkheight() {
                                var frame_url = $(".vod_frame iframe")[0].contentWindow.location.href;
                                var page_name = frame_url.split("?")[0];
                                page_name = page_name.split("/");
                                page_name = page_name[page_name.length - 1];
                                if (page_name == "nncms_content_vodlist.php") {
                                        $(".display_list").show();
                                } else {
                                        $(".display_list").hide();
                                        $("#search_advance_tr").hide();
                                }

                                $(".vod_frame iframe").height($(".vod_frame iframe").contents().find(".content").height());
                                $(".category_tree").height($(".category_editbox").height());
                                $(".split_btn").height($(".category_editbox").height() - 20);
                                window.parent.resetFrameHeight();

                        }

                        function close_select() {
                                $(".selectbox").hide();
                        }
                        function form_submit() {
                                if ($('#image').val() == '') {
                                        alert('<?php echo cms_get_lang('please_select_pic'); ?>！ ');
                                        return;
                                }
                                $('add_form').submit();
                        }
                        function image_sort(action, image_id) {
                                $.ajax({
                                        url: "../vod_pages/nncms_content_vod_stills_control.php",
                                        type: "post",
                                        data: {image_id: image_id, action: action},
                                        success: function(ret) {
                                                if (ret == 1 || ret == -1) {
                                                        alert("<?php echo cms_get_lang('sort_success'); ?>");
                                                        window.location.reload();
                                                } else {
                                                        alert("<?php echo cms_get_lang('sort_fail'); ?>");
                                                }
                                        }
                                });
                        }
                        function delete_image(image_id) {
                                $.ajax({
                                        url: "nncms_content_stills_control.php",
                                        type: "post",
                                        data: {image_id: image_id, action: 'delete'},
                                        success: function(ret) {
                                                if (ret == 1) {
                                                        alert("<?php echo cms_get_lang('webkz_sccg'); ?>");
                                                        window.location.reload();
                                                } else {
                                                        alert("<?php echo cms_get_lang('webkz_scsb'); ?>");
                                                }
                                        }
                                });
                        }
                        function modify_image(obj, image_id) {
                                var summary = $(obj).parent().parent().find("td").eq(2).find("font").html();
                                $(obj).parent().parent().find("td").eq(2).find("font").html("");
                                $(obj).html("<?php echo cms_get_lang('confirm'); ?>");
                                var html = "<textarea id='nns_summary' name='summary' sytle='width:100%'>" + summary + "</textarea>";
                                $(obj).parent().parent().find("td").find("#" + image_id).append(html);
                                $("#" + image_id).find("#image_id").attr("value", image_id);
                                $(obj).hide();
                                $(obj).parent().find("#edit_action_form").show();
                        }
                        function checkform(obj, image_id) {
                                checkForm("", "", $(obj).parent().parent().find("#" + image_id), "add");
                        }
                </script>
        </head>

        <body>
                <div class="content">
                        <div class="content_table formtable">
                                <table width="100%"  border="0" cellspacing="0" cellpadding="0" >
                                        <tr>
                                                <?php if ($_GET['url_type'] == '2' || $_GET['url_type'] == '1') { ?>
                                                        <td><?php echo cms_get_lang('video_id'); ?>：<a class="link" href="../vod_pages/nncms_content_vod_detail.php?view_type=<?php echo $video_type; ?>&nns_id=<?php echo $video_id; ?>"><?php echo $video_id; ?></a></td><td><?php echo cms_get_lang('video_spmc'); ?>：<?php echo $video_name ?></td>
                                                <?php } else if ($_GET['url_type'] == '4' || $_GET['url_type'] == '3') { ?>
                                                        <td><?php echo cms_get_lang('video_id'); ?>：<a class="link" href="../live_pages/nncms_content_live_detail.php?view_type=<?php echo $video_type; ?>&nns_id=<?php echo $video_id; ?>"><?php echo $video_id; ?></a></td><td><?php echo cms_get_lang('video_spmc'), '：', $video_name ?></td>
                                                <?php } ?>
                                        </tr>
                                </table>
                                <br/>
                                <div style="background:#CBDEE8;height:30px;line-height:30px;margin-bottom:2px;color:#6B6B6B">&nbsp;<b><?php echo cms_get_lang('media_jzgl'); ?></b></div>
                                <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                        <thead>
                                                <th width="50px"><?php echo cms_get_lang('segnumber'); ?></th>
                                                <th width="300px"><?php echo cms_get_lang('xxgl_tp'); ?></th>
                                                <th width="150px"><?php echo cms_get_lang('describe'); ?></th>
                                                <th width="150px"><?php echo cms_get_lang('order'); ?></th>
                                                <th width="100px"><?php echo cms_get_lang('action'); ?></th>
                                        </thead>
                                        <tbody>

                                                <?php
                                                if (is_array($stills_list)) {
                                                        $num = 0;
                                                        foreach ($stills_list as $image) {
                                                                $num++;
                                                                ?>
                                                                <tr id="<?php echo $image["nns_id"] ?>">
                                                                        <td><input type="hidden" value="<?php echo $image["nns_id"] ?>" /><?php echo $num ?></td>
                                                                        <td><div class="img_content">
                                                                                        <div class='img_view'>
                                                                                                <?php if (!empty($image["nns_image"])) { ?>
                                                                                                        <a style='padding:0px;background:none;' href='<?php echo nl_image::get_manager_image_url($image["nns_image"]); ?>' target='_blank'>
                                                                                                                <img src='<?php echo nl_image::get_manager_image_url($image["nns_image"]); ?>' style='border:none;width:200px;' />
                                                                                                        </a>
                                                                                                <?php } ?>
                                                                                                <h4 class='resolution' ></h4>
                                                                                        </div>
                                                                                </div>
                                                                        </td>
                                                                        <td>
                                                                                <form id="<?php echo $image['nns_id']; ?>" action="nncms_content_s"video_id" name="video_id" />
                                                                                        <input type="hidden" value="<?php echo $video_name; ?>" tills_control.php" method="post">
                                                                                        <input type="hidden" value="<?php echo $video_id; ?>" id=id="video_name" name="video_name" />
                                                                                        <input type="hidden" value="<?php echo $video_type; ?>" id="video_type" name="video_type"/>
                                                                                        <input type="hidden" value="" id="image_id" name="image_id"/>
                                                                                        <input type="hidden" name="action"  id="action" value="edit" />
                                                                                        <input type="hidden" name="summary" id="edit_image_id" value="" />
                                                                                        <font><?php echo $image["nns_summary"] ?></font>
                                                                                </form>
                                                                        </td>
                                                                        <td value="<?php echo $image["nns_id"] ?>">
                                                                                <a href="#" action="up"  onclick="image_sort('up', '<?php echo $image['nns_id']; ?>')"><?php echo cms_get_lang('order_pre'); ?></a>
                                                                                <a href="#" action="down" onclick="image_sort('down', '<?php echo $image['nns_id']; ?>')"><?php echo cms_get_lang('order_next'); ?></a>
                                                                                <a href="#" action="first" onclick="image_sort('first', '<?php echo $image['nns_id']; ?>')"><?php echo cms_get_lang('order_up'); ?></a>
                                                                                <a href="#" action="last" onclick="image_sort('last', '<?php echo $image['nns_id']; ?>')"><?php echo cms_get_lang('order_down'); ?></a>
                                                                        </td>
                                                                        <td value="<?php echo $image["nns_id"] ?>">
                                                                                <a href="#" onclick="javascript:delete_image('<?php echo $image['nns_id']; ?>')"><?php echo cms_get_lang('delete'); ?></a>
                                                                                <a href="#" onclick="javascript:modify_image(this, '<?php echo $image['nns_id']; ?>')"><?php echo cms_get_lang('edit'); ?></a>
                                                                                <a href="#" onclick="javascript:checkform(this, '<?php echo $image['nns_id']; ?>')" id="edit_action_form" style="display:none" action="edit"> <?php echo cms_get_lang('confirm'); ?></a>
                                                                        </td>
                                                                </tr>
                                                        <?php }
                                                }
                                                ?>
                                        </tbody>
                                </table>
<?php echo $pager->nav(); ?>
                                <br/>
                                <div style="background:#CBDEE8;height:30px;line-height:30px;margin:0px;20px;0xp;2px;color:#6B6B6B">&nbsp;<b><?php echo cms_get_lang('add|picture'); ?></b></div>
                                <form id="add_form" name="add_form" action="nncms_content_stills_control.php" method="post" enctype="multipart/form-data">
                                        <input type="hidden" value="<?php echo $video_id; ?>" id="video_id" name="video_id" />
                                        <input type="hidden" value="<?php echo $video_name; ?>" id="video_name" name="video_name" />
                                        <input type="hidden" value="<?php echo $video_type; ?>" id="video_type" name="video_type"/>
                                        <input type="hidden" value="add" id="action" name="action" />
                                        <input type="hidden" name="image_id" id="image_id" value="" />
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                        <tr>
                                                                <td><?php echo cms_get_lang('media_jz|describe'); ?></td><td><textarea name="summary" style="height:20px;"></textarea></td><td></td>
                                                        </tr>
                                                        <tr style="background:#E5E5E5;">
                                                                <td width="73px;"><?php echo cms_get_lang('select_new_image'); ?></td><td colspan="2"><img  src="" type="hidden" style="width:300px"/><input name="image" id="image" type="file"></td>		
                                                        </tr>
                                                        <tr>
                                                                <td></td><td colspan="2"> <input type="button" value="添加 " onclick="javascript:checkForm('', '', $('#add_form'), 'add');"/></td>
                                                        </tr>
                                                </tbody>
                                        </table>
                                </form>
                        </div>
                        <div class="controlbtns">
                                <?php if ($_GET['url_type'] == '1') { ?>
                                        <div class="controlbtn back"><a href="../vod_pages/nncms_content_vodlist.php?view_type=<?php echo $video_type; ?>&depot_detail_id=<?php echo $_GET['depot_detail_id'] ?>&depot_id=<?php echo $_GET['depot_id'] ?>&view_list_max_num=<?php echo $_GET['view_list_max_num'] ?>" action="add"><?php echo cms_get_lang('back'); ?></a></div>
                                <?php } else if ($_GET['url_type'] == '2') { ?>
                                        <div class="controlbtn back"><a href="../vod_pages/nncms_content_vod_detail.php?view_type=<?php echo $video_type; ?>&nns_id=<?php echo $video_id; ?>"><?php echo cms_get_lang('back'); ?></a></div>
                                <?php } else if ($_GET['url_type'] == '3') { ?>
                                        <div class="controlbtn back"><a href="../live_pages/nncms_content_livelist.php?view_type=<?php echo $video_type; ?>&depot_detail_id=<?php echo $_GET['depot_detail_id'] ?>&depot_id=<?php echo $_GET['depot_id'] ?>&view_list_max_num=<?php echo $_GET['view_list_max_num'] ?>"><?php echo cms_get_lang('back'); ?></a></div>
                                <?php } else if ($_GET['url_type'] == '4') { ?>
                                        <div class="controlbtn back"><a href="../live_pages/nncms_content_live_detail.php?view_type=<?php echo $video_type; ?>&nns_id=<?php echo $video_id; ?>"><?php echo cms_get_lang('back'); ?></a></div>
                                <?php } else { ?>
                                        <div class="controlbtn back"><a href="javascript:void(null);" onclick="javascript:history.go(-1)" action="add"><?php echo cms_get_lang('back'); ?></a></div>
                                <?php }
                                ?>
                                <div style="clear:both;"></div>
                        </div>
                </div>
        </body>
</html>