<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset ($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
include_once LOGIC_DIR	.'image/image.class.php';
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106103");
$checkpri = null;

if ($_GET["nns_org_id"]=="" || $_GET["nns_org_type"]==""){
	$nns_org_id=$_SESSION["nns_org_id"];
	$nns_org_type=$_SESSION["nns_manager_type"];
}else{
	$nns_org_id=$_GET["nns_org_id"];
	$nns_org_type=$_GET["nns_org_type"];
}
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");exit;
}

require_once $nncms_config_path. "nn_cms_config/nn_cms_global.php";
$base_id = 10000;
if (!empty($_COOKIE["page_max_num"])){
	$g_manager_list_max_num=$_COOKIE["page_max_num"];
}

include $nncms_config_path. "nn_logic/stills/stills.class.php";
//获取DC
$dc = nl_get_dc(array(
	'db_policy'=>NL_DB_WRITE,
	'cache_policy'=>NP_KV_CACHE_TYPE_MEMCACHE
)
		);
$dc->open();

//获取视频ID,视频名称
$video_id = $_GET['video_id'];
$video_name = $_GET['video_name'];
$video_type = $_GET['video_type'];
//分页设置
$total_num = nl_stills::count_stills_num($dc,$video_id,$video_type);
//$pages = ceil($total_num/$g_manager_list_max_num);
$pages = ceil($total_num/8);
if (!empty($_GET["page"])) $currentpage=$_GET["page"];
else $currentpage=1;
$since = $currentpage*8-8;
$url="?video_id=$video_id&video_name=$video_name&video_type=$video_type";
include $nncms_db_path. "nns_common/nns_db_pager_class.php";
$pager = new nns_db_pager_class($total_num,8,$currentpage,$url);
//获取剧照list
$stills_list = nl_stills::get_stills_list($dc,$video_id,$video_type,$since,8,$order = NL_ORDER_NUM_DESC);
//获取图片原始大小
foreach($stills_list as $key=>$image){
	$size = getimagesize("$nncms_config_path".$image['nns_image']);
	$stills_list[$key]['size'] = $size['0'].'X'.$size['1'];
	
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/dtree.js"></script>
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/tabs.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
</head>

<body>
<div class="content" style="background:#FFFFFF;border:0px;">
		<div style="background:#CBDEE8;height:30px;line-height:30px;margin-bottom:2px;#6B6B6B">&nbsp;<b><?php echo cms_get_lang('media_jzgl');?></b></div>
		<div class="content_table formtable" style="margin-top:10px;border:0px;margin-left:17px;">
			<table width="100%"  border="0" cellspacing="0" cellpadding="0">	
			<?php if(is_array($stills_list)){?>
				<tr>
					<td>
					<?php foreach($stills_list as $image){?>
						<div class="img_content" style="">
							<div class='img_view' style="width:82px;height:100px;">
								<?php if(!empty($image["nns_image"])){?>
								<a style='padding:0px;background:none;' href='<?php echo nl_image::get_manager_image_url($image["nns_image"]);?>' target='_blank'>
										<alt alt="<?php echo pub_func_get_stills_image_alt($image); ?>">
										<img src='<?php echo nl_image::get_manager_image_url($image["nns_image"]);?>' style='border:none;height:78px;width:80px;' />
										</alt>
								</a>
								<?php }?>
								<h5 class='resolution'><center><?php echo $image['size'];?></center></h5>
							</div>
						</div>
					<?php }?>
					</td>
				</tr>
			<?php } ?>
			</table>
		</div>
		<?php echo $pager->nav_3();?>
</div>
</body>
</html>