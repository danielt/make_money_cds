<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

 //加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$nns_id=$_GET["nns_id"];
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"103103");
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");



 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");

 	include $nncms_db_path. "nns_partner/nns_db_partner_class.php";
	$partner_inst=new nns_db_partner_class();
	if (!empty($nns_id)){
		$partner_info=$partner_inst->nns_db_partner_info($nns_id);
	}else{
		echo "<script>alert('".$partner_info["reason"] ."');self.location='nncms_content_rolelist.php';</script>";
	}
	if ($role_info["ret"]!=0){
		echo "<script>alert('".$partner_info["reason"] ."');self.location='nncms_content_rolelist.php';</script>";
	}
	$partner_inst=null;
	$edit_data=$partner_info["data"][0];
//	$edit_pri=$edit_data["nns_pri_id"];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/radiolist.js"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript">

</script>
</head>

<body>



<div class="content">
	<div class="content_position"><?php echo cms_get_lang('xtgl_hzgl');?> > <?php echo cms_get_lang('partner_hzhbxx');?></div>
    <div class="content_table">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
              <tr>
                <td class="rightstyle"><?php echo cms_get_lang('partner_hzhbmc');?>:</td>
                <td><?php echo $edit_data["nns_name"];?></td>

      		</tr>
            <tr>
                <td class="rightstyle"><?php echo cms_get_lang('lxr');?>:</td>
                <td><?php echo $edit_data["nns_contact"];?>
                </td>
          </tr>
          <tr>
                <td class="rightstyle"><?php echo cms_get_lang('lxdh');?>:</td>
                <td><?php echo $edit_data["nns_telephone"];?>
                </td>
          </tr>
          <tr>
                <td class="rightstyle"><?php echo cms_get_lang('email');?>:</td>
                <td><?php echo $edit_data["nns_email"];?>
                </td>
          </tr>
          <tr>
                <td class="rightstyle"><?php echo cms_get_lang('lxdz');?>:</td>
                <td><?php echo $edit_data["nns_addr"];?>
                </td>
          </tr>
          <tr>
                <td class="rightstyle"><?php echo cms_get_lang('ms');?>:</td>
                <td><?php echo $edit_data["nns_desc"];?>
                </td>
          </tr>

          </tbody>
        </table>
    </div>
    <div class="controlbtns">
        <div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        <div class="controlbtn edit">
<a action="edit" href="nncms_content_partner_addedit.php?action=edit&nns_id=<?php echo $_GET['nns_id']; ?>">修改</a></div>
        <div style="clear:both;"></div>
    </div>

</div>


</body>
</html>
