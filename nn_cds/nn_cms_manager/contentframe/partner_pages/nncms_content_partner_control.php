<?php
/*
 * Created on 2012-3-2
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 @header("Content-Type:text/html;charset=utf-8");
 include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
require_once $nncms_db_path. "nns_log".DIRECTORY_SEPARATOR . "nns_db_op_log_class.php";
$log_inst=new nns_db_op_log_class();

 $action=$_POST["action"];
 $nns_id=$_POST["nns_id"];
 $nns_partner_name=$_POST["nns_partner_name"];
 $nns_partner_contact=$_POST["nns_partner_contact"];
 $nns_partner_phone=$_POST["nns_partner_phone"];
 $nns_partner_email=$_POST["nns_partner_email"];
 $nns_partner_desc=$_POST["nns_partner_desc"];
 $nns_partner_addr=$_POST["nns_partner_addr"];

 //加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
if (!empty($action)){
	switch($action){
		case "edit":
		$action_str=cms_get_lang('edit|partner_hzhbxx');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"103101");
		break;
		case "add":
		$action_str=cms_get_lang('add|partner_hzhbxx');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"103100");
		break;
		case "delete":
		$action_str=cms_get_lang('delete|partner_hzhbxx');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"103102");
		break;
		default:
		break;
	}
}
$checkpri=null;
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
}else{

	include $nncms_db_path. "nns_partner/nns_db_partner_class.php";
	$partner_inst=new nns_db_partner_class();
	switch($action){
		case "edit":
		$result=$partner_inst->nns_db_partner_modify($nns_id,$nns_partner_name,0,$nns_partner_desc,$nns_partner_contact,$nns_partner_phone,$nns_partner_email,$nns_partner_addr);
		if ($result["ret"]!=0){
			echo "<script>alert('". $action_str. cms_get_lang('fault'). "');</script>";
		}else{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$nns_partner_name,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			echo "<script>alert('". $action_str. cms_get_lang('success'). "');</script>";
		}
		break;
		case "add":
		$result=$partner_inst->nns_db_partner_add($nns_partner_name,0,$nns_partner_desc,$nns_partner_contact,$nns_partner_phone,$nns_partner_email,$nns_partner_addr);
		if ($result["ret"]!=0){
			echo "<script>alert('". $action_str. cms_get_lang('fault'). "');</script>";
		}else{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$nns_partner_name,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			echo "<script>alert('". $action_str. cms_get_lang('success'). "');</script>";
		}
		break;
		case "delete":
		$delete_bool=true;
	//	$role_inst->
			$nns_ids=explode(",",$nns_id);
			$nns_names=$log_inst->nns_db_get_partner_name_arr($nns_id);
			$num=0;
			foreach($nns_ids as $nnsid){
				if (!empty($nnsid)){
					$num++;
					$resultarr=$partner_inst->nns_db_partner_delete($nnsid);
					if ($resultarr["ret"]!=0 && $num==1){
						$delete_bool=false;

						break;
					}
				}
			}
		if (!$delete_bool){
			echo "<script>alert('". $action_str. cms_get_lang('fault'). "');</script>";
		}else{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$nns_names,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			echo "<script>alert('". $action_str. cms_get_lang('success'). "');</script>";
		}
		break;
		default:

		break;
	}
	$partner_inst=null;
	//var_dump($result);
	$log_inst=null;
	echo "<script>self.location='nncms_content_partnerlist.php';</script>";
}
?>
