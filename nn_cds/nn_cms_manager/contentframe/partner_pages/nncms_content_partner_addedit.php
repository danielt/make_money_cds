<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$action=$_GET["action"];
$nns_id=$_GET["nns_id"];
$pri_bool=false;
if (!empty($action)){
	switch($action){
	case "edit":
		$action_str=cms_get_lang('save');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"103101");
		break;
	case "add":
		$action_str=cms_get_lang('add');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"103100");
		break;
	default:
		break;
	}
}
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");

//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");

if ($action=="edit"){
	include $nncms_db_path. "nns_partner/nns_db_partner_class.php";
	$partner_inst=new nns_db_partner_class();
	if (!empty($nns_id)){
		$partner_info=$partner_inst->nns_db_partner_info($nns_id);
	}else{
		echo "<script>alert('".$partner_info["reason"] ."');self.location='nncms_content_rolelist.php';</script>";
	}
	if ($role_info["ret"]!=0){
		echo "<script>alert('".$partner_info["reason"] ."');self.location='nncms_content_rolelist.php';</script>";
	}
	$partner_inst=null;
	$edit_data=$partner_info["data"][0];
	//	$edit_pri=$edit_data["nns_pri_id"];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/radiolist.js"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript">

</script>
</head>

<body>



<div class="content">
	<div class="content_position"><?php echo cms_get_lang('xtgl_hzgl');?> > <?php echo cms_get_lang('xtgl_hzgl')?></div>
	<form id="add_form" action="nncms_content_partner_control.php" method="post">
	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
	<?php if ($action=="edit"){?>
	<input name="nns_id" id="nns_id" type="hidden" value="<?php echo $nns_id;?>"/>
	<?php } ?>
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			  <tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('partner_hzhbmc');?>:</td>
				<td><input name="nns_partner_name" id="nns_partner_name" type="text" rule="noempty" value="<?php if($action=="edit"){echo $edit_data["nns_name"];}?>" /></td>

			</tr>
			<tr>
				<td class="rightstyle"><?php echo cms_get_lang('lxr')?>:</td>
				<td><input name="nns_partner_contact" id="nns_partner_contact" type="text" value="<?php echo $edit_data["nns_contact"];?>"/>
				</td>
		  </tr>
		  <tr>
				<td class="rightstyle"><?php echo cms_get_lang('lxdh');?>:</td>
				<td><input name="nns_partner_phone" id="nns_partner_phone" rule="phone" type="text" value="<?php echo $edit_data["nns_telephone"];?>"/>
				</td>
		  </tr>
		  <tr>
				<td class="rightstyle"><?php echo cms_get_lang('email');?>:</td>
				<td><input name="nns_partner_email" id="nns_partner_email" rule="email" type="text" value="<?php echo $edit_data["nns_email"];?>"/>
				</td>
		  </tr>
		  <tr>
				<td class="rightstyle"><?php echo cms_get_lang('lxdz');?>:</td>
				<td><input name="nns_partner_addr" id="nns_partner_addr" type="text" value="<?php echo $edit_data["nns_addr"];?>"/>
				</td>
		  </tr>
		  <tr>
				<td class="rightstyle"><?php echo cms_get_lang('ms');?>:</td>
				<td><textarea name="nns_partner_desc" id="nns_partner_desc" cols="" rows=""><?php echo $edit_data["nns_desc"];?></textarea>
				</td>
		  </tr>

		  </tbody>
		</table>
	</div>
	</form>
	<div class="controlbtns">
		<div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','<?php echo cms_get_lang('edit'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>


</body>
</html>
