<?php
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
include $nncms_config_path . 'nn_cms_manager/controls/nncms_controls_public_function.php';
require_once $nncms_db_path . 'nns_log' . DIRECTORY_SEPARATOR . 'nns_db_op_log_class.php';
$log_inst = new nns_db_op_log_class();

$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE, 
		'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$dc->open();
$params = $_REQUEST;

$action = $params['action'];
unset($params['action']);
$log_params = (!empty($params) && is_array($params)) ? json_encode($params) : '';
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);


/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/producer/producer.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/video/vod.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/live/live.class.php';
session_start();
switch ($action)
{
    case 'query_producer':
        $result_producer = nl_producer::query_by_cp_id($dc, $_REQUEST['nns_cp_id']);
        echo json_encode($result_producer);die;
        break;
	case "edit":
		$nns_id = $params['nns_id'];
		unset($params['nns_id']);
		$result_model = nl_cp::query_by_id($dc, $params['nns_cp_id']);
		if($result_model['ret'] !=0)
		{
		    echo "<script>alert('" . $result_model['reason'] . "');history.go(-1);</script>";
		    break;
		}
		if(!isset($result_model['data_info']) || !is_array($result_model['data_info']) || empty($result_model['data_info']))
		{
		    echo "<script>alert('cp已经被删除');history.go(-1);</script>";
		    break;
		}
		$result_unique = nl_producer::query_unique($dc, "where nns_cp_id='{$params['nns_cp_id']}' and nns_import_source='{$params['nns_import_source']}' and nns_domain='{$params['nns_domain']}' and nns_type='{$params['nns_type']}' and nns_id !='{$nns_id}'");
		if($result_unique['ret'] !=0)
		{
		    echo "<script>alert('" . $result_unique['reason'] . "');history.go(-1);</script>";
		    break;
		}
		if(isset($result_unique['data_info'][0]['count']) && $result_unique['data_info'][0]['count'] >0)
		{
		    echo "<script>alert('数据已经存在,不允许在添加');history.go(-1);</script>";
		    break;
		}
		$result = nl_producer::edit($dc, $params, $nns_id);
		if ($result['ret'])
		{
			echo "<script>alert('" . $result['reason'] . "');history.go(-1);</script>";
		}
		else
		{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "修改:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
			echo "<script>alert('修改成功');</script>";
		}
		break;
	case "add":
	    $params['nns_id'] = np_guid_rand();
	    $result_model = nl_cp::query_by_id($dc, $params['nns_cp_id']);
	    if($result_model['ret'] !=0)
	    {
	        echo "<script>alert('" . $result_model['reason'] . "');history.go(-1);</script>";
	        break;
	    }
	    if(!isset($result_model['data_info']) || !is_array($result_model['data_info']) || empty($result_model['data_info']))
	    {
	        echo "<script>alert('CP已经被删除');history.go(-1);</script>";
	        break;
	    }
	    $result_unique = nl_producer::query_unique($dc, "where nns_cp_id='{$params['nns_cp_id']}' and nns_import_source='{$params['nns_import_source']}' and nns_domain='{$params['nns_domain']}' and nns_type='{$params['nns_type']}'");
	    if($result_unique['ret'] !=0)
	    {
	        echo "<script>alert('" . $result_unique['reason'] . "');history.go(-1);</script>";
	        break;
	    }
	    if(isset($result_unique['data_info'][0]['count']) && $result_unique['data_info'][0]['count'] >0)
	    {
	        echo "<script>alert('数据已经存在,不允许在添加');history.go(-1);</script>";
	        break;
	    }
		$result = nl_producer::add($dc, $params);
		if ($result['ret'])
		{
			echo "<script>alert('" . $result['reason'] . "');</script>";
		}
		else
		{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "添加:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
			echo "<script>alert('添加成功');</script>";
		}
		break;
	case "delete":
		$nns_ids = explode(",", $params['nns_id']);
		$arr_temp_del = array();
		$str_other=$str_desc=null;
		foreach ($nns_ids as $nnsid)
		{
			if (strlen($nnsid) < 1)
			{
				continue;
			}
			$result_producer = nl_producer::query_by_id($dc, $nnsid);
			if($result_producer['ret'] !=0)
			{
			    $str_desc = $result_producer['reason'];
			    break;
			}
			if(!isset($result_producer['data_info']) || empty($result_producer['data_info']) || !is_array($result_producer['data_info']))
			{
			    continue;
			}
			$result_live = nl_live::check_cp_producer_exsist($dc,$result_producer['data_info']['nns_cp_id'],$result_producer['data_info']['nns_import_source']);
			if($result_live['ret'] !=0)
			{
			    $str_desc = $result_live['reason'];
			    break;
			}
			if(isset($result_live['data_info']) && is_array($result_live['data_info']) && !empty($result_live['data_info']))
			{
			    $str_other.="[直播,cp为{$result_producer['data_info']['nns_cp_id']},提供商为{$result_producer['data_info']['nns_import_source']}]";
			    continue;
			}
			$result_vod = nl_vod::check_cp_producer_exsist($dc,$result_producer['data_info']['nns_cp_id'],$result_producer['data_info']['nns_import_source']);
			if($result_vod['ret'] !=0)
			{
			    $str_desc = $result_vod['reason'];
			    break;
			}
			if(isset($result_vod['data_info']) && is_array($result_vod['data_info']) && !empty($result_vod['data_info']))
			{
			    $str_other.="[点播,cp为{$result_producer['data_info']['nns_cp_id']},提供商为{$result_producer['data_info']['nns_import_source']}]";
			    continue;
			}
			$result_del=nl_producer::delete($dc,$nnsid);
			if($result_del['ret'] != 0)
			{
				echo "<script>alert('".$result_del['reason']."');history.go(-1);</script>";die;
			}
		}
		$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "删除:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
		$str_dec_last = empty($str_desc) ? "删除成功" : $str_desc;
		$str_dec_last = empty($str_other) ? $str_dec_last : $str_dec_last."其中".$str_other.'存在数据，不允许删除';
		echo "<script>alert('".$str_dec_last."');</script>";
		break;
	default:
	    echo "<script>alert('没有此方式');</script>";
		break;
}
$partner_inst = null;
$log_inst = null;
echo "<script>self.location='".$base_file_name_base."_list.php';</script>";
