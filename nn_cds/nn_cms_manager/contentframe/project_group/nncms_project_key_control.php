<?php
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
include $nncms_config_path . 'nn_cms_manager/controls/nncms_controls_public_function.php';
require_once $nncms_db_path . 'nns_log' . DIRECTORY_SEPARATOR . 'nns_db_op_log_class.php';
$log_inst = new nns_db_op_log_class();

$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE, 
		'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$dc->open();
$params = $_REQUEST;

$action = $params['action'];
unset($params['action']);
$log_params = (!empty($params) && is_array($params)) ? json_encode($params) : '';
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);


/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/project_group/project_key.class.php';
session_start();
switch ($action)
{
	case "edit":
		$nns_id = $params['nns_id'];//key GUID
		unset($params['nns_id']);
		unset($params['nns_key']);
		//组装key-value
		if((empty($params['value_keys']) || empty($params['key_values'])) && $params['nns_key_type'] == '1')//如果为选择框，没有配置选择框内容，返回错误
		{
			echo "<script>alert('修改失败,未配置选择框内容');history.go(-1);</script>";die;
		}
		$key_value = array();
		foreach ($params['value_keys'] as $k=>$v)
		{
			if(empty($v) && $v != '0')
			{
				continue;
			}
			if(empty($params['key_values'][$k]) && $params['key_values'][$k] != '0')
			{
				continue;
			}
			$key_value[$v] = $params['key_values'][$k];
		}
		
		$params['nns_key_value'] = addslashes(json_encode($key_value));
		$result = nl_project_key::edit($dc, $params, $nns_id);
		if ($result['ret'])
		{
			echo "<script>alert('" . $result['reason'] . "');history.go(-1);</script>";
		}
		else
		{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "修改:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
			echo "<script>alert('修改成功');</script>";
		}
		break;
	case "add":
	    $result_unique = nl_project_key::query_unique($dc, "where nns_key='{$params['nns_key']}'");
	    if($result_unique['ret'] != 0)
	    {
	        echo "<script>alert('" . $result_unique['reason'] . "');history.go(-1);</script>";
	        break;
	    }
	    if(isset($result_unique['data_info'][0]['count']) && $result_unique['data_info'][0]['count'] >0)
	    {
	        echo "<script>alert('数据已经存在,不允许在添加');history.go(-1);</script>";
	        break;
	    }
	    //组装key-value
	    if((empty($params['value_keys']) || empty($params['key_values'])) && $params['nns_key_type'] == '1')//如果为选择框，没有配置选择框内容，返回错误
	    {
	    	echo "<script>alert('修改失败,未配置选择框内容');history.go(-1);</script>";
	    }
	    $key_value = array();
	    foreach ($params['value_keys'] as $k=>$v)
	    {
	    	if(empty($v) && $v != '0')
	    	{
	    		continue;
	    	}
	    	if(empty($params['key_values'][$k]) && $params['key_values'][$k] != '0')
	    	{
	    		continue;
	    	}
	    	$key_value[$v] = $params['key_values'][$k];
	    }
	    $params['nns_key_value'] = addslashes(json_encode($key_value));
	    
		$result = nl_project_key::add($dc, $params);
		if ($result['ret'])
		{
			echo "<script>alert('" . $result['reason'] . "');</script>";
		}
		else
		{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "添加:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
			echo "<script>alert('添加成功');</script>";
		}
		break;
	case "delete":
		$nns_ids = explode(",", $params['nns_id']);
		$arr_temp_del = array();
		foreach ($nns_ids as $nnsid)
		{
			if (strlen($nnsid) < 1)
			{
				continue;
			}
			$result_del=nl_project_key::delete($dc,$nnsid);
			if($result_del['ret'] != 0)
			{
				echo "<script>alert('".$result_del['reason']."');history.go(-1);</script>";die;
			}
		}
		$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "删除:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
		echo "<script>alert('删除成功');</script>";
		break;
	default:
	    echo "<script>alert('没有此方式');</script>";
		break;
}
$partner_inst = null;
$log_inst = null;
echo "<script>self.location='".$base_file_name_base."_list.php?model_id={$params['nns_model_id']}';</script>";
