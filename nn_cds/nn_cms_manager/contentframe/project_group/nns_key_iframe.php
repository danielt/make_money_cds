<?php
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) 
{
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool) 
{
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -5);
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
		"db_policy" => NL_DB_WRITE,
		"cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));



/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/project_group/project_key.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/project_group/project_model.class.php';

$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 12;
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) 
{
	$page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit_array = array(
		'page_size'=>$page_size,
		'page_num'=> $page_num,
);
$filter_array['where']['nns_model_id'] = $_REQUEST['model_id'];
if($_REQUEST['operation'] == 'search')
{
	if (isset($_REQUEST['nns_id']) && (strlen($_REQUEST['nns_id']) >0))
	{
	    $filter_array['where']['nns_id'] = $_REQUEST['nns_id'];
	}
// 	if (isset($_REQUEST['nns_is_more']) && (strlen($_REQUEST['nns_is_more']) >0))
// 	{
// 		$filter_array['where']['nns_is_more'] = $_REQUEST['nns_is_more'];
// 	}
	if (isset($_REQUEST['nns_key_type']) && (strlen($_REQUEST['nns_key_type']) >0))
	{
		$filter_array['where']['nns_key_type'] = $_REQUEST['nns_key_type'];
	}
	if (isset($_REQUEST['nns_key']) && (strlen($_REQUEST['nns_key']) >0))
	{
	    $filter_array['like']['nns_key'] = $_REQUEST['nns_key'];
	}
	if (isset($_REQUEST['nns_name']) && (strlen($_REQUEST['nns_name']) >0))
	{
	    $filter_array['like']['nns_name'] = $_REQUEST['nns_name'];
	}
}
//获取分组已经添加的键值
$group_data = nl_project_group::query_by_id($dc, $_GET['group_id']);
if($group_data['ret'] != 0)
{
	echo '<script>alert("'.$group_data['reason'].'");history.go(-1);</script>';die;
}
$filter_array['notin']['nns_id'] = explode(",", $group_data['data_info']['nns_key_id']);

$result_data = nl_project_key::query($dc,$filter_array,$limit_array);

if($_REQUEST['operation'] == 'search')
{
	if($result_data['ret'] != 0)
	{
		echo '<script>alert("'.$result_data['reason'].'");history.go(-1);</script>';die;
	}
}

$result_data_model = nl_project_model::query_by_id($dc, $_REQUEST['model_id']);
if($result_data_model['ret'] != 0)
{
	echo '<script>alert("'.$result_data_model['reason'].'");history.go(-1);</script>';die;
}
$model_name = $result_data_model['data_info']['nns_name'];
unset($result_data_model);
$vod_total_num = isset($result_data['page_info']['total_count']) ? $result_data['page_info']['total_count'] : 0;
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) 
{
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;

$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);

$key_type_arr = nl_project_key::$key_type_arr;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link href="../../css/public.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
        <script language="javascript">
            $(document).ready(function() {
                $(".but").click(function() {
                    
                    window.parent.close_select();
                });
                $(".closebtn").click(function() {
                    window.parent.close_select();
                });
                $('.sub').click(function() {
                    var par = $("input[type='radio']:checked").val();
                    if (!par) {
                        alert('请先选择一条数据!');
                    }
                    else
                    {
                        window.parent.set_key(par);
                    }
                });

            });
            function refresh_vod_page() 
            {
				var num = $("#nns_list_max_num").val();
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
        </script>
        </head>
        <body>
        	<div class="content" style="padding:0px; margin:0px;height:460px;overflow-y: scroll;border:3px solid #C0C0C0;">
        		<!-- 栏目导航描述div -->
				<div style="border-bottom:1px solid red;font-weight:bold;height:30px;line-height:30px;">
					<div style="float:left;">模块键值列表</div>
					<div style="float:right;" class="closebtn"><img src="../../images/topicon_08-topicon.png" /></div>
					<div style="clear: both;"></div>
				</div>
			
				<!-- 表单查询div -->
				<div class="content_table">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<form action="<?php echo $base_file_name;?>" method="get">
						    <input type="hidden" name="view_list_max_num" id="view_list_max_num" value="<?php echo $g_manager_list_max_num; ?>"/>
							<input type="hidden" name="model_id" value="<?php echo $_GET['model_id'];?>" />
							<input type="hidden" name="group_id" value="<?php echo $_GET['group_id'];?>" />
							<tbody>
								<tr>
									<td>
										GUID:
											<input type="text" name="nns_id" id="nns_id" value="<?php echo $_GET['nns_id']; ?>" style="width:80px;">
										&nbsp;&nbsp;键值:
	                                        <input type="text" name="nns_key" id="nns_key" value="<?php echo $_GET['nns_key']; ?>" style="width:80px;">
	                                    &nbsp;&nbsp;键名称:
	                                        <input type="text" name="nns_name" id="nns_name" value="<?php echo $_GET['nns_name']; ?>" style="width:80px;">                                    
	                                    &nbsp;&nbsp;键值输入类型:
	                                	    <select name="nns_key_type" style="width:80px;"> 	 
	                                	    	<option value="">--请选择--</option>
	                                	    	<?php foreach ($key_type_arr as $key=>$key_type){?>
	                                	    	<option value="<?php echo $key;?>" <?php if (strlen($_GET['nns_key_type']) > 0 && $_GET['nns_key_type'] == $key){echo "selected='selected'";}?>><?php echo $key_type;?></option>
	                                	    	<?php }?>
	                                	    </select>
										&nbsp;&nbsp;<input type="hidden" name="operation" value="search" /><input type="submit" value="<?php echo cms_get_lang('search'); ?>"  />										
									</td>
								</tr>
							</tbody>
						</form>	
					</table>
				</div>
				<div class="content_table formtable">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th>序号</th>
							<th>GUID</th>
							<th>键值</th>
							<th>键名称</th>
							<th>输入类型</th>
							<th>描述</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(is_array($result_data['data_info']) && !empty($result_data['data_info']))
						{
							$num = ($page_num - 1) * $page_size;
							foreach ($result_data['data_info'] as $item) 
							{
								$num++;
						?>
						      <tr>
						          <td><input name="input" type="radio" value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
						          <td><?php echo $item['nns_id']; ?></td>           
                                  <td><font color="green"><?php echo $item['nns_key']; ?></font></td>
						          <td><?php echo $item['nns_name']; ?></td>
						          <td><?php echo $key_type_arr[$item['nns_key_type']];?></td>
							      <td><?php echo $item['nns_desc']; ?></td>
                        </tr>
						<?php
						     }
						}
						$least_num = $g_manager_list_max_num - count($result_data['data_info']);
						for ($i = 0; $i < $least_num; $i++) 
						{
						?>
						  <tr>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						  </tr>
						<?php 
						}
						?>
					</tbody>
				</table>
			</div>
		    <!-- 分页DIV -->
		    <div class="pagecontrol">
		             共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                <?php if ($currentpage > 1) { ?>
                        <a href="<?php $base_file_name; ?>?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php $base_file_name; ?>?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } else { ?>
                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } if ($currentpage < $vod_pages) { ?>
                        <a href="<?php $base_file_name; ?>?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php $base_file_name; ?>?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } else { ?>
                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } ?>
    
                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                <a href="javascript:go_page_num('<?php $base_file_name; ?>?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                <input name="nns_list_max_num" id="nns_list_max_num" type="text" value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>" onclick="refresh_vod_page();"/>&nbsp;&nbsp;
            </div>	
            <div class="content_table" style="text-align: center;background:#dfdfdf;height:44px;line-height:44px;">
           		<input type="submit" class='sub' value='确定'/>
                <input type="button" class='but' value="取消" />
            </div>                                                              
			</div>	
        </body>
</html>