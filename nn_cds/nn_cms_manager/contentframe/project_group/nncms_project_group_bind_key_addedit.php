<?php
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$action=$_GET["action"];
$group_id=$_GET["group_id"];
$pri_bool=false;

include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';

$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);

/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/project_group/project_model.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/project_group/project_key.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/project_group/project_group.class.php';
$dc = nl_get_dc(array (
    "db_policy" => NL_DB_WRITE,
    "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));
if(isset($_GET['ajax']))
{
	$key_data = nl_project_key::query_by_id($dc, $_POST['key_id']);
	if($key_data['ret'] != 0 || empty($key_data['data_info']))
	{
		$return_arr = array('ret' => 0);
		echo json_encode($return_arr);die;
	}
	else 
	{
		$key_data['data_info']['ret'] = 1;
		$key_data['data_info']['nns_key_value'] = json_decode($key_data['data_info']['nns_key_value'],true);
		echo json_encode($key_data['data_info']);die;
	}
}

if (!empty($action))
{
	switch($action)
	{
		case "edit":
			$action_str=cms_get_lang('save');
			$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136002");
			break;
		case "add":
			$action_str=cms_get_lang('add');
			$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136003");
			break;
		default:
			break;
	}
}
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");

if ($action=="edit")
{
	$edit_data = nl_project_group::query_by_id($dc,$group_id);	
	if($edit_data['ret'] != 0 )
	{
		echo "<script>alert('". $action_str. $edit_data['reason']. "');</script>";
		echo "<script>self.location='".$base_file_name_base."_list.php;</script>";
	}
	$edit_data = $edit_data['data_info'];
	$key_data = nl_project_key::query_by_id($dc, $_GET['nns_key_id']);
	if($key_data['ret'] != 0)
	{
		echo "<script>alert('". $action_str. $edit_data['reason']. "');</script>";
		echo "<script>self.location='".$base_file_name_base."_list.php;</script>";
	}
	$key_data = $key_data['data_info'];
	
	$value = json_decode($edit_data['nns_value'],true);
	$key_data['nns_value'] = $value[$key_data['nns_key']];
	$key_value = json_decode($key_data['nns_key_value']);//选择框的值
	
}
else 
{
	$model_data = nl_project_model::query_all($dc);
	if($model_data['ret'] != 0)
	{
		echo "<script>alert('". $action_str. $model_data['reason']. "');</script>";
		echo "<script>self.location='".$base_file_name_base."_list.php;</script>";
	}
	$model_data = $model_data['data_info'];
	if(empty($model_data))
	{
		echo "<script>alert('先配置模块与键值');</script>";
		echo "<script>self.location='".$base_file_name_base."_list.php;</script>";
	}
	$key_data = nl_project_key::query_all($dc);
	if($key_data['ret'] != 0)
	{
		echo "<script>alert('". $action_str. $key_data['reason']. "');</script>";
		echo "<script>self.location='".$base_file_name_base."_list.php;</script>";
	}
	$key_data = $key_data['data_info'];	
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
    </head>
    <body>
	    <div class="selectbox" style="display:none;">
			<iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
		</div>
        <div class="content">
        	<div class="content_position">分组键值列表 > 添加/修改</div>
        	<form id="add_form" action="<?php echo $base_file_name_base;?>_control.php" method="post">
            	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
            	<input name="nns_id" id="nns_id" type="hidden"  value="<?php echo $group_id;?>" />
            	<div class="content_table" style="border-bottom:none;">
            		<table width="100%" border="0" cellspacing="0" cellpadding="0" id="ex_data">
            		  <tbody class="tr_tbody">
            		  <?php if($action == 'edit'){?>
            				<input type="hidden" name="nns_is_more" value="<?php echo $key_data['nns_is_more'];?>" />
            				<input type="hidden" name="nns_key_id" value="<?php echo $_GET['nns_key_id'];?>" />
                			<tr>
	            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>键值:</td>
	            				<td><input type="text" disabled value="<?php echo $key_data["nns_key"];?>" /></td>
	            			</tr>
	            			<tr>
	            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>键值名称:</td>
	            				<td><input type="text"  value="<?php echo $key_data["nns_name"];?>" disabled /></td>
	            			</tr>
	            			<tr>
	            				<td class="rightstyle">键值描述:</td>
	            				<td><?php echo $key_data["nns_desc"];?></td>
	            			</tr>	            			
	            				<?php 
	            				//支持多个
            					if($key_data['nns_is_more'] == '1')
            					{           						
            						if($key_data['nns_key_type'] == '0')
            						{    
            							if(!empty($key_data['nns_value']))
            							{
            								$num = 0;
	            							foreach ($key_data['nns_value'] as $k_val)
	            							{   
	            								$num++;
	            				?>	
		            							<!-- 文本输入框 -->
		            							<tr>
					            					<td class="rightstyle">键值内容-<?php echo $num?>:</td>
					            					<td class="tr_td">
				            							<input type="text" name="nns_value[]" value="<?php echo $k_val;?>" />
				            							<input type="button" data-change="<?php echo $num ?>" value="删除" class="button_del" />	
						            				</td>			
			            						</tr>
	            				<?php 
            								}
            							}
            							else
            							{
            					?>
            								<!-- 文本输入框 -->
            								<tr>
					            				<td class="rightstyle">键值内容-1:</td>
					            				<td class="tr_td">
	            									<input type="text" name="nns_value[]" value="" />
					            					<input type="button" data-change="<?php echo $num ?>" value="删除" class="button_del" />	
            									</td>
            								</tr>
            					<?php 
            							}
            						}
	            					else 
	            					{
            					?>
            							<tr>
				            				<td class="rightstyle">键值内容:</td>
				            				<td>
            					<?php 
	            						foreach ($key_value as $k=>$val)
	            						{
            					?>
		            							<!-- 选择框 -->
		            							<input type="checkbox" name="nns_value[]" value="<?php echo $k;?>" <?php if(in_array($k,$key_data['nns_value'])) echo "checked='checked'";?> /><?php echo $val;?>
            					<?php	
	            						}	
	            				?>
	            							</td>
	            						</tr>
	            				<?php             						
            						}
            					} 
            					else //只有一个
            					{
            					?>
            					<tr>
		            				<td class="rightstyle">键值内容:</td>
		            				<td>
            					<?php
            						if($key_data['nns_key_type'] == '0')
            						{
	            				?>
		            					<!-- 文本输入框 -->
		            					<input type="text" name="nns_value[]" id="nns_value[]" value="<?php echo $key_data['nns_value'];?>" />
	            				<?php
            						}
            						else 
            						{
            							foreach ($key_value as $k=>$val)
            							{
            					?>
	            							<!-- 选择框 -->
		            						<input type="radio" name="nns_value[]" value="<?php echo $k;?>" <?php if($key_data['nns_value'] == $k) echo "checked='checked'";?> /><?php echo $val;?>
								<?php
            							}
            						}
            					?>
            						</td>
            					</tr>
            					<?php 
	            				}
	            				if($key_data['nns_is_more'] == '1' && $key_data['nns_key_type'] == '0')
	            				{
	            				?>
	            				<tr>
	            					<td class="rightstyle"></td>
	            					<td><a href="#" class="button_add">扩展键值+</a></td>
	            				</tr>
	            				<?php	
	            				}
	            				?> 
	            			<?php if(!empty($key_data['nns_example'])){?>
	            			<tr>
	            				<td class="rightstyle">键值配置样例:</td>
	            				<td><?php echo $key_data['nns_example'];?></td>
	            			</tr>
                		<?php }}else{?>
                			<input type="hidden" name="nns_is_more" class="is_more" value="" />
            				<input type="hidden" name="nns_key_id" class="key_id" value="" />
            				<tr>
            					<td class="rightstyle">通用模块:</td>
            					<td>
            						<select class="select_model">
            							<option value="">--请选择--</option>
            						<?php 
            						foreach ($model_data as $m=>$model)
            						{
            						?>
            							<option value="<?php echo $model['nns_id'];?>"><?php echo $model['nns_name'];?></option>
            						<?php 	
            						}
            						?>
            						</select>
            					</td>
            				</tr>
                			<tr>
	            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>键值:</td>
	            				<td><input type="text" class="key" readonly="readonly" name="nns_key" value="" /><input type="button" onclick='select_key();' value='选择键值' /></td>
	            			</tr>
	            			<tr>
	            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>键值名称:</td>
	            				<td><input type="text" class="key_name" readonly="readonly"  value="" /></td>
	            			</tr>
	            			<tr>
	            				<td class="rightstyle">键值描述:</td>
	            				<td class="desc"></td>
	            			</tr>
	            			<tr style="display:none;">
	            				<td class="rightstyle c_key">键值内容:</td>
	            				<td class="tr_td"></td>
	            			</tr>
            				<tr style="display:none;">
            					<td class="rightstyle"></td>
            					<td><a href="#" class="button_add">扩展键值+</a></td>
            				</tr>
	            			<tr style="display:none;">
	            				<td class="rightstyle" class="example">键值配置样例:</td>
	            				<td class="example_content"></td>
	            			</tr>
                		<?php }?>
            		  </tbody>
            		</table>
            	</div>
        	</form>
        	<div class="controlbtns">
        		<div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','<?php echo cms_get_lang('webkz_qrccxg'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
        		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        		<div style="clear:both;"></div>
        	</div>
        </div>
        <script type="text/javascript">
	        $(document).ready(function() {
				$(".selectbox").hide();				
			});
			$(".button_add").click(function(){
				var obj = $('.tr_tbody .tr_td');
	            var copy = obj.eq(0).parents('tr').clone();
	            var input_length = obj.length;
	            var max_num = 10;
	            if (input_length >= max_num) 
	            {
	                alert('数量超出限制 > ' + max_num);
	                return;
	            }
	            var td_name = "键值内容-" + (input_length + 1) + ":";
	            copy.find('td').eq(0).html(td_name);
	            copy.find('td').eq(1).find("input[type='text']").attr('value','');
	            copy.find('.button_del').attr('data-change', input_length+1);
	            $(this).parents('tr').before(copy.removeClass().show('slow'));
	            tr_order();
	            window.parent.resetFrameHeight();
			})
			//点击删除
            $(".button_del").live("click", function(){
            	var obj = $(this);
                var data = obj.attr('data-change');
                switch (data)
                {
                    case '1':
                    	obj.parent().find("input[type='text']").attr("value","");
                        break;
                    default :
                    	obj.val("").parent().parent().remove();
                        reset_num_after_delete();
                        break;
                }
            })
            function tr_order() 
            {
                var class_name = 'tr_odd';
                $("tr:even").removeClass(class_name);
                $("tr:odd").addClass(class_name);
            }
			//重置数字
            function reset_num_after_delete()
            {
                var obj = $('.tr_tbody .tr_td')
                var len = obj.length;
                if (len < 2)
                {
                    return ;
                }
                for (var i = 0; i < len ; i++)
                {
                    var item = obj.eq(i).parent();
                    item.find('td').eq(0).html('键值内容-'+(i+1)+":");
                    item.find('.button_del').attr('data-change',i+1);
                }
            }
            function select_key()
            {
            	$(".selectbox").attr("src", "");
                var model_id = $(".select_model").attr('value');
                if(model_id == '')
                {
					alert('请先选择通用模块');
                }
                else
                {
					$("#select_frame").attr("src", "nns_key_iframe.php?group_id=<?php echo $_GET['group_id'];?>&model_id="+model_id).css('width', '800px');
	        		$(".selectbox").show();
                }
            }
            function close_select() 
            {
				$(".selectbox").hide();
			}
			function set_key(key_id)
			{
				var url = "../../../nn_cms_manager_v2/admin.php";
	            var data = {'group_id': '<?php echo $_GET['group_id'];?>', 'key_id': key_id};
	            $.post("<?php echo $base_file_name_base;?>_addedit.php?ajax", data, function(result){
	                if(result.ret == 0)
	                {
						alert('键值数据错误!');
	                }
	                else
	                {
						$(".is_more").attr("value",result.nns_is_more);
						$(".key_id").attr("value",result.nns_id);
						$(".key").attr("value",result.nns_key);
						$(".key_name").attr("value",result.nns_name);
						$(".desc").html(result.nns_desc);

						$(".tr_td").empty();
						$(".button_add").parent().parent().hide();
						switch (result.nns_key_type)
						{
							case '0'://文本
								if(result.nns_is_more == '0')//单个
								{
									var html = "<input type='text' name='nns_value[]' value='' />";
									$(".tr_td").append(html);
									$(".tr_td").parent().show();
								}
								else//多个
								{
									var html = "<input type='text' name='nns_value[]' value='' /><input type='button' data-change='1' value='删除' class='button_del' />";
									$(".tr_td").append(html);
									$(".c_key").html("键值内容-1:");
									$(".tr_td").parent().show();
									$(".button_add").parent().parent().show();
								}
								break;
							case '1'://选择框
								if(result.nns_is_more == '0')//单选
								{
									var html = '';
									for(var i in result.nns_key_value)
									{
										html += "<input type='radio' name='nns_value[]' value='"+i+"' />"+result.nns_key_value[i];
									}
									$(".tr_td").append(html);
									$(".tr_td").parent().show();
								}
								else//多选
								{
									var html = '';
									for(var i in result.nns_key_value)
									{
										html += "<input type='checkbox' name='nns_value[]' value='"+i+"' />"+result.nns_key_value[i];
									}
									$(".tr_td").append(html);
									$(".tr_td").parent().show();
								}
								break;
						}
						if(result.nns_example != '')
						{
							$(".example_content").html(result.nns_example);
							$(".example_content").parent().show();
						}
						close_select();
	                }
	            }, 'json');
			}
        </script>
    </body>
</html>
