<?php
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
include $nncms_config_path . 'nn_cms_manager/controls/nncms_controls_public_function.php';
require_once $nncms_db_path . 'nns_log' . DIRECTORY_SEPARATOR . 'nns_db_op_log_class.php';
$log_inst = new nns_db_op_log_class();

$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE, 
		'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$dc->open();
$params = $_REQUEST;
/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/project_group/project_model.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/project_group/project_key.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/project_group/project_group.class.php';

$action = $params['action'];
unset($params['action']);
$log_params = (!empty($params) && is_array($params)) ? json_encode($params) : '';
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);

$nns_id = $params['nns_id'];
unset($params['nns_id']);
$key_id = $params['nns_key_id'];
unset($params['nns_key_id']);

	
session_start();
switch ($action)
{
	case "edit":
		$key_data = nl_project_key::query_by_id($dc, $key_id);
		if($key_data['ret'] != 0 || empty($key_data['data_info']))
		{
			echo "<script>alert('修改失败');</script>";
			break;
		}
		$key = $key_data['data_info']['nns_key'];
		unset($params['$key']);
		//获取分组配置内容
		$p_data = nl_project_group::query_by_id($dc, $nns_id);
		if($p_data['ret'] != 0 || empty($p_data['data_info']))
		{
			echo "<script>alert('修改失败');</script>";
		}
		else
		{
			$group_value = json_decode($p_data['data_info']['nns_value'],true);
			if($params['nns_is_more'] == '0')//单个
			{
				$group_value[$key] = $params['nns_value'][0];
				
				$params['nns_value'] = addslashes(json_encode($group_value));
			}
			else 
			{
				$group_value[$key] = del_empty_param($params['nns_value']);
				$params['nns_value'] = addslashes(json_encode($group_value));
			}
			unset($params['nns_is_more']);
			$result = nl_project_group::edit($dc, $params, $nns_id);
			if ($result['ret'])
			{
				echo "<script>alert('" . $result['reason'] . "');</script>";
			}
			else
			{
				$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "修改:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
				echo "<script>alert('修改成功');</script>";
			}
		}
		break;
	case "add":
		$key_data = nl_project_key::query_by_id($dc, $key_id);
		if($key_data['ret'] != 0 || empty($key_data['data_info']))
		{
			echo "<script>alert('添加失败');</script>";
			break;
		}
		$key = $key_data['data_info']['nns_key'];
		unset($params['$key']);
		//获取分组配置内容
		$p_data = nl_project_group::query_by_id($dc, $nns_id);
		if($p_data['ret'] != 0 || empty($p_data['data_info']))
		{
			echo "<script>alert('添加失败');</script>";
		}
		else
		{
			$group_value = json_decode($p_data['data_info']['nns_value'],true);
			$key_id_arr = explode(",", $p_data['data_info']['nns_key_id']);
			array_push($key_id_arr, $key_id);
			
			$params['nns_key_id'] = implode(",", $key_id_arr);
			$params['nns_key_id'] = trim($params['nns_key_id'],",");
			if($params['nns_is_more'] == '0')//单个
			{
				$group_value[$key] = $params['nns_value'][0];
				
				$params['nns_value'] = addslashes(json_encode($group_value));		
			}
			else 
			{
				$group_value[$key] = del_empty_param($params['nns_value']);
				$params['nns_value'] = addslashes(json_encode($group_value));
			}
			unset($params['nns_is_more']);
			$result = nl_project_group::edit($dc, $params, $nns_id);
			if ($result['ret'])
			{
				echo "<script>alert('" . $result['reason'] . "');</script>";
			}
			else
			{
				$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "添加:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
				echo "<script>alert('添加成功');</script>";
			}
		}
		break;
	case "delete":
		//获取分组配置内容
		$p_data = nl_project_group::query_by_id($dc, $nns_id);
		if($p_data['ret'] != 0 || empty($p_data['data_info']))
		{
			echo "<script>alert('删除失败');</script>";
			break;
		}
		$group_value = json_decode($p_data['data_info']['nns_value'],true);
		$group_key_arr = explode(",", $p_data['data_info']['nns_key_id']);
		
		$key_id_arr = explode(",", $key_id);
		foreach ($key_id_arr as $keyid)
		{
			if (strlen($keyid) < 1)
			{
				continue;
			}
			$key_data = nl_project_key::query_by_id($dc, $keyid);
			if($key_data['ret'] != 0 || empty($key_data['data_info']))
			{
				continue;
			}
			$key = $key_data['data_info']['nns_key'];
			unset($group_value[$key]);
			$guid = array_search($keyid, $group_key_arr);
			unset($group_key_arr[$guid]);
		}
		$params['nns_key_id'] = implode(",", $group_key_arr);
		$params['nns_value'] = addslashes(json_encode($group_value));
		$result_del = nl_project_group::edit($dc, $params, $nns_id);
		if($result_del['ret'] != 0)
		{
			echo "<script>alert('" . $result_del['reason'] . "');</script>";
		}
		else 
		{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "删除:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
			echo "<script>alert('删除成功');</script>";
		}
		
		break;
	default:
	    echo "<script>alert('没有此方式');</script>";
		break;
}
$partner_inst = null;
$log_inst = null;
echo "<script>self.location='".$base_file_name_base."_list.php?group_id=".$nns_id."';</script>";

function del_empty_param($params)
{
	$return_params = array();
	if(is_array($params))
	{
		foreach ($params as $p=>$val)
		{
			if(!empty($val) || $val == '0')
			{
				$return_params[] = $val;
			}
		}
	}
	return $return_params;
}