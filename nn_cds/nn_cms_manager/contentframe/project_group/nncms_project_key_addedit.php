<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$action=$_GET["action"];
$nns_id=$_REQUEST["nns_id"];
$pri_bool=false;
if (!empty($action)){
	switch($action){
	case "edit":
		$action_str=cms_get_lang('save');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136002");
		break;
	case "add":
		$action_str=cms_get_lang('add');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136003");
		break;
	default:
		break;
	}
}
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';

$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);
$edit_data = $_REQUEST;






/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/project_group/project_key.class.php';
$dc = nl_get_dc(array (
    "db_policy" => NL_DB_WRITE,
    "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));
if ($action=="edit")
{
	$edit_data = nl_project_key::query_by_id($dc,$nns_id);
	
	if($edit_data['ret'] != 0 )
	{
		echo "<script>alert('". $action_str. $edit_data['reason']. "');</script>";
		echo "<script>self.location='".$base_file_name_base."_list.php;</script>";
	}
	$edit_data = $edit_data['data_info'];
}
// var_dump($edit_data);

$key_type_arr = nl_project_key::$key_type_arr;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link href="../../css/comm.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
        <script language="javascript">
        
        </script>
    </head>
    <body>
        <div class="content">
        	<div class="content_position">模块键值 > 添加/修改</div>
        	<form id="add_form" action="<?php echo $base_file_name_base;?>_control.php" method="post">
            	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
            	<input name="nns_model_id" type="hidden" value="<?php echo $_GET['model_id'];?>" />
            	<div class="content_table">
            		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            		  <tbody class="tr_tbody">
            		    <?php 
            		    if($action == 'edit'){
            		    ?>
                		    <tr>
                				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>GUID:</td>
                				<td>
                				    <input name="nns_id" id="nns_id" type="hidden"  value="<?php if(isset($edit_data["nns_id"])){echo $edit_data["nns_id"];}?>" rule="noempty"/>
                				    <input type="text" disabled value="<?php if(isset($edit_data["nns_id"])){echo $edit_data["nns_id"];}?>" />
                				</td>
                			</tr>
            		    <?php }?>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>键值KEY:</td>
            				<td><input name="nns_key" <?php if ($action == 'edit') echo 'disabled';?> id="nns_key" type="text"  value="<?php if(isset($edit_data["nns_key"])){echo $edit_data["nns_key"];}?>" rule="noempty" /></td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>键值名称:</td>
            				<td><input name="nns_name" id="nns_name" type="text"  value="<?php if(isset($edit_data["nns_name"])){echo $edit_data["nns_name"];}?>" rule="noempty" /></td>
            			</tr>
            			<tr>
            				<td class="rightstyle">键值配置样例:</td>
            				<td><textarea name="nns_example" id="nns_example"><?php if(isset($edit_data["nns_example"])){echo $edit_data["nns_example"];}?></textarea></td>
            			</tr>						
            			<tr>
            				<td class="rightstyle">描述:</td>
            				<td><textarea name="nns_desc" id="nns_desc"><?php if(isset($edit_data["nns_desc"])){echo $edit_data["nns_desc"];}?></textarea></td>
            			</tr>
            			<tr>
            				<td class="rightstyle">输入类型:</td>
            				<td>
            					<select name="nns_key_type" id="nns_key_type">
	            				<?php 
	            				foreach ($key_type_arr as $key=>$key_type)
	            				{
	            					if($action == 'edit' && $edit_data["nns_key_type"] == $key)
	            					{
	            						echo "<option value='{$key}' selected='selected'>{$key_type}</option>";
	            					}
	            					else 
	            					{
	            						echo "<option value='{$key}' {$checked}>{$key_type}</option>";
	            					}	
	            				}	
	            				?>
	            				</select>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle">是否支持配置多个:</td>
            				<td>
	            				<select name="nns_is_more" id="nns_is_more">
	            					<option value="0" <?php if(isset($edit_data["nns_is_more"]) && $edit_data["nns_is_more"] == '0'){echo "selected='selected'";}?>>否</option>
	            					<option value="1" <?php if(isset($edit_data["nns_is_more"]) && $edit_data["nns_is_more"] == '1'){echo "selected='selected'";}?>>是</option>
	            				</select>
            				</td>
            			</tr>
            			<?php 
            			if($action == 'edit' && $edit_data["nns_key_type"] == '1')//修改操作、类型为选择框
            			{
            				$key_value = json_decode($edit_data['nns_key_value'],true);
            				if(!empty($key_value))
            				{
            					$num = 0;
            					foreach ($key_value as $k=>$v)
            					{
            						$num++;
            			?>
            			<tr>
	            			<td class="rightstyle c_key">选择框内容-<?php echo $num;?>:</td>
	            			<td class="tr_td">KEY:<input type="text" name="value_keys[]" value="<?php echo $k;?>" style="width:100px;" />--VALUE:<input type="text" name="key_values[]" value="<?php echo $v;?>" style="width:440px;" /><input type="button" data-change="<?php echo $num;?>" value="删除" class="button_del" />	</td>
	            		</tr>
            			<?php	
            					}
            				}
            				else 
            				{
            			?>
            			<tr>
	            			<td class="rightstyle c_key">选择框内容-1:</td>
	            			<td class="tr_td">KEY:<input type="text" name="value_keys[]" style="width:100px;" />--VALUE:<input type="text" name="key_values[]" style="width:440px;" /><input type="button" data-change="1" value="删除" class="button_del" /></td>
	            		</tr>
            			<?php 	
            				}
            			?>
            			<tr>
            				<td class="rightstyle"></td>
            				<td><a href="#" class="button_add">扩展内容+</a></td>
            			</tr>
            			<?php }else{?>
            			<tr style="display:none;">
	            			<td class="rightstyle c_key">选择框内容-1:</td>
	            			<td class="tr_td">KEY:<input type="text" name="value_keys[]" style="width:100px;" />--VALUE:<input type="text" name="key_values[]" style="width:440px;" /><input type="button" data-change="1" value="删除" class="button_del" /></td>
	            		</tr>
            			<tr style="display:none;">
            				<td class="rightstyle"></td>
            				<td><a href="#" class="button_add">扩展内容+</a></td>
            			</tr>
            			<?php }?>
            		  </tbody>
            		</table>
            	</div>
        	</form>
        	<div class="controlbtns">
        		<div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','<?php echo cms_get_lang('webkz_qrccxg'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
        		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        		<div style="clear:both;"></div>
        	</div>
        </div>
        <script type="text/javascript">
	        $(document).ready(function() {
		        <?php if($action == 'edit'){?>
				var key_type = $("#nns_key_type").attr('value');
				if(key_type == '1')
				{
					$(".c_key").parent().show('slow');
					$(".button_add").parent().parent().show('slow');
				}
				<?php }?>				
			});
			$("#nns_key_type").change(function(){
				var key_type = $("#nns_key_type").attr('value');
				if(key_type == '1')
				{
					$(".c_key").parent().show('slow');
					$(".button_add").parent().parent().show('slow');
				}
				else
				{
					$(".c_key").parent().hide();
					$(".button_add").parent().parent().hide();
				}	
			})
			$(".button_add").click(function(){
				var obj = $('.tr_tbody .tr_td');
	            var copy = obj.eq(0).parents('tr').clone();
	            var input_length = obj.length;
	            var max_num = 10;
	            if (input_length >= max_num) 
	            {
	                alert('数量超出限制 > ' + max_num);
	                return;
	            }
	            var td_name = "选择框内容-" + (input_length + 1) + ":";
	            copy.find('td').eq(0).html(td_name);
	            copy.find('td').eq(1).find("input[type='text']").attr('value','');
	            copy.find('.button_del').attr('data-change', input_length+1);
	            $(this).parents('tr').before(copy.removeClass().show('slow'));
	            tr_order();
	            window.parent.resetFrameHeight();
			})
			//点击删除
            $(".button_del").live("click", function(){
            	var obj = $(this);
                var data = obj.attr('data-change');
                switch (data)
                {
                    case '1':
                    	obj.parent().find("input[type='text']").attr("value","");
                        break;
                    default :
                    	obj.val("").parent().parent().remove();
                        reset_num_after_delete();
                        break;
                }
            })
            function tr_order() 
            {
                var class_name = 'tr_odd';
                $("tr:even").removeClass(class_name);
                $("tr:odd").addClass(class_name);
            }
			//重置数字
            function reset_num_after_delete()
            {
                var obj = $('.tr_tbody .tr_td')
                var len = obj.length;
                if (len < 2)
                {
                    return ;
                }
                for (var i = 0; i < len ; i++)
                {
                    var item = obj.eq(i).parent();
                    item.find('td').eq(0).html('选择框内容-'+(i+1)+":");
                    item.find('.button_del').attr('data-change',i+1);
                }
            }
        </script>
    </body>
</html>
