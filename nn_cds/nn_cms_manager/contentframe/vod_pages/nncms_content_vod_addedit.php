<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
ob_start();
include("../../nncms_manager_inc.php");
include ($nncms_config_path."nn_cms_config/nn_cms_global.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/producer/producer.class.php';
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/language_contant.php";


$cp_list = nl_cp::query_all($dc);

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();

$action=$_GET["action"];
$nns_id=$_GET["nns_id"];
$view_type=$_GET["view_type"];
$depot_id=$_GET["depot_id"];
$depot_detail_id=$_GET["depot_detail_id"];
$pri_bool=false;
if (!empty($action)){
	switch($action){
	case "edit":
		$action_str=cms_get_lang('save');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"106101");
		break;
	case "add":
		$action_str=cms_get_lang('add');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"106100");
		break;
	default:
		break;
	}
}
$checkpri=null;



if (!$pri_bool){
	Header("Location: ../nncms_content_wrong.php");exit;
}

include ($nncms_db_path. "nns_common/nns_db_constant.php");
//include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include ($nncms_db_path. "nns_category/nns_db_category_list_class.php");
//关键字
//include($nncms_db_path. "nns_keyword/nns_db_keyword_category_class.php");
//include($nncms_db_path. "nns_keyword/nns_db_keyword_class.php");
//$keyword_category_model =new nns_db_keyword_category_class();
//$all_keyword_category = $keyword_category_model->get_keyword_category_list();
//$keyword_model =new nns_db_keyword_class();
//$all_keyword = $keyword_model->get_all_keyword();

// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");

if ($action=="edit"){
	include $nncms_db_path. "nns_vod/nns_db_vod_class.php";
	$vod_inst=new nns_db_vod_class();
	if (!empty($nns_id)){
		$vod_info=$vod_inst->nns_db_vod_info($nns_id,"*");
		$vod_inst=null;
	}else{
		echo "<script>alert('".$vod_info["reason"] ."');self.location='nncms_content_vodlist.php';</script>";
	}
	if ($vod_info["ret"]!=0){
		echo "<script>alert('".$vod_info["reason"] ."');self.location='nncms_content_vodlist.php';</script>";
	}
	$edit_data=$vod_info["data"][0];
	/***
	 * 废除 原TAG方式
	 */
	//for ($tag_num=0;$tag_num<32;$tag_num++){
	//	if ($edit_data["nns_tag".$tag_num]==1){
	//		$tar_str.=$tag_num.',';
	//	};
	//}

	$tar_str=ltrim($edit_data['nns_tag'],',');

	$depot_id=$edit_data["nns_depot_id"];
	$depot_detail_id=$edit_data["nns_category_id"];

	$nns_keyword_arr = explode(',',$edit_data["nns_keyword"]);

	//		$vod_inst=null;
	//	$edit_pri=$edit_data["nns_pri_id"];
}

$category_list_inst=new nns_db_category_list_class();
$category_list_result=$category_list_inst->nns_db_category_list_list("",0,null,null,0,0,0);
if ($category_list_result["ret"]==0) $category_list_data=$category_list_result["data"];

include $nncms_db_path. "nns_carrier/nns_db_carrier_class.php";
include $nncms_db_path. "nns_partner/nns_db_partner_class.php";
include $nncms_db_path. "nns_group/nns_db_group_class.php";
$carrier_inst=new nns_db_carrier_class();
$partner_inst=new nns_db_partner_class();
$group_inst=new nns_db_group_class();
if ($_SESSION["nns_manager_type"]==0){



	$carrier_result=$carrier_inst->nns_db_carrier_list();

	if ($carrier_result["ret"]==0){
		$carrier_data=$carrier_result["data"][0];
	}
	if ($action=="edit"){
		//		 $partner_inst=new nns_db_partner_class();
		$partner_result=$partner_inst->nns_db_partner_info($edit_data["nns_org_id"]);

		if ($partner_result["ret"]==0){
			$partner_data=$partner_result["data"][0];
		}

		//		 $group_inst=new nns_db_group_class();
		$group_result=$group_inst->nns_db_group_info($edit_data["nns_org_id"]);

		if ($group_result["ret"]==0){
			$group_data=$group_result["data"][0];
		}
	}
}else{

	//	类型为合作伙伴
	if ($_SESSION["nns_manager_type"]==1){

		$partner_result=$partner_inst->nns_db_partner_info($_SESSION["nns_org_id"]);

		if ($partner_result["ret"]==0){
			$partner_data=$partner_result["data"][0];
		}
		//	类型为集团
	}else if ($_SESSION["nns_manager_type"]==2){

		$group_result=$group_inst->nns_db_group_info($_SESSION["nns_org_id"]);

		if ($group_result["ret"]==0){
			$group_data=$group_result["data"][0];
		}
	}
}
ob_end_clean();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/radiolist.js"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/cms_ajax.js"></script>
<script language="javascript" src="../../js/cms_vod.js.php"></script>
<script language="javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="../../js/rate.js"></script>
<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
<script language="javascript" src="../../js/cms_datepicker.js"></script>
<script language="javascript">
var lable_obj={
	title_pcsj:["<?php echo cms_get_lang('media_zsc');?>","<?php echo cms_get_lang('media_mjpc');?>"],
		title_fps:["<?php echo cms_get_lang('media_fps');?>","<?php echo cms_get_lang('media_zjs');?>"],
		title_lx:["<?php echo cms_get_lang('media_dy|name').':';?>","<?php echo cms_get_lang('media_lxj|name').':';?>"]
};
$(document).ready(function(){
	//关键字选择
	$(".prant_check_box").click(function(){
		var bool;
		if ($(this).attr("checked")=="checked"){
			bool=true;
		}else{
			bool=false;
		}
		$(this).parent().parent().find("> .radioitem > input[type=checkbox]").attr("checked",bool);
	});

	$(".selectbox").hide();

	var $obj=$("#nns_carrier").find("option:selected");
	var index=$("#nns_carrier option").index($obj);
	selectCarrier(index);
	$("#nns_carrier").change(function(){
		var $obj=$(this).find("option:selected");
		var index=$("#nns_carrier option").index($obj);
		selectCarrier(index);
	});
	selectMode(<?php echo $edit_data["nns_view_type"];?>);
	$("#nns_view_type").change(function(){
		selectMode($("#nns_view_type").val());
	});

	get_data();
	$("#nns_cp_id").change(function(){
		get_data();
	});
	$(".extends").hide();

});

function set_show_minor(){

	if ($("#moreinfo_chk").attr("checked")){
		$(".extends").show();
	}else{
		$(".extends").hide();
	}
	window.parent.checkheight();
}

function selectMode(num){
	if (num==undefined || num==""){
		num=0;
	}
	for (pro in lable_obj){

		$("#"+pro).html(lable_obj[pro][num]);
	}
}
function selectCarrier(num){

	$("#nns_partner_control").hide();
	$("#nns_group_control").hide();
	$("#nns_group_input").attr("rule","");
	$("#nns_partner_input").attr("rule","");

	var val=$("#nns_carrier option:eq("+num+")").attr("value");
	switch(val){
	case "1":
		$("#nns_partner_input").attr("rule","noempty");
		$("#nns_partner_control").show();
		break;
	case "2":
		$("#nns_group_input").attr("rule","noempty");
		$("#nns_group_control").show();
		break;
	}
	window.parent.checkheight();
}

function begin_select_partner(){
	$("#select_frame").attr("src","../../controls/nncms_controls_select.php?method=partner");
	$(".selectbox").show();
}

function begin_select_group(){
	$("#select_frame").attr("src","../../controls/nncms_controls_select.php?method=group");
	$(".selectbox").show();
}
function close_select(){
	$(".selectbox").hide();
}

function set_partner_id(value,name){
	$("#nns_partner").attr("value",value);
	$("#nns_partner_input").attr("value",name);
}

function set_group_id(value,name){
	$("#nns_group").attr("value",value);
	$("#nns_group_input").attr("value",name);
}
//ajax 动态加载动态框  flag=true(转换控制模式的时候 动态框全部清空再添加动态框)  flag=false(添加动态框的时候)
function get_data()
{
	var str_cp_id= $('#nns_cp_id').val();
	str_cp_id = str_cp_id == 'undefined' ? '' : str_cp_id;
	$.get("nncms_content_vod_control.php", { action: "query",nns_cp_id : str_cp_id},
		function(data)
		{
    		$('#nns_import_source').html('');
    		if(data.ret !=0)
    		{
    			alert(data.reason);return;
        	}
        	var str_html='<option value="">--请选择内容提供商--</option>';
        	var exsist_str = '<?php if(isset($edit_data['nns_import_source'])){ echo $edit_data['nns_import_source'];}else{ echo "";}?>';
        	if(data.data_info.length > 0)
            {
            	for(var i=0;i<data.data_info.length;i++)
    			{
        			var str_selected = exsist_str == data.data_info[i].nns_import_source ? 'selected="selected"' : '';
            		str_html+="<option value='"+data.data_info[i].nns_import_source+"' "+str_selected+" >"+data.data_info[i].nns_producer+"</option>";
                }
            }
        	$('#nns_import_source').append(str_html);
		},'json');	
}
</script>
</head>

<body>

<div class="selectbox">
		<iframe scrolling="no" frameborder="0" id="select_frame"></iframe>
 </div>
<div class="content">
	<!--<div class="content_position"><?php echo cms_get_lang('dbgl');?> > <?php if ($view_type==0){ echo cms_get_lang('media_dy|dbgl');} else if ($view_type==1){echo cms_get_lang('media_lxj|dbgl');}?></div>-->
	<form id="add_form" action="nncms_content_vod_control.php" method="post">
	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
	<input name="nns_id" id="nns_id" type="hidden" value="<?php echo $nns_id;?>"/>
	<input name="nns_tags" id="nns_tags" type="hidden" value=""/>
	<input name="view_type" id="view_type" type="hidden" value="<?php echo $view_type;?>"/>
	 <input name="depot_id" id="depot_id" type="hidden" value="<?php echo $depot_id;?>"/>
	<input name="depot_detail_id" id="depot_detail_id" type="hidden" value="<?php echo $depot_detail_id;?>"/>
	  <input name="nns_partner" id="nns_partner" type="hidden" value="<?php echo $edit_data["nns_org_id"];?>"/>
	<input name="nns_group" id="nns_group" type="hidden" value="<?php echo $edit_data["nns_org_id"];?>"/>
	 <input name="nns_point" id="nns_point" type="hidden" value="1"/>
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			  <tr>
				<td id="title_lx" class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>:</td>
				<td><input name="nns_vod_name" id="nns_vod_name" type="text" rule="noempty" value="<?php if($action=="edit"){echo $edit_data["nns_name"];}?>" /> </td>

			</tr>

			<?php if($action=="edit"){?>
			<tr>
				<td class="rightstyle"><?php echo cms_get_lang('media_pinyin');?>:</td>
				<td>
					<input name="nns_pinyin" id="nns_pinyin" type="text" value="<?php echo $edit_data["nns_pinyin"];?>" />
				</td>
			</tr>
			<?php }?>
			<tr>
				<td class="rightstyle"><?php echo cms_get_lang('media_ywsy');?>:</td>
				<td>
					<input name="nns_eng_name" id="nns_eng_name" type="text" value="<?php if($action=="edit"){echo $edit_data["nns_eng_name"];}?>" />
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><?php echo cms_get_lang('media_bm');?>:</td>
				<td>
					<input name="nns_alias_name" id="nns_alias_name" type="text" value="<?php if($action=="edit"){echo $edit_data["nns_alias_name"];}?>" />
				</td>
			</tr>


			<tr class="select_carrier_class">
				<td class="rightstyle"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('group');?>:</td>
				<td><select name="nns_carrier" id="nns_carrier"  style="margin:5px 0px;">
						<option value="<?php echo $carrier_data["nns_id"];?>" <?php if ($action=="edit" && $edit_data["nns_type"]==0){?>selected="selected"<?php }?>><?php echo cms_get_lang('xtgl_yysgl')."/" . $carrier_data["nns_name"];?></option>
						<?php if ($g_partner_enabled==1){?>
						<option value="1" <?php if ($action=="edit" && $edit_data["nns_type"]==1){?>selected="selected"<?php }?>><?php echo cms_get_lang('partner');?></option>
						<?php }if ($g_group_enabled==1){?>
						<option value="2" <?php if ($action=="edit" && $edit_data["nns_type"]==2){?>selected="selected"<?php }?>><?php echo cms_get_lang('group');?></option>
						<?php }?>
					</select>
					<?php if ($g_partner_enabled==1){?>
						<div id="nns_partner_control">
						<input name="nns_partner_input" id="nns_partner_input" type="text" value="<?php echo $partner_data["nns_name"];?>" style="width:30%;" readonly="true" disabled="false"/>&nbsp;&nbsp;<input onclick="begin_select_partner();" name="" type="button" value="<?php echo cms_get_lang('search|partner')?>"/>
						 </div>
					<?php } if ($g_group_enabled==1){?>
						<div id="nns_group_control">
						<input name="nns_group_input" id="nns_group_input" type="text" value="<?php echo $group_data["nns_name"];?>" style="width:30%;" readonly="true"  disabled="false"/>&nbsp;&nbsp;<input name="" type="button" value="<?php echo cms_get_lang('search|group_jtxx');?>" onclick="begin_select_group();"/>
						 </div>
					<?php }?>


				</td>
			</tr>
			<!--<tr>
				<td class="rightstyle"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('media_video_lx');?>:</td>
				<td><select name="nns_view_type" id="nns_view_type"  style="margin:5px 0px;"><br>

						<option value="0" <?php if ($edit_data["nns_view_type"]==0){?>selected="selected"<?php }?>><?php echo cms_get_lang('media_dy');?></option>

						<option value="1" <?php if ($edit_data["nns_view_type"]==1){?>selected="selected"<?php }?>><?php echo cms_get_lang('media_lxj');?></option>

					</select>
				</td>
			</tr>-->
			<?php if ($g_key_word_enabled==1){?>
			<!--<tr>
				<td class="rightstyle"><?php echo cms_get_lang('keywords');?></td>
				<td>

<?php
if(!empty($all_keyword_category)){
	$html = '<div class="radiolist">';
	foreach($all_keyword_category as $keyword_category){
		$html .= '<div class="radiogroup">';//$keyword_category['nns_name']."：&nbsp;&nbsp;";
		$html .= '<h3><input class="prant_check_box" name="" type="checkbox"/>&nbsp;'.$keyword_category['nns_name'].' </h3>';
		foreach($all_keyword as $keyword){
			if($keyword['nns_keyword_category_id'] == $keyword_category['nns_id']){
				$html .= '<div class="radioitem">';
				$checked = '';
				if($nns_keyword_arr && in_array($keyword['nns_id'],$nns_keyword_arr)) $checked='checked';
				$html .= '<input type="checkbox" name="nns_keyword[]" value="'.$keyword['nns_id'].'" '.$checked.'/>&nbsp;&nbsp;'.$keyword['nns_name'];
				$html .= '</div>';
			}
		}
		$html .= '<div style="clear:both;"/>';
		$html .= '</div>';
	}
	$html .= '</div>';
	echo $html;
}else{
	echo cms_get_lang('keyword_no_gjz_can_using');
}

?>
				</td>
			</tr>-->
			<?php }?>
			<tr>
				<td class="rightstyle"><?php echo cms_get_lang('media_tjfs');?>:</td>
				<td>
					<span class="p_rate" id="p_rate">
					<i class="select" title="1<?php echo cms_get_lang('point');?>"></i>
					<i title="2<?php echo cms_get_lang('point');?>"></i>
					<i title="3<?php echo cms_get_lang('point');?>"></i>
					<i title="4<?php echo cms_get_lang('point');?>"></i>
					<i title="5<?php echo cms_get_lang('point');?>"></i>
					</span>
<script>
var Rate = new pRate("p_rate",function(){
	$("#nns_point").val(Rate.Index);
});
<?php if($action=="edit"){?>
Rate.setindex(<?php echo $edit_data["nns_point"];?>);
<?php }?>
</script>
				</td>
			</tr>



			<tr>
				<td class="rightstyle"><?php echo cms_get_lang('zdgl_zdscbj');?>:</td>
				<td>
					<div class="radiolist" id="tag_list">

					<div class="radiogroup">
						<?php if ( $g_mode!="xhpm"){?>
							<div class="radioitem" >
								<?php echo get_tag_input_box(NNS_PLAYER_STD_TAG,$tar_str);?>
							</div>
							<div class="radioitem" >
								<?php echo get_tag_input_box(NNS_PC_TAG,$tar_str);?>
							</div>
							<div class="radioitem" >
								<?php echo get_tag_input_box(NNS_ANDROID_PAD_TAG,$tar_str);?>
							</div>
							<div class="radioitem" >
								<?php echo get_tag_input_box(NNS_ANDROID_PHONE_TAG,$tar_str);?>
							</div>
							<div class="radioitem" >
								<?php echo get_tag_input_box(NNS_APPLE_PAD_TAG,$tar_str);?>
							</div>
							<div class="radioitem" >
								<?php echo get_tag_input_box(NNS_APPLE_PHONE_TAG,$tar_str);?>
							</div>

						<?php }?>

						 <?php for($device_num=0; $device_num<$g_manager_list_device_num; $device_num++){?>

						<div class="radioitem">
							<?php echo get_tag_input_box($device_num,$tar_str);?>

						</div>
						<?php } ?>
						<div style="clear:both;"/>
					</div>
				</div>
				</td>
			</tr>
			  <?php if(isset($cp_list['data_info']) && is_array($cp_list['data_info']) && !empty($cp_list['data_info'])){?>
				  <tr>
					  <td class="rightstyle" width="120">选择CP</td>
					  <td>
						  <select name="nns_cp_id" id="nns_cp_id" rule="">
							  <?php foreach ($cp_list['data_info'] as $cp_list_val){?>
								  <option value="<?php echo $cp_list_val['nns_id']?>" <?php if($action=="edit" && $edit_data["nns_cp_id"] == $cp_list_val['nns_id']){echo "selected";}?> ><?php echo $cp_list_val['nns_name']; ?></option>
							  <?php }?>
						  </select>
					  </td>
				  </tr>
				  
			  <?php }else{?>
			  <tr>
					  <td class="rightstyle" width="120">选择CP</td>
					  <td>
					   <select name="nns_cp_id" id="nns_cp_id" rule="">
				        <option value="<?php echo $cp_list['nns_id']; ?>" <?php echo $selected;?>><?php echo $cp_list['nns_name']; ?></option>
				        </select>
					  </td>
				  </tr>
			  <?php }?>
			  <tr>
					  <td class="rightstyle" width="120">选择内容提供商</td>
					  <td>
						  <select name="nns_import_source" id="nns_import_source">
						      
							  
						  </select>
					  </td>
				  </tr>
			<tr>
				<td class="rightstyle"><?php echo cms_get_lang('media_yp|type');?>:</td>
				<td>
					<input name="nns_kind" id="nns_kind" type="text" value="<?php if($action=="edit"){echo $edit_data["nns_kind"];}?>" />
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><?php echo cms_get_lang('media_yp|keyword');?>:</td>
				<td>
					<input name="nns_keyword" id="nns_keyword" type="text" value="<?php if($action=="edit"){echo $edit_data["nns_keyword"];}?>" />
				</td>
			</tr>

			<tr>
				<td class="rightstyle"><?php echo cms_get_lang('media_sysj');?>:</td>
				<td>

					<input name="nns_vod_show_time" id="nns_vod_show_time"  class="datepicker"  type="text" value="<?php if($action=="edit"){echo $edit_data["nns_show_time"];}?>" value="2006-1-1" readonly="readonly"/>
				</td>
			</tr>




			<tr>
				<td class="rightstyle"><?php echo cms_get_lang('media_daoyan');?>:</td>
				<td>
					<input name="nns_vod_directory" id="nns_vod_directory" type="text" value="<?php if($action=="edit"){echo $edit_data["nns_director"];}?>"/>
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><?php echo cms_get_lang('media_yy');?>:</td>
				<td>
					<input name="nns_vod_actor" id="nns_vod_actor" type="text" value="<?php if($action=="edit"){echo $edit_data["nns_actor"];}?>"/>
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><?php echo cms_get_lang('media_spyz');?>:</td>
				<td>
					<input name="nns_language" id="nns_language" type="text" value="<?php if($action=="edit"){echo $edit_data["nns_language"];}?>" />
				</td>
			</tr>

			<tr>
				<td class="rightstyle"><?php echo cms_get_lang('media_dq');?>:</td>
				<td>
					<input name="nns_vod_area" id="nns_vod_area" type="text" value="<?php if($action=="edit"){echo $edit_data["nns_area"];}?>"/>
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><?php echo cms_get_lang('zyk_play_count');?>:</td>
				<td>
					<input name="nns_play_count" id="nns_play_count" type="text" value="<?php if($action=="edit"){echo $edit_data["nns_play_count"];}?>"/>
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><font id="title_pcsj"></font>:</td>
				<td>
					<input name="nns_vod_view_len" id="nns_vod_view_len" type="text"  value="<?php if($action=="edit"){echo ceil($edit_data["nns_view_len"]/60);}?>"/>
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><font id="title_fps">*</font>:</td>
				<td>
					<input name="nns_vod_episode" id="nns_vod_episode" type="text" rule="noempty|uint" value="<?php if($action=="edit"){echo $edit_data["nns_all_index"];}else{echo 1;}?>" />
				</td>
			</tr>


			<tr>
				<td class="rightstyle"><?php echo cms_get_lang('media_nrjj');?>:</td>
				<td>
					<textarea name="nns_vod_summary" id="nns_vod_summary" cols="" rows=""><?php if($action=="edit"){echo $edit_data["nns_summary"];}?> </textarea>
					<input name="moreinfo_chk" id="moreinfo_chk" type="checkbox" onclick="set_show_minor();" /> <font style="color:#0000ff"><?php echo cms_get_lang('gdxx');?></a>
				</td>
			</tr>
			<tr class="extends">
				<td class="rightstyle"><?php echo cms_get_lang('media_copyright_date');?>:</td>
				<td>
					<input name="nns_copyright_date" id="nns_copyright_date" class="datepicker" type="text" value="<?php if($action=="edit"){echo $edit_data["nns_copyright_date"];}?>" value="2006-1-1" readonly="readonly"/>
				</td>
			</tr>

			<tr  class="extends">
				<td class="rightstyle"><?php echo cms_get_lang('media_zm');?>:</td>
				<td>
					<input name="nns_text_lang" id="nns_text_lang" type="text" value="<?php if($action=="edit"){echo $edit_data["nns_text_lang"];}?>" />
				</td>
			</tr>
			<tr  class="extends">
				<td class="rightstyle"><?php echo cms_get_lang('media_yp|media_bs');?>:</td>
				<td>
					<input name="nns_vod_part" id="nns_vod_part" type="text" value="<?php if($action=="edit"){echo $edit_data["nns_vod_part"];}?>" />
				</td>
			</tr>
			<tr  class="extends">
				<td class="rightstyle"><?php echo cms_get_lang('media_cpr');?>:</td>
				<td>
					<input name="nns_producer" id="nns_producer" type="text" value="<?php if($action=="edit"){echo $edit_data["nns_producer"];}?>" />
				</td>
			</tr>
			<tr  class="extends">
				<td class="rightstyle"><?php echo cms_get_lang('media_bj');?>:</td>
				<td>
					<input name="nns_screenwriter" id="nns_screenwriter" type="text" value="<?php if($action=="edit"){echo $edit_data["nns_screenwriter"];}?>" />
				</td>
			</tr>
			<tr  class="extends">
				<td class="rightstyle"><?php echo cms_get_lang('media_byjs');?>:</td>
				<td>
					<textarea name="nns_play_role" id="nns_play_role" cols="" rows=""><?php if($action=="edit"){echo $edit_data["nns_play_role"];}?></textarea>
				</td>
			</tr>



		  </tbody>
		</table>


	</div>
	</form>
	<div class="controlbtns">
		<div class="controlbtn <?php echo $action;?>"><a href="javascript:$('#nns_tags').attr('value',getCheckedData(true,$('#tag_list')));checkForm('<?php echo cms_get_lang('dbgl');?>','<?php echo cms_get_lang('msg_ask_change'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
		<?php $langs=g_cms_config::get_g_extend_language(); if ($action=='edit' && !empty($langs)){?>
		<div class="controlbtn lang"><a href="nncms_content_vod_info_language_extend.php?nns_id=<?php echo $nns_id;?>"><?php echo cms_get_lang('dyz');?></a></div>
		<?php }?>
		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>





</body>
</html>