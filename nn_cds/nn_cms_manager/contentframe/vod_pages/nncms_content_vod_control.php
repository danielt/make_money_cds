<?php

/*
 * Created on 2012-3-2
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
set_time_limit(0);
@header("Content-Type:text/html;charset=utf-8");
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
    $language_dir = $_SESSION["language_dir"];
}

// 获取cache模块
include LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'vod.class.php';
include LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'vod_info_language.class.php';
include LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'vod_index_info_language.class.php';
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$dc->open();
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//require_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_clear_cache.php";
require_once $nncms_db_path . "nns_log" . DIRECTORY_SEPARATOR . "nns_db_op_log_class.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/producer/producer.class.php';
//var_dump($_REQUEST);die;
$log_inst = new nns_db_op_log_class();
$action = $_REQUEST["action"];
$nns_id = $_POST["nns_id"];
$audit = $_POST["audit"];
$nns_url = $_POST["nns_url"];

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
if (!empty($action)) {
    switch ($action) {
        case "query":
            $action_str = cms_get_lang('edit|media_db');
            $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106101");
            break;
        case "edit":
            $action_str = cms_get_lang('edit|media_db');
            $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106101");
            break;
        case "add":
            $action_str = cms_get_lang('add|media_db');
            $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106100");
            break;
        case "delete":
            $action_str = cms_get_lang('delete|media_db');
            $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106102");
            break;
        case "realdelete":
            $action_str = cms_get_lang('real_delete|media_db');
            $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106102");
            break;
        case "audit":
            $action_str = cms_get_lang('audit|media_db');
            $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106103");
            break;
        case "reset":
            $action_str = cms_get_lang('reset|media_db');
            $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106102");
            break;
        case "move_category":
            $action_str = cms_get_lang('move_catalog|media_db');
            $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106101");
            break;
        case "unline":
            $action_str = cms_get_lang('media_xiax');
            $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106101");
            break;
        case "ctag":
            $action_str = cms_get_lang('edit_epg_more');
            $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106101");
            break;
        case "recover":
            $action_str = cms_get_lang('reset|media_db');
            $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106101");
            break;
        case "complement":
            $action_str = cms_get_lang('reset|media_db');
            $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106101");
            break;
        default:
            break;
    }
}
$checkpri = null;
if (!$pri_bool) {
    Header("Location: ../nncms_content_wrong.php");
} else {

    $nns_view_type = $_POST["nns_view_type"];
    $nns_view_type = empty($nns_view_type) ? 0 : $nns_view_type;
    $nns_vod_name = $_POST["nns_vod_name"];
    $nns_carrier = $_POST["nns_carrier"];
    $nns_partner_input = $_POST["nns_partner"];
    $nns_group_input = $_POST["nns_group"];
    $nns_vod_category_public = $_POST["nns_vod_category_public"];
    $nns_vod_category_public_id = $_POST["nns_vod_category_public_id"];
    $nns_vod_category_private = $_POST["nns_vod_category_private"];
    $nns_vod_category_private_id = $_POST["nns_vod_category_private_id"];
    $nns_vod_directory = $_POST["nns_vod_directory"];
    $nns_vod_actor = $_POST["nns_vod_actor"];
    $nns_vod_show_time = $_POST["nns_vod_show_time"];
    $nns_vod_view_len = $_POST["nns_vod_view_len"] * 60;
    $nns_vod_episode = $_POST["nns_vod_episode"];
    $nns_vod_area = $_POST["nns_vod_area"];
    $nns_tags = $_POST["nns_tags"];
    $nns_vod_summary = $_POST["nns_vod_summary"];
    $nns_vod_new_episode = $_POST["nns_vod_new_episode"];
    $depot_id = $_POST["depot_id"];
    $depot_detail_id = $_POST["depot_detail_id"];
    $nns_point = $_POST["nns_point"];
    $nns_pinyin = $_POST["nns_pinyin"];
    $nns_cp_id = $_POST['nns_cp_id'];

    $nns_copyright_date = $_POST["nns_copyright_date"];
    $nns_language = $_POST["nns_language"];
    $nns_producer = $_POST["nns_producer"];
    $nns_alias_name = $_POST["nns_alias_name"];
    $nns_screenwriter = $_POST["nns_screenwriter"];
    $nns_eng_name = $_POST["nns_eng_name"];
    $nns_play_role = $_POST["nns_play_role"];
    $nns_vod_part = $_POST["nns_vod_part"];
    $nns_keyword = $_POST["nns_keyword"];
    $nns_kind = $_POST["nns_kind"];

    // require_once $nncms_config_path . "nn_cms_manager/controls/nncms_control_memcache.php";

    //	判断管理类型
    if ($_SESSION["nns_manager_type"] == 0) {
        if ($nns_carrier == 1) {
            $nns_org_id = $nns_partner_input;
            $nns_org_type = $nns_carrier;
        } else if ($nns_carrier == 2) {
            $nns_org_id = $nns_group_input;
            $nns_org_type = $nns_carrier;
        } else {
            $nns_org_id = $nns_carrier;
            $nns_org_type = 0;
        }
    } else {
        $nns_org_id = $_SESSION["nns_org_id"];
        $nns_org_type = $_SESSION["nns_manager_type"];
    }



    switch ($nns_org_type) {
        case 0:
            $nns_vod_category_private = "";
            $nns_vod_category_private_id = "";
            break;
        case 1:
            if ($nns_vod_category_private == 0) {
                $nns_vod_category_private = "";
                $nns_vod_category_private_id = "";
            } else if ($nns_vod_category_public == 0) {
                $nns_vod_category_public = "";
                $nns_vod_category_public_id = "";
            }
            break;
        case 2:
            $nns_vod_category_public = "";
            $nns_vod_category_public_id = "";
            break;
    }

    $category_array = array("pub_category_id" => $nns_vod_category_public,
                            "pri_category_id" => $nns_vod_category_private,
                            "pub_category_detail_id" => $nns_vod_category_public_id,
                            "pri_category_detail_id" => $nns_vod_category_private_id);
    $tag_array = explode(",", $nns_tags);

    $nns_copyright_date = $_POST["nns_copyright_date"];
    $nns_language = $_POST["nns_language"];
    $nns_producer = $_POST["nns_producer"];
    $nns_alias_name = $_POST["nns_alias_name"];
    $nns_screenwriter = $_POST["nns_screenwriter"];
    $nns_eng_name = $_POST["nns_eng_name"];
    $nns_play_role = $_POST["nns_play_role"];
    $nns_vod_part = $_POST["nns_vod_part"];

    $vod_info_array = array(
        "director" => $nns_vod_directory,
        "actor" => $nns_vod_actor,
        "show_time" => $nns_vod_show_time,
        "view_len" => $nns_vod_view_len,
        "all_index" => $nns_vod_episode,
        "new_index" => $nns_vod_new_episode,
        "area" => $nns_vod_area,
        "vod_part" => $nns_vod_part,
        "play_role" => $nns_play_role,
        "eng_name" => $nns_eng_name,
        "screenwriter" => $nns_screenwriter,
        "alias_name" => $nns_alias_name,
        "producer" => $nns_producer,
        "language" => $nns_language,
        //	"nns_keyword"=> implode(',',$nns_keyword)
        "kind" => $nns_kind,
        "nns_keyword" => $nns_keyword,
        //	'nns_play_count' => isset($_POST["nns_play_count"])?$_POST["nns_play_count"]:0,
        //   应该判断是否为空，空则为0
        'nns_play_count' => empty($_POST["nns_play_count"]) ? 0 : $_POST["nns_play_count"],
    );

    include $nncms_db_path . "nns_common/nns_db_constant.php";
    include $nncms_db_path . "nns_vod/nns_db_vod_class.php";
    require_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_pinyin_class.php";
    $vod_inst = new nns_db_vod_class();
    switch ($action) {
        case 'complement':
            include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api_v2/control/public_sync_source.php';
            include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/mgtv_init.php';
            include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api_v2/nn_const.php';
            include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api_v2/common.php';
            include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
            include_once dirname(dirname(dirname(dirname(__FILE__))))  . '/nn_logic/nl_log_v2.func.php';
            include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';
            include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/message/nl_message.class.php';
            include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/models/import_model.php';
            include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/depot/depot.class.php';
            $_REQUEST['nns_id'] = isset($_REQUEST['nns_id']) ? $_REQUEST['nns_id'] : '';
            if(strlen($_REQUEST['nns_id']) <1)
            {
                echo "<script>alert('ID为空');</script>";
                echo "<script language=javascript>history.go(-1);</script>";die;
            }
            $arr_id = array_filter(explode(',', $_REQUEST['nns_id']));
            if(empty($arr_id) || !is_array($arr_id))
            {
                echo "<script>alert('ID为空');</script>";
                echo "<script language=javascript>history.go(-1);</script>";die;
            }
            $result_depot = nl_depot::get_depot_by_id($dc, $_REQUEST['complement_depot_id']);
            if($result_depot['ret'] !=0)
            {
                echo "<script>alert('" . $result_depot['reason'] . "');</script>";
                echo "<script language=javascript>history.go(-1);</script>";die;
            }
            $nns_category = isset($result_depot['data_info']['nns_category']) ? $result_depot['data_info']['nns_category'] : '';
            if(strlen($nns_category) <1)
            {
                echo "<script>alert('栏目查询为空');</script>";
                echo "<script language=javascript>history.go(-1);</script>";die;
            }
            //加载xml
            $dom = new DOMDocument('1.0', 'utf-8');
            $dom->loadXML($nns_category);
            $category_id_arr = array();
            $categorys = $dom->getElementsByTagName('category');
            $arr_cate = array();
            foreach ($categorys as $category)
            {
                $arr_cate[(string)$category->getAttribute('id')] = $category->getAttribute('name');
            }
            include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api_v2/control/standard/sync_source.php';
            $temp_arr_data = array();
            $vod_complement_flag = (isset($_REQUEST['vod_complement_flag']) && $_REQUEST['vod_complement_flag'] == '1') ? false : true;
            foreach ($arr_id as $val)
            {
                $obj_sync = null;
                if(strlen($val) <1)
                {
                    continue;
                }
                $result_vod = nl_vod::get_video_info_by_id($dc,$val);
                if(!isset($result_vod['data_info'][0]) || empty($result_vod['data_info'][0]) || !is_array($result_vod['data_info'][0]))
                {
                    continue;
                }
                $result_vod = $result_vod['data_info'][0];
                if(!isset($arr_cate[$result_vod['nns_category_id']]))
                {
                    continue;
                }
                $content = array(
                    'Name'=>$result_vod['nns_name'],
                    'CategoryName'=>$arr_cate[$result_vod['nns_category_id']],
                    'ReleaseYear'=>$result_vod['nns_show_time'],
                    'Kind'=>$result_vod['nns_kind'],
                    'SearchName'=>$result_vod['nns_name'],
                    'DirectorDisplay'=>$result_vod['nns_director'],
                    'AliasName'=>$result_vod['nns_alias_name'],
                    'EnglishName'=>$result_vod['nns_eng_name'],
                    'ActorDisplay'=>$result_vod['nns_actor'],
                    'Tags'=>$result_vod['nns_tag'],
                    'OriginalCountry'=>$result_vod['nns_area'],
                    'VolumnCount'=>$result_vod['nns_all_index'],
                    'bigpic'=>$result_vod['nns_image0'],
                    'square_img'=>$result_vod['nns_image_s'],
                    'smallpic'=>$result_vod['nns_image2'],
                    'middlepic'=>$result_vod['nns_image1'],
                    'verticality_img'=>$result_vod['nns_image_v'],
                    'horizontal_img'=>$result_vod['nns_image_h'],
                    'Description'=>$result_vod['nns_summary'],
                    'WriterDisplay'=>'',
                    'Language'=>$result_vod['nns_language'],
                    'Duration'=>$result_vod['nns_view_len'],
                );
                $result_cp = nl_cp::query_by_id($dc, $result_vod['nns_cp_id']);

                if(isset($temp_arr_data[$result_vod['nns_cp_id']]))
                {
                    if(empty($temp_arr_data[$result_vod['nns_cp_id']]) || !is_array($temp_arr_data[$result_vod['nns_cp_id']]))
                    {
                        continue;
                    }
                }
                else
                {
                    if($result_cp['ret'] !=0 || !isset($result_cp['data_info']) || !is_array($result_cp['data_info']) || empty($result_cp['data_info']))
                    {
                        continue;
                    }
                    $temp_arr_data[$result_vod['nns_cp_id']]['nns_name'] = $result_cp['data_info']['nns_name'];
                    $temp_arr_data[$result_vod['nns_cp_id']]['nns_config'] = (isset($result_cp['data_info']['nns_config']) && strlen($result_cp['data_info']['nns_config']) >0) ? json_decode($result_cp['data_info']['nns_config'],true) : null;
                }
                $obj_sync = new standard_sync_source(array('cp_id'=>$result_vod['nns_cp_id'],'cp_name'=>$temp_arr_data[$result_vod['nns_cp_id']]['nns_name']),$temp_arr_data[$result_vod['nns_cp_id']]['nns_config'],'',$result_vod['nns_cp_id']);
                $obj_sync->import->download_img_enabled = false;
                $obj_sync->no_need_change_info = $vod_complement_flag;
                $re = $obj_sync->modify_asset($content,$result_vod['nns_asset_import_id']);
                unset($obj_sync);
            }
            echo "<script>alert('" . $re['reason'] . "');</script>";
            break;
        case 'query':
            $result = nl_producer::query_by_cp_id($dc, $_REQUEST['nns_cp_id']);
            echo json_encode($result);die;
        case "unline":
            require_once $nncms_db_path . "nns_assist/nns_db_assist_item_class.php";
            require_once $nncms_db_path . "nns_service/nns_db_service_category_class.php";

            $asset_inst = new nns_db_assist_item_class();
            $service_inst = new nns_db_service_category_class();
            $asset_inst->nns_db_assist_item_unline($nns_id);
            $service_inst->nns_db_service_category_unline($nns_id);

            $asset_inst = null;
            $service_inst = null;
            $nns_names = $log_inst->nns_db_get_video_name_arr($nns_id);
            if ($result["ret"] != 0) {
                echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
            } else {
                $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_names, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
            }
            break;
        case "edit":
            $nns_import_source = $_POST['nns_import_source'];
            if(strlen($nns_import_source) <1)
            {
                echo "<script language=javascript>alert('内容提供商不能为空');history.go(-1);</script>";die;
            }
            $result_producer = nl_producer::query_by_cp_source_id($dc, $nns_cp_id,$nns_import_source);
            if($result_producer['ret'] !=0)
            {
                echo "<script language=javascript>alert('".$result_producer['reason']."');history.go(-1);</script>";die;
            }
            if(!isset($result_producer['data_info']) || empty($result_producer['data_info']) || !is_array($result_producer['data_info']))
            {
                echo "<script language=javascript>alert('查询内容发布商不存在');history.go(-1);</script>";die;
            }
            $vod_info_array['import_source'] = $nns_import_source;
            $vod_info_array['producer'] = $result_producer['data_info']['nns_producer'];
            $nns_pinyin_length = 0;
            //如果是修改电影名称 这样就没修改拼音
            //if (empty($nns_pinyin)) {

            if (mb_strlen($nns_vod_name, 'utf-8') != strlen($nns_pinyin)) {
                $pinyin = nns_controls_pinyin_class::get_pinyin_letter($nns_vod_name);
                $nns_pinyin_length = nns_controls_pinyin_class::$length;
            } else {
                $pinyin = $nns_pinyin;
            }

            $result = $vod_inst->nns_db_vod_modify($nns_id, $nns_vod_name, $nns_view_type, $depot_id, $depot_detail_id, $nns_org_type,
                $nns_org_id, $tag_array, $vod_info_array, $nns_vod_summary, $nns_point, $nns_copyright_date, null, $pinyin,
                $nns_pinyin_length,$nns_cp_id);
            if ($result["ret"] != 0) {
                echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
            } else {
                //$vod_inst->clear_memcache_by_asset_service($nns_id);
                $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_vod_name, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                // nl_vod::delete_vod_info_by_cache($dc, $nns_id);
                //delete_cache("detail_" . $nns_id);
                echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
            }
            break;
        case "add":
            $nns_import_source = $_POST['nns_import_source'];
            if(strlen($nns_import_source) <1)
            {
                echo "<script language=javascript>alert('内容提供商不能为空');history.go(-1);</script>";die;
            }
            $result_producer = nl_producer::query_by_cp_source_id($dc, $nns_cp_id,$nns_import_source);
            if($result_producer['ret'] !=0)
            {
                echo "<script language=javascript>alert('".$result_producer['reason']."');history.go(-1);</script>";die;
            }
            if(!isset($result_producer['data_info']) || empty($result_producer['data_info']) || !is_array($result_producer['data_info']))
            {
                echo "<script language=javascript>alert('查询内容发布商不存在');history.go(-1);</script>";die;
            }
            $pinyin = nns_controls_pinyin_class::get_pinyin_letter($nns_vod_name);
            $nns_pinyin_length = nns_controls_pinyin_class::$length;
            $result = $vod_inst->nns_db_vod_add($nns_vod_name, $nns_view_type, $depot_id, $depot_detail_id, $nns_org_type, $nns_org_id, $tag_array,
                $vod_info_array, $nns_vod_summary, $nns_point, $nns_copyright_date, null, null, $pinyin, $g_video_audit_enabled, $result_producer['data_info']['nns_import_source'],
                $nns_pinyin_length,$nns_cp_id,$result_producer['data_info']['nns_producer']);
            if ($result["ret"] != 0) {
                echo "<script>window.parent.refresh_tree_content();</script>";
                echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
            } else {
                $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_vod_name, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                $nns_id = $result["data"]["id"];
                echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
            }
            break;
        case "delete":
            $delete_bool = true;
            //	$role_inst->
            $nns_ids = explode(",", $nns_id);
            $nns_names = $log_inst->nns_db_get_video_name_arr($nns_id);
            $num = 0;
            foreach ($nns_ids as $nnsid) {
                if (!empty($nnsid)) {
                    $num++;
                    $resultarr = $vod_inst->nns_db_vod_delete($nnsid);
                    if ($resultarr["ret"] != 0 && $num == 1) {
                        $delete_bool = false;
                        break;
                    }
                }
            }
            if (!$delete_bool) {
                echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
            } else {
                $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_names, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
            }
            break;
        case "reset":
            $reset_bool = true;
            //	$role_inst->
            $nns_ids = explode(",", $nns_id);
            $nns_names = $log_inst->nns_db_get_video_name_arr($nns_id);
            $num = 0;
            foreach ($nns_ids as $nnsid) {
                if (!empty($nnsid)) {
                    $num++;
                    $resultarr = $vod_inst->nns_db_vod_retrieve($nnsid);
                    if ($resultarr["ret"] != 0 && $num == 1) {
                        $reset_bool = false;
                        break;
                    }
                }
            }
            if (!$reset_bool) {
                echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
            } else {
                $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_names, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
            }
            break;
        case "audit":
            $audit_bool = true;
            //	$role_inst->
            $nns_ids = explode(",", $nns_id);
            $nns_names = $log_inst->nns_db_get_video_name_arr($nns_id);
            $num = 0;
            foreach ($nns_ids as $nnsid) {
                if (!empty($nnsid)) {
                    $num++;

                    $resultarr = $vod_inst->nns_db_vod_check($nnsid, $audit);

                    if ($resultarr["ret"] != 0 && $num == 1) {
                        $audit_bool = false;
                        break;
                    }
                }
            }
            if (!$audit) {
                $action_str = cms_get_lang('cancel') . $action_str;
            }
            if (!$audit_bool) {
                echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
            } else {
                $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_names, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
            }
            break;
        case "realdelete":
            $delete_bool = true;
            //	$role_inst->
            $nns_ids = explode(",", $nns_id);
            $nns_names = $log_inst->nns_db_get_video_name_arr($nns_id);
            $num = 0;
            foreach ($nns_ids as $nnsid) {
                if (!empty($nnsid)) {
                    $num++;
                    $resultarr = $vod_inst->nns_db_vod_real_delete($nnsid);
                    if ($resultarr["ret"] != 0 && $num == 1) {
                        $delete_bool = false;
                        break;
                    }

                    nl_vod_info_language::delete_vod_info_by_language($dc, $nnsid);
                    nl_vod_index_info_language::delete_vod_index_info_by_language($dc, $nnsid);
                }
            }
            if (!$delete_bool) {
                echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
            } else {
                $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], "real_delete", $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_names, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
            }
            break;
        case "move_category":
            $depot_id = $_POST["move_depot_id"];
            $category_id = $_POST["move_category_id"];
            $delete_bool = true;
            //	$role_inst->
            $nns_ids = explode(",", $nns_id);
            $nns_names = $log_inst->nns_db_get_video_name_arr($nns_id);
            $num = 0;
            foreach ($nns_ids as $nnsid) {
                if (!empty($nnsid)) {
                    $num++;
                    $resultarr = $vod_inst->nns_db_vod_move_depot_category($nnsid, $depot_id, $category_id);
                    if ($resultarr["ret"] != 0 && $num == 1) {
                        $delete_bool = false;
                        break;
                    }
                }
            }
            if (!$delete_bool) {
                echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
            } else {
                $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_names, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
            }
            break;
        //		批量修改EPG标识 BY S67 2013/5/13
        case 'ctag':
            $nns_tag = $_POST["nns_tags"];
            //	$role_inst->
            $nns_ids = explode(",", $nns_id);
            $nns_names = $log_inst->nns_db_get_video_name_arr($nns_id);
            $num = 0;
            $result = $vod_inst->edit_epg_tags($nns_ids, $nns_tag);

            if ($result['ret'] != 0) {
                echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
            } else {
                $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], "edit", $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_names, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
            }
            break;
        //              批量恢复主媒资、分集、片源   删除分集...
        case 'recover':
            $recover_bool = true;
            //	$role_inst->
            $nns_ids = explode(",", $nns_id);
            //恢复媒资类型 主媒资 video 分集 index 片源 media
            $nns_type = (isset($_REQUEST['nns_type']) && in_array($_REQUEST['nns_type'],array('video','index','media'))) ? $_REQUEST['nns_type'] : 'video';
            $nns_queue_action = (isset($_REQUEST['nns_queue_action']) && in_array($_REQUEST['nns_queue_action'],array('add','modify','destroy'))) ? $_REQUEST['nns_queue_action'] : 'modify';
            $nns_names = $log_inst->nns_db_get_video_name_arr($nns_id);
            $num = 0;
            foreach ($nns_ids as $nnsid) {
                if (!empty($nnsid)) {
                    $num++;
                    $resultarr = $vod_inst->nns_db_vod_retrieve($nnsid,$nns_type,$nns_queue_action);
                    if ($resultarr["ret"] != 0 && $num == 1) {
                        $recover_bool = false;
                        break;
                    }
                }
            }
            if (!$recover_bool) {
                echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
            } else {
                if($nns_queue_action=='destroy')
                {
                    $action_str = cms_get_lang('delete|media_db');
                    $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_names, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                    echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
                }
                else
                {
                    $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_names, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                    echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
                }
            }
            break;
        default:

            break;
    }
    //var_dump($result);
    $category_inst = null;
    $log_inst = null;
    if ($action == 'complement' || $action == "delete" || $action == "audit" || $action=='recover' || $action == "reset" || $action == "realdelete" || $action == "move_category" || $action == "unline" || $action == "edit" || $action == 'ctag') {
        if($action=='recover')
        {
            echo "<script language=javascript>history.go(-1);</script>";
        }
        else
        {
            echo "<script>window.parent.refresh_tree_content();</script>";
        }
    } else {
        $langs = g_cms_config::get_g_extend_language();
        if (empty($langs)) {
            echo "<script>self.location='../media_pages/nncms_content_media_list.php?nns_id=" . $nns_id . "&view_type=" . $nns_view_type . "';</script>";
        } else {
            echo "<script>self.location='nncms_content_vod_info_language_extend.php?nns_id=" . $nns_id . "&view_type=" . $nns_view_type . "';</script>";
        }
    }
}
?>
