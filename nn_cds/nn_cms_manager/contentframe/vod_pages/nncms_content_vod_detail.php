<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
ob_start();
include("../../nncms_manager_inc.php");

//福建移动
include ($nncms_config_path . "mgtv/models/category_content_model.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/language_contant.php";
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include_once './nncms_content_vod_dimensions.php';
include LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'vod_info_language.class.php';
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
$dc->open();
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();

$action = $_GET["action"];
$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106104");
$pri_edit = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106101");
$checkpri = null;


if (!$pri_bool) {
        Header("Location: ../nncms_content_wrong.php");
        exit;
}

include ($nncms_db_path . "nns_common/nns_db_constant.php");
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");

include ($nncms_db_path . "nns_vod/nns_db_vod_index_class.php");
include ($nncms_db_path . "nns_vod/nns_db_vod_media_class.php");

// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");


include $nncms_db_path . "nns_vod/nns_db_vod_class.php";
$vod_inst = new nns_db_vod_class();
if (!empty($nns_id)) {
        $vod_info = $vod_inst->nns_db_vod_info($nns_id, "*");
        $vod_inst = null;
} else {
        echo "<script>alert('" . $vod_info["reason"] . "');self.location='nncms_content_vodlist.php';</script>";
}
if ($vod_info["ret"] != 0) {
        echo "<script>alert('" . $vod_info["reason"] . "');self.location='nncms_content_vodlist.php';</script>";
}

$edit_data = $vod_info["data"][0];


/**
 * 废除原TAG方式
 */
//for ($tag_num=0;$tag_num<32;$tag_num++){
//	if ($edit_data["nns_tag".$tag_num]==1){
//		$tar_str.=$tag_num.',';
//	};
//}

$tar_str = ltrim($edit_data['nns_tag'], ',');


//include($nncms_db_path . "nns_keyword/nns_db_keyword_class.php");
//$keyword_model = new nns_db_keyword_class();
//$all_keyword = $keyword_model->get_keyword_by_ids($edit_data['nns_keyword']);
//		$vod_inst=null;
//	$edit_pri=$edit_data["nns_pri_id"];

$vod_indexes_inst = new nns_db_vod_index_class();
$vod_index_inst = new nns_db_vod_media_class();

include $nncms_db_path . "nns_carrier/nns_db_carrier_class.php";
include $nncms_db_path . "nns_partner/nns_db_partner_class.php";
include $nncms_db_path . "nns_group/nns_db_group_class.php";
$carrier_inst = new nns_db_carrier_class();
$partner_inst = new nns_db_partner_class();
$group_inst = new nns_db_group_class();
if ($_SESSION["nns_manager_type"] == 0) {

        $carrier_result = $carrier_inst->nns_db_carrier_list();

        if ($carrier_result["ret"] == 0) {
                $carrier_data = $carrier_result["data"][0];
        }
        if ($action == "edit") {
                //		 $partner_inst=new nns_db_partner_class();
                $partner_result = $partner_inst->nns_db_partner_info($edit_data["nns_org_id"]);

                if ($partner_result["ret"] == 0) {
                        $partner_data = $partner_result["data"][0];
                }

                //		 $group_inst=new nns_db_group_class();
                $group_result = $group_inst->nns_db_group_info($edit_data["nns_org_id"]);

                if ($group_result["ret"] == 0) {
                        $group_data = $group_result["data"][0];
                }
        }
}


//	类型为合作伙伴
if ($_SESSION["nns_manager_type"] == 1) {

        $partner_result = $partner_inst->nns_db_partner_info($_SESSION["nns_org_id"]);

        if ($partner_result["ret"] == 0) {
                $partner_data = $partner_result["data"][0];
        }
        //	类型为集团
} else if ($_SESSION["nns_manager_type"] == 2) {

        $group_result = $group_inst->nns_db_group_info($_SESSION["nns_org_id"]);

        if ($group_result["ret"] == 0) {
                $group_data = $group_result["data"][0];
        }
}
//include $nncms_db_path . "nns_assist/nns_db_assist_item_class.php";
//include $nncms_db_path . "nns_assist/nns_db_assist_class.php";
//include $nncms_db_path . "nns_service/nns_db_service_category_class.php";
//include $nncms_db_path . "nns_service/nns_db_service_class.php";
//  include $nncms_db_path. "nns_service/nns_db_service_detail_video_class.php";

///$assist_inst = new nns_db_assist_class();
//$service_inst = new nns_db_service_class();
$vod_inst = new nns_db_vod_class();
$vod_info = $vod_inst->nns_db_vod_info($nns_id, 'nns_asset_import_id,nns_import_source');
$vod_info = $vod_info['data'][0];

//  $service_item_inst=new nns_db_service_detail_video_class();
//$online1 = $assist_inst->check_video_belong($nns_id);
//$online2 = $service_inst->check_video_belong($nns_id);
//  $online3=$service_item_inst->nns_db_service_detail_video_list(0,0,null,$nns_id,0);
$assist_inst = null;
$service_inst = null;
//  $service_item_inst=null;
if (count($online1["data"]) > 0 || count($online2["data"]) > 0) {
        $is_online = true;
} else {
        $is_online = false;
}


/**
 * 扩展语言 BY S67
 */
$extend_langs_str = g_cms_config::get_g_extend_language();
if (empty($extend_langs_str)) {
        $extend_langs = array();
} else {
        $extend_langs = explode('|', $extend_langs_str);
}

/**
 * 剧照 BY cb
 */
//获取剧照数量，判断是否显示剧照iframe
include LOGIC_DIR . 'stills' . DIRECTORY_SEPARATOR . 'stills.class.php';

$stills_num = nl_stills::count_stills_num($dc, $edit_data['nns_id'], $edit_data['nns_view_type']);
$stills_enabled = g_cms_config::get_g_config_value('g_stills_enabled');



//op=import&nns_id


if($_GET['op']=='import'){
	$media_id = $_GET['nns_media_id'];
	include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
	$queue_task_model = new queue_task_model();
	$queue_task_model->q_add_media_task($media_id);
	echo '<script>alert("操作成功");history.go(-1);</script>';die;
}



ob_end_clean();
function pub_func_get_desc_alt($title,$item)
{
    $alt_html = '<div style="width:300px;padding:5px;"><div style="font-size:16px;font-weight:bold;line-height:22px;"></div>';
    $alt_html.='<div style="font-size:12px;line-height:20px;">' .
        '<div style="float:left;margin:0px 5px 5px 0px;display:block;">';
    $alt_html.='</div>';
    $alt_html.='<p><b>'.$title.'：</b><br/>';
    if(strlen($item) >0)
    {
        $item = json_decode($item,true);
        if(is_array($item))
        {
            foreach ($item as $key=>$val)
            {
                if(is_array($val))
                {
                    $alt_html.="{$key} => <br/>";
                    foreach ($val as $_key=>$_val)
                    {
                        $alt_html.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$_key} => {$_val}<br/>";
                    }
                }
                else
                {
                    $alt_html.="{$key} => {$val}<br/>";
                }
            }
        }
    }
    $alt_html.='</p>';
    $alt_html.='</div></div>';
    return htmlspecialchars($alt_html);
}

?>
<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
                <link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../../js/table.js.php"></script>
                <script language="javascript" src="../../js/checkinput.js.php"></script>
                <script language="javascript" src="../../js/rate.js"></script>
                <script language="javascript" src="../../js/image_loaded_func.js"></script>
                <script language="javascript" src="../../js/cms_cookie.js"></script>
                <script language="javascript">
                
                
                		function add_task_tmp(nns_id){				
							$.get("../sp_content_pages/nncms_content_task.php?ajax=add&sp_id=<?php echo $_GET['sp_id'];?>&nns_id="+nns_id,function(result){					
								alert(result);window.location.reload();
							},'json');
						}
						
						function stop_task_tmp(nns_id){				
							$.get("../sp_content_pages/nncms_content_task.php?ajax=stop&sp_id=<?php echo $_GET['sp_id'];?>&nns_id="+nns_id,function(result){					
								alert(result);window.location.reload();					
							},'json');
						}
						
						
						function del_task(nns_id){				
							$.get("../sp_content_pages/nncms_content_task.php?ajax=del&sp_id=<?php echo $_GET['sp_id'];?>&nns_id="+nns_id,function(result){					
								alert(result);window.location.reload();					
							},'json');
						}
						
						function rsync_info(nns_id){
							$.get("../sp_content_pages/nncms_content_task.php?ajax=rsync&sp_id=<?php echo $_GET['sp_id'];?>&nns_id="+nns_id,function(result){					
								alert(result);window.location.reload();
							},'json');
						}
                		
                		
                        function set_td_max_width() {
                                var len = $(".content_table table tr").length;
                                for (var i = 0; i < len; i++) {
                                        var len1 = $(".content_table table tr:eq(" + i + ")").find("td").length;
                                        var wid = $(document).width() / len1;
                                        $(".content_table table tr:eq(" + i + ")").find("td").css("max-width", wid);
                                }
                        }
                        function gotoDelete(nns_id) {
                                $("#delete_form #nns_id").val(nns_id);
                                checkForm('<?php echo cms_get_lang('dbgl'); ?>', '<?php echo cms_get_lang('msg_ask_delete'); ?>', $('#delete_form'), '');
                        }
                        function gotoDelete_index(nns_id) {
                                $("#delete_index_form #nns_id").val(nns_id);
                                checkForm('<?php echo cms_get_lang('dbgl'); ?>', '<?php echo cms_get_lang('msg_ask_delete'); ?>', $('#delete_index_form'), '');
                        }
                        function gotoRecover(nns_id) {
                                $("#recover_media_form #nns_id").val(nns_id);
                                checkForm('<?php echo cms_get_lang('dbgl'); ?>', '<?php echo '是否进行本次恢复'; ?>', $('#recover_media_form'), '');
                        }
                        function gotoRecover_index(nns_id) {
                                $("#recover_index_form #nns_id").val(nns_id);
                                checkForm('<?php echo cms_get_lang('dbgl'); ?>', '<?php echo '是否进行本次恢复'; ?>', $('#recover_index_form'), '');
                        }
                        function showCDN(id, type) {
                                window.parent.begin_show_cdn(id, type);
                        }

                        function go_back() {
                                if (window.parent.refresh_tree_content) {
                                        window.parent.refresh_tree_content();
                                } else {
                                        history.go(-1);
                                }
                        }

                        function show_extend_lang(num) {

                                if ($(".lang_editor:eq(" + num + ")").is(":hidden") == false) {
                                        $(".lang_editor").hide();
                                        $(".lang_btn").html("<?php echo cms_get_lang('spread'); ?>");
                                } else {

                                        $(".lang_editor").hide();
                                        $(".lang_btn").html("<?php echo cms_get_lang('spread'); ?>");
                                        $(".lang_editor:eq(" + num + ")").show();
                                        $(".lang_btn:eq(" + num + ")").html("<?php echo cms_get_lang('fold'); ?>");
                                }
                                window.parent.checkheight();
                        }

                        function set_show_minor(num) {
                                $obj = $("input[name=moreinfo_chk]:eq(" + num + ")");
                                if ($obj.attr("checked") == "checked") {
                                        $(".extend_" + num).show();
                                } else {
                                        $(".extend_" + num).hide();
                                }
                                window.parent.checkheight();
                        }
                        
                        
                        function show_file(file_id){
                        	$("."+file_id).toggle();
                        }

                        $(document).ready(function() {
                                $(".lang_editor").hide();
                                show_extend_lang(0);
                                var len = $("input[name=moreinfo_chk]").length;
                                for (var i = 0; i < len; i++) {
                                        set_show_minor(i);
                                }
                        });
                </script>
        </head>

<!--<body onload="set_td_max_width();">-->
<body>
<div class="content" id='content_height' style="overflow-y:scroll !important;height: 900px !important;">
    <!--<div class="content_position"><?php echo cms_get_lang('dbgl'); ?> > <?php
        if ($view_type == 0) {
            echo cms_get_lang('media_dy|dbgl');
        } else if ($view_type == 1) {
            echo cms_get_lang('media_lxj|dbgl');
        }
    ?></div>-->

    <div class="content_table">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td class="rightstyle2a"><?php echo cms_get_lang('resource|id'); ?>:</td>
                <td class="rightstyle3"><?php echo $edit_data["nns_id"]; ?></td>
                <td class="rightstyle2a"><?php echo cms_get_lang('media_ywsy'); ?>:</td>
                <td class="rightstyle3"><?php echo $edit_data["nns_eng_name"]; ?></td>
            </tr>
            <tr>
                <td class="rightstyle2a"><?php
                        if ($view_type == 0) {
                            echo cms_get_lang('media_dy|name');
                        } else if ($view_type == 1) {
                            echo cms_get_lang('media_lxj|name');
                        }
                    ?>:
                </td>
                <td class="rightstyle3"><?php echo $edit_data["nns_name"]; ?></td>
                <td class="rightstyle2a"><?php echo cms_get_lang('media_pinyin'); ?>:</td>
                <td class="rightstyle3"><?php echo $edit_data["nns_pinyin"]; ?></td>
            </tr>
            <tr>
                <?php if ($_GET['sp_id']&&$_GET['category_id']){?>
                    <td class="rightstyle2a"><?php echo cms_get_lang('state'); ?>:</td>
                    <td  colspan="3">
                        <?php
                            if ($is_online) {
                                echo "<font style=\"color:#00CC00\">" . cms_get_lang('media_ysx') . "</font>";
                            } else {
                                echo "<font style=\"color:#999999\">" . cms_get_lang('media_wxx') . "</font>";
                            }
                        ?>
                    </td>
                    <?php
                }else{
                    ?>
                    <td class="rightstyle2a"><?php echo cms_get_lang('state'); ?>:</td>
                    <td   class="rightstyle3">
                        <?php
                            if ($is_online) {
                                echo "<font style=\"color:#00CC00\">" . cms_get_lang('media_ysx') . "</font>";
                            } else {
                                echo "<font style=\"color:#999999\">" . cms_get_lang('media_wxx') . "</font>";
                            }
                        ?>
                    </td>
                    <td class="rightstyle2a"><?php echo cms_get_lang('zdgl_zdscbj'); ?>:</td>
                    <td class="rightstyle3">
                        <alt alt="<?php echo get_tag_detail_html($tar_str); ?>"><?php echo $tar_str; ?></alt>
                    </td>
                    <?php
                }
                ?>

            </tr>
            <?php if (count($online1["data"]) > 0) { ?>
                <tr>
                    <td class="rightstyle2a"><?php echo cms_get_lang('assist_owner'); ?>:</td>
                    <td colspan="3">
                        <?php
                            foreach ($online1["data"] as $online1_arr_value) {
                                echo "<span style=\"color:#ff0000\">" . $online1_arr_value["nns_name"] . "</span><span style=\"color:#999999\">(" . $online1_arr_value["nns_assist_id"] . ")</span>&nbsp;&nbsp;&nbsp;&nbsp;";
                            }
                        ?>
                    </td>
                </tr>
            <?php } ?>
            <?php if (count($online2["data"]) > 0 && $g_service_enabled == 1) { ?>
                <tr>
                    <td class="rightstyle2a"><?php echo cms_get_lang('bkgl_owner'); ?>:</td>
                    <td colspan="3">
                        <?php
                            foreach ($online2["data"] as $online2_arr_value) {
                                echo "<span style=\"color:#ff0000\">" . $online2_arr_value["nns_name"] . "</span><span style=\"color:#999999\">(" . $online2_arr_value["nns_service_id"] . ")</span>&nbsp;&nbsp;&nbsp;&nbsp;";
                            }
                        ?>
                    </td>
                </tr>
            <?php } ?>
            <tr class="select_carrier_class">
                <td class="rightstyle2a"><?php echo cms_get_lang('group'); ?>:</td>
                <td class="rightstyle3"><?php
                        if ($edit_data["nns_org_type"] == 0) {
                            echo $carrier_data["nns_name"];
                        } else if ($edit_data["nns_org_type"] == 1) {
                            echo $partner_data["nns_name"];
                        } if ($edit_data["nns_org_type"] == 2) {
                            echo $group_data["nns_name"];
                        }
                    ?>
                </td>
                <td class="rightstyle2a"><?php echo cms_get_lang('media_copyright_date'); ?>:</td>
                <td class="rightstyle3">
                    <?php echo $edit_data["nns_copyright_date"]; ?>
                </td>
            </tr>

                                                <tr>
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('media_tjfs'); ?>:</td>
                                                        <td class="rightstyle3">
                                                                <span class="p_rate" id="p_rate">
                                                                        <i title="1<?php echo cms_get_lang('point'); ?>"></i>
                                                                        <i title="2<?php echo cms_get_lang('point'); ?>"></i>
                                                                        <i title="3<?php echo cms_get_lang('point'); ?>"></i>
                                                                        <i title="4<?php echo cms_get_lang('point'); ?>"></i>
                                                                        <i title="5<?php echo cms_get_lang('point'); ?>"></i>
                                                                </span>
                                                                <script>
                                                                        var Rate = new pRate("p_rate", function() {
                                                                                $("#nns_point").val(Rate.Index);
                                                                        }, true);
                                                                        Rate.setindex(<?php echo $edit_data["nns_point"]; ?>);

                                                                </script>
                                                        </td>
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('media_daoyan'); ?>:</td>
                                                        <td class="rightstyle3">
                                                                <?php echo $edit_data["nns_director"]; ?>
                                                        </td>
                                                </tr>

                                                <tr>
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('media_sysj'); ?>:</td>
                                                        <td class="rightstyle3">
                                                                <?php echo $edit_data["nns_show_time"]; ?>
                                                        </td>
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('media_dq'); ?>:</td>
                                                        <td class="rightstyle3">
                                                                <?php echo $edit_data["nns_area"]; ?>
                                                        </td>
                                                </tr>

                                                <tr>
                                                        <td class="rightstyle2a"><?php
                                                                if ($view_type == 0) {
                                                                        echo cms_get_lang('media_zsc');
                                                                } else if ($view_type == 1) {
                                                                        echo cms_get_lang('media_mjpc');
                                                                }
                                                                ?>:</td>
                                                        <td class="rightstyle3">
                                                                <?php echo ceil($edit_data["nns_view_len"] / 60); ?>
                                                        </td>
                                                        <td class="rightstyle2a"><?php
                                                                if ($view_type == 0) {
                                                                        echo cms_get_lang('media_fps');
                                                                } else if ($view_type == 1) {
                                                                        echo cms_get_lang('media_zjs');
                                                                }
                                                                ?>:</td>
                                                        <td class="rightstyle3">
                                                                <?php echo $edit_data["nns_all_index"]; ?>
                                                        </td>
                                                </tr>
                                                <?php if ($g_key_word_enabled == 1) { ?>
                                                        <tr>
                                                                <td class="rightstyle2a"><?php echo cms_get_lang('keywords'); ?>:</td>
                                                                <td colspan="3"><?php
                                                                        if (is_array($all_keyword)) {
                                                                                foreach ($all_keyword as $keyword) {
                                                                                        echo $keyword['nns_name'] . '，';
                                                                                }
                                                                        }
                                                                        ?>
                                                                </td>
                                                        </tr>
                                                <?php } ?>
                                                <tr>
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('zyk_play_count'); ?>:</td>
                                                        <td class="rightstyle3">
                                                                <?php echo $edit_data["nns_play_count"]; ?>
                                                        </td>
                                                        <td class="rightstyle2a"><?php echo "CP ID"; ?>:</td>
                                                        <td class="rightstyle3">
                                                                <?php echo $edit_data["nns_cp_id"]; ?>
                                                        </td>
                                                </tr>
                                                <tr>
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('media_yy'); ?>:</td>
                                                        <td colspan="3">
                                                                <?php echo $edit_data["nns_actor"]; ?>
                                                        </td>
                                                </tr>
                                                <tr>
                                                        <td class="rightstyle2a"><?php echo "提供商"; ?>:</td>
                                                        <td colspan="3">
                                                                <?php echo $edit_data["nns_producer"]; ?>
                                                        </td>
                                                </tr>
                                                <tr>
                                                        <td class="rightstyle2a">注入来源:</td>
                                                        <td class="rightstyle3">
                                                                <?php echo $vod_info["nns_import_source"]; ?>
                                                        </td>
                                                        <td class="rightstyle2a">注入ID:</td>
                                                        <td class="rightstyle3">
                                                                <?php echo $vod_info["nns_asset_import_id"]; ?>
                                                        </td>
                                                </tr>
                                                <tr>
                                                        <td colspan="4" class="rightstyle2a rightstyle4"><?php echo cms_get_lang('media_bjhb'); ?>:</td>
                                                </tr>

                                                <tr>
                                                        <td colspan="4" class="rightstyle4">
                                                                <div class="img_content">
                                                                        <?php if (!empty($edit_data["nns_image0"])) { ?>
                                                                                <div class="img_view"><h4><?php echo cms_get_lang('media_bjhb') . '(' . cms_get_lang('media_big') . ')' . cms_get_lang('media_slt'); ?></h4>
                                                                                        <a href="<?php if (stripos($edit_data["nns_image0"], 'http://') === false){
                                                                                            echo pub_func_get_image_url($edit_data["nns_image0"]); ?>?rand=<?php echo rand(); }
                                                                                            else { echo $edit_data["nns_image0"];} ?>" target="_blank">
                                                                                                <img src="<?php if (stripos($edit_data["nns_image0"], 'http://') === false){
                                                                                                    echo pub_func_get_image_url($edit_data["nns_image0"]);?>?rand=<?php echo rand(); }
                                                                                                    else { echo $edit_data["nns_image0"];} ?>" style="border:none;" /></a><h4 class="resolution"></h4></div>
                                                                        <?php } ?>

                                                                        <?php if (!empty($edit_data["nns_image1"])) { ?>
                                                                                <div class="img_view"><h4><?php echo cms_get_lang('media_bjhb') . '(' . cms_get_lang('media_normal') . ')' . cms_get_lang('media_slt'); ?></h4>
                                                                                        <a href="<?php if (stripos($edit_data["nns_image1"], 'http://') === false){
                                                                                            echo pub_func_get_image_url($edit_data["nns_image1"]); ?>?rand=<?php echo rand(); }
                                                                                            else { echo $edit_data["nns_image1"];} ?>" target="_blank">
                                                                                                <img src="<?php if (stripos($edit_data["nns_image1"], 'http://') === false){
                                                                                                    echo pub_func_get_image_url($edit_data["nns_image1"]);?>?rand=<?php echo rand(); }
                                                                                                    else { echo $edit_data["nns_image1"];} ?>" style="border:none;" /></a><h4 class="resolution"></h4></div>
                                                                        <?php } ?>
                                                                        <?php if (!empty($edit_data["nns_image2"])) { ?>
                                                                                <div class="img_view"><h4><?php echo cms_get_lang('media_bjhb') . "(" . cms_get_lang('media_small') . ")" . cms_get_lang('media_slt'); ?></h4>
                                                                                        <a href="<?php if (stripos($edit_data["nns_image2"], 'http://') === false){
                                                                                            echo pub_func_get_image_url($edit_data["nns_image2"]); ?>?rand=<?php echo rand(); }
                                                                                            else { echo $edit_data["nns_image2"]; } ?>" target="_blank">
                                                                                                <img src="<?php if (stripos($edit_data["nns_image2"], 'http://') === false){
                                                                                                    echo pub_func_get_image_url($edit_data["nns_image2"]);?>?rand=<?php echo rand(); }
                                                                                                    else {echo $edit_data["nns_image2"]; } ?>" style="border:none;" /></a><h4 class="resolution"></h4></div>
                                                                        <?php } ?>
														</td>
													</tr>
													<tr>
														<td colspan="4" class="rightstyle2a rightstyle4">新版海报:</td>
													</tr>
													<tr>
														<td colspan="4">
															<div class="img_content">
								
																<div class="img_view">
																	<h4><?php echo cms_get_lang('media_bjhb')."(竖图)".cms_get_lang('media_slt');?></h4>
								                                    <?php if(!empty($edit_data["nns_image_v"])){?>
								                                     <a href="<?php if (stripos($edit_data["nns_image_v"], 'http://') === false){
								                                         echo pub_func_get_image_url($edit_data["nns_image_v"]); ?>?rand=<?php echo rand(); }
								                                         else { echo $edit_data["nns_image_v"]; } ?>" target="_blank">
								                                     	<img src="<?php if (stripos($edit_data["nns_image_v"], 'http://') === false){
								                                     	    echo pub_func_get_image_url($edit_data["nns_image_v"]); ?>?rand=<?php echo rand(); }
								                                     	    else { echo $edit_data["nns_image_v"]; } ?>" style="border: none;" />
								                                     </a>
																	<h4 class="resolution"></h4>
								                                    <?php }?>
								                                 </div>
																 <div class="img_view">
																	<h4><?php echo cms_get_lang('media_bjhb')."(横图)".cms_get_lang('media_slt');?></h4>
								                                    <?php if(!empty($edit_data["nns_image_h"])){?>
								                                    <a	href="<?php if (stripos($edit_data["nns_image_h"], 'http://') === false){
								                                        echo pub_func_get_image_url($edit_data["nns_image_h"]);?>?rand=<?php echo rand(); }
								                                        else { echo $edit_data["nns_image_h"]; } ?>" target="_blank">
								                                    	<img src="<?php if (stripos($edit_data["nns_image_h"], 'http://') === false){
								                                    	    echo pub_func_get_image_url($edit_data["nns_image_h"]); ?>?rand=<?php echo rand(); }
								                                    	    else { echo $edit_data["nns_image_h"]; } ?>" style="border: none;" />
								                                    </a>
																	<h4 class="resolution"></h4>
								                                    <?php }?>
								                                 </div>
																 <div class="img_view">
																	<h4><?php echo cms_get_lang('media_bjhb')."(方图)".cms_get_lang('media_slt');?></h4>
								                                    <?php if(!empty($edit_data["nns_image_s"])){?>
								                                    <a href="<?php if (stripos($edit_data["nns_image_s"], 'http://') === false){
								                                        echo pub_func_get_image_url($edit_data["nns_image_s"]); ?>?rand=<?php echo rand(); }
								                                        else { echo $edit_data["nns_image_s"]; } ?>" target="_blank">
																		<img src="<?php if (stripos($edit_data["nns_image_s"], 'http://') === false){
																		    echo pub_func_get_image_url($edit_data["nns_image_s"]); ?>?rand=<?php echo rand(); }
																		    else { echo $edit_data["nns_image_s"]; } ?>" style="border: none;" />
																	</a>
																	<h4 class="resolution"></h4>
								                                    <?php }?>
								                                 </div>
								                              </div>
                                                        </td>
		                                                </tr>
		                                                <tr>
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('media_nrjj'); ?>:</td>
                                                        <td  colspan="3">
                                                                <?php echo $edit_data["nns_summary"]; ?>
                                                        </td>
                                                </tr>
                                                <?php if ($pri_edit) { 
                                                	?>
                                                        
                                                        <tr>
                                                                <td class="rightstyle2a"><?php echo cms_get_lang('action'); ?>:</td>
                                                                <td  colspan="3">
                                                                        <input name="" type="button" value="<?php echo cms_get_lang('media_yp_xx'); ?>" onclick="self.location.href = 'nncms_content_vod_addedit.php?action=edit&view_type=<?php echo $view_type; ?>&nns_id=<?php echo $nns_id; ?>';"  />&nbsp;&nbsp;
                                                                        <input name="" type="button" value="<?php echo cms_get_lang('media_bjhb'); ?>" onclick="self.location.href = 'nncms_content_vod_img_addedit.php?nns_id=<?php echo $nns_id; ?>';" />&nbsp;&nbsp;
                                                                       
  
                                                                </td>
                                                        </tr>
                                                <?php  }?>
                                                <?php if ($stills_enabled && $stills_num > 0) { ?>
                                                        <tr >
                                                                <td colspan="4" style="padding:0px">
                                                                        <div  style="height:200px;">
                                                                                <iframe scrolling="no" frameborder="0" style="width:100%;" height="410" src="../stills_pages/nncms_content_stills_list.php?video_id=<?php echo $edit_data["nns_id"]; ?>&video_type=<?php echo $edit_data['nns_view_type'] ?>"></iframe>
                                                                        </div>
                                                                </td>
                                                        </tr>
                                                <?php } ?>
                                                <!--语言扩展-->
                                                <?php
                                                $num = 0;
                                                foreach ($extend_langs as $extend_lang) {
                                                        $result = nl_vod_info_language::get_vod_info_by_language($dc, $nns_id, $extend_lang, NL_DC_DB);

                                                        if ($result !== FALSE || $result !== TRUE) {
                                                                $data = $result[0];
                                                        } else {
                                                                $data = NULL;
                                                        }
                                                        ?>
                                                        <tr>
                                                                <td colspan="4"   style="padding:0px;">
                                                                        <div class="radiolist" style=" border-bottom:1px solid #86B7CC;">
                                                                                <div class="radiogroup">
                                                                                        <h3 style="  background-color:#DCEEF5;">
                                                                                                <?php
                                                                                                $lang_name = get_language_name_by_code($extend_lang);
                                                                                                echo $lang_name, cms_get_lang('yyb|kz');
                                                                                                ?>
                                                                                                <div style='padding:2px 5px 2px 20px; float:right; background:url(../../images/add_1.png) 0px 50% no-repeat;'>
                                                                                                        <a href='javascript:show_extend_lang(<?php echo $num; ?>);' class="lang_btn"> <?php echo cms_get_lang('spread'); ?></a> </div> </h3>
                                                                                </div>
                                                                        </div>
                                                                </td>
                                                        </tr>
                                                        <tr class='lang_editor'>
                                                                <td colspan="4"   style="padding:0px;">
                                                                        <div class="content_table">
                                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                        <tbody>
                                                                                                <tr>
                                                                                                        <td width="120"><?php echo $lang_name, cms_get_lang('name'); ?>:</td>
                                                                                                        <td><?php echo $data['nns_name']; ?></td>
                                                                                                        <td width="120"><?php echo $lang_name, cms_get_lang('media_daoyan'); ?>:</td>
                                                                                                        <td><?php echo $data['nns_director']; ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                        <td width="120"><?php echo cms_get_lang('media_yy'); ?>:</td>
                                                                                                        <td><?php echo $data['nns_actor']; ?></td>
                                                                                                        <td width="120"><?php echo $lang_name, cms_get_lang('media_dq'); ?>:</td>
                                                                                                        <td><?php echo $data['nns_area']; ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                        <td width="120"><?php echo $lang_name, cms_get_lang('media_index_jj'); ?>:</td>
                                                                                                        <td colspan="2"> <?php echo $data['nns_summary']; ?> </td>
                                                                                                        <td width="120">
                                                                                                                <input  name="moreinfo_chk" id="moreinfo_chk" type="checkbox" onclick="set_show_minor(<?php echo $num; ?>);" /> <font style="color:#0000ff"><?php echo cms_get_lang('gdxx'); ?></font></a>
                                                                                                        </td>
                                                                                                </tr>
                                                                                                <tr class="extend_<?php echo $num; ?>">
                                                                                                        <td width="120"><?php echo $lang_name, cms_get_lang('media_bm'); ?>:</td>
                                                                                                        <td><?php echo $data['nns_alias_name']; ?></td>
                                                                                                        <td width="120"><?php echo $lang_name, cms_get_lang('media_cpr'); ?>:</td>
                                                                                                        <td><?php echo $data['nns_producer']; ?></td>
                                                                                                </tr>

                                                                                                <tr  class="extend_<?php echo $num; ?>">
                                                                                                        <td width="120"><?php echo $lang_name, cms_get_lang('media_bj'); ?>:</td>
                                                                                                        <td><?php echo $data['nns_screenwriter']; ?></td>
                                                                                                        <td width="120"><?php echo $lang_name, cms_get_lang('media_spyz'); ?>:</td>
                                                                                                        <td><?php echo $data['nns_vod_language']; ?></td>
                                                                                                </tr>

                                                                                                <tr  class="extend_<?php echo $num; ?>">
                                                                                                        <td width="120"><?php echo $lang_name, cms_get_lang('media_zm|lang'); ?>:</td>
                                                                                                        <td><?php echo $data['nns_text_language']; ?></td>
                                                                                                        <td width="120"><?php echo $lang_name, cms_get_lang('media_jsby'); ?>:</td>
                                                                                                        <td> <?php echo $data['nns_play_role']; ?> </td>
                                                                                                </tr>

                                                                                        </tbody>
                                                                                </table>
                                                                        </div>
                                                                </td>
                                                        </tr>
                                                        <?php
                                                        $num++;
                                                }
                                                ?>

                                                <!-----多語言結束-------->

                                                <!--分页计算-->
                                                <?php
                                               
                                                if (!empty($_COOKIE["page_max_num"])) {
                                                        $g_manager_list_max_num = $_COOKIE["page_max_num"];
                                                }
                                                else
												{
													$g_manager_list_max_num=12;
												}
                                                $index_pages = ceil($edit_data["nns_all_index"] / $g_manager_list_max_num);
                                                $current_page = $_GET['page'];
                                                if (empty($current_page)) {
                                                        $current_page = 1;
                                                }
                                                $begin_index = ($current_page - 1) * $g_manager_list_max_num + 1; //开始的集数
                                                //$end_index = $index_pages * $g_manager_list_max_num; //结束的集数
                                                $end_index = $begin_index + $g_manager_list_max_num -1; //结束的集数
                                                if ($end_index > $edit_data["nns_all_index"]) {
                                                        $end_index = $edit_data["nns_all_index"];
                                                }
                                                
                                                ?>


                                                <?php
                                                for ($vod_current_index = $begin_index; $vod_current_index <= $end_index; $vod_current_index++) {

                                                        $num = 0;

                    $vod_indexes_info = $vod_indexes_inst->nns_db_vod_index_info($nns_id, $vod_current_index - 1,false);
                    $index_intor_data = $vod_indexes_info["data"];

                    $vod_media_delete_arr = $vod_index_inst->nns_db_vod_media_list(0, 0, $nns_id, $vod_current_index - 1,null,null,  null,  null, 1);
                    if ($vod_media_delete_arr["ret"] == 0) {
                        $vod_media_delete_data = $vod_media_delete_arr["data"];
                    } else {
                        $vod_media_delete_data = null;
                    }
                    if(is_array($index_intor_data) && !empty($index_intor_data)) {
                        $index_un_del = 0;
                        $index_del = 0;
                        foreach ($index_intor_data as $v)
                        {
                            ($v['nns_deleted'] !=0) ? $index_del++ : $index_un_del++;
                        }
                        ?>

                        <tr>
                            <td colspan="4" style="padding:0px;">
                                <h3 style="float: left"> <?php
                                        if ($view_type == 0)
                                        {
                                            echo cms_get_lang('media_fps') . $vod_current_index;
                                        }
                                        else if ($view_type == 1)
                                        {
                                            echo cms_get_lang('media_js') . $vod_current_index;
                                        } ?>
                                </h3>
                                <button style="margin-left: 10px;font-family: Arial, Helvetica, sans-serif;color: white;padding: 0px 10px;border: none;border-radius: 5px;background-color: #7190a4" id="undeleted" onclick="setTab2('undeleted', <?php echo $index_intor_data[0]['nns_index']?>)">未删除(<?php echo $index_un_del?>)</button>
                                <button style="margin-left: 10px;font-family: Arial, Helvetica, sans-serif;color: white;padding: 0px 10px;border: none;border-radius: 5px;background-color: #7190a4" id="deleted" onclick="setTab2('deleted', <?php echo $index_intor_data[0]['nns_index']?>)">已删除(<?php echo $index_del?>)</button>
                                <span style="color: #7ab5d3">点击按钮切换查看不同删除状态的数据,默认显示未删除的</span>
                            </td>
                        </tr>
                        <?php foreach ($index_intor_data as $item) {
                            if ($item['nns_deleted'] == 0) { ?>
                                    <tr class="index_undeleted_<?php echo $item['nns_index']?>">
                                        <td width="120" style="padding-left:20px;border-top:#9d9d9d solid 1px!important;">分集ID:</td>
                                        <td style="border-top:#9d9d9d solid 1px!important;"><?php echo $item["nns_id"]; ?></td>
                                        <td width="120" style="padding-left:20px;border-top:#9d9d9d solid 1px!important;">分集号:</td>
                                        <td style="border-top:#9d9d9d solid 1px!important;"><?php echo $item["nns_index"]; ?> <?php
                                                if(g_cms_config::get_g_config_value('g_seekpoint_enabled')) {
                                                    ?>
                                                    <h3><span style="float:right;margin-right: 20px"><a href="#" onclick="self.location.href = '../../../nn_cms_manager_v2/view/html/point/point_manage.html?id=<?php echo $nns_id; ?>&index=<?php echo $item['nns_index']; ?>&video_name=<?php echo $item['nns_name']; ?>';">打点信息</a></span>
                                                    </h3>
                                                <?php }?></td>
                                    </tr>
                                    <tr class="index_undeleted_<?php echo $item['nns_index']?>">
                                        <td width="120" style="padding-left:20px;"><?php echo cms_get_lang('name'); ?>:</td>
                                        <td><?php echo $item["nns_name"]; ?></td>
                                        <td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_index_sc'); ?>:</td>
                                        <td><?php echo round($item["nns_time_len"] / 60); ?></td>
                                    </tr>
                                    <tr class="index_undeleted_<?php echo $item['nns_index']?>">
                                        <td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_daoyan'); ?>:</td>
                                        <td><?php echo $item["nns_director"]; ?></td>
                                        <td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_yy'); ?>:</td>
                                        <td><?php echo $item["nns_actor"]; ?></td>
                                    </tr>
                                    <tr class="index_undeleted_<?php echo $item['nns_index']?>">
                                        <td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_bjhb'); ?>:</td>
                                        <td>
                                            <div class="img_content">
                                                <?php if ($item["nns_image"] != "") { ?>
                                                    <div class="img_view">
                                                        <a href="<?php if (stripos($item["nns_image"], 'http://') === false){
                                                            echo pub_func_get_image_url($item["nns_image"]);
                                                            ?>?rand=<?php rand(); } else { echo $item["nns_image"];} ?>" target="_blank">
                                                            <img src="<?php if (stripos($item["nns_image"], 'http://') === false){
                                                                echo pub_func_get_image_url($item["nns_image"]);
                                                            } else { echo $item["nns_image"];} ?>" style="border:none;" />
                                                        </a>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_index_jj'); ?>:</td>
                                        <td><?php echo $index_intor_data["nns_summary"]; ?></td>
                                    </tr>
                                    <tr class="index_undeleted_<?php echo $item['nns_index']?>">
                                        <td width="120" style="padding-left:20px;">注入来源:</td>
                                        <td><?php echo $item["nns_import_source"]; ?></td>
                                        <td width="120" style="padding-left:20px;">注入ID:</td>
                                        <td><?php echo $item["nns_import_id"]; ?></td>
                                    </tr>
                                    <tr class="index_undeleted_<?php echo $item['nns_index']?>">
                                        <td width="120" style="padding-left:20px;">删除状态:</td>
                                        <td><font>未删除</font><span style="margin-left: 10px"><a href="javascript:gotoDelete_index('<?php echo $item["nns_id"]; ?>');" target="_self">删除</a></span></td>
                                        <td width="120" style="padding-left:30px;">修改时间:</td>
                                        <td><?php echo $item["nns_modify_time"] ; ?></td>
                                    </tr>
                                    <tr class="index_undeleted_<?php echo $item['nns_index']?>">
                                        <td width="120" style="padding-left:20px;">更新时间:</td>
                                        <td><?php echo date('Y-m-d',strtotime($item["nns_release_time"])); ?></td>
                                        <td width="120" style="padding-left:20px;">CP ID:</td>
                                        <td><?php echo $item["nns_cp_id"]; ?></td>
                                    </tr>

                                    <?php
                                        //                                                        $vod_media_arr = $vod_index_inst->nns_db_vod_media_list(0, 0, $nns_id, $vod_current_index - 1);
                                        $vod_media_arr = $vod_index_inst->nns_db_vod_media_list(0, 0, $nns_id, $vod_current_index - 1,$item['nns_id'],null,  null,  null,-1);

                                        if ($vod_media_arr["ret"] == 0) {
                                            $vod_media_data = $vod_media_arr["data"];
                                        } else {
                                            $vod_media_data = null;
                                        }


                                    ?>
                                    <tr class="index_undeleted_<?php echo $item['nns_index']?>">
                                            <td colspan="4">
                                                <div class="content_table formtable" style="padding:3px 10px; border:0px;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <thead>
                                                        <tr>
                                                            <th><?php echo cms_get_lang('segnumber'); ?></th>
                                                            <th><?php echo cms_get_lang('name'); ?></th>
                                                            <th><?php echo cms_get_lang('media_pydz'); ?></th>
                                                            <th><?php echo cms_get_lang('media_pylx'); ?></th>
                                                            <th><?php echo cms_get_lang('zdgl_zdscbj'); ?></th>
                                                            <th><?php echo cms_get_lang('media_pygs'); ?></th>
                                                            <th><?php echo cms_get_lang('media_mtid'); ?></th>
                                                            <th><?php echo cms_get_lang('media_pyml'); ?></th>
                                                            <th><?php echo "CP ID"; ?></th>
                                                            <th>影片展示类型</th>
                                                            <th>注入信息</th>
                                                            <th>注入时间</th>
                                                            <th width="45px">删除状态</th>
                                                            <th>注入动作</th>
                                                            <th>影片类型</th>
                                                            <th><?php echo cms_get_lang('action'); ?></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                            if ($vod_media_data != null) {
                                                                foreach ($vod_media_data as $item) {
                                                                    $num++;
                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo $num; ?></td>
                                                                        <td>
                                                                            <?php $arr_desc_info = pub_func_get_desc_alt('第三方视频信息:',$item['nns_ext_url']);?>
                                                                            <alt alt="<?php echo $arr_desc_info; ?>">
                                                                                <?php echo $item['nns_name']; ?>
                                                                            </alt>
                                                                        </td>
                                                                        <td><?php echo $item["nns_url"]; ?></td>
                                                                        <td><?php echo get_contant_language($item["nns_mode"]); ?></td>
                                                                        <td><alt alt="<?php echo get_tag_detail_html($item["nns_tag"]); ?>"><?php echo $item["nns_tag"]; ?></td>
                                                                        <td><?php echo $item["nns_filetype"]; ?></td>
                                                                        <td><?php echo $item["nns_content_id"]; ?></td>
                                                                        <td><?php echo $item["nns_kbps"]; ?></td>
                                                                        <td><?php echo $item["nns_cp_id"]; ?></td>
                                                                        <td> <?php echo get_dimensions_type($item['nns_dimensions']); ?> </td>
                                                                        <td><?php echo $item["nns_import_source"]; ?><hr/> <?php echo $item["nns_import_id"]; ?></td>
                                                                        <td><?php echo $item["nns_create_time"]; ?><hr/><?php echo $item["nns_modify_time"]; ?></td>
                                                                        <td><?php if($item['nns_deleted']==1){echo "<font color='#FF0000'><h1>已删除</h1></font>";}else{echo "<font>未删除</font>";} ?></td>
                                                                        <td><?php echo $item["nns_action"];?></td>
                                                                        <td><?php
                                                                                if($item["nns_encode_flag"] == '1')
                                                                                {
                                                                                    echo "<font color='#0033FF'>转码文件[{$item['nns_media_service']}]</font>";
                                                                                }
                                                                                else if($item["nns_encode_flag"] == '2')
                                                                                {
                                                                                    echo "CDN文件[{$item['nns_media_service']}]";
                                                                                }
                                                                                else
                                                                                {
                                                                                    echo "<font color='#00CC00'>原始文件[{$item['nns_media_service']}]</font>";
                                                                                }
                                                                            ?></td>
                                                                        <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                                                                            <?php if ($pri_edit) { ?>
                                                                                <a href="../media_pages/nncms_content_media_addedit.php?view_type=<?php echo $view_type; ?>&action=edit&vod_id=<?php echo $nns_id; ?>&index=<?php echo $vod_current_index - 1; ?>&nns_id=<?php echo $item["nns_id"]; ?>" target="_self"><?php echo cms_get_lang('edit'); ?></a>&nbsp;&nbsp;
                                                                                <?php if($item['nns_deleted']==0){?>
                                                                                    <a href="javascript:gotoDelete('<?php echo $item["nns_id"]; ?>');" target="_self"><?php echo cms_get_lang('delete'); ?></a>&nbsp;&nbsp;
                                                                                    <?php ;} elseif($item['nns_deleted']==1){?>
                                                                                    <a href="javascript:gotoRecover('<?php echo $item["nns_id"]; ?>');" target="_self"><?php echo '恢复'; ?></a>&nbsp;&nbsp;
                                                                                <?php } ?>
                                                                                <a href="javascript:showCDN('<?php echo $item["nns_content_id"]; ?>',0);" target="_self">分发状态</a>&nbsp;&nbsp;
                                                                                <a href="./nncms_content_vod_detail.php?nns_id=<?php echo $_GET['nns_id'];?>&page=<?php echo $_GET['page'];?>&op=import&nns_media_id=<?php echo $item["nns_id"]; ?>">注入指令重发</a>
                                                                            <?php } ?>
                                                                            <?php
                                                                                if (strtolower($item["nns_filetype"]) == "flv") {
                                                                                    $core_npss_url = explode(";", $g_core_npss_url);
                                                                                    $flv_url = "http://" . $core_npss_url[0] . "/nn_vod.flv?id=" . $item["nns_content_id"] . "&nst=iptv";
                                                                                    $flv_url = urlencode($flv_url);
                                                                                    ?>
                                                                                    <a href="../../models/flashplayer/flashplayer.php?vod_url=<?php echo $flv_url; ?>" target="_blank"><?php echo cms_get_lang('media_flv_play'); ?></a>&nbsp;&nbsp;
                                                                                <?php } ?>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }

                                                            if($vod_media_delete_data!=null){
                                                                ?>

                                                                <?php
                                                                foreach ($vod_media_delete_data as $item) {
                                                                    $num++;
                                                                    ?>

                                                                    <tr class="file_<?php echo $vod_current_index;?>" style="display: none;">
                                                                        <td><?php echo $num; ?></td>
                                                                        <td><?php echo $item["nns_name"]; ?></td>
                                                                        <td><?php echo $item["nns_url"]; ?></td>
                                                                        <td><?php echo get_contant_language($item["nns_mode"]); ?></td>
                                                                        <td><alt alt="<?php echo get_tag_detail_html($item["nns_tag"]); ?>"><?php echo $item["nns_tag"]; ?></td>
                                                                        <td><?php echo $item["nns_filetype"]; ?></td>
                                                                        <td><?php echo $item["nns_content_id"]; ?></td>
                                                                        <td><?php echo $item["nns_kbps"]; ?></td>
                                                                        <td> <?php echo get_dimensions_type($item['nns_dimensions']); ?> </td>
                                                                        <td><?php echo $item["nns_import_source"]; ?><br/> <?php echo $item["nns_import_id"]; ?></td>
                                                                        <td><?php echo $item["nns_create_time"]; ?><br/><?php echo $item["nns_modify_time"]; ?></td>
                                                                        <td><?php echo $item["nns_action"];?></td>

                                                                        <td  name="<?php echo cms_get_lang('action'); ?>">

                                                                        </td>
                                                                    </tr>

                                                                    <?php
                                                                }

                                                            }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                <?php } else { ?>
                                    <tr style="display: none" class="index_deleted_<?php echo $item['nns_index']?>">
                                        <td width="120" style="padding-left:20px;border-top:#9d9d9d solid 1px!important;">分集ID:</td>
                                        <td style="border-top:#9d9d9d solid 1px!important;"><?php echo $item["nns_id"]; ?></td>
                                        <td width="120" style="padding-left:20px;border-top:#9d9d9d solid 1px!important;">分集号:</td>
                                        <td style="border-top:#9d9d9d solid 1px!important;"><?php echo $item["nns_index"]; ?><?php
                                                if(g_cms_config::get_g_config_value('g_seekpoint_enabled')) {
                                                    ?>
                                                    <h3><span style="float:right;margin-right: 20px"><a href="#" onclick="self.location.href = '../../../nn_cms_manager_v2/view/html/point/point_manage.html?id=<?php echo $nns_id; ?>&index=<?php echo $item['nns_index']; ?>&video_name=<?php echo $item['nns_name']; ?>';">打点信息</a></span>
                                                    </h3>
                                                <?php }?>
                                        </td>
                                    </tr>
                                    <tr style="display: none" class="index_deleted_<?php echo $item['nns_index']?>">
                                        <td width="120" style="padding-left:20px;"><?php echo cms_get_lang('name'); ?>:</td>
                                        <td><?php echo $item["nns_name"]; ?></td>
                                        <td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_index_sc'); ?>:</td>
                                        <td><?php echo round($item["nns_time_len"] / 60); ?></td>
                                    </tr>
                                    <tr style="display: none" class="index_deleted_<?php echo $item['nns_index']?>">
                                        <td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_daoyan'); ?>:</td>
                                        <td><?php echo $item["nns_director"]; ?></td>
                                        <td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_yy'); ?>:</td>
                                        <td><?php echo $item["nns_actor"]; ?></td>
                                    </tr>
                                    <tr style="display: none" class="index_deleted_<?php echo $item['nns_index']?>">
                                        <td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_bjhb'); ?>:</td>
                                        <td>
                                            <div class="img_content">
                                                <?php if ($item["nns_image"] != "") { ?>
                                                    <div class="img_view">
                                                        <a href="<?php if (stripos($item["nns_image"], 'http://') === false){
                                                            echo pub_func_get_image_url($item["nns_image"]);
                                                            ?>?rand=<?php rand(); } else { echo $item["nns_image"];} ?>" target="_blank">
                                                            <img src="<?php if (stripos($item["nns_image"], 'http://') === false){
                                                                echo pub_func_get_image_url($item["nns_image"]);
                                                            } else { echo $item["nns_image"];} ?>" style="border:none;" />
                                                        </a>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_index_jj'); ?>:</td>
                                        <td><?php echo $item["nns_summary"]; ?></td>
                                    </tr>
                                    <tr style="display: none" class="index_deleted_<?php echo $item['nns_index']?>">
                                        <td width="120" style="padding-left:20px;">注入来源:</td>
                                        <td><?php echo $item["nns_import_source"]; ?></td>
                                        <td width="120" style="padding-left:20px;">注入ID:</td>
                                        <td><?php echo $item["nns_import_id"]; ?></td>
                                    </tr>
                                    <tr style="display: none" class="index_deleted_<?php echo $item['nns_index']?>">
                                        <td width="120" style="padding-left:20px;">删除状态:</td>
                                        <td><font color='#FF0000'>已删除</font><span style="margin-left: 10px"><a href="javascript:gotoRecover_index('<?php echo $item["nns_id"]; ?>');" target="_self">恢复</a></span></td>
                                        <td width="120" style="padding-left:20px;">修改时间:</td>
                                        <td><?php echo $item["nns_modify_time"] ; ?></td>
                                    </tr>
                                    <tr style="display: none" class="index_deleted_<?php echo $item['nns_index']?>">
                                        <td width="120" style="padding-left:20px;">更新时间:</td>
                                        <td><?php echo date('Y-m-d',strtotime($item["nns_release_time"])); ?></td>
                                        <td width="120" style="padding-left:20px;">CP ID:</td>
                                        <td><?php echo $item["nns_cp_id"]; ?></td>
                                    </tr>

                                    <?php
                                        //                                                        $vod_media_arr = $vod_index_inst->nns_db_vod_media_list(0, 0, $nns_id, $vod_current_index - 1);
                                        $vod_media_arr = $vod_index_inst->nns_db_vod_media_list(0, 0, $nns_id, $vod_current_index - 1,$item['nns_id'],null,  null,  null,-1);

                                        if ($vod_media_arr["ret"] == 0) {
                                            $vod_media_data = $vod_media_arr["data"];
                                        } else {
                                            $vod_media_data = null;
                                        }
                                    if (is_array($vod_media_data) && !empty($vod_media_data))
                                    {
                                    ?>

                                    <tr style="display: none" class="index_deleted_<?php echo $item['nns_index']?>">
                                            <td colspan="4">
                                                <div class="content_table formtable" style="padding:3px 10px; border:0px;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <thead>
                                                        <tr>
                                                            <th><?php echo cms_get_lang('segnumber'); ?></th>
                                                            <th><?php echo cms_get_lang('name'); ?></th>
                                                            <th><?php echo cms_get_lang('media_pydz'); ?></th>
                                                            <th><?php echo cms_get_lang('media_pylx'); ?></th>
                                                            <th><?php echo cms_get_lang('zdgl_zdscbj'); ?></th>
                                                            <th><?php echo cms_get_lang('media_pygs'); ?></th>
                                                            <th><?php echo cms_get_lang('media_mtid'); ?></th>
                                                            <th><?php echo cms_get_lang('media_pyml'); ?></th>
                                                            <th><?php echo "CP ID"; ?></th>
                                                            <th>影片展示类型</th>
                                                            <th>注入信息</th>
                                                            <th>注入时间</th>
                                                            <th width="45px">删除状态</th>
                                                            <th>注入动作</th>
                                                            <th>影片类型</th>
                                                            <th><?php echo cms_get_lang('action'); ?></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                            if ($vod_media_data != null) {
                                                                foreach ($vod_media_data as $item) {
                                                                    $num++;
                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo $num; ?></td>
                                                                        <td>
                                                                            <?php $arr_desc_info = pub_func_get_desc_alt('第三方视频信息:',$item['nns_ext_url']);?>
                                                                            <alt alt="<?php echo $arr_desc_info; ?>">
                                                                                <?php echo $item['nns_name']; ?>
                                                                            </alt>
                                                                        </td>
                                                                        <td><?php echo $item["nns_url"]; ?></td>
                                                                        <td><?php echo get_contant_language($item["nns_mode"]); ?></td>
                                                                        <td><alt alt="<?php echo get_tag_detail_html($item["nns_tag"]); ?>"><?php echo $item["nns_tag"]; ?></td>
                                                                        <td><?php echo $item["nns_filetype"]; ?></td>
                                                                        <td><?php echo $item["nns_content_id"]; ?></td>
                                                                        <td><?php echo $item["nns_kbps"]; ?></td>
                                                                        <td><?php echo $item["nns_cp_id"]; ?></td>
                                                                        <td> <?php echo get_dimensions_type($item['nns_dimensions']); ?> </td>
                                                                        <td><?php echo $item["nns_import_source"]; ?><hr/> <?php echo $item["nns_import_id"]; ?></td>
                                                                        <td><?php echo $item["nns_create_time"]; ?><hr/><?php echo $item["nns_modify_time"]; ?></td>
                                                                        <td><?php if($item['nns_deleted']==1){echo "<font color='#FF0000'><h1>已删除</h1></font>";}else{echo "<font>未删除</font>";} ?></td>
                                                                        <td><?php echo $item["nns_action"];?></td>
                                                                        <td><?php
                                                                                if($item["nns_encode_flag"] == '1')
                                                                                {
                                                                                    echo "<font color='#0033FF'>转码文件[{$item['nns_media_service']}]</font>";
                                                                                }
                                                                                else if($item["nns_encode_flag"] == '2')
                                                                                {
                                                                                    echo "CDN文件[{$item['nns_media_service']}]";
                                                                                }
                                                                                else
                                                                                {
                                                                                    echo "<font color='#00CC00'>原始文件[{$item['nns_media_service']}]</font>";
                                                                                }
                                                                            ?></td>
                                                                        <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                                                                            <?php if ($pri_edit) { ?>
                                                                                <a href="../media_pages/nncms_content_media_addedit.php?view_type=<?php echo $view_type; ?>&action=edit&vod_id=<?php echo $nns_id; ?>&index=<?php echo $vod_current_index - 1; ?>&nns_id=<?php echo $item["nns_id"]; ?>" target="_self"><?php echo cms_get_lang('edit'); ?></a>&nbsp;&nbsp;
                                                                                <?php if($item['nns_deleted']==0){?>
                                                                                    <a href="javascript:gotoDelete('<?php echo $item["nns_id"]; ?>');" target="_self"><?php echo cms_get_lang('delete'); ?></a>&nbsp;&nbsp;
                                                                                    <?php ;} elseif($item['nns_deleted']==1){?>
                                                                                    <a href="javascript:gotoRecover('<?php echo $item["nns_id"]; ?>');" target="_self"><?php echo '恢复'; ?></a>&nbsp;&nbsp;
                                                                                <?php } ?>
                                                                                <a href="javascript:showCDN('<?php echo $item["nns_content_id"]; ?>',0);" target="_self">分发状态</a>&nbsp;&nbsp;
                                                                                <a href="./nncms_content_vod_detail.php?nns_id=<?php echo $_GET['nns_id'];?>&page=<?php echo $_GET['page'];?>&op=import&nns_media_id=<?php echo $item["nns_id"]; ?>">注入指令重发</a>
                                                                            <?php } ?>
                                                                            <?php
                                                                                if (strtolower($item["nns_filetype"]) == "flv") {
                                                                                    $core_npss_url = explode(";", $g_core_npss_url);
                                                                                    $flv_url = "http://" . $core_npss_url[0] . "/nn_vod.flv?id=" . $item["nns_content_id"] . "&nst=iptv";
                                                                                    $flv_url = urlencode($flv_url);
                                                                                    ?>
                                                                                    <a href="../../models/flashplayer/flashplayer.php?vod_url=<?php echo $flv_url; ?>" target="_blank"><?php echo cms_get_lang('media_flv_play'); ?></a>&nbsp;&nbsp;
                                                                                <?php } ?>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }

                                                            if($vod_media_delete_data!=null){
                                                                ?>

                                                                <?php
                                                                foreach ($vod_media_delete_data as $item) {
                                                                    $num++;
                                                                    ?>

                                                                    <tr class="file_<?php echo $vod_current_index;?>" style="display: none;">
                                                                        <td><?php echo $num; ?></td>
                                                                        <td><?php echo $item["nns_name"]; ?></td>
                                                                        <td><?php echo $item["nns_url"]; ?></td>
                                                                        <td><?php echo get_contant_language($item["nns_mode"]); ?></td>
                                                                        <td><alt alt="<?php echo get_tag_detail_html($item["nns_tag"]); ?>"><?php echo $item["nns_tag"]; ?></td>
                                                                        <td><?php echo $item["nns_filetype"]; ?></td>
                                                                        <td><?php echo $item["nns_content_id"]; ?></td>
                                                                        <td><?php echo $item["nns_kbps"]; ?></td>
                                                                        <td> <?php echo get_dimensions_type($item['nns_dimensions']); ?> </td>
                                                                        <td><?php echo $item["nns_import_source"]; ?><br/> <?php echo $item["nns_import_id"]; ?></td>
                                                                        <td><?php echo $item["nns_create_time"]; ?><br/><?php echo $item["nns_modify_time"]; ?></td>
                                                                        <td><?php echo $item["nns_action"];?></td>

                                                                        <td  name="<?php echo cms_get_lang('action'); ?>">

                                                                        </td>
                                                                    </tr>

                                                                    <?php
                                                                }

                                                            }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>

                        <?php }}} ?>

                        <?php
                    }
                }
            ?>
            <!--<tr>
<td colspan="2" stype="padding:5px;">
<input name="" type="button" value="<?php echo cms_get_lang('media_bjhb'); ?>" onclick="self.location.href='nncms_content_vod_img_addedit.php?nns_id=<?php echo $nns_id; ?>';"/>&nbsp;&nbsp;
<input name="" type="button" value="<?php echo cms_get_lang('media_control_bjjj'); ?>" onclick="self.location.href='../vod_pages/nncms_content_vod_index_edit.php?nns_id=<?php echo $nns_id; ?>&view_type=<?php echo $view_type; ?>';"/>&nbsp;&nbsp;
                                                <?php if (!$is_online) { ?>
                                                                <input name="" type="button" value="<?php echo cms_get_lang('media_bind_media'); ?>" onclick="self.location.href='../media_pages/nncms_content_media_list.php?nns_id=<?php echo $nns_id; ?>&view_type=<?php echo $view_type; ?>';"/>&nbsp;&nbsp;
                                                <?php } else { ?>
                                                                <input name="" type="button" value="<?php echo cms_get_lang('media_bind_media'); ?>" onclick="self.location.href='../media_pages/nncms_content_media_list.php?nns_id=<?php echo $nns_id; ?>&view_type=<?php echo $view_type; ?>';"/>&nbsp;&nbsp;
                                                <?php } ?>
</td>
</tr>-->
                                        </tbody>
                                </table>
                        </div>
                        <?php if ($pri_edit) { ?>
                                <form id="delete_form" action="../media_pages/nncms_content_media_control.php" method="post">
                                        <input name="action" id="action" type="hidden" value="delete" />
                                        <input name="nns_id" id="nns_id" type="hidden" value="" />
                                        <input name="nns_vod_id" id="nns_vod_id" type="hidden" value="<?php echo $nns_id; ?>" />
                                </form>
                        <?php } ?>
                        <?php if ($pri_edit) { ?>
                                <form id="delete_index_form" action="../vod_pages/nncms_content_vod_control.php" method="post">
                                        <input name="action" id="action" type="hidden" value="recover" />
                                        <input name="nns_id" id="nns_id" type="hidden" value="" />
                                        <input name="nns_type" id="nns_type" type="hidden" value="index"/>
                                        <input name="nns_queue_action" id="nns_queue_action" type="hidden" value="destroy"/>
                                        <input name="nns_vod_id" id="nns_vod_id" type="hidden" value="<?php echo $nns_id; ?>" />
                                </form>
                        <?php } ?>
                        <?php if ($pri_edit) { ?>
                                <form id="recover_media_form" action="../vod_pages/nncms_content_vod_control.php" method="post">
                                        <input name="action" id="action" type="hidden" value="recover" />
                                        <input name="nns_id" id="nns_id" type="hidden" value="" />
                                        <input name="nns_type" id="nns_type" type="hidden" value="media"/>
                                        <input name="nns_vod_id" id="nns_vod_id" type="hidden" value="<?php echo $nns_id; ?>" />
                                </form>
                        <?php } ?>
                        <?php if ($pri_edit) { ?>
                                <form id="recover_index_form" action="../vod_pages/nncms_content_vod_control.php" method="post">
                                        <input name="action" id="action" type="hidden" value="recover" />
                                        <input name="nns_id" id="nns_id" type="hidden" value="" />
                                        <input name="nns_type" id="nns_type" type="hidden" value="index"/>
                                        <input name="nns_vod_id" id="nns_vod_id" type="hidden" value="<?php echo $nns_id; ?>" />
                                </form>
                        <?php } ?>
                        <div class="pagecontrol">
                                <?php 
                                if ($current_page > 1) { ?>
                                        <a href="nncms_content_vod_detail.php?page=1&nns_id=<?php echo $nns_id; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_vod_detail.php?page=<?php echo $current_page - 1; ?>&nns_id=<?php echo $nns_id; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($current_page < $index_pages) { ?>
                                        <a href="nncms_content_vod_detail.php?page=<?php echo $current_page + 1; ?>&nns_id=<?php echo $nns_id; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_vod_detail.php?page=<?php echo $index_pages; ?>&nns_id=<?php echo $nns_id; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $current_page; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_vod_detail.php?ran=1&nns_id=<?php echo $nns_id; ?>',<?php echo $index_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $current_page . "/" . $index_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_prepage_list();"/>&nbsp;&nbsp;
								
                        </div>

                        <div class="controlbtns">
                                <?php if ($pri_edit) { 
                                		if(empty($_GET['sp_id'])){
                                	?>
                                        <div class="controlbtn info"><a href="nncms_content_vod_addedit.php?action=edit&view_type=<?php echo $view_type; ?>&nns_id=<?php echo $nns_id; ?>"><?php echo cms_get_lang('media_yp_xx'); ?></a></div>
                                        <div class="controlbtn haibao"><a href="nncms_content_vod_img_addedit.php?nns_id=<?php echo $nns_id; ?>"><?php echo cms_get_lang('media_bjhb'); ?></a></div>

                <div class="controlbtn fenji"><a  href="../vod_pages/nncms_content_vod_index_edit.php?nns_id=<?php echo $nns_id; ?>&view_type=<?php echo $view_type; ?>"><?php echo cms_get_lang('media_control_bjjj'); ?></a></div>
                <div class="controlbtn media"><a href="../media_pages/nncms_content_media_list.php?nns_id=<?php echo $nns_id; ?>&view_type=<?php echo $view_type; ?>"><?php echo cms_get_lang('media_bind_media'); ?></a></div>
            <?php }} ?>
        <div class="controlbtn back"><a href="javascript:go_back();"><?php echo cms_get_lang('back'); ?></a></div>
        <div style="clear:both;"></div>
    </div>
</div>
<script>
    function setTab2(id,num) {
        if (id =='deleted') {
            $('.index_undeleted_'+ num).hide();
            $('.index_deleted_' + num).show();
        } else {
            $('.index_deleted_' + num).hide();
            $('.index_undeleted_'+ num).show();
        }
    }
</script>
</body>
</html>