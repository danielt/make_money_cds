<?php
/*
 * Created on 2012-11-15
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
  @header("Content-Type:text/html;charset=utf-8");
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
 
 // 获取cache模块
include LOGIC_DIR.'video'.DIRECTORY_SEPARATOR.'vod_info_language.class.php';
include LOGIC_DIR.'log'.DIRECTORY_SEPARATOR.'manager_log.class.php';
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
		));
$dc->open();
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";


 $nns_id=$_POST["vod_id"];

 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"106101");
$checkpri=null;

if (!$pri_bool){
	 Header("Location: ../nncms_content_wrong.php");
}else{
	$names=$_POST['nns_vod_name'];
	$directors=$_POST['nns_director'];
	$actors=$_POST['nns_actor'];
	$areas=$_POST['nns_area'];
	$summarys=$_POST['nns_summary'];
	$alias_names=$_POST['nns_alias_name'];
	$producers=$_POST['nns_producer'];
	$screenwriters=$_POST['nns_screenwriter'];
	$vod_languages=$_POST['nns_vod_language'];
	$text_languages=$_POST['nns_text_language'];
	$play_roles=$_POST['nns_play_role'];
	
	$langs=g_cms_config::get_g_extend_language();
	$langs=explode('|',$langs);
	$num=0;
	foreach ($langs as $lang){
		/*		Array(
		“name”=>点播名称，
		“director”=>导演，
		“actor”=>演员，
		“area”=>上映地区,
		“summary”=>简介,
		“alias_name”=>别名
		“vod_language”=>视频语言,
		“producer”=>出品人
		“screenwriter”=>编剧
		“play_role”=>角色扮演
		“text_language”=>字幕语言
		)*/
		$params=array();
		$params['name']=$names[$num];
		$params['director']=$directors[$num];
		$params['actor']=$actors[$num];
		$params['area']=$areas[$num];
		$params['summary']=$summarys[$num];
		$params['alias_name']=$alias_names[$num];
		$params['vod_language']=$vod_languages[$num];
		$params['producer']=$producers[$num];
		$params['screenwriter']=$screenwriters[$num];
		$params['play_role']=$play_roles[$num];
		$params['text_lang']=$text_languages[$num];
		$result=nl_vod_info_language::modify_vod_info_by_language($dc,$nns_id,$lang,$params);
		if ($result===TRUE){
			$name_result=nl_manager_log::get_names_by_video_id($dc,array($nns_id));
			if ($name_result===TRUE || $name_result===FALSE){
				$nns_name='';
			}else{
				$nns_name=$name_result[$nns_id]['nns_name'];
			}
			nl_manager_log::add($dc,$_SESSION["nns_mgr_name"].cms_get_lang('add|media_yp|dyy|information').":".$nns_name,'edit');
			$bool=true;
			
//			$nns_names=$log_inst->nns_db_get_video_name_arr($nns_id);
//			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],'edit',$_SESSION["nns_mgr_name"]."添加影片多语言信息:".$nns_names,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
		}else{
			$bool=false;
			
		}
		$num++;
	}
	
	if ($bool){
		echo "<script>alert('修改", cms_get_lang('success'), "');</script>";
	}
	else{
		echo "<script>alert('修改", cms_get_lang('fault'), "');</script>";
	}
	echo "<script>self.location='nncms_content_vod_detail.php?nns_id=$nns_id';</script>";
}
?>
