<?php
/*
 * Created on 2012-5-1
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
//ob_start();
error_reporting(E_ALL ^E_NOTICE);
include("../../nncms_manager_inc.php");
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
$view_type = isset($_GET["view_type"])?$_GET["view_type"]:"";
$view_delete = isset($_GET["view_delete"])?$_GET["view_delete"]:'';
$nns_vod_search = isset($_GET["nns_vod_search"])?$_GET["nns_vod_search"]:'';
$view_audit = isset($_GET["view_audit"])?$_GET["view_audit"]:'';
$depot_id = isset($_GET["depot_id"])?$_GET["depot_id"]:'';
$depot_detail_id = isset($_GET["depot_detail_id"])?$_GET["depot_detail_id"]:'';
$view_type_delete = isset($_GET["view_delete_type"])?$_GET["view_delete_type"]:'';


//高级查询
$search_yy = isset($_GET["search_yy"])?$_GET["search_yy"]:'';
$search_dy = isset($_GET["search_dy"])?$_GET["search_dy"]:'';
$search_point = isset($_GET["search_point"])?$_GET["search_point"]:'';
$search_sy_begin_date = isset($_GET["search_sy_begin_date"])?$_GET["search_sy_begin_date"]:'';
$search_sy_end_date = isset($_GET["search_sy_end_date"])?$_GET["search_sy_end_date"]:'';
$search_rk_begin_date = isset($_GET["search_rk_begin_date"])?$_GET["search_rk_begin_date"]:'';
$search_rk_end_date = isset($_GET["search_rk_end_date"])?$_GET["search_rk_end_date"]:'';
$search_copyright_begin_date = isset($_GET["search_copyright_begin_date"])?$_GET["search_copyright_begin_date"]:'';
$search_copyright_end_date = isset($_GET["search_copyright_end_date"])?$_GET["search_copyright_end_date"]:'';


// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
include($nncms_db_path. "nns_vod/nns_db_vod_class.php");
include $nncms_db_path. "nns_common/nns_db_constant.php";

$nns_ids = isset($_POST['nns_ids'])?$_POST['nns_ids']:null;
if(empty($nns_ids)){
	$nns_ids = isset($_GET['nns_ids'])?$_GET['nns_ids']:null;
}
$nns_ids_arr = null;
if($nns_ids){
	$nns_ids_arr = explode(',',$nns_ids);
}

$vod_inst=new nns_db_vod_class();
$vod_array=$vod_inst->nns_db_vod_list("nns_id,nns_name,nns_director,nns_actor,nns_show_time,nns_view_len,nns_all_index,nns_area,nns_image0,nns_image1,nns_image2,nns_summary,nns_copyright_date,nns_point",
	$nns_vod_search,"",$view_audit,$view_type,$depot_id,$depot_detail_id,"","","",$searchinfo,"",0,0,$view_delete,null,null,null,$search_sy_begin_date,$search_sy_end_date,$search_rk_begin_date,$search_rk_end_date,$search_copyright_begin_date,$search_copyright_end_date,$search_point,null,null,null,null,null,null,null,$nns_ids_arr);

$vod_inst=null;
// var_dump($manager_array);
if ($vod_array["ret"]!=0){
	$data=null;
	echo "<script>alert(". $vod_array["reason"].");</script>";
}else{
	$now_time_str=date("YmdHis");
	$csv_tmp_file_name = $depot_detail_id  ."_0_".$now_time_str. ".csv";
	if(!is_dir($nncms_config_path ."data/export_csv/")) {
		$old=umask(0);
		mkdir($nncms_config_path ."data/export_csv/",0777);
		chmod($nncms_config_path ."data/export_csv/",0777);
		touch($csv_tmp_file_name);
		chmod($csv_tmp_file_name,0777);
		umask($old);
	}
	$csv_tmp_file_path = $nncms_config_path ."data/export_csv/".$csv_tmp_file_name ;
	$data=$vod_array["data"];
	require_once "../../controls/nncms_controls_csv_export.php";
	$csv_inst=new nncms_controls_csv_export();
	//$csv_content=$csv_inst->create_csv_content($data);
	$csv_inst->create_csv_content2($data,$csv_tmp_file_path);

	$csv_inst=null;

	require_once $nncms_db_path. "nns_log".DIRECTORY_SEPARATOR . "nns_db_op_log_class.php";
	$log_inst=new nns_db_op_log_class();
	$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],"export",$_SESSION["nns_mgr_name"].$language_export_csv.":".cms_get_lang('lmgl_dblm').$depot_detail_id,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
	$log_inst=null;
	//ob_end_clean();
	// 	header( "content-length: ". strlen( $csv_content ) );
	//	header( "content-type: text/csv" ) ;
	//	header( "cache-control: public" );

	//header("Content-Type: application/force-download;charset=utf-8");//application/octet-stream
	header("Content-Type: application/octet-stream;charset=utf-8");
	header ("Content-Disposition:attachment;filename=". $csv_tmp_file_name);

	$csv_content = file_get_contents($csv_tmp_file_path);
	@unlink($csv_tmp_file_path);
	echo $csv_content;//iconv('UTF-8','GBK',$csv_content);
}
?>