<?php
/*
 * Created on 2012-6-29
 * 从excel导入
 */
header("Content-Type:text/html; charset=UTF-8");
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
//获取权限检查类start	yuliang.xia add  2012/08/20
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106100");
//权限检查类 end

if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");exit;
}

$is_import_success = false;
if(isset($_POST['submit']) && $_SERVER['REQUEST_METHOD']=='POST'){
	//print_r($_FILES);exit;
	if($_FILES['excel_upload']['error']>0) exit($_FILES['excel_upload']['error']);
	//    if($_FILES['excel_upload']['type'] != 'application/vnd.ms-excel') exit('请导入Excel文件');
	require_once dirname(dirname(dirname(__FILE__))).'/models/excel_reader/reader.php';
	// $objPHPExcel = PHPExcel_IOFactory::load($_FILES['excel_upload']['tmp_name']);
	$excel_reader = new Spreadsheet_Excel_Reader();
	$excel_reader->setOutputEncoding('utf-8');//CP936
	$excel_reader->setRowColOffset(0);
	$excel_reader->read($_FILES['excel_upload']['tmp_name']);

	//$sheetData = $objPHPExcel->getActiveSheet()->toArray('',true,true,true);
	$count_sheetData = count( $excel_reader->sheets[0]['numRows']);
	if($count_sheetData>200){
		unlink ($_FILES['excel_upload']['tmp_name']);
		echo '<script>alert("'.cms_get_lang('file_too_big').'");</script>';
		echo "<script>self.location='nncms_content_vod_import.php';</script>";
		exit;
	}
	//	判断管理类型
	$nns_org_id=$_SESSION["nns_org_id"];
	$nns_org_type=$_SESSION["nns_manager_type"];

	include($nncms_db_path. "nns_depot/nns_db_depot_class.php");
	$depot_inst=new nns_db_depot_class();
	$depot_array=$depot_inst->nns_db_depot_list("",$nns_org_type,$nns_org_id,0);
	$depot_id = $depot_array["data"][0]["nns_id"];

	require_once $nncms_db_path. "nns_vod/nns_db_vod_import_class.php";
	$model = new nns_db_vod_import_class();
	$result = $model->save_vod($excel_reader->sheets[0],$nns_org_type,$nns_org_id,$depot_id);
	if($result === true){
		$msg = cms_get_lang('all_import_success').'！';
	}else{
		if(count($sheetData) == count($result)+1){
			$msg = cms_get_lang('import_failed').'！';
		}else{
			$msg = cms_get_lang(part_of_import_success).'！';
		}

	}
	$is_import_success = true;
	unlink ($_FILES['excel_upload']['tmp_name']);

}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>excel导入</title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript">
$(function(){
<?php
if($is_import_success){
?>

	alert("<?php echo $msg;?>");
<?php
}
?>
});

function check_excel_exist(form){
	var excel_files = document.getElementById("excel_upload").value;
	//alert(excel_files);exit;
	if (excel_files==null||excel_files=="") {
		alert('<?php echo cms_get_lang("choose_file");?>');
		return(false);
	}
	return(true);
}
</script>
</head>

<body>

<div class="content">
	<div class="content_position"><?php echo cms_get_lang('media_zykgl'),' > ',cms_get_lang('media_excel|media_playbill_import');?></div>
	<form action="nncms_content_vod_import.php" method="post" id="upload_form" onSubmit="return check_excel_exist(this)" enctype="multipart/form-data">
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">

		<tbody>
		  <tr>
				<td class="rightstyle">  <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('media_excel|gswj');?> </td>
				<td><input name="excel_upload" id="excel_upload" type="file" /><?php echo cms_get_lang('media_excel_not_allow_big');?></td>
		  </tr>

		  </tbody>
		</table>
	</div>

	<div class="controlbtns">
		<div class="controlbtn edit"><input type="submit" name="submit" value="<?php echo cms_get_lang('start|upload');?>" /></div>

		<div style="clear:both;"></div>
	</div>
	</form>
	<?php if($is_import_success && is_array($result)){?>
	<div class="content_position"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font></div>
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">

		<tbody>
<?php if($is_import_success && is_array($result)){

}?>
		  <tr>
				<th> <?php echo cms_get_lang('line_num');?></th><th> <?php echo cms_get_lang('name');?></th><th> <?php echo cms_get_lang('fault|reason');?></th>

		  </tr>
		   <?php foreach($result as $item){?>
		 <tr>
				<td><?php echo $item['index']?> </td>
				<td><?php echo $item['data']['A']?> </td>
				<td><?php echo $item['error']?> </td>

		  </tr>
		  <?php }?>

		  </tbody>
		</table>
	</div>
   <?php }?>
</div>
</body>
</html>