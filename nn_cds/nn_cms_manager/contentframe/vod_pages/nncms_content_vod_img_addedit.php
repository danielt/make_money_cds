<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 ob_start();
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";
include_once $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php"; 

 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();

$nns_id=$_GET["nns_id"];
$pri_bool=false;
$pri_bool1=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"106101");
$pri_bool2=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"106100");
if ($pri_bool1 || $pri_bool2) $pri_bool=true;
$checkpri=null;



if (!$pri_bool){
	 Header("Location: ../nncms_content_wrong.php");
}else{

	 include ($nncms_db_path. "nns_common/nns_db_constant.php");
	 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");

	 
		
	// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
	// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");
		include $nncms_db_path. "nns_vod/nns_db_vod_class.php";
		$vod_inst=new nns_db_vod_class();
		if (!empty($nns_id)){
			$vod_info=$vod_inst->nns_db_vod_info($nns_id,"nns_image0,nns_image1,nns_image2,nns_id,nns_name,nns_view_type");
			$vod_inst=null;
		}else{ 
			echo "<script>alert('".$vod_info["reason"] ."');self.location='nncms_content_vodlist.php';</script>";
		}
		if ($vod_info["ret"]!=0){
			echo "<script>alert('".$vod_info["reason"] ."');self.location='nncms_content_vodlist.php';</script>";
		}
		
		$edit_data=$vod_info["data"][0];
//		$vod_inst=null;
	//	$edit_pri=$edit_data["nns_pri_id"];
	 
ob_end_clean();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/radiolist.js"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>

</head>

<body>

<div class="content">
	<!--<div class="content_position"><?php echo cms_get_lang('dbgl');?> > <?php echo cms_get_lang('media_bjhb');?></div>-->
	<form action="nncms_content_vod_img_control.php" id="add_form" method="post" enctype="multipart/form-data">
	<input name="nns_id" id="nns_id" type="hidden" value="<?php echo $nns_id;?>"/>
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			 <tr>                                                            
				<td width="120"><?php echo cms_get_lang('media_mtid');?></td>
				<td><?php echo $edit_data["nns_id"];?></td>
			</tr>
			  <tr>                                                            
				<td width="120"><?php echo cms_get_lang('media_mtmc');?></td>
				<td><?php echo $edit_data["nns_name"];?></td>
			</tr>
			<tr>                                                            
				<td width="120"><?php echo cms_get_lang('media_mtlx');?></td>
				<td><?php if ($edit_data["nns_view_type"]==0){echo cms_get_lang('media_dy');}else {echo cms_get_lang('media_lxj');}?></td>
			</tr>
			
				
			
			<tr>                                                            
				<td width="120"><?php echo cms_get_lang('media_bjhb')."(".cms_get_lang('media_big').")";?></td>
				<td>
					<div class="img_content">
					 <?php if (!empty($edit_data["nns_image0"])){?>
						<div class="img_view">
						<h4><?php echo cms_get_lang('media_bjhb')."(".cms_get_lang('media_big').")".cms_get_lang('media_slt');?></h4>
						<a href="<?php echo pub_func_get_image_url($edit_data["nns_image0"]);?>?rand=<?php echo rand();?>" target="_blank">
						<img src="<?php echo pub_func_get_image_url($edit_data["nns_image0"]);?>?rand=<?php echo rand();?>" style="border:none;" />
						</a>
						<h4 class="resolution"></h4>
						</div>
					<?php }?>
						<div class="img_edit"> <input name="nns_image0" id="nns_image0" type="file" size="60%"/></div>
						<div style="clear:both;"/>
					</div>
				
			   </td>
				
			</tr>
			<tr>                                                            
				<td width="120"><?php echo cms_get_lang('media_bjhb')."(".cms_get_lang('media_normal').")";?></td>
				<td>
					<div class="img_content">
					 <?php if (!empty($edit_data["nns_image1"])){?>
						<div class="img_view"><h4><?php echo cms_get_lang('media_bjhb')."(".cms_get_lang('media_normal').")".cms_get_lang('media_slt');?></h4>
						<a href="<?php echo pub_func_get_image_url($edit_data["nns_image1"]);?>?rand=<?php echo rand();?>" target="_blank">
						<img src="<?php echo pub_func_get_image_url($edit_data["nns_image1"]);?>?rand=<?php echo rand();?>" style="border:none;" /></a><h4 class="resolution"></h4></div>
					<?php }?>
						<div class="img_edit"> <input name="nns_image1" id="nns_image1" type="file" size="60%"/></div>
						<div style="clear:both;"/>
					</div>
				
			   </td>
				
			</tr>
			<tr>                                                            
				<td width="120"><?php echo cms_get_lang('media_bjhb')."(".cms_get_lang('media_small').")";?></td>
				<td>
					<div class="img_content">
					 <?php if (!empty($edit_data["nns_image2"])){?>
						<div class="img_view"><h4><?php echo cms_get_lang('media_bjhb')."(".cms_get_lang('media_small').")".cms_get_lang('media_slt');?></h4>
						<a href="<?php echo pub_func_get_image_url($edit_data["nns_image2"]);?>?rand=<?php echo rand();?>" target="_blank">
						<img src="<?php echo pub_func_get_image_url($edit_data["nns_image2"]);?>?rand=<?php echo rand();?>" style="border:none;" /></a><h4 class="resolution"></h4></div>
					<?php }?>
						<div class="img_edit"> <input name="nns_image2" id="nns_image2" type="file" size="60%"/></div>
						<div style="clear:both;"/>
					</div>
				
			   </td>
				
			</tr> 
			<!-- 横 竖  方图 -->
			<tr>
                 <td width="120"><?php echo cms_get_lang('media_bjhb')."(竖图)";?></td>
                 <td>
                     <div class="img_content">
                         <?php if (!empty($edit_data["nns_image_v"])){?>
                             <div class="img_view">
                                 <h4><?php echo cms_get_lang('media_bjhb')."(竖图)".cms_get_lang('media_slt');?></h4>
                                 <a href="<?php echo pub_func_get_image_url($edit_data["nns_image_v"]);?>?rand=<?php echo rand();?>" target="_blank">
                                    <img src="<?php echo pub_func_get_image_url($edit_data["nns_image_v"]);?>?rand=<?php echo rand();?>" style="border:none;" />
                                 </a>
                                 <h4 class="resolution"></h4>
                             </div>
                         <?php }?>
                         <div class="img_edit"> <input name="nns_image_v" id="nns_image_v" type="file" size="60%"/></div>
                         <div style="clear:both;"/>
                     </div>

                 </td>

            </tr>
            <tr>
                <td width="120"><?php echo cms_get_lang('media_bjhb')."(横图)";?></td>
                <td>
                    <div class="img_content">
                        <?php if (!empty($edit_data["nns_image_h"])){?>
                            <div class="img_view"><h4><?php echo cms_get_lang('media_bjhb')."(横图)".cms_get_lang('media_slt');?></h4>
                                <a href="<?php echo pub_func_get_image_url($edit_data["nns_image_h"]);?>?rand=<?php echo rand();?>" target="_blank">
                                     <img src="<?php echo pub_func_get_image_url($edit_data["nns_image_h"]);?>?rand=<?php echo rand();?>" style="border:none;" />
                                </a>
                                <h4 class="resolution"></h4></div>
                        <?php }?>
                        <div class="img_edit"> <input name="nns_image_h" id="nns_image_h" type="file" size="60%"/></div>
                        <div style="clear:both;"/>
                    </div>
                </td>
            </tr>
            <tr>
                 <td width="120"><?php echo cms_get_lang('media_bjhb')."(方图)";?></td>
                 <td>
                     <div class="img_content">
                         <?php if (!empty($edit_data["nns_image_s"])){?>
                             <div class="img_view"><h4><?php echo cms_get_lang('media_bjhb')."(方图)".cms_get_lang('media_slt');?></h4>
                                 <a href="<?php echo pub_func_get_image_url($edit_data["nns_image_s"]);?>?rand=<?php echo rand();?>" target="_blank">
                                     <img src="<?php echo pub_func_get_image_url($edit_data["nns_image_s"]);?>?rand=<?php echo rand();?>" style="border:none;" />
                                 </a>
                                 <h4 class="resolution"></h4></div>
                         <?php }?>
                         <div class="img_edit"> <input name="nns_image_s" id="nns_image_s" type="file" size="60%"/></div>
                         <div style="clear:both;"/>
                     </div>
                 </td>
            </tr>
			
		  </tbody>
		</table>
	</div>
	</form>
	 <div class="controlbtns">
		<div class="controlbtn edit"><a href="javascript:checkForm('<?php echo cms_get_lang('dbgl');?>','<?php echo cms_get_lang('msg_ask_change'); ?>',$('#add_form'));"><?php echo cms_get_lang('confirm'); ?></a></div>
		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>





</body>
</html>
<?php }?>
<script language="javascript" src="../../js/image_loaded_func.js"></script>