<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 ob_start();
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/language_contant.php";
include_once $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php";


include LOGIC_DIR.'video'.DIRECTORY_SEPARATOR.'vod_index_info_language.class.php';
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
		));
$dc->open();

 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();

$action=$_GET["action"];
$nns_id=$_GET["nns_id"];
$view_type=$_GET["view_type"];
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"106101");
$checkpri=null;



if (!$pri_bool){
	 Header("Location: ../nncms_content_wrong.php");
}else{

	 include ($nncms_db_path. "nns_common/nns_db_constant.php");
	 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");

	include ($nncms_db_path. "nns_vod/nns_db_vod_index_class.php");

	// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
	// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");


		include $nncms_db_path. "nns_vod/nns_db_vod_class.php";
		$vod_inst=new nns_db_vod_class();
		if (!empty($nns_id)){
			$vod_info=$vod_inst->nns_db_vod_info($nns_id,"*");
			$vod_inst=null;
		}else{
			echo "<script>alert('".$vod_info["reason"] ."');self.location='nncms_content_vodlist.php';</script>";
		}
		if ($vod_info["ret"]!=0){
			echo "<script>alert('".$vod_info["reason"] ."');self.location='nncms_content_vodlist.php';</script>";
		}

		$edit_data=$vod_info["data"][0];
//		$vod_inst=null;
	//	$edit_pri=$edit_data["nns_pri_id"];

	 $vod_indexes_inst=new nns_db_vod_index_class();

	 include $nncms_db_path. "nns_carrier/nns_db_carrier_class.php";
	 include $nncms_db_path. "nns_partner/nns_db_partner_class.php";
		 include $nncms_db_path. "nns_group/nns_db_group_class.php";
		$carrier_inst=new nns_db_carrier_class();
		$partner_inst=new nns_db_partner_class();
		$group_inst=new nns_db_group_class();
	if ($_SESSION["nns_manager_type"]==0){

	 $carrier_result=$carrier_inst->nns_db_carrier_list();

	 if ($carrier_result["ret"]==0){
		$carrier_data=$carrier_result["data"][0];
	 }
	 if ($action=="edit"){
//		 $partner_inst=new nns_db_partner_class();
		 $partner_result=$partner_inst->nns_db_partner_info($edit_data["nns_org_id"]);

		 if ($partner_result["ret"]==0){
			$partner_data=$partner_result["data"][0];
		 }

//		 $group_inst=new nns_db_group_class();
		 $group_result=$group_inst->nns_db_group_info($edit_data["nns_org_id"]);

		 if ($group_result["ret"]==0){
			$group_data=$group_result["data"][0];
		 }
	 }
	}


//	类型为合作伙伴
	if ($_SESSION["nns_manager_type"]==1){

		 $partner_result=$partner_inst->nns_db_partner_info($_SESSION["nns_org_id"]);

		 if ($partner_result["ret"]==0){
			$partner_data=$partner_result["data"][0];
		 }
//	类型为集团
	}else if ($_SESSION["nns_manager_type"]==2){

		 $group_result=$group_inst->nns_db_group_info($_SESSION["nns_org_id"]);

		 if ($group_result["ret"]==0){
			$group_data=$group_result["data"][0];
		 }
	}

	   /**
  * 扩展语言 BY S67
  */
  $extend_langs_str=g_cms_config::get_g_extend_language();
  if (empty($extend_langs_str)) {
	$extend_langs=array();
  }else{
	$extend_langs=explode('|',$extend_langs_str);
  }
	 ob_end_clean();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript">
function show_extend_lang(num){

		if ($(".lang_editor:eq("+num+")").is(":hidden")==false){
			$(".lang_editor").hide();
			$(".lang_btn").html("<?php echo cms_get_lang('spread');?>");
		}else{

			$(".lang_editor").hide();
			$(".lang_btn").html("<?php echo cms_get_lang('spread');?>");
			$(".lang_editor:eq("+num+")").show();
			$(".lang_btn:eq("+num+")").html("<?php echo cms_get_lang('fold');?>");
		}
		window.parent.checkheight();
	}

	function set_show_minor(num){
		$obj=$("input[name=moreinfo_chk]:eq("+num+")");
		if ($obj.attr("checked")=="checked"){
			$(".extend_"+num).show();
		}else{
			$(".extend_"+num).hide();
		}
		window.parent.checkheight();
	}

	$(document).ready(function(){
		$(".lang_editor").hide();
		show_extend_lang(0);
		var len=$("input[name=moreinfo_chk]").length;
		for(var i=0;i<len;i++){
			set_show_minor(i);
		}
	});
</script>
</head>

<body>
<div class="content">
	<!--<div class="content_position"><?php echo cms_get_lang('dbgl');?> > <?php if ($view_type==0){ echo cms_get_lang('media_dy|dbgl');} else if ($view_type==1){echo cms_get_lang('media_lxj|dbgl');}?></div>-->
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td width="120"><?php echo cms_get_lang('media_mtid');?>:</td>
				<td><?php echo $nns_id;?></td>
			</tr>
			  <tr>
				<td width="120"><?php if ($view_type==0){ echo cms_get_lang('media_dy|name');
				}else if ($view_type==1){ echo cms_get_lang('media_lxj|name');}?>:</td>
				<td><?php echo $edit_data["nns_name"];?></td>

			</tr>

			<tr class="select_carrier_class">
				<td width="120"><?php echo cms_get_lang('group');?>:</td>
				<td><?php if ($edit_data["nns_org_type"]==0){
						echo $carrier_data["nns_name"];
					}else if ($edit_data["nns_org_type"]==1){
						echo $partner_data["nns_name"];
					 } if ($edit_data["nns_org_type"]==2){
					 echo $group_data["nns_name"];
					 }?>
				</td>
			</tr>


			<tr>
				<td width="120"><?php echo cms_get_lang('zdgl_zdscbj');?>:</td>
				<td>

						 <?php for($device_num=0; $device_num<$g_manager_list_device_num; $device_num++){

						if ($edit_data["nns_tag". $device_num]==1){echo $device_num."&nbsp;&nbsp;&nbsp;&nbsp;";}

						} ?>
				</td>
			</tr>


<?php
		 if (!empty($_COOKIE["page_max_num"])){
		$g_manager_list_max_num=$_COOKIE["page_max_num"];
			}
		$index_pages=ceil($edit_data["nns_all_index"]/$g_manager_list_max_num);
		$current_page=$_GET['page'];
		if(empty($current_page)){
			$current_page=1;
		}
		$begin_index=($current_page-1)*$g_manager_list_max_num+1;//开始的集数
		$end_index=$current_page*$g_manager_list_max_num; //结束的集数
		if($end_index>$edit_data["nns_all_index"]){
			$end_index=$edit_data["nns_all_index"];
		}

?>


			<?php for($vod_current_index=$begin_index;$vod_current_index<=$end_index;$vod_current_index++){

				$num=0;

		$vod_indexes_info=$vod_indexes_inst->nns_db_vod_index_info($nns_id,$vod_current_index-1);
		$index_intor_data=$vod_indexes_info["data"][0];
		if(is_array($index_intor_data)) {
				?>
			<tr>
				<td colspan="2" style="padding:0px;"><div class="radiolist"><div class="radiogroup"><h3><?php if ($view_type==0){ echo cms_get_lang('media_fp'). $vod_current_index;}else if($view_type==1){ echo cms_get_lang('media_js'). $vod_current_index;}?>
					&nbsp;&nbsp;&nbsp;&nbsp;<a href="###" onclick="self.location='../media_pages/nncms_content_media_intor.php?view_type=<?php echo $view_type;?>&nns_id=<?php echo $nns_id;?>&index=<?php echo $vod_current_index;?>';" style="margin-top:-3px; margin-right: 20px;"><?php echo cms_get_lang('media_control_bjjj');?></a>
					<?php 
					if(g_cms_config::get_g_config_value('g_seekpoint_enabled')) {
					?>
					<a href="###" onclick="self.location='../../../nn_cms_manager_v2/view/html/point/point_manage.html?id=<?php echo $nns_id; ?>&index=<?php echo $index_intor_data['nns_index']; ?>&video_name=<?php echo $index_intor_data['nns_name']; ?>';">打点信息</a>
					<?php }?>
				</h3>
				</div></div></td>
			</tr>

			  <tr >
				<td width="120" style="padding-left:20px;"><?php echo cms_get_lang('name');?>:</td>
				<td><?php echo $index_intor_data["nns_name"];?></td>
			</tr>
			<tr>
				<td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_zsc');?>:</td>
				<td><?php echo intval($index_intor_data["nns_time_len"]/60);?></td>
			</tr>
			 <tr>
				<td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_bjhb');?>:</td>
				<td>
				<div class="img_content">
					<?php if ($index_intor_data["nns_image"]!=""){?>
				  <div class="img_view">
				  <a href="<?php echo pub_func_get_image_url($index_intor_data["nns_image"]);?>?rand=<?php rand();?>" target="_blank">
				  <img src="<?php echo pub_func_get_image_url($index_intor_data["nns_image"]);?>?rand=<?php echo rand();?>"" style="border:none;" /></a></div>
					<?php }?>
					</div>
			   </td>

			</tr>
			<tr>
				<td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_daoyan');?>:</td>
				<td><?php echo $index_intor_data["nns_director"];?></td>

			</tr>
			<tr>
				<td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_yy');?>:</td>
				<td><?php echo $index_intor_data["nns_actor"];?></td>

			</tr>
			<tr>
				<td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_index_jj');?>:</td>
				<td><?php echo $index_intor_data["nns_summary"];?></td>

			</tr>
			
			<tr>
                                                                <td width="120" style="padding-left:20px;">更新时间:</td>
                                                                <td><?php echo date('Y-m-d',strtotime($index_intor_data["nns_release_time"])); ?></td>
                                                                
                                                        </tr>
							<!--语言扩展-->
		<?php
			$num=0;
			foreach ($extend_langs as $extend_lang){
				$result=nl_vod_index_info_language::get_vod_index_info_by_language($dc,$nns_id,$vod_current_index-1,$extend_lang,NL_DC_DB);
				if ($result!==FALSE || $result!==TRUE){
					$data=$result[0];
				} else{
					$data=NULL;
				}
		?>
			<tr>
			<td colspan="4"   style="padding:0px;"><div class="radiolist" style=" border-bottom:1px solid #86B7CC;"><div class="radiogroup">
			<h3 style="  background-color:#DCEEF5;">
				<?php
					$lang_name=get_language_name_by_code($extend_lang);
					echo $lang_name,cms_get_lang('yyb|kz');
				?>
				<div style='padding:2px 5px 2px 20px; float:right; background:url(../../images/add_1.png) 0px 50% no-repeat;'>
				<a href='javascript:show_extend_lang(<?php echo $num;?>);' class="lang_btn"> <?php echo cms_get_lang('spread');?></a></div>
			</h3>

			</div></div></td>
		  </tr>
			<tr class='lang_editor'>
			<td colspan="4"   style="padding:0px;">
				<div class="content_table">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td width="120"><?php echo $lang_name,cms_get_lang('name');?>:</td>
							<td><?php echo $data['nns_name'];?></td>
							  <td width="120"><?php echo $lang_name,cms_get_lang('media_index_jj');?>:</td>
							<td >

									<?php echo $data['nns_summary'];?>
							</td>
						</tr>


					</tbody>
				</table>
				</div>
			</td>
		  </tr>
		  <?php $num++; }?>

		  <!-----多語言結束-------->
			<?php
					} 
			}
			?>

		  </tbody>
		</table>
	</div>

	<!--------------分页器---------------->
		  <div class="pagecontrol">
		<?php if ($current_page>1){?>
		<a href="nncms_content_vod_index_edit.php?page=1&nns_id=<?php echo $nns_id;?>" target="_self"><?php echo cms_get_lang('first_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="nncms_content_vod_index_edit.php?page=<?php echo $current_page-1;?>&nns_id=<?php echo $nns_id;?>" target="_self"><?php echo cms_get_lang('pre_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php }else{?>
			<span><?php echo cms_get_lang('first_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
			<span><?php echo cms_get_lang('pre_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php } if ($current_page<$index_pages){?>
		<a href="nncms_content_vod_index_edit.php?page=<?php echo $current_page+1;?>&nns_id=<?php echo $nns_id;?>" target="_self"><?php echo cms_get_lang('next_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="nncms_content_vod_index_edit.php?page=<?php echo $index_pages;?>&nns_id=<?php echo $nns_id;?>" target="_self"><?php echo cms_get_lang('last_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php }else{?>
			<span><?php echo cms_get_lang('next_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
			<span><?php echo cms_get_lang('last_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php }?>

		<?php echo cms_get_lang('jump_to');?> <input name="go_page_num" id="go_page_num" value="<?php echo $current_page;?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page');?> &nbsp;&nbsp;
		<a href="javascript:go_page_num('nncms_content_vod_index_edit.php?ran=1&nns_id=<?php echo $nns_id;?>',<?php echo $index_pages;?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
		<?php echo cms_get_lang('current');?><span style="font-weight:bold;color:#ff0000;"><?php echo $current_page."/".$index_pages;?></span><?php echo cms_get_lang('page');?>&nbsp;&nbsp;|&nbsp;&nbsp;
		<?php echo cms_get_lang('perpagenum');?>&nbsp;
		 <input name="nns_list_max_num" id="nns_list_max_num" type="text"
		 value="<?php echo $g_manager_list_max_num;?>" style="width:24px;"/>&nbsp;&nbsp;
		 <input type="button" value="<?php echo cms_get_lang('confirm');?>"
		 onclick="refresh_prepage_list();"/>&nbsp;&nbsp;

	</div>



	<div class="controlbtns">
		<div class="controlbtn add"><a href="../media_pages/nncms_content_media_intor.php?nns_id=<?php echo $nns_id;?>&index=1"><?php echo cms_get_lang('add');?></a></div>
		<div class="controlbtn back"><a href="javascript:window.parent.refresh_tree_content();"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>
</body>
</html>
<script language="javascript" src="../../js/image_loaded_func.js"></script>
<?php  }?>