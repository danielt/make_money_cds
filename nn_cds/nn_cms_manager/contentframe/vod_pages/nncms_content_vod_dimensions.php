<?php

include LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'vod_media.class.php';
/*
 * 
 */

function get_dimensions_type($code) {
        global $dimensions_list;
        $key = array_search($code, $dimensions_list);
        if (is_bool($key))
                $key = '';
        return cms_get_lang($key);
}

$dimensions_list = array(
    'DIMENSIONS_2D' => nl_vod_media::DIMENSIONS_2D_DEFAULT,
    'DIMENSIONS_3D' => nl_vod_media::DIMENSIONS_3D_DEFAULT,
    '3D_LEFT_RIGHT' => nl_vod_media::DIMENSIONS_3D_LEFT_RIGHT,
    '3D_UP_DOWN' => nl_vod_media::DIMENSIONS_3D_UP_DOWN,
);
?>