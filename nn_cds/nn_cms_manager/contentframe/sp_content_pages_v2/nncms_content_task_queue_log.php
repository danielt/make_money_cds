<?php

header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类

require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
include_once $nncms_config_path . 'mgtv_v2/models/queue_task_model.php';

$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 15;
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) {
	$page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit = $page_size;
$queue_task_model = new queue_task_model();
$result = $queue_task_model->get_task_queue_log($_GET['sp_id'],$_GET['video_id'],$_GET['video_type'],$_GET['op_id'],$offset, $limit);

$vod_total_num = $result['num'];
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) {
	$page_num = $vod_pages;
}

$currentpage = $page_num;
$g_manager_list_max_num = $page_size;



unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);
$data = $result['data'];

$data_do = $result['data_do'];
?>


<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
		<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/trim.js"></script>
		<script language="javascript">
			$(document).ready(function() {
				$(".closebtn").click(function() {
					window.parent.close_select();
				});
			});
			function check_select_height() {
				$(".selectbox iframe").height($(".selectbox iframe").contents().find(".selectcontrol").height());
				window.parent.check_select_height();
			}
		</script>
	</head>

	<body style='width:800px;'>
		<div class="content selectcontrol" style="padding:0px; margin:0px;">
			<div class="closebtn"><img src="../../images/topicon_08-topicon.png" /></div>
			<div class="content_table formtable">
				
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<th align="left">ID</th>
									<th align="left">名称</th>
									<th align="left">类型</th>
									<th align="left">动作</th>
									<th align="left">状态</th>
									<th align="left">时间</th>
								</tr>
							</thead>
							<tbody>
							<?php
							if($data!=null){
									
							?>
							<tr>
								<td colspan="6">历史执行的...</td>
							</tr>
							<?php
								
								
								foreach ($data as  $value) {
									
								
							?>	
								
							<tr>
								<td><?php echo $value['nns_id'];?></td>
									<td><?php echo $value['nns_name'];?></td>
									<td><?php echo $value['nns_type'];?></td>
									<td><?php echo $value['nns_action'];?></td>
									<td><?php 
									$str = "";
									switch ($value['nns_status']) {
										case 1:
											$str ="执行中";
										break;
										case 2:
											$str ="等待切片";
										break;
										case 3:
											$str ="切片中";
										break;
										case 4:
											$str ="取消";
										break;
										case 5:
											$str ="完成";
										break;
										case 0:
											$str ="等待执行";
										break;
									}
									
									echo $str;
									
									
									?></td>
									<td><?php echo $value['nns_create_time'];?></td>
							</tr>
							
							
							
							<?php
								}
							}
							if($data_do!=null){
							?>
							
							
							<tr>
								<td colspan="6">最近执行的...</td>
							</tr>
							
							<?php
							foreach ($data_do as $do_value) {
							?>
							<tr>
								<td><?php echo $do_value['nns_id'];?></td>
									<td><?php echo $do_value['nns_name'];?></td>
									<td><?php echo $do_value['nns_type'];?></td>
									<td><?php echo $do_value['nns_action'];?></td>
									<td><?php 
										
										$str = "";
									switch ($do_value['nns_status']) {
										case 1:
											$str ="执行中";
										break;
										case 2:
											$str ="等待切片";
										break;
										case 3:
											$str ="切片中";
										break;
										case 4:
											$str ="取消";
										break;
										case 5:
											$str ="完成";
										break;
										case 0:
											$str ="等待执行";
										break;
									}
									
									echo $str;
										
										?></td>
									<td><?php echo $do_value['nns_create_time'];?></td>
							</tr>
							
							<?php
							}
							
							}
							?>
							
							<tr>
								<td colspan="6">
								<div class="pagecontrol">
			  共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_task_queue_log.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_task_queue_log.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_content_task_queue_log.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_task_queue_log.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                        </div>
								</td>
							</tr>
							
							
							
							</tbody>
						</table>
			</div>
			<div style="clear:both;"></div>
		</div>
		</div>
	</body>
</html>
