<?php
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');

include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/language_contant.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107103");
$checkpri = null;
if (!$pri_bool)
        Header("Location: ../nncms_content_wrong.php");

$play_id = $_GET["live_id"];

$begin_date = $_GET["begin_day"];
$end_date = $_GET["end_day"];
$search_name = $_GET["name"];
$language = $_GET["language"];
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");


include_once $nncms_db_path . "nns_common/nns_db_pager_class.php";

include_once LOGIC_DIR . 'playbill' . DIRECTORY_SEPARATOR . 'playbill_item.class.php';
include_once LOGIC_DIR . 'playbill' . DIRECTORY_SEPARATOR . 'playbill_item_info_language.class.php';
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
$dc->open();
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
if (!empty($_COOKIE["page_max_num"])) {
        $g_manager_list_max_num = $_COOKIE["page_max_num"];
}
$page_size = $g_manager_list_max_num;

$currentpage = $_GET["page"];
$currentpage = empty($currentpage) ? 1 : $currentpage;

$params = array();
$params['begin_time'] = $begin_date;
$params['end_time'] = $end_date;
if (!empty($search_name)) {
        $params['name'] = '%' . trim($search_name) . '%';
}

$count = nl_playbill_item::count_search_playbill_item_list($dc, $play_id, $params);
$data = nl_playbill_item::search_playbill_item_list($dc, $play_id, $page_size * ($currentpage - 1), $page_size, $params);

$url = "?";
if($_GET['sp_id']){
	$url.="sp_id=".$_GET['sp_id']."&category_id=". $_GET['category_id'];
}
$url.="live_id=$play_id&begin_day=$begin_date&end_day=$end_date&name=$search_name&language=$language";
$pager = new nns_db_pager_class($count, $page_size, $currentpage, $url);

$current_lang = g_cms_config::get_g_current_language();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../../js/alertbox.js"></script>
                <script language="javascript" src="../../js/table.js.php"></script>
                <script language="javascript" src="../../js/checkinput.js.php"></script>
                <script language="javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
                <script language="javascript" src="../../js/cms_datepicker.js"></script>
                <script language="javascript" src="../../js/cms_cookie.js"></script>
                <script language="javascript" src="../../js/image_loaded_func.js"></script>
                <script language="javascript">
                        function checkhiddenInput() {
                                var par = getAllCheckBoxSelect();
                                $("#action").attr("value", "delete");
                                if (par == "") {

                                        alert("<?php echo cms_get_lang('media_select_jmd'); ?>");

                                } else {
                                        $('#nns_id').attr('value', getAllCheckBoxSelect());
                                        checkForm('<?php echo cms_get_lang('jmdgl'); ?>', '<?php echo cms_get_lang('msg_ask_delete'); ?>', $('#delete_form'));
                                }

                        }
                        
                        
                        function sync_playbill(id,type,cmd,sp){
							$.get("../sp_content_pages/nncms_content_vodlist.php?c2_task=ajax&vod_id="+id+"&type="+type+"&cmd="+cmd+"&sp_id="+sp, function(result) {
								alert(result);window.location.reload();
							}, 'json');
						}

                        function checkhiddenInput_audit(bool) {
                                var par = getAllCheckBoxSelect();
                                $("#action").attr("value", "audit");
                                if (par == "") {
                                        alert("<?php echo cms_get_lang('media_select_jmd'); ?>");
                                } else {
                                        if (bool) {
                                                $("#command").val('0');
                                        } else {
                                                $("#command").val('1');
                                        }
                                        $('#nns_id').attr('value', getAllCheckBoxSelect());
                                        checkForm('<?php echo cms_get_lang('jmdgl'); ?>', '<?php echo cms_get_lang('whether_change_program_state'); ?>', $('#delete_form'));
                                }
                        }

                        function editPlaybill(nnsid) {
                                var url = "./nncms_content_playbill_edit.php?nns_id=" + nnsid
                                window.parent.parent.editPlaybill(url);
                        }


                        function delete_playbill(id) {
                                $("#action").attr("value", "delete");
                                $('#nns_id').attr('value', id);
                                checkForm('<?php echo cms_get_lang('jmdgl'); ?>', '<?php echo cms_get_lang('msg_ask_delete'); ?>', $('#delete_form'));
                        }
                        function audit_playbill(id, bool) {
                                $("#action").attr("value", "audit");
                                if (bool) {
                                        $("#command").val('0');
                                } else {
                                        $("#command").val('1');
                                }
                                $('#nns_id').attr('value', id);
                                checkForm('<?php echo cms_get_lang('jmdgl'); ?>', '<?php echo cms_get_lang('whether_change_program_state'); ?>', $('#delete_form'));
                        }
                        function go_search_playbill() {
                                window.location.href = "nncms_content_playbill_detail_list.php?nns_id=<?php echo $play_id; ?>&nns_begin_date=" + $("#playbill_export_date_begin").val() + "&nns_end_date=" + $("#playbill_export_date_end").val();
                        }

                        function export_current_playbill(live_id, live_date) {
                                $("#export_form #nns_id").val(live_id);
                                $("#export_form #nns_begin_date").val(live_date);
                                $("#export_form #nns_end_date").val(live_date);
                                $("#export_form")[0].submit();
                        }
                </script>
        </head>

        <body>
                <div class="content">

                        <div class="content_table formtable">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <thead>
                                                <tr>
                                                        <th><input name="" type="checkbox" value="" /></th>
                                                        <th><?php echo cms_get_lang('segnumber'); ?></th>
                                                        <th><?php echo cms_get_lang('media_playbill_jmdrq'); ?></th>
                                                        <th><?php echo cms_get_lang('media_playbill_kssj'); ?></th>
                                                        <th><?php echo cms_get_lang('media_playbill_jmmc'); ?></th>
                                                        <th><?php 
                                                        	echo cms_get_lang('state');
														 
                                                        ?></th>
                                                        <th><?php echo cms_get_lang('time_played_with_second'); ?></th>
                                                        

                                                </tr>
                                        </thead>
                                        <tbody>
                                                <?php
                                                if (is_array($data)) {
                                                        $num = ($currentpage - 1) * $page_size;

                                                        foreach ($data as $ikey => $item) {
                                                                
                                                                if (isset($data[$ikey + 1]['nns_begin_time']) && date('Y-m-d H:i:s', strtotime($item['nns_begin_time']) + $item['nns_time_len']) != $data[$ikey + 1]['nns_begin_time']) {
                                                                       $timelen="<font style=color:red>".$item["nns_time_len"]."</font>"; 
                                                                }else{
                                                                           $timelen=$item["nns_time_len"]; 
                                                                }
																
																

                                                                if ($current_lang != $language && !empty($language)) {
                                                                        $result = nl_playbill_item_info_language::get_playbill_item_info_by_language($dc, $item['nns_id'], $language, NL_DC_DB);
                                                                        if (is_array($result)) {
                                                                                $item['nns_name'] = $result[0]['nns_name'];
                                                                                $item['nns_summary'] = $result[0]['nns_summary'];
                                                                        } else {
                                                                                $item['nns_name'] = '';
                                                                                $item['nns_summary'] = '';
                                                                        }
																		
                                                                }
                                                                ?>
                                                                <tr>
                                                                        
                                                                        
                                                                        
                                                                        <td><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>" /></td>
                                                                        <td><?php echo ++$num; ?>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $item["nns_id"]; ?></td>
                                                                        <td><?php echo date('Y-m-d', strtotime($item["nns_begin_time"])); ?></td>
                                                                        <td><?php echo date('H:i:s', strtotime($item["nns_begin_time"])); ?></td>
                                                                        <td>
                                                                                        
                                                                                                <?php 
                                                                                                
                                                                                                echo $item["nns_name"]; 
																								
                                                                                                ?></td>
                                                                        <td><?php
                                                                                if ($item["nns_state"] == '0') {
                                                                                        echo '<span style="color:green">', cms_get_lang('normal'), '</span>';
                                                                                } elseif ($item["nns_state"] == '1') {
                                                                                        echo '<span style="color:red">', cms_get_lang('unrated'), '</span>';
                                                                                }
																				
                                                                                ?>
                                                                                
                                                                                </td>

                                                                        <td><?php echo $timelen; ?></td>

                                                                        

                                                                </tr>
                                                        <?php }
                                                }
                                                ?>
                                                <script language="javascript">
                                                        create_empty_tr($(".formtable"),<?php echo count($data); ?>,<?php echo $nncms_ui_min_list_item_count; ?>);
                                                </script>
                                        </tbody>
                                </table>
                        </div>
<?php echo $pager->nav(); ?>

                </div>
        </body>
</html>
