<?php 
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类

require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';

$sp_id = $_GET['sp_id'];



$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 12;
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) {
	$page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit = $page_size;
$filter = null;
$filter['category_id'] = isset($_GET['category_id']) ?  $_GET['category_id'] : 10000;
$filter['search'] = isset($_GET['search']) ?  $_GET['search'] : '';



$result = video_model::get_vod_list($filter,  $offset, $limit);

$vod_total_num = $result['rows'];
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) {
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;

$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);

$data = $result['data'];



?>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
        <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
        <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
        <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
        <script language="javascript" src="../../js/cms_cookie.js"></script>
        <script language="javascript" src="../../js/table.js.php"></script>
        <script language="javascript" src="../../js/trim.js"></script>
        <script language="javascript" src="../../js/rate.js"></script>
        <script language="javascript" src="../../js/image_loaded_func.js"></script>
        <script language="javascript">
            function get_more_select() {
                return getAllCheckBoxSelect();
            }
        </script>
        <style>
            .content_table {height:320px;  width: 100%;    overflow-y:auto; }
        </style>
    </head>

    <body>
        <div class="content" style="height:383px;">
            <div style="padding:5px;">
                <div class="content_table formtable">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <th><input name="" type="checkbox" value="" /></th>
                                <th>序号</th>
                                <th>名称</th>
                                <th>上映时间</th>
                                <th>区域</th>
                                <th>集数</th>
                            </tr>
                        </thead>
                        <tbody>
                                    
                                    <?php
						if($data!=null){
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) {
								$num++;
						?>
                                    <tr>
                                        <td><input name="input" type="checkbox" value="<?php echo $item['nns_id'];?>" /></td>
                                        <td><?php echo $num;?></td>
                                        <td><?php echo $item['nns_name'];?></td>


                                        <td><?php echo $item['nns_show_time'];?></td>
                                        <td><?php echo $item['nns_area'];?> </td>
                                        <td><?php echo $item['nns_all_index'];?> </td>


                                    </tr>
                                    <?php
							}
						}

						?>
                                    
                                     <script language="javascript">
                                create_empty_tr($(".formtable"),12,12);
                            </script>
                        </tbody>
                    </table>
                </div>
                
                <div class="pagecontrol">
			  共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_controls_video_more_select_list.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_controls_video_more_select_list.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_controls_video_more_select_list.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_controls_video_more_select_list.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_controls_video_more_select_list.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>
                               

                        </div>
                
            <script>
                    var str = "";
                    window.parent.set_adv_search(str);
            </script>
    </body>
</html>