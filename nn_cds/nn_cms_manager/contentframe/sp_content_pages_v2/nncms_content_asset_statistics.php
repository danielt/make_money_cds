<?php header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
$sp_id = $_GET['sp_id'];
$arr = array();
if($_GET['action_search']=='search'){
	//$params['nns_type'] = $_GET['nns_type'];
	//$params['nns_action'] = $_GET['nns_action'];
	//$params['nns_import_type'] = $_GET['nns_import_type'];
	$params['start_time'] = $_GET['day_picker_start'];
	$params['end_time'] = $_GET['day_picker_end'];
	
	$arr = statistics_model::get_statistics($sp_id,$params);
}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
		<script language="javascript">			
			$(document).ready(function() {
				$("#ajax_header").hide();
				$('#clear_time').click(function(){
					$('#day_picker_start').val('');
					$('#day_picker_end').val('');
				});
				
				
				
				
				
				
				
				//window.parent.now_frame_url = window.location.href;
			});
			
			
			
			
			
			
		</script>
	</head>

	<body>
		<div class="content">
			<div class="content_position">
				注入内容统计
			</div>
			<div class="content_table">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<form action="./nncms_content_asset_statistics.php" method="get">

						<tbody>
							<tr>
								<td><!-- &nbsp;&nbsp;&nbsp;&nbsp;类型：&nbsp;&nbsp;
								<select name="nns_type" style="width: 150px;">
									<option value="" >全部</option>
									<option value="video" <?php if($_GET['nns_type']=='video') echo 'selected="selected"'?>>影片</option>
									<option value="index" <?php if($_GET['nns_type']=='index') echo 'selected="selected"'?>>分集</option>
									<option value="media" <?php if($_GET['nns_type']=='media') echo 'selected="selected"'?>>片源</option>
								</select> &nbsp;&nbsp;&nbsp;&nbsp;操作：&nbsp;&nbsp;
								<select name="nns_action" style="width: 150px;">
									<option value="" >全部</option>
									<option value="add" <?php if($_GET['nns_action']=='add') echo 'selected="selected"'?>>添加</option>
									<option value="modify" <?php if($_GET['nns_action']=='modify') echo 'selected="selected"'?>>修改</option>
									<option value="destroy" <?php if($_GET['nns_action']=='destroy') echo 'selected="selected"'?>>删除</option>
								</select> -->
								&nbsp;&nbsp;&nbsp;&nbsp;
								选择时间段：&nbsp;&nbsp;&nbsp;
								<input name="day_picker_start" id="day_picker_start" type="text"  value="<?php
								if (isset($_GET['day_picker_start']))
									echo $_GET['day_picker_start'];
								?>" style="width:120px;" class="datetimepicker" callback="test" />
								-
								<input name="day_picker_end" id="day_picker_end" type="text"  value="<?php
								if (isset($_GET['day_picker_end']))
									echo $_GET['day_picker_end'];
								?>" style="width:120px;" class="datetimepicker" callback="test" />
								
								<input type="button" id="clear_time" name="clear_time" value="清除时间"/>
								<input type="hidden" name="sp_id" value="<?php echo $sp_id; ?>" />
								<input type="hidden" name="action_search" value="search" />
								&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="submit" value="<?php echo cms_get_lang('search'); ?>"   />
								</td>
							</tr>
						</tbody>

					</form>
				</table>
			</div>
			<div class="content_table formtable">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th>媒资类型</th>
							<th>成功</th>
							<th>失败</th>
							<th>等待</th>
							<th>执行。。</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(!empty($arr)){
							foreach ($arr as $item) {
								
							
							?>
						
						<tr>
							<td><?php echo $item['name'];?></td>
							<td><?php echo $item['ok'];?></td>
							<td><?php echo $item['fail'];?></td>
							<td><?php echo $item['wait'];?></td>
							<td><?php echo $item['do'];?></td>
						</tr>
							<?php
							}
						}
						?>
						</tbody>
				</table>
			</div>
		</div>
	</body>
</html>