<?php
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_db_path. "nns_common/nns_db_pager_class.php";
 //加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include_once(dirname(dirname(dirname(dirname(__FILE__)))).DIRECTORY_SEPARATOR."nn_cms_config.php");
include_once(dirname(dirname(dirname(dirname(__FILE__)))).DIRECTORY_SEPARATOR."mgtv_v2".DIRECTORY_SEPARATOR."models".DIRECTORY_SEPARATOR."video_model.php");
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
$model=new video_model();
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"104103");
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");
$dc=nl_get_dc(array("db_policy" => NL_DB_WRITE, "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE));

//获取主媒资、分集、片源的字段信息、描述信息
$video_filed=nl_query_by_db("show full columns from nns_vod", $dc->db());
$index_filed=nl_query_by_db("show full columns from nns_vod_index", $dc->db());
$media_filed=nl_query_by_db("show full columns from nns_vod_media", $dc->db());

if($_REQUEST['sp_id'])
{
    $sp_id=$_REQUEST['sp_id'];
}
if($_GET['op']=='import')
{
    $queue_task_model = new queue_task_model();
    //注入到队列
    $id= explode(',',$_GET['id']);
       foreach ($id as $v)
       {
           if(empty($v))
           {
               continue;
           }
           $sql ="select * from nns_vod where nns_id='$v'";
           $result_type=nl_query_by_db($sql, $dc->db());
           if(!empty($result_type) && is_array($result_type))
           {
               $type='video';
           }
           else
           {
               $sql ="select * from nns_vod_index where nns_id='$v'";
               $result_type=nl_query_by_db($sql, $dc->db());
               if(!empty($result_type) && is_array($result_type))
               {
                   $type='index';
               }
               else
               {
                   $type='media';
               }
           }
           $result=$queue_task_model->q_add_task($v , $type, 'add',$_GET['sp_id'],null,true);
           if($result)
           {
               nn_log::write_log_message(LOG_MODEL_QUEUE,"资源id为:".$_GET['id']." 添加队列成功",$_GET['sp_id'],$_GET['type'],'');
               //echo "<script>alert('注入成功')</script>";
           }
           else
           {
               nn_log::write_log_message(LOG_MODEL_QUEUE,"资源id为:".$_GET['id']." 添加队列失败",$_GET['sp_id'],$_GET['type'],'');
               //echo "<script>alert('注入失败')</script>";
           }
               echo "<script>alert('注入成功')</script>";
       }
}
$where = $_REQUEST;
//媒资
if(!empty($where['nns_video']['key'][0]))
{
    $video='';
    foreach ($where['nns_video']['key'] as $k=>$v)
    {
        if(empty($v)||empty($where['nns_video']['value'][$k]))
        {
            continue;
        }
        if(strstr($where['nns_video']['value'][$k],","))
        {
             $tem_arr=explode(',',$where['nns_video']['value'][$k]);
             $str = '';
             foreach ($tem_arr as $v1)
            {
                $str .= $str ? ",'".$v1."'" : "'".$v1."'";
             }
            $video .=  $video ? " and ".$v . " in (".$str.")" : $v . " in (".$str.")" ;
        }
        else
        {
            if ($where['nns_video']['is_like'][$k])
            {
                $video .= $k > 0 || $video ? " and " . $v . " like '%" . $where['nns_video']['value'][$k] . "%'" : $v . " like '%" . $where['nns_video']['value'][$k] . "%'";
            }
            else
            {
                $video .= $k > 0 || $video ? " and " . $v . " ='" . $where['nns_video']['value'][$k] . "'" : $v . " ='" . $where['nns_video']['value'][$k] . "'";
            }
        }
    }
}
//分集
if(!empty($where['nns_index']['key'][0]))
{
    $index='';
    foreach ($where['nns_index']['key'] as $k=>$v)
    {
        if(empty($v)||empty($where['nns_index']['value'][$k]))
        {
            continue;
        }
        if(strstr($where['nns_index']['value'][$k],","))
        {
            $tem_arr=explode(',',$where['nns_index']['value'][$k]);
            $str = '';
            foreach ($tem_arr as $v1)
            {
                $str .= $str ? ",'".$v1."'" : "'".$v1."'";
            }
            $index .=  $index ? " and ".$v . " in (".$str.")" : $v . " in (".$str.")" ;
        }
        else
        {
            if ($where['nns_index']['is_like'][$k])
            {
                $index .= $k > 0 || $index ? " and" . $v . " like '%" . $where['nns_index']['value'][$k] . "%'" : $v . " like '%" . $where['nns_index']['value'][$k] . "%'";
            }
            else
            {
                $index .= $k > 0 || $index ? " and " . $v . "='" . $where['nns_index']['value'][$k] . "'" : $v . "='" . $where['nns_index']['value'][$k] . "'";
            }
        }
    }
}
//片源
if(!empty($where['nns_media']['key'][0]))
{
    $media='';
    foreach ($where['nns_media']['key'] as $k=>$v)
    {
        if(empty($v)||empty($where['nns_media']['value'][$k]))
        {
            continue;
        }
        if(strstr($where['nns_media']['value'][$k],","))
        {
            $tem_arr=explode(',',$where['nns_media']['value'][$k]);
            $str = '';
            foreach ($tem_arr as $v1)
            {
                $str .= $str ? ",'".$v1."'" : "'".$v1."'";
            }
            $media .=  $media ? " and ".$v . " in (".$str.")" : $v . " in (".$str.")" ;
        }
        else
        {
            if ($where['nns_media']['is_like'][$k])
            {
                $media .= $k > 0 || $media ? " and " . $v . " like '%" . $where['nns_media']['value'][$k] . "%'" : $v . " like '%" . $where['nns_media']['value'][$k] . "%'";
            }
            else
            {
                $media .= $k > 0 || $media ? " and " . $v . "='" . $where['nns_media']['value'][$k] . "'" : $v . "=" . $where['nns_media']['value'][$k] . "'";
            }
        }
    }
}
$result=array();
if($video)
{
 //echo $video;die;
    //查询媒资
    $video_sql = "select * from nns_vod where ".$video." and nns_cp_id='".$where['nns_cp_id']."'";

    $video_result= nl_query_by_db($video_sql, $dc->db());
    if(!empty($video_result) && is_array($video_result))
    {
        foreach ($video_result as $v)
        {
            $arr['nns_id']=$v['nns_id'];
            $arr['nns_type']= 'video';
            $arr['nns_name'] ='['.$v['nns_name'].']';
            $arr['nns_cp_id']=$v['nns_cp_id'];
            $arr['nns_create_time']=$v['nns_create_time'];
            if(!is_exit_op($arr['nns_id'],$arr['nns_type'],$sp_id,$dc))
            {
                $result[]=$arr;
            }
            //查找分集
            $index_sql = $index ? "select * from nns_vod_index where " . $index . " and nns_vod_id='" . $v['nns_id']."' and nns_cp_id='".$where['nns_cp_id']."'":"select * from nns_vod_index where  nns_vod_id='" . $v['nns_id']."' and nns_cp_id='".$where['nns_cp_id']."'";
            //echo $index_sql;die;
            $index_result = nl_query_by_db($index_sql, $dc->db());
            if (!empty($index_result) && is_array($index_result))
            {
                foreach ($index_result as $val)
                {
                    $arr['nns_id']=$val['nns_id'];
                    $arr['nns_type']= 'index';
                    $arr['nns_name'] ='['.$v['nns_name'].']['.$val['nns_name'].']';
                    $arr['nns_cp_id']=$val['nns_cp_id'];
                    $arr['nns_create_time']=$val['nns_create_time'];
                    if(!is_exit_op($arr['nns_id'],$arr['nns_type'],$sp_id,$dc))
                    {
                        $result[]=$arr;
                    }
                    //查找片源
                    $media_sql = $media ? "select * from nns_vod_media where " . $media . " and nns_vod_id='" . $v['nns_id'] . "' and nns_vod_index_id='".$val['nns_id'] ."' and nns_cp_id='".$where['nns_cp_id']."'" :"select * from nns_vod_media where  nns_vod_id='" . $v['nns_id'] . "' and nns_vod_index_id='".$val['nns_id'] ."' and nns_cp_id='".$where['nns_cp_id']."'";
                    //echo $media_sql;die;
                    $media_result = nl_query_by_db($media_sql, $dc->db());
                    //var_dump($media_result);die;
                    if (!empty($media_result) && is_array($media_result))
                    {
                        foreach ($media_result as $value)
                        {
                            $arr['nns_id'] = $value['nns_id'];
                            $arr['nns_type'] = 'media';
                            $arr['nns_name'] = '['.$v['nns_name'].']['.$val['nns_name'].']['.$value['nns_kbps'].'/kbps]'.$value['nns_mode'];
                            $arr['nns_cp_id'] = $value['nns_cp_id'];
                            $arr['nns_create_time'] = $value['nns_create_time'];
                            if(!is_exit_op($arr['nns_id'],$arr['nns_type'],$sp_id,$dc))
                            {
                                $result[]=$arr;
                            }
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            else
            {
                continue;
            }
        }
    }
}
include_once($nncms_config_path.'nn_logic/nl_common.func.php');

if(!empty($_REQUEST['view_list_max_num'])){
	$g_manager_list_max_num=$_REQUEST['view_list_max_num'];
	setcookie("page_max_num", $g_manager_list_max_num);
}else{
	if (!empty($_COOKIE["page_max_num"])){
		$g_manager_list_max_num=$_COOKIE["page_max_num"];
	}
}
$total_num= count($result);
$pages=ceil($total_num/$g_manager_list_max_num);
$currentpage=1;
if (!empty($_GET["page"])) $currentpage=$_GET["page"];
$ad_page_template_inst = null;
$limit=array();
$limit['left']=($currentpage-1)*$g_manager_list_max_num;
$limit['right']=$g_manager_list_max_num;

$result_cp = nl_cp::query_all($dc);
if($result_cp['ret'] != 0)
{
    echo '<script>alert("'.$result_cp['reason'].'");history.go(-1);</script>';die;
}
$result_cp = isset($result_cp['data_info']) ? $result_cp['data_info'] : null;
$arr_cp_info = array();
if(is_array($result_cp) && !empty($result_cp))
{
    foreach ($result_cp as $val)
    {
        $arr_temp_config = isset($val['nns_config'])?json_decode($val['nns_config'],true):null;
        $arr_cp_info[$val['nns_id']] = $val['nns_name'];
    }
}
//判断是否存在
function is_exit_op($id,$type,$sp_id,$dc)
{
    $bool=false;
    if($type=='video')
    {
        $sql = "select * from nns_mgtvbk_op_log where nns_video_id='$id' and nns_type='$type' and nns_org_id='$sp_id'";
    }
    else if($type=='index')
    {
        $sql = "select * from nns_mgtvbk_op_log where nns_index_id='$id' and nns_type='$type' and nns_org_id='$sp_id'";
    }
    else if($type=='media')
    {
        $sql = "select * from nns_mgtvbk_op_log where nns_media_id='$id' and nns_type='$type' and nns_org_id='$sp_id'";
    }
    $result=nl_query_by_db($sql, $dc->db());
    if (!empty($result) && is_array($result))
    {
        $bool=true;
    }
    return $bool;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="../../js/calendar/jscal2.css" type="text/css" />
<link rel="stylesheet" href="../../js/calendar/border-radius.css" type="text/css" />
<link rel="stylesheet" href="../../js/calendar/win2k.css" />
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script type="text/javascript" charset="utf-8" src="../../js/calendar/calendar.js"></script>
<script type="text/javascript" src="../../js/calendar/calendar.lang.js.php"></script>
<script type="text/javascript" charset="utf-8" src="../../js/report_select_time_bar.js"></script>
<script type="text/javascript" charset="utf-8" src="../../js/tabs.js"></script>
<script type="text/javascript" charset="utf-8" src="../../js/cms_cookie.js"></script>
<script language="javascript">

$(function(){
	$('clear_time').click(function(){
		//$('#begin_time').val('');
		//$('#begin_time').val('');
	});
});
function ajax(){

	url="nncms_content_vod_select.php"+;
	window.location.href=url;
}
</script>
</head>   
<body>
<div class="content" >
	<div class="content_position"> > 媒资筛选</div>
	<form id="tag_form" action="nncms_content_vod_select.php" method="post" >
    <input type="hidden" name="sp_id" value="<?php echo $sp_id ?>">
        <div class="content_table">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody class="tr_tbody">
        <tr>
            <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>CP ID:</td>
            <td>
                <select id="nns_cp_id" name='nns_cp_id' style="width:550px;">
                    <?php if(is_array($arr_cp_info) && !empty($arr_cp_info)){foreach ($arr_cp_info as $cp_key=>$cp_val){?>
                        <option value="<?php echo $cp_key;?>" <?php if(isset($edit_data['nns_cp_id']) && $edit_data['nns_cp_id'] == $cp_key){ echo 'selected="selected"';}?>><?php echo $cp_key.' / '.$cp_val;?></option>
                        <?php }}?>
                </select>
            </td>
        </tr>
        <tr><td class="rightstyle">主媒资:(多个值用逗号隔开)</td></tr>
        <tr class='nns_video_value'>
            <td class="rightstyle"></td>
            <td>
                &nbsp;&nbsp;键:&nbsp;&nbsp;<select id="nns_video[key]" name='nns_video[key][]' style="width:550px;">
                <?php if(is_array($video_filed) && !empty($video_filed)){foreach ($video_filed as $key=>$val){?>
                    <?php if($val['Comment']){ ?>
                    <option value="<?php echo $val['Field'];?>"><?php echo $val['Comment'];?></option>
                        <?php } ?>
                <?php }}?>
                </select>
                &nbsp;&nbsp;值:&nbsp;&nbsp;<input type="text" name="nns_video[value][]" value="<?php echo $nns_video['value'];?>" style="width:200px;" />
                &nbsp;&nbsp;是否精确查找:&nbsp;&nbsp;<input type="text" name="nns_video[is_like][]" value="" style="width:200px;" placeholder="1 精确查找"/>
                &nbsp;&nbsp;<input type="button" data_type="video" value="删除" id="button_delete" />
            </td>
        </tr>
        <tr>
            <td class="rightstyle"></td>
            <td>
                &nbsp;&nbsp;<a href="javascript:add_config('video');">+添加选项+</a>
            </td>
        </tr>

        <tr><td class="rightstyle">分集:(多个值用逗号隔开)</td></tr>
        <tr class='nns_index_value'>
            <td class="rightstyle"></td>
            <td>
                &nbsp;&nbsp;键:&nbsp;&nbsp;<select id="nns_index[key]" name='nns_index[key][]' style="width:550px;">
                    <?php if(is_array($index_filed) && !empty($index_filed)){foreach ($index_filed as $key=>$val){?>
                        <?php if($val['Comment']){ ?>
                        <option value="<?php echo $val['Field'];?>"><?php echo $val['Comment'];?></option>
                            <?php } ?>
                    <?php }}?>
                </select>
                &nbsp;&nbsp;值:&nbsp;&nbsp;<input type="text" name="nns_index[value][]" value="<?php echo $nns_index['value'];?>" style="width:200px;" />
                &nbsp;&nbsp;是否精确查找:&nbsp;&nbsp;<input type="text" name="nns_index[is_like][]" value="" style="width:200px;" placeholder="1 精确查找"/>
                &nbsp;&nbsp;<input type="button" data_type="index" value="删除" id="button_delete" />
            </td>
        </tr>
        <tr>
            <td class="rightstyle"></td>
            <td>
                &nbsp;&nbsp;<a href="javascript:add_config('index');">+添加选项+</a>
            </td>
        </tr>
        <tr><td class="rightstyle">片源:(多个值用逗号隔开)</td></tr>
        <tr class='nns_media_value'>
            <td class="rightstyle"></td>
            <td>
                &nbsp;&nbsp;键:&nbsp;&nbsp;<select id="nns_media[key]" name='nns_media[key][]' style="width:550px;">
                    <?php if(is_array($media_filed) && !empty($media_filed)){foreach ($media_filed as $key=>$val){?>
                        <?php if($val['Comment']){ ?>
                        <option value="<?php echo $val['Field'];?>"><?php echo $val['Comment'];?></option>
                            <?php }?>
                    <?php }}?>
                </select>
                &nbsp;&nbsp;值:&nbsp;&nbsp;<input type="text" name="nns_media[value][]" value="<?php echo $nns_media['value'];?>" style="width:200px;" />
                &nbsp;&nbsp;是否精确查找:&nbsp;&nbsp;<input type="text" name="nns_media[is_like][]" value="" style="width:200px;" placeholder="1 精确查找"/>&nbsp;&nbsp;
                <input type="button" data_type="index" value="删除" id="button_delete" />
            </td>
        </tr>
        <tr>
            <td class="rightstyle"></td>
            <td>
                &nbsp;&nbsp;<a href="javascript:add_config('media');">+添加选项+</a>
            </td>
        </tr>
        <tr>
            <td>
        <input style="float:right;margin-top:-2px" type="submit" value="<?php echo cms_get_lang('search');?>" onclick="ajax()">
            </td>
        </tr>
        </tbody>
        </table>
    </div>
    </form>
	<div class="content_table formtable">	
	</div>
    <div class="content_table formtable">
    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <th><input name="" type="checkbox" value="">序号</th>
                <th>资源id</th>
                <th>名称</th>
                <th>类型</th>
                <th>CP_ID</th>
                <th>创建时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody class="db_info">
            <?php
            foreach ($result as $v){
            ?>
                <tr>
                <tr>
                    <td id="is_delete"><input name="input" type="checkbox" value="<?php echo $v['nns_id']; ?>"></td>
                    <td><?php echo $v['nns_id'] ?></td>
                    <td><?php echo $v['nns_name'] ?></td>
                    <td><?php echo $v['nns_type'] ?></td>
                    <td><?php echo $v['nns_cp_id'] ?></td>
                    <td><?php echo $v['nns_create_time'] ?></td>
                    <td><a href="nncms_content_vod_select.php?op=import&type=<?php echo $v['nns_type']?>&sp_id=<?php echo $sp_id?>&id=<?php echo $v['nns_id']
                        ?>">注入</a></td>
                </tr>
            <?php }
            ?>
            </tbody>
    </table>
</div>
	<div class="pagecontrol">
			共<span style="font-weight:bold;color:#ff0000;"><?php echo $total_num;?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_db_quantity.php?page=1&begin_time=<?php echo $array['begin_time']; ?>&end_time=<?php echo $array['end_time'] ?>&fenlei=<?php echo $array['table'] ?>&where=<?php echo $array['where']; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_db_quantity.php?page=<?php echo $currentpage - 1; ?>&begin_time=<?php echo $array['begin_time']; ?>&end_time=<?php echo $array['end_time'] ?>&fenlei=<?php echo $array['table'] ?>&where=<?php echo $array['where']; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $pages) { ?>
                                        <a href="nncms_content_db_quantity.php?page=<?php echo $currentpage + 1; ?>&begin_time=<?php echo $array['begin_time']; ?>&end_time=<?php echo $array['end_time'] ?>&fenlei=<?php echo $array['table'] ?>&where=<?php echo $array['where']; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_db_quantity.php?page=<?php echo $pages; ?>&begin_time=<?php echo $array['begin_time']; ?>&end_time=<?php echo $array['end_time'] ?>&fenlei=<?php echo $array['table'] ?>&where=<?php echo $array['where']; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_db_quantity.php?ran=1&begin_time=<?php echo $array['begin_time']; ?>&end_time=<?php echo $array['end_time'] ?>&fenlei=<?php echo $array['table'] ?>&where=<?php echo $array['where']; ?>',<?php echo $pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

    </div>
    <div class="controlbtns">
        <div class="controlbtn move"><a href="javascript:op_import();">注入</a></div>
    </div>
    <script>
        function add_config(type)
        {
            var obj_tr = $('.nns_'+type+'_value');
            var clone = obj_tr.eq(0).clone();
            clone.find("input[type='text']").attr("value","");
            obj_tr.eq(obj_tr.length-1).after(clone);
            window.parent.resetFrameHeight();
        }
        $("#button_delete").live("click", function(){
            var obj = $(this);
            var data_type= obj.attr('data_type');
            var length = $('.nns_'+data_type+'_value').length;
            if(length >1)
            {
                obj.parent().parent().remove();
            }
            else
            {
                obj.parent().find("input[type='text']").attr("value","");
            }
        });
        function op_import(){
            var r=confirm("是否进行该操作");
            if(r == true){
                var ids=getAllCheckBoxSelect();
                ids = ids.substr(0,ids.length-1);
                var url = "nncms_content_vod_select.php?sp_id=<?php echo $sp_id ?>&op=import&id="+ids;
                window.location.href = url;
            }
        }
    </script>
</body>
</html>
