<?php header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135201");
$checkpri = null;
 
if (!$pri_bool) {
	header("Location: ../nncms_content_wrong.php");
	exit ;
} 
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php'; 
include_once $nncms_config_path . 'nn_logic/video/vod.class.php';
 
$sp_id = $_GET['sp_id'];

$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : (!empty($_COOKIE["page_max_num"]) ?  $_COOKIE["page_max_num"] : 18);
$page_num = isset($_GET['page'])?$_GET['page']:1;
if($page_num<1){
	$page_num = 1;
}
$offset = intval($page_num-1)*$page_size;
$limit = $page_size;

$filter = null;
if($_GET['action']=='search'){
    $_GET['nns_name'] = trim( $_GET['nns_name']);
	$filter['nns_name'] = $_GET['nns_name'];
	$filter['nns_status'] = $_GET['nns_status'];
	$filter['nns_category'] = $_GET['nns_category'];	
	$filter['day_picker_start'] = $_GET['day_picker_start'];
	$filter['day_picker_end'] = $_GET['day_picker_end'];
	$filter['nns_action'] = $_GET['nns_action'];
}
if($_GET['action']=='show'){
	
	$nns_id = $_GET['nns_id'];
	$filter = array(
	'nns_id'=>$nns_id
	); 
	$xml = import_model::get_mgtvbk_import_epg_log($sp_id,$filter,$offset,$limit); 
	$data = $xml['data'];
//	$data1 = $data[0]['nns_data'];
    if(isset($data[0]['nns_file_dir']) && strlen($data[0]['nns_file_dir']) && file_exists(dirname(dirname(dirname(dirname(__FILE__))))."/data/".$data[0]['nns_file_dir']))
    {
        $data[0]['nns_data'] = file_get_contents(dirname(dirname(dirname(dirname(__FILE__))))."/data/".$data[0]['nns_file_dir']);
    }
	header("Content-Type:text/xml;charset=utf-8");  
	echo $data[0]['nns_data'];
	die;
} 
$result = import_model::get_mgtvbk_import_epg_log($sp_id,$filter,$offset,$limit);
$vod_total_num = $result['rows'];
$vod_pages = ceil($vod_total_num / $page_size);//总页数
if($page_num>$vod_pages){
	$page_num = $vod_pages;
} 
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;
//$nns_url = "&view_list_max_num=$page_size&sp_id=$sp_id";
//$refresh = "?page=$page_num&sp_id=$sp_id&view_list_max_num=";
unset($_GET['page']);
$nns_url = "&".http_build_query($_GET);//"&view_list_max_num=$page_size&sp_id=$sp_id";
$refresh = "?".http_build_query($_GET);//"?page=$page_num&sp_id=$sp_id&view_list_max_num=";

$data = $result['data'];
global $g_bk_version_number;
global $g_bk_web_url;
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
		<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>	
		<script type="text/javascript" src="../../js/dtree.js"></script>
		<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript">
			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
				var page = $("#go_page_num").val();
                setCookie("page_max_num",num);
				window.location.href='<?php echo $refresh;?>&view_list_max_num='+num+'&page='+page
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				});
				return BoxKey;
			}
			$(document).ready(function() {
				$('#clear_time').click(function(){
					$('#day_picker_start').val('');
					$('#day_picker_end').val('');
				});	
				window.parent.now_frame_url = window.location.href;
				
				$(".selectbox").hide();
			});
			
			function show_import_log(data){
				 alert(data);
					$("#select_frame").attr("src", "");
					$("#select_frame").attr("src",'11111');
                    $(".selectbox").show();
				 
			}
			function close_select() {
				$(".selectbox").hide();
			}
		</script>
	</head>

	<body>
		<div class="selectbox">
			<iframe scrolling="no" frameborder="0"  id="select_frame" ></iframe>
		</div>
		<div class="content">
			<div class="content_position">
				EPG注入日志
			</div>
				<div class="content_table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<form action="" method="get">
							<tbody>
								<tr>
							     <td>
							     名称：<input type="text" name="nns_name" id="nns_name" value="<?php  if(isset($_GET['nns_name'])) echo $_GET['nns_name'];?>" />
							     </td>
							     <td>
							     	状态：&nbsp;&nbsp;
							     	<select name="nns_status" style="width: 80px;">
											<option value="" >全部</option>
											<option value="2" <?php if($_GET['nns_status']=='2') echo 'selected="selected"'?>>成功</option>
											<option value="1" <?php if($_GET['nns_status']=='1') echo 'selected="selected"'?>>失败</option>
											
                                    </select>
							     </td>
									<td>
                                                                                                            分类：&nbsp;&nbsp;
                                    <select name="nns_category"  id="nns_category" style="width: 80px;">
                                    	<option value="">全部</option>
                                    	<option <?php if($_GET['nns_category'] == 'vod') echo 'selected="selected"'?> value="vod">点播</option>
                                    	<option <?php if($_GET['nns_category'] == 'index') echo 'selected="selected"'?> value="index">分集</option>
                                    	<option <?php if($_GET['nns_category'] == 'media') echo 'selected="selected"'?> value="media">片源</option>
										<option value="live" <?php if($_GET['nns_category']=='live') echo 'selected="selected"'?>>直播频道</option>
										<option value="live_media" <?php if($_GET['nns_category']=='live_media') echo 'selected="selected"'?>>直播源</option>
										<option value="playbill" <?php if($_GET['nns_category']=='playbill') echo 'selected="selected"'?>>节目单</option>
										<option value="file" <?php if($_GET['nns_category']=='file') echo 'selected="selected"'?>>包文件</option>
										<option value="product" <?php if($_GET['nns_category']=='product') echo 'selected="selected"'?>>产品包</option>
                                    </select>
                                   </td> 
                                   <td>
                                                                                                            动作：&nbsp;&nbsp;
                                    <select name="nns_action"  id="nns_action" style="width: 80px;">
                                    	<option value="">全部</option>
                                    	<option <?php if($_GET['nns_action'] == 'add') echo 'selected="selected"'?> value="add">添加</option>
                                    	<option <?php if($_GET['nns_action'] == 'modify') echo 'selected="selected"'?> value="modify">修改</option>
                                    	<option <?php if($_GET['nns_action'] == 'destroy') echo 'selected="selected"'?> value="destroy">删除</option>
                                    </select>
                                   </td> 
                                  	<td>
										选择时间段：&nbsp;&nbsp;&nbsp;<input name="day_picker_start" id="day_picker_start" type="text"  value="<?php  if(isset($_GET['day_picker_start'])) echo $_GET['day_picker_start'];?>" style="width:120px;" class="datetimepicker" callback="test" />
										 - <input name="day_picker_end" id="day_picker_end" type="text"  value="<?php  if(isset($_GET['day_picker_end'])) echo $_GET['day_picker_end'];?>" style="width:120px;" class="datetimepicker" callback="test" />
										 &nbsp;&nbsp;<input type="button" id="clear_time" name="clear_time" value="清除时间"/>
										<input type="hidden" name="sp_id" value="<?php echo $sp_id;?>" />
										<input type="hidden" name="action" value="search" />
										&nbsp;&nbsp;&nbsp;&nbsp;	
										<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  onclick="refresh_vod_page();"/>
									</td>
								</tr>
							</tbody>
							</form>
						</table>
				</div>			
			<div class="content_table formtable">

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th>序号</th>
							<th>名称</th> 
							<th>类型</th>
							<th>创建时间</th>
							<th>结果</th> 
							<th>状态</th>
							<th>动作</th>
						    <th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php
						 #$db = nn_get_db(NL_DB_READ);
						if($data!=null){
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) {
								$num++;
						?>
						<tr>
						<td><?php echo $num; ?></td>

                        <td ><?php 
                        #$name = nl_vod::get_videoname_by_videoid($db,$item['nns_video_id'],$item['nns_video_type']);
                        echo $item['nns_name']; 
//                        $dom = @simplexml_load_string($item['nns_data']);
//                        $params = json_decode(json_encode($dom),TRUE);  
//                        echo $params['@attributes']['name'];
                         ?>              
                        </td> 
                        <td>
                        <?php
                        if($item['nns_video_type']=='vod'){
                        	echo '点播';
                        }elseif($item['nns_video_type']=='index'){
                        	echo '分集';
                        }elseif($item['nns_video_type']=='media'){
                        	echo '片源';
                        }elseif($item['nns_video_type']=='live'){
                        	echo '直播频道';
                        }elseif($item['nns_video_type']=='live_media'){
                        	echo '直播源';
                        }elseif($item['nns_video_type']=='playbill'){
                        	echo '节目单';
                        }elseif($item['nns_video_type']=='file'){
                        	echo '包文件';
                        }elseif($item['nns_video_type']=='product'){
                        	echo '产品包';
                        }
                        ?>
                        </td>                        
                        <td><?php echo $item['nns_create_time'];?></td> 
                        <td><?php 
                        if($item['nns_status']==0){
                        	echo '<font color="#0000FF">成功</font>';
                        }else{
                        	echo '<font color="#FF0000">失败</font>';
                        }
                        
                        ?></td> 
                        <td><?php echo $item['nns_reason']; 
                        ?>
						</td>
						
						<td><?php echo $item['nns_action']; 
                        ?>
						</td>
						
						<td>
                            <?php if($g_bk_version_number == '1'){ ?>
                                <a target="_blank" href="<?php echo $g_bk_web_url . '/data/' . $item['nns_data'];?>">查看内容</a>
                                    <?php }else{ ?>
                                <a target="_blank" href="?action=show&sp_id=<?php echo $sp_id;?>&nns_id=<?php echo $item['nns_id'];?>">查看内容</a>
                            <?php } ?>
                        </td>
                        </tr>
						<?php
							}
						}$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) {
						?>
						<tr>
                                                               <td>&nbsp;</td>
                                                               <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp;</td>
                                                                 <td>&nbsp;</td>
                                                                 <td>&nbsp;</td>
                                                                 

                                                        </tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="pagecontrol">
			  共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num;?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_mgtv_epg_log.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_mgtv_epg_log.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_mgtv_epg_log.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_mgtv_epg_log.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_mgtv_epg_log.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

                        </div>
		</div>
	</body>
</html>
<script language="javascript">			
		$(".selectbox").css("top",100);
</script>