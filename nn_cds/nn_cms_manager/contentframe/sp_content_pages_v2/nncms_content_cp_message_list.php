<?php 
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include_once $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include_once $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
$queue_task_model = new queue_task_model();

if($_GET['op_field'] == 'nns_delete')
{
	$g_message_time = (isset($g_message_send_again) && isset($g_message_send_again['least']) && $g_message_send_again['least'] > 0) ? $g_message_send_again['least'] : 100;
	
    $ids = explode(',', $_GET['ids']);
    $ids = join("','", $ids);
    if(isset($_GET['org_id']) && !empty($_GET['org_id']))
    {
    	include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api/' . $_GET['org_id'] . '/' . $_GET['org_id'] . '_sync_source.php';
    }	
    else 
    {
    	include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api/mgtv/sync_source.php';
    }
   
    $dc = dc();
    $data = data("Y-m-d H:i:s");
    $str_delete = $_GET['op'] == '0' ? '0' : '1';
    $str_set = $str_delete == '0' ? ",nns_again='0'" : "";
    $str_where = $str_delete == '0' ? "" : " and nns_again >='{$g_message_time}'";
    $sql_update = "update nns_mgtvbk_message set nns_modify_time='{$data}',nns_delete='{$str_delete}'{$str_set} where nns_id in ('$ids') {$str_where} ";
    $result_update=nl_execute_by_db($sql_update, $dc->db());
    if($result_update)
    {
    	echo '<script>alert("操作成功");history.go(-1);</script>';die;
    }
   	else
   	{
   		echo '<script>alert("操作失败");history.go(-1);</script>';die;
   	}
}

if($_GET['op'] == 'import')
{
	$is_success = false;
	$ids = explode(',', $_GET['ids']);
	ini_set('display_errors', 1);
    if(isset($_GET['org_id']) && !empty($_GET['org_id']))
    {
    	include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api/' . $_GET['org_id'] . '/' . $_GET['org_id'] . '_sync_source.php';
		$class_name = $_GET['org_id'] . '_sync_source';
	}	
    else 
    {
    	include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api/mgtv/sync_source.php';
		$class_name = 'sync_source';
    }
	$dc = dc();
	$sync_source = new $class_name();
	foreach ($ids as $value)
	{
		$params['nns_id'] = $value;
		$message_info = $queue_task_model->get_cp_message_list($params);
		$message_info = $message_info['data'][0];
		if ($message_info['nns_message_state'] == '0' || $message_info['nns_message_state'] == '4' || $message_info['nns_message_state'] == '3'
				 || $message_info['nns_message_state'] == '6' || $message_info['nns_message_state'] == '7')
		{
			$dom = new DOMDocument('1.0', 'utf-8');
			$dom->loadXML($message_info['nns_message_content']);
			$nns_message_content = $dom->saveXML();
			$re = $sync_source->parse_xml($nns_message_content,$message_info['nns_message_id']);
			$date = date("Y-m-d H:i:s");
			if($re['ret']=='0')
			{
				$sql_update = "update nns_mgtvbk_message set nns_message_state=3,nns_modify_time='{$date}' where nns_id='{$value}'";
				$is_success = true;
			}
			elseif($re['ret']=='6')
			{
				$sql_update = "update nns_mgtvbk_message set nns_message_state=6,nns_modify_time='{$date}' where nns_id='{$value}'";
			}
			elseif($re['ret']=='7')
			{
				$sql_update = "update nns_mgtvbk_message set nns_message_state=7,nns_modify_time='{$date}' where nns_id='{$value}'";
			}
			else if ($re['ret'] == '2')
			{
				$sql_update = "update nns_mgtvbk_message set nns_message_state=5,nns_modify_time='{$date}' where nns_id='{$value}'";
				$is_success = true;
			}
			else
			{
				$sql_update = "update nns_mgtvbk_message set nns_message_state=4,nns_modify_time='{$date}' where nns_id='{$value}'";
				$is_success = false;
			}
			nl_execute_by_db($sql_update, $dc->db());
		}
	}
	
	$str = $is_success ? '操作成功' : '操作失败';
	echo '<script>alert("'.$str.'");history.go(-1);</script>';die;
}
else if($_GET['op'] == 'ftp_import')
{
	$is_success = false;
	$ids = explode(',', $_GET['ids']);
	ini_set('display_errors', 1);
	include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/np/np_http_curl.class.php';
	global $g_mgtvbk_base_url;
	$mgtvbk_base_url = $g_mgtvbk_base_url;
	unset($g_mgtvbk_base_url);
	if(empty($mgtvbk_base_url))
	{
		echo '<script>alert("配置文件$mgtvbk_base_url未配置注入地址");history.go(-1);</script>';die;
	}
	$arr_log=array();
	foreach ($ids as $value)
	{
		$curl = new np_http_curl_class();
		$data = array();
		$params['nns_id'] = $value;
		$message_info = $queue_task_model->get_cp_message_list($params);
		$message_info = $message_info['data'][0];
		//if($message_info['nns_message_state'] != 2)
		
		if($message_info['nns_message_state'] != 1)
		{
			continue;
		}
		$data['action'] = 're_import';
		$data['cmspost'] = $message_info['nns_message_xml'];
		$curl->post($mgtvbk_base_url,$data);
		$curl_info = $curl->curl_getinfo();
		if($curl_info['http_code'] != '200')
		{
			$arr_log[] = "消息id：{$value},错误信息：".$curlcurl_error();
		}
		unset($curl);
	}
	if(!empty($arr_log))
	{
		echo '<script>alert("操作有失败部分['.implode('],[', $pieces).']");history.go(-1);</script>';die;
	}
	else
	{
		echo '<script>alert("操作成功");history.go(-1);</script>';die;
	}
}
$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 18;
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) {
	$page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit = $page_size;
$message_id = '';
$message_state = '';
$params = array();
if ($_GET['actionsearch'] == 'search') {
	$params['nns_message_id'] = $_GET['nns_message_id'];
	$params['nns_message_state'] = $_GET['nns_message_state'];
	$params['nns_name'] = $_GET['nns_name'];
	$params['nns_type'] = $_GET['nns_type'];
	$params['nns_action'] = $_GET['nns_action'];
	if(!empty($_GET['day_picker_start']) && !empty($_GET['day_picker_end']))
	{
		if($_GET['day_picker_start'] > $_GET['day_picker_end'])
		{
			$temp = $_GET['day_picker_start'];
			$_GET['day_picker_start'] = $_GET['day_picker_end'];
			$_GET['day_picker_end'] = $temp;
		}
	}
	if(!empty($_GET['nns_begin_time']) && !empty($_GET['nns_end_time']))
	{
		if($_GET['nns_begin_time'] > $_GET['nns_end_time'])
		{
			$temp = $_GET['nns_begin_time'];
			$_GET['nns_begin_time'] = $_GET['nns_end_time'];
			$_GET['nns_end_time'] = $temp;
		}
	}
	$params['date_begin'] = $_GET['day_picker_start'];
	$params['date_end'] = $_GET['day_picker_end'];
	
	$params['date_begin_modify'] = $_GET['nns_begin_time'];
	$params['date_end_modify'] = $_GET['nns_end_time'];
	
	$params['nns_content'] = $_GET['nns_content'];
	$params['nns_again'] = $_GET['nns_again'];
	$params['nns_delete'] = $_GET['nns_delete_status'];
	$params['nns_cp_id'] = $_GET['nns_cp_id'];
}

$result = $queue_task_model->get_cp_message_list($params, $offset, $limit);
$vod_total_num = $result['rows'];
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) {
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;

$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);

$data = $result['data']; 
$dc = nl_get_dc(array(
        'db_policy' => NL_DB_READ,
        'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$result_cp_id_data = nl_query_by_db("select nns_cp_id from nns_mgtvbk_message group by nns_cp_id", $dc->db());
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<style>
.mytip_ul
{
    text-align: left;
    font-size: 12px;
    list-style-type: none;
    padding: 0;
    margin: 0;
}

.mytip_title
{
    margin-left: 10px;
    width: 100px;
    float: left;
    text-align: left;
}

.mytip_close
{
    margin-right: 10px;
    margin-top: 5px;
    width: 100px;
    float: right;
    text-align: right;
    cursor: pointer;
}

.mytip_top
{
    background-image: url(../../images/tips_titlebj.gif);
    color: #FFFFFF;
    font-size: 12px;
    height: 24px;
    line-height: 24px;
    font-weight: 600;
}

.tips_li
{
    height: 25px;
    line-height: 25px;
    border-bottom: #ccc 1px solid;
}

.tips_li_span
{
    color: #07507d;
    margin-left: 10px;
}

.tips_li_span2
{
    color: #6d6d6d;
}		
.myalert_div1
{
    height: 25px;
    line-height: 25px;
    border-bottom: #ccc 1px solid;
    width: 620px;
}

.myalert_div1_leftDiv
{
    float: left;
    width: 50%;
    margin-left: 10px;
}

.myalert_div1_rightDIv
{
    float: left;
    margin-left: 10px;
}

.myalert_div1_Div
{
    float: left;
    width: 100%;
    margin-left: 10px;
}

.myalert_div2
{
    height: 84px;
    line-height: 70px;
    border-bottom: #ccc 1px solid;
    width: 620px;
}

.myalert_div2_Div
{
    float: left;
    width: 100%;
    margin-left: 10px;
    height: 84px;
}

.myaelrt_li_title
{
    color: #07507d;
}

.myalert_li_txt
{
    color: #6d6d6d;
}

.myalert_inputTxt
{
    width: 220px;
    border: 0px solid #eff0f0;
    padding-left: 2px;
    background-color: #eff0f0;
    color: #6d6d6d;
}		
		</style>		
		
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
		<script language="javascript">

			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				})
				return BoxKey;
			}
			
			function msg_ftp_import()
			{
				var r=confirm("是否进行该操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "nncms_content_cp_message_list.php?op=ftp_import&ids="+ids+"&org_id=<?php echo $_GET['org_id']?>";
						window.location.href = url;
					}
				}
			}
			
			function msg_import(){
				var r=confirm("是否进行该操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "nncms_content_cp_message_list.php?op=import&ids="+ids+"&org_id=<?php echo $_GET['org_id']?>";
						window.location.href = url;
					}
				}
			}

			function msg_delete_or_recover(str){
				var r=confirm("是否进行该操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "nncms_content_cp_message_list.php?op_field=nns_delete&op="+ str +"&ids="+ids+"&org_id=<?php echo $_GET['org_id']?>";
						window.location.href = url;
					}
				}
			}

			
			$(document).ready(function() {
				$('#clear_time').click(function(){
					$('#day_picker_start').val('');
					$('#day_picker_end').val('');

				});
				window.parent.now_frame_url = window.location.href;
			});

			$(document).ready(function() {
				$('#clear_time_modify').click(function(){
					$('#nns_begin_time').val('');
					$('#nns_end_time').val('');

				});
				window.parent.now_frame_url = window.location.href;
			});
			
		</script>

	</head>

	<body>
		<div class="content">
			<div class="content_position">
				CP消息列表
			</div>
				<div class="content_table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<form action="" method="get">
							<input type="hidden" name="org_id" value="<?php echo $_GET['org_id'];?>" /> 
							<tbody>
								<tr>
									<td>
										消息ID：&nbsp;&nbsp;<input type="text" name="nns_message_id" id="nns_message_id" value="<?php echo $_GET['nns_message_id']; ?>" style="width:200px;">
										名称：&nbsp;&nbsp;<input type="text" name="nns_name" id="nns_name" value="<?php echo $_GET['nns_name']; ?>" style="width:200px;">
										内容查询：&nbsp;&nbsp;<input type="text" name="nns_content" id="nns_content" value="<?php echo $_GET['nns_content']; ?>" style="width:200px;">
										&nbsp;&nbsp;&nbsp;&nbsp;状态：&nbsp;&nbsp;<select name="nns_message_state" style="width: 150px;">
											<option value="" >全部</option>
											<option value="0" <?php if($_GET['nns_message_state']=='0') echo 'selected="selected"'?>>等待注入</option>
											<option value="1" <?php if($_GET['nns_message_state']=='1') echo 'selected="selected"'?>>FTP连接失败</option>
											<option value="2" <?php if($_GET['nns_message_state']=='2') echo 'selected="selected"'?>>XML获取失败</option>
											<option value="3" <?php if($_GET['nns_message_state']=='3') echo 'selected="selected"'?>>注入成功</option>
											<option value="4" <?php if($_GET['nns_message_state']=='4') echo 'selected="selected"'?>>注入失败</option>
											<option value="5" <?php if($_GET['nns_message_state']=='5') echo 'selected="selected"'?>>平台过滤消息</option>
                                    		<option value="6" <?php if($_GET['nns_message_state']=='6') echo 'selected="selected"'?>>主媒资未注入</option>
											<option value="7" <?php if($_GET['nns_message_state']=='7') echo 'selected="selected"'?>>分集未注入</option>
                                    	</select> 	
                                    	<br />
                                    	媒资类型：<select name="nns_type" style="width: 150px;">
											<option value="" >全部</option>
											<option value="1" <?php if($_GET['nns_type']=='1') echo 'selected="selected"'?>>主媒资</option>
											<option value="2" <?php if($_GET['nns_type']=='2') echo 'selected="selected"'?>>分集</option>
											<option value="3" <?php if($_GET['nns_type']=='3') echo 'selected="selected"'?>>片源</option>
                                    	</select>
                                    	&nbsp;&nbsp;&nbsp;&nbsp;
                                    	操作类型：<select name="nns_action" style="width: 150px;">
											<option value="" >全部</option>
											<option value="1" <?php if($_GET['nns_action']=='1') echo 'selected="selected"'?>>添加</option>
											<option value="2" <?php if($_GET['nns_action']=='2') echo 'selected="selected"'?>>修改</option>
											<option value="3" <?php if($_GET['nns_action']=='3') echo 'selected="selected"'?>>删除</option>
											<option value="4" <?php if($_GET['nns_action']=='4') echo 'selected="selected"'?>>上线</option>
											<option value="5" <?php if($_GET['nns_action']=='5') echo 'selected="selected"'?>>下线</option>
                                    	</select>
                                    	 &nbsp;&nbsp;&nbsp;&nbsp;
                                    	  重试次数：&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="nns_again" id="nns_again" style="width:100px;" value="<?php if($_GET['nns_again']) echo $_GET['nns_again'];?>">
										 &nbsp;&nbsp;&nbsp;&nbsp;删除状态：&nbsp;&nbsp;&nbsp;&nbsp;<select name="nns_delete_status" style="width: 150px;">
											<option value="" >全部</option>
											<option value="1" <?php if($_GET['nns_delete_status']=='1') echo 'selected="selected"'?>>删除</option>
											<option value="0" <?php if($_GET['nns_delete_status']=='0') echo 'selected="selected"'?>>未删除</option>
                                    	</select>
                                    	<?php if(is_array($result_cp_id_data) && !empty($result_cp_id_data))
                                    	{
                                    		
                                    	?>
                                    		&nbsp;&nbsp;&nbsp;&nbsp;CP ID：&nbsp;&nbsp;&nbsp;&nbsp;<select name="nns_cp_id" style="width: 150px;">
                                    		<option value="" >全部</option>
                                    	<?php 
                                    		foreach ($result_cp_id_data as $cp_id_val)
											{
												?>
												<option value="<?php echo $cp_id_val['nns_cp_id']; ?>" <?php if($_GET['nns_cp_id']==$cp_id_val['nns_cp_id']) echo 'selected="selected"'?>><?php echo $cp_id_val['nns_cp_id']; ?></option>
												<?php 
											}
											echo "</select>";
                                    	}?>
                                    	<br>
										选择创建时间段：&nbsp;&nbsp;&nbsp;<input name="day_picker_start" id="day_picker_start" type="text"  value="<?php
										if (isset($_GET['day_picker_start']))
											echo $_GET['day_picker_start'];
									?>" style="width:120px;" class="datetimepicker" callback="test" />
										 - <input name="day_picker_end" id="day_picker_end" type="text"  value="<?php
											if (isset($_GET['day_picker_end']))
												echo $_GET['day_picker_end'];
										?>" style="width:120px;" class="datetimepicker" callback="test" />
										 &nbsp;&nbsp;<input type="button" id="clear_time" name="clear_time" value="清除时间"/>
										&nbsp;&nbsp;选择修改时间段：&nbsp;&nbsp;&nbsp;<input name="nns_begin_time" id="nns_begin_time" type="text"  value="<?php
										if (isset($_GET['nns_begin_time']))
											echo $_GET['nns_begin_time'];
									?>" style="width:120px;" class="datetimepicker" callback="test" />
										 - <input name="nns_end_time" id="nns_end_time" type="text"  value="<?php
											if (isset($_GET['nns_end_time']))
												echo $_GET['nns_end_time'];
										?>" style="width:120px;" class="datetimepicker" callback="test" />
										 &nbsp;&nbsp;<input type="button" id="clear_time_modify" name="clear_time_modify" value="清除时间"/>
										<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  />		
										<input type="hidden" name="actionsearch" value="search" />								
									</td>
								</tr>
							</tbody>
							</form>
						</table>
				</div>			
			<div class="content_table formtable">

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th><input name="" type="checkbox" value="">序号</th>
							<!-- 
							<th style="width:120px;">ID</th>
							 -->
							<th style="width:120px;">消息ID</th>
							<th style="width:35px;">CP ID</th>
							<th style="width:120px;">名称</th>
							<th style="width:50px;">消息内容</th>
							<th style="width:50px;">消息XML文件</th>
							<th style="width:50px;">注入内容</th>
							<th>媒资类型</th>
							<th>操作类型</th>
							<th>状态</th>
							<th>重试次数</th>
							<th style="width:50px;">删除状态</th>
							<th>创建时间</th>
							<th>修改时间</th>
						    <th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if($data!=null){
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) {
								$num++;
						?>
						<tr>
						<td id="is_delete"><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
						<!-- 
						<td >                        
                        <?php
						
							//echo $item['nns_id'];
						
                        ?>
                   
                        </td>
						 -->
                        <td>
                        	
							                    
                        <?php
						
							echo $item['nns_message_id'];
						
                        ?>
                        
                        
                   
                        </td>
                        <td>
                        	
							                    
                        <?php
						
							echo $item['nns_cp_id'];
						
                        ?>
                        
                        
                   
                        </td>
                        <td><?php echo $item['nns_name']?></td>
                        <td>
                        	<a href="./nncms_content_cp_message_view.php?nns_id=<?php echo $item['nns_id']?>&nns_type=0" target="_blank">点击查看</a>
                        </td>
                        <td>   
                         	<a href="./nncms_content_cp_message_view.php?nns_id=<?php echo $item['nns_id']?>&nns_type=1" target="_blank">点击查看</a>                    
                        </td>
                        <td>
                        	<a href="./nncms_content_cp_message_view.php?nns_id=<?php echo $item['nns_id']?>&nns_type=2" target="_blank">点击查看</a>
                        </td>
                        <td>
                        <?php 
                        	switch ($item['nns_type'])
                        	{
                        		case '1': echo '主媒资';break;
                        		case '2': echo '分集';break;
                        		case '3': echo '片源';break;
                        		default: echo '';
                        	}
                        ?>
                        </td>
                        <td>
                        <?php 
                        	switch ($item['nns_action'])
                        	{
                        		case '1': echo '添加';break;
                        		case '2': echo '修改';break;
                        		case '3': echo '删除';break;
                        		case '4': echo '上线';break;
                        		case '5': echo '下线';break;
                        		default: echo '';
                        	}
                        ?>
                        </td>
                        <td>
                        <?php 
                        	switch ($item['nns_message_state'])
                        	{
                        		case '0': echo '等待注入';break;
                        		case '1': echo 'FTP连接失败';break;
                        		case '2': echo '获取XML失败';break;
                        		case '3': echo '注入成功';break;
                        		case '4': echo '注入失败';break;
                        		case '5': echo '平台过滤消息';break;
                        		case '6': echo '主媒资未注入';break;
                        		case '7': echo '分集未注入';break;
                        		default : echo '';
                        	}
                        ?>
                        </td>
                        <td>
                        <?php 
                        	echo $item['nns_again'];
                        ?>
                        </td>
                        <td>
                        <?php 
                        	switch ($item['nns_delete'])
                        	{
                        		case '1': echo "<font style=\"color:#FF0000\">已删除</font>";break;
                        		case '0': echo "<font style=\"color:#00CC00\">未删除</font>";break;
                        		default : echo '';
                        	}
                        ?>
                        </td>    
                        <td>	
                        <?php
                        	echo $item['nns_create_time'];
                        ?>
						</td>
						 <td>	
                        <?php
                        	echo $item['nns_modify_time'];
                        ?>
						</td>
                        <td>
                        	<?php if($item['nns_message_state'] == '4' || $item['nns_message_state'] == '0' || $item['nns_message_state'] == '6' || $item['nns_message_state'] == '7'){?>
                        	<a href="nncms_content_cp_message_list.php?op=import&ids=<?php echo $item['nns_id'];?>&org_id=<?php echo $_GET['org_id']?>">注入</a>
                        	<?php }?>
                        	</td>
                        </tr>
						<?php
							}
						}$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) {
						?>
						<tr>
                                                                                                                                                                                          
                                                               
                                                               <td>&nbsp;</td>
                                                               <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp;</td>
                                                                 <td>&nbsp;</td>
                                                                 <td>&nbsp;</td>
                                                                 <td>&nbsp;</td>
                                                                  <td>&nbsp;</td>
                                                                   <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
																	<td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                        </tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="pagecontrol">
			  共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_cp_message_list.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_cp_message_list.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_content_cp_message_list.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_cp_message_list.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_cp_message_list.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

                        </div>
                        
                 <div class="controlbtns">
    	<div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
        <div class="controlbtn move"><a href="javascript:msg_import();">注入</a></div>
        <div class="controlbtn move"><a href="javascript:msg_import();">重新注入</a></div>
        <div class="controlbtn move"><a href="javascript:msg_delete_or_recover('1');">删除</a></div>
        <div class="controlbtn move"><a href="javascript:msg_delete_or_recover('0');">恢复</a></div>
        <div class="controlbtn move"><a href="javascript:msg_ftp_import();">ftp重注入</a></div>
        <div style="clear:both;"></div>
    </div>    
                        
		</div>
	</body>
</html>
