<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
ob_start();
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/language_contant.php";
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include_once '../vod_pages/nncms_content_vod_dimensions.php';
include LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'vod_info_language.class.php';
include $nncms_config_path . "v2/common.php";
\ns_core\m_load::load("ns_core.m_config");

$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
$dc->open();
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();

$action = $_GET["action"];
$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106104");
$pri_edit = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106101");
$checkpri = null;


if (!$pri_bool) {
        Header("Location: ../nncms_content_wrong.php");
        exit;
}

include ($nncms_db_path . "nns_common/nns_db_constant.php");
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");

include ($nncms_db_path . "nns_vod/nns_db_vod_index_class.php");
include ($nncms_db_path . "nns_vod/nns_db_vod_media_class.php");

// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");


include $nncms_db_path . "nns_vod/nns_db_vod_class.php";
$vod_inst = new nns_db_vod_class();
if (!empty($nns_id)) {
        $vod_info = $vod_inst->nns_db_vod_info($nns_id, "*");
        $vod_inst = null;
} else {
        echo "<script>alert('" . $vod_info["reason"] . "');self.location='nncms_content_vodlist.php';</script>";
}
if ($vod_info["ret"] != 0) {
        echo "<script>alert('" . $vod_info["reason"] . "');self.location='nncms_content_vodlist.php';</script>";
}

$edit_data = $vod_info["data"][0]; 
/**
 * 废除原TAG方式
 */
//for ($tag_num=0;$tag_num<32;$tag_num++){
//	if ($edit_data["nns_tag".$tag_num]==1){
//		$tar_str.=$tag_num.',';
//	};
//}

$tar_str = ltrim($edit_data['nns_tag'], ',');


//include($nncms_db_path . "nns_keyword/nns_db_keyword_class.php");
//$keyword_model = new nns_db_keyword_class();
//$all_keyword = $keyword_model->get_keyword_by_ids($edit_data['nns_keyword']);
//		$vod_inst=null;
//	$edit_pri=$edit_data["nns_pri_id"];

$vod_indexes_inst = new nns_db_vod_index_class();
$vod_index_inst = new nns_db_vod_media_class();

include $nncms_db_path . "nns_carrier/nns_db_carrier_class.php";
include $nncms_db_path . "nns_partner/nns_db_partner_class.php";
include $nncms_db_path . "nns_group/nns_db_group_class.php";
$carrier_inst = new nns_db_carrier_class();
$partner_inst = new nns_db_partner_class();
$group_inst = new nns_db_group_class();
if ($_SESSION["nns_manager_type"] == 0) {

        $carrier_result = $carrier_inst->nns_db_carrier_list();

        if ($carrier_result["ret"] == 0) {
                $carrier_data = $carrier_result["data"][0];
        }
        if ($action == "edit") {
                //		 $partner_inst=new nns_db_partner_class();
                $partner_result = $partner_inst->nns_db_partner_info($edit_data["nns_org_id"]);

                if ($partner_result["ret"] == 0) {
                        $partner_data = $partner_result["data"][0];
                }

                //		 $group_inst=new nns_db_group_class();
                $group_result = $group_inst->nns_db_group_info($edit_data["nns_org_id"]);

                if ($group_result["ret"] == 0) {
                        $group_data = $group_result["data"][0];
                }
        }
}


//	类型为合作伙伴
if ($_SESSION["nns_manager_type"] == 1) {

        $partner_result = $partner_inst->nns_db_partner_info($_SESSION["nns_org_id"]);

        if ($partner_result["ret"] == 0) {
                $partner_data = $partner_result["data"][0];
        }
        //	类型为集团
} else if ($_SESSION["nns_manager_type"] == 2) {

        $group_result = $group_inst->nns_db_group_info($_SESSION["nns_org_id"]);

        if ($group_result["ret"] == 0) {
                $group_data = $group_result["data"][0];
        }
}
//include $nncms_db_path . "nns_assist/nns_db_assist_item_class.php";
//include $nncms_db_path . "nns_assist/nns_db_assist_class.php";
//include $nncms_db_path . "nns_service/nns_db_service_category_class.php";
//include $nncms_db_path . "nns_service/nns_db_service_class.php";
//  include $nncms_db_path. "nns_service/nns_db_service_detail_video_class.php";

///$assist_inst = new nns_db_assist_class();
//$service_inst = new nns_db_service_class();
$vod_inst = new nns_db_vod_class();
$vod_info = $vod_inst->nns_db_vod_info($nns_id, 'nns_asset_import_id,nns_import_source');
$vod_info = $vod_info['data'][0];

//  $service_item_inst=new nns_db_service_detail_video_class();
//$online1 = $assist_inst->check_video_belong($nns_id);
//$online2 = $service_inst->check_video_belong($nns_id);
//  $online3=$service_item_inst->nns_db_service_detail_video_list(0,0,null,$nns_id,0);
$assist_inst = null;
$service_inst = null;
//  $service_item_inst=null;
if (count($online1["data"]) > 0 || count($online2["data"]) > 0) {
        $is_online = true;
} else {
        $is_online = false;
}


/**
 * 扩展语言 BY S67
 */
$extend_langs_str = g_cms_config::get_g_extend_language();
if (empty($extend_langs_str)) {
        $extend_langs = array();
} else {
        $extend_langs = explode('|', $extend_langs_str);
}

/**
 * 剧照 BY cb
 */
//获取剧照数量，判断是否显示剧照iframe
include LOGIC_DIR . 'stills' . DIRECTORY_SEPARATOR . 'stills.class.php';

$stills_num = nl_stills::count_stills_num($dc, $edit_data['nns_id'], $edit_data['nns_view_type']);
$stills_enabled = g_cms_config::get_g_config_value('g_stills_enabled');
//./nncms_content_vod_detail.php?op=import_cdn&type=index&sp_id=<?php echo $_GET['sp_id'];>
//&nns_id=<?php //echo $edit_data['nns_id'];?&nns_import_id=<?php //echo $index_intor_data['nns_id'];>
$sp_id = $_GET['sp_id'];

/********************新版数据规整手动重新注入 start**************************/
//播控v2
global $g_bk_version_number;
$bk_version_number = $g_bk_version_number == '1' ? true : false;
unset($g_bk_version_number);
$enable_center_import = get_config_v2('g_enable_center_import_v2');
if ($bk_version_number && $_GET['op']=='import_cdn')
{

    if(!$enable_center_import)
    {
        \ns_core\m_load::load("ns_model.center_op.center_op_queue");
        $message = array(
            'base_info' => array(
                'nns_video_type' => $_GET['type'],
                'nns_action' => NS_CDS_ADD,
                'nns_id' => $_GET['nns_id'],
                'nns_weight' => 0,
                'nns_message_id'=> $_GET['nns_id'],
                'nns_is_group'=> 0,
                'nns_cp_id'=> $_GET['nns_cp_id'],
            ),
        );
        $center_op_queue = new \ns_model\center_op\center_op_queue($sp_id, $_GET['type']);
        $result = $center_op_queue->push_one($message);
        unset($center_op_queue);
        $reason = $result['reason'];
        if ($result['ret'] !=0 )
        {
            echo "<script>alert('操作失败,原因" . $result['reason'] . "');history.go(-1);</script>";die;
        }
        else
        {
            echo '<script>alert("操作成功");history.go(-1);</script>';die;
        }
    }
    else
    {
        \ns_core\m_load::load("ns_model.center_import.center_import_queue");
        $message = array(
            'base_info'=>array(
                'nns_message_id' => $_GET['nns_id'],
                'nns_video_id' => $_GET['nns_id'],
                'nns_video_type'=> $_GET['type'],
                'nns_op_mtime' => round(microtime(TRUE) * 1000),
                'nns_weight'=>  0,
                'nns_is_group' =>  0,
                'nns_cp_id'=> $_GET['nns_cp_id'],
                'nns_action' => NS_CDS_ADD,
            ),
        );
        $center_import_queue = new \ns_model\center_import\center_import_queue();
        $result = $center_import_queue->push_one($message);
        unset($center_import_queue);
        if ($result['ret'] !=0 )
        {
            echo "<script>alert('操作失败,原因" . $result['reason'] . "');history.go(-1);</script>";die;
        }
        else
        {
            echo '<script>alert("操作成功");history.go(-1);</script>';die;
        }
    }
}
if ($bk_version_number && $_GET['op']=='add_queue')
{
    $nns_id = $_GET['nns_id'];
    $ids = $_GET['ids'];
    $ids_array = explode(',', $ids);
    if (is_array($ids_array) && count($ids_array) > 0)
    {
        foreach ($ids_array as $value)
        {
            if(!$enable_center_import)
            {
                \ns_core\m_load::load("ns_model.center_op.center_op_queue");
                $message = array(
                    'base_info' => array(
                        'nns_video_type' => $_GET['type'],
                        'nns_action' => $str_action,
                        'nns_id' => $value,
                        'nns_weight' => 0,
                        'nns_message_id'=> $value,
                        'nns_is_group'=> 0,
                        'nns_cp_id'=> $_GET['nns_cp_id'],
                    ),
                );
                $center_op_queue = new \ns_model\center_op\center_op_queue();
                $result = $center_op_queue->push_one($message);
                unset($center_op_queue);
                $reason = $result['reason'];
            }
            else
            {
                \ns_core\m_load::load("ns_model.center_import.center_import_queue");
                $message = array(
                    'base_info'=>array(
                        'nns_message_id' => $value,
                        'nns_video_id' => $value,
                        'nns_video_type'=> $_GET['type'],
                        'nns_op_mtime' => round(microtime(TRUE) * 1000),
                        'nns_weight'=>  0,
                        'nns_is_group' =>  0,
                        'nns_cp_id'=> $_GET['nns_cp_id'],
                        'nns_action' => NS_CDS_ADD,
                    ),
                );
                $center_import_queue = new \ns_model\center_import\center_import_queue();
                $result = $center_import_queue->push_one($message);
                unset($center_import_queue);
            }
        }
        if ($result['ret'] !=0 )
        {
            echo "<script>alert('操作失败,原因" . $result['reason'] . "');location.href='nncms_content_vod_detail.php?sp_id=".$sp_id."&view_type=&nns_id=".$nns_id."'</script>";die;
        }
        else
        {
            echo '<script>alert("操作成功");location.href="nncms_content_vod_detail.php?sp_id='.$sp_id.'&view_type=&nns_id='.$nns_id.'"</script>';die;
        }
    }
    else
    {
        echo '<script>alert("操作失败,媒资id传入错误");location.href="nncms_content_vod_detail.php?sp_id='.$sp_id.'&view_type=&nns_id='.$nns_id.'"</script>';die;
    }
}
/********************新版数据规整手动重新注入 end**************************/

if($_GET['op']=='import_cdn'){
	include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
	$queue_task_model = new queue_task_model();
	$nns_import_id = $_GET['nns_import_id'];
	$op_type = $_GET['type'];
	$op_action = 'add';	
	$re = $queue_task_model->q_add_task($nns_import_id, $op_type, $op_action,$sp_id,null,true);
	if($re){
		echo '<script>alert("操作成功");history.go(-1);</script>';die;
	}else{
		echo '<script>alert("操作失败");history.go(-1);</script>';die;
	}	
}
if ($_GET['op'] == 'add_queue')
{
    include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
	$queue_task_model = new queue_task_model();
	$nns_id = $_GET['nns_id'];
	$ids = $_GET['ids'];
	$op_type = $_GET['type'];
	$op_action = 'add';
	$ids_array = explode(',', $ids);  
	if(is_array($ids_array))
	{
		foreach($ids_array as $v)
		{
		   $re = $queue_task_model->q_add_task($v, $op_type, $op_action,$sp_id,null,true);  
		}
		echo '<script>alert("操作成功");location.href="nncms_content_vod_detail.php?sp_id='.$sp_id.'&view_type=&nns_id='.$nns_id.'"</script>';die;
	}
	else
	{
		echo '<script>alert("操作失败");location.href="nncms_content_vod_detail.php?sp_id='.$sp_id.'&view_type=&nns_id='.$nns_id.'"</script>';die;
	}	
}

ob_end_clean();
?>
<!DOCTYPE html >
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/rate.js"></script>
<script language="javascript" src="../../js/image_loaded_func.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript"> 
function set_td_max_width() {
                                var len = $(".content_table table tr").length;
                                for (var i = 0; i < len; i++) {
                                        var len1 = $(".content_table table tr:eq(" + i + ")").find("td").length;
                                        var wid = $(document).width() / len1;
                                        $(".content_table table tr:eq(" + i + ")").find("td").css("max-width", wid);
                                }
                        }
function gotoDelete(nns_id) {
                                $("#delete_form #nns_id").val(nns_id);
                                checkForm('<?php echo cms_get_lang('dbgl'); ?>', '<?php echo cms_get_lang('msg_ask_delete'); ?>', $('#delete_form'), '');
                        }
function showCDN(id, type) {
                                window.parent.begin_show_cdn(id, type);
                        }

function go_back() {
                                if (window.parent.refresh_tree_content) {
                                        window.parent.refresh_tree_content();
                                } else {
                                        history.go(-1);
                                }
                        }
function show_extend_lang(num) {

                                if ($(".lang_editor:eq(" + num + ")").is(":hidden") == false) {
                                        $(".lang_editor").hide();
                                        $(".lang_btn").html("<?php echo cms_get_lang('spread'); ?>");
                                } else {

                                        $(".lang_editor").hide();
                                        $(".lang_btn").html("<?php echo cms_get_lang('spread'); ?>");
                                        $(".lang_editor:eq(" + num + ")").show();
                                        $(".lang_btn:eq(" + num + ")").html("<?php echo cms_get_lang('fold'); ?>");
                                }
                                window.parent.checkheight();
                        }

function set_show_minor(num) {
                                $obj = $("input[name=moreinfo_chk]:eq(" + num + ")");
                                if ($obj.attr("checked") == "checked") {
                                        $(".extend_" + num).show();
                                } else {
                                        $(".extend_" + num).hide();
                                }
                                window.parent.checkheight();
                        }
                        
                        
function show_file(file_id){
                        	$("."+file_id).toggle();
                        }

                        $(document).ready(function() {
                                $(".lang_editor").hide();
                                show_extend_lang(0);
                                var len = $("input[name=moreinfo_chk]").length;
                                for (var i = 0; i < len; i++) {
                                        set_show_minor(i);
                                }
                        });
                        //选中全部
function select_index_all(id) 
                        {   
                            if(id == 'index')
                            {
                                //选中全部分集
                                $(".index").attr("checked",true);
                            }
                            if(id == 'media')
                            {
                                //选中全部片源
                            	$(".media").attr("checked",true);
                            } 
                        } 
                        
function reset_index_all(id) 
                        {   
                        	if(id == 'index')
                            {
                                //取消全部分集
                                $(".index").attr("checked",false);
                            }
                            if(id == 'media')
                            { 
                                //取消全部片源
                            	$(".media").attr("checked",false);
                            } 
                            
                        } 
                        //批量注入分集
function import_task_more(action,id){
            				var r=confirm("是否进行该操作");
            				var class_name = ''; 
            				var type_name = '';
            				if(id == 'index')
                			{
                    			type_name = 'index';
            					class_name = ".index";
                    		}
            				if(id == 'media')
                			{
            					type_name = 'media';
            					class_name = ".media";
                    		} 
            				if(r == true)
                			{
            					//var ids=getAllCheckBoxSelect(); 
            					var ids = "";
                				var i = 0;
            					$(class_name).each(function() 
                    		    {
                        		    
            						if ($(this).attr("checked")) {  
            			                ids += $(this).val()+",";
            			                i = 1; 
            			            } 
            				    });
            				    if(i == 0)
                				{
            				    	alert('你还没有选择或者没有这选择对');window.location.reload();
                    			}
            					ids = ids.substr(0,ids.length-1);//除去最后一个“，” 
            					//alert(ids);
            				    //return false;
            					window.location.href = "nncms_content_vod_detail.php?nns_id=<?php echo $nns_id;?>&sp_id=<?php echo $sp_id;?>&nns_cp_id=<?php echo $edit_data['nns_cp_id'];?>&type="+type_name+"&op=add_queue&ids="+ids+"&action="+action;
 
            				}
            			}
                </script>
        </head>

        <!--<body onload="set_td_max_width();">-->
        <body>
                <div class="content">
                    <!--<div class="content_position"><?php echo cms_get_lang('dbgl'); ?> > <?php
                        if ($view_type == 0) {
                                echo cms_get_lang('media_dy|dbgl');
                        } else if ($view_type == 1) {
                                echo cms_get_lang('media_lxj|dbgl');
                        }
                        ?></div>-->
                        <div class="content_table">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                                <tr>
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('resource|id'); ?>:</td>
                                                        <td class="rightstyle3"><?php echo $edit_data["nns_id"]; ?></td>
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('media_ywsy'); ?>:</td>
                                                        <td class="rightstyle3"><?php echo $edit_data["nns_eng_name"]; ?></td>
                                                </tr>
                                                <tr>
                                                        <td class="rightstyle2a"><?php
                                                                if ($view_type == 0) {
                                                                        echo cms_get_lang('media_dy|name');
                                                                } else if ($view_type == 1) {
                                                                        echo cms_get_lang('media_lxj|name');
                                                                }
                                                                ?>:
                                                        </td>
                                                        <td class="rightstyle3"><?php echo $edit_data["nns_name"]; ?></td>

                                                        <td class="rightstyle2a"><?php echo cms_get_lang('media_pinyin'); ?>:</td>
                                                        <td class="rightstyle3"><?php echo $edit_data["nns_pinyin"]; ?></td>
                                                </tr>
                                                <tr>
                                                        	
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('state'); ?>:</td>
                                                        <td   class="rightstyle3">
                                                                <?php
                                                                if ($is_online) {
                                                                        echo "<font style=\"color:#00CC00\">" . cms_get_lang('media_ysx') . "</font>";
                                                                } else {
                                                                        echo "<font style=\"color:#999999\">" . cms_get_lang('media_wxx') . "</font>";
                                                                }
                                                                ?>
                                                        </td>
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('zdgl_zdscbj'); ?>:</td>
                                                        <td class="rightstyle3"> 
                                                                <alt alt="<?php echo get_tag_detail_html($tar_str); ?>"><?php echo $tar_str; ?></alt>
                                                        </td>
                                                        
                                                </tr>
                                                <?php if (count($online1["data"]) > 0) { ?>
                                                        <tr>
                                                                <td class="rightstyle2a"><?php echo cms_get_lang('assist_owner'); ?>:</td>
                                                                <td colspan="3">
                                                                        <?php
                                                                        foreach ($online1["data"] as $online1_arr_value) {
                                                                                echo "<span style=\"color:#ff0000\">" . $online1_arr_value["nns_name"] . "</span><span style=\"color:#999999\">(" . $online1_arr_value["nns_assist_id"] . ")</span>&nbsp;&nbsp;&nbsp;&nbsp;";
                                                                        }
                                                                        ?>
                                                                </td> 
                                                        </tr>
                                                <?php } ?>
                                                <?php if (count($online2["data"]) > 0 && $g_service_enabled == 1) { ?>
                                                        <tr>
                                                                <td class="rightstyle2a"><?php echo cms_get_lang('bkgl_owner'); ?>:</td>
                                                                <td colspan="3">
                                                                        <?php
                                                                        foreach ($online2["data"] as $online2_arr_value) {
                                                                                echo "<span style=\"color:#ff0000\">" . $online2_arr_value["nns_name"] . "</span><span style=\"color:#999999\">(" . $online2_arr_value["nns_service_id"] . ")</span>&nbsp;&nbsp;&nbsp;&nbsp;";
                                                                        }
                                                                        ?>
                                                                </td>
                                                        </tr>
                                                <?php } ?>
                                                <tr class="select_carrier_class">
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('group'); ?>:</td>
                                                        <td class="rightstyle3"><?php
                                                                if ($edit_data["nns_org_type"] == 0) {
                                                                        echo $carrier_data["nns_name"];
                                                                } else if ($edit_data["nns_org_type"] == 1) {
                                                                        echo $partner_data["nns_name"];
                                                                } if ($edit_data["nns_org_type"] == 2) {
                                                                        echo $group_data["nns_name"];
                                                                }
                                                                ?>
                                                        </td>
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('media_copyright_date'); ?>:</td>
                                                        <td class="rightstyle3">
                                                                <?php echo $edit_data["nns_copyright_date"]; ?>
                                                        </td>
                                                </tr>

                                                <tr>
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('media_tjfs'); ?>:</td>
                                                        <td class="rightstyle3">
                                                                <span class="p_rate" id="p_rate">
                                                                        <i title="1<?php echo cms_get_lang('point'); ?>"></i>
                                                                        <i title="2<?php echo cms_get_lang('point'); ?>"></i>
                                                                        <i title="3<?php echo cms_get_lang('point'); ?>"></i>
                                                                        <i title="4<?php echo cms_get_lang('point'); ?>"></i>
                                                                        <i title="5<?php echo cms_get_lang('point'); ?>"></i>
                                                                </span>
                                                                <script>
                                                                        var Rate = new pRate("p_rate", function() {
                                                                                $("#nns_point").val(Rate.Index);
                                                                        }, true);
                                                                        Rate.setindex(<?php echo $edit_data["nns_point"]; ?>);

                                                                </script>
                                                        </td>
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('media_daoyan'); ?>:</td>
                                                        <td class="rightstyle3">
                                                                <?php echo $edit_data["nns_director"]; ?>
                                                        </td>
                                                </tr>

                                                <tr>
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('media_sysj'); ?>:</td>
                                                        <td class="rightstyle3">
                                                                <?php echo $edit_data["nns_show_time"]; ?>
                                                        </td>
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('media_dq'); ?>:</td>
                                                        <td class="rightstyle3">
                                                                <?php echo $edit_data["nns_area"]; ?>
                                                        </td>
                                                </tr>

                                                <tr>
                                                        <td class="rightstyle2a"><?php
                                                                if ($view_type == 0) {
                                                                        echo cms_get_lang('media_zsc');
                                                                } else if ($view_type == 1) {
                                                                        echo cms_get_lang('media_mjpc');
                                                                }
                                                                ?>:</td>
                                                        <td class="rightstyle3">
                                                                <?php echo ceil($edit_data["nns_view_len"] / 60); ?>
                                                        </td>
                                                        <td class="rightstyle2a"><?php
                                                                if ($view_type == 0) {
                                                                        echo cms_get_lang('media_fps');
                                                                } else if ($view_type == 1) {
                                                                        echo cms_get_lang('media_zjs');
                                                                }
                                                                ?>:</td>
                                                        <td class="rightstyle3">
                                                                <?php echo $edit_data["nns_all_index"]; ?>
                                                        </td>
                                                </tr>
                                                <?php if ($g_key_word_enabled == 1) { ?>
                                                        <tr>
                                                                <td class="rightstyle2a"><?php echo cms_get_lang('keywords'); ?>:</td>
                                                                <td colspan="3"><?php
                                                                        if (is_array($all_keyword)) {
                                                                                foreach ($all_keyword as $keyword) {
                                                                                        echo $keyword['nns_name'] . '，';
                                                                                }
                                                                        }
                                                                        ?>
                                                                </td>
                                                        </tr>
                                                <?php } ?>
                                                <tr>
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('zyk_play_count'); ?>:</td>
                                                        <td class="rightstyle3">
                                                                <?php echo $edit_data["nns_play_count"]; ?>
                                                        </td>
                                                        <td class="rightstyle2a"><?php echo "CP ID"; ?>:</td>
                                                        <td class="rightstyle3">
                                                                <?php echo $edit_data["nns_cp_id"]; ?>
                                                        </td>
                                                </tr>
                                                <tr>
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('media_yy'); ?>:</td>
                                                        <td colspan="3">
                                                                <?php echo $edit_data["nns_actor"]; ?>
                                                        </td>
                                                </tr>
                                                <tr>
                                                        <td class="rightstyle2a">注入来源:</td>
                                                        <td class="rightstyle3">
                                                                <?php echo $vod_info["nns_import_source"]; ?>
                                                        </td>
                                                        <td class="rightstyle2a">注入ID:</td>
                                                        <td class="rightstyle3">
                                                                <?php echo $vod_info["nns_asset_import_id"]; ?>
                                                        </td>
                                                </tr>
                                                <tr>
                                                        <td colspan="4" class="rightstyle2a rightstyle4"><?php echo cms_get_lang('media_bjhb'); ?>:</td>
                                                </tr>

                                                <tr>
                                                        <td colspan="4" class="rightstyle4">
                                                                <div class="img_content">
                                                                        <?php if (!empty($edit_data["nns_image0"])) { ?>
                                                                                <div class="img_view"><h4><?php echo cms_get_lang('media_bjhb') . '(' . cms_get_lang('media_big') . ')' . cms_get_lang('media_slt'); ?></h4>
                                                                                        <a href="<?php echo pub_func_get_image_url($edit_data["nns_image0"]); ?>?rand=<?php echo rand(); ?>" target="_blank">
                                                                                                <img src="<?php echo pub_func_get_image_url($edit_data["nns_image0"]); ?>?rand=<?php echo rand(); ?>" style="border:none;" /></a><h4 class="resolution"></h4></div>
                                                                        <?php } ?>

                                                                        <?php if (!empty($edit_data["nns_image1"])) { ?>
                                                                                <div class="img_view"><h4><?php echo cms_get_lang('media_bjhb') . '(' . cms_get_lang('media_normal') . ')' . cms_get_lang('media_slt'); ?></h4>
                                                                                        <a href="<?php echo pub_func_get_image_url($edit_data["nns_image1"]); ?>?rand=<?php echo rand(); ?>" target="_blank">
                                                                                                <img src="<?php echo pub_func_get_image_url($edit_data["nns_image1"]); ?>?rand=<?php echo rand(); ?>" style="border:none;" /></a><h4 class="resolution"></h4></div>
                                                                        <?php } ?>
                                                                        <?php if (!empty($edit_data["nns_image2"])) { ?>
                                                                                <div class="img_view"><h4><?php echo cms_get_lang('media_bjhb') . "(" . cms_get_lang('media_small') . ")" . cms_get_lang('media_slt'); ?></h4>
                                                                                        <a href="<?php echo pub_func_get_image_url($edit_data["nns_image2"]); ?>?rand=<?php echo rand(); ?>" target="_blank">
                                                                                                <img src="<?php echo pub_func_get_image_url($edit_data["nns_image2"]); ?>?rand=<?php echo rand(); ?>" style="border:none;" /></a><h4 class="resolution"></h4></div>
                                                                        <?php } ?>
                                                                </div>
                                                        </td>
                                                </tr>
                                                <tr>
                                                        <td class="rightstyle2a"><?php echo cms_get_lang('media_nrjj'); ?>:</td>
                                                        <td  colspan="3">
                                                                <?php echo $edit_data["nns_summary"]; ?>
                                                        </td>
                                                </tr>
                                                <?php if ($pri_edit) { 
                                                	?>
                                                        
                                                        <tr>
                                                                <td class="rightstyle2a"><?php echo cms_get_lang('action'); ?>:</td>
                                                                <td  colspan="3">
                                                                        <input name="" type="button" value="<?php echo cms_get_lang('media_yp_xx'); ?>" onclick="self.location.href = '../vod_pages/nncms_content_vod_addedit.php?action=edit&view_type=<?php echo $view_type; ?>&nns_id=<?php echo $nns_id; ?>';"  />&nbsp;&nbsp;
                                                                        <input name="" type="button" value="<?php echo cms_get_lang('media_bjhb'); ?>" onclick="self.location.href = '../vod_pages/nncms_content_vod_img_addedit.php?nns_id=<?php echo $nns_id; ?>';" />&nbsp;&nbsp;
                                                                       
  
                                                                </td>
                                                        </tr>
                                                <?php  }?>
                                                <!--语言扩展-->
                                                
                                                <tr>
													<td colspan="4"   style="padding:0px;">
														<div class="radiolist" style=" border-bottom:1px solid #86B7CC;">
                                                        	<div class="radiogroup" style="background-color:#DCEEF5;padding:5px;">
                                                            	 <a href="./nncms_content_vod_detail.php?op=import_cdn&type=video&sp_id=<?php echo $_GET['sp_id'];?>&nns_id=<?php echo $edit_data['nns_id'];?>&nns_cp_id=<?php echo $edit_data['nns_cp_id'];?>&nns_import_id=<?php echo $edit_data['nns_id'];?>" style="color: red;"><b>主媒资注入</b></a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                

                                                <!-----多語言結束-------->

                                                <!--分页计算-->
                                                <?php
                                                if (!empty($_COOKIE["page_max_num"])) {
                                                        $g_manager_list_max_num = $_COOKIE["page_max_num"];
                                                }
                                                $index_pages = ceil($edit_data["nns_all_index"] / $g_manager_list_max_num);
                                                $current_page = $_GET['page'];
                                                if (empty($current_page)) {
                                                        $current_page = 1;
                                                }
                                                $begin_index = ($current_page - 1) * $g_manager_list_max_num + 1; //开始的集数
                                                $end_index = $current_page * $g_manager_list_max_num; //结束的集数
                                                if ($end_index > $edit_data["nns_all_index"]) {
                                                        $end_index = $edit_data["nns_all_index"];
                                                }
                                                ?>


                                                <?php
                                                for ($vod_current_index = $begin_index; $vod_current_index <= $end_index; $vod_current_index++) {

                                                        $num = 0;

                                                        $vod_indexes_info = $vod_indexes_inst->nns_db_vod_index_info($nns_id, $vod_current_index - 1);
                                                        $index_intor_data = $vod_indexes_info["data"][0];  
														$vod_media_delete_arr = $vod_index_inst->nns_db_vod_media_list(0, 0, $nns_id, $vod_current_index - 1,null,null,  null,  null, 1);
														if ($vod_media_delete_arr["ret"] == 0) {
                                                                $vod_media_delete_data = $vod_media_delete_arr["data"];
                                                        } else {
                                                                $vod_media_delete_data = null;
                                                        }
                                                        ?>

                                                        <tr>
                                                                <td colspan="4" style="padding:0px;">
                                                                        <div class="radiolist" >
                                                                                <div class="radiogroup">
                                                                                        <h3><?php  
                                                                                                if ($view_type == 0) {
                                                                                                        echo cms_get_lang('media_fps') . $vod_current_index;
																									if($vod_media_delete_data!=null){
																										echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"javascript:show_file('file_$vod_current_index');\" >历史注入</a>";
																									}
                                                                                                } else if ($view_type == 1) {
                                                                                                        echo cms_get_lang('media_js') . $vod_current_index;
                                                                                                }  
                                                                                                ?>
                                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                <input class="index" name="input" type="checkbox" value="<?php echo $index_intor_data['nns_id'];?>"> 
                                                                                                <a href="./nncms_content_vod_detail.php?op=import_cdn&type=index&sp_id=<?php echo $_GET['sp_id'];?>&nns_id=<?php echo $edit_data['nns_id'];?>&nns_cp_id=<?php echo $edit_data['nns_cp_id'];?>&nns_import_id=<?php echo $index_intor_data['nns_id'];?>" style="color: red;">
                                                                                                <b>分集注入</b></a>
                                                                                                </h3>
                                                                                </div>
                                                                        </div>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td width="120" style="padding-left:20px;"><?php echo cms_get_lang('name'); ?>:</td>
                                                                <td><?php echo $index_intor_data["nns_name"]; ?></td>
                                                                <td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_index_sc'); ?>:</td>
                                                                <td><?php echo round($index_intor_data["nns_time_len"] / 60); ?></td>
                                                        </tr>
                                                        <tr>
                                                                <td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_daoyan'); ?>:</td>
                                                                <td><?php echo $index_intor_data["nns_director"]; ?></td>
                                                                <td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_yy'); ?>:</td>
                                                                <td><?php echo $index_intor_data["nns_actor"]; ?></td>
                                                        </tr>
                                                        <tr>
                                                                <td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_bjhb'); ?>:</td>
                                                                <td>
                                                                        <div class="img_content">
                                                                                <?php if ($index_intor_data["nns_image"] != "") { ?>
                                                                                        <div class="img_view">
                                                                                                <a href="<?php echo pub_func_get_image_url($index_intor_data["nns_image"]); ?>?rand=<?php rand(); ?>" target="_blank">
                                                                                                        <img src="<?php echo pub_func_get_image_url($index_intor_data["nns_image"]); ?>" style="border:none;" />
                                                                                                </a>
                                                                                        </div>
                                                                                <?php } ?>
                                                                        </div>
                                                                </td>
                                                                <td width="120" style="padding-left:20px;"><?php echo cms_get_lang('media_index_jj'); ?>:</td>
                                                                <td><?php echo $index_intor_data["nns_summary"]; ?></td>
                                                        </tr>

                                                        <tr>
                                                                <td width="120" style="padding-left:20px;">注入来源:</td>
                                                                <td><?php echo $index_intor_data["nns_import_source"]; ?></td>
                                                                <td width="120" style="padding-left:20px;">注入ID:</td>
                                                                <td><?php echo $index_intor_data["nns_import_id"]; ?></td>
                                                        </tr>
														<tr>
                                                                <td width="120" style="padding-left:20px;">CP ID:</td>
                                                                <td><?php echo $index_intor_data["nns_cp_id"]; ?></td>
                                                                 <td width="120" style="padding-left:20px;"></td>
                                                                <td></td>
                                                        </tr>
                                                        <?php
                                                        $vod_media_arr = $vod_index_inst->nns_db_vod_media_list(0, 0, $nns_id, $vod_current_index - 1);
														
														

                                                        if ($vod_media_arr["ret"] == 0) {
                                                                $vod_media_data = $vod_media_arr["data"];
                                                        } else {
                                                                $vod_media_data = null;
                                                        }
														
														
														
														
                                                        ?>
                                                        <tr>
                                                                <td colspan="4">
                                                                        <div class="content_table formtable" style="padding:3px 10px; border:0px;">
                                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                        <thead>
                                                                                                <tr>
                                                                                                        <th><?php echo cms_get_lang('segnumber'); ?></th>
                                                                                                        <th><?php echo cms_get_lang('name'); ?></th>
                                                                                                        <th><?php echo cms_get_lang('media_pydz'); ?></th>
                                                                                                        <th><?php echo cms_get_lang('media_pylx'); ?></th>
                                                                                                        <th><?php echo cms_get_lang('zdgl_zdscbj'); ?></th>
                                                                                                        <th><?php echo cms_get_lang('media_pygs'); ?></th>
                                                                                                        <th><?php echo cms_get_lang('media_mtid'); ?></th>
                                                                                                        <th><?php echo cms_get_lang('media_pyml'); ?></th>
                                                                                                        <th><?php echo "CP ID"; ?></th>
                                                                                                        <th>影片展示类型</th>
                                                                                                        <th>注入信息</th>
                                                                                                        <th>注入时间</th>
                                                                                                        <th>注入动作</th>
                                                                                                        <th><?php echo cms_get_lang('action'); ?></th>
                                                                                                </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                                <?php 
                                                                                                if ($vod_media_data != null) { 
                                                                                                        foreach ($vod_media_data as $item) {
                                                                                                                $num++;
                                                                                                                ?>
                                                                                                                <tr> 
                                                                                                                        <td><input class="media" name="input" type="checkbox" value="<?php echo $item['nns_id']; ?>"><?php echo $num; ?></td>
                                                                                                                        <td><?php echo $item["nns_name"]; ?></td>
                                                                                                                        <td><?php echo $item["nns_url"]; ?></td>
                                                                                                                        <td><?php echo get_contant_language($item["nns_mode"]); ?></td>
                                                                                                                        <td><alt alt="<?php echo get_tag_detail_html($item["nns_tag"]); ?>"><?php echo $item["nns_tag"]; ?></td>
                                                                                                                        <td><?php echo $item["nns_filetype"]; ?></td>
                                                                                                                        <td><?php echo $item["nns_content_id"]; ?></td>
                                                                                                                        <td><?php echo $item["nns_kbps"]; ?></td>
                                                                                                                        <td><?php echo $item["nns_cp_id"]; ?></td>
                                                                                                                        <td> <?php echo get_dimensions_type($item['nns_dimensions']); ?> </td>
                                                                                                                        <td><?php echo $item["nns_import_source"]; ?><br/> <?php echo $item["nns_import_id"]; ?></td>
                                                                                                                        <td><?php echo $item["nns_create_time"]; ?><br/><?php echo $item["nns_modify_time"]; ?></td>
                                                                                                                        <td><?php echo $item["nns_action"];?></td>
                                                                                                                        <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                                                                                                                                <?php if ($pri_edit) { ?>
                                                                                                                                        <a href="../media_pages/nncms_content_media_addedit.php?view_type=<?php echo $view_type; ?>&action=edit&vod_id=<?php echo $nns_id; ?>&index=<?php echo $vod_current_index - 1; ?>&nns_id=<?php echo $item["nns_id"]; ?>" target="_self"><?php echo cms_get_lang('edit'); ?></a>&nbsp;&nbsp;
                                                                                                                                        <a href="javascript:gotoDelete('<?php echo $item["nns_id"]; ?>');" target="_self"><?php echo cms_get_lang('delete'); ?></a>&nbsp;&nbsp;
                                                                                                                                        <a href="./nncms_content_vod_detail.php?op=import_cdn&type=media&sp_id=<?php echo $_GET['sp_id'];?>&nns_id=<?php echo $edit_data['nns_id'];?>&nns_cp_id=<?php echo $edit_data['nns_cp_id'];?>&nns_import_id=<?php echo $item['nns_id'];?>">片源注入</a>
                                                                                                                                <?php } ?>
                                                                                                                                
                                                                                                                        </td>
                                                                                                                </tr>
                                                                                                                <?php
                                                                                                        }
                                                                                                }

																								if($vod_media_delete_data!=null){
																									?>
																									
																									<?php
																									foreach ($vod_media_delete_data as $item) {
                                                                                                                $num++;
                                                                                                                ?>
																									
																									<tr class="file_<?php echo $vod_current_index;?>" style="display: none;">
                                                                                                                        <td><?php echo $num; ?></td>
                                                                                                                        <td><?php echo $item["nns_name"]; ?></td>
                                                                                                                        <td><?php echo $item["nns_url"]; ?></td>
                                                                                                                        <td><?php echo get_contant_language($item["nns_mode"]); ?></td>
                                                                                                                        <td><alt alt="<?php echo get_tag_detail_html($item["nns_tag"]); ?>"><?php echo $item["nns_tag"]; ?></td>
                                                                                                                        <td><?php echo $item["nns_filetype"]; ?></td>
                                                                                                                        <td><?php echo $item["nns_content_id"]; ?></td>
                                                                                                                        <td><?php echo $item["nns_kbps"]; ?></td>
                                                                                                                        <td> <?php echo get_dimensions_type($item['nns_dimensions']); ?> </td>
                                                                                                                        <td><?php echo $item["nns_import_source"]; ?><br/> <?php echo $item["nns_import_id"]; ?></td>
                                                                                                                        <td><?php echo $item["nns_create_time"]; ?><br/><?php echo $item["nns_modify_time"]; ?></td>
                                                                                                                        <td><?php echo $item["nns_action"];?></td>
                                                                                                                        
                                                                                                                        <td  name="<?php echo cms_get_lang('action'); ?>">
                                                                                                                                
                                                                                                                        </td>
                                                                                                                </tr>
																									
																									<?php
																									}

																								}
                                                                                                ?>
                                                                                        </tbody>
                                                                                </table>
                                                                        </div>
                                                                </td>
                                                        </tr>
                                                <?php } ?>
<!--<tr>
<td colspan="2" stype="padding:5px;">
<input name="" type="button" value="<?php echo cms_get_lang('media_bjhb'); ?>" onclick="self.location.href='nncms_content_vod_img_addedit.php?nns_id=<?php echo $nns_id; ?>';"/>&nbsp;&nbsp;
<input name="" type="button" value="<?php echo cms_get_lang('media_control_bjjj'); ?>" onclick="self.location.href='../vod_pages/nncms_content_vod_index_edit.php?nns_id=<?php echo $nns_id; ?>&view_type=<?php echo $view_type; ?>';"/>&nbsp;&nbsp;
                                                <?php if (!$is_online) { ?>
                                                                <input name="" type="button" value="<?php echo cms_get_lang('media_bind_media'); ?>" onclick="self.location.href='../media_pages/nncms_content_media_list.php?nns_id=<?php echo $nns_id; ?>&view_type=<?php echo $view_type; ?>';"/>&nbsp;&nbsp;
                                                <?php } else { ?>
                                                                <input name="" type="button" value="<?php echo cms_get_lang('media_bind_media'); ?>" onclick="self.location.href='../media_pages/nncms_content_media_list.php?nns_id=<?php echo $nns_id; ?>&view_type=<?php echo $view_type; ?>';"/>&nbsp;&nbsp;
                                                <?php } ?>
</td>
</tr>-->
                                        </tbody>
                                </table>
                        </div>
                        <?php if ($pri_edit) { ?>
                                <form id="delete_form" action="../media_pages/nncms_content_media_control.php" method="post">
                                        <input name="action" id="action" type="hidden" value="delete" />
                                        <input name="nns_id" id="nns_id" type="hidden" value="" />
                                        <input name="nns_vod_id" id="nns_vod_id" type="hidden" value="<?php echo $nns_id; ?>" />
                                </form>
                        <?php } ?>
                        <div class="pagecontrol">
                                <?php 
                                if ($current_page > 1) { ?>
                                        <a href="nncms_content_vod_detail.php?page=1&nns_id=<?php echo $nns_id; ?>&sp_id=<?php echo $_GET['sp_id']; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_vod_detail.php?page=<?php echo $current_page - 1; ?>&nns_id=<?php echo $nns_id; ?>&sp_id=<?php echo $_GET['sp_id']; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($current_page < $index_pages) { ?>
                                        <a href="nncms_content_vod_detail.php?page=<?php echo $current_page + 1; ?>&nns_id=<?php echo $nns_id; ?>&sp_id=<?php echo $_GET['sp_id']; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_vod_detail.php?page=<?php echo $index_pages; ?>&nns_id=<?php echo $nns_id; ?>&sp_id=<?php echo $_GET['sp_id']; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $current_page; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_vod_detail.php?ran=1&nns_id=<?php echo $nns_id; ?>&sp_id=<?php echo $_GET['sp_id']; ?>',<?php echo $index_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $current_page . "/" . $index_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_prepage_list();"/>&nbsp;&nbsp;
								
                        </div>

                        <div class="controlbtns">
                                <?php if ($pri_edit) { 
                                		if(empty($_GET['sp_id'])){
                                	?>
                                        <div class="controlbtn info"><a href="nncms_content_vod_addedit.php?action=edit&view_type=<?php echo $view_type; ?>&nns_id=<?php echo $nns_id; ?>"><?php echo cms_get_lang('media_yp_xx'); ?></a></div>
                                        <div class="controlbtn haibao"><a href="nncms_content_vod_img_addedit.php?nns_id=<?php echo $nns_id; ?>"><?php echo cms_get_lang('media_bjhb'); ?></a></div>

                                        <div class="controlbtn fenji"><a  href="../vod_pages/nncms_content_vod_index_edit.php?nns_id=<?php echo $nns_id; ?>&view_type=<?php echo $view_type; ?>"><?php echo cms_get_lang('media_control_bjjj'); ?></a></div>
                                        <div class="controlbtn media"><a href="../media_pages/nncms_content_media_list.php?nns_id=<?php echo $nns_id; ?>&view_type=<?php echo $view_type; ?>"><?php echo cms_get_lang('media_bind_media'); ?></a></div>
                                <?php }} ?> 
                                <div class="controlbtn allselect"><a href="javascript:select_index_all('index');">全选分集</a></div>
                                <div class="controlbtn cancel"><a href="javascript:reset_index_all('index');">取消分集</a></div>
                                <div class="controlbtn move"><a href="javascript:import_task_more('add','index');">注入分集</a></div>
                                <div class="controlbtn allselect"><a href="javascript:select_index_all('media');">全选片源</a></div> 
                                <div class="controlbtn cancel"><a href="javascript:reset_index_all('media');">取消片源</a></div>
                                <div class="controlbtn move"><a href="javascript:import_task_more('add','media');">注入片源</a></div>
                                <div class="controlbtn back"><a href="javascript:go_back();"><?php echo cms_get_lang('back'); ?></a></div>
                                <div style="clear:both;"></div>
                        </div>
                </div>
        </body>
</html>