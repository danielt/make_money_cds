<?php

header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
*
* To change the template for this generated file go to
* Window - Preferences - PHPeclipse - PHP - Code Templates
*/
ob_start();
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}

include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language_media.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "135101");
$checkpri = null;
if (!$pri_bool)
	Header("Location: ../nncms_content_wrong.php");

if ($_SESSION["nns_manager_type"] == 0) {
	$nns_org_id = "";
} else {
	$nns_org_id = $_SESSION["nns_org_id"];
}
$sp_id = $_GET['sp_id'];
$view_type = $_GET["view_type"];
$view_delete = $_GET["view_delete"];
$nns_vod_search = $_GET["search_py"];
$view_audit = $_GET["view_audit"];
$depot_id = $_GET["depot_id"];
$depot_detail_id = strlen($_GET["depot_detail_id"]) != 5 ? $_GET["depot_detail_id"] : '';
$view_type_delete = $_GET["view_delete_type"];
$view_list_max_num = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : (!empty($_COOKIE["page_max_num"]) ?  $_COOKIE["page_max_num"] : 18);

//高级查询
$search_new_py = $nns_vod_search;
$params = array();
if (strlen($nns_vod_search) != 0 || strlen($_GET['search_vod_id']) != 0) {
	if(isset($nns_vod_search) && !empty($nns_vod_search)) {
		$params['nns_name'] = $nns_vod_search;
	}
	if(isset($_GET['search_vod_id']) && !empty($_GET['search_vod_id'])) {
		$params['nns_video_id'] = $_GET['search_vod_id'];
		$video_id_array = array($params['nns_video_id']);
	} else {
		$video_id_array = null;
	}
}
$params['nns_org_id'] = $sp_id;	
$search_yy = $_GET["search_yy"];
$search_dy = $_GET["search_dy"];
$search_point = $_GET["search_point"];
$search_sy_begin_date = $_GET["search_sy_begin_date"];
$search_sy_end_date = $_GET["search_sy_end_date"];
$search_rk_begin_date = $_GET["search_rk_begin_date"];
$search_rk_end_date = $_GET["search_rk_end_date"];
$search_copyright_begin_date = $_GET["search_copyright_begin_date"];
$search_copyright_end_date = $_GET["search_copyright_end_date"];
$search_type = $_GET["search_type"];
$search_episode = $_GET["search_episode"];
$search_dq = $_GET["search_dq"];
$search_len_min = $_GET["search_len_min"];
$search_len_max = $_GET["search_len_max"];
$search_summary = $_GET["search_summary"];
$search_state = $_GET["search_state"];
$search_pic = $_GET["search_pic"];
$searchinfo = array();
$searchinfo["director"] = $search_dy;
$searchinfo["actor"] = $search_yy;
$searchinfo["area"] = $search_dq;
$search_pyerror = $_GET["search_pyerror"];
$search_new_py = strtolower($search_new_py);
$search_vod_id = $_GET['search_vod_id'];

$search_url = "&search_state=$search_state&search_pic=$search_pic&search_py=$search_new_py&nns_vod_search=$nns_vod_search&search_yy=$search_yy&search_dy=$search_dy&search_point=$search_point&search_sy_begin_date=$search_sy_begin_date&search_sy_end_date=$search_sy_end_date&search_rk_begin_date=$search_rk_begin_date" .
"&search_rk_end_date=$search_rk_end_date&search_copyright_begin_date=$search_copyright_begin_date&search_copyright_end_date=$search_copyright_end_date" .
"&search_type=$search_type&search_episode=$search_episode&search_dq=$search_dq&search_len_min=$search_len_min&search_len_max=$search_len_max&search_summary=$search_summary&search_pyerror=$search_pyerror&search_vod_id=$search_vod_id&sp_id=$sp_id";

$nns_url = "&view_type=$view_type&depot_id=$depot_id&depot_detail_id=$depot_detail_id&view_delete=" . $view_delete . "&view_audit=" . $view_audit . "&view_delete_type=" . $view_type_delete . "&view_list_max_num=$view_list_max_num";
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
include($nncms_db_path . "nns_vod/nns_db_vod_class.php");
include $nncms_db_path . "nns_common/nns_db_constant.php";
include $nncms_db_path . "nns_carrier/nns_db_carrier_class.php";
include $nncms_db_path . "nns_partner/nns_db_partner_class.php";
include $nncms_db_path . "nns_group/nns_db_group_class.php";
require_once $nncms_config_path . "op/op_queue.class.php";
//打开DC
$dc = nl_get_dc( array(
		'db_policy'=>NL_DB_WRITE,
		'cache_policy'=>NP_KV_CACHE_TYPE_MEMCACHE
));
$dc->open();
$db = $dc->db();
//include $nncms_db_path . "nns_assist/nns_db_assist_item_class.php";

//  include $nncms_db_path. "nns_service/nns_db_service_detail_video_class.php";
$g_manager_list_max_num = $view_list_max_num;

$op = new op_queue();
$where = !empty($depot_detail_id) && isset($depot_detail_id) ? array('nns_depot_id'=>$depot_id,'nns_category_id'=>$depot_detail_id) : null;

$op_result = $op->get_weight($db, $params, $where);
if (!empty($depot_id)) {
	$vod_inst = new nns_db_vod_class();
	$vod_array = $vod_inst->nns_db_vod_list('nns_id,nns_name',array('nns_name'=>$params['nns_name'],'nns_pinyin'=>$params['nns_name']),null,null,null,$depot_id,$depot_detail_id,null,null,null,null,'',null,null,NNS_VOD_DELETED_FALSE,'weight',null,$params['nns_org_id'],null, null,null,null,null,null,null,null,null,null,null,null,null,null,$video_id_array);

	//$vod_array = $vod_inst->nns_db_vod_list("nns_id,nns_name", $nns_vod_search, $search_state, $view_audit, $search_type, $depot_id, $depot_detail_id, "", "", "", $searchinfo, "", ($currentpage - 1) * $g_manager_list_max_num, $g_manager_list_max_num, $view_delete, null, null, null, $search_sy_begin_date, $search_sy_end_date, $search_rk_begin_date, $search_rk_end_date, $search_copyright_begin_date, $search_copyright_end_date, $search_point
	//		, null, $search_summary, $search_episode, $search_len_min, $search_len_max, $search_py, $search_pic);


	if ($vod_array["ret"] != 0) {
		$data = null;
		echo "<script>alert('" . $vod_array["reason"] . "');</script>";
	}
	$vod_result = $vod_array["data"];
 

	if (isset($_GET['ajax']) && $_GET['ajax'] == 'count_vod') {
		$countArr = $vod_inst->nns_db_vod_count($nns_vod_search, $search_state, null, $search_type, $depot_id, $depot_detail_id, null, null, null, $searchinfo, null, null, null, null, $search_sy_begin_date, $search_sy_end_date, $search_rk_begin_date, $search_rk_end_date, $search_copyright_begin_date, $search_copyright_end_date, $search_point, null, $search_summary, $search_episode, $search_len_min, $search_len_max, $search_py, $search_pic);
		if ($countArr["ret"] == 0)
			$count_all = $countArr["data"][0]["num"];
		echo json_encode(array('count_all' => $count_all));
		exit;
	}
		/*
		//待审核资源 view_audit=0
		$countArr = $vod_inst->nns_db_vod_count($nns_vod_search, $search_state, 0, $search_type, $depot_id, $depot_detail_id, null, null, null, $searchinfo, null, null, null, null, $search_sy_begin_date, $search_sy_end_date, $search_rk_begin_date, $search_rk_end_date, $search_copyright_begin_date, $search_copyright_end_date, $search_point, null, $search_summary, $search_episode, $search_len_min, $search_len_max, $search_py, $search_pic);
		if ($countArr["ret"] == 0)
			$count_check = $countArr["data"][0]["num"];
		//回收站 view_delete=1
		$countArr = $vod_inst->nns_db_vod_count($nns_vod_search, $search_state, null, $search_type, $depot_id, $depot_detail_id, null, null, null, $searchinfo, 1, null, null, null, $search_sy_begin_date, $search_sy_end_date, $search_rk_begin_date, $search_rk_end_date, $search_copyright_begin_date, $search_copyright_end_date, $search_point, null, $search_summary, $search_episode, $search_len_min, $search_len_max, $search_py, $search_pic);
		if ($countArr["ret"] == 0)
			$count_deleted = $countArr["data"][0]["num"];
		echo json_encode(array('count_all' => $count_all, 'count_check' => $count_check, 'count_deleted' => $count_deleted));
		exit;
	} */
	$result = array();
	if(is_array($vod_result) && is_array($op_result)) {
		$result = array_merge($op_result,$vod_result);
	} else {
		if(is_array($vod_result)) {
			$result = $vod_result;
		}
		if(is_array($op_result)) {
			$result = $op_result;
		}
	}
	$vod_total_num = count($result);
	//ajax 取总数（所有资源，待审核资源，回收站）
	
	$vod_pages = ceil($vod_total_num / $g_manager_list_max_num);
	
	$currentpage = 1;
	$data = array();
	if (!empty($_GET["page"])) {
		$currentpage = $_GET["page"];
	}
	$start = ($currentpage-1)*$g_manager_list_max_num;
	for($i=$start;$i<($g_manager_list_max_num+$start);$i++) {
		if(empty($result[$i])) {
			break;
		}
		$data[$i] = $result[$i];
	}
}
//  echo "<pre>";
// var_dump($data);die; 
$carrier_inst = new nns_db_carrier_class();
$partner_inst = new nns_db_partner_class();
$group_inst = new nns_db_group_class();
/**
 * 扩展语言 BY S67
*/
$extend_langs_str = g_cms_config::get_g_extend_language();
$extend_langs_str = trim($extend_langs_str);
/**
 * 剧照 by cb
*/
$stills_enabled = g_cms_config::get_g_config_value('g_stills_enabled');


ob_end_clean();
?>
<!DOCTYPE html >
<html >
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../../js/cms_cookie.js"></script>
                <script language="javascript" src="../../js/table.js.php"></script>
                <script language="javascript" src="../../js/checkinput.js.php"></script>
                <script language="javascript" src="../../js/rate.js"></script>
                <script language="javascript" src="../../js/image_loaded_func.js"></script>
                <script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
                <script language="javascript">
                        function checkhiddenInput() {

                                var par = getAllCheckBoxSelect("online");
                                $("#action").attr("value", "delete");
                                if (par == "") {
<?php if ($view_type == 0) { ?>
                                                alert("<?php echo cms_get_lang('media_select_dy'); ?>");
<?php } else if ($view_type == 1) { ?>
                                                alert("<?php echo cms_get_lang('media_select_lxj'); ?>");
<?php } ?>
                                } else {
                                        $('#nns_id').attr('value', getAllCheckBoxSelect("online"));
                                        checkForm('<?php echo cms_get_lang('dbgl'); ?>', '<?php echo cms_get_lang('msg_ask_delete'); ?>', $('#delete_form'));
                                }
                        }
                        
                        
                        function get_select(){
                        	par = getAllCheckBoxSelect();
                        	return par;
                        }

                        function get_tags_edit(tags) {
                                $("#action").attr("value", "ctag");
                                $("#nns_tags").attr("value", tags);
                                var par = getAllCheckBoxSelect();
                                if (par == "") {
<?php if ($view_type == 0) { ?>
                                                alert("<?php echo cms_get_lang('media_select_dy'); ?>");
<?php } else if ($view_type == 1) { ?>
                                                alert("<?php echo cms_get_lang('media_select_lxj'); ?>");
<?php } ?>
                                } else {
                                        $('#nns_id').attr('value', par);
                                        checkForm('<?php echo cms_get_lang('dbgl'); ?>', '<?php echo cms_get_lang('msg_ask_ctag'); ?>', $('#delete_form'));
                                }
                        }

                        function checkhiddenBox(type) {
                                BoxKey = false;
                                $("input.checkhiddenInput:checked").each(function() {
                                        if ($(this).attr('rel') == type) {

                                                BoxKey = true;
                                                return false;
                                        }


                                })
                                return BoxKey;
                        }
                        function checkhiddenInput_audit(bool) {
                                if (bool) {
                                        var checkType = 1;
                                } else {
                                        var checkType = 0;
                                }
                                var ifBoxKey = checkhiddenBox(checkType);//验证是否已经通过审核
                                if (ifBoxKey && checkType) {
                                        alert('<?php echo cms_get_lang('js_msg_vodlist_audit'); ?>');
                                        return false;
                                } else if (ifBoxKey && !checkType) {

                                        alert('<?php echo cms_get_lang('js_msg_vodlist_audit_cancel'); ?>');
                                        return false;
                                } else {

                                }

                                var par = getAllCheckBoxSelect("online");
                                $("#action").attr("value", "audit");
                                if (bool) {
                                        $("#audit").attr("value",<?php echo NNS_VOD_CHECK_OK; ?>);
                                } else {
                                        $("#audit").attr("value",<?php echo NNS_VOD_CHECK_WAITING; ?>);
                                }
                                if (par == "") {
<?php if ($view_type == 0) { ?>
                                                alert("<?php echo cms_get_lang('media_select_dy'); ?>");
<?php } else if ($view_type == 1) { ?>
                                                alert("<?php echo cms_get_lang('media_select_lxj'); ?>");
<?php } ?>
                                } else {
                                        $('#nns_id').attr('value', getAllCheckBoxSelect("online"));
                                        checkForm('<?php echo cms_get_lang('dbgl'); ?>', '<?php echo cms_get_lang('msg_ask_audit'); ?>', $('#delete_form'));
                                }

                        }


                        function reset_vod_delete() {
                                var par = getAllCheckBoxSelect("online");
                                $("#action").attr("value", "reset");
                                if (par == "") {
<?php if ($view_type == 0) { ?>
                                                alert("<?php echo cms_get_lang('media_select_dy'); ?>");
<?php } else if ($view_type == 1) { ?>
                                                alert("<?php echo cms_get_lang('media_select_lxj'); ?>");
<?php } ?>
                                } else {
                                        $('#nns_id').attr('value', getAllCheckBoxSelect("online"));
                                        checkForm('<?php echo cms_get_lang('dbgl'); ?>', '<?php echo cms_get_lang('msg_ask_reset'); ?>', $('#delete_form'));
                                }
                        }
                        function vod_real_delete() {
                                var par = getAllCheckBoxSelect("online");
                                $("#action").attr("value", "realdelete");
                                if (par == "") {

                                        alert("<?php echo cms_get_lang('media_select_jm'); ?>");

                                } else {
                                        $('#nns_id').attr('value', getAllCheckBoxSelect("online"));
                                        checkForm('<?php echo cms_get_lang('zbgl'); ?>', '<?php echo cms_get_lang('msg_ask_delete'); ?>', $('#delete_form'));
                                }
                        }

                        function set_move_category_id(value, depot_id) {
                                var par = getAllCheckBoxSelect();
                                $("#delete_form").find("#move_category_id").val(value);
                                $("#delete_form").find("#move_depot_id").val(depot_id);
                                $("#delete_form").find("#action").val("move_category");
                                if (par == "") {
                                        alert("<?php echo cms_get_lang('media_select_jm'); ?>");
                                } else {
                                        $('#nns_id').attr('value', getAllCheckBoxSelect());
                                        checkForm('<?php echo cms_get_lang('dbgl'); ?>', '<?php echo cms_get_lang('msg_ask_change'); ?>', $('#delete_form'));
                                }
                        }

                        function set_vod_unline(value) {
                                if (confirm("<?php echo cms_get_lang('msg_force_xx'); ?>")) {
                                        $("#delete_form").find("#nns_id").val(value);
                                        $("#delete_form").find("#action").val("unline");
                                        checkForm('<?php echo cms_get_lang('dbgl'); ?>', '<?php echo cms_get_lang('msg_ask_change'); ?>', $('#delete_form'));
                                }
                        }

                        function begin_select_category() {
                                var par = getAllCheckBoxSelect();
                                if (par == "") {


                                        alert("<?php echo cms_get_lang('msg_4'); ?>");
                                } else {
                                        window.parent.begin_select_category();
                                }
                        }
                        /*****************************************/
                		function add_action(num) {
                			for(var i=1;i<=<?php echo $vod_total_num; ?>;i++) {
                				$("#old_weight_"+i).show();
                				$("#new_weight_"+i).hide();
                				$("#control_old_"+i).show();
                				$("#control_new_"+i).hide();
                				$("#btns_old_"+i).show();
                				$("#btns_new_"+i).hide();
                			}
                			$("#old_weight_"+num).hide();
                			$("#new_weight_"+num).show();
                			$("#control_old_"+num).hide();
                			$("#control_new_"+num).show();			
                		}
                		function update_action(num) {
                			for(var i=1;i<=<?php echo $vod_total_num; ?>;i++) {
                				$("#old_weight_"+i).show();
                				$("#new_weight_"+i).hide();
                				$("#control_old_"+i).show();
                				$("#control_new_"+i).hide();
                				$("#btns_old_"+i).show();
                				$("#btns_new_"+i).hide();
                			}
                			$("#old_weight_"+num).hide();
                			$("#new_weight_"+num).show();
                			$("#btns_old_"+num).hide();
                			$("#btns_new_"+num).show();	
                		}
                		function add_weight(id,sp,num,action) {
                			var weight = $("#input_weight_"+num).val();
                			if(weight > 65535 || weight < 1) {
                				alert('添加失败,权重值起始值为1结束值为65535');
                			} else {
                				action_weight(id,weight,action,sp);
                			}
                		}
                		function update_weight(id,sp,num,action) {
                			var weight = $("#input_weight_"+num).val();
                			if(weight > 65535 || weight < 1) {
                				alert('修改失败,权重值起始值为1结束值为65535');
                			} else {
                				action_weight(id,weight,action,sp);
                			}
                		}
                		function delete_weight(id,sp,num,action) {
                			var w = $("#old_weight_"+num).html();
                			if(confirm("确定删除权重吗?")) {
                				action_weight(id,w,action,sp);
                			}
                		}
                		function cancel() {
                			for(var i=1;i<=<?php echo $vod_total_num; ?>;i++) {
                				$("#old_weight_"+i).show();
                				$("#new_weight_"+i).hide();
                				$("#control_old_"+i).show();
                				$("#control_new_"+i).hide();
                				$("#btns_old_"+i).show();
                				$("#btns_new_"+i).hide();
                			}
                			//self.location.reload();
                		}
                		function action_weight(id,weight,action,sp) {
                			params = {
                					  'nns_id': id,
                					  'sp_id': sp,
                					  'weight': weight,
                					  'action': action,
                					 }
                					 $.post('nncms_content_control_op.php', params, function(data) {
                					  window.location.reload();
                					 }, 'json');
                			//window.location.href='nncms_content_control_op.php?action='+action+'&sp_id='+sp+'&nns_id='+id+'&weight='+weight;
                		}

                        /*****************************************/
                        
                        $(document).ready(function() {
                                window.parent.now_frame_url = window.location.href;
                        });

                </script>
        </head>

        <body>
                <div class="content">
                                    <!--<div class="content_position"><?php echo cms_get_lang('dbgl'); ?> > <?php
                        if ($view_type == 0) {
                                cms_get_lang('media_dy|dbgl');
                        } else if ($view_type == 1) {
                                echo cms_get_lang('media_lxj|dbgl');
                        }
                        ?></div>-->
                        <div class="content_table formtable">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <thead>
                                                <tr>
                                                        <th><input name="" type="checkbox" value="" /></th>
                                                        <th><?php echo cms_get_lang('segnumber'); ?></th>
                                                        <th><?php echo cms_get_lang('name'); ?></th>
                                                        <th>权重</th>
                                                        <th><?php echo cms_get_lang('action'); ?></th>

                                                </tr>
                                        </thead>
                                        <tbody>
                                                <?php
                                                if ($data != null) {
                                                        $num = ($currentpage - 1) * $g_manager_list_max_num;
                                                        foreach ($data as $item) {
                                                                $num++;
//          计算片源是否都已经绑定
                                                                $is_bind = $vod_inst->check_bind_state($item["nns_id"], $item["nns_all_index"]);
//        	计算是否上线

                                                                ?>
                                                                <tr>
                                                                        <td><input name="input" class="checkhiddenInput" rel="<?php echo $item["nns_check"]; ?>" type="checkbox" value="<?php echo isset($item['nns_video_id']) ? $item["nns_video_id"] : $item['nns_id']; ?>"  <?php
                                                                                if ($is_online == true) {
                                                                                        echo "online='true'";
                                                                                }
                                                                                ?> />
                                                                            
                                                                        </td>
                                                                        <td><?php echo $num; ?></td>
                                                                        <td><alt alt="<?php echo pub_func_get_vod_alt($item); ?>">
                                                                                        <a href="../sp_content_pages_v2/nncms_content_vod_detail.php?sp_id=<?php echo $_GET['sp_id'];?>&view_type=<?php echo $view_type; ?>&nns_id=<?php echo isset($item['nns_video_id']) ? $item["nns_video_id"] : $item['nns_id']; ?>"
                                                                                           <?php if ($item["nns_state"] == 1) { ?>class="import"
                                                                                           <?php } else if ($item["nns_state"] == 2) { ?>class="importx"
                                                                                           <?php } else { ?>  class="link" <?php } ?>
                                                                                           target="_self">
                                                                                                   <?php
                                                                                                   if (mb_strlen($item["nns_name"], 'utf-8') != $item['nns_pinyin_length']) {
                                                                                                           $item["nns_name"] = "<font color='red'>{$item["nns_name"]} </font>";
                                                                                                   }
                                                                                                   echo $item["nns_name"] . '&nbsp;&nbsp;' . $item['nns_pinyin'];
                                                                                                   ?>
                                                                                        </a></alt></td>

                                                                        <td>
                                                                        	<span id="old_weight_<?php echo $num; ?>"><?php echo isset($item['nns_weight']) ? $item['nns_weight'] : 0; ?></span>
																			<span id="new_weight_<?php echo $num; ?>" style="display:none;"><input type="text" id="input_weight_<?php echo $num; ?>" value="10000" style="width:45px;"/></span>
                                                                        </td>                                                                                                                                                                                                                                                        <!--<td><?php echo $item["nns_create_time"]; ?> </td>-->
                                                                        <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                                                                           <?php
												                       			if($item['nns_weight'] < 10000) {
																				?>
												                       			<span id="control_old_<?php echo $num; ?>"><a href="javascript:add_action(<?php echo $num; ?>);">添加权重</a></span>
												                        		<span id="control_new_<?php echo $num; ?>" style="display:none;">
												                        			<a href="javascript:add_weight('<?php echo $item['nns_id']; ?>','<?php echo $sp_id; ?>',<?php echo $num; ?>,'add');">确定</a>
												                        			<a href="javascript:cancel();">取消</a>
												                        		</span>
												                        		<?php 
												                        		}
												                        		if($item['nns_weight'] >= 10000) {
												                        		?>
												                        		<span id="btns_old_<?php echo $num; ?>">
												                        			<a href="javascript:update_action(<?php echo $num; ?>);">修改权重</a>
												                        			<a href="javascript:delete_weight('<?php echo $item['nns_video_id']; ?>','<?php echo $sp_id; ?>',<?php echo $num; ?>,'delete');">删除权重</a>
												                        		</span>
												                        		<span id="btns_new_<?php echo $num; ?>" style="display:none;">
												                        			<a href="javascript:update_weight('<?php echo $item['nns_video_id']; ?>','<?php echo $sp_id; ?>',<?php echo $num; ?>,'update');">确定</a>
												                        			<a href="javascript:cancel();">取消</a>
												                        		</span>
												                        	<?php }?>    
    
                                                                        </td>
                                                                </tr>
                                                                <?php
                                                        }
                                                } $least_num = $g_manager_list_max_num - count($data);
                                                for ($i = 0; $i < $least_num; $i++) {
                                                        ?>
                                                        <tr>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                        </tr>

                                                <?php } ?>
                                        </tbody>
                                </table>
                        </div>
                        <div class="pagecontrol">
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_op_control_bk.php?sp_id=<? echo $_GET['sp_id'];?>&page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_op_control_bk.php?sp_id=<? echo $_GET['sp_id'];?>&page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_content_op_control_bk.php?sp_id=<? echo $_GET['sp_id'];?>&page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_op_control_bk.php?sp_id=<? echo $_GET['sp_id'];?>&page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_op_control_bk.php?sp_id=<? echo $_GET['sp_id'];?>&ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_prepage_list();"/>&nbsp;&nbsp;

                        </div>
                        <!--<div class="controlbtns">
                                <div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);"><?php echo cms_get_lang('select'); ?></a></div>
                                <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);"><?php echo cms_get_lang('cancel'); ?></a></div>
                                <?php
                                if ($view_delete == "" && $view_audit == "") {
                                        if ($view_type_delete != 1) {
                                                ?>
                                                <div class="controlbtn add"><a href="javascript:void(null);" pos="nncms_content_vod_addedit.php?view_type=<?php echo $view_type; ?>&depot_id=<?php echo $depot_id; ?>&depot_detail_id=<?php echo $depot_detail_id; ?>" action="add"><?php echo cms_get_lang('add'); ?></a></div>
                                                <div class="controlbtn edit"><a href="javascript:void(null);" pos="nncms_content_vod_addedit.php?view_type=<?php echo $view_type; ?>&depot_id=<?php echo $depot_id; ?>&depot_detail_id=<?php echo $depot_detail_id; ?>" action="edit"><?php echo cms_get_lang('edit'); ?></a></div>
                                                <div class="controlbtn move"><a href="javascript:begin_select_category();" action="edit"><?php echo cms_get_lang('move_catalog'); ?></a></div>
                                        <?php } ?>

                                <?php }if (($view_delete == "") && $view_type_delete != 1) { ?>
                                        <div class="controlbtn delete"><a href="javascript:checkhiddenInput();"  action="delete"><?php echo cms_get_lang('delete'); ?></a></div>
                                        <div class="controlbtn audit"><a href="###" onclick="return checkhiddenInput_audit(true);"  action="audit"><?php echo cms_get_lang('access_audit'); ?></a></div>
                                        <div class="controlbtn cancel_audit"><a href="###" onclick="return checkhiddenInput_audit(false);"  action="audit"><?php echo cms_get_lang('cancel|audit'); ?></a></div>
                                <?php } ?>
                                <?php if ($view_delete != "" && $view_type_delete != 1) { ?>
                                        <div class="controlbtn back"><a href="javascript:reset_vod_delete();"  action="reset"><?php echo cms_get_lang('reset'); ?></a></div>
                                        <div class="controlbtn real_delete"><a href="javascript:vod_real_delete();"  action="delete"><?php echo cms_get_lang('real_delete'); ?></a></div>
                                <?php } ?>
                                <div class="controlbtn edit"><a href="javascript:window.parent.begin_show_tags();" action="edit"><?php echo cms_get_lang('edit_epg'); ?></a></div>
                                <div style="clear:both;"></div>
                        </div>-->
                        <form id="delete_form" action="nncms_content_vod_control.php" method="post">
                                <input name="action" id="action" type="hidden" value="delete" />
                                <input name="view_type" id="view_type" type="hidden" value="<?php echo $view_type; ?>" />
                                <input name="nns_id" id="nns_id" type="hidden" value="" />
                                <input name="nns_tags" id="nns_tags" type="hidden" value="" />
                                <input name="audit" id="audit" type="hidden" value="" />
                                <input name="nns_url" id="nns_url" type="hidden" value="<?php echo $nns_url; ?>"/>
                                <input name="move_category_id" id="move_category_id" type="hidden" value=""/>
                                <input name="move_depot_id" id="move_depot_id" type="hidden" value=""/>
                        </form>
                </div>
        </body>
</html>
<?php
$vod_inst = null;
$carrier_inst = null;
$partner_inst = null;
$group_inst = null;
$assist_inst = null;
$service_inst = null;
?>