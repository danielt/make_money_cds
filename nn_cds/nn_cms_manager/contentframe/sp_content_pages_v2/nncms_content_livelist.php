<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
ob_start();
include("../../nncms_manager_inc.php");
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
// 导入多语言包
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/playbill.class.php';

//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/language_contant.php";

//op=cdn&sp_id=<?php echo g_cms_config::get_g_config_value('g_sp_org_id');\>&video_id=<?php echo $item["nns_id"]; >&op_action=add
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
/**
 * 
 * 
 * 注入到epg==============================================
 * 
 * 
 * 
 */
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));

if(isset($_GET['op'])){
	
	$db = $dc->db();
	$live_id = $_GET['video_id'];
	if(empty($live_id)){
		$re_str = '操作失败片源id为空';
		echo '<script>alert("'.$re_str.'");history.go(-1);</script>';die;
	}
	$config_sp_id = g_cms_config::get_g_config_value('g_bk_sp');
	$live_info = array();
	$str_result='';
	if($_GET['op']=='cdn')
	{
		$re = false;
		if($config_sp_id != 0)
		{
			if($_GET['sp_id']!=g_cms_config::get_g_config_value('g_bk_sp'))
			{
				$re_str = 'sp_id与配置的g_bk_sp不匹配';
				echo '<script>alert("'.$re_str.'");history.go(-1);</script>';die;
			}
			$sql = "select * from nns_live where nns_id='$live_id' and nns_deleted!=1";
			$live_info = nl_db_get_one($sql, $db);
			if($live_info===false||$live_info===null){
				$re_str = '操作失败';
				echo '<script>alert("'.$re_str.'");history.go(-1);</script>';die;
			}
			$live_info['nns_task_name'] = $live_info['nns_name'];
			$live_info['nns_task_id'] = $live_info['nns_id'];
			unset($live_info['nns_category_id']);
			$date = date('Y-m-d').' 00:00:00';
			$check_sql = "SELECT count(*) as num FROM `nns_mgtvbk_c2_log` WHERE `nns_task_id`='".$live_info['nns_task_id']."' AND  `nns_task_type` = 'Channel' AND `nns_org_id` = '".$_GET['sp_id']."' AND `nns_create_time` >= '".$date."'";
			$num = nl_db_get_col($check_sql, $db);
			if($num>0){
				echo '<script>alert("今天已经注入过一次了，请删除了注入日志再次注入");history.go(-1);</script>';exit;
			}
			switch ($_GET['op_action']) {
				case 'add':
					$re = c2_task_model::add_live($live_info,$_GET['sp_id']);
				break;
				case 'modify':
					$re = c2_task_model::update_live($live_info,$_GET['sp_id']);
				break;
				case 'destroy':
					$re = c2_task_model::delete_live($live_info,$_GET['sp_id']);
				break;
			}
		}
	}
	else if($_GET['op']=='cdn_live')
	{
		$live_info['nns_channel_id'] = $live_id;
		$result = c2_task_model::do_live($live_info,'REGIST',$_GET['sp_id']);
		$re = ($result['ret'] == 0) ? true : false;
		$str_result = $result['reason'];
	}
	else if($_GET['op']=='epg_live')
	{
	    $live_info['nns_channel_id'] = $live_id;
	    $result = import_model::epg_live($dc,$live_info,'REGIST',$_GET['sp_id']);
	    $re = ($result['ret'] == 0) ? true : false;
	    $str_result = $result['reason'];
	}
	else if($_GET['op']=='cdn_playbill')
	{
		$result_playbill_info=nl_playbill::query_by_live_id($dc, $live_id, date("Y-m-d",strtotime("-7 day")));
		if($result_playbill_info['ret'] !=0)
		{
			echo '<script>alert("'.$result_playbill_info['data_info'].'");history.go(-1);</script>';exit;
		}
		if(!isset($result_playbill_info['data_info']) || empty($result_playbill_info['data_info']) || !is_array($result_playbill_info['data_info']))
		{
			echo '<script>alert("查无节目单数据");history.go(-1);</script>';exit;
		}
		$result_playbill_info=$result_playbill_info['data_info'];
		$temp_array_error = array();
		foreach ($result_playbill_info as $playbill_val)
		{
			$temp_playbill_info = array('nns_playbill_id'=>$playbill_val['nns_id']);
			$re = c2_task_model::add_playbill($temp_playbill_info,'REGIST',$_GET['sp_id']);
			if($re['ret'] !=0)
			{
				$temp_array_error[] = "节目单ID:[{$playbill_val['nns_id']}][{$re['reason']}]";
			}
		}
		$str_result = implode(';', $temp_array_error);
	}
	if($re){
		$re_str = '操作成功'.$str_result;
	}else{
		$re_str = '操作失败'.$str_result;
	}
	echo '<script>alert("'.$re_str.'");history.go(-1);</script>';exit;
	die;
}
/**
 * 注入到epg    结束================================================
 * 
 */

if (count($_POST) == 3) {
        include $nncms_config_path . "nn_logic/mgtv/func_mgtv_boss.php";
        $list = notice_boss_live_add($_POST['vid'], $_POST['vname'], $_POST['type']);
        if ($list)
                exit("1");
        exit("0");
}

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "135005");
$checkpri = null;
if (!$pri_bool)
        Header("Location: ../nncms_content_wrong.php");



$view_delete = $_GET["view_delete"];
$nns_live_search = $_GET["nns_live_search"];
$view_audit = $_GET["view_audit"];
$view_type_delete = $_GET["view_delete_type"];
$depot_id = $_GET["depot_id"];
$depot_detail_id = $_GET["depot_detail_id"];
$view_list_max_num = $_GET["view_list_max_num"];

//高级查询
$search_py = $_GET["search_py"];
$search_yy = $_GET["search_yy"];
$search_dy = $_GET["search_dy"];
$search_point = $_GET["search_point"];
$search_sy_begin_date = $_GET["search_sy_begin_date"];
$search_sy_end_date = $_GET["search_sy_end_date"];
$search_rk_begin_date = $_GET["search_rk_begin_date"];
$search_rk_end_date = $_GET["search_rk_end_date"];
$search_copyright_begin_date = $_GET["search_copyright_begin_date"];
$search_copyright_end_date = $_GET["search_copyright_end_date"];
$search_type = $_GET["search_type"];
$search_episode = $_GET["search_episode"];
$search_dq = $_GET["search_dq"];
$search_len_min = $_GET["search_len_min"];
$search_len_max = $_GET["search_len_max"];
$search_summary = $_GET["search_summary"];


$searchinfo = array();
$searchinfo["director"] = $search_dy;
$searchinfo["actor"] = $search_yy;
$searchinfo["area"] = $search_dq;
$search_py = strtolower($search_py);

$search_url = "&search_py=$search_py&nns_live_search=$nns_live_search&search_yy=$search_yy&search_dy=$search_dy&search_point=$search_point&search_sy_begin_date=$search_sy_begin_date&search_sy_end_date=$search_sy_end_date&search_rk_begin_date=$search_rk_begin_date" .
        "&search_rk_end_date=$search_rk_end_date&search_copyright_begin_date=$search_copyright_begin_date&search_copyright_end_date=$search_copyright_end_date" .
        "&search_type=$search_type&search_episode=$search_episode&search_dq=$search_dq&search_len_min=$search_len_min&search_len_max=$search_len_max&search_summary=$search_summary";

$nns_url = "&depot_id=$depot_id&depot_detail_id=$depot_detail_id&view_delete=" . $view_delete . "&view_audit=" . $view_audit . "&view_delete_type=" . $view_type_delete . "&view_list_max_num=$view_list_max_num";

// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
include($nncms_db_path . "nns_live/nns_db_live_class.php");
include $nncms_db_path . "nns_common/nns_db_constant.php";
include $nncms_db_path . "nns_carrier/nns_db_carrier_class.php";
include $nncms_db_path . "nns_partner/nns_db_partner_class.php";
include $nncms_db_path . "nns_group/nns_db_group_class.php";
//include $nncms_db_path . "nns_assist/nns_db_assist_item_class.php";
//include $nncms_db_path . "nns_service/nns_db_service_category_class.php";
//  include $nncms_db_path. "nns_service/nns_db_service_detail_video_class.php";

//$assist_inst = new nns_db_assist_item_class();
//$service_inst = new nns_db_service_category_class();
//  $service_item_inst=new nns_db_service_detail_video_class();

if (!empty($_COOKIE["page_max_num"])) {
        $g_manager_list_max_num = $_COOKIE["page_max_num"];
}

if (!empty($depot_id)) {
        $live_inst = new nns_db_live_class();
        $countArr = $live_inst->nns_db_live_count($nns_live_search, "", $view_audit, $search_type, $depot_id, $depot_detail_id, null, null, null, $searchinfo, $view_delete, null, null, null, $search_sy_begin_date, $search_sy_end_date, $search_rk_begin_date, $search_rk_end_date, $search_copyright_begin_date, $search_copyright_end_date, $search_point, null, $search_summary, $search_episode, $search_len_min, $search_len_max, $search_py);
        if ($countArr["ret"] == 0)
                $live_total_num = $countArr["data"][0]["num"];

        //ajax 取总数（所有资源，待审核资源，回收站）
        if (isset($_GET['ajax']) && $_GET['ajax'] == 'count_live') {
                //wuha: 将下面实参$view_audit(第三个参数)改为了传入空字符串，修复点击待审核资源后 所有资源里数量跟审核相同 - 2013-4-11
                $countArr = $live_inst->nns_db_live_count($nns_live_search, "", '', $search_type, $depot_id, $depot_detail_id, null, null, null, $searchinfo, null, null, null, null, $search_sy_begin_date, $search_sy_end_date, $search_rk_begin_date, $search_rk_end_date, $search_copyright_begin_date, $search_copyright_end_date, $search_point, null, $search_summary, $search_episode, $search_len_min, $search_len_max, $search_py);
                $count_all = 0;
                if ($countArr["ret"] == 0) {
                        $count_all = $countArr["data"][0]["num"];
                }
                //待审核资源 view_audit=0
                $countArr = $live_inst->nns_db_live_count($nns_live_search, "", 0, $search_type, $depot_id, $depot_detail_id, null, null, null, $searchinfo, null, null, null, null, $search_sy_begin_date, $search_sy_end_date, $search_rk_begin_date, $search_rk_end_date, $search_copyright_begin_date, $search_copyright_end_date, $search_point, null, $search_summary, $search_episode, $search_len_min, $search_len_max, $search_py);
                $count_check = 0;
                if ($countArr["ret"] == 0) {
                        $count_check = $countArr["data"][0]["num"];
                }
                //回收站 view_delete=1
                $countArr = $live_inst->nns_db_live_count($nns_live_search, "", null, $search_type, $depot_id, $depot_detail_id, null, null, null, $searchinfo, 1, null, null, null, $search_sy_begin_date, $search_sy_end_date, $search_rk_begin_date, $search_rk_end_date, $search_copyright_begin_date, $search_copyright_end_date, $search_point, null, $search_summary, $search_episode, $search_len_min, $search_len_max, $search_py);
                $count_deleted = 0;
                if ($countArr["ret"] == 0) {
                        $count_deleted = $countArr["data"][0]["num"];
                }
                echo json_encode(array('count_all' => $count_all, 'count_check' => $count_check, 'count_deleted' => $count_deleted));
                exit;
        }
        $live_pages = ceil($live_total_num / $g_manager_list_max_num);
        $currentpage = 1;
        if (!empty($_GET["page"]))
                $currentpage = $_GET["page"];
        $live_array = $live_inst->nns_db_live_list("*", $nns_live_search, "", $view_audit, $view_type, $depot_id, $depot_detail_id, "", "", "", $searchinfo, "", ($currentpage - 1) * $g_manager_list_max_num, $g_manager_list_max_num, $view_delete, null, null, null, $search_sy_begin_date, $search_sy_end_date, $search_rk_begin_date, $search_rk_end_date, $search_copyright_begin_date, $search_copyright_end_date, $search_point, null, $search_summary, $search_episode, $search_len_min, $search_len_max, $search_py);

// var_dump($manager_array);
        if ($live_array["ret"] != 0) {
                $data = null;
                echo "<script>alert(" . $live_array["reason"] . ");</script>";
        }
        $data = $live_array["data"];
}

$carrier_inst = new nns_db_carrier_class();
$partner_inst = new nns_db_partner_class();
$group_inst = new nns_db_group_class();

/**
 * 扩展语言 BY S67
 */
$extend_langs_str = g_cms_config::get_g_extend_language();
$extend_langs_str = trim($extend_langs_str);

/**
 * 剧照 by cb
 */
$stills_enabled = g_cms_config::get_g_config_value('g_stills_enabled');

ob_end_clean();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../../js/cms_cookie.js"></script>
                <script language="javascript" src="../../js/table.js.php"></script>
                <script language="javascript" src="../../js/checkinput.js.php"></script>
                <script language="javascript" src="../../js/rate.js"></script>
                <script language="javascript" src="../../js/image_loaded_func.js"></script>
                <script language="javascript">
                        function checkhiddenInput() {
                                var par = getAllCheckBoxSelect("online");
                                $("#action").attr("value", "delete");
                                if (par == "") {

                                        alert("<?php echo cms_get_lang('msg_select_lm'); ?>");

                                } else {
                                        $('#nns_id').attr('value', getAllCheckBoxSelect("online"));
                                        checkForm('<?php echo cms_get_lang('media_zbgl'); ?>', '<?php echo cms_get_lang('msg_ask_delete'); ?>', $('#delete_form'));
                                }

                        }

                        function checkhiddenBox(type) {
                                BoxKey = false;
                                $("input.checkhiddenInput:checked").each(function() {
                                        if ($(this).attr('rel') == type) {

                                                BoxKey = true;
                                                return false;
                                        }


                                })
                                return BoxKey;
                        }

                        function checkhiddenInput_audit(bool) {

                                if (bool) {
                                        var checkType = 1;
                                } else {
                                        var checkType = 0;
                                }
                                var ifBoxKey = checkhiddenBox(checkType);//验证是否已经通过审核
                                if (ifBoxKey && checkType) {
                                        alert('<?php echo cms_get_lang('js_msg_vodlist_audit'); ?>');
                                        return false;
                                } else if (ifBoxKey && !checkType) {

                                        alert('<?php echo cms_get_lang('js_msg_vodlist_audit_cancel'); ?>');
                                        return false;
                                } else {

                                }

                                var par = getAllCheckBoxSelect();
                                $("#action").attr("value", "audit");
                                if (bool) {
                                        $("#audit").attr("value",<?php echo NNS_LIVE_CHECK_OK; ?>);
                                } else {
                                        $("#audit").attr("value",<?php echo NNS_LIVE_CHECK_WAITING; ?>);
                                }
                                if (par == "") {
                                        alert("<?php echo cms_get_lang('media_select_jm'); ?>");

                                } else {
                                        $('#nns_id').attr('value', getAllCheckBoxSelect());
                                        checkForm('<?php echo cms_get_lang('media_zbgl'); ?>', '<?php echo cms_get_lang('msg_ask_audit'); ?>', $('#delete_form'));
                                }

                        }

                        function reset_live_delete() {
                                var par = getAllCheckBoxSelect("online");
                                $("#action").attr("value", "reset");
                                if (par == "") {

                                        alert("<?php echo cms_get_lang('media_select_jm'); ?>");

                                } else {
                                        $('#nns_id').attr('value', getAllCheckBoxSelect("online"));
                                        checkForm('<?php echo cms_get_lang('media_zbgl'); ?>', '<?php echo cms_get_lang('msg_ask_reset'); ?>', $('#delete_form'));
                                }
                        }
                        function live_real_delete() {
                                var par = getAllCheckBoxSelect("online");
                                $("#action").attr("value", "realdelete");
                                if (par == "") {

                                        alert("<?php echo cms_get_lang('media_select_jm'); ?>");

                                } else {
                                        $('#nns_id').attr('value', getAllCheckBoxSelect("online"));
                                        checkForm('<?php echo cms_get_lang('media_zbgl'); ?>', '<?php echo cms_get_lang('msg_ask_delete'); ?>', $('#delete_form'));
                                }
                        }
                        function set_move_category_id(value, depot_id) {
                                var par = getAllCheckBoxSelect();
                                $("#delete_form").find("#move_category_id").val(value);
                                $("#delete_form").find("#move_depot_id").val(depot_id);
                                $("#delete_form").find("#action").val("move_category");
                                if (par == "") {
                                        alert("<?php echo cms_get_lang('media_select_jm'); ?>");
                                } else {
                                        $('#nns_id').attr('value', getAllCheckBoxSelect());
                                        checkForm('<?php echo cms_get_lang('media_zbgl'); ?>', '<?php echo cms_get_lang('msg_ask_change'); ?>', $('#delete_form'));
                                }
                        }
                        function set_vod_unline(value) {
                                if (confirm("<?php echo cms_get_lang('msg_force_xx'); ?>")) {
                                        $("#delete_form").find("#nns_id").val(value);
                                        $("#delete_form").find("#action").val("unline");
                                        checkForm('<?php echo cms_get_lang('media_zbgl'); ?>', '<?php echo cms_get_lang('msg_ask_change'); ?>', $('#delete_form'));
                                }
                        }
                        function begin_select_category() {
                                var par = getAllCheckBoxSelect();
                                if (par == "") {
                                        alert("<?php echo cms_get_lang('msg_select_one_data'); ?>");
                                } else {
                                        window.parent.begin_select_category();
                                }
                        }

                        $(document).ready(function() {
                                                $('.boosup').click(function() {
                                var vtype = $(this).attr('vtype');
                                var vname = $(this).attr('vname');
                                var vid = $(this).attr('vid');
                                $.post('', {vtype: vtype, vname: vname, vid: vid}, function(type) {
                                        if (type == '1') {
                                                alert("同步成功");
                                        } else {
                                                alert("同步失败");
                                        }
                                });
                        })
                                window.parent.now_frame_url = window.location.href;
                        });

                        function get_tags_edit(tags) {
                                $("#action").attr("value", "ctag");
                                $("#nns_tags").attr("value", tags);
                                var par = getAllCheckBoxSelect();
                                if (par == "") {
<?php if ($view_type == 0) { ?>
                                                alert("<?php echo cms_get_lang('media_select_dy'); ?>");
<?php } else if ($view_type == 1) { ?>
                                                alert("<?php echo cms_get_lang('media_select_lxj'); ?>");
<?php } ?>
                                } else {
                                        $('#nns_id').attr('value', par);
                                        checkForm('<?php echo cms_get_lang('dbgl'); ?>', '<?php echo cms_get_lang('msg_ask_ctag'); ?>', $('#delete_form'));
                                }
                        }

                </script>
        </head>

        <body>
                <div class="content">
                                    <!--<div class="content_position"><?php echo cms_get_lang('zbgl'); ?></div>-->

                        <div class="content_table formtable">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <thead>
                                                <tr>
                                                        <th><input name="" type="checkbox" value="" /></th>
                                                        <th><?php echo cms_get_lang('segnumber'); ?></th>
                                                        <th><?php echo cms_get_lang('name'); ?></th>
                                                        <th><?php echo cms_get_lang('group'); ?></th>
                                                        <th><?php echo cms_get_lang('jmdgl_jmdcl') ?></th>
                                                        <!--<th>分镜数</th>-->

                                                        <th><?php echo cms_get_lang('audit'); ?></th>
                                                        <th><?php echo cms_get_lang('state'); ?></th>
                                                        <!--<th><?php echo cms_get_lang('create_time'); ?></th>-->
                                                        <th><?php echo cms_get_lang('action'); ?></th>

                                                </tr>
                                        </thead>
                                        <tbody>
                                                <?php
                                                
                                                if ($data != null) {
                                                        include $nncms_db_path . "nns_live/nns_db_live_ex_class.php";
                                                        $num = ($currentpage - 1) * $g_manager_list_max_num;
                                                        foreach ($data as $item) {
                                                                $num++;
//          计算片源是否都已经绑定
                                                                $is_bind = $live_inst->check_bind_state($item["nns_id"], $item["nns_all_index"]);
                                                               // $count1 = $assist_inst->nns_db_assist_item_count(null, null, $item["nns_id"], 1);
                                                               // $count2 = $service_inst->nns_db_service_category_count(null, null, null, $item["nns_id"]);
                                                                /**
                                                                 * 获取视频次要参数 BY S67 2012-8-22
                                                                 */
                                                                $play_bill_type = '';
                                                                $live_ex_inst = new nns_db_live_ex_class();
                                                                $play_bill_type = $live_ex_inst->nns_db_live_ex_info($item["nns_id"], 'play_bill_type');
                                                                $live_type = $live_ex_inst->nns_db_live_ex_info($item["nns_id"], 'live_type');
                                                                $live_url = $live_ex_inst->nns_db_live_ex_info($item["nns_id"], $_GET['sp_id'].'_live');
                                                                $live_type = empty($live_type) ? "LIVE,TSTV" : $live_type;
//            $count3=$service_item_inst->nns_db_service_detail_video_count(null,$item["nns_id"],1);
                                                                /*if ($count1["data"][0]["num"] > 0 || $count2["data"][0]["num"] > 0) {
                                                                        $is_online = true;
                                                                } else {
                                                                        $is_online = false;
                                                                }*/
                                                                ?>
                                                                <tr>
                                                                        <td><input name="input" type="checkbox" class="checkhiddenInput" rel="<?php echo $item["nns_check"]; ?>" value="<?php echo $item["nns_id"]; ?>" <?php
                                                                                if ($is_online == true) {
                                                                                        echo "online='true'";
                                                                                }
                                                                                ?>/></td>
                                                                        <td><?php echo $num; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $item["nns_id"]; ?>
                                                                            <?php if(!empty($live_url)){?>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;url:&nbsp;&nbsp;<?php echo $live_url;} ?>
                                                                        </td>
                                                                        <td><?php echo $item["nns_name"]; ?></td>
                                                                        <td><?php
                                                                                switch ($item["nns_org_type"]) {
                                                                                        case 0:
                                                                                                $info_result = $carrier_inst->nns_db_carrier_info($item["nns_org_id"]);
                                                                                                break;
                                                                                        case 1:
                                                                                                $info_result = $partner_inst->nns_db_partner_info($item["nns_org_id"]);
                                                                                                break;
                                                                                        case 2:
                                                                                                $info_result = $group_inst->nns_db_group_info($item["nns_org_id"]);
                                                                                                break;
                                                                                }
                                                                                if ($info_result["ret"] == 0) {
                                                                                        echo $info_result["data"][0]["nns_name"];
                                                                                }
                                                                                ?></td>
                                                                        <td>
                                                                                <?php echo get_video_ex_name($play_bill_type); ?>
                                                                        </td>

                                                                        <td><?php
                                                                                switch ($item["nns_check"]) {
                                                                                        case NNS_VOD_CHECK_OK:
                                                                                                echo "<font style=\"color:#00CC00\">", cms_get_lang('audited'), "</font>";
                                                                                                break;
                                                                                        case NNS_VOD_CHECK_WAITING:
                                                                                                echo "<font style=\"color:#999999\">", cms_get_lang('media_dsh'), "</font>";
                                                                                                break;
//                	case NNS_live_STATE_OFFLINE:
//                	echo "<font style=\"color:#ff0000\">". $language_live_media_addedit_yxx. "</font>";
//                	break;
                                                                                }
                                                                                ?> </td>
                                                                        <td><?php
                                                                                if ($is_online) {
                                                                                        echo "<font style=\"color:#00CC00\">" . cms_get_lang('media_ysx') . "</font>";
                                                                                } else {
                                                                                        echo "<font style=\"color:#999999\">" . cms_get_lang('media_wxx') . "</font>";
                                                                                }
                                                                                ?> </td>

                                                                                                                                                                                                                                                                                                                                                                                                                                                <!--<td><?php echo $item["nns_create_time"]; ?> </td>-->
                                                                        <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                                                                                 
                                                                                
                                                                                
                                                                                <?php
                                                                                if ($is_bind) {
																					$config_sp_id = g_cms_config::get_g_config_value('g_bk_sp');
																					if($config_sp_id != 0)
																					{
                                                                                	?>
	                                                                                	<a href="./nncms_content_livelist.php?op=cdn&sp_id=<?php echo g_cms_config::get_g_config_value('g_bk_sp');?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=add">添加直播CND</a>
		                                                                                <a href="./nncms_content_livelist.php?op=cdn&sp_id=<?php echo g_cms_config::get_g_config_value('g_bk_sp');?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=modify">修改直播CND</a>
		                                                                                <a href="./nncms_content_livelist.php?op=cdn&sp_id=<?php echo g_cms_config::get_g_config_value('g_bk_sp');?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=destroy">删除直播CND</a>
                                                                                	<?php
																					}
																					else if(isset($_REQUEST['sp_id']) && strlen($_REQUEST['sp_id']) >0)
																					{
																					?>
																						<a href="./nncms_content_livelist.php?op=cdn_live&sp_id=<?php echo $_REQUEST['sp_id'];?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=add">注入直播CND</a>
																						<a href="./nncms_content_livelist.php?op=epg_live&sp_id=<?php echo $_REQUEST['sp_id'];?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=add">注入直播EPG</a>
                                                                                	<?php 	
                                                                                		$playbill_query_exsist = nl_playbill::query_exsist_by_live_id($dc, $item["nns_id"]);
                                                                                		if(isset($playbill_query_exsist['data_info']) && $playbill_query_exsist['data_info'] >0)
																						{
																							?>
																							<a href="./nncms_content_livelist.php?op=cdn_playbill&sp_id=<?php echo $_REQUEST['sp_id'];?>&video_id=<?php echo $item["nns_id"]; ?>&op_action=add">注入节目单CND</a>
																							<?php 
																						}
																					}
                                                                                }
                                                                                ?>
                                                                           
                                                                </tr>


                                                                <?php
                                                        }
                                                } $least_num = $nncms_ui_min_list_item_count - count($data);
                                                for ($i = 0; $i < $least_num; $i++) {
                                                        ?>
                                                        <tr>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>                                                                                                                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>

                                                <?php } ?>
                                        </tbody>
                                </table>
                        </div>
                        <div class="pagecontrol">
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_livelist.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_livelist.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $live_pages) { ?>
                                        <a href="nncms_content_livelist.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_livelist.php?page=<?php echo $live_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_livelist.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $live_pages; ?>);">GO>></a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;
                                <?php echo cms_get_lang('total'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $live_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_prepage_list();"/>

                        </div>
                </div>
        </body>
</html>
<?php
$carrier_inst = null;
$partner_inst = null;
$group_inst = null;
$assist_inst = null;
$service_inst = null;
$live_inst = null;
//   $service_item_inst=null;
?>

