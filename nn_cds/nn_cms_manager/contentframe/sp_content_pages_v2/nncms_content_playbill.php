<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";

//导入语言包
if (isset ($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "135006");
$checkpri = null;

if ($_GET["nns_org_id"]=="" || $_GET["nns_org_type"]==""){
	$nns_org_id=$_SESSION["nns_org_id"];
	$nns_org_type=$_SESSION["nns_manager_type"];
}else{
	$nns_org_id=$_GET["nns_org_id"];
	$nns_org_type=$_GET["nns_org_type"];
}

$nns_id = $_GET["nns_id"];
if($_GET['op']=='import_cdn'){
	
	
	//?op=import_cdn&sp_id="+sp_id+"&start_time="+day_picker_start+"&end_time="+day_picker_end+"ids="+ids;
	
	$sp_id = $_GET['sp_id'];
	$start_time = $_GET['start_time'];
	$end_time = $_GET['end_time'];
	$ids = $_GET['ids'];
	
	
	
	include $nncms_config_path . 'mgtv_v2/mgtv_init.php';
	include $nncms_config_path . 'mgtv_v2/models/import_model.php';
	
	
	$re = import_model::import_cdn_playbill($sp_id,$ids,$start_time,$end_time);
	echo '<script>alert("注入成功,请查看CDN列表");history.go(-1);</script>';
	die;
	
}
if($_GET['op']=='import_epg'){


	//?op=import_cdn&sp_id="+sp_id+"&start_time="+day_picker_start+"&end_time="+day_picker_end+"ids="+ids;

	$sp_id = $_GET['sp_id'];
	$start_time = $_GET['start_time'];
	$end_time = $_GET['end_time'];
	$ids = $_GET['ids']; 
	include $nncms_config_path . 'mgtv_v2/mgtv_init.php';
	include $nncms_config_path . 'mgtv_v2/models/import_model.php';
    $id_array = explode(',', $ids);
    if(is_array($id_array) && !empty($id_array[0]))
    { 
    	foreach($id_array as $k=>$v)
    	{
    	    $re = import_model::import_epg_playbill($sp_id,$v,$start_time,$end_time);
    	} 
    } 
    else
    {
    	echo '<script>alert("请最少选择一条数据");history.go(-1);</script>';
	    die;
    }
	echo '<script>alert("注入成功,请查看epg列表");history.go(-1);</script>';
	die;

}






if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
} else {
	include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
	// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
	 include($nncms_db_path. "nns_depot/nns_db_depot_class.php");
	 include_once $nncms_config_path. 'nn_logic/video/live.class.php';
	 $depot_inst=new nns_db_depot_class();
	 $depot_array=$depot_inst->nns_db_depot_list("",$nns_org_type,$nns_org_id,1);
	// var_dump($partner_array);
	 if ($depot_array["ret"]!=0){
		$data=null;
		echo "<script>alert(". $depot_array["reason"].");</script>";
	 }
	 $depot_inst=null;
	 $data=$depot_array["data"];
	 $base_name=cms_get_lang('media_zb|service_pk');
	 $base_id=10000;
	 
	 $live_arr = array();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
		<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
		<link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/dtree.js"></script>
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/tabs.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
		<script language="javascript" src="../../js/category_count.js"></script>

		<script language="javascript">
			
$(document).ready(function(){
$('#clear_time').click(function(){
					$('#day_picker_start').val('');
					$('#day_picker_end').val('');

				});

});

function set_content_width(){
if ($(".category_tree").is(":hidden")){
$(".category_editbox").width($(".content").width()-25);
}else{
$(".category_editbox").width($(".content").width()-$(".category_tree").width()-50);
}

}



	function import_cdn_data()
	{
	var querys={};
	var day_picker_start = $("#day_picker_start").val();
	var day_picker_end = $("#day_picker_end").val();
	
	var sp_id = $("#sp_id").val();
	if(day_picker_start==''){
		alert("请选择开始时间");
		return false;
	}
	
	
	if(day_picker_end==''){
		alert("请选择结束时间");
		return false;
	}
	
	
	var r=confirm("是否进行该操作");
	if(r == true){
		var ids=getAllCheckBoxSelect();
		//alert(ids);
	}
	
	var query_str = "?op=import_cdn&sp_id="+sp_id+"&start_time="+day_picker_start+"&end_time="+day_picker_end+"&ids="+ids;
	query_str=query_str.substr(0,query_str.length-1);
	//alert(query_str);return false;
	window.location.href = 'nncms_content_playbill.php'+query_str;
	
	/*
	var query_str='?op=import_cdn&live_id=<?php echo $play_id;?>&sp_id=<?php echo $_GET['sp_id'];?>&category_id=<?php echo $_GET['category_id'];?>&';
	//if ($("#day_pick_str").is(":hidden")){
	//	querys['begin_day']=$("#playbill_date_begin").val();
	//	querys['end_day']=$("#playbill_date_end").val();
	//	querys['name']=$("#playbill_name").val();
	// }else if($("#custom_pick_str").is(":hidden")){
		// querys['begin_day']=$("#day_picker").val()+' 00:00:00';
		// querys['end_day']=$("#day_picker").val()+' 23:59:59';
	// }
// 
	// for (var key in querys){
		// query_str+=key+'='+querys[key]+'&';	
		// }
	// query_str=query_str.substr(0,query_str.length-1);
	// window.location.href = 'nncms_content_playbill_detail_list.php'+query_str;
	// */
	
}
	function import_epg_data()
	{
	var querys={};
	var day_picker_start = $("#day_picker_start").val();
	var day_picker_end = $("#day_picker_end").val();
	
	var sp_id = $("#sp_id").val();
	if(day_picker_start==''){
		alert("请选择开始时间");
		return false;
	}
	
	
	if(day_picker_end==''){
		alert("请选择结束时间");
		return false;
	}
	
	//alert(day_picker_start);return false;
	var r=confirm("是否进行该操作");
	if(r == true){
		var ids=getAllCheckBoxSelect();
		//alert(ids);
		if(ids == ''){
			alert("请最少选择一条数据");
			return false;
		}
	}
	 
	var query_str = "?op=import_epg&sp_id="+sp_id+"&start_time="+day_picker_start+"&end_time="+day_picker_end+"&ids="+ids;
	query_str=query_str.substr(0,query_str.length-1);
	//alert(query_str);return false;
	window.location.href = 'nncms_content_playbill.php'+query_str;
}

	

	



	function checkheight(){
	var hei=$(".playbill_frame iframe").contents().find(".content").height();
	$(".playbill_frame iframe").height(hei);
	$(".category_tree").height($(".category_editbox").height()-20);
	$(".split_btn").height($(".category_editbox").height());
	window.parent.resetFrameHeight();
	}

		</script>
	</head>

	<body>
		<div class="content">
			<div class="content_position">
				节目单管理
			</div>
			<?php

			if ($data != null) {
			$depot_content = $data[0]["nns_category"];
			$dom = new DOMDocument('1.0', 'utf-8');
			$dom->loadXML($depot_content);
			$category = $dom->getElementsByTagName('category');
			include($nncms_db_path. "nns_live/nns_db_live_class.php");
			$live_inst=new nns_db_live_class();

			$dc=nl_get_dc(Array(
			'db_policy'=>NL_DB_WRITE,
			'cache_policy'=>NP_KV_CACHE_TYPE_MEMCACHE
			));

			$dc->open();

			foreach ($category as $item) {

			if ($item->getAttribute("parent"))
			?>

			<?php
			$live_array=$live_inst->nns_db_live_list("nns_id,nns_name",
			"","",1,$view_type,$data[0]["nns_id"],$item->getAttribute("id"));

			if($live_array["ret"]==0){
			$live_data=$live_array["data"];
			foreach($live_data as $item_data){
			$live_info=nl_live::epg_get_live_info($dc,$item_data['nns_id'],NL_DC_DB);
			if (strstr($live_info[0]['nns_live_type'],'TSTV')){

			$a = array('nns_id'=>$item_data["nns_id"],'nns_name'=>$item_data["nns_name"]);
			$live_arr[] = $a;
			?>
			<?php }}} ?>
			<?php }$live_inst=null;} ?>

			<?php

			}
			?>

			<div class="content_table">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<form action="" method="get">
							<tbody>
								<tr>
									<td>
										选择时间段：&nbsp;&nbsp;&nbsp;<input name="day_picker_start" id="day_picker_start" type="text"  value="" style="width:120px;" class="datepicker" callback="test" />
										 - <input name="day_picker_end" id="day_picker_end" type="text"  value="" style="width:120px;" class="datepicker" callback="test" />
										 &nbsp;&nbsp;<input type="button" id="clear_time" name="clear_time" value="清除时间"/>
										<input type="hidden" name="sp_id" id="sp_id" value="<?php echo $_GET['sp_id']; ?>" />
										&nbsp;&nbsp;&nbsp;&nbsp;	
										<input type="button" value="注入EPG"  onclick="import_epg_data();" />	
										&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="button" value="注入CDN"  onclick="import_cdn_data();" />										
									</td>
								</tr>
							</tbody>
							</form>
						</table>
			</div>
			<div class="content_table formtable">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th><input name="" type="checkbox" value="">序号</th>
							<th>资源ID</th>
							<th>名称</th>
						    <!--<th>操作</th>-->
						</tr>
					</thead>
					<tbody>
						<?php
						if($live_arr!=null){
							$num = 0;
							foreach ($live_arr as $item) {
								$num++;
						?>
						<tr>
						<td><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
						<td><?php echo $item["nns_id"]; ?></td>
						<td><?php echo $item["nns_name"]; ?></td>
						<!--<td><?php echo $item["nns_name"]; ?></td>-->
						</tr>
						<?php
							}
						}
						?>
						</tbody>
				</div>

		</div>
	</body>
</html>