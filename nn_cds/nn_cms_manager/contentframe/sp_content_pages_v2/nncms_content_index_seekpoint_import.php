<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
header("Content-Type:text/html;charset=utf-8");
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";


 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();

$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"137102");
//if ($_SESSION["nns_manager_type"]!=0) $pri_bool=false;
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");

$nns_id=$_GET["nns_id"];
 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
<link href="../../css/uploadify.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/radiolist.js"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<!--<script language="javascript" src="../../js/cms_datepicker.js"></script>-->
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/swfobject.js"></script>
<script language="javascript" src="../../js/muti_upload/jquery.uploadify.min.js"></script>

<script language="javascript">
var stauts_log='';
	$(document).ready(function(){
		<?php $timestamp = time();?>
			$('#vod_data_file').uploadify({
				'formData'     : {
					'timestamp' : '<?php echo $timestamp;?>',
					'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',	
					'action':'import',
					'session_id':'<?php echo $_SESSION['nns_mgr_id'];?>',
					'language_dir':'<?php echo $_SESSION['language_dir']?>',
					'nns_org_id':'<?php echo $_SESSION['nns_org_id']?>',
					'nns_org_type':'<?php echo $_SESSION['nns_manager_type']?>',
					'nns_role_id':'<?php echo $_SESSION['nns_role_id']?>'
				},
				'fileTypeExts':'*.csv',
				'buttonText':'选择导入文件',
				'scriptAccess':'always',
				'fileDesc':'请选择csv文件',
				'swf'      : 'uploadify.swf',
				'successTimeout':30,
				'auto'         : true,
				'uploader' : 'nncms_content_index_seekpoint_import_control.php',
				'onQueueComplete':function(data){
					$('#stauts_log').html(stauts_log);
					window.parent.resetFrameHeight();
					
					
					
				},
				'overrideEvents' : [ 'onDialogClose', 'onSelectError' ],
				'onSelectError':uploadify_onSelectError,
		        'onUploadSuccess':function(file,data,response){
		        	var stauts_str='';
		        	if (data == 'success'){
		        		stauts_str = file.name+' ----- '+'<span>操作成功!</span>';
		        	}else{
		        		stauts_str = file.name+' ----- '+'<span>'+data+'</span>';
		        	}
		        	stauts_str+='<br>';
		        	stauts_log+=stauts_str;
		      		window.parent.resetFrameHeight();
		        }
			});
		
	});

	var uploadify_onSelectError = function(file, errorCode, errorMsg){
		var msgText = "文件上传失败\n";
		switch (errorCode) {
			case SWFUpload.QUEUE_ERROR.INVALID_FILETYPE:
				msgText = "文件格式不正确，仅限csv文件";
				break;
			default:
				msgText = errorMsg;
		}
		alert(msgText);
	}

</script>
</head>

<body>


<div class="content" style="background:#fff;">
	<div class="content_position">批量注入分集打点信息</div>
    <form id="import_form" action="nncms_content_index_seekpoint_import_control.php" method="post"  enctype="multipart/form-data">
    <div class="content_table">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
              <tr>
                <td width="120">分集打点信息文件</td>
                <td> 
                	<div style="padding-top:10px;"><input name="vod_data_file" id="vod_data_file" type="file" size="60%" multiple="true"  /></div>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">文件格式为CSV文件。</font></td>
      		</tr>
			<tr>
				<td width="120">上传状态</td>
				<td ><div id="stauts_log"  style="min-height:40px;"></div></td>
				<td></td>
			</tr>
          </tbody>
        </table>
    </div>
    </form>
    <div class="controlbtns">
    	<div class="controlbtn back"><a href="javascript:$('#vod_data_file').uploadify('stop');">取消上传</a></div>
        <div style="clear:both;"></div>
    </div>
</div>
</body>
</html>
