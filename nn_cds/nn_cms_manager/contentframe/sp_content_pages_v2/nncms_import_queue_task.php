<?php 
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$group_name = $_GET["group_name"];
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
$queue_task_model = new queue_task_model();
$sp_id = $_GET['sp_id'];

include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$result_cp_data = nl_query_by_db("select nns_id,nns_name from nns_cp", $dc->db());
$result_cp_data_keys = $result_cp_id_data = array();
if(is_array($result_cp_data))
{
    foreach ($result_cp_data as $cp_val)
    {
        $result_cp_id_data[$cp_val['nns_id']] = $cp_val['nns_name'];
    }
    $result_cp_data_keys = array_keys($result_cp_id_data);
}

if($_GET['op'] == 'queue')
{
	$str = '操作成功';
	$ids = explode(',', $_GET['ids']);
	foreach ($ids as $value) 
	{
		$queue_task_model->add_task_to_queue($sp_id,$value);
	}
	
	echo '<script>alert("'.$str.'");history.go(-1);</script>';die;
}
elseif ($_GET['op'] == 'delete_queue')
{
	$str = '操作成功';
	$ids = explode(',', $_GET['ids']);
	foreach ($ids as $value)
	{
		$queue_task_model->delete_import_op($sp_id,$value);
	}
	
	echo '<script>alert("'.$str.'");history.go(-1);</script>';die;	
}

$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : (!empty($_COOKIE["page_max_num"]) ?  $_COOKIE["page_max_num"] : 18);
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) {
	$page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit = $page_size;
$filter = null;
if ($_GET['actionsearch'] == 'search') 
{
	$_GET['nns_name'] = trim($_GET['nns_name']);
	$filter['nns_name'] = $_GET['nns_name'];
	$filter['nns_type'] = $_GET['nns_type'];
	$filter['nns_action'] = $_GET['nns_action_state'];
	$filter['day_picker_start'] = $_GET['day_picker_start'];
	$filter['day_picker_end'] = $_GET['day_picker_end'];
	if(isset($_GET['nns_cp_id']) && strlen($_GET['nns_cp_id'])>0)
	{
	    $filter['nns_cp_id'] = $_GET['nns_cp_id'];
	}
}

$result = $queue_task_model->get_import_op_list($sp_id, $filter, $offset, $limit);
$vod_total_num = $result['rows'];
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) {
	$page_num = $vod_pages;
}
$currentpage = $page_num;

$g_manager_list_max_num = $page_size;

$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);

$data = $result['data']; 
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<style>
.mytip_ul
{
    text-align: left;
    font-size: 12px;
    list-style-type: none;
    padding: 0;
    margin: 0;
}

.mytip_title
{
    margin-left: 10px;
    width: 100px;
    float: left;
    text-align: left;
}

.mytip_close
{
    margin-right: 10px;
    margin-top: 5px;
    width: 100px;
    float: right;
    text-align: right;
    cursor: pointer;
}

.mytip_top
{
    background-image: url(../../images/tips_titlebj.gif);
    color: #FFFFFF;
    font-size: 12px;
    height: 24px;
    line-height: 24px;
    font-weight: 600;
}

.tips_li
{
    height: 25px;
    line-height: 25px;
    border-bottom: #ccc 1px solid;
}

.tips_li_span
{
    color: #07507d;
    margin-left: 10px;
}

.tips_li_span2
{
    color: #6d6d6d;
}		
.myalert_div1
{
    height: 25px;
    line-height: 25px;
    border-bottom: #ccc 1px solid;
    width: 620px;
}

.myalert_div1_leftDiv
{
    float: left;
    width: 50%;
    margin-left: 10px;
}

.myalert_div1_rightDIv
{
    float: left;
    margin-left: 10px;
}

.myalert_div1_Div
{
    float: left;
    width: 100%;
    margin-left: 10px;
}

.myalert_div2
{
    height: 84px;
    line-height: 70px;
    border-bottom: #ccc 1px solid;
    width: 620px;
}

.myalert_div2_Div
{
    float: left;
    width: 100%;
    margin-left: 10px;
    height: 84px;
}

.myaelrt_li_title
{
    color: #07507d;
}

.myalert_li_txt
{
    color: #6d6d6d;
}

.myalert_inputTxt
{
    width: 220px;
    border: 0px solid #eff0f0;
    padding-left: 2px;
    background-color: #eff0f0;
    color: #6d6d6d;
}		
		</style>		
		
		
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
		<script language="javascript">

			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
                setCookie("page_max_num",num);
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				})
				return BoxKey;
			}
			
			
			function queue(){
				var r=confirm("是否进行该操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "nncms_import_queue_task.php?sp_id=<?php echo $sp_id?>&op=queue&ids="+ids;
						window.location.href = url;
					}
				}
			}
			function delete_queue()
			{
				var r=confirm("是否进行该操作");
				if(r == true)
				{
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids == "")
					{
						alert('请选择数据');
					}
					else
					{
						var url = "nncms_import_queue_task.php?sp_id=<?php echo $sp_id?>&op=delete_queue&ids="+ids;
						window.location.href = url;
					}
				}
			}
			
			$(document).ready(function() {
				$('#clear_time').click(function(){
					$('#day_picker_start').val('');
					$('#day_picker_end').val('');

				});
				window.parent.now_frame_url = window.location.href;
				window.parant.resetFrameHeight();
			});
			
		</script>

	</head>

	<body>
		<div class="content">
			<div class="content_position">
				中心注入指令
			</div>
				<div class="content_table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<form action="" method="get">
							<tbody>
								<tr>
									<td>
										影片名：&nbsp;&nbsp;<input type="text" name="nns_name" id="nns_name" value="<?php echo $_GET['nns_name']; ?>" style="width:200px;">
										&nbsp;&nbsp;&nbsp;&nbsp;类型：&nbsp;&nbsp;<select name="nns_type" style="width: 150px;">
											<option value="" >全部</option>
											<option value="video" <?php if($_GET['nns_type']=='video') echo 'selected="selected"'?>>影片</option>
											<option value="index" <?php if($_GET['nns_type']=='index') echo 'selected="selected"'?>>分集</option>
											<option value="media" <?php if($_GET['nns_type']=='media') echo 'selected="selected"'?>>片源</option>
											<option value="live" <?php if($_GET['nns_type']=='live') echo 'selected="selected"'?>>直播频道</option>
											<option value="live_media" <?php if($_GET['nns_type']=='live_media') echo 'selected="selected"'?>>直播源</option>
											<option value="playbill" <?php if($_GET['nns_type']=='playbill') echo 'selected="selected"'?>>节目单</option>
											<option value="file" <?php if($_GET['nns_type']=='file') echo 'selected="selected"'?>>包文件</option>
											<option value="product" <?php if($_GET['nns_type']=='product') echo 'selected="selected"'?>>产品包</option>
                                            <option value="seekpoint" <?php if($_GET['nns_type']=='seekpoint') echo 'selected="selected"'?>>打点信息</option>
                                        </select>
                                    &nbsp;&nbsp;&nbsp;&nbsp;操作：&nbsp;&nbsp;<select name="nns_action_state" style="width: 150px;">
											<option value="" >全部</option>
											<option value="add" <?php if($_GET['nns_action_state']=='add') echo 'selected="selected"'?>>添加</option>
											<option value="modify" <?php if($_GET['nns_action_state']=='modify') echo 'selected="selected"'?>>修改</option>
											<option value="destroy" <?php if($_GET['nns_action_state']=='destroy') echo 'selected="selected"'?>>删除</option>
                                    </select>
                                    &nbsp;&nbsp;&nbsp;&nbsp;CP ID：&nbsp;&nbsp;&nbsp;&nbsp;<select name="nns_cp_id" style="width: 150px;">
                                    		<option value="" >全部</option>
                                    	<?php 
                                    		foreach ($result_cp_id_data as $cp_id_key=>$cp_id_val)
											{
												?>
												<option value="<?php echo $cp_id_key; ?>" <?php if(isset($_GET['nns_cp_id']) && $_GET['nns_cp_id']==$cp_id_key) echo 'selected="selected"'?>><?php echo $cp_id_val; ?></option>
												<?php 
											}
											echo "</select>";
                                    	?>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
										选择时间段：&nbsp;&nbsp;&nbsp;<input name="day_picker_start" id="day_picker_start" type="text"  value="<?php
										if (isset($_GET['day_picker_start']))
											echo $_GET['day_picker_start'];
									?>" style="width:120px;" class="datetimepicker" callback="test" />
										 - <input name="day_picker_end" id="day_picker_end" type="text"  value="<?php
											if (isset($_GET['day_picker_end']))
												echo $_GET['day_picker_end'];
										?>" style="width:120px;" class="datetimepicker" callback="test" />
										 &nbsp;&nbsp;<input type="button" id="clear_time" name="clear_time" value="清除时间"/>
										<input type="hidden" name="sp_id" value="<?php echo $sp_id; ?>" />
										<input type="hidden" name="actionsearch" value="search" />
										&nbsp;&nbsp;&nbsp;&nbsp;	
										<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  />										
									</td>
								</tr>
							</tbody>
							</form>
						</table>
				</div>			
			<div class="content_table formtable">

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th><input name="" type="checkbox" value="">序号</th>
							<th>队列ID</th>
							<th>名称</th>
							<th>类型</th>
							<th>动作</th>
							<th>入列队时间</th>
							<th>列队状态</th>
							<th>权重</th>
							<th>是否属于分组</th>
							<th>CP ID</th>
						    <th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if($data!=null){
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) {
								$num++;
						?>
						<tr>
						<td id="is_delete"><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
						<td >                        
                        <?php
						
							echo $item['nns_id'];
						
                        ?>
                   
                        </td>

                        <td>
                        	
							                    
                        <?php
						
							echo $item['nns_video_name'];
						
                        ?>
                        
                        
                   
                        </td>
                        <td>
                        <?php
						switch($item['nns_video_type']) {
							case 'video' :
								echo '影片';
								break;
							case 'index' :
								echo '分集';
								break;
							case 'media' :
								echo '片源';
								break;
							case 'live' :
							    echo '直播频道';
							    break;
							case 'live_media' :
							    echo '直播源';
							    break;
						    case 'playbill' :
						        echo '节目单';
						        break;
					        case 'file' :
					            echo '包文件';
					            break;
				            case 'product' :
                                echo '产品包';
                                break;
                            case 'seekpoint' :
                                echo '打点信息';
                                break;
						}
                        ?>
                        </td>
                        <td>                        
                        <?php
							switch($item['nns_action']) {
								case 'add' :
									echo '添加';
									break;
								case 'modify' :
									echo '修改';
									break;
								case 'destroy' :
									echo '删除';
									break;
							}
                        ?>
                   
                        </td>
                        <td><?php echo $item['nns_create_time']; ?></td>     
                        <td>	
                        <?php
                        $nns_op_sp = explode(',', $item['nns_op_sp']);
						if(!in_array($sp_id, $nns_op_sp)){
							echo "<font color=green>已经下发</font>";
						}else{
							echo "<font color=red>未下发</font>";
						}
                        ?>
						</td>
						<td><?php echo $item['nns_weight']; ?></td>
						<td><?php echo ($item['nns_is_group'] != '1') ? "否" : "是"; ?></td>
						<td>
					    <?php if(in_array($item['nns_cp_id'], $result_cp_data_keys))
					    {
					        echo isset($result_cp_id_data[$item['nns_cp_id']]) ? $result_cp_id_data[$item['nns_cp_id']] : '';
					    }
					    else
					    {
					        echo '<font color="#FF0000">非法</font>';
					    }?>
					    </td>
                            <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                        	<a href="nncms_import_queue_task.php?sp_id=<?php echo $sp_id;?>&op=queue&ids=<?php echo $item['nns_id'];?>">同步下发</a>
                        	<?php if(isset($item['nns_bk_queue_excute_url']) && strlen($item['nns_bk_queue_excute_url']) >0){?>
                                <a href="./../../../data/<?php echo $item['nns_bk_queue_excute_url'];?>" target="_blank">点击查看</a>
                            <?php }?>
                        	</td>
                        </tr>
						<?php
							}
						}$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) {
						?>
						<tr>
                                                                                                                                                                                          
                                                               
                                                               <td>&nbsp;</td>
                                                               <td>&nbsp;</td>
                                                               <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp;</td><td>&nbsp;</td>
                                                                 <td>&nbsp;</td>
                                                                 <td>&nbsp;</td>
<td>&nbsp;</td>

                                                        </tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="pagecontrol">
			  共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_import_queue_task.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_import_queue_task.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_import_queue_task.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_import_queue_task.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_import_queue_task.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

                        </div>
                        
                 <div class="controlbtns">
    	<div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
        <div class="controlbtn delete"><a href="javascript:delete_queue();">删除</a></div>
        <div class="controlbtn move"><a href="javascript:queue();">同步下发</a></div>
        <div style="clear:both;"></div>
    </div>    
                        
		</div>
	</body>
</html>
