<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";


$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106103");
$checkpri = null;

if ($_GET["nns_org_id"] == "" || $_GET["nns_org_type"] == "") {
        $nns_org_id = $_SESSION["nns_org_id"];
        $nns_org_type = $_SESSION["nns_manager_type"];
} else {
        $nns_org_id = $_GET["nns_org_id"];
        $nns_org_type = $_GET["nns_org_type"];
}

$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$group_name = $_GET["group_name"];
if (!$pri_bool) {
        Header("Location: ../nncms_content_wrong.php");
        exit;
}

// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include($nncms_db_path . "nns_depot/nns_db_depot_class.php");
$depot_inst = new nns_db_depot_class();

$depot_array = $depot_inst->nns_db_depot_list("", $nns_org_type, $nns_org_id, 0);
// var_dump($partner_array);
if ($depot_array["ret"] != 0) {
        $data = null;
        echo "<script>alert('" . $depot_array["reason"] . "');</script>";
}
$depot_inst = null;
$data = $depot_array["data"];

include $nncms_db_path . "nns_carrier/nns_db_carrier_class.php";
include $nncms_db_path . "nns_partner/nns_db_partner_class.php";
include $nncms_db_path . "nns_group/nns_db_group_class.php";


if ($_SESSION["nns_manager_type"] == 0) {
        $carrier_inst = new nns_db_carrier_class();
        $carrier_result = $carrier_inst->nns_db_carrier_list();

        if ($carrier_result["ret"] == 0) {
                $carrier_data = $carrier_result["data"][0];
                $base_name = $carrier_data["nns_name"];
        }
}

//		 	类型为合作伙伴
if ($nns_org_type == 1) {
        $partner_inst = new nns_db_partner_class();
        $partner_result = $partner_inst->nns_db_partner_info($nns_org_id);

        if ($partner_result["ret"] == 0) {
                $partner_data = $partner_result["data"][0];
                $base_name = $partner_data["nns_name"];
        }
        //	类型为集团
} else if ($nns_org_type == 2) {
        $group_inst = new nns_db_group_class();
        $group_result = $group_inst->nns_db_group_info($nns_org_id);

        if ($group_result["ret"] == 0) {
                $group_data = $group_result["data"][0];
                $base_name = $group_data["nns_name"];
        }
}


$base_id = 10000;
if (!empty($_COOKIE["page_max_num"])) {
        $g_manager_list_max_num = $_COOKIE["page_max_num"];
}
$sp_id = $_GET['sp_id'];
$product_id = $_GET['product_id'];
?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
        <link href="../../css/smoothness-1.8.13/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
        <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="../../js/calendar/jscal2.css" type="text/css" />
        <link rel="stylesheet" href="../../js/calendar/border-radius.css" type="text/css" />
        <link rel="stylesheet" href="../../js/calendar/win2k.css" />
        <link rel="stylesheet" href="../../css/ui.dropdownchecklist.themeroller.css" />
        <script language="javascript" src="../../js/dtree.js"></script>
        <script language="javascript" src="../../js/jquery-1.6.2.min.js"></script>
        <script language="javascript" src="../../js/table.js.php"></script>
        <script language="javascript" src="../../js/trim.js"></script>
        <script language="javascript" src="../../js/category_count.js"></script>
        <script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
        <script language="javascript" src="../../js/jquery-ui-1.8.13.custom.min.js"></script>
        <script language="javascript" src="../../js/ui.dropdownchecklist-1.4-min.js"></script>
        <script language="javascript" src="../../js/cms_datepicker.js"></script>

        <script language="javascript">
            function set_more_video_id() {
                var ids = $(".video_frame iframe")[0].contentWindow.get_more_select();
                
                var is_black = $("#nns_method").val();


                
                if(ids.length==0){
                	alert('请选择一个影片');return false;
                }
               
                <?php if ($sp_id != 'starcor'){?>
                window.parent.parent.set_black(ids,is_black);
                <?php }else{ ?>
				var product_id = '<?php echo $product_id; ?>';
                window.parent.parent.set_vod_id(ids, product_id);
				<?php } ?>
                window.parent.parent.close_select();
                //refresh_video_box();
            }

            var now_select_id;
            $(document).ready(function() {
                $(".closebtn").click(function() {
                    window.parent.close_select();
                });
                $(".category_editbox").width($(".content").width() - 240);
                $(".service_frame iframe").width($(".content").width() - 250);
                $(window).resize(function() {
                    $(".category_editbox").width($(".content").width() - 250);
                    $(".service_frame iframe").width($(".content").width() - 250);
                });


                $(".selectbox").hide();
                $(".selectbox").css("left", ($("body").width() - $(".selectbox").width()) / 2);
                $(".selectbox").css("top", 10);

                $("#nns_carrier").change(function() {
                    var $obj = $(this).find("option:selected");
                    var index = $("#nns_carrier option").index($obj);
                    selectCarrier(index);
                });
                

                
            });

            

            function select_tree_item(item_id, item_name, parent_id) {


				//alert(item_id);
				//alert(item_name);
				
				//alert(parent_id);

                $(".category_edit_id").html(item_id);
                $(".category_edit_name").html(item_name);
                $(".video_frame").show();
                now_select_id = item_id;
                var p = $('.submit_search').serialize();
                //alert(p);
                refresh_video_box(p);
            }
            function refresh_video_box(p) {
                    $(".video_frame iframe").attr("src", "nncms_controls_video_more_select_list.php?sp_id=<?php echo $sp_id;?>&category_id=" + now_select_id + "&search=" + encodeURI($("#nns_video_search").val()) + '&' + p);
            }


            function checkheight() {

                $(".video_frame iframe").height($(".video_frame iframe").contents().find(".content").height());
                $(".category_tree").height($(".category_editbox").height() - 20);
            }

            

            function close_select() {
                $(".selectbox").hide();
            }

            

           
            

        </script>
        <style>
            .video_select_control {
                border: none;
                border-right: 5px solid #ccc;
            }

            div.category_info{float: right;}
            div.category_info span{ padding: 0 5px; font-weight: bold;}

            div.adv_search_div{
                background:#fff;
                width: 100%;
                border:1px solid #ccc;
            }
            div.adv_search_div  td{ padding:5px 0;}
            div.adv_search_div input{padding:2px 5px;}
            div.adv_search_div .left{color: rgb(107, 107, 107);
                                     word-break: break-all;text-align:right;}
            .adv_str{display: block; width:100%; padding: 2px 5px; margin-top: 5px; font-weight: bold; overflow: hidden;}
            div.adv_search_div .right{padding-left: 10px;}
            .formdiv{ width: 96%; text-align: right;  padding-bottom: 5px;  }
            .formdiv input{ padding: 2px 10px; margin-right: 4.5%;  color:#6B6B6B}
            table td { vertical-align: top }
            dd { padding-bottom: 15px }
        </style>



    </head>

    <body  style='width:760px;'>
        <div class="selectbox">
            <iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
        </div>
        <div class="content video_select_control" style="padding:0px; margin:0px;">

            <div class="select_title"><b>媒体选择框&nbsp;&nbsp;栏目信息: <span class="category_edit_name"></span> / <span class="category_edit_id"></span></b><div class="category_info"></div></div>
            <div class="content_table">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr class="select_carrier_class">
							<?php if($sp_id != 'starcor'){?>
                            <td  width="60"><b>名单类型:</b></td>
                            <td  width="70"><select name="nns_method" id="nns_method"  style=" padding:3px; width:80%;"><br>
                                        <option value="0">白名单</option>
                                        <option value="1"  >黑名单</option>
                                     </select>
                            </td>
							<?php }?>
                            <td width="75">ID/拼音/片名:</td>
                            <td width="200">
                                <input name="nns_video_search" id="nns_video_search" type="text"
                                       value="" onchange="refresh_video_box();" />
                                <input type="button"  style='margin:0 5px;' value="查询" onclick="search_video();"/></td>

                            <td width="145">
                                                                    <input type="button" style='float:right;' value="确定选择" onclick="set_more_video_id();"/>
                                                            </td>

                        </tr>

                        </tr>
                    </tbody>
                </table>
            </div>
            <span class="adv_str"></span>
            

            <div style="background-color:#FFF;">

                <div class="category_tree">
                    
                    <div style="padding:5px; background:#eeeeee; border:3px solid #ccc; margin-bottom:10px;">
                                                <?php if ($_SESSION["nns_manager_type"] == 0) { ?>
                                                        <tr class="select_carrier_class">

                                                                <td><select name="nns_carrier" id="nns_carrier"  style=" padding:3px; width:100%;"><br>
                                                                                        <option value="<?php echo $carrier_data["nns_id"]; ?>" <?php if ($nns_org_type == 0) { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('xtgl_yysgl'), '/', $carrier_data["nns_name"]; ?></option>
                                                                                        <?php if ($g_partner_enabled == 1) { ?>
                                                                                                <option value="1"  <?php if ($nns_org_type == 1) { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('partner'); ?></option>
                                                                                        <?php }if ($g_group_enabled == 1) { ?>
                                                                                                <option value="2"  <?php if ($nns_org_type == 2) { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('group_jtxx'); ?></option>
                                                                                        <?php } ?>
                                                                        </select>

                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                        </tr>
                                                <?php } ?>
                                        </div>
                                        <?php
                                        if ($data != null) {
                                                $assist_content = $data[0]["nns_category"];
                                        } else {
                                                $assist_content = null;
                                        }
                                        echo pub_func_get_tree_by_html($assist_content, $base_name, $base_id);
                                        ?>
                                    </div>

                <div class="category_editbox"  style="padding:10px; margin:0px;">
                    <div class="video_frame" style="height:400px; overflow-y:hidden; width:100%">
                        <iframe  scrolling="auto" frameborder="0" onload="checkheight();" style="width:100%;overflow-x:hidden;"></iframe>
                    </div>

                </div>
                <div style="clear:both;"/>
            </div>
        </div>
        
    </body>
</html>