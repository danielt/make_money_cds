<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";

//导入语言包
if (isset ($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107103");
$checkpri = null;

if ($_GET["nns_org_id"]=="" || $_GET["nns_org_type"]==""){
	$nns_org_id=$_SESSION["nns_org_id"];
	$nns_org_type=$_SESSION["nns_manager_type"];
}else{
	$nns_org_id=$_GET["nns_org_id"];
	$nns_org_type=$_GET["nns_org_type"];
}

$nns_id = $_GET["nns_id"];
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
} else {
	include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
	// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
	 include($nncms_db_path. "nns_depot/nns_db_depot_class.php");
	 include_once $nncms_config_path. 'nn_logic/video/live.class.php';
	 $depot_inst=new nns_db_depot_class();
	 $depot_array=$depot_inst->nns_db_depot_list("",$nns_org_type,$nns_org_id,1);
	// var_dump($partner_array);
	 if ($depot_array["ret"]!=0){
		$data=null;
		echo "<script>alert(". $depot_array["reason"].");</script>";
	 }
	 $depot_inst=null;
	 $data=$depot_array["data"];
	 $base_name=cms_get_lang('media_zb|service_pk');
	 $base_id=10000;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/dtree.js"></script>
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/tabs.js"></script>
<script language="javascript" src="../../js/cms_datepicker.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
<script language="javascript" src="../../js/category_count.js"></script>

<script language="javascript">
var now_select_id;
$(document).ready(function(){
	$(".category_editbox").width($(".content").width()-250);
	$(".service_frame iframe").width($(".content").width()-250);
	 $(window).resize(function(){
							$(".category_editbox").width($(".content").width()-250);
							$(".service_frame iframe").width($(".content").width()-250);
													});
	var live_tabs=new mytabs($(".state_tabs"),refresh_table);
	live_tabs.init();
	
	$(".split_btn").click(function(){
			if ($(".category_tree").is(":hidden")){
				$(".category_tree").show();
			}else{
				$(".category_tree").hide();
			}
			set_content_width();
		});

});

function set_content_width(){
	if ($(".category_tree").is(":hidden")){
	$(".category_editbox").width($(".content").width()-25);
	}else{
	$(".category_editbox").width($(".content").width()-$(".category_tree").width()-50);
	}
	 
}

function refresh_table(path){
	$(".live_frame iframe").attr("src",path+"&depot_detail_id="+now_select_id+"&depot_id=<?php echo $data[0]["nns_id"];?>");
}
function checkhiddenInput(){
	var par=getAllCheckBoxSelect();
	if (par==""){
		alert("<?php echo cms_get_lang('lmgl_msg_noselect');?>");
	}else{
		$('#nns_id').attr('value',getAllCheckBoxSelect());
		checkForm('<?php echo cms_get_lang('lmgl_lmmbgl');?>','<?php echo cms_get_lang('msg_ask_delete');?>',$('#delete_form'));
	}
}

function select_tree_item(item_id,item_name,parent_id){
	$(".category_tr").show();
//	$(".live_tr").hide();
	$(".playbill_frame").hide();
	$(".category_editbox").find(".category_edit_id").html(item_id);
	$(".category_editbox").find(".category_edit_name").html(item_name);
}

function select_tree_live(live_id,live_name,live_category){
	$(".category_tr").hide();
//	$(".live_tr").show();
	$(".playbill_frame").show();
//	$(".live_tr .live_name").html(live_name);
	$(".playbill_frame iframe").attr("src","nncms_content_playbill_detail_list.php?sp_id=<?php echo $_GET['sp_id'];?>&nns_id="+live_id);
}

function editPlaybill(url){

        $("#select_frame").attr("src",url);
	$(".selectbox").show();
    
  }

//function add_node(){
//	var node_value=$(".category_edit_id").html();
//	$(".service_frame iframe").attr("src","nncms_content_service_category_addedit.php?service_id="+node_value+"&action=add&service_package_id=<?php echo $data[0]["nns_id"];?>");
//}

function checkheight(){
	var hei=$(".playbill_frame iframe").contents().find(".content").height();
		$(".playbill_frame iframe").height(hei);
		$(".category_tree").height($(".category_editbox").height()-20);
		$(".split_btn").height($(".category_editbox").height());
		window.parent.resetFrameHeight();
	}


</script>
</head>
<div class="selectbox" style="display: none;">
		<iframe scrolling="no" frameborder="0"  id="select_frame" ></iframe>
 </div>

<body>
<div class="content">
	<div class="content_position"> 节目单管理</div>
	<div style="background-color:#FFF;">
		<div class="category_tree" >
			
			<script type="text/javascript">
			<!--

			d = new dTree('d');

			d.add(0,-1,'<?php echo $base_name;?>',"javascript:select_tree_item('<?php echo $base_id?>','<?php echo $base_name;?>','0');");
			<?php


	if ($data != null) {
		$depot_content = $data[0]["nns_category"];
		$dom = new DOMDocument('1.0', 'utf-8');
		$dom->loadXML($depot_content);
		$category = $dom->getElementsByTagName('category');
		include($nncms_db_path. "nns_live/nns_db_live_class.php");
		 $live_inst=new nns_db_live_class();
		 
		  $dc=nl_get_dc(Array(
				'db_policy'=>NL_DB_WRITE,
				'cache_policy'=>NP_KV_CACHE_TYPE_MEMCACHE
				));
				
		$dc->open();
		 
		foreach ($category as $item) {

			if ($item->getAttribute("parent"))
?>
			d.add(<?php echo $item->getAttribute("id");?>,<?php echo $item->getAttribute("parent");?>,'<?php echo $item->getAttribute("name");?>',"javascript:select_tree_item('<?php echo $item->getAttribute("id");?>','<?php echo $item->getAttribute("name");?>','<?php echo $item->getAttribute("parent");?>');");
//			直播用遍历直播栏目所有内容
			<?php
				$live_array=$live_inst->nns_db_live_list("nns_id,nns_name",
"","",1,$view_type,$data[0]["nns_id"],$item->getAttribute("id"));

				if($live_array["ret"]==0){
					$live_data=$live_array["data"];
					foreach($live_data as $item_data){
						$live_info=nl_live::epg_get_live_info($dc,$item_data['nns_id'],NL_DC_DB);
						if (strstr($live_info[0]['nns_live_type'],'TSTV')){
						?>
					d.add('<?php echo $item_data["nns_id"];?>',<?php echo $item->getAttribute("id");?>,'<?php echo $item_data["nns_name"];?>',"javascript:select_tree_live('<?php echo $item_data["nns_id"];?>','<?php echo $item_data["nns_name"];?>','<?php echo $item->getAttribute("id");?>');",0,"","","../../images/tree/cd.png");
					<?php }}}?>
//			-------------------------------
			<?php }$live_inst=null;}?>

			document.write(d);


			//-->

			</script>
		</div>
		<div class="split_btn"></div>
		<div class="category_editbox" >
			 <div class="content_table">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tbody>
					<tr class="category_tr">
							 
							
							<td class="rightstyle2a"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>
						<?php echo cms_get_lang('lmgl_nrlmid').'/'.cms_get_lang('name');?>:</td>
					<td colspan="3"><span class="category_edit_id"></span>/<span class="category_edit_name"></span></td>
						
						</tr>
						

				  </tbody>
				  </table>
				</div>

				<div class="playbill_frame">
					<iframe  scrolling="no" class="content_if" frameborder="0" onload="checkheight();" style="width:100%;"></iframe>
				</div>

		</div>
		<div style="clear:both;"/>
	</div>


</div>
<!--
         <form action="nncms_content_playbilll_upload.php" method="post" enctype="multipart/form-data">
            <input type="file" name="Filedata" />
            <input type="submit"/>
        </form>!-->
</body>
</html>
<script>
var node=d.selectedNode;
if (!node) node=0;
var node_id=d.aNodes[node].id;
var node_parent_id=d.aNodes[node].pid;
var node_name=d.aNodes[node].name;
if (node_id==0){
	node_id="1000";
}
if (node_parent_id==-1){
	node_parent_id=0;
}
if (node_id.length==32){
	select_tree_live(node_id,node_name,node_parent_id);
}else{
	select_tree_item(node_id,node_name,node_parent_id);
}
</script>
<?php }?>
