<?php 
header("Content-Type:text/html;charset=utf-8");
ob_start();
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include $nncms_db_path . "nns_vod/nns_db_vod_class.php";
require_once $nncms_config_path . "op/op_queue.class.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
//打开DC
$dc=nl_get_dc( array(
		'db_policy'=>NL_DB_WRITE,
		'cache_policy'=>NP_KV_CACHE_TYPE_MEMCACHE
));
$dc->open();
$db = $dc->db();
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}

$sp_id = $_GET['sp_id'];
$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 18;
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) {
	$page_num = 1;
}
$start = intval($page_num - 1) * $page_size;
$params = null;
if ($_GET['actionsearch'] == 'search') {
	if(isset($_GET['nns_name']) && !empty($_GET['nns_name'])) {
		$params['nns_name'] = trim($_GET['nns_name']);
	}
	if(isset($_GET['video_id']) && !empty($_GET['video_id'])) {
		$params['nns_video_id'] = $_GET['video_id'];
		$video_id_array = array($params['nns_video_id']);
	} else {
		$video_id_array = null;
	}	
}
$params['nns_org_id'] = $sp_id; 
$op = new op_queue();
$vod = new nns_db_vod_class();
//$field = null, $name = null, $state = null, $check = null, $view_type = null, $depot_id = null, $depot_detail_id = null,$org_type = null
//$org_id = null, $tag = null, $vod_info = null, $order = null, $min = 0, $num = 0, $deleted = NNS_VOD_DELETED_FALSE,$except_method = null
//$except_detail_id = null, $except_id = null, $begin_sy_date = null, $end_sy_date = null, $begin_create_date = null, $end_create_date = null
//$begin_copyright_date = null, $end_copyright_date = null, $searchpoint = null, $depot_like_detail_id = null, $summary = null, $index_num = null
//$min_len = null, $max_len = null, $nns_pinyin = null, $nns_no_pic = null, $nns_ids_arr = null
$vod_result = $vod->nns_db_vod_list('nns_id,nns_name',array('nns_name'=>$params['nns_name'],'nns_pinyin'=>$params['nns_name']),null,null,null,null,null,null,null,null,null,'',null,null,NNS_VOD_DELETED_FALSE,'weight',null,$params['nns_org_id'],null, null,null,null,null,null,null,null,null,null,null,null,null,null,$video_id_array);
$op_result = $op->get_weight($db, $params);

$result = array();
if(is_array($vod_result) && is_array($op_result)) {
	$result = array_merge($op_result,$vod_result['data']);
} else {
	if(is_array($vod_result)) {
		$result = $vod_result['data'];
	}
	if(is_array($op_result)) {
		$result = $op_result;
	}
}
$vod_total_num = count($result);
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) {
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);
$data = array();
for($i=$start;$i<($page_size+$start);$i++) {
	if(empty($result[$i])) {
		break;
	}
	$data[$i] = $result[$i];
}
//var_dump($data);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript">
		function refresh_vod_page() {
			var num = $("#nns_list_max_num").val();
			window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
		}
		function add_action(num) {
			for(var i=1;i<=<?php echo $vod_total_num; ?>;i++) {
				$("#old_weight_"+i).show();
				$("#new_weight_"+i).hide();
				$("#control_old_"+i).show();
				$("#control_new_"+i).hide();
				$("#btns_old_"+i).show();
				$("#btns_new_"+i).hide();
			}
			$("#old_weight_"+num).hide();
			$("#new_weight_"+num).show();
			$("#control_old_"+num).hide();
			$("#control_new_"+num).show();			
		}
		function update_action(num) {
			for(var i=1;i<=<?php echo $vod_total_num; ?>;i++) {
				$("#old_weight_"+i).show();
				$("#new_weight_"+i).hide();
				$("#control_old_"+i).show();
				$("#control_new_"+i).hide();
				$("#btns_old_"+i).show();
				$("#btns_new_"+i).hide();
			}
			$("#old_weight_"+num).hide();
			$("#new_weight_"+num).show();
			$("#btns_old_"+num).hide();
			$("#btns_new_"+num).show();	
		}
		function add_weight(id,sp,num,action) {
			var weight = $("#input_weight_"+num).val();
			if(weight < 10000) {
				alert('设置失败,权重值起始值为10000');
			} else {
				action_weight(id,weight,action,sp);
			}
		}
		function update_weight(id,sp,num,action) {
			var weight = $("#input_weight_"+num).val();
			if(weight < 10000) {
				alert('修改失败,权重值起始值为10000');
			} else {
				action_weight(id,weight,action,sp);
			}
		}
		function delete_weight(id,sp,num,action) {
			var w = $("#old_weight_"+num).html();
			if(confirm("确定删除权重吗?")) {
				action_weight(id,w,action,sp);
			}
		}
		function cancel() {
			for(var i=1;i<=<?php echo $vod_total_num; ?>;i++) {
				$("#old_weight_"+i).show();
				$("#new_weight_"+i).hide();
				$("#control_old_"+i).show();
				$("#control_new_"+i).hide();
				$("#btns_old_"+i).show();
				$("#btns_new_"+i).hide();
			}
			//self.location.reload();
		}
		function action_weight(id,weight,action,sp) {
			params = {
					  'nns_id': id,
					  'sp_id': sp,
					  'weight': weight,
					  'action': action,
					 }
					 $.post('nncms_content_control_op.php', params, function(data) {
					  window.location.reload();
					 }, 'json');
			//window.location.href='nncms_content_control_op.php?action='+action+'&sp_id='+sp+'&nns_id='+id+'&weight='+weight;
		}
		</script>
	</head>

	<body>
		<div class="content">
			<div class="content_position">
				权重设置
			</div>
				<div class="content_table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<form action="" method="get">
							<tbody>
								<tr>
									<td>
										影片名/拼音：&nbsp;&nbsp;<input type="text" name="nns_name" id="nns_name" value="<?php echo $_GET['nns_name']; ?>" style="width:200px;">
										&nbsp;&nbsp;&nbsp;&nbsp;
										影片ID：&nbsp;&nbsp;<input type="text" name="video_id" id="video_id" value="<?php echo $_GET['video_id']; ?>" style="width:230px;">	
										&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="hidden" name="sp_id" value="<?php echo $sp_id; ?>" />
										<input type="hidden" name="view_list_max_num" value="<?php echo $page_size; ?>" />
										<input type="hidden" name="actionsearch" value="search" />
										&nbsp;&nbsp;&nbsp;&nbsp;	
										<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  />										
									</td>
								</tr>
							</tbody>
							</form>
						</table>
				</div>			
			<div class="content_table formtable">

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th><input name="" type="checkbox" value=""></th>
							<th>序号</th>
							<th>名称</th>
							<th>权重</th>
						    <th>操作</th>
						</tr>
					</thead>
					<tbody>
					<?php
						if($data!=null){
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) {
								$num++;
					?>
						<tr>
							<td><input name="input" type="checkbox"></td>
							<td><?php echo $num; ?></td>
							<td><a href="../vod_pages/nncms_content_vod_detail.php?nns_id=<?php echo isset($item['nns_weight']) ? $item['nns_video_id'] : $item['nns_id']?>"><?php echo $item['nns_name']; ?></a></td>
							<td>
								<span id="old_weight_<?php echo $num; ?>"><?php echo isset($item['nns_weight']) ? $item['nns_weight'] : 0; ?></span>
								<span id="new_weight_<?php echo $num; ?>" style="display:none;"><input type="text" id="input_weight_<?php echo $num; ?>" value="10000" style="width:45px;"/></span>
							</td>
							<td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
	                       			<?php
	                       			if($item['nns_weight'] < 10000) {
									?>
	                       			<span id="control_old_<?php echo $num; ?>"><a href="javascript:add_action(<?php echo $num; ?>);">添加权重</a></span>
	                        		<span id="control_new_<?php echo $num; ?>" style="display:none;">
	                        			<a href="javascript:add_weight('<?php echo $item['nns_id']; ?>','<?php echo $sp_id; ?>',<?php echo $num; ?>,'add');">确定</a>
	                        			<a href="javascript:cancel();">取消</a>
	                        		</span>
	                        		<?php 
	                        		}
	                        		if($item['nns_weight'] >= 10000) {
	                        		?>
	                        		<span id="btns_old_<?php echo $num; ?>">
	                        			<a href="javascript:update_action(<?php echo $num; ?>);">修改权重</a>
	                        			<a href="javascript:delete_weight('<?php echo $item['nns_video_id']; ?>','<?php echo $sp_id; ?>',<?php echo $num; ?>,'delete');">删除权重</a>
	                        		</span>
	                        		<span id="btns_new_<?php echo $num; ?>" style="display:none;">
	                        			<a href="javascript:update_weight('<?php echo $item['nns_video_id']; ?>','<?php echo $sp_id; ?>',<?php echo $num; ?>,'update');">确定</a>
	                        			<a href="javascript:cancel();">取消</a>
	                        		</span>
	                        		<?php }?>
                        	</td>
						</tr>
					<?php 
							}
						}
						$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) {
					?>
						<tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp; </td>
                            <td>&nbsp; </td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="pagecontrol">
			  共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_op.php?page=1<?php echo $nns_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_op.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_content_op.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_op.php?page=<?php echo $vod_pages; ?><?php echo $nns_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_op.php?ran=1<?php echo $nns_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

                        </div>                        
		</div>
	</body>
</html>
