<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset ($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "135005");
$checkpri = null;

if ($_GET["nns_org_id"]=="" || $_GET["nns_org_type"]==""){
	$nns_org_id=$_SESSION["nns_org_id"];
	$nns_org_type=$_SESSION["nns_manager_type"];
}else{
	$nns_org_id=$_GET["nns_org_id"];
	$nns_org_type=$_GET["nns_org_type"];
}

$nns_id = $_GET["nns_id"];
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
} else {
	include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
	// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
	include($nncms_db_path. "nns_depot/nns_db_depot_class.php");
	$depot_inst=new nns_db_depot_class();
	$depot_array=$depot_inst->nns_db_depot_list("",$nns_org_type,$nns_org_id,1);
	// var_dump($partner_array);
	if ($depot_array["ret"]!=0){
		$data=null;
		echo "<script>alert(". $depot_array["reason"].");</script>";
	}
	$depot_inst=null;
	$data=$depot_array["data"];

	include $nncms_db_path. "nns_carrier/nns_db_carrier_class.php";
	include $nncms_db_path. "nns_partner/nns_db_partner_class.php";
	include $nncms_db_path. "nns_group/nns_db_group_class.php";


	if ($_SESSION["nns_manager_type"]==0){
		$carrier_inst=new nns_db_carrier_class();
		$carrier_result=$carrier_inst->nns_db_carrier_list();

		if ($carrier_result["ret"]==0){
			$carrier_data=$carrier_result["data"][0];
			$base_name=$carrier_data["nns_name"];
		}
	}

	//		 	类型为合作伙伴
	if ($nns_org_type==1){
		$partner_inst=new nns_db_partner_class();
		$partner_result=$partner_inst->nns_db_partner_info($nns_org_id);

		if ($partner_result["ret"]==0){
			$partner_data=$partner_result["data"][0];
			$base_name=$partner_data["nns_name"];
		}
		//	类型为集团
	}else if ($nns_org_type==2){
		$group_inst=new nns_db_group_class();
		$group_result=$group_inst->nns_db_group_info($nns_org_id);

		if ($group_result["ret"]==0){
			$group_data=$group_result["data"][0];
			$base_name=$group_data["nns_name"];
		}
	}

	$base_id=10000;

	if (!empty($_COOKIE["page_max_num"])){
		$g_manager_list_max_num=$_COOKIE["page_max_num"];
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/dtree.js"></script>
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/tabs.js"></script>
<script language="javascript" src="../../js/cms_datepicker.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
<script language="javascript" src="../../js/category_count.js"></script>
	<script language="javascript">
	var now_select_id;
	var search_str="";
	var live_tabs;
	var url_tab="";
	var now_frame_url;
	$(document).ready(function(){
		<!-- 2012-8-29 bo.chen@starcorcn.com  统计资源库 直播分类下面的影片数量 start -->
			category.count('live',<?php echo '\''.$data[0]['nns_id'].'\''?>);
		<!-- 2012-8-29 bo.chen@starcorcn.com  统计资源库 直播分类下面的影片数量 end -->
			search_advance_show();
		set_content_width();
		$(window).resize(function(){
			set_content_width();
	});
	live_tabs=new mytabs($(".state_tabs"),refresh_table);
	//live_tabs.init();

	$(".selectbox").hide();

	$("#nns_carrier").change(function(){

		var index=$("#nns_carrier").val();
		selectCarrier(index);
	});
	$(".split_btn").click(function(){
		if ($(".category_tree").is(":hidden")){
			$(".category_tree").show();
	}else{
		$(".category_tree").hide();
	}
	set_content_width();
	});

	$(document).keydown(function(event){
		if(event.which == 13){
			search_live();
	}
	});
	});

	function begin_show_cdn(id,type){
		$("#select_frame").attr("src","../../controls/nncms_control_show_cdn.php?media_id="+id+"&media_type="+type);
		$(".selectbox").show();
	}

	function refresh_tree_content(){
		$("#advance_search").attr("checked",false);
		$(".live_frame iframe").attr("src",now_frame_url);
		search_advance_show();
	}

	function set_content_width(){
		if ($(".category_tree").is(":hidden")){
			$(".category_editbox").width($(".content").width()-25);
	}else{
		$(".category_editbox").width($(".content").width()-$(".category_tree").width()-50);
	}
	}

	function refresh_table(path){
		var tab_select_id=live_tabs.get_select_id();
		switch(tab_select_id){
		case 0:
			url_tab="";
			break;
		case 1:
			url_tab="&view_audit=<?php echo NNS_VOD_CHECK_WAITING;?>";
			break;
		case 2:
			url_tab="&view_delete=<?php echo NNS_VOD_DELETED_TRUE;?>";
			break;
	}
	refresh_live_frame();
	}
	function checkhiddenInput(){
		var par=getAllCheckBoxSelect();
		if (par==""){
			alert("<?php echo cms_get_lang('lmgl_msg_noselect');?>");
	}else{
		$('#nns_id').attr('value',getAllCheckBoxSelect());
		checkForm('<?php echo cms_get_lang('media_zykgl');?>','<?php echo cms_get_lang('msg_ask_delete');?>',$('#delete_form'));
	}
	}

	function refresh_live_frame(){
		close_select();
		//CMSVIDEO-112-6号问题修复 by 吴昊 2013/2/5
		//setCookie("page_max_num",$("#nns_list_max_num").val());
		$(".live_frame iframe").attr("src","nncms_content_livelist.php?sp_id=<?php echo $_GET['sp_id']?>&view_type=<?php echo $view_type;?>&depot_detail_id="+now_select_id+"&depot_id=<?php echo $data[0]["nns_id"];?>&view_list_max_num="+$("#nns_list_max_num").val()+search_str+url_tab);

		$.get("nncms_content_livelist.php?sp_id=<?php echo $_GET['sp_id']?>&ajax=count_live&view_type=<?php echo $view_type;?>&depot_detail_id="+now_select_id+"&depot_id=<?php echo $data[0]["nns_id"];?>&view_list_max_num="+$("#nns_list_max_num").val()+search_str+url_tab,function(result){
			$('#count_all').html(result.count_all);
			$('#count_check').html(result.count_check);
			$('#count_deleted').html(result.count_deleted);
	},'json');
	}

	function select_tree_item(item_id,item_name,parent_id){


		$(".category_editbox").find(".category_edit_id").html(item_id);
		$(".category_editbox").find(".category_edit_name").html(item_name);
		$(".live_frame").show();
		now_select_id=item_id;
		search_str="";
		refresh_live_frame();

	}



	function checkheight(){
		var frame_url=$(".live_frame iframe")[0].contentWindow.location.href;
		var page_name=frame_url.split("?")[0];
		page_name=page_name.split("/");
		page_name=page_name[page_name.length-1];
		if (page_name=="nncms_content_livelist.php"){
			$(".display_list").show();
	}else{
		$(".display_list").hide();
		$("#search_advance_tr").hide();
	}

	$(".live_frame iframe").height($(".live_frame iframe").contents().find(".content").height());
	$(".category_tree").height($(".category_editbox").height()-20);
	$(".split_btn").height($(".category_editbox").height()-20);
	window.parent.resetFrameHeight();

	}

	function search_live(){
		search_str="&search_py="+encodeURI($("#nns_live_pinyin").val());
		if ($("#advance_search").attr("checked")){
			search_str+="&nns_live_search="+encodeURI($("#nns_live_search").val());
			search_str+="&search_yy="+encodeURI($("#nns_yy_search").val());
			search_str+="&search_dy="+encodeURI($("#nns_dy_search").val());
			search_str+="&search_type="+encodeURI($("#view_type_search").val());
			search_str+="&search_episode="+encodeURI($("#episode_search").val());
			search_str+="&search_dq="+encodeURI($("#media_dq_search").val());
			search_str+="&search_len_min="+encodeURI($("#view_len_min_search").val()*60);
			search_str+="&search_len_max="+encodeURI($("#view_len_max_search").val()*60);
			search_str+="&search_summary="+encodeURI($("#summary_search").val());
			$check_arr=$("#nns_point_search input[type=checkbox]:checked");
			var len=$check_arr.length;
			var point_search="";
			for (var i=0;i<len;i++){
				point_search+=$check_arr.eq(i).val()+",";
	}
	search_str+="&search_point="+encodeURI(point_search);
	search_str+="&search_sy_begin_date="+$("#nns_sy_begin_date").val();
	search_str+="&search_sy_end_date="+$("#nns_sy_end_date").val();
	search_str+="&search_rk_begin_date="+$("#nns_rk_begin_date").val();
	search_str+="&search_rk_end_date="+$("#nns_rk_end_date").val();
	search_str+="&search_copyright_begin_date="+$("#nns_copyright_begin_date").val();
	search_str+="&search_copyright_end_date="+$("#nns_copyright_end_date").val();
	}
	refresh_live_frame();

	}
	function selectCarrier(num){
		switch(num){
		case "1":
			begin_select_partner();
			break;
		case "2":
			begin_select_group();
			break;
		default:
			window.location.href="nncms_content_live.php?nns_id=<?php echo $nns_id;?>&view_type=<?php echo $view_type;?>";
			break;
	}
	}

	function begin_select_partner(){
		$("#select_frame").attr("src","../../controls/nncms_controls_select.php?method=partner");
		$(".selectbox").show();
	}

	function begin_select_group(){
		$("#select_frame").attr("src","../../controls/nncms_controls_select.php?method=group");
		$(".selectbox").show();
	}
	function close_select(){
		$(".selectbox").hide();
	}

	function set_partner_id(value,name){
		window.location.href="../contentframe/live_pages/nncms_content_live.php?nns_org_id="+value+"&nns_org_type=1&nns_id=<?php echo $nns_id;?>&view_type=<?php echo $view_type;?>&group_name="+name;
	}

	function set_group_id(value,name){
		window.location.href="../contentframe/live_pages/nncms_content_live.php?nns_org_id="+value+"&nns_org_type=2&nns_id=<?php echo $nns_id;?>&view_type=<?php echo $view_type;?>&group_name="+name;
	}


	function set_move_category_id(value,depot_id){
		$(".live_frame iframe")[0].contentWindow.set_move_category_id(value,depot_id);
		close_select();
	}
	function begin_select_category(){
		$("#select_frame").attr("src","../../controls/video_category_select/nncms_controls_video_category_select.php?method=live&org_id=<?php echo $nns_org_id;?>&org_type=<?php echo $nns_org_type;?>");
		$(".selectbox").show();
	}

	function search_advance_show(){
		if ($("#advance_search").attr("checked")=="checked"){

			$("#search_advance_tr").show();
	}else{
		search_live();
		$("#search_advance_tr").hide();
	}
	window.parent.resetFrameHeight();
	}
	function export_csv_file(){
		window.open("nncms_content_live_csv_export.php?depot_detail_id="+now_select_id+"&depot_id=<?php echo $data[0]["nns_id"];?>"+search_str+url_tab);
	}
	
	
function begin_select_partner(){
	$("#select_frame").attr("src","../../controls/nncms_controls_select.php?method=partner");
	$(".selectbox").show();
}
function get_tags_edit(tags){
close_select();
	$(".live_frame iframe")[0].contentWindow.get_tags_edit(tags);
}
function begin_show_tags(){
	$("#select_frame").attr("src","../../controls/epg_select/nncms_controls_epg_select.php");
	$(".selectbox").show();
}
	</script>
</head>

<body>
<div class="selectbox">
		<iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
 </div>
<div class="content">
	<div class="content_position">直播注入</div>
	<div style="background-color:#FFF;">
		<div class="category_tree">
		<div style="padding:5px; background:#eeeeee; border:3px solid #ccc; margin-bottom:10px;">
			<?php if ($_SESSION["nns_manager_type"]==0){?>
				<tr class="select_carrier_class">

					<td>
				<select name="nns_carrier" id="nns_carrier"  style=" padding:3px; width:100%;">
							<option value="<?php echo $carrier_data["nns_id"];?>" <?php if ($nns_org_type==0){?>selected="selected"<?php }?>><?php echo cms_get_lang('xtgl_yysgl'),'/',$carrier_data["nns_name"];?></option>
							<?php if ($g_partner_enabled==1){?>
							<option value="1"  <?php if ($nns_org_type==1){?>selected="selected"<?php }?>><?php echo cms_get_lang('partner');?></option>
							<?php }if ($g_group_enabled==1){?>
							<option value="2"  <?php if ($nns_org_type==2){?>selected="selected"<?php }?>><?php echo cms_get_lang('group');?></option>
							<?php }?>
						</select>
							</td>
					<td></td>
					<td></td>
				</tr>
				<?php }?>
				</div>
<?php
	if ($data!=null){
		$assist_content=$data[0]["nns_category"];
	}else{
		$assist_content=null;
	}
	echo	pub_func_get_tree_by_html($assist_content,$base_name,$base_id);

?>

		</div>
		 <div class="split_btn"></div>
		<div class="category_editbox" >
			 <div class="content_table">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tbody>

			<tr>
							<td class="rightstyle2"><?php echo cms_get_lang('lmgl_nrlmid');?>&nbsp;/&nbsp;<?php echo cms_get_lang('lmgl_lmmc');?>:</td>
							<td><span class="category_edit_id"></span><span class="category_edit_name"></span></td>
						</tr>
						  <tr class="display_list">
							<td class="rightstyle2"><?php echo cms_get_lang('media_zb|search');?>:</td>
							<td><?php echo cms_get_lang('media_pinyin');?>&nbsp;&nbsp;<input name="nns_live_pinyin" id="nns_live_pinyin" type="text"
								value="" style="width:150px;"
								/>&nbsp;&nbsp;<input type="button" value="<?php echo cms_get_lang('search');?>" onclick="search_live();"/></td>

							<td><input name="input" type="checkbox" value="0" onclick="search_advance_show();" name="advance_search" id="advance_search"/>&nbsp;&nbsp;<?php echo cms_get_lang('advance|search');?></td>
							<td></td>

						</tr>
						<tr  id="search_advance_tr" >
							<td colspan="4">
								<div class="content_table formtable" style="border:2px solid #B0BEC6; padding:5px 0px;">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<!--<td><?php echo cms_get_lang('media_video_lx');?></td>
									<td><select name="view_type_search" id="view_type_search"  style="margin:5px 0px;"><br>
										<option value="" >--</option>
										<option value="0" ><?php echo cms_get_lang('media_dy');?></option>
											<option value="1" ><?php echo cms_get_lang('media_lxj');?></option>
									</select></td>-->
									<td><?php echo cms_get_lang('title');?></td>
									<td><input name="nns_vod_search" id="nns_vod_search" type="text"
									value="" style="width:120px;"
									/></td>
									<td><?php echo cms_get_lang('media_dq');?></td>
									<td><input name="media_dq_search" id="media_dq_search" type="text" value=""  style="width:120px;" /></td>
								</tr>
								<tr>

									<td><?php echo cms_get_lang('media_yy');?></td>
									<td><input name="nns_yy_search" id="nns_yy_search" type="text"
										value="" style="width:120px;"
										/></td>

									
									<td><?php echo cms_get_lang('media_tjfs');?></td>
									<td id="nns_point_search"  style="width:230px;">

										<input name="input" type="checkbox" value="1" />1<?php echo cms_get_lang('point');?>&nbsp;
										<input name="input" type="checkbox" value="2" />2<?php echo cms_get_lang('point');?>&nbsp;
										<input name="input" type="checkbox" value="3" />3<?php echo cms_get_lang('point');?>&nbsp;
										<input name="input" type="checkbox" value="4" />4<?php echo cms_get_lang('point');?>&nbsp;
										<input name="input" type="checkbox" value="5" />5<?php echo cms_get_lang('point');?>&nbsp;
									</td>
								</tr>
								<tr>
									<td><?php echo cms_get_lang('media_daoyan');?></td>
									<td><input name="nns_dy_search" id="nns_dy_search" type="text"
										value=""  style="width:120px;"
										/></td>
									<td><?php echo cms_get_lang('create_time');?></td>
									<td><input name="nns_rk_begin_date" id="nns_rk_begin_date" type="text"
								value=""  style="width:70px;" class="datepicker date_begin"/> - <input name="nns_rk_end_date" id="nns_rk_end_date" type="text" value=""  style="width:70px;" class="datepicker date_end" />
									</td>
								</tr>
								<tr>
									<td><?php echo cms_get_lang('media_sysj');?></td>
									<td><input name="nns_sy_begin_date" id="nns_sy_begin_date" type="text"
								value=""  style="width:70px;" class="datepicker date_begin"/> - <input name="nns_sy_end_date" id="nns_sy_end_date" type="text" value=""  style="width:70px;" class="datepicker date_end" /></td>
									
									<td><?php echo cms_get_lang('media_copyright_date');?></td>
									<td><input name="nns_copyright_begin_date" id="nns_copyright_begin_date" type="text"
								value=""  style="width:70px;" class="datepicker date_begin"/> - <input name="nns_copyright_end_date" id="nns_copyright_end_date" type="text" value=""  style="width:70px;" class="datepicker date_end"/>
									</td>
								</tr>
								<tr>
									<td><?php echo cms_get_lang('media_fps');?></td>
									<td><input name="episode_search" id="episode_search" type="text"
										value=""  style="width:120px;"
									/></td>
									<td><?php echo cms_get_lang('media_zsc');?></td>
									<td><input name="view_len_min_search" id="view_len_min_search" type="text"
										value=""  style="width:70px;"
										/> - <input name="view_len_max_search" id="view_len_max_search" type="text"
										value=""  style="width:70px;"
										/></td>
								</tr>
								<tr><td><?php echo cms_get_lang('media_nrjj');?></td>
									<td><input name="summary_search" id="summary_search" type="text"
										value=""  style="width:120px;"
										/></td></tr>
								</table>
								</div>
							</td>
						</tr>

				  </tbody>
				  </table>
				</div>
				 <div class="state_tabs display_list">
					<div class="state_tab" pos="nncms_content_livelist.php?view_type=0"><?php echo cms_get_lang('service_syzy'),'(<b id="count_all"></b>)';?></div>
					<div class="state_tab" pos="nncms_content_livelist.php?view_audit=<?php echo NNS_VOD_CHECK_WAITING;?>"><?php echo cms_get_lang('service_dshzy'),'(<b id="count_check"></b>)';?></div>
					<div class="state_tab" pos="nncms_content_livelist.php?view_delete=<?php echo NNS_VOD_DELETED_TRUE;?>"><?php echo cms_get_lang('service_ysczy'),'(<b id="count_deleted"></b>)';?></div>
					<div class="list_max_num" style=" float:right; margin-right:10px;">
						&nbsp;&nbsp;&nbsp;<input style="display:none;" name="nns_list_max_num" id="nns_list_max_num" type="text" value="<?php echo $g_manager_list_max_num;?>" style="width:20px; padding:3px;"/>
						&nbsp;&nbsp;<input style="display:none;" type="button" value="<?php echo cms_get_lang('confirm');?>" onclick="refresh_vod_frame();"/>
						&nbsp;&nbsp;<input style="display:none;" type="button" value="<?php echo cms_get_lang('export_csv');?>" onclick="export_csv_file();"/>
					</div>
					<div style="clear:both;"></div>
				</div>
				<div class="live_frame">
					<iframe  scrolling="no" frameborder="0" onload="checkheight();" style="width:100%;"></iframe>
				</div>

		</div>
		<div style="clear:both;"/>
	</div>


</div>
</body>
</html>

<?php }?>
