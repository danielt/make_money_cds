<?php
header("Content-Type:text/html;charset=utf-8");
include("../../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
require_once $nncms_config_path . "op/op_queue.class.php";
require_once $nncms_db_path . "nns_vod/nns_db_vod_class.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
			)); 
 $dc->open();
 $db = $dc->db();
 $params = array();
 $params['org_id'] = $_POST["sp_id"];
 $params['ex_id'] = $_POST["nns_id"];
 $params['type'] = BK_OP_VIDEO;
 $params['weight'] = $_POST["weight"];
 $params['action'] = $_POST["action"];
 $op_inst = new op_queue();
 $vod_inst = new nns_db_vod_class();
 switch($params['action']){
	case "add":
		$result = $op_inst->update_weight($db,$params);
	break;
	case "update":
		$result = $op_inst->update_weight($db,$params);
	break;
	case "delete":
		$result = $op_inst->delete_weight($db,$params);
	break;
	case "once": //一键设置权重
		$category_id = strlen($_POST["depot_detail_id"]) != 5 ? $_POST["depot_detail_id"] : '';
		$depot_id = $_POST['depot_id'];
		$name = $_POST['nns_name'];
		$vod_id = $_POST['nns_vod_id'];
		$ids = $_POST['ids'];
		$org_id = $_POST["sp_id"];
		if(isset($vod_id) && !empty($vod_id)) { //有查询内容
			$video_id_array = array($vod_id);
		} elseif (strlen($ids) > 0 && !empty($ids)) { //选择的复选框
			$str = rtrim($ids,',');
			$video_id_array = explode(',', $str);
		} else {
			$video_id_array = null;
		}

		$vod_result = $vod_inst->nns_db_vod_list('nns_id',array('nns_name'=>$name,'nns_pinyin'=>$name),null,null,null,$depot_id,$category_id,null,null,null,null,'',null,null,NNS_VOD_DELETED_FALSE,'weight',null,$org_id,null, null,null,null,null,null,null,null,null,null,null,null,null,null,$video_id_array);

		if(is_array($vod_result['data']) && !empty($vod_result['data'])) {
			$data = $vod_result['data'];
			$count = count($data);
		} else {
			$result = false;
			break;
		}
		$re = $op_inst->get_max_weight($db, array('org_id'=>$org_id));
		if(is_array($re)) {
			$max = !empty($re[0]['num']) ? $re[0]['num'] : 10000;
		}
		$weight_info = array();
		$weight_info['org_id'] = $org_id;		
		$weight_info['type'] = BK_OP_VIDEO;		
		$desc_weight = $count+$max;

		foreach($data as $key=>$value) {
			$weight_info['ex_id'] = $value["nns_id"];
			$weight_info['weight'] = $desc_weight;
			$result = $op_inst->update_weight($db,$weight_info);
			--$desc_weight;
		}
	break;
	case "cancel": //一键取消权重
		$category_id = strlen($_POST["depot_detail_id"]) != 5 ? $_POST["depot_detail_id"] : '';
		$depot_id = $_POST['depot_id'];
		$name = $_POST['nns_name'];
		$vod_id = $_POST['nns_vod_id'];
		$ids = $_POST['ids'];
		$info = array();
		$info['nns_org_id'] = $_POST["sp_id"];
		if(isset($name) && !empty($name)) { //查询出的名称
			$info['nns_name'] = $name;
		}
		if(isset($vod_id) && !empty($vod_id)) { //有查询内容
			$info['nns_video_id'] = $vod_id;
			$video_id_array = array('nns_id'=>$info['nns_video_id']);
		} elseif (strlen($ids) > 0 && !empty($ids)) { //选择的复选框
			
			$video_id_array = array('nns_id' => $ids);
		} else {
			$video_id_array = null;
		}
		
		$where = array('nns_depot_id'=>$depot_id,'nns_category_id'=>$category_id);
		if(is_array($video_id_array)) {
			$where = array_merge($where,$video_id_array);
		}
		$op_result = $op_inst->get_weight($db, $info, $where);
//		$vod_result = $vod_inst->nns_db_vod_list('nns_id',array('nns_name'=>$info['nns_name'],'nns_pinyin'=>$info['nns_name']),null,null,null,$depot_id,$category_id,null,null,null,null,'',null,null,NNS_VOD_DELETED_FALSE,'weight',null,$info['nns_org_id'],null, null,null,null,null,null,null,null,null,null,null,null,null,null,$video_id_array);

		if(is_array($op_result) && !empty($op_result)) {
			$data = $op_result;
		} else {
			$result = false;
			break;
		}
		//删除权重
		$weight_info = array();
		$weight_info['org_id'] = $info['nns_org_id'];
		$weight_info['type'] = BK_OP_VIDEO;

		foreach ($data as $value) {				
			$weight_info['ex_id'] = $value['nns_video_id'];
			$weight_info['weight'] = $value['nns_weight'];		
			$result = $op_inst->delete_weight($db,$weight_info);
		}
	break;
  }
  if($result === true) {
  	//echo "<script>alert('操作成功!');</script>";
  	//echo "<script>self.location='nncms_content_op.php?sp_id=".$params['org_id']."';</script>";
  	return true;
  } else {
  	return false;
  	//echo "<script>alert('操作失败!');</script>";
  	//echo "<script>self.location='nncms_content_op.php?sp_id=".$params['org_id']."';</script>";
  }
?>