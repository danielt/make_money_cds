<?php
header('Content-Type:text/html;charset=utf-8');
include '../../nncms_manager_inc.php';
include $nncms_config_path . '/nn_cms_manager/controls/nncms_controls_check_login.php';

$action = $_POST['action'];

include $nncms_db_path . '/nns_pri/nns_db_pri_class.php';
$checkPri = new nns_db_pri_class();
$hasPri = false;
$privileges = array(
    'edit' => '135005',
    'add' => '135005',
    'delete' => '135005',
    'up' => '135005',
    'down' => '135005',
    'syn_category'=>'135005',
);
$hasPri = $checkPri->nns_db_pri_check($_SESSION['nns_role_pris'], $privileges[$action]);
$checkPri = null;

if (!$hasPri) {
    header('Location: ../nncms_content_wrong.php');
    exit(-1);
}

include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
include $nncms_config_path . '/nn_cms_db/nns_log/nns_db_op_log_class.php';
$log = new nns_db_op_log_class();
$op_desc = "栏目管理SP:".$_POST['org_id'].'   :数据:'.htmlspecialchars(var_export($_POST,true),ENT_QUOTES);
$org_type = 0;
$org_id = $_SESSION['nns_org_id'];
$log->nns_db_op_log_add($_SESSION['nns_mgr_id'], $action, $op_desc, $org_type, $org_id);
switch ($action) {
    case 'edit':		
        $data = array(
            'nns_name' => $_POST['nns_category_detail_name'],
            //'nns_sort' => 0,
            
        );
        $condition = array(
            'nns_id' => $_POST['nns_category_id'],
            'nns_org_id' => $_POST['org_id'],

        );
        showAlert(sp_category_model::modify_asset_category($data, $condition));
        break;

    case 'add':
        if (empty($_POST['category_parent'])) {
            $categoryId = $_POST['baseid'] . $_POST['nns_category_detail_id'];
        } else {
            $categoryId = $_POST['category_parent'] . $_POST['nns_category_detail_id'];
        }
        if (empty($_POST['nns_sp_category_id'])) {
            $nns_sp_category_id = $categoryId;
        } else {
            $nns_sp_category_id = $_POST['nns_sp_category_id'];
        }

        if (empty($_POST['category_parent'])) {
            $parentId = $_POST['baseid'];
        } else {
            $parentId = $_POST['category_parent'];
        }
		
		if($nns_sp_category_id == $_POST['base_sp_id']){
			echo '<script type="text/javascript">alert("不能创建root栏目");</script>';
			echo '<script type="text/javascript">history.go(-1);</script>';
			die;
		}

        $maxSort === true && $maxSort = -1;
        $b_exists  = sp_category_model::exists_asset_category($categoryId,$_POST['org_id']);
        if($b_exists){
        	 echo '<script type="text/javascript">alert("栏目ID已存在");</script>';
        }else{
	        $data = array(
	            'nns_id' => $categoryId,
	            'nns_org_id' => $_POST['org_id'],
	            'nns_sp_category_id' => $nns_sp_category_id,
	            'nns_name' => $_POST['nns_category_detail_name'],
	            'nns_modify_time' => date('00-00-00 00:00:00'),
	            'nns_create_time' => date('Y-m-d H:i:s'),
	            'nns_sort' => 0,
	        );
	        $result = sp_category_model::add_asset_category($data);
         }
        showAlert($result);
        break;

    case 'delete':		
        showAlert(sp_category_model::delete_asset_category($_POST['nns_category_id'],$_POST['org_id']));
        break;

    case 'up':
    case 'down':
        $result = sp_category_model::sort_asset_category($_POST['nns_category_id'],$action);//modify_asset_category_pos($_POST['nns_category_id'], 'swap', $action);

        showAlert($result);

        break;
    case 'syn_category':		
         $data = array(
            'nns_name' => $_POST['nns_category_detail_name'],
            'nns_sp_category_id' => $_POST['nns_sp_category_id'],
            'nns_org_id'=>$_POST['org_id']
         );
         $result = sp_category_model::syn_asset_category($data);
         showAlert($result);
        break;
    default:
        break;
}
echo '<script type="text/javascript">history.go(-1);</script>';
exit(0);

function showAlert($result)
{
	   	
    if ($result) {
        echo '<script type="text/javascript">alert("栏目更改成功");location.href=document.referrer;</script>';
    } else {
        echo '<script type="text/javascript">alert("栏目更改失败");history.go(-1);</script>';
    }
}
