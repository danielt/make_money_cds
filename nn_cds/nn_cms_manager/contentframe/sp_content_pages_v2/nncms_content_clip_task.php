<?php header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135003");
$checkpri = null;



$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$group_name = $_GET["group_name"];
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}


include_once dirname(dirname(dirname(dirname(__FILE__)))) .'/nn_logic/sp/sp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) .'/mgtv_v2/models/queue_task_model.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) .'/nn_logic/clip_task/clip_task.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) .'/nn_logic/clip_task/clip_task_log.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_class/file_encode/file_encode.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/op_queue/op_queue.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_class/ftp/ftp.class.php';
include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/np/np_http_curl.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_class/file_encode/make_file_encode.class.php';
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
$sp_id = $_GET['sp_id'];
$obj_dc= nl_get_dc(array (
		'db_policy' => NL_DB_WRITE, 
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$sp_config = nl_sp::get_sp_config($obj_dc, $sp_id);
$sp_config = isset($sp_config['data_info']['nns_config']) ? $sp_config['data_info']['nns_config'] : null;

$config_file_encode_flag = false;
if(isset($sp_config['clip_file_encode_enable']) && intval($sp_config['clip_file_encode_enable']) ===1 && isset($sp_config['clip_file_encode_model']) && intval($sp_config['clip_file_encode_model']) ===1)
{
    $config_file_encode_flag = true;
}


$result_cp_data = nl_query_by_db("select nns_id,nns_name from nns_cp", $obj_dc->db());
$result_cp_data_keys = $result_cp_id_data = array();
if(is_array($result_cp_data))
{
    foreach ($result_cp_data as $cp_val)
    {
        $result_cp_id_data[$cp_val['nns_id']] = $cp_val['nns_name'];
    }
    $result_cp_data_keys = array_keys($result_cp_id_data);
}
if(isset($_GET['op'])){
	$action =trim($_GET['op']);
	switch($action){
		/*
        case 'force_restart':
        $result = clip_task_model::restart_task($_GET['task_id'],true);
        if($result){
        	echo '<script>alert("强制切片成功");</script>';
        }else{
        	echo '<script>alert("强制切片失败");</script>';
        }
        break;		
        */
        case 'restart':
	        $force = $_GET['force']?true:false;
	        
	        $task_ids = explode(",",$_GET['task_id']);
	        foreach($task_ids as $item_id){
	        	$item_id && clip_task_model::restart_task($item_id,$force);
	        }
            if($force){
            	echo '<script>alert("添加强制任务成功");</script>';
            }else{
            	echo '<script>alert("重新添加任务成功");</script>';
            }
        	

        break;        
        case 'sort':
            if($_GET['d'] == 'top')
            {
                $result_max = nl_clip_task::get_max_priority($obj_dc,$sp_id);
                if($result_max['ret'] !=0)
                {
                    echo '<script>alert("'.$result_max['reason'].'");</script>';
                }
                else
                {
                    $num = isset($result_max['data_info']) ? (int)$result_max['data_info'] : 0;
                    
                    $arr_task_id = explode(',', $_GET['task_id']);
                    if(is_array($arr_task_id) && !empty($arr_task_id))
                    {
                        foreach ($arr_task_id as $str_task_id)
                        {
                            if(strlen($str_task_id) <1)
                            {
                                continue;
                            }
                            $num++;
                            $num = ($num > 65535) ? 65535 : $num;
                            $result = nl_clip_task::edit($obj_dc,array('nns_priority'=>$num),$str_task_id);
                        }
                    }
                    if($result){
                        echo '<script>alert("任务排序成功");</script>';
                    }else{
                        echo '<script>alert("任务排序失败");</script>';
                    }
                }
            }
            else if($_GET['d'] == 'bottom')
            {
                $arr_task_id = array_filter(explode(',', $_GET['task_id']));
                $result = nl_clip_task::edit($obj_dc,array('nns_priority'=>0),$arr_task_id);
                if($result){
                    echo '<script>alert("任务排序成功");</script>';
                }else{
                    echo '<script>alert("任务排序失败");</script>';
                }
            }
            else if($_GET['d'] == 'up')
            {
                $num = isset($_GET['nns_priority']) ? (int)$_GET['nns_priority'] : 0;
                $num++;
                $num = ($num > 65535) ? 65535 : $num;
                $result = nl_clip_task::edit($obj_dc,array('nns_priority'=>$num),$_GET['task_id']);
                if($result){
                    echo '<script>alert("任务排序成功");</script>';
                }else{
                    echo '<script>alert("任务排序失败");</script>';
                }
                unset($_GET['nns_priority']);
            }
            else if($_GET['d'] == 'down')
            {
                $num = isset($_GET['nns_priority']) ? (int)$_GET['nns_priority'] : 0;
                if($num <1)
                {
                    echo '<script>alert("任务排序成功");</script>';
                }
                else
                {
                    $num--;
                    $result = nl_clip_task::edit($obj_dc,array('nns_priority'=>$num),$_GET['task_id']);
                    if($result){
                        echo '<script>alert("任务排序成功");</script>';
                    }else{
                        echo '<script>alert("任务排序失败");</script>';
                    }
                }
                unset($_GET['nns_priority']);
            }
            else if($_GET['d'] == 'set')
            {
                $num = (isset($_GET['nns_priority']) && (int)$_GET['nns_priority'] >=0) ? (int)$_GET['nns_priority'] : 0;
                $num = ($num > 65535) ? 65535 : $num;
                $result = nl_clip_task::edit($obj_dc,array('nns_priority'=>$num),$_GET['task_id']);
                if($result){
                    echo '<script>alert("任务排序成功");</script>';
                }else{
                    echo '<script>alert("任务排序失败");</script>';
                }
                unset($_GET['nns_priority']);
            }
	        break;
        case 'file_encode':
            $arr_task_id = explode(',', $_GET['task_id']);
            if(is_array($arr_task_id) && !empty($arr_task_id))
            {
                $obj_file_encode = new make_file_encode_queue($obj_dc);
                foreach ($arr_task_id as $str_task_id)
                {
                    if(strlen($str_task_id) <1)
                    {
                        continue;
                    }
                    $result = $obj_file_encode->init($sp_id,$str_task_id);
                }
            }
            if($result['ret'] =='0'){
                echo '<script>alert("任务转码成功");</script>';
            }else{
                echo '<script>alert("任务转码失败");</script>';
            }
            break;
	   case 'delete_movie':
	        $task_ids = explode(",",$_GET['task_id']);
	        $result = clip_task_model::delete_movie_by_task_id($task_ids);
	        if($result){
	        	echo '<script>alert("删除执行成功");</script>';
	        }else{
	        	echo '<script>alert("删除执行失败");</script>';
	        }        
	        break;
        case 'set_weight':
        	$result = clip_task_model::set_weight($_GET['str_weight_ids'],$_GET['sp_id']);
        	if($result === true)
        	{
        		echo '<script>alert("一键设置权重执行成功");</script>';
        	}
        	else if($result === 9999)
        	{
        		echo '<script>alert("一键设置权重执行成功,部分数据执行失败！\r\n权重上限为9999，取消一些权重或者等待队列执行完毕再设置权重！");</script>';
        	}
        	else
        	{
        		echo '<script>alert("一键设置权重执行失败");</script>';
        	}
        	unset($_GET['str_weight_ids']);
        	break;
        case 'cancel_weight':
        	$result = clip_task_model::cancel_weight($_GET['str_weight_ids'],$_GET['sp_id']);
        	if($result)
        	{
        		echo '<script>alert("一键取消权重执行成功");</script>';
        	}
        	else
        	{
        		echo '<script>alert("一键取消权重执行失败");</script>';
        	}
        	unset($_GET['str_weight_ids']);
        	break;
        case 'res_encode':
            $_GET['task_id'] = trim($_GET['task_id'],',');
            $task_ids = explode(",",$_GET['task_id']);
            $result_task= nl_clip_task::query_by_ids($obj_dc, $task_ids);
            if($result_task['ret'] !=0)
            {
                echo '<script>alert("'.$result_task['reason'].'");history.go(-1);</script>';
            }
            $queue_task_model = new queue_task_model($obj_dc,$sp_id,$sp_config);
            $result_task_queue=$queue_task_model->get_encode_clip_fail_task(0,$result_task);
            if($result_task_queue['ret'] !=0)
            {
                echo '<script>alert("'.$result_task_queue['reason'].'");history.go(-1);</script>';
            }
            break;
	}
	unset($_GET['op']);
	unset($_GET['force']);
	unset($_GET['task_id']);
	unset($_GET['d']);
}


$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : (!empty($_COOKIE["page_max_num"]) ?  $_COOKIE["page_max_num"] : 18);
$page_num = isset($_GET['page'])?$_GET['page']:1;
if($page_num<1){
	$page_num = 1;
}

$offset = intval($page_num-1)*$page_size;
$limit = $page_size;
$filter = null;
if($_GET['action']=='search'){
	$filter['nns_task_name'] = trim($_GET['nns_task_name']);
	$filter['nns_state'] = $_GET['nns_state'];
	$filter['day_picker_start'] = $_GET['day_picker_start'];
	$filter['day_picker_end'] = $_GET['day_picker_end'];
	$filter['day_picker_put_start'] = $_GET['day_picker_put_start'];
	$filter['day_picker_put_end'] = $_GET['day_picker_put_end'];
	$filter['start_time'] = $_GET['start_time'];
	$filter['end_time'] = $_GET['end_time'];
	$filter['nns_id'] = $_GET['nns_id'];
	$filter['nns_file_hit'] = $_GET['nns_file_hit'];
	$filter['nns_video_type'] = (isset($_GET['nns_video_type']) && strlen($_GET['nns_video_type']) >0) ? $_GET['nns_video_type'] : '';
	if(isset($_GET['nns_cp_id']) && strlen($_GET['nns_cp_id'])>0)
	{
	    $filter['nns_cp_id'] = $_GET['nns_cp_id'];
	}
	//切片服务器id
    $filter['nns_tom3u8_id'] = $_GET['nns_tom3u8_id'];
}
$result =  clip_task_model::get_task_list($sp_id,$filter,$offset,$limit);

$vod_total_num = $result['rows'];//记录条数

$vod_pages = ceil($vod_total_num / $page_size);//总页数
if($page_num>$vod_pages){
	$page_num = $vod_pages;
}


$currentpage = $page_num;
$g_manager_list_max_num = $page_size;
unset($_GET['page']);
$nns_url = "&".http_build_query($_GET);//"&view_list_max_num=$page_size&sp_id=$sp_id";
$refresh = "?".http_build_query($_GET);//"?page=$page_num&sp_id=$sp_id&view_list_max_num=";


$data = $result['data'];
$sp_config = sp_model::get_sp_config($sp_id);
if (isset($_GET['export']) && $_GET['export'] == 1) {
	
	header('Content-Type: text/csv');
	header('Content-Disposition: attachment;filename=clip_task_fail.csv');
	$fp = fopen('php://output', 'w');
	fwrite($fp, "\xEF\xBB\xBF");
	$dom	= new DOMDocument('1.0', 'utf-8');
	$array_category = array(
		'10000001'=>'电影',
		'10000002'=>'电视剧',
		'10000003'=>'综艺',
		'10000004'=>'动漫',
		'10000005'=>'音乐',
		'10000006'=>'纪实',
		'10000010'=>'纪录片',
	);
	if (is_array($data)) {
		
		$cdn_import_media_mode = $sp_config['cdn_import_media_mode'];
		foreach ($data as $item0) {	
			$cols = array();
			//$cols[] = $item0['nns_task_name'];
			
			
			$info = video_model::get_vod_info($item0['nns_video_id']);
			$index_info = video_model::get_vod_index_info($item0['nns_video_index_id']);
			$media_info = video_model::get_vod_media_info($item0['nns_video_media_id']);
			if(!empty($item0['nns_content'])){
			
				$xml = htmlspecialchars_decode($item0['nns_content']);
				$dom -> loadXML($xml);
				$xpath = new DOMXPath($dom);
				$nns_vod_name = $xpath->query('/task/video/index_list/index')->item(0)->getAttribute('name');
				$nns_vod = $xpath->query('/task/video')->item(0)->getAttribute('name');
				$cols[] = $nns_vod;
				$cols[] = $nns_vod_name;
				$cols[] = $info['nns_category_id'];
				$cols[] = $info['nns_asset_import_id'];
				$cols[] = $index_info['nns_import_id'];
				$cols[] = $media_info['nns_import_id'];
				unset($info);
				unset($index_info);
				unset($media_info);
				$cols[] = $item0['nns_desc'];
				$entries = $xpath->query('/task/video/index_list/index/media_list/media');
				foreach($entries as $item){
					$cols[] = $item->getAttribute('content_url');
				}
			}else{
				$info_index = video_model::get_vod_index_info($item0['nns_video_index_id']);
				$nns_vod = $info['nns_name'];
				$nns_vod_name = $info_index['nns_name'];
				$cols[] = $nns_vod;
				$cols[] = $nns_vod_name;
				$cols[] = $info['nns_category_id'];
				$cols[] = $info['nns_asset_import_id'];
				$cols[] = $index_info['nns_import_id'];
				$cols[] = $media_info['nns_import_id'];
				unset($info);
				unset($index_info);
				unset($media_info);
				$str = "";
				if($cdn_import_media_mode==1){//混合模式				
					$fileids = video_model::get_vod_medias_by_index($item0['nns_video_index_id']);
					foreach ($fileids as $value) {
						$str .=$value['nns_import_id'].",";
					}				
				}else{//单片模式
					$fileid = video_model::get_vod_media_info($item0['nns_video_media_id']);
					$str = $fileid['nns_import_id'];
				}
				$cols[] = $str;
				$cols[] = $item0['nns_desc'];
			}
			
			
			//$cols[] = $nns_type;
			//$cols[] = $item0['nns_action'];
			//$cols[] = $item0['nns_create_time'];
			//if(empty())
			/*$str = "";
			if($cdn_import_media_mode==1){//混合模式				
				$fileids = video_model::get_vod_medias_by_index($item0['nns_video_index_id']);
				foreach ($fileids as $value) {
					$str .=$value['nns_import_id'].",";
				}				
			}else{//单片模式
				$fileid = video_model::get_vod_media_info($item0['nns_video_media_id']);
				$str = $fileid['nns_import_id'];
			}
			$cols[] = $str;*/
			fputcsv($fp, $cols);
			unset($fileids,$str,$fileid,$cols);
		}
	}
	fclose($fp);
	exit ;
	
	
	
}


?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript">
			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
                setCookie("page_max_num",num);
				window.location.href='<?php echo $refresh;?>&view_list_max_num='+num
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				})
				return BoxKey;
			}
			
			function add_task_tmp(nns_id){				
				$.get("nncms_content_task.php?sp_id=<?php echo $sp_id?>&ajax=add&nns_id="+nns_id,function(result){					
					alert(result);window.location.reload();
				},'json');
			}			
			function stop_task_tmp(nns_id){				
				$.get("nncms_content_task.php?sp_id=<?php echo $sp_id?>&ajax=stop&nns_id="+nns_id,function(result){					
					alert(result);window.location.reload();					
				},'json');
			}			
			function rsync_info(nns_id){				
				$.get("nncms_content_task.php?sp_id=<?php echo $sp_id?>&ajax=rsync&nns_id="+nns_id,function(result){					
					alert(result);window.location.reload();
				},'json');
			}			
			$(document).ready(function() {
				window.parent.now_frame_url = window.location.href;
				$('#clear_time1').click(function(){
					$('#day_picker_start').val('');
					$('#day_picker_end').val('');			
				});
				$('#clear_time2').click(function(){
					$('#day_picker_put_start').val('');
					$('#day_picker_put_end').val('');			
				});
				$('#clear_time3').click(function(){
					$('#start_time').val('');
					$('#end_time').val('');			
				});

				$('.edit_priority').click(function(){
					var click_td = $(this);
					var tr = $(this).parent('tr').first();
					//本条任务的id
					var val_id = tr.find('input').val();
					//本条任务的权重(去掉空格)
					var priority_old = $(this).text().trim();
					//在td中加入input表单，并把权重值填入
					$(this).html('<input style="width:50px" type="text" value="'+ priority_old +'"/>');
					var this_td_input = $(this).find('input');
					this_td_input.focus().select().blur(function(){
						var priority_new = $(this).val();
						priority_new = parseFloat(priority_new);
						if(isNaN(priority_new))
						{
							alert('请输入数字');
							this.focus();
						}
						else
						{
							click_td.text(priority_new);
							window.location.href="nncms_content_clip_task.php?sp_id=<?php echo $sp_id;?>&op=sort&d=set&nns_priority="+priority_new+"&task_id="+val_id;
						}
					});
				});

			});			
			function restart_more(force){
				force = force?1:0;
				var id=getAllCheckBoxSelect();
				if (id==""){
					alert("请选择至少选择一项");
				}else{
					var r=confirm("是否要执行任务？");
					if(r == true){
                        window.location.href="nncms_content_clip_task.php?sp_id=<?php echo $sp_id;?>&op=restart&force="+force+"&task_id="+id+"<?php echo $nns_url . $search_url; ?>";
					}
                }				
				return;
			}
			function delete_movie_more(){
				var id=getAllCheckBoxSelect();
				if (id==""){
					alert("请选择至少选择一项");
				}else{
					var r=confirm("是否要执行删除操作？");
					if(r == true){
                        window.location.href="nncms_content_clip_task.php?sp_id=<?php echo $sp_id;?>&op=delete_movie&task_id="+id+"<?php echo $nns_url . $search_url; ?>";
					}
                }				
				return;				
			}
			
			function export_data() {
				window.location.href='<?php echo $refresh; ?>&export=1&view_list_max_num=<?php echo $vod_total_num;?>'; 
			}

			function set_weight()
			{
				if(confirm("确认一键设置权重?")) 
				{
					var str=""; 
					 $('input[name="input"]:checked').each(function(){    
						 str+=$(this).val()+",";
					});
					if(str.length <=0)
					{
						alert('没有一条数据被选中');exit;
					}
					location.href = "./nncms_content_clip_task.php?str_weight_ids="+str+
					"&op=set_weight&nns_task_name=<?php echo $_GET['nns_task_name']; ?>&nns_id=<?php echo $_GET['nns_task_name']; ?>"+
					"&nns_state=<?php echo $_GET['nns_state']; ?>&nns_file_hit=<?php echo $_GET['nns_file_hit']; ?>"+
					"&day_picker_put_start=<?php echo $_GET['day_picker_put_start']; ?>&day_picker_put_end=<?php echo $_GET['day_picker_put_end']; ?>"+
					"&start_time=<?php echo $_GET['start_time']; ?>&end_time=<?php echo $_GET['end_time']; ?>"+
					"&sp_id=<?php echo $_GET['sp_id']; ?>&action=search"+
					"&day_picker_start=<?php echo $_GET['day_picker_start']; ?>&day_picker_end=<?php echo $_GET['day_picker_end']; ?>";
				}
			}

			function sort(type){
				type = type == 'top' ? 'top' : 'bottom';
				var r=confirm("是否进行该操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					var url = "nncms_content_clip_task.php?sp_id=<?php echo $sp_id?>&op=sort&d="+type+"&task_id="+ids+"<?php echo $nns_url . $search_url;?>";
						window.location.href = url;
				}
			}

			function file_encode()
			{
				var r=confirm("是否进行转码操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					var url = "nncms_content_clip_task.php?sp_id=<?php echo $sp_id?>&op=file_encode&task_id="+ids+"<?php echo $nns_url . $search_url;?>";
						window.location.href = url;
				}
			}

			function cancel_weight()
			{
				if(confirm("确认一键取消权重?")) 
				{
					var str=""; 
					 $('input[name="input"]:checked').each(function(){    
						 str+=$(this).val()+",";
					});
					if(str.length <=0)
					{
						alert('没有一条数据被选中');exit;
					}
					location.href = "./nncms_content_clip_task.php?str_weight_ids="+str+
					"&op=cancel_weight&nns_task_name=<?php echo $_GET['nns_task_name']; ?>&nns_id=<?php echo $_GET['nns_task_name']; ?>"+
					"&nns_state=<?php echo $_GET['nns_state']; ?>&nns_file_hit=<?php echo $_GET['nns_file_hit']; ?>"+
					"&day_picker_put_start=<?php echo $_GET['day_picker_put_start']; ?>&day_picker_put_end=<?php echo $_GET['day_picker_put_end']; ?>"+
					"&start_time=<?php echo $_GET['start_time']; ?>&end_time=<?php echo $_GET['end_time']; ?>"+
					"&sp_id=<?php echo $_GET['sp_id']; ?>&action=search"+
					"&day_picker_start=<?php echo $_GET['day_picker_start']; ?>&day_picker_end=<?php echo $_GET['day_picker_end']; ?>";
				}
			}
		</script>
	</head>

	<body>
		<div class="content">
			<div class="content_position">
				切片管理 > 切片列表
			</div>
				<div class="content_table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<form action="" method="get">
							<tbody>
								<tr>
									<td>
										影片名：&nbsp;&nbsp;<input type="text" name="nns_task_name" id="nns_task_name" value="<?php echo isset($_GET['nns_task_name'])?$_GET['nns_task_name']:'';?>" style="width:200px;"/>
										&nbsp;&nbsp;&nbsp;&nbsp;ID：&nbsp;&nbsp;<input type="text" name="nns_id" id="nns_id" value="<?php echo isset($_GET['nns_id'])?$_GET['nns_id']:'';?>" style="width:200px;"/>
										&nbsp;&nbsp;&nbsp;&nbsp;
										状态：&nbsp;&nbsp;<select name="nns_state" style="width: 150px;">
											<option value="">全部</option>
											<option value="wait" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='wait') echo 'selected="selected"';?>>等待下发</option>
											<option value="handle" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='handle') echo 'selected="selected"';?>>已经下发</option>
											<option value="get_media_url_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='get_media_url_fail') echo 'selected="selected"';?>>获取下载地址失败</option>
											<option value="download_succ" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='download_succ') echo 'selected="selected"';?>>下载成功</option>
											<option value="download" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='download') echo 'selected="selected"';?>>下载...</option>
											<option value="download_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='download_fail') echo 'selected="selected"';?>>下载失败</option>
											<option value="cutting" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='cutting') echo 'selected="selected"';?>>正在切片</option>
											<option value="clip_succ" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='clip_succ') echo 'selected="selected"';?>>切片成功</option>
											<option value="clip_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='clip_fail') echo 'selected="selected"';?>>切片失败</option>
											<option value="ok" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='ok') echo 'selected="selected"';?>>上报成功</option>
											<option value="c2_handle" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='c2_handle') echo 'selected="selected"';?>>注入执行中</option>
											<option value="c2_ok" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='c2_ok') echo 'selected="selected"';?>>注入成功</option>
										    <option value="c2_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='c2_fail') echo 'selected="selected"';?>>注入失败</option>
											<option value="drm_ok" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='drm_ok') echo 'selected="selected"';?>>注入DRM成功</option>
											<option value="drm_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='drm_fail') echo 'selected="selected"';?>>注入DRM失败</option>
										    <option value="get_drm_key_succ" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='get_drm_key_succ') echo 'selected="selected"';?>>获取DRM密钥成功</option>
										    <option value="get_drm_key_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='get_drm_key_fail') echo 'selected="selected"';?>>获取DRM密钥失败</option>
										    <option value="c2_delete_handle" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='c2_delete_handle') echo 'selected="selected"';?>>等待删除已注入片源</option>
										    <option value="drm_encrypt_ok" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='drm_encrypt_ok') echo 'selected="selected"';?>>DRM加密成功</option>
										    <option value="drm_encrypt_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='drm_encrypt_fail') echo 'selected="selected"';?>>DRM加密失败</option>
										    <option value="drm_encrypt_loading" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='drm_encrypt_loading') echo 'selected="selected"';?>>正在DRM加密</option>

										    <option value="clip_encode_wait" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='clip_encode_wait') echo 'selected="selected"';?>>等待转码</option>
										    <option value="clip_encode_loading" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='clip_encode_loading') echo 'selected="selected"';?>>正在转码</option>
										    <option value="clip_encode_ok" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='clip_encode_ok') echo 'selected="selected"';?>>转码成功</option>
										    <option value="clip_encode_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='clip_encode_fail') echo 'selected="selected"';?>>转码失败</option>
										    
										    <option value="live_to_media_wait" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='live_to_media_wait') echo 'selected="selected"';?>>等待直播转点播</option>
										    <option value="live_to_media_loading" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='live_to_media_loading') echo 'selected="selected"';?>>正在直播转点播</option>
										    <option value="live_to_media_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='live_to_media_fail') echo 'selected="selected"';?>>直播转点播失败</option>

                                            <option value="uploading" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='uploading') echo 'selected="selected"';?>>上传中</option>
                                            <option value="upload_suc" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='upload_suc') echo 'selected="selected"';?>>上传成功</option>
                                            <option value="upload_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='upload_fail') echo 'selected="selected"';?>>上传失败</option>

                                        </select>
										&nbsp;&nbsp;&nbsp;&nbsp;
										命中状态：&nbsp;&nbsp;<select name="nns_file_hit" style="width: 150px;">
											<option value="">全部</option>
											<option value="local" <?php if(isset($_GET['nns_file_hit']) && $_GET['nns_file_hit']=='local') echo 'selected="selected"';?>>本地命中</option>
											<option value="net" <?php if(isset($_GET['nns_file_hit']) && $_GET['nns_file_hit']=='net') echo 'selected="selected"';?>>公网下载</option>
											</select>
									    &nbsp;&nbsp;&nbsp;&nbsp;
										文件类型：&nbsp;&nbsp;<select name="nns_video_type" style="width: 150px;">
											<option value="">全部</option>
											<option value="0" <?php if(isset($_GET['nns_video_type']) && $_GET['nns_video_type']=='0') echo 'selected="selected"';?>>点播</option>
											<option value="1" <?php if(isset($_GET['nns_video_type']) && $_GET['nns_video_type']=='1') echo 'selected="selected"';?>>直播</option>
											<option value="2" <?php if(isset($_GET['nns_video_type']) && $_GET['nns_video_type']=='2') echo 'selected="selected"';?>>文件包</option>
											<option value="3" <?php if(isset($_GET['nns_video_type']) && $_GET['nns_video_type']=='3') echo 'selected="selected"';?>>EPG模板文件</option>
											</select>
									</td>
									</tr>
									
								
								<tr>
									<td>
									上报时间：&nbsp;&nbsp;&nbsp;<input name="day_picker_put_start" id="day_picker_put_start" type="text"  value="<?php echo isset($_GET['day_picker_put_start']) ? $_GET['day_picker_put_start'] : "";?>" style="width:120px;" class="datetimepicker" callback="test" />
									 - <input name="day_picker_put_end" id="day_picker_put_end" type="text"  value="<?php echo isset($_GET['day_picker_put_end']) ? $_GET['day_picker_put_end'] : "";?>" style="width:120px;" class="datetimepicker" callback="test" />
									<input type="button" id="clear_time2" name="clear_time2" value="清除时间"/>&nbsp;&nbsp;	
										&nbsp;&nbsp;开始结束时间：&nbsp;&nbsp;<input name="start_time" id="start_time" type="text"  value="<?php echo isset($_GET['start_time']) ? $_GET['start_time'] : "";?>" style="width:120px;" class="datetimepicker" callback="test" />
										 - <input name="end_time" id="end_time" type="text"  value="<?php echo isset($_GET['end_time']) ? $_GET['end_time'] : "";?>" style="width:120px;" class="datetimepicker" callback="test" />
										<input type="hidden" name="sp_id" value="<?php echo $sp_id;?>" />
										<input type="hidden" name="action" value="search" />	
										&nbsp;&nbsp;&nbsp;<input type="button" id="clear_time3" name="clear_time3" value="清除时间"/>&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;CP ID：&nbsp;&nbsp;&nbsp;&nbsp;<select name="nns_cp_id" style="width: 150px;">
                                    		<option value="" >全部</option>
                                    	<?php 
                                    		foreach ($result_cp_id_data as $cp_id_key=>$cp_id_val)
											{
												?>
												<option value="<?php echo $cp_id_key; ?>" <?php if(isset($_GET['nns_cp_id']) && $_GET['nns_cp_id']==$cp_id_key) echo 'selected="selected"'?>><?php echo $cp_id_val; ?></option>
												<?php 
											}
											echo "</select>";
                                    	?>
                                            切片服务器ID：<input type="" name="nns_tom3u8_id" value="" style="width: 150px;"/>
									</td>
								</tr>
								<tr>
										<td>
										创建时间：&nbsp;&nbsp;&nbsp;<input name="day_picker_start" id="day_picker_start" type="text"  value="<?php echo isset($_GET['day_picker_start']) ? $_GET['day_picker_start'] : "";?>" style="width:120px;" class="datetimepicker" callback="test" /> - 
										<input name="day_picker_end" id="day_picker_end" type="text"  value="<?php echo isset($_GET['day_picker_end']) ? $_GET['day_picker_end'] : "";?>" style="width:120px;" class="datetimepicker" callback="test" />
										<input type="button" id="clear_time1" name="clear_time1" value="清除时间"/>&nbsp;&nbsp;
										<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  onclick="refresh_vod_page();"/>	
										&nbsp;&nbsp;&nbsp;&nbsp;	
										<input type="button" value="导出CSV格式"  onclick="export_data();" />	
									</td>
								</tr>
							</tbody>
							</form>
						</table>
				</div>			
			<div class="content_table formtable">

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th><input name="" type="checkbox" value="">序号</th>							
							<th>影片名称</th>					
							<th>类型</th>
							<th>状态</th>							
							<th>创建时间</th>
							<th>开始时间</th>
							<th>结束时间</th>
							<th>上报时间</th>
							<th>权重</th>
							<th>CP ID</th>
                            <th>切片服务器id</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$flag = false;
						if($data!=null){
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) {
								$num++;
						?>
						<tr>
						<td><input name="input" type="checkbox" value="<?php echo $item["nns_id"];?>" /><?php echo $num; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $item['nns_id'];?></td>   
					                     
                        <td> 
                        	
                        	
                        	<?php
                        	
                        	switch ($item['nns_file_hit']) {
								case 'local':
									echo '<font color=green>[本地命中]</font>';
									break;
								
								case 'net':
									echo '<font color=red>[公网下载]</font>';
									break;
							}
                        	?>
                        	
                        	
                        	<a href="nncms_content_clip_task_detail.php?sp_id=<?php echo $_GET['sp_id'];?>&task_id=<?php echo $item['nns_id'];?>">                        
                        <?php  echo $item['nns_task_name'];?>    </a>                                             
                        </td>
                        <td>                        
                        <?php  if($item['nns_video_type'] == '1'){ echo "直播";}
                        else if($item['nns_video_type'] == '2'){ echo "文件包";}
                        else if($item['nns_video_type'] == '3'){ echo "EPG模板文件";}
                        else{ echo "点播";};?>
                        </td>                     
						<td><?php
							
							if(empty($item['nns_state'])){
								$flag = true;
								echo '未执行';
							}else{
								if(isset( clip_task_model::$task_state_arr[$item['nns_state']])){
									echo clip_task_model::$task_state_arr[$item['nns_state']];
								}else{
									echo $item['nns_state'];
								}
							}

							
						?></td>
						
                        <td>                        
                        <?php echo $item['nns_create_time'];?>                                            
                        </td>
                        <td>                        
                        <?php echo $item['nns_start_time'];?>                                        
                        </td> 
                        <td>                        
                        <?php echo $item['nns_end_time'];?>                                        
                        </td>                                               
                        <td>                        
                        <?php echo $item['nns_alive_time'];?>                                            
                        </td>
                        <td class="edit_priority">
                        <?php echo $item['nns_priority'];?>                                            
                        </td>
                        <td>
					    <?php if(in_array($item['nns_cp_id'], $result_cp_data_keys))
					    {
					        echo isset($result_cp_id_data[$item['nns_cp_id']]) ? $result_cp_id_data[$item['nns_cp_id']] : '';
					    }
					    else
					    {
					        echo '<font color="#FF0000">非法</font>';
					    }?>
					    </td>
                         <td>
                            <?php echo $item['nns_tom3u8_id'];?>
                         </td>
						<td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
						<?php if(isset($sp_config['clip_file_encode_enable']) && $sp_config['clip_file_encode_enable'] == 1){?>
						      <?php if($item['nns_state'] == 'clip_encode_fail' || $item['nns_state'] == 'clip_encode_ok' || $item['nns_state'] == 'clip_encode_loading'){?>
						          <a href="?sp_id=<?php echo $sp_id;?>&op=res_encode&task_id=<?php echo $item['nns_id']?><?php echo $nns_url . $search_url; ?>">重发转码任务</a>
						          <!-- 
						          <a href="?sp_id=<?php echo $sp_id;?>&op=res_del_encode&task_id=<?php echo $item['nns_id']?><?php echo $nns_url . $search_url; ?>">删除转码任务</a>
						           -->
						      <?php }?>
						<?php }?>
						<?php if(isset($sp_config['disabled_clip']) && $sp_config['disabled_clip'] != 3){?>
						<a href="?sp_id=<?php echo $sp_id;?>&op=restart&force=0&task_id=<?php echo $item['nns_id']?><?php echo $nns_url . $search_url; ?>">重发任务</a>
						<?php }?>
						<!--<a href="?sp_id=<?php echo $sp_id;?>&op=restart&force=1&task_id=<?php echo $item['nns_id']?><?php echo $nns_url . $search_url; ?>">强制重切</a>-->
						<?php if($item['nns_state']){?>
						<a target="_blank" href="./nncms_content_clip_task_log_xml.php?sp_id=<?php echo $sp_id;?>&task_id=<?php echo $item['nns_id']?>">查看内容</a>
						<?php }?>
						<?php if(isset($sp_config['disabled_clip']) && $sp_config['disabled_clip'] != 3){?>
						<a href="?sp_id=<?php echo $sp_id;?>&op=sort&d=top&task_id=<?php echo $item['nns_id']?><?php echo $nns_url . $search_url; ?>">置顶</a>
						<a href="?sp_id=<?php echo $sp_id;?>&op=sort&d=up&task_id=<?php echo $item['nns_id']?>&nns_priority=<?php echo $item['nns_priority'];?><?php echo $nns_url . $search_url; ?>">向上</a>
						<a href="?sp_id=<?php echo $sp_id;?>&op=sort&d=down&task_id=<?php echo $item['nns_id']?>&nns_priority=<?php echo $item['nns_priority'];?><?php echo $nns_url . $search_url; ?>">向下</a>
						<a href="?sp_id=<?php echo $sp_id;?>&op=sort&d=bottom&task_id=<?php echo $item['nns_id']?><?php echo $nns_url . $search_url; ?>">置底</a>
						<?php }?>
						</td>
                        </tr>
						<?php
							}
						}
						$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) {
						?>
						<tr>
                            <td>&nbsp; </td>
                            <td>&nbsp; </td>
                            <td>&nbsp; </td>
                            <td>&nbsp; </td>                                                                
                            <td>&nbsp; </td>
                            <td>&nbsp; </td>
                            <td>&nbsp; </td><td>&nbsp; </td>
                            <td>&nbsp; </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="pagecontrol">
			共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num;?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_clip_task.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_clip_task.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_content_clip_task.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_clip_task.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_clip_task.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

                        </div>
                        
		     <div class="controlbtns">
		    	<div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
		        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
		        <div class="controlbtn delete"><a href="javascript:delete_movie_more();" >删除片源</a></div>
		        <?php if(isset($sp_config['disabled_clip']) && $sp_config['disabled_clip'] != 3){?>
		        <div class="controlbtn add"><a href="javascript:restart_more(0);" >重发任务</a></div>
		        <!--<div class="controlbtn delete"><a href="javascript:restart_more(1);" >强制重切</a></div>-->
		        <div class="controlbtn move"><a href="javascript:sort('top');">批量置顶</a></div>
                <div class="controlbtn move"><a href="javascript:sort('button');">批量置底</a></div>
                <?php if($config_file_encode_flag){?>
                <div class="controlbtn move"><a href="javascript:file_encode();">转码</a></div>
		        <?php }?>
		        <?php 
		        	if($flag)
					{
				?>
						<div class="controlbtn audit"><a href="javascript:set_weight();" >设置权重</a></div>
						<div class="controlbtn cancel_audit"><a href="javascript:cancel_weight();" >取消权重</a></div>
				<?php 
					}
		        ?>
		        <?php }?>
		        <div style="clear:both;"></div>
		    </div>                          
		</div>
	</body>
</html>
