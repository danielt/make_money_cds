<?php header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135206");
$checkpri = null;



$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$group_name = $_GET["group_name"];
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}


// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';




if(isset($_GET['op'])){
	$action =trim($_GET['op']);
	switch($action){
        case 'delete_movie':
        $task_ids = explode(",",$_GET['task_id']);
        $result = clip_task_model::delete_movie_by_task_id($task_ids,true);
        if($result){
        	echo '<script>alert("删除执行成功");</script>';
        }else{
        	echo '<script>alert("删除执行失败");</script>';
        }        
        break;
        
	}
	unset($_GET['op']);
	unset($_GET['force']);
	unset($_GET['task_id']);
	unset($_GET['d']);
}




$sp_id = $_GET['sp_id'];
$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : (!empty($_COOKIE["page_max_num"]) ?  $_COOKIE["page_max_num"] : 18);
$page_num = isset($_GET['page'])?$_GET['page']:1;
if($page_num<1){
	$page_num = 1;
}

$offset = intval($page_num-1)*$page_size;
$limit = $page_size;
$filter = null;
if($_GET['action']=='search'){
	$filter['nns_task_name'] = trim($_GET['nns_task_name']);
	$filter['nns_id'] = $_GET['nns_id'];
}
$result =  clip_task_model::get_delete_task($sp_id,$filter,$offset,$limit);



//print_r($result);

$vod_total_num = $result['rows'];//记录条数

$vod_pages = ceil($vod_total_num / $page_size);//总页数
if($page_num>$vod_pages){
	$page_num = $vod_pages;
}


$currentpage = $page_num;
$g_manager_list_max_num = $page_size;
unset($_GET['page']);
$nns_url = "&".http_build_query($_GET);//"&view_list_max_num=$page_size&sp_id=$sp_id";
$refresh = "?".http_build_query($_GET);//"?page=$page_num&sp_id=$sp_id&view_list_max_num=";


$data = $result['data'];



?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript">
			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
                setCookie("page_max_num",num);
				window.location.href='<?php echo $refresh;?>&view_list_max_num='+num
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				})
				return BoxKey;
			}
			
						
			$(document).ready(function() {
				window.parent.now_frame_url = window.location.href;
				$('#clear_time1').click(function(){
					$('#day_picker_start').val('');
					$('#day_picker_end').val('');			
				});
				$('#clear_time2').click(function(){
					$('#day_picker_put_start').val('');
					$('#day_picker_put_end').val('');			
				});
				$('#clear_time3').click(function(){
					$('#start_time').val('');
					$('#end_time').val('');			
				});								
			});			
			
			function delete_movie_more(){
				var id=getAllCheckBoxSelect();
				if (id==""){
					alert("请选择至少选择一项");
				}else{
					var r=confirm("是否要执行删除操作？");
					if(r == true){
                        window.location.href="nncms_content_clip_amend.php?sp_id=<?php echo $sp_id;?>&op=delete_movie&task_id="+id+"<?php echo $nns_url . $search_url; ?>";
					}
                }
				return;
			}
			
			

		</script>
	</head>

	<body>
		<div class="content">
			<div class="content_position">
				切片管理 > 切片数据修正
			</div>
				<div class="content_table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<form action="" method="get">
							<tbody>
								<tr>
									<td>
										影片名：&nbsp;&nbsp;<input type="text" name="nns_task_name" id="nns_task_name" value="<?php echo isset($_GET['nns_task_name'])?$_GET['nns_task_name']:'';?>" style="width:200px;"/>
										&nbsp;&nbsp;&nbsp;&nbsp;ID：&nbsp;&nbsp;<input type="text" name="nns_id" id="nns_id" value="<?php echo isset($_GET['nns_id'])?$_GET['nns_id']:'';?>" style="width:200px;"/>
											<input type="hidden" name="sp_id" value="<?php echo $sp_id;?>" />
										<input type="hidden" name="action" value="search" />
										<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  onclick="refresh_vod_page();"/>	
										&nbsp;&nbsp;&nbsp;&nbsp;	
									</td>
								</tr>
							</tbody>
							</form>
						</table>
				</div>			
			<div class="content_table formtable">

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th><input name="" type="checkbox" value="">序号</th>							
							<th>影片名称</th>	
							<th>状态</th>							
							<th>创建时间</th>
							<th>开始时间</th>
							<th>上报时间</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if($data!=null){
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) {
								$num++;
						?>
						<tr>
						<td><input name="input" type="checkbox" value="<?php echo $item["nns_id"];?>" /><?php echo $num; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $item['nns_id'];?></td>   
					                     
                        <td> 
                        	
                        	
                        	<?php
                        	
                        	switch ($item['nns_file_hit']) {
								case 'local':
									echo '<font color=green>[本地命中]</font>';
									break;
								
								case 'net':
									echo '<font color=red>[公网下载]</font>';
									break;
							}
                        	?>
                        	
                        	
                        	<a href="nncms_content_clip_task_detail.php?sp_id=<?php echo $_GET['sp_id'];?>&task_id=<?php echo $item['nns_id'];?>">                        
                        <?php  echo $item['nns_task_name'];?>    </a>                                             
                        </td>                        
						<td><?php 
							if(empty($item['nns_state'])){
								echo '未执行';
							}else{
								if(isset( clip_task_model::$task_state_arr[$item['nns_state']])){
									echo clip_task_model::$task_state_arr[$item['nns_state']];
								}else{
									echo $item['nns_state'];
								}
							}

							
						?></td>
						
                        <td>                        
                        <?php echo $item['nns_create_time'];?>                                            
                        </td>
                        <td>                        
                        <?php echo $item['nns_start_time'];?>                                        
                        </td>                                              
                        <td>                        
                        <?php echo $item['nns_alive_time'];?>                                            
                        </td>
                        </tr>
						<?php
							}
						}
						$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) {
						?>
						<tr>
                            <td>&nbsp; </td>
                            <td>&nbsp; </td>                                                                
                            <td>&nbsp; </td>
                            <td>&nbsp; </td>
                            <td>&nbsp; </td>
                            <td>&nbsp;</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="pagecontrol">
			共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num;?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_clip_amend.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_clip_amend.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_content_clip_amend.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_clip_amend.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_clip_amend.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

                        </div>
                        
		     <div class="controlbtns">
		    	<div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
		        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
		        <div class="controlbtn delete"><a href="javascript:delete_movie_more();" >删除片源</a></div>
		        <div style="clear:both;"></div>
		    </div>                          
		</div>
	</body>
</html>
