<?php 
header("Content-Type:text/xml;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) 
{
	$language_dir = $_SESSION["language_dir"];
}
include_once $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
include_once $nncms_config_path . "nn_logic/notify_message/notify_message.class.php";
$dc = nl_get_dc(array(
		'db_policy' => NL_DB_READ | NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$dc->open();
$params['nns_id'] = $_GET['id'];

$notify_message = nl_notify_message::get_nofity_message($dc,$params);
$message_info = $notify_message['data_info'][0];

echo $message_info['nns_notify'];