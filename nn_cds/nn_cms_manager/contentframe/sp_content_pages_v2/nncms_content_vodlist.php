<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "135205");
$checkpri = null;


$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$group_name = $_GET["group_name"];
if (!$pri_bool) {
        Header("Location: ../nncms_content_wrong.php");
        exit;
}

// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";


include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';

//include_once dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv/models/category_model.php';

if($_GET['c2_task']=='ajax'){
	$vod_id = $_GET['vod_id'];
	$type = $_GET['type'];
	$cmd = $_GET['cmd'];
	$sp_id = $_GET['sp_id'];
	//$cid = $_GET['cid'];
	
	$re = category_content_model::exe_cmd($vod_id,$type,$cmd,$sp_id);
	echo json_encode($re);exit;
	
}


$base_id = 10000;
if (!empty($_COOKIE["page_max_num"])) {
        $g_manager_list_max_num = $_COOKIE["page_max_num"];
}

include_once($nncms_config_path.'nn_cms_manager/base.inc.php');
$category_id = $_GET['category_id'];
$sp_id = $_GET['sp_id'];
$page_size = $_GET["view_list_max_num"];
$page_num = isset($_GET['page'])?$_GET['page']:1;
$search = $_GET['search_py'];
$day_picker_start = isset($_GET['day_picker_start']) ? $_GET['day_picker_start'] : null;
$day_picker_end = isset($_GET['day_picker_end']) ? $_GET['day_picker_end'] : null;
if($page_num<1){
	$page_num = 1;
}



if($_GET['ajax']=='count'){
	$filter = array(
		'sp_id'=>$sp_id,
		'category_id'=>$category_id,
		'search'=>$search,
		'day_picker_start'=>$day_picker_start,
		'day_picker_end'=>$day_picker_end,
	);	
	$re = sp_content_model::get_category_count($filter);
	echo json_encode($re);
	unset($re,$filter);exit;
}elseif($_GET['ajax']=='explode_vod'){
	$filter = array(
		'sp_id'=>$sp_id,
		'category_id'=>$category_id,
		'search'=>$search,
		'day_picker_start'=>$day_picker_start,
		'day_picker_end'=>$day_picker_end,
	);	
	
	//die;
	$re = sp_content_model::explode_vod($filter);
	echo json_encode($re);
	unset($re,$filter);exit;
}elseif($_GET['ajax']=='copy'){	
	$params = array(
		'sp_id'=>$_GET['sp_id'],
		'nns_id'=>$_GET['nns_id'],
		'category_id'=>$_GET['category_id'],
		'category_name'=>$_GET['category_name'],
	);
	$count_vod = explode(',', $_GET['nns_id']);
	if(count($count_vod)==1){
		$re = sp_content_model::move_vod($params);
	}elseif(count($count_vod)>1){	
		$re = sp_content_model::move_vod_arr($params);
	}
	echo json_encode($re);
	unset($re);exit;
}
$filter = array(
		'sp_id'=>$sp_id,
		'category_id'=>$category_id,
		'search'=>$search,
		'day_picker_start'=>$day_picker_start,
		'day_picker_end'=>$day_picker_end,
	);
$vod_total_num = sp_content_model::get_category_count($filter);//记录条数
unset($filter);
$vod_pages = ceil($vod_total_num / $page_size);//总页数
if($page_num>$vod_pages){
	$page_num = $vod_pages;
}
$offset = intval($page_num-1)*$page_size;
$limit = $page_size;
$filter = array(
		'sp_id'=>$sp_id,
		'category_id'=>$category_id,
		'search'=>$search,
		'day_picker_start'=>$day_picker_start,
		'day_picker_end'=>$day_picker_end,
		'offset'=>$offset,
		'limit'=>$limit,
	);
$data = sp_content_model::get_vodlist($filter);
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;
//$nns_url = "&category_id=$category_id&sp_id=$sp_id&view_list_max_num=$page_size";
//$search_url="&search_py=$search";

unset($_GET['page']);
$nns_url = "&".http_build_query($_GET);//"&view_list_max_num=$page_size&sp_id=$sp_id";
$refresh = "?".http_build_query($_GET);//"?page=$page_num&sp_id=$sp_id&view_list_max_num=";



?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
		<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>	
		<script type="text/javascript" src="../../js/dtree.js"></script>
		<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
		<script language="javascript">
		var nns_id;
			$(document).ready(function() {
				//category.count('<?php echo $_GET['sp_id']; ?>');
				$(".selectbox").hide();
			});
			
			function close_select() {
	$(".selectbox").hide();
}
		
			function refresh_vod_page(){
				
				var num = $("#nns_list_max_num").val();
				window.parent.refresh_vod_page_num(num);
			}
			function checkhiddenInput() {

				var par = getAllCheckBoxSelect("online");
				$("#action").attr("value", "delete");
				if (par == "") {
					alert("请选择一部未上线电影！");
				} else {
					$('#nns_id').attr('value', getAllCheckBoxSelect("online"));
					checkForm('点播管理', '是否进行本次删除？', $('#delete_form'));
				}

			}

			function get_tags_edit(tags) {
				$("#action").attr("value", "ctag");
				$("#nns_tags").attr("value", tags);
				var par = getAllCheckBoxSelect();
				if (par == "") {
					alert("请选择一部未上线电影！");
				} else {
					$('#nns_id').attr('value', par);
					checkForm('点播管理', '是否批量修改EPG标识', $('#delete_form'));
				}
			}

			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {

						BoxKey = true;
						return false;
					}

				})
				return BoxKey;
			}

			function checkhiddenInput_audit(bool) {
				if (bool) {
					var checkType = 1;
				} else {
					var checkType = 0;
				}
				var ifBoxKey = checkhiddenBox(checkType);
				//验证是否已经通过审核
				if (ifBoxKey && checkType) {
					alert('选择的内容中已经存在已审核内容');
					return false;
				} else if (ifBoxKey && !checkType) {

					alert('选择的内容中已经存在待审核内容');
					return false;
				} else {

				}

				var par = getAllCheckBoxSelect("online");
				$("#action").attr("value", "audit");
				if (bool) {
					$("#audit").attr("value", 1);
				} else {
					$("#audit").attr("value", 0);
				}
				if (par == "") {
					alert("请选择一部未上线电影！");
				} else {
					$('#nns_id').attr('value', getAllCheckBoxSelect("online"));
					checkForm('点播管理', '是否进行本次审核？', $('#delete_form'));
				}

			}

			function reset_vod_delete() {
				var par = getAllCheckBoxSelect("online");
				$("#action").attr("value", "reset");
				if (par == "") {
					alert("请选择一部未上线电影！");
				} else {
					$('#nns_id').attr('value', getAllCheckBoxSelect("online"));
					checkForm('点播管理', '是否进行本次恢复？', $('#delete_form'));
				}
			}

			function vod_real_delete() {
				var par = getAllCheckBoxSelect("online");
				$("#action").attr("value", "realdelete");
				if (par == "") {

					alert("请选择一部节目！");

				} else {
					$('#nns_id').attr('value', getAllCheckBoxSelect("online"));
					checkForm('zbgl:key_not find', '是否进行本次删除？', $('#delete_form'));
				}
			}

			function set_move_category_id(value, depot_id, category_name) {
				if(value=='10000'){
					alert("请选择子栏目！");
				}else{
					$.get("nncms_content_vodlist.php?ajax=copy&sp_id=<?php echo $sp_id; ?>&nns_id="+nns_id+"&category_id="+value+"&category_name="+category_name,function(data){
						if(data=='1'){
							alert('成功');
						}else{
							alert('失败');
						}
						window.location.reload();
					},'json');
				}
				close_select();
				
			}

			function set_vod_unline(value) {
				if (confirm("是否强行下线该影片！")) {
					$("#delete_form").find("#nns_id").val(value);
					$("#delete_form").find("#action").val("unline");
					checkForm('点播管理', '是否进行本次修改？', $('#delete_form'));
				}
			}

			function begin_select_category() {
				var par = getAllCheckBoxSelect();
				if (par == "") {

					alert("请先选择一条数据！");
				} else {
					window.parent.begin_select_category();
				}
			}

			function export_csv_file() {
				ids = getAllCheckBoxSelect();

				window.parent.export_csv_file(ids);
			}
			
			
			function task_c2(id,type,cmd,sp){
				$.get("nncms_content_vodlist.php?c2_task=ajax&vod_id="+id+"&type="+type+"&cmd="+cmd+"&sp_id="+sp, function(result) {
					alert(result);window.location.reload();
				}, 'json');
				
			}
			
			function task_copy(id) {
                                $("#select_frame").attr("src", "../../controls/video_category_select/nncms_controls_fjyd_category_select.php?method=vod&sp_id=<?php echo $sp_id; ?>");
                                $(".selectbox").show();
                                nns_id = id;
                        }
			
			function delete_vod(id) {
            	var r=confirm("是否删除");
            	if(r == true){
            	$.get("nncms_content_vodlist.php?ajax=delete_vod&sp_id=<?php echo $sp_id; ?>&nns_id="+id,function(data){
					if(data=='1'){
						alert('操作成功');
					}else{
						alert('操作失败');
					}
					window.location.reload();
				},'json');
				}
            }
			$(document).ready(function() {
				$('#clear_time').click(function(){
					$('#day_picker_start').val('');
					$('#day_picker_end').val('');
			
				});	
				window.parent.now_frame_url = window.location.href;
			});
			
			
			function checkcopy(){
				var par=getAllCheckBoxSelect();
				if (par==""){
					alert("请选择一个影片");
				}else{
				$("#select_frame").attr("src", "../../controls/video_category_select/nncms_controls_fjyd_category_select.php?method=vod&sp_id=<?php echo $sp_id; ?>");
                                $(".selectbox").show();
                                nns_id = par;
                               }
			}
			
			
			function delete_vod_all(){
				var id=getAllCheckBoxSelect();
				if (id==""){
					alert("请选择一个影片");
				}else{
					var r=confirm("是否删除");
					if(r == true){
                	$.get("nncms_content_vodlist.php?ajax=delete_vod_all&sp_id=<?php echo $sp_id; ?>&nns_id="+id,function(data){
					if(data=='1'){
						alert('操作成功');
					}else{
						alert('操作失败');
					}
					window.location.reload();
					},'json');
					}
                }
			}

		</script>
	</head>

	<body>
		<div class="selectbox">
        	<iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
        </div>
		<div class="content">
			<!--<div class="content_position">点播管理 > </div>-->
			<div class="content_table formtable">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>							
							<th><input name="" type="checkbox" value="">序号</th>
							<th>名称</th>
							<th>集数</th>
							<th>创建时间</th>
							<!--<th>状态</th>-->
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if($data!=null){
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) {
								$num++;
						?>
						<tr>
						<td><input name="input" type="checkbox" value="<?php echo $item["nns_id"];?>"><?php echo $num; ?></td>
						<?php
						if($item["nns_video_type"]==0){
						?>
                        <td><a href="../vod_pages/nncms_content_vod_detail.php?nns_id=<?php echo $item['nns_video_id'];?>"><?php echo $item["nns_video_name"] . '&nbsp;&nbsp;' . $item['nns_pinyin'];?></a></td>
                        <?php
						}else{
							/*sp_id=<?php echo $item['nns_org_id'];?>&category_id=<?php echo $item['nns_category_id'];?>*/
						?>	
                        <td><a href="../playbill_pages/nncms_content_playbill_detail_list.php?nns_id=<?php echo $item['nns_video_id'];?>"><?php echo $item["nns_video_name"] . '&nbsp;&nbsp;' . $item['nns_pinyin'];?></a></td>						
						<?php	
						}
                        ?>                    
                        <td><?php if($item["nns_video_type"]==0){ echo $item["nns_all_index"];}else{echo "Live";}?></td>
                        <td><?php echo $item["nns_create_time"];?></td>
                        <!--<td><?php 
                        $nns_id = $item["nns_video_id"];
						if($item["nns_video_type"]==0){
							if($item["nns_all_index"]==1){
								$type = 'vod';
							}elseif($item["nns_all_index"]>1){
								$type = 'index';
							}
						}else{							
							$type = 'live';
						}
						$org_id = $item['nns_org_id'];
						$category_id = $item['nns_category_id'];
						//$video_type = $item['nns_category_id'];
                        //echo category_content_model::get_task_status($nns_id, $type,$org_id,$item["nns_video_type"]);
                        if($item["nns_status"]==0){
                        	echo "等待注入";
                        }else{
                        	echo "<font color='green'>注入成功</font>";
                        }
                        ?></td>-->
                        <td >
                        	<!--<a href="javascript:void()"  onclick="task_c2('<?php echo $nns_id;?>','<?php echo $type;?>','restart','<?php echo $org_id;?>');">重新注入</a>-->
                        	<a href="javascript:void()"  onclick="task_copy('<?php echo $item["nns_id"];?>');">复制到其他栏目</a>
                        	<!--<a href="javascript:void()"  onclick="delete_vod('<?php echo $item["nns_id"];?>');">删除</a>-->
                        </td>
                        </tr>
						<?php
							}
						}$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) {
						?>
						<tr>
                                                                
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <!--<td>&nbsp;</td>-->
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp;</td>


                                                        </tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="pagecontrol">
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_vodlist.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_vodlist.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_content_vodlist.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_vodlist.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_vodlist.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

                        </div>
            <div class="controlbtns">
    	<div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
        <div class="controlbtn move"><a href="javascript:checkcopy();" >复制</a></div>
        <!--<div class="controlbtn delete"><a href="javascript:delete_vod_all();" >删除</a></div>-->
        <div style="clear:both;"></div>
    </div>            			
			
		</div>
	</body>
</html>
