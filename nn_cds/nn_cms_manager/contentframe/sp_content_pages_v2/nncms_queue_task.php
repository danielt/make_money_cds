<?php 
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include $nncms_config_path . "v2/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_core.m_public");
\ns_core\m_load::load("ns_model.center_op.center_op_queue");
\ns_core\m_load::load("ns_data_model.command.m_command_task_log_inout");
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$group_name = $_GET["group_name"];
$sp_id = $_GET['sp_id'];
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}

global $g_bk_version_number;
$bk_version_number = $g_bk_version_number == '1' ? true : false;
unset($g_bk_version_number);
if($bk_version_number && isset($_GET['op']))
{
    if ($_GET['op'] == 'c2_task_start') //注入到c2队列
    {
        \m_config::$flag_operation = false;
        $log_timer_path = \m_config::return_child_path($nncms_config_path . "v2/ns_timer/delivery/delivery_center_op.timer.php", 'ns_timer');

        $ids = ($_GET['op'] == 'c2_task_start') ? explode(',', $_GET['ids']) : array($_GET['c2_task_id']);
        $ids = (is_array($ids) && !empty($ids)) ? array_filter($ids) : $ids;
        if (!is_array($ids) || empty($ids))
        {
            echo "<script>alert('传入查询ID为空');history.go(-1);</script>";
            die;
        }
        //获取中心同步指令队列执行任务
        $result_task = nl_op_queue::query_by_params(m_config::get_dc(), array('nns_id' => $ids, 'nns_org_id' => $sp_id));

        if ($result_task['ret'] != NS_CDS_SUCCE)
        {
            echo "<script>alert('数据库查询数据失败');history.go(-1);</script>";
            die;
        }
        if (empty($result_task['data_info']) || !is_array($result_task['data_info']))
        {
            echo "<script>alert('数据库查询无数据');history.go(-1);</script>";
            die;
        }
        \ns_core\m_load::load("ns_model.delivery.op_queue_delivery_explain");
        $explain_inst = new \ns_model\delivery\op_queue_delivery_explain($sp_id);
        foreach ($result_task['data_info'] as $task)
        {
            //实例化底层数据基础类
            $center_op_queue = new ns_model\center_op\center_op_queue($sp_id, $task['nns_type']);
            //过滤注入数据
            $center_op_explain = $center_op_queue->explain();
            if ($center_op_explain['ret'] != NS_CDS_SUCCE)
            {
                \m_config::timer_write_log($log_timer_path, var_export($center_op_explain['reason'], true), $task['nns_type']);
                continue;
            }
            //根据中心同步指令任务获取注入信息
            $message_arr = $center_op_queue->query_mixed_info(array($task));

            if ($message_arr['ret'] != NS_CDS_SUCCE)
            {
                \m_config::timer_write_log($log_timer_path, var_export($message_arr, true), $task['nns_type']);
                continue;
            }
            if (!isset($message_arr['data_info']) || empty($message_arr['data_info']) || !is_array($message_arr['data_info']))
            {
                \m_config::timer_write_log($log_timer_path, "查询消息队列数据为空，不再执行中心同步指令到c2注入", $task['nns_type']);
                continue;
            }
            m_config::write_op_queue_execute_log("***********开始执行中心同步指令注入到c2队列：".var_export($message_arr['data_info'][0],true), $sp_id, $task['nns_id']);
            $explain_inst->init($message_arr['data_info'][0]);
            $explain_result = $explain_inst->explain();
            $explain_inst->destroy_init();
            $re_w_log = m_config::write_op_queue_execute_log("***********执行中心同步指令结果：".var_export($explain_result,true), $sp_id, $task['nns_id']);
            if ($explain_result['ret'] != 0)
            {
                $command_status = NS_CDS_COMMAND_OP_FAIL;
            }
            else
            {
                $command_status = NS_CDS_COMMAND_C2_WAIT;
            }
            $command_task_log_params = array(
                'base_info' => array(
                    'nns_command_task_id' => $task['nns_command_task_id'],
                    'nns_status' => $command_status,
                    'nns_desc' => $explain_result['reason'],
                    'nns_execute_url' => $re_w_log['data_info']['base_dir'],
                )
            );
            $obj_command_task_log = new m_command_task_log_inout();
            $obj_command_task_log->add($command_task_log_params);
            unset($obj_command_task_log);
            unset($cdn_import_queue);
        }
        if(!isset($explain_result['reason']) || empty($explain_result['reason']))
        {
            $explain_result['reason'] = $explain_result['ret'] != NS_CDS_SUCCE ? '操作失败' : '操作成功';
        }
        echo "<script>alert('" . $explain_result['reason'] . "');history.go(-1);</script>";
        die;
    }
}
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) .'/nn_logic/op_queue/op_queue.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_class/file_encode/make_queue_file_encode.class.php';
$obj_dc= nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));

$result_cp_data = nl_query_by_db("select nns_id,nns_name from nns_cp", $obj_dc->db());
$result_cp_data_keys = $result_cp_id_data = array();
if(is_array($result_cp_data))
{
    foreach ($result_cp_data as $cp_val)
    {
        $result_cp_id_data[$cp_val['nns_id']] = $cp_val['nns_name'];
    }
    $result_cp_data_keys = array_keys($result_cp_id_data);
}



if(isset($_GET['ajax']) && $_GET['ajax']=='op_q_detail'){
	$queue_task_model = new queue_task_model();	
	$result = $queue_task_model->get_q_task_detail($_GET['op_id']);
	echo json_encode($result);
	exit;
}

if($_GET['op']=='queue_end'){	
	$queue_task_model = new queue_task_model();	
	$queue_task_model->update_queue_state($_GET['ids'],false);
	$queue_task_model=null;
	echo '<script>alert("操作成功");history.go(-1);</script>';die;
}elseif($_GET['op']=='queue_start'){
	$queue_task_model = new queue_task_model();	
	$queue_task_model->update_queue_state($_GET['ids'],true);
	$queue_task_model=null;
	echo '<script>alert("操作成功");history.go(-1);</script>';die;
}elseif($_GET['op']=='queue_destroy'){
	$queue_task_model = new queue_task_model();	
	$queue_task_model->delete_queue($_GET['ids']);
	$queue_task_model=null;//die;
	echo '<script>alert("操作成功");history.go(-1);</script>';die;
	
}elseif($_GET['op']=='queue_audit'){
	$queue_task_model = new queue_task_model();
	$queue_task_model->update_queue_audit($_GET['ids']);
	$queue_task_model=null;
	unset($_GET['nns_op_type'],$_GET['op'],$_GET['op_action'],$_GET['nns_op_id']);
	echo '<script>alert("操作成功");</script>';
	
}elseif($_GET['op']=='only'){
	//单体暂停
	//if($_GET['nns_type']=='video'){
		$queue_task_model = new queue_task_model();	
		if($_GET['op_action']=='pause'){			
			$queue_task_model->update_queue_state($_GET['nns_op_id'],false);
			$queue_task_model=null;
			//nns_op_type=video&op=only&op_action=pause&nns_op_id
			unset($_GET['nns_op_type'],$_GET['op'],$_GET['op_action'],$_GET['nns_op_id']);
			echo '<script>alert("操作成功");</script>';
		}elseif($_GET['op_action']=='resume'){
			//$queue_task_model = new queue_task_model();	
			$queue_task_model->update_queue_state($_GET['nns_op_id'],true);
			$queue_task_model=null;
			unset($_GET['nns_op_type'],$_GET['op'],$_GET['op_action'],$_GET['nns_op_id']);
			echo '<script>alert("操作成功");</script>';
		}
	//}
}elseif($_GET['op']=='all'){
	$queue_task_model = new queue_task_model();	
	if($_GET['op_action']=='pause'){			
		$queue_task_model->update_queue_all_state($_GET['nns_op_id'],$_GET['nns_op_type'],false);
		$queue_task_model=null;
		unset($_GET['nns_op_type'],$_GET['op'],$_GET['op_action'],$_GET['nns_op_id']);
		echo '<script>alert("操作成功");</script>';
	}elseif($_GET['op_action']=='resume'){
		//$queue_task_model = new queue_task_model();	
		$queue_task_model->update_queue_all_state($_GET['nns_op_id'],$_GET['nns_op_type'],true);
		$queue_task_model=null;
		unset($_GET['nns_op_type'],$_GET['op'],$_GET['op_action'],$_GET['nns_op_id']);
		echo '<script>alert("操作成功");</script>';
	}
}elseif($_GET['op']=='default'){
	$queue_task_model = new queue_task_model();
	$queue_task_model->update_queue_default_state($_GET['nns_op_id']);
	$queue_task_model=null;
	unset($_GET['nns_op_type'],$_GET['op'],$_GET['op_action'],$_GET['nns_op_id']);
	echo '<script>alert("操作成功");</script>';
}elseif($_GET['op']=='queue_default'){
	$queue_task_model = new queue_task_model();
	$queue_task_model->update_queue_default_state_all($_GET['ids']);
	$queue_task_model=null;
	unset($_GET['nns_op_type'],$_GET['op'],$_GET['op_action'],$_GET['ids']);
	echo '<script>alert("操作成功");</script>';
}elseif($_GET['op']=='c2_task_start'){
    $queue_task_model = new queue_task_model();
    $queue_task_model->c2_task_start_all($_GET['ids'],$_GET['sp_id']);
    $queue_task_model=null;
    unset($_GET['nns_op_type'],$_GET['op'],$_GET['op_action'],$_GET['ids']);
    echo '<script>alert("操作成功");</script>';
}
elseif($_GET['op']=='check_status'){
    //epg_check_video_online_status
    $video_id = $_GET['nns_video_id'];
    $sp_id = $_GET['sp_id'];
    $sp_config = sp_model::get_sp_config($sp_id);
    $data = file_get_contents($sp_config['epg_check_video_online_status'].$video_id);
    $data = json_decode($data,true);
    $str = "";
    if($data['state']=='0'){
        if(is_array($data['data'])){
            foreach($data['data'] as $key => $val){
                if(is_array($val)){
                    foreach ($val as $value) {
                       $str .=$key.'='.$value['name'].'\n\n'; 
                    }
                }
            }
        }else{
            $str .='未上线到首页和其他栏目';
        }
    }elseif($data['state']=='1'){
        $str .='接口获取失败:'.$data['reason'];
    }elseif($data['state']=='2'){
        $str .='数据返回格式错误:'.$data['reason'];
    }elseif($data['state']=='3'){
        $str .='数据库错误:'.$data['reason'];
    }elseif($data['state']=='4'){
        $str .='接口未实现:'.$data['reason'];
    }
    echo '<script>alert("'.$str.'");history.go(-1);</script>';die;
}elseif($_GET['op']=='check'){
    //epg_check_video_intact_status
    
}elseif($_GET['op'] == 'queue_clip'){
	$op_ids = $_GET['op_ids'];
	$op_ids = explode(',', $op_ids);
	$queue_task_model = new queue_task_model();
	$queue_task_model->q_hand_get_clip_task($sp_id,$op_ids);
	$queue_task_model = null;
	unset($_GET['nns_op_type'],$_GET['op'],$_GET['op_ids']);
	echo '<script>alert("操作成功");</script>';
}
else if($_GET['op'] == 'file_encode')
{
    $op_ids= $_GET['ids'];
	$op_ids = explode(',', $op_ids);
	if(is_array($op_ids) && !empty($obj_dc))
	{
	    foreach ($op_ids as $str_id)
	    {
	        if(strlen($str_id) <1)
	        {
	            continue;
	        }
	        $result_op_queue= nl_op_queue::query_by_id($obj_dc, $str_id);
	        if(isset($result_op_queue['data_info']) && !empty($result_op_queue['data_info']) && is_array($result_op_queue['data_info']))
	        {
	            $make_queue_file_encode = new make_queue_file_encode_queue($obj_dc);
	            $make_queue_file_encode->init($result_op_queue['data_info']);
	        }
	    }
	}
	echo '<script>alert("操作成功");</script>';
}
elseif ($_GET['op'] == 'sort')
{
    if($_GET['d'] == 'top')
    {
        $result_max = nl_op_queue::get_max_priority($obj_dc,$sp_id);
        if($result_max['ret'] !=0)
        {
            echo '<script>alert("'.$result_max['reason'].'");</script>';
        }
        else
        {
            $num = isset($result_max['data_info']) ? (int)$result_max['data_info'] : 0;
            $arr_op_id = explode(',', $_GET['nns_op_id']);
            if(is_array($arr_op_id) && !empty($arr_op_id))
            {
                foreach ($arr_op_id as $str_op_id)
                {
                    if(strlen($str_op_id) <1)
                    {
                        continue;
                    }
                    $num++;
                    $num = ($num > 65535) ? 65535 : $num;
                    $result = nl_op_queue::edit($obj_dc,array('nns_weight'=>$num),$str_op_id);
                }
            }
            if($result){
                echo '<script>alert("任务排序成功");</script>';
            }else{
                echo '<script>alert("任务排序失败");</script>';
            }
        }
    }
    else if($_GET['d'] == 'bottom')
    {
        $arr_op_id = array_filter(explode(',', $_GET['nns_op_id']));
        $result = nl_op_queue::edit($obj_dc,array('nns_weight'=>0),$arr_op_id);
        if($result){
            echo '<script>alert("任务排序成功");</script>';
        }else{
            echo '<script>alert("任务排序失败");</script>';
        }
    }
    else if($_GET['d'] == 'up')
    {
        $num = isset($_GET['nns_weight']) ? (int)$_GET['nns_weight'] : 0;
        $num++;
        $num = ($num > 65535) ? 65535 : $num;
        $result = nl_op_queue::edit($obj_dc,array('nns_weight'=>$num),$_GET['nns_op_id']);
        if($result){
            echo '<script>alert("任务排序成功");</script>';
        }else{
            echo '<script>alert("任务排序失败");</script>';
        }
        unset($_GET['nns_weight']);
    }
    else if($_GET['d'] == 'down')
    {
        $num = isset($_GET['nns_weight']) ? (int)$_GET['nns_weight'] : 0;
        if($num <1)
        {
            echo '<script>alert("任务排序成功");</script>';
        }
        else
        {
            $num--;
            $result = nl_op_queue::edit($obj_dc,array('nns_weight'=>$num),$_GET['nns_op_id']);
            if($result){
                echo '<script>alert("任务排序成功");</script>';
            }else{
                echo '<script>alert("任务排序失败");</script>';
            }
        }
        unset($_GET['nns_weight']);
    }
    else if($_GET['d'] == 'set')
    {
        $num = (isset($_GET['nns_weight']) && (int)$_GET['nns_weight'] >=0) ? (int)$_GET['nns_weight'] : 0;
        $num = ($num > 65535) ? 65535 : $num;
        $result = nl_op_queue::edit($obj_dc,array('nns_weight'=>$num),$_GET['nns_op_id']);
        if($result){
            echo '<script>alert("任务排序成功");</script>';
        }else{
            echo '<script>alert("任务排序失败");</script>';
        }
        unset($_GET['nns_weight']);
    }
    unset($_GET['op']);
    unset($_GET['nns_op_id']);
    unset($_GET['d']);
}





if($_GET['ajax']=='status'){
	$sp_id = $_GET['sp_id'];
	$op_id = $_GET['nns_id'];
	$queue_task_model = new queue_task_model();
	
	$re = $queue_task_model->check_task_status($op_id);
	echo json_encode($re);
	die;
}

$sp_config = nl_sp::get_sp_config($obj_dc, $sp_id);
$sp_config = isset($sp_config['data_info']['nns_config']) ? $sp_config['data_info']['nns_config'] : null;
$flag_op_queue_to_encode = false; 
if(isset($sp_config['clip_file_encode_enable']) && $sp_config['clip_file_encode_enable'] == '1' && isset($sp_config['clip_file_encode_model']) && $sp_config['clip_file_encode_model'] == '2' )
{
    $flag_op_queue_to_encode = true;
}

$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : (!empty($_COOKIE["page_max_num"]) ?  $_COOKIE["page_max_num"] : 18);
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) {
	$page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit = $page_size;
$filter = null;
if ($_GET['actionsearch'] == 'search') {
	$_GET['nns_name'] = trim($_GET['nns_name']);
	$filter['nns_name'] = $_GET['nns_name'];
	$filter['nns_type'] = $_GET['nns_type'];
	$filter['nns_state'] = $_GET['nns_state'];
	$filter['nns_action'] = $_GET['nns_action_state'];
	$filter['day_picker_start'] = $_GET['day_picker_start'];
	$filter['day_picker_end'] = $_GET['day_picker_end'];
	$filter['state_queue'] = $_GET['nns_state_queue'];
	if(isset($_GET['nns_cp_id']) && strlen($_GET['nns_cp_id'])>0)
	{
	    $filter['nns_cp_id'] = $_GET['nns_cp_id'];
	}
}
$queue_task_model = new queue_task_model();
$result = $queue_task_model->get_q_task_list($sp_id, $filter, $offset, $limit);

$vod_total_num = $result['rows'];
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) {
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;

$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);

$data = $result['data'];
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<style>
.mytip_ul
{
    text-align: left;
    font-size: 12px;
    list-style-type: none;
    padding: 0;
    margin: 0;
}

.mytip_title
{
    margin-left: 10px;
    width: 100px;
    float: left;
    text-align: left;
}

.mytip_close
{
    margin-right: 10px;
    margin-top: 5px;
    width: 100px;
    float: right;
    text-align: right;
    cursor: pointer;
}

.mytip_top
{
    background-image: url(../../images/tips_titlebj.gif);
    color: #FFFFFF;
    font-size: 12px;
    height: 24px;
    line-height: 24px;
    font-weight: 600;
}

.tips_li
{
    height: 25px;
    line-height: 25px;
    border-bottom: #ccc 1px solid;
}

.tips_li_span
{
    color: #07507d;
    margin-left: 10px;
}

.tips_li_span2
{
    color: #6d6d6d;
}		
.myalert_div1
{
    height: 25px;
    line-height: 25px;
    border-bottom: #ccc 1px solid;
    width: 620px;
}

.myalert_div1_leftDiv
{
    float: left;
    width: 50%;
    margin-left: 10px;
}

.myalert_div1_rightDIv
{
    float: left;
    margin-left: 10px;
}

.myalert_div1_Div
{
    float: left;
    width: 100%;
    margin-left: 10px;
}

.myalert_div2
{
    height: 84px;
    line-height: 70px;
    border-bottom: #ccc 1px solid;
    width: 620px;
}

.myalert_div2_Div
{
    float: left;
    width: 100%;
    margin-left: 10px;
    height: 84px;
}

.myaelrt_li_title
{
    color: #07507d;
}

.myalert_li_txt
{
    color: #6d6d6d;
}

.myalert_inputTxt
{
    width: 220px;
    border: 0px solid #eff0f0;
    padding-left: 2px;
    background-color: #eff0f0;
    color: #6d6d6d;
}		
		</style>		
		
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
		<script language="javascript">

			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
                setCookie("page_max_num",num);
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}

				})
				return BoxKey;
			}

			
			$(document).ready(function() {
				$('.show_alert').click(function(){
					
					var op_id = $(this).attr('op_id');
					$.get('nncms_queue_task.php?ajax=op_q_detail&op_id='+op_id,function(data){
						//
						//alert(data.q_task_info.nns_name);
						var action = '';
						if(data.q_task_info.nns_action=='add') {
							action="添加";
						}else if(data.q_task_info.nns_action=='modify'){
							action="修改";
						}else if(data.q_task_info.nns_action=='destroy'){
							action="删除";
						}
						var type= '';
						if(data.q_task_info.nns_type=='video'){
							type="影片";
						}else if(data.q_task_info.nns_type=='index'){
							type="分集";
						}else if(data.q_task_info.nns_type=='media'){
							type="片源";
						}
						
						var status = data.q_task_info.nns_status;
						if(data.q_task_info.nns_status==0){
							status="等待注入";
						}else if(data.q_task_info.nns_status==1){
							status="正在注入";
						}else if(data.q_task_info.nns_status==2){
							status="等待切片";
						}else if(data.q_task_info.nns_status==3){
							status="正在切片";
						}else if(data.q_task_info.nns_status==4){
							status="取消";
						}						
                        var _html = '';		
						_html += '<div class="myalert_div1">';
			            _html += '<div class="myalert_div1_leftDiv"><span class="myaelrt_li_title">队列ID：</span><span class="myalert_li_txt">'+data.q_task_info.nns_id+'</span></div>';
						_html += '<div class="myalert_div1_rightDIv"><span class="myaelrt_li_title">类型：</span><span class="myalert_li_txt">'+type+'</span></div>';
			           	_html += ' </div>' ; 
			           	
						_html += '<div class="myalert_div1">'	;		            
			            _html += '<div class="myalert_div1_leftDiv"><span class="myaelrt_li_title">名称：</span><span class="myalert_li_txt">'+data.q_task_info.nns_name+'</span></div>';
						_html += '<div class="myalert_div1_rightDIv"><span class="myaelrt_li_title">动作：</span><span class="myalert_li_txt">'+action+'</span></div>';
			           	_html += ' </div>';
						_html += '<div class="myalert_div1">';
			            _html += '<div class="myalert_div1_leftDiv"><span class="myaelrt_li_title">队列状态：</span><span class="myalert_li_txt">'+status+'</span></div>'
						_html += '<div class="myalert_div1_rightDIv"><span class="myaelrt_li_title">入队列时间：</span><span class="myalert_li_txt">'+data.q_task_info.nns_create_time+'</span></div>'
			           	_html += ' </div>'	;
	
			           	if(data.q_task_info.nns_type=='media'){
			           		var mode = data.media_info.nns_mode;
			           		if(data.media_info.nns_mode=='std'){
			           			mode = "标清";
			           		}else if(data.media_info.nns_mode=='hd'){
			           			mode = "高清";
			           		}else if(data.media_info.nns_mode=='low'){
			           			mode = "流畅";
			           		}
			           		var deleted ='';
			           		if(data.media_info.nns_deleted==1){
			           			deleted = "已删除";
			           		}else{
			           			deleted = "未删除";
			           		}
							_html += '<div class="myalert_div1">';
				            _html += '<div class="myalert_div1_leftDiv"><span class="myaelrt_li_title">影片ID：</span><span class="myalert_li_txt">'+data.media_info.nns_vod_id+'</span></div>';
							_html += '<div class="myalert_div1_rightDIv"><span class="myaelrt_li_title">分集：</span><span class="myalert_li_txt">'+data.media_info.nns_vod_index+'</span></div>';
				           	_html += ' </div>'	;
							_html += '<div class="myalert_div1">';
							_html += '<div class="myalert_div1_leftDiv"><span class="myaelrt_li_title">分集ID：</span><span class="myalert_li_txt">'+data.media_info.nns_vod_index_id+'</span></div>'	;            
							_html += '<div class="myalert_div1_rightDIv"><span class="myaelrt_li_title">媒体ID：</span><span class="myalert_li_txt">'+data.media_info.nns_content_id+'</span></div>';
				           	_html += ' </div>'	;
							_html += '<div class="myalert_div1">';
				            _html += '<div class="myalert_div1_leftDiv"><span class="myaelrt_li_title">片源ID：</span><span class="myalert_li_txt">'+data.media_info.nns_id+'</span></div>';
							_html += '<div class="myalert_div1_rightDIv"><span class="myaelrt_li_title">片源URL：</span><span class="myalert_li_txt">'+data.media_info.nns_url+'</span></div>';
				           	_html += ' </div>';
							_html += '<div class="myalert_div1">';
				            _html += '<div class="myalert_div1_leftDiv"><span class="myaelrt_li_title">清晰度：</span><span class="myalert_li_txt">'+mode+'</span></div>';
							_html += '<div class="myalert_div1_rightDIv"><span class="myaelrt_li_title">片源格式：</span><span class="myalert_li_txt">'+data.media_info.nns_filetype+'</span></div>';
				           	_html += ' </div>'	;
							_html += '<div class="myalert_div1">';
				            _html += '<div class="myalert_div1_leftDiv"><span class="myaelrt_li_title">片源码率(kbps)：</span><span class="myalert_li_txt">'+data.media_info.nns_kbps+'</span></div>';
							_html += '<div class="myalert_div1_rightDIv"><span class="myaelrt_li_title">是否删除：</span><span class="myalert_li_txt">'+deleted+'</span></div>';
				           	_html += ' </div>'	;
							_html += '<div class="myalert_div1">';
				            _html += '<div class="myalert_div1_leftDiv"><span class="myaelrt_li_title">来源ID：</span><span class="myalert_li_txt">'+data.media_info.nns_import_id+'</span></div>';
							_html += '<div class="myalert_div1_rightDIv"><span class="myaelrt_li_title">来源：</span><span class="myalert_li_txt">'+data.media_info.nns_import_source+'</span></div>';
				           	_html += ' </div>'	 ;    	
							_html += '<div class="myalert_div1">';
				            _html += '<div class="myalert_div1_leftDiv"><span class="myaelrt_li_title">创建时间：</span><span class="myalert_li_txt">'+data.media_info.nns_create_time+'</span></div>';
							_html += '<div class="myalert_div1_rightDIv"><span class="myaelrt_li_title">修改时间：</span><span class="myalert_li_txt">'+data.media_info.nns_modify_time+'</span></div>';
				           	_html += ' </div>'	;		           				           				           					           	
			           	}else if(data.q_task_info.nns_type=='video'){
							_html += '<div class="myalert_div1">';
				            _html += '<div class="myalert_div1_leftDiv"><span class="myaelrt_li_title">影片ID：</span><span class="myalert_li_txt">'+data.video_info.nns_id+'</span></div>';
							_html += '<div class="myalert_div1_rightDIv"><span class="myaelrt_li_title"></span><span class="myalert_li_txt"></span></div>';
				           	_html += ' </div>'	;			           		
							_html += '<div class="myalert_div1">';
				            _html += '<div class="myalert_div1_leftDiv"><span class="myaelrt_li_title">来源ID：</span><span class="myalert_li_txt">'+data.video_info.nns_asset_import_id+'</span></div>';
							_html += '<div class="myalert_div1_rightDIv"><span class="myaelrt_li_title">来源：</span><span class="myalert_li_txt">'+data.video_info.nns_import_source+'</span></div>';
				           	_html += ' </div>'	 ;    	
							_html += '<div class="myalert_div1">';
				            _html += '<div class="myalert_div1_leftDiv"><span class="myaelrt_li_title">创建时间：</span><span class="myalert_li_txt">'+data.video_info.nns_create_time+'</span></div>';
							_html += '<div class="myalert_div1_rightDIv"><span class="myaelrt_li_title">修改时间：</span><span class="myalert_li_txt">'+data.video_info.nns_modify_time+'</span></div>';
				           	_html += ' </div>'	;			           		
			           	}else if(data.q_task_info.nns_type=='index'){
							_html += '<div class="myalert_div1">';
				            _html += '<div class="myalert_div1_leftDiv"><span class="myaelrt_li_title">影片ID：</span><span class="myalert_li_txt">'+data.index_info.nns_vod_id+'</span></div>';
							_html += '<div class="myalert_div1_rightDIv"><span class="myaelrt_li_title"></span><span class="myalert_li_txt"></span></div>';
				           	_html += ' </div>'	;	
							_html += '<div class="myalert_div1">';
				            _html += '<div class="myalert_div1_leftDiv"><span class="myaelrt_li_title">分集ID：</span><span class="myalert_li_txt">'+data.index_info.nns_id+'</span></div>';
							_html += '<div class="myalert_div1_rightDIv"><span class="myaelrt_li_title">分集：</span><span class="myalert_li_txt">'+data.index_info.nns_index+'</span></div>';
				           	_html += ' </div>'	;					           			           		
							_html += '<div class="myalert_div1">';
				            _html += '<div class="myalert_div1_leftDiv"><span class="myaelrt_li_title">来源ID：</span><span class="myalert_li_txt">'+data.index_info.nns_import_id+'</span></div>';
							_html += '<div class="myalert_div1_rightDIv"><span class="myaelrt_li_title">来源：</span><span class="myalert_li_txt">'+data.index_info.nns_import_source+'</span></div>';
				           	_html += ' </div>'	 ;    	
							_html += '<div class="myalert_div1">';
				            _html += '<div class="myalert_div1_leftDiv"><span class="myaelrt_li_title">创建时间：</span><span class="myalert_li_txt">'+data.index_info.nns_create_time+'</span></div>';
							_html += '<div class="myalert_div1_rightDIv"><span class="myaelrt_li_title">修改时间：</span><span class="myalert_li_txt">'+data.index_info.nns_modify_time+'</span></div>';
				           	_html += ' </div>'	;			           		
			           	}
			           	$("#div_context_li").html(_html);	
			           	show_alert($("#myalert").html(), '640px', '320px');	           			           			           				           				           	                     					
			           	window.parent.resetFrameHeight();
					},'json');
					
				});
				$('#clear_time').click(function(){
					$('#day_picker_start').val('');
					$('#day_picker_end').val('');

				});

				$('.edit_weight').click(function(){
					var click_td = $(this);
					var tr = $(this).parent('tr').first();
					//本条任务的id
					var val_id = tr.find('input').val();
					//本条任务的权重(去掉空格)
					var priority_old = $(this).text().trim();
					//在td中加入input表单，并把权重值填入
					$(this).html('<input style="width:50px" type="text" value="'+ priority_old +'"/>');
					var this_td_input = $(this).find('input');
					this_td_input.focus().select().blur(function(){
						var priority_new = $(this).val();
						priority_new = parseFloat(priority_new);
						if(isNaN(priority_new))
						{
							alert('请输入数字');
							this.focus();
						}
						else
						{
							click_td.text(priority_new);
							window.location.href="nncms_queue_task.php?sp_id=<?php echo $sp_id;?>&op=sort&d=set&nns_weight="+priority_new+"&nns_op_id="+val_id;
						}
					});
				});

				window.parent.now_frame_url = window.location.href;
			});
			
			
			
			function queue_end(){
				var r=confirm("是否进行该操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();					
					ids = ids.substr(0,ids.length-1);
					var url = "nncms_queue_task.php?sp_id=<?php echo $sp_id?>&op=queue_end&ids="+ids;
						window.location.href = url;
				}
			}

			function file_encode()
			{
				var r=confirm("是否进行转码操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					var url = "nncms_queue_task.php?sp_id=<?php echo $sp_id?>&op=file_encode&ids="+ids+"<?php echo $nns_url_op;?>";
						window.location.href = url;
				}
			}
			
			function queue_start(){
				var r=confirm("是否进行该操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					var url = "nncms_queue_task.php?sp_id=<?php echo $sp_id?>&op=queue_start&ids="+ids;
						window.location.href = url;
				}
			}
			
			
			function task_destroy(){
				var r=confirm("是否进行该操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					var url = "nncms_queue_task.php?sp_id=<?php echo $sp_id?>&op=queue_destroy&ids="+ids;
						window.location.href = url;
				}
			}
			
			
			function queue_default(){
				var r=confirm("是否进行该操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					var url = "nncms_queue_task.php?sp_id=<?php echo $sp_id?>&op=queue_default&ids="+ids;
						window.location.href = url;
				}
			}
			
			
			function queue_audit(){
				var r=confirm("是否进行该操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					var url = "nncms_queue_task.php?sp_id=<?php echo $sp_id?>&op=queue_audit&ids="+ids;
						window.location.href = url;
				}
			}
			function queue_clip(){
				var r=confirm("是否进行该操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					var url = "nncms_queue_task.php?sp_id=<?php echo $sp_id?>&op=queue_clip&nns_op_type=media&op_ids="+ids+"<?php echo $nns_url_op;?>";
						window.location.href = url;
				}
			}

			function sort(type){
				type = type == 'top' ? 'top' : 'bottom';
				var r=confirm("是否进行该操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					var url = "nncms_queue_task.php?sp_id=<?php echo $sp_id?>&nns_op_type=media&op=sort&d="+type+"&nns_op_id="+ids+"<?php echo $nns_url_op;?>";
						window.location.href = url;
				}
			}

            function c2_task_start(){
                var r=confirm("是否进行该操作");
                if(r == true){
                    var ids=getAllCheckBoxSelect();
                    ids = ids.substr(0,ids.length-1);
                    var url = "nncms_queue_task.php?sp_id=<?php echo $sp_id?>&op=c2_task_start&ids="+ids;
                    window.location.href = url;
                }
            }


			$(document).ready(function() {
				$("td#is_delete").find("input").each(function(){
					var self = this;
					$.get("nncms_queue_task.php?ajax=status&sp_id=<?php echo $sp_id; ?>&nns_id="+$(this).val(),function(data){
						if(data.check>0){
							$("#"+data.nns_id).html("<font color='red'>"+data.text+"</font>");
							if(data.check>1){
								$(self).parent().parent().find('.control_btns_box').append("<br/><a href='./nncms_queue_task.php?sp_id=<?php echo $sp_id; ?>&op=default&nns_op_id="+data.nns_id+"'>重新注入</a>");
							}
					}else{
							$("#"+data.nns_id).html("<font color='green'>"+data.text+"</font>");
						}
						window.parent.resetFrameHeight();
					},'json');
				});


			});
		</script>

	</head>

	<body>
<div id="myalert" style="width: 620px; position: absolute; display:none; border: 1px solid gray; filter: progid:DXImageTransform.Microsoft.Shadow(color=#999999,direction=135,strength=3); left: 0; top: 5px;background-color:#FFF;">
    <div id="top" class="mytip_top">
        <div class="mytip_title">详细信息</div>
        <div class="mytip_close" onclick="doClose()"><img src="../../images/log_close.gif" width="9" height="9" /></div>
    </div>
    <div id="context" style="height:396px; background-color:#eff0f0; border:#888 solid 1px;"><!--如果数据过多，加入 overflow-y:scroll;  出滚动条-->
        <div class="mytip_ul" id="div_context_li">
            <div class="myalert_div1">
                <div class="myalert_div1_leftDiv"><span class="myaelrt_li_title"></span><span class="myalert_li_txt"></span></div>
                <div class="myalert_div1_rightDIv"><span class="myaelrt_li_title"></span><span class="myalert_li_txt"></span></div>
            </div>
        </div>
    </div>
</div>	
		<div class="content">
			<div class="content_position">
				操作列队
			</div>
				<div class="content_table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<form action="" method="get">
							<tbody>
								<tr>
									<td>
										影片名：&nbsp;&nbsp;<input type="text" name="nns_name" id="nns_name" value="<?php echo $_GET['nns_name']; ?>" style="width:200px;">
										&nbsp;&nbsp;&nbsp;&nbsp;类型：&nbsp;&nbsp;<select name="nns_type" style="width: 150px;">
											<option value="" >全部</option>
											<option value="video" <?php if($_GET['nns_type']=='video') echo 'selected="selected"'?>>影片</option>
											<option value="index" <?php if($_GET['nns_type']=='index') echo 'selected="selected"'?>>分集</option>
											<option value="media" <?php if($_GET['nns_type']=='media') echo 'selected="selected"'?>>片源</option>
											<option value="live" <?php if($_GET['nns_type']=='live') echo 'selected="selected"'?>>直播频道</option>
											<option value="live_media" <?php if($_GET['nns_type']=='live_media') echo 'selected="selected"'?>>直播源</option>
											<option value="playbill" <?php if($_GET['nns_type']=='playbill') echo 'selected="selected"'?>>节目单</option>
											<option value="file" <?php if($_GET['nns_type']=='file') echo 'selected="selected"'?>>包文件</option>
											<option value="product" <?php if($_GET['nns_type']=='product') echo 'selected="selected"'?>>产品包</option>
											<option value="product" <?php if($_GET['nns_type']=='epg_file') echo 'selected="selected"'?>>EPGFile模板文件</option>
                                    </select>
                                    &nbsp;&nbsp;&nbsp;&nbsp;操作：&nbsp;&nbsp;<select name="nns_action_state" style="width: 150px;">
											<option value="" >全部</option>
											<option value="add" <?php if($_GET['nns_action_state']=='add') echo 'selected="selected"'?>>添加</option>
											<option value="modify" <?php if($_GET['nns_action_state']=='modify') echo 'selected="selected"'?>>修改</option>
											<option value="destroy" <?php if($_GET['nns_action_state']=='destroy') echo 'selected="selected"'?>>删除</option>
                                    </select> 
                                     &nbsp;&nbsp;&nbsp;&nbsp;
										列队状态：&nbsp;&nbsp;<select name="nns_state" style="width: 150px;">
											<option value="" >全部</option>											
											<option value="bk_content_wait" <?php if($_GET['nns_state']=='bk_content_wait') echo 'selected="selected"'?>>等待注入</option>
											<option value="bk_content_doing" <?php if($_GET['nns_state']=='bk_content_doing') echo 'selected="selected"'?>>正在注入</option>
											<option value="bk_content_wait_clip" <?php if($_GET['nns_state']=='bk_content_wait_clip') echo 'selected="selected"'?>>等待切片</option>
											<option value="bk_content_clip_doing" <?php if($_GET['nns_state']=='bk_content_clip_doing') echo 'selected="selected"'?>>正在切片</option>
											<option value="bk_wait_sp_encode_begin" <?php if($_GET['nns_state']=='bk_wait_sp_encode_begin') echo 'selected="selected"'?>>等待转码</option>
											<option value="bk_wait_sp_encode" <?php if($_GET['nns_state']=='bk_wait_sp_encode') echo 'selected="selected"'?>>正在转码</option>
											<option value="bk_fail_sp_encode" <?php if($_GET['nns_state']=='bk_fail_sp_encode') echo 'selected="selected"'?>>转码失败</option>
											<option value="bk_content_cancel" <?php if($_GET['nns_state']=='bk_content_cancel') echo 'selected="selected"'?>>取消</option>
											<option value="bk_unaudit" <?php if($_GET['nns_state']=='bk_unaudit') echo 'selected="selected"'?>>未审核</option>
											<option value="bk_wait_sp" <?php if($_GET['nns_state']=='bk_wait_sp') echo 'selected="selected"'?>>等待前置SP</option>
                                    </select>
                                    
                                    
                                    &nbsp;&nbsp;&nbsp;&nbsp;
										列队运行状态：&nbsp;&nbsp;<select name="nns_state_queue" style="width: 150px;">
											<option value="" >全部</option>											
											<option value="op_pause" <?php if($_GET['nns_state_queue']=='op_pause') echo 'selected="selected"'?>>暂停</option>
											<option value="op_do" <?php if($_GET['nns_state_queue']=='op_do') echo 'selected="selected"'?>>运行</option>
                                    </select>                                                                   
                                    </td>
                                    </tr>
                                    <tr>
									<td>
									    &nbsp;&nbsp;&nbsp;&nbsp;CP ID：&nbsp;&nbsp;&nbsp;&nbsp;<select name="nns_cp_id" style="width: 150px;">
                                    		<option value="" >全部</option>
                                    	<?php 
                                    		foreach ($result_cp_id_data as $cp_id_key=>$cp_id_val)
											{
												?>
												<option value="<?php echo $cp_id_key; ?>" <?php if(isset($_GET['nns_cp_id']) && $_GET['nns_cp_id']==$cp_id_key) echo 'selected="selected"'?>><?php echo $cp_id_val; ?></option>
												<?php 
											}
											echo "</select>";
                                    	?>
                                    	&nbsp;&nbsp;&nbsp;&nbsp;
										选择时间段：&nbsp;&nbsp;&nbsp;<input name="day_picker_start" id="day_picker_start" type="text"  value="<?php
										if (isset($_GET['day_picker_start']))
											echo $_GET['day_picker_start'];
									?>" style="width:120px;" class="datetimepicker" callback="test" />
										 - <input name="day_picker_end" id="day_picker_end" type="text"  value="<?php
											if (isset($_GET['day_picker_end']))
												echo $_GET['day_picker_end'];
										?>" style="width:120px;" class="datetimepicker" callback="test" />
										 &nbsp;&nbsp;<input type="button" id="clear_time" name="clear_time" value="清除时间"/>
										<input type="hidden" name="sp_id" value="<?php echo $sp_id; ?>" />
										<input type="hidden" name="actionsearch" value="search" />
										&nbsp;&nbsp;&nbsp;&nbsp;	
										<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  />										
									</td>
								</tr>
							</tbody>
							</form>
						</table>
				</div>			
			<div class="content_table formtable">

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th><input name="" type="checkbox" value="">序号</th>
							<th>队列ID</th>
							<th>名称</th>
							<th>类型</th>
							<th>动作</th>
							<th>入列队时间</th>
							<th>权重</th>
							<th>列队状态</th>
							<th>列队运行状态</th>
							<th>是否属于分组</th>
							<th>CP ID</th>
						    <th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if($data!=null){
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) {
								$num++;
						?>
						<tr>
						<td id="is_delete"><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
						<td >                        
                        <?php
						
							echo '<a class="show_alert" style="cursor: pointer;" op_id="'.$item['nns_id'].'" href="javascript:void(0);">'.$item['nns_id'].'</a>';
						
                        ?>
                   
                        </td>

                        <td>
                        	<font id="<?php echo $item["nns_id"]; ?>"></font>
                        	 
                        	<?php 
                        	
                        	if($item['nns_status']==6){
								echo '<font color="blue">[未审核]</font>&nbsp;&nbsp;&nbsp;';
							}
                        
                        	if($item['nns_state']==1){
                        		echo '<font color="red">[暂停]</font>&nbsp;&nbsp;&nbsp;';                        		
                        	}else{
                        		echo '<font color="green">[运行]</font>&nbsp;&nbsp;&nbsp;';
                        	}
							
							
                        ?>                       
                        <?php
						
							echo $item['nns_name'];
						
                        ?>
                        
                        
                   
                        </td>
                        <td>
                        <?php
						switch($item['nns_type']) {
							case 'video' :
								echo '影片';
								break;
							case 'index' :
								echo '分集';
								break;
							case 'media' :
								echo '片源';
								break;
							case 'live' :
							    echo '直播频道';
							    break;
							case 'live_media' :
							    echo '直播源';
							    break;
						    case 'playbill' :
						        echo '节目单';
						        break;
					        case 'file' :
					            echo '包文件';
					            break;
				            case 'product' :
				                echo '产品包';
				                break;
				            case 'epg_file' :
				                echo 'EPGFile模板文件';
				                break;
						}
                        ?>
                        </td>
                        <td>                        
                        <?php
							switch($item['nns_action']) {
								case 'add' :
									echo '添加';
									break;
								case 'modify' :
									echo '修改';
									break;
								case 'destroy' :
									echo '删除';
									break;
							}
                        ?>
                   
                        </td>
                        <td><?php echo $item['nns_create_time']; ?></td> 
                        <td class="edit_weight"><?php echo $item['nns_weight']; ?></td>
                        <td>	
                        <?php 
                        
                        switch($item['nns_status']) {
							case 0 :
								echo '等待注入';
							break;
							
							case 1 :
								echo '正在注入';
							break;
							
							case 2 :
								echo '等待切片';
							break;
							
							case 3 :
								echo '正在切片';
							break;
							case 4 :
								echo '取消';
							break;
							case 6 :
								echo '未审核';
							break;
							case 7 :
								echo '等待前置SP';
							break;
							case 8 :
							    echo '正在转码';
							    break;
							case 9 :
							    echo '等待转码';
							    break;
							case 10 :
							    echo '转码失败';
							    break;
						}
						?>
						</td>
						<td>	
                        <?php 
                        
                        switch($item['nns_state']) {
							case 0 :
								echo "<font color='blue'>运行</font>";
							    break;
							default :
							    echo "<font color='red'>暂停</font>";
							    break;
						}
						?>
						</td>
						<td><?php echo ($item['nns_is_group'] != '1') ? "否" : "是"; ?></td>
						<td>
					    <?php if(in_array($item['nns_cp_id'], $result_cp_data_keys))
					    {
					        echo isset($result_cp_id_data[$item['nns_cp_id']]) ? $result_cp_id_data[$item['nns_cp_id']] : '';
					    }
					    else
					    {
					        echo '<font color="#FF0000">非法</font>';
					    }?>
					    </td>
                        <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                        	<?php
                        	switch ($item['nns_type']) {
								case 'video':
								?>
								<a href="./nncms_queue_task.php?sp_id=<?php echo $item['nns_org_id'];?>&nns_op_type=video&op=only&op_action=pause&nns_op_id=<?php echo $item['nns_id'].$nns_url_op; ?>">单体暂停</a>
                       			<a href="./nncms_queue_task.php?sp_id=<?php echo $item['nns_org_id'];?>&nns_op_type=video&op=all&op_action=pause&nns_op_id=<?php echo $item['nns_id'].$nns_url_op; ?>">全体暂停</a>
                       			<a href="./nncms_queue_task.php?sp_id=<?php echo $item['nns_org_id'];?>&nns_op_type=video&op=only&op_action=resume&nns_op_id=<?php echo $item['nns_id'].$nns_url_op; ?>">单体取消暂停</a>
                       			<a href="./nncms_queue_task.php?sp_id=<?php echo $item['nns_org_id'];?>&nns_op_type=video&op=all&op_action=resume&nns_op_id=<?php echo $item['nns_id'].$nns_url_op; ?>">全体取消暂停</a>
								<?php	
									break;
								case 'index':
									?>
								<a href="./nncms_queue_task.php?sp_id=<?php echo $item['nns_org_id'];?>&nns_op_type=index&op=only&op_action=pause&nns_op_id=<?php echo $item['nns_id'].$nns_url_op; ?>">单体暂停</a>
                       			<a href="./nncms_queue_task.php?sp_id=<?php echo $item['nns_org_id'];?>&nns_op_type=index&op=all&op_action=pause&nns_op_id=<?php echo $item['nns_id'].$nns_url_op; ?>">全体暂停</a>
                       			<a href="./nncms_queue_task.php?sp_id=<?php echo $item['nns_org_id'];?>&nns_op_type=index&op=only&op_action=resume&nns_op_id=<?php echo $item['nns_id'].$nns_url_op; ?>">单体取消暂停</a>
                       			<a href="./nncms_queue_task.php?sp_id=<?php echo $item['nns_org_id'];?>&nns_op_type=index&op=all&op_action=resume&nns_op_id=<?php echo $item['nns_id'].$nns_url_op; ?>">全体取消暂停</a>
									<?php
									break;	
								case 'media':
								?>
								<a href="./nncms_queue_task.php?sp_id=<?php echo $item['nns_org_id'];?>&nns_op_type=media&op=only&op_action=pause&nns_op_id=<?php echo $item['nns_id'].$nns_url_op; ?>">暂停</a>
                       			<a href="./nncms_queue_task.php?sp_id=<?php echo $item['nns_org_id'];?>&nns_op_type=media&op=all&op_action=resume&nns_op_id=<?php echo $item['nns_id'].$nns_url_op; ?>">取消暂停</a>
								<?php
								if($item['nns_status'] == 2)
								{
								?>
								<a href="./nncms_queue_task.php?sp_id=<?php echo $item['nns_org_id'];?>&nns_op_type=media&op=queue_clip&op_ids=<?php echo $item['nns_id'].$nns_url_op; ?>">单体切片</a>
								<?php			
									}
									break;	
							}
                        	
                        	?>
                        	
                        	
                        	<a href="./nncms_queue_task.php?sp_id=<?php echo $item['nns_org_id'];?>&op=check_status&nns_video_id=<?php echo $item['nns_video_id'].$nns_url_op; ?>">检测EPG资源使用</a>
                        	
                        	<a href="./nncms_queue_task.php?sp_id=<?php echo $item['nns_org_id'];?>&op=check&nns_video_id=<?php echo $item['nns_video_id'].$nns_url_op; ?>">检测EPG影片完整性</a>
                        	
                        	<a href="./nncms_queue_task.php?sp_id=<?php echo $item['nns_org_id'];?>&nns_op_type=media&op=sort&d=top&nns_op_id=<?php echo $item['nns_id'].$nns_url_op; ?>">置顶</a>
                        	<a href="./nncms_queue_task.php?sp_id=<?php echo $item['nns_org_id'];?>&nns_op_type=media&op=sort&d=up&nns_weight=<?php echo $item['nns_weight'];?>&nns_op_id=<?php echo $item['nns_id'].$nns_url_op; ?>">向上</a>
                        	<a href="./nncms_queue_task.php?sp_id=<?php echo $item['nns_org_id'];?>&nns_op_type=media&op=sort&d=down&nns_weight=<?php echo $item['nns_weight'];?>&nns_op_id=<?php echo $item['nns_id'].$nns_url_op; ?>">向下</a>
                       		<a href="./nncms_queue_task.php?sp_id=<?php echo $item['nns_org_id'];?>&nns_op_type=media&op=sort&d=bottom&nns_op_id=<?php echo $item['nns_id'].$nns_url_op; ?>">置底</a>
                            <?php
                            if($item['nns_status'] == 0) {
                                ?>
                                <a href="./nncms_queue_task.php?sp_id=<?php echo $item['nns_org_id']; ?>&op=c2_task_start&ids=<?php echo $item['nns_id']; ?>">注入</a>
                                <?php
                            }
                            ?>
                            <?php if($flag_op_queue_to_encode){?>
                                <?php if($item['nns_status'] == '9'){?>
                                <a href="./nncms_queue_task.php?sp_id=<?php echo $item['nns_org_id'];?>&op=file_encode&ids=<?php echo $item['nns_id'].$nns_url_op; ?>">转码</a>
                            
                                <?php }?>
                            <?php }?>
                        	</td>
                        </tr>
						<?php
							}
						}$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) {
						?>
						<tr>
                                                                                                                                                                                          
                                                               
                                                               <td>&nbsp;</td>
                                                               <td>&nbsp;</td>
                                                               <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp; </td><td>&nbsp; </td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp;</td>
                                                                 <td>&nbsp;</td>
                                                                 <td>&nbsp;</td>
                                                                 <td>&nbsp;</td>

                                                        </tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="pagecontrol">
			  共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_queue_task.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_queue_task.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_queue_task.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_queue_task.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_queue_task.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

                        </div>
                        
                 <div class="controlbtns">
    	<div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
        <div class="controlbtn move"><a href="javascript:queue_end();">暂停</a></div>
        <div class="controlbtn move"><a href="javascript:queue_start();">取消暂停</a></div>        
        <div class="controlbtn delete"><a href="javascript:task_destroy();">删除</a></div>
        <div class="controlbtn move"><a href="javascript:queue_default();">重新注入</a></div>
        <div class="controlbtn move"><a href="javascript:queue_audit();">通过审核</a></div>
        <div class="controlbtn move"><a href="javascript:queue_clip();">切片</a></div>
        <div class="controlbtn move"><a href="javascript:sort('top');">批量置顶</a></div>
        <div class="controlbtn move"><a href="javascript:sort('button');">批量置底</a></div>
        <div class="controlbtn move"><a href="javascript:c2_task_start();">注入</a></div>
        <?php if($flag_op_queue_to_encode){?>
                <div class="controlbtn move"><a href="javascript:file_encode();">转码</a></div>
        <?php }?>
        <div style="clear:both;"></div>
    </div>    
                        
		</div>
	</body>
</html>
