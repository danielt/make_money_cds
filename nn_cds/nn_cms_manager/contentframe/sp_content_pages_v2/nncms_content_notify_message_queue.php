<?php 
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) 
{
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
include_once $nncms_config_path . "nn_logic/notify_message/notify_message.class.php";
include_once $nncms_config_path . "nn_logic/sp/sp.class.php";

$dc = nl_get_dc(array(
       	'db_policy' => NL_DB_READ | NL_DB_WRITE,
        'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$dc->open();
$sp_all = nl_sp::query_all($dc);
$sp_arr = array();
$sp_info = array();
if($sp_all['ret'] == 0 && !empty($sp_all['data_info']) && is_array($sp_all['data_info']))
{
	$sp_arr = $sp_all['data_info'];
	$sp_info = np_array_rekey($sp_arr, 'nns_id');
}
$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 18;
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) 
{
	$page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit = $page_size;
$filter = null;
if(!empty($_GET['cp_id'])){
    $filter['nns_cp_id'] = $_GET['cp_id'];
}
if(isset($_GET['action_op']))
{
	switch ($_GET['action_op'])
	{
		case 'search':
			$filter['nns_message_id'] = $_GET['nns_message_id'];
			$filter['nns_status'] = $_GET['nns_status'];
			$filter['nns_notify'] = $_GET['nns_notify'];
			$filter['nns_org_id'] = $_GET['nns_org_id'];
			break;
	}
}
$result = nl_notify_message::get_nofity_message($dc, $filter, $offset, $limit,true);
$vod_total_num = $result['page_info']['total_count'];
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) 
{
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;

$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);

$data = $result['data_info']; 
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
		<script language="javascript">

			function refresh_vod_page() 
			{
				var num = $("#nns_list_max_num").val();
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) 
			{
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}

				})
				return BoxKey;
			}
				
		</script>

	</head>
	<body>
		<div class="content">
			<div class="content_position">
				异步反馈队列
			</div>
				<div class="content_table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<form action="" method="get">
							<tbody>
								<tr>
									<td>
										消息ID：&nbsp;&nbsp;<input type="text" name="nns_message_id" id="nns_message_id" value="<?php echo $_GET['nns_message_id']; ?>" style="width:200px;">
										&nbsp;&nbsp;反馈内容：&nbsp;&nbsp;<input type="text" name="nns_notify" id="nns_notify" value="<?php echo $_GET['nns_notify']; ?>" style="width:200px;">
										&nbsp;&nbsp;反馈状态：&nbsp;&nbsp;<select name="nns_status" style="width: 150px;">
											<option value="" >全部</option>
											<option value="0" <?php if($_GET['nns_status']=='0') echo 'selected="selected"'?>>等待反馈</option>
											<option value="2" <?php if($_GET['nns_status']=='-1') echo 'selected="selected"'?>>反馈失败</option>
											<option value="3" <?php if($_GET['nns_status']=='1') echo 'selected="selected"'?>>反馈成功</option>
                                    	</select>
										<input type="hidden" name="action_op" value="search" />
										&nbsp;&nbsp;&nbsp;&nbsp;	
										<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  />										
									</td>
								</tr>
							</tbody>
							</form>
						</table>
				</div>			
			<div class="content_table formtable">

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th><input name="" type="checkbox" value=""></th>
							<th>序号</th>
							<th>消息ID</th>
							<th>CPID</th>
							<th>SPID</th>	
							<th>反馈状态</th>					
							<th>队列创建时间</th>
							<th>队列修改时间</th>
							<th>操作</th>						    
						</tr>
					</thead>
					<tbody>
						<?php
						if($data != null)
						{
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) 
							{
								$num++;
						?>
						<tr>
						<td><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>" /></td>
						<td><?php echo $num; ?></td>
						<td><?php echo $item['nns_message_id'];?></td>
                        <td><?php echo $item['nns_cp_id'];?></td>
                        <td><?php echo $sp_info[$item['nns_org_id']]['nns_name'];?></td>
                        <td><font color="red">
                        <?php 
                        	switch ($item['nns_status'])
                        	{
                        		case '0':
                        			echo '等待反馈';
                        			break;
                        		case '1':
                        			echo '反馈成功';
                        			break;
                        		case '-1':
                        			echo '反馈失败';
                        			break;
                        	}
                        ?>
                        </font></td>
                        <td><?php echo $item['nns_create_time']; ?></td>  
                        <td><?php echo $item['nns_modify_time']; ?></td> 
                        <td><a target="_blank" href="nncms_content_notify_message_view.php?id=<?php echo $item['nns_id'];?>">查看内容</a></td>   
                        </tr>
						<?php
							}
						}
						$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) 
						{
						?>
						<tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                     	</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="pagecontrol">
			  共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_notify_message_queue.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_notify_message_queue.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_content_notify_message_queue.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_notify_message_queue.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_notify_message_queue.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

          	</div>
<!--        	<div class="controlbtns"> -->
<!-- 	    	<div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div> -->
<!-- 	        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div> -->
<!--            <div style="clear:both;"></div>  -->               
		</div>
	</body>
</html>
