<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/11/14 17:44
 */
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
set_time_limit(0);
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
    $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135202");
$checkpri = null;


if (!$pri_bool) {
    Header("Location: ../nncms_content_wrong.php");
    exit ;
}

require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';

global $g_bk_version_number;
global $g_bk_web_url;

$dc = nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$sp_id = $_GET['sp_id'];
$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : (!empty($_COOKIE["page_max_num"]) ?  $_COOKIE["page_max_num"] : 18);
$page_num = isset($_GET['page'])?$_GET['page']:1;
if($page_num<1){
    $page_num = 1;
}

$offset = intval($page_num-1)*$page_size;
$limit = $page_size;
$filter = null;
if($_GET['op']=='resend' && !empty($_GET['c2_id'])){
    c2_task_model::execute_c2_again($_GET['c2_id'],$_GET['sp_id']);
    echo '<script>alert("重发指令成功");</script>';
    unset($_GET['op']);
    unset($_GET['c2_id']);
}
else if($_GET['op']=='resend_more' && !empty($_GET['c2_ids'])){
    $nns_ids = explode(',',$_GET['c2_ids']);
    foreach($nns_ids as $item_id){
        if(!empty($item_id))
            c2_task_model::execute_c2_again($item_id,$_GET['sp_id']);
    }

    echo '<script>alert("重发指令成功");</script>';
    unset($_GET['op']);
    unset($_GET['c2_ids']);
}else if($_GET['op']=='delete_c2'  && !empty($_GET['c2_id'])){
    $nns_ids = explode(',',$_GET['c2_id']);
    c2_task_model::delete_c2($nns_ids);

    echo '<script>alert("删除指令成功");</script>';
    unset($_GET['op']);
    unset($_GET['c2_id']);
}
if($_GET['action']=='search'){
    $_GET['nns_task_name'] = trim($_GET['nns_task_name']);
    $filter['nns_task_type'] = $_GET['nns_task_type'];
    $filter['nns_action'] = $_GET['nns_action'];
    $filter['nns_state'] = $_GET['nns_state'];
    $filter['nns_task_name'] = $_GET['nns_task_name'];
    $filter['nns_id'] = $_GET['nns_id'];
    $filter['day_picker_start'] = $_GET['day_picker_start'];
    $filter['day_picker_end'] = $_GET['day_picker_end'];
    $filter['notify_time_start'] = $_GET['notify_time_start'];
    $filter['notify_time_end'] = $_GET['notify_time_end'];
    $filter['nns_cdn_send_time'] = $_GET['nns_cdn_send_time'];
    $filter['nns_result'] = $_GET['nns_result'];
}
$result = \nl_file_task_log::get_file_log_list($dc, $sp_id,$filter,$offset,$limit);

$vod_total_num = $result['rows'];//记录条数

$vod_pages = ceil($vod_total_num / $page_size);//总页数
if($page_num>$vod_pages){
    $page_num = $vod_pages;
}


$currentpage = $page_num;
$g_manager_list_max_num = $page_size;
unset($_GET['page']);
$nns_url = "&".http_build_query($_GET);//"&view_list_max_num=$page_size&sp_id=$sp_id";
$refresh = "?".http_build_query($_GET);//"?page=$page_num&sp_id=$sp_id&view_list_max_num=";

$data = $result['data'];

if (isset($_GET['export']) && $_GET['export'] == 1) {
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment;filename=c2_task_log.csv');
    $fp = fopen('php://output', 'w');
    fwrite($fp, "\xEF\xBB\xBF");
    if (is_array($data)) {
        foreach ($data as $item0) {
            $name = $item0['nns_task_name'];
            $cols = array();
            $cols[] = $item0['nns_id'];
            $cols[] = $name;

            $cols[] = $item0['nns_task_type'];
            $cols[] = $item0['nns_action'];
            $cols[] = $item0['nns_send_time'];
            fputcsv($fp, $cols);
        }
    }
    fclose($fp);
    exit ;
}
function is_json($string)
{
    if(!is_string($string))
    {
        return false;
    }
    $string = strlen($string) <1 ? '' : $string;
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}

ob_end_clean();
function pub_func_get_desc_alt($title,$item)
{
    $alt_html = '<div style="width:300px;padding:5px;"><div style="font-size:16px;font-weight:bold;line-height:22px;"></div>';
    $alt_html.='<div style="font-size:12px;line-height:20px;">' .
        '<div style="float:left;margin:0px 5px 5px 0px;display:block;">';
    $alt_html.='</div>';

    if(strlen($item) >0 && is_json($item))
    {
        $alt_html.='<p><b>注入信息：</b><br/>';
        $item = json_decode($item,true);
        if(is_array($item))
        {
            foreach ($item as $key=>$val)
            {
                $alt_html.="{$key} => {$val}<br/>";
            }
        }
    }
    else
    {
        $alt_html.='<p><b>错误信息：</b><br/>';
        $alt_html.="{$item}<br/>";
    }
    $alt_html.='</p>';
    $alt_html.='</div></div>';
    return htmlspecialchars($alt_html);
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
    <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
    <script language="javascript" src="../../js/cms_cookie.js"></script>
    <script language="javascript" src="../../js/table.js.php"></script>
    <script language="javascript" src="../../js/checkinput.js.php"></script>
    <script language="javascript" src="../../js/rate.js"></script>
    <script language="javascript" src="../../js/image_loaded_func.js"></script>
    <script language="javascript" src="../../js/cms_datepicker.js"></script>
    <script language="javascript">
        function refresh_vod_page() {

            var num = $("#nns_list_max_num").val();
            setCookie("page_max_num",num);
            //window.parent.refresh_vod_page_num(num);
            window.location.href='<?php echo $refresh;?>&view_list_max_num='+num
        }


        function export_data() {
            window.location.href='<?php echo $refresh; ?>&export=1&view_list_max_num=<?php echo $vod_total_num;?>';
        }



        function checkhiddenBox(type) {
            BoxKey = false;
            $("input.checkhiddenInput:checked").each(function() {
                if ($(this).attr('rel') == type) {

                    BoxKey = true;
                    return false;
                }

            })
            return BoxKey;
        }



        function resend_c2_more(){
            var id=getAllCheckBoxSelect();
            if (id==""){
                alert("请选择至少选择一项");
            }else{
                var r=confirm("是否重发");
                if(r == true){
                    window.location.href="nncms_content_file_log_list.php?op=resend_more&sp_id=<?php echo $sp_id;?>&c2_ids="+id+"<?php echo $nns_url . $search_url; ?>";
                }
            }
        }
        function delete_c2(id){
            var r=confirm("是否删除");
            if(r == true){
                window.location.href="nncms_content_file_log_list.php?op=delete_c2&c2_id="+id+"<?php echo $nns_url . $search_url; ?>";
            }
            return ;

        }
        function delete_c2_more(){
            var id=getAllCheckBoxSelect();
            if (id==""){
                alert("请选择至少选择一项");
            }else{
                var r=confirm("是否删除");
                if(r == true){
                    window.location.href="nncms_content_file_log_list.php?op=delete_c2&c2_id="+id+"<?php echo $nns_url . $search_url; ?>";
                }
            }
        }
        $(document).ready(function() {
            window.parent.now_frame_url = window.location.href;

            $('#clear_time').click(function(){
                $('#day_picker_start').val('');
                $('#day_picker_end').val('');

            });

            $('#clear_time2').click(function(){
                $('#notify_time_start').val('');
                $('#notify_time_end').val('');

            });
        });

    </script>
</head>

<body>
<div class="content">
    <div class="content_position">
        注入CDN命令列表
    </div>
    <div class="content_table">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <form action="" method="get">
                <tbody>
                <tr>
                    <td>
                        名称：&nbsp;&nbsp;<input type="text" name="nns_task_name" id="nns_task_name" value="<?php echo $_GET['nns_task_name'];?>" style="width:200px;">
                        &nbsp;&nbsp; ID:&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="nns_id" id="nns_id" value="<?php echo $_GET['nns_id'];?>" style="width:200px;">

                        <input type="hidden" name="sp_id" value="<?php echo $sp_id;?>" />
                        <input type="hidden" name="action" value="search" />

                        &nbsp;&nbsp; 对象：&nbsp;&nbsp;&nbsp;&nbsp;
                        <select name="nns_task_type" style="width: 80px;">
                            <option value="">全部</option>
                            <option value="Series" <?php if(isset($_GET['nns_task_type']) && $_GET['nns_task_type']=='Series') echo 'selected="selected"';?>>Series</option>
                            <option value="Program" <?php if(isset($_GET['nns_task_type']) && $_GET['nns_task_type']=='Program') echo 'selected="selected"';?>>Program</option>
                            <option value="Movie" <?php if(isset($_GET['nns_task_type']) && $_GET['nns_task_type']=='Movie') echo 'selected="selected"';?>>Movie</option>
                            <option value="Channel" <?php if(isset($_GET['nns_task_type']) && $_GET['nns_task_type']=='Channel') echo 'selected="selected"';?>>Channel</option>
                            <option value="PhysicalChannel" <?php if(isset($_GET['nns_task_type']) && $_GET['nns_task_type']=='PhysicalChannel') echo 'selected="selected"';?>>PhysicalChannel</option>
                            <option value="Schedule" <?php if(isset($_GET['nns_task_type']) && $_GET['nns_task_type']=='Schedule') echo 'selected="selected"';?>>Schedule</option>
                            <option value="Category" <?php if(isset($_GET['nns_task_type']) && $_GET['nns_task_type']=='Category') echo 'selected="selected"';?>>Category</option>
                            <option value="Maping" <?php if(isset($_GET['nns_task_type']) && $_GET['nns_task_type']=='Maping') echo 'selected="selected"';?>>Maping</option>
                            <option value="QueryPlay" <?php if(isset($_GET['nns_task_type']) && $_GET['nns_task_type']=='QueryPlay') echo 'selected="selected"';?>>QueryPlay</option>
                            <option value="Package" <?php if(isset($_GET['nns_task_type']) && $_GET['nns_task_type']=='Package') echo 'selected="selected"';?>>Package</option>


                        </select>

                        操作：&nbsp;&nbsp;
                        <select name="nns_action" style="width: 80px;">
                            <option value="">全部</option>
                            <option value="REGIST" <?php if(isset($_GET['nns_action']) && $_GET['nns_action']=='REGIST') echo 'selected="selected"';?>>REGIST</option>
                            <option value="UPDATE" <?php if(isset($_GET['nns_action']) && $_GET['nns_action']=='UPDATE') echo 'selected="selected"';?>>UPDATE</option>
                            <option value="DELETE" <?php if(isset($_GET['nns_action']) && $_GET['nns_action']=='DELETE') echo 'selected="selected"';?>>DELETE</option>
                            <option value="ONLINE" <?php if(isset($_GET['nns_action']) && $_GET['nns_action']=='ONLINE') echo 'selected="selected"';?>>ONLINE</option>
                            <option value="OFFLINE" <?php if(isset($_GET['nns_action']) && $_GET['nns_action']=='OFFLINE') echo 'selected="selected"';?>>OFFLINE</option>
                            <option value="DELIVERY_ADD" <?php if(isset($_GET['nns_action']) && $_GET['nns_action']=='ONLINE') echo 'selected="selected"';?>>DELIVERY_ADD</option>
                            <option value="DELIVERY_DEL" <?php if(isset($_GET['nns_action']) && $_GET['nns_action']=='OFFLINE') echo 'selected="selected"';?>>DELIVERY_DEL</option>
                        </select>&nbsp;&nbsp;发送状态：&nbsp;&nbsp;<select name="nns_result" style="width: 80px;">
                            <option value="">全部</option>
                            <option value="[0]" <?php if(isset($_GET['nns_result']) && ($_GET['nns_result']=='[0]' || $_GET['nns_result']=='[5]')) echo 'selected="selected"';?>>发送成功</option>
                            <option value="[1]" <?php if(isset($_GET['nns_result']) && strlen($_GET['nns_result']) >0 && $_GET['nns_result']!='[0]') echo 'selected="selected"';?>>发送失败</option>
                        </select>
                        </select>&nbsp;&nbsp;状态：&nbsp;&nbsp;<select name="nns_state" style="width: 80px;">
                            <option value="">全部</option>
                            <option value="request_state_succ" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='request_state_succ') echo 'selected="selected"';?>>发送成功</option>
                            <option value="request_state_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='request_state_fail') echo 'selected="selected"';?>>发送失败</option>
                            <option value="notify_state_wait" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='notify_state_wait') echo 'selected="selected"';?>>等待通知</option>
                            <option value="notify_state_succ" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='notify_state_succ') echo 'selected="selected"';?>>通知成功</option>
                            <option value="notify_state_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='notify_state_fail') echo 'selected="selected"';?>>通知失败</option>
                            <option value="resend_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='resend_fail') echo 'selected="selected"';?>>重发失败</option>
                        </select>
                        &nbsp;&nbsp; CDN发送次数：&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="nns_cdn_send_time" id="nns_cdn_send_time" value="<?php echo $_GET['nns_cdn_send_time'];?>" style="width:80px;">
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;创建时间：&nbsp;&nbsp;
                        <input name="day_picker_start" id="day_picker_start" type="text"  value="<?php if(isset($_GET['day_picker_start'])) echo $_GET['day_picker_start'];?>" style="width:120px;" class="datetimepicker" callback="test" />
                        - <input name="day_picker_end" id="day_picker_end" type="text"  value="<?php if(isset($_GET['day_picker_end'])) echo $_GET['day_picker_end'];?>" style="width:120px;" class="datetimepicker" callback="test" />
                        &nbsp;&nbsp;
                        <input type="button" id="clear_time" name="clear_time" value="清除时间"/>&nbsp;&nbsp;
                        &nbsp;&nbsp;通知时间：&nbsp;&nbsp;
                        <input name="notify_time_start" id="notify_time_start" type="text"  value="<?php if(isset($_GET['notify_time_start'])) echo $_GET['notify_time_start'];?>" style="width:120px;" class="datetimepicker" callback="test" />
                        - <input name="notify_time_end" id="notify_time_end" type="text"  value="<?php if(isset($_GET['notify_time_end'])) echo $_GET['notify_time_end'];?>" style="width:120px;" class="datetimepicker" callback="test" />
                        &nbsp;&nbsp;
                        <input type="button" id="clear_time2" name="clear_time2" value="清除时间"/>&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="submit" value="<?php echo cms_get_lang('search'); ?>"  onclick="refresh_vod_page();"/>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="button" value="导出CSV格式"  onclick="export_data();" />
                    </td>
                </tr>
                </tbody>
            </form>
        </table>
    </div>
    <div class="content_table formtable">

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <th><input name="" type="checkbox" value="">序号</th>
                <th>ID</th>
                <th>名称</th>
                <th>对象</th>
                <th>操作</th>
                <th>创建时间</th>
                <th>发送状态</th>
                <th>发送时间</th>
                <th>通知状态</th>
                <th>反馈失败原因</th>
                <th>CDN发送次数</th>
                <th>通知时间</th>
                <th>操作</th>

            </tr>
            </thead>
            <tbody>
            <?php
            if($data!=null){
                $num = ($page_num - 1) * $page_size;
                foreach ($data as $item) {
                    $num++;
                    ?>
                    <tr>
                        <td><input name="input" type="checkbox" value="<?php echo $item["nns_id"];?>" /><?php echo $num; ?></td>
                        <td>
                            <?php echo $item['nns_id'];?>
                        </td>
                        <td>


                            <?php $arr_desc_info = pub_func_get_desc_alt('错误信息:',$item['nns_desc']);?>
                            <alt alt="<?php echo $arr_desc_info; ?>">
                                <?php echo $item['nns_task_name']; ?>
                            </alt></td>
                        <td>
                            <?php echo $item['nns_task_type'];?>
                        </td>

                        <td>
                            <?php echo $item['nns_action'];?>
                        </td>
                        <td>
                            <?php echo $item['nns_create_time'];?>
                        </td>
                        <td>
                            <?php if(false!== strpos($item['nns_result'],'[0]')) {echo '成功';}
                            else if(false!== strpos($item['nns_result'],'[5]')){echo '发送成功,等待反馈'.$item['nns_result'];}
                            else {echo '<font color="red">失败</font>'.$item['nns_result'];} ?>
                            <?php if($item['nns_again']=='-1' && $item['nns_notify_result']=='-1') echo '&nbsp;&nbsp;|&nbsp;<font color="red">重发失败</font>';?>
                        </td>
                        <td>
                            <?php echo $item['nns_send_time'];?>
                        </td>
                        <td>
                            <?php if(strlen($item['nns_notify_result']) > 0) {if($item['nns_notify_result'] == '0')
                            {echo '成功';}
                            else if($item['nns_notify_result'] == '5'){echo '正在等待反馈'.$item['nns_result'];}
                            else {echo '<font color="red">失败['.$item['nns_notify_result'].']</font>';}}?>
                        </td>
                        <td>
                            <?php echo $item['nns_notify_fail_reason'];?>
                        </td>
                        <td>
                            <?php echo $item['nns_cdn_send_time'];?>
                        </td>
                        <td>
                            <?php echo $item['nns_notify_time'];?>
                        </td>
                        <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                                <a  title="<?php echo $item['nns_url']; ?>" href="<?php echo $g_bk_web_url . "/data/" . $item['nns_url']; ?>" target="_blank">查看发送内容</a>
                            <?php
                            if($item['nns_notify_result_url'] && $item['nns_notify_result_url'] !== 'NULL')
                            {?>
                                <a href="<?php echo $g_bk_web_url . "/data/" . $item['nns_notify_result_url'];?>"
                                    target="_blank"
                                    title="<?php echo $item['nns_notify_result_url'];?>">查看通知结果</a>
                            <?php }?>
                        </td>

                    </tr>
                    <?php
                }
            }
            $least_num = $g_manager_list_max_num - count($data);
            for ($i = 0; $i < $least_num; $i++) {
                ?>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="pagecontrol">
        共<span style="font-weight: bold; color: #ff0000;"><?php echo $vod_total_num;?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
        <?php if ($currentpage > 1) { ?>
            <a
                href="nncms_content_file_log_list.php?page=1<?php echo $nns_url . $search_url; ?>"
                target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a
                href="nncms_content_file_log_list.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>"
                target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php } else { ?>
            <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
            <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php } if ($currentpage < $vod_pages) { ?>
            <a
                href="nncms_content_file_log_list.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>"
                target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a
                href="nncms_content_file_log_list.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>"
                target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php } else { ?>
            <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
            <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php } ?>

        <?php echo cms_get_lang('jump_to'); ?> <input
            name="go_page_num" id="go_page_num"
            value="<?php echo $currentpage; ?>" type="text" style="width: 20px;" /> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
        <a
            href="javascript:go_page_num('nncms_content_file_log_list.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
        <?php echo cms_get_lang('current'); ?><span
            style="font-weight: bold; color: #ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
        <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
        <input name="nns_list_max_num"
               id="nns_list_max_num" type="text"
               value="<?php echo $g_manager_list_max_num; ?>" style="width: 24px;" />&nbsp;&nbsp;
        <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
               onclick="refresh_vod_page();" />&nbsp;&nbsp;

    </div>

    <div class="controlbtns">
        <!--<div class="controlbtn allselect">-->
        <!--    <a href="javascript:selectAllCheckBox(true);">全选</a>-->
        <!--</div>-->
        <!--<div class="controlbtn cancel">-->
        <!--    <a href="javascript:selectAllCheckBox(false);">取消</a>-->
        <!--</div>-->
        <!--<div class="controlbtn add">-->
        <!--    <a href="javascript:resend_c2_more();">重发</a>-->
        <!--</div>-->
        <!--<div class="controlbtn delete"><a href="javascript:delete_c2_more();" >删除</a></div>-->
        <div style="clear: both;"></div>
    </div>
</div>
</body>
</html>

