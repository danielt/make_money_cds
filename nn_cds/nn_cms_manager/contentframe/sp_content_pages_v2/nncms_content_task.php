<?php 
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include $nncms_config_path . "v2/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.cdn_import.cdn_import_queue");
\ns_core\m_load::load("ns_model.epg_import.epg_import_queue");
\ns_core\m_load::load("ns_data_model.command.m_command_task_log_inout");
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135004");
$checkpri = null;
$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$group_name = $_GET["group_name"];
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}
$sp_id = $_REQUEST['sp_id'];

global $g_bk_version_number;
$bk_version_number = $g_bk_version_number == '1' ? true : false;
unset($g_bk_version_number);
if($bk_version_number && isset($_GET['op']))
{
    if ($_GET['op'] == 'resend_c2_task' || $_GET['op'] == 'task_action') //CDN注入
    {
        \m_config::$flag_operation = false;
        $log_timer_path = \m_config::return_child_path($nncms_config_path . "v2/ns_timer/delivery/delivery_cdn.timer.php", 'ns_timer');

        $ids = ($_GET['op'] == 'task_action') ? explode(',', $_GET['ids']) : array($_GET['c2_task_id']);
        $ids = (is_array($ids) && !empty($ids)) ? array_filter($ids) : $ids;
        if (!is_array($ids) || empty($ids))
        {
            echo "<script>alert('传入查询ID为空');history.go(-1);</script>";
            die;
        }
        //获取C2执行任务
        $result_task = nl_c2_task::query_by_condition_v2(m_config::get_dc(), array('nns_id' => $ids, 'nns_org_id' => $sp_id));

        if ($result_task['ret'] != NS_CDS_SUCCE)
        {
            echo "<script>alert('数据库查询数据失败');history.go(-1);</script>";
            die;
        }
        if (empty($result_task['data_info']) || !is_array($result_task['data_info']))
        {
            echo "<script>alert('数据库查询无数据');history.go(-1);</script>";
            die;
        }
        $project = evn::get("project");
        \ns_core\m_load::load("ns_api.{$project}.delivery.{$sp_id}.delivery");
        $explain_inst = new delivery($sp_id);
        foreach ($result_task['data_info'] as $task)
        {
            //实例化底层数据基础类
            $cdn_import_queue = new ns_model\cdn_import\cdn_import_queue($sp_id, $task['nns_type']);
            //过滤注入数据
            $cdn_explain = $cdn_import_queue->explain();
            if ($cdn_explain['ret'] != NS_CDS_SUCCE)
            {
                \m_config::timer_write_log($log_timer_path, var_export($cdn_explain['reason'], true), $task['nns_type']);
                continue;
            }
            //根据C2任务获取注入信息
            $cdn_explain = $cdn_import_queue->query_mixed_info(array($task));
            if ($cdn_explain['ret'] != NS_CDS_SUCCE)
            {
                \m_config::timer_write_log($log_timer_path, var_export($cdn_explain, true), $task['nns_type']);
                continue;
            }
            if (!isset($cdn_explain['data_info']) || empty($cdn_explain['data_info']) || !is_array($cdn_explain['data_info']))
            {
                \m_config::timer_write_log($log_timer_path, "查询消息队列数据为空，不在执行CDN注入", $task['nns_type']);
                continue;
            }
            \m_config::timer_write_log($log_timer_path, "执行CDN注入：" . var_export($cdn_explain['data_info'][0], true), $task['nns_type']);

            $explain_inst->init($cdn_explain['data_info'][0]);
            $cdn_explain = $explain_inst->explain();
            $explain_inst->destroy_init();
            $re_w_log = \m_config::write_cdn_execute_log("***********执行CDN指令结果：".var_export($cdn_explain,true), $sp_id, $task['nns_id']);

            if ($cdn_explain['ret'] !=0 )
            {
                $command_task_log_params = array(
                    'base_info' => array(
                        'nns_command_task_id' => $task['base_info']['nns_command_task_id'],
                        'nns_status' => NS_CDS_COMMAND_CDN_FAIL,
                        'nns_desc' => var_export($cdn_explain,true),
                        'nns_execute_url' => $re_w_log['data_info']['base_dir'],
                    )
                );
                $obj_command_task_log = new m_command_task_log_inout();
                $obj_command_task_log->add($command_task_log_params);
                unset($obj_command_task_log);
            }
            \m_config::timer_write_log($log_timer_path, "执行CDN结果：" . var_export($cdn_explain, true), $task['nns_type']);
            unset($cdn_import_queue);
        }
        if(!isset($cdn_explain['reason']) || empty($cdn_explain['reason']))
        {
            $cdn_explain['reason'] = $cdn_explain['ret'] != NS_CDS_SUCCE ? '操作失败' : '操作成功';
        }
        echo "<script>alert('" . $cdn_explain['reason'] . "');history.go(-1);</script>";
        die;
    }
    elseif ($_GET['op'] == 'epg_action') //EPG注入
    {
        \m_config::$flag_operation = false;
        $log_timer_path = \m_config::return_child_path($nncms_config_path . "v2/ns_timer/delivery/delivery_epg.timer.php", 'ns_timer');

        $ids = explode(',', $_GET['ids']);
        $ids = (is_array($ids) && !empty($ids)) ? array_filter($ids) : $ids;
        if (!is_array($ids) || empty($ids))
        {
            echo "<script>alert('传入查询ID为空');history.go(-1);</script>";
            die;
        }
        //获取C2执行任务
        $result_task = nl_c2_task::query_by_condition_v2(m_config::get_dc(), array('nns_id' => $ids, 'nns_org_id' => $sp_id));
        if ($result_task['ret'] != NS_CDS_SUCCE)
        {
            echo "<script>alert('数据库查询数据失败');history.go(-1);</script>";
            die;
        }
        if (empty($result_task['data_info']) || !is_array($result_task['data_info']))
        {
            echo "<script>alert('数据库查询无数据');history.go(-1);</script>";
            die;
        }
        $project = evn::get("project");
        //开发自写注入EPG，可能是其他EPG方
        if(\ns_core\m_load::load("ns_api.{$project}.delivery.{$sp_id}.epg_delivery"))
        {
            $explain_inst = new epg_delivery($sp_id);
        }
        else
        {
            //视达科标准EPG注入
            \ns_core\m_load::load("ns_model.delivery.epg_delivery_explain");
            $explain_inst = new \ns_model\delivery\epg_delivery_explain($sp_id);
        }
        foreach ($result_task['data_info'] as $task)
        {
            //实例化底层数据基础类
            $epg_import_queue = new ns_model\epg_import\epg_import_queue($sp_id, $task['nns_type']);
            //过滤注入数据
            $epg_explain = $epg_import_queue->explain();
            if ($epg_explain['ret'] != NS_CDS_SUCCE)
            {
                \m_config::timer_write_log($log_timer_path, var_export($cdn_explain['reason'], true), $task['nns_type']);
                continue;
            }
            //根据C2任务获取注入信息
            $epg_explain = $epg_import_queue->query_mixed_info(array($task));
            if ($epg_explain['ret'] != NS_CDS_SUCCE)
            {
                \m_config::timer_write_log($log_timer_path, var_export($epg_explain, true), $task['nns_type']);
                continue;
            }
            if (!isset($epg_explain['data_info']) || empty($epg_explain['data_info']) || !is_array($epg_explain['data_info']))
            {
                \m_config::timer_write_log($log_timer_path, "查询消息队列数据为空，不在执行CDN注入", $task['nns_type']);
                continue;
            }
            \m_config::write_epg_execute_log("***********执行EPG注入开始：".var_export($epg_explain,true), $sp_id, $task['nns_id']);
            \m_config::timer_write_log($log_timer_path, "执行EPG注入：" . var_export($epg_explain['data_info'][0], true), $task['nns_type']);
            $explain_inst->init($epg_explain['data_info'][0]);
            $epg_explain = $explain_inst->explain();
            $explain_inst->destroy_init();
            \m_config::timer_write_log($log_timer_path, "执行EPG结果：" . var_export($epg_explain, true), $task['nns_type']);
            $re_w_log = m_config::write_epg_execute_log("***********执行EPG结果：".var_export($epg_explain,true), $sp_id, $task['nns_id']);
            if ($epg_explain['ret'] != 0)
            {
                $command_status = NS_CDS_COMMAND_EPG_FAIL;
            }
            else
            {
                $command_status = NS_CDS_COMMAND_EPG_SUCCESS;
            }
            $command_task_log_params = array(
                'base_info' => array(
                    'nns_command_task_id' => $task['base_info']['nns_command_task_id'],
                    'nns_status' => $command_status,
                    'nns_desc' => var_export($epg_explain,true),
                    'nns_execute_url' => $re_w_log['data_info']['base_dir'],
                )
            );
            $obj_command_task_log = new m_command_task_log_inout();
            $obj_command_task_log->add($command_task_log_params);
            unset($obj_command_task_log);
            unset($epg_import_queue);
        }

        if(!isset($epg_explain['reason']) || empty($epg_explain['reason']))
        {
            $epg_explain['reason'] = $epg_explain['ret'] != NS_CDS_SUCCE ? '操作失败' : '操作成功';
        }
        echo "<script>alert('" . $epg_explain['reason'] . "');</script>";
        echo "<script>history.go(-1);</script>";
        die;
    }
    elseif($_REQUEST['op'] == 'export_cdning_media_action')//导出正在注入CDN的片源数据
    {
        ignore_user_abort(true);
        set_time_limit(0);
        ob_implicit_flush();
        $project = evn::get("project");
        \ns_core\m_load::load("ns_api.{$project}.delivery.{$sp_id}.export_cdn_media");
        $obj_export_cdn_media = new export_cdn_media();
        $arr_ids = strlen($_REQUEST['ids']) > 0 ? explode(',',trim($_REQUEST['ids'],',')) : array();
        $arr_export_ret = $obj_export_cdn_media->init($arr_ids);
        unset($arr_ids); unset($obj_export_cdn_media);
        if($arr_export_ret['ret'] == 0)
        {
            echo "<script>alert('" . $arr_export_ret['reason'] . "');history.go(-1);</script>";
        }
        else
        {
            echo "<script>alert('" . $arr_export_ret['reason'] . "');</script>";
        }
        ob_flush();
        flush();
        die;
    }
}

include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/vod_media/vod_media.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/sp/sp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/c2_task/c2_task.class.php';
$dc = nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));


$result_cp_data = nl_query_by_db("select nns_id,nns_name from nns_cp", $dc->db());
$result_cp_data_keys = $result_cp_id_data = array();
if(is_array($result_cp_data))
{
    foreach ($result_cp_data as $cp_val)
    {
        $result_cp_id_data[$cp_val['nns_id']] = $cp_val['nns_name'];
    }
    $result_cp_data_keys = array_keys($result_cp_id_data);
}


$sp_info = nl_sp::query_by_id($dc,$sp_id);
$sp_info= (isset($sp_info['data_info'][0]['nns_config']) && !empty($sp_info['data_info'][0]['nns_config'])) ? json_decode($sp_info['data_info'][0]['nns_config'],true) : null;

$cdn_notify_third_party_enable= false;
if(isset($sp_info['cdn_notify_third_party_enable']) && $sp_info['cdn_notify_third_party_enable'] =='1' && isset($sp_info['cdn_notify_third_party_mark']) && strlen($sp_info['cdn_notify_third_party_mark'])>0)
{
    $cdn_notify_third_party_enable = true;
}

require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php'; 
if (isset($_GET['op'])) {
	if ($_GET['op'] == 'resend_c2_task') {
		$query = array('c2_task_id' => $_GET['c2_task_id'], 'action' => $_GET['action'], 'status' => SP_C2_WAIT, );
		$re = content_task_model::resend_c2_task($query);
		if ($re) {
			$re_str = '操作成功';
		} else {
			$re_str = '操作失败';
		}
		echo '<script>alert("' . $re_str . '");history.go(-1);</script>';
	} 
	elseif ($_GET['op'] == 'epg_action') 
	{
	    $reason = '';
		if(isset($sp_info['epg_import_model']) && $sp_info['epg_import_model'] == '1')
        {
            include_once dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv_v2/models/import_model_v2.php';
            $query = array_filter(explode(',', $_GET['ids']));
            $obj_import_model = new import_model_v2();
            $re=$obj_import_model->import_epg($query);
        }
        else
        {
    		$query = array('nns_id' => $_GET['ids'], 'nns_action' => $_GET['action'], );
    		$re = import_model::import_epg($query);
        }
		if ($re) {
			$re_str = '操作成功';
		} else {
			$re_str = '操作失败';
		}
		echo '<script>alert("' . $re_str . '");history.go(-1);</script>';
	}elseif($_GET['op']=='add_queue'){
		$query = $_GET['ids'];
		$re = content_task_model::add_queue($query);
		if ($re) {
			$re_str = '操作成功';
		} else {
			$re_str = '操作失败';
		}
		echo '<script>alert("' . $re_str . '");history.go(-1);</script>';
	}elseif($_GET['op']=='query_queue'){
		$ids = $_GET['ids'];
		$ids = array_filter(explode(',', $ids));
		foreach($ids as $id){
			content_task_model::do_query_media_url($sp_id,$id);
		}		
		echo '<script>alert("' . $re_str . '");history.go(-1);</script>';
	}elseif($_GET['op']=='cancel_queue'){
		$query = $_GET['ids'];
		$re = content_task_model::cancel_c2_queue($query);
		if ($re) {
			$re_str = '操作成功';
		} else {
			$re_str = '操作失败';
		}
		echo '<script>alert("' . $re_str . '");history.go(-1);</script>';
	}
	elseif ($_GET['op'] == 'task_action') {
		$action = $_GET['action'];
		$ids = $_GET['ids'];
		$re = content_task_model::resend_c2_task_all($action, $ids);

		if ($re) {
			$re_str = '操作成功';
		} else {
			$re_str = '操作失败';
		}
		echo '<script>alert("' . $re_str . '");history.go(-1);</script>';
	}
	else if($_GET['op'] == 'task_action_line')
	{
	    $arr = explode(',', $_GET['ids']);
	    foreach ($arr as $val)
	    {
	       $re = content_task_model::resend_c2_task_line($dc, $val,$sp_info,$sp_id);
	    }
	    if ($re) {
			$re_str = '操作成功';
		} else {
			$re_str = '操作失败';
		}
		echo '<script>alert("' . $re_str . '");history.go(-1);</script>';
	}
	else if($_GET['op'] == 'notify_third_party')
	{
	    include_once dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv_v2/models/c2_task_model.php';
	    $arr = explode(',', $_GET['ids']);
	    foreach ($arr as $val)
	    {
	        $result = c2_task_model::notify_upstream_third($val,$sp_info,$dc,$val);
	        nl_c2_task::edit($dc, array('nns_notify_third_state'=>($result['ret'] == '0') ? '1' : '2'), $val);
	    }
        echo '<script>alert("操作成功");history.go(-1);</script>';
	}
	else if($_GET['op'] == 'task_action_delivery')
	{
	    $arr = explode(',', $_GET['ids']);
	    foreach ($arr as $val)
	    {
	        $re = content_task_model::resend_c2_task_delivery($dc, $val,$sp_info,$sp_id);
	    }
	    if ($re) {
	        $re_str = '操作成功';
	    } else {
	        $re_str = '操作失败';
	    }
	    echo '<script>alert("' . $re_str . '");history.go(-1);</script>';
	}
	else if($_GET['op'] == 'task_action_playurl')
	{
	    $arr = explode(',', $_GET['ids']);
	    foreach ($arr as $val)
	    {
	        $re = content_task_model::resend_c2_task_playurl($dc, $val,$sp_info,$sp_id);
	    }
	    if ($re) {
	        $re_str = '操作成功';
	    } else {
	        $re_str = '操作失败';
	    }
	    echo '<script>alert("' . $re_str . '");history.go(-1);</script>';
	}
	unset($_GET['op']);
	unset($_GET['c2_task_id']);
}
if (isset($_GET['epg'])) {
	if ($_GET['epg'] == 'import') {
		$query = array('action' => $_GET['action'], 'nns_id' => $_GET['nns_id'], );
		$re = import_model::import_epg($query);
		if ($re) {
			$re_str = '操作成功';
		} else {
			$re_str = '操作失败';
		}
		echo '<script>alert("' . $re_str . '");</script>';
	}
	unset($_GET['epg']);
	unset($_GET['action']);
	unset($_GET['nns_id']);
}



$cdn_display_import = isset($sp_info['cdn_display_import'])&&(int)$sp_info['cdn_display_import']===1 ? 1 : 0;
$cdn_display_import_line = (isset($sp_info['cdn_query_mediaurl'])&&(int)$sp_info['cdn_query_mediaurl']===1 && isset($sp_info['cdn_send_cdi_mode_url']) && strlen($sp_info['cdn_send_cdi_mode_url']) >0) ? 1 : 0;
$epg_display_import = isset($sp_info['epg_display_import'])&&(int)$sp_info['epg_display_import']===1 ? 1 : 0;
$cdn_fail_time_display = isset($sp_info['cdn_fail_retry_mode']) && strlen($sp_info['cdn_fail_retry_mode']) && $sp_info['cdn_fail_retry_mode']==0 ? 0 : 1;
$epg_display_import_delete = isset($sp_info['epg_display_import_delete'])&&(int)$sp_info['epg_display_import_delete']===1 ? 1 : 0;

$cdn_display_import_delivery = (isset($sp_info['c2_import_asset_url']) && strlen($sp_info['c2_import_asset_url']) >0) ? 1 : 0;

$cdn_display_third_play_url = (isset($sp_info['c2_get_third_play_url']) && strlen($sp_info['c2_get_third_play_url']) >0) ? 1 : 0;


$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : (!empty($_COOKIE["page_max_num"]) ?  $_COOKIE["page_max_num"] : 18);
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) {
	$page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit = $page_size;

$filter = null;
if ($_GET['actionsearch'] == 'search') {
	$_GET['nns_name'] = trim($_GET['nns_name']);
	$filter['nns_name'] = $_GET['nns_name'];
	$filter['nns_type'] = $_GET['nns_type'];
	$filter['nns_state'] = $_GET['nns_state'];
	$filter['nns_action'] = $_GET['nns_action_state'];
	$filter['day_picker_start'] = $_GET['day_picker_start'];
	$filter['day_picker_end'] = $_GET['day_picker_end'];
	$filter['nns_epg_state'] = $_GET['nns_epg_state'];
	$filter['nns_cdn_fail_time'] = $_GET['nns_cdn_fail_time'];
    $filter['nns_ref_id'] = $_GET['nns_ref_id'];
    $filter['nns_src_id'] = $_GET['nns_src_id'];
	if(isset($_GET['nns_cp_id']) && strlen($_GET['nns_cp_id'])>0)
	{
	    $filter['nns_cp_id'] = $_GET['nns_cp_id'];
	}
}
$result = content_task_model::get_vod_task_list($sp_id, $filter, $offset, $limit);

$vod_total_num = $result['rows'];
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) {
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);
$data = $result['data'];
if (isset($_GET['export']) && $_GET['export'] == 1) {
	header('Content-Type: text/csv');
	header('Content-Disposition: attachment;filename=c2_task.csv');
	$fp = fopen('php://output', 'w');
	fwrite($fp, "\xEF\xBB\xBF");
	if (is_array($data)) {
		foreach ($data as $item0) {			
			$name = $item0['nns_name'];
			$cols = array();
			$cols[] = $item0['nns_ref_id'];
			$cols[] = $name;
			$nns_type = '';
			switch($item0['nns_type']) {
				case 'video' :
					$nns_type = '影片';
					break;
				case 'index' :
					$nns_type = '分集';
					break;
				case 'live' :
					$nns_type = '直播频道';
					break;
				case 'live_media' :
					$nns_type = '直播源';
					break;
				case 'playbill' :
					$nns_type = '节目单';
					break;
				case 'media' :
					$nns_type = '片源';
					break;
				case 'file' :
				    $nns_type = '包文件';
				    break;
			    case 'product' :
			        $nns_type = '产品包';
			        break;
			}
			$cols[] = $nns_type;
			$cols[] = $item0['nns_action'];
			$cols[] = $item0['nns_create_time'];
			fputcsv($fp, $cols);
		}
	}
	fclose($fp);
	exit ;
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
		<script language="javascript">
		
			$(document).ready(function() {
				$(".selectbox").hide();
			});
			
			function close_select() {
				$(".selectbox").hide();
			}
			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
                setCookie("page_max_num",num);
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function export_data() {
				window.location.href='<?php echo $refresh; ?>&export=1&view_list_max_num=100000';
			}

			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}

				})
				return BoxKey;
			}

			function add_task_tmp(nns_id){
				$.get("nncms_content_task.php?sp_id=<?php echo $sp_id?>&ajax=add&nns_id="+nns_id,function(result){
					alert(result);window.location.reload();
					},'json');
			}

			function stop_task_tmp(nns_id){
				$.get("nncms_content_task.php?sp_id=<?php echo $sp_id?>&ajax=stop&nns_id="+nns_id,function(result){
					alert(result);window.location.reload();
					},'json');
			}

			function rsync_info(nns_id){
				$.get("nncms_content_task.php?sp_id=<?php echo $sp_id?>&ajax=rsync&nns_id="+nns_id,function(result){
					alert(result);window.location.reload();
					},'json');
			}
			function delete_vod(nns_id){
				$.get("nncms_content_task.php?sp_id=<?php echo $sp_id?>&ajax=delete_vod&nns_id="+nns_id,function(result){
					alert(result);window.location.reload();
					},'json');
			}
			function delete_index(nns_id){
				$.get("nncms_content_task.php?sp_id=<?php echo $sp_id?>&ajax=delete_vod_index&nns_id="+nns_id,function(result){
					alert(result);window.location.reload();
					},'json');
			}
			$(document).ready(function() {
				$('#clear_time').click(function(){
					$('#day_picker_start').val('');
					$('#day_picker_end').val('');

				});
				window.parent.now_frame_url = window.location.href;
				window.parent.resetFrameHeight();
			});

			function task_action(action){
				var ids=getAllCheckBoxSelect();	
				if(ids == '')
				{
					alert('请选择数据');
					return ;
				}
				var r=confirm("是否进行该操作");
				if(r == true){
					var url = "nncms_content_task.php?sp_id=<?php echo $sp_id?>&op=task_action&ids="+ids+"&action="+action;
					window.location.href = url;
				}
			}

            function task_action_v2(){
                var ids=getAllCheckBoxSelect();
                if(ids == '')
                {
                    alert('请选择数据');
                    return ;
                }
                var r=confirm("是否进行该操作");
                if(r == true){
                    var url = "nncms_content_task.php?sp_id=<?php echo $sp_id?>&op=task_action&ids="+ids;
                    window.location.href = url;
                }
            }

			function notify_third_party()
			{
				var ids=getAllCheckBoxSelect();	
				if(ids == '')
				{
					alert('请选择数据');
					return ;
				}
				var r=confirm("是否进行该操作");
				if(r == true){
					var url = "nncms_content_task.php?sp_id=<?php echo $sp_id?>&op=notify_third_party&ids="+ids;
					window.location.href = url;
				}
			}
			
			function task_action_line(){
				var ids=getAllCheckBoxSelect();	
				if(ids == '')
				{
					alert('请选择数据');
					return ;
				}
				var r=confirm("是否进行该操作");
				if(r == true){
					var url = "nncms_content_task.php?sp_id=<?php echo $sp_id?>&op=task_action_line&ids="+ids;
					window.location.href = url;
				}
			}
			
			function task_action_delivery(){
				var ids=getAllCheckBoxSelect();	
				if(ids == '')
				{
					alert('请选择数据');
					return ;
				}
				var r=confirm("是否进行该操作");
				if(r == true){
					var url = "nncms_content_task.php?sp_id=<?php echo $sp_id?>&op=task_action_delivery&ids="+ids;
					window.location.href = url;
				}
			}

			function task_action_playurl(){
				var ids=getAllCheckBoxSelect();	
				if(ids == '')
				{
					alert('请选择数据');
					return ;
				}
				var r=confirm("是否进行该操作");
				if(r == true){
					var url = "nncms_content_task.php?sp_id=<?php echo $sp_id?>&op=task_action_playurl&ids="+ids;
					window.location.href = url;
				}
			}
			
			function epg_action(action){
				var ids=getAllCheckBoxSelect();	
				if(ids == '')
				{
					alert('请选择数据');
					return ;
				}
				var r=confirm("是否进行该操作");
				if(r == true){
					var url = "nncms_content_task.php?sp_id=<?php echo $sp_id?>&op=epg_action&ids="+ids+"&action="+action;
						window.location.href = url;
				}
			}
			
			
			
			function add_queue(action){
				var ids=getAllCheckBoxSelect();	
				if(ids == '')
				{
					alert('请选择数据');
					return ;
				}
				var r=confirm("是否进行该操作");
				if(r == true){
					var url = "nncms_content_task.php?sp_id=<?php echo $sp_id?>&op=add_queue&ids="+ids+"&action="+action;
						window.location.href = url;
				}
			}
			
			function query_queue(){
				var ids=getAllCheckBoxSelect();	
				if(ids == '')
				{
					alert('请选择数据');
					return ;
				}
				var r=confirm("是否进行该操作");
				if(r == true){								
					var url = "nncms_content_task.php?sp_id=<?php echo $sp_id?>&op=query_queue&ids="+ids;
						window.location.href = url;
				}
			}

			function cancel_queue(){
				var ids=getAllCheckBoxSelect();	
				if(ids == '')
				{
					alert('请选择数据');
					return ;
				}	
				var r=confirm("是否进行该操作");
				if(r == true){							
					var url = "nncms_content_task.php?sp_id=<?php echo $sp_id?>&op=cancel_queue&ids="+ids;
						window.location.href = url;
				}
			}
			
			//查看插座列队的日志
			function show_queue_log(video_id,video_type,op_id,sp_id){
				$("#select_frame").attr("src", "./nncms_content_task_queue_log.php?sp_id="+sp_id+"&video_id="+video_id+"&video_type="+video_type+"&op_id="+op_id);
				$(".selectbox").show();
			}

            /**
             * 导出正在注入CDN的片源数据（导出文件CSV格式）
             */
            function export_media_data_cdning()
            {
                var ids=getAllCheckBoxSelect();
                var r=confirm("是否导出正在注入CDN的片源数据（导出文件CSV格式）");
                if(r == true){
                    var url = "nncms_content_task.php?sp_id=<?php echo $sp_id?>&op=export_cdning_media_action&ids="+ids;
                    window.location.href = url;
                }
            }
		</script>
	</head>

	<body>
<!-- 		<div class="selectbox"> -->
<!-- 			<iframe scrolling="no" frameborder="0" id="select_frame" ></iframe> -->
<!-- 		</div> -->
		<div class="content">
			<div class="content_position">
				注入管理  > 内容列表
			</div>
				<div class="content_table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<form action="" method="get">
							<tbody>
								<tr>
									<td>
										影片名/拼音：&nbsp;&nbsp;<input type="text" name="nns_name" id="nns_name" value="<?php echo isset($_GET['nns_name']) ? $_GET['nns_name'] : ''; ?>" style="width:200px;">
										&nbsp;&nbsp;资源ID：&nbsp;&nbsp;<input type="text" name="nns_ref_id"  value="<?php echo isset($_GET['nns_ref_id']) ? $_GET['nns_ref_id'] : ''; ?>" style="width:200px;">
                                        &nbsp;&nbsp;父级资源ID：&nbsp;&nbsp;<input type="text" name="nns_src_id"  value="<?php echo isset($_GET['nns_src_id']) ? $_GET['nns_src_id'] : ''; ?>" style="width:200px;">
                                        &nbsp;&nbsp;类型：&nbsp;&nbsp;<select name="nns_type" style="width: 150px;">
											<option value="" >全部</option>
											<option value="video" <?php if($_GET['nns_type']=='video') echo 'selected="selected"'?>>影片</option>
											<option value="index" <?php if($_GET['nns_type']=='index') echo 'selected="selected"'?>>分集</option>
											<option value="media" <?php if($_GET['nns_type']=='media') echo 'selected="selected"'?>>片源</option>
											<option value="live" <?php if($_GET['nns_type']=='live') echo 'selected="selected"'?>>直播频道</option>
											<option value="live_media" <?php if($_GET['nns_type']=='live_media') echo 'selected="selected"'?>>直播源</option>
											<option value="playbill" <?php if($_GET['nns_type']=='playbill') echo 'selected="selected"'?>>节目单</option>
											<option value="file" <?php if($_GET['nns_type']=='file') echo 'selected="selected"'?>>包文件</option>
											<option value="product" <?php if($_GET['nns_type']=='product') echo 'selected="selected"'?>>产品包</option>
									</select>
									&nbsp;&nbsp;操作：&nbsp;&nbsp;<select name="nns_action_state" style="width: 150px;">
											<option value="" >全部</option>
											<option value="add" <?php if($_GET['nns_action_state']=='add') echo 'selected="selected"'?>>添加</option>
											<option value="modify" <?php if($_GET['nns_action_state']=='modify') echo 'selected="selected"'?>>修改</option>
											<option value="destroy" <?php if($_GET['nns_action_state']=='destroy') echo 'selected="selected"'?>>删除</option>
									</select>
                                        &nbsp;&nbsp;CDN失败次数：&nbsp;&nbsp;<input type="text" name="nns_cdn_fail_time"  value="<?php echo isset($_GET['nns_cdn_fail_time']) ? $_GET['nns_cdn_fail_time'] : ''; ?>" style="width:60px;">
									<?php
									if($cdn_display_import=='0')
									{
									?>
                                    </td></tr>
                                    <tr><td>
                                        CDN状态：&nbsp;&nbsp;<select name="nns_state" style="width: 150px;">
											<option value="" >全部</option>											
											<option value="content_wait" <?php if($_GET['nns_state']=='content_wait') echo 'selected="selected"'?>>等待注入CDN</option>
											<option value="content_wait_cdi" <?php if($_GET['nns_state']=='content_wait_cdi') echo 'selected="selected"'?>>等待获取CDN播放串</option>
											<option value="content_wait_cdi_doing" <?php if($_GET['nns_state']=='content_wait_cdi_doing') echo 'selected="selected"'?>>正在获取CDN播放串</option>
											<option value="content_wait_cdi_fail" <?php if($_GET['nns_state']=='content_wait_cdi_fail') echo 'selected="selected"'?>>获取CDN播放串失败</option>
											<option value="content_doing" <?php if($_GET['nns_state']=='content_doing') echo 'selected="selected"'?>>正在注入CDN</option>
											<option value="content_ok" <?php if($_GET['nns_state']=='content_ok') echo 'selected="selected"'?>>注入CDN成功</option>
											<option value="content_fail" <?php if($_GET['nns_state']=='content_fail') echo 'selected="selected"'?>>注入CDN失败</option>
											<option value="content_cancel" <?php if($_GET['nns_state']=='content_cancel') echo 'selected="selected"'?>>注入CDN取消</option>
											<option value="ftp_connet_fail" <?php if($_GET['nns_state']=='ftp_connet_fail') echo 'selected="selected"'?>>ftp连接失败</option>
											<option value="ftp_clip_get_fail" <?php if($_GET['nns_state']=='ftp_clip_get_fail') echo 'selected="selected"'?>>ftp片源获取失败</option>
											
											
											<option value="content_wait_del" <?php if($_GET['nns_state']=='content_wait_del') echo 'selected="selected"'?>>等待内容上下线</option>
											<option value="content_doing_del" <?php if($_GET['nns_state']=='content_doing_del') echo 'selected="selected"'?>>正在内容上下线</option>
											<option value="content_fail_del" <?php if($_GET['nns_state']=='content_fail_del') echo 'selected="selected"'?>>内容上下线失败</option>
									</select>
									<?php
									}
									?>
									<?php
									if($epg_display_import=='0')
									{
									?>
                                        &nbsp;&nbsp;EPG状态：&nbsp;&nbsp;<select name="nns_epg_state" style="width: 150px;">
											<option value="" >全部</option>
											<option value="content_epg_wait" <?php if($_GET['nns_epg_state']=='content_epg_wait') echo 'selected="selected"'?>>等待注入EPG</option>											
											<option value="content_epg_doing" <?php if($_GET['nns_epg_state']=='content_epg_doing') echo 'selected="selected"'?>>正在注入EPG</option>
											<option value="content_epg_ok" <?php if($_GET['nns_epg_state']=='content_epg_ok') echo 'selected="selected"'?>>注入EPG成功</option>
											<option value="content_epg_fail" <?php if($_GET['nns_epg_state']=='content_epg_fail') echo 'selected="selected"'?>>注入EPG失败</option>
											<option value="content_epg_cancel" <?php if($_GET['nns_epg_state']=='content_epg_cancel') echo 'selected="selected"'?>>注入EPG取消</option>
									</select>
									<?php
									}
									?>
                                        &nbsp;&nbsp;选择时间段：&nbsp;&nbsp;<input name="day_picker_start" id="day_picker_start" type="text"  value="<?php
										if (isset($_GET['day_picker_start']))
											echo $_GET['day_picker_start'];
									?>" style="width:120px;" class="datetimepicker" callback="test" />
										 - <input name="day_picker_end" id="day_picker_end" type="text"  value="<?php
											if (isset($_GET['day_picker_end']))
												echo $_GET['day_picker_end'];
										?>" style="width:120px;" class="datetimepicker" callback="test" />
										 &nbsp;&nbsp;<input type="button" id="clear_time" name="clear_time" value="清除时间"/>

										 	&nbsp;&nbsp;CP ID：&nbsp;&nbsp;<select name="nns_cp_id" style="width: 150px;">
                                    		<option value="" selected="selected" >全部</option>
                                    	<?php 
                                    		foreach ($result_cp_id_data as $cp_id_key=>$cp_id_val)
											{
												?>
												<option value="<?php echo $cp_id_key; ?>" <?php if(isset($_GET['nns_cp_id']) && $_GET['nns_cp_id']==$cp_id_key) echo 'selected="selected"'?>><?php echo $cp_id_val; ?></option>
												<?php 
											}
											echo "</select>";
                                    	?>

										<input type="hidden" name="sp_id" value="<?php echo $sp_id; ?>" />
										<input type="hidden" name="actionsearch" value="search" />
										&nbsp;&nbsp;&nbsp;&nbsp;	
										<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  />
										&nbsp;&nbsp;&nbsp;&nbsp;	
										<input type="button" value="导出CSV格式"  onclick="export_data();" />
                                        <?php
                                            if(isset($sp_info['cdn_media_playurl_by_csv']) && $sp_info['cdn_media_playurl_by_csv'] == 1)
                                            {
                                                echo '&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="导出正在注入CDN的片源数据"  onclick="export_media_data_cdning();" />';
                                            }
                                        ?>
									</td>
								</tr>
							</tbody>
							</form>
						</table>
				</div>			
			<div class="content_table formtable">

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th><input name="" type="checkbox" value="">序号</th>
							<th>资源ID</th>
                            <th>父级资源ID</th>
							<th>名称</th>
							<th>类型</th>
							<th>动作</th>
							<th>创建时间</th>
							<th>修改时间</th>
							<?php
									if($cdn_display_import=='0')
									{
							?>
							<th>CDN状态</th>
							<?php
									}
							?>
							<?php
									if($cdn_fail_time_display=='0')
									{
							?>
							<th>CDN失败次数</th>
							<?php
									}
							?>
							<?php
									if($epg_display_import=='0')
									{
									?>
							<th>EPG状态</th>
							<?php
									}
							?>
							<th>是否属于分组</th>
							<th>权重</th>
							<th>CP ID</th>
							<?php if($cdn_notify_third_party_enable){?>
							<th>消息反馈状态</th>
							<?php }?>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if($data!=null){
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) {
								$num++;
						?>
						<tr>
						<td><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
						<td>						
						<?php
						
							echo $item['nns_ref_id'];
						
						?>
				   
						</td>
                            <td>

                                <?php
                                echo empty($item['nns_src_id']) ? '' : $item['nns_src_id'];
                                ?>
                            </td>

						<td><a href="javascript:void(null);"  onclick="show_queue_log('<?php echo $item["nns_ref_id"]; ?>','<?php echo $item["nns_type"]; ?>','<?php echo $item["nns_op_id"]; ?>','<?php echo $item["nns_org_id"]; ?>');">						
						<?php
						
							echo $item['nns_name'];
						
						?></a>
				   
						</td>
						<td>
						<?php
						switch($item['nns_type']) {
							case 'video' :
								echo '影片';
								break;
							case 'index' :
								echo '分集';
								break;
							case 'media' :
								echo '片源';
								break;
							case 'live' :
							    echo '直播频道';
							    break;
							case 'live_media' :
							    echo '直播源';
							    break;
						    case 'playbill' :
						        echo '节目单';
						        break;
					        case 'file' :
					            echo '包文件';
					            break;
					        case 'product':
					            echo "产品包";
					            break;
						}
						?>
						</td>
						<td>						
						<?php
							switch($item['nns_action']) {
								case 'add' :
									echo '添加';
									break;
								case 'modify' :
									echo '修改';
									break;
								case 'destroy' :
									echo '删除';
									break;
							}
						?>
				   
						</td>
						<td><?php echo $item['nns_create_time']; ?></td>						
					   <td><?php echo $item['nns_modify_time']; ?></td>   
					   <?php
									if($cdn_display_import=='0')
									{
									?>
						<td>	
							
						<?php 
						
						switch($item['nns_status']) {
							case SP_C2_WAIT :
								echo '等待CDN注入';
								break;
							case SP_C2_OK:
								echo '注入CDN成功';
								break;
							case SP_C2_FAIL:
								echo '注入CDN失败';
								break;
							case SP_C2_DOING:
								echo '正在注入CDN';
								break;
							case 6:
								echo '等待获取CDN播放串';
								break;
							case 7:
								echo '注入CDN取消';
								break;
							case 8:
								echo '正在获取CDN播放串';
								break;
							case 9:
								echo '获取CDN播放串失败';
								break;
							case 10:
								echo 'ftp连接失败';
								break;
							case 11:
								echo 'ftp片源获取失败';
								break;

								case 12:
								    echo '等待内容上下线';
								    break;
								case 13:
								    echo '正在内容上下线';
								    break;
								case 14:
								    echo '内容上下线失败';
								    break;
						}
						?>
						
						</td>
						<?php
									}
						?>
						<?php
									if($cdn_fail_time_display=='0')
									{
						?>
					<td><?php echo $item['nns_cdn_fail_time']; ?></td>
						<?php
									}
						?>
							<?php
									if($epg_display_import=='0')
									{
									?>
						<td>
							
							<?php 
						
						switch($item['nns_epg_status']) {
                            case 96:
                                echo '注入EPG失败';
                                break;
							case SP_EPG_WAIT :
								echo '等待EPG注入';
							break;
							case SP_EPG_OK:
								echo '注入EPG成功';
							break;
							case SP_EPG_DOING:
								echo '正在注入EPG';
							break;
							case 100:
								echo '注入EPG取消';
								break;
						}
						?>
						
						</td>
						<?php
									}
						?>
						<td><?php echo ($item['nns_is_group'] != '1') ? "否" : "是"; ?></td>
						<td><?php echo $item['nns_weight']; ?></td>
						<td>
					    <?php if(in_array($item['nns_cp_id'], $result_cp_data_keys))
					    {
					        echo isset($result_cp_id_data[$item['nns_cp_id']]) ? $result_cp_id_data[$item['nns_cp_id']] : '';
					    }
					    else
					    {
					        echo '<font color="#FF0000">非法</font>';
					    }?>
					    </td>
					    <?php if($cdn_notify_third_party_enable){?>
						<td>
						  <?php if($item['nns_notify_third_state'] == '1'){ echo "成功";}elseif ($item['nns_notify_third_state'] == '2'){ echo "失败";}else{ echo "原始状态";}?>
						</td>
						<?php }?>
						<td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
							<?php
									if($cdn_display_import=='0')
									{
                                        if(!$bk_version_number) {
                                            ?>
                                            <a href="nncms_content_task.php?op=resend_c2_task&action=add&status=<?php echo SP_C2_WAIT; ?>&c2_task_id=<?php echo $item['nns_id'] ?><?php echo $nns_url . $search_url; ?>">添加CDN</a>
                                            <a href="nncms_content_task.php?op=resend_c2_task&action=modify&status=<?php echo SP_C2_WAIT; ?>&c2_task_id=<?php echo $item['nns_id'] ?><?php echo $nns_url . $search_url; ?>">修改CDN</a>
                                            <a href="nncms_content_task.php?op=resend_c2_task&action=destroy&status=<?php echo SP_C2_WAIT; ?>&c2_task_id=<?php echo $item['nns_id'] ?><?php echo $nns_url . $search_url; ?>">删除CDN</a>

                                            <?php
                                        }else{ ?>
                                            <a href="nncms_content_task.php?op=resend_c2_task&c2_task_id=<?php echo $item['nns_id'] ?><?php echo $nns_url . $search_url; ?>">注入CDN</a>
                                            <?php }}
									if($cdn_display_import_line == '1')
									{
									    ?>
									    <a href="nncms_content_task.php?op=task_action_line&ids=<?php echo $item['nns_id']?><?php echo $nns_url . $search_url; ?>">上下线CDN</a>
									    <?php 
									}

									if($cdn_display_import_delivery == '1')
									{
									    ?>
									    <a href="nncms_content_task.php?op=task_action_delivery&ids=<?php echo $item['nns_id']?><?php echo $nns_url . $search_url; ?>">发布媒资</a>
									<?php }?>
									<?php if($cdn_display_third_play_url == '1'){?>
									    <a href="nncms_content_task.php?op=task_action_playurl&ids=<?php echo $item['nns_id']?><?php echo $nns_url . $search_url; ?>">获取播放串</a>
									    <?php 
									}
									if($epg_display_import=='0')
									{
						?>
						<a href="nncms_content_task.php?op=epg_action&ids=<?php echo $item['nns_id']?>,&sp_id=<?php echo $_GET['sp_id']; ?>&action=add">注入EPG</a>
						<?php
									}
						if($epg_display_import_delete=='0' && !$bk_version_number)
						{
							?>
						<a href="nncms_content_task.php?op=epg_action&ids=<?php echo $item['nns_id']?>,&sp_id=<?php echo $_GET['sp_id']; ?>&action=destroy">删除EPG</a>	
						<?php	
						}
						?>
						<?php
						if($item['nns_type']=='media'&&$item['nns_action']!='destroy'){
						?>
						<a href="nncms_content_task.php?sp_id=<?php echo $sp_id?>&op=query_queue&ids=<?php echo $item['nns_id']?>">获取URL</a>
						<?php	
						}
						?>
						<a href="nncms_content_task.php?op=cancel_queue&ids=<?php echo $item['nns_id']?>&sp_id=<?php echo $_GET['sp_id']; ?>">取消队列</a>
                            <?php if($bk_version_number && !empty($item['nns_queue_execute_url'])){?>
                                <a target="_blank" href="<?php echo $g_bk_web_url . "/data/" . $item['nns_queue_execute_url']; ?>">查看日志</a>
                                    <?php } ?>
						</td>
						</tr>
						<?php
							}
						}$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) {
						?>
						<tr>
																																														  
															   <td>&nbsp;</td>
															   <td>&nbsp;</td>
															   <td>&nbsp;</td>
															   <td>&nbsp;</td>
															   <td>&nbsp;</td>
															   <td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp; </td>
																<td>&nbsp; </td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
                                                                <td>&nbsp;</td>
																<?php
									if($cdn_display_import=='0')
									{
									?>
																 <td>&nbsp;</td>
																 <?php
									}
																 ?>
									<?php
									if($cdn_fail_time_display=='0')
									{
										?>
										<td>&nbsp;</td>
										<?php
									}
									?>
																 <?php
									if($epg_display_import=='0')
									{
									?>
																 <td>&nbsp;</td>
																 <?php
									}
																 ?>


														</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="pagecontrol">
			  共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
								<?php if ($currentpage > 1) { ?>
										<a href="nncms_content_task.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
										<a href="nncms_content_task.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
								<?php } else { ?>
										<span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
										<span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
								<?php } if ($currentpage < $vod_pages) { ?>
										<a href="nncms_content_task.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
										<a href="nncms_content_task.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
								<?php } else { ?>
										<span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
										<span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
								<?php } ?>

								<?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
								<a href="javascript:go_page_num('nncms_content_task.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
								<?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
								<?php echo cms_get_lang('perpagenum'); ?>&nbsp;
								<input name="nns_list_max_num" id="nns_list_max_num" type="text"
									   value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
								<input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
									   onclick="refresh_vod_page();"/>&nbsp;&nbsp;

						</div>
						
				 <div class="controlbtns">
		<div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
		<div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
		<?php if($cdn_display_import=='0'){if(!$bk_version_number){?><div class="controlbtn move"><a href="javascript:task_action('add');">添加CDN</a></div>
		<div class="controlbtn move"><a href="javascript:task_action('modify');">修改CDN</a></div>
		<div class="controlbtn delete"><a href="javascript:task_action('destroy');">删除CDN</a></div><?php }else{?>
        <div class="controlbtn move"><a href="javascript:task_action_v2();">注入CDN</a></div><?php }}?>
		<?php if($cdn_display_import_line == '1'){?>
		<div class="controlbtn move"><a href="javascript:task_action_line();">上下线CDN</a></div><?php }?>
		<?php 
		      if($cdn_display_import_delivery == '1'){
		          ?>
		          <div class="controlbtn move"><a href="javascript:task_action_delivery();">发布媒资</a></div>
		       <?php }?>
		       <?php if($cdn_display_third_play_url == '1'){?>
		          <div class="controlbtn move"><a href="javascript:task_action_playurl();">获取播放串</a></div>
		          <?php 
		      }
		?>
		<?php if($epg_display_import=='0'){?><div class="controlbtn move"><a href="javascript:epg_action('add');">注入EPG</a></div><?php }?>
		<?php
						if($epg_display_import_delete=='0' && !$bk_version_number)
						{
							?>
		<div class="controlbtn move"><a href="javascript:epg_action('destroy');">删除EPG</a></div>					
							<?php
						}
							?>
	    <?php if($cdn_notify_third_party_enable){?>
	    <div class="controlbtn move"><a href="javascript:notify_third_party();">三方反馈</a></div>
	    <?php }?>
		<div class="controlbtn move"><a href="javascript:add_queue('add');">重新注入</a></div>
		<div class="controlbtn move"><a href="javascript:query_queue();">获取URL</a></div>
		<div class="controlbtn move"><a href="javascript:cancel_queue();">取消队列</a></div>
		<div style="clear:both;"></div>
	</div>	   
						
		</div>
	</body>
</html>
