<?php 
header("Content-Type:text/html;charset=utf-8");
ob_start();
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
require_once $nncms_config_path . "op/op_log.class.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
//打开DC
$dc=nl_get_dc( array(
		'db_policy'=>NL_DB_WRITE,
		'cache_policy'=>NP_KV_CACHE_TYPE_MEMCACHE
));
$dc->open();
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135204");
$checkpri = null;
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}

$sp_id = $_GET['sp_id'];
$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : (!empty($_COOKIE["page_max_num"]) ?  $_COOKIE["page_max_num"] : 18);
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) {
	$page_num = 1;
}
$start = intval($page_num - 1) * $page_size;
$params = null;
if ($_GET['actionsearch'] == 'search') {
	$params['name'] = isset($_GET['name']) ? trim($_GET['name']) : null;
	$params['video_id'] = isset($_GET['video_id']) ? $_GET['video_id'] : null;
	$params['id'] = isset($_GET['id']) ? $_GET['id'] : null;
	$params['type'] = isset($_GET['type']) ? $_GET['type'] : null;
	$params['action'] = isset($_GET['action']) ? $_GET['action'] : null;
	$params['video_id'] = isset($_GET['video_id']) ? $_GET['video_id'] : null;
	$params['weight'] = isset($_GET['weight']) ? $_GET['weight'] : null;
	$params['status'] = isset($_GET['status']) ? $_GET['status'] : null;
	$params['begin_time'] = isset($_GET['day_picker_start']) ? $_GET['day_picker_start'] : null;
	$params['end_time'] = isset($_GET['day_picker_end']) ? $_GET['day_picker_end'] : null;
}
$params['org_id'] = $sp_id; 
$op = new op_log();
$result = $op->search_op_log_list($dc, $params, $start, $page_size);

$vod_total_num = is_array($result) ? $result['count'] : 0;
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) {
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);
$data = $result['item'];
//var_dump($data);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript">
			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
                setCookie("page_max_num",num);
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			$(document).ready(function(){
				$('#clear_time').click(function(){
						$('#day_picker_start').val('');
						$('#day_picker_end').val('');

					});
			})
		</script>
	</head>

	<body>
		<div class="content">
			<div class="content_position">
				操作队列日志
			</div>
				<div class="content_table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<form action="" method="get">
							<tbody>
								<tr>
									<td>
										影片名：&nbsp;&nbsp;<input type="text" name="name" value="<?php echo $_GET['name']; ?>" style="width:200px;">
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										影片ID：&nbsp;&nbsp;<input type="text" name="video_id" value="<?php echo $_GET['video_id']; ?>" style="width:230px;">	
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										日志ID：&nbsp;&nbsp;<input type="text" name="id" value="<?php echo $_GET['id']; ?>" style="width:230px;">									
										<br/>
                                    	权重：&nbsp;&nbsp;<input type="text" name="weight" style="width:120px;" value="<?php echo $_GET['weight']; ?>"/>
                                    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;									
										类型：&nbsp;&nbsp;<select name="type" style="width: 100px;">
											<option value="" >全部</option>
											<option value="video" <?php if($_GET['type']=='video') echo 'selected="selected"'?>>影片</option>
											<option value="index" <?php if($_GET['type']=='index') echo 'selected="selected"'?>>分集</option>
											<option value="media" <?php if($_GET['type']=='media') echo 'selected="selected"'?>>片源</option>
                                    	</select>
                                    	&nbsp;&nbsp;&nbsp;&nbsp;
                                    	操作：&nbsp;&nbsp;<select name="action" style="width: 100px;">
											<option value="" >全部</option>
											<option value="add" <?php if($_GET['action']=='add') echo 'selected="selected"'?>>添加</option>
											<option value="modify" <?php if($_GET['action']=='modify') echo 'selected="selected"'?>>修改</option>
											<option value="destroy" <?php if($_GET['action']=='destroy') echo 'selected="selected"'?>>删除</option>
                                    	</select>
				 						&nbsp;&nbsp;&nbsp;&nbsp;
                                    	列队状态：&nbsp;&nbsp;<select name="status" style="width: 100px;">
											<option value="" >全部</option>
											<option value="bk_op_wait" <?php if($_GET['status']=='bk_op_wait') echo 'selected="selected"'?>>等待执行</option>
											<option value="bk_op_progress" <?php if($_GET['status']=='bk_op_progress') echo 'selected="selected"'?>>执行中</option>
											<option value="bk_op_wait_clip" <?php if($_GET['status']=='bk_op_wait_clip') echo 'selected="selected"'?>>等待切片</option>
											<option value="bk_op_clip" <?php if($_GET['status']=='bk_op_clip') echo 'selected="selected"'?>>切片中</option>
											<option value="bk_op_ok" <?php if($_GET['status']=='bk_op_ok') echo 'selected="selected"'?>>完成</option>
											<option value="bk_op_cancel" <?php if($_GET['status']=='bk_op_cancel') echo 'selected="selected"'?>>取消</option>
                                    	</select>&nbsp;&nbsp;&nbsp;&nbsp;
										创建时间：&nbsp;&nbsp;
										<input name="day_picker_start" id="day_picker_start" type="text"  value="<?php echo $_GET['day_picker_start']; ?>" style="width:120px;" class="datetimepicker" callback="test" /> - <input name="day_picker_end" id="day_picker_end" type="text"  value="<?php echo $_GET['day_picker_end'];?>" style="width:120px;" class="datetimepicker" callback="test" />
										<input type="button" id="clear_time" value="清除时间"/>
										<input type="hidden" name="sp_id" value="<?php echo $sp_id; ?>" />
										<input type="hidden" name="view_list_max_num" value="<?php echo $page_size; ?>" />
										<input type="hidden" name="actionsearch" value="search" />
										&nbsp;&nbsp;&nbsp;&nbsp;	
										<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  />										
									</td>
								</tr>
							</tbody>
							</form>
						</table>
				</div>			
			<div class="content_table formtable">

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th><input name="" type="checkbox" value=""></th>
							<th>序号</th>
							<th>日志ID</th>
							<th>名称</th>
							<th>类型</th>						    
						    <th>列队动作</th>
						    <th>列队状态</th>
						    <th>权重</th>
						    <th>创建时间</th>
						</tr>
					</thead>
					<tbody>
					<?php
						if($data!=null){
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) {
								$num++;
					?>
						<tr>
							<td><input name="input" type="checkbox"></td>
							<td><?php echo $num; ?></td>
							<td><?php echo $item['nns_id']; ?></td>
							<td><?php echo $item['nns_name']; ?></td>
							<td>
							<?php
								switch($item['nns_type']) {
									case 'video' :
										echo '影片';
										break;
									case 'index' :
										echo '分集';
										break;
									case 'media' :
										echo '片源';
										break;
								}
                        	?>
							</td>							
							<td>
							<?php
							switch($item['nns_action']) {
								case 'add' :
									echo '添加';
									break;
								case 'modify' :
									echo '修改';
									break;
								case 'destroy' :
									echo '删除';
									break;
							}
                        	?>
							</td>
							<td>
							<?php                        
	                        switch($item['nns_status']) {
								case 0 :
									echo '等待执行';
								break;
								
								case 1 :
									echo '执行中';
								break;
								
								case 2 :
									echo '等待切片';
								break;
								
								case 3 :
									echo '切片中';
								break;
								case 4:
									echo '取消';
								break;
								default:
									echo '完成';
								break;							
							}
							?>
							</td>
							<td><?php echo $item['nns_weight']; ?></td>
							<td><?php echo $item['nns_create_time']; ?></td>
						</tr>
					<?php 
							}
						}
						$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) {
					?>
						<tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="pagecontrol">
			  共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_op_log.php?page=1<?php echo $nns_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_op_log.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_content_op_log.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_op_log.php?page=<?php echo $vod_pages; ?><?php echo $nns_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_op_log.php?ran=1<?php echo $nns_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

                        </div>                        
		</div>
	</body>
</html>
