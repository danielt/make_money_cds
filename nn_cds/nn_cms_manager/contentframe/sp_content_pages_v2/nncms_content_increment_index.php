<?php header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
    $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool) {
    Header("Location: ../nncms_content_wrong.php");
    exit ;
}
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';

$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 18;
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) {
    $page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit = $page_size;
$filter = null;

if ($_GET['action'] == 'search') {
    $_GET['nns_name'] = trim($_GET['nns_name']);
    $filter['nns_name'] = $_GET['nns_name'];
    $filter['nns_action'] = $_GET['nns_action'];
    $filter['day_picker_start'] = $_GET['day_picker_start'];
    $filter['day_picker_end'] = $_GET['day_picker_end'];
}

$result = video_model::index_increment($filter, $offset, $limit);

$vod_total_num = $result['rows'];
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) {
    $page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;

$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);

$data = $result['data'];
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
        <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
        <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
        <script language="javascript" src="../../js/cms_cookie.js"></script>
        <script language="javascript" src="../../js/table.js.php"></script>
        <script language="javascript" src="../../js/checkinput.js.php"></script>
        <script language="javascript" src="../../js/rate.js"></script>
        <script language="javascript" src="../../js/image_loaded_func.js"></script>
        <script language="javascript" src="../../js/cms_datepicker.js"></script>
        <script language="javascript" src="../../js/alertbox.js"></script>
        <script language="javascript">
            function refresh_vod_page() {
            var num = $("#nns_list_max_num").val();
            window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
            }
    function checkhiddenBox(type) {
    BoxKey = false;
    $("input.checkhiddenInput:checked").each(function() {
    if ($(this).attr('rel') == type) {
    BoxKey = true;
    return false;
    }
    
    })
    return BoxKey;
    }
    $(document).ready(function() {
        $('#clear_time').click(function() {
            $('#day_picker_start').val('');
            $('#day_picker_end').val('');
        });
        window.parent.now_frame_url = window.location.href;
    });
    
    
    
    
    

        </script>

    </head>

    <body>
        <div class="content">
            <div class="content_position">
                分集修改
            </div>
            <div class="content_table">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <form action="" method="get">
                        <tbody>
                            <tr>
                                <td>影片名：&nbsp;&nbsp;
                                <input type="text" name="nns_name" id="nns_name" value="<?php echo $_GET['nns_name']; ?>" style="width:200px;">
                                &nbsp;&nbsp;&nbsp;&nbsp;动作：&nbsp;&nbsp;
                                <select name="nns_action" style="width: 150px;">
                                    <option value="" >全部</option>
                                    <option value="add" <?php if($_GET['nns_action']=='add') echo 'selected="selected"'?>>新增</option>
                                    <option value="modify" <?php if($_GET['nns_action']=='modify') echo 'selected="selected"'?>>修改</option>
                                    <option value="destroy" <?php if($_GET['nns_action']=='destroy') echo 'selected="selected"'?>>删除</option>
                                </select> &nbsp;&nbsp;&nbsp;&nbsp;
                                选择时间段：&nbsp;&nbsp;&nbsp;
                                <input name="day_picker_start" id="day_picker_start" type="text"  value="<?php
                                if (isset($_GET['day_picker_start']))
                                    echo $_GET['day_picker_start'];
                                ?>" style="width:120px;" class="datetimepicker" callback="test" />
                                -
                                <input name="day_picker_end" id="day_picker_end" type="text"  value="<?php
                                if (isset($_GET['day_picker_end']))
                                    echo $_GET['day_picker_end'];
                                ?>" style="width:120px;" class="datetimepicker" callback="test" />
                                &nbsp;&nbsp;
                                <input type="button" id="clear_time" name="clear_time" value="清除时间"/>
                                <input type="hidden" name="action" value="search" />
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="submit" value="<?php echo cms_get_lang('search'); ?>"  />
                                </td>
                            </tr>
                        </tbody>
                    </form>
                </table>
            </div>
            <div class="content_table formtable">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th>
                            <input name="" type="checkbox" value="">
                            序号</th>
                            <th>资源ID</th>
                            <th>名称</th>
                            <th>注入ID</th>
                            <th>类型</th>
                            <th>动作</th>
                            <th>创建时间</th>
                            <th>修改时间</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if($data!=null){
                            $num = ($page_num - 1) * $page_size;
                            foreach ($data as $item) {
                                $num++;
                        ?>
                        <tr>
                            <td><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
                            <td><?php echo $item["nns_id"]; ?></td>
                            <td><?php echo $item["nns_name"]; ?></td>
                            <td><?php echo $item["nns_import_id"]; ?></td>
                            <td><?php echo "分集"; ?></td>
                            <td><?php echo $item["nns_action"]; ?></td>
                            <td><?php echo $item["nns_create_time"]; ?></td>
                            <td><?php echo $item["nns_modify_time"]; ?></td>
                            <td><?php echo "分集"; ?></td>
                        </tr>
                        <?php
                            }
                        }$least_num = $g_manager_list_max_num - count($data);
                        for ($i = 0; $i < $least_num; $i++) {
                        ?>
                        <tr>
                                                                                                                                                                                          
                                                               
                                                               <td>&nbsp;</td>
                                                               <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp;</td>
                                                                 <td>&nbsp;</td>
                                                                 <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>

                                                        </tr>
                        <?php } ?>
                    </tbody>
                        
                </table>
            </div>
            
            <div class="pagecontrol">
              共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_increment_index.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_increment_index.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_content_increment_index.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_increment_index.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_increment_index.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

                        </div>
                        
                        <div class="controlbtns">
        <div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
        <div style="clear:both;"></div>
    </div> 

        </div>
    </body>
</html>