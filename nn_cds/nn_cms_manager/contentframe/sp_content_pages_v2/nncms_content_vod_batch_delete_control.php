<?php
header("Content-Type:text/html;charset=utf-8");
ob_start();
set_time_limit(0);
include("../../nncms_manager_inc.php");

include_once LOGIC_DIR . 'log' . DIRECTORY_SEPARATOR . 'manager_log.class.php';
include_once LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'vod_media.class.php';
include_once LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'vod_index.class.php';
include_once $nncms_config_path . 'mgtv_v2' . DIRECTORY_SEPARATOR . 'mgtv_init.php';
include_once $nncms_config_path . 'mgtv_v2' . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'queue_task_model.php';

//导入语言包
 if (isset($_POST["language_dir"]))
 {
 	$language_dir=$_POST["language_dir"];
 }
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

$org_id = $_POST["nns_org_id"];
$org_type = $_POST["nns_org_type"];
$sp_id = $_POST['sp_id'];

$targetFolder = dirname(__FILE__); 
$verifyToken = md5('unique_salt' . $_POST['timestamp']);
$action =null;
if($_POST['nns_action'] == 'delete_old')
{
	$action = 'delete_old';
	$file_path = $nncms_config_path . 'data' . DIRECTORY_SEPARATOR . 'log' . DIRECTORY_SEPARATOR . 'op_queue' . DIRECTORY_SEPARATOR . "$sp_id" . DIRECTORY_SEPARATOR. 'delete_media_task.txt';
}
else
{
	$file_path = $nncms_config_path . 'data' . DIRECTORY_SEPARATOR . 'log' . DIRECTORY_SEPARATOR . 'op_queue' . DIRECTORY_SEPARATOR . 'delete_vod_task.txt';
}

if (!empty($_FILES) && $_POST['token'] == $verifyToken)
{
	try
	{
		if($_POST['nns_action'] == 'delete_old')
		{
			$state = file_import_to_delete_media_txt($_FILES['Filedata']['tmp_name'], $sp_id, $file_path);
		}
		else
		{
			$state = file_import_to_txt($_FILES['Filedata']['tmp_name'], $sp_id, $file_path);
		}
		echo $state;
	}
	catch (Exception $e)
	{
		echo 'csv文件格式错误';
	}
}

function file_import_to_delete_media_txt($upload_file, $sp_id, $file_path)
{
	$str_log_dir = dirname($file_path);
	if (!is_dir($str_log_dir))
	{
		mkdir($str_log_dir, 0777, true);
	}
	$upload_file = fopen($upload_file, 'r');
	while (!feof($upload_file))
	{
		$line_arr = fgetcsv($upload_file);
		if (is_array($line_arr) && !empty($line_arr))
		{
			$media_id = trim($line_arr[0]);
			if(empty($media_id))
			{
				continue;
			}
			$media_name = (strlen($line_arr[1]) > 0) ? $line_arr[1] : 'null';
			file_put_contents($file_path, "{$sp_id}:|:{$media_id}:|:{$media_name}\r\n", FILE_APPEND);
		}
	}
	fclose($upload_file);
	return 'success';
}



/**
 * 将要删除的数据整理好放入txt文件中，给计划任务备用
 * @param string $upload_file 上传文件路径
 * @param string $sp_id SP ID
 * @param string $file_path txt文件路径
 * @return string
 * @author chunyang.shu
 * @date 2015-03-02
 */
function file_import_to_txt($upload_file, $sp_id, $file_path)
{
	$upload_file = fopen($upload_file, 'r');
	$queue_task_model = new queue_task_model();
	$dc = nl_get_dc(array (
			'db_policy' => NL_DB_READ,
	));
	$dc->open();
	if($sp_id === "sihua")
	{
		$str_fuc="file_import_to_txt_".$sp_id;
		return $str_fuc($dc,$upload_file, $file_path);
	}
	while (!feof($upload_file))
	{
		$line_arr = fgetcsv($upload_file);
		if (is_array($line_arr) && !empty($line_arr))
		{
			$vod_id = $line_arr[0];
			$vod_index_count = nl_vod_index::count_vod_index($dc, $vod_id);
			if ($vod_index_count > 0)
			{
				$step_count = ceil($vod_index_count / 100);
				for ($i = 0; $i < $step_count; $i++)
				{
					$vod_index_info = nl_vod_index::get_vod_index($dc, $vod_id, $i * 100, 100);
					if (is_array($vod_index_info) && !empty($vod_index_info))
					{
						foreach ($vod_index_info as $vod_index_item)
						{
							$vod_media_info = nl_vod_media::get_vod_media($dc, $vod_id, $vod_index_item['nns_index']);
							if (is_array($vod_media_info) && !empty($vod_media_info))
							{
								foreach ($vod_media_info as $vod_media_item)
								{
									file_put_contents($file_path, "{$sp_id}:{$vod_media_item['nns_id']}:media\r\n", FILE_APPEND);
								}
							}
							
							file_put_contents($file_path, "{$sp_id}:{$vod_index_item['nns_id']}:index\r\n", FILE_APPEND);
						}
					}
				}
			}
			file_put_contents($file_path, "{$sp_id}:{$vod_id}:video\r\n", FILE_APPEND);
		}
	}
	
	fclose($upload_file);
	return 'success';
}
/**
 * 将要删除的数据整理好放入txt文件中，给计划任务备用
 * @param string $upload_file 上传文件路径
 * @param string $sp_id SP ID
 * @param string $file_path txt文件路径
 * @param string $sp_id 运营商
 * @return string
 * @author chunyang.shu
 * @date 2015-03-02
 */
function file_import_to_txt_sihua($dc,$upload_file, $file_path, $sp_id = 'sihua')
{
	while (!feof($upload_file))
	{
		$line_arr = fgetcsv($upload_file);
		if (is_array($line_arr) && !empty($line_arr))
		{
			$vod_id = $line_arr[0];
			$vod_index_count = nl_vod_index::count_vod_index($dc, $vod_id);
			if ($vod_index_count > 0)
			{
				$step_count = ceil($vod_index_count / 100);
				for ($i = 0; $i < $step_count; $i++)
				{
					$vod_index_info = nl_vod_index::get_vod_index($dc, $vod_id, $i * 100, 100);
					if (is_array($vod_index_info) && !empty($vod_index_info))
					{
						foreach ($vod_index_info as $vod_index_item)
						{
							$vod_media_info = nl_vod_media::get_vod_media($dc, $vod_id, $vod_index_item['nns_index']);
							if (is_array($vod_media_info) && !empty($vod_media_info))
							{
								foreach ($vod_media_info as $vod_media_item)
								{
									file_put_contents($file_path, "{$sp_id}:{$vod_media_item['nns_id']}:media\r\n", FILE_APPEND);
								}
							}
						}
					}
				}
			}
		}
	}
	
	fclose($upload_file);
	return 'success';
}

