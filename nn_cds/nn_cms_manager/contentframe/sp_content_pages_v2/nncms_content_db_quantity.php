<?php
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_db_path. "nns_common/nns_db_pager_class.php";
 //加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');

include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include_once(dirname(dirname(dirname(dirname(__FILE__)))).DIRECTORY_SEPARATOR."nn_cms_config.php");
include_once(dirname(dirname(dirname(dirname(__FILE__)))).DIRECTORY_SEPARATOR."mgtv_v2".DIRECTORY_SEPARATOR."models".DIRECTORY_SEPARATOR."video_model.php");
$model=new video_model();
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"104103");
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");
$array=array();
$array['table']=$_REQUEST['fenlei']?$_REQUEST['fenlei']:'vod';
$array['where']=$_REQUEST['where']?$_REQUEST['where']:'nns_modify_time';

$array['begin_time']=$_REQUEST['begin_time']?$_REQUEST['begin_time']:date('Y-m-d H:i:s',strtotime(date('Y-m-d')));
$array['end_time']=$_REQUEST['end_time']?$_REQUEST['end_time']:date('Y-m-d H:i:s',strtotime(date('Y-m-d',strtotime('+1 day'))));
include_once($nncms_config_path.'nn_logic/nl_common.func.php');
$dc=nl_get_dc(array("db_policy" => NL_DB_WRITE, "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE));
if(!empty($_REQUEST['view_list_max_num'])){
	$g_manager_list_max_num=$_REQUEST['view_list_max_num'];
	setcookie("page_max_num", $g_manager_list_max_num);
}else{
	if (!empty($_COOKIE["page_max_num"])){
		$g_manager_list_max_num=$_COOKIE["page_max_num"];
	}
}
$total_num=$model->show_db_quantity_info_count($dc,$array);
$pages=ceil($total_num/$g_manager_list_max_num);
$currentpage=1;
if (!empty($_GET["page"])) $currentpage=$_GET["page"];
$ad_page_template_inst = null;
$limit=array();
$limit['left']=($currentpage-1)*$g_manager_list_max_num;
$limit['right']=$g_manager_list_max_num;
$result=$model->show_db_quantity_info($dc,$array,$limit);
$refresh="nncms_content_db_quantity.php?page=1&begin_time=".$array['begin_time']."&end_time=".$array['end_time']."&fenlei=".$array['table']."&where=".$array['where'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="../../js/calendar/jscal2.css" type="text/css" />
<link rel="stylesheet" href="../../js/calendar/border-radius.css" type="text/css" />
<link rel="stylesheet" href="../../js/calendar/win2k.css" />
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script type="text/javascript" charset="utf-8" src="../../js/calendar/calendar.js"></script>
<script type="text/javascript" src="../../js/calendar/calendar.lang.js.php"></script>
<script type="text/javascript" charset="utf-8" src="../../js/report_select_time_bar.js"></script>
<script type="text/javascript" charset="utf-8" src="../../js/tabs.js"></script>
<script type="text/javascript" charset="utf-8" src="../../js/cms_cookie.js"></script>
<script language="javascript">
function refresh_vod_page() {
	var num = $("#nns_list_max_num").val();
	window.location.href='<?php echo $refresh;?>&view_list_max_num='+num
}
$(function(){
	$('clear_time').click(function(){
		//$('#begin_time').val('');
		//$('#begin_time').val('');
	});
});
function ajax(){
	var fenlei=$('#fenlei').val();
	var begin_time=$('#begin_time').val();
    var end_time=$('#end_time').val();
    var where=$('#where').val();
	var url="";
	url="nncms_content_db_quantity.php"+'?fenlei='+fenlei+'&begin_time='+begin_time+'&end_time='+end_time+'&where='+where;
	window.location.href=url;
}
</script>
</head>   
<body>
<div class="content" >
	<div class="content_position"> > 资源数据库<?php if($array['where'] == 'nns_state'){ echo "删除量";}else if($array['where'] == 'nns_create_time'){ echo "创建量";}else{ echo "更新量"; } ?></div>
	<form id="tag_form" action="" method="post" >
    <input name="action" id="action" type="hidden" value="export"/>
    <!-- 
    <input type="text" name="begin" id="begin" value="<?php echo $array['begin_time']; ?>">
    <input type="text" name="end" id="end" value="<?php echo $array['end_time']; ?>">
     -->
     
    <div class="content_table">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
			<tr>
			<td>
			请选择类别:
			</td>	
			<td>
				<select class="fenlei" name="fenlei" id="fenlei" width="120">
					<option value="vod" <?php if($array['table'] == 'vod' ){ echo 'selected="selected"'; } ?>>主媒资</option>
					<option value="vod_index" <?php if($array['table'] == 'vod_index' ){ echo 'selected="selected"'; } ?>>分集</option>
					<option value="vod_media" <?php if($array['table'] == 'vod_media' ){ echo 'selected="selected"'; } ?>>片源</option>
				</select>
            </td>
            
            <td>
				<select class="where"  name="where" id="where" width="120">
					<option value="nns_modify_time" <?php if($array['where'] == 'nns_modify_time' ){ echo 'selected="selected"'; } ?>>更新</option>
					<option value="nns_create_time" <?php if($array['where'] == 'nns_create_time' ){ echo 'selected="selected"'; } ?>>新增</option>					
					<option value="nns_state" <?php if($array['where'] == 'nns_state' ){ echo 'selected="selected"'; } ?>>删除</option>
				</select>
			</td>
			<td ><?php echo cms_get_lang('operate_period');?></td>
			<td >
				<input name="begin_time" id="begin_time" type="text" value="<?php echo $_GET['begin_time'];?>"  style="width:120px;" /> - <input name="end_time" id="end_time" type="text" value="<?php echo $_GET['end_time'];?>"  style="width:120px;"  overto="begin_time" />
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" id="clear_time" name="clear_time" value="<?php echo cms_get_lang('time_reset');?>" /> 
			</td>
			<td>
			<input style="float:right;margin-top:-2px" type="button" value="<?php echo cms_get_lang('search');?>" onclick="ajax()">
			</td>
			</tr>
			<tr class='asset'>
			<td colspan="3">
			</td>			
			</tr>
          </tbody>
        </table>
    </div>
    </form>
	<div class="content_table formtable">	
	</div>
	
	<div class="content_table formtable">
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
			<?php switch ($array['table']){ case 'vod':?>
			<thead>
			<tr>
			<th>nns_name</th>
			<th>nns_state</th>
			<th>nns_view_type</th>
			<th>nns_pub_category_id</th>
			<th>nns_pri_category_id</th>
			<th>nns_pub_category_detail_id</th>
			<th>nns_create_time</th>
			<th>nns_modify_time</th>
			</tr>
			</thead>
			<tbody class="db_info">	
			<?php for($i=0;$i<count($result);$i++){ ?>					
			<tr>
					<td><?php echo $result[$i]['nns_name'] ?></td>
					<td><?php echo $result[$i]['nns_state'] ?></td>
					<td><?php echo $result[$i]['nns_view_type'] ?></td>
					<td><?php echo $result[$i]['nns_pub_category_id'] ?></td>
					<td><?php echo $result[$i]['nns_pri_category_id'] ?></td>
					<td><?php echo $result[$i]['nns_pub_category_detail_id'] ?></td>
					<td><?php echo $result[$i]['nns_create_time'] ?></td>
					<td><?php echo $result[$i]['nns_create_time'] ?></td>				
			</tr>						
			<?php }?>
			</tbody>
			<?php break; case 'vod_index':?>
			<thead>
			<tr>
			<th>nns_name</th>
			<th>nns_vod_id</th>
			<th>nns_index</th>
			<th>nns_time_len</th>
			<th>nns_summary</th>
			<th>nns_image</th>
			<th>nns_create_time</th>
			<th>nns_modify_time</th>
			</tr>
			</thead>
			<tbody class="db_info">	
			<?php for($i=0;$i<count($result);$i++){ ?>				
			<tr>
					<td><?php echo $result[$i]['nns_name'] ?></td>
					<td><?php echo $result[$i]['nns_vod_id'] ?></td>
					<td><?php echo $result[$i]['nns_index'] ?></td>
					<td><?php echo $result[$i]['nns_time_len'] ?></td>
					<td><?php echo $result[$i]['nns_summary'] ?></td>
					<td><?php echo $result[$i]['nns_image'] ?></td>
					<td><?php echo $result[$i]['nns_create_time'] ?></td>
					<td><?php echo $result[$i]['nns_modify_time'] ?></td>				
			</tr>						
			<?php }?>
			</tbody>
			
			
			<?php break; case 'vod_media':?>
			<thead>
			<tr>
			<th>nns_name</th>
			<th>nns_vod_id</th>
			<th>nns_vod_index_id</th>
			<th>nns_type</th>
			<th>nns_state</th>
			<th>nns_url</th>
			<th>nns_create_time</th>
			<th>nns_modify_time</th>
			</tr>
			</thead>
			<tbody class="db_info">	
			<?php for($i=0;$i<count($result);$i++){ ?>					
			<tr>
					<td><?php echo $result[$i]['nns_name'] ?></td>
					<td><?php echo $result[$i]['nns_vod_id'] ?></td>
					<td><?php echo $result[$i]['nns_vod_index_id'] ?></td>
					<td><?php echo $result[$i]['nns_type'] ?></td>
					<td><?php echo $result[$i]['nns_state'] ?></td>
					<td><?php echo $result[$i]['nns_url'] ?></td>
					<td><?php echo $result[$i]['nns_create_time'] ?></td>
					<td><?php echo $result[$i]['nns_modify_time'] ?></td>				
			</tr>						
			<?php }?>
			</tbody>
			<?php break; }?>
		</table>
	</div>
	<div class="pagecontrol">
			共<span style="font-weight:bold;color:#ff0000;"><?php echo $total_num;?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_db_quantity.php?page=1&begin_time=<?php echo $array['begin_time']; ?>&end_time=<?php echo $array['end_time'] ?>&fenlei=<?php echo $array['table'] ?>&where=<?php echo $array['where']; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_db_quantity.php?page=<?php echo $currentpage - 1; ?>&begin_time=<?php echo $array['begin_time']; ?>&end_time=<?php echo $array['end_time'] ?>&fenlei=<?php echo $array['table'] ?>&where=<?php echo $array['where']; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $pages) { ?>
                                        <a href="nncms_content_db_quantity.php?page=<?php echo $currentpage + 1; ?>&begin_time=<?php echo $array['begin_time']; ?>&end_time=<?php echo $array['end_time'] ?>&fenlei=<?php echo $array['table'] ?>&where=<?php echo $array['where']; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_db_quantity.php?page=<?php echo $pages; ?>&begin_time=<?php echo $array['begin_time']; ?>&end_time=<?php echo $array['end_time'] ?>&fenlei=<?php echo $array['table'] ?>&where=<?php echo $array['where']; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_db_quantity.php?ran=1&begin_time=<?php echo $array['begin_time']; ?>&end_time=<?php echo $array['end_time'] ?>&fenlei=<?php echo $array['table'] ?>&where=<?php echo $array['where']; ?>',<?php echo $pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

    </div>
</body>
</html>
