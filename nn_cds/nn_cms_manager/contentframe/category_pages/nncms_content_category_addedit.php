<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";


 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();

$action=$_GET["action"];
$nns_id=$_GET["nns_id"];
$pri_bool=false;
if (!empty($action)){
	switch($action){
		case "edit":
		$action_str=cms_get_lang('edit');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"105101");
		break;
		case "add":
		$action_str=cms_get_lang('add');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"105100");
		break;
		default:
		break;
	}
}
$checkpri=null;
 if (!isset($_GET["nns_manager_type"])){
	$pri_bool=false;
 }
 
 $category_type=$_GET["nns_manager_type"];
 $category_org_id="";
 if ($_SESSION["nns_manager_type"]!=0){
	$category_org_id=$_SESSION["nns_org_id"];
	if ($category_type!=$_SESSION["nns_manager_type"]){
		$pri_bool=false;
	}
 }

if (!$pri_bool){
	 Header("Location: ../nncms_content_wrong.php");
}else{

	 
	 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
	// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
	// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");
	 
	 if ($action=="edit"){
		include $nncms_db_path. "nns_category/nns_db_category_list_class.php";
		$category_inst=new nns_db_category_list_class();
		if (!empty($nns_id)){
			$category_info=$category_inst->nns_db_category_list_info($nns_id);
			$category_inst=null;
		}else{ 
			echo "<script>alert('".$category_info["reason"] ."');self.location='nncms_content_categorylist.php';</script>";
		}
		if ($category_info["ret"]!=0){
			echo "<script>alert('".$category_info["reason"] ."');self.location='nncms_content_categorylist.php';</script>";
		}
		$category_inst=null;
		$edit_data=$category_info["data"][0];
	//	$edit_pri=$edit_data["nns_pri_id"];
	 }
	 
	if ($category_type==1 && $_SESSION["nns_manager_type"]==0){
		include $nncms_db_path. "nns_partner/nns_db_partner_class.php";
		$partner_inst=new nns_db_partner_class();
		$partner_result=$partner_inst->nns_db_partner_list();
		$partner_inst=null;
		if ($partner_result["ret"]==0){
			$partner_data=$partner_result["data"];
		}
	}else if ($category_type==2 && $_SESSION["nns_manager_type"]==0){
		include $nncms_db_path. "nns_group/nns_db_group_class.php";
		$group_inst=new nns_db_group_class();
		 $group_result=$group_inst->nns_db_group_list();
		 $group_inst=null;
		if ($group_result["ret"]==0){
			$group_data=$group_result["data"];
		}
	}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/radiolist.js"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>

</head>

<body>


<div class="content">
	<div class="content_position"><?php echo cms_get_lang('lmgl');?> > <?php echo cms_get_lang('lmgl_lmmbgl');?></div>
	<form id="add_form" action="nncms_content_category_control.php" method="post">
	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
	<input name="category_type" id="category_type" type="hidden" value="<?php echo $category_type;?>" />
	<?php if ($_SESSION["nns_manager_type"]!=0){?>
	<input name="category_org_id" id="category_org_id" type="hidden" value="<?php echo $category_org_id;?>" />
	<?php }else if ($_SESSION["nns_manager_type"]==0 && $category_type==0){?>
	<input name="category_org_id" id="category_org_id" type="hidden" value="<?php echo $_SESSION["nns_org_id"];?>" />
	<?php }?>
	<?php if ($action=="edit"){?>
	<input name="nns_id" id="nns_id" type="hidden" value="<?php echo $nns_id;?>"/>
	<?php } ?>
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			  <tr>                                                            
				<td width="120"><?php echo cms_get_lang('lmgl_lmmbmc');?> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
				<td><input name="nns_category_name" id="nns_category_name" type="text" rule="noempty"
					value="<?php if($action=="edit"){echo $edit_data["nns_name"];}?>"
				/> </td>
				
			</tr>
			
			<?php if ($action=="add"){ if ($category_type==1 && $_SESSION["nns_manager_type"]==0){?>
			 <tr>                                                            
				<td width="120"><?php echo cms_get_lang('group');?> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
				<td>
					<select name="category_org_id" id="category_org_id" rule="noempty" style="margin-right:20px;"><br>
						<?php if ($partner_data!=null){ foreach($partner_data as $partner_item){?>
							<option value="<?php echo $partner_item["nns_id"];?>"  <?php if ($action=="edit" && $edit_data["nns_org_id"]==$partner_item["nns_id"]){?>selected="selected"<?php }?>><?php echo $partner_item["nns_name"];?></option>
						<?php }}?>
					</select> 
					</td>
				
			</tr> 
			<?php }else if ($category_type==2 && $_SESSION["nns_manager_type"]==0){?>
			<tr>                                                            
				<td width="120"><?php echo cms_get_lang('group');?>  <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
				<td>
					
					<select name="category_org_id" id="category_org_id" rule="noempty" style="margin-right:20px;"><br>
						<?php if ($group_data!=null){ foreach($group_data as $group_item){?>
							<option value="<?php echo $group_item["nns_id"];?>" <?php if ($action=="edit" && $edit_data["nns_org_id"]==$group_item["nns_id"]){?>selected="selected"<?php }?>><?php echo $group_item["nns_name"];?></option>
						<?php }}?>
					</select></td>
				
			</tr>  
		   <?php }}?>
		   <tr>                                                            
				<td width="120"><?php echo cms_get_lang('lmgl_lmmblx');?>  <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
				<td>
					
					<select name="category_live_type" id="category_live_type" rule="noempty" style="margin-right:20px;" <?php if ($action=="edit"){ echo "disabled";}?> ><br>
						
							<option value="0" <?php if ($action=="edit" && $edit_data["nns_live"]==0){?>selected="selected"<?php }?>><?php echo cms_get_lang('lmgl_dblm');?></option>
						<option value="1" <?php if ($action=="edit" && $edit_data["nns_live"]==1){?>selected="selected"<?php }?>><?php echo cms_get_lang('lmgl_zblm');?></option>
					</select></td>
				
			</tr>  
		  </tbody>
		</table>
	</div>
	</form>
	<div class="controlbtns">
		<div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('lmgl_lmmbgl');?>','<?php echo cms_get_lang('msg_ask_change'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo $cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>





</body>
</html>
<?php }?>

