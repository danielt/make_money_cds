<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
global $g_live_info_completion_url; //直播频道第三方信息获取接口
global $g_playbill_info_completion_url; //节目单第三方信息获取接口
$action=$_GET["action"];
$nns_id=$_REQUEST["nns_id"];
$pri_bool=false;
if (!empty($action)){
	switch($action){
	case "edit":
		$action_str=cms_get_lang('save');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136002");
		break;
	case "add":
		$action_str=cms_get_lang('add');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136003");
		break;
	default:
		break;
	}
}
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) .'/np/np_xml2array.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/queue/queue.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/live/third_live.class.php';
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);
$edit_data = $_REQUEST;
$dc = nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$dc->open();


/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/producer/producer.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/live/live.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/bigtv_bind/bigtv_bind.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';
$dc = nl_get_dc(array (
    "db_policy" => NL_DB_WRITE,
    "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));
if ($action=="edit"){
	$edit_data = nl_bigtv_bind::query_by_id($dc,$nns_id);
	
	if($edit_data['ret'] != 0 )
	{
		echo "<script>alert('". $action_str. $edit_data['reason']. "');</script>";
		echo "<script>self.location='".$base_file_name_base."_list.php;</script>";
	}
	$edit_data = $edit_data['data_info'];
}
// var_dump($edit_data);
$result_data_live = nl_live::query_all_filter_tstv($dc);
if($result_data_live['ret'] != 0)
{
    echo '<script>alert("'.$result_data_live['reason'].'");history.go(-1);</script>';die;
}
$result_data_live = isset($result_data_live['data_info']) ? $result_data_live['data_info'] : null;
$result_data_live_last=array();
if(is_array($result_data_live) && !empty($result_data_live))
{
    foreach ($result_data_live as $val)
    {
        $result_data_live_last[$val['nns_id']] = $val['nns_name'];
    }
}
unset($result_data_live);

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link href="../../css/comm.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
        <script language="javascript">
        
        </script>
    </head>
    <body>
        <div class="content">
        	<div class="content_position">第三方频道绑定列表 > 添加/修改</div>
        	<form id="add_form" action="<?php echo $base_file_name_base;?>_control.php" method="post">
            	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
            	<div class="content_table">
            		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            		  <tbody>
            		    <?php if($action == 'edit'){
            		        $result_data_live_last_key = !empty($result_data_live_last) ? array_keys($result_data_live_last) : array();
            		    ?>
                		    <tr>
                				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>GUID:</td>
                				<td>
                				    <input name="nns_id" id="nns_id" type="hidden"  value="<?php if(isset($edit_data["nns_id"])){echo $edit_data["nns_id"];}?>" rule="noempty"/>
                				    <input type="text" disabled value="<?php if(isset($edit_data["nns_id"])){echo $edit_data["nns_id"];}?>" />
                				</td>
                			</tr>
                			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>本地频道:</td>
            				<td>
            				    <select name='nns_bk_channel_id' style="width:150px;">
                                    <option value=''>--请选择--</option>
                                    <?php if(!empty($result_data_live_last)){foreach ($result_data_live_last as $model_key=>$model_val){?>
                                    <option value="<?php echo $model_key;?>" <?php if(isset($edit_data['nns_bk_channel_id']) && $edit_data['nns_bk_channel_id'] == $model_key){ echo 'selected="selected"';}?>><?php echo $model_val;?></option>
                                    <?php }}?>
                                </select>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>第三方频道:</td>
            				<td>
                                <input name="nns_bigtv_channel_name" id="nns_bigtv_channel_name" type="text"  value="<?php if(isset($edit_data["nns_bigtv_channel_name"])){echo $edit_data["nns_bigtv_channel_name"];}?>" rule="noempty"/>
            				</td>
            			</tr>
                	   <?php }else{?>
                	       <tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>本地频道:</td>
            				<td>
            				    <select name='nns_bk_channel_id' style="width:150px;">
                                    <option value=''>--请选择--</option>
                                    <?php if(!empty($result_data_live_last)){foreach ($result_data_live_last as $model_key=>$model_val){?>
                                    <option value="<?php echo $model_key;?>" <?php if(isset($edit_data['nns_bk_channel_id']) && $edit_data['nns_bk_channel_id'] == $model_key){ echo 'selected="selected"';}?>><?php echo $model_val;?></option>
                                    <?php }}?>
                                </select>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>第三方频道名称:</td>
            				<td>
            				    <input name="nns_bigtv_channel_name" id="nns_bigtv_channel_name" type="text"  value="<?php if(isset($edit_data["nns_bigtv_channel_name"])){echo $edit_data["nns_bigtv_channel_name"];}?>" rule="noempty"/>
            				</td>
            			</tr>
                	   <?php }?>
            			
            		  </tbody>
            		</table>
            	</div>
			</form>
        	<div class="controlbtns">
        		<div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','<?php echo cms_get_lang('webkz_qrccxg'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
        		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        		<div style="clear:both;"></div>
        	</div>
        </div>
    </body>
</html>
