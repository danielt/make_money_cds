<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$action=$_GET["action"];
$nns_id=$_REQUEST["nns_id"];
$pri_bool=false;
if (!empty($action)){
	switch($action){
	case "edit":
		$action_str=cms_get_lang('save');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136002");
		break;
	case "add":
		$action_str=cms_get_lang('add');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136003");
		break;
	default:
		break;
	}
}
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';

$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);
$edit_data = $_REQUEST;






/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/producer/producer.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';
$dc = nl_get_dc(array (
    "db_policy" => NL_DB_WRITE,
    "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));
if ($action=="edit"){
	$edit_data = nl_producer::query_by_id($dc,$nns_id);
	
	if($edit_data['ret'] != 0 )
	{
		echo "<script>alert('". $action_str. $edit_data['reason']. "');</script>";
		echo "<script>self.location='".$base_file_name_base."_list.php;</script>";
	}
	$edit_data = $edit_data['data_info'];
}
// var_dump($edit_data);
$result_data_cp = nl_cp::query_all($dc);
if($result_data_cp['ret'] != 0)
{
    echo '<script>alert("'.$result_data_cp['reason'].'");history.go(-1);</script>';die;
}
$result_data_cp = isset($result_data_cp['data_info']) ? $result_data_cp['data_info'] : null;
$result_data_cp_last=array();
if(is_array($result_data_cp) && !empty($result_data_cp))
{
    foreach ($result_data_cp as $cp_val)
    {
        $result_data_cp_last[$cp_val['nns_id']] = $cp_val['nns_name'];
    }
}
unset($result_data_cp);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link href="../../css/comm.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
        <script language="javascript">
        
        </script>
    </head>
    <body>
        <div class="content">
        	<div class="content_position">内容提供商 > 内容提供商 添加/修改</div>
        	<form id="add_form" action="<?php echo $base_file_name_base;?>_control.php" method="post">
            	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
            	<div class="content_table">
            		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            		  <tbody>
            		    <?php if($action == 'edit'){
            		        $result_data_cp_last_key = !empty($result_data_cp_last) ? array_keys($result_data_cp_last) : array();
            		    ?>
                		    <tr>
                				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>GUID:</td>
                				<td>
                				    <input name="nns_id" id="nns_id" type="hidden"  value="<?php if(isset($edit_data["nns_id"])){echo $edit_data["nns_id"];}?>" rule="noempty"/>
                				    <input type="text" disabled value="<?php if(isset($edit_data["nns_id"])){echo $edit_data["nns_id"];}?>" />
                				</td>
                			</tr>
                	   <?php }?>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>CP名称:</td>
            				<td>
            				    <select name='nns_cp_id' style="width:150px;">
                                    <option value=''>--请选择--</option>
                                    <?php if(!empty($result_data_cp_last)){foreach ($result_data_cp_last as $model_key=>$model_val){?>
                                    <option value="<?php echo $model_key;?>" <?php if(isset($edit_data['nns_cp_id']) && $edit_data['nns_cp_id'] == $model_key){ echo 'selected="selected"';}?>><?php echo $model_val;?></option>
                                    <?php }}?>
                                </select>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>影片类型:</td>
            				<td>
            				    <select name='nns_type' style="width:150px;">
                                    <option value=''>--请选择--</option>
                                    <option value='media' <?php if(isset($edit_data['nns_type']) && $edit_data['nns_type'] == 'media'){ echo 'selected="selected"';}?>>点播</option>
                                    <option value='live_media' <?php if(isset($edit_data['nns_type']) && $edit_data['nns_type'] == 'live_media'){ echo 'selected="selected"';}?>>直播</option>
                                    <option value='playbill' <?php if(isset($edit_data['nns_type']) && $edit_data['nns_type'] == 'playbill'){ echo 'selected="selected"';}?>>节目单</option>
                                </select>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>内容提供商ID:</td>
            				<td>
            				    <input name="nns_import_source" id="nns_import_source" type="text"  value="<?php if(isset($edit_data["nns_import_source"])){echo $edit_data["nns_import_source"];}?>" rule="noempty" />
            				</td>
        			    </tr>
                        <tr>
                            <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>权重优先级</td>
                            <td>
                                <input name="nns_weight" id="nns_weight" type="text"  value="<?php if(isset($edit_data["nns_weight"])){echo $edit_data["nns_weight"];}?>"/>
                            </td>
                        </tr>
        			    <tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>内容提供商名称:</td>
            				<td>
            				    <input name="nns_producer" id="nns_producer" type="text"  value="<?php if(isset($edit_data["nns_producer"])){echo $edit_data["nns_producer"];}?>" rule="noempty" />
            				</td>
        			    </tr>
            			
            			<tr>
            				<td class="rightstyle">DOMAIN:</td>
            				<td><input name="nns_domain" id="nns_domain" type="text" value="<?php if(isset($edit_data["nns_domain"])){ echo $edit_data["nns_domain"];}else{ echo '';}?>"/>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle">节点ID:</td>
            				<td><input name="nns_node_id" id="nns_node_id" type="text" value="<?php if(isset($edit_data["nns_node_id"])){ echo $edit_data["nns_node_id"];}else{ echo '';}?>"/>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle">节点列表:</td>
            				<td><input name="nns_list_ids" id="nns_list_ids" type="text" value="<?php if(isset($edit_data["nns_list_ids"])){ echo $edit_data["nns_list_ids"];}else{ echo '';}?>"/>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle">联系人</td>
            				<td><input name="nns_contactor" id="nns_contactor" type="text" value="<?php if(isset($edit_data["nns_contactor"])){ echo $edit_data["nns_contactor"];}else{ echo '';}?>"/>
            				</td>
            			</tr>
            			<tr>
            					<td class="rightstyle">联系电话</td>
            					<td><input name="nns_telephone" id="nns_telephone" rule="phone" type="text" value="<?php if(isset($edit_data["nns_telephone"])){ echo $edit_data["nns_telephone"];}else{ echo '';}?>"/>
            					</td>
            			</tr>
            			<tr>
            					<td class="rightstyle">Eamil</td>
            					<td><input name="nns_email" id="nns_email" rule="email" type="text" value="<?php if(isset($edit_data["nns_email"])){ echo $edit_data["nns_email"];}else{ echo '';}?>"/>
            					</td>
            			</tr>
            			<tr>
            				<td class="rightstyle">地址</td>
            				<td><input name="nns_addr" id="nns_addr" type="text" value="<?php if(isset($edit_data["nns_addr"])){ echo $edit_data["nns_addr"];}else{ echo '';}?>"/>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle">描述</td>
            				<td><textarea name="nns_desc" id="nns_desc" cols="" rows=""><?php if(isset($edit_data["nns_desc"])){ echo $edit_data["nns_desc"];}else{ echo '';}?></textarea>
            				</td>
            			</tr>
            		  </tbody>
            		</table>
            	</div>
        	</form>
        	<div class="controlbtns">
        		<div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','<?php echo cms_get_lang('webkz_qrccxg'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
        		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        		<div style="clear:both;"></div>
        	</div>
        </div>
    </body>
</html>
