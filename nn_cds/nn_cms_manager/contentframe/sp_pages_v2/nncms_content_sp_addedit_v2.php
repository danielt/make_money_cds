<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/11/22 10:04
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
    $language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$action=$_GET["action"];
$nns_id=$_GET["nns_id"];
$pri_bool=false;
if (!empty($action)){
    switch($action){
        case "edit":
            $action_str=cms_get_lang('save');
            $pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136002");
            break;
        case "add":
            $action_str=cms_get_lang('add');
            $pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136003");
            break;
        default:
            break;
    }
}
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");

//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//加载新版
include $nncms_config_path . "v2/common.php";
\ns_core\m_load::load("ns_core.m_config");

if ($action=="edit"){
    $result_sp = \m_config::_get_sp_info($nns_id);
    $edit_data = isset($result_sp['data_info'][0]) ? $result_sp['data_info'][0] : array();
    $edit_data['nns_config'] = isset($edit_data['nns_config']) ? json_decode($edit_data['nns_config'], true) : array();

}
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>

    <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
    <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
    <link href="../../css/comm.css" rel="stylesheet" type="text/css" />
    <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
    <script language="javascript" src="../../js/cms_cookie.js"></script>
    <script language="javascript" src="../../js/table.js.php"></script>
    <script language="javascript" src="../../js/checkinput.js.php"></script>
    <script language="javascript" src="../../js/rate.js"></script>
    <script language="javascript" src="../../js/image_loaded_func.js"></script>
    <script language="javascript" src="../../js/alertbox.js"></script>
    <script type="text/javascript">
        function change()
        {
            var radio = document.getElementsByName("nns_config[cdn_send_mode]");
            var radioLength = radio.length;
            for(var i =0;i < radioLength;i++)
            {
                if(radio[i].checked)
                {
                    var radioValue = radio[i].value;
                    if(radioValue == 1)
                    {
                        $('.cdn_send_mode_url').show();
                        $('.ftp_cdn_send_mode_url').hide();
                    }
                    else
                    {
                        $('.cdn_send_mode_url').hide();
                    }
                    if(radioValue == 2)
                    {
                        $('.cdn_send_mode_url').hide();
                        $('.ftp_cdn_send_mode_url').show();
                    }
                    else
                    {
                        $('.ftp_cdn_send_mode_url').hide();
                    }
                }
            }

            window.parent.resetFrameHeight();
        }

        //转码开关，开启时展示
        function change1()
        {
            var radio = document.getElementsByName("nns_config[clip_file_encode_enable]");
            var radioLength = radio.length;
            for(var i =0;i < radioLength;i++)
            {
                if(radio[i].checked)
                {
                    var radioValue = radio[i].value;
                    if(radioValue == 1)
                    {
                        $('.class_clip_to_encode').show();
                    }
                    else
                    {
                        $('.class_clip_to_encode').hide();
                    }
                }
            }

            window.parent.resetFrameHeight();
        }

        //按钮开关，是否开启，隐藏/展示
        function change2(type_name, class_name)
        {
            var radio = document.getElementsByName(type_name);
            var radioLength = radio.length;
            for(var i =0;i < radioLength;i++)
            {
                if(radio[i].checked)
                {
                    var radioValue = radio[i].value;
                    if(radioValue == 1)
                    {
                        $('.' + class_name).show();
                    }
                    else
                    {
                        $('.' + class_name).hide();
                    }
                }
            }

            window.parent.resetFrameHeight();
        }

        function delete_global_config()
        {
            $("input[name='nns_config[global_config_bind_key]']").val("")
        }
        function begin_select_group_config()
        {
            var group_nns_id = "<?php echo $edit_data['nns_config']["global_config_bind_key"]; ?>"
            $("#select_frame").attr("src","nncms_content_bind_group_config_list.php?action=sp&group_id="+group_nns_id);
            $(".selectbox").show();
            $('.selectbox iframe').load(function () {
                $(".selectbox iframe").css({
                    width:800,
                    height:400,
                });
                $(".selectbox").css({
                    top: 0
                });
            });
        }
        function close_select() {
            $(".selectbox").hide();
        }
        function show_hide_drm_config(class_css,flag)
        {
            var obj_class = $(class_css);
            var rule_str = '';
            if(flag == 1)
            {
                $(class_css).show();
                $(class_css+'_skip_rule').show();
                rule_str='noempty';
            }
            else
            {
                $(class_css).hide();
                $(class_css+'_skip_rule').hide();
            }
            if(obj_class.length < 1)
            {
                return false;
            }
            for(var i=0;i<obj_class.length;i++)
            {
                obj_class.eq(i).children('td').eq(1).children('input').eq(0).attr('rule',rule_str);
            }
        }
        $(document).ready(function(){
            var value_1 = $("input[name='nns_config[clip_drm_enabled]']:checked").val();
            value_1 = value_1 == 1 ? 1 : 0;
            show_hide_drm_config('.clip_drm_class',value_1);
            var value_2 = $("input[name='nns_config[main_drm_enabled]']:checked").val();
            value_2 = value_2 == 1 ? 1 : 0;
            show_hide_drm_config('.q_drm_class',value_2);
            var value_3 = $("input[name='nns_config[message_feedback_mode]']:checked").val();
            value_3 = value_3 == 1 ? 0 : 1;
            show_hide_drm_config('.message_feedback_mode_class',value_3);
            //切片后是否上传到ftp
            var value_4 = $("input[name='nns_config[upload_clip_to_ftp_enabled]']:checked").val();
            value_4 = value_4 == 1 ? 1 : 0;
            show_hide_drm_config('.q_upload_clip_to_ftp_class',value_4);
            //是否开启转码
            var value_5 = $("input[name='nns_config[clip_file_encode_enable]']:checked").val();
            value_5 = value_5 == 1 ? 1 : 0;
            show_hide_drm_config('.class_clip_to_encode',value_5);
            //是否开启上下线
            var value_6 = $("input[name='nns_config[online_disabled]']:checked").val();
            value_6 = value_6 == 1 ? 1 : 0;
            show_hide_drm_config('.class_online_disabled',value_6);
            //是否开启码率配置
            var value_7 = $("input[name='nns_config[import_ratebit_enabled]']:checked").val();
            value_7 = value_7 == 1 ? 1 : 0;
            show_hide_drm_config('.class_import_ratebit_enabled',value_7);

            $("input[name='nns_config[clip_drm_enabled]']").change(function(){
                var value_1 = $("input[name='nns_config[clip_drm_enabled]']:checked").val();
                value_1 = value_1 == 1 ? 1 : 0;
                show_hide_drm_config('.clip_drm_class',value_1);
                window.parent.resetFrameHeight();
            });
            $("input[name='nns_config[main_drm_enabled]']").change(function(){
                var value_2 = $("input[name='nns_config[main_drm_enabled]']:checked").val();
                value_2 = value_2 == 1 ? 1 : 0;
                show_hide_drm_config('.q_drm_class',value_2);
                window.parent.resetFrameHeight();
            });
            $("input[name='nns_config[message_feedback_mode]']").change(function(){
                var value_3 = $("input[name='nns_config[message_feedback_mode]']:checked").val();
                value_3 = value_3 == 1 ? 0 : 1;
                show_hide_drm_config('.message_feedback_mode_class',value_3);
                window.parent.resetFrameHeight();
            });
            <!--xinxin.deng 2017年11月09日14:55:00  start 添加clip是否上传到ftp-->
            $("input[name='nns_config[upload_clip_to_ftp_enabled]']").change(function(){
                var value_4 = $("input[name='nns_config[upload_clip_to_ftp_enabled]']:checked").val();
                value_4 = value_4 == 1 ? 1 : 0;
                show_hide_drm_config('.q_upload_clip_to_ftp_class',value_4);
                window.parent.resetFrameHeight();
            });
            <!--xinxin.deng 2017年11月09日14:55:00  end 添加clip是否上传到ftp-->

            <!--  start 添加转码开关是否开启-->
            $("input[name='nns_config[clip_file_encode_enable]']").change(function(){
                var value_5 = $("input[name='nns_config[clip_file_encode_enable]']:checked").val();
                value_5 = value_5 == 1 ? 1 : 0;
                show_hide_drm_config('.class_clip_to_encode',value_5);
                window.parent.resetFrameHeight();
            });
            <!--  end 添加转码开关是否开启-->

            <!--  start 添加媒资上下线开关是否开启-->
            $("input[name='nns_config[online_disabled]']").change(function(){
                var value_6 = $("input[name='nns_config[online_disabled]']:checked").val();
                value_6 = value_6 == 1 ? 1 : 0;
                show_hide_drm_config('.class_online_disabled',value_6);
                window.parent.resetFrameHeight();
            });
            <!--  end 添加媒资上下线开关是否开启-->

            <!--  start 添加码率配置开关是否开启-->
            $("input[name='nns_config[import_ratebit_enabled]']").change(function(){
                var value_7 = $("input[name='nns_config[import_ratebit_enabled]']:checked").val();
                value_7 = value_7 == 1 ? 1 : 0;
                show_hide_drm_config('.class_import_ratebit_enabled',value_7);
                window.parent.resetFrameHeight();
            });
            <!--  end 添加码率配置开关是否开启-->
            change();
            window.parent.resetFrameHeight();
        });


    </script>
</head>

<body>
<div class="selectbox" style="display: none;">
    <iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
</div>
<div class="content">
    <div class="content_position">SP 管理 > SP 添加/修改</div>
    <form id="add_form" action="nncms_content_sp_control.php" method="post">
        <input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
        <div class="content_table">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span><?php echo
                        cms_get_lang('id');?>:</td>
                    <td><input type="text" name="nns_id" rule="noempty" value="<?php echo $nns_id;?>" <?php echo $action ==
                        'edit' ? 'readonly="readonly"' : '';?> /></td>
                </tr>
                <tr>
                    <td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>SP名称:</td>
                    <td><input name="nns_name"  id="nns_name" type="text" rule="noempty" value="<?php if($action=='edit'){echo $edit_data['nns_name'];}?>"/></td>
                </tr>

                <tr>
                    <td class="rightstyle">联系人:</td>
                    <td><input name="nns_contact" id="nns_contact" type="text"  value="<?php if($action=="edit"){echo $edit_data["nns_contact"];}?>" /></td>
                </tr>

                <tr>
                    <td class="rightstyle">联系电话:</td>
                    <td><input name="nns_telphone" id="nns_telphone" type="text" value="<?php if($action=="edit"){echo $edit_data["nns_telphone"];}?>" /></td>
                </tr>

                <tr>
                    <td class="rightstyle">Email:</td>
                    <td><input name="nns_email" id="nns_email" type="text"  value="<?php if($action=="edit"){echo $edit_data["nns_email"];}?>" /></td>
                </tr>
                <tr>
                    <td class="rightstyle"><b>SP全局配置</b></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">CDN注入ID模式</td>
                    <td><input name="nns_config[import_id_mode]" id="nns_config" type="text" value="<?php if($action=="edit"){echo $edit_data['nns_config']['import_id_mode'];}?>" />(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;11:湖南注入中兴cdn专用)</td>
                </tr>
                <tr>
                    <td class="rightstyle">EPG主媒资注入ID模式</td>
                    <td><input name="nns_config[epg_video_import_id_mode]" id="nns_config" type="text" value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_video_import_id_mode'];}?>" />(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;)</td>
                </tr>
                <tr>
                    <td class="rightstyle">EPG分集注入ID模式</td>
                    <td><input name="nns_config[epg_index_import_id_mode]" id="nns_config" type="text" value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_index_import_id_mode'];}?>" />(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;)</td>
                </tr>
                <tr>
                    <td class="rightstyle">EPG片源注入ID模式</td>
                    <td><input name="nns_config[epg_media_import_id_mode]" id="nns_config" type="text" value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_media_import_id_mode'];}?>" />(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;11:湖南注入中兴cdn专用)</td>
                </tr>
                <tr>
                    <td class="rightstyle">EPG直播频道注入ID模式</td>
                    <td><input name="nns_config[epg_live_import_id_mode]" id="nns_config" type="text" value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_live_import_id_mode'];}?>" />(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;)</td>
                </tr>
                <tr>
                    <td class="rightstyle">EPG直播分集注入ID模式</td>
                    <td><input name="nns_config[epg_live_index_import_id_mode]" id="nns_config" type="text" value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_live_index_import_id_mode'];}?>" />(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;)</td>
                </tr>
                <tr>
                    <td class="rightstyle">EPG直播源注入ID模式</td>
                    <td><input name="nns_config[epg_live_media_import_id_mode]" id="nns_config" type="text" value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_live_media_import_id_mode'];}?>" />(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;)</td>
                </tr>
                <tr>
                    <td class="rightstyle">EPG节目单注入ID模式</td>
                    <td><input name="nns_config[epg_playbill_import_id_mode]" id="nns_config" type="text" value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_playbill_import_id_mode'];}?>" />(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;)</td>
                </tr>
                <tr>
                    <td class="rightstyle">消息反馈开关：</td>
                    <td>
                        <input name="nns_config[message_feedback_enabled]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['message_feedback_enabled'] == 1){echo "checked";}?>/>  关闭
                        <input name="nns_config[message_feedback_enabled]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['message_feedback_enabled']) || $edit_data['nns_config']['message_feedback_enabled'] != 1){echo "checked";}?>/>  开启
                        【关闭（消息反馈关闭），开启【消息反馈开启】由于加了播控注入播控逻辑，上游播控可以关闭消息反馈】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">消息反馈模式</td>
                    <td>
                        <input	 name="nns_config[message_feedback_mode]" id="nns_config" type="radio"  value="0" <?php if(isset($edit_data['nns_config']['message_feedback_mode']) && $edit_data['nns_config']['message_feedback_mode'] == 0){echo "checked='checked'";} ?> />原反馈模式
                        <input	 name="nns_config[message_feedback_mode]" id="nns_config" type="radio"  value="1" <?php if(isset($edit_data['nns_config']['message_feedback_mode']) && $edit_data['nns_config']['message_feedback_mode'] == 1){echo "checked='checked'";} ?> />CP反馈模式
                    </td>
                </tr>
                <tr class="message_feedback_mode_class">
                    <td class="rightstyle">站点ID</td>
                    <td><input	name="nns_config[site_id]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['site_id'];}?>" /></td>
                </tr>
                <tr class="message_feedback_mode_class">
                    <td class="rightstyle">站点反馈接口</td>
                    <td><input	 name="nns_config[site_callback_url]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['site_callback_url'];}?>" /></td>
                </tr>
                <tr>
                    <td class="rightstyle">通用配置分组绑定</td>
                    <td>
                        <input name="nns_config[global_config_bind_key]" id="nns_config" type="text" readonly="readonly" value="<?php if($action=="edit"){echo $edit_data['nns_config']['global_config_bind_key'];}?>" />
                        <input name="binding_global_config_bind_key" id="binding_global_config_bind_key" type="button" onclick="begin_select_group_config()" value="配置绑定">
                        <input name="unbundling_global_config_bind_key" id="unbundling_global_config_bind_key" type="button" onclick="delete_global_config()" value="解除绑定">(全局绑定配置项)
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle"><b>注入码率配置</b></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">码率配置开关：</td>
                    <td>
                        <input name="nns_config[import_ratebit_enabled]" id="nns_config" onchange ="change2('nns_config[import_ratebit_enabled]', 'class_import_ratebit_enabled');" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['import_ratebit_enabled']) || $edit_data['nns_config']['import_ratebit_enabled'] == 0){echo "checked";}?>/>  关闭
                        <input name="nns_config[import_ratebit_enabled]" id="nns_config" onchange ="change2('nns_config[import_ratebit_enabled]', 'class_import_ratebit_enabled');" type="radio" value="1" <?php if($edit_data['nns_config']['import_ratebit_enabled'] == 1){echo "checked";}?>/>  开启
                        &nbsp;&nbsp;【关闭任何片源码率都通过该SP，开启了就通过片源码率开始值和结束值判断】
                    </td>
                </tr>
                <tr class="class_import_ratebit_enabled">
                    <td class="rightstyle">片源码率开始值：</td>
                    <td><input	name="nns_config[import_ratebit_start]" style="width:100px;" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['import_ratebit_start'];}?>" />	 如果为配置则片源码率无下限，如果配置了片源码率至少大于等于该开始值</td>
                </tr>
                <tr class="class_import_ratebit_enabled">
                    <td class="rightstyle">片源码率结束值：</td>
                    <td><input	name="nns_config[import_ratebit_end]" style="width:100px;" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['import_ratebit_end'];}?>" />	 如果为配置则片源码率无上限，如果配置了片源码率至少小于等于该开始值</td>
                </tr>
                <tr>
                    <td class="rightstyle"><b>媒资上下线配置</b></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">媒资上下线开关</td>
                    <td>
                        <input name="nns_config[online_disabled]" id="nns_config" onchange ="change2('nns_config[online_disabled]', 'class_online_disabled');" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['online_disabled']) || $edit_data['nns_config']['online_disabled'] == 0){echo "checked";}?>/>  关闭
                        <input name="nns_config[online_disabled]" id="nns_config" onchange ="change2('nns_config[online_disabled]', 'class_online_disabled');" type="radio" value="1" <?php if($edit_data['nns_config']['online_disabled'] == 1){echo "checked";}?>/>  开启
                        &nbsp;&nbsp;【关闭后不生成上下线指令,默认关闭】
                    </td>
                </tr>
                <!--媒资上下线start-->
                <tr class="class_online_disabled">
                    <td class="rightstyle">媒资上下线注入模式</td>
                    <td>
                        <input name="nns_config[asset_unline_model]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['asset_unline_model']) || $edit_data['nns_config']['asset_unline_model'] == 0){echo "checked";}?>/>  redis消息队列上下线控制
                        <input name="nns_config[asset_unline_model]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['asset_unline_model'] == 1){echo "checked";}?>/>  epg注入上下线控制
                        &nbsp;&nbsp;【默认redis消息队列上下线控制】
                    </td>
                </tr>
                <tr class="class_online_disabled">
                    <td class="rightstyle">审核开关</td>
                    <td>
                        <input name="nns_config[online_audit]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['online_audit']) || $edit_data['nns_config']['online_audit'] == 0){echo "checked";}?>/>  自动审核
                        <input name="nns_config[online_audit]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['online_audit'] == 1){echo "checked";}?>/>  未审核
                        <input name="nns_config[online_audit]" id="nns_config" type="radio" value="2" <?php if($edit_data['nns_config']['online_audit'] == 2){echo "checked";}?>/>  下线未审核/上线自动审核
                        &nbsp;&nbsp;【默认媒资上下线为自动审核】
                    </td>
                </tr>
                <tr class="class_online_disabled">
                    <td class="rightstyle">验证最终状态开关</td>
                    <td>
                        <input name="nns_config[check_video_statuc_enabled]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['check_video_statuc_enabled'] == 1){echo "checked";}?>/>  否
                        <input name="nns_config[check_video_statuc_enabled]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['check_video_statuc_enabled']) || $edit_data['nns_config']['check_video_statuc_enabled'] == 0){echo "checked";}?>/>  是
                        &nbsp;&nbsp;【默认是】
                    </td>
                </tr>
                <tr class="class_online_disabled">
                    <td class="rightstyle">是否通知上下线队列开关</td>
                    <td>
                        <input name="nns_config[notify_online_queue_enabled]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['notify_online_queue_enabled'] == 1){echo "checked";}?>/>  否
                        <input name="nns_config[notify_online_queue_enabled]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['notify_online_queue_enabled']) || $edit_data['nns_config']['notify_online_queue_enabled'] == 0){echo "checked";}?>/>  是
                        &nbsp;&nbsp;【操作队列注入为最终状态是否通知上下线，默认 否】
                    </td>
                </tr>
                <tr class="class_online_disabled">
                    <td class="rightstyle">第三方媒资上下线关系注入开关</td>
                    <td>
                        <input name="nns_config[third_category_enabled]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['third_category_enabled']) || $edit_data['nns_config']['third_category_enabled'] == 0){echo "checked";}?>/>  否
                        <input name="nns_config[third_category_enabled]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['third_category_enabled'] == 1){echo "checked";}?>/>  是
                        &nbsp;&nbsp;【是否属于第三方媒资上下线关系注入，默认否】
                    </td>
                </tr>
                <tr class="class_online_disabled">
                    <td class="rightstyle">EPG上下线下发方式</td>
                    <td>
                        <input name="nns_config[online_send_way]" id="nns_config" type="radio" value="0" <?php if($edit_data['nns_config']['third_category_enabled'] == 0){echo "checked";}?>/>  FTP
                        <input name="nns_config[online_send_way]" id="nns_config" type="radio" value="1" <?php if(!isset($edit_data['nns_config']['online_send_way']) || $edit_data['nns_config']['online_send_way'] == 1){echo "checked";}?>/>  HTTP
                        &nbsp;&nbsp;【默认HTTP】
                    </td>
                </tr>
                <tr class="class_online_disabled">
                    <td class="rightstyle">媒资上下线EPG注入接口</td>
                    <td><input  name="nns_config[online_api]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['online_api'];}else{ echo '';}?>" /> 媒资上下线注入接口url地址</td>
                </tr>
                <!--媒资上下线end-->
                <tr>
                    <td class="rightstyle"><b>中心注入指令配置</b></td>
                    <td> </td>
                </tr>

                <tr>
                    <td class="rightstyle">中心注入指令开关：</td>
                    <td>
                        <input name="nns_config[import_op_enabled]" id="nns_config" type="radio" value="0" <?php if($edit_data['nns_config']['import_op_enabled'] == 0){echo "checked";}?>/>  开启
                        <input name="nns_config[import_op_enabled]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['import_op_enabled'] == 1){echo "checked";}?>/>  关闭
                        &nbsp;&nbsp;【关闭后不再注入中心注入指令，默认开启】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">中心注入指令消息处理模式：</td>
                    <td>
                        <input name="nns_config[op_queue_model]" id="nns_config" type="radio" value="0" <?php if($edit_data['nns_config']['op_queue_model'] == 0){echo "checked";}?>/>  播控作为TV平台方（原逻辑）
                        <input name="nns_config[op_queue_model]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['op_queue_model'] == 1){echo "checked";}?>/>  播控作为CDN平台方（新逻辑）
                        【播控以不同的角色展示给客户，即可以作为CDN平台，也可以作为TV平台】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">中心注入指令下发条数</td>
                    <td><input style="width:100px;"  name="nns_config[import_op_pre_max]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['import_op_pre_max'];}?>" /> 【每次取多少个操作任务（不设置为20）】</td>
                </tr>
                <tr>
                    <td class="rightstyle">队列允许通过的媒资开关：</td>
                    <td>
                        <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="video"      <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('video', $edit_data['nns_config']['op_queue_video_enabled']))      || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked";}?>/>  主媒资
                        <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="index"      <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('index', $edit_data['nns_config']['op_queue_video_enabled']))      || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked";}?>/>  分集
                        <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="media"      <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('media', $edit_data['nns_config']['op_queue_video_enabled']))      || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked";}?>/>  片源
                        <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="live"       <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('live', $edit_data['nns_config']['op_queue_video_enabled']))       || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked";}?>/>  直播频道
                        <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="live_index" <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('live_index', $edit_data['nns_config']['op_queue_video_enabled'])) || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked";}?>/>  频道分集
                        <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="live_media" <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('live_media', $edit_data['nns_config']['op_queue_video_enabled'])) || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked";}?>/>  直播源
                        <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="playbill"   <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('playbill', $edit_data['nns_config']['op_queue_video_enabled']))   || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked";}?>/>  节目单
                        <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="file"       <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('file', $edit_data['nns_config']['op_queue_video_enabled']))       || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked";}?>/>  文件
                        <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="category"   <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('category', $edit_data['nns_config']['op_queue_video_enabled']))   || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked";}?>/>  资源库栏目
                        <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="product"    <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('product', $edit_data['nns_config']['op_queue_video_enabled']))    || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked";}?>/>  产品包
                        <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="seekpoint"    <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('seekpoint', $edit_data['nns_config']['op_queue_video_enabled']))    || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked";}?>/>  打点信息
                        <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="epg_file"    <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('epg_file', $edit_data['nns_config']['op_queue_video_enabled']))    || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked";}?>/>  EPGFile模板文件
                        【未选择队列不在接收此类型文件，默认全选中】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">队列允许通过的片源文件类型：</td>
                    <td>
                        <input name="nns_config[op_queue_filetype_enabled][]" id="nns_config" type="checkbox" value="ts"      <?php if((isset($edit_data['nns_config']['op_queue_filetype_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_filetype_enabled']) && in_array('ts', $edit_data['nns_config']['op_queue_filetype_enabled']))      || !isset($edit_data['nns_config']['op_queue_filetype_enabled'])){echo "checked";}?>/>  ts
                        <input name="nns_config[op_queue_filetype_enabled][]" id="nns_config" type="checkbox" value="m3u8"      <?php if((isset($edit_data['nns_config']['op_queue_filetype_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_filetype_enabled']) && in_array('m3u8', $edit_data['nns_config']['op_queue_filetype_enabled']))      || !isset($edit_data['nns_config']['op_queue_filetype_enabled'])){echo "checked";}?>/>  m3u8
                        <input name="nns_config[op_queue_filetype_enabled][]" id="nns_config" type="checkbox" value="flv"      <?php if((isset($edit_data['nns_config']['op_queue_filetype_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_filetype_enabled']) && in_array('flv', $edit_data['nns_config']['op_queue_filetype_enabled']))      || !isset($edit_data['nns_config']['op_queue_filetype_enabled'])){echo "checked";}?>/>  flv
                        <input name="nns_config[op_queue_filetype_enabled][]" id="nns_config" type="checkbox" value="mp4"       <?php if((isset($edit_data['nns_config']['op_queue_filetype_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_filetype_enabled']) && in_array('mp4', $edit_data['nns_config']['op_queue_filetype_enabled']))       || !isset($edit_data['nns_config']['op_queue_filetype_enabled'])){echo "checked";}?>/>  mp4
                        <input name="nns_config[op_queue_filetype_enabled][]" id="nns_config" type="checkbox" value="mp3" <?php if((isset($edit_data['nns_config']['op_queue_filetype_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_filetype_enabled']) && in_array('mp3', $edit_data['nns_config']['op_queue_filetype_enabled'])) || !isset($edit_data['nns_config']['op_queue_filetype_enabled'])){echo "checked";}?>/>  mp3
                        <input name="nns_config[op_queue_filetype_enabled][]" id="nns_config" type="checkbox" value="mpg" <?php if((isset($edit_data['nns_config']['op_queue_filetype_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_filetype_enabled']) && in_array('mpg', $edit_data['nns_config']['op_queue_filetype_enabled'])) || !isset($edit_data['nns_config']['op_queue_filetype_enabled'])){echo "checked";}?>/>  mpg
                        <input name="nns_config[op_queue_filetype_enabled][]" id="nns_config" type="checkbox" value="mts" <?php if((isset($edit_data['nns_config']['op_queue_filetype_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_filetype_enabled']) && in_array('mts', $edit_data['nns_config']['op_queue_filetype_enabled'])) || !isset($edit_data['nns_config']['op_queue_filetype_enabled'])){echo "checked";}?>/>  mts
                        【未选择队列不在接收此片源文件类型，默认全选中】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">队列不允许通过的片源清晰度：</td>
                    <td>
                        <input name="nns_config[media_import_op_mode][]" id="nns_config" type="checkbox" value="low" <?php if((isset($edit_data['nns_config']['media_import_op_mode'])
                            && is_array($edit_data['nns_config']['media_import_op_mode']) && in_array('low', $edit_data['nns_config']['media_import_op_mode']))){echo "checked";}?>/>  低清(low)
                        <input name="nns_config[media_import_op_mode][]" id="nns_config" type="checkbox" value="std" <?php if((isset($edit_data['nns_config']['media_import_op_mode'])
                            && is_array($edit_data['nns_config']['media_import_op_mode']) && in_array('std', $edit_data['nns_config']['media_import_op_mode']))){echo "checked";}?>/>  标清(std)
                        <input name="nns_config[media_import_op_mode][]" id="nns_config" type="checkbox" value="hd" <?php if((isset($edit_data['nns_config']['media_import_op_mode'])
                            && is_array($edit_data['nns_config']['media_import_op_mode']) && in_array('hd', $edit_data['nns_config']['media_import_op_mode']))){echo "checked";}?>/>  高清(hd)
                        <input name="nns_config[media_import_op_mode][]" id="nns_config" type="checkbox" value="sd" <?php if((isset($edit_data['nns_config']['media_import_op_mode'])
                            && is_array($edit_data['nns_config']['media_import_op_mode']) && in_array('sd', $edit_data['nns_config']['media_import_op_mode']))){echo "checked";}?>/>  超清(sd)
                        <input name="nns_config[media_import_op_mode][]" id="nns_config" type="checkbox" value="4k" <?php if((isset($edit_data['nns_config']['media_import_op_mode'])
                            && is_array($edit_data['nns_config']['media_import_op_mode']) && in_array('4k', $edit_data['nns_config']['media_import_op_mode']))){echo "checked";}?>/>  4K(4k)
                        【已选择的清晰度不会生成中心注入队列，默认全部通过】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">队列允许通过的片源类型开关：</td>
                    <td>
                        <input name="nns_config[op_queue_media_type_enabled][]" id="nns_config" type="checkbox" value="0" <?php if((isset($edit_data['nns_config']['op_queue_media_type_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_media_type_enabled']) && in_array('0', $edit_data['nns_config']['op_queue_media_type_enabled'])) || !isset($edit_data['nns_config']['op_queue_media_type_enabled'])){echo "checked";}?>/>  原始片源
                        <input name="nns_config[op_queue_media_type_enabled][]" id="nns_config" type="checkbox" value="1" <?php if((isset($edit_data['nns_config']['op_queue_media_type_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_media_type_enabled']) && in_array('1', $edit_data['nns_config']['op_queue_media_type_enabled'])) || !isset($edit_data['nns_config']['op_queue_media_type_enabled'])){echo "checked";}?>/>  转码片源
                        <input name="nns_config[op_queue_media_type_enabled][]" id="nns_config" type="checkbox" value="2" <?php if((isset($edit_data['nns_config']['op_queue_media_type_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_media_type_enabled']) && in_array('2', $edit_data['nns_config']['op_queue_media_type_enabled'])) || !isset($edit_data['nns_config']['op_queue_media_type_enabled'])){echo "checked";}?>/>  CDN片源
                        【未选择队列不在接收此片源类型文件，默认全选中】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">队列允许通过的片源终端TAG开关：</td>
                    <td>
                        <input name="nns_config[op_queue_media_tag_enabled][]" id="nns_config" type="checkbox" value="26" <?php if((isset($edit_data['nns_config']['op_queue_media_tag_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_media_tag_enabled']) && in_array('26', $edit_data['nns_config']['op_queue_media_tag_enabled'])) || !isset($edit_data['nns_config']['op_queue_media_tag_enabled'])){echo "checked";}?>/>  STB
                        <input name="nns_config[op_queue_media_tag_enabled][]" id="nns_config" type="checkbox" value="27" <?php if((isset($edit_data['nns_config']['op_queue_media_tag_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_media_tag_enabled']) && in_array('27', $edit_data['nns_config']['op_queue_media_tag_enabled'])) || !isset($edit_data['nns_config']['op_queue_media_tag_enabled'])){echo "checked";}?>/>  IPHONE
                        <input name="nns_config[op_queue_media_tag_enabled][]" id="nns_config" type="checkbox" value="28" <?php if((isset($edit_data['nns_config']['op_queue_media_tag_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_media_tag_enabled']) && in_array('28', $edit_data['nns_config']['op_queue_media_tag_enabled'])) || !isset($edit_data['nns_config']['op_queue_media_tag_enabled'])){echo "checked";}?>/>  IPAD
                        <input name="nns_config[op_queue_media_tag_enabled][]" id="nns_config" type="checkbox" value="29" <?php if((isset($edit_data['nns_config']['op_queue_media_tag_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_media_tag_enabled']) && in_array('29', $edit_data['nns_config']['op_queue_media_tag_enabled'])) || !isset($edit_data['nns_config']['op_queue_media_tag_enabled'])){echo "checked";}?>/>  ANDROID_PHONE
                        <input name="nns_config[op_queue_media_tag_enabled][]" id="nns_config" type="checkbox" value="30" <?php if((isset($edit_data['nns_config']['op_queue_media_tag_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_media_tag_enabled']) && in_array('30', $edit_data['nns_config']['op_queue_media_tag_enabled'])) || !isset($edit_data['nns_config']['op_queue_media_tag_enabled'])){echo "checked";}?>/>  ANDROID_PAD
                        <input name="nns_config[op_queue_media_tag_enabled][]" id="nns_config" type="checkbox" value="31" <?php if((isset($edit_data['nns_config']['op_queue_media_tag_enabled'])
                                && is_array($edit_data['nns_config']['op_queue_media_tag_enabled']) && in_array('31', $edit_data['nns_config']['op_queue_media_tag_enabled'])) || !isset($edit_data['nns_config']['op_queue_media_tag_enabled'])){echo "checked";}?>/>  PC
                        【未选择队列不在接收此片源类型文件，默认全选中，如果片源tag标志为空则默认全部通过，无论设置与否】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle"><b>中心同步指令配置</b></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">中心同步指令开关：</td>
                    <td>
                        <input name="nns_config[op_queue_enabled]" id="nns_config" type="radio" value="0" <?php if($edit_data['nns_config']['op_queue_enabled'] == 0){echo "checked";}?>/>  关闭
                        <input name="nns_config[op_queue_enabled]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['op_queue_enabled'] == 1){echo "checked";}?>/>  开启
                        &nbsp;&nbsp;【关闭后不再注入中心同步指令,默认关闭】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>中心片库下载地址虚拟接口</td>
                    <td><input  rule="noempty"  name="nns_config[down_url]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['down_url'];}?>" /></td>
                </tr>
                <tr>
                    <td class="rightstyle">中心同步指令下发条数</td>
                    <td><input style="width:100px;"  name="nns_config[op_pre_max]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['op_pre_max'];}?>" /> 每次取多少个操作任务（不设置为100）</td>
                </tr>
                <tr>
                    <td class="rightstyle"> 中心同步指令冻结时间</td>
                    <td><input   class="datetimepicker"  style="width:120px;"  name="nns_config[op_task_freeze_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['op_task_freeze_time'];}?>" /> 冻结时间，这个时间以后的全部冻结  格式：YYYY-mm-dd HH:ii:SS</td>
                </tr>
                <tr>
                    <td class="rightstyle">中心同步指令操作排序:</td>
                    <td><input  style="width:300px;" name="nns_config[op_task_order]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['op_task_order'];}?>" /> </td>
                </tr>
                <tr>
                    <td class="rightstyle">中心同步删除指令生成C2虚拟任务:</td>
                    <td>
                        <input name="nns_config[virtual_destroy_task]" id="nns_config" type="radio" value="0" <?php if($edit_data['nns_config']['virtual_destroy_task'] == 0){echo "checked";}?>/>  关闭
                        <input name="nns_config[virtual_destroy_task]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['virtual_destroy_task'] == 1){echo "checked";}?>/>  开启
                        &nbsp;&nbsp;【当删除指令在C2中无历史注入任务时，关闭则删除当前删除指令，开启则生成C2虚拟任务。默认关闭】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">片源服务类型:</td>
                    <td><input  style="width:100px;"  name="nns_config[media_service_type]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_service_type'];}?>" />
                        <font color="red">【片源服务类型标示；同步指令创建片源模式为1后这个参数值必填】</font></td>
                </tr>
                <tr>
                    <td class="rightstyle"><b>切片指令配置</b></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">切片模式：</td>
                    <td>
                        <input name="nns_config[clip_import_model]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['clip_import_model']) || $edit_data['nns_config']['clip_import_model'] == 0){echo "checked";}?>/>  老模式切片下发
                        <input name="nns_config[clip_import_model]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['clip_import_model'] == 1){echo "checked";}?>/>  资源库中片源查询切片下发且只下载不切片
                        &nbsp;&nbsp;【默认老模式切片下发】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">切片CPID下发模式：</td>
                    <td>
                        <input name="nns_config[clip_cp_id_sent_model]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['clip_cp_id_sent_model']) || $edit_data['nns_config']['clip_cp_id_sent_model'] == 0){echo "checked";}?>/>  import_source(片源)
                        <input name="nns_config[clip_cp_id_sent_model]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['clip_cp_id_sent_model'] == 1){echo "checked";}?>/>  producer（主媒资）
                        &nbsp;&nbsp;【默认 import_source(片源)】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>切片任务下发条数</td>
                    <td><input  rule="noempty"  style="width:100px;"  name="nns_config[clip_pre_max]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['clip_pre_max'];}?>" /> 切片每次从操作列队获取多少个（不设置为100）</td>
                </tr>
                <tr>
                    <td class="rightstyle">切片任务下发模式</td>
                    <td>
                        <input name="nns_config[is_starcor_telecom]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['is_starcor_telecom']) || $edit_data['nns_config']['is_starcor_telecom'] == 0){echo "checked";}?>/>  非电信模式
                        <input name="nns_config[is_starcor_telecom]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['is_starcor_telecom'] == 1){echo "checked";}?>/>  电信模式(注入到starcor CDN下发domin和cms_id)
                        &nbsp;&nbsp;【默认 非电信模式】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>切片任务暂停</td>
                    <td><input  rule="noempty"  style="width:100px;"  name="nns_config[disabled_clip]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['disabled_clip'];}?>" />0:开启BK切片任务;1:关闭BK切片任务且不生成注入队列;2:关闭BK切片任务且生成注入队列;3:切片指令栏目展示且不生成注入队列，切片栏目只提供展示，且CDN拉片在片源操作只下载片源</td>
                </tr>
                <tr>
                    <td class="rightstyle">下载or切片完成是否上传至FTP</td>
                    <td>
                        <input name="nns_config[upload_clip_to_ftp_enabled]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['upload_clip_to_ftp_enabled']) || $edit_data['nns_config']['upload_clip_to_ftp_enabled'] == 0){echo "checked";}?>/>  否
                        <input name="nns_config[upload_clip_to_ftp_enabled]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['upload_clip_to_ftp_enabled'] == 1){echo "checked";}?>/>  是
                        【空或0表示否，默认0，0否 1是】
                    </td>
                </tr>
                <tr class='q_upload_clip_to_ftp_class'>
                    <td class="rightstyle">下载or切片完成上传至指定FTP目录:</td>
                    <td>
                        <input style="width:30px;" name="nns_config[upload_clip_to_ftp_dir_mode]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['upload_clip_to_ftp_dir_mode'];}?>" />
                        &nbsp;&nbsp;【未配置或配置成0：不额外指定FTP目录，上传至配置的FTP绝对地址 ； 1：根据片源的nns_ext_url字段进行额外目录的指定，需注意ext_url需为字符串】
                    </td>
                </tr>
                <tr class='q_upload_clip_to_ftp_class'>
                    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>下载or切片完成上传至FTP地址:</td>
                    <td>
                        <input rule="noempty" style="width:500px;" name="nns_config[upload_clip_to_ftp_url]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['upload_clip_to_ftp_url'];}?>" />
                        &nbsp;&nbsp;【下载or切片完成上传至FTP地址，绝对路径】
                    </td>
                </tr>

                <tr class="clip_file_encode_enable">
                    <td class="rightstyle">转码开关：</td>
                    <td>
                        <input name="nns_config[clip_file_encode_enable]" id="nns_config" onchange ="change2('nns_config[clip_file_encode_enable]', 'class_clip_to_encode');" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['clip_file_encode_enable']) || $edit_data['nns_config']['clip_file_encode_enable'] == 0){echo "checked";}?>/>  关闭
                        <input name="nns_config[clip_file_encode_enable]" id="nns_config" onchange ="change2('nns_config[clip_file_encode_enable]', 'class_clip_to_encode');" type="radio" value="1" <?php if($edit_data['nns_config']['clip_file_encode_enable'] == 1){echo "checked";}?>/>  开启
                        【切片完成后是否需要转码 开启:需要;关闭:不需要】
                    </td>
                </tr>
                <tr class="class_clip_to_encode">
                    <td class="rightstyle">转码模式：</td>
                    <td>
                        <input name="nns_config[clip_file_encode_model]" id="nns_config" type="radio" value="0" <?php if($edit_data['nns_config']['clip_file_encode_model'] != 1 && $edit_data['nns_config']['clip_file_encode_model'] != 2){echo "checked";}?>/>  切片与转码在同个队列中
                        <input name="nns_config[clip_file_encode_model]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['clip_file_encode_model'] == 1){echo "checked";}?>/>  切片与转码队列分离
                        <input name="nns_config[clip_file_encode_model]" id="nns_config" type="radio" value="2" <?php if($edit_data['nns_config']['clip_file_encode_model'] == 2){echo "checked";}?>/>  不需要切片就可以转码
                        【转码模式机制 兼容以前重庆有线转码队列】
                    </td>
                </tr>
                <tr class="class_clip_to_encode">
                    <td class="rightstyle">转码负载均衡开关：</td>
                    <td>
                        <input name="nns_config[transcode_load_balance_encode]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['transcode_load_balance_encode']) || $edit_data['nns_config']['transcode_load_balance_encode'] == 0){echo "checked";}?>/>  关闭
                        <input name="nns_config[transcode_load_balance_encode]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['transcode_load_balance_encode'] == 1){echo "checked";}?>/>  开启
                        &nbsp;&nbsp;【转码负载均衡配置 0或者未配置  关闭 | 1 开启】
                    </td>
                </tr>
                <tr class="class_clip_to_encode">
                    <td class="rightstyle">转码请求厂商模式：</td>
                    <td><input style="width:100px;"  name="nns_config[clip_encode_model]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['clip_encode_model'];}?>" />  【0或者未配置 锐马视讯转码；1大洋转码；2云视转码】</td>
                </tr>
                <tr class="class_clip_to_encode">
                    <td class="rightstyle">等待转码条数限制：</td>
                    <td><input style="width:100px;"  name="nns_config[clip_encode_wait_max]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['clip_encode_wait_max'];}?>" />  【配置后允许等待转码并发的条数，未配置默认为5】</td>
                </tr>
                <tr class="class_clip_to_encode">
                    <td class="rightstyle">转码失败条数限制：</td>
                    <td><input style="width:100px;"  name="nns_config[clip_encode_fail_max]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['clip_encode_fail_max'];}?>" />  【配置后允许转码失败后重发并发的条数，未配置默认失败数据不处理】</td>
                </tr>
                <tr class="class_clip_to_encode">
                    <td class="rightstyle">转码失败执行次数最大限制：</td>
                    <td><input style="width:100px;"  name="nns_config[clip_encode_fail_max_ex]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['clip_encode_fail_max_ex'];}?>" />  【配置后允许转码失败后小于该值的次数，未配置默认失败数据不处理】</td>
                </tr>
                <tr class="class_clip_to_encode">
                    <td class="rightstyle">转码片源输入地址：</td>
                    <td><input  name="nns_config[clip_file_addr_input]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['clip_file_addr_input'];}?>" />  【转码片源输入的地址目前只支持FTP,运维必须把目录置为可读可写】</td>
                </tr>
                <tr class="class_clip_to_encode">
                    <td class="rightstyle">转码片源输出地址：</td>
                    <td><input  name="nns_config[clip_file_addr_output]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['clip_file_addr_output'];}?>" />  【转码片源输出的地址目前只支持FTP,运维必须把目录置为可读可写】</td>
                </tr>
                <tr class="class_clip_to_encode">
                    <td class="rightstyle">转码抽帧图片输入地址：</td>
                    <td><input  name="nns_config[drawing_frame_addr_input]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['drawing_frame_addr_input'];}?>" />  【转码抽帧图片输入的地址目前只支持FTP,运维必须把目录置为可读可写】</td>
                </tr>
                <tr class="class_clip_to_encode">
                    <td class="rightstyle">转码抽帧图片输出地址：</td>
                    <td><input  name="nns_config[drawing_frame_addr_output]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['drawing_frame_addr_output'];}?>" />  【转码抽帧图片输出的地址目前只支持FTP,运维必须把目录置为可读可写】</td>
                </tr>
                <tr class="class_clip_to_encode">
                    <td class="rightstyle">转码API请求方式：</td>
                    <td>
                        <input name="nns_config[clip_file_encode_way]" id="nns_config" type="radio" value="post" <?php if($edit_data['nns_config']['clip_file_encode_way'] == "post"){echo "checked";}?>/>  POST
                        <input name="nns_config[clip_file_encode_way]" id="nns_config" type="radio" value="get" <?php if($edit_data['nns_config']['clip_file_encode_way'] == 'get'){echo "checked";}?>/>  GET
                        【转码API请求方式,默认为POST】
                    </td>
                </tr>
                <tr class="class_clip_to_encode">
                    <td class="rightstyle">转码API请求地址：</td>
                    <td><input  name="nns_config[clip_file_encode_api_url]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['clip_file_encode_api_url'];}?>" />  【转码API的请求地址】</td>
                </tr>
                <tr class="class_clip_to_encode">
                    <td class="rightstyle">切片转码模式：</td>
                    <td><input  style="width:100px;"  name="nns_config[media_encode_model]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_encode_model'];}?>" />
                        【0或者未配置:下载完后原片也保留继续走队列;1:原片处理完毕后，原片的队列直接删除，只允许转码后的影片继续走队列】</td>
                </tr>
                <!--feijian.gao 2017年2月23日20:24:49 透传队列配置 start-->
                <tr>
                    <td class="rightstyle"><b>透传队列配置</b></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">透传队列开关</td>
                    <td>
                        <input name="nns_config[pass_queue_disabled]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['pass_queue_disabled']) || $edit_data['nns_config']['pass_queue_disabled'] == 0){echo "checked";}?>/>  关闭
                        <input name="nns_config[pass_queue_disabled]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['pass_queue_disabled'] == 1){echo "checked";}?>/>  开启
                        &nbsp;&nbsp;【默认 关闭】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">透传队列是否开启自动审核</td>
                    <td>
                        <input name="nns_config[pass_queue_audit]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['pass_queue_audit']) || $edit_data['nns_config']['pass_queue_audit'] == 0){echo "checked";}?>/>  关闭
                        <input name="nns_config[pass_queue_audit]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['pass_queue_audit'] == 1){echo "checked";}?>/>  开启
                        &nbsp;&nbsp;【默认 关闭】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">透传队列注入地址</td>
                    <td><input  name="nns_config[pass_queue_import_url]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['pass_queue_import_url'];}?>" /> </td>
                </tr>

                <!--feijian.gao 2017年2月23日20:24:49 透传队列配置 end-->
                <!-- C2指令配置 start -->
                <tr>
                    <td class="rightstyle"><b>C2指令配置</b></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>C2注入主媒资操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">C2注入主媒资队列上限</td>
                    <td><input  style="width:100px;"  name="nns_config[cdn_video_max]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['cdn_video_max'];}?>" /> 不设置为100个</td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>C2注入分集操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">C2分集注入是否检查主媒资是否注入成功：</td>
                    <td>
                        <input name="nns_config[cdn_index_check_video_enabled]" id="nns_config" type="radio" value="0" <?php if($edit_data['nns_config']['cdn_index_check_video_enabled'] == 0){echo "checked";}?>/>  关闭
                        <input name="nns_config[cdn_index_check_video_enabled]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['cdn_index_check_video_enabled'] == 1){echo "checked";}?>/>  开启
                        【开启:需要检查;关闭:不需要检查】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">C2注入分集队列上限</td>
                    <td><input  style="width:100px;"  name="nns_config[cdn_index_max]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['cdn_index_max'];}?>" /> 不设置为100个</td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>C2注入片源操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">C2片源注入是否检查分集是否注入成功：</td>
                    <td>
                        <input name="nns_config[cdn_media_check_index_enabled]" id="nns_config" type="radio" value="0" <?php if($edit_data['nns_config']['cdn_media_check_index_enabled'] == 0){echo "checked";}?>/>  关闭
                        <input name="nns_config[cdn_media_check_index_enabled]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['cdn_media_check_index_enabled'] == 1){echo "checked";}?>/>  开启
                        【开启:需要检查;关闭:不需要检查】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">C2注入片源队列上限</td>
                    <td><input  style="width:100px;"  name="nns_config[cdn_media_max]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['cdn_media_max'];}?>" /> 不设置为100个</td>
                </tr>

                <tr>
                    <td class="rightstyle"><i>C2注入节目单操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">C2节目单注入是否检查直播频道是否注入成功：</td>
                    <td>
                        <input name="nns_config[cdn_playbill_check_live_enabled]" id="nns_config" type="radio" value="0" <?php if($edit_data['nns_config']['cdn_playbill_check_live_enabled'] == 0){echo "checked";}?>/>  关闭
                        <input name="nns_config[cdn_playbill_check_live_enabled]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['cdn_playbill_check_live_enabled'] == 1){echo "checked";}?>/>  开启
                        【开启:需要检查;关闭:不需要检查】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">C2注入节目单队列上限</td>
                    <td><input  style="width:100px;"  name="nns_config[cdn_playbill_max]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['cdn_playbill_max'];}?>" /> 不设置为100个</td>
                </tr>
                <tr>
                    <td class="rightstyle">C2任务总数限制模式</td>
                    <td><input  style="width:100px;"  name="nns_config[c2_count_mode]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['c2_count_mode'];}?>" /> 0:等待执行;1:执行失败;2:正在执行;3:等待获取CDN播放串;4:注入取消;5:正在获取CDN播放串;6:获取播放传失败(多个以逗号,分隔)</td>
                </tr>
                <tr>
                    <td class="rightstyle">C2任务跨域地址：</td>
                    <td><input  style="width:100px;"  name="nns_config[c2_play_url_ip]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['c2_play_url_ip'];}?>" /> 【CDN拼串跨域配置，只支持单个IP】</td>
                </tr>
                <!-- C2指令配置 end -->
                <!-- CDN指令配置 start -->
                <tr>
                    <td class="rightstyle"><b>CDN指令配置</b></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">CDN注入暂停</td>
                    <td>
                        <input name="nns_config[disabled_cdn]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['disabled_cdn'] == 1){echo "checked";}?>/>  关闭
                        <input name="nns_config[disabled_cdn]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['disabled_cdn']) || $edit_data['nns_config']['disabled_cdn'] == 0){echo "checked";}?>/>  开启
                        &nbsp;&nbsp;【默认开启】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">CDN允许注入媒资开关：</td>
                    <td>
                        <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="video"      <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                            && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('video', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked";}?>/>  主媒资
                        <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="index"      <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                            && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('index', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked";}?>/>  分集
                        <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="media"      <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                            && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('media', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked";}?>/>  片源
                        <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="live"       <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                            && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('live', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked";}?>/>  直播频道
                        <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="live_index" <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                            && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('live_index', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked";}?>/>  频道分集
                        <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="live_media" <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                            && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('live_media', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked";}?>/>  直播源
                        <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="playbill"   <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                            && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('playbill', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked";}?>/>  节目单
                        <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="file"       <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                            && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('file', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked";}?>/>  文件
                        <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="category"   <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                            && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('category', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked";}?>/>  资源库栏目
                        <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="product"    <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                            && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('product', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked";}?>/>  产品包
                        <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="epg_file"    <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                            && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('epg_file', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked";}?>/>  EPGFile模板文件
                        【未选择队列不运行注入队列】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">不显示CDN注入</td>
                    <td>
                        <input name="nns_config[cdn_display_import]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['cdn_display_import'] == 1){echo "checked";}?>/>  隐藏
                        <input name="nns_config[cdn_display_import]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['cdn_display_import']) || $edit_data['nns_config']['cdn_display_import'] == 0){echo "checked";}?>/>  显示
                        &nbsp;&nbsp;【默认 显示】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">CDN注入条数</td>
                    <td><input   style="width:100px;"  name="nns_config[cdn_exec_num]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['cdn_exec_num'];}?>" />  CDN注入条数，默认100</td>
                </tr>

                <tr>
                    <td class="rightstyle">发送模式</td>
                    <td>
                        <input name="nns_config[cdn_send_mode]" id="nns_config" type="radio" onchange ="change();" value="0" <?php if($edit_data['nns_config']['cdn_send_mode'] == 0){echo "checked";}?>/>  SOAP
                        <input name="nns_config[cdn_send_mode]" id="nns_config" type="radio" onchange ="change();" value="1" <?php if($edit_data['nns_config']['cdn_send_mode'] == 1){echo "checked";}?>/>  HTTP
                        <input name="nns_config[cdn_send_mode]" id="nns_config" type="radio" onchange ="change();" value="2" <?php if($edit_data['nns_config']['cdn_send_mode'] == 2){echo "checked";}?>/>  FTP
                        【发送消息到第三方CDN厂家的请求协议 默认SOAP】
                    </td>
                </tr>
                <tr class="cdn_send_mode_url">
                    <td class="rightstyle">发送ADI URL</td>
                    <td>
                        <input name="nns_config[cdn_send_mode_url]" id="cdn_send_mode_url" type="text" value="<?php if($action=="edit"){echo $edit_data['nns_config']['cdn_send_mode_url'];}?>" />
                        【使用HTTP的方式注入CDN的URL请求地址，只是注入媒资元数据】
                    </td>
                </tr>
                <tr class="cdn_send_mode_url">
                    <td class="rightstyle">发送CDI URL</td>
                    <td>
                        <input name="nns_config[cdn_send_cdi_mode_url]" id="cdn_send_cdi_mode_url" type="text" value="<?php if($action=="edit"){echo $edit_data['nns_config']['cdn_send_cdi_mode_url'];}?>" />
                        【使用HTTP的方式注入CDN的URL请求地址，只是注入媒资元数据的上下线操作】
                    </td>
                </tr>
                <tr class="ftp_cdn_send_mode_url">
                    <td class="rightstyle">FTP模式是否异步反馈注入状态</td>
                    <td>
                        <input name="nns_config[ftp_cdn_send_mode_status]" id="ftp_cdn_send_mode_status" type="text" value="<?php if($action=="edit"){echo $edit_data['nns_config']['ftp_cdn_send_mode_status'];}?>" />
                        【使用FTP的方式注入CDN，默认0或不配置上传FTP即注入成功，配置1为FTP需异步通知注入状态】
                    </td>
                </tr>
                <tr class="ftp_cdn_send_mode_url">
                    <td class="rightstyle">FTP模式上传FTP配置</td>
                    <td>
                        <input name="nns_config[ftp_cdn_send_mode_url]" id="ftp_cdn_send_mode_url" type="text" value="<?php if($action=="edit"){echo $edit_data['nns_config']['ftp_cdn_send_mode_url'];}?>" />
                        【使用FTP的方式注入CDN的URL】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">CDN注入XML指令FTP地址</td>
                    <td><input name="nns_config[xml_ftp]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['xml_ftp'];}?>" /></td>
                </tr>

                <tr>
                    <td class="rightstyle">CDN注入片源FTP地址</td>
                    <td><input name="nns_config[media_ftp]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_ftp'];}?>" /></td>
                </tr>

                <tr>
                    <td class="rightstyle">CDN注入片源FTP检测地址</td>
                    <td><input name="nns_config[cdn_check_ts_accord_address]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['cdn_check_ts_accord_address'];}?>" /> FTP下载和检测地址不一致时配置该配置(主动模式)</td>
                </tr>
                <tr>
                    <td class="rightstyle">CDN注入图片FTP地址</td>
                    <td><input name="nns_config[img_ftp]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['img_ftp'];}?>" /></td>
                </tr>
                <tr>
                    <td class="rightstyle">CDN注入片源模式</td>
                    <td>
                        <input name="nns_config[cdn_import_media_mode]" id="nns_config" type="radio" value="0" <?php if($edit_data['nns_config']['cdn_import_media_mode'] == 0){echo "checked";}?>/>  单独模式
                        <input name="nns_config[cdn_import_media_mode]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['cdn_import_media_mode'] == 1){echo "checked";}?>/>  混合模式
                        <input name="nns_config[cdn_import_media_mode]" id="nns_config" type="radio" value="2" <?php if($edit_data['nns_config']['cdn_import_media_mode'] == 2){echo "checked";}?>/>  单片码率最高
                        &nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">CDN失败重试次数</td>
                    <td><input   style="width:100px;"  name="nns_config[cdn_fail_retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['cdn_fail_retry_time'];}?>" />  CDN失败重试次数,默认0</td>
                </tr>
                <tr>
                    <td class="rightstyle">CDN失败重试条数</td>
                    <td><input   style="width:100px;"  name="nns_config[cdn_exec_fail_num]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['cdn_exec_fail_num'];}?>" />  CDN失败重试条数,默认20</td>
                </tr>
                <!-- CDN指令配置 end -->

                <!-- EPG指令配置 start -->
                <tr>
                    <td class="rightstyle"><b>EPG指令配置</b></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>EPG注入暂停</td>
                    <td><input  rule="noempty"  style="width:100px;"  name="nns_config[disabled_epg]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['disabled_epg'];}?>" /> 1为关闭EPG注入，0或则不设置为开启EPG注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">EPG允许注入媒资开关：</td>
                    <td>
                        <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="video"      <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                            && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('video', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked";}?>/>  主媒资
                        <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="index"      <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                            && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('index', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked";}?>/>  分集
                        <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="media"      <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                            && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('media', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked";}?>/>  片源
                        <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="live"       <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                            && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('live', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked";}?>/>  直播频道
                        <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="live_index" <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                            && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('live_index', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked";}?>/>  频道分集
                        <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="live_media" <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                            && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('live_media', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked";}?>/>  直播源
                        <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="playbill"   <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                            && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('playbill', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked";}?>/>  节目单
                        <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="file"       <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                            && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('file', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked";}?>/>  文件
                        <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="category"   <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                            && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('category', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked";}?>/>  资源库栏目
                        <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="product"    <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                            && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('product', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked";}?>/>  产品包
                        【未选择队列不运行注入队列】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>不显示EPG注入</td>
                    <td>
                        <input name="nns_config[epg_display_import]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['epg_display_import']) || $edit_data['nns_config']['epg_display_import'] == 0){echo "checked";}?>/>  显示
                        <input name="nns_config[epg_display_import]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['epg_display_import'] == 1){echo "checked";}?>/>  隐藏
                        &nbsp;&nbsp;【默认 显示】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>不显示EPG删除按钮</td>
                    <td>
                        <input name="nns_config[epg_display_import_delete]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['epg_display_import_delete']) || $edit_data['nns_config']['epg_display_import_delete'] == 0){echo "checked";}?>/>  显示
                        <input name="nns_config[epg_display_import_delete]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['epg_display_import_delete'] == 1){echo "checked";}?>/>  隐藏
                        &nbsp;&nbsp;【默认 显示】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">多剧集单集注入方式</td>
                    <td>
                        <input name="nns_config[epg_single_index_import_model]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['epg_single_index_import_model']) || $edit_data['nns_config']['epg_single_index_import_model'] == 0){echo "checked";}?>/>  单集主媒资注入CMS
                        <input name="nns_config[epg_single_index_import_model]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['epg_single_index_import_model'] == 1){echo "checked";}?>/>  注入分集不注入单集主媒资（仅限多剧集以单集形式存在才配置）
                        &nbsp;&nbsp;【默认 单集主媒资注入CMS】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>EPG注入下发条数</td>
                    <td><input  rule="noempty"  style="width:100px;"  name="nns_config[epg_pre_max]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_pre_max'];}?>" /> EPG每次下发的个数（不设置为100）</td>
                </tr>
                <tr>
                    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>EPG注入切片路径</td>
                    <td>
                        <input  rule="noempty"  style="width:100px;"  name="nns_config[import_srouce_url]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['import_srouce_url'];}?>" />
                        0为注入原始相对路径，1为注入切片后相对路径
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>EPG注入点播接口</td>
                    <td><input  rule="noempty"  name="nns_config[epg_url]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_url'];}?>" /></td>
                </tr>
                <tr>
                    <td class="rightstyle">EPG节目单注入接口</td>
                    <td><input	name="nns_config[epg_playbill_url]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_playbill_url'];}?>" /></td>
                </tr>

                <tr>
                    <td class="rightstyle">EPG注入携带CORE绑定ID</td>
                    <td>
                        <input style="position: relative; top:2px;"  name="nns_config[import_core_bind_id_enabled]"  type="radio"  value="1" <?php if($action=="edit" && $edit_data['nns_config']['import_core_bind_id_enabled']=='1'){echo "checked='checked'";}?> />是
                        <input style="position: relative; top:2px;"  name="nns_config[import_core_bind_id_enabled]"  type="radio"  value="0" <?php if($action=="edit" && $edit_data['nns_config']['import_core_bind_id_enabled']=='0'){echo "checked='checked'";}?> />否  【默认否】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">EPG失败重试次条数</td>
                    <td><input   style="width:100px;"  name="nns_config[epg_retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_retry_time'];}?>" />  EPG失败重试次数,默认0不重试</td>
                </tr>
                <tr>
                    <td class="rightstyle">EPG失败重试条数</td>
                    <td><input   style="width:100px;"  name="nns_config[epg_fail_max]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_fail_max'];}?>" />  EPG失败重试条数,默认20</td>
                </tr>
                <!-- EPG指令配置 end -->

                <tr>
                    <td class="rightstyle"><b><i>媒资全局操作</i></b></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">媒资CDN共享地址:</td>
                    <td><input  name="nns_config[asset_cdn_share_addr]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['asset_cdn_share_addr'];}?>" /> 【提供CMS方拉取文件的相对地址】</td>
                </tr>
                <tr>
                    <td class="rightstyle">主媒资全局注入模式:</td>
                    <td>
                        <input name="nns_config[video_cdn_model]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['video_cdn_model']) || $edit_data['nns_config']['video_cdn_model'] == 0){echo "checked";}?>/>  播控作为CMS转发平台
                        <input name="nns_config[video_cdn_model]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['video_cdn_model'] == 1){echo "checked";}?>/>  播控作为CDN响应平台
                        &nbsp;&nbsp;【默认 播控作为CMS转发平台】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">分集全局注入模式:</td>
                    <td>
                        <input name="nns_config[index_cdn_model]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['index_cdn_model']) || $edit_data['nns_config']['index_cdn_model'] == 0){echo "checked";}?>/>  播控作为CMS转发平台
                        <input name="nns_config[index_cdn_model]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['index_cdn_model'] == 1){echo "checked";}?>/>  播控作为CDN响应平台
                        &nbsp;&nbsp;【默认 播控作为CMS转发平台】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">片源全局注入模式:</td>
                    <td>
                        <input name="nns_config[media_cdn_model]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['media_cdn_model']) || $edit_data['nns_config']['media_cdn_model'] == 0){echo "checked";}?>/>  播控作为CMS转发平台
                        <input name="nns_config[media_cdn_model]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['media_cdn_model'] == 1){echo "checked";}?>/>  播控作为CDN响应平台
                        <input name="nns_config[media_cdn_model]" id="nns_config" type="radio" value="2" <?php if($edit_data['nns_config']['media_cdn_model'] == 2){echo "checked";}?>/>  播控作为CMS转发平台也作为CDN响应平台，且cdn注入后就反馈上游消息
                        &nbsp;&nbsp;【默认 播控作为CMS转发平台】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">媒资注入CMS模式:</td>
                    <td>
                        <input name="nns_config[asset_import_cms_model]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['asset_import_cms_model']) || $edit_data['nns_config']['asset_import_cms_model'] == 0){echo "checked";}?>/>  原有逻辑注入CMS
                        <input name="nns_config[asset_import_cms_model]" id="nns_config" type="radio" value="1" <?php if($edit_data['nns_config']['asset_import_cms_model'] == 1){echo "checked";}?>/>  播控注入播控推入跳板机(下游方只能拉取数据的情况，不能上游往下游推送)
                        &nbsp;&nbsp;【默认 播控作为CMS转发平台】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">片源完成态是否删除ftp片源文件:</td>
                    <td>
                        <input style="position: relative; top:2px;"  name="nns_config[delete_ftp_media]"  type="radio"  value="1" <?php if($action=="edit" && $edit_data['nns_config']['delete_ftp_media']=='1'){echo "checked='checked'";}?> />是
                        <input style="position: relative; top:2px;"  name="nns_config[delete_ftp_media]"  type="radio"  value="0" <?php if($action=="edit" && $edit_data['nns_config']['delete_ftp_media']=='0'){echo "checked='checked'";}?> />否
                        <input style="margin-left: 10px; width: 400px" type="text" name="nns_config[delete_ftp_media_address]" value="<?php if($action=="edit"){echo $edit_data['nns_config']['delete_ftp_media_address'];}?>" size="20" /> 【ftp地址(多个请使用逗号分隔)】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle"><b><i>主媒资操作</i></b></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>主媒资添加操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input  style="width:100px;" name="nns_config[video_add][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['video_add']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[video_add][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['video_add']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>

                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[video_add][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['video_add']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">主媒资添加失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[video_add][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['video_add']['retry_time'];}?>" /> 主媒资EPG注入添加操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle">主媒资CDN注入:</td>
                    <td><input style="width:100px;" name="nns_config[video_add][cdn_import_model]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['video_add']['cdn_import_model'];}?>" /> 0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入</td>
                </tr>


                <tr>
                    <td class="rightstyle"><i>主媒资修改操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[video_modify][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['video_modify']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[video_modify][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['video_modify']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>

                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[video_modify][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['video_modify']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">主媒资修改失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[video_modify][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['video_modify']['retry_time'];}?>" />  主媒资EPG注入修改操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle">主媒资CDN注入:</td>
                    <td><input style="width:100px;" name="nns_config[video_modify][cdn_import_model]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['video_modify']['cdn_import_model'];}?>" /> 0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入</td>
                </tr>


                <tr>
                    <td class="rightstyle"><i>主媒资删除操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[video_destroy][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['video_destroy']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[video_destroy][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['video_destroy']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>

                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[video_destroy][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['video_destroy']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">主媒资删除失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[video_destroy][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['video_destroy']['retry_time'];}?>" />  主媒资EPG注入删除操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle">主媒资CDN注入:</td>
                    <td><input style="width:100px;" name="nns_config[video_destroy][cdn_import_model]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['video_destroy']['cdn_import_model'];}?>" /> 0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入</td>
                </tr>

                <tr>
                    <td class="rightstyle"><b><i>分集操作</i></b></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>分集添加操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[index_add][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['index_add']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[index_add][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['index_add']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[index_add][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['index_add']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">分集添加失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[index_add][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['index_add']['retry_time'];}?>" /> 分集EPG注入添加操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle">分集CDN注入:</td>
                    <td><input style="width:100px;" name="nns_config[index_add][cdn_import_model]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['index_add']['cdn_import_model'];}?>" /> 0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入</td>
                </tr>

                <tr>
                    <td class="rightstyle"><i>分集修改操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[index_modify][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['index_modify']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[index_modify][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['index_modify']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[index_modify][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['index_modify']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">分集修改失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[index_modify][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['index_modify']['retry_time'];}?>" /> 分集EPG注入修改操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle">分集CDN注入:</td>
                    <td><input style="width:100px;" name="nns_config[index_modify][cdn_import_model]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['index_modify']['cdn_import_model'];}?>" /> 0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入</td>
                </tr>


                <tr>
                    <td class="rightstyle"><i>分集删除操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[index_destroy][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['index_destroy']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[index_destroy][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['index_destroy']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[index_destroy][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['index_destroy']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">分集删除失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[index_destroy][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['index_destroy']['retry_time'];}?>" /> 分集EPG注入删除操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle">分集CDN注入:</td>
                    <td><input style="width:100px;" name="nns_config[index_destroy][cdn_import_model]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['index_destroy']['cdn_import_model'];}?>" /> 0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入</td>
                </tr>

                <tr>
                    <td class="rightstyle"><b><i>片源操作</i></b></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>片源添加操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[media_add][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_add']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[media_add][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_add']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[media_add][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_add']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">片源添加失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[media_add][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_add']['retry_time'];}?>" /> 片源EPG注入添加操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle">片源CDN注入:</td>
                    <td><input style="width:100px;" name="nns_config[media_add][cdn_import_model]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_add']['cdn_import_model'];}?>" /> 0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入</td>
                </tr>


                <tr>
                    <td class="rightstyle"><i>片源修改操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[media_modify][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_modify']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[media_modify][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_modify']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[media_modify][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_modify']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">片源修改失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[media_modify][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_modify']['retry_time'];}?>" /> 片源EPG注入修改操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle">片源CDN注入:</td>
                    <td><input style="width:100px;" name="nns_config[media_modify][cdn_import_model]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_modify']['cdn_import_model'];}?>" /> 0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入</td>
                </tr>


                <tr>
                    <td class="rightstyle"><i>片源删除操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[media_destroy][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_destroy']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[media_destroy][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_destroy']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[media_destroy][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_destroy']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">片源删除失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[media_destroy][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_destroy']['retry_time'];}?>" /> 片源EPG注入删除操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle">片源CDN注入:</td>
                    <td><input style="width:100px;" name="nns_config[media_destroy][cdn_import_model]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_destroy']['cdn_import_model'];}?>" /> 0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入</td>
                </tr>

                <tr>
                    <td class="rightstyle"><b><i>直播频道操作</i></b></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>直播频道添加操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[live_add][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_add']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[live_add][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_add']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[live_add][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_add']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">注入失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[live_add][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_add']['retry_time'];}?>" /> 直播EPG注入添加操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>直播频道修改操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[live_modify][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_modify']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[live_modify][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_modify']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[live_modify][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_modify']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">注入失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[live_modify][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_modify']['retry_time'];}?>" /> 直播EPG注入添加操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>直播频道删除操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[live_destroy][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_destroy']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[live_destroy][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_destroy']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[live_destroy][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_destroy']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">注入失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[live_destroy][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_destroy']['retry_time'];}?>" /> 直播EPG注入添加操作失败重试次数,空则为原逻辑</td>
                </tr>

                <tr>
                    <td class="rightstyle"><b><i>直播源操作</i></b></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>直播源添加操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[live_media_add][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_media_add']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[live_media_add][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_media_add']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[live_media_add][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_media_add']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">注入失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[live_media_add][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_media_add']['retry_time'];}?>" /> 直播EPG注入添加操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>直播源修改操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[live_media_modify][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_media_modify']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[live_media_modify][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_media_modify']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[live_media_modify][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_media_modify']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">注入失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[live_media_modify][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_media_modify']['retry_time'];}?>" /> 直播EPG注入添加操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>直播源删除操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[live_media_destroy][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_media_destroy']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[live_media_destroy][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_media_destroy']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[live_media_destroy][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_media_destroy']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">注入失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[live_media_destroy][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['live_media_destroy']['retry_time'];}?>" /> 直播EPG注入添加操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle"><b><i>节目单操作</i></b></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">节目单分组开关:</td>
                    <td>
                        <input style="position: relative; top:2px;"  name="nns_config[playbill][enabled]"  type="radio"  value="0" <?php if($action=="edit" && $edit_data['nns_config']['playbill']['enabled']!='1'){echo "checked='checked'";}?> />关闭
                        <input style="position: relative; top:2px;"  name="nns_config[playbill][enabled]"  type="radio"  value="1" <?php if($action=="edit" && $edit_data['nns_config']['playbill']['enabled']=='1'){echo "checked='checked'";}?> />开启
                        【开启：以直播源分组查询节目单信息，批量注入CDN;关闭：默认，单条节目单注入】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">节目单分组获取数据条数:</td>
                    <td>
                        <input style="width:100px;" name="nns_config[playbill][group_num]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['playbill']['group_num'];}?>" />
                        【如果节目单分组开关开启这个需要配置，未配置默认100条】
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>节目单添加操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[playbill_add][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['playbill_add']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[playbill_add][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['playbill_add']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[playbill_add][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['playbill_add']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">节目单注入失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[playbill_add][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['playbill_add']['retry_time'];}?>" /> 节目单EPG注入添加操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>节目单修改操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[playbill_modify][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['playbill_modify']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[playbill_modify][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['playbill_modify']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[playbill_modify][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['playbill_modify']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">节目单注入失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[playbill_modify][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['playbill_modify']['retry_time'];}?>" /> 直播EPG注入添加操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>节目单删除操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[playbill_destroy][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['playbill_destroy']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[playbill_destroy][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['playbill_destroy']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[playbill_destroy][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['playbill_destroy']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">节目单注入失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[playbill_destroy][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['playbill_destroy']['retry_time'];}?>" /> 直播EPG注入添加操作失败重试次数,空则为原逻辑</td>
                </tr>


                <tr>
                    <td class="rightstyle"><b><i>产品包操作</i></b></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>产品包添加操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[product_add][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['product_add']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[product_add][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['product_add']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[product_add][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['product_add']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">注入失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[product_add][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['product_add']['retry_time'];}?>" /> EPG注入添加操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>产品包修改操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[product_modify][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['product_modify']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[product_modify][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['product_modify']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[product_modify][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['product_modify']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">注入失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[product_modify][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['product_modify']['retry_time'];}?>" /> EPG注入添加操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>产品包删除操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[product_destroy][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['product_destroy']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[product_destroy][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['product_destroy']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[product_destroy][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['product_destroy']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">产品包注入失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[product_destroy][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['product_destroy']['retry_time'];}?>" /> EPG注入添加操作失败重试次数,空则为原逻辑</td>
                </tr>
                <!--EPGFile模板文件 xinxin.deng 2018/11/13 16:54-->
                <tr>
                    <td class="rightstyle"><b><i>EPGFile模板文件操作</i></b></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>EPGFile模板文件添加操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[epg_file_add][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_file_add']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[epg_file_add][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_file_add']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[epg_file_add][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_file_add']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">注入失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[epg_file_add][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_file_add']['retry_time'];}?>" /> EPG注入添加操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>EPGFile模板文件修改操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[epg_file_modify][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_file_modify']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[epg_file_modify][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_file_modify']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[epg_file_modify][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_file_modify']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">注入失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[epg_file_modify][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_file_modify']['retry_time'];}?>" /> EPG注入添加操作失败重试次数,空则为原逻辑</td>
                </tr>
                <tr>
                    <td class="rightstyle"><i>EPGFile模板文件删除操作</i></td>
                    <td> </td>
                </tr>
                <tr>
                    <td class="rightstyle">最终状态:</td>
                    <td><input style="width:100px;" name="nns_config[epg_file_destroy][status]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_file_destroy']['status'];}?>" /> 0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
                </tr>
                <tr>
                    <td class="rightstyle">优先执行注入:</td>
                    <td><input style="width:100px;" name="nns_config[epg_file_destroy][priority]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_file_destroy']['priority'];}?>" /> 0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
                </tr>
                <tr>
                    <td class="rightstyle">自动审核:</td>
                    <td><input style="width:100px;" name="nns_config[epg_file_destroy][audit]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_file_destroy']['audit'];}?>" /> 若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
                </tr>
                <tr>
                    <td class="rightstyle">EPGFile模板文件注入失败重试次数:</td>
                    <td><input style="width:100px;" name="nns_config[epg_file_destroy][retry_time]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['epg_file_destroy']['retry_time'];}?>" /> EPG注入添加操作失败重试次数,空则为原逻辑</td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
    <div class="controlbtns">
        <div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','<?php echo cms_get_lang('webkz_qrccxg'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
        <div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        <div style="clear:both;"></div>
    </div>
</div>

</body>
</html>
