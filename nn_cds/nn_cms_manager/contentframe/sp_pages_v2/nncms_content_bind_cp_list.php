<?php
header("Content-Type:text/html;charset=utf-8");
ini_set('display_errors', 0);
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include ("../../nncms_manager_inc.php");
$params = $_REQUEST;
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/models/sp_model.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE, 
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$data = nl_cp::query($dc, array (
		'page_count' => 0
));
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
		<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/trim.js"></script>
<script language="javascript">
$(document).ready(function() {
	$(".closebtn").click(function() {
		window.parent.close_select();
	});
	$(".closebtn_1").click(function() {
		window.parent.close_select();
	});
});
function check_select_height() {
	$(".selectbox iframe").height($(".selectbox iframe").contents().find(".selectcontrol").height());
	window.parent.check_select_height();
}
</script>
</head>

<body style='width:800px;'>
	<div class="content selectcontrol" style="padding:0px; margin:0px;">
			<div class="closebtn"><img src="../../images/topicon_08-topicon.png" /></div>
			<form action="nncms_content_sp_control.php" method="get">
			<div class="content_table formtable">
			<input name="action" value="bind" type="hidden"/>
			<input name="sp_id" value="<?php echo $params['sp_id']; ?>" type="hidden"/>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th><input name="" type="checkbox" value="" /></th>
						<th>序号</th>
						<th>CP ID</th>
						<th>CP名称</th>
						<th>创建时间</th>
						<th>修改时间</th>
					</tr>
				</thead>
				<tbody>
        		<?php
				if ($data['data_info'] != null)
					{
						$params['cp_id'] = explode(',',$params['cp_id']);
						foreach ($data['data_info'] as $item)
						{
							$num++;
				?>
              				<tr>
              					<?php
              						if(in_array($item["nns_id"],$params['cp_id'])){
								?>
									<td><input name="cp_id[]" type="checkbox" checked="checked" value="<?php echo  $item["nns_id"];?>" /></td>
								<?php }else{?>
									<td><input name="cp_id[]" type="checkbox" value="<?php echo  $item["nns_id"];?>" /></td>
								<?php }?>

								<td><?php echo $num;?></td>
								<td><?php echo $item['nns_id'];?></td>
								<td><?php echo $item["nns_name"];?></td>
								<td><?php echo $item["nns_create_time"];?> </td>
								<td><?php echo $item["nns_modify_time"];?> </td>
							</tr>
          		<?php 
						}
					}
					$page_info['page_size'] = 12;
					$last_num = $page_info['page_size'] - count($data['data_info']);
					for($i = 0;$i < $last_num;$i++){
					?>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				<?php } ?>
		          <tr>
		          	<td colspan='6'>&nbsp;</td>
		          </tr>
          <tr>
          	<td colspan='6' style="text-align:center" >
          		<input type="submit" value="确定" onclick="return confirm('确认绑定当前CP吗？')" />
          		<input type="button" class="closebtn_1" value="取消" onclick=""/>
          	</td>
          </tr>
          </tbody>
		</table>
		</div>
		</form>


	</div>

</body>
</html>
