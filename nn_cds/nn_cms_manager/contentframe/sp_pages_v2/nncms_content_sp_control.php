<?php
/*
 * Created on 2012-3-2
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
@header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"]))
{
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
require_once $nncms_db_path . "nns_log" . DIRECTORY_SEPARATOR . "nns_db_op_log_class.php";
$log_inst = new nns_db_op_log_class();
$_POST = $_REQUEST;
$action = $_POST["action"];
$nns_id = $_POST["nns_id"];

//加载多语言
include_once ($nncms_config_path . 'nn_cms_config/nn_cms_global.php');
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/sp/sp.class.php';
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
if (!empty($action))
{
	switch ($action)
	{
		case "edit":
			$action_str = '修改SP';
			$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "136002");
			break;
		case "add":
			$action_str = '添加SP';
			$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "136003");
			break;
		case "delete":
			$action_str = '删除SP';
			$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "136004");
			break;
		case "unbind":
			$action_str = '解除CP绑定';
			$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "136004");
			break;
		case "bind":
			$action_str = 'CP绑定';
			$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "136004");
			break;
		case "state":
		    $action_str = 'SP状态';
		    $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "136004");
		    break;
		default:
			break;
	}
}
unset($checkpri);
if (!$pri_bool)
{
	Header("Location: ../nncms_content_wrong.php");
}
else
{
	
	include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/models/sp_model.php';
	switch ($action)
	{
		case "edit":
			if ($_POST['nns_config']['cdn_send_mode'] == 0)
			{
				$_POST['nns_config']['cdn_send_mode_url'] = null;
			}
			if (!isset($_POST['nns_config']['main_drm_enabled']) || $_POST['nns_config']['main_drm_enabled'] != 1 || !isset($_POST['nns_config']['clip_drm_enabled']) || $_POST['nns_config']['clip_drm_enabled'] != 1)
			{
				if (isset($_POST['nns_config']['clip_drm_config_params']))
				{
					unset($_POST['nns_config']['clip_drm_config_params']);
				}
				//$_POST['nns_config']['clip_drm_url']='';
			}
			$sp_info = array (
					'nns_id' => $_POST['nns_id'], 
					'nns_name' => $_POST['nns_name'], 
					'nns_telphone' => isset($_POST['nns_telphone']) ? $_POST['nns_telphone'] : null, 
					'nns_email' => isset($_POST['nns_email']) ? $_POST['nns_email'] : null, 
					'nns_contact' => isset($_POST['nns_contact']) ? $_POST['nns_contact'] : null, 
					//'nns_rule' =>  isset($_POST['nns_rule']) ? $_POST['nns_rule'] :null,
					'nns_config' => isset($_POST['nns_config']) ? json_encode($_POST['nns_config']) : null
			);
			$result = sp_model::update_sp($sp_info);
			if ($result["ret"] != 0)
			{
				echo "<script>alert('" . $action_str . cms_get_lang('fault') . "');</script>";
			}
			else
			{
				echo "<script>alert('" . $action_str . cms_get_lang('success') . "');</script>";
			}
			break;
		case "add":
			$b_exists = sp_model::exists_sp($_POST['nns_id']);
			if ($b_exists)
			{
				echo "<script>alert('ID已经存在');</script>";
			}
			else
			{
				if ($_POST['nns_config']['cdn_send_mode'] == 0)
				{
					$_POST['nns_config']['cdn_send_mode_url'] = null;
				}
				if (!isset($_POST['nns_config']['main_drm_enabled']) || $_POST['nns_config']['main_drm_enabled'] != 1 || !isset($_POST['nns_config']['clip_drm_enabled']) || $_POST['nns_config']['clip_drm_enabled'] != 1)
				{
					if (isset($_POST['nns_config']['clip_drm_config_params']))
					{
						unset($_POST['nns_config']['clip_drm_config_params']);
					}
					//	$_POST['nns_config']['clip_drm_url']='';
				}
				$sp_info = array (
						'nns_id' => $_POST['nns_id'], 
						'nns_name' => $_POST['nns_name'], 
						'nns_telphone' => isset($_POST['nns_telphone']) ? $_POST['nns_telphone'] : null, 
						'nns_email' => isset($_POST['nns_email']) ? $_POST['nns_email'] : null, 
						'nns_contact' => isset($_POST['nns_contact']) ? $_POST['nns_contact'] : null, 
						//'nns_rule' =>  isset($_POST['nns_rule']) ? $_POST['nns_rule'] :null,
						'nns_config' => isset($_POST['nns_config']) ? json_encode($_POST['nns_config']) : null
				);
				$result = sp_model::add_sp($sp_info);
				if ($result["ret"] != 0)
				{
					echo "<script>alert('" . $action_str . cms_get_lang('fault') . "');</script>";
				}
				else
				{
					echo "<script>alert('" . $action_str . cms_get_lang('success') . "');</script>";
				}
			}
			break;
		case "delete":
			$delete_bool = true;
			$nns_ids = explode(",", $nns_id);
			$delete_bool = sp_model::batch_delete_sp($nns_ids);
			if (!$delete_bool)
			{
				echo "<script>alert('" . $action_str . cms_get_lang('fault') . "');</script>";
			}
			else
			{
				echo "<script>alert('" . $action_str . cms_get_lang('success') . "');</script>";
			}
			break;
		case "unbind":
			$nns_ids = array_filter(explode(",", $nns_id));
			$result_unbind = nl_sp::unbind($dc,$nns_ids);
			if ($result_unbind['ret'] !=0)
			{
				echo "<script>alert('" . $result_unbind['reason'] . "');</script>";
			}
			else
			{
				echo "<script>alert('解除绑定成功');</script>";
			}
			break;
		case "bind":
		    if(empty($_REQUEST['cp_id']) || !is_array($_REQUEST['cp_id']))
		    {
		        echo "<script>alert('未绑定任何CP');</script>";
		        break;
		    }
			$cp_id = ','.implode(',', $_REQUEST['cp_id']).',';
			if(strpos($cp_id, ",") > 0)
			{
				$cp_id = "," . $cp_id;
			}
			$result_bind = nl_sp::bind($dc,$_REQUEST['sp_id'],$cp_id);
			if ($result_bind['ret'] !=0)
			{
				echo "<script>alert('" . $result_bind['reason'] . "');</script>";
			}
			else
			{
				echo "<script>alert('" . $result_bind['reason'] . "');</script>";
			}
			echo "<script>parent.location='nncms_content_sp_list.php';</script>";die;
			break;
		case "state":
		    $nns_ids = explode(",", $_REQUEST['nns_id']);
		    if(!is_array($nns_ids) || empty($nns_ids))
		    {
		        echo "<script>alert('修改状态成功,数据为空');</script>";
		        break;
		    }
		    $result = nl_sp::edit($dc, array('nns_state'=>$_REQUEST['nns_state']), $nns_ids);
		    if ($result['ret'])
		    {
		        echo "<script>alert('" . $result['reason'] . "');history.go(-1);</script>";
		    }
		    else
		    {
		        $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "修改:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
		        echo "<script>alert('修改成功');</script>";
		    }
		    break;
		default:
			
			break;
	}
	$partner_inst = null;
	$log_inst = null;
	echo "<script>self.location='nncms_content_sp_list.php';</script>";
}
?>
