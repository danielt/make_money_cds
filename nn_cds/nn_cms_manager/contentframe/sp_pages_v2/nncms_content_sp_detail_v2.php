<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/11/22 14:56
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
    $language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$nns_id=$_GET["nns_id"];
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136005");
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");
//加载全局配置
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");

//加载新版
include $nncms_config_path . "v2/common.php";
\ns_core\m_load::load("ns_core.m_config");
$result_sp = \m_config::_get_sp_info($nns_id);
$edit_data = isset($result_sp['data_info']) ? $result_sp['data_info'] : array();

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
    <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
    <script language="javascript" src="../../js/table.js.php"></script>
    <script language="javascript" src="../../js/radiolist.js"></script>
    <script language="javascript" src="../../js/checkinput.js.php"></script>
    <script language="javascript">

    </script>
</head>

<body>



<div class="content">
    <div class="content_position">SP 管理 > SP 信息</div>
    <div class="content_table">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td class="rightstyle"><?php echo cms_get_lang('id');?>:</td>
                <td><?php echo $edit_data['nns_id'];?></td>
            </tr>
            <tr>
                <td class="rightstyle">SP 名称:</td>
                <td><?php echo $edit_data["nns_name"];?></td>
            </tr>
            <tr>
                <td class="rightstyle">联系人:</td>
                <td><?php echo $edit_data["nns_contact"];?></td>
            </tr>
            <tr>
                <td class="rightstyle">联系电话:</td>
                <td><?php echo $edit_data["nns_telphone"];?></td>
            </tr>
            <tr>
                <td class="rightstyle">Email:</td>
                <td><?php echo $edit_data["nns_email"];?></td>
            </tr>
            <tr>
                <td class="rightstyle"><b>SP全局配置</b></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">CDN注入ID模式：</td>
                <td><?php echo $edit_data['nns_config']['import_id_mode'];?>(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;11:湖南注入中兴cdn专用)</td>
            </tr>
            <tr>
                <td class="rightstyle">EPG主媒资注入ID模式</td>
                <td><?php echo $edit_data['nns_config']['epg_video_import_id_mode'];?>(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;)</td>
            </tr>
            <tr>
                <td class="rightstyle">EPG分集注入ID模式</td>
                <td><?php echo $edit_data['nns_config']['epg_index_import_id_mode'];?>(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;)</td>
            </tr>
            <tr>
                <td class="rightstyle">EPG片源注入ID模式</td>
                <td><?php echo $edit_data['nns_config']['epg_media_import_id_mode'];?>(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;11:湖南注入中兴cdn专用)</td>
            </tr>
            <tr>
                <td class="rightstyle">EPG直播频道注入ID模式</td>
                <td><?php echo $edit_data['nns_config']['epg_live_import_id_mode']; ?>(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;)</td>
            </tr>
            <tr>
                <td class="rightstyle">EPG直播分集注入ID模式</td>
                <td><?php echo $edit_data['nns_config']['epg_live_index_import_id_mode'];?>(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;)</td>
            </tr>
            <tr>
                <td class="rightstyle">EPG直播源注入ID模式</td>
                <td><?php echo $edit_data['nns_config']['epg_live_media_import_id_mode'];?>(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;)</td>
            </tr>
            <tr>
                <td class="rightstyle">EPG节目单注入ID模式</td>
                <td><?php echo $edit_data['nns_config']['epg_playbill_import_id_mode'];?>(0:guid;1:数字id;2:原始id;3:内容数字组合ID;4:字符串ID;)</td>
            </tr>
            <tr>
                <td class="rightstyle">消息反馈开关：</td>
                <td>
                    <input name="nns_config[message_feedback_enabled]" type="radio" disabled value="1" <?php if($edit_data['nns_config']['message_feedback_enabled'] == 1){echo "checked";}?>/>  关闭
                    <input name="nns_config[message_feedback_enabled]" type="radio" disabled value="0" <?php if(!isset($edit_data['nns_config']['message_feedback_enabled']) || $edit_data['nns_config']['message_feedback_enabled'] != 1){echo "checked";}?>/>  开启
                    【关闭（消息反馈关闭），开启【消息反馈开启】由于加了播控注入播控逻辑，上游播控可以关闭消息反馈】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">消息反馈模式</td>
                <td>
                    <input name="nns_config[message_feedback_mode]" disabled type="radio"  value="0" <?php if(isset($edit_data['nns_config']['message_feedback_mode']) && $edit_data['nns_config']['message_feedback_mode'] == 0){echo "checked='checked'";} ?> />原反馈模式
                    <input name="nns_config[message_feedback_mode]" disabled type="radio"  value="1" <?php if(isset($edit_data['nns_config']['message_feedback_mode']) && $edit_data['nns_config']['message_feedback_mode'] == 1){echo "checked='checked'";} ?> />CP反馈模式
                </td>
            </tr>
            <tr>
                <td class="rightstyle"><b>注入码率配置</b></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">码率配置开关：</td>
                <td>
                    <input name="nns_config[import_ratebit_enabled]" disabled onchange ="change2('nns_config[import_ratebit_enabled]', 'class_import_ratebit_enabled');" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['import_ratebit_enabled']) || $edit_data['nns_config']['import_ratebit_enabled'] == 0){echo "checked";}?>/>  关闭
                    <input name="nns_config[import_ratebit_enabled]" disabled onchange ="change2('nns_config[import_ratebit_enabled]', 'class_import_ratebit_enabled');" type="radio" value="1" <?php if($edit_data['nns_config']['import_ratebit_enabled'] == 1){echo "checked";}?>/>  开启
                    &nbsp;&nbsp;【关闭任何片源码率都通过该SP，开启了就通过片源码率开始值和结束值判断】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">片源码率开始值：</td>
                <td><?php if(isset($edit_data['nns_config']['import_ratebit_start'])){ echo $edit_data['nns_config']['import_ratebit_start'];}?>	 如果为配置则片源码率无下限，如果配置了片源码率至少大于等于该开始值</td>
            </tr>
            <tr>
                <td class="rightstyle">片源码率结束值：</td>
                <td><?php if(isset($edit_data['nns_config']['import_ratebit_end'])){echo $edit_data['nns_config']['import_ratebit_end'];}?>	 如果为配置则片源码率无上限，如果配置了片源码率至少小于等于该开始值</td>
            </tr>

            <tr>
                <td class="rightstyle"><b>媒资上下线配置</b></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">媒资上下线开关：</td>
                <td>
                    <input name="nns_config[online_disabled]" disabled onchange ="change2('nns_config[online_disabled]', 'class_online_disabled');" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['online_disabled']) || $edit_data['nns_config']['online_disabled'] == 0){echo "checked";}?>/>  关闭
                    <input name="nns_config[online_disabled]" disabled onchange ="change2('nns_config[online_disabled]', 'class_online_disabled');" type="radio" value="1" <?php if($edit_data['nns_config']['online_disabled'] == 1){echo "checked";}?>/>  开启
                    &nbsp;&nbsp;【关闭后不生成上下线指令,默认关闭】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">媒资上下线注入模式：</td>
                <td>
                    <input name="nns_config[asset_unline_model]" disabled type="radio" value="0" <?php if(!isset($edit_data['nns_config']['asset_unline_model']) || $edit_data['nns_config']['asset_unline_model'] == 0){echo "checked";}?>/>  redis消息队列上下线控制
                    <input name="nns_config[asset_unline_model]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['asset_unline_model'] == 1){echo "checked";}?>/>  epg注入上下线控制
                    &nbsp;&nbsp;【默认redis消息队列上下线控制】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">审核开关：</td>
                <td>
                    <input name="nns_config[online_audit]" disabled type="radio" value="0" <?php if(!isset($edit_data['nns_config']['online_audit']) || $edit_data['nns_config']['online_audit'] == 0){echo "checked";}?>/>  自动审核
                    <input name="nns_config[online_audit]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['online_audit'] == 1){echo "checked";}?>/>  未审核
                    <input name="nns_config[online_audit]" disabled type="radio" value="2" <?php if($edit_data['nns_config']['online_audit'] == 2){echo "checked";}?>/>  下线未审核/上线自动审核
                    &nbsp;&nbsp;【默认媒资上下线为自动审核】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">验证最终状态开关：</td>

                <td>
                    <input name="nns_config[check_video_statuc_enabled]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['check_video_statuc_enabled'] == 1){echo "checked";}?>/>  否
                    <input name="nns_config[check_video_statuc_enabled]" disabled type="radio" value="0" <?php if(!isset($edit_data['nns_config']['check_video_statuc_enabled']) || $edit_data['nns_config']['check_video_statuc_enabled'] == 0){echo "checked";}?>/>  是
                    &nbsp;&nbsp;【默认是】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">是否通知上下线队列开关：</td>

                <td>
                    <input name="nns_config[notify_online_queue_enabled]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['notify_online_queue_enabled'] == 1){echo "checked";}?>/>  否
                    <input name="nns_config[notify_online_queue_enabled]" disabled type="radio" value="0" <?php if(!isset($edit_data['nns_config']['notify_online_queue_enabled']) || $edit_data['nns_config']['notify_online_queue_enabled'] == 0){echo "checked";}?>/>  是
                    &nbsp;&nbsp;【操作队列注入为最终状态是否通知上下线，默认 否】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">第三方媒资上下线关系注入开关：</td>

                <td>
                    <input name="nns_config[third_category_enabled]" disabled type="radio" value="0" <?php if(!isset($edit_data['nns_config']['third_category_enabled']) || $edit_data['nns_config']['third_category_enabled'] == 0){echo "checked";}?>/>  否
                    <input name="nns_config[third_category_enabled]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['third_category_enabled'] == 1){echo "checked";}?>/>  是
                    &nbsp;&nbsp;【是否属于第三方媒资上下线关系注入，默认否】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">EPG上下线下发方式：</td>

                <td>
                    <input name="nns_config[online_send_way]" disabled type="radio" value="0" <?php if($edit_data['nns_config']['third_category_enabled'] == 0){echo "checked";}?>/>  FTP
                    <input name="nns_config[online_send_way]" disabled type="radio" value="1" <?php if(!isset($edit_data['nns_config']['online_send_way']) || $edit_data['nns_config']['online_send_way'] == 1){echo "checked";}?>/>  HTTP
                    &nbsp;&nbsp;【默认HTTP】
                </td>
            </tr>

            <tr>
                <td class="rightstyle">媒资上下线EPG注入接口：</td>

                <td><?php echo $edit_data['nns_config']['online_api'];?>	(媒资上下线注入接口url地址)</td>
            </tr>
            <tr>
                <td class="rightstyle"><b>中心注入指令配置</b></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">中心注入指令开关：</td>
                <td>
                    <input name="nns_config[import_op_enabled]" disabled type="radio" value="0" <?php if($edit_data['nns_config']['import_op_enabled'] == 0){echo "checked";}?>/>  开启
                    <input name="nns_config[import_op_enabled]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['import_op_enabled'] == 1){echo "checked";}?>/>  关闭
                    &nbsp;&nbsp;【关闭后不再注入中心注入指令，默认开启】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">中心注入指令消息处理模式：</td>
                <td>
                    <input name="nns_config[op_queue_model]" disabled type="radio" value="0" <?php if($edit_data['nns_config']['op_queue_model'] == 0){echo "checked";}?>/>  播控作为TV平台方（原逻辑）
                    <input name="nns_config[op_queue_model]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['op_queue_model'] == 1){echo "checked";}?>/>  播控作为CDN平台方（新逻辑）
                    【播控以不同的角色展示给客户，即可以作为CDN平台，也可以作为TV平台】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">中心注入指令下发条数</td>
                <td><?php echo $edit_data['nns_config']['import_op_pre_max'];?>&nbsp;&nbsp;&nbsp;&nbsp;【每次取多少个操作任务（不设置为20）】</td>
            </tr>
            <tr>
                <td class="rightstyle">队列允许通过的媒资开关：</td>
                <td>
                    <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="video"      <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('video', $edit_data['nns_config']['op_queue_video_enabled']))      || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked disabled";}?>/>  主媒资
                    <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="index"      <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('index', $edit_data['nns_config']['op_queue_video_enabled']))      || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked disabled";}?>/>  分集
                    <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="media"      <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('media', $edit_data['nns_config']['op_queue_video_enabled']))      || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked disabled";}?>/>  片源
                    <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="live"       <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('live', $edit_data['nns_config']['op_queue_video_enabled']))       || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked disabled";}?>/>  直播频道
                    <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="live_index" <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('live_index', $edit_data['nns_config']['op_queue_video_enabled'])) || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked disabled";}?>/>  频道分集
                    <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="live_media" <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('live_media', $edit_data['nns_config']['op_queue_video_enabled'])) || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked disabled";}?>/>  直播源
                    <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="playbill"   <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('playbill', $edit_data['nns_config']['op_queue_video_enabled']))   || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked disabled";}?>/>  节目单
                    <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="file"       <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('file', $edit_data['nns_config']['op_queue_video_enabled']))       || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked disabled";}?>/>  文件
                    <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="category"   <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('category', $edit_data['nns_config']['op_queue_video_enabled']))   || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked disabled";}?>/>  资源库栏目
                    <input name="nns_config[op_queue_video_enabled][]" id="nns_config" type="checkbox" value="product"    <?php if((isset($edit_data['nns_config']['op_queue_video_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_video_enabled']) && in_array('product', $edit_data['nns_config']['op_queue_video_enabled']))    || !isset($edit_data['nns_config']['op_queue_video_enabled'])){echo "checked disabled";}?>/>  产品包
                    【未选择队列不在接收此类型文件，默认全选中】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">队列允许通过的片源文件类型：</td>
                <td>
                    <input name="nns_config[op_queue_filetype_enabled][]" id="nns_config" type="checkbox" value="ts"      <?php if((isset($edit_data['nns_config']['op_queue_filetype_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_filetype_enabled']) && in_array('ts', $edit_data['nns_config']['op_queue_filetype_enabled']))      || !isset($edit_data['nns_config']['op_queue_filetype_enabled'])){echo "checked disabled";}?>/>  ts
                    <input name="nns_config[op_queue_filetype_enabled][]" id="nns_config" type="checkbox" value="m3u8"      <?php if((isset($edit_data['nns_config']['op_queue_filetype_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_filetype_enabled']) && in_array('m3u8', $edit_data['nns_config']['op_queue_filetype_enabled']))      || !isset($edit_data['nns_config']['op_queue_filetype_enabled'])){echo "checked disabled";}?>/>  m3u8
                    <input name="nns_config[op_queue_filetype_enabled][]" id="nns_config" type="checkbox" value="flv"      <?php if((isset($edit_data['nns_config']['op_queue_filetype_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_filetype_enabled']) && in_array('flv', $edit_data['nns_config']['op_queue_filetype_enabled']))      || !isset($edit_data['nns_config']['op_queue_filetype_enabled'])){echo "checked disabled";}?>/>  flv
                    <input name="nns_config[op_queue_filetype_enabled][]" id="nns_config" type="checkbox" value="mp4"       <?php if((isset($edit_data['nns_config']['op_queue_filetype_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_filetype_enabled']) && in_array('mp4', $edit_data['nns_config']['op_queue_filetype_enabled']))       || !isset($edit_data['nns_config']['op_queue_filetype_enabled'])){echo "checked disabled";}?>/>  mp4
                    <input name="nns_config[op_queue_filetype_enabled][]" id="nns_config" type="checkbox" value="mp3" <?php if((isset($edit_data['nns_config']['op_queue_filetype_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_filetype_enabled']) && in_array('mp3', $edit_data['nns_config']['op_queue_filetype_enabled'])) || !isset($edit_data['nns_config']['op_queue_filetype_enabled'])){echo "checked disabled";}?>/>  mp3
                    <input name="nns_config[op_queue_filetype_enabled][]" id="nns_config" type="checkbox" value="mpg" <?php if((isset($edit_data['nns_config']['op_queue_filetype_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_filetype_enabled']) && in_array('mpg', $edit_data['nns_config']['op_queue_filetype_enabled'])) || !isset($edit_data['nns_config']['op_queue_filetype_enabled'])){echo "checked disabled";}?>/>  mpg
                    <input name="nns_config[op_queue_filetype_enabled][]" id="nns_config" type="checkbox" value="mts" <?php if((isset($edit_data['nns_config']['op_queue_filetype_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_filetype_enabled']) && in_array('mts', $edit_data['nns_config']['op_queue_filetype_enabled'])) || !isset($edit_data['nns_config']['op_queue_filetype_enabled'])){echo "checked disabled";}?>/>  mts
                    【未选择队列不在接收此片源文件类型，默认全选中】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">队列不允许通过的片源清晰度：</td>
                <td>
                    <input name="nns_config[media_import_op_mode][]" id="nns_config" type="checkbox" value="low" disabled <?php if((isset($edit_data['nns_config']['media_import_op_mode'])
                        && is_array($edit_data['nns_config']['media_import_op_mode']) && in_array('low', $edit_data['nns_config']['media_import_op_mode']))){echo "checked";}?>/>  低清(low)
                    <input name="nns_config[media_import_op_mode][]" id="nns_config" type="checkbox" value="std" disabled <?php if((isset($edit_data['nns_config']['media_import_op_mode'])
                        && is_array($edit_data['nns_config']['media_import_op_mode']) && in_array('std', $edit_data['nns_config']['media_import_op_mode']))){echo "checked";}?>/>  标清(std)
                    <input name="nns_config[media_import_op_mode][]" id="nns_config" type="checkbox" value="hd" disabled <?php if((isset($edit_data['nns_config']['media_import_op_mode'])
                        && is_array($edit_data['nns_config']['media_import_op_mode']) && in_array('hd', $edit_data['nns_config']['media_import_op_mode']))){echo "checked";}?>/>  高清(hd)
                    <input name="nns_config[media_import_op_mode][]" id="nns_config" type="checkbox" value="sd" disabled <?php if((isset($edit_data['nns_config']['media_import_op_mode'])
                        && is_array($edit_data['nns_config']['media_import_op_mode']) && in_array('sd', $edit_data['nns_config']['media_import_op_mode']))){echo "checked";}?>/>  超清(sd)
                    <input name="nns_config[media_import_op_mode][]" id="nns_config" type="checkbox" value="4k" disabled <?php if((isset($edit_data['nns_config']['media_import_op_mode'])
                        && is_array($edit_data['nns_config']['media_import_op_mode']) && in_array('4k', $edit_data['nns_config']['media_import_op_mode']))){echo "checked";}?>/>  4K(4k)
                    【已选择的清晰度不会生成片源中心注入队列，默认全部通过】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">队列允许通过的片源类型开关：</td>
                <td>
                    <input name="nns_config[op_queue_media_type_enabled][]" id="nns_config" type="checkbox" value="0" <?php if((isset($edit_data['nns_config']['op_queue_media_type_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_media_type_enabled']) && in_array('0', $edit_data['nns_config']['op_queue_media_type_enabled'])) || !isset($edit_data['nns_config']['op_queue_media_type_enabled'])){echo "checked disabled";}?>/>  原始片源
                    <input name="nns_config[op_queue_media_type_enabled][]" id="nns_config" type="checkbox" value="1" <?php if((isset($edit_data['nns_config']['op_queue_media_type_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_media_type_enabled']) && in_array('1', $edit_data['nns_config']['op_queue_media_type_enabled'])) || !isset($edit_data['nns_config']['op_queue_media_type_enabled'])){echo "checked disabled";}?>/>  转码片源
                    <input name="nns_config[op_queue_media_type_enabled][]" id="nns_config" type="checkbox" value="2" <?php if((isset($edit_data['nns_config']['op_queue_media_type_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_media_type_enabled']) && in_array('2', $edit_data['nns_config']['op_queue_media_type_enabled'])) || !isset($edit_data['nns_config']['op_queue_media_type_enabled'])){echo "checked disabled";}?>/>  CDN片源
                    【未选择队列不在接收此片源类型文件，默认全选中】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">队列允许通过的片源终端TAG开关：</td>
                <td>
                    <input name="nns_config[op_queue_media_tag_enabled][]" id="nns_config" type="checkbox" value="26" <?php if((isset($edit_data['nns_config']['op_queue_media_tag_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_media_tag_enabled']) && in_array('26', $edit_data['nns_config']['op_queue_media_tag_enabled'])) || !isset($edit_data['nns_config']['op_queue_media_tag_enabled'])){echo "checked disabled";}?>/>  STB
                    <input name="nns_config[op_queue_media_tag_enabled][]" id="nns_config" type="checkbox" value="27" <?php if((isset($edit_data['nns_config']['op_queue_media_tag_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_media_tag_enabled']) && in_array('27', $edit_data['nns_config']['op_queue_media_tag_enabled'])) || !isset($edit_data['nns_config']['op_queue_media_tag_enabled'])){echo "checked disabled";}?>/>  IPHONE
                    <input name="nns_config[op_queue_media_tag_enabled][]" id="nns_config" type="checkbox" value="28" <?php if((isset($edit_data['nns_config']['op_queue_media_tag_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_media_tag_enabled']) && in_array('28', $edit_data['nns_config']['op_queue_media_tag_enabled'])) || !isset($edit_data['nns_config']['op_queue_media_tag_enabled'])){echo "checked disabled";}?>/>  IPAD
                    <input name="nns_config[op_queue_media_tag_enabled][]" id="nns_config" type="checkbox" value="29" <?php if((isset($edit_data['nns_config']['op_queue_media_tag_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_media_tag_enabled']) && in_array('29', $edit_data['nns_config']['op_queue_media_tag_enabled'])) || !isset($edit_data['nns_config']['op_queue_media_tag_enabled'])){echo "checked disabled";}?>/>  ANDROID_PHONE
                    <input name="nns_config[op_queue_media_tag_enabled][]" id="nns_config" type="checkbox" value="30" <?php if((isset($edit_data['nns_config']['op_queue_media_tag_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_media_tag_enabled']) && in_array('30', $edit_data['nns_config']['op_queue_media_tag_enabled'])) || !isset($edit_data['nns_config']['op_queue_media_tag_enabled'])){echo "checked disabled";}?>/>  ANDROID_PAD
                    <input name="nns_config[op_queue_media_tag_enabled][]" id="nns_config" type="checkbox" value="31" <?php if((isset($edit_data['nns_config']['op_queue_media_tag_enabled'])
                            && is_array($edit_data['nns_config']['op_queue_media_tag_enabled']) && in_array('31', $edit_data['nns_config']['op_queue_media_tag_enabled'])) || !isset($edit_data['nns_config']['op_queue_media_tag_enabled'])){echo "checked disabled";}?>/>  PC
                    【未选择队列不在接收此片源类型文件，默认全选中，如果片源tag标志为空则默认全部通过，无论设置与否】
                </td>
            </tr>
            <tr>
                <td class="rightstyle"><b>中心同步指令配置</b></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">中心同步指令开关：</td>
                <td>
                    <input name="nns_config[op_queue_enabled]" disabled type="radio" value="0" <?php if($edit_data['nns_config']['op_queue_enabled'] == 0){echo "checked";}?>/>  关闭
                    <input name="nns_config[op_queue_enabled]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['op_queue_enabled'] == 1){echo "checked";}?>/>  开启
                    &nbsp;&nbsp;【关闭后不再注入中心同步指令,默认关闭】
                </td>
            </tr>

            <tr>
                <td class="rightstyle">中心片库下载地址虚拟接口</td>
                <td><?php  echo $edit_data['nns_config']['down_url'];?></td>
            </tr>
            <tr>
                <td class="rightstyle">中心同步指令下发条数</td>
                <td><?php echo $edit_data['nns_config']['op_pre_max'];?>&nbsp;&nbsp;&nbsp;&nbsp;每次取多少个操作任务（不设置为100）</td>
            </tr>
            <tr>
                <td class="rightstyle"> 中心同步指令冻结时间</td>
                <td><?php echo $edit_data['nns_config']['op_task_freeze_time'];?>冻结时间，这个时间以后的全部冻结  格式：YYYY-mm-dd HH:ii:SS</td>
            </tr>
            <tr>
                <td class="rightstyle">中心同步指令操作排序:</td>
                <td> <?php  echo $edit_data['nns_config']['op_task_order'];?> </td>
            </tr>
            <tr>
                <td class="rightstyle">中心同步删除指令生成C2虚拟任务:</td>
                <td>
                    <input name="nns_config[virtual_destroy_task]" disabled type="radio" value="0" <?php if($edit_data['nns_config']['virtual_destroy_task'] == 0){echo "checked";}?>/>  关闭
                    <input name="nns_config[virtual_destroy_task]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['virtual_destroy_task'] == 1){echo "checked";}?>/>  开启
                    &nbsp;&nbsp;【当删除指令在C2中无历史注入任务时，关闭则删除当前删除指令，开启则生成C2虚拟任务。默认关闭】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">片源服务类型:</td>
                <td> <?php  echo $edit_data['nns_config']['media_service_type'];?>&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">【片源服务类型标示；同步指令创建片源模式为1后这个参数值必填】</font></td>
            </tr>
            <tr>
                <td class="rightstyle"><b>切片指令配置</b></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">切片模式</td>
                <td>
                    <input name="nns_config[clip_import_model]" disabled type="radio" value="0" <?php if(!isset($edit_data['nns_config']['clip_import_model']) || $edit_data['nns_config']['clip_import_model'] == 0){echo "checked";}?>/>  老模式切片下发
                    <input name="nns_config[clip_import_model]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['clip_import_model'] == 1){echo "checked";}?>/>  资源库中片源查询切片下发且只下载不切片
                    &nbsp;&nbsp;【默认老模式切片下发】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">切片CPID下发模式</td>
                <td>
                    <input name="nns_config[clip_cp_id_sent_model]" disabled type="radio" value="0" <?php if(!isset($edit_data['nns_config']['clip_cp_id_sent_model']) || $edit_data['nns_config']['clip_cp_id_sent_model'] == 0){echo "checked";}?>/>  import_source(片源)
                    <input name="nns_config[clip_cp_id_sent_model]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['clip_cp_id_sent_model'] == 1){echo "checked";}?>/>  producer（主媒资）
                    &nbsp;&nbsp;【默认 import_source(片源)】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">切片任务下发条数</td>
                <td><?php echo $edit_data['nns_config']['clip_pre_max'];?>&nbsp;&nbsp;&nbsp;&nbsp;切片每次从操作列队获取多少个（不设置为100）</td>
            </tr>
            <tr>
                <td class="rightstyle">切片任务下发模式</td>
                <td>
                    <input name="nns_config[is_starcor_telecom]" disabled type="radio" value="0" <?php if(!isset($edit_data['nns_config']['is_starcor_telecom']) || $edit_data['nns_config']['is_starcor_telecom'] == 0){echo "checked";}?>/>  非电信模式
                    <input name="nns_config[is_starcor_telecom]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['is_starcor_telecom'] == 1){echo "checked";}?>/>  电信模式(注入到starcor CDN下发domin和cms_id)
                    &nbsp;&nbsp;【默认 非电信模式】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">切片任务暂停</td>
                <td><?php echo $edit_data['nns_config']['disabled_clip'];?>&nbsp;&nbsp;&nbsp;&nbsp;0:开启BK切片任务;1:关闭BK切片任务且不生成注入队列;2:关闭BK切片任务且生成注入队列;3:切片指令栏目展示且不生成注入队列，切片栏目只提供展示，且CDN拉片在片源操作只下载片源</td>
            </tr>
            <tr>
                <td class="rightstyle">检查片源是否已经下载过:</td>
                <td><?php echo $edit_data['nns_config']['check_file_exsist'];?>&nbsp;&nbsp;&nbsp;&nbsp;【0或者未配置:不检查,继续下载切片；1:要检查，如果片源存在则不再下载切片，队列为等待切片，如果不存在则下载切片，队列为等待注入】</td>
            </tr>
            <!--xinxin.deng 2017年11月09日14:55:00 添加图片是否上传到ftp  start-->
            <tr>
                <td class="rightstyle">下载or切片完成是否上传至FTP</td>
                <td>
                    <input name="nns_config[upload_clip_to_ftp_enabled]" disabled type="radio" value="0" <?php if(!isset($edit_data['nns_config']['upload_clip_to_ftp_enabled']) || $edit_data['nns_config']['upload_clip_to_ftp_enabled'] == 0){echo "checked";}?>/>  否
                    <input name="nns_config[upload_clip_to_ftp_enabled]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['upload_clip_to_ftp_enabled'] == 1){echo "checked";}?>/>  是
                    【空或0表示否，默认0，0否 1是】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">下载or切片完成上传至指定FTP目录:</td>
                <td><?php echo $edit_data['nns_config']['upload_clip_to_ftp_dir_mode'];?> &nbsp;&nbsp;&nbsp;&nbsp;
                    【未配置或配置成0：不额外指定FTP目录，上传至配置的FTP绝对地址 ； 1：根据片源的nns_ext_url字段进行额外目录的指定，需注意ext_url需为字符串】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">下载or切片完成上传至FTP地址:</td>
                <td><?php echo $edit_data['nns_config']['upload_clip_to_ftp_url'];?> &nbsp;&nbsp;&nbsp;&nbsp;
                    【下载or切片完成上传至FTP地址，绝对路径】
                </td>
            </tr>

            <!--xinxin.deng 2017年11月09日14:55:00 添加图片是否上传到ftp  end-->
            <tr>
                <td class="rightstyle">转码开关：</td>
                <td>
                    <input name="nns_config[clip_file_encode_enable]" disabled onchange ="change2('nns_config[clip_file_encode_enable]', 'class_clip_to_encode');" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['clip_file_encode_enable']) || $edit_data['nns_config']['clip_file_encode_enable'] == 0){echo "checked";}?>/>  关闭
                    <input name="nns_config[clip_file_encode_enable]" disabled onchange ="change2('nns_config[clip_file_encode_enable]', 'class_clip_to_encode');" type="radio" value="1" <?php if($edit_data['nns_config']['clip_file_encode_enable'] == 1){echo "checked";}?>/>  开启
                    【切片完成后是否需要转码 开启:需要;关闭:不需要】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">转码模式：</td>
                <td>
                    <input name="nns_config[clip_file_encode_model]" disabled type="radio" value="0" <?php if($edit_data['nns_config']['clip_file_encode_model'] != 1 && $edit_data['nns_config']['clip_file_encode_model'] != 2){echo "checked";}?>/>  切片与转码在同个队列中
                    <input name="nns_config[clip_file_encode_model]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['clip_file_encode_model'] == 1){echo "checked";}?>/>  切片与转码队列分离
                    <input name="nns_config[clip_file_encode_model]" disabled type="radio" value="2" <?php if($edit_data['nns_config']['clip_file_encode_model'] == 2){echo "checked";}?>/>  不需要切片就可以转码
                    【转码模式机制 兼容以前重庆有线转码队列】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">转码负载均衡开关：</td>
                <td>
                    <input name="nns_config[transcode_load_balance_encode]" disabled type="radio" value="0" <?php if(!isset($edit_data['nns_config']['transcode_load_balance_encode']) || $edit_data['nns_config']['transcode_load_balance_encode'] == 0){echo "checked";}?>/>  关闭
                    <input name="nns_config[transcode_load_balance_encode]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['transcode_load_balance_encode'] == 1){echo "checked";}?>/>  开启
                    &nbsp;&nbsp;【转码负载均衡配置 0或者未配置  关闭 | 1 开启】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">转码请求厂商模式：</td>
                <td>
                    <?php echo $edit_data['nns_config']['clip_encode_model'];?>&nbsp;&nbsp;&nbsp;&nbsp;【0或者未配置 锐马视讯转码；1大洋转码；2云视转码】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">等待转码条数限制：</td>
                <td><?php echo $edit_data['nns_config']['clip_encode_wait_max'];?>&nbsp;&nbsp;&nbsp;&nbsp;【配置后允许等待转码并发的条数，未配置默认为5】</td>
            </tr>
            <tr>
                <td class="rightstyle">转码失败条数限制：</td>
                <td><?php echo $edit_data['nns_config']['clip_encode_fail_max'];?>&nbsp;&nbsp;&nbsp;&nbsp;【配置后允许转码失败后重发并发的条数，未配置默认失败数据不处理】</td>
            </tr>
            <tr>
                <td class="rightstyle">转码失败执行次数最大限制：</td>
                <td><?php echo $edit_data['nns_config']['clip_encode_fail_max_ex'];?>&nbsp;&nbsp;&nbsp;&nbsp;【配置后允许转码失败后小于该值的次数，未配置默认失败数据不处理】</td>
            </tr>
            <tr>
                <td class="rightstyle">转码片源输入地址：</td>
                <td><?php echo $edit_data['nns_config']['clip_file_addr_input'];?>&nbsp;&nbsp;&nbsp;&nbsp;【转码片源输入的地址目前只支持FTP,运维必须把目录置为可读可写】</td>
            </tr>
            <tr>
                <td class="rightstyle">转码片源输出地址：</td>
                <td><?php echo $edit_data['nns_config']['clip_file_addr_output'];?>&nbsp;&nbsp;&nbsp;&nbsp;【转码片源输出的地址目前只支持FTP,运维必须把目录置为可读可写】</td>
            </tr>
            <tr>
                <td class="rightstyle">转码抽帧图片输入地址：</td>
                <td><?php echo $edit_data['nns_config']['drawing_frame_addr_input'];?>&nbsp;&nbsp;&nbsp;&nbsp;【转码抽帧图片输入的地址目前只支持FTP,运维必须把目录置为可读可写】</td>
            </tr>
            <tr>
                <td class="rightstyle">转码抽帧图片输出地址：</td>
                <td><?php echo $edit_data['nns_config']['drawing_frame_addr_output'];?>&nbsp;&nbsp;&nbsp;&nbsp;【转码抽帧图片输出的地址目前只支持FTP,运维必须把目录置为可读可写】</td>
            </tr>
            <tr>
                <td class="rightstyle">转码API请求方式：</td>
                <td>
                    <input name="nns_config[clip_file_encode_way]" disabled type="radio" value="post" <?php if($edit_data['nns_config']['clip_file_encode_way'] == "post"){echo "checked";}?>/>  POST
                    <input name="nns_config[clip_file_encode_way]" disabled type="radio" value="get" <?php if($edit_data['nns_config']['clip_file_encode_way'] == 'get'){echo "checked";}?>/>  GET
                    【转码API请求方式,默认为POST】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">转码API请求地址：</td>
                <td><?php echo $edit_data['nns_config']['clip_file_encode_api_url'];?>&nbsp;&nbsp;&nbsp;&nbsp;【转码API的请求地址】</td>
            </tr>
            <tr>
                <td class="rightstyle">切片转码模式：</td>
                <td><?php echo $edit_data['nns_config']['media_encode_model'];?>&nbsp;&nbsp;&nbsp;&nbsp;【0或者未配置:下载完后原片也保留继续走队列;1:原片处理完毕后，原片的队列直接删除，只允许转码后的影片继续走队列】</td>
            </tr>
            <!-- 透传队列配置 start -->
            <tr>
                <td class="rightstyle"><b>透传队列配置</b></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">透传队列开关</td>
                <td>
                    <input name="nns_config[pass_queue_disabled]" disabled type="radio" value="0" <?php if(!isset($edit_data['nns_config']['pass_queue_disabled']) || $edit_data['nns_config']['pass_queue_disabled'] == 0){echo "checked";}?>/>  关闭
                    <input name="nns_config[pass_queue_disabled]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['pass_queue_disabled'] == 1){echo "checked";}?>/>  开启
                    &nbsp;&nbsp;【默认 关闭】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">透传队列是否开启自动审核</td>
                <td>
                    <input name="nns_config[pass_queue_audit]" disabled type="radio" value="0" <?php if(!isset($edit_data['nns_config']['pass_queue_audit']) || $edit_data['nns_config']['pass_queue_audit'] == 0){echo "checked";}?>/>  关闭
                    <input name="nns_config[pass_queue_audit]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['pass_queue_audit'] == 1){echo "checked";}?>/>  开启
                    &nbsp;&nbsp;【默认 关闭】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">透传队列注入地址</td>
                <td><?php echo $edit_data['nns_config']['pass_queue_import_url'];?></td>
            </tr>
            <!-- 透传队列配置 end -->
            <!-- C2指令配置 start -->
            <tr>
                <td class="rightstyle"><b>C2指令配置</b></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle"><i>C2注入主媒资操作</i></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">C2注入主媒资队列上限</td>
                <td><?php echo $edit_data['nns_config']['cdn_video_max'];?>&nbsp;&nbsp;&nbsp;&nbsp;不设置为100个</td>
            </tr>
            <tr>
                <td class="rightstyle"><i>C2注入分集操作</i></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">C2分集注入是否检查主媒资是否注入成功：</td>
                <td>
                    <input name="nns_config[cdn_index_check_video_enabled]" disabled type="radio" value="0" <?php if($edit_data['nns_config']['cdn_index_check_video_enabled'] == 0){echo "checked";}?>/>  关闭
                    <input name="nns_config[cdn_index_check_video_enabled]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['cdn_index_check_video_enabled'] == 1){echo "checked";}?>/>  开启
                    【开启:需要检查;关闭:不需要检查】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">C2注入分集队列上限</td>
                <td><?php echo $edit_data['nns_config']['cdn_index_max'];?>&nbsp;&nbsp;&nbsp;&nbsp;不设置为100个</td>
            </tr>
            <tr>
                <td class="rightstyle"><i>C2注入片源操作</i></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">C2片源注入是否检查主媒资是否注入成功：</td>
                <td>
                    <input name="nns_config[cdn_media_check_video_enabled]" disabled type="radio" value="0" <?php if($edit_data['nns_config']['cdn_media_check_video_enabled'] == 0){echo "checked";}?>/>  关闭
                    <input name="nns_config[cdn_media_check_video_enabled]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['cdn_media_check_video_enabled'] == 1){echo "checked";}?>/>  开启
                    【开启:需要检查;关闭:不需要检查】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">C2片源注入是否检查分集是否注入成功：</td>
                <td>
                    <input name="nns_config[cdn_media_check_index_enabled]" disabled type="radio" value="0" <?php if($edit_data['nns_config']['cdn_media_check_index_enabled'] == 0){echo "checked";}?>/>  关闭
                    <input name="nns_config[cdn_media_check_index_enabled]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['cdn_media_check_index_enabled'] == 1){echo "checked";}?>/>  开启
                    【开启:需要检查;关闭:不需要检查】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">C2注入片源队列上限</td>
                <td><?php echo $edit_data['nns_config']['cdn_media_max'];?>&nbsp;&nbsp;&nbsp;&nbsp;不设置为100个</td>
            </tr>
            <tr>
                <td class="rightstyle">C2任务总数限制模式</td>
                <td><?php echo $edit_data['nns_config']['c2_count_mode'];?>&nbsp;&nbsp;&nbsp;&nbsp;0:等待执行;1:执行失败;2:正在执行;3:等待获取CDN播放串;4:注入取消;5:正在获取CDN播放串;6:获取播放传失败(多个以逗号,分隔)</td>
            </tr>
            <tr>
                <td class="rightstyle">C2任务跨域地址：</td>
                <td><?php echo $edit_data['nns_config']['c2_play_url_ip'];?>&nbsp;&nbsp;&nbsp;&nbsp;【CDN拼串跨域配置，只支持单个IP】</td>
            </tr>
            <!-- C2指令配置 end -->
            <!-- CDN指令配置 start -->
            <tr>
                <td class="rightstyle"><b>CDN指令配置</b></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">CDN注入暂停</td>
                <td>
                    <input name="nns_config[disabled_cdn]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['disabled_cdn'] == 1){echo "checked";}?>/>  关闭
                    <input name="nns_config[disabled_cdn]" disabled type="radio" value="0" <?php if(!isset($edit_data['nns_config']['disabled_cdn']) || $edit_data['nns_config']['disabled_cdn'] == 0){echo "checked";}?>/>  开启
                    &nbsp;&nbsp;【默认开启】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">CDN允许注入媒资开关：</td>
                <td>
                    <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="video"      <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                        && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('video', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked disabled";}?>/>  主媒资
                    <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="index"      <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                        && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('index', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked disabled";}?>/>  分集
                    <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="media"      <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                        && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('media', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked disabled";}?>/>  片源
                    <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="live"       <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                        && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('live', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked disabled";}?>/>  直播频道
                    <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="live_index" <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                        && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('live_index', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked disabled";}?>/>  频道分集
                    <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="live_media" <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                        && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('live_media', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked disabled";}?>/>  直播源
                    <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="playbill"   <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                        && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('playbill', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked disabled";}?>/>  节目单
                    <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="file"       <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                        && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('file', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked disabled";}?>/>  文件
                    <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="category"   <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                        && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('category', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked disabled";}?>/>  资源库栏目
                    <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="product"    <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                        && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('product', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked disabled";}?>/>  产品包
                    <input name="nns_config[cdn_assets_enabled][]" id="nns_config" type="checkbox" value="product"    <?php if(isset($edit_data['nns_config']['cdn_assets_enabled'])
                        && is_array($edit_data['nns_config']['cdn_assets_enabled']) && in_array('epg_file', $edit_data['nns_config']['cdn_assets_enabled'])){echo "checked disabled";}?>/>  EPGFile模板文件
                    【未选择队列不运行注入队列】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">CDN注入栏目关闭</td>
                <td><?php echo $edit_data['nns_config']['disabled_category'];?>&nbsp;&nbsp;&nbsp;&nbsp;1为关闭注入，0或则不设置为开启注入</td>
            </tr>
            <tr>
                <td class="rightstyle">不显示CDN注入</td>
                <td>
                    <input name="nns_config[cdn_display_import]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['cdn_display_import'] == 1){echo "checked";}?>/>  隐藏
                    <input name="nns_config[cdn_display_import]" disabled type="radio" value="0" <?php if(!isset($edit_data['nns_config']['cdn_display_import']) || $edit_data['nns_config']['cdn_display_import'] == 0){echo "checked";}?>/>  显示
                    &nbsp;&nbsp;【默认 显示】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">CDN注入条数</td>
                <td><?php echo $edit_data['nns_config']['cdn_exec_num'];?>&nbsp;&nbsp;&nbsp;&nbsp;CDN注入条数，默认100</td>
            </tr>
            <tr>
                <td class="rightstyle">发送模式</td>
                <td>
                    <input name="nns_config[cdn_send_mode]" disabled type="radio" onchange ="change();" value="0" <?php if($edit_data['nns_config']['cdn_send_mode'] == 0){echo "checked";}?>/>  SOAP
                    <input name="nns_config[cdn_send_mode]" disabled type="radio" onchange ="change();" value="1" <?php if($edit_data['nns_config']['cdn_send_mode'] == 1){echo "checked";}?>/>  HTTP
                    <input name="nns_config[cdn_send_mode]" disabled type="radio" onchange ="change();" value="2" <?php if($edit_data['nns_config']['cdn_send_mode'] == 2){echo "checked";}?>/>  FTP
                    【发送消息到第三方CDN厂家的请求协议 默认SOAP】
                </td>
            </tr>
            <?php if($edit_data['nns_config']['cdn_send_mode'] == 1){?>
                <tr class="cdn_send_mode_url">
                    <td class="rightstyle">发送ADI URL</td>
                    <td>
                        <?php echo $edit_data['nns_config']['cdn_send_mode_url'];?>【使用HTTP的方式注入CDN的URL请求地址，只是注入媒资元数据】
                    </td>
                </tr>
                <tr class="cdn_send_mode_url">
                    <td class="rightstyle">发送CDI URL</td>
                    <td>
                        <?php echo $edit_data['nns_config']['cdn_send_cdi_mode_url'];?>【使用HTTP的方式注入CDN的URL请求地址，只是注入媒资元数据的上下线操作】
                    </td>
                </tr>
            <?php }?>
            <tr>
                <td class="rightstyle">CDN注入XML指令FTP地址</td>
                <td><?php echo $edit_data['nns_config']['xml_ftp'];?></td>
            </tr>
            <tr>
                <td class="rightstyle">CDN注入片源FTP地址</td>
                <td><?php echo $edit_data['nns_config']['media_ftp'];?></td>
            </tr>
            <tr>
                <td class="rightstyle">CDN注入片源FTP检测地址</td>
                <td><?php echo $edit_data['nns_config']['cdn_check_ts_accord_address'];?> FTP下载和检测地址不一致时配置该配置(主动模式)</td>
            </tr>
            <tr>
                <td class="rightstyle">CDN注入图片FTP地址</td>
                <td><?php echo $edit_data['nns_config']['img_ftp'];?></td>
            </tr>
            <tr>
                <td class="rightstyle">CDN注入片源模式</td>
                <td>
                    <input name="nns_config[cdn_import_media_mode]" disabled type="radio" value="0" <?php if($edit_data['nns_config']['cdn_import_media_mode'] == 0){echo "checked";}?>/>  单独模式
                    <input name="nns_config[cdn_import_media_mode]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['cdn_import_media_mode'] == 1){echo "checked";}?>/>  混合模式
                    <input name="nns_config[cdn_import_media_mode]" disabled type="radio" value="2" <?php if($edit_data['nns_config']['cdn_import_media_mode'] == 2){echo "checked";}?>/>  单片码率最高
                    &nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td class="rightstyle">CDN失败重试模式</td>
                <td><?php echo $edit_data['nns_config']['cdn_fail_retry_mode'];?>&nbsp;&nbsp;&nbsp;&nbsp;  CDN失败重试模式 0,根据CDN失败重试次数下发等待注入的任务,默认不重试</td>
            </tr>
            <tr>
                <td class="rightstyle">CDN失败重试次数</td>
                <td><?php echo $edit_data['nns_config']['cdn_fail_retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;  CDN失败重试次数，默认0</td>
            </tr>
            <tr>
                <td class="rightstyle">CDN失败重试条数</td>
                <td><?php echo $edit_data['nns_config']['cdn_exec_fail_num'];?>&nbsp;&nbsp;&nbsp;&nbsp;  CDN失败重试条数，默认20</td>
            </tr>
            <!-- CDN指令配置 end -->

            <!-- EPG指令配置 start -->
            <tr>
                <td class="rightstyle"><b>EPG指令配置</b></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">EPG注入暂停</td>
                <td><?php echo $edit_data['nns_config']['disabled_epg'];?>&nbsp;&nbsp;&nbsp;&nbsp;1为关闭EPG注入，0或则不设置为开启EPG注入</td>
            </tr>
            <tr>
                <td class="rightstyle">EPG允许注入媒资开关：</td>
                <td>
                    <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="video"      <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                        && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('video', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked disabled";}?>/>  主媒资
                    <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="index"      <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                        && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('index', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked disabled";}?>/>  分集
                    <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="media"      <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                        && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('media', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked disabled";}?>/>  片源
                    <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="live"       <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                        && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('live', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked disabled";}?>/>  直播频道
                    <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="live_index" <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                        && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('live_index', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked disabled";}?>/>  频道分集
                    <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="live_media" <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                        && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('live_media', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked disabled";}?>/>  直播源
                    <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="playbill"   <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                        && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('playbill', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked disabled";}?>/>  节目单
                    <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="file"       <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                        && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('file', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked disabled";}?>/>  文件
                    <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="category"   <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                        && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('category', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked disabled";}?>/>  资源库栏目
                    <input name="nns_config[epg_assets_enabled][]" id="nns_config" type="checkbox" value="product"    <?php if(isset($edit_data['nns_config']['epg_assets_enabled'])
                        && is_array($edit_data['nns_config']['epg_assets_enabled']) && in_array('product', $edit_data['nns_config']['epg_assets_enabled'])){echo "checked disabled";}?>/>  产品包
                    【未选择队列不运行注入队列】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">不显示EPG注入</td>
                <td>
                    <input name="nns_config[epg_display_import]" disabled type="radio" value="0" <?php if(!isset($edit_data['nns_config']['epg_display_import']) || $edit_data['nns_config']['epg_display_import'] == 0){echo "checked";}?>/>  显示
                    <input name="nns_config[epg_display_import]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['epg_display_import'] == 1){echo "checked";}?>/>  隐藏
                    &nbsp;&nbsp;【默认 显示】
                </td>
            </tr>

            <tr>
                <td class="rightstyle">不显示EPG删除按钮</td>
                <td>
                    <input name="nns_config[epg_display_import_delete]" disabled type="radio" value="0" <?php if(!isset($edit_data['nns_config']['epg_display_import_delete']) || $edit_data['nns_config']['epg_display_import_delete'] == 0){echo "checked";}?>/>  显示
                    <input name="nns_config[epg_display_import_delete]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['epg_display_import_delete'] == 1){echo "checked";}?>/>  隐藏
                    &nbsp;&nbsp;【默认 显示】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">多剧集单集注入方式</td>
                <td>
                    <input name="nns_config[epg_single_index_import_model]" disabled type="radio" value="0" <?php if(!isset($edit_data['nns_config']['epg_single_index_import_model']) || $edit_data['nns_config']['epg_single_index_import_model'] == 0){echo "checked";}?>/>  单集主媒资注入CMS
                    <input name="nns_config[epg_single_index_import_model]" disabled type="radio" value="1" <?php if($edit_data['nns_config']['epg_single_index_import_model'] == 1){echo "checked";}?>/>  注入分集不注入单集主媒资（仅限多剧集以单集形式存在才配置）
                    &nbsp;&nbsp;【默认 单集主媒资注入CMS】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">EPG注入下发条数</td>
                <td><?php echo $edit_data['nns_config']['epg_pre_max'];?>&nbsp;&nbsp;&nbsp;&nbsp;EPG每次下发的个数（不设置为100）</td>
            </tr>
            <tr>
                <td class="rightstyle">EPG注入切片路径</td>
                <td><?php echo $edit_data['nns_config']['import_srouce_url'];?>&nbsp;&nbsp;&nbsp;&nbsp;0为注入原始相对路径，1为注入切片后相对路径</td>
            </tr>
            <tr>
                <td class="rightstyle">EPG注入点播接口</td>
                <td><?php echo $edit_data['nns_config']['epg_url'];?></td>
            </tr>
            <tr>
                <td class="rightstyle">EPG节目单注入接口</td>
                <td><?php echo $edit_data['nns_config']['epg_playbill_url'];?></td>
            </tr>

            <tr>
                <td class="rightstyle">EPG注入携带CORE绑定ID</td>
                <td>
                    <input style="position: relative; top:2px;"  name="nns_config[import_core_bind_id_enabled]" disabled  type="radio"  value="1" <?php if($action=="edit" && $edit_data['nns_config']['import_core_bind_id_enabled']=='1'){echo "checked='checked'";}?> />是
                    <input style="position: relative; top:2px;"  name="nns_config[import_core_bind_id_enabled]" disabled type="radio"  value="0" <?php if($action=="edit" && $edit_data['nns_config']['import_core_bind_id_enabled']=='0'){echo "checked='checked'";}?> />否
                    【默认否】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">EPG失败重试次条数</td>
                <td><?php echo $edit_data['nns_config']['epg_retry_time']; ?>&nbsp;&nbsp;&nbsp;&nbsp;EPG失败重试次数,默认0不重试</td>
            </tr>
            <tr>
                <td class="rightstyle">EPG失败重试条数</td>
                <td><?php echo $edit_data['nns_config']['epg_fail_max']; ?>&nbsp;&nbsp;&nbsp;&nbsp;EPG失败重试条数,默认20</td>
            </tr>
            <!-- EPG指令配置 end -->

            <tr>
                <td class="rightstyle"><b><i>媒资全局操作</i></b></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">媒资CDN共享地址:</td>
                <td><?php echo $edit_data['nns_config']['asset_cdn_share_addr'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;【提供CMS方拉取文件的相对地址】</td>
            </tr>
            <tr>
                <td class="rightstyle">主媒资全局注入模式:</td>
                <td><?php echo $edit_data['nns_config']['video_cdn_model'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;【0或则未设置播控作为CMS转发平台;1播控作为CDN响应平台】</td>
            </tr>
            <tr>
                <td class="rightstyle">分集全局注入模式:</td>
                <td><?php echo $edit_data['nns_config']['index_cdn_model'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;【0或则未设置播控作为CMS转发平台;1播控作为CDN响应平台】</td>
            </tr>
            <tr>
                <td class="rightstyle">片源全局注入模式:</td>
                <td><?php echo $edit_data['nns_config']['media_cdn_model'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;【0或则未设置播控作为CMS转发平台;1播控作为CDN响应平台;2播控作为CMS转发平台也作为CDN响应平台，且cdn注入后就反馈上游消息】</td>
            </tr>
            <tr>
                <td class="rightstyle">片源全局注入模式:</td>
                <td><?php echo $edit_data['nns_config']['asset_import_cms_model'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;【0或者为配置原有逻辑注入CMS，1播控注入播控推入跳板机(下游方只能拉取数据的情况，不能上游往下游推送)】</td>
            </tr>
            <tr>
                <td class="rightstyle"><b><i>主媒资操作</i></b></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle"><i>主媒资添加资操作</i></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['video_add']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>
            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['video_add']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['video_add']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <!--			<tr>-->
            <!--				<td class="rightstyle">主媒资添加CDN失败重试次数:</td>-->
            <!--				<td>--><?php //echo $edit_data['nns_config']['video_add']['cdn_retry_time'];?><!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;主媒资CDN注入添加失败重试次数，空则为原逻辑</td>-->
            <!--			</tr>-->
            <tr>
                <td class="rightstyle">主媒资添加失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['video_add']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;主媒资EPG注入添加失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle">主媒资CDN注入:</td>
                <td><?php echo $edit_data['nns_config']['video_add']['cdn_import_model'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入</td>
            </tr>

            <tr>
                <td class="rightstyle"><i>主媒资修改操作</i></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['video_modify']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>
            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['video_modify']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['video_modify']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">主媒资修改失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['video_modify']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;主媒资EPG注入修改失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle">主媒资CDN注入:</td>
                <td><?php echo $edit_data['nns_config']['video_modify']['cdn_import_model'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入</td>
            </tr>
            <tr>
                <td class="rightstyle"><i>主媒资删除操作</i></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['video_destroy']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>
            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['video_destroy']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['video_destroy']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">主媒资删除失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['video_destroy']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;主媒资EPG注入删除失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle">主媒资CDN注入:</td>
                <td><?php echo $edit_data['nns_config']['video_destroy']['cdn_import_model'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入</td>
            </tr>
            <tr>
                <td class="rightstyle"><b><i>分集操作</i></b></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle"><i>分集添加操作</i></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['index_add']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>
            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['index_add']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['index_add']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">分集添加失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['index_add']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;分集EPG注入添加失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle">分集CDN注入:</td>
                <td><?php echo $edit_data['nns_config']['index_add']['cdn_import_model'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入</td>
            </tr>

            <tr>
                <td class="rightstyle"><i>分集修改操作</i></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['index_modify']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>
            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['index_modify']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['index_modify']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">分集修改失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['index_modify']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;分集EPG注入修改失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle">分集CDN注入:</td>
                <td><?php echo $edit_data['nns_config']['index_modify']['cdn_import_model'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入</td>
            </tr>

            <tr>
                <td class="rightstyle"><i>分集删除操作</i></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['index_destroy']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>
            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['index_destroy']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['index_destroy']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">分集删除失败重次数:</td>
                <td><?php echo $edit_data['nns_config']['index_destroy']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;分集EPG注入删除失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle">分集CDN注入:</td>
                <td><?php echo $edit_data['nns_config']['index_destroy']['cdn_import_model'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入</td>
            </tr>

            <tr>
                <td class="rightstyle"><b><i>片源操作</i></b></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle"><i>片源添加操作</i></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['media_add']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>

            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['media_add']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['media_add']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">片源添加失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['media_add']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;片源EPG注入添加失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle">片源CDN注入:</td>
                <td><?php echo $edit_data['nns_config']['media_add']['cdn_import_model'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入</td>
            </tr>

            <tr>
                <td class="rightstyle"><i>片源修改操作</i></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['media_modify']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>
            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['media_modify']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['media_modify']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">片源修改失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['media_modify']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;片源EPG注入修改失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle">片源CDN注入:</td>
                <td><?php echo $edit_data['nns_config']['media_modify']['cdn_import_model'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入</td>
            </tr>

            <tr>
                <td class="rightstyle"><i>片源删除操作</i></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['media_destroy']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>
            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['media_destroy']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['media_destroy']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">片源删除失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['media_destroy']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;片源EPG注入删除失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle">片源CDN注入:</td>
                <td><?php echo $edit_data['nns_config']['media_destroy']['cdn_import_model'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或未配置为电影和连续剧注入，1为仅电影注入，2为仅连续剧注入，3为都不注入</td>
            </tr>

            <tr>
                <td class="rightstyle"><b><i>直播频道操作</i></b></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle"><i>直播频道添加操作</i></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['live_add']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>

            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['live_add']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['live_add']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">注入失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['live_add']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;直播EPG注入添加失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle"><i>直播频道修改操作</i></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['live_modify']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>

            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['live_modify']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['live_modify']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">注入失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['live_modify']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;直播EPG注入添加失败重试次数，空则为原逻辑</td>
            </tr><tr>
                <td class="rightstyle"><i>直播频道删除操作</i></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['live_destroy']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>

            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['live_destroy']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['live_destroy']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">注入失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['live_destroy']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;直播EPG注入添加失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle"><b><i>直播源操作</i></b></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle"><i>直播源添加操作</i></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['live_media_add']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>

            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['live_media_add']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['live_media_add']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">注入失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['live_media_add']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;直播EPG注入添加失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle"><i>直播源修改操作</i></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['live_media_modify']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>

            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['live_media_modify']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['live_media_modify']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">注入失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['live_media_modify']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;直播EPG注入添加失败重试次数，空则为原逻辑</td>
            </tr><tr>
                <td class="rightstyle"><i>直播源删除操作</i></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['live_media_destroy']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>

            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['live_media_destroy']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['live_media_destroy']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">注入失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['live_media_destroy']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;直播EPG注入添加失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle"><b><i>节目单操作</i></b></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">节目单分组开关:</td>
                <td><?php if($edit_data['nns_config']['playbill']['enabled'] == '1'){ echo '开启';}else{ echo '关闭';}?>
                    【开启：以直播源分组查询节目单信息，批量注入CDN;关闭：默认，单条节目单注入】</td>

            </tr>
            <tr>
                <td class="rightstyle">注入失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['playbill']['group_num'];?>【如果节目单分组开关开启这个需要配置，未配置默认100条】</td>
            </tr>
            <tr>
                <td class="rightstyle"><i>节目单添加操作</i></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['playbill_add']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>

            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['playbill_add']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['playbill_add']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">节目单注入失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['playbill_add']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;节目单EPG注入添加失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle"><i>节目单修改操作</i></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['playbill_modify']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>

            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['playbill_modify']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['playbill_modify']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">节目单注入失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['playbill_modify']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;节目单EPG注入添加失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle"><i>节目单删除操作</i></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['playbill_destroy']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>

            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['playbill_destroy']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['playbill_destroy']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">节目单注入失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['playbill_destroy']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;节目单EPG注入添加失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle"><b><i>产品包操作</i></b></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle"><i>产品包添加操作</i></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['product_add']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>

            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['product_add']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['product_add']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">注入失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['product_add']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EPG注入添加失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle"><i>产品包修改操作</i></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['product_modify']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>

            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['product_modify']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['product_modify']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">注入失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['product_modify']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EPG注入添加失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle"><i>产品包删除操作</i></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['product_destroy']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>

            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['product_destroy']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['product_destroy']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">注入失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['product_destroy']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EPG注入添加失败重试次数，空则为原逻辑</td>
            </tr>

            <tr>
                <td class="rightstyle"><b><i>EPGFile模板文件操作</i></b></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle"><i>EPGFile模板文件添加操作</i></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['epg_file_add']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>

            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['epg_file_add']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['epg_file_add']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">注入失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['epg_file_add']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EPG注入添加失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle"><i>EPGFile模板文件修改操作</i></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['epg_file_modify']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>

            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['epg_file_modify']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['epg_file_modify']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">注入失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['epg_file_modify']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EPG注入添加失败重试次数，空则为原逻辑</td>
            </tr>
            <tr>
                <td class="rightstyle"><i>EPGFile模板文件删除操作</i></td>
                <td> </td>
            </tr>

            <tr>
                <td class="rightstyle">最终状态:</td>
                <td><?php echo $edit_data['nns_config']['epg_file_destroy']['status'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0为注入CDN和EPG，1为注入CDN，2为注入EPG</td>
            </tr>

            <tr>
                <td class="rightstyle">优先执行注入:</td>
                <td><?php echo $edit_data['nns_config']['epg_file_destroy']['priority'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0或不设为优先执行CDN注入，再执行EPG注入，1为优先执行EPG注入，再执行CDN注入</td>
            </tr>
            <tr>
                <td class="rightstyle">自动审核:</td>
                <td><?php echo $edit_data['nns_config']['epg_file_destroy']['audit'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若为1则所有操作队列进来状态为待审核状态，0或则空则为通过审核</td>
            </tr>
            <tr>
                <td class="rightstyle">注入失败重试次数:</td>
                <td><?php echo $edit_data['nns_config']['epg_file_destroy']['retry_time'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EPG注入添加失败重试次数，空则为原逻辑</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="controlbtns">
        <div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        <div class="controlbtn edit">
            <a action="edit" href="nncms_content_sp_addedit.php?action=edit&nns_id=<?php echo $_GET['nns_id']; ?>">修改</a></div>
        <div style="clear:both;"></div>
    </div>

</div>


</body>
</html>
