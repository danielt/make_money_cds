<?php
header("Content-Type:text/html;charset=utf-8");
ini_set('display_errors',0);
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

 //加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136005");
$checkpri=null;
//echo $_SESSION["nns_role_pris"];die;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");


include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
if (!empty($_COOKIE["page_max_num"])){
	$g_manager_list_max_num=$_COOKIE["page_max_num"];
}

	
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv_v2/models/sp_model.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/cp/cp.class.php';
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
//分页控制
//$page_info = array ();
//$page_info['page_num'] = isset($_GET['page']) ? $_GET['page'] : 1;
//$page_info['page_size'] = isset($_GET['page_size']) ? $_GET['page_size'] : (isset($_COOKIE['page_max_num']) && $_COOKIE['page_max_num'] > 0) ? $_COOKIE['page_max_num'] : 18;

$data = nl_sp::query($dc,$page_info);
if($data['ret'] !=0)
{
	echo '<script>alert("'.$data['reason'].'");history.go(-1);</script>';die;
}
//$url = 'nncms_content_cp_info_list.php?flag_url=1';
//foreach ($_REQUEST as $k => $v)
//{
//	if ($k != 'flag_url' && $k != 'page')
//	{
//		$url .= '&' . $k . '=' . $v;
//	}
//}
//$pager = new nl_pager($result['page_info']['total_count'], $page_info['page_size'], $page_info['page_num'], $url);
$data = sp_model::get_sp_list();

//播控新版
global $g_bk_version_number;
$bk_version_number = $g_bk_version_number == '1' ? true : false;
unset($g_bk_version_number);

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
<script language="javascript">
function checkhiddenInput(){
	var par=getAllCheckBoxSelect();
	if (par==""){
		alert("<?php echo cms_get_lang('partner_msg_noselect');?>");
	}else{
		$('#nns_id').attr('value',getAllCheckBoxSelect());
		checkForm('<?php echo cms_get_lang('xtgl_hzgl');?>','<?php echo cms_get_lang('msg_ask_delete');?>',$('#delete_form'));
	}

}
function change_state(id,state) {
	var url = "nncms_content_sp_control.php?action=state&nns_id="+id+"&nns_state="+state;
	window.location.href = url;
}
function begin_select_cp(sp_id,cp_id){
	$("#select_frame").attr("src","nncms_content_bind_cp_list.php?cp_id="+cp_id+"&sp_id="+sp_id);
	$(".selectbox").show();
	$('.selectbox iframe').load(function () {
		$(".selectbox").css({
			top: 0
		});
	});
}
$(document).ready(function() {
	$(".selectbox").hide();
});

function close_select() {
	$(".selectbox").hide();
}
</script>
</head>

<body>
<div class="selectbox">
			<iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
		</div>
<div class="content">
	<div class="content_position">SP 管理 > SP 列表</div>
    <div class="content_table formtable">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
        	<tr>
            	<th><input name="" type="checkbox" value="" /></th>
                <th><?php echo cms_get_lang('segnumber');?></th>
                <th>SP ID</th>
                <th><?php echo cms_get_lang('name');?></th>
                <th>绑定CP</th>
                <th><?php echo cms_get_lang('create_time');?></th>
                <th><?php echo cms_get_lang('edit_time');?></th>
                <th>状态</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
        <?php
        	if ($data!=null){
        	 $num=0;
        	 $temp_cp_info = array();
        	 foreach($data as $item){
        	$num++;
        	?>
              <tr>
                <td><input name="input" type="checkbox" value="<?php echo  $item["nns_id"];?>" /></td>
                <td><?php echo $num;?></td>
                <td><?php echo $item['nns_id'];?></td>
                  <?php if ($bk_version_number){ ?>
                      <td><a href="nncms_content_sp_detail_v2.php?nns_id=<?php echo $item["nns_id"];?>"><?php echo $item["nns_name"];?></a></td>
                  <?PHP }else{?>
                      <td><a href="nncms_content_sp_detail.php?nns_id=<?php echo $item["nns_id"];?>"><?php echo $item["nns_name"];?></a></td>
                  <?PHP }?>

                <td>
                	<?php
                		if(strlen($item["nns_bind_cp"]) < 1)
						{
							echo "<font style=\"color:#FF0000\">无CP绑定</font>";
						}
						else
						{
							if(!isset($temp_cp_info[$item["nns_bind_cp"]]))
							{
								$item["nns_bind_cp"] = rtrim($item["nns_bind_cp"],',');
								$bind_cp = explode(',', $item["nns_bind_cp"]);
		                		$result_bind_cp=nl_cp::query_by_more_id($dc, $bind_cp);
		                		foreach ($result_bind_cp['data_info'] as $cp_info)
								{
									$temp_array[] = $cp_info['nns_id'].'/'.$cp_info['nns_name'];
								}
								echo implode("<br/>", $temp_array);
								unset($temp_array);
		                		//$temp_cp_info[$item["nns_bind_cp"]] = isset($result_bind_cp['data_info']['nns_id']) ? $result_bind_cp['data_info']['nns_id'].'/'.$result_bind_cp['data_info']['nns_name'] : '';
							}
							//echo $temp_cp_info[$item["nns_bind_cp"]];
						}
                	?>
                </td>
                <td><?php echo $item["nns_create_time"];?> </td>
                <td><?php echo $item["nns_modify_time"];?> </td>
                <td><a href="javascript:change_state('<?php echo $item['nns_id']?>',<?php echo ($item['nns_state'] !=0) ? 0 :1;?>);"><?php echo ($item['nns_state'] !='0') ? '<font color="red">禁用</font>' : '<font color="green">启用</font>'; ?></a></td>
						          
                <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                 <?php if ($bk_version_number){ ?>
                	<a href="nncms_content_sp_addedit_v2.php?action=edit&nns_id=<?php echo $item["nns_id"];?>">修改</a>
                 <?PHP }else{?>
                     <a href="nncms_content_sp_addedit.php?action=edit&nns_id=<?php echo $item["nns_id"];?>">修改</a>
                 <?PHP }?>
                	<a href="nncms_content_sp_control.php?action=delete&nns_id=<?php echo $item["nns_id"];?>">删除</a>
					<a href="javascript:begin_select_cp('<?php echo $item["nns_id"];?>','<?php echo $item["nns_bind_cp"];?>');" action="edit">绑定</a>
					<?php 
						if(strlen($item["nns_bind_cp"]) > 0)
						{
					?>
							<a href="nncms_content_sp_control.php?action=unbind&nns_id=<?php echo $item["nns_id"];?>">解绑</a>
					<?php 
						}
                	?>
                </td>
              </tr>
          <?php }} $least_num=$nncms_ui_min_list_item_count-count($data);
          		for ($i=0;$i<$least_num;$i++){?>
          		<tr>
         		<td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              	</tr>

          	<?php	}?>
          </tbody>
        </table>
    </div>
<?php //echo $pager->nav();?>
    <div class="controlbtns">
    	<div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);"><?php echo cms_get_lang('select');?></a></div>
        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);"><?php echo cms_get_lang('cancel');?></a></div>
        <?php if ($bk_version_number){ ?>
            <div class="controlbtn add"><a href="javascript:void(null);" pos="nncms_content_sp_addedit_v2.php" action="add"><?php echo cms_get_lang('add');?></a></div>
            <div class="controlbtn edit"><a href="javascript:void(null);" pos="nncms_content_sp_addedit_v2.php" action="edit"><?php echo cms_get_lang('edit');?></a></div>
        <?PHP }else{?>
            <div class="controlbtn add"><a href="javascript:void(null);" pos="nncms_content_sp_addedit.php" action="add"><?php echo cms_get_lang('add');?></a></div>
            <div class="controlbtn edit"><a href="javascript:void(null);" pos="nncms_content_sp_addedit.php" action="edit"><?php echo cms_get_lang('edit');?></a></div>
        <?PHP }?>
        <div class="controlbtn delete"><a href="javascript:checkhiddenInput();"  action="delete"><?php echo cms_get_lang('delete');?></a></div>
        <div style="clear:both;"></div>
    </div>
     <form id="delete_form" action="nncms_content_sp_control.php" method="post">
      <input name="action" id="action" type="hidden" value="delete" />
      <input name="nns_id" id="nns_id" type="hidden" value="" />
     </form>
</div>

</body>
</html>
