<?php
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/asset_online/asset_online.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/sp/sp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
//导入语言包
if (isset($_SESSION["language_dir"]))
{
	$language_dir = $_SESSION["language_dir"];
}
include_once $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include_once $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool)
{
	Header("Location: ../nncms_content_wrong.php");
	exit();
}
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE, 
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$sp_id = $_REQUEST['sp_id'];
$sp_data = nl_sp::query_by_id($dc,$sp_id);
$sp_data = isset($sp_data['data_info']) ? $sp_data['data_info'] : array();
if(isset($_REQUEST['op']) && !empty($_REQUEST['op']))
{
	if($_REQUEST['op'] == 'show')
	{
		header("Content-Type:text/html;charset=utf-8");
		$result=nl_asset_online_log::query_by_id($dc,$_REQUEST['ids']);
		$nns_content='nns_import_content';
		if($_REQUEST['type'] == 'callback')
		{
			$nns_content = 'nns_callback_content';
		}
		echo isset($result['data_info'][0][$nns_content]) ? $result['data_info'][0][$nns_content] : '<?xml version="1.0" encoding="utf-8"?><result>no data</result>';die;
	}
}
$page_info = $params = array ();
$params['where']['nns_org_id'] = $sp_id;
$page_info['page_num'] = isset($_GET['page']) ? $_GET['page'] : 1;
$page_info['page_size'] = isset($_GET['page_size']) ? $_GET['page_size'] : (isset($_COOKIE['page_max_num']) && $_COOKIE['page_max_num'] > 0) ? $_COOKIE['page_max_num'] : 18;
if (isset($_REQUEST['nns_video_name']) && strlen($_REQUEST['nns_video_name']) > 0)
{
	$params['like']['nns_video_name'] = $_REQUEST['nns_video_name'];
}
if (isset($_REQUEST['nns_message_id']) && strlen($_REQUEST['nns_message_id']) > 0)
{
	$params['where']['nns_message_id'] = $_REQUEST['nns_message_id'];
}
if (isset($_REQUEST['nns_import_id']) && strlen($_REQUEST['nns_import_id']) > 0)
{
	$params['where']['nns_import_id'] = $_REQUEST['nns_import_id'];
}
if (isset($_REQUEST['nns_status']) && strlen($_REQUEST['nns_status']) > 0)
{
	$params['where']['nns_status'] = $_REQUEST['nns_status'];
}
if (isset($_REQUEST['nns_action']) && strlen($_REQUEST['nns_action']) > 0)
{
	$params['where']['nns_action'] = $_REQUEST['nns_action'];
}
if (isset($_REQUEST['nns_category_id']) && strlen($_REQUEST['nns_category_id']) > 0)
{
	$params['where']['nns_category_id'] = $_REQUEST['nns_category_id'];
}
if (isset($_REQUEST['create_begin_time']) && strlen($_REQUEST['create_begin_time']) > 0)
{
	$params['where']['create_begin_time'] = $_REQUEST['create_begin_time'];
}
if (isset($_REQUEST['create_end_time']) && strlen($_REQUEST['create_end_time']) > 0)
{
	$params['where']['create_end_time'] = $_REQUEST['create_end_time'];
}
if (isset($_REQUEST['nns_asset_online_id']) && strlen($_REQUEST['nns_asset_online_id']) > 0)
{
	$params['where']['nns_asset_online_id'] = $_REQUEST['nns_asset_online_id'];
}
$url = 'nncms_content_asset_online_log.php?flag_url=1';
foreach ($_REQUEST as $k => $v)
{
	if ($k != 'flag_url' && $k != 'page')
	{
		$url .= '&' . $k . '=' . $v;
	}
}

$result = nl_asset_online_log::query($dc, $params, $page_info);
$pager = new nl_pager($result['page_info']['total_count'], $page_info['page_size'], $page_info['page_num'], $url);
//if ($result['ret'] != 0)
//{
//	echo '<script>alert("' . $result['reason'] . '");</script>';
//}
//if ($result_sp['ret'] != 0)
//{
//	echo '<script>alert("' . $result_sp['reason'] . '");</script>';
//}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
<link href="../../css/comm.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/rate.js"></script>
<script language="javascript" src="../../js/image_loaded_func.js"></script>
<script language="javascript" src="../../js/cms_datepicker.js"></script>
<script language="javascript" src="../../js/alertbox.js"></script>
<script language="javascript">
			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				})
				return BoxKey;
			}
			
			$(document).ready(function() {
				$('#clear_time').click(function(){
					$('#create_begin_time').val('');
					$('#create_end_time').val('');

				});
				window.parent.now_frame_url = window.location.href;
			});

			$(document).ready(function() {
				$('#clear_time_modify').click(function(){
					$('#nns_begin_time').val('');
					$('#nns_end_time').val('');

				});
				window.parent.now_frame_url = window.location.href;
			});
		</script>
</head>
<body>
	<div class="content">
		<div class="content_position">上下线消息列表</div>
		<div class="content_table">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<form action="nncms_content_asset_online_log.php" method="get">
					<tbody>
						<tr>
						<td>
							 &nbsp;&nbsp;&nbsp;&nbsp;消息ID：&nbsp;&nbsp;<input type="text" name="nns_message_id"
								id="nns_message_id" style="width:150px;"
								value="<?php echo $_GET['nns_message_id']; ?>"
								>
							 &nbsp;&nbsp;&nbsp;&nbsp;影片名称：&nbsp;&nbsp;<input type="text" name="nns_video_name"
								id="nns_video_name" style="width:150px;"
								value="<?php echo $_GET['nns_video_name']; ?>"
								>
							 &nbsp;&nbsp;&nbsp;&nbsp;队列ID：&nbsp;&nbsp;<input type="text" name="nns_asset_online_id"
								id="nns_asset_online_id" style="width:150px;"
								value="<?php echo $_GET['nns_asset_online_id']; ?>"
								>
							 &nbsp;&nbsp;&nbsp;&nbsp;注入id：&nbsp;&nbsp;<input type="text" name="nns_import_id"
								id="nns_import_id" style="width:150px;"
								value="<?php echo $_GET['nns_import_id']; ?>"
								>
							 &nbsp;&nbsp;&nbsp;&nbsp;栏目id：&nbsp;&nbsp;<input type="text" name="nns_category_id"
								id="nns_category_id" style="width:150px;"
								value="<?php echo $_GET['nns_category_id']; ?>"
								>
							<br/>
							 &nbsp;&nbsp;&nbsp;&nbsp;操作类型：&nbsp;&nbsp; <select name="nns_action" style="width: 120px;">
									<option value="">全部</option>
									<option value="online"
										<?php if($_GET['nns_action'] == 'online'){ echo "selected";}?>>上线</option>
									<option value="unline"
										<?php if($_GET['nns_action'] == 'unline'){ echo "selected";}?>> 下线</option>
							</select>
							
							 &nbsp;&nbsp;&nbsp;&nbsp;状态：&nbsp;&nbsp; <select name="nns_status" style="width: 120px;">
									<option value="">全部</option>
									<option value="0"
										<?php if($_GET['nns_status'] == '0'){ echo "selected";}?>>下发成功</option>
									<option value="1"
										<?php if($_GET['nns_status'] == '1'){ echo "selected";}?>>下发失败</option>
									<option value="2"
										<?php if($_GET['nns_status'] == '2'){ echo "selected";}?>>反馈成功</option>
									<option value="3"
										<?php if($_GET['nns_status'] == '3'){ echo "selected";}?>>反馈失败</option>
							</select>
							 &nbsp;&nbsp;&nbsp;&nbsp;选择创建时间段：&nbsp;&nbsp;&nbsp;<input name="create_begin_time" id="create_begin_time" type="text"  value="<?php
										if (isset($_GET['day_picker_start']))
											echo $_GET['day_picker_start'];
									?>" style="width:120px;" class="datetimepicker" callback="test" />
										 - <input name="create_end_time" id="create_end_time" type="text"  value="<?php
											if (isset($_GET['create_end_time']))
												echo $_GET['create_end_time'];
										?>" style="width:120px;" class="datetimepicker" callback="test" />
										 &nbsp;&nbsp;<input type="button" id="clear_time" name="clear_time" value="清除时间"/>
							<input type="hidden" value="<?php echo $sp_id; ?>" name="sp_id" />
							<input type="submit" value="查询" id="inquery" name="inquery" />
						</td>
						</tr>
					</tbody>
				</form>
			</table>
		</div>
		<div class="content_table formtable">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th style="width: 50px;"><input name="" type="checkbox" value="">序号</th>
						<th style="width: 120px;">消息ID</th>
						<th style="width: 120px;">队列ID</th>
						<th style="width: 120px;">影片名称</th>
						<th style="width: 100px;">注入ID</th>
						<th style="width: 80px;">栏目ID</th>
						<th style="width: 50px;">操作类型</th>
						<th style="width: 180px;">状态</th>
						<th style="width: 180px;">描述</th>
						<th style="width: 180px;">创建时间</th>
						<th style="width: 180px;">修改时间</th>
						<th style="width: 50px;">操作</th>
					</tr>
				</thead>
				<tbody>
						<?php
						if (!empty($result['data_info']) && is_array($result['data_info']))
						{
							$num = ($page_info['page_num'] - 1) * $page_info['page_size'];
							foreach ($result['data_info'] as $item)
							{
								$num++;
								?>
						<tr>
						<td id="is_delete"><input name="input" type="checkbox"
							value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
						<td><?php echo $item['nns_message_id'];?></td>
						<td><?php echo $item['nns_asset_online_id'];?></td>
						<td><?php echo $item['nns_video_name'];?></td>
						<td><?php echo $item['nns_import_id'];?></td>
						<td><?php echo $item['nns_category_id'];?></td>
						<td><?php if($item['nns_action'] == 'online'){ echo "上线";}else if($item['nns_action'] == 'unline'){ echo "下线";}else{ echo "<font style=\"color:#FF0000\">未知</font>";};?></td>
						<td>
							<?php   if($item['nns_status'] == '0')
									{
										echo "下发成功";
									}
									else if($item['nns_status'] == '1')
									{
										echo "<font style=\"color:#FF0000\">下发失败</font>";
									}
									else if($item['nns_status'] == '2')
									{
										echo "反馈成功";
									}
									else
									{
										echo "<font style=\"color:#FF0000\">反馈失败</font>";
									}
							?>
						</td>
						<td><?php echo $item['nns_reason'];?></td>
						<td><?php echo $item['nns_create_time'];?></td>
						<td><?php echo $item['nns_modify_time'];?></td>

						<td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                        	<?php if($item['nns_status'] == '0' || $item['nns_status'] == '1'){?>
                        		<a href="nncms_content_asset_online_log.php?op=show&type=import&sp_id=<?php echo $sp_id; ?>&ids=<?php echo $item['nns_id'];?>" target="_blank">查看下发内容</a>
                        		<!--<?php if($item['nns_status'] == '1'){?>
                        			<a href="nncms_content_asset_online_log.php?op=retry&sp_id=<?php echo $sp_id; ?>&ids=<?php echo $item['nns_id'];?>">重新下发</a>
                        		<?php }?> -->
                        	<?php }else{?>
                        		<a href="nncms_content_asset_online_log.php?op=show&type=import&sp_id=<?php echo $sp_id; ?>&ids=<?php echo $item['nns_id'];?>" target="_blank">查看下发内容</a>
                        		<a href="nncms_content_asset_online_log.php?op=show&type=callback&sp_id=<?php echo $sp_id; ?>&ids=<?php echo $item['nns_id'];?>" target="_blank">查看反馈内容</a>
                        		<!--
                        		<?php if($item['nns_status'] == '3'){?>
                        			<a href="nncms_content_asset_online_log.php?op=retry&sp_id=<?php echo $sp_id; ?>&ids=<?php echo $item['nns_id'];?>">重新反馈</a>
                        		<?php }?> -->
                        	<?php }?>
                        </td>
					</tr>
						<?php
							}
						}$least_num = $page_info['page_size'] - count($result['data_info']);
						for ($i = 0; $i < $least_num; $i++) {
						?>
						<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
						<?php } ?>
					</tbody>
			</table>
		</div>
			<?php echo $pager->nav();?>
			<!-- 
		<div class="controlbtns">
			<div class="controlbtn allselect">
				<a href="javascript:selectAllCheckBox(true);">全选</a>
			</div>
			<div class="controlbtn cancel">
				<a href="javascript:selectAllCheckBox(false);">取消</a>
			</div>
			<div class="controlbtn move">
				<a href="javascript:msg_retry('send');">下发</a>
			</div>
			<div class="controlbtn move">
				<a href="javascript:msg_retry('callback');">反馈重发</a>
			</div>
			<div class="controlbtn move">
				<a href="javascript:do_audit('pass');">审核通过</a>
			</div>
			<div class="controlbtn move">
				<a href="javascript:do_audit('refuse');">审核不通过</a>
			</div>
			<div class="controlbtn move">
				<a href="javascript:do_delete('delete');">删除</a>
			</div>
			<div class="controlbtn move">
				<a href="javascript:do_delete('recover');">删除恢复</a>
			</div>
			<div style="clear: both;"></div>
		</div>
			 -->
	</div>
</body>
</html>
