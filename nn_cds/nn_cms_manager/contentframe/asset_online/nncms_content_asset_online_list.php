<?php
error_reporting(7);
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/asset_online/asset_online.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/sp/sp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
include $nncms_config_path . "v2/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_core.m_public");
\ns_core\m_load::load("ns_model.epg_import.epg_line_queue");
//导入语言包
if (isset($_SESSION["language_dir"]))
{
	$language_dir = $_SESSION["language_dir"];
}
include_once $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include_once $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool)
{
	Header("Location: ../nncms_content_wrong.php");
	exit();
}
$dc = nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$sp_id = $_REQUEST['sp_id'];

global $g_bk_version_number;
$bk_version_number = $g_bk_version_number == '1' ? true : false;
unset($g_bk_version_number);
if($bk_version_number && isset($_REQUEST['op']) && !empty($_REQUEST['op']))
{
    if ($_GET['op'] == 'retry') //注入到c2队列
    {
        \m_config::$flag_operation = false;
        $log_timer_path = \m_config::return_child_path($nncms_config_path . "v2/ns_timer/delivery/delivery_epg_line.timer.php", 'ns_timer');
        $flag=($_REQUEST['mssage'] == 'send') ? true : false;
        $ids = array_filter(array_unique(explode(',', isset($_REQUEST['ids']) ? $_REQUEST['ids'] : '')));

        if (!is_array($ids) || empty($ids))
        {
            echo "<script>alert('传入查询ID为空');history.go(-1);</script>";
            die;
        }

        //获取上下线指令队列执行任务
        $result_task = nl_asset_online::query_by_ids(\m_config::get_dc(), $ids);

        if ($result_task['ret'] != NS_CDS_SUCCE)
        {
            echo "<script>alert('数据库查询数据失败');history.go(-1);</script>";
            die;
        }
        if (empty($result_task['data_info']) || !is_array($result_task['data_info']))
        {
            echo "<script>alert('数据库查询无数据');history.go(-1);</script>";
            die;
        }
        \ns_core\m_load::load("ns_model.delivery.epg_line_delivery_explain");
        $explain_inst = new \ns_model\delivery\epg_line_delivery_explain($sp_id);
        foreach ($result_task['data_info'] as $task)
        {
            //实例化底层数据基础类
            $epg_line_queue = new ns_model\epg_import\epg_line_queue($sp_id, $task['nns_type']);
            //过滤注入数据
            $epg_line_explain = $epg_line_queue->explain();
            if ($epg_line_explain['ret'] != NS_CDS_SUCCE)
            {
                \m_config::timer_write_log($log_timer_path, var_export($epg_line_explain['reason'], true), $task['nns_type']);
                continue;
            }

            //根据中心同步指令任务获取注入信息
            $message_arr = $epg_line_queue->query_mixed_info(array($task));

            if ($message_arr['ret'] != NS_CDS_SUCCE)
            {
                \m_config::timer_write_log($log_timer_path, var_export($message_arr, true), $task['nns_type']);
                continue;
            }
            if (!isset($message_arr['data_info']) || empty($message_arr['data_info']) || !is_array($message_arr['data_info']))
            {
                \m_config::timer_write_log($log_timer_path, "查询上下线队列数据为空，不再执行上下线指令到epg", $task['nns_type']);
                continue;
            }
            m_config::write_epg_line_execute_log("***********开始执行上下线指令注入到epg：".var_export($message_arr['data_info'][0],true), $sp_id, $task['nns_id']);
            $explain_inst->init($message_arr['data_info'][0]);
            $explain_result = $explain_inst->explain();
            $explain_inst->destroy_init();
            m_config::write_epg_line_execute_log("***********执行上下线指令结果：".var_export($explain_result,true), $sp_id, $task['nns_id']);
        }
        if(!isset($explain_result['reason']) || empty($explain_result['reason']))
        {
            $explain_result['reason'] = $explain_result['ret'] != NS_CDS_SUCCE ? '操作失败' : '操作成功';
        }
        echo "<script>alert('" . $explain_result['reason'] . "');history.go(-1);</script>";
        die;
    }
}
$sp_data = nl_sp::query_by_id($dc,$sp_id);
$sp_data = isset($sp_data['data_info']) ? $sp_data['data_info'] : array();
if(isset($_REQUEST['op']) && !empty($_REQUEST['op']))
{
	if($_REQUEST['op'] == 'retry')
	{
		$arr_id = array_filter(array_unique(explode(',', isset($_REQUEST['ids']) ? $_REQUEST['ids'] : '')));
		if(empty($arr_id) || !is_array($arr_id))
		{
			echo '<script>alert("操作成功");history.go(-1);</script>';die;
		}
		$flag=($_REQUEST['mssage'] == 'send') ? true : false;
		$arr_sp=nl_sp::query_by_id($dc, $sp_id);
		if(!isset($arr_sp['data_info'][0]['nns_config']) || empty($arr_sp['data_info'][0]['nns_config']))
		{
			echo '<script>alert("操作成功");history.go(-1);</script>';die;
		}
		$sp_config = json_decode($arr_sp['data_info']['0']['nns_config'],true);
		if (!isset($sp_config['online_disabled']) || (int)$sp_config['online_disabled'] == 0)
		{
			echo '<script>alert("上下线开关已关闭");history.go(-1);</script>';die;
		}
		$send_way = (isset($sp_config['online_send_way']) && $sp_config['online_send_way'] == '1') ? 'http' : 'ftp';
		$third_category_enabled = (isset($sp_config['third_category_enabled']) && $sp_config['third_category_enabled'] == '1') ? true : false;
		$check_video_statuc_enabled = (isset($sp_config['check_video_statuc_enabled']) && $sp_config['check_video_statuc_enabled'] == '1') ? false : true;
		$asset_unline_model = isset($sp_config['asset_unline_model']) ? $sp_config['asset_unline_model'] : 0;
		#TODO
		if($flag)
		{
			include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/mgtv_init.php';
			include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/' . $sp_id . '/init.php';
			foreach ($arr_id as $val)
			{
				$result=nl_asset_online::query_by_id($dc,$val);
				if(!isset($result['data_info'][0]) || $result['data_info'][0]['nns_audit'] !='1' || (!in_array($result['data_info'][0]['nns_status'], array(1,2,4,5,6,7))))
				{
					continue;
				}
				import_model::import_unline($result['data_info'][0],$send_way,$third_category_enabled,$check_video_statuc_enabled,$sp_config,$asset_unline_model);
			}
		}
		else
		{
			#TODO
		}
		echo '<script>alert("操作成功");history.go(-1);</script>';die;
	}
	else if($_REQUEST['op'] == 'do_audit')
	{
		$arr_id = array_filter(array_unique(explode(',', isset($_REQUEST['ids']) ? $_REQUEST['ids'] : '')));
		if(empty($arr_id) || !is_array($arr_id))
		{
			echo '<script>alert("操作成功");history.go(-1);</script>';die;
		}
		if(!in_array($_REQUEST['nns_audit'], array(0,1)))
		{
			echo '<script>alert("参数错误");history.go(-1);</script>';die;
		}
		$result=nl_asset_online::update($dc,array('nns_audit'=>$_REQUEST['nns_audit']),array('nns_id'=>$arr_id,'nns_status'=>array(1,2,5,6,7)));
		echo '<script>alert("'.$result['reason'].'");history.go(-1);</script>';die;
	}
	else if($_REQUEST['op'] == 'do_delete')
	{
		$arr_id = array_filter(array_unique(explode(',', isset($_REQUEST['ids']) ? $_REQUEST['ids'] : '')));
		if(empty($arr_id) || !is_array($arr_id))
		{
			echo '<script>alert("操作成功");history.go(-1);</script>';die;
		}
		if(!in_array($_REQUEST['nns_delete'], array(0,1)))
		{
			echo '<script>alert("参数错误");history.go(-1);</script>';die;
		}
		$temp_params=array('nns_delete'=>$_REQUEST['nns_delete']);
		if($_REQUEST['nns_delete'] == 0)
		{
			$temp_params['nns_fail_time'] = 0;
		}
		$result=nl_asset_online::update($dc,$temp_params,array('nns_id'=>$arr_id));
		echo '<script>alert("'.$result['reason'].'");history.go(-1);</script>';die;
	}
	else if($_REQUEST['op'] == 'show')
	{
		header("Content-Type:text/xml;charset=utf-8");
		$result=nl_asset_online::query_by_id($dc,$_REQUEST['ids']);
		echo isset($result['data_info'][0]['nns_content_xml']) ? $result['data_info'][0]['nns_content_xml'] : '<?xml version="1.0" encoding="utf-8"?><result>no data</result>';die;
	}
}
$page_info = $params = array ();
$params['where']['nns_org_id'] = $sp_id;
$page_info['page_num'] = isset($_GET['page']) ? $_GET['page'] : 1;
$page_info['page_size'] = isset($_GET['page_size']) ? $_GET['page_size'] : (isset($_COOKIE['page_max_num']) && $_COOKIE['page_max_num'] > 0) ? $_COOKIE['page_max_num'] : 18;
if (isset($_REQUEST['nns_video_name']) && strlen($_REQUEST['nns_video_name']) > 0)
{
	$params['like']['nns_video_name'] = $_REQUEST['nns_video_name'];
}
if (isset($_REQUEST['nns_message_id']) && strlen($_REQUEST['nns_message_id']) > 0)
{
	$params['where']['nns_message_id'] = $_REQUEST['nns_message_id'];
}
if (isset($_REQUEST['nns_import_id']) && strlen($_REQUEST['nns_import_id']) > 0)
{
	$params['where']['nns_import_id'] = $_REQUEST['nns_import_id'];
}
if (isset($_REQUEST['nns_status']) && strlen($_REQUEST['nns_status']) > 0)
{
	$params['where']['nns_status'] = $_REQUEST['nns_status'];
}
if (isset($_REQUEST['nns_audit']) && strlen($_REQUEST['nns_audit']) > 0)
{
	$params['where']['nns_audit'] = $_REQUEST['nns_audit'];
}
if (isset($_REQUEST['nns_action']) && strlen($_REQUEST['nns_action']) > 0)
{
	$params['where']['nns_action'] = $_REQUEST['nns_action'];
}
if (isset($_REQUEST['nns_category_id']) && strlen($_REQUEST['nns_category_id']) > 0)
{
	$params['where']['nns_category_id'] = $_REQUEST['nns_category_id'];
}
if (isset($_REQUEST['create_begin_time']) && strlen($_REQUEST['create_begin_time']) > 0)
{
	$params['where']['create_begin_time'] = $_REQUEST['create_begin_time'];
}
if (isset($_REQUEST['create_end_time']) && strlen($_REQUEST['create_end_time']) > 0)
{
	$params['where']['create_end_time'] = $_REQUEST['create_end_time'];
}
if (isset($_REQUEST['nns_delete']) && strlen($_REQUEST['nns_delete']) > 0)
{
	$params['where']['nns_delete'] = $_REQUEST['nns_delete'];
}
$url = 'nncms_content_asset_online_list.php?flag_url=1';
foreach ($_REQUEST as $k => $v)
{
	if ($k != 'flag_url' && $k != 'page')
	{
		$url .= '&' . $k . '=' . $v;
	}
}

$result = nl_asset_online::query($dc, $params, $page_info);
$pager = new nl_pager($result['page_info']['total_count'], $page_info['page_size'], $page_info['page_num'], $url);
//if ($result['ret'] != 0)
//{
//	echo '<script>alert("' . $result['reason'] . '");</script>';
//}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
<link href="../../css/comm.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/rate.js"></script>
<script language="javascript" src="../../js/image_loaded_func.js"></script>
<script language="javascript" src="../../js/cms_datepicker.js"></script>
<script language="javascript" src="../../js/alertbox.js"></script>
<script language="javascript">
			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				})
				return BoxKey;
			}
			function msg_retry(mes)
			{
				var r=confirm("是否进行该操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "nncms_content_asset_online_list.php?op=retry&mssage="+mes+"&sp_id=<?php echo $sp_id;?>&ids="+ids;
						window.location.href = url;
					}
				}
			}
			function do_audit(mes)
			{
				var r=confirm("是否进行该操作");
				var nns_audit;
				nns_audit = (mes == 'pass') ? 1 : 0;
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "nncms_content_asset_online_list.php?op=do_audit&sp_id=<?php echo $sp_id;?>&nns_audit="+nns_audit+"&ids="+ids;
						window.location.href = url;
					}
				}
			}
			function do_delete(mes)
			{
				var r=confirm("是否进行该操作");
				var nns_audit;
				nns_delete = (mes == 'recover') ? 0 : 1;
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "nncms_content_asset_online_list.php?op=do_delete&nns_delete="+nns_delete+"&sp_id=<?php echo $sp_id;?>&nns_audit="+nns_audit+"&ids="+ids;
						window.location.href = url;
					}
				}
			}
			$(document).ready(function() {
				$('#clear_time').click(function(){
					$('#create_begin_time').val('');
					$('#create_end_time').val('');

				});
				window.parent.now_frame_url = window.location.href;
			});

			$(document).ready(function() {
				$('#clear_time_modify').click(function(){
					$('#nns_begin_time').val('');
					$('#nns_end_time').val('');

				});
				window.parent.now_frame_url = window.location.href;
			});
		</script>
</head>
<body>
	<div class="content">
		<div class="content_position">上下线消息列表</div>
		<div class="content_table">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<form action="nncms_content_asset_online_list.php" method="get">
					<tbody>
						<tr>
						<td>
							 &nbsp;&nbsp;&nbsp;&nbsp;消息ID：&nbsp;&nbsp;<input type="text" name="nns_message_id"
								id="nns_message_id" style="width:150px;"
								value="<?php echo $_GET['nns_message_id']; ?>"
								>
							 &nbsp;&nbsp;&nbsp;&nbsp;影片名称：&nbsp;&nbsp;<input type="text" name="nns_video_name"
								id="nns_video_name" style="width:150px;"
								value="<?php echo $_GET['nns_video_name']; ?>"
								>
							 &nbsp;&nbsp;&nbsp;&nbsp;注入id：&nbsp;&nbsp;<input type="text" name="nns_import_id"
								id="nns_import_id" style="width:150px;"
								value="<?php echo $_GET['nns_import_id']; ?>"
								>
							 &nbsp;&nbsp;&nbsp;&nbsp;栏目id：&nbsp;&nbsp;<input type="text" name="nns_category_id"
								id="nns_category_id" style="width:150px;"
								value="<?php echo $_GET['nns_category_id']; ?>"
								>
							
							 &nbsp;&nbsp;&nbsp;&nbsp;操作类型：&nbsp;&nbsp; <select name="nns_action" style="width: 120px;">
									<option value="">全部</option>
									<option value="online"
										<?php if($_GET['nns_action'] == 'online'){ echo "selected";}?>>上线</option>
									<option value="unline"
										<?php if($_GET['nns_action'] == 'unline'){ echo "selected";}?>> 下线</option>
							</select>
							<br/>
							 &nbsp;&nbsp;&nbsp;&nbsp;状态：&nbsp;&nbsp; <select name="nns_status" style="width: 120px;">
									<option value="">全部</option>
									<option value="0"
										<?php if($_GET['nns_status'] == '0'){ echo "selected";}?>>成功</option>
									<option value="1"
										<?php if($_GET['nns_status'] == '1'){ echo "selected";}?>>等待注入</option>
									<option value="2"
										<?php if($_GET['nns_status'] == '2'){ echo "selected";}?>>注入失败</option>
									<option value="3"
										<?php if($_GET['nns_status'] == '3'){ echo "selected";}?>>等待反馈</option>
									<option value="4"
										<?php if($_GET['nns_status'] == '4'){ echo "selected";}?>>反馈失败</option>
									<option value="5"
										<?php if($_GET['nns_status'] == '5'){ echo "selected";}?>>主媒资不是最终状态</option>
									<option value="6"
										<?php if($_GET['nns_status'] == '6'){ echo "selected";}?>>分集不是最终状态</option>
									<option value="7"
										<?php if($_GET['nns_status'] == '7'){ echo "selected";}?>>片源不是最终状态</option>	
							</select>
							 &nbsp;&nbsp;&nbsp;&nbsp;审核：&nbsp;&nbsp; <select name="nns_audit" style="width: 120px;">
									<option value="">全部</option>
									<option value="0"
										<?php if($_GET['nns_audit'] == '0'){ echo "selected";}?>>审核未通过</option>
									<option value="1"
										<?php if($_GET['nns_audit'] == '1'){ echo "selected";}?>>审核通过</option>
							</select>
							 &nbsp;&nbsp;&nbsp;&nbsp;删除状态：&nbsp;&nbsp; <select name="nns_delete" style="width: 120px;">
									<option value="">全部</option>
									<option value="0"
										<?php if($_GET['nns_delete'] == '0'){ echo "selected";}?>>未删除</option>
									<option value="1"
										<?php if($_GET['nns_delete'] == '1'){ echo "selected";}?>>已删除</option>
							</select>
							 &nbsp;&nbsp;&nbsp;&nbsp;选择创建时间段：&nbsp;&nbsp;&nbsp;<input name="create_begin_time" id="create_begin_time" type="text"  value="<?php
										if (isset($_GET['day_picker_start']))
											echo $_GET['day_picker_start'];
									?>" style="width:120px;" class="datetimepicker" callback="test" />
										 - <input name="create_end_time" id="create_end_time" type="text"  value="<?php
											if (isset($_GET['create_end_time']))
												echo $_GET['create_end_time'];
										?>" style="width:120px;" class="datetimepicker" callback="test" />
										 &nbsp;&nbsp;<input type="button" id="clear_time" name="clear_time" value="清除时间"/>
							<input type="hidden" value="<?php echo $sp_id; ?>" name="sp_id" />
							<input type="submit" value="查询" id="inquery" name="inquery" />
						</td>
						</tr>
					</tbody>
				</form>
			</table>
		</div>
		<div class="content_table formtable">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th style="width: 50px;"><input name="" type="checkbox" value="">序号</th>
						<th style="width: 120px;">消息ID</th>
						<th style="width: 120px;">影片名称</th>
						<th style="width: 100px;">注入ID</th>
						<th style="width: 80px;">栏目ID</th>
						<th style="width: 50px;">媒资序号</th>
						<th style="width: 50px;">操作类型</th>
						<th style="width: 40px;">CP ID</th>
						<th style="width: 180px;">操作状态</th>
						<th style="width: 180px;">审核状态</th>
						<th style="width: 30px;">失败次数</th>
						<th style="width: 50px;">删除状态</th>
						<th style="width: 180px;">创建时间</th>
						<th style="width: 180px;">修改时间</th>
						<th style="width: 50px;">操作</th>
					</tr>
				</thead>
				<tbody>
						<?php
						if (!empty($result['data_info']) && is_array($result['data_info']))
						{
							$num = ($page_info['page_num'] - 1) * $page_info['page_size'];
							foreach ($result['data_info'] as $item)
							{
								$num++;
								?>
						<tr>
						<td id="is_delete"><input name="input" type="checkbox"
							value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
						<td><?php echo $item['nns_message_id'];?></td>
						<td><?php echo $item['nns_video_name'];?></td>
						<td><?php echo $item['nns_import_id'];?></td>
						<td><?php echo $item['nns_category_id'];?></td>
						<td><?php echo $item['nns_order'];?></td>
						<td><?php if($item['nns_action'] == 'online'){ echo "上线";}else if($item['nns_action'] == 'unline'){ echo "下线";}else{ echo "<font style=\"color:#FF0000\">未知</font>";};?></td>
						<td><?php echo $item['nns_cp_id'];?></td>
						<td>
							<?php   if($item['nns_status'] == '0')
									{
										echo "成功";
									}
									else if($item['nns_status'] == '1')
									{
										echo "等待注入";
									}
									else if($item['nns_status'] == '2')
									{
										echo "<font style=\"color:#FF0000\">注入失败</font>";
									}
									else if($item['nns_status'] == '3')
									{
										echo "等待反馈";
									}
									else if($item['nns_status'] == '4')
									{
										echo "<font style=\"color:#FF0000\">反馈失败</font>";
									}
									else if($item['nns_status'] == '5')
									{
										echo "主媒资没有最终状态";
									}
									else if($item['nns_status'] == '6')
									{
										echo "分集没有最终状态";
									}
									else if($item['nns_status'] == '7')
									{
										echo "片源没有最终状态";
									}
							?>
						</td>
						<td>
							<?php   if($item['nns_audit'] == '0')
									{
										echo "审核未通过";
									}
									else
									{
										echo "审核通过";
									}
							?>
						</td>
						<td><?php echo $item['nns_fail_time'];?></td>
						<td><?php switch ($item['nns_delete'])
                        	{
                        		case '1': echo "<font style=\"color:#FF0000\">已删除</font>";break;
                        		case '0': echo "<font style=\"color:#00CC00\">未删除</font>";break;
                        		default : echo '';
                        	}?></td>
						<td><?php echo $item['nns_create_time'];?></td>
						<td><?php echo $item['nns_modify_time'];?></td>

						<td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                        	<?php if($item['nns_status'] == '1' || $item['nns_status'] == '2' || $item['nns_status'] == '5' || $item['nns_status'] == '6' || $item['nns_status'] == '7'){?>
                        		<a href="nncms_content_asset_online_list.php?op=retry&mssage=send&sp_id=<?php echo $sp_id; ?>&ids=<?php echo $item['nns_id'];?>">下发</a>
                        	<?php }else if($item['nns_status'] == '4'){?>
                        		<a href="nncms_content_asset_online_list.php?op=retry&mssage=callback&sp_id=<?php echo $sp_id; ?>&ids=<?php echo $item['nns_id'];?>">反馈重发</a>
                        	<?php }?>
                        	<?php if(in_array($item['nns_status'], array(1,2,5,6,7))){?>
                        	<?php if($item['nns_audit'] == '0'){?>
                        		<a href="nncms_content_asset_online_list.php?op=do_audit&nns_audit=1&sp_id=<?php echo $sp_id; ?>&ids=<?php echo $item['nns_id'];?>">审核通过</a>
                        	<?php }else{?>
                        		<a href="nncms_content_asset_online_list.php?op=do_audit&nns_audit=0&sp_id=<?php echo $sp_id; ?>&ids=<?php echo $item['nns_id'];?>">审核不通过</a>
                        	<?php }?>
                        	<?php }?>
                        	<?php if($item['nns_delete'] == '0'){?>
                        		<a href="nncms_content_asset_online_list.php?op=do_delete&nns_delete=1&sp_id=<?php echo $sp_id; ?>&ids=<?php echo $item['nns_id'];?>">删除</a>
                        	<?php }else{?>
                        		<a href="nncms_content_asset_online_list.php?op=do_delete&nns_delete=0&sp_id=<?php echo $sp_id; ?>&ids=<?php echo $item['nns_id'];?>">删除恢复</a>
                        	<?php }?>
                        </td>
					</tr>
						<?php
							}
						}$least_num = $page_info['page_size'] - count($result['data_info']);
						for ($i = 0; $i < $least_num; $i++) {
						?>
						<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
						<?php } ?>
					</tbody>
			</table>
		</div>
			<?php echo $pager->nav();?>
		<div class="controlbtns">
			<div class="controlbtn allselect">
				<a href="javascript:selectAllCheckBox(true);">全选</a>
			</div>
			<div class="controlbtn cancel">
				<a href="javascript:selectAllCheckBox(false);">取消</a>
			</div>
			<div class="controlbtn move">
				<a href="javascript:msg_retry('send');">下发</a>
			</div>
			<div class="controlbtn move">
				<a href="javascript:msg_retry('callback');">反馈重发</a>
			</div>
			<div class="controlbtn move">
				<a href="javascript:do_audit('pass');">审核通过</a>
			</div>
			<div class="controlbtn move">
				<a href="javascript:do_audit('refuse');">审核不通过</a>
			</div>
			<div class="controlbtn move">
				<a href="javascript:do_delete('delete');">删除</a>
			</div>
			<div class="controlbtn move">
				<a href="javascript:do_delete('recover');">删除恢复</a>
			</div>
			<div style="clear: both;"></div>
		</div>

	</div>
</body>
</html>
