<?php 
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) 
{
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -5);
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
		"db_policy" => NL_DB_WRITE,
		"cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));

$sp_id = $_REQUEST['nns_sp_id'];

/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/file_encode/file_encode.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/transcode/transcode_load_balance.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';

$result_cp = nl_cp::query_all($dc);
if($result_cp['ret'] != 0)
{
    echo '<script>alert("'.$result_cp['reason'].'");history.go(-1);</script>';die;
}
$result_cp = isset($result_cp['data_info']) ? $result_cp['data_info'] : null;
$arr_sp_info = $arr_cp_info = array();
if(is_array($result_cp) && !empty($result_cp))
{
    foreach ($result_cp as $val)
    {
        $arr_cp_info[$val['nns_id']] = $val['nns_name'];
    }
}


$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : (!empty($_COOKIE["page_max_num"]) ?  $_COOKIE["page_max_num"] : 18);
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) 
{
	$page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit_array = array(
		'page_size'=>$page_size,
		'page_num'=> $page_num,
);
$filter_array['where']['nns_sp_id'] = $sp_id;
if (isset($_REQUEST['nns_in_media_id']) && (strlen($_REQUEST['nns_in_media_id']) >0))
{
    $filter_array['where']['nns_in_media_id'] = $_REQUEST['nns_in_media_id'];
}
if (isset($_REQUEST['nns_op_id']) && (strlen($_REQUEST['nns_op_id']) >0))
{
    $filter_array['where']['nns_op_id'] = $_REQUEST['nns_op_id'];
}
if (isset($_REQUEST['day_picker_start']) && (strlen($_REQUEST['day_picker_start']) >0))
{
    $filter_array['where']['nns_create_time_begin'] = $_REQUEST['day_picker_start'];
}
if (isset($_REQUEST['day_picker_end']) && (strlen($_REQUEST['day_picker_end']) >0))
{
    $filter_array['where']['nns_create_time_end'] = $_REQUEST['day_picker_end'];
}
if (isset($_REQUEST['nns_state']) && (strlen($_REQUEST['nns_state']) >0))
{
    $filter_array['where']['nns_state'] = $_REQUEST['nns_state'];
}
if (isset($_REQUEST['nns_loadbalance_id']) && (strlen($_REQUEST['nns_loadbalance_id']) >0))
{
    $filter_array['where']['nns_loadbalance_id'] = $_REQUEST['nns_loadbalance_id'];
}
if (isset($_REQUEST['nns_type']) && (strlen($_REQUEST['nns_type']) >0))
{
    $filter_array['where']['nns_type'] = $_REQUEST['nns_type'];
}
if (isset($_REQUEST['nns_cp_id']) && (strlen($_REQUEST['nns_cp_id']) >0))
{
    $filter_array['where']['nns_cp_id'] = $_REQUEST['nns_cp_id'];
}
if (isset($_REQUEST['nns_media_type']) && (strlen($_REQUEST['nns_media_type']) >0))
{
    $filter_array['where']['nns_media_type'] = $_REQUEST['nns_media_type'];
}
if (isset($_REQUEST['nns_queue_name']) && (strlen($_REQUEST['nns_queue_name']) >0))
{
    $filter_array['like']['nns_queue_name'] = $_REQUEST['nns_queue_name'];
}

$result_data = nl_file_encode::query($dc,$filter_array,$limit_array);

if($result_data['ret'] != 0)
{
	echo '<script>alert("'.$result_data['reason'].'");history.go(-1);</script>';die;
}
$result_load_balance = nl_transcode_load_balance::query_sp_info_v2($dc, $sp_id);
$arr_load_balance = null;
if(isset($result_load_balance['data_info']) && is_array($result_load_balance['data_info']) && !empty($result_load_balance['data_info']))
{
    foreach ($result_load_balance['data_info'] as $load_balance)
    {
        $arr_load_balance[$load_balance['nns_id']] = array(
            'name'=>$load_balance['nns_name'],
            'api'=>$load_balance['nns_api_url'],
        );
    }
}
$vod_total_num = isset($result_data['page_info']['total_count']) ? $result_data['page_info']['total_count'] : 0;
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) {
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;

$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link href="../../css/public.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
		<script language="javascript">
    		$(document).ready(function() {
    			$('#clear_time').click(function(){
    				$('#day_picker_start').val('');
    				$('#day_picker_end').val('');
    
    			});
    			window.parent.now_frame_url = window.location.href;
    		});
			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
                setCookie("page_max_num",num);
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				})
				return BoxKey;
			}

			function file_encode(){
				var r=confirm("是否进行批量转码操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "<?php echo $base_file_name_base;?>_control.php?action=file_encode&nns_id="+ids;
						window.location.href = url;
					}
				}
			}

			function delete_id(){
				var r=confirm("是否进行批量删除操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "<?php echo $base_file_name_base;?>_control.php?action=delete&nns_id="+ids;
						window.location.href = url;
					}
				}
			}
			
			function sort(type){
				var r=confirm("是否进行批量排序操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "<?php echo $base_file_name_base;?>_control.php?action=sort&d="+type+"&nns_id="+ids;
						window.location.href = url;
					}
				}
			}

			function again_reset(){
				var r=confirm("是否进行批量重置失败次数操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "<?php echo $base_file_name_base;?>_control.php?action=again_reset&nns_id="+ids;
						window.location.href = url;
					}
				}
			}
		</script>
	</head>
	<body>
		<div class="content">
		    <!-- 栏目导航描述div -->
			<div class="content_position">转码列表</div>
			<!-- 表单查询div -->
			<div class="content_table">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<form action="<?php echo $base_file_name;?>" method="get">
					    <input type="hidden" name="view_list_max_num" id="view_list_max_num" value="<?php echo $g_manager_list_max_num; ?>" style="width:150px;">
					    <input type="hidden" name="nns_sp_id" id="nns_sp_id" value="<?php echo $sp_id; ?>" style="width:150px;">
						<tbody>
							<tr>
								<td>
									&nbsp;&nbsp;CP名称:
                                        <select name='nns_cp_id' style="width:150px;">
                                            <option value=''>全部</option>
                                            <?php foreach ($arr_cp_info as $cp_key=>$cp_val){?>
                                            <option value="<?php echo $cp_key;?>" <?php if(isset($_GET['nns_cp_id']) && $_GET['nns_cp_id'] == $cp_key){ echo 'selected="selected"';}?>><?php echo $cp_key.' / '.$cp_val;?></option>
                                            <?php }?>
                                        </select>
                                    &nbsp;&nbsp;负载服务器:
                                        <select name='nns_loadbalance_id' style="width:150px;">
                                            <option value=''>全部</option>
                                            <?php foreach ($arr_load_balance as $cp_key=>$cp_val){?>
                                            <option value="<?php echo $cp_key;?>" <?php if(isset($_GET['nns_loadbalance_id']) && $_GET['nns_loadbalance_id'] == $cp_key){ echo 'selected="selected"';}?>><?php echo $cp_val['name'];?></option>
                                            <?php }?>
                                        </select>
                                    &nbsp;&nbsp;状态:
                                        <select name='nns_state' style="width:150px;">
                                            <option value=''>全部</option>
                                            <option value="0" <?php if(isset($_GET['nns_state']) && $_GET['nns_state'] == '0'){ echo 'selected="selected"';}?>>等待转码</option>
                                            <option value="1" <?php if(isset($_GET['nns_state']) && $_GET['nns_state'] == '1'){ echo 'selected="selected"';}?>>正在转码</option>
                                            <option value="2" <?php if(isset($_GET['nns_state']) && $_GET['nns_state'] == '2'){ echo 'selected="selected"';}?>>转码失败</option>
                                            <option value="3" <?php if(isset($_GET['nns_state']) && $_GET['nns_state'] == '3'){ echo 'selected="selected"';}?>>转码成功</option>
                                            <option value="4" <?php if(isset($_GET['nns_state']) && $_GET['nns_state'] == '4'){ echo 'selected="selected"';}?>>转码暂停</option>
                                            <option value="5" <?php if(isset($_GET['nns_state']) && $_GET['nns_state'] == '5'){ echo 'selected="selected"';}?>>失效队列</option>
                                        </select>
                                    &nbsp;&nbsp;OP QUEUE ID:
										<input type="text" name="nns_op_id" id="nns_op_id" value="<?php echo $_GET['nns_op_id']; ?>" style="width:150px;">
                                    &nbsp;&nbsp;队列名称:
										<input type="text" name="nns_queue_name" id="nns_queue_name" value="<?php echo $_GET['nns_queue_name']; ?>" style="width:150px;">
                                    &nbsp;&nbsp;原始片源ID:
										<input type="text" name="nns_in_media_id" id="nns_in_media_id" value="<?php echo $_GET['nns_in_media_id']; ?>" style="width:150px;">
									<br/>
									&nbsp;&nbsp;转码类型:
                                        <select name='nns_type' style="width:150px;">
                                            <option value=''>全部</option>
                                            <option value="0" <?php if(isset($_GET['nns_type']) && $_GET['nns_type'] == '0'){ echo 'selected="selected"';}?>>点播转码</option>
                                            <option value="1" <?php if(isset($_GET['nns_type']) && $_GET['nns_type'] == '1'){ echo 'selected="selected"';}?>>直播转码</option>
                                            <option value="2" <?php if(isset($_GET['nns_type']) && $_GET['nns_type'] == '2'){ echo 'selected="selected"';}?>>回看转点播</option>
                                        </select>
                                    &nbsp;&nbsp;原始片源类型:
                                        <select name='nns_media_type' style="width:150px;">
                                            <option value=''>全部</option>
                                            <option value="0" <?php if(isset($_GET['nns_media_type']) && $_GET['nns_media_type'] == '0'){ echo 'selected="selected"';}?>>点播</option>
                                            <option value="1" <?php if(isset($_GET['nns_media_type']) && $_GET['nns_media_type'] == '1'){ echo 'selected="selected"';}?>>直播</option>
                                        </select>
                                    &nbsp;&nbsp;选择时间段：&nbsp;&nbsp;&nbsp;<input name="day_picker_start" id="day_picker_start" type="text"  value="<?php
										if (isset($_GET['day_picker_start']))
											echo $_GET['day_picker_start'];
									?>" style="width:120px;" class="datetimepicker" callback="test" />
										 - <input name="day_picker_end" id="day_picker_end" type="text"  value="<?php
											if (isset($_GET['day_picker_end']))
												echo $_GET['day_picker_end'];
										?>" style="width:120px;" class="datetimepicker" callback="test" />
										 &nbsp;&nbsp;<input type="button" id="clear_time" name="clear_time" value="清除时间"/>
									&nbsp;&nbsp;<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  />										
								</td>
							</tr>
						</tbody>
					</form>
				</table>
			</div>			
			<!-- 内容列表 DIV -->
			<div class="content_table formtable">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th width='40px'><input name="" type="checkbox" value="">序号</th>
							<th>队列名称</th>
							<th>状态</th>
							<th>执行次数</th>
							<th>QUEUE ID</th>
							<th>原始片源ID</th>
							<th>原始片源类型</th>
							<th>转码类型</th>
							<th>权重</th>
							<th>CP</th>
							<th>负载均衡服务器</th>
							<th>失败次数</th>
							<th>修改时间</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(is_array($result_data['data_info']) && !empty($result_data['data_info']))
						{
							$num = ($page_num - 1) * $page_size;
							foreach ($result_data['data_info'] as $item) 
							{
								$num++;
						?>
						      <tr>
						          <td><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
						          <td><a href="nncms_file_encode_log_list.php?nns_file_encode_id=<?php echo $item["nns_id"];?>"><?php echo $item['nns_queue_name']; ?></a></td>
						          <td><?php 
						              switch ($item['nns_state'])
						              {
						                  case '0':
						                      echo "等待转码";
						                      break;
						                  case '1':
						                      echo "正在转码";
						                      break;
						                  case '2':
						                      echo "转码失败";
						                      break;
						                  case '3':
						                      echo "转码成功";
						                      break;
						                  case '4':
						                      echo "转码暂停";
						                      break;
						                  case '5':
						                      echo "转码取消";
						                      break;
						                  default:
						                      echo "未知状态";
						              }
						          ?></td>
						          <td><?php echo $item['nns_again']; ?></td>
						          <td><?php echo $item['nns_op_id']; ?></td>
						          <td><?php echo $item['nns_in_media_id']; ?></td>
						          <td><?php 
						              switch ($item['nns_media_type'])
						              {
						                  case '0':
						                      echo "点播";
						                      break;
						                  case '1':
						                      echo "直播";
						                      break;
						                  default:
						                      echo "未知类型";
						              }
						          ?></td>
						          <td><?php 
						              switch ($item['nns_type'])
						              {
						                  case '0':
						                      echo "点播转码";
						                      break;
						                  case '1':
						                      echo "直播转码";
						                      break;
					                      case '2':
					                          echo "回看转点播";
					                          break;
						                  default:
						                      echo "未知类型";
						              }
						          ?></td>
						          <td><?php echo $item['nns_priority'];?></td>
						          <td><?php echo $item['nns_cp_id']; ?>/<?php echo isset($arr_cp_info[$item['nns_cp_id']]) ? $arr_cp_info[$item['nns_cp_id']] : '<font color="red">未知</font>'; ?></td>
						          
							      <th><?php if(in_array($item['nns_state'], array('1','2','3'))){ if(strlen($item['nns_loadbalance_id'])<1){ 
							          echo "未走负载均衡";}else{ if(isset($arr_load_balance[$item['nns_loadbalance_id']])){ echo "负载服务器：{$arr_load_balance[$item['nns_loadbalance_id']]['name']}<hr/>接口地址：{$arr_load_balance[$item['nns_loadbalance_id']]['api']}";}else {echo "<font color='red'>负载均衡服务器不存在</font>";}} }?></th>
							      
							      
							      
						          <td><?php echo $item['nns_again'];?></td>
						          <td><?php echo $item['nns_modify_time']; ?></td>
						          <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                                	<a href="nncms_file_encode_log_list.php?nns_file_encode_id=<?php echo $item["nns_id"];?>">查看日志</a>
                                	
                                	<a href="<?php echo $base_file_name_base;?>_control.php?action=again_reset&nns_id=<?php echo $item["nns_id"];?>">重置失败次数</a>
            						<a href="<?php echo $base_file_name_base;?>_control.php?action=sort&d=top&nns_id=<?php echo $item['nns_id']?><?php echo $nns_url . $search_url; ?>">置顶</a>
            						<a href="<?php echo $base_file_name_base;?>_control.php?action=sort&d=up&nns_id=<?php echo $item['nns_id']?>&nns_priority=<?php echo $item['nns_priority'];?>">向上</a>
            						<a href="<?php echo $base_file_name_base;?>_control.php?action=sort&d=down&nns_id=<?php echo $item['nns_id']?>&nns_priority=<?php echo $item['nns_priority'];?>">向下</a>
            						<a href="<?php echo $base_file_name_base;?>_control.php?action=sort&d=button&nns_id=<?php echo $item['nns_id']?><?php echo $nns_url . $search_url; ?>">置底</a>
                                  </td>
                        </tr>
						<?php
						     }
						}
						$least_num = $g_manager_list_max_num - count($result_data['data_info']);
						for ($i = 0; $i < $least_num; $i++) 
						{
						?>
						  <tr>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						  </tr>
						<?php 
						}
						?>
					</tbody>
				</table>
			</div>
		    <!-- 分页DIV -->
		    <div class="pagecontrol">
		             共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                <?php if ($currentpage > 1) { ?>
                        <a href="<?php $base_file_name; ?>?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php $base_file_name; ?>?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } else { ?>
                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } if ($currentpage < $vod_pages) { ?>
                        <a href="<?php $base_file_name; ?>?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php $base_file_name; ?>?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } else { ?>
                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } ?>
    
                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                <a href="javascript:go_page_num('<?php $base_file_name; ?>?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                <input name="nns_list_max_num" id="nns_list_max_num" type="text" value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>" onclick="refresh_vod_page();"/>&nbsp;&nbsp;
            </div>    
            <div class="controlbtns">
                <div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
                <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
                <div class="controlbtn add"><a href="javascript:file_encode();">转码</a></div>
		        <div class="controlbtn move"><a href="javascript:sort('top');">批量置顶</a></div>
                <div class="controlbtn move"><a href="javascript:sort('button');">批量置底</a></div>
                <div class="controlbtn add"><a href="javascript:again_reset();">失败重置</a></div>
                <div class="controlbtn delete"><a href="javascript:delete_id();">删除</a></div>
                <div style="clear:both;">
            </div>
        </div>    
                        
		</div>
	</body>
</html>
