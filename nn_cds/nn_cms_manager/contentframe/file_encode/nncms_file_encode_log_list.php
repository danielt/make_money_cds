<?php 
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) 
{
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -5);
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
		"db_policy" => NL_DB_WRITE,
		"cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));

$nns_file_encode_id = $_REQUEST['nns_file_encode_id'];



/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/file_encode/file_encode.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/file_encode/file_encode_log.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';

$result_cp = nl_cp::query_all($dc);
if($result_cp['ret'] != 0)
{
    echo '<script>alert("'.$result_cp['reason'].'");history.go(-1);</script>';die;
}
$result_cp = isset($result_cp['data_info']) ? $result_cp['data_info'] : null;
$arr_sp_info = $arr_cp_info = array();
if(is_array($result_cp) && !empty($result_cp))
{
    foreach ($result_cp as $val)
    {
        $arr_cp_info[$val['nns_id']] = $val['nns_name'];
    }
}
$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : (!empty($_COOKIE["page_max_num"]) ?  $_COOKIE["page_max_num"] : 18);
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) 
{
	$page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit_array = array(
		'page_size'=>$page_size,
		'page_num'=> $page_num,
);
$filter_array['where']['nns_file_encode_id'] = $nns_file_encode_id;

$result_data = nl_file_encode_log::query($dc,$filter_array,$limit_array);
$result_encode_data = nl_file_encode::query_by_id($dc,$nns_file_encode_id);
$result_encode_data = isset($result_encode_data['data_info']) ? $result_encode_data['data_info'] : null;

if(isset($result_encode_data['nns_media_type']) && $result_encode_data['nns_media_type'] == '0')
{
    include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/vod_media/vod_media.class.php';
    $result_src_media = nl_vod_media_v2::query_by_id($dc, $result_encode_data['nns_in_media_id']);
    $result_src_media = isset($result_src_media['data_info']) ? $result_src_media['data_info'] : null;
    $result_src_media['nns_video_id'] = $result_src_media['nns_vod_id'];
    $result_src_media['nns_video_index_id'] = $result_src_media['nns_vod_index_id'];
}
else if(isset($result_encode_data['nns_media_type']) && $result_encode_data['nns_media_type'] == '1')
{
    include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/live/live_media.class.php';
    $result_src_media = nl_live_media::query_by_id($dc, $result_encode_data['nns_in_media_id']);
    $result_src_media['nns_video_id'] = $result_src_media['nns_live_id'];
    $result_src_media['nns_video_index_id'] = $result_src_media['nns_live_index_id'];
}
$result_encode_data['nns_file_url'] = strlen($result_encode_data['nns_file_url']) >0 ? json_decode($result_encode_data['nns_file_url'],true) : null;

if($result_data['ret'] != 0)
{
	echo '<script>alert("'.$result_data['reason'].'");history.go(-1);</script>';die;
}

$vod_total_num = isset($result_data['page_info']['total_count']) ? $result_data['page_info']['total_count'] : 0;
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) {
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;

$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);

?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link href="../../css/public.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
		<script language="javascript">
    		$(document).ready(function() {
    			$('#clear_time').click(function(){
    				$('#day_picker_start').val('');
    				$('#day_picker_end').val('');
    
    			});
    			window.parent.now_frame_url = window.location.href;
    		});
			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				})
				return BoxKey;
			}
			function delete_id(){
				var r=confirm("是否进行批量删除操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "<?php echo $base_file_name_base;?>_control.php?action=delete&nns_id="+ids;
						window.location.href = url;
					}
				}
			}
		</script>
	</head>
	<body>
		<div class="content">
		    <!-- 栏目导航描述div -->
			<div class="content_position">转码日志列表</div>
			<div class="content_table">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tbody>
							<tr class="display_list">
								<td class="rightstyle2a">任务ID：</td>
								<td class="rightstyle3"><?php echo $result_encode_data['nns_id'];?></td>
								<td class="rightstyle2a">队列名称：</td>
								<td class="rightstyle3"><?php echo $result_encode_data['nns_queue_name'];?></td>
							</tr>
							<tr class="display_list">
								<td class="rightstyle2a">原始片源类型：</td>
								<td class="rightstyle3"><?php 
								      switch ($result_encode_data['nns_media_type'])
						              {
						                  case '0':
						                      echo "点播";
						                      break;
						                  case '1':
						                      echo "直播";
						                      break;
						                  default:
						                      echo "未知类型";
						              }?></td>
						        <td class="rightstyle2a">转码类型：</td>
								<td class="rightstyle3"><?php 
								      switch ($result_encode_data['nns_type'])
						              {
						                  case '0':
						                      echo "点播转码";
						                      break;
						                  case '1':
						                      echo "直播转码";
						                      break;
					                      case '2':
					                          echo "回看转点播";
					                          break;
						                  default:
						                      echo "未知类型";
						              }?></td>
							</tr>
							<tr class="display_list">
								<td class="rightstyle2a">主媒资ID：</td>
								<td class="rightstyle3"><?php echo $result_src_media['nns_video_id'];?></td>
								<td class="rightstyle2a">分集ID：</td>
								<td class="rightstyle3"><?php echo $result_src_media['nns_video_index_id'];?></td>
							</tr>
							<tr class="display_list">
								<td class="rightstyle2a">原始片源ID：</td>
								<td class="rightstyle3"><?php echo $result_encode_data['nns_in_media_id'];?></td>
								<td class="rightstyle2a">转码后片源ID：</td>
								<td class="rightstyle3"><?php echo $result_encode_data['nns_out_media_id'];?></td>
							</tr>
							<tr class="display_list">
								<td class="rightstyle2a">OP ID：</td>
								<td class="rightstyle3"><?php echo $result_encode_data['nns_op_id'];?></td>
								<td class="rightstyle2a">CP名称：</td>
								<td class="rightstyle3"><?php echo $result_encode_data['nns_cp_id']; ?>/<?php echo isset($arr_cp_info[$result_encode_data['nns_cp_id']]) ? $arr_cp_info[$result_encode_data['nns_cp_id']] : '<font color="red">未知</font>'; ?></td>
							</tr>
							<tr class="display_list">
								<td class="rightstyle2a">状态：</td>
								<td class="rightstyle3"><?php switch ($result_encode_data['nns_state'])
						              {
						                  case '0':
						                      echo "等待转码";
						                      break;
						                  case '1':
						                      echo "正在转码";
						                      break;
						                  case '2':
						                      echo "转码失败";
						                      break;
						                  case '3':
						                      echo "转码成功";
						                      break;
						                  case '4':
						                      echo "转码暂停";
						                      break;
						                  case '5':
						                      echo "转码取消";
						                      break;
						                  default:
						                      echo "未知状态";
						              }?></td>
						         <td class="rightstyle2a">权重：</td>
								 <td class="rightstyle3"><?php echo $result_encode_data['nns_priority'];?></td>
							</tr>
							<tr>
								<td class="rightstyle2a">创建时间：</td>
								<td class="rightstyle3"><?php echo $result_encode_data['nns_create_time'];?></td>	
								<td class="rightstyle2a">修改时间：</td>
								<td class="rightstyle3"><?php echo $result_encode_data['nns_modify_time'];?></td>
							</tr>	
							<tr>
								<td class="rightstyle2a">转码片源路径：</td>
								<td class="rightstyle3"><?php 
								    echo "原始片源相对路径：<br/>";
								    if(isset($result_encode_data['nns_file_url']['media']['in']) && strlen($result_encode_data['nns_file_url']['media']['in']) >0)
								    {
								        echo $result_encode_data['nns_file_url']['media']['in'];
								    }
								    else
								    {
								        echo "<font color='blue'>无原始片源相对路径</font>";
								    }
								    echo "<hr/>转码后片源相对路径：<br/>";
								    if(isset($result_encode_data['nns_file_url']['media']['out']) && strlen($result_encode_data['nns_file_url']['media']['out']) >0)
								    {
								        echo $result_encode_data['nns_file_url']['media']['out'];
								    }
								    else
								    {
								        echo "<font color='blue'>无转码后片源相对路径</font>";
								    }
								?></td>	
								<td class="rightstyle2a">抽帧图片路径：</td>
								<td class="rightstyle3"><?php 
    								if(isset($result_encode_data['nns_file_url']['drawing_frame']) && is_array($result_encode_data['nns_file_url']['drawing_frame']) && !empty($result_encode_data['nns_file_url']['drawing_frame']))
    								{
    								    foreach ($result_encode_data['nns_file_url']['drawing_frame'] as $drawing_frame_key=>$drawing_frame_val)
    								    {
    								        echo "{$drawing_frame_key} : {$drawing_frame_val}<br>";
    								    }
    								}
    								else
    								{
    								    echo "<font color='blue'>无片源转码路径</font>";
    								}
								?></td>
							</tr>
							<tr>
								<td class="rightstyle2a">失败次数：</td>
								<td class="rightstyle3"><?php echo $result_encode_data['nns_again'];?></td>	
								<td class="rightstyle2a"></td>
								<td class="rightstyle3"></td>
							</tr>				
						</tbody>
						</form>
					</table>
			</div>		
			<!-- 内容列表 DIV -->
			<div class="content_table formtable">
			    
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th width='40px'><input name="" type="checkbox" value="">序号</th>
							<th>描述</th>
							<th>请求状态</th>
							<th>请求内容</th>
							<th>响应状态</th>
							<th>响应内容</th>
							<th>修改时间</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(is_array($result_data['data_info']) && !empty($result_data['data_info']))
						{
							foreach ($result_data['data_info'] as $key=>$item) 
							{
						?>
						      <tr>
						          <td><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>"><?php echo $key; ?></td>
						          <td><?php echo $item['nns_desc'];?></td>
						          <td><?php 
						              switch ($item['nns_request_state'])
						              {
						                  case '0':
						                      echo "请求成功";
						                      break;
						                  case '1':
						                      echo "请求失败";
						                      break;
						                  default:
						                      echo "未知状态";
						              }
						          ?></td>
						          <td>
						          <?php if(strlen($item["nns_request_content"])<1){ 
						                  echo "";
						           }else{?>
						              <a href="<?php echo '../../../data/mgtv/file_encode/'.strtolower($result_encode_data['nns_sp_id']).'/request/'.$item["nns_request_content"];?>"
        							     target="_blank" title="<?php echo $item['nns_request_content'];?>">查看内容</a> 
        						  <?php }?>
        						  </td>
						          <td><?php 
						              switch ($item['nns_notify_state'])
						              {
						                  case '0':
						                      echo "响应成功";
						                      break;
						                  case '1':
						                      echo "响应失败";
						                      break;
						                  default:
						                      echo "未知状态";
						              }
						          ?></td>
						          <td>
						          <?php if(strlen($item["nns_request_content"])<1){
						              echo "";
						           }else{?>
						              <a href="<?php echo '../../../data/mgtv/file_encode/'.strtolower($result_encode_data['nns_sp_id']).'/notify/'.$item["nns_notify_content"];?>"
        							target="_blank" title="<?php echo $item['nns_notify_content'];?>">查看内容</a> 
        						  <?php }?>
        						  </td>
						          <td><?php echo $item['nns_modify_time']; ?></td>
                        </tr>
						<?php
						     }
						}
						?>
					</tbody>
				</table>
			</div>
		    <!-- 分页DIV -->
		    <div class="pagecontrol">
		             
            </div>    
            <div class="controlbtns">
                <div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        		<div style="clear:both;"></div>
            </div>
        </div> 
	</body>
</html>
