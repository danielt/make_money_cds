<?php
/*
 * Created on 2012-12-10
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 @header("Content-Type:text/html;charset=utf-8");
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
require_once $nncms_config_path. "nn_cms_manager/controls/nncms_controls_clear_cache.php";
include $nncms_config_path.'nn_logic/log/manager_log.class.php';
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_config/nn_cms_global.php";
$pri_bool = false;
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111101");

//权限跳转
if ($pri_bool === false){
	 Header("Location: ../nncms_content_wrong.php");
}else{
	$action_str = cms_get_lang('edit|assist|lmtb');
	asset_tree_image_edit();
}

function asset_tree_image_edit(){
	global $nncms_config_path,$action_str,$nncms_db_path;
	//备份栏目树
	require_once $nncms_db_path."nns_category_backup".DIRECTORY_SEPARATOR."nns_category_backup_class.php";
	$nns_category_backup_inst=new nns_category_backup_class();


	//debug
	require_once $nncms_db_path. "nns_debug_log".DIRECTORY_SEPARATOR . "nns_debug_log_class.php";
	$nns_debug_log=new nns_debug_log_class();
	$nns_debug_log->setlogpath("treexml");

	$asset_id = trim($_REQUEST['asset_id']);
	$category_id = $_REQUEST['category_id'];
	$image0 = $_FILES['image0'];
	$image1 = $_FILES['image1'];
	$image2 = $_FILES['image2'];
	$nns_org_id = $_SESSION['nns_org_id'];
	$nns_org_type = $_SESSION['nns_manager_type'];

	//引入图片上传
	include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
	//图片1处理
	$img_url0 = pub_func_image_upload_by_asset_category($image0,$asset_id,$category_id);
	//图片2处理
	$img_url1 = pub_func_image_upload_by_asset_category($image1,$asset_id,$category_id);
	//图片3处理
	$img_url2 = pub_func_image_upload_by_asset_category($image2,$asset_id,$category_id);



	include($nncms_db_path. "nns_assist/nns_db_assist_class.php");
	 $assist_inst=new nns_db_assist_class();
//	var_dump($_REQUEST);
//	die;
	 $assist_array=$assist_inst->nns_db_assist_info($asset_id);
	// var_dump($partner_array);
	 if ($assist_array["ret"]==0){
		$data=$assist_array["data"][0];
	 }
//	 $assist_inst=null;

	 if ($data["nns_category"]!=""){
		 $assist_content=$data["nns_category"];
	 }else{
		$assist_content="<assist></assist>";
	 }
	$dom = new DOMDocument('1.0', 'utf-8');
	$dom->loadXML($assist_content);
	$category = $dom->getElementsByTagName('category');
	$nns_debug_log->setlog($assist_content,"asset_tree");


	$nns_category_backup_inst->backup_categoty($assist_content,"asset",$asset_id,$nns_org_type,$nns_org_id);

	if (!empty($assist_array['data'])){

		 foreach($category as $item){

			if ($item->getAttribute("id")===$category_id){
//    	 		echo $img_url0.':'.$img_url1.':'.$img_url2;
				if (!empty($img_url0)) $item->setAttribute("img0",$img_url0);
				if (!empty($img_url1)) $item->setAttribute("img1",$img_url1);
				if (!empty($img_url2)) $item->setAttribute("img2",$img_url2);
				break;
			}
		 }
		 $xml_data=$dom->saveXML();
//    	 echo $xml_data;
//    	 die;
	   $result=$assist_inst->nns_db_assist_content_modify($asset_id,$xml_data);

	}

	if ($result){
		echo "<script>alert('".$action_str. cms_get_lang('fault'). "');</script>";
	}else{
//		$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],'topic_tree_edit',$_SESSION["nns_mgr_id"].$action_str.":".$category_id,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
//		$topic_name=nl_topic::get_topic_info($dc,$topic_id);
//		$topic_name=$topic_name[0]["nns_name"];
//		$log=$_SESSION["nns_mgr_id"].$action_str.$asset_id;
//		nl_manager_log::add($dc,$log,'add');
		echo "<script>alert('". $action_str. cms_get_lang('success'). "');</script>";
	}
	echo "<script>history.go(-1);</script>";
}
?>
