<?php
header("Content-Type:text/html; charset=UTF-8");
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

include("../../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include ($nncms_config_path. "nn_cms_config/nn_cms_config_path.php");
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";

$checkpri=new nns_db_pri_class();
if (!$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111101")){
	Header("Location: ../nncms_content_wrong.php");
}
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
 $checkpri=null;
include LOGIC_DIR. "label/video_label.class.php";
include LOGIC_DIR. "asset/asset.class.php";
$assist_detail_id=$_GET["assist_package_id"];
$assist_id=$_GET["assist_id"];
$dc=nl_get_dc( Array(
'db_policy'=>NL_DB_WRITE,
'cache_policy'=>NP_KV_CACHE_TYPE_MEMCACHE 
));
$dc->open();
$label_types=array('1000','1002','1003','1004','1005','1006');
$category_params=nl_asset::get_virtual_category_params(
$dc,
$assist_detail_id,
$assist_id,
NL_DC_DB
);

if (is_bool($category_params)) $category_params=NULL;
//var_dump($category_params);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/trim.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript">

function checkheight($frame){

		$frame.height($frame.contents().find(".content").height());
		window.parent.checkheight();
}

function add_labels(){
	var len=$(".labels_frame").length;
	var labels='';
	for(var i=0;i<len;i++){
		labels+=$(".labels_frame iframe")[i].contentWindow.get_labels();
	}
	$("#labels_id").val(labels);
}

</script>
</head>

<body>
<div class="content">
  
	<div class="content_table">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">

		<tbody>  
		   <tr>
			<td   style="padding:0px;"><div class="radiolist"><div class="radiogroup"><h3><?php echo cms_get_lang('vain_column_filter');?></h3>
			
			</div></div></td>
		  </tr>
			  <tr>                                                            
				<td style="padding: 0px;">
					<?php foreach ($label_types as $label_type){?>
						<div id='<?php echo $label_type;?>' class="labels_frame">
							<iframe  scrolling="no" frameborder="0" onload="checkheight($(this));" style="width:100%;" 
							src="../../controls/label_select/nncms_control_label_select.php?label_type=<?php echo $label_type;?>&labels=<?php echo $category_params['nns_labels']['nns_value'];?>"></iframe>
						</div>
					<?php }?>
				</td>
				
			</tr>
		   
		  </tbody>
		</table>
	</div>
	<div class="controlbtns" style=" border-top:1px solid #cfcfcf;">
		<div class="controlbtn sure"><a href="javascript:add_labels();$('#labels_form')[0].submit();"  action="sure"><?php echo cms_get_lang('confirm');?></a></div>
		<div class="controlbtn back"><a href="nncms_content_assistlist.php"  action="reset"><?php echo $language_action_back;?></a></div>
		
		<div style="clear:both;"></div>
	</div>
	<form id="labels_form" action="nncms_content_virtual_category_control.php" method="post"  >
		<input name="labels_id" id="labels_id" type="hidden" value=""  />
		<input name="asset_id" id="asset_id" type="hidden" value="<?php echo $assist_detail_id;?>"  />
		<input name="category_id" id="category_id" type="hidden" value="<?php echo $assist_id;?>"  />
	</form>
</div>
</body>
</html>