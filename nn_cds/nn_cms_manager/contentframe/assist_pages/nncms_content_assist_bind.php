<?php

header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temassist for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temassists
 */
// ob_start();
include("../../nncms_manager_inc.php");
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include $nncms_config_path . "nn_cms_config/nn_cms_global.php";
// 导入多语言包
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/languages/language_contant.php";

//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "111103");
$checkpri = null;

$nns_org_id = $_SESSION["nns_org_id"];
$nns_org_type = $_SESSION["nns_manager_type"];
$nns_id = $_GET["nns_id"];
if (!$pri_bool) {
        Header("Location: ../nncms_content_wrong.php");
        exit;
}
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
include($nncms_db_path . "nns_assist/nns_db_assist_class.php");
$assist_inst = null;
$data = $assist_array["data"];
$assist_id = $data[0]["nns_category_root_id"];
$assist_name = $data[0]["nns_name"];
$nns_category = $data[0]["nns_category"];


$language_types = g_cms_config::get_g_extend_language();
$language_types = trim($language_types);
if (empty($language_types)) {
        $type_array = array();
} else {
        $type_array = explode("|", $language_types);
}
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        )
);
//连接数据库
$dc->open();
include LOGIC_DIR . "/asset_bind/asset_bind.class.php";

if (isset($_GET['get'])) {
        $param = array();
        $param['nns_assists_id'] = $_GET['a'];
        $param['nns_assists_cid'] = $_GET['b'];
        $result = nl_asset_bind::get_asset_bind($dc, $param);
        $result = is_array($result) ? $result : array();
        $html = '<div class="content_table"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><th>绑定媒资ID</th><th>绑定栏目ID</th><th>操作</th></tr>';
        foreach ($result as $key => $name) {
                $class = $key % 2 ? '' : 'tr_odd';
                $html .="<tr class='$class'><td>{$name['nns_assists_bind_id']}</td><td>{$name['nns_assists_bind_cid']}</td><td><a href=javascript:delassets('{$name['nns_id']}');>删除</a></td></tr>";
        }
//$html .="<tr class='rightstyle'><td></td><td></td><td></td></tr>";
        $html .="</table></div>";
        if (count($result) < 1)
                $html = '';
        exit($html);
} elseif (isset($_GET['del'])) {
        $nns_id = $_GET['nns_id'];
        $result = nl_asset_bind::del_asset($dc, $nns_id);
        if ($result)
                exit('1');
        exit('0');
} else {
        $param = array();

        $param['nns_assists_id'] = $_GET['a'];
        $param['nns_assists_cid'] = $_GET['b'];
        $param['nns_assists_bind_id'] = $_GET['c'];
        $param['nns_assists_bind_cid'] = $_GET['d'];
        $result = nl_asset_bind::add_asset_bind($dc, $param);
        if ($result)
                exit('1');
        exit('0');
}
?>