<?php
/*
 * Created on 2012-3-2
 *
 * To change the temassist for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temassists
 */
 @header("Content-Type:text/html;charset=utf-8");
include("../../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
require_once $nncms_config_path. "nn_cms_manager/controls/nncms_controls_clear_cache.php";

 // 获取cache模块
include LOGIC_DIR.'asset'.DIRECTORY_SEPARATOR.'asset_item.class.php';
include LOGIC_DIR.'playbill'.DIRECTORY_SEPARATOR.'playbill_item.class.php';
include LOGIC_DIR.'video'.DIRECTORY_SEPARATOR.'live.class.php';
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
			));
$dc->open();

 $action=$_POST["action"];

 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
if (!empty($action)){
	switch($action){
		case "edit":
		$action_str=cms_get_lang('edit|assist_bdnr');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111101");
		break;
		case "add":
		$action_str=cms_get_lang('add|assist_bdnr');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111100");
		break;
		case "delete":
		$action_str=cms_get_lang('delete|assist_bdnr');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111102");
		break;
		case "move_category":
		$action_str=cms_get_lang('move_catalog');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111101");
		break;
		case "bind_product":
		$action_str=cms_get_lang('bind|cpgl_cp');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111101");
		break;
		default:
		break;
	}
}
$checkpri=null;




if (!$pri_bool){
	 Header("Location: ../nncms_content_wrong.php");
}else{
	require_once $nncms_db_path. "nns_log".DIRECTORY_SEPARATOR . "nns_db_op_log_class.php";
	$log_inst=new nns_db_op_log_class();

	require_once $nncms_config_path. "nn_cms_manager/controls/nncms_control_memcache.php";



	$assist_video_ids=$_POST["assist_ids"];
	$assist_id=$_POST["assist_id"];
	$assist_video_type=$_POST["assist_video_type"];
	$assist_detail_id=$_POST["assist_detail_id"];

	//	资源变动记录
	require_once $nncms_config_path. "nn_cms_manager/controls/nncms_control_resources_record.php";

	$resource_inst=new nncms_control_resources_record();

	$resource_inst->save_packet_info(array(
		'packet_id'=>$assist_detail_id,
		'packet_type'=>'0',
		'group_id'=>$_SESSION['nns_org_id'],
		'group_type'=>$_SESSION['nns_manager_type']
	));

	include($nncms_db_path. "nns_assist/nns_db_assist_item_class.php");
	 $assist_inst=new nns_db_assist_item_class();
	switch($action){
		case "edit":

		break;
		case "add":
		$assist_video_ids=rtrim($assist_video_ids,',');
		$ids=explode(',',$assist_video_ids);
		
		$assist_video_type=empty($assist_video_type)?'0':$assist_video_type;
		$assist_video_type=rtrim($assist_video_type,',');
		$types=explode(',',$assist_video_type);
		
		$video_types=array();
		if (count($types)>1){
			foreach ($types as $key=>$type_item){
				if (!is_array($video_types[$type_item])){
					$video_types[$type_item]=array();
				}
				array_push($video_types[$type_item],$ids[$key]);
			}
		}else{
			$video_types[$types[0]]=$ids;
		}
		
//		var_dump($video_types);die;
		
		foreach ($video_types as $type_key=>$type_value){
				//		如果是时移节目
			if ($type_key==2){
				$playbills=nl_playbill_item::get_playbill_item_info_by_ids($dc,$type_value);
				$live_info=nl_live::epg_get_live_info($dc,$playbills[0]['nns_live_id'],NL_DC_DB);
				$tag=$live_info[0]['nns_tag'];
				foreach ($playbills as $playbill){
					online_playbill($playbill,$tag,$assist_id,$assist_detail_id,$assist_inst);
				}
				
			}else{
				$result= $assist_inst->nns_db_assist_item_add_more(implode(',',$type_value),$type_key,$assist_id,$assist_detail_id);
			}
			$nns_names=$log_inst->nns_db_get_assest_video_name_arr($type_value,"video");
		}
		
		




		if ($result["ret"]!=0){
			echo "<script>alert('". $action_str, cms_get_lang('fault'), "');</script>";
		}else{

			add_more_resource_record($assist_video_ids,$nns_names,$assist_video_type,$assist_id);
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$nns_names,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			nl_asset_item::delete_asset_item_list_by_child_cache($dc,$assist_detail_id);
			delete_cache("index_asset_".$assist_detail_id."_".$assist_id);
			echo "<script>alert('". $action_str, cms_get_lang('success'), "');</script>";
		}
		break;
		case "delete":
		$delete_bool=true;
	//	$role_inst->
			$nns_ids=explode(",",$assist_video_ids);
			$nns_names=$log_inst->nns_db_get_assest_video_name_arr($assist_video_ids);
			$nns_types=$log_inst->nns_db_get_assest_video_name_arr($assist_video_ids,'id','nns_video_type');
			$num=0;
			$nns_name_arr=explode(',',$nns_names);
			$nns_type_arr=explode(',',$nns_types);
			foreach($nns_ids as $nnsid){
				if (!empty($nnsid)){
					$num++;
					$resultarr=$assist_inst->nns_db_assist_item_delete($nnsid);
					if ($resultarr["ret"]!=0 && $num==1){
						$delete_bool=false;
						echo "<script>alert('".$result["reason"] ."');</script>";
						break;
					}

					save_recordone($nnsid,$nns_type_arr[$num-1],$nns_name_arr[$num-1],$assist_detail_id,'delete');
				}
			}
			$resource_inst->submit_actions();
		if (!$delete_bool){
			echo "<script>alert('". $action_str, cms_get_lang('fault'), "');</script>";
		}else{

			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$nns_names,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			nl_asset_item::delete_asset_item_list_by_child_cache($dc,$assist_detail_id);
			delete_cache("index_asset_".$assist_detail_id."_".$assist_id);
			echo "<script>alert('". $action_str, cms_get_lang('success'), "');</script>";
		}

		break;
		case "move_category":
		$move_category_id=$_POST["move_category_id"];
		$move_asset_id=$_POST["move_asset_id"];
		$delete_bool=true;
	//	$role_inst->
			$nns_ids=explode(",",$assist_video_ids);
			$nns_names=$log_inst->nns_db_get_assest_video_name_arr($assist_video_ids);
			$nns_types=$log_inst->nns_db_get_assest_video_name_arr($assist_video_ids,'id','nns_video_type');
			$num=0;
			$nns_name_arr=explode(',',$nns_names);
			$nns_type_arr=explode(',',$nns_types);
			foreach($nns_ids as $nnsid){
				if (!empty($nnsid)){
					$num++;
					$resultarr=$assist_inst->nns_db_assist_item_move_category($nnsid,$move_asset_id,$move_category_id);
					if ($resultarr["ret"]==2){
						break;
					}
					if ($resultarr["ret"]!=0 && $num==1){
						$delete_bool=false;
						echo "<script>alert('".$result["reason"] ."');</script>";
						break;
					}
					save_recordone($nnsid,$nns_type_arr[$num-1],$nns_name_arr[$num-1],$assist_detail_id,$assist_id,'delete');
					save_recordone($nnsid,$nns_type_arr[$num-1],$nns_name_arr[$num-1],$move_asset_id,$move_category_id,'add');
				}
			}
			$resource_inst->submit_actions();
			if (!$delete_bool){
				echo "<script>alert('". $action_str, cms_get_lang('fault'), "');</script>";
			}else{

				$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$nns_names,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
				nl_asset_item::delete_asset_item_list_by_child_cache($dc,$assist_detail_id);
				nl_asset_item::delete_asset_item_list_by_child_cache($dc,$move_asset_id);
				delete_cache("index_asset_".$assist_detail_id."_".$assist_id);
				delete_cache("index_asset_".$move_asset_id."_".$move_category_id);
				echo "<script>alert('". $action_str, cms_get_lang('success'), "');</script>";
			}

		break;
		case "bind_product":
			//require_once $nncms_db_path. "nns_products".DIRECTORY_SEPARATOR . "nns_db_products_class.php";
			require_once $nncms_db_path. "nns_products".DIRECTORY_SEPARATOR . "nns_db_product_video_class.php";
			//$product_inst = new nns_db_products_class();
			$product_video_inst = new nns_db_product_video_class();
			$assist_video_ids=rtrim($assist_video_ids,',');
			$nns_ids=explode(",",$assist_video_ids);
			$assist_item_arr = $product_video_inst->get_video_id_by_assist_item_ids($nns_ids);
			foreach($nns_ids as $assist_item_id){
				$video_id = $assist_item_arr[$assist_item_id];
				$result = $product_video_inst->video_bind_product($_POST['nns_product_id'],$_POST['nns_product_fee_id'], $_POST['assist_detail_id'], $_POST['assist_id'], $video_id);
				if ($result["ret"]!=0){
					echo "<script>alert('". $action_str, cms_get_lang('fault'), "');</script>";break;
				}
			}

		break;
		default:

		break;
	}
	//var_dump($result);
	$assist_inst=null;
	echo "<script>window.parent.refresh_tree_content();</script>";
}

function save_recordone($id,$video_type,$video_name,$category_id,$action){
		global $resource_inst;
		$params=array();
		$params['video_id']=$id;
		$params['video_type']=$video_type;
		$params['video_name']=$video_name;
		$params['category_id']=$category_id;
		$params['action']=$action;
		$resource_inst->save_action($params);
}

function add_more_resource_record($video_ids,$video_names,$video_type,$category_id){
	global $resource_inst;
	$video_arr=explode(',',rtrim($video_ids,','));
	$name_arr=explode(',',rtrim($video_names,','));
	$num=0;
	foreach ($video_arr as $id){
		save_recordone($id,$video_type,$name_arr[$num],$category_id,'add');
		$num++;
	}
	$resource_inst->submit_actions();
}

function online_playbill($playbill,$tag,$category_id,$asset_id,$asset_inst){
	
	$count = $asset_inst->nns_db_assist_item_count($category_id, $asset_id, $playbill['nns_id'], 2);
	
	if ($count['data'][0]['num']>0) return;
	
	$asset_inst->nns_db_assist_item_add(
		$playbill['nns_id'],
		2,
		$playbill['nns_name'],
		$playbill['nns_image0'],
		$playbill['nns_image1'],
		$playbill['nns_image2'],
		$playbill['nns_image3'],
		$playbill['nns_image4'],
		$playbill['nns_image5'],
		NULL,
		$category_id,
		$asset_id,
		1,0,0,0,0,
		$playbill['nns_pinyin'],
		$tag,
		$playbill['nns_eng_name'],
		$playbill['nns_summary']
	);
}





?>