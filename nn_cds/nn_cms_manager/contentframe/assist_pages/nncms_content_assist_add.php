<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_pri.php";
 include $nncms_config_path."nn_cms_db/nns_common/nns_db_constant.php";
 include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php";

 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();

$action=$_GET["action"];
$nns_id=$_GET["nns_id"];
$pri_bool=false;
if (!empty($action)){
	switch($action){
		case "edit":
		$action_str = cms_get_lang('save');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111101");
		break;
		case "add":
		$action_str = cms_get_lang('add');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111100");
		break;
		default:
		break;
	}
}
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");


 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
if ($action=="edit"){
 include($nncms_db_path. "nns_assist/nns_db_assist_class.php");
 $assist_inst=new nns_db_assist_class();
 $assist_arr=$assist_inst->nns_db_assist_info($nns_id);
 $assist_inst=null;
 if ($assist_arr["ret"]==0){
	$edit_data=$assist_arr["data"][0];
 }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/radiolist.js"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>

</head>

<body>

<div class="content">
	<div class="content_position"><?php echo cms_get_lang('assist_gl');?> > <?php echo $action_str.cms_get_lang('assist');?></div>

	<form id="add_form" action="nncms_content_assist_control.php" method="post"  enctype="multipart/form-data" >
	 <input name="action" id="action" type="hidden" value="<?php echo $action; ?>_1"/>
	 <input name="nns_id" id="nns_id" type="hidden" value="<?php echo $nns_id; ?>"  />
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>

	  <tr>
			  <td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('assist|id');?></td>
			  <td class="assist_types_td">
			 <input name="assistid" id="assistid" type="text" rule="noempty|no_ch"
					value="<?php echo $edit_data["nns_id"];?>" <?php if ($action=="edit"){?> readonly='readonly' style='background:#F3F3F3; border:1px solid #D1D1D1; color:#8E8E8E;' <?php }?>
				/>
			  </td>

	  </tr>
	  <tr style="display:none;">
			  <td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('mlsgid');?></td>
			  <td class="assist_types_td">
			 <input name="baseid" id="baseid" type="text" rule="noempty|int" maxlength="5"
					value="1000"
				/>
			  </td>

	  </tr>
	  <tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('assist|name');?> </td>
				<td><input name="nns_assist_name" id="nns_assist_name" type="text" rule="noempty"
					value="<?php echo $edit_data["nns_name"];?>"
				/></td>

	  </tr>
	  <tr>
				<td class="rightstyle"><?php echo cms_get_lang('media_bjhb');?>(<?php echo cms_get_lang('media_big')?>) </td>
				<td><div class="img_content">
					 <?php if (!empty($edit_data["nns_image"])){?>
						<div class="img_view">
						<h4><?php echo cms_get_lang('poster_thumb');?></h4>
						<a href="<?php echo pub_func_get_image_url($edit_data["nns_image"]);?>?rand=<?php echo rand();?>" target="_blank">
						<img src="<?php echo pub_func_get_image_url($edit_data["nns_image"]);?>?rand=<?php echo rand();?>" style="border:none;" /></a>
						<h4 class="resolution"></h4></div>

					<?php }?>
						<div class="img_edit"> <input name="assist_image" id="assist_image" type="file" size="60%" /></div>
						<div style="clear:both;"/>
					</div>
				</td>
	  </tr>
	  <tr>
				<td class="rightstyle"><?php echo cms_get_lang('media_bjhb');?>(<?php echo cms_get_lang('media_normal')?>) </td>
				<td><div class="img_content">
					 <?php if (!empty($edit_data["nns_image1"])){?>
						<div class="img_view">
						<h4><?php echo cms_get_lang('poster_thumb');?></h4>
						<a href="<?php echo pub_func_get_image_url($edit_data["nns_image1"]);?>?rand=<?php echo rand();?>" target="_blank">
						<img src="<?php echo pub_func_get_image_url($edit_data["nns_image1"]);?>?rand=<?php echo rand();?>" style="border:none;" /></a>
						<h4 class="resolution"></h4></div>

					<?php }?>
						<div class="img_edit"> <input name="assist_image1" id="assist_image1" type="file" size="60%" /></div>
						<div style="clear:both;"/>
					</div>
				</td>
	  </tr>
	  <tr>
				<td class="rightstyle"><?php echo cms_get_lang('media_bjhb');?>(<?php echo cms_get_lang('media_small')?>) </td>
				<td><div class="img_content">
					 <?php if (!empty($edit_data["nns_image2"])){?>
						<div class="img_view">
						<h4><?php echo cms_get_lang('media_bjhb|media_slt')?></h4>
						<a href="<?php echo pub_func_get_image_url($edit_data["nns_image2"]);?>?rand=<?php echo rand();?>" target="_blank">
						<img src="<?php echo pub_func_get_image_url($edit_data["nns_image2"]);?>?rand=<?php echo rand();?>" style="border:none;" /></a>
						<h4 class="resolution"></h4>
						</div>
											<?php }?>
						<div class="img_edit"> <input name="assist_image2" id="assist_image2" type="file" size="60%" /></div>
						<div style="clear:both;"/>
					</div>
				</td>
	  </tr>
		  </tbody>
		</table>
	</div>
	</form>

	<div class="controlbtns">
		<div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('assist_gl');?>','<?php echo cms_get_lang('msg_ask_change'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
		<?php if ($action=="edit"){?>
		<div class="controlbtn <?php echo $action;?>"><a href="nncms_content_assist_edit.php?nns_id=<?php echo $nns_id;?>"><?php echo cms_get_lang('lmgl');?></a></div>
		<?php }?>
		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>

</body>
</html>
<script language="javascript" src="../../js/image_loaded_func.js"></script>