<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temassist for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temassists
 */
ob_start();
include("../../nncms_manager_inc.php");
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
// 导入多语言包
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');

//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/language_contant.php";

//引入 评论LOGIC模块
include_once $nncms_config_path . "nn_logic/comment/comment_map.class.php";
include_once $nncms_config_path . "nn_logic/comment/comment_content.class.php";
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//打开DC
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_READ,
    'cache_policy' => NP_KV_CACHE_TYPE_NULL
        )
);
$dc->open();
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "111103");
$checkpri = null;

$nns_org_id = $_SESSION["nns_org_id"];
$nns_org_type = $_SESSION["nns_manager_type"];
$assist_detail_id = $_GET["assist_package_id"];
$assist_id = $_GET["assist_id"];
$search_video = $_GET["search_video"];
$search_py = $_GET["search_pinyin"];
$nns_tag = $_GET['nns_tag'];
if (!$pri_bool) {
        Header("Location: ../nncms_content_wrong.php");
} else {

        include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
        include($nncms_db_path . "nns_assist/nns_db_assist_item_class.php");
        include $nncms_db_path . "nns_common/nns_db_pager_class.php";
        // include_once $nncms_db_path. "nns_vod/nns_db_vod_class.php";
        // include_once $nncms_db_path. "nns_live/nns_db_live_class.php";
        if (!empty($_COOKIE["page_max_num"])) {
                $g_manager_list_max_num = $_COOKIE["page_max_num"];
        }

        $assist_category_inst = new nns_db_assist_item_class();
        if ($nns_tag) {
                $countArr = $assist_category_inst->nns_db_assist_item_count($assist_id, $assist_detail_id, null, null, 1, $search_video, null, $search_py, $nns_tag);
        } else {
                $countArr = $assist_category_inst->nns_db_assist_item_count($assist_id, $assist_detail_id, null, null, 1, $search_video, null, $search_py);
        }
        if ($countArr["ret"] == 0)
                $assist_category_total_num = $countArr["data"][0]["num"];
        $assist_category_pages = ceil($assist_category_total_num / $g_manager_list_max_num);
        $currentpage = 1;
        if (!empty($_GET["page"]))
                $currentpage = $_GET["page"];

        $url = "?";
        $url.="assist_id=$assist_id&assist_package_id=$assist_detail_id&nns_tag=$nns_tag";
        $pager = new nns_db_pager_class($assist_category_total_num, $g_manager_list_max_num, $currentpage, $url);
        if ($nns_tag) {
                $assist_category_array = $assist_category_inst->nns_db_assist_item_list(($currentpage - 1) * $g_manager_list_max_num, $g_manager_list_max_num, $assist_id, $assist_detail_id, null, null, 1, $search_video, null, null, $search_py, $nns_tag);
        } else {
                $assist_category_array = $assist_category_inst->nns_db_assist_item_list(($currentpage - 1) * $g_manager_list_max_num, $g_manager_list_max_num, $assist_id, $assist_detail_id, null, null, 1, $search_video, null, null, $search_py);
        }

        $assist_category_inst = null;
        // var_dump($manager_array);
        if ($assist_category_array["ret"] != 0) {
                $data = null;
                //echo "<script>alert(". $assist_category_array["reason"].");</script>";
        }
        $data = $assist_category_array["data"];

        //取媒资项绑定产品包
        include($nncms_db_path . "nns_products/nns_db_products_class.php");
        require_once $nncms_db_path . "nns_products" . DIRECTORY_SEPARATOR . "nns_db_product_video_class.php";

        $product_video_inst = new nns_db_product_video_class();
        $video_ids = array();
        if (!empty($data)) {
                foreach ($data as $assist_item) {
                        $video_ids[] = $assist_item['nns_video_id'];
                        if ($assist_item['nns_video_type'] == 0) {
                                $vod_ids[] = $assist_item['nns_video_id'];
                        } elseif ($assist_item['nns_video_type'] == 1) {
                                $live_ids[] = $assist_item['nns_video_id'];
                        } elseif ($assist_item['nns_video_type'] == 2) {
                                $playbill_ids[] = $assist_item['nns_video_id'];
                        }
                }
                $product_content_model = new nns_db_products_class();
                $single_video_model_produnct = $product_content_model->get_single_video_model_produnct();

                $assist_bind_product_list = $product_video_inst->get_video_bind_product($video_ids, $assist_detail_id, $assist_id);
                if (!empty($assist_bind_product_list)) {
                        //$product_ids = array();
                        $product_fee_ids = array();
                        foreach ($assist_bind_product_list as $assist_bind_product) {
                                ///$product_ids[] =  $assist_bind_product['nns_product_id'];
                                $product_fee_ids[] = $assist_bind_product['nns_product_fee_id'];
                                $product_fee_list = $product_content_model->get_product_fee_by_ids($single_video_model_produnct['nns_id'], $product_fee_ids);
                        }
                        // if(! empty($product_ids)){
                        //$product_name_list = $product_content_model->get_product_name_by_ids($product_ids);
                        //$product_fee_name_list = $product_content_model->get_product_fee_name_by_ids($product_fee_ids);
                        //}
                }

//		 取点播详细信息
                $vod_inst = new nns_db_vod_class();
                $vod_infos = $vod_inst->get_vod_by_ids($vod_ids);
                $vod_infos = np_array_rekey($vod_infos, 'nns_id');

//		 取直播详细信息
                $live_inst = new nns_db_live_class();
                $live_infos = $live_inst->get_live_by_ids($live_ids);
                $live_infos = np_array_rekey($live_infos, 'nns_id');


                //		 取节目详细信息
                include_once LOGIC_DIR . "playbill/playbill_item.class.php";
                $playbill_infos = nl_playbill_item::get_playbill_item_info_by_ids($dc, $playbill_ids);

                $live_inst = NULL;
                $vod_inst = NULL;

                //取当前分类绑定产品包
                //$assist_category_bind_product_content = $product_content_model->get_assist_bind_product($assist_detail_id, $assist_id);
        }

        ob_end_clean();
//echo $assist_detail_id;
        ?>
        <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title></title>
                        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                        <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                        <link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
                        <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
                        <script language="javascript" src="../../js/dtree.js"></script>
                        <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                        <script language="javascript" src="../../js/cms_cookie.js"></script>
                        <script language="javascript" src="../../js/table.js.php"></script>
                        <script language="javascript" src="../../js/radiolist.js"></script>
                        <script language="javascript" src="../../js/rate.js"></script>
                        <script language="javascript" src="../../js/checkinput.js.php"></script>
                        <script language="javascript" src="../../js/cms_ajax.js"></script>
                        <script language="javascript" src="../../js/image_loaded_func.js"></script>
                        <script language="javascript" >
                                function set_delete(guid) {
                                        var assist_ids = getAllCheckBoxSelect();
                                        if (assist_ids == "") {
                                                alert("<?php echo cms_get_lang('msg_4'); ?>");
                                        } else {
                                                $("#delete_form #action").val("delete");
                                                $("#delete_form #assist_ids").val(assist_ids);
                                                checkForm('<?php echo cms_get_lang('assist_gl'); ?>', '<?php echo cms_get_lang('msg_ask_offline'); ?>', $('#delete_form'), 'delete');
                                        }
                                }
                                function begin_select_assist(except_detail_id, except_id) {
                                        window.parent.begin_select_assist(except_detail_id, except_id);
                                }
                                function set_video_id(value, type, name) {
                                        $("#delete_form #action").val("add");
                                        $("#delete_form #assist_video_type").val(type);
                                        $("#delete_form #assist_ids").val(value);
                                        checkForm('<?php echo cms_get_lang('assist_gl'); ?>', '<?php echo cms_get_lang('msg_ask_delete'); ?>', $('#delete_form'), 'add');
                                }

                                function assist_order_up(id, order) {
                                        if (!confirm("确定要移动吗？")) {
                                                return false;
                                        }

                                        self.location = "nncms_content_assist_order_control.php?action=up&assist_id=<?php echo $assist_id; ?>&assist_detail_id=<?php echo $assist_detail_id; ?>&id=" + id + "&order=" + order;
                                }
                                function assist_order_down(id, order) {
                                        if (!confirm("确定要移动吗？")) {
                                                return false;
                                        }
                                        self.location = "nncms_content_assist_order_control.php?action=down&assist_id=<?php echo $assist_id; ?>&assist_detail_id=<?php echo $assist_detail_id; ?>&id=" + id + "&order=" + order;
                                }
                                function assist_order_pre(id, order) {
                                        if (!confirm("确定要移动吗？")) {
                                                return false;
                                        }
                                        self.location = "nncms_content_assist_order_control.php?action=pre&assist_id=<?php echo $assist_id; ?>&assist_detail_id=<?php echo $assist_detail_id; ?>&id=" + id + "&order=" + order;
                                }
                                function assist_order_next(id, order) {
                                        if (!confirm("确定要移动吗？")) {
                                                return false;
                                        }
                                        self.location = "nncms_content_assist_order_control.php?action=next&assist_id=<?php echo $assist_id; ?>&assist_detail_id=<?php echo $assist_detail_id; ?>&id=" + id + "&order=" + order;
                                }
                                function set_move_category_id(value, asset_id) {

                                        var par = getAllCheckBoxSelect();
                                        $("#delete_form").find("#move_category_id").val(value);
                                        $("#delete_form").find("#move_asset_id").val(asset_id);
                                        $("#delete_form").find("#action").val("move_category");
                                        if (par == "") {
                                                alert("<?php echo cms_get_lang('media_select_jm'); ?>");
                                        } else {
                                                $('#assist_ids').attr('value', getAllCheckBoxSelect());
                                                checkForm('<?php echo cms_get_lang('dbgl'); ?>', '<?php echo cms_get_lang('msg_ask_change'); ?>', $('#delete_form'));
                                        }
                                }
                                function begin_select_category() {
                                        var par = getAllCheckBoxSelect();
                                        if (par == "") {
                                                alert("<?php echo cms_get_lang('msg_4'); ?>");
                                        } else {
                                                window.parent.begin_select_category();
                                        }
                                }
                                function begin_video_bind_product(assist_item_id) {
                                        $('#assist_ids').attr('value', assist_item_id);
                                        window.parent.begin_assist_item_bind_product();
                                }
                                function begin_assist_item_bind_product() {
                                        var par = getAllCheckBoxSelect();
                                        if (par == "") {
                                                alert("<?php echo cms_get_lang('msg_4'); ?>");
                                        } else {
                                                $('#assist_ids').attr('value', getAllCheckBoxSelect());
                                                window.parent.begin_assist_item_bind_product();
                                        }
                                }
                                function set_video_bind_product_id(product_id, name) {
                                        $("#delete_form").find("#nns_product_id").val(product_id);
                                }
                                function set_product_fee_id(value, name) {
                                        $("#delete_form").find("#nns_product_fee_id").val(value);
                                        $("#delete_form").find("#action").val("bind_product");
                                        var par = $('#assist_ids').val();
                                        if (par == "") {
                                                alert("<?php echo cms_get_lang('msg_4'); ?>");
                                        } else {
                                                checkForm('<?php echo cms_get_lang('assist|lmgl_nrlmgl') ?>', '<?php echo cms_get_lang('sfbddpjfcpb') ?>', $('#delete_form'), "bind_product");
                                        }

                                }
                                /***
                                 function set_single_video_bill_model_product_id(product_id,fee_id){
                                 var par=getAllCheckBoxSelect();
                                 $("#delete_form").find("#nns_product_id").val(product_id);
                                 $("#delete_form").find("#nns_product_fee_id").val(fee_id);
                                         
                                 $("#delete_form").find("#action").val("bind_product");
                                 if (par==""){
                                 alert("<?php echo cms_get_lang('media_select_jm'); ?>");
                                 }else{
                                 $('#assist_ids').attr('value',getAllCheckBoxSelect());
                                 checkForm('<?php echo cms_get_lang('assist|lmgl_nrlmgl') ?>','<?php echo cms_get_lang('topic_bind_is') ?>',$('#delete_form'),"bind_product");
                                 }
                                 }
                                 ***/
                                function delete_video_bind_product(nns_id) {
                                        get_url = "action=delete_product_video&nns_id=" + nns_id + "&rad=" + Math.random();
                                        getAjax("../../controls/nncms_controls_ajax_assest_category.php?" + get_url, delete_assist_bind_product_ajax_result);
                                }
                                function delete_assist_bind_product_ajax_result(result) {
                                        if (result.ret == 0) {
                                                window.location.reload();
                                        }
                                }

                                function update_epg_control($item_id, $obj) {
                                        $radiolist = $obj.parent().parent().find(".radiolist");
                                        get_url = 'asset_item_id=' + $item_id + '&tag=' + getCheckedData(false, $radiolist);
                                        //		alert(get_url);
                                        getAjax("../../controls/nncms_control_ajax_update_assist_item_tag.php?" + get_url, update_epg_control_response);

                                }

                                function update_epg_control_response(result) {

                                        if (result.ret == 0) {
                                                window.location.reload();
                                        }
                                }

                                function show_epg_control_block($obj) {
                                        var $block = $obj.siblings(".epg_control_block");
                                        if ($block.is(":hidden")) {
                                                $obj.siblings(".epg_control_block").show();
                                        } else {
                                                $obj.siblings(".epg_control_block").hide();
                                        }
                                }

                                $(document).ready(function() {
                                        window.parent.now_frame_url = window.location.href;
                                        $(".epg_control_block").hide();
                                });

                                function comment_manage(obj) {
                                        var comment_resource_id = $(obj).parent().parent().find("td").eq(0).find("input").attr("value");
                                        var resource_id = $(obj).parent().find("input").attr("value");
                                        var comment_url = "../comment_pages/nncms_content_comment_list.php?pack_type=assist&pack_id=<?php echo $assist_detail_id; ?>&categoey_id=<?php echo $assist_id; ?>&pack_resource_id=" + comment_resource_id + "&resource_id=" + resource_id;
                                        window.parent.comment_link(comment_url);
                                }
                        </script>
                </head>
                <body>

                        <div class="content">
                                <div class="category_edit_box"   style="padding:0px; margin:0px;">

                                        <div class="content_table formtable">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <thead>
                                                                <tr>
                                                                        <th><input name="" type="checkbox" value="" /></th>
                                                                        <th><?php echo cms_get_lang('segnumber'); ?></th>
                                                                        <th><?php echo cms_get_lang('name'); ?></th>

                                                                        <th><?php echo cms_get_lang('media_mtlx'); ?></th>
                                                                        <th><?php echo cms_get_lang('play_count'); ?></th>
                                                                        <?php if ($g_single_video_bill_mode) { ?>
                                                                                <th><?php echo cms_get_lang('app_billing') ?></th>
                                                                                <?php
                                                                        }
                                                                        ?>
                                                                        <?php if ($g_asset_item_epg_enabled == 1) { ?>
                                                                                <th><?php echo cms_get_lang('epg_output_control') ?></th>
                                                                        <?php } if (g_cms_config::get_g_config_value('g_comment_enabled')) { ?>
                                                                                <th width="100px"><?php echo cms_get_lang('comment_manager') ?></th>
                                                                        <?php } ?>
                                                                        <th>排列序号</th>
                                                                        <th><?php echo cms_get_lang('order'); ?></th>

                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                                <?php
                                                                if ($data != null) {
                                                                        $num = ($currentpage - 1) * $g_manager_list_max_num;
                                                                        foreach ($data as $item) {
                                                                                $resource_id = $item["nns_video_id"];
                                                                                $map_info = nl_comment_map::get_comment_map_list($dc, $assist_detail_id, "assist", $assist_id, $resource_id);
                                                                                $map_id = $map_info[0]["nns_id"];
                                                                                $comment_enabled = $map_info[0]["nns_enabled"];
                                                                                if ($comment_enabled) {
                                                                                        $commnet_num = nl_comment_content::get_comment_list_by_map($dc, $map_id);
                                                                                        if (is_array($commnet_num)) {
                                                                                                $commnet_num = count($commnet_num);
                                                                                        }
                                                                                        else
                                                                                                $commnet_num = 0;
                                                                                }

                                                                                $num++;
                                                                                ?>
                                                                                <tr>
                                                                                        <td><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>" /></td>
                                                                                        <td><?php echo $num; ?></td>
                                                                                        <td>
                                                                                                <?php
                                                                                                if ($item["nns_video_type"] == 1) {
                                                                                                        $live_item_info = $live_infos[$item["nns_video_id"]];
                                                                                                        ?>
                                                                                                        <alt alt="<?php echo pub_func_get_video_alt($live_item_info); ?>">
                                                                                                                <a href="../live_pages/nncms_content_live_detail.php?nns_id=<?php echo $item["nns_video_id"]; ?>" class="live">
                                                                                                                        <?php
                                                                                                                } elseif ($item["nns_video_type"] == 0) {
                                                                                                                        $vod_item_info = $vod_infos[$item["nns_video_id"]];
                                                                                                                        ?>
                                                                                                                        <alt alt="<?php echo pub_func_get_vod_alt($vod_item_info); ?>">
                                                                                                                                <a href="../vod_pages/nncms_content_vod_detail.php?nns_id=<?php echo $item["nns_video_id"]; ?>" class="vod">
                                                                                                                                        <?php
                                                                                                                                } elseif ($item["nns_video_type"] == 2) {
                                                                                                                                        $playbill_info = $playbill_infos[$item["nns_video_id"]];
                                                                                                                                        ?>
                                                                                                                                        <alt alt="<?php echo pub_func_get_playbill_alt($playbill_info); ?>">
                                                                                                                                                <a href="../playbill_pages/nncms_content_playbill_look.php?action=edit&nns_id=<?php echo $item["nns_video_id"]; ?>" class="playbill">
                                                                                                                                                <?php } ?>
                                                                                                                                                <?php echo $item["nns_video_name"]; ?></a></alt></td>

                                                                                                                                <td><?php
                                                                                                                                        if ($item["nns_video_type"] == 0) {
                                                                                                                                                echo cms_get_lang('media_db');
                                                                                                                                        } elseif ($item["nns_video_type"] == 1) {
                                                                                                                                                echo cms_get_lang('media_zb');
                                                                                                                                        } elseif ($item["nns_video_type"] == 2) {
                                                                                                                                                echo cms_get_lang('media_sy|program');
                                                                                                                                        };
                                                                                                                                        ?> </td>

                                                                                                                                <td><?php echo (int) $item["nns_play_count"]; ?></td>

                                                                                                                                <?php if ($g_single_video_bill_mode) { ?>
                                                                                                                                        <td>
                                                                                                                                                <?php
                                                                                                                                                if (isset($assist_bind_product_list[$item["nns_video_id"]]) && isset($product_fee_list[$assist_bind_product_list[$item["nns_video_id"]]['nns_product_fee_id']]['nns_product_fee_name'])) {
                                                                                                                                                        //echo $product_name_list[$assist_bind_product_list[$item["nns_video_id"]]['nns_product_id']];
                                                                                                                                                        //echo '&nbsp;&nbsp;';
                                                                                                                                                        //echo '资费：'.$assist_bind_product_list[$item["nns_video_id"]]['nns_product_fee_id'];
                                                                                                                                                        echo $product_fee_list[$assist_bind_product_list[$item["nns_video_id"]]['nns_product_fee_id']]['nns_product_fee_name'];
                                                                                                                                                        echo '(' . $product_fee_list[$assist_bind_product_list[$item["nns_video_id"]]['nns_product_fee_id']]['nns_price'] . ')';
                                                                                                                                                        echo '&nbsp;&nbsp;&nbsp;&nbsp;<img style="cursor: pointer;" src="../../images/topicon_08-topicon.png" onclick="delete_video_bind_product(\'' . $assist_bind_product_list[$item["nns_video_id"]]['nns_id'] . '\');"  border="0"/>';
                                                                                                                                                } else {
                                                                                                                                                        echo '<a href="javascript:begin_video_bind_product(\'' . $item["nns_id"] . '\')">' . cms_get_lang('bind_tariff') . ' </a>';
                                                                                                                                                }
                                                                                                                                                ?>
                                                                                                                                        </td>
                                                                                                                                <?php } ?>
                                                                                                                                <?php if ($g_asset_item_epg_enabled == 1) { ?>
                                                                                                                                        <td>
                                                                                                                                                <alt alt="<?php echo get_tag_detail_html($item["nns_single_tag"]); ?>"><?php echo $item["nns_single_tag"]; ?></alt>
                                                                                                                                                <?php if (!empty($item["nns_tag"]) && $g_asset_item_epg_enabled == 1) { ?>
                                                                                                                                                        &nbsp;&nbsp;<a href="javascript:void(null);" onclick="show_epg_control_block($(this));" class="epg_control"><?php echo cms_get_lang('adjust') ?></a>
                                                                                                                                                        <div class="epg_control_block" style=" background:#FFF;position:absolute; width:170px; border:5px solid #CCC; padding:5px;">
                                                                                                                                                                <div class="radiolist">
                                                                                                                                                                        <?php
                                                                                                                                                                        $tag_str = rtrim($item["nns_tag"], ',');
                                                                                                                                                                        $single_tag_str = ',' . $item["nns_single_tag"];
                                                                                                                                                                        $tags = explode(',', $tag_str);
                                                                                                                                                                        foreach ($tags as $tag) {
                                                                                                                                                                                $bool = strstr($single_tag_str, ',' . $tag . ',');
                                                                                                                                                                                ?>
                                                                                                                                                                                <div class="radioitem"><input name="" type="checkbox" value="<?php echo $tag; ?>" <?php
                                                                                                                                                                                        if ($bool) {
                                                                                                                                                                                                echo 'checked="checked"';
                                                                                                                                                                                        }
                                                                                                                                                                                        ?>/> <?php echo get_tag_detail_html($tag); ?></div>
                                        <?php } ?>
                                                                                                                                                                </div>
                                                                                                                                                                <div style=" text-align:right;">
                                                                                                                                                                        <input name="" type="button" value="<?php echo cms_get_lang('epg_output') ?>" onclick="update_epg_control('<?php echo $item['nns_id']; ?>', $(this));"/>
                                                                                                                                                                </div>
                                                                                                                                                        </div>
                                                                                                                                        <?php } ?>
                                                                                                                                        </td>
                        <?php } if (g_cms_config::get_g_config_value('g_comment_enabled')) { ?>

                                                                                                                                        <td> <input type="hidden" value="<?php echo $item['nns_video_id'] ?>" />
                                                                                                                                                <div id="comment"  onclick="comment_manage(this)"  style="float:left"><a class="link" href="#"><?php echo cms_get_lang('manager') ?></a></div>
                                                                                                                                                &nbsp;&nbsp;&nbsp;&nbsp;<div style="float:left;margin-left:5px">
                                                                                                                                                        <?php
                                                                                                                                                        if ($comment_enabled == 0) {
                                                                                                                                                                echo "<font color='red'>" . cms_get_lang('closed') . "</font>";
                                                                                                                                                        } else {
                                                                                                                                                                if ($commnet_num == 0) {
                                                                                                                                                                        echo cms_get_lang('guess_comment');
                                                                                                                                                                } else {
                                                                                                                                                                        echo "(" . $commnet_num . ")" . cms_get_lang('number');
                                                                                                                                                                }
                                                                                                                                                        }
                                                                                                                                                        ?>
                                                                                                                                                </div>
                                                                                                                                        </td>
                        <?php } ?>

                                                                                                                                <td><?php echo $item["nns_order"]; ?></td>
                                                                                                                                <td  class="control_btns" name="<?php echo cms_get_lang('order') ?>">

                                                                                                                                        <a href="javascript:assist_order_up('<?php echo $item["nns_id"]; ?>',<?php echo $item["nns_order"]; ?>);"><?php echo cms_get_lang('order_up'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                                        <a href="javascript:assist_order_down('<?php echo $item["nns_id"]; ?>',<?php echo $item["nns_order"]; ?>);"><?php echo cms_get_lang('order_down'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                                        <a href="javascript:assist_order_pre('<?php echo $item["nns_id"]; ?>',<?php echo $item["nns_order"]; ?>);"><?php echo cms_get_lang('order_pre'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                                        <a href="javascript:assist_order_next('<?php echo $item["nns_id"]; ?>',<?php echo $item["nns_order"]; ?>);"><?php echo cms_get_lang('order_next'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                                </td>
                                                                                                                                </tr>
                <?php }
        }
        ?>
                                                                                                                <script language="javascript">
                                create_empty_tr($(".formtable"),<?php echo count($data); ?>,<?php echo $nncms_ui_min_list_item_count; ?>);
                                                                                                                </script>
                                                                                                                </tbody>
                                                                                                                </table>
                                                                                                                </div>

                                                                                                                </div>
        <?php echo $pager->nav(); ?>
                                                                                                                <div class="controlbtns" style=" border-top:1px solid #cfcfcf;">

                                                                                                                        <div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);"><?php echo cms_get_lang('select'); ?></a></div>
                                                                                                                        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);"><?php echo cms_get_lang('cancel'); ?></a></div>

                                                                                                                        <div class="controlbtn add"><a href="javascript:begin_select_assist('<?php echo $assist_detail_id; ?>','<?php echo $assist_id; ?>');"  action="add"><?php echo cms_get_lang('assist_nrsx'); ?></a></div>
                                                                                                                        <div class="controlbtn edit"><a href="javascript:begin_select_category();" action="edit"><?php echo cms_get_lang('move_catalog'); ?></a></div>
                                                                                                                        <div class="controlbtn delete"><a href="javascript:set_delete();"  action="delete"><?php echo cms_get_lang('assist_nrxx'); ?></a></div>
        <?php if ($g_single_video_bill_mode) { ?>
                                                                                                                                <div class="controlbtn edit"><a href="javascript:begin_assist_item_bind_product();" action="edit"><?php echo cms_get_lang('bind_tariff') ?></a></div>
        <?php } ?>
                                                                                                                        <div style="clear:both;"></div>
                                                                                                                </div>
                                                                                                                <form id="delete_form" action="nncms_content_assist_item_control.php" method="post" >
                                                                                                                        <input name="action" id="action" type="hidden" value="delete" />
                                                                                                                        <input name="assist_detail_id" id="assist_detail_id" type="hidden" value="<?php echo $assist_detail_id; ?>" />
                                                                                                                        <input name="assist_id" id="assist_id" type="hidden" value="<?php echo $assist_id; ?>" />
                                                                                                                        <input name="assist_ids" id="assist_ids" type="hidden" value="" />
                                                                                                                        <input name="assist_video_type" id="assist_video_type" type="hidden" value="" />
                                                                                                                        <input name="move_category_id" id="move_category_id" type="hidden" value="" />
                                                                                                                        <input name="move_asset_id" id="move_asset_id" type="hidden" value="" />
                                                                                                                        <input name="nns_product_id" id="nns_product_id" type="hidden" value="" />
                                                                                                                        <input name="nns_product_fee_id" id="nns_product_fee_id" type="hidden" value="" />
                                                                                                                </form>
                                                                                                                </div>
                                                                                                                </body>
                                                                                                                </html>
        <?php
}?>