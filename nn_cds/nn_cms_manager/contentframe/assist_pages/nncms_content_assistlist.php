<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 ob_start();
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111103");
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");

$nns_org_type=$_SESSION["nns_manager_type"];

$nns_org_id=$_SESSION["nns_org_id"];



include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
if (!empty($_COOKIE["page_max_num"])){
		$g_manager_list_max_num=$_COOKIE["page_max_num"];
	}
 include $nncms_db_path. "nns_assist/nns_db_assist_class.php";
 $assist_inst=new nns_db_assist_class();
 $countArr=$assist_inst->nns_db_assist_count($nns_org_id,$nns_org_type);
 if ($countArr["ret"]==0)
 $total_num=$countArr["data"][0]["num"];
 $pages=ceil($total_num/$g_manager_list_max_num);
 $currentpage=1;
 if (!empty($_GET["page"])) $currentpage=$_GET["page"];

 	$assist_array=$assist_inst->nns_db_assist_list($nns_org_id,$nns_org_type,null,($currentpage-1)*$g_manager_list_max_num,$g_manager_list_max_num);

 $assist_inst=null;
// var_dump($manager_array);
 if ($assist_array["ret"]!=0){
 	$data=null;
 	echo "<script>alert(". $assist_array["reason"].");</script>";
 }
 $data=$assist_array["data"];

 ob_end_clean();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript">
function checkhiddenInput(){
	var par=getAllCheckBoxSelect();
if (par==""){
		alert("<?php echo cms_get_lang('msg_4');?>");
	}else{
		$('#nns_id').attr('value',getAllCheckBoxSelect());
		checkForm('<?php echo cms_get_lang('assist_gl');?>','<?php echo cms_get_lang('msg_ask_delete');?>',$('#delete_form'));
	}
}


</script>
</head>

<body>
<div class="content">
	<div class="content_position"><?php echo cms_get_lang('assist_gl');?> > <?php echo cms_get_lang('assist_lb');?></div>

    <div class="content_table formtable">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
        	<tr>
        		<th><input name="" type="checkbox" value="" /></th>
                <th><?php echo cms_get_lang('segnumber');?></th>
                <th><?php echo cms_get_lang('id');?></th>
                <th><?php echo cms_get_lang('name');?></th>

                <th><?php echo cms_get_lang('create_time');?></th>
                <th><?php echo cms_get_lang('edit_time');?></th>
                 <th><?php echo cms_get_lang('action');?></th>

            </tr>
        </thead>
        <tbody>
        <?php
        	if ($data!=null){
        	 $num=($currentpage-1)*$g_manager_list_max_num; foreach($data as $item){
        	$num++;
        	?>
              <tr>
              <td><input name="input" type="checkbox" value="<?php echo  $item["nns_id"];?>" /></td>
                <td><?php echo $num;?></td>
                <td><a class="link" href="nncms_content_assist_add.php?nns_id=<?php echo $item["nns_id"];?>&action=edit" target="_self"><?php echo $item["nns_id"];?></a></td>
                <td><?php echo $item["nns_name"];?></td>

                <td><?php echo $item["nns_create_time"];?> </td>
                <td><?php echo $item["nns_modify_time"];?> </td>
                <td>
                	<a href="nncms_content_assist_edit.php?nns_id=<?php echo $item["nns_id"];?>"><?php echo cms_get_lang('lmgl');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                	<a href="nncms_content_assist_tree_addedit.php?nns_id=<?php echo $item["nns_id"];?>"><?php echo cms_get_lang('assist_nrgl');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                	<?php if ($g_metadata_enabled==1){?>
                	<a href="nncms_content_assist_metadata.php?asset_id=<?php echo $item["nns_id"];?>"><?php echo cms_get_lang('ysjgl')?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                	<?php }?>
                </td>
              </tr>
          <?php }} $least_num=$nncms_ui_min_list_item_count-count($data);
          		for ($i=0;$i<$least_num;$i++){?>
          		<tr>
          		<td>&nbsp;</td>
         		<td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              	</tr>

          	<?php	}?>
          </tbody>
        </table>
    </div>
     <div class="pagecontrol">
     	<?php if ($currentpage>1){?>
     		<a href="nncms_content_assistlist.php?page=1<?php echo $nns_url;?>" target="_self"><?php echo cms_get_lang('first_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
    		<a href="nncms_content_assistlist.php?page=<?php echo $currentpage-1;?><?php echo $nns_url;?>" target="_self"><?php echo cms_get_lang('pre_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
    	<?php }else{?>
    		<span><?php echo cms_get_lang('first_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
    		<span><?php echo cms_get_lang('pre_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
    	<?php } ?>

    	<?php if ($currentpage<$pages){?>
        <a href="nncms_content_assistlist.php?page=<?php echo $currentpage+1;?><?php echo $nns_url;?>" target="_self"><?php echo cms_get_lang('next_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="nncms_content_assistlist.php?page=<?php echo $pages;?><?php echo $nns_url;?>" target="_self"><?php echo cms_get_lang('last_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php }else{?>
      		<span><?php echo cms_get_lang('next_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
    		<span><?php echo cms_get_lang('last_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
      	<?php }?>
        <?php echo cms_get_lang('jump_to');?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage;?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page');?> &nbsp;&nbsp;
        <a href="javascript:go_page_num('nncms_content_assistlist.php?ran=1<?php echo $nns_url;?>',<?php echo $pages;?>);">GO>></a>&nbsp;&nbsp;
        <?php echo cms_get_lang('current');?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage."/".$pages;?></span><?php echo cms_get_lang('page');?>&nbsp;&nbsp;|&nbsp;&nbsp;
         <?php echo cms_get_lang('perpagenum');?>&nbsp;
         <input name="nns_list_max_num" id="nns_list_max_num" type="text"
         value="<?php echo $g_manager_list_max_num;?>" style="width:24px;"/>&nbsp;&nbsp;
         <input type="button" value="<?php echo cms_get_lang('confirm');?>"
         onclick="refresh_prepage_list();"/>
    </div>
    <div class="controlbtns">
         <div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);"><?php echo cms_get_lang('select');?></a></div>
        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);"><?php echo cms_get_lang('cancel');?></a></div>

        <div class="controlbtn add"><a href="javascript:void(null);" pos="nncms_content_assist_add.php" action="add"><?php echo cms_get_lang('add');?></a></div>
    	<div class="controlbtn edit"><a href="javascript:void(null);" pos="nncms_content_assist_add.php" action="edit"><?php echo cms_get_lang('edit');?></a></div>
        <div class="controlbtn delete"><a href="javascript:checkhiddenInput();"  action="delete"><?php echo cms_get_lang('delete');?></a></div>

        <div style="clear:both;"></div>
    </div>
     <form id="delete_form" action="nncms_content_assist_control.php" method="post">
      <input name="action" id="action" type="hidden" value="delete_1" />
      <input name="nns_id" id="nns_id" type="hidden" value="" />
     </form>
</div>
</body>
</html>