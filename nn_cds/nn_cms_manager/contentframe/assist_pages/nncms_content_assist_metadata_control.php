<?php
/*
 * Created on 2012-5-30
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

include("../../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include ($nncms_config_path. "nn_cms_config/nn_cms_config_path.php");
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
 $action=$_POST["action"];
 $nns_id=$_POST["nns_id"];
 $asset_id=$_POST["asset_id"];
 $key=$_POST["key"];
 $value=$_POST["value"];
$checkpri=new nns_db_pri_class();
if (!$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111101")){
	Header("Location: nncms_content_wrong.php");
}
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
 $checkpri=null;
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
require_once $nncms_db_path. "nns_metadata".DIRECTORY_SEPARATOR . "nns_metadata_class.php";
$metadata_inst=new nns_metadata_class();

$params=array();
$params["metadata_id"]=$asset_id;
$params["id"]=$nns_id;
$params["metadata_key"]=$key;
$params["metadata_value"]=$value;
$params["metadata_type"]="asset";

 switch ($action){
	case "add":
	if (trim($key)!="" && trim($value)!=""){
		$action_str=cms_get_lang('add|media_package_metadata');
		$result=$metadata_inst->nns_metadata_add($params);
		}else{
			echo "<script>alert(", cms_get_lang('data_not_allow_null'),"'！');self.location.href='nncms_content_assist_metadata.php?asset_id=$asset_id';</script>";
		}
	break;
	case "edit":
	if (trim($key)!="" && trim($value)!=""){
		$action_str=cms_get_lang('edit|media_package_metadata');
		$result=$metadata_inst->nns_metadata_modify($params);
		}else{
			echo "<script>alert(", cms_get_lang('data_not_allow_null'),"'！');self.location.href='nnncms_content_assist_metadata.php?asset_id=$asset_id';</script>";
		}
	break;
	case "delete":
		$action_str=cms_get_lang('delete|media_package_metadata');
		$result=$metadata_inst->nns_metadata_delete($nns_id);
	break;
 }
 
 if($result["ret"]==0){
	echo "<script>alert('".$action_str, cms_get_lang('success'),"！');self.location.href='nncms_content_assist_metadata.php?asset_id=$asset_id';</script>";
 }else{
	echo "<script>alert('".$action_str, cms_get_lang('fault'),"！');self.location.href='nncms_content_assist_metadata.php?asset_id=$asset_id';</script>";
 }	

?>
