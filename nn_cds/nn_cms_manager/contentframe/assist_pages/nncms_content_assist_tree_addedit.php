<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temassist for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temassists
 */
ob_start();
include ("../../nncms_manager_inc.php");
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
// 导入多语言包
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/language_contant.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "111103");
$checkpri = null;

$nns_org_id = $_SESSION["nns_org_id"];
$nns_org_type = $_SESSION["nns_manager_type"];
/**
 * @author txc 
 * 评论管理跳转到资源详细页
 */
if ($_GET["action"] == 'comment') {
        $item_id = $_GET["item_id"];
        $action = $_GET["action"];
        $item_type = $_GET["item_type"];
}
$nns_id = $_GET["nns_id"];

if (!$pri_bool) {
        Header("Location: ../nncms_content_wrong.php");
} else {
        include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
        // echo $nncms_db_path. "nns_role/nns_db_role_class.php";
        include($nncms_db_path . "nns_assist/nns_db_assist_class.php");
        $assist_inst = new nns_db_assist_class();
        $assist_array = $assist_inst->nns_db_assist_info($nns_id);
        // var_dump($partner_array);
        if ($assist_array["ret"] != 0) {
                $data = null;
                echo "<script>alert(" . $assist_array["reason"] . ");</script>";
        }
        $assist_inst = null;
        $data = $assist_array["data"];
        $assist_id = $data[0]["nns_category_root_id"];
        $assist_name = $data[0]["nns_name"];
        if (!empty($_COOKIE["page_max_num"])) {
                $g_manager_list_max_num = $_COOKIE["page_max_num"];
        }
        ob_end_clean();
        ?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title></title>
                        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                        <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                        <link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
                        <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
                        <script language="javascript" src="../../js/dtree.js"></script>
                        <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                        <script language="javascript" src="../../js/cms_cookie.js"></script>
                        <script language="javascript" src="../../js/table.js.php"></script>
                        <script language="javascript" src="../../js/tabs.js"></script>
                        <script language="javascript" src="../../js/checkinput.js.php"></script>
                        <script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
                        <script language="javascript" src="../../js/category_count.js"></script>
                        <script language="javascript" src="../../js/image_loaded_func.js"></script>
                        <script language="javascript">
                                var baseurl;
                                var now_frame_url;
                                var vod_tabs;
                                var comment = 1;
                                var now_item_id;              //评论——>详细页的响应标识
                                $(document).ready(function() {

                                        category.count('assist',<?php echo '\'' . $data[0]['nns_id'] . '\'' ?>);

                                        set_content_width();
                                        $(window).resize(function() {
                                                set_content_width();
                                        });
                                        $(".selectbox").hide();


                                        $(".split_btn").click(function() {
                                                if ($(".category_tree").is(":hidden")) {
                                                        $(".category_tree").show();
                                                } else {
                                                        $(".category_tree").hide();
                                                }
                                                set_content_width();
                                        });

                                        vod_tabs = new mytabs($(".state_tabs"), refresh_table);
                                                   maxcount = 0;
                                        $('.clip .dTreeNode').each(function() {
                                                var t = $(this).find('a');
                                                if (t.attr('target') == '_top') {
                                                        $.get('nncms_content_virtual_category_vod_list.php?ajax&assist_package_id=<?php echo $nns_id; ?>&assist_id=' + t.attr('categoryid'), function(count) {
                                                                t.find('span').html(count);
                                                                maxcount +=Number(count);
                                                        });
                                                } else {
                                                        maxcount += Number(t.find('span').html());
                                                }
                                        })
                                });


                                function refresh_table(actions) {
                                        var tab_select_id = vod_tabs.get_select_id();
                                        get_virtual_pages(tab_select_id);
                                }

                                function refresh_tree_content() {
                                        $(".assist_frame iframe").attr("src", now_frame_url);
                                }

                                function set_content_width() {
                                        if ($(".category_tree").is(":hidden")) {
                                                $(".category_editbox").width($(".content").width() - 25);
                                        } else {
                                                $(".category_editbox").width($(".content").width() - $(".category_tree").width() - 50);
                                        }
                                        var len2c = $(".rightstyle2c").text().length;
                                        $(".rightstyle2c").width(len2c * 6);
                                }
                                function checkhiddenInput() {
                                        var par = getAllCheckBoxSelect();
                                        if (par == "") {
                                                alert("<?php echo cms_get_lang('lmgl_msg_noselect'); ?>");
                                        } else {
                                                $('#nns_id').attr('value', getAllCheckBoxSelect());
                                                checkForm('<?php echo cms_get_lang('assist_gl'); ?>', '<?php echo cms_get_lang('msg_ask_delete'); ?>', $('#delete_form'));
                                        }
                                }

                                function begin_show_cdn(id, type) {
                                        $("#selectbox2 iframe").attr("src", "../../controls/nncms_control_show_cdn.php?media_id=" + id + "&media_type=" + type);
                                        $("#selectbox2").show();
                                }

                                function select_tree_item(item_id, item_name, parent_id, virtual) {
                                        window.now_item_id = item_id;
                                        if (virtual == 1) {
                                                $(".state_tabs").show();
                                                $(".state_tab").removeClass("select");
                                                $(".state_tab:eq(0)").addClass("select");
                                                baseurl = "nncms_content_virtual_category_vod_list.php?assist_id=" + now_item_id + "&assist_package_id=<?php echo $data[0]["nns_id"]; ?>";

                                                //        baseurl = "nncms_content_virtual_category_edit.php?assist_id=" + item_id + "&assist_package_id=<?php echo $data[0]["nns_id"]; ?>";
                                        } else {
                                                $(".state_tabs").hide();
                                                baseurl = "nncms_content_assist_item_list.php?assist_id=" + item_id + "&assist_package_id=<?php echo $data[0]["nns_id"]; ?>";
                                        }
                                        now_frame_url = baseurl;
                                        $("#nns_video_search").val("");
                                        $(".category_editbox").find(".category_edit_id").html(item_id);
                                        $(".category_editbox").find(".category_edit_name").html(item_name);
                                        $(".assist_frame iframe").attr("src", baseurl);
                                        //评论管理跳转到详细页的过渡js
        <?php
        if ($action == 'comment') {
                if ($item_type == 0) {
                        ?>
                                                        if (comment == 1) {
                                                                now_frame_url = "../vod_pages/nncms_content_vod_detail.php?nns_id=<?php echo $item_id ?>";
                                                                refresh_tree_content();
                                                                comment = 0;
                                                        }
                <?php } else { ?>
                                                        if (comment == 1) {
                                                                now_frame_url = "../live_pages/nncms_content_live_detail.php?nns_id=<?php echo $item_id ?>";
                                                                refresh_tree_content();
                                                                comment = 0;
                                                        }
                <?php
                }
        }
        ?>


                                }

                                function get_virtual_pages(method) {
                                        if (method == 0) {
                                                baseurl = "nncms_content_virtual_category_vod_list.php?assist_id=" + now_item_id + "&assist_package_id=<?php echo $data[0]["nns_id"]; ?>";
                                        } else if (method == 1) {
                                                baseurl = "nncms_content_virtual_category_edit.php?assist_id=" + now_item_id + "&assist_package_id=<?php echo $data[0]["nns_id"]; ?>";
                                        } else {
                                                var item_id = $(".category_editbox").find(".category_edit_id").html();
                                                baseurl = "nncms_content_assist_item_recom_list.php?assist_id=" + now_item_id + "&assist_package_id=<?php echo $data[0]["nns_id"]; ?>";
                                        }
                                        $(".assist_frame iframe").attr("src", baseurl);
                                }


                                function refresh_assist_frame() {
                                        setCookie("page_max_num", $("#nns_list_max_num").val());
                                        $(".assist_frame iframe")[0].contentWindow.location.reload();
                                }

                                function search_video() {
                                        search_str = "&search_pinyin=" + $("#nns_video_pinyin").val();
                                        search_str += "&search_video=" + $("#nns_video_search").val();
                                        search_str += "&nns_tag=" + $("#nns_tag").val();



                                        $(".assist_frame iframe").attr("src", baseurl + search_str);
                                }

                                $(document).keydown(function(event) {
                                        if (event.which == 13) {
                                                search_video();
                                        }
                                });
                                function set_move_category_id(value, asset_id) {
                                        $(".assist_frame iframe")[0].contentWindow.set_move_category_id(value, asset_id);
                                        close_select();
                                }

                                function begin_select_category() {
                                        $("#selectbox2 iframe").attr("src", "../../controls/video_category_select/nncms_controls_asset_category_select.php?method=asset&tree_id=<?php echo $nns_id; ?>");
                                        $("#selectbox2").show();
                                }
                                function begin_assist_item_bind_product() {
                                        //$("#selectbox2 iframe").attr("src","../../controls/nncms_controls_select.php?method=video_bind_product");
                                        $("#select_frame").attr("src", "../../controls/nncms_controls_select.php?method=product_fee&video_bind_product=1");
                                        $("#selectbox2").show();
                                }

                                function set_video_bind_product_id(product_id, name) {
                                        //$("#nns_product_id").attr("value",product_id);
                                        $(".assist_frame iframe")[0].contentWindow.set_video_bind_product_id(product_id, name);
                                        $("#select_frame").attr("src", "../../controls/nncms_controls_select.php?method=product_fee&product_id=" + product_id + "");
                                        $(".selectbox").show();
                                }
                                function set_product_fee_id(value, name) {
                                        $(".assist_frame iframe")[0].contentWindow.set_product_fee_id(value, name);
                                        /*
                                         $("#nns_product_fee_id").attr("value",value);
                                         $('#action').val('video_bind_product');
                                         checkForm('<?php echo cms_get_lang('assist|lmgl') ?>','<?php echo cms_get_lang('sfbddpjfcpb') ?>',$('#edit_form'),'video_bind_product');
                                         */
                                }
                                function set_video_id(value, type, name) {
                                        $(".assist_frame iframe")[0].contentWindow.set_video_id(value, type, name);
                                        //	close_select();
                                }

                                function checkheight() {
                                        var frame_url = $(".assist_frame iframe")[0].contentWindow.location.href;
                                        var page_name = frame_url.split("?")[0];
                                        page_name = page_name.split("/");
                                        page_name = page_name[page_name.length - 1];
                                        if (page_name == "nncms_content_assist_item_list.php") {
                                                $(".display_list").show();
                                        } else {
                                                $(".display_list").hide();
                                        }

                                        $(".assist_frame iframe").height($(".assist_frame iframe").contents().find(".content").height());
                                        $(".category_tree").height($(".category_editbox").height());
                                        $(".split_btn").height($(".category_editbox").height() + 20);
                                        window.parent.resetFrameHeight();

                                }
                                function close_select() {
                                        $(".selectbox").hide();
                                }

                                function begin_select_assist(except_detail_id, except_id) {
                                        var urlstr = "../../controls/video_select/nncms_controls_video_select.php?method=vod&org_type=<?php echo $nns_org_type; ?>&org_id=<?php echo $nns_org_id; ?>&more_select=true&except_detail_id=" + except_detail_id + "&except_id=" + except_id + "&except_method=assist";
                                        //window.open (urlstr,'newwindow','height=100,width=400,top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no')
                                        //yxt2012年9月2日10:28:25，给下面的if加了注释，如果有这个判断的第二次点击添加内容的时候，之前添加的内容就还会显示出来，因为没有重新去查找。
                                        //if ($("#selectbox1 iframe").attr("src")!=urlstr)
                                        $("#selectbox1 iframe").attr("src", urlstr);
                                        $("#selectbox1").show();
                                }
                                //跳转到评论管理的过渡

                                function comment_link(url) {
                                        window.location.href = url;
                                }
                                function begin_select_label(vid) {
                                        $("#select_frame").attr("src", "../../../nn_models/nn_label/admin.php?action=videos_of_label_list&video_id=" + vid + "&video_type=0");
                                        $("#select_frame").css('width', '800px');
                                        $(".selectbox").show();
                                }
                                // 添加虚栏目推荐
                                function begin_select_asset_video(asset_id, category_id) {
                                        var urlstr = "../../controls/video_select/nncms_controls_asset_video_select.php?asset_id=" + asset_id + "&category_id=" + category_id + "&more_select=true";

                                        $("#selectbox1 iframe").attr("src", urlstr);
                                        $("#selectbox1").show();
                                }

                        </script>
                </head>

                <body>
                        <div class="selectbox" id="selectbox2">
                                <iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
                        </div>
                        <div class="selectbox video_select" id="selectbox1">
                                <iframe scrolling="no" frameborder="0" id="select_frame"></iframe>
                        </div>
                        <div class="content">
                                <div class="content_position"><?php echo cms_get_lang('assist_gl'); ?> > <?php echo cms_get_lang('lmgl_nrlmgl'); ?></div>
                                <div style="background-color:#FFF;">
                                        <div class="category_tree">

                                                <?php
                                                if ($data != null) {
                                                        $assist_content = $data[0]["nns_category"];
                                                } else {
                                                        $assist_content = null;
                                                }
                                                echo pub_func_get_tree_by_html($assist_content, $data[0]["nns_name"], $assist_id);
                                                ?>

                                        </div>
                                        <div class="split_btn"></div>
                                        <div class="category_editbox">
                                                <div class="content_table">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tbody>
                                                                        <tr>

                                                                                <td class="rightstyle2c"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>
        <?php echo cms_get_lang('assist|lmgl_nrlmid'); ?>/<?php echo cms_get_lang('name'); ?>:</td>
                                                                                <td colspan="3"><span class="category_edit_id"></span>/<span class="category_edit_name"></span></td>

                                                                        </tr>
                                                                        <tr class="display_list">
                                                                                <td><?php echo cms_get_lang('media_ypcx'); ?></td>
                                                                                <td colspan="3"><?php echo cms_get_lang('media_pinyin'); ?>:&nbsp;&nbsp;<input name="nns_video_pinyin" id="nns_video_pinyin" type="text" value="" style="width:150px;"/>
                                                                                        &nbsp;&nbsp;&nbsp;<?php echo cms_get_lang('title'); ?>:&nbsp;&nbsp;<input name="nns_video_search" id="nns_video_search" type="text" value=""  style="width:150px;"/>
                                                                                        &nbsp;&nbsp;&nbsp;<?php //echo NNS_PLAYER_STD_TAG    ?>
                                                                                        <select id="nns_tag" style="width:120px;">
                                                                                                <option name="">----</option>
                                                                                                <?php echo get_tag_options(NNS_PLAYER_STD_TAG, '') ?>
                                                                                                <?php echo get_tag_options(NNS_APPLE_PHONE_TAG, '') ?>
                                                                                                <?php echo get_tag_options(NNS_APPLE_PAD_TAG, '') ?>
                                                                                                <?php echo get_tag_options(NNS_ANDROID_PHONE_TAG, '') ?>
                                                                                                <?php echo get_tag_options(NNS_ANDROID_PAD_TAG, '') ?>
        <?php echo get_tag_options(NNS_PC_TAG, '') ?>
                                                                                        </select>
                                                                                        &nbsp;&nbsp;&nbsp;<input type="button" value="<?php echo cms_get_lang('search'); ?>" onclick="search_video();"/></td>

                                                                        </tr>

                                                                </tbody>
                                                        </table>
                                                </div>
                                                <div class="state_tabs">
                                                        <div class="state_tab" >标签影片列表</div>
                                                        <div class="state_tab" >虚栏目筛选条件</div>
                                                        <div class="state_tab" >虚栏目推荐</div>

                                                        <div style="clear:both;"></div>
                                                </div>
                                                <div class="assist_frame vod_frame">
                                                        <iframe  scrolling="no" frameborder="0" onload="checkheight();" style="width:100%;"></iframe>
                                                </div>

                                        </div>
                                        <div style="clear:both;"/>
                                </div>


                        </div>
                </body>
        </html>
<?php } ?>
