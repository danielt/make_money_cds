<?php
header("Content-Type:text/html; charset=UTF-8");
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

include("../../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
include ($nncms_config_path . "nn_cms_config/nn_cms_config_path.php");
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
if (!$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "111101")) {
        Header("Location: ../nncms_content_wrong.php");
}
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
$checkpri = null;
include LOGIC_DIR . "label/video_label.class.php";
include LOGIC_DIR . "asset/asset.class.php";
include $nncms_db_path . "nns_common/nns_db_pager_class.php";
$assist_detail_id = $_GET["assist_package_id"];
$assist_id = $_GET["assist_id"];
$search = $_GET["search"];
$dc = nl_get_dc(Array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
$dc->open();
$label_types = array('1000', '1002', '1003', '1004', '1005', '1006');
$category_params = nl_asset::get_virtual_category_params(
                $dc, $assist_detail_id, $assist_id, NL_DC_DB
);
$labels = explode(',', $category_params['nns_labels']['nns_value']);

array_pop($labels); //删除最后一个是空格
$category_bind = nl_asset_bind::get_asset_bind(
                $dc, array(
            'nns_assists_id' => $assist_detail_id,
            'nns_assists_cid' => $assist_id
                )
);

$url = "?";
$url.="assist_id=$assist_id&assist_package_id=$assist_detail_id&search=$search";
if (is_array($category_bind)) {
        $assist_detail_id = $category_bind[0]['nns_assists_bind_id'];
        $assist_id = $category_bind[0]['nns_assists_bind_cid'];
}
if (!empty($_COOKIE["page_max_num"])) {
        $g_manager_list_max_num = $_COOKIE["page_max_num"];
}
$count = nl_asset_item_bo::count_asset_item_search_by_labels($dc, $tag, $assist_detail_id, '', $labels, '1', 'pinyin_firstchar|name_likechar', $search);
$count = is_bool($count) ? '0' : $count;
if (isset($_GET['ajax']))
        exit($count);
$pager = new nns_db_pager_class($assist_category_total_num, $g_manager_list_max_num, $currentpage, $url);
$assist_category_total_num = $count;
$assist_category_pages = ceil($assist_category_total_num / $g_manager_list_max_num);
$currentpage = 1;
if (!empty($_GET["page"]))
        $currentpage = $_GET["page"];

$pager = new nns_db_pager_class($assist_category_total_num, $g_manager_list_max_num, $currentpage, $url);
$list = nl_asset_item_bo::epg_get_asset_item_search_by_labels($dc, '', $assist_detail_id, '', $labels, $pager->startnum, $pager->page_size, 'NL_ORDER_NUM_ASC', '1', 'pinyin_firstchar|name_likechar', $search);
$list = is_array($list) ? $list : array();
if (is_bool($category_params))
        $category_params = NULL;
//var_dump($category_params);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../../js/trim.js"></script>
                <script language="javascript" src="../../js/table.js.php"></script>
                <script language="javascript" src="../../js/cms_cookie.js"></script>
                <script language="javascript" src="../../js/checkinput.js.php"></script>
                <script language="javascript">

                        function checkheight($frame) {

                                $frame.height($frame.contents().find(".content").height());
                                window.parent.checkheight();
                        }

                        function add_labels() {
                                var len = $(".labels_frame").length;
                                var labels = '';
                                for (var i = 0; i < len; i++) {
                                        labels += $(".labels_frame iframe")[i].contentWindow.get_labels();
                                }
                                $("#labels_id").val(labels);
                        }

                </script>
        </head>

        <body>
                <div class="content">
                        <div class="content_table formtable">
                                <form name="s_form" action="nncms_content_virtual_category_vod_list.php" method="get">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                        <td>
                                                                影片名称/拼音：
                                                                <input type="text" name="search" style="width:150px;" value=""<?php echo $_GET['search'] ?>/>
                                                                <input type="hidden" name="assist_id" value="<?php echo $_GET['assist_id'] ?>"/>
                                                                <input type="hidden" name="assist_package_id" value="<?php echo $_GET['assist_package_id'] ?>"/>
                                                                <input type="submit" value="搜索"/>
                                                                </table>
                                                                </form>
                                                                </div>
                                                                <div class="content_table formtable">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                <thead>
                                                                                        <tr>
                                                                                                <th><?php echo cms_get_lang('segnumber'); ?></th>
                                                                                                <th><?php echo cms_get_lang('video_name'); ?></th>
                                                                                                <th><?php echo cms_get_lang('play_count'); ?></th>
                                                                                                <th>操作</th>
                                                                                        </tr>
                                                                                </thead>
                                                                                <tbody>

                                                                                        <?php
                                                                                        foreach ($list as $key => $v) {
                                                                                                ?>
                                                                                                <tr>
                                                                                                        <td><?php echo $key + 1; ?></td>   <td><?php echo $v['nns_video_name']; ?></td>   <td><?php echo $v['nns_play_count']; ?></td>   <td><a href="../vod_pages/nncms_content_vod_detail.php?nns_id=<?php echo $v['nns_video_id']; ?>">查看影片详情</a></td>
                                                                                                </tr>
                                                                                                <?php
                                                                                        }
                                                                                        $size = $pager->page_size - count($list);
                                                                                        for ($i = 0; $i < $size; $i++) {
                                                                                                ?>
                                                                                                <tr >
                                                                                                        <td colspan="17">&nbsp;</td>
                                                                                                </tr>
                                                                                        <?php }
                                                                                        ?>



                                                                                </tbody>
                                                                        </table>
                                                                        <?php echo $pager->nav(); ?>
                                                                        <div class="controlbtns" style=" border-top:1px solid #cfcfcf;">
                                                                                <div class="controlbtn sure"><a href="javascript:add_labels();$('#labels_form')[0].submit();"  action="sure"><?php echo cms_get_lang('confirm'); ?></a></div>
                                                                                <div class="controlbtn back"><a href="nncms_content_assistlist.php"  action="reset"><?php echo $language_action_back; ?></a></div>

                                                                                <div style="clear:both;"></div>
                                                                        </div>
                                                                        <form id="labels_form" action="nncms_content_virtual_category_control.php" method="post"  >
                                                                                <input name="labels_id" id="labels_id" type="hidden" value=""  />
                                                                                <input name="asset_id" id="asset_id" type="hidden" value="<?php echo $assist_detail_id; ?>"  />
                                                                                <input name="category_id" id="category_id" type="hidden" value="<?php echo $assist_id; ?>"  />
                                                                        </form>
                                                                </div>
                                                                </body>
                                                                </html>