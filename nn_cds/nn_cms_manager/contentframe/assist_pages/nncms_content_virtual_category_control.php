<?php
/*
 * Created on 2013-1-21
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 header("Content-Type:text/html; charset=UTF-8");

include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include ($nncms_config_path. "nn_cms_config/nn_cms_config_path.php");
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";

$checkpri=new nns_db_pri_class();
if (!$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111101")){
	Header("Location: ../nncms_content_wrong.php");
}
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
 $checkpri=null;
include LOGIC_DIR. "label/video_label.class.php";
include LOGIC_DIR. "asset/asset.class.php";

$labels_id=$_POST['labels_id'];
$asset_id=$_POST['asset_id'];
$category_id=$_POST['category_id'];

$dc=nl_get_dc( array(
'db_policy'=>NL_DB_WRITE,
'cache_policy'=>NP_KV_CACHE_TYPE_MEMCACHE 
));
$dc->open();

$result=nl_asset::update_virtual_category_kv(
		$dc,
		$asset_id,
		$category_id,
		'nns_labels',
		$labels_id
	);
if ($result===TRUE){
	echo "<script>alert('".cms_get_lang('update_success')."');</script>";
}else{
	echo "<script>alert('".cms_get_lang('update_fault')."');</script>";
}
echo "<script>window.parent.refresh_tree_content();</script>";
?>
