<?php
header("Content-Type:text/html; charset=UTF-8");
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include ($nncms_config_path. "nn_cms_config/nn_cms_config_path.php");
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";

$checkpri=new nns_db_pri_class();
if (!$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111101")){
	Header("Location: ../nncms_content_wrong.php");
}
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
 $checkpri=null;
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
require_once $nncms_db_path. "nns_metadata".DIRECTORY_SEPARATOR . "nns_metadata_class.php";

$asset_id=$_GET["asset_id"];
$metadata_inst=new nns_metadata_class();
$params=array();
$params["metadata_id"]=$asset_id;
$params["metadata_type"]="asset";
$result_data=$metadata_inst->nns_metadata_list($params);
if ($result_data["ret"]==0){
	$data=$result_data["data"];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/trim.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript">
function checkForm(formobj){
	_formobj=formobj;
	if (checkInputRules()){
		window.parent.cmsalert.showAlert("<?php echo cms_get_lang('assist|ysjgl')?>","<?php echo cms_get_lang('msg_ask_change');?>",submitForm);
	}
}

function go_delete(id,num){
	$("#metadata_form #action").val("delete");
	$("#metadata_form #nns_id").val(id);
	$("#metadata_form")[0].submit();
}
function go_edit(id,num){
	$(".dis_tr_"+num).hide();
	$(".edit_tr_"+num).show();
}
function complete_edit(id,num){
	$("#metadata_form #action").val("edit");
	var key=$(".edit_tr_"+num).find("#metadata_key").val();
	var value=$(".edit_tr_"+num).find("#metadata_value").val();
	if (key){
		$("#metadata_form #key").val(key);
		$("#metadata_form #value").val(value);
		$("#metadata_form #nns_id").val(id);
		$("#metadata_form")[0].submit();
	}else{
		alert("<?php echo cms_get_lang('field_not_null')?>");
	}
}

function go_add(){
	$(".add_tr").show();
}
function complete_add(){
	$("#metadata_form #action").val("add");
	var key=$(".add_tr").find("#metadata_key").val();
	var value=$(".add_tr").find("#metadata_value").val();
	if (key){
		$("#metadata_form #key").val(key);
		$("#metadata_form #value").val(value);
		$("#metadata_form")[0].submit();
	}else{
		alert("<?php echo cms_get_lang('field_not_null')?>");
	}
}
function cancel_add(){
	$(".add_tr").hide();
}
</script>
</head>

<body>
<div class="content">
	<div class="content_position"><?php echo cms_get_lang('assist_gl');?> > <?php echo cms_get_lang('assist|ysjgl')?></div>
  
    <div class="content_table">
    	
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tbody>
        
           <tr>
                <td class="rightstyle"><?php echo cms_get_lang('assist|id')?>:</td>
                <td><?php echo $asset_id;?>
                </td>
          </tr>
          
          
           <tr>
          	<td colspan="2"   style="padding:0px;"><div class="radiolist"><div class="radiogroup"><h3><?php echo cms_get_lang('metadata_set')?>&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="<?php echo cms_get_lang('metadata_add')?>" onclick="go_add();"  style=" margin-right:20px; margin-top:-3px;"/></h3>
          	
          	</div></div></td>
          </tr>
              <tr>                                                            
                <td colspan="2" style="padding:10px;">
           <div class="content_table formtable">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
        	<tr>
        		 <th><?php echo cms_get_lang('segnumber');?></th> 
                <th><?php echo cms_get_lang('key')?></th>
                <th><?php echo cms_get_lang('value')?></th>
                <th><?php echo cms_get_lang('action')?></th>
            </tr>
        </thead>
        <tbody>
        <?php
        	if ($data!=null){
        	 foreach($data as $item){
        	$num++;
        	?>
              <tr class="dis_tr_<?php echo $num;?>">   
              <td><?php echo $num;?></td>                                                    
                <td><?php echo $item["nns_metadata_key"];?></td>
                <td style="max-width:500px;"><?php echo htmlspecialchars($item["nns_metadata_value"], ENT_QUOTES);?></td>
                <td>
                	<a href="javascript:go_delete('<?php echo $item["nns_id"];?>',<?php echo $num;?>);"><?php echo cms_get_lang('delete')?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                	<a href="javascript:go_edit('<?php echo $item["nns_id"];?>',<?php echo $num;?>);"><?php echo cms_get_lang('edit')?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
              </tr>
              <tr class="edit_tr_<?php echo $num;?>" style="display:none;">
              <td><?php echo $num;?></td>                                                     
                <td><input style="width:120px;" value='<?php echo $item["nns_metadata_key"];?>' name="metadata_key" id="metadata_key"/></td>
                <td><textarea style="width:500px; height:50px;"  name="metadata_value" id="metadata_value"><?php echo htmlspecialchars($item["nns_metadata_value"], ENT_QUOTES);?></textarea></td>
                <td>
                	
                	<a href="javascript:complete_edit('<?php echo $item["nns_id"];?>',<?php echo $num;?>);"><?php echo cms_get_lang('finish|edit')?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
              </tr>
           <?php }}?>
           	<tr class="add_tr" style="display:none;">
           		<td><?php echo $num+1;?></td>                                                    
           	     <td><input  style="width:120px;"  value='' name="metadata_key" id="metadata_key"/></td>
                <td><textarea style="width:500px; height:50px;"   name="metadata_value" id="metadata_value"></textarea></td>
                <td>
                	
                	<a href="javascript:complete_add();"><?php echo cms_get_lang('add')?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                	<a href="javascript:cancel_add();"><?php echo cms_get_lang('cancel')?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
              </tr>
          </tbody>
        </table>
			    </div>
                </td>
                
      		</tr>
           
          </tbody>
        </table>
    </div>
    <div class="controlbtns" style=" border-top:1px solid #cfcfcf;">
         
        <div class="controlbtn back"><a href="nncms_content_assistlist.php"  action="reset"><?php echo cms_get_lang('back');?></a></div>
        
        <div style="clear:both;"></div>
    </div>
    <form id="metadata_form" action="nncms_content_assist_metadata_control.php" method="post"  >
    	<input name="action" id="action" type="hidden" value=""/>
     	<input name="asset_id" id="asset_id" type="hidden" value="<?php echo $asset_id;?>"  />
     	<input name="nns_id" id="nns_id" type="hidden" value=""  />
     	<input name="key" id="key" type="hidden" value=""  />
     	<input name="value" id="value" type="hidden" value=""  />
    </form>
</div>
</body>
</html>