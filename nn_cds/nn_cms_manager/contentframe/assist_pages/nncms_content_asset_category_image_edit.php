<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-11-08
 *
 * To change the temassist for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temassists
 */
// ob_start();
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php";
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"120101");
$checkpri = null;

$asset_id = $_GET['asset_id'];
$category_id = $_GET['category_id'];

//权限跳转
if (!$pri_bool){
	Header("Location: ../nncms_content_wrong.php");exit;
}

//全局变量引入
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");

 include($nncms_db_path. "nns_assist/nns_db_assist_class.php");
 $assist_inst=new nns_db_assist_class();

 $assist_array=$assist_inst->nns_db_assist_info($asset_id);
// var_dump($partner_array);
 if ($assist_array["ret"]!=0){
 	$data=null;
 	echo "<script>alert(". $assist_array["reason"].");</script>";
 }else{
 	$data=$assist_array['data'];
 }
 $assist_inst=null;
 $parent_name=$data[0]["nns_name"];
 $category=$data[0]["nns_category"];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/dtree.js"></script>
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/cms_ajax.js"></script>
<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
<script language="javascript">
	var category_tree='<?php echo str_replace("\n",'',$category);?>';
	var $category_item;
	$(document).ready(function(){
		$category_item=$(category_tree).find("category[id=<?php echo $category_id;?>]");
		$("#category_name").html($category_item.attr("name"));
		
		var len=$(".img_view").length;
		$(".img_view").hide();
		for(var i=0;i<len;i++){
//		alert($category_item.attr("img"+i));
			if ($category_item.attr("img"+i)!='' && $category_item.attr("img"+i)!=undefined){
				$(".img_view:eq("+i+")").show();
				$(".img_view:eq("+i+")").find("img").attr("src","<?php echo  str_replace('***','',pub_func_get_image_url('***'));?>"+$category_item.attr("img"+i));
				$(".img_view:eq("+i+")").find("a").attr("href","<?php echo str_replace('***','',pub_func_get_image_url('***'));?>"+$category_item.attr("img"+i));
			}
		}
	});
</script>
</head>

<body>
<div class="content">
	<!--<div class="content_position"><?php echo cms_get_lang('dbgl');?> > <?php echo cms_get_lang('media_bjhb');?></div>-->
    <form action="nncms_content_asset_category_image_control.php" id="add_form" method="post" enctype="multipart/form-data">
    <input name="asset_id" id="asset_id" type="hidden" value="<?php echo $asset_id;?>"/>
    <input name="category_id" id="category_id" type="hidden" value="<?php echo $category_id;?>"/>
    <div class="content_table">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        	 <tr>                                                            
                                                                    
                <td width="120"><?php echo cms_get_lang('lmgl_nrlmid')?></td>
                <td><?php echo $category_id;?></td>
                 <td width="120"><?php echo cms_get_lang('lmgl_lmmc')?></td>
                <td id="category_name"></td>
      		</tr>
      		
      		
      		<tr>                                                            
                <td width="120"><?php echo cms_get_lang('media_bjhb')."(".cms_get_lang('media_big').")";?></td>
                <td colspan="3">
                	<div class="img_content">
                    	<div class="img_view">
                    	<h4><?php echo cms_get_lang('media_bjhb')."(".cms_get_lang('media_big').")".cms_get_lang('media_slt');?></h4>
                    	<a href="" target="_blank">
                    	<img src="" style="border:none;" />
                    	</a>
                    	<h4 class="resolution"></h4>
                    	</div>
                    	
                		<div class="img_edit"> <input name="image0" id="image0" type="file" size="60%"/></div>
                		<div style="clear:both;"/>
                	</div>
                
               </td>
                
      		</tr>
      		<tr>                                                            
                <td width="120"><?php echo cms_get_lang('media_bjhb')."(".cms_get_lang('media_normal').")";?></td>
                <td colspan="3">
                	<div class="img_content">
                	
                    	<div class="img_view"><h4><?php echo cms_get_lang('media_bjhb')."(".cms_get_lang('media_normal').")".cms_get_lang('media_slt');?></h4>
                    	<a href="" target="_blank">
                    	<img src="" style="border:none;" /></a><h4 class="resolution"></h4></div>
                
                		<div class="img_edit"> <input name="image1" id="image1" type="file" size="60%"/></div>
                		<div style="clear:both;"/>
                	</div>
                
               </td>
                
      		</tr>
      		<tr>                                                            
                <td width="120"><?php echo cms_get_lang('media_bjhb')."(".cms_get_lang('media_small').")";?></td>
                <td colspan="3">
                	<div class="img_content">
                	
                    	<div class="img_view"><h4><?php echo cms_get_lang('media_bjhb')."(".cms_get_lang('media_small').")".cms_get_lang('media_slt');?></h4>
                    	<a href="" target="_blank">
                    	<img src="" style="border:none;" /></a><h4 class="resolution"></h4></div>
                	
                		<div class="img_edit"> <input name="image2" id="image2" type="file" size="60%"/></div>
                		<div style="clear:both;"/>
                	</div>
                
               </td>
                
      		</tr> 
      		
      		
      		
          </tbody>
        </table>
    </div>
    </form>
     <div class="controlbtns">
    	<div class="controlbtn edit"><a href="javascript:checkForm('<?php echo cms_get_lang('dbgl');?>','<?php echo cms_get_lang('msg_ask_change'); ?>',$('#add_form'));"><?php echo cms_get_lang('confirm'); ?></a></div>
        <div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        <div style="clear:both;"></div>
    </div>
</div>





</body>
</html>

<script language="javascript" src="../../js/image_loaded_func.js"></script>