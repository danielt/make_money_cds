<?php

header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temassist for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temassists
 */
// ob_start();
include("../../nncms_manager_inc.php");
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include $nncms_config_path . "nn_cms_config/nn_cms_global.php";
// 导入多语言包
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/languages/language_contant.php";

//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "111103");
$checkpri = null;

$nns_org_id = $_SESSION["nns_org_id"];
$nns_org_type = $_SESSION["nns_manager_type"];
$nns_id = $_GET["nns_id"];
if (!$pri_bool) {
        Header("Location: ../nncms_content_wrong.php");
        exit;
}
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
include($nncms_db_path . "nns_assist/nns_db_assist_class.php");

$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        )
);
//连接数据库
$dc->open();
include LOGIC_DIR . "/buss/buss_map.class.php";

if (isset($_GET['idarr'])) {
        $data = array(
            'nns_buss_id' => $_GET['b'],
            'nns_ext_type' => $_GET['a'],
            'nns_ext_id' => $_GET['c'],
        );
        @nl_buss_map::delete_buss_map($dc, $data);
        exit;
}

$params = array();
$params['nns_ext_id'] = $_GET['name'];
$type_name = array(
    'asset_include' => '正常绑定',
    'asset_except' => '反向绑定',
);
$List = nl_buss_map::get_buss_map_list($dc, $params);

$List = isset($List['data']['list']) ? $List['data']['list'] : array(array());
$html = '        <div class="content_table"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><th>业务号ID</th><th>业务号名称</th><th>绑定类型</th><th>操作</th></tr>';
foreach ($List as $key => $name) {
        $bus_list = nl_buss::get_buss_list($dc, array('nns_id' => $name['nns_buss_id']));
        $bus_list = $bus_list['data']['result'];
        $bus_name = isset($bus_list[1]['nns_name']) ? $bus_list[1]['nns_name'] : $bus_list[0]['nns_name'];
        $class = $key % 2 ? '' : 'tr_odd';
        $html .="<tr class='$class'><td>{$name['nns_buss_id']}</td><td>{$bus_name}</td><td>{$type_name[$name['nns_ext_type']]}</td><td><a href=javascript:delbuss('{$name['nns_ext_type']}','{$name['nns_buss_id']}','{$name['nns_ext_id']}');>删除</a></td></tr>";
}
//$html .="<tr class='rightstyle'><td></td><td></td><td></td></tr>";
$html .="</table></div>";
if (count($List) < 1)
        $html = '';
exit($html);
// ob_end_clean();
?>