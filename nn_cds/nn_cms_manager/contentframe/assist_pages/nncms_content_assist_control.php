<?php
/*
 * Created on 2012-3-2
 *
 * To change the temassist for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temassists
 */
 @header("Content-Type:text/html;charset=utf-8");
 include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
require_once $nncms_config_path. "nn_cms_manager/controls/nncms_controls_clear_cache.php";

 $action=$_POST["action"];
 $nns_id=$_POST["nns_id"];
 $language_types=$_POST["language_types"];


 $nns_org_id=$_SESSION["nns_org_id"];
 $nns_org_type=$_SESSION["nns_manager_type"];

 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
if (!empty($action)){
	switch($action){
		case "edit":
		$action_str = cms_get_lang('edit|assist|lmgl_tjzlm');
		$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111101");
		break;
		case "edit_1":
		$action_str = cms_get_lang('edit|assist');
		$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111101");
		break;
		case "add":
		$action_str=cms_get_lang('add|assist|lmgl_tjzlm');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111100");
		break;
		case "add_1":
		$action_str=cms_get_lang('add_media_package');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111100");
		break;
		case "delete":
		$action_str = cms_get_lang('delete|assist|lmgl_tjzlm');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111102");
		break;
		case "delete_1":
		$action_str = cms_get_lang('delete|assist');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111102");
		break;
		case "up":
		$action_str = cms_get_lang('order|assist|lmgl_tjzlm');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111101");
		break;
		case "down":
		$action_str = cms_get_lang('order|assist|lmgl_tjzlm');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111101");
		break;
		case "video_bind_product":;
		case "bind_product":
		$action_str = cms_get_lang('bdcpb');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"111101");
		break;
		default:
		break;
	}
}
$checkpri=null;




if (!$pri_bool){
	 Header("Location: ../nncms_content_wrong.php");
}else{
	require_once $nncms_db_path. "nns_log".DIRECTORY_SEPARATOR . "nns_db_op_log_class.php";
	$log_inst=new nns_db_op_log_class();

	require_once $nncms_config_path. "nn_cms_manager/controls/nncms_control_memcache.php";

	require_once $nncms_db_path. "nns_debug_log".DIRECTORY_SEPARATOR . "nns_debug_log_class.php";

	require_once $nncms_db_path."nns_category_backup".DIRECTORY_SEPARATOR."nns_category_backup_class.php";

	$nns_debug_log=new nns_debug_log_class();
	$nns_debug_log->setlogpath("treexml");

	$nns_category_backup_inst=new nns_category_backup_class();

	$nns_category_detail_name=$_POST["nns_category_detail_name"];
	$nns_category_detail_id=$_POST["nns_category_detail_id"];
	$category_parent=$_POST["category_parent"];
	$baseid=$_POST["baseid"];
	$assist_name=$_POST["nns_assist_name"];
	$category_id=$_POST["nns_category_id"];
	$category_name=$_POST["nns_category_name"];
	$assist_id=$_POST["assistid"];
	
	include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
	include($nncms_db_path. "nns_assist/nns_db_assist_class.php");
	 $assist_inst=new nns_db_assist_class();

	 $assist_array=$assist_inst->nns_db_assist_info($nns_id);
	// var_dump($partner_array);
	 if ($assist_array["ret"]==0){
		$data=$assist_array["data"][0];
	 }
//	 $assist_inst=null;

	 if ($data["nns_category"]!=""){
		 $assist_content=$data["nns_category"];
	 }else{
		$assist_content="<assist></assist>";
	 }
				$dom = new DOMDocument('1.0', 'utf-8');
				$dom->loadXML($assist_content);
				$category = $dom->getElementsByTagName('category');
				$nns_debug_log->setlog($assist_content,"asset_tree");
	switch($action){
		case "edit":
		$nns_category_backup_inst->backup_categoty($assist_content,"asset",$nns_id,$nns_org_type,$nns_org_id);
		if ($data!=null){

				 foreach($category as $item){

					if ($item->getAttribute("id")===$category_id){
						$nns_category_detail_name=str_replace("<","",$nns_category_detail_name);
						$nns_category_detail_name=str_replace("/>","",$nns_category_detail_name);
						$nns_category_detail_name=str_replace("\"","",$nns_category_detail_name);
						$nns_category_detail_name=str_replace("'","",$nns_category_detail_name);
						$item->setAttribute("name",$nns_category_detail_name);
						 if (!empty($language_types)){
							$arr=explode("|",$language_types);
							
							for($i=0; $i<count($arr); $i++) {
							 $item->setAttribute($arr[$i],$_POST[$arr[$i]]);	
							}
						 }
						break;
					}
				 }
				 $xml_data=$dom->saveXML();
				 $result=$assist_inst->nns_db_assist_content_modify($nns_id,$xml_data);

		}

		if ($result["ret"]!=0){
			echo "<script>alert('". $action_str, cms_get_lang('fault') ."');</script>";
		}else{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$category_id,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			$cache_manager_control->clear_cache_media_assets_category($nns_id);

			delete_cache("asset_$nns_id");

			echo "<script>alert('". $action_str. cms_get_lang('success'). "');</script>";
		}
		break;
		case "add_1":
			$result=$assist_inst->nns_db_assist_info($assist_id);
			if (count($result["data"])==0){
				$imgurl = pub_func_image_upload_by_asset($_FILES['assist_image'], $assist_id);
				$img1 =   pub_func_image_upload_by_asset($_FILES['assist_image1'],$assist_id);
				$img2 =   pub_func_image_upload_by_asset($_FILES['assist_image2'],$assist_id);
			$return_id=$assist_inst->nns_db_assist_add($assist_id,$nns_org_id,$nns_org_type,"<?xml version=\"1.0\"?><assist></assist>",$assist_name,$imgurl,$baseid,$img1,$img2);
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],"add",$_SESSION["nns_mgr_name"].$action_str.":".$assist_id,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);

			delete_cache("assets");

			}else{
				echo "<script>alert('".cms_get_lang('msg_10')."');self.location='nncms_content_assist_add.php?action=add&nns_id=$nns_id';</script>";
			}

		break;
		case "edit_1":
			$imgurl = pub_func_image_upload_by_asset($_FILES['assist_image'], $assist_id);
			$imgurl1 = pub_func_image_upload_by_asset($_FILES['assist_image1'], $assist_id);
			$imgurl2 = pub_func_image_upload_by_asset($_FILES['assist_image2'], $assist_id);
			
			$result=$assist_inst->nns_db_assist_info($assist_id);

			if (!empty($imgurl)){
				pub_func_image_delete($result['data'][0]['nns_image']);
			}else{
				$imgurl=$result['data'][0]['nns_image'];
			}
			if (!empty($imgurl1)){
				pub_func_image_delete($result['data'][0]['nns_image1']);
			}else{
				$imgurl1=$result['data'][0]['nns_image1'];
			}
			if (!empty($imgurl2)){
				pub_func_image_delete($result['data'][0]['nns_image2']);
			}else{
				$imgurl2=$result['data'][0]['nns_image2'];
			}


			//生成GUID


			$result=$assist_inst->nns_db_assist_modify($nns_id,$nns_org_id,$nns_org_type,$assist_name,$baseid,$imgurl,$imgurl1,$imgurl2);
			if ($result["ret"]!=0){
				echo "<script>alert('". $action_str. cms_get_lang('fault'). "');</script>";
			}else{
				$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],"edit",$_SESSION["nns_mgr_name"].$action_str.":".$nns_id,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
				delete_cache("assets");
				echo "<script>alert('". $action_str. cms_get_lang('success'). "');</script>";
			}
		break;
		case "add":

				$bool=true;
				/**
				 * 是否虚栏目 BY S67
				 */
				$virtual_category=$_POST["virtual_category"];
				
				$nns_category_backup_inst->backup_categoty($assist_content,"asset",$nns_id,$nns_org_type,$nns_org_id);
				if ($category_parent!=0){
					$nns_category_detail_id=$category_parent. $nns_category_detail_id;
				 foreach($category as $item){
					if ($item->getAttribute("id")===$nns_category_detail_id){
						echo "<script>alert('".cms_get_lang('msg_10')."');</script>";
						$bool=false;
						break;
					}
				 }
				 if ($bool){
					 foreach($category as $item){
						if ($item->getAttribute("id")===$category_parent){
							$item_new = $dom->createElement("category");
							$item_new->setAttribute("id",$nns_category_detail_id);
							$nns_category_detail_name=str_replace("<","",$nns_category_detail_name);
							$nns_category_detail_name=str_replace("/>","",$nns_category_detail_name);
							$nns_category_detail_name=str_replace("\"","",$nns_category_detail_name);
							$nns_category_detail_name=str_replace("'","",$nns_category_detail_name);
							$item_new->setAttribute("name",$nns_category_detail_name);
							$item_new->setAttribute("parent",$category_parent);
//		        	 		虚栏目为	1 非虚栏目为空或0
							$item_new->setAttribute("virtual",$virtual_category);
							if (!empty($language_types)){
								$arr=explode("|",$language_types);
								
								for($i=0; $i<count($arr); $i++) {
								 $item_new->setAttribute($arr[$i],$_POST[$arr[$i]]);	
								}
							}
							$item->appendChild($item_new);
						
							break;
						}
					 }

				 }else break;
			}else{
				$nns_category_detail_id=$baseid. $nns_category_detail_id;
				foreach($category as $item){
					if ($item->getAttribute("id")===$nns_category_detail_id){
						echo "<script>alert('".cms_get_lang('msg_10')."');</script>";
						$bool=false;
						break;
					}
				 }
				 if ($bool){
					$item_new = $dom->createElement("category");
					$item_new->setAttribute("id",$nns_category_detail_id);
					$item_new->setAttribute("name",$nns_category_detail_name);
					$item_new->setAttribute("parent",$category_parent);
		//		    虚栏目为	1 非虚栏目为空或0
					$item_new->setAttribute("virtual",$virtual_category);
					if (!empty($language_types)){
						$arr=explode("|",$language_types);
								
						for($i=0; $i<count($arr); $i++) {
						$item_new->setAttribute($arr[$i],$_POST[$arr[$i]]);	
						}
					 }
					$dom->firstChild->appendChild($item_new);
				 }else break;

			}
			
			$xml_data=$dom->saveXML();
			$result=$assist_inst->nns_db_assist_content_modify($nns_id,$xml_data);


		if ($result["ret"]!=0){
			echo "<script>alert('". $action_str. cms_get_lang('fault'). "');</script>";
		}else{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$nns_category_detail_id,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);

			delete_cache("asset_$nns_id");

			echo "<script>alert('". $action_str. cms_get_lang('success'). "');</script>";
		}
		break;
		case "delete":
		if ($data!=null){
				$bool=true;
				$nns_category_backup_inst->backup_categoty($assist_content,"asset",$nns_id,$nns_org_type,$nns_org_id);
				 foreach($category as $item){
					if ($item->getAttribute("id")===$category_id){
						require_once $nncms_db_path. "nns_assist/nns_db_assist_item_class.php";
						$asset_item_inst=new nns_db_assist_item_class();
						$count_arr=$asset_item_inst->nns_db_assist_item_count(null,$nns_id,null,null,null,null,$category_id);
						$asset_item_inst=null;
						if ($count_arr["data"][0]["num"]>0){
							$bool=false;
						}else{
							$item->parentNode->removeChild($item);
							$xml_data=$dom->saveXML();
							$result=$assist_inst->nns_db_assist_content_modify($nns_id,$xml_data);
						}
						break;
					}
				 }


		}

		if (!$bool){
			if (empty($count_arr)){
				echo "<script>alert('". $action_str. cms_get_lang('fault'). "');</script>";
			}else{
				echo "<script>alert('". $action_str. cms_get_lang('fault').",".cms_get_lang('msg_12') ."');</script>";
			}
		}else{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$category_id,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			delete_cache("asset_$nns_id");
			echo "<script>alert('". $action_str. cms_get_lang('success'). "');</script>";
		}
		break;
		case "delete_1":
		$delete_bool=true;
	//	$role_inst->
			$nns_ids=explode(",",$nns_id);
			$num=0;
			foreach($nns_ids as $nnsid){
				if (!empty($nnsid)){
					$num++;
					require_once $nncms_db_path. "nns_assist/nns_db_assist_item_class.php";
					$asset_item_inst=new nns_db_assist_item_class();
					$count_arr=$asset_item_inst->nns_db_assist_item_count(null,$nnsid);
					$asset_item_inst=null;
					if ($count_arr["data"][0]["num"]>0){
						$delete_bool=false;
						break;
					}else{
						$resultarr=$assist_inst->nns_db_assist_delete($nnsid);
					}
					if ($resultarr["ret"]!=0 && $num==1){
						$delete_bool=false;
						break;
					}
				}
			}
		if (!$delete_bool){
			if ($count_arr["data"][0]["num"]==0){
				echo "<script>alert('". $action_str.  cms_get_lang('fault'). "');</script>";
			}else{
				echo "<script>alert('". $action_str.cms_get_lang('fault').",".cms_get_lang('msg_12') ."');</script>";
			}
		}else{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],"delete",$_SESSION["nns_mgr_name"].$action_str.":".rtrim($nns_id,','),$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			delete_cache("assets");
			echo "<script>alert('". $action_str.  cms_get_lang('success'). "');</script>";
		}
		break;
		case "up":
		$nns_category_backup_inst->backup_categoty($assist_content,"asset",$nns_id,$nns_org_type,$nns_org_id);
			if ($data!=null){
				$bool=true;

				$count_category=$category->length;
				$true_index=0;
				for ($index=0;$index<$count_category;$index++){
					$value=$category->item($index)->getAttribute("id");
					if ($value===$category_id){
						$true_index=$index;
						break;
					}
				}
				if ($true_index>0){
					$item=$category->item($true_index);
					$true_index_before=$true_index-1;
					while ($true_index_before>=0){
						$item_before=$category->item($true_index_before);
						if ($item_before->getAttribute("parent")===$category->item($true_index)->getAttribute("parent")){
							if ($true_index_before>=0){
								$item->parentNode->removeChild($item);
								$item_before->parentNode->insertBefore($item,$item_before);
								$xml_data=$dom->saveXML();
								$result=$assist_inst->nns_db_assist_content_modify($nns_id,$xml_data);
								$bool=true;
								break;
							}
						}else{
							$bool=false;
						}
						$true_index_before--;
					}
				}else $bool=false;
		}

		if (!$bool){
			echo "<script>alert('". $action_str. cms_get_lang('fault'). "');</script>";
		}else{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],"order",$_SESSION["nns_mgr_name"].$action_str.":".$category_id,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			delete_cache("asset_$nns_id");
			echo "<script>alert('". $action_str. cms_get_lang('success'). "');</script>";
		}
		break;
		case "down":
		$nns_category_backup_inst->backup_categoty($assist_content,"asset",$nns_id,$nns_org_type,$nns_org_id);
			if ($data!=null){
				$bool=true;

				$count_category=$category->length;
				$true_index=0;
				for ($index=0;$index<$count_category;$index++){
					$value=$category->item($index)->getAttribute("id");
					if ($value===$category_id){
						$true_index=$index;
						break;
					}
				}
				if ($true_index<$count_category-1){
					$item=$category->item($true_index);
					$true_index_after=$true_index+1;
					while ($true_index_after<$count_category){
						$item_after=$category->item($true_index_after);
						if ($item_after->getAttribute("parent")===$category->item($true_index)->getAttribute("parent")){
							if ($true_index_after<$count_category){
								$item_after->parentNode->removeChild($item_after);
								$item->parentNode->insertBefore($item_after,$item);
								$xml_data=$dom->saveXML();
								$result=$assist_inst->nns_db_assist_content_modify($nns_id,$xml_data);
								$bool=true;
								break;
							}
						}else $bool=false;
						$true_index_after++;
					}

				}else $bool=false;
		}

		if (!$bool){
			echo "<script>alert('". $action_str.cms_get_lang('fault'). "');</script>";
		}else{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],"order",$_SESSION["nns_mgr_name"].$action_str.":".$category_id,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			delete_cache("asset_$nns_id");
			echo "<script>alert('". $action_str. cms_get_lang('success'). "');</script>";
		}
		break;
		case "bind_product"://绑定普通包
			//echo 'product_id:'.$_POST['nns_product_id'];exit;
			require_once $nncms_db_path. "nns_products".DIRECTORY_SEPARATOR . "nns_db_product_bind_content_class.php";
			$product_inst = new nns_db_product_bind_content_class();
			$result = $product_inst->assist_bind_product($_POST['nns_product_id'],$nns_id,$_POST['nns_category_id'],$category_name);
			if ($result["ret"]!=0){
				echo "<script>alert('". $action_str. cms_get_lang('fault'). "');</script>";
			}
		break;
		case "video_bind_product"://绑定单片包
			require_once $nncms_db_path. "nns_products".DIRECTORY_SEPARATOR . "nns_db_product_video_class.php";
			$product_inst = new nns_db_product_video_class();
			$result = $product_inst->video_bind_product($_POST['nns_product_id'],$_POST['nns_product_fee_id'],$nns_id,$_POST['nns_category_id']);
			if ($result["ret"]!=0){
				echo "<script>alert('". $action_str. cms_get_lang('fault'). "');</script>";
			}
			//echo 'product_id:'.$_POST['nns_product_id'],'fee:',$_POST['nns_product_fee_id'];exit;
		break;
		default:

		break;
	}
	//var_dump($result);
	$assist_inst=null;
	$log_inst=null;
	if ($action=="add_1"){
		echo "<script>self.location='nncms_content_assist_edit.php?nns_id=$return_id';</script>";
	}else if($action=="delete_1"){
		echo "<script>self.location='nncms_content_assistlist.php';</script>";
	}else{
		echo "<script>self.location='nncms_content_assist_edit.php?nns_id=$nns_id';</script>";
	}
}
?>