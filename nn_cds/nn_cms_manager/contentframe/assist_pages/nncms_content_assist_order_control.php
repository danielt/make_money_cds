<?php
/*
 * Created on 2012-4-9
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temassist for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temassists
 */
include("../../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
  // 获取cache模块
include LOGIC_DIR.'asset'.DIRECTORY_SEPARATOR.'asset_item.class.php';
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
			));
$dc->open();

include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
require_once $nncms_config_path. "nn_cms_manager/controls/nncms_controls_clear_cache.php";
 $assist_id=$_GET["assist_id"];
 $assist_detail_id=$_GET["assist_detail_id"];
 $nns_id=$_GET["id"];
 $order=$_GET["order"];
 $action=$_GET["action"];
 include($nncms_db_path. "nns_assist/nns_db_assist_item_class.php");
 $assist_item_inst=new nns_db_assist_item_class();
 $action_str=cms_get_lang('order|assist_bdnr');
 require_once $nncms_db_path. "nns_log".DIRECTORY_SEPARATOR . "nns_db_op_log_class.php";
$log_inst=new nns_db_op_log_class();
require_once $nncms_config_path. "nn_cms_manager/controls/nncms_control_memcache.php";
 $video_name=$log_inst->nns_db_get_assest_video_name_arr($nns_id);
  switch($action){
	case "up":
		$result=$assist_item_inst->nns_db_assist_item_order_up($nns_id,$order,$assist_id,$assist_detail_id);
	break;
	case "down":
		$result=$assist_item_inst->nns_db_assist_item_order_down($nns_id,$order,$assist_id,$assist_detail_id);
	break;
	case "next":
		$result=$assist_item_inst->nns_db_assist_item_order_exchange($nns_id,$order,"next",$assist_id,$assist_detail_id);
	break;
	case "pre":
		$result=$assist_item_inst->nns_db_assist_item_order_exchange($nns_id,$order,"pre",$assist_id,$assist_detail_id);
	break;
  }

  if ($result["ret"]==0){
	$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],"order",$_SESSION["nns_mgr_name"].$action_str.":".$video_name,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
	nl_asset_item::delete_asset_item_list_by_child_cache($dc,$assist_detail_id);
	delete_cache("index_".$assist_detail_id."_".$assist_id);
	echo "<script>alert('", cms_get_lang('order|success'),"');</script>";
  }else{
	echo "<script>alert('", cms_get_lang('order|fault'),"');</script>";
  }
  $log_inst=null;
  echo "<script>self.location='nncms_content_assist_item_list.php?assist_id=$assist_id&assist_package_id=$assist_detail_id';</script>";
?>