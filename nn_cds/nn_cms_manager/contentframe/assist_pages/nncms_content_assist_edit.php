<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temassist for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temassists
 */
// ob_start();
include("../../nncms_manager_inc.php");
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include $nncms_config_path . "nn_cms_config/nn_cms_global.php";
// 导入多语言包
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/languages/language_contant.php";

//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "111103");
$checkpri = null;

$nns_org_id = $_SESSION["nns_org_id"];
$nns_org_type = $_SESSION["nns_manager_type"];
$nns_id = $_GET["nns_id"];
if (!$pri_bool) {
        Header("Location: ../nncms_content_wrong.php");
        exit;
}
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
include($nncms_db_path . "nns_assist/nns_db_assist_class.php");
$assist_inst = new nns_db_assist_class();

$assist_array = $assist_inst->nns_db_assist_info($nns_id);
// var_dump($partner_array);
if ($assist_array["ret"] != 0) {
        $data = null;
        echo "<script>alert(" . $assist_array["reason"] . ");</script>";
}
$assist_inst = null;
$data = $assist_array["data"];
$assist_id = $data[0]["nns_category_root_id"];
$assist_name = $data[0]["nns_name"];
$nns_category = $data[0]["nns_category"];


$language_types = g_cms_config::get_g_extend_language();
$language_types = trim($language_types);
if (empty($language_types)) {
        $type_array = array();
} else {
        $type_array = explode("|", $language_types);
}
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        )
);
//连接数据库
$dc->open();
include LOGIC_DIR . "/buss/buss_map.class.php";
$List = nl_buss_map::get_buss_map_list($dc, $params);
// ob_end_clean();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                <link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
                <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../../js/dtree.js"></script>
                <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../../js/table.js.php"></script>
                <script language="javascript" src="../../js/checkinput.js.php"></script>
                <script language="javascript" src="../../js/cms_ajax.js"></script>
                <script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
                <script language="javascript" src="../../js/category_count.js"></script>
                <script language="javascript">
                        var xml_str = '<?php
$content = str_replace("\n", "", $data[0]["nns_category"]);
echo $content;
?>';
                        var item_value;
                        var item_category_name;
                        $(document).ready(function() {
<!-- 2013-1-15 bo.chen@starcorcn.com  媒资包 分类下面的影片数量 start -->
                                category.count('assist',<?php echo '\'' . $data[0]['nns_id'] . '\'' ?>);
<!-- 2013-1-15 bo.chen@starcorcn.com  媒资包 分类下面的影片数量 end -->
                                $(".category_editbox").width($(".content").width() - 400);
                                $(window).resize(function() {
                                        $(".category_editbox").width($(".content").width() - 400);
                                });
                                $(".selectbox").hide();
                                $(".selectbox").css("left", ($("body").width() - $(".selectbox").width()) / 2);
                                $(".selectbox").css("top", 10);

                        });

                        function close_select() {
                                $(".selectbox").hide();
                        }
                        function select_tree_item(item_id, item_name, parent_id, item_category) {
                                item_category_name = item_name;
                                if (item_id ==<?php echo $assist_id ?>) {
                                        item_value = 0;
                                        $(".category_edit_box").find("#nns_category_detail_name").attr("disabled", true);
<?php
foreach ($type_array as $lang_extend) {
        ?>
                                                $("#<?php echo $lang_extend; ?>").val('');
                                                $("#<?php echo $lang_extend; ?>").attr("disabled", true);
<?php } ?>
                                        $(".category_edit_box").find("input[type='button']:eq(0)").attr("disabled", true);
                                        $(".category_edit_box").find("input[type='button']:eq(1)").attr("disabled", true);
                                        $(".category_edit_box").find("input[type='button']:eq(2)").attr("disabled", true);
                                        $(".category_edit_box").find("input[type='button']:eq(3)").attr("disabled", true);
                                        $(".category_edit_box").find("input[type='button']:eq(6)").attr("disabled", true);
                                } else {
                                        item_value = item_id;
<?php
foreach ($type_array as $lang_extend) {
        ?>
                                                $("#<?php echo $lang_extend; ?>").attr("disabled", false);
<?php } ?>
                                        $(".category_edit_box").find("#nns_category_detail_name").attr("disabled", false);
                                        $(".category_edit_box").find("input[type='button']:eq(0)").attr("disabled", false);
                                        $(".category_edit_box").find("input[type='button']:eq(1)").attr("disabled", false);
                                        $(".category_edit_box").find("input[type='button']:eq(2)").attr("disabled", false);
                                        $(".category_edit_box").find("input[type='button']:eq(3)").attr("disabled", false);
                                        $(".category_edit_box").find("input[type='button']:eq(6)").attr("disabled", false);
                                }

                                $(".category_edit_box").show();
                                $(".category_edit_box").find("#category_parent").val(parent_id);
                                $(".category_edit_box").find("#nns_category_id").val(item_value);
                                $(".category_add_box").find("#category_parent").val(item_value);
                                $(".category_edit_box").find(".category_edit_id").html(item_id);
                                $(".category_edit_box").find("#nns_category_detail_name").val(item_name);
                                var len = $(xml_str).find("category").length;
                                for (var i = 0; i < len; i++) {
                                        if ($(xml_str).find("category:eq(" + i + ")").attr("id") === item_id) {
<?php
foreach ($type_array as $lang_extend) {
        ?>
                                                        $("#<?php echo $lang_extend; ?>").val($(xml_str).find("category:eq(" + i + ")").attr('<?php echo $lang_extend; ?>'));
<?php } ?>
                                                break;
                                        }
                                }

                                $.get('nncms_content_assist_buss.php?name=<?php echo $_GET['nns_id'] ?>|' + item_id, function(html) {
                                        $('#video_buss').html(html);
                                });
                                if (item_category == 1) {
                                        $('.shuju').show();
                                } else {
                                        $('.shuju').hide();
                                }
                                $.get('./nncms_content_assist_bind.php?get=true&a=<?php echo $_GET['nns_id'] ?>&b=' + item_id, function(html) {
                                        $('#video_assets').html(html);
                                });
                                $(".category_add_box").hide();
                                set_ajax_params("search");
                                set_ajax_params("get_bind_product");
                        }
                        function  delassets(nns_id) {
                                $.get('./nncms_content_assist_bind.php?del=true&nns_id=' + nns_id, function(type) {
                                        if (type == '1') {
                                                alert('删除成功');
                                                window.location.reload();
                                        } else {
                                                alert('删除失败');
                                        }
                                });

                        }
                        function set_ajax_params(action, video_package_id, video_category_id, video_category_name) {
                                var get_url;
                                switch (action) {
                                        case "add":
                                                get_url = "action=" + action + "&assest_id=<?php echo $nns_id; ?>&assest_category_id=" + item_value + "&video_package_id=" + video_package_id + "&video_category_id=" + video_category_id + "&assest_category_name=" + encodeURIComponent(item_category_name) + "&video_category_name=" + encodeURIComponent(video_category_name);
                                                getAjax("../../controls/nncms_controls_ajax_assest_category.php?" + get_url, get_ajax_success);
                                                break;
                                        case "delete":

                                                get_url = "action=" + action + "&assest_id=<?php echo $nns_id; ?>&assest_category_id=" + item_value + "&video_package_id=" + video_package_id + "&video_category_id=" + video_category_id;
                                                getAjax("../../controls/nncms_controls_ajax_assest_category.php?" + get_url, get_ajax_success);
                                                break;
                                        case "search":

                                                get_url = "action=" + action + "&assest_id=<?php echo $nns_id; ?>&assest_category_id=" + item_value + "&rad=" + Math.random();
                                                getAjax("../../controls/nncms_controls_ajax_assest_category.php?" + get_url, get_ajax_result);
                                                break;
                                        case "get_bind_product":
                                                get_url = "action=" + action + "&assest_id=<?php echo $nns_id; ?>&assest_category_id=" + item_value + "&rad=" + Math.random();
                                                getAjax("../../controls/nncms_controls_ajax_assest_category.php?" + get_url, get_bind_product_ajax_result);
                                                break;
                                }
                        }
                        function get_bind_product_ajax_result(result) {
                                if (result) {
                                        var html_str = "";
                                        if (result.product_content) {
                                                var product_content = result.product_content;
                                                html_str = "<div class=\"block_category\">" + product_content['nns_name'] + "&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"javascript:delete_content_bind_product('" + product_content['nns_id'] + "');\"><img src=\"../../images/topicon_08-topicon.png\"  border=\"0\"/></a></div>";
                                                $("#bind_product").html(html_str);
                                        } else {
                                                $("#bind_product").html("");
                                        }
                                        if (result.product_video) {
                                                var product_video = result.product_video;
                                                html_str = "<div class=\"block_category\">&nbsp;&nbsp;" + product_video['nns_product_fee_name'] + "(" + product_video['nns_price'] + ")&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"javascript:delete_video_bind_product('" + product_video['nns_id'] + "');\"><img src=\"../../images/topicon_08-topicon.png\"  border=\"0\"/></a></div>";
                                                //var html_str += "<div class=\"block_category\">"+result.nns_name+"&nbsp;&nbsp;&nbsp;&nbsp;</div>";
                                                $("#video_bind_product").html(html_str);
                                        } else {
                                                $("#video_bind_product").html("");
                                        }


                                } else {
                                        $("#bind_product").html("");
                                }

                        }
                        function delete_content_bind_product(nns_product_content_id) {
                                get_url = "action=delete_product_content&nns_id=" + nns_product_content_id + "&rad=" + Math.random();
                                getAjax("../../controls/nncms_controls_ajax_assest_category.php?" + get_url, delete_bind_product_ajax_result);
                        }
                        function delete_video_bind_product(nns_product_video_id) {
                                get_url = "action=delete_product_video&nns_id=" + nns_product_video_id + "&rad=" + Math.random();
                                getAjax("../../controls/nncms_controls_ajax_assest_category.php?" + get_url, delete_bind_product_ajax_result);
                        }
                        function delete_bind_product_ajax_result(result) {
                                if (result.ret == 0) {
                                        set_ajax_params("get_bind_product");
                                }
                        }
                        function get_ajax_result(result) {
                                var html_str = "";
                                var len = result.data.length;
                                for (var i = 0; i < len; i++) {
                                        html_str += "<div class=\"block_category\">" + result.data[i]["nns_video_category_name"] + "&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"javascript:set_ajax_params('delete','" + result.data[i]["nns_video_package_id"] + "','" + result.data[i]["nns_video_category_id"] + "');\"><img src=\"../../images/topicon_08-topicon.png\"  border=\"0\"/></a></div>";
                                }
                                if (html_str == "") {
                                        $(".category_edit_box").find("input[type='button']:eq(5)").attr("disabled", false);
                                } else {
                                        $(".category_edit_box").find("input[type='button']:eq(5)").attr("disabled", true);
                                }
                                $("#bind_category_list").html(html_str);
                        }

                        function get_ajax_success(result) {
                                if (result.ret == 0) {
                                        set_ajax_params("search");
                                }
                        }

                        function add_category_command() {
                                if ($(".category_add_box").is(":hidden")) {
                                        $(".category_add_box").show();
                                        //	$(".category_add_box").find("#category_parent").val($(".category_edit_box").find("#nns_category_id").val());
                                        $(".category_add_box").find(".parent_category_box").html($(".category_edit_box").find("#nns_category_detail_name").val());
                                } else {
                                        $(".category_add_box").hide();
                                }
                                window.parent.resetFrameHeight();
                        }

                        function order_node(action_str) {

                                $(".category_edit_box").find("#action").val(action_str);
                                checkForm('<?php echo cms_get_lang('lmgl'); ?>', '<?php echo cms_get_lang('msg_ask_change'); ?>', $('#edit_form'), "edit");
                        }

                        function edit_node() {

                                $(".category_edit_box").find("#action").val("edit");
                                checkForm('<?php echo cms_get_lang('assist_gl'); ?>', '<?php echo cms_get_lang('msg_ask_change'); ?>', $('#edit_form'), "edit");
                        }
                        function delete_node() {

                                $(".category_edit_box").find("#action").val("delete");
                                checkForm('<?php echo cms_get_lang('assist_gl'); ?>', '<?php echo cms_get_lang('msg_ask_change'); ?>', $('#edit_form'), "delete");
                        }

                        function set_move_category_id(value, depot_id, category_name) {
                                set_ajax_params("add", depot_id, value, category_name);
                                close_select();
                        }
                        function begin_select_category() {
                                $("#select_frame").attr("src", "../../controls/video_category_select/nncms_controls_video_category_select.php?method=vod&org_id=<?php echo $nns_org_id; ?>&org_type=<?php echo $nns_org_type; ?>");
                                $(".selectbox").show();
                        }
                        function begin_select_product() {
                                $("#select_frame").attr("src", "../../controls/nncms_controls_select.php?method=common_product");
                                $(".selectbox").show();
                        }
                        function set_common_product_id(product_id, nns_single_video_bill_mode) {
                                /********
                                 if(nns_single_video_bill_mode==1){//绑定单片包
                                 $("#nns_product_id").attr("value",product_id);
                                 $("#select_frame").attr("src","../../controls/nncms_controls_select.php?method=product_fee&product_id="+product_id+"");
                                 $(".selectbox").show();
                                 }else{//绑定普通包
                                 *********/
                                $('#nns_product_id').val(product_id);
                                //$('#nns_product_fee_id').val(product_fee_id);
                                $('#nns_category_name').val(item_category_name);
                                $('#action').val('bind_product');
                                checkForm('<?php echo cms_get_lang('assist|lmgl') ?>', '<?php echo cms_get_lang('topic_bind_is') ?>', $('#edit_form'), "bind_product");
                                //}
                        }
                        function begin_select_video_product() {
                                $("#select_frame").attr("src", "../../controls/nncms_controls_select.php?method=product_fee&video_bind_product=1");
                                $(".selectbox").show();
                        }

                        function begin_select_video_buss() {
                                var category_edit_id = $('.category_edit_id').text();
                                $("#select_frame").attr("src", "../../mgtv/epg_v1/nns_mgtv_addbuss_mzb.php?i=<?php echo $_GET['nns_id'] ?>|" + category_edit_id).css('width', '400px');
                                $(".selectbox").show();
                                window.parent.resetFrameHeight();
                        }

                        function set_product_fee_id(value, name) {
                                $("#nns_product_fee_id").attr("value", value);
                                $('#action').val('video_bind_product');
                                checkForm('<?php echo cms_get_lang('assist|lmgl') ?>', '<?php echo cms_get_lang('sfbddpjfcpb') ?>', $('#edit_form'), 'video_bind_product');
                        }

                        function add_category_image() {
                                window.location.href = "nncms_content_asset_category_image_edit.php?asset_id=<?php echo $nns_id; ?>&category_id=" + item_value;
                        }


                </script>
        </head>

        <body>
                <div class="selectbox">
                        <iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
                </div>
                <div class="content">
                        <div class="content_position"><?php echo cms_get_lang('assist_gl'); ?> > <?php echo cms_get_lang('lmgl'); ?></div>
                        <div style="background-color:#FFF;">
                                <div class="category_tree" style="min-height:600px;">
                                        <?php
                                        if ($data != null) {
                                                $assist_content = $data[0]["nns_category"];
                                        } else {
                                                $assist_content = null;
                                        }
                                        echo pub_func_get_tree_by_html($assist_content, $data[0]["nns_name"], $assist_id);
                                        ?>


                                </div>
                                <div class="category_editbox" >
                                        <div class="category_edit_box">
                                                <form id="edit_form" action="nncms_content_assist_control.php" method="post">
                                                        <input name="action" id="action" type="hidden" value="" />
                                                        <input name="category_parent" id="category_parent" type="hidden" value="" />
                                                        <input name="nns_assist_name" id="nns_assist_name" type="hidden" value="<?php echo $assist_name; ?>" />
                                                        <input name="nns_id" id="nns_id" type="hidden" value="<?php echo $nns_id; ?>" />
                                                        <input name="nns_category_id" id="nns_category_id" type="hidden" value="" />
                                                        <input name="nns_category_name" id="nns_category_name" type="hidden" value="" />
                                                        <input name="nns_product_id" id="nns_product_id" type="hidden" value="" />
                                                        <input name="nns_product_fee_id" id="nns_product_fee_id" type="hidden" value="" />

                                                        <div class="content_table">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                                <tr>
                                                                                        <td class="rightstyle"><?php echo cms_get_lang('lmgl_nrlmid'); ?></td>
                                                                                        <td class="category_edit_id"></td>

                                                                                </tr>
                                                                                <tr>
                                                                                        <td class="rightstyle"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('lmgl_lmnrmc'); ?></td>
                                                                                        <td><input name="nns_category_detail_name" id="nns_category_detail_name" type="text" rule="noempty"
                                                                                                   value=""
                                                                                                   /></td>

                                                                                </tr>

                                                                                <input name="language_types" id="language_types"  type="hidden"  value="<?php echo $language_types ?>" />
                                                                                <?php for ($i = 0; $i < count($type_array); $i++) { ?>
                                                                                        <tr>
                                                                                                <td class="rightstyle">
                                                                                                        <?php echo get_language_name_by_code($type_array[$i]); ?>
                                                                                                </td>
                                                                                                <td><input name="<?php echo $type_array[$i] ?>" id="<?php echo $type_array[$i] ?>" type="text" value="" /></td>
                                                                                        </tr>
                                                                                <?php } ?>

                                                                                <tr>
                                                                                        <td class="rightstyle"><?php echo cms_get_lang('binded|media_zyklm'); ?></td>
                                                                                        <td id="bind_category_list">

                                                                                        </td>

                                                                                </tr>
                                                                                <?php if ($g_single_video_bill_mode) { ?>
                                                                                        <tr>
                                                                                                <td class="rightstyle"><?php echo cms_get_lang('ybdcpb') ?></td>
                                                                                                <td id="bind_product">

                                                                                                </td>

                                                                                        </tr>
                                                                                        <tr>
                                                                                                <td class="rightstyle"><?php echo cms_get_lang('ybddpzf') ?></td>
                                                                                                <td id="video_bind_product">

                                                                                                </td>

                                                                                        </tr>

                                                                                        <?php
                                                                                }
                                                                                ?>
                                                                                <tr>
                                                                                        <td class="rightstyle">业务号列表</td>
                                                                                        <td id="video_buss">

                                                                                        </td>

                                                                                </tr>
                                                                                <tr>
                                                                                        <td class="rightstyle">绑定数据列表</td>
                                                                                        <td id="video_assets">

                                                                                        </td>

                                                                                </tr>
                                                                                <tr>
                                                                                        <td class="rightstyle"><?php echo cms_get_lang('action'); ?> </td>
                                                                                        <td>
                                                                                                <input name="" type="button" value="<?php echo cms_get_lang('edit'); ?>" onclick="edit_node();"/>&nbsp;&nbsp;
                                                                                                <input name="" type="button" value="<?php echo cms_get_lang('delete'); ?>" onclick="delete_node();"/>&nbsp;&nbsp;
                                                                                                <input name="" type="button" value="<?php echo cms_get_lang('order_pre'); ?>" onclick="order_node('up');"/>&nbsp;&nbsp;
                                                                                                <input name="" type="button" value="<?php echo cms_get_lang('order_next'); ?>" onclick="order_node('down');"/>&nbsp;&nbsp;
                                                                                                <input name="" type="button" value="<?php echo cms_get_lang('add|lmgl_tjzlm'); ?>" onclick="add_category_command();"/>&nbsp;&nbsp;
                                                                                                <input name="" type="button" value="<?php echo cms_get_lang('bind|media_zyklm'); ?>" onclick="begin_select_category();"/>&nbsp;&nbsp;
                                                                                                <input name="" type="button" value="<?php echo cms_get_lang('lmtb') ?>" onclick="add_category_image();"/>&nbsp;&nbsp;
                                                                                                <input name="" type="button" value="<?php echo cms_get_lang('bdcpb') ?>" onclick="begin_select_product();"/>&nbsp;&nbsp;
                                                                                                <input name="" type="button" value="<?php echo cms_get_lang('bddpb') ?>" onclick="begin_select_video_product();"/>&nbsp;&nbsp;
                                                                                                <input name="" type="button" value="绑定业务号" onclick="begin_select_video_buss();"/>&nbsp;&nbsp;
                                                                                                <input name="" type="button" value="绑定数据源" class="shuju"  style="display:none;"  onclick="begin_select_video_info();"/>&nbsp;&nbsp;
                                                                                        </td>
                                                                                </tr>
                                                                        </tbody>
                                                                </table>
                                                        </div>
                                                </form>
                                        </div>
                                        <div class="category_add_box">
                                                <form id="add_form" action="nncms_content_assist_control.php" method="post">
                                                        <input name="action" id="action" type="hidden" value="add" />
                                                        <input name="baseid" id="baseid" type="hidden" value="<?php echo $assist_id; ?>" />
                                                        <input name="nns_id" id="nns_id" type="hidden" value="<?php echo $nns_id; ?>" />
                                                        <input name="nns_assist_name" id="nns_assist_name" type="hidden" value="<?php echo $assist_name; ?>" />
                                                        <input name="category_parent" id="category_parent" type="hidden" value="" />
                                                        <div class="content_table">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                                <tr>
                                                                                        <td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('lmgl_sjlm'); ?> </td>
                                                                                        <td class="parent_category_box">

                                                                                        </td>

                                                                                </tr>
                                                                                <tr>
                                                                                        <td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('lmgl_nrlmid|lmgl_3wszbm'); ?> </td>
                                                                                        <td><input name="nns_category_detail_id" id="nns_category_detail_id" type="text" rule="noempty|int" len="3" style="width:20%" maxlength="3"
                                                                                                   value=""
                                                                                                   />  &nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="virtual_category" value="1" /><?php echo cms_get_lang('sfwxlm') ?></td>

                                                                                </tr>
                                                                                <tr>
                                                                                        <td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('lmgl_lmnrmc'); ?> </td>
                                                                                        <td><input name="nns_category_detail_name" id="nns_category_detail_name" type="text" rule="noempty"
                                                                                                   value=""
                                                                                                   /></td>

                                                                                </tr>

                                                                                <input name="language_types" id="language_types"  type="hidden"  value="<?php echo $language_types ?>" />
                                                                                <?php for ($i = 0; $i < count($type_array); $i++) { ?>
                                                                                        <tr>
                                                                                                <td class="rightstyle">
                                                                                                        <?php echo get_language_name_by_code($type_array[$i]); ?>
                                                                                                </td>
                                                                                                <td><input name="<?php echo $type_array[$i] ?>" id="<?php echo $type_array[$i] ?>" type="text" value="" /></td>
                                                                                        </tr>
                                                                                <?php } ?>

                                                                                <tr>
                                                                                        <td class="rightstyle"><?php echo cms_get_lang('action'); ?></td>
                                                                                        <td>
                                                                                                <input name="" type="button" value="<?php echo cms_get_lang('add'); ?>" onclick="checkForm('<?php echo cms_get_lang('lmgl'); ?>', '<?php echo cms_get_lang('msg_ask_change'); ?>', $('#add_form'), 'add');"/>&nbsp;&nbsp;
                                                                                        </td>

                                                                                </tr>
                                                                        </tbody>
                                                                </table>
                                                        </div>
                                                </form>
                                        </div>
                                </div>
                                <div style="clear:both;"/>
                        </div>
                        <div class="controlbtns" style=" border-top:1px solid #cfcfcf;">

                                <div class="controlbtn back"><a href="nncms_content_assistlist.php"  action="reset"><?php echo cms_get_lang('back'); ?></a></div>

                                <div style="clear:both;"></div>
                        </div>

                </div>
                <script>
                        function begin_select_video_info() {
                                $("#select_frame").attr("src", "../../controls/video_category_select/nncms_controls_package_select_index.php?method=asset&packages=asset").css('width', '400px');
                                $(".selectbox").show();
                        }

                        function  set_package_id(id, method, name, url) {
                                begin_select_category_mzb(id, method);
                        }

                        function set_move_category_id_mzb(value, depot_id, category_name) {
                                $.get('./nncms_content_assist_bind.php?a=<?php echo $_GET['nns_id'] ?>&b=' + $('.category_edit_id').text() + '&c=' + depot_id + '&d=' + value, function(type) {

                                        if (type == '1') {
                                                alert('绑定成功');
                                                window.location.reload();
                                        } else {
                                                alert('已经绑定');
                                        }
                                });
                        }

                        function begin_select_category_mzb(a, b, c) {

                                $("#select_frame").attr("src", "../../controls/video_category_select/nncms_controls_asset_category_select.php?ismzb=true&method=asset&tree_id=" + a);
                                $(".selectbox").show();
                        }

                        function delbuss(a, b, c) {
                                $.get('nncms_content_assist_buss.php?idarr=true&a=' + a + '&b=' + b + '&c=' + c, function() {
                                        alert('删除成功');
                                        window.location.reload();
                                });
                        }
                </script>
        </body>
</html>
