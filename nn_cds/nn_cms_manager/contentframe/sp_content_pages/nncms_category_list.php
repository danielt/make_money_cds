<?php
header('Content-Type:text/html;charset=utf-8');
include_once '../../nncms_manager_inc.php';
include_once $nncms_config_path . 'nn_cms_manager/controls/nncms_controls_check_login.php';
include_once $nncms_config_path . 'nn_cms_manager/controls/nncms_controls_public_function.php';
include_once $nncms_db_path . 'nns_pri/nns_db_pri_class.php';

$checkPri = new nns_db_pri_class();
$hasPri = false;
$hasPri = $checkPri->nns_db_pri_check($_SESSION['nns_role_pris'], '135005');
$checkPri = null;
/*
if ($_GET['nns_org_id'] == '' || $_GET['nns_org_type'] == '') {
    $nnsOrgId = $_SESSION['nns_org_id'];
    $nnsOrgType = $_SESSION['nns_manager_type'];
} else {
    $nnsOrgId = $_GET['nns_org_id'];
    $nnsOrgType = $_GET['nns_org_type'];
}*/
$nnsOrgId = $_GET['sp_id'];
include_once $nncms_config_path . 'mgtv/mgtv_init.php';
//
$nnsOrgType = '';
$action = $_GET['action'];
if (!$hasPri) {
    header('Location: ../nncms_content_wrong.php');
    exit(-1);
}
//include_once $nncms_config_path . 'mgtv/models/asset_category_model.php';
$assetCategory = asset_category_model::get_asset_category_list($nnsOrgId);//get_asset_category(null, array('nns_sort' => ''),$nnsOrgId);
$baseName = '栏目';
$baseId = 10000;
$nns_org_id = $_SESSION["nns_org_id"];
$nns_org_type = $_SESSION["nns_manager_type"];
if($_GET['ajax']=='bind'){
	$params = array(
	'sp_id' => $_GET['sp_id'],
	'sp_category_id' => $_GET['sp_category_id'],
	'bind_category_id' => $_GET['bind_category_id'],
	'video_type'=>$_GET['video_type'],
	);

	$re = asset_category_model::bind_vod_item($params);
	die(json_encode($re));
}elseif($_GET['ajax']=='get_bind'){
	$sp_id = $_GET['sp_id'];
	$nns_id= $_GET['nns_id'];
	$re = asset_category_model::get_bind_vod_item($sp_id,$nns_id);
	die(json_encode($re));
}elseif($_GET['ajax']=='unbind'){
	$sp_id = $_GET['sp_id'];
	$nns_id= $_GET['nns_id'];
	$re = asset_category_model::unbind_vod_item($sp_id,$nns_id);
	die(json_encode($re));
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/dtree.js"></script>
<script type="text/javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script type="text/javascript" src="../../js/table.js.php"></script>
<script type="text/javascript" src="../../js/checkinput.js.php"></script>
<script type="text/javascript" src="../../js/category_count.js"></script>
<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
<script type="text/javascript">
var sp_item_id;
var video_type;
$(document).ready(function() {
	$(".category_editbox").width($(".content").width() - 400);
	$(window).resize(function() {
		$(".category_editbox").width($(".content").width() - 400);
	});
	 $(".selectbox").hide();
});
function close_select() {
	$(".selectbox").hide();
}
function set_move_category_id(value, depot_id, category_name) {
	if(value=='10000'){
		alert("不能绑定根栏目");
	}else{
	$.get("nncms_category_list.php?ajax=bind&video_type="+video_type+"&sp_id=<?php echo $nnsOrgId;?>&sp_category_id="+sp_item_id+"&bind_category_id="+value+"&bind_category_name="+category_name,function(data){
		if(data==0){
			alert("绑定成功");window.location.reload();
		}else{
			alert("绑定失败");window.location.reload();
		}
	},'json');
	close_select();
	}
}
function delete_bind(nns_id){
	$.get("nncms_category_list.php?ajax=unbind&sp_id=<?php echo $nnsOrgId;?>&nns_id="+nns_id,function(data){
		if(data==0){
			alert("解除绑定成功");window.location.reload();
		}else{
			alert("解除绑定失败");window.location.reload();
		}
	},'json');
}
function select_tree_item(item_id, item_name, parent_id, sp_category_id) {
	$(".selectbox").hide();
	$("#video_bind_product").html("");
	sp_item_id = sp_category_id;
    var $add_btn = $("#add_btn");
	if (item_id ==<?php echo $baseId ?>) {
		item_value = 0;
		$(".category_edit_box").find("#nns_category_detail_name").attr("disabled", true);
		$(".category_edit_box").find("input[type='button']:eq(0)").attr("disabled", true);
		$(".category_edit_box").find("input[type='button']:eq(1)").attr("disabled", true);
		$(".category_edit_box").find("input[type='button']:eq(4)").attr("disabled", true);
		$(".category_edit_box").find("input[type='button']:eq(3)").attr("disabled", true);
		$(".category_edit_box").find("input[type='button']:eq(5)").attr("disabled", true);
	} else {
		item_value = item_id;
		//video_type = 0;
		//video_bind_category_html = "已绑定点播栏目:";
		$(".category_edit_box").find("#nns_category_detail_name").attr("disabled", false);
		$(".category_edit_box").find("input[type='button']:eq(0)").attr("disabled", false);
		$(".category_edit_box").find("input[type='button']:eq(1)").attr("disabled", false);
		//$(".category_edit_box").find("input[type='button']:eq(2)").attr("disabled", false);
		//$(".category_edit_box").find("input[type='button']:eq(3)").attr("disabled", false);
		$(".category_edit_box").find("input[type='button']:eq(3)").attr("disabled", false);
		$(".category_edit_box").find("input[type='button']:eq(4)").attr("disabled", false);
		$(".category_edit_box").find("input[type='button']:eq(5)").attr("disabled", false);
		html_str = "";
		
		$.get("nncms_category_list.php?ajax=get_bind&sp_id=<?php echo $nnsOrgId;?>&nns_id="+item_id,function(data){
			if(data.data=='0'){
	    		html_str = "<div class=\"block_category\">" + data.name + "&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"javascript:delete_bind('" + item_id + "');\"><img src=\"../../images/topicon_08-topicon.png\"  border=\"0\"/></a></div>";
	    		$("#video_bind_product").html(html_str);
			}		
			//alert(data.video_type)	
			if(data.video_type=='1'){
				video_type = 1;
				video_bind_category_html = "已绑定直播栏目:";
			}
			if(data.video_type=='0'){
				video_type = 0;
				video_bind_category_html = "已绑定点播栏目:";
			}
			if(data.video_type=='2'){
				video_bind_category_html = "未绑定栏目:";
			}
			$("#video_bind_category_html").html(video_bind_category_html);
		},'json');

	}
	$(".category_edit_box").show();
	$(".category_edit_box").find("#category_parent").val(parent_id);
	$(".category_edit_box").find("#nns_category_id").val(item_value);

	$(".category_edit_box").find(".category_edit_id").html(item_id);
	$(".category_edit_box").find("#sp_category_id").html(sp_category_id);
	$(".category_edit_box").find("#nns_sp_category_id").val(sp_category_id);
	$(".category_edit_box").find("#nns_category_detail_name").val(item_name);
	$(".category_add_box").hide();
    if ($add_btn.val() == '取消') {
        $add_btn.val($add_btn.data('val'));
    }
}

function add_category_command(t) {
    var $t = $(t);
	if ($(".category_add_box").is(":hidden")) {
		$(".category_add_box").show();

		$(".category_add_box").find("#category_parent").val($(".category_edit_box").find("#nns_category_id").val());
		$(".category_add_box").find(".parent_category_box").html($(".category_edit_box").find("#nns_category_detail_name").val());
        $t.data('val', $t.val());
		$t.val("取消");
	} else {
		$t.val($t.data('val'));
		$(".category_add_box").hide();
	}
}

function order_node(action_str) {
	$(".category_edit_box").find("#action").val(action_str);
	checkForm('栏目管理', '是否进行本次修改？', $('#edit_form'), "edit");
}

function edit_node() {
	$(".category_edit_box").find("#action").val("edit");
	checkForm('栏目管理', '是否进行本次修改？', $('#edit_form'), "edit");
}

function delete_node() {
	$(".category_edit_box").find("#action").val("delete");
	checkForm('栏目管理', '是否进行本次修改？', $('#edit_form'), "delete");
}
function syn_node() {
	$(".category_edit_box").find("#action").val("syn_category");
	checkForm('栏目管理', '是否进行同步？', $('#edit_form'), "syn_category");
}
function begin_select_category(method) {
	
	if(method == 'live'){
		video_type=1;
	}else if (method == 'vod'){
		video_type=0;
	}
	$("#select_frame").attr("src", "../../controls/video_category_select/nncms_controls_video_category_select.php?method=vod&org_id=<?php echo $nns_org_id; ?>&org_type=<?php echo $nns_org_type; ?>&method="+method);
	
	$(".selectbox").show();
	
}
</script>
</head>

<body>
	<div class="selectbox">
                        <iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
                </div>
<div class="content">

    <div class="content_position">栏目管理</div>
    <div style="background-color:#FFF;">
        <div class="category_tree" style="height:400px;">
            <?php
            echo asset_category_model::build_tree_use_array($assetCategory, $baseName, $baseId);
            ?>
        </div>
        <div class="category_editbox" >
            <div class="category_edit_box">
                <form id="edit_form" action="nncms_category_control.php?sp_id=<?php echo $nnsOrgId; ?>" method="post">
                    <input name="action" id="action" type="hidden" value="" />
                    <input name="category_parent" id="category_parent" type="hidden" value="" />
                    <input name="nns_category_id" id="nns_category_id" type="hidden" value="" />
                    <input name="nns_sibling_id" id="nns_sibling_id" type="hidden" />
                    <input name="org_id" id="org_id" type="hidden" value="<?php echo $nnsOrgId; ?>" />
                    <input name="org_type" id="org_type" type="hidden" value="<?php echo $nnsOrgType; ?>" />
                    <input type="hidden" name="nns_sp_category_id" id="nns_sp_category_id" value=""/>
                    <div class="content_table">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td class="rightstyle2">栏目ID:</td>
                                    <td class="category_edit_id"></td>
                                </tr>
                                <tr>
                                    <td class="rightstyle2">SP栏目ID:</td>
                                    <td id="sp_category_id">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightstyle2"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>栏目内容名称:</td>
                                    <td><input name="nns_category_detail_name" id="nns_category_detail_name" type="text" rule="noempty" /></td>
                                </tr>
                                <tr>
                                    <td class="rightstyle2" id="video_bind_category_html">已绑定资源库栏目:</td>
                                    <td id="video_bind_product">
                                    </td>
                                </tr>
                                <!--<tr>
                                    <td class="rightstyle2">点播/直播:</td>
                                    <td id="video_type">
                                    </td>
                                </tr>-->
                                <input name="nns_sort" id="nns_sort" type="hidden"  value="0" />
                                <tr>
                                    <td class="rightstyle2">操作:</td>
                                    <td><input name="" type="button" value="修改" onclick="edit_node();"/>&nbsp;&nbsp;
                                        <input name="" type="button" value="删除" onclick="delete_node();"/>&nbsp;&nbsp;
                                        <input name="" type="button" value="添加子栏目" onclick="add_category_command(this);" id="add_btn" />&nbsp;&nbsp;
                                    	<input name="" type="button" value="绑定点播栏目" onclick="begin_select_category('vod');" id="add_btn" />&nbsp;&nbsp;
                                    	<?php if($nnsOrgId=='fjyd'||$nnsOrgId=='jllt'){?>
                                    	<input name="" type="button" value="绑定直播栏目" onclick="begin_select_category('live');" id="add_btn" />&nbsp;&nbsp;
                                    	<input name="" type="button" value="同步栏目" onclick="syn_node();"/>&nbsp;&nbsp;
                                    	<?php } ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
            <div class="category_add_box">
                <form id="add_form" action="nncms_category_control.php?sp_id=<?php echo $nnsOrgId; ?>" method="post">
                    <input name="action" id="action" type="hidden" value="add" />
                    <input name="baseid" id="baseid" type="hidden" value="<?php echo $baseId; ?>" />
                    <input name="category_parent" id="category_parent" type="hidden" value="" />
                    <input name="org_id" id="org_id" type="hidden" value="<?php echo $nnsOrgId; ?>" />
                    <input name="org_type" id="org_type" type="hidden" value="<?php echo $nnsOrgType; ?>" />
                    <div class="content_table">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td class="rightstyle2"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>上级栏目:</td>
                                    <td class="parent_category_box"></td>
                                </tr>
                                <tr>
                                    <td class="rightstyle2"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>栏目ID:</td>
                                    <td><input name="nns_category_detail_id" id="nns_category_detail_id" type="text" rule="noempty|int" len="3" style="width:20%" maxlength="3" value=""/></td>
                                </tr>
                                <tr>
                                    <td class="rightstyle2"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>SP栏目ID: </td>
                                    <td><input type="text" id="nns_sp_category_id" name="nns_sp_category_id" rule="noempty|int" value="" /></td>
                                </tr>
                                <tr>
                                    <td class="rightstyle2"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>栏目内容名称:</td>
                                    <td><input name="nns_category_detail_name" id="nns_category_detail_name" type="text" rule="noempty" value=""  /></td>
                                </tr>
                                
                                <!--<tr>
                                    <td class="rightstyle2"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>直播/点播:</td>
                                    <td><input  type="radio" name="nns_video_type" id+"nns_video_type" value="1" >直播&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="nns_video_type" id="nns_video_type" value="0"  checked="checked" />点播</td>
                                </tr>-->
                                <input type="hidden" id="nns_sort" name="nns_sort"  value="0" />                               
                                <tr>
                                    <td class="rightstyle2">操作:</td>
                                    <td><input name="" type="button" value="添加" onclick="checkForm('栏目管理', '是否进行本次修改？', $('#add_form'), 'add');"/>
                                        &nbsp;&nbsp; </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
        <div style="clear:both;"/></div>
    </div>
</div>
</body>
</html>
