<?php header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135008");
$checkpri = null;



$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$group_name = $_GET["group_name"];
if (!$pri_bool) {
	header("Location: ../nncms_content_wrong.php");
	exit ;
}

// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv/mgtv_init.php';


$sp_id = $_GET['sp_id'];
$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 12;
$page_num = isset($_GET['page'])?$_GET['page']:1;
if($page_num<1){
	$page_num = 1;
}
$offset = intval($page_num-1)*$page_size;
$limit = $page_size;

$filter = null;
if($_GET['action']=='search'){
    $_GET['nns_name'] = trim( $_GET['nns_name']);
	$filter['nns_name'] = $_GET['nns_name'];
	$filter['nns_status'] = $_GET['nns_status'];
	$filter['nns_category'] = $_GET['nns_category'];	
	$filter['day_picker_start'] = $_GET['day_picker_start'];
	$filter['day_picker_end'] = $_GET['day_picker_end'];
}
$result = category_content_model::get_content_log($sp_id,$filter,$offset,$limit,true);
$vod_total_num = $result['rows'];
$vod_pages = ceil($vod_total_num / $page_size);//总页数
if($page_num>$vod_pages){
	$page_num = $vod_pages;
}

$category_all = asset_category_model::get_category_all($sp_id);

$currentpage = $page_num;
$g_manager_list_max_num = $page_size;
//$nns_url = "&view_list_max_num=$page_size&sp_id=$sp_id";
//$refresh = "?page=$page_num&sp_id=$sp_id&view_list_max_num=";
unset($_GET['page']);
$nns_url = "&".http_build_query($_GET);//"&view_list_max_num=$page_size&sp_id=$sp_id";
$refresh = "?".http_build_query($_GET);//"?page=$page_num&sp_id=$sp_id&view_list_max_num=";

$data = $result['data'];

?>


<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
		<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/trim.js"></script>
		<script language="javascript">
			var now_select_id;
			$(document).ready(function() {
				$(".closebtn").click(function() {
					window.parent.close_select();
				});

				$(".selectbox").hide();
				$(".selectbox").css("left", ($("body").width() - $(".selectbox").width()) / 2);
			});

			function check_select_height() {
				$(".selectbox iframe").height($(".selectbox iframe").contents().find(".selectcontrol").height());
			}
			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
				var page = $("#go_page_num").val();
				window.location.href='<?php echo $refresh;?>&view_list_max_num='+num+'&page='+page
			}
			function binding(nns_id){
				window.parent.close_select();
				window.parent.binding(nns_id);
			}
		</script>
	</head>

	<body style='width:800px;height:400px;'>
		<div class="content selectcontrol" style="padding:0px; margin:0px;">
			<div class="closebtn"><img src="../../images/topicon_08-topicon.png" />
			</div>
			<div style="background-color:#FFF;">
				<div class="content_table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<form action="" method="get">
							<tbody>
								<tr>
									<td>
										影片名/拼音：&nbsp;&nbsp;<input type="text" name="nns_name" id="nns_name" value="<?php echo $_GET['nns_name'];?>" style="width:200px;">
										&nbsp;&nbsp;&nbsp;&nbsp;<input type="hidden" name="sp_id" value="<?php echo $sp_id;?>" />
										<input type="hidden" name="action" value="search" />
                                    分类：&nbsp;&nbsp;
                                    <select name="nns_category" style="width: 150px;">
											<option value="10000" <?php if($_GET['nns_category']=='10000') echo 'selected="selected"'?> >全部</option>
											<?php
											if($category_all!=null){
												foreach ($category_all as $item) {
													?>
													<option value="<?php echo $item['nns_id'];?>" <?php if($_GET['nns_category']==$item['nns_id']) echo 'selected="selected"'?>><?php echo $item['nns_name'];?></option>
													<?php
												}
											}
											?>
                                    </select>
                                    <input type="submit" value="<?php echo cms_get_lang('search'); ?>"  onclick="refresh_vod_page();"/>
                                   </td>
                                  </tr>                                  
							</tbody>
							</form>
						</table>
				</div>
				<div class="category_editbox" style="width:770px; margin:0px;height:340px;">
					<div class="content_table">
						<table width="770" border="0" cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<th>ID</th>
									<th>名称</th>									
									<th>操作</th>									
								</tr>
							</thead>
							<tbody>
								<?php
						if($data!=null){
							$num = 0;
							foreach ($data as $item) {
								$num++;
						?>
								<tr>
									<td><?php echo $num; ?>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $item["nns_id"];?></td>
									<td><?php echo $item['nns_video_name'];?></td>
									<td><a href="javascript:void();" onclick="binding('<?php echo $item["nns_id"];?>')">绑定</a></td>
								</tr><?php }}
						 
						 $least_num = $g_manager_list_max_num - count($data);
						 for ($i = 0; $i < $least_num; $i++) {
						 	?>
						 	<tr>
                                                               <td>&nbsp;</td>
                                                               <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                </tr>
						 	<?php
						 }
						 ?>
								<tr>
									<td colspan="3">
										共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num;?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_mgtv_index_lab.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_mgtv_index_lab.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_mgtv_index_lab.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_mgtv_index_lab.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>
									</td>
								</tr> 
							</tbody>
						</table>
					</div>
				</div>
				
			</div>
			<div style="clear:both;"></div>
		</div>
	</body>
</html>