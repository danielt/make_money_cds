<?php
header("Content-Type:text/html;charset=utf-8");
ini_set('display_errors',1);
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "135001");
$checkpri = null;



$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$group_name = $_GET["group_name"];
if (!$pri_bool) {
        Header("Location: ../nncms_content_wrong.php");
        exit;
}

// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";


$base_id = 10000;
if (!empty($_COOKIE["page_max_num"])) {
        $g_manager_list_max_num = $_COOKIE["page_max_num"];
}
$sp_id = $_GET['sp_id'];
include_once $nncms_config_path . 'mgtv/mgtv_init.php';
//include_once $nncms_config_path . 'mgtv/models/asset_category_model.php';
$assetCategory = asset_category_model::get_asset_category_list($sp_id);//get_asset_category(null, array('nns_sort' => ''),$sp_id);
$baseName = '栏目';
$baseId = 10000;
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
		<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
		<link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/dtree.js"></script>
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/tabs.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
		<script language="javascript" src="../../js/category_count_fjyd.js"></script>
		<script language='javascript'></script>
		<script language="javascript">
			var now_select_id;
			var now_sp_id;
			var search_str = "";
			var vod_tabs;
			var url_tab = "";
			var now_frame_url;
			$(document).ready(function() {
				category.count('<?php echo $sp_id; ?>');
				$('#clear_time').click(function(){
					$('#day_picker_start').val('');
					$('#day_picker_end').val('');
			
				});	
				search_advance_show();
				set_content_width();
				$(window).resize(function() {
					set_content_width();
				});
				vod_tabs = new mytabs($(".state_tabs"), refresh_table);
				$(".selectbox").hide();
				$("#nns_carrier").change(function() {
					var index = $("#nns_carrier").val();
					selectCarrier(index);
				});
				$(".split_btn").click(function() {
					if ($(".category_tree").is(":hidden")) {
						$(".category_tree").show();
					} else {
						$(".category_tree").hide();
					}
					set_content_width();
				});
				$(document).keydown(function(event) {
					if (event.which == 13) {
						search_vod();
					}
				});
			});
			function refresh_tree_content() {
				$("#advance_search").attr("checked", false);
				$(".vod_frame iframe").attr("src", now_frame_url);
				search_advance_show();
			}
			function set_content_width() {
				if ($(".category_tree").is(":hidden")) {
					$(".category_editbox").width($(".content").width() - 25);
				} else {
					$(".category_editbox").width($(".content").width() - $(".category_tree").width() - 50);
				}
				var len2c = $(".rightstyle2c").text().length;
				$(".rightstyle2c").width(len2c * 10);
			}

			function refresh_table(path) {
				var tab_select_id = vod_tabs.get_select_id();
				switch (tab_select_id) {
					case 0:
						url_tab = "";
						break;
					case 1:
						url_tab = "&view_audit=0";
						break;
					case 2:
						url_tab = "&view_delete=1";
						break;
				}
				refresh_vod_frame();
			}

			function checkhiddenInput() {
				var par = getAllCheckBoxSelect();
				if (par == "") {
					alert("请选择一条栏目模板！");
				} else {
					$('#nns_id').attr('value', getAllCheckBoxSelect());
					checkForm('资源库管理', '是否进行本次删除？', $('#delete_form'));
				}
			}

			function select_tree_item(item_id, item_name, parent_id,category_id) {
				$(".category_editbox").find(".category_edit_id").html(item_id);
				$(".category_editbox").find(".category_edit_name").html(item_name);
				$(".vod_frame").show();
				parent_id = '<?php echo $sp_id;?>';	
				now_sp_id = parent_id;
				now_select_id = item_id;
				search_str = "";
				refresh_vod_frame();
			}
			
			function refresh_vod_page_num(num){
				$("#nns_list_max_num").attr("value", num);
				refresh_vod_frame();
			}

			function refresh_vod_frame() {
				close_select();
				now_sp_id = '<?php echo $sp_id;?>';
				var max_num = $("#nns_list_max_num").val();
				if (!isNaN(max_num) && max_num.indexOf(".") == -1) {
					$(".vod_frame iframe").attr("src", "nncms_content_vodlist.php?category_id=" + now_select_id + "&sp_id="+ now_sp_id +"&view_list_max_num=" + $("#nns_list_max_num").val() + search_str + url_tab);
				}
				$.get("nncms_content_vodlist.php?ajax=count&category_id=" + now_select_id + "&sp_id="+ now_sp_id +"&view_list_max_num=" + $("#nns_list_max_num").val() + search_str + url_tab, function(result) {
					$('#count_all').html(result);
				}, 'json');
			}
			
			
			function refresh_explode_vod(){
				close_select();
				now_sp_id = '<?php echo $sp_id;?>';
				$.get("nncms_content_vodlist.php?ajax=explode_vod&category_id=" + now_select_id + "&sp_id="+ now_sp_id +"&view_list_max_num=" + $("#nns_list_max_num").val() + search_str + url_tab, function(result) {
					$(".vod_frame iframe").attr("src", result.url);
				}, 'json');
			}

			function checkheight() {
				var frame_url = $(".vod_frame iframe")[0].contentWindow.location.href;
				var page_name = frame_url.split("?")[0];
				page_name = page_name.split("/");
				page_name = page_name[page_name.length - 1];
				if (page_name == "nncms_content_vodlist.php") {
					$(".display_list").show();
				} else {
					$(".display_list").hide();
					$("#search_advance_tr").hide();
				}

				$(".vod_frame iframe").height($(".vod_frame iframe").contents().find(".content").height());
				$(".category_tree").height($(".category_editbox").height());
				$(".split_btn").height($(".category_editbox").height() - 20);
				window.parent.resetFrameHeight();

			}

			function search_vod() {
				search_str = "&search_py=" + encodeURI($("#nns_vod_pinyin").val())+"&day_picker_start="+encodeURI($("#day_picker_start").val())+"&day_picker_end="+encodeURI($("#day_picker_end").val());
				refresh_vod_frame();
			}
			
			function explode_vod() {
				search_str = "&search_py=" + encodeURI($("#nns_vod_pinyin").val())+"&day_picker_start="+encodeURI($("#day_picker_start").val())+"&day_picker_end="+encodeURI($("#day_picker_end").val());
				var r=confirm("可能需要些时间，是否继续！");
            	if(r == true){
					refresh_explode_vod();
				}
			}

			function selectCarrier(num) {

				switch (num) {
					case "1":
						begin_select_partner();
						break;
					case "2":
						begin_select_group();
						break;
					default:
						window.location.href = "nncms_content_vod.php?nns_id=&view_type=";
						break;
				}

			}

			function begin_show_cdn(id, type) {
				$("#select_frame").attr("src", "../../controls/nncms_control_show_cdn.php?media_id=" + id + "&media_type=" + type);
				$(".selectbox").show();
			}

			function begin_show_tags() {
				$("#select_frame").attr("src", "../../controls/epg_select/nncms_controls_epg_select.php");
				$(".selectbox").show();
			}

			function begin_select_partner() {
				$("#select_frame").attr("src", "../../controls/nncms_controls_select.php?method=partner");
				$(".selectbox").show();
			}

			function begin_select_group() {
				$("#select_frame").attr("src", "../../controls/nncms_controls_select.php?method=group");
				$(".selectbox").show();
			}

			function close_select() {
				$(".selectbox").hide();
			}

			function begin_select_category() {
				$("#select_frame").attr("src", "../../controls/video_category_select/nncms_controls_video_category_select.php?method=vod&org_id=7173fca93fbd535824299963a6f88e39&org_type=0");
				$(".selectbox").show();
			}

			function set_move_category_id(value, depot_id) {
				$(".vod_frame iframe")[0].contentWindow.set_move_category_id(value, depot_id);
				close_select();
			}

			function set_partner_id(value, name) {
				window.location.href = "../contentframe/vod_pages/nncms_content_vod.php?nns_org_id=" + value + "&nns_org_type=1&nns_id=&view_type=&group_name=" + name;
			}

			function set_group_id(value, name) {
				window.location.href = "../contentframe/vod_pages/nncms_content_vod.php?nns_org_id=" + value + "&nns_org_type=2&nns_id=&view_type=&group_name=" + name;
			}

			function search_advance_show() {
				if ($("#advance_search").attr("checked")) {

					$("#search_advance_tr").show();
				} else {
					search_vod();
					$("#search_advance_tr").hide();
				}

				window.parent.resetFrameHeight();
			}

			function export_csv_file(ids) {
				url = "nncms_content_vod_csv_export.php?view_type=&depot_detail_id=" + now_select_id + "&depot_id=dfdb6ee2bc1a2427ddac6f1f50d2d46f" + search_str + url_tab;

				if (ids) {
					url += "&nns_ids=" + ids;
					$('#nns_ids').val(ids);
					$("#export_csv_form").attr("action", url);
					$("#export_csv_form").submit();

				} else {
					window.open(url);
				}
			}

			function begin_select_label(vid) {
				$("#select_frame").attr("src", "../../../nn_models/nn_label/admin.php?action=videos_of_label_list&video_id=" + vid + "&video_type=0");
				$("#select_frame").css('width', '800px');
				$(".selectbox").show();
			}

			function get_tags_edit(tags) {
				close_select();
				$(".vod_frame iframe")[0].contentWindow.get_tags_edit(tags);
			}
		</script>
	</head>

	<body>

		<div class="selectbox">
			<iframe scrolling="no" frameborder="0"  id="select_frame" ></iframe>
		</div>

		<div class="content">
			<div class="content_position">
				栏目内容管理
			</div>
			<div style="background-color:#FFF;">

				<div class="category_tree">					
					
						<?php
            echo asset_category_model::build_tree_use_array($assetCategory, $baseName, $baseId);
            ?>
					

				</div>

				<div class="split_btn"></div>
				<div class="category_editbox">
					<div class="content_table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tbody>
								<tr>
									<td class="rightstyle2c"><font  class="font_red" style="color:#F00;font-size:16px; font-weight:bold; width: 200px;">*</font> 栏目ID/名称:</td>
									<td colspan="3"><span class="category_edit_id"></span>/<span class="category_edit_name"></span></td>
								</tr>
								<tr class="display_list">
									<td class="">点播查询:</td>
									<td>影片名/拼音&nbsp;&nbsp;
									<input name="nns_vod_search" id="nns_vod_pinyin" type="text" value="" style="width:100px;" />
									选择时间段 : <input name="day_picker_start" id="day_picker_start" type="text"  value="" style="width:100px;" class="datetimepicker" callback="test" />-<input name="day_picker_end" id="day_picker_end" type="text"  value="" style="width:100px;" class="datetimepicker" callback="test" />
									<input  name="nns_list_max_num" id="nns_list_max_num" type="hidden" value="20" />
									&nbsp;&nbsp;<input type="button" id="clear_time" name="clear_time" value="清除时间"/>
									<input type="button" value="导出" onclick="explode_vod();"/>
									<input type="button" value="查询" onclick="search_vod();"/>									
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>					
					<div class="vod_frame">
						<iframe  scrolling="no" frameborder="0" onload="checkheight();" style="width:100%;"></iframe>
					</div>

				</div>
				<div style="clear:both;"/>
			</div>
			<form id="export_csv_form" name="export_csv_form" action="" method="post">
				<input name="action" id="action" type="hidden" value="export" />
				<input name="nns_ids" id="nns_ids" type="hidden" value="" />
			</form>

		</div>
	</body>
</html>