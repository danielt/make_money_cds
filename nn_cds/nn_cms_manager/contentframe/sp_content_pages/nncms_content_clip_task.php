<?php header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135003");
$checkpri = null;



$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$group_name = $_GET["group_name"];
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}


// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv/mgtv_init.php';

if(isset($_GET['op'])){
	$action =trim($_GET['op']);
	switch($action){
		/*
        case 'force_restart':
        $result = clip_task_model::restart_task($_GET['task_id'],true);
        if($result){
        	echo '<script>alert("强制切片成功");</script>';
        }else{
        	echo '<script>alert("强制切片失败");</script>';
        }
        break;		
        */
        case 'restart':
	        $force = $_GET['force']?true:false;
	        
	        $task_ids = explode(",",$_GET['task_id']);
	        foreach($task_ids as $item_id){
	        	$item_id && clip_task_model::restart_task($item_id,$force);
	        }
            if($force){
            	echo '<script>alert("添加强制任务成功");</script>';
            }else{
            	echo '<script>alert("重新添加任务成功");</script>';
            }
        	

        break;        
        case 'sort':
        $result = clip_task_model::set_task_priority($_GET['task_id'],$_GET['d']);
        if($result){
        	echo '<script>alert("任务排序成功");</script>';
        }else{
        	echo '<script>alert("任务排序失败");</script>';
        }        
        break;
        case 'delete_movie':
        $task_ids = explode(",",$_GET['task_id']);
        $result = clip_task_model::delete_movie_by_task_id($task_ids);
        if($result){
        	echo '<script>alert("删除执行成功");</script>';
        }else{
        	echo '<script>alert("删除执行失败");</script>';
        }        
        break;
        
	}
	unset($_GET['op']);
	unset($_GET['force']);
	unset($_GET['task_id']);
	unset($_GET['d']);
}


$sp_id = $_GET['sp_id'];
$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 12;
$page_num = isset($_GET['page'])?$_GET['page']:1;
if($page_num<1){
	$page_num = 1;
}

$offset = intval($page_num-1)*$page_size;
$limit = $page_size;
$filter = null;
if($_GET['action']=='search'){
	$filter['nns_name'] = trim($_GET['nns_name']);
	$filter['nns_state'] = $_GET['nns_state'];
	$filter['day_picker_start'] = $_GET['day_picker_start'];
	$filter['day_picker_end'] = $_GET['day_picker_end'];
	$filter['day_picker_put_start'] = $_GET['day_picker_put_start'];
	$filter['day_picker_put_end'] = $_GET['day_picker_put_end'];
	$filter['start_time'] = $_GET['start_time'];
	$filter['end_time'] = $_GET['end_time'];
	$filter['nns_id'] = $_GET['nns_id'];
}
$result =  clip_task_model::get_task_list($sp_id,$filter,$offset,$limit);

$vod_total_num = $result['rows'];//记录条数

$vod_pages = ceil($vod_total_num / $page_size);//总页数
if($page_num>$vod_pages){
	$page_num = $vod_pages;
}


$currentpage = $page_num;
$g_manager_list_max_num = $page_size;
unset($_GET['page']);
$nns_url = "&".http_build_query($_GET);//"&view_list_max_num=$page_size&sp_id=$sp_id";
$refresh = "?".http_build_query($_GET);//"?page=$page_num&sp_id=$sp_id&view_list_max_num=";


$data = $result['data'];




?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript">
			function refresh_vod_page() {

				var num = $("#nns_list_max_num").val();
				//window.parent.refresh_vod_page_num(num);
				window.location.href='<?php echo $refresh;?>&view_list_max_num='+num
			}
			
			

			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {

						BoxKey = true;
						return false;
					}

				})
				return BoxKey;
			}
			
			function add_task_tmp(nns_id){				
				$.get("nncms_content_task.php?sp_id=<?php echo $sp_id?>&ajax=add&nns_id="+nns_id,function(result){					
					alert(result);window.location.reload();
				},'json');
			}
			
			function stop_task_tmp(nns_id){				
				$.get("nncms_content_task.php?sp_id=<?php echo $sp_id?>&ajax=stop&nns_id="+nns_id,function(result){					
					alert(result);window.location.reload();					
				},'json');
			}
			
			function rsync_info(nns_id){				
				$.get("nncms_content_task.php?sp_id=<?php echo $sp_id?>&ajax=rsync&nns_id="+nns_id,function(result){					
					alert(result);window.location.reload();
				},'json');
			}
			
			
			$(document).ready(function() {
				window.parent.now_frame_url = window.location.href;
				
				$('#clear_time1').click(function(){
					$('#day_picker_start').val('');
					$('#day_picker_end').val('');
			
				});
				$('#clear_time2').click(function(){
					$('#day_picker_put_start').val('');
					$('#day_picker_put_end').val('');
			
				});
				$('#clear_time3').click(function(){
					$('#start_time').val('');
					$('#end_time').val('');
			
				});								
			});
			
			function restart_more(force){
				force = force?1:0;
				var id=getAllCheckBoxSelect();
				if (id==""){
					alert("请选择至少选择一项");
				}else{
					var r=confirm("是否要执行任务？");
					if(r == true){
                        window.location.href="nncms_content_clip_task.php?sp_id=<?php echo $sp_id;?>&op=restart&force="+force+"&task_id="+id+"<?php echo $nns_url . $search_url; ?>";
					}
                }				
				return;
			}
			function delete_movie_more(){
				var id=getAllCheckBoxSelect();
				if (id==""){
					alert("请选择至少选择一项");
				}else{
					var r=confirm("是否要执行删除操作？");
					if(r == true){
                        window.location.href="nncms_content_clip_task.php?sp_id=<?php echo $sp_id;?>&op=delete_movie&task_id="+id+"<?php echo $nns_url . $search_url; ?>";
					}
                }				
				return;				
			}

		</script>
	</head>

	<body>
		<div class="content">
			<div class="content_position">
				切片管理 > 切片列表
			</div>
				<div class="content_table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<form action="" method="get">
							<tbody>
								<tr>
									<td>
										影片名/拼音：&nbsp;&nbsp;<input type="text" name="nns_name" id="nns_name" value="<?php echo isset($_GET['nns_name'])?$_GET['nns_name']:'';?>" style="width:200px;"/>
										&nbsp;&nbsp;&nbsp;&nbsp;ID：&nbsp;&nbsp;<input type="text" name="nns_id" id="nns_id" value="<?php echo isset($_GET['nns_id'])?$_GET['nns_id']:'';?>" style="width:200px;"/>
										&nbsp;&nbsp;&nbsp;&nbsp;
										状态：&nbsp;&nbsp;<select name="nns_state" style="width: 150px;">
											<option value="">全部</option>
											<option value="wait" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='wait') echo 'selected="selected"';?>>等待下发</option>
											<option value="handle" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='handle') echo 'selected="selected"';?>>已经下发</option>
											<option value="get_media_url_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='get_media_url_fail') echo 'selected="selected"';?>>获取下载地址失败</option>
											<option value="download_succ" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='download_succ') echo 'selected="selected"';?>>下载成功</option>
											<option value="download_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='download_fail') echo 'selected="selected"';?>>下载失败</option>											
											<option value="cliping" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='cliping') echo 'selected="selected"';?>>正在切片</option>
											<option value="clip_succ" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='clip_succ') echo 'selected="selected"';?>>切片成功</option>
											<option value="clip_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='clip_fail') echo 'selected="selected"';?>>切片失败</option>
											<option value="ok" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='ok') echo 'selected="selected"';?>>上报成功</option>
											<option value="c2_handle" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='c2_handle') echo 'selected="selected"';?>>注入执行中</option>
											<option value="c2_ok" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='c2_ok') echo 'selected="selected"';?>>注入成功</option>
										    <option value="c2_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='c2_fail') echo 'selected="selected"';?>>注入失败</option>
										</select>
									</td>
									</tr>
									
								
								<tr>
									<td>
									上报时间：&nbsp;&nbsp;&nbsp;<input name="day_picker_put_start" id="day_picker_put_start" type="text"  value="<?php echo isset($_GET['day_picker_put_start']) ? $_GET['day_picker_put_start'] : "";?>" style="width:120px;" class="datetimepicker" callback="test" />
									 - <input name="day_picker_put_end" id="day_picker_put_end" type="text"  value="<?php echo isset($_GET['day_picker_put_end']) ? $_GET['day_picker_put_end'] : "";?>" style="width:120px;" class="datetimepicker" callback="test" />
									<input type="button" id="clear_time2" name="clear_time2" value="清除时间"/>&nbsp;&nbsp;	
										&nbsp;&nbsp;开始结束时间：&nbsp;&nbsp;<input name="start_time" id="start_time" type="text"  value="<?php echo isset($_GET['start_time']) ? $_GET['start_time'] : "";?>" style="width:120px;" class="datetimepicker" callback="test" />
										 - <input name="end_time" id="end_time" type="text"  value="<?php echo isset($_GET['end_time']) ? $_GET['end_time'] : "";?>" style="width:120px;" class="datetimepicker" callback="test" />
										<input type="hidden" name="sp_id" value="<?php echo $sp_id;?>" />
										<input type="hidden" name="action" value="search" />	
										&nbsp;&nbsp;&nbsp;<input type="button" id="clear_time3" name="clear_time3" value="清除时间"/>&nbsp;&nbsp;
																		
									</td>
								</tr>
								<tr>
										<td>
										创建时间：&nbsp;&nbsp;&nbsp;<input name="day_picker_start" id="day_picker_start" type="text"  value="<?php echo isset($_GET['day_picker_start']) ? $_GET['day_picker_start'] : "";?>" style="width:120px;" class="datetimepicker" callback="test" /> - 
										<input name="day_picker_end" id="day_picker_end" type="text"  value="<?php echo isset($_GET['day_picker_end']) ? $_GET['day_picker_end'] : "";?>" style="width:120px;" class="datetimepicker" callback="test" />
										<input type="button" id="clear_time1" name="clear_time1" value="清除时间"/>&nbsp;&nbsp;
										<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  onclick="refresh_vod_page();"/>	
									</td>
								</tr>
							</tbody>
							</form>
						</table>
				</div>			
			<div class="content_table formtable">

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th><input name="" type="checkbox" value="">序号</th>
							
							<th>影片名称</th>
						
							<th>分片</th>
							<th>状态</th>
							
							<th>创建时间</th>
							<th>开始时间</th>
							<th>结束时间</th>
							<th>上报时间</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if($data!=null){
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) {
								$num++;
						?>
						<tr>
						<td><input name="input" type="checkbox" value="<?php echo $item["nns_id"];?>" /><?php echo $num; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $item['nns_id'];?></td>   
					                     
                        <td> <a href="nncms_content_clip_task_detail.php?sp_id=<?php echo $_GET['sp_id'];?>&task_id=<?php echo $item['nns_id'];?>">                        
                        <?php if(!empty($item['index_name'])) {echo $item['index_name'];}else{ echo $item['vod_name'];}?>    </a>                                             
                        </td> 
                         
                         <td>  
                                                                      分片<?php echo $item['nns_index']+1;?>
                        </td>                        
						<td><?php 
							if(empty($item['nns_state'])){
								echo '未执行';
							}else{
								if(isset( clip_task_model::$task_state_arr[$item['nns_state']])){
									echo clip_task_model::$task_state_arr[$item['nns_state']];
								}else{
									echo $item['nns_state'];
								}
							}

							
						?></td>
						
                        <td>                        
                        <?php echo $item['nns_create_time'];?>                                            
                        </td>
                        <td>                        
                        <?php echo $item['nns_start_time'];?>                                        
                        </td> 
                        <td>                        
                        <?php echo $item['nns_end_time'];?>                                        
                        </td>                                               
                        <td>                        
                        <?php echo $item['nns_alive_time'];?>                                            
                        </td>	                        						
						<td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
						<a href="?sp_id=<?php echo $sp_id;?>&op=restart&force=0&task_id=<?php echo $item['nns_id']?><?php echo $nns_url . $search_url; ?>">重发任务</a>
						<a href="?sp_id=<?php echo $sp_id;?>&op=restart&force=1&task_id=<?php echo $item['nns_id']?><?php echo $nns_url . $search_url; ?>">强制重切</a>
						<?php if($item['nns_state']){?>
						<a target="_blank" href="./nncms_content_log_xml.php?sp_id=<?php echo $sp_id;?>&task_id=<?php echo $item['nns_id']?>">查看内容</a>
						<?php }?>
						<a href="?sp_id=<?php echo $sp_id;?>&op=sort&d=top&task_id=<?php echo $item['nns_id']?>">置顶</a>
						<a href="?op=sort&d=up&task_id=<?php echo $item['nns_id']?><?php echo $nns_url . $search_url; ?>">向上</a>
						<a href="?op=sort&d=down&task_id=<?php echo $item['nns_id']?><?php echo $nns_url . $search_url; ?>">向上</a>
						<a href="?sp_id=<?php echo $sp_id;?>&op=sort&d=bottom&task_id=<?php echo $item['nns_id']?>">置底</a>
						
						</td>
                        </tr>
						<?php
							}
						}
						$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) {
						?>
						<tr>
                                                               
                                                                
                                                               
                                                               
                                                            
                                                                 <td>&nbsp; </td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp; </td>                                                                
                                                                <td>&nbsp; </td>
                                                                 <td>&nbsp; </td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>


                                                        </tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="pagecontrol">
			共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num;?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_clip_task.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_clip_task.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_content_clip_task.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_clip_task.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_clip_task.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

                        </div>
                        
		     <div class="controlbtns">
		    	<div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
		        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
		        <div class="controlbtn add"><a href="javascript:restart_more(0);" >重发任务</a></div>
		        <div class="controlbtn delete"><a href="javascript:restart_more(1);" >强制重切</a></div>
		        <div class="controlbtn delete"><a href="javascript:delete_movie_more();" >删除片源</a></div>
		        <div style="clear:both;"></div>
		    </div>                          
		</div>
	</body>
</html>
