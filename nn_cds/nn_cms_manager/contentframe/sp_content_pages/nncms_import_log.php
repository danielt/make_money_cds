<?php header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135006");
$checkpri = null;



$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$group_name = $_GET["group_name"];
if (!$pri_bool) {
	header("Location: ../nncms_content_wrong.php");
	exit ;
}

// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv/mgtv_init.php';
$sp_id = $_GET['sp_id'];
$method = $_GET['method'];
$vod_id = $_GET['vod_id'];
$index_num = isset($_GET['index_num']) ? $_GET['index_num'] : 0;
$video_index_id = isset($_GET['video_index_id']) ? $_GET['video_index_id'] : 0;
$data = category_model::get_import_log($sp_id,$method,$vod_id,$index_num,$video_index_id);
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
		<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/trim.js"></script>
		<script language="javascript">
			var now_select_id;
			$(document).ready(function() {
				$(".closebtn").click(function() {
					window.parent.close_select();
				});

				$(".selectbox").hide();
				$(".selectbox").css("left", ($("body").width() - $(".selectbox").width()) / 2);
			});

			function check_select_height() {
				$(".selectbox iframe").height($(".selectbox iframe").contents().find(".selectcontrol").height());
			}
		</script>
	</head>

	<body style='width:800px;height:400px;'>
		<div class="content selectcontrol" style="padding:0px; margin:0px;">
			<div class="closebtn"><img src="../../images/topicon_08-topicon.png" />
			</div>
			<div style="background-color:#FFF;">
				<div class="category_editbox" style="width:770px; margin:0px;height:370px;">
					<div class="content_table">
						<table width="770" border="0" cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<th>ID</th>
									<th>名称</th>
									<th>对象</th>
									<th>操作</th>
									<th>创建时间</th>
									<th>发送状态</th>
									<th>发送时间</th>
									<th>通知状态</th>
									<th>通知时间</th>
									
									
								</tr>
							</thead>
							<tbody>
								<?php
						if($data!=null){
							$num = 0;
							foreach ($data as $item) {
								$num++;
						?>
								<tr>
									<td><?php echo $num; ?>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $item["nns_id"];?></td>
									<td><?php echo $item['nns_task_name'];?></td>
									<td><?php echo $item['nns_task_type'];?> </td>
									<td><?php echo $item['nns_action'];?></td>
									<td><?php echo $item['nns_create_time'];?></td>
									<td>
									<?php if(false!== strpos($item['nns_result'],'[0]')) {echo '成功';}else {echo '<font color="red">失败</font>'.$item['nns_result'];} ?>
                         			<?php if($item['nns_again']=='-1' && $item['nns_notify_result']=='-1') echo '&nbsp;&nbsp;|&nbsp;<font color="red">重发失败</font>';?>
									</td>
									<td><?php echo $item['nns_send_time'];?></td>
									<td><?php  if($item['nns_notify_result'] !== null) {if($item['nns_notify_result']=='-1') {echo '<font color="red">失败</font>';}else {echo '成功';}}?></td>
									<td><?php echo $item['nns_notify_time'];?></td>
								</tr><?php }} ?> 
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div style="clear:both;"></div>
		</div>
	</body>
</html>