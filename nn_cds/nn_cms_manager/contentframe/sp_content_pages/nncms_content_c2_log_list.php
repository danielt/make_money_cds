<?php header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
set_time_limit(0);
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135004");
$checkpri = null;




if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}


// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv/mgtv_init.php';



$sp_id = $_GET['sp_id'];
$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 12;
$page_num = isset($_GET['page'])?$_GET['page']:1;
if($page_num<1){
	$page_num = 1;
}

$offset = intval($page_num-1)*$page_size;
$limit = $page_size;
$filter = null;
if($_GET['op']=='resend' && !empty($_GET['c2_id'])){
	 c2_task_model::execute_c2_again($_GET['c2_id']);
	 echo '<script>alert("重发指令成功");</script>';
	 unset($_GET['op']);
	 unset($_GET['c2_id']);
}
else if($_GET['op']=='resend_more' && !empty($_GET['c2_ids'])){
	 $nns_ids = explode(',',$_GET['c2_ids']);
	 foreach($nns_ids as $item_id){
	 	if(!empty($item_id))
	 	    c2_task_model::execute_c2_again($item_id);
	 }
	 
	 echo '<script>alert("重发指令成功");</script>';
	 unset($_GET['op']);
	 unset($_GET['c2_ids']);
}else if($_GET['op']=='delete_c2'  && !empty($_GET['c2_id'])){
	 $nns_ids = explode(',',$_GET['c2_id']);
	 c2_task_model::delete_c2($nns_ids);
	  
	 echo '<script>alert("删除指令成功");</script>';
	 unset($_GET['op']);
	 unset($_GET['c2_id']);	 
}
if($_GET['action']=='search'){
	$_GET['nns_task_name'] = trim($_GET['nns_task_name']);
	$filter['nns_task_type'] = $_GET['nns_task_type'];
	$filter['nns_action'] = $_GET['nns_action'];
	$filter['nns_state'] = $_GET['nns_state'];
	$filter['nns_task_name'] = $_GET['nns_task_name'];
	$filter['nns_id'] = $_GET['nns_id'];
	$filter['day_picker_start'] = $_GET['day_picker_start'];
	$filter['day_picker_end'] = $_GET['day_picker_end'];
	$filter['notify_time_start'] = $_GET['notify_time_start'];
	$filter['notify_time_end'] = $_GET['notify_time_end'];	
}
$result =  c2_task_model::get_c2_log_list($sp_id,$filter,$offset,$limit);

$vod_total_num = $result['rows'];//记录条数

$vod_pages = ceil($vod_total_num / $page_size);//总页数
if($page_num>$vod_pages){
	$page_num = $vod_pages;
}


$currentpage = $page_num;
$g_manager_list_max_num = $page_size;
unset($_GET['page']);
$nns_url = "&".http_build_query($_GET);//"&view_list_max_num=$page_size&sp_id=$sp_id";
$refresh = "?".http_build_query($_GET);//"?page=$page_num&sp_id=$sp_id&view_list_max_num=";
/*select vod_index.nns_name as index_name,vod.nns_name as vod_name,vod.nns_pinyin,task.* from nns_mgtvbk_clip_task task inner join nns_vod vod on (
 task.nns_video_id=vod.nns_id
)  inner join nns_vod_index vod_index on (
 vod_index.nns_id=task.nns_video_index_id
) where (task.nns_state!='cancel' or task.nns_state is null)*/

$data = $result['data'];




?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript">
			function refresh_vod_page() {

				var num = $("#nns_list_max_num").val();
				//window.parent.refresh_vod_page_num(num);
				window.location.href='<?php echo $refresh;?>&view_list_max_num='+num
			}
			
			

			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {

						BoxKey = true;
						return false;
					}

				})
				return BoxKey;
			}
			

			
			function resend_c2_more(){
				var id=getAllCheckBoxSelect();
				if (id==""){
					alert("请选择至少选择一项");
				}else{
					var r=confirm("是否重发");
					if(r == true){
                        window.location.href="nncms_content_c2_log_list.php?op=resend_more&c2_ids="+id+"<?php echo $nns_url . $search_url; ?>";
					}
                }
			}
			function delete_c2(id){
				var r=confirm("是否删除");
				if(r == true){
                    window.location.href="nncms_content_c2_log_list.php?op=delete_c2&c2_id="+id+"<?php echo $nns_url . $search_url; ?>";
				}
				return ;
                
			}			
			function delete_c2_more(){
				var id=getAllCheckBoxSelect();
				if (id==""){
					alert("请选择至少选择一项");
				}else{
					var r=confirm("是否删除");
					if(r == true){
                        window.location.href="nncms_content_c2_log_list.php?op=delete_c2&c2_id="+id+"<?php echo $nns_url . $search_url; ?>";
					}
                }
			}			
			$(document).ready(function() {
				window.parent.now_frame_url = window.location.href;
				
				$('#clear_time').click(function(){
					$('#day_picker_start').val('');
					$('#day_picker_end').val('');
			
				});
				
				$('#clear_time2').click(function(){
					$('#notify_time_start').val('');
					$('#notify_time_end').val('');
			
				});				
			});

		</script>
	</head>

	<body>
		<div class="content">
			<div class="content_position">
				注入华为命令列表
			</div>
				<div class="content_table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<form action="" method="get">
							<tbody>
								<tr>
									<td>
									   名称：&nbsp;&nbsp;<input type="text" name="nns_task_name" id="nns_task_name" value="<?php echo $_GET['nns_task_name'];?>" style="width:200px;">
									  &nbsp;&nbsp; ID:&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="nns_id" id="nns_id" value="<?php echo $_GET['nns_id'];?>" style="width:200px;">
										
										<input type="hidden" name="sp_id" value="<?php echo $sp_id;?>" />
										<input type="hidden" name="action" value="search" />	
										
										&nbsp;&nbsp; 对象：&nbsp;&nbsp;&nbsp;&nbsp; 
										<select name="nns_task_type" style="width: 80px;">
											<option value="">全部</option>
											<option value="Series" <?php if(isset($_GET['nns_task_type']) && $_GET['nns_task_type']=='Series') echo 'selected="selected"';?>>Series</option>
											<option value="Program" <?php if(isset($_GET['nns_task_type']) && $_GET['nns_task_type']=='Program') echo 'selected="selected"';?>>Program</option>
											<option value="Movie" <?php if(isset($_GET['nns_task_type']) && $_GET['nns_task_type']=='Movie') echo 'selected="selected"';?>>Movie</option>
											<option value="Channel" <?php if(isset($_GET['nns_task_type']) && $_GET['nns_task_type']=='Channel') echo 'selected="selected"';?>>Channel</option>
											<option value="PhysicalChannel" <?php if(isset($_GET['nns_task_type']) && $_GET['nns_task_type']=='PhysicalChannel') echo 'selected="selected"';?>>PhysicalChannel</option>
											<option value="Schedule" <?php if(isset($_GET['nns_task_type']) && $_GET['nns_task_type']=='Schedule') echo 'selected="selected"';?>>Schedule</option>
											<option value="Category" <?php if(isset($_GET['nns_task_type']) && $_GET['nns_task_type']=='Category') echo 'selected="selected"';?>>Category</option>
											<option value="Img" <?php if(isset($_GET['nns_task_type']) && $_GET['nns_task_type']=='Img') echo 'selected="selected"';?>>Img</option>
										</select>
										
										操作：&nbsp;&nbsp;
                                         <select name="nns_action" style="width: 80px;">
											<option value="">全部</option>
											<option value="REGIST" <?php if(isset($_GET['nns_action']) && $_GET['nns_action']=='REGIST') echo 'selected="selected"';?>>REGIST</option>
											<option value="UPDATE" <?php if(isset($_GET['nns_action']) && $_GET['nns_action']=='UPDATE') echo 'selected="selected"';?>>UPDATE</option>
											<option value="DELETE" <?php if(isset($_GET['nns_action']) && $_GET['nns_action']=='DELETE') echo 'selected="selected"';?>>DELETE</option>
											
										</select>&nbsp;&nbsp;状态：&nbsp;&nbsp;<select name="nns_state" style="width: 80px;">
											<option value="">全部</option>
											<option value="request_state_succ" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='request_state_succ') echo 'selected="selected"';?>>发送成功</option>
											<option value="request_state_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='request_state_fail') echo 'selected="selected"';?>>发送失败</option>
											<option value="notify_state_wait" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='notify_state_wait') echo 'selected="selected"';?>>等待通知</option>
											<option value="notify_state_succ" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='notify_state_succ') echo 'selected="selected"';?>>通知成功</option>
											<option value="notify_state_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='notify_state_fail') echo 'selected="selected"';?>>通知失败</option>
											<option value="resend_fail" <?php if(isset($_GET['nns_state']) && $_GET['nns_state']=='resend_fail') echo 'selected="selected"';?>>重发失败</option>
										</select>														                         
									</td>
								</tr>
								<tr>
									<td>
                                   &nbsp;&nbsp;创建时间：&nbsp;&nbsp;
									<input name="day_picker_start" id="day_picker_start" type="text"  value="<?php if(isset($_GET['day_picker_start'])) echo $_GET['day_picker_start'];?>" style="width:120px;" class="datetimepicker" callback="test" />
									 - <input name="day_picker_end" id="day_picker_end" type="text"  value="<?php if(isset($_GET['day_picker_end'])) echo $_GET['day_picker_end'];?>" style="width:120px;" class="datetimepicker" callback="test" />
										&nbsp;&nbsp;
					                         <input type="button" id="clear_time" name="clear_time" value="清除时间"/>&nbsp;&nbsp;									
                                   &nbsp;&nbsp;通知时间：&nbsp;&nbsp;
									<input name="notify_time_start" id="notify_time_start" type="text"  value="<?php if(isset($_GET['notify_time_start'])) echo $_GET['notify_time_start'];?>" style="width:120px;" class="datetimepicker" callback="test" />
									 - <input name="notify_time_end" id="notify_time_end" type="text"  value="<?php if(isset($_GET['notify_time_end'])) echo $_GET['notify_time_end'];?>" style="width:120px;" class="datetimepicker" callback="test" />
										&nbsp;&nbsp;
					                         <input type="button" id="clear_time2" name="clear_time2" value="清除时间"/>&nbsp;&nbsp;	
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  onclick="refresh_vod_page();"/>
									</td>
								</tr>
							</tbody>
							</form>
						</table>
				</div>			
			<div class="content_table formtable">

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th><input name="" type="checkbox" value="">序号</th>
							<th>ID</th>
							<th>名称</th>
							<th>对象</th>						
							<th>操作</th>
							<th>创建时间</th>
							<th>发送状态</th>
							<th>发送时间</th>
							<th>通知状态</th>								
							<th>通知时间</th>						
						   <th>操作</th>
							
						</tr>
					</thead>
					<tbody>
						<?php
						if($data!=null){
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) {
								$num++;
						?>
						<tr>
						<td><input name="input" type="checkbox" value="<?php echo $item["nns_id"];?>" /><?php echo $num; ?></td>   
                        <td>                       
                        <?php echo $item['nns_id'];?>                                            
                        </td> 
                        <td><?php echo $item['nns_task_name'];?></td>					                     
                        <td>                       
                        <?php echo $item['nns_task_type'];?>                                            
                        </td> 
                         
                         <td>  
                         <?php echo $item['nns_action'];?>
                        </td>    
                        <td>  
                         <?php echo $item['nns_create_time'];?>
                        </td>                                             
						<td>  
                         <?php if(false!== strpos($item['nns_result'],'[0]')) {echo '成功';}else {echo '<font color="red">失败</font>'.$item['nns_result'];} ?>
                         <?php if($item['nns_again']=='-1' && $item['nns_notify_result']=='-1') echo '&nbsp;&nbsp;|&nbsp;<font color="red">重发失败</font>';?>
                        </td>
                        <td>  
                         <?php echo $item['nns_send_time'];?>
                        </td>  
                        <td>                        
                        <?php  if($item['nns_notify_result'] !== null) {if($item['nns_notify_result']=='-1') {echo '<font color="red">失败</font>';}else {echo '成功';}}?>                                            
                        </td>
                                   
                        <td>                        
                        <?php echo $item['nns_notify_time'];?>                                        
                        </td>                                               
                        <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">     
                         <a href="<?php	echo C2_FTP.$item['nns_url']; ?>" target="_blank">查看发送内容</a>   
                         <?php
                         if($item['nns_notify_result_url']){  
                        $url_arr = parse_url($item['nns_notify_result_url']);
                        $notify_save_path = $nncms_config_path.'data/mgtv/'.$item['nns_org_id'].'/notify'.$url_arr["path"];

                        if(!file_exists($notify_save_path) && !empty($item['nns_notify_result_url'])){
                        	include_once NPDIR . '/np_ftp.php';
							$ftp1=new nns_ftp($url_arr['host'],$url_arr["user"],$url_arr["pass"],$url_arr["port"]);
							$ftpcon1=$ftp1->connect();
				            if (empty($ftpcon1)) {
				                return FALSE;
				            }

				            $path_parts = pathinfo($notify_save_path);
				             if (!is_dir($path_parts['dirname'])){
				                 mkdir($path_parts['dirname'], 0777, true);
				             }            
				            $ftp1->get(ltrim($url_arr["path"],'/'),$notify_save_path);	
				            $nns_notify_content = file_get_contents($notify_save_path);
			            	if($nns_notify_content){
			            		c2_task_model::update_notify_content($item['nns_id'],$nns_notify_content);
			            	}

                        }
                        if(file_exists($notify_save_path) && empty($item['nns_notify_content'])){
				            $nns_notify_content = file_get_contents($notify_save_path);
			            	if($nns_notify_content){
			            		c2_task_model::update_notify_content($item['nns_id'],$nns_notify_content);
			            	}                        	
                        }
                    
                        ?>                       
                        <a href="/nn_mgtvbk/<?php echo $item['nns_notify_result_url']?>" target="_blank" title="<?php echo $item['nns_notify_result_url'];?>">查看通知结果</a>                                  
                        <?php } ?>                        
                                        
                        <a href="?op=resend&c2_id=<?php echo $item['nns_id']?><?php echo $nns_url . $search_url; ?>">重发指令</a>                                      
                        <a href="javascript:delete_c2('<?php echo $item['nns_id']?>')">删除指令</a> 
                        </td> 	                        						

                        </tr>
						<?php
							}
						}
						$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) {
						?>
						<tr>
                                                               
                                                               
                                                           
                                                            <td>&nbsp; </td>   
                                                            <td>&nbsp; </td>
                                                            <td>&nbsp; </td>
                                                              
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp; </td>                                                                
                                                                <td>&nbsp; </td>
                                                                 <td>&nbsp; </td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>


                                                        </tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="pagecontrol">
			共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num;?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_c2_log_list.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_c2_log_list.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_content_c2_log_list.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_c2_log_list.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_c2_log_list.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

                        </div>
                        
             <div class="controlbtns">
    	<div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
        <div class="controlbtn add"><a href="javascript:resend_c2_more();" >重发</a></div>
        <div class="controlbtn delete"><a href="javascript:delete_c2_more();" >删除</a></div>
        <div style="clear:both;"></div>
    </div>                         
		</div>
	</body>
</html>

