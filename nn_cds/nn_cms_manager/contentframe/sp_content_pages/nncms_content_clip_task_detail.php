<?php header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135003");
$checkpri = null;


$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$group_name = $_GET["group_name"];
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}


// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv/mgtv_init.php';




//$sp_id = $_GET['sp_id'];
$task_vod_info = clip_task_model::get_task_vod_info($_GET['task_id']);
$data =  clip_task_model::get_task_detail_log($_GET['task_id']);//get_task_list($sp_id,$filter,$offset,$limit);


?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!--meta http-equiv="refresh" content="3;url=" /-->
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
        <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
        <script language="javascript" src="../../js/table.js.php"></script>
	</head>

	<body>
		<div class="content">
			<div class="content_position">
				切片管理 > 状态详细
			</div>
			<div class="content_table">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<form action="" method="get">
						<tbody>
							<tr class="display_list">
								<td class="rightstyle2a">任务ID：</td>
								<td class="rightstyle3"><?php echo $task_vod_info['nns_id'];?></td>
								<td class="rightstyle2a">创建时间：</td>
								<td class="rightstyle3"><?php echo $task_vod_info['nns_create_time'];?></td>	
							</tr>
							<tr>
								<td class="rightstyle2a">影片：</td>
								<td class="rightstyle3"><?php echo $task_vod_info['vod_name'];?></td>	
								<td class="rightstyle2a">开始时间：</td>
								<td class="rightstyle3"><?php echo $task_vod_info['nns_start_time'];?></td>	
							</tr>
																																							
							<tr class="display_list">
								<td class="rightstyle2a">分集名：</td>
								<td class="rightstyle3"><?php echo $task_vod_info['index_name'];?></td>
								<td class="rightstyle2a">结束时间：</td>
								<td class="rightstyle3"><?php echo $task_vod_info['nns_end_time'];?></td>
							</tr>
							<tr>		
								<td class="rightstyle2a">分片：</td>
								<td class="rightstyle3"><?php echo $task_vod_info['nns_index'];?></td>	
								<td class="rightstyle2a">上报时间：</td>
								<td class="rightstyle3"><?php echo $task_vod_info['nns_alive_time'];?></td>	
							</tr>
							<tr>
								<td class="rightstyle2a"> 状态：</td>
								<td class="rightstyle3"><?php echo $task_vod_info['nns_state'];?></td>																			
								<td class="rightstyle2a">描述：</td>
								<td class="rightstyle3"><?php echo $task_vod_info['nns_desc'];?></td>															
							</tr>							
						</tbody>
						</form>
					</table>
			</div>				
			<div class="content_table formtable">

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th>序号</th>
						
							<th>状态</th>
							
							<th>描述</th>

							<th>上报时间</th>

						</tr>
					</thead>
					<tbody>
						<?php
						if($data!=null){
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) {
								$num++;
						?>
						<tr>
						<td><?php echo $num; ?></td>                        
                       
						<td><?php 
							if(empty($item['nns_state'])){
								echo '未执行';
							}else{
								echo $item['nns_state'];
							}
							
						?></td>
                        <td>                        
                        <?php echo $item['nns_desc'];?>                                            
                        </td>						
                        <td>                        
                        <?php echo $item['nns_create_time'];?>                                            
                        </td>

                        </tr>
						<?php
							}
						}$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) {
						?>
						<tr>
                                                               
                                                                
                                                               
                                                               
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp; </td>                                                                



                                                        </tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="pagecontrol">


             </div>
		</div>
	</body>
</html>
