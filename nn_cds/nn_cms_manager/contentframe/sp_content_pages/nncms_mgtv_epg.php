<?php header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;



$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$group_name = $_GET["group_name"];
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}

// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv/mgtv_init.php';

	
if(isset($_GET['op'])){
	if($_GET['op']=='resend_c2_task'){
		$query = array(
			'c2_task_id'=>$_GET['c2_task_id'],
			'action'=>$_GET['action'],
			'status'=>$_GET['status'],
		);
		//?op=resend_c2_task&action=add&status=1&
		$re = content_task_model::resend_c2_task($query);
		if($re){
			$re_str = '操作成功';
		}else{
			$re_str = '操作失败';
		}
		echo '<script>alert("'.$re_str.'");</script>';
	}
	unset($_GET['op']);unset($_GET['c2_task_id']);
}


if(isset($_GET['epg'])){
	if($_GET['epg']=='import'){
		$query = array(
			'action'=>$_GET['action'],
			'nns_id'=>$_GET['nns_id'],
		);
		$re = content_task_model::import_epg($query);
		if($re){
			$re_str = '操作成功';
		}else{
			$re_str = '操作失败';
		}
		echo '<script>alert("'.$re_str.'");</script>';
	}
	unset($_GET['epg']);unset($_GET['action']);unset($_GET['nns_id']);
}

$sp_id = $_GET['sp_id'];
$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 12;
$page_num = isset($_GET['page'])?$_GET['page']:1;
if($page_num<1){
	$page_num = 1;
}
$offset = intval($page_num-1)*$page_size;
$limit = $page_size;

$filter = null;
if($_GET['actionsearch']=='search'){
    $_GET['nns_name'] = trim( $_GET['nns_name']);
	$filter['nns_name'] = $_GET['nns_name'];
	$filter['nns_type'] = $_GET['nns_type'];
	$filter['nns_state'] = $_GET['nns_state'];
	$filter['nns_action'] = $_GET['nns_action_state'];
	$filter['day_picker_start'] = $_GET['day_picker_start'];
	$filter['day_picker_end'] = $_GET['day_picker_end'];
	
}
$result = content_task_model::get_vod_task_list($sp_id,$filter,$offset,$limit);
$vod_total_num = $result['rows'];
$vod_pages = ceil($vod_total_num / $page_size);//总页数
if($page_num>$vod_pages){
	$page_num = $vod_pages;
}
//"nncms_content_task.php?sp_id=<?php echo $sp_id\>\&ajax=task_action&ids="+ids+"&action="+action

if($_GET['ajax']=='task_action'){
	$action = $_GET['action'];
	$ids = $_GET['ids'];
	$re = content_task_model::resend_c2_task_all($action,$ids);
	if($re){
			$re_str = '操作成功';
		}else{
			$re_str = '操作失败';
		}
	echo json_encode($re_str);die;	
}

$currentpage = $page_num;
$g_manager_list_max_num = $page_size;
//$nns_url = "&view_list_max_num=$page_size&sp_id=$sp_id";
//$refresh = "?page=$page_num&sp_id=$sp_id&view_list_max_num=";
unset($_GET['page']);
$nns_url = "&".http_build_query($_GET);//"&view_list_max_num=$page_size&sp_id=$sp_id";
$refresh = "?".http_build_query($_GET);//"?page=$page_num&sp_id=$sp_id&view_list_max_num=";

$data = $result['data'];

//print_r($data);

?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript">
			function refresh_vod_page() {

				var num = $("#nns_list_max_num").val();
				//window.parent.refresh_vod_page_num(num);
				window.location.href='<?php echo $refresh;?>&view_list_max_num='+num
			}
			
			

			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {

						BoxKey = true;
						return false;
					}

				})
				return BoxKey;
			}
			
			function add_task_tmp(nns_id){				
				$.get("nncms_content_task.php?sp_id=<?php echo $sp_id?>&ajax=add&nns_id="+nns_id,function(result){					
					alert(result);window.location.reload();
				},'json');
			}
			
			function stop_task_tmp(nns_id){				
				$.get("nncms_content_task.php?sp_id=<?php echo $sp_id?>&ajax=stop&nns_id="+nns_id,function(result){					
					alert(result);window.location.reload();					
				},'json');
			}
			
			function rsync_info(nns_id){				
				$.get("nncms_content_task.php?sp_id=<?php echo $sp_id?>&ajax=rsync&nns_id="+nns_id,function(result){					
					alert(result);window.location.reload();
				},'json');
			}
			function delete_vod(nns_id){				
				$.get("nncms_content_task.php?sp_id=<?php echo $sp_id?>&ajax=delete_vod&nns_id="+nns_id,function(result){					
					alert(result);window.location.reload();
				},'json');
			}
			function delete_index(nns_id){				
				$.get("nncms_content_task.php?sp_id=<?php echo $sp_id?>&ajax=delete_vod_index&nns_id="+nns_id,function(result){					
					alert(result);window.location.reload();
				},'json');
			}			
			$(document).ready(function() {
				$('#clear_time').click(function(){
					$('#day_picker_start').val('');
					$('#day_picker_end').val('');
			
				});	
				window.parent.now_frame_url = window.location.href;
			});
			
			
			function task_action(action){
				
				var r=confirm("是否进行该操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					$.get("nncms_content_task.php?sp_id=<?php echo $sp_id?>&ajax=task_action&ids="+ids+"&action="+action,function(result){
						alert(result);window.location.reload();
					},'json');
				}
				
			}

		</script>
	</head>

	<body>
		<div class="content">
			<div class="content_position">
				注入管理  > 内容列表
			</div>
				<div class="content_table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<form action="" method="get">
							<tbody>
								<tr>
									<td>
										影片名/拼音：&nbsp;&nbsp;<input type="text" name="nns_name" id="nns_name" value="<?php echo $_GET['nns_name'];?>" style="width:200px;">
										&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                       类型：&nbsp;&nbsp;<select name="nns_type" style="width: 150px;">
											<option value="" >全部</option>
											<option value="vod" <?php if($_GET['nns_type']=='vod') echo 'selected="selected"'?>>影片</option>
											<option value="index" <?php if($_GET['nns_type']=='index') echo 'selected="selected"'?>>分集</option>
											<option value="media" <?php if($_GET['nns_type']=='media') echo 'selected="selected"'?>>片源</option>
                                    </select> 	
                                    &nbsp;&nbsp;&nbsp;&nbsp;									
                                                                                                      操作：&nbsp;&nbsp;<select name="nns_action_state" style="width: 150px;">
											<option value="" >全部</option>
											<option value="add" <?php if($_GET['nns_action_state']=='add') echo 'selected="selected"'?>>添加</option>
											<option value="modify" <?php if($_GET['nns_action_state']=='modify') echo 'selected="selected"'?>>修改</option>
											<option value="destroy" <?php if($_GET['nns_action_state']=='destroy') echo 'selected="selected"'?>>删除</option>
                                    </select> 
                                     &nbsp;&nbsp;&nbsp;&nbsp;
										状态：&nbsp;&nbsp;<select name="nns_state" style="width: 150px;">
											<option value="" >全部</option>
											<option value="content_wait" <?php if($_GET['nns_state']=='content_wait') echo 'selected="selected"'?>>等待注入影片</option>
											<option value="content_ok" <?php if($_GET['nns_state']=='content_ok') echo 'selected="selected"'?>>注入成功影片</option>
											<option value="content_fail" <?php if($_GET['nns_state']=='content_fail') echo 'selected="selected"'?>>注入失败</option>
                                    </select>
                                    &nbsp;&nbsp;&nbsp;&nbsp;                                                                     
                                    </td>
                                    </tr>
                                    <tr>
									<td>
										选择时间段：&nbsp;&nbsp;&nbsp;<input name="day_picker_start" id="day_picker_start" type="text"  value="<?php  if(isset($_GET['day_picker_start'])) echo $_GET['day_picker_start'];?>" style="width:120px;" class="datetimepicker" callback="test" />
										 - <input name="day_picker_end" id="day_picker_end" type="text"  value="<?php  if(isset($_GET['day_picker_end'])) echo $_GET['day_picker_end'];?>" style="width:120px;" class="datetimepicker" callback="test" />
										 &nbsp;&nbsp;<input type="button" id="clear_time" name="clear_time" value="清除时间"/>
										<input type="hidden" name="sp_id" value="<?php echo $sp_id;?>" />
										<input type="hidden" name="actionsearch" value="search" />
										&nbsp;&nbsp;&nbsp;&nbsp;	
										<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  onclick="refresh_vod_page();"/>
									</td>
								</tr>
							</tbody>
							</form>
						</table>
				</div>			
			<div class="content_table formtable">

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th><input name="" type="checkbox" value="">序号</th>
							<th>名称</th>
							<th>类型</th>
							<th>动作</th>
							<th>创建时间</th>
							<th>修改时间</th>
							<th>状态</th>
							<th>EPG注入状态</th>
						    <th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if($data!=null){
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) {
								$num++;
						?>
						<tr>
						<td><input name="input" type="checkbox" value="<?php echo $item["nns_id"];?>"><?php echo $num; ?></td>

                        <td>                        
                        <?php
                         if(empty($item['nns_name'])) {
                         	$name = '';
                         	if($item['nns_type']=='index'){
                         		$name = content_task_model::get_vod_index_name($item['nns_ref_id']);
                         	}else if($item['nns_type']=='vod'){
                         		$name =content_task_model::get_vod_name($item['nns_ref_id']);
                         	}
                         	content_task_model::update_c2_task_name($item['nns_id'],$name);
                         	echo $name;
                         }else{
                         	echo $item['nns_name'];
                         }
                         
                        ?>
                   
                        </td>
                        <td>
                        <?php
                        switch($item['nns_type']){
                        	case 'vod':
                        	echo '影片';
                        	break;
                        	case 'index':
                        	echo '分集';
                        	break;
							case 'live':
                        	echo '直播';
                        	break;
							case 'playbill':
                        	echo '节目单';
                        	break;
							case 'media':
                        	echo '片源';
                        	break;
                        }
                        ?>
                        </td>
                        <td>                        
                        <?php 
                        
                        switch($item['nns_action']){
                        	case 'add':
                        	echo '添加';
                        	break;
                        	case 'modify':
                        	echo '修改';
                        	break;
                        	case 'destroy':
                        	echo '删除';
                        	break;
                        }
                        
                        ?>
                   
                        </td>
                        <td><?php echo $item['nns_create_time'];?></td>                        
                       <td><?php echo $item['nns_modify_time'];?></td>   
                        <td>	
                        <?php
                        $nns_status = 0;
                        if($item['nns_status']=='-1'){
                        	echo '注入失败'; 
							$nns_status = 1;
                        }
                        else if($item['nns_status']=='0' && $item['nns_action']=='destroy'){
                        	echo '影片删除成功';  
                        }else if($item['nns_status']=='0' && ($item['nns_action']=='add' ||$item['nns_action']=='modify') ){
                        	echo '影片注入成功';
							$nns_status = 2;
                        }else if($item['nns_status']!='0' && $item['nns_action']=='destroy' ) {
                        	echo '等待删除';
                        }else if($item['nns_status']!='0'&& ($item['nns_action']=='add' ||$item['nns_action']=='modify') ) {
                        	echo '等待注入';
							$nns_status = 3;
                        }					                      
						?>
						</td>
						<td>
							<?php
							if($item['epg']==1&&$item['count']==1){
								switch($item['action']){
		                        	case 'add':
		                        	echo '添加';
		                        	break;
		                        	case 'modify':
		                        	echo '修改';
		                        	break;
		                        	case 'destroy':
		                        	echo '删除';
		                        	break;
		                        }
								if($item['status']==0){
									echo '成功';
								}else{
									echo '失败';
								}
							}elseif($item['epg']==1&&$item['count']==0){
								echo '未注入EPG';
							}
							?>
						</td>
                        <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                        <a href="nncms_content_task.php?op=resend_c2_task&action=add&status=1&c2_task_id=<?php echo $item['nns_id']?><?php echo $nns_url . $search_url; ?>">添加</a>
                        <a href="nncms_content_task.php?op=resend_c2_task&action=modify&status=2&c2_task_id=<?php echo $item['nns_id']?><?php echo $nns_url . $search_url; ?>">修改</a>
                        <a href="nncms_content_task.php?op=resend_c2_task&action=destroy&status=3&c2_task_id=<?php echo $item['nns_id']?><?php echo $nns_url . $search_url; ?>">删除</a>
                        <?php
                        if($item['epg']==1){
                        ?>
                        <a href="nncms_content_task.php?epg=import&action=add&nns_id=<?php echo $item['nns_id'];?>">注入EPG内容</a>
                        <a href="nncms_content_task.php?epg=import&action=modify&nns_id=<?php echo $item['nns_id'];?>">修改EPG内容</a>
                        <a href="nncms_content_task.php?epg=import&action=destroy&nns_id=<?php echo $item['nns_id'];?>">删除EPG内容</a>
                        <?php	
                        }
                        ?>
                        </td>
                        </tr>
						<?php
							}
						}$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) {
						?>
						<tr>
                                                                                                                                                                                          
                                                               <td>&nbsp;</td>
                                                               <td>&nbsp;</td>
                                                               <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp;</td>
                                                                 <td>&nbsp;</td>
                                                                 <td>&nbsp;</td>


                                                        </tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="pagecontrol">
			  共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num;?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_task.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_task.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_content_task.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_task.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_task.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

                        </div>
                        
                 <div class="controlbtns">
    	<div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
        <div class="controlbtn move"><a href="javascript:task_action('add');">添加</a></div>
        <div class="controlbtn move"><a href="javascript:task_action('modify');">修改</a></div>
        <div class="controlbtn delete"><a href="javascript:task_action('destroy');">删除</a></div>
        <div style="clear:both;"></div>
    </div>       
                        
		</div>
	</body>
</html>
