<?php 
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135006");
$checkpri = null;
if (!$pri_bool) {
	header("Location: ../nncms_content_wrong.php");
	exit ;
}
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv/mgtv_init.php';
$query = array();
$query['sp_id'] = $_GET['sp_id'];
$query['nns_id'] = $_GET['nns_id'];
$query['nns_video_id'] = $_GET['nns_video_id'];
$query['nns_all_index'] = $_GET['nns_all_index'];


$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 12;
$page_num = isset($_GET['page'])?$_GET['page']:1;



if($page_num<1){
	$page_num = 1;
}

$offset = intval($page_num-1)*$page_size;
$limit = $page_size;



$content_detail = category_model::get_content_info($query,$offset,$limit);
$vod_total_num = $content_detail['rows'];
$vod_pages = ceil($vod_total_num / $page_size);//总页数

$info = $content_detail['info'];
$data = $content_detail['index_list'];

$currentpage = $page_num;
$g_manager_list_max_num = $page_size;
//$nns_url = "&category_id=$category_id&sp_id=$sp_id&view_list_max_num=$page_size";
//$search_url="&search_py=$search";

unset($_GET['page']);
$nns_url = "&".http_build_query($_GET);//"&view_list_max_num=$page_size&sp_id=$sp_id";
$refresh = "?".http_build_query($_GET);//"?page=$page_num&sp_id=$sp_id&view_list_max_num=";
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
		<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>	
		<script type="text/javascript" src="../../js/dtree.js"></script>
		<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript">			
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				});
				return BoxKey;
			}
			$(document).ready(function() {
				$('#clear_time').click(function(){
					$('#day_picker_start').val('');
					$('#day_picker_end').val('');
				});	
				window.parent.now_frame_url = window.location.href;
				$(".selectbox").hide();
				
			});
			
			function show_import_log(org_id,video_id,video_index_id){
					$("#select_frame").attr("src", "");
					$("#select_frame").attr("src", "./nncms_import_log.php?method=index&sp_id="+org_id+"&vod_id="+video_id+"&video_index_id="+video_index_id);
                    $(".selectbox").show();
			}
			function close_select() {
				$(".selectbox").hide();
			}
		</script>
	</head>
	<body>
		<div class="selectbox">
			<iframe scrolling="no" frameborder="0"  id="select_frame" ></iframe>
		</div>
		<div class="content">
			<div class="content_position">
				[<?php echo $info['nns_video_name'];?>]内容注入日志
			</div>							
			<div class="content_table">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<form action="" method="get">
					<tbody>
						<tr>
							<td>
								内容注入列表
							</td>
						</tr>
					</tbody>
					</form>
				</table>
			</div>
			<div class="content_table formtable">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th>序号</th>
						<th>名称</th>
						<th>注入状态</th>
						<th>切片状态</th>
						<th>切片注入状态</th>
						<th>操作</th>			   
					</tr>
				</thead>
				<tbody>
					<?php
						if($data!=null){
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) {
								$num++;
						?>
					<tr>
						<td><?php echo $num;?>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $item['nns_id'];?></td>
						<td><?php echo $item['nns_name'];?></td>
						<td>
							<?php
                        if($item['nns_status']=='-1'){
                        	echo '注入失败'; 
                        }
                        else if($item['nns_status']=='0' && $item['nns_action']=='destroy'){
                        	echo '影片删除成功';  
                        }else if($item['nns_status']=='0' && ($item['nns_action']=='add' ||$item['nns_action']=='modify') ){
                        	echo '影片注入成功';  
                        }else if($item['nns_status']!='0' && $item['nns_action']=='destroy' ) {
                        	echo '等待删除';
                        }else if($item['nns_status']!='0'&& ($item['nns_action']=='add' ||$item['nns_action']=='modify') ) {
                        	echo '等待注入';
                        }
					                      
						?>
						</td>
						<td>
						<?php 
						$nns_state = $item['clip_state'];
						if(empty($nns_state)){
							$str= "未执行切片任务";
						}elseif($nns_state=='c2_ok'){
							$str= "切片完成";
						}elseif($nns_state=='handle'){
							$str= "切片任务已下发";
						}elseif($nns_state=='download_fail'){
							$str= "下载片源失败";
						}elseif($nns_state=='download_succ'){
							$str= "下载片源成功";
						}elseif($nns_state=='cliping'){
							$str= "正在切片";
						}elseif($nns_state=='cancel'){
							$str= "切片任务取消";
						}elseif($nns_state=='c2_fail'){
							$str= "切片失败";
						}elseif($nns_state=='clip_exists'){
							$str= "切片存在";
						}elseif($nns_state=='unkown'){
							$str= "未知错误";
						}else{
							$str= "没有生成切片任务";
						}
						echo $str;
						?>
						</td>
						<td>
						 <?php if($nns_state=='c2_ok'){?>		  
                         <?php if(false!== strpos($item['movie_nns_result'],'[0]')) {echo '成功';}else {echo '<font color="red">失败</font>'.$item['movie_nns_result'];} ?>
                         <?php if($item['movie_nns_again']=='-1' && $item['movie_nns_notify_result']=='-1') echo '&nbsp;&nbsp;|&nbsp;<font color="red">重发失败</font>';?>
                        	<?php }else{
                        		echo '切片未完成无注入状态';
                        	} ?>
                        </td>
                        <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                        	<a href="javascript:void();" onclick="show_import_log('<?php echo $item['nns_org_id'];?>','<?php echo $item['nns_src_id'];?>','<?php echo $item['nns_ref_id'];?>')">查看影片信息注入记录</a>
                        </td>
						
					</tr>
					<?php
							}
						}$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) {
					?>
					<tr>
                                                                
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp; </td>


                                                        </tr>
						<?php } ?>
				</tbody>
				</table>
			</div>
			
			<div class="pagecontrol">
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_log_detail.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_log_detail.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_content_log_detail.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_log_detail.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_log_detail.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

                        </div>						
		</div>
	</body>
</html>
<script language="javascript">			
		$(".selectbox").css("top",100);
</script>
