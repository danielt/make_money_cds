<?php
header("Content-Type:text/html;charset=utf-8");
ini_set('display_errors',1);
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "135008");
$checkpri = null;



$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$group_name = $_GET["group_name"];
if (!$pri_bool) {
        Header("Location: ../nncms_content_wrong.php");
        exit;
}

// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";


$base_id = 10000;
if (!empty($_COOKIE["page_max_num"])) {
        $g_manager_list_max_num = $_COOKIE["page_max_num"];
}
$sp_id = $_GET['sp_id'];
include_once $nncms_config_path . 'mgtv/mgtv_init.php';
//include_once $nncms_config_path . 'mgtv/models/asset_category_model.php';
$data = category_content_model::statistics();
echo '.';
//print_r($data);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
		<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
		<link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/dtree.js"></script>
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>		
		<script language="javascript" src="../../js/amcharts.js"></script>
		<script type="text/javascript">
            var chart;
            var chart_index;
            var chart_clip;

            var chartData = [{
                mgtv: "MGTV注入影片",
                visits: <?php echo $data['vod'];?>
            }, {
                mgtv: "注入华为成功影片",
                visits: <?php echo $data['vod_huawei_success'];?>
            }, {
                mgtv: "等待注入华为影片",
                visits: <?php echo $data['vod_huawei_wait'];?>
            }, {
                mgtv: "注入华为失败影片",
                visits: <?php echo $data['vod_huawei_failed'];?>
            }];
            
            var chartData_index = [{
                mgtv: "MGTV注入分集",
                visits: <?php echo $data['vod_index'];?>
            }, {
                mgtv: "注入华为成功分集",
                visits: <?php echo $data['vod_index_huawei_success'];?>
            }, {
                mgtv: "等待注入华为分集",
                visits: <?php echo $data['vod_index_huawei_wait'];?>
            }, {
                mgtv: "注入华为失败分集",
                visits: <?php echo $data['vod_index_huawei_failed'];?>
            }];
            
            var chartData_clip = [{
                mgtv: "MGTV注入片源",
                visits: <?php echo $data['vod_media'];?>
            }, {
                mgtv: "注入华为成功片源",
                visits: <?php echo $data['vod_media_clip_success'];?>
            }, {
                mgtv: "等待注入华为片源",
                visits: <?php echo $data['vod_media_clip_wait'];?>
            }, {
                mgtv: "注入华为失败片源",
                visits: <?php echo $data['vod_media_clip_failed'];?>
            }];


            AmCharts.ready(function () {
                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();
                chart.dataProvider = chartData;
                chart.categoryField = "mgtv";
                chart.startDuration = 1;
                
                chart_index = new AmCharts.AmSerialChart();
                chart_index.dataProvider = chartData_index;
                chart_index.categoryField = "mgtv";
                chart_index.startDuration = 1;
                
                
                chart_clip = new AmCharts.AmSerialChart();
                chart_clip.dataProvider = chartData_clip;
                chart_clip.categoryField = "mgtv";
                chart_clip.startDuration = 1;

                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.labelRotation = 20;
                categoryAxis.gridPosition = "start";
                
                var categoryAxis_index = chart_index.categoryAxis;
                categoryAxis_index.labelRotation = 20;
                categoryAxis_index.gridPosition = "start";
                
                var categoryAxis_clip = chart_clip.categoryAxis;
                categoryAxis_clip.labelRotation = 20;
                categoryAxis_clip.gridPosition = "start";

                // value
                // in case you don't want to change default settings of value axis,
                // you don't need to create it, as one value axis is created automatically.

                // GRAPH
                var graph = new AmCharts.AmGraph();
                graph.valueField = "visits";
                graph.balloonText = "[[category]]: [[value]]";
                graph.type = "column";
                graph.lineAlpha = 0;
                graph.fillAlphas = 0.8;
                chart.addGraph(graph);
                chart.write("chartdiv");
                
                
                
                var graph_index = new AmCharts.AmGraph();
                graph_index.valueField = "visits";
                graph_index.balloonText = "[[category]]: [[value]]";
                graph_index.type = "column";
                graph_index.lineAlpha = 0;
                graph_index.fillAlphas = 0.8;
                chart_index.addGraph(graph_index);
                chart_index.write("chartdiv_index");
                
                
                var graph_clip = new AmCharts.AmGraph();
                graph_clip.valueField = "visits";
                graph_clip.balloonText = "[[category]]: [[value]]";
                graph_clip.type = "column";
                graph_clip.lineAlpha = 0;
                graph_clip.fillAlphas = 0.8;
                chart_clip.addGraph(graph_clip);
                chart_clip.write("chartdiv_clip");
            });
        </script>
		
	</head>

	<body>

		<div class="content">
			<div class="content_position" style="margin-top: -12px;">
				数据统计
			</div>				
				<div class="content_table">
					<div id="chartdiv" style="width: 300; height: 450px;float: left;"></div>
					<div id="chartdiv_index" style="width: 300; height: 450px;float: left;"></div>	
					<div id="chartdiv_clip" style="width: 300; height: 450px;float: left;"></div>					
				</div>
		</div>
	</body>
</html>