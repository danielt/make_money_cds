<?php
class nl_vod_audit_notify_control
{
	public static  $dc = null; 
	public static $desc = null;
	
	/**
	 * 发送审核状态信息
	 * @param object $dc 数据库操作对象
	 * @param unknown $nns_ids 审核队列的guid
	 * @param unknown $sp_id 运营商id
	 * @param unknown $check 审核状态   0 未审核 | 1 审核
	 * @return multitype:number string |multitype:number NULL
	 * @author liangpan
	 * @date 2015-09-10
	 */
	public static function message_notify_check($dc,$nns_ids,$sp_id,$check)
	{
		self::$dc = $dc;
		$array_id = explode(',', $nns_ids);
		if(empty($array_id) || !is_array($array_id))
		{
			return array(
					'ret' => 1,
					'reason' => '没有传入参数id'
			);
		}
		$check = intval($check);
		foreach ($array_id as $val)
		{
			$result=nl_vod_audit::get_vod_audit_list(self::$dc, array('nns_id'=>$val),array('page_count'=>1));
			if($result['ret'] !=0)
			{
				continue;
			}
			if(!isset($result['data']['data_info'][0]) || empty($result['data']['data_info'][0]))
			{
				self::$desc.='[数据为空，id为:' . $val . ']';
				continue;
			}
			$result = $result['data']['data_info'][0];
			if($check == $result['nns_check'])
			{
				$str_check = ($check == 2) ? '未审核' : '已审核';
				self::$desc.='[数据已经为'.$str_check.'状态，无需再次处理,id为:' . $val . ']';
				continue;
			}
			$result_asset = nl_vod_index_media::query_video_index_media_info_by_import_id(self::$dc, $result['nns_import_id'],$result['nns_video_type'],$result['nns_cp_id']);
			if(!isset($result_asset['data']['data_info'][0]) || empty($result_asset['data']['data_info'][0]))
			{
				self::$desc.='[数据为空，注入id为:' . $result['nns_import_id'] . ']';
				continue;
			}
			if($result['nns_video_type'] == 'index' || $result['nns_video_type'] == 'media')
			{
				$mix_id = $result['nns_parent_import_id'];
				$result_mix=nl_vod_audit::get_vod_audit_list(self::$dc, array('nns_import_id'=>$mix_id,'nns_org_id'=>$sp_id,'nns_cp_id'=>$result['nns_cp_id']),array('page_count'=>1));
				if($result_mix['ret'] !=0 || !isset($result_mix['data']['data_info'][0]['nns_check']))
				{
					continue;
				}
				$str_desc = ($result['nns_video_type'] == 'index') ?  "主媒资" : "分集"; 
				if($result_mix['data']['data_info'][0]['nns_check'] != $check)
				{
					if($result_mix['data']['data_info'][0]['nns_check'] == 0)
					{
						self::$desc.='['.$str_desc.'为未审核状态请先审核'.$str_desc.'，id为:' . $val . ']';
						continue;
					}
					else if($result_mix['data']['data_info'][0]['nns_check'] == 2)
					{
						self::$desc.='['.$str_desc.'为取消审核状态请先审核'.$str_desc.'，id为:' . $val . ']';
						continue;
					}
				}
			}
			$nns_id = $result_asset['data']['data_info'][0]['nns_id'];
			
			nl_vod_audit::modify_vod_audit_checkstate(self::$dc, $result['nns_id'], $check,'check');
			$result_notify=nl_vod_audit_notify_action::push_message_notify(self::$dc,$nns_id,$check,$sp_id,'check',$result['nns_video_type'],$result['nns_action'],$result['nns_name']);
			if($result_notify['ret'] !=0)
			{
				self::$desc.='[数据为空，注入id为:' . $result['nns_import_id'] . ',原因：'.$result['reason'].']';
				continue;
			}
		}
		return array(
					'ret' => 0,
					'reason' => self::$desc
		); 
	}
	
	/**
	 * 发送上下线状态信息
	 * @param object $dc 数据库操作对象
	 * @param string $nns_ids 审核队列的guid
	 * @param string $sp_id 运营商id
	 * @param number $state  上下线状态   1 上线|2 下线
	 * @param boolean $opreation  false 不强制上线 | true 强制上线 
	 * @return multitype:number string |multitype:number NULL
	 * @author liangpan
	 * @date 2015-09-10
	 */
	public static function message_notify_state($dc,$nns_ids,$sp_id,$state,$opreation = false)
	{
		self::$dc = $dc;
		$array_id = explode(',', $nns_ids);
		if(empty($array_id) || !is_array($array_id))
		{
			return array(
					'ret' => 1,
					'reason' => '没有传入参数id'
			);
		}
		$state = intval($state);
		foreach ($array_id as $val)
		{
			$result=nl_vod_audit::get_vod_audit_list(self::$dc, array('nns_id'=>$val),array('page_count'=>1));
			if($result['ret'] !=0)
			{
				return $result;
			}
			if(!isset($result['data']['data_info'][0]) || empty($result['data']['data_info'][0]))
			{
				self::$desc.='[数据为空，id为:' . $val . ']';
				continue;
			}
			$result = $result['data']['data_info'][0];
			if($state == $result['nns_state'])
			{
				$str_state = ($state == 1) ? '上线' : '下线';
				self::$desc.='[数据已经为'.$str_state.'状态，无需再次处理,id为:' . $val . ']';
				continue;
			}
			if($result['nns_video_type'] == 'index' || $result['nns_video_type'] == 'media')
			{
				$mix_id = $result['nns_parent_import_id'];
				$result_mix=nl_vod_audit::get_vod_audit_list(self::$dc, array('nns_import_id'=>$mix_id,'nns_org_id'=>$sp_id,'nns_cp_id'=>$result['nns_cp_id']),array('page_count'=>1));
				if($result_mix['ret'] !=0 || !isset($result_mix['data']['data_info'][0]['nns_check']))
				{
					continue;
				}
				$str_desc = ($result['nns_video_type'] == 'index') ?  "主媒资" : "分集";
				if($result_mix['data']['data_info'][0]['nns_state'] != $state)
				{
					if($result_mix['data']['data_info'][0]['nns_state'] == 0)
					{
						self::$desc.='['.$str_desc.'为未处理状态请先上下线'.$str_desc.'，id为:' . $val . ']';
						continue;
					}
					else if($result_mix['data']['data_info'][0]['nns_state'] == 2)
					{
						self::$desc.='['.$str_desc.'为下线状态请先下线'.$str_desc.'，id为:' . $val . ']';
						continue;
					}
				}
			}
			if($result['nns_check'] != 1 && !$opreation)
			{
				self::$desc.='[数据未审核，不能上下线处理,注入id为:' . $result['nns_import_id'] . ']';
				continue;
			}
			else
			{
				if($result['nns_check'] != $state)
				{
					nl_vod_audit::modify_vod_audit_checkstate(self::$dc, $result['nns_id'], 1, 'check');
					nl_vod_audit_notify_action::push_message_notify(self::$dc,$nns_id,1,$sp_id,'check',$result['nns_video_type'],$result['nns_action'],$result['nns_name']);
					@sleep(1);
				}
			}
			$result_asset = nl_vod_index_media::query_video_index_media_info_by_import_id(self::$dc, $result['nns_import_id'],$result['nns_video_type'],$result['nns_cp_id']);
			if(!isset($result_asset['data']['data_info'][0]) || empty($result_asset['data']['data_info'][0]))
			{
				self::$desc.='[数据为空，注入id为:' . $result['nns_import_id'] . ']';
				continue;
			}
			$nns_id = $result_asset['data']['data_info'][0]['nns_id'];
			nl_vod_audit::modify_vod_audit_checkstate(self::$dc, $result['nns_id'], $state, 'line');
			$result_notify=nl_vod_audit_notify_action::push_message_notify(self::$dc,$nns_id,$state,$sp_id,'line',$result['nns_video_type'],$result['nns_action'],$result['nns_name']);
			if($result_notify['ret'] !=0)
			{
				self::$desc.='[数据为空，注入id为:' . $result['nns_import_id'] . ',原因：'.$result['reason'].']';
				continue;
			}
		}
		return array(
				'ret' => 0,
				'reason' => self::$desc
		);
	}
	
}