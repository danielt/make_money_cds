<?php 
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$group_name = $_GET["group_name"];
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/vod_audit/vod_audit_log.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
		"db_policy" => NL_DB_WRITE,
		"cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));

$sp_id = $_GET['sp_id'];
$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 18;
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) {
	$page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit_array = array(
		'page_size'=>$page_size,
		'page_num'=> $page_num,
);

$filter_array =array(
		'nns_operator'=> isset($_GET['nns_operator']) ? trim($_GET['nns_operator']) : null,
		'nns_vod_name'=> isset($_GET['nns_vod_name']) ? $_GET['nns_vod_name'] : null,
		'nns_action'=> isset($_GET['nns_action']) ? trim($_GET['nns_action']) : null,
		'nns_state'=> isset($_GET['nns_state']) ? $_GET['nns_state'] : null,
		'nns_org_id'=> $sp_id,
		'nns_begin_time'=> isset($_GET['nns_cp_id']) ? $_GET['nns_cp_id'] : null,
		'nns_end_time'=> isset($_GET['nns_end_time']) ? $_GET['nns_end_time'] : null,
);
$result_data = nl_vod_audit_log::query($dc,$filter_array,$limit_array);
if($result_data['ret'] != 0)
{
	echo '<script>alert("'.$result_data['reason'].'");history.go(-1);</script>';die;
}

$data = $result_data['data']['data_info'];
$vod_total_num = $result_data['data']['page_info']['total_count'];
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) {
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;

$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);

// $data = $result['data']; 
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<style>
.mytip_ul
{
    text-align: left;
    font-size: 12px;
    list-style-type: none;
    padding: 0;
    margin: 0;
}

.mytip_title
{
    margin-left: 10px;
    width: 100px;
    float: left;
    text-align: left;
}

.mytip_close
{
    margin-right: 10px;
    margin-top: 5px;
    width: 100px;
    float: right;
    text-align: right;
    cursor: pointer;
}

.mytip_top
{
    background-image: url(../../images/tips_titlebj.gif);
    color: #FFFFFF;
    font-size: 12px;
    height: 24px;
    line-height: 24px;
    font-weight: 600;
}

.tips_li
{
    height: 25px;
    line-height: 25px;
    border-bottom: #ccc 1px solid;
}

.tips_li_span
{
    color: #07507d;
    margin-left: 10px;
}

.tips_li_span2
{
    color: #6d6d6d;
}		
.myalert_div1
{
    height: 25px;
    line-height: 25px;
    border-bottom: #ccc 1px solid;
    width: 620px;
}

.myalert_div1_leftDiv
{
    float: left;
    width: 50%;
    margin-left: 10px;
}

.myalert_div1_rightDIv
{
    float: left;
    margin-left: 10px;
}

.myalert_div1_Div
{
    float: left;
    width: 100%;
    margin-left: 10px;
}

.myalert_div2
{
    height: 84px;
    line-height: 70px;
    border-bottom: #ccc 1px solid;
    width: 620px;
}

.myalert_div2_Div
{
    float: left;
    width: 100%;
    margin-left: 10px;
    height: 84px;
}

.myaelrt_li_title
{
    color: #07507d;
}

.myalert_li_txt
{
    color: #6d6d6d;
}

.myalert_inputTxt
{
    width: 220px;
    border: 0px solid #eff0f0;
    padding-left: 2px;
    background-color: #eff0f0;
    color: #6d6d6d;
}		
		</style>		
		
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
		<script language="javascript">

			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				})
				return BoxKey;
			}
			
			
			function queue(){
				var r=confirm("是否进行该操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "nncms_import_queue_task.php?sp_id=<?php echo $sp_id?>&op=queue&ids="+ids;
						window.location.href = url;
					}
				}
			}

			
			$(document).ready(function() {
				$('#clear_time').click(function(){
					$('#nns_begin_time').val('');
					$('#nns_end_time').val('');

				});
				window.parent.now_frame_url = window.location.href;
			});
			
		</script>

	</head>

	<body>
		<div class="content">
			<div class="content_position">
				中心流程指向
			</div>
				<div class="content_table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<form action="" method="get">
							<tbody>
								<tr>
									<td>									
										操作人：
											<input type="text" name="nns_operator" id="nns_operator" value="<?php echo $_GET['nns_operator']; ?>" style="width:150px;">
	                                   	操作内容：
											<input type="text" name="nns_vod_name" id="nns_vod_name" value="<?php echo $_GET['nns_vod_name']; ?>" style="width:150px;">
	                                    &nbsp;&nbsp;是否禁用：
	                                    	<select name="nns_state" style="width: 100px;">
												<option value="" >全部</option>
												<option value="pass" <?php if($_GET['nns_state']=='pass') echo 'selected="selected"'?>>通过</option>
												<option value="refused" <?php if($_GET['nns_state']=='refused') echo 'selected="selected"'?>>不通过</option>
												<option value="online" <?php if($_GET['nns_state']=='online') echo 'selected="selected"'?>>上线</option>
												<option value="unline" <?php if($_GET['nns_state']=='unline') echo 'selected="selected"'?>>下线</option>
                                    		</select>
                                    	&nbsp;&nbsp;行为操作：
	                                    	<select name="nns_action" style="width: 100px;">
												<option value="" >全部</option>
												<option value="destory" <?php if($_GET['nns_state']=='destory') echo 'selected="selected"'?>>删除</option>
												<option value="add" <?php if($_GET['nns_state']=='add') echo 'selected="selected"'?>>添加</option>
                                    		</select>
										&nbsp;&nbsp;选择时间段：
											<input name="nns_begin_time" id="nns_begin_time" type="text"  value="<?php
												if (isset($_GET['nns_begin_time']))
													echo $_GET['nns_begin_time'];
											?>" style="width:120px;" class="datetimepicker" callback="test" />
										- 
										 	<input name="nns_end_time" id="nns_end_time" type="text"  value="<?php
												if (isset($_GET['nns_end_time']))
													echo $_GET['nns_end_time'];
											?>" style="width:120px;" class="datetimepicker" callback="test" />
										
										 	<input type="button" id="clear_time" name="clear_time" value="清除时间"/>
											<input type="hidden" name="sp_id" value="<?php echo $sp_id; ?>" />
											<input type="hidden" name="actionsearch" value="search" />
										&nbsp;&nbsp;	
										<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  />										
									</td>
								</tr>
							</tbody>
							</form>
						</table>
				</div>			
			<div class="content_table formtable">

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th><input name="" type="checkbox" value="">序号</th>
							<th>操作人</th>
							<th>操作内容</th>
							<th>操作行为</th>
							<th>操作动作</th>
							<th>CP ID</th>
							<th>创建时间</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(is_array($data) && !empty($data)){
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) {
								$num++;
						?>
						<tr>
						<td id="is_delete"><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
						<td ><?php echo $item['nns_operator']; ?></td>
						<!-- 
						<td ><?php echo $item['nns_vod_name']; ?></td>
						 -->
						<td><a href="nncms_vod_audit_log_xml.php?sp_id=<?php echo $sp_id; ?>&nns_id=<?php echo $item["nns_id"]; ?>" target='_blank' > 查看内容 </a></td>
                        <td ><?php if($item['nns_action'] =='add'){ echo "添加"; }else{ echo "删除"; } ?></td>
                        <td ><?php if($item['nns_state'] =='pass'){ echo "通过"; }else if($item['nns_state'] =='refused'){ echo "不通过"; }else if($item['nns_state'] =='online'){ echo "上线"; }else{ echo "下线"; } ?></td>
                        <td ><?php echo $item['nns_cp_id']; ?></td>
                        <td ><?php echo $item['nns_create_time']; ?></td>                        
                        
                        </tr>
						<?php
							}
						}$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) {
						?>
						<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="pagecontrol">
			  共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_vod_audit_log_list.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_vod_audit_log_list.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_vod_audit_log_list.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_vod_audit_log_list.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_vod_audit_log_list.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

                        </div>
                        
                 <div class="controlbtns">
    </div>    
                        
		</div>
	</body>
</html>
