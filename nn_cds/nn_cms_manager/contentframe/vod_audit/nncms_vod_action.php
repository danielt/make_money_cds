<?php
class nl_vod_action
{
	public static function check_vod_perfect($dc,$vod_id)
	{
		$result_index_media_info = nl_vod_index_media::query_vod_index_media_info($dc, $vod_id);
		$temp_range_info=$temp_index_info=$temp_media_info=$temp_array_info=array();
		if($result_index_media_info['ret'] != '0')
		{
			return array(
					'index'=> $result_index_media_info['reason'],
					'media'=>'',
					'flag'=>false,
			);
		}
		$result_index_media_info = $result_index_media_info['data'];
		if(empty($result_index_media_info) || !is_array($result_index_media_info['data_info']) || !is_array($result_index_media_info['other_info']))
		{
			return array(
					'index'=> '缺少分集',
					'media'=> '缺少片源',
					'flag'=>false,
			);
		}
		$temp_array_info['flag'] = false;
		$total_index =  $result_index_media_info['other_info'][0]['max'] - $result_index_media_info['other_info'][0]['min'] +1;
		if($total_index == $result_index_media_info['other_info'][0]['count'])
		{
			$temp_array_info['index'] = '分集正常';
			foreach ($result_index_media_info['data_info'] as $val)
			{
				if(empty($val['nns_media_id']))
				{
					$temp_media_info[]=$val['nns_index']+1;
				}
			}
		}
		else
		{
			$temp_range_info=range(1, $result_index_media_info['other_info'][0]['max']+1);
			foreach ($result_index_media_info['data_info'] as $val)
			{
				$temp_index_info[]=$val['nns_index']+1;
				if(empty($val['nns_media_id']))
				{
					$temp_media_info[]=$val['nns_index']+1;
				}
			}
			$temp_index_info = array_diff($temp_range_info, $temp_index_info);
			$temp_media_info = array_merge($temp_media_info,$temp_index_info);
			if(empty($temp_index_info))
			{
				$temp_array_info['index'] = '分集正常1';
			}
			else
			{
				if(count($temp_index_info) > 100)
				{
					$temp_array_info['index'] = '分集缺失100集以上';
				}
				else
				{
					$temp_array_info['index'] = "第[".implode('],[', $temp_index_info)."]集分集缺失";
				}
			}
		}
		if(empty($temp_media_info))
		{
			$temp_array_info['flag'] = true;
			$temp_array_info['media'] = '片源正常';
			unset($temp_range_info);
			unset($temp_index_info);
			unset($temp_media_info);
			return $temp_array_info;
		}
		$temp_media_info = array_unique($temp_media_info);
		if(count($temp_media_info) > 100)
		{
			$temp_array_info['media'] = '片源缺失100集以上';
		}
		else
		{
			$temp_array_info['media'] = "第[".implode('],[', $temp_media_info)."]集片源缺失";
		}
		unset($temp_range_info);
		unset($temp_index_info);
		unset($temp_media_info);
		return $temp_array_info;
	}
}