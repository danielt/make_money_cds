<?php 
header("Content-Type:text/xml;charset=utf-8");
include ("../../nncms_manager_inc.php");
include_once $np_path . 'np/np_xml_to_array.class.php';
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include_once $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include_once $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}

require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/vod_audit/vod_audit_log.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
		"db_policy" => NL_DB_WRITE,
		"cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));

$sp_id = $_GET['sp_id'];
$nns_id = $_GET['nns_id'];
$result_data = nl_vod_audit_log::query($dc,array('nns_id'=>$nns_id,'nns_org_id'=>$sp_id),array('page_count'=>'1'));
if($result_data['ret'] !='0')
{
	echo '<?xml version="1.0" encoding="utf-8"?><result>'.$result_data['reason'].'</result>';
}
else
{
	echo $result_data['data']['data_info'][0]['nns_vod_name'];
}


