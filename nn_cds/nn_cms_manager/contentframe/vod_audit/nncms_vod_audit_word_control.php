<?php
/*
 * Created on 2012-3-2
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 @header("Content-Type:text/html;charset=utf-8");
 include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

$action=$_REQUEST["action"];

 //加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
if (!empty($action)){
	switch($action){
		case "edit":
		$action_str = '修改敏感词';
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136002");
		break;
		case "add":
		$action_str= '添加敏感词';
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136003");
		break;
		case "delete":
		$action_str= '删除敏感词';
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136004");
		break;
		default:
		break;
	}
}
unset($checkpri);
if (!$pri_bool) {	
	Header("Location: ../nncms_content_wrong.php");
}else{

	include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/vod_audit/vod_audit_word.class.php';
	include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
	$dc = nl_get_dc(array (
			"db_policy" => NL_DB_WRITE,
			"cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
	));
	switch($action){
		case "edit":
			$sp_id = (isset($_POST['sp_id']) && !empty($_POST['sp_id'])) ? $_POST['sp_id'] : null;
			$nns_id = (isset($_POST['nns_id']) && !empty($_POST['nns_id'])) ? $_POST['nns_id'] : null;
			$word = (isset($_POST['nns_word']) && !empty($_POST['nns_word'])) ? $_POST['nns_word'] : null;
			$enable = (isset($_POST['nns_enable']) && strlen($_POST['nns_enable']) > 0) ? $_POST['nns_enable'] : '0';
			$result=   nl_vod_audit_word::modify($dc,$nns_id,$word,$enable,$sp_id);
		break;
		case "add":
			$sp_id = (isset($_POST['sp_id']) && !empty($_POST['sp_id'])) ? $_POST['sp_id'] : null;
			$word = (isset($_POST['nns_word']) && !empty($_POST['nns_word'])) ? $_POST['nns_word'] : null;
			$enable = (isset($_POST['nns_enable']) && strlen($_POST['nns_enable']) > 0) ? $_POST['nns_enable'] : '0';
			$result=   nl_vod_audit_word::add($dc,$word,$sp_id,$enable);
		break;
		case "delete":
			$nns_ids=$_REQUEST["nns_id"];
			$nns_ids=explode(',', $nns_ids);
			$result=   nl_vod_audit_word::del($dc,$nns_ids);
		break;
		default:

		break;
	}
	if ($result["ret"]!=0){
		echo "<script>alert('". $result['reason']. "');</script>";
	}else{
		echo "<script>alert('". $result['reason']. "');</script>";
	}
	echo "<script>self.location='nncms_vod_audit_word_list.php?sp_id=".$sp_id."';</script>";
}
?>
