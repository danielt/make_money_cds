<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$action=$_GET["action"];
$nns_id=$_REQUEST["nns_id"];
$pri_bool=false;
if (!empty($action)){
	switch($action){
	case "edit":
		$action_str=cms_get_lang('save');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136002");
		break;
	case "add":
		$action_str=cms_get_lang('add');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136003");
		break;
	default:
		break;
	}
}
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");
$sp_id = $_GET['sp_id'];
//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");

if ($action=="edit"){
	include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/vod_audit/vod_audit_word.class.php';
	include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
	$dc = nl_get_dc(array (
			"db_policy" => NL_DB_WRITE,
			"cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
	));
	$edit_data = nl_vod_audit_word::query($dc,array('nns_id'=>$nns_id),array('page_count'=>1));
	
	if($edit_data['ret'] != 0 )
	{
		echo "<script>alert('". $action_str. cms_get_lang('success'). "');</script>";
		echo "<script>self.location='nncms_vod_audit_word_list.php?sp_id=".$sp_id.";</script>";
	}
	$edit_data = $edit_data['data']['data_info'][0];
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>


<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/jquery-1.9.1.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
<script language="javascript">

</script>
</head>

<body>



<div class="content">
	<div class="content_position">SP 管理 > SP 添加/修改</div>
	<form id="add_form" action="nncms_vod_audit_word_control.php" method="post">
	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
	<input name="sp_id" id="sp_id" type="hidden" value="<?php echo $sp_id;?>" />
	<input name="nns_id" id="nns_id" type="hidden"  value="<?php if($action=="edit"){echo $edit_data["nns_id"];}?>" />
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td class="rightstyle">敏感词:</td>
				<td><input name="nns_word" id="nns_word" type="text"  value="<?php if($action=="edit"){echo $edit_data["nns_word"];}?>" /></td>
			</tr>
			
			<tr>
				<td class="rightstyle">是否启用:</td>
				<td>
					<select name="nns_enable" style="width: 100px;">
						<option value="0" <?php if($action=="edit" && $edit_data['nns_enabled']=='0') echo 'selected="selected"'?>>禁用</option>
						<option value="1" <?php if($action=="edit" && $edit_data['nns_enabled']=='1') echo 'selected="selected"'?>>启用</option>
                    </select>
				</td>
			</tr>
		  </tbody>
		</table>
	</div>
	</form>
	<div class="controlbtns">
		<div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','<?php echo cms_get_lang('webkz_qrccxg'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>


</body>
</html>
