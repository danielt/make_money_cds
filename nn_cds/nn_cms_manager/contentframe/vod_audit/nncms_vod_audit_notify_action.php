<?php
class nl_vod_audit_notify_action
{
	private static $exchange_name = null;
	private static $channel_name = null;
	private static $route_name = null;
	private static $amqp_config = null;
	
	
	
	/**
	 * 推送到amqp
	 * @param string $message 
	 * @return multitype:number string
	 * @author liangpan
	 * @date 2015-09-10
	 */
	private static function add_amqp($message)
	{
		if(strlen($message) < 1)
		{
			return array(
					'ret'=>2,
					'reason'=>"params error message is empty",
			);
		}
		//创建连接和channel
		$conn = new AMQPConnection(self::$amqp_config);
		if (!$conn->connect()) {
			return array(
					'ret'=>1,
					'reason'=>"Cannot connect to the broker",
			);
		}
		$channel = new AMQPChannel($conn);
		//创建交换机对象
		$ex = new AMQPExchange($channel);
		$ex->setName(self::$exchange_name);
		
		//发送消息
		//$channel->startTransaction(); //开始事务
		
		$ex->publish($message, self::$route_name);
		//$channel->commitTransaction(); //提交事务
		
		$conn->disconnect();
		return array(
				'ret'=>0,
				'reason'=>'成功'
		);
	}
	
	private static function make_file_in_local()
	{
		
	}
	
	
	/**
	 * 推送消息的判断  开关那些是否开启
	 * @param objec $dc 数据库操作对象
	 * @param string $nns_id 媒资id | 分集 id | 片源id
	 * @param unknown $check 0 未审核 | 1 审核或则上线 | 2 下线
	 * @param string $sp_id 运营商id
	 * @param number $action check 审核 | line 上下线
	 * @param string $video_type  video 主媒资 | index 分集 | media 片源 
	 * @return multitype:number string |Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据'), multitype:NULL number string , multitype:NULL , multitype:a r y s t i n g , array>
	 * @author liangpan
	 * @date 2015-09-10
	 */
	public static function push_message_notify($dc,$nns_id,$check,$sp_id,$action,$video_type,$nns_action,$nns_name)
	{
		$sp_list_sql = "select * from nns_mgtvbk_sp where nns_id='{$sp_id}'";
		$sp = nl_db_get_one($sp_list_sql, $dc->db());
		$sp_config = json_decode($sp['nns_config'], true);
		if($action == 'check')
		{
			if(!isset($sp_config['flow_audit_notify']) || empty($sp_config['flow_audit_notify']))
			{
				return array(
						'ret'=>0,
						'reason'=>'发送审核消息开关关闭',
				);
			}
			$send_flag = $sp_config['flow_audit_notify'];
			$notify_result = ($check == '1') ? 'pass' : 'refused';
		}
		else if($action == 'line')
		{
			if(!isset($sp_config['flow_unline_notify']) || empty($sp_config['flow_unline_notify']))
			{
				return array(
						'ret'=>0,
						'reason'=>'发送上下线消息开关关闭'
				);
			}
			$send_flag = $sp_config['flow_unline_notify'];
			$notify_result = ($check == '1') ? 'online' : 'unline';
		}
		else
		{
			return array(
					'ret'=>9,
					'reason'=>'没有此认证方式'
			);
		}
		$result_asset_info=nl_vod_index_media::query_video_index_media_info($dc, $nns_id, $video_type);
		if($result_asset_info['ret'] != 0)
		{
			return $result_asset_info;
		}
		if(!isset($result_asset_info['data']['data_info'][0]) || empty($result_asset_info['data']['data_info'][0]))
		{
			return array(
					'ret'=>0,
					'reason'=>'数据库没有查询到数据'
			);
		}
		$result_asset_info = $result_asset_info['data']['data_info'][0];
		$xml = new DOMDocument( "1.0", "UTF-8" );
// 		if(!$flag && $action =='line')
// 		{
// 			if($val['nns_check'] == '0')
// 			{
// 				return array(
// 					'ret'=>8,
// 					'reason'=>'没有审核，不允许上线',
// 					'data'=>$nns_id
// 				);
// 			}
// 		}
		if(empty($send_flag))
		{
			return array(
					'ret'=>0,
					'reason'=>'成功，且不发送反馈消息'
			); 
		}
		else if($send_flag == 1)
		{
			$array_info = array(
					'nns_import_id'=>$result_asset_info['nns_import_id'],
					'nns_video_type'=>$video_type,
					'notify_result'=>$notify_result,
					'nns_cp_id'=>$result_asset_info['nns_cp_id'],
					'nns_name'=>$nns_name,
					'nns_action'=>$nns_action,
			);
			$str_xml = self::make_xml($array_info);
			$nns_operator = (isset($_SESSION['nns_mgr_name']) && strlen($_SESSION['nns_mgr_name']) > 0) ? $_SESSION['nns_mgr_name'] : null;
			nl_vod_audit_log::add($dc,$nns_operator, $str_xml, $result_asset_info['nns_cp_id'], $nns_action, $notify_result, $sp_id);
			return self::send_info_amqp($str_xml,$sp_id);
		}
		else
		{
			return array(
					'ret'=>5,
					'reason'=>'没有开发此发送方式'
			);
		}
	}
	
	/**
	 * 生成推送到amqp的xml文件
	 * @param array $notify_result
	 * @return string
	 * @author liangpan
	 * @date 2015-09-10
	 */
	private static function make_xml($notify_result)
	{
		$xml = new DOMDocument( "1.0", "UTF-8" );
		$video = $xml->createElement( "video" );
		$video->setAttribute( "id", $notify_result['nns_import_id'] );
		$video->setAttribute( "name", $notify_result['nns_name'] );
		$video->setAttribute( "video_type", $notify_result['nns_video_type'] );
		$video->setAttribute( "action", $notify_result['nns_action'] );
		$video->setAttribute( "result", $notify_result['notify_result'] );
		$video->setAttribute( "contentprovider", $notify_result['nns_cp_id'] );
		$xml->appendChild( $video );
		$str_xml =  $xml->saveXML();
		unset($xml);
		return $str_xml;
	}
	
	
	/**
	 * 发送消息的方式
	 * @param string $message  消息信息
	 * @param string $channel_name 
	 * @param string $exchange_name
	 * @param string $route_name
	 * @param string $anction   方式
	 * @return multitype:number string
	 * @author liangpan
	 * @date 2015-09-10
	 */
	private static function send_info_amqp($message,$sp_id,$anction='all_file')
	{
		if(empty($message))
		{
			return array(
					'ret'=>2,
					'reason'=>'message is empty'
			);
		}
		
		global $g_mgtv_amqp_name_config;
		if(isset($g_mgtv_amqp_name_config[$sp_id]['channel_name']) && isset($g_mgtv_amqp_name_config[$sp_id]['exchange_name']) && isset($g_mgtv_amqp_name_config[$sp_id]['route_name']))
		{
			self::$channel_name = empty($g_mgtv_amqp_name_config[$sp_id]['channel_name']) ? '' : $g_mgtv_amqp_name_config[$sp_id]['channel_name'];
			self::$exchange_name = empty($g_mgtv_amqp_name_config[$sp_id]['exchange_name']) ? '' : $g_mgtv_amqp_name_config[$sp_id]['exchange_name'];
			self::$route_name = empty($g_mgtv_amqp_name_config[$sp_id]['route_name']) ? '' : $g_mgtv_amqp_name_config[$sp_id]['route_name'];
		}
		else
		{
			unset($g_mgtv_amqp_name_config);
			return array(
					'ret'=>2,
					'reason'=>'params error no set g_mgtv_amqp_name_config in config file'
			);
		}
		unset($g_mgtv_amqp_name_config);
		global $g_mgtv_amqp_host_config;
		if(empty($g_mgtv_amqp_host_config))
		{
			return array(
					'ret'=>2,
					'reason'=>'params error no set g_mgtv_amqp_host_config in config file'
			);
		}
		self::$amqp_config = $g_mgtv_amqp_host_config;
		unset($g_mgtv_amqp_host_config);
		switch ($anction)
		{
			case "all_file":
				return self::add_amqp($message);
			case "local_file":
				
				break;
			default:
				return  array(
					'ret'=>4,
					'reason'=>'没有发送此消息到amqp的方法'
				);
		}
	}
}