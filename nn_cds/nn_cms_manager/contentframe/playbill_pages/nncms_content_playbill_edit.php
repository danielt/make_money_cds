<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
    $language_dir = $_SESSION["language_dir"];
}
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/language_contant.php";
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";


//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();

$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107103");
if ($_SESSION["nns_manager_type"] != 0)
    $pri_bool = false;
$checkpri = null;
if (!$pri_bool)
    Header("Location: ../nncms_content_wrong.php");
$nns_id = $_GET["nns_id"];

include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");
include $nncms_db_path . "nns_live/nns_db_live_class.php";
include_once $nncms_db_path . "nns_live/nns_db_live_ex_class.php";
include_once LOGIC_DIR . 'playbill' . DIRECTORY_SEPARATOR . 'playbill_item_info_language.class.php';
include_once LOGIC_DIR . 'playbill' . DIRECTORY_SEPARATOR . 'playbill_item.class.php';
include_once LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'live_media.class.php';
include_once LOGIC_DIR . 'npss' . DIRECTORY_SEPARATOR . 'npss.class.php';


$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
$dc->open();


if (isset($_POST['begindate'])) {

    $$params = array();
    $params['begin_time'] = $_POST['begindate'];
    $params['time_len'] = $_POST['timelen'];
    $result = nl_playbill_item::set_playbill_item_by_info($dc, $_POST['nns_id'], $params);
    exit($result);
}
$data = array();
$data_info = nl_playbill_item::get_playbill_item_info($dc, $nns_id, NULL, NL_DC_DB);

$strarr = array(' ', ':', '-');
$startime = str_replace($strarr, '', $data_info[0]['nns_begin_time']);
$endtime = is_numeric($data_info[0]['nns_time_len']) ? $data_info[0]['nns_time_len'] : 0;
$live_inst = new nns_db_live_class();

$live_id = $data_info[0]['nns_live_id'];
$live_info_arr = $live_inst->nns_db_live_info($live_id);

$live_media = new nl_live_media();

$play_bill_value = $live_media->get_live_media($dc, $live_id);
$params = $play_bill_value[0];

$active_npss_url = nl_npss::get_active_npss_address();
if ($params["nns_video_type"] == 0) {
    $func_str = "vod";
} else {
    $func_str = "live";
}
$kbps_str = "&ck=" . $params["nns_kbps"];

$media_extend_url = ''; //"&uid=" . $params["nns_user_id"] . "&cn=" . $video_name . "&did=" . $params["nns_device_id"];
$media_http_url = "http://" . $active_npss_url . "/nn_$func_str." . $params['nns_filetype'] . "?id=" . $params["nns_content_id"] . $kbps_str . "&nst=iptv" . $media_extend_url;
//$media_http_url = 'http://113.247.251.11:5000/nn_live.ts?id=HNWS'; //测试地址
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
        <link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
        <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
        <script language="javascript" src="../../js/table.js.php"></script>
        <script language="javascript" src="../../js/radiolist.js"></script>
        <script language="javascript" src="../../js/checkinput.js.php"></script>
        <script language="javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
        <script language="javascript" src="../../js/cms_datepicker.js"></script>

    </head>

    <body>
        <div style="padding:0px; margin:0px;" class="content selectcontrol">
            <div class="closebtn"><img style="cursor: pointer; float: right;" src="../../images/topicon_08-topicon.png"></div>

            <div style="background-color:#161616;">
                <font color="red"> * 审片工具只能工作于IE7/8/9版本，以ActiveX控件的方式提供</font>

                <script language="JavaScript" for="TSTVPlayer" event="on_nn_live_playbill_time_reset(begin_date,begin_time,time_s_len)">
                  
                    $.post("nncms_content_playbill_edit.php", {nns_id: '<?php echo $data_info[0]['nns_id']; ?>', begindate: begin_date + begin_time, timelen: time_s_len}, function(data)
                    {
                        if (data == '1') {
                            alert('修改成功');
                        } else {
                            alert('修改失败');
                        }
                        return false;
                    });
                </script>
                <OBJECT ID="TSTVPlayer" CLASSID="CLSID:3FB7DA97-E6DF-45E9-9E39-72E0ADFDC8AD" width="800" height="500" CODEBASE="StarCor_PC_Player.exe"></OBJECT>
                <script language="JavaScript">
                
                    TSTVPlayer.set_mod("live_playbill_check");
                    TSTVPlayer.set_url("<?php echo $media_http_url; ?>");
                    TSTVPlayer.set_live_playbill_check("8", "<?php echo $startime; ?> ", "<?php echo $endtime; ?>", "1");
                    TSTVPlayer.play();
                </script>
                <div style="clear:both;"></div>
            </div>
        </div>
        <script>


            var ifheight = $(window.parent).height()-25;
            if(ifheight > 600){
             var ifheight= 600
            }
            $('#TSTVPlayer').attr('height', ifheight);
            $('.closebtn img').click(function() {

                window.parent.$('.selectbox').hide();
                var iframe=$(window.parent.document).find(".content_if")[0];
               	$(iframe.contentWindow.document).find(".playbill_frame iframe")[0].contentWindow.location.reload();
            });
        </script>
    </body>
</html>
