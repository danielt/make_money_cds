<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107103");
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");

 if ($_SESSION["nns_manager_type"]==0){
	$nns_org_id="";
 }else{
	$nns_org_id=$_SESSION["nns_org_id"];
 }

 $action=$_GET["action"];
 switch($action){
	case "select":
		$action_str=$language_media_playbill_jmdlb;
		$base_url="nncms_content_playbill_detail_list.php";
	break;
	case "export":
		$action_str=$language_media_playbill_dcjmd;
		$base_url="nncms_content_playbill_export.php";
	break;
	case "import":
		$action_str=$language_media_playbill_drjmd;
		$base_url="nncms_content_playbill_import.php";
	break;
 }

 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
   if (!empty($_COOKIE["page_max_num"])){
		$g_manager_list_max_num=$_COOKIE["page_max_num"];
	}
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
 include($nncms_db_path. "nns_live/nns_db_live_class.php");
  include $nncms_db_path. "nns_carrier/nns_db_carrier_class.php";
 include $nncms_db_path. "nns_partner/nns_db_partner_class.php";
 include $nncms_db_path. "nns_group/nns_db_group_class.php";
  include $nncms_db_path. "nns_common/nns_db_constant.php";
$live_inst=new nns_db_live_class();
 $countArr=$live_inst->nns_db_live_count($nns_live_search,"",$view_audit,"","","",$nns_org_id);
 if ($countArr["ret"]==0)
 $live_total_num=$countArr["data"][0]["num"];
 $live_pages=ceil($live_total_num/$g_manager_list_max_num);
 $currentpage=1;
 if (!empty($_GET["page"])) $currentpage=$_GET["page"];
	$live_array=$live_inst->nns_db_live_list("nns_id,nns_check,nns_director,nns_actor,nns_show_time,nns_view_len,nns_all_index,nns_new_index,nns_area,nns_name,nns_org_type,nns_org_id,nns_create_time,nns_modify_time,nns_state",$nns_live_search,"",$view_audit,$view_type,"","",$nns_org_id,"","","",($currentpage-1)*$g_manager_list_max_num,$g_manager_list_max_num,$view_delete);
 $live_inst=null;
// var_dump($manager_array);
 if ($live_array["ret"]!=0){
	$data=null;
	echo "<script>alert(". $live_array["reason"].");</script>";
 }
 $data=$live_array["data"];

  $carrier_inst=new nns_db_carrier_class();
 $partner_inst=new nns_db_partner_class();
 $group_inst=new nns_db_group_class();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript">
function checkhiddenInput(){
	var par=getAllCheckBoxSelect();
	if (par==""){
		alert("<?php echo $language_manager_group_msg_noselect;?>");
	}else{
		$('#nns_id').attr('value',getAllCheckBoxSelect());
		checkForm('<?php echo $language_manager_xtgl_jtgl;?>','<?php echo $language_msg_ask_delete;?>',$('#delete_form'));
	}

}
</script>
</head>

<body>
<div class="content">
	<div class="content_position"><?php echo $language_manager_jmdgl;?> > <?php echo $action_str;?></div>

	<div class="content_table formtable">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<thead>
			<tr>

				<th><?php echo $language_manager_table_segnumber;?></th>
				<th><?php echo $language_manager_table_name;?></th>
				<th><?php echo $language_manager_table_group;?></th>
				<!--<th><?php echo $language_media_daoyan;?></th>
				<th><?php echo $language_media_yy;?></th>-->
				<th><?php echo $language_media_sysj;?></th>
				<th><?php echo $language_media_dq;?></th>
				<th><?php echo $language_media_zsc;?></th>
				<!--<th>分镜数</th>-->
				<!--<th><?php echo $language_media_dqgx;?></th>-->

				<th><?php echo $language_action_audit;?></th>
				<th><?php echo $language_manager_table_create_time;?></th>
				<th><?php echo $language_manager_table_edit_time;?></th>

			</tr>
		</thead>
		<tbody>
		<?php
			if ($data!=null){
			 $num=($currentpage-1)*$g_manager_list_max_num; foreach($data as $item){
			$num++;
			?>
			  <tr>

				<td><?php echo $num;?></td>
				<td><a href="<?php echo $base_url;?>?nns_id=<?php echo $item["nns_id"];?>" target="_self"><?php echo $item["nns_name"];?></a></td>
				<td><?php switch($item["nns_org_type"]){
						case 0:
							$info_result=$carrier_inst->nns_db_carrier_info($item["nns_org_id"]);
						break;
						case 1:
							$info_result=$partner_inst->nns_db_partner_info($item["nns_org_id"]);
						break;
						case 2:
							$info_result=$group_inst->nns_db_group_info($item["nns_org_id"]);
						break;
					}
					if ($info_result["ret"]==0){
						echo $info_result["data"][0]["nns_name"];
					}
				?></td>
			   <!-- <td><?php echo $item["nns_director"];?> </td>
				<td><?php echo $item["nns_actor"];?> </td>-->
				<td><?php echo $item["nns_show_time"];?> </td>
				<td><?php echo $item["nns_area"];?> </td>
				<td><?php echo ceil($item["nns_view_len"]/60);?> </td>
				<!--<td><?php echo $item["nns_all_index"];?> </td>-->
				<!--<td><?php echo $item["nns_new_index"];?> </td>-->

				<td><?

				switch ($item["nns_check"]){
					case NNS_VOD_CHECK_OK:
					echo "<font style=\"color:#00CC00\">". $language_media_addedit_ysh. "</font>";
					break;
					case NNS_VOD_CHECK_WAITING:
					echo "<font style=\"color:#999999\">". $language_media_addedit_ddsh. "</font>";
					break;
//                	case NNS_VOD_STATE_OFFLINE:
//                	echo "<font style=\"color:#ff0000\">". $language_media_addedit_yxx. "</font>";
//                	break;
				}?> </td>
				<td><?php echo $item["nns_create_time"];?> </td>
				<td><?php echo $item["nns_modify_time"];?> </td>
			  </tr>
		  <?php }} $least_num=$g_manager_list_max_num-count($data);
				for ($i=0;$i<$least_num;$i++){?>
				<tr>

				<td>&nbsp;</td>
				<td>&nbsp;</td>

				<!--<td>&nbsp; </td>-->
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				 <td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				</tr>

			<?php	}?>
		  </tbody>
		</table>
	</div>
	 <div class="pagecontrol">
		<?php if ($currentpage>1){?>
		<a href="nncms_content_livelist.php?page=1&view_delete=<?php echo $view_delete;?>&nns_live_search=<?php echo $nns_live_search;?>&view_audit=<?php echo $view_audit;?>&view_delete_type=<?php echo $view_delete_type;?>" target="_self"><?php echo $language_manager_table_first_page;?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="nncms_content_livelist.php?page=<?php echo $currentpage-1;?>&view_delete=<?php echo $view_delete;?>&nns_live_search=<?php echo $nns_live_search;?>&view_audit=<?php echo $view_audit;?>&view_delete_type=<?php echo $view_delete_type;?>" target="_self"><?php echo $language_manager_table_pre_page;?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php }else{?>
			<span><?php echo $language_manager_table_first_page;?></span>&nbsp;&nbsp;&nbsp;&nbsp;
			<span><?php echo $language_manager_table_pre_page;?></span>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php } if ($currentpage<$live_pages){?>
		<a href="nncms_content_livelist.php?page=<?php echo $currentpage+1;?>&view_delete=<?php echo $view_delete;?>&nns_live_search=<?php echo $nns_live_search;?>&view_audit=<?php echo $view_audit;?>&view_delete_type=<?php echo $view_delete_type;?>" target="_self"><?php echo $language_manager_table_next_page;?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="nncms_content_livelist.php?page=<?php echo $live_pages;?>&view_delete=<?php echo $view_delete;?>&nns_live_search=<?php echo $nns_live_search;?>&view_audit=<?php echo $view_audit;?>&view_delete_type=<?php echo $view_delete_type;?>" target="_self"><?php echo $language_manager_table_last_page;?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php }else{?>
			<span><?php echo $language_manager_table_next_page;?></span>&nbsp;&nbsp;&nbsp;&nbsp;
			<span><?php echo $language_manager_table_last_page;?></span>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php }?>

		<?php echo $language_manager_table_jump_to;?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage;?>" type="text" style="width:20px;"/> <?php echo $language_manager_table_page;?> &nbsp;&nbsp;
		<a href="javascript:go_page_num('nncms_content_livelist.php?view_delete=<?php echo $view_delete;?>&nns_live_search=<?php echo $nns_live_search;?>&view_audit=<?php echo $view_audit;?>&view_delete_type=<?php echo $view_delete_type;?>',<?php echo $live_pages;?>);">GO>></a>&nbsp;&nbsp;
		<?php echo $language_manager_table_current;?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage."/".$live_pages;?></span><?php echo $language_manager_table_page;?>&nbsp;&nbsp;|&nbsp;&nbsp;
		<?php echo $language_action_perpagenum;?>&nbsp;
		 <input name="nns_list_max_num" id="nns_list_max_num" type="text"
		 value="<?php echo $g_manager_list_max_num;?>" style="width:24px;"/>&nbsp;&nbsp;
		 <input type="button" value="<?php echo $language_action_confirm;?>"
		 onclick="refresh_prepage_list();"/>

	</div>


</div>
</body>
</html>
<?php
$carrier_inst=null;
 $partner_inst=null;
 $group_inst=null;
 ?>
