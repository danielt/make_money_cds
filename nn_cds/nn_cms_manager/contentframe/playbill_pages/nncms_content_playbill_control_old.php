<?php
/*
 * Created on 2012-3-2
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 @header("Content-Type:text/html;charset=utf-8");
 include("../../nncms_manager_inc.php");
 //加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";



 $action=$_POST["action"];
 $nns_id=$_POST["nns_id"];

 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;



$pri_bool1=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107101");
$pri_bool2=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107100");

if ($pri_bool1 || $pri_bool2 ) $pri_bool=true;

$checkpri=null;
if (!$pri_bool){
	 Header("Location: ../nncms_content_wrong.php");
}else{
	require_once $nncms_db_path. "nns_log".DIRECTORY_SEPARATOR . "nns_db_op_log_class.php";
	$log_inst=new nns_db_op_log_class();

	include $nncms_db_path. "nns_live/nns_db_live_playbill_class.php";
		$live_playbill_inst=new nns_db_live_playbill_class();
	include $nncms_db_path. "nns_live/nns_db_live_class.php";
		$live_inst=new nns_db_live_class();
		if ($nns_id){
			$live_name_arr=$live_inst->nns_db_live_info($nns_id,"nns_name");
			if ($live_name_arr["ret"]==0){
				$live_name=$live_name_arr["data"][0]["nns_name"];
			}
		}
	if ($action=="import"){
		$action_str=cms_get_lang('media_playbill_drjmd');
		$playbill_mode=$_POST["playbill_import_mode"];

		if ($playbill_mode==0){
			$playbill_day=$_POST["playbill_import_date"];
			$import_date=date("Y-m-d",strtotime($playbill_day));
		}
		$play_bill_file=$_FILES["playbill_import_file"];
		$play_bill_str=file_get_contents($play_bill_file["tmp_name"]);
		$play_bill_xml = new DOMDocument("1.0","UTF-8");
		try{
			$play_bill_xml->loadXML($play_bill_str);

		$playbill_program = $play_bill_xml->getElementsByTagName("program");
		$pro_id=$playbill_program->item(0)->getAttribute('pid');
		foreach($play_bill_xml->getElementsByTagName("day") as $day_item){
			$playbill_date=date("Y-m-d",strtotime($day_item->getAttribute('day')));
			$playbill_str= $day_item->ownerDocument->saveXML( $day_item );
//			echo "日期". $playbill_date;
			if ($playbill_mode==0 && $playbill_date==$import_date){
				$result=$live_playbill_inst->nns_db_live_playbill_import($nns_id,date("Ymd",strtotime($playbill_date)),$playbill_str);
				break;
			}
			else if ($playbill_mode==1){
				$result=$live_playbill_inst->nns_db_live_playbill_import($nns_id,date("Ymd",strtotime($playbill_date)),$playbill_str);
			}

		};
		}catch(Exception $e){
			var_dump($e);
		}

	}else if ($action=="delete"){
		$action_str=cms_get_lang('media_playbill_scjmd');
		$play_ids=$_POST["play_id"];
		$play_ids_arr=explode(",",$play_ids);
			$num=0;
			foreach($play_ids_arr as $playid){
				if (!empty($playid)){
					$num++;
					$result=$live_playbill_inst->nns_db_live_playbill_delete($playid);
					if ($result["ret"]!=0 && $num==1){
						break;
					}
				}
			}

	}else if($action=="export"){
		$action_str=cms_get_lang('media_playbill_dcjmd');
		$playbill_begin_date=date("Ymd",strtotime($_POST["nns_begin_date"]));
		$playbill_end_date=date("Ymd",strtotime($_POST["nns_end_date"]));
		$count=$live_playbill_inst->nns_db_live_playbill_count($nns_id,"",$playbill_begin_date,$playbill_end_date);
		if ($count["data"][0]["num"]>0){
		$result["ret"]=0;
		$playbill_list=$live_playbill_inst->nns_db_live_playbill_list(0,0,$nns_id,"",$playbill_begin_date,$playbill_end_date);
		$xmlstr='<?xml version="1.0" encoding="utf-8" ?>';
		$xmlstr=$xmlstr. '<playbill><program pid="'.$nns_id.'">';
			foreach ($playbill_list["data"] as $item){
				$xmlstr=$xmlstr. $item["nns_playbill"];
			}
		$xmlstr=$xmlstr. '</program></playbill>';
		if ($playbill_begin_date<$playbill_end_date){
			$filename_str=$playbill_begin_date."_".$playbill_end_date;
		}else{
			$filename_str=$playbill_begin_date;
		}
		ob_start();
		header ( "Content-Type: application/force-download" );

			$live_name=urlencode($live_name);

		header ( "Content-Disposition: attachment; filename*=UTF-8''". $live_name  ."_".$filename_str. ".xml" );
		 echo $xmlstr;
		 ob_end_flush();
		}else{
			$result["ret"]=1;
		}
	}



	if ($action!="export"){
			if ($result["ret"]!=0 || $result==null){

				echo "<script>alert('". $action_str, cms_get_lang('fault'), "');</script>";
			}else{
				$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
				echo "<script>alert('". $action_str, cms_get_lang('success'), "');</script>";
			}

		$log_inst=null;
		//var_dump($result);

		echo "<script>self.location='nncms_content_playbill_detail_list.php?nns_id=".$nns_id."';</script>";
	}
}
?>
