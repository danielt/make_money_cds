<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";


 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();

$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107103");
if ($_SESSION["nns_manager_type"]!=0) $pri_bool=false;
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");


 $nns_id=$_GET["nns_id"];
 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/radiolist.js"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/cms_datepicker.js"></script>
<script language="javascript">
	$(document).ready(function(){
		$("input[type=radio]").click(function(){
			if ($("input[type=radio]:eq(0)").attr("checked")){
				$(".select_date_tr").show();
			}else{
				$(".select_date_tr").hide();
			}
		})

		
	});
</script>
</head>

<body>


<div class="content" style="min-height:300px; background:#fff;">
	<div class="content_position"><?php echo cms_get_lang('jmdgl');?> > <?php echo cms_get_lang('media_playbill_dcjmd');?></div>
	<form id="search_form" action="nncms_content_playbill_control.php" method="post">
		<input name="action" id="action" type="hidden" value="export" />
		<input name="nns_id" id="nns_id" type="hidden" value="<?php echo $nns_id;?>" />
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>

			<tr class="select_date_tr">
				<td width="120"><?php echo cms_get_lang('media_playbill_select_date');?></td>
				<td><input name="nns_begin_date" id="nns_begin_date" type="text"
					 value="" readonly="readonly" style="width:120px;" class="datepicker"/> - <input name="nns_end_date" id="nns_end_date" type="text"
					 value="" readonly="readonly"  style="width:120px;"  class="datepicker"/></td>

			</tr>
			  <tr>
				<td width="120"></td>
				<td></td>
			</tr>


		  </tbody>
		</table>
	</div>
	</form>
	<div class="controlbtns">
		<div class="controlbtn edit"><a href="javascript:$('#search_form')[0].submit();"><?php echo cms_get_lang('media_playbill_export');?></a></div>
		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>
</body>
</html>
