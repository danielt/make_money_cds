<?php

/*
 * Created on 2012-3-2
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
@header("Content-Type:text/html;charset=utf-8");
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";

include_once LOGIC_DIR . 'playbill' . DIRECTORY_SEPARATOR . 'playbill_item.class.php';
include_once LOGIC_DIR . 'live' . DIRECTORY_SEPARATOR . 'live.class.php';
include_once LOGIC_DIR . 'log' . DIRECTORY_SEPARATOR . 'manager_log.class.php';
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
$dc->open();

//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language_media.php";



$action = $_POST["action"];
$nns_id = $_POST["nns_id"];

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;



$pri_bool1 = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107101");
$pri_bool2 = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107100");

if ($pri_bool1 || $pri_bool2)
        $pri_bool = true;

$checkpri = null;
if (!$pri_bool) {

        Header("Location: ../nncms_content_wrong.php");
} else {


        include $nncms_db_path . "nns_live/nns_db_live_class.php";
        $live_inst = new nns_db_live_class();
        if ($nns_id) {
                $live_name_arr = $live_inst->nns_db_live_info($nns_id, "nns_name");
                if ($live_name_arr["ret"] == 0) {
                        $live_name = $live_name_arr["data"][0]["nns_name"];
                }
        }
        if ($action == "import") {
                $action_str = cms_get_lang('media_playbill_drjmd');
                $playbill_mode = $_POST["playbill_import_mode"];

                if ($playbill_mode == 0) {
                        $playbill_day = $_POST["playbill_import_date"];
                        $import_date = date("Y-m-d", strtotime($playbill_day));
                }
                $play_bill_file = $_FILES["playbill_import_file"];
                $play_bill_str = file_get_contents($play_bill_file["tmp_name"]);

                $play_bill_xml = new DOMDocument("1.0", "UTF-8");
                try {
                        $play_bill_xml->loadXML($play_bill_str);

                        $playbill_program = $play_bill_xml->getElementsByTagName("program");
                        $pro_id = $playbill_program->item(0)->getAttribute('pid');
                        foreach ($play_bill_xml->getElementsByTagName("day") as $day_item) {
                                if ($playbill_mode == 1) {
                                        $import_date = date("Ymd", strtotime($day_item->getAttribute('day')));
                                }
                                foreach ($day_item->getElementsByTagName('item') as $playbill_item) {
                                        $params = array();
                                        $params['name'] = $playbill_item->getAttribute('text');
                                        $playbill_begin_time = $playbill_item->getAttribute('begin');
                                        $params['time_len'] = $playbill_item->getAttribute('time_len');

                                        $playbill_begin_time = $import_date . 'T' . $playbill_begin_time;
                                        $params['begin_time'] = date('Y-m-d H:i:s', strtotime($playbill_begin_time));



                                        nl_playbill_item::add_playbill_item($dc, $nns_id, $params);
                                }
                        };
                } catch (Exception $e) {
                        var_dump($e);
                }
        } else if ($action == "delete") {
                $action_str = cms_get_lang('media_playbill_scjmd');
                $play_ids = $_POST["play_id"];
                $play_ids_arr = explode(",", $play_ids);
                $num = 0;
                foreach ($play_ids_arr as $playid) {
                        if (!empty($playid)) {
                                $num++;
                                $result = nl_playbill_item::delete_playbill_item($dc, $nns_id);
                                ;
                                if ($result === FALSE && $num == 1) {
                                        break;
                                }
                        }
                }
        } else if ($action == "export") {

                $action_str = cms_get_lang('media_playbill_dcjmd');
                $playbill_begin_date = date("Y-m-d H:i:s", strtotime($_POST["nns_begin_date"]));
                $playbill_end_date = date("Y-m-d H:i:s", strtotime($_POST["nns_end_date"]) + 3600 * 24);
                $count = nl_playbill_item::count_playbill_item_list($dc, $nns_id, $playbill_begin_date, $playbill_end_date, 0);

                if ($count <= 0) {
                        echo "<script>alert('没有数据可导出!');self.location='nncms_content_playbill_detail_list.php?nns_id=" . $nns_id . "';</script>";
                        exit;
                }
                include $nncms_db_path . "nns_live/nns_db_live_ex_class.php";
                $live_ex_inst = new nns_db_live_ex_class();
                $live_info = nl_live::get_live_list($dc, true, array('nns_id' => $nns_id));
                $live_info = $live_info[0];
                $name = $live_info['nns_name'];
                $pid = $live_ex_inst->nns_db_live_ex_info($nns_id, 'pid');
                $filename = empty($pid) ? 'ChannelIdEmpty' : $pid;
                $filename = $filename . '__Count_' . $count . '_Date_' . $_POST["nns_begin_date"] . '/' . $_POST["nns_end_date"];
                $playbill_list = nl_playbill_item::get_playbill_item_list($dc, $nns_id, $playbill_begin_date, $playbill_end_date, 0, 0, 100000);

                $xml_arr = array();
                $date = '';
                $r = "\n";
                $txt = 'Channel:' . $name . "\r" . $r;
                foreach ($playbill_list as $k => $item) {
                        $listdate = date('Ymd', strtotime($item['nns_begin_time']));
                        if ($listdate != $date) {
                                $txt .="Date:" . $listdate . "\r" . $r;
                                $date = $listdate;
                        }
                        if(empty($item['nns_name'])){
                                $item['nns_name']=" \r";
                        }
                        $txt.= date('Hi', strtotime($item['nns_begin_time'])) . '-' . date('Hi', strtotime($item['nns_begin_time']) + $item['nns_time_len']) . '|' . $item['nns_name'] . $r;
                }
                //  $txt=iconv("GBK",'UTF-8',$txt);
                //  var_dump($txt);exit;
                ob_start();
                header("Content-Type: application/force-download");
                header("Content-Disposition: attachment; filename*=UTF-8''" . $filename . ".txt");
                $txt = iconv('UTF-8', 'GBK', $txt);
                echo($txt);
                ob_end_flush();
        }



        if ($action != "export") {
                if ($result === FALSE) {

                        echo "<script>alert('" . $action_str, cms_get_lang('fault'), "');</script>";
                } else {
                        nl_manager_log::add($dc, $_SESSION["nns_mgr_name"] . $action_str, $action);
//				$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
                        echo "<script>alert('" . $action_str, cms_get_lang('success'), "');</script>";
                }

                //var_dump($result);

                echo "<script>self.location='nncms_content_playbill_detail_list.php?nns_id=" . $nns_id . "';</script>";
        }
}
?>
