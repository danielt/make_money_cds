<?php
/*
 * Created on 2012-3-2
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 @header("Content-Type:text/html;charset=utf-8");
 include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";



include_once LOGIC_DIR.'playbill'.DIRECTORY_SEPARATOR.'playbill_item_info_language.class.php';
include_once LOGIC_DIR.'playbill'.DIRECTORY_SEPARATOR.'playbill_item.class.php';
include_once LOGIC_DIR.'live'.DIRECTORY_SEPARATOR.'live_media.class.php';
include_once LOGIC_DIR.'log'.DIRECTORY_SEPARATOR.'manager_log.class.php';
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
			));
$dc->open();

//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/language_contant.php";
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_pinyin_class.php";

include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/models/queue_task_model.php';

 $action=$_REQUEST["action"];
 $nns_id=$_REQUEST["nns_id"];
 $live_id=$_POST["live_id"];
 $sp_id=$_REQUEST["sp_id"];

 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;

if (!empty($action)){
	switch($action){
		case "edit":
		$action_str=cms_get_lang('edit|media_playbill_jmdlb');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"106101");
		break;
		case "add":
		$action_str=cms_get_lang('add|media_playbill_jmdlb');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"106100");
		break;
		case "delete":
		$action_str=cms_get_lang('delete|media_playbill_jmdlb');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"106102");
		break;
		case "audit":
		$action_str=cms_get_lang('media_playbill_jmdlb').cms_get_lang('change_state');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"106103");
		break;
		case "import_cdn":
			$action_str=cms_get_lang('add|media_playbill_jmdlb');
			$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"106100");
			break;
		default:
		break;
	}
}

$checkpri=null;
if (!$pri_bool){
	 Header("Location: ../nncms_content_wrong.php");
}else{
	include_once $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php";
	$playbill_name=$_POST["nns_name"];
	$playbill_summary=$_POST["nns_summary"];
	$playbill_name_lan=$_POST["nns_name_lan"];
	$playbill_summary_lan=$_POST["nns_summary_lan"];
	$playbill_datetime=$_POST["begin_date"].'T'.$_POST["show_begin_hour"].':'.$_POST["show_begin_minute"].':'.$_POST["show_begin_sencond"];
	$playbill_timelen=$_POST["time_len"];
	$playbill_pinyin=$_POST["pinyin"];
	$playbill_engname=$_POST["eng_name"];
	$playbill_kind=$_POST["kind"];
	$playbill_keyword=$_POST["keyword"];

	$langs=g_cms_config::get_g_extend_language();
	$langs=explode('|',$langs);

	switch ($action){
		case 'add':
			

			$add_id=np_guid_rand();
			$params=array();
//			“name”=>节目名称，
//“begin_time”=>节目播出时间，
//“time_len”=>节目时长，
//“summary”=>节目简介，
//“image”=>节目剧照
			$params['id']=$add_id;
			$params['begin_time']=date('Y-m-d H:i:s',strtotime($playbill_datetime));
			$params['time_len']=$playbill_timelen;
			$params['summary']=$playbill_summary;
			$params['name']=$playbill_name;
			$params['eng_name']=$playbill_engname;
			$params['keyword']=$playbill_keyword;
			$params['kind']=$playbill_kind;
			$params['pinyin']=nns_controls_pinyin_class::get_pinyin_letter($playbill_name);
			
			$result_live_media = nl_live_media::query_by_channel_id($dc, $live_id);
			if($result_live_media['ret'] !=0)
			{
				echo "<script>alert('". $action_str, $result_live_media['reason'], "');history.go(-1);</script>";die;
			}
			if(!isset($result_live_media['data_info']) || empty($result_live_media['data_info']) || !is_array($result_live_media['data_info']) || count($result_live_media['data_info']) !=1)
			{
				echo "<script>alert('". $action_str, '片源查询有多个,或者没有片源', "');history.go(-1);</script>";die;
			}
			$params['media_id']=$result_live_media['data_info'][0]['nns_id'];
			$params['cp_id']=$result_live_media['data_info'][0]['nns_cp_id'];
//			var_dump($params);die;
//			$result=TRUE;
			
	        $result_info_cp=nl_cp::query_by_id($dc, $params['cp_id']);
		    $arr_cp_config['base_info'] = $arr_cp_config['base_config'] = null;
		    if(isset($result_info_cp['data_info']['nns_config']) && strlen($result_info_cp['data_info']['nns_config']) >0)
		    {
		        $result_info_config = json_decode($result_info_cp['data_info']['nns_config'],true);
		        $arr_cp_config['base_config'] = (is_array($result_info_config) && !empty($result_info_config)) ? $result_info_config : null;
		    }
		    else
		    {
		        $arr_cp_config['base_config'] = null;
		    }
		    if(isset($result_info_cp['data_info']['nns_config']))
		    {
		        unset($result_info_cp['data_info']['nns_config']);
		    }
		    if(is_array($result_info_cp['data_info']) && !empty($result_info['data_info']))
		    {
		        foreach ($result_info_cp['data_info'] as $data_info_key=>$data_info_val)
		        {
		            $arr_cp_config['base_info'][$data_info_key]=$data_info_val;
		        }
		    }
			// 载入全局的芒果tv的ftp配置数据,是一个二维数组
			if (isset($arr_cp_config['base_config']['g_ftp_conf']['img_enabled']) && $arr_cp_config['base_config']['g_ftp_conf']['img_enabled'] == 1)
			{
			    $g_ftp_conf = $arr_cp_config['g_ftp_conf'];
			}
			else
			{
			    global $g_ftp_conf;
			}
			$imgurl1=pub_func_image_upload_by_playbill($_FILES['nns_image0'],$live_id,date('Ymd',strtotime($playbill_datetime)),$g_ftp_conf,$params['cp_id']);
			$imgurl2=pub_func_image_upload_by_playbill($_FILES['nns_image1'],$live_id,date('Ymd',strtotime($playbill_datetime)),$g_ftp_conf,$params['cp_id']);
			$imgurl3=pub_func_image_upload_by_playbill($_FILES['nns_image2'],$live_id,date('Ymd',strtotime($playbill_datetime)),$g_ftp_conf,$params['cp_id']);
			$images=array(
			    0=>$imgurl1,
			    1=>$imgurl2,
			    2=>$imgurl3
			);
			$params['image']=$images;
			
			
			$result=nl_playbill_item::add_playbill_item($dc,$live_id,$params,NL_DC_DB);
			if ($result===TRUE){
			    
			    $import_flag = false;
			    if(isset($arr_cp_config['base_config']['message_playbill_import_enable']) && $arr_cp_config['base_config']['message_playbill_import_enable']  == '1')
			    {
			        $queue_task_model = new queue_task_model($dc);
                    $queue_task_model->q_add_task_op_mgtv($params['id'], BK_OP_PLAYBILL, BK_OP_ADD);
                    unset($queue_task_model);
			    }
				$num=0;
				if (is_array($langs)){
					foreach ($langs as $lang){
						if (!empty($playbill_name_lan[$num])){
							$params=array();
							$params['name']=$playbill_name_lan[$num];
							$params['summary']=$playbill_summary_lan[$num];
							nl_playbill_item_info_language::modify_playbill_item_info_by_language($dc,$add_id,$lang,$params,NL_DC_DB);
						}
						$num++;
					}
				}
				$video_names=nl_manager_log::get_names_by_video_id($dc,array($live_id),'live');
				nl_manager_log::add($dc,$_SESSION["nns_mgr_name"].cms_get_lang('for_live').'<'.$video_names[$live_id]['nns_name'].'>'.$action_str.':'.$playbill_name,$action);
				
			}else{
				echo "<script>alert('". $action_str, cms_get_lang('fault'), "');</script>";
			}
		break;
		case 'edit':
		    $result_playbill=nl_playbill_item::get_playbill_item_info_by_ids($dc,array($nns_id));
		    $result_info_cp=nl_cp::query_by_id($dc, $result_playbill[$nns_id]['nns_cp_id']);
		    $arr_cp_config['base_info'] = $arr_cp_config['base_config'] = null;
		    if(isset($result_info_cp['data_info']['nns_config']) && strlen($result_info_cp['data_info']['nns_config']) >0)
		    {
		        $result_info_config = json_decode($result_info_cp['data_info']['nns_config'],true);
		        $arr_cp_config['base_config'] = (is_array($result_info_config) && !empty($result_info_config)) ? $result_info_config : null;
		    }
		    else
		    {
		        $arr_cp_config['base_config'] = null;
		    }
		    if(isset($result_info_cp['data_info']['nns_config']))
		    {
		        unset($result_info_cp['data_info']['nns_config']);
		    }
		    if(is_array($result_info_cp['data_info']) && !empty($result_info['data_info']))
		    {
		        foreach ($result_info_cp['data_info'] as $data_info_key=>$data_info_val)
		        {
		            $arr_cp_config['base_info'][$data_info_key]=$data_info_val;
		        }
		    }
		    // 载入全局的芒果tv的ftp配置数据,是一个二维数组
		    if (isset($arr_cp_config['base_config']['g_ftp_conf']['img_enabled']) && $arr_cp_config['base_config']['g_ftp_conf']['img_enabled'] == 1)
		    {
		        $g_ftp_conf = $arr_cp_config['g_ftp_conf'];
		    }
		    else
		    {
		        global $g_ftp_conf;
		    }
			$imgurl1=pub_func_image_upload_by_playbill($_FILES['nns_image0'],$live_id,date('Ymd',strtotime($playbill_datetime)),$g_ftp_conf,$result_playbill[$nns_id]['nns_cp_id']);
			$imgurl2=pub_func_image_upload_by_playbill($_FILES['nns_image1'],$live_id,date('Ymd',strtotime($playbill_datetime)),$g_ftp_conf,$result_playbill[$nns_id]['nns_cp_id']);
			$imgurl3=pub_func_image_upload_by_playbill($_FILES['nns_image2'],$live_id,date('Ymd',strtotime($playbill_datetime)),$g_ftp_conf,$result_playbill[$nns_id]['nns_cp_id']);
			$images=array(
				0=>$imgurl1,
				1=>$imgurl2,
				2=>$imgurl3
			);
			$params=array();
//			“name”=>节目名称，
//“begin_time”=>节目播出时间，
//“time_len”=>节目时长，
//“summary”=>节目简介，
//“image”=>节目剧照
			$params['begin_time']=date('Y-m-d H:i:s',strtotime($playbill_datetime));
			$params['time_len']=$playbill_timelen;
			$params['summary']=$playbill_summary;
			$params['image']=$images;
			$params['name']=$playbill_name;
			$params['eng_name']=$playbill_engname;
			if (empty($playbill_pinyin)) $playbill_pinyin = nns_controls_pinyin_class::get_pinyin_letter($playbill_name);
			$params['pinyin']=$playbill_pinyin;
			$params['keyword']=$playbill_keyword;
			$params['kind']=$playbill_kind;
                  	$result=TRUE;
			$result=nl_playbill_item::modify_playbill_item($dc,$nns_id,$params,NL_DC_DB);
			if ($result===TRUE){
			    
			    
			    $import_flag = false;
			    if(isset($arr_cp_config['base_config']['message_playbill_import_enable']) && $arr_cp_config['base_config']['message_playbill_import_enable']  == '1')
			    {
			        $queue_task_model = new queue_task_model($dc);
			        $queue_task_model->q_add_task_op_mgtv($nns_id, BK_OP_PLAYBILL, BK_OP_MODIFY);
			        unset($queue_task_model);
			    }
			    
				$img_update='';
				foreach ($images as $key=>$img_str){
					if (!empty($img_str)){
						$img_update.="nns_video_img{$key}='{$img_str}',";
					}
				}
				
				/***
				 * 修改媒资包对应节目数据
				 * BY S67 2013-5-3
				 */
				nl_execute_by_db('update nns_assists_item set ' .
						'nns_video_name=\''.$params['name'].'\',' .
						$img_update .
						'nns_pinyin=\''.$params['pinyin'].'\',' .
						'nns_english_index=\''.$params['eng_name'].'\' ' .
						'where nns_video_id=\''.$nns_id.'\' and nns_video_type=2',
						$dc->db());
					
						
				
				$num=0;
				if (is_array($langs)){
					foreach ($langs as $lang){
						if (!empty($playbill_name_lan[$num])){
						$params=array();
						$params['name']=$playbill_name_lan[$num];
						$params['summary']=$playbill_summary_lan[$num];
						nl_playbill_item_info_language::modify_playbill_item_info_by_language($dc,$nns_id,$lang,$params,NL_DC_DB);
						
							
						}
						$num++;
					}
				}
				$video_names=nl_manager_log::get_names_by_video_id($dc,array($live_id),'live');
				nl_manager_log::add($dc,$_SESSION["nns_mgr_name"].cms_get_lang('for_live').'<'.$video_names[$live_id]['nns_name'].'>'.$action_str.':'.$playbill_name,$action);
//				echo "<script>alert('". $action_str.  $language_action_success. "');</script>";
			}
		break;
		case 'delete':
			$nns_id=rtrim($nns_id,',');
			$nns_ids=explode(',',$nns_id);
			foreach ($nns_ids as $id){
				$result=nl_playbill_item::delete_playbill_item($dc,$id,NL_DC_DB);
				if ($result===TRUE){
					nl_playbill_item_info_language::delete_playbill_item_info_by_language($dc,$id,NL_DC_DB);
				}
			}
			$video_names=nl_manager_log::get_names_by_video_id($dc,array($live_id),'live');
			nl_manager_log::add($dc,$_SESSION["nns_mgr_name"].cms_get_lang('for_live').''.$video_names[$live_id]['nns_name'].'>'.$action_str.':'.$id,$action);
//			echo "<script>alert('". $action_str.  $language_action_success. "');</script>";
		break;
		case 'audit':
			$command=$_POST['command'];
			$nns_id=rtrim($nns_id,',');
			$nns_ids=explode(',',$nns_id);
			$playbill_flag = true;
			$playbiil_action = ($command == '0') ? BK_OP_MODIFY : BK_OP_DELETE;
			foreach ($nns_ids as $id){
				$result=nl_playbill_item::update_playbill_item_state($dc,$id,$command);
				if($playbill_flag)
				{
    				$result_playbill=nl_playbill_item::get_playbill_item_info_by_ids($dc,array($id));
    				$result_info_cp=nl_cp::query_by_id($dc, $result_playbill[$nns_id]['nns_cp_id']);
    				$arr_cp_config['base_info'] = $arr_cp_config['base_config'] = null;
    				if(isset($result_info_cp['data_info']['nns_config']) && strlen($result_info_cp['data_info']['nns_config']) >0)
    				{
    				    $result_info_config = json_decode($result_info_cp['data_info']['nns_config'],true);
    				    $arr_cp_config['base_config'] = (is_array($result_info_config) && !empty($result_info_config)) ? $result_info_config : null;
    				}
    				if(isset($result_info_cp['data_info']['nns_config']))
    				{
    				    unset($result_info_cp['data_info']['nns_config']);
    				}
    				if(is_array($result_info_cp['data_info']) && !empty($result_info['data_info']))
    				{
    				    foreach ($result_info_cp['data_info'] as $data_info_key=>$data_info_val)
    				    {
    				        $arr_cp_config['base_info'][$data_info_key]=$data_info_val;
    				    }
    				}
    				$import_flag = false;
				}
				if(!isset($arr_cp_config['base_config']['message_playbill_import_enable']) || $arr_cp_config['base_config']['message_playbill_import_enable']  != '1')
				{
				    continue;
				}
				$queue_task_model = new queue_task_model($dc);
				
				$queue_task_model->q_add_task_op_mgtv($nns_id, BK_OP_PLAYBILL, $playbiil_action);
				unset($queue_task_model);
			}
			$video_names=nl_manager_log::get_names_by_video_id($dc,array($live_id),'live');
			nl_manager_log::add($dc,$_SESSION["nns_mgr_name"].cms_get_lang('for_live').'<'.$video_names[$live_id]['nns_name'].'>'.$action_str.':'.$id,$action);
//			echo "<script>alert('". $action_str.  $language_action_success. "');</script>";
		break;
		case 'import_cdn':
			include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/live/playbill.class.php';
			include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
			$nns_id=rtrim($nns_id,',');
			$nns_ids=explode(',',$nns_id);
			foreach ($nns_ids as $id){
				$temp_playbill_info = array('nns_playbill_id'=>$id);
				$re = c2_task_model::add_playbill($temp_playbill_info,'REGIST',$sp_id);
				if($re['ret'] !=0)
				{
					echo "<script>alert('". $action_str,"节目单ID:[{$id}][{$re['reason']}]", "');history.go(-1);</script>";die;
				}
			}
			echo "<script>alert('". $action_str, cms_get_lang('success'), "');history.go(-1);</script>";die;
			break;
	}





			if ($result===FALSE){

				echo "<script>alert('". $action_str, cms_get_lang('fault'), "');</script>";
			}else{

				echo "<script>alert('". $action_str, cms_get_lang('success'), "');</script>";
			}

		//var_dump($result);

		if ($action=='delete' || $action=='audit'){
			echo "<script>history.go(-1);</script>";
		}else{
			echo "<script>history.go(-2);</script>";
		}

}
?>
