<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";

 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$nns_id=$_GET["playbill_id"];
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107103");
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");

$nns_sp_id = isset($_GET["sp_id"]) ? $_GET["sp_id"] : null;
 
 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");
 
	include $nncms_db_path. "nns_live/nns_db_live_playbill_class.php";
	$playbill_inst=new nns_db_live_playbill_class();
	if (!empty($nns_id)){
		$playbill_info=$playbill_inst->nns_db_live_playbill_info($nns_id);
		$playbill_inst=null;
	}else{ 
		echo "<script>alert('".$playbill_info["reason"] ."');self.location='nncms_content_grouplist.php';</script>";
	}
	if ($playbill_info["ret"]!=0){
		echo "<script>alert('".$playbill_info["reason"] ."');self.location='nncms_content_grouplist.php';</script>";
	}
	$edit_data=$playbill_info["data"][0];
	$playbill_xml = new DOMDocument("1.0","UTF-8");
	$playbill_xml->loadXML($edit_data["nns_playbill"]);
	$day_element=$playbill_xml->getElementsByTagName("day")->item(0);
	$item_elements=$day_element->getElementsByTagName("item");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/radiolist.js"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript">

</script>
</head>

<body>


<div class="content">
	<div class="content_position"><?php echo cms_get_lang('jmdgl'),' > ',date("Y-m-d",strtotime($day_element->getAttribute('day')));?></div>
   <div class="content_table formtable" style="padding:3px 10px;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							
							<th><?php echo cms_get_lang('segnumber');?></th>                                                              
							<th><?php echo cms_get_lang('media_playbill_jmmc');?></th>
							<th><?php echo cms_get_lang('media_playbill_kssj');?></th>
							<th><?php echo cms_get_lang('media_zsc');?></th>
							
			
						</tr>
					</thead>
					<tbody>
					<?php
						if ($item_elements!=null){
							
						  foreach($item_elements as $item){
						$num++;
						?>
							  <tr>                                                     
							   
								<td><?php echo $num;?></td>
								<td><?php echo $item->getAttribute("text");?></td>
								<td><?php echo date("H:i:s",strtotime($item->getAttribute("begin")))?> </td>
								<td><?php echo ceil($item->getAttribute("time_len")/60);?> </td>
							   
								
							  </tr>
					   
			
			<?php	}}?>  
						  </tbody>
						</table>
					</div>
					<div class="controlbtns">
		 
	   
		<div class="controlbtn export"><a href="javascript:$('#export_form')[0].submit();"  action="export"><?php echo cms_get_lang('media_playbill_export');?></a></div>
		<div class="controlbtn import"><a href="nncms_content_playbill_import.php?playbill_date=<?php echo $day_element->getAttribute('day');?>&nns_id=<?php echo $edit_data["nns_live_id"];?>" target="_self"><?php echo cms_get_lang('media_playbill_import');?></a></div>
		<div class="controlbtn back"><a href="javascript:returnToHistory();"  action="delete"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
	</div>
	<form id="export_form" action="nncms_content_playbill_control.php" method="post">
	  <input name="action" id="action" type="hidden" value="export" />
	  <input name="nns_id" id="nns_id" type="hidden" value="<?php echo $edit_data["nns_live_id"];?>" />
	  <input name="nns_begin_date" id="nns_begin_date" type="hidden" value="<?php echo $edit_data["nns_day"];?>" />
	  <input name="nns_end_date" id="nns_end_date" type="hidden" value="<?php echo $edit_data["nns_day"];?>" />
	 </form>
	
</div>





</body>
</html>
