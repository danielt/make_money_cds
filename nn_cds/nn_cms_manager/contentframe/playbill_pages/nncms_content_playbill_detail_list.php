<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/language_contant.php";
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107103");
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");

$play_id=$_GET["nns_id"];

$begin_date=$_GET["nns_begin_date"];
$end_date=$_GET["nns_end_date"];
 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
 include $nncms_db_path. "nns_live/nns_db_live_playbill_class.php";
 include $nncms_db_path. "nns_live/nns_db_live_class.php";
 include_once $nncms_db_path. "nns_live/nns_db_live_ex_class.php";
 include_once $nncms_db_path. "nns_common/nns_db_pager_class.php";
 include_once LOGIC_DIR.'playbill'.DIRECTORY_SEPARATOR.'playbill_item.class.php';
 $dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
			));
$dc->open();


 $live_inst=new nns_db_live_class();
 $live_info_arr=$live_inst->nns_db_live_info($play_id);
 if ($live_info_arr["ret"]==0){
	$live_info=$live_info_arr["data"][0];
 }

$live_ex_inst=new nns_db_live_ex_class();
$play_bill_value=$live_ex_inst->nns_db_live_ex_info($play_id,'play_bill_type');


$begin_date=date('Y-m-d',time());


  $extend_langs_str=g_cms_config::get_g_extend_language();
  $extend_langs=explode('|',$extend_langs_str);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="../../js/cms_datepicker.js"></script>
<script language="javascript">
function checkhiddenInput(){
	var par=getAllCheckBoxSelect();
	$("#action").attr("value","delete");
	if (par==""){

			alert("<?php echo cms_get_lang('media_select_jmd');?>");

	}else{
		$('#play_id').attr('value',getAllCheckBoxSelect());
		checkForm('<?php echo cms_get_lang('jmdgl');?>','<?php echo cms_get_lang('msg_ask_delete');?>',$('#delete_form'));
	}

}


function export_current_playbill(live_id,live_date){
	$("#export_form #nns_id").val(live_id);
	$("#export_form #nns_begin_date").val(live_date);
	$("#export_form #nns_end_date").val(live_date);
	$("#export_form")[0].submit();
}

$(document).ready(function(){
	click_tab(0);
	$(".state_tab").click(function(){
		click_tab($(".state_tab").index($(this)));
	});

	$("#language_sel").change(function(){
		refresh_search_list();
	});

	$(document).keydown(function(event){
		   if(event.which == 13){
			   refresh_search_list();
		 }
	});
});

function click_tab(num){
	$(".state_tab").removeClass("select");
	$(".state_tab:eq("+num+")").addClass("select");
	switch (num){
		case 0:
			$("#day_pick_str").show();
			$("#custom_pick_str").hide();
		break;
		case 1:
			$("#day_pick_str").hide();
			$("#custom_pick_str").show();
		break;
	}
	refresh_search_list();
}

function next_day(){
	var day_arr=$("#day_picker").val().split("-");
	var next_day=new Date(Number(day_arr[0]),Number(day_arr[1])-1,Number(day_arr[2])+1);
	$("#day_picker").val(next_day.getFullYear()+"-"+(next_day.getMonth()+1)+"-"+next_day.getDate());
	refresh_search_list();
}

function pre_day(){
	var day_arr=$("#day_picker").val().split("-");
	var next_day=new Date(Number(day_arr[0]),Number(day_arr[1])-1,Number(day_arr[2])-1);
	$("#day_picker").val(next_day.getFullYear()+"-"+(next_day.getMonth()+1)+"-"+next_day.getDate());
	refresh_search_list();
}

function crrent_day(){
	var crrent_day=new Date();
	$("#day_picker").val(crrent_day.getFullYear()+"-"+(crrent_day.getMonth()+1)+"-"+crrent_day.getDate());
	refresh_search_list();
}


function refresh_search_list(){
	var querys={};
	if ($("#day_pick_str").is(":hidden")){
		querys['begin_day']=$("#playbill_date_begin").val();
		querys['end_day']=$("#playbill_date_end").val();
		querys['name']=$("#playbill_name").val();
	}else if($("#custom_pick_str").is(":hidden")){
		querys['begin_day']=$("#day_picker").val()+' 00:00:00';
		querys['end_day']=$("#day_picker").val()+' 23:59:59';
	}

	querys['language']=$("#language_sel").val();
	refresh_frame(querys);
}

function refresh_frame(querys){
	<?php
	if(empty($_GET['sp_id'])){
	?>
	var query_str='?live_id=<?php echo $play_id;?>&';
	<?php
	}else{
	?>
	var query_str='?live_id=<?php echo $play_id;?>&sp_id=<?php echo $_GET['sp_id'];?>&category_id=<?php echo $_GET['category_id'];?>&';
	<?php
	}
	?>
	for (var key in querys){
		query_str+=key+'='+querys[key]+'&';
	}
	query_str=query_str.substr(0,query_str.length-1);
	$(".playbill_frame iframe").attr("src",'nncms_content_playbill_list.php'+query_str);
}


function checkheight(){
	var src_url=$(".playbill_frame iframe")[0].contentWindow.location;
	if (src_url.toString().indexOf("nncms_content_playbill_list.php")>=0){
		$(".content_table").show();
	}else{
		$(".content_table").hide();
	}
	var hei=$(".playbill_frame iframe").contents().find(".content").height();
	$(".playbill_frame iframe").height(hei);
	window.parent.checkheight();
}



</script>
</head>

<body>
<div class="content">


	<div class="content_table">
		<div class="state_tabs display_list" >
					<div class="state_tab" ><?php echo cms_get_lang('follow|date|index');?></div>
					<div class="state_tab"><?php echo cms_get_lang('custom|index');?></div>
					<?php
					if(empty($_GET['sp_id'])){
						?>
					<div style="float:left; margin-left:10px;padding:2px;width:200px;<?php if (empty($extend_langs_str)){?> display:none;<?php }?>"  >
						<?php echo cms_get_lang('lang|select');?>：
						<select type="text" name="language_sel" id="language_sel" >
							<?php
							$current_lang=g_cms_config::get_g_current_language();
								echo '<option value=\''.$current_lang.'\' >'.get_language_name_by_code($current_lang).' </option>';
							foreach ($extend_langs as $lang){
								echo '<option value=\''.$lang.'\' >'.get_language_name_by_code($lang).' </option>';
							}?>
						</select>
					</div>
					<?php
					}
					?>

					<div style="clear:both;"></div>
		</div>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>

			<tr id="day_pick_str">
				<td  colspan="4">
						<div class="quike_btns" style="padding:5px;">
						&nbsp;&nbsp;&nbsp;&nbsp;<?php echo cms_get_lang('media_playbill_select_date');?> : <input name="day_picker" id="day_picker" type="text"
					 value="<?php echo $begin_date?>"  style="width:120px;" class="datepicker" callback="test" />
					 &nbsp;&nbsp;&nbsp;&nbsp;
					 <input type="button" value="<?php echo cms_get_lang('search');?>" onclick="refresh_search_list();"/>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="button" value="<< <?php echo cms_get_lang('previous_day');?>" onclick="pre_day();"/>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="button" value="<?php echo cms_get_lang('next_day');?> >>" onclick="next_day();"/>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="button" value="<?php echo cms_get_lang('current_day');?>" onclick="crrent_day();"/>
						</div>

					 </td>
			</tr>

			<tr id="custom_pick_str">
				<td  colspan="4" style="background:#fff;">
				<div class="quike_btns" style="padding:5px; background:#fff;">
					&nbsp;&nbsp;&nbsp;&nbsp;<?php echo cms_get_lang('media_playbill_select_date');?> : <input name="playbill_date_begin" id="playbill_date_begin" type="text"
					 value="<?php echo $begin_date?> 00:00:00"  style="width:120px;" class="datetimepicker"/> - <input name="playbill_date_end" id="playbill_date_end" type="text"
					 value="<?php echo $begin_date?> 23:59:59"   style="width:120px;" class="datetimepicker"/>
					 &nbsp;&nbsp;&nbsp;&nbsp;<?php echo cms_get_lang('media_playbill_jmmc');?> : <input name="playbill_name" id="playbill_name" type="text"
					  style="width:120px;" />
					 &nbsp;&nbsp;&nbsp;&nbsp;
					 <input type="button" value="<?php echo cms_get_lang('search');?>" onclick="refresh_search_list();"/>

					 </div>
					 <!--&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="<?php echo cms_get_lang('media_playbill_dcjmd');?>" onclick="self.location='nncms_content_playbill_export.php?nns_id=<?php echo $play_id;?>';"/>
					 &nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="<?php echo cms_get_lang('media_playbill_drjmd');?>" onclick="self.location='nncms_content_playbill_import.php?nns_id=<?php echo $play_id;?>';"/>-->
					 </td>
			</tr>
		  </tbody>
		</table>

	</div>
	<div class="playbill_frame">
			<iframe  scrolling="no" frameborder="0" onload="checkheight();" style="width:100%;"></iframe>
		</div>

</div>





</body>
</html>
