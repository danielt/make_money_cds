<?php
/*
 * Created on 2013-1-16
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 /** GXCATV节目单导入主体方法
 * @param String 
 * @return 错误描述
 * 			成功为"success"
 */
 function gxcatv_import_playbill(){
 	$fileParts = pathinfo($_FILES['Filedata']['name']);
	if (strtolower($fileParts['extension'])=='txt'){
//		导入文件为TXT 则调用TXT解析
		$state=gxcatv_import_playbill_by_txt();
	}elseif(strtolower($fileParts['extension'])=='xml'){
//		导入文件为EXCEL 则调用EXCEL解析
		$state=gxcatv_import_playbill_by_xml();
    }else{
        $state='';
    }
        
        return $state;
 }
 
 
 function gxcatv_import_playbill_by_xml(){
 	$tempFile = $_FILES['Filedata']['tmp_name'];
 	$file_name=$_FILES['Filedata']['name'];
 	$file_info=explode('__',$file_name);
 	$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
			));
	$dc->open();
	$pro_id=$file_info[0];
	$play_bill_str=file_get_contents($tempFile);
	$play_bill_xml = new DOMDocument("1.0","UTF-8");
	try{
//		error_log('$play_bill_str:'.$play_bill_str."\n",3,'e:\test'.date('His').'.txt');
//		$play_bill_str=iconv('gbk','utf-8',$play_bill_str);
       	$bool=$play_bill_xml->loadXML($play_bill_str);
//       	error_log('$bool:'.var_export($bool,true)."\n",3,'e:\test'.date('His').'.txt');
       	if (!$bool){
       		return 'XML格式错误！';
       	}
		$live_list=nl_live_bo::get_video_ids_by_pid($dc->db(),$pro_id);
//				error_log('$live_list:'.var_export($live_list,true)."\n",3,'e:\test'.date('His').'.txt');
		if ($live_list===FALSE || $live_list===TRUE) return '该频道不存在！';
		foreach ($live_list as $live_item){
			gxcatv_import_playbill_to_video($dc,$play_bill_xml,$live_item['nns_live_id']);
		}
		
		return 'success';
		
	}catch(Exception $e){
		return 'XML格式错误！';
//			var_dump($e);
	}
 }
 
 function gxcatv_import_playbill_by_txt(){
 	$tempFile = $_FILES['Filedata']['tmp_name'];
 	$file_name=$_FILES['Filedata']['name'];
 	$file_info=explode('__',$file_name);
 	$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
			));
	$dc->open();
	$pro_id=$file_info[0];
	$play_bill_str=file_get_contents($tempFile);
//	error_log('string:'.$play_bill_str."\n",3,'e:\test'.date('His').'.txt');
	try{
       
		$live_list=nl_live_bo::get_video_ids_by_pid($dc->db(),$pro_id);
		if ($live_list===FALSE || $live_list===TRUE) return '该频道不存在！';
		foreach ($live_list as $live_item){
			gxcatv_import_playbill_to_video_by_txt($dc,$play_bill_str,$live_item['nns_live_id']);
		}
		
		return 'success';
		
	}catch(Exception $e){
		return 'TXT格式错误！';
//			var_dump($e);
	}
 }
 
 function create_full_date($gxcatv_date){
 	$gxcatv_dates=explode("/",$gxcatv_date);
 	return '20'.$gxcatv_dates[0].$gxcatv_dates[1].$gxcatv_dates[2];
 }
 
 function gxcatv_import_playbill_to_video_by_txt($dc,$play_bill_txt,$video_id){
// 	error_log('string:'.$play_bill_txt."\n",3,'e:\test'.date('His').'.txt');
 	if (empty($play_bill_txt)) return;
 	$play_bill_txt=str_replace("\r","",$play_bill_txt);
	$playbill_len_arr=explode("\n\n",$play_bill_txt);
 	$num=0;
 	$playbill_day_info='';
 	foreach ($playbill_len_arr as $playbill_day){
 		$playbill_day=iconv('gbk','utf-8',$playbill_day);
 		if ($num%2==0 || $num==0){
// 			error_log('string_day:'.$playbill_day."\n",3,'e:\test'.date('His').'.txt');
 			$playbill_day=rtrim($playbill_day);
 			$playbill_day_info=explode(" ",$playbill_day);
// 			error_log('string_day1:'.create_full_date($playbill_day_info[0])."\n",3,'e:\test'.date('His').'.txt');
 			$playbill_day_info=date('Y-m-d',strtotime(create_full_date($playbill_day_info[0])));
 			
 		}else{
 			$playbill_day=rtrim($playbill_day,"\n");
 			$playbill_info_arr=explode("\n",$playbill_day);
 			$len=count($playbill_info_arr);
 			for($i=0;$i<$len;$i++){
 				$now_item=$playbill_info_arr[$i];
 				$now_item=explode(" ",$now_item);
 				$params=array();
 				$params['begin_time']=	date('Y-m-d H:i:s',strtotime($playbill_day_info.' '.$now_item[0]));
 				if ($i==$len-1){
 					$params['time_len']=strtotime(date('Y-m-d',strtotime('+1 day')).' 00:00:00')-strtotime($now_item[0]);
 				}else{
 					$next_item=$playbill_info_arr[$i+1];
 					$next_item=explode(" ",$next_item);
 					$params['time_len']=strtotime($next_item[0])-strtotime($now_item[0]);
 				}
 				$params['name']=$now_item[1];
 				$params['pinyin']=nns_controls_pinyin_class::get_pinyin_letter($now_item[1]);
// 				error_log('$video_id:'.$video_id."\n",3,'e:\test'.date('His').'.txt');
// 				error_log('import:'.var_export($params,true)."\n",3,'e:\test'.date('His').'.txt');
 				nl_playbill_item::add_playbill_item($dc,$video_id,$params);
 			}
 		}
 		$num++;
 	}
 }
 
function gxcatv_import_playbill_to_video($dc,$play_bill_xml,$video_id){

	foreach($play_bill_xml->getElementsByTagName("Event") as $item){
//		error_log('step:'.var_export($item,true)."\n",3,'e:\test'.date('His').'.txt');
			$import_date=date("Y-m-d H:i:s",strtotime($item->getAttribute('begintime')));
			$params['begin_time']=$import_date;
			$params['time_len']=strtotime($item->getAttribute('duration'))-strtotime('000000');
			
			foreach ($item->getElementsByTagName('EventText') as $name_item){
				if ($name_item->getAttribute('language')=='chi'){
					$params['name']=$name_item->getElementsByTagName('Name')->item(0)->nodeValue;
					break;
				}
			}
//			error_log('import:'.var_export($params,true)."\n",3,'e:\test'.date('His').'.txt');
			nl_playbill_item::add_playbill_item($dc,$video_id,$params);
		};
}
?>
