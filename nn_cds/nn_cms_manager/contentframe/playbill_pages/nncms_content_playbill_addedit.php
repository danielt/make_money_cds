<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/language_contant.php";
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";


//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();

$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107103");
if ($_SESSION["nns_manager_type"] != 0)
        $pri_bool = false;
$checkpri = null;
if (!$pri_bool)
        Header("Location: ../nncms_content_wrong.php");

$live_id = $_GET["live_id"];
$action = $_GET["action"];
$nns_id = $_GET["nns_id"];
$add_day = $_GET["day"];
$default_day = $_GET["default_date"];
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");
include $nncms_db_path . "nns_live/nns_db_live_class.php";
include_once $nncms_db_path . "nns_live/nns_db_live_ex_class.php";
include_once LOGIC_DIR . 'playbill' . DIRECTORY_SEPARATOR . 'playbill_item_info_language.class.php';
include_once LOGIC_DIR . 'playbill' . DIRECTORY_SEPARATOR . 'playbill_item.class.php';
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
$dc->open();
$data = array();
if ($action == 'edit') {
        $data_info = nl_playbill_item::get_playbill_item_info($dc, $nns_id, null,NL_DC_DB);

        if (is_array($data_info)) {
                $data = $data_info[0];
                $add_day = date('Y-m-d', strtotime($data['nns_begin_time']));
        }

        $live_id = $data['nns_live_id'];
} else {

        if (!empty($add_day)) {
                $add_day = date('Y-m-d', strtotime($add_day));
        }
}

if (empty($add_day))
        $add_day = date('Y-m-d', time());

$extend_langs_str = g_cms_config::get_g_extend_language();
if (empty($extend_langs_str)) {
        $extend_langs = array();
} else {
        $extend_langs = explode('|', $extend_langs_str);
}

$time_str = date('His', strtotime($data['nns_begin_time']));

$live_inst = new nns_db_live_class();
$live_info_arr = $live_inst->nns_db_live_info($live_id);
// var_dump($live_info_arr); die;
if ($live_info_arr["ret"] == 0) {
        $live_info = $live_info_arr["data"][0];
}

$live_ex_inst = new nns_db_live_ex_class();
$play_bill_value = $live_ex_inst->nns_db_live_ex_info($live_id, 'play_bill_type');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../../js/table.js.php"></script>
                <script language="javascript" src="../../js/radiolist.js"></script>
                <script language="javascript" src="../../js/checkinput.js.php"></script>
                <script language="javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
                <script language="javascript" src="../../js/cms_datepicker.js"></script>
                <script language="javascript">
                        function show_extend_lang(num) {

                                if ($(".lang_editor:eq(" + num + ")").is(":hidden") == false) {
                                        $(".lang_editor").hide();
                                        $(".lang_btn").html("<?php echo cms_get_lang('spread'); ?>");
                                } else {

                                        $(".lang_editor").hide();
                                        $(".lang_btn").html("<?php echo cms_get_lang('spread'); ?>");
                                        $(".lang_editor:eq(" + num + ")").show();
                                        $(".lang_btn:eq(" + num + ")").html("<?php echo cms_get_lang('fold'); ?>");
                                }
                                window.parent.checkheight();
                        }

                        function set_show_minor(num) {
                                $obj = $("input[name=moreinfo_chk]:eq(" + num + ")");
                                if ($obj.attr("checked") == "checked") {
                                        $(".extend_" + num).show();
                                } else {
                                        $(".extend_" + num).hide();
                                }
                                window.parent.checkheight();
                        }

                        $(document).ready(function() {
                                $(".lang_editor").hide();
                                show_extend_lang(0);
                                var len = $("input[name=moreinfo_chk]").length;
                                for (var i = 0; i < len; i++) {
                                        set_show_minor(i);
                                }
                        });
                </script>
        </head>

        <body>


                <div class="content" style="min-height:300px; background:#fff;">
                        <form id="add_form" action="nncms_content_playbill_item_control.php" method="post"  enctype="multipart/form-data">
                                <input name="action" id="action" type="hidden" value="<?php echo $action; ?>" />
                                <input name="nns_id" id="nns_id" type="hidden" value="<?php echo $nns_id; ?>" />
                                <input name="live_id" id="nns_id" type="hidden" value="<?php echo $live_id; ?>" />
                                <div class="content_table">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                        <tr>
                                                                <td class="rightstyle"><?php echo $language_media_zbpdmc; ?></td>
                                                                <td style="max-width:400px;"><?php echo $live_info["nns_name"]; ?></td>
                                                                <td width="120"><?php echo cms_get_lang('jmdgl_jmdcl'); ?></td>
                                                                <td><?php echo get_video_ex_name($play_bill_type); ?>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('program|name'); ?>:</td>
                                                                <td colspan="3"><input name="nns_name" type="text" rule="noempty"
                                                                                       value="<?php echo $data['nns_name']; ?>"
                                                                                       /> </td>

                                                        </tr>
                                                        <?php if ($action == 'edit') {
                                                                ?>
                                                                <tr>
                                                                        <td class="rightstyle"><?php echo cms_get_lang('name|media_spell'); ?>:</td>
                                                                        <td colspan="3"><input name="pinyin" type="text"
                                                                                               value="<?php echo $data['nns_pinyin']; ?>"
                                                                                               /></td>

                                                                </tr>
                                                        <?php } ?>
                                                        <tr>
                                                                <td class="rightstyle"><?php echo cms_get_lang('english|name'); ?>:</td>
                                                                <td colspan="3"><input name="eng_name" type="text"
                                                                                       value="<?php echo $data['nns_eng_name']; ?>"
                                                                                       /></td>

                                                        </tr>
                                                        <tr>
                                                                <td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('program|media_bfsj'); ?>:</td>
                                                                <td colspan="3"><input name="begin_date" id="begin_date" type="text" rule="noempty" style="width:120px;"
                                                                                       value="<?php echo $add_day; ?>" class="datepicker" />
                                                                        <select type="text" name="show_begin_hour" value="<?php echo substr($time_str, 0, 2); ?>" style="margin:5px 0px;width:40px;">
                                                                                <?php
                                                                                for ($h = 0; $h < 24; $h++) {
                                                                                        $h = strlen($h) < 2 ? "0" . $h : $h;
                                                                                        $selected = "";

                                                                                        if ($h == substr($time_str, 0, 2))
                                                                                                $selected = ' selected="selected" ';
                                                                                        echo '<option value="' . $h . '" ' . $selected . ' >' . $h . '</option>';
                                                                                }
                                                                                ?>
                                                                        </select> <?php echo cms_get_lang('playbill_hour'); ?>
                                                                        <select type="text" name="show_begin_minute" value="<?php echo substr($time_str, 2, 2); ?>" style="margin:5px 0px;width:40px;">
                                                                                <?php
                                                                                for ($m = 0; $m < 60; $m++) {
                                                                                        $m = strlen($m) < 2 ? "0" . $m : $m;
                                                                                        $selected = "";
                                                                                        if ($action == 'edit') {
                                                                                                if ($m == substr($time_str, 2, 2))
                                                                                                        $selected = ' selected="selected" ';
                                                                                        }
                                                                                        echo '<option value="' . $m . '" ' . $selected . ' >' . $m . '</option>';
                                                                                }
                                                                                ?>
                                                                        </select> <?php echo cms_get_lang('point'); ?>

                                                                        <select type="text" name="show_begin_sencond" value="<?php echo substr($time_str, 4, 2); ?>" style="margin:5px 0px;width:40px;">
                                                                                <?php
                                                                                for ($s = 0; $s < 60; $s++) {
                                                                                        $s = strlen($s) < 2 ? "0" . $s : $s;
                                                                                        $selected = "";
                                                                                        if ($action == 'edit') {
                                                                                                if ($s == substr($time_str, 4, 2))
                                                                                                        $selected = ' selected="selected" ';
                                                                                        }
                                                                                        echo '<option value="' . $s . '" ' . $selected . ' >' . $s . '</option>';
                                                                                }
                                                                                ?>
                                                                        </select> <?php echo cms_get_lang('second'); ?> </td>

                                                        </tr>
                                                        <tr>
                                                                <td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('program|play|media_zscm'); ?>:</td>
                                                                <td colspan="3"><input name="time_len" type="text" rule="noempty|int"
                                                                                       value="<?php echo $data['nns_time_len']; ?>" rules="noempty"
                                                                                       /></td>

                                                        </tr>
                                                        <tr>
                                                                <td class="rightstyle"><?php echo cms_get_lang('program|category'); ?>:</td>
                                                                <td colspan="3"><input name="kind" type="text"
                                                                                       value="<?php echo $data['nns_kind']; ?>"
                                                                                       /></td>

                                                        </tr>
                                                        <tr>
                                                                <td class="rightstyle"><?php echo cms_get_lang('program|keyword'); ?>:</td>
                                                                <td colspan="3"><input name="keyword" type="text"
                                                                                       value="<?php echo $data['nns_keyword']; ?>"
                                                                                       /></td>

                                                        </tr>
                                                        <tr>
                                                                <td class="rightstyle"><?php echo cms_get_lang('program|media_big|media_bjhb'); ?>:</td>
                                                                <td colspan="3">
                                                                        <div class="img_content">
                                                                                <?php if (!empty($data["nns_image0"])) { ?>
                                                                                        <div class="img_view"><h4><?php echo $language_media_addedit_pictrue; ?></h4>
                                                                                                <a href="<?php echo pub_func_get_image_url($data["nns_image0"]); ?>?rand=<?php rand(); ?>" target="_blank">
                                                                                                        <img src="<?php echo pub_func_get_image_url($data["nns_image0"]); ?>?rand=<?php rand(); ?>" style="border:none;" />
                                                                                                </a><h4 class="resolution"></h4></div>
                                                                                <?php } ?>
                                                                                <div class="img_edit"> <input name="nns_image0" id="nns_image0" type="file" size="60%"/></div>
                                                                                <div style="clear:both;"/>
                                                                        </div>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td class="rightstyle"><?php echo cms_get_lang('program|media_normal|media_bjhb'); ?>:</td>
                                                                <td colspan="3">
                                                                        <div class="img_content">
                                                                                <?php if (!empty($data["nns_image1"])) { ?>
                                                                                        <div class="img_view"><h4><?php echo $language_media_addedit_pictrue; ?></h4>
                                                                                                <a href="<?php echo pub_func_get_image_url($data["nns_image1"]); ?>?rand=<?php rand(); ?>" target="_blank">
                                                                                                        <img src="<?php echo pub_func_get_image_url($data["nns_image1"]); ?>?rand=<?php rand(); ?>" style="border:none;" />
                                                                                                </a><h4 class="resolution"></h4></div>
                                                                                <?php } ?>
                                                                                <div class="img_edit"> <input name="nns_image1" id="nns_image1" type="file" size="60%"/></div>
                                                                                <div style="clear:both;"/>
                                                                        </div>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td class="rightstyle"><?php echo cms_get_lang('program|media_small|media_bjhb'); ?>:</td>
                                                                <td colspan="3">
                                                                        <div class="img_content">
                                                                                <?php if (!empty($data["nns_image2"])) { ?>
                                                                                        <div class="img_view"><h4><?php echo $language_media_addedit_pictrue; ?></h4>
                                                                                                <a href="<?php echo pub_func_get_image_url($data["nns_image2"]); ?>?rand=<?php echo rand(); ?>" target="_blank">
                                                                                                        <img src="<?php echo pub_func_get_image_url($data["nns_image2"]); ?>?rand=<?php echo rand(); ?>" style="border:none;" />
                                                                                                </a><h4 class="resolution"></h4></div>
                                                                                <?php } ?>
                                                                                <div class = "img_edit"> <input name = "nns_image2" id = "nns_image2" type = "file" size = "60%"/></div>
                                                                                <div style = "clear:both;"/>
                                                                        </div>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td class = "rightstyle"><?php echo cms_get_lang('program|introduction');
                                                                                ?>:</td>
                                                                <td colspan="3"><textarea name="nns_summary" cols="" rows=""><?php echo $data['nns_summary']; ?></textarea></td>
                                                        </tr>

                                                        <!--语言扩展-->
                                                        <?php
                                                        $num = 0;
                                                        foreach ($extend_langs as $extend_lang) {
                                                                $result = nl_playbill_item_info_language::get_playbill_item_info_by_language($dc, $nns_id, $extend_lang, NL_DC_DB);
                                                                if ($result !== FALSE || $result !== TRUE) {
                                                                        $data = $result[0];
                                                                } else {
                                                                        $data = NULL;
                                                                }
                                                                ?>
                                                                <tr>
                                                                        <td colspan="4"   style="padding:0px;">
                                                                                <div class="radiolist" style=" border-bottom:1px solid #86B7CC;"><div class="radiogroup">
                                                                                                <h3 style="  background-color:#DCEEF5;">
                                                                                                        <?php
                                                                                                        $lang_name = get_language_name_by_code($extend_lang);
                                                                                                        echo $lang_name, cms_get_lang('yyb|kz');
                                                                                                        ?>
                                                                                                        <div style='padding:2px 5px 2px 20px; float:right; background:url(../../images/add_1.png) 0px 50% no-repeat;'>
                                                                                                                <a href='javascript:show_extend_lang(<?php echo $num; ?>);' class="lang_btn"> <?php echo cms_get_lang('spread'); ?></a></div>
                                                                                                </h3>

                                                                                        </div></div></td>
                                                                </tr>
                                                                <tr class='lang_editor'>
                                                                        <td colspan="4"   style="padding:0px;">
                                                                                <div class="content_table">
                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                <tbody>
                                                                                                        <tr>
                                                                                                                <td width="120"><?php echo $lang_name, cms_get_lang('media_playbill_jmmc'); ?>:</td>
                                                                                                                <td><input name="nns_name_lan[]" type="text"
                                                                                                                           value="<?php echo $data['nns_name']; ?>"
                                                                                                                           /> </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                                <td width="120"><?php echo $lang_name, cms_get_lang('program|introduction'); ?>:</td>
                                                                                                                <td><textarea name="nns_summary_lan[]" cols="" rows=""><?php echo $data['nns_summary']; ?></textarea>
                                                                                                                </td>
                                                                                                        </tr>


                                                                                                </tbody>
                                                                                        </table>
                                                                                </div>
                                                                        </td>
                                                                </tr>
                                                                <?php
                                                                $num++;
                                                        }
                                                        ?>
                                                </tbody>
                                        </table>
                                </div>
                        </form>
                        <div class="controlbtns">
                                <div class="controlbtn <?php echo $action; ?>"><a href="javascript:checkForm('<?php echo cms_get_lang('jmdgl'); ?>','<?php echo cms_get_lang('msg_ask_change'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php
                                                if ($action == 'add') {
                                                        echo cms_get_lang('add');
                                                } else {
                                                        echo cms_get_lang('edit');
                                                }
                                                ?></a></div>
                                <div class="controlbtn back"><a href="javascript:history.go(-1);"><?php echo cms_get_lang('back'); ?></a></div>
                                <div style="clear:both;"></div>
                        </div>
                </div>
        </body>
</html>
<script language="javascript" src="../../js/image_loaded_func.js"></script>