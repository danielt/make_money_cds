<?php
set_time_limit(0);
header("Content-Type:text/html;charset=utf-8");
//ob_start();
include("../../nncms_manager_inc.php");

include_once LOGIC_DIR . 'playbill' . DIRECTORY_SEPARATOR . 'playbill_item.class.php';
include_once LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'live_bo.class.php';
include_once LOGIC_DIR . 'log' . DIRECTORY_SEPARATOR . 'manager_log.class.php';
/**
 * GXCATV专用
 */
include_once 'gxcatv_import_playbill_by_xml.func.php';

//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language_media.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_pinyin_class.php";
include $nncms_db_path . "nns_live/nns_db_live_class.php";
include "./mgtv_import_playbill_by_xml.func.php";
$nns_id = isset($_POST["nns_id"]) ? $_POST["nns_id"] : '';
// Define a destination
$targetFolder = dirname(__FILE__); // Relative to the root
$times = isset($_POST['timestamp']) ? $_POST['timestamp'] : '';
$verifyToken = md5('unique_salt' . $times);

if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
        $targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
        //$targetPath=$targetFolder;
        $targetFile = rtrim($targetPath, '/') . '/' . $_FILES['Filedata']['name'];
        // Validate the file type
        $fileTypes = array('xml', 'txt', 'xls'); // File extensions
        $state = mgtv_import_playbill();
        //ob_end_clean();
        exit($state);
}

function import_xml_to_playbill($nns_id) {

        $tempFile = $_FILES['Filedata']['tmp_name'];
        $dc = nl_get_dc(array(
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
        $dc->open();
        $play_bill_str = file_get_contents($tempFile);
        $play_bill_xml = new DOMDocument("1.0", "UTF-8");
        try {
//		error_log($play_bill_str."\n",'3','E:\test.txt');
                $play_bill_xml->loadXML($play_bill_str);
                $playbill_program = $play_bill_xml->getElementsByTagName("program");
                $pro_id = $playbill_program->item(0)->getAttribute('pid');
                $live_list = nl_live_bo::get_video_ids_by_pid($dc->db(), $pro_id);
//		error_log('db1:'.var_export($live_list)."\n",3,'e:\test.txt');
                if ($live_list === FALSE || $live_list === TRUE) {
                        if (empty($nns_id)) {
                                return 'No video';
                        } else {
                                $live_list = array(
                                    0 => array(
                                        'nns_live_id' => $nns_id
                                    )
                                );
                        }
                };



                foreach ($live_list as $live_item) {
                        import_playbill_to_video($dc, $play_bill_xml, $live_item['nns_live_id']);
                }

                return '1';
        } catch (Exception $e) {
                return '500 error';
//			var_dump($e);
        }
}

function import_playbill_to_video($dc, $play_bill_xml, $video_id) {
        foreach ($play_bill_xml->getElementsByTagName("day") as $day_item) {
                $import_date = date("Ymd", strtotime($day_item->getAttribute('day')));
                foreach ($day_item->getElementsByTagName('item') as $playbill_item) {
                        $params = array();
                        $params['name'] = $playbill_item->getAttribute('text');
                        $params['pinyin'] = nns_controls_pinyin_class::get_pinyin_letter($params['name']);
                        $playbill_begin_time = $playbill_item->getAttribute('begin');
                        $params['time_len'] = $playbill_item->getAttribute('time_len');

                        $playbill_begin_time = $import_date . 'T' . $playbill_begin_time;
                        $params['begin_time'] = date('Y-m-d H:i:s', strtotime($playbill_begin_time));

//				error_log('db2_params:'.var_export($params)."\n",3,'e:\test.txt');

                        nl_playbill_item::add_playbill_item($dc, $video_id, $params);
                }
        };
}

?>