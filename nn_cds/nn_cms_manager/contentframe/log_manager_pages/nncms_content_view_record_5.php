<?php
/*
 * Created on 2012-8-1
 *  实时模板页面访问
 * (5分钟的历史记录)
 *
 */
header("Content-Type:text/html;charset=utf-8");

include("../../nncms_manager_inc.php");
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_db_path. "nns_log/nns_db_view_record_class.php";
include $nncms_db_path. "nns_common/nns_db_pager_class.php";
include $nncms_db_path. "nns_vod/nns_db_vod_class.php";
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;

$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"114101");

$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");

include ($nncms_db_path. "nns_template/nns_db_template_class.php");
$template_class_ins = nns_db_template_class::get_instance();

$db = new nns_db_view_record_class();

$cur_page = isset($_GET['page'])?$_GET['page']:1;
$cur_page =(int)$cur_page;

if (!empty($_COOKIE["page_max_num"])){
	$page_size = intval($_COOKIE["page_max_num"]);
}else if(isset($_GET['page_size']) && $_GET['page_size'] >0){
	$page_size = $_GET['page_size'];
}else{
	$page_size = $g_manager_list_max_num;
}

$now = time();
$_GET['begin_time'] = date('Y-m-d H:i:s',$now-5*60);
$_GET['end_time'] = date('Y-m-d H:i:s',$now);


$count = $db->view_record_count($_GET);
$url = '?';
if(isset($_GET['tpl_name'])) $url .="tpl_name=".$_GET['tpl_name'];
if(isset($_GET['tpl_type_id']))  $url .= "&tpl_type_id=".$_GET['tpl_type_id'];
if(isset($_GET['tpl_id'])) $url .= "&tpl_id=".$_GET['tpl_id'];
if(isset($_GET['device_id'])) $url .= "&device_id=".$_GET['device_id'];
if(isset($_GET['user_id'])) $url .= "&user_id=".$_GET['user_id'];
//if(isset($_GET['begin_time'])) $url .= "&begin_time=".$_GET['begin_time'];
//if(isset($_GET['end_time'])) $url .= "&end_time=".$_GET['end_time'];

$url .= "&page_size=".$page_size;
$url = str_replace('?&','?',$url);

$pager = new nns_db_pager_class($count,$page_size,$cur_page,$url);

$view_record_list = $db->view_record_list($_GET,($cur_page-1)*$page_size,$page_size);
//取影片名称
$media_ids = array();
foreach($view_record_list as $item){
	if(!empty($item['nns_content_id'])){
		$media_ids[] = $item['nns_content_id'];
	}
}
$vod_media_ins = new nns_db_vod_class();
$vod_media_arr =  $vod_media_ins->get_vod_by_ids($media_ids);
//取模板目录
$tpl_type_arr=getDirFiles($nncms_config_path. "data/template");
$js_tpl_type_data= 'var js_tpl_type_data=[];'."\n";
$js_tpl_id_data= 'var js_tpl_id_data=[];'."\n";
$js_tpl_data= 'var js_tpl_data=[];'."\n";
//全部模板中文名称
$tpl_cn_name_arr = array();
foreach($tpl_type_arr as $tpl_type){
	if(is_dir($nncms_config_path. "data/template/".$tpl_type)){
		$js_tpl_type_data .= 'js_tpl_type_data.push("'.$tpl_type.'");'."\n";
		$js_tpl_id_data .= 'js_tpl_id_data["'.$tpl_type.'"]=[];'."\n";
		$js_tpl_data .= 'js_tpl_data["'.$tpl_type.'"]=[];'."\n";
		$tpl_id_arr = getDirFiles($nncms_config_path. "data/template/". $tpl_type );
		foreach($tpl_id_arr as $tpl_id){
			if(is_dir($nncms_config_path. "data/template/".$tpl_type.'/'.$tpl_id)){
				$js_tpl_id_data .= 'js_tpl_id_data["'.$tpl_type.'"].push("'.$tpl_id.'");'."\n";
				$js_tpl_data .= 'js_tpl_data["'.$tpl_type.'"]["'.$tpl_id.'"]=[];'."\n";
				//读取模板中文名
				//$template_config = array();
				$_GET['template_cn_name'] = $_GET['nns_template_name'];//默认模板中文
				/*
				$template_ini = $nncms_config_path. "nn_cms_view/template/". $tpl_type.'/'.$tpl_id.'/template_config.ini';
				if(file_exists($template_ini)){
					$template_config = parse_ini_file($template_ini);

				}*/
				$template_config = $template_class_ins->load_config($tpl_type,$tpl_id,true);

				$tpl_file_arr = getDirFiles($nncms_config_path. "data/template/". $tpl_type.'/'.$tpl_id );

				$js_str ='';
				foreach($tpl_file_arr as $tpl_file){
					if(is_file($nncms_config_path. "data/template/". $tpl_type.'/'.$tpl_id.'/'.$tpl_file)){
						$ROW_1++;
						if(false !==($pos=strpos($tpl_file,"."))){
							$tpl_file_basename =  substr($tpl_file,0,$pos);
							$ext = substr($tpl_file,$pos+1);
							if($ext=='html'){


								$template_cn_name = substr($tpl_file,0,$pos);//$tpl_file;
								$template_name  = $tpl_file_basename;//substr($tpl_file,0,$pos);
								if(!empty($template_config['skin_config'][$template_name])){
									$template_cn_name = $template_config['skin_config'][$template_name];
								}
								//读取模板中文名
								$tpl_cn_name_arr[$tpl_type][$tpl_id][$template_name] =$template_cn_name;

								$js_tpl_data .= 'js_tpl_data["'.$tpl_type.'"]["'.$tpl_id.'"].push("'.$tpl_file_basename.'|'.$template_cn_name.'");'."\n";

							}
						}

					}
				}
			}
		}
	}
}

function getDirFiles($dir)
{
	$files = array();
	if ($handle = opendir($dir)){
		/* Because the return type could be false or other equivalent type(like 0),
		this is the correct way to loop over the directory. */
		while (false !== ($file = readdir($handle))) {
			if ($file!="." && $file!=".." && $file!=".svn"){
				$files[]=$file;
			}
		}
	}
	closedir($handle);

	return $files;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/tabs.js"></script>
<script language="javascript" src="../../js/cms_datepicker.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript">
$(document).ready(function(){

	$(document).keydown(function(e){

		if(e.which  ==13)
		{
			$('#form_inquery').submit();
		}

	});

	<?php echo $js_tpl_type_data;?>
	<?php echo $js_tpl_id_data;?>
	<?php echo $js_tpl_data;?>

	for(var i=0;i<js_tpl_type_data.length;i++){
		$('#tpl_type_id').append('<option value="'+js_tpl_type_data[i]+'">'+js_tpl_type_data[i]+'</option>');
	}

	<?php if(isset($_GET['tpl_type_id'])){?>
	$('#tpl_type_id').val('<?php echo $_GET['tpl_type_id'];?>');
	<?php }?>

	//初始化模板ID
	var tpl_type_id = $('#tpl_type_id').val();

	if(tpl_type_id !='0'){

		for(var i=0;i<js_tpl_id_data[tpl_type_id].length;i++){
			$('#tpl_id').append('<option value="'+js_tpl_id_data[tpl_type_id][i]+'">'+js_tpl_id_data[tpl_type_id][i]+'</option>');
		}
	}

	<?php if(isset($_GET['tpl_id'])){?>
	$('#tpl_id').val('<?php echo $_GET['tpl_id'];?>');
	<?php }?>

	$('#tpl_type_id').change(function(){
		$('#tpl_id').html('');
		var tpl_type_id = $(this).val();
		for(var i=0;i<js_tpl_id_data[tpl_type_id].length;i++){
			$('#tpl_id').append('<option value="'+js_tpl_id_data[tpl_type_id][i]+'">'+js_tpl_id_data[tpl_type_id][i]+'</option>');
		}
	});
	//初始化页面名称
	var tpl_id = $('#tpl_id').val();
	if(tpl_id !='0'){
		for(var i=0;i<js_tpl_data[tpl_type_id][tpl_id].length;i++){
			var tpl_name = (js_tpl_data[tpl_type_id][tpl_id][i]).split("|");

			$('#tpl_name').append('<option value="'+tpl_name[0]+'">'+tpl_name[1]+'</option>');
		}
	}

	<?php if(isset($_GET['tpl_name'])){?>
	$('#tpl_name').val('<?php echo $_GET['tpl_name'];?>');
	<?php }?>

	$('#tpl_id').change(function(){
		$('#tpl_name').html('');
		var tpl_type_id = $('#tpl_type_id').val();
		var tpl_id = $('#tpl_id').val();
		for(var i=0;i<js_tpl_data[tpl_type_id][tpl_id].length;i++){
			var tpl_name = (js_tpl_data[tpl_type_id][tpl_id][i]).split("|");

			$('#tpl_name').append('<option value="'+tpl_name[0]+'">'+tpl_name[1]+'</option>');
		}
	});


});

</script>
</head>

<body>
<div class="content">
	<div class="content_position"><?php echo cms_get_lang('stat');?> > <?php echo cms_get_lang('stat_view_record');?></div>
		<div class="content_table formtable">
		<form id="form_inquery" action="nncms_content_view_record_5.php" method="get">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">

			<tr>

				<td><?php echo cms_get_lang('mblx')?>:&nbsp;&nbsp;
				<select name="tpl_type_id" id="tpl_type_id" style="width:150px;" >
				<option value="0">...</option>
				</seletct>
				</td>
				<td><?php echo cms_get_lang('mbgl_mbys');?>:&nbsp;&nbsp;
				<select name="tpl_id" id="tpl_id" style="width:150px;" >
				<option value="0"><?php echo cms_get_lang('selected');?>...</option>
				</seletct>
				</td>
				 <td><?php echo cms_get_lang('mbgl_ymmc');?>:&nbsp;&nbsp;
					<select name="tpl_name" id="tpl_name" style="width:150px;" >
					<option value="0"><?php echo cms_get_lang('selected');?>...</option>
					</seletct>
				 </td>
				 <td>&nbsp;</td>


			</tr>
			<tr class="tr_odd">


				<td><?php echo cms_get_lang('zdgl_sbid');?>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['device_id'];?>"  name="device_id" style="width:150px;" /></td>
				<td><?php echo cms_get_lang('user_id')?>:&nbsp;&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['user_id'];?>"  name="user_id" style="width:150px;" /></td>

				<td>&nbsp;<input type="submit" value="<?php echo cms_get_lang('search');?>" id="inquery" name="inquery" /></td>

			</tr>


			</table>
			</form>
		 </div>

 <div class="content_table formtable">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<thead>
			<tr>

				<th><?php echo cms_get_lang('mblx');?></th>
				<th><?php echo cms_get_lang('mbgl_mbys')?></th>
				<th><?php echo cms_get_lang('mbgl_ymmc');?></th>
				<th><?php echo cms_get_lang('assist_id');?></th>
				<th><?php echo cms_get_lang('service_id');?></th>
				<th><?php echo cms_get_lang('lmgl_nrlmid');?></th>
				<th><?php echo cms_get_lang('stat_vod_name');?></th>
				<th><?php echo cms_get_lang('user_id');?></th>
				<th><?php echo cms_get_lang('zdgl_sbid');?></th>
				<th><?php echo cms_get_lang('user_ip');?></th>
				<th><?php echo cms_get_lang('stat_time');?></th>
			</tr>
		</thead>
		<tbody>
<?php
if (!empty($view_record_list)){
	foreach($view_record_list as $k =>$item){

?>
			  <tr>
				<td><?php echo $item["nns_template_type"];?></td>
				<td><?php echo $item["nns_template_id"];?> </td>
				<td><?php echo $tpl_cn_name_arr[$item["nns_template_type"]][$item["nns_template_id"]][$item["nns_template_name"]];?> </td>
				<td><?php echo $item["nns_asset_id"];?> </td>
				<td><?php echo $item["nns_service_id"];?> </td>
				<td><?php echo $item["nns_category_id"];?> </td>
				<td><?php
		if(!empty($vod_media_arr)){

			if(isset($item["nns_content_id"])){
				foreach( $vod_media_arr as $v_m){
					if($v_m['nns_id'] == $item["nns_content_id"]) echo '<alt alt="'.$item["nns_content_id"].'" href="javascript:void(null);">'.$v_m['nns_name'].'</alt>';
				}
			}
		}
?> </td>

				<td><?php echo $item["nns_user_id"];?> </td>
				<td><?php echo $item["nns_device_id"];?> </td>
				<td><?php echo $item["nns_user_ip"];?> </td>
				<td><?php echo $item["nns_create_time"];?> </td>
			  </tr>
<?php
	}}
$least_num=$nncms_ui_min_list_item_count-count($data_list);
for ($i=0;$i<$least_num;$i++){
?>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
		<?php	}?>
		  </tbody>
		</table>
	</div>
	<?php echo $pager->nav();?>

</div>
</body>
</html>
