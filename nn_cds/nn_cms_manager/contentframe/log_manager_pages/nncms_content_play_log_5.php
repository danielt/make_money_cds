<?php
/*
 * Created on 2012-6-28
 * 实时模板页面访问(5分钟播放日志)
 */
header("Content-Type:text/html;charset=utf-8");
include("../../nncms_manager_inc.php");
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_db_path. "nns_log/nns_db_play_log_class.php";
include $nncms_db_path. "nns_common/nns_db_pager_class.php";

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;

$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"114101");

$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");

$model = new nns_db_play_log_class();

$cur_page = isset($_GET['page'])?$_GET['page']:1;
$cur_page =(int)$cur_page;

if(!empty($_COOKIE["page_max_num"])){
	$page_size = intval($_COOKIE["page_max_num"]);
}else if(isset($_GET['page_size']) && $_GET['page_size'] >0){
	$page_size = $_GET['page_size'];
}else{
	$page_size = $g_manager_list_max_num;
}

$now = time();
$_GET['nns_begin_position'] = null;
$_GET['nns_end_position'] = null;//date('Y-m-d H:i:s',$now);

$_GET['play_over_time'] = date('Y-m-d H:i:s',$now-5*60);

$count = $model->nns_db_play_log_count($_GET);
$url = '?';

if(isset($_GET['nns_video_name'])){
	$url .="nns_video_name=".$_GET['nns_video_name'];
}
if(isset($_GET['nns_video_id']))  $url .= "&nns_video_id=".$_GET['nns_video_id'];
//if(isset($_GET['nns_begin_position'])) $url .= "&nns_begin_position=".$_GET['nns_begin_position'];
//if(isset($_GET['nns_end_position'])) $url .= "&nns_end_position=".$_GET['nns_end_position'];
if(isset($_GET['nns_video_type'])) $url .= "&nns_video_type=".$_GET['nns_video_type'];
if(isset($_GET['nns_device_id'])) $url .= "&nns_device_id=".$_GET['nns_device_id'];
if(isset($_GET['nns_qam_area_code'])) $url .= "&nns_qam_area_code=".$_GET['nns_qam_area_code'];
$url .= "&page_size=".$page_size;

$url = str_replace('?&','?',$url);

$pager = new nns_db_pager_class($count,$page_size,$cur_page,$url);

$data_list = $model->nns_db_play_log_list($_GET,($cur_page-1)*$page_size,$page_size);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/tabs.js"></script>
<script language="javascript" src="../../js/cms_datepicker.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript">
$(document).ready(function(){
	$(document).keydown(function(e){

		if(e.which  ==13)
		{
			$('#form_inquery').submit();
		}
	});

});

</script>
</head>

<body>
<div class="content">
	<div class="content_position"><?php echo cms_get_lang('stat_tongji_bb');?> &gt;<?php echo cms_get_lang('stat_bbrztj');?> </div>
	<form id="form_inquery" action="nncms_content_play_log_5.php" method="get">
		<div class="content_table formtable">

						<table width="100%" border="0" cellspacing="0" cellpadding="0">
						   <tbody>
								<tr>
									<td>
								 <?php echo cms_get_lang('stat_product');?>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_product_id'];?>"  name="nns_product_id" style="width:150px;"/>
									</td>

									<td>
									<?php echo cms_get_lang('stat_content_name');?>:&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_video_name'];?>"  name="nns_video_name" style="width:150px;"/>
									</td>
									<td>
								  <?php echo cms_get_lang('content_id');?>:&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_video_id'];?>"  name="nns_video_id"  style="width:150px;"/></td>
									<td><?php echo cms_get_lang('ipqam_dqm');?>:&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_qam_area_code'];?>"  name="nns_qam_area_code" style="width:150px;"/></td>
								<td>&nbsp;</td>
								</tr>
								<tr >
									<td>
								   <?php echo cms_get_lang('ipqam_addr');?>:&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_qam_ips'];?>"  name="nns_qam_ips" style="width:150px;"/>
									</td>
									<td>
									<?php echo cms_get_lang('ipqam_pd');?>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_qam_frequence'];?>"  name="nns_qam_frequence" style="width:150px;"/>
									</td>
									<td>
									VSIP:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_nmds_ips'];?>"  name="nns_nmds_ips" style="width:150px;"/>
									</td>
									<td>
								   <?php echo cms_get_lang('stat_fw_ip');?>:&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_ua_ips'];?>"  name="nns_ua_ips" style="width:150px;"/>
									</td>
									<td>&nbsp;</td>
								</tr>
								<tr>

									<td><?php echo cms_get_lang('user_id');?>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_user_id'];?>"  name="nns_user_id"  style="width:150px;"/></td>

									<td><?php echo cms_get_lang('zdgl_sbid');?>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_device_id'];?>"  name="nns_device_id" style="width:150px;"/></td>


									<td>&nbsp;<input type="submit" value="<?php echo cms_get_lang('selected');?>" id="inquery" name="inquery" /></td>
									<td>&nbsp;</td><td>&nbsp;</td>

								</tr>
						   </tbody>

					   </table>

		 </div></form>

 <div class="content_table formtable">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<th><?php echo cms_get_lang('user_id');?></th>
				<th><?php echo cms_get_lang('stat_product');?></th>

				<th><?php echo cms_get_lang('stat_vod_name');?></th>
				<th><?php echo cms_get_lang('ipqam_dqm');?></th>
				 <th><?php echo cms_get_lang('ipqam_addr');?></th>
				<th><?php echo cms_get_lang('ipqam_pd');?></th>
				<th>VSIP</th>

				<th><?php echo cms_get_lang('stat_bf_begin_time');?></th>
				<th><?php echo cms_get_lang('stat_bf_time_s');?></th>

				<th><?php echo cms_get_lang('zdgl_sbid');?></th>

				<th><?php echo cms_get_lang('stat_fw_ip');?></th>


			</tr>
		</thead>
		<tbody>
<?php
if (!empty($data_list)){
	foreach($data_list as $k =>$item){

?>
			  <tr>
				<td><?php echo $item["nns_user_id"];?></td>
				<td><?php echo $item["nns_product_id"];?> </td>

				<td><?php echo $item["nns_video_name"];?> </td>
				<td><?php echo $item["nns_qam_area_code"];?> </td>
				<td><?php echo $item["nns_qam_ips"];?> </td>
				<td><?php echo $item["nns_qam_frequence"];?> </td>
				<td><?php echo $item["nns_nmds_ips"];?> </td>
				<td><?php echo $item["nns_begin_position"];?> </td>
				<td><?php
		echo (strtotime($item["nns_end_position"]) - strtotime($item["nns_begin_position"]));?> </td>

				<td><?php echo $item["nns_device_id"];?> </td>

				<td><?php echo $item["nns_ua_ips"];?> </td>

			  </tr>
<?php
	}}
$least_num=$nncms_ui_min_list_item_count-count($data_list);
for ($i=0;$i<$least_num;$i++){
?>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
		<?php	}?>
		  </tbody>
		</table>
	</div>
	<?php echo $pager->nav();?>

</div>
</body>
</html>
