<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
ob_start();
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"100100");
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");



$op_admin_id=$_GET['op_admin_id'];//管理员账号
$org_id=$_SESSION['nns_org_id'];//查询的部门
$op_type=$_GET['op_type'];    //查询操作类型
$nns_op_desc = $_GET['nns_op_desc'];    //查询操作详情
$begin_time=$_GET['begin_time'];//开始时间
$end_time=$_GET['end_time'];     //结束时间
$nns_list_max_num=$_GET['page_max_num']; //每页的显示数
$current_page=$_GET['page'];//当前页数从1开始

//xyl 2013-07-12 17:12:36 增加nns_url 传递参数。方便后面导出数据
$nns_url="?&op_admin_id=$op_admin_id&begin_time=$begin_time&end_time=$end_time&op_type=$op_type&nns_op_desc=$nns_op_desc&org_type=&org_id=$org_id";

$nns_url = str_replace('?&','?',$nns_url);

if($current_page == null){
	$current_page=1;
}
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
 /*
  if (!empty($_COOKIE["nns_list_max_num"])){
		$g_manager_list_max_num=$_COOKIE["nns_list_max_num"];
	}
  */
include($nncms_db_path. "nns_live/nns_db_live_class.php");
include $nncms_db_path. "nns_common/nns_db_constant.php";
include $nncms_db_path. "nns_carrier/nns_db_carrier_class.php";
include $nncms_db_path. "nns_partner/nns_db_partner_class.php";
include $nncms_db_path. "nns_group/nns_db_group_class.php";
include $nncms_db_path. "nns_assist/nns_db_assist_item_class.php";
include $nncms_db_path. "nns_service/nns_db_service_category_class.php";
include $nncms_db_path. "nns_log/nns_db_op_log_class.php";

$opera_log=new nns_db_op_log_class();
$ret=$opera_log->nns_db_op_log_count($op_admin_id, $org_type, $org_id, $op_type, $begin_time, $end_time,$nns_op_desc);//查询符合条件的记录条数。

$num=$ret[data][0][num];
//设置每页的条数.
if (!empty($_COOKIE["page_max_num"])){
	$g_manager_list_max_num=$_COOKIE["page_max_num"];
}elseif($nns_list_max_num !=NULL){
	$g_manager_list_max_num=$nns_list_max_num;
}else{
	$g_manager_list_max_num=12;
}

//计算总的页数$page_num是总页数
$page_num=ceil($num/$g_manager_list_max_num);

$result=$opera_log->nns_db_op_log_list($op_admin_id,$org_type,$org_id,$op_type,$begin_time,$end_time,$nns_op_desc,($current_page-1)*$g_manager_list_max_num,$g_manager_list_max_num);

if ($result["ret"]!=0){
	$data=null;
	echo "<script>alert(". $result["reason"].");</script>";
}
$data=$result["data"];
ob_end_clean();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/dtree.js"></script>
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/tabs.js"></script>
<script language="javascript" src="../../js/cms_datepicker.js"></script>
<script language="javascript">
$(document).ready(function(){
	$("#inquery").click(function(){
		setCookie("nns_list_max_num",$("#nns_list_max_num").val());
		checkForm('','',$('#form_inquery'),"add");

	});
	$("#op_type").val("<?php echo $op_type;?>");

});



</script>
</head>

<body>
<div class="content">
	<div class="content_position"><?php echo cms_get_lang('xtpz'),' > ',cms_get_lang('log_rzgl');?></div>
		<div class="content_table formtable">
		<form id="form_inquery" action="nncms_content_opera_log.php" method="get">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><?php echo cms_get_lang('manager_id');?>:&nbsp;&nbsp;
						<input name="op_admin_id" id="op_admin_id" type="text" value="<?php echo $op_admin_id;?>" style="width:150px;" />
					</td>
					<td><?php echo cms_get_lang('log_detail');?>:&nbsp;&nbsp;<input name="nns_op_desc" id="nns_op_desc" type="text" value="<?php echo $nns_op_desc;?>" style="width:150px;" /></td>
					<td><?php echo cms_get_lang('log_type');?>:&nbsp;&nbsp;
						<select name="op_type" id="op_type">
							<option value="">--</option>
							<option value ="edit" ><?php echo cms_get_lang('edit');?></option>
							<option value ="delete"><?php echo cms_get_lang('delete');?></option>
							<option value="add"><?php echo cms_get_lang('add');?></option>
							<option value="login"><?php echo cms_get_lang('manager_login');?></option>
							<option value="audit"><?php echo cms_get_lang('audit'),'\\',cms_get_lang('cancel_audit');?></option>
							<option value="lock"><?php echo cms_get_lang('lock'),'\\',cms_get_lang('unlock'),cms_get_lang('zdgl_yh');?></option>
							<option value="order"><?php echo cms_get_lang('order');?></option>
							<option value="unline"><?php echo cms_get_lang('xtgl_xxyp');?></option>
							<option value="reset"><?php echo cms_get_lang('xtgl_hfhssj');?></option>
							<option value="real_delete"><?php echo cms_get_lang('xtgl_cdscsj');?></option>
							<option value="move_category"><?php echo cms_get_lang('move|lm');?></option>
						</select>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr class="tr_odd">
					<td><?php echo cms_get_lang('log_time');?>:&nbsp;&nbsp;&nbsp;
						<input name="begin_time" id="begin_time" type="text" value="<?php echo $begin_time;?>"  style="width:80px;" class="datepicker" /> - <input name="end_time" id="end_time" type="text" value="<?php echo $end_time;?>"  style="width:80px;" class="datepicker" overto="begin_time" />
					</td>
					<!--td><?php echo cms_get_lang('perpagenum');?></td>
					<td><input type="text" value="<?php echo $g_manager_list_max_num;?>" width="20px" name="page_max_numxxx" id="page_max_numxx" /></td-->
					<td><input type="button" value="<?php echo cms_get_lang('search');?>" id="inquery" name="inquery" /></td>
					<td>&nbsp;
						<input type="button" value="<?php echo cms_get_lang('export_excel');?>"  onClick="location.href='../../controls/nncms_controls_export_excel.php<?php echo $nns_url;?>&op=opera_log'" />
					</td>
					<td>&nbsp;</td><td>&nbsp;</td>
				</tr>
			</table>
		</form>
		</div>

 <div class="content_table formtable">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<th><?php echo cms_get_lang('segnumber');?></th>
				<th><?php echo cms_get_lang('manager_id');?></th>
				<th><?php echo cms_get_lang('log_type');?></th>
				<th><?php echo cms_get_lang('log_detail');?></th>
				 <th><?php echo cms_get_lang('log_time');?></th>
			</tr>
		</thead>
		<tbody>
<?php
if ($data!=null){
	$current_num=($current_page-1)*$g_manager_list_max_num; foreach($data as $item){
		$current_num++;

?>
			<tr>
				<td><?php echo $current_num;?></td>
				<td><?php echo $item["nns_op_admin_id"];?></td>
				<td><?php echo $item["nns_op_type"];?> </td>
				<td><div class="tddetail"><?php echo $item["nns_op_desc"];?></div> </td>
				<td><?php echo $item["nns_op_time"];?> </td>
			</tr>
<?php } $least_num=$nncms_ui_min_list_item_count-count($data);
for ($i=0;$i<$least_num;$i++){?>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<?php	}}?>
		  </tbody>
		</table>
	</div>
	 <div class="pagecontrol">
		<?php if ($current_page>1){?>
		<a href="nncms_content_opera_log.php?page=1<?php echo $nns_url;?>" target="_self"><?php echo cms_get_lang('first_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="nncms_content_opera_log.php?page=<?php echo $current_page-1;?><?php echo $nns_url;?>" target="_self"><?php echo cms_get_lang('pre_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php }else{?>
			<span><?php echo cms_get_lang('first_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
			<span><?php echo cms_get_lang('pre_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php } if ($current_page<$page_num){?>
		<a href="nncms_content_opera_log.php?page=<?php echo $current_page+1;?>&nns_list_max_num=<?php echo $nns_list_max_num;?><?php echo $nns_url;?>" target="_self"><?php echo cms_get_lang('next_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="nncms_content_opera_log.php?page=<?php echo $page_num;?><?php echo $nns_url;?>" target="_self"><?php echo cms_get_lang('last_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php }else{?>
			<span><?php echo cms_get_lang('next_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
			<span><?php echo cms_get_lang('last_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php }?>

		<?php echo cms_get_lang('jump_to');?> <input name="go_page_num" id="go_page_num" value="<?php echo $current_page;?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page');?> &nbsp;&nbsp;
		<a href="javascript:go_page_num('nncms_content_opera_log.php?ran=1<?php echo $nns_url;?>',<?php echo $page_num;?>);">GO>></a>&nbsp;&nbsp;
		<?php echo cms_get_lang('current');?><span style="font-weight:bold;color:#ff0000;"><?php echo $current_page."/".$page_num;?></span><?php echo cms_get_lang('page');?>&nbsp;&nbsp;|&nbsp;&nbsp;
		<?php echo cms_get_lang('perpagenum');?>&nbsp;
		 <input name="nns_list_max_num" id="nns_list_max_num" type="text" value="<?php echo $g_manager_list_max_num;?>" style="width:24px;"/>&nbsp;&nbsp;
		 <input type="button" value="<?php echo cms_get_lang('confirm');?>" onclick="refresh_prepage_list();"/>
	</div>
</div>
</body>
</html>

