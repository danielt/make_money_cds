<?php
/*
 * 实时访问统计（5分钟访问量）
 * nncms_content_access_count_5.php
 *
 * Created on 2012-7-6
 *
 * @author xsong512
 *
 */
 header("Content-Type:text/html; charset=UTF-8");
 include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;

$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"114101");

$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");

include ($nncms_db_path. "nns_template/nns_db_template_class.php");
$template_class_ins = nns_db_template_class::get_instance();

include $nncms_db_path. "nns_log/nns_db_access_count_class.php";
$model = new nns_db_access_count_class();
$data_list =$model->get_access_count_5();
$html_data = '';
if(!empty($data_list)){
	foreach($data_list as $item){
		//模板中文名字
	/*
		$template_config = null;
	$template_ini = $nncms_config_path. "nn_cms_view/template/". $item['nns_template_type'].'/'.$item['nns_template_id'].'/template_config.ini';
	if(file_exists($template_ini)){
		$template_config = parse_ini_file($template_ini);

	}*/
	$template_config = $template_class_ins->load_config($item['nns_template_type'],$item['nns_template_id'],true);
	$nns_template_name = !empty($template_config['skin_config'][$item['nns_template_name']])?$template_config['skin_config'][$item['nns_template_name']].'('.$item['nns_template_name'].')':$item['nns_template_name'];
	$html_data .="<tr>";
	$html_data .="<td>".$item['nns_template_type']."</td><td>".$item['nns_template_id']."</td><td>".$nns_template_name."</td><td>".$item['nns_access_count']."</td>";
	$html_data .="</tr>";
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<title></title>
</head>

<body>
<div class="content">
<div class="content_position"<?php echo cms_get_lang('stat_tongji_bb');?> &gt; <?php echo cms_get_lang('stat_ssfwtj');?></div>
<div class="content_table formtable">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">

		<thead>
		<tr><th><?php echo cms_get_lang('mblx')?></th><th><?php echo cms_get_lang('mbgl_mbys');?></th><th><?php echo cms_get_lang('mbgl_mbmc');?></th><th><?php echo cms_get_lang('stat_visits');?></th></tr>
		</thead>
		<tbody>
<?php echo $html_data;?>
<?php
$least_num=$nncms_ui_min_list_item_count-count($data_list);
for ($i=0;$i<$least_num;$i++){
?>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
		<?php	}?>
  		</tbody>
  	</table>
</div>
</div>
</body>
</html>