<?php
/*
 * Created on 2012-6-28
 *
 */
header("Content-Type:text/html;charset=utf-8");
include("../../nncms_manager_inc.php");
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_db_path. "nns_log/nns_db_play_log_class.php";
include $nncms_db_path. "nns_common/nns_db_pager_class.php";

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;

$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"114101");

$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");

$model = new nns_db_play_log_class();

$cur_page = isset($_GET['page'])?$_GET['page']:1;
$cur_page =(int)$cur_page;

if(!empty($_COOKIE["page_max_num"])){
	$page_size = intval($_COOKIE["page_max_num"]);
}else if(isset($_GET['page_size']) && $_GET['page_size'] >0){
	$page_size = $_GET['page_size'];
}else{
	$page_size = $g_manager_list_max_num;
}

$count = $model->nns_db_play_log_count($_GET);
$url = '?';

if(isset($_GET['nns_video_name'])){
	$url .="nns_video_name=".$_GET['nns_video_name'];
}
if(isset($_GET['nns_user_id']))  $url .= "&nns_user_id=".$_GET['nns_user_id'];
if(isset($_GET['nns_product_id']))  $url .= "&nns_product_id=".$_GET['nns_product_id'];
if(isset($_GET['nns_qam_ips']))  $url .= "&nns_qam_ips=".$_GET['nns_qam_ips'];
if(isset($_GET['nns_qam_frequence']))  $url .= "&nns_qam_frequence=".$_GET['nns_qam_frequence'];
if(isset($_GET['nns_video_id']))  $url .= "&nns_video_id=".$_GET['nns_video_id'];
if(isset($_GET['nns_nmds_ips']))  $url .= "&nns_nmds_ips=".$_GET['nns_nmds_ips'];
if(isset($_GET['nns_ua_ips']))  $url .= "&nns_ua_ips=".$_GET['nns_ua_ips'];
if(isset($_GET['nns_begin_position'])) $url .= "&nns_begin_position=".$_GET['nns_begin_position'];
if(isset($_GET['nns_end_position'])) $url .= "&nns_end_position=".$_GET['nns_end_position'];
if(isset($_GET['nns_video_type'])) $url .= "&nns_video_type=".$_GET['nns_video_type'];
if(isset($_GET['nns_device_id'])) $url .= "&nns_device_id=".$_GET['nns_device_id'];
if(isset($_GET['nns_qam_area_code'])) $url .= "&nns_qam_area_code=".$_GET['nns_qam_area_code'];
$url .= "&page_size=".$page_size;

$url = str_replace('?&','?',$url);

$pager = new nns_db_pager_class($count,$page_size,$cur_page,$url);

$data_list = $model->nns_db_play_log_list($_GET,($cur_page-1)*$page_size,$page_size);
//xyl 2013-05-27 20:40:15 总播放时长
$data_list2 = $model->nns_db_play_log_time_count($_GET);

//$js_get = json_encode($_GET);
//导出excel
//$data_list1 = $model->nns_db_play_log_list_infinite($_GET);
//$export_excel = '';
//$export_excel = $model->nns_db_log_export_excel($data_list1,$data_list2,$count);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>

<link rel="stylesheet" type="text/css" href="../../js/calendar/jscal2.css"/>
<link rel="stylesheet" type="text/css" href="../../js/calendar/border-radius.css"/>
<link rel="stylesheet" type="text/css" href="../../js/calendar/win2k.css"/>
<script type="text/javascript" src="../../js/calendar/calendar.js"></script>
<script type="text/javascript" src="../../js/calendar/calendar.lang.js.php"></script>
<script  src="../../js/report_select_time_bar.js"></script>

<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/tabs.js"></script>

<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript">
$(document).ready(function(){
	$(document).keydown(function(e){

		if(e.which  ==13)
		{
			$('#form_inquery').submit();
		}
	});

});

//function export_excel(){
	//var param = '<?php echo $js_get;?>';
	//$.ajax({
		//url : "../../controls/nncms_controls_export_excel.php",
			//type:"get",
			//datatype:"xml",
			//data:{type:"get",search:param,action:"export"},
			//cache:false,
			//success:function(strRtn){
				
				//alert(strRtn);
			//}

	//});
//}
</script>
</head>

<body>
<div class="content">
	<div class="content_position"><?php echo cms_get_lang('stat_tongji_bb');?> &gt; <?php echo cms_get_lang('stat_bbrztj');?></div>
	<form id="form_inquery" action="nncms_content_play_log.php" method="get">
		<div class="content_table formtable">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td>
							<?php echo cms_get_lang('stat_product_p');?>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo cms_get_lang('id');?>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_product_id'];?>"  name="nns_product_id" style="width:150px;"/>
						</td>
						<td>
							<?php echo cms_get_lang('stat_media_name')?>:&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_video_name'];?>"  name="nns_video_name" style="width:150px;"/>
						</td>
						<td>
							<?php echo cms_get_lang('stat_media_ID')?>:&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_video_id'];?>"  name="nns_video_id"  style="width:150px;"/></td>
						<td>
							<?php echo cms_get_lang('media_bfsj')?>:<input name="nns_begin_position" id="begin_time" type="text" value="<?php echo $_GET['nns_begin_position'];?>"  style="width:120px;" />
									- <input name="nns_end_position" id="end_time" type="text" value="<?php echo $_GET['nns_end_position'];?>"  style="width:120px;"  overto="begin_time" /></td>
					</tr>
					<tr >
						<td>
							<?php echo cms_get_lang('ipqam_addr');?>:&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_qam_ips'];?>"  name="nns_qam_ips" style="width:150px;"/>
						</td>
						<td>
							<?php echo cms_get_lang('ipqam_pd')?>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_qam_frequence'];?>"  name="nns_qam_frequence" style="width:150px;"/>
						</td>
						<td>
									VSIP:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_nmds_ips'];?>"  name="nns_nmds_ips" style="width:150px;"/>
						</td>
						<td>
									<?php echo cms_get_lang('stat_fw_ip');?>:&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_ua_ips'];?>"  name="nns_ua_ips" style="width:150px;"/>
						</td>
					</tr>
					<tr >

						<td><?php echo cms_get_lang('user_id');?>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_user_id'];?>"  name="nns_user_id"  style="width:150px;"/></td>

						<td><?php echo cms_get_lang('zdgl_sbid');?>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_device_id'];?>"  name="nns_device_id" style="width:150px;"/></td>

						<td><?php echo cms_get_lang('ipqam_dqm');?>:&nbsp;&nbsp;<input type="text" value="<?php echo $_GET['nns_qam_area_code'];?>"  name="nns_qam_area_code" style="width:150px;"/></td>
						<td>&nbsp;
							<input type="submit" value="<?php echo cms_get_lang('search');?>" id="inquery" name="inquery" />
							&nbsp;&nbsp;&nbsp;&nbsp;
<?php
if (!empty($data_list2)){
	foreach ($data_list2 as $key=>$v) {
		echo cms_get_lang('total'),cms_get_lang('time_played_with_second').":".'<font color="#000000">'.$v['nns_time_length_sum'].'</font>';
	}
}
?>
	&nbsp;&nbsp;&nbsp;&nbsp;
<!--						<a href="../../controls/nncms_controls_export_excel.php<?php echo $url;?>"><?php echo cms_get_lang('export_excel');?></a>-->
	<input type="button" value="<?php echo cms_get_lang('export_excel');?>"  onClick="location.href='../../controls/nncms_controls_export_excel.php<?php echo $url;?>&op=play_log'" />
<!--<input type="button" value="<?php echo cms_get_lang('export_excel');?>" onclick="export_excel();"/>-->
						</td>
					</tr>
				</tbody>
			</table>

		 </div></form>

 <div class="content_table formtable">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<th><?php echo cms_get_lang('user_id');?></th>
				<th><?php echo cms_get_lang('stat_product');?></th>

				<th><?php echo cms_get_lang('stat_vod_name');?></th>
				<th><?php echo cms_get_lang('ipqam_dqm');?></th>
				 <th><?php echo cms_get_lang('ipqam_addr');?></th>
				<th><?php echo cms_get_lang('ipqam_pd');?></th>
				<th>VSIP</th>

				<th><?php echo cms_get_lang('stat_bf_begin_time');?></th>
				<th><?php echo cms_get_lang('stat_bf_time_s');?></th>

				<th><?php echo cms_get_lang('zdgl_sbid');?></th>

				<th><?php echo cms_get_lang('stat_fw_ip');?></th>

			</tr>
		</thead>
		<tbody>
<?php
if (!empty($data_list)){
	foreach($data_list as $k =>$item){

?>
			  <tr>
				<td><?php echo $item["nns_user_id"];?></td>
				<td><?php echo $item["nns_product_id"];?> </td>

				<td><?php echo $item["nns_video_name"];?> </td>
				<td><?php echo $item["nns_qam_area_code"];?> </td>
				<td><?php echo $item["nns_qam_ips"];?> </td>
				<td><?php echo $item["nns_qam_frequence"];?> </td>
				<td><?php echo $item["nns_nmds_ips"];?> </td>
				<td><?php echo $item["nns_begin_position"];?> </td>
				<td><?php echo $item['nns_time_length'];
		//echo (strtotime($item["nns_end_position"]) - strtotime($item["nns_begin_position"]));?> </td>
				<td><?php echo $item["nns_device_id"];?> </td>

				<td><?php echo $item["nns_ua_ips"];?> </td>

			  </tr>
<?php
}}
$least_num = $nncms_ui_min_list_item_count-count($data_list);
for ($i = 0; $i < $least_num; $i++) {
?>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
<?php	} ?>
		  </tbody>
		</table>
	</div>
	<?php echo $pager->nav();?>

</div>
</body>
</html>