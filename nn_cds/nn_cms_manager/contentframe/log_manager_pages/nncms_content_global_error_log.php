<?php header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135201");
$checkpri = null;

if (!$pri_bool) {
	header("Location: ../nncms_content_wrong.php");
	exit ;
}
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
include_once $nncms_config_path . 'nn_logic/global_error_log/global_error_log.class.php';
$dc = nl_get_dc(array (
	'db_policy' => NL_DB_WRITE,
	'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$page_info = $params = array ();
$page_info['page_num'] = isset($_GET['page']) ? $_GET['page'] : 1;
$page_info['page_size'] = isset($_GET['page_size']) ? $_GET['page_size'] : (isset($_COOKIE['page_max_num']) && $_COOKIE['page_max_num'] > 0) ? $_COOKIE['page_max_num'] : 18;
if (isset($_REQUEST['nns_name']) && strlen($_REQUEST['nns_name']) > 0)
{
	$params['like']['nns_name'] = $_REQUEST['nns_name'];
}
if (isset($_REQUEST['nns_desc']) && strlen($_REQUEST['nns_desc']) > 0)
{
	$params['like']['nns_desc'] = $_REQUEST['nns_desc'];
}
if (isset($_REQUEST['nns_model']) && strlen($_REQUEST['nns_model']) > 0)
{
	$params['where']['nns_model'] = $_REQUEST['nns_model'];
}
if (isset($_REQUEST['nns_type']) && strlen($_REQUEST['nns_type']) > 0)
{
	$params['where']['nns_type'] = $_REQUEST['nns_type'];
}
if (isset($_REQUEST['nns_cp_id']) && strlen($_REQUEST['nns_cp_id']) > 0)
{
	$params['where']['nns_cp_id'] = $_REQUEST['nns_cp_id'];
}
if (isset($_REQUEST['nns_sp_id']) && strlen($_REQUEST['nns_sp_id']) > 0)
{
	$params['where']['nns_sp_id'] = $_REQUEST['nns_sp_id'];
}
if (isset($_REQUEST['nns_action']) && strlen($_REQUEST['nns_action']) > 0)
{
	$params['where']['nns_action'] = $_REQUEST['nns_action'];
}
if (isset($_REQUEST['nns_message_id']) && strlen($_REQUEST['nns_message_id']) > 0)
{
	$params['where']['nns_message_id'] = $_REQUEST['nns_message_id'];
}
if (isset($_REQUEST['nns_video_import_id']) && strlen($_REQUEST['nns_video_import_id']) > 0)
{
	$params['where']['nns_video_import_id'] = $_REQUEST['nns_video_import_id'];
}
if (isset($_REQUEST['nns_index_import_id']) && strlen($_REQUEST['nns_index_import_id']) > 0)
{
	$params['where']['nns_index_import_id'] = $_REQUEST['nns_index_import_id'];
}
if (isset($_REQUEST['nns_media_import_id']) && strlen($_REQUEST['nns_media_import_id']) > 0)
{
	$params['where']['nns_media_import_id'] = $_REQUEST['nns_media_import_id'];
}
if (isset($_REQUEST['create_begin_time']) && strlen($_REQUEST['create_begin_time']) > 0)
{
	$params['where']['create_begin_time'] = $_REQUEST['create_begin_time'];
}
if (isset($_REQUEST['create_end_time']) && strlen($_REQUEST['create_end_time']) > 0)
{
	$params['where']['create_end_time'] = $_REQUEST['create_end_time'];
}
$url = 'nncms_content_global_error_log.php?flag_url=1';
foreach ($_REQUEST as $k => $v)
{
	if ($k != 'flag_url' && $k != 'page')
	{
		$url .= '&' . $k . '=' . $v;
	}
}
$cp_list = nl_cp::query($dc,null);
$sp_list = nl_sp::query($dc,null);
$result = nl_global_error_log::query($dc, $params, $page_info);
$pager = new nl_pager($result['page_info']['total_count'], $page_info['page_size'], $page_info['page_num'], $url);
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title></title>
	<style>
		html,body{font-size:12px;margin:0px;height:100%;}
		.mesWindow{border:#666 1px solid;background:#fff;}
		.mesWindowTop{border-bottom:#eee 1px solid;margin-left:4px;padding:3px;font-weight:bold;text-align:left;font-size:12px;}
		.mesWindowContent{margin:4px;font-size:12px;}
		.mesWindow .close{height:15px;width:28px;border:none;cursor:pointer;text-decoration:underline;background:#fff}
	</style>
	<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
	<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
	<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
	<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
	<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
	<script language="javascript" src="../../js/cms_cookie.js"></script>
	<script language="javascript" src="../../js/table.js.php"></script>
	<script language="javascript" src="../../js/checkinput.js.php"></script>
	<script language="javascript" src="../../js/rate.js"></script>
	<script language="javascript" src="../../js/image_loaded_func.js"></script>
	<script type="text/javascript" src="../../js/dtree.js"></script>
	<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
	<script language="javascript" src="../../js/cms_datepicker.js"></script>
	<script language="javascript">
		function refresh_error_log_page() {
			var num = $("#nns_list_max_num").val();
			window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
		}
		function checkhiddenBox(type) {
			BoxKey = false;
			$("input.checkhiddenInput:checked").each(function() {
				if ($(this).attr('rel') == type) {
					BoxKey = true;
					return false;
				}
			})
			return BoxKey;
		}
		$(document).ready(function() {
			$('#clear_time').click(function(){
				$('#create_begin_time').val('');
				$('#create_end_time').val('');

			});
			window.parent.now_frame_url = window.location.href;
		});

		var isIe=(document.all)?true:false;

		function mousePosition(ev)
		{
			if(ev.pageX || ev.pageY)
			{
				return {x:ev.pageX, y:ev.pageY};
			}
			return {
				x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,y:ev.clientY + document.body.scrollTop - document.body.clientTop
			};
		}
		//弹出方法
		function showMessageBox(wTitle,content,pos,wWidth)
		{
			closeWindow();
			var bWidth=parseInt(document.documentElement.scrollWidth);
			var bHeight=parseInt(document.documentElement.scrollHeight);
			if(isIe){
				setSelectState('hidden');}
			var back=document.createElement("div");
			back.id="back";
			var styleStr="top:0px;left:0px;position:absolute;background:#666;width:"+bWidth+"px;height:"+bHeight+"px;";
			styleStr+=(isIe)?"filter:alpha(opacity=0);":"opacity:0;";
			back.style.cssText=styleStr;
			document.body.appendChild(back);
			showBackground(back,50);
			var mesW=document.createElement("div");
			mesW.id="mesWindow";
			mesW.className="mesWindow";
			mesW.innerHTML="<div class='mesWindowTop'><table width='100%' height='100%'><tr><td>"+wTitle+"</td><td style='width:1px;'><input type='button' onclick='closeWindow();' title='关闭窗口' class='close' value='关闭' /></td></tr></table></div><div class='mesWindowContent' id='mesWindowContent'>"+content+"</div><div class='mesWindowBottom'></div>";
			styleStr="left:"+(((pos.x-wWidth)>0)?(pos.x-wWidth):pos.x)+"px;top:"+(pos.y-120)+"px;position:absolute;width:"+wWidth+"px;";
			mesW.style.cssText=styleStr;
			document.body.appendChild(mesW);
		}
		//让背景渐渐变暗
		function showBackground(obj,endInt)
		{
			if(isIe)
			{
				obj.filters.alpha.opacity+=1;
				if(obj.filters.alpha.opacity<endInt)
				{
					setTimeout(function(){showBackground(obj,endInt)},5);
				}
			}else{
				var al=parseFloat(obj.style.opacity);al+=0.01;
				obj.style.opacity=al;
				if(al<(endInt/100))
				{setTimeout(function(){showBackground(obj,endInt)},5);}
			}
		}
		//关闭窗口
		function closeWindow()
		{
			if(document.getElementById('back')!=null)
			{
				document.getElementById('back').parentNode.removeChild(document.getElementById('back'));
			}
			if(document.getElementById('mesWindow')!=null)
			{
				document.getElementById('mesWindow').parentNode.removeChild(document.getElementById('mesWindow'));
			}
			if(isIe){
				setSelectState('');}
		}
		function export_excel()
		{
			var nns_name = $('#nns_name').val();
			var nns_desc = $('#nns_desc').val();
			var nns_model = $('#nns_model').val();
			var nns_type = $('#nns_type').val();
			var nns_action = $('#nns_action').val();
			var nns_cp_id = $('#nns_cp_id').val();
			var nns_sp_id = $('#nns_sp_id').val();
			var msg_id = $('#nns_message_id').val();
			var nns_video_import_id = $('#nns_video_import_id').val();
			var nns_index_import_id = $('#nns_index_import_id').val();
			var nns_media_import_id = $('#nns_media_import_id').val();
			var create_begin_time = $('#create_begin_time').val();
			var create_end_time = $('#create_end_time').val();
			location.href="../../controls/nncms_controls_export_excel.php?nns_name="+nns_name+"&nns_desc="+nns_desc+"&nns_model="+nns_model+
				"&nns_type="+nns_type+"&nns_action="+nns_action+"&nns_cp_id="+nns_cp_id+"&nns_sp_id="+nns_sp_id+"&nns_message_id="+msg_id+"&nns_video_import_id="+nns_video_import_id+
				"&nns_index_import_id="+nns_index_import_id+"&nns_media_import_id="+nns_media_import_id+"&create_begin_time="+create_begin_time+"&create_end_time="+create_end_time+"&op=global_error_log";
		}
	</script>
</head>

<body>
<?php
function pub_func_get_desc_alt($item) {
	if(!empty($item))
	{
		$array['substr'] = (mb_strlen($item) > 12) ? mb_substr( $item, 0, 12, 'utf-8' ).'...' : $item;
	}
	$array['html'] =  $item;
	return $array;
}

?>
<div class="content">
	<div class="content_position">
		<?php echo cms_get_lang('xtpz'),' >  全局错误日志' ;?>
	</div>
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<form action="" method="get">
				<tbody>
				<tr>
					<td>
						&nbsp;&nbsp;名称：<input type="text" name="nns_name" id="nns_name" style="width: 150px" value="<?php  if(isset($_GET['nns_name'])) echo $_GET['nns_name'];?>" />
						&nbsp;&nbsp;&nbsp;描述：<input type="text" name="nns_desc" id="nns_desc" style="width: 150px" value="<?php  if(isset($_GET['nns_desc'])) echo $_GET['nns_desc'];?>" />
						&nbsp;&nbsp;&nbsp;日志模块：
						<select name="nns_model" id="nns_model" style="width: 120px;">
							<option value="" >全部</option>
							<option value="cdn_log" <?php if($_GET['nns_model']=='cdn_log') echo 'selected="selected"'?>>CDN模块</option>
							<option value="cms_log" <?php if($_GET['nns_model']=='cms_log') echo 'selected="selected"'?>>CMS模块</option>
							<option value="asset_import_log" <?php if($_GET['nns_model']=='asset_import_log') echo 'selected="selected"'?>>媒资注入播控模块</option>
							<option value="message_log" <?php if($_GET['nns_model']=='message_log') echo 'selected="selected"'?>>消息队列模块</option>
							<option value="sp_import_log" <?php if($_GET['nns_model']=='sp_import_log') echo 'selected="selected"'?>>运营商下发模块</option>
							<option value="distribute_log" <?php if($_GET['nns_model']=='distribute_log') echo 'selected="selected"'?>>内容分发模块</option>
							<option value="op_log" <?php if($_GET['nns_model']=='op_log') echo 'selected="selected"'?>>注入队列模块</option>
							<option value="op_queue_log" <?php if($_GET['nns_model']=='op_queue_log') echo 'selected="selected"'?>>中心注入指令模块</option>
							<option value="clip_log" <?php if($_GET['nns_model']=='clip_log') echo 'selected="selected"'?>>切片模块</option>
							<option value="unline_log" <?php if($_GET['nns_model']=='unline_log') echo 'selected="selected"'?>>上下线模块</option>
							<option value="callback_log" <?php if($_GET['nns_model']=='callback_log') echo 'selected="selected"'?>>消息反馈模块</option>
							<option <?php if($_GET['nns_model'] == '--') echo 'selected="selected"'?> value="--">其它</option>
						</select>
						&nbsp;&nbsp;&nbsp;类型：
						<select name="nns_type"  id="nns_type" style="width: 110px;">
							<option value="">全部</option>
							<option <?php if($_GET['nns_type'] == 'video') echo 'selected="selected"'?> value="video">主媒资</option>
							<option <?php if($_GET['nns_type'] == 'index') echo 'selected="selected"'?> value="index">分集</option>
							<option <?php if($_GET['nns_type'] == 'media') echo 'selected="selected"'?> value="media">片源</option>
							<option <?php if($_GET['nns_type'] == '--') echo 'selected="selected"'?> value="--">其它</option>
						</select>
						&nbsp;&nbsp;&nbsp;动作：
						<select name="nns_action"  id="nns_action" style="width: 110px;">
							<option value="">全部</option>
							<option <?php if($_GET['nns_action'] == 'add') echo 'selected="selected"'?> value="add">添加</option>
							<option <?php if($_GET['nns_action'] == 'modify') echo 'selected="selected"'?> value="modify">修改</option>
							<option <?php if($_GET['nns_action'] == 'destroy') echo 'selected="selected"'?> value="destroy">删除</option>
							<option <?php if($_GET['nns_action'] == 'online') echo 'selected="selected"'?> value="online">上线</option>
							<option <?php if($_GET['nns_action'] == 'unline') echo 'selected="selected"'?> value="unline">下线</option>
							<option <?php if($_GET['nns_action'] == 'sync_category') echo 'selected="selected"'?> value="sync_category">同步栏目</option>
							<option <?php if($_GET['nns_action'] == 'cancel_sync_category') echo 'selected="selected"'?> value="cancel_sync_category">删除栏目</option>
							<option <?php if($_GET['nns_action'] == 'sync_recom') echo 'selected="selected"'?> value="sync_recom">同步推荐</option>
							<option <?php if($_GET['nns_action'] == 'cancel_sync_recom') echo 'selected="selected"'?> value="cancel_sync_recom">取消推荐</option>
							<option <?php if($_GET['nns_action'] == 'cms_add_cp') echo 'selected="selected"'?> value="cms_add_cp">同步CP</option>
							<option <?php if($_GET['nns_action'] == 'portalms_sync_columnimage') echo 'selected="selected"'?> value="portalms_sync_columnimage">同步栏目图片</option>
							<option <?php if($_GET['nns_action'] == 'portalms_del_columnimage') echo 'selected="selected"'?> value="portalms_del_columnimage">删除栏目图片</option>
						</select>
						&nbsp;&nbsp;CP_ID：
						<select name="nns_cp_id"  id="nns_cp_id" style="width: 110px;">
							<option value="">全部</option>
							<?php foreach($cp_list['data_info'] as $val){ ?>
								<option <?php if(isset($_GET['nns_cp_id'])){if($_GET['nns_cp_id']==$val['nns_id']){echo 'selected';}} ?> value="<?php echo $val['nns_id']?>"><?php echo $val['nns_name'] ?></option>
							<?php }?>
							<option <?php if($_GET['nns_cp_id'] == '--') echo 'selected="selected"'?> value="--">其它</option>
						</select>
						&nbsp;&nbsp;SP_ID：&nbsp;&nbsp;
						<select name="nns_sp_id"  id="nns_sp_id" style="width: 110px;">
							<option value="">全部</option>
							<?php foreach($sp_list['data_info'] as $val){ ?>
							<option <?php if(isset($_GET['nns_sp_id'])){if($_GET['nns_sp_id']==$val['nns_id']){ echo 'selected';}}?> value="<?php echo $val['nns_id']?>"><?php echo $val['nns_name'] ?></option>
							<?php }?>
							<option <?php if($_GET['nns_sp_id'] == '--') echo 'selected="selected"'?> value="--">其它</option>
						</select>
						&nbsp;&nbsp;&nbsp;消息ID：&nbsp;&nbsp;<input type="text" name="nns_message_id" id="nns_message_id" style="width: 200px" value="<?php  if(isset($_GET['nns_message_id'])) echo $_GET['nns_message_id'];?>" />
						&nbsp;&nbsp;&nbsp;主媒资注入ID：&nbsp;&nbsp;<input type="text" name="nns_video_import_id" id="nns_video_import_id" style="width: 200px" value="<?php  if(isset($_GET['nns_video_import_id'])) echo $_GET['nns_video_import_id'];?>" />
						&nbsp;&nbsp;&nbsp;分集注入ID：&nbsp;&nbsp;<input type="text" name="nns_index_import_id" id="nns_index_import_id" style="width: 200px" value="<?php  if(isset($_GET['nns_index_import_id'])) echo $_GET['nns_index_import_id'];?>" />
						&nbsp;&nbsp;&nbsp;片源注入ID：&nbsp;&nbsp;<input type="text" name="nns_media_import_id" id="nns_media_import_id" style="width: 200px" value="<?php  if(isset($_GET['nns_media_import_id'])) echo $_GET['nns_media_import_id'];?>" />
						&nbsp;&nbsp;&nbsp;&nbsp;选择时间段：&nbsp;&nbsp;&nbsp;
						<input name="create_begin_time" id="create_begin_time" type="text"  value="<?php
						if (isset($_GET['create_begin_time']))
							echo $_GET['create_begin_time'];
						?>" style="width:120px;" class="datetimepicker" callback="test" />-
						<input name="create_end_time" id="create_end_time" type="text"  value="<?php
						if (isset($_GET['create_end_time']))
							echo $_GET['create_end_time'];
						?>" style="width:120px;" class="datetimepicker" callback="test" />&nbsp;&nbsp;
						<input type="button" id="clear_time" name="clear_time" value="清除时间"/>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  onclick="refresh_error_log_page();"/>
						<input type="button" class="export_excel" value="<?php echo cms_get_lang('export_excel');?>" onclick="export_excel();" />
					</td>
				</tr>
				</tbody>
			</form>
		</table>
	</div>
	<div class="content_table formtable">

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<thead>
			<tr>
				<th>序号</th>
				<th>消息ID</th>
				<th>主媒资注入ID</th>
				<th>分集注入ID</th>
				<th>片源注入ID</th>
				<th>绑定ID</th>
				<th>名称</th>
				<th>类型</th>
				<th>动作</th>
				<th>CP_ID</th>
				<th>SP_ID</th>
				<th>创建时间</th>
				<th>描述</th>
				<th>操作</th>
			</tr>
			</thead>
			<tbody>
			<?php
			#$db = nn_get_db(NL_DB_READ);
			if($result['data_info']!=null){
				$num = ($page_info['page_num'] - 1) * $page_info['page_size'];
				if(is_array($result['data_info'])){
				foreach ($result['data_info'] as $item) {
					$num++;
					?>
					<tr>
						<td><?php echo $num; ?></td>

						<td >
							<?php echo $item['nns_message_id']; ?>
						</td>
						<td >
							<?php echo $item['nns_video_import_id']; ?>
						</td>
						<td >
							<?php echo $item['nns_index_import_id']; ?>
						</td>
						<td >
							<?php echo $item['nns_media_import_id']; ?>
						</td>
						<td>
							<?php echo $item['nns_bind_id']; ?>
						</td>
						<td>
							<?php echo $item['nns_name'];?>
						</td>
						<td>
							<?php
							if($item['nns_type']=='video'){
								echo '主媒资';
							}elseif($item['nns_type']=='index'){
								echo '分集';
							}elseif($item['nns_type']=='media'){
								echo '片源';
							}
							?>
						</td>
						<td>
							<?php echo $item['nns_action']; ?>
						</td>
						<td>
							<?php echo $item['nns_cp_id'];?>
						</td>
						<td>
							<?php echo $item['nns_sp_id'];?>
						</td>
						<td>
							<?php echo $item['nns_create_time'];?>
						</td>
						<td>
							<?php $arr_desc_info= pub_func_get_desc_alt($item['nns_desc']);?>
								<a href="#none" onclick="testMessageBox(event,<?php echo $num; ?>);" style="color:#f95100"><?php echo $arr_desc_info['substr']; ?></a>
								<p id="text_content_<?php echo $num;?>" style="display:none;" ><?php echo $arr_desc_info['html']; ?></p>
						</td>
						<td>
							<?php
								if(strlen($item['nns_link_file']) >0)
								{
									?>
										<a href="../../../<?php  echo $item['nns_link_file']; ?>" target="_blank"> 查看关联文件</a>
									<?php
								}
							?>
						</td>

					</tr>
					<?php
				}
			}
			}$least_num = $g_manager_list_max_num - count($result['data_info']);
			for ($i = 0; $i < $least_num; $i++) {
				?>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp; </td>
					<td>&nbsp; </td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
	<?php echo $pager->nav();?>
</div>
</body>
<script>
	//测试弹出
	function testMessageBox(ev,num)
	{
		var objPos = mousePosition(ev);
		var content_1 = '#text_content_'+num;
		var content =$(content_1).html();
		var messContent="<div style='padding:20px 20px 20px 20px;margin:0px 20px;text-align:center'>"+content+"</div>";
		showMessageBox('错误描述',messContent,objPos,350);
	}
</script>
</html>