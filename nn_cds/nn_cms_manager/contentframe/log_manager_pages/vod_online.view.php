<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
ob_start();
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"100100");
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");


require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include($nncms_db_path . "nns_depot/nns_db_depot_class.php");
$depot_inst = new nns_db_depot_class();

if ($_GET["nns_org_id"] == "" || $_GET["nns_org_type"] == "") {
        $nns_org_id = $_SESSION["nns_org_id"];
        $nns_org_type = $_SESSION["nns_manager_type"];
} else {
        $nns_org_id = $_GET["nns_org_id"];
        $nns_org_type = $_GET["nns_org_type"];
}

$depot_array = $depot_inst->nns_db_depot_list("", $nns_org_type, $nns_org_id, 0);
if ($depot_array['ret']==0 && is_array($depot_array['data'][0]))
$depot_data = $depot_array["data"][0];


//设置每页的条数.
if (!empty($_COOKIE["page_max_num"])){
	$g_manager_list_max_num=$_COOKIE["page_max_num"];
}elseif($nns_list_max_num !=NULL){
	$g_manager_list_max_num=$nns_list_max_num;
}else{
	$g_manager_list_max_num=12;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/dtree.js"></script>
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/tabs.js"></script>
<script language="javascript" src="../../js/cms_datepicker.js"></script>
<script language="javascript">
var list_num=<?php echo $g_manager_list_max_num;?>;
$(document).ready(function(){
	$("#inquery").click(function(){
		vod_online_list.bench=0;
		vod_online_list.total=0;
		vod_online_list.__search();
	});
	
	$("#export").click(function(){
		vod_online_list.bench=0;
		vod_online_list.total=0;
		var params={};
		params.search=$('#search_value').val();
		params.category=$('#online_category').val();
		params.type=$('#online_type').val();
		params.btime=$('#begin_time').val();
		params.etime=$('#end_time').val();
		params.online=$('#state_type').val();
		params.bench=vod_online_list.bench;
		params.total=vod_online_list.total;
		
		
		$query_str='';
		for (var attr in params){
			$query_str+= '&'+attr+'='+params[attr];
		}
		
		window.location.href='vod_online.control.php?export=1'+$query_str;
		
	});
});



var vod_online_list={};
vod_online_list.bench=0;
vod_online_list.total=0;
vod_online_list.__search=function(type){
	
	if (vod_online_list.bench==0){
		$("#total_num").html('<font color="#ff0000">读取中...</font>');
		$("#online_state tbody").html('');
	}
	
	var params={};
	params.search=$('#search_value').val();
	params.category=$('#online_category').val();
	params.type=$('#online_type').val();
	params.btime=$('#begin_time').val();
	params.etime=$('#end_time').val();
	params.online=$('#state_type').val();
	params.bench=vod_online_list.bench;
	params.total=vod_online_list.total;
	params.update=$('#update').val();
	
	$.ajax("vod_online.control.php",{
		success:function(data){
			vod_online_list.build_html(data);
		},
		dataType:'json',
		type:'post',
		data:params
	});	
}

vod_online_list._unline=function($obj){
	var params={};
		var id=$obj.parent().attr("id");
		params.nns_video_id=id;
		params.unline=1;
		
		$.ajax("vod_online.control.php",{
		success:function(data){
			if (data.code==0){
				alert($obj);
				var $state=$obj.parent().parent().find("#status");
				alert($state);
				$state.html('<font color="#cccccc">未上线</font>');
				var $op_html=vod_online_list._op_html('offline');
				$obj.parent().html($op_html);
				alert('操作下线成功！');
			}else{
				alert('操作下线失败！');
			}
		},
		dataType:'json',
		type:'post',
		data:params
	});	
}

vod_online_list._lock=function($obj){
	var params={};
	var id=$obj.parent().attr("id");
		params.nns_video_id=id;
		params.lock=1;
		
		$.ajax("vod_online.control.php",{
		success:function(data){
			if (data.code==0){
				var $state=$obj.parent().parent().find("#status");
				$state.html('<font color="#ff0000">上线被锁定</font>');
				var $op_html=vod_online_list._op_html('lock');
				$obj.parent().html($op_html);
				
				alert('操作锁定成功！');
			}else{
				alert('操作锁定失败！');
			}
		},
		dataType:'json',
		type:'post',
		data:params
	});	
}

vod_online_list._unlock=function($obj){
	var params={};
	var id=$obj.parent().attr("id");
		params.nns_video_id=id;
		params.lock=-1;
		
		$.ajax("vod_online.control.php",{
		success:function(data){
			if (data.code==0){
				var $state=$obj.parent().parent().find("#status");
				$state.html('<font color="#0099cc">已上线</font>');
				var $op_html=vod_online_list._op_html('online');
				$obj.parent().html($op_html);
				
				alert('操作锁定成功！');
			}else{
				alert('操作锁定失败！');
			}
		},
		dataType:'json',
		type:'post',
		data:params
	});	
}


var total_tmp = 0;

vod_online_list.build_html=function(data){
	var count=data.data.length;
	
	
	
	var $html=$("#online_state tbody").html();
	for (var i=0;i<count;i++){
		
		if(data.data[i]['indexs'].length==0){
			continue;
		}
		
		$html+='<tr>';
		$html+='<td>'+(i+1+vod_online_list.total)+'</td>';
		$html+='<td ><a href="../vod_pages/nncms_content_vod_detail.php?nns_id='+data.data[i]['nns_id']+'" target="_blank">'+data.data[i]['nns_name']+'</a></td>';
		
		switch(data.data[i]['nns_check']){
			case 'online':
				$html+='<td  id="status"><font color="#0099cc">已上线</font></td>';
			break;
			case 'lock':
				$html+='<td  id="status"><font color="#ff0000">上线被锁定</font></td>';
			break;
			case 'offline':
				$html+='<td  id="status"><font color="#cccccc">未上线</font></td>';
				
			break;
		}
		
		
		switch(data.data[i]['type']){
			case 'index':
				$html+='<td><font color="#0099cc">缺分集</font></td>';
			break;
			case 'media':
				var reason=''; 
				if (data.data[i]['update']==0){
					reason='[尾集缺片源]';
				}
				$html+='<td><font color="#ff0000">缺片源</font>'+reason+'</td>';
			break;
			default:
				$html+='<td><font color="#cccccc">无异常</font></td>';
			break;
		}
		
		$html+='<td>'+data.data[i]['indexs']+'</td>';
		$html+='<td>'+data.data[i]['nns_create_time']+'</td>';
		
		$op_html=vod_online_list._op_html(data.data[i]['nns_check']);
		
		//$html+='<td  id="'+data.data[i]['nns_id']+'">'+$op_html+'</td>';
		$html+='</tr>';
	}
	
	vod_online_list.bench=data.bench;
	vod_online_list.total=data.total;
	
	$("#online_state tbody").html($html);
	
	$("#online_state tbody > tr").removeClass("tr_odd");
	$("#online_state tbody > tr:odd").addClass("tr_odd");
	
	window.top.resetFrameHeight();
	
	if (!data.over){
		vod_online_list.__search();
	}else{
		$("#total_num").html('<font color="#ff0000">'+data.total+'条记录</font>');
	}
}


vod_online_list._op_html=function(state){
	$op_html='';
		switch (state){
			case 'online':
				//$op_html='<a href="javascript:void(null);" onclick="vod_online_list._lock($(this))">锁定</a> <a href="javascript:vod_online_list._unline($(this))">下线</a>';
				//$op_html='<a href="javascript:void(null);" onclick="vod_online_list._lock($(this))">锁定</a> ';
			break;
			case 'lock':
				//$op_html='<a href="javascript:void(null);" onclick="vod_online_list._unlock($(this))">解锁</a> ';
				//$op_html='<a href="javascript:void(null);" onclick="vod_online_list._unline($(this))">下线</a>';
			break;
		}
		return $op_html;
}

</script>
</head>

<body>
<div class="content">
	<div class="content_position"><?php echo cms_get_lang('xtpz'),' > 点播下线情况';?></div>
		<div class="content_table formtable">
		<form id="form_inquery" action="nncms_content_opera_log.php" method="get">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="300px">影片ID/影片名称/拼音:&nbsp;&nbsp;
						<input name="search_value" id="search_value" type="text" value="<?php echo $search_value;?>" style="width:150px;" />
					</td>
					<td>资源库栏目:&nbsp;&nbsp;<select name="online_category" id="online_category">
							<option value="">--</option>
							<?php  $dom = new DOMDocument('1.0', 'utf-8');
//							var_dump($data);die;
        $dom->loadXML($depot_data['nns_category']);
        $category = $dom->getElementsByTagName('category');
        foreach ($category as $item) {
        	?>
							<option value ="<?php echo $depot_data['nns_id'].','.$item->getAttribute("id");?>" ><?php echo $item->getAttribute("name");?></option>
			<?php }?>
						</select>
					</td>	
					<td>问题原因:&nbsp;&nbsp;
						<select name="online_type" id="online_type">
							<option value="">--</option>
							<option value ="index" >缺分集</option>
							<option value ="media">缺片源</option>
						</select>
					</td>
					<td>
						
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr class="tr_odd">
					
					<td>创建时间:&nbsp;&nbsp;&nbsp;
						<input name="begin_time" id="begin_time" type="text" value="<?php echo $begin_time;?>"  style="width:80px;" class="datepicker" /> - <input name="end_time" id="end_time" type="text" value="<?php echo $end_time;?>"  style="width:80px;" class="datepicker" overto="begin_time" />
					</td>
					<td>
					</td>
					<td>查询结果：<span id="total_num"></span></td>					
					<td><input type="button" value="<?php echo cms_get_lang('search');?>" id="inquery" name="inquery" />&nbsp; <input type="button" value="<?php echo cms_get_lang('export_excel');?>"  id="export" /> </td><td></td>
				</tr>
			</table>
		</form>
		</div>

 <div class="content_table formtable" id="online_state">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<th><?php echo cms_get_lang('segnumber');?></th>
				<th>影片名</th>
				<th>状态</th>
				<th>原因</th>
				<th width="300px">分集索引</th>
				 <th>创建时间</th>
			</tr>
		</thead>
		<tbody>
			
		  </tbody>
		</table>
	</div>
	 
</div>
</body>
</html>

