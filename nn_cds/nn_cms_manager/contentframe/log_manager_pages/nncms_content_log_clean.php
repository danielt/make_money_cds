<?php
header("Content-Type:text/html; charset=UTF-8");
include("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php";
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include ($nncms_config_path. "nn_cms_config/nn_cms_config_path.php");
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";

$checkpri=new nns_db_pri_class();
if (!$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"100100")){
	Header("Location: ../nncms_content_wrong.php");
}
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
$checkpri=null;
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
require_once $nncms_db_path. "nns_log/nns_db_op_log_class.php";
$before_month=date("Y-m-d",strtotime("-1 month"));
$op_inst=new nns_db_op_log_class();
$op_count_result=$op_inst->nns_db_op_log_count();
$op_count=$op_count_result["data"][0]["num"];
$op_inst=null;
require_once $nncms_db_path. "nns_log/nns_db_play_log_class.php";
$playlist_inst=new nns_db_play_log_class();
$playlist_count=$playlist_inst->nns_db_play_log_count();
$playlist_inst=null;
require_once $nncms_db_path. "nns_log/nns_db_view_record_class.php";
$view_inst=new nns_db_view_record_class();
$view_count=$view_inst->view_record_count();
$view_inst=null;
$asset_import=pub_func_get_module_right("115");
if($asset_import){
	require_once $nncms_db_path. "nns_asset_import/nns_db_asset_import_class.php";
	$asset_inst=new nns_db_asset_import_class();
	$asset_count_result=$asset_inst->nns_db_play_media_count();
	$asset_count=$asset_count_result["data"][0]["num"];
	$asset_inst=null;
}
if($g_ad_enabled==1){
	require_once $nncms_db_path. "nns_ad/nns_ad_log_class.php";
	$ad_inst=new nns_ad_log_class();
	$ad_play_count=$ad_inst->nns_ad_log_count();
	$asset_inst=null;
}
require_once $nncms_config_path . 'mgtv_v2/models/clear_log_model.php';
$count_inst=new clear_log_model();

//日志清理统计记录条数
$op_count_result=$count_inst->count_op_log();
$op_inst=$op_count_result["data"][0]["num"];

$epg_count_result = $count_inst->count_epg_log();
$epg_inst = $epg_count_result["data"][0]["num"];

$cp_count_result = $count_inst->count_cp_log();
$cp_inst = $cp_count_result["data"][0]["num"];

$global_error_count_result = $count_inst->count_global_error_log();
$global_error_inst = $global_error_count_result["data"][0]["num"];

$count_inst=null;

//导出excel xyl 2013-07-16 15:04:49
if(isset($_GET['begin_time_opera_log'])) $begin_time_opera_log = $_GET['begin_time_opera_log'];
if(isset($_GET['end_time_opera_log'])) $end_time_opera_log = $_GET['end_time_opera_log'];

if(isset($_GET['begin_time_play_log'])) $begin_time_play_log = $_GET['begin_time_play_log'];
if(isset($_GET['end_time_play_log'])) $end_time_play_log = $_GET['end_time_play_log'];

if(isset($_GET['begin_time_ad_log'])) $begin_time_ad_log = $_GET['begin_time_ad_log'];
if(isset($_GET['end_time_ad_log'])) $end_time_ad_log = $_GET['end_time_ad_log'];

if(isset($_GET['begin_time_asset_import'])) $begin_time_asset_import = $_GET['begin_time_asset_import'];
if(isset($_GET['end_time_asset_import'])) $end_time_asset_import = $_GET['end_time_asset_import'];

if(isset($_GET['begin_time_view_record'])) $begin_time_view_record = $_GET['begin_time_view_record'];
if(isset($_GET['end_time_view_record'])) $end_time_view_record = $_GET['end_time_view_record'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="../../js/cms_datepicker.js"></script>
<script language="javascript">
function delete_record(method){
	$("#delete_form #action").val(method);
	var date=$("#"+method+"_date").val();
	$("#delete_form #before_date").val(date);
	//alert($("#delete_form")[0]);
	$("#delete_form")[0].submit();
}

//xyl 2013-07-16 15:20:12 获取开始，结束时间,method 传递的op
//$(".export_excel").each(function(k){
	//$(this).bind("click",function(){
		//var begin_time = $(".begin_time").eq(k).val();
		//var end_time = $(".end_time").eq(k).val();
		//var method = $(this).val('op');
		//location.href="../../controls/nncms_controls_export_excel.php?&begin_time="+begin_time+"&end_time="+end_time+"&op="+method;
	//});
//});
function export_excel(method,begin_time,end_time){
	var begin_time = $("#"+begin_time).val();
	var end_time   = $("#"+end_time).val();
	location.href="../../controls/nncms_controls_export_excel.php?&begin_time="+begin_time+"&end_time="+end_time+"&op="+method;
}
</script>
</head>

<body>
<div class="content">
	<div class="content_position"><?php echo cms_get_lang('xtpz'),' > ',cms_get_lang('xtgl_rzql');?></div>
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
		<!-- *****************************           操作日志             -->
		<tr> <td colspan="2" style="padding:0px;"> <div class="radiolist"> <div class="radiogroup"> <h3><?php echo cms_get_lang('action|log_rz');?></h3> </div> </div> </td> </tr>
		<tr>
			<td width="120" class="rightstyle"><?php echo cms_get_lang('dqjls');?>:</td>
			<td><font style="color:#ff0000"><?php echo $op_count;?></font></td>
		</tr>
		<tr>
			<td width="120" class="rightstyle"><?php echo cms_get_lang('log_clean_select_time');?>:</td>
			<td>
				<input name="begin_time_opera_log" id="begin_time_opera_log" class="begin_time datepicker" type="text" value="<?php echo $begin_time;?>"  style="width:80px;" /> - <input name="end_time_opera_log" id="end_time_opera_log" class="end_time datepicker" type="text" value="<?php echo $end_time;?>"  style="width:80px;" overto="begin_time" />
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" class="export_excel" value="<?php echo cms_get_lang('export_excel');?>" onclick="export_excel('opera_log','begin_time_opera_log','end_time_opera_log');" />
				&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;

				<?php echo cms_get_lang('date|search_xz');?>:
				<?php echo cms_get_lang('cleanup');?> <input name="op_date" id="op_date" type="text" value="<?php echo $before_month;?>" class="datepicker"  style="width:80px;" readonly /> <?php echo cms_get_lang('before_data');?>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" name="op_btn" value="<?php echo cms_get_lang('cleanup|data');?>" id="op_btn" onclick="delete_record('op');"/>
			</td>
		</tr>
<!-- *****************************           中心同步操作日志             -->
		<tr> <td colspan="2" style="padding:0px;"> <div class="radiolist"> <div class="radiogroup"> <h3><?php echo cms_get_lang('xtgl_zxtbzl');?></h3> </div> </div> </td> </tr>
		<tr>
			<td width="120" class="rightstyle"><?php echo cms_get_lang('dqjls');?>:</td>
			<td><font style="color:#ff0000"><?php echo $op_inst;?></font></td>
		</tr>
		<tr>
			<td width="120" class="rightstyle"><?php echo cms_get_lang('log_clean_select_time');?>:</td>
			<td>
				<input name="begin_time_play_log" id="begin_time_play_log" class="begin_time datepicker" type="text" value="<?php echo $begin_time;?>"  style="width:80px;" /> - <input name="end_time_play_log" id="end_time_play_log" class="end_time datepicker" type="text" value="<?php echo $end_time;?>"  style="width:80px;" overto="begin_time" />
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" class="export_excel" value="<?php echo cms_get_lang('export_excel');?>" onclick="export_excel('opera_log','begin_time_play_log','end_time_play_log');" />
				&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;

				<?php echo cms_get_lang('date|search_xz');?>:
				<?php echo cms_get_lang('cleanup');?> <input name="zxtb_date" id="zxtb_date" type="text" value="<?php echo $before_month;?>" class="datepicker"  style="width:80px;" readonly /> <?php echo cms_get_lang('before_data');?>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" name="zxtb_btn" value="<?php echo cms_get_lang('cleanup|data');?>" id="zxtb_btn" onclick="delete_record('zxtb');"/>
			</td>
		</tr>
<!--  *****************************         播放日志             -->


<!-- *****************************          模板访问日志             -->

<!-- *****************************       媒资注入日志             -->
<?php 
if($asset_import){
?>
		<tr> <td colspan="2" style="padding:0px;"><div class="radiolist"><div class="radiogroup"><h3><?php echo cms_get_lang('assist_mzdr|log_rz');?></h3></div></div></td> </tr>
		<tr>
			<td width="120" class="rightstyle"><?php echo cms_get_lang('dqjls');?>:</td>
			<td><font style="color:#ff0000"><?php echo $asset_count;?></font></td>
		</tr>
		<tr>
			<td width="120" class="rightstyle"><?php echo cms_get_lang('log_clean_select_time');?>:</td>
			<td>
				<input name="begin_time_asset_import" id="begin_time_asset_import" class="begin_time datepicker" type="text" value="<?php echo $begin_time;?>"  style="width:80px;" /> - <input name="end_time_asset_import" id="end_time_asset_import" class="end_time datepicker" type="text" value="<?php echo $end_time;?>"  style="width:80px;" overto="begin_time" />
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" class="export_excel" value="<?php echo cms_get_lang('export_excel');?>" onclick="export_excel('asset_import','begin_time_asset_import','end_time_asset_import');" />
				&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;

			<?php echo cms_get_lang('date|search_xz');?>:
			<?php echo cms_get_lang('cleanup');?> <input name="asset_date" id="asset_date" type="text" value="<?php echo $before_month;?>" class="datepicker"  style="width:80px;" readonly /> <?php echo cms_get_lang('before_data');?>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" name="asset_btn" value="<?php echo cms_get_lang('cleanup|data');?>" id="asset_btn" onclick="delete_record('asset');"/>
			</td>
		</tr>
<?php } ?>
<?php
	if($g_ad_enabled==1){
?>
<!--  *****************************      广告播放日志             -->
		<tr> <td colspan="2" style="padding:0px;"><div class="radiolist"><div class="radiogroup"><h3><?php echo cms_get_lang('xtgl_ggbfrz');?></h3></div></div></td> </tr>
		<tr>
			<td width="120" class="rightstyle"><?php echo cms_get_lang('dqjls');?>:</td>
			<td><font style="color:#ff0000"><?php echo $ad_play_count;?></font></td>
		</tr>
		<tr>
			<td width="120" class="rightstyle"><?php echo cms_get_lang('log_clean_select_time');?>:</td>
			<td>
				<input name="begin_time_ad_log" id="begin_time_ad_log" class="begin_time datepicker" type="text" value="<?php echo $begin_time;?>"  style="width:80px;" /> - <input name="end_time_ad_log" id="end_time_ad_log" class="end_time datepicker" type="text" value="<?php echo $end_time;?>"  style="width:80px;" overto="begin_time" />
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" class="export_excel" value="<?php echo cms_get_lang('export_excel');?>" onclick="export_excel('ad_log','begin_time_ad_log','end_time_ad_log');" />
				&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;

			<?php echo cms_get_lang('date|search_xz');?>:
			<?php echo cms_get_lang('cleanup');?> <input name="ad_date" id="ad_date" type="text" value="<?php echo $before_month;?>" class="datepicker"  style="width:80px;" readonly /> <?php echo cms_get_lang('before_data');?>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" name="ad_btn" value="<?php echo cms_get_lang('cleanup|data');?>" id="asset_btn" onclick="delete_record('ad');"/>
			</td>
		</tr>
<?php } ?>


<!--  *****************************      EPG注入日志             -->
		<tr> <td colspan="2" style="padding:0px;"><div class="radiolist"><div class="radiogroup"><h3><?php echo cms_get_lang('xtgl_epglog');?></h3></div></div></td> </tr>
		<tr>
			<td width="120" class="rightstyle"><?php echo cms_get_lang('dqjls');?>:</td>
			<td><font style="color:#ff0000"><?php echo $epg_inst;?></font></td>
		</tr>
		<tr>
			<td width="120" class="rightstyle"><?php echo cms_get_lang('date|search_xz');?>:</td>
			<td><?php echo cms_get_lang('cleanup');?> <input name="epg_date" id="epg_date" type="text" value="<?php echo $before_month;?>" class="datepicker"  style="width:80px;" readonly /> <?php echo cms_get_lang('before_data');?>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" name="epg_btn" value="<?php echo cms_get_lang('cleanup|data');?>" id="epg_btn" onclick="delete_record('epg');"/>
			</td>
		</tr>
<!--          end                    -->
		<!--  *****************************      CP注入日志             -->
		<tr> <td colspan="2" style="padding:0px;"><div class="radiolist"><div class="radiogroup"><h3><?php echo cms_get_lang('xtgl_importlog');?></h3></div></div></td> </tr>
		<tr>
			<td width="120" class="rightstyle"><?php echo cms_get_lang('dqjls');?>:</td>
			<td><font style="color:#ff0000"><?php echo $cp_inst;?></font></td>
		</tr>
		<tr>
			<td width="120" class="rightstyle"><?php echo cms_get_lang('date|search_xz');?>:</td>
			<td><?php echo cms_get_lang('cleanup');?> <input name="cp_date" id="cp_date" type="text" value="<?php echo $before_month;?>" class="datepicker"  style="width:80px;" readonly /> <?php echo cms_get_lang('before_data');?>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" name="cp_btn" value="<?php echo cms_get_lang('cleanup|data');?>" id="cp_btn" onclick="delete_record('cp');"/>
			</td>
		</tr>
<!--          end                    -->
		<!--  *****************************      全局错误注入日志             -->
		<tr> <td colspan="2" style="padding:0px;"><div class="radiolist"><div class="radiogroup"><h3><?php echo "全局错误日志";?></h3></div></div></td> </tr>
		<tr>
			<td width="120" class="rightstyle"><?php echo cms_get_lang('dqjls');?>:</td>
			<td><font style="color:#ff0000"><?php echo $global_error_inst;?></font></td>
		</tr>
		<tr>
			<td width="120" class="rightstyle"><?php echo cms_get_lang('date|search_xz');?>:</td>
			<td><?php echo cms_get_lang('cleanup');?> <input name="global_error_date" id="global_error_date" type="text" value="<?php echo $before_month;?>" class="datepicker"  style="width:80px;" readonly /> <?php echo cms_get_lang('before_data');?>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" name="global_error_btn" value="<?php echo cms_get_lang('cleanup|data');?>" id="global_error_btn" onclick="delete_record('global_error');"/>
			</td>
		</tr>
<!--          end                    -->
		</tbody>
		</table>
	</div>
	<form id="delete_form" action="nncms_content_log_clean_control.php" method="post">
		<input name="action" id="action" type="hidden" value="" />
		<input name="before_date" id="before_date" type="hidden" value="" />
	</form>
</div>
</body>
</html>