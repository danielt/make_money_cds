<?php
/*
 * Created on 2012-8-1
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include ($nncms_config_path. "nn_cms_config/nn_cms_config_path.php");
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";

$checkpri=new nns_db_pri_class();
if (!$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"100100")){
	Header("Location: nncms_content_wrong.php");
}
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
 $checkpri=null;
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

$action=$_POST["action"];
$before_date=$_POST["before_date"];
$user_id = $_SESSION['nns_mgr_id'];
//清理日志
include_once $nncms_config_path . 'mgtv_v2/models/clear_log_model.php';
$clear_result = new clear_log_model();
$result = $clear_result->clear_log($user_id,$action,$before_date);

if ($result["ret"]==0){
	echo "<script>alert('". cms_get_lang('delete|success'). "');</script>";
}else{
	echo "<script>alert('".  cms_get_lang('delete|fault'). "');</script>";
}
$db_obj=null;
//echo $str_sql;
echo "<script>self.location='nncms_content_log_clean.php';</script>";
?>