<?php
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135201");
$checkpri = null;

if (!$pri_bool) {
	header("Location: ../nncms_content_wrong.php");
	exit ;
}
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
include_once $nncms_config_path . 'nn_logic/slow_log/general_log.class.php';
//创建数据库操作类
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
//需要筛选出来，展示的status的参考值
$arr_need_show_status = array(
		'Bytes_received',
		'Bytes_sent',
		'Com_select',
		'Handler_commit',
		'Handler_read_first',
		'Handler_read_key',
		'Handler_read_rnd_next',
		'Handler_write',
		'Last_query_cost',
		'Qcache_free_memory',
		'Qcache_inserts',
		'Qcache_queries_in_cache',
		'Qcache_total_blocks',
		'Qcache_hits',
		'Questions',
		'Innodb_buffer_pool_read_requests',
		'Select_scan',
		'Innodb_rows_read',
		'Innodb_data_reads',
		'Table_locks_immediate',
		'Opened_tables',
		);
//获取需要分析的SQL
$str_need_analyse_sql = trim($_GET['sql']);
//开启mysql配置profiling
nl_general_log::set_mysql_config($dc,1,'profiling');
//设置profiling显示保存的数据量
nl_general_log::set_mysql_config($dc,0,'profiling_history_size');
nl_general_log::set_mysql_config($dc,5,'profiling_history_size');
//刷新一次MySQL的status
$arr_result_profiles = nl_general_log::mysql_flush_status($dc,'status');
//执行一次SQL
nl_general_log::execute_sql($dc,$str_need_analyse_sql);

//>>1-status参考值的结果集
//获取status状态
$arr_result_status = nl_general_log::show_mysql_parameter($dc,'status');
$arr_result_status_finally = array();
foreach($arr_result_status['data_info'] as $item_status)
{
	if(in_array($item_status['Variable_name'],$arr_need_show_status))
	{
		$arr_result_status_finally[$item_status['Variable_name']] = $item_status['Value'];
	}
}
unset($arr_result_status);


//>>2-profiling排序结果集
//获取profiling的状态
$arr_result_profiles = nl_general_log::show_mysql_parameter($dc,'profiles');
//获取耗时排序
$arr_select_consume_sort = nl_general_log::select_consumption_time_sort($dc,$arr_result_profiles['data_info'][2]['Query_ID']);
$arr_tmp_consume = array_column($arr_select_consume_sort['data_info'],'duration (summed) in sec');
$arr_select_consume_sort['data_info'][] = array('state'=>'total','duration (summed) in sec'=>array_sum($arr_tmp_consume));
//关闭MySQL的profiling配置项
nl_general_log::set_mysql_config($dc,0,'profiling');
unset($arr_tmp_consume);

//>>3-explain分析SQL语句的结果集
$arr_result_explain = nl_general_log::explain_select_sql($dc, $str_need_analyse_sql);
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title></title>
	<style>
		html,body{font-size:12px;margin:0px;height:100%;}
		.mesWindow{border:#666 1px solid;background:#fff;}
		.mesWindowTop{border-bottom:#eee 1px solid;margin-left:4px;padding:3px;font-weight:bold;text-align:left;font-size:12px;}
		.mesWindowContent{margin:4px;font-size:12px;}
		.mesWindow .close{height:15px;width:28px;border:none;cursor:pointer;text-decoration:underline;background:#fff}
	</style>
	<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
	<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
	<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
	<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
	<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
	<script language="javascript" src="../../js/cms_cookie.js"></script>
	<script language="javascript" src="../../js/table.js.php"></script>
	<script language="javascript" src="../../js/checkinput.js.php"></script>
	<script language="javascript" src="../../js/rate.js"></script>
	<script language="javascript" src="../../js/image_loaded_func.js"></script>
	<script type="text/javascript" src="../../js/dtree.js"></script>
	<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
	<script language="javascript" src="../../js/cms_datepicker.js"></script>
	<script type="text/javascript">
		function go_back()
		{
			history.go(-1);
		}
	</script>
</head>
<body>
<div class="content">
	<div class="content_position">
		<?php echo cms_get_lang('xtpz'),' >  全局查询日志' ;?>
	</div>
	<div class="content_table formtable">
		<table style="float: left" width="50%" border="0" cellspacing="0" cellpadding="0">
			<caption>耗时行为排行</caption>
			<thead>
			<tr>
				<th>state</th>
				<th>duration(summed) in sec</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($arr_select_consume_sort['data_info'] as $item_consume):?>
				<tr>
					<td><?php echo $item_consume['state']?></td>
					<td><?php echo $item_consume['duration (summed) in sec']?></td>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table>
		<table style="float: left;" width="50%" border="0" cellspacing="0" cellpadding="0">
			<caption>Status状态</caption>
			<thead>
			<tr>
				<th>state</th>
				<th>duration(summed) in sec</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($arr_result_status_finally as$item_status_key=>$item_status_value):?>
				<tr>
					<td><?php echo $item_status_key;?></td>
					<td><?php echo $item_status_value;?></td>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table>
	</div>
	<div class="content_table formtable">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<caption>Explain数据</caption>
			<thead>
			<tr>
				<th>id</th>
				<th>select_type</th>
				<th>table</th>
				<th>type</th>
				<th>possible_keys</th>
				<th>key</th>
				<th>key_len</th>
				<th>ref</th>
				<th>rows</th>
				<th>Extra</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($arr_result_explain['data_info'] as $item_explain):?>
				<tr>
					<td><?php echo $item_explain['id']  ?></td>
					<td><?php echo $item_explain['select_type']  ?></td>
					<td><?php echo $item_explain['table']  ?></td>
					<td><?php echo $item_explain['type']  ?></td>
					<td><?php echo $item_explain['possible_keys']  ?></td>
					<td><?php echo $item_explain['key']  ?></td>
					<td><?php echo $item_explain['key_len']  ?></td>
					<td><?php echo $item_explain['ref']  ?></td>
					<td><?php echo $item_explain['rows']  ?></td>
					<td><?php echo $item_explain['Extra']  ?></td>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table>
	</div>
	<div class="controlbtns">
		<div class="controlbtn back"><a href="javascript:go_back();"><?php echo cms_get_lang('back'); ?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>
</body>
</html>
