<?php
/*
 * Created on 2012-7-3
 *
 * @author xsong512
 *
 */
header("Content-Type:text/html; charset=UTF-8");
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;

$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"114101");

$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");

$data_list = array();
$tpl_type_arr=getDirFiles($nncms_config_path. "data/template");
$html_data='';

foreach($tpl_type_arr as $tpl_type){
	if(is_dir($nncms_config_path. "data/template/".$tpl_type)){
		$tpl_id_arr = getDirFiles($nncms_config_path. "data/template/". $tpl_type );
		foreach($tpl_id_arr as $tpl_id){
			if(is_dir($nncms_config_path. "data/template/".$tpl_type.'/'.$tpl_id)){

				//读取模板中文名
				$template_config = array();
				$_GET['template_cn_name'] = $_GET['nns_template_name'];//默认模板中文
				$template_ini = $nncms_config_path. "data/template/". $tpl_type.'/'.$tpl_id.'/template_config.ini';
				if(file_exists($template_ini)){
					$template_config = parse_ini_file($template_ini);

				}

				$tpl_file_arr = getDirFiles($nncms_config_path. "data/template/". $tpl_type.'/'.$tpl_id );

				$html_data .= '<tr>';
				$html_data .= '<td style="width:50px;">'.$tpl_type.'</td>';
				$html_data .= '<td style="width:50px;">'.$tpl_id.'</td><td>';
				foreach($tpl_file_arr as $tpl_file){
					if(is_file($nncms_config_path. "data/template/". $tpl_type.'/'.$tpl_id.'/'.$tpl_file)){
						$ROW_1++;
						if(false !==($pos=strpos($tpl_file,"."))){
							$ext = substr($tpl_file,$pos+1);
							if($ext=='html'){
								$template_cn_name = $tpl_file;
								$template_name  = substr($tpl_file,0,$pos);
								if(!empty($template_config[$template_name])){
									$template_cn_name = $template_config[$template_name];
								}

								$html_data .= '<a href="nncms_content_view_record_report.php?nns_template_type='.$tpl_type.'&nns_template_id='.$tpl_id.'&nns_template_name='.$template_name.'">'.$template_cn_name.'</a>&nbsp;&nbsp;&nbsp;';

							}
						}

					}
				}
				$html_data .= '</td></tr>';
			}
		}
	}
}

function getDirFiles($dir)
{
	$files = array();
	if ($handle = opendir($dir)){
		/* Because the return type could be false or other equivalent type(like 0),
		this is the correct way to loop over the directory. */
		while (false !== ($file = readdir($handle))) {
			if ($file!="." && $file!=".." && $file!=".svn"){
				$files[]=$file;
			}
		}
	}
	closedir($handle);

	return $files;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<title></title>
</head>

<body>
<div class="content">
<div class="content_position"><?php echo cms_get_lang('stat_tongji_bb')?> &gt; <?php echo cms_get_lang('stat_xz_template');?></div>
<div class="content_table formtable">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">

		<thead>
		<tr><th><?php echo cms_get_lang('mblx')?></th><th><?php echo cms_get_lang('mbgl_mbys')?></th><th><?php echo cms_get_lang('mbgl_mbmc');?></th></tr>
		</thead>
		<tbody>
<?php echo $html_data;?>
		</tbody>
	</table>
</div>
</div>
</body>
</html>