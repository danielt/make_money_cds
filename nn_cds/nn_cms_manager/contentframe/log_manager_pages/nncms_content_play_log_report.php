<?php
/*
 *
 * 播放日志统计图表
 * nncms_content_play_log_report.php
 *
 * Created on 2012-7-5
 *
 * @author xsong512
 *
 */

header("Content-Type:text/html;charset=utf-8");
include("../../nncms_manager_inc.php");

$start_time = isset($_GET['start_time'])?$_GET['start_time']:date('Y-m-d H:i',time()-24*60*60);
$end_time = isset($_GET['end_time'])?$_GET['end_time']:date('Y-m-d H:i',time());

include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//添加多语言
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}

include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_pri.php";
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"114101");
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");

if(!empty($_GET['ajax'])){
	include $nncms_db_path. "nns_log/nns_db_play_log_class.php";
	$model = new nns_db_play_log_class();
	$filter['start_time'] =  strtotime($start_time);
	$filter['end_time'] =  strtotime($end_time);
	//$filter['nns_video_name'] =  isset($_GET['nns_video_name'])?$_GET['nns_video_name']:'';

	$filter['x_size'] =  27;
	if(($filter['end_time'] - $filter['start_time'])<31*60) {
		$filter['x_size'] =  6;
	}else if(($filter['end_time'] - $filter['start_time'])<61*60){
		$filter['x_size'] =  12;
	}
	$data_list = $model->get_report_data_list($filter);

	echo $data_list;exit;

}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />

<script  src="../../js/jquery-1.6.2.min.js"></script>

<link rel="stylesheet" type="text/css" href="../../js/calendar/jscal2.css"/>
<link rel="stylesheet" type="text/css" href="../../js/calendar/border-radius.css"/>
<link rel="stylesheet" type="text/css" href="../../js/calendar/win2k.css"/>
<script type="text/javascript" src="../../js/calendar/calendar.js"></script>
<script type="text/javascript" src="../../js/calendar/calendar.lang.js.php"></script>

<script  src="../../js/table.js.php"></script>
<script  src="../../js/swfobject.js"></script>
<script  src="../../js/flash_embed.js"></script>
<script  src="../../js/report_select_time_bar.js"></script>


<script type="text/javascript">

create_linechat("access_count_report","access_count_report","100%",400,"callback_init1");

$can_set_data = false;
//默认加载回调函数
function callback_init1(){

	$can_set_data = true;
	get_data_show();
}

function get_data_show(){
	//获取时间
	var start_time =  $("#begin_time").val();
	var end_time = $("#end_time").val();

	if(start_time == "" || end_time==""){
		alert("<?php echo cms_get_lang('stat_select_time');?>");
		return false;
	}

	// $("#show_btime").html(start_time+ '&nbsp;&nbsp;&nbsp;&nbsp;至&nbsp;&nbsp;&nbsp;&nbsp;'+end_time);
	show_time_str(start_time,end_time);

	$.get("nncms_content_play_log_report.php",
	{start_time:start_time,end_time:end_time,ajax:1},
	function (data){

		if($can_set_data){
			var lineChatTable1 = document.getElementById('access_count_report');

			try{
				lineChatTable1.setLineChartData(data);
			}catch (e){
				alert(e);
			}
		}else{
			alert("flash <?php echo cms_get_lang('stat_jz_fualt');?>");
		}
	},"text"
	);
}




</script>
</head>

<body>
<div class="content">
<div class="content_position"><?php echo cms_get_lang('stat_tongji_bb');?> &gt; <?php echo cms_get_lang('stat_bftjtb');?></div>
<div class="content_table">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<form id="report_form" name="report_form"  action="nncms_content_view_record_report.php">
	<tbody>

		<tr>
		  <td style="60%"><?php echo cms_get_lang('stat_time_d');?>:&nbsp;&nbsp;<input type="text" id="begin_time" name="begin_time"
				style="width: 100px" value="<?php echo $start_time;?>"/>&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" id="end_time" name="end_time" value="<?php echo $end_time;?>"
				style="width: 100px" />
				 &nbsp;&nbsp;&nbsp; <input type="button" id="clear_time" name="clear_time" value="<?php echo cms_get_lang('stat_clear_time');?>"/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" id="search" name="search"
				value="<?php echo cms_get_lang('search');?>" /></td>
		</tr>
		<tr>
			<td><?php echo cms_get_lang('ad_fast_time_query');?>:&nbsp;&nbsp;<input type="button" id="harf_hour" name="harf_hour"
				value="<?php echo cms_get_lang('stat_recent_half_hour');?>" /> &nbsp;&nbsp;&nbsp;&nbsp;<input type="button" id="one_hour" name="one_hour"
				value="<?php echo cms_get_lang('stat_recent_hour');?>" />
			  &nbsp;&nbsp;&nbsp;&nbsp;
			  <input type="button" id="three_hour" name="three_hour"
				value="<?php echo cms_get_lang('stat_recent_three_hour');?>" /> &nbsp;&nbsp;&nbsp;&nbsp; <input type="button" id="six_hour" name="six_hour"
				value="<?php echo cms_get_lang('stat_recent_six_hour');?>" /> &nbsp;&nbsp;&nbsp;&nbsp;
			  <input type="button" id="twelve_hour" name="twelve_hour"
				value="<?php echo cms_get_lang('stat_recent_twelve_hour');?>" />			  &nbsp;&nbsp;&nbsp;&nbsp;
			  <input type="button"
				id="one_day" name="one_day" value="<?php echo cms_get_lang('stat_recent_one_day');?>" />
			  &nbsp;&nbsp;&nbsp;&nbsp;
			  <input type="button" id="three_day" name="three_day"
				value="<?php echo cms_get_lang('stat_recent_three_day');?>" /> &nbsp;&nbsp;&nbsp;&nbsp;<input
				type="button" id="seven_day" name="seven_day" value="<?php echo cms_get_lang('stat_recent_one_week');?>" />&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td><?php echo cms_get_lang('stat_surveys_time');?>:&nbsp;&nbsp;
			<b style="width: 120px" id="show_btime" name="show_btime"></b>
			</td>
		</tr>
		<tr>
		  <td>
			<div id="access_count_report"></div>			</td>
		</tr>
		<tr>
		  <td>
			<div id="flow_mbps"></div>			</td>
		</tr>
	</tbody>
	</form>
</table>

</div>
</div>
</body>
</html>