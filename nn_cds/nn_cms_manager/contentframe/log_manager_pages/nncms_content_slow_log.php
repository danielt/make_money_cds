<?php header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135201");
$checkpri = null;

if (!$pri_bool) {
	header("Location: ../nncms_content_wrong.php");
	exit ;
}
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
include_once $nncms_config_path . 'nn_logic/slow_log/slow_log.class.php';
if(defined('g_db_name') && strlen(g_db_name) >1 && (!isset($_REQUEST['db']) || strlen($_REQUEST['db']) <1))
{
    $_REQUEST['db'] = g_db_name;
}
$dc = nl_get_dc(array (
	'db_policy' => NL_DB_WRITE,
	'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'op')
{
    $op_flag= $_REQUEST['op_flag'] == 0 ? 'ON' : 'OFF';
    $result = nl_slow_log::opration_log($dc,$op_flag);
}
if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'clear')
{
    nl_slow_log::del_date($dc);
}
$result_state=nl_slow_log::show_state($dc);
$state_time = $state_flag = 0;
$state_url = '';
if(is_array($result_state['data_info']) && is_array($result_state['data_info']) && !empty($result_state['data_info']))
{
    $result_state = $result_state['data_info'];
    foreach ($result_state as $val)
    {
        if(($val['Variable_name'] == 'log_slow_queries' && (strtoupper($val['Value']) == 'ON' || $val['Value'] == '1')) || ($val['Variable_name'] == 'slow_query_log' && (strtoupper($val['Value']) == 'ON' || $val['Value'] == '1')))
        {
            $state_flag=1;
        }
        if($val['Variable_name'] == 'slow_query_log_file')
        {
            $state_url = $val['Value'];
        }
        if($val['Variable_name'] == 'slow_launch_time')
        {
            $state_time = ($val['Value']>0) ? $val['Value'] : 0;
        }
    }
}

$result_database = nl_slow_log::query_exsist_database($dc);

$page_info = $params = array ();
$page_info['page_num'] = isset($_GET['page']) ? $_GET['page'] : 1;
$page_info['page_size'] = isset($_GET['page_size']) ? $_GET['page_size'] : (isset($_COOKIE['page_max_num']) && $_COOKIE['page_max_num'] > 0) ? $_COOKIE['page_max_num'] : 18;
if (isset($_REQUEST['db']) && strlen($_REQUEST['db']) > 0)
{
	$params['where']['db'] = $_REQUEST['db'];
}
if (isset($_REQUEST['query_time']) && strlen($_REQUEST['query_time']) > 0 && $_REQUEST['query_time'] >0)
{
    $_REQUEST['time_op'] = (isset($_REQUEST['time_op']) && strlen($_REQUEST['time_op']) >0) ? $_REQUEST['time_op'] : 'u';
    $str_fu = '>';
    switch ($_REQUEST['time_op'])
    {
        case 'uq':
            $str_fu = '>=';
            break;
        case 'd':
            $str_fu = '<';
            break;
        case 'dq':
            $str_fu = '<=';
            break;
        case 'q':
            $str_fu = '=';
            break;
        default:
            $_REQUEST['time_op'] = 'u';
    }
    $params['other'][] = " SEC_TO_TIME(query_time) {$str_fu} '{$_REQUEST['query_time']}' ";
}
if (isset($_REQUEST['sql_text']) && strlen($_REQUEST['sql_text']) > 0)
{
	$params['like']['sql_text'] = $_REQUEST['sql_text'];
}
if (isset($_REQUEST['create_begin_time']) && strlen($_REQUEST['create_begin_time']) > 0)
{
	$params['where']['create_begin_time'] = $_REQUEST['create_begin_time'];
}
if (isset($_REQUEST['create_end_time']) && strlen($_REQUEST['create_end_time']) > 0)
{
	$params['where']['create_end_time'] = $_REQUEST['create_end_time'];
}
if (isset($_REQUEST['o_query_time']) && strlen($_REQUEST['o_query_time']) > 0)
{
    $params['order'][] = " query_time {$_REQUEST['o_query_time']} ";
}
if (isset($_REQUEST['rows_examined']) && strlen($_REQUEST['rows_examined']) > 0)
{
    $params['order'][] = " rows_examined {$_REQUEST['rows_examined']} ";
}
$url = 'nncms_content_slow_log.php?flag_url=1';
foreach ($_REQUEST as $k => $v)
{
	if ($k != 'flag_url' && $k != 'page')
	{
		$url .= '&' . $k . '=' . $v;
	}
}
$result = nl_slow_log::query($dc, $params, $page_info);
$pager = new nl_pager($result['page_info']['total_count'], $page_info['page_size'], $page_info['page_num'], $url);
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title></title>
	<style>
		html,body{font-size:12px;margin:0px;height:100%;}
		.mesWindow{border:#666 1px solid;background:#fff;}
		.mesWindowTop{border-bottom:#eee 1px solid;margin-left:4px;padding:3px;font-weight:bold;text-align:left;font-size:12px;}
		.mesWindowContent{margin:4px;font-size:12px;}
		.mesWindow .close{height:15px;width:28px;border:none;cursor:pointer;text-decoration:underline;background:#fff}
	</style>
	<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
	<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
	<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
	<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
	<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
	<script language="javascript" src="../../js/cms_cookie.js"></script>
	<script language="javascript" src="../../js/table.js.php"></script>
	<script language="javascript" src="../../js/checkinput.js.php"></script>
	<script language="javascript" src="../../js/rate.js"></script>
	<script language="javascript" src="../../js/image_loaded_func.js"></script>
	<script type="text/javascript" src="../../js/dtree.js"></script>
	<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
	<script language="javascript" src="../../js/cms_datepicker.js"></script>
	<script language="javascript">
		function refresh_error_log_page() {
			var num = $("#nns_list_max_num").val();
			window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
		}

		function clear_log_page() {
			window.location.href='nncms_content_slow_log.php?action=clear';
		}

		function op_log(op_flag) {
			window.location.href='nncms_content_slow_log.php?action=op&op_flag='+op_flag;
		}
		
		function checkhiddenBox(type) {
			BoxKey = false;
			$("input.checkhiddenInput:checked").each(function() {
				if ($(this).attr('rel') == type) {
					BoxKey = true;
					return false;
				}
			})
			return BoxKey;
		}
		$(document).ready(function() {
			$('#clear_time').click(function(){
				$('#create_begin_time').val('');
				$('#create_end_time').val('');

			});
			window.parent.now_frame_url = window.location.href;
		});

		var isIe=(document.all)?true:false;

		function mousePosition(ev)
		{
			if(ev.pageX || ev.pageY)
			{
				return {x:ev.pageX, y:ev.pageY};
			}
			return {
				x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,y:ev.clientY + document.body.scrollTop - document.body.clientTop
			};
		}
		//弹出方法
		function showMessageBox(wTitle,content,pos,wWidth)
		{
			closeWindow();
			var bWidth=parseInt(document.documentElement.scrollWidth);
			var bHeight=parseInt(document.documentElement.scrollHeight);
			if(isIe){
				setSelectState('hidden');}
			var back=document.createElement("div");
			back.id="back";
			var styleStr="top:0px;left:0px;position:absolute;background:#666;width:"+bWidth+"px;height:"+bHeight+"px;";
			styleStr+=(isIe)?"filter:alpha(opacity=0);":"opacity:0;";
			back.style.cssText=styleStr;
			document.body.appendChild(back);
			showBackground(back,50);
			var mesW=document.createElement("div");
			mesW.id="mesWindow";
			mesW.className="mesWindow";
			mesW.innerHTML="<div class='mesWindowTop'><table width='100%' height='100%'><tr><td>"+wTitle+"</td><td style='width:1px;'><input type='button' onclick='closeWindow();' title='关闭窗口' class='close' value='关闭' /></td></tr></table></div><div class='mesWindowContent' id='mesWindowContent'>"+content+"</div><div class='mesWindowBottom'></div>";
			styleStr="left:"+(((pos.x-wWidth)>0)?(pos.x-wWidth):pos.x)+"px;top:"+(pos.y-100)+"px;position:absolute;width:"+wWidth+"px;";
			mesW.style.cssText=styleStr;
			document.body.appendChild(mesW);
		}
		//让背景渐渐变暗
		function showBackground(obj,endInt)
		{
			if(isIe)
			{
				obj.filters.alpha.opacity+=1;
				if(obj.filters.alpha.opacity<endInt)
				{
					setTimeout(function(){showBackground(obj,endInt)},5);
				}
			}else{
				var al=parseFloat(obj.style.opacity);al+=0.01;
				obj.style.opacity=al;
				if(al<(endInt/100))
				{setTimeout(function(){showBackground(obj,endInt)},5);}
			}
		}
		//关闭窗口
		function closeWindow()
		{
			if(document.getElementById('back')!=null)
			{
				document.getElementById('back').parentNode.removeChild(document.getElementById('back'));
			}
			if(document.getElementById('mesWindow')!=null)
			{
				document.getElementById('mesWindow').parentNode.removeChild(document.getElementById('mesWindow'));
			}
			if(isIe){
				setSelectState('');}
		}
		function export_excel()
		{
			var nns_name = $('#nns_name').val();
			var nns_desc = $('#nns_desc').val();
			var nns_model = $('#nns_model').val();
			var nns_type = $('#nns_type').val();
			var nns_action = $('#nns_action').val();
			var nns_cp_id = $('#nns_cp_id').val();
			var nns_sp_id = $('#nns_sp_id').val();
			var msg_id = $('#nns_message_id').val();
			var nns_video_import_id = $('#nns_video_import_id').val();
			var nns_index_import_id = $('#nns_index_import_id').val();
			var nns_media_import_id = $('#nns_media_import_id').val();
			var create_begin_time = $('#create_begin_time').val();
			var create_end_time = $('#create_end_time').val();
			location.href="../../controls/nncms_controls_export_excel.php?nns_name="+nns_name+"&nns_desc="+nns_desc+"&nns_model="+nns_model+
				"&nns_type="+nns_type+"&nns_action="+nns_action+"&nns_cp_id="+nns_cp_id+"&nns_sp_id="+nns_sp_id+"&nns_message_id="+msg_id+"&nns_video_import_id="+nns_video_import_id+
				"&nns_index_import_id="+nns_index_import_id+"&nns_media_import_id="+nns_media_import_id+"&create_begin_time="+create_begin_time+"&create_end_time="+create_end_time+"&op=global_error_log";
		}
	</script>
</head>

<body>
<?php
function pub_func_get_desc_alt($item) {
	if(!empty($item))
	{
		$array['substr'] = (mb_strlen($item) > 200) ? mb_substr( $item, 0, 200, 'utf-8' ).'...' : $item;
	}
	$array['html'] =  $item;
	return $array;
}

?>
<div class="content">
	<div class="content_position">
		<?php echo cms_get_lang('xtpz'),' >  慢查询日志' ;?>
	</div>
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<form action="" method="get">
				<tbody>
				<tr>
					<td>
						&nbsp;&nbsp;SQL：<input type="text" name="sql_text" id="sql_text" style="width: 150px" value="<?php  if(isset($_GET['sql_text'])) echo $_GET['sql_text'];?>" />
						&nbsp;&nbsp;DATABASE：
						<select name="db"  id="db" style="width: 110px;">
							<option value="">全部</option>
							<?php if(isset($result_database['data_info']) && is_array($result_database['data_info'])){foreach($result_database['data_info'] as $val){ ?>
								<option <?php if(isset($_REQUEST['db'])){if($_REQUEST['db']==$val){echo 'selected';}} ?> value="<?php echo $val;?>"><?php echo $val ?></option>
							<?php }}?>
						</select>
						&nbsp;&nbsp;消费时间:
						<select name="time_op"  id="time_op" style="width: 60px;">
							<option <?php if(isset($_REQUEST['time_op'])){if($_REQUEST['time_op']=='u'){echo 'selected';}} ?> value="u">大于</option>
							<option <?php if(isset($_REQUEST['time_op'])){if($_REQUEST['time_op']=='uq'){echo 'selected';}} ?> value="uq">大于等于</option>
							<option <?php if(isset($_REQUEST['time_op'])){if($_REQUEST['time_op']=='d'){echo 'selected';}} ?> value="d">小于</option>
							<option <?php if(isset($_REQUEST['time_op'])){if($_REQUEST['time_op']=='dq'){echo 'selected';}} ?> value="dq">小于等于</option>
							<option <?php if(isset($_REQUEST['time_op'])){if($_REQUEST['time_op']=='q'){echo 'selected';}} ?> value="q">等于</option>
						</select>
						
						<input type="text" name="query_time" id="query_time" style="width: 60px" value="<?php  if(isset($_GET['query_time'])) echo $_GET['query_time'];?>" />
						&nbsp;&nbsp;命中行数排序:
						<input <?php if(isset($_REQUEST['rows_examined'])){if($_REQUEST['rows_examined'] !='desc' || $_REQUEST['rows_examined'] !='asc'){echo 'checked';}}else{echo 'checked';} ?>  name='rows_examined' id="rows_examined" type="radio" value=''>默认
						<input <?php if(isset($_REQUEST['rows_examined'])){if($_REQUEST['rows_examined']=='desc'){echo 'checked';}} ?>  name='rows_examined' id="rows_examined" type="radio" value='desc'>降序
						<input <?php if(isset($_REQUEST['rows_examined'])){if($_REQUEST['rows_examined']=='asc'){echo 'checked';}} ?> name='rows_examined' id="rows_examined" type="radio" value='asc'>升序
						
						&nbsp;&nbsp;消费时间排序:
						<input <?php if(isset($_REQUEST['o_query_time'])){if($_REQUEST['o_query_time'] !='desc' || $_REQUEST['o_query_time'] !='asc'){echo 'checked';}}else{echo 'checked';} ?>  name='o_query_time' id="o_query_time" type="radio" value=''>默认
						<input <?php if(isset($_REQUEST['o_query_time'])){if($_REQUEST['o_query_time']=='desc'){echo 'checked';}} ?>  name='o_query_time' id="o_query_time" type="radio" value='desc'>降序
						<input <?php if(isset($_REQUEST['o_query_time'])){if($_REQUEST['o_query_time']=='asc'){echo 'checked';}} ?> name='o_query_time' id="o_query_time" type="radio" value='asc'>升序
						<br/>
						&nbsp;&nbsp;&nbsp;&nbsp;选择时间段：&nbsp;&nbsp;&nbsp;
						<input name="create_begin_time" id="create_begin_time" type="text"  value="<?php
						if (isset($_REQUEST['create_begin_time']))
							echo $_REQUEST['create_begin_time'];
						?>" style="width:120px;" class="datetimepicker" callback="test" />-
						<input name="create_end_time" id="create_end_time" type="text"  value="<?php
						if (isset($_REQUEST['create_end_time']))
							echo $_REQUEST['create_end_time'];
						?>" style="width:120px;" class="datetimepicker" callback="test" />&nbsp;&nbsp;
						<input type="button" id="clear_time" name="clear_time" value="清除时间"/>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  onclick="refresh_error_log_page();"/>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="button" value="清空数据"  onclick="clear_log_page();"/>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="button" value="<?php if($state_flag == 0){ echo "开启";}else{ echo "关闭";}?>日志"  onclick="op_log(<?php echo $state_flag;?>);"/>
						&nbsp;&nbsp;&nbsp;&nbsp;
						存储地址:[<?php echo $state_url;?>]
						&nbsp;&nbsp;&nbsp;&nbsp;
						慢查询时间:[<?php echo $state_time."S";?>]
						
						<!--  <input type="button" class="export_excel" value="<?php echo cms_get_lang('export_excel');?>" onclick="export_excel();" />  -->
					</td>
				</tr>
				</tbody>
			</form>
		</table>
	</div>
	<div class="content_table formtable">

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<thead>
			<tr>
				<th>序号</th>
				<th>查询时间</th>
				<th>用户</th>
				<th>消费时间</th>
				<th>锁定时间</th>
				<th>输出行数</th>
				<th>命中行数</th>
				<th>最后插入ID</th>
				<th>插入ID</th>
				<th>服务ID</th>
				<th>SQL</th>
			</tr>
			</thead>
			<tbody>
			<?php
			#$db = nn_get_db(NL_DB_READ);
			if($result['data_info']!=null){
				$num = ($page_info['page_num'] - 1) * $page_info['page_size'];
				if(is_array($result['data_info'])){
				foreach ($result['data_info'] as $item) {
					$num++;
					?>
					<tr>
						<td><?php echo $num; ?></td>

						<td >
							<?php echo $item['start_time']; ?>
						</td>
						<td >
							<?php echo $item['user_host']; ?>
						</td>
						<td >
							<?php echo $item['s_query_time']; ?>
						</td>
						<td >
							<?php echo $item['s_lock_time']; ?>
						</td>
						<td>
							<?php echo $item['rows_sent']; ?>
						</td>
						<td>
							<?php echo $item['rows_examined'];?>
						</td>
						
                        <td>
							<?php echo $item['last_insert_id'];?>
						</td>
						<td>
							<?php echo $item['insert_id'];?>
						</td>
						<td>
							<?php echo $item['server_id'];?>
						</td>
						<td>
						  <?php $arr_desc_info= pub_func_get_desc_alt($item['sql_text']);?>
								<a href="#none" onclick="testMessageBox(event,<?php echo $num; ?>);" style="color:#f95100"><?php echo $arr_desc_info['substr']; ?></a>
								<p id="text_content_<?php echo $num;?>" style="display:none;" ><?php echo $arr_desc_info['html']; ?></p>
						</td>
					</tr>
					<?php
				}
			}
			}$least_num = $g_manager_list_max_num - count($result['data_info']);
			for ($i = 0; $i < $least_num; $i++) {
				?>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp; </td>
					<td>&nbsp; </td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
	<?php echo $pager->nav();?>
</div>
</body>
<script>
	//测试弹出
	function testMessageBox(ev,num)
	{
		var objPos = mousePosition(ev);
		var content_1 = '#text_content_'+num;
		var content =$(content_1).html();
		var messContent="<div style='padding:20px 20px 20px 20px;margin:0px 20px;text-align:center'>"+content+"</div>";
		showMessageBox('SQL描述',messContent,objPos,350);
	}
</script>
</html>