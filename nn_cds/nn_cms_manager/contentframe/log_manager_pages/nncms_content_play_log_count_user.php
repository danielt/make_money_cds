<?php
/*
 * @file_name	: 用户播放统计 
 * @createtime	:2013-06-04 11:54:17
 * @modifytime	:2013-06-04 11:54:47
 */
header("Content-Type:text/html;charset=utf-8");
include("../../nncms_manager_inc.php");
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_db_path. "nns_log/nns_db_play_log_class.php";
include $nncms_db_path. "nns_common/nns_db_pager_class.php";

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;

$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"114101");

$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");

$model = new nns_db_play_log_class();

$cur_page = isset($_GET['page'])?$_GET['page']:1;
$cur_page =(int)$cur_page;

if(!empty($_COOKIE["page_max_num"])){
	$page_size = intval($_COOKIE["page_max_num"]);
}else if(isset($_GET['page_size']) && $_GET['page_size'] >0){
	$page_size = $_GET['page_size'];
}else{
	$page_size = $g_manager_list_max_num;
}

$url = '?';
//if(isset($_GET['log_year']) && isset($_GET['log_month'])){
	//$nns_date = $_GET['log_year'].'-'.$_GET['log_month'];
	//$url .= "&nns_date=".$nns_date;
//}
if(isset($_GET['log_year'])) $url .= "&log_year=".$_GET['log_year'];
if(isset($_GET['log_month'])) $url .= "&log_month=".$_GET['log_month'];
if(isset($_GET['nns_count_order'])) $url .= "&nns_count_order=".$_GET['nns_count_order'];
if(isset($_GET['nns_count_order'])) $url .= "&nns_count_order=".$_GET['nns_count_order'];
if(isset($_GET['nns_time_length_order'])) $url .= "&nns_time_length_order=".$_GET['nns_time_length_order'];
$url .= "&page_size=".$page_size;

$url = str_replace('?&','?',$url);

$count = $model->nns_db_play_log_user_count($_GET);
$pager = new nns_db_pager_class($count,$page_size,$cur_page,$url);

$data_list = $model->nns_db_play_log_user_list($_GET,($cur_page-1)*$page_size,$page_size);
$data_list2 = $model->nns_db_play_log_time_count($_GET);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/tabs.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript">
$(document).ready(function(){
	$(document).keydown(function(e){

		if(e.which  ==13)
		{
			$('#form_inquery').submit();
		}
	});

});
</script>
</head>

<body>
<div class="content">
	<div class="content_position"><?php echo cms_get_lang('stat_tongji_bb');?> &gt; <?php echo cms_get_lang('stat_yfbftj');?></div>
	<form id="form_inquery" action="nncms_content_play_log_count_user.php" method="get">
		<div class="content_table formtable">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td style="width:160px;"><?php echo cms_get_lang('time');?>:&nbsp;&nbsp;&nbsp;&nbsp;
							<select id="log_year" name="log_year" style="width:80px;">
							<?php
							//随着系统年份增加输出年份
							$year = date('Y')+1;
							if ($year < 2013) {
								$year = 2013;
							}
							for ($i = 2012; $i < $year; $i++) {
								//if (!empty($_GET['log_year'])) {
									//echo "<option value='".$_GET['log_year']."' class='log_year".$_GET['log_year']."' >".$_GET['log_year']."</option>";
								//}else {
									//拼合选项
									$option_year = "<option value='";
									$option_year .=$i."' class='log_year".$i."'";
									if ($_GET['log_year'] && $_GET['log_year']==$i) {
										$option_year.=" selected";
									}
									$option_year .= " >".$i."</option>";
									echo $option_year;
									//echo "<option value='".$i."' class='log_year".$i."' >".$i."</option>";
								//}
							}
							?>
							</select>&nbsp;&nbsp;<?php echo cms_get_lang('year');?>
						</td>
						<td style="width:130px;">
							<select name="log_month" id="log_month" style="width:80px;">
							<?php
							for ($i = 1; $i < 13; $i++) {
								if ($i<10) {
									$i = '0'.$i;
								}
								//拼合选项
								$option_month = "<option value='";
								$option_month .=$i."' class='log_month".$i."'";
								if ($_GET['log_month'] && $_GET['log_month']==$i) {
									$option_month.=" selected";
								}
								$option_month .= " >".$i."</option>";
								echo $option_month;
								//echo "<option value='".$i."' class='log_month".$i."' >".$i."</option>";
							}
							?>
							</select>
							</select>&nbsp;&nbsp;<?php echo cms_get_lang('month');?>
						</td>
						<td>
							<input type="submit" value="<?php echo cms_get_lang('search');?>" id="inquery" name="inquery" />
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="button" value="<?php echo cms_get_lang('export_excel');?>"  onClick="location.href='../../controls/nncms_controls_export_excel.php<?php echo $url;?>&op=play_log_count_user'" />
						</td>
					</tr>
				</tbody>
			</table>

		 </div></form>

 <div class="content_table formtable">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<th><?php echo cms_get_lang('segnumber');?></th>
				<th><?php echo cms_get_lang('user_id');?></th>
				<th><?php echo cms_get_lang('stat_bf_time_s');?></th>
				<th><?php echo cms_get_lang('stat_play_times_sum');?></th>
			</tr>
		</thead>
		<tbody>
<?php
if (!empty($data_list)){
	$num=($cur_page-1)*$g_manager_list_max_num;
	foreach($data_list as $k =>$item){
		$num++;
?>
			<tr>
				<td><?php echo $num;?></td>
				<td><?php echo $item["nns_user_id"];?></td>
				<td><?php echo $item['nns_time_length'];?></td>
				<td><?php echo $item["nns_count"];?> </td>
			</tr>
<?php
}}
$least_num = $nncms_ui_min_list_item_count-count($data_list);
for ($i = 0; $i < $least_num; $i++) {
?>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
<?php	} ?>
		</tbody>
		</table>
	</div>
	<?php echo $pager->nav();?>

</div>
</body>
</html>