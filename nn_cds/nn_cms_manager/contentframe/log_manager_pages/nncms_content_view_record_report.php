<?php
/*
 * Created on 2012-6-30
 *
 * 模板访问统计图表
 *
 */
header("Content-Type:text/html;charset=utf-8");
include("../../nncms_manager_inc.php");
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

//print_r($_GET);
if(empty($_GET['nns_template_type']) && empty($_GET['nns_template_id']) && empty($_GET['nns_template_name'])){
	if(empty($_GET['ajax'])){
		header("Location:nncms_content_template_select.php");
	}else{
		echo cms_get_lang('templat_is_null');exit;
	}

}
$_GET['template_cn_name'] = $_GET['nns_template_name'];//默认模板中文
$template_ini = $nncms_config_path. "data/template/". $_GET['nns_template_type'].'/'.$_GET['nns_template_id'].'/template_config.ini';
if(file_exists($template_ini)){
	$template_config = parse_ini_file($template_ini);
	if(!empty($template_config[$_GET['nns_template_name']]))
		$_GET['template_cn_name'] = $template_config[$_GET['nns_template_name']];
}
$start_time = isset($_GET['start_time'])?$_GET['start_time']:date('Y-m-d H:i',time()-24*60*60);
$end_time = isset($_GET['end_time'])?$_GET['end_time']:date('Y-m-d H:i',time());

include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}

include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_pri.php";
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"114101");
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");

include $nncms_db_path. "nns_log/nns_db_access_count_class.php";
$model = new nns_db_access_count_class();
$filter['start_time'] =  strtotime($start_time);
$filter['end_time'] =  strtotime($end_time);
$filter['nns_template_type'] =  isset($_GET['nns_template_type'])?$_GET['nns_template_type']:'';
$filter['nns_template_id'] =  isset($_GET['nns_template_id'])?$_GET['nns_template_id']:'';
$filter['nns_template_name'] = isset($_GET['nns_template_name'])?$_GET['nns_template_name']:'' ;
$filter['x_size'] =  27;
$data_list = $model->get_report_data_list($filter);
if(!empty($_GET['ajax'])){
	echo $data_list;exit;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script  src="../../js/jquery-1.6.2.min.js"></script>

<link rel="stylesheet" type="text/css" href="../../js/calendar/jscal2.css"/>
<link rel="stylesheet" type="text/css" href="../../js/calendar/border-radius.css"/>
<link rel="stylesheet" type="text/css" href="../../js/calendar/win2k.css"/>
<script type="text/javascript" src="../../js/calendar/calendar.js"></script>
<script type="text/javascript" src="../../js/calendar/calendar.lang.js.php"></script>
<script  src="../../js/table.js.php"></script>
<script  src="../../js/swfobject.js"></script>
<script  src="../../js/flash_embed.js"></script>
<script  src="../../js/report_select_time_bar.js"></script>
<script type="text/javascript">
//
create_linechat("access_count_report","access_count_report","100%",400,"callback_init1");

$can_set_data = false;
//默认加载回调函数
function callback_init1(){

	$can_set_data = true;
	//
	var lineChatTable1 = document.getElementById('access_count_report');

	try{
		lineChatTable1.setLineChartData('<?php echo $data_list;?>');
	}catch (e){
		alert(e);
	}
}

function get_data_show(){
	var nns_template_type = $('#nns_template_type').val();
	var nns_template_id = $('#nns_template_id').val();
	var nns_template_name = $('#nns_template_name').val();
	//获取时间
	var start_time =  $("#begin_time").val();
	var end_time = $("#end_time").val();

	if(start_time == "" || end_time==""){
		alert("<?php echo cms_get_lang('stat_select_time');?>");
		return false;
	}

	// $("#show_btime").html(start_time+ '&nbsp;&nbsp;&nbsp;&nbsp;至&nbsp;&nbsp;&nbsp;&nbsp;'+end_time);
	show_time_str(start_time,end_time);

	$.get("nncms_content_view_record_report.php",
	{nns_template_type:nns_template_type,nns_template_id:nns_template_id,nns_template_name:nns_template_name,start_time:start_time,end_time:end_time,ajax:1},
	function (data){

		if($can_set_data){
			var lineChatTable1 = document.getElementById('access_count_report');

			try{
				lineChatTable1.setLineChartData(data);
			}catch (e){
				alert(e);
			}
		}else{
			alert("flash <?php echo cms_get_lang('stat_jz_fualt');?>");
		}
	},"text"
	);
}


</script>
</head>

<body>
<div class="content">
<div class="content_position"><?php echo cms_get_lang('stat_tongji_bb');?> &gt; <?php echo cms_get_lang('stat_template_tb');?></div>
<div class="content_table">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<form id="report_form" name="report_form"  action="nncms_content_view_record_report.php">
	<tbody>
		<tr>
			<td><?php echo cms_get_lang('mbgl_mblx');?>:&nbsp;&nbsp;
			<input type="text" value="<?php echo $_GET['nns_template_type'];?>"  name="nns_template_type" id="nns_template_type" readonly="redonly" style="width:150px;"/>
			</td>
			<td>&nbsp;<?php echo cms_get_lang('mbgl_mbys');?>:&nbsp;
			&nbsp;
			<input type="text" value="<?php echo $_GET['nns_template_id'];?>"  name="nns_template_id" id="nns_template_id" readonly="redonly" style="width:150px;"/></td>
			<td><?php echo cms_get_lang('mbgl_mbmc');?>:&nbsp;&nbsp;
			<input type="text" value="<?php echo $_GET['template_cn_name'];?>" width="20px" name="template_cn_name" id="template_cn_name" readonly="redonly" style="width:150px;"/>
			<input type="hidden" value="<?php echo $_GET['nns_template_name'];?>" width="20px" name="nns_template_name" id="nns_template_name"/>
			</td>
		</tr>
		<tr>
		  <td><?php echo cms_get_lang('stat_time_d');?>:&nbsp;&nbsp;<input type="text" id="begin_time" name="begin_time"
				style="width: 100px" value="<?php echo $start_time;?>"/>
		 &nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" id="end_time" name="begin_time" value="<?php echo $end_time;?>"
				style="width: 100px" />&nbsp;&nbsp;&nbsp; <input type="button" id="clear_time" name="clear_time" value="<?php echo cms_get_lang('stat_clear_time');?>"/> </td>
			<td width="30%"><input type="button" id="search" name="search"
				value="<?php echo cms_get_lang('search'); ?>" /></td>
				<td></td>
		</tr>
		<tr>
			<td colspan="6"><?php echo cms_get_lang('ad_fast_time_query');?>：&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" id="harf_hour" name="harf_hour"
				value="<?php echo cms_get_lang('stat_recent_half_hour'); ?>" /> &nbsp;&nbsp;&nbsp;&nbsp;<input type="button" id="one_hour" name="one_hour"
				value="<?php echo cms_get_lang('stat_recent_hour'); ?>" />
			  &nbsp;&nbsp;&nbsp;&nbsp;
			  <input type="button" id="three_hour" name="three_hour"
				value="<?php echo cms_get_lang('stat_recent_three_hour'); ?>" /> &nbsp;&nbsp;&nbsp;&nbsp; <input type="button" id="six_hour" name="six_hour"
				value="<?php echo cms_get_lang('stat_recent_six_hour'); ?>" /> &nbsp;&nbsp;&nbsp;&nbsp;
			  <input type="button" id="twelve_hour" name="twelve_hour"
				value="<?php echo cms_get_lang('stat_recent_twelve_hour'); ?>" />			  &nbsp;&nbsp;&nbsp;&nbsp;
			  <input type="button"
				id="one_day" name="one_day" value="<?php echo cms_get_lang('stat_recent_one_day'); ?>" />
			  &nbsp;&nbsp;&nbsp;&nbsp;
			  <input type="button" id="three_day" name="three_day"
				value="<?php echo cms_get_lang('stat_recent_three_day'); ?>" /> &nbsp;&nbsp;&nbsp;&nbsp;<input
				type="button" id="seven_day" name="seven_day" value="<?php echo cms_get_lang('stat_recent_one_week'); ?>" />&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5"><?php echo cms_get_lang('stat_surveys_time'); ?>：&nbsp;&nbsp;&nbsp;&nbsp;
			<b style="width: 120px" id="show_btime" name="show_btime"></b>
			</td>
		</tr>
		<tr>
		  <td colspan="5">
			<div id="access_count_report"></div>			</td>
		</tr>
		<tr>
		  <td colspan="5">
			<div id="flow_mbps"></div>			</td>
		</tr>
	</tbody>
	</form>
</table>

</div>
</div>
</body>
</html>
