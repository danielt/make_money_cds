<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
global $g_live_info_completion_url; //直播频道第三方信息获取接口
global $g_playbill_info_completion_url; //节目单第三方信息获取接口
$action=$_GET["action"];
$nns_id=$_REQUEST["nns_id"];
$pri_bool=false;
if (!empty($action)){
	switch($action){
	case "edit":
		$action_str=cms_get_lang('save');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136002");
		break;
	case "add":
		$action_str=cms_get_lang('add');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136003");
		break;
	default:
		break;
	}
}
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/sp/sp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/transcode/transcode_load_balance.class.php';
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);
$edit_data = $_REQUEST;
$dc = nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$dc->open();

/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/transcode/transcode_manufacturer.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/transcode/transcode_load_balance.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/sp/sp.class.php';
$dc = nl_get_dc(array (
    "db_policy" => NL_DB_WRITE,
    "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));


$result_sp = nl_sp::query_all($dc);
$arr_manufacturer = $arr_sp = null;
if(isset($result_sp['data_info']) && is_array($result_sp['data_info']) && !empty($result_sp['data_info']))
{
    foreach ($result_sp['data_info'] as $sp_val)
    {
        if(!isset($sp_val['nns_config']) || strlen($sp_val['nns_config'])<1)
        {
            continue;
        }
        $arr_sp_config = json_decode($sp_val['nns_config'],true);
        #TODO
        $arr_sp[$sp_val['nns_id']] = $sp_val['nns_id'].' / '.$sp_val['nns_name'];
    }
}
$result_manufacturer = nl_transcode_manufacturer::query_data($dc,array('nns_state'=>'0'));
if(isset($result_manufacturer['data_info']) && is_array($result_manufacturer['data_info']) && !empty($result_manufacturer['data_info']))
{
    foreach ($result_manufacturer['data_info'] as $manufacturer_val)
    {
        $arr_manufacturer[$manufacturer_val['nns_id']] = $manufacturer_val['nns_name'];
    }
}



if ($action=="edit"){
	$edit_data = nl_transcode_load_balance::query_by_id($dc,$nns_id);
	
	if($edit_data['ret'] != 0 )
	{
		echo "<script>alert('". $action_str. $edit_data['reason']. "');</script>";
		echo "<script>self.location='".$base_file_name_base."_list.php;</script>";
	}
	$edit_data = $edit_data['data_info'];
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link href="../../css/comm.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
        <script language="javascript">
        
        </script>
    </head>
    <body>
        <div class="content">
        	<div class="content_position">转码负载均衡配置 > 添加/修改</div>
        	<form id="add_form" action="<?php echo $base_file_name_base;?>_control.php" method="post">
            	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
            	<div class="content_table">
            		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            		  <tbody>
            		    
            		    <?php if($action == 'edit'){
            		        $result_data_live_last_key = !empty($result_data_live_last) ? array_keys($result_data_live_last) : array();
            		    ?>
                		    <tr>
                				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>GUID:</td>
                				<td>
                				    <input name="nns_id" id="nns_id" type="hidden"  value="<?php if(isset($edit_data["nns_id"])){echo $edit_data["nns_id"];}?>" rule="noempty"/>
                				    <input type="text" disabled value="<?php if(isset($edit_data["nns_id"])){echo $edit_data["nns_id"];}?>" />
                				</td>
                			</tr>
                			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>本地频道:</td>
            				<td>
            				    <?php if($edit_data['nns_type'] == '1'){ echo "直播";}else{ echo "点播";}?>
            				</td>
            			</tr>
                	   <?php }else{?>
                	   
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>转码厂家:</td>
            				<td>
            				    <select name='nns_type'>
                                    <option value="0" <?php if(isset($edit_data['nns_type']) && $edit_data['nns_type'] == '0'){ echo 'selected="selected"';}?>>点播</option>
                                    <option value="1" <?php if(isset($edit_data['nns_type']) && $edit_data['nns_type'] == '1'){ echo 'selected="selected"';}?>>直播</option>
                                </select>
            				</td>
            			</tr>
            			<?php }?>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>负载均衡名称:</td>
            				<td>
                                <input name="nns_name" id="nns_name" type="text"  value="<?php if(isset($edit_data["nns_name"])){echo $edit_data["nns_name"];}?>" rule="noempty"/>
            				</td>
            			</tr>
            	       <tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>转码厂家:</td>
            				<td>
            				    <select name='nns_manufacturer_id'>
                                    <?php if(!empty($arr_manufacturer)){foreach ($arr_manufacturer as $manufacturer_key=>$manufacturer_val){?>
                                    <option value="<?php echo $manufacturer_key;?>" <?php if(isset($edit_data['nns_manufacturer_id']) && $edit_data['nns_manufacturer_id'] == $manufacturer_key){ echo 'selected="selected"';}?>><?php echo $manufacturer_val;?></option>
                                    <?php }}?>
                                </select>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>下游平台:</td>
            				<td>
            				    <select name='nns_sp_id'>
                                    <?php if(!empty($arr_sp)){foreach ($arr_sp as $sp_key=>$sp_val){?>
                                    <option value="<?php echo $sp_key;?>" <?php if(isset($edit_data['nns_sp_id']) && $edit_data['nns_sp_id'] == $sp_key){ echo 'selected="selected"';}?>><?php echo $sp_val;?></option>
                                    <?php }}?>
                                </select>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>API请求地址:</td>
            				<td>
                                <input name="nns_api_url" id="nns_api_url" type="text"  value="<?php if(isset($edit_data["nns_api_url"])){echo $edit_data["nns_api_url"];}?>" rule="noempty"/>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>承载最大线程:</td>
            				<td>
                                <input name="nns_max_num" id="nns_max_num" type="text"  value="<?php if(isset($edit_data["nns_max_num"])){echo $edit_data["nns_max_num"];}else{ echo '0';}?>" rule="int"/>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>转码厂家:</td>
            				<td>
            				    <select name='nns_state'>
                                    <option value="0" <?php if(isset($edit_data['nns_type']) && $edit_data['nns_type'] == '0'){ echo 'selected="selected"';}?>>启用</option>
                                    <option value="1" <?php if(isset($edit_data['nns_type']) && $edit_data['nns_type'] == '1'){ echo 'selected="selected"';}?>>禁用</option>
                                </select>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle">描述:</td>
            				<td><textarea name="nns_desc"><?php if(isset($edit_data["nns_desc"])){echo $edit_data["nns_desc"];}else{ echo '';}?></textarea></td>
            			</tr>
            		  </tbody>
            		</table>
            	</div>
			</form>
        	<div class="controlbtns">
        		<div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','<?php echo cms_get_lang('webkz_qrccxg'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
        		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        		<div style="clear:both;"></div>
        	</div>
        </div>
    </body>
</html>
