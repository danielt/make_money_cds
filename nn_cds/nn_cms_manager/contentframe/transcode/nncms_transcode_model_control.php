<?php
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
include $nncms_config_path . 'nn_cms_manager/controls/nncms_controls_public_function.php';
require_once $nncms_db_path . 'nns_log' . DIRECTORY_SEPARATOR . 'nns_db_op_log_class.php';
$log_inst = new nns_db_op_log_class();

function params_check($params)
{
    $arr_config = array(
        'nns_video_config',
        'nns_audio_config',
        'nns_other_config',
    );
    foreach ($arr_config as $config_val)
    {
        if(isset($params[$config_val]['key']) && is_array($params[$config_val]['key']) && !empty($params[$config_val]['key']))
        {
            $temp_data = null;
            foreach ($params[$config_val]['key'] as $key=>$val)
            {
                $val = trim($val);
                if(strlen($val)<1)
                {
                    continue;
                }
                $value = (isset($params[$config_val]['value'][$key]) && strlen($params[$config_val]['value'][$key])>0) ? trim($params[$config_val]['value'][$key]) : '';
                $desc = (isset($params[$config_val]['desc'][$key])  && strlen($params[$config_val]['desc'][$key])>0) ? trim($params[$config_val]['desc'][$key]) : '';
                if(strlen($value)<1)
                {
                    continue;
                }
                if(isset($temp_data[$val]))
                {
                    $str_error_desc = ($config_val == 'nns_video_config') ? '视频' : '音频';
                    echo "<script>alert('{$str_error_desc}格式参数错误，参数重复{$val}');history.go(-1);</script>";
                }
                $temp_data[$val]=array(
                    'value'=>$value,
                    'desc'=>$desc,
                );
            }
            $params[$config_val] = (!empty($temp_data) && is_array($temp_data)) ? json_encode($temp_data,JSON_UNESCAPED_UNICODE) : '';
        }
    }
    return $params;
}
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE, 
		'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$dc->open();
$params = $_REQUEST;
$params = params_check($params);
$action = $params['action'];
unset($params['action']);
$log_params = (!empty($params) && is_array($params)) ? json_encode($params) : '';
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);


/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/transcode/transcode_model.class.php';
session_start();
$arr_need_data = array(
    'need_filed'=>array(
            'nns_cp_id',
            'nns_sp_id',
            'nns_model_id',
            'nns_input_definition',
            'nns_output_definition',
            'nns_input_file_type',
            'nns_output_file_type',
            'nns_state',
        ),
    'default_value'=>array(
        'nns_start_bitrate'=>0,
        'nns_end_bitrate'=>0,
        'nns_output_bitrate'=>0,
        'nns_output_file_tag'=>array('26'),
    ),
);



switch ($action)
{
	case "edit":
		$nns_id = $params['nns_id'];//key GUID
		unset($params['nns_id']);
		$temp_need_data = null;
		foreach ($arr_need_data as $key=>$val)
		{
		    if($key == 'need_filed')
		    {
		        foreach ($val as $_v)
		        {
		            if(!isset($params[$_v]))
		            {
		                $temp_need_data[] = $_v;
		            }
		        }
		    }
		    else
		    {
		        foreach ($val as $_k=>$_v)
		        {
		            $params[$_k] = isset($params[$_k]) ? $params[$_k] : $_v;
		            if($_k == 'nns_output_file_tag')
		            {
		                $params[$_k] = (is_array($params[$_k]) && !empty($params[$_k])) ? $params[$_k] : $_v;
		                $params[$_k] = implode(',', $params[$_k]).',';
		            }
		            $params[$_k] = strlen($params[$_k]) >0 ? $params[$_k] : $_v;
		        }
		    }
		}
		if(is_array($temp_need_data) && !empty($temp_need_data))
		{
		    echo "<script>alert('缺失参数".implode(',', $temp_need_data)."');history.go(-1);</script>";
		    break;
		}
		$result_unique = nl_transcode_model::query_unique($dc, "where nns_type='{$params['nns_type']}' and nns_input_definition='{$params['nns_input_definition']}' and nns_output_definition='{$params['nns_output_definition']}' ".
	    " and nns_input_file_type='{$params['nns_input_file_type']}' and nns_output_file_type='{$params['nns_output_file_type']}' and nns_output_file_tag='{$params['nns_output_file_tag']}' and nns_start_bitrate='{$params['nns_start_bitrate']}' and  ".
	    " nns_end_bitrate='{$params['nns_end_bitrate']}' and nns_output_bitrate='{$params['nns_output_bitrate']}' ".
	    " and nns_name='{$params['nns_name']}' and nns_cp_id='{$params['nns_cp_id']}' and nns_sp_id='{$params['nns_sp_id']}' and nns_model_id='{$params['nns_model_id']}' and nns_id !='{$nns_id}'");
		 
		if($result_unique['ret'] != 0)
		{
		    echo "<script>alert('" . $result_unique['reason'] . "');history.go(-1);</script>";
		    break;
		}
		if(isset($result_unique['data_info'][0]['count']) && $result_unique['data_info'][0]['count'] >0)
		{
		    echo "<script>alert('数据已经存在,不允许在添加');history.go(-1);</script>";
		    break;
		}
		$result_unique = nl_transcode_model::query_group_model($dc, $params['nns_model_id'],$params['nns_cp_id']);
		
		if($result_unique['ret'] != 0)
		{
		    echo "<script>alert('" . $result_unique['reason'] . "');history.go(-1);</script>";
		    break;
		}
		if(isset($result_unique['data_info']) && is_array($result_unique['data_info']) && count($result_unique['data_info']) >1)
		{
		    echo "<script>alert('数据不能绑定多个厂家,不允许在添加');history.go(-1);</script>";
		    break;
		}
	    else if(isset($result_unique['data_info']) && is_array($result_unique['data_info']) && count($result_unique['data_info']) == 1 && $result_unique['data_info'][0]['manufacturer_id'] != $params['nns_manufacturer_id'])
	    {
	        echo "<script>alert('数据不能绑定多个厂家,不允许在添加');history.go(-1);</script>";
	        break;
	    }
	    unset($params['nns_manufacturer_id']);
        if($params['nns_drawing_frame'] == '1')
	    {
	        if(empty($params['nns_drawing_type']) || !is_array($params['nns_drawing_type']))
	        {
	            echo "<script>alert('抽帧图片类型未选择');history.go(-1);</script>";
	            break;
	        }
	        if(empty($params['nns_img_type']))
	        {
	            echo "<script>alert('抽帧图片文件后缀未选择');history.go(-1);</script>";
	            break;
	        }
	        $params['nns_drawing_rule'] = (int)$params['nns_drawing_rule'];
	        if($params['nns_drawing_rule']<1)
	        {
	            echo "<script>alert('抽帧时间规则必须大于0');history.go(-1);</script>";
	            break;
	        }
	        $params['nns_drawing_type'] = json_encode($params['nns_drawing_type']);
	    }
	    else
	    {
	        $params['nns_drawing_type'] = '';
	        $params['nns_img_type'] = '';
	        $params['nns_drawing_rule'] = '0';
	    }
		$result = nl_transcode_model::edit($dc, $params, $nns_id);
		if ($result['ret'])
		{
			echo "<script>alert('" . $result['reason'] . "');history.go(-1);</script>";
		}
		else
		{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "修改:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
			echo "<script>alert('修改成功');</script>";
		}
		break;
	case "add":
	    $temp_need_data = null;
	    foreach ($arr_need_data as $key=>$val)
	    {
	        if($key == 'need_filed')
	        {
	            foreach ($val as $_v)
	            {
	                if(!isset($params[$_v]))
	                {
	                    $temp_need_data[] = $_v;
	                }
	            }
	        }
	        else
	        {
	            foreach ($val as $_k=>$_v)
	            {
	                $params[$_k] = isset($params[$_k]) ? $params[$_k] : $_v;
	                if($_k == 'nns_output_file_tag')
	                {
	                    $params[$_k] = (is_array($params[$_k]) && !empty($params[$_k])) ? $params[$_k] : $_v;
	                    $params[$_k] = implode(',', $params[$_k]).',';
	                }
	                $params[$_k] = strlen($params[$_k]) >0 ? $params[$_k] : $_v;
	            }
	        }
	    }
	    if(is_array($temp_need_data) && !empty($temp_need_data))
	    {
	        echo "<script>alert('缺失参数".implode(',', $temp_need_data)."');history.go(-1);</script>";
	        break;
	    }
	    
	    $result_unique = nl_transcode_model::query_unique($dc, "where  nns_type='{$params['nns_type']}' and nns_input_definition='{$params['nns_input_definition']}' and nns_output_definition='{$params['nns_output_definition']}' ".
	    " and nns_input_file_type='{$params['nns_input_file_type']}' and nns_output_file_type='{$params['nns_output_file_type']}' and nns_output_file_tag='{$params['nns_output_file_tag']}' and nns_start_bitrate='{$params['nns_start_bitrate']}' and  ".
	    " nns_end_bitrate='{$params['nns_end_bitrate']}' and nns_output_bitrate='{$params['nns_output_bitrate']}' ".
	    " and nns_name='{$params['nns_name']}' and nns_sp_id='{$params['nns_sp_id']}' and nns_cp_id='{$params['nns_cp_id']}' and nns_model_id='{$params['nns_model_id']}'");
	    
	    if($result_unique['ret'] != 0)
	    {
	        echo "<script>alert('" . $result_unique['reason'] . "');history.go(-1);</script>";
	        break;
	    }
	    if(isset($result_unique['data_info'][0]['count']) && $result_unique['data_info'][0]['count'] >0)
	    {
	        echo "<script>alert('数据已经存在,不允许在添加');history.go(-1);</script>";
	        break;
	    }
	    
	    $result_unique = nl_transcode_model::query_group_model($dc, $params['nns_model_id'],$params['nns_cp_id']);
	     
	    if($result_unique['ret'] != 0)
	    {
	        echo "<script>alert('" . $result_unique['reason'] . "');history.go(-1);</script>";
	        break;
	    }
	    if(isset($result_unique['data_info']) && is_array($result_unique['data_info']) && count($result_unique['data_info']) >1)
	    {
	        echo "<script>alert('数据不能绑定多个厂家,不允许在添加');history.go(-1);</script>";
	        break;
	    }
	    else if(isset($result_unique['data_info']) && is_array($result_unique['data_info']) && count($result_unique['data_info']) == 1 && $result_unique['data_info'][0]['manufacturer_id'] != $params['nns_manufacturer_id'])
	    {
	        echo "<script>alert('数据不能绑定多个厂家,不允许在添加');history.go(-1);</script>";
	        break;
	    }
	    if($params['nns_drawing_frame'] == '1')
	    {
	        if(empty($params['nns_drawing_type']) || !is_array($params['nns_drawing_type']))
	        {
	            echo "<script>alert('抽帧图片类型未选择');history.go(-1);</script>";
	            break;
	        }
	        if(empty($params['nns_img_type']))
	        {
	            echo "<script>alert('抽帧图片文件后缀未选择');history.go(-1);</script>";
	            break;
	        }
	        $params['nns_drawing_rule'] = (int)$params['nns_drawing_rule'];
	        if($params['nns_drawing_rule']<1)
	        {
	            echo "<script>alert('抽帧时间规则必须大于0');history.go(-1);</script>";
	            break;
	        }
	        $params['nns_drawing_type'] = json_encode($params['nns_drawing_type']);
	    }
	    else
	    {
	        $params['nns_drawing_type'] = '';
	        $params['nns_img_type'] = '';
	        $params['nns_drawing_rule'] = '0';
	    }
	    unset($params['nns_manufacturer_id']);
		$result = nl_transcode_model::add($dc, $params);
		if ($result['ret'])
		{
		    echo "<script>alert('" . $result['reason'] . "');</script>";
		}
		else
		{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "添加:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
			echo "<script>alert('添加成功');</script>";
		}
		break;
	case "delete":
        $nns_ids = explode(",", $params['nns_id']);
		if(!is_array($nns_ids) || empty($nns_ids))
		{
		    echo "<script>alert('删除成功,数据为空');</script>";
		    break;
		}
		foreach ($nns_ids as $con_val)
		{
			nl_transcode_model::delete($dc,$con_val);
		}
		$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "删除:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
		echo "<script>alert('删除成功');</script>";
		break;
	case "state":
        $result = nl_transcode_model::edit($dc, array('nns_state'=>$params['nns_state']), $params['nns_id']);
		if ($result['ret'])
		{
			echo "<script>alert('" . $result['reason'] . "');history.go(-1);</script>";
		}
		else
		{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "修改:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
			echo "<script>alert('修改成功');</script>";
		}
	    break;
	default:
	    echo "<script>alert('没有此方式');</script>";
		break;
}
$partner_inst = null;
$log_inst = null;
echo "<script>self.location='".$base_file_name_base."_list.php?model_id={$params['nns_model_id']}';</script>";
