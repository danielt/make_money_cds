<?php
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
include $nncms_config_path . 'nn_cms_manager/controls/nncms_controls_public_function.php';
require_once $nncms_db_path . 'nns_log' . DIRECTORY_SEPARATOR . 'nns_db_op_log_class.php';
$log_inst = new nns_db_op_log_class();

$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE, 
		'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$dc->open();
$params = $_REQUEST;

$action = $params['action'];
unset($params['action']);
$log_params = (!empty($params) && is_array($params)) ? json_encode($params) : '';
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);


/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/transcode/transcode_manufacturer_model.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/transcode/transcode_manufacturer.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/transcode/transcode_model.class.php';
session_start();
switch ($action)
{
	case "edit":
		$nns_id = $params['nns_id'];//key GUID
		unset($params['nns_id']);
		if(!isset($params['nns_manufacturer_id']) || strlen($params['nns_manufacturer_id']) <1)
		{
		    echo "<script>alert('厂家为空');history.go(-1);</script>";
		    break;
		}
        $result_unique = nl_transcode_manufacturer_model::query_unique($dc, "where (nns_name='{$params['nns_name']}' or (nns_manufacturer_id='{$params['nns_manufacturer_id']}' and nns_mark='{$params['nns_mark']}') or (nns_manufacturer_id='{$params['nns_manufacturer_id']}' and nns_file_path='{$params['nns_file_path']}') ) and nns_id !='{$nns_id}'");
	    if($result_unique['ret'] != 0)
	    {
	        echo "<script>alert('" . $result_unique['reason'] . "');history.go(-1);</script>";
	        break;
	    }
	    if(isset($result_unique['data_info'][0]['count']) && $result_unique['data_info'][0]['count'] >0)
	    {
	        echo "<script>alert('数据已经存在,不允许在添加');history.go(-1);</script>";
	        break;
	    }
		$result = nl_transcode_manufacturer_model::edit($dc, $params, $nns_id);
		if ($result['ret'])
		{
			echo "<script>alert('" . $result['reason'] . "');history.go(-1);</script>";
		}
		else
		{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "修改:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
			echo "<script>alert('修改成功');</script>";
		}
		break;
	case "add":
	    if(!isset($params['nns_manufacturer_id']) || strlen($params['nns_manufacturer_id']) <1)
	    {
	        echo "<script>alert('厂家为空');history.go(-1);</script>";
	        break;
	    }
	    $result_unique = nl_transcode_manufacturer_model::query_unique($dc, "where (nns_name='{$params['nns_name']}' or (nns_manufacturer_id='{$params['nns_manufacturer_id']}' and nns_mark='{$params['nns_mark']}') or (nns_manufacturer_id='{$params['nns_manufacturer_id']}' and nns_file_path='{$params['nns_file_path']}') )");
	    if($result_unique['ret'] != 0)
	    {
	        echo "<script>alert('" . $result_unique['reason'] . "');history.go(-1);</script>";
	        break;
	    }
	    if(isset($result_unique['data_info'][0]['count']) && $result_unique['data_info'][0]['count'] >0)
	    {
	        echo "<script>alert('数据已经存在,不允许在添加');history.go(-1);</script>";
	        break;
	    }
		$result = nl_transcode_manufacturer_model::add($dc, $params);
		if ($result['ret'])
		{
		    echo "<script>alert('" . $result['reason'] . "');</script>";
		}
		else
		{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "添加:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
			echo "<script>alert('添加成功');</script>";
		}
		break;
	case "delete":
        $nns_ids = explode(",", $params['nns_id']);
		if(!is_array($nns_ids) || empty($nns_ids))
		{
		    echo "<script>alert('删除成功,数据为空');</script>";
		    break;
		}
		foreach ($nns_ids as $con_val)
		{
		    $result_con = nl_transcode_model::query_by_model_id($dc,$con_val);
		    if(isset($result_con['data_info']) && is_array($result_con['data_info']) && !empty($result_con['data_info']))
		    {
		        continue;
		    }
			nl_transcode_manufacturer_model::delete($dc,$con_val);
		}
		$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "删除:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
		echo "<script>alert('删除成功');</script>";
		break;
	case "state":
	    $result = nl_transcode_manufacturer_model::edit($dc, array('nns_state'=>$params['nns_state']), $params['nns_id']);
	    if ($result['ret'])
	    {
	        echo "<script>alert('" . $result['reason'] . "');history.go(-1);</script>";
	    }
	    else
	    {
	        $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "修改:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
	        echo "<script>alert('修改成功');</script>";
	    }
	    break;
	default:
	    echo "<script>alert('没有此方式');</script>";
		break;
}
$partner_inst = null;
$log_inst = null;
echo "<script>self.location='".$base_file_name_base."_list.php?model_id={$params['nns_model_id']}';</script>";
