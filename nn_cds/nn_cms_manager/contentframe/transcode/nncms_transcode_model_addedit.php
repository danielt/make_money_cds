<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$action=$_GET["action"];
$nns_id=$_REQUEST["nns_id"];
$pri_bool=false;
if (!empty($action)){
	switch($action){
	case "edit":
		$action_str=cms_get_lang('save');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136002");
		break;
	case "add":
		$action_str=cms_get_lang('add');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136003");
		break;
	default:
		break;
	}
}
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
    "db_policy" => NL_DB_WRITE,
    "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);
$edit_data = $_REQUEST;


/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/transcode/transcode_manufacturer_model.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/vod_media/vod_media.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/transcode/transcode_model.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/transcode/transcode_manufacturer.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/sp/sp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/project_group/project_key.class.php';

$result_cp = nl_cp::query_all($dc);
if($result_cp['ret'] != 0)
{
    echo '<script>alert("'.$result_cp['reason'].'");history.go(-1);</script>';die;
}
$result_cp = isset($result_cp['data_info']) ? $result_cp['data_info'] : null;
$arr_cp_info = array();
if(is_array($result_cp) && !empty($result_cp))
{
    foreach ($result_cp as $val)
    {
        $arr_temp_config = isset($val['nns_config'])?json_decode($val['nns_config'],true):null;
        if(!isset($arr_temp_config['cp_transcode_file_enabled']) || $arr_temp_config['cp_transcode_file_enabled']!=1)
        {
            continue;
        }
        $arr_cp_info[$val['nns_id']] = $val['nns_name'];
    }
}
$result_sp = nl_sp::query_all($dc);
if($result_sp['ret'] != 0)
{
    echo '<script>alert("'.$result_sp['reason'].'");history.go(-1);</script>';die;
}
$result_sp = isset($result_sp['data_info']) ? $result_sp['data_info'] : null;
$arr_sp_info = array();
if(is_array($result_sp) && !empty($result_sp))
{
    foreach ($result_sp as $val)
    {
        $arr_temp_config = isset($val['nns_config'])?json_decode($val['nns_config'],true):null;
        if(!isset($arr_temp_config['clip_file_encode_enable']) || $arr_temp_config['clip_file_encode_enable']!=1)
        {
            continue;
        }
        $arr_sp_info[$val['nns_id']] = $val['nns_name'];
    }
}
$result_manufacturer = nl_transcode_manufacturer::query_all($dc);
$arr_manufacturer_model = $arr_manufacturer  =null;
if(isset($result_manufacturer['data_info']) && !empty($result_manufacturer['data_info']) && is_array($result_manufacturer['data_info']))
{
    foreach ($result_manufacturer['data_info'] as $val_manufacturer)
    {
        $arr_manufacturer[$val_manufacturer['nns_id']] = $val_manufacturer['nns_name'];
    }
}

$result_project_key = nl_project_key::query_by_key($dc, 'drawing_frame');
$result_project_key = (isset($result_project_key['data_info']['nns_key_value']) && strlen($result_project_key['data_info']['nns_key_value']) >0) ? json_decode($result_project_key['data_info']['nns_key_value'],true) : null;
$result_project_key = (is_array($result_project_key) && !empty($result_project_key)) ? $result_project_key : null;

$result_manufacturer_model = nl_transcode_manufacturer_model::query_all($dc);
if(isset($result_manufacturer_model['data_info']) && !empty($result_manufacturer_model['data_info']) && is_array($result_manufacturer_model['data_info']))
{
    foreach ($result_manufacturer_model['data_info'] as $val_manufacturer_model)
    {
        $arr_manufacturer_model[$val_manufacturer_model['nns_id']] = $val_manufacturer_model['nns_name'];
    }
}

$arr_definition = array(
    'std'=>'标清',
    'hd'=>'高清',
    'sd'=>'超高清',
    '4k'=>'4K',
    'low'=>'流畅',
    'dubi'=>'杜比',
    'default'=>'默认清晰度',
);
$arr_filetype = array(
    'ts'=>'ts',
    'm3u8'=>'m3u8',
    'mp4'=>'mp4',
    'mts'=>'mts',
    'mpg'=>'mpg',
    'default'=>'默认文件类型',
);
$result_definition = nl_vod_media_v2::query_group_mode($dc);
if(isset($result_definition['data_info']) && !empty($result_definition['data_info']) && is_array($result_definition['data_info']))
{
    $arr_definition_keys = array_keys($arr_definition);
    foreach ($result_definition['data_info'] as $val)
    {
        if(!in_array($val['nns_mode'], $arr_definition_keys))
        {
            $arr_definition[$val['nns_mode']] = $val['nns_mode'];
        }
    }
}
$result_filetype = nl_vod_media_v2::query_group_filetype($dc);
if(isset($result_filetype['data_info']) && !empty($result_filetype['data_info']) && is_array($result_filetype['data_info']))
{
    foreach ($result_filetype['data_info'] as $val)
    {
        if(!in_array($val['nns_filetype'], $arr_filetype))
        {
            $arr_filetype[$val['nns_filetype']] = $val['nns_filetype'];
        }
    }
}
if ($action=="edit")
{
    $edit_data = nl_transcode_model::query_by_id($dc,$nns_id);

    if($edit_data['ret'] != 0 )
    {
        echo "<script>alert('". $action_str. $edit_data['reason']. "');</script>";
        echo "<script>self.location='".$base_file_name_base."_list.php;</script>";
    }
    $edit_data = $edit_data['data_info'];
    
    $result_model_data = nl_transcode_manufacturer_model::query_by_id($dc, $edit_data['nns_model_id']);
    $edit_data['nns_manufacturer_id'] = isset($result_model_data['data_info']['nns_manufacturer_id']) ?$result_model_data['data_info']['nns_manufacturer_id'] : '';
    
    $edit_data['nns_drawing_type'] = strlen($edit_data['nns_drawing_type']) >0 ? json_decode($edit_data['nns_drawing_type'],true) : null;
    $edit_data['nns_video_config'] = strlen($edit_data['nns_video_config']) >0 ? json_decode($edit_data['nns_video_config'],true) : null;
    $edit_data['nns_audio_config'] = strlen($edit_data['nns_audio_config']) >0 ? json_decode($edit_data['nns_audio_config'],true) : null;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link href="../../css/comm.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
        <script language="javascript">
            $(document).ready(function(e){
            	change();
            	$('input[type=radio][name=nns_drawing_frame]').change(function(){
                	change();
            	});
            	//点击删除
                $(document).on('click',function(e){
                	var obj = $(this);
                	var data_type= obj.attr('data_type');
                	var length = $('.nns_'+data_type+'_config_value').length;
                    if(length >1)
                    {
                    	obj.parent().parent().remove();
                    }
                    else
                    {
                    	obj.parent().find("input[type='text']").attr("value","");
                    }
                });
            });

            function change()
            {
            	var nns_type = $("input[name='nns_drawing_frame']:checked").val();
            	if(nns_type == '1')
                {
            		$('.nns_drawing_type').show();
                }
            	else
                {
            		$('.nns_drawing_type').hide();
                }
            }

            function add_config(type)
            {
				var obj_tr = $('.nns_'+type+'_config_value');
				var clone = obj_tr.eq(0).clone();
				clone.find("input[type='text']").attr("value","");
				obj_tr.eq(obj_tr.length-1).after(clone);
	            window.parent.resetFrameHeight();
            }
	
        	
            $("#button_delete").live("click", function(){
            	var obj = $(this);
            	var data_type= obj.attr('data_type');
            	var length = $('.nns_'+data_type+'_config_value').length;
                if(length >1)
                {
                	obj.parent().parent().remove();
                }
                else
                {
                	obj.parent().find("input[type='text']").attr("value","");
                }
            });
			
        </script>
    </head>
    <body>
        <div class="content">
        	<div class="content_position">转码策略 > 添加/修改</div>
        	<form id="add_form" action="<?php echo $base_file_name_base;?>_control.php" method="post">
            	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
            	<div class="content_table">
            		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            		  <tbody class="tr_tbody">
            		    <?php 
            		    if($action == 'edit'){
            		    ?>
                		    <tr>
                				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>GUID:</td>
                				<td>
                				    <input name="nns_id" id="nns_id" type="hidden"  value="<?php if(isset($edit_data["nns_id"])){echo $edit_data["nns_id"];}?>" rule="noempty"/>
                				    <input type="text" style="width:540px;"  disabled value="<?php if(isset($edit_data["nns_id"])){echo $edit_data["nns_id"];}?>" />
                				</td>
                			</tr>
                			<tr>
                				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>厂家模板:</td>
                				<td>
                                    <?php 
                                        $flag = true;
                                        if(is_array($arr_manufacturer) && !empty($arr_manufacturer) && isset($edit_data['nns_manufacturer_id']))
                                        {
                                            foreach ($arr_manufacturer as $cp_key=>$cp_val)
                                            {
                                                if($cp_key == $edit_data['nns_manufacturer_id'])
                                                {
                                                    echo "<font>{$cp_val}</font>";
                                                    echo "<input name='nns_manufacturer_id' id='nns_manufacturer_id' type='hidden' value='{$cp_key}'/>";
                                                    $flag = false;
                                                }
                                            }
                                        }
                                        if($flag)
                                        {
                                            echo "<font color='red'>数据错误</font>";
                                            echo "<input name='nns_manufacturer_id' id='nns_manufacturer_id' type='hidden' value=''/>";
                                        }
                                    ?>
                				</td>
                			</tr>
                			<tr>
                				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>厂家模板:</td>
                				<td>
                                    <?php 
                                        $flag = true;
                                        if(is_array($arr_manufacturer_model) && !empty($arr_manufacturer_model) && isset($edit_data['nns_model_id']))
                                        {
                                            foreach ($arr_manufacturer_model as $cp_key=>$cp_val)
                                            {
                                                if($cp_key == $edit_data['nns_model_id'])
                                                {
                                                    echo "<font>{$cp_val}</font>";
                                                    echo "<input name='nns_model_id' id='nns_model_id' type='hidden' value='{$cp_key}'/>";
                                                    $flag = false;
                                                }
                                            }
                                        }
                                        if($flag)
                                        {
                                            echo "<font color='red'>数据错误</font>";
                                            echo "<input name='nns_model_id' id='nns_model_id' type='hidden' value=''/>";
                                        }
                                    ?>
                				</td>
                			</tr>
            		    <?php }else{?>
            		    <tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>厂家:</td>
            				<td>
            				    <select id="nns_manufacturer_id" name='nns_manufacturer_id' style="width:550px;">
                                    <?php if(is_array($arr_manufacturer) && !empty($arr_manufacturer)){foreach ($arr_manufacturer as $cp_key=>$cp_val){?>
                                    <option value="<?php echo $cp_key;?>" <?php if(isset($edit_data['nns_manufacturer_id']) && $edit_data['nns_manufacturer_id'] == $cp_key){ echo 'selected="selected"';}?>><?php echo $cp_val;?></option>
                                    <?php }}?>
                                </select>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>厂家模板:</td>
            				<td>
            				    <select id="nns_model_id" name='nns_model_id' style="width:550px;">
                                    <?php if(is_array($arr_manufacturer_model) && !empty($arr_manufacturer_model)){foreach ($arr_manufacturer_model as $cp_key=>$cp_val){?>
                                    <option value="<?php echo $cp_key;?>" <?php if(isset($edit_data['nns_model_id']) && $edit_data['nns_model_id'] == $cp_key){ echo 'selected="selected"';}?>><?php echo $cp_val;?></option>
                                    <?php }}?>
                                </select>
            				</td>
            			</tr>
            			<?php }?>
            			<tr>
            				<td class="rightstyle">转码文件类型:</td>
            				<td>
            				    <select name='nns_type' style="width:550px;">
                                    <option value="0" <?php if(isset($edit_data['nns_type']) && $edit_data['nns_type'] != '1'){ echo 'selected="selected"';}?>>点播</option>
                                    <option value="1" <?php if(isset($edit_data['nns_type']) && $edit_data['nns_type'] == '1'){ echo 'selected="selected"';}?>>直播</option>
                                </select>
            				</td>
            			</tr>
            		    <tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>转码策略名称:</td>
            				<td><input style="width:540px;" name="nns_name" id="nns_name" type="text"  value="<?php if(isset($edit_data["nns_name"])){echo $edit_data["nns_name"];}?>" rule="noempty" /></td>
            			</tr>
            			
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>CP ID:</td>
            				<td>
            				    <select id="nns_cp_id" name='nns_cp_id' style="width:550px;">
                                    <?php if(is_array($arr_cp_info) && !empty($arr_cp_info)){foreach ($arr_cp_info as $cp_key=>$cp_val){?>
                                    <option value="<?php echo $cp_key;?>" <?php if(isset($edit_data['nns_cp_id']) && $edit_data['nns_cp_id'] == $cp_key){ echo 'selected="selected"';}?>><?php echo $cp_key.' / '.$cp_val;?></option>
                                    <?php }}?>
                                </select>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>SP ID:</td>
            				<td>
            				    <select id="nns_sp_id" name='nns_sp_id' style="width:550px;">
                                    <?php if(is_array($arr_sp_info) && !empty($arr_sp_info)){foreach ($arr_sp_info as $sp_key=>$sp_val){?>
                                    <option value="<?php echo $sp_key;?>" <?php if(isset($edit_data['nns_sp_id']) && $edit_data['nns_sp_id'] == $sp_key){ echo 'selected="selected"';}?>><?php echo $sp_key.' / '.$sp_val;?></option>
                                    <?php }}?>
                                </select>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>输入清晰度:</td>
            				<td>
            				    <select id="nns_input_definition" name='nns_input_definition' style="width:550px;">
                                    <?php if(isset($arr_definition) && is_array($arr_definition) && !empty($arr_definition)){foreach ($arr_definition as $cp_key=>$cp_val){?>
                                    <option value="<?php echo $cp_key;?>" <?php if(isset($edit_data['nns_input_definition']) && $edit_data['nns_input_definition'] == $cp_key){ echo 'selected="selected"';}?>><?php echo $cp_val;?></option>
                                    <?php }}?>
                                </select>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>输出清晰度:</td>
            				<td>
            				    <select id="nns_output_definition" name='nns_output_definition' style="width:550px;">
                                    <?php if(isset($arr_definition) && is_array($arr_definition) && !empty($arr_definition)){foreach ($arr_definition as $cp_key=>$cp_val){
                                        if($cp_key == 'default'){
                                            continue;
                                        }
                                    ?>
                                    <option value="<?php echo $cp_key;?>" <?php if(isset($edit_data['nns_output_definition']) && $edit_data['nns_output_definition'] == $cp_key){ echo 'selected="selected"';}?>><?php echo $cp_val;?></option>
                                    <?php }}?>
                                </select>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>输入文件类型:</td>
            				<td>
            				    <select id="nns_input_file_type" name='nns_input_file_type' style="width:550px;">
                                    <?php if(isset($arr_filetype) && is_array($arr_filetype) && !empty($arr_filetype)){foreach ($arr_filetype as $cp_key=>$cp_val){?>
                                    <option value="<?php echo $cp_key;?>" <?php if(isset($edit_data['nns_input_file_type']) && $edit_data['nns_input_file_type'] == $cp_key){ echo 'selected="selected"';}?>><?php echo $cp_val;?></option>
                                    <?php }}?>
                                </select>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>输出文件类型:</td>
            				<td>
            				    <select id="nns_output_file_type" name='nns_output_file_type' style="width:550px;">
                                    <?php if(isset($arr_filetype) && is_array($arr_filetype) && !empty($arr_filetype)){foreach ($arr_filetype as $cp_key=>$cp_val){
                                        if($cp_key == 'default'){
                                            continue;
                                        }
                                    ?>
                                    <option value="<?php echo $cp_key;?>" <?php if(isset($edit_data['nns_output_file_type']) && $edit_data['nns_output_file_type'] == $cp_key){ echo 'selected="selected"';}?>><?php echo $cp_val;?></option>
                                    <?php }}?>
                                </select>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>输出EPG TAG标示:</td>
            				<td>
            				    <input name="nns_output_file_tag[]" id="nns_output_file_tag" type="checkbox" value="26" <?php if(isset($edit_data["nns_output_file_tag"]) && (strpos($edit_data['nns_output_file_tag'],'26,') !== false)){echo "checked";}?>/>  STB
            					<input name="nns_output_file_tag[]" id="nns_output_file_tag" type="checkbox" value="27" <?php if(isset($edit_data["nns_output_file_tag"]) && (strpos($edit_data['nns_output_file_tag'],'27,') !== false)){echo "checked";}?>/>  IPHONE
            					<input name="nns_output_file_tag[]" id="nns_output_file_tag" type="checkbox" value="28" <?php if(isset($edit_data["nns_output_file_tag"]) && (strpos($edit_data['nns_output_file_tag'],'28,') !== false)){echo "checked";}?>/>  IPAD
            					<input name="nns_output_file_tag[]" id="nns_output_file_tag" type="checkbox" value="29" <?php if(isset($edit_data["nns_output_file_tag"]) && (strpos($edit_data['nns_output_file_tag'],'29,') !== false)){echo "checked";}?>/>  ANDROID PHONE
            					<input name="nns_output_file_tag[]" id="nns_output_file_tag" type="checkbox" value="30" <?php if(isset($edit_data["nns_output_file_tag"]) && (strpos($edit_data['nns_output_file_tag'],'30,') !== false)){echo "checked";}?>/>  ANDROID PAD
            					<input name="nns_output_file_tag[]" id="nns_output_file_tag" type="checkbox" value="31" <?php if(isset($edit_data["nns_output_file_tag"]) && (strpos($edit_data['nns_output_file_tag'],'31,') !== false)){echo "checked";}?>/>  PC
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle">输入文件开始码率:</td>
            				<td><input style="width:540px;"  name="nns_start_bitrate" id="nns_start_bitrate" type="text"  value="<?php if(isset($edit_data["nns_start_bitrate"])){echo $edit_data["nns_start_bitrate"];}?>"/></td>
            			</tr>
            			<tr>
            				<td class="rightstyle">输入文件结束码率:</td>
            				<td><input style="width:540px;"  name="nns_end_bitrate" id="nns_end_bitrate" type="text"  value="<?php if(isset($edit_data["nns_end_bitrate"])){echo $edit_data["nns_end_bitrate"];}?>"/></td>
            			</tr>
            			<tr>
            				<td class="rightstyle">输出码率:</td>
            				<td><input style="width:540px;"  name="nns_output_bitrate" id="nns_output_bitrate" type="text"  value="<?php if(isset($edit_data["nns_output_bitrate"])){echo $edit_data["nns_output_bitrate"];}?>"/></td>
            			</tr>
            			<tr>
            				<td class="rightstyle">界面展示码率:</td>
            				<td><input style="width:540px;"  name="nns_view_bitrate" id="nns_output_bitrate" type="text"  value="<?php if(isset($edit_data["nns_view_bitrate"])){echo $edit_data["nns_view_bitrate"];}?>"/></td>
            			</tr>
            			<tr>
            				<td class="rightstyle">视屏扩展配置:</td>
            				<td>
            				    
            				</td>
            			</tr>
            			<?php 
            		    if($action == 'edit' && isset($edit_data['nns_video_config']) && !empty($edit_data['nns_video_config']) && is_array($edit_data['nns_video_config'])){
            		      foreach ($edit_data['nns_video_config'] as $nns_video_config_key=>$nns_video_config){
            		    ?>
            		    <tr class='nns_video_config_value'>
	            			<td class="rightstyle"></td>
	            			<td>
    	            			&nbsp;&nbsp;键:&nbsp;&nbsp;<input type="text" name="nns_video_config[key][]" value="<?php echo $nns_video_config_key;?>" style="width:250px;" />
    	            			&nbsp;&nbsp;值:&nbsp;&nbsp;<input type="text" name="nns_video_config[value][]" value="<?php echo $nns_video_config['value'];?>" style="width:250px;" />
    	            			&nbsp;&nbsp;描述:&nbsp;&nbsp;<input type="text" name="nns_video_config[desc][]" value="<?php echo $nns_video_config['desc'];?>" style="width:400px;" />
    	            			&nbsp;&nbsp;<input type="button" data_type="video" value="删除" id="button_delete" />
    	            		</td>
	            		</tr>
            		    
            		    <?php }}else{?>
            			<tr class='nns_video_config_value'>
	            			<td class="rightstyle"></td>
	            			<td>
    	            			&nbsp;&nbsp;键:&nbsp;&nbsp;<input type="text" name="nns_video_config[key][]" value="" style="width:250px;" />
    	            			&nbsp;&nbsp;值:&nbsp;&nbsp;<input type="text" name="nns_video_config[value][]" value="" style="width:250px;" />
    	            			&nbsp;&nbsp;描述:&nbsp;&nbsp;<input type="text" name="nns_video_config[desc][]" value="" style="width:400px;" />
    	            			&nbsp;&nbsp;<input type="button" data_type="video" value="删除" id="button_delete" />
    	            		</td>
	            		</tr>
	            		<?php }?>
	            		<tr>
	            			<td class="rightstyle"></td>
	            			<td>
    	            			&nbsp;&nbsp;<a href="javascript:add_config('video');">+添加配置+</a>
    	            		</td>
	            		</tr>
            			<tr>
            				<td class="rightstyle">音频扩展配置:</td>
            				<td>
            				    
            				</td>
            			</tr>
            			<?php 
            		    if($action == 'edit' && isset($edit_data['nns_audio_config']) && !empty($edit_data['nns_audio_config']) && is_array($edit_data['nns_audio_config'])){
            		      foreach ($edit_data['nns_audio_config'] as $nns_audio_config_key=>$nns_audio_config){
            		    ?>
            		    <tr class='nns_audio_config_value'>
	            			<td class="rightstyle"></td>
	            			<td>
    	            			&nbsp;&nbsp;键:&nbsp;&nbsp;<input type="text" name="nns_audio_config[key][]" value="<?php echo $nns_audio_config_key;?>" style="width:250px;" />
    	            			&nbsp;&nbsp;值:&nbsp;&nbsp;<input type="text" name="nns_audio_config[value][]" value="<?php echo $nns_audio_config['value'];?>" style="width:250px;" />
    	            			&nbsp;&nbsp;描述:&nbsp;&nbsp;<input type="text" name="nns_audio_config[desc][]" value="<?php echo $nns_audio_config['desc'];?>" style="width:400px;" />
    	            			&nbsp;&nbsp;<input type="button" data_type="audio" value="删除" id="button_delete" />
    	            		</td>
	            		</tr>
            		    <?php }}else{?>
            			<tr class='nns_audio_config_value'>
	            			<td class="rightstyle"></td>
	            			<td>
    	            			&nbsp;&nbsp;键:&nbsp;&nbsp;<input type="text" name="nns_audio_config[key][]" value="" style="width:250px;" />
    	            			&nbsp;&nbsp;值:&nbsp;&nbsp;<input type="text" name="nns_audio_config[value][]" value="" style="width:250px;" />
    	            			&nbsp;&nbsp;描述:&nbsp;&nbsp;<input type="text" name="nns_audio_config[desc][]" value="" style="width:400px;" />
    	            			&nbsp;&nbsp;<input type="button" data_type="audio" value="删除" id="button_delete" />
    	            		</td>
	            		</tr>
	            		<?php }?>
	            		<tr>
	            			<td class="rightstyle"></td>
	            			<td>
    	            			&nbsp;&nbsp;<a href="javascript:add_config('audio');">+添加配置+</a>
    	            		</td>
	            		</tr>
	            		
            			<tr>
            				<td class="rightstyle">其他扩展配置:</td>
            				<td>
            				    
            				</td>
            			</tr>
            			<?php 
            		    if($action == 'edit' && isset($edit_data['nns_other_config']) && !empty($edit_data['nns_other_config']) && is_array($edit_data['nns_other_config'])){
            		      foreach ($edit_data['nns_other_config'] as $nns_other_config_key=>$nns_other_config){
            		    ?>
            		    <tr class='nns_other_config_value'>
	            			<td class="rightstyle"></td>
	            			<td>
    	            			&nbsp;&nbsp;键:&nbsp;&nbsp;<input type="text" name="nns_other_config[key][]" value="<?php echo $nns_other_config_key;?>" style="width:250px;" />
    	            			&nbsp;&nbsp;值:&nbsp;&nbsp;<input type="text" name="nns_other_config[value][]" value="<?php echo $nns_other_config['value'];?>" style="width:250px;" />
    	            			&nbsp;&nbsp;描述:&nbsp;&nbsp;<input type="text" name="nns_other_config[desc][]" value="<?php echo $nns_other_config['desc'];?>" style="width:400px;" />
    	            			&nbsp;&nbsp;<input type="button" data_type="other" value="删除" id="button_delete" />
    	            		</td>
	            		</tr>
            		    <?php }}else{?>
            			<tr class='nns_other_config_value'>
	            			<td class="rightstyle"></td>
	            			<td>
    	            			&nbsp;&nbsp;键:&nbsp;&nbsp;<input type="text" name="nns_other_config[key][]" value="" style="width:250px;" />
    	            			&nbsp;&nbsp;值:&nbsp;&nbsp;<input type="text" name="nns_other_config[value][]" value="" style="width:250px;" />
    	            			&nbsp;&nbsp;描述:&nbsp;&nbsp;<input type="text" name="nns_other_config[desc][]" value="" style="width:400px;" />
    	            			&nbsp;&nbsp;<input type="button" data_type="other" value="删除" id="button_delete" />
    	            		</td>
	            		</tr>
	            		<?php }?>
	            		<tr>
	            			<td class="rightstyle"></td>
	            			<td>
    	            			&nbsp;&nbsp;<a href="javascript:add_config('other');">+添加配置+</a>
    	            		</td>
	            		</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>转码是否支持抽帧:</td>
            				<td>
            				    <input name="nns_drawing_frame" id="nns_drawing_frame" type="radio" value="0" <?php if(!isset($edit_data['nns_drawing_frame']) || $edit_data['nns_drawing_frame'] !='1'){echo "checked";}?>/>  不支持
            					<input name="nns_drawing_frame" id="nns_drawing_frame" type="radio" value="1" <?php if(isset($edit_data['nns_drawing_frame']) && $edit_data['nns_drawing_frame'] =='1'){echo "checked";}?>/>  支持
            				</td>
            			</tr>
            			
            			<tr class='nns_drawing_type'>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>抽帧图片格式:</td>
            				<td>
            				    <?php if(is_array($result_project_key)){foreach ($result_project_key as $img_key=>$img_val){?>
            				        <input name="nns_drawing_type[<?php echo $img_key;?>]" id="nns_drawing_type"  type="checkbox" value="<?php echo $img_val;?>" <?php if(isset($edit_data['nns_drawing_type'][$img_key]) && $edit_data['nns_drawing_type'][$img_key] == $img_val){ echo "checked";}?>/>  <?php echo $img_val;?>
            				    <?php }}else{ echo "<font color='red'>全部配置里面没配置任何抽帧数据</font>";}?>
            				</td>
            			</tr>
            			
            			<tr class='nns_drawing_type'>
            				<td class="rightstyle">图片扩展名称:</td>
            				<td>
            				    <select name='nns_img_type' style="width:550px;">
                                    <option value="jpg" <?php if(isset($edit_data['nns_img_type']) && $edit_data['nns_img_type'] == 'jpg'){ echo 'selected="selected"';}?>>JPG</option>
                                    <option value="png" <?php if(isset($edit_data['nns_img_type']) && $edit_data['nns_img_type'] == 'png'){ echo 'selected="selected"';}?>>PNG</option>
                                    <option value="gif" <?php if(isset($edit_data['nns_img_type']) && $edit_data['nns_img_type'] == 'gif'){ echo 'selected="selected"';}?>>GIF</option>
                                </select>
            				</td>
            			</tr>
            			<tr class='nns_drawing_type'>
            				<td class="rightstyle">抽帧时间规则(秒):</td>
            				<td><input style="width:540px;"  name="nns_drawing_rule" id="nns_drawing_rule" type="text"  value="<?php if(isset($edit_data["nns_drawing_rule"])){echo $edit_data["nns_drawing_rule"];}else{ echo '0';}?>"/></td>
            			</tr>
            			<tr>
            				<td class="rightstyle">状态:</td>
            				<td>
            				    <select name='nns_state' style="width:550px;">
                                    <option value="0" <?php if(isset($edit_data['nns_state']) && $edit_data['nns_state'] == '0'){ echo 'selected="selected"';}?>>启用</option>
                                    <option value="1" <?php if(isset($edit_data['nns_state']) && $edit_data['nns_state'] == '1'){ echo 'selected="selected"';}?>>未启用</option>
                                </select>
            				</td>
            			</tr>
            		  </tbody>
            		</table>
            	</div>
        	</form>
        	<div class="controlbtns">
        		<div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','<?php echo cms_get_lang('webkz_qrccxg'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
        		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        		<div style="clear:both;"></div>
        	</div>
        </div>
        <script type="text/javascript">
	        $(document).ready(function() {
		        <?php if($action == 'edit'){?>
				var key_type = $("#nns_key_type").attr('value');
				if(key_type == '1')
				{
					$(".c_key").parent().show('slow');
					$(".button_add").parent().parent().show('slow');
				}
				<?php }?>				
			});
			$("#nns_key_type").change(function(){
				var key_type = $("#nns_key_type").attr('value');
				if(key_type == '1')
				{
					$(".c_key").parent().show('slow');
					$(".button_add").parent().parent().show('slow');
				}
				else
				{
					$(".c_key").parent().hide();
					$(".button_add").parent().parent().hide();
				}	
			})
			$(".button_add").click(function(){
				var obj = $('.tr_tbody .tr_td');
	            var copy = obj.eq(0).parents('tr').clone();
	            var input_length = obj.length;
	            var max_num = 10;
	            if (input_length >= max_num) 
	            {
	                alert('数量超出限制 > ' + max_num);
	                return;
	            }
	            var td_name = "选择框内容-" + (input_length + 1) + ":";
	            copy.find('td').eq(0).html(td_name);
	            copy.find('td').eq(1).find("input[type='text']").attr('value','');
	            copy.find('.button_del').attr('data-change', input_length+1);
	            $(this).parents('tr').before(copy.removeClass().show('slow'));
	            tr_order();
	            window.parent.resetFrameHeight();
			})
			//点击删除
            $(".button_del").live("click", function(){
            	var obj = $(this);
                var data = obj.attr('data-change');
                switch (data)
                {
                    case '1':
                    	obj.parent().find("input[type='text']").attr("value","");
                        break;
                    default :
                    	obj.val("").parent().parent().remove();
                        reset_num_after_delete();
                        break;
                }
            })
            function tr_order() 
            {
                var class_name = 'tr_odd';
                $("tr:even").removeClass(class_name);
                $("tr:odd").addClass(class_name);
            }
			//重置数字
            function reset_num_after_delete()
            {
                var obj = $('.tr_tbody .tr_td')
                var len = obj.length;
                if (len < 2)
                {
                    return ;
                }
                for (var i = 0; i < len ; i++)
                {
                    var item = obj.eq(i).parent();
                    item.find('td').eq(0).html('选择框内容-'+(i+1)+":");
                    item.find('.button_del').attr('data-change',i+1);
                }
            }
        </script>
    </body>
</html>
