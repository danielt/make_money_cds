<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$action=$_GET["action"];
$nns_id=$_REQUEST["nns_id"];
$pri_bool=false;
if (!empty($action)){
	switch($action){
	case "edit":
		$action_str=cms_get_lang('save');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136002");
		break;
	case "add":
		$action_str=cms_get_lang('add');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136003");
		break;
	default:
		break;
	}
}
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
    "db_policy" => NL_DB_WRITE,
    "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);
$edit_data = $_REQUEST;






/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/transcode/transcode_manufacturer.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/sp/sp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';

$result_cp = nl_cp::query_all($dc);
if($result_cp['ret'] != 0)
{
    echo '<script>alert("'.$result_cp['reason'].'");history.go(-1);</script>';die;
}
$result_cp = isset($result_cp['data_info']) ? $result_cp['data_info'] : null;
$arr_cp_info = array();
if(is_array($result_cp) && !empty($result_cp))
{
    foreach ($result_cp as $val)
    {
        $arr_temp_config = isset($val['nns_config'])?json_decode($val['nns_config'],true):null;
        if(!isset($arr_temp_config['cp_transcode_file_enabled']) || $arr_temp_config['cp_transcode_file_enabled']!=1)
        {
            continue;
        }
        $arr_cp_info[$val['nns_id']] = $val['nns_name'];
    }
}

if ($action=="edit")
{
    $edit_data = nl_transcode_manufacturer::query_by_id($dc,$nns_id);

    if($edit_data['ret'] != 0 )
    {
        echo "<script>alert('". $action_str. $edit_data['reason']. "');</script>";
        echo "<script>self.location='".$base_file_name_base."_list.php;</script>";
    }
    $edit_data = $edit_data['data_info'];
    $edit_data['nns_api_addr'] = (isset($edit_data['nns_api_addr']) && strlen($edit_data['nns_api_addr'])>0) ? json_decode($edit_data['nns_api_addr'],true) : null;
} 
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link href="../../css/comm.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
        <script language="javascript">
            $(document).ready(function(){
            	change();
            	$('input[type=radio][name=nns_type]').change(function(){
                	change();
            	});
            });
            function change()
            {
            	var nns_type = $("input[name='nns_type']:checked").val();
            	if(nns_type == '0')
                {
            		$('.nns_api_addr_video').show();
            		$('.nns_api_addr_live').hide();
            		$('#nns_api_addr_video').attr('rule','noempty');
            		$('#nns_api_addr_live').attr('rule','');
                }
            	else if(nns_type == '1')
                {
            		$('.nns_api_addr_video').hide();
            		$('.nns_api_addr_live').show();
            		$('#nns_api_addr_video').attr('rule','');
            		$('#nns_api_addr_live').attr('rule','noempty');
                }
            	else
                {
            		$('.nns_api_addr_video').show();
            		$('.nns_api_addr_live').show();
            		$('#nns_api_addr_video').attr('rule','noempty');
            		$('#nns_api_addr_live').attr('rule','noempty');
                }
            }
        </script>
    </head>
    <body>
        <div class="content">
        	<div class="content_position">转码厂家 > 添加/修改</div>
        	<form id="add_form" action="<?php echo $base_file_name_base;?>_control.php" method="post">
            	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
            	<div class="content_table">
            		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            		  <tbody class="tr_tbody">
            		    <?php 
            		    if($action == 'edit'){
            		    ?>
                		    <tr>
                				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>GUID:</td>
                				<td>
                				    <input name="nns_id" id="nns_id" type="hidden"  value="<?php if(isset($edit_data["nns_id"])){echo $edit_data["nns_id"];}?>" rule="noempty"/>
                				    <input type="text" style="width:540px;"  disabled value="<?php if(isset($edit_data["nns_id"])){echo $edit_data["nns_id"];}?>" />
                				</td>
                			</tr>
            		    <?php }?>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>转码厂家:</td>
            				<td><input style="width:540px;" name="nns_name" id="nns_name" type="text"  value="<?php if(isset($edit_data["nns_name"])){echo $edit_data["nns_name"];}?>" rule="noempty"/></td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>标示(创建文件夹):</td>
            				<td><input style="width:540px;" name="nns_mark" id="nns_mark" type="text" <?php if(isset($edit_data["nns_mark"])){echo 'readonly';}?> value="<?php if(isset($edit_data["nns_mark"])){echo $edit_data["nns_mark"];}?>" rule="noempty"  /></td>
            			</tr>
            			<?php 
            		    if($action == 'edit'){
            		    ?>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>厂家支持转码类型:</td>
            				<td>
            				    <input name="nns_type" id="nns_type" type="radio" value="0" <?php if(isset($edit_data["nns_type"]) && $edit_data['nns_type'] == '0'){echo "checked";}?>/>  点播
            					<input name="nns_type" id="nns_type" type="radio" value="1" <?php if(isset($edit_data["nns_type"]) && $edit_data['nns_type'] == '1'){echo "checked";}?>/>  直播
            					<input name="nns_type" id="nns_type" type="radio" value="2" <?php if(isset($edit_data["nns_type"]) && $edit_data['nns_type'] == '2'){echo "checked";}?>/>  点播、直播
            				</td>
            			</tr>
            			<?php }else{?>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>厂家支持转码类型:</td>
            				<td>
            				    <input name="nns_type" id="nns_type" type="radio" value="0" <?php echo "checked";?>/>  点播
            					<input name="nns_type" id="nns_type" type="radio" value="1" />  直播
            					<input name="nns_type" id="nns_type" type="radio" value="2" />  点播、直播
            				</td>
            			</tr>
            			<?php }?>
            			<tr class='nns_api_addr_video'>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>API点播请求地址:</td>
            				<td><input style="width:540px;" name="nns_api_addr[video]" id="nns_api_addr_video" type="text" value="<?php if(isset($edit_data["nns_api_addr"]['video'])){echo $edit_data["nns_api_addr"]['video'];}?>" rule="noempty"  /></td>
            			</tr>
            			<tr class='nns_api_addr_live'>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>API直播请求地址:</td>
            				<td><input style="width:540px;" name="nns_api_addr[live]" id="nns_api_addr_live" type="text" value="<?php if(isset($edit_data["nns_api_addr"]['live'])){echo $edit_data["nns_api_addr"]['live'];}?>" rule="noempty"  /></td>
            			</tr>
            			<tr>
            				<td class="rightstyle">状态:</td>
            				<td>
            				    <select name='nns_state' style="width:550px;" >
                                    <option value="0" <?php if(isset($edit_data['nns_state']) && $edit_data['nns_state'] == '0'){ echo 'selected="selected"';}?>>启用</option>
                                    <option value="1" <?php if(isset($edit_data['nns_state']) && $edit_data['nns_state'] == '1'){ echo 'selected="selected"';}?>>未启用</option>
                                </select>
            				</td>
            			</tr>
            		  </tbody>
            		</table>
            	</div>
        	</form>
        	<div class="controlbtns">
        		<div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','<?php echo cms_get_lang('webkz_qrccxg'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
        		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        		<div style="clear:both;"></div>
        	</div>
        </div>
        <script type="text/javascript">
	        $(document).ready(function() {
		        <?php if($action == 'edit'){?>
				var key_type = $("#nns_key_type").attr('value');
				if(key_type == '1')
				{
					$(".c_key").parent().show('slow');
					$(".button_add").parent().parent().show('slow');
				}
				<?php }?>				
			});
			$("#nns_key_type").change(function(){
				var key_type = $("#nns_key_type").attr('value');
				if(key_type == '1')
				{
					$(".c_key").parent().show('slow');
					$(".button_add").parent().parent().show('slow');
				}
				else
				{
					$(".c_key").parent().hide();
					$(".button_add").parent().parent().hide();
				}	
			})
			$(".button_add").click(function(){
				var obj = $('.tr_tbody .tr_td');
	            var copy = obj.eq(0).parents('tr').clone();
	            var input_length = obj.length;
	            var max_num = 10;
	            if (input_length >= max_num) 
	            {
	                alert('数量超出限制 > ' + max_num);
	                return;
	            }
	            var td_name = "选择框内容-" + (input_length + 1) + ":";
	            copy.find('td').eq(0).html(td_name);
	            copy.find('td').eq(1).find("input[type='text']").attr('value','');
	            copy.find('.button_del').attr('data-change', input_length+1);
	            $(this).parents('tr').before(copy.removeClass().show('slow'));
	            tr_order();
	            window.parent.resetFrameHeight();
			})
			//点击删除
            $(".button_del").live("click", function(){
            	var obj = $(this);
                var data = obj.attr('data-change');
                switch (data)
                {
                    case '1':
                    	obj.parent().find("input[type='text']").attr("value","");
                        break;
                    default :
                    	obj.val("").parent().parent().remove();
                        reset_num_after_delete();
                        break;
                }
            })
            function tr_order() 
            {
                var class_name = 'tr_odd';
                $("tr:even").removeClass(class_name);
                $("tr:odd").addClass(class_name);
            }
			//重置数字
            function reset_num_after_delete()
            {
                var obj = $('.tr_tbody .tr_td')
                var len = obj.length;
                if (len < 2)
                {
                    return ;
                }
                for (var i = 0; i < len ; i++)
                {
                    var item = obj.eq(i).parent();
                    item.find('td').eq(0).html('选择框内容-'+(i+1)+":");
                    item.find('.button_del').attr('data-change',i+1);
                }
            }
        </script>
    </body>
</html>
