<?php 
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) 
{
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -5);
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
		"db_policy" => NL_DB_WRITE,
		"cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));



/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/transcode/transcode_manufacturer_model.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/vod_media/vod_media.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/transcode/transcode_model.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/transcode/transcode_manufacturer.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/sp/sp.class.php';

$result_cp = nl_cp::query_all($dc);
if($result_cp['ret'] != 0)
{
    echo '<script>alert("'.$result_cp['reason'].'");history.go(-1);</script>';die;
}
$result_cp = isset($result_cp['data_info']) ? $result_cp['data_info'] : null;
$arr_cp_info = array();
if(is_array($result_cp) && !empty($result_cp))
{
    foreach ($result_cp as $val)
    {
        $arr_temp_config = isset($val['nns_config'])?json_decode($val['nns_config'],true):null;
        if(!isset($arr_temp_config['cp_transcode_file_enabled']) || $arr_temp_config['cp_transcode_file_enabled']!=1)
        {
            continue;
        }
        $arr_cp_info[$val['nns_id']] = $val['nns_name'];
    }
}


$result_sp = nl_sp::query_all($dc);
if($result_sp['ret'] != 0)
{
    echo '<script>alert("'.$result_sp['reason'].'");history.go(-1);</script>';die;
}
$result_sp = isset($result_sp['data_info']) ? $result_sp['data_info'] : null;
$arr_sp_info = array();
if(is_array($result_sp) && !empty($result_sp))
{
    foreach ($result_sp as $val)
    {
        $arr_temp_config = isset($val['nns_config'])?json_decode($val['nns_config'],true):null;
        if(!isset($arr_temp_config['clip_file_encode_enable']) || $arr_temp_config['clip_file_encode_enable']!=1)
        {
            continue;
        }
        $arr_sp_info[$val['nns_id']] = $val['nns_name'];
    }
}

$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 18;
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) 
{
	$page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit_array = array(
		'page_size'=>$page_size,
		'page_num'=> $page_num,
);
if (isset($_REQUEST['nns_manufacturer_id']) && (strlen($_REQUEST['nns_manufacturer_id']) >0))
{
    $filter_array['where']['nns_manufacturer_id'] = $_REQUEST['nns_manufacturer_id'];
}
if (isset($_REQUEST['nns_state']) && (strlen($_REQUEST['nns_state']) >0))
{
    $filter_array['where']['nns_state'] = $_REQUEST['nns_state'];
}
if (isset($_REQUEST['nns_model_id']) && (strlen($_REQUEST['nns_model_id']) >0))
{
    $filter_array['where']['nns_model_id'] = $_REQUEST['nns_model_id'];
}
if (isset($_REQUEST['nns_cp_id']) && (strlen($_REQUEST['nns_cp_id']) >0))
{
    $filter_array['where']['nns_cp_id'] = $_REQUEST['nns_cp_id'];
}
if (isset($_REQUEST['nns_sp_id']) && (strlen($_REQUEST['nns_sp_id']) >0))
{
    $filter_array['where']['nns_sp_id'] = $_REQUEST['nns_sp_id'];
}
if (isset($_REQUEST['nns_input_definition']) && (strlen($_REQUEST['nns_input_definition']) >0))
{
    $filter_array['where']['nns_input_definition'] = $_REQUEST['nns_input_definition'];
}
if (isset($_REQUEST['nns_output_definition']) && (strlen($_REQUEST['nns_output_definition']) >0))
{
    $filter_array['where']['nns_output_definition'] = $_REQUEST['nns_output_definition'];
}
if (isset($_REQUEST['nns_input_file_type']) && (strlen($_REQUEST['nns_input_file_type']) >0))
{
    $filter_array['where']['nns_input_file_type'] = $_REQUEST['nns_input_file_type'];
}
if (isset($_REQUEST['nns_output_file_type']) && (strlen($_REQUEST['nns_output_file_type']) >0))
{
    $filter_array['where']['nns_output_file_type'] = $_REQUEST['nns_output_file_type'];
}
if (isset($_REQUEST['nns_state']) && (strlen($_REQUEST['nns_state']) >0))
{
    $filter_array['where']['nns_state'] = $_REQUEST['nns_state'];
}
if (isset($_REQUEST['nns_output_file_tag']) && (strlen($_REQUEST['nns_output_file_tag']) >0))
{
    $filter_array['like']['nns_output_file_tag'] = $_REQUEST['nns_output_file_tag'];
}


$result_data = nl_transcode_model::query($dc,$filter_array,$limit_array);


$result_manufacturer = nl_transcode_manufacturer::query_all($dc);
$arr_manufacturer  =null;
if(isset($result_manufacturer['data_info']) && !empty($result_manufacturer['data_info']) && is_array($result_manufacturer['data_info']))
{
    foreach ($result_manufacturer['data_info'] as $val_manufacturer)
    {
        $arr_manufacturer[$val_manufacturer['nns_id']] = $val_manufacturer['nns_name'];
    }
}
$arr_definition = array(
    'std'=>'标清',
    'hd'=>'高清',
    'sd'=>'超高清',
    '4k'=>'4K',
    'low'=>'流畅',
    'dubi'=>'杜比',
    'default'=>'默认配置',
);
$arr_filetype = array(
    'ts',
    'm3u8',
    'mp4',
    'default',
);
$result_definition = nl_vod_media_v2::query_group_mode($dc);
if(isset($result_definition['data_info']) && !empty($result_definition['data_info']) && is_array($result_definition['data_info']))
{
    $arr_definition_keys = array_keys($arr_definition);
    foreach ($result_definition['data_info'] as $val)
    {
        if(!in_array($val['nns_mode'], $arr_definition_keys))
        {
            $arr_definition[$val['nns_mode']] = $val['nns_mode'];
        }
    }
}
$result_filetype = nl_vod_media_v2::query_group_filetype($dc);
if(isset($result_filetype['data_info']) && !empty($result_filetype['data_info']) && is_array($result_filetype['data_info']))
{
    foreach ($result_filetype['data_info'] as $val)
    {
        if(!in_array($val['nns_filetype'], $arr_filetype))
        {
            $arr_filetype[] = $val['nns_filetype'];
        }
    }
}
if($result_data['ret'] != 0)
{
	echo '<script>alert("'.$result_data['reason'].'");history.go(-1);</script>';die;
}

$vod_total_num = isset($result_data['page_info']['total_count']) ? $result_data['page_info']['total_count'] : 0;
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) {
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;

$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);

?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link href="../../css/public.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
		<script language="javascript">
			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				})
				return BoxKey;
			}
			function change_state(id,state) {
				var url = "<?php echo $base_file_name_base;?>_control.php?action=state&nns_id="+id+"&nns_state="+state;
				window.location.href = url;
			}
			function delete_id(){
				var r=confirm("是否进行批量删除操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "<?php echo $base_file_name_base;?>_control.php?action=delete&nns_id="+ids;
						window.location.href = url;
					}
				}
			}
		</script>
	</head>
	<body>
		<div class="content">
		    <!-- 栏目导航描述div -->
			<div class="content_position">转码策略列表</div>
			<!-- 表单查询div -->
			<div class="content_table">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<form action="<?php echo $base_file_name;?>" method="get">
					    <input type="hidden" name="view_list_max_num" id="view_list_max_num" value="<?php echo $g_manager_list_max_num; ?>" style="width:150px;">
						<tbody>
							<tr>
								<td>
									
                                    &nbsp;&nbsp;CP ID:
                                        <select name='nns_cp_id' style="width:200px;">
                                            <option value=''>全部</option>
                                            <?php if(is_array($arr_cp_info) && is_array($arr_cp_info)){foreach ($arr_cp_info as $cp_key=>$cp_val){?>
                                            <option value="<?php echo $cp_key;?>" <?php if(isset($_GET['nns_cp_id']) && $_GET['nns_cp_id'] == $cp_key){ echo 'selected="selected"';}?>><?php echo $cp_key.' / '.$cp_val;?></option>
                                            <?php }}?>
                                        </select>
                                    &nbsp;&nbsp;SP ID:
                                        <select name='nns_sp_id' style="width:200px;">
                                            <option value=''>全部</option>
                                            <?php if(is_array($arr_sp_info) && is_array($arr_sp_info)){foreach ($arr_sp_info as $sp_key=>$sp_val){?>
                                            <option value="<?php echo $sp_key;?>" <?php if(isset($_GET['nns_sp_id']) && $_GET['nns_sp_id'] == $sp_key){ echo 'selected="selected"';}?>><?php echo $sp_key.' / '.$sp_val;?></option>
                                            <?php }}?>
                                        </select>
                                    &nbsp;&nbsp;转码厂家:
                                        <select name='nns_manufacturer_id' style="width:200px;">
                                            <option value=''>全部</option>
                                            <?php if(is_array($arr_manufacturer) && !empty($arr_manufacturer)){foreach ($arr_manufacturer as $cp_key=>$cp_val){?>
                                            <option value="<?php echo $cp_key;?>" <?php if(isset($_GET['nns_manufacturer_id']) && $_GET['nns_manufacturer_id'] == $cp_key){ echo 'selected="selected"';}?>><?php echo $cp_val;?></option>
                                            <?php }}?>
                                        </select>
                                    &nbsp;&nbsp;策略名称:
										<input type="text" name="nns_name" id=""nns_name"" value="<?php echo $_GET['"nns_name"']; ?>" style="width:200px;">
								    &nbsp;&nbsp;输入清晰度:
                                        <select name='nns_input_definition' style="width:100px;">
                                            <option value=''>全部</option>
                                            <?php if(is_array($arr_definition) && !empty($arr_definition)){foreach ($arr_definition as $cp_key=>$cp_val){?>
                                            <option value="<?php echo $cp_key;?>" <?php if(isset($_GET['nns_input_definition']) && $_GET['nns_input_definition'] == $cp_key){ echo 'selected="selected"';}?>><?php echo $cp_val;?></option>
                                            <?php }}?>
                                        </select>
                                    &nbsp;&nbsp;输出清晰度:
                                        <select name='nns_output_definition' style="width:100px;">
                                            <option value=''>全部</option>
                                            <?php if(is_array($arr_definition) && !empty($arr_definition)){foreach ($arr_definition as $cp_key=>$cp_val){
                                                if($cp_key == 'default'){
                                                    continue;
                                                }
                                            ?>
                                            <option value="<?php echo $cp_key;?>" <?php if(isset($_GET['nns_output_definition']) && $_GET['nns_output_definition'] == $cp_key){ echo 'selected="selected"';}?>><?php echo $cp_val;?></option>
                                            <?php }}?>
                                        </select>
                                    <br/>
                                    &nbsp;&nbsp;输入文件格式:
                                        <select name='nns_input_file_type' style="width:100px;">
                                            <option value=''>全部</option>
                                            <?php if(is_array($arr_filetype) && !empty($arr_filetype)){foreach ($arr_filetype as $cp_val){?>
                                            <option value="<?php echo $cp_val;?>" <?php if(isset($_GET['nns_input_file_type']) && $_GET['nns_input_file_type'] == $cp_val){ echo 'selected="selected"';}?>><?php echo $cp_val;?></option>
                                            <?php }}?>
                                        </select>
                                    &nbsp;&nbsp;输出文件格式:
                                        <select name='nns_output_file_type' style="width:100px;">
                                            <option value=''>全部</option>
                                            <?php if(is_array($arr_filetype) && !empty($arr_filetype)){foreach ($arr_filetype as $cp_val){
                                                if($cp_val == 'default'){
                                                    continue;
                                                }
                                            ?>
                                            <option value="<?php echo $cp_val;?>" <?php if(isset($_GET['nns_output_file_type']) && $_GET['nns_output_file_type'] == $cp_val){ echo 'selected="selected"';}?>><?php echo $cp_val;?></option>
                                            <?php }}?>
                                        </select>
                                    &nbsp;&nbsp;输出tag:
                                        <select name='nns_output_file_tag' style="width:100px;">
                                            <option value=''>全部</option>
                                            <option value="26," <?php if(isset($_GET['nns_output_file_tag']) && $_GET['nns_output_file_tag'] == '26,'){ echo 'selected="selected"';}?>> STB </option>
                                            <option value="27," <?php if(isset($_GET['nns_output_file_tag']) && $_GET['nns_output_file_tag'] == '27,'){ echo 'selected="selected"';}?>> IPHONE </option>
                                            <option value="28," <?php if(isset($_GET['nns_output_file_tag']) && $_GET['nns_output_file_tag'] == '28,'){ echo 'selected="selected"';}?>> IPAD </option>
                                            <option value="29," <?php if(isset($_GET['nns_output_file_tag']) && $_GET['nns_output_file_tag'] == '29,'){ echo 'selected="selected"';}?>> ANDROID PHONE </option>
                                            <option value="30," <?php if(isset($_GET['nns_output_file_tag']) && $_GET['nns_output_file_tag'] == '30,'){ echo 'selected="selected"';}?>> ANDROID PAD </option>
                                            <option value="31," <?php if(isset($_GET['nns_output_file_tag']) && $_GET['nns_output_file_tag'] == '31,'){ echo 'selected="selected"';}?>> PC </option>
                                        </select>
                                    &nbsp;&nbsp;状态:
                                        <select name='nns_state' style="width:100px;">
                                            <option value=''>全部</option>
                                            <option value="0" <?php if(isset($_GET['nns_state']) && $_GET['nns_state'] == '0'){ echo 'selected="selected"';}?>>启用</option>
                                            <option value="1" <?php if(isset($_GET['nns_state']) && $_GET['nns_state'] == '1'){ echo 'selected="selected"';}?>>未启用</option>
                                        </select>
                                    &nbsp;&nbsp;转码类型:
                                        <select name='nns_type' style="width:100px;">
                                            <option value=''>全部</option>
                                            <option value="0" <?php if(isset($_GET['nns_type']) && $_GET['nns_type'] == '0'){ echo 'selected="selected"';}?>>点播</option>
                                            <option value="1" <?php if(isset($_GET['nns_type']) && $_GET['nns_type'] == '1'){ echo 'selected="selected"';}?>>直播</option>
                                        </select>
									&nbsp;&nbsp;<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  />										
								</td>
							</tr>
						</tbody>
					</form>
				</table>
			</div>			
			<!-- 内容列表 DIV -->
			<div class="content_table formtable">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th width='40px'><input name="" type="checkbox" value="">序号</th>
							<th>策略名称</th>
							<th>厂家模板</th>
							<th>CP ID</th>
							<th>SP ID</th>
							<th>转码类型</th>
							<th>输入清晰度</th>
							<th>输出清晰度</th>
							<th>输入文件类型</th>
							<th>输出文件类型</th>
							<th>输出TAG</th>
							<th>开始码率</th>
							<th>结束码率</th>
							<th>输出码率</th>
							<th>抽帧配置</th>
							<th>状态</th>
							<th>修改时间</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(is_array($result_data['data_info']) && !empty($result_data['data_info']))
						{
							$num = ($page_num - 1) * $page_size;
							foreach ($result_data['data_info'] as $item) 
							{
								$num++;
						?>
						      <tr>
						          <td><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
						          <td><?php echo $item['nns_name']; ?></td>
						          <td>
						              <?php 
						                  echo "厂家:";
						                  echo (isset($arr_manufacturer[$item['transcode_manufacturer_id']])) ? $arr_manufacturer[$item['transcode_manufacturer_id']] : '<font color="red">未知厂家</font>';
						                  echo "<br/>模板:{$item['transcode_manufacturer_model_name']}";
						              ?>
						          </td>
							      <td><?php echo isset($arr_cp_info[$item['nns_cp_id']]) ? $item['nns_cp_id'].'/'.$arr_cp_info[$item['nns_cp_id']] : "<font color='red'>{$item['nns_cp_id']} / 未知CP/SP</font>"; ?></td>
							      <td><?php echo isset($arr_sp_info[$item['nns_sp_id']]) ? $item['nns_sp_id'].'/'.$arr_sp_info[$item['nns_sp_id']] : "<font color='red'>{$item['nns_sp_id']} / 未知CP/SP</font>"; ?></td>
							      <td><?php if($item['nns_type'] == '1'){ echo "直播";}else{ echo "点播";} ?></td>
							      <td><?php echo $item['nns_input_definition']; ?></td>
							      <td><?php echo $item['nns_output_definition']; ?></td>
							      <td><?php echo $item['nns_input_file_type']; ?></td>
							      <td><?php echo $item['nns_output_file_type']; ?></td>
							      <td><?php echo $item['nns_output_file_tag']; ?></td>
							      <td><?php echo $item['nns_start_bitrate']; ?></td>
							      <td><?php echo $item['nns_end_bitrate']; ?></td>
							      <td><?php echo $item['nns_output_bitrate']; ?></td>
							      <td><?php 
							         if($item['nns_drawing_frame'] == '1')
							         {
							             echo "<font>支持抽帧配置</font><hr/>";
							             echo "<font>抽帧图片格式:{$item['nns_drawing_type']}</font><hr/>";
							             echo "<font>图片扩展名称:{$item['nns_img_type']}</font><hr/>";
							             echo "<font>抽帧时间规则(秒):{$item['nns_drawing_rule']}</font>";
							         }
							         else
							         {
							             echo "<font color='blue'>不支持抽帧配置</font>";
							         }
							      ?></td>
							      <td><a href="javascript:change_state('<?php echo $item['nns_id']?>',<?php echo ($item['nns_state'] !=0) ? 0 :1;?>);"><?php echo ($item['nns_state'] !='0') ? '<font color="red">未启用</font>' : '<font color="green">启用</font>'; ?></a></td>
						          <td><?php echo $item['nns_modify_time']; ?></td>
						          <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                                	<a href="<?php echo $base_file_name_base;?>_addedit.php?action=edit&nns_id=<?php echo $item["nns_id"];?>">修改</a>
                                	<a href="<?php echo $base_file_name_base;?>_control.php?action=delete&nns_id=<?php echo $item["nns_id"];?>">删除</a>
                                  </td>
                        </tr>
						<?php
						     }
						}
						$least_num = $g_manager_list_max_num - count($result_data['data_info']);
						for ($i = 0; $i < $least_num; $i++) 
						{
						?>
						  <tr>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						  </tr>
						<?php 
						}
						?>
					</tbody>
				</table>
			</div>
		    <!-- 分页DIV -->
		    <div class="pagecontrol">
		             共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                <?php if ($currentpage > 1) { ?>
                        <a href="<?php $base_file_name; ?>?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php $base_file_name; ?>?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } else { ?>
                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } if ($currentpage < $vod_pages) { ?>
                        <a href="<?php $base_file_name; ?>?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php $base_file_name; ?>?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } else { ?>
                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } ?>
    
                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                <a href="javascript:go_page_num('<?php $base_file_name; ?>?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                <input name="nns_list_max_num" id="nns_list_max_num" type="text" value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>" onclick="refresh_vod_page();"/>&nbsp;&nbsp;
            </div>    
            <div class="controlbtns">
                <div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
                <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
                <div class="controlbtn add"><a href="<?php echo $base_file_name_base;?>_addedit.php?action=add">添加</a></div>
                <div class="controlbtn delete"><a href="javascript:delete_id();">删除</a></div>
                <div style="clear:both;">
            </div>
        </div>    
                        
		</div>
	</body>
</html>
