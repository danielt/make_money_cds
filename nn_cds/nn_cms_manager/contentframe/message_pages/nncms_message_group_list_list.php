<?php 
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -5);
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
		"db_policy" => NL_DB_WRITE,
		"cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));

$cp_id = $_REQUEST['cp_id'];
$sp_id = $_REQUEST['sp_id'];
/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/message/message_group.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/message/message_group_list.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';

$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 18;
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) {
	$page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit_array = array(
		'page_size'=>$page_size,
		'page_num'=> $page_num,
);
$result_cp = nl_cp::query_by_id($dc, $cp_id);
$filter_array['where']['nns_message_group_id'] = $_REQUEST['nns_message_group_id'];
$str_cp_name= isset($result_cp['data_info']['nns_name']) ? $result_cp['data_info']['nns_name'] : 'NULL';
$str_cp_name = $cp_id.'/'.$str_cp_name;
if (isset($_REQUEST['nns_type']) && (strlen($_REQUEST['nns_type']) >0))
{
    $filter_array['where']['nns_type'] = $_REQUEST['nns_type'];
}
if (isset($_REQUEST['nns_video_id']) && (strlen($_REQUEST['nns_video_id']) >0))
{
    $filter_array['where']['nns_video_id'] = $_REQUEST['nns_video_id'];
}
if (isset($_REQUEST['nns_state']) && (strlen($_REQUEST['nns_state']) >0))
{
    $filter_array['where']['nns_state'] = $_REQUEST['nns_state'];
}
if (isset($_REQUEST['create_begin_time']) && (strlen($_REQUEST['create_begin_time']) >0))
{
    $filter_array['where']['create_begin_time'] = $_REQUEST['create_begin_time'];
}
if (isset($_REQUEST['create_end_time']) && (strlen($_REQUEST['create_end_time']) >0))
{
    $filter_array['where']['create_end_time'] = $_REQUEST['create_end_time'];
}
if (isset($_REQUEST['nns_desc']) && (strlen($_REQUEST['nns_desc']) >0))
{
    $filter_array['like']['nns_desc'] = $_REQUEST['nns_desc'];
}
$result_data = nl_message_group_list::query($dc,$filter_array,$limit_array);
if($result_data['ret'] != 0)
{
	echo '<script>alert("'.$result_data['reason'].'");history.go(-1);</script>';die;
}
$result_data_cp = nl_cp::query_all($dc);
if($result_data_cp['ret'] != 0)
{
    echo '<script>alert("'.$result_data_cp['reason'].'");history.go(-1);</script>';die;
}
$vod_total_num = isset($result_data['page_info']['total_count']) ? $result_data['page_info']['total_count'] : 0;
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) {
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;

$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link href="../../css/public.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
		<script language="javascript">
			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				})
				return BoxKey;
			}
			function back_vod_page() {
				var num = $("#nns_list_max_num").val();
				window.location.href='nncms_message_group_list.php?cp_id=<?php echo $cp_id;?>&view_list_max_num='+num;
			}
			function delete_id(){
				var r=confirm("是否进行批量删除操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "<?php echo $base_file_name_base;?>_control.php?action=delete&nns_id=&nns_id="+ids;
						window.location.href = url;
					}
				}
			}
			$(document).ready(function() {
				$('#clear_time').click(function(){
					$('#create_begin_time').val('');
					$('#create_end_time').val('');

				});
				window.parent.now_frame_url = window.location.href;
			});
		</script>
	</head>
	<body>
		<div class="content">
		    <!-- 栏目导航描述div -->
			<div class="content_position">消息组列表 -> 消息组详情列表</div>
			<!-- 表单查询div -->
			<div class="content_table">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<form action="<?php echo $base_file_name;?>" method="get">
					    <input type="hidden" name="view_list_max_num" id="view_list_max_num" value="<?php echo $g_manager_list_max_num; ?>" style="width:150px;">
						<tbody>
							<tr>
								<td>
								   
                                    <input type="hidden" name="nns_message_group_id" id="nns_message_group_id" value="<?php echo $_REQUEST['nns_message_group_id']; ?>" style="width:150px;">
                                    &nbsp;&nbsp;影片GUID:
                                        <input type="text" name="nns_video_id" id="nns_video_id" value="<?php echo $_GET['nns_video_id']; ?>" style="width:150px;">
                                    &nbsp;&nbsp;注入ID:
                                        <input type="text" name="nns_import_id" id="nns_import_id" value="<?php echo $_GET['nns_import_id']; ?>" style="width:150px;">
                                    &nbsp;&nbsp;描述:
                                        <input type="text" name="nns_desc" id="nns_desc" value="<?php echo $_GET['nns_desc']; ?>" style="width:150px;">
                                    &nbsp;&nbsp;影片类型:
                                        <select name='nns_type' style="width:150px;">
                                            <option value=''>全部</option>
                                            <option value="video" <?php if($_GET['nns_type']=='video') echo 'selected="selected"'?>>主媒资</option>
                                            <option value="index" <?php if($_GET['nns_type']=='index') echo 'selected="selected"'?>>分集</option>
                                            <option value="media" <?php if($_GET['nns_type']=='media') echo 'selected="selected"'?>>片源</option>
                                            <option value="live" <?php if($_GET['nns_type']=='live') echo 'selected="selected"'?>>直播频道</option>
                                            <option value="live_index" <?php if($_GET['nns_type']=='live_index') echo 'selected="selected"'?>>直播分集</option>
                                            <option value="live_media" <?php if($_GET['nns_type']=='live_media') echo 'selected="selected"'?>>直播源</option>
                                            <option value="playbill" <?php if($_GET['nns_type']=='playbill') echo 'selected="selected"'?>>节目单</option>
                                        </select>
                                    &nbsp;&nbsp;状态:
                                        <select name='nns_state' style="width:150px;">
                                            <option value=''>全部</option>
                                            <option value="0" <?php if(isset($_GET['nns_state']) && $_GET['nns_state'] == '0'){ echo 'selected="selected"';}?>>队列处理中</option>
                                            <option value="1" <?php if(isset($_GET['nns_state']) && $_GET['nns_state'] == '1'){ echo 'selected="selected"';}?>>处理成功</option>
                                            <option value="2" <?php if(isset($_GET['nns_state']) && $_GET['nns_state'] == '2'){ echo 'selected="selected"';}?>>处理失败</option>
                                        </select>
                                     <hr/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;选择创建时间段：&nbsp;&nbsp;&nbsp;<input name="create_begin_time" id="create_begin_time" type="text"  value="<?php
										if (isset($_GET['day_picker_start']))
											echo $_GET['day_picker_start'];
									?>" style="width:120px;" class="datetimepicker" callback="test" />
										 - <input name="create_end_time" id="create_end_time" type="text"  value="<?php
											if (isset($_GET['create_end_time']))
												echo $_GET['create_end_time'];
										?>" style="width:120px;" class="datetimepicker" callback="test" />
										 &nbsp;&nbsp;<input type="button" id="clear_time" name="clear_time" value="清除时间"/>
									&nbsp;&nbsp;<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  />										
								</td>
							</tr>
						</tbody>
					</form>
				</table>
			</div>			
			<!-- 内容列表 DIV -->
			<div class="content_table formtable">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th><input name="" type="checkbox" value="">序号</th>
							<th>消息组GUID</th>
							<th>CP名称</th>
							<th>影片类型</th>
							<th>影片GUID</th>
							<th>注入ID</th>
							<th>状态</th>
							<th>描述</th>
							<th>创建时间</th>
                            <th>修改时间</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$result_data_cp_last_key = !empty($result_data_cp_last) ? array_keys($result_data_cp_last) : array();
						if(is_array($result_data['data_info']) && !empty($result_data['data_info']))
						{
							$num = ($page_num - 1) * $page_size;
							foreach ($result_data['data_info'] as $item) 
							{
								$num++;
						?>
						      <tr>
						          <td><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
						          <td><?php echo $item['nns_message_group_id']; ?></td>
						          <td><?php echo $str_cp_name; ?></td>
                                  <td><?php echo $item['nns_type']; ?></td>
                                  <td><?php echo $item['nns_video_id']; ?></td>
                                  <td><?php echo $item['nns_import_id']; ?></td>
                                  <td><?php 
                                        if($item['nns_state'] == '0')
                                        {
                                            echo "队列处理中";
                                        }
                                        else if($item['nns_state'] == '1')
                                        {
                                            echo "处理成功";
                                        }
                                        else
                                        {
                                            echo "处理失败";
                                        }
                                        ?></td>
                                  <td><?php echo $item['nns_desc']; ?></td>
                                  <td><?php echo $item['nns_create_time']; ?></td>
                                  <td><?php echo $item['nns_modify_time']; ?></td>
						          
                        </tr>
						<?php
						     }
						}
						$least_num = $g_manager_list_max_num - count($result_data['data_info']);
						for ($i = 0; $i < $least_num; $i++) 
						{
						?>
						  <tr>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						  </tr>
						<?php 
						}
						?>
					</tbody>
				</table>
			</div>
		    <!-- 分页DIV -->
		    <div class="pagecontrol">
		             共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                <?php if ($currentpage > 1) { ?>
                        <a href="<?php $base_file_name; ?>?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php $base_file_name; ?>?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } else { ?>
                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } if ($currentpage < $vod_pages) { ?>
                        <a href="<?php $base_file_name; ?>?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php $base_file_name; ?>?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } else { ?>
                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } ?>
    
                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                <a href="javascript:go_page_num('<?php $base_file_name; ?>?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                <input name="nns_list_max_num" id="nns_list_max_num" type="text" value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>" onclick="refresh_vod_page();"/>&nbsp;&nbsp;
            </div>    
            <div class="controlbtns">
                <div class="controlbtn back" onclick="back_vod_page();"><a >返回</a></div>
                <div style="clear:both;">
            </div>
        </div>    
                        
		</div>
	</body>
</html>
