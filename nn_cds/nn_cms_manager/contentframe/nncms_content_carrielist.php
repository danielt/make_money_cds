<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../nncms_manager_inc.php");
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
session_start();
$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"100100");
if (!$pri_bool) Header("Location: nncms_content_wrong.html");


 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
 include($nncms_db_path. "nns_role/nns_db_role_class.php");
 $role_inst=new nns_db_role_class();
 $countArr=$role_inst->nns_db_role_count();
 if ($countArr["ret"]==0)
 $role_total_num=$countArr["data"][0]["num"];
 $role_pages=ceil($role_total_num/$g_manager_list_max_num);
 $currentpage=1;
 if (!empty($_GET["page"])) $currentpage=$_GET["page"];
 $role_array=$role_inst->nns_db_role_mgr_num(($currentpage-1)*$g_manager_list_max_num,$g_manager_list_max_num);
// var_dump($role_array);
 if ($role_array["ret"]!=0){
 	$data=null;
 	echo "<script>alert(". $role_array["reason"].");</script>";
 }
 $data=$role_array["data"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/table.js"></script>
<script language="javascript" src="../js/checkinput.js"></script>
<script language="javascript">
function checkhiddenInput(){
	var par=getAllCheckBoxSelect();
	if (par==""){
		alert("<?php echo cms_get_lang('select_role')?>");
	}else{
		$('#nns_id').attr('value',getAllCheckBoxSelect());
		checkForm('<?php echo cms_get_lang('xtgl_jsgl')?>','<?php echo cms_get_lang('is_delete_role')?>',$('#delete_form'));
	}

}
</script>
</head>

<body>
<div class="content">
	<div class="content_position"><?php echo cms_get_lang('xtgl_manager')?>> <?php echo cms_get_lang('xtgl_jsgl')?></div>
   <!-- <div class="controlbtns">
    	<div class="controlbtn edit"><a href="javascript:void(null);">编辑</a></div>
        <div class="controlbtn allselect"><a href="javascript:void(null);">全选</a></div>
        <div class="controlbtn cancel"><a href="javascript:void(null);">取消</a></div>
        <div class="controlbtn delete"><a href="javascript:void(null);">删除</a></div>
        <div style="clear:both;"></div>
    </div> -->
    <div class="content_table formtable">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
        	<tr>
            	<th><input name="" type="checkbox" value="" /></th>
                <th><?php echo cms_get_lang('id')?></th>
                <th><?php echo cms_get_lang('name')?></th>
                <th><?php echo cms_get_lang('create_time')?></th>
                <th><?php echo cms_get_lang('edit_time')?></th>
                <th><?php echo cms_get_lang('role_manager')?></th>
            </tr>
        </thead>
        <tbody>
        <?php
        	if ($data!=null){
        	 $num=($currentpage-1)*$g_manager_list_max_num; foreach($data as $item){
        	$num++;
        	?>
              <tr>
                <td><input name="input" type="checkbox" value="<?php echo  $item["nns_id"];?>" /></td>
                <td><?echo $num;?></td>
                <td><?echo $item["nns_name"];?></td>
                <td><?echo $item["nns_create_time"];?> </td>
                <td><?echo $item["nns_modify_time"];?> </td>
                <td><?echo $item["mgr_num"];?></td>
              </tr>
          <?php }}?>
          </tbody>
        </table>
    </div>
     <div class="pagecontrol">
     	<?php if ($currentpage>1){?>
     	<a href="nncms_content_rolelist.php?page=1" target="_self"><?php echo cms_get_lang('first_page')?></a>&nbsp;&nbsp;&nbsp;&nbsp;
    	<a href="nncms_content_rolelist.php?page=<?php echo $currentpage-1;?>" target="_self"><?php echo cms_get_lang('pre_page')?></a>&nbsp;&nbsp;&nbsp;&nbsp;
    	<?php } if ($currentpage<$role_pages){?>
        <a href="nncms_content_rolelist.php?page=<?php echo $currentpage+1;?>" target="_self"><?php echo cms_get_lang('next_page')?></a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="nncms_content_rolelist.php?page=<?php echo $role_pages;?>" target="_self"><?php echo cms_get_lang('last_page')?></a>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php }?>
    </div>
    <div class="controlbtns">
    	<div class="controlbtn edit"><a href="javascript:void(null);" pos="nncms_content_role_addedit.php" action="edit"><?php echo cms_get_lang('editing')?></a></div>
        <div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);"><?php echo cms_get_lang('select')?></a></div>
        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);"><?php echo cms_get_lang('cancel')?></a></div>
        <div class="controlbtn delete"><a href="javascript:checkhiddenInput();"  action="delete"><?php echo cms_get_lang('delete')?></a></div>
        <div class="controlbtn delete"><a href="javascript:void(null);" pos="nncms_content_role_addedit.php" action="add"><?php echo cms_get_lang('add')?></a></div>
        <div style="clear:both;"></div>
    </div>
     <form id="delete_form" action="nncms_content_role_control.php" method="post">
      <input name="action" id="action" type="hidden" value="delete" />
      <input name="nns_id" id="nns_id" type="hidden" value="" />
     </form>
</div>
</body>
</html>
