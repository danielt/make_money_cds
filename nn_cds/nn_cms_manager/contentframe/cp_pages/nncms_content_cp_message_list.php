<?php
/**
 * Created by PhpStorm.
 * User: xingcheng.hu
 * Date: 2016/3/11
 * Time: 9:55
 */
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include_once $nncms_config_path . "v2/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load("ns_model.message.message_queue");

//导入语言包
if (isset($_SESSION["language_dir"])) {
    $language_dir = $_SESSION["language_dir"];
}
include_once $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include_once $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool) {
    Header("Location: ../nncms_content_wrong.php");
    exit ;
}
$cp_id = $_GET['org_id'];

global $g_bk_version_number;
$bk_version_number = $g_bk_version_number == '1' ? true : false;
unset($g_bk_version_number);

if($bk_version_number && $_GET['op'] == 'import')
{
    m_config::$flag_operation = false;
    $log_timer_path = \m_config::return_child_path($nncms_config_path . "v2/ns_timer/import/import_message.timer.php", 'ns_timer');
    $ids = explode(',', $_GET['ids']);
    $ids= (is_array($ids) && !empty($ids)) ? array_filter($ids) : $ids;
    if(!is_array($ids) || empty($ids))
    {
        echo "<script>alert('传入查询ID为空');history.go(-1);</script>";die;
    }
    $result_message = nl_message::query_by_condition(m_config::get_dc(), array('nns_id'=>$ids,'nns_cp_id'=>$cp_id));
    if($result_message['ret'] !=0)
    {
        echo "<script>alert('数据库查询数据失败');history.go(-1);</script>";die;
    }
    if(empty($result_message['data_info']) || !is_array($result_message['data_info']))
    {
        echo "<script>alert('数据库查询无数据');history.go(-1);</script>";die;
    }
    $project = evn::get("project");
    \ns_core\m_load::load("ns_api.{$project}.message.{$cp_id}.message");
    foreach ($result_message['data_info'] as $message)
    {
        //调用对应的注入类型解释器，如路径：ns_input/{import_mode}/message_explain.class.php
        $explain_inst = new message($message);
        $result = $explain_inst->explain($message);
        m_config::timer_write_log($log_timer_path,"执行消息【{$message['nns_message_id']}】结果：".var_export($result,true));
        //此处还有反馈其他状态，如主媒资未注入、分集未注入、XML解析失败等
        if($result['ret'] != 0)
        {
            nl_message::update_message_state(m_config::get_dc(),$message['nns_id'],array('nns_message_state'=>4),true);
        }
        else
        {
            nl_message::update_message_state(m_config::get_dc(),$message['nns_id'],array('nns_message_state'=>3),true);
        }
        unset($explain_inst);
    }
//    $timer = new import_message_timer(m_config::return_child_path($nncms_config_path . "v2/ns_timer/import/import_message.timer.php",'timer'),$cp_id);
//    $result = $timer->run($result_message);

    ob_clean();
    echo "<script>alert('".$result['reason']."');history.go(-1);</script>";die;
}

include_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
include_once $nncms_config_path . 'nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/v2/ns_core/m_linux_mysql_query.class.php';
$queue_task_model = new queue_task_model();

$dc = nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$dc->open();
$cp_info = nl_cp::query_by_id($dc, $cp_id);
if($cp_info['ret'] !=0)
{
    echo "<script>alert('".$cp_info['reason']."');</script>";die;
}

if(!isset($cp_info['data_info']) || !is_array($cp_info['data_info']))
{
    echo "<script>alert('查无cp配置');</script>";die;
}
$cp_type = isset($cp_info['data_info']['nns_type']) ? $cp_info['data_info']['nns_type'] : '0';
$cp_name = isset($cp_info['data_info']['nns_name']) ? $cp_info['data_info']['nns_name'] : '';
$cp_config = (isset($cp_info['data_info']['nns_config']) && !empty($cp_info['data_info']['nns_config'])) ? json_decode($cp_info['data_info']['nns_config'],true) : null;
$message_parse_version = (isset($cp_config['message_parse_version']) && !empty($cp_config['message_parse_version'])) ? true : false;
$message_parse_model = (strlen($cp_config['message_parse_model']) >0) ? $cp_config['message_parse_model'] : '';
if($_GET['op_field'] == 'nns_delete')
{
    $g_message_time = (isset($g_message_send_again) && isset($g_message_send_again['least']) && $g_message_send_again['least'] > 0) ? $g_message_send_again['least'] : 100;

    $ids = explode(',', $_GET['ids']);
    $ids = join("','", $ids);
    if(isset($_GET['org_id']) && !empty($_GET['org_id']))
    {
        include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api/' . $_GET['org_id'] . '/' . $_GET['org_id'] . '_sync_source.php';
    }
    else
    {
        include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api/mgtv/sync_source.php';
    }

    //$dc = dc();
    $data = date("Y-m-d H:i:s");
    $str_delete = $_GET['op'] == '0' ? '0' : '1';
    $str_set = $str_delete == '0' ? ",nns_again='0',nns_message_state=0" : "";
    $str_where = '';
    //$str_where = $str_delete == '0' ? "" : " and nns_again >='{$g_message_time}'";
    $sql_update = "update nns_mgtvbk_message set nns_modify_time='{$data}',nns_delete='{$str_delete}'{$str_set} where nns_id in ('$ids') {$str_where} ";
    $result_update=nl_execute_by_db($sql_update, $dc->db());
    if($result_update)
    {
        echo '<script>alert("操作成功");history.go(-1);</script>';die;
    }
    else
    {
        echo '<script>alert("操作失败");history.go(-1);</script>';die;
    }
}

if($_GET['op'] == 'import')
{
    $is_success = false;
    $ids = explode(',', $_GET['ids']);
    ini_set('display_errors', 1);

    if(strlen($message_parse_model) > 0 && $message_parse_version)//V4新版本
    {
        include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api_v4/common.php';
        $parse_config = get_config_v2("g_import_sourcce");
        if(!isset($parse_config[$message_parse_model]))
        {
            echo '<script>alert("V4版本没有此注入模式");history.go(-1);</script>';die;
        }
        $class_name = $parse_config[$message_parse_model] . "_sync_source";
        $sync_source = new $class_name($cp_info,$cp_config,LOG_MODEL_MESSAGE);
    }
    elseif(strlen($message_parse_model) > 0 && $message_parse_model !='1000' && !$message_parse_version)//V2版本
    {
        include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api_v2/control/public_sync_source.php';
        include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/mgtv_init.php';
        include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api_v2/nn_const.php';
        include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api_v2/common.php';
        include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
        include_once dirname(dirname(dirname(dirname(__FILE__))))  . '/nn_logic/nl_log_v2.func.php';
        include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';
        include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/message/nl_message.class.php';
        include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/models/import_model.php';
        //引入np类文件
        include_np('np_xml2array.class');
        include_np('np_ftp');
        include_np('np_string');
        include_np('np_http_curl.class');
        include_np('np_xml_to_array.class');

        $parse_config = get_config_v2("g_import_sourcce_v2");
        $parse_model = $parse_config[$message_parse_model];
        include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api_v2/control/' . $parse_model[0] .'/sync_source.php';
        $class_name = $parse_model[1];
        $sync_source = new $class_name(array('cp_id'=>$cp_id,'cp_name'=>$cp_name,'cp_type'=>$cp_type),$cp_config,'',$cp_id);
    }
    else
    {
        if(isset($_GET['org_id']) && !empty($_GET['org_id']))
        {
            include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api/' . $_GET['org_id'] . '/' . $_GET['org_id'] . '_sync_source.php';
            $class_name = $_GET['org_id'] . '_sync_source';
        }
        else
        {
            include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/api/mgtv/sync_source.php';
            $class_name = 'sync_source';
        }
        $sync_source = new $class_name();
    }
    //$dc = dc();
    $arr_cp_config = null;
    $arr_info_data = array();
    foreach ($ids as $value)
    {
        $params['nns_id'] = $value;
        $message_info = $queue_task_model->get_cp_message_list($params);
        $message_info = $message_info['data'][0];
        if ($message_info['nns_message_state'] != '1' && $message_info['nns_message_state'] != '2' && $message_info['nns_message_state'] != '5')
        {
            if(preg_match("/^(http:\/\/|ftp:\/\/).*$/", $message_info['nns_message_content']))
            {
                $nns_message_content=$message_info['nns_message_content'];
            }
            else
            {
                if(strlen($message_info['nns_message_content']) <1)
                {
                    $message_info['nns_message_content'] = file_get_contents(dirname(dirname(dirname(dirname(__FILE__)))).'/data/'.$message_info['nns_message_url']);
                }
                $dom = new DOMDocument('1.0', 'utf-8');
                $dom->loadXML($message_info['nns_message_content']);
                $nns_message_content = $dom->saveXML();
            }
            $sync_source->is_message_again = false;//初始化是否消息队列重试
            if($message_info['nns_again'] > 0) //重试
            {
                $sync_source->is_message_again = true;
            }
            $sync_source->message_guid = $value;//消息GUID
            if(strlen($message_parse_model) > 0 && $message_parse_version)//V4新版本
            {
                $re = $sync_source->parse_xml($nns_message_content, $message_info['nns_message_id']);
            }
            else
            {

                $sync_source->csp_data['CorrelateID'] = $message_info['nns_message_id'];
                if (isset($sync_source->xml_path) && !empty($message_info['nns_message_xml']))
                {
                    $sync_source->xml_path = $message_info['nns_message_xml'];
                }
                $re = $sync_source->parse_xml($nns_message_content, $message_info['nns_message_id'], $cp_id);
            }
            if($re['ret'] !=0)
            {
                $str_temp_error_info = isset($re['reason']) ? $re['reason'] : '';
                $arr_info_data[] = "消息ID为[{$message_info['nns_message_id']}],CP为:[{$cp_id}];错误原因：".$str_temp_error_info;
            }
            $date = date("Y-m-d H:i:s");
            if($re['ret']=='0')
            {
                $sql_update = "update nns_mgtvbk_message set nns_message_state=3,nns_modify_time='{$date}',nns_again=nns_again+1 where nns_id='{$value}'";
                $is_success = true;
            }
            elseif($re['ret']=='6')//主媒资未注入
            {
                $sql_update = "update nns_mgtvbk_message set nns_message_state=6,nns_modify_time='{$date}',nns_again=nns_again+1 where nns_id='{$value}'";
            }
            elseif($re['ret']=='7')//分集未注入
            {
                $sql_update = "update nns_mgtvbk_message set nns_message_state=7,nns_modify_time='{$date}',nns_again=nns_again+1 where nns_id='{$value}'";
            }
            else if ($re['ret'] == '2')//平台过滤消息
            {
                $sql_update = "update nns_mgtvbk_message set nns_message_state=5,nns_modify_time='{$date}',nns_again=nns_again+1 where nns_id='{$value}'";
                $is_success = true;
            }
            else
            {
                $sql_update = "update nns_mgtvbk_message set nns_message_state=4,nns_modify_time='{$date}',nns_again=nns_again+1 where nns_id='{$value}'";
                $is_success = false;
            }
            nl_execute_by_db($sql_update, $dc->db());
            if(empty($arr_cp_config) || !is_array($arr_cp_config))
            {
                $arr_cp_config = nl_cp::query_by_id($dc, $message_info['nns_cp_id']);
                $arr_cp_config = (isset($arr_cp_config['data_info']['nns_config']) && !empty($arr_cp_config['data_info']['nns_config'])) ? json_decode($arr_cp_config['data_info']['nns_config'],true) : null;
            }
            //失败返回
            $flag_fail_mode = (isset($arr_cp_config['content_feedback_mode']) && $arr_cp_config['content_feedback_mode'] == '1') ? true : false;
            //始终反馈
            $flag_always_mode = (isset($arr_cp_config['content_feedback_mode']) && $arr_cp_config['content_feedback_mode'] == '2') ? true : false;
            if ($flag_always_mode || $flag_fail_mode)
            {
                if(method_exists($sync_source, 'create_xml_respond_msg_to_csp'))
                {
                    if($re['ret'] == 0)
                    {
                        $sync_source->create_xml_respond_msg_to_csp(SUCCESS_STATE, $re['reason']);
                    }
                    if($re['ret'] != 0)
                    {
                        $sync_source->create_xml_respond_msg_to_csp(FAIL_STATE, '注入失败可能原因' . $re['reason']);
                    }
                }
                else if(method_exists($sync_source, 'respond_msg_to_csp'))
                {
                    if($re['ret'] != 0)
                    {
                        $result_notice = $sync_source->respond_msg_to_csp($re,$message_info);
                    }
                }
                elseif(method_exists($sync_source, 'async_feedback'))//V4版本调用反馈
                {
                    //array('msgid','state','msg',info=>array(asset_type,asset_id,part_id,file_id,cdn_id))
                    $return_arr = array(
                        'msgid' => $message_info['nns_message_id'],
                        'state' => $re['ret'],
                        'msg' => $re['reason']
                    );
                    $sync_source->async_feedback($return_arr);
                }
            }
        }
    }

    $str = $is_success ? '操作成功' : '操作失败';
    if(!empty($arr_info_data) && !empty($arr_info_data))
    {
        $str.=":<br/>".implode("<br/>", $arr_info_data);
    }
    echo '<script>alert("'.$str.'");history.go(-1);</script>';die;
}
else if($_GET['op'] == 'ftp_import')
{
    $is_success = false;
    $ids = explode(',', $_GET['ids']);
    ini_set('display_errors', 1);
    include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/np/np_http_curl.class.php';
    global $g_mgtvbk_base_url;
    $mgtvbk_base_url = $g_mgtvbk_base_url;
    unset($g_mgtvbk_base_url);
    $arr_log=array();
    include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/message/nl_message.class.php';
    foreach ($ids as $value)
    {
        $curl = new np_http_curl_class();
        $data = array();
        $params['nns_id'] = $value;
        $message_info = $queue_task_model->get_cp_message_list($params);
        $message_info = $message_info['data'][0];
        if(isset($message_info['nns_message_xml']) && strlen($message_info['nns_message_xml'])>0 && preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$message_info['nns_message_xml']))
        {
            $result = m_public::get_curl_content($message_info['nns_message_xml']);
            if(isset($result['data_info']) && strlen($result['data_info']) > 0 && m_public::is_xml($result['data_info']))
            {
                $message_info_v2= array(
                    'nns_message_state'=>0,
                    'nns_message_content'=>$result['data_info'],
                );
                $result = nl_message::update_message($dc,array('nns_id'=>$message_info['nns_id']),$message_info_v2);
            }
            else
            {
                echo '<script>alert("'.$result['reason'].'");history.go(-1);</script>';die;
            }
            continue;
        }
        

        if(empty($mgtvbk_base_url))
        {
            echo '<script>alert("配置文件$mgtvbk_base_url未配置注入地址");history.go(-1);</script>';die;
        }
        //if($message_info['nns_message_state'] != 2)

        if($message_info['nns_message_state'] != 1)
        {
            continue;
        }
        $data['action'] = 're_import';
        $data['cmspost'] = $message_info['nns_message_xml'];
        $curl->post($mgtvbk_base_url,$data);
        $curl_info = $curl->curl_getinfo();
        if($curl_info['http_code'] != '200')
        {
            $arr_log[] = "消息id：{$value},错误信息：".$curl->curl_error();
        }
        unset($curl);
    }
    if(!empty($arr_log))
    {
        echo '<script>alert("操作有失败部分['.implode('],[', $pieces).']");history.go(-1);</script>';die;
    }
    else
    {
        echo '<script>alert("操作成功");history.go(-1);</script>';die;
    }
}
$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : (!empty($_COOKIE["page_max_num"]) ?  $_COOKIE["page_max_num"] : 18);
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) {
    $page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit = $page_size;
$message_id = '';
$message_state = '';
$params = array();
$params['nns_cp_id'] = $_GET['org_id'];
if ($_GET['actionsearch'] == 'search') {
    $params['nns_message_id'] = $_GET['nns_message_id'];
    $params['nns_message_state'] = $_GET['nns_message_state'];
    $params['nns_name'] = $_GET['nns_name'];
    $params['nns_type'] = $_GET['nns_type'];
    $params['nns_action'] = $_GET['nns_action'];
    if(!empty($_GET['day_picker_start']) && !empty($_GET['day_picker_end']))
    {
        if($_GET['day_picker_start'] > $_GET['day_picker_end'])
        {
            $temp = $_GET['day_picker_start'];
            $_GET['day_picker_start'] = $_GET['day_picker_end'];
            $_GET['day_picker_end'] = $temp;
        }
    }
    if(!empty($_GET['nns_begin_time']) && !empty($_GET['nns_end_time']))
    {
        if($_GET['nns_begin_time'] > $_GET['nns_end_time'])
        {
            $temp = $_GET['nns_begin_time'];
            $_GET['nns_begin_time'] = $_GET['nns_end_time'];
            $_GET['nns_end_time'] = $temp;
        }
    }
    $params['date_begin'] = $_GET['day_picker_start'];
    $params['date_end'] = $_GET['day_picker_end'];

    $params['date_begin_modify'] = $_GET['nns_begin_time'];
    $params['date_end_modify'] = $_GET['nns_end_time'];


    $params['nns_again'] = $_GET['nns_again'];
    $params['nns_delete'] = $_GET['nns_delete_status'];

    $params['nns_content'] = $_GET['nns_content'];
}
$result = $queue_task_model->get_cp_message_list($params, $offset, $limit,$bk_version_number);
$vod_total_num = $result['rows'];
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) {
    $page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;

$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);

$data = $result['data'];
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_READ,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$result_cp_id_data = nl_query_by_db("select nns_cp_id from nns_mgtvbk_message group by nns_cp_id", $dc->db());
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
    <style>
        .mytip_ul
        {
            text-align: left;
            font-size: 12px;
            list-style-type: none;
            padding: 0;
            margin: 0;
        }

        .mytip_title
        {
            margin-left: 10px;
            width: 100px;
            float: left;
            text-align: left;
        }

        .mytip_close
        {
            margin-right: 10px;
            margin-top: 5px;
            width: 100px;
            float: right;
            text-align: right;
            cursor: pointer;
        }

        .mytip_top
        {
            background-image: url(../../images/tips_titlebj.gif);
            color: #FFFFFF;
            font-size: 12px;
            height: 24px;
            line-height: 24px;
            font-weight: 600;
        }

        .tips_li
        {
            height: 25px;
            line-height: 25px;
            border-bottom: #ccc 1px solid;
        }

        .tips_li_span
        {
            color: #07507d;
            margin-left: 10px;
        }

        .tips_li_span2
        {
            color: #6d6d6d;
        }
        .myalert_div1
        {
            height: 25px;
            line-height: 25px;
            border-bottom: #ccc 1px solid;
            width: 620px;
        }

        .myalert_div1_leftDiv
        {
            float: left;
            width: 50%;
            margin-left: 10px;
        }

        .myalert_div1_rightDIv
        {
            float: left;
            margin-left: 10px;
        }

        .myalert_div1_Div
        {
            float: left;
            width: 100%;
            margin-left: 10px;
        }

        .myalert_div2
        {
            height: 84px;
            line-height: 70px;
            border-bottom: #ccc 1px solid;
            width: 620px;
        }

        .myalert_div2_Div
        {
            float: left;
            width: 100%;
            margin-left: 10px;
            height: 84px;
        }

        .myaelrt_li_title
        {
            color: #07507d;
        }

        .myalert_li_txt
        {
            color: #6d6d6d;
        }

        .myalert_inputTxt
        {
            width: 220px;
            border: 0px solid #eff0f0;
            padding-left: 2px;
            background-color: #eff0f0;
            color: #6d6d6d;
        }
    </style>

    <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
    <script language="javascript" src="../../js/cms_cookie.js"></script>
    <script language="javascript" src="../../js/table.js.php"></script>
    <script language="javascript" src="../../js/checkinput.js.php"></script>
    <script language="javascript" src="../../js/rate.js"></script>
    <script language="javascript" src="../../js/image_loaded_func.js"></script>
    <script language="javascript" src="../../js/cms_datepicker.js"></script>
    <script language="javascript" src="../../js/alertbox.js"></script>
    <script language="javascript">

        function refresh_vod_page() {
            var num = $("#nns_list_max_num").val();
            setCookie("page_max_num",num);
            window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
        }
        function checkhiddenBox(type) {
            BoxKey = false;
            $("input.checkhiddenInput:checked").each(function() {
                if ($(this).attr('rel') == type) {
                    BoxKey = true;
                    return false;
                }
            })
            return BoxKey;
        }

        function msg_ftp_import()
        {
            var r=confirm("是否进行该操作");
            if(r == true){
                var ids=getAllCheckBoxSelect();
                ids = ids.substr(0,ids.length-1);
                if(ids==""){
                    alert('请选择数据');
                }else{
                    var url = "nncms_content_cp_message_list.php?op=ftp_import&ids="+ids+"&org_id=<?php echo $_GET['org_id']?>";
                    window.location.href = url;
                }
            }
        }

        function msg_import(){
            var r=confirm("是否进行该操作");
            if(r == true){
                var ids=getAllCheckBoxSelect();
                ids = ids.substr(0,ids.length-1);
                if(ids==""){
                    alert('请选择数据');
                }else{
                    var url = "nncms_content_cp_message_list.php?op=import&ids="+ids+"&org_id=<?php echo $_GET['org_id']?>";
                    window.location.href = url;
                }
            }
        }

        function msg_delete_or_recover(str){
            var r=confirm("是否进行该操作");
            if(r == true){
                var ids=getAllCheckBoxSelect();
                ids = ids.substr(0,ids.length-1);
                if(ids==""){
                    alert('请选择数据');
                }else{
                    var url = "nncms_content_cp_message_list.php?op_field=nns_delete&op="+ str +"&ids="+ids+"&org_id=<?php echo $_GET['org_id']?>";
                    window.location.href = url;
                }
            }
        }


        $(document).ready(function() {
            $('#clear_time').click(function(){
                $('#day_picker_start').val('');
                $('#day_picker_end').val('');

            });
            window.parent.now_frame_url = window.location.href;
        });

        $(document).ready(function() {
            $('#clear_time_modify').click(function(){
                $('#nns_begin_time').val('');
                $('#nns_end_time').val('');

            });
            window.parent.now_frame_url = window.location.href;
        });

    </script>

</head>

<body>
<div class="content">
    <div class="content_position">
        CP消息列表 > <?php echo $_GET['org_id']; ?>
    </div>
    <div class="content_table">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <form action="" method="get">
                <input type="hidden" name="org_id" value="<?php echo $_GET['org_id'];?>" />
                <tbody>
                <tr>
                    <td>
                        消息ID：&nbsp;&nbsp;<input type="text" name="nns_message_id" id="nns_message_id" value="<?php echo $_GET['nns_message_id']; ?>" style="width:200px;">
                        名称：&nbsp;&nbsp;<input type="text" name="nns_name" id="nns_name" value="<?php echo $_GET['nns_name']; ?>" style="width:200px;">
                        内容查询：&nbsp;&nbsp;<input type="text" name="nns_content" id="nns_content" value="<?php echo $_GET['nns_content']; ?>" style="width:200px;">
                        &nbsp;&nbsp;&nbsp;&nbsp;状态：&nbsp;&nbsp;<select name="nns_message_state" style="width: 150px;">
                            <option value="" >全部</option>
                            <option value="0" <?php if($_GET['nns_message_state']=='0') echo 'selected="selected"'?>>等待注入</option>
                            <option value="1" <?php if($_GET['nns_message_state']=='1') echo 'selected="selected"'?>>FTP连接失败</option>
                            <option value="2" <?php if($_GET['nns_message_state']=='2') echo 'selected="selected"'?>>XML获取失败</option>
                            <option value="3" <?php if($_GET['nns_message_state']=='3') echo 'selected="selected"'?>>注入成功</option>
                            <option value="4" <?php if($_GET['nns_message_state']=='4') echo 'selected="selected"'?>>注入失败</option>
                            <option value="5" <?php if($_GET['nns_message_state']=='5') echo 'selected="selected"'?>>平台过滤消息</option>
                            <option value="6" <?php if($_GET['nns_message_state']=='6') echo 'selected="selected"'?>>主媒资未注入</option>
                            <option value="7" <?php if($_GET['nns_message_state']=='7') echo 'selected="selected"'?>>分集未注入</option>
                            <option value="8" <?php if($_GET['nns_message_state']=='8') echo 'selected="selected"'?>>正在发送MSP</option>
                            <option value="9" <?php if($_GET['nns_message_state']=='9') echo 'selected="selected"'?>>MSP响应失败</option>
                            <option value="10" <?php if($_GET['nns_message_state']=='10') echo 'selected="selected"'?>>频道未注入</option>
                            <option value="11" <?php if($_GET['nns_message_state']=='11') echo 'selected="selected"'?>>直播源未注入</option>
                            <option value="12" <?php if($_GET['nns_message_state']=='12') echo 'selected="selected"'?>>异步下载上游ftp工单</option>
                            <option value="99" <?php if($_GET['nns_message_state']=='99') echo 'selected="selected"'?>>媒资查询成功</option>
                            <option value="100" <?php if($_GET['nns_message_state']=='100') echo 'selected="selected"'?>>媒资查询失败</option>
                            <option value="101" <?php if($_GET['nns_message_state']=='101') echo 'selected="selected"'?>>内容有问题不做任何处理</option>
                        </select>
                        <br />
                        媒资类型：<select name="nns_type" style="width: 150px;">
                            <option value="" >全部</option>
                            <option value="1" <?php if($_GET['nns_type']=='1') echo 'selected="selected"'?>>主媒资</option>
                            <option value="2" <?php if($_GET['nns_type']=='2') echo 'selected="selected"'?>>分集</option>
                            <option value="3" <?php if($_GET['nns_type']=='3') echo 'selected="selected"'?>>片源</option>
                            <?php
                            if(g_cms_config::get_g_config_value('nomarl_c2_import_mode') == '1')
                            {
                                ?>
                                <option value="4" <?php if($_GET['nns_type']=='4') echo 'selected="selected"'?>>Series</option>
                                <option value="5" <?php if($_GET['nns_type']=='5') echo 'selected="selected"'?>>Program</option>
                                <option value="6" <?php if($_GET['nns_type']=='6') echo 'selected="selected"'?>>Movie</option>
                                <option value="7" <?php if($_GET['nns_type']=='7') echo 'selected="selected"'?>>Picture</option>
                                <option value="8" <?php if($_GET['nns_type']=='8') echo 'selected="selected"'?>>Category</option>
                                <option value="9" <?php if($_GET['nns_type']=='9') echo 'selected="selected"'?>>Channel</option>
                                <option value="10" <?php if($_GET['nns_type']=='10') echo 'selected="selected"'?>>PhysicalChannel</option>
                                <option value="11" <?php if($_GET['nns_type']=='11') echo 'selected="selected"'?>>Schedule</option>
                                <?php
                            }
                            else
                            {
                                ?>
                                <option value="4" <?php if($_GET['nns_type']=='4') echo 'selected="selected"'?>>直播频道</option>
                                <option value="5" <?php if($_GET['nns_type']=='5') echo 'selected="selected"'?>>直播分集</option>
                                <option value="6" <?php if($_GET['nns_type']=='6') echo 'selected="selected"'?>>直播源</option>
                                <option value="7" <?php if($_GET['nns_type']=='7') echo 'selected="selected"'?>>节目单</option>
                                <option value="8" <?php if($_GET['nns_type']=='8') echo 'selected="selected"'?>>文件包</option>
                                <?php
                            }
                            ?>
                        </select>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        操作类型：<select name="nns_action" style="width: 150px;">
                            <option value="" >全部</option>
                            <option value="1" <?php if($_GET['nns_action']=='1') echo 'selected="selected"'?>>添加</option>
                            <option value="2" <?php if($_GET['nns_action']=='2') echo 'selected="selected"'?>>修改</option>
                            <option value="3" <?php if($_GET['nns_action']=='3') echo 'selected="selected"'?>>删除</option>
                            <option value="4" <?php if($_GET['nns_action']=='4') echo 'selected="selected"'?>>上线</option>
                            <option value="5" <?php if($_GET['nns_action']=='5') echo 'selected="selected"'?>>下线</option>
                            <option value="6" <?php if($_GET['nns_action']=='6') echo 'selected="selected"'?>>锁定</option>
                            <option value="7" <?php if($_GET['nns_action']=='7') echo 'selected="selected"'?>>解锁</option>
                            <option value="8" <?php if($_GET['nns_action']=='8') echo 'selected="selected"'?>>绑定</option>
                            <option value="9" <?php if($_GET['nns_action']=='9') echo 'selected="selected"'?>>解绑</option>
                            <?php
                            if(g_cms_config::get_g_config_value('nomarl_c2_import_mode') != '1')
                            {
                                ?>
                                <option value="6" <?php if($_GET['nns_action']=='6') echo 'selected="selected"'?>>栏目同步</option>
                                <option value="7" <?php if($_GET['nns_action']=='7') echo 'selected="selected"'?>>删除栏目</option>
                                <option value="8" <?php if($_GET['nns_action']=='8') echo 'selected="selected"'?>>同步推荐</option>
                                <option value="9" <?php if($_GET['nns_action']=='9') echo 'selected="selected"'?>>取消推荐</option>
                                <option value="10" <?php if($_GET['nns_action']=='10') echo 'selected="selected"'?>>同步CP</option>
                                <option value="11" <?php if($_GET['nns_action']=='11') echo 'selected="selected"'?>>同步栏目图片</option>
                                <option value="12" <?php if($_GET['nns_action']=='12') echo 'selected="selected"'?>>删除栏目图片</option>
                                <?php
                            }
                            ?>
                            <option value="99" <?php if($_GET['nns_action']=='99') echo 'selected="selected"'?>>媒资查询</option>
                            <option value="100" <?php if($_GET['nns_action']=='100') echo 'selected="selected"'?>>未知</option>
                        </select>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        重试次数：&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="nns_again" id="nns_again" style="width:100px;" value="<?php if($_GET['nns_again']) echo $_GET['nns_again'];?>">
                        &nbsp;&nbsp;&nbsp;&nbsp;删除状态：&nbsp;&nbsp;&nbsp;&nbsp;<select name="nns_delete_status" style="width: 150px;">
                            <option value="" >全部</option>
                            <option value="1" <?php if($_GET['nns_delete_status']=='1') echo 'selected="selected"'?>>删除</option>
                            <option value="0" <?php if($_GET['nns_delete_status']=='0') echo 'selected="selected"'?>>未删除</option>
                        </select>
                        <?php if(is_array($result_cp_id_data) && !empty($result_cp_id_data))
                        {

                        ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;CP ID：&nbsp;&nbsp;&nbsp;&nbsp;<select name="nns_cp_id" style="width: 150px;">
                            <option value="" >全部</option>
                            <?php
                            foreach ($result_cp_id_data as $cp_id_val)
                            {
                                ?>
                                <option value="<?php echo $cp_id_val['nns_cp_id']; ?>" <?php if($_GET['nns_cp_id']==$cp_id_val['nns_cp_id']) echo 'selected="selected"'?>><?php echo $cp_id_val['nns_cp_id']; ?></option>
                                <?php
                            }
                            echo "</select>";
                            }?>
                            <br>
                            选择创建时间段：&nbsp;&nbsp;&nbsp;<input name="day_picker_start" id="day_picker_start" type="text"  value="<?php
                            if (isset($_GET['day_picker_start']))
                                echo $_GET['day_picker_start'];
                            ?>" style="width:120px;" class="datetimepicker" callback="test" />
                            - <input name="day_picker_end" id="day_picker_end" type="text"  value="<?php
                            if (isset($_GET['day_picker_end']))
                                echo $_GET['day_picker_end'];
                            ?>" style="width:120px;" class="datetimepicker" callback="test" />
                            &nbsp;&nbsp;<input type="button" id="clear_time" name="clear_time" value="清除时间"/>
                            &nbsp;&nbsp;选择修改时间段：&nbsp;&nbsp;&nbsp;<input name="nns_begin_time" id="nns_begin_time" type="text"  value="<?php
                            if (isset($_GET['nns_begin_time']))
                                echo $_GET['nns_begin_time'];
                            ?>" style="width:120px;" class="datetimepicker" callback="test" />
                            - <input name="nns_end_time" id="nns_end_time" type="text"  value="<?php
                            if (isset($_GET['nns_end_time']))
                                echo $_GET['nns_end_time'];
                            ?>" style="width:120px;" class="datetimepicker" callback="test" />
                            &nbsp;&nbsp;<input type="button" id="clear_time_modify" name="clear_time_modify" value="清除时间"/>
                            <input type="submit" value="<?php echo cms_get_lang('search'); ?>"  />
                            <input type="hidden" name="actionsearch" value="search" />
                    </td>
                </tr>
                </tbody>
            </form>
        </table>
    </div>
    <div class="content_table formtable">

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <th><input name="" type="checkbox" value="" /></th>
                <th width="30px">序号</th>
                <th style="width:120px;">消息ID</th>
                <th style="width:35px;">CP ID</th>
                <th style="width:120px;">名称</th>
                <th style="width:50px;">消息队列日志</th>
                <th style="width:50px;">消息原始内容</th>
                <th style="width:50px;">消息注入内容</th>
                <th style="width:50px;">消息流程日志</th>
                <th>媒资类型</th>
                <th>操作类型</th>
                <th>状态</th>
                <th>重试次数</th>
                <th style="width:50px;">删除状态</th>
                <th>创建时间</th>
                <th>修改时间</th>
                <th>内容数量</th>
                <th width="30px">操作</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if($data!=null){
                $num = ($page_num - 1) * $page_size;
                foreach ($data as $item) {
                    $num++;
                    ?>
                    <tr>
                        <td id="is_delete"><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>"></td>
                        <td><?php echo $num; ?></td>
                        <!--
						<td >
                        <?php

                        //echo $item['nns_id'];

                        ?>

                        </td>
						 -->
                        <td>


                            <?php

                            echo $item['nns_message_id'];

                            ?>



                        </td>
                        <td>


                            <?php

                            echo $item['nns_cp_id'];

                            ?>



                        </td>
                        <td><?php echo $item['nns_name']?></td>
                        <td>
                            <?php if(isset($item['nns_bk_queue_excute_url']) && strlen($item['nns_bk_queue_excute_url']) >0){?>
                                <a href="./../../../data/<?php echo $item['nns_bk_queue_excute_url'];?>" target="_blank">点击查看</a>
                            <?php }else{?>
                            <a href="./nncms_content_cp_message_view.php?nns_id=<?php echo $item['nns_id']?>&nns_type=0" target="_blank">点击查看</a>
                            <?php }?>
                        </td>
                        <td>
                            <?php if(isset($item['nns_message_original_url']) && strlen($item['nns_message_original_url']) >0){?>
                                <a href="./../../../data/<?php echo $item['nns_message_original_url'];?>" target="_blank">点击查看</a>
                            <?php }else{?>
                            <a href="./nncms_content_cp_message_view.php?nns_id=<?php echo $item['nns_id']?>&nns_type=1" target="_blank">点击查看</a>
                            <?php }?>
                        </td>
                        <td>
                            <?php if(isset($item['nns_message_url']) && strlen($item['nns_message_url']) >0){?>
                                <a href="./../../../data/<?php echo $item['nns_message_url'];?>" target="_blank">点击查看</a>
                            <?php }else{?>
                            <a href="./nncms_content_cp_message_view.php?nns_id=<?php echo $item['nns_id']?>&nns_type=2" target="_blank">点击查看</a>
                            <?php }?>
                        </td>
                        <td>
                            <?php if($bk_version_number){?>
                                <a href="./../command/nncms_content_command_list.php?nns_message_id=<?php echo $item['nns_message_id']?>">点击查看</a>
                            <?php }?>
                        </td>
                        <td>
                            <?php
                            if(g_cms_config::get_g_config_value('nomarl_c2_import_mode') == '1')
                            {
                                switch ($item['nns_type'])
                                {
                                    case '1': echo '主媒资';break;
                                    case '2': echo '分集';break;
                                    case '3': echo '片源';break;
                                    case '4': echo 'Series';break;
                                    case '5': echo 'Program';break;
                                    case '6': echo 'Movie';break;
                                    case '7': echo 'Picture';break;
                                    case '8': echo 'Category';break;
                                    case '9': echo 'Channel';break;
                                    case '10': echo 'PhysicalChannel';break;
                                    case '11': echo 'Schedule';break;
                                    case '12': echo 'EPGFile';break;
                                    default: echo '';
                                }
                            }
                            else
                            {
                                switch ($item['nns_type'])
                                {
                                    case '1': echo '主媒资';break;
                                    case '2': echo '分集';break;
                                    case '3': echo '片源';break;
                                    case '4': echo '直播频道';break;
                                    case '5': echo '直播分集';break;
                                    case '6': echo '直播源';break;
                                    case '7': echo '节目单';break;
                                    case '8': echo '文件包';break;
                                    case '9': echo 'TASK';break;
                                    case '12': echo 'EPGFile';break;
                                    default: echo '';
                                }
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            switch ($item['nns_action'])
                            {
                                case '1': echo '添加';break;
                                case '2': echo '修改';break;
                                case '3': echo '删除';break;
                                case '4': echo '上线';break;
                                case '5': echo '下线';break;
                                case '6': echo '栏目同步';break;
                                case '7': echo '删除栏目';break;
                                case '8': echo '同步推荐';break;
                                case '9': echo '取消推荐';break;
                                case '10': echo '同步CP';break;
                                case '11': echo '同步栏目图片';break;
                                case '12': echo '删除栏目图片';break;
                                case '99': echo '媒资查询';break;
                                case '100': echo '未知';break;
                                default: echo '';
                            }
                            ?>
                        </td>
                        <td>
                            <?php

                            switch ($item['nns_message_state'])
                            {
                                case '0': echo '等待注入';break;
                                case '1': echo 'FTP连接失败';break;
                                case '2': echo '获取XML失败';break;
                                case '3': echo '注入成功';break;
                                case '4': echo '注入失败';break;
                                case '5': echo '平台过滤消息';break;
                                case '6': echo '主媒资未注入';break;
                                case '7': echo '分集未注入';break;
                                case '8': echo '正在发送MSP';break;
                                case '9': echo 'MSP响应失败';break;
                                case '99': echo '媒资查询成功';break;
                                case '100': echo '媒资查询失败';break;
                                case '101': echo '内容有问题不做任何处理';break;
                                default : echo '';
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            echo $item['nns_again'];
                            ?>
                        </td>
                        <td>
                            <?php
                            switch ($item['nns_delete'])
                            {
                                case '1': echo "<font style=\"color:#FF0000\">已删除</font>";break;
                                case '0': echo "<font style=\"color:#00CC00\">未删除</font>";break;
                                default : echo '';
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            echo $item['nns_create_time'];
                            ?>
                        </td>
                        <td>
                            <?php
                            echo $item['nns_modify_time'];
                            ?>
                        </td>
                        <td>
                            <?php
                            echo $item['nns_content_number'];
                            ?>
                        </td>

                        <td>
                            <?php if($item['nns_message_state'] == '4' || $item['nns_message_state'] == '0' || $item['nns_message_state'] == '6' || $item['nns_message_state'] == '7'){?>
                                <a href="nncms_content_cp_message_list.php?op=import&ids=<?php echo $item['nns_id'];?>&org_id=<?php echo $_GET['org_id']?>">注入</a>
                            <?php }?>
                        </td>
                    </tr>
                    <?php
                }
            }$least_num = $g_manager_list_max_num - count($data);
            for ($i = 0; $i < $least_num; $i++) {
                ?>
                <tr>

                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="pagecontrol">
        共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
        <?php if ($currentpage > 1) { ?>
            <a href="nncms_content_cp_message_list.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="nncms_content_cp_message_list.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php } else { ?>
            <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
            <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php } if ($currentpage < $vod_pages) { ?>
            <a href="nncms_content_cp_message_list.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="nncms_content_cp_message_list.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php } else { ?>
            <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
            <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php } ?>

        <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
        <a href="javascript:go_page_num('nncms_content_cp_message_list.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
        <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
        <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
        <input name="nns_list_max_num" id="nns_list_max_num" type="text"
               value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
        <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
               onclick="refresh_vod_page();"/>&nbsp;&nbsp;

    </div>

    <div class="controlbtns">
        <div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
        <div class="controlbtn move"><a href="javascript:msg_import();">注入</a></div>
        <!--<div class="controlbtn move"><a href="javascript:msg_import();">重新注入</a></div>-->
        <div class="controlbtn move"><a href="javascript:msg_delete_or_recover('1');">删除</a></div>
        <div class="controlbtn move"><a href="javascript:msg_delete_or_recover('0');">恢复</a></div>
        <div class="controlbtn move"><a href="javascript:msg_ftp_import();">FTP重注入</a></div>
        <div style="clear:both;"></div>
    </div>

</div>
</body>
</html>