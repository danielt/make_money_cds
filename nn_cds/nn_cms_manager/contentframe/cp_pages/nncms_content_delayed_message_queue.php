<?php 
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) 
{
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/np/np_redis_check.class.php';

$dc = nl_get_dc(array(
       	'db_policy' => NL_DB_READ | NL_DB_WRITE,
        'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$dc->open();
$cp_id = $_GET['cp_id'];
//获取CP配置
$cp_re = nl_cp::query_by_id($dc, $cp_id);

$cp_config = json_decode($cp_re['data_info']['nns_config'],true);
$delayed_message_import_overtime = (int)$cp_config['delayed_message_import_overtime'] > 0 ? $cp_config['delayed_message_import_overtime'] : 86400;

/**
 * redis循环队列，操作参数
 * $config = array("queue_name" => "cp_id","recover_mode" => "loop","command_lose_time" => "指令失效时间");
 * array("guid","message_id","action","type","import_id")
 */
$config = array(
		"queue_name" => $cp_id,
		"command_lose_time" => $delayed_message_import_overtime
);
$redis_obj = nl_get_redis();
$redis_check = new np_redis_check_class($redis_obj, $config);

$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 18;
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) 
{
	$page_num = 1;
}

$offset = intval($page_num - 1) * $page_size;
$end = $offset+$page_size-1;
$result = $redis_check->get_list($offset, $end);
if(empty($result))
{
	$vod_total_num = 0;
	$data = array();
}
else 
{
	$vod_total_num = $result['num'];
	$data = $result['data'];
}
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) 
{
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;

$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);

?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
		<script language="javascript">

			function refresh_vod_page() 
			{
				var num = $("#nns_list_max_num").val();
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) 
			{
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}

				})
				return BoxKey;
			}
			function export_file()
            {
                window.location.href = "nncms_content_delayed_message_view.php?action=export&cp_id=<?php echo $cp_id;?>&total_num=<?php echo $vod_total_num;?>";
            }
		</script>

	</head>
	<body>
		<div class="content">
			<div class="content_position">
				延时注入队列
			</div>			
			<div class="content_table formtable">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th>序号</th>
							<th>队列状态</th>
                            <th>消息ID</th>
                            <th>原始ID</th>
                            <th>分集号</th>
                            <th>操作类型</th>
							<th>创建时间</th>
                            <th>重试时间</th>
							<th>内容</th>						    
						</tr>
					</thead>
					<tbody>
						<?php
						if(!empty($data))
						{
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) 
							{
								$num++;
								$command = json_decode($item['command'],true);
						?>
						<tr>
						<td width="50px"><?php echo $num; ?></td>
						<td>
						<?php 
							if(time() - strtotime($item['time']) > $delayed_message_import_overtime)//大于超时时间
							{
						?>
								<font color="red">超时</font>
						<?php 	
							}
							else 
							{
						?>
								<font color="green">正常</font>
						<?php  
							}
						?>
						</td>
                        <td><?php echo $command['message_id'];?></td>
                        <td><?php echo $command['import_id'];?></td>
                        <td><?php echo $command['index'];?></td>
                        <td><?php
                            switch ($command['action'])
                            {
                                case 'assest_package':
                                    echo '媒资打包';
                                    break;
                                case 'online':
                                    echo '上线';
                                    break;
                                case 'unline':
                                    echo '下线';
                                    break;
                                default:
                                    echo $command['action'];
                                    break;
                            }?></td>
						<td><?php echo $item['time'];?></td>
                            <td><?php echo $command['execute_time'];?></td>
                        <td><a target="_blank" href="nncms_content_delayed_message_view.php?cp_id=<?php echo $cp_id;?>&num=<?php echo $num;?>&header_type=<?php $arr = json_decode($item['command'],true);if(is_array($arr)){echo 'json';}else{echo 'xml';}?>">查看内容</a></td>                    
                        </tr>
						<?php
							}
						}
						$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) 
						{
						?>
						<tr>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                     	</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="pagecontrol">
			  共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_delayed_message_queue.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_delayed_message_queue.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_content_delayed_message_queue.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_delayed_message_queue.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_delayed_message_queue.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

          	</div>
        	<div class="controlbtns">
 	    	<div class="controlbtn export"><a href="javascript:export_file();">导出</a></div>
<!-- 	        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div> -->
<!-- 	        <div class="controlbtn move"><a href="javascript:msg_retry('import');">注入</a></div> -->
<!-- 			<div class="controlbtn move"><a href="javascript:do_audit('pass');">审核通过</a></div> -->
<!-- 			<div class="controlbtn move"><a href="javascript:do_audit('refuse');">审核不通过</a></div> -->
<!-- 			<div class="controlbtn move"><a href="javascript:do_pasue('stop');">暂停</a></div> -->
<!-- 			<div class="controlbtn move"><a href="javascript:do_pasue('normal');">取消暂停</a></div> -->
            <div style="clear:both;"></div>
 		</div>
	</body>
</html>
