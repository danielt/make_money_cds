<?php
/**
 * Created by PhpStorm.
 * User: xingcheng.hu
 * Date: 2016/4/12
 * Time: 18:10
 */
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
    $language_dir = $_SESSION["language_dir"];
}
include_once $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include_once $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool) {
    Header("Location: ../nncms_content_wrong.php");
    exit ;
}
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
include_once $nncms_config_path . 'nn_logic/cp/cp.class.php';

include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/np/np_redis.class.php';
include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/np/np_db_check.class.php';
include_once $nncms_config_path . 'nn_logic/nl_common.func.php';
include_once $nncms_config_path . 'nn_logic/redis_asset_import/redis_asset_import.class.php';
$obj_redis = nl_get_redis();
if(!$obj_redis->open())
{
    echo "redis服务未开启";die;
}
if($_REQUEST['action'] == 'delete')
{
    $params = array(
        'nns_delete' =>1,
    );
    $_REQUEST['nns_id'] = rtrim($_REQUEST['nns_id'],',');
    $arr_nns_id = explode(',',$_REQUEST['nns_id']);
    foreach($arr_nns_id as $nns_id)
    {
    $result[] = nl_redis_asset_import::modify($obj_redis,$nns_id,$params);
    }
    $int_fai_num=$int_suc_num=0;
    foreach($result as $value)
    {
        if($value['ret']==0)
        {
            $int_suc_num++;
        }
        if($value['ret']==1)
        {
            $int_fai_num++;
        }
    }
    echo $int_fai_num;
    $str = $int_suc_num."成功"."--".$int_fai_num."失败";
    echo '<script>alert("'.$str.'");history.go(-1);</script>';die;
}
if($_REQUEST['action'] == 'recover')
{
    $params = array(
        'nns_delete' =>0,
    );
    $_REQUEST['nns_id'] = rtrim($_REQUEST['nns_id'],',');
    $arr_nns_id = explode(',',$_REQUEST['nns_id']);
    foreach($arr_nns_id as $nns_id)
    {
        $result[] = nl_redis_asset_import::modify($obj_redis,$nns_id,$params);
    }
    $int_fai_num=$int_suc_num=0;
    foreach($result as $value)
    {
        if($value['ret']==0)
        {
            $int_suc_num++;
        }
        if($value['ret']==1)
        {
            $int_fai_num++;
        }
    }
    echo $int_fai_num;
    $str = $int_suc_num."成功"."--".$int_fai_num."失败";
    echo '<script>alert("'.$str.'");history.go(-1);</script>';die;
}
$page_info = $params = array ();
$page_info['page_num'] = isset($_GET['page']) ? $_GET['page'] : 1;
$page_info['page_size'] = isset($_GET['page_size']) ? $_GET['page_size'] : (isset($_COOKIE['page_max_num']) && $_COOKIE['page_max_num'] > 0) ? $_COOKIE['page_max_num'] : 18;
$page_info_redis = array(($page_info['page_num']-1)*$page_info['page_size'],$page_info['page_size']);
if (isset($_REQUEST['nns_message_id']) && strlen($_REQUEST['nns_message_id']) > 0)
{
    $params['nns_message_id'] = $_REQUEST['nns_message_id'];
}
if (isset($_REQUEST['nns_index_import_id']) && strlen($_REQUEST['nns_index_import_id']) > 0)
{
    $params['nns_index_import_id'] = $_REQUEST['nns_index_import_id'];
}
if (isset($_REQUEST['nns_media_import_id']) && strlen($_REQUEST['nns_media_import_id']) > 0)
{
    $params['nns_media_import_id'] = $_REQUEST['nns_media_import_id'];
}
if (isset($_REQUEST['nns_state']) && strlen($_REQUEST['nns_state']) > 0)
{
    $params['nns_state'] = $_REQUEST['nns_state'];
}
if (isset($_REQUEST['nns_delete']) && strlen($_REQUEST['nns_delete']) > 0)
{
    $params['nns_delete'] = $_REQUEST['nns_delete'];
}
$params['nns_cp_id'] = $_REQUEST['org_id'];
$url = 'nncms_content_cp_temp_message_list.php?flag_url=1';
foreach ($_REQUEST as $k => $v)
{
    if ($k != 'flag_url' && $k != 'page')
    {
        $url .= '&' . $k . '=' . $v;
    }
}
$result = nl_redis_asset_import::query($obj_redis,$params,$page_info_redis);
//var_dump($result);
$pager = new nl_pager($result['count'], $page_info['page_size'], $page_info['page_num'], $url);
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
    <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
    <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
    <script language="javascript" src="../../js/cms_cookie.js"></script>
    <script language="javascript" src="../../js/table.js.php"></script>
    <script language="javascript" src="../../js/checkinput.js.php"></script>
    <script language="javascript" src="../../js/rate.js"></script>
    <script language="javascript" src="../../js/image_loaded_func.js"></script>
    <script type="text/javascript" src="../../js/dtree.js"></script>
    <script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
    <script language="javascript" src="../../js/cms_datepicker.js"></script>
    <script language="javascript">

        function checkhiddenInput(){
            var par=getAllCheckBoxSelect();
            if (par==""){
                alert("<?php echo "请选择一条信息";?>");
            }else{
                $('#nns_id_delete').attr('value',getAllCheckBoxSelect());
                checkForm('<?php echo cms_get_lang('xtgl_hzgl');?>','<?php echo cms_get_lang('msg_ask_delete');?>',$('#delete_form'));
            }

        }


        function checkhiddenInput_recover(){
            var par=getAllCheckBoxSelect();
            if (par==""){
                alert("<?php echo "请选择一条信息";?>");
            }else{
                $('#nns_id_recover').attr('value',getAllCheckBoxSelect());
                checkForm('<?php echo cms_get_lang('xtgl_hzgl');?>','<?php echo "是否进行本次恢复";?>',$('#recover_form'));
            }
        }

        function refresh_temp_list_page() {
            var num = $("#nns_list_max_num").val();
            window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
        }

        function checkhiddenBox(type) {
            BoxKey = false;
            $("input.checkhiddenInput:checked").each(function() {
                if ($(this).attr('rel') == type) {
                    BoxKey = true;
                    return false;
                }
            })
            return BoxKey;
        }

        function msg_delete_or_recover(str){
            var r=confirm("是否进行该操作");
            if(r == true){
                var ids=getAllCheckBoxSelect()
                alert(ids);
                ids = ids.substr(0,ids.length-1);
                if(ids==""){
                    alert('请选择数据');
                }else{
                    var url = "nncms_content_cp_message_list.php?op_field=nns_delete&op="+ str +"&ids="+ids+"&org_id=<?php echo $_GET['org_id']?>";
                    window.location.href = url;
                }
            }
        }
        $(document).ready(function() {
            $('#clear_time').click(function(){
                $('#create_begin_time').val('');
                $('#create_end_time').val('');

            });
            window.parent.now_frame_url = window.location.href;
        });


    </script>
</head>

<body>
<?php
function pub_func_get_desc_alt($title,$item) {

    $alt_html = '<div style="width:300px;padding:5px;"><div style="font-size:16px;font-weight:bold;line-height:22px;"></div>';
    $alt_html.='<div style="font-size:12px;line-height:20px;">' .
        '<div style="float:left;margin:0px 5px 5px 0px;display:block;">';
    $alt_html.='</div>';
    $alt_html.='<p><b>'.$title.'：</b><br/>';
    $lenth = 24;
    $time = ceil(mb_strlen($item,"utf-8")/$lenth);
    for ($i=0;$i<$time;$i++)
    {
        $alt_html.=mb_substr($item, $i*$lenth, $lenth,"utf-8").'<br/>';
    }
    $alt_html.='</p>';
    $alt_html.='</div></div>';
    $array =array(
        'substr'=>'',
    );
    if(!empty($item))
    {
        $array['substr'] = (mb_strlen($item) > 12) ? mb_substr( $item, 0, 12, 'utf-8' ).'...' : $item;
    }
    $array['html'] =  htmlspecialchars($alt_html);
    return $array;
}

?>
<div class="content">
    <div class="content_position">
        <?php echo 'CP临时消息管理 > '.$_GET['org_id'] ;?>
    </div>
    <div class="content_table">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <form action="" method="get">
                <input type="hidden" name="org_id" value="<?php echo $_REQUEST['org_id'] ?>">
                <tbody>
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;消息ID：&nbsp;&nbsp;<input type="text" name="nns_message_id" id="nns_message_id" style="width: 270px" value="<?php  if(isset($_GET['nns_message_id'])) echo $_GET['nns_message_id'];?>" />
                        &nbsp;&nbsp;&nbsp;分集注入ID：&nbsp;&nbsp;<input type="text" name="nns_index_import_id" id="nns_index_import_id" style="width: 270px" value="<?php  if(isset($_GET['nns_index_import_id'])) echo $_GET['nns_index_import_id'];?>" />
                        &nbsp;&nbsp;&nbsp;片源注入ID：&nbsp;&nbsp;<input type="text" name="nns_media_import_id" id="nns_media_import_id" style="width: 270px" value="<?php  if(isset($_GET['nns_media_import_id'])) echo $_GET['nns_media_import_id'];?>" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注入状态：
                        <select name="nns_state"  id="nns_state" style="width: 150px;">
                            <option value="">全部</option>
                            <option <?php if($_GET['nns_state'] == '0') echo 'selected="selected"'?> value="0">未注入资源库</option>
                            <option <?php if($_GET['nns_state'] == '1') echo 'selected="selected"'?> value="1">已注入资源库</option>
                        </select>
                        &nbsp;&nbsp;&nbsp;删除状态：
                        <select name="nns_delete"  id="nns_delete" style="width: 110px;">
                            <option value="">全部</option>
                            <option <?php if($_GET['nns_delete'] == '0') echo 'selected="selected"'?> value="0">未删除</option>
                            <option <?php if($_GET['nns_delete'] == '1') echo 'selected="selected"'?> value="1">已删除</option>
                        </select>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="submit" value="<?php echo cms_get_lang('search'); ?>"  onclick="refresh_temp_list_page();"/>
                    </td>
                </tr>
                </tbody>
            </form>
        </table>
    </div>
    <div class="content_table formtable">

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <th><input name="" type="checkbox" value="" /></th>
                <th width="30px">序号</th>
                <th>消息ID</th>
                <th>分集注入ID</th>
                <th>片源注入ID</th>
                <th>注入状态</th>
                <th width="60px">删除状态</th>
                <th>注入内容</th>
                <th>注入时间</th>
                <th>修改时间</th>
                <th width="60px">查看XML</th>
                <th width="30px">操作</th>
            </tr>
            </thead>
            <tbody>
            <?php
            #$db = nn_get_db(NL_DB_READ);
            if($result['data']!=null){
                $num = ($page_num - 1) * $page_size;
                if(is_array($result['data'])){
                    foreach ($result['data'] as $item) {
                        $num++;
                        ?>
                        <tr>
                            <td><input name="input" type="checkbox" value="<?php echo  $item["#id"];?>" /></td>
                            <td><?php echo $num; ?></td>

                            <td >
                                <?php echo $item['nns_message_id']; ?>
                            </td>
                            <td >
                                <?php echo $item['nns_index_import_id']; ?>
                            </td>
                            <td >
                                <?php echo $item['nns_media_import_id']; ?>
                            </td>
                            <td>
                                <?php
                                if($item['nns_state']=='0'){
                                    echo '<font color="#FF0000">未注入资源库</font>';
                                }elseif($item['nns_state']=='1'){
                                    echo '已注入资源库';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if($item['nns_delete']=='0'){
                                    echo '未删除';
                                }elseif($item['nns_delete']=='1'){
                                    echo '<font color="#FF0000">已删除</font>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php $arr_desc_info= pub_func_get_desc_alt('注入内容',$item['nns_content']);?>
                                <alt alt="<?php echo $arr_desc_info['html']; ?>">
                                    <?php echo $arr_desc_info['substr'];
                                    ?>
                                </alt>
                            </td>
                            <td>
                                <?php echo date("Y-m-d H:i:s",$item['nns_create_time']); ?>
                            </td>
                            <td>
                                <?php echo date("Y-m-d H:i:s",$item['nns_modify_time']); ?>
                            </td>
                            <td>
                                <a href="./nncms_content_cp_temp_message_view.php?nns_cp_id=<?php echo $_REQUEST['org_id'];?>&nns_message_id=<?php echo $item['nns_message_id'];?>" target="_blank">点击查看</a>
                            </td>
                            <td>
                                <?php if($item['nns_delete']==1){?>
                                    <a onclick="return confirm('确认恢复该信息吗？')" href="nncms_content_cp_temp_message_list.php?action=recover&nns_id=<?php echo $item["#id"];?>">恢复</a>
                                <?php }elseif($item['nns_delete']==0){ ?>
                                    <a onclick="return confirm('确认删除该信息吗？')" href="nncms_content_cp_temp_message_list.php?action=delete&nns_id=<?php echo $item['#id'];?>">删除</a>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
            }$least_num = $g_manager_list_max_num - count($data);
            for ($i = 0; $i < $least_num; $i++) {
                ?>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <?php echo $pager->nav();?>
    <div class="controlbtns">
        <div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
        <div class="controlbtn delete"><a href="javascript:checkhiddenInput();"  action="delete">删除</a></div>
        <div class="controlbtn back"><a href="javascript:checkhiddenInput_recover();"  action="recover">恢复</a></div>
        <form id="delete_form" action="nncms_content_cp_temp_message_list.php" method="post">
            <input name="action" id="action" type="hidden" value="delete" />
            <input name="nns_id" id="nns_id_delete" type="hidden" value="" />
        </form>
        <form id="recover_form" action="nncms_content_cp_temp_message_list.php" method="post">
            <input name="action" id="action" type="hidden" value="recover" />
            <input name="nns_id" id="nns_id_recover" type="hidden" value="" />
        </form>
    </div>
</div>
</body>
</html>