<?php 
include ("../../nncms_manager_inc.php");
include_once $np_path . 'np/np_xml_to_array.class.php';
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include_once $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include_once $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}

include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/v2/common.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/v2/ns_core/m_public.class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
$queue_task_model = new queue_task_model();
$id = $_GET['nns_id'];
$type = $_GET['nns_type'];
$params['nns_id'] = $id;

$message_info = $queue_task_model->get_cp_message_list($params);
$message_info = $message_info['data'][0];
if ($type == '2')
{
	if(strlen($message_info['nns_message_content'])<1 && isset($message_info['nns_message_url']) && strlen($message_info['nns_message_url'])>0)
	{
	    $message_info['nns_message_content'] = file_get_contents(dirname(dirname(dirname(dirname(__FILE__)))).'/data/'.$message_info['nns_message_url']);
	}
    $str = $message_info['nns_message_content'];
	
}
else if ($type == '1')
{
	$xml_data = np_xml_to_array::parse2($message_info['nns_message_xml']);
	$xml_data['url'] = 'ftp://cmsftp:cmsftp@10.1.204.115/xml/' . $xml_data['url'];
	$url_arr = parse_url($xml_data['url']);
	$save_path = dirname(dirname(dirname(dirname(__FILE__)))) . '/data/log/iptv/import_xml/' . $url_arr["path"];
	if (file_exists($save_path))
	{
		 $str = file_get_contents($save_path);
	}
	else
	{
		 $str = '<?xml version="1.0" encoding="utf-8"?><result>no file</result>';
	}
}
else if ($type == '0')
{
    if(strlen($message_info['nns_message_xml'])<1 && isset($message_info['nns_bk_queue_excute_url']) && strlen($message_info['nns_bk_queue_excute_url'])>0)
    {
        $message_info['nns_message_xml'] = file_get_contents(dirname(dirname(dirname(dirname(__FILE__)))).'/data/'.$message_info['nns_bk_queue_excute_url']);
    }
	$str = $message_info['nns_message_xml'];
}
if(m_public::is_xml($str))
{
    header("Content-Type:text/xml;charset=utf-8");
    echo $str;
}
else
{
    header("Content-Type:text/html;charset=utf-8");
    $str = str_replace("\r\n", "<br/>", $str);
    echo $str;
}

