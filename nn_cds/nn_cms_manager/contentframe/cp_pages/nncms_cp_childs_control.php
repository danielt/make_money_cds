<?php
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
include $nncms_config_path . 'nn_cms_manager/controls/nncms_controls_public_function.php';
include $nncms_config_path . 'nn_cms_manager/controls/nncms_controls_pinyin_class.php';
require_once $nncms_db_path . 'nns_log' . DIRECTORY_SEPARATOR . 'nns_db_op_log_class.php';
$log_inst = new nns_db_op_log_class();

$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE, 
		'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$dc->open();
$params = $_REQUEST;
$action = $params['action'];
unset($params['action']);
$log_params = (!empty($params) && is_array($params)) ? json_encode($params) : '';
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);


/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/sp/sp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp_childs.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/common/pinyin.class.php';
session_start();
switch ($action)
{
	
	case "add":
	    $params['nns_name'] = trim($params['nns_name']);
	    $str_pinyin = nns_controls_pinyin_class::get_pinyin_letter($params['nns_name']);
	    if(strlen($str_pinyin) <1 || strlen($params['nns_name']) <1)
	    {
	        echo "<script>alert('拼音长度为空');history.go(-1);</script>";
	        break;
	    }
	    $result_cp_data = nl_cp::query_by_id($dc, $params['nns_cp_id']);
	    if(!isset($result_cp_data['data_info']) || empty($result_cp_data['data_info']) || !is_array($result_cp_data['data_info']))
	    {
	        echo "<script>alert('查询父级cp未数据');history.go(-1);</script>";
	        break;
	    }
	    $result_cp_data = $result_cp_data['data_info'];
	    $params['nns_cp_child_name'] = $result_cp_data['nns_name'].'-'.$params['nns_name'];
	    $params['nns_cp_child_id'] = $result_cp_data['nns_id'].'_'.$str_pinyin;
		$check_param = array(
			'nns_cp_id' => $params['nns_cp_id'],
		    'nns_name' => $params['nns_name'],
		);
	    $result_unique = nl_cp_childs::query_data($dc, $check_param);
        if($result_unique['ret'] !=0)
	    {
	        echo "<script>alert('" . $result_unique['reason'] . "');history.go(-1);</script>";
	        break;
	    }
	    if(isset($result_unique['data_info']) && is_array($result_unique['data_info']) && !empty($result_unique['data_info']))
	    {
	        echo "<script>alert('数据已经存在');history.go(-1);</script>";
	        break;
	    }
	    $check_param = array(
	        'nns_cp_child_id' => $params['nns_cp_child_id'],
	    );
	    $result_unique = nl_cp_childs::query_data($dc, $check_param);
	    if($result_unique['ret'] !=0)
	    {
	        echo "<script>alert('" . $result_unique['reason'] . "');history.go(-1);</script>";
	        break;
	    }
	    if(isset($result_unique['data_info']) && is_array($result_unique['data_info']) && !empty($result_unique['data_info']))
	    {
	        echo "<script>alert('数据已经存在');history.go(-1);</script>";
	        break;
	    }
	    $params['nns_id'] = np_guid_rand();
		$result = nl_cp_childs::add($dc, $params);
		if ($result['ret'])
		{
			echo "<script>alert('" . $result['reason'] . "');</script>";
		}
		else
		{
		    $result_cp_child = nl_cp::query_by_id($dc, $params['nns_cp_child_id']);
		    if(!isset($result_cp_child['data_info']) || !is_array($result_cp_child['data_info']) && empty($result_cp_child['data_info']))
		    {
		        $add_data = $result_cp_data;
		        $add_data['nns_id'] =  $params['nns_cp_child_id'];
		        $add_data['nns_name'] =  $params['nns_cp_child_name'];
		        unset($add_data['nns_create_time']);
		        unset($add_data['nns_modify_time']);
		        unset($add_data['nns_guid']);
		        $result = nl_cp::add($dc, $add_data);
		        if ($result['ret'])
		        {
		            echo "<script>alert('" . $result['reason'] . "');</script>";
		        }
		        else
		        {
		            $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "添加:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
		            echo "<script>alert('添加成功');</script>";
		        }
		    }
		}
		break;
	case "delete":
		$nns_ids = explode(",", $params['nns_id']);
		$arr_temp_del = array();
		$str_other=$str_desc=null;
		foreach ($nns_ids as $nnsid)
		{
			if (strlen($nnsid) < 1)
			{
				continue;
			}
			$check_param = array(
			    'nns_id' => $nnsid,
			);
			$result_unique = nl_cp_childs::query_data($dc, $check_param);
			if(isset($result_unique['data_info']) && is_array($result_unique['data_info']) && !empty($result_unique['data_info']))
			{
			     nl_cp::delete($dc, $result_unique['data_info'][0]['nns_cp_child_id']);
			}
			#TODO
			$result_del=nl_cp_childs::delete($dc,$nnsid);
			if($result_del['ret'] != 0)
			{
				echo "<script>alert('".$result_del['reason']."');history.go(-1);</script>";die;
			}
		}
		$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "删除:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
		$str_dec_last = empty($str_desc) ? "删除成功" : $str_desc;
		echo "<script>alert('".$str_dec_last."');</script>";
		break;
	default:
	    echo "<script>alert('没有此方式');</script>";
		break;
}
$partner_inst = null;
$log_inst = null;
echo "<script>self.location='".$base_file_name_base."_list.php';</script>";
