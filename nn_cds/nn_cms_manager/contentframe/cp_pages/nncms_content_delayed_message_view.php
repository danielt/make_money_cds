<?php
if((!isset($_GET['action']) || empty($_GET['action'])) && $_GET['header_type'] == 'xml')
{
	header("Content-Type:text/xml;charset=utf-8");
}
else if(!isset($_GET['action']) || empty($_GET['action']))
{
	header("Content-Type:text/html;charset=utf-8");
}
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"]))
{
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/np/np_redis_check.class.php';
$dc = nl_get_dc(array(
		'db_policy' => NL_DB_READ | NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$dc->open();
$cp_id = $_GET['cp_id'];
//获取CP配置
$cp_re = nl_cp::query_by_id($dc, $cp_id);

$cp_config = json_decode($cp_re['data_info']['nns_config'],true);
$delayed_message_import_overtime = (int)$cp_config['delayed_message_import_overtime'] > 0 ? $cp_config['delayed_message_import_overtime'] : 86400;

$config = array(
		"queue_name" => $cp_id,
		"command_lose_time" => $delayed_message_import_overtime
);
$redis_obj = nl_get_redis();
$redis_check = new np_redis_check_class($redis_obj, $config);
if(isset($_GET['action']) && $_GET['action'] == 'export') //导出
{
    $total_num  = (int)$_GET['total_num'] > 0 ? $_GET['total_num'] : 1;
    $result = $redis_check->get_list(0,$total_num);
    $i = 0;
    $delayed_arr = array(
//        array(cms_get_lang('segnumber'),'原始ID','分集号','操作类型'),
        array('原始ID','分集号'),
    );
    $arr = array();
    if(is_array($result['data']) && !empty($result['data']))
    {
        foreach ($result['data'] as $value)
        {
            $command = json_decode($value['command'], true);
//            switch ($command['action']) {
//                case 'assest_package':
//                    $action = '媒资打包';
//                    break;
//                case 'online':
//                    $action = '上线';
//                    break;
//                case 'unline':
//                    $action = '下线';
//                    break;
//                default:
//                    $action = $command['action'];
//                    break;
//            }
//            $delayed_arr[] = array($i,$command['import_id'],$command['index'],$action);
            $arr[$command['import_id']] = array($command['import_id'],$command['index']);
        }
    }
    $delayed_arr = array_merge($delayed_arr,array_values($arr));
    $xls = new Excel_XML('UTF-8',false,cms_get_lang('count'));
    $xls->addArray($delayed_arr);
    $xls->generateXML('delayed_message_queue');
}
else
{
    $start = (int)$_GET['num'] - 1;
    $end = $start;
    $result = $redis_check->get_list($start,$end);
    echo $result["data"][0]['command'];
}