<?php
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$nns_id=$_GET["nns_id"];
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"134100");
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");


include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
include $nncms_config_path. "nn_cms_config/nn_cms_global.php";
include $nncms_config_path. '/nn_logic/cp/cp.class.php';
include $nncms_config_path.'nn_cms_manager/controls/nncms_controls_public_function.php';
 
$dc = nl_get_dc(array(
	'db_policy'=>NL_DB_READ,
	'cache_policy'=>NP_KV_CACHE_TYPE_NULL
));
$dc->open();

if(strlen($nns_id)<1){
	echo "<script>alert('参数有误');self.location='nncms_content_cp_info_list.php';</script>";exit;
}

$data = nl_cp::query_by_id($dc,$nns_id);

if(!is_array($data['data_info'])){
	echo "<script>alert('没有找到您要的信息');self.location='nncms_content_cp_info_list.php';</script>";exit;
}
$data = $data['data_info'];
$data['nns_config'] = !empty($data['nns_config']) ? json_decode($data['nns_config'],true) : null;
// var_dump($data);die;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/radiolist.js"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript">

</script>
</head>

<body>


<div class="content">
	<div class="content_position">CP详细信息</div>
    <div class="content_table">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
		<tr>
			<td class="rightstyle">ID <font style="color: #F00; font-size: 16px; font-weight: bold;">*</font></td>
			<td><?php echo $data["nns_id"]?></td>
		</tr>
		<tr>
			<td class="rightstyle">CP 名称 <font style="color: #F00; font-size: 16px; font-weight: bold;">*</font></td>
			<td><?php echo $data["nns_name"]?></td>
		</tr>
		<tr>
			<td class="rightstyle">上游文件包资源库展示开关</td>
			<td><?php if(isset($data['nns_file_state_enable']) && $data['nns_file_state_enable'] == 1){ echo "开启";}else{ echo "关闭";};?> 
		     &nbsp;&nbsp;&nbsp;&nbsp;【上游文件包后台资源展示开关，默认关闭】</td>
		</tr>
		<tr>
                <td class="rightstyle"><?php echo $language_manager_table_lxr;?></td>
                <td><?php echo $data["nns_contactor"];?></td>
          </tr>
          <tr>
                <td class="rightstyle"><?php echo $language_manager_table_lxdh;?></td>
                <td><?php echo $data["nns_telephone"];?></td>
          </tr>
          <tr>
                <td class="rightstyle"><?php echo $language_manager_table_email;?></td>
                <td><?php echo $data["nns_email"];?></td>
          </tr>
          <tr>
                <td class="rightstyle"><?php echo $language_manager_table_lxdz;?></td>
                <td><?php echo $data["nns_addr"]?></td>
          </tr>
          
          <tr>
                <td class="rightstyle"><?php echo $language_manager_global_ms;?></td>
                <td><?php echo $data["nns_desc"];?></td>
          </tr>
		<tr>
			<td class="rightstyle"><b>全局配置</b></td>
			<td> </td>
		</tr>
		<tr>
			<td class="rightstyle">消息队列获取消息ID模式</td>
			<td><?php echo $data['nns_config']['message_import_message_model'];?> 
			【为空或者不配置或者为0 查询存在（cp+messageid）则修改;1注入一条数据直接加一条;2查询存在则什么都不操作】</td>
		</tr>
		<tr>
			<td class="rightstyle">片源队列注入开关</td>
			<td><?php if(isset($data['nns_config']['message_import_enable']) && $data['nns_config']['message_import_enable'] == 1){ echo "开启";}else{ echo "关闭";};?> 
			&nbsp;&nbsp;&nbsp;&nbsp;【关闭  (片源正常注入); 开启 (依据下方片源注入队列模式，限制片源注入队列)】</td>
		</tr>
		<?php if($data['nns_config']['message_import_enable']==1){?>
			<tr>
				<td class="rightstyle">片源队列注入模式</td>
				<td><?php echo $data['nns_config']['message_import_mode'];?> 【1 : content ID不为空注入队列，反之不注入队列;】</td>
			</tr>
			<tr>
				<td class="rightstyle">片源队列注入时间范围控制</td>
				<td><?php echo $data['nns_config']['message_import_time_control'];?> 【格式9:30-15:30（24小时制无前置0,多个|分隔，为空不限制范围）】</td>
			</tr>
		<?php }?>
		
		<tr>
			<td class="rightstyle">HTTP消息注入开关</td>
			<td><?php if(isset($data['nns_config']['message_http_import_enable']) && $data['nns_config']['message_http_import_enable'] == 1){ echo "开启";}else{ echo "关闭";};?> 
			&nbsp;&nbsp;&nbsp;&nbsp;【关闭  (不接收MQ消息); 开启 (接收MQ消息)】</td>
		</tr>
		<?php if($data['nns_config']['message_http_import_enable']==1){?>
			<tr>
				<td class="rightstyle">消息队列获取数据条数</td>
				<td><?php echo $data['nns_config']['message_http_import_num'];?> 【HTTP消息队列获取条数，不设置默认获取100条】</td>
			</tr>
			<tr>
				<td class="rightstyle">消息队列地址</td>
				<td><?php echo $data['nns_config']['message_http_import_url'];?> 【HTTP消息队列服务器拉取地址】</td>
			</tr>
		<?php }?>
		
		
		<tr>
			<td class="rightstyle">MQ消息注入开关</td>
			<td><?php if(isset($data['nns_config']['message_queue_import_enable']) && $data['nns_config']['message_queue_import_enable'] == 1){ echo "开启";}else{ echo "关闭";};?> 
			&nbsp;&nbsp;&nbsp;&nbsp;【关闭  (不接收MQ消息); 开启 (接收MQ消息)】</td>
		</tr>
		<?php if($data['nns_config']['message_queue_import_enable']==1){?>
			<tr>
				<td class="rightstyle">消息队列获取数据条数</td>
				<td><?php echo $data['nns_config']['message_queue_import_num'];?> 【MQ消息队列获取条数，不设置默认获取100条】</td>
			</tr>
			<tr>
				<td class="rightstyle">消息队列地址</td>
				<td><?php echo $data['nns_config']['message_queue_import_host'];?> 【MQ消息队列服务器IP地址】</td>
			</tr>
			<tr>
				<td class="rightstyle">消息队列端口号</td>
				<td><?php echo (isset($data['nns_config']['message_queue_import_port']) && strlen($data['nns_config']['message_queue_import_port']) >0) ? $data['nns_config']['message_queue_import_port'] : 5672;?> 【MQ消息队列服务器端口号,为空默认为5672】</td>
			</tr>
			<tr>
				<td class="rightstyle">消息队列账号</td>
				<td><?php echo $data['nns_config']['message_queue_import_user'];?> 【MQ消息队列账号】</td>
			</tr>
			<tr>
				<td class="rightstyle">消息队列端密码</td>
				<td><?php echo $data['nns_config']['message_queue_import_pass'];?> 【MQ消息队列密码】</td>
			</tr>
			<tr>
				<td class="rightstyle">消息队列路径</td>
				<td><?php echo $data['nns_config']['message_queue_import_vhost'];?> 【MQ消息队列服务器路径】</td>
			</tr>
			<tr>
				<td class="rightstyle">消息队列路由名</td>
				<td><?php echo $data['nns_config']['message_queue_import_channel'];?> 【MQ消息队列路由名】</td>
			</tr>
			<tr>
				<td class="rightstyle">消息队列交换名</td>
				<td><?php echo $data['nns_config']['message_queue_import_exchange'];?> 【MQ消息队列交换名】</td>
			</tr>
			<tr>
				<td class="rightstyle">消息队列数据格式</td>
				<td><?php echo $data['nns_config']['message_queue_data_format'];?> 【消息队列数据格式】</td>
			</tr>
		<?php }?>
		<?php if($data['nns_config']['message_ftp_import_enable']==1){?>
			<tr>
				<td class="rightstyle">读取FTP文件的后缀</td>
				<td><?php echo $data['nns_config']['message_ftp_import_file_ex'];?> 【未设置默认为xml的文件后缀】</td>
			</tr>
			<tr>
				<td class="rightstyle">FTP拉取的地址信息：</td>
				<td><?php echo $data['nns_config']['message_ftp_import_file_url'];?> 【FTP拉取文件夹的ftp地址】</td>
			</tr>
			<tr>
				<td class="rightstyle">FTP拉取memcache缓存时间：</td>
				<td><?php echo $data['nns_config']['message_ftp_import_mem_time'];?> 【FTP拉取memcache缓存时间如果为配置默认1200秒】</td>
			</tr>
		<?php }?>
		<tr>
			<td class="rightstyle">产品包注入开关</td>
			<td><?php if(isset($data['nns_config']['message_queue_import_product_enable']) && $data['nns_config']['message_queue_import_product_enable'] == 1){ echo "开启";}else{ echo "关闭";};?>
				&nbsp;&nbsp;&nbsp;&nbsp;【关闭  (不接受产品包注入信息); 开启 (接受产品包注入信息)】</td>
		</tr>
		<?php if($data['nns_config']['message_queue_import_product_enable']==1){?>
			<tr>
				<td class="rightstyle">产品同步注入CMS地址</td>
				<td><?php echo $data['nns_config']['import_product_to_cms_url'];?> 【产品同步注入CMS地址】</td>
			</tr>
		<?php }?>
        <tr>
            <td class="rightstyle">第三方FTP扫描工单开关</td>
            <td><?php if(isset($data['nns_config']['third_ftp_scan_enabled']) && $data['nns_config']['third_ftp_scan_enabled'] == 1){ echo "开启";}else{ echo "关闭";};?>
                &nbsp;&nbsp;&nbsp;&nbsp;【上游发送的工单任务是否在固定的目录生成第三方FTP扫描工单，关闭|未配置  不生成; 开启 生成到对应的目录中】</td>
        </tr>
        <?php if($data['nns_config']['third_ftp_scan_enabled']==1){?>
            <tr>
                <td class="rightstyle">第三方FTP扫描工单地址</td>
                <td><?php echo $data['nns_config']['third_ftp_scan_url'];?> 【未配置时默认在本地/data/log_v2/ftp_scan目录下】</td>
            </tr>
        <?php }?>
		  <tr>
				<td class="rightstyle"><b>CDN接收管理配置</b></td>
				<td> </td>
		  </tr>
		  <tr>
			<td class="rightstyle">CDN接收开关</td>
			<td><?php if(isset($data['nns_config']['cdn_receive_import_enable']) && $data['nns_config']['cdn_receive_import_enable'] == 1){ echo "开启";}else{ echo "关闭";};?> 
			&nbsp;&nbsp;&nbsp;&nbsp;【关闭  (片源正常注入); 开启 (TV平台接收信息注入到CDN平台，并反馈TV播放串)】</td>
		  </tr>
		  <tr>
			<td class="rightstyle">CDN处理直播频道开关</td>
			<td><?php if(isset($data['nns_config']['cdn_handele_live_enable']) && $data['nns_config']['cdn_handele_live_enable'] == 1){ echo "开启";}else{ echo "关闭";};?> 
			&nbsp;&nbsp;&nbsp;&nbsp;【关闭  (接收但是不处理); 开启 (接收并且处理)】</td>
		  </tr>
		  <tr>
			<td class="rightstyle">CDN处理直播分集开关</td>
			<td><?php if(isset($data['nns_config']['cdn_handele_live_index_enable']) && $data['nns_config']['cdn_handele_live_index_enable'] == 1){ echo "开启";}else{ echo "关闭";};?> 
			&nbsp;&nbsp;&nbsp;&nbsp;【关闭  (接收但是不处理); 开启 (接收并且处理)】</td>
		  </tr>
		  <tr>
			<td class="rightstyle">CDN处理直播片源开关</td>
			<td><?php if(isset($data['nns_config']['cdn_handele_live_media_enable']) && $data['nns_config']['cdn_handele_live_media_enable'] == 1){ echo "开启";}else{ echo "关闭";};?> 
			&nbsp;&nbsp;&nbsp;&nbsp;【关闭  (接收但是不处理); 开启 (接收并且处理)】</td>
		  </tr>
		  <tr>
			<td class="rightstyle">CDN处理直播片源模板</td>
			<td><?php echo $data['nns_config']['cdn_handele_live_media_template'];?></td>
		  </tr>
		  <tr>
			<td class="rightstyle">CDN处理节目单开关</td>
			<td><?php if(isset($data['nns_config']['cdn_handele_playbill_enable']) && $data['nns_config']['cdn_handele_playbill_enable'] == 1){ echo "开启";}else{ echo "关闭";};?> 
			&nbsp;&nbsp;&nbsp;&nbsp;【关闭  (接收但是不处理); 开启 (接收并且处理)】</td>
		  </tr>
		  <tr>
			<td class="rightstyle">节目单注入下游CMS模式</td>
			<td><?php if(isset($data['nns_config']['live_playbill_cms_model']) && $data['nns_config']['live_playbill_cms_model'] == 1){ echo "以直播频道的注入ID为准";}else{ echo "以直播源的注入ID为准";};?> 
			&nbsp;&nbsp;&nbsp;&nbsp;【0|未配置  (节目单注入后用直播源的注入ID反查信息); 1 (节目单注入后用直播频道的注入ID反查信)】
			</td>
		  </tr>
          <tr>
				<td class="rightstyle"><b>站点配置</b></td>
				<td> </td>
			</tr>
			<tr>
				<td class="rightstyle">站点ID</td>
				<td><?php echo $data['nns_config']['site_id'];?></td>
			</tr>
			<tr>
				<td class="rightstyle">站点反馈接口</td>
				<td><?php echo $data['nns_config']['site_callback_url'];?></td>
			</tr>
			
<!--			<tr>-->
<!--				<td class="rightstyle">站点注入ID</td>-->
<!--				-->
<!--				<td>--><?php //echo $data['nns_config']['import_id_mode'];?><!--(0:guid;1:数字id;2:原始id;3:内容数字组合ID)</td>-->
<!--			</tr>-->
<!--			<tr>-->
<!--				<td class="rightstyle">内容提供商CSPID</td>-->
<!--				-->
<!--				<td>--><?php //echo $data['nns_config']['import_csp_id'];?><!--&nbsp;&nbsp;&nbsp;&nbsp;内容提供商CSPID</td>-->
<!--			</tr>-->
			<tr>
				<td class="rightstyle">CDN是否通知第三方外部厂家</td>
				<td>
					<?php if(!isset($edit_data['nns_config']['cdn_notify_third_party_enable']) || (isset($edit_data['nns_config']['cdn_notify_third_party_enable']) && $edit_data['nns_config']['cdn_notify_third_party_enable'] != '1')){echo "开启";}else{echo "关闭";} ?>   
					【开启:需要通知;关闭:不需通知，默认开启 （目前新疆电信对接福富用到）】
				</td>
			</tr>
			<tr>
				<td class="rightstyle">内容反馈模式</td>
				
				<td><?php echo $data['nns_config']['content_feedback_mode'];?>(0:不反馈;1:失败时反馈;2:始终反馈)</td>
			</tr>
			<tr>
				<td class="rightstyle">原始CP是否下发</td>
				
				<td><?php echo $data['nns_config']['original_cp_enabled'];?>  0或则未配置下发当前配置CP;1下发原始CP(producer);2下发默认0的CP</td>
			</tr>
			<tr>
				<td class="rightstyle">无条件注入分集/片源<font style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
				<td><?php echo $data['nns_config']['check_metadata_enabled'];?> 0:注入ID重复也可按照新增分集、片源注入;1:原逻辑注入，需判断注入ID+import_source是否存在;</td>
			</tr>				
			<tr>
				<td class="rightstyle">上游分集任务是否同步删除分集下片源</td>
				<td><?php echo $data['nns_config']['sync_delete_media_with_index'];?> 0或不设置默认同步删除当前分集下片源;1:不删除当前分集下片源;</td>
			</tr>
		
			<tr>
				<td class="rightstyle"><b>消息配置</b></td>
				<td> </td>
			</tr>
			<tr>
				<td class="rightstyle">消息队列第一次消息注入的条数</td>
				<td><?php echo $data['nns_config']['message_first_import_num'];?> (消息队列中消息状态为等待注入的消息获取条数上限)</td>
			</tr>
			<tr>
				<td class="rightstyle">失败消息注入开关</td>
				<td><?php echo $data['nns_config']['fail_message_import_enable'];?> &nbsp;&nbsp;&nbsp;&nbsp;(0|未配置 关闭, 1 开启;是获取消息队列中状态为注入失败的消息,开启为获取)</td>
			</tr>
		<?php if($data['nns_config']['fail_message_import_enable']==1){?>
			<tr>
				<td class="rightstyle">失败消息注入条数</td>
				<td><?php echo $data['nns_config']['fail_message_import_num'];?> (从消息队列中获取失败消息的条数上限)</td>
			</tr>
		<?php }?>
			<tr>
				<td class="rightstyle">消息失败重发次数</td>
				<td><?php echo $data['nns_config']['fail_message_time'];?> (消息失败重发次数)</td>
			</tr>
			<tr>
				<td class="rightstyle">消息队列子级注入开关</td>
				<td><?php echo $data['nns_config']['message_child_import_enable'];?> &nbsp;&nbsp;&nbsp;&nbsp;(0|未配置 关闭, 1	开启;  是否获取主媒资未注入、分集未注入消息状态的消息，开启为获取)</td>
			</tr>
		<?php if($data['nns_config']['message_child_import_enable']==1){?>
			<tr>
				<td class="rightstyle">消息队列子级注入条数</td>
				<td><?php echo $data['nns_config']['message_child_import_num'];?> (获取主媒资未注入、分集未注入消息状态消息的条数上限)</td>
			</tr>
		<?php } ?>
			<tr>
				<td class="rightstyle">解析版本</td>
				<td>
					<input disabled type="radio" value="0" <?php if(!isset($data['nns_config']['message_parse_version']) || empty($data['nns_config']['message_parse_version'])){echo "checked='checked'";}?>/>  解析版本V2【默认】
					<input disabled type="radio" value="1" <?php if(isset($data['nns_config']['message_parse_version']) && $data['nns_config']['message_parse_version'] == 1){echo "checked='checked'";}?>/>  解析版本V4
				</td>
			</tr>
			<tr>
				<td class="rightstyle">消息解析模式</td>
				<td><?php echo $data['nns_config']['message_parse_model'];?> 【参照config_v2注入来源清单】
				</td>
			</tr>
            <tr>
                <td class="rightstyle">消息生成注入队列的优先级</td>
                <td><?php echo $data['nns_config']['source_import_priority_level'];?> 【注入队列则根据来源配置的优先级按从高到低的顺序执行（当多个来源对同一个下游分发时使用）】
                </td>
            </tr>
            <tr>
				<td class="rightstyle">临时消息开关</td>
				<td><?php echo $data['nns_config']['temp_message_enabled'];?>&nbsp;&nbsp;&nbsp;&nbsp;0|未配置关闭 1 开启</td>
			</tr>
			<tr>
				<td class="rightstyle">消息分组开关</td>
				<td><?php echo $data['nns_config']['message_group_enabled'] == 1 ? "开启" : "关闭";?>&nbsp;&nbsp;&nbsp;&nbsp;
				【厂家注入一个XML存在多部片的情况,开启:全部处理完毕后一条记录反馈;关闭:原逻辑 单条反馈,默认关闭】</td>
			</tr>
			<tr>
				<td class="rightstyle">限制消息超时时限开关</td>
				<td><?php if(isset($data['nns_config']['message_import_overtime_enabled']) && $data['nns_config']['message_import_overtime_enabled'] == 1){ echo "开启";}else{ echo "关闭";};?> 
				</td>
			</tr>
			<?php if($data['nns_config']['message_import_overtime_enabled']==1){?>
			<tr>
				<td class="rightstyle">消息注入超时时限</td>
				<td><?php echo $data['nns_config']['message_import_overtime'];?> 【以注入播控平台多长时间为超时，单位秒】</td>
			</tr>
		<?php } ?>
			<tr>
				<td class="rightstyle">异步消息队列开关</td>
				<td><?php if(isset($data['nns_config']['asyn_feedback_message_enabled']) && $data['nns_config']['asyn_feedback_message_enabled'] == 1){ echo "开启";}else{ echo "关闭";};?>
				</td>
			</tr>
         <tr>
            <td class="rightstyle">异步下载上游ftp工单开关</td>
            <td><?php if(isset($data['nns_config']['asyn_download_ftp_enabled']) && $data['nns_config']['asyn_download_ftp_enabled'] == 1){ echo "开启";}else{ echo "关闭";};?>
                【是否异步下载上游ftp工单 0:否 1:是】
            </td>
        </tr>
			</tr>
				<td class="rightstyle">消息延时注入队列开关</td>
				<td><?php echo $data['nns_config']['delayed_message_import_enabled'];?>&nbsp;&nbsp;&nbsp;&nbsp;0|未配置关闭 1 开启【REDIS控制队列，用于上下线、媒资打包、推荐位等队列】</td>
			</tr>
			<?php if($data['nns_config']['delayed_message_import_enabled']==1){?>
			<tr>
				<td class="rightstyle">消息注入超时时限</td>
				<td><?php echo $data['nns_config']['delayed_message_import_overtime'];?> 【延时队列超时上限,单位秒(不配置默认86400S)】</td>
			</tr>
		<?php } ?>
		
			<tr>
				<td class="rightstyle"><b>中心流程方向配置</b></td>
				<td> </td>
			</tr>
			<tr>
				<td class="rightstyle">自动审核开关：</td>
				
				<td><?php echo $data['nns_config']['auto_audit_enable'];?>	(0或则未配置关闭 ;1 开启)</td>
			</tr>
			<tr>
				<td class="rightstyle">媒资审核结构：</td>
				
				<td><?php echo $data['nns_config']['audit_model'];?>	(0或则未配置则主媒资、分集、片源注入审核队列;1主媒资、分集注入审核队列;2主媒资注入审核队列)</td>
			</tr>
			<tr>
				<td class="rightstyle">敏感词库过滤开关：</td>
				
				<td><?php echo $data['nns_config']['sensitive_word_enabled'];?>	(0或者不配置为需通过敏感词库筛选;1为不通过敏感词库筛选)</td>
			</tr>
			<tr>
				<td class="rightstyle">审核流程：</td>
				
				<td><?php echo $data['nns_config']['flow_audit'];?>	(0或则未配置走以前的流程;1审核不注入cdn)</td>
			</tr>
			<tr>
				<td class="rightstyle">审核反馈方式：</td>
				
				<td><?php echo $data['nns_config']['flow_audit_notify'];?>	(0或则未配置则不反馈;1 amqp反馈)</td>
			</tr>
			<tr>
				<td class="rightstyle">上下线流程：</td>
				
				<td><?php echo $data['nns_config']['flow_unline'];?>	(0或则未配置关闭 ;1开启)</td>
			</tr>
			<tr>
				<td class="rightstyle">上下线反馈方式：</td>
				
				<td><?php echo $data['nns_config']['flow_unline_notify'];?>	(0或则未配置则不反馈 ;1 amqp反馈)</td>
			</tr>
			<tr>
				<td class="rightstyle"><b>CP控制切片配置</b></td>
				<td></td>
			</tr>
			<tr>
				<td class="rightstyle">外部源ID</td>
				<td>
				<?php echo $data['nns_config']['clip_custom_origin_id'];?>&nbsp;&nbsp;&nbsp;&nbsp;【MSP平台上配置的外部源ID列表,填入的时候添加MSP的外部源ID即可，为空则默认走以前的老逻辑，多个外部源ID以;分割】
				</td>
			</tr>
            <tr>
                <td class="rightstyle">删除CP源物理文件开关</td>
                <td>
                    <?php echo (int)$data['nns_config']['del_cp_file_enabled'] ? '开启' : '关闭';?>&nbsp;&nbsp;【默认不删除CP存放在FTP上的物理文件；开启删除时只有片源为最终注入状态时才能删除】
                </td>
            </tr>
			<tr>
				<td class="rightstyle"><b>上游CP/SP控制转码配置</b></td>
				<td></td>
			</tr>
			<tr>
				<td class="rightstyle">上游CP/SP转码开关:</td>
				<td>
				<?php echo $data['nns_config']['cp_transcode_file_enabled'];?>&nbsp;&nbsp;&nbsp;&nbsp;
				【0或者为空 关闭 | 1:开启 默认关闭】
				</td>
			</tr>
			<tr>
				<td class="rightstyle">上游CP/SP点播转码消息反馈地址:</td>
				<td>
				<?php echo $data['nns_config']['video_source_notify_url'];?>&nbsp;&nbsp;&nbsp;&nbsp;
				【上游CP/SP直播转点播消息反馈地址】
				</td>
			</tr>
			<?php if(isset($data['nns_config']['cp_transcode_file_enabled']) && $data['nns_config']['cp_transcode_file_enabled'] == '1'){?>
			<tr>
				<td class="rightstyle">上游CP/SP直播转点播开关:</td>
				<td>
				<?php echo "开启";?>&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
			</tr>
			<tr>
				<td class="rightstyle">上游CP/SP直播转点播消息反馈地址:</td>
				<td>
				<?php echo $data['nns_config']['source_notify_url'];?>&nbsp;&nbsp;&nbsp;&nbsp;
				【上游CP/SP直播转点播消息反馈地址】
				</td>
			</tr>
			<?php }else{?>
			 <tr>
				<td class="rightstyle">上游CP/SP转码开关:</td>
				<td>
				<?php echo "关闭";?>&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
			</tr>
			<?php }?>

			<tr>
				<td class="rightstyle"><b>DRM指令配置</b></td>
				<td> </td>
			</tr>
			<tr>
				<td class="rightstyle">DRM主开关</td>
				<td><?php echo $data['nns_config']['main_drm_enabled'];?>&nbsp;&nbsp;&nbsp;&nbsp;0关闭,1开启</td>
			</tr>
			
			<tr>
				<td class="rightstyle">切片DRM厂商标识</td>
				<td><?php echo $data['nns_config']['q_drm_identify'];?>【DRM加密方案】</td>
			</tr>
			
			<tr>
				<td class="rightstyle">切片前DRM开关</td>
				<td><?php echo $data['nns_config']['q_disabled_drm'];?>&nbsp;&nbsp;&nbsp;&nbsp;0关闭,1开启</td>
			</tr>
			<?php if(isset($data['nns_config']['q_disabled_drm']) && $data['nns_config']['q_disabled_drm'] == 1){?>
			
			
			<tr>
				<td class="rightstyle">切片前DRM密钥地址</td>
				<td><?php echo $data['nns_config']['q_drm_key_file_url'];?>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<?php }?>
			<tr>
				<td class="rightstyle">DRM切片加密开关</td>
				<td><?php echo $data['nns_config']['clip_drm_enabled'];?>&nbsp;&nbsp;&nbsp;&nbsp;0|未配置关闭 1 开启</td>
			</tr>
			
			<?php if(isset($data['nns_config']['clip_drm_enabled']) && $data['nns_config']['clip_drm_enabled'] == 1){?>
			
			
			<tr>
				<td class="rightstyle">认证授权服务器地址</td>
				<td><?php if(isset($data['nns_config']['clip_drm_config_params']['auth_server_ip'])){echo $data['nns_config']['clip_drm_config_params']['auth_server_ip'];}else{ echo '';}?>&nbsp;&nbsp;&nbsp;&nbsp;本地字节顺序</td>
			</tr>
			<tr>
				<td class="rightstyle">认证授权服务器端口号</td>
				<td><?php if(isset($data['nns_config']['clip_drm_config_params']['auth_server_port'])){echo $data['nns_config']['clip_drm_config_params']['auth_server_port'];}else{ echo "";}?>&nbsp;&nbsp;&nbsp;&nbsp;本地字节顺序</td>
			</tr>
			<tr>
				<td class="rightstyle">内容密钥服务器管理地址</td>
				<td><?php if(isset($data['nns_config']['clip_drm_config_params']['key_server_ip'])){echo $data['nns_config']['clip_drm_config_params']['key_server_ip'];}else{ echo "";}?>&nbsp;&nbsp;&nbsp;&nbsp;本地字节顺序</td>
			</tr>
			<tr>
				<td class="rightstyle">内容密钥服务器端口号</td>
				<td><?php if(isset($data['nns_config']['clip_drm_config_params']['key_server_port'])){echo $data['nns_config']['clip_drm_config_params']['key_server_port'];}else{ echo "";}?>&nbsp;&nbsp;&nbsp;&nbsp;本地字节顺序</td>
			</tr>
			<tr>
				<td class="rightstyle">节目号</td>
				<td><?php if(isset($data['nns_config']['clip_drm_config_params']['program_number'])){echo $data['nns_config']['clip_drm_config_params']['program_number'];}else{ echo "0";}?>&nbsp;&nbsp;&nbsp;&nbsp;如果为0默认选择码流中的第一套节目</td>
			</tr>
			<tr>
				<td class="rightstyle">预览时间</td>
				<td><?php if(isset($data['nns_config']['clip_drm_config_params']['preview_time'])){echo $data['nns_config']['clip_drm_config_params']['preview_time'];}else{ echo "";}?>&nbsp;&nbsp;&nbsp;&nbsp;单位/秒</td>
			</tr>
			<tr>
				<td class="rightstyle">打包支持模式</td>
				<td><?php if(isset($data['nns_config']['clip_drm_config_params']['package_mode'])){echo $data['nns_config']['clip_drm_config_params']['package_mode'];}else{ echo "0";}?>&nbsp;&nbsp;&nbsp;&nbsp;1为支持苹果打包模式，0为单文件模式，默认为0</td>
			</tr>
			<tr>
				<td class="rightstyle">内容标识</td>
				<td><?php if(isset($data['nns_config']['clip_drm_config_params']['p_cid'])){echo $data['nns_config']['clip_drm_config_params']['p_cid'];}else{ echo "";}?>&nbsp;&nbsp;&nbsp;&nbsp;默认为空</td>
			</tr>
			<tr>
				<td class="rightstyle">加密模式</td>
				<td><?php if(isset($data['nns_config']['clip_drm_config_params']['encrypt_mode'])){echo $data['nns_config']['clip_drm_config_params']['encrypt_mode'];}else{ echo "2";}?>&nbsp;&nbsp;&nbsp;&nbsp;0 加密BP帧，1加密I帧，2加密所有帧，默认为2</td>
			</tr>
			<tr>
				<td class="rightstyle">加密帧数</td>
				<td><?php if(isset($data['nns_config']['clip_drm_config_params']['max_frame_packet_encryption'])){echo $data['nns_config']['clip_drm_config_params']['max_frame_packet_encryption'];}else{ echo "1";}?>&nbsp;&nbsp;&nbsp;&nbsp;默认为1</td>
			</tr>
			<tr>
				<td class="rightstyle">跳过帧数</td>
				<td><?php if(isset($data['nns_config']['clip_drm_config_params']['max_frame_packet_encrypt_skip'])){echo $data['nns_config']['clip_drm_config_params']['max_frame_packet_encrypt_skip'];}else{ echo "0";}?>&nbsp;&nbsp;&nbsp;&nbsp;加密多少帧，跳过多少帧，默认加密所有帧</td>
			</tr>
			
			<?php }?>
            <tr>
				<td class="rightstyle">Marlin DRM开关</td>
    			<td><?php if(isset($data['nns_config']['q_marlin_drm_enable']) && $data['nns_config']['q_marlin_drm_enable'] == 1){ echo "开启";}else{ echo "关闭";};?> 
    			&nbsp;&nbsp;&nbsp;&nbsp;【开启:Marlin DRM开启;关闭:Marlin DRM关闭】</td>
			</tr>
			<?php if(isset($data['nns_config']['q_marlin_drm_enable']) && $data['nns_config']['q_marlin_drm_enable'] == 1){?>
    			<tr>
    			    <td class="rightstyle">点播生成密钥条数</td>
    				<td><?php echo (isset($data['nns_config']['marlin_drm_config']['marlin_drm_vod_num']) && strlen($data['nns_config']['marlin_drm_config']['marlin_drm_vod_num'])) ? $data['nns_config']['marlin_drm_config']['marlin_drm_vod_num'] : 1;?> 
    				【点播生成密钥条数：如果未配置或者为空默认为1】
    				</td>
    			</tr>
    			<tr>
    			    <td class="rightstyle">直播生成密钥条数</td>
    				<td><?php echo (isset($data['nns_config']['marlin_drm_config']['marlin_drm_live_num']) && strlen($data['nns_config']['marlin_drm_config']['marlin_drm_live_num'])) ? $data['nns_config']['marlin_drm_config']['marlin_drm_live_num'] : 10;?> 
    				【直播生成密钥条数：如果未配置或者为空默认为1】
    				</td>
    			</tr>
			<?php }?>
			<tr>
				<td class="rightstyle"><b>图片处理配置</b></td>
				<td> </td>
			</tr>
			<tr>
				<td class="rightstyle">是否开启图片下载</td>
				<!-- <td><?php echo $data['nns_config']['g_ftp_conf']['download_img_enabled'];?></td> -->
				<td>
					<?php echo $data['nns_config']['g_ftp_conf']['download_img_enabled'];?>&nbsp;&nbsp;&nbsp;&nbsp;【0|未配置开启 1 关闭将保留海报原始路径】

				</td>
			</tr>
            <tr>
                <td class="rightstyle">是否忽略图片处理失败</td>
                <!-- <td><?php echo $edit_data['nns_config']['g_ftp_conf']['download_img_enabled'];?></td> -->
                <td><?php echo $data['nns_config']['g_ftp_conf']['ignore_img_dowload_fail'];?>
                    &nbsp;&nbsp;【开启后，若主媒资或分集图片处理失败将忽略，不影响注入】
                </td>
            </tr>
            <!--xinxin.deng 2017年11月09日14:55:00 添加图片是否上传到ftp  start-->
            <tr>
                <td class="rightstyle">是否开启图片上传到ftp</td>
                <td><?php echo $data['nns_config']['g_ftp_conf']['upload_img_to_ftp_enabled'];?> &nbsp;&nbsp;&nbsp;&nbsp;
                    【0或者为空 关闭 | 1:开启 默认关闭】
                </td>
            </tr>
            <tr>
                <td class="rightstyle">图片下载后上传到ftp地址:</td>
                <td><?php echo $data['nns_config']['g_ftp_conf']['upload_img_to_ftp_url'];?> &nbsp;&nbsp;&nbsp;&nbsp;
                    【图片下载后上传到ftp地址，绝对路径】
                </td>
            </tr>
            <!--xinxin.deng 2017年11月09日14:55:00 添加图片是否上传到ftp  end-->
			<tr>
				<td class="rightstyle">图片处理是否采用以下配置</td>
				<!-- <td><?php echo $data['nns_config']['g_ftp_conf']['img_enabled'];?></td> -->
				<td>
					<?php echo $data['nns_config']['g_ftp_conf']['img_enabled'];?>&nbsp;&nbsp;&nbsp;&nbsp;【0|未配置关闭 1 开启后配置文件中的配置将失效】
					
				</td>
			</tr>
			<tr>
				<td class="rightstyle">图片存储基本路径</td>
				<td><?php echo $data['nns_config']['g_ftp_conf']['down_img_dir'];?>  图片在近端存放地址(绝对路径)</td>
				<!-- <td><input	name="nns_config[g_ftp_conf][down_img_dir]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $data['nns_config']['g_ftp_conf']['down_img_dir'];}?>" /> 图片在近端存放地址(绝对路径)</td>  -->
			</tr>
			<tr>
				<td class="rightstyle">下载地址</td>
				<td><?php echo $data['nns_config']['g_ftp_conf']['domain'];?> 图片在远端存放地址(相对地址)</td>
				<!-- <td><input	 name="nns_config[g_ftp_conf][domain]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $data['nns_config']['g_ftp_conf']['domain'];}?>" /> 图片在远端存放地址(相对地址)</td>  -->
			</tr>
			<tr>
				<td class="rightstyle">匹配规则(正则内容)</td>
				<td><?php echo $data['nns_config']['g_ftp_conf']['rule'];?>  正则内容和匹配地址成对出现</td>
				<!-- <td><input	 name="nns_config[g_ftp_conf][rule]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $data['nns_config']['g_ftp_conf']['rule'];}?>" /> </td> -->
			</tr>
			<tr>
				<td class="rightstyle">匹配规则(匹配地址)</td>
				<td><?php echo $data['nns_config']['g_ftp_conf']['rule_domain'];?> 正则内容和匹配地址成对出现</td>
				<!-- <td><input	 name="nns_config[g_ftp_conf][rule_domain]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $data['nns_config']['g_ftp_conf']['rule_domain'];}?>" /> 正则内容和匹配地址成对出现</td>  -->
			</tr>
			<tr>
				<td class="rightstyle">播控手动上传图片路径</td>
				<td><?php echo $data['nns_config']['g_ftp_conf']['handle_upload_img'];?> 【播控手动上传图片路径(相对路径)】</td>
				<!-- <td><input	 name="nns_config[g_ftp_conf][rule_domain]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $data['nns_config']['g_ftp_conf']['rule_domain'];}?>" /> 正则内容和匹配地址成对出现</td>  -->
			</tr>
			<tr>
				<td class="rightstyle"><b>片源替换逻辑[模式由低到高优先规则]</b></td>
				<td> </td>
			</tr>
			<tr>
				<td class="rightstyle">片源替换模式一</td>
				<td><?php echo $data['nns_config']['media_replace_mode_one'];?>【0 关 1开 注入ID不同且匹配信息相同】</td>
			</tr>
			<tr>
				<td class="rightstyle">片源替换模式二</td>
				<td><?php echo $data['nns_config']['media_replace_mode_two'];?>【0 关 1开 注入ID相同或除注入ID外匹配信息相同】</td>
			</tr>
			<tr>
				<td class="rightstyle">片源替换模式三</td>
				<td><?php echo $data['nns_config']['media_replace_mode_three'];?>【0 关 1开 高清晰度替换低清晰度规则】</td>
			</tr>
			<tr>
				<td class="rightstyle"><b>中心指令配置</b></td>
				<td> </td>
			</tr>
			<tr>
                <td class="rightstyle">下载的片源验证片源信息开关</td>
                <td>
                    <?php echo (int)$data['nns_config']['is_need_real_media_info'] ? '开启' : '关闭';?>&nbsp;&nbsp;【切片下载的时候获取片源的真实信息】
                </td>
            </tr>
			<tr>
				<td class="rightstyle">中心片库下载地址实际接口</td>
				<td><?php echo $data['nns_config']['down_url_real'];?></td>
			</tr>
			<tr>
				<td class="rightstyle">中心片库下载是否采用FTP/HTTP</td>
				<td><?php echo $data['nns_config']['down_method'];?>  1FTP,0HTTP</td>
			</tr>
			
			<tr>
				<td class="rightstyle">中心片库下载FTP模式</td>
				<td><?php echo $data['nns_config']['down_method_ftp'];?>  1主动模式,0被动模式</td>
			</tr>
            <tr>
				<td class="rightstyle">中心片库下载url中文符号转换模式</td>
				<td><?php echo $data['nns_config']['down_method_ftp_illegal_character'];?>  【1中文符号不需要转换,0或者未配置中文符号需要转换】</td>
			</tr>
            <tr>
                <td class="rightstyle"><b>CP-CDN配置</b></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">CP媒资注入CDN栏目配置</td>
                <td><?php echo $data['nns_config']['import_cdn_category'];?> &nbsp;&nbsp;   在CP媒资注入CDN时，CDN会提供一个栏目ID并要求注入到此CDN时需携带此栏目ID</td>
            </tr>
            <tr>
                <td class="rightstyle">CP媒资注入MSP CDN策略配置</td>
                <td><?php echo $data['nns_config']['import_msp_cdn_policy'];?> &nbsp;&nbsp; 【CP媒资注入MSP CDN策略配置例如CDN1,CDN2】</td>
            </tr>
          </tbody>
        </table>
    </div>
    <div class="controlbtns">
    	<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo $language_action_back;?></a></div>
		<div class="controlbtn edit">
			<a action="edit" href="nncms_content_cp_info_addedit.php?action=edit&nns_id=<?php echo $data["nns_id"]; ?>">修改</a></div>
		<div style="clear:both;"></div>
    </div>
</div>
</body>
</html>
