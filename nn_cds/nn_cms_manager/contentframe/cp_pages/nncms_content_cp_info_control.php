<?php
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
include $nncms_config_path . 'nn_logic/cp/cp.class.php';
include $nncms_config_path . 'nn_logic/sp/sp.class.php';
include $nncms_config_path . 'nn_cms_manager/controls/nncms_controls_public_function.php';
require_once $nncms_db_path . 'nns_log' . DIRECTORY_SEPARATOR . 'nns_db_op_log_class.php';
$log_inst = new nns_db_op_log_class();

$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE, 
		'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$dc->open();

$params = $_REQUEST;
$action = $params['action'];
$params['nns_file_state_enable'] = (isset($params['nns_file_state_enable']) && in_array($params['nns_file_state_enable'], array(0,1))) ? $params['nns_file_state_enable'] : 0;
unset($params['action']);
session_start();
switch ($action)
{
	case "edit":
		$nns_id = $params['nns_id'];
		unset($params['nns_id']);
		$params['nns_config'] = (isset($params['nns_config']) && !empty($params['nns_config'])) ? json_encode($params['nns_config']) : '';
		$result = nl_cp::edit($dc, $params, $nns_id);
		if ($result['ret'])
		{
			echo "<script>alert('" . $result['reason'] . "');history.go(-1);</script>";
		}
		else
		{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "修改CP:" . $params['nns_name'], $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
			echo "<script>alert('修改CP成功');</script>";
		}
		break;
	case "add":
		$params['nns_create_time'] = $params['nns_modify_time'] = date('Y-m-d H:i:s');
		$params['nns_config'] = (isset($params['nns_config']) && !empty($params['nns_config'])) ? json_encode($params['nns_config']) : '';
		$result_unique = nl_cp::query_by_id($dc, $params['nns_id']);
		if ($result_unique['ret'])
		{
			echo "<script>alert('" . $result_unique['reason'] . "');history.go(-1);</script>";die;
		}
		if(!empty($result_unique['data_info']))
		{
			echo "<script>alert('CP ID 已经存在不允许重复添加');history.go(-1);</script>";die;
		}
		$result = nl_cp::add($dc, $params);
		if ($result['ret'])
		{
			echo "<script>alert('" . $result['reason'] . "');</script>";
		}
		else
		{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "添加CP:" . $params['nns_name'], $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
			echo "<script>alert('CP添加成功');</script>";
		}
		break;
	case "delete":
		$nns_ids = explode(",", $params['nns_id']);
		$arr_temp_del = array();
		foreach ($nns_ids as $nnsid)
		{
			if (strlen($nnsid) < 1)
			{
				continue;
			}
			//去掉绑定字符串中的cp
			$result_sp_info = nl_sp::query_bind_cp_info($dc,$nnsid);//查出SP
			if(is_array($result_sp_info['data_info']))
			{
				foreach($result_sp_info['data_info'] as $data_info_val)
				{
					$bind_cp = $data_info_val['nns_bind_cp'];
		//				$bind_cp = explode(',',rtrim($bind_cp,','));
		//				$position = array_search($nnsid,$bind_cp);
		//				if(count($bind_cp)==1)
		//				{
		//					$bind_cp_new = array_splice($bind_cp, $position, 0);
		//					$cp_id = implode(',', $bind_cp_new);
		//				}
		//				else
		//				{
		//					$bind_cp_new = array_splice($bind_cp, $position, 1);
		//					$cp_id = implode(',', $bind_cp_new);
		//					$cp_id .= ',';
		//				}
					$cp_id = str_replace($nnsid.',','',$bind_cp);
					$result = nl_sp::update_bind_cp_by_sp($dc,$data_info_val['nns_id'],$cp_id);
					if($result['ret'] != 0)
					{
						echo "<script>alert('".$result['reason']."');history.go(-1);</script>";die;
					}
				}
			}
			$result_del=nl_cp::delete($dc,$nnsid);
			if($result_del['ret'] != 0)
			{
				echo "<script>alert('".$result_del['reason']."');history.go(-1);</script>";die;
			}
		}
//		$str_info = !empty($arr_temp_del) ? ",其中cpid为：[" . implode("],[", $arr_temp_del) . "]已在SP表中做了绑定，不允许删除" : '';
		$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "删除CP_id:" . $params['nns_id'], $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
		echo "<script>alert('删除CP成功');</script>";
		break;
	case "state":
	    $nns_ids = explode(",", $params['nns_id']);
	    if(!is_array($nns_ids) || empty($nns_ids))
	    {
	        echo "<script>alert('修改状态成功,数据为空');</script>";
	        break;
	    }
	    $result = nl_cp::edit($dc, array('nns_state'=>$params['nns_state']), $nns_ids);
	    if ($result['ret'])
	    {
	        echo "<script>alert('" . $result['reason'] . "');history.go(-1);</script>";
	    }
	    else
	    {
	        $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "修改:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
	        echo "<script>alert('修改成功');</script>";
	    }
	    break;
	case "bind":
		if(!isset($_REQUEST['sp_id']) || empty($_REQUEST['sp_id']))
		{
			echo "<script>alert('请至少选择一条Sp数据');</script>";die;
		}
		foreach ($_REQUEST['sp_id'] as $val)
		{
			$result_bind=nl_sp::bind($dc, $val, $_REQUEST['cp_id']);
			if($result_bind['ret'] != 0)
			{
				echo "<script>alert('".$result_bind['reason']."');</script>";die;
			}
			
		}
		$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "绑定CP_id:" . $params['cp_id']."sp id 为[".implode('],[', $_REQUEST['sp_id'])."]", $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
		echo "<script>alert('绑定CP成功".$str_info."');</script>";
		echo "<script>parent.location='nncms_content_cp_info_list.php';</script>";die;
		break;
	default:
		break;
}
$partner_inst = null;
$log_inst = null;
echo "<script>self.location='nncms_content_cp_info_list.php';</script>";
