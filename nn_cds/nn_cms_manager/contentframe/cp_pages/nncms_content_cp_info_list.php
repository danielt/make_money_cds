<?php
header("Content-Type:text/html;charset=utf-8");
ini_set('dicplay_errors',0);
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
//导入语言包
if (isset($_SESSION["language_dir"])){
    $language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136005");
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");

include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");

include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/sp/sp.class.php';
//加载新版
include $nncms_config_path . "v2/common.php";
\ns_core\m_load::load("ns_core.m_config");
global $g_bk_version_number;
$bk_version_number = $g_bk_version_number == '1' ? true : false;
unset($g_bk_version_number);

$dc = \m_config::get_dc();
$data = nl_cp::query($dc, array (
    'page_count' => 0
));

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
    <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
    <script language="javascript" src="../../js/cms_cookie.js"></script>
    <script language="javascript" src="../../js/table.js.php"></script>
    <script language="javascript" src="../../js/checkinput.js.php"></script>
    <script language="javascript" src="../../js/rate.js"></script>
    <script language="javascript" src="../../js/image_loaded_func.js"></script>
    <script language="javascript" src="../../js/cms_datepicker.js"></script>
    <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
    <script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
    <script language="javascript">
        function checkhiddenInput(){
            var par=getAllCheckBoxSelect();
            if (par==""){
                alert("<?php echo cms_get_lang('partner_msg_noselect');?>");
            }else{
                $('#nns_id').attr('value',getAllCheckBoxSelect());
                checkForm('<?php echo cms_get_lang('xtgl_hzgl');?>','<?php echo cms_get_lang('msg_ask_delete');?>',$('#delete_form'));
            }

        }
        function begin_select_sp(cp_id){
            $("#select_frame").attr("src","nncms_content_bind_sp_list.php?cp_id="+cp_id);
            $(".selectbox").show();
        }

        function change_state(id,state) {
            var url = "nncms_content_cp_info_control.php?action=state&nns_id="+id+"&nns_state="+state;
            window.location.href = url;
        }
        $(document).ready(function() {
            $(".selectbox").hide();
        });

        function close_select() {
            $(".selectbox").hide();
        }
    </script>
</head>

<body>
<div class="selectbox">
    <iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
</div>
<div class="content">
    <div class="content_position">CP 管理 > CP 列表</div>
    <div class="content_table formtable">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <th><input name="" type="checkbox" value="" /></th>
                <th><?php echo cms_get_lang('segnumber');?></th>
                <th>CP ID</th>
                <th>CP名称</th>
                <th>绑定SP列表</th>
                <th>联系人</th>
                <th>联系电话</th>
                <th>Eamil</th>
                <th>地址</th>
                <th>描述</th>
                <th>CP类型</th>
                <th>状态</th>
                <th>修改时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (!empty($data['data_info']) && is_array($data['data_info']))
            {
                $num = ($page_info['page_num'] - 1) * $page_info['page_size'];
                foreach ($data['data_info'] as $item)
                {
                    $num++;
                    ?>
                    <tr>
                        <td><input name="input" type="checkbox" value="<?php echo  $item["nns_id"];?>" /></td>
                        <td><?php echo $num;?></td>
                        <td><?php echo $item['nns_id'];?></td>
                        <td><a href="nncms_content_cp_info_detail.php?nns_id=<?php echo $item["nns_id"];?>"><?php echo $item["nns_name"];?></a></td>
                        <td>
                            <?php
                            $result_bind_sp=nl_sp::query_bind_cp_info($dc, $item["nns_id"]);
                            if(isset($result_bind_sp['data_info']) && !empty($result_bind_sp['data_info']) && is_array($result_bind_sp['data_info']))
                            {
                                $temp_array = array();
                                foreach ($result_bind_sp['data_info'] as $sp_info)
                                {
                                    $temp_array[] = $sp_info['nns_id'].'/'.$sp_info['nns_name'];
                                }
                                echo implode("<br/>", $temp_array);
                                unset($temp_array);
                            }
                            else
                            {
                                echo "<font style=\"color:#FF0000\">无SP绑定</font>";
                            }
                            ?>
                        </td>
                        <td><?php echo $item["nns_contactor"];?> </td>
                        <td><?php echo $item["nns_telephone"];?> </td>
                        <td><?php echo $item["nns_email"];?> </td>
                        <td><?php echo $item["nns_addr"];?> </td>
                        <td><?php echo $item["nns_desc"];?> </td>
                        <td><?php echo ($item['nns_type'] !='1') ? '非融合上游' : '融合上游'; ?></td>
                        <td><a href="javascript:change_state('<?php echo $item['nns_id']?>',<?php echo ($item['nns_state'] !=0) ? 0 :1;?>);"><?php echo ($item['nns_state'] !='0') ? '<font color="red">禁用</font>' : '<font color="green">启用</font>'; ?></a> | <?php echo (strlen($item['nns_config'])<1) ? '<font color="red">未配置</font>' : '<font color="green">已配置</font>'; ?></td>


                        <td><?php echo $item["nns_modify_time"];?> </td>
                        <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                            <?php if ($bk_version_number) {?>
                                <a href="nncms_content_cp_info_addedit_v2.php?action=edit&nns_id=<?php echo $item["nns_id"];?>">修改</a>
                            <?php }else{?>
                                <a href="nncms_content_cp_info_addedit.php?action=edit&nns_id=<?php echo $item["nns_id"];?>">修改</a>
                            <?php }?>
                            <a href="nncms_content_cp_info_control.php?action=delete&nns_id=<?php echo $item["nns_id"];?>">删除</a>
                            <!--                	<a href="javascript:begin_select_sp('<?php echo $item["nns_id"];?>');" action="edit">绑定</a>-->
                        </td>
                    </tr>
                    <?php
                }
            }
            $page_info['page_size'] = 18;
            $least_num = $page_info['page_size'] - count($data['data_info']);
            for ($i = 0; $i < $least_num; $i++) {
                ?>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <?php //echo $pager->nav();?>
    <div class="controlbtns">
        <div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);"><?php echo cms_get_lang('select');?></a></div>
        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);"><?php echo cms_get_lang('cancel');?></a></div>
        <?php if ($bk_version_number) {?>
            <div class="controlbtn add"><a href="javascript:void(null);" pos="nncms_content_cp_info_addedit_v2.php" action="add"><?php echo cms_get_lang('add');?></a></div>
        <?php }else{?>
            <div class="controlbtn add"><a href="javascript:void(null);" pos="nncms_content_cp_info_addedit.php" action="add"><?php echo cms_get_lang('add');?></a></div>
        <?php }?>
        <div class="controlbtn delete"><a href="javascript:checkhiddenInput();"  action="delete"><?php echo cms_get_lang('delete');?></a></div>
        <div style="clear:both;"></div>
    </div>
    <form id="delete_form" action="nncms_content_cp_info_control.php" method="post">
        <input name="action" id="action" type="hidden" value="delete" />
        <input name="nns_id" id="nns_id" type="hidden" value="" />
    </form>
</div>
</body>
</html>
