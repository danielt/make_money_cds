<?php
header("Content-Type:text/html;charset=utf-8");
ini_set('display_errors', 0);
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include ("../../nncms_manager_inc.php");
$params = $_REQUEST;
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/models/sp_model.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/sp/sp.class.php';
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE, 
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$data = nl_sp::query($dc, array (
		'page_count' => 0
));
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
		<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/trim.js"></script>
<script language="javascript">
$(document).ready(function() {
	$(".closebtn").click(function() {
		window.parent.close_select();
	});
	$(".closebtn_1").click(function() {
		window.parent.close_select();
	});
});
function check_select_height() {
	$(".selectbox iframe").height($(".selectbox iframe").contents().find(".selectcontrol").height());
	window.parent.check_select_height();
}
</script>
</head>

<body style='width:800px;'>
	<div class="content selectcontrol" style="padding:0px; margin:0px;">
			<div class="closebtn"><img src="../../images/topicon_08-topicon.png" /></div>
			<form action="nncms_content_cp_info_control.php" method="get">
			<div class="content_table formtable">
			<input name="action" value="bind" type="hidden"/>
			<input name="cp_id" value="<?php echo $params['cp_id']; ?>" type="hidden"/>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th><input name="" type="checkbox" value="" /></th>
						<th>序号</th>
						<th>SP ID</th>
						<th>SP名称</th>
						<th>创建时间</th>
						<th>修改时间</th>
					</tr>
				</thead>
				<tbody>
        		<?php
					if ($data['data_info'] != null)
					{
						$num = 0;
						foreach ($data['data_info'] as $item)
						{
							$num++;
				?>
              				<tr>
              					<?php 
              						if(isset($params['cp_id']) && $params['cp_id'] == $item["nns_bind_cp"]){
								?>
									<td><input name="sp_id[]" type="checkbox" checked="checked" value="<?php echo  $item["nns_id"];?>" /></td>
								<?php }else{?>
									<td><input name="sp_id[]" type="checkbox" value="<?php echo  $item["nns_id"];?>" /></td>
								<?php }?>
								
								<td><?php echo $num;?></td>
								<td><?php echo $item['nns_id'];?></td>
								<td><?php echo $item["nns_name"];?></td>
								<td><?php echo $item["nns_create_time"];?> </td>
								<td><?php echo $item["nns_modify_time"];?> </td>
							</tr>
          		<?php 
						}
					} 
				?>
		          <tr>
		          	<td colspan='6'>&nbsp;</td>
		          </tr>
          <tr>
          	<td colspan='6' style="text-align:center" >
          		<input type="submit" value="确定"/>
          		<input type="button" class="closebtn_1" value="取消" onclick=""/>
          	</td>
          </tr>
          </tbody>
		  </table>
		  </div>
		</form>



	</div>

</body>
</html>
