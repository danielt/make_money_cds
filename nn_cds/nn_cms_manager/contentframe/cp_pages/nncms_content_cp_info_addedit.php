<?php
include("../../nncms_manager_inc.php");
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
include $nncms_config_path. 'nn_cms_config/nn_cms_global.php';
include $nncms_config_path. 'nn_logic/cp/cp.class.php';
$action = isset($_GET["action"]) ? $_GET["action"] : 'add';
$nns_id = isset($_GET["nns_id"]) ? $_GET["nns_id"] : null;

if ($action == "edit") {

	$dc = nl_get_dc(array(
		'db_policy'=>NL_DB_READ,
		'cache_policy'=>NP_KV_CACHE_TYPE_NULL
	));
	$dc->open();

	if(strlen($nns_id)<1){
		echo "<script>alert('参数有误');self.location='nncms_content_cp_info_list.php';</script>";exit;
	}

	$edit_data = nl_cp::query_by_id($dc,$nns_id);
	if($edit_data['ret'] !=0)
	{
		echo '<script>alert("'.$edit_data['reason'].'");history.go(-1);</script>';die;
	}
	$edit_data = $edit_data['data_info'];
	$edit_data['nns_config'] = !empty($edit_data['nns_config']) ? json_decode($edit_data['nns_config'],true) : null;
	if(!is_array($edit_data)){
		echo "<script>alert('没有找到您要的信息');self.location='nncms_content_cp_info_list.php';</script>";exit;
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
		<link href="../../css/comm.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
<!-- 		<script language="javascript" src="../../js/cms_datepicker.js"></script> -->
		<script language="javascript" src="../../js/alertbox.js"></script>
<script type="text/javascript">
function show_hide_drm_config(class_css,flag)
{
	var obj_class = $(class_css);
	var obj_class_skip_rule = $(class_css+'_skip_rule');
	var rule_str = '';
	if(flag == 1)
	{
		if(obj_class.length >0)
		{
			$(class_css).show();
		}
		if(obj_class_skip_rule.length >0)
		{
			$(class_css+'_skip_rule').show();
		}
		rule_str='noempty';
	}
	else
	{
		if(obj_class.length >0)
		{
			$(class_css).hide();
		}
		if(obj_class_skip_rule.length >0)
		{
			$(class_css+'_skip_rule').hide();
		}
	}
	if(obj_class.length >0)
	{
		for(var i=0;i<obj_class.length;i++)
		{
			obj_class.eq(i).children('td').eq(1).children('input').eq(0).attr('rule',rule_str);
		}
	}
	if(obj_class_skip_rule.length >0)
	{
		for(var i=0;i<obj_class_skip_rule.length;i++)
		{
			obj_class.eq(i).children('td').eq(1).children('input').eq(0).attr('rule','');
		}
	}
	return true;
}
function begin_select_group_config()
{
	var group_nns_id = "<?php if(isset($edit_data['nns_config']["global_cp_config_bind_key"])) echo $edit_data['nns_config']["global_cp_config_bind_key"]; ?>"
	$("#select_frame").attr("src","../sp_pages_v2/nncms_content_bind_group_config_list.php?action=cp&group_id="+group_nns_id);
	$(".selectbox").show();
	$('.selectbox iframe').load(function () {
		$(".selectbox iframe").css({
			width:800,
			height:400,
		});
		$(".selectbox").css({
			top: 0
		});
	});
}

function check_radio_class()
{
	var arr = [];
	$("input:radio").each(
		function()
		{
			var value = $(this).attr('name').replace('nns_config[','').replace('][','_').replace(']','');
			if($.inArray(value, arr) == -1)
			{
				arr.push(value);
			}
		}
	);
	return arr;
}

function close_select() {
	$(".selectbox").hide();
}
function delete_global_config()
{
	$("input[name='nns_config[global_cp_config_bind_key]']").val("")
}
$(document).ready(function()
{
	var arr = check_radio_class();
	
	if(arr.length >0)
	{
    	$.each(arr, function(i,val_class){  
    		console.log(i);
    		console.log(val_class);
    		var value = $("input[name='nns_config["+val_class+"]']:checked").val();
    		value = value == 1 ? 1 : 0;
    		show_hide_drm_config('.'+val_class+'_class',value);
    		$("input[name='nns_config["+val_class+"]']").change(function(){
    			var value_1 = $("input[name='nns_config["+val_class+"]']:checked").val();
    			value_1 = value_1 == 1 ? 1 : 0;
    			show_hide_drm_config('.'+val_class+'_class',value_1);
    			window.parent.resetFrameHeight();
    		});
        });
	}
    $("input[name='nns_config[g_ftp_conf][upload_img_to_ftp_enabled]']").change(function(){
        var value_1 = $("input[name='nns_config[g_ftp_conf][upload_img_to_ftp_enabled]']:checked").val();
        value_1 = value_1 == 1 ? 1 : 0;
        show_hide_drm_config('.g_ftp_conf_upload_img_to_ftp_enabled_class',value_1);
        window.parent.resetFrameHeight();
    });
	window.parent.resetFrameHeight();
});
</script>
</head>

<body>
<div class="selectbox" style="display: none;">
	<iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
</div>
<div class="content">
	<div class="content_position">CP管理 > CP编辑</div>
	<form id="add_form" action="nncms_content_cp_info_control.php" method="post" enctype="multipart/form-data">
		<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
		<?php if ($action=="edit"){?>
			<input name="nns_id" id="nns_id" type="hidden" value="<?php echo $nns_id;?>"/>
		<?php } ?>
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td class="rightstyle">CP ID<font style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
				<td>
					<?php if($action=='edit'):?>
						<?php echo $edit_data["nns_id"]?>
						<input name="nns_id" type="hidden" value="<?php echo $edit_data["nns_id"]?>"/>
					<?php else:?>
						<input name="nns_id" id="nns_id" type="text" rule="noempty"
							value="<?php if($action=="edit"){echo $edit_data["nns_id"];}?>" />
					<?php endif;?>
				</td>
			</tr>
			<tr>
				<td class="rightstyle">CP 名称 <font style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
				<td><input name="nns_name" id="nns_name" type="text" rule="noempty"
					value="<?php if($action=="edit"){echo $edit_data["nns_name"];}?>"
				/></td>
			</tr>
			<tr>
				<td class="rightstyle">上游文件包资源库展示开关</td>
				<td>
					<input name="nns_file_state_enable" id="nns_file_state_enable" type="radio"  value="0" <?php if(isset($edit_data['nns_file_state_enable']) && $edit_data['nns_file_state_enable'] != 1){echo "checked='checked'";} ?> /> 关闭  
					<input name="nns_file_state_enable" id="nns_file_state_enable" type="radio"  value="1" <?php if(isset($edit_data['nns_file_state_enable']) && $edit_data['nns_file_state_enable'] == 1){echo "checked='checked'";} ?> /> 开启【上游文件包后台资源展示开关，默认关闭】
				</td>
			</tr>
			<tr>
				<td class="rightstyle">联系人</td>
				<td><input name="nns_contactor" id="nns_contactor" type="text" value="<?php if(isset($edit_data["nns_contactor"])){ echo $edit_data["nns_contactor"];}else{ echo '';}?>"/>
				</td>
			</tr>
			<tr>
					<td class="rightstyle">联系电话</td>
					<td><input name="nns_telephone" id="nns_telephone" rule="phone" type="text" value="<?php if(isset($edit_data["nns_telephone"])){ echo $edit_data["nns_telephone"];}else{ echo '';}?>"/>
					</td>
			</tr>
			<tr>
					<td class="rightstyle">Eamil</td>
					<td><input name="nns_email" id="nns_email" rule="email" type="text" value="<?php if(isset($edit_data["nns_email"])){ echo $edit_data["nns_email"];}else{ echo '';}?>"/>
					</td>
			</tr>
			<tr>
				<td class="rightstyle">地址</td>
				<td><input name="nns_addr" id="nns_addr" type="text" value="<?php if(isset($edit_data["nns_addr"])){ echo $edit_data["nns_addr"];}else{ echo '';}?>"/>
				</td>
			</tr>
			<tr>
				<td class="rightstyle">描述</td>
				<td><textarea name="nns_desc" id="nns_desc" cols="" rows=""><?php if(isset($edit_data["nns_desc"])){ echo $edit_data["nns_desc"];}else{ echo '';}?></textarea>
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><b>通用配置</b></td>
				<td> </td>
			</tr>
			<tr>
				<td class="rightstyle">通用配置分组绑定</td>
				<td>
					<input name="nns_config[global_cp_config_bind_key]" id="nns_config" type="text" readonly="readonly" value="<?php if($action=="edit" && isset($edit_data['nns_config']['global_cp_config_bind_key'])){echo $edit_data['nns_config']['global_cp_config_bind_key'];}?>" />
					<input name="binding_global_config_bind_key" id="binding_global_config_bind_key" type="button" onclick="begin_select_group_config()" value="配置绑定">
					<input name="unbundling_global_config_bind_key" id="unbundling_global_config_bind_key" type="button" onclick="delete_global_config()" value="解除绑定">(通用绑定配置项)
				</td>
			</tr>
			
			<tr>
				<td class="rightstyle"><b>全局配置</b></td>
				<td> </td>
			</tr>
			<tr>
				<td class="rightstyle">消息队列获取消息ID模式</td>
				<td><input	name="nns_config[message_import_message_model]" style="width:100px;" id="nns_config" type="text"  value="<?php 
				if($action=="edit" && isset($edit_data['nns_config']['message_import_message_model'])){echo $edit_data['nns_config']['message_import_message_model'];}?>" />
                    【为空或者不配置或者为0 查询存在（cp+messageid）则修改;1注入一条数据直接加一条;2查询存在则什么都不操作】</td>
			</tr>
			<tr>
				<td class="rightstyle">片源队列注入开关</td>
				<td>
					<input name="nns_config[message_import_enable]" id="nns_config" type="radio"  value="0" <?php if(isset($edit_data['nns_config']['message_import_enable']) && $edit_data['nns_config']['message_import_enable'] == 0){echo "checked='checked'";} ?> /> 关闭  
					<input name="nns_config[message_import_enable]" id="nns_config" type="radio"  value="1" <?php if(isset($edit_data['nns_config']['message_import_enable']) && $edit_data['nns_config']['message_import_enable'] == 1){echo "checked='checked'";} ?> /> 开启【关闭  (片源正常注入); 开启 (依据下方片源注入队列模式，限制片源注入队列)】
				</td>
			</tr>
			<tr>
				<td class="rightstyle">直播源队列注入开关</td>
				<td>
					<input name="nns_config[message_live_media_import_enable]" id="nns_config" type="radio"  value="0" <?php if(isset($edit_data['nns_config']['message_live_media_import_enable']) && $edit_data['nns_config']['message_live_media_import_enable'] == 0){echo "checked='checked'";} ?> /> 关闭  
					<input name="nns_config[message_live_media_import_enable]" id="nns_config" type="radio"  value="1" <?php if(isset($edit_data['nns_config']['message_live_media_import_enable']) && $edit_data['nns_config']['message_live_media_import_enable'] == 1){echo "checked='checked'";} ?> /> 开启【关闭|未配置  (直播源不注入); 开启 (直播源注入队列)】
				</td>
			</tr>
			<tr>
				<td class="rightstyle">节目单队列注入开关</td>
				<td>
					<input name="nns_config[message_playbill_import_enable]" id="nns_config" type="radio"  value="0" <?php if(isset($edit_data['nns_config']['message_playbill_import_enable']) && $edit_data['nns_config']['message_playbill_import_enable'] == 0){echo "checked='checked'";} ?> /> 关闭  
					<input name="nns_config[message_playbill_import_enable]" id="nns_config" type="radio"  value="1" <?php if(isset($edit_data['nns_config']['message_playbill_import_enable']) && $edit_data['nns_config']['message_playbill_import_enable'] == 1){echo "checked='checked'";} ?> /> 开启【关闭|未配置  (节目单不注入); 开启 (节目单注入队列)】
				</td>
			</tr>
			<tr>
				<td class="rightstyle">节目单注入下游CMS模式</td>
				<td>
					<input name="nns_config[live_playbill_cms_model]" id="nns_config" type="radio"  value="0" <?php if(isset($edit_data['nns_config']['live_playbill_cms_model']) && $edit_data['nns_config']['live_playbill_cms_model'] != '1'){echo "checked='checked'";} ?> /> 以直播源的注入ID为准  
					<input name="nns_config[live_playbill_cms_model]" id="nns_config" type="radio"  value="1" <?php if(isset($edit_data['nns_config']['live_playbill_cms_model']) && $edit_data['nns_config']['live_playbill_cms_model'] == 1){echo "checked='checked'";} ?> /> 以直播频道的注入ID为准【0|未配置  (节目单注入后用直播源的注入ID反查信息); 1 (节目单注入后用直播频道的注入ID反查信)】
				</td>
			</tr>
			<tr class="message_import_enable_class">
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>片源队列注入模式</td>
				<td><input	name="nns_config[message_import_mode]" style="width:100px;" id="nns_config" type="text"  value="<?php if($action=="edit" && isset($edit_data['nns_config']['message_import_mode'])){echo $edit_data['nns_config']['message_import_mode'];}?>" /> 【1 : content ID不为空注入队列，反之不注入队列;】</td>
			</tr>
			<tr class="message_import_enable_class_skip_rule">
				<td class="rightstyle">片源队列注入时间范围控制</td>
				<td><input	name="nns_config[message_import_time_control]" id="nns_config" type="text"  value="<?php if($action=="edit" && isset($edit_data['nns_config']['message_import_time_control'])){echo $edit_data['nns_config']['message_import_time_control'];}?>" /> 【格式9:30-15:30（24小时制无前置0,多个|分隔，为空不限制时间）】</td>
			</tr>
			<tr>
				<td class="rightstyle">HTTP消息注入开关</td>
				<td>
					<input name="nns_config[message_http_import_enable]" id="nns_config" type="radio"  value="0" <?php 
					if(isset($edit_data['nns_config']['message_http_import_enable']) && $edit_data['nns_config']['message_http_import_enable'] == 0){echo "checked='checked'";} ?> /> 
					关闭  
					<input name="nns_config[message_http_import_enable]" id="nns_config" type="radio"  value="1" <?php 
					if(isset($edit_data['nns_config']['message_http_import_enable']) && $edit_data['nns_config']['message_http_import_enable'] == 1){echo "checked='checked'";} ?> /> 
					开启【关闭  (不接收HTTP消息); 开启 (接收HTTP消息)】
				</td>
			</tr>
			<tr class="message_http_import_enable_class_skip_rule">
				<td class="rightstyle">消息队列获取数据条数</td>
				<td><input	name="nns_config[message_http_import_num]" style="width:200px;" id="nns_config" type="text"  value="<?php 
				if($action=="edit" && isset($edit_data['nns_config']['message_http_import_num'])){echo $edit_data['nns_config']['message_http_import_num'];}?>" /> 
				【HTTP消息队列获取条数，不设置默认获取100条】</td>
			</tr>
			<tr class="message_http_import_enable_class">
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>消息队列拉取地址</td>
				<td><input	name="nns_config[message_http_import_url]" id="nns_config" type="text"  value="<?php 
				if($action=="edit" && isset($edit_data['nns_config']['message_http_import_url'])){echo $edit_data['nns_config']['message_http_import_url'];}?>" /> 
				【HTTP消息队列服务器拉取地址】</td>
			</tr>
			<tr>
				<td class="rightstyle">MQ消息注入开关</td>
				<td>
					<input name="nns_config[message_queue_import_enable]" id="nns_config" type="radio"  value="0" <?php 
					if(isset($edit_data['nns_config']['message_queue_import_enable']) && $edit_data['nns_config']['message_queue_import_enable'] == 0){echo "checked='checked'";} ?> /> 
					关闭  
					<input name="nns_config[message_queue_import_enable]" id="nns_config" type="radio"  value="1" <?php 
					if(isset($edit_data['nns_config']['message_queue_import_enable']) && $edit_data['nns_config']['message_queue_import_enable'] == 1){echo "checked='checked'";} ?> /> 
					开启【关闭  (不接收MQ消息); 开启 (接收MQ消息)】
				</td>
			</tr>
			<tr class="message_queue_import_enable_class_skip_rule">
				<td class="rightstyle">消息队列获取数据条数</td>
				<td><input	name="nns_config[message_queue_import_num]" style="width:200px;" id="nns_config" type="text"  value="<?php 
				if($action=="edit" && isset($edit_data['nns_config']['message_queue_import_num'])){echo $edit_data['nns_config']['message_queue_import_num'];}?>" /> 
				【MQ消息队列获取条数，不设置默认获取100条】</td>
			</tr>
			<tr class="message_queue_import_enable_class">
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>消息队列地址</td>
				<td><input	name="nns_config[message_queue_import_host]" style="width:200px;" id="nns_config" type="text"  value="<?php 
				if($action=="edit" && isset($edit_data['nns_config']['message_queue_import_host'])){echo $edit_data['nns_config']['message_queue_import_host'];}?>" /> 
				【MQ消息队列服务器IP地址】</td>
			</tr>
			<tr class="message_queue_import_enable_class_skip_rule">
				<td class="rightstyle">消息队列端口号</td>
				<td><input	name="nns_config[message_queue_import_port]" style="width:200px;" id="nns_config" type="text"  value="<?php 
				if($action=="edit" && isset($edit_data['nns_config']['message_queue_import_port'])){echo $edit_data['nns_config']['message_queue_import_port'];}?>" /> 
				【MQ消息队列服务器端口号,为空默认为5672】</td>
			</tr>
			<tr class="message_queue_import_enable_class_skip_rule">
				<td class="rightstyle">消息队列账号</td>
				<td><input	name="nns_config[message_queue_import_user]" style="width:200px;" id="nns_config" type="text"  value="<?php 
				if($action=="edit" && isset($edit_data['nns_config']['message_queue_import_user'])){echo $edit_data['nns_config']['message_queue_import_user'];}?>" /> 
				【MQ消息队列账号】</td>
			</tr>
			<tr class="message_queue_import_enable_class_skip_rule">
				<td class="rightstyle">消息队列端密码</td>
				<td><input	name="nns_config[message_queue_import_pass]" style="width:200px;" id="nns_config" type="text"  value="<?php 
				if($action=="edit" && isset($edit_data['nns_config']['message_queue_import_pass'])){echo $edit_data['nns_config']['message_queue_import_pass'];}?>" /> 
				【MQ消息队列密码】</td>
			</tr>
			<tr class="message_queue_import_enable_class_skip_rule">
				<td class="rightstyle">消息队列路径</td>
				<td><input	name="nns_config[message_queue_import_vhost]" style="width:200px;" id="nns_config" type="text"  value="<?php 
				if($action=="edit" && isset($edit_data['nns_config']['message_queue_import_vhost'])){echo $edit_data['nns_config']['message_queue_import_vhost'];}?>" /> 
				【MQ消息队列服务器路径】</td>
			</tr>
			<tr class="message_queue_import_enable_class">
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>消息队列路由名</td>
				<td><input	name="nns_config[message_queue_import_channel]" style="width:200px;" id="nns_config" type="text"  value="<?php 
				if($action=="edit" && isset($edit_data['nns_config']['message_queue_import_channel'])){echo $edit_data['nns_config']['message_queue_import_channel'];}?>" /> 
				【MQ消息队列路由名】</td>
			</tr>
			<tr class="message_queue_import_enable_class_skip_rule">
				<td class="rightstyle">消息队列交换名</td>
				<td><input	name="nns_config[message_queue_import_exchange]" style="width:200px;" id="nns_config" type="text"  value="<?php 
				if($action=="edit" && isset($edit_data['nns_config']['message_queue_import_exchange'])){echo $edit_data['nns_config']['message_queue_import_exchange'];}?>" /> 
				【MQ消息队列交换名】</td>
			</tr>
			<tr class="message_queue_import_enable_class_skip_rule">
				<td class="rightstyle">消息队列数据格式</td>
				<td><input	name="nns_config[message_queue_data_format]" style="width:200px;" id="nns_config" type="text"  value="<?php
					if($action=="edit" && isset($edit_data['nns_config']['message_queue_data_format'])){echo $edit_data['nns_config']['message_queue_data_format'];}?>" />
					【消息队列数据格式】</td>
			</tr>
			<tr>
				<td class="rightstyle">FTP消息注入开关</td>
				<td>
					<input name="nns_config[message_ftp_import_enable]" id="nns_config" type="radio"  value="0" <?php 
					if(isset($edit_data['nns_config']['message_ftp_import_enable']) && $edit_data['nns_config']['message_ftp_import_enable'] == 0){echo "checked='checked'";} ?> /> 
					关闭  
					<input name="nns_config[message_ftp_import_enable]" id="nns_config" type="radio"  value="1" <?php 
					if(isset($edit_data['nns_config']['message_ftp_import_enable']) && $edit_data['nns_config']['message_ftp_import_enable'] == 1){echo "checked='checked'";} ?> /> 
					开启【关闭  (循环拉取FTP消息); 开启 (不循环拉取FTP消息)】
				</td>
			</tr>
			<tr class="message_ftp_import_enable_class_skip_rule">
				<td class="rightstyle">读取FTP文件的后缀</td>
				<td><input	name="nns_config[message_ftp_import_file_ex]" style="width:200px;" id="nns_config" type="text"  value="<?php 
				if($action=="edit" && isset($edit_data['nns_config']['message_ftp_import_file_ex'])){echo $edit_data['nns_config']['message_ftp_import_file_ex'];}?>" /> 
				【未设置默认为xml的文件后缀】</td>
			</tr>
			<tr class="message_ftp_import_enable_class">
				<td class="rightstyle">FTP拉取的地址信息：</td>
				<td><input	name="nns_config[message_ftp_import_file_url]"  id="nns_config" type="text"  value="<?php 
				if($action=="edit" && isset($edit_data['nns_config']['message_ftp_import_file_url'])){echo $edit_data['nns_config']['message_ftp_import_file_url'];}?>" /> 
				【FTP拉取文件夹的ftp地址】</td>
			</tr>
			<tr class="message_ftp_import_enable_class_skip_rule">
				<td class="rightstyle">FTP拉取的地址信息：</td>
				<td><input	name="nns_config[message_ftp_import_mem_time]"  id="nns_config" type="text"  value="<?php 
				if($action=="edit" && isset($edit_data['nns_config']['message_ftp_import_mem_time'])){echo $edit_data['nns_config']['message_ftp_import_mem_time'];}?>" /> 
				【FTP拉取memcache缓存时间如果为配置默认1200秒】</td>
			</tr>
			<tr>
				<td class="rightstyle">产品包注入开关</td>
				<td>
					<input name="nns_config[message_queue_import_product_enable]" id="nns_config" type="radio"  value="0" <?php
					if(isset($edit_data['nns_config']['message_queue_import_product_enable']) && $edit_data['nns_config']['message_queue_import_product_enable'] == 0){echo "checked='checked'";} ?> />
					关闭
					<input name="nns_config[message_queue_import_product_enable]" id="nns_config" type="radio"  value="1" <?php
					if(isset($edit_data['nns_config	']['message_queue_import_product_enable']) && $edit_data['nns_config']['message_queue_import_product_enable'] == 1){echo "checked='checked'";} ?> />
					开启【关闭  (不接收产品包信息); 开启 (接收产品包信息)】
				</td>
			</tr>
			<tr class="message_queue_import_product_enable_class">
				<td class="rightstyle">产品同步注入CMS地址</td>
				<td><input	name="nns_config[import_product_to_cms_url]" style="width:200px;" id="nns_config" type="text"  value="<?php
					if($action=="edit" && isset($edit_data['nns_config']['import_product_to_cms_url'])){echo $edit_data['nns_config']['import_product_to_cms_url'];}?>" />
					【产品同步注入CMS地址】</td>
			</tr>
            <tr>
                <td class="rightstyle">第三方FTP扫描工单开关</td>
                <td>
                    <input name="nns_config[third_ftp_scan_enabled]" id="nns_config" type="radio"  value="0" <?php if(isset($edit_data['nns_config']['third_ftp_scan_enabled']) && $edit_data['nns_config']['third_ftp_scan_enabled'] == 0){echo "checked='checked'";} ?> /> 关闭
                    <input name="nns_config[third_ftp_scan_enabled]" id="nns_config" type="radio"  value="1" <?php if(isset($edit_data['nns_config']['third_ftp_scan_enabled']) && $edit_data['nns_config']['third_ftp_scan_enabled'] == 1){echo "checked='checked'";} ?> /> 开启【上游发送的工单任务是否在固定的目录生成第三方FTP扫描工单，关闭|未配置  不生成; 开启 生成到对应的目录中】
                </td>
            </tr>
            <tr class="third_ftp_scan_enabled_class_skip_rule">
                <td class="rightstyle">第三方FTP扫描工单地址</td>
                <td><input name="nns_config[third_ftp_scan_url]" style="width:300px;" id="nns_config" type="text"  value="<?php if($action=="edit" && isset($edit_data['nns_config']['third_ftp_scan_url'])){echo $edit_data['nns_config']['third_ftp_scan_url'];}?>" /> 【未配置时默认在本地/data/log_v2/ftp_scan目录下，仅支持FTP地址】</td>
            </tr>

			<tr>
				<td class="rightstyle"><b>CDN接收管理配置</b></td>
				<td> </td>
			</tr>
			<tr>
				<td class="rightstyle">CDN接收开关</td>
				<td>
					<input name="nns_config[cdn_receive_import_enable]" id="nns_config" type="radio"  value="0" <?php if(isset($edit_data['nns_config']['cdn_receive_import_enable']) && $edit_data['nns_config']['cdn_receive_import_enable'] == 0){echo "checked='checked'";} ?> /> 关闭  
					<input name="nns_config[cdn_receive_import_enable]" id="nns_config" type="radio"  value="1" <?php if(isset($edit_data['nns_config']['cdn_receive_import_enable']) && $edit_data['nns_config']['cdn_receive_import_enable'] == 1){echo "checked='checked'";} ?> /> 开启【关闭  (片源正常注入); 开启 (TV平台接收信息注入到CDN平台，并反馈TV播放串)】
				</td>
			</tr>
			<tr>
				<td class="rightstyle">CDN处理直播频道开关</td>
				<td>
					<input name="nns_config[cdn_handele_live_enable]" id="nns_config" type="radio"  value="0" <?php if(isset($edit_data['nns_config']['cdn_handele_live_enable']) && $edit_data['nns_config']['cdn_receive_import_enable'] == 0){echo "checked='checked'";} ?> /> 关闭  
					<input name="nns_config[cdn_handele_live_enable]" id="nns_config" type="radio"  value="1" <?php if(isset($edit_data['nns_config']['cdn_handele_live_enable']) && $edit_data['nns_config']['cdn_receive_import_enable'] == 1){echo "checked='checked'";} ?> /> 开启【关闭  (接收但是不处理); 开启 (接收并且处理)】
				</td>
			</tr>
			<tr>
				<td class="rightstyle">CDN处理直播频道模板ID</td>
				<td><input	name="nns_config[cdn_handele_live_media_template]" style="width:200px;" id="nns_config" type="text"  value="<?php 
				if($action=="edit" && isset($edit_data['nns_config']['cdn_handele_live_media_template'])){echo $edit_data['nns_config']['cdn_handele_live_media_template'];}?>" /> 
				【CDN处理直播频道模板ID】</td>
			</tr>
			<tr>
				<td class="rightstyle">CDN处理直播分集开关</td>
				<td>
					<input name="nns_config[cdn_handele_live_index_enable]" id="nns_config" type="radio"  value="0" <?php if(isset($edit_data['nns_config']['cdn_handele_live_index_enable']) && $edit_data['nns_config']['cdn_handele_live_index_enable'] == 0){echo "checked='checked'";} ?> /> 关闭  
					<input name="nns_config[cdn_handele_live_index_enable]" id="nns_config" type="radio"  value="1" <?php if(isset($edit_data['nns_config']['cdn_handele_live_index_enable']) && $edit_data['nns_config']['cdn_handele_live_index_enable'] == 1){echo "checked='checked'";} ?> /> 开启【关闭  (接收但是不处理); 开启 (接收并且处理)】
				</td>
			</tr>
			<tr>
				<td class="rightstyle">CDN处理直播片源开关</td>
				<td>
					<input name="nns_config[cdn_handele_live_media_enable]" id="nns_config" type="radio"  value="0" <?php if(isset($edit_data['nns_config']['cdn_handele_live_media_enable']) && $edit_data['nns_config']['cdn_handele_live_media_enable'] == 0){echo "checked='checked'";} ?> /> 关闭  
					<input name="nns_config[cdn_handele_live_media_enable]" id="nns_config" type="radio"  value="1" <?php if(isset($edit_data['nns_config']['cdn_handele_live_media_enable']) && $edit_data['nns_config']['cdn_handele_live_media_enable'] == 1){echo "checked='checked'";} ?> /> 开启【关闭  (接收但是不处理); 开启 (接收并且处理)】
				</td>
			</tr>
			<tr>
				<td class="rightstyle">CDN处理节目单开关</td>
				<td>
					<input name="nns_config[cdn_handele_playbill_enable]" id="nns_config" type="radio"  value="0" <?php if(isset($edit_data['nns_config']['cdn_handele_playbill_enable']) && $edit_data['nns_config']['cdn_handele_playbill_enable'] == 0){echo "checked='checked'";} ?> /> 关闭  
					<input name="nns_config[cdn_handele_playbill_enable]" id="nns_config" type="radio"  value="1" <?php if(isset($edit_data['nns_config']['cdn_handele_playbill_enable']) && $edit_data['nns_config']['cdn_handele_playbill_enable'] == 1){echo "checked='checked'";} ?> /> 开启【关闭  (接收但是不处理); 开启 (接收并且处理)】
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><b>站点配置</b></td>
				<td> </td>
			</tr>
			<tr>
				<td class="rightstyle">站点ID</td>
				<!-- <td><?php echo $edit_data['nns_config']['site_id'];?></td> -->
				<td><input	name="nns_config[site_id]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['site_id'];}?>" /></td>
			</tr>
			<tr>
				<td class="rightstyle">站点反馈接口</td>
				<!-- <td><?php echo $edit_data['nns_config']['site_callback_url'];?></td> -->
				<td><input	 name="nns_config[site_callback_url]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['site_callback_url'];}?>" /></td>
			</tr>
<!--			<tr>-->
<!--				<td class="rightstyle">站点注入ID</td>-->
<!--				-->
<!--				<td><input	 name="nns_config[import_id_mode]" id="nns_config" type="text"  value="--><?php //if($action=="edit"){echo $edit_data['nns_config']['import_id_mode'];}?><!--" />(0:guid;1:数字id;2:原始id;3:内容数字组合ID)</td>-->
<!--			</tr>-->
<!--			<tr>-->
<!--				<td class="rightstyle">内容提供商CSPID</td>-->
<!--				-->
<!--				<td><input	 name="nns_config[import_csp_id]" id="nns_config" type="text"  value="--><?php //if($action=="edit"){echo $edit_data['nns_config']['import_csp_id'];}?><!--" />内容提供商CSPID</td>-->
<!--			</tr>-->
			
			<tr>
				<td class="rightstyle">CDN是否通知第三方外部厂家</td>
				<td>
					<input name="nns_config[cdn_notify_third_party_enable]" id="nns_config" type="radio"  value="0" <?php if(!isset($edit_data['nns_config']['cdn_notify_third_party_enable']) || (isset($edit_data['nns_config']['cdn_notify_third_party_enable']) && $edit_data['nns_config']['cdn_notify_third_party_enable'] != '1')){echo "checked='checked'";} ?> /> 开启  
					<input name="nns_config[cdn_notify_third_party_enable]" id="nns_config" type="radio"  value="1" <?php if(isset($edit_data['nns_config']['cdn_notify_third_party_enable']) && $edit_data['nns_config']['cdn_notify_third_party_enable'] == 1){echo "checked='checked'";} ?> /> 关闭
					【开启:需要通知;关闭:不需通知，默认开启 （目前新疆电信对接福富用到）】
				</td>
			</tr>
			<tr>
				<td class="rightstyle">内容反馈模式</td>
				<td><input	 name="nns_config[content_feedback_mode]" style="width:100px;" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['content_feedback_mode'];}?>" />(0:不反馈;1:失败时反馈;2:始终反馈)</td>
			</tr>
			<tr>
				<td class="rightstyle">原始CP是否下发</td>
				<td><input	 name="nns_config[original_cp_enabled]" style="width:100px;" id="nns_config" type="text"  value="<?php if($action=="edit" && isset($edit_data['nns_config']['original_cp_enabled'])){echo $edit_data['nns_config']['original_cp_enabled'];}?>" />0或则未配置下发当前配置CP;1下发原始CP(producer);2下发默认0的CP</td>
			</tr>
			<tr>
				<td class="rightstyle">无条件注入分集/片源<font style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
				<td><input name="nns_config[check_metadata_enabled]" style="width:100px;" id="nns_config" type="text" rule="noempty" value="<?php if($action=="edit" && isset($edit_data['nns_config']['check_metadata_enabled'])){echo $edit_data['nns_config']['check_metadata_enabled'];}?>" />0:注入ID重复也可按照新增分集、片源注入;1:原逻辑注入，需判断注入ID+import_source是否存在;</td>
			</tr>	
			<tr>
				<td class="rightstyle">上游分集任务是否同步删除分集下片源</td>
				<td><input name="nns_config[sync_delete_media_with_index]" style="width:100px;" id="nns_config" type="text" value="<?php if($action=="edit" && isset($edit_data['nns_config']['sync_delete_media_with_index'])){echo $edit_data['nns_config']['sync_delete_media_with_index'];}?>" />0或不设置默认同步删除当前分集下片源;1:不删除当前分集下片源;</td>
			</tr>		

			<tr>
				<td class="rightstyle"><b>消息配置</b></td>
				<td> </td>
			</tr>

			<tr>
				<td class="rightstyle">消息队列第一次消息注入的条数</td>
				<td><input	 name="nns_config[message_first_import_num]" style="width:100px;" id="nns_config" type="text"  value="<?php if($action=="edit" && isset($edit_data['nns_config']['message_first_import_num'])){echo $edit_data['nns_config']['message_first_import_num'];}?>" /> (消息队列中消息状态为等待注入的消息获取条数上限)</td>
			</tr>
			<tr>
				<td class="rightstyle">失败消息注入开关</td>
				<td>
				<input	 name="nns_config[fail_message_import_enable]" id="nns_config" type="radio"  value="0" <?php if(isset($edit_data['nns_config']['fail_message_import_enable']) && $edit_data['nns_config']['fail_message_import_enable'] == 0){echo "checked='checked'";} ?> />关闭
				<input	 name="nns_config[fail_message_import_enable]" id="nns_config" type="radio"  value="1" <?php if(isset($edit_data['nns_config']['fail_message_import_enable']) && $edit_data['nns_config']['fail_message_import_enable'] == 1){echo "checked='checked'";} ?> />开启  (是否获取消息队列中状态为注入失败的消息,开启为获取)
				</td>
			</tr>
			<tr class="fail_message_import_enable_class">
				<td class="rightstyle">失败消息注入条数</td>
				<td><input	 name="nns_config[fail_message_import_num]" style="width:100px;" id="nns_config" type="text"  value="<?php if($action=="edit" && isset($edit_data['nns_config']['fail_message_import_num'])){echo $edit_data['nns_config']['fail_message_import_num'];}?>" /> (从消息队列中获取失败消息的条数上限)</td>
			</tr>
			<tr>
				<td class="rightstyle">失败消息重发次数</td>
				<td><input	 name="nns_config[fail_message_time]" style="width:100px;" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['fail_message_time'];}?>" /> (消息失败重发次数)</td>
			</tr>
			<tr>
				<td class="rightstyle">消息队列子级注入开关</td>
				<td>
					<input	 name="nns_config[message_child_import_enable]" id="nns_config" type="radio"  value="0" <?php if(isset($edit_data['nns_config']['message_child_import_enable']) && $edit_data['nns_config']['message_child_import_enable'] == 0){echo "checked='checked'";} ?> />关闭
					<input	 name="nns_config[message_child_import_enable]" id="nns_config" type="radio"  value="1" <?php if(isset($edit_data['nns_config']['message_child_import_enable']) && $edit_data['nns_config']['message_child_import_enable'] == 1){echo "checked='checked'";} ?> />开启    (是否获取主媒资未注入、分集未注入消息状态的消息，开启为获取)
				</td>
			</tr>
			<tr class="message_child_import_enable_class">
				<td class="rightstyle">消息队列子级注入条数</td>
				<td><input	 name="nns_config[message_child_import_num]" style="width:100px;" id="nns_config" type="text"  value="<?php if($action=="edit" &&isset($edit_data['nns_config']['message_child_import_num'])){echo $edit_data['nns_config']['message_child_import_num'];}?>" /> (获取主媒资未注入、分集未注入消息状态消息的条数上限)</td>
			</tr>
			<tr>
				<td class="rightstyle">解析版本</td>
				<td>
					<input name="nns_config[message_parse_version]" id="nns_config" type="radio" value="0" <?php if(!isset($edit_data['nns_config']['message_parse_version']) || empty($edit_data['nns_config']['message_parse_version'])){echo "checked='checked'";}?>/>  解析版本V2【默认】
					<input name="nns_config[message_parse_version]" id="nns_config" type="radio" value="1" <?php if(isset($edit_data['nns_config']['message_parse_version']) && $edit_data['nns_config']['message_parse_version'] == 1){echo "checked='checked'";}?>/>  解析版本V4
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>消息解析模式</td>
				<td><input rule="noempty" name="nns_config[message_parse_model]" style="width:100px;" id="nns_config" type="text"  value="<?php 
					if($action=="edit"){echo $edit_data['nns_config']['message_parse_model'];}?>" />【参照config_v2注入来源清单】
				</td>
			</tr>
            <tr>
                <td class="rightstyle">消息生成注入队列的优先级</td>
                <td><input	 name="nns_config[source_import_priority_level]" style="width:100px;" id="nns_config" type="text"  value="<?php if($action=="edit" && isset($edit_data['nns_config']['source_import_priority_level'])){echo $edit_data['nns_config']['source_import_priority_level'];}?>" /> 【注入队列则根据来源配置的优先级按从高到低的顺序执行（当多个来源对同一个下游分发时使用）】</td>
            </tr>
			<tr>
				<td class="rightstyle">临时消息开关</td>
				<td>
					<input name="nns_config[temp_message_enabled]" id="nns_config" type="radio" value="0" <?php if(isset($edit_data['nns_config']['temp_message_enabled']) && $edit_data['nns_config']['temp_message_enabled'] == 0){echo "checked='checked'";}?>/>  关闭
					<input name="nns_config[temp_message_enabled]" id="nns_config" type="radio" value="1" <?php if(isset($edit_data['nns_config']['temp_message_enabled']) && $edit_data['nns_config']['temp_message_enabled'] == 1){echo "checked='checked'";}?>/>  开启
				</td>
			</tr>
			<tr>
				<td class="rightstyle">消息分组开关</td>
				<td>
					<input name="nns_config[message_group_enabled]" id="nns_config" type="radio" value="0" <?php if(isset($edit_data['nns_config']['message_group_enabled']) && $edit_data['nns_config']['message_group_enabled'] == 0){echo "checked='checked'";}?>/>  关闭
					<input name="nns_config[message_group_enabled]" id="nns_config" type="radio" value="1" <?php if(isset($edit_data['nns_config']['message_group_enabled']) && $edit_data['nns_config']['message_group_enabled'] == 1){echo "checked='checked'";}?>/>  开启
				     【厂家注入一个XML存在多部片的情况,开启:全部处理完毕后一条记录反馈;关闭:原逻辑 单条反馈,默认关闭】
				</td>
			</tr>
			<tr>
				<td class="rightstyle">限制消息超时时限开关</td>
				<td>
					<input name="nns_config[message_import_overtime_enabled]" id="nns_config" type="radio" value="0" <?php if(isset($edit_data['nns_config']['message_import_overtime_enabled']) && $edit_data['nns_config']['message_import_overtime_enabled'] == 0){echo "checked='checked'";}?>/>  关闭
					<input name="nns_config[message_import_overtime_enabled]" id="nns_config" type="radio" value="1" <?php if(isset($edit_data['nns_config']['message_import_overtime_enabled']) && $edit_data['nns_config']['message_import_overtime_enabled'] == 1){echo "checked='checked'";}?>/>  开启
				</td>
			</tr>
			<tr class="message_import_overtime_enabled_class">
				<td class="rightstyle">消息注入超时时限</td>
				<td><input	 name="nns_config[message_import_overtime]" style="width:100px;" id="nns_config" type="text"  value="<?php if($action=="edit" &&isset($edit_data['nns_config']['message_import_overtime'])){echo $edit_data['nns_config']['message_import_overtime'];}?>" /> 【以注入播控平台多长时间为超时，单位秒】</td>
			</tr>
			<tr>
				<td class="rightstyle">异步消息队列开关</td>
				<td>
					<input name="nns_config[asyn_feedback_message_enabled]" id="nns_config" type="radio" value="0" <?php if(isset($edit_data['nns_config']['asyn_feedback_message_enabled']) && $edit_data['nns_config']['asyn_feedback_message_enabled'] == 0){echo "checked='checked'";}?>/>  关闭
					<input name="nns_config[asyn_feedback_message_enabled]" id="nns_config" type="radio" value="1" <?php if(isset($edit_data['nns_config']['asyn_feedback_message_enabled']) && $edit_data['nns_config']['asyn_feedback_message_enabled'] == 1){echo "checked='checked'";}?>/>  开启
				</td>
			</tr>
			<tr>
				<td class="rightstyle">消息延时注入队列开关</td>
				<td>
					<input name="nns_config[delayed_message_import_enabled]" id="nns_config" type="radio" value="0" <?php if(isset($edit_data['nns_config']['delayed_message_import_enabled']) && $edit_data['nns_config']['delayed_message_import_enabled'] == 0){echo "checked='checked'";}?>/>  关闭
					<input name="nns_config[delayed_message_import_enabled]" id="nns_config" type="radio" value="1" <?php if(isset($edit_data['nns_config']['delayed_message_import_enabled']) && $edit_data['nns_config']['delayed_message_import_enabled'] == 1){echo "checked='checked'";}?>/>  开启
					【REDIS控制队列，用于上下线、媒资打包、推荐位等队列】
				</td>
			</tr>
			<tr class="delayed_message_import_enabled_class">
				<td class="rightstyle">延时队列超时时限</td>
				<td><input	 name="nns_config[delayed_message_import_overtime]" style="width:100px;" id="nns_config" type="text"  value="<?php if($action=="edit" &&isset($edit_data['nns_config']['delayed_message_import_overtime'])){echo $edit_data['nns_config']['delayed_message_import_overtime'];}?>" /> 【延时队列超时上限,单位秒(不配置默认86400S)】</td>
			</tr>
			
			
			<tr>
				<td class="rightstyle"><b>中心流程方向配置</b></td>
				<td> </td>
			</tr>
			<tr>
				<td class="rightstyle">自动审核开关：</td>
				<td><input   name="nns_config[auto_audit_enable]" style="width:100px;" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['auto_audit_enable'];}?>" />	 0或则未配置关闭 ;1 开启</td>
			</tr>
			<tr>
				<td class="rightstyle">媒资审核结构：</td>
				<td><input	name="nns_config[audit_model]" style="width:100px;" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['audit_model'];}?>" />	 0或则未配置主媒资、分集、片源注入审核队列;1主媒资、分集注入审核队列;2主媒资审核队列</td>
			</tr>
			<tr>
				<td class="rightstyle">敏感词库过滤开关：</td>
				<td><input	name="nns_config[sensitive_word_enabled]" style="width:100px;" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['sensitive_word_enabled'];}?>" />	 0或者不配置为需通过敏感词库筛选;1为不通过敏感词库筛选</td>
			</tr>
			<tr>
				<td class="rightstyle">审核流程：</td>
				<td><input	name="nns_config[flow_audit]" style="width:100px;" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['flow_audit'];}?>" />	 0或则未配置走以前的流程;1审核，不注入</td>
			</tr>
			<tr>
				<td class="rightstyle">审核反馈方式：</td>
				<td><input	name="nns_config[flow_audit_notify]" style="width:100px;" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['flow_audit_notify'];}?>" /> 	0或则未配置则不反馈;1 amqp反馈</td>
			</tr>
			<tr>
				<td class="rightstyle">上下线流程：</td>
				<td><input   name="nns_config[flow_unline]" style="width:100px;" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['flow_unline'];}?>" /> 	0或则未配置关闭 ;1开启</td>
			</tr>
			<tr>
				<td class="rightstyle">上下线反馈方式：</td>
				<td><input   name="nns_config[flow_unline_notify]" style="width:100px;" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['flow_unline_notify'];}?>" /> 	0或则未配置则不反馈 ;1 qmqp反馈</td>
			</tr>
			<tr>
				<td class="rightstyle"><b>CP控制切片配置</b></td>
				<td></td>
			</tr>
			<tr>
				<td class="rightstyle">外部源ID</td>
				<td>
				<input name="nns_config[clip_custom_origin_id]" id="nns_config" type="text"  value="<?php if($action=="edit" && isset($edit_data['nns_config']['clip_custom_origin_id'])){echo $edit_data['nns_config']['clip_custom_origin_id'];}?>" />
				【MSP平台上配置的外部源ID列表,填入的时候添加MSP的外部源ID即可，为空则默认走以前的老逻辑，多个外部源ID以;分割】
				</td>
			</tr>
			<tr>
				<td class="rightstyle">删除CP源物理文件开关</td>
				<td>
					<input name="nns_config[del_cp_file_enabled]" id="nns_config" type="radio" value="0" <?php if(isset($edit_data['nns_config']['del_cp_file_enabled']) && $edit_data['nns_config']['del_cp_file_enabled'] == 0){echo "checked='checked'";}?>/>  关闭
					<input name="nns_config[del_cp_file_enabled]" id="nns_config" type="radio" value="1" <?php if(isset($edit_data['nns_config']['del_cp_file_enabled']) && $edit_data['nns_config']['del_cp_file_enabled'] == 1){echo "checked='checked'";}?>/>  开启 &nbsp;&nbsp;【默认不删除CP存放在FTP上的物理文件；开启删除时只有片源为最终注入状态时才能删除】
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><b>上游CP/SP控制转码配置</b></td>
				<td></td>
			</tr>
			<tr>
				<td class="rightstyle">上游CP/SP转码开关:</td>
				<td>
					<input name="nns_config[cp_transcode_file_enabled]" id="nns_config" type="radio" value="0" <?php if(isset($edit_data['nns_config']['cp_transcode_file_enabled']) && $edit_data['nns_config']['cp_transcode_file_enabled'] != 1){echo "checked='checked'";}?>/>  关闭
					<input name="nns_config[cp_transcode_file_enabled]" id="nns_config" type="radio" value="1" <?php if(isset($edit_data['nns_config']['cp_transcode_file_enabled']) && $edit_data['nns_config']['cp_transcode_file_enabled'] == 1){echo "checked='checked'";}?>/>  开启 
					&nbsp;&nbsp;【0或者为空 关闭 | 1:开启 默认关闭】
				</td>
			</tr>
			<tr class='cp_transcode_file_enabled_class_skip_rule'>
				<td class="rightstyle">上游CP/SP点播转码消息反馈地址:</td>
				<td>
					<input style="width:500px;" name="nns_config[video_source_notify_url]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['video_source_notify_url'];}?>" />
					&nbsp;&nbsp;【上游CP/SP点播转码消息反馈地址】
				</td>
			</tr>
			<tr>
				<td class="rightstyle">上游CP/SP直播转点播开关:</td>
				<td>
					<input name="nns_config[cp_playbill_to_media_enabled]" id="nns_config" type="radio" value="0" <?php if(isset($edit_data['nns_config']['cp_playbill_to_media_enabled']) && $edit_data['nns_config']['cp_playbill_to_media_enabled'] != 1){echo "checked='checked'";}?>/>  关闭
					<input name="nns_config[cp_playbill_to_media_enabled]" id="nns_config" type="radio" value="1" <?php if(isset($edit_data['nns_config']['cp_playbill_to_media_enabled']) && $edit_data['nns_config']['cp_playbill_to_media_enabled'] == 1){echo "checked='checked'";}?>/>  开启 
					&nbsp;&nbsp;【0或者为空 关闭 | 1:开启 默认关闭】
				</td>
			</tr>
			<tr class='cp_playbill_to_media_enabled_class'>
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>上游CP/SP直播转点播消息反馈地址:</td>
				<td>
					<input rule="noempty" name="nns_config[source_notify_url]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['source_notify_url'];}?>" />
					&nbsp;&nbsp;【上游CP/SP直播转点播消息反馈地址】
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><b>DRM指令配置</b></td>
				<td> </td>
			</tr>
			<tr>
				<td class="rightstyle">DRM主开关</td>
				<td>
					<input name="nns_config[main_drm_enabled]" id="nns_config" type="radio" value="0" <?php if(isset($edit_data['nns_config']['main_drm_enabled']) && $edit_data['nns_config']['main_drm_enabled'] == 0){echo "checked='checked'";}?>/>  关闭
					<input name="nns_config[main_drm_enabled]" id="nns_config" type="radio" value="1" <?php if(isset($edit_data['nns_config']['main_drm_enabled']) && $edit_data['nns_config']['main_drm_enabled'] == 1){echo "checked='checked'";}?>/>  开启
				</td>
			</tr>
			
			<tr>
				<td class="rightstyle">切片DRM厂商标识</td>
				<td><input style="width:100px;" name="nns_config[q_drm_identify]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['q_drm_identify'];}?>" />【DRM加密方案】</td>
			</tr>
			
			<tr>
				<td class="rightstyle">切片前DRM开关</td>
				<td>
					<input name="nns_config[q_disabled_drm]" id="nns_config" type="radio" value="0" <?php if(isset($edit_data['nns_config']['q_disabled_drm']) && $edit_data['nns_config']['q_disabled_drm'] == 0){echo "checked='checked'";}?>/>  关闭
					<input name="nns_config[q_disabled_drm]" id="nns_config" type="radio" value="1" <?php if(isset($edit_data['nns_config']['q_disabled_drm']) && $edit_data['nns_config']['q_disabled_drm'] == 1){echo "checked='checked'";}?>/>  开启
				</td>
			</tr>
<!--			<tr>-->
<!--				<td class="rightstyle">DRM登陆地址</td>-->
<!--				<td><input name="nns_config[q_drm_soap_login_url]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['q_drm_soap_login_url'];}?>" />多个厂商配置多个地址,以"||"分割,并与厂商一一对应</td>-->
<!--			</tr>-->
<!--			<tr>-->
<!--				<td class="rightstyle">DRM内容管理地址</td>-->
<!--				<td><input name="nns_config[q_drm_soap_url]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['q_drm_soap_url'];}?>" />多个厂商配置多个地址,以"||"分割,并与厂商一一对应</td>-->
<!--			</tr>-->
<!--			<tr>-->
<!--				<td class="rightstyle">DRM的管理域ID</td>-->
<!--				<td><input name="nns_config[q_drm_network_id]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['q_drm_network_id'];}?>" />多个厂商配置多个管理域,以"||"分割,并与厂商一一对应,若没有置空</td>-->
<!--			</tr>-->
<!--			<tr>-->
<!--				<td class="rightstyle">DRM的管理域类型</td>-->
<!--				<td><input name="nns_config[q_drm_network_type]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['q_drm_network_type'];}?>" />多个厂商配置多个管理域类型,以"||"分割,并与厂商一一对应,若没有置空</td>-->
<!--			</tr>-->
<!--			<tr>-->
<!--				<td class="rightstyle">DRM登陆账号</td>-->
<!--				<td><input name="nns_config[q_drm_user_name]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['q_drm_user_name'];}?>" />多个厂商配置多个登陆账号,以"||"分割,并与厂商一一对应,若没有置空</td>-->
<!--			</tr>-->
<!--			<tr>-->
<!--				<td class="rightstyle">DRM登录密码</td>-->
<!--				<td><input name="nns_config[q_drm_user_password]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['q_drm_user_password'];}?>" />多个厂商配置多个登陆密码,以"||"分割,并与厂商一一对应,若没有置空</td>-->
<!--			</tr>-->
			<tr class='q_disabled_drm_class'>
				<td class="rightstyle">切片前DRM密钥地址</td>
				<td><input  name="nns_config[q_drm_key_file_url]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['q_drm_key_file_url'];}?>" /></td>
			</tr>
			
			
			<tr>
				<td class="rightstyle">DRM切片加密开关</td>
				<td>
					<input name="nns_config[clip_drm_enabled]" id="nns_config" type="radio" value="0" <?php if(isset($edit_data['nns_config']['clip_drm_enabled']) && $edit_data['nns_config']['clip_drm_enabled'] == 0){echo "checked='checked'";}?>/>  关闭
					<input name="nns_config[clip_drm_enabled]" id="nns_config" type="radio" value="1" <?php if(isset($edit_data['nns_config']['clip_drm_enabled']) && $edit_data['nns_config']['clip_drm_enabled'] == 1){echo "checked='checked'";}?>/>  开启
				</td>
			</tr>
			<!-- 
			<tr class='clip_drm_enabled_class'>
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>DRM切片加密地址</td>
				<td><input rule="noempty" name="nns_config[clip_drm_url]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['clip_drm_url'];}?>" /></td>
			</tr>
			 -->
			<tr class='clip_drm_enabled_class'>
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>认证授权服务器地址</td>
				<td><input rule="noempty" name="nns_config[clip_drm_config_params][auth_server_ip]" id="nns_config" type="text" value="<?php if($action=="edit" && isset($edit_data['nns_config']['clip_drm_config_params']['auth_server_ip'])){echo $edit_data['nns_config']['clip_drm_config_params']['auth_server_ip'];}else{ echo "";}?>" /> 本地字节顺序</td>
			</tr>
			<tr class='clip_drm_enabled_class'>
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>认证授权服务器端口号</td>
				<td><input rule="noempty" name="nns_config[clip_drm_config_params][auth_server_port]" id="nns_config" type="text" value="<?php if($action=="edit" && isset($edit_data['nns_config']['clip_drm_config_params']['auth_server_port'])){echo $edit_data['nns_config']['clip_drm_config_params']['auth_server_port'];}else{ echo "";}?>" /> 本地字节顺序</td>
			</tr>
			<tr class='clip_drm_enabled_class'>
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>内容密钥服务器管理地址</td>
				<td><input rule="noempty" name="nns_config[clip_drm_config_params][key_server_ip]" id="nns_config" type="text" value="<?php if($action=="edit" && isset($edit_data['nns_config']['clip_drm_config_params']['key_server_ip'])){echo $edit_data['nns_config']['clip_drm_config_params']['key_server_ip'];}else{ echo "";}?>" /> 本地字节顺序</td>
			</tr>
			<tr class='clip_drm_enabled_class'>
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>内容密钥服务器端口号</td>
				<td><input rule="noempty" name="nns_config[clip_drm_config_params][key_server_port]" id="nns_config" type="text" value="<?php if($action=="edit" && isset($edit_data['nns_config']['clip_drm_config_params']['key_server_port'])){echo $edit_data['nns_config']['clip_drm_config_params']['key_server_port'];}else{ echo "";}?>" /> 本地字节顺序</td>
			</tr>
			<tr class='clip_drm_enabled_class'>
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>节目号</td>
				<td><input rule="noempty" name="nns_config[clip_drm_config_params][program_number]" id="nns_config" type="text" value="<?php if($action=="edit" && isset($edit_data['nns_config']['clip_drm_config_params']['program_number'])){echo $edit_data['nns_config']['clip_drm_config_params']['program_number'];}else{ echo "0";}?>" /> 如果为0默认选择码流中的第一套节目</td>
			</tr>
			<tr class='clip_drm_enabled_class'>
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>预览时间</td>
				<td><input rule="noempty" name="nns_config[clip_drm_config_params][preview_time]" id="nns_config" type="text" value="<?php if($action=="edit" && isset($edit_data['nns_config']['clip_drm_config_params']['preview_time'])){echo $edit_data['nns_config']['clip_drm_config_params']['preview_time'];}else{ echo "";}?>" /> 单位/秒</td>
			</tr>
			<tr class='clip_drm_enabled_class'>
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>打包支持模式</td>
				<td><input rule="noempty" name="nns_config[clip_drm_config_params][package_mode]" id="nns_config" type="text" value="<?php if($action=="edit" && isset($edit_data['nns_config']['clip_drm_config_params']['package_mode'])){echo $edit_data['nns_config']['clip_drm_config_params']['package_mode'];}else{ echo "0";}?>" /> 1为支持苹果打包模式，0为单文件模式，默认为0</td>
			</tr>
			<tr class='clip_drm_enabled_class_skip_rule'>
				<td class="rightstyle">内容标识</td>
				<td><input name="nns_config[clip_drm_config_params][p_cid]" id="nns_config" type="text" value="<?php if($action=="edit" && isset($edit_data['nns_config']['clip_drm_config_params']['p_cid'])){echo $edit_data['nns_config']['clip_drm_config_params']['p_cid'];}else{ echo "";}?>" /> 默认为空</td>
			</tr>
			<tr class='clip_drm_enabled_class'>
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>加密模式</td>
				<td><input rule="noempty" name="nns_config[clip_drm_config_params][encrypt_mode]" id="nns_config" type="text" value="<?php if($action=="edit" && isset($edit_data['nns_config']['clip_drm_config_params']['encrypt_mode'])){echo $edit_data['nns_config']['clip_drm_config_params']['encrypt_mode'];}else{ echo "2";}?>" /> 0 加密BP帧，1加密I帧，2加密所有帧，默认为2</td>
			</tr>
			<tr class='clip_drm_enabled_class'>
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>加密帧数</td>
				<td><input rule="noempty" name="nns_config[clip_drm_config_params][max_frame_packet_encryption]" id="nns_config" type="text" value="<?php if($action=="edit" && isset($edit_data['nns_config']['clip_drm_config_params']['max_frame_packet_encryption'])){echo $edit_data['nns_config']['clip_drm_config_params']['max_frame_packet_encryption'];}else{ echo "1";}?>" /> 默认为1</td>
			</tr>
			<tr class='clip_drm_enabled_class'>
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>跳过帧数</td>
				<td><input rule="noempty" name="nns_config[clip_drm_config_params][max_frame_packet_encrypt_skip]" id="nns_config" type="text" value="<?php if($action=="edit" && isset($edit_data['nns_config']['clip_drm_config_params']['max_frame_packet_encrypt_skip'])){echo $edit_data['nns_config']['clip_drm_config_params']['max_frame_packet_encrypt_skip'];}else{ echo "0";}?>" /> 加密多少帧，跳过多少帧，默认加密所有帧</td>
			</tr>
			
			<tr>
				<td class="rightstyle">Marlin DRM开关</td>
				<td>
					<input name="nns_config[q_marlin_drm_enable]" id="nns_config" type="radio" value="0" 
					<?php if(isset($edit_data['nns_config']['q_marlin_drm_enable']) && $edit_data['nns_config']['q_marlin_drm_enable'] == 0){echo "checked='checked'";}?>/>  关闭
					<input name="nns_config[q_marlin_drm_enable]" id="nns_config" type="radio" value="1" 
					<?php if(isset($edit_data['nns_config']['q_marlin_drm_enable']) && $edit_data['nns_config']['q_marlin_drm_enable'] == 1){echo "checked='checked'";}?>/>  开启
					【开启:Marlin DRM开启;关闭:Marlin DRM关闭】
				</td>
			</tr>
			<tr class='q_marlin_drm_enable_class'>
				<td class="rightstyle">点播生成密钥条数</td>
				<td><input rule="noempty" name="nns_config[marlin_drm_config][marlin_drm_vod_num]" id="nns_config" type="text" 
				value="<?php if($action=="edit" && isset($edit_data['nns_config']['marlin_drm_config']['marlin_drm_vod_num'])){echo $edit_data['nns_config']['marlin_drm_config']['marlin_drm_vod_num'];}?>" /> 
				【点播生成密钥条数：如果未配置或者为空默认为1】</td>
			</tr>
			<tr class='q_marlin_drm_enable_class'>
				<td class="rightstyle">直播生成密钥条数</td>
				<td><input rule="noempty" name="nns_config[marlin_drm_config][marlin_drm_live_num]" id="nns_config" type="text" 
				value="<?php if($action=="edit" && isset($edit_data['nns_config']['marlin_drm_config']['marlin_drm_live_num'])){echo $edit_data['nns_config']['marlin_drm_config']['marlin_drm_live_num'];}?>" /> 
				【直播生成密钥条数：如果未配置或者为空默认为10】</td>
			</tr>
<!--			<tr>-->
<!--				<td class="rightstyle"><b>切片指令配置</b></td>-->
<!--				<td> </td>-->
<!--			</tr>-->
<!--			<tr>-->
<!--				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>切片任务暂停</td>-->
<!--				<td><input  rule="noempty"  style="width:100px;"  name="nns_config[disabled_clip]" id="nns_config" type="text"  value="--><?php //if($action=="edit"){echo $edit_data['nns_config']['disabled_clip'];}?><!--" />0:开启BK切片任务;1:关闭BK切片任务且不生成注入队列;2:关闭BK切片任务且生成注入队列</td>-->
<!--			</tr>-->
			
			<tr>
				<td class="rightstyle"><b>图片处理配置</b></td>
				<td> </td>
			</tr>
			<tr>
				<td class="rightstyle">是否开启图片下载</td>
				<!-- <td><?php echo $edit_data['nns_config']['g_ftp_conf']['download_img_enabled'];?></td> -->
				<td>
					<input name="nns_config[g_ftp_conf][download_img_enabled]" id="nns_config" type="radio" value="1" <?php if(isset($edit_data['nns_config']['g_ftp_conf']['download_img_enabled']) && $edit_data['nns_config']['g_ftp_conf']['download_img_enabled'] == 1){echo "checked='checked'";}?>/>  关闭
					<input name="nns_config[g_ftp_conf][download_img_enabled]" id="nns_config" type="radio" value="0" <?php if(isset($edit_data['nns_config']['g_ftp_conf']['download_img_enabled']) && $edit_data['nns_config']['g_ftp_conf']['download_img_enabled'] == 0){echo "checked='checked'";}?>/>  开启
                    &nbsp;&nbsp;【0|未配置开启 1 关闭将保留海报原始路径】
                </td>
			</tr>
            <tr>
                <td class="rightstyle">是否忽略图片处理失败</td>
                <td>
                    <input name="nns_config[g_ftp_conf][ignore_img_dowload_fail]" id="nns_config" type="radio" value="0" <?php if(isset($edit_data['nns_config']['g_ftp_conf']['ignore_img_dowload_fail']) && $edit_data['nns_config']['g_ftp_conf']['ignore_img_dowload_fail'] == 0){echo "checked='checked'";}?>/>  关闭
                    <input name="nns_config[g_ftp_conf][ignore_img_dowload_fail]" id="nns_config" type="radio" value="1" <?php if(isset($edit_data['nns_config']['g_ftp_conf']['ignore_img_dowload_fail']) && $edit_data['nns_config']['g_ftp_conf']['ignore_img_dowload_fail'] == 1){echo "checked='checked'";}?>/>  开启
                    &nbsp;&nbsp;【开启后，若主媒资或分集图片处理失败将忽略，不影响注入】
                </td>
            </tr>
            <!--xinxin.deng 2017年11月09日14:55:00 添加图片是否上传到ftp  start-->
            <tr>
                <td class="rightstyle">是否开启图片上传到ftp</td>
                <td>
                    <input name="nns_config[g_ftp_conf][upload_img_to_ftp_enabled]" id="nns_config" type="radio" value="0" <?php if(isset($edit_data['nns_config']['g_ftp_conf']['upload_img_to_ftp_enabled']) && $edit_data['nns_config']['g_ftp_conf']['upload_img_to_ftp_enabled'] != 1){echo "checked='checked'";}?>/>  关闭
                    <input name="nns_config[g_ftp_conf][upload_img_to_ftp_enabled]" id="nns_config" type="radio" value="1" <?php if(isset($edit_data['nns_config']['g_ftp_conf']['upload_img_to_ftp_enabled']) && $edit_data['nns_config']['g_ftp_conf']['upload_img_to_ftp_enabled'] == 1){echo "checked='checked'";}?>/>  开启
                    &nbsp;&nbsp;【0或者为空 关闭 | 1:开启 默认关闭】
                </td>
            </tr>
            <tr class='g_ftp_conf_upload_img_to_ftp_enabled_class'>
                <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>图片下载后上传到ftp地址:</td>
                <td>
                    <input rule="noempty" style="width:500px;" name="nns_config[g_ftp_conf][upload_img_to_ftp_url]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['g_ftp_conf']['upload_img_to_ftp_url'];}?>" />
                    &nbsp;&nbsp;【图片下载后上传到ftp地址，绝对路径】
                </td>
            </tr>
            <!--xinxin.deng 2017年11月09日14:55:00  end-->
			<tr>
				<td class="rightstyle">图片处理是否采用以下配置</td>
				<!-- <td><?php echo $edit_data['nns_config']['g_ftp_conf']['img_enabled'];?></td> -->
				<td>
					<input name="nns_config[g_ftp_conf][img_enabled]" id="nns_config" type="radio" value="0" <?php if(isset($edit_data['nns_config']['g_ftp_conf']['img_enabled']) && $edit_data['nns_config']['g_ftp_conf']['img_enabled'] == 0){echo "checked='checked'";}?>/>  关闭
					<input name="nns_config[g_ftp_conf][img_enabled]" id="nns_config" type="radio" value="1" <?php if(isset($edit_data['nns_config']['g_ftp_conf']['img_enabled']) && $edit_data['nns_config']['g_ftp_conf']['img_enabled'] == 1){echo "checked='checked'";}?>/>  开启
                    &nbsp;&nbsp;【0|未配置关闭 1 开启后配置文件中的配置将失效】
                </td>
			</tr>
			<tr>
				<td class="rightstyle">图片存储基本路径</td>
				<!-- <td><?php echo $edit_data['nns_config']['g_ftp_conf']['down_img_dir'];?></td> -->
				<td><input	name="nns_config[g_ftp_conf][down_img_dir]" id="nns_config" type="text"  value="<?php if($action=="edit" &&isset($edit_data['nns_config']['g_ftp_conf']['down_img_dir'])){echo $edit_data['nns_config']['g_ftp_conf']['down_img_dir'];}?>" /> 图片在近端存放地址(绝对路径)</td>
			</tr>
			<tr>
				<td class="rightstyle">下载地址</td>
				<!-- <td><?php echo $edit_data['nns_config']['g_ftp_conf']['domain'];?></td> -->
				<td><input	 name="nns_config[g_ftp_conf][domain]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['g_ftp_conf']['domain'];}?>" /> 图片在远端存放地址(相对地址)</td>
			</tr>
			<tr>
				<td class="rightstyle">匹配规则(正则内容)</td>
				<!-- <td><?php echo $edit_data['nns_config']['g_ftp_conf']['rule'];?></td> -->
				<td><input	 name="nns_config[g_ftp_conf][rule]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['g_ftp_conf']['rule'];}?>" /> 正则内容和匹配地址成对出现</td>
			</tr>
			<tr>
				<td class="rightstyle">匹配规则(匹配地址)</td>
				<!-- <td><?php echo $edit_data['nns_config']['g_ftp_conf']['rule_domain'];?></td> -->
				<td><input	 name="nns_config[g_ftp_conf][rule_domain]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['g_ftp_conf']['rule_domain'];}?>" /> 正则内容和匹配地址成对出现</td>
			</tr>
			<tr>
				<td class="rightstyle">播控手动上传图片路径</td>
				<!-- <td><?php echo $edit_data['nns_config']['g_ftp_conf']['handle_upload_img'];?></td> -->
				<td><input	name="nns_config[g_ftp_conf][handle_upload_img]" id="nns_config" type="text"  value="<?php if($action=="edit" &&isset($edit_data['nns_config']['g_ftp_conf']['handle_upload_img'])){echo $edit_data['nns_config']['g_ftp_conf']['handle_upload_img'];}?>" /> 【播控手动上传图片路径(相对路径)】</td>
			</tr>

			<!--feijian.gao 2017年2月23日20:01:53  start-->
            <tr>
                <td class="rightstyle"><b>分集替换逻辑</b></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">分集替换逻辑</td>
                <td><input name="nns_config[clip_replace_mode_one]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['clip_replace_mode_one'];}?>" style="width: 100px;" />【0或不填:存在则替换 1:禁止替换】</td>
            </tr>
			<tr>
				<td class="rightstyle"><b>片源替换逻辑【模式由低到高优先规则】</b></td>
				<td> </td>
			</tr>
			<tr>
				<td class="rightstyle">片源替换模式一</td>
				<td><input name="nns_config[media_replace_mode_one]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_replace_mode_one'];}?>" style="width: 100px;" />0 关 1开【注入ID不同且匹配信息相同】</td>
			</tr>
			<tr>
				<td class="rightstyle">片源替换模式二</td>
				<td><input name="nns_config[media_replace_mode_two]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_replace_mode_two'];}?>" style="width: 100px;" />0 关 1开【注入ID相同或除注入ID外匹配信息相同】</td>
			</tr>
			<tr>
				<td class="rightstyle">片源替换模式三</td>
				<td><input name="nns_config[media_replace_mode_three]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['media_replace_mode_three'];}?>" style="width: 100px;" />0 关 1开【高清晰度替换低清晰度规则】</td>
			</tr>
			<!--feijian.gao 2017年2月23日20:01:53  end-->
			<tr>
				<td class="rightstyle"><b>中心指令配置</b></td>
				<td> </td>
			</tr>
			<tr>
				<td class="rightstyle">下载的片源验证片源信息开关</td>
				<td>
					<input name="nns_config[is_need_real_media_info]" id="nns_config" type="radio" value="0" <?php if(isset($edit_data['nns_config']['is_need_real_media_info']) && $edit_data['nns_config']['is_need_real_media_info'] != '1'){echo "checked='checked'";}?>/>  关闭
					<input name="nns_config[is_need_real_media_info]" id="nns_config" type="radio" value="1" <?php if(isset($edit_data['nns_config']['is_need_real_media_info']) && $edit_data['nns_config']['is_need_real_media_info'] == 1){echo "checked='checked'";}?>/>  开启 【切片下载的时候获取片源的真实信息】
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>中心片库下载地址实际接口</td>
				<td><input  rule="noempty"  name="nns_config[down_url_real]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['down_url_real'];}?>" /></td>
			</tr>
			<tr>
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>中心片库下载是否采用FTP/HTTP</td>
				<td><input  rule="noempty" style="width:100px;" name="nns_config[down_method]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['down_method'];}?>" />1FTP,0HTTP</td>
			</tr>
			<tr>
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>中心片库下载FTP模式</td>
				<td><input  rule="noempty" style="width:100px;" name="nns_config[down_method_ftp]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['down_method_ftp'];}?>" />  1主动模式,0被动模式</td>
			</tr>
			<tr>
				<td class="rightstyle">中心片库下载url中文符号转换模式</td>
				<td><input  style="width:100px;" name="nns_config[down_method_ftp_illegal_character]" id="nns_config" type="text"  value="<?php
				if($action=="edit" && isset($edit_data['nns_config']['down_method_ftp_illegal_character'])){echo $edit_data['nns_config']['down_method_ftp_illegal_character'];}?>" />  【1中文符号不需要转换,0或者未配置中文符号需要转换】</td>
			</tr>
            <tr>
                <td class="rightstyle"><b>CP-CDN配置</b></td>
                <td> </td>
            </tr>
            <tr>
                <td class="rightstyle">CP媒资注入CDN栏目配置</td>
                <td><input name="nns_config[import_cdn_category]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['import_cdn_category'];}?>" style="width: 300px;" />在CP媒资注入CDN时，CDN会提供一个栏目ID并要求注入到此CDN时需携带此栏目ID</td>
            </tr>
            <tr>
                <td class="rightstyle">CP媒资注入MSP CDN策略配置</td>
                <td><input name="nns_config[import_msp_cdn_policy]" id="nns_config" type="text"  value="<?php if($action=="edit"){echo $edit_data['nns_config']['import_msp_cdn_policy'];}?>" style="width: 300px;" />【CP媒资注入MSP CDN策略配置例如CDN1,CDN2】</td>
            </tr>
			</tbody>
		</table>
	</div>
	</form>
	<div class="controlbtns">
		<div class="controlbtn <?php echo $action; ?>"><a href="javascript:checkForm('集团管理','是否进行本次修改',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action == 'edit' ? '修改' : '添加'; ?></a></div>
		<div class="controlbtn back"><a href="javascript:returnToHistory();">返回</a></div>
		<div style="clear:both;"></div>
	</div>
</div>

</body>
</html>
