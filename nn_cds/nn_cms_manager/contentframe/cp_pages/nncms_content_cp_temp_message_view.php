<?php
header("Content-Type:text/xml;charset=utf-8");
include ("../../nncms_manager_inc.php");
include_once $np_path . 'np/np_xml_to_array.class.php';
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include_once $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include_once $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
$queue_task_model = new queue_task_model();
$param = array(
	'nns_cp_id'=> $_REQUEST['nns_cp_id'],
	'nns_message_id' => $_REQUEST['nns_message_id'],
	);
$message_info = $queue_task_model->get_cp_message_list($params);
$message_info = $message_info['data'][0];
echo $message_info['nns_message_content'];


