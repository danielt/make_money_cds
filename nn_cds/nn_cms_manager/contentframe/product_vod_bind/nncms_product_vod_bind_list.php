<?php header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135102");
$checkpri = null;
$nns_id = $_GET["nns_id"];
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/product/product_standard.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/product/product_vod_bind.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/video/vod.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
	"db_policy" => NL_DB_WRITE,
	"cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));
$sp_id = $_GET['sp_id'];
//$queue_task_model = new queue_task_model();


if($_GET['ajax']=='add'){
	$product_id = $_GET['product_id'];
	//"nncms_product_vod_bind.php?sp_id=<?php echo $sp_id\>&ajax=add&ids="+ids+"&is_black="+is_black
	$sp_id = $_GET['sp_id'];
	$ids= $_GET['ids'];
	$ids = explode(',', $ids);
	$ids = array_filter($ids);
	foreach ($ids as $id)
	{
		$re = nl_vod::get_video_info_by_id($dc,$id);
		if ($re['ret'] != 0)
		{
			echo '<script>alert("查询媒资失败");history.go(-1);</script>';die;
		}
		else if (!is_array($re['data_info']))
		{
			continue;
		}
		else
		{
			$check_arr = array(
				'where' => array(
					'nns_product_id' => $product_id,
					'nns_vod_id' => $re['data_info'][0]['nns_id'],
					'nns_cp_id' => $re['data_info'][0]['nns_cp_id'],
				)
			);
			$re_check = nl_product_vod_bind::query($dc,$check_arr);
			if ($re_check['ret'] != 0 )
			{
				echo '<script>alert("查询绑定表失败");history.go(-1);</script>';die;
			}
			else if(is_array($re_check['data_info']))
			{
				continue;
			}
			else
			{
				$insert_arr = array(
					'nns_product_id' => $product_id,
					'nns_vod_id' => $re['data_info'][0]['nns_id'],
					'nns_asset_import_id' => $re['data_info'][0]['nns_asset_import_id'],
					'nns_import_source' => $re['data_info'][0]['nns_import_source'],
					'nns_cp_id' => $re['data_info'][0]['nns_cp_id'],
				);
				$re_insert = nl_product_vod_bind::add($dc,$insert_arr);
				if ($re_insert['ret'] != 0)
				{
					echo '<script>alert("失败");history.go(-1);</script>';die;
				}
			}
		}
	}

	echo '<script>alert("成功");history.go(-1);</script>';
}elseif($_GET['ajax']=='del'){
	$sp_id = $_GET['sp_id'];
	$ids= $_GET['ids'];
	$ids = explode(',', $ids);
	$ids = array_filter($ids);
	foreach ($ids as $id)
	{
		$re_del = nl_product_vod_bind::delete($dc, $id);
		if ($re_del['ret'] != 0)
		{
			echo '<script>alert("失败");history.go(-1);</script>';die;
		}
	}
	echo '<script>alert("成功");history.go(-1);</script>';
}




$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 18;
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) {
	$page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit_array = array(
	'page_size'=>$page_size,
	'page_num'=> $page_num,
);
//搜索查询
$filter['where']['nns_product_id'] = $nns_id;
//$filter['nns_name'] = $_GET['nns_name'];
//$filter['day_picker_start'] = $_GET['day_picker_start'];
//$filter['day_picker_end'] = $_GET['day_picker_end'];
//$filter['nns_is_black'] = $_GET['nns_is_black'];

$result_data = nl_product_vod_bind::query_ex_info_by_id($dc, $nns_id);
if($result_data['ret'] != 0)
{
	echo '<script>alert("'.$result_data['reason'].'");history.go(-1);</script>';die;
}
$vod_total_num = isset($result_data['page_info']['total_count']) ? $result_data['page_info']['total_count'] : 0;
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) {
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;
$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);

$data = $result_data['data_info'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title></title>
	<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
	<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
	<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
	<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
	<script language="javascript" src="../../js/cms_cookie.js"></script>
	<script language="javascript" src="../../js/table.js.php"></script>
	<script language="javascript" src="../../js/checkinput.js.php"></script>
	<script language="javascript" src="../../js/rate.js"></script>
	<script language="javascript" src="../../js/image_loaded_func.js"></script>
	<script language="javascript" src="../../js/cms_datepicker.js"></script>
	<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
	<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>

	<script language="javascript">

		$(document).ready(function() {
			$('#clear_time').click(function(){
				$('#day_picker_start').val('');
				$('#day_picker_end').val('');

			});
			$(".selectbox").hide();
		});

		function close_select() {
			$(".selectbox").hide();
		}


		function add_black(){
			$("#select_frame").attr("src", "../sp_content_pages_v2/nncms_controls_category_select.php?sp_id=<?php echo $sp_id; ?>&method=video&packages=category,video&product_id=<?php echo $nns_id; ?>");
			$(".selectbox").show();
		}

		function set_vod_id(ids,product_id){

			window.location.href="../product_vod_bind/nncms_product_vod_bind_list.php?sp_id=<?php echo $sp_id?>&ajax=add&ids="+ids+"&product_id="+product_id;
		}

		function destroy(action){
			var r=confirm("是否进行该操作");
			if(r == true){
				var ids=getAllCheckBoxSelect();
				var url = "nncms_product_vod_bind_list.php?sp_id=<?php echo $sp_id?>&ajax=del&ids="+ids;
				window.location.href = url;
			}
		}
		function back()
		{
			var url = "../product/nncms_product_list.php";
			window.location.href = url;
		}


	</script>

</head>

<body>
<div class="selectbox">
	<iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
</div>

<div class="content">
	<div class="content_position">
		产品包列表/绑定媒资
	</div>
<!--	<div class="content_table">-->
<!--		<table width="100%" border="0" cellspacing="0" cellpadding="0">-->
<!--			<form action="" method="get">-->
<!--				<tbody>-->
<!--				<tr>-->
<!--					<td> 影片名：&nbsp;&nbsp;-->
<!--						<input type="text" name="nns_name" id="nns_name" value="--><?php //echo $_GET['nns_name']; ?><!--" style="width:200px;">-->
<!--						&nbsp;&nbsp;&nbsp;&nbsp;-->
<!--						黑白名单：&nbsp;&nbsp;-->
<!--						<select name="nns_is_black" style="width: 150px;">-->
<!--							<option value="" >全部</option>-->
<!--							<option value="10" --><?php //if($_GET['nns_is_black']=='10') echo 'selected="selected"'?><!--白名单</option>-->
<!--							<option value="11" --><?php //if($_GET['nns_is_black']=='11') echo 'selected="selected"'?><!--黑名单</option>-->
<!--						</select> &nbsp;&nbsp;&nbsp;&nbsp;-->
<!--						选择时间段：&nbsp;&nbsp;&nbsp;-->
<!--						<input name="day_picker_start" id="day_picker_start" type="text"  value="--><?php
//						if (isset($_GET['day_picker_start']))
//							echo $_GET['day_picker_start'];
//						?><!--" style="width:120px;" class="datetimepicker" callback="test" />-->
<!--						--->
<!--						<input name="day_picker_end" id="day_picker_end" type="text"  value="--><?php
//						if (isset($_GET['day_picker_end']))
//							echo $_GET['day_picker_end'];
//						?><!--" style="width:120px;" class="datetimepicker" callback="test" />-->
<!--						&nbsp;&nbsp;-->
<!--						<input type="button" id="clear_time" name="clear_time" value="清除时间"/>-->
<!--						<input type="hidden" name="sp_id" value="--><?php //echo $sp_id; ?><!--" />-->
<!--						<input type="hidden" name="actionsearch" value="search" />-->
<!--						&nbsp;&nbsp;&nbsp;&nbsp;-->
<!--						<input type="submit" value="--><?php //echo cms_get_lang('search'); ?><!--"  />-->
<!--					</td>-->
<!--				</tr>-->
<!--				</tbody>-->
<!--			</form>-->
<!--		</table>-->
<!---->
<!--	</div>-->
	<div class="content_table formtable">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<thead>
			<tr>
				<th>
					<input name="" type="checkbox" value="">
					序号</th>
				<th>产品名称</th>
				<th>媒资名称</th>
				<th>媒资注入ID</th>
				<th>注入CP</th>
			</tr>
			</thead>
			<tbody>
			<?php
			if($data!=null){
				$num = ($page_num - 1) * $page_size;
				foreach ($data as $item) {
					$num++;
					?>
					<tr>
						<td><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
						<td><?php echo $item['nns_product_name']; ?></td>
						<td><?php echo $item['nns_vod_name']; ?></td>
						<td><?php echo $item['nns_asset_import_id']; ?></td>
						<td><?php echo $item['nns_cp_id']; ?></td>						</tr>
					<?php
				}
			}$least_num = $g_manager_list_max_num - count($data);
			for ($i = 0; $i < $least_num; $i++) {
				?>
				<tr>


					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp; </td>
					<td>&nbsp; </td>


				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>

	<div class="pagecontrol">
		共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
		<?php if ($currentpage > 1) { ?>
			<a href="nncms_product_vod_bind.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="nncms_product_vod_bind.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php } else { ?>
			<span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
			<span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php } if ($currentpage < $vod_pages) { ?>
			<a href="nncms_product_vod_bind.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="nncms_product_vod_bind.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php } else { ?>
			<span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
			<span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php } ?>

		<?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
		<a href="javascript:go_page_num('nncms_product_vod_bind.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
		<?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
		<?php echo cms_get_lang('perpagenum'); ?>&nbsp;
		<input name="nns_list_max_num" id="nns_list_max_num" type="text"
			   value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
		<input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
			   onclick="refresh_vod_page();"/>&nbsp;&nbsp;

	</div>

	<div class="controlbtns">
		<div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
		<div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
		<div class="controlbtn add"><a href="javascript:add_black();">添加</a></div>
		<div class="controlbtn delete"><a href="javascript:destroy();">删除</a></div>
		<div class="controlbtn back"><a href="javascript:back();">返回</a></div>
		<!--<div class="controlbtn move"><a href="javascript:queue_start();">取消暂停</a></div>
		<div class="controlbtn delete"><a href="javascript:task_destroy();">删除</a></div>
		<div class="controlbtn move"><a href="javascript:queue_default();">重新注入</a></div>-->
		<div style="clear:both;"></div>
	</div>
</div>
</body>
</html>
