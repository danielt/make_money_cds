<?php 
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) 
{
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
include_once $nncms_config_path . "nn_logic/import_status/vod_index_import_status.class.php";

$dc = nl_get_dc(array(
       	'db_policy' => NL_DB_READ | NL_DB_WRITE,
        'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$dc->open();
$sp_id = $_GET['sp_id'];
$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 18;
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) 
{
	$page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit = $page_size;
$filter = null;
$filter['nns_org_id'] = $sp_id;
$vod_id = $_GET['vod_id'];
//获取分集绑定列表
$vod_index_bind = nl_vod_index_import_status::get_list_by_vod_id($dc,$vod_id,$offset, $limit,true);
if($vod_index_bind['ret'] != 0 || empty($vod_index_bind['data_info']))
{
	$data = array();
	$vod_total_num = 0;
}
else 
{
	$data = $vod_index_bind['data_info'];
	$vod_total_num = $vod_index_bind['page_info']['total_count'];
}

$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) 
{
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;

$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);

//获取提供商列表
// $producer_list = nl_vod_import_status::get_producer($dc, array('nns_org_id' => $_GET['sp_id']));
// $producer_list = $producer_list['data_info'];
//注入状态
$import_status = get_config_v2('g_import_status');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
		<script language="javascript">

			function refresh_vod_page() 
			{
				var num = $("#nns_list_max_num").val();
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}

		</script>

	</head>
	<body>
		<div class="content">
			<div class="content_position">
				分集注入状态
			</div>
		
			<div class="content_table formtable">

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th><!-- <input name="" type="checkbox" value=""> -->序号</th>
							<th>消息ID</th>
							<th>注入ID</th>
							<th>名称</th>			
							<th>动作</th>
							<th>来源</th>
							<th>状态</th>
							<th>描述</th>
							<th>创建时间</th>
							<th>修改时间</th>							
							<th>操作</th>						    
						</tr>
					</thead>
					<tbody>
						<?php
						if($data != null)
						{
							$num = ($page_num - 1) * $page_size;
							foreach ($data as $item) 
							{
								$num++;
						?>
						<tr>
						<td width="50px"><!-- <input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>" /> --><?php echo $num; ?></td>
						<td><?php echo $item['nns_message_id'];?></td>
						<td><?php echo $item['nns_import_id'];?></td>
						<td><?php echo $item['nns_name'];?></td>         
                        <td><?php echo $item['nns_action'];?></td>
                        <td><?php echo $item['nns_cp_id'];?></td>
                        <td>
                        <?php 
                        	if(!empty($import_status) && is_array($import_status))
                        	{
                        		echo $import_status[$item['nns_status']];
                        	}
                        ?>
                        </td>
                        <td><a href="">查看</a></td>
                        <td><?php echo $item['nns_create_time']; ?></td>  
                        <td><?php echo $item['nns_modify_time']; ?></td>
                        <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
							<a href="nncms_content_vod_media_import_status.php?id=<?php echo $item['nns_index_id'];?>&entry=index&sp_id=<?php echo $sp_id; ?>">片源状态</a>
                        </td>                         
                        </tr>
						<?php
							}
						}
						$least_num = $g_manager_list_max_num - count($data);
						for ($i = 0; $i < $least_num; $i++) 
						{
						?>
						<tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                     	</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="pagecontrol">
			  共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if ($currentpage > 1) { ?>
                                        <a href="nncms_content_pass_queue_list.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_pass_queue_list.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } if ($currentpage < $vod_pages) { ?>
                                        <a href="nncms_content_pass_queue_list.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="nncms_content_pass_queue_list.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } ?>

                                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                                <a href="javascript:go_page_num('nncms_content_pass_queue_list.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                                <input name="nns_list_max_num" id="nns_list_max_num" type="text"
                                       value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
                                       onclick="refresh_vod_page();"/>&nbsp;&nbsp;

          	</div>
       	<div class="controlbtns">
<!-- 	    	<div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div> -->
<!-- 	        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div> -->
<!-- 	        <div class="controlbtn move"><a href="javascript:msg_retry('import');">注入</a></div> -->
<!-- 			<div class="controlbtn move"><a href="javascript:do_audit('pass');">审核通过</a></div> -->
<!-- 			<div class="controlbtn move"><a href="javascript:do_audit('refuse');">审核不通过</a></div> -->
<!-- 			<div class="controlbtn move"><a href="javascript:do_pasue('stop');">暂停</a></div> -->
<!-- 			<div class="controlbtn move"><a href="javascript:do_pasue('normal');">取消暂停</a></div> -->
       			<div class="controlbtn back"><a href="javascript:history.go(-1);"><?php echo cms_get_lang('back'); ?></a></div>
            	<div style="clear:both;"></div>          
		</div>
		</div>
	</body>
</html>
