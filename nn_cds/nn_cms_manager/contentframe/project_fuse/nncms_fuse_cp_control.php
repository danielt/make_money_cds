<?php
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
include $nncms_config_path . 'nn_cms_manager/controls/nncms_controls_public_function.php';
require_once $nncms_db_path . 'nns_log' . DIRECTORY_SEPARATOR . 'nns_db_op_log_class.php';
$log_inst = new nns_db_op_log_class();

$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE, 
		'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$dc->open();
$params = $_REQUEST;

$action = $params['action'];
unset($params['action']);
$log_params = (!empty($params) && is_array($params)) ? json_encode($params) : '';
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);


/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/fuse/fuse_cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/sp/sp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';
session_start();
switch ($action)
{
	case "edit":
		$nns_id = $params['nns_id'];//key GUID
		unset($params['nns_id']);
        $result_unique = nl_fuse_cp::query_unique($dc, "where (nns_cp_id='{$params['nns_cp_id']}' or nns_cp_name='{$params['nns_cp_name']}') and nns_id !='{$nns_id}'");
	    if($result_unique['ret'] != 0)
	    {
	        echo "<script>alert('" . $result_unique['reason'] . "');history.go(-1);</script>";
	        break;
	    }
	    if(isset($result_unique['data_info'][0]['count']) && $result_unique['data_info'][0]['count'] >0)
	    {
	        echo "<script>alert('cp融合数据已经存在,不允许在添加');history.go(-1);</script>";
	        break;
	    }
	    $result_unique = nl_cp::query_unique($dc, "where nns_name='{$params['nns_cp_name']}'");
	    if($result_unique['ret'] != 0)
	    {
	        echo "<script>alert('" . $result_unique['reason'] . "');history.go(-1);</script>";
	        break;
	    }
	    if(isset($result_unique['data_info'][0]['count']) && $result_unique['data_info'][0]['count'] >0)
	    {
	        echo "<script>alert('cp列表数据已经存在,不允许在添加');history.go(-1);</script>";
	        break;
	    }
	    nl_cp::edit($dc, array('nns_name'=>$params['nns_cp_name']), $params['nns_cp_id']);
		$result = nl_fuse_cp::edit($dc, $params, $nns_id);
		if ($result['ret'])
		{
			echo "<script>alert('" . $result['reason'] . "');history.go(-1);</script>";
		}
		else
		{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "修改:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
			echo "<script>alert('修改成功');</script>";
		}
		break;
	case "add":
	    $result_unique = nl_fuse_cp::query_unique($dc, "where nns_cp_id='{$params['nns_cp_id']}' or nns_cp_name='{$params['nns_cp_name']}'");
	    if($result_unique['ret'] != 0)
	    {
	        echo "<script>alert('" . $result_unique['reason'] . "');history.go(-1);</script>";
	        break;
	    }
	    if(isset($result_unique['data_info'][0]['count']) && $result_unique['data_info'][0]['count'] >0)
	    {
	        echo "<script>alert('cp融合数据已经存在,不允许在添加');history.go(-1);</script>";
	        break;
	    }
	    $result_unique = nl_cp::query_unique($dc, "where nns_id='{$params['nns_cp_id']}' or nns_name='{$params['nns_cp_name']}'");
	    if($result_unique['ret'] != 0)
	    {
	        echo "<script>alert('" . $result_unique['reason'] . "');history.go(-1);</script>";
	        break;
	    }
	    if(isset($result_unique['data_info'][0]['count']) && $result_unique['data_info'][0]['count'] >0)
	    {
	        echo "<script>alert('cp列表数据已经存在,不允许在添加');history.go(-1);</script>";
	        break;
	    }
		$result = nl_fuse_cp::add($dc, $params);
		if ($result['ret'])
		{
		    echo "<script>alert('" . $result['reason'] . "');</script>";
		}
		else
		{
			nl_cp::add($dc, array('nns_id'=>$params['nns_cp_id'],'nns_name'=>$params['nns_cp_name'],'nns_state'=>$params['nns_state'],'nns_type'=>1,'nns_config'=>''));
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "添加:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
			echo "<script>alert('添加成功');</script>";
		}
		break;
	case "delete":
		$nns_ids = explode(",", $params['nns_id']);
		$arr_temp_del = array();
		$result_con = nl_fuse_cp::query_by_condition($dc,$nns_ids);
		if(!isset($result_con['data_info']) || empty($result_con['data_info']) || !is_array($result_con['data_info']))
		{
		    echo "<script>alert('删除成功');</script>";
		    break;
		}
		foreach ($result_con['data_info'] as $con_val)
		{
		    $nnsid = $con_val['nns_cp_id'];
			if (strlen($nnsid) < 1)
			{
				continue;
			}
			$result_cp_info = nl_cp::query_by_id($dc,$nnsid);//查出cP
			if(isset($result_cp_info['data_info']['nns_config']) && strlen($result_cp_info['data_info']['nns_config']) >0)
			{
			    continue;
			}
			$result_sp_info = nl_sp::query_bind_cp_info($dc,$nnsid);//查出SP
			if(is_array($result_sp_info['data_info']))
			{
				foreach($result_sp_info['data_info'] as $data_info_val)
				{
					$bind_cp = $data_info_val['nns_bind_cp'];
					$cp_id = str_replace($nnsid.',','',$bind_cp);
					$result = nl_sp::update_bind_cp_by_sp($dc,$data_info_val['nns_id'],$cp_id);
					if($result['ret'] != 0)
					{
						echo "<script>alert('".$result['reason']."');history.go(-1);</script>";die;
					}
				}
			}
			nl_cp::delete($dc,$nnsid);
			nl_fuse_cp::delete($dc,$con_val['nns_id']);
		}
		$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "删除:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
		echo "<script>alert('删除成功');</script>";
		break;
	default:
	    echo "<script>alert('没有此方式');</script>";
		break;
}
$partner_inst = null;
$log_inst = null;
echo "<script>self.location='".$base_file_name_base."_list.php?model_id={$params['nns_model_id']}';</script>";
