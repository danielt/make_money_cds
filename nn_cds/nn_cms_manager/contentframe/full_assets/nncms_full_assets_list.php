<?php 
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) 
{
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -5);
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
		"db_policy" => NL_DB_WRITE,
		"cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));



/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/full_assets/full_assets.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';

$result_cp = nl_cp::query_all($dc);
if($result_cp['ret'] != 0)
{
    echo '<script>alert("'.$result_cp['reason'].'");history.go(-1);</script>';die;
}
$result_cp = isset($result_cp['data_info']) ? $result_cp['data_info'] : null;
$arr_cp_info = array();
if(is_array($result_cp) && !empty($result_cp))
{
    foreach ($result_cp as $val)
    {
        if(!isset($val['nns_type']) || $val['nns_type']==1)
        {
            continue;
        }
        $arr_cp_info[$val['nns_id']] = $val['nns_id'].' / '.$val['nns_name'];
    }
}

$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 18;
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) 
{
	$page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit_array = array(
		'page_size'=>$page_size,
		'page_num'=> $page_num,
);

if (isset($_REQUEST['nns_cp_id']) && (strlen($_REQUEST['nns_cp_id']) >0))
{
    $filter_array['where']['nns_cp_id'] = $_REQUEST['nns_cp_id'];
}
if (isset($_REQUEST['nns_state']) && (strlen($_REQUEST['nns_state']) >0))
{
    $filter_array['where']['nns_state'] = $_REQUEST['nns_state'];
}
if (isset($_REQUEST['nns_asset_name']) && (strlen($_REQUEST['nns_asset_name']) >0))
{
    $filter_array['like']['nns_asset_name'] = $_REQUEST['nns_asset_name'];
}
if (isset($_REQUEST['nns_asset_id']) && (strlen($_REQUEST['nns_asset_id']) >0))
{
    $filter_array['where']['nns_asset_id'] = $_REQUEST['nns_asset_id'];
}
if (isset($_REQUEST['nns_asset_import_id']) && (strlen($_REQUEST['nns_asset_import_id']) >0))
{
    $filter_array['where']['nns_asset_import_id'] = $_REQUEST['nns_asset_import_id'];
}
$result_data = nl_full_assets::query($dc,$filter_array,$limit_array);

if($result_data['ret'] != 0)
{
	echo '<script>alert("'.$result_data['reason'].'");history.go(-1);</script>';die;
}

$vod_total_num = isset($result_data['page_info']['total_count']) ? $result_data['page_info']['total_count'] : 0;
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) {
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;

$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);


function pub_func_get_desc_alt($title,$item,$flag=true)
{
    $alt_html = '<div style="width:300px;padding:5px;"><div style="font-size:16px;font-weight:bold;line-height:22px;"></div>';
    $alt_html.='<div style="font-size:12px;line-height:20px;">' .
        '<div style="float:left;margin:0px 5px 5px 0px;display:block;">';
    $alt_html.='</div>';
    $alt_html.='<p><b>'.$title.'：</b><br/>';
    if(strlen($item) >0)
    {
        if($flag)
        {
            $item = json_decode(base64_decode($item),true);
            if(is_array($item))
            {
                foreach ($item as $key=>$val)
                {
                    $alt_html.="{$key} => ";
                    if (isset($val) && is_array($val))
                    {
                        foreach ( $val as $v)
                        {
                            $alt_html.= var_export($v, true);
                        }
                    }
                    else
                    {
                        $alt_html.="{$val}";
                    }
                    $alt_html.="<br/>";
                }
            }
        }
        else
        {
            $alt_html.=$item;
        }
    }
    $alt_html.='</p>';
    $alt_html.='</div></div>';
    return htmlspecialchars($alt_html);
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link href="../../css/public.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
		<script language="javascript">
			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				})
				return BoxKey;
			}
			function delete_id(){
				var r=confirm("是否进行批量删除操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "<?php echo $base_file_name_base;?>_control.php?action=delete&nns_id="+ids;
						window.location.href = url;
					}
				}
			}

			function reset(){
				var r=confirm("是否进行批量重置次数操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "<?php echo $base_file_name_base;?>_control.php?action=reset&nns_id="+ids;
						window.location.href = url;
					}
				}
			}

			function state(state){
				var r=confirm("是否进行批量重置次数操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "<?php echo $base_file_name_base;?>_control.php?nns_state="+state+"&action=state&nns_id="+ids;
						window.location.href = url;
					}
				}
			}
		</script>
	</head>
	<body>
		<div class="content">
		    <!-- 栏目导航描述div -->
			<div class="content_position">转码厂家列表</div>
			<!-- 表单查询div -->
			<div class="content_table">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<form action="<?php echo $base_file_name;?>" method="get">
					    <input type="hidden" name="view_list_max_num" id="view_list_max_num" value="<?php echo $g_manager_list_max_num; ?>" style="width:150px;">
						<tbody>
							<tr>
								<td>
								    &nbsp;&nbsp;CP厂家:
                                        <select name='nns_cp_id' style="width:150px;">
                                            <option value=''>全部</option>
                                            <?php if(is_array($arr_cp_info) && !empty($arr_cp_info)){foreach ($arr_cp_info as $cp_key=>$cp_val){?>
                                            <option value="<?php echo $cp_key?>" <?php if(isset($_GET['nns_cp_id']) && $_GET['nns_cp_id'] == $cp_key && strlen($_GET['nns_cp_id'])>0){ echo 'selected="selected"';}?>><?php echo $cp_val?></option>
                                            <?php }}?>
                                        </select>
									&nbsp;&nbsp;媒资注入ID:
										<input type="text" name="nns_asset_id" id="nns_asset_id" value="<?php echo $_GET['nns_asset_id']; ?>" style="width:150px;">
									&nbsp;&nbsp;原始ID:
									   <input type="text" name="nns_asset_import_id" id="nns_asset_import_id" value="<?php echo $_GET['nns_asset_import_id']; ?>" style="width:150px;">
									&nbsp;&nbsp;媒资名称:
										<input type="text" name="nns_asset_name" id="nns_asset_name" value="<?php echo $_GET['nns_asset_name']; ?>" style="width:150px;">
                                    &nbsp;&nbsp;状态:
                                        <select name='nns_state' style="width:150px;">
                                            <option value=''>全部</option>
                                            <option value="0" <?php if(isset($_GET['nns_state']) && $_GET['nns_state'] == '0'){ echo 'selected="selected"';}?>>未使用</option>
                                            <option value="1" <?php if(isset($_GET['nns_state']) && $_GET['nns_state'] == '1'){ echo 'selected="selected"';}?>>未补全</option>
                                            <option value="2" <?php if(isset($_GET['nns_state']) && $_GET['nns_state'] == '2'){ echo 'selected="selected"';}?>>已补全</option>
                                            <option value="3" <?php if(isset($_GET['nns_state']) && $_GET['nns_state'] == '3'){ echo 'selected="selected"';}?>>数据错误</option>
                                        </select>
									&nbsp;&nbsp;<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  />										
								</td>
							</tr>
						</tbody>
					</form>
				</table>
			</div>			
			<!-- 内容列表 DIV -->
			<div class="content_table formtable">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th width='40px'><input name="" type="checkbox" value="">序号</th>
							<th width='150px'>GUID</th>
							<th width='100px'>CP名称</th>
							<th width='100px'>原始ID</th>
							<th width='150px'>媒资注入ID</th>
							<th width='300px'>媒资名称</th>
							<th width='50px'>状态</th>
							<th width='50px'>操作次数</th>
							<th width='100px'>修改时间</th>
							<th width='80px'>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(is_array($result_data['data_info']) && !empty($result_data['data_info']))
						{
							$num = ($page_num - 1) * $page_size;
							foreach ($result_data['data_info'] as $item) 
							{
								$num++;
						?>
						      <tr>
						          <td><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
						          <td><?php echo $item['nns_id']; ?></td>
						          <td><?php if(isset($arr_cp_info[$item['nns_cp_id']])){ echo $arr_cp_info[$item['nns_cp_id']];}else{ echo "<font color='red'>CP厂家不存在<font>";}?></td>
						          <td><?php echo $item['nns_asset_import_id']; ?></td>
						          <td><?php echo $item['nns_asset_id']; ?></td>
						          <td>
						               <?php $arr_desc_info = pub_func_get_desc_alt('上传内容详情',$item['nns_content']);?>
						               <alt alt="<?php echo $arr_desc_info; ?>">
						                  <?php echo $item['nns_asset_name']; ?>
						               </alt>
						          </td>
							      <td>
							      <?php $reason_info = pub_func_get_desc_alt('执行信息',$item['nns_reason'],false);?>
						               <alt alt="<?php echo $reason_info; ?>">
    							      <?php 
    						              if($item['nns_state'] == '0')
    						              {
    						                  echo "<font color='green'>未使用</font>";
    						              }
    						              else if($item['nns_state'] == '1')
    						              {
    						                  echo "<font color='red'>未补全</font>";
    						              }
    						              else if($item['nns_state'] == '3')
    						              {
    						                  echo "<font color='red'>数据错误</font>";
    						              }
    						              else
    						              {
    						                  echo "<font>已补全</font>";
    						              }
    						          ?>
    						          </alt>
						          </td>
						          <td><?php echo $item['nns_execute_time']; ?></td>
						          <td><?php echo $item['nns_modify_time']; ?></td>
						          <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                                	<a href="<?php echo $base_file_name_base;?>_addedit.php?action=edit&nns_id=<?php echo $item["nns_id"];?>">修改</a>
                                	<a href="<?php echo $base_file_name_base;?>_control.php?action=delete&nns_id=<?php echo $item["nns_id"];?>">删除</a>
                                	<a href="<?php echo $base_file_name_base;?>_control.php?action=reset&nns_id=<?php echo $item["nns_id"];?>">重置次数</a>
                                	<?php 
						              if($item['nns_state'] == '0')
						              {
						            ?>
						                  <a href="<?php echo $base_file_name_base;?>_control.php?action=state&nns_state=1&nns_id=<?php echo $item["nns_id"];?>">未补全</a>
						                  <a href="<?php echo $base_file_name_base;?>_control.php?action=state&nns_state=2&nns_id=<?php echo $item["nns_id"];?>">已补全</a>
						             <?php 
						              }
						              else if($item['nns_state'] == '1')
						              {
						             ?>
						                  <a href="<?php echo $base_file_name_base;?>_control.php?action=state&nns_state=0&nns_id=<?php echo $item["nns_id"];?>">未使用</a>
						                  <a href="<?php echo $base_file_name_base;?>_control.php?action=state&nns_state=2&nns_id=<?php echo $item["nns_id"];?>">已补全</a>
						             <?php 
						              }
						              else if($item['nns_state'] == '3')
						              {
						                  ?>
						                  <a href="<?php echo $base_file_name_base;?>_control.php?action=state&nns_state=0&nns_id=<?php echo $item["nns_id"];?>">未使用</a>
						                  <a href="<?php echo $base_file_name_base;?>_control.php?action=state&nns_state=2&nns_id=<?php echo $item["nns_id"];?>">已补全</a>
						                  <?php 
						              }
						              else
						              {
						             ?>
						                  <a href="<?php echo $base_file_name_base;?>_control.php?action=state&nns_state=0&nns_id=<?php echo $item["nns_id"];?>">未使用</a>
						                  <a href="<?php echo $base_file_name_base;?>_control.php?action=state&nns_state=1&nns_id=<?php echo $item["nns_id"];?>">未补全</a>
						             <?php 
						              }
						          ?>
                                  </td>
                        </tr>
						<?php
						     }
						}
						$least_num = $g_manager_list_max_num - count($result_data['data_info']);
						for ($i = 0; $i < $least_num; $i++) 
						{
						?>
						  <tr>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						  </tr>
						<?php 
						}
						?>
					</tbody>
				</table>
			</div>
		    <!-- 分页DIV -->
		    <div class="pagecontrol">
		             共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                <?php if ($currentpage > 1) { ?>
                        <a href="<?php $base_file_name; ?>?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php $base_file_name; ?>?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } else { ?>
                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } if ($currentpage < $vod_pages) { ?>
                        <a href="<?php $base_file_name; ?>?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php $base_file_name; ?>?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } else { ?>
                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } ?>
    
                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                <a href="javascript:go_page_num('<?php $base_file_name; ?>?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                <input name="nns_list_max_num" id="nns_list_max_num" type="text" value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>" onclick="refresh_vod_page();"/>&nbsp;&nbsp;
            </div>    
            <div class="controlbtns">
                <div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
                <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
                <div class="controlbtn add"><a href="<?php echo $base_file_name_base;?>_addedit.php?action=add">添加</a></div>
                <div class="controlbtn delete"><a href="javascript:delete_id();">删除</a></div>
                <div class="controlbtn online"><a href="javascript:reset();">重置次数</a></div>
                <div class="controlbtn fenji"><a href="javascript:state(0);">未使用</a></div>
                <div class="controlbtn fenji"><a href="javascript:state(1);">未补全</a></div>
                <div class="controlbtn fenji"><a href="javascript:state(2);">已补全</a></div>
                <div style="clear:both;">
            </div>
        </div>    
                        
		</div>
	</body>
</html>
