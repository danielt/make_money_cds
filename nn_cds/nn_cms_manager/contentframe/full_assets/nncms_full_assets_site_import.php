<?php
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();

include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
    "db_policy" => NL_DB_WRITE,
    "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -7);
$edit_data = $_REQUEST;
$action='import';

$timestamp = time();
$token = md5('unique_salt' . $timestamp);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
        <link href="../../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
        <link href="../../css/uploadify.css" rel="stylesheet" type="text/css" />
        <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
        <script language="javascript" src="../../js/table.js.php"></script>
        <script language="javascript" src="../../js/radiolist.js"></script>
        <script language="javascript" src="../../js/checkinput.js.php"></script>
        <script language="javascript" src="../../js/checkinput.js.php"></script>
        <script language="javascript" src="../../js/swfobject.js"></script>
        <script language="javascript" src="../../js/muti_upload/jquery.uploadify.min.js"></script>
    </head>
    <body>
        <div class="content" style="background:#fff;">
        	<div class="content_position">站点文件导入</div>
        	<form id="add_form" action="<?php echo $base_file_name_base;?>_control.php" method="post" enctype="multipart/form-data">
            	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
            	<input name="timestamp" id="timestamp" type="hidden" value="<?php echo $timestamp;?>" />
            	<input name="token" id="token" type="hidden" value="<?php echo $token;?>" />
            	<div class="content_table">
            		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            		  <tbody class="tr_tbody">
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>站点数据文件<br>(<font color="red">文件格式为CSV文件</font>):</td>
            				<td>
            				    <input name="vod_data_file" id="vod_data_file" type="file" size="60%" />
            				</td>
            			</tr>
            		  </tbody>
            		</table>
            	</div>
        	</form>
            <div class="controlbtns">
        		<div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','确认导入文件？',$('#add_form'),'<?php echo $action; ?>');">文件上传</a></div>
        		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        		<div style="clear:both;"></div>
        	</div>
        </div>
    </body>
</html>
