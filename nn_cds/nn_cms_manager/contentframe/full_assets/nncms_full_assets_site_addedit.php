<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$action=$_GET["action"];
$nns_id=$_REQUEST["nns_id"];
$pri_bool=false;
if (!empty($action)){
	switch($action){
	case "edit":
		$action_str=cms_get_lang('save');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136002");
		break;
	case "add":
		$action_str=cms_get_lang('add');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136003");
		break;
	default:
		break;
	}
}
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/full_assets/full_assets_site.class.php';
$dc = nl_get_dc(array (
    "db_policy" => NL_DB_WRITE,
    "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);
$edit_data = $_REQUEST;

if ($action=="edit")
{
    $edit_data = nl_full_assets_site::query_by_id($dc,$nns_id);

    if($edit_data['ret'] != 0 )
    {
        echo "<script>alert('". $action_str. $edit_data['reason']. "');</script>";
        echo "<script>self.location='".$base_file_name_base."_list.php;</script>";
    }
    $edit_data = $edit_data['data_info'];
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link href="../../css/comm.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
    </head>
    <body>
        <div class="content">
        	<div class="content_position">全量媒资站点 > 添加/修改</div>
        	<form id="add_form" action="<?php echo $base_file_name_base;?>_control.php" method="post">
            	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
            	<div class="content_table">
            		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            		  <tbody class="tr_tbody">
            		    <?php 
            		    if($action == 'edit'){
            		    ?>
                		    <tr>
                				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>GUID:</td>
                				<td>
                				    <input name="nns_id" id="nns_id" type="hidden"  value="<?php if(isset($edit_data["nns_id"])){echo $edit_data["nns_id"];}?>" rule="noempty"/>
                				    <input type="text" style="width:540px;"  disabled value="<?php if(isset($edit_data["nns_id"])){echo $edit_data["nns_id"];}?>" />
                				</td>
                			</tr>
            		    <?php }?>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>站点ID:</td>
            				<td><input style="width:540px;" name="nns_siteid" id="nns_siteid" type="text" value="<?php if(isset($edit_data["nns_siteid"])){echo $edit_data["nns_siteid"];}?>" rule="noempty"  /></td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>站点名称:</td>
            				<td><input style="width:540px;" name="nns_name" id="nns_name" type="text" value="<?php if(isset($edit_data["nns_name"])){echo $edit_data["nns_name"];}?>" rule="noempty"  /></td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>状态:</td>
            				<td>
            				    <select name='nns_state' style="width:550px;" >
                                    <option value="0" <?php if(isset($edit_data['nns_state']) && $edit_data['nns_state'] == '0'){ echo 'selected="selected"';}?>>启用</option>
                                    <option value="1" <?php if(isset($edit_data['nns_state']) && $edit_data['nns_state'] == '1'){ echo 'selected="selected"';}?>>禁用</option>
                                </select>
            				</td>
            			</tr>
            		  </tbody>
            		</table>
            	</div>
        	</form>
        	<div class="controlbtns">
        		<div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','<?php echo cms_get_lang('webkz_qrccxg'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
        		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        		<div style="clear:both;"></div>
        	</div>
        </div>
    </body>
</html>
