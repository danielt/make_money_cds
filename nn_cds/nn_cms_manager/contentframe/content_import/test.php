<?php
/**
 * Created by IntelliJ IDEA.
 * User: LZ
 * Date: 2018/12/11
 * Time: 11:48
 */
session_start();
if(empty($_SESSION) || !isset($_SESSION['nns_org_id']))
{
    die('请登录平台后操作！');
}
$org_id = $_SESSION['nns_org_id'];
$timestamp = time();
$token = md5('unique_salt' . $timestamp);
$arr_type = array(
    'video_import' => '主媒资',
    'index_import' => '分集',
    'media_import' => '片源',
);
$arr_queue_name = array(
    'core_import' => '中心同步队列',
    'c2_import' => 'C2队列',
);
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
<form action="./nncms_content_stock_import_by_csv_control.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="nns_org_id" value="<?php echo $org_id; ?>"/>
    <input type="hidden" name="nns_org_type" value="0"/>
    <input type="hidden" name="timestamp" value="<?php echo $timestamp;?>"/>
    <input type="hidden" name="token" value="<?php echo $token;?>"/>
    <table>
        <tr>
            <td>CPID:</td>
            <td><input type="text" name="nns_cp_id" /></td>
        </tr>
        <tr>
            <td width="120">导入媒资类型:</td>
            <td>
                <select name='nns_type'>
                    <option value=''>--请选择媒资类型--</option>
                    <?php foreach ($arr_type as $key=>$val){?>
                        <option value="<?php echo $key;?>"><?php echo $val;?></option>
                    <?php }?>
                </select>
            </td>
        </tr>
        <tr>
            <td width="120">队列名:</td>
            <td>
                <select name='nns_queue_name'>
                    <option value=''>--请选择队列--</option>
                    <?php foreach ($arr_queue_name as $q_key=>$q_val){?>
                        <option value="<?php echo $q_key;?>"><?php echo $q_val;?></option>
                    <?php }?>
                </select>&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">未选择队列时将不会生成注入队列</font>
            </td>
        </tr>
        <tr>
            <td>CSV文件</td>
            <td><input type="file" name="Filedata"/></td>
        </tr>
        <tr><td><input type="submit" name="提交"/></td></tr>
    </table>
</form>
</body>
</html>
