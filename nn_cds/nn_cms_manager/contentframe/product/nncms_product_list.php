<?php 
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -5);
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
		"db_policy" => NL_DB_WRITE,
		"cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));


/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/product/product_standard.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';

$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : (!empty($_COOKIE["page_max_num"]) ?  $_COOKIE["page_max_num"] : 18);
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) {
	$page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit_array = array(
		'page_size'=>$page_size,
		'page_num'=> $page_num,
);
if (isset($_REQUEST['nns_id']) && (strlen($_REQUEST['nns_id']) >0))
{
    $filter_array['where']['nns_id'] = $_REQUEST['nns_id'];
}
if (isset($_REQUEST['nns_cp_id']) && (strlen($_REQUEST['nns_cp_id']) >0))
{
    $filter_array['like']['nns_cp_id'] = $_REQUEST['nns_cp_id'];
}
if (isset($_REQUEST['nns_order_number']) && (strlen($_REQUEST['nns_order_number']) >0))
{
    $filter_array['like']['nns_order_number'] = $_REQUEST['nns_order_number'];
}
if (isset($_REQUEST['nns_order_name']) && (strlen($_REQUEST['nns_order_name']) >0))
{
    $filter_array['like']['nns_order_name'] = $_REQUEST['nns_order_name'];
}
if (isset($_REQUEST['nns_order_type']) && (strlen($_REQUEST['nns_order_type']) >0))
{
    $filter_array['where']['nns_order_type'] = $_REQUEST['nns_order_type'];
}
if (isset($_REQUEST['nns_order_desc']) && (strlen($_REQUEST['nns_order_desc']) >0))
{
    $filter_array['like']['nns_order_desc'] = $_REQUEST['nns_order_desc'];
}
$result_data = nl_product_standard::query($dc,$filter_array,$limit_array);
// var_dump($result_data);
if($result_data['ret'] != 0)
{
	echo '<script>alert("'.$result_data['reason'].'");history.go(-1);</script>';die;
}
$vod_total_num = isset($result_data['page_info']['total_count']) ? $result_data['page_info']['total_count'] : 0;
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) {
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;

$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);
// $data = $result['data'];
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link href="../../css/public.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
		<script language="javascript">
			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
                setCookie("page_max_num",num);
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				})
				return BoxKey;
			}
			function delete_id(){
				var r=confirm("是否进行批量删除操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "<?php echo $base_file_name_base;?>_control.php?action=delete&nns_id=&nns_id="+ids;
						window.location.href = url;
					}
				}
			}
		</script>
	</head>
	<body>
		<div class="content">
		    <!-- 栏目导航描述div -->
			<div class="content_position">内容提供商列表</div>
			<!-- 表单查询div -->
			<div class="content_table">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<form action="<?php echo $base_file_name;?>" method="get">
					    <input type="hidden" name="view_list_max_num" id="view_list_max_num" value="<?php echo $g_manager_list_max_num; ?>" style="width:120px;">
						<tbody>
							<tr>
								<td>
									CP名称:
									<input type="text" name="nns_cp_id" id="nns_cp_id" value="<?php echo $_GET['nns_cp_id']; ?>" style="width:120px;">
                                    &nbsp;&nbsp;产品类型:
                                        <select name='nns_order_type' style="width:150px;">
                                            <option value=''>全部</option>
                                            <option value="0" <?php if(isset($_GET['nns_order_type']) && $_GET['nns_order_type'] == 0 && strlen($_GET['nns_order_type']) > 0){ echo 'selected="selected"';}?>>接收CP/SP所有</option>
                                            <option value="1" <?php if(isset($_GET['nns_order_type']) && $_GET['nns_order_type'] == 1 && strlen($_GET['nns_order_type']) > 0){ echo 'selected="selected"';}?>>融合后产品包</option>
                                        </select>
                                    &nbsp;&nbsp;产品编号:
                                        <input type="text" name="nns_order_number" id="nns_order_number" value="<?php echo $_GET['nns_order_number']; ?>" style="width:120px;">
                                    &nbsp;&nbsp;产品名称:
                                        <input type="text" name="nns_order_name" id="nns_order_name" value="<?php echo $_GET['nns_order_name']; ?>" style="width:120px;">
                                    &nbsp;&nbsp;产品描述:
                                	    <input type="text" name="nns_order_desc" id="nns_order_desc" value="<?php echo $_GET['nns_order_desc']; ?>" style="width:150px;">
									&nbsp;&nbsp;<input type="submit" value="<?php echo cms_get_lang('search'); ?>"  />										
								</td>
							</tr>
						</tbody>
					</form>
				</table>
			</div>			
			<!-- 内容列表 DIV -->
			<div class="content_table formtable">
				<table width="107%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th><input name="" type="checkbox" value="">序号</th>
							<th>CP名称</th>
							<th>产品编号</th>
							<th>产品名称</th>
							<th>类型</th>
							<th>价格</th>
							<th>描述</th>
							<th>创建时间</th>
                            <th>修改时间</th>
                            <th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(is_array($result_data['data_info']) && !empty($result_data['data_info']))
						{
							$num = ($page_num - 1) * $page_size;
							foreach ($result_data['data_info'] as $item) 
							{
								$num++;
						?>
						      <tr>
						          <td><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
						          <td><?php echo $item['nns_cp_id']; ?></td>
						          <td><?php echo $item['nns_order_number']; ?></td>
                                  <td><?php echo $item['nns_order_name']; ?></td>
                                  <td><?php
                                        if($item['nns_order_type'] == 0)
                                        {
                                            echo "接收CP/SP所有";
                                        }
                                        else if($item['nns_order_type'] == 1)
                                        {
                                            echo "融合后产品包";
                                        }
                                        else
                                        {
                                            echo "未知";
                                        }
                                        ?></td>
                                  <td><?php echo $item['nns_order_price']; ?></td>
                                  <td><?php echo $item['nns_order_desc']; ?></td>
						          <td><?php echo $item['nns_create_time']; ?></td>
						          <td><?php echo $item['nns_modify_time']; ?></td>
						          <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                                	<a href="<?php echo $base_file_name_base;?>_addedit.php?action=edit&nns_id=<?php echo $item["nns_id"];?>">修改</a>
                                	<a href="../product_vod_bind/nncms_product_vod_bind_list.php?&nns_id=<?php echo $item["nns_id"];?>&sp_id=starcor">绑定媒资</a>
                                	<a href="<?php echo $base_file_name_base;?>_control.php?action=delete&nns_id=<?php echo $item["nns_id"];?>">删除</a>
                                  </td>
                        </tr>
						<?php
						     }
						}
						$least_num = $g_manager_list_max_num - count($result_data['data_info']);
						for ($i = 0; $i < $least_num; $i++) 
						{
						?>
						  <tr>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						      <td>&nbsp;</td>
						  </tr>
						<?php 
						}
						?>
					</tbody>
				</table>
			</div>
		    <!-- 分页DIV -->
		    <div class="pagecontrol">
		             共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                <?php if ($currentpage > 1) { ?>
                        <a href="<?php $base_file_name; ?>?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php $base_file_name; ?>?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } else { ?>
                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                        <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } if ($currentpage < $vod_pages) { ?>
                        <a href="<?php $base_file_name; ?>?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php $base_file_name; ?>?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } else { ?>
                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                        <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } ?>
    
                <?php echo cms_get_lang('jump_to'); ?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                <a href="javascript:go_page_num('<?php $base_file_name; ?>?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                <input name="nns_list_max_num" id="nns_list_max_num" type="text" value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
                <input type="button" value="<?php echo cms_get_lang('confirm'); ?>" onclick="refresh_vod_page();"/>&nbsp;&nbsp;
            </div>    
            <div class="controlbtns">
                <div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
                <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
                <div class="controlbtn add"><a href="<?php echo $base_file_name_base;?>_addedit.php?action=add">添加</a></div>
                <div class="controlbtn delete"><a href="javascript:delete_id();">删除</a></div>
                <div style="clear:both;">
            </div>
        </div>    
                        
		</div>
	</body>
</html>
