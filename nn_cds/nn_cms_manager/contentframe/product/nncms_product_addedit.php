<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$action=$_GET["action"];
$nns_id=$_REQUEST["nns_id"];
$pri_bool=false;
if (!empty($action)){
	switch($action){
	case "edit":
		$action_str=cms_get_lang('save');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136002");
		break;
	case "add":
		$action_str=cms_get_lang('add');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136003");
		break;
	default:
		break;
	}
}
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';

$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);
$edit_data = $_REQUEST;






/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/product/product_standard.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';
$dc = nl_get_dc(array (
    "db_policy" => NL_DB_WRITE,
    "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));
if ($action=="edit"){
	$edit_data = nl_product_standard::query_by_id($dc,$nns_id);
	
	if($edit_data['ret'] != 0 )
	{
		echo "<script>alert('". $action_str. $edit_data['reason']. "');</script>";
		echo "<script>self.location='".$base_file_name_base."_list.php;</script>";
	}
	$edit_data = $edit_data['data_info'];
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link href="../../css/comm.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
        <script language="javascript">
        
        </script>
    </head>
    <body>
        <div class="content">
        	<div class="content_position">内容提供商 > 内容提供商 添加/修改</div>
        	<form id="add_form" action="<?php echo $base_file_name_base;?>_control.php" method="post">
            	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
            	<div class="content_table">
            		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            		  <tbody>
            		    <?php if($action == 'edit'){
            		    ?>
                		    <tr>
                				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>GUID:</td>
                				<td>
                				    <input name="nns_id" id="nns_id" type="hidden"  value="<?php if(isset($edit_data["nns_id"])){echo $edit_data["nns_id"];}?>" rule="noempty"/>
                				    <input type="text" disabled value="<?php if(isset($edit_data["nns_id"])){echo $edit_data["nns_id"];}?>" />
                				</td>
                			</tr>
                	   <?php }?>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>CP:</td>
            				<td>
								<input name="nns_cp_id" id="nns_cp_id" type="text"  value="<?php if(isset($edit_data["nns_cp_id"])){echo $edit_data["nns_cp_id"];}else{echo 'starcor';} ?>" rule="noempty" />

            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>产品编号:</td>
            				<td>
								<input name="nns_order_number" id="nns_order_number" type="text"  value="<?php if(isset($edit_data["nns_order_number"])){echo $edit_data["nns_order_number"];}?>" rule="noempty" />

            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>产品名称:</td>
            				<td>
            				    <input name="nns_order_name" id="nns_order_name" type="text"  value="<?php if(isset($edit_data["nns_order_name"])){echo $edit_data["nns_order_name"];}?>" rule="noempty" />
            				</td>
        			    </tr>
        			    <tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>产品类型:</td>
            				<td>
								<select name='nns_order_type' style="width:150px;" rule="noempty">
									<option value=''>--请选择--</option>
									<option value= '0' <?php if(isset($edit_data['nns_order_type']) && $edit_data['nns_order_type'] == 0){ echo 'selected="selected"';}?>>接收CP/SP所有</option>
									<option value='1' <?php if(isset($edit_data['nns_order_type']) && $edit_data['nns_order_type'] == 1){ echo 'selected="selected"';}?>>融合后产品包</option>
								</select>            				</td>
        			    </tr>
            			<tr>
							<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>产品价格:</td>
            				<td><input name="nns_order_price" id="nns_order_price" type="text" value="<?php if(isset($edit_data["nns_order_price"])){ echo $edit_data["nns_order_price"];}else{ echo 0;}?>"/>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle">描述</td>
            				<td><textarea name="nns_order_desc" id="nns_order_desc" cols="" rows=""><?php if(isset($edit_data["nns_order_desc"])){ echo $edit_data["nns_order_desc"];}else{ echo '';}?></textarea>
            				</td>
            			</tr>
            		  </tbody>
            		</table>
            	</div>
        	</form>
        	<div class="controlbtns">
        		<div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','<?php echo cms_get_lang('webkz_qrccxg'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
        		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        		<div style="clear:both;"></div>
        	</div>
        </div>
    </body>
</html>
