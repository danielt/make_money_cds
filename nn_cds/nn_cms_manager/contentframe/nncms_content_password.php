<?php
header("Content-Type:text/html; charset=UTF-8");
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

$pass_bool=false;
if (isset($_SESSION["nns_mgr_id"])){
	if (!empty($_SESSION["nns_mgr_id"])) $pass_bool=true;
}
if ($pass_bool){
	require_once $nncms_db_path. "nns_log".DIRECTORY_SEPARATOR . "nns_db_op_log_class.php";
	$log_inst=new nns_db_op_log_class();
	$old_pass=$_POST["manager_pass"];
	$new_pass=$_POST["manager_new_pass"];
	if ($old_pass){
		include $nncms_db_path. "nns_manager/nns_db_manager_class.php";
		$manager_inst=new nns_db_manager_class();
		$result=$manager_inst->nns_db_manager_modify_pwd($_SESSION["nns_mgr_id"],$old_pass,$new_pass);
		$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],"edit",$_SESSION["nns_mgr_name"].$language_manager_xtgl_xgmm,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
		$log_inst=null;
		$manager_inst=null;
		if ($result["ret"]==0){
			echo "<script>alert('". cms_get_lang('xtgl_xgmm|success'). "');</script>";	
		}else{
			echo "<script>alert('". cms_get_lang('xtgl_xgmm|fault'). "');</script>";
		}
	}	
}

//var_dump($languages);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/table.js.php"></script>
<script language="javascript" src="../js/checkinput.js.php"></script>
<script language="javascript">
function checkForm(formobj){
	_formobj=formobj
	if (checkInputRules())
	window.parent.cmsalert.showAlert("<?php echo cms_get_lang('xtgl_xgmm');?>","<?php echo cms_get_lang('msg_ask_change');?>",submitForm);
	
}
</script>
</head>

<body>
<div class="content">
	<div class="content_position"><?php echo cms_get_lang('xtgl'),' > ',cms_get_lang('xtgl_xgmm');?></div>
	<form action="nncms_content_password.php" method="post" id="globalform">
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">

		<tbody>
		   
		  <tr>
				<td width="120"><?php echo cms_get_lang('oldpass');?> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
				<td><input name="manager_pass" id="manager_pass" type="password" value="" rule="noempty"/>
				</td>
		  </tr>
		   <tr>
				<td width="120"><?php echo cms_get_lang('newpass');?> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
				<td><input name="manager_new_pass" id="manager_new_pass" type="password" value="" rule="noempty"/>
				</td>
		  </tr>
		  <tr>
				<td width="120"><?php echo cms_get_lang('pass_confirm');?> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
				<td><input name="manager_new_pass_confirm" id="manager_new_pass_confirm" type="password" value="" rule="noempty" sameto="manager_new_pass"/>
				</td>
		  </tr>
		  </tbody>
		</table>
	</div>
	</form>
	<div class="controlbtns">
		<div class="controlbtn sure"><a href="javascript:checkForm($('#globalform'));"><?php echo cms_get_lang('confirm');?></a></div>
		<div class="controlbtn back"><a href="javascript:resetForm($('#globalform'));"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>
</body>
</html>
