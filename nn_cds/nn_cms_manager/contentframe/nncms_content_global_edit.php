<?php
header("Content-Type:text/html; charset=UTF-8");
include("../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include ($nncms_config_path. "nn_cms_config/nn_cms_config_path.php");
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";

$checkpri=new nns_db_pri_class();
if (!$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"100100")){
	Header("Location: nncms_content_wrong.php");
}
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
$checkpri=null;
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/table.js.php"></script>
<script language="javascript" src="../js/checkinput.js.php"></script>
<script type="text/javascript" charset="utf-8" src="../js/question_mark.js" ></script>
<script language="javascript">
function checkForm(formobj){
	_formobj=formobj;
	if (checkInputRules()){
		window.parent.cmsalert.showAlert("<?php echo cms_get_lang('xtgl_cmsgl');?>","<?php echo cms_get_lang('msg_ask_change');?>",submitForm);
	}
}
//window.onload=alt_tr_bg;
</script>
</head>

<body>
<div class="content">
	<div class="content_position"><?php echo cms_get_lang('xtgl');?> > <?php echo cms_get_lang('xtgl_cmsgl');?></div>
	<form action="nncms_content_global.php" method="post" id="globalform">
	<input name="action" type="hidden" value="edit"/>
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
		    
		    <tr style="display:none;">
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>全局web地址(老项目使用):</td>
				<td><input name="g_webdir" id="g_webdir" type="text" value="<?php echo $g_webdir;?>" rule="noempty"/></td>
			</tr>
			
			<tr style="display:none;">
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>缓存开关(0关闭|1开启):</td>
				<td><input name="g_cache_enabled" id="g_cache_enabled" type="text" value="<?php echo $g_cache_enabled;?>" rule="noempty"/></td>
			</tr>
			<tr style="display:none;">
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>播放日志存储天数:</td>
				<td><input name="g_play_log_limit" id="g_play_log_limit" type="text" value="<?php echo $g_play_log_limit;?>" rule="noempty"/></td>
			</tr>
			<tr>
				<td colspan="2" style="padding:0px;">
					<div class="radiolist">
						<div class="radiogroup">
							<h3>播控后台配置</h3>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>项目全局中文名称:</td>
				<td><input name="g_prject_ch_name" id="g_prject_ch_name" type="text" value="<?php echo strlen($g_prject_ch_name) <1 ? "播控子平台-媒资注入平台" : $g_prject_ch_name;?>" rule="noempty"/></td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>播控版本切换:</td>
				<td>
				    <input type="radio" name="g_bk_version_number" value="1" id="g_bk_version_number" <?php if ($g_bk_version_number==1) {echo "checked";}?>/><?php echo "播控V2版本";?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>融合平台开关:</td>
				<td>
				    <input type="radio" name="g_fuse_enabled" value="1" id="g_fuse_enabled" <?php if ($g_fuse_enabled==1) {echo "checked";}?>/><?php echo cms_get_lang('kq');?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" name="g_fuse_enabled" value="0" id="g_fuse_enabled" <?php if ($g_fuse_enabled==0) {echo "checked";}?>/><?php echo cms_get_lang('bkq');?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>介质库平台开关:</td>
				<td>
				    <input type="radio" name="g_medium_enabled" value="1" id="g_medium_enabled" <?php if ($g_medium_enabled==1) {echo "checked";}?>/><?php echo cms_get_lang('kq');?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" name="g_medium_enabled" value="0" id="g_medium_enabled" <?php if ($g_medium_enabled==0) {echo "checked";}?>/><?php echo cms_get_lang('bkq');?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
			</tr>
			<tr>
				<td class="rightstyle">PHP环境执行地址:<br>linux:[/usr/local/php/bin/php]<br/>windows:[E:\php\php.exe]</td>
				<td><input name="g_evn_operation_bin" id="g_evn_operation_bin" type="text" value="<?php echo $g_evn_operation_bin;?>" rule=""/></td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>全局项目ID:</td>
				<td><input name="g_project_name" id="g_project_name" type="text" value="<?php echo $g_project_name;?>" rule="noempty"/></td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>后台基本地址(数据库配置文件父级目录):</td>
				<td><input name="g_bk_web_url" id="g_bk_web_url" type="text" value="<?php echo $g_bk_web_url;?>" rule="noempty"/></td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>后台日志地址(/nn_mgtvbk/data/):</td>
				<td><input name="g_bk_log_web_url" id="g_bk_log_web_url" type="text" value="<?php echo $g_bk_log_web_url;?>" rule="noempty"/></td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>后台默认数据展示条数:</td>
				<td><input name="g_manager_list_max_num" id="g_manager_list_max_num" type="text" value="<?php echo $g_manager_list_max_num;?>" rule="noempty|uint"/></td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>memcache缓存开关:</td>
				<td>
				    <input type="radio" name="g_mem_cache_enabled" value="1" id="g_mem_cache_enabled" <?php if ($g_mem_cache_enabled==1) {echo "checked";}?>/><?php echo cms_get_lang('kq');?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" name="g_mem_cache_enabled" value="0" id="g_mem_cache_enabled" <?php if ($g_mem_cache_enabled==0) {echo "checked";}?>/><?php echo cms_get_lang('bkq');?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>媒资审核开关:</td>
				<td>
				    <input type="radio" name="g_video_audit_enabled" value="1" id="g_video_audit_enabled" <?php if ($g_video_audit_enabled==1) {echo "checked";}?>/><?php echo cms_get_lang('kq');?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" name="g_video_audit_enabled" value="0" id="g_video_audit_enabled" <?php if ($g_video_audit_enabled==0) {echo "checked";}?>/><?php echo cms_get_lang('bkq');?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    </td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>登录验证码开关:</td>
				<td>
				    <input type="radio" name="g_login_code_enabled" value="1" id="g_login_code_enabled" <?php if ($g_login_code_enabled==1) {echo "checked";}?>/><?php echo cms_get_lang('kq');?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" name="g_login_code_enabled" value="0" id="g_login_code_enabled" <?php if ($g_login_code_enabled==0) {echo "checked";}?>/><?php echo cms_get_lang('bkq');?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    </td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>媒资信息补全开关:</td>
				<td>
				    <input type="radio" name="g_assets_info_completion_enabled" value="1" id="g_assets_info_completion_enabled" <?php if ($g_assets_info_completion_enabled==1) {echo "checked";}?>/><?php echo cms_get_lang('kq');?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" name="g_assets_info_completion_enabled" value="0" id="g_assets_info_completion_enabled" <?php if ($g_assets_info_completion_enabled!=1) {echo "checked";}?>/><?php echo cms_get_lang('bkq');?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    </td>
			</tr>
			<tr>
				<td class="rightstyle">媒资信息补全接口:</td>
				<td><input name="g_assets_info_completion_url" id="g_assets_info_completion_url" type="text" value="<?php echo $g_assets_info_completion_url;?>"/></td>
			</tr>
			<tr>
				<td class="rightstyle">媒资信息补全结果MEM缓存时间（秒）:</td>
				<td><input name="g_assets_info_completion_time" id="g_assets_info_completion_time" type="text" value="<?php echo $g_assets_info_completion_time;?>"/></td>
			</tr>
			<tr>
				<td colspan="2" style="padding:0px;">
					<div class="radiolist">
						<div class="radiogroup">
							<h3>MSP配置</h3>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="rightstyle">请求地址:</td>
				<td><input name="g_core_url" id="g_core_url" type="text" value="<?php echo $g_core_url;?>" /></td>
			</tr>
            <tr>
				<td class="rightstyle">账号:</td>
				<td><input name="g_core_userid" id="g_core_userid" type="text" value="<?php echo $g_core_userid;?>" /></td>
			</tr>
			<tr>
				<td class="rightstyle">密码:</td>
				<td><input name="g_core_password" id="g_core_password" type="text" value="<?php echo $g_core_password;?>" /></td>
			</tr>
			<tr>
				<td class="rightstyle">播放地址:</td>
				<td><input name="g_core_npss_url" id="g_core_npss_url" type="text" value="<?php echo $g_core_npss_url;?>" /></td>
			</tr>
			<tr>
				<td colspan="2" style="padding:0px;">
					<div class="radiolist">
						<div class="radiogroup">
							<h3>转码配置</h3>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="rightstyle">转出统一片源后缀(未配置默认ts):</td>
				<td>
				    <select name="g_encode_file_ex_type">
				        <option value="ts" <?php if ($g_encode_file_ex_type=='ts') {echo "selected";}?>>ts</option>
				        <option value="mp4" <?php if ($g_encode_file_ex_type=='mp4') {echo "selected";}?>>mp4</option>
				        <option value="m3u8" <?php if ($g_encode_file_ex_type=='m3u8') {echo "selected";}?>>m3u8</option>
				        <option value="flv" <?php if ($g_encode_file_ex_type=='flv') {echo "selected";}?>>flv</option>
				        <option value="avi" <?php if ($g_encode_file_ex_type=='avi') {echo "selected";}?>>avi</option>
				    </select>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding:0px;">
					<div class="radiolist">
						<div class="radiogroup">
							<h3>测试桩配置</h3>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="rightstyle">SOAP模拟ftp相对地址:</td>
				<td><input name="g_soap_test_ftp_url" id="g_soap_test_ftp_url" type="text" value="<?php echo $g_soap_test_ftp_url;?>" /></td>
			</tr>
			<tr>
				<td colspan="2" style="padding:0px;">
					<div class="radiolist">
						<div class="radiogroup">
							<h3>DEBUG配置</h3>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="rightstyle">SQL语句收集开关:</td>
				<td>
				    <input type="radio" name="g_debug_sql_enable" value="1" id="g_debug_sql_enable" <?php if ($g_debug_sql_enable==1) {echo "checked";}?>/><?php echo cms_get_lang('kq');?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" name="g_debug_sql_enable" value="0" id="g_debug_sql_enable" <?php if ($g_debug_sql_enable==0) {echo "checked";}?>/><?php echo cms_get_lang('bkq');?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
			</tr>
			<tr>
				<td class="rightstyle">SQL语句收集执行大于时间<br>(如果为0全部收集 单位/秒):</td>
				<td>
				    <input name="g_debug_sql_ex_second" id="g_debug_sql_ex_second" type="text" value="<?php echo $g_debug_sql_ex_second;?>" />
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding:0px;">
					<div class="radiolist">
						<div class="radiogroup">
							<h3>自定义配置</h3>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="rightstyle">选择DB:</td>
				<td>
				    <select name="g_db_configer_enable">
				        <option value="localhost" <?php if ($g_encode_file_ex_type=='localhost') {echo "selected";}?>>本地数据库</option>
				        <option value="jscn_test" <?php if ($g_encode_file_ex_type=='jscn_test') {echo "selected";}?>>江苏有线内网测试数据库</option>
				        <option value="jscn_official" <?php if ($g_encode_file_ex_type=='jscn_official') {echo "selected";}?>>江苏有线内网正式数据库</option>
				    </select>
				</td>
			</tr>
		</tbody>
		</table>
	</div>
	</form>
	<div class="controlbtns">
		<div class="controlbtn sure"><a href="javascript:checkForm($('#globalform'));"><?php echo cms_get_lang('confirm');?></a></div><span class="btn_split"></span>
		<div class="controlbtn back"><a href="javascript:history.go(-1)"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>
</body>
</html>
