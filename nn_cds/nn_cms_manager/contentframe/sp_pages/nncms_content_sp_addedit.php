<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$action=$_GET["action"];
$nns_id=$_GET["nns_id"];
$pri_bool=false;
if (!empty($action)){
	switch($action){
	case "edit":
		$action_str=cms_get_lang('save');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136002");
		break;
	case "add":
		$action_str=cms_get_lang('add');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136003");
		break;
	default:
		break;
	}
}
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");

//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");

if ($action=="edit"){
	include_once dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv/models/sp_model.php';
	$edit_data = sp_model::get_sp_info($nns_id);
	//	$edit_pri=$edit_data["nns_pri_id"];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/radiolist.js"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript">

</script>
</head>

<body>



<div class="content">
	<div class="content_position">SP 管理 > SP 添加/修改</div>
	<form id="add_form" action="nncms_content_sp_control.php" method="post">
	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
            <tr>
                <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span><?php echo
                cms_get_lang('id');?>:</td>
                <td><input type="text" name="nns_id" rule="noempty" value="<?php echo $nns_id;?>" <?php echo $action ==
                'edit' ? 'readonly="readonly"' : '';?> /></td>
            </tr>
			  <tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>SP名称:</td>
				<td><input name="nns_name" id="nns_name" type="text" rule="noempty" value="<?php if($action=="edit"){echo $edit_data["nns_name"];}?>" /></td>
			</tr>
			
			<tr>
				<td class="rightstyle">联系人:</td>
				<td><input name="nns_contact" id="nns_contact" type="text"  value="<?php if($action=="edit"){echo $edit_data["nns_contact"];}?>" /></td>
			</tr>
			
			<tr>
				<td class="rightstyle">联系电话:</td>
				<td><input name="nns_telphone" id="nns_telphone" type="text" value="<?php if($action=="edit"){echo $edit_data["nns_telphone"];}?>" /></td>
			</tr>
			
			<tr>
				<td class="rightstyle">Email:</td>
				<td><input name="nns_email" id="nns_email" type="text"  value="<?php if($action=="edit"){echo $edit_data["nns_email"];}?>" /></td>
			</tr>			
			
			<tr>
				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>接口权限:</td>
				<td>
					<input type="checkbox" name="nns_rule[]" value="100000"   <?php if($action=="edit" && strstr($edit_data["nns_rule"], '100000')){?> checked="checked" <?php } ?> />是否需要切片
				</td>
			</tr>
			
		  </tbody>
		</table>
	</div>
	</form>
	<div class="controlbtns">
		<div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','<?php echo cms_get_lang('webkz_qrccxg'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>


</body>
</html>
