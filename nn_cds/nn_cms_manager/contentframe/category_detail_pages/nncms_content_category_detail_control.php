<?php
/*
 * Created on 2012-3-2
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 @header("Content-Type:text/html;charset=utf-8");
 include("../../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";


 $action=$_POST["action"];
 $nns_id=$_POST["nns_id"];



 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
if (!empty($action)){
	switch($action){
		case "edit":
		$action_str=cms_get_lang('edit|lmgl_lmmb');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"105101");
		break;
		case "add":
		$action_str=cms_get_lang('add|lmgl_lmmb');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"105100");
		break;
		case "delete":
		$action_str=cms_get_lang('delete|lmgl_lmmb');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"105102");
		break;
		default:
		break;
	}
}

$checkpri=null;

 $category_type=$_POST["category_type"];
 $category_org_id="";
 if ($_SESSION["nns_manager_type"]!=0){
	$category_org_id=$_SESSION["nns_org_id"];
	if ($category_type!=$_SESSION["nns_manager_type"]){
		$pri_bool=false;
	}
 }

if (!$pri_bool){
	 Header("Location: ../nncms_content_wrong.php");
}else{
	$category_list=$_POST["category_list"];
	$category_parent=$_POST["category_parent"];
	$nns_category_detail_name=$_POST["nns_category_detail_name"];
	$nns_category_detail_id=$_POST["nns_category_detail_id"];
	include $nncms_db_path. "nns_category/nns_db_category_detail_class.php";
	$category_inst=new nns_db_category_detail_class();
	switch($action){
		case "edit":
		$result=$category_inst->nns_db_category_detail_modify($nns_id,$nns_category_detail_name,0,$category_list,$category_parent);
		if ($result["ret"]!=0){
			echo "<script>alert('". $action_str, cms_get_lang('fault'), "');</script>";
		}else{
			echo "<script>alert('". $action_str, cms_get_lang('success'), "');</script>";
		}
		break;
		case "add":
		$result=$category_inst->nns_db_category_detail_add($nns_category_detail_id,$nns_category_detail_name,0,$category_list,$category_parent);
		if ($result["ret"]!=0){
			echo $result["reason"];
			echo "<script>alert('". $action_str, cms_get_lang('fault'), "');</script>";
		}else{
			echo "<script>alert('". $action_str, cms_get_lang('success'), "');</script>";
		}
		break;
		case "delete":
		$delete_bool=true;
	//	$role_inst->
			$nns_ids=explode(",",$nns_id);
			$num=0;
			foreach($nns_ids as $nnsid){
				if (!empty($nnsid)){
					$num++;
					$resultarr=$category_inst->nns_db_category_detail_delete($nnsid);
					if ($resultarr["ret"]!=0 && $num==1){
						$delete_bool=false;
						break;
					}
				}
			}
		if (!$delete_bool){
			echo "<script>alert('". $action_str, cms_get_lang('fault'), "');</script>";
		}else{
			echo "<script>alert('". $action_str, cms_get_lang('success'), "');</script>";
		}
		break;
		default:

		break;
	}
	$category_inst=null;
	//var_dump($result);

	echo "<script>self.location='nncms_content_detail_categorylist.php?nns_manager_type=". $category_type ."&nns_id=". $category_list."';</script>";
}
?>
