<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";

$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"105103");


 if (!isset($_GET["nns_manager_type"])){
	$pri_bool=false;
 }

 $category_type=$_GET["nns_manager_type"];
 $category_org_id="";
 if ($_SESSION["nns_manager_type"]!=0){
	$category_org_id=$_SESSION["nns_org_id"];
	if ($category_type!=$_SESSION["nns_manager_type"]){
		$pri_bool=false;
	}
 }
 $checkpri=null;
 if (!$pri_bool){
	 Header("Location: ../nncms_content_wrong.php");
 }else{
	 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
	// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
	 include($nncms_db_path. "nns_category/nns_db_category_list_class.php");
	 $category_inst=new nns_db_category_list_class();
	 $countArr=$category_inst->nns_db_category_list_count();
	 if ($countArr["ret"]==0)
	 $category_total_num=$countArr["data"][0]["num"];
	 $category_pages=ceil($category_total_num/$g_manager_list_max_num);
	 $currentpage=1;
	 if (!empty($_GET["page"])) $currentpage=$_GET["page"];
	 $category_array=$category_inst->nns_db_category_list_list("",$category_type,$category_org_id,"",($currentpage-1)*$g_manager_list_max_num,$g_manager_list_max_num);
	// var_dump($partner_array);
	 if ($category_array["ret"]!=0){
		$data=null;
		echo "<script>alert(". $category_array["reason"].");</script>";
	 }
	 $category_inst=null;
	 $data=$category_array["data"];
//	 if ($category_type==0){
//	 	 include($nncms_db_path. "nns_carrier/nns_db_carrier_class.php");
//	 	 $carrier_inst=new nns_db_carrier_class();
//	 }else if ($category_type==1){
//	 	 include($nncms_db_path. "nns_partner/nns_db_partner_class.php");
//	 	 $partner_inst=new nns_db_partner_class();
//	 }else if($category_type==2){
//	 	 include($nncms_db_path. "nns_group/nns_db_group_class.php");
//	 	 $group_inst=new nns_db_group_class();
//	 }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />

<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>

</head>

<body>
<div class="content">
	<div class="content_position"><?php echo cms_get_lang('lmgl');?> > <?php echo cms_get_lang('lmgl_nrlmgl');?></div>
	<div class="content_table formtable">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<thead>
			<tr>

				<th><?php echo cms_get_lang('segnumber');?></th>
				<th><?php echo cms_get_lang('name');?></th>
				<th><?php echo cms_get_lang('lmgl_lmmblx');?></th>
				<th><?php echo cms_get_lang('create_time');?></th>
				<th><?php echo cms_get_lang('edit_time');?></th>
			</tr>
		</thead>
		<tbody>
		<?php
			if ($data!=null){
			 $num=($currentpage-1)*$g_manager_list_max_num; foreach($data as $item){
			$num++;
			?>
			  <tr>
				<td><?php echo $num;?></td>
				<td><a href="nncms_content_detail_categorylist.php?nns_manager_type=<?php echo $category_type;?>&action=edit&nns_id=<?php echo  $item["nns_id"];?>"><?php echo $item["nns_name"];?></a></td>
				 <td><?php if ($item["nns_live"]==0){echo cms_get_lang('lmgl_dblm');}else if ($item["nns_live"]==1){echo cms_get_lang('lmgl_zblm');};?></td>
				<td><?php echo $item["nns_create_time"];?> </td>
				<td><?php echo $item["nns_modify_time"];?> </td>
			  </tr>
		  <?php }} $least_num=$nncms_ui_min_list_item_count-count($data);
				for ($i=0;$i<$least_num;$i++){?>
				<tr>

				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp; </td>
				<td>&nbsp; </td>
				</tr>

			<?php	}?>

		  </tbody>
		</table>
	</div>
	 <div class="pagecontrol">
		<?php if ($currentpage>1){?>
		<a href="nncms_content_list_categorylist.php?nns_manager_type=<?php echo $category_type;?>&page=1" target="_self"><?php echo cms_get_lang('first_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="nncms_content_list_categorylist.php?nns_manager_type=<?php echo $category_type;?>&page=<?php echo $currentpage-1;?>" target="_self"><?php echo cms_get_lang('pre_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php } if ($currentpage<$category_pages){?>
		<a href="nncms_content_list_categorylist.php?nns_manager_type=<?php echo $category_type;?>&page=<?php echo $currentpage+1;?>" target="_self"><?php echo cms_get_lang('next_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="nncms_content_list_categorylist.php?nns_manager_type=<?php echo $category_type;?>&page=<?php echo $category_pages;?>" target="_self"><?php echo cms_get_lang('last_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php }?>
	</div>

	 <!--<form id="delete_form" action="nncms_content_category_control.php" method="post">
	  <input name="action" id="action" type="hidden" value="delete" />
	  <input name="nns_id" id="nns_id" type="hidden" value="" />
	  <input name="category_type" id="category_type" type="hidden" value="<?php echo $category_type;?>" />
	 </form>-->
</div>
</body>
</html>
<?php }?>
