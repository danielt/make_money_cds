<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";

$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"105103");
$checkpri=null;
 $nns_id=$_GET["nns_id"];
 if (!isset($_GET["nns_manager_type"])){
	$pri_bool=false;
 }

 $category_type=$_GET["nns_manager_type"];
 $category_org_id="";
 if ($_SESSION["nns_manager_type"]!=0){
	$category_org_id=$_SESSION["nns_org_id"];
	if ($category_type!=$_SESSION["nns_manager_type"]){
		$pri_bool=false;
	}
 }

 if (!$pri_bool){
	 Header("Location: ../nncms_content_wrong.php");
 }else{
	 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
	// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
	 include($nncms_db_path. "nns_category/nns_db_category_detail_class.php");
	 $category_inst=new nns_db_category_detail_class();

	 $category_array=$category_inst->nns_db_category_detail_list("",$nns_id);
	// var_dump($partner_array);
	 if ($category_array["ret"]!=0){
		$data=null;
		echo "<script>alert(". $category_array["reason"].");</script>";
	 }
	 $category_inst=null;
	 $data=$category_array["data"];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
<script language="javascript" src="../../js/dtree.js"></script>
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript">
$(document).ready(function(){
	$(".category_editbox").width($(".content").width()-400);
	 $(window).resize(function(){
							$(".category_editbox").width($(".content").width()-380);
													});

});
function checkhiddenInput(){
	var par=getAllCheckBoxSelect();
	if (par==""){
		alert("<?php echo cms_get_lang('lmgl_msg_noselect');?>");
	}else{
		$('#nns_id').attr('value',getAllCheckBoxSelect());
		checkForm('<?php echo cms_get_lang('lmgl_lmmbgl');?>','<?php echo cms_get_lang('msg_ask_delete');?>',$('#delete_form'));
	}

}

function select_tree_item(item_id,item_name,parent_id){
	if (item_id==<?php echo $nns_id?>){
		item_value=0;
		$(".category_edit_box").find("#nns_category_detail_name").attr("disabled",true);
		$(".category_edit_box").find("input[type='button']:eq(0)").attr("disabled",true);
		$(".category_edit_box").find("input[type='button']:eq(1)").attr("disabled",true);
	}else{
		item_value=item_id;
		$(".category_edit_box").find("#nns_category_detail_name").attr("disabled",false);
		$(".category_edit_box").find("input[type='button']:eq(0)").attr("disabled",false);
		$(".category_edit_box").find("input[type='button']:eq(1)").attr("disabled",false);
	}
	$(".category_edit_box").show();
	$(".category_edit_box").find("#category_parent").val(parent_id);
	$(".category_edit_box").find("#nns_id").val(item_value);

	$(".category_edit_box").find(".category_edit_id").html(item_id);
	$(".category_edit_box").find("#nns_category_detail_name").val(item_name);
	$(".category_add_box").hide();
}

function add_category_command(){
	$(".category_add_box").show();
	$(".category_add_box").find("#category_parent").val($(".category_edit_box").find("#nns_id").val());
	$(".category_add_box").find(".parent_category_box").html($(".category_edit_box").find("#nns_category_detail_name").val());
}


function edit_node(){

	$(".category_edit_box").find("#action").val("edit");
	checkForm('<?php echo cms_get_lang('lmgl');?>','<?php echo cms_get_lang('msg_ask_change');?>',$('#edit_form'),"edit");
}
function delete_node(){

	$(".category_edit_box").find("#action").val("delete");
	checkForm('<?php echo cms_get_lang('lmgl');?>','<?php echo cms_get_lang('msg_ask_change');?>',$('#edit_form'),"delete");
}
</script>
</head>

<body>
<div class="content">
	<div class="content_position"><?php echo cms_get_lang('lmgl');?> > <?php echo cms_get_lang('lmgl_nrlmgl');?></div>
	<div style="background-color:#FFF;">
		<div class="category_tree">
			<script type="text/javascript">
			<!--

			d = new dTree('d');

			d.add(0,-1,'<?php echo cms_get_lang('lmgl_gmu');?>',"javascript:select_tree_item('<?php echo $nns_id?>','<?php echo cms_get_lang('lmgl_gmu');?>','0');");
			<?php if ($data!=null){
				 foreach($data as $item){

				if ($item["nns_parent_id"])
				?>
			d.add(<?php echo $item["nns_id"];?>,<?php echo $item["nns_parent_id"];?>,'<?php echo $item["nns_name"];?>',"javascript:select_tree_item('<?php echo $item["nns_id"];?>','<?php echo $item["nns_name"];?>','<?php echo $item["nns_parent_id"];?>');");
			<?php }}?>

			document.write(d);


			//-->

			</script>
		</div>
		<div class="category_editbox" >
			<div class="category_edit_box">
				 <form id="edit_form" action="nncms_content_category_detail_control.php" method="post">
				<input name="action" id="action" type="hidden" value="" />
				<input name="category_list" id="category_list" type="hidden" value="<?php echo $nns_id;?>" />
				<input name="category_type" id="category_type" type="hidden" value="<?php echo $category_type;?>" />
				<input name="category_parent" id="category_parent" type="hidden" value="" />
				<input name="nns_id" id="nns_id" type="hidden" value="" />
				<div class="content_table">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td width="120"><?php echo cms_get_lang('lmgl_nrlmid');?> </td>
							<td class="category_edit_id"></td>

						</tr>
						  <tr>
							<td width="120"><?php echo cms_get_lang('lmgl_lmnrmc');?> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
							<td><input name="nns_category_detail_name" id="nns_category_detail_name" type="text" rule="noempty"
								value=""
							/></td>

						</tr>
						<tr>
							<td width="120"><?php echo cms_get_lang('action');?> </td>
							<td><input name="" type="button" value="<?php echo cms_get_lang('edit');?>" onclick="edit_node();"/>&nbsp;&nbsp;
								<input name="" type="button" value="<?php echo cms_get_lang('delete');?>" onclick="delete_node();"/>&nbsp;&nbsp;
								<input name="" type="button" value="<?php echo cms_get_lang('add|lmgl_tjzlm');?>" onclick="add_category_command();"/>&nbsp;&nbsp;
							</td>
						</tr>
					  </tbody>
					</table>
				</div>
				</form>
			</div>
			<div class="category_add_box">
				 <form id="add_form" action="nncms_content_category_detail_control.php" method="post">
				<input name="action" id="action" type="hidden" value="add" />
				<input name="category_list" id="category_list" type="hidden" value="<?php echo $nns_id;?>" />
				<input name="category_type" id="category_type" type="hidden" value="<?php echo $category_type;?>" />
				<input name="category_parent" id="category_parent" type="hidden" value="" />
				<div class="content_table">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td width="120"><?php echo cms_get_lang('lmgl_sjlm');?> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
							<td class="parent_category_box">

							</td>

						</tr>
						<tr>
							<td width="120"><?php echo cms_get_lang('lmgl_nrlmid');?> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
							<td><input name="nns_category_detail_id" id="nns_category_detail_id" type="text" rule="noempty|int" len="3" style="width:20%" maxlength="3"
								value=""
							/></td>

						</tr>
						  <tr>
							<td width="120"><?php echo cms_get_lang('lmgl_lmnrmc');?> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
							<td><input name="nns_category_detail_name" id="nns_category_detail_name" type="text" rule="noempty"
								value=""
							/></td>

						</tr>


						 <tr>
							<td width="120"><?php echo cms_get_lang('action');?></td>
							<td>
								<input name="" type="button" value="<?php echo cms_get_lang('add');?>" onclick="checkForm('<?php echo cms_get_lang('lmgl');?>','<?php echo cms_get_lang('msg_ask_change');?>',$('#add_form'),'add');"/>&nbsp;&nbsp;
							</td>

						</tr>
					  </tbody>
					</table>
				</div>
				</form>
			</div>
		</div>
		<div style="clear:both;"/>
	</div>
	  <div class="controlbtns" style=" border-top:1px solid #cfcfcf;">

		<div class="controlbtn back"><a href="javascript:returnToHistory();"  action="reset"><?php echo cms_get_lang('back');?></a></div>

		<div style="clear:both;"></div>
	</div>

</div>
</body>
</html>
<script>
d.clearCookie();
select_tree_item('<?php echo $nns_id?>','<?php echo cms_get_lang('lmgl_gmu');?>','0');
</script>
<?php }?>
