<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";


 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();

$action=$_GET["action"];
$nns_id=$_GET["nns_id"];
$category_list=$_GET["category_list"];
$pri_bool=false;
if (!empty($action)){
	switch($action){
		case "edit":
		$action_str=cms_get_lang('edit');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"105101");
		break;
		case "add":
		$action_str=cms_get_lang('add');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"105100");
		break;
		default:
		break;
	}
}
$checkpri=null;
 if (!isset($_GET["nns_manager_type"])){
	$pri_bool=false;
 }
 
 $category_type=$_GET["nns_manager_type"];
 $category_org_id="";
 if ($_SESSION["nns_manager_type"]!=0){
	$category_org_id=$_SESSION["nns_org_id"];
	if ($category_type!=$_SESSION["nns_manager_type"]){
		$pri_bool=false;
	}
 }

if (!$pri_bool){
	 Header("Location: ../nncms_content_wrong.php");
}else{

	 
	 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
	// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
	// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");
	 include $nncms_db_path. "nns_category/nns_db_category_detail_class.php";
	$category_inst=new nns_db_category_detail_class();
	 if ($action=="edit"){
		
		if (!empty($nns_id)){
			$category_info=$category_inst->nns_db_category_detail_info($nns_id);
			
		}else{ 
			echo "<script>alert('".$category_info["reason"] ."');self.location='nncms_content_detail_categorylist.php?nns_manager_type=".$category_type ."&nns_id=". $nns_id."';</script>";
		}
		if ($category_info["ret"]!=0){
			echo "<script>alert('".$category_info["reason"] ."');self.location='nncms_content_detail_categorylist.php?nns_manager_type=".$category_type ."&nns_id=". $nns_id."';</script>";
		}
		$edit_data=$category_info["data"][0];
	//	$edit_pri=$edit_data["nns_pri_id"];
	 }
		
		$category_result=$category_inst->nns_db_category_detail_list("",$category_list);
		$category_inst=null;
		if ($category_result["ret"]==0){
			$category_data=$category_result["data"];
		}
		


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/radiolist.js"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>

</head>

<body>
<div class="content">
	<div class="content_position"><?php echo cms_get_lang('lmgl');?> > <?php echo cms_get_lang('lmgl_nrlmgl');?></div>
	<form id="add_form" action="nncms_content_category_detail_control.php" method="post">
	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
	<input name="category_list" id="category_list" type="hidden" value="<?php echo $category_list;?>" />
	<input name="category_type" id="category_type" type="hidden" value="<?php echo $category_type;?>" />
	<?php if ($action=="edit"){?>
	<input name="nns_id" id="nns_id" type="hidden" value="<?php echo $nns_id;?>"/>
	<?php } ?>
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			  <tr>                                                            
				<td width="120"><?php echo cms_get_lang('lmgl_lmnrmc');?> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
				<td><input name="nns_category_detail_name" id="nns_category_detail_name" type="text" rule="noempty"
					value="<?php if($action=="edit"){echo $edit_data["nns_name"];}?>"
				/></td>
				
			</tr>
	  
			 <tr>                                                            
				<td width="120"><?php echo cms_get_lang('lmgl_sjlm');?> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
				<td>
					<select name="category_parent" id="category_parent" rule="noempty" style="margin-right:20px;"><br>
						<option value="0" ><?php echo cms_get_lang('lmgl_gmu');?></option>
<!--  						<?php if ($category_data){ foreach($category_data as $category_item){
							if ($action=="edit"){
								if ($category_item["nns_id"]==$edit_data["nns_id"])
								continue;
							}
							?>
							<option value="<?php echo $category_item["nns_id"];?>"  <?php if ($action=="edit" && $edit_data["nns_parent_id"]==$category_item["nns_id"]){?>selected="selected"<?php }?>><?php echo $category_item["nns_name"];?></option>
						<?php }}?>-->
					</select> 
					</td>
				
			</tr> 
		  </tbody>
		</table>
	</div>
	</form>
	<div class="controlbtns">
		<div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('lmgl_lmmbgl');?>','<?php echo cms_get_lang('msg_ask_change'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>





</body>
</html>
<?php }?>

