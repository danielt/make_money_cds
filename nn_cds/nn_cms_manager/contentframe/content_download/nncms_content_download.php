<?php
error_reporting(7);
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/vod_media/vod_media.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
//导入语言包
if (isset($_SESSION["language_dir"]))
{
	$language_dir = $_SESSION["language_dir"];
}
include_once $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include_once $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool)
{
	Header("Location: ../nncms_content_wrong.php");
	exit();
}
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE, 
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$cp_data = nl_cp::query_all($dc);
$cp_data = isset($cp_data['data_info']) ? $cp_data['data_info'] : array();
if(isset($_REQUEST['op']) && !empty($_REQUEST['op']))
{
	if($_REQUEST['op'] == 'retry')
	{
		$clip_state = (isset($_REQUEST['clip_state']) && $_REQUEST['clip_state'] == 0) ? 0 : 4;
		$arr_id = array_filter(array_unique(explode(',', isset($_REQUEST['ids']) ? $_REQUEST['ids'] : '')));
		if(empty($arr_id) || !is_array($arr_id))
		{
			echo '<script>alert("操作成功");history.go(-1);</script>';die;
		}
		$result=nl_vod_media_v2::edit_arr_id($dc, $arr_id,$clip_state);
		if($result['ret'] !=0)
		{
			echo '<script>alert("操作失败：'.$result['reason'].'");history.go(-1);</script>';die;
		}
		echo '<script>alert("操作成功");history.go(-1);</script>';die;
	}
}
$page_info = $params = array ();
$page_info['page_num'] = isset($_GET['page']) ? $_GET['page'] : 1;
$page_info['page_size'] = isset($_GET['page_size']) ? $_GET['page_size'] : (isset($_COOKIE['page_max_num']) && $_COOKIE['page_max_num'] > 0) ? $_COOKIE['page_max_num'] : 18;
if (isset($_REQUEST['nns_video_name']) && strlen($_REQUEST['nns_video_name']) > 0)
{
	$params['like']['nns_video_name'] = $_REQUEST['nns_video_name'];
}
if (isset($_REQUEST['nns_cp_id']) && strlen($_REQUEST['nns_cp_id']) > 0)
{
	$params['where']['nns_cp_id'] = $_REQUEST['nns_cp_id'];
}
if (isset($_REQUEST['nns_media_name']) && strlen($_REQUEST['nns_media_name']) > 0)
{
	if(strpos($_REQUEST['nns_media_name'], 'mgtv') ===  0)
	{
		$_REQUEST['nns_media_name']=substr($_REQUEST['nns_media_name'], 4);
	}
	$params['like']['nns_media_name'] = $_REQUEST['nns_media_name'];
}
if (isset($_REQUEST['nns_import_id']) && strlen($_REQUEST['nns_import_id']) > 0)
{
	$params['where']['nns_import_id'] = $_REQUEST['nns_import_id'];
}
if (isset($_REQUEST['nns_clip_state']) && strlen($_REQUEST['nns_clip_state']) > 0)
{
	$params['where']['nns_clip_state'] = $_REQUEST['nns_clip_state'];
}
if (isset($_REQUEST['nns_content_id']) && strlen($_REQUEST['nns_content_id']) > 0)
{
	$params['where']['nns_content_id'] = $_REQUEST['nns_content_id'];
}
if (isset($_REQUEST['create_begin_time']) && strlen($_REQUEST['create_begin_time']) > 0)
{
	$params['where']['create_begin_time'] = $_REQUEST['create_begin_time'];
}
if (isset($_REQUEST['create_end_time']) && strlen($_REQUEST['create_end_time']) > 0)
{
	$params['where']['create_end_time'] = $_REQUEST['create_end_time'];
}
if (isset($_REQUEST['nns_deleted']) && strlen($_REQUEST['nns_deleted']) > 0)
{
	$params['where']['nns_deleted'] = $_REQUEST['nns_deleted'];
}
$url = 'nncms_content_download.php?flag_url=1';
foreach ($_REQUEST as $k => $v)
{
	if ($k != 'flag_url' && $k != 'page')
	{
		$url .= '&' . $k . '=' . $v;
	}
}

$result = nl_vod_media_v2::query($dc, $params, $page_info);
$pager = new nl_pager($result['page_info']['total_count'], $page_info['page_size'], $page_info['page_num'], $url);
//if ($result['ret'] != 0)
//{
//	echo '<script>alert("' . $result['reason'] . '");</script>';
//}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
<link href="../../css/comm.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/rate.js"></script>
<script language="javascript" src="../../js/image_loaded_func.js"></script>
<script language="javascript" src="../../js/cms_datepicker.js"></script>
<script language="javascript" src="../../js/alertbox.js"></script>
<script language="javascript">
			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				})
				return BoxKey;
			}
			function msg_retry()
			{
				var r=confirm("是否进行该操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "nncms_content_download.php?op=retry&ids="+ids;
						window.location.href = url;
					}
				}
			}
			
			$(document).ready(function() {
				$('#clear_time').click(function(){
					$('#create_begin_time').val('');
					$('#create_end_time').val('');

				});
				window.parent.now_frame_url = window.location.href;
			});

			$(document).ready(function() {
				$('#clear_time_modify').click(function(){
					$('#nns_begin_time').val('');
					$('#nns_end_time').val('');

				});
				window.parent.now_frame_url = window.location.href;
			});
		</script>
</head>
<body>
	<div class="content">
		<div class="content_position">片源列表</div>
		<div class="content_table">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<form action="nncms_content_download.php" method="get">
					<tbody>
						<tr>
						<td>
							 &nbsp;&nbsp;&nbsp;&nbsp;影片名称：&nbsp;&nbsp;<input type="text" name="nns_video_name"
								id="nns_video_name" style="width:150px;"
								value="<?php echo $_GET['nns_video_name']; ?>"
								>
							&nbsp;&nbsp;&nbsp;&nbsp;片源文件名称：&nbsp;&nbsp;<input type="text" name="nns_media_name"
								id="nns_media_name" style="width:150px;"
								value="<?php echo $_GET['nns_media_name']; ?>"
								>
							 &nbsp;&nbsp;&nbsp;&nbsp;注入ID：&nbsp;&nbsp;<input type="text" name="nns_import_id"
								id="nns_import_id" style="width:150px;"
								value="<?php echo $_GET['nns_import_id']; ?>"
								>
							
							&nbsp;&nbsp;&nbsp;&nbsp;content ID：&nbsp;&nbsp;<input type="text" name="nns_content_id"
								id="nns_content_id" style="width:150px;"
								value="<?php echo $_GET['nns_content_id']; ?>"
								>
								<br/>
							 &nbsp;&nbsp;&nbsp;&nbsp;CP ID：&nbsp;&nbsp; <select name="nns_cp_id" style="width: 120px;">
									<option value="">全部</option>
									<?php if(is_array($cp_data) && !empty($cp_data)){
										foreach ($cp_data as $val)
										{
									?>
										<option value="<?php echo $val['nns_id'];?>"
										<?php if($_GET['nns_cp_id'] == $val['nns_id']){ echo "selected";}?>><?php echo $val['nns_name'];?></option>
									<?php 
										}
									}?>
							</select>
							
							 &nbsp;&nbsp;&nbsp;&nbsp;下载状态：&nbsp;&nbsp; <select name="nns_clip_state" style="width: 120px;">
									<option value="">全部</option>
									<option value="0"
										<?php if($_GET['nns_clip_state'] == '0'){ echo "selected";}?>>原始状态</option>
									<option value="1"
										<?php if($_GET['nns_clip_state'] == '1'){ echo "selected";}?>>等待下载</option>
									<option value="2"
										<?php if($_GET['nns_clip_state'] == '2'){ echo "selected";}?>>正在下载</option>
									<option value="3"
										<?php if($_GET['nns_clip_state'] == '3'){ echo "selected";}?>>下载成功</option>
									<option value="4"
										<?php if($_GET['nns_clip_state'] == '4'){ echo "selected";}?>>下载失败</option>
							</select>
							
							 &nbsp;&nbsp;&nbsp;&nbsp;删除状态：&nbsp;&nbsp; <select name="nns_deleted" style="width: 120px;">
									<option value="">全部</option>
									<option value="0"
										<?php if($_GET['nns_deleted'] == '0'){ echo "selected";}?>>未删除</option>
									<option value="1"
										<?php if($_GET['nns_deleted'] == '1'){ echo "selected";}?>>已删除</option>
							</select>
							<br/>
							 &nbsp;&nbsp;&nbsp;&nbsp;选择创建时间段：&nbsp;&nbsp;&nbsp;<input name="create_begin_time" id="create_begin_time" type="text"  value="<?php
										if (isset($_GET['create_begin_time']))
											echo $_GET['create_begin_time'];
									?>" style="width:120px;" class="datetimepicker" callback="test" />
										 - <input name="create_end_time" id="create_end_time" type="text"  value="<?php
											if (isset($_GET['create_end_time']))
												echo $_GET['create_end_time'];
										?>" style="width:120px;" class="datetimepicker" callback="test" />
										 &nbsp;&nbsp;<input type="button" id="clear_time" name="clear_time" value="清除时间"/>
							<input type="submit" value="查询" id="inquery" name="inquery" />
						</td>
						</tr>
					</tbody>
				</form>
			</table>
		</div>
		<div class="content_table formtable">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th style="width: 30px;"><input name="" type="checkbox" value=""></th>
						<th style="width: 150px;">影片名称</th>
						<th style="width: 80px;">片源文件名称</th>
						<th style="width: 150px;">片源路径</th>
						<th style="width: 80px;">content ID</th>
						<th style="width: 80px;">注入ID</th>
						<th style="width: 50px;">CP ID</th>
						<th style="width: 50px;">下载状态</th>
						<th style="width: 40px;">删除状态</th>
						<th style="width: 60px;">创建时间</th>
						<th style="width: 60px;">修改时间</th>
						<th style="width: 50px;">操作</th>
					</tr>
				</thead>
				<tbody>
						<?php
						if (!empty($result['data_info']) && is_array($result['data_info']))
						{
							$num = ($page_info['page_num'] - 1) * $page_info['page_size'];
							foreach ($result['data_info'] as $item)
							{
								$num++;
								?>
						<tr>
						<td id="is_delete"><input name="input" type="checkbox"
							value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
						<td><?php echo $item['nns_name'];?></td>
						<td><?php echo $item['nns_media_name'];?></td>
						<td><?php echo $item['nns_url'];?></td>
						<td><?php echo $item['nns_content_id'];?></td>
						<td><?php echo $item['nns_import_id'];?></td>
						<td><?php echo $item['nns_cp_id'];?></td>
						<td><?php if($item['nns_clip_state'] == 0){ echo "原始状态";}
								  else if($item['nns_clip_state'] == 1){ echo "等待下载";}
								  else if($item['nns_clip_state'] == 2){ echo "正在下载";}
								  else if($item['nns_clip_state'] == 3){ echo "下载成功";}
								  else if($item['nns_clip_state'] == 4){ echo "<font style=\"color:red\">下载失败</font>";}
								  else{ echo "<font style=\"color:#FF0000\">未知</font>";};?>
						</td>
						<td>
							<?php   if($item['nns_deleted'] == '0')
									{
										echo "未删除";
									}
									else
									{
										echo "<font style=\"color:red\">已删除</font>";
									}
							?>
						</td>
						
						<td><?php echo $item['nns_create_time'];?></td>
						<td><?php echo $item['nns_modify_time'];?></td>

						<td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                        	<?php if($item['nns_clip_state'] == '4'){?>
                        		<a href="nncms_content_download.php?op=retry&ids=<?php echo $item['nns_id'];?>">重新下载</a>
                        	<?php }else if($item['nns_clip_state'] == '0'){?>
                        		<a href="nncms_content_download.php?op=retry&ids=<?php echo $item['nns_id'];?>&clip_state=0&clip_flag=1">下载注入</a>
                        	<?php }?>
                        		<a href="../media_pages/nncms_content_media_addedit.php?view_type=&action=edit&vod_id=<?php echo $item['nns_vod_id'];?>&index=<?php echo $item['index_index'];?>&nns_id=<?php echo $item['nns_id'];?>">修改</a>
                        </td>
					</tr>
						<?php
							}
						}$least_num = $page_info['page_size'] - count($result['data_info']);
						for ($i = 0; $i < $least_num; $i++) {
						?>
						<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
						<?php } ?>
					</tbody>
			</table>
		</div>
			<?php echo $pager->nav();?>
		<div class="controlbtns">
			<div class="controlbtn allselect">
				<a href="javascript:selectAllCheckBox(true);">全选</a>
			</div>
			<div class="controlbtn cancel">
				<a href="javascript:selectAllCheckBox(false);">取消</a>
			</div>
			<div class="controlbtn move">
				<a href="javascript:msg_retry();">重新下载</a>
			</div>
		</div>

	</div>
</body>
</html>
