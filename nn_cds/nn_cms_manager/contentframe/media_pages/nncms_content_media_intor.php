<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

include LOGIC_DIR.'video'.DIRECTORY_SEPARATOR.'vod_index_info_language.class.php';
include LOGIC_DIR.'log'.DIRECTORY_SEPARATOR.'manager_log.class.php';
   // 获取cache模块
include LOGIC_DIR.'video'.DIRECTORY_SEPARATOR.'vod_index.class.php';
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
			));
 $dc->open();
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";
include $nncms_config_path. "nn_cms_manager/languages/language_contant.php";
//require_once $nncms_config_path. "nn_cms_manager/controls/nncms_controls_clear_cache.php";
include_once $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php";

 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();

$nns_id=$_REQUEST["nns_id"];
$index=$_GET["index"];
$view_type=$_GET["view_type"];
$pri_bool=false;
$pri_bool1=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"106101");
$pri_bool2=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"106100");
if ($pri_bool1 || $pri_bool2) $pri_bool=true;
$checkpri=null;


if (!$pri_bool){
	 Header("Location: ../nncms_content_wrong.php");
}
else
{	
		include ($nncms_db_path. "nns_common/nns_db_constant.php");
		require_once $nncms_config_path. "nn_cms_manager/controls/nncms_control_memcache.php";
		$nns_id_post=$_POST["nns_id"];
		$index_post=$_POST["index"];
		$nns_vod_index_name=$_POST["nns_vod_index_name"];
		$nns_vod_index_summary=$_POST["nns_vod_index_summary"];
		$nns_vod_index_time_len=$_POST["nns_vod_index_time_len"];
		$nns_director=$_POST["nns_director"];
		$nns_actor=$_POST["nns_actor"];
		$nns_vod_index_time_len=$nns_vod_index_time_len*60;
	//	多语言参数
		$index_names=$_POST['nns_index_name'];
		$summarys=$_POST['nns_summary'];
		$nns_release_time = $_POST['nns_release_time'];

	/**
	  * 扩展语言 BY S67
	  */

		$extend_langs_str=g_cms_config::get_g_extend_language();
		if (empty($extend_langs_str)){
			$extend_langs=array();
		}else{
			$extend_langs=explode('|',$extend_langs_str);
		}
		include $nncms_db_path. "nns_vod/nns_db_vod_index_class.php";
		$vod_inst=new nns_db_vod_index_class();
		$vod_info=$vod_inst->nns_db_vod_index_info($nns_id,$index-1);
		$edit_data=$vod_info["data"][0];
		$name_result=nl_manager_log::get_names_by_video_id($dc,array($nns_id));
		if ($name_result===TRUE || $name_result===FALSE){
			$nns_name='';
			$cp_id = '';
		}else{
			$nns_name=$name_result[$nns_id]['nns_name'];
			$cp_id = $name_result[$nns_id]['nns_cp_id'];
		}

		$result_cp = nl_cp::query_by_id($dc, $cp_id);
		if($result_cp['ret'] !=0)
		{
		    echo "<script>alert('". $result_cp['reason']. "');history.go(-2);</script>";die;
		}
		$result_cp = isset($result_cp['data_info']) ? $result_cp['data_info'] : null;
		$arr_cp_config = (isset($result_cp['nns_config']) || strlen($result_cp['nns_config']) >0) ? json_decode($result_cp['nns_config'],true) : null;
		// 载入全局的芒果tv的ftp配置数据,是一个二维数组
		if (isset($arr_cp_config['g_ftp_conf']['img_enabled']) && $arr_cp_config['g_ftp_conf']['img_enabled'] == 1)
		{
		    $g_ftp_conf = $arr_cp_config['g_ftp_conf'];
		}
		else
		{
		    global $g_ftp_conf;
		}
		
		
		
		if (!empty($nns_id_post))
		{
			$vod_info=$vod_inst->nns_db_vod_index_info($nns_id_post,$index_post);
			$edit_data=$vod_info["data"][0];
			$img_old_url=$edit_data["nns_image"];

			$imgurl=pub_func_image_upload_by_vod_index($_FILES['index_image'],$nns_id_post,$index_post,$g_ftp_conf,$cp_id);


			if (!empty($imgurl)){
				pub_func_image_delete($img_old_url,$g_ftp_conf);
			}else{
				$imgurl=$img_old_url;
			}
			$vod_result=$vod_inst->nns_db_vod_index_modify(
			$nns_id_post,
			$index_post,
			$nns_vod_index_name,
			$nns_vod_index_time_len,
			$imgurl,
			$nns_vod_index_summary,
			$nns_director,
			$nns_actor,$edit_data['nns_import_source'],null,$nns_release_time,$cp_id);
			$vod_inst=null;

//			修改多语言信息
				$num=0;
				foreach ($extend_langs as $lang){

					$params=array();
					$params['name']=$index_names[$num];
					$params['summary']=$summarys[$num];
					$result=nl_vod_index_info_language::modify_vod_index_info_by_language($dc,$nns_id_post,$index_post,$lang,$params);


					$num++;
				}


			if ($vod_result["ret"]==0){

				nl_manager_log::add($dc,$_SESSION["nns_mgr_name"]."修改点播分片信息:".$nns_name.'分片'.($index_post+1),'edit');
				echo "<script>alert('修改成功!');history.go(-2);</script>";
			}else{
				echo "<script>alert('".$vod_result["reason"] ."');history.go(-2);</script>";
			}

		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/cms_datepicker.js"></script>

<script language="javascript">
	function show_extend_lang(num){

		if ($(".lang_editor:eq("+num+")").is(":hidden")==false){
			$(".lang_editor").hide();
			$(".lang_btn").html("<?php echo cms_get_lang('spread')?>");
		}else{

			$(".lang_editor").hide();
			$(".lang_btn").html("<?php echo cms_get_lang('spread')?>");
			$(".lang_editor:eq("+num+")").show();
			$(".lang_btn:eq("+num+")").html("<?php echo cms_get_lang('fold')?>");
		}
		window.parent.checkheight();
	}

	function set_show_minor(num){
		$obj=$("input[name=moreinfo_chk]:eq("+num+")");
		if ($obj.attr("checked")=="checked"){
			$(".extend_"+num).show();
		}else{
			$(".extend_"+num).hide();
		}
		window.parent.checkheight();
	}

	$(document).ready(function(){
		$(".lang_editor").hide();
		show_extend_lang(0);
		var len=$("input[name=moreinfo_chk]").length;
		for(var i=0;i<len;i++){
			set_show_minor(i);
		}
	});
</script>
</head>

<body>

<div class="content">

    <form action="nncms_content_media_intor.php" id="add_form" method="post" enctype="multipart/form-data">
    <input name="nns_id" id="nns_id" type="hidden" value="<?php echo $nns_id;?>"/>
    <div class="content_table">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        	 <tr>
                <td width="120"><?php echo cms_get_lang('media_mtid')?>:</td>
                <td><?php echo $nns_id;?></td>
      		</tr>
      		 <tr>
                <td width="120"><?php echo cms_get_lang('media_mtmc');?>:</td>
                <td><?php echo $nns_name;?></td>
      		</tr>
        	<tr>
      			<td colspan="2"   style="padding:0px;"><div class="radiolist"  style="padding:3px 0px;"><div class="radiogroup">
      			<h3><?php if($view_type==0){
      				 echo cms_get_lang('media_fp'). $index;
      			}else{
      				 echo cms_get_lang('media_js').$index;
      			} ?></h3>
      			</div></div></td>
      		</tr>
              <tr>
                <td width="120"><?php echo cms_get_lang('name');?>:</td>
                <td><input name="nns_vod_index_name" id="nns_vod_index_name" type="text" size="60%" value="<?php echo $edit_data["nns_name"];?>"/></td>
      		</tr>
			<tr>
				<td width="120">分集号:</td>
				<td><input name="index" id="index" type="text" value="<?php echo $index-1;?>"/>【从0开始】</td>
			</tr>
      	 	<tr>
                <td width="120"><?php echo cms_get_lang('media_index_sc');?>:</td>
                <td><input name="nns_vod_index_time_len" id="nns_vod_index_time_len" type="text" size="60%" value="<?php echo round($edit_data["nns_time_len"]/60);?>" rule="int"/>【单位为秒】</td>
      		</tr>
      		 <tr>
                <td width="120"><?php echo cms_get_lang('media_bjhb')?>:</td>
                <td>
                	<div class="img_content">
                	 <?php if (!empty($edit_data["nns_image"])){?>
                    	<div class="img_view"><h4><?php echo $language_media_addedit_pictrue;?></h4>
                    	<a href="<?php echo pub_func_get_image_url($edit_data["nns_image"]);?>?rand=<?php rand();?>" target="_blank">
                    	<img src="<?php echo pub_func_get_image_url($edit_data["nns_image"]);?>?rand=<?php rand();?>" style="border:none;" /></a></div>
                	<?php }?>
                		<div class="img_edit"> <input name="index_image" id="index_image" type="file" size="60%"/></div>
                		<div style="clear:both;"/>
                	</div>

               </td>
             <tr>
                <td width="120"><?php echo cms_get_lang('media_daoyan')?>:</td>
                <td><input name="nns_director" id="nns_director" type="text" size="60%" value="<?php echo $edit_data["nns_director"];?>"/></td>
      		</tr>
      		<tr>
                <td width="120"><?php echo cms_get_lang('media_yy')?>:</td>
                <td><input name="nns_actor" id="nns_actor" type="text" size="60%" value="<?php echo $edit_data["nns_actor"];?>" /></td>
      		</tr>
      		</tr>
      		<tr>
                <td width="120"><?php echo cms_get_lang('media_index_jj');?>:</td>
                <td><textarea name="nns_vod_index_summary" id="nns_vod_index_summary" cols="" rows=""><?php echo $edit_data["nns_summary"];?></textarea></td>

      		</tr>
      		<tr>
      		<td>更新时间：</td>
      		<td><input name="nns_release_time" id="nns_release_time" type="text" value="<?php if(!isset($edit_data['nns_release_time'])){ echo date('Y-m-d');}else{echo date('Y-m-d',strtotime($edit_data['nns_release_time']));}?>"  style="width:70px;" class="datepicker date_begin"/></td>
      		</tr>
      	<!--语言扩展-->
      	<?php
      		$num=0;
      		foreach ($extend_langs as $extend_lang){
      			$result=nl_vod_index_info_language::get_vod_index_info_by_language($dc,$nns_id,$index-1,$extend_lang,NL_DC_DB);
      			if ($result!==FALSE || $result!==TRUE){
      				$data=$result[0];
      			} else{
      				$data=NULL;
      			}
      	?>
      		<tr>
          	<td colspan="4"   style="padding:0px;"><div class="radiolist" style=" border-bottom:1px solid #86B7CC;"><div class="radiogroup">
          	<h3 style="  background-color:#DCEEF5;">
				<?php
					$lang_name=get_language_name_by_code($extend_lang);
					echo $lang_name;
				?><?php echo cms_get_lang('language_ex')?>
				<div style='padding:2px 5px 2px 20px; float:right; background:url(../../images/add_1.png) 0px 50% no-repeat;'>
				<a href='javascript:show_extend_lang(<?php echo $num;?>);' class="lang_btn"> <?php echo cms_get_lang('spread')?></a></div>
			</h3>

          	</div></div></td>
          </tr>
      	 	<tr class='lang_editor'>
          	<td colspan="4"   style="padding:0px;">
          		<div class="content_table">
          		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			        <tbody>
			        	<tr>
			                <td width="120"><?php echo $lang_name;?><?php echo cms_get_lang('name')?> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
			                <td><input name="nns_index_name[]" type="text" 
                					value="<?php echo $data['nns_name'];?>" 
               				 />:</td>
			      		</tr>

			      		<tr>
			                <td width="120"><?php echo $lang_name;?><?php echo cms_get_lang('media_index_jj')?>:</td>
			                <td>
			                	<textarea name="nns_summary[]" cols="" rows=""><?php echo $data['nns_summary'];?></textarea>

			                </td>
			      		</tr>

          			</tbody>
          		</table>
          		</div>
          	</td>
          </tr>
          <?php $num++; }?>
          </tbody>
        </table>
    </div>
    </form>
     <div class="controlbtns">
    	<div class="controlbtn edit"><a href="javascript:checkForm('<?php echo cms_get_lang('dbgl');?>','<?php echo cms_get_lang('msg_ask_change'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo cms_get_lang('confirm'); ?></a></div>
        <div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        <div style="clear:both;"></div>
    </div>
</div>

</body>
</html>
<?php }?>
<script language="javascript" src="../../js/image_loaded_func.js"></script>