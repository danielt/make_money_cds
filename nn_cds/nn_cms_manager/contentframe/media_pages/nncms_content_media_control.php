<?php

/*
 * Created on 2012-3-2
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
@header("Content-Type:text/html;charset=utf-8");
include("../../nncms_manager_inc.php");
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
// 导入多语言包
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}

// 获取cache模块
include LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'vod_media.class.php';
include LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'vod_index.class.php';
include LOGIC_DIR . 'video' . DIRECTORY_SEPARATOR . 'vod.class.php';
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
$dc->open();
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_error.php";
//require_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_clear_cache.php";
require_once $nncms_db_path . "nns_log" . DIRECTORY_SEPARATOR . "nns_db_op_log_class.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_class/drm/drm.class.php';
//require_once $nncms_db_path.DIRECTORY_SEPARATOR.'nns_debug_log'.DIRECTORY_SEPARATOR.'nns_debug_log_class.php';
//$log =  new nns_debug_log_class();
//$log->setlogpath('vod_media_sql.log');
//$log->write(var_export($_POST,true));
$log_inst = new nns_db_op_log_class();
$action = $_POST["action"];
$nns_id = $_POST["nns_id"];
$vod_id = $_POST["nns_vod_id"];
$audit = $_POST["audit"];
$result_video_info=nl_vod::get_video_info_by_id($dc, $vod_id);
if(isset($result_video_info['data_info'][0]) && is_array($result_video_info['data_info'][0]) && !empty($result_video_info['data_info'][0]))
{
    $_POST["nns_cp_id"] = $result_video_info['data_info'][0]['nns_cp_id'];
}
$cp_id = (isset($_POST["nns_cp_id"]) && strlen($_POST["nns_cp_id"]) > 0) ? $_POST["nns_cp_id"] : '0';
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
if (!empty($action)) {
        switch ($action) {
                case "edit":
                        $action_str = cms_get_lang('edit|media_db|media_mt');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106101");
                        break;
                case "add":
                        $action_str = cms_get_lang('add|media_db|media_mt');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106100");
                        break;
                case "delete":
                        $action_str = cms_get_lang('delete|media_db|media_mt');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106102");
                        break;
                case "audit":
                        $action_str = cms_get_lang('audit|media_db|media_mt');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "106103");
                        break;
                default:
                        break;
        }
}
$checkpri = null;

if (!$pri_bool) {
        Header("Location: ../nncms_content_wrong.php");
} else {
        $nns_media_name = $_POST["nns_media_name"];
        $nns_device_post_type = $_POST["nns_device_type"];
        $nns_tags = $_POST["nns_tags"];
        $nns_device_tag = $_POST["nns_device_tag"];
        $nns_media_url = $_POST["nns_media_url"];
        $nns_media_stream = $_POST["nns_media_stream"];
        $nns_media_mode = $_POST["nns_media_mode"];
        $nns_vod_id = $_POST["nns_vod_id"];
        $nns_index = $_POST["nns_index"];
        $nns_media_type_post = $_POST["nns_media_type"];
        $nns_media_policy = $_POST["nns_media_policy"];
        $insert_method = $_POST["insert_method"];
        $nns_media_content_id = $_POST["nns_media_content_id"];
        $nns_media_caps = $_POST["nns_media_caps"];
        $nns_file_size = $_POST["nns_file_size"];
		$nns_file_time_len = $_POST["nns_file_time_len"];
        $nns_kbps = $_POST['nns_kbps'];
        $nns_media_dimensions = $_POST['nns_media_dimensions'];
        if (is_array($nns_media_caps))
                $nns_media_caps = implode(',', $nns_media_caps);
//	对TAG进行扩展	
        if (!empty($nns_device_tag)) {
                $nns_tags.=$nns_device_tag . ",";
        }
        $nns_domain = isset($_POST['nns_domain']) ? $_POST['nns_domain'] : '';
        $nns_name = $log_inst->nns_db_get_video_name_arr($nns_vod_id);
        //require_once $nncms_config_path . "nn_cms_manager/controls/nncms_control_memcache.php";
        include $nncms_db_path . "nns_vod/nns_db_vod_media_class.php";
        $vod_inst = new nns_db_vod_media_class();
        switch ($action) {
                case "edit":
                        $result = $vod_inst->nns_db_vod_media_modify($nns_id, $nns_media_name, $nns_device_post_type, $nns_media_url, $nns_tags, $nns_media_mode, $nns_media_stream, $nns_media_caps, 
                        $nns_media_dimensions,$nns_media_content_id,$nns_domain);
                        $nns_media_name = $log_inst->nns_db_get_media_name_arr($nns_id);
                        if ($result["ret"] != 0) {
                                echo "<script>alert('" . $action_str . cms_get_lang('fault') . "');</script>";
                        } else {
                                $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_name . cms_get_lang('media_js') . ($nns_index + 1) . $nns_media_name, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                                //nl_vod_media::delete_vod_media_by_cache($dc, $nns_vod_id, $nns_index);
                                //delete_cache("media_$nns_vod_id" . "_" . $nns_index);
                                $params_media['file_size'] = $nns_file_size;
								$params_media['file_time_len'] = $nns_file_time_len;
                                $params_media['kbps'] = $nns_kbps;
                                $vod_inst->add_media_other_data($nns_id, $params_media);
                                $obj_drm = new nn_drm($dc,$cp_id);
                                $arr_drm_params = array(
                                    'asset_type'=>'vod',
                                    'nns_id'=>$nns_id
                                );
                                $result_drm_data = $obj_drm->make_drm_params($arr_drm_params);
                                if($result_drm_data['ret'] !=0)
                                {
                                    echo "<script>alert('" . var_export($result_drm_data,true) . "');</script>";
                                }
                                else
                                {
                                    $result_media_info = nl_vod_media::query_by_id($dc,$nns_id);
                                    if(isset($result_media_info['data_info']['nns_import_id']) && strlen($result_media_info['data_info']['nns_import_id']) <1)
                                    {
                                        $params_media['import_id'] = $result_media_info['data_info']['nns_integer_id'];
                                    }
                                    if(isset($result_drm_data['data_info']['drm_flag']))
                                    {
                                        $params_media['drm_enabled'] = $result_drm_data['data_info']['drm_flag'];
                                    }
                                    if(isset($result_drm_data['data_info']['drm_encrypt_solution']))
                                    {
                                        $params_media['drm_encrypt_solution'] = $result_drm_data['data_info']['drm_encrypt_solution'];
                                    }
                                    if(isset($result_drm_data['data_info']['drm_ext_info']))
                                    {
                                        $params_media['drm_ext_info'] = (is_array($result_drm_data['data_info']['drm_ext_info']) && !empty($result_drm_data['data_info']['drm_ext_info'])) ? json_encode($result_drm_data['data_info']['drm_ext_info']) : $result_drm_data['data_info']['drm_ext_info'];
                                        $params_media['drm_ext_info'] = (strlen($params_media['drm_ext_info']) <1) ? '' : $params_media['drm_ext_info'];
                                    }
                                    $vod_inst->add_media_other_data($nns_id, $params_media,$cp_id);
                                    echo "<script>alert('" . $action_str . cms_get_lang('success') . "');</script>";
                                }
//                                 echo "<script>alert('" . $action_str . cms_get_lang('success') . "');</script>";
                        }
                        break;
                case "add":
                        if ($insert_method == 0) {
                                $result = $vod_inst->nns_db_vod_media_add($nns_vod_id, "", $nns_media_name, $nns_index, $nns_media_mode, $nns_media_stream, 
                                    $nns_device_post_type, $nns_tags, $nns_media_url, $nns_file_time_len, null, $nns_media_policy, $nns_media_type_post,
                                    $nns_media_caps, $nns_media_dimensions,null,'0',$cp_id,1,'',date('Y-m-d H:i:s'),'',$nns_domain);
                            $nns_media_name = $log_inst->nns_db_get_media_name_arr($result["nns_id"]);
                        } else if ($insert_method == 1) {
                                $result = $vod_inst->nns_db_vod_media_bind($nns_vod_id, "", $nns_media_name, $nns_index, $nns_media_mode, $nns_media_stream, $nns_device_post_type, $nns_tags, $nns_media_url, 
                                    null, null, $nns_media_content_id, $nns_media_caps,null,null,'0',$cp_id,1,'',date('Y-m-d H:i:s'),'',$nns_domain);
                                $nns_media_name = $nns_media_content_id;
                        }
                       
                        
                        if ($result["ret"] != 0) {
                                $error_info = get_error_infomation($result["ret"]);
                                echo "<script>alert('" . $error_info . $action_str . cms_get_lang('fault') . "');</script>";
                        } else {
                                $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_name . $language_media_js . ($nns_index + 1) . $nns_media_name, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                                //nl_vod_media::delete_vod_media_by_cache($dc, $nns_vod_id, $nns_index);
                                //nl_vod_index::delete_vod_index_by_cache($dc, $nns_vod_id);
                                //delete_cache("media_$nns_vod_id" . "_" . $nns_index);
                                $obj_drm = new nn_drm($dc,$cp_id);
                                $arr_drm_params = array(
                                    'asset_type'=>'vod',
                                    'nns_id'=>$result['nns_id']
                                );
                                $result_drm_data = $obj_drm->make_drm_params($arr_drm_params);
                                if($result_drm_data['ret'] !=0)
                                {
                                    echo "<script>alert('" . var_export($result_drm_data,true) . "');</script>";
                                }
                                else
                                {
                                    $result_media_info = nl_vod_media::query_by_id($dc,$result['nns_id']);
                                    if(isset($result_media_info['data_info']['nns_import_id']) && strlen($result_media_info['data_info']['nns_import_id']) <1)
                                    {
                                        $params_media['import_id'] = $result_media_info['data_info']['nns_integer_id'];
                                    }
                                    if(isset($result_drm_data['data_info']['drm_flag']))
                                    {
                                        $params_media['drm_enabled'] = $result_drm_data['data_info']['drm_flag'];
                                    }
                                    if(isset($result_drm_data['data_info']['drm_encrypt_solution']))
                                    {
                                        $params_media['drm_encrypt_solution'] = $result_drm_data['data_info']['drm_encrypt_solution'];
                                    }
                                    if(isset($result_drm_data['data_info']['drm_ext_info']))
                                    {
                                        $params_media['drm_ext_info'] = (is_array($result_drm_data['data_info']['drm_ext_info']) && !empty($result_drm_data['data_info']['drm_ext_info'])) ? json_encode($result_drm_data['data_info']['drm_ext_info']) : $result_drm_data['data_info']['drm_ext_info'];
                                        $params_media['drm_ext_info'] = (strlen($params_media['drm_ext_info']) <1) ? '' : $params_media['drm_ext_info'];
                                    }
                                    $params_media['file_size'] = $nns_file_size;
    								$params_media['file_time_len'] = $nns_file_time_len;
                                    $params_media['kbps'] = $nns_kbps;
                                    $vod_inst->add_media_other_data($result['nns_id'], $params_media,$cp_id);
                                    echo "<script>alert('" . $action_str . cms_get_lang('success') . "');</script>";
                                }
                        }
                        break;
                case "delete":
                        $delete_bool = true;
                        //	$role_inst->
                        $nns_ids = explode(",", $nns_id);
                        $nns_media_name = $log_inst->nns_db_get_media_name_arr($nns_id);
                        $num = 0;
                        foreach ($nns_ids as $nnsid) {
                                if (!empty($nnsid)) {
                                        $num++;
                                        $resultarr = $vod_inst->nns_db_vod_media_delete($nnsid);
                                        if ($resultarr["ret"] != 0 && $num == 1) {
                                                $delete_bool = false;
                                                break;
                                        }
                                }
                        }
                        if (!$delete_bool) {
                                echo "<script>alert('" . $action_str . cms_get_lang('fault') . "');</script>";
                        } else {
                                $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . $action_str . ":" . $nns_name . $language_media_js . ($nns_index + 1) . $nns_media_name, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
                                //nl_vod_media::delete_vod_media_by_cache($dc, $nns_vod_id, $nns_index);
                                //delete_cache("media_$nns_vod_id" . "_" . $nns_index);
                                echo "<script>alert('" . $action_str . cms_get_lang('success') . "');</script>";
                        }
                        break;
                case "audit":
                        $audit_bool = true;
                        //	$role_inst->
                        $nns_ids = explode(",", $nns_id);
                        $num = 0;
                        foreach ($nns_ids as $nnsid) {
                                if (!empty($nnsid)) {
                                        $num++;

                                        $resultarr = $vod_inst->nns_db_vod_media_check($nnsid, $audit);

                                        if ($resultarr["ret"] != 0 && $num == 1) {
                                                $audit_bool = false;
                                                break;
                                        }
                                }
                        }
                        if (!$audit_bool) {
                                echo "<script>alert('" . $action_str . cms_get_lang('fault') . "');</script>";
                        } else {
                               // $cache_manager_control->clear_cache_video_info($nns_vod_id);
                                echo "<script>alert('" . $action_str . cms_get_lang('success') . "');</script>";
                        }
                        break;
                default:

                        break;
        }
        //var_dump($result);
        $category_inst = null;
        $log_inst = null;
        if ($action == "delete") {
                echo "<script>self.location.href='" . $_SERVER["HTTP_REFERER"] . "';</script>";
        } else {
//		echo "<script>self.location.href='nncms_content_media_list.php?nns_id=".$vod_id ."&view_type=".$nns_view_type ."';</script>";
                echo "<script>self.history.go(-2);</script>";
        }
}
?>
