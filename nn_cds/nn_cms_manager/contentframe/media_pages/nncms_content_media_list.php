<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
ob_start();

include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";
include $nncms_config_path. "nn_cms_manager/languages/language_contant.php";
include_once '../vod_pages/nncms_content_vod_dimensions.php';
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();

$nns_id=$_GET["nns_id"];
$view_type=$_GET["view_type"];
$pri_bool=false;
$pri_bool1=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"106101");
$pri_bool2=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"106100");
if ($pri_bool1 || $pri_bool2) $pri_bool=true;
$checkpri=null;

if (!$pri_bool || empty($nns_id)){
	Header("Location: ../nncms_content_wrong.php");
}else{

	include ($nncms_db_path. "nns_common/nns_db_constant.php");
	include $nncms_db_path. "nns_vod/nns_db_vod_class.php";
	$vod_inst=new nns_db_vod_class();
	$vod_index_count_arr = $vod_inst->nns_db_vod_info($nns_id,"*");
	$vod_inst=null;
	if ($vod_index_count_arr["ret"]==0)	$vod_index_data=$vod_index_count_arr["data"][0];

	$tar_str=ltrim($vod_index_data['nns_tag'],',');
	// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
	// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");
	include $nncms_db_path. "nns_vod/nns_db_vod_media_class.php";
	$vod_index_inst=new nns_db_vod_media_class();

	ob_end_clean();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/radiolist.js"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
	<script>
	function gotoDelete(nns_id){
		$("#nns_id").val(nns_id);
		checkForm('<?php echo cms_get_lang('dbgl');?>','<?php echo cms_get_lang('msg_ask_delete'); ?>',$('#delete_form'),'');
	}
	function gotoAudit(nns_id,value){
		$("#audit_form > #nns_id").val(nns_id);
		$("#audit").val(value);
		if (value==1){
			checkForm('<?php echo cms_get_lang('dbgl');?>','<?php echo cms_get_lang('msg_ask_audit'); ?>',$('#audit_form'),'');
	}else{
		checkForm('<?php echo cms_get_lang('dbgl');?>','<?php echo cms_get_lang('cancel|msg_ask_audit'); ?>',$('#audit_form'),'');
	}
	}

	function showCDN(id,type){
		window.parent.begin_show_cdn(id,type);
	}
	</script>
</head>

<body>

<div class="content">
	<div class="content_position"><?php echo cms_get_lang('dbgl');?> > <?php echo cms_get_lang('media_mtwjgl');?></div>
	<div class="content_table">

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td width="120"><?php echo cms_get_lang('media_mtid')?></td>
				<td><?php echo $nns_id;?></td>
			</tr>
			   <tr>
				<td width="120"><?php echo cms_get_lang('media_mtmc');?></td>
				<td><?php echo $vod_index_data["nns_name"];?></td>
			</tr>
			<tr>
				<td width="120"><?php echo cms_get_lang('media_mtlx');?></td>
				<td><?php if ($vod_index_data["nns_view_type"]==0){echo cms_get_lang('media_dy');}else {echo cms_get_lang('media_lxj');}?></td>
			</tr>
			<tr>
				<td width="120"><?php echo cms_get_lang('zdgl_zdscbj');?></td>
				<td><alt alt="<?php echo get_tag_detail_html($tar_str);?>"><?php echo $tar_str;?></alt>
				</td>
			</tr>

			<!---分页--->
<?php
	if (!empty($_COOKIE["page_max_num"])){
		$g_manager_list_max_num=$_COOKIE["page_max_num"];
	}
	$index_pages=ceil($vod_index_data["nns_all_index"]/$g_manager_list_max_num);
	$current_page=$_GET['page'];
	if(empty($current_page)){
		$current_page=1;
	}
	$begin_index=($current_page-1)*$g_manager_list_max_num+1;//开始的集数
	$end_index=$current_page*$g_manager_list_max_num; //结束的集数
	if($end_index>$vod_index_data["nns_all_index"]){
		$end_index=$vod_index_data["nns_all_index"];
	}
?>

<?php for($vod_current_index=$begin_index;$vod_current_index<=$end_index;$vod_current_index++){
	$vod_media_data=null;
	$num=0;
?>
			<tr>
				<td colspan="2"   style="padding:0px;"><div class="radiolist"  style="padding:3px 0px;"><div class="radiogroup"><h3><?php if ($view_type==0){ echo cms_get_lang('media_fp'). $vod_current_index;}else if($view_type==1){ echo cms_get_lang('media_js'). $vod_current_index;}?>
				&nbsp;&nbsp;&nbsp;&nbsp;<input name="" type="button" value="<?php echo cms_get_lang('media_control_tjmt');?>"
				onclick="self.location='nncms_content_media_addedit.php?view_type=<?php echo $view_type;?>&action=add&vod_id=<?php echo $nns_id;?>&index=<?php echo $vod_current_index-1;?>';"
				style="margin-top:-3px;"/></h3>

				</div></div></td>
			</tr>

<?php $vod_media_arr=$vod_index_inst->nns_db_vod_media_list(0,0,$nns_id,$vod_current_index-1);
if ($vod_media_arr["ret"]==0) $vod_media_data=$vod_media_arr["data"];

?>
			<tr>
				<td colspan="2">
				<div class="content_table formtable" style="padding:3px 10px;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th><?php echo cms_get_lang('segnumber');?></th>
							<th><?php echo cms_get_lang('name');?></th>
							<th><?php echo cms_get_lang('media_pydz');?></th>
							<th><?php echo cms_get_lang('media_pylx');?></th>
							<th><?php echo cms_get_lang('zdgl_zdscbj');?></th>
							<th><?php echo cms_get_lang('media_pygs');?></th>
							<th><?php echo cms_get_lang('media_pymtid');?></th>
							 <th><?php echo cms_get_lang('media_pyml');?></th>
							<th><?php echo cms_get_lang('media_sfdrmjm');?></th>
                                                         <th>影片展示方式</th>
							<th><?php echo cms_get_lang('action');?></th>
						</tr>
					</thead>
					<tbody>
<?php
if ($vod_media_data!=null){


	foreach($vod_media_data as $item){
		$num++;
?>
							  <tr>

								<td><?php echo $num;?></td>
								<td><?php echo $item["nns_name"];?></td>
								<td><?php echo $item["nns_url"];?></td>
								<td><?php echo get_contant_language($item["nns_mode"]);?></td>
								<td><alt alt="<?php echo get_tag_detail_html($item["nns_tag"]);?>"><?php echo $item["nns_tag"];?></alt></td>
								<td><?php echo $item["nns_filetype"];?></td>
								<td><?php echo $item["nns_content_id"];?></td>
								<td><?php echo $item["nns_kbps"];?></td>
								<td><?php if($item['nns_drm_enabled']){
										echo cms_get_lang('right');
										echo "({$item['nns_drm_encrypt_solution']})";
									}  else{
										echo cms_get_lang('fail');
									}
									?></td>
                                 <td> <?php echo get_dimensions_type($item['nns_dimensions']); ?> </td>
								<td class="control_btns" name="<?php echo cms_get_lang('action');?>">
								<a href="nncms_content_media_addedit.php?view_type=<?php echo $view_type;?>&action=edit&vod_id=<?php echo $nns_id;?>&index=<?php echo $vod_current_index-1;?>&nns_id=<?php echo $item["nns_id"];?>" target="_self"><?php echo cms_get_lang('edit');?></a>&nbsp;&nbsp;
								<a href="javascript:gotoDelete('<?php echo $item["nns_id"]; ?>');" target="_self"><?php echo cms_get_lang('delete');?></a>&nbsp;&nbsp;
								<a href="javascript:showCDN('<?php echo $item["nns_content_id"]; ?>',0);" target="_self"><?php echo cms_get_lang('contorl_ffzt');?></a>&nbsp;&nbsp;
<?php if (strtolower($item["nns_filetype"])=="flv"){
	$core_npss_url=explode(";",$g_core_npss_url);
	$flv_url="http://".$core_npss_url[0]."/nn_vod.flv?id=".$item["nns_content_id"]."&nst=iptv";
	$flv_url=urlencode($flv_url);
?>
									<a href="../../models/flashplayer/flashplayer.php?vod_url=<?php echo $flv_url;?>" target="_blank"><?php echo cms_get_lang('media_flv_play');?></a>&nbsp;&nbsp;
								<?php }?>
								<!--<?php if($item["nns_check"]==0){?>
								<a href="javascript:gotoAudit('<?php echo $item["nns_id"]; ?>',1);" target="_self" style="color:#ff0000;"><?php echo cms_get_lang('audit');?></a>&nbsp;&nbsp;
								<?php }else if($item["nns_check"]==1){?>
								<a href="javascript:gotoAudit('<?php echo $item["nns_id"]; ?>',0);" target="_self"><?php echo cms_get_lang('cancel|audit');?></a>&nbsp;&nbsp;
								<?php }?>-->
								</td>

							  </tr>
						  <?php }}?>
						  </tbody>
						</table>
					</div>
					</td>
			</tr>
			<?php }?>
		  </tbody>
		</table>
	</div>
	<form id="delete_form" action="nncms_content_media_control.php" method="post">
	  <input name="action" id="action" type="hidden" value="delete" />
	  <input name="nns_id" id="nns_id" type="hidden" value="" />
	  <input name="nns_vod_id" id="nns_vod_id" type="hidden" value="<?php echo $nns_id;?>" />

	 </form>
	<form id="audit_form" action="nncms_content_media_control.php" method="post">
	  <input name="action" id="action" type="hidden" value="audit" />
	  <input name="nns_id" id="nns_id" type="hidden" value="" />
	  <input name="audit" id="audit" type="hidden" value="" />
	  <input name="nns_vod_id" id="nns_vod_id" type="hidden" value="<?php echo $nns_id;?>" />
	 </form>


	 <div class="pagecontrol">
		<?php if ($current_page>1){?>
		<a href="nncms_content_media_list.php?page=1&nns_id=<?php echo $nns_id;?>" target="_self"><?php echo cms_get_lang('first_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="nncms_content_media_list.php?page=<?php echo $current_page-1;?>&nns_id=<?php echo $nns_id;?>" target="_self"><?php echo cms_get_lang('pre_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php }else{?>
			<span><?php echo cms_get_lang('first_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
			<span><?php echo cms_get_lang('pre_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php } if ($current_page<$index_pages){?>
		<a href="nncms_content_media_list.php?page=<?php echo $current_page+1;?>&nns_id=<?php echo $nns_id;?>" target="_self"><?php echo cms_get_lang('next_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="nncms_content_media_list.php?page=<?php echo $index_pages;?>&nns_id=<?php echo $nns_id;?>" target="_self"><?php echo cms_get_lang('last_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php }else{?>
			<span><?php echo cms_get_lang('next_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
			<span><?php echo cms_get_lang('last_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php }?>

		<?php echo cms_get_lang('jump_to');?> <input name="go_page_num" id="go_page_num" value="<?php echo $current_page;?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page');?> &nbsp;&nbsp;
		<a href="javascript:go_page_num('nncms_content_media_list.php?ran=1&nns_id=<?php echo $nns_id;?>',<?php echo $index_pages;?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
		<?php echo cms_get_lang('current');?><span style="font-weight:bold;color:#ff0000;"><?php echo $current_page."/".$index_pages;?></span><?php echo cms_get_lang('page');?>&nbsp;&nbsp;|&nbsp;&nbsp;
		<?php echo cms_get_lang('perpagenum');?>&nbsp;
		 <input name="nns_list_max_num" id="nns_list_max_num" type="text"
		 value="<?php echo $g_manager_list_max_num;?>" style="width:24px;"/>&nbsp;&nbsp;
		 <input type="button" value="<?php echo cms_get_lang('confirm');?>"
		 onclick="refresh_prepage_list();"/>&nbsp;&nbsp;
	</div>

	 <div class="controlbtns">

		<div class="controlbtn back"><a href="javascript:window.parent.refresh_tree_content();"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>
</body>
</html>
<?php }?>



