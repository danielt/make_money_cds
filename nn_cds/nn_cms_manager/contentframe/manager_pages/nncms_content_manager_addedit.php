<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
//加载多语言
include $nncms_config_path. 'nn_cms_config/nn_cms_global.php';
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$action=$_GET["action"];
$nns_id=$_GET["nns_id"];
$pri_bool=false;
if (!empty($action)){
	switch($action){
	case "edit":
		$action_str=cms_get_lang('save');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"102101");
		break;
	case "add":
		$action_str=cms_get_lang('add');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"102100");
		break;
	default:
		break;
	}
}
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");



include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include $nncms_db_path. "nns_role/nns_db_role_class.php";

// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");

if ($action=="edit"){
	include $nncms_db_path. "nns_manager/nns_db_manager_class.php";
	$manager_inst=new nns_db_manager_class();
	if (!empty($nns_id)){
		$manager_info=$manager_inst->nns_db_manager_info($nns_id);
	}else{
		echo "<script>alert('".$manager_info["reason"] ."');self.location='nncms_content_managerlist.php';</script>";
	}
	if ($manager_info["ret"]!=0){
		echo "<script>alert('".$manager_info["reason"] ."');self.location='nncms_content_managerlist.php';</script>";
	}
	$manager_inst=null;
	$edit_data=$manager_info["data"][0];
	//	$edit_pri=$edit_data["nns_pri_id"];
}

$role_inst=new nns_db_role_class();
$role_result=$role_inst->nns_db_role_list();
$role_inst=null;
if ($role_result["ret"]==0){
	$roles_data=$role_result["data"];
}else{
	echo "<script>alert('".$role_result["reason"] ."');self.location='nncms_content_managerlist.php';</script>";
}

include $nncms_db_path. "nns_carrier/nns_db_carrier_class.php";
include $nncms_db_path. "nns_partner/nns_db_partner_class.php";
include $nncms_db_path. "nns_group/nns_db_group_class.php";

$carrier_inst=new nns_db_carrier_class();
$carrier_result=$carrier_inst->nns_db_carrier_list();
$carrier_inst=null;
if ($carrier_result["ret"]==0){
	$carrier_data=$carrier_result["data"][0];
}
if ($action=="edit"){
	$partner_inst=new nns_db_partner_class();
	$partner_result=$partner_inst->nns_db_partner_info($edit_data["nns_org_id"]);
	$partner_inst=null;
	if ($partner_result["ret"]==0){
		$partner_data=$partner_result["data"][0];
	}

	$group_inst=new nns_db_group_class();
	$group_result=$group_inst->nns_db_group_info($edit_data["nns_org_id"]);
	$group_inst=null;
	if ($group_result["ret"]==0){
		$group_data=$group_result["data"][0];
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/radiolist.js"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
<script language="javascript">
$(document).ready(function(){
	$(".selectbox").hide();

	var $obj=$("#nns_carrier").find("option:selected");
	var index=$("#nns_carrier option").index($obj);
	selectCarrier(index);
	$("#nns_carrier").change(function(){
		var $obj=$(this).find("option:selected");
		var index=$("#nns_carrier option").index($obj);
		selectCarrier(index);
	});

});
function selectCarrier(num){

	$("#nns_partner_control").hide();
	$("#nns_group_control").hide();
	$("#nns_group_input").attr("rule","");
	$("#nns_partner_input").attr("rule","");

	var val=$("#nns_carrier option:eq("+num+")").attr("value");
	switch(val){
	case "1":
		$("#nns_partner_input").attr("rule","noempty");
		$("#nns_partner_control").show();
		break;
	case "2":
		$("#nns_group_input").attr("rule","noempty");
		$("#nns_group_control").show();
		break;
	}
}

function begin_select_partner(){
	$("#select_frame").attr("src","../../controls/nncms_controls_select.php?method=partner");
	$(".selectbox").show();
}

function begin_select_group(){
	$("#select_frame").attr("src","../../controls/nncms_controls_select.php?method=group");
	$(".selectbox").show();
}
function close_select(){
	$(".selectbox").hide();
}

function set_partner_id(value,name){
	$("#nns_partner").attr("value",value);
	$("#nns_partner_input").attr("value",name);
}

function set_group_id(value,name){
	$("#nns_group").attr("value",value);
	$("#nns_group_input").attr("value",name);
}

</script>
</head>

<body>
<div class="selectbox">
		<iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
 </div>



<div class="content">
	<div class="content_position"><?php echo cms_get_lang('xtgl');?> > <?php echo $language_manager_xtgl_glygl;?></div>

	<form id="add_form" action="nncms_content_manager_control.php" method="post">
	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
	<input name="nns_partner" id="nns_partner" type="hidden" value="<?php echo $edit_data["nns_org_id"];?>" />
	<input name="nns_group" id="nns_group" type="hidden" value="<?php echo $edit_data["nns_org_id"];?>" />
	<?php if ($action=="edit"){?>
	<input name="nns_id" id="nns_id" type="hidden" value="<?php echo $nns_id;?>"/>
	<?php } ?>
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">

		<tbody>
			 <tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('manager_id');?>:</td>
				<td><input name="nns_manager_id" id="nns_manager_id" type="text" rule="noempty"
					value="<?php if($action=="edit"){echo $edit_data["nns_id"];}?>" <?php if($action=="edit"){echo "disabled";}?>
				/></td>

			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('manager_name');?>:</td>
				<td><input name="nns_manager_name" id="nns_manager_name" type="text" rule="noempty"
					value="<?php if($action=="edit"){echo $edit_data["nns_name"];}?>"
				/></td>

			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('pass');?>:</td>
				<td><input name="nns_manager_pass" id="nns_manager_pass" type="password" rule="noempty"
					value=""
				/></td>

			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('pass_confirm');?>:</td>
				<td><input name="nns_manager_pass_again" id="nns_manager_pass_again" type="password" rule="noempty" sameto="nns_manager_pass"
					value=""
				/> </td>

			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('edit_role');?>:</td>
				<td><select name="nns_role_id" id="nns_role_id" rule="noempty">
					<?php foreach($roles_data as $role_item){?>
						<option value="<?php echo $role_item["nns_id"];?>"  <?php if($action=="edit" && $edit_data["nns_role_id"]==$role_item["nns_id"]){?> selected="selected"<?php }?> ><?php echo $role_item["nns_name"];?></option>
					<?php }?>
					</select>
				</td>
			</tr>
			<tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('group');?>:</td>
				<td><select name="nns_carrier" id="nns_carrier" rule="noempty"  style="margin:5px 0px;"><br>
						<option value="<?php echo $carrier_data["nns_id"];?>" <?php if ($action=="edit" && $edit_data["nns_type"]==0){?>selected="selected"<?php }?>><?php echo cms_get_lang('xtgl_yysgl')."/" . $carrier_data["nns_name"];?></option>
						<?php if ($g_partner_enabled==1){?>
						<option value="1" <?php if ($action=="edit" && $edit_data["nns_type"]==1){?>selected="selected"<?php }?>><?php echo cms_get_lang('partner');?></option>
						<?php }if ($g_group_enabled==1){?>
						<option value="2" <?php if ($action=="edit" && $edit_data["nns_type"]==2){?>selected="selected"<?php }?>><?php echo cms_get_lang('group_jtxx');?></option>
						<?php }?>
					</select>
					<?php if ($g_partner_enabled==1){?>
						<div id="nns_partner_control">
						<input name="nns_partner_input" id="nns_partner_input" type="text" value="<?php echo $partner_data["nns_name"];?>" style="width:30%;" readonly="true" disabled="false"/>&nbsp;&nbsp;<input onclick="begin_select_partner();" name="" type="button" value="<?php echo cms_get_lang('search'). cms_get_lang('xtgl_yysgl')?>"/>
						 </div>
					<?php } if ($g_group_enabled==1){?>
						<div id="nns_group_control">
						<input name="nns_group_input" id="nns_group_input" type="text" value="<?php echo $group_data["nns_name"];?>" style="width:30%;" readonly="true"  disabled="false"/>&nbsp;&nbsp;<input name="" type="button" value="<?php echo cms_get_lang('search'). cms_get_lang('group_jtxx')?>" onclick="begin_select_group();"/>
						 </div>
					<?php }?>
				</td>
			</tr>
		  </tbody>
		</table>
	</div>
	</form>
	<div class="controlbtns">
		<div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_glygl');?>','<?php echo cms_get_lang('msg_ask_change'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>





</body>
</html>
