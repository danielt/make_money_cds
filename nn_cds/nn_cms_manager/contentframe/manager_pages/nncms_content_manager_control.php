<?php
/*
 * Created on 2012-3-2
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 @header("Content-Type:text/html;charset=utf-8");
 include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
require_once $nncms_db_path. "nns_log".DIRECTORY_SEPARATOR . "nns_db_op_log_class.php";
$log_inst=new nns_db_op_log_class();

$action=$_POST["action"];
	 $nns_id=$_POST["nns_id"];
	 $nns_manager_name=$_POST["nns_manager_name"];
	 $nns_manager_id=$_POST["nns_manager_id"];
	 $nns_manager_pass=$_POST["nns_manager_pass"];
	 $nns_role_id=$_POST["nns_role_id"];
	 $nns_carrier=$_POST["nns_carrier"];
	 $nns_partner=$_POST["nns_partner"];
	 $nns_group=$_POST["nns_group"];

 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();

$pri_bool=false;
if (!empty($action)){
	switch($action){
		case "edit":
		$action_str=$language_action_edit. $language_manager_manager_glyxx;
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"102101");
		break;
		case "add":
		$action_str=$language_action_add. $language_manager_manager_glyxx;
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"102100");
		break;
		case "delete":
		$action_str=$language_action_delete. $language_manager_manager_glyxx;
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"102102");
		break;
		default:
		break;
	}
}
$checkpri=null;
if (!$pri_bool){
 Header("Location: ../nncms_content_wrong.php");

}else{

	 if ($nns_carrier==1){
	 	$nns_type=1;
	 	$nns_partner_id=$nns_partner;
	 }else if($nns_carrier==2){
	 	$nns_type=2;
	 	$nns_partner_id=$nns_group;
	 }else{
	 	$nns_type=0;
	 	$nns_partner_id=$nns_carrier;
	 }
	include $nncms_db_path. "nns_manager/nns_db_manager_class.php";
	$manager_inst=new nns_db_manager_class();
	switch($action){
		case "edit":
		$result=$manager_inst->nns_db_manager_modify($nns_id,$nns_manager_name,$nns_manager_pass,$nns_type,$nns_partner_id,$nns_role_id);
		if ($result["ret"]!=0){
			echo "<script>alert('". $action_str. $language_action_fault. "');</script>";
		}else{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$nns_id,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			echo "<script>alert('". $action_str. $language_action_success. "');</script>";
		}
		break;
		case "add":
		$result=$manager_inst->nns_db_manager_add($nns_manager_id,$nns_manager_name,$nns_manager_pass,$nns_type,$nns_partner_id,$nns_role_id);
		
		//print_r($result);
		//die;
		if ($result["ret"]!=0){

	//		echo $result["reason"];
			echo "<script>alert('". $action_str. $language_action_fault. "');</script>";
		}else{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$nns_id,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			echo "<script>alert('". $action_str. $language_action_success. "');</script>";
		}
//		var_dump($result);
		break;
		case "delete":
		$delete_bool=true;
	//	$role_inst->
			$nns_ids=explode(",",$nns_id);
			$num=0;
			foreach($nns_ids as $nnsid){
				if (!empty($nnsid)){
					$num++;
					$resultarr=$manager_inst->nns_db_manager_delete($nnsid);
					if ($resultarr["ret"]!=0 && $num==1){
						$delete_bool=false;
						break;
					}
				}
			}
		if (!$delete_bool){
			echo "<script>alert('". $action_str. $language_action_fault. "');</script>";
		}else{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".rtrim($nns_id,','),$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			echo "<script>alert('". $action_str. $language_action_success. "');</script>";
		}
		break;
		default:

		break;
	}
	$manager_inst=null;
	//var_dump($result);
	$log_inst=null;
	echo "<script>self.location='nncms_content_managerlist.php';</script>";
}
?>
