<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}

//加载多语言
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

include LOGIC_DIR.'video'.DIRECTORY_SEPARATOR.'live_index.class.php';
$dc = nl_get_dc(array (
	'db_policy' => NL_DB_WRITE,
	'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$dc->open(); 
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";
require_once $nncms_config_path. "nn_cms_manager/controls/nncms_controls_clear_cache.php"; 
include_once $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php"; 

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();

$nns_id=$_GET["nns_id"];
$index=$_GET["index"];
$pri_bool=false;
$pri_bool1=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107101");
$pri_bool2=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107100");
if ($pri_bool1 || $pri_bool2) $pri_bool=true;
$checkpri=null;



if (!$pri_bool){
	Header("Location: ../nncms_content_wrong.php");
}else{

	include ($nncms_db_path. "nns_common/nns_db_constant.php");
	require_once $nncms_config_path. "nn_cms_manager/controls/nncms_control_memcache.php";
	$nns_id_post=$_POST["nns_id"];
	$index_post=$_POST["index"];
	$nns_live_index_name=$_POST["nns_live_index_name"];
	$nns_live_index_summary=$_POST["nns_live_index_summary"];
	$nns_live_index_time_len=$_POST["nns_live_index_time_len"];
	$nns_live_index_time_len=$nns_live_index_time_len*60;
	// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
	// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");
	include $nncms_db_path. "nns_live/nns_db_live_index_class.php";
	$live_inst=new nns_db_live_index_class();
	$live_info=$live_inst->nns_db_live_index_info($nns_id,$index);
	$edit_data=$live_info["data"][0];
	require_once $nncms_db_path. "nns_log".DIRECTORY_SEPARATOR . "nns_db_op_log_class.php";
	$log_inst=new nns_db_op_log_class();
	$nns_name=$log_inst->nns_db_get_video_name_arr($nns_id,1);
	if (!empty($nns_id_post)){


		$img_old_url=$edit_data["nns_image"];

		$imgurl=pub_func_image_upload_by_live_index($_FILES['index_image'],$nns_id_post,$index_post);


		if (!empty($imgurl)){
			pub_func_image_delete($img_old_url);
		}else{
			$imgurl=$img_old_url;
		}

		$nns_name=$log_inst->nns_db_get_video_name_arr($nns_id_post,1);

		$live_result=$live_inst->nns_db_live_index_modify($nns_id_post,$index_post,$nns_live_index_name,$nns_live_index_time_len,$imgurl,$nns_live_index_summary);
		$live_inst=null;
		if ($live_result["ret"]==0){
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],"edit",$_SESSION["nns_mgr_name"].$language_media_control_bjjj.":".$nns_name.$language_media_js.($index_post+1),$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			$log_inst=null;
			nl_live_index::delete_live_index_by_cache($dc,$nns_id_post);
			delete_cache("detail_$nns_id_post");
			echo "<script>self.location='nncms_content_live_media_list.php?nns_id=".$nns_id_post."';</script>";
		}else{
			echo "<script>alert('".$live_result["reason"] ."');self.location='nncms_content_live_media_list.php?nns_id=".$nns_id_post."';</script>";
		}
	}


	//		$live_inst=null;
	//	$edit_pri=$edit_data["nns_pri_id"];


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>

</head>

<body>

<div class="content">
	<div class="content_position"><?php echo cms_get_lang('media_zbgl');?> > <?php echo cms_get_lang('media_control_bjzbjj');?></div>
	<form action="nncms_content_live_media_intor.php" id="add_form" method="post" enctype="multipart/form-data">
	<input name="nns_id" id="nns_id" type="hidden" value="<?php echo $nns_id;?>"/>
	<input name="index" id="index" type="hidden" value="<?php echo $index;?>"/>
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td width="120"><?php echo cms_get_lang('media_mtid');?>:</td>
				<td><?php echo $nns_id;?></td>

			</tr>
			<tr>
				<td width="120"><?php echo cms_get_lang('media_mtmc');?>:</td>
				<td><?php echo $nns_name;?></td>
			</tr>
			<tr>
				<td colspan="2"   style="padding:0px;"><div class="radiolist"  style="padding:3px 0px;"><div class="radiogroup">
				<h3><?php echo cms_get_lang('media_zb');?></h3>
				</div></div></td>
			</tr>
			  <tr>
				<td width="120"><?php echo cms_get_lang('name');?>:</td>
				<td><input name="nns_live_index_name" id="nns_live_index_name" type="text" size="60%" value="<?php echo $edit_data["nns_name"];?>"/></td>
			</tr>
			<tr>
				<td width="120"><?php echo cms_get_lang('media_index_sc');?>:</td>
				<td><input name="nns_live_index_time_len" id="nns_live_index_time_len" type="text" size="60%" value="<?php echo $edit_data["nns_time_len"]/60;?>" rule="int"/></td>
			</tr>
			<tr>
				<td width="120"><?php echo cms_get_lang('media_bjhb');?>:</td>
				<td>
					<div class="img_content">
					 <?php if (!empty($edit_data["nns_image"])){?>
						<div class="img_view"><h4><?php echo cms_get_lang('media_bjhb');?></h4><a href="<?php echo pub_func_get_image_url($edit_data["nns_image"]);?>?rand=<?php rand();?>" target="_blank">
						<img src="<?php echo pub_func_get_image_url($edit_data["nns_image"]);?>?rand=<?php rand();?>" style="border:none;" /></a></div>
					<?php }?>
						<div class="img_edit"> <input name="index_image" id="index_image" type="file" size="60%"/></div>
						<div style="clear:both;"/>
					</div>

			   </td>

			</tr>
			<tr>
				<td width="120"><?php echo cms_get_lang('media_index_jj');?>:</td>
				<td><textarea name="nns_live_index_summary" id="nns_live_index_summary" cols="" rows=""><?php echo $edit_data["nns_summary"];?></textarea></td>

			</tr>





		  </tbody>
		</table>
	</div>
	</form>
	 <div class="controlbtns">
		<div class="controlbtn edit"><a href="javascript:checkForm("<?php echo cms_get_lang('media_zbgl');?>","<?php echo cms_get_lang('meg_ask_edit'); ?>",$('#add_form'),'<?php echo $action; ?>');"><?php echo cms_get_lang('confirm'); ?></a></div>
		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>





</body>
</html>
<script language="javascript" src="../../js/image_loaded_func.js"></script>
<?php }?>