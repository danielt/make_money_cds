<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
ob_start();
include("../../nncms_manager_inc.php");
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}

//加载多语言
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');

//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";
include $nncms_config_path . "nn_cms_manager/languages/language_contant.php";

include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/service_type/service_type.class.php';
$dc = nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));

$result_service_type = nl_service_type::query_by_video_type($dc,'1');
$arr_service_type = null;
if(isset($result_service_type['data_info']) && is_array($result_service_type['data_info']))
{
    foreach ($result_service_type['data_info'] as $service_type_val)
    {
        $arr_service_type[$service_type_val['nns_service_type']]=$service_type_val['nns_service_type'].'/'.$service_type_val['nns_service_name'];
    }
}
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$action = $_GET["action"];
$nns_id = $_GET["nns_id"];
$live_id = $_GET["live_id"];
$index = $_GET["index"];
$pri_bool = false;
if (!empty($action)) {
        switch ($action) {
                case "edit":
                        $action_str = cms_get_lang('edit');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107101");
                        break;
                case "add":
                        $action_str = cms_get_lang('add');
                        $pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "107100");
                        break;
                default:
                        break;
        }
}
if ($pri_bool1 || $pri_bool2)
        $pri_bool = true;
$checkpri = null;



if (!$pri_bool) {
        Header("Location: ../nncms_content_wrong.php");
} else {
    $cp_list = nl_cp::query_all($dc);
        include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
        include ($nncms_db_path . "nns_common/nns_db_constant.php");
        // echo $nncms_db_path. "nns_role/nns_db_role_class.php";
        // include($nncms_db_path. "nns_pri/nns_db_pri_class.php");

        include $nncms_db_path . "nns_live/nns_db_live_class.php";
        $live_inst = new nns_db_live_class();
        $live_index_count_arr = $live_inst->nns_db_live_info($live_id, "*");
        $live_inst = null;
        if ($live_index_count_arr["ret"] == 0)
                $live_index_data = $live_index_count_arr["data"][0];
        $nns_name = $live_index_data["nns_name"];
        $tar_str = ltrim($live_index_data['nns_tag'], ',');

        require_once $nncms_db_path . "nns_live" . DIRECTORY_SEPARATOR . "nns_db_live_index_class.php";
        $live_index_inst = new nns_db_live_index_class();
        $index_result = $live_index_inst->nns_db_live_index_info($live_id, $index);
        $live_index_inst = null;
        if ($index_result["ret"] == 0) {
                $index_name = $index_result["data"][0]["nns_name"];
        }
        if (empty($index_name)) {
                $index_name = $nns_name;
                if ($live_index_data["nns_all_index"] > 1) {
                        $index_name.="-" . ($index + 1);
                }
        }

        require_once $nncms_db_path . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_curl_content_class.php";

        //------core start-----------
        //从config 中获取http——url
        $http_url = $g_core_url;
        //通过CURL抓取返回值
        $curl_post = 'func=get_cdn_policy_list';
        $data = nns_db_curl_content_class::nns_db_curl_content($http_url, $curl_post);
        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->loadXML($data);

        ob_end_clean();
        if ($action == "edit") {
                include $nncms_db_path . "nns_live/nns_db_live_media_class.php";
                $live_inst = new nns_db_live_media_class();
                if (!empty($nns_id)) {
                        $live_info = $live_inst->nns_db_live_media_info($nns_id);
                } else {
                        echo "<script>alert('" . $live_info["reason"] . "');self.location='nncms_content_live_media_list.php';</script>";
                }
                if ($live_info["ret"] != 0) {
                        echo "<script>alert('" . $live_info["reason"] . "');self.location='nncms_content_live_media_list.php';</script>";
                }
                //	$live_info=null;
                $edit_data = $live_info["data"][0];
                $nns_tags_arr = explode(",", $edit_data["nns_tag"]);
                unset($nns_tags_arr[count($nns_tags_arr) - 1]);
                //	$edit_pri=$edit_data["nns_pri_id"];
        }
        global  $g_global_domain;
        $arr_global_domain = isset($g_global_domain) ? $g_global_domain : null;
        unset($g_global_domain);
        ?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title></title>
                        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                        <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                        <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
                        <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                        <script language="javascript" src="../../js/table.js.php"></script>
                        <script language="javascript" src="../../js/checkinput.js.php"></script>
                        <script language="javascript" src="../../js/radiolist.js"></script>
                        <script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
                        <script language="javascript">
                        $(document).ready(function() {
                        	$(".addLive").hide();
                        	$("#insert_method").change(function(){
                                			if ($(this).val()==0){
                                				$(".addLive").hide();
                                			}else if($(this).val()==1){
                                				$(".addLive").show();
                                			}
                                		});
                        });
        <?php if ($action == "add") { ?>

                                        $(document).ready(function() {
                                        	$(".addLive").hide();
                                                //		$("#media_id_tr").hide();
                                                //		$("#insert_method").change(function(){
                                                //			if ($(this).val()==0){
                                                //				$("#media_id_tr").hide();
                                                //				$("#media_name_tr").show();
                                                //				$("#media_type_tr").show();
                                                //				$("#media_policy_tr").show();
                                                //				$("#media_url_tr").show();
                                                //				$("#nns_media_url").attr("rule","noempty");
                                                //				$("#media_gs_tr").show();
                                                //			}else if($(this).val()==1){
                                                //				$("#media_id_tr").show();
                                                //				$("#media_name_tr").hide();
                                                //				$("#media_type_tr").hide();
                                                //				$("#media_policy_tr").hide();
                                                //				$("#media_url_tr").hide();
                                                //				$("#nns_media_url").attr("rule","");
                                                //				$("#media_gs_tr").hide();
                                                //			}
                                                //		});
                                                $(".radioitem input:eq(0)").attr("checked", true);
                                                $("#media_id_tr").show();
                                                $("#media_name_tr").show();
                                                $("#media_type_tr").show();
//                                                 $("#media_policy_tr").hide();
                                                //$("#media_url_tr").hide();
                                                //$("#nns_media_url").attr("rule", "");
//                                                 $("#media_gs_tr").hide();
                                        });
        <?php } ?>
                                function check_submit() {
                                        var tag_value = getCheckedData();
                                        if (tag_value == "") {
                                                alert("<?php echo cms_get_lang('select_epg_output_flag'); ?>");
                                                return;
                                        }

                                        if ($("#live_mode_1").attr("checked") || $("#live_mode_2").attr("checked") || $("#live_mode_3").attr("checked"))  {
                                                $('#nns_tags').attr('value', tag_value);
                                                checkForm("<?php echo cms_get_lang('media_zbgl'); ?>", "<?php echo cms_get_lang('msg_ask_change'); ?>", $('#add_form'), '<?php echo $action; ?>');
                                        } else {
                                                alert("<?php echo cms_get_lang('media_choose_py_type'); ?>");
                                        }
                                }
                        </script>
                </head>

                <body>
                        <div class="selectbox" id="selectbox2">
                                <iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
                        </div>
                        <div class="content">
                                <div class="content_position"><?php echo cms_get_lang('media_zbgl'); ?> > <?php echo cms_get_lang('media_zbygl'); ?></div>
                                <form id="add_form" action="nncms_content_live_media_control.php" method="post">
                                        <input name="action" id="action" type="hidden" value="<?php echo $action; ?>" />
                                        <input name="nns_tags" id="nns_tags" type="hidden" value="" />
                                        <input name="nns_live_id" id="nns_live_id" type="hidden" value="<?php echo $live_id; ?>" />
                                        <input name="nns_index" id="nns_index" type="hidden" value="<?php echo $index; ?>" />
                                        <?php if ($action == "edit") { ?>
                                                <input name="nns_id" id="nns_id" type="hidden" value="<?php echo $nns_id; ?>"/>
                                        <?php } ?>
                                        <div class="content_table">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                                <tr>
                                                                        <td width="120"><?php echo cms_get_lang('media_mtid'); ?>:</td>
                                                                        <td><?php echo $live_id; ?></td>

                                                                </tr>
                                                                <tr>
                                                                        <td width="120"><?php echo cms_get_lang('media_zbymc'); ?>:</td>
                                                                        <td><?php echo $nns_name; ?></td>
                                                                </tr>
                                                                <tr>
                                                                        <td width="120"><?php echo cms_get_lang('zdgl_zdscbj'); ?>:</td>
                                                                        <td><alt alt="<?php echo get_tag_detail_html($tar_str); ?>"><?php echo $tar_str; ?></alt>
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                        <td colspan="2"   style="padding:0px;"><div class="radiolist"  style="padding:3px 0px;"><div class="radiogroup">
                                                                                                <h3><?php echo cms_get_lang('media_zb'); ?></h3>
                                                                                        </div></div></td>
                                                                </tr>
                                                                <!--直播只绑定添加片源 BY S67 2012-8-21-->
                                                                <?php //if ($action == "add") { ?>
                                                                        <tr>
                                                                                <td width="120"><?php echo cms_get_lang('media_tjpyfs'); ?>:</td>
                                                                                <td><select name="insert_method" id="insert_method" rule="">
                                                                                                <option value="0"><?php echo cms_get_lang('media_new_media'); ?></option>
                                                                                                <option value="1"><?php echo cms_get_lang('media_exist_media'); ?></option>
                                                                                        </select> </td>

                                                                        </tr>
                                                                <?php //} ?>
                                                                <!--
                                                                <?php if(isset($cp_list['data_info']) && is_array($cp_list['data_info']) && !empty($cp_list['data_info'])){?>
                                                                        <tr>
                                                                            <td width="120">选择CP</td>
                                                                            <td>
                                                                                <select name="nns_cp_id" id="insert_method" rule="">
                                                                                    <?php foreach ($cp_list['data_info'] as $cp_list_val){?>    
                                                                                    <option value="<?php echo $cp_list_val['nns_id']?>" ><?php echo $cp_list_val['nns_name']; ?></option>
                                                                                    <?php }?>
                                                                                </select> 
                                                                            </td>
                                                                        </tr>
                                                                <?php }else{?>
                                                                        <option value="0" select="selected"><?php "视达科"; ?></option>
                                                                <?php }?>
                                                                  -->
                                                                <!--END-->
                                                                <tr id="media_name_tr">
                                                                        <td width="120"><?php echo cms_get_lang('media_pymc'); ?>:</td>
                                                                        <td><input name="nns_media_name" id="nns_media_name" type="text"
                                                                                   value="<?php
                                                                                   if ($action == "edit") {
                                                                                           echo $edit_data["nns_name"];
                                                                                   } else {
                                                                                           echo $index_name;
                                                                                   }
                                                                                   ?>"
                                                                                   /> </td>

                                                                </tr>


                                                                <tr>
                                                                        <td width="120"><?php echo cms_get_lang('zdgl_zdscbj'); ?>:</td>
                                                                        <td>
                                                                                <div class="radiolist">

                                                                                        <div class="radiogroup">
                                                                                                <?php if ($g_mode != "xhpm") { ?>
                                                                                                        <div class="radioitem" >
                                                                                                                <?php echo get_tag_input_box(NNS_PLAYER_STD_TAG, $edit_data["nns_tag"] . ','); ?>
                                                                                                        </div>
                                                                                                        <div class="radioitem" >
                                                                                                                <?php echo get_tag_input_box(NNS_PC_TAG, $edit_data["nns_tag"] . ','); ?>
                                                                                                        </div>
                                                                                                        <div class="radioitem" >
                                                                                                                <?php echo get_tag_input_box(NNS_ANDROID_PAD_TAG, $edit_data["nns_tag"] . ','); ?>
                                                                                                        </div>
                                                                                                        <div class="radioitem" >
                                                                                                                <?php echo get_tag_input_box(NNS_ANDROID_PHONE_TAG, $edit_data["nns_tag"] . ','); ?>
                                                                                                        </div>
                                                                                                        <div class="radioitem" >
                                                                                                                <?php echo get_tag_input_box(NNS_APPLE_PAD_TAG, $edit_data["nns_tag"] . ','); ?>
                                                                                                        </div>
                                                                                                        <div class="radioitem" >
                                                                                                                <?php echo get_tag_input_box(NNS_APPLE_PHONE_TAG, $edit_data["nns_tag"] . ','); ?>
                                                                                                        </div>

                                                                                                <?php } ?>
                                                                                                <?php for ($device_num = 0; $device_num < $g_manager_list_device_num; $device_num++) { ?>
                                                                                                        <div class="radioitem" >
                                                                                                                <?php echo get_tag_input_box($device_num, $edit_data["nns_tag"] . ','); ?>

                                                                                                        </div>
                                                                                                <?php } ?>
                                                                                                <div style="clear:both;"/>
                                                                                        </div>
                                                                                </div>
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                        <td width="120"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('media_pylx'); ?> :</td>
                                                                        <td>

                                                                                &nbsp;&nbsp;&nbsp;<input type="checkbox" value="LIVE" name="nns_media_caps[]" id="live_mode_1" <?php
                                                                                if ($action == "add" || ($action == "edit" && strstr($edit_data["nns_media_caps"], 'LIVE'))) {
                                                                                        echo 'checked';
                                                                                }
                                                                                ?> /> <?php echo cms_get_lang('media_ontime_live'); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <input type="checkbox" value="TSTV" name="nns_media_caps[]" id="live_mode_2"  <?php
                                                                                if ($action == "edit" && strstr($edit_data["nns_media_caps"], 'TSTV')) {
                                                                                        echo 'checked';
                                                                                }
                                                                                ?> /> <?php echo cms_get_lang('media_sytstv'); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																				<input type="checkbox" value="PLAYBACK" name="nns_media_caps[]" id="live_mode_3"  <?php
																				if ($action == "edit" && strstr($edit_data["nns_media_caps"], 'PLAYBACK')) {
																					echo 'checked';
																				}
																				?> /> <?php echo cms_get_lang('media_playback_pb'); ?>
                                                                        </td>
                                                                </tr>
                                                                <tr  id="media_id_tr">
                                                                        <td width="120"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('media_pymtid'); ?>:</td>
                                                                        <td>
                                                                                <input name="nns_media_content_id" id="nns_media_content_id" type="text"
                                                                                       value="<?php
                                                                                       if ($action == "edit") {
                                                                                               echo $edit_data["nns_content_id"];
                                                                                       }
                                                                                       ?>" <?php
                                                                                       if ($action == "edit") {
                                                                                               //echo "readonly='readonly' style='background:#F3F3F3; border:1px solid #D1D1D1; color:#8E8E8E;'";
                                                                                       }
                                                                                       ?>
                                                                                       /><input type="button" class="addLive"  value="添加直播ID" />
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                                <td width="120">直播方式:</td>
                                                                                <td><select name="nns_cast_type" id="nns_cast_type" rule="">
                                                                                                <option value="0" <?php  if ($action == "edit" && $edit_data["nns_cast_type"] != '0'){  echo 'selected'; }?>>单播</option>
                                                                                                <option value="1" <?php  if ($action == "edit" && $edit_data["nns_cast_type"] == '1'){  echo 'selected'; }?>>组播</option>
                                                                                        </select> </td>

                                                                        </tr>
                                                                <tr id="media_url_tr">
                                                                        <td width="120"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('media_pydz'); ?>:</td>
                                                                        <td>
                                                                                <input name="nns_media_url" id="nns_media_url" type="text"
                                                                                       value="<?php
                                                                                       if ($action == "edit") {
                                                                                               echo $edit_data["nns_url"];
                                                                                       }
                                                                                       ?>" <?php
                                                                                       if ($action == "edit") {
                                                                                               //echo "readonly='readonly' style='background:#F3F3F3; border:1px solid #D1D1D1; color:#8E8E8E;'";
                                                                                       }
                                                                                       ?>
                                                                                       />
                                                                        </td>
                                                                </tr>
                                                                <tr id="media_type_tr">
                                                                        <td width="120"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('media_pylx'); ?>:</td>
                                                                        <td>

                                                                                <select name="nns_media_mode" id="nns_media_mode" rule="noempty">
                                                                                        <?php foreach ($nns_vod_mode as $nns_live_mode_item_key => $nns_live_mode_item_value) { ?>
                                                                                                <option value="<?php echo $nns_live_mode_item_value; ?>"  <?php if ($action == "edit" && $edit_data["nns_mode"] == $nns_live_mode_item_value) { ?>selected="selected"<?php } ?>><?php echo get_contant_language($nns_live_mode_item_value); ?></option>
                                                                                        <?php } ?>
                                                                                </select>
                                                                        </td>
                                                                </tr>
                                                                <tr id="media_gs_tr">
                                                                        <td width="120"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('media_pygs'); ?>:</td>
                                                                        <td>

                                                                                <select name="nns_media_type" id="nns_media_type" rule="noempty" <?php
                                                                                if ($action == "edit") {
                                                                                        //echo "readonly style='background:#F3F3F3; border:1px solid #D1D1D1; color:#8E8E8E;'";
                                                                                }
                                                                                ?>>
                                                                                                <?php foreach ($nns_media_type as $nns_media_type_item_key => $nns_media_type_item_value) { ?>
                                                                                                <option value="<?php echo $nns_media_type_item_value; ?>"  <?php if ($action == "edit" && $edit_data["nns_filetype"] == $nns_media_type_item_value) { ?>selected="selected"<?php } ?>><?php echo $nns_media_type_item_value; ?></option>
                                                                                        <?php } ?>
                                                                                </select>
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="120">DOMAIN域</td>
                                                                    <td>
                                                                        <select name="nns_domain" id="nns_domain" rule="">
                                                                            <option value=''>--请选择domain--</option>
                                                                            <?php if(is_array($arr_global_domain) && !empty($arr_global_domain)){foreach ($arr_global_domain as $domain_key=>$domain_val){?>    
                                                                            <option value="<?php echo $domain_key?>" <?php if($action == "edit" && $edit_data["nns_domain"] == $domain_key){ echo 'selected="selected"';}?>><?php echo $domain_val; ?></option>
                                                                            <?php }}?>
                                                                        </select> 
                                                                    </td>
                                                                </tr>
                                                                
                                                                <tr>
                                                                    <td width="120">片源服务类型</td>
                                                                    <td>
                                                                        <select name="nns_media_service" id="nns_media_service" rule="">
                                                                            <option value=''>--请选择片源服务类型--</option>
                                                                            <?php if(is_array($arr_service_type) && !empty($arr_service_type))
                                                                            {
                                                                                foreach ($arr_service_type as $service_type_key=>$service_type_val)
                                                                                {?>    
                                                                            <option value="<?php echo $service_type_key?>" <?php if($action == "edit" && $edit_data["nns_media_service"] == $service_type_key){ echo 'selected="selected"';}?>><?php echo $service_type_val; ?></option>
                                                                            <?php 
                                                                                }
                                                                            }?>
                                                                        </select> 
                                                                    </td>
                                                                </tr>
                                                                <tr id="media_url_tr">
                                                                    <td width="120"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>片源码率(kbps)： </td>
                                                                    <td>
                                                                        <input name="nns_kbps" id="nns_kbps" type="text"
                                                                               value="<?php
                                                                               if ($action == "edit") {
                                                                                   echo $edit_data["nns_kbps"];
                                                                               }
                                                                               ?>"
                                                                        />
                                                                    </td>
                                                                </tr>

                                                                <tr  id="media_policy_tr">
                                                                        <td width="120"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('media_bdcl'); ?>:</td>
                                                                        <td>

                                                                                <select name="nns_media_policy" id="nns_media_policy" <?php
                                                                                if ($action == "edit") {
                                                                                        //echo "readonly='readonly' onchange='javascript:this.selectedIndex=0' style='background:#F3F3F3; border:1px solid #D1D1D1; color:#8E8E8E;'";
                                                                                }
                                                                                ?>>

                                                                                        <?php
                                                                                        $policy = $dom->getElementsByTagName("cdn_policy");
                                                                                        foreach ($policy as $policy_item) {
                                                                                                ?>
                                                                                                <option value="<?php echo $policy_item->getAttribute("id"); ?>"  <?php if ($action == "edit" && $edit_data["nns_filetype"] == $policy_item->getAttribute("id")) { ?>selected="selected"<?php } ?>><?php echo $policy_item->getAttribute("id") . "/" . $policy_item->getAttribute("name"); ?></option>
                                                                                        <?php } ?>
                                                                                </select>
                                                                        </td>
                                                                </tr>
                                                                <tr <?php
                                                                if ($g_video_stream == 0) {
                                                                        echo "style='display:none;'";
                                                                }
                                                                ?>>
                                                                        <td width="120"><?php echo cms_get_lang('media_pyml'); ?>:</td>
                                                                        <td>
                                                                                <input name="nns_media_stream" id="nns_media_stream" type="text" rule="int"
                                                                                       value="<?php
                                                                                       if ($action == "edit") {
                                                                                               echo $edit_data["nns_kbps"];
                                                                                       } else {
                                                                                               echo "0";
                                                                                       }
                                                                                       ?>"
                                                                                       />
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                        <td width="120">时移时长(单位分钟):</td>
                                                                        <td><input name="nns_timeshift_delay" id="nns_timeshift_delay" type="text"
                                                                                   value="<?php
                                                                                   if ($action == "edit") {
                                                                                           echo $edit_data["nns_timeshift_delay"];
                                                                                   } else {
                                                                                           echo '';
                                                                                   }
                                                                                   ?>"
                                                                                   /> </td>

                                                                </tr>
                                                                <tr>
                                                                        <td width="120">录播标识:</td>
                                                                        <td>
                                                                        	<select name="nns_storage_status" id="nns_storage_status" rule="noempty">
                                                                                <option value="0" <?php if ($action == "edit" && $edit_data["nns_storage_status"] == 0) { ?>selected="selected"<?php } ?>>不生效</option>        
                                                                            	<option value="1" <?php if ($action == "edit" && $edit_data["nns_storage_status"] == 1) { ?>selected="selected"<?php } ?>>生效</option>
                                                                            </select>
                                                                        </td>

                                                                </tr>
                                                                <tr>
                                                                        <td width="120">录播存储时长(单位小时):</td>
                                                                        <td><input name="nns_storage_delay" id="nns_storage_delay" type="text"
                                                                                   value="<?php
                                                                                   if ($action == "edit") {
                                                                                           echo $edit_data["nns_storage_delay"];
                                                                                   } else {
                                                                                           echo 0;
                                                                                   }
                                                                                   ?>"
                                                                                   /> </td>
                                                                </tr>
                                                                <tr  style="display:none;">
                                                                        <td width="120"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('zdgl_zdpt'); ?>:</td>
                                                                        <td><select name="nns_device_type" id="nns_device_type" rule="noempty">
                                                                                        <?php foreach ($nns_device_type as $device_type_item_key => $device_type_item_value) { ?>
                                                                                                <option value="<?php echo $device_type_item_key; ?>"  <?php if ($action == "edit" && $edit_data["nns_type"] == $device_type_item_key) { ?>selected="selected"<?php } ?>><?php echo $device_type_item_value; ?></option>
                                                                                        <?php } ?>
                                                                                </select>
                                                                                123                                     </td>
                                                                </tr>
                                                        </tbody>
                                                </table>
                                        </div>
                                </form>
                                <div class="controlbtns">
                                        <div class="controlbtn <?php echo $action; ?>"><a href="javascript:check_submit();"><?php echo $action_str; ?></a></div>
                                        <div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back') ?></a></div>
                                        <div style="clear:both;"></div>
                                </div>
                        </div>




                        <script>
                                $('.addLive').click(function() {

                                        $("#selectbox2 iframe").attr("src", "../live_pages/nncms_content_live_core_list.php").css('width', '700px');
                                        ;
                                        //    checkheight();
                                        $("#selectbox2").show();

                                })

                                function setLiveId(id, name) {
                                        $('#nns_media_name').val(name);
                                        $('#nns_media_content_id').val(id);
                                        close_select();
                                }

                                function close_select() {
                                        $("#selectbox2").hide();
                                }
                                close_select();
                        </script>
                </body>
        </html>

        <?php
}?>