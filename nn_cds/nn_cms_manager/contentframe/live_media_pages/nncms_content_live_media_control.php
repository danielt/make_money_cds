<?php
/*
 * Created on 2012-3-2
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
@header("Content-Type:text/html;charset=utf-8");
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}

//加载多语言
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

// 获取cache模块
include LOGIC_DIR.'video'.DIRECTORY_SEPARATOR.'live_media.class.php';
include LOGIC_DIR.'video'.DIRECTORY_SEPARATOR.'live_index.class.php';
include LOGIC_DIR.'live'.DIRECTORY_SEPARATOR.'live.class.php';
$dc = nl_get_dc(array (
	'db_policy' => NL_DB_WRITE,
	'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$dc->open();

include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_error.php";
//require_once $nncms_config_path. "nn_cms_manager/controls/nncms_controls_clear_cache.php";
require_once $nncms_db_path. "nns_log".DIRECTORY_SEPARATOR . "nns_db_op_log_class.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/nn_class/drm/drm.class.php';
$log_inst=new nns_db_op_log_class();
$action=$_POST["action"];
$nns_id=$_POST["nns_id"];
$live_id=$_POST["nns_live_id"];
$audit=$_POST["audit"];
$result_live_info=nl_live::query_by_id($dc, $live_id);
if(isset($result_live_info['data_info']) && is_array($result_live_info['data_info']) && !empty($result_live_info['data_info']))
{
    $_POST["nns_cp_id"] = $result_live_info['data_info']['nns_cp_id'];
}
$cp_id = (isset($_POST["nns_cp_id"]) && strlen($_POST["nns_cp_id"]) > 0) ? $_POST["nns_cp_id"] : '0';

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
if (!empty($action)){
	switch($action){
	case "edit":
		$action_str=cms_get_lang('edit|media_zb_mtwj');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107101");
		break;
	case "add":
		$action_str=cms_get_lang('add|media_zb_mtwj');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107100");
		break;
	case "delete":
		$action_str=cms_get_lang('delete|media_zb_mtwj');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107102");
		break;
	case "audit":
		$action_str=cms_get_lang('audit|media_zb_mtwj');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"107103");
		break;
	default:
		break;
	}
}
$checkpri=null;

if (!$pri_bool){
	Header("Location: ../nncms_content_wrong.php");
}else{
	$nns_media_name=$_POST["nns_media_name"];
	$nns_device_post_type=$_POST["nns_device_type"];
	$nns_tags=$_POST["nns_tags"];
	$nns_device_tag=$_POST["nns_device_tag"];
	$nns_media_url=$_POST["nns_media_url"];
	$nns_media_stream=$_POST["nns_media_stream"];
	$nns_media_mode=$_POST["nns_media_mode"];
	$nns_live_id=$_POST["nns_live_id"];
	$nns_index=$_POST["nns_index"];
	$nns_media_type_post=$_POST["nns_media_type"];
	$nns_media_policy=$_POST["nns_media_policy"];
	$nns_kpbs = $_POST['nns_kbps'];

	$insert_method=$_POST["insert_method"];
	$nns_media_content_id=$_POST["nns_media_content_id"];
	$nns_media_caps=$_POST["nns_media_caps"];
	$nns_domain = isset($_POST['nns_domain']) ? $_POST['nns_domain'] : '';
	$nns_cast_type = (isset($_POST["nns_cast_type"]) && in_array($_POST["nns_cast_type"], array(0,1))) ? $_POST["nns_cast_type"] : 0;
	$nns_media_service = (isset($_POST['nns_media_service'])) ? $_POST['nns_media_service'] : '';
	
	if (is_array($nns_media_caps))
	{
		$nns_media_caps=implode(',',$nns_media_caps);
		$temp_nns_media_caps = strtolower($nns_media_caps);
	}
	$nns_timeshift_delay=isset($_POST["nns_timeshift_delay"]) ? (int)$_POST["nns_timeshift_delay"] : 0;
	$nns_storage_status=isset($_POST["nns_storage_status"]) ? $_POST["nns_storage_status"] : 0;
	$nns_storage_delay=isset($_POST["nns_storage_delay"]) ? (int)$_POST["nns_storage_delay"] : 0;
	$nns_timeshift_status = (strpos($temp_nns_media_caps,'tstv') === false) ? 0 : 1;
	//	对TAG进行扩展	
	if (!empty($nns_device_tag)){
		$nns_tags.=$nns_device_tag.",";
	}

	$nns_name=$log_inst->nns_db_get_video_name_arr($nns_live_id,1);

	require_once $nncms_config_path. "nn_cms_manager/controls/nncms_control_memcache.php";

	include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/mgtv_v2/models/queue_task_model.php';
	include $nncms_db_path. "nns_live/nns_db_live_media_class.php";
	$live_inst=new nns_db_live_media_class();
	
	$result_info_cp=nl_cp::query_by_id($dc, $cp_id);
	if($result_info_cp['ret'] !=0)
	{
	    echo "<script>alert('". $result_info_cp['reason']. "');</script>";
    	if ($action=="delete"){
    		echo "<script>self.location.href='".$_SERVER["HTTP_REFERER"] ."';</script>";
    	}else{
    		echo "<script>self.location='nncms_content_live_media_list.php?nns_id=".$live_id ."';</script>";
    	}
    	die;
	}
	$arr_cp_config['base_info'] = $arr_cp_config['base_config'] = null;
	if(isset($result_info_cp['data_info']['nns_config']) && strlen($result_info_cp['data_info']['nns_config']) >0)
	{
	    $result_info_config = json_decode($result_info_cp['data_info']['nns_config'],true);
	    $arr_cp_config['base_config'] = (is_array($result_info_config) && !empty($result_info_config)) ? $result_info_config : null;
	}
	if(isset($result_info_cp['data_info']['nns_config']))
	{
	    unset($result_info_cp['data_info']['nns_config']);
	}
	if(is_array($result_info_cp['data_info']) && !empty($result_info['data_info']))
	{
	    foreach ($result_info_cp['data_info'] as $data_info_key=>$data_info_val)
	    {
	        $arr_cp_config['base_info'][$data_info_key]=$data_info_val;
	    }
	}
	switch($action){
	case "edit":
		$result=$live_inst->nns_db_live_media_modify($nns_id,$nns_media_name,$nns_device_post_type,$nns_media_url,$nns_tags,$nns_media_mode,$nns_media_stream,$nns_media_caps,
		$nns_timeshift_status,$nns_timeshift_delay,$nns_storage_status,$nns_storage_delay,$nns_cast_type,$nns_media_type_post,$nns_domain);
		$nns_media_name=$log_inst->nns_db_get_media_name_arr($nns_id,1);
		if ($result["ret"]!=0){
			echo "<script>alert('". $action_str. cms_get_lang('fault'). "');</script>";
		}else{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$nns_name.$language_media_js.($nns_index+1).$nns_media_name,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			//nl_live_media::delete_live_media_by_cache($dc,$nns_live_id,$nns_index);
    		
			
			if(isset($arr_cp_config['base_config']['message_live_media_import_enable']) && $arr_cp_config['base_config']['message_live_media_import_enable']  == '1')
			{
			    $queue_task_model = new queue_task_model($dc);
			    $queue_task_model->q_add_task_op_mgtv($nns_id, BK_OP_LIVE_MEDIA, BK_OP_MODIFY);
			    unset($queue_task_model);
			}
			
			$obj_drm = new nn_drm($dc,$cp_id,$arr_cp_config);
    		$arr_drm_params = array(
    		    'asset_type'=>'live',
    		    'nns_id'=>$nns_id,
    		);
    		$result_drm_data = $obj_drm->make_drm_params($arr_drm_params);
    		if($result_drm_data['ret'] !=0)
    		{
    		    echo "<script>alert('" . var_export($result_drm_data,true) . "');</script>";
    		}
    		else
    		{
    		    $params_media = null;
    		    if(isset($result_drm_data['data_info']['drm_flag']))
    		    {
    		        $params_media['drm_flag'] = $result_drm_data['data_info']['drm_flag'];
    		    }
    		    if(isset($result_drm_data['data_info']['drm_encrypt_solution']))
    		    {
    		        $params_media['drm_encrypt_solution'] = $result_drm_data['data_info']['drm_encrypt_solution'];
    		    }
    		    if(strlen($nns_media_service) >0)
    		    {
    		        $params_media['media_service'] = $nns_media_service;
    		    }
    		    if(isset($result_drm_data['data_info']['drm_ext_info']))
    		    {
    		        $params_media['drm_ext_info'] = (is_array($result_drm_data['data_info']['drm_ext_info']) && !empty($result_drm_data['data_info']['drm_ext_info'])) ? json_encode($result_drm_data['data_info']['drm_ext_info']) : $result_drm_data['data_info']['drm_ext_info'];
    		        $params_media['drm_ext_info'] = (strlen($params_media['drm_ext_info']) <1) ? '' : $params_media['drm_ext_info'];
    		    }
				$params_media['kbps'] = $nns_kpbs;
    		    $live_inst->add_media_other_data($nns_id, $params_media,$cp_id);
    		    echo "<script>alert('". $action_str.  cms_get_lang('success'). "');</script>";
    		}
		}
		
		break;
	case "add":
		//		$result=$live_inst->nns_db_live_media_add($nns_live_id,"",$nns_media_name,$nns_index,$nns_media_mode,$nns_media_stream,$nns_device_post_type,$nns_tags,$nns_media_url,null,null,$nns_media_policy,$nns_media_type);
		//echo  $nns_media_name;die;
		if ($insert_method==0){
			$result=$live_inst->nns_db_live_media_add($nns_live_id,"",$nns_media_name,$nns_index,$nns_media_mode,$nns_media_stream,$nns_device_post_type,$nns_tags,$nns_media_url,null,null,
			    $nns_media_policy,$nns_media_type_post,$nns_media_caps,$nns_timeshift_status,$nns_timeshift_delay,$nns_storage_status,$nns_storage_delay,$nns_media_content_id,$nns_cast_type,$nns_domain,$nns_media_service);
			$nns_media_name=$log_inst->nns_db_get_media_name_arr($nns_id,1);
		}else if($insert_method==1){
			$result=$live_inst->nns_db_live_media_bind($nns_live_id,"",$nns_media_name,$nns_index,$nns_media_mode,$nns_media_stream,$nns_device_post_type,$nns_tags,$nns_media_url,null,null,
			    $nns_media_content_id,$nns_media_caps,$nns_timeshift_status,$nns_timeshift_delay,$nns_storage_status,$nns_storage_delay,$nns_cast_type,$nns_domain,$nns_media_service);
			$nns_media_name=$nns_media_content_id;
		}
		if ($result["ret"]!=0){
			$error_info=get_error_infomation($result["ret"]);
			echo "<script>alert('". $error_info. $action_str.  cms_get_lang('fault'). "');</script>";
		}else{
		    if(isset($arr_cp_config['base_config']['message_live_media_import_enable']) && $arr_cp_config['base_config']['message_live_media_import_enable']  == '1')
		    {
		        $queue_task_model = new queue_task_model($dc);
		        $queue_task_model->q_add_task_op_mgtv($result['nns_id'], BK_OP_LIVE_MEDIA, BK_OP_ADD);
		        unset($queue_task_model);
		    }
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$nns_name.$language_media_js.($nns_index+1).$nns_media_name,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			//nl_live_media::delete_live_media_by_cache($dc,$nns_live_id,$nns_index);
			//nl_live_index::delete_live_index_by_cache($dc,$nns_live_id);
			$obj_drm = new nn_drm($dc,$cp_id,$arr_cp_config);
			$arr_drm_params = array(
			    'asset_type'=>'live',
			    'nns_id'=>$result['nns_id'],
			);
			$result_drm_data = $obj_drm->make_drm_params($arr_drm_params);
			if($result_drm_data['ret'] !=0)
			{
			    echo "<script>alert('" . var_export($result_drm_data,true) . "');</script>";
			}
			else
			{
			    $params_media = null;
			    if(isset($result_drm_data['data_info']['drm_flag']))
			    {
			        $params_media['drm_flag'] = $result_drm_data['data_info']['drm_flag'];
			    }
			    if(isset($result_drm_data['data_info']['drm_encrypt_solution']))
			    {
			        $params_media['drm_encrypt_solution'] = $result_drm_data['data_info']['drm_encrypt_solution'];
			    }
			    if(isset($result_drm_data['data_info']['drm_ext_info']))
			    {
			        $params_media['drm_ext_info'] = (is_array($result_drm_data['data_info']['drm_ext_info']) && !empty($result_drm_data['data_info']['drm_ext_info'])) ? json_encode($result_drm_data['data_info']['drm_ext_info']) : $result_drm_data['data_info']['drm_ext_info'];
                    $params_media['drm_ext_info'] = (strlen($params_media['drm_ext_info']) <1) ? '' : $params_media['drm_ext_info'];
			    }
				$params_media['kbps'] = $nns_kpbs;
			    $live_inst->add_media_other_data($result['nns_id'], $params_media,$cp_id);
			    echo "<script>alert('". $action_str.  cms_get_lang('success'). "');</script>";
			}
		}
		break;
	case "delete":
		$delete_bool=true;
		//	$role_inst->
		$nns_ids=explode(",",$nns_id);
		$nns_media_name=$log_inst->nns_db_get_media_name_arr($nns_id,1);
		
		$deleted = $_REQUEST['nns_deleted'];
		$live_media_action = ($deleted == '0') ? BK_OP_MODIFY : BK_OP_DELETE;
		$num=0;
		foreach($nns_ids as $nnsid){
			if (!empty($nnsid)){
				$num++;
				$resultarr= ($deleted == '0') ? $live_inst->nns_db_live_media_retrieve($nnsid) : $live_inst->nns_db_live_media_deleted($nnsid);
				if ($resultarr["ret"]!=0 && $num==1){
					$delete_bool=false;
					break;
				}
				if(isset($arr_cp_config['base_config']['message_live_media_import_enable']) && $arr_cp_config['base_config']['message_live_media_import_enable']  == '1')
				{
				    $queue_task_model = new queue_task_model($dc);
				    $queue_task_model->q_add_task_op_mgtv($nns_id, BK_OP_LIVE_MEDIA, $live_media_action);
				    unset($queue_task_model);
				}
			}
		}
		if (!$delete_bool){
			echo "<script>alert('". $action_str.  cms_get_lang('fault'). "');</script>";
		}else{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$nns_name.$language_media_js.($nns_index+1).$nns_media_name,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			nl_live_media::delete_live_media_by_cache($dc,$nns_live_id,$nns_index);
			delete_cache("media_$nns_live_id"."_".$nns_index);
			echo "<script>alert('". $action_str.  cms_get_lang('success'). "');</script>";
		}
		break;
	case "audit":
		$audit_bool=true;
		//	$role_inst->
		$nns_ids=explode(",",$nns_id);
		$num=0;
		foreach($nns_ids as $nnsid){
			if (!empty($nnsid)){
				$num++;

				$resultarr=$live_inst->nns_db_live_media_check($nnsid,$audit);

				if ($resultarr["ret"]!=0 && $num==1){
					$audit_bool=false;
					break;
				}
			}
		}
		if (!$audit_bool){
			echo "<script>alert('". $action_str. cms_get_lang('fault') . "');</script>";
		}else{
			//			$cache_manager_control->clear_cache_video_info($nns_live_id);
			echo "<script>alert('". $action_str.  cms_get_lang('success'). "');</script>";
		}
		break;
	default:

		break;
	}
	//var_dump($result);
	$category_inst=null;
	$log_inst=null;
	if ($action=="delete"){
		echo "<script>self.location.href='".$_SERVER["HTTP_REFERER"] ."';</script>";
	}else{
		echo "<script>self.location='nncms_content_live_media_list.php?nns_id=".$live_id ."';</script>";

	}
}
?>