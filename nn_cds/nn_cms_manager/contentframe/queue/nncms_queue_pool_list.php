<?php 
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) 
{
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -5);
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
		"db_policy" => NL_DB_WRITE,
		"cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));
$redis = nl_get_redis();


/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/queue/queue.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/queue/queue_pool.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/queue/queue_redis.class.php';

$result_queue = nl_queue::query_by_queue_id($dc, $_REQUEST['nns_queue']);
$result_queue = isset($result_queue['data_info']) ? $result_queue['data_info'] : null;

$result_data = nl_queue_pool::query_by_queue_id($dc,$_REQUEST['nns_queue']);

if($result_data['ret'] != 0)
{
	echo '<script>alert("'.$result_data['reason'].'");history.go(-1);</script>';die;
}
$result_data = isset($result_data['data_info']) ? $result_data['data_info'] : null;
$result_data_redis = nl_queue_redis::get_list($redis,$_REQUEST['nns_queue']);

if($result_data_redis['ret'] != 0)
{
    echo '<script>alert("'.$result_data_redis['reason'].'");history.go(-1);</script>';die;
}

$result_data_redis = isset($result_data_redis['data_info']) ? $result_data_redis['data_info'] : null;
$flag_is_unusual = false;
if(is_array($result_data_redis) && !empty($result_data_redis) && isset($result_queue['nns_state']) && $result_queue['nns_state'] == '0' && $result_queue['nns_often_state'] == '0')
{
    $result_data_redis_1 = array_filter($result_data_redis);
    if(count($result_data_redis) != count($result_data_redis_1))
    {
        $flag_is_unusual = true;
    }
}
$last_data = $last_result_data = $last_data_redis = null;
if(is_array($result_data_redis))
{
    foreach ($result_data_redis as $val)
    {
        $last_data_redis[$val] = $val;
    }
}
if(is_array($result_data))
{
    foreach ($result_data as $val)
    {
        $temp_right_value='';
        if(isset($last_data_redis[$val['nns_value']]))
        {
            $temp_right_value = $last_data_redis[$val['nns_value']];
            unset($last_data_redis[$val['nns_value']]);
        }
        $arr_right = array(
            'nns_value'=>$temp_right_value
        );
        $last_data[] = array(
            'left'=>$val,
            'right'=>$arr_right,
        );
    }
}
if(!empty($last_data_redis) && is_array($last_data_redis) )
{
    $last_data = $last_data === null ? array() : $last_data;
    foreach ($last_data_redis as $val)
    {
        $temp_data = array(
            'left'=>null,
            'right'=>array(
                'nns_value'=>$val
            )
        );
        array_unshift($last_data, $temp_data);
    }
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link href="../../css/public.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/cms_datepicker.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
		<script language="javascript">
			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				})
				return BoxKey;
			}
			function change_state(nns_queue,id,state) {
				var url = "<?php echo $base_file_name_base;?>_control.php?nns_queue="+nns_queue+"&action=state&nns_id="+id+"&nns_state="+state;
				window.location.href = url;
			}
			function delete_id(){
				var r=confirm("是否进行批量删除操作");
				if(r == true){
					var ids=getAllCheckBoxSelect();
					ids = ids.substr(0,ids.length-1);
					if(ids==""){
						alert('请选择数据');
					}else{
						var url = "<?php echo $base_file_name_base;?>_control.php?action=delete&nns_id="+ids;
						window.location.href = url;
					}
				}
			}
		</script>
	</head>
	<body>
		<div class="content">
		    <!-- 栏目导航描述div -->
			<div class="content_position">队列池列表</div>
			<!-- 表单查询div -->
			<div class="content_table">
			     <?php if($flag_is_unusual){ ?>
			     <table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tbody>
							<tr>
								<td><center><font color='red'><h1>redis队列数据异常</h1></font></center></td>
							</tr>
						</tbody>
				</table>
				<?php }?>
			</div>			
			<!-- 内容列表 DIV -->
			<div class="content_table formtable">
				<table style="float:left;" width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
					    <tr>
							<th colspan='5'><center><h1>数据库数据</h1></center></th>
							<th colspan='2'><center><h1>redis数据</h1></center></th>
						</tr>
						<tr>
							<th width='40px'><input name="" type="checkbox" value="">序号</th>
							<th width='250px'>模板ID/名称</th>
							<th width='250px'><center>值</center></th>
							<th width='50px'><center>状态</center></th>
							<th width='100px'><center>修改时间</center></th>
							<th width='250px'>值</th>
							<th width='80px'>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$num = 0;
						if(is_array($last_data) && !empty($last_data))
						{
							foreach ($last_data as $item) 
							{
							    $num++;
						?>
						      <tr>
						          <?php if(is_array($item['left']) && !empty($item['left'])){?>
    						          <td><input name="input" type="checkbox" value="<?php echo $item['left']["nns_id"]; ?>"><?php echo $num; ?></td>
    						          <td><?php echo $item['left']['nns_queue']. " / ". $item['left']['queue_name']; ?></td>
    						          <td><?php echo $item['left']['nns_value']; ?></td>
    							      <td><a href="javascript:change_state('<?php echo $_REQUEST['nns_queue'];?>','<?php echo $item['left']['nns_id']?>',<?php echo ($item['left']['nns_state'] !=0) ? 0 :1;?>);"><?php echo ($item['left']['nns_state'] !='0') ? '<font color="red">禁用</font>' : '<font color="green">启用</font>'; ?></a></td>
    						          <td><?php echo $item['left']['nns_modify_time']; ?></td>
						          <?php }else{?>
						              <td><input name="input" type="checkbox" value=""><?php echo $num; ?></td>
						              <td colspan='4'><center><font color='red'>数据不存在</font></center></td>
						          <?php }?>
						              <td><?php echo strlen($item['right']['nns_value']) >0 ? $item['right']['nns_value'] : '<font color="green">redis队列不存在<font>'; ?></td>
    						          <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
    						            <?php if(isset($item['left']["nns_value"]) && strlen($item['left']["nns_value"]) >0){?>
                                    	<a href="<?php echo $base_file_name_base;?>_control.php?nns_id=<?php echo $item['left']["nns_id"];?>&nns_queue=<?php echo $_REQUEST['nns_queue'];?>&action=delete&nns_value=<?php echo urlencode($item['left']["nns_value"]);?>">删除数据库及队列池</a>
                                    	<a href="<?php echo $base_file_name_base;?>_control.php?nns_queue=<?php echo $_REQUEST['nns_queue'];?>&action=push&nns_value=<?php echo urlencode($item['left']["nns_value"]);?>">插入队列池</a>
                                    	<?php }?>
                                    	<?php if(isset($item['right']["nns_value"]) && strlen($item['right']["nns_value"]) >0){?>
                                    	<a href="<?php echo $base_file_name_base;?>_control.php?nns_queue=<?php echo $_REQUEST['nns_queue'];?>&action=delete_redis&nns_value=<?php echo urlencode($item['right']["nns_value"]);?>">删除队列池</a>
                                        <?php }?>
                                      </td>
                              </tr>
						<?php
						     }
						}
						?>
					</tbody>
				</table>
				
			</div>
			<!-- 分页DIV -->
		    <div class="pagecontrol">
		             共<span style="font-weight:bold;color:#ff0000;"><?php echo count($last_data); ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo 1 . "/" . 1; ?></span>&nbsp;&nbsp;|&nbsp;&nbsp;
                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                <?php echo count($last_data); ?>&nbsp;
            </div>  
            <div class="controlbtns">
        		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        		<div style="clear:both;"></div>
        	</div>  
        </div>  
	</body>
</html>
