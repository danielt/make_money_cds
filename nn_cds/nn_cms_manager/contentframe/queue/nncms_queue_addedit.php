<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
//加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$action=$_GET["action"];
$nns_id=$_REQUEST["nns_id"];
$pri_bool=false;
if (!empty($action)){
	switch($action){
	case "edit":
		$action_str=cms_get_lang('save');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136002");
		break;
	case "add":
		$action_str=cms_get_lang('add');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"136003");
		break;
	default:
		break;
	}
}
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
    "db_policy" => NL_DB_WRITE,
    "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);
$edit_data = $_REQUEST;






/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/queue/queue.class.php';
global $g_arr_queue_pool;
$arr_queue_pool = (isset($g_arr_queue_pool) && is_array($g_arr_queue_pool) && !empty($g_arr_queue_pool)) ? $g_arr_queue_pool : null;
unset($g_arr_queue_pool);
$arr_queue_pool_2 = $arr_queue_pool_1 = null;
if(is_array($arr_queue_pool))
{
    foreach ($arr_queue_pool as $val)
    {
        if(isset($val['ext_info']) && !empty($val['ext_info']) && is_array($val['ext_info']))
        {
            $arr_queue_pool_1[$val['name']] = $val['ext_info'];
        }
        $arr_queue_pool_2[$val['name']] = $val;
    }
}
if ($action=="edit")
{
    $edit_data = nl_queue::query_by_id($dc,$nns_id);

    if($edit_data['ret'] != 0 )
    {
        echo "<script>alert('". $action_str. $edit_data['reason']. "');</script>";
        echo "<script>self.location='".$base_file_name_base."_list.php;</script>";
    }
    $edit_data = $edit_data['data_info'];
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
		<link href="../../css/comm.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
		<script language="javascript" src="../../js/cms_cookie.js"></script>
		<script language="javascript" src="../../js/table.js.php"></script>
		<script language="javascript" src="../../js/checkinput.js.php"></script>
		<script language="javascript" src="../../js/rate.js"></script>
		<script language="javascript" src="../../js/image_loaded_func.js"></script>
		<script language="javascript" src="../../js/alertbox.js"></script>
		<script language="javascript">
    		var type = "<?php echo $action;?>";
            var json_queue_pool = <?php echo is_array($arr_queue_pool) ? json_encode($arr_queue_pool) : json_encode(array()); ?>;
            $(document).ready(function(){
                change();
                $('#nns_queue').change(function(){
                	change();
                });
            });
            function change()
            {
                var nns_queue = $('#nns_queue option:selected').val();
                $("tr").remove(".content_ext_info");
                $.each( json_queue_pool, function(index, content)
      		  	{
      		   		if(content.name == nns_queue)
          		   	{
          		   		$('#nns_name').val(content.desc);
              		   	if("undefined" == typeof(content.ext_info))
                  		{
                      		return;
              		   	}
                		var html="<tr class='content_ext_info'><td class='rightstyle'>扩展参数:</td><td></td></tr>";
                		$.each( content.ext_info, function(index_ex, content_ex)
                      	{
                  		  	html+="<tr class='content_ext_info'>";
                  		  	if(content_ex.rule.length>0)
                      		{
                    		    html+=	"<td class='rightstyle'><span style='color:#FF0000; font-size:16px; font-weight:bold;'>*</span>"+content_ex.desc+":</td>";
                    		    html+=	"<td><input style='width:540px;' name='nns_ext_info["+content_ex.name+"]' type='text' value='"+content_ex.default_val+"' rule='"+content_ex.rule+"'/></td>";
                      		}
                  		  	else
                      		{
                      		  	html+=	"<td class='rightstyle'>"+content_ex.desc+":</td>";
                    		    html+=	"<td><input style='width:540px;' name='nns_ext_info["+content_ex.name+"]' type='text' value='"+content_ex.default_val+"'/></td>";
                          	}
                    		html+="</tr>";
              			});
                		$('#tr_tbody tbody').append(html);
          		   	}
      		  	});
	            window.parent.resetFrameHeight();
            }
        </script>
    </head>
    <body>
        <div class="content">
        	<div class="content_position">队列模板 > 添加/修改</div>
        	<form id="add_form" action="<?php echo $base_file_name_base;?>_control.php" method="post">
            	<input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
            	<div class="content_table">
            		<table width="100%" border="0" cellspacing="0" cellpadding="0" id='tr_tbody'>
            		  <tbody class="tr_tbody" >
            		    <?php 
            		    if($action == 'edit'){
            		    ?>
                		    <tr>
                				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>GUID:</td>
                				<td>
                				    <input name="nns_id" id="nns_id" type="hidden"  value="<?php if(isset($edit_data["nns_id"])){echo $edit_data["nns_id"];}?>" rule="noempty"/>
                				    <input type="text" style="width:540px;"  disabled value="<?php if(isset($edit_data["nns_id"])){echo $edit_data["nns_id"];}?>" />
                				</td>
                			</tr>
                			<?php if(isset($arr_queue_pool_2[$edit_data['nns_queue']])){?>
                			    <tr>
                				    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>定时器队列池模板:</td>
                				    <td>
                        				<input name="nns_queue" id="nns_queue" type="hidden"  value="<?php echo $edit_data["nns_queue"];?>" rule="noempty"/>
                    				    <input type="text" style="width:540px;"  disabled value="<?php echo $edit_data["nns_queue"];?>" />
                				    </td>
                    			</tr>
                    			<tr>
                    			    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>名称:</td>
                    				<td>
                    				    <input name="nns_name" id="nns_name" type="hidden"  value="<?php echo $arr_queue_pool_2[$edit_data['nns_queue']]["desc"];?>" rule="noempty"/>
                    				    <input type="text" style="width:540px;"  disabled value="<?php echo $arr_queue_pool_2[$edit_data['nns_queue']]["desc"];?>" />
                    				</td>
                    			</tr>
                    			<?php }else{?>
                    			<tr>
                				    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>定时器队列池模板:</td>
                				    <td><font color='red'>模板不存在</font></td>
                    			</tr>
                    			<tr>
                    			    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>名称:</td>
                    				<td><font color='red'>名称不存在</font></td>
                    			</tr>
                			<?php }?>
                			
            		    <?php }else{?>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>定时器队列池模板:</td>
            				<td>
            				    <select name='nns_queue' id='nns_queue' style="width:550px;" >
            				        <option value="">--请选择模板--</option>
                                    <?php foreach ($arr_queue_pool as $key=>$val){?>
                                        <option value="<?php echo $val['name'];?>" <?php if(isset($edit_data['nns_queue']) && $edit_data['nns_queue'] == $val['name']){ echo 'selected="selected"';}?>><?php echo $val['desc'];?></option>
                                    <?php }?>
                                </select>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>名称:</td>
            				<td><input style="width:540px;" name="nns_name" id="nns_name" type="text" value="" rule="noempty" readonly/></td>
            			</tr>
            			<?php }?>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>定时器运行时间间隔（单位/分钟）:</td>
            				<td><input style="width:540px;" name="nns_timer" id="nns_timer" type="text" value="<?php if(isset($edit_data["nns_timer"])){echo $edit_data["nns_timer"];}?>" rule="int"  /></td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>是否频繁生成队列:</td>
            				<td>
            				    <select name='nns_often_state' style="width:550px;" >
                                    <option value="0" <?php if(isset($edit_data['nns_often_state']) && $edit_data['nns_often_state'] == '0'){ echo 'selected="selected"';}?>>是</option>
                                    <option value="1" <?php if(isset($edit_data['nns_often_state']) && $edit_data['nns_often_state'] == '1'){ echo 'selected="selected"';}?>>否</option>
                                </select>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>状态:</td>
            				<td>
            				    <select name='nns_state' style="width:550px;" >
                                    <option value="0" <?php if(isset($edit_data['nns_state']) && $edit_data['nns_state'] == '0'){ echo 'selected="selected"';}?>>启用</option>
                                    <option value="1" <?php if(isset($edit_data['nns_state']) && $edit_data['nns_state'] == '1'){ echo 'selected="selected"';}?>>禁用</option>
                                </select>
            				</td>
            			</tr>
            			<tr>
            				<td class="rightstyle">描述:</td>
            				<td>
            				    <textarea name="nns_desc" id="nns_desc" cols="" rows="" style="width:542px;"><?php if(isset($edit_data["nns_desc"])){ echo $edit_data["nns_desc"];}else{ echo '';}?></textarea>
            				</td>
            			</tr>
            			<?php if($action == 'edit' && isset($arr_queue_pool_1[$edit_data['nns_queue']]) && !empty($arr_queue_pool_1[$edit_data['nns_queue']])){?>
            			    <tr class='content_ext_info'><td class="rightstyle">扩展参数:</td><td></td></tr>
            			    <?php $edit_data["nns_ext_info"] = strlen($edit_data["nns_ext_info"]) >0 ? json_decode($edit_data["nns_ext_info"],true) : null;?>
            			    <?php foreach ($arr_queue_pool_1[$edit_data['nns_queue']] as $val){?>
            			         <tr>
            			            <?php if(strlen($val['rule']) >0){?>
                        				<td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span><?php echo $val['desc']?>:</td>
                        				<td><input style="width:540px;" name="nns_ext_info[<?php echo $val['name']?>]" id="nns_ext_info[<?php echo $val['name'];?>]" type="text" value="<?php if(isset($edit_data["nns_ext_info"][$val['name']])){echo $edit_data["nns_ext_info"][$val['name']];}else{ echo '';}?>" rule="<?php echo $val['rule'];?>"  /></td>
                    			    <?php }else{?>
                        			    <td class="rightstyle"><?php echo $val['desc']?>:</td>
                        				<td><input style="width:540px;" name="nns_ext_info[<?php echo $val['name']?>]" id="nns_ext_info[<?php echo $val['name'];?>]" type="text" value="<?php if(isset($edit_data["nns_ext_info"][$val['name']])){echo $edit_data["nns_ext_info"][$val['name']];}else{ echo '';}?>" /></td>
                    			     <?php }?>
                    			</tr>
            			    <?php }?>
            			<?php }?>
            		  </tbody>
            		</table>
            	</div>
        	</form>
        	<div class="controlbtns">
        		<div class="controlbtn <?php echo $action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','<?php echo cms_get_lang('webkz_qrccxg'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str; ?></a></div>
        		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        		<div style="clear:both;"></div>
        	</div>
        </div>
    </body>
</html>
