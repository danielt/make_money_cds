<?php
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
include $nncms_config_path . 'nn_cms_manager/controls/nncms_controls_public_function.php';
require_once $nncms_db_path . 'nns_log' . DIRECTORY_SEPARATOR . 'nns_db_op_log_class.php';
$log_inst = new nns_db_op_log_class();

$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE, 
		'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$dc->open();
$obj_redis = nl_get_redis();
$params = $_REQUEST;

$action = $params['action'];
unset($params['action']);
$log_params = (!empty($params) && is_array($params)) ? json_encode($params) : '';
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);

/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/queue/queue.class.php';


global $g_arr_queue_pool;
$arr_queue_pool = (isset($g_arr_queue_pool) && is_array($g_arr_queue_pool) && !empty($g_arr_queue_pool)) ? $g_arr_queue_pool : array();
unset($g_arr_queue_pool);
session_start();
switch ($action)
{
	case "push":
		$nns_queue = $params['nns_queue'];//key GUID
	    $nns_value = urldecode($params['nns_value']);//key GUID
		if(strlen($nns_queue) <1 || strlen($nns_value) <1)
		{
		    echo "<script>alert('参数错误');history.go(-1);</script>";
		    break;
		}
        $result_queue_pool = nl_queue_redis::get_list($obj_redis, $nns_queue);
	    $result_queue_pool = (isset($result_queue_pool['data_info']) && is_array($result_queue_pool['data_info']) && !empty($result_queue_pool['data_info'])) ? $result_queue_pool['data_info'] : array();
	    if(in_array($nns_value, $result_queue_pool))
	    {
	        echo "<script>alert('队列数据已经存在');history.go(-1);</script>";
	        break;
	    }
	    $result = nl_queue_redis::push($obj_redis, $nns_queue, $nns_value);
		if ($result['ret'])
		{
			echo "<script>alert('" . $result['reason'] . "');history.go(-1);</script>";
		}
		else
		{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "修改:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
			echo "<script>alert('修改成功');</script>";
		}
		break;
	case "delete_redis":
	    $nns_queue = $params['nns_queue'];//key GUID
	    $nns_value = urldecode($params['nns_value']);//key GUID
	    if(strlen($nns_queue) <1 || strlen($nns_value) <1)
	    {
	        echo "<script>alert('参数错误');history.go(-1);</script>";
	        break;
	    }
	    $result = nl_queue_redis::delete($obj_redis, $nns_queue, $nns_value);
		if ($result['ret'])
		{
		    echo "<script>alert('" . $result['reason'] . "');</script>";
		}
		else
		{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "添加:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
			echo "<script>alert('添加成功');</script>";
		}
		break;
	case "delete":
		$nns_queue = $params['nns_queue'];//key GUID
	    $nns_value = urldecode($params['nns_value']);//key GUID
	    if(strlen($nns_queue) <1 || strlen($nns_value) <1)
	    {
	        echo "<script>alert('参数错误');history.go(-1);</script>";
	        break;
	    }
	    $result = nl_queue_redis::delete($obj_redis, $nns_queue, $nns_value);
	    $result = nl_queue_pool::delete($dc,$params['nns_value']);
	    
		$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "删除:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
		echo "<script>alert('删除成功');</script>";
		break;
	case "state":
	    $result = nl_queue_pool::edit($dc, array('nns_state'=>$params['nns_state']), $params['nns_id']);
	    if ($result['ret'])
	    {
	        echo "<script>alert('" . $result['reason'] . "');history.go(-1);</script>";
	    }
	    else
	    {
	        $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "修改:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
	        echo "<script>alert('修改成功');</script>";
	    }
	    break;
	default:
	    echo "<script>alert('没有此方式');</script>";
		break;
}
$partner_inst = null;
$log_inst = null;
echo "<script>self.location='".$base_file_name_base."_list.php?nns_queue={$params['nns_queue']}';</script>";
