<?php
header("Content-Type:text/html; charset=UTF-8");
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
if (!$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"100100")){
	Header("Location: nncms_content_wrong.php");
}
$checkpri=null;
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include $nncms_db_path. "nns_carrier/nns_db_carrier_class.php";
$carrier_inst=new nns_db_carrier_class();
 $carrier_name=$_POST["carrier_name"];
 $carrier_phone=$_POST["carrier_phone"];
 $carrier_contact=$_POST["carrier_contact"];
 $carrier_email=$_POST["carrier_email"];
 $carrier_addr=$_POST["carrier_addr"];
	 
 $carrier_desc=$_POST["carrier_desc"];
 $result=$carrier_inst->nns_db_carrier_list();
// var_dump($result);
// echo $_SERVER['PHP_SELF'];
 if (!empty($carrier_name)){
	
	$count=$carrier_inst->nns_db_carrier_count();

	if ($count["ret"]==0){
		if ($count["data"][0]["num"]>0){
//			var_dump($result);
			$modify_result=$carrier_inst->nns_db_carrier_modify($result["data"][0]["nns_id"],$carrier_name,0,$carrier_desc,$carrier_contact,$carrier_phone,$carrier_email,$carrier_addr);
		}else{
			$modify_result=$carrier_inst->nns_db_carrier_add($carrier_name,0,$carrier_desc,$carrier_contact,$carrier_phone,$carrier_email,$carrier_addr);
			if ($_SESSION["nns_mgr_name"]==g_sys_loginid && $modify_result["ret"]==0) $_SESSION["nns_org_id"]=$modify_result["data"]["id"];
		}
//		var_dump($modify_result);
		if($modify_result["ret"]==0){
			echo "<script>alert('".cms_get_lang('edit|success') ."');self.location='nncms_content_carrier.php';</script>";
		}else{
			echo "<script>alert('".cms_get_lang('edit|fault')."');</script>";
		}
	}
	
 }
$carrier_inst=null;
 if ($result["ret"]==0){
	$carrier_data=$result["data"][0];
	if ($_SESSION["nns_mgr_name"]==g_sys_loginid)
	$_SESSION["nns_org_id"]=$result["data"][0]["nns_id"];
	
 }else if ($result["ret"]==8032){
	echo "<script>alert('". cms_get_lang('please_set_platform_first')."');</script>";
 }
 
//var_dump($languages);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/table.js.php"></script>
<script language="javascript" src="../js/checkinput.js.php"></script>
<script type="text/javascript" src="../js/text_to_form.js"></script>
<script language="javascript">
function checkForm(formobj){
	_formobj=formobj
	if (checkInputRules())
	window.parent.cmsalert.showAlert("<?php echo cms_get_lang('xtgl_yysgl');?>","<?php echo cms_get_lang('msg_ask_change');?>",submitForm);
	
}
</script>
</head>

<body>
<div class="content">
	<div class="content_position"><?php echo cms_get_lang('xtpz'),' > ',cms_get_lang('xtgl_yysgl')?></div>
	<form action="nncms_content_carrier.php" method="post" id="globalform">
	<div class="content_table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">

		<tbody>
		   
		  <tr>
				<td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('yysmc');?>:</td>
				<td><input name="carrier_name" id="carrier_name" type="text" value="<?php echo $carrier_data["nns_name"];?>" rule="noempty"/> 
				</td>
		  </tr>
		   <tr>
				<td class="rightstyle"><?php echo cms_get_lang('lxr');?>:</td>
				<td><input name="carrier_contact" id="carrier_contact" type="text" value="<?php echo $carrier_data["nns_contact"];?>"/>
				</td>
		  </tr>
		  <!-- 2012-8-20 陈波 1661507441@qq.com 增加数据格式验证   start -->
		  <tr>
				<td  class="rightstyle"><?php echo cms_get_lang('lxdh');?>:</td>
				<td><input name="carrier_phone" id="carrier_phone" type="text" rule='phone' value="<?php echo $carrier_data["nns_telephone"];?>"/>
				</td>
		  </tr>
		  <tr>
				<td class="rightstyle"><?php echo cms_get_lang('email');?>:</td>
				<td><input name="carrier_email" id="carrier_email" type="text" rule='email' value="<?php echo $carrier_data["nns_email"];?>"/>
				</td>
		  </tr>
		  <!-- 2012-8-20 陈波 1661507441@qq.com 增加数据格式验证   end -->
		  <tr>
				<td class="rightstyle"><?php echo cms_get_lang('lxdz');?>:</td>
				<td><input name="carrier_addr"  id="carrier_addr" type="text" value="<?php echo $carrier_data["nns_addr"];?>"/>
				</td>
		  </tr>
		  <tr>
				<td class="rightstyle"><?php echo cms_get_lang('ms');?>:</td>
				<td><textarea name="carrier_desc" id="carrier_desc" cols="" rows=""><?php echo $carrier_data["nns_desc"];?></textarea>
				</td>
		  </tr>
		  </tbody>
		</table>
	</div>
	</form>
	<div class="controlbtns">
		<div class="controlbtn sure"><a href="javascript:checkForm($('#globalform'));"><?php echo cms_get_lang('confirm');?></a></div>
		<div class="controlbtn back"><a href="javascript:resetForm($('#globalform'));"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
	</div>
</div>
</body>
</html>
