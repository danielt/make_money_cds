<?php
/*
 * Created on 2012-3-2
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
@header("Content-Type:text/html;charset=utf-8");

include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
require_once $nncms_db_path. "nns_log".DIRECTORY_SEPARATOR . "nns_db_op_log_class.php";
$log_inst=new nns_db_op_log_class();

 $action=$_POST["action"];
 $nns_id=$_POST["nns_id"];
 $nns_role_pri=$_POST["nns_role_pri"];
 $nns_role_name=$_POST["nns_role_name"];
 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
if (!empty($action)){
	switch($action){
		case "edit":
		$action_str=$language_action_edit. $language_manager_role_jsxx;
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"101101");
		break;
		case "add":
		$action_str=$language_action_add. $language_manager_role_jsxx;
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"101100");
		break;
		case "delete":
		$action_str=$language_action_delete. $language_manager_role_jsxx;
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"101102");
		break;
		default:
		break;
	}
}
$checkpri=null;
if (!$pri_bool){
	 Header("Location: ../nncms_content_wrong.php");
}else{

	include $nncms_db_path. "nns_role/nns_db_role_class.php";
	$role_inst=new nns_db_role_class();
	switch($action){
		case "edit":
		$result=$role_inst->nns_db_role_modify($nns_id,$nns_role_name,$nns_role_pri);
		if ($result["ret"]!=0){
			echo "<script>alert('". $action_str. $language_action_fault. "');</script>";
		}else{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$nns_role_name,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			echo "<script>alert('". $action_str. $language_action_success. "');</script>";
		}
		break;
		case "add":
		$result=$role_inst->nns_db_role_add($nns_role_name,$nns_role_pri);
		if ($result["ret"]!=0){
			echo "<script>alert('". $action_str. $language_action_fault. "');</script>";
		}else{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$nns_role_name,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			echo "<script>alert('". $action_str. $language_action_success. "');</script>";
		}
		break;
		case "delete":
		$delete_bool=true;
	//	$role_inst->
			$nns_ids=explode(",",$nns_id);
			$role_names=$log_inst->nns_db_get_role_name_arr($nns_id);
			$num=0;
			foreach($nns_ids as $nnsid){
				if (!empty($nnsid)){
					$num++;
					$resultarr=$role_inst->nns_db_role_delete($nnsid);
					if ($resultarr["ret"]!=0 && $num==1){
						$delete_bool=false;
						echo "<script>alert('".$result["reason"] ."');</script>";
						break;
					}
				}
			}
		if (!$delete_bool){
			echo "<script>alert('". $action_str. $language_action_fault. "');</script>";
		}else{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],$action,$_SESSION["nns_mgr_name"].$action_str.":".$role_names,$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			echo "<script>alert('". $action_str. $language_action_success. "');</script>";
		}
		break;
		default:

		break;
	}
	$role_inst=null;
	//var_dump($result);
	$log_inst=null;
	echo "<script>self.location='nncms_content_rolelist.php';</script>";
}
?>
