<?php
/*
 * Created on 2012-2-29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../../nncms_manager_inc.php");
 // 导入多语言包
 //加载多语言
include_once($nncms_config_path.'nn_cms_config/nn_cms_global.php');
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/language_pri.php";

 //获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();

$action=$_GET["action"];
$nns_id=$_GET["nns_id"];
$pri_bool=false;
if (!empty($action)){
	switch($action){
		case "edit":
		$action_str=cms_get_lang('save');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"101101");
		break;
		case "add":
		$action_str=cms_get_lang('add');
		$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"101100");
		break;
		default:
		break;
	}
}
$checkpri=null;
if (!$pri_bool) Header("Location: ../nncms_content_wrong.php");



 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
 include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php";
// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
// include($nncms_db_path. "nns_pri/nns_db_pri_class.php");
 //echo dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv_v2/models/sp_model.php';
 include_once dirname(dirname(dirname(dirname(__FILE__)))).'/mgtv_v2/models/sp_model.php';
 
 

 $pri_inst=new nns_db_pri_class();
 $priArr=$pri_inst->nns_db_pri_list();
 if ($priArr["ret"]!=0){
 	$data=null;
 	echo "<script>alert('".$priArr["reason"] ."');</script>";
 }
 $data=$priArr["data"];
 
$sp_list = sp_model::get_sp_list();

foreach ($sp_list as $item_sp_list){
	$sp_arr = array("code"=>$item_sp_list['nns_id']."00","desc"=>$item_sp_list['nns_name']);
	array_push($data['37']['pri'], $sp_arr);
}

 $pri_inst=null;
 if ($action=="edit"){
 	include $nncms_db_path. "nns_role/nns_db_role_class.php";
	$role_inst=new nns_db_role_class();
	if (!empty($nns_id)){
		$role_info=$role_inst->nns_db_role_info($nns_id);
	}else{
		echo "<script>alert('".$role_info["reason"] ."');self.location='nncms_content_rolelist.php';</script>";
	}
	if ($role_info["ret"]!=0){
		echo "<script>alert('".$role_info["reason"] ."');self.location='nncms_content_rolelist.php';</script>";
	}
	$role_inst=null;
	$edit_data=$role_info["data"][0];
	$edit_pri=$edit_data["nns_pri_id"];
 }
//echo '<pre>';
//print_r($data);
//print_r($edit_pri);exit;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/radiolist.js"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript">
	$(document).ready(function(){
		$(".prant_check_box").click(function(){
			var bool;
			if ($(this).attr("checked")=="checked"){
				bool=true;
			}else{
				bool=false;
			}
			$(this).parent().parent().find("> .radioitem > input[type=checkbox]").attr("checked",bool);
		});

	});
	function get_all_check_value(){
		var datastr = getCheckedData();

		var len = $(".no_class_check_box").length;
		for (var i=0;i<len;i++){
            var $obj=$(".no_class_check_box:eq("+i+")");
           	if ($obj.attr("checked")){
				datastr+=$obj.attr("value")+",";
			}
		}

		$('#nns_role_pri').attr('value',datastr);
	}
</script>
</head>

<body>


<div class="content">
	<div class="content_position"><?php echo cms_get_lang('xtgl');?> > <?php echo cms_get_lang('xtgl_jsgl');?></div>

    <form id="add_form" action="nncms_content_role_control.php" method="post">
    <input name="action" id="action" type="hidden" value="<?php echo $action;?>" />
    <input name="nns_role_pri" id="nns_role_pri" type="hidden" value="" />
    <?php if ($action=="edit"){?>
    <input name="nns_id" id="nns_id" type="hidden" value="<?php echo $nns_id;?>"/>
    <?php } ?>
    <div class="content_table">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
              <tr>
                <td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('role_name');;?></td>
                <td><input name="nns_role_name" id="nns_role_name" type="text" rule="noempty"
                	value="<?php if($action=="edit"){echo $edit_data["nns_name"];}?>"
                /></td>

      </tr>
              <tr>
               
                <td colspan="2">
                <div class="radiolist">
                <?php foreach($data as $item){
                		if ( pub_func_get_module_right($item['code'],'role')==false){
                			 continue;
                		}
                	?>

                	<div class="radiogroup">
                		<h3 style="background:#fff">
                		<?php if ($item["pri"]){?>
                		<input class="prant_check_box" name="" type="checkbox"/>&nbsp;
                		<?php }?>
                		 <?php echo $item["desc"];?>
                		&nbsp;&nbsp;&nbsp;
                		<!-- <input class="no_class_check_box" value="<?php echo $item["code"].'001';?>" type="checkbox" <?php if (false!==strpos($edit_pri,$item["code"].'001')){echo "checked";}?> />&nbsp; <?php echo cms_get_lang('pri_module_invisible')?> -->
                		</h3>

                		<?php
                		if ($item["pri"]){
                		 foreach($item["pri"] as $subitem){
                			if($subitem["code"] != $item["code"].'001'){
                			?>
	   	  				<div class="radioitem">
	                		<input name="" type="checkbox" value="<?php echo $subitem["code"];?>" <?php if (strstr($edit_pri,$subitem["code"])){echo "checked";}?>/>&nbsp; <?php echo $subitem["desc"]; ?>
	                    </div>
	                    <?php
                			}
	                    } }?>
	                    <div style="clear:both;"/>
                    </div>
      			<?php }?>
                </div></td>
          </tr>

          </tbody>
        </table>
    </div>
    </form>
    <div class="controlbtns">
    	<div class="controlbtn <?php echo $action;?>"><a href="javascript:get_all_check_value();checkForm('<?php echo cms_get_lang('xtgl_jsgl');?>','<?php echo cms_get_lang('msg_confirm_current_option'); ?>',$('#add_form'),'<?php echo $action; ?>');"><?php echo $action_str;?></a></div>
        <div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back')?></a></div>
        <div style="clear:both;"></div>
    </div>
</div>


</body>
</html>