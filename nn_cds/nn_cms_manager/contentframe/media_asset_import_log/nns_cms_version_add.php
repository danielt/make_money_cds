<?php
header("Content-Type:text/html;charset=utf-8");
ob_start();
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
//导入语言包
session_start();
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
include $nncms_config_path . 'nn_logic/nl_config.php';
include $nncms_config_path . 'nn_logic/version_mac/version_mac.class.php';
require_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

if (count($_POST) > 0) {

        $dc = nl_get_dc(array(
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
                )
        );
//连接数据库
        $dc->open();
        $result = nl_version_mac::add_version($dc, $_POST);
        ob_end_clean();
        if ($result === "0") {
                exit("<script>alert('版本号已经存在');history.go(-1);</script>");
        }
        exit("<script>alert('添加成功');window.location.href='nns_cms_version_list.php'</script>");
}
$g_update_type = array(
    "manual" => "手动升级",
    "force" => "强制升级",
);

$update_type = $g_update_type;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../../js/table.js.php"></script>
                <script language="javascript" src="../../js/checkinput.js.php"></script>
                <script language="javascript" src="../../js/tabs.js"></script>
                <script language="javascript" src="../../js/cms_cookie.js"></script>
                <link rel="stylesheet" type="text/css" href="../../js/calendar/jscal2.css"/>
                <link rel="stylesheet" type="text/css" href="../../js/calendar/border-radius.css"/>
                <link rel="stylesheet" type="text/css" href="../../js/calendar/win2k.css"/>
                <script type="text/javascript" src="../../js/calendar/calendar.js"></script>
                <script type="text/javascript" src="../../js/calendar/calendar.lang.js.php"></script>
                <script  src="../../js/report_select_time_bar.js"></script>


        </head>
        <body>
                <div class="content">
                        <div class="content_position">终端管理 > 版本添加</div>
                        <form id="add_form" action="" method="post"  enctype="multipart/form-data">
                                <input name="action" id="action" type="hidden" value="add" />
                                <div class="content_table">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                        <tr>
                                                                <td class="rightstyle"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>版本号 :</td>
                                                                <td><input name="nns_id" id="nns_device_id"  maxlength="32" type="text" rule="noempty"  value="" /> </td>
                                                        </tr>
                                                        <tr>
                                                                <td class="rightstyle"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;"></font>版本号说明:</td>
                                                                <td><input name="nns_intor" id="nns_device_id"  maxlength="256" type="text"  value="" /> </td>
                                                        </tr>

                                                        <tr>
                                                                <td class="rightstyle"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>升级时间:</td>
                                                                <td><input name="nns_app_update_time"  id="begin_time" rule="noempty"  maxlength="32" type="text" value="" /> </td>
                                                        </tr>
                                                        <tr>
                                                                <td class="rightstyle"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>升级类型:</td>
                                                                <td><select name="nns_app_type"><?php
                                                                                foreach ($update_type as $k => $v) {
                                                                                        echo "<option value='$k'>$v</option>";
                                                                                }
                                                                                ?></select> </td>
                                                        </tr>
                                                        <tr>
                                                                <td class="rightstyle">MAC地址列表:<br/>多个用逗号分隔</td>
                                                                <td>
                                                                        <textarea name="nns_mac" maxlength="1024"  style="width: 60%; height: 150px;"></textarea>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td class="rightstyle">升级说明:</td>
                                                                <td>
                                                                        <textarea name="nns_app_desc" maxlength="1024"  style="width: 60%; height: 150px;"></textarea>
                                                                </td>
                                                        </tr>

                                                        <tr>
                                                                <td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>升级包URL:</td>
                                                                <td>
                                                                        <div class="img_content">
                                                                                <input name="nns_app_url" rule="noempty" id="skin_zip" type="text" style='width: 60%'/>
                                                                                <div style="clear:both;"/>
                                                                        </div>
                                                                </td>
                                                        </tr>

                                                </tbody>
                                        </table>
                                </div>
                        </form>
                        <div class="controlbtns">
                                <div class="controlbtn add"><a href="javascript:checkForm('终端管理','是否进行本次修改？',$('#add_form'),'add');">添加</a></div>
                                <div class="controlbtn back"><a href="javascript:returnToHistory();">返回</a></div>
                                <div style="clear:both;"></div>
                        </div>
                </div>



        </body>
</html>

