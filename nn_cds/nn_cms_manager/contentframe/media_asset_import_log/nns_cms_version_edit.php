<?php
header("Content-Type:text/html;charset=utf-8");
ob_start();
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
//导入语言包
session_start();
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
include $nncms_config_path . 'nn_logic/nl_config.php';
include $nncms_config_path . 'nn_logic/version_mac/version_mac.class.php';
require_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        )
);
//连接数据库
$dc->open();
if (count($_POST) > 0) {
        $result = nl_version_mac::set_version($dc, $_POST);
        ob_end_clean();
        exit("<script>alert('修改成功');window.location.href='nns_cms_version_look.php?nns_id={$_GET['nns_id']}'</script>");
}
$where['nns_id'] = $_GET['nns_id'];
$result = nl_version_mac::get_version_list($dc->db(), $where);
if (!is_array($result))
        exit("<script>alert('没有数据');window.location.href='nns_cms_version_list.php'</script>");
$l = $result[0];
$g_update_type = array(
    "manual" => "手动升级",
    "force" => "强制升级",
);

$update_type = $g_update_type;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../../js/table.js.php"></script>
                <script language="javascript" src="../../js/checkinput.js.php"></script>
                <script language="javascript" src="../../js/tabs.js"></script>
                <script language="javascript" src="../../js/cms_cookie.js"></script>

                <link rel="stylesheet" type="text/css" href="../../js/calendar/jscal2.css"/>
                <link rel="stylesheet" type="text/css" href="../../js/calendar/border-radius.css"/>
                <link rel="stylesheet" type="text/css" href="../../js/calendar/win2k.css"/>
                <script type="text/javascript" src="../../js/calendar/calendar.js"></script>
                <script type="text/javascript" src="../../js/calendar/calendar.lang.js.php"></script>
                <script  src="../../js/report_select_time_bar.js"></script>

        </head>
        <body>
                <div class="content">
                        <div class="content_position">终端管理 > 版本编辑</div>
                        <form id="add_form" action="" method="post"  enctype="multipart/form-data">
                                <div class="content_table">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                        <tr>
                                                                <td class="rightstyle"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>版本号 :</td>
                                                                <td><input name="nns_id" id="nns_device_id"  maxlength="32" type="hidden"value="<?php echo $l['nns_id'] ?>" /><?php echo $l['nns_id'] ?> </td>
                                                        </tr>
                                                        <tr>
                                                                <td class="rightstyle"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;"></font>版本号说明:</td>
                                                                <td><input name="nns_intor" id="nns_device_id"  maxlength="256" type="text"  value="<?php echo $l['nns_intor'] ?>" /> </td>
                                                        </tr>

                                                        <tr>
                                                                <td class="rightstyle"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>APP升级时间:</td>
                                                                <td><input name="nns_app_update_time"  id="begin_time" rule="noempty"  maxlength="32" type="text" value="<?php echo $l['nns_app_update_time'] ?>" /> </td>
                                                        </tr>
                                                        <tr>
                                                                <td class="rightstyle"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>升级类型:</td>
                                                                <td><select   name="nns_app_type"><?php
                                                                                foreach ($update_type as $k => $v) {
                                                                                        $selectd = '';
                                                                                        if ($l['nns_app_type'] == $k) {
                                                                                                $selectd = "selected=selected";
                                                                                        }
                                                                                        echo "<option $selectd value='$k'>$v</option>";
                                                                                }
                                                                                ?></select> </td>

                                                        </tr>
                                                        <tr>
                                                                <td class="rightstyle">MAC地址列表:<br/>多个用逗号分隔</td>
                                                                <td>
                                                                        <textarea name="nns_mac" maxlength="1024"  style="width: 60%; height: 150px;"><?php echo $l['nns_mac'] ?></textarea>
                                                                </td>
                                                        </tr>

                                                        <tr>
                                                                <td class="rightstyle">升级说明:</td>
                                                                <td>
                                                                        <textarea name="nns_app_desc" maxlength="1024"  style="width: 60%; height: 150px;"><?php echo $l['nns_app_desc'] ?></textarea>
                                                                </td>
                                                        </tr>


                                                        <tr>
                                                                <td class="rightstyle"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>升级包URL:</td>
                                                                <td>
                                                                        <div class="img_content">
                                                                                <div class="img_edit"><input name="nns_app_url" rule="noempty" id="skin_zip" value=' <?php echo $l['nns_app_url'] ?>' type="text" size="60%"/></div>
                                                                                <div style="clear:both;"/>
                                                                        </div>
                                                                </td>
                                                        </tr>

                                                </tbody>
                                        </table>
                                </div>
                        </form>
                        <div class="controlbtns">
                                <div class="controlbtn edit"><a href="javascript:checkForm('终端管理','是否进行本次修改？',$('#add_form'),'add');">修改</a></div>
                                <div class="controlbtn back"><a href="javascript:returnToHistory();">返回</a></div>
                                <div style="clear:both;"></div>
                        </div>
                </div>



        </body>
</html>

