<?php
header("Content-Type:text/html;charset=utf-8");
ob_start();
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
//导入语言包
session_start();
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
include $nncms_config_path . 'nn_logic/nl_config.php';
include $nncms_config_path . 'nn_logic/nns_media_asset_import_log/nns_media_asset_import_log.class.php';
require_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
include $nncms_db_path . "nns_common/nns_db_pager_class.php";
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        )
);
//连接数据库
$dc->open();
if (isset($_POST['delete'])) {

        $del = explode(',', $_POST['nns_id']);
        $delid = '';
        foreach ($del as $v) {
                $delid .="'$v',";
        }
        if ($_POST['delete'] == 'edit') {
                $data['nns_state'] = '1';
                $del = nl_media_asset_import_log::set_log($dc, $delid, $data);
        } else {
                $del = nl_media_asset_import_log::del_log($dc, $delid);
        }
        ob_end_clean();
        exit("<script>alert('操作成功');window.location.href='nns_cms_log_list.php'</script>");
}
$count = nl_media_asset_import_log::get_log_list($dc->db(), '', "count(nns_id) as count");

$currentpage = 1;
if (!empty($_GET["page"]))
        $currentpage = $_GET["page"];
$url = "nns_cms_log_list.php?";
$num = 12;
$pager = new nns_db_pager_class($count, $num, $currentpage, $url);
$list = nl_media_asset_import_log::get_log_list($dc->db(), '', '*', $pager->startnum, $num);
if (!is_array($list))
        $list = array();
$g_update_type = array(
    "1001000" => "图片错误",
);
include_once dirname(dirname(dirname(dirname(__FILE__)))) . "/nn_cms_db/nns_vod/nns_db_vod_class.php";
$video_inst = new nns_db_vod_class();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../../js/cms_cookie.js"></script>
                <script language="javascript" src="../../js/table.js.php"></script>
                <script language="javascript" src="../../js/checkinput.js.php"></script>
                <script language="javascript">
                        function checkhiddenInput(type) {
                                var par = getAllCheckBoxSelect();
                                if (par == "") {
                                        alert("请选择一条数据");
                                } else {
                                        $('#nns_id').val(par);
                                        $('#action').val(type);
                                        $('#delete_form').submit();
                                }
                        }
                </script>
        </head>

        <body>
                <div class="content">
                        <div class="content_position">日志管理> 日志列表</div>

                        <div class="content_table formtable">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <thead>
                                                <tr>
                                                        <th><input name="" type="checkbox" value="" /></th>
                                                        <th>ID</th>
                                                        <th>导入ID</th>      
                                                        <th>影片名称</th>
                                                        <th>影片类型</th>
                                                        <th>错误描述</th>
                                                        <th>错误类型</th>   
                                                        <th>导入时间</th>
                                                        <th>错误状态</th>
                                                        <th>操作</th>
                                                </tr>
                                        </thead>
                                        <tbody>
                                                <?php foreach ($list as $k => $v) { ?>
                                                        <tr>
                                                                <td><input name="" type="checkbox" value="<?php echo $v['nns_id']; ?>" /></td>
                                                                <td><?php echo $v['nns_id']; ?></td>
                                                                <td><?php echo $v['nns_import_video_id']; ?></td>
                                                                <td><?php echo $v['nns_video_name'] ?></td>
                                                                <td>
                                                                        <?php
                                                                        if ($v['nns_import_type'] == 0) {
                                                                                echo "点播";
                                                                        } elseif ($v['nns_import_type'] == 1) {
                                                                                echo "直播";
                                                                        } else {
                                                                                echo "时移节目";
                                                                        }
                                                                        ?>
                                                                </td>
                                                                <td><?php echo $v['nns_error_desc'] ?></td>
                                                                <td><?php
                                                                        if (isset($g_update_type[$v['nns_error_code']])) {
                                                                                echo $g_update_type[$v['nns_error_code']];
                                                                        } else {
                                                                                echo "未知类型";
                                                                        }
                                                                        ?></td>

                                                                <td><?php echo $v['nns_create_time'] ?></td>
                                                                <td><?php
                                                                        if ($v['nns_state'] == 0) {
                                                                                echo "未解决";
                                                                        } else {
                                                                                echo "已解决";
                                                                        }
                                                                        ?></td>
                                                                <td>
                                                                        <?php
                                                                        $exists_id = $video_inst->get_video_id_by_import_id('mgtv', $v['nns_import_video_id']);
                                                                        if (is_string($exists_id)) {
                                                                                echo "<a href='../vod_pages/nncms_content_vod_detail.php?view_type=&nns_id=$exists_id'>编辑影片</a>";
                                                                        } else {
                                                                                echo "未导入成功";
                                                                        }
                                                                        ?>
                                                                </td>
                                                        </tr>
                                                        <?php
                                                }
                                                for ($i = 0; $i < 12 - count($list); $i++) {
                                                        echo "<tr><td colspan='10'>&nbsp;</td></tr>";
                                                }
                                                ?>
                                        </tbody>
                                </table>
                        </div>
                        <?php echo $pager->nav(); ?>   <div  class="controlbtns">
                                <div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
                                <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
                                <div class="controlbtn delete"><a href="javascript:checkhiddenInput();"  action="delete">删除</a></div>
                                <div class="controlbtn edit"><a href="javascript:checkhiddenInput('edit');"  action="edit">已解决</a></div>

                                <div style="clear:both;"></div>
                        </div>
                        <form id="delete_form" action="nns_cms_log_list.php" method="post">
                                <input name="delete" id="action" type="hidden"/>
                                <input name="nns_id" id="nns_id" type="hidden" value="" />
                        </form>
                </div>
        </body>
</html>