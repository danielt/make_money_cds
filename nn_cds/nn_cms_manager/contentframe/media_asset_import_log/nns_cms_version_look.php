<?php
header("Content-Type:text/html;charset=utf-8");
ob_start();
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
//导入语言包
session_start();
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
include $nncms_config_path . 'nn_logic/nl_config.php';
include $nncms_config_path . 'nn_logic/version_mac/version_mac.class.php';
require_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        )
);
//连接数据库
$dc->open();
$where['nns_id'] = $_GET['nns_id'];
$result = nl_version_mac::get_version_list($dc->db(), $where);
if (!is_array($result))
        exit("<script>alert('没有数据');window.location.href='nns_cms_version_list.php'</script>");
$l = $result[0];
$g_update_type = array(
    "manual" => "手动升级",
    "force" => "强制升级",
);

$update_type = $g_update_type;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>

                <script language="javascript" src="../../js/table.js.php"></script>
                <script language="javascript" src="../../js/checkinput.js.php"></script>
                <script language="javascript" src="../../js/tabs.js"></script>
                <script language="javascript" src="../../js/cms_cookie.js"></script>

        </head>
        <body>
                <div class="content">
                        <div class="content_position">终端管理 > 版本查看</div>
                        <form id="add_form" action="" method="post"  enctype="multipart/form-data">
                                <div class="content_table">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                        <tr>
                                                                <td class="rightstyle"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>版本号 :</td>
                                                                <td><?php echo $l['nns_id'] ?> </td>
                                                        </tr>
                                                        <tr>
                                                                <td class="rightstyle"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;"></font>版本号说明:</td>
                                                                <td><?php echo $l['nns_intor'] ?>"</td>
                                                        </tr>

                                                        <tr>
                                                                <td class="rightstyle"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>APP升级时间:</td>
                                                                <td><?php echo $l['nns_app_update_time'] ?></td>
                                                        </tr>
                                                        <tr>
                                                                <td class="rightstyle"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>升级类型:</td>
                                                                <td><?php echo $update_type[$l['nns_app_type']]; ?> </td>

                                                        </tr>
                                                        <tr>
                                                                <td class="rightstyle">MAC地址列表:</td>
                                                                <td>
                                                                        <?php echo $l['nns_mac'] ?>
                                                                </td>
                                                        </tr>

                                                        <tr>
                                                                <td class="rightstyle">升级说明:</td>
                                                                <td>
                                                                        <?php echo $l['nns_app_desc'] ?>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td class="rightstyle">URL:</td>
                                                                <td>
                                                                        <?php echo $l['nns_app_url'] ?>
                                                                </td>
                                                        </tr>
                                                </tbody>
                                        </table>
                                </div>
                        </form>
                        <div class="controlbtns">
                                <div class="controlbtn edit"><a href="nns_cms_version_edit.php?nns_id=<?php echo $_GET['nns_id'] ?>">编辑</a></div>
                                <div class="controlbtn back"><a href="nns_cms_version_list.php">返回</a></div>
                                <div style="clear:both;"></div>
                        </div>
                </div>



        </body>
</html>

