<?php
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
include $nncms_config_path . 'nn_cms_manager/controls/nncms_controls_public_function.php';
require_once $nncms_db_path . 'nns_log' . DIRECTORY_SEPARATOR . 'nns_db_op_log_class.php';
$log_inst = new nns_db_op_log_class();

$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE, 
		'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$dc->open();
$params = $_REQUEST;
$action = $params['action'];
unset($params['action']);
$log_params = (!empty($params) && is_array($params)) ? json_encode($params) : '';
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);


/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/medium/medium_library_type.class.php';
session_start();
switch ($action)
{
	case "edit":
	    $result_unique = nl_medium_library_type::query_unique($dc, "nns_name='{$params['nns_name']}' and nns_id !='{$params['nns_id']}' ");
	    if($result_unique['ret'] !=0)
	    {
	        echo "<script>alert('" . $result_unique['reason'] . "');history.go(-1);</script>";
	        break;
	    }
	    if(isset($result_unique['data_info'][0]['count']) && $result_unique['data_info'][0]['count'] >0)
	    {
	        echo "<script>alert('数据已经存在');history.go(-1);</script>";
	        break;
	    }
	    $nns_id = $params['nns_id'];
	    unset($params['nns_id']);
		$result = nl_medium_library_type::edit($dc, $params, $nns_id);
		if ($result['ret'])
		{
			echo "<script>alert('" . $result['reason'] . "');history.go(-1);</script>";
		}
		else
		{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "修改:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
			echo "<script>alert('修改成功');</script>";
		}
		break;
	case "add":
		$check_param = array(
			'nns_name' => $params['nns_name'],
		);
	    $result_unique = nl_medium_library_type::query_data($dc, $check_param);
        if($result_unique['ret'] !=0)
	    {
	        echo "<script>alert('" . $result_unique['reason'] . "');history.go(-1);</script>";
	        break;
	    }
	    if(isset($result_unique['data_info']) && is_array($result_unique['data_info']) && !empty($result_unique['data_info']))
	    {
	        echo "<script>alert('数据已经存在');history.go(-1);</script>";
	        break;
	    }
	    $params['nns_id'] = np_guid_rand();
		$result = nl_medium_library_type::add($dc, $params);
		if ($result['ret'])
		{
			echo "<script>alert('" . $result['reason'] . "');</script>";
		}
		else
		{
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "添加:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
			echo "<script>alert('添加成功');</script>";
		}
		break;
	case "delete":
		$nns_ids = explode(",", $params['nns_id']);
		$arr_temp_del = array();
		$str_other=$str_desc=null;
		foreach ($nns_ids as $nnsid)
		{
			if (strlen($nnsid) < 1)
			{
				continue;
			}
			#TODO
			$result_del=nl_medium_library_type::delete($dc,$nnsid);
			if($result_del['ret'] != 0)
			{
				echo "<script>alert('".$result_del['reason']."');history.go(-1);</script>";die;
			}
		}
		$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "删除:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
		$str_dec_last = empty($str_desc) ? "删除成功" : $str_desc;
		echo "<script>alert('".$str_dec_last."');</script>";
		break;
	case "state":
	    $result = nl_medium_library_type::edit($dc, array('nns_state'=>$params['nns_state']), $params['nns_id']);
	    if ($result['ret'])
	    {
	        echo "<script>alert('" . $result['reason'] . "');history.go(-1);</script>";
	    }
	    else
	    {
	        $log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"], $action, $_SESSION["nns_mgr_name"] . "修改:" . $log_params, $_SESSION["nns_manager_type"], $_SESSION["nns_org_id"]);
	        echo "<script>alert('修改成功');</script>";
	    }
	    break;
	default:
	    echo "<script>alert('没有此方式');</script>";
		break;
}
$partner_inst = null;
$log_inst = null;
echo "<script>self.location='".$base_file_name_base."_list.php';</script>";
