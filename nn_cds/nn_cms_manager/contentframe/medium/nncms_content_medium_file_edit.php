<?php
/**
 * Use：介质库文件管理-添加/修改
 * Author：kan.yang@starcor.cn
 * DateTime：18-3-26 上午9:58
 * Description：
 */
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
    $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
//$pri_bool = false;
//$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
//$checkpri = null;
//$nns_id = $_GET["nns_id"];
//$view_type = $_GET["view_type"];
//$group_name = $_GET["group_name"];
//if (!$pri_bool) {
//    Header("Location: ../nncms_content_wrong.php");
//    exit ;
//}
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';

include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$obj_dc = nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));

//引入服务器处理类
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_data_model/medium/m_medium_cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_data_model/medium/m_medium_disk.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/medium/medium_library_type.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/medium/medium_library_filter_extension.class.php';
$obj_medium_cp = new m_medium_cp();$obj_medium_disk = new m_medium_disk();

global $g_bk_web_url;
$bk_web_url = $g_bk_web_url;
unset($g_bk_web_url);
$bk_web_url = strlen($bk_web_url) ? $bk_web_url : '';
$bk_web_url = trim(rtrim($bk_web_url,'/'));
if(strlen($bk_web_url) <1)
{
    echo "<script>alert('全局接口未配置');history.go(-1);</script>";
}
$bk_web_url.="/v2/ns_timer/medium/medium_analyze_file.timer.php";
//查询cp列表
$arr_cp_list = $obj_medium_cp->query_one(array('nns_id'=>$_REQUEST['nns_cp_id']));
if($arr_cp_list['ret'] !=0 || !isset($arr_cp_list['data_info']) || empty($arr_cp_list['data_info']) || !is_array($arr_cp_list['data_info']))
{
    echo "<script>alert('查询CP信息不存在');history.go(-1);</script>";
}
$arr_cp_list = $arr_cp_list['data_info'];
//查询磁盘列表
$arr_disk_list = $obj_medium_disk->query_one(array('nns_id'=>$_REQUEST['nns_disk_id']));
if($arr_disk_list['ret'] !=0 || !isset($arr_disk_list['data_info']) || empty($arr_disk_list['data_info']) || !is_array($arr_disk_list['data_info']))
{
    echo "<script>alert('查询磁盘信息不存在');history.go(-1);</script>";
}
$arr_disk_list = $arr_disk_list['data_info'];
if(!isset($arr_disk_list['nns_disk_path']) || strlen($arr_disk_list['nns_disk_path']) <1)
{
    echo "<script>alert('查询磁盘路径不存在');history.go(-1);</script>";
}
if(!isset($arr_cp_list['nns_cp_name']) || strlen($arr_cp_list['nns_cp_name']) <1)
{
    echo "<script>alert('查询CP名称不存在');history.go(-1);</script>";
}
$arr_categray = nl_medium_library_type::query_by_id($obj_dc,$_REQUEST['nns_categray']);
if($arr_categray['ret'] !=0 || !isset($arr_categray['data_info']) || empty($arr_categray['data_info']) || !is_array($arr_categray['data_info']))
{
    echo "<script>alert('查询影片类型信息不存在，请先添加');history.go(-1);</script>";
}
$arr_categray = $arr_categray['data_info'];
if(!isset($arr_categray['nns_name']) || strlen($arr_categray['nns_name']) <1)
{
    echo "<script>alert('查询影片类型信息不存在，请先添加');history.go(-1);</script>";
}
$result_file_format = nl_medium_library_filter_extension::query_all($obj_dc,0);
if($result_file_format['ret'] !=0)
{
    echo "<script>alert('查询文件后缀过滤条件失败');history.go(-1);</script>";
}
$result_file_format = isset($result_file_format['data_info']) ? $result_file_format['data_info'] : null;
$arr_file_format = null;
if(is_array($result_file_format) && !empty($result_file_format))
{
    foreach ($result_file_format as $value_format)
    {
        $arr_file_format[] = ".".$value_format['nns_extension'];
    }
}
if(empty($arr_file_format) || !is_array($arr_file_format))
{
    echo "<script>alert('查询影片扩展信息不存在，请先添加');history.go(-1);</script>";
}
$str_formart = json_encode($arr_file_format);

$nns_type = $_REQUEST['nns_type'] == '1' ? "单集" : "多集";
$nns_categray = $arr_categray['nns_name'];
$date = date("Ymd");
$nns_base_path = $arr_disk_list['nns_disk_path'].'/'.$arr_cp_list['nns_cp_name'].'/'.$date.'/'.$nns_type.'/'.$nns_categray.'/';
$nns_base_path = str_replace('//','/',str_replace("\\", '/', $nns_base_path));
$arr_disk_list['nns_disk_path'] = urlencode(trim($arr_disk_list['nns_disk_path']));
$arr_cp_list['nns_cp_name'] = urlencode(trim($arr_cp_list['nns_cp_name']));
$nns_type = urlencode(trim($nns_type));
$nns_categray = urlencode(trim($nns_categray));

$nns_base_url = "&diskid={$_REQUEST['nns_disk_id']}&nns_path={$arr_disk_list['nns_disk_path']}&nns_cp={$arr_cp_list['nns_cp_name']}&nns_date={$date}&nns_type={$nns_type}&nns_categray={$nns_categray}";
$nns_base_url = str_replace('//','/',str_replace("\\", '/', $nns_base_url));
ob_clean();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
    <link href="../../plugin/uploads/css/stream-v1.css" rel="stylesheet" type="text/css">
    <style type="text/css">

    </style>
    <script type="text/javascript" src="../../js/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="../../js/cms_cookie.js"></script>
    <script type="text/javascript" src="../../plugin/uploads/js/stream-v1.js"></script>
</head>

<body>
<div class="content">
    <div class="content_position">介质库管理 > 介质库文件上传</div>
    <div class="content_table formtable">
        <div id="i_select_files">
        </div>

        <div id="i_stream_files_queue">
        </div>
        <div>
                    基本消息:
            <div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr>
                            <td class="rightstyle">所属CP</td>
                            <td>
                                <?php echo urldecode($arr_cp_list['nns_cp_name']);?>
                            </td>
                        </tr>
                        <tr>
                            <td class="rightstyle">服务器磁盘</td>
                            <td>
                                <?php echo urldecode($arr_disk_list['nns_disk_name']);?>
                            </td>
                        </tr>
                        <tr>
                            <td class="rightstyle">分类类型</td>
                            <td>
                                <?php echo urldecode($nns_type);?>
                            </td>
                        </tr>
                        <tr>
                            <td class="rightstyle">分类类型</td>
                            <td>
                                <?php echo urldecode($nns_categray);?>
                            </td>
                        </tr>
                        <tr>
                            <td class="rightstyle">上传基本路径</td>
                            <td>
                                <?php echo $nns_base_path;?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
                    上传消息:
            <div id="i_stream_message_container" class="stream-main-upload-box" style="overflow: auto;height:200px;">
            </div>
        </div>
    <div class="controlbtns">
        <div class="controlbtn display"><a onclick="javascript:_t.upload();">开始上传</a></div>
        <div class="controlbtn nodisplay"><a onclick="javascript:_t.stop();">停止上传</a></div>
        <div class="controlbtn cancel"><a onclick="javascript:_t.cancel();">取消上传</a></div>
        <div class="controlbtn back"><a href="javascript:returnToHistory();">返回</a></div>
        <div style="clear:both;"></div>
    </div>
</div>
<script type="text/javascript">
    var config = {
        browseFileId : "i_select_files",
        browseFileBtn : "<div>请选择文件</div>",
        dragAndDropArea: "i_select_files",
        dragAndDropTips: "<span>把文件(文件夹)拖拽到这里</span>",
        filesQueueId : "i_stream_files_queue",
        filesQueueHeight : 400,
        messagerId : "i_stream_message_container",
        multipleFiles: true,
        autoUploading: false,
        autoRemoveCompleted : false,
        retryCount : 5,
        swfURL : "../../plugin/uploads/swf/FlashUploader.swf",
        tokenURL : "nncms_content_medium_file_edit_control.php?action=get_token<?php echo $nns_base_url;?>",
        frmUploadURL : "nncms_content_medium_file_edit_control.php?action=fd<?php echo $nns_base_url;?>",
        uploadURL : "nncms_content_medium_file_edit_control.php?action=upload_file<?php echo $nns_base_url;?>",
        simLimit: 100,
        extFilters: <?php echo $str_formart;?>,
        isNeedAJAX : true,
        AJAXQueryURL : "<?php echo $bk_web_url;?>",
        BaseStorageURL : "<?php echo $nns_base_path;?>",
    };
    var _t = new Stream(config);
</script>
</body>
</html>

