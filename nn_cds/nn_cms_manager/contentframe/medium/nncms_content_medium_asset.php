<?php
/**
 * Use：介质库分类管理
 * Author：kan.yang@starcor.cn
 * DateTime：18-3-26 下午15:00
 * Description：
 */
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
    $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
//$pri_bool = false;
//$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
//$checkpri = null;
//$nns_id = $_GET["nns_id"];
//$view_type = $_GET["view_type"];
//$group_name = $_GET["group_name"];
//if (!$pri_bool) {
//    Header("Location: ../nncms_content_wrong.php");
//    exit ;
//}
include_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$obj_dc = nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
//引入服务器处理类
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_data_model/medium/m_medium_asset.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_data_model/medium/m_medium_cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_data_model/medium/m_medium_disk.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_data_model/medium/m_medium_file.class.php';
$obj_medium_disk = new m_medium_disk(); 
$obj_medium_cp = new m_medium_cp();
$obj_medium_asset = new m_medium_asset();
$obj_medium_file = new m_medium_file();
//CP集合
$arr_server_list = array();
//查询条件
$arr_search_params = array();

//CP集合
$arr_cp_list = $obj_medium_cp->query_list(array(),null);
if($arr_cp_list['ret'] == 0 && !empty($arr_cp_list['data_info']))
{
    $arr_cp_list = $arr_cp_list['data_info'];
}
else
{
    $arr_cp_list = array();
}
$temp_cp_list = array();
if(!empty($arr_cp_list) && is_array($arr_cp_list))
{
    foreach ($arr_cp_list as $cp_data)
    {
        $temp_cp_list[$cp_data['nns_id']] = $cp_data['nns_cp_name'];
    }
}


$arr_disk_list = $obj_medium_disk->query_list(array(),null);
if($arr_disk_list['ret'] == 0 && !empty($arr_disk_list['data_info']))
{
    $arr_disk_list = $arr_disk_list['data_info'];
}
else
{
    $arr_disk_list = array();
}
$temp_disk_list = array();
if(!empty($arr_disk_list) && is_array($arr_disk_list))
{
    foreach ($arr_disk_list as $cp_data)
    {
        $temp_disk_list[$cp_data['nns_id']] = $cp_data['nns_disk_name'];
    }
}
//跳转界面
$str_server_list_url = 'window.location.href = \'nncms_content_medium_asset.php?';
//名称
if(isset($_REQUEST['nns_name']) && strlen($_REQUEST['nns_name']) > 0)
{
    $arr_search_params['nns_asset_name'] = $_REQUEST['nns_name'];
    $str_server_list_url .= '&nns_name=' . $_REQUEST['nns_name'];
}
//状态
if(isset($_REQUEST['nns_state']) && strlen($_REQUEST['nns_state']) > 0)
{
    $arr_search_params['nns_state'] = $_REQUEST['nns_state'];
    $str_server_list_url .= '&nns_state=' . $_REQUEST['nns_state'];
}
//状态
if(isset($_REQUEST['nns_cp_id']) && strlen($_REQUEST['nns_cp_id']) > 0)
{
    $arr_search_params['nns_cp_id'] = $_REQUEST['nns_cp_id'];
    $str_server_list_url .= '&nns_cp_id=' . $_REQUEST['nns_cp_id'];
}//状态
if(isset($_REQUEST['nns_disk_id']) && strlen($_REQUEST['nns_disk_id']) > 0)
{
    $arr_search_params['nns_cp_id'] = $_REQUEST['nns_disk_id'];
    $str_server_list_url .= '&nns_disk_id=' . $_REQUEST['nns_disk_id'];
}
//开始时间
if(isset($_REQUEST['day_picker_start']) && strlen($_REQUEST['day_picker_start']) > 0)
{
    $arr_search_params['nns_start_time'] = $_REQUEST['day_picker_start'];
    $str_server_list_url .= '&day_picker_start=' . $_REQUEST['day_picker_start'];
}
//结束时间
if(isset($_REQUEST['day_picker_end']) && strlen($_REQUEST['day_picker_end']) > 0)
{
    $arr_search_params['nns_end_time'] = $_REQUEST['day_picker_end'];
    $str_server_list_url .= '&day_picker_end=' . $_REQUEST['day_picker_end'];
}
$str_server_list_url .= '\';';
if(isset($_REQUEST['action']))
{
    $str_action_name = '';
    $arr_action_ret = array('ret' => 1, 'reason' => '参数错误');
    switch(strtolower($_REQUEST['action']))
    {
        case 'delete':
            $str_action_name= '删除介质库分类信息';
            $arr_id = explode(',',rtrim($_REQUEST['nns_id']));
            foreach ($arr_id as $key=>$value)
            {
                $result_server_disk = $obj_medium_file->query_one(array('nns_asset_id'=>$value));
                if(isset($result_server_disk['data_info']) && !empty($result_server_disk['data_info']) && is_array($result_server_disk['data_info']))
                {
                    unset($arr_id[$key]);
                }
            }
            if(is_array($arr_id) && !empty($arr_id))
            {
                $arr_action_ret = $obj_medium_asset->del(array('nns_id' => $arr_id));
            }
            else
            {
                $arr_action_ret = array('ret'=>0);
            }
            break;
        case 'modify_state':
            $arr_action_ret = $obj_medium_asset->edit(array('nns_id' => explode(',',rtrim($_REQUEST['nns_id']))),array(
                'nns_state' => isset($_REQUEST['modify_state']) ? $_REQUEST['modify_state'] : null,
            ));
            $str_action_name= '更新介质库分类状态';
            break;
        default:
            break;
    }
    //成功返回
    if($arr_action_ret['ret'] == 0)
    {
        echo '<script>alert("' . $str_action_name . '：成功");' . $str_server_list_url . '</script>';die;
    }
    else
    {
        echo '<script>alert("' . $str_action_name . '：失败");' . $str_server_list_url . '</script>';die;
    }
}
else
{
    //分页信息
    $page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 12;
    $page_num = isset($_GET['page']) && $_GET['page'] >= 1 ? $_GET['page'] - 1 : 0;
    //接口调用
    $arr_server_list = $obj_medium_asset->query_list($arr_search_params,array($page_num,$page_size));
    if($arr_server_list['ret'] != 0 || empty($arr_server_list['data_info']))
    {
        $vod_total_num = 0;
        $arr_server_list = array();
    }
    else
    {
        $vod_total_num = $arr_server_list['page_info']['total_count'];
        $arr_server_list = $arr_server_list['data_info'];
    }
    //分页处理
    $vod_pages = ceil($vod_total_num / $page_size);
    if (($page_num + 1) > $vod_pages)
    {
        $page_num = $vod_pages;
    }
    $currentpage = $page_num + 1;
    $g_manager_list_max_num = $page_size;
}
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
    <style>
        .mytip_ul
        {
            text-align: left;
            font-size: 12px;
            list-style-type: none;
            padding: 0;
            margin: 0;
        }

        .mytip_title
        {
            margin-left: 10px;
            width: 100px;
            float: left;
            text-align: left;
        }

        .mytip_close
        {
            margin-right: 10px;
            margin-top: 5px;
            width: 100px;
            float: right;
            text-align: right;
            cursor: pointer;
        }

        .mytip_top
        {
            background-image: url(../../images/tips_titlebj.gif);
            color: #FFFFFF;
            font-size: 12px;
            height: 24px;
            line-height: 24px;
            font-weight: 600;
        }

        .tips_li
        {
            height: 25px;
            line-height: 25px;
            border-bottom: #ccc 1px solid;
        }

        .tips_li_span
        {
            color: #07507d;
            margin-left: 10px;
        }

        .tips_li_span2
        {
            color: #6d6d6d;
        }
        .myalert_div1
        {
            height: 25px;
            line-height: 25px;
            border-bottom: #ccc 1px solid;
            width: 620px;
        }

        .myalert_div1_leftDiv
        {
            float: left;
            width: 50%;
            margin-left: 10px;
        }

        .myalert_div1_rightDIv
        {
            float: left;
            margin-left: 10px;
        }

        .myalert_div1_Div
        {
            float: left;
            width: 100%;
            margin-left: 10px;
        }

        .myalert_div2
        {
            height: 84px;
            line-height: 70px;
            border-bottom: #ccc 1px solid;
            width: 620px;
        }

        .myalert_div2_Div
        {
            float: left;
            width: 100%;
            margin-left: 10px;
            height: 84px;
        }

        .myaelrt_li_title
        {
            color: #07507d;
        }

        .myalert_li_txt
        {
            color: #6d6d6d;
        }

        .myalert_inputTxt
        {
            width: 220px;
            border: 0px solid #eff0f0;
            padding-left: 2px;
            background-color: #eff0f0;
            color: #6d6d6d;
        }
    </style>
    <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
    <script language="javascript" src="../../js/cms_cookie.js"></script>
    <script language="javascript" src="../../js/table.js.php"></script>
    <script language="javascript" src="../../js/checkinput.js.php"></script>
    <script language="javascript" src="../../js/rate.js"></script>
    <script language="javascript" src="../../js/image_loaded_func.js"></script>
    <script language="javascript" src="../../js/cms_datepicker.js"></script>
    <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
    <script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
    <script language="javascript">
        function del(str_id)
        {
            if(!str_id)
            {
                str_id=getAllCheckBoxSelect();
            }
            //验证参数是否有效
            if(!str_id)
            {
                alert('请至少选择一条数据');
                return false;
            }
            var r=confirm("是否进行该操作");
            if(r == true)
            {
                window.location.href = "nncms_content_medium_asset.php?action=delete&nns_id=" + str_id;
            }
            return true;
        }
        function modify_state(str_id,$int_state)
        {
            //验证参数是否有效
            if(!str_id || !$int_state)
            {
                alert('请至少选择一条数据');
                return false;
            }
            window.location.href = "nncms_content_medium_asset.php?action=modify_state&nns_id=" + str_id + "&modify_state=" + $int_state;
            return true;
        }
        function refresh_vod_page() {
            var num = $("#nns_list_max_num").val();
            window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
        }
        //页面加载完成后执行
        $(document).ready(function() {
            $('#clear_time').click(function(){
                $('#day_picker_start').val('');
                $('#day_picker_end').val('');

            });
            window.parent.now_frame_url = window.location.href;
            window.parant.resetFrameHeight();
        });
    </script>

</head>

<body>
<div class="content">
    <div class="content_position">
        介质库分类管理
    </div>
    <div class="content_table">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <form action="" method="get">
                <tbody>
                <tr>
                    <td>
                        <label>分类名称：</label><input type="text" name="nns_name" id="nns_name" value="<?php echo $_GET['nns_name']; ?>" style="width:200px;">
                        <label>所属CP：</label><select name="nns_cp_id" style="width: 150px;">
                                            <option value="" >全部</option>
                                            <?php foreach($arr_cp_list as $cp){?>
                                                <option value="<?php echo $cp['nns_id']; ?>" <?php if(isset($_GET['nns_cp_id']) && intval($_GET['nns_cp_id']) == $cp['nns_id']) echo 'selected="selected"';?>>
                                                    <?php echo $cp['nns_cp_name']; ?>
                                                </option>
                                            <?php }?>
                                        </select>
                        <label>所属磁盘：</label><select name="nns_disk_id" style="width: 150px;">
                                            <option value="" >全部</option>
                                            <?php foreach($arr_disk_list as $cp){?>
                                                <option value="<?php echo $cp['nns_id']; ?>" <?php if(isset($_GET['nns_disk_id']) && intval($_GET['nns_disk_id']) == $cp['nns_id']) echo 'selected="selected"';?>>
                                                    <?php echo $cp['nns_disk_name']; ?>
                                                </option>
                                            <?php }?>
                                        </select>
                        <label>状态：</label><select name="nns_state" style="width: 150px;">
                            <option value="" >全部</option>
                            <option value="0" <?php if(isset($_GET['nns_state']) && strlen($_GET['nns_state']) >0 && $_GET['nns_state'] == '0') echo 'selected="selected"';?>>有效</option>
                            <option value="1" <?php if(isset($_GET['nns_state']) && strlen($_GET['nns_state']) >0 && $_GET['nns_state'] == '1') echo 'selected="selected"';?>>无效</option>
                        </select>
                        <label>选择时间段：</label><input name="day_picker_start" id="day_picker_start" type="text"  value="<?php
                        if (isset($_GET['day_picker_start']))
                            echo $_GET['day_picker_start'];
                        ?>" style="width:120px;" class="datetimepicker" callback="test" />
                        - <input name="day_picker_end" id="day_picker_end" type="text"  value="<?php
                        if (isset($_GET['day_picker_end']))
                            echo $_GET['day_picker_end'];
                        ?>" style="width:120px;" class="datetimepicker" callback="test" />
                        <input type="button" id="clear_time" name="clear_time" value="清除时间"/>
                        <input type="submit" value="<?php echo cms_get_lang('search'); ?>"  />
                    </td>
                </tr>
                </tbody>
            </form>
        </table>
    </div>
    <div class="content_table formtable">

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <th><input name="" type="checkbox" value="">序号</th>
                <th>CP名称</th>
                <th>磁盘名称</th>
                <th>分类名称</th>
                <th>分类路径名称</th>
                <th>分类类型</th>
                <th>创建时间</th>
                <th>状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(isset($arr_server_list) && !empty($arr_server_list))
            {
                $num = $page_num * $page_size;
                foreach ($arr_server_list as $item)
                {
                    $num++;
                    ?>
                    <tr>
                        <td id="is_delete"><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
                        <td><?php echo isset($temp_cp_list[$item['nns_cp_id']]) ? $temp_cp_list[$item['nns_cp_id']] : '<font color="red">未知CP</font>'; ?></td>
                        <td><?php echo isset($temp_disk_list[$item['nns_disk_id']]) ? $temp_disk_list[$item['nns_disk_id']] : '<font color="red">未知磁盘</font>'; ?></td>
                        <td><?php echo $item['nns_asset_name']; ?></td>
                        <td><?php echo $item['nns_asset_path']; ?></td>
                        <td><?php echo isset($item['nns_asset_type']) && $item['nns_asset_type'] == 1 ? '多集' : '单集'; ?></td>
                        <td><?php echo $item['nns_create_time']; ?></td>
                        <td>
                            <a href="javascript:;" onclick="modify_state('<?php echo $item['nns_id']; ?>','<?php echo isset($item['nns_state']) && $item['nns_state'] == 1 ? 0 : 1; ?>')"><?php echo isset($item['nns_state']) && $item['nns_state'] == 1 ? '禁用' : '开启'; ?></a>
                        </td>
                        <td class="control_btns" name="<?php echo cms_get_lang('action'); ?>">
                            <a href="nncms_content_medium_asset_edit.php?nns_id=<?php echo $item['nns_id'];?>">修改</a>
                            <a href="javascript:;" onclick="del('<?php echo $item['nns_id']; ?>')">删除</a>
                        </td>
                    </tr>
                <?php
                }
            }
            $least_num = $g_manager_list_max_num - count($arr_server_list);
            for ($i = 0; $i < $least_num; $i++)
            {
                ?>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp; </td>
                    <td>&nbsp; </td>
                    <td>&nbsp;</td><td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="pagecontrol">
        共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
        <?php if ($currentpage > 1) { ?>
            <a href="nncms_content_medium_asset.php?page=1<?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="nncms_content_medium_asset.php?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php } else { ?>
            <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
            <span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php } if ($currentpage < $vod_pages) { ?>
            <a href="nncms_content_medium_asset.php?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="nncms_content_medium_asset.php?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>" target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php } else { ?>
            <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
            <span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php } ?>

        <?php echo cms_get_lang('jump_to'); ?>
        <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage; ?>" type="text" style="width:20px;"/>
        <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
        <a href="javascript:go_page_num('nncms_content_medium_asset.php?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
        <?php echo cms_get_lang('current'); ?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
        <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
        <input name="nns_list_max_num" id="nns_list_max_num" type="text"
               value="<?php echo $g_manager_list_max_num; ?>" style="width:24px;"/>&nbsp;&nbsp;
        <input type="button" value="<?php echo cms_get_lang('confirm'); ?>"
               onclick="refresh_vod_page();"/>&nbsp;&nbsp;

    </div>

    <div class="controlbtns">
        <div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
        <div class="controlbtn add"><a href="nncms_content_medium_asset_edit.php">添加</a></div>
        <div class="controlbtn delete"><a href="javascript:del();">删除</a></div>
        <div style="clear:both;"></div>
    </div>

</div>
</body>
</html>
