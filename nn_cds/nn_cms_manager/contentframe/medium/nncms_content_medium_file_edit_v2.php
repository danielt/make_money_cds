<?php
/**
 * Created by PhpStorm.
 * User: song.wang
 * Date: 2018/9/3
 * Time: 23:27
 */
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
    $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$obj_dc = nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
//引入文件处理类
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_data_model/medium/m_medium_file.class.php';
$obj_medium_file = new m_medium_file();
//引入CP处理类
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_data_model/medium/m_medium_cp.class.php';
$obj_medium_cp = new m_medium_cp();
$str_server_list_url = 'window.location.href = "nncms_content_medium_file.php";';
if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit')
{
    if($_REQUEST['nns_copyright_start_time'] >= $_REQUEST['nns_copyright_end_time'])
    {
        echo '<script>alert("版权开始日期必须小于结束日期");' . $str_server_list_url . '</script>';die;
    }
    //修改
    $arr_modify_ret = $obj_medium_file->edit(array('nns_id' => $_REQUEST['nns_id']),array(
        'nns_file_name'        => isset($_REQUEST['nns_file_name']) ? $_REQUEST['nns_file_name'] : '',
        'nns_cp_id'      => isset($_REQUEST['nns_cp_id']) ? $_REQUEST['nns_cp_id'] : '',
        'nns_create_time'      => isset($_REQUEST['nns_create_time']) ? $_REQUEST['nns_create_time'] : '',
        'nns_file_url' => isset($_REQUEST['nns_file_url']) ? $_REQUEST['nns_file_url'] : '',
        'nns_copyright_start_time'   => isset($_REQUEST['nns_copyright_start_time']) ? $_REQUEST['nns_copyright_start_time'] : '',
        'nns_copyright_end_time'   => isset($_REQUEST['nns_copyright_end_time']) ? $_REQUEST['nns_copyright_end_time'] : '',
        'nns_summary'   => isset($_REQUEST['nns_summary']) ? $_REQUEST['nns_summary'] : '',
    ));
    if($arr_modify_ret['ret'] == 0)
    {
        echo '<script>alert("更新文件信息成功");' . $str_server_list_url . '</script>';die;
    }
    else
    {
        echo '<script>alert("更新文件信息失败");</script>';die;
    }
}
if(isset($_REQUEST['nns_id']) && strlen($_REQUEST['nns_id']) > 0)
{
    //1、查出文件基本信息
    $arr_medium_file_info = $obj_medium_file->query_one(array('nns_id' => $_REQUEST['nns_id']));
    if($arr_medium_file_info['ret'] == 0 && !empty($arr_medium_file_info['data_info']))
    {
        $arr_medium_file_info = $arr_medium_file_info['data_info'];
    }
    else
    {
        echo '<script>alert("更新文件信息失败：根据文件ID查询文件信息失败");' . $str_server_list_url . '</script>';
        die;
    }
    //2、查出所有CP
    $arr_cp_list = $obj_medium_cp->query_list(array(),null);
    if($arr_cp_list['ret'] == 0 && !empty($arr_cp_list['data_info']))
    {
        $arr_cp_list = $arr_cp_list['data_info'];
    }
    else
    {
        echo '<script>alert("更新文件信息失败：查询CP列表失败");' . $str_server_list_url . '</script>';
        die;
    }
    $temp_cp_list = array();
    if(!empty($arr_cp_list) && is_array($arr_cp_list))
    {
        foreach ($arr_cp_list as $cp_data)
        {
            $temp_cp_list[$cp_data['nns_id']] = $cp_data['nns_cp_name'];
        }
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
    <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
    <link href="../../css/comm.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../js/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="../../js/cms_cookie.js"></script>
    <script type="text/javascript" src="../../js/table.js.php"></script>
    <script type="text/javascript" src="../../js/checkinput.js.php"></script>
    <script type="text/javascript" src="../../js/rate.js"></script>
    <script type="text/javascript" src="../../js/image_loaded_func.js"></script>
    <script type="text/javascript" src="../../js/cms_datepicker_v2.js"></script>
    <script type="text/javascript" src="../../js/alertbox.js"></script>
    <script type="text/javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript">
        $("#nns_copyright_end_time").
    </script>
</head>
<body>
<div class="selectbox" style="display: none;">
    <iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
</div>
<div class="content">
    <div class="content_position">介质库管理 > 文件管理 > 修改文件</div>
    <form id="add_form" action="nncms_content_medium_file_edit_v2.php?action=edit" method="post" enctype="multipart/form-data">
        <input name="nns_id" id="nns_id" type="hidden" value="<?php echo $_REQUEST['nns_id'];?>"/>
        <input name="action" id="action" type="hidden" value="edit"/>
        <div class="content_table">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>

                <tr>
                    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>文件名称</td>
                    <td>
                        <input name="nns_file_name" id="nns_file_name" type="text" value="<?php echo isset($arr_medium_file_info['nns_file_name']) ? $arr_medium_file_info['nns_file_name'] : ''; ?>" rule="noempty"/>
                    </td>
                </tr>

                <tr>
                    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>内容提供商</td>
                    <td>
                        <select name="nns_cp_id" style="width: 150px;">
                            <option value="" >全部</option>
                            <?php foreach($arr_cp_list as $cp){?>
                                <option value="<?php echo $cp['nns_id']; ?>" <?php if(isset($arr_medium_file_info['nns_cp_id']) && intval($arr_medium_file_info['nns_cp_id']) == $cp['nns_id']) echo 'selected="selected"';?>>
                                    <?php echo $cp['nns_cp_name']; ?>
                                </option>
                            <?php }?>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>上传日期</td>
                    <td>
                        <input name="nns_create_time" id="nns_create_time" type="text" value="<?php
                               if (isset($arr_medium_file_info['nns_create_time']))
                                   echo $arr_medium_file_info['nns_create_time'];
                               ?>" style="width: 120px;" class="datetimepicker" callback="test" />
                    </td>
                </tr>

                <tr>
                    <td class="rightstyle">文件路径</td>
                    <td>
                        <input name="nns_file_url" id="nns_file_url" type="text" value="<?php echo isset($arr_medium_file_info['nns_file_url']) ? $arr_medium_file_info['nns_file_url'] : ''; ?>" rule="noempty"/>
                    </td>
                </tr>

                <tr>
                    <td class="rightstyle">版权开始日期</td>
                    <td>
                        <input name="nns_copyright_start_time" id="nns_copyright_start_time" type="text" value="<?php
                        if (isset($arr_medium_file_info['nns_copyright_start_time']))
                            echo $arr_medium_file_info['nns_copyright_start_time'];
                        ?>" style="width: 120px;" class="datetimepicker" callback="test" />
                    </td>
                </tr>

                <tr>
                    <td class="rightstyle">版权结束日期</td>
                    <td>
                        <input name="nns_copyright_end_time" id="nns_copyright_end_time" type="text" value="<?php
                        if (isset($arr_medium_file_info['nns_copyright_end_time']))
                            echo $arr_medium_file_info['nns_copyright_end_time'];
                        ?>" style="width: 120px;" class="datetimepicker" callback="test" />
                    </td>
                </tr>

                <tr>
                    <td class="rightstyle">备注</td>
                    <td>
                        <textarea name="nns_summary" id="nns_summary"><?php echo isset($arr_medium_file_info['nns_summary']) ? $arr_medium_file_info['nns_summary'] : ''; ?></textarea>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
    </form>
    <div class="controlbtns">
        <div class="controlbtn edit"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','<?php echo cms_get_lang('webkz_qrccxg'); ?>',$('#add_form'),'edit');">修改</a></div>
        <div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        <div style="clear:both;"></div>
    </div>
</div>

</body>
</html>
