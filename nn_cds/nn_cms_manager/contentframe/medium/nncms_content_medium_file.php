<?php 
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
	$language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool) {
	Header("Location: ../nncms_content_wrong.php");
	exit ;
}
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -5);
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
		"db_policy" => NL_DB_WRITE,
		"cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));
$page_size = isset($_GET["view_list_max_num"]) ? $_GET["view_list_max_num"] : 18;
$page_num = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page_num < 1) {
	$page_num = 1;
}
$offset = intval($page_num - 1) * $page_size;
$limit_array = array(
		'page_size'=>$page_size,
		'page_num'=> $page_num,
);
function export_csv($data,$cp_data)
{
    $header = array (
        'CP/SP名称',
        '分集号',
        '文件名称',
        '文件路径',
        '文件大小',
        '文件时长',
        '文件全局格式',
        '文件码率模式',
        '视屏码率',
        '视屏格式',
        '视屏码率模式',
        '音频码率',
        '音频格式',
        '音频码率模式',
    );
    $filename=date('Ymd_His').'_medium_file.csv';
    header('Content-Type: application/vnd.ms-excel');
    header("Content-Disposition: attachment;filename=". $filename);
    header('Cache-Control: max-age=0');
    print(chr(0xEF).chr(0xBB).chr(0xBF));
    ob_start();
    $fp = fopen('php://output', 'a');
    if(!empty($header))
    {
        fputcsv($fp, $header);
    }
    foreach ($data as $_v)
    {
        $_v['nns_cp_id'] = isset($cp_data[$_v['nns_cp_id']]) ? $cp_data[$_v['nns_cp_id']] : '未知CP/SP';
        fputcsv($fp, array_values($_v));
    }
    unset($fp);
    unset($data);
    ob_flush();
    flush();
    return ;
}
function pub_func_get_desc_alt($title,$item)
{
    $alt_html = '<div style="width:300px;padding:5px;"><div style="font-size:16px;font-weight:bold;line-height:22px;"></div>';
    $alt_html.='<div style="font-size:12px;line-height:20px;">' .
        '<div style="float:left;margin:0px 5px 5px 0px;display:block;">';
    $alt_html.='</div>';

    $alt_html.='<p><b>'.$title.'</b><br/>';
    $arr = array(
        'nns_file_size'=>'文件大小/byte',
        'nns_file_duration'=>'文件时长/秒',
        'nns_file_type'=>'文件类型',
        'nns_general_fomart'=>'文件格式',
        'nns_general_bit_rate_mode'=>'文件码率模式',
        'nns_videos_kbps'=>'视屏码率bit/s',
        'nns_videos_format'=>'视屏格式',
        'nns_videos_bit_rate_mode'=>'视屏码率模式',
        'nns_audios_kbps'=>'音频码率bit/s',
        'nns_audios_format'=>'音频格式',
        'nns_audios_bit_rate_mode'=>'音频码率模式',
        'nns_is_analysis'=>'解析状态',
        'nns_state'=>'文件状态',
    );
    if(is_array($item))
    {
        foreach ($arr as $key=>$val)
        {
            $temp_value = isset($item[$key]) ? $item[$key] : '';
            if(in_array($key,array('nns_is_analysis','nns_state')))
            {
                $temp_value = get_state_change($key,$temp_value);
            }
            $alt_html.="{$val} => {$temp_value} <br/>";
        }
    }
    $alt_html.='</p>';
    $alt_html.='</div></div>';
    return htmlspecialchars($alt_html);
}
 function get_state_change($type,$value)
{
    $result = "";
    if($type == 'nns_is_analysis')
    {
        switch ($value)
        {
            case 0:
                $result = '未解析';
                break;
            case 1:
                $result = '解析成功';
                break;
            case 2:
                $result = '解析失败';
                break;
        }
    }
    elseif($type == 'nns_state')
    {
        switch ($value)
        {
            case 0:
                $result = '启用';
                break;
            case 1:
                $result = '禁用';
                break;
        }
    }
    return $result;
}


//引入服务器处理类
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_data_model/medium/m_medium_file.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_data_model/medium/m_medium_disk.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_data_model/medium/m_medium_cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_data_model/medium/m_medium_asset.class.php';
$obj_medium_file = new m_medium_file();$obj_medium_disk = new m_medium_disk();
$obj_medium_cp = new m_medium_cp();$obj_medium_asset = new m_medium_asset();

//文件名称
if (isset($_REQUEST['nns_file_name']) && (strlen($_REQUEST['nns_file_name']) >0))
{
    $filter_array['nns_file_name'] = $_REQUEST['nns_file_name'];
}
//内容提供商
if (isset($_REQUEST['nns_cp_id']) && (strlen($_REQUEST['nns_cp_id']) >0))
{
    $filter_array['nns_cp_id'] = $_REQUEST['nns_cp_id'];
}
//磁盘
if(isset($_REQUEST['nns_disk_id']) && strlen($_REQUEST['nns_disk_id']) > 0)
{
    $filter_array['nns_disk_id'] = $_REQUEST['nns_disk_id'];
}
//文件分类
if(isset($_REQUEST['nns_asset_id']) && strlen($_REQUEST['nns_asset_id']) > 0)
{
    $filter_array['nns_asset_id'] = $_REQUEST['nns_asset_id'];
}
//解析状态
if(isset($_REQUEST['nns_is_analysis']) && strlen($_REQUEST['nns_is_analysis']) > 0)
{
    $filter_array['nns_is_analysis'] = $_REQUEST['nns_is_analysis'];
}
//状态
if(isset($_REQUEST['nns_state']) && strlen($_REQUEST['nns_state']) > 0)
{
    $filter_array['nns_state'] = $_REQUEST['nns_state'];
}
//开始时间
if(isset($_REQUEST['day_picker_start']) && strlen($_REQUEST['day_picker_start']) > 0)
{
    $filter_array['nns_start_time'] = $_REQUEST['day_picker_start'];
}
//结束时间
if(isset($_REQUEST['day_picker_end']) && strlen($_REQUEST['day_picker_end']) > 0)
{
    $filter_array['nns_end_time'] = $_REQUEST['day_picker_end'];
}
//文件类型
if(isset($_REQUEST['nns_file_type']) && strlen($_REQUEST['nns_file_type']) > 0)
{
    $filter_array['nns_file_type'] = $_REQUEST['nns_file_type'];
}
//CP集合
$arr_cp_list = $obj_medium_cp->query_list(array(),null);
if($arr_cp_list['ret'] == 0 && !empty($arr_cp_list['data_info']))
{
    $arr_cp_list = $arr_cp_list['data_info'];
}
else
{
    $arr_cp_list = array();
}
$temp_cp_list = array();
if(!empty($arr_cp_list) && is_array($arr_cp_list))
{
    foreach ($arr_cp_list as $cp_data)
    {
        $temp_cp_list[$cp_data['nns_id']] = $cp_data['nns_cp_name'];
    }
}
//磁盘集合
$arr_disk_list = $obj_medium_disk->query_list(array(),null);
if($arr_disk_list['ret'] == 0 && !empty($arr_disk_list['data_info']))
{
    $arr_disk_list = $arr_disk_list['data_info'];
}
else
{
    $arr_disk_list = array();
}
//dump($_REQUEST);exit;
if(isset($_REQUEST['action']) && strlen($_REQUEST['action']) >1)
{
    $str_action_name = '';
    $arr_action_ret = array('ret' => 1, 'reason' => '参数错误');
    switch(strtolower($_REQUEST['action']))
    {

        case 'delete':
            $str_action_name= '删除介质库文件信息';
            $arr_action_ret = $obj_medium_file->del(array('nns_id' => explode(',',rtrim($_REQUEST['nns_id']))));
            break;
        case 'modify_state':
            $arr_action_ret = $obj_medium_file->edit(array('nns_id' => explode(',',rtrim($_REQUEST['nns_id']))),array(
            'nns_state' => isset($_REQUEST['modify_state']) ? $_REQUEST['modify_state'] : null,
            ));
            $str_action_name= '更新介质库文件状态';
            break;
        case 'analysis':
            $arr_action_ret = $obj_medium_file->analysis(array('nns_id' => explode(',',rtrim($_REQUEST['nns_id'])),'nns_state'=>0));
            $str_action_name= '解析文件';
            break;
        case "export":
            $result = $obj_medium_file->query_export($filter_array);
            export_csv($result['data_info'],$temp_cp_list);die;
        default:
            break;
    }
    //成功返回
    if($arr_action_ret['ret'] == 0)
    {
        echo "<script>alert('" . $str_action_name . "：成功');history.go(-1);</script>";die;
    }
    else
    {
        echo "<script>alert('" . $str_action_name . "：失败');history.go(-1);</script>";die;
    }
}


$temp_server_group=array();
$arr_server_group = $obj_medium_file->query_sql("select * from nns_medium_library_file where nns_file_type is not null and nns_file_type !='' group by nns_file_type");
if($arr_server_group['ret'] == 0 && is_array($arr_server_group['data_info']))
{
    foreach ($arr_server_group['data_info'] as $cp_data)
    {
        $temp_server_group[] = $cp_data['nns_file_type'];
    }
}
$arr_server_list = $obj_medium_file->query_list($filter_array,array($page_num-1,$page_size));
if($arr_server_list['ret'] != 0 || empty($arr_server_list['data_info']))
{
    $vod_total_num = 0;
    $arr_server_list = array();
}
else
{
    $vod_total_num = $arr_server_list['page_info']['total_count'];
    $arr_server_list = $arr_server_list['data_info'];
}
$vod_pages = ceil($vod_total_num / $page_size);
//总页数
if ($page_num > $vod_pages) {
	$page_num = $vod_pages;
}
$currentpage = $page_num;
$g_manager_list_max_num = $page_size;

$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);

// $data = $result['data']; 
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet"
	type="text/css" />
<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
<link href="../../css/public.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/rate.js"></script>
<script language="javascript" src="../../js/image_loaded_func.js"></script>
<script language="javascript" src="../../js/cms_datepicker.js"></script>
<script language="javascript" src="../../js/alertbox.js"></script>
<script language="javascript">
			function refresh_vod_page() {
				var num = $("#nns_list_max_num").val();
				window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
			}
			function checkhiddenBox(type) {
				BoxKey = false;
				$("input.checkhiddenInput:checked").each(function() {
					if ($(this).attr('rel') == type) {
						BoxKey = true;
						return false;
					}
				})
				return BoxKey;
			}
			function del(str_id)
	        {
	            if(!str_id)
	            {
	                str_id=getAllCheckBoxSelect();
	            }
	            //验证参数是否有效
	            if(!str_id)
	            {
	                alert('请至少选择一条数据');
	                return false;
	            }
	            var r=confirm("是否进行该操作");
	            if(r == true)
	            {
	                window.location.href = "nncms_content_medium_file.php?action=delete&nns_id=" + str_id;
	            }
	            return true;
	        }
//            function edit(str_id)
//            {
//                if(!str_id)
//                {
//                    alert('请至少选择一条数据');
//                    return false;
//                }
//                window.location.href = "nncms_content_medium_file.php?action=delete&nns_id=" + str_id;
//                return true;
//            }
	        function modify_state(str_id,$int_state)
	        {
	            //验证参数是否有效
	            if(!str_id || !$int_state)
	            {
	                alert('请至少选择一条数据');
	                return false;
	            }
	            window.location.href = "nncms_content_medium_file.php?action=modify_state&nns_id=" + str_id + "&modify_state=" + $int_state;
	            return true;
	        }
	        function analysis(str_id)
	        {
	            if(!str_id)
	            {
	                str_id=getAllCheckBoxSelect();
	            }
	            //验证参数是否有效
	            if(!str_id)
	            {
	                alert('请至少选择一条数据');
	                return false;
	            }
	            var r=confirm("是否进行该操作");
	            if(r == true)
	            {
	                window.location.href = "nncms_content_medium_file.php?action=analysis&nns_id=" + str_id;
	            }
	            return true;
	        }
	        function refresh_vod_page() {
	            var num = $("#nns_list_max_num").val();
	            window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
	        }
	        //页面加载完成后执行
	        $(document).ready(function() {
	            $('#clear_time').click(function(){
	                $('#day_picker_start').val('');
	                $('#day_picker_end').val('');

	            });
	            $("#button_query").click(function(){
	            	 $("#query_action").val('');
	            	 $("#mix_query").submit();
            	});
	            $("#button_export").click(function(){
	            	 $("#query_action").val('export');
	            	 $("#mix_query").submit();
          		});
	            window.parent.now_frame_url = window.location.href;
	            window.parant.resetFrameHeight();
	        });
		</script>
</head>
<body>
	<div class="content">
		<!-- 栏目导航描述div -->
		<div class="content_position">内容提供商列表</div>
		<!-- 表单查询div -->
		<div class="content_table">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<form action="<?php echo $base_file_name;?>" method="get"
					id="mix_query">
					<input type="hidden" name="view_list_max_num"
						id="view_list_max_num"
						value="<?php echo $g_manager_list_max_num; ?>"
						style="width: 150px;"> <input type="hidden" id="query_action"
						name="action" value="" style="width: 150px;">
					<tbody>
						<tr>
							<td>分类名称： <input type="text" name="nns_file_name"
								id="nns_file_name" value="<?php echo $_GET['nns_file_name']; ?>"
								style="width: 200px;"> <label>服务器磁盘：</label><select
								name="nns_disk_id" style="width: 150px;">
									<option value="">全部</option>
                                            <?php foreach($arr_disk_list as $disk){?>
                                                <option
										value="<?php echo $disk['nns_id']; ?>"
										<?php if(isset($_GET['nns_disk_id']) && $_GET['nns_disk_id'] == $disk['nns_id']) echo 'selected="selected"';?>>
                                                    <?php echo $disk['nns_disk_name']; ?>
                                                </option>
                                            <?php }?>
                                        </select> 文件类型：<select
								name="nns_file_type" style="width: 150px;">
									<option value="">全部</option>
                                            <?php foreach($temp_server_group as $disk){?>
                                                <option
										value="<?php echo $disk; ?>"
										<?php if(isset($_GET['nns_file_type']) && $_GET['nns_file_type'] == $disk) echo 'selected="selected"';?>>
                                                    <?php echo $disk; ?>
                                                </option>
                                            <?php }?>
                                        </select> <label>所属CP：</label><select
								name="nns_cp_id" style="width: 150px;">
									<option value="">全部</option>
                                            <?php foreach($arr_cp_list as $cp){?>
                                                <option
										value="<?php echo $cp['nns_id']; ?>"
										<?php if(isset($_GET['nns_cp_id']) && $_GET['nns_cp_id'] == $cp['nns_id']) echo 'selected="selected"';?>>
                                                    <?php echo $cp['nns_cp_name']; ?>
                                                </option>
                                            <?php }?>
                                        </select> <label>解析状态：</label><select
								name="nns_is_analysis" style="width: 150px;">
									<option value="">全部</option>
									<option value="0"
										<?php if(isset($_GET['nns_is_analysis']) && strlen($_GET['nns_is_analysis']) >0 && $_GET['nns_is_analysis'] == '0'){echo 'selected="selected"';}?>>未解析</option>
									<option value="1"
										<?php if(isset($_GET['nns_is_analysis']) && strlen($_GET['nns_is_analysis']) >0 && $_GET['nns_is_analysis'] == '1'){echo 'selected="selected"';}?>>解析成功</option>
									<option value="2"
										<?php if(isset($_GET['nns_is_analysis']) && strlen($_GET['nns_is_analysis']) >0 && $_GET['nns_is_analysis'] == '2'){echo 'selected="selected"';}?>>解析失败</option>
							</select> <label>状态：</label><select name="nns_state"
								style="width: 150px;">
									<option value="">全部</option>
									<option value="0"
										<?php if(isset($_GET['nns_state']) && strlen($_GET['nns_state']) >0 && intval($_GET['nns_state']) === 0) echo 'selected="selected"';?>>开启</option>
									<option value="1"
										<?php if(isset($_GET['nns_state']) && strlen($_GET['nns_state']) >0 && intval($_GET['nns_state']) === 1) echo 'selected="selected"';?>>禁用</option>
							</select> <label>选择时间段：</label>
                                <input name="day_picker_start"
								id="day_picker_start" type="text"
								value="<?php
                                        if (isset($_GET['day_picker_start']))
                                            echo $_GET['day_picker_start'];
                                        ?>"
								style="width: 120px;" class="datetimepicker" callback="test" />

								- <input name="day_picker_end" id="day_picker_end" type="text"
								value="<?php
                                        if (isset($_GET['day_picker_end']))
                                            echo $_GET['day_picker_end'];
                                        ?>"
								style="width: 120px;" class="datetimepicker" callback="test" />

								<input type="button" id="clear_time" name="clear_time"
								value="清除时间" /> &nbsp;&nbsp;<input type="button"
								id="button_query" value="<?php echo cms_get_lang('search'); ?>" />
								&nbsp;&nbsp;<input type="button" id="button_export"
								value="导出EXCEL" />
							</td>
						</tr>
					</tbody>
				</form>
			</table>
		</div>
		<!-- 内容列表 DIV -->
		<div class="content_table formtable">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th><input name="" type="checkbox" value="">序号</th>
						<th>所属CP</th>
						<th>文件名称</th>
						<th>文件路径</th>
						<th>上传时间</th>
						<th>修改时间</th>
						<th>版权开始时间</th>
						<th>版权结束时间</th>
						<th>备注</th>
						<th>解析状态</th>
						<th>文件状态</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
						<?php
						$result_data_cp_last_key = !empty($result_data_cp_last) ? array_keys($result_data_cp_last) : array();
						if(is_array($arr_server_list) && !empty($arr_server_list))
						{
							$num = ($page_num - 1) * $page_size;
							foreach ($arr_server_list as $item) 
							{
								$num++;
						?>
						      <tr>
<!--                          cp名称        -->
						<td><input name="input" type="checkbox"
							value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
						<td><?php echo isset($temp_cp_list[$item['nns_cp_id']]) ? $temp_cp_list[$item['nns_cp_id']] : '<font color="red">未知CP</font>'; ?></td>


                        <td>
                                        <?php $arr_desc_info = pub_func_get_desc_alt('MediaInfo:',$item);?>
                            <alt
								alt="<?php echo $arr_desc_info; ?>"><?php echo $item['nns_file_name']; ?></alt>
						</td>


						<td><?php echo $item['nns_file_url']; ?></td>

						<td><?php echo $item['nns_create_time']; ?></td>

						<td><?php echo $item['nns_modify_time']; ?></td>

						<td><?php echo $item['nns_copyright_start_time']; ?></td>

						<td><?php echo $item['nns_copyright_end_time']; ?></td>

                        <td><?php echo $item['nns_summary']; ?></td>

                      <td>
                          <?php

                          switch ($item['nns_is_analysis']) {
                              case 0:
                                  echo '未解析';
                                  break;
                              case 1:
                                  echo '解析成功';
                                  break;
                              case 2:
                                  echo '解析失败';
                                  break;
                              default:
                                  echo '未知状态';
                                  break;
                          }; ?>
                      </td>

                        <td>
                            <a href="javascript:;" onclick="modify_state('<?php echo $item['nns_id']; ?>','<?php
                            if($item['nns_state'] == '1')
                            {
                                echo '0';
                            }
                            else
                            {
                                echo '1';
                            }
                            ?>')"><?php
                                if($item['nns_state'] == '1')
                                {
                                    echo '禁用';
                                }
                                else
                                {
                                    echo '开启';
                                }
                                ?></a>
                        </td>


						<td class="control_btns"
							name="<?php echo cms_get_lang('action'); ?>">
                            <a href="nncms_content_medium_file_edit_v2.php?nns_id=<?php echo $item['nns_id'];?>">修改</a>
                                        <?php if(isset($item['nns_is_analysis']) && intval($item['nns_is_analysis']) !== 1) {?>
                                            <a href="javascript:;"
							onclick="analysis('<?php echo $item['nns_id']; ?>')">解析</a>
                                        <?php }?>

                        </td>
					</tr>

						<?php
						     }
						}
						$least_num = $g_manager_list_max_num - count($arr_server_list);
						for ($i = 0; $i < $least_num; $i++) 
						{
						?>
						  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
						<?php 
						}
						?>
					</tbody>
			</table>
		</div>
		<!-- 分页DIV -->
		<div class="pagecontrol">
			共<span style="font-weight: bold; color: #ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
                <?php if ($currentpage > 1) { ?>
                        <a
				href="<?php $base_file_name; ?>?page=1<?php echo $nns_url . $search_url; ?>"
				target="_self"><?php echo cms_get_lang('first_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
			<a
				href="<?php $base_file_name; ?>?page=<?php echo $currentpage - 1; ?><?php echo $nns_url . $search_url; ?>"
				target="_self"><?php echo cms_get_lang('pre_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } else { ?>
                        <span><?php echo cms_get_lang('first_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
			<span><?php echo cms_get_lang('pre_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } if ($currentpage < $vod_pages) { ?>
                        <a
				href="<?php $base_file_name; ?>?page=<?php echo $currentpage + 1; ?><?php echo $nns_url . $search_url; ?>"
				target="_self"><?php echo cms_get_lang('next_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
			<a
				href="<?php $base_file_name; ?>?page=<?php echo $vod_pages; ?><?php echo $nns_url . $search_url; ?>"
				target="_self"><?php echo cms_get_lang('last_page'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } else { ?>
                        <span><?php echo cms_get_lang('next_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
			<span><?php echo cms_get_lang('last_page'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } ?>
    
                <?php echo cms_get_lang('jump_to'); ?> <input
				name="go_page_num" id="go_page_num"
				value="<?php echo $currentpage; ?>" type="text" style="width: 20px;" /> <?php echo cms_get_lang('page'); ?> &nbsp;&nbsp;
                <a
				href="javascript:go_page_num('<?php $base_file_name; ?>?ran=1<?php echo $nns_url . $search_url; ?>',<?php echo $vod_pages; ?>);">GO&gt;&gt;</a>&nbsp;&nbsp;
                <?php echo cms_get_lang('current'); ?><span
				style="font-weight: bold; color: #ff0000;"><?php echo $currentpage . "/" . $vod_pages; ?></span><?php echo cms_get_lang('page'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
                <?php echo cms_get_lang('perpagenum'); ?>&nbsp;
                <input name="nns_list_max_num" id="nns_list_max_num"
				type="text" value="<?php echo $g_manager_list_max_num; ?>"
				style="width: 24px;" />&nbsp;&nbsp; <input type="button"
				value="<?php echo cms_get_lang('confirm'); ?>"
				onclick="refresh_vod_page();" />&nbsp;&nbsp;
		</div>
		<div class="controlbtns">
			<div class="controlbtn allselect">
				<a href="javascript:selectAllCheckBox(true);">全选</a>
			</div>
			<div class="controlbtn cancel">
				<a href="javascript:selectAllCheckBox(false);">取消</a>
			</div>
			<div class="controlbtn add">
				<a href="nncms_content_medium_file_edit_control_next.php">上传影片</a>
			</div>
			<div class="controlbtn media">
				<a href="javascript:analysis()">解析</a>
			</div>
<!--			<div class="controlbtn delete">-->
<!--				<a href="javascript:del();">删除</a>-->
<!--			</div>-->
			<div style="clear: both;"></div>
		</div>
	</div>

	</div>
</body>
</html>
