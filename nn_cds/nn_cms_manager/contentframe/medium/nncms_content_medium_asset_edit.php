<?php
/**
 * Use：介质库分类管理-添加/修改
 * Author：kan.yang@starcor.cn
 * DateTime：18-3-26 上午9:58
 * Description：
 */
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
    $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
//$pri_bool = false;
//$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
//$checkpri = null;
//$nns_id = $_GET["nns_id"];
//$view_type = $_GET["view_type"];
//$group_name = $_GET["group_name"];
//if (!$pri_bool) {
//    Header("Location: ../nncms_content_wrong.php");
//    exit ;
//}
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';

include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$obj_dc = nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
//引入服务器处理类
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_data_model/medium/m_medium_asset.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_data_model/medium/m_medium_cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_data_model/medium/m_medium_disk.class.php';
$obj_medium_asset = new m_medium_asset();$obj_medium_cp = new m_medium_cp();$obj_medium_disk = new m_medium_disk();
//查询cp列表
$arr_cp_list = $obj_medium_cp->query_list(array(),null);
if($arr_cp_list['ret'] == 0 && !empty($arr_cp_list['data_info']))
{
    $arr_cp_list = $arr_cp_list['data_info'];
}
else
{
    $arr_cp_list = array();
}
//查询磁盘列表
$arr_disk_list = $obj_medium_disk->query_list(array(),null);
if($arr_disk_list['ret'] == 0 && !empty($arr_disk_list['data_info']))
{
    $arr_disk_list = $arr_disk_list['data_info'];
}
else
{
    $arr_disk_list = array();
}

$bool_edit = false; $str_action = 'add';
$str_server_list_url = 'window.location.href = "nncms_content_medium_asset.php";';
$str_server_edit_url = 'window.location.href = \'nncms_content_medium_asset_edit.php';
if(isset($_REQUEST['action']))
{
    if($_REQUEST['action'] == 'edit')
    {//修改
        $arr_modify_ret = $obj_medium_asset->edit(array('nns_id' => $_REQUEST['nns_id']),array(
            'nns_cp_id'        => isset($_REQUEST['nns_cp_id']) ? $_REQUEST['nns_cp_id'] : '',
            'nns_disk_id'      => isset($_REQUEST['nns_disk_id']) ? $_REQUEST['nns_disk_id'] : '',
            'nns_asset_name'   => isset($_REQUEST['nns_asset_name']) ? $_REQUEST['nns_asset_name'] : '',
            'nns_asset_path'   => isset($_REQUEST['nns_asset_path']) ? $_REQUEST['nns_asset_path'] : '',
            'nns_asset_type'   => isset($_REQUEST['nns_asset_type']) ? $_REQUEST['nns_asset_type'] : 0,
            'nns_asset_summary'=> isset($_REQUEST['nns_asset_summary']) ? $_REQUEST['nns_asset_summary'] : '',
        ));
        if($arr_modify_ret['ret'] == 0)
        {
            echo '<script>alert("更新介质库分类信息成功");' . $str_server_list_url . '</script>';die;
        }
        else
        {
            echo '<script>alert("更新介质库分类信息失败");' . $str_server_edit_url . '?nns_id=' . $_REQUEST['nns_id'] . '\';</script>';die;
        }
    }
    else
    {//添加

        $arr_modify_ret = $obj_medium_asset->add(array(
            'nns_cp_id'        => isset($_REQUEST['nns_cp_id']) ? $_REQUEST['nns_cp_id'] : '',
            'nns_disk_id'      => isset($_REQUEST['nns_disk_id']) ? $_REQUEST['nns_disk_id'] : '',
            'nns_asset_name'   => isset($_REQUEST['nns_asset_name']) ? $_REQUEST['nns_asset_name'] : '',
            'nns_asset_path'   => isset($_REQUEST['nns_asset_path']) ? $_REQUEST['nns_asset_path'] : '',
            'nns_asset_type'   => isset($_REQUEST['nns_asset_type']) ? $_REQUEST['nns_asset_type'] : 0,
            'nns_asset_summary'=> isset($_REQUEST['nns_asset_summary']) ? $_REQUEST['nns_asset_summary'] : '',
        ));
        if($arr_modify_ret['ret'] == 0)
        {
            echo '<script>alert("添加介质库分类信息成功");' . $str_server_list_url . '</script>';die;
        }
        else
        {
            echo '<script>alert("添加介质库分类信息失败");' . $str_server_edit_url . '\';</script>';die;
        }
    }
}
if(isset($_REQUEST['nns_id']) && strlen($_REQUEST['nns_id']) > 0)
{
    $arr_server_info = $obj_medium_asset->query_one(array('nns_id' => $_REQUEST['nns_id']));
    if($arr_server_info['ret'] == 0 && !empty($arr_server_info['data_info']))
    {
        $arr_server_info = $arr_server_info['data_info'];
    }
    else
    {
        echo '<script>alert("更新介质库分类信息失败：根据分类ID查询介质库分类信息失败");' . $str_server_list_url . '</script>';die;
    }
    $bool_edit = true; $str_action = 'edit';
}
$str_action_name = $str_action == 'add' ? "添加" : '修改';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
    <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
    <link href="../../css/comm.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../js/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="../../js/cms_cookie.js"></script>
    <script type="text/javascript" src="../../js/table.js.php"></script>
    <script type="text/javascript" src="../../js/checkinput.js.php"></script>
    <script type="text/javascript" src="../../js/rate.js"></script>
    <script type="text/javascript" src="../../js/image_loaded_func.js"></script>
    <script type="text/javascript" src="../../js/alertbox.js"></script>
</head>

<body>
<div class="selectbox" style="display: none;">
    <iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
</div>
<div class="content">
    <div class="content_position">介质库管理 > 介质库分类编辑</div>
    <form id="add_form" action="nncms_content_medium_asset_edit.php?action=<?php echo $str_action; ?>" method="post" enctype="multipart/form-data">
        <input name="action" id="action" type="hidden" value="<?php echo $str_action;?>" />
        <?php if ($bool_edit){?>
            <input name="nns_id" id="nns_id" type="hidden" value="<?php echo $_REQUEST['nns_id'];?>"/>
        <?php } ?>
        <div class="content_table">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>

                <tr>
                    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>分类名称</td>
                    <td>
                        <input name="nns_asset_name" id="nns_asset_name" type="text" value="<?php echo isset($arr_server_info['nns_asset_name']) ? $arr_server_info['nns_asset_name'] : ''; ?>" rule="noempty"/>
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>分类路径名称</td>
                    <td>
                        <input name="nns_asset_path" id="nns_asset_path" type="text" value="<?php echo isset($arr_server_info['nns_asset_path']) ? $arr_server_info['nns_asset_path'] : ''; ?>" rule="noempty"/>
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">所属CP</td>
                    <td>
                        <select name="nns_cp_id" style="width: 857px;">
                            <option value="">请选择</option>
                            <?php foreach($arr_cp_list as $cp){?>
                                <option value="<?php echo $cp['nns_id']; ?>" <?php if(isset($arr_server_info['nns_cp_id']) && $arr_server_info['nns_cp_id'] == $cp['nns_id']) echo 'selected="selected"'?>>
                                    <?php echo $cp['nns_cp_name']; ?>
                                </option>
                            <?php }?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">服务器磁盘</td>
                    <td>
                        <select name="nns_disk_id" style="width: 857px;">
                            <option value="">请选择</option>
                            <?php foreach($arr_disk_list as $disk){?>
                                <option value="<?php echo $disk['nns_id']; ?>" <?php if(isset($arr_server_info['nns_disk_id']) && $arr_server_info['nns_disk_id'] == $disk['nns_id']) echo 'selected="selected"'?>>
                                    <?php echo $disk['nns_disk_name']; ?>
                                </option>
                            <?php }?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">分类类型</td>
                    <td>
                        <select name="nns_asset_type" style="width: 857px;">
                            <option value="" >请选择</option>
                            <option value="0" <?php if(isset($arr_server_info['nns_state']) && intval($arr_server_info['nns_state']) === 0) echo 'selected="selected"'?>>单集</option>
                            <option value="1" <?php if(isset($arr_server_info['nns_state']) && intval($arr_server_info['nns_state']) === 1) echo 'selected="selected"'?>>多集</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">分类描述</td>
                    <td>
                        <textarea name="nns_asset_summary" id="nns_asset_summary"><?php echo isset($arr_server_info['nns_asset_summary']) ? $arr_server_info['nns_asset_summary'] : ''; ?></textarea>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
    <div class="controlbtns">
        <div class="controlbtn <?php echo $str_action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','<?php echo cms_get_lang('webkz_qrccxg'); ?>',$('#add_form'),'<?php echo $str_action; ?>');"><?php echo $str_action_name; ?></a></div>
		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
    </div>
</div>

</body>
</html>
