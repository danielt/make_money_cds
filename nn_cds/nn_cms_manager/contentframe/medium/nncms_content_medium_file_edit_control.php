<?php
header("Content-Type:text/html;charset=utf-8");
ob_start();
set_time_limit(0);
include ("../../nncms_manager_inc.php");
include $nncms_config_path . 'nn_cms_manager/controls/nncms_controls_public_function.php';
require_once $nncms_db_path . 'nns_log' . DIRECTORY_SEPARATOR . 'nns_db_op_log_class.php';
//引入文件
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_core/m_upload.class.php';
$log_inst = new nns_db_op_log_class();

$dc = nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_NULL
));
$dc->open();
$params = $_REQUEST;

$log_params = (!empty($params) && is_array($params)) ? json_encode($params) : '';
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -8);

$arr_params = array(
    'nns_path',
    'nns_cp',
    'nns_date',
    'nns_type',
    'nns_categray',
);
function make_encode($array)
{
    if(!is_array($array) || empty($array))
    {
        return array();
    }
    $last_data = array();
    foreach ($array as $key=>$value)
    {
        if($key == 'filePath' || $key=='name')
        {
            $last_data[$key] = base64_encode($value);
        }
        else
        {
            $last_data[$key] = $value;
        }
    }
    return $last_data;
}
$dir_file_path_1 = $dir_file_path = '';

if(is_array($arr_params) && !empty($arr_params))
{
    foreach ($arr_params as $value)
    {
        $value = urldecode($value);
        if(!isset($_REQUEST[$value]) || strlen($_REQUEST[$value]) <1)
        {
            echo "<script>alert('参数不合法{$value}');history.go(-1);</script>";
        }
        $dir_file_path_1.=$_REQUEST[$value].'/';
        if (strtolower(substr(php_uname(), 0, 7)) == "windows")//windows下执行
        {
            $_REQUEST[$value] = iconv("UTF-8", 'GBK',$_REQUEST[$value] );
        }
        $dir_file_path.=$_REQUEST[$value].'/';
        unset($_REQUEST[$value]);
    }
}
$dir_file_path_1.='/'.$params['name'];
if (strtolower(substr(php_uname(), 0, 7)) == "windows")//windows下执行
{
    $params['name'] = iconv("UTF-8", 'GBK',$params['name'] );
}
$dir_file_path.='/'.$params['name'];
$dir_file_path = trim(trim(str_replace('//', '/', str_replace('\\', '/', $dir_file_path)),'/'));
$dir_file_path_1 = trim(trim(str_replace('//', '/', str_replace('\\', '/', $dir_file_path_1)),'/'));
if (strtolower(substr(php_uname(), 0, 7)) != "windows")//windows下执行
{
    $dir_file_path = '/'.$dir_file_path;
    $dir_file_path_1 = '/'.$dir_file_path_1;
}
$result = m_upload::init($params,$dir_file_path);
if($result['ret'] !=0)
{
    echo json_encode(array('success'=>false));die;
}
$data_info = (isset($result['data_info']) && is_array($result['data_info']) && !empty($result['data_info'])) ? $result['data_info'] : array('success'=>false);
$data_info['filePath'] = $dir_file_path_1;
echo json_encode(make_encode($data_info));die;