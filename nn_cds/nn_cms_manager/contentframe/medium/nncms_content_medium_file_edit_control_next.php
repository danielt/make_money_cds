<?php
/**
 * Use：介质库分类管理-添加/修改
 * Author：kan.yang@starcor.cn
 * DateTime：18-3-26 上午9:58
 * Description：
 */
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
    $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();

require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';

include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$obj_dc = nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
//引入服务器处理类
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_data_model/medium/m_medium_cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_data_model/medium/m_medium_disk.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/medium/medium_library_type.class.php';
$obj_medium_cp = new m_medium_cp();$obj_medium_disk = new m_medium_disk();
//查询cp列表
$arr_cp_list = $obj_medium_cp->query_list(array(),null);
if($arr_cp_list['ret'] == 0 && !empty($arr_cp_list['data_info']))
{
    $arr_cp_list = $arr_cp_list['data_info'];
}
else
{
    $arr_cp_list = array();
}
//查询磁盘列表
$arr_disk_list = $obj_medium_disk->query_list(array(),null);
if($arr_disk_list['ret'] == 0 && !empty($arr_disk_list['data_info']))
{
    $arr_disk_list = $arr_disk_list['data_info'];
}
else
{
    $arr_disk_list = array();
}
$arr_categray = nl_medium_library_type::query_all($obj_dc,0);
if($arr_categray['ret'] !=0 || !isset($arr_categray['data_info']) || empty($arr_categray['data_info']) || !is_array($arr_categray['data_info']))
{
    echo "<script>alert('查询影片类型信息不存在，请先添加');history.go(-1);</script>";
}
$arr_categray = $arr_categray['data_info'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
    <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
    <link href="../../css/comm.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../js/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="../../js/cms_cookie.js"></script>
    <script type="text/javascript" src="../../js/table.js.php"></script>
    <script type="text/javascript" src="../../js/checkinput.js.php"></script>
    <script type="text/javascript" src="../../js/rate.js"></script>
    <script type="text/javascript" src="../../js/image_loaded_func.js"></script>
    <script type="text/javascript" src="../../js/alertbox.js"></script>
    <script type="text/javascript">
        function modify()
        {
            $('#add_form').submit();
        }
    </script>
</head>

<body>
<div class="selectbox" style="display: none;">
    <iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
</div>
<div class="content">
    <div class="content_position">介质库管理 > 介质库片源上传</div>
    <form id="add_form" action="nncms_content_medium_file_edit.php" method="post" enctype="multipart/form-data">
        <input name="action" id="action" type="hidden" value="next" />
        <div class="content_table">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td class="rightstyle">所属CP</td>
                    <td>
                        <select name="nns_cp_id" style="width: 800px;">
                            <option value="">请选择</option>
                            <?php foreach($arr_cp_list as $cp){?>
                                <option value="<?php echo $cp['nns_id']; ?>">
                                    <?php echo $cp['nns_cp_name']; ?>
                                </option>
                            <?php }?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">服务器磁盘</td>
                    <td>
                        <select name="nns_disk_id" style="width: 800px;">
                            <option value="">请选择</option>
                            <?php foreach($arr_disk_list as $disk){?>
                                <option value="<?php echo $disk['nns_id']; ?>">
                                    <?php echo $disk['nns_disk_name']; ?>
                                </option>
                            <?php }?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">分类类型</td>
                    <td>
                        <input type="radio" name="radio" value='1' checked/> 单集
                        <input type="radio" name="radio" value='2' /> 多集
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">栏目分类</td>
                    <td>
                        <select name="nns_categray" style="width: 800px;">
                            <option value="" >请选择</option>
                            <?php foreach($arr_categray as $categray){?>
                                <option value="<?php echo $categray['nns_id']; ?>">
                                    <?php echo $categray['nns_name']; ?>
                                </option>
                            <?php }?>
                        </select>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
    <div class="controlbtns">
        <div class="controlbtn add"><a href="javascript:modify()">下一步</a></div>
        <div class="controlbtn back"><a href="javascript:returnToHistory();">返回</a></div>
        <div style="clear:both;"></div>
    </div>
</div>

</body>
</html>