<?php
/**
 * Use：介质库CP管理-添加/修改
 * Author：kan.yang@starcor.cn
 * DateTime：18-3-26 上午9:58
 * Description：
 */
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
    $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
//$pri_bool = false;
//$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
//$checkpri = null;
//$nns_id = $_GET["nns_id"];
//$view_type = $_GET["view_type"];
//$group_name = $_GET["group_name"];
//if (!$pri_bool) {
//    Header("Location: ../nncms_content_wrong.php");
//    exit ;
//}
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';

include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$obj_dc = nl_get_dc(array (
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));

//引入服务器处理类
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/v2/ns_data_model/medium/m_medium_cp.class.php';
$obj_medium_cp = new m_medium_cp();
$bool_edit = false; $str_action = 'add';
$str_server_list_url = 'window.location.href = "nncms_content_medium_cp.php";';
$str_server_edit_url = 'window.location.href = \'nncms_content_medium_cp_edit.php';
if(isset($_REQUEST['action']))
{
    if($_REQUEST['action'] == 'edit')
    {//修改
        $nns_cp_name = isset($_REQUEST['nns_cp_name']) ? $_REQUEST['nns_cp_name'] : '';
        $_REQUEST['nns_id'] = md5($nns_cp_name);
        $arr_modify_ret = $obj_medium_cp->edit(array('nns_id' => $_REQUEST['nns_id']),array(
            'nns_cp_name'        => isset($_REQUEST['nns_cp_name']) ? $_REQUEST['nns_cp_name'] : '',
            'nns_contactor'      => isset($_REQUEST['nns_contactor']) ? $_REQUEST['nns_contactor'] : '',
            'nns_telephone'      => isset($_REQUEST['nns_telephone']) ? $_REQUEST['nns_telephone'] : '',
            'nns_email'          => isset($_REQUEST['nns_email']) ? $_REQUEST['nns_email'] : '',
            'nns_addr'           => isset($_REQUEST['nns_addr']) ? $_REQUEST['nns_addr'] : '',
            'nns_cp_summary'     => isset($_REQUEST['nns_cp_summary']) ? $_REQUEST['nns_cp_summary'] : '',
            'nns_image_h'        => isset($_REQUEST['nns_image_h']) ? $_REQUEST['nns_image_h'] : '',
            'nns_image_v'        => isset($_REQUEST['nns_image_v']) ? $_REQUEST['nns_image_v'] : '',
            'nns_image_s'        => isset($_REQUEST['nns_image_s']) ? $_REQUEST['nns_image_s'] : '',
        ));
        if($arr_modify_ret['ret'] == 0)
        {
            echo '<script>alert("更新介质库CP信息成功");' . $str_server_list_url . '</script>';die;
        }
        else
        {
            echo '<script>alert("更新介质库CP信息失败");' . $str_server_edit_url . '?nns_id=' . $_REQUEST['nns_id'] . '\';</script>';die;
        }
    }
    else
    {//添加
        $nns_cp_name = isset($_REQUEST['nns_cp_name']) ? $_REQUEST['nns_cp_name'] : '';
        $_REQUEST['nns_id'] = md5($nns_cp_name);
        $arr_modify_ret = $obj_medium_cp->add(array(
            'nns_id'             => $_REQUEST['nns_id'],
            'nns_cp_name'        => isset($_REQUEST['nns_cp_name']) ? $_REQUEST['nns_cp_name'] : '',
            'nns_contactor'      => isset($_REQUEST['nns_contactor']) ? $_REQUEST['nns_contactor'] : '',
            'nns_telephone'      => isset($_REQUEST['nns_telephone']) ? $_REQUEST['nns_telephone'] : '',
            'nns_email'          => isset($_REQUEST['nns_email']) ? $_REQUEST['nns_email'] : '',
            'nns_addr'           => isset($_REQUEST['nns_addr']) ? $_REQUEST['nns_addr'] : '',
            'nns_cp_summary'     => isset($_REQUEST['nns_cp_summary']) ? $_REQUEST['nns_cp_summary'] : '',
            'nns_image_h'        => isset($_REQUEST['nns_image_h']) ? $_REQUEST['nns_image_h'] : '',
            'nns_image_v'        => isset($_REQUEST['nns_image_v']) ? $_REQUEST['nns_image_v'] : '',
            'nns_image_s'        => isset($_REQUEST['nns_image_s']) ? $_REQUEST['nns_image_s'] : '',
        ));
        if($arr_modify_ret['ret'] == 0)
        {
            echo '<script>alert("添加介质库CP信息成功");' . $str_server_list_url . '</script>';die;
        }
        else
        {
            echo '<script>alert("添加介质库CP信息失败");' . $str_server_edit_url . '\';</script>';die;
        }
    }
}
if(isset($_REQUEST['nns_id']) && strlen($_REQUEST['nns_id']) > 0)
{
    $arr_server_info = $obj_medium_cp->query_one(array('nns_id' => $_REQUEST['nns_id']));
    if($arr_server_info['ret'] == 0 && !empty($arr_server_info['data_info']))
    {
        $arr_server_info = $arr_server_info['data_info'];
    }
    else
    {
        echo '<script>alert("更新介质库CP信息失败：根据CP ID查询介质库CP信息失败");' . $str_server_list_url . '</script>';die;
    }
    $bool_edit = true; $str_action = 'edit';
}
$str_action_name = $str_action == 'add' ? "添加" : '修改';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
	<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
	<link href="../../css/rate.css" rel="stylesheet" type="text/css" />
	<link href="../../css/comm.css" rel="stylesheet" type="text/css" />
	<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
	<script language="javascript" src="../../js/cms_cookie.js"></script>
	<script language="javascript" src="../../js/table.js.php"></script>
	<script language="javascript" src="../../js/checkinput.js.php"></script>
	<script language="javascript" src="../../js/rate.js"></script>
	<script language="javascript" src="../../js/image_loaded_func.js"></script>
	<script language="javascript" src="../../js/alertbox.js"></script>
</head>

<body>
<div class="selectbox" style="display: none;">
    <iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
</div>
<div class="content">
    <div class="content_position">介质库管理 > 介质库CP编辑</div>
    <form id="add_form" action="nncms_content_medium_cp_edit.php?action=<?php echo $str_action; ?>" method="post" enctype="multipart/form-data">
        <input name="action" id="action" type="hidden" value="<?php echo $str_action;?>" />
        <?php if ($bool_edit){?>
            <input name="nns_id" id="nns_id" type="hidden" value="<?php echo $_REQUEST['nns_id'];?>"/>
        <?php } ?>
        <div class="content_table">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td class="rightstyle"><span style="color:#FF0000; font-size:16px; font-weight:bold;">*</span>CP名称</td>
                    <td>
                        <input name="nns_cp_name" id="nns_cp_name" type="text" value="<?php echo isset($arr_server_info['nns_cp_name']) ? $arr_server_info['nns_cp_name'] : ''; ?>" rule="noempty"/>
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">联系人</td>
                    <td>
                        <input name="nns_contactor" id="nns_contactor" type="text" value="<?php echo isset($arr_server_info['nns_contactor']) ? $arr_server_info['nns_contactor'] : ''; ?>"/>
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">电话</td>
                    <td>
                        <input name="nns_telephone" id="nns_telephone" type="text" value="<?php echo isset($arr_server_info['nns_telephone']) ? $arr_server_info['nns_telephone'] : ''; ?>"/>
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">Email</td>
                    <td>
                        <input name="nns_email" id="nns_email" type="text" value="<?php echo isset($arr_server_info['nns_email']) ? $arr_server_info['nns_email'] : ''; ?>"/>
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">地址</td>
                    <td>
                        <input name="nns_addr" id="nns_addr" type="text" value="<?php echo isset($arr_server_info['nns_addr']) ? $arr_server_info['nns_addr'] : ''; ?>"/>
                    </td>
                </tr>
                <?php if(false) {?>
                <tr>
                    <td class="rightstyle">横海报</td>
                    <td>
                        <input name="nns_image_h" id="nns_image_h" type="text" value="<?php echo isset($arr_server_info['nns_image_h']) ? $arr_server_info['nns_image_h'] : ''; ?>"/>
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">竖海报</td>
                    <td>
                        <input name="nns_image_v" id="nns_image_v" type="text" value="<?php echo isset($arr_server_info['nns_image_v']) ? $arr_server_info['nns_image_v'] : ''; ?>"/>
                    </td>
                </tr>
                <tr>
                    <td class="rightstyle">方海报</td>
                    <td>
                        <input name="nns_image_s" id="nns_image_s" type="text" value="<?php echo isset($arr_server_info['nns_image_s']) ? $arr_server_info['nns_image_s'] : ''; ?>"/>
                    </td>
                </tr>
                <?php }?>
                <tr>
                    <td class="rightstyle">CP描述</td>
                    <td>
                        <textarea name="nns_cp_summary" id="nns_cp_summary"><?php echo isset($arr_server_info['nns_cp_summary']) ? $arr_server_info['nns_cp_summary'] : ''; ?></textarea>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
    <div class="controlbtns">
        <div class="controlbtn <?php echo $str_action;?>"><a href="javascript:checkForm('<?php echo cms_get_lang('xtgl_jtgl');?>','<?php echo cms_get_lang('webkz_qrccxg'); ?>',$('#add_form'),'<?php echo $str_action; ?>');"><?php echo $str_action_name; ?></a></div>
		<div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
		<div style="clear:both;"></div>
    </div>
</div>

</body>
</html>
