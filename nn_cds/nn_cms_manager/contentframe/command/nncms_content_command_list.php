<?php
/**
 * 指令池
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/8/7 15:12
 */
header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"]))
{
    $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
include_once $nncms_config_path . "nn_logic/pass_queue/pass_queue.class.php";
include_once $nncms_config_path . "nn_logic/cp/cp.class.php";
include_once $nncms_config_path . 'mgtv_v2/callback_notify/import_queue.class.php';

$dc = nl_get_dc(array(
    'db_policy' => NL_DB_READ | NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));

$hash_dc = nl_get_dc(
    array('db_policy' => NL_DB_WRITE | NL_DB_READ,'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE),
    array('db_type' => NP_DB_TYPE_HASH_TABLE_MYSQL)
);

$dc->open();
$sp_id = $_GET['sp_id'];

$message_id = $_GET['nns_message_id'] ;
$filter = null;

if(isset($_GET['action_op']))
{
    switch ($_GET['action_op'])
    {
        case 'search':
            if (!empty($_GET['nns_message_id']) && strlen($_GET['nns_message_id']) > 0)
            {
                $filter['nns_message_id'] = $_GET['nns_message_id'];
            }
            if (!empty($_GET['nns_original_id']) && strlen($_GET['nns_original_id']) > 0)
            {
                $filter['nns_original_id'] = $_GET['nns_original_id'];
            }
            if (!empty($_GET['nns_name']) && strlen($_GET['nns_name']) > 0)
            {
                $filter['nns_name'] = $_GET['nns_name'];
            }
            if (!empty($_GET['nns_type']) && strlen($_GET['nns_type']) > 0)
            {
                $filter['nns_type'] = $_GET['nns_type'];
            }
            break;
    }
}
$result = array();
if (!empty($message_id) && strlen($message_id) > 0)
{
    $filter['nns_message_id'] = $message_id;
    /**********先进性查询指令队列**********/
    $re_command = nl_command::query_by_condition($hash_dc, $filter);
    $command_id = array();
    $command_data = array();//command 数据
    if ($re_command['ret'] == 0 && is_array($re_command['data_info']) && count($re_command['data_info']) >0)
    {
        foreach ($re_command['data_info'] as $item)
        {
            $command_id[] = $item['nns_id'];
            $command_data[$item['nns_id']] =  $item;
        }
        $command_task_f['nns_command_id'] = $command_id;
        if (!empty($_GET['nns_sp_id']) && strlen($_GET['nns_sp_id']) > 0)
        {
            $command_task_f['nns_sp_id'] = $_GET['nns_sp_id'];
        }
        $result = nl_command_task::get_list($hash_dc, $command_task_f, 'nns_create_time desc');
        $command_task_id = array();
        if ($result['ret'] == 0 && is_array($result['data_info']) && count($result['data_info']) > 0)
        {
            if (strlen($_GET['nns_status']) > 0)
            {
                $command_task_log_f['nns_status'] = $_GET['nns_status'];
            }
            foreach ($result['data_info'] as &$items)
            {
                //默认父级nns_command信息
                $items['nns_message_id'] = $command_data[$items['nns_command_id']]['nns_message_id'];
                $items['nns_name'] = $command_data[$items['nns_command_id']]['nns_name'];
                $items['nns_original_id'] = $command_data[$items['nns_command_id']]['nns_original_id'];
                $items['nns_type'] = $command_data[$items['nns_command_id']]['nns_type'];
                $items['nns_source_id'] = $command_data[$items['nns_command_id']]['nns_source_id'];

                $command_task_log_f['nns_command_task_id'] = $items['nns_id'];
                $re_command_task_log = nl_command_task_log::get_list($hash_dc, $command_task_log_f, 'nns_create_time desc');
                if ($re_command_task_log['ret'] == 0 && is_array($re_command_task_log['data_info']) && count($re_command_task_log['data_info']) > 0)
                {
                    $items['nns_status'] = $re_command_task_log['data_info'][0]['nns_status'];
                    $items['nns_desc'] = $re_command_task_log['data_info'][0]['nns_desc'];
                    $items[$command_task_log_f['nns_command_task_id']] = $re_command_task_log['data_info'];
                }
                else
                {
                    $items['nns_status'] = 1;
                }
            }
        }

    }
    unset($command_id);
    unset($command_data);
}

//获取sp
$sp_re = nl_sp::query_all($dc);
if($sp_re['ret'] == 0 && is_array($sp_re['data_info']))
{
    $result_sp_data_keys = $result_sp_id_data = array();
    foreach ($sp_re['data_info'] as $sp_val)
    {
        $result_sp_id_data[$sp_val['nns_id']] = $sp_val['nns_name'];
    }
    $result_sp_data_keys = array_keys($result_sp_id_data);
    $sp_arr = $sp_re['data_info'];
}

//获取CP
$cp_re = nl_cp::query_all($dc);
if($cp_re['ret'] == 0 && is_array($cp_re['data_info']))
{
    $result_cp_data_keys = $result_cp_id_data = array();
    foreach ($cp_re['data_info'] as $cp_val)
    {
        $result_cp_id_data[$cp_val['nns_id']] = $cp_val['nns_name'];
    }
    $result_cp_data_keys = array_keys($result_cp_id_data);
    $cp_arr = $cp_re['data_info'];
}

$vod_total_num = count($result['data_info']);
$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);

$data = $result['data_info'];
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
    <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
    <script language="javascript" src="../../js/cms_cookie.js"></script>
    <script language="javascript" src="../../js/table.js.php"></script>
    <script language="javascript" src="../../js/checkinput.js.php"></script>
    <script language="javascript" src="../../js/rate.js"></script>
    <script language="javascript" src="../../js/image_loaded_func.js"></script>
    <script language="javascript" src="../../js/cms_datepicker.js"></script>
    <script language="javascript" src="../../js/alertbox.js"></script>
</head>
<body>
<div class="content">
    <div class="content_position">
        流程日志
    </div>
    <div class="content_table">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <form action="" method="get">
                <tbody>
                <tr>
                    <td>
                        消息ID：&nbsp;&nbsp;<h1 style="font-size:150%"><?php echo $_GET['nns_message_id']; ?></h1>
                        <input type="hidden" name="nns_message_id" value="<?php echo $_GET['nns_message_id']; ?>" /><br>
                        操作类型：&nbsp;&nbsp;<select name="nns_type" style="width: 80px;">
                            <option value="" >全部</option>
                            <option value="video" <?php if($_GET['nns_type']=='video') echo 'selected="selected"'?>>主媒资</option>
                            <option value="index" <?php if($_GET['nns_type']=='index') echo 'selected="selected"'?>>分集</option>
                            <option value="media" <?php if($_GET['nns_type']=='media') echo 'selected="selected"'?>>片源</option>
                        </select>
                        <!--状态：&nbsp;&nbsp;<select name="nns_status" style="width: 80px;">
                            <option value="" >全部</option>
                            <option value="0" <?php if($_GET['nns_status']=='0') echo 'selected="selected"'?>>已达最终状态</option>
                            <option value="1" <?php if($_GET['nns_status']=='1') echo 'selected="selected"'?>>中心同步指令等待注入</option>
                            <option value="2" <?php if($_GET['nns_status']=='2') echo 'selected="selected"'?>>中心同步指令等待切片</option>
                            <option value="3" <?php if($_GET['nns_status']=='3') echo 'selected="selected"'?>>中心同步指令正在切片</option>
                            <option value="4" <?php if($_GET['nns_status']=='4') echo 'selected="selected"'?>>中心同步指令取消注入</option>
                            <option value="5" <?php if($_GET['nns_status']=='5') echo 'selected="selected"'?>>中心同步指令等待前置sp</option>
                            <option value="6" <?php if($_GET['nns_status']=='6') echo 'selected="selected"'?>>中心同步指令未审核</option>
                            <option value="7" <?php if($_GET['nns_status']=='7') echo 'selected="selected"'?>>中心同步指令正在转码</option>
                            <option value="8" <?php if($_GET['nns_status']=='8') echo 'selected="selected"'?>>中心同步指令等在转码</option>
                            <option value="9" <?php if($_GET['nns_status']=='9') echo 'selected="selected"'?>>转码失败</option>
                            <option value="10" <?php if($_GET['nns_status']=='10') echo 'selected="selected"'?>>中心同步指令注入失败</option>
                            <option value="11" <?php if($_GET['nns_status']=='11') echo 'selected="selected"'?>>C2队列等待注入</option>
                            <option value="12" <?php if($_GET['nns_status']=='12') echo 'selected="selected"'?>>C2队列正在注入CDN</option>
                            <option value="13" <?php if($_GET['nns_status']=='13') echo 'selected="selected"'?>>C2队列注入CDN成功</option>
                            <option value="14" <?php if($_GET['nns_status']=='14') echo 'selected="selected"'?>>C2队列注入CDN失败</option>
                            <option value="15" <?php if($_GET['nns_status']=='15') echo 'selected="selected"'?>>C2队列注入CDN取消</option>
                            <option value="16" <?php if($_GET['nns_status']=='16') echo 'selected="selected"'?>>C2队列正在注入EPG</option>
                            <option value="17" <?php if($_GET['nns_status']=='17') echo 'selected="selected"'?>>C2队列注入EPG成功</option>
                            <option value="18" <?php if($_GET['nns_status']=='18') echo 'selected="selected"'?>>C2队列注入EPG失败</option>
                            <option value="19" <?php if($_GET['nns_status']=='19') echo 'selected="selected"'?>>C2队列注入EPG取消</option>
                            <option value="20" <?php if($_GET['nns_status']=='20') echo 'selected="selected"'?>>C2队列取消</option>
                            <option value="21" <?php if($_GET['nns_status']=='21') echo 'selected="selected"'?>>C2队列等待注入CDN</option>
                            <option value="22" <?php if($_GET['nns_status']=='22') echo 'selected="selected"'?>>C2队列等待注入EPG</option>
                            <option value="23" <?php if($_GET['nns_status']=='23') echo 'selected="selected"'?>>中心指令发送到切片队列失败</option>
                            <option value="24" <?php if($_GET['nns_status']=='24') echo 'selected="selected"'?>>切片失败</option>
                            <option value="25" <?php if($_GET['nns_status']=='25') echo 'selected="selected"'?>>切片成功</option>
                            <option value="26" <?php if($_GET['nns_status']=='26') echo 'selected="selected"'?>>转码成功</option>
                            <option value="27" <?php if($_GET['nns_status']=='27') echo 'selected="selected"'?>>正在转码</option>
                        </select>-->
                        SP：&nbsp;&nbsp;<select name="nns_sp_id" style="width: 120px;">
                            <option value="" >全部</option>
                            <?php foreach ($sp_arr as $sp_list){?>
                                <option value="<?php echo $sp_list['nns_id'];?>" <?php if($_GET['nns_sp_id']==$sp_list['nns_id']) echo 'selected="selected"'?>><?php echo $sp_list['nns_name'];?></option>
                            <?php }?>
                        </select>
                        <input type="hidden" name="action_op" value="search" />
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="submit" value="<?php echo cms_get_lang('search'); ?>"  />
                    </td>
                </tr>
                </tbody>
            </form>
        </table>
    </div>
    <div class="content_table formtable">

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <th><input name="" type="checkbox" value="">序号</th>
                <th>消息ID</th>
                <th>指令队列ID</th>
                <th>名称</th>
                <th>类型</th>
                <th>动作</th>
                <th>队列状态</th>
                <th>SPID</th>
                <th>描述</th>
                <th>创建时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if($data != null)
            {
                $num = 0;
                foreach ($data as $item)
                {
                    $num++;
                    ?>
                    <tr>
                        <td width="50px"><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>" /><?php echo $num; ?></td>
                        <td><?php echo $item['nns_message_id'];?></td>
                        <td><?php echo $item['nns_id'];?></td>
                        <td><?php echo $item['nns_name'];?></td>
                        <td>
                            <?php
                            switch ($item['nns_type'])
                            {
                                case 'video':
                                    echo '主媒资';
                                    break;
                                case 'index':
                                    echo '分集';
                                    break;
                                case 'media':
                                    echo '片源';
                                    break;
                                default:
                                    echo '<font color="red">未知</font>';
                                    break;
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            switch ($item['nns_action'])
                            {
                                case 'add':
                                    echo '添加';
                                    break;
                                case 'modify':
                                    echo '修改';
                                    break;
                                case 'destroy':
                                    echo '删除';
                                    break;
                                default:
                                    echo '<font color="red">未知</font>';
                                    break;
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            switch ($item['nns_status'])
                            {
                                case '0':
                                    echo '<font color="blue">已达最终状态</font>';
                                    break;
                                case '1':
                                    echo '<font color="green">中心同步指令等待注入</font>';
                                    break;
                                case '2':
                                    echo '<font color="green">中心同步指令等待切片</font>';
                                    break;
                                case '3':
                                    echo '<font color="green">中心同步指令正在切片</font>';
                                    break;
                                case '4':
                                    echo '<font color="gray">中心同步指令取消注入</font>';
                                    break;
                                case '5':
                                    echo '<font color="green">中心同步指令等待前置sp</font>';
                                    break;
                                case '6':
                                    echo '<font color="green">中心同步指令未审核</font>';
                                    break;
                                case '7':
                                    echo '<font color="green">中心同步指令正在转码</font>';
                                    break;
                                case '8':
                                    echo '<font color="green">中心同步指令等在转码</font>';
                                    break;
                                case '9':
                                    echo '<font color="red">转码失败</font>';
                                    break;
                                case '10':
                                    echo '<font color="green">中心同步指令注入失败</font>';
                                    break;
                                case '11':
                                    echo '<font color="green">C2队列等待注入</font>';
                                    break;
                                case '12':
                                    echo '<font color="green">C2队列正在注入CDN</font>';
                                    break;
                                case '13':
                                    echo '<font color="green">C2队列注入CDN成功</font>';
                                    break;
                                case '14':
                                    echo '<font color="red">C2队列注入CDN失败</font>';
                                    break;
                                case '15':
                                    echo '<font color="gray">C2队列注入CDN取消</font>';
                                    break;
                                case '16':
                                    echo '<font color="green">C2队列正在注入EPG</font>';
                                    break;
                                case '17':
                                    echo '<font color="green">C2队列注入EPG成功</font>';
                                    break;
                                case '18':
                                    echo '<font color="red">C2队列注入EPG失败</font>';
                                    break;
                                case '19':
                                    echo '<font color="gray">C2队列注入EPG取消</font>';
                                    break;
                                case '20':
                                    echo '<font color="green">C2队列取消</font>';
                                    break;
                                case '21':
                                    echo '<font color="green">C2队列等待注入CDN</font>';
                                    break;
                                case '22':
                                    echo '<font color="green">C2队列等待注入EPG</font>';
                                    break;
                                case '23':
                                    echo '<font color="green">中心指令发送到切片队列失败</font>';
                                    break;
                                case '24':
                                    echo '<font color="red">切片失败</font>';
                                    break;
                                case '25':
                                    echo '<font color="green">切片成功</font>';
                                    break;
                                case '26':
                                    echo '<font color="green">转码成功</font>';
                                    break;
                                case '27':
                                    echo '<font color="green">正在转码</font>';
                                    break;
                            }
                            ?>
                        </td>
                        <td>
                            <?php if(in_array($item['nns_sp_id'], $result_sp_data_keys))
                            {
                                echo isset($result_sp_id_data[$item['nns_sp_id']]) ? $result_sp_id_data[$item['nns_sp_id']] : '';
                            }
                            else
                            {
                                echo '<font color="#FF0000">非法</font>';
                            }?>
                        </td>
                        <td><?php echo $item['nns_desc']; ?></td>
                        <td><?php echo $item['nns_create_time']; ?></td>
                        <td><a href="./nncms_content_command_log_list.php?command_task_id=<?php echo $item['nns_id'];?>">查看</a>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
    </div>
    <div class="pagecontrol">
        共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="controlbtns">
        <div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
        <div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        <div style="clear:both;"></div>
    </div>
</body>
</html>