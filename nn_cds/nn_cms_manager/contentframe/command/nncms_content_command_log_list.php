<?php
/**
 * 单个流程日志列表
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/8/8 10:51
 */

header("Content-Type:text/html;charset=utf-8");
include ("../../nncms_manager_inc.php");
//加载多语言
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"]))
{
    $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri -> nns_db_pri_check($_SESSION["nns_role_pris"], "135002");
$checkpri = null;
if (!$pri_bool)
{
    Header("Location: ../nncms_content_wrong.php");
    exit ;
}
$arr_file = pathinfo(__FILE__);
$base_file_name = $arr_file['basename'];
$base_file_name_base = substr($arr_file['filename'],0, -5);
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
    "db_policy" => NL_DB_WRITE,
    "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
));

$hash_dc = nl_get_dc(
    array('db_policy' => NL_DB_WRITE | NL_DB_READ,'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE),
    array('db_type' => NP_DB_TYPE_HASH_TABLE_MYSQL)
);

/**
 * 加载LOGIC
 */
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/nn_logic/command/command_task_log.class.php';


$filter['nns_command_task_id'] = $_REQUEST['command_task_id'];

$result_data = \nl_command_task_log::get_list($hash_dc, $filter);

if($result_data['ret'] != 0 || !is_array($result_data['data_info']))
{
    echo '<script>alert("'.$result_data['reason'].'");history.go(-1);</script>';die;
}
$vod_total_num = count($result_data['data_info']);
$nns_url_op = "&" . http_build_query($_GET);
unset($_GET['page']);
$nns_url = "&" . http_build_query($_GET);
$refresh = "?" . http_build_query($_GET);

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
    <link href="../../css/rate.css" rel="stylesheet" type="text/css" />
    <link href="../../css/public.css" rel="stylesheet" type="text/css" />
    <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
    <script language="javascript" src="../../js/cms_cookie.js"></script>
    <script language="javascript" src="../../js/table.js.php"></script>
    <script language="javascript" src="../../js/checkinput.js.php"></script>
    <script language="javascript" src="../../js/rate.js"></script>
    <script language="javascript" src="../../js/image_loaded_func.js"></script>
    <script language="javascript" src="../../js/cms_datepicker.js"></script>
    <script language="javascript" src="../../js/alertbox.js"></script>
    <script language="javascript">
        function refresh_vod_page() {
            var num = $("#nns_list_max_num").val();
            setCookie("page_max_num",num);
            window.location.href='<?php echo $refresh; ?>&view_list_max_num='+num;
        }
        function checkhiddenBox(type) {
            BoxKey = false;
            $("input.checkhiddenInput:checked").each(function() {
                if ($(this).attr('rel') == type) {
                    BoxKey = true;
                    return false;
                }
            })
            return BoxKey;
        }
        function delete_id(){
            var r=confirm("是否进行批量删除操作");
            if(r == true){
                var ids=getAllCheckBoxSelect();
                ids = ids.substr(0,ids.length-1);
                if(ids==""){
                    alert('请选择数据');
                }else{
                    var url = "<?php echo $base_file_name_base;?>_control.php?action=delete&nns_id=&nns_id="+ids;
                    window.location.href = url;
                }
            }
        }
    </script>
</head>
<body>
<div class="content">
    <!-- 栏目导航描述div -->
    <div class="content_position">流程日志 > 详细流程日志列表</div>
    <!-- 内容列表 DIV -->
    <div class="content_table formtable">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <th><input name="" type="checkbox" value="">序号</th>
                <th>状态</th>
                <th>描述</th>
                <th>创建时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(is_array($result_data['data_info']) && !empty($result_data['data_info']))
            {
                $num = 0;
                foreach ($result_data['data_info'] as $item)
                {
                    $num++;
                    ?>
                    <tr>
                        <td><input name="input" type="checkbox" value="<?php echo $item["nns_id"]; ?>"><?php echo $num; ?></td>
                        <td>
                            <?php
                            switch ($item['nns_status'])
                            {
                                case '0':
                                    echo '<font color="blue">已达最终状态</font>';
                                    break;
                                case '1':
                                    echo '<font color="green">中心同步指令等待注入</font>';
                                    break;
                                case '2':
                                    echo '<font color="green">中心同步指令等待切片</font>';
                                    break;
                                case '3':
                                    echo '<font color="green">中心同步指令正在切片</font>';
                                    break;
                                case '4':
                                    echo '<font color="gray">中心同步指令取消注入</font>';
                                    break;
                                case '5':
                                    echo '<font color="green">中心同步指令等待前置sp</font>';
                                    break;
                                case '6':
                                    echo '<font color="green">中心同步指令未审核</font>';
                                    break;
                                case '7':
                                    echo '<font color="green">中心同步指令正在转码</font>';
                                    break;
                                case '8':
                                    echo '<font color="green">中心同步指令等在转码</font>';
                                    break;
                                case '9':
                                    echo '<font color="red">转码失败</font>';
                                    break;
                                case '10':
                                    echo '<font color="green">中心同步指令注入失败</font>';
                                    break;
                                case '11':
                                    echo '<font color="green">C2队列等待注入</font>';
                                    break;
                                case '12':
                                    echo '<font color="green">C2队列正在注入CDN</font>';
                                    break;
                                case '13':
                                    echo '<font color="green">C2队列注入CDN成功</font>';
                                    break;
                                case '14':
                                    echo '<font color="red">C2队列注入CDN失败</font>';
                                    break;
                                case '15':
                                    echo '<font color="gray">C2队列注入CDN取消</font>';
                                    break;
                                case '16':
                                    echo '<font color="green">C2队列正在注入EPG</font>';
                                    break;
                                case '17':
                                    echo '<font color="green">C2队列注入EPG成功</font>';
                                    break;
                                case '18':
                                    echo '<font color="red">C2队列注入EPG失败</font>';
                                    break;
                                case '19':
                                    echo '<font color="gray">C2队列注入EPG取消</font>';
                                    break;
                                case '20':
                                    echo '<font color="green">C2队列取消</font>';
                                    break;
                                case '21':
                                    echo '<font color="green">C2队列等待注入CDN</font>';
                                    break;
                                case '22':
                                    echo '<font color="green">C2队列等待注入EPG</font>';
                                    break;
                                case '23':
                                    echo '<font color="green">中心指令发送到切片队列失败</font>';
                                    break;
                                case '24':
                                    echo '<font color="red">切片失败</font>';
                                    break;
                                case '25':
                                    echo '<font color="green">切片成功</font>';
                                    break;
                                case '26':
                                    echo '<font color="green">转码成功</font>';
                                    break;
                                case '27':
                                    echo '<font color="green">正在转码</font>';
                                    break;
                            }
                            ?>
                        </td>
                        <td><?php echo $item['nns_desc']; ?></td>
                        <td><?php echo $item['nns_create_time']; ?></td>
                        <td><a href="<?php echo $g_bk_web_url . "/data/" . $item['nns_execute_url']; ?>" target="_blank">查看日志</a></td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
    </div>
    <!-- 分页DIV -->
    <div class="pagecontrol">
        共<span style="font-weight:bold;color:#ff0000;"><?php echo $vod_total_num; ?></span>条记录&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="controlbtns">
        <!--
        <div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);">全选</a></div>
        <div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);">取消</a></div>
        <div class="controlbtn delete"><a href="javascript:delete_id();">删除</a></div>
        -->
        <div class="controlbtn back"><a href="javascript:returnToHistory();"><?php echo cms_get_lang('back');?></a></div>
        <div style="clear:both;">
        </div>
    </div>

</div>
</body>
</html>