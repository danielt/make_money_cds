<?php //header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
error_reporting(E_ALL ^ E_NOTICE);
session_start();

include ("nncms_manager_inc.php");
include ($nncms_config_path . "nn_cms_config.php");
//加载多语言
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');

include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include $nncms_config_path . 'nn_cms_db/nns_manager/nns_db_manager_class.php';
include $nncms_config_path . 'nn_cms_db/nns_role/nns_db_role_class.php';

$nns_manager = new nns_db_manager_class();
if (isset($_SESSION['nns_mgr_id'])) {
	$nns_manager_list = $nns_manager->nns_db_manager_info($_SESSION['nns_mgr_id']);;
}

$nns_role = new nns_db_role_class();
if ($nns_manager_list['ret'] == 0) {
	$nns_role_info = $nns_role->nns_db_role_info($nns_manager_list['data'][0]['nns_role_id']);;
}

if ($nns_role_info['ret'] == 0) {
	$nns_role_pri = $nns_role_info['data'][0]['nns_pri_id'];
}


if (!isset($_SESSION["nns_mgr_id"])) {
	Header("Location: index.php");
} else if (empty($_SESSION["nns_mgr_id"])) {
	Header("Location: index.php");
}

$g_bk_mode_v2 = g_cms_config::get_g_config_value('g_bk_mode');
$g_bk_sp = g_cms_config::get_g_config_value('g_bk_sp');
global $g_medium_enabled;
$medium_enabled = $g_medium_enabled;
unset($g_medium_enabled);
if(empty($g_bk_mode_v2)){
	include_once dirname(dirname(__FILE__)) . '/mgtv/models/sp_model.php';
}elseif($g_bk_mode_v2==1){
	include_once dirname(dirname(__FILE__)) . '/mgtv_v2/models/sp_model.php';
}

include_once dirname(dirname(__FILE__)) . '/nn_logic/sp/sp.class.php';


include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
$checkpri = new nns_db_pri_class();
include_once dirname(dirname(__FILE__)) . '/nn_logic/sp/sp.class.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));

$data = nl_sp::query_by_condition($dc, array('nns_state'=>0));
$data = $data['data_info'];
// $data = sp_model::get_sp_list();

$result = nl_cp::query_all($dc);

if($result['ret'] !=0)
{
	echo '<script>alert("'.$result['reason'].'");history.go(-1);</script>';die;
}
$display_sp_flag = false;
$result_cp = null;

if(isset($result['data_info']) && is_array($result['data_info']) && !empty($result['data_info']))
{
	$display_sp_flag = true;
	foreach ($result['data_info'] as $val)
	{
		$str_cp_id = $val['nns_id'];
		$cp_config = (strlen($val['nns_config']) >0) ? json_decode($val['nns_config'],true) : null;
		
		if(isset($cp_config['cdn_receive_import_enable']) && $cp_config['cdn_receive_import_enable'] == '1')
		{
			$result_cp[] = $str_cp_id;
		}
	}
}

// $display_sp_flag = (isset($result['data_info']) && $result['data_info'] > 0 ) ? true : false;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>媒资分发系统</title>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="css/alert.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="js/jquery-1.4.3.min.js"></script>
<script src="../nn_cms_manager_v2/view/public/js/jquery.min.js"></script>
        <script src="../nn_cms_manager_v2/view/public/Starcor.S67.js"></script>
        <script src="../nn_cms_manager_v2/view/public/Starcor.S67.MVC.js"></script>
        <script language="javascript" src="../nn_cms_manager_v2/view/public/NBOSS.js"></script>
<script language="javascript" src="js/alertbox.js"></script>
<script language="javascript" src="js/frame.js"></script>
<script language="javascript" src="js/cms_ajax.js"></script>
<script>
	$(document).ready(function() {
		//setInterval(function(){
		//	getAjax('controls/nncms_control_ajax_check_database.php?1=1',get_db_config);
		//},5000)

	});

	function get_db_config(config) {
		if (config.mode == 'READ') {
			//			alert("主数据库挂掉！");
			$('#errors').html("当前数据库:" + config.host + "  主数据库已挂掉！");
		}
	}
</script>
</head>
<body>

<div class="top">
	<div class="topContent">
			<span class="loginlogo_bg"><?php echo strlen($g_prject_ch_name) <1 ? "播控子平台-媒资注入平台" : $g_prject_ch_name;?></span>
		<div class="topUserStatus">
			<div class="header_user"><img src="images/header_admin<?php
			if ($_SESSION['nns_mgr_id'] == g_sys_loginid) { echo '_super';
			} else {
			};
		?>.png" /></div>
			<div class="header_username"><span class="userName"><?php echo $_SESSION["nns_mgr_name"]; ?></span></div>
			<div class="header_rightname">
				<span class="rightName"><?php echo $_SESSION["nns_role_name"]; ?></span>
			</div>
			<div class="header_quit">
				<span class="header_divide"><img src="images/header_divide.png" alt="<?php echo cms_get_lang('quit'); ?>" /></span>
				<a href="index.php" class="quit_link">
					<span class="topControlBar"><?php echo cms_get_lang('quit'); ?></span>
				</a>
			</div>
		</div>
		<div class="header_logintime">
				<span id="errors" style="font-size:12px; color:#ff0000"></span>
				<?php if($_SESSION["nns_mgr_id"] !=  g_sys_loginid){ ?>
			 <?php echo cms_get_lang('dlsj'); ?>: <span class="lastTime"><?php echo $_SESSION["nns_last_login_time"]; ?></span> <?php }
					else { echo cms_get_lang('current_time').':&nbsp;'.date('H:i:s'); }
 ?>
		</div>
	</div>
</div>
<div class="menu">
	<dl>
		<dt>
			<div class="left_btn"><img src="images/leftsplit.png"/></div>
		</dt>
		<dd>
			<div class="menubtn select">
				<div class="menubtn_left"></div>
				<div class="menubtn_center"><a href="javascript:void(null);" pos="leftframe/nncms_left_xtsz.php"><?php echo cms_get_lang('xtpz'); ?></a></div>
				<div class="menubtn_right"></div>

			</div>
		</dd>
		<?php if($medium_enabled){?>
		<dd>
            <div class="menubtn select">
                <div class="menubtn_left"></div>
                <div class="menubtn_center"><a href="javascript:void(null);" pos="leftframe/nncms_left_jzkgl.php">介质库管理</a></div>
                <div class="menubtn_right"></div>
            </div>
        </dd>
		<?php }else{?>
		<dd>
			<div class="menubtn select">
				<div class="menubtn_left"></div>
				<div class="menubtn_center"><a href="javascript:void(null);" pos="leftframe/nncms_left_cp.php">上游平台管理</a></div>
				<div class="menubtn_right"></div>
			</div>
		</dd>
		<?php if($display_sp_flag){?>
		<dd>
			<div class="menubtn select">
				<div class="menubtn_left"></div>
				<?php
				if(empty($g_bk_mode_v2)){
				?>
				<div class="menubtn_center"><a href="javascript:void(null);" pos="leftframe/nncms_left_sp.php">下游平台管理</a></div>
				<?php
				}elseif($g_bk_mode_v2==1){
				?>
				<div class="menubtn_center"><a href="javascript:void(null);" pos="leftframe/nncms_left_sp_v2.php">下游平台管理</a></div>
				<?php
				}
				?>
				<div class="menubtn_right"></div>
			</div>
		</dd>
		<dd>
			<div class="menubtn select">
				<div class="menubtn_left"></div>
				<div class="menubtn_center"><a href="javascript:void(null);" pos="leftframe/nncms_left_project.php">通用配置管理</a></div>
				<div class="menubtn_right"></div>
			</div>
		</dd>
		<?php }?>
		
		<?php if (pub_func_get_module_right('103')){?>
		<dd>
			<div class="menubtn select">
				<div class="menubtn_left"></div>
				<div class="menubtn_center"><a href="javascript:void(null);" pos="leftframe/nncms_left_hzhb.php"><?php echo cms_get_lang('xtgl_hzgl'); ?></a></div>
				<div class="menubtn_right"></div>

			</div>
		</dd>
		<?php } ?>


		<?php if (pub_func_get_module_right('106') || pub_func_get_module_right('107')){?>
		<dd>
			<div class="menubtn select">
				<div class="menubtn_left"></div>
				<div class="menubtn_center"><a href="javascript:void(null);" pos="leftframe/nncms_left_ypgl.php"><?php echo cms_get_lang('media_zykgl'); ?></a></div>
				<div class="menubtn_right"></div>

			</div>
		</dd>
		<?php } ?>

		<?php if(is_array($result_cp) && !empty($result_cp))
			{
				?>
				<dd>
					<div class="menubtn select">
						<div class="menubtn_left"></div>
						<div class="menubtn_center"><a href="javascript:void(null);" pos="leftframe/nncms_left_cdn.php">CDN接收管理</a></div>
						<div class="menubtn_right"></div>
					</div>
				</dd>
				<?php 
			}
		?>
        
		<?php if (pub_func_get_module_right('133')):?>
		<dd>
			<div class="menubtn select">
				<div class="menubtn_left"></div>
				<div class="menubtn_center"><a href="javascript:void(null);" pos="../nn_models/nn_label/admin.php?action=left_menu&menu=label"><?php echo cms_get_lang('xtgl_labels_management'); ?></a></div>
				<div class="menubtn_right"></div>
			</div>
		</dd>
		<?php endif; ?>






		<?php if (pub_func_get_module_right('135')){
			foreach ($data as  $value) {
                if($value['nns_state'] !='1'){
				if (isset($nns_role_pri)) {
					if (strpos($nns_role_pri, $value['nns_id']) !== false) {

		?>
		<dd>
			<div class="menubtn select">
				<div class="menubtn_left"></div>
				<?php
				if(empty($g_bk_mode_v2)){
				?>
				<div class="menubtn_center"><a href="javascript:void(null);" pos="leftframe/nncms_left_fjyd.php?sp_id=<?php echo $value['nns_id'];?>"><?php echo $value['nns_name'];?></a></div>
				<?php
				}elseif($g_bk_mode_v2==1){
				?>
				<div class="menubtn_center"><a href="javascript:void(null);" pos="leftframe/nncms_left_fjyd_v2.php?sp_id=<?php echo $value['nns_id'];?>"><?php echo $value['nns_name'];?></a></div>
				<?php
				}
				?>
				<div class="menubtn_right"></div>
			</div>
		</dd>
		<?php }}else {	?>				
         <dd>
			<div class="menubtn select">
				<div class="menubtn_left"></div>
				<?php
				if(empty($g_bk_mode_v2)){
				?>
				<div class="menubtn_center"><a href="javascript:void(null);" pos="leftframe/nncms_left_fjyd.php?sp_id=<?php echo $value['nns_id'];?>"><?php echo $value['nns_name'];?></a></div>
				<?php
				}elseif($g_bk_mode_v2==1){
				?>
				<div class="menubtn_center"><a href="javascript:void(null);" pos="leftframe/nncms_left_fjyd_v2.php?sp_id=<?php echo $value['nns_id'];?>"><?php echo $value['nns_name'];?></a></div>
				<?php
				}
				?>
				<div class="menubtn_right"></div>
			</div>
		</dd>
		<?php }}}}}  ?>
		<dt style="float:right">
		<div class="right_btn"><img src="images/rightsplit.png"/></div>
		</dt>
	</dl>
</div>
<div class="main">
	<div class="left_frame" name="left_frame">
		<iframe scrolling="no" frameborder="0" onload="javascript:resetFrameHeight();"></iframe>
	</div>
	<div class="split_btn"></div>
	<div class="right_frame">
		<iframe scrolling="no" frameborder="0" onload="javascript:resetFrameHeight();"></iframe>
	</div>
	<div style="clear:both;"></div>
</div>
<div class="bottom">
	<div class="bottomright">
	<div class="bottomleft"> Copyright &copy; <?php echo date('Y');?> <?php echo cms_get_lang('company'), ' ', cms_get_lang('copyright'); ?> &nbsp;&nbsp;<?php echo NN_VERSION; ?></div>
	</div>
</div>
<div class="modal" id="myModal2" tabindex="-1" role="alert" aria-labelledby="myModalalert" aria-hidden="true" style="display:none;">
            <div class="modal-dialog alertsize">
                <div class="modal-content">
                    <div class="modal-header">
                        <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
                        <h4 class="modal-title" id="myModalLabel">错误</h4>
                    </div>
                    <div class="modal-body">
                        添加成功
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="__alert_btn_sure" class="btn btn-primary">确认</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
            <div class="modal-dialog dialogsize">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="dailog_head">Modal title</h4>
                        <a href="javascript:;" onclick="return false" class="close">关闭</a>
                    </div>
                    <div class="modal-body">
                        <iframe src="" id="alertframe"></iframe>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="close" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="button" id="dailog_sure" class="btn btn-primary">确认</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <div id="loading" style="display: none;">
            <div id="laod_percent"></div>
            <div id="loading_images" style="background-position: -1000px 50%;"></div>
        </div>
        <div id="alertbox"></div>
</body>
</html>
