<?php

/*
 * Created on 2012-5-1
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
//include "nns_pinyin_class.php";

class nns_controls_pinyin_class {

        public static $length = 0;

        public static function get_pinyin_letter($str) {
                $py = new nl_pinyin();
                $pinyin = $py->str2py($str);
                self::$length = $py->length;
                return $pinyin;
        }

}
?>