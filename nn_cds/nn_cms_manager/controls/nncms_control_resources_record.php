<?php
/*
 * Created on 2012-8-21
 * by S67
 * 资源变动增减记录
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

include dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.'nn_cms_db/nns_boss/nns_boss_class.php';
include_once dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.'nn_cms_config'.DIRECTORY_SEPARATOR.'nn_cms_global.php';
 //include dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.'nn_cms_db/nns_ftp/nncms_ftp_control.php';
include_once dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.'nn_logic/log/manager_log.class.php';
 class nncms_control_resources_record{
 	private $actions=array();
 	private $packet_header;
 	private $category_names;
 	private $dc;
 	public function nncms_control_resources_record(){
		$this->dc=nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
			));
		$this->dc->open();	
 	}

 	public function save_packet_info($params){
 				
 		if ($params['packet_type']=='0'){
 			$packet_type='asset';
 		}elseif ($params['packet_type']=='1'){
 			$packet_type='service';
 		}elseif ($params['packet_type']=='2'){
 			$packet_type='topic';
 		}		
 		$this->category_names=nl_manager_log::get_names_by_packet_id($this->dc,$params['packet_id'],$packet_type);
 		$params['packet_name']=empty($params['packet_name'])?$this->category_names['1000']:$params['packet_name'];
 		$this->packet_header=$params['packet_id'].'|' .
 				$params['packet_name'].'|'.
 				$params['packet_type'].'|'.
 				$params['group_id'].'|' .
 				$params['group_type']."\n";
 	}

 	public function save_action($params){
 		array_push($this->actions,$params);
 	}

 	public function clear_actions(){
 		$this->actions=null;
 		$this->actions=array();
// 		$this->dc->close();
// 		$this->dc=NULL;
 	}

 	public function submit_actions($method='asset',$detail=''){
 		global $g_resource_file_path;
 		$record=$this->packet_header;
 		$num=0;
 		$len=count($this->actions);
 		foreach ($this->actions as $action){
 			
 			$action['category_name']=empty($action['category_name'])?$this->category_names[$action['category_id']]:$action['category_name'];
 			
 			$record_one=$action['video_id'].'|' .
 					$action['video_type'].'|' .
 					$action['video_name'].'|' .
 					$action['category_id'].'|' .
 					$action['category_name'].'|' .
 					$action['action']."\n";
 			$record.=$record_one;
 			$num++;
 			if ($num==$len){
 				$record.='md5:'.MD5($record_one);
 			}
			
 		}
 		$params=array();
 		$params['header']=$method;
 		$params['g_boss_file_path']=$g_resource_file_path;
 		$params['user_id']=$detail;
 		$params['boss_ftp_record']=$record;
 		//nncms_ftp_control::write_boss_play_log($params);
// 		var_dump($g_resource_file_path);
 	
 		nns_boss_class::write_video_data($params);
 	}
 }
?>