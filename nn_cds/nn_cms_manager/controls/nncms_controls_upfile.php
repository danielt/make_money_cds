<?php
/*
 * Created on 2012-3-15
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class nncms_controls_upfile{
public $previewsize=0.125  ;   //预览图片比例
public $preview=0;   //是否生成预览，是为1，否为0
    public $datetime;   //随机数
    public $ph_name;   //上传图片文件名
    public $ph_tmp_name;    //图片临时文件名
    public $ph_path="./userimg/";    //上传文件存放路径
	public $ph_type;   //图片类型
    public $ph_size;   //图片大小
    public $imgsize;   //上传图片尺寸，用于判断显示比例
    public $al_ph_type=array('image/jpg','image/jpeg','image/png','image/pjpeg','image/gif','image/bmp','image/x-png');    //允许上传图片类型
    public $al_ph_size=50000000;   //允许上传文件大小
    public $web_base_url;
    public $web_address;
    public $img_type;
  function __construct(){
    $this->set_datatime();
  }
  function set_datatime(){
   $this->datetime=date("YmdHis");
  }

  function broken_dir($num){
  	if ($num>0){
	  	$url_dirs=explode("/",$this->web_base_url);
	  	$broken_dir=$url_dirs[$num];
	  	$broken_dir_new=substr($broken_dir,strlen($broken_dir)-1,1)."/".substr($broken_dir,strlen($broken_dir)/2,1)."/".substr($broken_dir,0,1)."/".$broken_dir;
	  	$this->web_base_url=str_replace($broken_dir,$broken_dir_new,$this->web_base_url);
  	}
  }

  function get_ph_defaultname($defaultname){
  	$this->img_type=ltrim(strrchr($defaultname,'.'),'.');
  }

   //获取文件类型
  function get_ph_type($phtype){
     $this->ph_type=$phtype;
  }
  //获取文件大小
  function get_ph_size($phsize){
     $this->ph_size=$phsize."<br>";
  }
  //获取上传临时文件名
  function get_ph_tmpname($tmp_name){
    $this->ph_tmp_name=$tmp_name;
    $this->imgsize=getimagesize($tmp_name);
  }
  //获取原文件名
  function get_ph_name($phname){
  	 $this->web_address=$this->web_base_url.$phname. ".".$this->img_type;
    $this->ph_name=$this->ph_path.$phname. ".".$this->img_type; //strrchr获取文件的点最后一次出现的位置
//$this->ph_name=$this->datetime.strrchr($phname,"."); //strrchr获取文件的点最后一次出现的位置
	return $this->ph_name;
  }
// 判断上传文件存放目录
  function check_path($default_dir=''){
  	if (empty($default_dir)){
  		$default_dir=$this->ph_path;
  	}
    if(!file_exists($default_dir)){
    	if (strstr($default_dir, '\\')){
    		$default_dir=str_replace('/','\\',$default_dir);
    		 $this->create_path_dir($default_dir);
    	}else{
    		 $this->create_path_dir($default_dir,'/');
    	}


    }
  }
  //判断上传文件是否超过允许大小
  function check_size(){
    if($this->ph_size>$this->al_ph_size){
     $this->showerror(cms_get_lang('contorl_sctpgd'));
    }
  }
  //判断文件类型
  function check_type(){
   if(!in_array($this->ph_type,$this->al_ph_type)){
         $this->showerror(cms_get_lang('contorl_sctplxcw'));
   }
  }
  //上传图片
   function up_photo(){
	   if(!move_uploaded_file($this->ph_tmp_name,$this->ph_name)){

	   }else{
	   		$old=umask(0);
	   		chmod($this->ph_name, 0777);
	   		umask($old);
	   }

  }
  //图片预览
   function showphoto(){
      if($this->preview==1){
      if($this->imgsize[0]>2000){
        $this->imgsize[0]=$this->imgsize[0]*$this->previewsize;
             $this->imgsize[1]=$this->imgsize[1]*$this->previewsize;
      }
         echo("<img src=\"{$this->ph_name}\" width=\"{$this->imgsize['0']}\" height=\"{$this->imgsize['1']}\">");
     }
   }
  //错误提示
  function showerror($errorstr){
    echo "<script language=java script>alert('$errorstr');location='java script:history.go(-1);';</script>";
   exit();
  }
  function save(){
   $this->check_path();
   $this->check_size();
//   $this->check_type();
   $this->up_photo();


//   $this->showphoto();
  }

  private function create_path_dir($path,$split='\\') {
		$dir_arr = explode($split, $path);
		$dir_str = $dir_arr[0];
		$num=0;
		$old=umask(0);
//		if(!file_exists($path)){
//			 mkdir($path, 0777,true);
//		}
//		@chmod($path, 0777);

		foreach ($dir_arr as $dir) {
			if ($num!=0 && $num!=count($dir_arr)-1){
				$dir_str .=$split. $dir ;
				if (!file_exists($dir_str)) {
					mkdir($dir_str, 0777,true);
				}
				chmod($dir_str, 0777);
			}
			$num++;
		}
		umask($old);
	}
}
?>