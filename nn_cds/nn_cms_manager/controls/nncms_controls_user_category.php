<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-9-29
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include("../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php";
include $nncms_config_path. "nn_cms_db/nns_common/nns_db_guid_class.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";
$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"110103");
$checkpri=null;
//memcache  缓存
include $nncms_config_path. "nn_logic/user/user.class.php";
$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
	));
$nl_user = new nl_user();

 $nns_org_id=$_SESSION["nns_org_id"];
 $nns_org_type=$_SESSION["nns_manager_type"];
 if (!$pri_bool){
 	 Header("Location: ../nncms_content_wrong.php");
 }else{
	 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
	$act = $_REQUEST['act'];
	 //获取用户分组绑定用户目录
	 include($nncms_db_path.'nns_user/nns_db_user_category_class.php');
	 $user_category = new nns_db_user_category_class();
	 include($nncms_db_path.'nns_user/nns_db_user_catalog_class.php');
	 $user_catalog = new nns_db_user_catalog_class();

	 if($act == 'get'){
	 	if(empty($_REQUEST['category_id'])){
	 		exit(error_msg());
	 	}
	 	$array = array(
	 		'nns_category_id'=> $_REQUEST['category_id'],
	 	);
	 	$category_info = $user_category->nns_db_user_category_catalog($array,'get');
	 	echo json_encode($category_info);exit;

	 }else if($act == 'add'){

	 	$category_id = $_REQUEST['category_id'];
	 	$catalog_id = $_REQUEST['catalog_id'];
	 	$catalog_name = $_REQUEST['catalog_name'];
	 	//验证参数
	 	if(empty($category_id) || empty($catalog_id) || empty($catalog_name)){
	 		$array = array(
	 			'ret'=> '1',
	 			'message'=>cms_get_lang('contorl_qscs'),
	 		);
	 		exit(json_encode($array));
	 	}
	 	//判断当前目录是否已经绑定该目录以及父目录和子目录
	 	$array = array(
	 		'nns_category_id'=> $category_id,
	 	);
	 	$catalog_info = $user_category->nns_db_user_catalog_info($catalog_id);
//	 	echo '<pre>';
//	 	print_r($catalog_info);exit;
		$catalog_path = $catalog_info['data']['0']['nns_id'];
	 	$category_rela = $user_category->nns_db_user_category_catalog($array,'get');

	 	if(count($category_rela['data'])>0){
	 		foreach($category_rela['data'] as $key=>$val){
	 			$catalog_arr[] = $val['nns_catalog_id'];
	 		}

	 		$catalog_arr = $user_category->nns_db_user_catalog_info($catalog_arr);
	 		foreach($catalog_arr['data'] as $key=>$val){
	 			//不能绑定已绑定目录的子目录
	 			//echo $catalog_path.'----'.$val['nns_catalog_path'];exit;
	 			if(strpos($catalog_path,$val['nns_id']) !== FALSE){
	 				$array = array(
			 			'ret'=> '1',
			 			'message'=>cms_get_lang('contorl_mlcf'),
			 		);
			 		exit(json_encode($array));
	 			}
	 			//如果绑定已绑定目录的父目录，则应该删除子目录
	 			if(strpos($val['nns_id'],$catalog_path) !== FALSE){
	 				$where = array(
				 		'catalog_id'=> $val['nns_id'],
				 		'category_id'=> $category_id,
				 	);
	 				$result = $user_category->nns_db_user_category_catalog(array(),'del',$where);
	 			}
	 		}

	 	}

	 	//绑定用户目录
	 	$guid = new Guid();
		$nns_id = $guid->toString();
		$current_date = date('Y-m-d H:i:s',time());
	 	$array = array(
	 		'nns_id'=> $nns_id,
	 		'nns_category_id'=> $category_id,
	 		'nns_catalog_id'=> $catalog_id,
	 		'nns_create_time'=> $current_date,
	 	);

	 	$result = $user_category->nns_db_user_category_catalog($array,'add');
	 	if($result['ret'] == '0'){
	 		$array = array(
	 			'nns_category_id'=> $category_id,
	 		);
	 		$category_info = $user_category->nns_db_user_category_catalog($array,'get');

	 		echo json_encode($category_info);exit;
	 	}
	 	exit(error_msg());

	 }else if($act == 'del'){

	 	$where = array(
	 		'catalog_id'=> $_REQUEST['catalog_id'],
	 		'category_id'=> $_REQUEST['category_id'],
	 	);

	 	$result = $user_category->nns_db_user_category_catalog(array(),'del',$where);
	 	if($result['ret'] == '0'){
	 		$array = array(
	 			'nns_category_id'=> $_REQUEST['category_id'],
	 		);
	 		$category_info = $user_category->nns_db_user_category_catalog($array,'get');

	 		echo json_encode($category_info);exit;
	 	}
	 	exit(error_msg());
	 }else if ($act == 'group_get'){
	 	if(empty($_REQUEST['user_id'])){
	 		exit(error_msg());
	 	}
	 	$array = array(
	 		'nns_user_id'=> $_REQUEST['user_id'],
	 	);
	 	$category_info = $user_category->nns_db_user_group_category('get',$array);

	 	echo json_encode($category_info);exit;
	 }else if ($act == 'group_add'){
	 	$user_id =  $_REQUEST['user_id'];
	 	$category_id = $_REQUEST['category_id'];
	 	$category_arr = explode(',',trim($category_id,','));
	 	//判断是否已经被帮顶了
	 	$array = array(
	 		'nns_user_id'=>$user_id,
	 	);
	 	$category_rela = $user_category->nns_db_user_group_category('get',$array);

	 	foreach($category_arr as $category_id){

		 	if(count($category_rela['data'])>0){
		 		//获取当前绑定的分组信息
		 		$category_info = $user_category->nns_db_user_category_info($category_id);
		 		$category_path = $category_info['data']['0']['nns_id'];
		 		//组装category 数组
		 		foreach($category_rela['data'] as $key=>$val){
		 			$category_arr2[] = $val['nns_user_category_id'];
		 		}

		 		$category_arr = $user_category->nns_db_user_category_info($category_arr2);
		 		foreach($category_arr['data'] as $key=>$val){
		 			//不能绑定已绑定目录的子目录
		 			if(strpos($category_path,$val['nns_id']) !== FALSE){
//		 				$array = array(
//				 			'ret'=> '1',
//				 			'message'=>'不能重复绑定相同目录！',
//				 		);
//				 		exit(json_encode($array));
		 			}
		 			//如果绑定已绑定目录的父目录，则应该删除子目录
		 			if(strpos($val['nns_id'],$category_path) !== FALSE){
		 				$where = array(
					 		'nns_user_id'=> $user_id,
					 		'nns_user_category_id'=> $val['nns_id'],
					 	);

					 	$result = $user_category->nns_db_user_group_category('del',$array=array(),$where);
		 			}
		 		}

		 	}
		 	//绑定用户目录
		 	$guid = new Guid();
			$nns_id = $guid->toString();
			$current_date = date('Y-m-d H:i:s',time());
		 	$array = array(
		 		'nns_id' =>$nns_id,
		 		'nns_user_category_id'=> $category_id,
		 		'nns_user_id'=> $_REQUEST['user_id'],
		 		'nns_create_time'=> $current_date
		 	);
		 	$result = $user_category->nns_db_user_group_category('add',$array);

		 	if($result['ret'] != '0'){
		 		echo json_encode($category_info);exit;
		 	}

	 	}

	 	//删除缓存
	 	$nl_user->delete_user_category_by_cache($dc,$_REQUEST['user_id']);
	 	$array = array(
 			'nns_user_id'=> $_REQUEST['user_id'],
 		);
 		$category_info = $user_category->nns_db_user_group_category('get',$array);

 		echo json_encode($category_info);exit;

	 }else if ($act == 'group_del'){
	 	$where = array(
	 		'nns_user_id'=> $_REQUEST['user_id'],
	 		'nns_user_category_id'=> $_REQUEST['category_id'],
	 	);

	 	$result = $user_category->nns_db_user_group_category('del',$array=array(),$where);

	 	if($result['ret'] == '0'){
	 		//删除缓存
	 		$nl_user->delete_user_category_by_cache($dc,$_REQUEST['user_id']);
	 		$array = array(
	 			'nns_user_id'=> $_REQUEST['user_id'],
	 		);
	 		$category_info = $user_category->nns_db_user_group_category('get',$array);
	 		echo json_encode($category_info);exit;
	 	}
	 	exit(error_msg());
	 }


 }
 function error_msg(){
 	$array = array(
	 			'ret'=> '1',
	 		);
	return json_encode($array);
 }
 $catalog_info = null;

 function check_user_catalog($catalog_id,$category_id){
 	global $nncms_db_path;
	global $user_catalog;
	global $user_category;
	global $catalog_info;
	$catalog_info = $user_catalog->nns_db_user_catalog_info($catalog_id);
 	if($catalog_info['ret'] == 0  && count($catalog_info['data'])>0){
 		$info = $catalog_info['data']['0'];
 		$array = array(
 			'nns_catalog_id'=> $info['nns_parent_id'],
 			'nns_category_id'=>$category_id,
 		);

 		$category_info = $user_category->nns_db_user_category_catalog($array,'get');

 		if(count($category_info['data'])>0){
 			return true;
 		}else{
 			if($info['nns_catalog_id'] == '0'){
 				return false;
 			}else if(!empty($info['nns_catalog_id'])){
 				check_user_catalog($info['nns_catalog_id'],$category_id);
 			}

 		}
		return false;

 	}
 	return false;

 }
?>