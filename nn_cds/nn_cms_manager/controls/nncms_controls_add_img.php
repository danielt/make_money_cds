<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../nncms_manager_inc.php");
//获取权限检查类
session_start();
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/table.js.php"></script>
<script language="javascript" src="../js/trim.js"></script>
<script language="javascript">
_selectfunc=select_table_tr;

$(document).ready(function(){
	$(".closebtn").click(function(){
		window.parent.close_select();
	});
})


</script>
</head>

<body>
<div class="selectcontrol">
	<div class="closebtn"><img src="../images/topicon_08-topicon.png" /></div>
	<div class="selecttitle"><?php echo cms_get_lang('contorl_sctp');?></div>
    <div class="searchbox"><input name="" type="text" id="search_input_param" value="<?php echo $search;?>"/>&nbsp;&nbsp;<input name="" type="button" value="<?php echo $language_action_search;?>" onclick="search_result();"/></div>
    <div style="padding:5px;">
    <form id="upimg_form" name="upimg_form"  method="post" action="../contentframe/service_pages/nncms_content_service_detail_control.php" enctype="multipart/form-data">

     <div class="content_table formtable">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tbody>
              <tr>
                <td><?php echo cms_get_lang('contorl_tpmc');?></td>
                <td><input name="image_name" id="image_name" type="text" size="80%"/></td>
              </tr>
              <tr>
                <td><?php echo cms_get_lang('contorl_tpwj');?></td>
                <td><input name="image_addr" id="image_addr" type="file" size="60%"/></td>
              </tr>
              <tr>
                <td></td>
                <td><input  type="button" value="<?php echo cms_get_lang('upload');?>"/></td>
              </tr>
          </tbody>
        </table>
    </div>
     </form>
    <!--<div class="btns"><input name="" type="button" value="确定"/>&nbsp;&nbsp;&nbsp;&nbsp;<input name="" type="button" value="关闭"/></div>
    </div>-->
</div>
</body>
</html>
