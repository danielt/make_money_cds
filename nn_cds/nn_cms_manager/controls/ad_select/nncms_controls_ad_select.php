<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
//ob_start();
session_start();
error_reporting(E_ALL ^ E_NOTICE);
include("../../nncms_manager_inc.php");
//获取权限检查类

//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
if (!empty($_COOKIE["page_max_num"])){
	$g_manager_list_max_num=$_COOKIE["page_max_num"];
}
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include($nncms_db_path. "nns_common/nns_db_pager_class.php");
$search=trim($_GET["search"]);
$except=trim($_GET["except"]);
$provider_name = trim($_GET['ad_provider_name']);


include($nncms_db_path. "nns_ad/nns_ad_item_class.php");
$params=array();
$params["name"]=$search;
$params['provider_name'] = $provider_name;
$params["except"]=$except;


//$ad_inst=new nns_ad_item_class();
//$countArr=$ad_inst->nns_ad_item_count($params);
//if ($countArr["ret"]==0)
//$total_num=$countArr["data"][0]["num"];

//$data_array=$ad_inst->nns_ad_item_list($params,($currentpage-1)*$g_manager_list_max_num,$g_manager_list_max_num);
//$ad_inst=null;

include_once LOGIC_DIR. "nl_common.func.php";
include_once LOGIC_DIR. "ad/ad_provider.class.php";
	
$dc = nl_get_dc(array("db_policy" => NL_DB_READ, "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE));
$dc -> open();

$currentpage=1;
if (!empty($_GET["page"])) $currentpage=$_GET["page"];

$page = ($currentpage-1)*$g_manager_list_max_num;
if ($search && !$provider_name){
	$count = nl_ad_item::count_ad_item_by_name_list($dc, $search);
	if(is_array($count)){
		$list = nl_ad_item::get_ad_item_by_name_list($dc, $search, $page, $g_manager_list_max_num);
	}
}elseif (!$search && $provider_name){
	$count = nl_ad_item::count_ad_item_by_proiver_name_list($dc, $provider_name);
	if(is_array($count)){
		$list = nl_ad_item::get_ad_item_by_proivder_name_list($dc, $provider_name, $page, $g_manager_list_max_num);
	}
}elseif ($search && $provider_name){
	$count = nl_ad_item::count_ad_item_by_name_and_provider_name_list($dc, $search, $provider_name);
	if(is_array($count)){
		$list = nl_ad_item::get_ad_item_by_name_and_provider_name_list($dc, $search, $provider_name, $page, 
							$g_manager_list_max_num);
	}
}else{
	$count = nl_ad_item::count_ad_item_all($dc);
	if(is_array($count)){
		$list = nl_ad_item::get_ad_item_all($dc, $page, $g_manager_list_max_num);
	}
}

if(is_array($count) && $count[0]['num']){
	$total_num = $count[0]['num'];
	$pages=ceil($total_num/$g_manager_list_max_num);
}else{
	$total_num = 0;
	$list = array();
	echo '<script>alert("没有找到您要的数据")</script>';
}

$url="?";
$url.="search={$search}&except={$except}&ad_provider_name={$provider_name}";
$pager = new nns_db_pager_class($total_num, $g_manager_list_max_num, $currentpage, $url, true);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet"
	type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/trim.js"></script>
<script language="javascript">

$(document).ready(function(){

	$(".closebtn").click(function(){
		window.parent.close_select();
	});

})

	function get_more_select(){
		var selects= getAllCheckBoxSelect();
		var select_name=getAllCheckBoxSelect(null,"nns_name");
		window.parent.set_ad_id(selects,select_name);
	}

function research_location(){
	var search = $("#nns_ad_search").val();
	var ad_provider_name = $("#nns_ad_provider_search").val();
	
	if(search == '- <?php echo cms_get_lang('search_content_need');?> -'){
		search = '';
	}else{
		search = encodeURI(search);
	}
	if(ad_provider_name == '- <?php echo cms_get_lang('search_content_need');?> -'){
		ad_provider_name = '';
	}else{
		ad_provider_name = encodeURI(ad_provider_name);
	}
	window.location.href="nncms_controls_ad_select.php?search="+search+"&ad_provider_name="+ad_provider_name;
}

//_selectfunc=select_table_tr;
//
//function select_table_tr($obj){
//	$(".content_table table tbody tr").removeClass("select");
//	$obj.addClass("select");
//	$(".category_edit_id").html($obj.find("td:eq(1)").html());
//	$(".category_edit_name").html($obj.find("td:eq(2)").html());
//	window.parent.set_ad_id($obj.find("td:eq(1)").html(),$obj.find("td:eq(2)").html());
//}
</script>
</head>

<body style='width: 600px; height: 550px;'>

<div class="content selectcontrol" style="padding: 0px; margin: 0px;">
<div class="closebtn"><img src="../../images/topicon_08-topicon.png" /></div>

<div style="background-color: #FFF;">


<div style="width: 96%; margin: 0px;">
<div class="content_table">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
			<td>广告<input name="nns_ad_search" id="nns_ad_search" type="text"
				onkeydown="if (event.keyCode==13){}"
				onblur="if(this.value=='') value='- <?php echo cms_get_lang('search_content_need');?> -'"
				onfocus="if(this.value=='- <?php echo cms_get_lang('search_content_need');?> -') value=''"
				value="<?php echo $search?$search:'- '.cms_get_lang('search_content_need').' -';?>"
				style="width: 130px;" />&nbsp;&nbsp;</td>
				
			<td>广告商<input name="nns_ad_provider_search" id="nns_ad_provider_search" type="text"
				onkeydown="if (event.keyCode==13){}"
				onblur="if(this.value=='') value='- <?php echo cms_get_lang('search_content_need');?> -'"
				onfocus="if(this.value=='- <?php echo cms_get_lang('search_content_need');?> -') value=''"
				value="<?php echo $provider_name?$provider_name:'- '.cms_get_lang('search_content_need').' -';?>"
				style="width: 130px;" />&nbsp;&nbsp;
				<input type="button" value="<?php echo cms_get_lang('search');?>"
				onclick="research_location();" /></td>
				
			<td><input type="button"
				value="<?php echo cms_get_lang('confirm_search_xz');?>"
				onclick="get_more_select();" /></td>
		</tr>

	</tbody>
</table>
</div>

</div>
<div class="content_table formtable">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<th width="5px"><input name="" type="checkbox" value="" /></th>
			<th width="26px"><?php echo cms_get_lang('segnumber');?></th>
			<th width="200px"><?php echo cms_get_lang('name');?></th>
			<th>广告商</th>
		</tr>
	</thead>
	<tbody>
	<?php
	if ($total_num){
		$num=($currentpage-1)*$g_manager_list_max_num; foreach($list as $item){
			$num++;
			?>
		<tr>
			<td><input nns_name="<?php echo $item["nns_name"];?>" name="input"
				type="checkbox" value="<?php echo  $item["nns_id"];?>" /></td>
			<td><?php echo $num;?></td>
			<td>
			<p
				style="width: 200px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis;"><?php echo $item["nns_name"];?></p>
			</td>
			<td><?php
			$ad_provider = nl_ad_provider::get_ad_provider_by_id($dc, $item["nns_ad_provider_id"]);
			if(is_array($ad_provider)){
				echo $ad_provider[0]['nns_name'];
			}
			?></td>
		</tr>
		<?php } } $least_num=$g_manager_list_max_num-count($list);
		for ($i=0;$i<$least_num;$i++){?>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<?php }?>
	</tbody>
</table>
</div>
		<?php echo $pager->nav_1();?></div>
</div>
</body>
</html>
