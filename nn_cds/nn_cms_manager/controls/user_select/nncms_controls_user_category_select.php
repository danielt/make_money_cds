<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include("../../nncms_manager_inc.php");
// 导入多语言包
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//导入语言包
 if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php";

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";

$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"110103");
$checkpri=null;

 $nns_org_id=$_SESSION["nns_org_id"];
 $nns_org_type=$_SESSION["nns_manager_type"];
 $category_id = $_REQUEST['category_id'];
 $selected = $_REQUEST['selected'];
 if (!$pri_bool){
	 Header("Location: ../nncms_content_wrong.php");
 }else{
	 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
	 include ($nncms_db_path . "nns_user/nns_db_user_class.php");
	 //获取用户目录信息
	 include($nncms_db_path.'nns_user/nns_db_user_category_class.php');
	 $user_category = new nns_db_user_category_class();

	 if (!empty($_COOKIE["page_max_num"])){
		$g_manager_list_max_num=$_COOKIE["page_max_num"];
	 }
	 //获取用户分组绑定用户目录
	 if($_REQUEST['type'] == 'ajax' && !empty($_REQUEST['category_id'])){
		$category_info = $user_category->nns_db_user_category_catalog($_REQUEST['category_id']);
		echo json_encode($category_info);exit;
	 }
	if($category_id){
		foreach($g_user_label_group as $key=>$val){
			if($val['nns_id'] == $category_id){
				$category_info = $val;
				$parent_name = $val['nns_name'];
			}
		}
	}

	 if ($category_info["ret"]!=0){
		$data=null;
		echo "<script>alert(". $service_array["reason"].");</script>";
	 }

// 用户群列表
	$nns_url="&nns_user_category_id=$category_id";
	$user_inst = new nns_db_user_class();

	if(!empty($category_id)){
		$str_sql = "select count(1) as num from nns_user_category  where nns_parent_id = '$category_id'";

	}else{
		$str_sql = "select count(1) as num from nns_user_category  where nns_parent_id = '".$g_user_label_group['0']['nns_id']."'";
	}
	$countArr = $user_category->nns_db_user_category_query($str_sql);
	if ($countArr["ret"] == 0)
		$total_num = $countArr["data"][0]["num"];
	$user_pages = ceil($user_total_num / $g_manager_list_max_num);
	$currentpage = 1;
	if (!empty ($_GET["page"]))
	$currentpage = $_GET["page"];

	$url="?1=1&category_id={$category_id}";
	include $nncms_db_path. "nns_common/nns_db_pager_class.php";
	$pager = new nns_db_pager_class($total_num,$g_manager_list_max_num,$currentpage,$url);

	if(!empty($category_id)){
		//$str_sql = "select nuc.*,nuc2.nns_category_name as parent_name from nns_user_category as nuc left join  nns_user_category as nuc2 on nuc2.nns_id = nuc.nns_parent_id where nuc.nns_parent_id = '$category_id'";
		$str_sql = "select nu.* from nns_user_group_category_relation as nugc left join nns_user_category as nu on nu.nns_id = nugc.nns_category_id where nugc.nns_group_id = '$category_id' limit ".($currentpage -1) * $g_manager_list_max_num.','. $g_manager_list_max_num;

	}else{
		$str_sql = "select nu.* from nns_user_group_category_relation as nugc left join nns_user_category as nu on nu.nns_id = nugc.nns_category_id where nugc.nns_group_id = '".$g_user_label_group['0']['nns_id']."' limit ".($currentpage -1) * $g_manager_list_max_num.','. $g_manager_list_max_num;
	}
	$user_array = $user_category->nns_db_user_category_query($str_sql);
	$data = $user_array['data'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
<script language="javascript" src="../../js/dtree.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
<script language="javascript">
$(document).ready(function(){
	$(".category_editbox").width($(".content").width()-400);
	 $(window).resize(function(){
			$(".category_editbox").width($(".content").width()-400);
	});
});


function select_tree_item(item_id,item_name,parent_id){
	$(".category_editbox").find(".category_edit_id").html(item_id);
	$(".category_editbox").find(".category_edit_name").html(item_name);
		$(".video_frame").show();
		now_select_id=item_id;
}
var now_select_id;
$(document).ready(function(){
	$(".closebtn").click(function(){
		window.parent.close_select();
	});

});

function set_user_category_id(){
	var user_id_str = '';
	$('.formtable input:checkbox:checked').each(function(){
		user_id_str += $(this).val()+',';
	});
	if(user_id_str == ''){
		alert('<?php echo cms_get_lang('msg_4');?>');return;
	}
	window.parent.set_user_category_id(user_id_str,$(".category_edit_name").html());
}

function check_select_height(){
	$(".selectbox iframe").height($(".selectbox iframe").contents().find(".selectcontrol").height());
	window.parent.check_select_height();
}

function close_select(){
		$(".selectbox").hide();
}
function category_select(){
	var category_id = $('#category_select').val();;
	window.location.href = 'nncms_controls_user_category_select.php'+'?category_id='+category_id;
}
</script>
</head>

<body>
<div class="content">
<div class="content selectcontrol" style="padding:0px; margin:0px;">
<div class="closebtn"><img src="../../images/topicon_08-topicon.png" /></div>

	<div style="background-color:#FFF;">


		<div class="category_editbox" style="width:700px; margin:0px;">
			 <div class="content_table">
			<table width="700" border="0" cellspacing="0" cellpadding="0">
			<tbody>
			<!--
				<tr>
					<td><?php echo cms_get_lang('lmgl_nrlmid');?>:</td>
					<td class="category_edit_id"></td>
				</tr>
				<tr>
					<td><?php echo cms_get_lang('lmgl_lmmc');?>:</td>
					<td  class="category_edit_name"></td>
				</tr>-->
				<tr>
					<!-- 这里暂时不知道是什么，暂时没替换 -->
					<td><?php echo cms_get_lang('zdgl_yhqz');?>:</td>
					<td  class="category_select">
						<select name ='category_select' id='category_select' <?php if ($selected){echo "disabled='disabled'";}?> onchange='javascript:category_select();'>
							<?php
								foreach($g_user_label_group as $key=>$val){

								?>
								<option <?php if($category_id == $val['nns_id']){echo "selected='selected'";}?> value='<?php echo $val['nns_id'];?>'><?php echo $val['nns_name'];?></option>
								<?php
								}
							?>
						</select>
					</td>
					 <td><input type="button" value="<?php echo cms_get_lang('confirm_search_xz');?>" onclick="set_user_category_id();"/></td>
				</tr>


				  </tbody>
				  </table>
				</div>

		</div>
		<div class="content_table formtable" style='float:left;' style="width:700px; margin:0px;">
			<table width="700px" border="0" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th><input name="" type="checkbox" value="" /></th>
					<th><?php echo cms_get_lang('segnumber');?></th>
					<th><?php echo cms_get_lang('zdgl_yhqid');?></th>
					<th><?php echo cms_get_lang('zdgl_yhm');?></th>
					<th><?php echo cms_get_lang('zdgl_lsqz');?></th>
					<th><?php echo cms_get_lang('order');?></th>
					<th><?php echo cms_get_lang('edit_time');?></th>
					<th><?php echo cms_get_lang('create_time');?></th>
				</tr>
			</thead>
			<tbody>
			<?php
			if ($data != null) {
				$num = ($currentpage -1) * $g_manager_list_max_num;
				foreach ($data as $item) {
					$num++;
			?>
				  <tr id='<?php echo  $item["nns_id"];?>'>
					<td><input name="input" type="checkbox" value="<?php echo  $item["nns_id"];?>" /></td>
					<td><?php echo $num;?></td>
					<td><?php echo $item["nns_id"];?></td>
					<td><?php echo $item["nns_category_name"];?></td>
					<td><?php if($parent_name == null)echo '用户群组';else echo $parent_name;?> </td>
					<td><?php echo $item['nns_category_sort']?></td>
					<td><?php echo $item["nns_modify_time"];?> </td>
					<td><?php echo $item["nns_create_time"];?> </td>
				  </tr>
			  <?php

				}
			}
			$least_num = $nncms_ui_min_list_item_count -count($data);
			for ($i = 0; $i < $least_num; $i++) {
		?>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp; </td>
					<td>&nbsp; </td>
					</tr>

				<?php	}?>
			  </tbody>
			</table>
			<?php echo $pager->nav();?>
		</div>
		<div style='float:left;'>

		 </div>
		<div style="clear:both;"/>
	</div>

</div>
</body>
</html>

<?php }?>
