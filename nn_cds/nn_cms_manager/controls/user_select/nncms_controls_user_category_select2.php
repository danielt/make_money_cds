<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
 include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php";

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";

$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"110103");
$checkpri=null;

 $nns_org_id=$_SESSION["nns_org_id"];
 $nns_org_type=$_SESSION["nns_manager_type"];
 $category_id = $_REQUEST['category_id'];
 if (!$pri_bool){
 	 Header("Location: ../nncms_content_wrong.php");
 }else{
	 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
<script language="javascript" src="../../js/dtree.js"></script>
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
<script language="javascript">
function set_albel_group(){
	var group_str = '';
	$('.category_editbox input:checkbox:checked').each(function(){
		group_str += $(this).val()+',';
	});
	if(group_str == ''){
		alert('<?php echo cms_get_lang('contorl_wxzsj');?>');
	}
	//alert(group_str);
	window.parent.set_albel_group(group_str);
}
function close_select(){
	window.parent.close_select();
}

</script>
</head>

<body>
<div class="content">
<div class="content selectcontrol" style="padding:0px; margin:0px;">
<div class="closebtn"><img onclick='close_select();' src="../../images/topicon_08-topicon.png" /></div>

	<div style="background-color:#FFF;">
	    <div class="content_table formtable" >
	    <div class="content_table">
	    	<table width="450" border="0" cellspacing="0" cellpadding="0">
	        <tbody>
	    		<tr>
	             	<td><?php echo cms_get_lang('lmgl_nrlmid');?>:</td>
	                <td class="category_edit_id"><?php if($category_id){}else{echo '10000';}?></td>
		      	</tr>
		      	<tr>
		      		<td><?php echo cms_get_lang('lmgl_lmmc');?>:</td>
	              	<td  class="category_edit_name"><?php if($category_id){}else{echo cms_get_lang('zdgl_yhqz');}?></td>
		      	</tr>
            	<tr>
	                <td><?php echo cms_get_lang('action');?>:</td>
	                <td><input type="button" value="<?php echo cms_get_lang('confirm_search_xz');?>" onclick="set_albel_group();"/></td>

		      	</tr>
			      </tbody>
			      </table>
			    </div>

	    </div>
	    <div class="category_editbox content_table" style="width:450px;">

	    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	        <thead>
	        	<tr>
	            	<th><input name="" type="checkbox" value="" /></th>
	                <th><?php echo cms_get_lang('segnumber');?></th>
	                <th><?php echo cms_get_lang('zdgl_yhqid');?></th>
	                <th><?php echo cms_get_lang('zdgl_yhm');?></th>
	                <th><?php echo cms_get_lang('zdgl_lsqz');?></th>
	            </tr>
	        </thead>
	        <tbody>
	        <?php
			$num = 0;
			foreach ($g_user_label_group as $item) {
//					 echo '<pre>';
//	 print_r($item);exit;
				$num++;
	?>
	              <tr id='<?php echo  $item["nns_id"];?>'>
	                <td><input name="input" type="checkbox" value="<?php echo  $item["nns_id"];?>" /></td>
	                <td><?php echo $num;?></td>
	                <td><?php echo $item["nns_id"];?></td>
	                <td><?php echo $item["nns_name"];?></td>

					<td><?php if($item["parent_name"] == null)echo cms_get_lang('zdgl_yhqz');else echo $item["parent_name"];?> </td>
	              </tr>
	          <?php

			}
		$least_num = $nncms_ui_min_list_item_count -count($g_user_label_group);
		for ($i = 0; $i < $least_num; $i++) {
	?>
	          		<tr>
	                <td>&nbsp;</td>
	                <td>&nbsp;</td>
	                <td>&nbsp; </td>
	                <td>&nbsp; </td>
	                <td>&nbsp; </td>
	              	</tr>

	          	<?php	}?>
	          </tbody>
	        </table>
	    </div>

	    <div style="clear:both;"></div>
	    <!--
	    <div class="controlbtns" style="width:450px;">
	    <div class="controlbtn allselect"><a href="javascript:selectAllCheckBox(true);"><?php echo cms_get_lang('select');?></a></div>
    	<div class="controlbtn cancel"><a href="javascript:selectAllCheckBox(false);"><?php echo cms_get_lang('cancel');?></a></div>
        <div style="clear:both;"></div>-->
    </div>
    </div>

</div>
</body>
</html>

<?php }?>
