<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-9-28
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";

$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"110103");
$checkpri=null;

 	$nns_org_id=$_SESSION["nns_org_id"];
 	$nns_org_type=$_SESSION["nns_manager_type"];
 if (!$pri_bool){
 	 Header("Location: ../nncms_content_wrong.php");
 }else{
	 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");

	 //获取用户目录信息
	 include($nncms_db_path.'nns_user/nns_db_user_catalog_class.php');
	 $user_catalog = new nns_db_user_catalog_class();
	 $catalog_info = $user_catalog->nns_db_user_catalog_info();
	 foreach($catalog_info['data'] as $key =>$val){
	 	$data[$val['nns_id']][] = $val;

	 }

	 if ($catalog_info["ret"]!=0){
	 	$data=null;
	 	echo "<script>alert(". $service_array["reason"].");</script>";
	 }

	$data = $catalog_info['data'];
 }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/dtree.js"></script>
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/trim.js"></script>
<script language="javascript">
var now_select_id;
$(document).ready(function(){
	$(".closebtn").click(function(){
		window.parent.close_select();
	});

});
function select_tree_item(item_id,item_name,parent_id){


	$(".category_editbox").find(".category_edit_id").html(item_id);
	$(".category_editbox").find(".category_edit_name").html(item_name);
		$(".video_frame").show();
		now_select_id=item_id;

}

function set_user_catalog_id(){
	window.parent.set_user_catalog_id(now_select_id,$(".category_edit_name").html());
}

function check_select_height(){
	$(".selectbox iframe").height($(".selectbox iframe").contents().find(".selectcontrol").height());
	window.parent.check_select_height();
}
</script>
</head>

<body  style='width:400px;'>

<div class="content selectcontrol" style="padding:0px; margin:0px;">
<div class="closebtn"><img src="../../images/topicon_08-topicon.png" /></div>

	<div style="background-color:#FFF;">


	    <div class="category_editbox" style="width:370px; margin:0px;">
		     <div class="content_table">
	    	<table width="370" border="0" cellspacing="0" cellpadding="0">
	        <tbody>
	    		<tr>
	             	<td><?php echo cms_get_lang('lmgl_nrlmid');?>:</td>
	                <td class="category_edit_id"></td>
		      	</tr>
		      	<tr>
		      		<td><?php echo cms_get_lang('lmgl_lmmc');?>:</td>
	              	<td  class="category_edit_name"></td>
		      	</tr>
            	<tr>
	                <td><?php echo cms_get_lang('action');?>:</td>
	                <td><input type="button" value="<?php echo cms_get_lang('confirm_search_xz');?>" onclick="set_user_catalog_id();"/></td>

		      	</tr>


			      </tbody>
			      </table>
			    </div>

	    </div>
	      <div class="category_tree" style="overflow:auto;width:370px; height:200px; margin:0px;">
			<?php
	   			$catalog_content = isset($data)?$data:'';
	   			foreach($catalog_content as $key=>$val){
					$catalog_content[$key]['nns_name'] = $val['nns_catalog_name'];
				}
	   			echo pub_func_get_tree_by_html_array($catalog_content,cms_get_lang('contorl_yhml'),'10000');

			?>

	    </div>

	    <div style="clear:both;"></div>
    </div>
</div>
</body>
</html>