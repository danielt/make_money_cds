<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include("../../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//获取权限检查类
include $nncms_db_path. "nns_pri/nns_db_pri_class.php";

$checkpri=new nns_db_pri_class();
$pri_bool=false;
$pri_bool=$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"],"110103");
$checkpri=null;

 $nns_org_id=$_SESSION["nns_org_id"];
 $nns_org_type=$_SESSION["nns_manager_type"];
 $nns_category_id = $_REQUEST['category_id'];
 $parent_id = $_REQUEST['parent_id'];
 $action = $_REQUEST['action'];
 if($action == 'add'){
	 if(empty($action) || empty($parent_id)){
	 	echo "<script>alert(".cms_get_lang('cscw').");</script>";
	 }
 }else if ($action == 'move'){
 	if(empty($action)){
	 	echo "<script>alert(".cms_get_lang('cscw').");</script>";
	 }
 }else{
 	if(empty($action) || empty($parent_id) || empty($nns_category_id)){
	 	echo "<script>alert(".cms_get_lang('cscw').");</script>";
	 }
 }
 if (!$pri_bool){
 	 Header("Location: ../nncms_content_wrong.php");
 }else{
	 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");

	 //获取用户目录信息
	 include($nncms_db_path.'nns_user/nns_db_user_category_class.php');
	 $user_category = new nns_db_user_category_class();
	 //获取用户分组绑定用户目录
	 //$category_list = $user_category->nns_db_user_category_catalog($nns_category_id);

	 $category_info = $user_category->nns_db_user_category_info($nns_category_id);
	 //获取父分组 名字 
	 $is_parent = false;
	 foreach($g_user_label_group as $val){
	 	if($val['nns_id'] == $parent_id){
	 		$is_parent = true;
	 		$category_name = $val['nns_name'];
	 	}
	 }
	 if(!$is_parent){
	 	$result = $user_category->nns_db_user_category_info($parent_id);
	 	$category_name = $result['data']['0']['nns_category_name'];
	 }

	 if ($category_info["ret"]!=0){
	 	echo "<script>alert(". $service_array["reason"].");</script>";
	 }
	 
	$data = $category_info['data'];
 }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
<script language="javascript" src="../../js/dtree.js"></script>
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
<script language="javascript">
$(document).ready(function(){
	$(".category_editbox").width($(".content").width()-400);
	 $(window).resize(function(){
		$(".category_editbox").width($(".content").width()-400);
	});
	
});
function get_catalog_user_list(){
	var category_id = $('#nns_category_id').val();
	var data = 'ajax=1&nns_user_category_id='+category_id;
	$(".user_list_frame iframe").attr("src","nncms_content_user_list.php?"+data);
}
function add_category_command(){
	if ($(".category_add_box").is(":hidden")){
		$(".category_add_box").show();
		$(".category_add_box").find(".parent_category_box").html($(".category_edit_box").find("#nns_category_detail_name").val());
	}else{
		$(".category_add_box").hide();
	}
}
function get_user_category_catalog(action,category_id,catalog_id,catalog_name){
	var data = '';
	switch (action){
		case "get":
			if(category_id == '10000'){
				data = '';
			}else{
				data = 'category_id='+category_id+'&act=get';
			}	
		break;
		case 'add':
			data = 'category_id='+category_id+'&catalog_id='+catalog_id+'&catalog_name='+catalog_name+'&act=add';
		break;
		case 'del':
			data = 'category_id='+category_id+'&catalog_id='+catalog_id+'&act=del';
		break;
	}

	$.ajax({
		url 	:'../../controls/nncms_controls_user_category.php',
		data	:data,
		type	:'POST',
		global	:false,
		success	:function(result){
			if(result != ''){
				var result = eval('('+result+')');
				if('undefined' != typeof result.data){
					var len = result.data.length;
	
					var html_str = '';
					for (var i=0;i<len;i++){
						html_str+="<div class=\"block_category\">"+result.data[i]["nns_catalog_name"]+"&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"javascript:get_user_category_catalog('del','"+result.data[i]["nns_category_id"]+"','"+result.data[i]["nns_catalog_id"]+"');\"><img src=\"../../images/topicon_08-topicon.png\"  border=\"0\"/></a></div>";
					}
					$('#bind_category_list').html(html_str);
				}
			}
			
			
		},
		
	});
}

function set_user_catalog_id(select_catalog_id,catalog_name){
	var select_category_id = $('#nns_category_id').val();
	get_user_category_catalog('add',select_category_id,select_catalog_id,catalog_name)
	
	close_select();
}
function order_node(action_str){

	$(".category_edit_box").find("#action").val(action_str);
	checkForm('<?php echo cms_get_lang('lmgl');?>','<?php echo cms_get_lang('msg_ask_edit');?>',$('#edit_form'),"edit");
}

function edit_node(){

	$(".category_edit_box").find("#action").val("edit");
	alert($('#edit_form'));
	checkForm('<?php echo  cms_get_lang('lmgl');?>','<?php echo cms_get_lang('msg_ask_edit');?>',$('#edit_form'),"edit");
}
function delete_node(){

	$(".category_edit_box").find("#action").val("delete");
	checkForm('<?php echo  cms_get_lang('lmgl');?>','<?php echo cms_get_lang('msg_ask_edit');?>',$('#edit_form'),"delete");
}
function begin_select_catalog(){
	var urlstr="../../controls/user_select/nncms_controls_user_catalog_select.php?";
	if ($("#selectbox1 iframe").attr("src")!=urlstr) $("#selectbox1 iframe").attr("src",urlstr);
	$("#selectbox1").show();
}
function close_select(){
		window.parent.close_select();
}
</script>
</head>
<body>
<?php if($action == 'add'){?>
<div class="content" style='width:520px;'>
<div class="content selectcontrol" style="padding:0px; margin:0px;">
<div class="closebtn" onclick = 'close_select();'><img src="../../images/topicon_08-topicon.png" /></div>
	<div style="background-color:#FFF;">
		    <div class="content_position"><?php echo cms_get_lang('contorl_tjfz');?></div>
		    <div class="category_add_box" style='width:500px;'>
		    	<form id="add_form" action="../../contentframe/devices_pages/nncms_content_user_category_control.php?ajax=1" method="post">
			    <input name="action" id="action" type="hidden" value="add" />
			    <input name="category_parent" id="category_parent" type="hidden" value="<?php echo $parent_id;?>" />
			    <div class="content_table">
			    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
			        <tbody>
			         	<tr>
			                <td class="rightstyle"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('lmgl_sjlm');?></td>
			                <td class="parent_category_box">
								<input type ='text' name='parent_category_name' disabled='true' value='<?php echo $category_name;?>' />
			                </td>

			      		</tr>
			              <tr>
			                <td class="rightstyle"> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('lmgl_lmnrmc');?></td>
			                <td><input name="nns_category_detail_name" id="nns_category_detail_name" type="text" rule="noempty"
			                	value=""
			                /></td>

			      		</tr>


			      		 <tr>
			                <td width="140"></td>
			                <td>
			                	<input name="" type="button" value="<?php echo cms_get_lang('confirm');?>" onclick="checkForm('<?php echo cms_get_lang('lmgl');?>','<?php echo cms_get_lang('msg_ask_edit');?>',$('#add_form'),'add');"/>&nbsp;&nbsp;
			                </td>

			      		</tr>
			          </tbody>
			        </table>
			    </div>
			    </form>
		    </div>
	    <div style="clear:both;"></div>
    </div>
</div>
<?php }?>
<!--
<?php if($action == 'edit'){?>
	<div class="content" style='width:723px;'>
<div class="closebtn" onclick = 'close_select();'><img src="../../images/topicon_08-topicon.png" /></div>
	<div style="background-color:#FFF;" >
	    	<div class="category_edit_box" style='width:723px;'>
				<div class="content_position"><?php echo cms_get_lang('modify_group');?></div>
		    <form id="edit_form" action="nncms_content_user_category_control.php" method="post">
			    <input name="action" id="action" type="hidden" value="" />
			     <input name="category_parent" id="category_parent" type="hidden" value="<?php echo $parent_id;?>" />
			    <input name="nns_category_id" id="nns_category_id" type="hidden" value="<?php echo $data['0']['nns_id'];?>" />
			    <div class="content_table">
			    	<table width="100%"  border="0" cellspacing="0" cellpadding="0">
			        <tbody>
			        	<tr>
			                <td width="120"><?php echo cms_get_lang('lmgl_nrlmid');?>:</td>
			                <td class="category_edit_id"><?php echo $data['0']['nns_id'];?></td>

			      		</tr>
			             <tr>
			                <td width="120"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('lmgl_lmmc');?>:</td>
			                <td><input name="nns_category_detail_name" id="nns_category_detail_name" type="text" rule="noempty" value="<?php echo $data['0']['nns_category_name'];?>"/></td>

			      		</tr>
			      		 <tr>
			                <td width="120"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font><?php echo cms_get_lang('lmgl_bdyhml');?>:</td>
			                <td id="bind_category_list">
			                </td>
			      		</tr>
			      		<tr>
			      			<td width="120"><?php echo cms_get_lang('action');?>:</td>
			                <td>
			                	<input name="" type="button" value="<?php echo cms_get_lang('edit');?>" onclick="edit_node();"/>&nbsp;&nbsp;
			                	<input name="" type="button" value="<?php echo cms_get_lang('delete');?>" onclick="delete_node();"/>&nbsp;&nbsp;
			                	<input name="" type="button" value="<?php echo cms_get_lang('order_pre');?>" onclick="order_node('up');"/>&nbsp;&nbsp;
			                	<input name="" type="button" value="<?php echo cms_get_lang('order_next');?>" onclick="order_node('down');"/>&nbsp;&nbsp;
			                	<input name="" type="button" value="<?php echo cms_get_lang('add|lmgl_tjzlm');?>" onclick="add_category_command();"/>&nbsp;&nbsp;
								<input name="" type="button" value="<?php echo $language_catelog_xzyhml;?>"  onclick="begin_select_catalog();"/>
			                </td>
			      		</tr>
			          </tbody>
			        </table>
			    </div>
			    </form>
		    </div>
	    <div style="clear:both;"></div>
    </div>
</div>
</div>
<?php }?>-->
</body>
</html>
