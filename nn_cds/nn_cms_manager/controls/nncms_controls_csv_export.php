<?php
/*
 * Created on 2012-5-1
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class nncms_controls_csv_export{
	private $db_path;
	public function nncms_controls_csv_export(){
		$this->db_path=dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR."nn_cms_db".DIRECTORY_SEPARATOR;
	}
	public function create_csv_content($search_data){
		$csv_content="cms_get_lang('contorl_zy_ID'),
		cms_get_lang('contorl_dymc'),
		cms_get_lang('media_daoyan'),
		cms_get_lang('media_yy'),
		cms_get_lang('media_sysj'),
		cms_get_lang('media_zsc'),
		cms_get_lang('media_fps'),
		cms_get_lang('media_dq'),
		cms_get_lang('contorl_fb_d'),
		cms_get_lang('contorl_fb_z'),
		cms_get_lang('contorl_fb_x'),
		cms_get_lang('media_nrjj'),
		cms_get_lang('media_copyright_date'),
		cms_get_lang('media_tjfs')\n";
		foreach($search_data as $search_item){
//nns_id,nns_name,nns_director,nns_actor,nns_show_time,nns_view_len,nns_all_index,nns_area,
//nns_image0,nns_image1,nns_image2,nns_summary,nns_copyright_date,nns_point
			$row_str=$this->return_string_format($search_item["nns_id"]) .",";
			$row_str.=$this->return_string_format($search_item["nns_name"]) .",";
			$row_str.=$this->return_string_format($search_item["nns_director"]) .",";
			$row_str.=$this->return_string_format($search_item["nns_actor"]) .",";
			$row_str.=$this->return_string_format($search_item["nns_show_time"]) .",";
			$row_str.=$this->return_string_format($search_item["nns_view_len"]) .",";
			$row_str.=$this->return_string_format($search_item["nns_all_index"]) .",";
			$row_str.=$this->return_string_format($search_item["nns_area"]) .",";
			$row_str.=$this->return_string_format($search_item["nns_image0"]) .",";
			$row_str.=$this->return_string_format($search_item["nns_image1"]) .",";
			$row_str.=$this->return_string_format($search_item["nns_image2"]) .",";
			$row_str.=$this->return_string_format($search_item["nns_summary"]) .",";
			$row_str.=$this->return_string_format($search_item["nns_copyright_date"]) .",";
			$row_str.=$this->return_string_format($search_item["nns_point"]);

			$csv_content.=$row_str."\n";
		}
		return $csv_content;
	}

	private function return_string_format($str){
		include_once $this->db_path."nns_string/nns_string_public_function.php";
		$str=convertStrType($str,'TOSBC');
		$return_str=str_replace("\"","\"\"",$str);
		if (strstr($str,",") || strstr($str,"\"")){
			$return_str="\"$str\"";
		}
		return $return_str;
	}

	public function create_csv_content2($search_data,$file_path){
		$fp = fopen($file_path, 'w');
		fwrite($fp, "\xEF\xBB\xBF"); // utf-8 bom

		$line = array(
		cms_get_lang('contorl_zy_ID'),
		cms_get_lang('contorl_dymc'),
		cms_get_lang('media_daoyan'),
		cms_get_lang('media_yy'),
		cms_get_lang('media_sysj'),
		cms_get_lang('media_zsc'),
		cms_get_lang('media_fps'),
		cms_get_lang('media_dq'),
		cms_get_lang('contorl_fb_d'),
		cms_get_lang('contorl_fb_z'),
		cms_get_lang('contorl_fb_x'),
		cms_get_lang('media_nrjj'),
		cms_get_lang('media_copyright_date'),
		cms_get_lang('media_tjfs')
		);
		fputcsv($fp, $line);
		foreach ($search_data as $search_item) {
			$line = array(
			$search_item["nns_id"],
			$search_item["nns_name"],
			$search_item["nns_director"],
			$search_item["nns_actor"],
			$search_item["nns_show_time"],
			$search_item["nns_view_len"],
			$search_item["nns_all_index"],
			$search_item["nns_area"],
			$search_item["nns_image0"],
			$search_item["nns_image1"],
			$search_item["nns_image2"],
			$search_item["nns_summary"],
			$search_item["nns_copyright_date"],
			$search_item["nns_point"]
			);
		    fputcsv($fp,$line);
		}

		fclose($fp);

	}

}
