<?php
/*
 * Created on 2012-3-27
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
  //require_once $nncms_config_path. "nn_cms_view/models/cache_manager.class.php";
 require_once $nncms_config_path. "nn_cms_config/nn_cms_global.php";
	$cache_manager_control=new manager_cache_clear();
	$cache_manager_control->set_nncms_cache_path($nncms_config_path. "data/");
	$cache_manager_control->enabled=$g_cache_enabled;


// include $nncms_config_path. "nn_cms_view/application.class.php";
 class manager_cache_clear{
	 public $cache_inst;
	 public $app_params;
	 public $application_path;
	 public $enabled=1;
	 private $nncms_cache_path;
	 function manager_cache_clear(){
	 	//$this->cache_inst=new cache_manager();
//	 	$this->app_params=new application();
	 }
	 function set_nncms_cache_path($path){
	 	//$this->cache_inst->g_cache_path=$path;
	 }
//	 清除总入口缓存
	 function clear_cache_index_list(){
	 	if ($this->enabled==1){
		 	$path_arr=array("n3_a","index");
		 	//$this->cache_inst->init_cache_params($path_arr);
		 	//$this->cache_inst->clear_cache();
	 	}
	 }
//	 清除指定服务包树缓存
	 function clear_cache_service_category($service_id){
	 	if ($this->enabled==1){
		 	$path_arr=array("n3_a","n3_a_b");
		 	//$this->cache_inst->init_cache_params($path_arr);
		 	//$this->cache_inst->clear_cache($service_id);
	 	}
	 }
	 
//	 清除指定服务包列表缓存
	 function clear_cache_service_list($service_id,$service_category_id){
	 	if ($this->enabled==1){
		 	$path_arr=array("n3_a","n3_a_b",$service_id,$service_category_id);
		 	//$this->cache_inst->init_cache_params($path_arr);
		 	//$this->cache_inst->clear_cache();
	 	}
	 }
//	 清除指定服务包内容缓存
	 function clear_cache_service_item($service_id,$service_item_id){
	 	if ($this->enabled==1){
		 	$path_arr=array("n3_a","n3_a_b",$service_id,$service_item_id);
		 	//$this->cache_inst->init_cache_params($path_arr);
		 	//$this->cache_inst->clear_cache();
	 	}
	 }
//	 清除指定媒资包树缓存
	 function clear_cache_media_assets_category($media_assets_id){
	 	if ($this->enabled==1){
		 	$path_arr=array("n3_a","n3_a_d");
		 	//$this->cache_inst->init_cache_params($path_arr);
		 	//$this->cache_inst->clear_cache($media_assets_id);
	 	}
	 }
	 
//	 清除指定媒资包列表缓存
	 function clear_cache_media_assets_list($media_assets_id,$media_assets_category_id){
	 	if ($this->enabled==1){
		 	$path_arr=array("n3_a","n3_a_d",$media_assets_id,$media_assets_category_id);
		 	//$this->cache_inst->init_cache_params($path_arr);
		 	//$this->cache_inst->clear_cache();
	 	}
	 } 
	 
//	 清除影片信息
	 function clear_cache_video_info($video_id){
	 	if ($this->enabled==1){
		 	$path_arr=array("n3_a","n3_a_a",$video_id);
		 	//$this->cache_inst->init_cache_params($path_arr);
		 	//$this->cache_inst->clear_cache();
	 	}
	 } 
 }
?>