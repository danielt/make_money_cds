<?php
header("Content-Type:text/html;charset=utf-8");
include("../nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_config/nn_cms_global.php";
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_check_login.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
//$action = "";
//if (isset ( $_REQUEST ["action"] )) {
	//if (! empty ( $_REQUEST ["action"] )) {
		//$action = $_REQUEST ["action"];
	//}
//}
//$_GET = json_decode($_REQUEST['search']);
$db_path=dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR."nn_cms_db".DIRECTORY_SEPARATOR;
$logic_path = dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR."nn_logic".DIRECTORY_SEPARATOR;

//判断op,是从何页面跳转的. 以文件名后的几个字符为参数
$op = $_GET['op'];
//播放日志导出 
if ($op=='play_log') {
	//play_log
	include_once $db_path."nns_log".DIRECTORY_SEPARATOR."nns_db_play_log_class.php";
	$log_model = new nns_db_play_log_class();
	if (isset($_GET['begin_time'])) $param['nns_begin_position'] = $_GET['begin_time'];
	if (isset($_GET['end_time'])) $param['nns_end_position'] = $_GET['end_time'];
	if (isset($_GET['nns_video_name'])) $param['nns_video_name']=$_GET['nns_video_name'];
	if (isset($_GET['nns_user_id'])) $param['nns_user_id']=$_GET['nns_user_id'];
	if (isset($_GET['nns_product_id'])) $param['nns_product_id']=$_GET['nns_product_id'];
	if (isset($_GET['nns_qam_ips'])) $param['nns_qam_ips']=$_GET['nns_qam_ips'];
	if (isset($_GET['nns_qam_frequence'])) $param['nns_qam_frequence']=$_GET['nns_qam_frequence'];
	if (isset($_GET['nns_video_id'])) $param['nns_video_id']=$_GET['nns_video_id'];
	if (isset($_GET['nns_nmds_ips'])) $param['nns_nmds_ups']=$_GET['nns_nmds_ips'];
	if (isset($_GET['nns_ua_ips'])) $param['nns_ua_ips']=$_GET['nns_ua_ips'];
	if (isset($_GET['nns_begin_position'])) $param['nns_begin_position']=$_GET['nns_begin_position'];
	if (isset($_GET['nns_end_position'])) $param['nns_end_position']=$_GET['nns_end_position'];
	if (isset($_GET['nns_video_type'])) $param['nns_video_type']=$_GET['nns_video_type'];
	if (isset($_GET['nns_device_id'])) $param['nns_device_id']=$_GET['nns_device_id'];
	if (isset($_GET['nns_qam_area_code'])) $param['nns_qam_area_code']=$_GET['nns_qam_area_code'];
	if (isset($_GET['page_size'])) $param['page_size']=$_GET['page_size'];
	$count = $log_model->nns_db_play_log_count($param);
	$data_list2 = $log_model->nns_db_play_log_time_count($param);
	$data_list1 = $log_model->nns_db_play_log_list_infinite($param);
	$export_excel = '';
	//ob_start();
	$export_excel = $log_model->nns_db_log_export_excel($data_list1,$data_list2,$count);
}
//操作日志导出 
if ($op=='opera_log') {
	include_once $db_path."nns_log/nns_db_op_log_class.php";
	//组合参数
	$op_admin_id = $_GET['op_admin_id'];//管理员账号
	$org_type	 = $_GET['org_type'];
	$org_id		 = $_GET['org_id'];//查询的部门
	$op_type	 = $_GET['op_type'];    //查询操作类型
	$begin_time	 = $_GET['begin_time'];//开始时间
	$end_time	 = $_GET['end_time'];     //结束时间
	$nns_op_desc = $_GET['nns_op_desc'];    //查询操作详情
	//查询符合条件的记录条数
	$log_model = new nns_db_op_log_class();
	$ret=$log_model->nns_db_op_log_count($op_admin_id, $org_type, $org_id, $op_type, $begin_time, $end_time,$nns_op_desc);
	//取出记录总数
	$count = $ret[data][0][num];
	//取出list所有条目
	$data_list=$log_model->nns_db_op_log_list($op_admin_id,$org_type,$org_id,$op_type,$begin_time,$end_time,$nns_op_desc);
	$data_list1 = $data_list['data'];
	$export_excel = '';
	$param['begin_time'] = $begin_time;
	$param['end_time'] = $end_time;
	$export_excel = $log_model->nns_db_op_log_export_excel($data_list1,$count,$param);
}
//媒资注入导出
if ($op=='asset_import') {
	include_once $db_path."nns_asset_import/nns_db_asset_import_class.php";
	$log_model = new nns_db_asset_import_class();
	$begin_time = $_GET['begin_time'];
	$end_time = $_GET['end_time'];
	$nns_asset_name = $_GET['nns_asset_name'];
	$nns_import_datetime = $_GET['nns_import_datetime'];
	$nns_import_state = $_GET['nns_import_state'];
	$nns_asset_id = $_GET['nns_asset_id'];
	$ret=$log_model->nns_db_play_media_count($nns_import_datetime, $nns_import_state,$begin_time, $end_time, $nns_asset_name);
	$count = $ret[data][0][num];
	$data_list=$log_model->nns_db_asset_import_list($nns_import_state, $nns_asset_id,$begin_time, $end_time,$nns_asset_name,0,$count);
	$data_list1 = $data_list['data'];
	$export_excel = '';
	$param['begin_time'] = $begin_time;
	$param['end_time'] = $end_time;
	$export_excel = $log_model->nns_db_asset_import_log_export_excel($data_list1,$count,$param);

}
//播放日志用户统计导出
if ($op=='play_log_count_user') {
	//play_log
	include_once $db_path."nns_log".DIRECTORY_SEPARATOR."nns_db_play_log_class.php";
	$log_model = new nns_db_play_log_class();
	$data_list1 = $log_model->nns_db_play_log_user_list($_GET,0,12,1);
	$count = $log_model->nns_db_play_log_user_count($_GET);
	$export_excel = '';
	$export_excel = $log_model->nns_db_log_user_export_excel($data_list1,$count,$_GET);
}
//模板日志导出
if ($op=='view_record') {
	//view_record
	include_once $db_path. "nns_log".DIRECTORY_SEPARATOR."nns_db_view_record_class.php";
	include_once $db_path. "nns_template".DIRECTORY_SEPARATOR."nns_db_template_class.php";
	include_once $db_path. "nns_vod".DIRECTORY_SEPARATOR."nns_db_vod_class.php";

	$template_class_ins = nns_db_template_class::get_instance();
	$view_model = new nns_db_view_record_class();

	if (isset($_GET['page_size'])) $param['page_size'] = $_GET['page_size'];
	if (isset($_GET['tpl_name'])) $param['tpl_name'] = $_GET['tpl_name'];
	if (isset($_GET['tpl_type_id'])) $param['tpl_type_id'] = $_GET['tpl_type_id'];
	if (isset($_GET['tpl_id'])) $param['tpl_id'] = $_GET['tpl_id'];
	if (isset($_GET['device_id'])) $param['device_id'] = $_GET['device_id'];
	if (isset($_GET['user_id'])) $param['user_id'] = $_GET['user_id'];
	if (isset($_GET['begin_time'])) $param['begin_time'] = $_GET['begin_time'];
	if (isset($_GET['end_time'])) $param['end_time'] = $_GET['end_time'];
	//view_record_list 最后一个参数是开关.开关分页,为空就启用分页
	$data_list1 = $view_model->view_record_list($_GET,0,12,1);
	//影片名
	$media_ids = array();
	foreach ($data_list1 as $item) {
		if (!empty($item['nns_content_id'])) {
			$media_ids[] = $item['nns_content_id'];
		}
	}
	$vod_media_ins = new nns_db_vod_class();
	$vod_media_arr = $vod_media_ins->get_vod_by_ids($media_ids);
	//页面名称
	$tpl_type_arr = getDirFiles($nncms_config_path."nn_cms_view/template");
	$tpl_cn_name_arr = array();
	foreach ($tpl_type_arr as $tpl_type) {
		if (is_dir($nncms_config_path."data/template/".$tpl_type)) {
			$tpl_id_arr = getDirFiles($nncms_config_path. "data/template/". $tpl_type );
			//循环模板ID
			foreach ($tpl_id_arr as $tpl_id) {
				if(is_dir($nncms_config_path. "data/template/".$tpl_type.'/'.$tpl_id)){
					//循环模板页面
					$template_config = $template_class_ins->load_config($tpl_type,$tpl_id,TRUE);
					$tpl_file_arr = getDirFiles($nncms_config_path."data/template/".$tpl_type."/".$tpl_id);

					//读取模板中文名
					foreach($tpl_file_arr as $tpl_file){
						if(is_file($nncms_config_path. "data/template/". $tpl_type.'/'.$tpl_id.'/'.$tpl_file)){
							$ROW_1++;
							if(false !==($pos=strpos($tpl_file,"."))){
								$tpl_file_basename =  substr($tpl_file,0,$pos);
								$ext = substr($tpl_file,$pos+1);
								if($ext=='html'){

									$template_cn_name = substr($tpl_file,0,$pos);//$tpl_file;
									$template_name  = $tpl_file_basename;//substr($tpl_file,0,$pos);
									if(!empty($template_config['skin_config'][$template_name])){
										$template_cn_name = $template_config['skin_config'][$template_name];
									}
									//读取模板中文名
									$tpl_cn_name_arr[$tpl_type][$tpl_id][$template_name] =$template_cn_name;
								}
							}
						}
					}
				}
			}
		}
	}
	//栏目ID
	//影片名称
	$count = $view_model->view_record_count($_GET);
	$export_excel = '';
	$export_excel = $view_model->view_record_export_excel($data_list1,$count,$vod_media_arr,$tpl_cn_name_arr);
}
//广告历史日志导出
if ($op=='ad_log') {
	//ad_log
	include_once $db_path. "nns_ad".DIRECTORY_SEPARATOR."nns_ad_log_class.php";
	$log_model  = new nns_ad_log_class();
	$param = '';
	$param['ad_position_id'] = $_GET['ad_position_id'];
	$param['ad_id']		= $_GET['ad_id'];
	$param['event']		= $_GET['event'];
	$param['user_id']	= $_GET['user_id'];
	$param['guest_ip'] = $_GET['guest_ip'];
	$param['user_agent'] = $_GET['guest_ip'];
	$param['begin_time'] = $_GET['begin_time'];
	$param['end_time'] = $_GET['end_time'];
	$count = $log_model->nns_ad_log_count($param);
	$data_list1 = $log_model->nns_ad_log_list($param,0,$count);
	$export_excel = '';
	$export_excel = $log_model->nns_ad_log_export_excel($data_list1,$count,$_GET);
}

//全局错误日志导出 xingcheng.hu
if($op='global_error_log')
{
	include_once $logic_path.DIRECTORY_SEPARATOR."nl_common.func.php";
	include_once $logic_path."global_error_log".DIRECTORY_SEPARATOR."global_error_log.class.php";
	$global_error_log_model  = new nl_global_error_log();
	$dc = nl_get_dc(array(
		"db_policy" => NL_DB_READ,
		"cache_policy" => NP_KV_CACHE_TYPE_NULL
	));
	$params= array();
	if (isset($_REQUEST['nns_name']) && strlen($_REQUEST['nns_name']) > 0)
	{
		$params['like']['nns_name'] = $_REQUEST['nns_name'];
	}
	if (isset($_REQUEST['nns_desc']) && strlen($_REQUEST['nns_desc']) > 0)
	{
		$params['like']['nns_desc'] = $_REQUEST['nns_desc'];
	}
	if (isset($_REQUEST['nns_model']) && strlen($_REQUEST['nns_model']) > 0)
	{
		$params['where']['nns_model'] = $_REQUEST['nns_model'];
	}
	if (isset($_REQUEST['nns_type']) && strlen($_REQUEST['nns_type']) > 0)
	{
		$params['where']['nns_type'] = $_REQUEST['nns_type'];
	}
	if (isset($_REQUEST['nns_cp_id']) && strlen($_REQUEST['nns_cp_id']) > 0)
	{
		$params['where']['nns_cp_id'] = $_REQUEST['nns_cp_id'];
	}
	if (isset($_REQUEST['nns_sp_id']) && strlen($_REQUEST['nns_sp_id']) > 0)
	{
		$params['where']['nns_sp_id'] = $_REQUEST['nns_sp_id'];
	}
	if (isset($_REQUEST['nns_action']) && strlen($_REQUEST['nns_action']) > 0)
	{
		$params['where']['nns_action'] = $_REQUEST['nns_action'];
	}
	if (isset($_REQUEST['nns_message_id']) && strlen($_REQUEST['nns_message_id']) > 0)
	{
		$params['where']['nns_message_id'] = $_REQUEST['nns_message_id'];
	}
	if (isset($_REQUEST['nns_video_import_id']) && strlen($_REQUEST['nns_video_import_id']) > 0)
	{
		$params['where']['nns_video_import_id'] = $_REQUEST['nns_video_import_id'];
	}
	if (isset($_REQUEST['nns_index_import_id']) && strlen($_REQUEST['nns_index_import_id']) > 0)
	{
		$params['where']['nns_index_import_id'] = $_REQUEST['nns_index_import_id'];
	}
	if (isset($_REQUEST['nns_media_import_id']) && strlen($_REQUEST['nns_media_import_id']) > 0)
	{
		$params['where']['nns_media_import_id'] = $_REQUEST['nns_media_import_id'];
	}
	if (isset($_REQUEST['create_begin_time']) && strlen($_REQUEST['create_begin_time']) > 0)
	{
		$params['where']['create_begin_time'] = $_REQUEST['create_begin_time'];
	}
	if (isset($_REQUEST['create_end_time']) && strlen($_REQUEST['create_end_time']) > 0)
	{
		$params['where']['create_end_time'] = $_REQUEST['create_end_time'];
	}
	array_pop($_REQUEST);
	$result=  $global_error_log_model->query($dc,$params);
	$count = $result['page_info'];
	$count['page_count'] = $count['total_count'];
	$result = $global_error_log_model->query($dc,$params,$count);
	$global_error_log_model->global_error_log_export_excel($result,$count['page_count'],$_REQUEST);
}

function getDirFiles($dir) {
	$files = array();
	if ($handle = opendir($dir)){
		/* Because the return type could be false or other equivalent type(like 0),
		this is the correct way to loop over the directory. */
		while (false !== ($file = readdir($handle))) {
			if ($file!="." && $file!=".." && $file!=".svn"){
				$files[]=$file;
			}
		}
	}
	closedir($handle);

	return $files;
}

//ob_end_clean();
//file_put_content("excel_url",$export_excel);
//return 1;
?>
