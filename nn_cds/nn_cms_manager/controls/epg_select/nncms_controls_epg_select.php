<?php
header("Content-Type:text/html; charset=UTF-8");

include("../../nncms_manager_inc.php");
include_once LOGIC_DIR . "common/global.class.php";
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
include ($nncms_config_path . "nn_cms_config/nn_cms_config_path.php");
//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";
include_once $nncms_db_path . "nns_common/nns_db_constant.php";

$checkpri = new nns_db_pri_class();
if (!$checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "100100")) {
    Header("Location: ../nncms_content_wrong.php");
}
//导入语言包
if (isset($_SESSION["language_dir"])) {
    $language_dir = $_SESSION["language_dir"];
}
$checkpri = null;

include $nncms_config_path . "nn_cms_manager/languages/language_contant.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/trim.js"></script>
<script language="javascript" src="../../js/radiolist.js"></script>
<script>
	function set_tags_edit(){
		window.parent.get_tags_edit(getCheckedData(false));
	}
	
	function clear_tags(){
		$('.radioitem input').attr("checked",false);
	}
	$(document).ready(function(){
		$(".closebtn").click(function(){
		window.parent.close_select();
	});
	})
	
</script>
</head>

<body style='width:400px;'>
<div class="selectcontrol" style="background:#fff">
	<div class="closebtn"><img src="../../images/topicon_08-topicon.png" /></div>
	<div style="padding:5px;">
	 <div class="content_table formtable">
		  <div class="radiolist select">

                            <div class="radiogroup">
                      														<div class="radioitem" >
                                                                                <?php echo get_tag_input_box(NNS_PLAYER_STD_TAG, ''); ?>
                                                                            </div>
                                                                            <div class="radioitem" >
                                                                                <?php echo get_tag_input_box(NNS_PC_TAG,''); ?>
                                                                            </div>
                                                                            <div class="radioitem" >
                                                                                <?php echo get_tag_input_box(NNS_ANDROID_PAD_TAG, ''); ?>
                                                                            </div>
                                                                            <div class="radioitem" >
                                                                                <?php echo get_tag_input_box(NNS_ANDROID_PHONE_TAG, ''); ?>
                                                                            </div>
                                                                            <div class="radioitem" >
                                                                                <?php echo get_tag_input_box(NNS_APPLE_PAD_TAG,''); ?>
                                                                            </div>
                                                                            <div class="radioitem" >
                                                                                <?php echo get_tag_input_box(NNS_APPLE_PHONE_TAG, ''); ?>
                                                                            </div>


                                                                            <?php for ($device_num = 0; $device_num < $g_manager_list_device_num; $device_num++) { ?>
                                                                                <div class="radioitem" >
                                                                                    <?php echo get_tag_input_box($device_num, $item["nns_tag"] . ','); ?>

                                                                                </div>
                                                                            <?php } ?>
                                                                            <div style="clear:both;"></div>
                                                                        </div>
                                                                        </div>
                                                                   
	</div>
<div class="btns" style="padding:5px;"><input name="sure_btn" type="button" value="<?php echo cms_get_lang('confirm');?>" onclick="set_tags_edit();"/>&nbsp;&nbsp;&nbsp;&nbsp;<input name="empty_btn" type="button" value="<?php echo cms_get_lang('empty');?>" onclick="clear_tags();"/></div>
	</div>
</div>
</div>
</body>
</html>