<?php
header("Content-Type:text/html;charset=utf-8");

include("../nncms_manager_inc.php");
//获取权限检查类
session_start();

// 导入多语言包
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

include_once $nncms_config_path .'/nn_logic/nl_common.func.php';
include $nncms_config_path.'nn_logic/buss/buss.class.php';

$dc = nl_get_dc(array(
		'db_policy'=>NL_DB_READ,
		'cache_policy'=>NP_KV_CACHE_TYPE_NULL
		)
	);
$dc->open(); 




$page_size = isset($_COOKIE['page_max_num'])?$_COOKIE['page_max_num']:1;
//$page_size = NN_G_PAGE_SIZE;
$cur_page = isset($_GET['page'])?(int)$_GET['page']:1;

//$hot_word_lib_model = new nl_hot_word_lib($this->dc);
$filter = array();



$method = isset($_GET["method"])?$_GET["method"]:'';
$search = isset($_GET["search"])?$_GET["search"]:'';
//$search = iconv('gbk','UTF-8',$search);
//echo $search;
$url = '?method='.$method;
if($search){
	$url.= '&search='. $search;
	$filter['nns_name'] = $search;
}


$filter['start'] = ($cur_page-1)*$page_size;
$filter['size'] = $page_size;
$result = nl_buss::get_buss_list($dc,$filter);
if($result['ret'] !== 0){
	//$this->show_message('db error');
}


$pager = new nl_pager($result['data']['rows'],$page_size,$cur_page,$url,1);
    	
$data  = $result['data']['result'];



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/cms_cookie.js"></script>
<script language="javascript" src="../js/table.js.php"></script>
<script language="javascript" src="../js/trim.js"></script>
<script language="javascript">
//_selectfunc=select_table_tr;

$(document).ready(function(){
	$(".closebtn").click(function(){
		window.parent.close_select();
	});
})
	function select_table_tr($obj){
		window.parent.close_select();

	    window.parent.set_<?php echo $method;?>_id($obj.find("input:eq(0)").attr("value"),$obj.find("td:eq(1)").html());

	}
function select_buss_ids(){
	var id_str = '';
	$('.formtable input:checkbox:checked').each(function(){
		id_str += $(this).val()+',';
	});
	if(id_str == ''){
		alert('<?php echo cms_get_lang('msg_4');?>');return;
	}
	window.parent.select_buss_ids(id_str);
}
function search_result(){
	var search_str=encodeURI($("#search_input_param").attr("value").trim());
	window.location.href="?method=<?php echo $method;?>&search="+search_str;
}
</script>
</head>

<body  style='width:400px;'>
<div class="selectcontrol">
	<div class="closebtn"><img src="../images/topicon_08-topicon.png" /></div>
	<div class="selecttitle"><?php echo cms_get_lang('search_xz'). cms_get_lang('buss');?></div>
	<div class="searchbox"><input name="" type="text" id="search_input_param" value="<?php echo $search;?>"/>&nbsp;&nbsp;<input name="" type="button" value="<?php echo cms_get_lang('search');?>" onclick="search_result();"/>
	
	<input type="button" value="<?php echo cms_get_lang('confirm_search_xz');?>" onclick="select_buss_ids();"/>
	</div>
	<div style="padding:5px;">
	 <div class="content_table formtable">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
			    <th><input name="" type="checkbox" value="" /></th>
				<th><?php echo cms_get_lang('segnumber');?></th>

                <th><?php echo cms_get_lang('buss');?></th>
				<th><?php echo cms_get_lang('name');?></th>


			</tr>
		</thead>
		<tbody>
<?php
			if (!empty($data)){
				$num = ($pager->cur_page-1)*$pager->page_size;
				 foreach($data as $item){
					$num++;
?>
			  <tr class="selecttr">
			    <td><input name="input" type="checkbox" value="<?php echo  $item["nns_id"];?>" /></td>
				<td><?php echo $num;?></td>	
				<td><?php echo $item["nns_id"];?></td>
				<td><?php echo $item["nns_name"];?></td>

			  </tr>
		  <?php }}?>
		  </tbody>
		</table>
	</div>
<?php echo $pager->nav();?>
	<!--<div class="btns"><input name="" type="button" value="确定"/>&nbsp;&nbsp;&nbsp;&nbsp;<input name="" type="button" value="关闭"/></div>
	</div>-->
</div>
</body>
</html>