<?php
/*
 * Created on 2012-7-11
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
session_start();
ob_start();
include("../nncms_manager_inc.php");
require_once  $nncms_config_path. "nn_cms_config/nn_cms_global.php";
require_once  $nncms_db_path. "nns_common/nns_db_curl_content_class.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
error_reporting(E_ALL ^ E_NOTICE);
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/language_contant.php";

$media_id=$_GET["media_id"];
$media_type=$_GET["media_type"];
$http_url = $g_core_url;
//通过CURL抓取返回值
$curl_post = 'func=get_content' . "&id=" . $media_id . "&live=" . $media_type ;
$data = nns_db_curl_content_class::nns_db_curl_content ( $http_url, $curl_post );
ob_end_clean();
//echo $data;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/table.js.php"></script>
<script language="javascript">

$(document).ready(function(){
	$(".closebtn").click(function(){
		window.parent.close_select();
	});
})

	</script>
</head>

<body  style='width:500px;'>
<div class="selectcontrol">
	<div class="closebtn"><img src="../images/topicon_08-topicon.png" /></div>
	<div class="selecttitle"><?php echo cms_get_lang('contorl_ffjd');?></div>
	<div class="searchbox"><?php echo cms_get_lang('contorl_py_ID');?>： <?php echo $media_id;?></div>
	<div style="padding:5px;">
	 <div class="content_table formtable selecttable">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<th><?php echo cms_get_lang('contorl_fw');?></th>
			   <th><?php echo cms_get_lang('contorl_ffms');?></th>
			   <th><?php echo cms_get_lang('contorl_ffzt');?></th>
			   <th><?php echo cms_get_lang('type');?></th>
			</tr>
		</thead>
		<tbody>
<?php
if ($data){
	$xml = simplexml_load_string ( $data );
	$ncds=$xml->content->ncds_list->ncds;
	if (count($ncds)>0){
		foreach($ncds as $nmds){
?>
			  <tr>
				<td><?php echo $nmds["id"].'/'.$nmds["name"].'/'.$nmds["friend_name"];?></td>
			   <td><?php echo get_cdn_mode($nmds["mode"]);?></td>
			   <td><?php echo get_cdn_state($nmds["state"]);?></td>
			   <td><?php echo get_cdn_type($nmds["type"]);?></td>
			</tr>
<?php }}
}?>
		  </tbody>
		</table>
	</div>

	<!--<div class="btns"><input name="" type="button" value="确定"/>&nbsp;&nbsp;&nbsp;&nbsp;<input name="" type="button" value="关闭"/></div>
	</div>-->
</div>
</body>
</html>