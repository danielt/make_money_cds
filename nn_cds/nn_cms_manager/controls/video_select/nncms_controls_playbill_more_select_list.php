<?php
header("Content-Type:text/html;charset=utf-8");
 error_reporting(E_ALL ^ E_NOTICE);
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 ob_start();
 include("../../nncms_manager_inc.php");
//获取权限检查类
session_start();
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

 $method=$_GET["method"];
 $search=$_GET["search"];
 $depot_id=$_GET["depot_id"];
 $depot_detail_id=$_GET["depot_detail_id"];

 $except_method=$_GET["except_method"];
 $except_detail_id=$_GET["except_detail_id"];
 $except_id=$_GET["except_id"];
 $live_day=$_GET['day'];
$live_id=$_GET['live_id'];
 $nns_url="&search=".urlencode($search)."&depot_id=".$depot_id."&depot_detail_id=".$depot_detail_id.
"&except_method=".$except_method."&except_detail_id=".$except_detail_id."&except_id=".$except_id;

  $currentpage=1;
  if (!empty($_GET["page"])) $currentpage=$_GET["page"];
  $listnum=12;
 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
  if (!empty($_COOKIE["page_max_num"])){
		$g_manager_list_max_num=$_COOKIE["page_max_num"];
	}
 include_once $nncms_db_path. "nns_common/nns_db_pager_class.php";	
 include_once LOGIC_DIR.'playbill'.DIRECTORY_SEPARATOR.'playbill_item.class.php';
 $dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
			));
$dc->open();
if ($except_method=='assist') $except_method='asset';
 	
 $count=nl_playbill_item::count_search_playbill_item_list(
 $dc,
 $live_id,
 array(
 	'begin_time'=> $live_day.' 00:00:00',
 	'end_time'=> date('Y-m-d H:i:s',strtotime($live_day.' 00:00:00')+3600*24),
 	'name'=>$search,
 	'pinyin'=>$search,
 	'state'=>0,
 	'except'=>array(
 		'packet_type'=>$except_method,
		'packet_id'=>$except_detail_id,
		'category_id'=>$except_id
 	)
 ),
 'pinyin_firstchar|name_likechar'
);

 $playbill_list=nl_playbill_item::search_playbill_item_list(
 $dc,
 $live_id,
 ($currentpage-1)*$listnum,
 $listnum,
 array(
 	'begin_time'=> $live_day.' 00:00:00',
 	'end_time'=> date('Y-m-d H:i:s',strtotime($live_day.' 00:00:00')+3600*24),
 	'name'=>$search,
 	'pinyin'=>$search,
 	'state'=>0,
 	'except'=>array(
 		'packet_type'=>$except_method,
		'packet_id'=>$except_detail_id,
		'category_id'=>$except_id
 	)
 ),
  'pinyin_firstchar|name_likechar'
);

$url="?";
 $url.="&search=".urlencode($search)."&depot_id=".$depot_id."&depot_detail_id=".$depot_detail_id
 ."&except_method=".$except_method."&except_detail_id=".$except_detail_id."&except_id=".$except_id
 ."&day=".$live_day."&live_id=".$live_id;
 $pager = new nns_db_pager_class($count,$listnum,$currentpage,$url,false,false);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/trim.js"></script>
<script language="javascript">
function get_more_select(){
	return getAllCheckBoxSelect();
}

</script>
</head>

<body>
<div class="content">
    <div style="padding:5px;">
     <div class="content_table formtable">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
        	<tr>
        	<th><input name="" type="checkbox" value="" /></th>
        
                <th><?php echo cms_get_lang('segnumber');?></th>
                <th><?php echo cms_get_lang('name');?></th>

                <th><?php echo cms_get_lang('media_bfsj');?></th>
                <th><?php echo cms_get_lang('contorl_bfsc_s');?></th>
                


            </tr>
        </thead>
        <tbody>
         <?php
        	if (is_array($playbill_list)){
        	 $num=($currentpage-1)*$listnum; foreach($playbill_list as $item){
        	$num++;
        	?>
              <tr  class="selecttr">
               	<td><input name="input" type="checkbox" value="<?php echo  $item["nns_id"];?>" /></td>
                <td><?php echo $num;?></td>
                <td><?php echo $item["nns_name"];?></td>


                <td><?php echo $item["nns_begin_time"];?> </td>
                <td><?php echo $item["nns_time_len"];?> </td>


              </tr>
          <?php }} ?>
          <script language="javascript">
          		create_empty_tr($(".formtable"),<?php echo count($playbill_list);?>,<?php echo $listnum;?>);
          	</script>
          </tbody>
        </table>
    </div>
     <?php echo $pager->nav();?>
     </div>
</div>
</body>
</html>