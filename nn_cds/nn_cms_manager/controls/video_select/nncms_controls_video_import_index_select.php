<?php
header("Content-Type:text/html;charset=utf-8");
error_reporting(E_ALL ^ E_NOTICE);
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
//获取权限检查类
session_start();
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language_media.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
$method = $_GET["method"];
$search = $_GET["search"];
$nns_org_id = $_GET["org_id"];
$nns_org_type = $_GET["org_type"];
$nns_more_select = $_GET["more_select"];
$video_id = $_GET["video_id"];
$except_method = $_GET["except_method"];
$except_detail_id = $_GET["except_detail_id"];
$except_id = $_GET["except_id"];
$import_video_id = $_GET["import_video_id"];
$selectmethod = $_GET["selectmethod"] == '' ? 'import' : 'select';
$except_url = "";

$nns_org_type = empty($nns_org_type) ? '0' : $nns_org_type;
if ($except_method == "assist" || $except_method == "service") {
        $except_url = "&except_method=$except_method&except_detail_id=$except_detail_id&except_id=$except_id";
}

$nns_url = "&org_id=" . $nns_org_id . "&org_type=" . $nns_org_type;
$currentpage = 1;
if (!empty($_GET["page"]))
        $currentpage = $_GET["page"];
$listnum = 8;
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
$method_str = $language_media_mt;
if ($method == "vod") {
        $method_param = 0;
} else if ($method == "live") {
        $method_param = 1;
}
include($nncms_db_path . "nns_depot/nns_db_depot_class.php");
$depot_inst = new nns_db_depot_class();
$depot_array = $depot_inst->nns_db_depot_list("", $nns_org_type, $nns_org_id, $method_param);
// var_dump($partner_array);
if ($depot_array["ret"] != 0) {
        $data = null;
//	 	echo "<script>alert('". $depot_array["reason"]."');</script>";
}
$depot_inst = null;
$data = $depot_array["data"];

include $nncms_db_path . "nns_carrier/nns_db_carrier_class.php";
include $nncms_db_path . "nns_partner/nns_db_partner_class.php";
include $nncms_db_path . "nns_group/nns_db_group_class.php";


if ($_SESSION["nns_manager_type"] == 0) {
        $carrier_inst = new nns_db_carrier_class();
        $carrier_result = $carrier_inst->nns_db_carrier_list();

        if ($carrier_result["ret"] == 0) {
                $carrier_data = $carrier_result["data"][0];
                $base_name = $carrier_data["nns_name"];
        }
}

//		 	类型为合作伙伴
if ($nns_org_type == 1) {
        $partner_inst = new nns_db_partner_class();
        $partner_result = $partner_inst->nns_db_partner_info($nns_org_id);

        if ($partner_result["ret"] == 0) {
                $partner_data = $partner_result["data"][0];
                $base_name = $partner_data["nns_name"];
        }
        //	类型为集团
} else if ($nns_org_type == 2) {
        $group_inst = new nns_db_group_class();
        $group_result = $group_inst->nns_db_group_info($nns_org_id);

        if ($group_result["ret"] == 0) {
                $group_data = $group_result["data"][0];
                $base_name = $group_data["nns_name"];
        }
}


$base_id = 1000;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                <link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
                <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../../js/dtree.js"></script>
                <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../../js/table.js.php"></script>
                <script language="javascript" src="../../js/trim.js"></script>
                <script language="javascript" src="../../js/cms_ajax.js"></script>
                <script language="javascript" src="../../js/alertbox.js"></script>
                <script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
                <script language="javascript">
                        var now_video_index;
                        var now_video_id;
                        var now_video_type;
                        var now_video_name;

                        function set_video_id(id, type, name, star, index) {
                                //	window.parent.set_video_id(id,type,name,star);
                                //	window.parent.close_select();
                                //	refresh_video_box();
                         
                                now_video_id = id;
                                now_video_type = type;
                                now_video_name = name;
                                $("#nns_video_index").html("");
                                var option_html = "";
                                for (var j = 0; j < index; j++) {
                                        option_html += "<option value='" + j + "' >" + (j + 1) + "</option>";
                                }
                                //$("#nns_video_index")[0].size=8;
                                $("#nns_video_index").html(option_html);
                        }

                        function import_media() {
                                var now_index = $("#nns_video_index").val();



                                if (now_video_id == undefined || now_video_id == "") {
                                        alert("<?php echo cms_get_lang('msg_4'); ?>");
                                } else {
<?php if (isset($_GET['locktype'])) { ?>

                                                window.parent.parent.setVideo(now_video_id, now_video_type, now_video_name, now_index);
                                                return false;
<?php } ?>
<?php if ($selectmethod == 'import') { ?>
                                                getAjax("../nncms_controls_ajax_import_video_index.php?method=check&accept_video_id=" + now_video_id + "&import_video_id=<?php echo $import_video_id; ?>&accept_index=" + now_index, get_response_check);
<?php } elseif ($selectmethod == 'select') { ?>
                                                window.parent.set_video_id(now_video_id, now_video_type, now_video_name, now_index);
<?php } ?>
                                }
                        }
                        function get_response_check(response) {
                                //	alert(response["import"]+"|"+response["index"]+"|"+response["media"]);
                                var alert_str = "";
                                if (response["import"] == false) {
                                        alert_str += "导入的影片无媒体文件!\n";
                                        alert(alert_str);
                                        return;
                                }
                                if (response["index"] == false) {
                                        alert_str += "<?php echo cms_get_lang('contorl_fgts') ?>\n";

                                }
                                if (response["media"] == false) {
                                        alert_str += "<?php echo cms_get_lang('contorl_fgts') ?>\n";
                                }
                                if (alert_str == "") {
                                        set_import_media();
                                } else {
                                        if (confirm(alert_str)) {
                                                set_import_media();
                                        }
                                        ;
                                }
                                //alert(alert_str);
                        }

                        function set_import_media() {
                                var now_index = $("#nns_video_index").val();
                                getAjax("../nncms_controls_ajax_import_video_index.php?method=import&accept_video_id=" + now_video_id + "&import_video_id=<?php echo $import_video_id; ?>&accept_index=" + now_index, get_response_import);
                        }

                        function get_response_import(response) {
                                //alert(response["import"]+"|"+response["index"]+"|"+response["media"]);
                                var alert_str = "";
                                if (response["import"] == false) {
                                        alert_str += "<?php echo cms_get_lang('contorl_wmtwjts') ?>\n";
                                }
                                if (response["index"] == false) {
                                        alert_str += "<?php echo cms_get_lang('contorl_fgts') ?>\n";

                                }
                                if (response["media"] == false) {
                                        alert_str += "<?php echo cms_get_lang('contorl_fgts') ?>\n";
                                }
                                if (alert_str == "")
                                        alert_str = "<?php echo cms_get_lang('contorl_ydcgts') ?>";
                                window.parent.close_select();
                                alert(alert_str);
                        }
                        function set_more_video_id() {
                                var ids = $(".video_frame iframe")[0].contentWindow.get_more_select();
                                window.parent.set_video_id(ids,<?php echo $method_param; ?>);
                                //	window.parent.close_select();
                                refresh_video_box();
                        }

                        var now_select_id;
                        $(document).ready(function() {
                                $(".closebtn").click(function() {
                                        window.parent.close_select();
                                });
                                $(".category_editbox").width($(".content").width() - 250);
                                $(".service_frame iframe").width($(".content").width() - 250);
                                $(window).resize(function() {
                                        $(".category_editbox").width($(".content").width() - 250);
                                        $(".service_frame iframe").width($(".content").width() - 250);
                                });


                                $(".selectbox").hide();

                                $("#nns_carrier").change(function() {
                                        var $obj = $(this).find("option:selected");
                                        var index = $("#nns_carrier option").index($obj);
                                        selectCarrier(index);
                                });
                                $("#nns_method").change(function() {
                                        var $obj = $(this).find("option:selected");
                                        var index = $("#nns_method option").index($obj);
                                        selectMethod(index);
                                })
                        });

                        function selectMethod(index) {
                                switch (index) {
                                        case 0:
                                                index_str = "vod";
                                                break;
                                        case 1:
                                                index_str = "live";
                                                break;
                                }
                                window.location.href = "nncms_controls_video_import_index_select.php?selectmethod=<?php echo $selectmethod; ?>&org_id=<?php echo $nns_org_id; ?>&org_type=<?php echo $nns_org_type; ?>&method=" + index_str + "&more_select=<?php echo $nns_more_select; ?><?php echo $except_url; ?>";
                        }


                        function checkhiddenInput() {
                                var par = getAllCheckBoxSelect();
                                if (par == "") {
                                        alert("<?php echo cms_get_lang('lmgl_msg_noselect'); ?>");
                                } else {
                                        $('#nns_id').attr('value', getAllCheckBoxSelect());
                                        checkForm('<?php echo cms_get_lang('titile'); ?>', '<?php echo cms_get_lang('msg_ask_delete'); ?>', $('#delete_form'));
                                }
                        }

                        function select_tree_item(item_id, item_name, parent_id) {


                                $(".category_editbox").find(".category_edit_id").html(item_id);
                                $(".category_editbox").find(".category_edit_name").html(item_name);
                                $(".video_frame").show();
                                now_select_id = item_id;
                                refresh_video_box();
                        }
                        function refresh_video_box() {
<?php if ($nns_more_select == true) { ?>
                                        $(".video_frame iframe").attr("src", "nncms_controls_video_more_select_list.php?method=" + $("#nns_method").val() + "&depot_detail_id=" + now_select_id + "&depot_id=<?php echo $data[0]["nns_id"]; ?><?php echo $except_url; ?>&search=" + encodeURI($("#nns_video_search").val()));
<?php } else { ?>
                                        $(".video_frame iframe").attr("src", "nncms_controls_video_select_list.php?method=" + $("#nns_method").val() + "&depot_detail_id=" + now_select_id + "&depot_id=<?php echo $data[0]["nns_id"]; ?><?php echo $except_url; ?>&search=" + encodeURI($("#nns_video_search").val()));
<?php } ?>
                        }


                        function checkheight() {
                                $(".video_frame iframe").height($(".video_frame iframe").contents().find(".content").height());
                                $(".category_tree").height($(".category_editbox").height() - 20);
                                if (window.parent.resetFrameHeight)
                                        window.parent.resetFrameHeight();

                        }

                        function selectCarrier(num) {

                                switch (num) {
                                        case 1:
                                                begin_select_partner();
                                                break;
                                        case 2:
                                                begin_select_group();
                                                break;
                                        case 0:
                                                window.location.href = "nncms_controls_video_select.php?org_id=<?php echo $carrier_data["nns_id"]; ?>&org_type=0&method=<?php echo $method; ?>" + "<?php echo $except_url; ?>&more_select=<?php echo $nns_more_select; ?>";
                                                break;
                                }
                        }

                        function begin_select_partner() {
                                $("#select_frame").attr("src", "../nncms_controls_select.php?method=partner");
                                $(".selectbox").show();
                        }

                        function begin_select_group() {
                                $("#select_frame").attr("src", "../nncms_controls_select.php?method=group");
                                $(".selectbox").show();
                        }
                        function close_select() {
                                $(".selectbox").hide();
                        }

                        function set_partner_id(value, name) {
                                window.location.href = "video_select/nncms_controls_video_select.php?org_id=" + value + "&org_type=1&method=<?php echo $method; ?>" + "&more_select=<?php echo $nns_more_select; ?><?php echo $except_url; ?>";
                        }

                        //	function set_video_id(value,type,name){
                        //
                        //		window.parent.set_video_id(value,type,name);
                        //	}

                        function set_group_id(value, name) {
                                window.location.href = "video_select/nncms_controls_video_select.php?org_id=" + value + "&org_type=2&method=<?php echo $method; ?>" + "&more_select=<?php echo $nns_more_select; ?><?php echo $except_url; ?>";
                        }



                </script>
        </head>

        <body style='width:760px;'>
                <div class="selectbox">
                        <iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
                </div>
                <div class="content video_select_control" style="padding:0px;  <?php if (isset($_GET['locktype'])) { ?>margin:-10px 0 0 -10px;<?php
                } else {
                        echo 'margin:0;';
                }
                ?>">
                        <?php if (!isset($_GET['locktype'])) { ?><div class="closebtn"><img src="../../images/topicon_08-topicon.png" /></div><?php } ?>
                        <div class="select_title"><b><?php echo cms_get_lang('media_select_box'); ?></b></div>

                        <div style="background-color:#FFF;">

                                <div class="category_tree" style="padding:10px; margin:10px; overflow-y:auto;width:180px;background:#F5F5F5;min-height:410px;">
                                        <?php
                                        if ($data != null) {
                                                $assist_content = $data[0]["nns_category"];
                                        } else {
                                                $assist_content = null;
                                        }
                                        echo pub_func_get_tree_by_html($assist_content, $base_name, $base_id);
                                        ?>


                                </div>
                                <div class="category_editbox"  style="padding:10px; margin:0px;">
                                        <div class="content_table">

                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>

                                                                <tr class="select_carrier_class">
                                                                        <?php if ($_SESSION["nns_manager_type"] == 0) { ?>
                                                                                <td  width="120"><b><?php echo cms_get_lang('service_qhpk'); ?></b></td>
                                                                                <td  width="200"><select name="nns_carrier" id="nns_carrier"  style=" padding:3px; width:60%;"><br>
                                                                                                        <option value="<?php echo $carrier_data["nns_id"]; ?>" <?php if ($nns_org_type == 0) { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('xtgl_yysgl') . "/" . $carrier_data["nns_name"]; ?></option>
                                                                                                        <?php if ($g_partner_enabled == 1) { ?>
                                                                                                                <option value="1"  <?php if ($nns_org_type == 1) { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('partner'); ?></option>
                                                                                                        <?php }if ($g_group_enabled == 1) { ?>
                                                                                                                <option value="2"  <?php if ($nns_org_type == 2) { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('group_jtxx'); ?></option>
                                                                                                        <?php } ?>
                                                                                        </select>

                                                                                </td>
                                                                        <?php } ?>
                                                                        <td  width="100"><b><?php echo cms_get_lang('service_pklx'); ?></b></td>
                                                                        <td  width="220"><select name="nns_method" id="nns_method"  style=" padding:3px; width:60%;"><br>
                                                                                                <option value="0"  <?php if ($method == "vod") { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('media_db'); ?></option>
                                                                                                <?php if ($g_live_enabled == "1") { ?>
                                                                                                        <option value="1"  <?php if ($method == "live") { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('media_zb'); ?></option>
                                                                                                <?php } ?>

                                                                                </select>

                                                                        </td>

                                                                        </td>
                                                                </tr>


                                                                <tr>
                                                                        <td width="100"><?php echo cms_get_lang('lmgl_nrlmid'); ?></td>
                                                                        <td class="category_edit_id"></td>
                                                                        <td width="120"><?php echo cms_get_lang('lmgl_lmmc'); ?> <font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font></td>
                                                                        <td  class="category_edit_name"  width="200"></td>
                                                                </tr>
                                                                <tr>
                                                                        <td width="100"><?php echo cms_get_lang('search'); ?></td>
                                                                        <td width="220"><input name="nns_video_search" id="nns_video_search" type="text"
                                                                                               value="" onchange="refresh_video_box();" style="width:100px;"
                                                                                               />&nbsp;<input type="button" value="<?php echo cms_get_lang('search'); ?>" onclick="search_video();"/></td>
                                                                        <td  width="100"><?php echo cms_get_lang('contorl_zydfj') ?></td>
                                                                        <td  width="220"><?php echo cms_get_lang('media_js'); ?><select name="nns_video_index" id="nns_video_index"  style=" padding:2px; width:40px;"></select>	&nbsp;&nbsp;<input type="button" value="<?php echo cms_get_lang('confirm'); ?>" onclick="import_media();"/></td>

                                                                </tr>

                                                        </tbody>
                                                </table>
                                        </div>

                                        <div class="video_frame">
                                                <iframe  scrolling="auto" frameborder="0" onload="checkheight();" style="width:100%;overflow-x:hidden;"></iframe>
                                        </div>

                                </div>
                                <div style="clear:both;"/>
                        </div>
                </div>
        </body>
</html>