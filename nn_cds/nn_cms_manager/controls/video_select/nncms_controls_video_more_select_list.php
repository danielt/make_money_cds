<?php
header("Content-Type:text/html;charset=utf-8");
 error_reporting(E_ALL ^ E_NOTICE);
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 ob_start();
 include("../../nncms_manager_inc.php");
//获取权限检查类
session_start();
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
 $method=$_GET["method"];
 $search=$_GET["search"];
 $depot_id=$_GET["depot_id"];
 $depot_detail_id=$_GET["depot_detail_id"];

 $except_method=$_GET["except_method"];
 $except_detail_id=$_GET["except_detail_id"];
 $except_id=$_GET["except_id"];

 $nns_url="&search=".urlencode($search)."&depot_id=".$depot_id."&depot_detail_id=".$depot_detail_id.
"&except_method=".$except_method."&except_detail_id=".$except_detail_id."&except_id=".$except_id;
  $currentpage=1;
  if (!empty($_GET["page"])) $currentpage=$_GET["page"];
 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
  if (!empty($_COOKIE["page_max_num"])){
		$g_manager_list_max_num=$_COOKIE["page_max_num"];
	}
	
	//        支持拼音和名称同时查询 BY S67 2013-5-19
        $search=array(
        	'nns_pinyin'=>$search,
        	'nns_name'=>$search
        );
	
 if ($method==0){
 	 include($nncms_db_path. "nns_vod/nns_db_vod_class.php");
	 $vod_inst=new nns_db_vod_class();
	 $countArr=$vod_inst->nns_db_vod_count($search,"",1,"",$depot_id,$depot_detail_id,null,null,null,null,NNS_VOD_DELETED_FALSE,$except_method,$except_detail_id,$except_id);
	 if ($countArr["ret"]==0)
	 $vod_total_num=$countArr["data"][0]["num"];
	 $pages=ceil($vod_total_num/$g_manager_list_max_num);
	 $currentpage=1;
	 if (!empty($_GET["page"])) $currentpage=$_GET["page"];

	 	$vod_array=$vod_inst->nns_db_vod_list("nns_id,nns_check,nns_director,nns_actor,nns_show_time,nns_view_len,nns_all_index,nns_new_index,nns_area,nns_name,nns_org_type,nns_org_id,nns_create_time,nns_modify_time,nns_state",
	 	$search,"",1,"",$depot_id,$depot_detail_id,"","","","","",($currentpage-1)*$g_manager_list_max_num,$g_manager_list_max_num,NNS_VOD_DELETED_FALSE,$except_method,$except_detail_id,$except_id);

	 $vod_inst=null;
	// var_dump($manager_array);
	 if ($vod_array["ret"]!=0){
	 	$data=null;
//	 	echo "<script>alert(". $vod_array["reason"].");</script>";
	 }
	 $data=$vod_array["data"];
 }else if ($method==1){
 	include($nncms_db_path. "nns_live/nns_db_live_class.php");
 	 $live_inst=new nns_db_live_class();
	 $countArr=$live_inst->nns_db_live_count($search,"",1,"",$depot_id,$depot_detail_id,null,null,null,null,NNS_VOD_DELETED_FALSE,$except_method,$except_detail_id,$except_id);
	 if ($countArr["ret"]==0)
	 $live_total_num=$countArr["data"][0]["num"];
	 $pages=ceil($live_total_num/$g_manager_list_max_num);
	 $currentpage=1;
	 if (!empty($_GET["page"])) $currentpage=$_GET["page"];
	$live_array=$live_inst->nns_db_live_list("nns_id,nns_check,nns_director,nns_actor,nns_show_time,nns_view_len,nns_all_index,nns_new_index,nns_area,nns_name,nns_org_type,nns_org_id,nns_create_time,nns_modify_time,nns_state",
	$search,"",1,"",$depot_id,$depot_detail_id,"","","","","",
	($currentpage-1)*$g_manager_list_max_num,$g_manager_list_max_num,NNS_VOD_DELETED_FALSE,$except_method,$except_detail_id,$except_id);
	 $live_inst=null;
	// var_dump($manager_array);
	 if ($live_array["ret"]!=0){
	 	$data=null;
//	 	echo "<script>alert(". $live_array["reason"].");</script>";
	 }
	 $data=$live_array["data"];
 }
include $nncms_db_path. "nns_common/nns_db_pager_class.php";
$url="?";
$url.=$nns_url;
$pager = new nns_db_pager_class($countArr["data"][0]["num"],$g_manager_list_max_num,$currentpage,$url);
 ob_end_clean();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/trim.js"></script>
<script language="javascript">
function get_more_select(){
	return getAllCheckBoxSelect();
}

</script>
</head>

<body>
<div class="content">
    <div style="padding:5px;">
     <div class="content_table formtable">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
        	<tr>
        		<th><input name="" type="checkbox" value="" /></th>
                <th><?php echo cms_get_lang('segnumber');?></th>
                <th><?php echo cms_get_lang('name');?></th>

                <th><?php echo cms_get_lang('media_sysj');?></th>
                <th><?php echo cms_get_lang('media_dqgx');?></th>
                <th><?php echo cms_get_lang('contorl_sc_m');?></th>


            </tr>
        </thead>
        <tbody>
         <?php
        	if ($data!=null){
        	 $num=($currentpage-1)*$g_manager_list_max_num; foreach($data as $item){
        	$num++;
        	?>
			<!-- 2012-8-20 陈波 1661507441@qq.com 取消PHP短标签的使用   start -->
              <tr>
               	 <td><input name="input" type="checkbox" value="<?php echo  $item["nns_id"];?>" /></td>
                <td><?php echo $num;?></td>
                <td><?php echo $item["nns_name"];?></td>


                <td><?php echo $item["nns_show_time"];?> </td>
                <td><?php echo $item["nns_area"];?> </td>
                <td><?php echo ceil($item["nns_view_len"]/60);?> </td>


              </tr>
			 <!-- 2012-8-20 陈波 1661507441@qq.com 取消PHP短标签的使用   end -->
          <?php }}?>
          <script language="javascript">
			create_empty_tr($(".formtable"),<?php echo count($data);?>,<?php echo $nncms_ui_min_list_item_count;?>);
		  </script>
          </tbody>
        </table>
    </div>
    <?php echo $pager->nav_4();?>
    <!-- <div class="pagecontrol">&nbsp;
     	<?php if ($currentpage>1){?>
     	<a href="nncms_controls_video_more_select_list.php?method=<?php echo $method;?><?php echo $nns_url;?>&page=1" target="_self"><?php echo cms_get_lang('first_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
    	<a href="nncms_controls_video_more_select_list.php?method=<?php echo $method;?><?php echo $nns_url;?>&page=<?php echo $currentpage-1;?>" target="_self"><?php echo cms_get_lang('pre_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
    	<?php }else{?>
    		<span><?php echo cms_get_lang('first_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
    		<span><?php echo cms_get_lang('pre_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
    	<?php } if ($currentpage<$pages){?>
        <a href="nncms_controls_video_more_select_list.php?method=<?php echo $method;?><?php echo $nns_url;?>&page=<?php echo $currentpage+1;?>" target="_self"><?php echo cms_get_lang('next_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="nncms_controls_video_more_select_list.php?method=<?php echo $method;?><?php echo $nns_url;?>&page=<?php echo $pages;?>" target="_self"><?php echo cms_get_lang('last_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
         <?php }else{?>
      		<span><?php echo cms_get_lang('next_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
    		<span><?php echo cms_get_lang('last_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
      	<?php }?>

        <?php echo cms_get_lang('jump_to');?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage;?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page');?> &nbsp;&nbsp;
        <a href="javascript:go_page_num('nncms_controls_video_more_select_list.php?method=<?php echo $method;?><?php echo $nns_url;?>',<?php echo $pages;?>);">GO>></a>&nbsp;&nbsp;
        <?php echo cms_get_lang('current');?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage."/".$pages;?></span><?php echo cms_get_lang('page');?>&nbsp;&nbsp;

    </div>-->
</div>
</body>
</html>