<?php
header("Content-Type:text/html;charset=utf-8");
error_reporting(E_ALL ^ E_NOTICE);
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../../nncms_manager_inc.php");
//获取权限检查类
session_start();
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language_media.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
$method = $_GET["method"];
$search = $_GET["search"];
$nns_org_id = $_GET["org_id"];
$nns_org_type = $_GET["org_type"];
$nns_more_select = $_GET["more_select"];

$except_method = $_GET["except_method"];
$except_detail_id = $_GET["except_detail_id"];
$except_id = $_GET["except_id"];
$except_url = "";
if ($except_method == "assist" || $except_method == "service" || $except_method == "ad" || $except_method == "ad_policy") {
        $except_url = "&except_method=$except_method&except_detail_id=$except_detail_id&except_id=$except_id";
}

$nns_url = "&org_id=" . $nns_org_id . "&org_type=" . $nns_org_type;
$currentpage = 1;
if (!empty($_GET["page"]))
        $currentpage = $_GET["page"];
$listnum = 8;
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
$method_str = $language_media_mt;

include($nncms_db_path . "nns_depot/nns_db_depot_class.php");
include_once $nncms_config_path . 'nn_logic/video/live.class.php';
$depot_inst = new nns_db_depot_class();
$depot_array = $depot_inst->nns_db_depot_list("", $nns_org_type, $nns_org_id, 1);
// var_dump($partner_array);
if ($depot_array["ret"] != 0) {
        $data = null;
//	 	echo "<script>alert(". $depot_array["reason"].");</script>";
}
$depot_inst = null;
$data = $depot_array["data"];

include $nncms_db_path . "nns_carrier/nns_db_carrier_class.php";
include $nncms_db_path . "nns_partner/nns_db_partner_class.php";
include $nncms_db_path . "nns_group/nns_db_group_class.php";


if ($_SESSION["nns_manager_type"] == 0) {
        $carrier_inst = new nns_db_carrier_class();
        $carrier_result = $carrier_inst->nns_db_carrier_list();

        if ($carrier_result["ret"] == 0) {
                $carrier_data = $carrier_result["data"][0];
                $base_name = $carrier_data["nns_name"];
        }
}

//		 	类型为合作伙伴
if ($nns_org_type == 1) {
        $partner_inst = new nns_db_partner_class();
        $partner_result = $partner_inst->nns_db_partner_info($nns_org_id);

        if ($partner_result["ret"] == 0) {
                $partner_data = $partner_result["data"][0];
                $base_name = $partner_data["nns_name"];
        }
        //	类型为集团
} else if ($nns_org_type == 2) {
        $group_inst = new nns_db_group_class();
        $group_result = $group_inst->nns_db_group_info($nns_org_id);

        if ($group_result["ret"] == 0) {
                $group_data = $group_result["data"][0];
                $base_name = $group_data["nns_name"];
        }
}


$base_id = 10000;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                <link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
                <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../../js/dtree.js"></script>
                <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../../js/table.js.php"></script>
                <script language="javascript" src="../../js/trim.js"></script>
                <script language="javascript" src="../../js/category_count.js"></script>
                <script language="javascript" src="../../js/cms_alert_select_box_style.js"></script>
                <script language="javascript" src="../../js/cms_datepicker.js"></script>
                <script language="javascript">

                        function set_video_id(id, type, name, star) {

                                window.parent.parent.set_video_id(id, type, name, '0');
                                //window.parent.close_select();
                                //refresh_video_box();
                        }

                        function set_more_video_id() {
                                var ids = $(".video_frame iframe")[0].contentWindow.get_more_select();
                                window.parent.set_video_id(ids, 2);
                                //alert(ids);
                                //	window.parent.close_select();
                                refresh_video_box();
                        }

                        var now_select_id;
                        $(document).ready(function() {

                                $(".closebtn").click(function() {
                                        window.parent.close_select();
                                });
                                $(".category_editbox").width($(".content").width() - 250);
                                $(".service_frame iframe").width($(".content").width() - 250);
                                $(window).resize(function() {
                                        $(".category_editbox").width($(".content").width() - 250);
                                        $(".service_frame iframe").width($(".content").width() - 250);
                                });


                                $(".selectbox").hide();
                                $(".selectbox").css("left", ($("body").width() - $(".selectbox").width()) / 2);
                                $(".selectbox").css("top", 10);

                                $("#nns_carrier").change(function() {
                                        var $obj = $(this).find("option:selected");
                                        var index = $("#nns_carrier option").index($obj);
                                        selectCarrier(index);
                                });
                                $("#nns_method").change(function() {
                                        var $obj = $(this).find("option:selected");
                                        var index = $("#nns_method option").index($obj);
                                        selectMethod(index);
                                })
                        });

                        function selectMethod(index) {
                                switch (index) {
                                        case 0:
                                                index_str = "vod";
                                                $select_url = "nncms_controls_video_select_index.php?org_id=<?php echo $nns_org_id; ?>&org_type=<?php echo $nns_org_type; ?>&method=vod&more_select=<?php echo $nns_more_select; ?><?php echo $except_url; ?>";
                                                break;
                                        case 1:
                                                $select_url = "nncms_controls_video_select._index.php?org_id=<?php echo $nns_org_id; ?>&org_type=<?php echo $nns_org_type; ?>&method=live&more_select=<?php echo $nns_more_select; ?><?php echo $except_url; ?>";
                                                break;
                                        case 2:
                                                $select_url = "nncms_controls_playbill_select_index.php?org_id=<?php echo $nns_org_id; ?>&org_type=<?php echo $nns_org_type; ?>&method=playbill&more_select=<?php echo $nns_more_select; ?><?php echo $except_url; ?>";
                                                break;
                                }
                                window.location.href = $select_url;
                        }


                        function checkhiddenInput() {
                                var par = getAllCheckBoxSelect();
                                if (par == "") {
                                        alert("<?php echo cms_get_lang('lmgl_msg_noselect'); ?>");
                                } else {
                                        $('#nns_id').attr('value', getAllCheckBoxSelect());
                                        checkForm('<?php echo cms_get_lang('titile'); ?>', '<?php echo cms_get_lang('msg_ask_delete'); ?>', $('#delete_form'));
                                }
                        }

                        function select_tree_live(item_id, item_name, parent_id) {

                                $(".video_frame").show();
                                now_select_id = item_id;
                                $("#pro_name").html(item_name);
                                refresh_video_box();
                        }
                        function refresh_video_box() {
                                var day = $('#nns_day').val();
<?php if ($nns_more_select == true) { ?>
                                        $(".video_frame iframe").attr("src", "nncms_controls_playbill_more_select_list.php?method=" + $("#nns_method").val() + "&live_id=" + now_select_id + "&day=" + day + "<?php echo $except_url; ?>&search=" + encodeURI($("#nns_video_search").val()));
<?php } else { ?>
                                        $(".video_frame iframe").attr("src", "nncms_controls_playbill_select_list.php?method=" + $("#nns_method").val() + "&live_id=" + now_select_id + "&day=" + day + "<?php echo $except_url; ?>&search=" + encodeURI($("#nns_video_search").val()));
<?php } ?>
                        }


                        function checkheight() {
                                $(".video_frame iframe").height($(".video_frame iframe").contents().find(".content").height());
                                $(".category_tree").height($(".category_editbox").height() - 20);

                        }

                        function selectCarrier(num) {

                                switch (num) {
                                        case 1:
                                                begin_select_partner();
                                                break;
                                        case 2:
                                                begin_select_group();
                                                break;
                                        case 0:
                                                window.location.href = "nncms_controls_playbill_select.php?org_id=<?php echo $carrier_data["nns_id"]; ?>&org_type=0&method=<?php echo $method; ?>" + "<?php echo $except_url; ?>&more_select=<?php echo $nns_more_select; ?>";
                                                break;
                                }
                        }

                        function begin_select_partner() {
                                $("#select_frame").attr("src", "../nncms_controls_select.php?method=partner");
                                $(".selectbox").show();
                        }

                        function begin_select_group() {
                                $("#select_frame").attr("src", "../nncms_controls_select.php?method=group");
                                $(".selectbox").show();
                        }
                        function close_select() {
                                $(".selectbox").hide();
                        }

                        function set_partner_id(value, name) {
                                window.location.href = "video_select/nncms_controls_playbill_select.php?org_id=" + value + "&org_type=1&method=<?php echo $method; ?>" + "&more_select=<?php echo $nns_more_select; ?><?php echo $except_url; ?>";
                        }

                        //	function set_video_id(value,type,name){
                        //
                        //		window.parent.set_video_id(value,type,name);
                        //	}

                        function set_group_id(value, name) {
                                window.location.href = "video_select/nncms_controls_playbill_select.php?org_id=" + value + "&org_type=2&method=<?php echo $method; ?>" + "&more_select=<?php echo $nns_more_select; ?><?php echo $except_url; ?>";
                        }

                </script>
        </head>

        <body  style='width:760px;'>
                <div class="selectbox">
                        <iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
                </div>
                <div class="content video_select_control" style="padding:0px; margin:0px;">

                        <div class="select_title"><b><?php echo cms_get_lang('media_select_box'); ?></b></div>

                        <div style="background-color:#FFF;">

                                <div class="category_tree" style="padding:10px; margin:10px; overflow-y:auto;width:180px;background:#F5F5F5;min-height:410px;">
                                        <script type="text/javascript">
<!--

                                                d = new dTree('d');

                                                d.add(0, -1, '<?php echo $base_name; ?>', "javascript:select_tree_item('<?php echo $base_id ?>','<?php echo $base_name; ?>','0');");
<?php
if ($data != null) {
        $depot_content = $data[0]["nns_category"];
        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->loadXML($depot_content);
        $category = $dom->getElementsByTagName('category');
        include($nncms_db_path . "nns_live/nns_db_live_class.php");
        $live_inst = new nns_db_live_class();
        $dc = nl_get_dc(Array(
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));

        $dc->open();

        foreach ($category as $item) {

                if ($item->getAttribute("parent"))
                        
                        ?>
                                                                d.add(<?php echo $item->getAttribute("id"); ?>,<?php echo $item->getAttribute("parent"); ?>, '<?php echo $item->getAttribute("name"); ?>', "");
                                                                //			直播用遍历直播栏目所有内容
                <?php
                $live_array = $live_inst->nns_db_live_list("nns_id,nns_name", "", "", 1, $view_type, $data[0]["nns_id"], $item->getAttribute("id"));

                if ($live_array["ret"] == 0) {
                        $live_data = $live_array["data"];
                        foreach ($live_data as $item_data) {
                                $live_info = nl_live::epg_get_live_info($dc, $item_data['nns_id'], NL_DC_DB);
                                if (strstr($live_info[0]['nns_live_type'], 'TSTV')) {
                                        ?>
                                                                                        d.add('<?php echo $item_data["nns_id"]; ?>',<?php echo $item->getAttribute("id"); ?>, '<?php echo $item_data["nns_name"]; ?>', "javascript:select_tree_live('<?php echo $item_data["nns_id"]; ?>','<?php echo $item_data["nns_name"]; ?>','<?php echo $item->getAttribute("id"); ?>');", 0, "", "", "../../images/tree/cd.png");
                                        <?php
                                }
                        }
                }
                ?>
                                                                //			-------------------------------
                <?php
        }$live_inst = null;
}
?>

                                                document.write(d);


                                                //-->



                                        </script>
                                </div>
                                <div class="category_editbox"  style="padding:10px; margin:0px;">
                                        <div class="content_table">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>

                                                                <tr class="select_carrier_class">
                                                                        <?php if ($_SESSION["nns_manager_type"] == 0) { ?>
                                                                                <td  width="120"><b><?php echo cms_get_lang('service_qhpk'); ?></b></td>
                                                                                <td  width="200"><select name="nns_carrier" id="nns_carrier"  style=" padding:3px; width:60%;"><br>
                                                                                                        <option value="<?php echo $carrier_data["nns_id"]; ?>" <?php if ($nns_org_type == 0) { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('xtgl_yysgl') . "/" . $carrier_data["nns_name"]; ?></option>
                                                                                                        <?php if ($g_partner_enabled == 1) { ?>
                                                                                                                <option value="1"  <?php if ($nns_org_type == 1) { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('partner'); ?></option>
                                                                                                        <?php }if ($g_group_enabled == 1) { ?>
                                                                                                                <option value="2"  <?php if ($nns_org_type == 2) { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('group_jtxx'); ?></option>
                                                                                                        <?php } ?>
                                                                                        </select>

                                                                                </td>
                                                                        <?php } ?>
                                                                        <td  width="120"><b><?php echo cms_get_lang('service_pklx'); ?></b></td>
                                                                        <td  width="200"><select name="nns_method" id="nns_method"  style=" padding:3px; width:60%;"><br>
                                                                                                <option value="0"  <?php if ($method == "vod") { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('media_db'); ?></option>
                                                                                                <?php if ($g_live_enabled == "1") { ?>
                                                                                                        <option value="1"  <?php if ($method == "live") { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('media_zb'); ?></option>
                                                                                                <?php } ?>
                                                                                                <?php if ($g_live_bill_enabled == "1") { ?>
                                                                                                        <option value="2"  <?php if ($method == "playbill") { ?>selected="selected"<?php } ?>>时移节目</option>
                                                                                                <?php } ?>
                                                                                </select>

                                                                        </td>
                                                                </tr>


                                                                <tr>
                                                                        <td width="120"><?php echo cms_get_lang('contorl_pd'); ?></td>
                                                                        <td  width="200" id="pro_name"></td>
                                                                        <td width="120"><?php echo cms_get_lang('date'); ?></td>
                                                                        <td  width="200"><input name="nns_day" id="nns_day" type="text" class="datepicker"
                                                                                                value="<?php echo date('Y-m-d'); ?>"
                                                                                                /> </td>
                                                                </tr>
                                                                <tr>

                                                                        <td width="120"><?php echo cms_get_lang('contorl_jmm'); ?></td>
                                                                        <td ><input name="nns_video_search" id="nns_video_search" type="text"
                                                                                    value=""
                                                                                    /></td>
                                                                        <td><input type="button" value="<?php echo cms_get_lang('media_ypcx'); ?>" onclick="refresh_video_box();"/>
                                                                        </td>
                                                                        <td><?php if ($nns_more_select == true) { ?>
                                                                                        <input type="button" value="<?php echo cms_get_lang('confirm_search_xz'); ?>" onclick="set_more_video_id();"/>
                                                                                <?php } ?>
                                                                        </td>
                                                                </tr>

                                                        </tbody>
                                                </table>
                                        </div>

                                        <div class="video_frame">
                                                <iframe  scrolling="auto" frameborder="0" onload="checkheight();" style="width:100%;overflow-x:hidden;"></iframe>
                                        </div>

                                </div>
                                <div style="clear:both;"/>
                        </div>
                </div>
        </body>
</html>
<script>
                                                var node = d.selectedNode;
                                                if (!node)
                                                        node = 0;
                                                var node_id = d.aNodes[node].id;
                                                var node_parent_id = d.aNodes[node].pid;
                                                var node_name = d.aNodes[node].name;
                                                if (node_id == 0) {
                                                        node_id = "1000";
                                                }
                                                if (node_parent_id == -1) {
                                                        node_parent_id = 0;
                                                }
                                                if (node_id.length == 32) {
                                                        select_tree_live(node_id, node_name, node_parent_id);
                                                } else {
                                                        select_tree_item(node_id, node_name, node_parent_id);
                                                }
</script>