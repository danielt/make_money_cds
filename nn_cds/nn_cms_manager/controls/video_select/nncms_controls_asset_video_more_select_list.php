<?php
header("Content-Type:text/html;charset=utf-8");
 error_reporting(E_ALL ^ E_NOTICE);
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 ob_start();
 include("../../nncms_manager_inc.php");
//获取权限检查类
session_start();
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

 $search=$_GET["search"];
 $asset_id=$_GET["asset_id"];
 $category_id=$_GET["category_id"];
$virtual=$_GET["virtual"];


 $nns_url="&search=".urlencode($search)."&asset_id=".$asset_id."&category_id=".$category_id."&virtual=".$virtual;
  $currentpage=1;
  if (!empty($_GET["page"])) $currentpage=$_GET["page"];
 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
  if (!empty($_COOKIE["page_max_num"])){
		$g_manager_list_max_num=$_COOKIE["page_max_num"];
  }
 	
 	include_once LOGIC_DIR. "asset/asset_item.class.php";
 	 include_once LOGIC_DIR. "asset/asset_item_bo.class.php"; 	
 	$dc = nl_get_dc(array (
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
			));
	$dc->open();
	
	$search=empty($search)?NULL:$search;
	if ($virtual==1){
		$category_params=nl_asset::get_virtual_category_params(
		$dc,
		$asset_id,
		$category_id,
		NL_DC_DB
		);
		
		$labels=$category_params['nns_labels']['nns_value'];
		
		$labels=rtrim($labels,',');
		$labels=explode(',',$labels);
		
		$category_id=substr($category_id,0,strlen($category_id)-3);
		$count=nl_asset_item_bo::count_asset_item_search_by_labels($dc,NULL,$asset_id,$category_id,$labels,0,'name_likechar|pinyin_firstchar',$search);
		$data=nl_asset_item_bo::epg_get_asset_item_search_by_labels($dc,null,$asset_id,$category_id,$labels,($currentpage-1)*$g_manager_list_max_num,$g_manager_list_max_num,NL_ORDER_NUM_ASC,0,'name_likechar|pinyin_firstchar',$search);
	}else{
		
		$count=nl_asset_item::count_asset_item_search($dc,NULL,$asset_id,$category_id,0,'name_likechar|pinyin_firstchar',$search);
 	 	$data=nl_asset_item::epg_get_asset_item_search($dc,NULL,$asset_id,$category_id,($currentpage-1)*$g_manager_list_max_num,$g_manager_list_max_num,NL_ORDER_NUM_DESC,0,'name_likechar|pinyin_firstchar',$search);
	}
// var_dump($data);die;	

include $nncms_db_path. "nns_common/nns_db_pager_class.php";
$url="?";
$url.=$nns_url;
$pager = new nns_db_pager_class($count,$g_manager_list_max_num,$currentpage,$url);
 ob_end_clean();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/trim.js"></script>
<script language="javascript">
function get_more_select(){
	var arr=getAllCheckBoxSelect();
	arr=arr.split(',');
	var arr1=[];
	var arr2=[];
	for (var i=0;i<arr.length;i++){
		if (arr[i]){
			var data=arr[i].split('|');
			arr1.push(data[0]);
			if (data[1]){
			 arr2.push(data[1]);
			}else{
				arr2.push(0);
			}
		}
	}
	return {
		ids:arr1.join(','),types:arr2.join(',')
	};
}

</script>
</head>

<body>
<div class="content">
    <div style="padding:5px;">
     <div class="content_table formtable">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
        	<tr>
        		<th><input name="" type="checkbox" value="" /></th>
                <th><?php echo cms_get_lang('segnumber');?></th>
				<th><?php echo cms_get_lang('name');?></th>

				<th><?php echo cms_get_lang('media_mtlx');?></th>
				 <th><?php echo cms_get_lang('play_count');?></th>


            </tr>
        </thead>
        <tbody>
         <?php
        	if (is_array($data)){
        	 $num=($currentpage-1)*$g_manager_list_max_num; foreach($data as $item){
        	$num++;
        	?>
			<!-- 2012-8-20 陈波 1661507441@qq.com 取消PHP短标签的使用   start -->
              <tr>
               	 <td><input name="input" type="checkbox" value="<?php echo  $item["nns_video_id"].'|'.$item["nns_video_type"];?>" /></td>
                <td><?php echo $num;?></td>
                <td><?php echo $item["nns_video_name"];?></td>


                <td><?php if ($item["nns_video_type"]==0){
				echo cms_get_lang('media_db');
			   }elseif ($item["nns_video_type"]==1){
				echo cms_get_lang('media_zb');
			   }elseif($item["nns_video_type"]==2){
				echo cms_get_lang('media_sy|program');
			   };?> </td>
                <td><?php echo (int)$item["nns_play_count"];?></td>


              </tr>
			 <!-- 2012-8-20 陈波 1661507441@qq.com 取消PHP短标签的使用   end -->
          <?php }}?>
          <script language="javascript">
			create_empty_tr($(".formtable"),<?php echo count($data);?>,<?php echo $nncms_ui_min_list_item_count;?>);
		  </script>
          </tbody>
        </table>
    </div>
    <?php echo $pager->nav_4();?>
    <!-- <div class="pagecontrol">&nbsp;
     	<?php if ($currentpage>1){?>
     	<a href="nncms_controls_video_more_select_list.php?method=<?php echo $method;?><?php echo $nns_url;?>&page=1" target="_self"><?php echo cms_get_lang('first_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
    	<a href="nncms_controls_video_more_select_list.php?method=<?php echo $method;?><?php echo $nns_url;?>&page=<?php echo $currentpage-1;?>" target="_self"><?php echo cms_get_lang('pre_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
    	<?php }else{?>
    		<span><?php echo cms_get_lang('first_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
    		<span><?php echo cms_get_lang('pre_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
    	<?php } if ($currentpage<$pages){?>
        <a href="nncms_controls_video_more_select_list.php?method=<?php echo $method;?><?php echo $nns_url;?>&page=<?php echo $currentpage+1;?>" target="_self"><?php echo cms_get_lang('next_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="nncms_controls_video_more_select_list.php?method=<?php echo $method;?><?php echo $nns_url;?>&page=<?php echo $pages;?>" target="_self"><?php echo cms_get_lang('last_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
         <?php }else{?>
      		<span><?php echo cms_get_lang('next_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
    		<span><?php echo cms_get_lang('last_page');?></span>&nbsp;&nbsp;&nbsp;&nbsp;
      	<?php }?>

        <?php echo cms_get_lang('jump_to');?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage;?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page');?> &nbsp;&nbsp;
        <a href="javascript:go_page_num('nncms_controls_video_more_select_list.php?method=<?php echo $method;?><?php echo $nns_url;?>',<?php echo $pages;?>);">GO>></a>&nbsp;&nbsp;
        <?php echo cms_get_lang('current');?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage."/".$pages;?></span><?php echo cms_get_lang('page');?>&nbsp;&nbsp;

    </div>-->
</div>
</body>
</html>