<?php
/**
 * @author txc  by 2012-12-10
 */
 header("Content-Type:text/html;charset=utf-8");
 include("../../nncms_manager_inc.php");
 include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php";
  include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
$category_id=$_GET["category_id"];
$topic_id=$_GET["topic_id"];
$ids=$_GET["ids"];
$currentpage=$_GET["currentpage"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/dtree.js"></script>
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/trim.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript">

	


function close_select(){
	window.parent.close_select();
}

function check_select_height(){
	$(".selectbox iframe").height($(".selectbox iframe").contents().find(".selectcontrol").height());
	window.parent.check_select_height();
}
function refuse_audit_edit(){
	var content=document.getElementById("edit_1").value;
	window.parent.refuse_audit_edit("<?php echo $ids?>",content);
	
}
</script>
</head>

<body  style='width:400px;'>
<div class="content selectcontrol" style="padding:0px; margin:0px;">
<div class="closebtn" onclick="close_select()"><img src="../../images/topicon_08-topicon.png" /></div>

	<div style="background-color:#FFF;">
	    <div class="category_editbox" style="width:370px; margin:0px;">
		     <div class="content_table">
	    	<table width="370" border="0" cellspacing="0" cellpadding="0">
	        <tbody>
	    		<tr>
			                <td><?php echo cms_get_lang('lmgl_nrlmid');?></td>
			                <td class="category_edit_id"><?php echo $_GET["category_id"]?></td>
			                <td><?php echo cms_get_lang('lmgl_lmmc');?></td>
			                <td  class="category_edit_name"><?php echo $_GET["category_name"]?></td>
			      	</tr>
			        
			      </tbody>
			      </table>
			    </div>

	    </div>
	      <div class="category_tree" style="overflow:auto;width:370px; height:200px; margin:0px;">
	      <form  id="edit" action="../../contentframe/topic_pages/nncms_content_topic_item_control.php" method="post" >
			<table width="370" border="0" cellspacing="0" cellpadding="0">
				<tbody>
				<input type="hidden" name="action" value="refuse_audit" />
				<input type="hidden" name="category_id" value="<?php echo $category_id?>" />
				<input type="hidden" name="topic_id" value="<?php echo $topic_id?>" />
				<input type="hidden" name="item_id" value="<?php echo $ids?>" />
				<input type="hidden" name="currentpage" value="<?php echo $currentpage?>" />
				<tr>
					<td height="20px" > <h3><?php echo  cms_get_lang('contorl_jjshyy');?><h3></td>
				</tr>
				<tr>
					<td><textarea style="width:350px;height:320px"  name="content"  id="edit_1"></textarea></td>
				</tr>
				<tr>
				 <td valign="center"><input type="button" value="<?php echo cms_get_lang('contorl_tj');?>" onclick="refuse_audit_edit();"/></td>
				 </tr>
				</tbody>
			</table>
			</form>
	    </div>

	    <div style="clear:both;"></div>
    </div>
</div>
</body>
</html>