<?php
/**
 * @author txc by 2012-11-22
 */
 header("Content-Type:text/html;charset=utf-8");
 include("../../nncms_manager_inc.php");
 include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php";
 include $nncms_config_path.'nn_logic/topic/topic.class.php';
 include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
$topic_id=$_GET["topic_id"];
$action=$_GET['action'];

$dc = nl_get_dc(array(
			'db_policy'=>NL_DB_READ,
			'cache_policy'=>NP_KV_CACHE_TYPE_NULL
                     )
		);
$dc->open();
$topic_info = nl_topic::get_topic_info($dc,$topic_id);
$parent_name = $topic_info['0']['nns_name'];
$category = $topic_info['0']['nns_category'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/dtree.js"></script>
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/trim.js"></script>
<script language="javascript">




var now_select_id;
$(document).ready(function(){
	$(".closebtn").click(function(){
		window.parent.close_select();
	});

});
function select_tree_item(item_id,item_name,parent_id){
	$(".category_editbox").find(".category_edit_id").html(item_id);
	$(".category_editbox").find(".category_edit_name").html(item_name);
		$(".video_frame").show();
		now_select_id=item_id;
}
function set_move_category_id(){
		window.parent.set_move_category_id(now_select_id,'<?php echo $topic_id;?>','<?php echo $action?>');
	}

	function check_select_height(){
		$(".selectbox iframe").height($(".selectbox iframe").contents().find(".selectcontrol").height());
		window.parent.check_select_height();
	}
</script>
</head>

<body  style='width:400px;'>
<div class="content selectcontrol" style="padding:0px; margin:0px;">
<div class="closebtn"><img src="../../images/topicon_08-topicon.png" /></div>

	<div style="background-color:#FFF;">


	    <div class="category_editbox" style="width:370px; margin:0px;">
		     <div class="content_table">
	    	<table width="370" border="0" cellspacing="0" cellpadding="0">
	        <tbody>
	    		<tr>
			                <td><?php echo cms_get_lang('lmgl_nrlmid');?></td>
			                <td class="category_edit_id"></td>
			                <td><?php echo cms_get_lang('lmgl_lmmc');?></td>
			                <td  class="category_edit_name"></td>
			      	</tr>
			          <tr>
				           <td></td>
		                	<td>

						</td>

			                <td><input type="button" value="<?php echo cms_get_lang('confirm_search_xz');?>" onclick="set_move_category_id();"/></td>
			                <td></td>

			      	</tr>


			      </tbody>
			      </table>
			    </div>

	    </div>
	      <div class="category_tree" style="overflow:auto;width:370px; height:200px; margin:0px;">
			<?php

			echo	pub_func_get_tree_by_html($category,$parent_name,'1000');

	        ?>

	    </div>

	    <div style="clear:both;"></div>
    </div>
</div>
</body>
</html>