<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
ob_start();
include("../../nncms_manager_inc.php");
//获取权限检查类
session_start();
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language_media.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
$method = $_GET["method"];
$tree_id = $_GET["tree_id"];
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
$method_str = $language_media_mt;
if ($method == "asset") {
        include($nncms_db_path . "nns_assist/nns_db_assist_class.php");
        $asset_inst = new nns_db_assist_class();
        $data_array = $asset_inst->nns_db_assist_info($tree_id);
        if (empty($data_array['data'])) {

                array_unshift($data_array['data'], array(
                    'nns_id' => 'All',
                    'nns_name' => '全部',
                    'nns_category' => '',
                    'nns_category_root_id' => '1000',
                ));
        }

        $asset_inst = null;
} else if ($method == "service") {
        include($nncms_db_path . "nns_service/nns_db_service_class.php");
        $service_inst = new nns_db_service_class();
        $data_array = $service_inst->nns_db_service_info($tree_id);
        $service_inst = null;
}

// var_dump($partner_array);
if ($data_array["ret"] != 0) {
        $data = null;
        echo "<script>alert(" . $data_array["reason"] . ");</script>";
}
$data = $data_array["data"];
$base_name = $data[0]["nns_name"];
$base_id = 1000;
ob_end_clean();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                <link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
                <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../../js/dtree.js"></script>
                <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../../js/table.js.php"></script>
                <script language="javascript" src="../../js/trim.js"></script>
                <script language="javascript">

                        var xml_str = '<?php
$content = str_replace("\n", "", $data[0]["nns_category"]);
echo $content;
?>';


                        var now_select_id;
                        $(document).ready(function() {
                                $(".closebtn").click(function() {
                                        window.parent.close_select();
                                });

                        });
                        function select_tree_item(item_id, item_name, parent_id) {


                                $(".category_editbox").find(".category_edit_id").html(item_id);
                                $(".category_editbox").find(".category_edit_name").html(item_name);
                                $(".video_frame").show();
                                now_select_id = item_id;

                        }




                        function set_move_category_id() {
                                var image = encodeURI($(xml_str).find("#" + now_select_id).attr("image"));

<?php if (isset($_GET['ismzb'])) { ?>
                                        window.parent.set_move_category_id_mzb(now_select_id, '<?php echo $data[0]["nns_id"]; ?>', $(".category_edit_name").html(), image);
<?php } else { ?>
                                        window.parent.set_move_category_id(now_select_id, '<?php echo $data[0]["nns_id"]; ?>', $(".category_edit_name").html(), image);
<?php } ?>

                        }

                        function check_select_height() {
                                $(".selectbox iframe").height($(".selectbox iframe").contents().find(".selectcontrol").height());
                                window.parent.check_select_height();
                        }
                </script>
        </head>

        <body  style='width:400px;'>

                <div class="content selectcontrol" style="padding:0px; margin:0px;">
                        <div class="closebtn"><img src="../../images/topicon_08-topicon.png" /></div>

                        <div style="background-color:#FFF;">


                                <div class="category_editbox" style="width:370px; margin:0px;">
                                        <div class="content_table">
                                                <table width="370" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                                <tr>
                                                                        <td><?php echo cms_get_lang('lmgl_nrlmid'); ?></td>
                                                                        <td class="category_edit_id"></td>
                                                                        <td><?php echo cms_get_lang('lmgl_lmmc'); ?></td>
                                                                        <td  class="category_edit_name"></td>
                                                                </tr>
                                                                <tr>
                                                                        <td></td>
                                                                        <td>

                                                                        </td>
                                                                        <td><?php echo cms_get_lang('action'); ?></td>
                                                                        <td><input type="button" value="<?php echo cms_get_lang('confirm_search_xz'); ?>" onclick="set_move_category_id();"/></td>

                                                                </tr>


                                                        </tbody>
                                                </table>
                                        </div>

                                </div>
                                <div class="category_tree" style="overflow:auto;width:370px; height:200px; margin:0px;">
                                        <?php
                                        if ($data != null) {
                                                $assist_content = $data[0]["nns_category"];
                                        } else {
                                                $assist_content = null;
                                        }
                                        echo @pub_func_get_tree_by_html($assist_content, $base_name, $base_id) ;
                                        ?>

                                </div>

                                <div style="clear:both;"></div>
                        </div>
                </div>
        </body>
</html>