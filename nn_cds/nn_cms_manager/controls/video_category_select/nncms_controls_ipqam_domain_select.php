<?php
include("../../nncms_manager_inc.php");
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
require_once $nncms_db_path . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_curl_content_class.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//------core start-----------
//从config 中获取http——url
$http_url = $g_core_url;
//通过CURL抓取返回值
$curl_post = 'func=get_core_net_domain' ;
$data = nns_db_curl_content_class::nns_db_curl_content ( $http_url, $curl_post );
$data=str_replace("\n","",$data);
$data=str_replace('<?xml version="1.0" encoding="utf-8" ?>',"",$data);
?>
<!DOCTYPE html PUBLIC "-//W3C//D XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/dtree.js"></script>
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/trim.js"></script>
<script language="javascript" src="../../js/cms_ajax.js"></script>
<script language="javascript">
var d = new dTree('d');
var domain_arr={};
$(document).ready(function(){
	$(".closebtn").click(function(){
		window.parent.close_select();
	});
	//	alert("<?php echo $g_core_url;?>?func=get_core_net_domain");
	//		getAjax("<?php echo $g_core_url;?>?func=get_core_net_domain",get_tree_data,"xml");
	d.clearCookie();
	get_tree_data('<?php echo $data;?>');
});
//处理AJAX返回的XML数据
function get_tree_data(domain_xml){

	d.add(0,-1,'domain',"javascript:select_tree_item('1000','domain','0');");
	$(domain_xml).find("category").each(function(){
		var id = $(this).attr("id");
		var name = $(this).attr("name");
		var domain = $(this).attr("net_domain");
		var parent = $(this).attr("parent");
		if (domain_arr.hasOwnProperty(parent)){
			domain_arr[id]=domain_arr[parent]+"."+domain;
		}else{
			domain_arr[id]=domain;
		}
		d.add(id,parent,name,"javascript:select_tree_item('"+id+"','"+name+"','"+parent+"');");

	});
	d.closeAll();

	$(".category_tree").html(d.toString());
}

function select_tree_item(item_id,item_name,parent_id){
	$(".domain_id").html(domain_arr[item_id]);
}
function set_domain_id(){
	window.parent.set_domain($(".domain_id").html());
}


</script>
</head>

<body  style='width:400px;'>
<div class="content selectcontrol" style="padding:0px; margin:0px;">
<div class="closebtn"><img src="../../images/topicon_08-topicon.png" /></div>

		<div class="category_editbox" style="width:370px; margin:0px;">
			 <div class="content_table">
			<table width="370" border="0" cellspacing="0" cellpadding="0">
			<tbody>
				<tr>
							<td><?php echo cms_get_lang('ipqam_sjip_domain');?></td>
							<td class="domain_id"></td>
							<td><input type="button" value="<?php echo cms_get_lang('confirm_search_xz');?>" onclick="set_domain_id();"/></td>
					</tr>
				  </tbody>
				  </table>
				</div>

		</div>

		 <div class="category_tree" style="overflow:auto;width:370px; height:200px; margin:0px;">

		</div>

		<div style="clear:both;"></div>
	</div>
</div>
</body>
</html>