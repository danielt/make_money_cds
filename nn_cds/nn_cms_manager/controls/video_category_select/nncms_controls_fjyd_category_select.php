<?php
header("Content-Type:text/html;charset=utf-8");
ini_set('display_errors',1);
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";

//获取权限检查类
include $nncms_db_path . "nns_pri/nns_db_pri_class.php";

$checkpri = new nns_db_pri_class();
$pri_bool = false;
$pri_bool = $checkpri->nns_db_pri_check($_SESSION["nns_role_pris"], "135006");
$checkpri = null;



$nns_id = $_GET["nns_id"];
$view_type = $_GET["view_type"];
$group_name = $_GET["group_name"];
if (!$pri_bool) {
        Header("Location: ../nncms_content_wrong.php");
        exit;
}

// echo $nncms_db_path. "nns_role/nns_db_role_class.php";
require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";


$base_id = 10000;
if (!empty($_COOKIE["page_max_num"])) {
        $g_manager_list_max_num = $_COOKIE["page_max_num"];
}
$sp_id = $_GET['sp_id'];
include_once $nncms_config_path . 'mgtv_v2/mgtv_init.php';
//include_once $nncms_config_path . 'mgtv/models/asset_category_model.php';
$assetCategory = sp_category_model::get_asset_category_list($sp_id);//get_asset_category(null, array('nns_sort' => ''),$sp_id);
$baseName = '栏目';
$baseId = 10000;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/dtree.js"></script>
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/trim.js"></script>
<script language="javascript">




var now_select_id;
$(document).ready(function(){
	$(".closebtn").click(function(){
		window.parent.close_select();
	});



	$(".selectbox").hide();
		$(".selectbox").css("left",($("body").width()-$(".selectbox").width())/2);
		$(".selectbox").css("top",10);

		$("#nns_carrier").change(function(){
			var $obj=$(this).find("option:selected");
			var index=$("#nns_carrier option").index($obj);
			selectCarrier(index);
		});
		$("#nns_method").change(function(){
			var $obj=$(this).find("option:selected");
			var index=$("#nns_method option").index($obj);
			selectMethod(index);
		})
});

function selectMethod(index){
	switch(index){
		case 0:
		index_str="vod";
		break;
		case 1:
		index_str="live";
		break;
	}
	window.location.href="nncms_controls_video_category_select.php?org_id=<?php echo $nns_org_id;?>&org_type=<?php echo $nns_org_type;?>&method="+index_str;
}




function select_tree_item(item_id,item_name,parent_id){


	$(".category_editbox").find(".category_edit_id").html(item_id);
	$(".category_editbox").find(".category_edit_name").html(item_name);
		$(".video_frame").show();
		now_select_id=item_id;

}



function selectCarrier(num){

		switch(num){
			case 1:
			begin_select_partner();
			break;
			case 2:
			begin_select_group();
			break;
			case 0:
			window.location.href="nncms_controls_video_select.php?org_id=<?php echo $carrier_data["nns_id"];?>&org_type=0&method=<?php echo $method;?>";
			break;
		}
	}

	function begin_select_partner(){
		$("#select_frame").attr("src","../nncms_controls_select.php?method=partner");
		$(".selectbox").show();
	}

	function begin_select_group(){
		$("#select_frame").attr("src","../nncms_controls_select.php?method=group");
		$(".selectbox").show();
	}
	function close_select(){
		$(".selectbox").hide();
	}

	function set_partner_id(value,name){
		window.location.href="video_select/nncms_controls_video_select.php?org_id="+value+"&org_type=1&method=<?php echo $method;?>";
	}

	function set_move_category_id(){
   
                           <?php if(isset($_GET['ismzb'])) { ?>
		window.parent.set_move_category_id_mzb(now_select_id,'<?php echo $data[0]["nns_id"];?>',$(".category_edit_name").html());
                           <?php }else{ ?>
                                   window.parent.set_move_category_id(now_select_id,'<?php echo $data[0]["nns_id"];?>',$(".category_edit_name").html());
                           <?php } ?>
	}

	function set_group_id(value,name){
		window.location.href="video_select/nncms_controls_video_select.php?org_id="+value+"&org_type=2&method=<?php echo $method;?>";
	}
	function check_select_height(){
		$(".selectbox iframe").height($(".selectbox iframe").contents().find(".selectcontrol").height());
		//window.parent.check_select_height();
	}
</script>
</head>

<body style='width:400px;'>
<div class="selectbox">
    	<iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
 </div>
<div class="content selectcontrol" style="padding:0px; margin:0px;">
<div class="closebtn"><img src="../../images/topicon_08-topicon.png" /></div>

	<div style="background-color:#FFF;">
	    <div class="category_editbox" style="width:370px; margin:0px;">
		     <div class="content_table">
	    	<table width="370" border="0" cellspacing="0" cellpadding="0">
	        <tbody>
	    		<tr>
			                <td><?php echo cms_get_lang('lmgl_nrlmid');?></td>
			                <td class="category_edit_id"></td>
			                <td><?php echo cms_get_lang('lmgl_lmmc');?></td>
			                <td  class="category_edit_name"></td>
			      	</tr>
			          <tr>
				           <td><!--<b><?php echo cms_get_lang('service_pklx');?></b>--></td>
		                	<td><!--<select name="nns_method" id="nns_method"  style=" padding:2px; width:60px;"><br>
		  						<option value="0"  <?php if ($method=="vod"){?>selected="selected"<?php }?>><?php echo cms_get_lang('media_db');?></option>
		  						<?php if ($g_live_enabled=="1"){?>
		  						<option value="1"  <?php if ($method=="live"){?>selected="selected"<?php }?>><?php echo cms_get_lang('media_zb');?></option>
		  						<?php }?>
							</select>-->

						</td>
			                <td><?php echo  cms_get_lang('action');?></td>
			                <td><input type="button" value="<?php echo cms_get_lang('confirm_search_xz');?>" onclick="set_move_category_id();"/></td>

			      	</tr>


			      </tbody>
			      </table>
			    </div>

	    </div>
	      <div class="category_tree" style="overflow:auto;width:370px; height:200px; margin:0px;">

	    	<?php
            echo sp_category_model::build_tree_use_array($assetCategory, $baseName, $baseId);
            ?>
	    </div>

	    <div style="clear:both;"></div>
    </div>
</div>
</body>
</html>