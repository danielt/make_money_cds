<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
ob_start();
session_start();
error_reporting(E_ALL ^ E_NOTICE);
include("../../nncms_manager_inc.php");

//获取权限检查类
//导入语言包
if (isset($_SESSION["language_dir"])) {
        $language_dir = $_SESSION["language_dir"];
}
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language.php";
include $nncms_config_path . "nn_cms_manager/languages/" . $language_dir . "/language_media.php";
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');

$method = $_GET["method"];
$nns_org_id = $_GET["org_id"];
$nns_org_type = $_GET["org_type"];
$selected = $_GET["selected"];
$packages_str = $_GET["packages"];
if (!empty($_COOKIE["page_max_num"])) {
        $g_manager_list_max_num = $_COOKIE["page_max_num"];
}
include ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
include($nncms_db_path . "nns_common/nns_db_pager_class.php");

include_once LOGIC_DIR . 'topic' . DIRECTORY_SEPARATOR . 'topic.class.php';
include_once LOGIC_DIR . 'web_ex' . DIRECTORY_SEPARATOR . 'web_ex.class.php';
$dc = nl_get_dc(array('db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        )
);
$service_right = g_cms_config::get_g_config_value('g_service_enabled');
$method_str = $language_media_mt;
$currentpage = 1;
if ($method == "asset") {
        $action_str = cms_get_lang('assist');
        include($nncms_db_path . "nns_assist/nns_db_assist_class.php");
        $asset_inst = new nns_db_assist_class();
        $countArr = $asset_inst->nns_db_assist_count($nns_org_id, $nns_org_type);
        if ($countArr["ret"] == 0)
                $total_num = $countArr["data"][0]["num"];
        $pages = ceil($total_num / $g_manager_list_max_num);
        if (!empty($_GET["page"]))
                $currentpage = $_GET["page"];
        $data_array = $asset_inst->nns_db_assist_list($nns_org_id, $nns_org_type, null, ($currentpage - 1) * $g_manager_list_max_num, $g_manager_list_max_num);
        $data = $data_array["data"];
        array_unshift($data, array(
            'nns_id' => 'All',
            'nns_name' => '全部',
            'nns_category_root_id' => '1000',
        ));
        $asset_inst = null;
}elseif ($method == "service") {
        $action_str = cms_get_lang('bkgl_bk');
        include($nncms_db_path . "nns_service/nns_db_service_class.php");
        $service_inst = new nns_db_service_class();
        $countArr = $service_inst->nns_db_service_count($nns_org_id, $nns_org_type);
        if ($countArr["ret"] == 0)
                $total_num = $countArr["data"][0]["num"];
        $pages = ceil($total_num / $g_manager_list_max_num);
        if (!empty($_GET["page"]))
                $currentpage = $_GET["page"];
        $data_array = $service_inst->nns_db_service_list($nns_org_id, $nns_org_type, ($currentpage - 1) * $g_manager_list_max_num, $g_manager_list_max_num);
        $data = $data_array["data"];
        $service_inst = null;
}elseif ($method == "info") {
        $dc->open();
        $total_num = nl_topic::count_topic_list($dc, $nns_org_type, $nns_org_id);
        $pages = ceil($total_num / $g_manager_list_max_num);
        if (!empty($_GET["page"]))
                $currentpage = $_GET["page"];
        $data = nl_topic::get_topic_list($dc, $nns_org_type, $nns_org_id, ($currentpage - 1) * $g_manager_list_max_num, $g_manager_list_max_num);
}elseif ($method == "web") {
        $dc->open();
        $data = nl_web_ex::select($dc);
} elseif ($method == "video") {
        $html = '<iframe id="select_frame" scrolling="no" frameborder="0" style="width:760px; height: 500px;" src="../video_select/nncms_controls_video_select_index.php?org_id=7173fca93fbd535824299963a6f88e39&org_type=0&method=vod&more_select=0&except_method=assist&except_detail_id=TVseries&except_id=1000&locktype"></iframe>';
}



// var_dump($partner_array);
if ($data_array["ret"] != 0) {
        $data = null;
        echo "<script>alert(" . $data_array["reason"] . ");</script>";
}


$url = "?";
$url.="method=$method&more_select=$more_select&org_id=$nns_org_id&org_type=$nns_org_type&packages=$packages_str";
$pager = new nns_db_pager_class($total_num, $g_manager_list_max_num, $currentpage, $url, true);
$packages = explode(',', $packages_str);
ob_end_clean();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                <link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../../js/table.js.php"></script>
                <script language="javascript" src="../../js/trim.js"></script>
                <script language="javascript">
                        $(document).ready(function() {
                                $("#nns_type").change(function() {
                                        window.location.href = "?method=" + $("#nns_type").val() + "&org_id=<?php echo $nns_org_id; ?>&org_type=<?php echo $nns_org_type; ?>&packages=<?php echo $packages_str; ?>";
                                })
                                $(".closebtn").click(function() {
                                        window.parent.close_select();
                                });

                                $(".content_table table tbody tr").removeClass("select");
                                $(".selecttr:eq(0)").addClass("select");
                                $(".category_edit_id").html($(".selecttr:eq(0)").find("td:eq(1)").html());
                                $(".category_edit_name").html($(".selecttr:eq(0)").find("td:eq(2)").html());
                        })
                        _selectfunc = select_table_tr;
                        function select_table_tr($obj) {
                                $(".content_table table tbody tr").removeClass("select");
                                $obj.addClass("select");

                                $(".category_edit_id").html($obj.find("td:eq(1)").html());
                                $(".category_edit_name").html($obj.find("td:eq(2)").html());
                                window.parent.set_package_id($obj.find("td:eq(1)").html(), "<?php echo $method; ?>", $obj.find("td:eq(2)").html(), $obj.attr('url'));
                        }
                </script>
        </head>

        <body>

                <div class="content selectcontrol" style="padding:0px; margin:0px;">
                        <div class="closebtn"><img src="../../images/topicon_08-topicon.png" /></div>
                        <div style="background-color:#FFF;">
                                <div style="margin:0px; ">
                                        <div class="content_table">
                                                <table width="370" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                                <tr>
                                                                        <td><?php echo $action_str . "ID"; ?></td>
                                                                        <td class="category_edit_id"></td>
                                                                        <?php if (empty($selected) && count($packages) > 1) { ?>
                                                                                <td><?php echo cms_get_lang('type_choice'); ?></td>
                                                                                <td><select name="nns_type" id="nns_type"  style="margin:5px 0px;">
                                                                                                <?php
                                                                                                foreach ($packages as $packet) {
                                                                                                        switch ($packet) {
                                                                                                                case 'asset':
                                                                                                                        $packet_name = cms_get_lang('assist');
                                                                                                                        break;
                                                                                                                case 'service':
                                                                                                                        $packet_name = cms_get_lang('bkgl_bk');
                                                                                                                        break;
                                                                                                                case 'info':
                                                                                                                        $packet_name = cms_get_lang('topic_xxb');
                                                                                                                        break;
                                                                                                                case 'buss':
                                                                                                                        $packet_name = cms_get_lang('contorl_ywb');
                                                                                                                        break;
                                                                                                                case 'web':
                                                                                                                        $packet_name = cms_get_lang('contorl_WEBkz');
                                                                                                                        break;
                                                                                                                case 'video':
                                                                                                                        $packet_name = "视频";
                                                                                                                        break;
                                                                                                        }
                                                                                                        if (!$service_right && $packet == 'service')
                                                                                                                continue;
                                                                                                        ?>
                                                                                                        <option value="<?php echo $packet; ?>" <?php
                                                                                                        if ($method == $packet) {
                                                                                                                echo 'selected';
                                                                                                        }
                                                                                                        ?>><?php echo $packet_name; ?></option>
                                                                                                        <?php } ?>
                                                                                        </select></td>
                                                                        <?php } ?>
                                                                </tr>



                                                        </tbody>
                                                </table>
                                        </div>
                                </div>


                                <?php if (empty($html)) { ?>
                                        <div class="content_table formtable selecttable">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <thead>
                                                                <tr>
                                                                        <th><?php echo cms_get_lang('segnumber'); ?></th>
                                                                        <th><?php echo cms_get_lang('id'); ?></th>
                                                                        <th><?php echo cms_get_lang('name'); ?></th>
                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                                <?php
                                                                if (is_array($data)) {
                                                                        $num = ($currentpage - 1) * $g_manager_list_max_num;
                                                                        foreach ($data as $item) {
                                                                                $num++;
                                                                                ?>
                                                                                <tr  class="selecttr" url="<?php if (isset($item['nns_web_url'])) echo $item['nns_web_url']; ?>">
                                                                                        <td><?php echo $num; ?></td>
                                                                                        <td><?php echo $item["nns_id"]; ?></td>
                                                                                        <td><?php echo $item["nns_name"]; ?></td>
                                                                                </tr>
                                                                                <?php
                                                                        }
                                                                } $least_num = $listnum - count($data);
                                                                for ($i = 0; $i < $least_num; $i++) {
                                                                        ?>
                                                                        <tr>
                                                                                <td>&nbsp;</td>
                                                                                <td>&nbsp; </td>
                                                                                <td>&nbsp; </td>
                                                                        </tr>

                                                                <?php } ?>
                                                        </tbody>
                                                </table>
                                        </div>
                                        <?php echo $pager->nav(); ?>
                                        <?php
                                } else {
                                        echo $html;
                                }
                                ?>
                        </div>
                </div>
        </body>
</html>