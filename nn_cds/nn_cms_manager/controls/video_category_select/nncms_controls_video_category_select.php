<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../../nncms_manager_inc.php");
//获取权限检查类
session_start();
//导入语言包
 if (isset($_SESSION["language_dir"])){
 	$language_dir=$_SESSION["language_dir"];
 }
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language_media.php";
include $nncms_config_path. "nn_cms_manager/controls/nncms_controls_public_function.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
 $method=$_GET["method"];
 $nns_org_id=$_GET["org_id"];
 $nns_org_type=$_GET["org_type"];


 $nns_url="&org_id=".$nns_org_id."&org_type=".$nns_org_type;
  $currentpage=1;
 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
 $method_str=$language_media_mt;
 	if ($method=="vod"){
 		$method_param=0;
 	}else if ($method=="live"){
 		$method_param=1;
 	}
 	 include($nncms_db_path. "nns_depot/nns_db_depot_class.php");
	 $depot_inst=new nns_db_depot_class();
	 $depot_array=$depot_inst->nns_db_depot_list("",$nns_org_type,$nns_org_id,$method_param);
	// var_dump($partner_array);
	 if ($depot_array["ret"]!=0){
	 	$data=null;
	 	echo "<script>alert(". $depot_array["reason"].");</script>";
	 }
	 $depot_inst=null;
	 $data=$depot_array["data"];

	 include $nncms_db_path. "nns_carrier/nns_db_carrier_class.php";
	 include $nncms_db_path. "nns_partner/nns_db_partner_class.php";
		 include $nncms_db_path. "nns_group/nns_db_group_class.php";


	 	if ($_SESSION["nns_manager_type"]==0){
	 		$carrier_inst=new nns_db_carrier_class();
		 	 $carrier_result=$carrier_inst->nns_db_carrier_list();

			 if ($carrier_result["ret"]==0){
			 	$carrier_data=$carrier_result["data"][0];
			 	$base_name=$carrier_data["nns_name"];
			 }
	 	}

//		 	类型为合作伙伴
		if ($nns_org_type==1){
		$partner_inst=new nns_db_partner_class();
		 $partner_result=$partner_inst->nns_db_partner_info($nns_org_id);

		 if ($partner_result["ret"]==0){
		 	$partner_data=$partner_result["data"][0];
		 	$base_name=$partner_data["nns_name"];
		 }
	//	类型为集团
		}else if ($nns_org_type==2){
			$group_inst=new nns_db_group_class();
			 $group_result=$group_inst->nns_db_group_info($nns_org_id);

			 if ($group_result["ret"]==0){
			 	$group_data=$group_result["data"][0];
			 	$base_name=$group_data["nns_name"];
			 }
		}


	 $base_id=10000;



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../css/dtree.css" type="text/css" />
<link href="../../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/dtree.js"></script>
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/trim.js"></script>
<script language="javascript">




var now_select_id;
$(document).ready(function(){
	$(".closebtn").click(function(){
		window.parent.close_select();
	});



	$(".selectbox").hide();
		$(".selectbox").css("left",($("body").width()-$(".selectbox").width())/2);
		$(".selectbox").css("top",10);

		$("#nns_carrier").change(function(){
			var $obj=$(this).find("option:selected");
			var index=$("#nns_carrier option").index($obj);
			selectCarrier(index);
		});
		$("#nns_method").change(function(){
			var $obj=$(this).find("option:selected");
			var index=$("#nns_method option").index($obj);
			selectMethod(index);
		})
});

function selectMethod(index){
	switch(index){
		case 0:
		index_str="vod";
		break;
		case 1:
		index_str="live";
		break;
	}
	window.location.href="nncms_controls_video_category_select.php?org_id=<?php echo $nns_org_id;?>&org_type=<?php echo $nns_org_type;?>&method="+index_str;
}




function select_tree_item(item_id,item_name,parent_id){


	$(".category_editbox").find(".category_edit_id").html(item_id);
	$(".category_editbox").find(".category_edit_name").html(item_name);
		$(".video_frame").show();
		now_select_id=item_id;

}



function selectCarrier(num){

		switch(num){
			case 1:
			begin_select_partner();
			break;
			case 2:
			begin_select_group();
			break;
			case 0:
			window.location.href="nncms_controls_video_select.php?org_id=<?php echo $carrier_data["nns_id"];?>&org_type=0&method=<?php echo $method;?>";
			break;
		}
	}

	function begin_select_partner(){
		$("#select_frame").attr("src","../nncms_controls_select.php?method=partner");
		$(".selectbox").show();
	}

	function begin_select_group(){
		$("#select_frame").attr("src","../nncms_controls_select.php?method=group");
		$(".selectbox").show();
	}
	function close_select(){
		$(".selectbox").hide();
	}

	function set_partner_id(value,name){
		window.location.href="video_select/nncms_controls_video_select.php?org_id="+value+"&org_type=1&method=<?php echo $method;?>";
	}

	function set_move_category_id(){
   
                           <?php if(isset($_GET['ismzb'])) { ?>
		window.parent.set_move_category_id_mzb(now_select_id,'<?php echo $data[0]["nns_id"];?>',$(".category_edit_name").html());
                           <?php }else{ ?>
                                   window.parent.set_move_category_id(now_select_id,'<?php echo $data[0]["nns_id"];?>',$(".category_edit_name").html());
                           <?php } ?>
	}

	function set_group_id(value,name){
		window.location.href="video_select/nncms_controls_video_select.php?org_id="+value+"&org_type=2&method=<?php echo $method;?>";
	}
	function check_select_height(){
		$(".selectbox iframe").height($(".selectbox iframe").contents().find(".selectcontrol").height());
		window.parent.check_select_height();
	}
</script>
</head>

<body style='width:400px;'>
<div class="selectbox">
    	<iframe scrolling="no" frameborder="0" id="select_frame" ></iframe>
 </div>
<div class="content selectcontrol" style="padding:0px; margin:0px;">
<div class="closebtn"><img src="../../images/topicon_08-topicon.png" /></div>

	<div style="background-color:#FFF;">


	    <div class="category_editbox" style="width:370px; margin:0px;">
		     <div class="content_table">
	    	<table width="370" border="0" cellspacing="0" cellpadding="0">
	        <tbody>
	    		<tr>
			                <td><?php echo cms_get_lang('lmgl_nrlmid');?></td>
			                <td class="category_edit_id"></td>
			                <td><?php echo cms_get_lang('lmgl_lmmc');?></td>
			                <td  class="category_edit_name"></td>
			      	</tr>
			          <tr>
				           <td><!--<b><?php echo cms_get_lang('service_pklx');?></b>--></td>
		                	<td><!--<select name="nns_method" id="nns_method"  style=" padding:2px; width:60px;"><br>
		  						<option value="0"  <?php if ($method=="vod"){?>selected="selected"<?php }?>><?php echo cms_get_lang('media_db');?></option>
		  						<?php if ($g_live_enabled=="1"){?>
		  						<option value="1"  <?php if ($method=="live"){?>selected="selected"<?php }?>><?php echo cms_get_lang('media_zb');?></option>
		  						<?php }?>
							</select>-->

						</td>
			                <td><?php echo  cms_get_lang('action');?></td>
			                <td><input type="button" value="<?php echo cms_get_lang('confirm_search_xz');?>" onclick="set_move_category_id();"/></td>

			      	</tr>


			      </tbody>
			      </table>
			    </div>

	    </div>
	      <div class="category_tree" style="overflow:auto;width:370px; height:200px; margin:0px;">

	    	<?php
			if ($data!=null){
				$assist_content=$data[0]["nns_category"];
			}else{
				$assist_content=null;
			}
			echo	pub_func_get_tree_by_html($assist_content,$base_name,$base_id);

	        ?>
	    </div>

	    <div style="clear:both;"></div>
    </div>
</div>
</body>
</html>