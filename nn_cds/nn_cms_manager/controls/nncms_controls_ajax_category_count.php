<?php
/*
 * Created on 2012-8-29
 * author:bo.chen@starcorcn.com
 * 用于栏目树的AJAX 统计
 */
 ini_set('display_errors','Off');
 include("../nncms_manager_inc.php");
 include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
  /**
  * status 请求状态
  * data   返回结果集
  * $result 根据业务需求可以自定义扩展
  */
 $result  = array('status'=>true,'reason'=>'ok','data'=>'');
 
 /**
  * 统计类型
  * category    影片分类的影片数量统计
  */
 $type = isset($_REQUEST['type'])?$_REQUEST['type']:'';
 //栏目树节点ID
 $tree_id = isset($_REQUEST['tree_id'])?$_REQUEST['tree_id']:'';
 //分类ID list
 $category_id = isset($_REQUEST['category_list'])?$_REQUEST['category_list']:'';
 $category_name = isset($_REQUEST['category_name'])?$_REQUEST['category_name']:'';
 $category_id_array = explode(',',$category_id);
 $category_name_array = explode(',',$category_name);
 $category_list = null;
 foreach($category_id_array as $key=>$val){
 	if(empty($val) && $key !=0){
 		unset($category_id_array[$key]);
 	}else{
 		$category_list[$val] = $category_name_array[$key];	
 	}
 	
 }
 $category_id_array[0] = 1000;

 if(empty($type)){
 	$result['reason'] = cms_get_lang('cscw').'！';
 	$result['status'] = false;
 	echo json_encode($result);exit;
 //直播分类影片统计
 }
if($type == 'vod'){
	$category_id_array[0] = 10000;
 	//vod model引入
 	include($nncms_db_path. "nns_vod/nns_db_vod_class.php");
 	$vod_inst=new nns_db_vod_class();
 	$like_sql = db_where_like($category_id_array ,'nns_category_id');
 	$sql = 'select nns_category_id AS category_id,count(1) as num from nns_vod where ('.$like_sql.') and nns_depot_id=\''.$tree_id.'\' and nns_deleted =0 group by nns_category_id';
 	
 	$data = $vod_inst->nns_db_vod_category_count($sql);
 	
 }else if($type == 'live'){
 	$category_id_array[0] = 10000;
 	//live model引入
 	include($nncms_db_path. "nns_live/nns_db_live_class.php");
 	$live_inst=new nns_db_live_class();
 	$like_sql = db_where_like($category_id_array ,'nns_category_id');
 	$sql = 'select nns_category_id AS category_id,count(1) as num from nns_live where ('.$like_sql.') and nns_depot_id=\''.$tree_id.'\' and nns_deleted =0 group by nns_category_id';
 	$data = $live_inst->nns_db_live_category_count($sql);
 	
 }else if($type == 'service'){
	//service model引入
 	include($nncms_db_path. "nns_service/nns_db_service_category_class.php");
 	$service_category_inst = new nns_db_service_category_class();
 	$like_sql = db_where_like($category_id_array ,'nns_category_id');
 	$sql ='select nns_category_id as category_id,count(1) as num from nns_service_item where ('.$like_sql.') and nns_service_id=\''.$tree_id.'\' group by nns_category_id ';
 	//echo $sql;exit; 
 	$data = $service_category_inst->nns_db_service_category_count2($sql);
 	
 }else if($type == 'assist'){
 	//assist model引入
 	include($nncms_db_path. "nns_assist/nns_db_assist_item_class.php");
	$assist_category_inst=new nns_db_assist_item_class();
	$like_sql = db_where_like($category_id_array ,'nns_category_id');
	$sql = 'select nns_category_id as category_id,count(1) as num from nns_assists_item where ('.$like_sql.')   and  nns_assist_id=\''.$tree_id.'\' and nns_check=1  group by nns_category_id ';
	$data = $assist_category_inst->nns_db_assist_category_count($sql);
	//print_r($data);exit;
 }else if($type == 'topic'){
 	include $nncms_config_path.'nn_logic/topic/topic.class.php';
 	include $nncms_config_path.'nn_logic/topic/topic_item.class.php';
 	$dc = nl_get_dc(array(
			'db_policy'=>NL_DB_READ,
			'cache_policy'=>NP_KV_CACHE_TYPE_NULL
			)
		);
	$dc->open(); 	
 	$i=0;
 	$data_1[$i]["category_id"]='1000';
 	$params["topic_id"]=$tree_id;
 	$params["category_id"]='1000';
 	$data_1[$i]["num"]=nl_topic_item::count_topic_item_list($dc, $params);
	$i++;
	$category_ids=nl_topic::get_son_category_by_category_id($dc,$tree_id,null,null);
	if(is_array($category_ids)){
 	foreach($category_ids as $item_id){
 		$data_1[$i]["category_id"]=$item_id["id"];
 		$params["topic_id"]=$tree_id;
 		$params["category_id"]=$item_id["id"];
 		$data_1[$i]["num"]=nl_topic_item::count_topic_item_list($dc, $params);
 			$i++;
 	}}
 	$data['data'] =$data_1;
 	$data["ret"]=0;
 	$data["reason"]='mysql query ok!';
 	
}else if($type == 'assest_label'){
	include $nncms_config_path . 'nn_logic/asset_label/asset_label_bind.class.php';
 	$dc = nl_get_dc(array(
			'db_policy'=>NL_DB_READ,
			'cache_policy'=>NP_KV_CACHE_TYPE_NULL
			)
		);
	$dc->open();
	$data['data'] = nl_asset_label_bind::get_assest_label_count($dc,$category_id_array,$tree_id);
	$data["ret"]=0;
 	$data["reason"]='mysql query ok!';
// 	print_r($result);exit;
	
}
 	$category_array = array();
 //如果执行sql失败
 	if(isset($data['ret']) && $data['ret']){
 		$result['status'] = false;
 		$result['reason'] =cms_get_lang('contorl_mysql_yc');
 		echo json_encode($result);exit;
 	}
	
 	foreach($data['data']  as $key=>$val){
 		$category_array[$val['category_id']] = $val;
 	}

 	foreach($category_list as $key => $val){
 		if(!array_key_exists($key,$category_array)){
 			$category_array[$key]['category_id'] = $key;
 			$category_array[$key]['num'] = 0;
 		}
 	}
 	
 	$data['data'] = $category_array;
 	$total = 0;
 	if(isset($data['data']) && $data['data']){
 		//分类数据叠加
 		foreach($data['data'] as $key=>$val){
 			foreach($data['data'] as $k=>$v){
 				//如果key 相等不需要叠加影片数据
 				if($key == $k){
 					continue;
 				}
 				//判断是否为$v['category_id'] 是否为$val['category_id']的父节点
 				$pos = strpos(trim($v['category_id']),trim($val['category_id']));
 				if($pos === 0){
 					$data['data'][$key]['num'] += $v['num'];
 				}
 			}
			$total += $val['num'];
 		}
	 	foreach($data['data'] as $key=>$val){
	 		if(array_key_exists($val['category_id'],$category_list) && $val['category_id'] !=0){
	 			$category_list[$val['category_id']] .= '[<span style="color:#ff0000">'.$val['num'].'</span>]';
	 			
	 		}
	 	}
 	}

 	//判断父节点下面是否绑定有影片。
 	if($data['data'][0]['category_id']== $category_id_array[0]){
 		$category_list[0] .=  '[<span style="color:#ff0000">'.$data['data'][0]['num'].'</span>]';
 	}else{
 		$category_list[0] .=  '[<span style="color:#ff0000">'.$total.'</span>]';
 	}
	
	//没有绑定影片的默认0
 	foreach($category_list  as $key=>$val){
 		 if(!strstr($val,'span')){
 			$category_list[$key] .= '[<span style="color:#ff0000">0</span>]';
 		}
 	}
 	
	$result['data'] = $category_list;
 	echo json_encode($result);exit;
 
 
 function db_where_like($array = array(),$filed = null){
 	
 	if(empty($array) || empty($filed)){
 		return;
 	}
 	$where = '';
 	foreach($array as $key=>$val){
 		if(empty($where)){
 			$where = $filed.' LIKE \''.$val.'%\'';
 		}else{
 			$where .= ' OR '.$filed.' LIKE \''.$val.'%\'';
 		}
 	}
 	return $where;
 }
?>

