<?php
/*
 * Created on 2012-6-12
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 ob_start();
  error_reporting(E_ALL ^ E_NOTICE);
 require_once("../nncms_manager_inc.php");
 require_once $nncms_db_path. "nns_vod/nns_db_vod_index_class.php";
 require_once $nncms_db_path. "nns_vod/nns_db_vod_media_class.php";
require_once $nncms_db_path. "nns_common/nns_db_class.php";
require_once $nncms_db_path. "nns_common/nns_db_guid_class.php";
  
 $method=$_GET["method"];
 $accept_video_id=$_GET["accept_video_id"];
 $import_video_id=$_GET["import_video_id"];
 $accept_index=$_GET["accept_index"];
 $result=array();
 $db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
$conn = $db_obj->nns_db_connect ();
 $index_inst=new nns_db_vod_index_class();
 $media_inst=new nns_db_vod_media_class();
 $media_check=true;
 	$index_check=true;
 $import_check=true;
 if ($method=="check"){
 	 
// 检查移动目标是否已存在相同片源，询问是否覆盖
	$index_accept_result=$index_inst->nns_db_vod_index_info($accept_video_id,$accept_index);
	if ($index_accept_result["ret"]==0){
		$index_check=false;
	}
 $media_import_result=$media_inst->nns_db_vod_media_list(0,0,$import_video_id,0);

 if ($media_import_result["ret"]==0){
 	foreach ($media_import_result["data"] as $media_import_item){
 		$media_count=$media_inst->nns_db_vod_media_count($accept_video_id,$accept_index,null,null,null,null,null,$media_import_item["nns_filetype"],$media_import_item["nns_mode"]);
 		if ($media_count["data"][0]["num"]>0){
 			$media_check=false;
 			break;
 		}
 	}
 }else{
 	$import_check=false;
 }
}else if ($method=="import"){
 $index_import_result=$index_inst->nns_db_vod_index_info($import_video_id,0);
 $index_accept_result=$index_inst->nns_db_vod_index_info($accept_video_id,$accept_index);

 if ($index_import_result["ret"]==0){
 	$index_import_name=$index_import_result["data"][0]["nns_name"];
 	$index_import_picture=$index_import_result["data"][0]["nns_image"];
 	$index_import_timelen=$index_import_result["data"][0]["nns_time_len"];
 	$index_import_summary=$index_import_result["data"][0]["nns_summary"];
 	$result=$index_inst->nns_db_vod_index_modify($accept_video_id,$accept_index,$index_import_name,$index_import_timelen,$index_import_picture,$index_import_summary);
 	if ($result["ret"]!=0){
 		$index_check=false;
 	}else{
 		$media_import_result=$media_inst->nns_db_vod_media_list(0,0,$import_video_id,0);
		 if ($media_import_result["ret"]==0){
		 	foreach ($media_import_result["data"] as $media_import_item){
		 		$media_name=$media_import_item["nns_name"];
		 		$media_url=$media_import_item["nns_url"];
		 		$media_tag=$media_import_item["nns_tag"];
		 		$media_mode=$media_import_item["nns_mode"];
		 		$media_stream=$media_import_item["nns_kbps"];
		 		$media_filetype=$media_import_item["nns_filetype"];
		 		$media_type=$media_import_item["nns_type"];
		 		$media_content_id=$media_import_item["nns_content_id"];
		 		$media_content_state=$media_import_item["nns_content_state"];
		 		$media_count=$media_inst->nns_db_vod_media_list(0,1,$accept_video_id,$accept_index,null,null,null,null,null,$media_import_item["nns_filetype"],$media_import_item["nns_mode"]);
		 		if (count($media_count["data"])>0){
		 			$str_sql="update nns_vod_media set nns_name='$media_name',nns_tag='$media_tag',nns_url='$media_url',nns_mode='$media_mode',nns_kbps='$media_stream',nns_modify_time=now(),nns_content_id='$media_content_id',nns_content_state='$media_content_state',nns_filetype='$media_filetype' where nns_id='".$media_count["data"][0]["nns_id"]."'" ;
		 				
		 		}else{
		 			//生成vod media GUID
					$guid = new Guid ();
					$media_id = $guid->toString ();
		 			$str_sql="insert into nns_vod_media(nns_id,nns_name,nns_vod_id,nns_vod_index,nns_vod_index_id,nns_state,nns_check,nns_tag,nns_type,nns_url,nns_mode,nns_kbps,nns_create_time,nns_modify_time,nns_deleted,nns_content_id,nns_content_state,nns_filetype)values" .
		 					"('$media_id','$media_name','$accept_video_id',$accept_index,'".$index_accept_result["data"][0]["nns_id"]."',0,1,'$media_tag',$media_type,'$media_url','$media_mode','$media_stream',now(),now()," . NNS_VOD_MEDIA_DELETED_FALSE . ",'$media_content_id','$media_content_state','$media_filetype')";
		 		}
		 		$result=$db_obj->nns_db_execute($str_sql);
		 		if ($result["ret"]!=0){
		 			$media_check=false;
		 		}
		 	}
		 }
 	}
 }else{
 	 $import_check=false;
 }
}
$db_obj=null;
$index_inst=null;
$media_inst=null;
 $result["media"]=$media_check;
 $result["index"]=$index_check;
 $result["import"]=$import_check;
  ob_end_clean();
 echo json_encode($result);
?>