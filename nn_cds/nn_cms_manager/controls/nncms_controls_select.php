<?php
header("Content-Type:text/html;charset=utf-8");
error_reporting(E_ALL ^ E_NOTICE);
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include("../nncms_manager_inc.php");
//获取权限检查类
session_start();
//导入语言包
if (isset($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
}
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
// 导入多语言包
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

$method=$_GET["method"];
$search=$_GET["search"];
$nns_org_id=$_GET["org_id"];
$nns_org_type=$_GET["org_type"];
$nns_url="&org_id=".$nns_org_id."&org_type=".$nns_org_type;
$currentpage=1;
if (!empty($_GET["page"])) $currentpage=$_GET["page"];
$listnum=8;
if ($method=="partner"){
	$method_str = cms_get_lang('partner_hzhbxx');
	include($nncms_db_path. "nns_partner/nns_db_partner_class.php");
	$partner_inst=new nns_db_partner_class();

	$countArr=$partner_inst->nns_db_partner_count($search);
	if ($countArr["ret"]==0)
		$partner_total_num=$countArr["data"][0]["num"];
	$group_pages=ceil($partner_total_num/$listnum);

	$partner_array=$partner_inst->nns_db_partner_list($search,($currentpage-1)*$listnum,$listnum);
	// var_dump($role_array);
	if ($partner_array["ret"]!=0){
		$data=null;
		echo "<script>alert('". $partner_array["reason"]."');</script>";
	}
	$data=$partner_array["data"];
}else if ($method=="group"){
	$data = null;
	//取用户绑定集团
	if(!empty($_GET['user_id'])){
		if($_GET['nobind']==1){//取用户没有绑定的集团
			include($nncms_db_path. "nns_user/nns_db_user_group_class.php");
			$model = new nns_db_user_group_class();
			$data =$model->get_nobind_group($_GET['user_id']);
		}else{
			include($nncms_db_path. "nns_user/nns_db_user_group_class.php");
			$model = new nns_db_user_group_class();
			$data =$model->get_group_by_user_id($_GET['user_id']);
		}

		//var_dump($data);
	}else{//取所有集团
		$method_str = cms_get_lang('group_jtxx');
		include($nncms_db_path. "nns_group/nns_db_group_class.php");
		$group_inst=new nns_db_group_class();
		$countArr=$group_inst->nns_db_group_count($search);
		if ($countArr["ret"]==0)
			$group_total_num=$countArr["data"][0]["num"];
		$group_pages=ceil($group_total_num/$listnum);

		$group_array=$group_inst->nns_db_group_list($search,($currentpage-1)*$listnum,$listnum);
		// var_dump($role_array);
		if ($group_array["ret"]!=0){
			$data=null;
			echo "<script>alert('". $group_array["reason"]."');</script>";
		}
		$data=$group_array["data"];
	}
}
else if ($method=="common_product"){
	$method_str = cms_get_lang('cpgl_cplb');

	include($nncms_db_path. "nns_products/nns_db_products_class.php");
	$products_inst=new nns_db_products_class();
	$user_total_num=$products_inst->count_single_video_model_produnct($search,0);

	$group_pages=ceil($user_total_num/$listnum);
	$data  = $products_inst->get_single_video_model_produnct_list($search, ($currentpage-1)*$listnum,$listnum,0);
}
else if ($method=="user"){
	$method_str = cms_get_lang('zdgl_yhlb');

	include($nncms_db_path. "nns_user/nns_db_user_class.php");
	$user_inst=new nns_db_user_class();
	$countArr=$user_inst->nns_db_user_count(null,$search,$nns_org_type,$nns_org_id);
	if ($countArr["ret"]==0)
		$user_total_num=$countArr["data"][0]["num"];
	$group_pages=ceil($user_total_num/$listnum);
	$user_array=$user_inst->nns_db_user_list(($currentpage-1)*$listnum,$listnum,null,$search,$nns_org_type,$nns_org_id);
	// var_dump($role_array);
	if ($user_array["ret"]!=0){
		$data=null;
		echo "<script>alert(". $user_array["reason"].");</script>";
	}
	$data=$user_array["data"];
}else if ($method=="product"){
	$method_str = cms_get_lang('cpgl_cplb');

	include($nncms_db_path. "nns_products/nns_db_products_class.php");
	$products_inst=new nns_db_products_class();
	$countArr=$products_inst->nns_db_products_count(1,$search,"","","",$nns_org_id,$nns_org_type,0);
	if ($countArr["ret"]==0)
		$user_total_num=$countArr["data"][0]["num"];
	$group_pages=ceil($user_total_num/$listnum);
	$products_array=$products_inst->nns_db_products_list(($currentpage-1)*$listnum,$listnum,1,$search,"","","",$nns_org_id,$nns_org_type,0);
	// var_dump($role_array);
	if ($products_array["ret"]!=0){
		$data=null;
		echo "<script>alert(". $products_array["reason"].");</script>";
	}
	$data=$products_array["data"];
}
else if ($method=="video_bind_product"){
	$method_str = cms_get_lang('cpgl_cplb');

	include($nncms_db_path. "nns_products/nns_db_products_class.php");
	$products_inst=new nns_db_products_class();
	$user_total_num=$products_inst->count_single_video_model_produnct($search);

	$group_pages=ceil($user_total_num/$listnum);
	$data  = $products_inst->get_single_video_model_produnct_list($search,($currentpage-1)*$listnum,$listnum);


}else if ($method=="product_fee"){
	$method_str = cms_get_lang('cpgl_zf');
	$product_id=isset($_GET["product_id"])?$_GET["product_id"]:'';
	if(isset($_GET["video_bind_product"]))
	{
		include($nncms_db_path. "nns_products/nns_db_products_class.php");
		$products_inst = new nns_db_products_class();
		$single_video_model_produnct = $products_inst->get_single_video_model_produnct();
		if($single_video_model_produnct) $product_id = $single_video_model_produnct['nns_id'];
	}

	$nns_url .= "&product_id=".$product_id;

	include($nncms_db_path. "nns_product_fee/nns_db_product_fee_class.php");
	$products_inst=new nns_db_product_fee_class();
	$countArr=$products_inst->nns_db_product_fee_count($product_id,null,null,$search);
	if ($countArr["ret"]==0)
		$user_total_num=$countArr["data"][0]["num"];
	$group_pages=ceil($user_total_num/$listnum);
	$products_array=$products_inst->nns_db_product_fee_list(($currentpage-1)*$listnum,$listnum,$product_id,null,null,$search);
	// var_dump($role_array);
	if ($products_array["ret"]!=0){
		$data=null;
		echo "<script>alert('". $products_array["reason"]."');</script>";
	}
	$data=$products_array["data"];
}

if ($search) {
	$nns_url_query="&search=".urlencode($search);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link href="../css/selectbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../js/table.js.php"></script>
<script language="javascript" src="../js/trim.js"></script>
<script language="javascript">
_selectfunc=select_table_tr;

$(document).ready(function(){
	$(".closebtn").click(function(){
		window.parent.close_select();
	});
})
	function select_table_tr($obj){
		window.parent.close_select();
		<?php if($method=='product'){?>
		window.parent.set_<?php echo $method;?>_id($obj.find("input:eq(0)").attr("value"),$obj.find("input:eq(1)").attr("value"));
<?php
		}else{
?>
			window.parent.set_<?php echo $method;?>_id($obj.find("input:eq(0)").attr("value"),$obj.find("td:eq(1)").html());
<?php
		}
?>

	}

function search_result(){
	var search_str=encodeURI($("#search_input_param").attr("value").trim());
	window.location.href="nncms_controls_select.php?method=<?php echo $method;?>&search="+search_str+"<?php echo $nns_url;?>";
}
</script>
</head>

<body  style='width:400px;'>
<div class="selectcontrol">
	<div class="closebtn"><img src="../images/topicon_08-topicon.png" /></div>
	<div class="selecttitle"><?php echo cms_get_lang('search_xz'). $method_str;?></div>
	<div class="searchbox"><input name="" type="text" id="search_input_param" value="<?php echo $search;?>"/>&nbsp;&nbsp;<input name="" type="button" value="<?php echo cms_get_lang('search');?>" onclick="search_result();"/></div>
	<div style="padding:5px;">
	 <div class="content_table formtable selecttable">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<th><?php echo cms_get_lang('segnumber');?></th>
				<?php if ($method=="user"){?>
				<th><?php echo cms_get_lang('user_id');?></th>
				<?php }else?>
<?php
		if ($method=="product"){?>
				<th><?php echo cms_get_lang('cpgl_cp|id');?></th>
				<?php }else?>
				 <?php if ($method=="product_fee"){?>
				<th><?php echo cms_get_lang('cpgl_zf|id');?></th>
				 <th><?php echo cms_get_lang('cpgl_cpjg');?></th>
<?php }else
		{
?>
				<th><?php echo cms_get_lang('name');?></th>
				<?php }?>
				 <?php if ($method=="single_video_bill_model_product"){?>
					 <th><?php echo cms_get_lang('cpgl_zf|id');?></th>
					 <th><?php echo cms_get_lang('cpgl_cpjg');?></th>
				 <?php }?>


			</tr>
		</thead>
		<tbody>
<?php
			if (!empty($data)){
				$num=($currentpage-1)*$listnum; foreach($data as $item){
					$num++;
?>
			  <tr class="selecttr">
			  <?php if ($method=="product_fee"){?>
			   <input type="hidden" value="<?php echo $item["nns_product_fee_id"];?>" />
			  <?php }else{?>
				<input type="hidden" value="<?php echo $item["nns_id"];?>" />
				<?php }?>

			  <?php if ($method=="product" ){?>
				<input type="hidden" value="<?php echo $item["nns_single_video_bill_mode"];?>" />
			  <?php }?>

				<td><?php echo $num;?></td>
				 <?php if ($method=="user" || $method=="product"){?>
				<td><?php echo $item["nns_id"];?></td>
				<?php }else if($method=="product_fee"){?>
					<td><?php echo $item["nns_product_fee_id"];?></td>
					<?php }?>
				<?php if ($method=="product_fee"){?>
				 <td><?php echo $item["nns_product_fee_name"];?>&nbsp;&nbsp;<?php echo "<font style='color:#ff0000;'>(".$item["nns_price"].")</font>";?></td>
				<?php }else{?>
				<td><?php echo $item["nns_name"];?></td>
				<?php }?>
				 <?php if ($method=="single_video_bill_model_product"){?>
				<td><?php echo $item["nns_product_fee_id"];?></td>
				<td><?php echo $item["nns_product_fee_name"];?></td>
				<?php }?>
			  </tr>
		  <?php }}?>
		  </tbody>
		</table>
	</div>
	 <div class="pagecontrol">
		<?php if ($currentpage>1){?>
		<a href="nncms_controls_select.php?method=<?php echo $method;?><?php echo $nns_url;?>&page=1<?php echo $nns_url_query;?>" target="_self"><?php echo cms_get_lang('first_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="nncms_controls_select.php?method=<?php echo $method;?><?php echo $nns_url;?>&page=<?php echo $currentpage-1;?><?php echo $nns_url_query;?>" target="_self"><?php echo cms_get_lang('pre_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php } if ($currentpage<$group_pages){?>
		<a href="nncms_controls_select.php?method=<?php echo $method;?><?php echo $nns_url;?>&page=<?php echo $currentpage+1;?><?php echo $nns_url_query;?>" target="_self"><?php echo cms_get_lang('next_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="nncms_controls_select.php?method=<?php echo $method;?><?php echo $nns_url;?>&page=<?php echo $group_pages;?><?php echo $nns_url_query;?>" target="_self"><?php echo cms_get_lang('last_page');?></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php }?>
		<?php if ($group_pages>1){?>
			<?php echo cms_get_lang('jump_to');?> <input name="go_page_num" id="go_page_num" value="<?php echo $currentpage;?>" type="text" style="width:20px;"/> <?php echo cms_get_lang('page');?> &nbsp;&nbsp;
			<a href="javascript:go_page_num('nncms_controls_select.php?method=<?php echo $method;?><?php echo $nns_url;?><?php echo $nns_url_query;?>',<?php echo $group_pages;?>);">GO>></a>&nbsp;&nbsp;
			<?php echo cms_get_lang('current');?><span style="font-weight:bold;color:#ff0000;"><?php echo $currentpage;?></span><?php echo cms_get_lang('page');?>&nbsp;&nbsp;
			<?php echo cms_get_lang('total');?><span style="font-weight:bold;color:#ff0000;"><?php echo $group_pages;?></span><?php echo cms_get_lang('page');?>&nbsp;&nbsp;
		<?php }?>
	</div>
	<!--<div class="btns"><input name="" type="button" value="确定"/>&nbsp;&nbsp;&nbsp;&nbsp;<input name="" type="button" value="关闭"/></div>
	</div>-->
</div>
</body>
</html>