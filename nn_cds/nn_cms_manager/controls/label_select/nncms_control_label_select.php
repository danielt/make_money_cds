<?php
/*
 * Created on 2013-1-18
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../../nncms_manager_inc.php");
 include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
 include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include ($nncms_config_path. "nn_cms_config/nn_cms_config_path.php");
include ($nncms_config_path. "nn_cms_db/nns_common/nns_db_pager_class.php");
include LOGIC_DIR. "label/video_label.class.php";
include LOGIC_DIR. "label/label_type.class.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');

$dc=nl_get_dc( Array(
'db_policy'=>NL_DB_WRITE,
'cache_policy'=>NP_KV_CACHE_TYPE_NULL
));
$dc->open();

$type_id=$_GET['label_type'];
$type_info=nl_label_type::get_label_type_by_id($dc,$type_id);
$current_page=$_GET['page'];
$search_name=$_GET['name'];
$exists_labels=$_GET['labels'];
$current_page=empty($current_page)?1:$current_page;

if(!empty($_COOKIE["page_max_num"])){
	$page_size = intval($_COOKIE["page_max_num"]);
}else if(isset($_GET['page_size']) && $_GET['page_size'] >0){
	$page_size = $_GET['page_size'];
}else{
	$page_size = $g_manager_list_max_num;
}
$params=array();
$params['type']=$type_id;
$params['source']='main';
$params['state']=1;
if (!empty($search_name))
$params['name']=$search_name;

$count=nl_video_label::count_video_label(
        			$dc,
        			$params,
        			true
        	);
$labels=nl_video_label::get_video_label_list(
        			$dc,
        			$params,
        			($current_page-1)*$page_size,
        			$page_size,
        			true,
        			NL_ORDER_CLICK_DESC,
        			NL_DC_DB
        	);
 $exists_labels=rtrim($exists_labels,',');
 $label_ids=explode(',',$exists_labels);
 $exist_labels=nl_video_label::get_label_info_by_ids($dc,$label_ids);       	
$url='nncms_control_label_select.php?label_type='.$type_id.'&name='.$search_name.'&labels='.$exists_labels;
$page=new nns_db_pager_class($count, $page_size, $current_page,$url);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/rightframestyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../js/trim.js"></script>
<script language="javascript" src="../../js/table.js.php"></script>
<script language="javascript" src="../../js/checkinput.js.php"></script>
<script language="javascript" src="../../js/cms_cookie.js"></script>
<script language="javascript">
var page_num='<?php echo $current_page;?>';
function refresh_location(){
	$search_name=encodeURI($('#label_search').val());
	$refresh_url='nncms_control_label_select.php?label_type=<?php echo $type_id;?>&name='+$search_name+'&page='+page_num+'&labels='+get_labels();
	//alert($refresh_url);
	window.location=$refresh_url;
}

$(document).ready(function(){
	$('.label_move').live('click',function(){
		$(this).remove();
		$('#'+$(this).attr("label_id")).removeClass("disabled");
	});
	$('.label_btn').click(function(){
		
		if ($(this).hasClass("disabled")) return;
		
		var	html='<div  label_id="'+$(this).attr("id")+'"  class="label_move">'+
				$(this).find(".name").html()+'</div>';
		$(this).addClass("disabled");		
		$("#top_banner").append(html);
	});
});

function get_labels(){
	var len=$('.label_move').length;
	var labels='';
	for(var i=0;i<len;i++){
		labels+=$('.label_move').eq(i).attr('label_id')+',';
	}
	
	return labels;
}


</script>
</head>

<body>
<div class="content">
	    <div class="content_table formtable">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
        	<tbody>
        	
        		<tr>
        			<td style="background:#80ACC1; color:#fff; padding:5px; line-height:26px;" id="top_banner">
        			<div style="float:left;"><b><?php echo $type_info['nns_name'];?></b></div>
        			<?php if (is_array($exist_labels)){
                			foreach ($exist_labels as $label){
                				if ($label['nns_type']==$type_id){
                		?>
                				<div  label_id="<?php echo $label['nns_id'];?>"  class="label_move"><?php echo $label['nns_name'];?></div>
                		<?php }}}?>
        			<div style="float:right;margin-right:10px;">
        				<input type="text" name="label_search" id="label_search" style="padding:4px;" value="<?php echo $search_name;?>" /> 
        				<input type="button" onclick="refresh_location();" value="<?php echo cms_get_lang('contorl_ss');?>"/>
        			</div>
        			</td>
        		</tr>
        		<tr>
        			<td style="padding:5px; background:#ffffff;">
                	<?php if (is_array($labels)){
                			foreach ($labels as $label){
                		?>
                			<div class="label_btn <?php if (array_key_exists($label['nns_id'],$exist_labels)){ echo 'disabled';}?>" id="<?php echo $label['nns_id'];?>"   >
    							<label class="name"><?php echo $label['nns_name'];?></label><span style="color:#888888">[<?php echo $label['nns_count'];?>]</span>
    						</div>
                	<?php }}?>
                		<div style="clear:both;"></div>
        			</td>
        		</tr>
        	</tbody>
        </table>
	</div>
	<?php echo $page->nav();?>
</div>
</body>
</html>