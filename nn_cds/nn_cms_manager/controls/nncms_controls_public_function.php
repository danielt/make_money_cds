<?php

/*
 * Created on 2012-8-14
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include_once dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'nn_cms_config' . DIRECTORY_SEPARATOR . 'nn_cms_global.php';
include_once LOGIC_DIR . 'image' . DIRECTORY_SEPARATOR . 'image.class.php';

//节目单图片上传
function pub_func_image_upload_by_playbill($file, $live_id, $date,$cp_config=null,$cp_id=null)
{
    if(isset($cp_config['down_img_dir']) && strlen($cp_config['down_img_dir']) >0 && strlen($cp_id) >0)
    {
        $result =  pub_func_image_upload_v2($file,$cp_config['down_img_dir'],$cp_id,'playbill');
        return (isset($result['data_info']) && strlen($result['data_info']) >0) ? $result['data_info'] : '';
    }
    else
    {
        $result = pub_func_image_upload($file, "data/upimg/playbill/" . $live_id . "/" . $date . "/");
        return $result['path'];
    }
}

function pub_func_image_upload_by_topic_category($file, $topic_id, $category_id) {
    $result = pub_func_image_upload($file, "data/upimg/topic/" . $topic_id . "/" . $category_id . "/");
    return $result['path'];
}

function pub_func_image_upload_by_asset_category($file, $asset_id, $category_id) {
    $result = pub_func_image_upload($file, "data/upimg/asset/" . $asset_id . "/" . $category_id . "/");
    return $result['path'];
}

function pub_func_image_upload_by_service_category($file, $service_id, $category_id) {
    $result = pub_func_image_upload($file, "data/upimg/service/" . $service_id . "/" . $category_id . "/");
    return $result['path'];
}

//升级包上传
function pub_func_file_upload_by_update($file, $device_type_id, $type) {
    $result = pub_func_image_upload($file, "data/upfile/update/" . $device_type_id . "/", 0, $type);
    return $result['path'];
}

//媒资包图片
function pub_func_image_upload_by_asset($file, $asset_id) {
    $result = pub_func_image_upload($file, "data/upimg/asset/" . $asset_id . "/");
    return $result['path'];
}

//信息包记录图片
function pub_func_image_upload_by_topic_photo($file, $topic_id, $item_id) {
    $result = pub_func_image_upload($file, "data/upimg/info/" . $topic_id . "/" . $item_id . "/", 4);
    return $result['path'];
}

//web扩展图片
function pub_func_image_upload_by_web_ex_photo($file, $nns_id) {
    $result = pub_func_image_upload($file, "data/upimg/info/web_ex/" . $nns_id . "/", 4);
    return $result['path'];
}

//信息包记录图片
function pub_func_image_upload_by_vod_stills($file, $vod_id) {
    $result = pub_func_image_upload($file, "data/upimg/info/" . $vod_id . "/", 4);
    return $result['path'];
}

//点播图片
function pub_func_image_upload_by_vod($file, $video_id,$cp_config=null,$cp_id=null)
{
    if(isset($cp_config['down_img_dir']) && strlen($cp_config['down_img_dir']) >0 && strlen($cp_id) >0)
    {
        $result =  pub_func_image_upload_v2($file,$cp_config['down_img_dir'],$cp_id,'vod');
        return (isset($result['data_info']) && strlen($result['data_info']) >0) ? $result['data_info'] : '';
    }
    else
    {
        $result = pub_func_image_upload($file, "data/upimg/vod/" . $video_id . "/", 3);
        return $result['path'];
    }
}

//点播分集图片
function pub_func_image_upload_by_vod_index($file, $video_id, $index,$cp_config=null,$cp_id=null)
{
    if(isset($cp_config['down_img_dir']) && strlen($cp_config['down_img_dir']) >0 && strlen($cp_id) >0)
    {
        $result =  pub_func_image_upload_v2($file,$cp_config['down_img_dir'],$cp_id,'index');
        return (isset($result['data_info']) && strlen($result['data_info']) >0) ? $result['data_info'] : '';
    }
    else
    {
        $result = pub_func_image_upload($file, "data/upimg/vod/" . $video_id . "/" . $index . "/", 3);
        return $result['path'];
    }
}

//直播图片
function pub_func_image_upload_by_live($file, $video_id,$cp_config=null,$cp_id=null)
{
    if(isset($cp_config['down_img_dir']) && strlen($cp_config['down_img_dir']) >0 && strlen($cp_id) >0)
    {
        $result =  pub_func_image_upload_v2($file,$cp_config['down_img_dir'],$cp_id,'live');
        return (isset($result['data_info']) && strlen($result['data_info']) >0) ? $result['data_info'] : '';
    }
    else
    {
        $result = pub_func_image_upload($file, "data/upimg/live/" . $video_id . "/", 3);
        return $result['path'];
    }
}

//服务包图片
function pub_func_image_upload_by_service($file, $service_id) {
    $result = pub_func_image_upload($file, "data/upimg/service/" . $service_id . "/");
    return $result['path'];
}

//服务包记录图片
function pub_func_image_upload_by_service_item($file, $service_id, $service_item_id) {
    $result = pub_func_image_upload($file, "data/upimg/service/" . $service_id . "/" . $service_item_id . "/", 4);
    return $result['path'];
}

//服务包相册图片
function pub_func_image_upload_by_service_item_list($file, $service_id, $service_item_id) {
    $result = pub_func_image_upload($file, "data/upimg/service/" . $service_id . "/" . $service_item_id . "/images/", 4);
    return $result['path'];
}

//直播分集图片
function pub_func_image_upload_by_live_index($file, $video_id, $index) {
    $result = pub_func_image_upload($file, "data/upimg/live/" . $video_id . "/" . $index . "/", 3);
    return $result['path'];
}

//广告元素图片
function pub_func_image_upload_by_ad($file, $ad_id) {
    $result = pub_func_image_upload($file, "data/upimg/ad/" . $ad_id . "/", 3);
    //var_dump($result);exit;
    return $result['path'];
}

//广告模板图片
function pub_func_image_upload_by_ad_template($file, $template_id) {
    $template_id = str_replace(':', '', $template_id);
    $template_id = str_replace('.', '', $template_id);
    $result = pub_func_image_upload($file, "data/upimg/ad/template/" . $template_id . "/");
    return $result;
}

//信息包图片
function pub_func_image_upload_by_topic($file, $topic_id) {
    $result = pub_func_image_upload($file, "data/upimg/topic/" . $topic_id . "/");
    return $result['path'];
}

//生成图片URL策略
function pub_func_get_image_url($img_url = '', $bool = true) {
    if (empty($img_url) && $bool) {
        return '';
    }
    //由于在加载图片为直接不是下载得图片uil时，不需要进行拼装url，优化页面速度太慢，xinxin.deng 2017-09-21 17:20:00
    if (stripos($img_url, 'http://') !== false)
    {
        return $img_url;
    }
    return nl_image::get_manager_image_url($img_url);
}

//生成文件URL策略
function pub_func_get_file_url($file_url = '') {
    if (empty($file_url))
        return '';
    return nl_image::get_manager_image_url($file_url);
}

//CP图片
function pub_func_image_upload_by_cp($file,$cp_id){
    $result=pub_func_image_upload($file,"data/upimg/cp/".$cp_id."/");
    return $result['path'];
}

function pub_func_image_upload($file, $path, $num = 0, $default_name = '') {

    global $g_syn_data_path;
    include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "nncms_controls_upfile.php";
    if ($file['tmp_name'] && !empty($file['size'])) {
        $upimg = new nncms_controls_upfile();
        $upimg->web_base_url = $path;
        $upimg->broken_dir($num);
        $upimg->ph_path = dirname(dirname(dirname(__FILE__))) . "/" . $upimg->web_base_url;
        $upimg->get_ph_defaultname($file['name']);
        $upimg->get_ph_tmpname($file['tmp_name']);
        $upimg->get_ph_type($file['type']);
        $upimg->get_ph_size($file['size']);
        if (empty($default_name)) {
            $file_name = pub_func_get_image_guid();
        } else {
            $file_name = $default_name;
        }
        $upimg->get_ph_name($file_name);
        $old_phname = $upimg->ph_name;
        $path = $upimg->web_address;
        $upimg->save();

        $result = array();
        $result['path'] = $path;
        $result['size'] = $upimg->imgsize;

        //转存数据
        if (!empty($g_syn_data_path)) {

            $old = umask(0);
            $g_syn_paths = explode(';', $g_syn_data_path);
            if (count($g_syn_paths) > 0) {
                foreach ($g_syn_paths as $item_path) {
                    if (file_exists($item_path)) {
                        $syn_path = $item_path . $path;

                        $upimg->check_path($syn_path);
                        copy($old_phname, $syn_path);

                        chmod($syn_path, 0777);
                    }
                }
            }
            umask($old);
        }
        $result_path = nl_image::_ftp_copy_images($upimg->web_base_url, $result["path"]);
        if ($result_path === FALSE) {
            echo '<script>alert("上传图片到ftp失败！");histroy.go(-1);</script>';
            exit;
        }

        $result['path'] = $result_path;
        //		global $ftp_conf;
        //		if (is_array($ftp_conf)){
        //			include_once NPDIR.'np_ftp.php';
        //		//连接ftp
        //			$ftp1=new nns_ftp($ftp_conf["ftp_to1"]["address"],$ftp_conf["ftp_to1"]["user"],$ftp_conf["ftp_to1"]["password"],$ftp_conf["ftp_to1"]["port"]);
        //			$ftp2=new nns_ftp($ftp_conf["ftp_to2"]["address"],$ftp_conf["ftp_to2"]["user"],$ftp_conf["ftp_to2"]["password"],$ftp_conf["ftp_to2"]["port"]);
        //			$ftpcon1=$ftp1->connect();
        //			$ftpcon2=$ftp2->connect();
        //			if(empty($ftpcon1)||empty($ftpcon2)){
        //				echo '<script>alert("连接ftp失败！");histroy.go(-1);</script>';
        //
        ////echo $ftp_conf["ftp_to1"]["address"].'|'.$ftp_conf["ftp_to1"]["user"].'|'.$ftp_conf["ftp_to1"]["password"].'|'.$ftp_conf["ftp_to1"]["port"];
        //				exit;$ftp_to1=new  nns_ftp($ftp_conf['ftp_to1']["address"],$ftp_conf['ftp_to1']["user"],$ftp_conf['ftp_to1']["password"],$ftp_conf['ftp_to1']["port"]);
        //			}
        //			//上传
        //			$path=str_replace('data/upimg/','',$upimg->web_base_url);
        //			$ftp1_path=$ftp_conf["ftp_to1"]["img_cms"].'/'.$path;
        //			$ftp2_path=$ftp_conf["ftp_to2"]["img_cms"].'/'.$path;
        //			$img_name=str_replace($upimg->web_base_url,'',$result["path"]);//图片名称，不包括路径
        //			$bool1=$ftp1->up($ftp1_path,$img_name,dirname(dirname(dirname(__FILE__))).'/'.$result["path"]);
        //			$bool2=$ftp2->up($ftp2_path,$img_name,dirname(dirname(dirname(__FILE__))).'/'.$result["path"]);
        //			if (!$bool1 || !$bool2){
        //				echo '<script>alert("上传图片到ftp失败！");histroy.go(-1);</script>';
        //				exit;
        //			}
        //			//删除本地图片
        //	//		unlink($result["path"]);
        //			//$result["path"]=$ftp1_path.$img_name;
        //			$result['path']=str_replace("data/upimg", "/prev/StarCor", $result['path']);
        //		}

        return $result;
    } else {
        return null;
    }
}



function pub_func_image_upload_v2($file,$down_img_dir, $cp_id,$type='vod')
{
    if (empty($file['tmp_name']) || empty($file['size']) || empty($file['name']))
    {
        return  array(
            "ret" => 1,
            "reason" => "上传文件有问题",
        );
    }
    $temp_img_pathinfo = pathinfo($file['name']);
    $temp_img_extension = (isset($temp_img_pathinfo['extension']) && strlen($temp_img_pathinfo['extension']) > 0) ? strtolower($temp_img_pathinfo['extension']) : 'jpg';
    //播控生成自己的文件名规则了，cp+日期+guid
    $str_date = date("Ymd");
    $str_path_with_cp = 'cp_' . strtolower($cp_id) . '/' . $str_date.'/'.$type;
    $bk_img_store_dir = rtrim(rtrim($down_img_dir . "/prev/KsImg/" . $str_path_with_cp, '/'), '\\');
    $bk_img_store_relative_dir = rtrim(rtrim("/prev/KsImg/" . $str_path_with_cp, '/'), '\\');


    if (! is_dir($bk_img_store_dir))
    {
        $flg = mkdir($bk_img_store_dir, 0777, true);
        if (! $flg)
        {
            return  array(
                "ret" => 1,
                "reason" => "本地创建路径失败:{$down_img_dir}:{$bk_img_store_dir}",
            );
        }
        @chmod($bk_img_store_dir, 0777);
    }
    $save_name_v2 = np_guid_rand() . '.' . $temp_img_extension;
    if (file_exists($bk_img_store_dir . '/' . $save_name_v2))
    {
        @unlink($bk_img_store_dir . '/' . $save_name_v2);
    }
    $img = move_uploaded_file($file['tmp_name'], $bk_img_store_dir . '/' . $save_name_v2);
    if (! $img)
    {
        return array(
            "ret" => 1,
            "reason" => "本地创建路径失败:存储文件{$bk_img_store_dir}" . "/" . "{$save_name_v2}",
        );
    }
    $str_path = str_replace("//", "/", str_replace("//", "/", $bk_img_store_relative_dir . '/' . $save_name_v2));
    return array(
        "ret" => 0,
        "reason" => "本地创建路径失败:存储文件{$bk_img_store_dir}" . "/" . "{$save_name_v2}",
        'data_info'=>$str_path
    );
}

function pub_func_get_image_guid() {
    include_once dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . "nn_cms_db" . DIRECTORY_SEPARATOR . "nns_common" . DIRECTORY_SEPARATOR . "nns_db_guid_class.php";
    $guid_inst = new Guid();
    return $guid_inst->toString();
}

function pub_func_image_delete($path, $cp_config = null)
{
    global $g_syn_data_path;
    if (! empty($path))
    {
        if (isset($cp_config['down_img_dir']) && strlen($cp_config['down_img_dir']) > 0)
        {
            $old_path = rtrim(rtrim($cp_config['down_img_dir'],'/'),'\\') . '/' . ltrim(ltrim($path,'/'),'\\');
        }
        else
        {
            $old_path = dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . $path;
        }
        if (file_exists($old_path) && is_file($old_path))
        {
            unlink($old_path);
        }
        // 试图删除同步文件夹内的内容
        if (empty($g_syn_data_path))
        {
            return true;
        }
        $g_syn_paths = explode(';', $g_syn_data_path);
        if (count($g_syn_paths) <1)
        {
            return true;
        }
        foreach ($g_syn_paths as $item_path)
        {
            if (!is_dir($item_path))
            {
                continue;
            }
            $syn_path = $item_path . $path;
            if (file_exists($syn_path) && is_file($syn_path))
            {
                unlink($syn_path);
            }
        }
        return true;
    }
}

function pub_func_get_tree_by_html($content, $root_name, $root_id,$selects=NULL) {
    $js_func='"javascript:select_tree_item(' .
        '\'' . $root_id . '\',' .
        '\'' . $root_name . '\',' .
        '\'0\'' .
        ')"';

    if (is_array($selects)){
        if (array_search('1000',$selects)===FALSE){
            $js_func='"javascript:void(null);"';
        }
    }
    $return_str = '<script type="text/javascript">';
    $return_str .= 'd = new dTree("d");';
    $return_str .= 'd.add("0",' .
        '"-1",' .
        '"' . $root_name . '",' .
        $js_func.',' .
        '"0");';
    if (!empty($content)) {
        $alt_array = array();
        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->loadXML($content);
        $category = $dom->getElementsByTagName('category');
        foreach ($category as $item) {

            if (is_array($selects)){
                if (array_search($item->getAttribute("id"),$selects)===FALSE) continue;
            }

            $category_id = substr($item->getAttribute("id"), -3, 3);
            $return_str .= 'd.add("' . $item->getAttribute("id") . '",' .
                '"' . $item->getAttribute("parent") . '",' .
                '"' . $item->getAttribute("name") . '",' .
                '"javascript:select_tree_item(' .
                '\'' . $item->getAttribute("id") . '\',' .
                '\'' . $item->getAttribute("name") . '\',' .
                '\'' . $item->getAttribute("parent") . '\',' .
                '\'' . $item->getAttribute("virtual") . '\'' .
                ')",' .
                '"' . $category_id . '"';
            if ($item->hasAttribute("virtual")) {
                if ($item->getAttribute("virtual")) {
                    $return_str.=',1,"_top","../../images/tag_blue.png","../../images/tag_red.png"';
                } else {
                    $return_str.=',0';
                }
            } else {
                $return_str.=',0';
            }
            $return_str.=');';

            //			$alt_array[$item->getAttribute("id")]=pub_func_create_tree_alt_by_asset($item);
        }
    }
    $return_str .= 'document.write(d);';
    foreach ($alt_array as $key=>$alt){
        $return_str .= '$node=$(".dtree .node[categoryid='.$key.']");' .
            'var old_html=$node.html();' .
            'var new_html=\'<alt alt="'.$alt.'">\'+old_html+\'</alt>\';'.
            '$node.html(new_html);' ;
    }
    $return_str .= '$(document).ready(function(){';


    $return_str .= 'var node=d.selectedNode;if (!node) node=0;' .
        'var node_id=d.aNodes[node].id;' .
        'var node_parent_id=d.aNodes[node].pid;' .
        'var node_name=d.aNodes[node].name;' .
        'var node_data=d.aNodes[node].data;' .
        'if (node_id==0){node_id="' . $root_id . '";}' .
        'if (node_parent_id==-1){node_parent_id=0;}' .
        'select_tree_item(node_id,node_name,node_parent_id,node_data);';
    $return_str .= '});';
    $return_str .= '</script>';
    //	echo $return_str;exit;
    //	echo '<pre>';
    //	print_r($return_str);exit;
    return $return_str;
}

function pub_func_get_tree_by_html_array($category, $root_name, $root_id, $category_id = null) {
    $return_str = '<script type="text/javascript">';
    $return_str .= 'd = new dTree("d");';
    if ($category_id) {
        $return_str .= 'd.add("' . $category_id . '",' .
            '"-1",' .
            '"' . $root_name . '",' .
            '"javascript:select_tree_item(' .
            '\'' . $root_id . '\',' .
            '\'' . $root_name . '\',' .
            '\'0\'' .
            ')",' .
            '"0");';
    } else {
        $return_str .= 'd.add("10000",' .
            '"-1",' .
            '"' . $root_name . '",' .
            '"javascript:select_tree_item(' .
            '\'' . $root_id . '\',' .
            '\'' . $root_name . '\',' .
            '\'0\'' .
            ')",' .
            '"0");';
    }

    if (!empty($category)) {
        foreach ($category as $item) {

            $return_str .= 'd.add("' . $item['nns_id'] . '",' .
                '"' . $item['nns_parent_id'] . '",' .
                '"' . $item['nns_name'] . '",' .
                '"javascript:select_tree_item(' .
                '\'' . $item['nns_id'] . '\',' .
                '\'' . $item['nns_name'] . '\',' .
                '\'' . $item['nns_parent_id'] . '\'' .
                ')",' .
                '"' . $item['nns_id'] . '");';
        }
    }
    $return_str .= 'document.write(d);';
    $return_str .= '$(document).ready(function(){';
    $return_str .= 'var node=d.selectedNode;if (!node) node=0;' .
        'var node_id=d.aNodes[node].id;' .
        'var node_parent_id=d.aNodes[node].pid;' .
        'var node_name=d.aNodes[node].name;' .
        'if (node_id==0){node_id="' . $root_id . '";}' .
        'if (node_parent_id==-1){node_parent_id=0;}' .
        'select_tree_item(node_id,node_name,node_parent_id);';
    $return_str .= '});';
    $return_str .= '</script>';

    return $return_str;
}

function pub_func_get_tree_by_array($category, $root_name, $root_id) {
    $js_func='"javascript:select_tree_item(' .
        '\'' . $root_id . '\',' .
        '\'' . $root_name . '\',' .
        '\'0\'' .
        ')"';

    $return_str = '<script type="text/javascript">';
    $return_str .= 'd = new dTree("d");';
    $return_str .= 'd.add("0",' .
        '"-1",' .
        '"' . $root_name . '",' .
        $js_func.',' .
        '"0");';

    if (!empty($category)) {
        foreach ($category as $item) {
            $category_id = substr($item['nns_category_id'], -3, 3);
            $return_str .= 'd.add("' . $item['nns_category_id'] . '",' .
                '"' . $item['nns_parent_id'] . '",' .
                '"' . $item['nns_name'] . '",' .
                '"javascript:select_tree_item(' .
                '\'' . $item['nns_category_id'] . '\',' .
                '\'' . $item['nns_name'] . '\',' .
                '\'' . $item['nns_parent_id'] . '\'' .
                ')",' .
                '"' . $category_id . '"';
            $return_str.=',0';
            $return_str.=');';

        }
    }
    $return_str .= 'document.write(d);';
    $return_str .= '$(document).ready(function(){';
    $return_str .= 'var node=d.selectedNode;if (!node) node=0;' .
        'var node_id=d.aNodes[node].id;' .
        'var node_parent_id=d.aNodes[node].pid;' .
        'var node_name=d.aNodes[node].name;' .
        'if (node_id==0){node_id="' . $root_id . '";}' .
        'if (node_parent_id==-1){node_parent_id=0;}' .
        'select_tree_item(node_id,node_name,node_parent_id);';
    $return_str .= '});';
    $return_str .= '</script>';

    return $return_str;
}

function pub_func_create_tree_alt_by_asset($item) {
    $url_img = $item->getAttribute('img0');
    $alt_html = '<div style="width:300px;padding:5px;">' .
        '<div style="font-size:16px;font-weight:bold;line-height:22px;">' . $item->getAttribute('id') . '</div>';
    $alt_html.='<div style="font-size:12px;line-height:20px;">';
    if (!empty($url_img)) {
        $alt_html .= '<img id="treeimg_' . $item->getAttribute('id') . '" src="' . pub_func_get_image_url($item->getAttribute('img0')) .
            '" style="float:left; margin:0px 5px 5px 0px;  border:2px solid #cccccc;"/>';
        $alt_html .= '<script>add_dy_func_by_img($("#treeimg_' . $item->getAttribute('id') . '"),120);</script>';
    }
    //	$alt_html.=mb_substr($item["nns_summary"],0,200,"utf-8").'</div>';
    $alt_html.='</div>';
    return htmlspecialchars($alt_html, ENT_QUOTES);
}

function pub_func_get_topic_alt($item) {
    $alt_html = '<div style="width:300px;padding:5px;"><div style="font-size:16px;font-weight:bold;line-height:22px;">' . $item["nns_title"] . '</div>';
    $alt_html.='<div style="font-size:12px;color:#888888;">来源:' . $item["nns_source"] . '</div>';
    $alt_html.='<div style="font-size:12px;color:#888888;">时间:' . $item["nns_create_time"] . '</div>';
    $alt_html.='<div style="font-size:12px;line-height:20px;">';
    //	if (!empty ($item["nns_image"])) {
    //		$alt_html .= '<img id="img_'.$item["nns_id"].'" src="../../../' . $item["nns_image"] . '" style="float:left; margin:0px 5px 5px 0px;  border:2px solid #cccccc;"/>' ;
    //		$alt_html .= '<script>add_dy_func_by_img($("#img_'.$item["nns_id"].'"),120);</script>';
    //	}
    $alt_html.=mb_substr($item["nns_summary"], 0, 200, 'utf-8') . '</div>';
    $alt_html.='</div>';
    return htmlspecialchars($alt_html, ENT_QUOTES);
}

function pub_func_get_web_ex_alt($item) {
    $alt_html = '<div style="width:300px;padding:5px"><img style="width:290px" src="' . pub_func_get_image_url($item['nns_image']) . '"/></div>';
    return htmlspecialchars($alt_html, ENT_QUOTES);
}

/**
 * 拒绝审核的原因
 */
function pub_func_get_topic_audit_alt($item) {
    $alt_html = '<div style="width:300px;padding:5px;"><div style="font-size:16px;font-weight:bold;line-height:22px;">' . $item["nns_title"] . '</div>';
    $alt_html.='<div style="font-size:12px;line-height:20px;">拒绝原因：';
    $alt_html.=mb_substr($item["nns_audit_reason"], 0, 200, 'utf-8') . '</div>';
    $alt_html.='</div>';
    return htmlspecialchars($alt_html, ENT_QUOTES);
}

function pub_func_get_video_alt($item) {
    global $language_action_point;
    $alt_html = '<div style="font-size:16px;font-weight:bold;line-height:22px;">' . $item["nns_name"] . '</div>';
    $alt_html.='<div style="font-size:12px;line-height:20px;">' .
        '<div style="float:left;margin:0px 5px 5px 0px;display:block;">';
    if (!empty($item["nns_image2"])) {
        $alt_html .= '<img id="img_' . $item["nns_id"] . '" src="' . pub_func_get_image_url($item["nns_image2"]) . '" style="margin:5px auto; border:2px solid #cccccc;"/><br/>';
        $alt_html .= '<script>add_dy_func_by_img($("#img_' . $item["nns_id"] . '"),120);</script>';
    }
    $alt_html .= '<span class="p_rate" id="p_rate_' . $item["nns_id"] . '" style="margin:0px auto;">';
    for ($point = 1; $point < 6; $point++) {
        if ($point <= $item["nns_point"]) {
            $alt_html .= '<i title="' . $point . $language_action_point . '" class="select" ></i>';
        } else {
            $alt_html .= '<i title="' . $point . $language_action_point . '" ></i>';
        }
    }

    $alt_html .= '</span>' .
        '<script>
						var Rate = new pRate("p_rate_' . $item["nns_id"] . '",null,true);


						</script>';
    $alt_html.='</div></div>';
    return htmlspecialchars($alt_html, ENT_QUOTES);
}

function pub_func_get_vod_alt($item) {
    global $language_action_point;
    $alt_html = '<div style="width:300px;padding:5px;"><div style="font-size:16px;font-weight:bold;line-height:22px;">' . $item["nns_name"] . '</div>';
    $alt_html.='<div style="font-size:12px;line-height:20px;">' .
        '<div style="float:left;margin:0px 5px 5px 0px;display:block;">';
    if (!empty($item["nns_image2"])) {
        $alt_html .= '<img id="img_' . $item["nns_id"] . '" src="' . pub_func_get_image_url($item["nns_image2"]) . '" style="margin:5px auto; border:2px solid #cccccc;"/><br/>';
        $alt_html .= '<script>add_dy_func_by_img($("#img_' . $item["nns_id"] . '"),120);</script>';
    }
    $alt_html .= '<span class="p_rate" id="p_rate_' . $item["nns_id"] . '" style="margin:0px auto;">';
    for ($point = 1; $point < 6; $point++) {
        if ($point <= $item["nns_point"]) {
            $alt_html .= '<i title="' . $point . $language_action_point . '" class="select" ></i>';
        } else {
            $alt_html .= '<i title="' . $point . $language_action_point . '" ></i>';
        }
    }

    $alt_html .= '</span>' .
        '<script>
						var Rate = new pRate("p_rate_' . $item["nns_id"] . '",null,true);


						</script>';
    $alt_html.='</div>';
    $alt_html.='<p><b>导演：</b>' . mb_substr($item["nns_director"], 0, 20, "utf-8") . '</p>';
    $alt_html.='<p><b>演员：</b>' . mb_substr($item["nns_actor"], 0, 40, "utf-8") . '</p>';
    $alt_html.='<p><b>上映时间：</b>' . $item["nns_show_time"] . '</p>';
    $alt_html.='<p><b>地区：</b>' . $item["nns_area"] . '</p>';
    $alt_html.='<p><b>时长：</b>' . ceil($item["nns_view_len"] / 60) . '分钟</p>';
    $alt_html.='<p><b>简介：</b>' . mb_substr($item["nns_summary"], 0, 200, "utf-8") . '</p>';
    $alt_html.='</div></div>';
    return htmlspecialchars($alt_html, ENT_QUOTES);
}

function pub_func_get_playbill_alt($item) {
    $alt_html = '<div style="width:300px;padding:5px;"><div style="font-size:16px;font-weight:bold;line-height:22px;">' . $item["nns_name"] . '</div>';
    $alt_html.='<div style="font-size:12px;line-height:20px;">';

    if (!empty($item["nns_image2"])) {
        $alt_html.='<div style="float:left;margin:0px 5px 5px 0px;display:block;">';
        $alt_html .= '<img id="img_' . $item["nns_id"] . '" src="' . pub_func_get_image_url($item["nns_image2"]) . '" style="margin:5px auto; border:2px solid #cccccc;"/><br/>';
        $alt_html .= '<script>add_dy_func_by_img($("#img_' . $item["nns_id"] . '"),120);</script>';
        $alt_html.='</div>';
    }

    $alt_html.='<p><b>日期：</b>' . date('Y-m-d', strtotime($item["nns_begin_time"])) . '</p>';
    $alt_html.='<p><b>播出时间：</b>' . date('H:i:s', strtotime($item["nns_begin_time"])) . '</p>';
    $alt_html.='<p><b>播出时长：</b>' . ceil($item["nns_begin_time"] / 60) . '分钟</p>';
    $alt_html.='<p><b>简介：</b>' . mb_substr($item["nns_summary"], 0, 200, "utf-8") . '</p>';
    $alt_html.='</div></div>';
    return htmlspecialchars($alt_html, ENT_QUOTES);
}

function pub_func_get_stills_image_alt($image) {
    global $language_action_point;
    $alt_html = '<div style="width:200px;word-wrap:break-word;padding:-150px;z-index:1000"><div style="font-size:16px;font-weight:bold;line-height:22px;"><b>描述：</b>' . $image["nns_summary"] . '</div>';
    $alt_html.='</div>';
    return htmlspecialchars($alt_html, ENT_QUOTES);
}

function pub_func_get_module_right2($g_value) {
    global $$key;
    if (!isset($$key))
        $$key = 0;
    return $$key;
}

function pub_func_get_module_right($code, $method = 'menu') {
    global $nncms_config_path;
    global $g_partner_enabled;
    global $g_group_enabled;
    global $g_live_enabled;
    global $g_product_enabled;
    global $g_service_enabled;
    global $g_asset_enabled;
    global $g_ipqam_enabled;
    global $g_web_mode;
    global $g_calculate;
    global $g_mode;
    global $g_key_word_enabled;
    global $g_ad_enabled;
    global $g_message_enabled;
    global $g_bbs_enabled;
    global $g_appstore_enabled;
    global $g_information_enabled;
    global $g_hotel_enabled;
    global $g_comment_enabled;
    global $g_schedule_enabled;
    global $g_web_ex_enabled;
    global $g_hospital_enabled;
    global $g_voting_enabled;
    global $g_web_weather_enabled;
    global $g_buss_enabled;
    global $g_hot_word_enabled;
    global $g_adi_import_enabled;
    global $g_label_mode;
    global $g_bk_enabled;
    switch ($code) {
        case '101':

            break;
        case '102':

            break;
        case '103':
            if ($g_partner_enabled == 0)
                return false;
            break;
        case '104':
            if ($g_group_enabled == 0)
                return false;

            break;
        case '105':

            break;
        case '106':

            break;
        case '107':
            if ($g_live_enabled == 0)
                return false;

            break;
        case '108':

            break;
        case '109':
            if ($g_product_enabled == 0)
                return false;
            break;
        case '110':
            if ($g_service_enabled == 0)
                return false;
            break;
        case '111':
            if ($g_asset_enabled == 0)
                return false;
            break;
        case '112':
            if ($g_ipqam_enabled == 0)
                return false;

            break;
        case '113':
            //		模板管理
            if ($g_web_mode == 0)
                return false;
            break;
        case '114':
            if ($g_calculate == 0)
                return false;
            break;
        case '115':
            if ($g_mode != "gxcatv" && !$g_adi_import_enabled)
                return false;
            break;
        case '116':
            if ($g_key_word_enabled == 0)
                return false;
            break;
        case '117':
            if ($g_ad_enabled == 0)
                return false;
            break;
        case '118':
            if ($g_message_enabled == 0)
                return false;
            break;
        case '119':
            if ($g_bbs_enabled == 0)
                return false;
            break;
        case '120':
            if ($g_appstore_enabled == 0)
                return false;
            break;
        case '121':
            if ($g_information_enabled == 0)
                return false;
            break;
        case '122':
            if ($g_hotel_enabled == 0)
                return false;
            break;
        case '123':
            if ($g_comment_enabled == 0)
                return false;
            break;
        case '124':
            if ($g_schedule_enabled == 0)
                return false;
            break;
        case '125':
            if ($g_appstore_enabled == 0)
                return false;
            break;
        case '126':
            if ($g_web_ex_enabled == 0)
                return false;
            break;
        case '127':
            if (0 == $g_web_weather_enabled)
                return false;
            break;
        case '128':
            if ($g_hospital_enabled == 0)
                return false;
            break;
        case '129':
            if ($g_voting_enabled == 0)
                return false;
            break;
        case '130':
            if ($g_buss_enabled == 0)
                return false;
            break;
        case '131':
            if ($g_hot_word_enabled == 0)
                return false;
            break;
        case '132':
            if ($g_mode != 'mgtv')
                return false;
            break;
        case '133':
            if ($g_label_mode == 0)
                return false;
            break;
        case '135':
            if ($g_bk_enabled == 0)
                return false;
            break;
    }

    if ($method == 'menu') {

        if (strstr($_SESSION["nns_role_pris"], $code . '001')) {

            return false;
        }
    }

    return true;
}

?>