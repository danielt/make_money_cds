<?php
/*
 * Created on 2012-8-29
 * author:bo.chen@starcorcn.com
 * 用于栏目树的AJAX 统计
 */
 ini_set('display_errors','Off');
 include("../nncms_manager_inc.php");
 include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
  /**
  * status 请求状态
  * data   返回结果集
  * $result 根据业务需求可以自定义扩展
  */
 $result  = array('status'=>true,'reason'=>'ok','data'=>'');
 
 /**
  * 统计类型
  * category    影片分类的影片数量统计
  */
 $type = isset($_GET['type'])?$_GET['type']:'vod';
 //栏目树节点ID
 $sp_id = isset($_GET['sp_id'])?$_GET['sp_id']:'';
 //分类ID list
 $category_id = isset($_GET['category_list'])?$_GET['category_list']:'';
 $category_name = isset($_GET['category_name'])?$_GET['category_name']:'';
 $category_id_array = explode(',',$category_id);
 $category_name_array = explode(',',$category_name);
 $category_list = null;
 foreach($category_id_array as $key=>$val){
 	if(empty($val) && $key !=0){
 		unset($category_id_array[$key]);
 	}else{
 		$category_list[$val] = $category_name_array[$key];	
 	}
 	
 }
 $category_id_array[0] = 1000;

 if(empty($type)){
 	$result['reason'] = cms_get_lang('cscw').'！';
 	$result['status'] = false;
 	echo json_encode($result);exit;
 //直播分类影片统计
 }
if($type == 'vod'){
	$category_id_array[0] = 10000;
 	
	include_once($nncms_config_path."mgtv_v2/mgtv_init.php");
	$data = sp_category_model::get_category_count($sp_id);
 	
 }
 	$category_array = array();
 //如果执行sql失败

	
 	foreach($data['data']  as $key=>$val){
 		$category_array[$val['category_id']] = $val;
 	}

 	foreach($category_list as $key => $val){
 		if(!array_key_exists($key,$category_array)){
 			$category_array[$key]['category_id'] = $key;
 			$category_array[$key]['num'] = 0;
 		}
 	}
 	
 	$data['data'] = $category_array;
 	$total = 0;
 	if(isset($data['data']) && $data['data']){
 		//分类数据叠加
 		foreach($data['data'] as $key=>$val){
 			foreach($data['data'] as $k=>$v){
 				//如果key 相等不需要叠加影片数据
 				if($key == $k){
 					continue;
 				}
 				//判断是否为$v['category_id'] 是否为$val['category_id']的父节点
 				$pos = strpos(trim($v['category_id']),trim($val['category_id']));
 				if($pos === 0){
 					$data['data'][$key]['num'] += $v['num'];
 				}
 			}
			$total += $val['num'];
 		}
	 	foreach($data['data'] as $key=>$val){
	 		if(array_key_exists($val['category_id'],$category_list) && $val['category_id'] !=0){
	 			$category_list[$val['category_id']] .= '[<span style="color:#ff0000">'.$val['num'].'</span>]';
	 			
	 		}
	 	}
 	}

 	//判断父节点下面是否绑定有影片。
 	if($data['data'][0]['category_id']== $category_id_array[0]){
 		$category_list[0] .=  '[<span style="color:#ff0000">'.$data['data'][0]['num'].'</span>]';
 	}else{
 		$category_list[0] .=  '[<span style="color:#ff0000">'.$total.'</span>]';
 	}
	
	//没有绑定影片的默认0
 	foreach($category_list  as $key=>$val){
 		 if(!strstr($val,'span')){
 			$category_list[$key] .= '[<span style="color:#ff0000">0</span>]';
 		}
 	}
 	
	$result['data'] = $category_list;
 	echo json_encode($result);exit;
 

?>

