$(function(){
    $(document).keydown(function(e){
    	if(e.which  ==13)
    	{
    		$('#search').click();
    	}
    });

    $('#search').click(function(){

    	get_data_show();

    });
	var cd_1 =Calendar.setup({
		weekNumbers: true,
	    inputField : "begin_time",
	    trigger    : "begin_time",
	    dateFormat: "%Y-%m-%d %H:%M:%S",
	    showTime: true,
	    minuteStep: 1,
	    onSelect   : function() {
	    	this.hide();}
		});
	var cd_2= Calendar.setup({
		weekNumbers: true,
	    inputField : "end_time",
	    trigger    : "end_time",
	    dateFormat: "%Y-%m-%d %H:%M:%S",
	    showTime: true,
	    minuteStep: 1,
	    onSelect   : function() {
	    	this.hide();}
		});
	$('#clear_time').click(function(){
		$('#begin_time').val('');
		$('#end_time').val('');

	});

	  $("#harf_hour").click(function(){

    	var edate = new Date();
        var bdate = new Date(edate.getTime()-1800*1000);
        var begin_time = bdate.getFullYear()+"-"+(bdate.getMonth()+1)+"-"+bdate.getDate()+" "+bdate.getHours()+":"+bdate.getMinutes();
        var end_time = edate.getFullYear()+"-"+(edate.getMonth()+1)+"-"+edate.getDate()+" "+edate.getHours()+":"+edate.getMinutes();

         $("#begin_time").val(begin_time);
         $("#end_time").val(end_time);

         show_time_str(begin_time,end_time);

         get_data_show();
    });


    $("#one_hour").click(function(){

    	var edate = new Date();
        var bdate = new Date(edate.getTime()-3600*1000);
        var begin_time = bdate.getFullYear()+"-"+(bdate.getMonth()+1)+"-"+bdate.getDate()+" "+bdate.getHours()+":"+bdate.getMinutes();
        var end_time = edate.getFullYear()+"-"+(edate.getMonth()+1)+"-"+edate.getDate()+" "+edate.getHours()+":"+edate.getMinutes();

         $("#begin_time").val(begin_time);
         $("#end_time").val(end_time);

         show_time_str(begin_time,end_time);


	    get_data_show();

    });
	$("#three_hour").click(function(){

    	var edate = new Date();
        var bdate = new Date(edate.getTime()-3*3600*1000);
        var begin_time = bdate.getFullYear()+"-"+(bdate.getMonth()+1)+"-"+bdate.getDate()+" "+bdate.getHours()+":"+bdate.getMinutes();
        var end_time = edate.getFullYear()+"-"+(edate.getMonth()+1)+"-"+edate.getDate()+" "+edate.getHours()+":"+edate.getMinutes();

         $("#begin_time").val(begin_time);
         $("#end_time").val(end_time);

         show_time_str(begin_time,end_time);


	    get_data_show();

    });

    $("#six_hour").click(function(){

    	var edate = new Date();
        var bdate = new Date(edate.getTime()-6*3600*1000);
        var begin_time = bdate.getFullYear()+"-"+(bdate.getMonth()+1)+"-"+bdate.getDate()+" "+bdate.getHours()+":"+bdate.getMinutes();
        var end_time = edate.getFullYear()+"-"+(edate.getMonth()+1)+"-"+edate.getDate()+" "+edate.getHours()+":"+edate.getMinutes();

         $("#begin_time").val(begin_time);
         $("#end_time").val(end_time);

         show_time_str(begin_time,end_time);


	    get_data_show();

    });

	$("#twelve_hour").click(function(){

    	var edate = new Date();
        var bdate = new Date(edate.getTime()-12*3600*1000);

        var begin_time = bdate.getFullYear()+"-"+(bdate.getMonth()+1)+"-"+bdate.getDate()+" "+bdate.getHours()+":"+bdate.getMinutes();
        var end_time = edate.getFullYear()+"-"+(edate.getMonth()+1)+"-"+edate.getDate()+" "+edate.getHours()+":"+edate.getMinutes();

         $("#begin_time").val(begin_time);
         $("#end_time").val(end_time);

         show_time_str(begin_time,end_time);

	    get_data_show();

    });


    $("#one_day").click(function(){
    	var edate = new Date();
        var bdate = new Date(edate.getTime()-24*3600*1000);
        var begin_time = bdate.getFullYear()+"-"+(bdate.getMonth()+1)+"-"+bdate.getDate()+" "+bdate.getHours()+":"+bdate.getMinutes();
        var end_time = edate.getFullYear()+"-"+(edate.getMonth()+1)+"-"+edate.getDate()+" "+edate.getHours()+":"+edate.getMinutes();

         $("#begin_time").val(begin_time);
         $("#end_time").val(end_time);

         show_time_str(begin_time,end_time);

    	   get_data_show();
        });

	$("#three_day").click(function(){

    	var edate = new Date();
        var bdate = new Date(edate.getTime()-3*24*3600*1000);
        var begin_time = bdate.getFullYear()+"-"+(bdate.getMonth()+1)+"-"+bdate.getDate()+" "+bdate.getHours()+":"+bdate.getMinutes();
        var end_time = edate.getFullYear()+"-"+(edate.getMonth()+1)+"-"+edate.getDate()+" "+edate.getHours()+":"+edate.getMinutes();

         $("#begin_time").val(begin_time);
         $("#end_time").val(end_time);

         show_time_str(begin_time,end_time);


	     get_data_show();

    });

    $("#seven_day").click(function(){

    	var edate = new Date();
        var bdate = new Date(edate.getTime()-7*24*3600*1000);
        var begin_time = bdate.getFullYear()+"-"+(bdate.getMonth()+1)+"-"+bdate.getDate()+" "+bdate.getHours()+":"+bdate.getMinutes();
        var end_time = edate.getFullYear()+"-"+(edate.getMonth()+1)+"-"+edate.getDate()+" "+edate.getHours()+":"+edate.getMinutes();

         $("#begin_time").val(begin_time);
         $("#end_time").val(end_time);

         show_time_str(begin_time,end_time);

	     get_data_show();

        });



});
function show_time_str(start_time,end_time){
    $("#show_btime").html(start_time+ '&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;'+end_time);
}
function format_time_str(time_str){
	var len=time_str.toString().length;
	if (len==1){
		time_str="0"+time_str;
	}
	return time_str;
}