//wuha 2013-4-7
$(function(){
	var $field	= $('input, textarea'),
		$select	= $('select'),
		$submit	= $('.sure a, .edit a'),
		$back	= $('.back a');
	$back.click(function(event){
		$field.attr('disabled', 'disabled').addClass('disable_field');
		$select.each(function(index, element) {
			var $instead	= $('input.instead');
			if ( ! $instead.length){
				$('<input>', {val:$(this).val(), 'class': 'instead disable_field', 'disabled': 'disabled'}) .insertAfter($(this));
			}else{
				$instead.show();
			}
		});
		$select.hide();
		event.preventDefault();//阻止默认行为
		$submit.bind('click', function(event){
			if ($field.hasClass('disable_field')){
				$field.removeAttr('disabled').removeClass('disable_field');
				$('input.instead') .hide();
				$select.show();
				event.preventDefault();
			}else{
				$(this).bind('click', function(){});
			}
		});
	}).trigger('click');
});