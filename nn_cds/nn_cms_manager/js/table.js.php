<?php
session_start();
include_once ("../nncms_manager_inc.php");

//加载多语言
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//导入语言包
 if (!empty($_SESSION["language_dir"])){
	$language_dir=$_SESSION["language_dir"];
 } 
//include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
?>
var _formobj;
var _selectfunc;
$(document).ready(function(){

						   $(".content_table > table > tbody > tr:odd").addClass("tr_odd");

							$(".selecttable table > tbody > .selecttr").click(function(){

																	selectTableItem($(this));
																	});

							$(".content_table table:has('> thead')").find(" > tbody > tr").bind("mouseover",function(){

								$(this).addClass("tr_over");
							 });

							 $(".content_table table:has('thead')").find(" > tbody > tr").bind("mouseout",function(){

								$(this).removeClass("tr_over");
							 });



							$(".controlbtns .controlbtn a").click(function(){
								if("function"==typeof user_controlBtnClick) {
									user_controlBtnClick($(this));
								}else{
									controlBtnClick($(this));
								}
							});
							$(".formtable table > thead > tr  input[type=checkbox]").click(function(){
								var bool;
									if ($(this).attr("checked")=="checked"){
										bool=true;
									}else{
										bool=false;
									}
								selectAllCheckBox(bool);
							});
							var alt_len=$("alt").length;
							for (var i=0;i<alt_len;i++){
								create_alt_box($("alt:eq("+i+")"));
							}


							$("alt").live("mouseover",function(){
								show_alt($(this));
							});
							$("alt").live("mouseout",function(){
								remove_alt_box($(this));
							});

							create_control_btns();

							//$(".formtable table > tbody > tr > td:has(a)").css("white-space","nowrap");

						   });

function show_alt($obj){
	var index=$("alt").index($obj);
	var $alt_box=$(".alt_box:eq("+index+")");
	var top=$obj.offset().top+$obj.height();
	var left=$obj.offset().left+$obj.width();
	if(top+$alt_box.height()+20>$("body").height()){
		top=$("body").height()-$alt_box.height()-20;
	}
	if (left+$alt_box.width()+20>$("body").width()){
		left=$obj.offset().left-$alt_box.width()-20;
	}
	$alt_box.css({"left":left+"px","top":top+"px"});
	$alt_box.show();
}

function create_alt_box($obj){ 
//	var box_html="<div class='alt_box' style='position:absolute; border:2px solid #CCC; background-color:#FFF;filter:alpha(opacity=80);-moz-opacity:0.8;-khtml-opacity: 0.8;opacity: 0.8; padding:5px 10px; '><div style='color:#333; font-size:12px; line-height:20px;'>";
	var box_html="<div class='alt_box' pclass='"+$obj.attr("class")+"' style='position:absolute; border:2px solid #CCC; background-color:#FFF; padding:5px 10px; z-index:100; '><div style='color:#333; font-size:12px; line-height:20px;'>";
	box_html+=$obj.attr("alt");
	box_html+="</div></div>";
	$("body").append(box_html);
	$(".alt_box").hide();


	return $(".alt_box").eq($(".alt_box").length-1);
}
function remove_alt_box($obj){
	var index=$("alt").index($obj);
	var $alt_box=$(".alt_box:eq("+index+")");
	$alt_box.hide();
}
function selectAllCheckBox(bool){

	$(".formtable table > thead > tr input[type=checkbox]").attr("checked",bool);
	$(".formtable table > tbody >tr > td > input[type=checkbox]:not(input[disabled])").attr("checked",bool);
}
function selectTableItem($obj){
	$(".formtable table > tbody > tr").removeClass("select");
	$obj.addClass("select");
	var bool=$obj.find("input[type=checkbox]").attr("checked");
	$obj.find("input[type=checkbox]").attr("checked",!bool);
	if (_selectfunc) _selectfunc($obj);
}

function controlBtnClick($obj){
	var path=$obj.attr("pos");
	var action=$obj.attr("action");
	var target=$obj.attr("target");
	if (path!=undefined && path!="" && path!=null){
		if (path.indexOf("?")>0){

		path+="&action="+action;
		}else{
		path+="?action="+action;
		}
		if(action!="add"){
			var nns_id=getAllCheckBoxSelect();
			var nns_ids=nns_id.split(",");
			if (nns_id!=""){
				if(action=="unbind"){//解除绑定集团
					path+="&nns_ids="+nns_id;
				}else{
					path+="&nns_id="+nns_ids[0];
				}

			}else{
				alert("<?php echo cms_get_lang('msg_4');?>");
				return;
			}
		}
//		alert(path);
		if (target=="_parent"){
			window.parent.location.href=path;
		}else{
			window.location.href=path;
		}
	}
}

function getAllCheckBoxSelect(except,attr){
	var datastr="";
	var len=$(".formtable table > tbody > tr").length;
	for (var i=0;i<len;i++){
		$obj=$(".formtable table > tbody > tr:eq("+i+")").find("input[type=checkbox]");
		if ($obj.attr("checked")){
			if ($obj.attr(except)!="true" || except==undefined){
				if (!attr){
					datastr+=$obj.attr("value")+",";
				}else{
					datastr+=$obj.attr(attr)+",";
				}
			}
		}
	}
	return datastr;
}

function returnToHistory(){
	history.go(-1);
}

function submitForm(){
	_formobj[0].submit();
}

function resetForm($formobj){
	$formobj[0].reset();
}

function checkForm(title,content,formobj,method){

	_formobj=formobj;
	if (checkInputRules(formobj)){
		if(method=="add"){
			submitForm();
		}else{
			var cms_alert= window.top.cmsalert;

			cms_alert.showAlert(title,content,submitForm);
		}
	}
window.parent.resetFrameHeight();
}

function refresh_prepage_list(){
	setCookie("page_max_num",$("#nns_list_max_num").val());
	var url = window.location.href;

	window.location.href = url.replace(/\&page=(\d)*/i, "&page=1");

}

function  go_page_num(url,maxpage,key){
				var pagenum=$("#go_page_num").val();
				if (isNaN(pagenum) || pagenum.indexOf(".")>=0){
					alert("<?php echo cms_get_lang('msg_3');?>");
					return;
				}
				if (pagenum>maxpage){
					pagenum=maxpage;
				}
				if (pagenum<1){
					pagenum=1;
				}

				if (key==undefined || key==""){
					url=url+"&page="+pagenum;
				}else{
					url=url+"&"+key+"="+pagenum;
				}
				window.location.href = url.replace('?&','&');

 }

 function create_empty_tr(table,items,max_items){
	var len=table.find('thead tr:eq(0)').find('th').length;
	var trhtml='';
	for (var i=0;i<max_items-items;i++){
		trhtml+='<tr>';
		for (var j=0;j<len;j++){
			trhtml+='<td>&nbsp;</td>';
		}
		trhtml+='</tr>';
	}
	table.find('tbody').append(trhtml);
	  $(".content_table > table > tbody > tr:odd").addClass("tr_odd");
 }

 function get_current_addr(){
	var href=window.location.href;
	var url_arr=href.split("/");
	url_arr.splice(-1,1);
	var current_addr=url_arr.join("/");
	return current_addr;
 }


 function create_control_btns(){
	var len=$(".control_btns").length;
	for (var i=0;i<len;i++){
		var btns=$(".control_btns:eq("+i+")").html();
		var lena=$(".control_btns:eq("+i+")").find("a").length;
		btns=btns.replace(/<a\s*/g,'<br><a ');
		btns=btns.replace('<br>','');
		var wid=0;

		var alt_str='<div style="padding:5px;"><div style="font-size:16px;font-weight:bold;line-height:22px;">'+btns+'</div></div>';
		var html_str='<span style="cursor:pointer;" class="control_btn" >'+$(".control_btns:eq("+i+")").attr("name");

		var box_html="<div class='control_btns_box'  style='position:absolute;white-space:nowrap; border:2px solid #CCC; background-color:#FFF; padding:5px 10px;z-index:999;'>";
		box_html+=btns;
		box_html+="</div></span>";

		$(".control_btns:eq("+i+")").html(html_str+box_html);

	}

	$(".control_btns_box").hide();

	$(".control_btn").live("mouseover",function(){
		show_alt_btns($(this));
	});
	$(".control_btn").live("mouseleave",function(){
		var index=$(".control_btn").index($(this));
		var $alt_box=$(".control_btns_box").eq(index);
		$alt_box.hide();
		//$(this).hide();
	});
	/*$(".alt_box[pclass='control_btn']").live("mouseout",function(){
		$(this).hide();
	});*/
 }

 function show_alt_btns($obj){

	var index=$(".control_btn").index($obj);
	var $alt_box=$(".control_btns_box").eq(index);
		//alert($alt_box);
	//var top=$obj.offset().top-($alt_box.height()-$obj.height())/2-10;
	var top=$obj.offset().top-7;
	var left=$obj.offset().left-$alt_box.width()+$obj.width()-10;
	if(top+$alt_box.height()+20>$("body").height()){
		top=$("body").height()-$alt_box.height()-20;
	}

	$alt_box.css({"left":left+"px","top":top+"px"});
	$alt_box.show();
 }

  function htmlspecialchars(str)
	{
		var s = "";
		if (str.length == 0) return "";
		for   (var i=0; i<str.length; i++)
		{
			switch (str.substr(i,1))
			{
				case "<": s += "&lt;"; break;
				case ">": s += "&gt;"; break;
				case "&": s += "&amp;"; break;
			   /* case " ":
					if(str.substr(i + 1, 1) == " "){
						s += " &nbsp;";
						i++;
					} else s += " ";
					break;*/
				case "\"": s += "&quot;"; break;
				case "\n": s += ""; break;
				default: s += str.substr(i,1); break;
			}
		}
		return s;
	}

