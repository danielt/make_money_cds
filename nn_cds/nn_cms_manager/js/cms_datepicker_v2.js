function add_js(src) {
    var js = document.createElement('script');
    js.src = src;
    js.type="text/javascript";
    try {
        document.getElementsByTagName('head')[0].appendChild(js);
    } catch (exp) {}
    js = null;
}
add_link('../../js/calendar/jscal2.css');

add_link('../../js/calendar/border-radius.css');
add_link('../../js/calendar/win2k.css');
add_js('../../js/calendar/calendar.js');
add_js('../../js/calendar/calendar.lang.js.php');
function add_link(src) {
    var link = document.createElement('link');
    link.href = src;
    link.type="text/css";
    link.rel="stylesheet";
    try {
        document.getElementsByTagName('head')[0].appendChild(link);
    } catch (exp) {}
    link = null;
}


function create_date_picker(){

    if (Calendar){

        var len= $("input.datepicker").length;
        var len1= $("input.datetimepicker").length;
        for (var i=0;i<len;i++){
            Calendar.setup({
                weekNumbers: true,
                inputField : $("input.datepicker:eq("+i+")").attr("name"),
                trigger    : $("input.datepicker:eq("+i+")").attr("name"),
                // dateFormat: "%Y-%m-%d",
                showTime: false,
                minuteStep: 1,
                onSelect   : function() {
                    this.hide();
                }
            });
        }
        for (var j=0;j<len1;j++){
            Calendar.setup({
                weekNumbers: true,
                inputField : $("input.datetimepicker:eq("+j+")").attr("name"),
                trigger    : $("input.datetimepicker:eq("+j+")").attr("name"),
                // dateFormat: "%Y-%m-%d %H:%M:%S",
                showTime: false,
                minuteStep: 1,
                onSelect   : function() {
                    this.hide();

                }
            });
        }
        clearInterval(intervalId);
    }
}
var intervalId;
$(document).ready(function(){
    intervalId=setInterval(create_date_picker,500);


    // $("input.datepicker").datepicker({
    //          dateFormat: 'yy-mm-dd',  //日期格式，自己设置
    //          clearText:'清除',
    //          closeText:'关闭',
    //          prevText:'前一月',
    //          nextText:'后一月',
    //          currentText:' ',
    // 	   dayNamesMin:['日','一','二','三','四','五','六'],
    //          monthNames:['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
    //          monthNamesShort:['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
    //          changeMonth:true,
    //          changeYear:true,
    //          beforeShow: function () {
    //           setTimeout(
    //               function () {
    //                   $('#ui-datepicker-div').css("z-index", 999999);
    //               }, 100
    //           );
    //       }
    // });
});