/**
 *
 */
function create_linechat(div_id,obj_id,width,height,init_callback,bgcolor){
	 var swfVersionStr = "10.2.0";
	 if (!bgcolor) bgcolor="#ffffff";
     var flashvars = {};
     flashvars.init=init_callback;
     var params = {};
     params.quality = "high";
     params.bgcolor = bgcolor;
     params.allowscriptaccess = "always";
     params.allowfullscreen = "true";
     params.wmode="transparent";
     var attributes = {};
     attributes.id = obj_id;
     attributes.name = obj_id;
     attributes.align = "middle";
     swfobject.embedSWF(
         "../../js/lineChatTable.swf", div_id,
         width, height,
         swfVersionStr, "",
         flashvars, params, attributes);
     swfobject.createCSS("#"+div_id, "display:block;text-align:left;");
}

