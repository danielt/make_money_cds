<?php
session_start();
include("../nncms_manager_inc.php");
 //导入语言包
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
?>
function checkInputRules($form){
	if ($form){
		$check_obj1=$form.find("input[type=text],input[type=password]");
		$check_obj2=$form.find("select");
	}else{
		$check_obj1=$("input");
		$check_obj2=$("select");
	}
	var len=$check_obj1.length;
	var len1=$check_obj2.length;
	var bool=true;
	for (var i=0;i<len;i++){
		var rule=$check_obj1.eq(i).attr("rule");
		if (rule!=undefined && rule!=""){
			if (!splitRule(rule,$check_obj1.eq(i))) bool=false;
		}
		var sameto=$check_obj1.eq(i).attr("sameto");
		if (sameto!=undefined && sameto!=""){

			if ($("#"+sameto).val()!=$check_obj1.eq(i).val()){
				setInputWarning("<?php echo cms_get_lang('msg_1');?>",$check_obj1.eq(i));
				bool=false;
			}
		}

		var overto=$check_obj1.eq(i).attr("overto");
		if (overto!=undefined && overto!=""){
			if ($("#"+overto).val()!="" || $check_obj1.eq(i).val()!=""){
				if ($("#"+overto).val()>=$check_obj1.eq(i).val()){
					setInputWarning("<?php echo cms_get_lang('msg_11');?>",$check_obj1.eq(i));
					bool=false;
				}
			}
		}

		var len_num=$check_obj1.eq(i).attr("len");
		if (len_num){
			if ($check_obj1.eq(i).val().length!=len_num){
				setInputWarning("<?php echo cms_get_lang('msg_9');?>",$check_obj1.eq(i));
				bool=false;
			}
		}
	}
	for (var j=0;j<len1;j++){
		var rule=$check_obj2.eq(j).attr("rule");
		if (rule!=undefined && rule!=""){
			if (!splitRule(rule,$check_obj2.eq(j))) bool=false;
		}
	}

	return bool;
}

function splitRule(rulestr,$obj){
	var rules=rulestr.split("|");
	var m=rules.length;
	var bool=true;
	for (var j=0;j<m;j++){
		if (!checkRule(rules[j],$obj)){
			 bool=false;
			 return bool;
		}
	}
	return bool;
}

function checkRule(rule,$obj){
	var bool=true;
	switch(rule){
		case "noempty":
			if ($.trim($obj.val())==""){
				setInputWarning("<?php echo cms_get_lang('msg_2');?>",$obj);
				bool=false;
				return bool;
			}
		break;
		case "int":
			var int1=$obj.attr("value");
			if(isNaN(int1) || int1.indexOf(".")>=0){

				setInputWarning("<?php echo cms_get_lang('msg_3');?>",$obj);
				bool=false;
				return bool;
			}
		break;
		case "uint":
			var int1=$obj.attr("value");
			if(isNaN(int1) || int1.indexOf(".")>=0 || int1<=0){

				setInputWarning("<?php echo cms_get_lang('msg_13');?>",$obj);
				bool=false;
				return bool;
			}
		break;
		case "float":
			var int1=$obj.attr("value");
			if(isNaN(int1)){

				setInputWarning("<?php echo cms_get_lang('msg_3');?>",$obj);
				bool=false;
				return bool;
			}
		break;
		case "phone":
			bool=isTel($obj.attr("value"));
			if (!bool){
				setInputWarning("<?php echo cms_get_lang('msg_6');?>",$obj);
				bool=false;
				return bool;
			}
		break;
		case "email":
			bool=emailCheck($obj.attr("value"));
			if (!bool){
				setInputWarning("<?php echo cms_get_lang('msg_7');?>",$obj);
				bool = false;
				return bool;
			}
		break;
		case "no_ch"://不含中文
		bool=  hasChinese($obj.attr("value"));
		if(!bool){
			setInputWarning("不能含中文",$obj);return false;
		}
		break;
	}
	 $obj.parent().find("span").html("");
	return bool;
}

function setInputWarning(waining,$obj){
	var html;
	if ($obj.parent().find("span").length==0){
		$obj.parent().append("<span style='color:#ff0000'>&nbsp "+waining+"</span>");
	}else{
		$obj.parent().find("span").html("&nbsp "+waining);
	}
}
function hasChinese(str){//包含中文
	if(/.*[\u4e00-\u9fa5]+.*$/.test(str)) {
		return false;
	}
	return true;
}
/* 2012-8-20 陈波 1661507441@qq.com 修改对电话号、email的验证方式   start*/
//检查电话号码
function isTel(str){
	if(str.length == 0){
		return true;
	}
	var reg=/^([0-9]|[\-])+$/g ;
	if(str.length<7 || str.length>18){
		return false;
	}
	if(!reg.exec(str)){
		return false;
	}
	return true;
}

function emailCheck (str) {
	var emailStr=str;
	if(emailStr.length ==0 ){
		return true;
	}
	var emailPat=/^(.+)@(.+)$/;
	var matchArray=emailStr.match(emailPat);
	if (matchArray==null) {
		return false;
	}
	return true;
}

/* 2012-8-20 陈波 1661507441@qq.com 修改对电话号、email的验证方式   start*/