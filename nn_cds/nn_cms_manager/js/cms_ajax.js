﻿function getAjax(ajax_url,func){
	//alert(ajax_url);
	$.ajax({
		url:ajax_url+"&rand="+Math.random(),
		type:"get",
		dataType:"json",
		cache:false,
		timeout: 10000,
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			//alert(XMLHttpRequest.responseText);
//			alert(XMLHttpRequest.status);
//			alert(textStatus);
//			alert(errorThrown);
		},
		success: function(response) {
			//alert(response);
			func(response);
		}
	});

}