function mytabs($tabs,callback){
	var _select_id=0;
	$tabs.find(".state_tab").click(function(){
		select_obj($(this));
	});
	
	function select_obj($obj){
		_select_id=$tabs.find(".state_tab").index($obj);
		$tabs.find(".state_tab").removeClass("select");
		$obj.addClass("select");
		if (callback){
			callback($obj.attr("pos"));
		}
	}
	
	this.get_select_id=function(){
		return _select_id;
	}
	
	this.init=function(){
		$tabs.find(".state_tab").removeClass("select");
		$tabs.find(".state_tab:eq(0)").addClass("select");
		if (callback){
			callback($tabs.find(".state_tab:eq(0)").attr("pos"));
		}
	}
}
