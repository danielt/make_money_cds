<?php
include("../nncms_manager_inc.php");
 //导入语言包
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
include $nncms_config_path. "nn_cms_manager/languages/". $language_dir. "/language.php";
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
?>
var old_public_category_html;
$(document).ready(function(){
//	$("#category_public_id_tr").hide();
//	$("#nns_vod_category_public").vod("change",function(){
//		ajaxCategoryList(true,$(this).val());
//
//	});
//
//	$("#nns_vod_category_private").vod("change",function(){
//		ajaxCategoryList(false,$(this).val());
//
//	});



//	选择集团弹窗
$(".selectbox").hide();

//		var $obj=$("#nns_carrier").find("option:selected");
//			var index=$("#nns_carrier option").index($obj);
			selectCarrier(<?php if(isset($_SESSION["nns_manager_type"])) echo $_SESSION["nns_manager_type"];?>);


		$("#nns_carrier").change(function(){
			var $obj=$(this).find("option:selected");
			var index=$("#nns_carrier option").index($obj);
			selectCarrier(index);
		});
		$(".selectbox").css("left",($("body").width()-$(".selectbox").width())/2);
		$(".selectbox").css("top",50);
//		window.parent.resetFrameHeight();
});

	function ajaxCategoryList(method,id){

		var ajaxpath="../../controls/nncms_controls_ajax_category.php?category_type=1&category_list="+id;
		if (method){

		getAjax(ajaxpath,responsePublicData);
		}else{
		getAjax(ajaxpath,responsePrivateData);
		}
	}

	function responsePrivateData(response){

		$("#category_private_id_tr").hide();
		if (response.ret==0){
			var objarr=response.data;
			category_html="";
			for(ind in objarr){
				var obj=objarr[ind];
				if (obj.nns_id==$("#nns_vod_category_private_id").attr("default")){
					category_html+="<option value=\""+obj.nns_id+"\" selected>"+obj.nns_name+"</option>";
				}else{
					category_html+="<option value=\""+obj.nns_id+"\">"+obj.nns_name+"</option>";
				}
			}
			$("#nns_vod_category_private_id").html(category_html);
			$("#category_private_id_tr").show();
			window.parent.resetFrameHeight();
			$("#nns_vod_category_private").parent().find("span").html("");
		}else{
			setInputWarning("<?php echo cms_get_lang('msg_8');?>",$("#nns_vod_category_private"));
		}
	}
	function responsePublicData(response){

//		$("#category_public_id_tr").hide();
		if (response.ret==0){
			var objarr=response.data;
			category_html="";
			var num=0;
			for(ind in objarr){
				num++;
				var obj=objarr[ind];
				if (obj.nns_id==$("#nns_vod_category_public_id").attr("default")){
					category_html+="<option value=\""+obj.nns_id+"\" selected>"+obj.nns_name+"</option>";
				}else{
					category_html+="<option value=\""+obj.nns_id+"\">"+obj.nns_name+"</option>";
				}
			}
			$("#nns_vod_category_public_id").html(category_html);
			if (num>0){
			$("#category_public_id_tr").show();

			}else{
				$("#category_public_id_tr").hide();
			}
				window.parent.resetFrameHeight();
			$("#nns_vod_category_public").parent().find("span").html("");

		}else{

			$("#nns_vod_category_public_id").html("");
			setInputWarning("<?php echo cms_get_lang('msg_8');?>",$("#nns_vod_category_public"));
		}
	}


	function selectCarrier(num){

		$("#nns_partner_control").hide();
		$("#nns_group_control").hide();
//		$("#category_public_tr").hide();
//		$("#category_public_id_tr").hide();
		$("#nns_group_input").attr("rule","");
		$("#nns_partner_input").attr("rule","");
//		$("#category_private_tr").hide();
//		$("#category_private_id_tr").hide();

		switch(num){
			case 1:
			$("#nns_partner_input").attr("rule","noempty");
			$("#nns_partner_control").show();
//			$("#category_public_id_tr").show();
//			$("#category_public_tr").show();

//			ajaxCategoryList(true,$("#nns_vod_category_public").val());
//		ajaxCategoryList(false,$("#nns_vod_category_private").val());
			window.parent.checkheight();


			break;
			case 2:
			$("#nns_group_input").attr("rule","noempty");
			$("#nns_group_control").show();
//			$("#category_private_tr").show();
//			ajaxCategoryList(false,$("#nns_vod_category_private").val());
			window.parent.checkheight();

			break;
			default:

//			$("#category_public_id_tr").show();
//			$("#category_public_tr").show();
//			$("#nns_vod_category_public").html(old_public_category_html);
//			ajaxCategoryList(true,$("#nns_vod_category_public").val());
			break;
		}
	}

	function begin_select_partner(){
		$("#select_frame").attr("src","../../controls/nncms_controls_select.php?method=partner");

		$(".selectbox").show();
	}

	function begin_select_group(){
		$("#select_frame").attr("src","../../controls/nncms_controls_select.php?method=group");
		$(".selectbox").show();
	}
	function close_select(){
		$(".selectbox").hide();
	}

	function set_partner_id(value,name){
		$("#nns_partner").attr("value",value);
		$("#nns_partner_input").attr("value",name);
//		set_private_category_list_combobox(value,1);
//
//		$("#category_private_tr").hide();
//		$("#category_private_id_tr").hide();
//		window.parent.resetFrameHeight();
	}

	function set_private_category_list_combobox(value,num){
		var ajaxpath="../../controls/nncms_controls_ajax_category.php?category_type=1&group_type="+num+"&group_id="+value;

		getAjax(ajaxpath,setPrivateCategoryList);
	}

	function set_group_id(value,name){
		$("#nns_group").attr("value",value);
		$("#nns_group_input").attr("value",name);
//		set_private_category_list_combobox(value,2);
//		$("#category_private_tr").hide();
//		$("#category_private_id_tr").hide();
//		window.parent.resetFrameHeight();
	}

	function setPrivateCategoryList(response){
//		$("#category_private_tr").hide();

		if (response.ret==0){
			$("#category_private_tr").show();
//			alert(222);
			var objarr=response.data;
			category_html="";
			var num=0;
			for(ind in objarr){
				num++;
				var obj=objarr[ind];
				if (obj.nns_id==$("#nns_vod_category_private").attr("default")){
					category_html+="<option value=\""+obj.nns_id+"\" selected>"+obj.nns_name+"</option>";
				}else{
					category_html+="<option value=\""+obj.nns_id+"\">"+obj.nns_name+"</option>";
				}
			}
			if (obj.nns_type==1){
				category_html="<option value=\"0\"><?php echo cms_get_lang('media_lm_bslm');?></option>"+category_html;
					if ($("#nns_vod_category_public").find("option:eq(0)").val()!=0){

						old_public_category_html=$("#nns_vod_category_public").html();

						var category_public_html="<option value=\"0\"><?php echo cms_get_lang('media_lm_bslm');?></option>"+old_public_category_html;
						$("#nns_vod_category_public").html(category_public_html);
//						ajaxCategoryList(true,0);
					}
			}

			$("#nns_vod_category_private").html(category_html);


			window.parent.resetFrameHeight();
			$("#nns_vod_category_private").parent().find("span").html("");
			ajaxCategoryList(false,$("#nns_vod_category_private").val());
		}else{
			$("#nns_vod_category_private").html("");
			$("#nns_vod_category_private_id").html("");
			setInputWarning("<?php echo cms_get_lang('msg_8');?>",$("#nns_vod_category_private"));
		}
	}
	function checkheight(){
		$(".selectbox iframe").height($(".selectbox iframe").contents().find("body").attr("offsetHeight"));
	}