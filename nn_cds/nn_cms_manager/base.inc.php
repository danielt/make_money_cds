<?php
/*
 * Created on 2013-4-16
 *
 *
 *
 */
include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "nncms_manager_inc.php";
// 导入多语言包
include_once ($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include_once ($nncms_config_path . "nn_cms_config/nn_cms_global.php");
include_once $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
/**
 * 加载LOGIC目录中文件
 *
 * @param mixed $logic_name 逻辑库文件名(不包含.class.php)
 * @param mixed $logic_dir 逻辑库文件夹名(不含separator)
 * @return void 无返回值
 */
function load_logic($logic_name, $logic_dir = null) {
	$ext = '.class.php';
	$include_file_path = LOGIC_DIR . $logic_dir . '/' . $logic_name . $ext;
	if (!file_exists($include_file_path)) {
		die('file ' . str_replace(LOGIC_DIR, '', $include_file_path) . ' not find');
	}
	require_once $include_file_path;
}

/**
 * 加载dc
 * @return obj
 */
function load_dc() {
	$include_file_path = LOGIC_DIR . 'nl_common.func.php';
	if (!file_exists($include_file_path)) {
		die('file ' . str_replace(LOGIC_DIR, '', $include_file_path) . ' not find');
	}
	require_once $include_file_path;
	$dc = nl_get_dc(array("db_policy" => NL_DB_WRITE, "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE));
	$dc ->open();
	return $dc;
}
function get_gpc($k,$req='request'){
	if(!$k) return null;
	switch($req){
		case 'get':
		    return isset($_GET[$k])?$_GET[$k]:null;
		case 'post':
		    return isset($_POST[$k])?$_POST[$k]:null;
		case 'session':
		    return isset($_SESSION[$k])?$_SESSION[$k]:null;
		case 'cookie':
		    return isset($_COOKIE[$k])?$_COOKIE[$k]:null;
		default:
		    return isset($_REQUEST[$k])?$_REQUEST[$k]:null;
	}
	return null;
}
?>
