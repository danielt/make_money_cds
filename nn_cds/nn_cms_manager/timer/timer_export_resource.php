<?php
/*
 * Created on 2012-8-21
 * 	BY S67
 * 导出资源记录
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 header("Content-Type:text/html ;charset=utf-8");
 error_reporting(E_ALL ^ E_NOTICE);
 include '../controls/nncms_control_resources_record.php';

 $resource_record_inst=new nncms_control_resources_record();

// 定时导出媒资包资源
 include '../../nn_cms_db/nns_assist/nns_db_assist_item_class.php';
 include '../../nn_cms_db/nns_assist/nns_db_assist_class.php';


 $asset_inst=new nns_db_assist_class();
 $asset_result=$asset_inst->nns_db_assist_list(null,null);

 $asset_item_inst=new nns_db_assist_item_class();

 foreach($asset_result['data'] as $asset_item){
	$dom = new DOMDocument('1.0', 'utf-8');
	$dom->loadXML($asset_item['nns_category']);
 	create_file_record_by_asset($asset_item['nns_id'],$asset_item['nns_org_id'],$asset_item['nns_org_type'],$asset_item['nns_name'],$dom);
 }
 $asset_inst=null;
 $asset_item_inst=null;

 function create_file_record_by_asset($asset_id,$org_id,$org_type,$asset_name,$xml){
 	global $resource_record_inst;
 	global $asset_item_inst;

 	$resource_record_inst->save_packet_info(array(
		'packet_id'=>$asset_id,
		'packet_name'=>$asset_name,
		'packet_type'=>'0',
		'group_id'=>$org_id,
		'group_type'=>$org_type
	));

 	$asset_item_result=$asset_item_inst->nns_db_assist_item_list(0,0,null,$asset_id);
 	foreach($asset_item_result['data'] as $asset_item){
 		$params=array();
 		$params['video_id']=$asset_item['nns_video_id'];
		$params['video_type']=$asset_item['nns_video_type'];
		$params['video_name']=$asset_item['nns_video_name'];
		$params['category_id']=$asset_item['nns_category_id'];
		$params['category_name']=get_categoy_name_by_xml($asset_item['nns_category_id'],$xml);
		$params['action']='info';
 		$resource_record_inst->save_action($params);
 	}
 	$resource_record_inst->submit_actions('asset');
 	$resource_record_inst->clear_actions();
 }
 
 function get_categoy_name_by_xml($category_id,$xml){
	$category = $xml->getElementsByTagName('category');
	foreach($category as $item){

	        	 	if ($item->getAttribute("id")===$category_id){
	        	 		return $item->getAttribute("name");
	        	 		break;
	        	 	}
	        	 }
 }

 // 定时导出服务包资源
 include '../../nn_cms_db/nns_service/nns_db_service_category_class.php';
 include '../../nn_cms_db/nns_service/nns_db_service_class.php';

 $service_inst=new nns_db_service_class();
 $service_result=$service_inst->nns_db_service_list(null,null);

 $service_category_inst=new nns_db_service_category_class();

 foreach($service_result['data'] as $service_item){
 	$dom = new DOMDocument('1.0', 'utf-8');
	$dom->loadXML($service_item['nns_category']);
 	create_file_record_by_service($service_item['nns_id'],$service_item['nns_org_id'],$service_item['nns_org_type'],$service_item['nns_name'],$dom);
 }
 $service_inst=null;
 $service_category_inst=null;

 function create_file_record_by_service($service_id,$org_id,$org_type,$service_name,$xml){
 	global $resource_record_inst;
 	global $service_category_inst;

 	$resource_record_inst->save_packet_info(array(
		'packet_id'=>$service_id,
		'packet_type'=>'1',
		'packet_name'=>$service_name,
		'group_id'=>$org_id,
		'group_type'=>$org_type
	));

 	$service_category_result=$service_category_inst->nns_db_service_category_list(0,0,null,null,$service_id);
 	foreach($service_category_result['data'] as $service_category){
 		$params=array();
 		$params['video_id']=$service_category['nns_video_id'];
		$params['video_type']=$service_category['nns_video_type'];
		$params['video_name']=$service_category['nns_video_name'];
		$params['category_id']=$service_category['nns_category_id'];
		$params['category_name']=get_categoy_name_by_xml($service_category['nns_category_id'],$xml);
		$params['action']='info';
 		$resource_record_inst->save_action($params);
 	}
 	$resource_record_inst->submit_actions('service');
 	$resource_record_inst->clear_actions();
 }

?>