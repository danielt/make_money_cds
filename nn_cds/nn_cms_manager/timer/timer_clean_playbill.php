<?php
/*
 * Created on 2013-1-17
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 include '../../nn_logic/nl_common.func.php';
 include '../../nn_cms_config/nn_cms_global.php';
 $dc=nl_get_dc( array(
'db_policy'=>NL_DB_WRITE,
'cache_policy'=>NP_KV_CACHE_TYPE_NULL
));

$dc->open();
$exists_day=g_cms_config::get_g_config_value('g_playbill_exists_day');

$clean_sql='delete b from nns_live_playbill_item a,' .
		'nns_live_playbill_item_language b ' .
		'where ' .
		'a.nns_id=b.nns_playbill_id and ' .
		'(a.nns_begin_time > \''.date('Y-m-d',strtotime('+'.($exists_day+1).' day')).'\' or ' .
		'a.nns_begin_time < \''.date('Y-m-d',strtotime('-'.$exists_day.' day')).'\') ';
echo $clean_sql;		
nl_execute_by_db($clean_sql,$dc->db());

$clean_sql='delete b from nns_live_playbill_item a,' .
		'nns_assists_item b ' .
		'where ' .
		'a.nns_id=b.nns_video_id and ' .
		'(a.nns_begin_time > \''.date('Y-m-d',strtotime('+'.($exists_day+1).' day')).'\' or ' .
		'a.nns_begin_time < \''.date('Y-m-d',strtotime('-'.$exists_day.' day')).'\') ';
echo $clean_sql;		
nl_execute_by_db($clean_sql,$dc->db());

$clean_sql='delete b from nns_live_playbill_item a,' .
		'nns_video_label_bind_content b ' .
		'where ' .
		'a.nns_id=b.nns_video_id and ' .
		'(a.nns_begin_time > \''.date('Y-m-d',strtotime('+'.($exists_day+1).' day')).'\' or ' .
		'a.nns_begin_time < \''.date('Y-m-d',strtotime('-'.$exists_day.' day')).'\') ';
echo $clean_sql;		
nl_execute_by_db($clean_sql,$dc->db());

$clean_sql='delete a from nns_live_playbill_item a ' .
		'where ' .
		'(a.nns_begin_time > \''.date('Y-m-d',strtotime('+'.($exists_day+1).' day')).'\' or ' .
		'a.nns_begin_time < \''.date('Y-m-d',strtotime('-'.$exists_day.' day')).'\') ';
echo $clean_sql;			
nl_execute_by_db($clean_sql,$dc->db());
?>
