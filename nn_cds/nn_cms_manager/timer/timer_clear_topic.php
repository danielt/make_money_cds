<?php
/**
 * 定期删除信息包记录
 */
//配置cms服务器目录
$cms_dir="/data/web/nn_cms/";
date_default_timezone_set("PRC"); //设置时区（中国）
include_once $cms_dir.'nn_logic/nl_common.func.php';
include_once $cms_dir.'nn_cms_config/child_config/nn_cms_config_cyyy.php';
$config=array(
	"db_policy"=>NL_DB_WRITE,
	"cache_policy"=>NP_KV_CACHE_TYPE_MEMCACHE,
);
$dc=nl_get_dc($config,$params=NULL);
$dc->open();
$db=$dc->db();
$param=parse_ini_file("{$cms_dir}nn_cms_manager/timer/timer_clear_topic.ini",true);
foreach ($param as $val){
	clear($val['topic_id'],$val['save_time'],$cms_dir,$db);
}
/**
 * 根据时间返回信息内容ids
 * @param $time datetime:[2012-12-12 12:12:12]
 * @param $ids array:一维数组,没有返回null
 */
function get_item_ids($db,$info_id,$time=null){
	$sql="select nns_id from nns_topic_item where nns_topic_id='{$info_id}' and nns_create_time<='{$time}'";
	//返回二维数组
	$nns_id_arr=nl_query_by_db($sql, $db);
	$ids=array();
	if(is_array($nns_id_arr)){
		foreach ($nns_id_arr as $val){
			$ids[]=$val['nns_id'];
		}
	}else{
		$ids=null;
	}
	return $ids;
}
/**
 * 删除指定时间以前的记录
 * @param $time datetime:[2012-12-12 12:12:12]
 */
function del_item($db,$info_id,$time=null){
	$sql="delete from nns_topic_item where nns_topic_id='{$info_id}' nns_create_time<='{$time}'";
	return nl_execute_by_db($sql, $db);
}
/**
 * 根据内容ids获取图片名称（存放地址）
 * @param $item_ids:内容ids[一维数组]
 * @return $img_ids；一维数组，如果没有返回null
 */
function get_imgs($db,$item_ids=array()){
	$img_ids=null;//一维数组；
	foreach ($item_ids as $item_id){
		$sql="select nns_image from nns_topic_item_images where nns_topic_item_id='{$item_id}'";
		$id=nl_query_by_db($sql, $db);
		if(is_array($id)){
			$img_ids[]=$id[0]['nns_id'];
		}
	}
	return $img_ids;
}
/**
 * 根据内容id删除图片记录
 * @param $item_ids:一维数组
 */
function del_img($db,$item_ids=array()){
	foreach ($item_ids as $item_id){
		$sql=" delete from nns_topic_item_images where nns_topic_item_id='{$item_id}'";
		nl_execute_by_db($sql, $db);
	}
}
/**
 * 根据图片名称，删除物理图片
 * @param $pre:nn_cms的路径
 * @param $imgs:一维数组
 */
function del_phy_img($cms_dir="",$imgs=array()){
	global $g_syn_data_path;
	$imgs=explode(";", $g_syn_data_path);
	foreach($imgs as $img_name){
		unlink($cms_dir.$img_name);
		if(is_array($imgs)){
			foreach ($imgs as $img_dev){
				unlink($img_dev.$img_name);
			}
		}
	}
}
/**
 *根据信息包id和保存时间执行清理操作 
 */
function clear($info_id,$save_time,$cms_dir,$db){
	//获取保存天数
	$t=date("Y-m-d H:i:s",strtotime("-{$save_time}day"));
	//获取超出时间的内容id[一维数组]
	$item_ids=get_item_ids($db,$info_id,$t);
	if($item_ids===NULL){
		return ;
	}
	//删除超出时间的记录，
	del_item($db,$info_id,$t);
	//根据内容id获取图片记录，一维数组或者null
	$img_name_arr=get_imgs($db,$item_ids);
	if($img_name_arr===NULL){
		return ;
	}
	//删除图片数据记录
	del_img($db,$item_ids);
	//删除物理图片
	del_phy_img($cms_dir,$img_name_arr);
}