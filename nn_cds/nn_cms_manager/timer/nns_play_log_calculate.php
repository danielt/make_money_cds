<?php
/*
 * Created on 2012-6-3
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
  error_reporting(E_ALL ^ E_NOTICE);
 require_once "../../nn_cms_db/nns_log/nns_db_play_log_class.php";
 require_once "../../nn_cms_db/nns_assist/nns_db_assist_item_class.php";
 require_once "../../nn_cms_db/nns_service/nns_db_service_category_class.php";
 require_once "../../nn_cms_db/nns_common/nns_db_class.php";
 require_once "../../nn_cms_config/nn_cms_global.php";

// 需要统计的媒资包数组
 $calculate_array=array(
 	array(
 		"media_asset_id"=>"high_video",
 		"category_id"=>null
 	),
 	array(
 		"media_asset_id"=>"high_live",
 		"category_id"=>null
 	)
 );

  $db_obj = new nns_db_class ( g_db_host, g_db_username, g_db_password, g_db_name );
  $conn = $db_obj->nns_db_connect ();


// 清除当天播放次数
 $asset_inst=new nns_db_assist_item_class();
 $asset_inst->nns_db_clear_play_day_count();
 $service_inst=new nns_db_service_category_class();
  $service_inst->nns_db_clear_play_day_count();

 foreach ($calculate_array as $calculate_item){
 	$media_asset_id=$calculate_item["media_asset_id"];
 	$category_id=$calculate_item["category_id"];
 	$top_100_total_arr=$asset_inst->nns_db_total_play_top100_list($media_asset_id,$category_id);
 	$top_100_day_arr=$asset_inst->nns_db_day_play_top100_list($media_asset_id,$category_id);

 	// 更新前100总排名
 	if ($top_100_total_arr["ret"]==0){
 		$top_100_total_data=$top_100_total_arr["data"];
	 	$delete_top_100_total_sql=get_delete_sql("asset","total",$media_asset_id,$category_id);
	 	$db_obj->nns_db_execute($delete_top_100_total_sql);

	 	$total_order=0;
	 	foreach ($top_100_total_data as $top_100_total_item){
	 		$total_order++;
	 		$insert_top_100_total_sql=get_insert_sql("asset","total",$top_100_total_item,$total_order,$media_asset_id,$top_100_total_item["nns_play_count"],$top_100_total_item["nns_category_id"]);
	 		$db_obj->nns_db_execute($insert_top_100_total_sql);
	 	}
 	}

 	// 更新前100日排名
 	if ($top_100_day_arr["ret"]==0){
 		$top_100_day_data=$top_100_day_arr["data"];
	 	$delete_top_100_day_sql=get_delete_sql("asset","day",$media_asset_id,$category_id);
	 	$db_obj->nns_db_execute($delete_top_100_day_sql);

	 	$day_order=0;
	 	foreach ($top_100_day_data as $top_100_day_item){
	 		$day_order++;
	 		$insert_top_100_day_sql=get_insert_sql("asset","day",$top_100_day_item,$day_order,$media_asset_id,$top_100_total_item["nns_play_day_count"],$top_100_day_item["nns_category_id"]);
	 		$db_obj->nns_db_execute($insert_top_100_day_sql);
	 	}
 	}
 }

 // 清除播放记录
$play_log_inst=new nns_db_play_log_class();
$limit_date=date("Y-m-d H:i:s",time()-$g_play_log_limit*24*3600);
$play_log_inst->nns_db_play_log_delete($limit_date);
$play_log_inst=null;

 function get_delete_sql($method,$type,$media_asset_id,$category_id){
 	$delete_top_sql="delete from nns_media_asset_top where nns_top_type='$type' and nns_asset_id='$media_asset_id' and nns_top_method='$method'";
	 	if (!empty($category_id)){
	 		$category_str=" and nns_asset_category_id='$category_id' ";
	 	}
	 return $delete_top_sql.$category_str;
 }

 function get_insert_sql($method,$type,$top_item,$order,$media_asset_id,$play_count,$category_id=null){
 	$play_count=(int)$play_count;
 	$insert_top_sql="insert into nns_media_asset_top (nns_video_id,nns_video_type,nns_asset_id,nns_asset_category_id,nns_video_name,nns_order,nns_top_type,nns_top_method,nns_play_count)" .
	 				"values('".$top_item["nns_video_id"]."',".$top_item["nns_video_type"].",'".$media_asset_id."','".$category_id."','".$top_item["nns_video_name"]."',$order,'$type','$method',$play_count)";

	 return $insert_top_sql;
 }

?>