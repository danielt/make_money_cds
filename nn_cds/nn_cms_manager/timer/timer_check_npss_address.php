<?php
/*
 * Created on 2012-12-16
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 /**
  * 定期检查NPSS调度服务器是否正常
  */
 include_once dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.'nn_cms_config'.DIRECTORY_SEPARATOR.'nn_cms_global.php';
 $nncms_config_path=dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.'nn_cms_config'.DIRECTORY_SEPARATOR;
//	获取npss地址列表
 $npss_url_arr = explode(";", $g_core_npss_url);
 $npss_address='servers=';
 foreach ($npss_url_arr as $npss_url){
 	$npss_http_url='http://'.$npss_url.'/nn_op/test_alive';
// 	http://127.0.0.1:5100/nn_op/test_alive
 	$result=get_curl_address_state($npss_http_url);
 	if ($result['state']=='200'){
 		$npss_address.=$npss_url.';'; 	
 	}else{
 		$result['request']=$npss_http_url;
 		var_dump( $result);
 	}
 }
 $npss_address.="\r\n";
 
 $npss_info='';
 $npss_info.=$npss_address;
 write_to_active_npss_ini($npss_info);
 
 function get_curl_address_state($url){
// 	$header=get_headers($url);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_NOBODY, true);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
	
	$data = curl_exec ( $ch );
	
//	var_dump( $data);
//	var_dump(curl_getinfo($ch));
	$result=array();
	if ($data!==FALSE && $data!==''){
		
		$header=explode("\n",$data);
//		var_dump($header);
		preg_match_all('/HTTP\/[0-9.]+\s([0-9]+)\s([a-zA-Z\s]*)/',$header[0],$matchs);
		$result['state']=$matchs[1][0];
 		$result['reason']=$matchs[2][0];
	}else{
		$result['state']='404';
 		$result['reason']='Not Found';
	}

 	return $result;
 }
 
 function write_to_active_npss_ini($npss_ini){
 	global $nncms_config_path;
 	$fp1 = @fopen( $nncms_config_path. "global_npss_ini","w+");
	@fwrite($fp1,$npss_ini);
	@fclose($fp1);
 }
?>
