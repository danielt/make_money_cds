<?php
/*
 * Created on 2012-2-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
//ob_start();
error_reporting(E_ALL ^ E_NOTICE);
session_start();
if (!empty($_COOKIE['language'])) {
	$language_dir_post = $_COOKIE['language'];
	if($language_dir_post == 'ch'){
		$_SESSION["lang_dir"]='zh_CN';
	}else if ($language_dir_post == 'en'){
		$_SESSION["lang_dir"]='en_US';
	}
}
include("nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path.'nn_cms_manager/nncms_mlang.php');
//  include ($nncms_db_path. "nns_common/nn_cms_config.php");
//	$_SESSION=null;
$username=$_POST["username"];
$userpass=$_POST["userpass"];
$logincode=$_POST["logincode"];
//$language_dir_post=$_POST["nns_language_id"];

$checkpri=null;
include $nncms_config_path. "nn_cms_manager/languages/language_contant.php";
include ($nncms_config_path. "nn_cms_config/nn_cms_global.php");
//include ($nncms_config_path. "nn_cms_config.php");
if ($username && $userpass){

	if (strtoupper($logincode)==strtoupper($_SESSION["login_code"]) || $g_login_code_enabled==0){
		//		 include "nncms_manager_inc.php";
		include $nncms_db_path. "nns_manager".DIRECTORY_SEPARATOR . "nns_db_manager_class.php";
		$manager_inst=new nns_db_manager_class();
		$login_arr=$manager_inst->nns_db_manager_login($username,$userpass);
		if ($login_arr["ret"]!=0){
			echo "<script>alert('". $login_arr["reason"]."');window.location.href='index.php';</script>";
		}else if ($login_arr["ret"]==0){
			if ($username==g_sys_loginid){

				include $nncms_db_path. "nns_carrier".DIRECTORY_SEPARATOR . "nns_db_carrier_class.php";
				$carrier_inst=new nns_db_carrier_class();
				$carrier_result=$carrier_inst->nns_db_carrier_list();
				if ($carrier_result["ret"]==0){

					if (count($carrier_result["data"])){
						$_SESSION["nns_org_id"]=$carrier_result["data"][0]["nns_id"];
						//$_SESSION["nns_org_id"]='bb571ad76ec8c4ca6526d79e1d882375';
					}
				}
				$_SESSION["nns_mgr_name"]=$username;
				$_SESSION["nns_role_pris"]="100100";

				$_SESSION["nns_manager_type"]=0;
				$_SESSION["nns_mgr_id"]="cms_admin";
				$_SESSION["language_dir"]=$language_dir_post;
				if($language_dir_post == 'ch'){
					$_SESSION["lang_dir"]='zh_CN';
				}else if ($language_dir_post == 'en'){
					$_SESSION["lang_dir"]='en_US';
				}
				$_SESSION["nns_role_name"]=cms_get_lang('super_admin');
				$_SESSION["nns_last_login_time"]=cms_get_lang('unknown_time');
			}else{
				$_SESSION["nns_mgr_name"]=$login_arr["data"][0]["nns_name"];
				$_SESSION["nns_last_login_time"]=$login_arr["data"][0]["nns_login_time"];
				$_SESSION["nns_role_pris"]=$login_arr["data"][0]["nns_pri_id"];
				$_SESSION["nns_mgr_id"]=$login_arr["data"][0]["nns_id"];
				$_SESSION["nns_org_id"]=$login_arr["data"][0]["nns_org_id"];
				//$_SESSION["nns_org_id"]='bb571ad76ec8c4ca6526d79e1d882375';
				$_SESSION["nns_role_id"]=$login_arr["data"][0]["nns_role_id"];
				$_SESSION["nns_manager_type"]=$login_arr["data"][0]["nns_type"];
				$_SESSION["nns_role_name"]=$login_arr["data"][0]["role_name"];
				$_SESSION["language_dir"]=$language_dir_post;
				if($language_dir_post == 'ch'){
					$_SESSION["lang_dir"]='zh_CN';
				}else if ($language_dir_post == 'en'){
					$_SESSION["lang_dir"]='en_US';
				}

			}
			include $nncms_db_path. "nns_log".DIRECTORY_SEPARATOR . "nns_db_op_log_class.php";
			$log_inst=new nns_db_op_log_class();
			$log_inst->nns_db_op_log_add($_SESSION["nns_mgr_id"],"login",$_SESSION["nns_mgr_name"].cms_get_lang('manager_login'),$_SESSION["nns_manager_type"],$_SESSION["nns_org_id"]);
			$log_inst=null;
			header("Location: nncms_manager.php");exit;
			//ob_end_flush();
		}
	}

}
//注销会话 by xsong512 2012-7-28 13:55
session_destroy();



function getDirFiles($dir)
{
	if ($handle = opendir($dir)){
	/* Because the return type could be false or other equivalent type(like 0),
	this is the correct way to loop over the directory. */
		while (false !== ($file = readdir($handle))) {
			$files[]=$file;
		}
	}
	closedir($handle);
	if($files) return $files;
	else return false;
}
$languages=getDirFiles("languages");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/loginstyle.css" rel="stylesheet" type="text/css" />
<link href="css/allstyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="js/cms_cookie.js"></script>
<script>

function set_default_language(){
	var value=$("#nns_language_id").find("option:selected").val();
	setCookie('language',value);
	var default_lan=getCookie("language")
	set_language(default_lan);
	//window.location.reload();
	history.go(0);
}
$(document).ready(function(){
	$('.login_code img').click(function(){
		$(this).attr("src","controls/nncms_controls_code_image.php?rnd="+Math.random());
	});

	$('#userpass').keydown(function(event){
		if(event.which == 13){
			document.getElementById('login_form').submit();
		}
	});
	$('#logincode').keydown(function(event){
		if(event.which == 13){
			document.getElementById('login_form').submit();
		}
	});
	var default_lan=getCookie("language")
		if (default_lan=="undefined" || default_lan==""){
			set_language("ch");
		}else{
			set_language(default_lan);
		}

});

function set_language(value){

	var len=$("#nns_language_id option").length;
	for (var i=0;i<len;i++){
		var value1=$("#nns_language_id option:eq("+i+")").val();
		if (value==value1){
			$("#nns_language_id")[0].selectedIndex=i;
			break;
		}
	}
}

</script>
</head>

<body class="loginbody">

	<div class="login_language">
		<tr>
			<td><select style="width:90px;"  name="nns_language_id" id="nns_language_id" rule="noempty" onchange="javascript:set_default_language()";>
<?php foreach($languages as $language){
if ($language!="." && $language!=".."){
	$lang = get_language_name($language);
	if(!empty($lang)){
?>
				<option value="<?php echo $language;?>" <?php if($language=='ch'){echo 'selected="selected"';}?>>&nbsp;<?php  echo $lang;?></option>
				<?php }}}?>
				</select>
			</td>
		</tr>
	</div>
	<div class="login_control">

		<div class="loginlogo">
			<span class="loginlogo_bg"><?php echo strlen($g_prject_ch_name) <1 ? "播控子平台-媒资注入平台" : $g_prject_ch_name;?></span>
		</div>

		<div class="loginbox">
			<div class="login_title"><?php echo cms_get_lang('manager_login');?></div>
			<form action="index.php" method="post" id="login_form">
			<div class="login_content">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><?php echo cms_get_lang('yhm');?>:</td>
						<td>
							<input type="text" name="username" id="username" style="width:150px;"/></td>
					</tr>
					<tr>
						<td><?php echo cms_get_lang('pass');?>:</td>
						<td><input type="password" name="userpass" id="userpass" style="width:150px;"/></td>
					</tr>

					<?php if ($g_login_code_enabled==1){?>
					<tr>
						<td><?php echo cms_get_lang('xtgl_dlyzm')?>:</td>
						<td valign="middle">
							<input type="text" name="logincode" id="logincode" style="width:60px; float:left;" />
							<div class="login_code"><img src="controls/nncms_controls_code_image.php"/>
							</div>
						</td>
					</tr>
					<?php }?>
				</table>
			</div>
			</form>
			<div style="background:url(images/login-03.png) 0px 0px repeat-x; height:2px;"></div>
			<div class="login_controlbtns">
				<div class="loginbtn">
					<a href="javascript:document.getElementById('login_form').submit();">
						<p class="loginbtn_bg"><?php echo cms_get_lang('login');?></p>
<!--<img src="images/login-05.png" width="81" height="34" border="0"/>-->
					</a>
				</div>
				<!--<div class="forget"><a href="#">忘记密码?</a></div>-->
				<!--<div style="clear:both;"></div>-->
			</div>
		</div>
		 <div class="copyinfo">Copyright &copy; <?php echo date('Y');?> <a href="http://www.starcorcn.com" title="<?php echo cms_get_lang('company');?>"><?php echo cms_get_lang('company'),'</a>&nbsp;',cms_get_lang('copyright');?>  &nbsp;&nbsp;<?php echo NN_VERSION;?>
		</div>
	</div>
</body>
</html>