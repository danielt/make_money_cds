<?php

/*
 * 框架入口文件
 */
error_reporting(E_ALL ^ E_NOTICE);
if (!defined("LOGIC_DIR")) {
    define("LOGIC_DIR", dirname(dirname(__FILE__)) . '/nn_logic/');
}
if (!defined("NP_DIR")) {
    define("NP_DIR", dirname(dirname(dirname(__FILE__))) . '/np/');
}
if (!defined("PROJECT_DIR")) {
    define("PROJECT_DIR", dirname(dirname(__FILE__)) . '/');
}
//载入logic配置文件
include_once LOGIC_DIR . 'nl_common.func.php';
if(isset($_REQUEST['_g_log_module'])){
    nl_log_v2_init_epg($_REQUEST['_g_log_module']);
}else{
    nl_log_v2_init_manager($_REQUEST['m']);
}
define("NN_FRAMEWORK_DIR", dirname(__FILE__));
date_default_timezone_set("PRC");
$include_path = get_include_path();
$include_path .= PATH_SEPARATOR . NN_FRAMEWORK_DIR . "/bases/";
$include_path .= PATH_SEPARATOR . NN_FRAMEWORK_DIR . "/extension/";
$include_path .= PATH_SEPARATOR . APP_DIR . "/control/";
$include_path .= PATH_SEPARATOR . NP_DIR . '/';
$arr_config = array();
$str_config=PROJECT_DIR . '/nn_cms_meta_config_v2.php';
if (file_exists($str_config)) {
    include_once $str_config;
}
if (file_exists(APP_DIR . '/config/config.inc.php')) {
    $arr_config_child = include APP_DIR . '/config/config.inc.php';
    $arr_config = array_merge_recursive($arr_config, $arr_config_child);
}
if(file_exists(APP_DIR.'/common/common.fun.php')){
    include APP_DIR.'/common/common.fun.php';
}
include NN_FRAMEWORK_DIR . '/common/nf_common.fun.php';
set_include_path($include_path);
nf_get_config($arr_config);
function nf_autoload($className)
{
    include $className . ".class.php";
}
spl_autoload_register('nf_autoload');
if (strpos($_REQUEST['m'], '/') !== false) {
    include APP_DIR . '/control/' . $_REQUEST['m'] . '_action.class.php';
    $arr_dir = explode('/', $_REQUEST['m']);
    $str_module = array_pop($arr_dir) . '_action';
} else {
    $str_module = $_REQUEST['m'] . '_action';
}
if (!class_exists($str_module)) {
    header("Status: 500 {$str_module} not exists");
    die;
}

$obj_control = new $str_module();
$obj_control->run();
