<?php

class nf_action
{

    protected $str_code = '1'; //返回的状态，默认是成功
    protected $str_reason = 'success'; //返回的状态说明信息
    protected $arr_debug_info = array(); //返回的调试信息
    private $str_output_type = 'json'; //返回的数据类型
    protected $arr_reponse_data = array(); //返回的数据部分
    private $arr_re_all_data = array(); //返回的所有数据结构
    protected $arr_request;
    protected $arr_post;
    protected $arr_get;
    private $str_start_time = null;
    private $str_end_time = null;

    function run()
    {
        $this->str_start_time = microtime(true);
        $this->arr_get = $_GET;
        $this->arr_post = $_POST;
        $this->arr_request = $_REQUEST;
        nl_log_v2_info("nf_framework","request data:\n" . var_export($_REQUEST, true));
        $_GET = null;
        $_POST = null;
        $_REQUEST = null;
        //如果有子类Common，调用这个类的init()方法 做权限控制
        if (method_exists($this, "init")) {
            $this->init();
        }

        //根据动作去找对应的方法
        $method = $this->arr_request["a"];
        if (method_exists($this, $method)) {
            $this->$method();
        } else {
            header("Status: 500 {$method} not exists");
            die;
        }
    }

    /**
     * 递归删除nns_前缀
     * @param type $arr_data
     */
    private function del_nnspre($arr_data)
    {
        if (!is_array($arr_data)) {
            return $arr_data;
        }
        $arr_re = array();
        foreach ($arr_data as $_k => $_v) {
            if (strpos($_k, 'nns_') === 0) {
                $_k = substr($_k, 4);
            }
            if (is_array($_v)) {
                $_v = $this->del_nnspre($_v);
            }
            $arr_re[$_k] = $_v;
        }
        return $arr_re;
    }

    protected function set_response_data($arr_db_data, $str_data = 'json')
    {
        $this->str_output_type = isset($this->arr_request['output_type']) ? $this->arr_request['output_type'] : $str_data;
        if ($str_data == 'xml') {
            $this->arr_re_all_data['data'] = $arr_db_data;
        } else {
            if ($arr_db_data['ret'] != 1) {
                $this->str_code = $arr_db_data['ret'];
                $this->str_reason = $arr_db_data['reason'];
                nl_log_v2_error("nf_framework",$arr_db_data['reason']);
            }
            $this->arr_reponse_data = $arr_db_data['data'];
        }
    }

    protected function out_put()
    {
        $this->str_end_time = microtime(true);
        nl_log_v2_info("nf_framework","Run Sql:" . var_export(nf_logic_base::$arr_sql, true));
        nl_log_v2_info("nf_framework","response data:\n" . var_export($this->arr_re_all_data, true));
        $this->arr_debug_info = array_merge($this->arr_debug_info, nf_logic_base::$arr_sql);
        $this->arr_debug_info['run_time'] = $this->str_end_time - $this->str_start_time;
        if ($this->str_output_type == 'xml') {
            $this->_out_put_xml(); //输出xml
        } else {
            $this->arr_re_all_data = array(
                'code' => $this->str_code,
                'reason' => $this->str_reason,
                'output_type' => $this->str_output_type,
                'data' => $this->del_nnspre($this->arr_reponse_data), //统一删除nns_前缀
            );
            $this->arr_re_all_data['debug_info'] = $this->arr_debug_info;
            $this->arr_re_all_data['request_data'] = $this->arr_request;
            switch ($this->str_output_type) {
                case "json":
                    $this->_out_put_json();
                    break;
                case "array":
                    $this->_out_put_array(); //用于调试
                    break;
            }
        }
    }

    private function _out_put_json()
    {
        echo json_encode($this->arr_re_all_data);
    }

    private function _out_put_array()
    {
        p($this->arr_re_all_data);
    }

    private function _out_put_xml()
    {
        header("Content-Type:text/xml;charset=utf-8");
        if (defined('DEBUG') && DEBUG == 1) {
            header("debug:" . implode("\n", $this->arr_debug_info));
        }
        if (!is_string($this->arr_re_all_data['data'])) {
            echo $this->arr_re_all_data['data']->saveXML();
        } else {
            echo $this->arr_re_all_data['data'];
        }
    }

}
