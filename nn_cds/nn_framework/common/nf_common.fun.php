<?php

/**
 * 公共函数库
 */

/**
 * 获取配置
 * @param string $name
 * @return null|array
 */
function nf_get_config($name = null, $value = null)
{
    static $_config = array();
    // 无参数时获取所有
    if (empty($name))
        return $_config;
    // 优先执行设置获取或赋值
    if (is_string($name)) {
        //一维
        if (!strpos($name, '.')) {
            $name = strtolower($name);
            if (is_null($value)) {
                return isset($_config[$name]) ? $_config[$name] : null;
            }
            $_config[$name] = $value;
            return;
        }
        // 二维数组设置和获取支持
        $arr_name = explode('.', $name);
        $flag = '$_config';
        foreach ($arr_name as $_name) {
            $flag .= "['{$_name}']";
        }
        if (is_null($value)) {
            eval('$re=' . $flag . ';');
            return $re;
        } else {
            eval($flag . '=' . $value . ';');
            return;
        }
    }
    // 批量设置
    if (is_array($name)) {
        return $_config = array_merge($_config, array_change_key_case($name));
    }
    return null; // 避免非法参数
}

/**
 * 输出各种类型的数据，调试程序时打印数据使用。
 * @param    mixed    参数：可以是一个或多个任意变量或值
 */
function p()
{
    $args = func_get_args(); //获取多个参数
    if (count($args) < 1) {
        echo "必须为p()函数提供参数!";
        return;
    }

    echo '<pre>';
    //多个参数循环输出
    foreach ($args as $arg) {
        if (is_array($arg)) {
            print_r($arg);
            echo '<br>';
        } else if (is_string($arg)) {
            echo $arg . '<br>';
        } else {
            var_dump($arg);
            echo '<br>';
        }
    }
    echo '</pre>';
}

/**
 * 获取客户端ip地址
 * @return string $ip
 */
function nf_get_clicent_ip()
{
    static $ip = NULL;
    if ($ip !== NULL)
        return $ip;
    if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
        $pos = array_search('unknown', $arr);
        if (false !== $pos)
            unset($arr[$pos]);
        $ip = trim($arr[0]);
    } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (isset($_SERVER['REMOTE_ADDR'])) {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    // IP地址合法验证
    $ip = (false !== ip2long($ip)) ? $ip : '0.0.0.0';
    return $ip;
}

/**
 * 写配置ini配置文件支持一维和二维数组
 * @params string $str_params调用参数，如果则返回整个ini配置文件，如果有则返回指定的配置项，可以支持二维，使用.分隔
 * @params mixed $mixed 可以是字符串和一维数组,如果是null则为删除这个配置项
 * @params string $path ini文件地址，默认是项目目录下的config/config.ini
 * @return boolean 写入是否成功
 */
function nf_set_ini($str_params, $mixed = null, $path = null)
{
    if (!is_string($str_params)) {
        return false;
    }
    if ($path == null) {
        $path = APP_DIR . '/config/config.ini';
    }
    $arr_ini = parse_ini_file($path, true);
    if (strpos($str_params, '.') === false) {
        if (is_array($mixed)) {
            foreach ($mixed as $_k => $_v) {
                $arr_ini[$str_params][$_k] = $_v;
            }
        } else {
            $arr_ini[$str_params] = $mixed;
        }
        if ($mixed === null) {
            unset($arr_ini[$str_params]);
        }
    } else {
        $arr_flag = explode('.', $str_params);
        $arr_ini[$arr_flag[0]][$arr_flag[1]] = $mixed;
        if ($mixed === null) {
            unset($arr_ini[$arr_flag[0]][$arr_flag[1]]);
        }
    }
    $content = '';
    foreach ($arr_ini as $key => $elem) {
        $content .= "[" . $key . "]\n";
        foreach ($elem as $key2 => $elem2) {
            if (is_array($elem2)) {
                for ($i = 0; $i < count($elem2); $i++) {
                    $content .= $key2 . "[] = \"" . $elem2[$i] . "\"\n";
                }
            } else if ($elem2 == "")
                $content .= $key2 . " = \n";
            else
                $content .= $key2 . " = \"" . $elem2 . "\"\n";
        }
    }
    file_put_contents($path, $content);
    return true;
}

/**
 * 获取ini配置文件内容,返回的是一个二维数组，包含了项目根目录下的config/config.ini文件中的配置信息
 * @params string $str_params调用参数，如果则返回整个ini配置文件，如果有则返回指定的配置项，可以支持二维，使用.分隔
 * @return
 */
function nf_get_ini($str_params = null, $str_file = null)
{
    if ($str_file == null) {
        $str_file = APP_DIR . '/config/config.inc.ini';
    }
    $arr_ini = parse_ini_file($str_file, true);
    if (empty($str_params)) {
        return $arr_ini;
    }
    if (strpos($str_params, '.') === false) {
        return $arr_ini[$str_params];
    } else {
        $arr_flag = explode('.', $str_params);
        return $arr_ini[$arr_flag[0]][$arr_flag[1]];
    }
}

/**
 * 设置和获取sql语句
 * @param string $str_sql
 * @return array
 */
function nf_sql_statement($str_sql = null)
{
    static $arr_sql = array();
    if ($str_sql == null) {
        return $arr_sql;
    }
    $arr_sql[] = $str_sql;
}

/**
 * 便利一个目录的所有一级子文件
 * @staticvar array $arr_files
 * @param array $arr_files
 */
function nf_dir_list($dir)
{
    $arr_files = array();
    $h = opendir($dir);
    while (false !== ($name = readdir($h))) {
        if ($name == '.' || $name == '..') {
            continue;
        }
        $str_file = $dir . "/" . $name;
        if (is_file($str_file)) {
            $arr_files[] = $name;
        }
    }
    return $arr_files;
}

/*
 * 获取logic对象
 * @auth yxt
 * 2014年3月11日17:22:20
 * @params string $str_logic_name logic类路径，从nn_logic一下开始，比如在nn_logic下的user下的user.class.php，则传入"user/user"
 * @params obj $dc dc对象，如果为空则表示调用原来的logic类
 */

function nf_get_logic($str_logic_name = null, $dc = null)
{
    if (!empty($str_logic_name)) {
        $str_logic_name = str_replace(".", "/", $str_logic_name);
        $arr_logic = explode("/", $str_logic_name);
        if (defined(LOGIC_DIR)) {
            include_once LOGIC_DIR . $str_logic_name . '.class.php';
        } else {
            include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/' . $str_logic_name . '.class.php';
        }
        $str_class_name = array_pop($arr_logic);
        $obj_logic = new $str_class_name();
    } else {
        $obj_logic = new nf_logic_base();
    }
    if ($dc !== null) {
        $dc->open();
        $obj_logic->set_dc($dc);
    }
    return $obj_logic;
}

function get_mgtv_img_host()
{
    include_once PROJECT_DIR . '/nn_cms_config/child_config/nn_cms_config_mgtv.php';
    include_once PROJECT_DIR . '/nn_cms_config/nn_cms_global.php';
    global $g_bk_version_number;
    $bk_version_number = $g_bk_version_number;
    unset($g_bk_version_number);
    if($bk_version_number == '1')
    {
        global $g_bk_web_url;
        $bk_web_url = $g_bk_web_url;
        unset($g_bk_web_url);
        $bk_web_url = trim(trim(trim($bk_web_url,'/'),'\\')).'/data/downimg/';
        return $bk_web_url;
    }
    else
    {
        global $g_image_host;
        $arr_imgs = explode(";", $g_image_host);
        $int_key = array_rand($arr_imgs);
        return $arr_imgs[$int_key];
    }
}

function array_to_json($array)
{
    arrayRecursive($array, 'urlencode');
    $json = json_encode($array);
    $json = urldecode($json);
    // ext需要不带引号的bool类型
    $json = str_replace("\"false\"", "false", $json);
    $json = str_replace("\"true\"", "true", $json);
    return $json;
}

function arrayRecursive(&$array, $function, $apply_to_keys_also = false)
{
    static $recursive_counter = 0;
    if (++$recursive_counter > 1000) {
        die('possible deep recursion attack');
    }
    foreach ($array as $key => $value) {
        if (is_array($value)) {
            arrayRecursive($array[$key], $function, $apply_to_keys_also);
        } else {
            $array[$key] = $function($value);
        }
        if ($apply_to_keys_also && is_string($key)) {
            $new_key = $function($key);
            if ($new_key != $key) {
                $array[$new_key] = $array[$key];
                unset($array[$key]);
            }
        }
    }
    $recursive_counter--;
}

/**
 * @param $str_assist_id 媒资包id
 * @param $arr_ui_bind ui配置
 */
function get_detail_file_name($str_assist_id, $arr_ui_bind)
{
    $str_ui_style = '1';
    foreach ($arr_ui_bind as $_v) {
        if ($_v['asset_id'] == $str_assist_id) {
            $str_ui_style = $_v['ui_style'];
            break;
        }
    }
    return $str_ui_style;
}

/**
 * @param string $str_assist_id 媒资包id
 * @param array $arr_list_file 详情配置
 */
function get_list_file_name($str_assist_id, $arr_list_file)
{
    $str_ui_style=$arr_list_file['other']['file_name'];
    foreach ($arr_list_file as $_str_assist_id=>$arr_file) {
        if ($_str_assist_id == $str_assist_id) {
            $str_ui_style = $arr_file['file_name'];
            break;
        }
    }
    return array('file_name'=>$str_ui_style);
}