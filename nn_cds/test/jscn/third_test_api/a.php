<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2017/11/6 17:18
 */


//这里测试可以配置数字,不同数字表示不同交互。响应不同
define("DEFAULT_KEY", 1);

//存储位置,可自行设置。目前存放在test目录/log_file/年月日/时分/
$log_file = dirname(dirname(dirname(__FILE__))) . '/log_file/' . date('Ymd') . '/' . date('Hi') . '/';

if (!file_exists($log_file))
{
    mkdir($log_file, 0777,true);
}

$ret = receiveStreamFile($log_file . 'receive_file.txt');

echo $ret;
/**php 接收流文件
 * @param $receive_file //接收后保存的文件名
 * @return array|int
 */
function receiveStreamFile($receive_file){
    $streamData = isset($GLOBALS['HTTP_RAW_POST_DATA'])? $GLOBALS['HTTP_RAW_POST_DATA'] : '';

    if(empty($streamData)){
        $streamData = file_get_contents('php://input');
    }

    if($streamData!=''){
        $result = file_put_contents($receive_file, $streamData, true);
    }else{
        $result = false;
    }

    if ($result)
    {
        switch (DEFAULT_KEY)
        {
            case 1:
                $ret = json_encode(
                    array(
                        'code' => 0,
                        'reason' => 'transcode successfully !',
                        'replyKey' => '',//测试自己填写下发的key
                    )
                );
                break;
            case 2://注入cdn返回
                $ret = '<?xml version="1.0" encoding="UTF-8"?>';
                $ret .=  '<xsi:ADI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
                $ret .=  '<Reply>';
                $ret .=     '<Property Name="Result">0</Property>';
                $ret .=           '<Property Name="Description"/>';
                $ret .=   '</Reply>';
                $ret .=   '</xsi:ADI>';
                break;
            //TO DO.可以动态添加返回状态

            default://下面的
                $ret = json_encode(
                    array(
                        'code' => 0,
                        'reason' => 'transcode successfully !',
                        'replyKey' => '',//测试自己填写下发的key
                    )
                );
                break;
        }
    }
    else
    {
        $ret = json_encode(
            array(
                'code' => 1,
                'reason' => 'transcode error !',
                'replyKey' => '',//测试自己填写下发的key
            )
        );
    }
    return $ret;
}


