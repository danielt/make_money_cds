<?php
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))).'/np/np_http_curl.class.php';

function _write_log($data,$file_dir)
{
    echo $data."<br/>";
    $arr_path_info = pathinfo($file_dir);
    if (!is_dir($arr_path_info['dirname']))
    {
        mkdir($arr_path_info['dirname'], 0777, true);
    }
    @file_put_contents($file_dir,$data);
    return ;
}
$queue_id = np_guid_rand();
if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'push')
{
    $file_dir = dirname(__FILE__).'/ftp_push/'.date("Ymd")."/".date("H").'/ftp_push.txt';
    _write_log("---------发送到ftp推片工具反馈的数据开始------------",$file_dir);
    _write_log("接收的POST数据：".var_export($_REQUEST,true),$file_dir);
    _write_log("队列ID[{$queue_id}]",$file_dir);
    _write_log("---------发送到ftp推片工具反馈的数据结束------------",$file_dir);
    if(!isset($_REQUEST['queue_id']) || strlen($_REQUEST['queue_id']) <1)
    {
        echo "<script>alert('queue_id为空');history.go(-1);</script>";die;
    }
    if(!isset($_REQUEST['request_url']) || strlen($_REQUEST['request_url']) <1)
    {
        echo "<script>alert('GO请求地址为空');history.go(-1);</script>";die;
    }
    if(!isset($_REQUEST['local_file_path']) || strlen($_REQUEST['local_file_path']) <1)
    {
        echo "<script>alert('GO请求地址为空');history.go(-1);</script>";die;
    }
    $obj_curl = new np_http_curl_class();
    $params = array(
        'action'=>'upload',
        'target'=>$_REQUEST['des_file_path'],
        'source'=>$_REQUEST['local_file_path'],
        'id'=>$_REQUEST['queue_id'],
    );
    $arr_header = array(
        "Content-Type:application/x-www-form-urlencoded"
    );
    $str = '';
    foreach ($params as $key=>$val)
    {
        $str.="{$key}={$val}&";
    }
    $str = trim($str,'&');
    $result = $obj_curl->post($_REQUEST['request_url'], $str);
    _write_log("请求HTTP信息URL[{$_REQUEST['request_url']}];参数[".var_export($params,true)."]",$file_dir);
    $curl_info = $obj_curl->curl_getinfo();
    _write_log("请求HTTP状态：".var_export($curl_info['http_code'],true),$file_dir);
    _write_log("请求HTTP结果：".var_export($result,true),$file_dir);
    _write_log("---------发送到ftp推片工具反馈的数据结束------------",$file_dir);
}
else if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'query')
{
    $file_dir = dirname(__FILE__).'/ftp_query/'.date("Ymd")."/".date("H").'/ftp_query.txt';
    _write_log("---------发送到ftp推片工具反馈的数据开始------------",$file_dir);
    _write_log("接收的POST数据：".var_export($_REQUEST,true),$file_dir);
    _write_log("队列ID[{$queue_id}]",$file_dir);
    _write_log("---------发送到ftp推片工具反馈的数据结束------------",$file_dir);
    if(!isset($_REQUEST['queue_id']) || strlen($_REQUEST['queue_id']) <1)
    {
        echo "<script>alert('queue_id为空');history.go(-1);</script>";die;
    }
    if(!isset($_REQUEST['request_url']) || strlen($_REQUEST['request_url']) <1)
    {
        echo "<script>alert('GO请求地址为空');history.go(-1);</script>";die;
    }
    $obj_curl = new np_http_curl_class();
    $params = array(
        'action'=>'status',
        'id'=>$_REQUEST['queue_id'],
    );
    $str = '';
    foreach ($params as $key=>$val)
    {
        $str.="{$key}={$val}&";
    }
    $str = trim($str,'&');
    $result = $obj_curl->post($_REQUEST['request_url'], $str);
    _write_log("请求HTTP信息URL[{$_REQUEST['request_url']}];参数[".var_export($params,true)."]",$file_dir);
    $curl_info = $obj_curl->curl_getinfo();
    _write_log("请求HTTP状态：".var_export($curl_info['http_code'],true),$file_dir);
    _write_log("请求HTTP结果：".var_export($result,true),$file_dir);
    _write_log("---------发送到ftp推片工具反馈的数据结束------------",$file_dir);
}
else
{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>江苏有线-GO推片</title>
    </head>
    <body>
        <form method="post" action="./ftp_push.php">
            <input name="action" type='hidden' value='push'><br/>
            <br>
                    江苏有线-GO推片 脚本：<hr/><br>
            query ID:
            <input style="width: 500PX;" name="queue_id" value="<?php echo $queue_id;?>"/><br/><br/>
            GO请求地址:
            <input style="width: 500PX;" name="request_url" value="http://172.16.183.102:5431/ftpPush"/><br/><br/>
                    本地文件路径:
            <input style="width: 500PX;" name="local_file_path" value="/data/starcor/www/media_file/29e9a5ee719172e739a8acfb1fa8b403.ts" /><br/><br/>
                    影片接收后的影片别名:
            <input style="width: 500PX;" name="des_file_path" value="29e9a5ee719172e739a8acfb1fa8b403.ts"/><br/><br/>
            <input type="submit" value="提交"/>
        </form>
        <form method="post" action="./ftp_push.php">
            <input name="action" type='hidden' value='query'><br/>
            <br>
                    江苏有线-GO推片查询 脚本：<hr/><br>
            query ID:
            <input style="width: 500PX;" name="request_url" value="http://172.16.183.102:5431/ftpPush"/><br/><br/>
                    本地文件路径:
            <input style="width: 500PX;" name="queue_id" value="<?php echo $queue_id;?>" /><br/><br/>
            <input type="submit" value="提交"/>
        </form>
    </body>
</html>
<?php 
}
?>