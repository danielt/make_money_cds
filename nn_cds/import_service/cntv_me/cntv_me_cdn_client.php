<?php
ini_set('display_errors',1);
set_time_limit(0);
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/mgtv_init.php';
define('ORG_ID', 'cntv_me');
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/'.ORG_ID.'/init.php';
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/'.ORG_ID.'/define.php';
define('CPID','000010');//内容提供方标识
define('CMSID','000003');//CDN标识
define('KEY_ENCRYPT','5io5yobf3j6z/VOPRerQSg==');//加密密钥
class cntv_me_cdn_client
{
	//const WSDL = 'http://127.0.0.1/nn_cntvbk/mgtv/cntv_me/cntv_me_cdn_service.php?wsdl';
	const WSDL = 'http://221.228.39.45:9080/CMSPlus/ContentMngReq?wsdl';
	static public function ContentMngReq($CorrelateID,$ContentMngXMLURL,$XMLURLQC)
	{		
		$client = self::get_soap_client();
		//获取毫秒级时间戳
		$time = explode ( " ", microtime () );  
		$time = $time [1] . ($time [0] * 1000);  
		$time2 = explode ( ".", $time );  
		$time = $time2 [0];
		$params = array(
			"A" => CPID,
			"B" => CMSID,
			"C" => $CorrelateID,
			"D" => $ContentMngXMLURL,
			"E" => $XMLURLQC,
			"F" => $time
		);
		$key = base64_decode(KEY_ENCRYPT);
		$Encrypt = json_encode($params);
		$pad = 16 - (strlen($Encrypt) % 16);
		$Encrypt .= str_repeat(chr($pad), $pad);
		$Encrypt = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128,$key,$Encrypt,MCRYPT_MODE_ECB));
		i_write_log_core('ContentMngReq request args:'.var_export(array(CPID,CMSID,$CorrelateID,$ContentMngXMLURL,$XMLURLQC,$Encrypt),true),ORG_ID.'/c2');
		$ret = $client->ContentMngReq(CPID,CMSID,$CorrelateID,$ContentMngXMLURL,$XMLURLQC,$Encrypt);
		i_write_log_core('ContentMngReq return:'.var_export($ret,true),ORG_ID.'/c2');
		i_write_log_core('ContentMngReq return:'.json_encode($ret),ORG_ID.'/c2json');
		return $ret;
	}
	public static function get_soap_client(){
		return new SOAPClient(self::WSDL,array('trace'=>true));
	}
}
