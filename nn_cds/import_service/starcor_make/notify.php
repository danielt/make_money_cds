<?php
ini_set('display_errors',1);
set_time_limit(0);
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/mgtv_init.php';
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/models/child_models/c2_task_assign.class.php';

$ip = i_get_ip();
$postdata = $_REQUEST;

$file_init_dir = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/'.ORG_ID.'/init.php';
$file_init_define = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/'.ORG_ID.'/define.php';
if(!file_exists($file_init_define) || !file_exists($file_init_dir))
{
    nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '初始化文件不存在:[' . $file_init_dir . ']['.$file_init_define.']', ORG_ID);
    exit();
}
include_once $file_init_dir;
include_once $file_init_define;

nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------BK收到CDN消息反馈开始--------------', ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '执行文件:' . __FILE__, ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '收到来自CDN的消息,IP:' . $ip . ';收到来自CDN的内容:' . var_export($postdata,true), ORG_ID);

class notify
{
    /**
     * VOD系统下载完成后通知
     * @param string $id packageid
     * @return array success	通知消息的结果：true-成功；false-失败。description	如果操作结果不为true，则媒资需要填充原因后返回。
     */
    public function msg_notify($data)
    {
        $obj_dc = nl_get_dc(
            array(
                'db_policy' =>  NL_DB_READ | NL_DB_WRITE,
                'cache_policy' => NP_KV_CACHE_TYPE_NULL
            )
        );
        $sp_config = sp_model::get_sp_config(ORG_ID);
        $verify_result = json_decode($data['verify_result'],true);
        nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '通知返回入参参数：' . var_export($verify_result,true), ORG_ID);

        if(!is_array($verify_result) || empty($verify_result))
        {
            $arr = array(
                "msg_id" => "", // 消息ID
                "status" => 1, // 操作状态 0 成功 其他失败
                "sync_time" => date('Y-m-d H:i:s'), // 同步时间
                "summary" => "通知格式错误"  // 描述
            );
            nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------通知处理结束--------------', ORG_ID);
            return array($arr);
        }
        $notify_arr = array();
        $media_notify = array();
        $media_info = array();
        //排查非注入通知与非片源通知
        foreach ($verify_result as $verify)
        {
            if(!isset($verify['type']) || empty($verify['type']) || $verify['type'] !== 'media' || !isset($verify['opt_type']) || (int)$verify['opt_type'] !== 2)
            {
                $notify_arr[] = array(
                    'msg_id' => $verify['nns_id'],
                    'status' => 0,
                    "sync_time" => date('Y-m-d H:i:s'), // 同步时间
                    "summary" => "成功"
                );
                continue;
            }
            $media_info[] = array(
                'msg_id' => $verify['nns_id'],
                'original_id' => $verify['original_id'],
                'cp_id' => $verify['cp_id'],
            );
        }
        if(empty($media_info))
        {
            nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------通知处理结束--------------', ORG_ID);
            return $notify_arr;
        }
        $obj_c2_task_assign = new c2_task_assign();
        $result_c2_task_assign = $obj_c2_task_assign->auto_load_task_model($sp_config['c2_import_cdn_model'], 'save_c2_notify',$obj_dc,ORG_ID,$sp_config,$media_info);
        nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '通知返回处理结果：' . var_export($result_c2_task_assign, true), ORG_ID);
        foreach ($result_c2_task_assign as $value)
        {
            $notify_arr[] = array(
                'msg_id' => $value['msg_id'],
                'status' => $value['status'],
                "sync_time" => date('Y-m-d H:i:s'), // 同步时间
                "summary" => $value['status'] == '0' ? "成功" : "失败",
            );
        }
        nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------通知处理结束--------------', ORG_ID);
        return $notify_arr;
    }
}
$notify = new notify();
$result = $notify->msg_notify($postdata);
echo json_encode($result);