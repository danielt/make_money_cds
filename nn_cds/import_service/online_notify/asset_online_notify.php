<?php
/**
 * redis媒资上下线公共反馈接口
 */
ini_set('display_errors', 7);
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/callback_notify/callback_notify.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/asset_online/asset_online_log.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/asset_online/asset_online.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/cp/cp.class.php';
include_once dirname(dirname(dirname(dirname(__FILE__)))) . '/np/np_xml_to_array.class.php';

#是否是定时器
define("IS_LOG_TIMER_OPERATION", true);
$data = $_POST;
nn_log::write_log_message(LOG_MODEL_NOTIFY,"================数据POST================\r\n".var_export($data,true),'',VIDEO_TYPE_VIDEO);
$callback_notify = new callback_notify();
$callback_notify->unline_notify($data);
unset($data);
