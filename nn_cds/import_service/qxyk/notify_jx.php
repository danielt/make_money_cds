<?php
/**
 * 江西本地播控cdn反馈
 */
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('display_errors',1);
set_time_limit(0);
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/mgtv_init.php';
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/models/child_models/c2_task_assign.class.php';

$ip = i_get_ip();
$postdata = file_get_contents("php://input");

$file_init_dir = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/'.ORG_ID.'/init.php';
$file_init_define = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/'.ORG_ID.'/define.php';
if(!file_exists($file_init_define) || !file_exists($file_init_dir))
{
	nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '初始化文件不存在:[' . $file_init_dir . ']['.$file_init_define.']', ORG_ID);
	exit();
}
include_once $file_init_dir;
include_once $file_init_define;
$file_wsdl_file_name = 'VODNoticeService.wsdl';
if(!file_exists(dirname(__FILE__).'/'.$file_wsdl_file_name))
{
	nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '初始化WSDL文件不存在:[' . dirname(__FILE__).'/'.$file_wsdl_file_name . ']', ORG_ID);
	exit();
}
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------BK收到CDN消息反馈开始--------------', ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '执行文件:' . __FILE__, ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '加载WSDL文件:' . dirname(__FILE__).'/'.$file_wsdl_file_name, ORG_ID);
nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '收到来自CDN的消息,IP:' . $ip . ';收到来自CDN的内容:' . $postdata, ORG_ID);

class qxyk_notify
{
	/**
	 * VOD系统下载完成后通知
	 * @param string $id packageid
	 * @return array success	通知消息的结果：true-成功；false-失败。description	如果操作结果不为true，则媒资需要填充原因后返回。
	 */
	public function downloadFinishNotice($id)
	{
		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------VOD系统下载完成后通知开始--------------', ORG_ID);
		$obj_dc = nl_get_dc(
			array(
				'db_policy' =>  NL_DB_READ | NL_DB_WRITE,
				'cache_policy' => NP_KV_CACHE_TYPE_NULL
			)
		);
		$sp_config = sp_model::get_sp_config(ORG_ID);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '通知返回入参参数：' . $id, ORG_ID);
		$json="{".$id."}";
		$notify_arr=json_decode($json,true);
        $obj_c2_task_assign = new c2_task_assign();
		$result_c2_task_assign = $obj_c2_task_assign->auto_load_task_model($sp_config['c2_import_cdn_model'], 'save_c2_jx_notify',$obj_dc,ORG_ID,$sp_config,$notify_arr);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '通知返回处理结果：' . var_export($result_c2_task_assign, true), ORG_ID);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------VOD系统下载完成后通知结束--------------', ORG_ID);
		if($result_c2_task_assign['ret'] != 2)
		{
			return ($result_c2_task_assign['ret'] == 0) ? array('success'=>0,'description'=>'notify ok') : array('success'=>1,'description'=>'notify fail');
		}	
		return array('success'=>1,'description'=>'notify fail');
	}
	/**
	 * VOD系统删除VOD后，通知媒资修改节目状态
	 * @param string $id  packageid 
	 * @return array success	通知消息的结果：true-成功；false-失败。description	如果操作结果不为true，则媒资需要填充原因后返回。
	 */
	public function programDeleteNotice($id)
	{
		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------VOD系统删除VOD后，通知媒资修改节目状态开始--------------', ORG_ID);
		$obj_dc = nl_get_dc(
				array(
						'db_policy' =>  NL_DB_READ | NL_DB_WRITE,
						'cache_policy' => NP_KV_CACHE_TYPE_NULL
				)
		);
		$sp_config = sp_model::get_sp_config(ORG_ID);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '通知返回入参参数：' . $id, ORG_ID);
		$obj_c2_task_assign = new c2_task_assign();
		$result_c2_task_assign = $obj_c2_task_assign->auto_load_task_model($sp_config['c2_import_cdn_model'], 'notice_delete_notify',$obj_dc,ORG_ID,$sp_config,array('id'=>$id));
		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, __FUNCTION__ . '通知返回处理结果：' . var_export($result_c2_task_assign, true), ORG_ID);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '--------------VOD系统删除VOD后，通知媒资修改节目状态结束--------------', ORG_ID);
		if($result_c2_task_assign['ret'] != 2)
		{
			return ($result_c2_task_assign['ret'] == 0) ? array('success'=>0,'description'=>'DeleteNotice ok') : array('success'=>1,'description'=>'DeleteNotice fail');
		}
		return array('success'=>1,'description'=>'DeleteNotice fail');
	}
}
$server = new SOAPServer('VODNoticeService.wsdl');
$server->setClass('qxyk_notify');
$server->handle();