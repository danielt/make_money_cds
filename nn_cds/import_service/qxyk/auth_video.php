<?php
/**
 * Created by IntelliJ IDEA.
 * User: LZ
 * Date: 2018/3/27
 * Time: 15:39
 */
header("content-type:text/html;charset=utf-8");
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/mgtv_init.php';
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/models/child_models/c2_task_assign.class.php';
$file_init_dir = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/'.ORG_ID.'/init.php';
$file_init_define = dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/'.ORG_ID.'/define.php';
if(!file_exists($file_init_define) || !file_exists($file_init_dir))
{
    nn_log::write_log_message(LOG_MODEL_BK_CDN_NOTIFY, '初始化文件不存在:[' . $file_init_dir . ']['.$file_init_define.']', ORG_ID);
    exit();
}
include_once $file_init_dir;
include_once $file_init_define;
$obj_dc = nl_get_dc(
    array(
        'db_policy' =>  NL_DB_READ | NL_DB_WRITE,
        'cache_policy' => NP_KV_CACHE_TYPE_NULL
    )
);
if(!empty($_POST) && isset($_POST['id']))
{
    $id = $_POST['id'];
    $sp_config = sp_model::get_sp_config(ORG_ID);
    $obj_c2_task_assign = new c2_task_assign();
    $result_c2_task_assign = $obj_c2_task_assign->auto_load_task_model($sp_config['c2_import_cdn_model'], 'save_c2_notify',$obj_dc,ORG_ID,$sp_config,array('id'=>$id));
    var_dump($result_c2_task_assign);
}
?>

<html>
    <head>
        <title>辽宁专用</title>
    </head>
    <body>
        <form action="" method="post">
        <table>
            <tr>
                <td>鉴权的片源ID【此处在CDN未反馈注入状态的时候使用】：</td>
                <td><input type="text" name="id" value="" /></td>
                <td><input type="submit" value="提交" /></td>
            </tr>
        </table>
        </form>
    </body>
</html>