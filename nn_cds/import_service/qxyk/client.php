<?php
ini_set('display_errors',1);
set_time_limit(0);
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/mgtv_init.php';
$arr_dir = explode('|',str_replace(array('/','\\'), '|', __DIR__));
$str_dir = array_pop($arr_dir);
define('ORG_ID', $str_dir);
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/'.ORG_ID.'/init.php';
include_once  dirname(dirname(dirname(__FILE__))).'/mgtv_v2/'.ORG_ID.'/define.php';
ini_set("soap.wsdl_cache_enabled", "0");

class client
{
	public static  $wsdl = null;
	//soap 请求配置
	public static $soap_config = array ('trace' => true,'cache_wsdl' =>WSDL_CACHE_NONE);
	
	static public function publish($ftpPath,$adiFile,$detailParams,$bk_web_url,$video_type=VIDEO_TYPE_VIDEO)
	{
		$arg_list = func_get_args();
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '--------------BK到CDN消息发送开始--------------', ORG_ID, $video_type);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '执行文件:' . __FILE__ , ORG_ID, $video_type);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '执行方法:' . __FUNCTION__, ORG_ID, $video_type);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '客户端请求传入参数:' . var_export($arg_list, true), ORG_ID, $video_type);
		$params = array(
				'ftpPath' => $ftpPath,
				'adiFile' => $adiFile,
				'detailParams' => $detailParams,
		);
		$client = self::get_soap_client($bk_web_url,$video_type);
		$ret = $client->publish($params);
	
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '客户端输出结果:' . var_export($ret, true), ORG_ID, $video_type);
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '--------------BK到CDN消息发送结束--------------', ORG_ID, $video_type);
		return $ret;
	}
	public static function get_soap_client($bk_web_url,$video_type)
	{
		self::$wsdl = $bk_web_url.'/import_service/'.ORG_ID.'/service.php?wsdl';
		nn_log::write_log_message(LOG_MODEL_BK_CDN_SEND, '客户端请求wsdl地址:' . self::$wsdl . ',wsdl配置参数:' . var_export(self::$soap_config,true), ORG_ID, $video_type);
		return new SOAPClient(self::$wsdl, self::$soap_config);
	}
}