<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/8/22 9:12
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'nn_cms_config.php';
$nn_cms = g_db_name;
?>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../nn_cms_manager/css/bootstrap.min.css">
    <script type="text/javascript" src="../nn_cms_manager/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="../nn_cms_manager/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../nn_cms_manager/js/layer/layer.min.js"></script>
    <style>
#tip{
-moz-animation:animations 1.5s ease infinite;
            -webkit-animation:animations 1.5s ease infinite;
            -o-animation:animations 1.5s ease infinite;
            -ms-animation:animations 1.5s ease infinite;
            animation:animations 1.5s ease infinite;
        }
        @-webkit-keyframes animations{
    0%{box-shadow:0 0 10px rgba(204,102,0,.8);}
            50%{box-shadow:0 0 10px rgba(204,102,0,.3);}
            100%{box-shadow:0 0 10px rgba(204,102,0,.8);}
        }
        @-moz-keyframes animations{
    0%{box-shadow:0 0 10px rgba(204,102,0,.8);}
            50%{box-shadow:0 0 10px rgba(204,102,0,.3);}
            100%{box-shadow:0 0 10px rgba(204,102,0,.8);}
        }
        @-o-keyframes animations{
    0%{box-shadow:0 0 10px rgba(204,102,0,.8);}
            50%{box-shadow:0 0 10px rgba(204,102,0,.3);}
            100%{box-shadow:0 0 10px rgba(204,102,0,.8);}
        }
        @-ms-keyframes animations{
    0%{box-shadow:0 0 10px rgba(204,102,0,.8);}
            50%{box-shadow:0 0 10px rgba(204,102,0,.3);}
            100%{box-shadow:0 0 10px rgba(204,102,0,.8);}
        }
        @keyframes animations{
    0%{box-shadow:0 0 10px rgba(204,102,0,.8);}
            50%{box-shadow:0 0 10px rgba(204,102,0,.3);}
            100%{box-shadow:0 0 10px rgba(204,102,0,.8);}
        }
    </style>
</head>
<body>
<div class="content">
    <div class="content_name"><span class="label label-default" style="margin: 10px;font-size: 16px;">检查数据库</span><span id="tip"></span></div>
    <div id="field" style="margin-left: 20px;">
        <form id="form1" class="form-inline" style="display: inline-block" >
            <select id="dbname" style="width: 150px" name="db_name">
                <option stdb="" value="">请选择数据库</option>
                <option stdb="nn_cms" value="<?php echo $nn_cms; ?>">播控主库</option>
            </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <label>表名</label>
            <input style="height: 25px" type="text" name="table_name" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <label>字段名</label>
            <input style="height: 25px" type="text" name="field_name" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </form>
        <button id="btn1" class="btn btn-default">查询</button>
        <button style="margin-left: 20px;" id="btn2" class="btn btn-default">导入全量索引</button>
        <div class="checkbox">
            <label>
                <input id="is_check_index" type="checkbox" value="">检查索引
            </label>
        </div>
    </div>
    <div class="table_button">
        <a style="text-decoration: none;"  href="javascript:;" sdb="nn_cms" db="<?php echo $nn_cms; ?>" ><button class="btn btn-info"><span style="color: white;">播控主库</span></button></a>


    </div>
    <script> type="text/javascript">
    $(function(){
        $('#tip').hide();
        $('.content_table').hide();
        $('.index_table').hide();
        $('#btn1').click(function(){
            var select  = $("#dbname option:selected");
            var stdb = select.attr('stdb');
            if ($("#is_check_index").is(':checked'))
            {
                $('.content_table').hide();
                $.ajax({
                            url:'check_db.php',
                            data: $('#form1').serialize()+'&stdb='+stdb+'&index=',
                            type:"get",
                            success:function(data){
                $("#index_result").empty();
                var ts = '';
                var search_html = '';
                var obj = JSON.parse(data);
                if (obj.ret == 0 || obj.ret == 2)
                {
                    ts = obj.ts;
                    alert(ts);
                }
                if (obj.ret == 1)
                {
                    $('.index_table').show();
                    $.each(obj.row, function(index,item){
                        if (item.status !== '正常')
                        {
                            search_html += "<tr><td>" + item.table_name + "</td><td>" + item.index_name + "</td><td>" + item.now_nns_column + "</td><td>" + item.should_nns_column + "</td><td>" + item.version + "</td><td style='width:100px;text-align: center;' class='btn-danger'>" + item.status + "</td></tr>";
                        }else{
                            search_html += "<tr><td>" + item.table_name + "</td><td>" + item.index_name + "</td><td>" + item.now_nns_column + "</td><td>" + item.should_nns_column + "</td><td>" + item.version + "</td><td style='width:100px;text-align: center;' class='btn-success'>" + item.status + "</td></tr>";
                        }
                    });
                    $("#index_result").html(search_html);
                }

            }
                        });
                    }
            else
            {
                $('.index_table').hide();
                $.ajax({
                            url:'check_db.php',
                            data: $('#form1').serialize()+'&stdb='+stdb,
                            type:"get",
                            success:function(data){
                $("#result").empty();
                var ts = '';
                var search_html = '';
                var obj = JSON.parse(data);
                if (obj.ret == 0 || obj.ret == 2)
                {
                    ts = obj.ts;
                    alert(ts);
                }
                if (obj.ret == 1)
                {
                    $('.content_table').show();
                    $.each(obj.row, function(index,item){
                        if (item.status !== '正常')
                        {
                            search_html += "<tr><td>" + item.table_name + "</td><td>" + item.field_name + "</td><td>" + item.now_field_type + "</td><td>" + item.field_type + "</td><td>" + item.version + "</td><td style='width:100px;text-align: center;' class='btn-danger'>" + item.status + "</td></tr>";
                        }else{
                            search_html += "<tr><td>" + item.table_name + "</td><td>" + item.field_name + "</td><td>" + item.now_field_type + "</td><td>" + item.field_type + "</td><td>" + item.version + "</td><td style='width:100px;text-align: center;' class='btn-success'>" + item.status + "</td></tr>";

                        }
                    });
                    $("#result").html(search_html);
                }

            }
                        });
                    }

        });
        $('#btn2').click(function(){
            $.ajax({
                        type: "post",
                        url:'check_db.php',
                        dataType:"json",
                        data: {
                "export":'2'
                        },
                    success:function(data){
                if(data.ret == 1)
                {
                    alert('导入失败,原因:'+data.reason)
                            }
                else
                {
                    alert(data.reason);
                }
                window.location.reload();
            }
                    });
                });

        $("a").click(function()
        {
            $('#tip').empty();
            var db = $(this).attr("db");
            var sdb = $(this).attr("sdb");
            var tip = '正在检查'+db+'库,请耐心等候...';
            $('#tip').html(tip);
            $('#tip').show();
            if ($("#is_check_index").is(':checked'))
            {
                var index = '';
                $.ajax({
                            type: "post",
                            url: "check_db.php",
                            dataType:"json",
                            data: {
                "sdb":sdb,
                                "db":db,
                                "index":index
                            },
                            success: function(data) {
                $('#tip').hide();
                $('.content_table').hide();
                $('.index_table').show();
                $("#index_result").empty();
                var html ='';
                $.each(data, function(index,item){
                    if (item.status !== '正常') {
                        html += "<tr><td>" + item.table_name + "</td><td>" + item.index_name + "</td><td>" + item.now_nns_column + "</td><td>" + item.should_nns_column + "</td><td>" + item.version + "</td><td style='width:100px;text-align: center;' class='btn-danger'>" + item.status + "</td></tr>";
                    }
                });
                $("#index_result").html(html);
            }
                        });
                    }
            else
            {
                $.ajax({
                            type: "post",
                            url: "check_db.php",
                            dataType:"json",
                            data: {
                "sdb":sdb,
                                "db":db
                            },
                            success: function(data) {
                $('#tip').hide();
                $('.index_table').hide();
                $('.content_table').show();
                $("#result").empty();
                var html ='';
                $.each(data, function(index,item){
                    if (item.status !== '正常') {
                        html += "<tr><td>" + item.table_name + "</td><td>" + item.field_name + "</td><td>" + item.now_field_type + "</td><td>" + item.field_type + "</td><td>" + item.version + "</td><td style='width:100px;text-align: center;' class='btn-danger'>" + item.status + "</td></tr>";
                    }
                });
                $("#result").html(html);
            }
                        });
                    }
        });
    });
    </script>
    <div class="content_table">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>表名</th>
                <th>字段名</th>
                <th>当前类型</th>
                <th>应为类型</th>
                <th>版本号</th>
                <th style='text-align: center;'>状态</th>
            </tr>
            </thead>
            <tbody id="result">

            </tbody>
        </table>
    </div>
    <div class="index_table">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>表名</th>
                <th>索引名</th>
                <th>当前字段</th>
                <th>应为字段</th>
                <th>版本号</th>
                <th style='text-align: center;'>状态</th>
            </tr>
            </thead>
            <tbody id="index_result">
            </tbody>
        </table>
    </div>
</div>
</body>
</html>