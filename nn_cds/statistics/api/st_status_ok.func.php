<?php
/*
 * Created on 2014-2-28
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 function st_status_ok($db,$params){
 	$begin_time=$params['begin'];
 	$end_time=$params['end'];
 		
 	$ex_sql='';
 	if (!empty($begin_time)){
 		$ex_sql.=' and nns_create_time >= "'.$begin_time.'" ';
 	}
 	
 	if (!empty($end_time)){
 		$ex_sql.=' and nns_create_time < "'.$end_time.'" ';
 	}
 	
 	if (!empty($params['sp'])){
 		 $ex_sql.=' and nns_org_id="'.$params['sp'].'"';
 	}
    
    if(!empty($params['category'])){
        $ex_sql.=' and nns_category_id="'.$params['category'].'"';
    }
 	
	$re=__st_status_media_ok($db,$ex_sql);
	
	$re1=__st_status_video_ok($db,$ex_sql);
	$re2=__st_status_index_ok($db,$ex_sql);
	
	$re['video']=$re1;
	$re['index']=$re2;

	
	
//	return array(
//		'clip'=>array(
//			'done'=>100,
//			'undone'=>200,
//			'total'=>300
//		),'video'=>array(
//			'done'=>100,
//			'undone'=>200,
//			'total'=>300
//		),'media'=>array(
//			'done'=>100,
//			'undone'=>200,
//			'total'=>300
//		),'index'=>array(
//			'done'=>100,
//			'undone'=>200,
//			'total'=>300
//		)
//	
//	);

	$task=__st_queue_check($db,$params['sp'],$params['category']);
	$re['task']=$task;
	
	$re['clip_fail']=__st_clip_fail($db,$params['sp']);
	
	$re['inject_fail']=__st_inject_fail($db,$params['sp']);
	return $re;
	
 }
 
 
 function __st_inject_fail($db,$sp_id){
 	$sql="SELECT COUNT(1) AS num,nns_type,nns_action FROM nns_mgtvbk_c2_task WHERE nns_org_id='{$sp_id}' AND nns_status='-1' AND nns_op_id!='' GROUP BY  nns_type,nns_action";
 	$re=nl_query_by_db($sql,$db);
 
 	$task_fail=array();
 	if (is_array($re)){
 		foreach ($re as $item){
 			$task_fail[$item['nns_type']]=__st_is_array($task_fail[$item['nns_type']]);
 			
 			
 			
 			if (empty($task_fail[$item['nns_type']][$item['nns_action']])){
 				$task_fail[$item['nns_type']][$item['nns_action']]=0;
 			}
 			
 			$task_fail[$item['nns_type']][$item['nns_action']] += $item['num'];
 		}
 	}
 	
 	return $task_fail;
 	
 }
 
 
 function __st_clip_fail($db,$sp_id){
// 	$sql="SELECT COUNT(DISTINCT nns_op_id) AS num FROM nns_mgtvbk_clip_task WHERE nns_org_id='fjyd'  AND  nns_state IN ('get_media_url_fail','download_fail','clip_fail') ";
 	$sql="SELECT COUNT(1) AS num FROM nns_mgtvbk_clip_task WHERE nns_org_id='$sp_id'  AND  nns_state IN ('get_media_url_fail','download_fail','clip_fail') and nns_op_id in (select nns_id from nns_mgtvbk_op_queue where nns_org_id='$sp_id' and nns_status='3')";
 	$re=nl_query_by_db($sql,$db);
 
 	if (is_array($re)){
 		return $re[0]['num'];
 	}else{
 		return 0;
 	}
 }
 
 
 function __st_queue_check($db,$sp_id,$category=null){
    if($category){
        $sql='select count(1) as num,a.nns_status,a.nns_type,a.nns_action from nns_mgtvbk_op_queue as a, nns_mgtvbk_op_log as b 
    where a.nns_org_id="'.$sp_id.'"  and (a.nns_id=b.nns_op_id and a.nns_status=b.nns_status and b.nns_category_id="'.$category.'")  group by a.nns_type,a.nns_status,a.nns_action';
    }
    else 
    {
       $sql='select count(1) as num,nns_status,nns_type,nns_action from nns_mgtvbk_op_queue where nns_org_id="'.$sp_id.'"  group by nns_type,nns_status,nns_action';
       
    }
 	$re=nl_query_by_db($sql,$db);
 
 	$task=array();

 	if (is_array($re)){
 		foreach ($re as $item){
 			$task[$item['nns_type']]=__st_is_array($task[$item['nns_type']]);
 			
 			switch  ($item['nns_status']){
 				case BK_OP_WAIT:
 					$key='wait';
 				break;
 				case BK_OP_PROGRESS:
 					$key='doing';
 				break;
 				case BK_OP_CLIP:
 					$key='doing_clip';
 				break;
 				case BK_OP_WAIT_CLIP:
 					$key='wait_clip';
 				break;
 				default:
 					$key='';
 				break;
 				
 			}
 			
 			if (empty($key)) continue;
 		
 			
 			if (empty($task[$item['nns_type']][$item['nns_action'].'_'.$key])){
 				$task[$item['nns_type']][$item['nns_action'].'_'.$key]=0;
 			}
 			
 			$task[$item['nns_type']][$item['nns_action'].'_'.$key] += $item['num'];
 		}
 	}
 	
 	return $task;
 }
 
 
 function __st_is_array($obj){
 	if (!is_array($obj)){
 		$obj=array();
 	}
 	return $obj;
 }
 
  function __st_status_index_ok($db,$ex_sql){
 		// 	计算片源情况
	$sql='select count(1) as num,nns_status,nns_action from nns_mgtvbk_op_log where nns_type="'.BK_OP_INDEX.'"'.$ex_sql.' group by nns_status,nns_action';
	
	$re=nl_query_by_db($sql,$db);
	
	$index=array();
	$index['done']=0;
	$index['undone']=0;
	$total_media=0;
	if (is_array($re)){
	foreach ($re as $item){
		switch ($item['nns_status']){
			case BK_OP_OK:
				$index['done'] += (int)$item['num'];
				$key=$item['nns_action'].'_done';
				$index[$key]=(int)$item['num'];
			break;
			case BK_OP_WAIT:
				$total_media += (int)$item['num'];
				$key=$item['nns_action'].'_total';
				$index[$key]=(int)$item['num'];
			break;
		}
	}
	}
	
	$index['undone']=$total_media-$index['done'];
	
	$index['total']=$total_media;
	$re=NULL;
	return $index;
  }
 
 
  function __st_status_video_ok($db,$ex_sql){
 		// 	计算片源情况
	$sql='select count(1) as num,nns_status,nns_action from nns_mgtvbk_op_log where nns_type="'.BK_OP_VIDEO.'"'.$ex_sql.' group by nns_status,nns_action';
//	var_dump($sql);die;
	$re=nl_query_by_db($sql,$db);
	
	$video=array();
	$video['done']=0;
	$video['undone']=0;
	$total_media=0;
	if (is_array($re)){
	
	foreach ($re as $item){
		switch ($item['nns_status']){
			case BK_OP_OK:
				$video['done'] += (int)$item['num'];
				$key=$item['nns_action'].'_done';
				$video[$key]=(int)$item['num'];
			break;
			case BK_OP_WAIT:
				$total_media += (int)$item['num'];
				$key=$item['nns_action'].'_total';
				$video[$key]=(int)$item['num'];
			break;
		}
	}
	}
	
	$video['undone']=$total_media-$video['done'];
	
	$video['total']=$total_media;
	$re=NULL;
	return $video;
  }
 function __st_status_media_ok($db,$ex_sql){
 	// 	计算片源情况
	$sql='select count(1) as num,nns_status,nns_action,sum(nns_file_len) as len,sum(nns_file_size) as size from nns_mgtvbk_op_log where nns_type="'.BK_OP_MEDIA.'"'.$ex_sql.' group by nns_status,nns_action';
	$re=nl_query_by_db($sql,$db);
	//var_dump($sql);die;
	$clip=array();
	$clip['done']=0;
	$clip['undone']=0;
	$media=array();
	$media['done']=0;
	$media['undone']=0;
	$total_media=0;
	$total_clip=0;
	if (is_array($re)){
	foreach ($re as $item){
		switch ($item['nns_status']){
			case BK_OP_WAIT:
				$clip['done'] += (int)$item['num'];
				$total_media  += (int)$item['num'];
				$key=$item['nns_action'].'_done';
				$clip[$key]=(int)$item['num'];
				$key1=$item['nns_action'].'_total';
				$media[$key1]=(int)$item['num'];
			break;
			case BK_OP_OK:
				$media['done'] += (int)$item['num'];
				$key=$item['nns_action'].'_done';
				$media[$key]=(int)$item['num'];
                $media[$key.'_len'] = (int)$item['len'];
                $media[$key.'_size'] = (int)$item['size'];
			break;
			case BK_OP_WAIT_CLIP:
				$total_clip += (int)$item['num'];
				$key=$item['nns_action'].'_total';
				$clip[$key]=(int)$item['num'];
			break;
		}
	}
	}
	
	$media['undone']=$total_media-$media['done'];
	$clip['undone']=$total_clip-$clip['done'];
	$media['total']=$total_media;
	$clip['total']=$total_clip;
	
	$re=NULL;
	return array('media'=>$media,'clip'=>$clip);
 }
 
?>
