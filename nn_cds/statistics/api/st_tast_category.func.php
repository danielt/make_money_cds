<?php

function st_tast_category($db,$params) {

	$num = __st_category_all($db,$params);
	if($num === true || empty($num)) {
		$num = 0;
	} 
	$count = __st_index_category($db, $params);
	if($count === true || empty($count[0]['dex'])) {
		$dex = 0;
	} else {
		$dex = $count[0]['dex'];
	}
	
	$file_size = __st_media_category($db, $params);
	if($file_size === true || empty($file_size[0]['size'])) {
		$size = 0;
	} else {
		$size = $file_size[0]['size'];
	}
	
	$media_count = __st_media_count_category($db, $params);

	if($media_count === true || empty($media_count[0]['num'])) {
		$media = 0;
	} else {
		$media = $media_count[0]['num'];
	}
	$data = array(
					'num' => $num,
					'dex' => $dex,
					'size' => $size,
					'media' => $media,			
				);
	return $data;
}
//计算总数
function __st_category_all($db, $info) {
	$str_sql = 'select count(*) as num from nns_vod where nns_category_id like "'.$info["category_id"].'%"';
	$re = nl_query_by_db($str_sql,$db);
	
	if($re === false) {
		return false;
	} 
	return $re[0]['num'];
}
//计算分集总时间
function __st_index_category($db,$item) {
	if(strlen($item['category_id']) > 5) {
		$sql = 'select sum(nns_index) as dex from nns_vod_index as n right join nns_vod as v on n.nns_vod_id=v.nns_id where v.nns_category_id="'.$item["category_id"].'"';
	} else {
		$sql = 'select sum(nns_index) as dex from nns_vod_index';
	}

	$result = nl_query_by_db($sql,$db);

	if($result === false) {
		return false;
	}
	return $result;
}

//计算片源文件大小
function __st_media_category($db,$info) {
	if(strlen($info['category_id']) > 5) {
		$sql = 'select sum(nns_file_size) as size from nns_vod_media as a right join nns_vod as v on a.nns_vod_id=v.nns_id where v.nns_category_id="'.$info["category_id"].'"';
	} else {
		$sql = 'select sum(nns_index) as size from nns_vod_meida';
	}
	
	$result = nl_query_by_db($sql,$db);
	
	if($result === false) {
		return false;
	}
	return $result;
}

//计算片源总数
function __st_media_count_category($db,$param) {
	if(strlen($param['category_id']) > 5) {
		$sql = 'select count(*) as num from nns_vod_media as a right join nns_vod as v on a.nns_vod_id=v.nns_id where v.nns_category_id="'.$param["category_id"].'"';
	} else {
		$sql = 'select count(*) as num from nns_vod_media';
	}

	$result = nl_query_by_db($sql,$db);

	if($result === false) {
		return false;
	}
	return $result;
}