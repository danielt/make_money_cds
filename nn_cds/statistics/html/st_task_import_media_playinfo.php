<?php
/**
  * Use：新增导入点播片源播放串的CSV文件
  * Author：kan.yang@starcor.cn
  * DateTime：18-3-12 下午1:50
  * Description：
*/
ini_set('display_errors',0);
include_once dirname(dirname(dirname(__FILE__))) . "/v2/common.php";
$project = evn::get("project");
include_once dirname(dirname(dirname(__FILE__))) . '/v2/ns_api/' . $project . '/delivery/' . $_REQUEST['sp_id'] . '/import_cdn_media.class.php';

$obj_import_cdn_media = new import_cdn_media();
$str_reload_url = "window.location.href='st_task_import_media_playinfo.php?action=import_media_playurl&sp_id=" . $_REQUEST['sp_id'] . "';";
if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'import_media_playurl')
{
    if (isset($_REQUEST['upload']) && $_REQUEST['upload'] == 1)
    {
        $arr_import_ret  =$obj_import_cdn_media->import_csv_file();
        if($arr_import_ret['ret'] == 0)
        {
            echo "<script>alert('" . $arr_export_ret['reason'] . "');" . $str_reload_url . "</script>";
        }
        else
        {
            echo "<script>alert('" . $arr_export_ret['reason'] . "');" . $str_reload_url . "</script>";
        }
        die();
    }
}
elseif(isset($_REQUEST['action']) && $_REQUEST['action'] == 'execute_media_file')
{
    $arr_execute_ret = $obj_import_cdn_media->execute_media_cdn_flow();
    if($arr_execute_ret['ret'] == 0)
    {
        echo "<script>alert('" . $arr_export_ret['reason'] . "');" . $str_reload_url . "</script>";
    }
    else
    {
        echo "<script>alert('" . $arr_export_ret['reason'] . "');" . $str_reload_url . "</script>";
    }
    die();
}
unset($obj_import_cdn_media);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../../nn_cms_manager/css/allstyle.css" rel="stylesheet" type="text/css" />
    <link href="../../nn_cms_manager/css/rightframestyle.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../../nn_cms_manager/css/dtree.css" type="text/css" />
    <link href="../../nn_cms_manager/css/selectbox.css" rel="stylesheet" type="text/css" />
    <link href="../../nn_cms_manager/css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../nn_cms_manager/js/jquery-1.4.3.min.js"></script>
    <script type="text/javascript">
        //当前SP ID
        var sp_id = "<?php echo $_GET['sp_id'];?>";

        function import_media_file()
        {
            $('#add_form').submit();
        }

        function execute_media_file()
        {
            var url = 'st_task_import_media_playinfo.php?action=execute_media_file&sp_id=' + sp_id;
            window.location.href = url;
        }

    </script>
</head>
<body>
    <div class="content">
        <div class="content_position">导入点播片源播放串</div>
        <div class="content_table formtable">
            <form action="st_task_import_media_playinfo.php?sp_id=<?php echo $_REQUEST['sp_id'];?>&upload=1&action=<?php echo $_REQUEST['action']; ?>" id="add_form" method="post" enctype="multipart/form-data">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="120px">导入csv文件:&nbsp;&nbsp;
                        </td>
                        <td>
                            <input name="csv" id="csv" type="file" size="60%"/>
                        </td>
                        <td>
                            <input type="button" value="上传文件" onclick="import_media_file()" />
                        </td>
                        <td>
                            <input type="button" value="导入" onclick="execute_media_file()"/>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
    <div></div>
</body>
</html>

