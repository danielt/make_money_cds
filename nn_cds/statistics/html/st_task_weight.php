<?php 
ini_set('display_errors',0);

if (isset($_REQUEST['upload']) && $_REQUEST['upload']==1){
    include_once '../api/st_get_csv.func.php';
    $re=st_get_csv();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../nn_cms_manager/css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../nn_cms_manager/css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../nn_cms_manager/css/dtree.css" type="text/css" />
<link href="../../nn_cms_manager/css/selectbox.css" rel="stylesheet" type="text/css" />
<link href="../../nn_cms_manager/css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
<style>
#status p{
	padding:5px;
}

#status .red{
	color:#ff0000;
}
#status .green{
	color:green;
}
#status .glay{
	color:#666666;
}
#status span{
	float:right;
}

#status{
	font-size:12px;color:#666666;background:#eeeeee;padding:5px 10px;
}

</style>
<script language="javascript" src="../../nn_cms_manager/js/dtree.js"></script>
<script language="javascript" src="../../nn_cms_manager/js/jquery-1.4.3.min.js"></script>
<script language="javascript" src="../../nn_cms_manager/js/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="../../nn_cms_manager/js/cms_cookie.js"></script>
<script language="javascript" src="../../nn_cms_manager/js/table.js"></script>
<script language="javascript" src="../../nn_cms_manager/js/checkinput.js.php"></script>
<script language="javascript" src="../../nn_cms_manager/js/tabs.js"></script>
<script language="javascript" src="../js/st_task_weight.js"></script>
<script language="javascript">
	
	var sp_id="<?php echo $_GET['sp_id'];?>";
</script>
</head>

<body>
<div class="content">
	<div class="content_position">导入缺失影片记录</div>
		<div class="content_table formtable">
		<form action="st_task_weight.php?sp_id=<?php echo $_REQUEST['sp_id'];?>&upload=1" id="add_form" method="post" enctype="multipart/form-data">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="120px">导入csv文件:&nbsp;&nbsp;
					</td>
					
					<td>
						<input name="csv" id="csv" type="file" size="60%"/>
					</td>
					<td>
						<input type="submit" value="导入"  id="btn6" />
					</td>
					
					<?php if (is_array($re)){?>
					<td>
						<input name="weight" id="weight" type="text" />&nbsp;<input type="button" value="设置权重"  id="select_btn" />
					</td>
					
					<?php }?>
				</tr>
			</table>
			</form>
		</div>
<div  id="status"></div>
<div class="content_table formtable" id="view">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<th><input name="" type="checkbox" value="" /></th>
				<th>序号</th>
				<th>影片名</th>
				<th>状态</th>
				<th>类型</th>
				<th width="300px">分集索引</th>
				 <th>创建时间</th>
				 
			</tr>
		</thead>
		<tbody>
			<?php if (is_array($re)){
				foreach ($re as $item){?>
				<tr>
					<td><input name="" type="checkbox" attr="<?php echo $item["num"]; ?>" value="<?php echo $item["id"]; ?>" /></td>
					<td><?php echo $item["num"]; ?></td>
					<td><?php echo $item["name"]; ?></td>
					<td><?php echo $item["status"]; ?></td>
					<td><?php echo $item["type"]; ?></td>
					<td><?php echo $item["indexes"]; ?></td>
					<td><?php echo $item["time"]; ?></td>
				</tr>
			<?php }}?>
		  </tbody>
		</table>
	</div>
</div>
</body>
</html>

