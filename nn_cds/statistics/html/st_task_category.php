<?php
header("Content-Type:text/html;charset=utf-8");
/*
 * Created on 2012-2-25
 *
 * To change the temservice for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Temservices
 */
include ("../../nn_cms_manager/nncms_manager_inc.php");
//加载多语言
include_once($nncms_config_path . 'nn_cms_manager/nncms_mlang.php');
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_check_login.php";
include $nncms_config_path . "nn_cms_manager/controls/nncms_controls_public_function.php";

if ($_GET["nns_org_id"] == "" || $_GET["nns_org_type"] == "") {
        $nns_org_id = $_SESSION["nns_org_id"];
        $nns_org_type = $_SESSION["nns_manager_type"];
} else {
        $nns_org_id = $_GET["nns_org_id"];
        $nns_org_type = $_GET["nns_org_type"];
}

require_once $nncms_config_path . "nn_cms_config/nn_cms_global.php";
include($nncms_db_path . "nns_depot/nns_db_depot_class.php");
$depot_inst = new nns_db_depot_class();

$depot_array = $depot_inst->nns_db_depot_list("", $nns_org_type, $nns_org_id, 0);
// var_dump($partner_array);
if ($depot_array["ret"] != 0) {
        $data = null;
        echo "<script>alert('" . $depot_array["reason"] . "');</script>";
}
$depot_inst = null;
$data = $depot_array["data"];

include $nncms_db_path . "nns_carrier/nns_db_carrier_class.php";
include $nncms_db_path . "nns_partner/nns_db_partner_class.php";
include $nncms_db_path . "nns_group/nns_db_group_class.php";

if ($_SESSION["nns_manager_type"] == 0) {
        $carrier_inst = new nns_db_carrier_class();
        $carrier_result = $carrier_inst->nns_db_carrier_list();

        if ($carrier_result["ret"] == 0) {
                $carrier_data = $carrier_result["data"][0];
                $base_name = $carrier_data["nns_name"];
        }
}

//		 	类型为合作伙伴
if ($nns_org_type == 1) {
        $partner_inst = new nns_db_partner_class();
        $partner_result = $partner_inst->nns_db_partner_info($nns_org_id);

        if ($partner_result["ret"] == 0) {
                $partner_data = $partner_result["data"][0];
                $base_name = $partner_data["nns_name"];
        }
        //	类型为集团
} else if ($nns_org_type == 2) {
        $group_inst = new nns_db_group_class();
        $group_result = $group_inst->nns_db_group_info($nns_org_id);

        if ($group_result["ret"] == 0) {
                $group_data = $group_result["data"][0];
                $base_name = $group_data["nns_name"];
        }
}
$base_id = 10000;
//var_dump($data);die;
?>
<!DOCTYPE html>
<html>
        <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                <link href="../css/allstyle.css" rel="stylesheet" type="text/css" />
                <link href="../css/rightframestyle.css" rel="stylesheet" type="text/css" />
                <link rel="stylesheet" href="../css/selectbox.css" rel="stylesheet" type="text/css" />
                <link href="../css/dtree.css" type="text/css" rel="stylesheet"/>
                <link href="../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="../../nn_cms_manager/js/jquery-1.4.3.min.js"></script>
                <script language="javascript" src="../../nn_cms_manager/js/jquery-ui-1.8.16.custom.min.js"></script>
                <script language="javascript" src="../js/dtree.js"></script>
                <script language="javascript" src="../js/category_count.js"></script>
                <script language="javascript" src="../../nn_cms_manager/js/table.js.php"></script>
                <script language="javascript" src="../../nn_cms_manager/js/checkinput.js.php"></script>
                <script language="javascript" src="../../nn_cms_manager/js/tabs.js"></script>
                <script language="javascript" src="../../nn_cms_manager/js/cms_datepicker.js"></script>
                <script language="javascript" src="../../nn_cms_manager/js/cms_cookie.js"></script>
                <script language="javascript" src="../../nn_cms_manager/js/cms_alert_select_box_style.js"></script>

                <script language='javascript' src="../js/st_status_category.js"></script>
                <link rel="stylesheet" href="../css/demo.css" type="text/css"/>
				<link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css"/>

        </head>

        <body>
        <div class="alert alert-danger" style="display:none;margin:0px auto;width:300px;position:fixed;z-index:100;float:right;"><b>读取中...</b></div>
        	<div class="content">
        		<div class="content_position" style="padding:10px;"></div>
            	<div style="background-color:#FFF;">
					<div class="category_tree" style="float:left;">
                    	<div style="padding:5px; background:#eeeeee; border:3px solid #ccc; margin-bottom:10px;">
                        	<?php if ($_SESSION["nns_manager_type"] == 0) { ?>
                        	<table>
                            <tr class="select_carrier_class">
								<td><select name="nns_carrier" id="nns_carrier"  style=" padding:3px; width:100%;"><br>
                                <option value="<?php echo $carrier_data["nns_id"]; ?>" <?php if ($nns_org_type == 0) { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('xtgl_yysgl'), '/', $carrier_data["nns_name"]; ?></option>
                                <?php if ($g_partner_enabled == 1) { ?>
                                <option value="1"  <?php if ($nns_org_type == 1) { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('partner'); ?></option>
                                <?php }if ($g_group_enabled == 1) { ?>
                                <option value="2"  <?php if ($nns_org_type == 2) { ?>selected="selected"<?php } ?>><?php echo cms_get_lang('group_jtxx'); ?></option>
                                <?php } ?>
                                </select></td>
                                <td></td>
                                <td></td>
                             </tr>
                             </table>                  
                             <?php } ?>
                        </div>
                             <?php
                             if ($data != null) {
                             	$assist_content = $data[0]["nns_category"];
                             } else {
                                $assist_content = null;
                             }
                             echo pub_func_get_tree_by_html($assist_content, $base_name, $base_id);
                             ?>
                         
					</div>
					<div class="category_editbox" style="float:right;width:80%;margin:0 20px 0 5px;">
                    	<div class="content_table">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        	<tbody>
                            	<tr>
                                	<td class="rightstyle2c"><font class="font_red" style="color:#F00;font-size:16px; font-weight:bold;">*</font>
<?php echo cms_get_lang('lmgl_nrlmid') . '/' . cms_get_lang('name'); ?>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="category_edit_id"></span>/<span class="category_edit_name"></span></td>
                                </tr>
                            </tbody>
                         </table>
                         <div style="background:#BBD7F0;width:100%;height:30px;font-size:12px;color:white;padding:5px 20px;">主媒资总数</div>
                         <div id="assist_num" style="background:white;width:100%;height:40px;font-size:14px;color:red;padding:10px 30px;border-left:1px solid #BBD7F0;border-right:1px solid #BBD7F0;"></div>
                         <div style="background:#BBD7F0;width:100%;height:30px;font-size:12px;color:white;padding:5px 20px;">分集总集数</div>
                         <div id="index_num" style="background:white;width:100%;height:40px;font-size:14px;color:red;padding:10px 30px;border-left:1px solid #BBD7F0;border-right:1px solid #BBD7F0;"></div>
                         <div style="background:#BBD7F0;width:100%;height:30px;font-size:12px;color:white;padding:5px 20px;">片源总数</div>
                         <div id="media_count" style="background:white;width:100%;height:40px;font-size:14px;color:red;padding:10px 30px;border-left:1px solid #BBD7F0;border-right:1px solid #BBD7F0;"></div>
                         <div style="background:#BBD7F0;width:100%;height:30px;font-size:12px;color:white;padding:5px 20px;">片源文件大小</div>
                         <div id="media_file" style="background:white;width:100%;height:40px;font-size:14px;color:red;padding:10px 30px;border-left:1px solid #BBD7F0;border-right:1px solid #BBD7F0;"></div>
                    	</div>
                	</div>
                	<div style="clear:both;"></div>
                </div>
              </div>
        </body>
</html>