<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="../../nn_cms_manager/css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../nn_cms_manager/css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../../nn_cms_manager/css/dtree.css" type="text/css" />
		<link href="../../nn_cms_manager/css/selectbox.css" rel="stylesheet" type="text/css" />
		<link href="../../nn_cms_manager/css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
		<style>
			#status p {
				padding: 5px;
			}

			#status .red {
				color: #ff0000;
			}
			#status .green {
				color: green;
			}
			#status .glay {
				color: #666666;
			}
			#status span {
				float: right;
			}

			#status {
				font-size: 12px;
				color: #666666;
				background: #eeeeee;
				padding: 5px 10px;
			}

			.header {
				padding: 5px 10px;
				background: #BBD7F0;
				color: #fff;
				font-weight: bold;
			}

			.content_table {
				color: #fff;
			}

			.content_table table thead th {
				background: #eee;
				color: #666;
				font-weight: bold;
				font-size: 14px;
			}
			.inject {
				width: 200px;
			}
			.html_color{
				color: red;
			}
		</style>
		<link href="../../nn_cms_manager/css/allstyle.css" rel="stylesheet" type="text/css" />
		<link href="../../nn_cms_manager/css/rightframestyle.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../nn_cms_manager/js/jquery-1.4.3.min.js"></script>

		<script language="javascript" src="../js/st_task_import_state.js?val=<?php echo time();?>"></script>
		<script language="javascript" src="../js/ichart.js"></script> 
		<script language="javascript" src="../js/ichart.1.2.min.js"></script>
		
		<script language="javascript" src="../../nn_cms_manager/js/checkinput.js.php"></script>

		<script language="javascript" src="../../nn_cms_manager/js/table.js.php"></script>

		<script type="text/javascript" src="../js/bootstrap.min.js"></script>
		<script type="text/javascript" src="../js/chart.js"></script>
		<link rel="stylesheet" href="../css/demo.css" type="text/css"/>
		<link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css"/>

		<script language="javascript" src="../js/cms_datepicker.js"></script>
		<script language="javascript" src="../js/Starcor.S67.js"></script> 
		<script language="javascript"> 
		var timestmp =<?php echo time(); ?>;
			var sp_id = '<?php echo $_GET['sp_id']; ?>';
	var nowtime=<?php echo strtotime(date('Ymd')); ?>;
		</script>
	</head>

	<body>
		<div class="alert alert-danger" style="margin:0px auto;width:300px;position:fixed;z-index:100;float:right;">
			<b>读取中...</b>
		</div>

		<div class="content" style="background:#fff;">
			<div class="content_position" style="padding:10px;"></div>
			<div class="content_table">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
                        <td id="num"></td>
						<td style="padding:10px 25px;">
						<div class="btn-group">
							<button type="button" class="btn btn-primary" id="btn2">
								今日
							</button>
							<button type="button" class="btn btn-primary" id="btn3">
								昨日
							</button>
							<button type="button" class="btn btn-primary" id="btn4">
								前天
							</button>
							<button type="button" class="btn btn-primary" id="btn5">
								前一周
							</button>
							<button type="button" class="btn btn-primary" id="btn6">
								全部
							</button>
						</div></td>
						<td style="padding:10px 25px; z-index:100">
						<input  name="begin_date"  id="begin_date" type="text"
						value="" readonly="readonly" style="width:200px; padding:5px 10px;" class="datepicker"/>
						-
						<input  name="end_date" id="end_date" type="text"
						value="" readonly="readonly"  style="width:200px;padding:5px 10px;"  class="datepicker"/>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<button type="button" class="btn btn-primary"  id="btn7">
							自定义时间查询
						</button></td>
					</tr>
				</table>
			</div> 
		    <div class="content_table">
				<div class="header">
					统计图表
				</div>
				<div id="canvasDiv">统计</div>
			</div>
			</br></br></br></br></br></br>
		</div> 
	</body>
</html>
