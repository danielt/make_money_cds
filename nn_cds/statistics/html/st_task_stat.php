<?php 
ini_set('display_errors',0);
	if (isset($_REQUEST['upload']) && $_REQUEST['upload']==1){
		include_once '../api/st_get_csv.func.php';
		$re=st_get_csv();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../../nn_cms_manager/css/allstyle.css" rel="stylesheet" type="text/css" />
<link href="../../nn_cms_manager/css/rightframestyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../nn_cms_manager/css/dtree.css" type="text/css" />
<link href="../../nn_cms_manager/css/selectbox.css" rel="stylesheet" type="text/css" />
<link href="../../nn_cms_manager/css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />

 <link href="../../nn_cms_manager/css/allstyle.css" rel="stylesheet" type="text/css" />
           <link href="../../nn_cms_manager/css/rightframestyle.css" rel="stylesheet" type="text/css" />
           <script language="javascript" src="../../nn_cms_manager/js/jquery-1.4.3.min.js"></script>
          
           
           <script language="javascript" src="../../nn_cms_manager/js/checkinput.js.php"></script>
           
           <script language="javascript" src="../../nn_cms_manager/js/table.js.php"></script>
		
		<script type="text/javascript" src="../js/bootstrap.min.js"></script>		
		<link rel="stylesheet" href="../css/demo.css" type="text/css"/>
		<link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css"/>
		
		<script language="javascript" src="../js/cms_datepicker.js"></script>
		<script language="javascript" src="../js/Starcor.S67.js"></script>
		<script language="javascript" src="../js/st_status_stat.js"></script>
<script language="javascript">
	var timestmp=<?php echo time();?>;
			var sp_id='<?php echo $_GET['sp_id'];?>';
			var nowtime=<?php echo strtotime(date('Ymd'));?>;
</script>

<style>
#status p{
	padding:5px;
}

#status .red{
	color:#ff0000;
}
#status .green{
	color:green;
}
#status .glay{
	color:#666666;
}
#status span{
	float:right;
}

#status{
	font-size:12px;color:#666666;background:#eeeeee;padding:5px 10px;
}

.header{
	padding:5px 10px;
	background:#BBD7F0;
	color:#fff;
	font-weight:bold;
}

.content_table {
	color:#fff;
	
}

.content_table table thead th{
	background:#eee;
	color:#666;
	font-weight:bold;
	font-size:14px;
}
.inject{
	width:200px;
}
</style>
</head>

<body>
<div class="alert alert-danger" style="margin:0px auto;width:300px;position:fixed;z-index:100;float:right;"><b>读取中...</b></div>
  <div class="content" style="background:#fff;">
      <div class="content_position" style="padding:10px;"></div>
      <div class="content_table">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr>
               <td style="padding:10px 25px;">
					<div class="btn-group">
					  <button type="button" class="btn btn-primary" id="btn2">今日</button>
					  <button type="button" class="btn btn-primary" id="btn3">昨日</button>
					  <button type="button" class="btn btn-primary" id="btn4">前天</button>
					  <button type="button" class="btn btn-primary" id="btn5">前一周</button>
					  <button type="button" class="btn btn-primary" id="btn6">全部</button>
					</div>
					<div class="form-group"  style="width:130px; overflow: hidden; float: right;margin-bottom:0">
    					<select class="form-control" style="width:120px;" id='category'>
                          <option value="">全部栏目</option>
                          <option value="10000001">电影</option>
                          <option value="10000002">电视剧</option>
                          <option value="10000003">综艺</option>
                          <option value="10000004">动漫</option>
                          <option value="10000005">音乐</option>
                          <option value="10000006">纪实</option>
                        </select>
                    </div>
               </td>
               <td style="padding:10px 25px; z-index:100">
               		<input  name="begin_date"  id="begin_date" type="text"
					 value="" readonly="readonly" style="width:200px; padding:5px 10px;" class="datepicker"/>
					  - 
					  <input  name="end_date" id="end_date" type="text"
					 value="" readonly="readonly"  style="width:200px;padding:5px 10px;"  class="datepicker"/> 
					  &nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary"  id="btn7">自定义时间查询</button>
               </td>
            </tr>
           </table>                        
         </div>
  <div class="content_table">
  		<div class="header">主媒资</div>
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<th></th>
				<th colspan="3"><b>添加</b></th>
				<th colspan="3"><b>修改</b></th>
				<th colspan="3"><b>删除</b></th>
			</tr>
		</thead>
		<tbody>
				<tr>
				    <td class="inject">中心同步指令数量</td>
				    <td colspan="3" class="add_total" style="text-align:center;">&nbsp;</td> 
				    <td colspan="3" class="modify_total" style="text-align:center;">&nbsp;</td>
				    <td colspan="3" class="delete_total" style="text-align:center;">&nbsp;</td>
				 </tr>
						
			  <tr>
			    <td rowspan="2">执行情况</td>
			    <td>等待注入</td>
			    <td>正在注入</td>
			    <td>注入完成</td>
			    <td>等待注入</td>
			    <td>正在注入</td>
			    <td>注入完成</td>
			    <td>等待注入</td>
			    <td>正在注入</td>
			    <td>注入完成</td>
			  </tr>
			  <tr>
			    <td class="add_wait">&nbsp;</td>
			    <td class="add_doing">&nbsp;</td>
			    <td class="add_done">&nbsp;</td>
			    <td class="modify_wait">&nbsp;</td>
			    <td class="modify_doing">&nbsp;</td>
			    <td class="modify_done">&nbsp;</td>
			    <td class="delete_wait">&nbsp;</td>
			    <td class="delete_doing">&nbsp;</td>
			    <td class="delete_done">&nbsp;</td>
			  </tr>
			</tbody>	
		</table>
	</div>
	 <div class="content_table"> 
	 <div class="header">分集</div>
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<th><b></b></th>
				<th colspan="3"><b>添加</b></th>
				<th colspan="3"><b>修改</b></th>
				<th colspan="3"><b>删除</b></th>
			</tr>
		</thead>
		<tbody>			
			  <tr>
				    <td class="inject">中心同步指令数量</td>
				    <td colspan="3" class="add_total" style="text-align:center;">&nbsp;</td> 
				    <td colspan="3" class="modify_total" style="text-align:center;">&nbsp;</td>
				    <td colspan="3" class="delete_total" style="text-align:center;">&nbsp;</td>
				 </tr>
						
			  <tr>
			    <td rowspan="2">执行情况</td>
			    <td>等待注入</td>
			    <td>正在注入</td>
			    <td>注入完成</td>
			    <td>等待注入</td>
			    <td>正在注入</td>
			    <td>注入完成</td>
			    <td>等待注入</td>
			    <td>正在注入</td>
			    <td>注入完成</td>
			  </tr>
			  <tr>
			    <td class="add_wait">&nbsp;</td>
			    <td class="add_doing">&nbsp;</td>
			    <td class="add_done">&nbsp;</td>
			    <td class="modify_wait">&nbsp;</td>
			    <td class="modify_doing">&nbsp;</td>
			    <td class="modify_done">&nbsp;</td>
			    <td class="delete_wait">&nbsp;</td>
			    <td class="delete_doing">&nbsp;</td>
			    <td class="delete_done">&nbsp;</td>
			  </tr>
			</tbody>	
		</table>
	</div>
	<div class="content_table"> 
	 <div class="header">片源</div>
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<th><b></b></th>
				<th colspan="5"><b>添加</b></th>
				<th colspan="3"><b>删除</b></th>
			</tr>
		</thead>
		<tbody>			
			  <tr>
				    <td class="inject">中心同步指令数量</td>
				    <td colspan="5" class="add_total" style="text-align:center;">&nbsp;</td> 
				    <td colspan="3" class="delete_total" style="text-align:center;">&nbsp;</td>
				 </tr>
						
			  <tr>
			    <td rowspan="2">执行情况</td>
			    <td>等待切片</td>
			    <td>正在切片</td>
			    <td>等待注入</td>
			    <td>正在注入</td>
			    <td>注入完成(条数|时长<!--|大小-->)</td>
			    
			    <td>等待注入</td>
			    <td>正在注入</td>
			    <td>注入完成(条数|时长<!--|大小-->)</td>
			  </tr>
			  <tr>
			  	<td class="add_clip">&nbsp;</td>
			  	<td class="add_doing_clip">&nbsp;</td>
			    <td class="add_wait">&nbsp;</td>
			    <td class="add_doing">&nbsp;</td>
			    <td><span  class="add_done"></span><span class="add_done_len"></span><!-- <span class="add_done_size"></span> --></td>

			    <td class="delete_wait">&nbsp;</td>
			    <td class="delete_doing">&nbsp;</td>
			    <td><span  class="delete_done"></span><span class="delete_done_len"></span><!-- <span class="delete_done_size"></span> --></td>
			  </tr>
			</tbody>	
		</table>
	</div>
</div>
</body>
</html>
