	var st_status_ok={};
	
	st_status_ok.chart=null;
	
	$(function(){
		
//		var data = [
//			         	{
//			         		name : '注入完成',
//			         		value:[54841,72400,76776,83361],
//			         		color:'#32bdbc'
//			         	},
//			         	{
//			         		name : '未注入',
//			         		value:[22790,33284,52148,68333],
//			         		color:'#d75a5e'
//			         	}
//			         ];



//			         st_status_ok.done(data);
			        st_status_ok.call();
			        $("#btn1").addClass('active');
			        $("#btn1").click(function(){
			        	$('.btn-primary').removeClass('active');
			        	st_status_ok.call();	
			        });
			        
			        $("#btn2").click(function(){
			       		 $('.btn-primary').removeClass('active');
			        	st_status_ok.call(st_status_ok.buildDateStr(0),st_status_ok.buildDateStr(1));	
			        });
			        
			        $("#btn3").click(function(){
			        	$('.btn-primary').removeClass('active');
			        	st_status_ok.call(st_status_ok.buildDateStr(-1),st_status_ok.buildDateStr(0));	
			        });
			         $("#btn4").click(function(){
			        	$('.btn-primary').removeClass('active');
			        	st_status_ok.call(st_status_ok.buildDateStr(-3),st_status_ok.buildDateStr(1));		
			        });
			        
			         $("#btn5").click(function(){
			         	$('.btn-primary').removeClass('active');
			        	st_status_ok.call(st_status_ok.buildDateStr(-7),st_status_ok.buildDateStr(1));	
			        });
			        
			        $("#btn6").click(function(){
			        	var $begin=$("#begin_date").val();
			        	var $end=$("#end_date").val();
			        	st_status_ok.call($begin+' 00:00:00',$end+' 23:59:59');	
			        });
	});
	
	
	st_status_ok.buildDateStr=function(num){
		return Starcor.S67.$S.ftime(window.nowtime+num*24*3600,'YYYY-MM-dd HH:mm:ss');
	}
	
	
	st_status_ok.call=function($begin,$end){
		var params={};
		if ($begin){
			params.begin=$begin;
		}
		
		if ($end){
			params.end=$end;
		}
		
		params.sp=window.sp_id;
		params.time=window.timestmp;
		
		params.action='st_status_ok';
		
		$('.alert').show();
		
		$.ajax("../api/api.php",{
			success:function(data){
				if (data.state==0){
					st_status_ok.done(data.data);
				}
				$('.alert').hide();
			},
			dataType:'json',
			type:'post',
			data:params
		});	
		
	}
	
	st_status_ok.done=function(data){
		
		if (st_status_ok.chart) st_status_ok.chart=null;
		
		var total=data.video.total>data.index.total?data.video.total:data.index.total;
		
		total=total>data.media.total?total:data.media.total;
		
		total=total>data.clip.total?total:data.clip.total;
		
		total=Math.round(total/100)*100;
		
		var clip=Math.round(total/8);
		
		var ex_data = [
			         	{
			         		name : '注入完成',
			         		value:[
			         		data.clip.done,
			         		data.media.done,
			         		data.index.done,
			         		data.video.done,
			         		],
			         		color:'#32bdbc'
			         	},
			         	{
			         		name : '未注入',
			         		value:[
			         		data.clip.undone,
			         		data.media.undone,
			         		data.index.undone,
			         		data.video.undone,
			         		],
			         		color:'#d75a5e'
			         	}
			         ];
		
		
		st_status_ok.chart = new iChart.ColumnStacked2D({
					render : 'canvasDiv',
					data: ex_data,
					labels:["切片","片源","分集","主媒资"],
					title : {
						text:'注入任务统计',
						color:'#000',
						textAlign:'left',
						padding:'0 40',
						font:'微软雅黑',
						/*border:{
							enable:true,
							width:[0,0,4,0],
							color:'#698389'
						},*/
						height:40
					},
					/*footnote : {
						text:'数据来源：中华人民共和国国家统计局',
						font:'微软雅黑',
						padding:'0 8',
						color:'#dcd6cb'
					},*/
					padding:'8 0',
					width : 800,
					height : 400,
					column_width:70,
					//gradient : true,//应用背景渐变
					//gradient_mode:'LinearGradientDownUp',//渐变类型
					//color_factor : 0.1,//渐变因子
					background_color : '#fff',
					sub_option:{
						label:{color:'#666',fontsize:12,fontweight:600},
						border : false,
						listeners:{
							parseText:function(r,t){
								return t;
							},
							click:function(re,e,param){
//								alert(re.options.id);
							}
						}
					},
					label:{color:'#666',font:'微软雅黑',fontsize:12,fontweight:600},
					legend:{
						enable:true,
						background_color : null,
						line_height:25,
						color:'#666',
						fontsize:12,
						font:'微软雅黑',
						fontweight:600,
						border : {
							enable : false
						}
					},
					column_width:80,
					coordinate:{
						background_color : 0,
						grid_color:'#999',
						axis : {
							color : '#999',
							width : 0
						}, 
						scale:[{
							 position:'left',	
							 scale_enable : false,
							 start_scale:0,
							 scale_space:clip,
							 end_scale:total,
							 label:{color:'#666',fontsize:11,fontweight:600}
						}],
						width:'80%',
						height:'76%'
					},tip:{enable:true}
			});

		
			
			st_status_ok.chart.draw();
	}
	
