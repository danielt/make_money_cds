$(document).ready(function(){
		$("#status").hide();
		$("#select_btn").click(function(){
			doselect();
			return false;
		});
	});
	
	
	function doselect(){
		$checkbox=$("#view tbody input[type=checkbox]");
		var len=$checkbox.length;
		var data=[];
		for (var i=0;i<len;i++){
			if ($checkbox.eq(i).attr("checked")=="checked"){
				var obj={};
				var dom=$("#view tbody tr").eq(i);
				obj.name=dom.find("td").eq(2).html();
				obj.id=$checkbox.eq(i).val();
				obj.status=dom.find("td").eq(3).html();
				obj.type=dom.find("td").eq(4).html();
				obj.indexes=dom.find("td").eq(5).html();
				obj.time=dom.find("td").eq(6).html();
				data.push(obj);
			}
		}
		
		var weight=$("#weight").val();
		set_weight(data,weight);
	}
	
	function set_weight(data,weight){
		var params={};
		params.action='st_set_weight';
		params.data=data;
		params.weight=weight;
		params.sp_id=window.sp_id;
	
		$.ajax("../api/api.php",{
			success:function(data){
				if (data.state==0){
				alert("设置完毕");
					set_status(data);
					
				}
				$('.alert').hide();
			},
			dataType:'json',
			type:'post',
			data:params
		});	
	}
	
	
	function set_status(data){
		var len=data.data.length;
		var html='';
		for (var i=0;i<len;i++){
			var obj=data.data[i];
			html +="<p>["+obj.id+"]&nbsp;"+obj.name;
			var reason='';
			switch(obj.state){
				case 0:
					reason='<span class="green">设置权重成功</span>';
				break;
				case 1:
					reason='<span class="red">设置权重失败</span>';
				break;
				case -1:
					reason='<span class="glay">队列内未找到相关影片</span>';
				break;
			}
			
			html+="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+reason+"</p>";
		}
		
		$("#status").html(html);
		$("#status").show();
		window.top.resetFrameHeight();
	}