var st_status_ok = {};
$(function() {
	st_status_ok.call(st_status_ok.buildDateStr(0), st_status_ok.buildDateStr(1));
	$("#btn2").addClass('active');

	$("#btn2").click(function() {
		$('.btn-primary').removeClass('active');
		st_status_ok.call(st_status_ok.buildDateStr(0), st_status_ok.buildDateStr(1));
	});

	$("#btn3").click(function() {
		$('.btn-primary').removeClass('active');
		st_status_ok.call(st_status_ok.buildDateStr(-1), st_status_ok.buildDateStr(0));
	});
	$("#btn4").click(function() {
		$('.btn-primary').removeClass('active');
		st_status_ok.call(st_status_ok.buildDateStr(-3), st_status_ok.buildDateStr(1));
	});

	$("#btn5").click(function() {
		$('.btn-primary').removeClass('active');
		st_status_ok.call(st_status_ok.buildDateStr(-7), st_status_ok.buildDateStr(1));
	});
	
	
	$("#btn6").click(function() {
		$('.btn-primary').removeClass('active');
		st_status_ok.call('','');
	});
	
	
	$("#btn7").click(function() {
		var $begin = $("#begin_date").val();
		var $end = $("#end_date").val();
		st_status_ok.call($begin + ' 00:00:00', $end + ' 23:59:59');
	});

});
st_status_ok.buildDateStr = function(num) {
	return Starcor.S67.$S.ftime(window.nowtime + num * 24 * 3600, 'YYYY-MM-dd HH:mm:ss');
}

st_status_ok.call = function($begin, $end) {
	var params = {};
	if ($begin) {
		params.begin = $begin;
	}

	if ($end) {
		params.end = $end;
	}
	params.sp = window.sp_id;
	params.time = window.timestmp;

	params.action = 'st_task_import_state';
	$('.alert').show(); 
	
	$.ajax("../api/api.php", {
		success : function(data) {   
			if (data.state == 0) { 
				st_status_ok.done(data.data);
			}
			$('.alert').hide();
			
			var num = 'EPG影片总数：'+data.num;
			$("#num").html(num);
			//var json_data = JSON.stringify(data.data); 
			//掉生成图表接口
			//alert(json_data); 
			$("#canvasDiv").html('');
			var _temp_arr=[];
			//json转化数组
			for(var val in data.data )
			{   
				var _temp_item={};
				_temp_item.title=val;
				_temp_item.value=data.data[val];
				_temp_arr.push(_temp_item);
			}
			get_map(_temp_arr); 
		},
		dataType : 'json',
		type : 'post',
		data : params
	});
}

st_status_ok.done = function(data) 
{ 
	for (var val in data )
	{ 
		$("."+val).html("<font style='color:green;font-weight:600;' id='"+val+"'>"+data[val]+"</font>");
		//$("."+val).addClass('html_color');
	} 
}
function get_map(data)
{ 
	//var json_data = JSON.stringify(data);
	//alert(json_data); 
	var _new_data=[];
	var arry_point_item = []//鼠标指上去显示的标题
	var array_x_title = []//x轴实际显示的标题
	var _temp_time_title = ""; 
	for(var i=4;i<data.length;i++)
	{   //alert(data[i].title);
		arry_point_item.push(data[i].title);
		 
		if (data.length > 24) 
		{
            if (!(i % 4)) 
            { 
                array_x_title.push(data[i].title);
                //主媒资cdn注入添加
                if(data[i].title == 'cdn_video_add_ok')
                {
                	_temp_time_title='主cdn加';
                }
                //主媒资cdn注入修改
                if(data[i].title == 'cdn_video_modify_ok')
                {
                	_temp_time_title='主cdn改';
                }
                //主媒资cdn注入删除
                if(data[i].title == 'cdn_video_destroy_ok')
                {
                	_temp_time_title='主cdn删';
                } 
                //主媒资epg注入添加
                if(data[i].title == 'epg_video_add_ok')
                {
                	_temp_time_title='主epg加';
                } 
                //主媒资epg注入修改
                if(data[i].title == 'epg_video_modify_ok')
                {
                	_temp_time_title='主epg改';
                }
                //主媒资epg注入删除
                if(data[i].title == 'epg_video_destroy_ok')
                {
                	_temp_time_title='主epg删';
                }
                //分集cdn注入添加
                if(data[i].title == 'cdn_index_add_ok')
                {
                	_temp_time_title='分cdn加';
                }
                //分集cdn注入添加
                if(data[i].title == 'cdn_index_modify_ok')
                {
                	_temp_time_title='分cdn改';
                }
                //分集cdn注入删除
                if(data[i].title == 'cdn_index_destroy_ok')
                {
                	_temp_time_title='分cdn删';
                }
                //分集epg注入添加
                if(data[i].title == 'epg_index_add_ok')
                {
                	_temp_time_title='分epg加';
                }
                //分集epg注入修改
                if(data[i].title == 'epg_index_modify_ok')
                {
                	_temp_time_title='分epg改';
                }
                //分集epg注入删除
                if(data[i].title == 'epg_index_destroy_ok')
                {
                	_temp_time_title='分epg删';
                }
                //片源cdn注入添加
                if(data[i].title == 'cdn_media_add_ok')
                {
                	_temp_time_title='片cdn加';
                }
                //片源cdn注入修改
                if(data[i].title == 'cdn_media_modify_ok')
                {
                	_temp_time_title='片cdn改';
                }
                //片源cdn注入删除
                if(data[i].title == 'cdn_media_destroy_ok')
                {
                	_temp_time_title='片cdn删除';
                }
                //片源epg注入添加
                if(data[i].title == 'epg_media_add_ok')
                {
                	_temp_time_title='片epg加';
                } 
                //片源epg注入修改
                if(data[i].title == 'epg_media_modify_ok')
                {
                	_temp_time_title='片epg改';
                }  
                //片源epg注入添加
                if(data[i].title == 'epg_media_destroy_ok')
                {
                	_temp_time_title='片epg删除';
                }  
            }
            else
            {
            	_temp_time_title="";
            }
	    } 
		else 
	    {
	        array_x_title.push(data[i].title);
	        _temp_time_title=data[i].title;
	    } 
		var item={};
		item.name=_temp_time_title;
		item.value=data[i].value;
		if(data[i].title == 'epg_video_add_ok' ||
				data[i].title == 'epg_index_add_ok' ||
				data[i].title == 'epg_media_add_ok' ||
				data[i].title == 'cdn_video_add_ok' ||
				data[i].title == 'cdn_index_add_ok' ||
				data[i].title == 'cdn_media_add_ok' ||
				data[i].title == 'epg_video_modify_ok' ||
				data[i].title == 'epg_index_modify_ok' ||
				data[i].title == 'epg_media_modify_ok' ||
				data[i].title == 'cdn_video_modify_ok' ||
				data[i].title == 'cdn_index_modify_ok' ||
				data[i].title == 'cdn_media_modify_ok' ||
				data[i].title == 'epg_video_destroy_ok' ||
				data[i].title == 'epg_index_destroy_ok' ||
				data[i].title == 'epg_media_destroy_ok' ||
				data[i].title == 'cdn_video_destroy_ok' ||
				data[i].title == 'cdn_index_destroy_ok' ||
				data[i].title == 'cdn_media_destroy_ok')
		{
			item.color="#246ec5";
		}
		else if(data[i].title == 'epg_video_add_do' ||
				data[i].title == 'epg_index_add_do' ||
				data[i].title == 'epg_media_add_do' ||
				data[i].title == 'cdn_video_add_do' ||
				data[i].title == 'cdn_index_add_do' ||
				data[i].title == 'cdn_media_add_do' ||
				data[i].title == 'epg_video_modify_do' ||
				data[i].title == 'epg_index_modify_do' ||
				data[i].title == 'epg_media_modify_do' ||
				data[i].title == 'cdn_video_modify_do' ||
				data[i].title == 'cdn_index_modify_do' ||
				data[i].title == 'cdn_media_modify_do' ||
				data[i].title == 'epg_video_destroy_do' ||
				data[i].title == 'epg_index_destroy_do' ||
				data[i].title == 'epg_media_destroy_do' ||
				data[i].title == 'cdn_video_destroy_do' ||
				data[i].title == 'cdn_index_destroy_do' ||
				data[i].title == 'cdn_media_destroy_do')
		{
			item.color="#FFFF00";
		}
		else if(data[i].title == 'epg_video_add_fail' ||
				data[i].title == 'epg_index_add_fail' ||
				data[i].title == 'epg_media_add_fail' ||
				data[i].title == 'cdn_video_add_fail' ||
				data[i].title == 'cdn_index_add_fail' ||
				data[i].title == 'cdn_media_add_fail' ||
				data[i].title == 'epg_video_modify_fail' ||
				data[i].title == 'epg_index_modify_fail' ||
				data[i].title == 'epg_media_modify_fail' ||
				data[i].title == 'cdn_video_modify_fail' ||
				data[i].title == 'cdn_index_modify_fail' ||
				data[i].title == 'cdn_media_modify_fail' ||
				data[i].title == 'epg_video_destroy_fail' ||
				data[i].title == 'epg_index_destroy_fail' ||
				data[i].title == 'epg_media_destroy_fail' ||
				data[i].title == 'cdn_video_destroy_fail' ||
				data[i].title == 'cdn_index_destroy_fail' ||
				data[i].title == 'cdn_media_destroy_fail')
		{
			item.color="#FF0000";
		}
		else
		{
			item.color="#DCDCDC";
		}
		 
		_new_data.push(item);
	} 
	//alert(array_x_title.length);
	//var json_data = JSON.stringify(_new_data); 
	//alert(json_data); 
	var foot_title = '';
	var min_title = '每个模块颜色依次表示为：蓝色表示成功，红色表示失败，灰色表示等待，黄色表示执行。';
 show_bar_1('canvasDiv', "统计情况", min_title, arry_point_item,foot_title, array_x_title, _new_data, '最大量','单位(部)','部',"注入统计"); 
}