$(document).ready(function() {
	set_content_width();
	$(window).resize(function() {
         set_content_width();
    });
        $(".selectbox").hide();
        $("#nns_carrier").change(function() {
             var index = $("#nns_carrier").val();
        });
        $(".split_btn").click(function() {
             if ($(".category_tree").is(":hidden")) {
                  $(".category_tree").show();
             } else {
                  $(".category_tree").hide();
             }
             set_content_width();
        });
        $(document).keydown(function(event) {
             if (event.which == 13) {
                 search_vod();
             }
        });
});
function set_content_width() {
        if ($(".category_tree").is(":hidden")) {
             $(".category_editbox").width($(".content").width() - 25);
        } else {
             $(".category_editbox").width($(".content").width() - $(".category_tree").width() - 50);
        }
             var len2c = $(".rightstyle2c").text().length;
        $(".rightstyle2c").width(len2c * 8);
}
function on_set(id){
     $("#cp_id").val(id);
}
function import_default_data_end(){
	end = 1;
}
function select_tree_item(item_id, item_name, parent_id) {
	$('.alert').show();
    $(".category_editbox").find(".category_edit_id").html(item_id);
    $(".category_editbox").find(".category_edit_name").html(item_name);                  
    $("#search_category_id").val(item_id);
    params = {
				"action" : "st_tast_category",
				"category_id" : item_id,
            }
	$.ajax("../api/api.php",{
		success:function(data){
			if(data.state == 0) {
				$('#assist_num').html(data.data.num);
				$('#index_num').html(data.data.dex);
				$('#media_file').html(data.data.size);
				$('#media_count').html(data.data.media);
				$('.alert').hide();
    		}
			
		},
		dataType:'json',
		type:'post',
		data:params
	});	
	 
}