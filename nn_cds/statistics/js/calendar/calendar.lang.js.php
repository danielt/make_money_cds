<?php
session_start();
$nn_cms_manager_dir = dirname(dirname(dirname(dirname(__FILE__)))).'/nn_cms_manager';
include_once($nn_cms_manager_dir."/nncms_manager_inc.php");
include $nncms_config_path. "nn_cms_config/nn_cms_global.php";
include_once($nn_cms_manager_dir.'/nncms_mlang.php');
echo 'var js_lang_calendar_today="'.cms_get_lang('calendar_today').'";';
echo 'var js_lang_calendar_week="'.cms_get_lang('calendar_week').'";';
echo 'var js_lang_calendar_monday="'.cms_get_lang('calendar_monday').'";';
echo 'var js_lang_calendar_tuesday="'.cms_get_lang('calendar_tuesday').'";';
echo 'var js_lang_calendar_wednesday="'.cms_get_lang('calendar_wednesday').'";';
echo 'var js_lang_calendar_thursday="'.cms_get_lang('calendar_thursday').'";';
echo 'var js_lang_calendar_friday="'.cms_get_lang('calendar_friday').'";';
echo 'var js_lang_calendar_saturday="'.cms_get_lang('calendar_saturday').'";';
echo 'var js_lang_calendar_sunday="'.cms_get_lang('calendar_sunday').'";';

echo 'var js_lang_calendar_january="'.cms_get_lang('calendar_january').'";';
echo 'var js_lang_calendar_february="'.cms_get_lang('calendar_february').'";';
echo 'var js_lang_calendar_march="'.cms_get_lang('calendar_march').'";';
echo 'var js_lang_calendar_april="'.cms_get_lang('calendar_april').'";';
echo 'var js_lang_calendar_may="'.cms_get_lang('calendar_may').'";';
echo 'var js_lang_calendar_june="'.cms_get_lang('calendar_june').'";';
echo 'var js_lang_calendar_july="'.cms_get_lang('calendar_july').'";';
echo 'var js_lang_calendar_august="'.cms_get_lang('calendar_august').'";';
echo 'var js_lang_calendar_september="'.cms_get_lang('calendar_september').'";';
echo 'var js_lang_calendar_october="'.cms_get_lang('calendar_october').'";';
echo 'var js_lang_calendar_november="'.cms_get_lang('calendar_november').'";';
echo 'var js_lang_calendar_december="'.cms_get_lang('calendar_december').'";';
?>

Calendar.LANG("cn", "中文", {

        fdow: 1,                // first day of week for this locale; 0 = Sunday, 1 = Monday, etc.

        goToday: js_lang_calendar_today,

        today: js_lang_calendar_today,         // appears in bottom bar

        wk: js_lang_calendar_week,

        weekend: "0,6",         // 0 = Sunday, 1 = Monday, etc.

        AM: "AM",

        PM: "PM",

        mn : [ js_lang_calendar_january,
               js_lang_calendar_february,
               js_lang_calendar_march,
               js_lang_calendar_april,
               js_lang_calendar_may,
               js_lang_calendar_june,
               js_lang_calendar_july,
               js_lang_calendar_august,
               js_lang_calendar_september,
               js_lang_calendar_october,
               js_lang_calendar_november,
               js_lang_calendar_december],

      smn : [  js_lang_calendar_january,
               js_lang_calendar_february,
               js_lang_calendar_march,
               js_lang_calendar_april,
               js_lang_calendar_may,
               js_lang_calendar_june,
               js_lang_calendar_july,
               js_lang_calendar_august,
               js_lang_calendar_september,
               js_lang_calendar_october,
               js_lang_calendar_november,
               js_lang_calendar_december],

        dn : [ js_lang_calendar_sunday,
               js_lang_calendar_monday,
               js_lang_calendar_tuesday,
               js_lang_calendar_wednesday,
               js_lang_calendar_thursday,
               js_lang_calendar_friday,
               js_lang_calendar_saturday,
               js_lang_calendar_sunday ],

        sdn : [ js_lang_calendar_sunday,
               js_lang_calendar_monday,
               js_lang_calendar_tuesday,
               js_lang_calendar_wednesday,
               js_lang_calendar_thursday,
               js_lang_calendar_friday,
               js_lang_calendar_saturday,
               js_lang_calendar_sunday ]

});

