<?php
/*
 * Created on 2014-1-24
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 define ('BK_OP_WAIT',0);//等待执行
 define ('BK_OP_PROGRESS',1);//执行中
 define ('BK_OP_WAIT_CLIP',2);//等待切片
 define ('BK_OP_CLIP',3);//切片中
 define ('BK_OP_OK',5);//完成
 define ('BK_OP_CANCEL',4);//取消
 define ('BK_OP_UNAUDIT',6);//未审核
 define ('BK_OP_WAIT_SP',7);//等待前置SP执行
 define ('BK_OP_WAIT_SP_ENCODE_BEGIN',9);//等待转码
 define ('BK_OP_WAIT_SP_ENCODE',8);//正在转码
 define ('BK_OP_FAIL_SP_ENCODE',10);//转码失败
 define ('BK_OP_IMPORT_SP_ENCODE_FAIL',11);//原始数据注入转码失败
 
 define ('BK_OP_MEDIA','media');
 define ('BK_OP_INDEX','index');
 define ('BK_OP_VIDEO','video');
 define ('BK_OP_LIVE_MEDIA','live_media');
 define ('BK_OP_LIVE_INDEX','live_index');
 define ('BK_OP_LIVE','live');
 define ('BK_OP_PLAYBILL','playbill');
 define ('BK_OP_FILE','file');
 define ('BK_OP_PRODUCT','product');
 define ('BK_OP_SEEKPOINT','seekpoint');

 define ('BK_OP_ADD','add');
 define ('BK_OP_MODIFY','modify');
 define ('BK_OP_DELETE','destroy');
 
 
 define ('BK_OP_ONLINE','online');
 define ('BK_OP_UNLINE','unline');
 
 
 define ('BK_OP_IMPORT_TASK_DELAY',30);
 define ('BK_OP_CLIP_TASK_DELAY',30);
 
 define ('BK_OP_RE_CLIP_WEIGHT',99);
 
?>
