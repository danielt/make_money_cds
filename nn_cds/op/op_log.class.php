<?php
include_once dirname(dirname(__FILE__)).'/nn_logic/nl_common.func.php';
include_once 'op_queue.const.php';

class op_log{
	/*
	 * 获取op 日志
	 * $params 条件查询数组array()
	 */
	public function search_op_log_list($dc, $params, $since, $num) {
		$where = null;
		if(is_array($params) && !empty($params)) {
			$where = $this->search_op_log_where($params);
		}
		$sql = 'select * from nns_mgtvbk_op_log' . $where . ' order by nns_create_time desc limit ' . $since . ',' . $num;
		//echo $sql;
		$result = nl_query_by_db($sql, $dc->db());
		if ($result === false) {
			return false;
		}
		if (is_array($result)) {
			//统计条数
			$sql_nums = 'select count(1) as num from nns_mgtvbk_op_log' . $where;

			$nums = nl_query_by_db($sql_nums, $dc->db());
			$arr = array(
				 'count' => $nums[0]['num'],
				 'item' => $result
			);
			return $arr;
		}
		if ($result === true) {
			return true;
		}
	}
	/*
	 * 条件查询拼装
	 * 查询条件$params array()
	 */
	public function search_op_log_where($params) {
		$where = null;
		$and = ' and ';
		foreach($params as $key=>$val) {
			if($key == 'name' && $val != null) {
				$where .= " {$and} nns_name like '%{$val}%'";
			} elseif($key == 'status' && $val != null){
				switch($val){
					case 'bk_op_wait':
						$where .= " {$and} nns_{$key}=".BK_OP_WAIT;
						break;
					case 'bk_op_progress':
						$where .= " {$and} nns_{$key}=".BK_OP_PROGRESS;
						break;
					case 'bk_op_wait_clip':
						$where .= " {$and} nns_{$key}=".BK_OP_WAIT_CLIP;
						break;
					case 'bk_op_clip':
						$where .= " {$and} nns_{$key}=".BK_OP_CLIP;
						break;
					case 'bk_op_ok' :
						$where .= " {$and} nns_{$key}=".BK_OP_OK;
						break;
					case 'bk_op_cancel':
						$where .= " {$and} nns_{$key}=".BK_OP_CANCEL;
						break;
				}
			} elseif($key == 'begin_time' && $val != null) {
				$where .= "{$and} nns_create_time>'{$val}'";
			} elseif($key == 'end_time' && $val != null) {
				$where .= "{$and} nns_create_time<'{$val}'";
			} elseif($key == 'org_id' && $val !=null) {
				 $str = " where nns_{$key}='{$val}'";
			} elseif($val != null) {
				$where .= " {$and} nns_{$key}='{$val}' ";
			}
		}
		$where = $str . $where;
		return $where;
	}
}