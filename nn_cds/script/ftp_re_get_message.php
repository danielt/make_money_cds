<?php 
header("Content-type:text/html;charset=utf-8");
include '../nn_logic/nl_common.func.php';
include '../nn_logic/message/nl_message.class.php';
include dirname(dirname(dirname(__FILE__))).'/np/np_xml2array.class.php';
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$sql="select * from nns_mgtvbk_message where nns_cp_id like 'fuse_%' and nns_message_xml !='' and nns_message_content=''";
$result = nl_query_by_db($sql,$dc->db());
if(!is_array($result) || empty($result))
{
    echo "empty";die;
}
function trim_xml_header($xml,$encode=null)
{
    if(strlen($xml) < 1)
    {
        return '';
    }
    if(strlen($encode) > 0 )
    {
        $xml = mb_convert_encoding($xml,'UTF-8', $encode);
    }
    $xml = preg_replace('/\<\?\s*xml\s+version\s*\=\s*(\"|\').*(\"|\')\s+encoding\s*\=\s*(\"|\')[a-z0-9-]+(\"|\')\s*\?\>/i', '', $xml);
    $xml = preg_replace('/\<\?\s*xml\s+encoding\s*\=\s*(\"|\')[a-z0-9-]+(\"|\')\s*\?\>/i', '', $xml);
    $xml = preg_replace('/\<\?\s*xml\s+version\s*\=\s*(\"|\').*(\"|\')\s*\?\>/i', '', $xml);
    //去除XML中的单引号
    // 		$xml = str_replace(array("\'"), "’", $xml);
    return $xml;
}
function return_data($ret, $reason, $data = null)
{
    return array (
        'ret' => $ret,
        'reason' => $reason,
        'data_info' => ($data !== null) ? $data : null
    );
}
function public_get_curl_content($file_path , $time = 30, $file_save_path = null)
{
    //curl抓取 远程文件的内容
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $file_path);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $time);
    $content = curl_exec($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    //如果内容为空  或者根本没这个xml文件
    if (empty($content) || !$content || $http_code >= 400)
    {
        return return_data(2, "消息：curl下载文件失败地址:" . $file_path . '超时时间:' . $time);
    }
    return return_data(0, 'CURL获取成功', $content);
}
foreach ($result as $val)
{
    $result = public_get_curl_content($val['nns_message_xml'], 30, true);
    if ($result['ret'] != 0)
	{
	    continue;
	}
    $message_content = trim_xml_header($result['data_info']);
    $dom = new DOMDocument('1.0', 'utf-8');
    $dom->loadXML($message_content);
    $xml = $dom->saveXML();
    $xml_arr = np_xml2array::getArrayData($xml);
    $Objects = null;
    if(isset($xml_arr['data']['children']) && is_array($xml_arr['data']['children']) && !empty($xml_arr['data']['children']))
    {
        foreach ($xml_arr['data']['children'] as $v)
        {
            if (empty($Objects) && $v['name'] == 'Objects')
            {
                $Objects = $v['children'];
            }
        }
    }
    $str_type='';
    $str_action='';
    $message_data_temp = array();
    if (!empty($Objects) && is_array($Objects))
    {
        foreach ($Objects as $obj)
        {
            $str_action = (isset($obj['attributes']['Action']) && strtoupper($obj['attributes']['Action']) == 'DELETE') ? 3 : 1;
            if ($obj['attributes']['ElementType'] == 'Series')
            {
                $str_type=1;
            }
            else if ($obj['attributes']['ElementType'] == 'Program')
            {
                $str_type=2;
            }
            else if ($obj['attributes']['ElementType'] == 'Movie')
            {
                $str_type=3;
            }
            //直播频道
            else if ($obj['attributes']['ElementType'] == 'Channel')
            {
                $str_type=4;
            }
            //直播片源
            else if ($obj['attributes']['ElementType'] == 'PhysicalChannel')
            {
                $str_type=6;
            }
            //节目单
            else if ($obj['attributes']['ElementType'] == 'ScheduleRecord')
            {
                $str_type=7;
            }
        }
    }
    $message_data_temp['nns_message_content'] = mysql_escape_string($result['data_info']);
    $message_data_temp['nns_type'] = $str_type;
    $message_data_temp['nns_action'] = $str_action;
    $message_data_temp['nns_name'] = '';
    $message_data_temp['nns_modify_time'] = date("Y-m-d H:i:s");
    $message_data_temp['nns_message_state'] = 0;
    $result = nl_message::update_message_state($dc, $val['nns_id'], $message_data_temp);
    var_dump($result);
}