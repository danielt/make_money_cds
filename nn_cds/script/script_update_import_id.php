<?php
/**
 * 贵州广电修复 播控按电影类型自动生成主媒资的注入ID与原始ID，生成规则为001000+分集ID(20位)+000000
 */
header('content-type:text/html;charset=utf-8');
set_time_limit(0);    //设置脚本的最大执行时间，  这里不限制时间
error_reporting(E_ALL ^ E_NOTICE);
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nn_logic/nl_common.func.php';
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nn_cms_config/nn_cms_global.php';

define("RUN_SQL_NUM", 200);//一次性运行的SQL数
define("STOP_RUN_NUM",0);//运行最大条数

class script_update_import_id
{
	public $dc = null;
	public $log_module = 'script_update_import_id';
	private $video_table = "nns_vod";//主媒资表
	private $index_table = "nns_vod_index";//分集表
	private $media_table = "nns_vod_media";//片源表
	private $rule_info = "001000";
	private $notuse_video = array();
		
	public function __construct()
	{
		$this->dc = nl_get_dc(array(
				'db_policy' => NL_DB_WRITE,
            	'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
		));
	}
	/**
	 * 脚本执行
	 */
	public function run()
	{
		$num = 0;
		//获取带有规则的原始ID
		while ($result = $this->get_rule_vod_info($num))
		{
			foreach ($result as $import)
			{
				$this->nn_echo_log('开始--影片名：' . $import['nns_name'] . '--注入ID：' . $import['nns_asset_import_id']);
				//过滤 非001000开头的数据
				$six_str = substr($import['nns_asset_import_id'], 0,6);
				if($six_str != $this->rule_info)
				{
					//收集过滤的数据
					$this->notuse_video[] = $import['nns_asset_import_id'];
					$this->nn_echo_log('结束');
					continue;
				}
				$original_id = substr($import['nns_asset_import_id'], 6,20);
				//首先确认此ID是否存在 array 存在 /false 不存在
				$check_import = $this->check_is_exist($original_id);
				if(!$check_import)
				{
					//不存在，直接修复
					$this->update_data($import['nns_id'], $original_id);
					continue;
				}
				//$this->notuse_video[] = $import['nns_asset_import_id'];
				//存在原始ID相同的媒资，删掉001000
				$this->delete_asset($import['nns_id']);
				continue;
			}
			$num += count($result);
			$this->nn_echo_log("已执行条数:".$num);
		}
	}
	/**
	 * 删除媒资
	 */
	private function delete_asset($id)
	{
		//删除主媒资
		$del_vod_sql = "delete from " . $this->video_table . " where nns_id='{$id}'";
		nl_execute_by_db($del_vod_sql, $this->dc->db());
		//删除分集
		$del_index_sql = "delete from " . $this->index_table . " where nns_vod_id='{$id}'";
		nl_execute_by_db($del_index_sql, $this->dc->db());
		//删除片源
		$del_meida_sql = "delete from " . $this->media_table . " where nns_vod_id='{$id}'";
		nl_execute_by_db($del_meida_sql, $this->dc->db());
	}
	/**
	 * 修改数据
	 */
	private function update_data($id,$import_id,$cp_id='',$real_cp='')
	{
		$sql = "update " . $this->video_table . " set nns_asset_import_id='{$import_id}'";
		if(!empty($cp_id))
		{
			$sql .= ",nns_cp_id='{$cp_id}'";
		}
		if(!empty($real_cp))
		{
			$sql .= ",nns_producer='{$real_cp}'";
		}
		$sql .= " where nns_id='{$id}'";
		nl_execute_by_db($sql, $this->dc->db());
	}
	/**
	 * 验证媒资是否存在
	 */
	private function check_is_exist($import_id)
	{
		$check_sql = "select nns_id,nns_cp_id,nns_producer from " . $this->video_table . " where nns_asset_import_id='{$import_id}' and nns_cp_id='gzgd'";
		$check_re = nl_query_by_db($check_sql, $this->dc->db());
		if(is_array($check_re))
		{
			return $check_re;
		}
		return false;
	}
	/**
	 * 获取带有分隔符的标签信息
	 */
	private function get_rule_vod_info($num)
	{
		if(STOP_RUN_NUM !== 0)
		{
			if($num >= STOP_RUN_NUM)
			{
				return false;
			}
		}
		if((STOP_RUN_NUM === 0) || (STOP_RUN_NUM - $num > RUN_SQL_NUM))
		{
			$get_num = RUN_SQL_NUM;
		}
		else
		{
			$get_num = STOP_RUN_NUM - $num;
		}
		$notuse_videos = '';
		$where = "";
		if(!empty($this->notuse_video))
		{
			$notuse_videos = implode("','", $this->notuse_video);
		}
		if(!empty($notuse_videos))
		{
			$where = " and nns_asset_import_id not in ('{$notuse_videos}') ";
		}
		$where_like = " where nns_asset_import_id like '" . $this->rule_info . "%'";
		$sql = "select nns_id,nns_cp_id,nns_producer,nns_name,nns_asset_import_id from " . $this->video_table . " {$where_like} {$where} limit {$get_num}";
		$this->nn_echo_log("获取规则生成的SQL为" . $sql);
		$result = nl_query_by_db($sql, $this->dc->db());
		if(!is_array($result))
		{
			$this->nn_echo_log("规则生成的主媒资获取完毕");
			return false;
		}
		return $result;
	}
	/**
	 * 记录日志
	 * @param string $msg
	 */
	public function nn_echo_log($msg)
	{
		$str_cms_dir = dirname(dirname(__FILE__)) . '/';
		$str_log_dir = $str_cms_dir . 'data/log/' . $this->log_module . '/' . date('Y-m-d') .'/';
		if (empty($msg))
		{
			return false;
		}
		if(STOP_RUN_NUM !== 0)
		{
			$str_log_dir .= STOP_RUN_NUM . '/';
		}
		$str_start_time = '['.date('Y-m-d H:i:s').']';
	
		//写入日志文件
		$str_log_file = $str_log_dir . date('H') . '.txt';
		if (!is_dir($str_log_dir))
		{
			mkdir($str_log_dir, 0777, true);
		}
		file_put_contents($str_log_file, $str_start_time . $msg . "\r\n", FILE_APPEND);
	}
}
$script_update_import_id = new script_update_import_id();
$script_update_import_id->run();