<?php
/**
 * 辽宁注入CDN专用
 * 背景：由于辽宁CDN无法对注入CDN失败的影片，进行相同注入ID的重复注入
 * 方案：此web界面用于更改注入ID，辽宁的注入ID为GUID
 * User: LZ
 * Date: 2018/3/14
 * Time: 11:21
 */
include_once dirname(dirname(dirname(__FILE__))) . "/nn_logic/nl_common.func.php";
header("content-type:text/html;charset=utf-8");
if(isset($_POST) && isset($_POST['id']) && !empty($_POST['id']))
{
    $dc = nl_get_dc(array(
        'db_policy' => NL_DB_WRITE | NL_DB_READ,
        'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
    ));
    $dc->open();
    $ids_arr = explode("|",$_POST['id']);
    $not_found_ids = array();
    foreach ($ids_arr as $id)
    {
        if(!empty($id))
        {
            //查询nns_vod_media/nns_mgtvbk_c2_task是否存在片源
            $query_sql = "select nns_id from nns_vod_media where nns_id = '{$id}' and nns_deleted = 0";
            $media_re = nl_query_by_db($query_sql,$dc->db());
            if(!is_array($media_re))
            {
                $not_found_ids[] = $id;
                continue;
            }
            $c2_sql = "select nns_id from nns_mgtvbk_c2_task where nns_ref_id = '{$id}'";
            $c2_re = nl_query_by_db($c2_sql,$dc->db());
            if(!is_array($c2_re))
            {
                $not_found_ids[] = $id;
                continue;
            }
            //修改片源GUID
            $new_vod_media_id = np_guid_rand('media');
            $media_ex = "update nns_vod_media set nns_id = '{$new_vod_media_id}' where nns_id = '{$id}'";
            nl_execute_by_db($media_ex,$dc->db());
            $c2_ex = "update nns_mgtvbk_c2_task set nns_ref_id = '{$new_vod_media_id}' where nns_ref_id = '{$id}'";
            nl_execute_by_db($c2_ex,$dc->db());
            $op_queue_ex = "update nns_mgtvbk_op_queue set nns_media_id = '{$new_vod_media_id}' where nns_media_id = '{$id}'";
            nl_execute_by_db($op_queue_ex,$dc->db());
            $clip_ex = "update nns_mgtvbk_clip_task set nns_video_media_id = '{$new_vod_media_id}' where nns_video_media_id = '{$id}'";
            nl_execute_by_db($clip_ex,$dc->db());
        }
    }
    if(empty($not_found_ids))
    {
        echo "<script>alert('操作成功');history.go(-1);</script>";die;
    }
    else
    {
        $not_found_str = implode("|",$not_found_ids);
        echo "<script>alert('操作失败：" . $not_found_str . "');history.go(-1);</script>";die;
    }
}
?>
<html>
    <head>
        <title>辽宁专用</title>
    </head>
    <body>
        <form action="" method="post">
        <table>
            <tr>
                <td>当前需替换的片源GUID：</td>
                <td><textarea name="id" cols="50" rows="10">多个ID以|分隔</textarea></td>
                <td><input type="submit" value="提交" /></td>
            </tr>
        </table>
        </form>
    </body>
</html>
