<?php
/**
 * 辽宁注入CDN专用
 * 背景：重新从云存储上下载视频文件并重新注入
 * 方案：此web界面用于更改片源的下载地址，及修改注入ID，并执行重新注入。
 * User: LZ
 * Date: 2018/3/14
 * Time: 11:21
 */
header("content-type:text/html;charset=utf-8");
include_once dirname(dirname(dirname(__FILE__))) . "/nn_logic/nl_common.func.php";
include_once dirname(dirname(dirname(__FILE__))) . "/mgtv_v2/mgtv_init.php";
include_once dirname(dirname(dirname(__FILE__))) . "/mgtv_v2/models/queue_task_model.php";

if(isset($_POST) && isset($_POST['id']) && !empty($_POST['id']) && !empty($_POST['url']))
{
    $dc = nl_get_dc(array(
        'db_policy' => NL_DB_WRITE | NL_DB_READ,
        'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
    ));
    $dc->open();
    $id = $_POST['id'];
    $url = $_POST['url'];
    //查询nns_vod_media/nns_mgtvbk_c2_task是否存在片源
    $query_sql = "select nns_id from nns_vod_media where nns_id = '{$id}' and nns_deleted = 0";
    $media_re = nl_query_by_db($query_sql,$dc->db());
    if(!is_array($media_re))
    {
        echo "<script>alert('操作失败：VOD_MEDIA中不存在操作的片源或已被替换');history.go(-1);</script>";die;
    }
    $c2_sql = "select * from nns_mgtvbk_c2_task where nns_ref_id = '{$id}'";
    $c2_re = nl_query_by_db($c2_sql,$dc->db());
    if(!is_array($c2_re))
    {
        echo "<script>alert('操作失败：C2中不存在操作的片源');history.go(-1);</script>";die;
    }
    //修改片源GUID及URL下载地址
    $new_vod_media_id = np_guid_rand('media');
    $media_ex = "update nns_vod_media set nns_id = '{$new_vod_media_id}',nns_url='{$url}' where nns_id = '{$id}'";
    nl_execute_by_db($media_ex,$dc->db());
    //修改C2任务
    $c2_ex = "update nns_mgtvbk_c2_task set nns_ref_id = '{$new_vod_media_id}' where nns_ref_id = '{$id}'";
    nl_execute_by_db($c2_ex,$dc->db());
    //修改中心同步指令
    $op_queue_ex = "update nns_mgtvbk_op_queue set nns_media_id = '{$new_vod_media_id}' where nns_media_id = '{$id}'";
    nl_execute_by_db($op_queue_ex,$dc->db());
    //修改切片指令中的任务
    $clip_ex = "update nns_mgtvbk_clip_task set nns_video_media_id = '{$new_vod_media_id}' where nns_video_media_id = '{$id}'";
    nl_execute_by_db($clip_ex,$dc->db());

    //执行重新注入
    $info = $c2_re[0];
    nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=7,nns_epg_status=100 where nns_id='{$info['nns_id']}'", $dc->db());
    $queue_task_model = new queue_task_model();
    $queue_task_model->q_report_task($info['nns_id']);
    $queue_task_model->q_add_task($new_vod_media_id,$info['nns_type'] , $info['nns_action'],$info['nns_org_id'],null,true,$info['nns_message_id'],null,99);

    echo "<script>alert('操作成功');history.go(-1);</script>";die;
}
?>
<html>
<head>
    <title>辽宁专用</title>
</head>
<body>
<form action="" method="post">
    <table>
        <tr><td colspan="2"><h3>辽宁重新下载片源并注入CDN专用</h3></td></tr>
        <tr>
            <td>当前需替换的片源GUID：</td>
            <td><input type="text" name="id" style="width:240px;"/></td>
        </tr>
        <tr>
            <td>替换后片源地址：</td>
            <td><input type="text" name="url" style="width:800px;"/></td>
        </tr>
        <tr>
            <td colspan="2" align="middle"><input type="submit" value="提交" /></td>
        </tr>
    </table>
</form>
</body>
</html>