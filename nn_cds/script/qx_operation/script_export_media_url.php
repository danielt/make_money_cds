<?php
/**
 * 辽宁注入CDN专用，此仅用于C2队列重发。
 * 背景：重新从云存储上下载视频文件并重新注入
 * 方案：从CSV文件中获取并用于更改片源的下载地址，及修改注入ID，并执行重新注入。
 * User: LZ
 * Date: 2018/3/14
 * Time: 11:21
 */
header("content-type:text/html;charset=utf-8");
ini_set("display_errors",0);
include_once dirname(dirname(dirname(__FILE__))) . "/nn_logic/nl_common.func.php";
include_once dirname(dirname(dirname(__FILE__))) . "/mgtv_v2/mgtv_init.php";
include_once dirname(dirname(dirname(__FILE__))) . "/mgtv_v2/models/queue_task_model.php";

if(isset($_FILES) && isset($_FILES['urls']) && !empty($_FILES['urls']))
{
    $dc = nl_get_dc(array(
        'db_policy' => NL_DB_WRITE | NL_DB_READ,
        'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
    ));
    $dc->open();
    $fail_arr = array();
    $fp = fopen($_FILES['urls']['tmp_name'],"r");
    while ($result = fgetcsv($fp))
    {
        $id = $result[0];
        $url = $result[1];
        //查询nns_vod_media/nns_mgtvbk_c2_task是否存在片源
        $query_sql = "select nns_id from nns_vod_media where nns_id = '{$id}' and nns_deleted = 0";
        $media_re = nl_query_by_db($query_sql,$dc->db());
        if(!is_array($media_re))
        {
            $fail_arr[] = $result;
            continue;
        }
        $c2_sql = "select * from nns_mgtvbk_c2_task where nns_ref_id = '{$id}'";
        $c2_re = nl_query_by_db($c2_sql,$dc->db());
        if(!is_array($c2_re))
        {
            $fail_arr[] = $result;
            continue;
        }
        //修改片源GUID及URL下载地址
        $new_vod_media_id = np_guid_rand('media');
        $media_ex = "update nns_vod_media set nns_id = '{$new_vod_media_id}',nns_url='{$url}' where nns_id = '{$id}'";
        nl_execute_by_db($media_ex,$dc->db());
        //修改C2任务
        $c2_ex = "update nns_mgtvbk_c2_task set nns_ref_id = '{$new_vod_media_id}' where nns_ref_id = '{$id}'";
        nl_execute_by_db($c2_ex,$dc->db());
        //修改中心同步指令
        $op_queue_ex = "update nns_mgtvbk_op_queue set nns_media_id = '{$new_vod_media_id}' where nns_media_id = '{$id}'";
        nl_execute_by_db($op_queue_ex,$dc->db());
        //修改切片指令中的任务
        $clip_ex = "update nns_mgtvbk_clip_task set nns_video_media_id = '{$new_vod_media_id}' where nns_video_media_id = '{$id}'";
        nl_execute_by_db($clip_ex,$dc->db());

        //执行重新注入
        $info = $c2_re[0];
        nl_execute_by_db("update nns_mgtvbk_c2_task set nns_status=7,nns_epg_status=100 where nns_id='{$info['nns_id']}'", $dc->db());
        $queue_task_model = new queue_task_model();
        $queue_task_model->q_report_task($info['nns_id']);
        $queue_task_model->q_add_task($new_vod_media_id,$info['nns_type'] , $info['nns_action'],$info['nns_org_id'],null,true,$info['nns_message_id'],null,99);
    }
    fclose($fp);
    if(!empty($fail_arr))
    {
        $file_path = dirname(__FILE__).'/fail_replace.csv';
        $fp_2 = fopen($file_path, 'w+');
        fwrite($fp_2, "\xEF\xBB\xBF");
        foreach ($fail_arr as $value)
        {
            fputcsv($fp_2, $value);
        }
        fclose($fp_2);
        //打开文件
        $basename = basename($file_path);
        $file = fopen ( $file_path, "r" );
        Header ( "Content-type: application/octet-stream" );
        Header ( "Accept-Ranges: bytes" );
        Header ( "Accept-Length: " . filesize ( $file_path ) );
        Header ( "Content-Disposition: attachment; filename=" . $basename );
        echo fread ( $file, filesize ( $file_path ) );
        fclose ( $file );
        exit ();
    }
    else
    {
        echo "<script>alert('操作成功');history.go(-1);</script>";die;
    }
}
?>
<html>
<head>
    <title>辽宁专用</title>
</head>
<body>
<form action="" method="post" enctype="multipart/form-data">
    <table>
        <tr><td colspan="2"><h3>辽宁重新下载导入专用</h3></td></tr>
        <tr>
            <td>导入的CSV文件【格式需为UTF8】：</td>
            <td><input type="file" name="urls"/></td>
            <td><input type="submit" value="提交" /></td>
        </tr>
    </table>
</form>
</body>
</html>