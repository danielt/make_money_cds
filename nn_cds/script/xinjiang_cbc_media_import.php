<?php
header("Content-type:text/html;charset=utf-8");
set_time_limit(0);
include '../nn_logic/nl_common.func.php';
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
function insert_c2_task($dc,$data,$nns_org_id,$nns_epg_status=null)
{
    $date = date("Y-m-d H:i:s");
    $data['nns_id'] = md5($data['nns_id']."_".$nns_org_id);
    $data['nns_org_id'] = $nns_org_id;
    $data['nns_epg_fail_time'] = 0;
    $data['nns_cdn_fail_time'] = 0;
    $data['nns_notify_third_state'] = 0;
    $data['nns_create_time'] = $date;
    $data['nns_modify_time'] = $date;
    if($nns_epg_status!==null && strlen($nns_epg_status)>0)
    {
        $data['nns_epg_status'] = $nns_epg_status;
    }
    $sql="select * from nns_mgtvbk_c2_task where nns_org_id='{$data['nns_org_id']}' and nns_type='{$data['nns_type']}' and nns_ref_id='{$data['nns_ref_id']}'";
    $result = nl_query_by_db($sql, $dc->db());
    if(is_array($result) && !empty($result))
    {
        echo "data exsist:".$sql.'<br/>';
        return true;
    }
    $result_ex = nl_execute_by_db(sql_insert('nns_mgtvbk_c2_task',$data), $dc->db());
    if(!$result_ex)
    {
        echo "execute sql error:".$sql.'<br/>';
        die;
    }
    echo "execute sql Ok:".$sql.'<br/>';
    return true;
}

function sql_insert($table, $data) {
    $field_str = '';
    $value_str = '';
    foreach ($data as $k => $v) 
    {
        $field_str .= $k . ',';
        if(strlen($v)<1)
        {
            $value_str .= "'',";
        }
        else
        {
            $value_str .= "'$v',";
        }
    }
    $field_str = rtrim($field_str, ',');
    $value_str = rtrim($value_str, ',');
    return "insert into $table ($field_str) values($value_str)";
}
$sql="select nns_id from nns_vod where nns_name in('倒霉熊第一部', '加菲猫第三季', '海底小纵队第一季', '最强战士迷你特工队', '瓢虫雷迪', '超级飞侠 第三季', '小马宝莉友谊的魔力第二季', '喜羊羊与灰太狼之羊羊小侦探',".
"'新葫芦兄弟', '铠甲勇士捕将', '愤怒的小鸟动画版第二季', '愤怒的小鸟动画版第一季', '熊出没之奇幻空间', '小幻与冲冲第2季', '搞笑长鼻怪第一季', '小黄人与格鲁日记',".
"'阿U之神奇幻镜', '爱探险的朵拉第八部', '钢甲小龙侠', '最强战士迷你特工队之英雄的诞生', '外星猴子', '梦想召唤王', '聪明的顺溜之特殊任务', '故事枕头',".
"'多米诺之家', '图腾领域第一季', '早上好咕咕妈', '超级飞侠大百科第一季', '熊出没之熊心归来', '巴啦啦小魔仙之飞越彩灵堡第一季', '愤怒的小鸟之猪猪传', '搞笑长鼻怪第二季',".
"'碰碰狐伊索寓言故事', '凯特与米米兔', '猪猪侠第十一部光明守卫者', '猫和老鼠：间谍使命', '樱桃小丸子', '贝瓦儿歌萌宝欢乐颂', '怪诞小镇第一季', '积高侠与阿里巴巴',".
"'变形金刚领袖的挑战第二季', '嗨，顾得白', '加菲猫第五季', '蜡笔小新：梦境世界大突击', '变形金刚救援汽车人第三季', '小公主苏菲亚', '开心兔第一季', '小马国女孩之彩虹摇滚',".
"'芭比之神秘之门', '乐高星球大战费明克银河探险', '甜甜私房猫第三季', '贝瓦学堂', '少年锦衣卫第二季', '土豆侠第三季', '猪猪侠之终极决战', '袜子家族',".
"'喜羊羊与灰太狼之漫镜头', '功夫熊猫3', '芭比公主的力量', '芭比之粉红舞鞋', '多吉律动儿歌第二季', '超人总动员', '凯蒂猫育儿动画系列', '赛车总动员2',".
"'小鸡彩虹', '元气拯救队', '动植物大战', '愤怒的小鸟之猪猪传第二季', '火力少年王之悠风三少年', '开心汉字', '偶像活动第四季', '仙履奇缘3：时间魔法',".
"'魔术星球', '小飞机卡卡直升机芒果文第四季', '愤怒的小鸟stell第一季', '开心兔mini系列', '星原小宝之奇妙探索', '超级伙伴', '嘟哒和达达第二季', '那年那兔那些事儿',".
"'方形狗与大圆猫', '哆啦A梦：新·大雄的宇宙开拓史', '夏目友人帐第一季', '熊孩子儿歌之文明礼仪篇', '魔法总动员', '洞穴女孩第一季', '新我们这一家第二季', '神经豆',".
"'熊孩子数数歌', '勇敢传说', '树干小火车', '怪兽雄师第一季', '夏目友人帐第四季', '东南特卫队', '寻宝小队', '城市小英雄第一季',".
"'夏目友人帐第二季', '魔幻车神第二季', '多宝一家人', '洛克王国神宠外传', '泡芙小姐', '愤怒的小鸟Stella第二季', '猪猪侠之囧囧危机', '功夫熊猫2',".
"'天降美食2', '趣趣知知鸟第一季', '爆笑丛林第四季', '夏目友人帐第三季', '芭比与姐妹之赛马记', '魔法少女伊莉雅第四季', '贝乐虎古诗', '小王子',".
"'小狐狸一家第二季', '波噜噜问世界', '小鱼儿奇遇记', '洛克王国魔法学院', '天降美食1', '怪盗JOKER第一季', '哆啦A梦：大雄与奇迹之岛', '你看起来好像很好吃',".
"'美女与野兽（动漫）', '老雷斯的故事', '少年曹操', '花月精灵', '小飞机卡卡之直升机芒果文', '小叮当拯救精灵大作战', '狮子王2', '怪盗JOKER第三季',".
"'彗星路西法', '好想告诉你第二季', '毛球大牙奇趣之旅之最长的旅途', '哆啦A梦：大雄与绿巨人传', '天真与闪电', '丛林大反攻4吓傻了', '久保与二弦琴', '一周的朋友',".
"'趣味成语故事', '雨果带你看世界', '摩登原始人石器时代大乱斗', '海底总动员2多莉去哪儿', '怪兽雄师第二季', '大闹天宫', '超级大坏蛋', '蜜蜜与莉莎的魔幻旅程',".
"'恐龙王第二季', '恐龙王第一季', '大吉成长记', '枕边故事', '幽灵公主', '风夏', '怪盗JOKER第三季', '小马宝莉第六季',".
"'少年师爷之大禹宝藏', '少年张謇', '里约大冒险2', '阿童木', '风声水起之24节气的故事', '千意快乐小百科', '魔女宅急便', '花精灵战队',".
"'蝙蝠侠披风斗士归来', '趣趣知知鸟第二季', '小鹿斑比2', '云龙传奇之九龙战记', '兔八哥之兔子快跑', '小鸡彩虹第二季', '新网球王子OVA第2季', '最终幻想',".
"'丰丰农场', '阿里巴巴大盗奇兵', '小飞侠', '艾特熊和赛娜鼠', '正义联盟：毁灭',".
"'宠物小精灵：幻影的霸者索罗亚克') and nns_cp_id='cbczq' and nns_deleted !=1 and nns_id not in(select nns_ref_id from nns_mgtvbk_c2_task where nns_org_id='xjdx_hw' and nns_type='video') limit 1";
$result = nl_query_by_db($sql, $dc->db());
if(!is_array($result) || empty($result))
{
    echo $sql;die;
}
foreach ($result as $value)
{
    $sql_task_vod = "select * from nns_mgtvbk_c2_task where nns_org_id='cbczq_zte' and nns_type='video' and nns_status='0' and nns_ref_id='{$value['nns_id']}'";
    $result_task_vod = nl_query_by_db($sql_task_vod, $dc->db());
    if(!$result_task_vod)
    {
        echo "sql query error:".$sql_task_vod.'<br/>';
    }
    if(is_array($result_task_vod) && !empty($result_task_vod))
    {
        echo "sql query ok:".$sql_task_vod.'<br/>';
//         continue;
    }
    $result_task_vod = $result_task_vod[0];
    insert_c2_task($dc, $result_task_vod, 'xjdx_hw');
    insert_c2_task($dc, $result_task_vod, 'xjdx_zte','97');
    $sql_task_index = "select * from nns_mgtvbk_c2_task where nns_org_id='cbczq_zte' and nns_type='index' and nns_status='0' and nns_ref_id in(select nns_id from nns_vod_index where nns_vod_id='{$value['nns_id']}' and nns_deleted !=1)";
    $result_task_index = nl_query_by_db($sql_task_index, $dc->db());
    if(!$result_task_index)
    {
        echo "sql query error:".$sql_task_index.'<br/>';
    }
    if(is_array($result_task_index) && !empty($result_task_index))
    {
        foreach ($result_task_index as $index_value)
        {
            insert_c2_task($dc, $index_value, 'xjdx_hw');
            insert_c2_task($dc, $index_value, 'xjdx_zte','97');
        }
    }
    $sql_task_media = "select * from nns_mgtvbk_c2_task where nns_org_id='cbczq_zte' and nns_type='media' and nns_status='0' and nns_ref_id in(select nns_id from nns_vod_media where nns_vod_id='{$value['nns_id']}' and nns_deleted !=1)";
    $result_task_media = nl_query_by_db($sql_task_media, $dc->db());
    if(!$result_task_media)
    {
        echo "sql query error:".$sql_task_media.'<br/>';
    }
    if(is_array($result_task_media) && !empty($result_task_media))
    {
        foreach ($result_task_media as $media_value)
        {
            insert_c2_task($dc, $media_value, 'xjdx_hw');
            insert_c2_task($dc, $media_value, 'xjdx_zte','97');
        }
    }
}

