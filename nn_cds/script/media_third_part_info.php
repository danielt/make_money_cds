<?php 
set_time_limit(0);
header("Content-type:text/html;charset=utf-8");
include '../nn_logic/nl_common.func.php';
$sql="select * from nns_vod_media where nns_cp_id='TV_sou'";
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$result = nl_query_by_db($sql, $dc->db());
function is_json($string)
{
    if(!is_string($string))
    {
        return false;
    }
    $string = strlen($string) <1 ? '' : $string;
    $string = trim($string);
    if(strlen($string) <1)
    {
        return false;
    }
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}
if(empty($result) || !is_array($result))
{
    echo "ok";die;
}
$temp_data = null;
foreach ($result as $val)
{
    if(!is_json($val['nns_ext_url']))
    {
        continue;
    }
    $arr_ext_url = json_decode($val['nns_ext_url'],true);
    if(!is_array($arr_ext_url) || empty($arr_ext_url))
    {
        continue;
    }
    foreach ($arr_ext_url as $ex_key=>$ex_val)
    {
        if(isset($temp_data[$ex_key]))
        {
            continue;
        }
        $temp_data[$ex_key] = $ex_key;
    }
}
$str_date = date("Y-m-d H:i:s");
if(is_array($temp_data) && !empty($temp_data))
{
    
    foreach ($temp_data as $val)
    {
        $sql="select * from nns_full_assets_site where nns_siteid='{$val}'";
        $result = nl_query_by_db($sql, $dc->db());
        if(!is_array($result))
        {
            $str_guid = np_guid_rand();
            $sql_in = "insert into nns_full_assets_site(nns_id,nns_siteid,nns_name,nns_state,nns_img,nns_create_time,nns_modify_time) values('{$str_guid}','{$val}','{$val}','0','','{$str_date}','{$str_date}')";
            nl_execute_by_db($sql_in, $dc->db());
        }
    }
}
