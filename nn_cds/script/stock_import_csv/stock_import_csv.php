<?php
/**
 * Created by PhpStorm.
 * User: Hushengs
 * Date: 2016/10/26
 * Time: 2:07
 */
define('ORG_ID', 'cqyx_hw');
include '../../nn_logic/nl_common.func.php';
include '../../mgtv_v2/models/queue_task_model.php';
include '../../mgtv_v2/utils.func.php';
$dc_r = nl_get_dc(array(
    'db_policy' => NL_DB_READ,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$db_r =$dc_r->db();
$file_data = fopen("file.csv",'r');
$queue_task_model = new queue_task_model();
while (!feof($file_data))
{
    $line_arr = fgetcsv($file_data);
    if(is_array($line_arr) && !empty($line_arr))
    {
        $file_name = substr($line_arr[0],0,-3);
        $sql = "select * from nns_vod_media where nns_media_name ='$file_name'";
        $check_result = nl_query_by_db($sql, $db_r);
        if(is_array($check_result) && !empty($check_result))
        {
            $queue_task_model->cqyx_stock_import(ORG_ID,$check_result[0]['nns_id'],$check_result[0]['nns_vod_index_id'],$check_result[0]['nns_vod_id']);
        }
        else
        {
            $unmetadata_media[] = array($file_name.'.ts');
        }
    }
}
fclose($file_data);
ob_clean();
ob_flush();
$filename = date('His')."_unmetadata_media_list.csv";
$csv_save_file_path = dirname(__FILE__).'/'.$filename ;
header("Content-Disposition: attachment;filename=". $filename);
header('Cache-Control: max-age=0');
ob_start();
$fp = fopen($csv_save_file_path,'a');
export_media_csv($fp,array('file_name'));
foreach($unmetadata_media as $val)
{
    export_media_csv($fp, $val);
}
function export_media_csv($fp,$data)
{
    fputcsv($fp, $data);
    unset($data);
    return ;
}
