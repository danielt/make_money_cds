<?php 
/**
 * script  测试发送amqp的数据
 * @author liangpan
 * @date 2015-09-15
 */
header("Content-type:text/html;charset=utf-8");
set_time_limit(0);
date_default_timezone_set('Asia/Chongqing');
include_once dirname(__FILE__)."/common/common.php";
define("MODULE_NAME", 'script_send_amqp_message_by_sztw');
define("ORG_ID", 'sztw');
$date_time=date("YmdHis");

global $g_mgtv_amqp_name_config;
if(!isset($g_mgtv_amqp_name_config[ORG_ID]['channel_name']) || !isset($g_mgtv_amqp_name_config[ORG_ID]['exchange_name']) || !isset($g_mgtv_amqp_name_config[ORG_ID]['route_name']))
{
	nn_echo_ex_msg("没有配置g_mgtv_amqp_name_config参数为：".var_export($g_mgtv_amqp_name_config,true),MODULE_NAME,$date_time);
	unset($g_mgtv_amqp_name_config);exit;
}
global $g_mgtv_amqp_host_config;
if(!isset($g_mgtv_amqp_host_config) || !is_array($g_mgtv_amqp_host_config) || empty($g_mgtv_amqp_host_config))
{
	nn_echo_ex_msg("没有配置g_mgtv_amqp_host_config参数为：".var_export($g_mgtv_amqp_host_config,true),MODULE_NAME,$date_time);
	unset($g_mgtv_amqp_name_config);exit;
}
$amqp_host_config = $g_mgtv_amqp_host_config;
$amqp_name_config = $g_mgtv_amqp_name_config[ORG_ID];
unset($g_mgtv_amqp_name_config);
unset($g_mgtv_amqp_name_config);

$message='<?xml version="1.0" encoding="UTF-8"?>
<video id="158070" name="[朱可儿]" video_type="video" result="pass" contentprovider="test_22"/>
';


//创建连接和channel
$conn = new AMQPConnection($amqp_host_config);
if (!$conn->connect()) {
	var_dump(array(
			'ret'=>1,
			'reason'=>"Cannot connect to the broker",
	));die;
}
$channel = new AMQPChannel($conn);
//创建交换机对象
$ex = new AMQPExchange($channel);
//队列名
$route_name = $amqp_name_config['route_name'];
$exchange_name = $amqp_name_config['exchange_name'];
$ex->setName($exchange_name);

//发送消息
//$channel->startTransaction(); //开始事务
var_dump($ex->publish($message, $route_name));
//$channel->commitTransaction(); //提交事务

$conn->disconnect();
