<?php 
header("Content-type:text/html;charset=utf-8");
include '../nn_logic/nl_common.func.php';
include_once dirname(dirname(__FILE__)).'/nn_logic/sp/sp.class.php';


$dc = nl_get_dc(array(
    'db_policy' => NL_DB_READ,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$result_sp = nl_sp::query_by_condition($dc, array('nns_state'=>0));
$result_sp = isset($result_sp['data_info']) ? $result_sp['data_info'] : null;

if(isset($_REQUEST['action']) && $_REQUEST['action']=='import')
{
    $arr_type = array(
        'media'=>'nns_vod_media',
        'index'=>'nns_vod_index',
        'video'=>'nns_vod',
        'live'=>'nns_live',
        'live_index'=>'nns_live_index',
        'live_media'=>'nns_live_media',
        'playbill'=>'nns_live_playbill_item',
        'file'=>'nns_file_package',
        'product'=>'nns_product',
    );
    include dirname(dirname(__FILE__)).'/mgtv_v2/models/queue_task_model.php';
    $sql = isset($_REQUEST['ex_sql']) ? trim($_REQUEST['ex_sql']) : '';
    $type = isset($_REQUEST['type']) ? trim($_REQUEST['type']) : '';
    $sp_id = isset($_REQUEST['sp_id']) ? trim($_REQUEST['sp_id']) : '';
    $arr_sp_id = strlen($sp_id) >0 ? array($sp_id) : null;
    if(strlen($sql) <1)
    {
        echo "<script>alert('SQL 语句为空');history.go(-1);</script>";die;
    }
    if(!in_array($type, array_keys($arr_type)))
    {
        echo "<script>alert('影片类型非法');history.go(-1);</script>";die;
    }
    if(!preg_match("/^select\s+\*\s+from\s+{$arr_type[$type]}\s+where\s+.*+/i", $sql))
    {
        echo "<script>alert('SQL 非法');history.go(-1);</script>";die;
    }
    $result = nl_query_by_db($sql, $dc->db());
    if(is_array($result))
    {
        foreach ($result as $val)
        {
            $queue_task_model = new queue_task_model();
            $queue_task_model->q_add_task_op_mgtv($val['nns_id'], $type, 'add','',true,null,$arr_sp_id);
            unset($queue_task_model);
        }
    }
    echo "<script>alert('成功');history.go(-1);</script>";die;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>中心注入指令</title>
    </head>
    <body>
        <form method="post" action="./import_queue.php">
            <input name="action" type='hidden' value='import'><br/>
            <br>
                    中心注入指令 脚本：<hr/><br>
                    选择下游平台（可不选）:
            <select name="sp_id">
                <option value="">--请选择下游平台--</option>
                <?php if(is_array($result_sp) && !empty($result_sp)){foreach ($result_sp as $sp_value){?>
                    <option value="<?php echo $sp_value['nns_id'];?>"><?php echo $sp_value['nns_id'].' | '.$sp_value['nns_name'];?></option>
                <?php }} ?>
            </select><br/><br>
                    选择影片类型:
            <select name="type">
                <option value="">--请选择影片类型--</option>
                <option value="video">主媒资</option>
                <option value="index">分集</option>
                <option value="media">片源</option>
                <option value="live">直播</option>
                <option value="live_index">直播分集</option>
                <option value="live_media">直播源</option>
                <option value="playbill">节目单</option>
                <option value="file">文件</option>
                <option value="product">产品包</option>
            </select><br/><br>
            SQL 语句：
            <textarea name="ex_sql" cols=100 rows=5></textarea><br/><br/>
            <input type="submit" value="提交"/>
        </form>
    </body>
</html>