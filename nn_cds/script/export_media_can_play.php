<?php 
header("Content-type:text/html;charset=utf-8");
include '../nn_logic/nl_common.func.php';
function export_csv($fp,$data)
{
    ob_clean();
    if(empty($data) || !is_array($data))
    {
        return ;
    }
    foreach ($data as $_k=>$_v)
    {
        if($_k == 0)
        {
            fputcsv($fp, array_keys($_v));
        }
        fputcsv($fp, array_values($_v));
    }
    unset($fp);
    unset($data);
    ob_flush();
    flush();
    return ;
}
function return_data($ret, $reason, $data = null)
{
    return array (
        'ret' => $ret,
        'reason' => $reason,
        'data_info' => ($data !== null) ? $data : null
    );
}
function is_json($string)
{
    if(!is_string($string))
    {
        return false;
    }
    $string = strlen($string) <1 ? '' : $string;
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}
function public_get_curl_content($play_id , $time = 30)
{
    if(strlen($play_id) <1)
    {
        return return_data(2, "播放ID为空");
    }
    $file_path="http://172.31.183.124/getRequestPlayUrl?userCode=130726187&assetID={$play_id}&spCode=SP1N02M00_04_063&targetSystemID=30-S&devicetype=1";
    //curl抓取 远程文件的内容
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $file_path);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $time);
    $content = curl_exec($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    //如果内容为空  或者根本没这个xml文件
    if (empty($content) || !$content || $http_code >= 400)
    {
        return return_data(2, "消息：curl下载文件失败地址:" . $file_path . '超时时间:' . $time);
    }
    return return_data(0, 'CURL获取成功', $content);
}

$dc = nl_get_dc(array(
    'db_policy' => NL_DB_READ,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$sql = "SELECT media.nns_id,media.nns_content_id,media.nns_kbps,inde.nns_index as index_index,vod.nns_name as vod_name FROM `nns_vod_media` as media left join ".
" nns_vod_index as inde on inde.nns_id=media.nns_vod_index_id left join nns_vod as vod on vod.nns_id=media.nns_vod_id WHERE media.`nns_cp_id` IN ".
" ('fuse_xiaole','fuse_xiaole_encode') AND media.`nns_filetype` = 'ts' AND media.`nns_url` LIKE '%.ts%' AND media.`nns_kbps` = '2300' ".
" and media.nns_content_id !='' and media.nns_media_service='jscn_sihua_old' ORDER BY media.nns_vod_id desc,inde.nns_index asc limit 0,500";
if(strlen($sql) <1)
{
    echo "<script>alert('SQL 语句为空');history.go(-1);</script>";die;
}
$result = nl_query_by_db($sql, $dc->db());
$filename=date('Ymd_His').'_media_info.csv';
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=". $filename);
header('Cache-Control: max-age=0');
print(chr(0xEF).chr(0xBB).chr(0xBF));
ob_start();
$fp = fopen('php://output', 'a');
$arr_result = null;
foreach ($result as $val)
{
    $curl_data = public_get_curl_content($val['nns_content_id']);
    $val['index_index']++;
    if($curl_data['ret'] !=0 || !isset($curl_data['data_info']) || strlen($curl_data['data_info']) <1)
    {
        $arr_result[] = array(
            'media_id'=>$val['nns_id'],
            'nns_id'=>$val['nns_content_id'],
            'nns_cp_id'=>$val['nns_cp_id'] == 'fuse_xiaole' ? '孝乐自动转码' : '孝乐手动转码',
            'media_name'=>$val['vod_name'].'第'.$val['index_index'].'集,码率['.$val['nns_kbps'].']',
            'desc'=>$curl_data['reason'],
        );
        continue;
    }
    if(is_json($curl_data['data_info']))
    {
        $data_info = json_decode($curl_data['data_info'],true);
        $data_info['resultCode'] = isset($data_info['resultCode']) ? $data_info['resultCode'] : '';
        $data_info['resultMsg'] = isset($data_info['resultMsg']) ? $data_info['resultMsg'] : '';
        $data_info['onDemandURL'] = isset($data_info['onDemandURL']) ? $data_info['onDemandURL'] : '';
        if(strlen($data_info['resultCode']) <1 || $data_info['resultCode'] !='0')
        {
            $arr_result[] = array(
                'media_id'=>$val['nns_id'],
                'nns_id'=>$val['nns_content_id'],
                'nns_cp_id'=>$val['nns_cp_id'] == 'fuse_xiaole' ? '孝乐自动转码' : '孝乐手动转码',
                'media_name'=>$val['vod_name'].'第'.$val['index_index'].'集,码率['.$val['nns_kbps'].']',
                'desc'=>"结果[code:{$data_info['resultCode']},描述:{$data_info['resultMsg']},url:{$data_info['onDemandURL']}]",
            );
        }
    }
    else
    {
        $arr_result[] = array(
            'media_id'=>$val['nns_id'],
            'nns_id'=>$val['nns_content_id'],
            'nns_cp_id'=>$val['nns_cp_id'] == 'fuse_xiaole' ? '孝乐自动转码' : '孝乐手动转码',
            'media_name'=>$val['vod_name'].'第'.$val['index_index'].'集,码率['.$val['nns_kbps'].']',
            'desc'=>"非json",
        );
    }
}
export_csv($fp,$arr_result);