<?php
set_time_limit(0);
header("Content-type:text/html;charset=utf-8");
include '../nn_logic/nl_common.func.php';
$sql="select * from nns_file_encode";
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$result = nl_query_by_db($sql, $dc->db());
if(!is_array($result) || empty($result))
{
    exit('ok');
}
foreach ($result as $val)
{
    $nns_file_url = isset($val['nns_file_url']) ? $val['nns_file_url'] : '';
    if(!is_json($nns_file_url))
    {
        continue;
    }
    $arr_file_url = json_decode($nns_file_url,true);
    if(!isset($arr_file_url['media']['in']) || strlen($arr_file_url['media']['in']) <1 || !isset($arr_file_url['media']['out']) || strlen($arr_file_url['media']['out']) <1)
    {
        continue;
    }
    $arr_in = pathinfo($arr_file_url['media']['in']);
    $arr_out = pathinfo($arr_file_url['media']['out']);
    if(isset($arr_in['extension']) && strtolower($arr_in['extension']) == 'mp4' && isset($arr_out['extension']) && strtolower($arr_out['extension']) == 'mp4')
    {
//         echo json_encode($val);die;
        $sql_ex ="update nns_file_encode set nns_state='4' where nns_id='{$val['nns_id']}'";
        nl_execute_by_db($sql_ex, $dc->db());
    }
}



function is_json($string)
{
    $string = strlen($string) <1 ? '' : $string;
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}