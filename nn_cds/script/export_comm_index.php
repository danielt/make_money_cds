<?php
/**
 * 删除重复注入的 媒资包 分集 片源
 */
header("Content-type:text/html;charset=utf-8");
include '../nn_logic/nl_common.func.php';
$dc_r = nl_get_dc(array(
		'db_policy' => NL_DB_READ,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$db_r =$dc_r->db();

$now_num = $_REQUEST['page'] ? $_REQUEST['page'] : 0;
$pre_num =200;
$start = $now_num * $pre_num;
#先查询重复注入的分集
$sql_index_comm="select temp.nns_import_id,temp.num from (
			select count(nns_import_id) as num,nns_import_id,nns_create_time,nns_name 
			from nns_vod_index GROUP BY nns_import_id) as temp where temp.num>1 limit {$start},{$pre_num}";
$result_index_comm = nl_query_by_db($sql_index_comm, $db_r);
if(!$result_index_comm)
{
	echo "db error ,query comm index sql:".$sql_index_comm;
}
$filename="分集".date('Ymd').'.csv';
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=". $filename);
header('Cache-Control: max-age=0');
print(chr(0xEF).chr(0xBB).chr(0xBF));
ob_start();
$fp = fopen('php://output', 'a');
fputcsv($fp, array('分集','分集id','名称','创建时间','原因','重复次数'));
if(is_array($result_index_comm) && !empty($result_index_comm))
{
	foreach ($result_index_comm as $val)
	{
		$sql_query_index = "select nns_id,nns_name,nns_vod_id,nns_create_time from nns_vod_index where nns_import_id='{$val['nns_import_id']}'";
		$result_query_index = nl_query_by_db($sql_query_index, $db_r);
		if(!$result_query_index)
		{
			echo "db error ,query every index sql:".$sql_query_index;
		}
		if(is_array($result_query_index))
		{
			foreach ($result_query_index as $_v)
			{
// 				$sql_query_vod ="select count(*) as num from nns_vod where nns_id='{$_v['nns_vod_id']}'";
// 				$result_query_vod = nl_query_by_db($sql_query_vod, $db_r);
// 				if(!$result_query_vod || !is_array($result_query_vod))
// 				{
// 					echo "db error ,query index vod sql:".$sql_query_media;
// 					exit;
// 				}
// 				if($result_query_vod[0]['num'] <= 0)
// 				{
// 					fputcsv($fp, array(' ',$_v['nns_id'],$_v['nns_name'],$_v['nns_create_time'],'no vod'));
// 				}
				$sql_query_media ="select count(*) as num from nns_vod_media where nns_vod_index='{$_v['nns_id']}'";
				$result_query_media = nl_query_by_db($sql_query_media, $db_r);
				if(!$result_query_media || !is_array($result_query_media))
				{
					echo "db error ,query index media sql:".$sql_query_media;
					exit;
				}
				$num = $result_query_media[0]['num'];
				if($num <= 0)
				{
					fputcsv($fp, array(' ',$_v['nns_id'],$_v['nns_name'],$_v['nns_create_time'],'no media',$val['num']));
				}
			}
		}
	}
}
