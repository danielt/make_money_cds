<?php
set_time_limit(0);
header("Content-type:text/html;charset=utf-8");
include_once dirname(dirname(__FILE__)) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/models/queue_task_model.php';
define('XIAOLE_MOIVE_IMPORT_SP', 'vstartek');
$dc = nl_get_dc(array(
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$int_limit = '2000';
function make_insert_sql($table,$params)
{
    $field_str = '';
    $value_str = '';
    if (is_array($params) && !empty($params))
    {
        if(isset($params[0]) && !empty($params[0]) && is_array($params[0]))
        {
            foreach ($params as $k=>$v)
            {
                if(!is_array($v) || empty($v))
                {
                    continue;
                }
                foreach ($v as $_k=>$_v)
                {
                    if($k == 0)
                    {
                        if (strpos($_k, 'nns_') === FALSE)
                        {
                            $_k = 'nns_' . $_k;
                        }
                        $field_str .= $_k . ',';
                    }
                    $value_str .= "'$_v',";
                }
                $value_str = rtrim($value_str, ',');
                $value_str.="),(";
            }
            $value_str = rtrim($value_str,'),(');
        }
        else
        {
            foreach ($params as $k => $v)
            {
                if (strpos($k, 'nns_') === FALSE)
                {
                    $k = 'nns_' . $k;
                }
                $field_str .= $k . ',';
                $value_str .= "'$v',";
            }
        }
    }
    $field_str = rtrim($field_str, ',');
    $value_str = rtrim($value_str, ',');
    $sql = "insert into $table ($field_str) values($value_str)";
    return $sql;
}
$last_data = null;
$sql_c2="select task.*,inde.nns_cp_id as index_cp_id,inde.nns_vod_id as index_vod_id from nns_mgtvbk_c2_task as task left join nns_vod_index ".
    " as inde on inde.nns_id=task.nns_ref_id where task.nns_org_id='".XIAOLE_MOIVE_IMPORT_SP."' and task.nns_type='index' and ".
    " task.nns_ref_id not in (select nns_index_id from nns_mgtvbk_op_queue where nns_org_id='".XIAOLE_MOIVE_IMPORT_SP."' and nns_type='media') limit {$int_limit}";
echo $sql_c2."<br>";
$result_c2 = nl_query_by_db($sql_c2,$dc->db());
if(is_array($result_c2) && !empty($result_c2))
{
    foreach ($result_c2 as $xiaole_vod_key=>$xiaole_vod_val)
    {
        if($xiaole_vod_val['index_cp_id'] == 'fuse_xiaole_encode')
        {
            $sql_media="select * from nns_vod_media where nns_vod_id='{$xiaole_vod_val['index_vod_id']}' and nns_vod_index_id='{$xiaole_vod_val['nns_ref_id']}' ".
                " and nns_deleted !='1' and nns_encode_flag='0' and (nns_content_id='' or isnull(nns_content_id))
                    and (nns_media_service='' or isnull(nns_media_service)) order by nns_kbps desc,nns_create_time desc limit 1";
        }
        else
        {
            $sql_media="select * from nns_vod_media where nns_vod_id='{$xiaole_vod_val['index_vod_id']}' and nns_vod_index_id='{$xiaole_vod_val['nns_ref_id']}' ".
                " and nns_deleted !='1' and nns_encode_flag='0' and nns_url like 'cvideo/%' and (nns_content_id='' or isnull(nns_content_id))
                    and (nns_media_service='' or isnull(nns_media_service)) and nns_kbps in('2300','2301','0') order by nns_kbps desc,nns_create_time desc limit 1";
        }
        echo $sql_media."<hr>";
        $result_media = nl_query_by_db($sql_media,$dc->db());
        if(!$result_media || empty($result_media) || !is_array($result_media))
        {
            continue;
        }
        $last_data[] = $result_media[0]['nns_id'];
    }
}
if(!is_array($last_data) || empty($last_data))
{
    die();
}
$i=0;
foreach ($last_data as $val)
{
    $queue_task_model = new queue_task_model();
    $queue_task_model->q_add_task($val, 'media', 'add',XIAOLE_MOIVE_IMPORT_SP,null,true);
}