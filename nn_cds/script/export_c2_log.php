<?php 
header("Content-type:text/html;charset=utf-8");
include '../nn_logic/nl_common.func.php';
function export_csv($fp,$data)
{
    ob_clean();
    if(empty($data) || !is_array($data))
    {
        return ;
    }
    foreach ($data as $_k=>$_v)
    {
        if($_k == 0)
        {
            fputcsv($fp, array_keys($_v));
        }
        fputcsv($fp, array_values($_v));
    }
    unset($fp);
    unset($data);
    ob_flush();
    flush();
    return ;
}
function make_other_guid($str_integer_id,$video_type='',$index=0,$str_type='PO',$str_header)
{
    $str_type = strlen($str_type) == 2 ? $str_type : 'PO';
    $index = strlen($index) == 1 ? $index : 0;
    switch ($video_type)
    {
        case 'video':
            $video_type = 'PT';
            break;
        case 'index':
            $video_type = 'TI';
            break;
        case 'media':
            $video_type = 'MO';
            break;
        case 'live':
            $video_type = 'CH';
            break;
        case 'live_index':
            $video_type = 'LI';
            break;
        case 'live_media':
            $video_type = 'PC';
            break;
        case 'playbill':
            $video_type = 'SD';
            break;
        case 'file':
            $video_type = 'FI';
            break;
        default:
            $video_type = 'PU';
    }
    return $str_header.$str_type .$video_type. str_pad($str_integer_id, 7, "0", STR_PAD_LEFT).$index;
}
function get_guid($str_integer_id,$str_header,$video_type)
{
    //兼容结束
    switch ($video_type)
    {
        case 'video':
            $import_id_model_flags_4 = 'PT';
            break;
        case 'index':
            $import_id_model_flags_4 = 'TI';
            break;
        case 'media':
            $import_id_model_flags_4 = 'MO';
            break;
    }
    
    $str_header = (strlen($str_header) < 8) ? str_pad($str_header, 8, "0", STR_PAD_RIGHT) : substr($str_header, 0,8);
    return $str_header.$import_id_model_flags_4 . str_pad($str_integer_id, 10, "0", STR_PAD_LEFT);
}
if(isset($_REQUEST['action']) && $_REQUEST['action']=='import')
{
    $dc = nl_get_dc(array(
        'db_policy' => NL_DB_READ,
        'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
    ));
    $sql = isset($_REQUEST['ex_sql']) ? trim($_REQUEST['ex_sql']) : '';
    $type = isset($_REQUEST['type']) ? trim($_REQUEST['type']) : '';
    if(strlen($sql) <1)
    {
        echo "<script>alert('SQL 语句为空');history.go(-1);</script>";die;
    }
    $result = nl_query_by_db($sql, $dc->db());
    $filename=date('Ymd_His').'_c2_log.csv';
    header('Content-Type: application/vnd.ms-excel');
    header("Content-Disposition: attachment;filename=". $filename);
    header('Cache-Control: max-age=0');
    print(chr(0xEF).chr(0xBB).chr(0xBF));
    ob_start();
    $fp = fopen('php://output', 'a');
    $arr_result = null;
    foreach ($result as $val)
    {
        $arr_result[]=array(
            'name'=>$val['nns_name'],
            'c2_task_id'=>$val['c2_task_id'],
            'c2_log_id'=>$val['nns_id'],
            'package_id'=>make_other_guid($val['video_integer_id'],'video',0,'PA','JSB12028'),
            'video_title_id'=>get_guid($val['video_integer_id'],'JSB12028', 'video'),
            'index_title_id'=>get_guid($val['index_integer_id'],'JSB12028', 'index'),
            'media_title_id'=>get_guid($val['media_integer_id'],'JSB12028', 'media'),
        );
    }
    export_csv($fp,$arr_result);
}
else
{
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>中心注入指令</title>
        </head>
        <body>
            <form method="post" action="./export_c2_log.php">
                <input name="action" type='hidden' value='import'><br/>
                <br>
                c2_task_log 脚本：<hr/><br>
                SQL 语句：
                <textarea name="ex_sql" cols=100 rows=5></textarea><br/><br/>
                <input type="submit" value="提交"/>
            </form>
        </body>
    </html>
<?php 
}
?>