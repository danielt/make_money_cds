<?php
header("Content-type:text/html;charset=utf-8");
include dirname(dirname(dirname(__FILE__))).'/nn_logic/nl_common.func.php';
include dirname(dirname(dirname(__FILE__))).'/mgtv_v2/models/queue_task_model.php';
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$db_copy = nl_get_log_db(NL_DB_READ);
$db_copy->open();
$data_file_path = dirname(__FILE__) .'/test.txt';
$oper_data_num_each_time = 20; //每次读取的数据条数

if (!file_exists($data_file_path))
{
    echo '数据文件不存在';
    exit();
}
$arr_cp = array(
    'no_drm',0
);
// 逐行读取数据
$data_file = fopen($data_file_path, 'r');
$count = 1;
$is_left_empty = true;
$locked = flock($data_file, LOCK_EX + LOCK_NB);
while (!feof($data_file))
{
    if ($count <= $oper_data_num_each_time)
    {
        $data_item = fgets($data_file);
        if (!$data_item)
        {
            continue;
        }
        if ($count == $oper_data_num_each_time)
        {
            // 最后一条时，记录剩余内容
            ob_start();
            fpassthru($data_file);
            $is_left_empty = false;
        }
        $data_item = trim($data_item);
        $sql="select nns_ref_id from nns_mgtvbk_c2_task where nns_type='media' and nns_file_path ='{$data_item}' limit 1";
        $result_copy = nl_query_by_db($sql, $db_copy);
        if(!isset($result_copy[0]['nns_ref_id']) || empty($result_copy[0]['nns_ref_id']))
        {
            continue;
        }
        $str_media_id = $result_copy[0]['nns_ref_id'];
        $result_media = nl_query_by_db("select * from nns_vod_media where nns_id='{$str_media_id}' limit 1", $db_copy);
        
        if(!isset($result_media[0]) || !is_array($result_media[0]) || empty($result_media[0]))
        {
            continue;
        }
        $result_media = $result_media[0];
        $str_index_id = $result_media['nns_vod_index_id'];
        $str_video_id = $result_media['nns_vod_id'];
        $str_media_import_id = $result_media['nns_import_id'];
        $str_cp_id = $result_media['nns_cp_id'];
        $result_media['nns_url'] = $data_item;
        $result_media_ex = nl_query_by_db("select * from nns_vod_media_ex where nns_vod_media_id='{$str_media_import_id}' and nns_cp_id='{$str_cp_id}'", $db_copy);
        $result_index = nl_query_by_db("select * from nns_vod_index where nns_id='{$str_index_id}' limit 1", $db_copy);
        if(!isset($result_index[0]) || !is_array($result_index[0]) || empty($result_index[0]))
        {
            continue;
        }
        $result_index = $result_index[0];
        $result_index_ex = nl_query_by_db("select * from nns_vod_index_ex where nns_vod_index_id='{$result_index['nns_import_id']}' and nns_cp_id='{$str_cp_id}'", $db_copy);
        
        $result_video = nl_query_by_db("select * from nns_vod where nns_id='{$str_video_id}' limit 1", $db_copy);
        
        if(!isset($result_video[0]) || !is_array($result_video[0]) || empty($result_video[0]))
        {
            continue;
        }
        $result_video = $result_video[0];
        $result_video_ex = nl_query_by_db("select * from nns_vod_ex where nns_vod_id='{$result_video['nns_asset_import_id']}' and nns_cp_id='{$str_cp_id}'", $db_copy);
        foreach ($arr_cp as $val)
        {
            $flag = ($val == '0') ? true : false;
            vod_data_in($dc, $result_video,$result_video_ex,$flag);
            index_data_in($dc, $result_index,$result_index_ex,$flag);
            media_data_in($dc, $result_media,$result_media_ex,$flag);
        }
        die();
        $count++;
    }
    else
    {
        break;
    }
}
// 如果最后数据不为空，将剩余内容重新写入到文件中
if ($is_left_empty)
{
    $left_content = '';
}
else
{
    $left_content = ob_get_clean();
}
flock($data_file, LOCK_EX + LOCK_NB);
fclose($data_file);
$result = file_put_contents($data_file_path, $left_content);


function make_insert_sql($table,$data,$flag=true)
{
    if(!is_array($data) || empty($data))
    {
        return '';
    }
    $str_key='';
    $str_value = '';
    if($flag)
    {
        foreach ($data as $key=>$val)
        {
            if(strlen($val) <1)
            {
                continue;
            }
            if($key == 'nns_integer_id')
            {
                continue;
            }
            $str_key.="{$key},";
            $str_value.="'{$val}',";
        }
    }
    else
    {
        foreach ($data as $key=>$val)
        {
            $str_value_in = '';
            foreach ($val as $_k=>$_v)
            {
                if($key == 0)
                {
                    $str_key.="{$_k},";
                }
                $str_value_in.="'{$_v}',";
            }
            $str_value_in = rtrim($str_value_in,',');
            $str_value.= "({$str_value_in}),";
        }
        $str_value = rtrim($str_value,',');
        $str_value = ltrim(rtrim($str_value,')'),'(');
    }
    $str_key = rtrim($str_key,',');
    $str_value = rtrim($str_value,',');
    $sql="insert into {$table} ({$str_key}) values ($str_value) ";
    return $sql;
}

function vod_data_in($dc,$data,$data_ex=null,$flag=true)
{
    $data['nns_tag'] = '26,27,28,29,30,31,';
    if($flag)
    {
        $data['nns_cp_id'] = '0';
        $data['nns_name'] = "DRM加密-".$data['nns_name'];
    }
    else
    {
        $data['nns_cp_id'] = 'no_drm';
        $data['nns_name'] = "清流-".$data['nns_name'];
        $data['nns_id'] = md5($data['nns_id']);
        $data['nns_asset_import_id'] = md5($data['nns_asset_import_id']);
        if(is_array($data_ex) && !empty($data_ex))
        {
            foreach ($data_ex as $key=>$val)
            {
                $data_ex[$key]['nns_cp_id']='no_drm';
                $data_ex[$key]['nns_vod_id']=$data['nns_asset_import_id'];
            }
        }
    }
    $resut_exsist = nl_query_by_db("select * from nns_vod where nns_id='{$data['nns_id']}' limit 1", $dc->db());
    if(is_array($resut_exsist))
    {
        return true;
    }
    $sql_vod = make_insert_sql('nns_vod', $data);
    if(strlen($sql_vod) >0)
    {
        nl_execute_by_db($sql_vod, $dc->db());
    }
    $sql_vod_ex = make_insert_sql('nns_vod_ex', $data_ex,false);
    if(strlen($sql_vod_ex) >0)
    {
        nl_execute_by_db($sql_vod_ex, $dc->db());
    }
    $queue_task_model = new queue_task_model();
    $queue_task_model->q_add_task_op_mgtv($data['nns_id'], 'video', 'add');
    unset($queue_task_model);
    return true;
}

function index_data_in($dc,$data,$data_ex=null,$flag=true)
{
    if($flag)
    {
        $data['nns_cp_id'] = '0';
        $data['nns_name'] = "DRM加密-".$data['nns_name'];
    }
    else
    {
        $data['nns_cp_id'] = 'no_drm';
        $data['nns_name'] = "清流-".$data['nns_name'];
        $data['nns_id'] = md5($data['nns_id']);
        $data['nns_vod_id'] = md5($data['nns_vod_id']);
        $data['nns_import_id'] = md5($data['nns_import_id']);
        if(is_array($data_ex) && !empty($data_ex))
        {
            foreach ($data_ex as $key=>$val)
            {
                $data_ex[$key]['nns_cp_id']='no_drm';
                $data_ex[$key]['nns_vod_index_id']=$data['nns_import_id'];
            }
        }
    }
    $resut_exsist = nl_query_by_db("select * from nns_vod_index where nns_id='{$data['nns_id']}' limit 1", $dc->db());
    if(is_array($resut_exsist))
    {
        return true;
    }
    $sql_vod = make_insert_sql('nns_vod_index', $data);
    if(strlen($sql_vod) >0)
    {
        nl_execute_by_db($sql_vod, $dc->db());
    }
    $sql_vod_ex = make_insert_sql('nns_vod_index_ex', $data_ex,false);
    if(strlen($sql_vod_ex) >0)
    {
        nl_execute_by_db($sql_vod_ex, $dc->db());
    }
    $queue_task_model = new queue_task_model();
    $queue_task_model->q_add_task_op_mgtv($data['nns_id'], 'index', 'add');
    unset($queue_task_model);
    return true;
}

function media_data_in($dc,$data,$data_ex=null,$flag=true)
{
    $data['nns_tag'] = '26,27,28,29,30,31,';
    if($flag)
    {
        $arr_drm_params = array();
        $str_temp = MD5($data['nns_id']."_".rand(0,9999)."_1");
        $arr_drm_params[$str_temp] = MD5($str_temp);
        $data['nns_cp_id'] = '0';
        $data['nns_drm_enabled'] = '1';
        $data['nns_drm_ext_info'] = json_encode($arr_drm_params);
        $data['nns_drm_encrypt_solution'] = 'marlin';
    }
    else
    {
        $data['nns_id'] = md5($data['nns_id']);
        $data['nns_vod_id'] = md5($data['nns_vod_id']);
        $data['nns_vod_index_id'] = md5($data['nns_vod_index_id']);
        $data['nns_import_id'] = md5($data['nns_import_id']);
        $data['nns_cp_id'] = 'no_drm';
        $data['nns_drm_enabled'] = '0';
        $data['nns_drm_ext_info'] = '';
        $data['nns_drm_encrypt_solution'] = '';
        if(is_array($data_ex) && !empty($data_ex))
        {
            foreach ($data_ex as $key=>$val)
            {
                $data_ex[$key]['nns_cp_id']='no_drm';
                $data_ex[$key]['nns_vod_media_id']=$data['nns_import_id'];
            }
        }
    }
    $resut_exsist = nl_query_by_db("select * from nns_vod_media where nns_id='{$data['nns_id']}' limit 1", $dc->db());
    if(is_array($resut_exsist))
    {
        return true;    
    }
    $sql_vod = make_insert_sql('nns_vod_media', $data);
    if(strlen($sql_vod) >0)
    {
        nl_execute_by_db($sql_vod, $dc->db());
    }
    $sql_vod_ex = make_insert_sql('nns_vod_media_ex', $data_ex,false);
    if(strlen($sql_vod_ex) >0)
    {
        nl_execute_by_db($sql_vod_ex, $dc->db());
    }
    $queue_task_model = new queue_task_model();
    $queue_task_model->q_add_task_op_mgtv($data['nns_id'], 'media', 'add');
    unset($queue_task_model);
    return true;
}
