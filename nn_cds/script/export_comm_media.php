<?php
/**
 * 删除重复注入的 媒资包 分集 片源
 */
header("Content-type:text/html;charset=utf-8");
include '../nn_logic/nl_common.func.php';
$dc_r = nl_get_dc(array(
		'db_policy' => NL_DB_READ,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$db_r =$dc_r->db();
#先查询重复注入的片源
$sql_media_comm="select temp.nns_import_id from (
			select count(nns_import_id) as num,nns_import_id 
			from nns_vod_media GROUP BY nns_import_id) as temp where temp.num>1 limit 50";
$result_media_comm = nl_query_by_db($sql_media_comm, $db_r);
if(!$result_media_comm)
{
	echo "db error ,query comm media sql:".$sql_media_comm;
}
$filename="片源".date('Ymd').'.csv';
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=". $filename);
header('Cache-Control: max-age=0');
print(chr(0xEF).chr(0xBB).chr(0xBF));
ob_start();
$fp = fopen('php://output', 'a');
fputcsv($fp, array('片源','片源id','名称','原因'));
if(is_array($result_media_comm) && !empty($result_media_comm))
{
	foreach ($result_media_comm as $val)
	{
		$sql_query_media = "select nns_id,nns_vod_id,nns_vod_index_id from nns_vod_media where nns_import_id='{$val['nns_import_id']}'";
		$result_query_media = nl_query_by_db($sql_query_media, $db_r);
		if(!$result_query_media)
		{
			echo "db error ,query every media sql:".$sql_query_media;
		}
		if(is_array($result_query_media))
		{
			$sql_query_index ="select count(*) as num from nns_vod_index where nns_id='{$_v['nns_vod_index_id']}'";
			$result_query_index = nl_query_by_db($sql_query_index, $db_r);
			if(!$result_query_index || !is_array($result_query_index))
			{
				echo "db error ,query media index sql:".$sql_query_index;
				exit;
			}
			$sql_query_vod ="select count(*) as num from nns_vod where nns_id='{$_v['nns_vod_id']}'";
			$result_query_vod = nl_query_by_db($sql_query_vod, $db_r);
			if(!$result_query_vod || !is_array($result_query_vod))
			{
				echo "db error ,query media vod sql:".$sql_query_vod;
				exit;
			}
			if($result_query_index[0]['num'] <= 0)
			{
				fputcsv($fp, array(' ',$_v['nns_id'],' ','no index'));
			}
			if($result_query_vod[0]['num'] <= 0)
			{
				fputcsv($fp, array(' ',$_v['nns_id'],' ','no vod'));
			}
		}
	}
}
