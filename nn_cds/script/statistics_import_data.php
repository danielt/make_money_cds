<?php
header("Content-type:text/html;charset=utf-8");
include '../nn_logic/nl_common.func.php';
$dc = nl_get_dc(array(
		'db_policy' => NL_DB_READ,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));

$array = array(
		'video'=>array(
				'table'=>'nns_vod',
				'name'=>'主媒资'
		),
		'index'=>array(
				'table'=>'nns_vod_index',
				'name'=>'分集'
		),
		'media'=>array(
				'table'=>'nns_vod_media',
				'name'=>'片源'
		),
);
$ex_data = array();
foreach ($array as $key=>$val)
{
	$sql="select count(*) as count,cp.nns_name,t1.nns_deleted from " . $val['table'] . " as t1 left join nns_cp as cp on cp.nns_id=t1.nns_cp_id group by t1.nns_cp_id,t1.nns_deleted ";
	$result_count = nl_query_by_db($sql, $dc->db());
	if(is_array($result_count))
	{
		foreach ($result_count as $count_val)
		{
			$count_val['nns_name'] = strlen($count_val['nns_name']) >0 ? $count_val['nns_name'] : '未知CP';
			$count_val['nns_deleted'] = $count_val['nns_deleted'] == 1 ? '已删除' : '未删除';
			$ex_data['资源库存量'][$val['name']][$count_val['nns_name']][$count_val['nns_deleted']] = isset($ex_data['资源库存量'][$val['name']][$count_val['nns_name']][$count_val['nns_deleted']]) ? $ex_data['资源库存量'][$val['name']][$count_val['nns_name']][$count_val['nns_deleted']]+$count_val['count'] : $count_val['count'];
		}
	}
	$sql = "select count(*) as count,sp.nns_name,task.nns_action,task.nns_epg_status from nns_mgtvbk_c2_task as task left join nns_mgtvbk_sp as sp on sp.nns_id=task.nns_org_id " .
	 " where task.nns_type='{$key}' group by task.nns_org_id,task.nns_action,task.nns_epg_status";
	$result_count = nl_query_by_db($sql, $dc->db());
	if(is_array($result_count))
	{
		foreach ($result_count as $count_val)
		{
			$count_val['nns_epg_status'] = $count_val['nns_epg_status']=='99' ? '注入成功' : '等待注入';
			
			$count_val['nns_name'] = strlen($count_val['nns_name']) >0 ? $count_val['nns_name'] : '未知SP';
			$count_val['nns_action'] = $count_val['nns_action'] == 'destroy' ? 'EPG删除' : 'EPG添加';
			$ex_data['EPG注入'][$val['name']][$count_val['nns_name']][$count_val['nns_action']][$count_val['nns_epg_status']] = isset($ex_data['EPG注入'][$val['name']][$count_val['nns_name']][$count_val['nns_action']][$count_val['nns_epg_status']]) ? $ex_data['EPG注入'][$val['name']][$count_val['nns_name']][$count_val['nns_action']][$count_val['nns_epg_status']]+$count_val['count'] : $count_val['count'];
		}
	}
	
	$sql = "select count(*) as count,sp.nns_name,task.nns_action,task.nns_status from nns_mgtvbk_c2_task as task left join nns_mgtvbk_sp as sp on sp.nns_id=task.nns_org_id " .
		 " where task.nns_type='{$key}' group by task.nns_org_id,task.nns_action,task.nns_status";
	$result_count = nl_query_by_db($sql, $dc->db());
	if(is_array($result_count))
	{
		foreach ($result_count as $count_val)
		{
			if($count_val['nns_status'] == '0')
			{
				$count_val['nns_status'] = '注入成功';
			}
			else if($count_val['nns_status'] == '-1')
			{
				$count_val['nns_status'] = '切片失败';
			}
			else
			{
				$count_val['nns_status'] = '处理中';
			}
				
			$count_val['nns_name'] = strlen($count_val['nns_name']) >0 ? $count_val['nns_name'] : '未知SP';
			$count_val['nns_action'] = $count_val['nns_action'] == 'destroy' ? 'EPG删除' : 'EPG添加';
			$ex_data['CDN注入'][$val['name']][$count_val['nns_name']][$count_val['nns_action']][$count_val['nns_status']] = isset($ex_data['CDN注入'][$val['name']][$count_val['nns_name']][$count_val['nns_action']][$count_val['nns_status']]) ? $ex_data['CDN注入'][$val['name']][$count_val['nns_name']][$count_val['nns_action']][$count_val['nns_status']]+$count_val['count'] : $count_val['count'];
		}
	}
	
	
	if($key == 'media')
	{
		$sql = "select count(*) as count,sp.nns_name,task.nns_state from nns_mgtvbk_clip_task as task left join nns_mgtvbk_sp as sp on sp.nns_id=task.nns_org_id group by task.nns_org_id,task.nns_state";
		$result_count = nl_query_by_db($sql, $dc->db());
		if(is_array($result_count))
		{
			foreach ($result_count as $count_val)
			{
				if($count_val['nns_state'] == 'c2_ok')
				{
					$count_val['nns_state'] = '注入成功';
				}
				else if($count_val['nns_state'] == 'download_fail')
				{
					$count_val['nns_state'] = '下载失败';
				}
				else if($count_val['nns_state'] == 'clip_fail')
				{
					$count_val['nns_state'] = '切片失败';
				}
				else
				{
					$count_val['nns_state'] = '处理中';
				}
				$count_val['nns_name'] = strlen($count_val['nns_name']) >0 ? $count_val['nns_name'] : '未知SP';
				$count_val['nns_action'] = $count_val['nns_action'] == 'destroy' ? 'EPG删除' : 'EPG添加';
				$ex_data['切片'][$val['name']][$count_val['nns_name']][$count_val['nns_state']] = isset($ex_data['切片'][$val['name']][$count_val['nns_name']][$count_val['nns_state']]) ? $ex_data['切片'][$val['name']][$count_val['nns_name']][$count_val['nns_state']]+$count_val['count'] : $count_val['count'];
			}
		}
	}
	
}




echo json_encode($ex_data);
