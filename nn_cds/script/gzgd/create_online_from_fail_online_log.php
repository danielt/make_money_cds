<?php
/**
 * 从上下线队列日志中把上下线日志中的重新生成上下线队列
 * Created by IntelliJ IDEA.
 * User: LZ
 * Date: 2017/8/31
 * Time: 15:36
 */

include_once dirname(dirname(__FILE__)) . '/nn_logic/nl_common.func.php';
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_READ | NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$sql = "select * from nns_import_asset_online_log where nns_status=3 and nns_reason='该媒资还未注入EPG' and nns_create_time > '2017-07-27' group by nns_import_id";
$result = nl_query_by_db($sql,$dc->db());

foreach ($result as $val)
{
    //先根据上下线GUID查询是否存在
    $check_sql = "select nns_id from nns_import_asset_online where nns_id = '{$val['nns_asset_online_id']}'";
    $check_re = nl_query_by_db($check_sql,$dc->db());
    if(is_array($check_re))
    {
        continue;
    }
    $xml = $val['nns_import_content'];
    $xml_data = simplexml_load_string($xml);
    $con_xml = file_get_contents($xml_data->url);
    if(!$con_xml)
    {
        continue;
    }
    $content = simplexml_load_string($con_xml);
    $order = $content->content->order;
    $insert_sql = "INSERT INTO `nns_import_asset_online` (`nns_id`, `nns_message_id`, `nns_video_name`, `nns_import_id`, `nns_category_id`, `nns_order`, `nns_action`, `nns_cp_id`, `nns_org_id`, `nns_status`, `nns_audit`, `nns_create_time`, `nns_modify_time`, `nns_fail_time`, `nns_delete`, `nns_vod_id`, `nns_type`, `nns_asset_id`) VALUES
('{$val['nns_asset_online_id']}','{$val['nns_message_id']}','{$val['nns_video_name']}','{$val['nns_import_id']}','{$val['nns_category_id']}','{$order}','{$val['nns_action']}','tz_cp','{$val['nns_org_id']}',	1,	1,	now(),	now(),	0,	0,	'{$val['nns_vod_id']}',	'video',	'')";
    nl_execute_by_db($insert_sql,$dc->db());
}
