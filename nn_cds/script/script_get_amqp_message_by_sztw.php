<?php 
/**
 * script  测试获取amqp的数据
 * @author liangpan
 * @date 2015-05-04
 */
header("Content-type:text/html;charset=utf-8");
set_time_limit(0);
date_default_timezone_set('Asia/Chongqing');
include_once dirname(__FILE__)."/common/common.php";
define("MODULE_NAME", 'script_get_amqp_message_by_sztw');
define("ORG_ID", 'sztw');
$date_time=date("YmdHis");
nn_echo_ex_msg("-------------------script开始取出amqp的数据-------------------",MODULE_NAME,$date_time);
global $g_mgtv_amqp_name_config;
if(!isset($g_mgtv_amqp_name_config[ORG_ID]['channel_name']) || !isset($g_mgtv_amqp_name_config[ORG_ID]['exchange_name']) || !isset($g_mgtv_amqp_name_config[ORG_ID]['route_name']))
{
	nn_echo_ex_msg("没有配置g_mgtv_amqp_name_config参数为：".var_export($g_mgtv_amqp_name_config,true),MODULE_NAME,$date_time);
	unset($g_mgtv_amqp_name_config);
	nn_echo_ex_msg("-------------------script取出amqp的数据结束-------------------",MODULE_NAME,$date_time);exit;
}
global $g_mgtv_amqp_host_config;
if(!isset($g_mgtv_amqp_host_config) || !is_array($g_mgtv_amqp_host_config) || empty($g_mgtv_amqp_host_config))
{
	nn_echo_ex_msg("没有配置g_mgtv_amqp_host_config参数为：".var_export($g_mgtv_amqp_host_config,true),MODULE_NAME,$date_time);
	unset($g_mgtv_amqp_name_config);
	nn_echo_ex_msg("-------------------script取出amqp的数据结束-------------------",MODULE_NAME,$date_time);exit;
}
$amqp_host_config = $g_mgtv_amqp_host_config;
$amqp_name_config = $g_mgtv_amqp_name_config[ORG_ID];
unset($g_mgtv_amqp_name_config);
unset($g_mgtv_amqp_name_config);


$conn = new AMQPConnection($amqp_host_config);
$conn->connect();
if (!$conn)
{
	nn_echo_ex_msg("列队服务器不能访问",MODULE_NAME,$date_time);
	nn_echo_ex_msg("-------------------script取出amqp的数据结束-------------------",MODULE_NAME,$date_time);
}
$channel = new AMQPChannel($conn);
$q_name = $amqp_name_config['channel_name'];
//队列名
$e_name = $amqp_name_config['exchange_name'];
//交换机名[目前未用到]

$q = new AMQPQueue($channel);
$q->setName($q_name);
$q->setFlags(AMQP_DURABLE);

	
$ex = new AMQPExchange($channel);
$ex->setName($e_name);
//		//创建名字
$ex->setType(AMQP_EX_TYPE_DIRECT);


$ex->setFlags(AMQP_DURABLE | AMQP_AUTODELETE);
nn_echo_ex_msg('-------------------begin queue connect-------------------',MODULE_NAME,$date_time);


$queue_now_num = 1;
while (($messages = $q->get()))
{
	if ($messages===FALSE || $messages === null)
	{
		break;
	}
	# 获取XML
	$xml = htmlspecialchars_decode($messages->getBody(), ENT_QUOTES);
	nn_echo_ex_msg("获取的第{$queue_now_num}条数据xml为：".var_export($xml,true),MODULE_NAME,$date_time);
	$q->ack($messages->getDeliveryTag());
	$queue_now_num++;
}
nn_echo_ex_msg('-------------------end queue connect-------------------',MODULE_NAME,$date_time);
nn_echo_ex_msg("-------------------script取出amqp的数据结束-------------------",MODULE_NAME,$date_time);
