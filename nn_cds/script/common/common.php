<?php
include_once dirname(dirname(dirname(__FILE__))).'/nn_cms_config/child_config/nn_cms_config_mgtv_fjyd.php';
/**
  * 写入日志
  * @return true写入成功 false 写入失败
  * @param $msg 信息
  * @author liangpan
  * @date 2014-7-21
  */
function nn_echo_ex_msg($msg, $module,$date_time)
{
	$date = date("Ymd");
	if (empty($msg))
	{
		return false;
	}
	echo date('Y-m-d H:i:s') . "   " . $msg . "\r\n<br/>";
	//写入日志文件
	$str_cms_dir = dirname(dirname(dirname(__FILE__))) . '/';
	$str_log_dir = $str_cms_dir . 'data/log/nn_script/' . $module . '/' . $date . "/";
	$str_log_file = $date_time . '.txt';
	$str_start_time = "[" . date("Y-m-d H:i:s") . "]";
	if (!is_dir($str_log_dir))
	{
		mkdir($str_log_dir, 0777, true);
	}
	file_put_contents($str_log_dir . $str_log_file, $str_start_time . $msg . "\r\n", FILE_APPEND);
	return true;
}
