<?php
header("Content-Type:text/html;charset=utf-8");
include_once dirname(dirname(dirname(__FILE__)))."/nn_logic/nl_common.func.php";
set_time_limit(0);
ini_set('memory_limit', '2048M');
$argv = $_GET;
$database = $argv[1]; //播控固定录入cms
$table_name = $argv[2]; //表名
$filename = $argv[3];
$limit = empty($argv[4])?500:$argv[3];
$begin = empty($argv[5])?0:$argv[4];
var_dump($argv);
msg("==========================check data to hash table begin===================");
if (empty($database) || empty($table_name) )
{
	msg("argv is not empty");
}


script($database,$table_name,$filename,$limit,$begin);

function script($database,$table_name,$filename,$limit,$begin)
{

	if ($database == "cms")
	{
		$func_name = "nl_get_dc";
	}else
	{
		$func_name = "nl_get_{$database}_dc";
	}

	if (!defined('g_db_hash_table_rules'))
	{
		msg("rules is empty");
		return;
	}

	if (!function_exists($func_name))
	{
		msg($func_name." is not exsists");
		return;
	}

	$dc1 = $func_name(array('db_policy'=>NL_DB_WRITE));

	$dc2 = $func_name(array('db_policy'=>NL_DB_WRITE),array('db_type'=>NP_DB_TYPE_HASH_TABLE_MYSQL,'db_hash_table_modes'=>array()));

	//$begin = 0;
	//$limit = 500;

	$sql_builder = new np_sql_builder($table_name);

	 $sql = " show columns from ".$table_name;
     $table_field_maps = nl_query_by_db($sql,$dc2->db());
     unset($sql_builder);
     // var_dump($table_field_maps);die;

	while(true)
    {
		$sql_builder = new np_sql_builder($table_name);
		$sql_1 = $sql_builder->limit($begin,$limit)->query();

		$data = nl_query_by_db($sql_1,$dc1->db());
		if ($data === true)
		{
			unset($sql_builder,$data);
			break;
		}
		$ids = "";
		// foreach ($data as $item){
		// 	$ids .= "'{$item["nns_id"]}',";
		// }
		// $ids = trim($ids,",");
		// echo $ids; die;



		$where = array();
		foreach ($data as $key=>&$item)
		{
			$where[] = $item[$filename];
		}


		$sql_2 = $sql_builder->where(array($filename=>$where))->query();
		//echo $sql_2;
		$result = nl_query_by_db($sql_2,$dc2->db()); 

		if ($result === false)
		{
			unset($sql_builder,$data);
			msg($sql_2." execute fail![db error]".$dc2->db()->last_error_desc());
			break;
		}

		$begin += $limit;

		//$data = np_array_rekey($data,$filename);
		$result = np_array_rekey($result,$filename);
		// var_dump($result);

		foreach($data as $key=>$item)
		{
			if (!is_array($result[$item[$filename]]))
			{
				msg($key." is empty\n");
			}
			$re = array_diff($result[$item[$filename]],$item);
			if (!empty($re))
			{
				msg($key." is not match!\n".var_export($re,true));
			}
		}
		
		msg("{$begin} is complete\n");
		unset($sql_builder,$data,$result);
	}
}

    /**
     * 日志记录方法
     */
    function msg($msg)
    {
        if(empty($msg))
        {
            return false;
        }
        echo date('Y-m-d H:i:s') . "   " . $msg . "\r\n<br/>";
        //写入日志文件
        $str_cms_dir = dirname(dirname(dirname(__FILE__))) . '/';
        $str_log_dir = $str_cms_dir . 'data/log/nn_script/script_move_data_to_hash_table/' . date("Ymd") . "/";
        $str_log_file = date("H") . '.txt';
        $str_start_time = "[" . date("Y-m-d H:i:s") . "]";
        if(!is_dir($str_log_dir))
        {
            mkdir($str_log_dir, 0777, true);
        }
        file_put_contents($str_log_dir . $str_log_file, $str_start_time . $msg . "\r\n", FILE_APPEND);
    }
