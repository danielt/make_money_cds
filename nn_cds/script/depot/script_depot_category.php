<?php
/**
 * 获取资源库栏目信息到新版资源库中
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/12/5 11:36
 */
set_time_limit(0);
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
header("Content-type:text/html; charset=utf-8");
$nncms_config_path = dirname(dirname(dirname(__FILE__)))."/";
$np_path = dirname(dirname(dirname(dirname(__FILE__))))."/";
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/depot/depot.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/depot/depot_v2.class.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_cms_manager/nncms_mlang.php';
include_once dirname(dirname(dirname(__FILE__))) . '/nn_cms_config/nn_cms_global.php';

class script_depot_category
{
    private $dc;
    private $language;

    public function __construct()
    {
        //数据库对象
        $this->dc  = nl_get_dc(
            array(
                'db_policy' => NL_DB_WRITE,
                'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
            )
        );
    }

    public function action()
    {
        $language_types = g_cms_config::get_g_extend_language();
        $language_types = trim($language_types);
        if (empty($language_types))
        {
            $language_types = array ();
        }
        else
        {
            $language_types = explode("|", $language_types);
        }
        $this->language = $language_types;
        $sql = "SELECT * FROM nns_depot";
        $result = nl_query_by_db($sql, $this->dc->db());
        if(is_array($result) && !empty($result))
        {
            foreach ($result as $depot)
            {
                if(!empty($depot['nns_category']))
                {
                    $dom_child = new DOMDocument('1.0', 'utf-8');
                    $dom_child->loadXML($depot['nns_category']);
                    $this->get_xml_category_arr($dom_child->firstChild, $depot['nns_id'],$depot['nns_org_id'],$depot['nns_type']);
                }
            }
        }
        echo "执行结束";
    }

    /**
     * 资源库的添加修改操作
     * @Author   HanWen.Liang
     * @DateTime 2017-06-12T14:46:08+0800
     * @return   无返回值
     */
    public function modify($params)
    {
        $where = array(
            'nns_category_id'=> $params['category_id'],
            'nns_import_category_id'=> $params['import_category_id'],
            'nns_import_parent_category_id'=> $params['import_parent_category_id'],
            'nns_depot_id'   => $params['depot_id'],
            'nns_type'       => $params['type'],  //点播直播
        );

        $result = \nl_depot_v2::query($this->dc, $where, 'nns_id');
        if(is_array($result['data_info']) && isset($result['data_info']['nns_id']))
        {
            //修改
            $category_id  = $params['category_id'];
            $depot_id     = $params['depot_id'];
            $type         = $params['type'];

            $st = nl_depot_v2::edit($this->dc, $category_id, $depot_id, $type, $params,false);
            if($st['ret'] != 0)
            {
                $str_sql = isset($st['sql'])?$st['sql']:"";
                echo $st['reason'].":\r\n name:".$params['name'].",category_id:".$params['category_id'].",type:".$params['type'].",sql:".$str_sql."\r\n";
            }
        }
        else
        {//添加
            //先检查是否存在
            $st = nl_depot_v2::add($this->dc, $params);
            if($st['ret'] != 0)
            {
                $str_sql = isset($st['sql'])?$st['sql']:"";
                // echo $st['reason']."\r\n<br>sql:".$st['sql']."<br/>";
                echo $st['reason'].":\r\n name:".$params['name'].",category_id:".$params['category_id'].",type:".$params['type'].",sql:".$str_sql."\r\n";
            }
        }
        return ;
    }

    /**
     * 获取资源库子栏目
     * @Author   HanWen.Liang
     * @DateTime 2017-06-12T15:21:02+0800
     * @param    xml节点      $dom_node   资源库子栏目
     * @param    string       $depot_id   资源库ID
     * @param    string       $org_id     媒资来源ID
     * @param    int          $type       媒资类型
     * @return   null/true    如果不存在子栏目，则返回空。否则返回true,
     */
    private function get_xml_category_arr($dom_node, $depot_id,$org_id,$type, $parent_name='')
    {
        $nodes = $dom_node->childNodes;
        $nodes = $dom_node->childNodes;
        if ($nodes->length == 0)
            return NULL;

        $child_arr = array();
        $loop = 1;//栏目排序下标
        foreach ($nodes as $node)
        {
            if($node->nodeType !== XML_ELEMENT_NODE)
            {
                continue;
            }
            $add_params = array(
                'name'                      => $node->getAttribute('name'),
                'parent_name'              => $parent_name,
                'category_id'               => $node->getAttribute('id'),
                //这里的parent直接从xml里面取，导致资源库栏目根栏目和一级栏目的parent_id都为0
                'parent_id'                 => $node->getAttribute('parent'),
                'import_category_id'        => $node->getAttribute('import_id'),
                'import_parent_category_id' => $node->getAttribute('parent_import_id'),
                'depot_id'                  => $depot_id,
                'org_id'                    => $org_id,
                'order'                     => $loop,
                'type'                      => $type,
            );
            $this->modify($add_params);
            //子栏目
            $this->get_xml_category_arr($node, $depot_id,$org_id,$type, $node->getAttribute('name'));
            $loop ++;
        }

        return true;
    }
}

$obj = new script_depot_category();
$obj->action();