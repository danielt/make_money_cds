<?php
set_time_limit(0);
header("Content-type:text/html;charset=utf-8");
include_once dirname(dirname(__FILE__)) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/models/queue_task_model.php';
define('XIAOLE_MOIVE_IMPORT_SP', 'gdgd_xiaole');
$dc = nl_get_dc(array(
		'db_policy' => NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$int_limit = '30';
function make_insert_sql($table,$params)
{
    $field_str = '';
    $value_str = '';
    if (is_array($params) && !empty($params))
    {
        if(isset($params[0]) && !empty($params[0]) && is_array($params[0]))
        {
            foreach ($params as $k=>$v)
            {
                if(!is_array($v) || empty($v))
                {
                    continue;
                }
                foreach ($v as $_k=>$_v)
                {
                    if($k == 0)
                    {
                        if (strpos($_k, 'nns_') === FALSE)
                        {
                            $_k = 'nns_' . $_k;
                        }
                        $field_str .= $_k . ',';
                    }
                    $value_str .= "'$_v',";
                }
                $value_str = rtrim($value_str, ',');
                $value_str.="),(";
            }
            $value_str = rtrim($value_str,'),(');
        }
        else
        {
            foreach ($params as $k => $v)
            {
                if (strpos($k, 'nns_') === FALSE)
                {
                    $k = 'nns_' . $k;
                }
                $field_str .= $k . ',';
                $value_str .= "'$v',";
            }
        }
    }
    $field_str = rtrim($field_str, ',');
    $value_str = rtrim($value_str, ',');
    $sql = "insert into $table ($field_str) values($value_str)";
    return $sql;
}
$last_exclude_vod = $last_data = null;
$sql_vod="select * from nns_vod where nns_cp_id='fuse_xiaole' and nns_deleted !='1' ".
    " and nns_id not in(select nns_video_id from nns_mgtvbk_op_queue where nns_org_id='".XIAOLE_MOIVE_IMPORT_SP."' and nns_type='video') ".
    " and nns_id not in(select nns_ref_id from nns_mgtvbk_c2_task where nns_org_id='".XIAOLE_MOIVE_IMPORT_SP."' and nns_type='video') ".
    " and nns_id not in(select nns_video_id from  nns_exclude_vod where nns_sp_id='".XIAOLE_MOIVE_IMPORT_SP."') limit {$int_limit}";
$result_vod = nl_query_by_db($sql_vod,$dc->db());
if(is_array($result_vod) && !empty($result_vod))
{
    foreach ($result_vod as $xiaole_vod_key=>$xiaole_vod_val)
    {
        $temp_arr = array(
            'video'=>$xiaole_vod_val['nns_id'],
        );
        $sql_index="select * from nns_vod_index where nns_vod_id='{$xiaole_vod_val['nns_id']}' and nns_cp_id='fuse_xiaole' and nns_deleted !='1' group by nns_index order by nns_index asc";
        $result_index = nl_query_by_db($sql_index,$dc->db());
        if(!$result_index || empty($result_index) || !is_array($result_index))
        {
            $last_exclude_vod[]=array(
                'nns_video_id'=>$xiaole_vod_val['nns_id'],
                'nns_sp_id'=>XIAOLE_MOIVE_IMPORT_SP,
            );
            continue;
        }
        $temp_arr['index'] = array();
        $temp_arr['media'] = array();
        foreach ($result_index as $xiaole_index_key=>$xiaole_index_val)
        {
            $temp_arr['index'][] = $xiaole_index_val['nns_id'];
            $sql_media="select * from nns_vod_media where nns_vod_id='{$xiaole_vod_val['nns_id']}' and nns_vod_index_id='{$xiaole_index_val['nns_id']}' and nns_cp_id='fuse_xiaole' ".
            " and nns_deleted !='1' and nns_encode_flag='0' and nns_url like 'cvideo/%' and (nns_content_id='' or isnull(nns_content_id)) and (nns_media_service='' or isnull(nns_media_service)) and nns_kbps in('2300','2301') order by nns_kbps desc,nns_create_time desc limit 1";
            $result_media = nl_query_by_db($sql_media,$dc->db());
            if(!$result_media || empty($result_media) || !is_array($result_media))
            {
                break;
            }
            $temp_arr['media'][$xiaole_index_val['nns_id']] = $result_media[0]['nns_id'];
            unset($result_media);
        }
        unset($result_index);
        if(count($temp_arr['index']) != count($temp_arr['media']))
        {
            $last_exclude_vod[]=array(
                'nns_video_id'=>$xiaole_vod_val['nns_id'],
                'nns_sp_id'=>XIAOLE_MOIVE_IMPORT_SP,
            );
            continue;
        }
        if(count($temp_arr['index']) != $xiaole_vod_val['nns_all_index'])
        {
            $last_exclude_vod[]=array(
                'nns_video_id'=>$xiaole_vod_val['nns_id'],
                'nns_sp_id'=>XIAOLE_MOIVE_IMPORT_SP,
            );
            continue;
        }
        $last_data[] = $temp_arr;
    }
    unset($result_vod);
}
$last_exclude_vod = $last_data = null;

$sql_vod="select * from nns_vod where nns_cp_id='xiaole_encode' and nns_deleted !='1' ".
    " and nns_id not in(select nns_video_id from nns_mgtvbk_op_queue where nns_org_id='".XIAOLE_MOIVE_IMPORT_SP."' and nns_type='video') ".
    " and nns_id not in(select nns_ref_id from nns_mgtvbk_c2_task where nns_org_id='".XIAOLE_MOIVE_IMPORT_SP."' and nns_type='video') ".
    " and nns_id not in(select nns_video_id from  nns_exclude_vod where nns_sp_id='".XIAOLE_MOIVE_IMPORT_SP."') limit {$int_limit}";
$result_vod = nl_query_by_db($sql_vod,$dc->db());
if(is_array($result_vod) && !empty($result_vod))
{
    foreach ($result_vod as $xiaole_vod_key=>$xiaole_vod_val)
    {
        $temp_arr = array(
            'video'=>$xiaole_vod_val['nns_id'],
        );
        $sql_index="select * from nns_vod_index where nns_vod_id='{$xiaole_vod_val['nns_id']}' and nns_cp_id='xiaole_encode' and nns_deleted !='1' group by nns_index order by nns_index asc";
        $result_index = nl_query_by_db($sql_index,$dc->db());
        if(!$result_index || empty($result_index) || !is_array($result_index))
        {
            $last_exclude_vod[]=array(
                'nns_video_id'=>$xiaole_vod_val['nns_id'],
                'nns_sp_id'=>XIAOLE_MOIVE_IMPORT_SP,
            );
            continue;
        }
        $temp_arr['index'] = array();
        $temp_arr['media'] = array();
        foreach ($result_index as $xiaole_index_key=>$xiaole_index_val)
        {
            $temp_arr['index'][] = $xiaole_index_val['nns_id'];
            $sql_media="select * from nns_vod_media where nns_vod_id='{$xiaole_vod_val['nns_id']}' and nns_vod_index_id='{$xiaole_index_val['nns_id']}' and nns_cp_id='xiaole_encode' ".
                " and nns_deleted !='1' and (nns_ext_info !='' or nns_ext_info IS NOT NULL) order by nns_kbps desc,nns_create_time desc limit 1";
            $result_media = nl_query_by_db($sql_media,$dc->db());
            if(!$result_media || empty($result_media) || !is_array($result_media))
            {
                break;
            }
            $nns_ext_info = json_decode($result_media[0]['nns_ext_info'],true);
            if(!isset($nns_ext_info['path']) || strlen($nns_ext_info['path']) <1)
            {
                break;
            }
            $temp_arr['media'][$xiaole_index_val['nns_id']] = $result_media[0]['nns_id'];
            unset($result_media);
        }
        unset($result_index);
        if(count($temp_arr['index']) != count($temp_arr['media']))
        {
            $last_exclude_vod[]=array(
                'nns_video_id'=>$xiaole_vod_val['nns_id'],
                'nns_sp_id'=>XIAOLE_MOIVE_IMPORT_SP,
            );
            continue;
        }
        if(count($temp_arr['index']) != $xiaole_vod_val['nns_all_index'])
        {
            $last_exclude_vod[]=array(
                'nns_video_id'=>$xiaole_vod_val['nns_id'],
                'nns_sp_id'=>XIAOLE_MOIVE_IMPORT_SP,
            );
            continue;
        }
        $last_data[] = $temp_arr;
    }
    unset($result_vod);
}
// $sql_vod="select * from nns_vod where nns_cp_id='xiaole_mp3' and nns_deleted !='1' ".
//     " and nns_id not in(select nns_video_id from nns_mgtvbk_op_queue where nns_org_id='".XIAOLE_MOIVE_IMPORT_SP."' and nns_type='video') ".
//     " and nns_id not in(select nns_ref_id from nns_mgtvbk_c2_task where nns_org_id='".XIAOLE_MOIVE_IMPORT_SP."' and nns_type='video') ".
//     " and nns_id not in(select nns_video_id from nns_exclude_vod where nns_sp_id='".XIAOLE_MOIVE_IMPORT_SP."') limit {$int_limit}";
// $result_vod = nl_query_by_db($sql_vod,$dc->db());
// if(is_array($result_vod) && !empty($result_vod))
// {
//     foreach ($result_vod as $xiaole_vod_key=>$xiaole_vod_val)
//     {
//         $temp_arr = array(
//             'video'=>$xiaole_vod_val['nns_id'],
//         );
//         $sql_index="select * from nns_vod_index where nns_vod_id='{$xiaole_vod_val['nns_id']}' and nns_cp_id='xiaole_mp3' and nns_deleted !='1' group by nns_index order by nns_index asc";
//         $result_index = nl_query_by_db($sql_index,$dc->db());
//         if(!$result_index || empty($result_index) || !is_array($result_index))
//         {
//             $last_exclude_vod[]=array(
//                 'nns_video_id'=>$xiaole_vod_val['nns_id'],
//                 'nns_sp_id'=>XIAOLE_MOIVE_IMPORT_SP,
//             );
//             continue;
//         }
//         $temp_arr['index'] = array();
//         $temp_arr['media'] = array();
//         foreach ($result_index as $xiaole_index_key=>$xiaole_index_val)
//         {
//             $temp_arr['index'][] = $xiaole_index_val['nns_id'];
//             $sql_media="select * from nns_vod_media where nns_vod_id='{$xiaole_vod_val['nns_id']}' and nns_vod_index_id='{$xiaole_index_val['nns_id']}' and nns_cp_id='xiaole_mp3' ".
//                 " and nns_deleted !='1' and (nns_ext_info !='' or nns_ext_inf IS NOT NULL) order by nns_kbps desc,nns_create_time desc limit 1";
//             $result_media = nl_query_by_db($sql_media,$dc->db());
//             if(!$result_media || empty($result_media) || !is_array($result_media))
//             {
//                 break;
//             }
//             $nns_ext_info = json_decode($result_media[0]['nns_ext_info'],true);
//             if(!isset($nns_ext_info['path']) || strlen($nns_ext_info['path']) <1)
//             {
//                 break;
//             }
//             $temp_arr['media'][$xiaole_index_val['nns_id']] = $result_media[0]['nns_id'];
//             unset($result_media);
//         }
//         unset($result_index);
//         if(count($temp_arr['index']) != count($temp_arr['media']))
//         {
//             $last_exclude_vod[]=array(
//                 'nns_video_id'=>$xiaole_vod_val['nns_id'],
//                 'nns_sp_id'=>XIAOLE_MOIVE_IMPORT_SP,
//             );
//             continue;
//         }
//         if(count($temp_arr['index']) != $xiaole_vod_val['nns_all_index'])
//         {
//             $last_exclude_vod[]=array(
//                 'nns_video_id'=>$xiaole_vod_val['nns_id'],
//                 'nns_sp_id'=>XIAOLE_MOIVE_IMPORT_SP,
//             );
//             continue;
//         }
//         $last_data[] = $temp_arr;
//     }
//     unset($result_vod);
// }
if(!is_array($last_data) || empty($last_data))
{
    die();
}
$i=0;
foreach ($last_data as $val)
{
    if(!isset($val['media']) || !is_array($val['media']) || !isset($val['video']) || empty($val['video']))
    {
        continue;
    }
    $i++;
    $queue_task_model = new queue_task_model();
//     $queue_task_model->q_add_task($val['video'], 'video', 'add',XIAOLE_MOIVE_IMPORT_SP,null,true);
    foreach ($val['media'] as $index_value=>$media_value)
    {
//         $queue_task_model->q_add_task($index_value, 'index', 'add',XIAOLE_MOIVE_IMPORT_SP,null,true);
        $queue_task_model->q_add_task($media_value, 'media', 'add',XIAOLE_MOIVE_IMPORT_SP,null,true);
        $i++;
        $i++;
    }
    $sql_del="delete from nns_exclude_vod where nns_video_id='{$val['video']}' and nns_sp_id='".XIAOLE_MOIVE_IMPORT_SP."'";
    nl_execute_by_db($sql_del, $dc->db());
    unset($queue_task_model);
}
if(is_array($last_exclude_vod) && !empty($last_exclude_vod))
{
    foreach ($last_exclude_vod as $last_exclude_vod_val)
    {
        $sql_insert = make_insert_sql('nns_exclude_vod',$last_exclude_vod_val);
        nl_execute_by_db($sql_insert, $dc->db());
    }
}
var_dump($i);