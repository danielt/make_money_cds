<?php
header("Content-type: text/html; charset=utf-8");
ini_set('display_errors', 1);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/models/queue_task_model.php';
$db = nn_get_db(NL_DB_WRITE);
$bool = true;
$i = 0;


$limit = 500;
$queue_task_model = new queue_task_model();
while ($bool) {

	$clip_sql = "select * from nns_mgtvbk_clip_task where nns_state!='c2_ok' and nns_state!='ok'   order by nns_create_time asc limit $i,$limit";
	$infos = nl_db_get_all($clip_sql, $db);
	
	//error_log(var_export($infos,true),3,'update.log');
	unset($clip_sql);
	if ($infos===true) {
		$bool = false;
		break;
	}
	//查看是那个SP  查看是混合模式还是单独模式
	foreach ($infos as $info) {
		$sp_id = $info['nns_org_id'];
		$sp_config = sp_model::get_sp_config($sp_id);
		
		//单独模式
		$cdn_media_mode = false;
		if (isset($sp_config['cdn_import_media_mode']) && (int)$sp_config['cdn_import_media_mode'] === 1) {
			//混合模式	
			$cdn_media_mode = true;
		}
		//error_log(var_export($sp_config,true),3,'config.log');		
		//查询这个分集下所有的片源		
		$sql_media = "select * from nns_vod_media where nns_vod_id='{$info['nns_video_id']}' and nns_vod_index_id='{$info['nns_video_index_id']}' and nns_deleted!=1 order by nns_kbps desc ";		
		$media_infos = nl_db_get_all($sql_media, $db);
		
		error_log(var_export($media_infos,true).$sql_media,3,'media.log');
		if(count($media_infos)==0){
			continue;
		}
		//error_log(var_export($media_infos,true).$sql_media,3,'media.log');
		//切片成功的任务将数据库规整到任务列队中，并将路径文件的路径计算出来，写入c2_path 
		if($info['nns_state']!='ok'){
			if($cdn_media_mode===false){			
				foreach ($media_infos as $media_info) {
					//如果是单独模式   比如思华  则片源要每个写入
					$queue_task_model->q_add_task($media_info['nns_id'],BK_OP_MEDIA,BK_OP_ADD,$sp_id);
				}	
			}elseif($cdn_media_mode===true){
				//如果是混合模式 就只添加一个
				$queue_task_model->q_add_task($media_infos[0]['nns_id'],BK_OP_MEDIA,BK_OP_ADD,$sp_id);
			}		
		}
		unset($sp_id,$sp_config,$sql_media,$media_infos);
	}
	unset($infos);
	
	//$bool = false;
	$i+=$limit;	
	
}
//nl_execute_by_db("delete from nns_mgtvbk_clip_task where  nns_state!='c2_ok'  and  nns_state!='ok'", $db);
unset($queue_task_model,$db);
?>