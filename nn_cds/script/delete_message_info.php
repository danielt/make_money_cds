<?php
header("Content-type:text/html;charset=utf-8");
if(isset($_POST['nns_op']) && $_POST['nns_op'] == 'delete')
{
	if(empty($_POST['nns_type']))
	{
		echo "<script>alert('影片类型为空');</script>";
		exit;
	}
	if(empty($_POST['nns_action']))
	{
		echo "<script>alert('操作行为为空');</script>";
		exit;
	}
	if(empty($_POST['nns_state']))
	{
		echo "<script>alert('队列状态为空');</script>";
		exit;
	}
	include '../nn_logic/nl_common.func.php';
	$dc = nl_get_dc(array(
			'db_policy' => NL_DB_WRITE,
			'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
	));
	$sql_delete="delete * from nns_mgtvbk_message where ";
	$str_type = implode("','", $_POST['nns_type']);
	$str_action = implode("','", $_POST['nns_action']);
	$str_state = implode("','", $_POST['nns_state']);
	$sql_delete.= " nns_type in ('{$str_type}')  and nns_action in ('{$str_action}') and nns_message_state in ('{$str_state}') ";
	$result=nl_execute_by_db($sql_delete, $dc->db());
	if(!$result)
	{
		echo "<script>alert('操作成功！');</script>";
		exit;
	}
	echo "<script>alert('操作失败！');</script>";
	exit;
}
else
{
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>批量删除message信息</title>
		<script src="./../nn_cms_manager/js/jquery-1.9.1.min.js"></script>
    </head>
    <body>
		<form action="./delete_message_info.php" method="post">
		<input type='hidden' name='nns_op' value='delete'>
			<table>
				<tr>
					<td>请选择影片类型：</td>
					<td>
						<input  type='checkbox' name='nns_type[]' value="1">主媒资</input>
						<input  type='checkbox' name='nns_type[]' value="2">分集</input>
						<input  type='checkbox' name='nns_type[]' value="3">片源</input>
					</td>
				</tr>
				<tr>
					<td>请选择影片操作行为：</td>
					<td>
						<input  type='checkbox' name='nns_action[]' value="1">添加</input>
						<input  type='checkbox' name='nns_action[]' value="2">修改</input>
						<input  type='checkbox' name='nns_action[]' value="3">删除</input>
					</td>
				</tr>
				<tr>
					<td>请选择队列状态：</td>
					<td>
						<input   type='checkbox' name='nns_state[]' value="0">等待注入</input>
						<input   type='checkbox' name='nns_state[]' value="1">FTP连接失败</input>
						<input   type='checkbox' name='nns_state[]' value="2">XML获取失败</input>
						<input   type='checkbox' name='nns_state[]' value="3">注入成功</input>
						<input   type='checkbox' name='nns_state[]' value="4">注入失败</input>
					</td>
				</tr>
			</table>
		</form>
		<button id="submit_sql">提交</button>
    </body>
</html>
<script type="text/javascript" charset="utf-8">
	$('#submit_sql').click(function(){
		var str_type;
		var str_action;
		var str_state;
		var flag = false;
		var sql_delete = 'delete * from nns_mgtvbk_message where ';
		str_type = return_value('nns_type[]');
		if(str_type === false)
		{
			alert("影片类型未选择！");
			exit;
		}
		str_action = return_value('nns_action[]');
		if(str_action === false)
		{
			alert("操作行为未选择！");
			exit;
		}
		str_state = return_value('nns_state[]');
		if(str_state === false)
		{
			alert("队列状态未选择！");
			exit;
		}
		sql_delete +=' nns_type in ' + str_type + ' and nns_action in ' + str_action + ' and nns_message_state in ' + str_state;
		if(confirm('操作的sql语句为：'+sql_delete))
		{
			$("form").submit();
		}
	});
	function return_value(name)
	{
		var str="";
		$('input[name="'+name+'"]:checked').each(function(){
			str+=$(this).val()+"','";
		});
		if(str.length == 0)
		{
			return false;
		}
		str = str.slice(0,-2);  
		str="('"+str+")";
		return str;
	}
</script>
<?php
}
?>