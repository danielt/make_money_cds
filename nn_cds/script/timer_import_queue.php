<?php
header("Content-type:text/html;charset=utf-8");
set_time_limit(0);
include '../nn_logic/nl_common.func.php';
$arr_type = array(
    'video'=>array(
        'table'=>'nns_vod',
        'sql'=>'',
        'flag'=>false,
    ),
    'index'=>array(
        'table'=>'nns_vod_index',
        'sql'=>"select * from nns_vod_index where nns_cp_id='aiqiyi' ".
                " and nns_id not in(select nns_video_id from nns_mgtvbk_import_op where nns_video_type='index') ".
                " and nns_id not in(select nns_index_id from nns_mgtvbk_op_queue where nns_type='index' and nns_org_id='internet') ".
                " and nns_id not in(select nns_ref_id from nns_mgtvbk_c2_task where nns_org_id='internet' and nns_type='index') limit 5000",
        'flag'=>true,
    ),
    'media'=>array(
        'table'=>'nns_vod_media',
        'sql'=>"select * from nns_vod_media where nns_cp_id='aiqiyi' ".
                " and nns_id not in(select nns_video_id from nns_mgtvbk_import_op where nns_video_type='media') ".
                " and nns_id not in(select nns_media_id from nns_mgtvbk_op_queue where nns_type='media' and nns_org_id='internet') ".
                " and nns_id not in(select nns_ref_id from nns_mgtvbk_c2_task where nns_org_id='internet' and nns_type='media') limit 5000",
        'flag'=>false,
    ),
    'live'=>array(
        'table'=>'nns_live',
        'sql'=>'',
        'flag'=>false,
    ),
    'live_index'=>array(
        'table'=>'nns_live_index',
        'sql'=>'',
        'flag'=>false,
    ),
    'live_media'=>array(
        'table'=>'nns_live_media',
        'sql'=>'',
        'flag'=>false,
    ),
    'playbill'=>array(
        'table'=>'nns_live_playbill_item',
        'sql'=>'',
        'flag'=>false,
    ),
    'file'=>array(
        'table'=>'nns_file_package',
        'sql'=>'',
        'flag'=>false,
    ),
    'product'=>array(
        'table'=>'nns_product',
        'sql'=>'',
        'flag'=>false,
    ),
);
include dirname(dirname(__FILE__)).'/mgtv_v2/models/queue_task_model.php';
foreach ($arr_type as $type=>$value)
{
    if(!isset($value['table']) || !isset($value['sql']) || !isset($value['flag']))
    {
        continue;
    }
    $table = trim($value['table']);
    $sql=trim($value['sql']);
    if(strlen($table) <1 || strlen($sql)<1)
    {
        continue;
    }
    $flag = $value['flag'] ? true : false;
    if(!$flag)
    {
        continue;
    }
    if(!preg_match("/^select\s+\*\s+from\s+{$table}\s+where\s+.*+/i", $sql))
    {
        echo "SQL 非法";
        continue;
    }
    $dc = nl_get_dc(array(
        'db_policy' => NL_DB_READ,
        'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
    ));
    $result = nl_query_by_db($sql, $dc->db());
    if(is_array($result))
    {
        foreach ($result as $val)
        {
            $queue_task_model = new queue_task_model();
            $queue_task_model->q_add_task_op_mgtv($val['nns_id'], $type, 'add','',true,null);
            unset($queue_task_model);
        }
    }
}