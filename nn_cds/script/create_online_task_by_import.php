<?php
/**
 * 爱上使用
 */
header("Content-type:text/html;charset=utf-8");
set_time_limit(0);
if(isset($_POST['action']) && !empty($_POST['import_id']))
{
	include_once '../nn_logic/nl_common.func.php';
	include_once '../api/common.php';
	include_once '../api/cntv_me/cntv_sync_source.php';
	$sp_id = $_POST['sp_id'];

	$_POST['import_id'] = trim($_POST['import_id'],',');
	$import_id = explode(',', $_POST['import_id']);
	if(empty($import_id) || count($import_id) > 200)
	{
		echo "<script>alert('无效操作！');</script>";
		exit;
	}	
	$dc = nl_get_dc(array(
			'db_policy' => NL_DB_WRITE | NL_DB_READ,
			'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
	));
	$cntv_sync_source = new cntv_sync_source();
	foreach ($import_id as $value)
	{
		if(strlen($value) > 32)
		{
			continue;
		}
		$sql = "select a.nns_message_id,a.nns_message_content from nns_mgtvbk_message as a left join nns_import_asset_online_log as l on a.nns_message_id=l.nns_message_id where l.nns_import_id='{$value}' and l.nns_status=2 and l.nns_org_id='{$sp_id}' order by l.nns_create_time desc limit 1";

		$re = nl_query_by_db($sql, $dc->db());
		if(!is_array($re))
		{
			$cntv_sync_source->parse_xml($re[0]['nns_message_content'],$re[0]['nns_message_id']);
		}
		unset($sql);
		unset($re);
	}
	if(empty($import_id))
	{
		echo "<script>alert('操作完成');</script>";
		exit;
	}	
}
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>根据注入ID重新生成已下发的上下线消息</title>
		<script src="./../nn_cms_manager/js/jquery-1.9.1.min.js"></script>
    </head>
    <body>
		<form action="./create_online_task_by_import.php" method="post">
		<input type="hidden" name="action" value="" />
			<table>
				<tr>
					<td>SPID:<input type="text" name="sp_id" value="cntv_me"/></td>
				</tr>
				<tr>
					<td>注入ID(一次性至多填入200个,多个以逗号分隔;确保在填入前注入ID在上下线日志中已存在并获取的是最新的上下线成功的日志)</td>
				</tr>
				<tr>
					<td>
						<textarea rows="8" cols="50" name="import_id"></textarea>
					</td>
				</tr>
			</table>
			<input type="submit" value="提交"/>
		</form>		
    </body>
</html>