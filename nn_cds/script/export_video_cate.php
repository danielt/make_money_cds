<?php
/**
 * 导出媒资数据
 */
header("Content-type:text/html;charset=utf-8");
include '../nn_logic/nl_common.func.php';
$dc_r = nl_get_dc(array(
		'db_policy' => NL_DB_READ,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$db_r =$dc_r->db();
#先查询重复注入的主媒资
$sql_vod="select nns_cp_id,nns_name,nns_pinyin,nns_director,nns_actor,nns_show_time,nns_all_index,nns_new_index,nns_area,nns_summary,nns_remark,nns_category_id,nns_language,nns_keyword,nns_kind from nns_vod";
$result_vod = nl_query_by_db($sql_vod, $db_r);
if(!$result_vod)
{
	echo "db error ,query vod sql:".$result_vod;die;
}
if(empty($result_vod) || !is_array($result_vod))
{
    echo "db error ,query vod empty sql:".$result_vod;die;
}
$sql_cate = "select * from nns_depot where nns_type='0'";
$result_cate= nl_query_by_db($sql_cate, $db_r);
if(!$result_cate)
{
    echo "db error ,query cate sql:".$sql_cate;die;
}
if(empty($result_cate[0]) || !is_array($result_vod[0]))
{
    echo "db error ,query vod empty sql:".$result_cate;die;
}
$result_cate = $result_cate[0];


$sql_cp = "select * from nns_cp";
$result_cp= nl_query_by_db($sql_cp, $db_r);
if(!$result_cp)
{
    echo "db error ,query cate sql:".$sql_cate;die;
}
if(empty($result_cp) || !is_array($result_cp))
{
    echo "db error ,query vod empty sql:".$sql_cp;die;
}
$arr_cp = array();
foreach ($result_cp as $cp_val)
{
    $arr_cp[$cp_val['nns_id']] = $cp_val['nns_name'];
}

//加载xml
$dom = new DOMDocument('1.0', 'utf-8');
$dom->loadXML($result_cate['nns_category']);
$category_id_arr = array();
$categorys = $dom->getElementsByTagName('category');

foreach ($categorys as $category)
{
    $id = $category->getAttribute('id');
    $category_id_arr[$id] = $category->getAttribute('name');
}



$filename="media_".date('Ymd').'.csv';
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=". $filename);
header('Cache-Control: max-age=0');
print(chr(0xEF).chr(0xBB).chr(0xBF));
ob_start();
$fp = fopen('php://output', 'a');
fputcsv($fp, array('内容提供商','影片名称','拼音检索','导演','演员','上映时间','总集数','最新集数','上映地区','描述','标记','栏目','语言','关键词','种类'));
if(is_array($result_vod) && !empty($result_vod))
{
	foreach ($result_vod as $val)
	{
		$val['nns_category_id'] = isset($category_id_arr[$val['nns_category_id']]) ? $category_id_arr[$val['nns_category_id']] : '未分类栏目';
		$val['nns_cp_id'] = isset($arr_cp[$val['nns_cp_id']]) ? $arr_cp[$val['nns_cp_id']] : '未知提供商';
		fputcsv($fp, array_values($val));
	}
}
