<?php 
header("Content-type:text/html;charset=utf-8");
include '../nn_logic/nl_common.func.php';
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$sql_queue="SELECT * FROM `nns_mgtvbk_op_queue` WHERE `nns_org_id` = 'JSWS' and `nns_type` = 'media'  and nns_ex_data <>''";
$result_queue = nl_query_by_db($sql_queue, $dc->db());
if(is_array($result_queue) && !empty($result_queue))
{
    foreach ($result_queue as $queue_val)
    {
        if(strlen($queue_val['nns_ex_data']) <1)
        {
            continue;
        }
        $flag = false;
        if(!isset($queue_val['nns_ex_data']) || strlen($queue_val['nns_ex_data'])<1 || !is_json($queue_val['nns_ex_data']))
        {
            continue;
        }
        $temp_ex_data = json_decode($queue_val['nns_ex_data'],true);
        $type = strrpos($temp_ex_data['url'], 'manual_import');
        if($type === 0)
        {
            continue;
        }
        elseif ($type === false)
        {
            $temp_ex_data['url'] = $temp_ex_data['path'] = 'manual_import/'.$temp_ex_data['url'];
            $sql_update = "update nns_mgtvbk_op_queue set nns_ex_data='".json_encode($temp_ex_data)."' where nns_id='{$queue_val['nns_id']}'";
            nl_execute_by_db($sql_update, $dc->db());
        }
        else
        {
            $arr_url = explode('manual_import/', $temp_ex_data['url']);
            $temp_ex_data['url'] = $temp_ex_data['path'] = 'manual_import/'.$arr_url[0];
            $sql_update = "update nns_mgtvbk_op_queue set nns_ex_data='".json_encode($temp_ex_data)."' where nns_id='{$queue_val['nns_id']}'";
            nl_execute_by_db($sql_update, $dc->db());
        }
    }
}


$sql_media="SELECT * FROM `nns_vod_media` WHERE `nns_cp_id` = 'manual_import_pc' and nns_ext_info <>''";


$result_media = nl_query_by_db($sql_media, $dc->db());
if(is_array($result_media) && !empty($result_media))
{
    foreach ($result_media as $media_val)
    {
        if(strlen($media_val['nns_ext_info']) <1)
        {
            continue;
        }
        $flag = false;
        if(!isset($media_val['nns_ext_info']) || strlen($media_val['nns_ext_info'])<1 || !is_json($media_val['nns_ext_info']))
        {
            continue;
        }
        $temp_ex_data = json_decode($media_val['nns_ext_info'],true);
        $type = strrpos($temp_ex_data['url'], 'manual_import');
        if($type === 0)
        {
            continue;
        }
        elseif ($type === false)
        {
            $temp_ex_data['url'] = $temp_ex_data['path'] = 'manual_import/'.$temp_ex_data['url'];
            $sql_update = "update nns_vod_media set nns_ext_info='".json_encode($temp_ex_data)."' where nns_id='{$media_val['nns_id']}'";
            nl_execute_by_db($sql_update, $dc->db());
        }
        else
        {
            $arr_url = explode('manual_import/', $temp_ex_data['url']);
            $temp_ex_data['url'] = $temp_ex_data['path'] = 'manual_import/'.$arr_url[0];
            $sql_update = "update nns_vod_media set nns_ext_info='".json_encode($temp_ex_data)."' where nns_id='{$media_val['nns_id']}'";
            nl_execute_by_db($sql_update, $dc->db());
        }
    }
}

function is_json($string)
{
    if(!is_string($string))
    {
        return false;
    }
    $string = strlen($string) <1 ? '' : $string;
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}