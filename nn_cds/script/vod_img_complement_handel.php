<?php
ini_set('display_errors', 7);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . "/nn_cms_db/nns_common/nns_db_constant.php";
include_once dirname(dirname(__FILE__)) . "/nn_cms_db/nns_vod/nns_db_vod_class.php";
include_once dirname(dirname(__FILE__)) . '/api_v2/control/public_sync_source.php';
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(__FILE__)) . '/api_v2/nn_const.php';
include_once dirname(dirname(__FILE__)) . '/api_v2/common.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(__FILE__))  . '/nn_logic/nl_log_v2.func.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/cp/cp.class.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/message/nl_message.class.php';
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/models/import_model.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/depot/depot.class.php';
include_once dirname(dirname(__FILE__)) . '/api_v2/control/standard/sync_source.php';
include_once dirname(dirname(__FILE__)) . '/nn_cms_manager/service/asset_import/import_assets_proc.class.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/video/vod.class.php';
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE | NL_DB_READ,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$sql="select * from nns_vod where nns_cp_id in('youku')";
$result = nl_query_by_db($sql, $dc->db());
if(!is_array($result) || empty($result))
{
    var_dump($result,$sql);die;
}
$result_depot = nl_depot::get_depot_by_id($dc, 'de59c7d03429f2e947ba44ad91e3eb92');
if($result_depot['ret'] !=0)
{
    echo "<script>alert('" . $result_depot['reason'] . "');</script>";
    echo "<script language=javascript>history.go(-1);</script>";die;
}
$nns_category = isset($result_depot['data_info']['nns_category']) ? $result_depot['data_info']['nns_category'] : '';
if(strlen($nns_category) <1)
{
    echo "<script>alert('栏目查询为空');</script>";
    echo "<script language=javascript>history.go(-1);</script>";die;
}
//加载xml
$dom = new DOMDocument('1.0', 'utf-8');
$dom->loadXML($nns_category);
$category_id_arr = array();
$categorys = $dom->getElementsByTagName('category');
$arr_cate = array();
foreach ($categorys as $category)
{
    $arr_cate[(string)$category->getAttribute('id')] = $category->getAttribute('name');
}
include_once dirname(dirname(__FILE__)) . '/api_v2/control/standard/sync_source.php';
$temp_arr_data = array();
$vod_complement_flag = true;
// echo json_encode($result);
foreach ($result as $val)
{
    if(!isset($arr_cate[$val['nns_category_id']]))
    {
        continue;
    }
    $content = array(
            'Name'=>$val['nns_name'],
            'CategoryName'=>$arr_cate[$val['nns_category_id']],
            'ReleaseYear'=>$val['nns_show_time'],
            'Kind'=>$val['nns_kind'],
            'SearchName'=>$val['nns_name'],
            'DirectorDisplay'=>$val['nns_director'],
            'AliasName'=>$val['nns_alias_name'],
            'EnglishName'=>$val['nns_eng_name'],
            'ActorDisplay'=>$val['nns_actor'],
            'Tags'=>$val['nns_tag'],
            'OriginalCountry'=>$val['nns_area'],
            'VolumnCount'=>$val['nns_all_index'],
            'bigpic'=>$val['nns_image0'],
            'square_img'=>$val['nns_image_s'],
            'smallpic'=>$val['nns_image2'],
            'middlepic'=>$val['nns_image1'],
            'verticality_img'=>$val['nns_image_v'],
            'horizontal_img'=>$val['nns_image_h'],
            'Description'=>$val['nns_summary'],
            'WriterDisplay'=>'',
            'Language'=>$val['nns_language'],
            'Duration'=>$val['nns_view_len'],
        );
        $result_cp = nl_cp::query_by_id($dc, $val['nns_cp_id']);
        if(isset($temp_arr_data[$val['nns_cp_id']]))
        {
            if(empty($temp_arr_data[$val['nns_cp_id']]) || !is_array($temp_arr_data[$val['nns_cp_id']]))
            {
                continue;
            }
        }
        else
        {
            if($result_cp['ret'] !=0 || !isset($result_cp['data_info']) || !is_array($result_cp['data_info']) || empty($result_cp['data_info']))
            {
                continue;
            }
            $temp_arr_data[$val['nns_cp_id']]['nns_name'] = $result_cp['data_info']['nns_name'];
            $temp_arr_data[$val['nns_cp_id']]['nns_config'] = (isset($result_cp['data_info']['nns_config']) && strlen($result_cp['data_info']['nns_config']) >0) ? json_decode($result_cp['data_info']['nns_config'],true) : null; 
        }
        $obj_sync = new standard_sync_source(array('cp_id'=>$val['nns_cp_id'],'cp_name'=>$temp_arr_data[$val['nns_cp_id']]['nns_name']),$temp_arr_data[$val['nns_cp_id']]['nns_config'],'',$val['nns_cp_id']);
        $obj_sync->import->download_img_enabled = false;
        $obj_sync->no_need_change_info = $vod_complement_flag;
        $re = $obj_sync->modify_asset($content,$val['nns_asset_import_id']);
        unset($obj_sync);
}