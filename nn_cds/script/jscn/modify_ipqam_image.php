<?php
header("Content-type:text/html;charset=utf-8");
include_once dirname(dirname(dirname(__FILE__)))."/v2/common.php";
\ns_core\m_load::load("ns_core.m_config");
include_once dirname(dirname(dirname(__FILE__))).'/mgtv_v2/models/queue_task_model.php';
define('IMAGE_URL_BASE','/data/image/imgs');
function check_file_exsist($url='')
{
    $url=trim($url);
    if(strlen($url) <1)
    {
        return false;
    }
    if(file_exists($url))
    {
        $image_info = @getimagesize($url);
        if(!is_array($image_info) || empty($image_info))
        {
            return false;
        }
        $width = (isset($image_info['0']) && (int)$image_info['0'] >0) ?  (int)$image_info['0']  : 0;
        $height = (isset($image_info['1']) && (int)$image_info['1'] >0) ?  (int)$image_info['1']  : 0;
        if($width >0 && $height >0)
        {
            return array('width'=>$width,'height'=>$height);
        }
        return false;
    }
    return false;
}

function cp_file_to_url($img_from)
{
    $str_date = date("Ymd");
    $str_time = date("H");
    $temp_img_pathinfo = pathinfo($img_from);
    $temp_img_extension = (isset($temp_img_pathinfo['extension']) && strlen($temp_img_pathinfo['extension']) > 0) ? strtolower($temp_img_pathinfo['extension']) : 'jpg';
    $save_name = np_guid_rand() . '.' . $temp_img_extension;
    $str_path = "/downimg/prev/KsImg/cp_cds_ipqam/video/{$str_date}/{$str_time}/{$save_name}";
    $result_make_dir = m_config::make_dir($str_path);
    if($result_make_dir['ret'] !=0)
    {
        return false;
    }
    if(!isset($result_make_dir['data_info']['absolute_dir']) || strlen($result_make_dir['data_info']['absolute_dir']) <1)
    {
        return false;
    }
    $result = @copy($img_from,$result_make_dir['data_info']['absolute_dir']);
    if(!$result)
    {
        return false;
    }
    $result_make_dir['data_info']['base_dir'] = substr($result_make_dir['data_info']['base_dir'], 8);
    return $result_make_dir['data_info'];
}
$sql="select * from nns_temp_media_compare where nns_url !='' and nns_url is not null limit 200";

$result = nl_query_by_db($sql, m_config::get_dc()->db());
if(!is_array($result) || empty($result))
{
    die('no data');
}
foreach ($result as $value)
{
    $sql_vod="select vod.nns_id from nns_vod_media as media left join nns_vod as vod on vod.nns_id=media.nns_vod_id where media.nns_cp_id='cds_ipqam' and nns_content_id='{$value['nns_import_id']}'";
    $result_vod = nl_query_by_db($sql_vod, m_config::get_dc()->db());
    if(!is_array($result_vod) || empty($result_vod) || !$result_vod)
    {
        nl_execute_by_db("delete from nns_temp_media_compare where nns_import_id='{$value['nns_import_id']}'", m_config::get_dc()->db());
        continue;
    }
    $value['nns_id'] = $result_vod[0]['nns_id'];
    $value['nns_url'] = trim(ltrim(ltrim($value['nns_url'],'/'),'\\'));
    if(strlen($value['nns_url']) <1)
    {
        nl_execute_by_db("delete from nns_temp_media_compare where nns_import_id='{$value['nns_import_id']}'", m_config::get_dc()->db());
        continue;
    }
    $value['nns_url'] = IMAGE_URL_BASE.'/'.$value['nns_url'];
    $check = check_file_exsist($value['nns_url']);
    if(!is_array($check) || empty($check))
    {
        nl_execute_by_db("delete from nns_temp_media_compare where nns_import_id='{$value['nns_import_id']}'", m_config::get_dc()->db());
        continue;
    }
    $bili = ceil(($check['width'] / $check['height']) * 10000);
    $modify_data = array();
    $image_filed = '';
    if($bili>11000)
    {
        $image_filed='nns_image_h';
    }
    else if($bili<9000)
    {
        $image_filed='nns_image_v';
    }
    else
    {
        $image_filed='nns_image_s';
    }
    $result_cp = cp_file_to_url($value['nns_url']);
    if(is_bool($result_cp) || !is_array($result_cp) || empty($result_cp) || !isset($result_cp['base_dir']) || strlen($result_cp['base_dir']) <1)
    {
//         nl_execute_by_db("delete from nns_temp_media_compare where nns_import_id='{$value['nns_import_id']}'", m_config::get_dc()->db());
        continue;
    }
    $sql_update="update nns_vod set {$image_filed}='{$result_cp['base_dir']}' where nns_id='{$value['nns_id']}'";
    $exec = nl_execute_by_db($sql_update, m_config::get_dc()->db());
    if($exec)
    {
        $queue_task_model = new queue_task_model();
        $queue_task_model->q_add_task_op_mgtv($value['nns_id'], 'video', 'modify','',true,null);
        unset($queue_task_model);
    }
    nl_execute_by_db("delete from nns_temp_media_compare where nns_import_id='{$value['nns_import_id']}'", m_config::get_dc()->db());
}