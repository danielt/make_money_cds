<?php
/**
 * 复制c2_task数据到其他sp
 */
header("Content-type:text/html;charset=utf-8");
include_once dirname(dirname(dirname(__FILE__)))."/v2/common.php";
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/c2_task/c2_task.class.php';
include_once dirname(dirname(__FILE__)).'/nn_logic/sp/sp.class.php';
\ns_core\m_load::load("ns_core.m_config");
class copy_c2_task_data_to_another
{
    public $del_num = 10000;
    public $select_num = 2000;
    
    
    public function insert_into_compare($org_sp_id,$des_sp_id)
    {
        $sql_count = "select count(*) as count from nns_mgtvbk_c2_task_compare where nns_src_sp_id='{$org_sp_id}' and nns_des_sp_id='{$des_sp_id}'";
        $result_count = nl_query_by_db($sql_count, m_config::get_dc()->db());
        if(isset($result_count[0]['count']) && $result_count[0]['count'] >0)
        {
            $num = ceil($result_count[0]['count']/$this->del_num);
            if($num >0)
            {
                for ($i=0;$i<$num;$i++)
                {
                    $sql_clear = "delete from nns_mgtvbk_c2_task_compare where nns_src_sp_id='{$org_sp_id}' and nns_des_sp_id='{$des_sp_id}' limit {$this->del_num}";
                    $result = nl_execute_by_db($sql_clear, m_config::get_dc()->db());
                    if(!$result)
                    {
                        return m_config::return_data(1,'SQL 非法'.$sql_clear);
                    }
                }
            }
        }
        $sql_clear = 'delete from nns_mgtvbk_c2_task_compare';
        $result = nl_execute_by_db($sql_clear, m_config::get_dc()->db());
        if(!$result)
        {
            return m_config::return_data(1,'SQL 非法'.$sql_clear);
        }
        $sql = "INSERT into nns_mgtvbk_c2_task_compare (nns_id,nns_org_id,nns_src_sp_id,nns_des_sp_id) select md5(CONCAT(nns_id, '_', '{$des_sp_id}')) AS nns_id, nns_id AS nns_org_id,'{$org_sp_id}' as nns_src_sp_id,'{$des_sp_id}' as nns_des_sp_id FROM nns_mgtvbk_c2_task WHERE nns_org_id = '{$org_sp_id}'";
        $result = nl_execute_by_db($sql, m_config::get_dc()->db());if(!$result)
        {
            return m_config::return_data(1,'SQL 非法'.$sql);
        }
        return m_config::return_data(0,'ok');
    }
    
    public function copy($type,$org_sp_id,$des_sp_id)
    {
        $temp_value='';
        $table = 'nns_vod';
        if($type=='index')
        {
            $table = 'nns_vod_index';
        }
        else if($type=='media')
        {
            $table = 'nns_vod_media';
            $temp_value = ' and tem.nns_content_id is not null ';
        }
        $sql="SELECT task.* FROM nns_mgtvbk_c2_task_compare AS compare LEFT JOIN nns_mgtvbk_c2_task AS task ON task.nns_id = compare.nns_org_id left join {$table} as tem on task.nns_ref_id=tem.nns_id " .
            " WHERE compare.nns_id NOT IN (SELECT nns_id FROM nns_mgtvbk_c2_task WHERE and task.nns_action !='destroy' nns_org_id = '{$des_sp_id}' ) and compare.nns_src_sp_id='{$org_sp_id}' and compare.nns_des_sp_id='{$des_sp_id}' AND task.nns_type = '{$type}' and tem.nns_deleted !='1' {$temp_value} limit {$this->select_num} ";
        $result = nl_query_by_db($sql, m_config::get_dc()->db());
        if(!$result)
        {
            return m_config::return_data(1,'SQL 非法'.$sql);
        }
        if(empty($result) || !is_array($result))
        {
            return m_config::return_data(0,'查无数据');
        }
        foreach ($result as $value)
        {
            $time = date("Y-m-d H:i:s");
            $temp_value = $value;
            $temp_value['nns_id'] = md5($value['nns_id']."_".$des_sp_id);
            $temp_value['nns_status'] = '1';
            $temp_value['nns_create_time'] = $time;
            $temp_value['nns_modify_time'] = $time;
            $temp_value['nns_org_id'] = $des_sp_id;
            $temp_value['nns_op_id'] = md5($value['nns_op_id']."_".$des_sp_id);
            $temp_value['nns_message_id'] = md5($value['nns_message_id']."_".$des_sp_id);
            $temp_value['nns_epg_status'] = '97';
            $temp_value['nns_epg_fail_time'] = '0';
            $temp_value['nns_cdn_fail_time'] = '0';
            nl_c2_task::add(m_config::get_dc(), $temp_value);
        } 
        return m_config::return_data(0,'ok');
    }
}
$result_sp = nl_sp::query_by_condition(m_config::get_dc(), array('nns_state'=>0));
$result_sp = isset($result_sp['data_info']) ? $result_sp['data_info'] : null;

if(isset($_REQUEST['action']) && $_REQUEST['action']=='insert_into_compare')
{
    $org_sp_id = isset($_REQUEST['org_sp_id']) ? trim($_REQUEST['org_sp_id']) : '';
    $des_sp_id = isset($_REQUEST['des_sp_id']) ? trim($_REQUEST['des_sp_id']) : '';
    if(strlen($org_sp_id) <1)
    {
        echo "<script>alert('原始下游平台参数为空');history.go(-1);</script>";die;
    }
    if(strlen($des_sp_id) <1)
    {
        echo "<script>alert('目标下游平台参数为空');history.go(-1);</script>";die;
    }
    if($org_sp_id == $des_sp_id)
    {
        echo "<script>alert('原始下游平台与目标下游平台不能相同');history.go(-1);</script>";die;
    }
    $obj_copy_c2_task_data_to_another = new copy_c2_task_data_to_another();
    $result = $obj_copy_c2_task_data_to_another->insert_into_compare($org_sp_id,$des_sp_id);
    echo "<script>alert('".$result['reason']."');history.go(-1);</script>";die;
}
else if(isset($_REQUEST['action']) && $_REQUEST['action']=='copy')
{
    $type = isset($_REQUEST['type']) ? trim($_REQUEST['type']) : '';
    $des_sp_id = isset($_REQUEST['des_sp_id']) ? trim($_REQUEST['des_sp_id']) : '';
    if(strlen($type) <1)
    {
        echo "<script>alert('类型参数为空');history.go(-1);</script>";die;
    }
    $org_sp_id = isset($_REQUEST['org_sp_id']) ? trim($_REQUEST['org_sp_id']) : '';
    $des_sp_id = isset($_REQUEST['des_sp_id']) ? trim($_REQUEST['des_sp_id']) : '';
    if(strlen($org_sp_id) <1)
    {
        echo "<script>alert('原始下游平台参数为空');history.go(-1);</script>";die;
    }
    if(strlen($des_sp_id) <1)
    {
        echo "<script>alert('目标下游平台参数为空');history.go(-1);</script>";die;
    }
    if($org_sp_id == $des_sp_id)
    {
        echo "<script>alert('原始下游平台与目标下游平台不能相同');history.go(-1);</script>";die;
    }
    $obj_copy_c2_task_data_to_another = new copy_c2_task_data_to_another();
    $result = $obj_copy_c2_task_data_to_another->copy($type,$org_sp_id,$des_sp_id);
    echo "<script>alert('".$result['reason']."');history.go(-1);</script>";die;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>中心注入指令</title>
    </head>
    <body>
        <hr/>
        <form method="post" action="./copy_c2_task_data_to_another.php">
            <input name="action" type='hidden' value='insert_into_compare'><br/>
            <br>
                    加入C2对比表数据 脚本：<hr/><br>
                    源端下游平台:
            <select name="org_sp_id">
                <option value="">--请选择源端下游平台--</option>
                <?php if(is_array($result_sp) && !empty($result_sp)){foreach ($result_sp as $sp_value){?>
                    <option value="<?php echo $sp_value['nns_id'];?>"><?php echo $sp_value['nns_id'].' | '.$sp_value['nns_name'];?></option>
                <?php }} ?>
            </select><br/><br>
                    目的端下游平台:
            <select name="des_sp_id">
                <option value="">--请选择目的下游平台--</option>
                <?php if(is_array($result_sp) && !empty($result_sp)){foreach ($result_sp as $sp_value){?>
                    <option value="<?php echo $sp_value['nns_id'];?>"><?php echo $sp_value['nns_id'].' | '.$sp_value['nns_name'];?></option>
                <?php }} ?>
            </select><br/><br>
            <input type="submit" value="提交"/>
        </form>
        <hr/>
        <form method="post" action="./copy_c2_task_data_to_another.php">
            <input name="action" type='hidden' value='copy'><br/>
            <br>
                    复制C2表数据 脚本：<hr/><br>
                    源端下游平台:
            <select name="org_sp_id">
                <option value="">--请选择源端下游平台--</option>
                <?php if(is_array($result_sp) && !empty($result_sp)){foreach ($result_sp as $sp_value){?>
                    <option value="<?php echo $sp_value['nns_id'];?>"><?php echo $sp_value['nns_id'].' | '.$sp_value['nns_name'];?></option>
                <?php }} ?>
            </select><br/><br>
                    目的端下游平台:
            <select name="des_sp_id">
                <option value="">--请选择目的下游平台--</option>
                <?php if(is_array($result_sp) && !empty($result_sp)){foreach ($result_sp as $sp_value){?>
                    <option value="<?php echo $sp_value['nns_id'];?>"><?php echo $sp_value['nns_id'].' | '.$sp_value['nns_name'];?></option>
                <?php }} ?>
            </select><br/><br>
                    选择影片类型:
            <select name="type">
                <option value="">--请选择影片类型--</option>
                <option value="video">主媒资</option>
                <option value="index">分集</option>
                <option value="media">片源</option>
                <option value="live">直播</option>
                <option value="live_index">直播分集</option>
                <option value="live_media">直播源</option>
                <option value="playbill">节目单</option>
                <option value="file">文件</option>
                <option value="product">产品包</option>
            </select><br/><br>
            <input type="submit" value="提交"/>
        </form>
        <hr/>
    </body>
</html>
