<?php
header("Content-type:text/html;charset=utf-8");
include_once dirname(dirname(dirname(__FILE__)))."/v2/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load_old("api_v2/control/public_sync_source.php");
\ns_core\m_load::load_np("np_http_curl.php");
\ns_core\m_load::load_np("np_xml_to_array.php");
class export_asset extends public_sync_source
{
    public function __construct()
    {
        
    }
    
    public function init()
    {
        $this->assets_info_completion_enabled = true;
        global  $g_assets_info_completion_url;
        $this->assets_info_completion_url = $g_assets_info_completion_url;
        $sql_query="select * from nns_outside_assit_completion where nns_state='0' limit 10";
        $result_query = nl_query_by_db($sql_query, m_config::get_dc()->db());
        if(!is_array($result_query) || empty($result_query) || !$result_query)
        {
            return m_config::return_data(1,"sql error :[{$sql_query}]"); 
        }
        foreach ($result_query as $value)
        {
            var_dump($this->assets_info_completion_url."&name={$value['nns_name']}&type={$value['nns_type']}&year={$value['nns_year']}");
            echo "<br/>";
            $result_completion = $this->public_get_curl_completion_content(m_config::get_dc(),$value['nns_name'],$value['nns_type'],$value['nns_year']);
            var_dump($result_completion);
            echo "<hr/>";
            if($result_completion['ret'] !=0)
            {
                $sql_update ="update nns_outside_assit_completion set nns_state='2' where nns_id='{$value['nns_id']}'";
                nl_execute_by_db($sql_update, m_config::get_dc()->db());
                continue;
            }
            if(isset($result_completion['data_info']) || !is_array($result_completion['data_info']) || empty($result_completion['data_info']))
            {
                $sql_update ="update nns_outside_assit_completion set nns_state='2' where nns_id='{$value['nns_id']}'";
                nl_execute_by_db($sql_update, m_config::get_dc()->db());
                continue;
            }
            $result_completion = (isset($result_completion['data_info']) || !is_array($result_completion['data_info']) || empty($result_completion['data_info'])) ? $result_completion['data_info'] : null;
            echo json_encode($result_completion);die;
            //获取酷控媒资数据数据封装
            $content = $this->public_make_completion_content($content, $result_completion);
        }
        return m_config::return_data(0,"未处理到任何数据");
    }
    
    function export_csv($data,$cp_data)
    {
        $header = array (
            'CP/SP名称',
            '分集号',
            '文件名称',
            '文件路径',
            '文件大小',
            '文件时长',
            '文件全局格式',
            '文件码率模式',
            '视屏码率',
            '视屏格式',
            '视屏码率模式',
            '音频码率',
            '音频格式',
            '音频码率模式',
        );
        $filename=date('Ymd_His').'_medium_file.csv';
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=". $filename);
        header('Cache-Control: max-age=0');
        print(chr(0xEF).chr(0xBB).chr(0xBF));
        ob_start();
        $fp = fopen('php://output', 'a');
        if(!empty($header))
        {
            fputcsv($fp, $header);
        }
        foreach ($data as $_v)
        {
            $_v['nns_cp_id'] = isset($cp_data[$_v['nns_cp_id']]) ? $cp_data[$_v['nns_cp_id']] : '未知CP/SP';
            fputcsv($fp, array_values($_v));
        }
        unset($fp);
        unset($data);
        ob_flush();
        flush();
        return ;
    }
}
$export_asset = new export_asset();
$result = $export_asset->init();
echo json_encode($result);
