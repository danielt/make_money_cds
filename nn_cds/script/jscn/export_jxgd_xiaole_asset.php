<?php
/**
 * 删除重复注入的 媒资包 分集 片源
 */
// header("Content-type:text/html;charset=utf-8");
include_once dirname(dirname(dirname(__FILE__)))."/v2/common.php";
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/c2_task/c2_task.class.php';
\ns_core\m_load::load("ns_core.m_config");
$dc_r = nl_get_dc(array(
    'db_policy' => NL_DB_READ,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));

class export_jxgd_xiaole
{

    public $obj_dc = null;

    public $obj_cms_dc = null;

    public function __construct()
    {
        $this->obj_cms_dc = nl_get_cms_dc(array(
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ),null,'cms_epg');
        
        $this->obj_dc = nl_get_dc(array(
            'db_policy' => NL_DB_READ | NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_NULL
        ));
    }
    
    public function init()
    {
        $sql_task = "select vod.nns_id,vod.nns_cp_id,vod.nns_asset_import_id from nns_mgtvbk_c2_task as task left join nns_vod as vod on vod.nns_id=task.nns_ref_id where ".
        " task.nns_org_id='jxgd_xiaole' and task.nns_type='video' and  task.nns_status='0'";
        $result_vod = nl_query_by_db($sql_task, $this->obj_dc->db());
        if(!is_array($result_vod) || empty($result_vod))
        {
            return ;
        }
        $last_data = null;
        foreach ($result_vod as $value)
        {
            $sql="select item.nns_assist_id,item.nns_category_id from nns_vod as vod LEFT JOIN nns_assists_item as item on vod.nns_id=item.nns_video_id where vod.nns_cp_id='{$value['nns_cp_id']}' and vod.nns_asset_import_id='{$value['nns_id']}'";
//             echo $sql."<hr/>";;
            $result = nl_query_by_db($sql_task, $this->obj_cms_dc->db());
            $arr_cate = null;
            if(is_array($result))
            {
                foreach ($result as $_v)
                {
                    $arr_cate[] = $_v['nns_assist_id']."|".$_v['nns_category_id'];
                }
            }
            $last_data[] = array(
                $value['nns_id'],
                is_array($arr_cate) ? implode(";", $arr_cate) : '未上线'
            );
        }
//         echo json_encode($last_data);die;
        $this->export_csv($last_data);
    }
    
    public function export_csv($data)
    {
        $filename="主媒资".date('Ymd').'.csv';
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=". $filename);
        header('Cache-Control: max-age=0');
        print(chr(0xEF).chr(0xBB).chr(0xBF));
        ob_clean();
        ob_start();
        $fp = fopen('php://output', 'a');
        fputcsv($fp, array('主媒资ID','栏目'));
        foreach ($data as $value)
        {
            fputcsv($fp, $value);
        }
    }
}
$export_jxgd_xiaole = new export_jxgd_xiaole();
$export_jxgd_xiaole->init();