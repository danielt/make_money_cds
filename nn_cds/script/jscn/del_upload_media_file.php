<?php
/**
 * @author pan.liang
 * 删除下载的文件
 */
header("Content-type:text/html;charset=utf-8");
include_once dirname(dirname(dirname(__FILE__)))."/v2/common.php";
\ns_core\m_load::load("ns_core.m_config");
\ns_core\m_load::load_old("nn_class/ftp/ftp.class.php");
class del_upload_media_file
{
    public $arr_cp;
    
    public $str_base_dir = '/tbjy';
    
    public function __construct($arr_cp)
    {
        $this->arr_cp = $arr_cp;
    }
    
    public function init()
    {
        if(empty($this->arr_cp) || !is_array($this->arr_cp))
        {
            return m_config::return_data(1,'CP 为空');
        }
        $sql="select media.nns_url,task.nns_id from nns_vod_media as media left join nns_mgtvbk_clip_task as task on ".
        " task.nns_video_media_id=media.nns_id where task.nns_org_id='starcor_make' and task.nns_state='ok' and media.nns_cp_id in('".implode("','", $this->arr_cp)."') ".
        " and media.nns_url like 'ftp://tbjy:tbjy@172.26.2.17:21/JSCN%' limit 10000";
        $result = nl_query_by_db($sql, m_config::get_dc()->db());
        if(!is_array($result) || !$result || empty($result))
        {
            return m_config::return_data(0,'no data SQL:'.$sql);
        }
        foreach ($result as $value)
        {
            $arr_parse_url = parse_url($value['nns_url']);
            $str_path = isset($arr_parse_url['path']) ? $arr_parse_url['path'] : '';
            $str_path = trim(trim(trim($str_path,'/'),'\\'));
            if(strlen($str_path) <1)
            {
                continue;
            }
            
            $arr_path = pathinfo($str_path);
            $file_full_dir = $this->str_base_dir.'/'.$arr_path['dirname'].'/'.iconv('UTF-8','GBK',$arr_path['basename']);

            
            $file_full_dir = $this->str_base_dir.'/'.$arr_path['dirname'].'/'.$arr_path['basename'];
            
            
//             $file_full_dir=iconv('UTF-8','GBK',$file_full_dir);
//             var_dump(is_dir($this->str_base_dir.'/'.$arr_path['dirname']),file_exists($file_full_dir),$file_full_dir);
//             continue;
//             if(file_exists($file_full_dir))
//             {
                @unlink($file_full_dir);
//             }
//             $obj_ftp = new nl_ftp(null,null,null,21,30,false,$value['nns_url']);
//             $result_del = $obj_ftp->delete_file($str_path);
//             var_dump($result_del);die;
        }
        return m_config::return_data(0,'ok');
    }   
}
$obj_del_upload_media_file = new del_upload_media_file(array('JSCN_xes','JSCN_ych'));
$result = $obj_del_upload_media_file->init();
echo json_encode($result);