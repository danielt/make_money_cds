<?php 
set_time_limit(0);
header("Content-type:text/html; charset=utf-8");
include '../nn_logic/nl_common.func.php';
function export_csv($fp,$data)
{
    ob_clean();
    if(empty($data) || !is_array($data))
    {
        return ;
    }
    foreach ($data as $_k=>$_v)
    {
        if($_k == 0)
        {
            fputcsv($fp, array_keys($_v));
        }
        fputcsv($fp, array_values($_v));
    }
    unset($fp);
    unset($data);
    ob_flush();
    flush();
    return ;
}
$dc = nl_get_dc(NL_DB_READ);
$sql_vod="select * from nns_vod where nns_cp_id in('fuse_xiaole','fuse_xiaole_encode') and nns_all_index>1 ";

$result_vod = nl_query_by_db($sql_vod, $dc->db());
$filename=date('Ymd_His').'_c2_log.csv';
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=". $filename);
header('Cache-Control: max-age=0');
print(chr(0xEF).chr(0xBB).chr(0xBF));
ob_start();
$fp = fopen('php://output', 'a');
$arr_result = null;
foreach ($result_vod as $val)
{
    $sql_index="select * from nns_vod_index where nns_vod_id ='{$val['nns_id']}' group by nns_index";
    $result_index = nl_query_by_db($sql_index, $dc->db());
    if(!is_array($result_index) || empty($result_index))
    {
        $arr_result[$val['nns_id']] = array(
            'vod_id'=>$val['nns_id'],
            'name'=>$val['nns_name'],
            'index_type'=>'分集缺失',
            'index_desc'=>array('缺失所有分集'),
            'all_index'=>$val['nns_all_index'],
        );
        continue;
    }
    $sql_index="select * from nns_vod_index where nns_vod_id ='{$val['nns_id']}'";
    $result_index = nl_query_by_db($sql_index, $dc->db());
    if(!is_array($result_index) || empty($result_index))
    {
        continue;
    }
    $temp_array = range(1,$val['nns_all_index']);
    $arr__index = null;
    foreach ($result_index as $index_val)
    {
        $sql_media = "select * from nns_vod_media where nns_vod_index_id='{$index_val['nns_id']}' and nns_vod_id='{$val['nns_id']}'";
        $result_media = nl_query_by_db($sql_media, $dc->db());
        if(!is_array($result_media) || empty($result_media))
        {
            if(!isset($arr_result[$val['nns_id']]))
            {
                $arr_result[$val['nns_id']] = array(
                    'vod_id'=>$val['nns_id'],
                    'name'=>$val['nns_name'],
                    'media_type'=>'分集存在但是片源缺失',
                    'all_index'=>$val['nns_all_index'],
                );
            }
            $arr_result[$val['nns_id']]['media_desc'][] = $index_val['nns_index']+1;
        }
        $arr__index[] = $index_val['nns_index']+1;
    }
    if(empty($arr__index))
    {
        continue;
    }
    $data = array_diff($temp_array, $arr__index);
    if(!empty($data) && is_array($data))
    {
        if(!isset($arr_result[$val['nns_id']]))
        {
            $arr_result[$val['nns_id']] = array(
                'vod_id'=>$val['nns_id'],
                'name'=>$val['nns_name'],
                'index_media_type'=>'分集和片源同时缺失',
                'all_index'=>$val['nns_all_index'],
                'index_media_type'=>array_values($data),
            );
        }
        else
        {
            $arr_result[$val['nns_id']]['index_type']='分集和片源同时缺失';
            $arr_result[$val['nns_id']]['index_media_type']=array_values($data);
        }
    }
}
if(is_array($arr_result))
{
    foreach ($arr_result as $key=>$val)
    {
        foreach ($val as $_key=>$_val)
        {
            if(is_array($_val) && !empty($_val))
            {
                $arr_result[$key][$_key]=implode(",", $_val);
            }
        }
    }
    $arr_result = array_values($arr_result);
}
export_csv($fp,$arr_result);