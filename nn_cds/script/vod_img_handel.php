<?php
ini_set('display_errors', 7);
set_time_limit(0);
include_once dirname(dirname(__FILE__)) . "/nn_cms_db/nns_common/nns_db_constant.php";
include_once dirname(dirname(__FILE__)) . "/nn_cms_db/nns_vod/nns_db_vod_class.php";
include_once dirname(dirname(__FILE__)) . '/api_v2/control/public_sync_source.php';
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/mgtv_init.php';
include_once dirname(dirname(__FILE__)) . '/api_v2/nn_const.php';
include_once dirname(dirname(__FILE__)) . '/api_v2/common.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(__FILE__))  . '/nn_logic/nl_log_v2.func.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/cp/cp.class.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/message/nl_message.class.php';
include_once dirname(dirname(__FILE__)) . '/mgtv_v2/models/import_model.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/depot/depot.class.php';
include_once dirname(dirname(__FILE__)) . '/api_v2/control/standard/sync_source.php';
include_once dirname(dirname(__FILE__)) . '/nn_cms_manager/service/asset_import/import_assets_proc.class.php';
include_once dirname(dirname(__FILE__)) . '/nn_logic/video/vod.class.php';
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE | NL_DB_READ,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$sql="select * from nns_vod where nns_cp_id in('youku')";
$sql_cp="select * from nns_cp where nns_id in(select nns_cp_id from nns_vod group by nns_cp_id)";
$result = nl_query_by_db($sql, $dc->db());
$result_cp = nl_query_by_db($sql_cp, $dc->db());
if(!is_array($result) || empty($result))
{
    var_dump($result,$sql);die;
}
if(!is_array($result_cp) || empty($result_cp))
{
    var_dump($result_cp,$sql_cp);die;
}
$cp_config = null;
foreach ($result_cp as $cp_val)
{
    if(strlen($cp_val['nns_config']) >0)
    {
        $cp_config[$cp_val['nns_id']] = json_decode($cp_val['nns_config'],true);
    }
}
global $g_image_host;
$image_host = (isset($g_image_host) && strlen($g_image_host) >0) ? trim(trim($g_image_host,'/'),'\\') : null;
unset($g_image_host);
if(strlen($image_host) <1)
{
    echo "g_image_host 未在配置文件配置";die;
}
$img_arr = array(
    'nns_image0',
    'nns_image1',
    'nns_image2',
    'nns_image3',
    'nns_image4',
    'nns_image5',
    'nns_image_v',
    'nns_image_s',
    'nns_image_h',
);

var_dump($result);echo "<hr>";
foreach ($result as $val)
{
    if(!isset($cp_config[$val['nns_cp_id']]) || empty($cp_config[$val['nns_cp_id']]) || !is_array($cp_config[$val['nns_cp_id']]))
    {
        continue;
    }
    $import_assets_class = new import_assets_class($cp_config[$val['nns_cp_id']]);
    $import_assets_class->cp_id = $val['nns_cp_id'];
    $edit_img = $temp_img = null;
    foreach ($img_arr as $_v)
    {
        $val[$_v] = isset($val[$_v]) ? trim(trim($val[$_v],'/'),'\\') : '';
        if(strlen($val[$_v])>0)
        {
            $arr_pathinfo = pathinfo($val[$_v]);
            if(isset($arr_pathinfo['filename']) && strlen($arr_pathinfo['filename']) ==32)
            {
                continue;
            }
            $temp_img[$_v] = $image_host.'/'.$val[$_v];
        }
    }
    if(is_array($temp_img) && !empty($temp_img))
    {
        foreach ($temp_img as $temp_img_key=>$temp_img_val)
        {
            $result = $import_assets_class->img_handel($temp_img_val);
            if(!$result)
            {
                var_dump("图片处理失败",$import_assets_class->re_code);
            }
            if(strlen($result)>0)
            {
                $edit_img[$temp_img_key] = $result;
            }
        }
    }
    if(is_array($edit_img) && !empty($edit_img))
    {
        $result_edit = nl_vod::edit($dc, $edit_img, $val['nns_id']);
        var_dump($result_edit);
    }
    unset($edit_img);
    unset($temp_img);
}