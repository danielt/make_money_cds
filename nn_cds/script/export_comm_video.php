<?php
/**
 * 删除重复注入的 媒资包 分集 片源
 */
header("Content-type:text/html;charset=utf-8");
include '../nn_logic/nl_common.func.php';
$dc_r = nl_get_dc(array(
		'db_policy' => NL_DB_READ,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$db_r =$dc_r->db();
#先查询重复注入的主媒资
$sql_vod_comm="select temp.nns_asset_import_id from 
		(select count(nns_asset_import_id) as num,nns_asset_import_id from nns_vod 
		GROUP BY nns_asset_import_id) as temp where temp.num>1";
$result_vod_comm = nl_query_by_db($sql_vod_comm, $db_r);
if(!$result_vod_comm)
{
	echo "db error ,query comm vod sql:".$sql_vod_comm;
}
$filename="主媒资".date('Ymd').'.csv';
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=". $filename);
header('Cache-Control: max-age=0');
print(chr(0xEF).chr(0xBB).chr(0xBF));
ob_start();
$fp = fopen('php://output', 'a');
fputcsv($fp, array('主媒资','主媒资id','名称','创建时间','原因'));
if(is_array($result_vod_comm) && !empty($result_vod_comm))
{
	foreach ($result_vod_comm as $val)
	{
		$sql_query_vod = "select nns_id,nns_name,nns_create_time from nns_vod where nns_asset_import_id='{$val['nns_asset_import_id']}'";
		$result_query_vod = nl_query_by_db($sql_query_vod, $db_r);
		if(!$result_query_vod)
		{
			echo "db error ,query every vod sql:".$sql_query_vod;
		}
		if(is_array($result_query_vod))
		{
			foreach ($result_query_vod as $_v)
			{
				$sql_query_index ="select count(*) as num from nns_vod_index where nns_vod_id='{$_v['nns_id']}'";
				$result_query_index = nl_query_by_db($sql_query_index, $db_r);
				$sql_query_media ="select count(*) as num from nns_vod_media where nns_vod_id='{$_v['nns_id']}'";
				$result_query_media = nl_query_by_db($sql_query_media, $db_r);
				if(!$result_query_index || !is_array($result_query_index))
				{
					echo "db error ,query vod index sql:".$sql_query_index;
					exit;
				}
				if(!$result_query_media || !is_array($result_query_media))
				{
					echo "db error ,query vod media sql:".$sql_query_media;
					exit;
				}
				if($result_query_index[0]['num'] <= 0)
				{
					fputcsv($fp, array(' ',$_v['nns_id'],$_v['nns_name'],$_v['nns_create_time'],'no index'));
				}
				if($result_query_media[0]['num'] <= 0)
				{
					fputcsv($fp, array(' ',$_v['nns_id'],$_v['nns_name'],$_v['nns_create_time'],'no media'));
				}
			}
		}
	}
}
