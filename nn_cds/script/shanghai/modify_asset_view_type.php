<?php
include dirname(dirname(dirname(__FILE__))).'/nn_logic/nl_common.func.php';
include dirname(dirname(dirname(__FILE__))) .'/mgtv_v2/models/queue_task_model.php';
set_time_limit(0);
class modify_asset_view_type
{
    private $obj_dc_read;
    private $obj_dc_write;
    /**
     * 栏目和媒资类型的映射表
     * @var array
     */
    private $view_type_category_mapping = array(
        10000001 => 0,  //电影
        10000002 => 1,  //电视剧
        10000003 => 2,  //综艺
        10000004 => 3,  //动漫
        10000005 => 4,  //音乐
        10000006 => 5,  //纪实
        10000010 => 5,  //纪实
        10000021 => 12,  //广告
        10000028 => 8,  //生活
        10000036 => 7,  //体育
        10000045 => 6,  //教育
        10000055 => 10,  //微电影
        10000066 => 9,  //财经
        10000078 => 11,  //快乐购
        10000091 => 29,  //其他
        10000105 => 11,  //品牌
        10000120 => 13,  //新闻
        10000136 => 19,  //原创
        10000153 => 18,  //游戏
        10000171 => 29,  //其他
    );

    public function __construct()
    {
        $this->obj_dc_read = nl_get_dc(array(
            'db_policy' => NL_DB_READ,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
        $this->obj_dc_write = nl_get_dc(array(
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
    }

    public function run()
    {
        echo "开始[".date("Y-m-d H:i:s")."]<br>" ;
        $this->modify_view_type_info();
        echo "结束[".date("Y-m-d H:i:s")."]<br>" ;
    }
    /**
     * 修改nns_vod表中的nns_view_type值
     */
    private function modify_view_type_info()
    {
        $arr_vod_id = array();
        $arr_where = array(
            "nns_view_type" => 0,
        );
        $arr_filter = array(
            "nns_id",
            "nns_view_type",
            "nns_category_id",
        );
        $arr_vod_info_1 = $this->get_bad_data_from_vod_info($this->obj_dc_read);
        if($arr_vod_info_1["ret"] != 0)
        {
            echo "统计需处理的数据失败：{$arr_vod_info_1['reason']}";die;
        }
        $arr_vod_info_2 = $this->get_bad_data_from_vod_info($this->obj_dc_read,1,10000002);
        if($arr_vod_info_2["ret"] != 0)
        {
            echo "统计需处理的数据失败：{$arr_vod_info_2['reason']}";die;
        }

        $arr_bad_info = array_merge($arr_vod_info_1["data_info"],$arr_vod_info_2["data_info"]);
        $int_count = count($arr_bad_info);
        echo "需处理的数据为===>{$int_count}<br>";
        foreach ($arr_bad_info as $arr_vod_item)
        {
            $int_view_type = $this->view_type_category_mapping[$arr_vod_item["nns_category_id"]];
            $arr_vod_item["nns_id"] = "060c601597c1215b89954265c3460d05";
            $arr_vod_result = $this->update_vod_view_tpye($this->obj_dc_write,$int_view_type,$arr_vod_item["nns_id"]);
            if($arr_vod_result["ret"] != 0)
            {
                echo "更新主媒资的媒资类型失败,nns_id：{$arr_vod_item["nns_id"]}<br>";die;
            }
            //查询是否生成了C2队列
            $arr_check_c2 = $this->check_c2_info($this->obj_dc_read, $arr_vod_item["nns_id"]);
            if($arr_check_c2['ret'] != 0)
            {
                echo "查询C2队列数据失败：{$arr_vod_info_2['reason']}";die;
            }
            if($arr_check_c2['data_info'] === true)
            {
                //重新生成中心注入队列
                $this->import_vod_info_into_egp($arr_vod_item["nns_id"]);
            }
            $arr_c2_result = $this->modify_c2_state($this->obj_dc_write,$arr_vod_item["nns_id"]);
            if($arr_c2_result["ret"] != 0)
            {
                $this->update_vod_view_tpye($this->obj_dc_write,$arr_vod_item["nns_view_type"],$arr_vod_item["nns_id"]);
                echo "更新C2队列状态失败,nns_ref_id：{$arr_vod_item["nns_id"]}<br>";die;
            }
            $int_count--;
            echo "更新成功：主媒资【{$arr_vod_item["nns_id"]}】";
            echo "剩余数据=====>{$int_count}";
            echo "<hr>";
        }
    }

    /**
     * 生成中心注入队列
     * @author <feijian.gao@starcor.cn>
     * @date   2017年8月14日11:36:58
     */
    private function import_vod_info_into_egp($str_vod_id)
    {
        $queue_task_model = new queue_task_model();
        $arr_inject_result = $queue_task_model->q_add_task_op_mgtv($str_vod_id, BK_OP_VIDEO, BK_OP_MODIFY);
        @error_log("生成中心注入队列【{$str_vod_id}】:".var_export($arr_inject_result)."\r\n",3,"inject.log");
        unset($queue_task_model);
        return $arr_inject_result;
    }
    /**
     * 查询该媒资是否生成了C2队列
     * @author <feijian.gao@starocr.cn>
     * @date    2017年8月14日10:57:15
     * @param   object   $dc
     * @param   string   $str_ref_id
     * @return array
     */
    private function check_c2_info($dc, $str_ref_id)
    {
        $sql = "select * from nns_mgtvbk_c2_task where nns_ref_id = '{$str_ref_id}' limit 1";
        $result = nl_query_by_db($sql, $dc->db());
        if($result === false)
        {
            return $this->return_data(1,"查询C2队列失败".$sql);
        }
        return $this->return_data(0,"查询C2队列成功",$result);
    }

    /**
     * 修改C2队列的状态，等待注入EPG
     * @author <feijian.gao@starocr.cn>
     * @date   2017年8月11日10:07:10
     */
    private function modify_c2_state($dc,$str_vod_id)
    {
        $str_modify_time = date("Y-m-d H:i:s");
        $sql = "update nns_mgtvbk_c2_task set nns_epg_status=97,nns_modify_time='{$str_modify_time}' where nns_ref_id = '{$str_vod_id}' and nns_epg_status = 99 and nns_type='video'";
        $result = nl_query_by_db($sql, $dc->db());
        if(!$result)
        {
            return $this->return_data(1,"修改主媒资数据信息失败".$sql);
        }
        return $this->return_data(0,"修改主媒数据资信息成功",$result);
    }

    /**
     * 用于上海视频基地修复旧数据中的影片类型问题
     * @author <feijian.gao@starcor.cn>
     * @date   2017年8月10日16:35:49
     * @param object    $dc                 数据库操作类
     * @param int       $int_view_type      影片类型
     * @param string    $str_category_id    栏目id
     * @return array array('ret'=>'返回码','reason'=>'返回说明','data_info'=>‘返回的数据’)
     */
    private function get_bad_data_from_vod_info($dc,$int_view_type=0,$str_category_id="10000001")
    {
        $sql = "select nns_id,nns_view_type,nns_category_id from nns_vod where nns_view_type={$int_view_type} and nns_category_id != '{$str_category_id}' and nns_deleted=0";
        $result = nl_query_by_db($sql, $dc->db());
        if(!is_array($result) && count($result) < 1)
        {
            return $this->return_data(1,"查询主媒资数据信息失败".$sql);
        }
        return $this->return_data(0,"查询主媒资数据信息成功",$result);
    }

    /**
     * @param $dc
     * @param $int_view_type
     * @param $str_id
     * @return array
     */
    private function update_vod_view_tpye($dc,$int_view_type,$str_id)
    {
        $str_modify_time = date("Y-m-d H:i:s");
        $sql = "update nns_vod set nns_view_type={$int_view_type},nns_modify_time='{$str_modify_time}' where nns_id = '{$str_id}'";
        $result = nl_query_by_db($sql, $dc->db());
        if(!$result)
        {
            return $this->return_data(1,"修改主媒资数据信息失败".$sql);
        }
        return $this->return_data(0,"修改主媒数据资信息成功",$result);
    }

    /**
     * @param $int_ret
     * @param $str_info
     * @param $arr_data
     * @return array
     */
    private function return_data($int_ret, $str_info, $arr_data)
    {
        $arr_return_data = array(
            "ret" =>$int_ret,
            "reason" =>$str_info,
            "data_info" =>$arr_data,
        );
        return $arr_return_data;

    }
}

$obj_deal_view_type = new modify_asset_view_type();
$obj_deal_view_type->run();