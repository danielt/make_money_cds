<?php
/**
 * 1、根据原始ID，并通过C2任务向CDN+EPG发起删除任务，action为delete_media
 * 2、扫描原始ID中的任务是否删除完成，删除完成后进行任务的重切并注入CDN+EPG，action为restart_queue
 */
set_time_limit(0);
include_once dirname(dirname(dirname(__FILE__))) . '/nn_logic/nl_common.func.php';
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/models/content_task_model.php';
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/models/queue_task_model.php';
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';


//需要删除的片源的文本
define('DELETE_MEDIA_TASK_CSV', 'delete_media.csv');
//删除片源时生成的文本执行列表与发生错误，没有执行的列表路径
define('EXECUTE_SUCCESS_LIST_PATH',dirname(__FILE__) . '/success_delete_list.csv');
define('EXECUTE_ERROR_LIST_PATH',dirname(__FILE__) . '/error_delete_list.csv');

//重发任务时，生成的文本执行列表与发生错误，没有执行的列表路径
define('EXECUTE_RESTART_SUCCESS_LIST_PATH',dirname(__FILE__) . '/success_restart_list.csv');
define('EXECUTE_RESTART_ERROR_LIST_PATH',dirname(__FILE__) . '/error_restart_list.csv');
//允许操作的动作
//delete_media
//restart_queue
$action = 'restart_queue';

define('SPID','hndx');
define('CPID', 'ZTE');

include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/' . SPID . '/init.php';

$dc = nl_get_dc(array(
		'db_policy' => NL_DB_READ | NL_DB_WRITE,
		'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));

if(!isset($action) || empty($action))
{
	nn_echo_msg('无操作类型');
	exit;
}

$action($dc);
/**
 * 片源删除操作,并同时生成已经执行的任务列表与发生错误，没有执行的列表
 */
function delete_media($dc)
{
	//创建错误文件列表
	$error_fp = fopen(EXECUTE_ERROR_LIST_PATH, 'w');
	fwrite($error_fp, "\xEF\xBB\xBF"); // utf-8 bom		
	$line = array('片源播控ID','片源上游原始ID','主媒资播控ID','分集播控ID','片源集数','自增序号','错误原因');
	fputcsv($error_fp, $line);
	fclose($error_fp);
	//创建成功文件列表
	$succ_fp = fopen(EXECUTE_SUCCESS_LIST_PATH, 'w');
	fwrite($succ_fp, "\xEF\xBB\xBF"); // utf-8 bom		
	$succ_line = array('片源播控ID','片源上游原始ID','主媒资播控ID','分集播控ID','片源集数','自增序号');
	fputcsv($succ_fp, $succ_line);
	fclose($succ_fp);
	
	$csv_files = fopen(DELETE_MEDIA_TASK_CSV, 'r');
	while (!feof($csv_files))
	{
		$line_arr = fgetcsv($csv_files);
		if (is_array($line_arr) && !empty($line_arr))
		{
			$media_original_id = $line_arr[0];
			//根据原始ID查询片源信息
			$sql = "select nns_id,nns_import_id,nns_vod_id,nns_vod_index_id,nns_vod_index,nns_integer_id from nns_vod_media where nns_import_id='{$media_original_id}' and nns_cp_id='" . CPID . "'";
			$media_re = nl_query_by_db($sql, $dc->db());
			if(!is_array($media_re))
			{
				//生成错误列表
				$list = array('',$media_original_id,'','','','','片源表nns_vod_media中无当前片源数据');
				__write_svc($list,EXECUTE_ERROR_LIST_PATH);
				continue;
			}			
			//查询C2中是否存在当前任务，且已完全注入成功
			$c2_sql = "select nns_id,nns_status,nns_epg_status,nns_action from nns_mgtvbk_c2_task where nns_ref_id='{$media_re[0]['nns_id']}'";
			$c2_re = nl_query_by_db($c2_sql, $dc->db());
			if(!is_array($c2_re))
			{
				//生成错误列表
				$list = array(
					$media_re[0]['nns_id'],
					$media_original_id,
					$media_re[0]['nns_vod_id'],
					$media_re[0]['nns_vod_index_id'],
					$media_re[0]['nns_vod_index'],
					$media_re[0]['nns_integer_id'],
					'C2不存在当前任务'
				);
				__write_svc($list,EXECUTE_ERROR_LIST_PATH);
				continue;
			}
			//c2中任务已经属于删除任务
			if($c2_re[0]['nns_action'] == 'destroy')
			{
				//忽略
				$list = array(
					$media_re[0]['nns_id'],
					$media_original_id,
					$media_re[0]['nns_vod_id'],
					$media_re[0]['nns_vod_index_id'],
					$media_re[0]['nns_vod_index'],
					$media_re[0]['nns_integer_id'],
					'C2中的任务已经处于删除状态,忽略'
				);
				__write_svc($list,EXECUTE_ERROR_LIST_PATH);
				continue;
			}
			//CDN未执行成功
			if($c2_re[0]['nns_status'] != '0')
			{
				//生成错误列表
				$list = array(
					$media_re[0]['nns_id'],
					$media_original_id,
					$media_re[0]['nns_vod_id'],
					$media_re[0]['nns_vod_index_id'],
					$media_re[0]['nns_vod_index'],
					$media_re[0]['nns_integer_id'],
					'C2中的当前任务还未注入CDN'
				);
				__write_svc($list,EXECUTE_ERROR_LIST_PATH);
				//按照正常流程来说，此处应该取消当前任务，并重新进行注入
				//SO DO SOMETING...
				continue;
			}
			//EPG未执行成功
			if($c2_re[0]['nns_epg_status'] != '99')
			{
				//生成错误列表
				$list = array(
					$media_re[0]['nns_id'],
					$media_original_id,
					$media_re[0]['nns_vod_id'],
					$media_re[0]['nns_vod_index_id'],
					$media_re[0]['nns_vod_index'],
					$media_re[0]['nns_integer_id'],
					'C2中的当前任务还未注入EPG'
				);
				__write_svc($list,EXECUTE_ERROR_LIST_PATH);
				//按照正常流程来说，可以直接发送删除指令
				//SO DO SOMETING...
				continue;
			}
			//修改C2任务状态，发送删除指令
			$update_sql = "update nns_mgtvbk_c2_task set nns_status=1,nns_action='destroy',nns_epg_status=97 where nns_id='" . $c2_re[0]['nns_id'] . "'";
			$up_re = nl_execute_by_db($update_sql,$dc->db());
			if($up_re === false)
			{
				//生成错误列表
				$list = array(
					$media_re[0]['nns_id'],
					$media_original_id,
					$media_re[0]['nns_vod_id'],
					$media_re[0]['nns_vod_index_id'],
					$media_re[0]['nns_vod_index'],
					$media_re[0]['nns_integer_id'],
					'修改C2任务为删除初始状态，数据库执行失败'
				);
				__write_svc($list,EXECUTE_ERROR_LIST_PATH);
				continue;
			}	
			$re = content_task_model::vod($c2_re[0]['nns_id'],null,SPID);
			if($re)
			{
				//生成成功列表
				$list = array(
					$media_re[0]['nns_id'],
					$media_original_id,
					$media_re[0]['nns_vod_id'],
					$media_re[0]['nns_vod_index_id'],
					$media_re[0]['nns_vod_index'],
					$media_re[0]['nns_integer_id'],
				);
				__write_svc($list,EXECUTE_SUCCESS_LIST_PATH);
			}
			else 
			{
				//生成错误列表
				$list = array(
					$media_re[0]['nns_id'],
					$media_original_id,
					$media_re[0]['nns_vod_id'],
					$media_re[0]['nns_vod_index_id'],
					$media_re[0]['nns_vod_index'],
					$media_re[0]['nns_integer_id'],
					'未知错误'
				);
				__write_svc($list,EXECUTE_ERROR_LIST_PATH);
			}
			unset($sql);
			unset($c2_sql);
			unset($update_sql);
		}
	}
	fclose($csv_files);
}
/**
 * 重发已删除的片源任务
 * @param $dc
 */
function restart_queue($dc)
{
	//创建错误文件列表
	$error_fp = fopen(EXECUTE_RESTART_ERROR_LIST_PATH, 'w');
	fwrite($error_fp, "\xEF\xBB\xBF"); // utf-8 bom		
	$line = array('片源播控ID','片源上游原始ID','主媒资播控ID','分集播控ID','片源集数','自增序号','错误原因');
	fputcsv($error_fp, $line);
	fclose($error_fp);
	//创建成功文件列表
	$succ_fp = fopen(EXECUTE_RESTART_SUCCESS_LIST_PATH, 'w');
	fwrite($succ_fp, "\xEF\xBB\xBF"); // utf-8 bom		
	$succ_line = array('片源播控ID','片源上游原始ID','主媒资播控ID','分集播控ID','片源集数','自增序号');
	fputcsv($succ_fp, $succ_line);
	fclose($succ_fp);
		
	$queue_task_model = new queue_task_model();
	
	$csv_files = fopen(DELETE_MEDIA_TASK_CSV, 'r');
	while (!feof($csv_files))
	{
		$line_arr = fgetcsv($csv_files);
		if (is_array($line_arr) && !empty($line_arr))
		{
			$media_original_id = $line_arr[0];
			//根据原始ID查询片源信息
			$sql = "select nns_id,nns_import_id,nns_vod_id,nns_vod_index_id,nns_vod_index,nns_integer_id from nns_vod_media where nns_import_id='{$media_original_id}' and nns_cp_id='" . CPID . "'";
			$media_re = nl_query_by_db($sql, $dc->db());
			if(!is_array($media_re))
			{
				//生成错误列表
				$list = array('',$media_original_id,'','','','','片源表nns_vod_media中无当前片源数据');
				__write_svc($list,EXECUTE_RESTART_ERROR_LIST_PATH);
				continue;
			}			
			//查询C2中是否存在当前任务，且已完全注入成功
			$c2_sql = "select nns_id,nns_status,nns_epg_status,nns_action from nns_mgtvbk_c2_task where nns_ref_id='{$media_re[0]['nns_id']}'";
			$c2_re = nl_query_by_db($c2_sql, $dc->db());
			if(!is_array($c2_re))
			{
				//生成错误列表
				$list = array(
					$media_re[0]['nns_id'],
					$media_original_id,
					$media_re[0]['nns_vod_id'],
					$media_re[0]['nns_vod_index_id'],
					$media_re[0]['nns_vod_index'],
					$media_re[0]['nns_integer_id'],
					'C2不存在当前任务'
				);
				__write_svc($list,EXECUTE_RESTART_ERROR_LIST_PATH);
				continue;
			}
			if($c2_re[0]['nns_action'] != 'destroy')
			{
				//生成错误列表
				$list = array(
					$media_re[0]['nns_id'],
					$media_original_id,
					$media_re[0]['nns_vod_id'],
					$media_re[0]['nns_vod_index_id'],
					$media_re[0]['nns_vod_index'],
					$media_re[0]['nns_integer_id'],
					'C2的当前任务不是删除状态'
				);
				__write_svc($list,EXECUTE_RESTART_ERROR_LIST_PATH);
				continue;				
			}
			if($c2_re[0]['nns_status'] != '0')
			{
				//生成错误列表
				$list = array(
					$media_re[0]['nns_id'],
					$media_original_id,
					$media_re[0]['nns_vod_id'],
					$media_re[0]['nns_vod_index_id'],
					$media_re[0]['nns_vod_index'],
					$media_re[0]['nns_integer_id'],
					'C2的当前任务还未注入CDN成功'
				);
				__write_svc($list,EXECUTE_RESTART_ERROR_LIST_PATH);
				continue;				
			}
			if($c2_re[0]['nns_epg_status'] != '99')
			{
				//生成错误列表
				$list = array(
					$media_re[0]['nns_id'],
					$media_original_id,
					$media_re[0]['nns_vod_id'],
					$media_re[0]['nns_vod_index_id'],
					$media_re[0]['nns_vod_index'],
					$media_re[0]['nns_integer_id'],
					'C2的当前任务还未注入EPG成功'
				);
				__write_svc($list,EXECUTE_RESTART_ERROR_LIST_PATH);
				continue;				
			}
			//重新进入队列
			$update_sql = "update nns_mgtvbk_c2_task set nns_status=7,nns_epg_status=100 where nns_id='{$c2_re[0]['nns_id']}'";
			nl_execute_by_db($update_sql, $dc->db());
			$queue_task_model->q_report_task($c2_re[0]['nns_id']);
			$re = $queue_task_model->q_add_task($media_re[0]['nns_id'],'media','destroy',SPID,null,true);
			if($re)
			{
				//生成成功列表
				$list = array(
					$media_re[0]['nns_id'],
					$media_original_id,
					$media_re[0]['nns_vod_id'],
					$media_re[0]['nns_vod_index_id'],
					$media_re[0]['nns_vod_index'],
					$media_re[0]['nns_integer_id'],
				);
				__write_svc($list,EXECUTE_RESTART_SUCCESS_LIST_PATH);
			}
			else 
			{
				//生成错误列表
				$list = array(
					$media_re[0]['nns_id'],
					$media_original_id,
					$media_re[0]['nns_vod_id'],
					$media_re[0]['nns_vod_index_id'],
					$media_re[0]['nns_vod_index'],
					$media_re[0]['nns_integer_id'],
					'未知错误'
				);
				__write_svc($list,EXECUTE_RESTART_ERROR_LIST_PATH);
			}
			unset($sql);
			unset($c2_sql);
			unset($update_sql);
		}
	}
}
/**
 * 追加写入CSV文件中
 */
function __write_svc($list, $file_path)
{
	$fp = fopen($file_path, 'a');
	fwrite($fp, "\xEF\xBB\xBF");
	fputcsv($fp,$list);	
	fclose($fp);	
}
/**
 * 记录日志
 */
function nn_echo_msg($msg ,$type=null)
{
	if (empty($msg))
	{
		return false;
	}

	$type = empty($type) ? 'delete_media' : $type;
	//写入日志文件
	$str_cms_dir = dirname(dirname(dirname(__FILE__))) . '/';
	$str_log_dir = $str_cms_dir . 'data/log/' . SPID . '/' . $type . '/';
	$str_log_file =  date('YmdH') . '.txt';
	$str_start_time = "[" . date("Y-m-d H:i:s") . "]";
	if (!is_dir($str_log_dir))
	{
		mkdir($str_log_dir, 0777, true);
	}
	file_put_contents($str_log_dir . $str_log_file, $str_start_time . $msg . "\r\n", FILE_APPEND);
	return true;
}