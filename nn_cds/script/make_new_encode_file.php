<?php
set_time_limit(0);
header("Content-type:text/html;charset=utf-8");
include '../nn_logic/nl_common.func.php';
$sql="select * from nns_file_encode";
$dc = nl_get_dc(array(
    'db_policy' => NL_DB_WRITE,
    'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
));
$result = nl_query_by_db($sql, $dc->db());
if(!is_array($result) || empty($result))
{
    exit('ok');
}
foreach ($result as $val)
{
    $nns_file_url = isset($val['nns_file_url']) ? $val['nns_file_url'] : '';
    if(!is_json($nns_file_url))
    {
        continue;
    }
    $arr_file_url = json_decode($nns_file_url,true);
    if(isset($arr_file_url['media']['in']) && strlen($arr_file_url['media']['in']) >0)
    {
        continue;
    }
    
    if(isset($arr_file_url['media']) && is_string($arr_file_url['media']))
    {
        $temp_arr = array_filter(explode('/', $arr_file_url['media']));
        array_shift($temp_arr);
        array_shift($temp_arr);
        $temp_str = implode('/', $temp_arr);
        $temp_file_url = $arr_file_url;
        $temp_file_url['media'] = array(
                'in'=>$temp_str,
                'out'=>$arr_file_url['media'],
        );
        $str_temp_nns_file_url = json_encode($temp_file_url);
        $sql_ex ="update nns_file_encode set nns_file_url='{$str_temp_nns_file_url}' where nns_id='{$val['nns_id']}'";
        nl_execute_by_db($sql_ex, $dc->db());
    }
}



function is_json($string)
{
    $string = strlen($string) <1 ? '' : $string;
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}