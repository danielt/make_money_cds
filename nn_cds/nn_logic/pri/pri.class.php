<?php

class nl_pri {
	/**
	 *
	 * 权限列表 ...
	 */
	public function nns_db_pri_list() {
$result = array();
$result["ret"] = "0";
$result["reason"] = "pri list is ok";
$result["data"] = array(
				"0"=>array(
						   "code"=>"100",
						   "desc"=>cms_get_lang('pri_xtgly'),
						   "pri"=>array(
										 "0"=>array("code"=>"100100",
													"desc"=>cms_get_lang('pri_xtgly')
													)
										)
						  ),
				 "1"=>array(
						   "code"=>"101",
						   "desc"=>cms_get_lang('pri_jsgl'),
						   "pri"=>array(
										 "0"=>array("code"=>"101103",
													"desc"=>cms_get_lang('pri_show_role')
													) ,
										 "1"=>array("code"=>"101100",
													"desc"=>cms_get_lang('pri_add_role')
													),
										 "2"=>array("code"=>"101101",
													"desc"=>cms_get_lang('pri_modify_role')
													),
										 "3"=>array("code"=>"101102",
													"desc"=>cms_get_lang('pri_del_role')
													),
										 "4"=>array("code"=>"101001",
													"desc"=>cms_get_lang('pri_module_invisible')
													)

										)
						   ),

				"2"=>array(
						   "code"=>"102",
						   "desc"=>cms_get_lang('pri_glygl'),
						   "pri"=>array(
										 "0"=>array("code"=>"102103",
													"desc"=>cms_get_lang('pri_show_admin_user')
													) ,
										 "1"=>array("code"=>"102100",
													"desc"=>cms_get_lang('pri_add_admin_user')
													),
										 "2"=>array("code"=>"102101",
													"desc"=>cms_get_lang('pri_modify_admin_user')
													),
										 "3"=>array("code"=>"102102",
													"desc"=>cms_get_lang('pri_del_admin_user')
													),
										 "4"=>array("code"=>"102001",
													"desc"=>cms_get_lang('pri_module_invisible')
													)
										)
						   ),
				"3"=>array(
						   "code"=>"103",
						   "desc"=>cms_get_lang('pri_hzhbgl'),
						   "pri"=>array(
										 "0"=>array("code"=>"103103",
													"desc"=>cms_get_lang('pri_show_partner')
													)  ,
										 "1"=>array("code"=>"103100",
													"desc"=>cms_get_lang('pri_add_partner')
													),
										 "2"=>array("code"=>"103101",
													"desc"=>cms_get_lang('pri_modify_partner')
													),
										 "3"=>array("code"=>"103102",
													"desc"=>cms_get_lang('pri_del_partner')
													),
										 "4"=>array("code"=>"103001",
													"desc"=>cms_get_lang('pri_module_invisible')
													)

										  )
						   ),
				"4"=>array(
						   "code"=>"104",
						   "desc"=>cms_get_lang('pri_jtgl'),
						   "pri"=>array(
										 "0"=>array("code"=>"104103",
													"desc"=>cms_get_lang('pri_show_group')
													),
										 "1"=>array("code"=>"104100",
													"desc"=>cms_get_lang('pri_add_group')
													),
										 "2"=>array("code"=>"104101",
													"desc"=>cms_get_lang('pri_modify_group')
													),
										 "3"=>array("code"=>"104102",
													"desc"=>cms_get_lang('pri_del_group')
													),
										 "4"=>array("code"=>"104001",
													"desc"=>cms_get_lang('pri_module_invisible')
													)

										)
						   ),
//                 "5"=>array(
//                           "code"=>"105",
//                           "desc"=>cms_get_lang('pri_lmgl'),
//                           "pri"=>array(
//                                         "0"=>array("code"=>"105103",
//                                                    "desc"=>cms_get_lang('pri_show_lm')
//                                                    ),
//                                         "1"=>array("code"=>"105100",
//                                                    "desc"=>cms_get_lang('pri_add_lm')
//                                                    ),
//                                         "2"=>array("code"=>"105101",
//                                                    "desc"=>cms_get_lang('pri_modify_lm')
//                                                    ),
//                                         "3"=>array("code"=>"105102",
//                                                    "desc"=>cms_get_lang('pri_del_lm')
//                                                    )
//                                        )
//                           ),
				 "6"=>array(
						   "code"=>"106",
						   "desc"=>cms_get_lang('pri_dbgl'),
						   "pri"=>array(
										 "0"=>array("code"=>"106103",
													"desc"=>cms_get_lang('pri_show_vod')
													) ,
										 "1"=>array("code"=>"106100",
													"desc"=>cms_get_lang('pri_add_vod')
													),
										 "2"=>array("code"=>"106101",
													"desc"=>cms_get_lang('pri_modify_vod')
													),
										 "3"=>array("code"=>"106102",
													"desc"=>cms_get_lang('pri_del_vod')
													),
										 "4"=>array("code"=>"106104",
													"desc"=>cms_get_lang('pri_audit_vod')
													),
										 "5"=>array("code"=>"106001",
													"desc"=>cms_get_lang('pri_module_invisible')
													)
										)
						   ),
				   "7"=>array(
						   "code"=>"107",
						   "desc"=>cms_get_lang('pri_zbgl'),
						   "pri"=>array(
										 "0"=>array("code"=>"107103",
													"desc"=>cms_get_lang('pri_show_live')
													) ,
										 "1"=>array("code"=>"107100",
													"desc"=>cms_get_lang('pri_add_live')
													),
										 "2"=>array("code"=>"107101",
													"desc"=>cms_get_lang('pri_modify_live')
													),
										 "3"=>array("code"=>"107102",
													"desc"=>cms_get_lang('pri_del_live')
													),
										 "4"=>array("code"=>"107104",
													"desc"=>cms_get_lang('pri_audit_live')
													),
										 "5"=>array("code"=>"107001",
													"desc"=>cms_get_lang('pri_module_invisible')
													)
										)
						   ),
				   "8"=>array(
						   "code"=>"108",
						   "desc"=>cms_get_lang('pri_device_manage'),
						   "pri"=>array(
										 "0"=>array("code"=>"108103",
													"desc"=>cms_get_lang('pri_show_device')
													) ,
										 "1"=>array("code"=>"108100",
													"desc"=>cms_get_lang('pri_add_device')
													),
										 "2"=>array("code"=>"108101",
													"desc"=>cms_get_lang('pri_modify_device')
													),
										 "3"=>array("code"=>"108102",
													"desc"=>cms_get_lang('pri_del_device')
													),
										 "4"=>array("code"=>"108001",
													"desc"=>cms_get_lang('pri_module_invisible')
													)
										)
						   ),

					"10"=>array(
						   "code"=>"110",
						   "desc"=>cms_get_lang('pri_service_manage'),
						   "pri"=>array(
										 "0"=>array("code"=>"110103",
													"desc"=>cms_get_lang('pri_show_service')
													) ,
										 "1"=>array("code"=>"110100",
													"desc"=>cms_get_lang('pri_add_service')
													),
										 "2"=>array("code"=>"110101",
													"desc"=>cms_get_lang('pri_modify_service')
													),
										 "3"=>array("code"=>"110102",
													"desc"=>cms_get_lang('pri_del_service')
													),
										 "4"=>array("code"=>"110001",
													"desc"=>cms_get_lang('pri_module_invisible')
													)
										)
						   ),
					 "11"=>array(
						   "code"=>"111",
						   "desc"=>cms_get_lang('pri_assists_manage'),
						   "pri"=>array(
										 "0"=>array("code"=>"111103",
													"desc"=>cms_get_lang('pri_show_assists')
													) ,
										 "1"=>array("code"=>"111100",
													"desc"=>cms_get_lang('pri_add_assists')
													),
										 "2"=>array("code"=>"111101",
													"desc"=>cms_get_lang('pri_modify_assists')
													),
										 "3"=>array("code"=>"111102",
													"desc"=>cms_get_lang('pri_del_assists')
													),
										 "4"=>array("code"=>"111001",
													"desc"=>cms_get_lang('pri_module_invisible')
													)
										)
						   ),
					  "9"=>array(
						   "code"=>"109",
						   "desc"=>cms_get_lang('pri_product_manage'),
						   "pri"=>array(
										 "0"=>array("code"=>"109103",
													"desc"=>cms_get_lang('pri_show_product')
													) ,
										 "1"=>array("code"=>"109100",
													"desc"=>cms_get_lang('pri_add_product')
													),
										 "2"=>array("code"=>"109101",
													"desc"=>cms_get_lang('pri_modify_product')
													),
										 "3"=>array("code"=>"109102",
													"desc"=>cms_get_lang('pri_del_product')
													),
										 "4"=>array("code"=>"109001",
													"desc"=>cms_get_lang('pri_module_invisible')
													)
										)
						   ),
					  "12"=>array(
						   "code"=>"112",
						   "desc"=>cms_get_lang('pri_ipqam_manage'),
						   "pri"=>array(
										 "0"=>array("code"=>"112103",
													"desc"=>cms_get_lang('pri_show_ipqam')
													) ,
										 "1"=>array("code"=>"112100",
													"desc"=>cms_get_lang('pri_add_ipqam')
													),
										 "2"=>array("code"=>"112101",
													"desc"=>cms_get_lang('pri_modify_ipqam')
													),
										 "3"=>array("code"=>"112102",
													"desc"=>cms_get_lang('pri_del_ipqam')
													),
										 "4"=>array("code"=>"112001",
													"desc"=>cms_get_lang('pri_module_invisible')
													)
										)
						   )
					  ,
					  "13"=>array(
						   "code"=>"113",
						   "desc"=>cms_get_lang('pri_tpl_manage'),
						   "pri"=>array(

										 "0"=>array("code"=>"113101",
													"desc"=>cms_get_lang('pri_tpl_manage')
													),
										   "1"=>array("code"=>"113102",
													"desc"=>cms_get_lang('pri_tag_manage')
													),
										  "2"=>array("code"=>"113001",
													"desc"=>cms_get_lang('pri_module_invisible')
													)
										)
						   )
					   ,
					  "14"=>array(
						   "code"=>"114",
						   "desc"=>cms_get_lang('pri_stat_chart_manage'),
						   "pri"=>array(

										 "0"=>array("code"=>"114101",
													"desc"=>cms_get_lang('pri_show_stat_chart_manage')
													),
										 "1"=>array("code"=>"114001",
													"desc"=>cms_get_lang('pri_module_invisible')
													)
										)
						   )
						,
					  "15"=>array(
						   "code"=>"115",
						   "desc"=>cms_get_lang('pri_media_assist_import'),
						   "pri"=>array(
										 "0"=>array("code"=>"115101",
													"desc"=>cms_get_lang('pri_media_assist_import_manage')
													),
										 "1"=>array("code"=>"115001",
													"desc"=>cms_get_lang('pri_module_invisible')
													)

										)

						   ) ,
					   "16"=>array(
						   "code"=>"116",
						   "desc"=>cms_get_lang('pri_keyword_manage'),
						   "pri"=>array(
										 "0"=>array("code"=>"116100",
													"desc"=>cms_get_lang('pri_keyword_edit')
													),
										 "1"=>array("code"=>"116101",
													"desc"=>cms_get_lang('pri_keyword_list')
													),
										 "2"=>array("code"=>"116001",
													"desc"=>cms_get_lang('pri_module_invisible')
													)

										)
							),
						  "17"=>array(
						   "code"=>"117",
						   "desc"=>cms_get_lang('pri_ad_manage'),
						   "pri"=>array(
										 "0"=>array("code"=>"117100",
													"desc"=>cms_get_lang('pri_ad_pos_edit')
													),
										 "1"=>array("code"=>"117101",
													"desc"=>cms_get_lang('pri_ad_content_edit')
													),
										 "2"=>array("code"=>"117102",
													"desc"=>cms_get_lang('pri_ad_policy_edit')
													),
										 "3"=>array("code"=>"117103",
													"desc"=>cms_get_lang('pri_ad_list')
													),
										 "4"=>array("code"=>"117001",
													"desc"=>cms_get_lang('pri_module_invisible')
													)

										)
							),
							/*
							"18"=>array(
								"code"=>"118",
								"desc"=>cms_get_lang('pri_msg_manage'),
								"pri"=>array(
									"0"=>array("code"=>"118101",
												"desc"=>cms_get_lang('pri_msg_show')
												),
									"1"=>array("code"=>"118102",
												"desc"=>cms_get_lang('pri_msg_edit')
												),
									"2"=>array("code"=>"118103",
												"desc"=>cms_get_lang('pri_msg_del')
												)
									)
								),
							"19"=>array(
								"code"=>"119",
								"desc"=>cms_get_lang('pri_yhlm'),
								'pri'=>array(
									'0'=> array(
										'code'=>'119101',
										"desc"=>cms_get_lang('pri_user_column_add')
										),
									'1'=>array(
										'code'=>'119102',
										"desc"=>cms_get_lang('pri_user_column_modify')
										),
									'2'=>array(
										'code'=>'119103',
										"desc"=>cms_get_lang('pri_user_column_del')
										),
									'3'=>array(
										'code'=>'119104',
										"desc"=>cms_get_lang('pri_user_column_move')
										),
									'4'=>array(
										'code'=>'119105',
										"desc"=>cms_get_lang('pri_user_column_sort')
										),
									'5'=>array(
										'code'=>'119106',
										"desc"=>cms_get_lang('pri_user_column_query')
										)
								)

							),
							"20"=>array(
								"code"=>"120",
								"desc"=>cms_get_lang('pri_user_group'),
								'pri'=>array(
									'0'=>array(
										'code'=>'120101',
										"desc"=>cms_get_lang('pri_user_column_add')
										),
									'1'=>array(
										'code'=>'120102',
										"desc"=>cms_get_lang('pri_user_column_modify')
										),
									'2'=>array(
										'code'=>'120103',
										"desc"=>cms_get_lang('pri_user_column_del')
										),
									'3'=>array(
										'code'=>'120104',
										"desc"=>cms_get_lang('pri_user_group_sort')
										),
									'4'=>array(
										'code'=>'120105',
										"desc"=>cms_get_lang('pri_group_column_add')
										),
									'5'=>array(
										'code'=>'120106',
										"desc"=>cms_get_lang('pri_group_column_modify')
										),
									'6'=>array(
										'code'=>'120107',
										"desc"=>cms_get_lang('pri_group_column_del')
										),
									'7'=>array(
										'code'=>'120108',
										"desc"=>cms_get_lang('pri_bind_catalog')
										),
									'8'=>array(
										'code'=>'120109',
										"desc"=>cms_get_lang('pri_group_bind_user')
										)
								)
							),*/
							"21"=>array(
								"code"=>"121",
								"desc"=>cms_get_lang('pri_info_package_manage'),
								"pri"=>array(
										"0"=>array(
										"code"=>"121100",
										"desc"=>cms_get_lang('pri_add_info_package')
										),
										"1"=>array(
										"code"=>"121101",
										"desc"=>cms_get_lang('pri_modify_info_package')
										),
										"2"=>array(
										"code"=>"121102",
										"desc"=>cms_get_lang('pri_del_info_package')
										),
										'3'=>array(
										'code'=>'121103',
										"desc"=>cms_get_lang('pri_add_info_package_column')
										),
										'4'=>array(
										'code'=>'121104',
										"desc"=>cms_get_lang('pri_modify_info_package_column')
										),
										'5'=>array(
										'code'=>'121105',
										"desc"=>cms_get_lang('pri_del_info_package_column')
										),
										'6'=>array(
										'code'=>'121106',
										"desc"=>cms_get_lang('pri_info_package_column_order')
										),
										'7'=>array(
										'code'=>'121107',
										"desc"=>cms_get_lang('pri_add_info_item')
										),
										'8'=>array(
										'code'=>'121108',
										"desc"=>cms_get_lang('pri_modify_info_item')
										),
										'9'=>array(
										'code'=>'121109',
										"desc"=>cms_get_lang('pri_del_info_item')
										),
										'10'=>array(
										'code'=>'121110',
										"desc"=>cms_get_lang('pri_audit_info_item')
										),
										'11'=>array(
										'code'=>'121111',
										"desc"=>cms_get_lang('pri_order_info_item')
										),
										'12'=>array(
										'code'=>'121112',
										"desc"=>cms_get_lang('pri_move_info_item')
										),


								)
							),
							'22'=>array(
								'code'=>'122',
								"desc"=>cms_get_lang('pri_yhlm_manage'),
								'pri'=>array(
										'0'=>array(
										'code'=>'122101',
										"desc"=>cms_get_lang('pri_user_column_add')
										),
										'1'=>array(
										'code'=>'122102',
										"desc"=>cms_get_lang('pri_user_column_modify')
										),
										'2'=>array(
										'code'=>'122103',
										"desc"=>cms_get_lang('pri_user_column_del')
										),
										'3'=>array(
										'code'=>'122104',
										"desc"=>cms_get_lang('pri_user_column_sort')
										),
										'4'=>array(
										'code'=>'122105',
										"desc"=>cms_get_lang('pri_user_column_query')
										),
										'5'=>array(
										'code'=>'122106',
										"desc"=>cms_get_lang('pri_user_column_move')
										),
								)
							),
							/*
							"24"=>array(
								"code"=>"124",
								"desc"=>cms_get_lang('pri_schedule_manage'),
								"pri"=>array(
									"0"=>array("code"=>"124100",
												"desc"=>cms_get_lang('pri_schedule_show')
												),
									"1"=>array("code"=>"124101",
												"desc"=>cms_get_lang('pri_schedule_addedit')
												),
									"2"=>array("code"=>"124102",
												"desc"=>cms_get_lang('pri_schedule_del')
												),
									)
								),

							"25"=>array(
								"code"=>"125",
								"desc"=>cms_get_lang('pri_app_manage'),
								"pri"=>array(
									"0"=>array("code"=>"125100",
												"desc"=>cms_get_lang('pri_app_package_show')
												),
									"1"=>array("code"=>"125101",
												"desc"=>cms_get_lang('pri_app_package_addedit')
												),
									"2"=>array("code"=>"125102",
												"desc"=>cms_get_lang('pri_app_package_del')
												),

									"3"=>array("code"=>"125103",
												"desc"=>cms_get_lang('pri_app_sort')
												),
									"4"=>array("code"=>"125104",
												"desc"=>cms_get_lang('pri_app_move')
												),
									"5"=>array("code"=>"125105",
												"desc"=>cms_get_lang('pri_app_audit')
												),
									)
								),
								'26'=>array(
									'code'=>'126',
									"desc"=>cms_get_lang('pri_webjz_manage'),
									'pri'=>array(
										'0'=>array(
											'code'=>'126100',
											"desc"=>cms_get_lang('pri_webkz_show')
										),
										'1'=>array(
											'code'=>'126101',
											"desc"=>cms_get_lang('pri_webkz_add')

										),
										'2'=>array(
											'code'=>'126102',
											"desc"=>cms_get_lang('pri_webkz_modify')
										),
										'3'=>array(
											'code'=>'126103',
											"desc"=>cms_get_lang('pri_webkz_del')
										),
										)
								)*/
				"27"=>array(
						   "code"=>"127",
						   "desc"=>cms_get_lang('weather_management'),
						   "pri"=>array(
										 "0"=>array("code"=>"127101",
													"desc"=>cms_get_lang('weather_pic_management')
													)
										)
						   ),
);
	    return $result;
	}
	/**
	 *
	 * 权限检测 ...
	 * @param unknown_type $pri_list
	 * @param unknown_type $pri_op
	 */
	static public function pri_check($pri_list, $pri_op) {
	    if(!isset($pri_list) || !isset($pri_op)){
	    	return FALSE;
	    }
	    if(empty($pri_list) || empty($pri_op)){
	    	return FALSE;
	    }

	    if(strlen($pri_op) != 6){
	    	return FALSE;
	    }
	    if(false !== strpos($pri_list, '100100')){
	    	return TRUE;
	    }
	    if(false === strpos($pri_list, $pri_op)){
	    	return FALSE;
	    }
	    else {
	    	return TRUE;
	    }
	}
}
?>