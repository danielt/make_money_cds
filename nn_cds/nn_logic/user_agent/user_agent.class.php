<?php
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'nl_constant.php';
class nl_user_agent {

 	private static $STD='nn_player/std';
 	private static $IPHONE='nn_phone/iphone';
 	private static $IPAD='nn_pad/ipad';
 	private static $ANDROIDPHONE='nn_phone/android_phone';
 	private static $ANDROIDPAD='nn_pad/android_pad';
 	private static $PC='nn_pc/std';
 	private static $STD_CYYY = 'nn_player/cy-tnl';
 	private static $log_inst;

 	public static function get_user_agent_tag($user_agnt){

 		if (!empty($user_agnt)){
 			if (false !== strpos($user_agnt,self::$STD) || false !== strpos($user_agnt,self::$STD_CYYY)){
 				return NNS_PLAYER_STD_TAG;
 			}elseif(false !== strpos($user_agnt,self::$IPHONE)){
 				return NNS_APPLE_PHONE_TAG;
 			}elseif(false !== strpos($user_agnt,self::$IPAD)){
 				return NNS_APPLE_PAD_TAG;
 			}elseif(false !== strpos($user_agnt,self::$ANDROIDPHONE)){
 				return NNS_ANDROID_PHONE_TAG;
 			}elseif(false !== strpos($user_agnt,self::$ANDROIDPAD)){
 				return NNS_ANDROID_PAD_TAG;
 			}elseif(false !== strpos($user_agnt,self::$PC)){
 				return NNS_PC_TAG;
 			}
 		}
 		
 		return null;
 	}

 	public static function get_http_agent_tag(){
 		$user_agnt = isset($_SERVER["HTTP_USER_AGENT"])?$_SERVER["HTTP_USER_AGENT"]:null;
 		if($user_agnt==null) return null;
 		$user_agnt=strtolower($user_agnt);
 		if (
 		(	false !== strpos($user_agnt,'windows') ||
 			false !== strpos($user_agnt,'linux') ||
 			false !== strpos($user_agnt,'macintosh') ) &&
 			false !== strpos($user_agnt,'mozilla')
 			){
 			return NNS_PC_TAG;
 		}
 		return null;
 	}
}
?>