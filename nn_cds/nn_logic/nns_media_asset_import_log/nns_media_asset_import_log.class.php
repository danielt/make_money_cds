<?php

/*
 * Created on 2013-5-15 16:00 LMS
 *  
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';

class nl_media_asset_import_log {

        const CACHE_TIME_OUT = 300;

        /**
         * 添加新版本
         * @param db DB模块
         * @param String 数据集
         * @return *
         * 			
         * 			FALSE 添加失败
         * 			TRUE  添加成功
         */
        static private function _add_log_by_db($db, $data) {
                $sql = "INSERT INTO  `nns_media_asset_import_log` (
`nns_import_video_id` ,
`nns_import_type` ,
`nns_create_time` ,
`nns_error_desc`,
`nns_error_code` ,
`nns_state` ,
`nns_video_name`
)
VALUES ('{$data['nns_import_video_id']}'  , '{$data['nns_import_type']}' , NOW() , '{$data['nns_error_desc']}' , '{$data['nns_error_code']}',  '0',  '{$data['nns_video_name']}'
);";
                return nl_execute_by_db($sql, $db);
        }

        /**
         * 获取版本列表
         * @param type $db DB模块
         * @param type $where where条件  array(key=>val)
         * @param type $field 获取的字段 默认*
         * @param type $order 排序方式
         * @param type $star 开始数据
         * @param type $len 数据长度
         * @return type LIST  TRUE  FALSE
         */
        static public function get_log_list($db, $where, $field = "*", $star = "", $len = "", $order = 'nns_create_time desc') {
                $sql = "SELECT $field FROM `nns_media_asset_import_log` WHERE";
                if (!is_array($where))
                        $where = array();
                $wheresql = '';
                foreach ($where as $k => $v) {
                        $wheresql .= " `$k`='$v' AND";
                }
                $wheresql .=" 1=1 ORDER BY $order";
                $wheresql .= $star >= 0 && !empty($len) ? " LIMIT $star , $len" : "";
                $sql = $sql . $wheresql;
                $list = nl_query_by_db($sql, $db);
                if (isset($list[0]['count']))
                        return $list[0]['count'];
                return $list;
        }

        /**
         * 新增版本
         * @param dc DC模块
         * @param String 数据集
         * @return *
         * 			Array 
         * 			FALSE 新增失败
         *                                                                                                              0        已经存在
         * 			TRUE  新增成功
         */
        static public function add_log($dc, $data) {
                if (empty($data['nns_import_video_id']))
                        return false;
                $dc = $dc->db();
                $filed = array(
                    'nns_import_video_id',
                    'nns_import_type',
                    'nns_create_time',
                    'nns_error_code',
                    'nns_error_desc',
                    'nns_video_name');
                foreach ($filed as $v) {
                        if (!isset($data[$v]))
                                $data[$v] = "";
                }
                return self::_add_log_by_db($dc, $data);
        }

        /**
         * 修改版本
         * @param dc DC模块
         * @param String 数据集
         * @return *
         * 			Array 
         * 			FALSE 失败
         * 			TRUE  成功
         */
        static public function set_log($dc, $nns_id, $data) {
                if (empty($nns_id))
                        return false;
                $set = '';
                foreach ($data as $k => $v) {
                        $set .= "`$k`='$v',";
                }
                $nns_id = $nns_id . "''";
                $sql = "UPDATE `nns_media_asset_import_log` SET $set`nns_modify_time`=NOW()  WHERE   `nns_id` in ($nns_id)";

                return nl_query_by_db($sql, $dc->db());
        }

        /**
         *   删除版本
         * @param dc DC模块
         * @param String 数据集
         * @return *
         * 			Array 
         * 			FALSE 失败
         * 			TRUE  成功
         */
        static public function del_log($dc, $nns_id) {
                if (empty($nns_id))
                        return false;
                $nns_id = $nns_id . "''";
                $sql = "DELETE FROM `nns_media_asset_import_log` WHERE `nns_id` in ($nns_id)";
                return nl_execute_by_db($sql, $dc->db());
        }

}

?>