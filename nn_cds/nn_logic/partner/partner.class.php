<?php
/**
 * @author guanghu.zhou
 * @refactor date 2013-04-16
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';

class nl_partner{
	static private $prefix = "partner_logic#|#";	
	/**
	 * @subpackage 获取合作伙伴的信息
	 * @param $param = array('id'=>$id)
	 * @param dc  obj
	 * @return array
	 * @notice 原函数名称 nns_db_partner_info
	 * @access public
	 */
	static public function get_partner_info($dc,$policy,$param){
		$key = implode('-', $param);

		switch ($policy) {
		case NL_DC_DB :
			$result = self::_get_partner_info_by_db($dc -> db(), $param);
			break;
		case NL_DC_CACHE :
			$result = $dc -> cache()->get(self::$prefix.$key);
			break;
		case NL_DC_AUTO :
			if ($dc -> is_cache_enabled()) {
				$result = $dc -> cache()->get(self::$prefix.$key);
				if ($result === FALSE) {
					$result = self::_get_partner_info_by_db($dc -> db(), $param);
					$dc->cache->set(self::$prefix.$key,$result);
				}
			} else {
				$result = self::_get_partner_info_by_db($dc -> db(), $param);
			}
			break;
		}
		return $result;


	}
	/**
	 * @subpackage 获取合作伙伴的信息
	 * @param param array
	 * @param dc  obj
	 * @return array
	 * @access public
	 */
	static	private function _get_partner_info_by_db($db, $param)
	{
		extract($param);
		unset($param);
		if (! isset ( $id ) || empty ( $id )) {
			$result = array( 'ret'=>NNS_ERROR_ARGS, 'msg'=>'partner info param error' );
			return $result;
		}
		$str_sql = "select * from nns_partner where nns_id='$id'";
		$result = nl_db_get_all($str_sql, $db);
		return array('ret'=>0,'data'=>$result,'msg'=>null);
	}

}
