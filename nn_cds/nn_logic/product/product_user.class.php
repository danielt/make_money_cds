<?php
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
class nl_product_user {
	const CACHE_TIME_OUT=300;
/*
 * *********************************************
 * get_product_list_by_user_id
 * 获取用户授权的产品及资费信息
 * ********************************************
 */

	/**
	 * 从DC模块获取用户授权的产品及资费信息
	 * @param dc DC模块
	 * @param String 用户ID
	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
	 * @return *
	 * 			Array 产品及资费信息结果
	 * 			FALSE 查询失败
	 * 			TRUE  查询成功，但无数据
	 */
	static public function get_product_list_by_user_id($dc,$user_id,$policy=NL_DC_AUTO) {
		$result = null;
		if ($policy == NL_DC_AUTO){
			if ($dc->is_cache_enabled()) {
				$result = self::_get_product_list_by_user_id_cache($dc->cache(),$user_id);
				if ($result === FALSE ) {
					$result = self::_get_product_list_by_user_id_db($dc->db(),$user_id);
					self::_set_product_list_by_user_id_cache($dc->cache(),$user_id,$result);
				}
			}
			else {
				$result = self::_get_product_list_by_user_id_db($dc->db(),$user_id);
			}
		}elseif($policy == NL_DC_DB){
			$result = self::_get_product_list_by_user_id_db($dc->db(),$user_id);
		}elseif($policy == NL_DC_CACHE){
			$result = self::_get_product_list_by_user_id_cache($dc->cache(),$user_id);
		}

		return $result;
	}

	static private function _get_product_list_by_user_id_db($db,$user_id) {
			$sql = ' select a.*,' .
	    		'b.nns_check,' .
	    		'b.nns_name as product_name,' .
	    		'b.nns_content_org_id,' .
	    		'b.nns_single_video_bill_mode,' .
	    		'b.nns_content_org_type ' .
	    		'from nns_user_product as a ' .
	    		'left join nns_product as b ' .
	    		'on a.nns_product_id=b.nns_id ' .
	    		'where a.nns_user_id=\''.$user_id.'\' ' .
	    		'and b.nns_check=1 ' .
	    		'and b.nns_state=0 ';
//		$sql = ' select a.*,' .
//	    		'b.nns_check,' .
//	    		'b.nns_name as product_name,' .
//	    		'b.nns_content_org_id,' .
//	    		'b.nns_content_org_type,' .
//	    		'c.nns_price,' .
//	    		'c.nns_price_unit,' .
//	    		'c.nns_product_fee_name,' .
//	    		'c.nns_begindatetime,'.
//	    		'c.nns_enddatetime,'.
//	    		'c.nns_gathering_mode as fee_gathering_mode,'.
//	    		'c.nns_amount ' .
//	    		'from nns_user_product as a ' .
//	    		'left join nns_product as b ' .
//	    		'on a.nns_product_id=b.nns_id ' .
//	    		'left join nns_product_fee c ' .
//	    		'on a.nns_product_id=c.nns_product_id ' .
//	    		'and a.nns_product_fee_id=c.nns_product_fee_id ' .
//	    		'where a.nns_user_id=\''.$user_id.'\' ' .
//	    		'and b.nns_check=1 ' .
//	    		'and b.nns_state=0';

		return nl_query_by_db($sql, $db);
	}

	static private function _get_product_list_by_user_id_cache($cache,$user_id) {
		if ($cache===NULL) return FALSE;
		return unserialize($cache->get('product_user|#'.$user_id));
	}

	static private function _set_product_list_by_user_id_cache($cache,$user_id,$data) {
		if ($cache===NULL) return FALSE;
		return $cache->set('product_user|#'.$user_id,serialize($data),self::CACHE_TIME_OUT);
	}

	static public function delete_product_list_by_user_id_cache($dc,$user_id) {
		if ($dc->cache()===NULL) return FALSE;
		return $dc->cache()->delete('product_user|#'.$user_id);
	}

}
?>