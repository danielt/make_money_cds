<?php


/*
 * Created on 2012-10-18
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include_once 'product_fee.class.php';
include_once 'product_user.class.php';
include_once 'product.class.php';
include_once 'product_bo.class.php';
class nl_product_fee_bo {

	/**根据当前用户产品 返回最优常规资费
		 * @param DC模块
		 * @param $user_id 用户ID
		 * @param $packet_id 包ID
		 * @param $packet_type 包类型
		 * @param $category_id 栏目ID 可为空 为空默认为1000
		 * @return ARRAY 单一资费对象
		 * 			FALSE 用户没有授权
		 * 			TRUE  免费
		 * */
	static public function filter_product_fee_by_user_id($dc, $user_id, $packet_id, $packet_type, $category_id) {

		if( empty ($user_id) ) {
			return FALSE;
		}
		if( empty($packet_id) ){
			return TRUE;
		}

		if (empty($category_id)) $category_id='1000';

		$contents = nl_product :: epg_count_product_content($dc, $packet_id, $packet_type, $category_id, NULL);

		if ($contents === TRUE) {
			nl_write_error('packet_id('.$packet_type.'):'.$packet_id.',category_id:'.$category_id.'不隶属任何产品包,为免费视频');
//	 	免费视频，不隶属任何产品包
			return TRUE;
		}
		nl_write_error('packet_id('.$packet_type.'):'.$packet_id.',category_id:'.$category_id.'有绑定产品包,该栏目下视频不是免费');


//		STEP1 筛选有效产品
		$products = nl_product_bo :: get_active_product_list_by_user_id($dc, $user_id);

		if ($products === TRUE || $products === FALSE){
			nl_write_error('用户:'.$user_id.'没有授权任何产品包,不能观看该栏目下视频');
			return FALSE;			
		}


//		STEP2 剔除不作用于该栏目的产品包
		foreach ($products as $key => $product) {
            nl_write_error('给用户:'.$user_id.'授权的产品包:'.$product['product_name'].'('.$product['nns_product_id'].')');
			if ($product['nns_single_video_bill_mode']==1){
				unset ($products[$key]);
				continue;
			}
			$contents = nl_product :: epg_count_product_content($dc, $packet_id, $packet_type, $category_id, $product['nns_product_id']);

			if ($contents === FALSE || $contents === TRUE) {
				unset ($products[$key]);
			}
		}
		if (empty($products)){
			nl_write_error('授权给用户:'.$user_id.'的产品包中,都没有绑定栏目['.'packet_id('.$packet_type.'):'.$packet_id.',category_id:'.$category_id.']'.',不能观看该栏目下视频');
			//return FALSE;			
		}


//		STEP3 筛选有效资费
		$fees=self::get_active_product_fee_list($dc,$products);

//		STEP4 获取最优资费
		$return_fee = self::_get_usort_product_fee($fees);



		return $return_fee;
	}

	/**
	 * 根据资费单位判定当前产品价格类型
	 * @param $fee 资费
	 * @return String 价格类型
	 *
	 * nns_price_unit可能需要改为英文标识？？？？？？？？？
	 */
	static public function get_price_unit_by_tag($fee) {
		$return_str = null;
		if (empty ($fee) || $fee===FALSE) {
			$return_str = '无授权';
		} else {
			if ($fee === 'free' || $fee===TRUE) {
				$return_str = '免费';
			}
			elseif (is_array($fee)) {
				$fee_unit = isset($fee['nns_price_unit'])?$fee['nns_price_unit']:null;
				switch ($fee_unit) {
					case '年' :
						$return_str = '包年价';
						break;
					case '月' :
						$return_str = '包月价';
						break;
					case '日' :
						$return_str = '单天价';
						break;
					case '部' :
						$return_str = '单次点播价';
						break;
				}
			}
		}

		return $return_str;

	}

	/**根据当前产品列表 筛选有效资费
	 * @param ARRAY 产品列表
	 * @param String 指定资费ID
	 * @return ARRAY 有效资费列表
	 * 			TRUE  无数据
	 * */
	static public function get_active_product_fee_list($dc,$products) {

		$fees=array();
		foreach ($products as $product_item){

			$product_fees=nl_product_fee::get_product_fee_by_product_id($dc,$product_item['nns_product_id']);

			if(!is_array($product_fees) || empty($product_fees)) continue;
			foreach ($product_fees as $product_fee){
				if ($product_fee['nns_product_fee_id']==$product_item['nns_product_fee_id']){
					//		暂未判断

					//	 				if (!empty($fee_item["nns_begindatetime"])){
					//	 						if (strtotime($fee_item["nns_begindatetime"]) > strtotime(date("Ymd"))){
					//	 							continue;
					//	 						}
					//	 					}
					//	 					if (!empty($fee_item["nns_enddatetime"])){
					//	 						if (strtotime($fee_item["nns_enddatetime"]) < strtotime(date("Ymd"))){
					//	 							continue;
					//	 						}
					//	 					}

					//	将授权接口提供的资费模式写入数组
		 			$product_fee['auth_gathering_mode']=$product_item['nns_gathering_mode'];
					array_push($fees,$product_fee);
					continue;
				}
			}
		}

//		若无有效资费，则返回FALSE
		if (count($fees)==0) return FALSE;



		return $fees;
	}

	/**
	 * 对资费进行算法排序 并返回最终资费
	 * @param 资费列表
	 * @return 最优化资费
	 * 			FALSE为空资费
	 */
	static public function _get_usort_product_fee($fee_arr){
	 	if ($fee_arr===FALSE || $fee_arr===TRUE) return FALSE;
	 	if (count($fee_arr)>0){
	 		if (usort($fee_arr,array(nl_product_fee_bo,"_usort_product_fee_rule"))){
	 			 return $fee_arr[0];
	 		}else{
	 			return FALSE;
	 		}
	 	}else{
	 		return FALSE;
	 	}
	 }


	 /**
	  * 资费排序规则算法
	  */
	 static private function _usort_product_fee_rule($fee1,$fee2){
	 	$backkey=0;
	 	if ($fee1===TRUE) return -1;
	 	if ($fee2===TRUE) return 1;
	 	$unit_sort_arr=array("年","月","日","部");

//	 	$gathering_mode1=empty($fee1["auth_gathering_mode"])?$fee1["nns_gathering_mode"]:$fee1["auth_gathering_mode"];
//	 	$gathering_mode2=empty($fee2["auth_gathering_mode"])?$fee2["nns_gathering_mode"]:$fee2["auth_gathering_mode"];

	 	if ($fee1["nns_price_type"]==$fee2["nns_price_type"]){
//	 		全部为周期收费时比较收费方式
	 		if ($fee1["nns_price_unit"]==$fee2["nns_price_unit"]){
	 			if ($fee1["nns_price"]<$fee2["nns_price"]){
	 				$backkey=-1;
	 			}else{
	 				$backkey=1;
	 			}
	 		}else{
		 		$fee1_num=0;
		 		$fee2_num=0;
		 		$num=0;
		 		foreach ($unit_sort_arr as $item){
		 			if ($fee1["nns_price_unit"]==$item){
		 				$fee1_num=$num;
		 			}
		 			if ($fee2["nns_price_unit"]==$item){
		 				$fee2_num=$num;
		 			}
		 			$num++;
		 		}
		 		if ($fee1_num<$fee2_num){
		 			$backkey=-1;
		 		}else{
		 			$backkey=1;
		 		}
	 		}
	 	}elseif ($fee1["nns_price_type"]=="RecurringCharge"){
	 		$backkey = -1;
	 	}else{
	 		$backkey = 1;
	 	}
	 	return $backkey;
	 }
/******************************************************************************
 *	单片计费相关方法
 *	filter_single_product_fee_by_video 判断是否采用单片计费模式
 *
 ********************************************************************************
 */


	 /**
	  * 判断是否采用单片计费模式
	  * 若常规资费为单片方式，则一律按单片计费模式进行收费
	  * @param DC模块
	  * @param $fee 常规资费
	  * @param $media_asset_id 媒资包ID
	  * @param $category_id 媒资包栏目ID
	  * @param $video_id 视频ID
	  * @return $return_fee 计算后资费
	  */
	static public function filter_single_product_fee_by_video(
			$dc,
	 		$fee,
	 		$user_id,
	 		$media_asset_id=null,
	 		$category_id=null,
	 		$video_id=null){



//	 	若视频ID和媒资包ID为空 则默认用常规资费
	 	if (empty($video_id) || empty($media_asset_id)) return $fee;

//		若常规资费为非免费且按周期计费方式	 则默认用常规资费
	 	if ($fee!==TRUE && $fee!==FALSE){
//	 		if ($fee['nns_price_unit']!='部'){

	 			return $fee;
//	 		}
	 	}



	 	$single_product_fee=self::filter_single_product_fee_by_user_id($dc, $user_id, $media_asset_id, $category_id,$video_id);



//	 	$single_product_fee=self::_get_usort_single_product_fee($single_product_fees);
		if ($single_product_fee===TRUE) return $fee;


//	 	var_dump($single_product_fee);
//	 	die;

	 	return $single_product_fee;
	 }

	/**根据当前单片计费产品列表 筛选有效资费
	 * @param ARRAY 产品列表
	 * @return ARRAY 有效资费
	 * 			TRUE  单片免费视频
	 * 			FALSE 无授权
	 * */
	 static private function filter_single_product_fee_by_user_id($dc, $user_id, $packet_id, $category_id,$video_id=null){
/**************************************
 *
 * STEP 1 根据单媒资判断计费
 * ********************************
 */

//		获取单片计费产品
	 	$single_product=nl_product::get_product_by_video_id($dc,$packet_id,$category_id,$video_id);

	 	if ($single_product===TRUE ) return TRUE;
//	 		查看用户是否有申请该系列单片计费

	 		foreach ($single_product as $key=>$single_product_item){
	 			$bool=self::_check_authorization_product($dc,$user_id,$single_product_item['nns_product_id']);
	 			if ($bool===FALSE){
	 				unset($single_product[$key]);
	 			}else{
	 				$single_product[$key]['nns_gathering_mode']=$bool;
	 			}
	 		}
//	 	var_dump($single_product);
//	 		 die;
//	 		按单片计费规则排序
	 	$single_product=self::_get_usort_product_fee_by_single($single_product);

//	 	获取资费信息
	 	if (is_array($single_product)){
	 		$single_product=self::get_active_product_fee_list($dc,$single_product);
	 	}



////	 		按单片计费规则排序
//	 		$single_product=self::_get_usort_product_fee_by_single($single_product);


	 		if ($single_product!==FALSE){
	 			return $single_product[0];
	 		}else{
	 			return FALSE;
	 		}

/**************************************
 *
 * STEP 2 根据媒资隶属判断计费
 * ********************************
 */

////		若单个媒资的单片计费产品为空，或查询失败 则对资源隶属栏目或包进行查询是否存在单片计费
//	 	$single_product=nl_product::get_product_by_category($dc,$packet_id,$category_id);
//
////	 	查询成功，则判断资费有效性，并根据ID查询资费内容
//	 	if (is_array($single_product)){
//	 		$single_product=self::get_active_product_fee_list($dc,$single_product);
//	 	}
//
////	 	资费有效,则直接返回单个媒资的单片资费
//	 	if ($single_product!==FALSE){
//	 		return $single_product[0];
//	 	}

	 }

	/**根据当前单片计费产品列表
	 * 按作用区域从小到大规则筛选有效资费
	 * @param ARRAY 产品列表
	 * @return ARRAY 有效资费列表
	 * 			TRUE  无数据
	 * */
	 static public function _get_usort_product_fee_by_single($fee_arr){
	 	if ($fee_arr===FALSE || $fee_arr===TRUE) return FALSE;

	 	if (count($fee_arr)>0){
	 		if (usort($fee_arr,array('nl_product_fee_bo',"_usort_single_product_fee_rule"))){
//				var_dump($fee_arr);
//				die;
	 			 return $fee_arr;
	 		}else{
	 			return FALSE;
	 		}
	 	}else{
	 		return FALSE;
	 	}
	 }


	/**
	  * 单片资费排序规则算法
	  */
	 static private function _usort_single_product_fee_rule($fee1,$fee2){
	 	if ($fee1===TRUE) return -1;
	 	if ($fee2===TRUE) return 1;
	 	if ($fee1['nns_bind_type']=='video') return -1;
	 	if ($fee2['nns_bind_type']=='video') return 1;
	 	if ($fee1['nns_price_unit']!='部' && $fee1['nns_price_unit']!='次') return 1;
	 	if ($fee2['nns_price_unit']!='部' && $fee2['nns_price_unit']!='次') return -1;

	 	if (strpos($fee1['nns_category_id'],$fee2['nns_category_id'])==0){
	 		if ($fee1['nns_category_id']===$fee2['nns_category_id']){
	 			if ($fee1["nns_price"]>$fee2["nns_price"]) return 1;
	 		}
	 		return -1;
	 	}else{
	 		return 1;
	 	}
	 }

	 /**检查产品是否被用户订购
	 * @param String 产品ID
	 * @return ARRAY 有效资费列表
	 * 			TRUE  无数据
	 *
	 * */
	 static private function _check_authorization_product($dc,$user_id,$product_id){
	 	$user_products = nl_product_bo :: get_active_product_list_by_user_id($dc, $user_id);
	 	if (is_array($user_products)){
		 	foreach ($user_products as $user_product){
		 		if ($user_product['nns_product_id']==$product_id){

		 			return $user_product['nns_gathering_mode'];
		 		}
		 	}
	 	}

	 	return FALSE;
	 }

    static public function get_price_code($product){
    	
    	if (!is_array($product)){
			if ($product===TRUE || $product=='free'){
				return np_authcode('TRUE','ENCODE');
			}else{
				return np_authcode('FALSE','ENCODE');
			}
    	}else{
//    		var_dump($product);die;
    		return np_authcode($product['nns_product_id'].
								'|#'.$product['nns_product_fee_id'].
								'|#'.$product['nns_price'].
								'|#'.$product['nns_unit'],'ENCODE');
    	}
	}
	
	 static public function decode_price_code($price_code){
	 	$product_fee=np_authcode($price_code,'DECODE');
    	if ($product_fee==='TRUE') return TRUE;
    	if ($product_fee==='FALSE') return FALSE;
    	$product_fee_arr=explode('|#',$product_fee);
    	return array(
    			'nns_product_id'=>$product_fee_arr[0],
    			'nns_product_fee_id'=>$product_fee_arr[1],
    			'nns_price'=>$product_fee_arr[2],
    			'nns_unit'=>$product_fee_arr[3]
    		);
	}
	
	 /**
 * LOGIC方法获取资费信息
 */
 static function get_product_fee($dc,$params){
 	$product_enabled=g_cms_config::get_g_config_value('g_product_enabled');
	$single_fee_enabled=g_cms_config::get_g_config_value('g_single_fee_enabled');
//	echo($single_fee_enabled);die;
	
	if (empty($product_enabled)) return TRUE;
	
	/**			获取所购买产品资费
	 * 			若指定了媒资包，则按媒资包进行资费
	 * 			若指定了服务包，则按服务包进行资费
	 */
	$fee=TRUE;
	if (!empty ($_GET["nns_media_assets_id"])) {
		$fee=self::filter_product_fee_by_user_id(
			$dc,
			$params["nns_user_id"],
			$params["nns_media_assets_id"],
			0,
			$params["nns_category_id"]
		);

//		若单片计费模式开启，则对单片计费进行检查
		if ($single_fee_enabled==1){
			$fee=self::filter_single_product_fee_by_video(
				$dc,
				$fee,
				$params["nns_user_id"],
				$params["nns_media_assets_id"],
				$params["nns_category_id"],
				$params['nns_video_id']
			);
		}
	}
	elseif (!empty ($_GET["nns_service_id"])) {
		$fee=self::filter_product_fee_by_user_id(
			$dc,
			$params["nns_user_id"],
			$params["nns_service_id"],
			1,
			$params["nns_category_id"]
		);
	}
	
	
	return $fee;
 }
	  /**
	 * 检查用户资费权限
	 * @param $user_id 用户ID
	 * @param $media_asset_id 媒资包ID
	 * @param $service_asset_id 服务包ID
	 * @param $category_id 包栏目ID
	 * @param $video_id 视频ID 可空
	 * @return $result_product_fee 已计算出的资费信息 state为状态
	 * 			state  0为授权播放
	 * 					1为未授权
	 * 					4为余额不足
	 */
	 static function check_product_fee_state($dc,$params){
			global $g_single_fee_enabled,$g_check_product_fee_state_enabled;
			if(!$g_check_product_fee_state_enabled) {//不启用资费检查
				$return_arr=array();
				$return_arr['state']=0;

				return $return_arr;
			}
			
			$user_id = $params['nns_user_id'];
			$media_asset_id = isset($params['nns_asset_id'])?$params['nns_asset_id']:null;
			$service_asset_id = isset($params['nns_service_id'])?$params['nns_service_id']:null;
			$category_id=$params['nns_category_id'];
			$video_id=$params['nns_video_id'];
			
			
			$result_product_fee=TRUE;
			if (!empty ($media_asset_id)) {
					$result_product_fee = nl_product_fee_bo :: filter_product_fee_by_user_id($dc, $user_id, $media_asset_id, 0, $category_id);
					
					//		若单片计费模式开启，则对单片计费进行检查
					if ($g_single_fee_enabled == 1) {
						$result_product_fee = nl_product_fee_bo :: filter_single_product_fee_by_video($dc,$result_product_fee,$user_id , $media_asset_id, $category_id, $video_id);
					}
				}
				elseif (!empty ($service_asset_id)) {
				$result_product_fee = nl_product_fee_bo :: filter_product_fee_by_user_id($dc, $user_id, $service_asset_id, 1, $category_id);
			}
			
            return self::get_product_fee_state($user_id,$result_product_fee);
	 }
	static public function get_product_fee_state($user_id,$result_product_fee){
			$state=0;
            //没有资费信息，则该片表示没有授权	‘free’代表免费视频
			if ($result_product_fee===FALSE || empty($result_product_fee)){
				$state=1;
			}

            //判断是否余额不足
			if (is_array($result_product_fee)){
				 $bool= self::get_product_fee_right($user_id,$result_product_fee);
				 if (!$bool){
				 	$state=4;
				 }
			}
			if (is_array($result_product_fee)){
				$result_product_fee['state']=$state;
				return $result_product_fee;
			}else{
				$return_arr=array();
				$return_arr['state']=$state;

				return $return_arr;
			}
	}	 
	static public function get_product_fee_right($user_id,$product_fee){
		$product_fee['user_id']=$user_id;
		include_once dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.'nn_cms_db'.DIRECTORY_SEPARATOR. "nns_boss/nns_boss_class.php";

		$bool=nns_boss_class::has_enough_prepaid($product_fee);
		$boss_inst=null;
		return $bool;
	} 
}
?>
