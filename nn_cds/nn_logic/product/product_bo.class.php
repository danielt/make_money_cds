<?php
include_once 'product_fee.class.php';
include_once 'product_user.class.php';
include_once 'product.class.php';
class nl_product_bo {

	/**根据当前用户产品 筛选有效产品
	 * @param DC模块
	 * @param $user_id 用户ID
	 * @return ARRAY 有效产品列表
	 * 			FALSE 查询产品失败
	 * 			TRUE  无数据
	 * */

	static public function get_active_product_list_by_user_id($dc, $user_id) {
		$result = nl_product_user :: get_product_list_by_user_id($dc, $user_id);
		if ($result === TRUE || $result === FALSE)
			return $result;

		foreach ($result as $key => $item) {
			if (!empty ($item["nns_begin_date"]) && !empty ($item["nns_end_date"])) {
				$now_date = new DateTime();
				if (!empty ($item["nns_begin_date"])) {
					$begin_date = new DateTime($item["nns_begin_date"]);
				} else {
					$begin_date = new DateTime();
					$begin_date->sub(new DateInterval('P1D'));
				}
				if (!empty ($item["nns_end_date"])) {
					$end_date = new DateTime($item["nns_end_date"]);
				} else {
					$end_date = new DateTime();
					$end_date->add(new DateInterval('P1D'));
				}
				if ($begin_date > $now_date || $end_date < $now_date) {
					unset ($result[$key]);
				}
			}
		}

		if (count($result) == 0)
			return TRUE;

		return $result;
	}
}
?>