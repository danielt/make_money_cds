<?php
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
class nl_product_fee {
	const CACHE_TIME_OUT=300;

/*
 * *********************************************
 * get_product_fee_by_product_id
 * 根据产品ID获取产品资费列表
 * ********************************************
 */

	/**
	 * 从DC模块根据产品ID获取产品资费列表
	 * @param dc DC模块
	 * @param String 产品ID
	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
	 * @return *
	 * 			ARRAY 资费信息列表
	 * 			FALSE 查询失败
	 * 			TRUE  查询成功，但无数据
	 */
	static public function get_product_fee_by_product_id($dc,$product_id,$policy=NL_DC_AUTO) {
		$result = null;
		if ($policy == NL_DC_AUTO){
			if ($dc->is_cache_enabled()) {
				$result = self::_get_product_fee_by_product_id_cache($dc->cache(),$product_id);
				if ($result === FALSE ) {
					$result = self::_get_product_fee_by_product_id_db($dc->db(),$product_id);
					self::_set_product_fee_by_product_id_cache($dc->cache(),$product_id,$result);
				}
			}
			else {
				$result = self::_get_product_fee_by_product_id_db($dc->db(),$product_id);
			}
		}elseif($policy == NL_DC_DB){
			$result = self::_get_product_fee_by_product_id_db($dc->db(),$product_id);
		}elseif($policy == NL_DC_CACHE){
			$result = self::_get_product_fee_by_product_id_cache($dc->cache(),$product_id);
		}
		return $result;
	}

	static private function _get_product_fee_by_product_id_db($db,$product_id) {
		$sql = 'select * from nns_product_fee ' .
    			'where nns_product_id=\''.$product_id.'\' ';
		return nl_query_by_db($sql, $db);
	}

	static private function _get_product_fee_by_product_id_cache($cache,$product_id) {
		if ($cache===NULL) return FALSE;
		return unserialize($cache->get('product_fee|#'.$product_id));
	}

	static private function _set_product_fee_by_product_id_cache($cache,$product_id,$data) {
		if ($cache===NULL) return FALSE;
		return $cache->set('product_fee|#'.$product_id,serialize($data),self::CACHE_TIME_OUT);
	}
	
	static public function delete_product_fee_by_product_id_cache($dc,$product_id) {
		if ($dc->cache()===NULL) return FALSE;
		return $dc->cache()->delete('product_fee|#'.$product_id);
	}
	

}
?>