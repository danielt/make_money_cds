<?php
/**
 * 内容提供商管理
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_product_vod_bind extends nl_public
{
	static $base_table='nns_product_vod_bind';
	static $arr_filed = array(
			'nns_id',
			'nns_vod_id',
			'nns_product_id',
			'nns_asset_import_id',
	        'nns_import_source',
			'nns_cp_id',
			'nns_create_time',
	        'nns_modify_time',
	);
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function query_by_id($dc,$nns_id)
	{
		$sql = "select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
		return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function query_by_cp_id($dc,$nns_cp_id)
	{
	    $sql = "select * from " . self::$base_table . " where nns_cp_id='{$nns_cp_id}'";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function query_by_cp_source_id($dc,$nns_cp_id,$nns_import_source)
	{
	    $sql = "select * from " . self::$base_table . " where nns_cp_id='{$nns_cp_id}' and nns_import_source='{$nns_import_source}' limit 1";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result[0]) && !empty($result[0]) && is_array($result[0])) ? $result[0] : null;
	    return self::return_data(0,'OK',$result);
	}
	
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function query_by_cp_source_domain_id($dc,$video_type,$nns_cp_id,$nns_import_source,$nns_domain)
	{
	    $sql = "select * from " . self::$base_table . " where nns_cp_id='{$nns_cp_id}' and nns_import_source='{$nns_import_source}' and nns_domain='{$nns_domain}' and nns_type='{$video_type}' limit 1";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result[0]) && !empty($result[0]) && is_array($result[0])) ? $result[0] : null;
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function query_by_cp_request_source_id($dc,$nns_cp_id,$nns_import_source)
	{
	    $sql = "select * from " . self::$base_table . " where nns_cp_id='{$nns_cp_id}' and nns_request_source='{$nns_import_source}' limit 1";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result[0]) && !empty($result[0]) && is_array($result[0])) ? $result[0] : null;
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function query_by_ids($dc,$nns_id)
	{
	    $str_id = is_array($nns_id) ? implode("','", $nns_id) : $nns_id;
	    $sql = "select * from " . self::$base_table . " where nns_id in('{$nns_id}')";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 添加内容提供商数据
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function add($dc,$params)
	{
	    $params['nns_id'] = (isset($params['nns_id']) && !empty($params['nns_id'])) ? $params['nns_id'] : np_guid_rand();
		$params['nns_create_time'] = $params['nns_modify_time'] = date('Y-m-d H:i:s');
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$sql = self::make_insert_sql(self::$base_table, $params);
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 修改内容提供商数据
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function edit($dc,$params,$nns_id)
	{
		if(strlen($nns_id)<0)
		{
			return self::return_data(1,'内容提供商数据_id为空');
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$params['nns_modify_time'] = date("Y-m-d H:i:s");
		$sql = self::make_update_sql(self::$base_table, $params,array('nns_id'=>$nns_id));
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}

	
	/**
	 * 查询内容提供商数据列表
	 * @param object $dc 数据库对象
	 * @param array $params 查询参数
	 * @param array $page_info 分页信息
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function query($dc,$params,$page_info=null)
	{
		if(isset($params['where']) && !empty($params['where']))
		{
			foreach ($params['where'] as $where_key=>$where_val)
			{
				self::$str_where.=" {$where_key} = '{$where_val}' and ";
			}	
		}
		if(isset($params['like']) && !empty($params['like']))
		{
			foreach ($params['like'] as $like_key=>$like_val)
			{
				self::$str_where.=" {$like_key} like '%{$like_val}%' and ";
			}
		}
		self::$str_where = (strlen(self::$str_where) > 5) ? trim(self::$str_where,'and ') : '';
		$str_limit = self::make_page_limit($page_info);
		$sql="select * from " . self::$base_table . " " . self::$str_where . " order by nns_create_time desc {$str_limit} ";
		$sql_count = "select count(*) as count from " . self::$base_table . " " . self::$str_where;
		$result_count = nl_query_by_db($sql_count,  $dc->db());
		$result = nl_query_by_db($sql, $dc->db());
		self::init_where();
		if(!$result || !$result_count)
		{
			return self::return_data(1,"查询数据失败".$sql.$sql_count);
		}
		return self::return_data(0,"查询数据成功".$sql_count.$sql,$result,$result_count);
	}
	
	/**
	 * 查询是否存在
	 * @param unknown $dc
	 * @param string $str_param
	 */
	static public function query_unique($dc,$str_param=null)
	{
	    $sql="select count(*) as count from " . self::$base_table;
	    if(isset($str_param) && strlen($str_param))
	    {
	        $str_param = trim($str_param);
	    }
	    $is_set_where = substr($str_param, 0,5);
	    if(strtolower($is_set_where) != 'where')
	    {
	        $str_param = " where ".$str_param;
	    }
	    $str_param = (strtolower($is_set_where) != 'where') ? " where ".$str_param : " ".$str_param;
	    $sql = $sql.$str_param;
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,"查询数据失败".$sql);
	    }
	    return self::return_data(0,"查询数据成功".$sql,$result);
	}
	
	/**
	 * 查询所有
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function query_all($dc)
	{
		$sql="select * from " . self::$base_table;
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		return self::return_data(0,'OK',$result);
	}
	
	
	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function delete($dc,$nns_id)
	{
		if(strlen($nns_id) <1)
		{
			return self::return_data(1,'内容提供商数据_id为空');
		}
		$sql = "delete from " . self::$base_table . " where nns_id='{$nns_id}' ";
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		return self::return_data(0,'OK');
	}

	/**
	 * 通过产品ID联查绑定媒资关系
	 * @param $dc
	 * @param $nns_id
	 * @return array
	 */
	static public function query_ex_info_by_id($dc, $nns_id)
	{
		$sql = "select c.nns_order_name as nns_product_name,b.nns_name as nns_vod_name,a.* from " . self::$base_table . " as a left join nns_vod as b on a.nns_vod_id = b.nns_id left join nns_product as c on  a.nns_product_id= c.nns_id where a.nns_product_id = '{$nns_id}'";
		$sql_count = "select count(*) from " . self::$base_table . " as a left join nns_vod as b on a.nns_vod_id = b.nns_id left join nns_product as c on  a.nns_product_id= c.nns_id where a.nns_product_id = '{$nns_id}'";
		$result_count = nl_query_by_db($sql_count,  $dc->db());
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result || !$result_count)
		{
			return self::return_data(1,"查询数据失败".$sql.$sql_count);
		}
		return self::return_data(0,"查询数据成功".$sql_count.$sql,$result,$result_count);
	}
}
