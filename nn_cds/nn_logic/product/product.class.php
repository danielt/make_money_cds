<?php
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
class nl_product {
	const CACHE_TIME_OUT=300;
/*
 * *********************************************
 * get_product_by_video_id
 * 获取媒资包单片计费信息
 * ********************************************
 */

	/**
	 * 从DC模块获取媒资包单片计费产品
	 * @param dc DC模块
	 * @param String 媒资包ID
	 * @param String 媒资包栏目ID
	 * @param String 视频ID
	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
	 * @return *
	 * 			ARRAY 单片计费产品
	 * 			FALSE 查询失败
	 * 			TRUE  查询成功，但无数据
	 */
	static public function get_product_by_video_id($dc,$media_asset_id,$category_id,$video_id,$policy=NL_DC_AUTO) {
//		$result = null;
//		if ($policy == NL_DC_AUTO){
//			if ($dc->is_cache_enabled()) {
//				$result = self::_get_product_list_by_user_id_cache($dc->cache(),$video_id);
//				if ($result === FALSE ) {
//					$result = self::_get_product_list_by_user_id_db($dc->db(),$video_id);
//					self::_set_product_list_by_user_id_cache($dc->cache(),$video_id,$result);
//				}
//			}
//			else {
//				$result = self::_get_product_list_by_user_id_db($dc->db(),$video_id);
//			}
//		}elseif($policy == NL_DC_DB){
//			$result = self::_get_product_list_by_user_id_db($dc->db(),$video_id);
//		}elseif($policy == NL_DC_CACHE){
//			$result = self::_get_product_list_by_user_id_cache($dc->cache(),$video_id);
//		}
		$result=self::_get_product_by_video_id_db($dc->db(),$media_asset_id,$category_id,$video_id);
		return $result;
	}

	static private function _get_product_by_video_id_db($db,$media_asset_id,$category_id,$video_id) {
//		$sql = 'select nns_product_id,nns_product_fee_id from nns_product_video ' .
//    			'where nns_assist_id=\''.$media_asset_id.'\' '.
//    			'and nns_category_id=\''.$category_id.'\' ' .
//    			'and nns_video_id=\''.$video_id.'\'';

		$category_id=empty($category_id)?'1000':$category_id;
		
		$sql = 'select nns_product_id,' .
				'nns_product_fee_id,' .
				'nns_video_id,' .
				'nns_bind_type,' .
				'nns_category_id ' .
				'from nns_product_video ' .
    			'where nns_assist_id=\''.$media_asset_id.'\' '.
    			'and (LOCATE(nns_category_id,\''.$category_id.'\')=1 ' .
    			'and nns_bind_type=\'category\') ' .
    			'or ( nns_category_id=\''.$category_id.'\' ' .
    			'and nns_video_id=\''.$video_id.'\' ' .
    			' and nns_bind_type=\'video\' ) ' ;
		
		return nl_query_by_db($sql, $db);
	}

//	static private function _get_product_by_video_id_cache($cache,$user_id) {
//		if ($cache===NULL) return FALSE;
//		return unserialize($cache->get('product_user|#'.$user_id));
//	}
//
//	static private function _set_product_by_video_id_cache($cache,$user_id,$data) {
//		if ($cache===NULL) return FALSE;
//		return $cache->set('product_user|#'.$user_id,serialize($data),self::CACHE_TIME_OUT);
//	}
//	
//	static public function delete_product_by_video_id_cache($dc,$user_id) {
//		if ($dc->cache()===NULL) return FALSE;
//		return $dc->cache()->delete('product_|#'.$user_id);
//	}

/*
 * *********************************************
 * epg_count_product_content
 * 检查产品是否包含终端栏目
 * ********************************************
 */

	/**
	 * 检查产品是否包含终端栏目
	 * @param dc DC模块
	 * @param String 包ID
	 * @param String 包类型 0为媒资包，1为服务包
	 * @param String 栏目ID
	 * @param String 产品ID 可为NULL 
	 * #为NULL代表查询该包栏目信息是否免费，
	 * 	当查询结果为TRUE时，代表该包栏目不属于任何产品包，则免费。
	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
	 * @return *
	 * 			ARRAY 符合条件的包栏目信息列表
	 * 			FALSE 查询失败
	 * 			TRUE  查询成功，但无数据
	 * 			
	 */
	static public function epg_count_product_content($dc,$packet_id,$packet_type,$category_id,$product_id=NULL,$policy=NL_DC_AUTO) {
		$result=self::_epg_count_product_content_by_db($dc->db(),$packet_id,$packet_type,$category_id,$product_id);
		return $result;
	}

	static private function _epg_count_product_content_by_db($db,$packet_id,$packet_type,$category_id,$product_id=NULL) {
		$sql = 'select * from nns_product_content ' .
				'where  nns_content_id = \''.$packet_id.'\' ' .
				'and nns_content_type= \''.$packet_type.'\' ' .
				'and LOCATE(nns_category_id,\''.$category_id.'\')=1 ';
				
				if (!empty($product_id)){
					$sql.='and nns_product_id = \''.$product_id.'\' ';
				}	
				
		return nl_query_by_db($sql, $db);;
	}
	
	
//		/**
//	 * 从DC模块获取包，栏目上生效的单片计费产品
//	 * @param dc DC模块
//	 * @param String 媒资包ID
//	 * @param String 媒资包栏目ID
//	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
//	 * @return *
//	 * 			ARRAY 单片计费产品
//	 * 			FALSE 查询失败
//	 * 			TRUE  查询成功，但无数据
//	 */
//	static public function get_product_by_category($dc,$media_asset_id,$category_id,$policy=NL_DC_AUTO) {
//		$result=self::_get_product_by_category_db($dc->db(),$media_asset_id,$category_id);
//		return $result;
//	}
//	
//	static private function _get_product_by_category_db($db,$media_asset_id,$category_id){
//		$category_id=empty($category_id)?'1000':$category_id;
//		
//		$sql = 'select nns_product_id,nns_product_fee_id,nns_category_id from nns_product_video ' .
//    			'where nns_assist_id=\''.$media_asset_id.'\' '.
//    			'and LOCATE(nns_category_id,\''.$category_id.'\')=1 ' ;
//
//		return nl_query_by_db($sql, $db);
//	}

}
?>