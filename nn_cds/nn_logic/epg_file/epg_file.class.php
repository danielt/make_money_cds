<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/11/8 11:27
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_epg_file extends nl_public
{
    static public $table = "nns_epg_file";
    static public $arr_filed = array(
        'nns_id',
        'nns_epg_file_set_id',
        'nns_import_id',
        'nns_cp_id',
        'nns_deleted',
        'nns_dest_path',
        'nns_dest_file',
        'nns_md5',
        'nns_url',
        'nns_create_time',
        'nns_modify_time',
    );

    /**
     * 添加
     * @param $dc
     * @param $params
     * @return array
     */
    static public function add($dc, $params)
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (empty($params) || !is_array($params))
        {
            return self::return_data(1, '参数为空');
        }
        if (empty($params['nns_id']) || strlen($params['nns_id']) < 1)
        {
            $params['nns_id'] = np_guid_rand();
        }
        $params['nns_create_time'] = $params['nns_modify_time'] = date('Y-m-d H:i:s');
        $sql = self::make_insert_sql(self::$table, $params);
        $result = nl_execute_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        return self::return_data(0, 'ok:' . $sql);
    }
    /**
     * 多个条件进行修改
     * @param $dc
     * @param $params
     * @param $where
     * @return array
     */
    static public function modify($dc,$params,$where)
    {
        if(empty($where) || !is_array($where) || empty($params) || !is_array($params))
        {
            return self::return_data(1,'参数错误');
        }
        $update_str = '';
        $where_str = '';
        $params['nns_modify_time'] = date('Y-m-d H:i:s',time());
        foreach ($params as $key=>$value)
        {
            $update_str .= " {$key} = '{$value}',";
        }
        $update_str = rtrim($update_str,',');
        foreach ($where as $k=>$val)
        {
            if($k == 'in' && is_array($val))
            {
                foreach ($val as $l => $v)
                {
                    $where_str .= " {$l} in ('" . implode("'.'", $v) . "') AND ";
                }
            }
            elseif(!empty($val) || $val == '0')
            {
                $where_str .= " {$k} = '{$val}' AND ";
            }
        }
        $where_str = rtrim($where_str,' AND ');
        $sql = "update " . self::$table . " set " . $update_str . " where " . $where_str;
        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'失败'.$sql);
        }
        return self::return_data(0,'成功');
    }

    /**
     * 根据指定单个id进行修改
     *
     * @param object $dc
     *            数据库对象
     * @param array $params
     *            数据数组
     * @param array|string $nns_id
     *            GUID
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     *         @date 2016-09-24
     */
    static public function edit($dc, $params, $nns_id)
    {
        if (isset($params['nns_id']))
        {
            unset($params['nns_id']);
        }
        $params = self::make_nns_pre($params);
        if (strlen($nns_id) < 0)
        {
            return self::return_data(1, 'CDN直播频道队列guid为空');
        }
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (empty($params))
        {
            return self::return_data(1, '参数为空');
        }
        $params['nns_modify_time'] = date("Y-m-d H:i:s");
        $sql = self::make_update_sql(self::$table, $params, array('nns_id' => $nns_id));
        $result = nl_execute_by_db($sql, $dc->db());
        if (! $result)
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        return self::return_data(0, 'ok');
    }

    /**
     * @param $dc
     * @param $params
     * @param null $since
     * @param null $num
     * @param bool $is_count
     * @return array
     */
    static public function get_epg_file_set($dc,$params,$since=null,$num=null,$is_count=false)
    {
        $where = '';
        $str_limit = '';
        if(is_array($params))
        {
            foreach ($params as $key=>$value)
            {
                if($key === 'nns_dest_file' && strlen($value) > 0)
                {
                    $where .= " {$key} like '%{$value}%' AND ";
                }
                elseif(strlen($value) > 0)
                {
                    $where .= " {$key} = '{$value}' AND ";
                }
            }
        }
        if(!empty($where))
        {
            $where = rtrim($where," AND ");
            $where = " where " . $where;
        }
        if(strlen($since) > 0 && strlen($num) > 0)
        {
            $str_limit = " limit {$since},{$num}";
        }
        $sql = "select * from " . self::$table . $where . " order by nns_create_time desc " . $str_limit;
        $data = nl_query_by_db($sql, $dc->db());
        if($data === false)
        {
            return self::return_data(1,'数据库执行失败'.$sql);
        }
        if($data === true)
        {
            $data = array();
        }
        $count_info = array();
        if($is_count)
        {
            $count_sql = "select count(1) as count from " . self::$table . $where;
            $count_info = nl_query_by_db($count_sql, $dc->db());
            if($count_info === false)
            {
                return self::return_data(1,'数据库执行失败'.$sql);
            }
        }
        return self::return_data(0,'ok',$data,$count_info);
    }

    /**
     * 查询单个列表
     * @param $dc
     * @param $nns_id
     * @return array
     */
    static public function query_by_id($dc,$nns_id)
    {
        if(empty($nns_id))
        {
            return self::return_data(1,'id为空');
        }
        $sql="select * from " . self::$table . " where nns_id='{$nns_id}' limit 1";
        $result = nl_query_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,"失败".$sql);
        }
        return self::return_data(0,"成功",$result);
    }

    /**
     * 依据条件查询信息
     * @param $dc
     * @param $params
     * @return array
     */
    static public function query_by_condition($dc, $params)
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (!is_array($params) || empty($params))
        {
            return self::return_data(1, '查询条件为空不允许查询');
        }
        $sql = self::make_query_sql(self::$table, $params);
        $result = nl_query_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (isset($result) && !empty($result)) ? $result : null;
        return self::return_data(0, 'OK' . $sql, $result);
    }

    /**
     * 根据id批量获取epgfile信息
     * @param $dc
     * @param $arr_id
     * @param bool $is_query_ex
     * @return array
     */
    static public function query_data_by_id($dc,$arr_id,$is_query_ex=true)
    {
        if(empty($arr_id))
        {
            return self::return_data(1,'参数错误');
        }
        $str_ids = implode("','",$arr_id);
        $sql = "select * from " . self::$table . " where nns_id in ('{$str_ids}')";
        $vod_re = nl_query_by_db($sql,$dc->db());
        if(!is_array($vod_re))
        {
            return self::return_data(1,'数据库执行失败或数据查询为空：'.$sql);
        }
        $result = array();
        foreach ($vod_re as $info)
        {
            $result[$info['nns_id']]['base_info'] = $info;
            $result[$info['nns_id']]['ex_info'] = array();
        }
        if(empty($result))
        {
            return self::return_data(1,'vod数据查询结果为空');
        }
        return self::return_data(0,'ok'.$sql,$result);
    }

}