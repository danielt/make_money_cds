<?php
/**
 * VOD_INDEX后台操作LOGIC
 * @author LZ
 *
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_vod_index_v2 extends nl_public
{
	static $base_table='nns_vod_index';
	static $base_ex_table="nns_vod_index_ex";
	
	/**
	 * 根据来源ID与注入ID查询数据是否存在，查询VOD_INDEX数据
	 * @param object $dc 数据库操作对象
	 * @param string $import_id 注入ID
	 * @param string $source_id 上游来源ID
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据'),
	 * @author zhiyong.luo
	 * @date 2017-08-16
	 */
	static public function query_vod_index_info_by_import_id($dc,$import_id,$source_id='')
	{
		if(empty($import_id))
		{
			return self::return_data(1,'注入ID为空');
		}
		$sql = "select * from " . self::$base_table . " where nns_import_id='{$import_id}'";
		if(!empty($source_id))
		{
			$sql .= " and nns_cp_id='{$source_id}'";
		}
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok',$result);
	}
	/**
	 * 添加vod_index信息
	 * @param unknown $dc
	 * @param unknown $params
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 */
	static public function add($dc,$params)
	{
		if(!is_array($params) || empty($params))
		{
			return self::return_data(1,'参数错误');
		}
		$params['nns_id'] = (isset($params['nns_id']) && !empty($params['nns_id'])) ? $params['nns_id'] : np_guid_rand();
		$params['nns_create_time'] = $params['nns_modify_time'] = date('Y-m-d H:i:s');
		$sql = self::make_insert_sql(self::$base_table, $params);
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	/**
	 * 修改vod_index媒资信息
	 * @param unknown $dc
	 * @param unknown $params
	 * @param unknown $id
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 */
	static public function edit($dc,$params,$id)
	{
		if(empty($id) || !is_array($params) || empty($params))
		{
			return self::return_data(1,'参数错误');
		}
		$params['nns_modify_time'] = date("Y-m-d H:i:s");
		$sql = self::make_update_sql(self::$base_table, $params,array('nns_id'=>$id));
	
		$result = nl_execute_by_db($sql, $dc->db());
	
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok'.$sql);
	}
    /**
     * 根据分集ID获取媒资分集信息
     * @param $dc
     * @param $arr_id 分集ID一维数组
     * @param $is_query_ex 是否查询媒资分集扩展信息
     * @return array
     */
    static public function query_vod_index_by_id($dc,$arr_id,$is_query_ex=true)
    {
        if(empty($arr_id))
        {
            return self::return_data(1,'参数错误');
        }
        $str_ids = implode("','",$arr_id);
        $sql = "select * from " . self::$base_table . " where nns_id in ('{$str_ids}')";
        $vod_re = nl_query_by_db($sql,$dc->db());
        if(!is_array($vod_re))
        {
            return self::return_data(1,'数据库执行失败或数据查询为空：'.$sql);
        }
        $vod_ex = array();
        if($is_query_ex)
        {
            $ex_sql = "select ex.*,v.nns_id from " . self::$base_ex_table . " as ex left join " . self::$base_table . " as v on v.nns_import_id=ex.nns_vod_index_id and v.nns_cp_id=ex.nns_cp_id where v.nns_id in ('{$str_ids}')";
            $vod_ex_re = nl_query_by_db($ex_sql,$dc->db());
            if(!is_array($vod_ex_re))
            {
                $vod_ex_re = array();
            }
            if(!empty($vod_ex_re))
            {
                foreach ($vod_ex_re as $ex_info)
                {
                    $vod_ex[$ex_info['nns_id']][$ex_info['nns_key']] = $ex_info['nns_value'];
                }
            }
        }
        $result = array();
        foreach ($vod_re as $info)
        {
            $result[$info['nns_id']]['base_info'] = $info;
            if(isset($vod_ex[$info['nns_id']]))
            {
                $result[$info['nns_id']]['ex_info'] = $vod_ex[$info['nns_id']];
            }
            else
            {
                $result[$info['nns_id']]['ex_info'] = array();
            }
        }
        if(empty($result))
        {
            return self::return_data(1,'vod数据查询结果为空');
        }
        return self::return_data(0,'ok'.$sql,$result);
    }
}