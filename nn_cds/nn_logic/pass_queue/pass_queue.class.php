<?php
/**
 * 透传队列
 * @author zhiyong.luo
 *
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_pass_queue extends nl_public
{
	static public $table = "nns_pass_queue";
	static public $arr_filed = array(
	    'nns_id',
	    'nns_action',
	    'nns_type',
	    'nns_message_id',
	    'nns_org_id',
	    'nns_cp_id',
	    'nns_audit',
	    'nns_pause',
	    'nns_status',
	    'nns_content',
	    'nns_result_desc',
	    'nns_queue_time',
	    'nns_create_time',
	    'nns_modify_time',
    );
	/**
	 * 添加透传队列
	 * @param object $dc
	 * @param array $params
	 * @author zhiyong.luo 
	 * @date 2017-02-23
	 */
	static public function add($dc,$params)
	{
		if(empty($params) || !is_array($params))
		{
			return self::return_data(1,'参数错误');
		}
		$params['nns_id'] = np_guid_rand();
		$params['nns_create_time'] = date('Y-m-d H:i:s',time());
		$params['nns_modify_time'] = $params['nns_create_time'];
		$params['nns_content'] = addslashes($params['nns_content']);
		$result = nl_db_insert($dc->db(), self::$table, $params);
		if($result)
		{
			return self::return_data(0,'OK');
		}
		else
		{
			self::return_data(1,'mysql execute failed');
		}
	}
	/**
	 * 修改反馈消息
	 * @param object $dc
	 * @param array $params
	 * @param array $where
	 * @author zhiyong.luo 
	 * @date 2017-02-23
	 */
	static public function modify($dc,$params,$where)
	{
		if(empty($where) || !is_array($where) || empty($params) || !is_array($params))
		{
			return self::return_data(1,'参数错误');
		}
		$update_str = '';
		$where_str = '';
		$params['nns_modify_time'] = date('Y-m-d H:i:s',time());
		foreach ($params as $key=>$value)
		{
			$update_str .= " {$key} = '{$value}',";
		}
		$update_str = rtrim($update_str,',');
		foreach ($where as $k=>$val)
		{
			if($k == 'in' && is_array($val))
			{
				foreach ($val as $l => $v)
				{
					$where_str .= " {$l} in ('" . implode("'.'", $v) . "') AND ";
				}
			}
			elseif(!empty($val) || $val == '0')
			{
				$where_str .= " {$k} = '{$val}' AND ";
			}			
		}
		$where_str = rtrim($where_str,' AND ');
		$sql = "update " . self::$table . " set " . $update_str . " where " . $where_str;
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'失败'.$sql);
		}
		return self::return_data(0,'成功');
	}
	/**
	 * 获取反馈消息
	 * @param object $dc
	 * @param array $params
	 * @param string $since
	 * @param string $num
	 * @param boolean $is_count
	 * @author zhiyong.luo 
	 * @date 2017-02-23
	 */
	static public function get_pass_queue($dc,$params,$since=null,$num=null,$is_count=false)
	{
		$where = '';
		$str_limit = '';
		if(is_array($params))
		{
			foreach ($params as $key=>$value)
			{
				if($key === 'nns_content' && strlen($value) > 0)
				{
					$where .= " {$key} like '%{$value}%' AND ";
				}
				elseif(strlen($value) > 0)
				{
					$where .= " {$key} = '{$value}' AND ";
				}
			}
		}
		if(!empty($where))
		{
			$where = rtrim($where," AND ");
			$where = " where " . $where;
		}
		if(strlen($since) > 0 && strlen($num) > 0)
		{
			$str_limit = " limit {$since},{$num}";
		}
		$sql = "select * from " . self::$table . $where . " order by nns_create_time desc " . $str_limit;
		$data = nl_query_by_db($sql, $dc->db());
		if($data === false)
		{
			return self::return_data(1,'数据库执行失败'.$str_sql);
		}
		if($data === true)
		{
			$data = array();
		}
		$count_info = array();
		if($is_count)
		{
			$count_sql = "select count(1) as count from " . self::$table . $where;
			$count_info = nl_query_by_db($count_sql, $dc->db());
			if($count_info === false)
			{
				return self::return_data(1,'数据库执行失败'.$str_sql);
			}
		}
		return self::return_data(0,'ok',$data,$count_info);
	}
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author zhiyong.luo
	 * @date 2017-02-26
	 */
	static public function query_by_id($dc,$nns_id)
	{
		if(empty($nns_id))
		{
			return self::return_data(1,'id为空');
		}
		$sql="select * from " . self::$table . " where nns_id='{$nns_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,"失败".$sql);
		}
		return self::return_data(0,"成功",$result);
	}
	/**
	 * 定时器获取队列
	 */
	static public function get_pass_queue_for_timer($dc,$params,$limit)
	{
		$where = '';
		$str_limit = ' limit ' . $limit;
		if(is_array($params))
		{
			foreach ($params as $key=>$value)
			{
				$where .= " {$key} = '{$value}' AND ";
			}
		}
		if(!empty($where))
		{
			$where = rtrim($where," AND ");
			$where = " where " . $where;
		}
		$sql = "select * from " . self::$table . $where . " order by nns_queue_time asc " . $str_limit;
		$data = nl_query_by_db($sql, $dc->db());
		if($data === false)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		if($data === true)
		{
			$data = array();
		}
		return self::return_data(0,'ok',$data);
	}
    /**
     * 依据条件查询主媒资信息
     * @param $dc
     * @param $params
     * @return array
     */
    static public function query_by_condition($dc, $params)
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (!is_array($params) || empty($params))
        {
            return self::return_data(1, '查询条件为空不允许查询');
        }
        $sql = self::make_query_sql(self::$table, $params);
        $result = nl_query_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (isset($result) && !empty($result)) ? $result : null;
        return self::return_data(0, 'OK' . $sql, $result);
    }
}