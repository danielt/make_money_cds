<?php
/**
 * @author guanghu.zhou
 * @refactor date 2013-04-16
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
class nl_group{
	static private $prefix = "group_logic#|#";

	/**
	 * @subpackage 获取集团信息
	 * @param $param = array('id'=>$id)
	 * @param dc obj
	 * @return array
	 * @notice 原来函数名nns_db_group_info
	 */
	public function get_group_info($dc,$param,$policy) {
		$key = implode('-', $param);		

		switch ($policy) {
		case NL_DC_DB :
			$result = self::_get_group_info_by_db($dc -> db(), $param);
			break;
		case NL_DC_CACHE :
			$result = $dc -> cache()->get(self::$prefix.$key);
			break;
		case NL_DC_AUTO :
			if ($dc -> is_cache_enabled()) {
				$result = $dc -> cache()->get(self::$prefix.$key);
				if ($result === FALSE) {
					$result = self::_get_group_info_by_db($dc -> db(), $param);
					$dc -> cache()->set(self::$prefix.$key, $result);
				}
			} else {
				$result = self::_get_group_info_by_db($dc -> db(), $param);
			}
			break;
		}
		return $result;
	}

	/**
	 * @subpackage 获取集团信息
	 * @param param array
	 * @param dc obj
	 * @return array
	 */
	static private function _get_group_info_by_db($db,$param){
		extract($param);//解析参数
		$result = array ();
		if (! isset ( $id ) || empty ( $id )) {
			$result = array( 'ret'=>NNS_ERROR_ARGS, 'msg'=>'group info param error' );
			return $result;
		}
		$str_sql = "select * from nns_group where nns_id='$id'";
		$data = nl_db_get_all($str_sql,$db);
		return $result = array(
			'ret'=>0,
			'data'=>$data,
			'msg'=>null,
		);
	}
	static public function get_group_by_user_id($dc,$user_id){
		$sql = "select g.nns_id as nns_id,g.nns_name as nns_name from nns_user_group ug left join  nns_group g on ug.nns_group_id=g.nns_id where ug.nns_user_id='".$user_id."'";
		return nl_db_get_all ( $sql,$dc->db() );
	}	
}
