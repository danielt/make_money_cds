<?php

include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
include_once dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'nn_cms_db' .
        DIRECTORY_SEPARATOR . 'nns_common' . DIRECTORY_SEPARATOR . 'nns_db_guid_class.php';
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'label' . DIRECTORY_SEPARATOR . 'video_label_bo.class.php';

class nl_asset_bind {
        //缓存时间
        //static private $cache_time = 3600;

        const CACHE_TIME_OUT = 300;
        const SAVE_KEY = 'assists_bind';
        const SAVE_STRING = '|';

        function add_asset_bind($dc, $param) {
                if (count($param) != 4)
                        return false;
                $data = self::_get_param($param);
                $val = $data['value'];
                $type = $data['type'];
                $sql = "SELECT *
FROM `nns_asset_category_bind`
WHERE `nns_assists_id` = '{$param['nns_assists_id']}'
 AND `nns_assists_cid` = '{$param['nns_assists_cid']}'
AND `nns_assists_value` = '{$val}'
        AND `nns_assists_type` ='{$type}'
LIMIT 1";
                $result = nl_query_by_db($sql, $dc->db());
                if (is_array($result))
                        return false;
                $guid = np_guid_rand();
                $sql = "INSERT INTO `nns_asset_category_bind` (`nns_id`, `nns_assists_id`,`nns_assists_cid`, `nns_assists_value`,`nns_assists_type`) VALUES 
                        ('$guid', '{$param['nns_assists_id']}','{$param['nns_assists_cid']}','$val','$type')";
                $result = nl_execute_by_db($sql, $dc->db());
                return $result;
        }

        static private function _get_param($param) {

                $data['value'] = $param['nns_assists_bind_id'] . self::SAVE_STRING . $param['nns_assists_bind_cid'];
                $data['type'] = self::SAVE_KEY;
                return $data;
        }

        function get_asset_bind($dc, $param) {
                if (count($param) != 2)
                        return false;
                $data = self:: _get_param($param);
                $type = $data['type'];
                $sql = "SELECT *
FROM `nns_asset_category_bind`
WHERE `nns_assists_id` = '{$param['nns_assists_id']}' AND  `nns_assists_cid` = '{$param['nns_assists_cid']}' AND `nns_assists_type` = '$type'";
                $result = nl_query_by_db($sql, $dc->db());
                if (!is_array($result))
                        return false;
                foreach ($result as &$v) {
                        $exp = explode(self::SAVE_STRING, $v['nns_assists_value']);
                        $v['nns_assists_bind_id'] = $exp[0];
                        $v['nns_assists_bind_cid'] = $exp[1];
                }
                return $result;
        }

        function del_asset($dc, $nns_id) {
                if (strlen($nns_id) != 32)
                        return false;
                $sql = "DELETE FROM `nns_asset_category_bind` WHERE `nns_asset_category_bind`.`nns_id` ='$nns_id'  LIMIT 1";
                $result = nl_execute_by_db($sql, $dc->db());
                return $result;
        }

}