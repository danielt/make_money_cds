<?php
/*
 * Created on 2012-11-27
 * @author cb
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';

class nl_template_label{
	const CACHE_TIME_OUT = 300;
	/**
	 * 新增template_label标签
	 * @param array $params 
	 */
	static function add_template_label($dc,$params,$policy = NL_DC_AUTO){

		$time = date('Y-m-d H:i:s');
		$params['nns_create_time'] = $time;
		$params['nns_modify_time'] = $time;
		$params['nns_id'] = $params['nns_id'];
		$params['nns_name'] = $params['nns_name'];
		$params['nns_summary'] = $params['nns_summary'];
		$params['nns_label'] = str_replace("'",'"',$params['nns_label']);
		
	 	$sql =self::_set_insert_sql($params,'nns_template_label');

	 	$result = nl_execute_by_db($sql,$dc->db());
	 	
	 	//判断是否需要建立记录缓存
	 	if($policy === NL_DC_AUTO){
			self::_set_cache_by_ids ($dc,array($params['nns_id']));
	 	}
	 	return $result;
	}
	/**
	 * 修改 template_label 标签
	 * @param string $name 
	 * @param array $params
	 */
	static function modify_template_label($dc,$id,$params,$policy = NL_DC_AUTO){
		$sql = self::_set_update_sql($params,'nns_template_label');
	  	$sql .= ' WHERE nns_id =\''.$id.'\'';
	  	$result = nl_execute_by_db($sql,$dc->db());
	  	//如果删除成功，判断是否需要删除对应的机构缓存和记录缓存
		self::_del_cache_by_ids($dc,array($id),$policy);
	  	
	  	return $result;
	}
	/**
	  * 删除template_label标签
	  * @param object $dc
	  * @param string $name
	  * @param int $policy 读取策略
	  */
	static function delete_template_label($dc,$ids,$policy = NL_DC_AUTO){
	 	$db=$dc->db();
	 	if(is_array($ids)){
	 		$id_str = implode("','",$ids);
	 	}
	 	if(!empty($ids)){
	 		$sql="delete from nns_template_label where nns_id in ('{$id_str}')";
			$result=nl_execute_by_db($sql,$db);
			if($result){
				//删除记录缓存
				foreach($ids as $id){
					self::_del_cache_by_ids($dc,array($id),NL_DC_DB);
				}
			}
			return $result;
	 	}
	 	return false;
		
	}
	/**
	 * 获取template_label 标签详情
	 * @param DC $dc
	 * @param string $name
	 */
	static function get_template_label_info($dc,$id,$policy = NL_DC_AUTO){
		switch($policy){
			case NL_DC_AUTO: $result=self:: _get_cache_by_ids ($dc,array($id),$policy);break;
			case NL_DC_DB: $result=self::_get_template_label_info_db ($dc->db(),$id);break;
			case NL_DC_CACHE: $result=self::_get_cache_by_ids ($dc,array($id),$policy);break;
		}
		return $result;
		
	}
	static private function _get_template_label_info_db($db,$id){
		$sql = "select * from nns_template_label where nns_id = '{$id}' limit 1";

		return nl_query_by_db($sql,$db);
	}
	/**
	 * 获取template_label列表
	 * @param DC $dc
	 */
	static function get_template_label_list($dc,$since = 0,$num=20){
		$sql="select * from nns_template_label order by nns_modify_time desc limit {$since},{$num}";
		return nl_query_by_db($sql,$dc->db());
	}
	/**
	 * 统计template_label总数
	 * @param DC $dc
	 */
	static function  count_template_label_list($dc){
		$sql="select count(1) as num from nns_template_label";
		return nl_query_by_db($sql,$dc->db());
	} 
	
	/**
	 * 通过template_label 名字设置缓存
	 * param DC $dc
	 * param array $ids 
	 */
	static private function _set_cache_by_ids($dc,$ids = array()){
		if(!$dc->is_cache_enabled()) return false;
		$db = $dc->db();
	  	$cache = $dc->cache();
		//循环从数据库获取记录，然后存入缓存
		if(is_array($ids)){
			foreach($ids as $id){
				$sql = "select * from nns_template_label where nns_id='{$id}'";
				$result = nl_query_by_db($sql,$db);
				if($result !== true && $result !== false){
					$cache->set('nns_template_label|#'.$id,$result,self::CACHE_TIME_OUT);
				}elseif($result === false){
					return false;
				}
			}
		}
		return false;
	}
	/**
	 * 根据ids获取记录缓存
	 * @param DC $dc
	 * @param array $ids
	 * @param array 需要获取的数据字段
	 * @param string 是否在没有获取到数据的情况下从数据库读取并存入缓存
	 */
	  static private function _get_cache_by_ids ($dc,$ids = array(),$policy = NL_DC_AUTO) {
			$cache = $dc->cache();
			$db = $dc->db();
			$data = array();
			if(is_array($ids)){
			$i = 0;
			foreach($ids as $id){
				if($dc->is_cache_enabled()){
					//获取缓存，成功返回二维数组
					$result = $cache->get('nns_template_label|#'.$id);
					if($result){
						$data[$i] = $result[0];
					}else if($policy === NL_DC_AUTO){
						$sql="select * from nns_template_label where nns_id='{$id}'";
						$result = nl_query_by_db($sql, $db);
						self::_set_cache_by_ids($dc,$arr=array($id));
						if($result !== false&&$result !== true){
							$data[$i] = $result[0];
						}
					}
				}else{
					$sql="select * from nns_template_label where nns_id='{$id}'";
						$result = nl_query_by_db($sql, $db);
						self::_set_cache_by_ids($dc,$arr=array($id));
						if($result !== false&&$result !== true){
							$data[$i] = $result[0];
						}
				}
				
				$i++;
			}
			return $data;
		}else{
			return false;
		}
	  }
	/**
	 * 根据ids删除记录缓存
	 * @param DC $dc
	 * @param array $ids
	 * @param string 是删除还是更新缓存默认是删除，如果是NL_DC_AUTO则为更新
	 */
	 static private function _del_cache_by_ids($dc,$ids = array(),$policy = NL_DC_AUTO){
	 	if(!$dc->is_cache_enabled()) return false;
	 	$cache = $dc->cache();
		if(is_array($ids)){
			if($policy === NL_DC_AUTO){
				self::_set_cache_by_ids ($dc,$ids);
			}elseif($policy === null){
				foreach($ids as $id){
					$cache->delete('nns_template_label|#'.$id);
				}
			}
		}else{
			return false;
		}
	 }
	
	/**
	 * 构造 INSERT sql语句
	 * @param array $params  插入数据格式：字段名=>字段值
	 * @param string $table_name 表名字
	 */
	static private function _set_insert_sql($params, $table_name) {
		$ins_sql = 'INSERT INTO `' . $table_name . '` (';
		foreach ($params as $key => $val) {
//			$val = $val;
			$ins_key[] =$key;
			$ins_val[] = '\'' . $val . '\'';
		}
		$ins_sql .= implode(',', $ins_key) . ') VALUES(' . implode(',', $ins_val) . ')';
		return $ins_sql;
	}
	
	/**
	   * 构造 UPDATE sql语句
	   * @param array $params
	   * @param string $table_name
	   */
	 static private function _set_update_sql($params,$table_name){
	  	$upd_sql = 'UPDATE `'.$table_name.'` SET ';
	  	foreach($params as $key=>$val){
//			$val = htmlentities($val);
	  		$sql_arr[] = $key.'=\''.$val.'\'';
	  	}
	  	$upd_sql .= implode(',',$sql_arr);
	  	return $upd_sql;
	 }
}
?>