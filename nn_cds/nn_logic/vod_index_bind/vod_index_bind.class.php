<?php
/**
 * 主媒资注入状态
 * @author zhiyong.luo
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_vod_index_bind extends nl_public
{
	static public $table = "nns_vod_index_bind";
	/**
	 * 添加主媒资与分集绑定关系
	 * @param object $dc
	 * @param array $params
	 * @author zhiyong.luo 
	 * @date 2017-02-23
	 */
	static public function add($dc,$params)
	{
		if(empty($params) || !is_array($params))
		{
			return self::return_data(1,'参数错误');
		}
		$params['nns_id'] = np_guid_rand();
		$params['nns_create_time'] = date('Y-m-d H:i:s',time());
		$params['nns_modify_time'] = $params['nns_create_time'];
		$result = nl_db_insert($dc->db(), self::$table, $params);
		if($result)
		{
			return self::return_data(0,'OK');
		}
		else
		{
			self::return_data(1,'mysql execute failed');
		}
	}
	/**
	 * 修改主媒资与分集绑定关系
	 * @param object $dc
	 * @param array $params
	 * @param array $where
	 * @author zhiyong.luo 
	 * @date 2017-02-23
	 */
	static public function modify($dc,$params,$where)
	{
		if(empty($where) || !is_array($where) || empty($params) || !is_array($params))
		{
			return self::return_data(1,'参数错误');
		}
		$update_str = '';
		$where_str = '';
		$params['nns_modify_time'] = date('Y-m-d H:i:s',time());
		foreach ($params as $key=>$value)
		{
			$update_str .= " {$key} = '{$value}',";
		}
		$update_str = rtrim($update_str,',');
		foreach ($where as $k=>$val)
		{
			if($k == 'in' && is_array($val))
			{
				foreach ($val as $l => $v)
				{
					$where_str .= " {$l} in ('" . implode("'.'", $v) . "') AND ";
				}
			}
			elseif(!empty($val) || $val == '0')
			{
				$where_str .= " {$k} = '{$val}' AND ";
			}			
		}
		$where_str = rtrim($where_str,' AND ');
		$sql = "update " . self::$table . " set " . $update_str . " where " . $where_str;
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'失败'.$sql);
		}
		return self::return_data(0,'成功');
	}
	/**
	 * 获取主媒资与分集绑定关系
	 * @param object $dc
	 * @param array $params
	 * @param string $since
	 * @param string $num
	 * @param boolean $is_count
	 * @author zhiyong.luo 
	 * @date 2017-02-23
	 */
	static public function get_list($dc,$params,$since=null,$num=null,$is_count=false)
	{
		$where = '';
		$str_limit = '';
		if(is_array($params))
		{
			foreach ($params as $key=>$value)
			{
				if(strlen($value) > 0)
				{
					$where .= " {$key} = '{$value}' AND ";
				}
			}
		}
		if(!empty($where))
		{
			$where = rtrim($where," AND ");
			$where = " where " . $where;
		}
		if(strlen($since) > 0 && strlen($num) > 0)
		{
			$str_limit = " limit {$since},{$num}";
		}
		$sql = "select * from " . self::$table . $where . " order by nns_create_time desc " . $str_limit;
		$data = nl_query_by_db($sql, $dc->db());
		if($data === false)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		if($data === true)
		{
			$data = array();
		}
		$count_info = array();
		if($is_count)
		{
			$count_sql = "select count(1) as count from " . self::$table . $where;
			$count_info = nl_query_by_db($count_sql, $dc->db());
			if($count_info === false)
			{
				return self::return_data(1,'数据库执行失败'.$sql);
			}
		}
		return self::return_data(0,'ok',$data,$count_info);
	}

    /**
     * 删除分集绑定关系
     * @param $dc //数据库缓存操作对象
     * @param $params = array(
     *      'nns_vod_id'    => '主媒资ID',
     *      'nns_index_id'  => '分集ID',
     *      'nns_index'     => '分集号',
     *      ......nns_vod_index_bind表其他字段......
     * )
     * @return array(
     *      'ret'   => '状态码，0表示成功，其他表示失败',
     *      'reason'=> '状态描述'
     * )
     * @author chunyang.shu
     * @date 2017-09-15
     */
    public static function delete($dc, $params)
    {
        // 检查参数
        if (!is_array($params) || empty($params))
        {
            return self::return_data(1, '删除参数为空');
        }

        // 组装删除条件
        $str_where = '';
        foreach ($params as $str_key => $str_value)
        {
            $str_where .= " {$str_key}='{$str_value}' AND ";
        }
        if (strlen($str_where) > 0)
        {
            $str_where = "WHERE " . rtrim(trim($str_where), 'AND');
        }

        // 拼装sql
        $str_sql = "DELETE FROM `" . self::$table ."` {$str_where}";

        // 删除数据
        $result = nl_execute_by_db($str_sql, $dc->db());
        if (false === $result)
        {
            return self::return_data(1, '数据库执行失败');
        }
        return self::return_data(0, 'OK');
    }
}