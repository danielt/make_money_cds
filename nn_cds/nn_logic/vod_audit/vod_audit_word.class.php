<?php
/**
 * 资源库数据
 */
include_once dirname(dirname(__FILE__))."/public.class.php";
class nl_vod_audit_word extends nl_public
{
	//敏感词库
	public static $base_table='nns_vod_audit_word';
	
	/**
	 * 查询敏感词
	 * @param object $dc 数据库操作对象
	 * @param array $params 查询的参数
	 * @param array $page_info 分页信息
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function query($dc,$params,$page_info=null)
	{
		$sql="select * from " . self::$base_table . " ";
		$sql_count="select count(*) as count from " . self::$base_table . " ";
		if(isset($params['nns_id']) && !empty($params['nns_id']))
		{
			self::$str_where.=" nns_id = '{$params['nns_id']}' and ";
		}
		if(isset($params['nns_word']) && !empty($params['nns_word']))
		{
			self::$str_where.=" nns_word like '%{$params['nns_word']}%' and ";
		}
		if(isset($params['nns_org_id']) && !empty($params['nns_org_id']))
		{
			self::$str_where.=" nns_org_id = '{$params['nns_org_id']}' and ";
		}
		if(isset($params['nns_enabled']) && strlen($params['nns_enabled'] > 0))
		{
			self::$str_where.=" nns_enabled ='{$params['nns_enabled']}' and ";
		}
		if(isset($params['nns_begin_time']) && !empty($params['nns_begin_time']))
		{
			self::$str_where.=" nns_create_time >'{$params['nns_begin_time']}' and ";
		}
		if(isset($params['nns_end_time']) && !empty($params['nns_end_time']))
		{
			self::$str_where.=" nns_create_time <'{$params['nns_end_time']}' and ";
		}
		self::$str_where = (strlen(self::$str_where) > 5) ? self::$str_where : '';
		self::$str_where = rtrim(self::$str_where,'and ');
		$sql.= self::$str_where . " order by nns_create_time desc " . self::make_page_limit($page_info);
		$sql_count.= self::$str_where;
		$result_count = nl_query_by_db($sql_count, $dc->db());
		$result = nl_query_by_db($sql, $dc->db());
		self::$str_where = "where";
		if(!$result)
		{
			return self::return_data(1,'sql error:'.$sql,null);
		}
		if(!$result_count || !isset($result_count[0]['count']))
		{
			return self::return_data(1,'sql count error:'.$sql_count,null);
		}
		self::$page_info['total_count'] = isset($result_count[0]['count']) ? intval($result_count[0]['count']) : 0;
		return self::return_data(0,'ok',$result,self::$page_info);
	}
	
	/**
	 * 查询单词组合列表
	 * @param object $dc 数据库操作对象
	 * @param string $sp_id 运营商id
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据'), multitype:NULL number string , multitype:NULL , multitype:a r y s t i n g , array>
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function query_word_list($dc,$sp_id)
	{
		if(empty($sp_id))
		{
			return self::return_data(2,'params error empty sp_id');
		}
		$sql_query="select group_concat(nns_word separator '|') as nns_words from " . self::$base_table . " where nns_org_id='{$sp_id}' and nns_enabled='1' ";
		$result_query = nl_query_by_db($sql_query, $dc->db());
		if(!$result_query)
		{
			return self::return_data(1,'sql error:'.$sql_query);
		}
		return self::return_data(0,'ok',$result_query);
	}
	
	/**
	 * 添加敏感词库
	 * @param object $dc 数据库操作对象
	 * @param string $word 敏感词
	 * @param string $sp_id 运营商id
	 * @param number $enable 是否可当敏感词（0 不可用做敏感词 | 1可用做敏感词 ）
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function add($dc,$word,$sp_id,$enable=1)
	{
		if(empty($word))
		{
			return self::return_data(2,'params error empty word');
		}
		if(empty($sp_id))
		{
			return self::return_data(2,'params error empty sp_id');
		}
		$sql_query="select count(*) as count from " . self::$base_table . " where nns_word='{$word}' and nns_org_id='{$sp_id}' ";
		$result_query = nl_query_by_db($sql_query, $dc->db());
		if(!$result_query || !is_array($result_query) || !isset($result_query[0]['count']))
		{
			return self::return_data(1,'db query error:'.$sql_query);
		}
		$date = date('Y-m-d H:i:s');
		if($result_query[0]['count'] == 0)
		{
			$enable = intval($enable);
			$nns_id = np_guid_rand();
			$sql_insert ="insert into " . self::$base_table . "(nns_id,nns_word,nns_enabled,nns_create_time,nns_modify_time,nns_org_id) values".
					"('{$nns_id}','{$word}','{$enable}','{$date}','{$date}','{$sp_id}')";
			$result_insert = nl_execute_by_db($sql_insert, $dc->db());
			if(!$result_insert)
			{
				return self::return_data(1,'db insert error:'.$sql_insert);
			}
			return self::return_data(0,'insert');
		}
		$sql_update="update " . self::$base_table . " set nns_modify_time='{$date}',nns_enabled='{$enable}' where nns_word='{$word}' and nns_org_id='{$sp_id}'";
		$result_update=nl_execute_by_db($sql_update, $dc->db());
		if(!$result_update)
		{
			return self::return_data(1,'db update error:'.$sql_update);
		}
		return self::return_data(0,'update');
	}
	
	/**
	 * 敏感词修改
	 * @param object $dc 数据库操作对象 
	 * @param string $nns_id guid
	 * @param string $word 敏感词
	 * @param string $sp_id 运营商id
	 * @param number $enable 是否可当敏感词（0 不可用做敏感词 | 1可用做敏感词 ）
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据'),
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function modify($dc,$nns_id,$word,$enable=1,$sp_id)
	{
		if(empty($word))
		{
			return self::return_data(2,'params error empty word');
		}
		if(empty($nns_id))
		{
			return self::return_data(2,'params error empty id');
		}
		if(empty($sp_id))
		{
			return self::return_data(2,'params error empty sp_id');
		}
		$sql_query="select count(*) as count from " . self::$base_table . " where nns_word='{$word}' and nns_org_id='{$sp_id}' and nns_id !='{$nns_id}'";
		$result_query = nl_query_by_db($sql_query, $dc->db());
		if(!$result_query || !is_array($result_query) || !isset($result_query[0]['count']))
		{
			return self::return_data(1,'db query error:'.$sql_query);
		}
		$date = date('Y-m-d H:i:s');
		if($result_query[0]['count'] == 0)
		{
			$enable = intval($enable);
			$sql_update="update " . self::$base_table . " set nns_modify_time='{$date}',nns_word='{$word}',nns_enabled='{$enable}' where nns_id='{$nns_id}'";
			$result_update = nl_execute_by_db($sql_update, $dc->db());
			if(!$result_update)
			{
				return self::return_data(1,'db update error:'.$sql_insert);
			}
			return self::return_data(0,'ok');
		}
		return self::return_data(0,'word is exsist');
	}
	
	
	
	/**
	 * 删除数据
	 * @param object $dc 数据库操作对象
	 * @param string|array $nns_ids guid
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function del($dc,$nns_ids)
	{
		if(empty($nns_ids))
		{
			return self::return_data(2,'params error empty id');
		}
		if(is_array($nns_ids))
		{
			$str_id = implode("','", $nns_ids);
		}
		else if(is_string($nns_ids))
		{
			$str_id = $nns_ids;
		}
		else
		{
			return self::return_data(2,'params error is not string or array');
		}
		if(empty($str_id))
		{
			return self::return_data(0,'ok');
		}
		$sql = "delete from " . self::$base_table . " where nns_id in('{$str_id}')";
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'db delete error:'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	
}
