<?php
/**
 * 资源库数据
 */
include_once dirname(dirname(__FILE__))."/public.class.php";
class nl_vod_audit_log extends nl_public
{
	//敏感词库
	public static $base_table='nns_vod_audit_log';
	
	/**
	 * 审核队列的注入日志
	 * @param object $dc 数据库操作对象
	 * @param array $params 查询的参数
	 * @param array $page_info 分页信息
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function query($dc,$params,$page_info=null)
	{
		$sql="select * from " . self::$base_table . " ";
		$sql_count="select count(*) as count from " . self::$base_table . " ";
		if(!empty($params['nns_operator']))
		{
			self::$str_where.=" nns_operator like '%{$params['nns_operator']}%' and ";
		}
		if(!empty($params['nns_id']))
		{
			self::$str_where.=" nns_id = '{$params['nns_id']}' and ";
		}
		if(!empty($params['nns_vod_name']))
		{
			self::$str_where.=" nns_vod_name like '%{$params['nns_vod_name']}%' and ";
		}
		if(isset($params['nns_cp_id']) && strlen($params['nns_cp_id']) > 0)
		{
			self::$str_where.=" nns_cp_id = '{$params['nns_cp_id']}' and ";
		}
		if(!empty($params['nns_action']))
		{
			self::$str_where.=" nns_action = '{$params['nns_action']}' and ";
		}
		if(!empty($params['nns_state']))
		{
			self::$str_where.=" nns_state = '{$params['nns_state']}' and ";
		}
		if(isset($params['nns_begin_time']) && !empty($params['nns_begin_time']))
		{
			self::$str_where.=" nns_create_time >'{$params['nns_begin_time']}' and ";
		}
		if(isset($params['nns_end_time']) && !empty($params['nns_end_time']))
		{
			self::$str_where.=" nns_create_time <'{$params['nns_end_time']}' and ";
		}
		if(isset($params['nns_org_id']) && strlen($params['nns_org_id']) > 0)
		{
			self::$str_where.=" nns_org_id = '{$params['nns_org_id']}' and ";
		}
		self::$str_where = (strlen(self::$str_where) > 5) ? self::$str_where : '';
		self::$str_where = rtrim(self::$str_where,'and ');
		$sql.= self::$str_where . " order by nns_create_time desc " . self::make_page_limit($page_info);
		$sql_count.= self::$str_where;
		$result_count = nl_query_by_db($sql_count, $dc->db());
		$result = nl_query_by_db($sql, $dc->db());
		self::$str_where = "where";
		if(!$result)
		{
			return self::return_data(1,'sql error:'.$sql,null);
		}
		if(!$result_count || !isset($result_count[0]['count']))
		{
			return self::return_data(1,'sql count error:'.$sql_count,null);
		}
		self::$page_info['total_count'] = isset($result_count[0]['count']) ? intval($result_count[0]['count']) : 0;
		return self::return_data(0,'ok',$result,self::$page_info);
	}
	
	/**
	 * 添加审核操作日志
	 * @param object $dc 数据库操作对象
	 * @param string $nns_operator 操作人
	 * @param string $nns_vod_name 操作内容
	 * @param string $nns_cp_id cp_id
	 * @param string $nns_action 内容动作（add 新增 | destory 删除）
	 * @param string $nns_state 内容操作动作（pass审核通过|refused不通过|online上线|unline下线
	 * @param string $nns_org_id 运营商id
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据'), multitype:NULL number string , multitype:NULL , multitype:a r y s t i n g , array>
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function add($dc,$nns_operator=null,$nns_vod_name,$nns_cp_id,$nns_action,$nns_state,$nns_org_id)
	{
		if(empty($nns_vod_name))
		{
			return self::return_data(2,'params error empty vod_name');
		}
		if(empty($nns_cp_id))
		{
			return self::return_data(2,'params error empty cp_id');
		}
		if(empty($nns_action))
		{
			return self::return_data(2,'params error empty action');
		}
		if(empty($nns_state))
		{
			return self::return_data(2,'params error empty state');
		}
		if(empty($nns_org_id))
		{
			return self::return_data(2,'params error empty org_id');
		}
		if(strlen($nns_operator) < 1)
		{
			$nns_operator = 'import admin';
		}
		$date=date("Y-m-d H:i:s");
		$nns_id = np_guid_rand();
		//$nns_vod_name = htmlspecialchars($nns_vod_name, ENT_QUOTES);
		$sql_insert ="insert into " . self::$base_table . "(nns_id,nns_operator,nns_vod_name,nns_cp_id,nns_action,nns_state,nns_create_time,nns_org_id) values".
				"('{$nns_id}','{$nns_operator}','{$nns_vod_name}','{$nns_cp_id}','{$nns_action}','{$nns_state}','{$date}','{$nns_org_id}')";
		//var_dump($sql_insert);die;
		$result_insert = nl_execute_by_db($sql_insert, $dc->db());
		if(!$result_insert)
		{
			return self::return_data(1,'db insert error:'.$sql_insert);
		}
		return self::return_data(0,'insert');
	}
}
