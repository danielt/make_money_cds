<?php
/**
 * 获取流程指向的媒资审核数据
 */
include_once dirname(dirname(__FILE__))."/public.class.php";
class nl_vod_audit extends nl_public
{
	//流程指向表
	public static $base_table='nns_vod_audit';
	//主媒资表
	public static $vod_table='nns_vod';
	//分集表
	public static $index_table='nns_vod_index';
	//片源表
	public static $media_table='nns_vod_media';
	
	/**
	 * 添加流程指向数据
	 * @param object $dc 数据库操作对象
	 * @param string $import_id 运营媒资注入id
	 * @param string $org_id 运营商id
	 * @param string $cp_id cp id
	 * @param number $check 审核状态   0 未审核 | 1 已审核 | 2 取消审核
	 * @param number $state 上下线状态  0 未处理| 1 已上线 | 2 已下线
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function add_vod_audit($dc,$nns_name,$import_id,$org_id,$cp_id,$action,$video_type,$check=null,$state=null,$parent_import_id=null)
	{
		if(empty($nns_name))
		{
			return self::return_data(2,'params error empty vod name');
		}
		if(empty($import_id))
		{
			return self::return_data(2,'params error empty import id');
		}
		if(empty($org_id))
		{
			return self::return_data(2,'params error empty org id');
		}
		if(empty($cp_id))
		{
			return self::return_data(2,'params error empty cp id');
		}
		if(empty($action))
		{
			return self::return_data(2,'params error empty action');
		}
		if(empty($video_type))
		{
			return self::return_data(2,'params error empty video type');
		}
		$parent_import_id = empty($parent_import_id) ? '' : $parent_import_id;
		$action = ($action != 'destroy') ? 'add' : $action;
		$sql_query="select count(*) as count from " . self::$base_table . " where nns_video_type='{$video_type}' and nns_import_id='{$import_id}' and nns_org_id='{$org_id}' and nns_cp_id='{$cp_id}' ";
		$result_query = nl_query_by_db($sql_query, $dc->db());
		if(!$result_query)
		{
			return self::return_data(1,'db query error:'.$sql_query);
		}
		else
		{
			$sql_query_one="select nns_id from " . self::$base_table . " where nns_video_type='{$video_type}' and nns_import_id='{$import_id}' and nns_org_id='{$org_id}' and nns_cp_id='{$cp_id}' limit 1";
			$result_query_one = nl_query_by_db($sql_query_one, $dc->db());
			if(!$result_query_one)
			{
				return self::return_data(1,'db query error:'.$sql_query_one);
			}
			$nns_id = isset($result_query_one[0]['nns_id']) ? $result_query_one[0]['nns_id'] : null;
		}
		$date = date('Y-m-d H:i:s');
		if($result_query[0]['count'] == 0)
		{
			$check = (strlen($check) > 0) ? intval($check) : 0;
// 			$state = 0;
			$state = (strlen($state) > 0) ? intval($state) : 0;
			$nns_id = np_guid_rand();
			$sql_insert ="insert into " . self::$base_table . "(nns_id,nns_name,nns_import_id,nns_check,nns_state,nns_org_id,nns_create_time,nns_modify_time,nns_cp_id,nns_action,nns_video_type,nns_parent_import_id) values".
					"('{$nns_id}','{$nns_name}','{$import_id}','{$check}','{$state}','{$org_id}','{$date}','{$date}','{$cp_id}','{$action}','{$video_type}','{$parent_import_id}')";
			$result_insert = nl_execute_by_db($sql_insert, $dc->db());
			if(!$result_insert)
			{
				return self::return_data(1,'db insert error:'.$sql_insert);
			}
			return self::return_data(0,'insert',$nns_id);
		}
		$str_check= (strlen($check) > 0) ? ",nns_check='".intval($check)."' " : ",nns_check='0'";
		$str_state= (strlen($state) > 0) ? ",nns_state='".intval($state)."' " : ",nns_state='0'";
		$sql_update="update " . self::$base_table . " set nns_parent_import_id='{$parent_import_id}',nns_modify_time='{$date}',nns_action='{$action}',nns_name='{$nns_name}'{$str_check}{$str_state} where nns_import_id='{$import_id}' and nns_org_id='{$org_id}' and nns_cp_id='{$cp_id}' and nns_video_type='{$video_type}'";
		$result_update=nl_execute_by_db($sql_update, $dc->db());
		if(!$result_update)
		{
			return self::return_data(1,'db update error:'.$sql_update);
		}
		return self::return_data(0,'update',$nns_id);
	}
	
	/**
	 * 查询审核队列 是否存在
	 * @param object $dc  数据库操作对象
	 * @param string $import_id 注入id
	 * @param string $org_id 运营商id
	 * @param string $cp_id cp_id
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function query_exsist_vod_audit($dc,$import_id,$org_id ,$cp_id)
	{
		if(empty($import_id))
		{
			return self::return_data(2,'params error empty import id');
		}
		if(empty($org_id))
		{
			return self::return_data(2,'params error empty org id');
		}
		if(empty($cp_id))
		{
			return self::return_data(2,'params error empty cp id');
		}
		$sql_query="select * from " . self::$base_table . " where nns_import_id='{$import_id}' and nns_org_id='{$org_id}' and nns_cp_id='{$cp_id}' ";
		$result_query = nl_query_by_db($sql_query, $dc->db());
		if(!$result_query)
		{
			return self::return_data(1,'db query error:'.$sql_query);
		}
		return self::return_data(0,'ok',$result_query);
	}
	
	/**
	 * 修改流程指向的审核状态
	 * @param object $dc 数据库操作对象
	 * @param string $nns_id guid
	 * @param number $check 审核状态   0 未审核 | 1 已审核 | 2 取消审核
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function modify_vod_audit_checkstate($dc,$nns_id,$check,$nns_filed)
	{
		if(empty($nns_filed) || !in_array($nns_filed, array('check','line')))
		{
			return self::return_data(2,'params error empty nns_filed'.var_export($nns_filed,true));
		}
		if(empty($nns_id))
		{
			return self::return_data(2,'params error empty id');
		}
		if(strlen($check) < 1)
		{
			return self::return_data(2,'params error empty check');
		}
		if(is_array($nns_id))
		{
			$str_nns_id = implode("','", $nns_id);
		}
		else if(is_string($nns_id))
		{
			$str_nns_id = $nns_id;
		}
		else
		{
			return self::return_data(2,'params error nns_id is not array or string');
		}
		$check = intval($check);
		$date=date("Y-m-d H:i:s");
		if($nns_filed == 'check')
		{
			$sql = "update " . self::$base_table . " set nns_check='{$check}',nns_modify_time='{$date}' where nns_id in('{$nns_id}')";
		}
		else
		{
			$sql = "update " . self::$base_table . " set nns_state='{$check}',nns_modify_time='{$date}' where nns_id in('{$nns_id}')";
		}
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'db update error:'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 修改流程指向的上下线状态
	 * @param object $dc 数据库操作对象
	 * @param string $nns_id  guid
	 * @param number $state 上下线状态  0 未处理| 1 已上线 | 2 已下线
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function modify_vod_audit_state($dc,$nns_id,$state)
	{
		if(empty($nns_id))
		{
			return self::return_data(2,'params error empty id');
		}
		if(strlen($state) < 1)
		{
			return self::return_data(2,'params error empty state');
		}
		$state = intval($state);
		$date=date("Y-m-d H:i:s");
		$sql = "update " . self::$base_table . " set nns_state='{$state}',nns_modify_time='{$date}' where nns_id='{$nns_id}'";
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'db update error:'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 删除流程指向数据
	 * @param object $dc 数据库操作对象
	 * @param string|array $nns_ids guid
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function del_vod_audit($dc,$nns_ids)
	{
		if(empty($nns_ids))
		{
			return self::return_data(2,'params error empty id');
		}
		if(is_array($nns_ids))
		{
			$str_id = implode("','", $nns_ids);
		}
		else if(is_string($nns_ids))
		{
			$str_id = $nns_ids;
		}
		else
		{
			return self::return_data(2,'params error is not string or array');
		}
		if(empty($str_id))
		{
			return self::return_data(0,'ok');
		}
		$sql = "delete from " . self::$base_table . " where nns_id in('{$str_id}')";
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'db delete error:'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 查询流程指向信息
	 * @param object $dc 数据库操作对象
	 * @param array $params 查询数组
	 * @param string $page_info 分页信息
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function get_vod_audit_list($dc,$params,$page_info=null)
	{
		$sql="select * from " . self::$base_table . " ";
		$sql_count="select count(*) as count from " . self::$base_table . " ";
		if(isset($params['nns_id']) && !empty($params['nns_id']))
		{
			if(is_array($params['nns_id']))
			{
				$str_nns_id=implode("','", $params['nns_id']);
				self::$str_where.=" nns_id in('{$str_nns_id}') and ";
			}
			else if(is_string($params['nns_id']))
			{
				self::$str_where.=" nns_id = '{$params['nns_id']}' and ";
			}
			else
			{
				return self::return_data(2,'params id error');
			}
		}
		if(isset($params['nns_video_type']) && !empty($params['nns_video_type']))
		{
			self::$str_where.=" nns_video_type = '{$params['nns_video_type']}' and ";
		}
		if(isset($params['nns_parent_import_id']) && !empty($params['nns_parent_import_id']))
		{
			self::$str_where.=" nns_parent_import_id = '{$params['nns_parent_import_id']}' and ";
		}
		if(!empty($params['nns_org_id']))
		{
			self::$str_where.=" nns_org_id = '{$params['nns_org_id']}' and ";
		}
		if(isset($params['nns_cp_id']) && strlen($params['nns_cp_id']) > 0)
		{
			self::$str_where.=" nns_cp_id = '{$params['nns_cp_id']}' and ";
		}
		if(isset($params['nns_action']) && !empty($params['nns_action']))
		{
			self::$str_where.=" nns_action = '{$params['nns_action']}' and ";
		}
		if(isset($params['nns_import_id']) && !empty($params['nns_import_id']))
		{
			self::$str_where.=" nns_import_id = '{$params['nns_import_id']}' and ";
		}
		if(isset($params['nns_name']) && !empty($params['nns_name']))
		{
			self::$str_where.=" nns_name like '%{$params['nns_name']}%' and ";
		}
		if(isset($params['nns_check']) && strlen($params['nns_check']) > 0)
		{
			self::$str_where.=" nns_check ='{$params['nns_check']}' and ";
		}
		if(isset($params['nns_state']) && strlen($params['nns_state']) > 0)
		{
			self::$str_where.=" nns_state ='{$params['nns_state']}' and ";
		}
		if(isset($params['nns_begin_time']) && !empty($params['nns_begin_time']))
		{
			self::$str_where.=" nns_create_time >'{$params['nns_begin_time']}' and ";
		}
		if(isset($params['nns_end_time']) && !empty($params['nns_end_time']))
		{
			self::$str_where.=" nns_create_time <'{$params['nns_end_time']}' and ";
		}
		self::$str_where = (strlen(self::$str_where) > 5) ? self::$str_where : '';
		self::$str_where = rtrim(self::$str_where,'and ');
		$sql.= self::$str_where . " order by nns_create_time desc " . self::make_page_limit($page_info);
		$sql_count.= self::$str_where;
		$result_count = nl_query_by_db($sql_count, $dc->db());
		$result = nl_query_by_db($sql, $dc->db());
		self::$str_where = "where";
		if(!$result)
		{
			return self::return_data(1,'sql error:'.$sql,null);
		}
		if(!$result_count || !isset($result_count[0]['count']))
		{
			return self::return_data(1,'sql count error:'.$sql_count,null);
		}
		self::$page_info['total_count'] = isset($result_count[0]['count']) ? intval($result_count[0]['count']) : 0;
		return self::return_data(0,'ok',$result,self::$page_info);
	}
	
	/**
	 * 查询所有的cp id
	 * @param object $dc 数据库操作对象
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function get_all_cp_id($dc,$org_id)
	{
		if(empty($org_id))
		{
			return self::return_data(2,'params error empty org id');
		}
		$sql="select nns_cp_id from " . self::$base_table . " where nns_org_id='{$org_id}' group by nns_cp_id ";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'sql error:'.$sql,null);
		}
		return self::return_data(0,'ok',$result);
	}
	
	/**
	 * 判断主媒资是否经过审核(用注入id查询)
	 * @param object $dc 数据库操作对象
	 * @param string $import_id 主媒资注入id
	 * @param string $org_id 运营商id
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function check_video_is_continue_by_import_id($dc,$import_id,$org_id)
	{
		if(empty($import_id))
		{
			return self::return_data(2,'params error empty import id');
		}
		if(empty($org_id))
		{
			return self::return_data(2,'params error empty org id');
		}
		$sql="select * from " . self::$base_table . " where nns_import_id='{$import_id}' and nns_org_id='{$org_id}'";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'sql query error:'.$sql,null);
		}
		return self::return_data(0,'ok',$result);
	}
	
	/**
	 * 判断主媒资是否经过审核(用主媒资id查询)
	 * @param object $dc 数据库操作对象
	 * @param string $vod_id 主媒资id
	 * @param string $org_id 运营商id
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function check_video_is_continue_by_id($dc,$vod_id,$org_id)
	{
		if(empty($vod_id))
		{
			return self::return_data(2,'params error empty vod id');
		}
		if(empty($org_id))
		{
			return self::return_data(2,'params error empty org id');
		}
		$sql="select audit.* from " . self::$vod_table . " as vod left join " . self::$base_table . " as audit on ".
			" vod.nns_asset_import_id=audit.nns_import_id where vod.nns_id='{$vod_id}' and audit.nns_org_id='{$org_id}' ";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'sql query error:'.$sql,null);
		}
		return self::return_data(0,'ok',$result);
	}
	
	
	/**
	 * 判断分集是否经过审核(用分集注入id、分集id查询)
	 * @param object $dc 数据库操作对象
	 * @param string $org_id 运营商id
	 * @param string $index_id 分集id
	 * @param string $index_import_id 分集注入id
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function check_index_is_continue_by_id($dc,$org_id,$index_id=null,$index_import_id=null)
	{
		if(empty($org_id))
		{
			return self::return_data(2,'params error empty org id');
		}
		if(empty($index_id) && empty($index_import_id))
		{
			return self::return_data(2,'params error empty index id or import id,index_id:'.var_export($index_id,true).";import_id:".var_export($index_import_id,true));
		}
		$sql="select audit.* from " . self::$index_table . " as inde left join " . self::$vod_table . " as vod on inde.nns_vod_id=vod.nns_id " . 
			"left join " . self::$base_table . " as audit on vod.nns_asset_import_id=audit.nns_import_id where audit.nns_org_id='{$org_id}' ";
		if(!empty($index_id))
		{
			$sql.=" and inde.nns_id='{$index_id}' ";
		}
		if(!empty($index_import_id))
		{
			$sql.=" and inde.nns_import_id='{$index_import_id}' ";
		}
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'sql query error:'.$sql,null);
		}
		return self::return_data(0,'ok',$result);
	}
	
	/**
	 * 判断片源是否经过审核(用片源注入id、片源id查询)
	 * @param object $dc 数据库操作对象
	 * @param string $org_id 运营商id
	 * @param string $media_id 片源id
	 * @param string $media_import_id 片源注入id
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function check_media_is_continue_by_id($dc,$org_id,$media_id=null,$media_import_id=null)
	{
		if(empty($org_id))
		{
			return self::return_data(2,'params error empty org id');
		}
		if(empty($media_id) && empty($media_import_id))
		{
			return self::return_data(2,'params error empty index id or import id,media_id:'.var_export($media_id,true).";import_id:".var_export($media_import_id,true));
		}
		$sql="select audit.* from " . self::$media_table . " as media left join " . self::$vod_table . " as vod on media.nns_vod_id=vod.nns_id " .
				" left join " . self::$base_table . " as audit on vod.nns_asset_import_id=audit.nns_import_id where audit.nns_org_id='{$org_id}' ";
		if(!empty($media_id))
		{
			$sql.=" and media.nns_id='{$media_id}' ";
		}
		if(!empty($media_import_id))
		{
			$sql.=" and media.nns_import_id='{$media_import_id}' ";
		}
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'sql query error:'.$sql,null);
		}
		return self::return_data(0,'ok',$result);
	}
	
	
}
