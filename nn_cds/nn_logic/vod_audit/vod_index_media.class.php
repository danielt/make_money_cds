<?php
/**
 * 资源库数据
 */
include_once dirname(dirname(__FILE__))."/public.class.php";
class nl_vod_index_media extends nl_public
{
	//主媒资表
	public static $base_table='nns_vod';
	public static $base_table_ex='nns_vod_ex';
	//分集表
	public static $index_table='nns_vod_index';
	public static $index_table_ex='nns_vod_index_ex';
	
	//片源表
	public static $media_table='nns_vod_media';
	public static $media_table_ex='nns_vod_media_ex';
	public static $asset_type = array('video','index','media');
	/**
	 * 以注入媒资查询该主媒资的信息
	 * @param object $dc 数据库操作对象
	 * @param string $import_id 运营媒资注入id
	 * @param string $import_source 注入源
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function query_vod_info_by_import_id($dc,$import_id,$import_source=null)
	{
		if(empty($import_id))
		{
			return self::return_data(2,'params error empty import id');
		}
		$sql = "select * from " . self::$base_table . " where nns_asset_import_id='{$import_id}' ";
		if(isset($import_source) && strlen($import_source) > 0)
		{
			$sql.=" and nns_import_source='{$import_source}' ";
		}
		$sql.=" limit 1 ";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'sql query error:'.$sql);
		}
		return self::return_data(0,'ok',$result);
	}
	
	
	/**
	 * 以媒资id查询主媒资注入id信息
	 * @param object $dc 数据库操作对象
	 * @param string $vod_id 主媒资id
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function query_video_import_id($dc,$vod_id)
	{
		if(empty($vod_id))
		{
			return self::return_data(2,'params error empty vod id');
		}
		$sql = "select nns_asset_import_id,nns_cp_id from " . self::$base_table . " where nns_id='{$vod_id}' limit 1 ";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result || !is_array($result) || !isset($result[0]['nns_asset_import_id']))
		{
			return self::return_data(1,'sql query error:'.$sql);
		}
		return self::return_data(0,'ok',$result[0]);
	}
	
	/**
	 * 以分集id查询分集注入id信息
	 * @param object $dc 数据库操作对象
	 * @param string $index_id 分集id
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function query_index_import_id($dc,$index_id)
	{
		if(empty($index_id))
		{
			return self::return_data(2,'params error empty index id');
		}
		$sql = "select nns_import_id as nns_asset_import_id,nns_cp_id,nns_vod_id from " . self::$index_table . " where nns_id='{$index_id}' limit 1 ";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result || !is_array($result) || !isset($result[0]['nns_asset_import_id']))
		{
			return self::return_data(1,'sql query error:'.$sql);
		}
		return self::return_data(0,'ok',$result[0]);
	}
	
	/**
	 * 以片源id查询片源注入id信息
	 * @param object $dc 数据库操作对象
	 * @param string $media_id 分集id
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function query_media_import_id($dc,$media_id)
	{
		if(empty($media_id))
		{
			return self::return_data(2,'params error empty index id');
		}
		$sql = "select nns_import_id as nns_asset_import_id,nns_cp_id,nns_vod_index_id from " . self::$media_table . " where nns_id='{$media_id}' limit 1 ";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result || !is_array($result) || !isset($result[0]['nns_asset_import_id']))
		{
			return self::return_data(1,'sql query error:'.$sql);
		}
		return self::return_data(0,'ok',$result[0]);
	}
	
	/**
	 * 查询分集信息
	 * @param object $dc 数据库操作对象
	 * @param string $vod_id 主媒资id
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function query_vod_index_info($dc,$vod_id)
	{
		if(empty($vod_id))
		{
			return self::return_data(2,'params error empty vod_id');
		}
		$sql="select nns_vod_id,nns_vod_index from " . self::$index_table . " where nns_vod_id ='{$vod_id}' order by nns_index asc";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'db query error:'.$sql);
		}
		return self::return_data(0,'ok',$result);
	}

	/**
	 * 查询片源信息
	 * @param object $dc 数据库操作对象
	 * @param string $vod_id 主媒资id
	 * @param string $index_id 分集id
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function query_vod_media_info($dc,$vod_id,$index_id)
	{
		if(empty($vod_id))
		{
			return self::return_data(2,'params error empty vod_id');
		}
		if(empty($index_id))
		{
			return self::return_data(2,'params error empty index_id');
		}
		$sql="select nns_vod_id,nns_vod_index_id from " . self::$media_table . " where nns_vod_id ='{$vod_id}' and nns_vod_index_id = '{$index_id}' ";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'db query error:'.$sql);
		}
		return self::return_data(0,'ok',$result);
	}
	
	/**
	 * 查询主媒资信息
	 * @param object $dc 数据库操作对象
	 * @param string $nns_id 主媒资id | 分集id | 片源id
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function query_video_index_media_info($dc,$nns_id,$video_type)
	{
		if(empty($nns_id))
		{
			return self::return_data(2,'params error empty vod_id');
		}
		if(empty($video_type) || !in_array($video_type, self::$asset_type))
		{
			return self::return_data(2,'params error empty video_type or video_type is illegitmacy');
		}
		if($video_type == 'video' )
		{
			$sql="select *,nns_asset_import_id as nns_import_id  from " . self::$base_table . " where nns_id ='{$nns_id}'";
		}
		else if($video_type == 'index')
		{
			$sql="select * from " . self::$index_table . " where nns_id ='{$nns_id}'";
		}
		else
		{
			$sql="select * from " . self::$media_table . " where nns_id ='{$nns_id}'";
		}
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'db query error:'.$sql);
		}
		return self::return_data(0,'ok',$result);
	}
	
	/**
	 * 查询主媒资信息
	 * @param object $dc 数据库操作对象
	 * @param string $nns_id 主媒资id | 分集id | 片源id
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function query_video_index_media_info_by_import_id($dc,$import_id,$video_type,$cp_id)
	{
		if(empty($import_id))
		{
			return self::return_data(2,'注入id为空');
		}
		if(empty($video_type) || !in_array($video_type, self::$asset_type))
		{
			return self::return_data(2,'没有此媒资类型');
		}
		if(strlen($cp_id) < 1)
		{
			return self::return_data(2,'cp_id为空');
		}
		if($video_type == 'video' )
		{
			$sql="select *,nns_asset_import_id as nns_import_id  from " . self::$base_table . " where nns_asset_import_id ='{$import_id}' and nns_cp_id='{$cp_id}'";
		}
		else if($video_type == 'index')
		{
			$sql="select * from " . self::$index_table . " where nns_import_id ='{$import_id}' and nns_cp_id='{$cp_id}' ";
		}
		else
		{
			$sql="select * from " . self::$media_table . " where nns_import_id ='{$import_id}' and nns_cp_id='{$cp_id}' ";
		}
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'db query error:'.$sql);
		}
		return self::return_data(0,'ok',$result);
	}
	
	
	/**
	 * 查询主媒资信息
	 * @param object $dc 数据库操作对象
	 * @param string $nns_id 主媒资id | 分集id | 片源id
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function query_index_media_info_by_id($dc,$nns_id,$video_type)
	{
		if(empty($nns_id))
		{
			return self::return_data(2,'guid为空');
		}
		if(empty($video_type) || !in_array($video_type, self::$asset_type))
		{
			return self::return_data(2,'没有此媒资类型');
		}
		if($video_type == 'index')
		{
			$sql="select vod.nns_asset_import_id as nns_import_id,vod.nns_cp_id as nns_cp_id from " . self::$index_table . " as inde left join " . self::$base_table .
			 " as vod on vod.nns_id=inde.nns_vod_id where inde.nns_id ='{$nns_id}' ";
		}
		else if($video_type == 'media')
		{
			$sql="select inde.nns_import_id as nns_import_id,inde.nns_cp_id as nns_cp_id from " . self::$media_table . " as media left join " . self::$index_table . 
			" as inde on inde.nns_id=media.nns_vod_index_id where media.nns_id ='{$nns_id}' ";
		}
		else
		{
			return self::return_data(9,'no such type');
		}
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'db query error:'.$sql);
		}
		return self::return_data(0,'ok',$result);
	}
	
	
	
	/**
	 * 查询主媒资信息
	 * @param object $dc 数据库操作对象
	 * @param string $nns_id 主媒资id | 分集id | 片源id
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function query_video_index_media_ex_info($dc,$nns_id,$video_type)
	{
		if(empty($nns_id))
		{
			return self::return_data(2,'params error empty vod_id');
		}
		if(empty($video_type) || !in_array($video_type, self::$asset_type))
		{
			return self::return_data(2,'params error empty video_type or video_type is illegitmacy');
		}
		if($video_type == 'video' )
		{
			$sql="select * from " . self::$base_table_ex . " where nns_id ='{$nns_id}'";
		}
		else if($video_type == 'index')
		{
			$sql="select * from " . self::$index_table . " where nns_id ='{$nns_id}'";
		}
		else
		{
			$sql="select * from " . self::$media_table . " where nns_id ='{$nns_id}'";
		}
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'db query error:'.$sql);
		}
		return self::return_data(0,'ok',$result);
	}
	
	
	/**
	 * 查询主媒资下的分集片源的 最大分集  最小分集 和片源信息
	 * @param object $dc 数据库操作对象
	 * @param string $vod_id 主媒资id
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function query_vod_index_media_info($dc,$vod_id)
	{
		if(empty($vod_id))
		{
			return self::return_data(2,'params error empty vod_id');
		}
		$sql = "select inde.nns_id as nns_index_id,inde.nns_index,media.nns_id as nns_media_id from " . self::$index_table . 
				" as inde left join " . self::$media_table . " as media on  media.nns_vod_index_id=inde.nns_id " . 
				"and media.nns_vod_id=inde.nns_vod_id where inde.nns_vod_id='{$vod_id}' order by inde.nns_index asc";
		$sql_other = "SELECT MAX(nns_index) AS max , MIN(nns_index) AS min, count(1) as count,nns_index ".
				" FROM " . self::$index_table . " WHERE nns_vod_id='{$vod_id}' group by nns_vod_id ";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'sql error:'.$sql);
		}
		$result_other = nl_query_by_db($sql_other, $dc->db());
		if(!$result_other)
		{
			return self::return_data(1,'sql error:'.$sql_other);
		}
		return self::return_data(0, 'ok',$result,null,$result_other);
	}
	
	/**
	 * 查询分集的总数
	 * @param object $dc 数据库操作对象
	 * @param string $vod_id 主媒资id
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function query_count_index($dc,$vod_id)
	{
		if(empty($vod_id))
		{
			return self::return_data(2,'params error empty vod_id');
		}
		$index_sql = "SELECT MAX(`nns_index`) AS max , MIN(`nns_index`) AS min, count(1) as count,nns_index ".
				"FROM " . self::$index_table . " WHERE nns_vod_id='$vod_id' "; //查询最大分集和最小分集
		$index_info = nl_query_by_db($index_sql, $db);
		if($index_info)
		{
			return self::return_data(1,'db query error:'.$index_sql);
		}
		return self::return_data(0,'ok',$index_info);
	}
	
	
}
