<?php
/**
 * 转码负载均衡管理
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_transcode_load_balance extends nl_public
{
	static $base_table='nns_transcode_load_balance';
	static $arr_filed = array(
			'nns_id',
        	'nns_type',
        	'nns_name',
        	'nns_api_url',
	        'nns_manufacturer_id',
        	'nns_sp_id',
        	'nns_state',
        	'nns_desc',
        	'nns_max_num',
        	'nns_create_time',
	        'nns_modify_time',
	);
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_by_id($dc,$nns_id)
	{
		$sql = "select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result[0]) && !empty($result[0]) && is_array($result[0])) ? $result[0] : null;
		return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_by_ids($dc,$nns_id)
	{
	    $nns_id = is_array($nns_id) ? $nns_id : array($nns_id);
	    $sql = "select * from " . self::$base_table . " where nns_id in('".implode("','", $nns_id)."')";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result) && !empty($result) && is_array($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
	
	
	/**
	 * 添加转码负载均衡日志
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function add($dc,$params)
	{
	    $date = date("Y-m-d H:i:s");
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		if(!isset($params['nns_create_time']) || strlen($params['nns_create_time']) <1)
		{
		    $params['nns_create_time'] = $date;
		}
		if(!isset($params['nns_modify_time']) || strlen($params['nns_modify_time']) <1)
		{
		    $params['nns_modify_time'] = $date;
		}
		$sql = self::make_insert_sql(self::$base_table, $params);
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 修改转码负载均衡日志
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function edit($dc,$params,$nns_id)
	{
		if(strlen($nns_id)<0)
		{
			return self::return_data(1,'转码负载均衡日志_id为空');
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$params['nns_modify_time'] = date("Y-m-d H:i:s");
		$sql = self::make_update_sql(self::$base_table, $params,array('nns_id'=>$nns_id));
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}

	/**
	 * 修改转码负载均衡日志
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function edit_v2($dc,$params,$where_condition)
	{
	    if(!is_array($where_condition))
	    {
	        return self::return_data(1,'条件查询必须为数组');
	    }
	    $where_condition = self::except_useless_params(self::$arr_filed, $where_condition);
	    if(empty($where_condition) || !is_array($where_condition))
	    {
	        return self::return_data(1,'转码负载均衡日志_id为空');
	    }
	    $params = self::except_useless_params(self::$arr_filed, $params);
	    if(empty($params))
	    {
	        return self::return_data(1,'参数为空');
	    }
	    $params['nns_modify_time'] = date("Y-m-d H:i:s");
	    $sql = self::make_update_sql(self::$base_table, $params,$where_condition);
	    $result = nl_execute_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库执行失败'.$sql);
	    }
	    return self::return_data(0,'ok');
	}
	
	
	
	
	/**
	 * 查询转码负载均衡日志列表
	 * @param object $dc 数据库对象
	 * @param array $params 查询参数
	 * @param array $page_info 分页信息
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query($dc,$params,$page_info=null)
	{
		if(isset($params['where']) && !empty($params['where']))
		{
			foreach ($params['where'] as $where_key=>$where_val)
			{
		        if($where_key == 'nns_create_time_begin')
		        {
		            self::$str_where.=" nns_create_time>='{$where_val}' and ";
		            continue;
		        }
		        else if($where_key == 'nns_create_time_end')
		        {
		            self::$str_where.=" nns_create_time<='{$where_val}' and ";
		            continue;
		        }
			    if(is_array($where_val))
			    {
				    self::$str_where.=" {$where_key} in ('".implode("','", $where_val)."') and ";
			    }
			    else
			    {
			        self::$str_where.=" {$where_key} = '{$where_val}' and ";
			    }
			}	
		}
		if(isset($params['like']) && !empty($params['like']))
		{
			foreach ($params['like'] as $like_key=>$like_val)
			{
				self::$str_where.=" {$like_key} like '%{$like_val}%' and ";
			}
		}
		self::$str_where = (strlen(self::$str_where) > 5) ? trim(self::$str_where,'and ') : '';
		$str_limit = self::make_page_limit($page_info);
		$sql="select * from " . self::$base_table . " " . self::$str_where . " order by nns_create_time desc {$str_limit} ";
		$sql_count = "select count(*) as count from " . self::$base_table . " " . self::$str_where;
		$result_count = nl_query_by_db($sql_count,  $dc->db());
		$result = nl_query_by_db($sql, $dc->db());
		self::init_where();
		if(!$result || !$result_count)
		{
			return self::return_data(1,"查询数据失败".$sql.$sql_count);
		}
		return self::return_data(0,"查询数据成功".$sql_count.$sql,$result,$result_count);
	}
	
	/**
	 * 查询是否存在
	 * @param unknown $dc
	 * @param string $str_param
	 */
	static public function query_unique($dc,$str_param=null)
	{
	    $sql="select count(*) as count from " . self::$base_table;
	    if(isset($str_param) && strlen($str_param))
	    {
	        $str_param = trim($str_param);
	    }
	    $is_set_where = substr($str_param, 0,5);
	    if(strtolower($is_set_where) != 'where')
	    {
	        $str_param = " where ".$str_param;
	    }
	    $sql = $sql.$str_param;
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,"查询数据失败".$sql);
	    }
	    return self::return_data(0,"查询数据成功".$sql,$result);
	}
	
	
	
	/**
	 * 依据条件查询数据  条件必须要存在
	 * @param unknown $dc
	 * @param unknown $arr_params
	 */
	static public function query_data($dc,$arr_params)
	{
	    $arr_params = self::except_useless_params(self::$arr_filed, $arr_params);
	    if(empty($arr_params) || !is_array($arr_params))
	    {
	        return self::return_data(0,'OK');
	    }
	    $arr_where = array();
	    foreach ($arr_params as $key=>$val)
	    {
	        if(is_array($val))
	        {
	            $arr_where[] = " {$key} in ('".implode("','", $val)."') ";
	        }
	        else
	        {
	            $arr_where[] = " {$key}='{$val}' ";
	        }
	    }
	    $str_where = empty($arr_where) ? '' : " where ".implode(" and ", $arr_where);
	    $sql="select * from " . self::$base_table . " {$str_where} ";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    return self::return_data(0,'OK',$result);
	}
	
	
	/**
	 * 根据GUID获取多个键值
	 * @param object $dc 数据库对象
	 * @param $arr_key_ids array 键值主键
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_by_condition($dc,$arr_key_ids=array(),$other_filter=array())
	{
		$sql = "select * from " . self::$base_table;
		$ids_str = '';
		if(!empty($arr_key_ids) && is_array($arr_key_ids))
		{
		   	$str_id = implode("','", $arr_key_ids);
			$ids_str = " nns_id in('{$str_id}') AND ";
		}
		$other_str = '';
		if(!empty($other_filter) && is_array($other_filter))
		{
			foreach ($other_filter as $oter=>$filter)
			{
				if(!empty($filter))
				{
					$other_str .= " {$oter} like '%{$filter}%' AND ";
				}
			}
		}
		$str_sql = $ids_str . $other_str;
		$str_sql = rtrim($str_sql," AND ");
		if(!empty($str_sql))
		{
			$sql .= " where {$str_sql}";
		}
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result) && !empty($result)) ? $result : null;
		return self::return_data(0,'OK',$result);
		
	}
	
	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function delete($dc,$nns_id)
	{
		if(strlen($nns_id) <1)
		{
			return self::return_data(1,'转码负载均衡日志_id为空');
		}
		$sql = "delete from " . self::$base_table . " where nns_id='{$nns_id}' ";
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		return self::return_data(0,'OK');
	}
	
	/**
	 * 查询sp下的厂商列表
	 * @param unknown $dc
	 * @param unknown $sp_id
	 * @param unknown $manufacturer_id
	 */
	static public function query_sp_info($dc,$sp_id)
	{
	    if(strlen($sp_id) <1)
	    {
	        return self::return_data(1,'查询转码负载均衡sp_id为空');
	    }
	    $sql="select * from " . self::$base_table . " where nns_sp_id='{$sp_id}' and nns_state='0'";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 查询sp下的厂商列表
	 * @param unknown $dc
	 * @param unknown $sp_id
	 * @param unknown $manufacturer_id
	 */
	static public function query_sp_info_v2($dc,$sp_id)
	{
	    if(strlen($sp_id) <1)
	    {
	        return self::return_data(1,'查询转码负载均衡sp_id为空');
	    }
	    $sql="select * from " . self::$base_table . " where nns_sp_id='{$sp_id}'";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
}