<?php
include_once dirname(dirname(dirname(__FILE__))) . '/nn_cms_config/nn_cms_global.php';

class nl_image
{

    static public function get_base_img_url($img_url)
    {
        if (empty($img_url)) 
        {
            return '';
        }
        global $g_bk_version_number;
        $bk_version_number = $g_bk_version_number;
        unset($g_bk_version_number);
        if ($bk_version_number == '1') 
        {
            global $g_bk_web_url;
            $bk_web_url = $g_bk_web_url;
            unset($g_bk_web_url);
            $bk_web_url = trim(trim(trim($bk_web_url, '/'), '\\')) . '/data/downimg/';
            $img_url = trim(trim(trim($img_url, '/'), '\\'));
            return $bk_web_url . $img_url;
        }
        global $g_image_host;
        $img_url = str_replace('JPG', 'jpg', $img_url);
        $img_url = ltrim($img_url, '/');
        if (empty($g_image_host)) 
        {
            return '../../../' . $img_url;
        }
        $image_hosts = explode(';', $g_image_host);
        $host_num = rand(0, count($image_hosts) - 1);
        $img_host = $image_hosts[$host_num];
        return $img_host . $img_url;
    }

    static public function get_manager_image_url($img_url)
    {
        return self::get_base_img_url($img_url);
    }

    static public function epg_packet_image_url($img_url)
    {
        return self::get_base_img_url($img_url);
    }

    static public function epg_category_image_url($img_url)
    {
        return self::get_base_img_url($img_url);
    }

    static public function epg_video_image_url($img_url)
    {
        return self::get_base_img_url($img_url);
    }

    static public function epg_ad_image_url($img_url)
    {
        return self::epg_video_image_url($img_url);
    }

    static function topic_image_url($img_url)
    {
        return self::get_base_img_url($img_url);
    }

    static public function weather_image_url($img_url)
    {
        return self::get_base_img_url($img_url);
    }

    protected static function _rand_hosts($hosts)
    {
        $host_num = rand(0, count($hosts) - 1);
        return $hosts[$host_num];
    }

    static public function _ftp_copy_images($web_url, $result_path)
    {
        include_once NPDIR . 'np_ftp.php';
        $g_image_ftp_addr = g_cms_config::get_g_config_value('g_image_ftp_addr');
        $ftp_conf = g_cms_config::get_g_config_value('g_ftp_conf');
        
        if (is_array($ftp_conf)) 
        {
            
            for ($i = 1; $i <= 32; $i ++) 
            {
                $ftp_result = self::_ftp_put_image($ftp_conf, $web_url, $result_path, $i);
                if ($ftp_result === NULL) 
                {
                    
                }
                if ($ftp_result === FALSE) 
                {
                    return FALSE;
                }
            }
            
            if (! empty($g_image_ftp_addr)) 
            {
                $result_path = str_replace("data/upimg", $g_image_ftp_addr, $result_path);
            }
        }
        return $result_path;
    }

    static private function _ftp_put_image($ftp_conf, $web_url, $result_path, $num)
    {
        $web_url = rtrim($web_url, '/');
        $web_url .= '/';
        $ftp_key = 'ftp_to' . $num;
        if (! $ftp_conf[$ftp_key])
        {
            return NULL;
        }
        $passivetype = isset($ftp_conf[$ftp_key]['passive']) && $ftp_conf[$ftp_key]['passive'] === false ? false : true;
        $ftp1 = new nns_ftp($ftp_conf[$ftp_key]["address"], $ftp_conf[$ftp_key]["user"], $ftp_conf[$ftp_key]["password"], $ftp_conf[$ftp_key]["port"]);
        $ftpcon1 = $ftp1->connect(30, $passivetype);
        if (empty($ftpcon1)) 
        {
            // np_debug::set_global_error('', implode($ftp1->error(), ','));
            return FALSE;
        }
        // 上传
        $path = str_replace('data/upimg/', '', $web_url);
        $ftp1_path = $ftp_conf["$ftp_key"]["img_cms"] . '/' . $path;
        $img_name = str_replace($web_url, '', $result_path); // 图片名称，不包括路径
        $bool1 = $ftp1->up($ftp1_path, $img_name, dirname(dirname(dirname(__FILE__))) . '/' . $result_path);
        if (! $bool1) 
        {
            // np_debug::set_global_error('', implode($ftp1->error(), ','));
            $ftp1 = null;
            return FALSE;
        }
        $ftp1 = null;
        return TRUE;
    }
}