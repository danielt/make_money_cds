<?php

class nl_npss {

    static public function get_active_npss_address(){
//	global $g_core_npss_url;
	$core_npss_url=g_cms_config::get_g_config_value('g_core_npss_url');
//	获取原始地址
	$npss_url_arr = explode(";", $core_npss_url);
	$npss_ini=dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.'nn_cms_config'.DIRECTORY_SEPARATOR.'global_npss_ini';
	if (file_exists($npss_ini)){
		$npss_result=parse_ini_file($npss_ini);
		$active_npss_str=$npss_result['servers'];
		$active_npss_url_arr = explode(";", $active_npss_str);
		foreach ($active_npss_url_arr as $active_npss_url){
			if (in_array($active_npss_url,$npss_url_arr)){
				return $active_npss_url;
			}
		}
	}
	return $npss_url_arr[0];
}
}
?>