<?php
/*
 * Created on 2012-10-10
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'nn_cms_config'.DIRECTORY_SEPARATOR.'nn_cms_global.php';
  
  include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'nn_cms_config.php';
  if (file_exists(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'nn_cms_sub_platform_config.php')){
  	include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'nn_cms_sub_platform_config.php';
  }
  
 /**
  * 基础类文件夹地址
  */
 define('NPDIR',dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.'np'.DIRECTORY_SEPARATOR);
 


define('NL_DB_WRITE',1);
define('NL_DB_READ',0);

define('NL_REDIS_WRITE', 1);
define('NL_REDIS_READ', 0);


define('NL_DC_DB',1);
define('NL_DC_CACHE',2);
define('NL_DC_AUTO',0);

define('NL_ORDER_NUM_DESC','0');
define('NL_ORDER_NUM_ASC','1');
define('NL_ORDER_TIME_DESC','2');
define('NL_ORDER_TIME_ASC','3');
define('NL_ORDER_CLICK_DESC','4');
define('NL_ORDER_CLICK_ASC','5');
define('NL_ORDER_TIME_AND_NUM_DESC', '6');


/**
 * @auth yxt
 * 用于读取老的配置
 * 主要提供给v2调用
 * @param string $str_config_key 配置参数名
 * @return mixed 返回对于配置项的值,如果变量不存在，返回false
 */
function get_config_v1($str_config_key){
    global $$str_config_key;
    if(isset($$str_config_key)){
        return $$str_config_key;
    }else{
        return null;
    }
}

/**
 * @auth yxt
 * 新的读取配置文件的函数
 * 调用顺序为：先读根目录下的nn_cms_config_v2->_get_config_v1->nn_meta_config_v2
 * @param string $str_config_key 配置参数名
 * @return mixed 返回对于配置项的值
 */
function get_config_v2($str_config_key){
    include dirname(dirname(__FILE__)).'/nn_cms_config/nn_cms_config_v2.php';
    if(isset($$str_config_key)){//如果存在直接返回配置值
        return $$str_config_key;
    }else{//如果改配置不存在
        //去老的配置里面获取
        $config_val=get_config_v1($str_config_key);
        if($config_val===null){//如果老的配置里面没有，则去meta_config_v2里面读
            include_once dirname(dirname(__FILE__)).'/nn_cms_config/nn_cms_meta_config_v2.php';
            if(isset($$str_config_key)){//如果存在直接返回
                return $$str_config_key;
            }else{//如果所有的配置都找不到
                nl_log_v2_error("get_config_v2","get config:{$str_config_key} error because the config not exists");
                return null;
            }
        }else{//如果存在直接返回
            return $config_val;
        }
    }
}
?>
