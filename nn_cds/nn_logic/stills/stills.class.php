<?php

/*
 * Created on 2012-12-18
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';

class nl_stills {

//排序规则
        static public $order = array(
            NL_ORDER_NUM_ASC => " order by  nns_order asc ",
            NL_ORDER_NUM_DESC => " order by  nns_order desc ",
            NL_ORDER_TIME_ASC => " order by  nns_create_time asc ",
            NL_ORDER_TIME_DESC => " order by  nns_create_time desc ",
        );
        //需要获取的字段
        static private $field = "`nns_id`,`nns_image`,`nns_video_id`,`nns_video_type`,`nns_create_time`,`nns_summary`,`nns_width`,`nns_height`,`nns_scale`";

        /**
         * 查询剧照 
         * 从DC模块获取剧照列表
         * @param dc DC模块
         * @param string 视频ID
         * @param int 视频类型
         * @param int 起始查询记录位置
         * @param int 查询记录条数
         * @param string 排序规则，默认为	NL_ORDER_NUM_DESC 
          NL_ORDER_NUM_ASC | NL_ORDER_NUM_DESC | NL_ORDER_TIME_ASC | NL_ORDER_TIME_DESC
         * @return *
         * 			Array 剧照列表结果
         * 			FALSE 查询失败
         * 			TRUE  查询成功，但无数据
         */
        static function get_stills_list($dc, $video_id, $video_type, $since, $num, $order = NL_ORDER_NUM_DESC) {
                $sql = "select * from nns_stills where nns_video_id= '$video_id' and nns_video_type='$video_type' " . self::$order[$order] . " limit $since,$num ";
                return nl_query_by_db($sql, $dc->db());
        }

        /**
         * 根据 video_id 和 video_type 统计剧照总数
         */
        static function count_stills_num($dc, $video_id, $video_type) {
                $sql = "select count(*) as num from nns_stills where nns_video_id='$video_id' and nns_video_type='$video_type' limit 1";

                $result = nl_query_by_db($sql, $dc->db());
                return $result['0']['num'];
        }

        /**
         * 添加剧照
         * 从DC模块获取剧照列表
         * @param dc DC模块
         * @param string 视频ID
         * @param int 视频类型
         * @param array(
          “image”=>剧照地址，
          “summary”=>剧照说明
          　　				)
         * @return *
         * 			FALSE失败
         * 			TRUE  成功
         */
        static public function add_stills($dc, $video_id, $video_type, $params) {
                if (!isset($params['nns_id'])) {
                        $params['nns_id'] = np_guid_rand();
                }$sql = "select max(nns_order) as c from nns_stills where nns_video_id='{$video_id}' and nns_video_type='{$video_type}'";
                $r = nl_query_by_db($sql, $dc->db());
                $order_num = (empty($r[0]['c']) && $r[0]['c'] !== '0') ? 0 : $r[0]['c'] + 1;
                $params['nns_video_id'] = $video_id;
                $params['nns_video_type'] = $video_type;
                $params['nns_order'] = $order_num;
                $sql = self::_set_insert_sql($params, 'nns_stills');

                return nl_execute_by_db($sql, $dc->db());
        }

        /**
         * 从DC模块获取剧照列表
         * @param dc DC模块
         * @param string  剧照ID
         * @param array(
          “image”=>剧照地址，
          “summary”=>剧照说明
          　　				)
         * @return *
         * 			FALSE失败
         * 			TRUE  成功
         */
        static public function modify_stills($dc, $id, $params) {
                $sql = self::_set_update_sql($params, 'nns_stills');
                $sql .= " where nns_id = '$id'";
                return nl_execute_by_db($sql, $dc->db());
        }

        /**
         * 从DC模块获取剧照列表
         * @param dc DC模块
         * @param string  剧照ID
         * @return *
         * 			FALSE失败
         * 			TRUE  成功
         */
        static public function delete_stills($dc, $id) {
                $sql = "delete from nns_stills where nns_id ='$id'";

                return nl_execute_by_db($sql, $dc->db());
        }

        /**
         * 从DC模块获取剧照列表
         * @param dc DC模块
         * @param string  剧照ID
         * @return *
         * 			FALSE失败
         * 			TRUE  成功
         */
        static public function update_stills_to_order_first($dc, $id) {
//获取剧照的id和order
                $sql = "select nns_video_id,nns_order from nns_stills where nns_id = '{$id}'";
                $result = nl_query_by_db($sql, $dc->db());
                $video_id = $result[0]['nns_video_id'];
                $order = $result[0]['nns_order'];
//判断图片是否已经处于最顶级，如果是则返回false
                $sql = "select * from nns_stills where nns_order>{$order} and nns_video_id ='{$video_id}'";
//		echo $sql;exit;
                $result = nl_query_by_db($sql, $dc->db());
                if ($result === false || $result === true) {
                        return false;
                }

//将所有比当前图片order大的order-1
                $sql = "update nns_stills set nns_order=nns_order-1 where nns_order>{$order} and nns_video_id = '{$video_id}'";

                $result = nl_execute_by_db($sql, $dc->db());
//将当前的order=count(1)
                $sql = "select max(nns_order) as order from nns_stills";
                $result = nl_query_by_db($sql, $dc->db());

                $sql = "update nns_stills set nns_order=" . ($result[0]['order'] + 1) . " where nns_id='{$id}' and nns_video_id='{$video_id}'";

                $result = nl_execute_by_db($sql, $dc->db());
                echo $result;
        }

        /**
         * 从DC模块获取剧照列表
         * @param dc DC模块
         * @param string  剧照ID
         * @return *
         * 			FALSE失败
         * 			TRUE  成功
         */
        static public function update_stills_to_order_last($dc, $id) {
//获取剧照的id和order
                $sql = "select nns_video_id,nns_order from nns_stills where nns_id = '{$id}'";
                $result = nl_query_by_db($sql, $dc->db());
                $video_id = $result[0]['nns_video_id'];
                $order = $result[0]['nns_order'];
//判断图片是否已经处于最顶级，如果是则返回false
                $sql = "select * from nns_stills where nns_order<{$order} and nns_video_id ='{$video_id}'";
                $result = nl_query_by_db($sql, $dc->db());
                if ($result === false || $result === true) {
                        return false;
                }

//将所有比当前图片order大的order+1
                $sql = "update nns_stills set nns_order=nns_order+1 where nns_order<{$order} and nns_video_id = '{$video_id}'";
                $result = nl_execute_by_db($sql, $dc->db());
                $sql = "update nns_stills set nns_order=0 where nns_id='{$id}' and nns_video_id='{$video_id}'";
                $result = nl_execute_by_db($sql, $dc->db());
                echo $result;
        }

        /**
         * 从DC模块获取剧照列表
         * @param dc DC模块
         * @param string  当前剧照ID
         * @param string  需要对调剧照ID
         * @return *
         * 			FALSE失败
         * 			TRUE  成功
         */
        static public function update_stills_to_order_switch($dc, $id, $other_id) {
//获取源图片的order
                $sql = "select nns_order from nns_stills where nns_id='{$id}'";
                $r1 = nl_query_by_db($sql, $dc->db());
//获取目标图片所属信息id和order
                $sql = "select nns_order from nns_stills where nns_id='{$other_id}'";
                $r2 = nl_query_by_db($sql, $dc->db());
//互换order
                $sql = "update nns_stills set nns_order={$r2[0]['nns_order']} where nns_id='{$id}'";
                $r3 = nl_execute_by_db($sql, $dc->db());
                $sql = "update nns_stills set nns_order={$r1[0]['nns_order']} where nns_id='{$other_id}'";
                $r4 = nl_execute_by_db($sql, $dc->db());
                return $r1 && $r2 && $r3 && $r4;
        }

        /**
         * 从DC模块获取剧照列表
         * @param dc DC模块
         * @param string  当前剧照ID
         * @return *
         * 			FALSE失败
         * 			TRUE  成功
         */
        static public function update_stills_to_order_up($dc, $id) {
//获取图片所属信息id和order
                $sql = "select nns_video_id,nns_order from nns_stills where nns_id='{$id}'";
                $result = nl_query_by_db($sql, $dc->db());
                $video_id = $result[0]['nns_video_id'];
                $order = $result[0]['nns_order'];
//判断是否有比当前order大的
                $sql = "select nns_id from nns_stills where nns_order={$order}+1 and nns_video_id='{$video_id}'";
                $r = nl_query_by_db($sql, $dc->db());
                if ($r === true) {
                        return -1;
                } elseif ($r === false) {
                        return 0;
                }
                $r = self::update_stills_to_order_switch($dc, $id, $r[0]['nns_id']);
                if ($r === false) {
                        return false;
                }
                return true;
        }

        /**
         * 从DC模块获取剧照列表
         * @param dc DC模块
         * @param string  当前剧照ID
         * @return *
         * 			FALSE失败
         * 			TRUE  成功
         */
        static public function update_stills_to_order_down($dc, $id) {
//获取图片所属信息id和order
                $sql = "select nns_video_id,nns_order from nns_stills where nns_id='{$id}'";
                $result = nl_query_by_db($sql, $dc->db());
                $video_id = $result[0]['nns_video_id'];
                $order = $result[0]['nns_order'];
//判断是否有比当前order小的
                $sql = "select nns_id from nns_stills where nns_order={$order}-1 and nns_video_id='{$video_id}'";
                $r = nl_query_by_db($sql, $dc->db());
                if ($r === true) {
                        return -1;
                } elseif ($r === false) {
                        return 0;
                }
                $r = self::update_stills_to_order_switch($dc, $id, $r[0]['nns_id']);
                if ($r === false) {
                        return false;
                }
                return true;
        }

        /**
         * 构造 INSERT sql语句
         * @param array $params  插入数据格式：字段名=>字段值
         * @param string $table_name 表名字
         */
        static private function _set_insert_sql($params, $table_name) {
                $ins_sql = 'INSERT INTO `' . $table_name . '` (';
                foreach ($params as $key => $val) {
//yxt一下一行是对所有的html实体标签和单双引号进行转义
                        $val = htmlentities($val, ENT_QUOTES, "UTF-8");
                        $ins_key[] = $key;
                        $ins_val[] = '\'' . $val . '\'';
                }
                $ins_sql .= implode(',', $ins_key) . ') VALUES(' . implode(',', $ins_val) . ')';
                return $ins_sql;
        }

        /**
         * 构造 UPDATE sql语句
         * @param array $params
         * @param string $table_name
         */
        static private function _set_update_sql($params, $table_name) {
                $upd_sql = 'UPDATE `' . $table_name . '` SET ';
                foreach ($params as $key => $val) {
                        $val = htmlentities($val, ENT_QUOTES, "UTF-8");
                        $sql_arr[] = $key . '=\'' . $val . '\'';
                }
                $upd_sql .= implode(',', $sql_arr);
                return $upd_sql;
        }

        /**
         * 2013 5.14 LMS
         * 查询剧照 
         * 根据图片高宽查询剧照
         * @param dc DC模块
         * @param string 视频ID
         * @param int 图片高度
         * @param int 图片宽度
         * @param int 视频类型 直播/点播
         * @param int 起始查询记录位置
         * @param int 查询记录条数
         * @param int 匹配模式 (0模糊/1高宽)匹配模式
         * @param 返回列表默认为最接近高宽的图片
         * @return *
         * 			Array 剧照列表结果
         * 			FALSE 查询失败
         * 			TRUE  查询成功，但无数据
         */
        Static public function get_stills_list_by_wh($dc, $video_id, $wid, $hei, $video_type = 1, $since = 0, $num = 12, $blur = 1) {
                if (self::emptys($video_id, $wid, $hei)) {
                        return false;
                }
                $since = $num * ($since - 1);
                $field = self::$field;
                if (!$blur) {
                        $field = "," . $field;
                        $scale = floor(($hei / $wid) * 100); //计算图片长宽比
                        $sql = "SELECT width+(scale*nns_width) / 100 as list$field FROM (SELECT ABS($scale - `nns_scale`) as scale,`width`$field FROM (SELECT  ABS($wid - `nns_width`) as width";
                        $sql .= "$field FROM nns_stills WHERE `nns_video_id`='$video_id') as re) as rw WHERE scale >=0 ORDER BY list asc ";
                } else {
                        $sql = "SELECT $field from nns_stills WHERE nns_video_id='$video_id' AND ";
                        $sql.="nns_width='$wid' AND nns_height='$hei'";
                }
                $sql .=" LIMIT $since,$num";

                $list = nl_query_by_db($sql, $dc->db());
                return $list;
        }

        /**
         * 2013 5.14 LMS
         * 查询剧照 
         * 根据图片高宽比查询剧照
         * @param dc DC模块
         * @param string 视频ID
         * @param int 图片高宽比
         * @param int 视频类型 直播/点播
         * @param int 起始查询记录位置
         * @param int 查询记录条数
         * @param int 匹配模式 (0模糊/1高宽)匹配模式
         * @param 返回列表默认为最接近高宽的图片
         * @return *
         * 			Array 剧照列表结果
         * 			FALSE 查询失败
         * 			TRUE  查询成功，但无数据
         */
        Static public function get_stills_list_by_scale($dc, $video_id, $wh, $video_type = 1, $since = 0, $num = 12, $blur = 1) {
                if (self::emptys($video_id, $wh)) {
                        return false;
                }
                $since = $num * ($since - 1);
                $field = self::$field;
                if (!$blur) {
                        $sql = "SELECT ABS($wh-`nns_scale`) as c ,$field,`nns_scale` from nns_stills WHERE nns_video_id='$video_id' AND nns_scale >=0 ORDER BY c asc";
                } else {
                        $sql = "SELECT $field from nns_stills WHERE nns_video_id='$video_id' AND ";
                        $sql.="nns_scale='$wh'";
                }
                $sql .=" LIMIT $since,$num";
                $list = nl_query_by_db($sql, $dc->db());
                return $list;
        }

        static private function emptys() {
                $empty = func_get_args();
                foreach ($empty as $v) {
                        if (empty($v))
                                return true;
                }
                return false;
        }

        static private function orderBy($order) {

                switch ($order) {
                        case 'NL_ORDER_NUM_ASC':
                                $sql = "nns_order ASC";
                                break;
                        case 'NL_ORDER_TIME_ASC':
                                $sql = "nns_create_time ASC";
                                break;
                        case 'NL_ORDER_TIME_DESC':
                                $sql = "nns_create_time DESC";
                                break;
                        default :
                                $sql = "nns_order DESC";
                }
                return $sql;
        }

}

?>