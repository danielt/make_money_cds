<?php
/**
 * 队列池管理
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_vod_index_seekpoint extends nl_public
{
	static $base_table='nns_vod_index_seekpoint';
	static $arr_filed = array(
			'nns_id',
			'nns_video_index',
			'nns_video_id',
	        'nns_type',
			'nns_image',
			'nns_begin',
	        'nns_end',
			'nns_name',
	);
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_by_id($dc,$nns_id)
	{
		$sql = "select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
		return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 查询打点信息
	 * @param unknown $dc
	 * @param unknown $str_vod_id
	 * @param unknown $str_vod_index
	 */
	static public function query_by_vod_index($dc,$str_vod_id,$str_vod_index)
	{
	    $sql="select * from " . self::$base_table . " where nns_video_id='{$str_vod_id}' and nns_video_index='{$str_vod_index}'";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (is_array($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK:'.$sql,$result);
	    	
	}
	
	/**
	 * 查询打点信息
	 * @param unknown $dc
	 * @param unknown $str_vod_id
	 * @param unknown $str_vod_index
	 */
	static public function delete_by_vod_index($dc,$str_vod_id,$str_vod_index)
	{
	    $sql="delete from " . self::$base_table . " where nns_video_id='{$str_vod_id}' and nns_video_index='{$str_vod_index}'";
	    $result = nl_execute_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库执行失败,sql:'.$sql);
	    }
	    return self::return_data(0,'OK:'.$sql,$result);
	
	}
	
	/**
	 * 添加全局错误日志
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function add($dc,$params)
	{
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$sql = self::make_insert_sql(self::$base_table, $params);
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 修改全局错误日志
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function edit($dc,$params,$nns_id)
	{
	    $nns_id = is_array($nns_id) ? $nns_id : array($nns_id);
		if(empty($nns_id))
		{
			return self::return_data(1,'全局错误日志_id为空');
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$sql = self::make_update_sql(self::$base_table, $params,array('nns_id'=>$nns_id));
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	
	/**
	 * 修改全局错误日志
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function edit_sql($dc,$sql)
	{
	    $result = nl_execute_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库执行失败'.$sql);
	    }
	    return self::return_data(0,'ok'.$sql);
	}

	/**
	 * 查询全局错误日志列表
	 * @param object $dc 数据库对象
	 * @param array $params 查询参数
	 * @param array $page_info 分页信息
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query($dc,$params,$page_info=null)
	{
		if(isset($params['where']) && !empty($params['where']))
		{
			foreach ($params['where'] as $where_key=>$where_val)
			{
			    if(is_array($where_val))
			    {
				    self::$str_where.=" {$where_key} in ('".implode("','", $where_val)."') and ";
			    }
			    else
			    {
			        self::$str_where.=" {$where_key} = '{$where_val}' and ";
			    }
			}	
		}
		if(isset($params['like']) && !empty($params['like']))
		{
			foreach ($params['like'] as $like_key=>$like_val)
			{
				self::$str_where.=" {$like_key} like '%{$like_val}%' and ";
			}
		}
		self::$str_where = (strlen(self::$str_where) > 5) ? trim(self::$str_where,'and ') : '';
		$str_limit = self::make_page_limit($page_info);
		$sql="select * from " . self::$base_table . " " . self::$str_where . " order by nns_state asc,nns_create_time desc {$str_limit} ";
		$sql_count = "select count(*) as count from " . self::$base_table . " " . self::$str_where;
		$result_count = nl_query_by_db($sql_count,  $dc->db());
		$result = nl_query_by_db($sql, $dc->db());
		self::init_where();
		if(!$result || !$result_count)
		{
			return self::return_data(1,"查询数据失败".$sql.$sql_count);
		}
		return self::return_data(0,"查询数据成功".$sql_count.$sql,$result,$result_count);
	}
	
	/**
	 * 查询是否存在
	 * @param unknown $dc
	 * @param string $str_param
	 */
	static public function query_unique($dc,$str_param=null)
	{
	    $sql="select * from " . self::$base_table;
	    if(isset($str_param) && strlen($str_param))
	    {
	        $str_param = trim($str_param);
	    }
	    $is_set_where = substr($str_param, 0,5);
	    if(strtolower($is_set_where) != 'where')
	    {
	        $str_param = " where ".$str_param;
	    }
	    $str_param = (strtolower($is_set_where) != 'where') ? " where ".$str_param : " ".$str_param;
	    $sql = $sql.$str_param;
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,"查询数据失败".$sql);
	    }
	    return self::return_data(0,"查询数据成功".$sql,$result);
	}
	
	
	/**
	 * 依据条件查询数据  条件必须要存在
	 * @param unknown $dc
	 * @param unknown $arr_params
	 */
	static public function query_data($dc,$arr_params)
	{
	    $arr_params = self::except_useless_params(self::$arr_filed, $arr_params);
	    if(empty($arr_params) || !is_array($arr_params))
	    {
	        return self::return_data(0,'OK');
	    }
	    $arr_where = array();
	    foreach ($arr_params as $key=>$val)
	    {
	        if(is_array($val))
	        {
	            $arr_where[] = " {$key} in ('".implode("','", $val)."') ";
	        }
	        else
	        {
	            $arr_where[] = " {$key}='{$val}' ";
	        }
	    }
	    $str_where = empty($arr_where) ? '' : " where ".implode(" and ", $arr_where);
	    $sql="select * from " . self::$base_table . " {$str_where} ";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function delete($dc,$nns_id)
	{
	    $nns_id = is_array($nns_id) ? $nns_id : array($nns_id);
	    
		if(empty($nns_id))
		{
			return self::return_data(1,'全局错误日志_id为空');
		}
		$sql = "delete from " . self::$base_table . " where nns_id in('".implode("','", $nns_id)."')";
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		return self::return_data(0,'OK');
	}
	
}
