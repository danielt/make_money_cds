<?php
/*
 * Created on 2012-10-11
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
class nl_live_index{
	const CACHE_TIME_OUT=300;
	/**
	 * 从DB模块获取当前视频信息
	 * @param db DB模块
	 * @param String 用户ID
	 * @return *
	 * 			Array 视频信息结果
	 * 			FALSE 查询失败
	 * 			TRUE  查询成功，但无数据
	 */
	static private function _get_live_index_by_db($db,$video_id) {
		$sql = 'select * ' .
//				'nns_id,' .
//				'nns_name,' .
//				'nns_live_id,' .
//				'nns_index,' .
//				'nns_image,' .
//				'nns_summary ' .
				'from nns_live_index ' .
				'where nns_live_id=\''.$video_id.'\' ' .
				'order by nns_index';
				
		return nl_query_by_db($sql, $db);
	}
	
	/**
	 * 从DC模块获取当前视频信息
	 * @param dc DC模块
	 * @param String 用户ID
	 * @return *
	 * 			Array 视频信息结果
	 * 			FALSE 查询失败
	 * 			TRUE  查询成功，但无数据
	 */
	static public function get_live_index($dc,$video_id,$policy=NL_DC_AUTO) {
		$result = null;
		if ($policy == NL_DC_AUTO){
			if ($dc->is_cache_enabled()) {
				$result = self::_get_live_index_by_cache($dc->cache(),$video_id);
				if ($result === FALSE ) {
					$result = self::_get_live_index_by_db($dc->db(),$video_id);
					self::_set_live_index_by_cache($dc->cache(),$video_id,$result);
				}
			}
			else {
				$result = self::_get_live_index_by_db($dc->db(),$video_id);
			}
		}elseif($policy == NL_DC_DB){
			$result = self::_get_live_index_by_db($dc->db(),$video_id);
		}elseif($policy == NL_DC_CACHE){
			$result = self::_get_live_index_by_cache($dc->cache(),$video_id);
		}
		
		return $result;
	}

	/**
	 * 从CACHE模块获取当前视频信息
	 * @param cache CACHE模块
	 * @param String 用户ID
	 * @return * 
	 * 			Array 信息查询结果
	 * 			FALSE 为未缓存
	 */
	static private function _get_live_index_by_cache($cache,$video_id) {
		if ($cache===NULL) return FALSE;
		return unserialize($cache->get('live_index|#'.$video_id));
	}
	
	/**
	 * 为当前用户获取的信息数据设置缓存
	 * @param cache CACHE模块
	 * @param Array 信息数据
	 * @param String 用户ID
	 */
	static private function _set_live_index_by_cache($cache,$video_id,$data) {
		if ($cache===NULL) return FALSE;
		return $cache->set('live_index|#'.$video_id,serialize($data),self::CACHE_TIME_OUT);
	}
	
	/**
	 * 为当前用户获取的信息数据重置缓存
	 * @param cache CACHE模块
	 * @param String 用户ID
	 */
	static public function delete_live_index_by_cache($dc,$video_id) {
		if ($dc->cache()===NULL) return FALSE;
		return $dc->cache()->delete('live_index|#'.$video_id);
	}
}
?>