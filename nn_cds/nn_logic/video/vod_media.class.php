<?php

/*
 * Created on 2012-10-11
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'public.class.php';

class nl_vod_media extends nl_public
{
    static $base_table = 'nns_vod_media';
	static $base_table_ex='nns_vod_media_ex';
	static $base_table_vod='nns_vod';
	static $base_table_vod_ex='nns_vod_ex';
	static $base_table_vod_index='nns_vod_index';
	static $base_table_vod_index_ex='nns_vod_index_ex';

    const CACHE_TIME_OUT = 300;

    const DIMENSIONS_2D_DEFAULT = 0;
    // 3D
    const DIMENSIONS_3D_DEFAULT = 100000;
    // 3D
    const DIMENSIONS_3D_LEFT_RIGHT = 100001;
    // 左右3D
    const DIMENSIONS_3D_UP_DOWN = 100002;
    // 上下3D
    static $arr_filed = array(
        'nns_id',
        'nns_name',
        'nns_vod_id',
        'nns_vod_index_id',
        'nns_type',
        'nns_state',
        'nns_url',
        'nns_tag',
        'nns_mode',
        'nns_kbps',
        'nns_content_id',
        'nns_content_state',
        'nns_create_time',
        'nns_modify_time',
        'nns_deleted',
        'nns_check',
        'nns_vod_index',
        'nns_filetype',
        'nns_play_count',
        'nns_score_total',
        'nns_score_count',
        'nns_media_caps',
        'nns_import_id',
        'nns_import_source',
        'nns_dimensions',
        'nns_status',
        'nns_action',
        'nns_ext_url',
        'nns_file_size',
        'nns_file_time_len',
        'nns_file_frame_rate',
        'nns_file_resolution',
        'nns_integer_id',
        'nns_cp_id',
        'nns_media_name',
        'nns_clip_state',
        'nns_encode_flag',
        'nns_ext_info',
        'nns_media_type',
        'nns_original_live_id',
        'nns_start_time',
        'nns_media_service',
        'nns_conf_info',
        'nns_encode_flag',
        'nns_domain',
        'nns_drm_enabled',
        'nns_drm_encrypt_solution',
        'nns_drm_ext_info',
        'nns_live_to_media'
    );

    /**
     * 查询片源的清晰度
     * 
     * @param unknown $dc            
     */
    static public function query_group_mode($dc)
    {
        $sql = "select nns_mode from " . self::$base_table . " group by nns_mode";
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        $result = (is_array($result) && ! empty($result)) ? $result : null;
        return self::return_data(0, 'ok', $result);
    }

    /**
     * 查询片源的文件类型
     * 
     * @param unknown $dc            
     */
    static public function query_group_filetype($dc)
    {
        $sql = "select nns_filetype from " . self::$base_table . " group by nns_filetype";
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        $result = (is_array($result) && ! empty($result)) ? $result : null;
        return self::return_data(0, 'ok', $result);
    }

    /**
     * 查询列表
     * 
     * @param object $dc
     *            数据库对象
     * @param array $params
     *            查询参数
     * @param array $page_info
     *            分页信息
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     *         @date 2015-11-26
     */
    static public function query($dc, $params = null, $page_info = null)
    {
        if (isset($params['where']) && ! empty($params['where'])) 
        {
            foreach ($params['where'] as $where_key => $where_val) 
            {
                if ($where_key == 'create_begin_time') 
                {
                    self::$str_where .= " media.nns_create_time >='{$where_val}' and ";
                } 
                else 
                {
                    if ($where_key == 'create_end_time') 
                    {
                        self::$str_where .= " media.nns_create_time <='{$where_val}' and ";
                    } 
                    else 
                    {
                        self::$str_where .= " media.{$where_key} ='{$where_val}' and ";
                    }
                }
            }
        }
        if (isset($params['like']) && ! empty($params['like'])) 
        {
            foreach ($params['like'] as $like_key => $like_val) 
            {
                self::$str_where .= " media.{$like_key} like '%{$like_val}%' and ";
            }
        }
        self::$str_where = (strlen(self::$str_where) > 5) ? trim(self::$str_where, 'and ') : '';
        $str_limit = self::make_page_limit($page_info);
        $sql = "select media.*,inde.nns_index as index_index from " . self::$base_table . " as media left join " . self::$base_table_vod_index . " as inde on inde.nns_id=media.nns_vod_index_id " . self::$str_where . " order by nns_create_time desc {$str_limit} ";
        $sql_count = "select count(*) as count from " . self::$base_table . " " . self::$str_where;
        // echo $sql;
        $result_count = nl_query_by_db($sql_count, $dc->db());
        $result = nl_query_by_db($sql, $dc->db());
        self::init_where();
        if (! $result || ! $result_count) 
        {
            return self::return_data(1, "查询数据失败");
        }
        return self::return_data(0, "查询数据成功", $result, $result_count);
    }

    /**
     * 查询等待切片的片源数据
     * 
     * @param object $dc            
     * @param string $nns_cp_id
     *            cp_id
     * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据'),
     * @author liangpan
     *         @date 2016-09-02
     */
    static public function query_wait_clip($dc, $nns_cp_id = '')
    {
        $sql = "select media.*,vod.nns_name as vod_name,vod.nns_pinyin as vod_pinyin,vod.nns_asset_import_id as vod_import_id,inde.nns_index as index_index," . "inde.nns_name as index_name,inde.nns_import_id as index_import_id from " . self::$base_table . " as media left join " . self::$base_table_vod . " as vod on vod.nns_id=media.nns_vod_id " . " left join " . self::$base_table_vod_index . " as inde on inde.nns_id=media.nns_vod_index_id " . " where media.nns_cp_id='{$nns_cp_id}' and media.nns_clip_state='1' and media.nns_deleted='0' order by media.nns_modify_time asc limit 1";
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        $result = (is_array($result) && ! empty($result) && isset($result[0])) ? $result[0] : null;
        return self::return_data(0, 'ok', $result);
    }

    /**
     * 以片源id查询片源信息
     * 
     * @param object $dc            
     * @param string $nns_cp_id
     *            cp_id
     * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据'),
     * @author liangpan
     *         @date 2016-09-02
     */
    static public function query_media_info_by_id($db, $media_id)
    {
        $sql = "select media.*,vod.nns_name as vod_name,inde.nns_index as index_index from " . self::$base_table . " as media left join " . self::$base_table_vod . " as vod on vod.nns_id=media.nns_vod_id " . " left join " . self::$base_table_vod_index . " as inde on inde.nns_id=media.nns_vod_index_id " . " where media.nns_id='{$media_id}' limit 1";
        $result = nl_query_by_db($sql, $db);
        if (! $result) 
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        $result = (is_array($result) && ! empty($result) && isset($result[0])) ? $result[0] : null;
        return self::return_data(0, 'ok', $result);
    }

    /**
     * 依据自增ID查询
     * 
     * @param unknown $dc            
     * @param unknown $integer_id            
     */
    static public function query_by_integer_id($dc, $integer_id)
    {
        $sql = "select * from " . self::$base_table . " where nns_integer_id='{$integer_id}' limit 1";
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (isset($result[0]) && ! empty($result[0])) ? $result[0] : null;
        return self::return_data(0, 'OK', $result);
    }

    /**
     * 修改片源信息
     * 
     * @param object $dc
     *            数据库对象
     * @param array $params
     *            数据数组
     * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     *         @date 2016-03-06
     */
    static public function edit($dc = null, $params, $nns_id, $db = null)
    {
        if (strlen($nns_id) < 0) 
        {
            return self::return_data(1, '片源GUID为空');
        }
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (empty($params)) 
        {
            return self::return_data(1, '参数为空');
        }
        $params['nns_modify_time'] = date("Y-m-d H:i:s");
        $sql = self::make_update_sql(self::$base_table, $params, array('nns_id' => $nns_id));
        if ($dc !== null) 
        {
            $result = nl_execute_by_db($sql, $dc->db());
        } 
        else 
        {
            $result = nl_execute_by_db($sql, $db);
        }
        if (! $result) 
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        return self::return_data(0, 'ok' . $sql);
    }

    /**
     * 修改片源信息
     * 
     * @param object $dc
     *            数据库对象
     * @param array $params
     *            数据数组
     * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     *         @date 2016-03-06
     */
    static public function edit_arr_id($dc, $arr_id, $clip_state = 4)
    {
        if (empty($arr_id) || ! is_array($arr_id)) 
        {
            return self::return_data(1, '片源GUID为空');
        }
        $date_time = date("Y-m-d H:i:s");
        $str_arr_id = implode("','", $arr_id);
        $sql = "update " . self::$base_table . " set nns_clip_state=1,nns_modify_time='{$date_time}' where nns_clip_state={$clip_state} and nns_deleted=0 and nns_id in('{$str_arr_id}')";
        $result = nl_execute_by_db($sql, $dc->db());
        if (! $result)
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        return self::return_data(0, 'ok');
    }

    /**
     * 查询扩展信息
     * 
     * @param unknown $dc            
     * @param unknown $nns_import_id            
     * @param unknown $nns_cp_id            
     */
    static public function query_ex_by_id($dc, $nns_import_id, $nns_cp_id)
    {
        if (strlen($nns_import_id) < 1) 
        {
            return self::return_data(1, '$nns_import_id为空');
        }
        if (strlen($nns_cp_id) < 1) 
        {
            return self::return_data(1, '$nns_cp_id为空');
        }
        $sql = "select * from " . self::$base_table_ex . " where nns_vod_media_id='{$nns_import_id}' and nns_cp_id='{$nns_cp_id}'";
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (! empty($result) && is_array($result)) ? $result : null;
        return self::return_data(0, 'OK' . $sql, $result);
    }

    /**
     * 查询扩展信息
     * 
     * @param unknown $dc            
     * @param unknown $nns_import_id            
     * @param unknown $nns_cp_id            
     */
    static public function query_ex_by_id_and_key($dc, $nns_import_id, $nns_cp_id, $nns_key)
    {
        if (strlen($nns_import_id) < 1) 
        {
            return self::return_data(1, '$nns_import_id为空');
        }
        if (strlen($nns_cp_id) < 1) 
        {
            return self::return_data(1, '$nns_cp_id为空');
        }
        $sql = "select * from " . self::$base_table_ex . " where nns_vod_media_id='{$nns_import_id}' and nns_key='{$nns_key}' and nns_cp_id='{$nns_cp_id}'";
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (! empty($result) && is_array($result)) ? $result : null;
        return self::return_data(0, 'OK' . $sql, $result);
    }

    /**
     * 插入数据
     * 
     * @param unknown $dc            
     * @param unknown $nns_import_id            
     * @param unknown $nns_cp_id            
     * @param unknown $nns_key            
     * @param unknown $nns_value            
     */
    static public function add_ex($dc, $nns_import_id, $nns_cp_id, $nns_key, $nns_value)
    {
        $sql = "insert into " . self::$base_table_ex . " (nns_vod_media_id,nns_key,nns_value,nns_cp_id) VALUES ('{$nns_import_id}', '{$nns_key}', '{$nns_value}', '{$nns_cp_id}')";
        $result = nl_execute_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        return self::return_data(0, 'ok' . $sql);
    }

    /**
     * 插入数据
     * 
     * @param unknown $dc            
     * @param unknown $nns_import_id            
     * @param unknown $nns_cp_id            
     * @param unknown $nns_key            
     * @param unknown $nns_value            
     */
    static public function edit_ex($dc, $nns_import_id, $nns_cp_id, $nns_key, $nns_value)
    {
        $sql = "update " . self::$base_table_ex . " set nns_value='{$nns_value}' where nns_vod_media_id='{$nns_import_id}' and nns_key='{$nns_key}' and nns_cp_id='{$nns_cp_id}'";
        $result = nl_execute_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        return self::return_data(0, 'ok' . $sql);
    }

    /**
     * 查询单个列表
     * 
     * @param object $dc
     *            数据库对象
     * @param string $nns_id
     *            guid
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     *         @date 2016-09-24
     */
    static public function query_by_id($dc, $nns_id)
    {
        $sql = "select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (isset($result[0]) && ! empty($result[0])) ? $result[0] : null;
        return self::return_data(0, 'OK' . $sql, $result);
    }

    static public function query_import_all_info($dc, $arr_import_id, $cp_id, $import_source)
    {
        if (empty($arr_import_id) || ! is_array($arr_import_id)) 
        {
            return self::return_data(0, 'OK', null);
        }
        $sql = "select nns_deleted,nns_import_id from " . self::$base_table . " where nns_import_id in(" . implode("','", $arr_import_id) . ") and nns_cp_id='{$cp_id}' and nns_import_source='{$import_source}'";
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (is_array($result) && ! empty($result)) ? $result : null;
        $last_result = null;
        if (is_array($result))
        {
            foreach ($result as $val) 
            {
                $val['nns_deleted'] = ($val['nns_deleted'] != 0) ? 'deleted' : 'exsist';
                $last_result[$val['nns_deleted']][] = $val['nns_import_id'];
            }
        }
        return self::return_data(0, 'OK', $result);
    }

    static public function add($dc, $params)
    {
        $params['nns_id'] = (isset($params['nns_id']) && ! empty($params['nns_id'])) ? $params['nns_id'] : np_guid_rand();
        $params['nns_create_time'] = $params['nns_modify_time'] = date('Y-m-d H:i:s');
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (empty($params)) 
        {
            return self::return_data(1, '参数为空');
        }
        $sql = self::make_insert_sql(self::$base_table, $params);
        $result = nl_execute_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        return self::return_data(0, 'ok');
    }

    /**
     * 查询片源是否存在
     * 
     * @param unknown $dc            
     * @param unknown $str_import_id            
     * @param unknown $cp_id            
     * @param unknown $import_source            
     */
    static public function query_media_exsist($dc, $str_import_id, $cp_id, $import_source, $media_service)
    {
        if (strlen($str_import_id) < 1) 
        {
            return self::return_data(0, 'OK', null);
        }
        $sql = "select * from " . self::$base_table . " where nns_import_id='{$str_import_id}' and nns_cp_id='{$cp_id}' and nns_import_source='{$import_source}' and nns_media_service='{$media_service}' and nns_deleted !=1";
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (isset($result[0]) && is_array($result[0]) && ! empty($result[0])) ? $result[0] : null;
        return self::return_data(0, 'OK', $result);
    }

    /**
     * 查询片源是否存在
     * 
     * @param unknown $dc            
     * @param unknown $str_import_id            
     * @param unknown $cp_id            
     * @param unknown $import_source            
     */
    static public function query_media_org_exsist($dc, $str_import_id, $cp_id, $import_source)
    {
        if (strlen($str_import_id) < 1) 
        {
            return self::return_data(0, 'OK', null);
        }
        $sql = "select * from " . self::$base_table . " where nns_import_id='{$str_import_id}' and nns_cp_id='{$cp_id}' and nns_import_source='{$import_source}'";
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (isset($result[0]) && is_array($result[0]) && ! empty($result[0])) ? $result[0] : null;
        return self::return_data(0, 'OK' . $sql, $result);
    }

    /**
     * 根据来源ID与注入ID查询数据是否存在，查询VOD_MEDIA数据
     * 
     * @param object $dc
     *            数据库操作对象
     * @param string $import_id
     *            注入ID
     * @param string $source_id
     *            上游来源ID
     * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据'),
     * @author zhiyong.luo
     *         @date 2017-08-16
     */
    static public function query_vod_media_info_by_import_id($dc, $import_id, $source_id = '')
    {
        if (empty($import_id)) 
        {
            return self::return_data(1, '注入ID为空');
        }
        $sql = "select * from " . self::$base_table . " where nns_import_id='{$import_id}'";
        if (! empty($source_id)) 
        {
            $sql .= " and nns_cp_id='{$source_id}'";
        }
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        return self::return_data(0, 'ok', $result);
    }

    /**
     * 从DC模块获取当前视频分集片源信息组
     * 
     * @param
     *            dc DC模块
     * @param
     *            String 视频ID
     * @param
     *            int 视频分集 默认为0
     * @param
     *            String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
     * @return * Array 视频信息结果
     *         FALSE 查询失败
     *         TRUE 查询成功，但无数据
     */
    static public function get_vod_media($dc, $video_id, $index = 0, $policy = NL_DC_AUTO)
    {
        $result = null;
        if (empty($index))
        {
            $index = 0;
        }
        if ($policy == NL_DC_AUTO) 
        {
            if ($dc->is_cache_enabled()) 
            {
                $result = self::_get_vod_media_by_cache($dc->cache(), $video_id, $index);
                if ($result === FALSE) 
                {
                    $result = self::_get_vod_media_by_db($dc->db(), $video_id, $index);
                    self::_set_vod_media_by_cache($dc->cache(), $video_id, $index, $result);
                }
            } 
            else 
            {
                $result = self::_get_vod_media_by_db($dc->db(), $video_id, $index);
            }
        } 
        elseif ($policy == NL_DC_DB) 
        {
            $result = self::_get_vod_media_by_db($dc->db(), $video_id, $index);
        } 
        elseif ($policy == NL_DC_CACHE) 
        {
            $result = self::_get_vod_media_by_cache($dc->cache(), $video_id, $index);
        }
        return $result;
    }

    static private function _get_vod_media_by_db($db, $video_id, $index)
    {
        $sql = "select * from nns_vod_media where nns_vod_id='{$video_id}' and nns_vod_index='{$index}'";
        return nl_query_by_db($sql, $db);
    }

    static private function _get_vod_media_by_cache($cache, $video_id, $index)
    {
        if ($cache === NULL)
        {
            return FALSE;
        }
        $index = empty($index) ? 0 : $index;
        return unserialize($cache->get('vod_media|#' . $video_id . '|#' . $index));
    }

    static private function _set_vod_media_by_cache($cache, $video_id, $index, $data)
    {
        if ($cache === NULL)
        {
            return FALSE;
        }
        $index = empty($index) ? 0 : $index;
        return $cache->set('vod_media|#' . $video_id . '|#' . $index, serialize($data), self::CACHE_TIME_OUT);
    }

    static public function delete_vod_media_by_cache($dc, $video_id, $index)
    {
        if ($dc->cache() === NULL)
        {
            return FALSE;
        }
        $index = empty($index) ? 0 : $index;
        return $dc->cache()->delete('vod_media|#' . $video_id . '|#' . $index);
    }

    /**
     * 查询片源数据信息(guid查询)
     * 
     * @param object $dc
     *            数据库对象
     * @param string $video_meida_id
     *            分集guid
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     *         @date 2015-12-29
     */
    static public function get_video_index_info_by_id($dc, $video_meida_id)
    {
        if (empty($video_meida_id)) 
        {
            return self::return_data(1, '片源guid为空');
        }
        $sql = "select * from " . self::$base_table . " where nns_id='{$video_meida_id}' limit 1";
        // echo $sql;die;
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, "查询片源数据信息失败" . $sql);
        }
        return self::return_data(0, "查询片源数据信息成功", $result);
    }

    /**
     * 查询片源数据信息(guid查询)
     * 
     * @param object $dc
     *            数据库对象
     * @param string $video_index_id
     *            分集guid
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     *         @date 2015-12-29
     */
    static public function get_video_index_info_by_import_id($dc, $video_media_import_id, $cp_id = 0)
    {
        if (empty($video_media_import_id)) 
        {
            return self::return_data(1, '片源注入id为空');
        }
        $sql = "select * from " . self::$base_table . " where nns_import_id='{$video_media_import_id}' and nns_cp_id='{$cp_id}' order by nns_modify_time desc limit 1";
        // echo $sql;die;
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, "查询片源数据信息失败" . $sql);
        }
        return self::return_data(0, "查询片源数据信息成功", $result);
    }

    /**
     * 根据分集注入ID查询片源信息
     * 
     * @param $index_import_id 分集注入ID            
     */
    static public function get_video_media_by_index_import_id($dc, $index_import_id, $cp_id = null)
    {
        if (empty($index_import_id)) 
        {
            return self::return_data(1, '分集注入id为空');
        }
        if (is_array($cp_id)) 
        {
            $cp_ids = implode("','", $cp_id);
            $str_cp = " and i.nns_cp_id in ('{$cp_ids}')";
        } 
        else 
        {
            $str_cp = $cp_id === null ? "" : " and i.nns_cp_id='{$cp_id}'";
        }
        $sql = "select m.* from " . self::$base_table . " as m,nns_vod_index as i where i.nns_import_id='{$index_import_id}' and m.nns_deleted!=1 and m.nns_vod_index_id=i.nns_id {$str_cp}";
        // echo $sql;die;
        $result = nl_query_by_db($sql, $dc->db());
        if (! is_array($result)) 
        {
            return self::return_data(1, "查询分集数据信息失败" . $sql);
        }
        return self::return_data(0, "查询分集数据信息成功", $result);
    }
}