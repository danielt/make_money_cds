<?php
/*
 * Created on 2012-10-11
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_live extends nl_public
{
	static public $base_table = 'nns_live';
	const CACHE_TIME_OUT=300;
	
	static private function _epg_get_live_list_by_db($db){
		$sql = 'select ' .
				'nns_id,' .
				'nns_tag,' .
				'nns_name,' .
				'nns_image0,' .
				'nns_image1,' .
				'nns_image2,' .
				'nns_summary,' .
				'nns_point ' .
				'from nns_live ' ;
				
		$live_list=nl_query_by_db($sql, $db);
		
		$live_list=np_array_rekey($live_list,'nns_id');
		
		return $live_list;
	}
	
	static public function epg_get_live_list($dc,$policy=NL_DC_AUTO) {
		$result = null;
		if ($policy == NL_DC_AUTO){
			if ($dc->is_cache_enabled()) {
				$result = self::_epg_get_live_list_by_cache($dc->cache());
				if ($result === FALSE ) {
					$result = self::_epg_get_live_list_by_db($dc->db());
					self::_epg_set_live_list_by_cache($dc->cache(),$result);
				}
			}
			else {
				$result = self::_epg_get_live_list_by_db($dc->db());
			}
		}elseif($policy == NL_DC_DB){
			$result = self::_epg_get_live_list_by_db($dc->db());
		}elseif($policy == NL_DC_CACHE){
			$result = self::_epg_get_live_list_by_cache($dc->cache());
		}
		
		return $result;
	}
	
	static private function _epg_get_live_list_by_cache($cache) {
		if ($cache===NULL) return FALSE;
		return unserialize($cache->get('live_list'));
	}
	

	static private function _epg_set_live_list_by_cache($cache,$data) {
		if ($cache===NULL) return FALSE;
		return $cache->set('live_list',serialize($data),self::CACHE_TIME_OUT);
	}
	

	static public function delete_live_list_by_cache($dc) {
		if ($dc->cache()===NULL) return FALSE;
		return $dc->cache()->delete('live_list');
	}
	
	
	/**
	 * 从DB模块获取当前视频信息
	 * @param db DB模块
	 * @param String 用户ID
	 * @return *
	 * 			Array 视频信息结果
	 * 			FALSE 查询失败
	 * 			TRUE  查询成功，但无数据
	 */
	static private function _epg_get_live_info_by_db($db,$video_id) {
		$sql = 'select ' .
				'nns_category_id,'.
				'nns_id,' .
				'nns_tag,' .
				'nns_name,' .
				'nns_image0,' .
				'nns_image1,' .
				'nns_image2,' .
				'nns_play_count,' .
				'nns_show_time,' .
				'nns_view_len,' .
				'nns_area,' .
				'nns_summary,' .
				'nns_point, ' .
				'(select nns_value from nns_live_ex where nns_live_id=\''.$video_id.'\' and nns_key=\'dvb\') as nns_dvb,' .
				'(select nns_value from nns_live_ex where nns_live_id=\''.$video_id.'\' and nns_key=\'live_type\') as nns_live_type,' .
				'(select nns_value from nns_live_ex where nns_live_id=\''.$video_id.'\' and nns_key=\'pid\') as nns_pid ' .
				'from nns_live ' .
				'where nns_id=\''.$video_id.'\'';
				
		$live_info=nl_query_by_db($sql, $db);
//		if ($live_info===FALSE || $live_info===TRUE) return $live_info;
//		
//		$ex_sql= 'select ' .
//				'nns_live_id,' .
//				'nns_key,' .
//				'nns_value ' .
//				'from nns_live_ex ' .
//				'where nns_live_id=\''.$video_id.'\'';
//				
//		$live_ex=nl_query_by_db($sql, $db);
//		
//			
//		foreach ($live_ex as $live_ex_item){
//			$live_info[0][$live_ex_item['nns_key']]=$live_ex_item['nns_value'];
//		}
		
		return $live_info;
	}
	
	/**
	 * 从DC模块获取当前视频信息
	 * @param dc DC模块
	 * @param String 用户ID
	 * @return *
	 * 			Array 视频信息结果
	 * 			FALSE 查询失败
	 * 			TRUE  查询成功，但无数据
	 */
	static public function epg_get_live_info($dc,$video_id,$policy=NL_DC_AUTO) {
		$result = null;
		 
		if ($policy == NL_DC_AUTO){
			if ($dc->is_cache_enabled()) {
				$result = self::_get_live_info_by_cache($dc->cache(),$video_id);
				if ($result === FALSE ) {
					$result = self::_epg_get_live_info_by_db($dc->db(),$video_id);
					self::_set_live_info_by_cache($dc->cache(),$video_id,$result);
				}
			}
			else {
				$result = self::_epg_get_live_info_by_db($dc->db(),$video_id);
			}
		}elseif($policy == NL_DC_DB){
			$result = self::_epg_get_live_info_by_db($dc->db(),$video_id);
		}elseif($policy == NL_DC_CACHE){
			$result = self::_get_live_info_by_cache($dc->cache(),$video_id);
		}
		
		return $result;
	}

	/**
	 * 从CACHE模块获取当前视频信息
	 * @param cache CACHE模块
	 * @param String 用户ID
	 * @return * 
	 * 			Array 信息查询结果
	 * 			FALSE 为未缓存
	 */
	static private function _get_live_info_by_cache($cache,$video_id) {
		if ($cache===NULL) return FALSE;
		return unserialize($cache->get('live|#'.$video_id));
	}
	
	/**
	 * 为当前用户获取的信息数据设置缓存
	 * @param cache CACHE模块
	 * @param Array 信息数据
	 * @param String 用户ID
	 */
	static private function _set_live_info_by_cache($cache,$video_id,$data) {
		if ($cache===NULL) return FALSE;
		return $cache->set('live|#'.$video_id,serialize($data),self::CACHE_TIME_OUT);
	}
	
	/**
	 * 为当前用户获取的信息数据重置缓存
	 * @param cache CACHE模块
	 * @param String 用户ID
	 */
	static public function delete_live_info_by_cache($dc,$video_id) {
		if ($dc->cache()===NULL) return FALSE;
		self::delete_live_list_by_cache($dc);
		return $dc->cache()->delete('live|#'.$video_id);
	}
	
	
	/**
	 * 查询直播数据信息(guid查询) (直播)
	 * @param object $dc 数据库对象
	 * @param string $live_import_id 直播guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-12-29
	 */
	static public function get_live_info_by_id($dc,$live_id,$cp_id=0)
	{
		if(empty($live_id))
		{
			return self::return_data(1,'直播id为空');
		}
	
		$sql="select * from " . self::$base_table . " where nns_id='{$live_id}' and nns_cp_id='{$cp_id}' order by nns_modify_time desc limit 1";
// 				echo $sql;die;
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,"查询直播数据信息失败".$sql);
		}
		return self::return_data(0,"查询直播数据信息成功",$result);
	}
}
?>