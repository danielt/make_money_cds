<?php

/*
 * Created on 2012-10-11
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'public.class.php';

class nl_vod_index extends nl_public
{

    public static $base_table = 'nns_vod_index';

    public static $base_table_ex = 'nns_vod_index_ex';

    static $arr_filed_ex = array(
        'nns_vod_index_id',
        'nns_key',
        'nns_value',
        'nns_cp_id'
    );

    static $arr_filed = array(
        'nns_id',
        'nns_name',
        'nns_vod_id',
        'nns_index',
        'nns_time_len',
        'nns_summary',
        'nns_create_time',
        'nns_modify_time',
        'nns_image',
        'nns_play_count',
        'nns_score_total',
        'nns_score_count',
        'nns_import_id',
        'nns_import_source',
        'nns_actor',
        'nns_director',
        'nns_new_media',
        'nns_status',
        'nns_new_media_time',
        'nns_deleted',
        'nns_action',
        'nns_release_time',
        'nns_update_time',
        'nns_integer_id',
        'nns_watch_focus',
        'nns_state',
        'nns_cp_id',
        'nns_conf_info',
        'nns_ext_url'
    );

    const CACHE_TIME_OUT = 300;
    
    // 2013-4-2 Jacen add
    static public function count_vod_index($dc, $video_id)
    {
        $sql = 'select count(1) as num ' . 'from nns_vod_index ' . 'where nns_vod_id=\'' . $video_id . '\' ';
        
        $result = nl_query_by_db($sql, $dc->db());
        if (! is_array($result))
        {
            return 0;
        }
        return $result[0]['num'];
    }

    /**
     * 查询扩展信息
     * 
     * @param unknown $dc            
     * @param unknown $nns_import_id            
     * @param unknown $nns_cp_id            
     */
    static public function query_ex_by_id($dc, $nns_import_id, $nns_cp_id)
    {
        if (strlen($nns_import_id) < 1) 
        {
            return self::return_data(1, '$nns_import_id为空');
        }
        if (strlen($nns_cp_id) < 1) 
        {
            return self::return_data(1, '$nns_cp_id为空');
        }
        $sql = "select * from " . self::$base_table_ex . " where nns_vod_index_id='{$nns_import_id}' and nns_cp_id='{$nns_cp_id}'";
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (! empty($result) && is_array($result)) ? $result : null;
        return self::return_data(0, 'OK' . $sql, $result);
    }

    /**
     * 查询单个列表
     * 
     * @param object $dc
     *            数据库对象
     * @param string $nns_id
     *            guid
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     *         @date 2016-09-24
     */
    static public function query_by_id($dc, $nns_id)
    {
        $sql = "select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (isset($result[0]) && ! empty($result[0])) ? $result[0] : null;
        return self::return_data(0, 'OK:' . $sql, $result);
    }

    /**
     * 从DB模块获取当前视频信息
     * 
     * @param
     *            db DB模块
     * @param
     *            String 用户ID
     * @return * Array 视频信息结果
     *         FALSE 查询失败
     *         TRUE 查询成功，但无数据
     */
    static private function _get_vod_index_by_db($db, $video_id, $since, $num, $order)
    {
        $sql = 'select * ' . 'from nns_vod_index ' . 'where nns_vod_id=\'' . $video_id . '\' ' . 'order by nns_index ' . $order . ' limit ' . $since . ',' . $num;
        return nl_query_by_db($sql, $db);
    }

    /**
     * 从DC模块获取当前视频信息
     * 
     * @param
     *            dc DC模块
     * @param
     *            String 用户ID
     * @return * Array 视频信息结果
     *         FALSE 查询失败
     *         TRUE 查询成功，但无数据
     */
    static public function get_vod_index($dc, $video_id, $since = 0, $num = 20, $order = 'asc', $policy = NL_DC_AUTO)
    {
        $result = null;
        if ($policy == NL_DC_AUTO) 
        {
            if ($dc->is_cache_enabled()) 
            {
                $result = self::_get_vod_index_by_cache($dc->cache(), $video_id, $since, $num, $order);
                if ($result === FALSE) 
                {
                    $result = self::_get_vod_index_by_db($dc->db(), $video_id, $since, $num, $order);
                    self::_set_vod_index_by_cache($dc->cache(), $video_id, $since, $num, $order, $result);
                }
            } 
            else 
            {
                $result = self::_get_vod_index_by_db($dc->db(), $video_id, $since, $num, $order);
            }
        } 
        elseif ($policy == NL_DC_DB) 
        {
            $result = self::_get_vod_index_by_db($dc->db(), $video_id, $since, $num, $order);
        } 
        elseif ($policy == NL_DC_CACHE) 
        {
            $result = self::_get_vod_index_by_cache($dc->cache(), $video_id, $since, $num, $order);
        }
        return $result;
    }

    /**
     * 从CACHE模块获取当前视频信息
     * 
     * @param
     *            cache CACHE模块
     * @param
     *            String 用户ID
     * @return * Array 信息查询结果
     *         FALSE 为未缓存
     */
    static private function _get_vod_index_by_cache($cache, $video_id, $since, $num, $order)
    {
        if ($cache === NULL)
        {
            return FALSE;
        }
        // 获取GUID标识
        $comp_key = $cache->get('vod_index|#' . $video_id);
        return unserialize($cache->get($comp_key . '|#' . $since . '|#' . $num . '|#' . $order));
    }

    /**
     * 为当前用户获取的信息数据设置缓存
     * 
     * @param
     *            cache CACHE模块
     * @param
     *            Array 信息数据
     * @param
     *            String 用户ID
     */
    static private function _set_vod_index_by_cache($cache, $video_id, $since, $num, $order, $data)
    {
        if ($cache === NULL)
        {
            return FALSE;
        }
        $comp_key = $cache->get('vod_index|#' . $video_id);
        $comp_key = ($comp_key === FALSE) ? np_guid_rand() : $comp_key;
        $cache->set('vod_index|#' . $video_id, $comp_key);
        return $cache->set($comp_key . '|#' . $since . '|#' . $num . '|#' . $order, serialize($data), self::CACHE_TIME_OUT);
    }

    /**
     * 为当前用户获取的信息数据重置缓存
     * 
     * @param
     *            cache CACHE模块
     * @param
     *            String 用户ID
     */
    static public function delete_vod_index_by_cache($dc, $video_id)
    {
        if ($dc->cache() === NULL)
        {
            return FALSE;
        }
        return $dc->cache()->delete('vod_index|#' . $video_id);
    }

    /**
     * 从DC模块获取当前视频信息
     * 
     * @param
     *            dc DC模块
     * @param
     *            String 用户ID
     * @param
     *            String 分集数
     * @return * Array 视频信息结果
     *         FALSE 查询失败
     *         TRUE 查询成功，但无数据
     */
    static public function get_vod_index_info($db, $video_id, $index)
    {
        $sql = 'select * ' . 'from nns_vod_index ' . 'where nns_vod_id=\'' . $video_id . '\' AND nns_index =\'' . $index . '\' LIMIT 1';
        return nl_query_by_db($sql, $db->db());
    }

    static public function get_index_info_by_import_id($dc, $import_id, $mix_field = null, $import_source = null, $arr_limit = null)
    {
        if (strlen($import_id) <= 0) 
        {
            return true;
        }
        $mix_field = empty($mix_field) ? " * " : $mix_field;
        if (is_string($mix_field)) 
        {
            $str_field = trim($mix_field, ',');
        } 
        else 
        {
            if (is_array($mix_field)) 
            {
                $str_field = implode(",", $mix_field);
            } 
            else 
            {
                $str_field = " * ";
            }
        }
        $str_where = empty($import_source) ? "" : " and nns_import_source='{$import_source}'";
        $str_limit = "";
        if (is_array($arr_limit) && ! empty($arr_limit)) 
        {
            $arr_limit = array_values($arr_limit);
            $count = count($arr_limit);
            if ($count == 1) 
            {
                $str_limit = " limit {$arr_limit[0]} ";
            } 
            else 
            {
                $str_limit = " limit {$arr_limit[0]},{$arr_limit[1]} ";
            }
        }
        $sql = "select {$str_field} from nns_vod_index where nns_import_id='{$import_id}' {$str_where} {$str_limit} ";
        return nl_query_by_db($sql, $dc->db());
    }

    /**
     * 查询分集数据信息(guid查询)
     * 
     * @param object $dc
     *            数据库对象
     * @param string $video_index_id
     *            分集guid
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     *         @date 2015-12-29
     */
    static public function get_video_index_info_by_id($dc, $video_index_id)
    {
        if (empty($video_index_id)) 
        {
            return self::return_data(1, '分集guid为空');
        }
        $sql = "select * from " . self::$base_table . " where nns_id='{$video_index_id}' limit 1";
        // echo $sql;die;
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, "查询分集数据信息失败" . $sql);
        }
        return self::return_data(0, "查询分集数据信息成功", $result);
    }

    /**
     * 查询分集数据信息(注入ID查询)
     * 
     * @param object $dc
     *            数据库对象
     * @param string $video_index_id
     *            分集guid
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     *         @date 2015-12-29
     */
    static public function get_video_index_info_by_import_id($dc, $video_index_import_id, $cp_id = null)
    {
        if (empty($video_index_import_id)) 
        {
            return self::return_data(1, '分集注入id为空');
        }
        if (is_array($cp_id)) 
        {
            $cp_ids = implode("','", $cp_id);
            $str_cp = " and nns_cp_id in ('{$cp_ids}')";
        } 
        else 
        {
            $str_cp = $cp_id === null ? "" : " and nns_cp_id='{$cp_id}'";
        }
        $sql = "select * from " . self::$base_table . " where nns_import_id='{$video_index_import_id}' and nns_deleted!=1 {$str_cp} limit 1";
        // echo $sql;die;
        $result = nl_query_by_db($sql, $dc->db());
        if (! is_array($result)) 
        {
            return self::return_data(1, "查询分集数据信息失败" . $sql);
        }
        return self::return_data(0, "查询分集数据信息成功", $result);
    }

    /**
     * 修改分集信息
     * 
     * @param object $dc
     *            数据库对象
     * @param array $params
     *            数据数组
     * @param array|string $nns_id
     *            GUID
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     *         @date 2016-09-24
     */
    static public function edit($dc, $params, $nns_id)
    {
        if (isset($params['nns_id'])) 
        {
            unset($params['nns_id']);
        }
        $params = self::make_nns_pre($params);
        if (strlen($nns_id) < 0) 
        {
            return self::return_data(1, 'CDN直播频道队列guid为空');
        }
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (empty($params)) 
        {
            return self::return_data(1, '参数为空');
        }
        $params['nns_modify_time'] = date("Y-m-d H:i:s");
        $sql = self::make_update_sql(self::$base_table, $params, array('nns_id' => $nns_id));
        $result = nl_execute_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        return self::return_data(0, 'ok');
    }

    /**
     * 依据条件查询分集信息
     * @param $dc
     * @param $params
     * @return array
     */
    static public function query_by_condition($dc, $params)
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (! is_array($params) || empty($params)) 
        {
            return self::return_data(1, '查询条件为空不允许查询');
        }
        $sql = self::make_query_sql(self::$base_table, $params);
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (isset($result) && ! empty($result)) ? $result : null;
        return self::return_data(0, 'OK' . $sql, $result);
    }

    /**
     * 添加分集信息
     * 
     * @param object $dc
     *            数据库对象
     * @param array $params
     *            数据数组
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     *         @date 2016-03-06
     */
    static public function add($dc, $params)
    {
        $params['nns_create_time'] = $params['nns_modify_time'] = date('Y-m-d H:i:s');
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (empty($params) || ! is_array($params)) 
        {
            return self::return_data(1, '参数为空');
        }
        $sql = self::make_insert_sql(self::$base_table, $params);
        $result = nl_execute_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        return self::return_data(0, 'ok:' . $sql);
    }

    /**
     * 查询分集扩展信息
     * @param $dc
     * @param $params
     * @return array
     */
    static public function del_ex_by_condition($dc, $params)
    {
        $params = self::make_nns_pre($params);
        if (empty($params) || ! is_array($params)) 
        {
            return self::return_data(1, '参数为空');
        }
        $sql = self::make_delete_sql(self::$base_table_ex, $params);
        if (strlen($sql) < 1) 
        {
            return self::return_data(1, '数据组装sql为空，不允许删除');
        }
        $result = nl_execute_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        return self::return_data(0, 'OK' . $sql);
    }

    /**
     * 添加分集扩展信息
     * 
     * @param object $dc
     *            数据库对象
     * @param array $params
     *            数据数组
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     *         @date 2016-03-06
     */
    static public function add_ex($dc, $params)
    {
        $params = self::except_useless_params(self::$arr_filed_ex, $params);
        if (empty($params) || ! is_array($params)) 
        {
            return self::return_data(1, '参数为空');
        }
        $sql = self::make_insert_sql(self::$base_table_ex, $params);
        $result = nl_execute_by_db($sql, $dc->db());
        if (! $result) 
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        return self::return_data(0, 'ok:' . $sql);
    }
}