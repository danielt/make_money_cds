<?php
/*
 * Created on 2012-11-14
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
 class nl_live_info_language{
 	const CACHE_TIME_OUT=300;
 	
 	/*
 	 * 从DC模块获取点播对应语言信息
 	 * get_live_info_by_language
 	 * _get_live_info_by_language_db
 	 * _get_live_info_by_language_cache
 	 * _set_live_info_by_language_cache
 	 * delete_live_info_by_language_cache
 	 */
 	
	/**
	 * 从DC模块获取点播对应语言信息
	 * @param dc DC模块
	 * @param string 视频ID
	 * @param string 	信息语言类型。ch|en|tw|jp 仅为其中一个
						同全局配置文件get_g_extend_language方法返回的数据。
	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
					默认为NL_DC_AUTO
	 * @return *
	 * 			Array 信息包列表结果
	 * 			FALSE 查询失败
	 * 			TRUE  查询成功，但无数据
	 */

	static public function get_live_info_by_language($dc,$live_id,$language ,$policy=NL_DC_AUTO) {
		$result = null;
		if ($policy == NL_DC_AUTO){
			if ($dc->is_cache_enabled()) {
				$result = self::_get_live_info_by_language_cache($dc->cache(),$live_id,$language);
				if ($result === FALSE ) {
					$result = self::_get_live_info_by_language_db($dc->db(),$live_id,$language);
					self::_set_live_info_by_language_cache($dc->cache(),$live_id,$language,$result);
				}
			}
			else {
				$result = self::_get_live_info_by_language_db($dc->db(),$live_id,$language);
			}
		}elseif($policy == NL_DC_DB){
			$result = self::_get_live_info_by_language_db($dc->db(),$live_id,$language);
		}elseif($policy == NL_DC_CACHE){
			$result = self::_get_live_info_by_language_cache($dc->cache(),$live_id,$language);
		}
		
		return $result;
	}
	
	static private function _get_live_info_by_language_db($db,$live_id,$language) {
		$sql = 'select * ' .
				'from nns_live_info_language ' .
				'where nns_live_id=\''.$live_id.'\' ' .
				'and nns_language=\''.$language.'\'';

		return nl_query_by_db($sql, $db);
	}
	static private function _get_live_info_by_language_cache($cache,$live_id,$language) {
		if ($cache===NULL) return FALSE;
		return unserialize($cache->get('live_ex_info|#'.$live_id.'|#'.$language));
	}
	static private function _set_live_info_by_language_cache($cache,$live_id,$language,$data) {
		if ($cache===NULL) return FALSE;
		return $cache->set('live_ex_info|#'.$live_id.'|#'.$language,serialize($data),self::CACHE_TIME_OUT);
	}

	static public function delete_live_info_by_language_cache($dc,$live_id,$language) {
		if ($dc->cache()===NULL) return FALSE;
		return $dc->cache()->delete('live_ex_info|#'.$live_id.'|#'.$language);
	}
	
	
	/*
 	 * 从DC模块添加对应语言视频信息
 	 * add_live_info_by_language
 	 * _add_live_info_by_language_db
 	 */
 	
	/**
	 * 从DC模块添加对应语言视频信息
	 * @param dc DC模块
	 * @param string 视频ID
	 * @param string 	信息语言类型。ch|en|tw|jp 仅为其中一个
						同全局配置文件get_g_extend_language方法返回的数据。
	 * @param array 所需参数组
				Array(
		
					 “name”=>点播名称，
					“area”=>上映地区,
					“summary”=>简介,
					“alias_name”=>别名
					“live_language”=>视频语言,

)
	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
					默认为NL_DC_DB
	 * @return *
	 * 			FALSE 添加失败
	 * 			TRUE  添加成功
	 */


	static public function add_live_info_by_language($dc,$live_id,$language ,$params,$policy=NL_DC_DB) {
		$result = self::_add_live_info_by_language_db($dc->db(),$live_id,$language ,$params);
		return $result;
	}
	
	static private function _add_live_info_by_language_db($db,$live_id,$language ,$params) {
		$filed_str='nns_live_id,nns_language,';
		$value_str='\''.$live_id.'\',\''.$language.'\',';
		foreach ($params as $param_key=>$param_item){
			$filed_str.='nns_'.$param_key.',';
			$value_str.='\''.htmlspecialchars($param_item, ENT_QUOTES).'\',';
		}
		$filed_str=rtrim($filed_str,',');
		$value_str=rtrim($value_str,',');
		$sql = 'insert into nns_live_info_language ' .
				'('.$filed_str.') values ('.$value_str.')';

		return nl_execute_by_db($sql, $db);
	}
	/*
 	 * 从DC模块修改对应语言视频信息
 	 * modify_live_info_by_language
 	 * _modify_live_info_by_language_db
 	 */
 	
	/**
	 * 从DC模块修改对应语言视频信息
	 * @param dc DC模块
	 * @param string 视频ID
	 * @param string 	信息语言类型。ch|en|tw|jp 仅为其中一个
						同全局配置文件get_g_extend_language方法返回的数据。
	 * @param array 所需参数组
				Array(
		
					 “name”=>点播名称，
					“area”=>上映地区,
					“summary”=>简介,
					“alias_name”=>别名
					“live_language”=>视频语言,
		
				)
	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
					默认为NL_DC_AUTO
	 * @return *
	 * 			FALSE 添加失败
	 * 			TRUE  添加成功
	 */



	static public function modify_live_info_by_language($dc,$live_id,$language ,$params,$policy=NL_DC_DB) {
		if (self::get_live_info_by_language($dc,$live_id,$language,$policy)===TRUE){
			$result=self::add_live_info_by_language($dc,$live_id,$language ,$params,$policy);
		}else{
			$result = self::_modify_live_info_by_language_db($dc->db(),$live_id,$language ,$params);
		}
		if ($result===TRUE){
			self::delete_live_info_by_language_cache($dc,$live_id,$language);
		}
		return $result;
	}
	
	static private function _modify_live_info_by_language_db($db,$live_id,$language ,$params) {
		$update_str='';
		foreach ($params as $param_key=>$param_item){
			$update_str.='nns_'.$param_key.'=\''.htmlspecialchars($param_item, ENT_QUOTES).'\',';
		}
		$update_str=rtrim($update_str,',');
		$sql = 'update nns_live_info_language set ' .
				$update_str.' where '.
				'nns_live_id=\''.$live_id.'\' and ' .
				'nns_language=\''.$language.'\' ';

		return nl_execute_by_db($sql, $db);
	}
	
	/*
 	 * 从DC模块删除对应语言的视频信息
 	 * delete_live_info_by_language
 	 * _delete_live_info_by_language_db
 	 */
 	
	/**
	 * 从DC模块删除对应语言的视频信息
	 * @param dc DC模块
	 * @param string 点播ID
	 * @param string 	信息语言类型。ch|en|tw|jp 仅为其中一个
						同全局配置文件get_g_extend_language方法返回的数据。
	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE 
						默认为NL_DC_DB
	 * @return *
	 * 			FALSE 删除失败
	 * 			TRUE 删除成功
	 */




	static public function delete_live_info_by_language($dc,$live_id ,$policy=NL_DC_DB) {
		$result = self::_delete_live_info_by_language_db($dc->db(),$live_id );
		return $result;
	}
	
	static private function _delete_live_info_by_language_db($db,$live_id ) {

		$sql = 'delete from nns_live_info_language ' .
				' where '.
				'nns_live_id=\''.$live_id.'\'  ' ;

		return nl_execute_by_db($sql, $db);
	}
	
	
	/*
 	 * 根据点播ID批量查询对应语言的信息
 	 * get_live_info_by_language_ids
 	 */
 	
	/**
	 * 根据点播ID批量查询对应语言的信息
	 * @param dc DC模块
     * @param array 点播包ID数组
	 * @param string 	信息语言类型。ch|en|tw|jp 仅为其中一个
						同全局配置文件get_g_extend_language方法返回的数据。
	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE 
					默认为NL_DC_DB
	 * @return *
	 *			ARRAY（
					“点播ID”=>array 点播内容
					“点播ID”=>array 点播内容
					） 查询成功
	 * 			FALSE 查询失败
	 * 			TRUE  操作成功，但无数据
	 */

	static public function get_live_info_by_language_ids($dc,$ids, $language,$policy=NL_DC_DB) {
		$array=array();
		foreach ($ids as $live_id){
			$result = self::get_live_info_by_language($dc,$live_id,$language );
			if ($result===FALSE) return FALSE;
			if ($result!==TRUE){
				$array[$live_id]=$result;
			}
		}
		
		if (count($array)==0) return TRUE;
		
		return $array;
	}
	
	/*
 	 * 从媒资包根据对应语言字段搜索影片，并返回该语言的影片信息。(前端专用)
 	 * epg_get_search_live_by_language_asset
 	 * _epg_get_search_live_by_language_asset_db
 	 * _create_search_live_by_language_asset_sql_str
 	 */
 	
	/**
	 * 从媒资包根据对应语言字段搜索影片，并返回该语言的影片信息。(前端专用)
	 * @param dc DC 模块
	 * @param string  tag epg输出标识
	 * @param string 	信息语言类型。ch|en|tw|jp 仅为其中一个
						同全局配置文件get_g_extend_language方法返回的数据。
	 * @param int 起始查询记录位置
	 * @param int 查询记录条数
	 * @param array 查询参数组
			Array(
				“asset_id”=>媒资包ID ，用“#”隔开  =查询 and连接
										用“|”隔开  =查询 OR连接
				“category_id”=>媒资栏目ID ，用“#”隔开  =查询 and连接
											用“|”隔开  =查询 OR连接
				“name”=>视频名称，LIKE查询，
				“summary”=>视频简介，LIKE查询,
				“alias_name”=>别名，LIKE查询,
				“live_language”=>视频语言，=查询
				“pinyin_index”=>拼音索引查询，首字匹配LIKE查询 
				“english_index”=>英文索引查询，首字匹配LIKE查询
				
)
	*@param string 排序规则，NL_ORDER_NUM_ASC | NL_ORDER_NUM_DESC | NL_ORDER_TIME_ASC | NL_ORDER_TIME_DESC | NL_ORDER_CLICK_ASC | NL_ORDER_CLICK_DESC
				默认为 NL_ORDER_NUM_DESC
				NL_ORDER_NUM_ASC 按ORDER字段升序排列
				NL_ORDER_NUM_DESC 按ORDER字段降序排列
				NL_ORDER_TIME_ASC 按创建时间升序排列
				NL_ORDER_TIME_ DESC 按创建时间降序排列
				NL_ORDER_CLICK_ASC按点击数升序排列
				NL_ORDER_CLICK_DESC按点击数降序排列
	 
	 * @return *
				ARRAY 查询结果
	 * 			FALSE 查询失败
	 * 			TRUE 查询成功，但没有数据
	 */


	static public function epg_get_search_live_by_language_asset($dc,$tag,$language,$since,$num,$params,$order) {
	
		$result = self::_epg_get_search_live_by_language_asset_db(
			$dc->db(),
			$tag,
			$language,
			$since,
			$num,
			$params,
			$order 
			);
		return $result;
	}
	static private function _epg_get_search_live_by_language_asset_db($db,$tag,$language,$since,$num,$params,$order) {
		
		$sql = self::_create_search_live_by_language_asset_sql_str($params);
		
		$sql.=' and ( ISNULL(a.nns_tag) or a.nns_tag=\'\' or LOCATE(\','.$tag.',\',CONCAT(\',\',a.nns_tag))>0 ) ' .
			  ' and ( ISNULL(a.nns_single_tag) or a.nns_single_tag=\'\' or LOCATE(\','.$tag.',\',CONCAT(\',\',a.nns_single_tag))>0 ) ';
		
		$sql.=' and b.nns_language=\''.$language.'\' ';
		
		switch ($order){
			case NL_ORDER_NUM_DESC:
				$order_str=' order by a.nns_order desc '; 
			break;
			case NL_ORDER_NUM_ASC:
				$order_str=' order by a.nns_order asc '; 
			break;
			case NL_ORDER_TIME_DESC:
				$order_str=' order by a.nns_create_time desc '; 
			break;
			case NL_ORDER_TIME_ASC:
				$order_str=' order by a.nns_create_time asc '; 
			break;
			case NL_ORDER_CLICK_DESC:
				$order_str=' order by a.nns_play_count desc '; 
			break;
			case NL_ORDER_CLICK_ASC:
				$order_str=' order by a.nns_play_count asc '; 
			break;
			default:
			break;
		}
		
		$sql.=$order_str;
		
		$sql.=' limit '.$since.','.$num;
		
		return nl_query_by_db($sql, $db);
	}
	
	
	static private function _create_search_live_by_language_asset_sql_str($params){
		$sql_fields='select a.*,b.nns_name ';
		$sql_str='from nns_assists_item as a ' .
				'left join nns_live_info_language as b ' .
				'on a.nns_video_id=b.nns_live_id where a.nns_video_type=1 ';
		foreach ($params as $param_key=>$param){
			
			if (empty($param)) continue;
			
			switch ($param_key){
				case 'asset_id':
					$command_str=nl_db_query_and_or($param,'a.nns_assist_id');
					$sql_str.='and (';					
					$sql_str.=$command_str.') ';
				break;
				case 'category_id':
					$command_str=nl_db_query_and_or($param,'a.nns_category_id');
					$sql_str.='and (';					
					$sql_str.=$command_str.') ';
				break;
				case 'pinyin_index':
					$sql_str.='and a.nns_pinyin LIKE \''.$param.'%\' ';					
				break;
				case 'english_index':
					$sql_str.='and a.nns_english_index LIKE \''.$param.'%\' ';					
				break;
				case 'name':
					$sql_str.='and b.nns_name LIKE \'%'.$param.'%\' ';					
				break;
				case 'summary':
					$sql_str.='and b.nns_summary LIKE \'%'.$param.'%\' ';
					$sql_fields.=',b.nns_summary ';				
				break;
			
				case 'alias_name':
					$sql_str.='and b.nns_alias_name LIKE \'%'.$param.'%\' ';
					$sql_fields.=',b.nns_alias_name ';				
				break;
				case 'live_language':
					$sql_str.='and b.nns_live_language = \''.$param.'\' ';
					$sql_fields.=',b.nns_live_language ';				
				break;
				
			}
		}
		
		$sql_str=$sql_fields.$sql_str;
		return $sql_str;
	}
 }
?>
