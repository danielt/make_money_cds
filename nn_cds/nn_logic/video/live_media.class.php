<?php
/*
 * Created on 2012-10-11
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_live_media extends nl_public
{
	
	static public $base_table = 'nns_live_media';
	const CACHE_TIME_OUT=300;
/*
 * *********************************************
 * get_live_media
 * 获取当前直播片源信息组
 * ********************************************
 */
 	/**
	 * 从DC模块获取当前直播片源信息组
	 * @param dc DC模块
	 * @param String 视频ID
	 * @param int 视频分集 可为空 默认为0
	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
	 * @return *
	 * 			Array 视频信息结果
	 * 			FALSE 查询失败
	 * 			TRUE  查询成功，但无数据
	 */
	static public function get_live_media($dc,$video_id,$index=0,$policy=NL_DC_AUTO) {
		$result = null;
		if (empty($index)) $index=0;
		if ($policy == NL_DC_AUTO){
			if ($dc->is_cache_enabled()) {
				$result = self::_get_live_media_by_cache($dc->cache(),$video_id,$index);
				if ($result === FALSE ) {
					$result = self::_get_live_media_by_db($dc->db(),$video_id,$index);
					self::_set_live_media_by_cache($dc->cache(),$video_id,$index,$result);
				}
			}
			else {
				$result = self::_get_live_media_by_db($dc->db(),$video_id,$index);
			}
		}elseif($policy == NL_DC_DB){
			$result = self::_get_live_media_by_db($dc->db(),$video_id,$index);
		}elseif($policy == NL_DC_CACHE){
			$result = self::_get_live_media_by_cache($dc->cache(),$video_id,$index);
		}
		
		return $result;
	}

	static private function _get_live_media_by_db($db,$video_id,$index) {
		$sql = 'select * ' .
//				'nns_id,' .
//				'nns_name,' .
//				'nns_live_id,' .
//				'nns_tag,' .
//				'nns_mode,' .
//				'nns_url,' .
//				'nns_kbps,' .
//				'nns_state,' .
//				'nns_content_id,' .
//				'nns_filetype,' .
//				'nns_media_caps,' .
//				'nns_live_index ' .
				'from nns_live_media ' .
				'where nns_live_id=\''.$video_id.'\' ' .
				'and nns_live_index='.$index;

		return nl_query_by_db($sql, $db);
	}
	

	static private function _get_live_media_by_cache($cache,$video_id,$index) {
		if ($cache===NULL) return FALSE;
		$index= empty($index)?0:$index;
		return unserialize($cache->get('live_media|#'.$video_id.'|#'.$index));
	}
	

	static private function _set_live_media_by_cache($cache,$video_id,$index,$data) {
		if ($cache===NULL) return FALSE;
		$index= empty($index)?0:$index;
		return $cache->set('live_media|#'.$video_id.'|#'.$index,serialize($data),self::CACHE_TIME_OUT);
	}
	

	static public function delete_live_media_by_cache($dc,$video_id,$index) {
		if ($dc->cache()===NULL) return FALSE;
		$index= empty($index)?0:$index;
		return $dc->cache()->delete('live_media|#'.$video_id.'|#'.$index);
	}
	
	/**
	 * 查询片源数据信息(guid查询) 直播
	 * @param object $dc 数据库对象
	 * @param string $live_media_import_id 直播片源注入guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-12-29
	 */
	static public function get_live_media_info_by_import_id($dc,$live_media_import_id,$cp_id=0)
	{
		if(empty($live_media_import_id))
		{
			return self::return_data(1,'片源注入id为空');
		}
	
		$sql="select * from " . self::$base_table . " where nns_content_id='{$live_media_import_id}' and nns_cp_id='{$cp_id}' order by nns_modify_time desc limit 1";
		// 		echo $sql;die;
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,"查询片源数据信息失败".$sql);
		}
		return self::return_data(0,"查询片源数据信息成功",$result);
	}
}
?>
