<?php
 include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
class nl_vod_index_info_language {
	const CACHE_TIME_OUT=300;
	
	/*
 	 * 从DC模块获取点播对应语言信息
 	 * get_vod_index_info_by_language
 	 * _get_vod_index_info_by_language_db
 	 * _get_vod_index_info_by_language_cache
 	 * _set_vod_index_info_by_language_cache
 	 * delete_vod_index_info_by_language_cache
 	 */
 	
	/**
	 * 从DC模块获取点播分集对应语言信息
	 * @param dc DC模块
	 * @param string 视频ID
	 * @param int 分集数 0～N  0为分片一
	 * @param string 	信息语言类型。ch|en|tw|jp 仅为其中一个
						同全局配置文件get_g_extend_language方法返回的数据。
	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
					默认为NL_DC_AUTO
	 * @return *
	 * 			Array 信息包列表结果
	 * 			FALSE 查询失败
	 * 			TRUE  查询成功，但无数据
	 */


	static public function get_vod_index_info_by_language($dc,$vod_id,$index,$language ,$policy=NL_DC_AUTO) {
		$index=empty($index)?0:$index;
		$result = null;
		if ($policy == NL_DC_AUTO){
			if ($dc->is_cache_enabled()) {
				$result = self::_get_vod_index_info_by_language_cache($dc->cache(),$vod_id,$index,$language);
				if ($result === FALSE ) {
					$result = self::_get_vod_index_info_by_language_db($dc->db(),$vod_id,$index,$language);
					self::_set_vod_index_info_by_language_cache($dc->cache(),$vod_id,$index,$language,$result);
				}
			}
			else {
				$result = self::_get_vod_index_info_by_language_db($dc->db(),$vod_id,$index,$language);
			}
		}elseif($policy == NL_DC_DB){
			$result = self::_get_vod_index_info_by_language_db($dc->db(),$vod_id,$index,$language);
		}elseif($policy == NL_DC_CACHE){
			$result = self::_get_vod_index_info_by_language_cache($dc->cache(),$vod_id,$index,$language);
		}
		
		return $result;
	}
	
	static private function _get_vod_index_info_by_language_db($db,$vod_id,$index,$language ) {
		$sql = 'select * ' .
				'from nns_vod_index_info_language ' .
				'where nns_vod_id=\''.$vod_id.'\' ' .
				'and nns_language=\''.$language.'\' ' ;
		if ($index!==NULL) $sql .='and nns_index='.$index;
		
		return nl_query_by_db($sql, $db);
	}
	static private function _get_vod_index_info_by_language_cache($cache,$vod_id,$index,$language ) {
		if ($cache===NULL) return FALSE;
		return unserialize($cache->get('vod_index_ex_info|#'.$vod_id.'|#'.$language.'|#'.$index));
	}
	static private function _set_vod_index_info_by_language_cache($cache,$vod_id,$index,$language ,$data) {
		if ($cache===NULL) return FALSE;
		return $cache->set('vod_index_ex_info|#'.$vod_id.'|#'.$language.'|#'.$index,serialize($data),self::CACHE_TIME_OUT);
	}

	static public function delete_vod_index_info_by_language_cache($dc,$vod_id,$index,$language ) {
		if ($dc->cache()===NULL) return FALSE;
		return $dc->cache()->delete('vod_index_ex_info|#'.$vod_id.'|#'.$language.'|#'.$index);
	}
	
	/*
 	 * 从DC模块添加对应语言视频分集信息
 	 * add_vod_index_info_by_language
 	 * _add_vod_index_info_by_language_db
 	 */
	/**
	 * 从DC模块添加对应语言视频分集信息
	 * @param dc DC模块
	 * @param string 视频ID
	 * @param int 分集数 0～N  0为分片一
	 * @param string 	信息语言类型。ch|en|tw|jp 仅为其中一个
						同全局配置文件get_g_extend_language方法返回的数据。
	 * @param array 所需参数组
		Array(

			 “name”=>分集名称，
			“summary”=>分集简介
			

			)
	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
					默认为NL_DC_DB
	 * @return *
	 * 			FALSE 添加失败
	 * 			TRUE  添加成功
	 */
	
	static public function add_vod_index_info_by_language ($dc,$vod_id,$index,$language ,$params,$policy=NL_DC_DB){
		$index=empty($index)?0:$index;
		$result=self::_add_vod_index_info_by_language_db($dc->db(),$vod_id,$index,$language ,$params);
		return $result;
	}
	static private function _add_vod_index_info_by_language_db ($db,$vod_id,$index,$language ,$params){
		$filed_str='nns_vod_id,nns_language,nns_index,';
		$value_str='\''.$vod_id.'\',\''.$language.'\','.$index.',';
		foreach ($params as $param_key=>$param_item){
			$filed_str.='nns_'.$param_key.',';
			$value_str.='\''.htmlspecialchars($param_item, ENT_QUOTES).'\',';
		}
		$filed_str=rtrim($filed_str,',');
		$value_str=rtrim($value_str,',');
		$sql = 'insert into nns_vod_index_info_language ' .
				'('.$filed_str.') values ('.$value_str.')';
				
//		echo $sql;
//		die;
		return nl_execute_by_db($sql, $db);
	}
	
	/*
 	 * 从DC模块修改对应语言视频分集信息
 	 * modify_vod_index_info_by_language
 	 * _modify_vod_index_info_by_language_db
 	 */
	/**
	 * 从DC模块修改对应语言视频分集信息
	 * @param dc DC模块
	 * @param string 视频ID
	 * @param int 分集数 0～N  0为分片一
	 * @param string 	信息语言类型。ch|en|tw|jp 仅为其中一个
						同全局配置文件get_g_extend_language方法返回的数据。
	 * @param array 所需参数组
		Array(

			 “name”=>分集名称，
			“summary”=>分集简介

)
	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
					默认为NL_DC_AUTO
	 * @return *
	 * 			FALSE 添加失败
	 * 			TRUE  添加成功
	 */

	
	static public function modify_vod_index_info_by_language ($dc,$vod_id,$index,$language ,$params,$policy=NL_DC_DB){
		$index=empty($index)?0:$index;
		if (self::get_vod_index_info_by_language($dc,$vod_id,$index,$language,$policy)===TRUE){
			$result=self::add_vod_index_info_by_language($dc,$vod_id,$index,$language ,$params,$policy);
		}else{
			$result=self::_modify_vod_index_info_by_language_db($dc->db(),$vod_id,$index,$language ,$params);
		}
		if ($result===TRUE){
			self::delete_vod_index_info_by_language_cache($dc,$vod_id,$index,$language);
		}
		return $result;
	}
	static private function _modify_vod_index_info_by_language_db ($db,$vod_id,$index,$language ,$params){
		$update_str='';
		foreach ($params as $param_key=>$param_item){
			$update_str.='nns_'.$param_key.'=\''.htmlspecialchars($param_item, ENT_QUOTES).'\',';
		}
		$update_str=rtrim($update_str,',');
		$sql = 'update nns_vod_index_info_language set ' .
				$update_str.' where '.
				'nns_vod_id=\''.$vod_id.'\' and ' .
				'nns_language=\''.$language.'\' and '.
				'nns_index=\''.$index.'\'';
				
//		echo $sql;
//		die;
		return nl_execute_by_db($sql, $db);
	}
	
	/*
 	 * 根据语言类型删除对应点播分集信息
 	 * delete_vod_index_info_by_language
 	 * _delete_vod_index_info_by_language_db
 	 */
	/**
	 * 从DC模块删除对应语言的视频分集信息
	 * @param dc DC模块
	 * @param string 点播ID
	 * @param int 分集数 0～N  0为分片一
	 * @param string 	信息语言类型。ch|en|tw|jp 仅为其中一个
						同全局配置文件get_g_extend_language方法返回的数据。
	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE 
					默认为NL_DC_DB
	 * @return *
	 * 			FALSE 删除失败
	 * 			TRUE 删除成功
	 */

	
	static public function delete_vod_index_info_by_language ($dc,$vod_id,$policy=NL_DC_DB){
		$index=empty($index)?0:$index;
		$result=self::_delete_vod_index_info_by_language_db($dc->db(),$vod_id );
		
		return $result;
	}
	static private function _delete_vod_index_info_by_language_db ($db,$vod_id ){
		
		$sql = 'delete from  nns_vod_index_info_language ' .
				' where '.
				'nns_vod_id=\''.$vod_id.'\' ' ;
		return nl_execute_by_db($sql, $db);
	}
	
}
?>