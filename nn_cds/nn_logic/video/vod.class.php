<?php
/*
 * Created on 2012-10-11
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'public.class.php';

class nl_vod extends nl_public
{
    static public $base_table = 'nns_vod';
    static public $base_table_ex = 'nns_vod_ex';
    const CACHE_TIME_OUT = 300;

    static $arr_filed_ex = array(
        'nns_vod_id',
        'nns_key',
        'nns_value',
        'nns_cp_id',
    );

    static $arr_filed = array(
        'nns_id',
        'nns_name',
        'nns_state',
        'nns_view_type',
//                               'nns_pub_category_id',
//                               'nns_pri_category_id',
//                               'nns_pub_category_detail_id',
//                               'nns_pri_category_detail_id',
        'nns_org_type',
        'nns_org_id',
        'nns_tag',
//                               'nns_tag0',
//                               'nns_tag1',
//                               'nns_tag2',
//                               'nns_tag3',
//                               'nns_tag4',
//                               'nns_tag5',
//                               'nns_tag6',
//                               'nns_tag7',
//                               'nns_tag8',
//                               'nns_tag9',
//                               'nns_tag10',
//                               'nns_tag11',
//                               'nns_tag12',
//                               'nns_tag13',
//                               'nns_tag14',
//                               'nns_tag15',
//                               'nns_tag16',
//                               'nns_tag17',
//                               'nns_tag18',
//                               'nns_tag19',
//                               'nns_tag20',
//                               'nns_tag21',
//                               'nns_tag22',
//                               'nns_tag23',
//                               'nns_tag24',
//                               'nns_tag25',
//                               'nns_tag26',
//                               'nns_tag27',
//                               'nns_tag28',
//                               'nns_tag29',
//                               'nns_tag30',
//                               'nns_tag31',
        'nns_director',
        'nns_actor',
        'nns_show_time',
        'nns_view_len',
        'nns_all_index',
        'nns_new_index',
        'nns_area',
        'nns_image0',
        'nns_image1',
        'nns_image2',
        'nns_image3',
        'nns_image4',
        'nns_image5',
        'nns_summary',
        'nns_remark',
        'nns_create_time',
        'nns_modify_time',
        'nns_deleted',
        'nns_check',
        'nns_depot_id',
        'nns_category_id',
        'nns_play_count',
        'nns_score_total',
        'nns_score_count',
        'nns_point',
        'nns_copyright_date',
        'nns_asset_import_id',
        'nns_pinyin',
        'nns_pinyin_length',
        'nns_alias_name',
        'nns_eng_name',
        'nns_language',
        'nns_text_lang',
        'nns_producer',
        'nns_screenwriter',
        'nns_play_role',
        'nns_copyright_range',
        'nns_vod_part',
        'nns_keyword',
        'nns_import_source',
        'nns_kind',
        'nns_copyright',
        'nns_clarity',
        'nns_status',
        'nns_action',
        'nns_integer_id',
        'nns_image_v',
        'nns_image_s',
        'nns_image_h',
        'nns_cp_id',
        'nns_conf_info',
        'nns_ext_url',
        'nns_ishuaxu',
    );

    /**
     * 从DB模块获取当前视频信息
     * @param db DB模块
     * @param String 用户ID
     * @return *
     *            Array 视频信息结果
     *            FALSE 查询失败
     *            TRUE  查询成功，但无数据
     */
    static private function _epg_get_vod_info_by_db($db, $video_id)
    {
        $sql = 'select ' .
            'nns_id,' .
            'nns_category_id,' .
            'nns_name,' .
            'nns_image0,' .
            'nns_image1,' .
            'nns_image2,' .
            'nns_play_count,' .
            'nns_show_time,' .
            'nns_view_type,' .
            'nns_view_len,' .
            'nns_all_index,' .
            'nns_director,' .
            'nns_actor,' .
            'nns_show_time,' .
            'nns_area,' .
            'nns_summary,' .
            'nns_kind,' .
            'nns_import_source,' .
            'nns_asset_import_id,' .
            'nns_point ' .
            'from nns_vod ' .
            'where nns_id=\'' . $video_id . '\'';
        return nl_query_by_db($sql, $db);
    }

    /**
     * 查询扩展信息
     * @param $dc
     * @param $nns_import_id
     * @param $nns_cp_id
     * @return array
     */
    static public function query_ex_by_id($dc, $nns_import_id, $nns_cp_id)
    {
        if (strlen($nns_import_id) < 1)
        {
            return self::return_data(1, '$nns_import_id为空');
        }
        if (strlen($nns_cp_id) < 1)
        {
            return self::return_data(1, '$nns_cp_id为空');
        }
        $sql = "select * from " . self::$base_table_ex . " where nns_vod_id='{$nns_import_id}' and nns_cp_id='{$nns_cp_id}'";
        $result = nl_query_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (!empty($result) && is_array($result)) ? $result : null;
        return self::return_data(0, 'OK' . $sql, $result);
    }

    /**
     * 查询单个列表
     * @param object $dc 数据库对象
     * @param string $nns_id guid
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     * @date 2016-09-24
     */
    static public function query_by_id($dc, $nns_id)
    {
        $sql = "select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
        $result = nl_query_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
        return self::return_data(0, 'OK:' . $sql, $result);
    }


    /**
     * 从DC模块获取当前视频信息
     * @param dc DC模块
     * @param String 用户ID
     * @return *
     *            Array 视频信息结果
     *            FALSE 查询失败
     *            TRUE  查询成功，但无数据
     */
    static public function epg_get_vod_info($dc, $video_id, $policy = NL_DC_AUTO)
    {
        $result = null;
        if ($policy == NL_DC_AUTO)
        {
            if ($dc->is_cache_enabled())
            {
                $result = self::_get_vod_info_by_cache($dc->cache(), $video_id);
                if ($result === FALSE)
                {
                    $result = self::_epg_get_vod_info_by_db($dc->db(), $video_id);
                    self::_set_vod_info_by_cache($dc->cache(), $video_id, $result);
                }
            } else
            {
                $result = self::_epg_get_vod_info_by_db($dc->db(), $video_id);
            }
        } elseif ($policy == NL_DC_DB)
        {
            $result = self::_epg_get_vod_info_by_db($dc->db(), $video_id);
        } elseif ($policy == NL_DC_CACHE)
        {
            $result = self::_get_vod_info_by_cache($dc->cache(), $video_id);
        }

        return $result;
    }

    /**
     * 从CACHE模块获取当前视频信息
     * @param cache CACHE模块
     * @param String 用户ID
     * @return *
     *            Array 信息查询结果
     *            FALSE 为未缓存
     */
    static private function _get_vod_info_by_cache($cache, $video_id)
    {
        if ($cache === NULL) return FALSE;
        return unserialize($cache->get('vod|#' . $video_id));
    }

    /**
     * 为当前用户获取的信息数据设置缓存
     * @param cache CACHE模块
     * @param Array 信息数据
     * @param String 用户ID
     */
    static private function _set_vod_info_by_cache($cache, $video_id, $data)
    {
        if ($cache === NULL) return FALSE;
        return $cache->set('vod|#' . $video_id, serialize($data), self::CACHE_TIME_OUT);
    }

    /**
     * 为当前用户获取的信息数据重置缓存
     * @param cache CACHE模块
     * @param String 用户ID
     */
    static public function delete_vod_info_by_cache($dc, $video_id)
    {
        if ($dc->cache() === NULL) return FALSE;
        return $dc->cache()->delete('vod|#' . $video_id);
    }

    /**
     * 根据视频id取视频名字
     * @param $dc
     * @param $video_id 视频id
     * @param $video_type 视频类型
     */
    static public function get_videoname_by_videoid($db, $video_id, $video_type)
    {
        if ($video_type == 'vod')
        {
            $sql = 'select nns_name from nns_vod where nns_id=\'' . $video_id . '\'';
        }
        if ($video_type == 'index')
        {
            $sql = 'select i.nns_name from nns_vod_index as i left join nns_vod as v on v.nns_id=i.nns_vod_id  where i.nns_id=\'' . $video_id . '\'';

        }
        if ($video_type == 'media')
        {
            $sql = 'select i.nns_name from nns_vod_media as m  left join nns_vod_index as i  on m.nns_vod_index_id=i.nns_id  where m.nns_id=\'' . $video_id . '\'';
        }
        return nl_query_by_db($sql, $db);
    }

    /**
     * 查询主媒资数据信息(guid查询)
     * @param object $dc 数据库对象
     * @param string $video_meida_id 分集guid
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     * @date 2015-12-29
     */
    static public function get_video_info_by_id($dc, $video_id)
    {
        if (empty($video_id))
        {
            return self::return_data(1, '主媒资guid为空');
        }
        $sql = "select * from " . self::$base_table . " where nns_id='{$video_id}' limit 1";
        // 		echo $sql;die;
        $result = nl_query_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, "查询主媒资数据信息失败" . $sql);
        }
        return self::return_data(0, "查询主媒资数据信息成功", $result);
    }

    /**
     * 查询主媒资数据信息(guid查询)
     * @param object $dc 数据库对象
     * @param string $video_index_id 分集guid
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     * @date 2015-12-29
     */
    static public function get_video_info_by_import_id($dc, $video_import_id, $cp_id = 0)
    {
        if (empty($video_import_id))
        {
            return self::return_data(1, '主媒资注入id为空');
        }

        $sql = "select * from " . self::$base_table . " where nns_asset_import_id='{$video_import_id}' and nns_cp_id='{$cp_id}' and nns_deleted!=1 order by nns_modify_time desc limit 1";
        // 		echo $sql;die;
        $result = nl_query_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, "查询主媒资数据信息失败" . $sql);
        }
        return self::return_data(0, "查询主媒资数据信息成功", $result);
    }

    /**
     * 检查数据是否存在通过内容提供商和上游cms标示
     * @param unknown $dc
     * @param unknown $cp_id
     * @param unknown $import_source
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     * @date 2017-02-27
     */
    static public function check_cp_producer_exsist($dc, $cp_id, $import_source)
    {
        $sql = "select * from " . self::$base_table . " where nns_cp_id='{$cp_id}' and nns_import_source='{$import_source}' limit 1";
        $result = nl_query_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
        return self::return_data(0, 'OK', $result);
    }


    /**
     * 修改主媒资信息
     * @param object $dc 数据库对象
     * @param array $params 数据数组
     * @param array|string $nns_id GUID
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     * @date 2016-09-24
     */
    static public function edit($dc, $params, $nns_id)
    {
        if (isset($params['nns_id']))
        {
            unset($params['nns_id']);
        }
        $params = self::make_nns_pre($params);
        if (strlen($nns_id) < 0)
        {
            return self::return_data(1, 'CDN直播频道队列guid为空');
        }
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (empty($params))
        {
            return self::return_data(1, '参数为空');
        }
        if (!isset($params['nns_modify_time']) || strlen($params['nns_modify_time']) < 1)
        {
            $params['nns_modify_time'] = date("Y-m-d H:i:s");
        }
        $sql = self::make_update_sql(self::$base_table, $params, array('nns_id' => $nns_id));
        $result = nl_execute_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        return self::return_data(0, 'ok');
    }

    /**
     * 依据条件查询主媒资信息
     * @param $dc
     * @param $params
     * @return array
     */
    static public function query_by_condition($dc, $params)
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (!is_array($params) || empty($params))
        {
            return self::return_data(1, '查询条件为空不允许查询');
        }
        $sql = self::make_query_sql(self::$base_table, $params);
        $result = nl_query_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (isset($result) && !empty($result)) ? $result : null;
        return self::return_data(0, 'OK' . $sql, $result);
    }


    /**
     * 添加主媒资信息
     * @param object $dc 数据库对象
     * @param array $params 数据数组
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     * @date 2016-03-06
     */
    static public function add($dc, $params)
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (empty($params) || !is_array($params))
        {
            return self::return_data(1, '参数为空');
        }
        $params['nns_create_time'] = $params['nns_modify_time'] = date('Y-m-d H:i:s');
        $sql = self::make_insert_sql(self::$base_table, $params);
        $result = nl_execute_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        return self::return_data(0, 'ok:' . $sql);
    }

    /**
     * 查询主媒资扩展信息
     * @param $dc
     * @param $params
     * @return array
     */
    static public function del_ex_by_condition($dc, $params)
    {
        $params = self::make_nns_pre($params);
        if (empty($params) || !is_array($params))
        {
            return self::return_data(1, '参数为空');
        }
        $sql = self::make_delete_sql(self::$base_table_ex, $params);
        if (strlen($sql) < 1)
        {
            return self::return_data(1, '数据组装sql为空，不允许删除');
        }
        $result = nl_execute_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        return self::return_data(0, 'OK' . $sql);
    }

    /**
     * 添加主媒资扩展信息
     * @param object $dc 数据库对象
     * @param array $params 数据数组
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     * @date 2016-03-06
     */
    static public function add_ex($dc, $params)
    {
        $params = self::except_useless_params(self::$arr_filed_ex, $params);
        if (empty($params) || !is_array($params))
        {
            return self::return_data(1, '参数为空');
        }
        $sql = self::make_insert_sql(self::$base_table_ex, $params);
        $result = nl_execute_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        return self::return_data(0, 'ok:' . $sql);
    }
}