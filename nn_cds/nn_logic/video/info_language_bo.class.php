<?php
include_once 'vod_info_language.class.php';
include_once 'live_info_language.class.php';
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'playbill'.DIRECTORY_SEPARATOR.'playbill_item_info_language.class.php';
class nl_info_language_bo {
	

	/**
	 * @return TRUE 代表主语言
	 * 			FALSE 代表扩展语言
	 * 			NULL 代表参数无效
	 */
	static public function check_language_param($language){
	 	$lang_current=g_cms_config::get_g_config_value('g_current_language');
	 	$lang_extend=g_cms_config::get_g_extend_language();

	 	$language=empty($language)?$lang_current:$language;

	 	if ($language!=$lang_current && !strstr($lang_extend,$language)){
	 		return NULL;
	 	}

	 	if ($language==$lang_current) return TRUE;
	 	if (strstr($lang_extend,$language)) return FALSE;
	}
	
static public function get_playbill_item_by_language($dc,$video_info,$language,$playbill_id){
	
		$playbill_info_lang = nl_playbill_item_info_language::get_playbill_item_info_by_language(
		$dc,
		$playbill_id,
		$language);
	if (is_array($playbill_info_lang)){
			$video_info['nns_name']=$playbill_info_lang[0]['nns_name'];
			$video_info['nns_summary']=$playbill_info_lang[0]['nns_summary'];
		
	}else{
		$video_info['nns_name']='';
		$video_info['nns_summary']='';
		$video_info['nns_alias_name']='';
		$video_info['nns_area']='';
	
		$video_info['nns_director']='';
		$video_info['nns_actor']='';
		$video_info['nns_language']='';
		$video_info['nns_text_lang']='';
		$video_info['nns_producer']='';
		$video_info['nns_screenwriter']='';
		$video_info['nns_play_role']='';
	}
	return $video_info;
}
	
	//获取多语言信息 
static public function get_info_by_language($dc,$video_info,$language){
	if ($video_info["nns_video_type"] == 0){
		$video_info_lang = nl_vod_info_language::get_vod_info_by_language(
		$dc,
		$video_info["nns_video_id"],
		$language);
	}elseif($video_info["nns_video_type"] == 1){
		$video_info_lang = nl_live_info_language::get_live_info_by_language(
		$dc,
		$video_info["nns_video_id"],
		$language);
	}
	
	if (is_array($video_info_lang)){
		$video_info['nns_name']=$video_info_lang[0]['nns_name'];
		$video_info['nns_summary']=$video_info_lang[0]['nns_summary'];
		$video_info['nns_alias_name']=$video_info_lang[0]['nns_alias_name'];
		$video_info['nns_area']=$video_info_lang[0]['nns_area'];
		if ($video_info["nns_video_type"] == 0){
			$video_info['nns_director']=$video_info_lang[0]['nns_director'];
			$video_info['nns_actor']=$video_info_lang[0]['nns_actor'];
			$video_info['nns_language']=$video_info_lang[0]['nns_vod_language'];
			$video_info['nns_text_lang']=$video_info_lang[0]['nns_text_lang'];
			$video_info['nns_producer']=$video_info_lang[0]['nns_producer'];
			$video_info['nns_screenwriter']=$video_info_lang[0]['nns_screenwriter'];
			$video_info['nns_play_role']=$video_info_lang[0]['nns_play_role'];
		}elseif ($video_info["nns_video_type"] == 1){
			$video_info['nns_language']=$video_info_lang[0]['nns_live_language'];
		}
		
	}else{
		$video_info['nns_name']='';
		$video_info['nns_summary']='';
		$video_info['nns_alias_name']='';
		$video_info['nns_area']='';
	
		$video_info['nns_director']='';
		$video_info['nns_actor']='';
		$video_info['nns_language']='';
		$video_info['nns_text_lang']='';
		$video_info['nns_producer']='';
		$video_info['nns_screenwriter']='';
		$video_info['nns_play_role']='';
	}
	
	return $video_info;
	}
	
	//获取多语言分集信息 
static public function get_info_index_by_language($dc,$video_index,$language){
	if ($video_index[0]['nns_video_type'] == 0){
		$video_index_lang = nl_vod_index_info_language::get_vod_index_info_by_language(
		$dc,
		$video_index[0]["nns_video_id"],
		NULL,
		$language);
	}else{
		//		直播没有分集信息
		$video_index_lang=NULL;
	}

	
	$video_index_lang=np_array_rekey($video_index_lang,'nns_index');
	
	foreach ($video_index as $video_index_data){
		$language_data=$video_index_lang[$video_index_data['nns_index']];
		if (is_array($language_data)){
		$video_index_data['nns_name']=$language_data['nns_name'];
		$video_index_data['nns_summary']=$language_data['nns_summary'];
		
		}else{
			$video_index_data['nns_name']='';
			$video_index_data['nns_summary']='';
		}
	}
	
	
	
	return $video_index;
}
}
?>