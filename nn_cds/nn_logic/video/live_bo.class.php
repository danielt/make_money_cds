<?php
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
class nl_live_bo {
const CACHE_TIME_OUT=300;
/**根据直播频道ID获取该频道下所有直播类型
 * @param string 频道ID
 * @param string EPG输出标识
 * @return Array(
 * 					0=>array(
 * 						'video_id'=>视频ID，
 * 						'packet_id'=>媒资包ID，
 * 						'category_id'=>栏目ID
 * 					),
 * 					1=>array(
 * 						'video_id'=>视频ID，
 * 						'packet_id'=>媒资包ID，
 * 						'category_id'=>栏目ID
 * 					),
 * 			)
 * 
 */
   static public function epg_get_live_list_by_pid($dc,$pid,$type,$tag,$policy=NL_DC_AUTO){
   		$result = null;
   		if (empty($pid)) return TRUE;
		if ($policy == NL_DC_AUTO){
			if ($dc->is_cache_enabled()) {
				$result = self::_epg_get_live_list_by_pid_cache($dc->cache(),$pid,$type,$tag);
				if ($result === FALSE ) {
					$result = self::_epg_get_live_list_by_pid_db($dc->db(),$pid,$type,$tag);
					self::_epg_set_live_list_by_pid_cache($dc->cache(),$pid,$type,$tag,$result);
				}
			}
			else {
				$result = self::_epg_get_live_list_by_pid_db($dc->db(),$pid,$type,$tag);
			}
		}elseif($policy == NL_DC_DB){
			$result = self::_epg_get_live_list_by_pid_db($dc->db(),$pid,$type,$tag);
		}elseif($policy == NL_DC_CACHE){
			$result = self::_epg_get_live_list_by_pid_cache($dc->cache(),$pid,$type,$tag);
		}
		
		return $result;
   }
   
   static private function _epg_get_live_list_by_pid_cache($cache,$pid,$type,$tag){
   		if ($cache===NULL) return FALSE;
//		获取GUID标识
		$comp_key=$cache->get('index_live_pid|#'.$pid);
		
		return unserialize($cache->get($comp_key.'|#'.$type.'|#'.$tag));
   }
   
    static private function _epg_set_live_list_by_pid_cache($cache,$pid,$type,$tag,$result){
   		if ($cache===NULL) return FALSE;
		$comp_key=$cache->get('index_live_pid|#'.$pid);
		$comp_key=($comp_key===FALSE)?np_guid_rand():$comp_key;
		$cache->set('index_live_pid|#'.$pid,$comp_key);
		return $cache->set($comp_key.'|#'.$type.'|#'.$tag,serialize($result),self::CACHE_TIME_OUT);
   }
   
   static private function _epg_get_live_list_by_pid_db($db,$pid,$type,$tag){
		$live_ids=self::get_video_ids_by_pid($db,$pid);
		if ($live_ids===FALSE || $live_ids===TRUE) return $live_ids;
   		$live_ids_str='';
//   		var_dump( $live_ids); die;
   		foreach ($live_ids as $live_id){
   			$live_ids_str.='\''.$live_id['nns_live_id'].'\',';
   		}
   		$live_ids_str=rtrim($live_ids_str,',');
   			
//      SETP2 获取符合条件的LIVE数据
		$sql2 = ' select nns_video_id,nns_video_type,nns_assist_id,nns_category_id ' .
				' from nns_assists_item where ' .
					'(nns_tag=\'\' or ' .
					'ISNULL(nns_tag) or ' .
					'LOCATE(\','.$tag.',\',CONCAT(\',\',nns_tag))>0) and ' .
					'(nns_single_tag=\'\' or ' .
					'ISNULL(nns_single_tag) or ' .
					'LOCATE(\','.$tag.',\',CONCAT(\',\',nns_single_tag))>0) and ' .
					'nns_video_id in ('.$live_ids_str.') and ' .
					'nns_video_type=1 ';
					
		$live_active_list=nl_query_by_db($sql2,$db);
		if ($live_active_list===FALSE || $live_active_list===TRUE) return $live_active_list;
		$result=array();
		foreach ($live_active_list as $live_item){
			$live_item_result=array();
			$live_item_result['video_id']=$live_item['nns_video_id'];
			$live_item_result['packet_id']=$live_item['nns_assist_id'];
			$live_item_result['category_id']=$live_item['nns_category_id'];
			array_push($result,$live_item_result);
		}
		return $result;
   }
   
    static public function epg_delete_live_list_by_pid_cache($dc,$pid){
		return $dc->cache()->delete('index_live_pid|#'.$pid);
   }
   /**
    * 根据视频ID获取频道ID
    */
   static public function epg_get_pid_by_video_id($dc,$video_id){
   		$sql='select nns_value from nns_live_ex where nns_key=\'pid\' and nns_live_id=\''.$video_id.'\'';
   		$result=nl_query_by_db($sql,$dc->db());
   		if (is_array($result)){
   			return $result[0]['nns_value'];
   		}
   		return $result;
   }
   /**
    * 根据dvb获取频道ID
    */
   static public function epg_get_pid_by_dvb($dc,$dvb){
   		$sql='select nns_value from nns_live_ex where nns_key=\'pid\' and nns_live_id in ' .
   				'(select nns_live_id from nns_live_ex where nns_key=\'dvb\' and nns_value=\''.$dvb.'\')';
   		$result=nl_query_by_db($sql,$dc->db());
   		if (is_array($result)){
   			return $result[0]['nns_value'];
   		}
   		return $result;
   }
   
   static public function get_video_ids_by_pid($db,$pid,$type=NULL){
   	//   	SETP1 查询所有频道ID下的LIVE_ID
   		$sql1='select nns_live_id from nns_live_ex ' .
   			'where nns_key=\'pid\' and ' .
   			'nns_value=\''.$pid.'\' ' ;
            
   		if (!empty($type)){
   			$sql1.='and nns_live_id in (select nns_live_id as num from nns_live_ex where nns_key=\'live_type\' and binary nns_value like \'%'.$type.'%\' )';
   		}
   		$sql1.=' group by nns_live_id ';
   		
   		$live_ids=nl_query_by_db($sql1,$db);
   		return $live_ids;
   }
   
   static public function get_video_ids_by_name($db, $video_name){
   		$sql1='select nns_id from nns_live ' .
   			'where nns_name=\''.$video_name.'\'';
   		
   		$live_ids=nl_query_by_db($sql1,$db);
   		return $live_ids;
   }
   
   
   static public function get_live_ex_info($dc,$id,$policy=NL_DC_AUTO){
   		$result = null;
   		if (empty($id)) return TRUE;
		if ($policy == NL_DC_AUTO){
			if ($dc->is_cache_enabled()) {
				$result = self::_get_live_ex_info_by_cache($dc->cache(),$id);
				if ($result === FALSE ) {
					$result = self::_get_live_ex_info_by_db($dc->db(),$id);
					self::_set_live_ex_info_by_cache($dc->cache(),$id,$result);
				}
			}
			else {
				$result = self::_get_live_ex_info_by_db($dc->db(),$id);
			}
		}elseif($policy == NL_DC_DB){
			$result = self::_get_live_ex_info_by_db($dc->db(),$id);
		}elseif($policy == NL_DC_CACHE){
			$result = self::_get_live_ex_info_by_cache($dc->cache(),$id);
		}
		
		return $result;
   } 
   
   static private function _get_live_ex_info_by_db($db,$id){
   		$sql='select * from nns_live_ex where nns_live_id=\''.$id.'\'';
   		$live_ex_infos=nl_query_by_db($sql,$db);
   		
   		if (!is_array($live_ex_infos)) return $live_ex_infos;
   		
   		$result=array();
   		foreach ($live_ex_infos as $info){
   			$result[$info['nns_key']]=$info['nns_value'];
   		}
   		
   		return $result;
   }
   
   static private function _set_live_ex_info_by_cache($cache,$id,$result){
   		if ($cache===NULL) return FALSE;
		return $cache->set('live_ex|#'.$id,serialize($result),self::CACHE_TIME_OUT);
   }
   
   static private function _get_live_ex_info_by_cache($cache,$id){
   		if ($cache===NULL) return FALSE;
		return unserialize($cache->get('live_ex|#'.$id));
   }
   
   static public function delete_live_ex_info_by_cache($dc,$id){
   		if ($dc->cache()===NULL) return FALSE;
		return $dc->cache()->delete('live_ex|#'.$id);
   }
}
?>