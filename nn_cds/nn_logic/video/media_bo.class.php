<?php

class nl_media_bo {

        /**
         * 筛选符合条件的片源对终端输出
         * @param medias 片源组
         * @param tag类型
         * @param 指定片源ID
         * @param 终端网络类型 3g|wifi|lan
         * @param 支持类型
         * 			点播为 VOD
         * 			直播为 LIVE | TSTV
         * 
         */
        static public function get_active_media($medias, $media_id, $carry, $caps, $nns_mode = '') {
                if (count($medias) == 0)
                        return NULL;
//	var_dump($caps);die;
                //如果需要筛选影片清晰度
                if (!empty($nns_mode)) {
                        $mediaslist = array();
                        foreach ($medias as $list) {
//                        	对大小写判断进行兼容 BY S67 2013/7/16
                                if (strtolower($list['nns_mode']) == strtolower($nns_mode)) {
                                        $mediaslist [] = $list;
                                        break;
                                }
                        }

                        $medias = $mediaslist;
                }

                if (empty($media_id)) {
                        $active_medias = array();
                        foreach ($medias as $key => $media_item) {

                                if (strstr($media_item['nns_media_caps'], $caps) || empty($caps) || empty($media_item['nns_media_caps'])) {
                                        array_push($active_medias, $media_item);
                                }
                        }
                        /*                         * *****************  2013-04-26 Jacen edit **************** */
//		模式下 对影片进行筛选
//      2013-04-11 Jacen edit: call static function 'self::_usort_media_by_lan'
                        if (!empty($carry)) {
//			usort($active_medias,"self::_usort_media_by_".$carry);
                                usort($active_medias, array(self, '_usort_media_by_' . $carry));
                        } else {
//			usort($active_medias,"self::_usort_media_by_lan");
                                usort($active_medias, array(self, '_usort_media_by_lan'));
                        }
                        return $active_medias[0];
                } else {
                        foreach ($medias as $media_item) {
                                if ($media_item['nns_id'] == $media_id) {
                                        return $media_item;
                                }
                        }
                        return NULL;
                }
        }

        static public function _usort_media_by_3g($media1, $media2) {
                $active_arr = array(
                    'LOW', 'STD', 'HD'
                );
                return self::_usort_media_by_arr($media1, $media2, $active_arr);
        }

        static public function _usort_media_by_wifi($media1, $media2) {
                $active_arr = array(
                    'STD', 'HD', 'LOW'
                );
                return self::_usort_media_by_arr($media1, $media2, $active_arr);
        }

        static public function _usort_media_by_lan($media1, $media2) {
                $active_arr = array(
                    'STD', 'HD', 'LOW'
                );
                return self::_usort_media_by_arr($media1, $media2, $active_arr);
        }

        static public function _usort_media_by_arr($media1, $media2, $sort_arr) {
                $key1 = array_search(strtoupper($media1['nns_mode']), $sort_arr);
                $key2 = array_search(strtoupper($media2['nns_mode']), $sort_arr);
                if ($key1 <= $key2) {
                        return -1;
                } else {
                        return 1;
                }
        }

        // 判断EPG输出
        static public function get_media_by_tag($tag, $media_list) {
                if (!is_array($media_list))
                        return $media_list;

                $tag = "," . $tag . ",";
                $len = count($media_list);
                for ($i = $len - 1; $i >= 0; $i--) {
                        if (strstr(',' . $media_list[$i]["nns_tag"], $tag) == false && !empty($media_list[$i]["nns_tag"])) {
                                array_splice($media_list, $i, 1);
                        }
                }
                return $media_list;
        }

        static public function get_constant_state_reason($code) {
                $return_code = '';
                //	视频错误INI配置
                $media_error_ini = dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'nn_cms_config' . DIRECTORY_SEPARATOR . 'global_error_media_ini';
                if (file_exists($media_error_ini)) {
                        $error_params = parse_ini_file($media_error_ini, true);
                } else {
                        $error_params = NULL;
                }
//		var_dump($error_params);die;
                switch ($code) {
                        case 0 :
                                if (is_array($error_params)) {
                                        $return_code = $error_params['reason']['media_normal'];
                                }
                                else
                                        $return_code = '正常可以播放';
                                break;
                        case 1 :
                                if (is_array($error_params)) {
                                        $return_code = $error_params['reason']['error_media_author'];
                                }
                                else
                                        $return_code = '没有授权';
                                break;
                        case 2 :
                                if (is_array($error_params)) {
                                        $return_code = $error_params['reason']['error_media_empty'];
                                }
                                else
                                        $return_code = '数据异常';
                                break;
                        case 3 :
                                if (is_array($error_params)) {
                                        $return_code = $error_params['reason']['error_media_timeout'];
                                }
                                else
                                        $return_code = '内容未到可播放时间';
                                break;
                        case 4 :
                                if (is_array($error_params)) {
                                        $return_code = $error_params['reason']['error_media_nomoney'];
                                }
                                else
                                        $return_code = '余额不足';
                                break;
                        case 5 :
                                if (is_array($error_params)) {
                                        $return_code = $error_params['reason']['error_media_expired'];
                                }
                                else
                                        $return_code = '内容过期';
                                break;
                        case 6 :
                                if (is_array($error_params)) {
                                        $return_code = $error_params['reason']['error_media_record'];
                                }
                                else
                                        $return_code = '内容正在录制';
                                break;
                }

                return $return_code;
        }

        static public function get_media_state_url($code, $url) {
                $return_url = $url;
                //	视频错误INI配置
                $media_error_ini = dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'nn_cms_config' . DIRECTORY_SEPARATOR . 'global_error_media_ini';
                if (file_exists($media_error_ini)) {
                        $error_params = parse_ini_file($media_error_ini, true);
                        switch ($code) {
                                case 1 :
                                        if (is_array($error_params)) {
                                                $return_url = $error_params['url']['error_media_author'];
                                        }
                                        break;
                                case 2 :
                                        if (is_array($error_params)) {
                                                $return_url = $error_params['url']['error_media_empty'];
                                        }
                                        break;
                                case 3 :
                                        if (is_array($error_params)) {
                                                $return_url = $error_params['url']['error_media_timeout'];
                                        }
                                        break;
                                case 4 :
                                        if (is_array($error_params)) {
                                                $return_url = $error_params['url']['error_media_nomoney'];
                                        }
                                        break;
                        }
                }

                return $return_url;
        }

        /**
         * 根据media数据创建播放串
         * @param $media_data media数据
         * @param $video_name 视频名称
         * @param $params 必要参数
         * 		nns_video_type 视频类型 0点播 1直播
         * 		nns_media_assets_id 媒资包ID
         * 		nns_service_id 服务包ID
         * 		nns_user_id	用户ID
         * 		nns_device_id 设备ID
         * 		nns_category_id 包栏目ID
         * @param 	$product_fee 视频资费信息
         * 		nns_product_id 产品ID
         * 
         * @return array 片源URL数组
         */
        static public function create_play_url($dc, $media_data, $video_name, $params, $product_fee) {
//		global $g_web_play_url_by_302;
                $web_play_url_type = g_cms_config::get_g_config_value('g_web_play_url_by_302');
                $params['nns_video_name'] = $video_name;
                if ($web_play_url_type) {
                        $media_url_arr = self::create_play_url_2($media_data, $video_name, $params, $product_fee);
//		$media_url_arr_http=create_play_url_1($media_data,$video_name,$params,$product_fee);
                        $media_url_arr_http = self::create_play_url_3($dc, $media_data, $params, $product_fee);
                        $media_url_arr['http'] = $media_url_arr_http['http'];
                } else {
//		$media_url_arr=create_play_url_1($media_data,$video_name,$params,$product_fee);
                        $media_url_arr = self::create_play_url_3($dc, $media_data, $params, $product_fee);
                }
                return $media_url_arr;
        }

        static public function create_play_url_3($dc, $media_data, $params, $product_fee = NULL) {
//	global $g_core_npss_url;
//	global $g_webdir;

                $webdir = g_cms_config::get_g_config_value('g_webdir');
                include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'play_ticket' . DIRECTORY_SEPARATOR . 'play_ticket_bo.class.php';
                include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'npss' . DIRECTORY_SEPARATOR . 'npss.class.php';
//	$npss_url_arr = explode(";", $g_core_npss_url);

                $params['nns_media_id'] = $media_data['nns_id'];
                $params['nns_product_id'] = isset($product_fee['nns_product_id']) ? $product_fee['nns_product_id'] : null;
                /**
                 * 为票据系统添加信息
                 */
                $ticket_id = nl_play_ticket_bo::add_play_ticket($dc, $params);

                $ticket_id = 'V1' . $ticket_id;

                $active_npss_url = nl_npss::get_active_npss_address();

                if ($params["nns_video_type"] == 0) {
                        $func_str = "vod";
                } elseif ($params["nns_video_type"] == 1 || $params["nns_video_type"] == 2) {
                        $func_str = "live";
                }
                $video_name = urlencode($params['nns_video_name']);
                $media_extend_url = "&uid=" . $params["nns_user_id"] . "&cn=" . $video_name . "&did=" . $params["nns_device_id"];


                $media_extend_url .= "&cms=$ticket_id";

                $kbps_str = "&ck=" . $media_data["nns_kbps"];
                $video_type = $media_data["nns_filetype"];
                $media_ip_url = "nn" . $func_str . "httpip://" . $active_npss_url . "/nn_$func_str." . $video_type . "?id=" . $media_data["nns_content_id"] . $kbps_str . "&nst=iptv" . $media_extend_url;
                $n6a_url = urlencode($webdir . "nn_ipqam/nn_ipqam_n6a.php");
                $media_ipqam_url = "nn" . $func_str . "rtspipqam://" . $active_npss_url . "/nn_$func_str." . $video_type . "?id=" . $media_data["nns_content_id"] . $kbps_str . "&nst=ipqam" . $media_extend_url . "&n6a=" . $n6a_url;
                $media_http_url = "http://" . $active_npss_url . "/nn_$func_str." . $video_type . "?id=" . $media_data["nns_content_id"] . $kbps_str . "&nst=iptv" . $media_extend_url;

                $media_url_arr = array();
                $media_url_arr['ip'] = $media_ip_url;
                $media_url_arr['ipqam'] = $media_ipqam_url;
                $media_url_arr['http'] = $media_http_url;


                return $media_url_arr;
        }

        static public function create_play_url_2($media_data, $video_name, $params, $product_fee) {
                $webdir = g_cms_config::get_g_config_value('g_webdir');
                $media_url = $webdir . 'nn_cms_view/create_play_url.interface.php';
                $media_url.='?';
                $media_ex_url = '';
                foreach ($params as $url_key => $url_param) {
                        $media_ex_url.=$url_key . '=' . $url_param . '&';
                }
                $media_ex_url.='nns_video_name=' . urlencode($video_name);
                $media_ex_url = rtrim($media_ex_url, '&');

                $media_url.=$media_ex_url . '&nns_media_id=' . $media_data['nns_id'];

//	判断是预付费还是后付费
                if (is_array($product_fee)) {
                        $nns_gathering_mode = isset($product_fee['nns_gathering_mode']) ? $product_fee['nns_gathering_mode'] : null;
                        $media_url.='&nns_type_state=' . $nns_gathering_mode;
                } else {
                        if ($product_fee == NULL || $product_fee === TRUE || $product_fee === 'free') {
                                $media_url.='&nns_type_state=FREE';
                        }
                }
                $nns_import_id = isset($media_data['nns_import_id']) ? $media_data['nns_import_id'] : '';
                $nns_import_source = isset($media_data['nns_import_source']) ? $media_data['nns_import_source'] : '';
                $media_url.='&nns_other_id=' . $nns_import_id;
                $media_url.='&nns_other_source=' . $nns_import_source;
                $media_url.='&nns_filetype=' . $media_data['nns_filetype'];

                $media_url_arr = array();
                $media_url_arr['ip'] = $media_url . '&nns_url_type=ip';
                $media_url_arr['ipqam'] = $media_url . '&nns_url_type=ipqam';
//	$media_url_arr['gxcatv']=$media_url.'&nns_url_type=gxcatv';
//	var_dump($media_url_arr);die;		
                return $media_url_arr;
        }

        /**
         * 构建播放串
         * @param Array(
         * 			"nns_category_id"=>栏目ID
         * 			"nns_packet_id"=>包ID
         * 			"nns_packet_type"=>0为媒资包 1为服务包
         * 			"nns_video_id"=>影片ID
         * 			"nns_video_type"=>影片类型
         * 			"nns_video_index"=>影片分集数
         * 		)
         * @return String 播放串
         */
        static public function encode_play_data($params) {
                $nns_category_id = isset($params['nns_category_id']) ? $params['nns_category_id'] : '';
                $nns_packet_id = isset($params['nns_packet_id']) ? $params['nns_packet_id'] : '';
                $nns_packet_type = isset($params['nns_packet_type']) ? $params['nns_packet_type'] : 0;

                $return_str = $nns_packet_id . '|#' .
                        $nns_packet_type . '|#' .
                        $nns_category_id . '|#' .
                        $params['nns_video_id'] . '|#' .
                        $params['nns_video_type'] . '|#' .
                        $params['nns_video_index'];

                return np_authcode($return_str, 'ENCODE');
        }

        /**
         * 解构播放额外参数串
         * @return Array(
         * 			"nns_category_id"=>栏目ID
         * 			"nns_packet_id"=>包ID
         * 			"nns_packet_type"=>0为媒资包 1为服务包
         * 			"nns_video_id"=>影片ID
         * 			"nns_video_type"=>影片类型
         * 			"nns_video_index"=>影片分集数
         * 		)
         * @param String 播放串
         */
        static public function decode_play_data($play_data) {
                $play_data = np_authcode($play_data);
                $play_params = explode('|#', $play_data);

                $return_data = array();
                $return_data['nns_packet_id'] = $play_params[0];
                $return_data['nns_packet_type'] = $play_params[1];
                $return_data['nns_category_id'] = $play_params[2];
                $return_data['nns_video_id'] = $play_params[3];
                $return_data['nns_video_type'] = $play_params[4];
                $return_data['nns_video_index'] = $play_params[5];


                return $return_data;
        }

}

?>