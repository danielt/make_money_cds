<?php
/**
 * 异步反馈消息
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_notify_message extends nl_public
{
	static public $table = "nns_notify_message";
	/**
	 * 添加统一异步反馈
	 * @param object $dc
	 * @param array $params
	 */
	static public function add_notify_message($dc,$params)
	{
		if(empty($params) || !is_array($params))
		{
			return self::return_data(1,'参数错误');
		}
		$params['id'] = np_guid_rand();
		$params['create_time'] = date('Y-m-d H:i:s',time());
		$params['modify_time'] = $params['nns_create_time'];
		$result = nl_db_insert($dc->db(), self::$table, $params);
		if($result)
		{
			return self::return_data(0,'OK');
		}
		else
		{
            return self::return_data(1,'mysql execute failed');
		}
	}
	/**
	 * 修改反馈消息
	 * @param object $dc
	 * @param array $params
	 * @param array $where
	 */
	static public function modify_notify_message($dc,$params,$where)
	{
		if(empty($where) || !is_array($where) || empty($params) || !is_array($params))
		{
			return self::return_data(1,'参数错误');
		}
		$update_str = '';
		$where_str = '';
		foreach ($params as $key=>$value)
		{
			$update_str .= " {$key} = '{$value}',";
		}
		$update_str = rtrim($update_str,',');
		foreach ($where as $k=>$val)
		{
			if(!empty($val) || $val == '0')
			{
				$where_str .= " {$k} = '{$val}' AND ";
			}
		}
		$where_str = rtrim($where_str,' AND ');
		$sql = "update " . self::$table . " set " . $update_str . " where " . $where_str;
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	/**
	 * 获取反馈消息
	 * @param object $dc
	 * @param array $params
	 * @param string $since
	 * @param string $num
	 * @param boolean $is_count
	 */
	static public function get_nofity_message($dc,$params,$since=null,$num=null,$is_count=false)
	{
		$where = '';
		$str_limit = '';
		if(is_array($params))
		{
			foreach ($params as $key=>$value)
			{
				if($key === 'nns_notify')
				{
					$where .= " {$key} like '%{$value}%' AND ";
				}
				elseif(!empty($value) || $value == '0')
				{
					$where .= " {$key} = '{$value}' AND ";
				}
			}
		}
		if(!empty($where))
		{
			$where = rtrim($where," AND ");
			$where = " where " . $where;
		}
		if(strlen($since) > 0 && strlen($num) > 0)
		{
			$str_limit = " limit {$since},{$num}";
		}
		$sql = "select * from " . self::$table . $where . " order by nns_create_time desc " . $str_limit;
		$data = nl_query_by_db($sql, $dc->db());
		if($data === false)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		if($data === true)
		{
			$data = array();
		}
		$count_info = array();
		if($is_count)
		{
			$count_sql = "select count(1) as count from " . self::$table . $where;
			$count_info = nl_query_by_db($count_sql, $dc->db());
			if($count_info === false)
			{
				return self::return_data(1,'数据库执行失败'.$sql);
			}
		}
		return self::return_data(0,'ok',$data,$count_info);
	}
    /**
     * 删除反馈消息
     * @param object $dc
     * @param string $message_id
     */
    static public function delete_nofity_message($dc,$message_id)
    {
     if(empty($message_id)){
         return self::return_data(1,'参数错误');
     }else{
         $sql = "select * from " . self::$table . " where nns_id = '$message_id'";
         $data = nl_query_by_db($sql, $dc->db());
         if($data === false)
         {
             return self::return_data(1,'数据库执行失败'.$sql);
         }
         if(empty($data)){
             return self::return_data(1,'删除的数据不存在'.$sql);
         }
         $sql="delete from " . self::$table . " where nns_id='$message_id'";
         $result=nl_execute_by_db($sql,$dc->db());
         if($result===false){
             return self::return_data(1,'数据库执行失败'.$sql);
         }
         if($result===true){
             return self::return_data(0,'OK');
         }else{
             return self::return_data(1,'删除失败');
         }
     }
    }
}