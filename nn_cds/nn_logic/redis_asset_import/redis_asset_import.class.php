<?php
/**
 * redis存储注入数据管理
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_redis_asset_import extends nl_public
{
	static public $redis_table_name = 'nns_asset_import';
	static public $fields = array(
			'nns_index_import_id', // 分集注入id
			'nns_media_import_id', // 片源注入id
			'nns_message_id', //消息id
			'nns_state',  //注入状态  0 未注入资源库 | 1 已注入资源库 
			'nns_delete',  //数据是否删除  0 未删除 | 1 已删除
			'nns_cp_id', //CP_ID
			'nns_content', // 注入内容
			'nns_create_time', //创建时间
			'nns_modify_time' //修改时间
	);
	
	
	static public $indexs = array(
			'nns_index_import_id',
			'nns_media_import_id',
			'nns_cp_id',
			'nns_state',
			'nns_delete',
			'nns_message_id',
			'nns_create_time',
			'nns_modify_time'
	);
	/**
	 * 添加 redis存储的数据
	 * @param object $obj_redis redis对象
	 * @return Ambigous ('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-04-12
	 */
	static public function add($obj_redis,$params)
	{
		$params = self::make_void_params($params);
		$nns_guid = np_guid_rand();
		if(!isset($params['nns_cp_id']) || strlen($params['nns_cp_id']) < 1)
		{
			unset($obj_redis);
			return self::return_data(1,'cp_id为空');
		}
		if(!isset($params['nns_index_import_id']) || strlen($params['nns_index_import_id']) < 1)
		{
			unset($obj_redis);
			return self::return_data(1,'分集注入id为空');
		}
		if(!isset($params['nns_media_import_id']) || strlen($params['nns_media_import_id']) < 1)
		{
			unset($obj_redis);
			return self::return_data(1,'片源注入id为空');
		}
		$obj_redis->hash_table(self::$redis_table_name);
		$params['nns_modify_time'] = $params['nns_create_time'] = microtime(true);
		$result_add=$obj_redis->hash_update($nns_guid,$params);
		if(!$result_add)
		{
			unset($obj_redis);
			return self::return_data(1,'添加添加失败');
		}
		unset($obj_redis);
		return self::return_data(0,'添加成功');
	}
	
	/**
	 * 修改 redis存储的数据
	 * @param object $obj_redis redis对象
	 * @return Ambigous ('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-04-12
	 */
	static public function modify($obj_redis,$nns_id,$params)
	{
		$params = self::make_void_params($params);
		if(isset($params['nns_cp_id']))
		{
			unset($params['nns_cp_id']);
		}
		if(isset($params['nns_index_import_id']))
		{
			unset($params['nns_index_import_id']);
		}
		if(isset($params['nns_media_import_id']))
		{
			unset($params['nns_media_import_id']);
		}
		if(isset($params['nns_message_id']))
		{
			unset($params['nns_message_id']);
		}
		$obj_redis->hash_table(self::$redis_table_name);
		$params['nns_modify_time'] = microtime(true);
		$result_add=$obj_redis->hash_update($nns_id,$params);
		if(!$result_add)
		{
			unset($obj_redis);
			return self::return_data(1,'修改失败,guid:'.$nns_id.',参数：'.var_export($params,true));
		}
		unset($obj_redis);
		return self::return_data(0,'修改成功');
	}
	/**
	 * 查询redis数据
	 * @param object $obj_redis redis对象
	 * @param array $params 查询的参数数组
	 * @param array $arr_page 分页信息
	 * @author liangpan
	 * @date 2016-04-12
	 */
	static public function query($obj_redis,$params=null,$arr_page=null,$flag=true)
	{
		$obj_redis->hash_table(self::$redis_table_name);
		$temp_params = array();
		if(is_array($params) && !empty($params))
		{
			foreach ($params as $key=>$val)
			{
				if(strlen($val) > 0)
				{
					if($key == 'nns_id')
					{
						$temp_params['#id'] = $val;
					}
					else
					{
						$temp_params[$key] = $val;
					}
				}
			}
		}
		$obj_redis->hash_where($temp_params);
		$obj_redis->hash_orderby('nns_create_time','desc');
		return $obj_redis->hash_query('*',$arr_page,0,$flag);
	}
	
	/**
	 * 验证redis数据表是否存在   不存在则添加
	 * @param object $obj_redis redis对象
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据'), multitype:NULL number string , multitype:NULL multitype:a r y s t i n g  array >
	 * @author liangpan
	 * @date 2016-04-12
	 */
	static public function check_table_exesit($obj_redis)
	{
		$result = $obj_redis->hash_table_exists(self::$redis_table_name);
		if($result)
		{
			unset($obj_redis);
			return self::return_data(0,'redis表已经存在');
		}
		$result_create=$obj_redis->hash_create_table(self::$redis_table_name,self::$fields,self::$indexs);
		if(!$result_create)
		{
			unset($obj_redis);
			return self::return_data(1,'redis表创建失败');
		}
		unset($obj_redis);
		return self::return_data(0,'redis表创建成功');
	}
	
	static public function remake_table($obj_redis)
	{
		$result = $obj_redis->hash_table_exists(self::$redis_table_name);
		if($result)
		{
			$obj_redis->hash_table(self::$redis_table_name);
			$obj_redis->hash_destroy();
		}
		//die;
		$result_create=$obj_redis->hash_create_table(self::$redis_table_name,self::$fields,self::$indexs);
		if(!$result_create)
		{
			unset($obj_redis);
			return self::return_data(1,'redis表创建失败');
		}
		unset($obj_redis);
		return self::return_data(0,'redis表创建成功');
	}
	
	

	
	/**
	 * 生成需要存入redis的数据数组
	 * @param array $arr_params   参数数组
	 * @return multitype:Ambigous <string, unknown>
	 * @author liangpan
	 * @date 2016-04-12
	 */
	static public function make_void_params($arr_params)
	{
		$temp_array = array();
		foreach ($arr_params as $key=>$val)
		{
			if(in_array($key, self::$fields))
			{
				$temp_array[$key] = $val;
			}
		}
		unset($arr_params);
		return $temp_array;
	}
	
	
	/**
	 * 查询redis需要统计的总数
	 * @param object $obj_redis redis对象
	 * @param 上报类型 $type 0 开机上报 | 1 实时上报
	 * @author liangpan
	 * @date 2016-04-12
	 */
	static public function query_table_count($obj_redis,$array=null)
	{
		$obj_redis->hash_table(self::$redis_table_name);
		$obj_redis->hash_where($array);
		return $obj_redis->hash_count();
	}	
}