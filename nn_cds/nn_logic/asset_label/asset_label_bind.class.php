<?php

include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
include_once dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'nn_cms_db' .
        DIRECTORY_SEPARATOR . 'nns_common' . DIRECTORY_SEPARATOR . 'nns_db_guid_class.php';
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'label' . DIRECTORY_SEPARATOR . 'video_label_bo.class.php';

class nl_asset_label_bind {

        //缓存时间
        //static private $cache_time = 3600;
        const CACHE_TIME_OUT=300;
        /**
         * 根据标签ID获取标签类型信息
         * @param String $label_id 标签ID
         * @return 成功返回Array('id'=>, 'name'=>), 错误返回 false
         */
        static protected function get_label_type_by_id($label_id) {
                $label_type_config = nl_video_label_bo::$type_arr;

                $label_id = substr($label_id, 0, 4);
                if (!$label_id)
                        return false;

                if ($label_type_config && is_array($label_type_config)) {
                        foreach ($label_type_config as $v) {
                                if ($v['id'] == $label_id) {
                                        return $v;
                                }
                        }
                }
                return false;
        }

        /**
         * 根据媒资包id及标签ID获取标签信息
         * @param string $assest_id 媒资包id
         * @param String $label_id 标签ID
         * @param String $category_id 媒资包栏目ID
         * @return 成功返回Array, 错误返回 false
         */
        static function get_bind_label_by_assest_id_and_label_id($dc, $assest_id, $label_id, $category_id = null) {
                if (!self::get_label_type_by_id($label_id))
                        return false;

                if (!$category_id) {
                        $sql = 'select * from nns_assest_lable_bind where nns_assest_id=\'' . $assest_id . '\' and nns_label_id=\'' . $label_id . '\'';
                } else {
                        $sql = 'select * from nns_assest_lable_bind where nns_assest_id=\'' . $assest_id . '\' and nns_category_id=\'' . $category_id . '\' and nns_label_id=\'' . $label_id . '\'';
                }
                return nl_query_by_db($sql, $dc->db());
        }

        static function get_all_bind_info($dc, $page = 0, $page_size = 12) {
                $sql = 'select * from nns_assest_lable_bind limit ' . $page . ',' . $page_size;
                return nl_query_by_db($sql, $dc->db());
        }

        /**
         * 添加媒资包标签绑定
         * @param DB Object $dc
         * @param String $assest_id 媒资包id
         * @param String $label_id 标签id
         * @param String $label_name 标签名称
         * @param String $category_id 媒资包栏目ID
         * @return 失败返回false，成功返回主键 nns_id
         */
        static function add_assest_label_bind($dc, $assest_id, $label_id, $label_name, $category_id = null) {

                // 读取标签类型
                $label_info = self::get_label_type_by_id($label_id);

                if (!$label_info) {
                        return false;
                }
                // 检测是否已经绑定过了
                if (is_array(self::get_bind_label_by_assest_id_and_label_id($dc, $assest_id, $label_id, $category_id))) {
                        return false;
                }

                $label_type_id = $label_info['id'];
                $label_type_name = $label_info['name'];

                // 读取当前媒资包标签最大排序值

                $sql = 'select max(nns_order) as max_num from nns_assest_lable_bind where nns_assest_id=\'' . $assest_id . '\'
				and nns_category_id=\'' . $category_id . '\' and nns_type_id=\'' . $label_type_id . '\'';
                $query_result = nl_query_by_db($sql, $dc->db());
                $order = $query_result[0]['max_num'] + 1;

                $guid = new Guid();
                $nns_id = $guid->toString();
                $create_time = date("Y-m-d H:i:s", time());

                $sql = 'insert into nns_assest_lable_bind(nns_id,nns_assest_id,nns_category_id,nns_type_id,nns_type_name,nns_label_id,nns_label_name,nns_create_time,nns_order) values(\'' . $nns_id . '\', \'' . $assest_id . '\', \'' . $category_id . '\',
				\'' . $label_type_id . '\',	\'' . $label_type_name . '\', \'' . $label_id . '\', \'' . $label_name . '\',
				\'' . $create_time . '\', ' . $order . ')';
                $result = $query_result = nl_execute_by_db($sql, $dc->db());
		        if($result && $dc->is_cache_enabled())
		        {
		        	self::_set_cache_data_version($dc->cache());//更新缓存版本号,老版本的缓存将失效
		        } 
		        return $result;	                
        }

        static public function order_assests_label_bind($dc, $nns_id, $order, $info) {
                $result = null;

                switch ($order) {
                        case 'up':
                                $info['order'] = $info['order'] + 1;
                                $sql = "update nns_assest_lable_bind set nns_order='{$info['order']}',nns_modify_time=NOW() where nns_id='$nns_id' AND nns_assest_id='{$info['asset_id']}' AND nns_label_id='{$info['label_id']}' AND nns_category_id='{$info['category_id']}'";
                                $result = nl_execute_by_db($sql, $dc->db());
                                break;
                        case 'down':
                                $info['order'] = $info['order'] - 1;
                                $sql = "update nns_assest_lable_bind set nns_order='{$info['order']}',nns_modify_time=NOW() where nns_id='$nns_id' AND nns_assest_id='{$info['asset_id']}' AND nns_label_id='{$info['label_id']}' AND nns_category_id='{$info['category_id']}'";
                                $result = nl_execute_by_db($sql, $dc->db());
                                break;
                        case 'top':
                                $sql = "SELECT nns_order
FROM `nns_assest_lable_bind`
where nns_assest_id='{$info['asset_id']}' AND nns_category_id='{$info['category_id']}'
ORDER BY `nns_order` DESC
LIMIT 1";
                                $result = nl_query_by_db($sql, $dc->db());
                                if ($info['order'] == $result[0]['nns_order']) //如果已经是顶置或者置底就不变
                                        return true;
                                $info['order'] = isset($result[0]['nns_order']) ? $result[0]['nns_order'] + 1 : '0';
                                $sql = "update nns_assest_lable_bind set nns_order='{$info['order']}',nns_modify_time=NOW() where nns_id='$nns_id' AND nns_assest_id='{$info['asset_id']}' AND nns_label_id='{$info['label_id']}' AND nns_category_id='{$info['category_id']}'";
                                $result = nl_execute_by_db($sql, $dc->db());
                                break;
                        case 'bottom':
                                $sql = "SELECT nns_order
FROM `nns_assest_lable_bind`
where nns_assest_id='{$info['asset_id']}' AND nns_category_id='{$info['category_id']}'
ORDER BY `nns_order` ASC
LIMIT 1";

                                $result = nl_query_by_db($sql, $dc->db());
                                if ($info['order'] == $result[0]['nns_order']) //如果已经是顶置或者置底就不变
                                        return true;
                                $info['order'] = isset($result[0]['nns_order']) ? $result[0]['nns_order'] - 1 : '0';
                                $sql = "update nns_assest_lable_bind set nns_order='{$info['order']}',nns_modify_time=NOW() where nns_id='$nns_id' AND nns_assest_id='{$info['asset_id']}' AND nns_label_id='{$info['label_id']}' AND nns_category_id='{$info['category_id']}'";
                                $result = nl_execute_by_db($sql, $dc->db());
                                break;
                        case 'auto':
                                $sql = "update nns_assest_lable_bind set nns_order='{$info['order']}' ,nns_modify_time=NOW() where nns_id='$nns_id' AND nns_assest_id='{$info['asset_id']}' AND nns_label_id='{$info['label_id']}' AND nns_category_id='{$info['category_id']}'";
                                $result = nl_execute_by_db($sql, $dc->db());
                }

                //echo $sql;exit;
                return $result;
        }

        /**
         * 媒资包中指定标签上移一位
         * @param DB Object $dc
         * @param String $nns_id 主键id
         * @return 成功返回true，失败返回false
         */
        static function up_assests_label_bind($dc, $nns_id) {
                //如果id为空则返回false
                if (empty($nns_id)) {
                        return false;
                }

                // 查找绑定标签ID
                $sql = 'select * from nns_assest_lable_bind where nns_id=\'' . $nns_id . '\'';
                $query_result = nl_query_by_db($sql, $dc->db());
                if (!$query_result || !is_array($query_result)) {
                        return false;
                }
                // 记录当前节点排序值
                $cur_order = $query_result[0]['nns_order'];

                // 查询排列在当前节点前一个节点(nns_order值越大越靠前)
                $sql = 'select nns_id,nns_order from nns_assest_lable_bind where nns_id!=\'' . $nns_id . '\'
				and nns_assest_id=\'' . $query_result[0]['nns_assest_id'] . '\'
				and nns_category_id=\'' . $query_result[0]['nns_category_id'] . '\'
				and nns_type_id=\'' . $query_result[0]['nns_type_id'] . '\' and nns_order>' . $cur_order . '
				order by nns_order ASC limit 0,1';

                $query_result = nl_query_by_db($sql, $dc->db());
                if (!$query_result) { // 没有找到当前已是最靠前节点(only one)
                        return false;
                } else {
                        $order = $query_result[0]['nns_order'];
                        $prev_order = $cur_order;
                        $prev_nns_id = $query_result[0]['nns_id'];

                        //nl_execute_by_db('set autcommit 0', $dc->db());
                        try {
                                // 更新当前节点排序值
                                $sql = 'update nns_assest_lable_bind set nns_order=' . $order . ' where nns_id=\'' . $nns_id . '\'';
                                nl_execute_by_db($sql, $dc->db());
                                $sql = 'update nns_assest_lable_bind set nns_order=' . $prev_order . ' where nns_id=\'' . $prev_nns_id . '\'';
                                nl_execute_by_db($sql, $dc->db());

                                //nl_execute_by_db('commit', $dc->db());
                                return true;
                        } catch (Exception $e) {
                                //nl_execute_by_db('rollback', $dc->db());
                                echo 'failed';
                        }
                        return false;
                }
                return false;
        }

        /**
         * 媒资包中指定标签下移一位
         * @param DB Object $dc
         * @param String $nns_id 主键id
         * @return 成功返回true，失败返回false
         */
        static function down_assests_label_bind($dc, $nns_id) {
                //如果id为空则返回false
                if (empty($nns_id)) {
                        return false;
                }

                // 查找绑定标签ID
                $sql = 'select * from nns_assest_lable_bind where nns_id=\'' . $nns_id . '\'';
                $query_result = nl_query_by_db($sql, $dc->db());
                if (!$query_result || !is_array($query_result)) {
                        return false;
                }

                // 记录当前节点排序值
                $cur_order = $query_result[0]['nns_order'];

                // 查询排列在当前节点前一个节点(nns_order值越大越靠前)
                $sql = 'select nns_id,nns_order from nns_assest_lable_bind where nns_id!=\'' . $nns_id . '\'
				and nns_assest_id=\'' . $query_result[0]['nns_assest_id'] . '\'
				and nns_category_id=\'' . $query_result[0]['nns_category_id'] . '\'
				and nns_type_id=\'' . $query_result[0]['nns_type_id'] . '\' and nns_order<' . $cur_order . '
				order by nns_order DESC limit 0,1';
                $query_result = nl_query_by_db($sql, $dc->db());
                if (!$query_result || !is_array($query_result)) { // 没有找到当前已是最靠前节点(only one)
                        return true;
                } else {
                        $order = $query_result[0]['nns_order'];
                        $prev_order = $cur_order;
                        $prev_nns_id = $query_result[0]['nns_id'];

                        nl_execute_by_db('set autcommit 0', $dc->db());
                        try {
                                // 更新当前节点排序值
                                $sql = 'update nns_assest_lable_bind set nns_order=' . $order . ' where nns_id=\'' . $nns_id . '\'';
                                nl_execute_by_db($sql, $dc->db());
                                $sql = 'update nns_assest_lable_bind set nns_order=' . $prev_order . ' where nns_id=\'' . $prev_nns_id . '\'';
                                nl_execute_by_db($sql, $dc->db());

                                nl_execute_by_db('commit', $dc->db());
                                return true;
                        } catch (Exception $e) {
                                nl_execute_by_db('rollback', $dc->db());
                        }
                        return false;
                }
                return false;
        }

        /**
         * 修改标签绑定信息
         * @param DB Object $dc
         * @param String $nns_id 主键id
         * @param String $nns_name 标签名称
         * @param String $category_id 媒资包栏目ID
         * @return 成功返回true，失败返回false
         */
        static function update_assests_label_bind($dc, $nns_id, $nns_name, $category_id) {
                //如果id为空则返回false
                if (empty($nns_id)) {
                        return false;
                }

                // 查找绑定标签ID
                $sql = 'select nns_id, nns_assest_id,nns_type_id from nns_assest_lable_bind where nns_id=\'' . $nns_id . '\'
		        and nns_category_id=\'' . $category_id . '\'';
                $query_result = nl_query_by_db($sql, $dc->db());
                if (!$query_result || !is_array($query_result)) {
                        return false;
                }

                $sql = 'update nns_assest_lable_bind set nns_label_name=\'' . $nns_name . '\' where nns_id=\'' . $nns_id . '\'';
                return nl_execute_by_db($sql, $dc->db());
        }

        /**
         * 根据主键id删除绑定关系
         * @param DB Object $dc
         * @param String $nns_id 主键id
         * @return 成功返回true，失败返回false
         */
        static function del_assest_label_bind_by_id($dc, $nns_id) {
                //如果id为空则返回false
                if (empty($nns_id)) {
                        return false;
                }

                // 查找绑定标签ID
                $sql = 'select nns_id, nns_assest_id,nns_type_id from nns_assest_lable_bind where nns_id=\'' . $nns_id . '\'';
                $query_result = nl_query_by_db($sql, $dc->db());
                if (!$query_result || !is_array($query_result)) {
                        return false;
                }

                $sql = 'delete from nns_assest_lable_bind where nns_id=\'' . $nns_id . '\'';
                $result = nl_execute_by_db($sql, $dc->db());
		        if($result && $dc->is_cache_enabled())
		        {
		        	self::_set_cache_data_version($dc->cache());//更新缓存版本号,老版本的缓存将失效
		        } 
		        return $result;                
        }

        /**
         * 删除整个媒资包中某一栏目的绑定关系
         * @param DB Object $dc
         * @param String $nns_assest_id 媒资包id
         * @param String $category_id 媒资包栏目ID
         * @return 成功返回true，失败返回false
         */
        static function del_assest_label_bind_by_assest($dc, $nns_assest_id, $category_id) {
                //如果id为空则返回false
                if (empty($nns_assest_id)) {
                        return false;
                }

                // 查找绑定标签ID
                $sql = 'select nns_id, nns_assest_id,nns_type_id from nns_assest_lable_bind
				where nns_assest_id=\'' . $nns_assest_id . '\' and nns_category_id=\'' . $category_id . '\'';
                $query_result = nl_query_by_db($sql, $dc->db());
                if (!$query_result || !is_array($query_result)) {
                        return false;
                }

                $sql = 'delete from nns_assest_lable_bind where nns_assest_id=\'' . $nns_assest_id . '\' and nns_category_id=\'' . $category_id . '\'';
                $result = nl_execute_by_db($sql, $dc->db());
		        if($result && $dc->is_cache_enabled())
		        {
		        	self::_set_cache_data_version($dc->cache());//更新缓存版本号,老版本的缓存将失效
		        } 
		        return $result;                 
        }

        /**
         * 根据媒资包及栏目id和标签类型id获取标签
         * @param DB Object $dc
         * @param String $nns_assest_id 媒资包id
         * @param String $category_id 媒资包栏目ID
         * @param String $type_id 类型id
         * @param int $page 第几页, 默认为0
         * @param int $pageSize 每页读取数据条数,默认为20
         * @param String $policy 读取策略 NL_DC_AUTO | NL_DC_DB | NL_DC_CACHE 默认为NL_DC_AUTO
         * @return Array 如果查询错误返回false，结果为空返回true，结果不为空返回二维数组，数组结构为
          Array(
          　　　　	[0]=>Array(
          'nns_type_name' => 类型名称,
          　　　　	　		'nns_id' => 主键,
          　　　　	　		'nns_label_id' => 标签id,
          'nns_label_name'=>标签名称,
          ),
          ),
          )
         */
        static function get_label_by_assest_type_id($dc, $nns_assest_id, $category_id, $type_id, $page = 0, $page_size = 20, $order = "default", $policy = NL_DC_AUTO) {
                $page = intval($page);
                $page_size = intval($page_size);
                if (empty($nns_assest_id) || empty($type_id) || $page < 0 || $page_size < 0 || !in_array($policy, array(NL_DC_AUTO, NL_DC_CACHE, NL_DC_DB))) {
                        return false;
                }

                switch ($order) {
                        case "create_time":$order = "bind.nns_create_time DESC";
                                break;
                        case "times":$order = "bind.nns_order DESC";
                                break;
                        case "name":$order = "bind.nns_label_name DESC";
                                break;
                        case "default":$order = "bind.nns_order DESC";
                                break;
                }
                $sql = 'select bind.nns_id,bind.nns_type_name,bind.nns_label_id,bind.nns_label_name
				from nns_video_label as label,nns_assest_lable_bind as bind where
				bind.nns_assest_id=\'' . $nns_assest_id . '\' and bind.nns_type_id=\'' . $type_id . '\'
				and bind.nns_category_id=\'' . $category_id . '\'
				and bind.nns_label_id=label.nns_id and label.nns_state="1" order by ' . $order . ' limit ' . $page . ',' . $page_size;
                return nl_query_by_db($sql, $dc->db());
        }

        /**
         * 通过媒资包及栏目id获取标签
         * @param DB Object $dc
         * @param String $nns_assest_id 媒资包id
         * @param String $category_id 媒资包栏目ID
         * @param int $page 第几页, 默认为0
         * @param int $pageSize 每页读取数据条数,默认为20
         * @param String $policy 读取策略 NL_DC_AUTO | NL_DC_DB | NL_DC_CACHE 默认为NL_DC_AUTO
         * @return Array 如果查询错误返回false，结果为空返回true，结果不为空返回二维数组，数组结构为
          Array(
          　　　　	[0]=>Array(
          'nns_type_name' => 类型名称,
          　　　　	　		'nns_id' => 主键,
          　　　　	　		'nns_label_id' => 标签id,
          'nns_label_name'=>标签名称,
          /////// 2013-3-2 Jacen add ////////////
          'nns_create_time'=>创建时间,
          'nns_order'=>排序数字
          ),
          ),
          )
         */
        static function get_label_by_assest($dc, $nns_assest_id, $category_id, $page = 0, $page_size = 20, $search_params = array(), $policy = NL_DC_AUTO) {
                $page = intval($page);
                $page_size = intval($page_size);
                if (empty($nns_assest_id) || $page < 0 || $page_size < 0 || !in_array($policy, array(NL_DC_AUTO, NL_DC_CACHE, NL_DC_DB))) {
                        return false;
                }
                $search_where = '';
                if (!empty($search_params)) {

                        foreach ($search_params as $key => $val) {
                                if ($key == 'name') {
                                        $search_where .= "  and nns_label_name like'%$val%'";
                                }
                                if ($key == 'type') {
                                        $search_where .= "  and nns_type_id like'%$val%'";
                                }
                        }
                }

                $sql = 'select nns_id,nns_type_name,nns_label_id,nns_label_name,nns_create_time,nns_order from nns_assest_lable_bind
				where nns_assest_id=\'' . $nns_assest_id . '\' and nns_category_id=\'' . $category_id . '\'' . $search_where . '
				order by nns_order DESC,nns_modify_time DESC limit ' . $page . ',' . $page_size;
                return nl_query_by_db($sql, $dc->db());
        }

        static function epg_get_label_by_assest($dc, $nns_assest_id, $category_id, $nns_video_label_type_id = null, $since = null, $page_size = null, $policy = NL_DC_AUTO) {

                if ($policy == NL_DC_AUTO) {
                	if ($dc->is_cache_enabled()) {
                        $cache_key = "label|#" . self::_get_cache_data_version($dc->cache()).$nns_assest_id . $category_id . $nns_video_label_type_id . $since . $page_size;
                        $data = $dc->cache()->get($cache_key);
                        if ($data === false) {
                                $data = self::epg_get_label_by_assest_db($dc, $nns_assest_id, $category_id, $nns_video_label_type_id, $since, $page_size);
                                $dc->cache()->set($cache_key, $data, self::CACHE_TIME_OUT);
                        }else{
                        	$data = $data;
                        }
                	}else{
                		$data = self::epg_get_label_by_assest_db($dc, $nns_assest_id, $category_id, $nns_video_label_type_id, $since, $page_size);
                	}
                    return $data;
                } else if ($policy == NL_DC_DB) {
                        return self::epg_get_label_by_assest_db($dc, $nns_assest_id, $category_id, $nns_video_label_type_id, $since, $page_size);
                }else if($policy == NL_DC_CACHE){
                	$cache_key = "label|#" . self::_get_cache_data_version($dc->cache()).$nns_assest_id . $category_id . $nns_video_label_type_id . $since . $page_size;
                	$data = $dc->cache()->get($cache_key);
                	return $data;
                }
        }

        static function epg_get_label_by_assest_db($dc, $nns_assest_id, $category_id, $nns_video_label_type_id = null, $since = null, $page_size = null) {
                $sql = "select l.* from nns_video_label l,nns_assest_lable_bind lb where l.nns_id=lb.nns_label_id and
				lb.nns_assest_id='$nns_assest_id' and lb.nns_category_id='$category_id' ";
                if ($nns_video_label_type_id)
                        $sql.= "and l.nns_type='$nns_video_label_type_id'";

                if ($since && $page_size)
                        $sql .= " limit $since,$page_size";

                $reuslt = nl_query_by_db($sql, $dc->db());

                return $reuslt;
        }

        static function epg_get_label_by_assest_cache($cache, $nns_assest_id, $nns_video_label_type_id = null, $since = null, $page_size = null) {
                //
        }

        static function epg_get_label_by_assest_count($dc, $nns_assest_id, $category_id, $nns_video_label_type_id = null, $policy = NL_DC_AUTO) {
                $sql = "select count(*) as sum from nns_video_label l,nns_assest_lable_bind lb where l.nns_id=lb.nns_label_id
				and lb.nns_category_id='$category_id' and lb.nns_assest_id='$nns_assest_id' ";
                if ($nns_video_label_type_id)
                        $sql.= "and l.nns_type='$nns_video_label_type_id'";
                $data = nl_db_get_col($sql, $dc->db());
                return $data ? $data : 0;
        }

        /**
         * 通过媒资包及栏目id或者含有标签类型id获取标签总数
         * @param DB Object $dc
         * @param String $nns_assest_id 媒资包id
         * @param String $category_id 媒资包栏目ID
         * @param String $type_id 类型id
         * @param String $policy 读取策略 NL_DC_AUTO | NL_DC_DB | NL_DC_CACHE 默认为NL_DC_AUTO
         * @return 如果查询错误返回false，成功返回int，如果没有返回0
         */
        static function get_count_labels_by_assest_type_id($dc, $nns_assest_id, $category_id, $type_id = false, $policy = NL_DC_AUTO) {

                if (empty($nns_assest_id) || !in_array($policy, array(NL_DC_AUTO, NL_DC_CACHE, NL_DC_DB))) {
                        return false;
                }
                if ($type_id === false) {
                        return self::get_count_types_by_assest($dc, $nns_assest_id, $category_id, $policy);
                }
                $sql = 'select count(*) as num from nns_assest_lable_bind as bind,nns_video_label as label where
				bind.nns_assest_id=\'' . $nns_assest_id . '\' and bind.nns_type_id=\'' . $type_id . '\' and label.nns_state="1"
				and bind.nns_category_id=\'' . $category_id . '\' and label.nns_id=bind.nns_label_id';

                $query_result = nl_query_by_db($sql, $dc->db());
                if (!$query_result)
                        return false;
                return $query_result[0]['num'];
        }

        /**
         * 通过媒资包及栏目id获取绑定的标签类型总数
         * @param DB Object $dc
         * @param String $nns_assest_id 媒资包id
         * @param String $category_id 媒资包栏目ID
         * @param String $policy 读取策略 NL_DC_AUTO | NL_DC_DB | NL_DC_CACHE 默认为NL_DC_AUTO
         * @return 如果查询错误返回false，成功返回int，如果没有返回0
         */
        static function get_count_types_by_assest($dc, $nns_assest_id, $category_id, $search_params = array(), $policy = NL_DC_AUTO) {

                if (empty($nns_assest_id) || !in_array($policy, array(NL_DC_AUTO, NL_DC_CACHE, NL_DC_DB))) {
                        return false;
                }
                $sql = 'select count(*) as num from nns_assest_lable_bind as bind,nns_video_label as label where
				bind.nns_assest_id=\'' . $nns_assest_id . '\' and label.nns_id=bind.nns_label_id and label.nns_state="1"
				and bind.nns_category_id=\'' . $category_id . '\'';

                $query_result = nl_query_by_db($sql, $dc->db());
                if (!$query_result)
                        return false;
                return $query_result[0]['num'];
        }

        /**
         * 根据媒资包获取标签类型信息
         * @param obj $dc
         * @param int $sina
         * @param int $num
         * @param unknown_type $policy
         */
        static function get_label_type_by_assest($dc, $nns_assest_id, $sina = 0, $num = 20, $policy = NL_DC_AUTO, $category_id = 1000) {
                $sql = "select * from nns_assest_lable_bind where nns_assest_id='{$nns_assest_id}' group by nns_type_id limit {$sina},{$num}";
                return nl_query_by_db($sql, $dc->db());
        }

        static function get_assest_label_count($dc, $category_id_array, $assest_id) {
                foreach ($category_id_array as $key => $val) {
                        if (empty($val)) {
                                unset($category_id_array[$key]);
                        }
                }
                $category_id = "'" . implode("','", $category_id_array) . "'";
                //$category_id = "'".str_replace(",","','",$category_id)."'";
                $sql = "select nns_category_id as category_id,count(1) as num from nns_assest_lable_bind where nns_assest_id='$assest_id' and nns_category_id in({$category_id}) group by nns_category_id";
                $result = nl_query_by_db($sql, $dc->db());
                //print_r($result);
                //echo $sql;exit;
                return $result;
        }
	private static function _get_cache_data_version($cache){
		$v = $cache->get('asset_label_bind_cache_version');
		if (!$v) {
			$v = self::_set_cache_data_version($cache);
		}
		return $v;
	}
	private static function _set_cache_data_version($cache){
		$guid = np_guid_rand();
		return $cache->set('asset_label_bind_cache_version',$guid,self::CACHE_TIME_OUT*2);
	} 
}