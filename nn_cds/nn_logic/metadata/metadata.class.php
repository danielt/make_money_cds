<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-3-12
 * Time: 下午2:29
 * To change this template use File | Settings | File Templates.
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
class nl_metadata
{
    const CACHE_TIME_OUT = 300;
    static function get_metadata_list($dc,$id,$type,$policy = NL_DC_AUTO)
    {
        $result = null;
        if ($policy == NL_DC_AUTO)
        {
            if ($dc->is_cache_enabled())
            {
                $result = self::_get_metadata_list_by_cache($dc->cache(),$id,$type);
                if ($result === FALSE )
                {
                    $result = self::_get_metadata_list_by_db($dc->db(),$id,$type);
                    self::_set_metadata_list_by_cache($dc->cache(),$id,$type,$result);
                }
            }
            else
            {
                $result = self::_get_metadata_list_by_db($dc->db(),$id,$type);
            }
        }
        elseif($policy == NL_DC_DB)
        {
            $result = self::_get_metadata_list_by_db($dc->db(),$id,$type);
        }
        elseif($policy == NL_DC_CACHE)
        {
            $result = self::_get_metadata_list_by_cache($dc->cache(),$id,$type);
        }

        return $result;
    }

    static function _get_metadata_list_by_db($db,$id,$type){
        $str_sql = "select * from nns_metadata where nns_metadata_id='$id' and nns_metadata_type='$type' order by nns_create_time asc";
        return nl_query_by_db ($str_sql,$db);
    }

    static function _get_metadata_list_by_cache($cache,$id,$type)
    {
        if ($cache===NULL) return FALSE;
        return unserialize($cache->get('metadata_list|#'.$id.$type));
    }

    static function _set_metadata_list_by_cache($cache,$id,$type,$data)
    {
        if ($cache===NULL) return FALSE;
        return $cache->set('metadata_list|#'.$id.$type,serialize($data),self::CACHE_TIME_OUT);
    }
}