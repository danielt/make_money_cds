<?php
/**
 * @author guanghu.zhou
 * @refactor date 2013-04-16
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_carrier extends nl_public
{
	static private $cache_time = 3600;
	static private $prefix = "carrier_logic#|#";
	
	static public $base_table = 'nns_carrier';
	
	static $arr_filed = array(
	    'nns_id',
	    'nns_name',
	    'nns_desc',
	    'nns_state',
	    'nns_create_time',
	    'nns_modify_time',
	    'nns_contact',
	    'nns_telephone',
	    'nns_email',
	    'nns_addr',
	);
	
	/**
	 * @subpackage 运营商列表
	 * @param param 	array
	 * @return array
	 * @access public 
	 * @param dc  obj
	 * @notice 原函数名称 nns_db_carrier_list
	 */

	static public function get_carrier_list($dc,$policy,$param) {
		
		/*
		 $param = array (
			'name'=>,
			'min'=>,
			'num'=>,
			)
		 */
		
		
		$key = implode('-', $param);
		switch ($policy) {
			case NL_DC_DB :
				$result = self::_get_carrier_list_by_db($dc -> db(), $param);
				break;
			case NL_DC_CACHE :
				$result = self::_get_cache($dc -> cache(), $key);
				break;
			case NL_DC_AUTO :
				if ($dc -> is_cache_enabled()) {
					$result = self::_get_cache($dc -> cache(), $key);
					if ($result === FALSE) {
						$result = self::_get_carrier_list_by_db($dc -> db(), $param);
						self::_set_cache($dc -> cache(), $key, $result);
					}
				} else {
					$result = self::_get_carrier_list_by_db($dc -> db(), $param);
				}
				break;
		}
		return $result;
		
	}
	
	
	/**
	 * @subpackage 运营商列表
	 * @param param 	array
	 * @return array
	 * @access public 
	 * @param dc  obj
	 */
	
	static private function _get_carrier_list_by_db($db,$param)
	{
		extract($param);
		$str_sql = "select * from nns_carrier ";
		if (isset ( $name ) && ! empty ( $name )) {			
			$str_sql .= " where nns_name like '%$name%' ";
		}
		if (isset ( $min ) && isset ( $num )) {
			if (! empty ( $num )) {
				$min = ( int ) $min;
				$num = ( int ) $num;
				$str_sql .= " limit $min,$num";
			}
		}		
		$data = nl_db_get_all($str_sql,$db);
		return $result = array(
			'ret'=>0,
			'data'=>$data,
			'msg'=>null,
		);
	}
	
	
	/**
	 * 获取cache
	 */
	static private function _get_cache($cache, $key) {
		return $cache -> get(self::$prefix . $key);
	}

	/**
	 * 设置cache
	 */
	static private function _set_cache($cache, $key, $data) {
		$cache -> set(self::$prefix . $key, $data, self::$cache_time);
	}
	static public function get_carrier_info($dc){
		$sql  = "select * from nns_carrier";
		return  nl_db_get_one($sql,$dc->db());
	}
	
	/**
	 * 查询所有
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_all($dc)
	{
	    $sql="select * from " . self::$base_table . " order by nns_create_time asc";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    return self::return_data(0,'OK'.$sql,$result);
	}
}
