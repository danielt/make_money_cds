<?php
/**
 * 媒资上线管理
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_asset_online extends nl_public
{
	static $fields = array(
			'nns_id',
			'nns_message_id',
			'nns_video_name',
			'nns_import_id',
			'nns_category_id',
			'nns_order',
			'nns_action',
			'nns_cp_id',
			'nns_org_id',
			'nns_status',
			'nns_audit',
			'nns_create_time',
			'nns_modify_time',
			'nns_fail_time',
			'nns_delete',
			'nns_vod_id',
			'nns_type',
			'nns_asset_id',
	);
	static $base_table='nns_import_asset_online';
	
	

	/**
	 * 查询列表
	 * @param object $dc 数据库对象
	 * @param array $params 查询参数
	 * @param array $page_info 分页信息
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-11-26
	 */
	static public function query($dc,$params=null,$page_info=null)
	{
		if(isset($params['where']) && !empty($params['where']))
		{
			foreach ($params['where'] as $where_key=>$where_val)
			{
				if($where_key == 'create_begin_time')
				{
					self::$str_where.=" nns_create_time >='{$where_val}' and ";
				}
				else if($where_key == 'create_end_time')
				{
					self::$str_where.=" nns_create_time <='{$where_val}' and ";
				}
				else
				{
					self::$str_where.=" {$where_key} ='{$where_val}' and ";
				}
			}	
		}
		if(isset($params['like']) && !empty($params['like']))
		{
			foreach ($params['like'] as $like_key=>$like_val)
			{
				self::$str_where.=" {$like_key} like '%{$like_val}%' and ";
			}
		}
		self::$str_where = (strlen(self::$str_where) > 5) ? trim(self::$str_where,'and ') : '';
		$str_limit = self::make_page_limit($page_info);
		$sql="select * from " . self::$base_table . " " . self::$str_where . " order by nns_delete desc,nns_fail_time desc,nns_create_time asc {$str_limit} ";
		$sql_count = "select count(*) as count from " . self::$base_table . " " . self::$str_where;
// 		echo $sql;
		$result_count = nl_query_by_db($sql_count,  $dc->db());
		$result = nl_query_by_db($sql, $dc->db());
		self::init_where();
		if(!$result || !$result_count)
		{
			return self::return_data(1,"查询数据失败");
		}
		return self::return_data(0,"查询数据成功",$result,$result_count);
	}
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-11-26
	 */
	static public function query_by_id($dc,$nns_id)
	{
		if(empty($nns_id))
		{
			return self::return_data(1,'id为空');
		}
		$sql="select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,"查询数据失败".$sql);
		}
		return self::return_data(0,"查询数据成功",$result);
	}

    /**
     * 查询多个列表
     * @param object $dc 数据库对象
     * @param array $nns_id guid
     * @return array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     * @date 2015-11-26
     */
    static public function query_by_ids($dc,$nns_id)
    {
        if(empty($nns_id) || !is_array($nns_id) || count($nns_id) < 1)
        {
            return self::return_data(1,'id为空或者不是数组');
        }
        $str_id = implode("','", $nns_id);

        $sql="select * from " . self::$base_table . " where nns_id in ('{$str_id}') limit 1";
        $result = nl_query_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,"查询数据失败".$sql);
        }
        return self::return_data(0,"查询数据成功",$result);
    }
	/**
	 * 添加
	 * @param object $dc 数据库对象
	 * @param array $params 插入参数
	 * @return array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-11-26
	 */
	static public function add($dc,$params)
	{
		$params = self::except_useless_params(self::$fields, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$params['nns_id'] = np_guid_rand();
		$params['nns_create_time'] = $params['nns_modify_time'] = date("Y-m-d H:i:s");
		$sql = self::make_insert_sql(self::$base_table, $params);
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 修改
	 * @param object $dc 数据库对象
	 * @param array $params 插入参数
	 * @param array $where_params 插入参数条件
	 * @return array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-11-26
	 */
	static public function update($dc,$params,$where_params,$other_params=null)
	{
		$params = self::except_useless_params(self::$fields, $params,$where_params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$params['nns_modify_time'] = date("Y-m-d H:i:s");
		$sql = self::make_update_sql(self::$base_table, $params,$where_params,$other_params);
// 		echo $sql;die;
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @param array|string $mix_id id
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-11-26
	 */
	static public function del_by_id($dc,$mix_id)
	{
		$str_sql='';
		if(empty($mix_id))
		{
			return self::return_data(1,'参数id为空');
		}
		if(is_string($mix_id))
		{
			$str_sql=$mix_id;
		}
		else if(is_array($mix_id))
		{
			$str_sql = implode("','", $mix_id);
		}
		else
		{
			return self::return_data(1,'参数id非法'.var_export($mix_id,true));
		}
		if(empty($str_sql))
		{
			return self::return_data(1,'参数id为空');
		}
		$sql = "delete from " . self::$base_table . " where nns_id in('{$str_sql}')";
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	/**
	 * 检查队列中的任务
	 */
	static public function check_online_queue($dc,$params)
	{
		$str_sql = self::build_where($params);
		if($str_sql === false)
		{
			return self::return_data(1,'入参错误');
		}
		$sql = "select count(1) as count from " . self::$base_table . " where " . $str_sql;
		$result = nl_db_get_one($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok',array('count'=>(isset($result['count']) && $result['count'] >=0) ? $result['count'] : 0));
// 		if($result['count'] > 0)
// 		{
// 			return self::return_data(0,'检查存在');
// 		}
// 		return self::return_data(1,'检查不存在');
	}
	/**
	 * 按条件删除队列
	 */
	static public function del_by_condition($dc,$params)
	{
		$str_sql = self::build_where($params);
		if($str_sql === false)
		{
			return self::return_data(1,'入参错误');
		}
		$sql = "delete from " . self::$base_table . " where " . $str_sql;
		$result = nl_execute_by_db($sql, $dc->db());
		if($result)
		{
			return self::return_data(0,'删除成功');
		}
		else
		{
			return self::return_data(1,'删除失败'.$sql);
		}
	}
	/**
	 * 组装where条件
	 */
	static public function build_where($params)
	{
		if(!is_array($params) && empty($params))
		{
			return false; 
		}
		$str_sql = '';
		foreach ($params as $key=>$val)
		{
			if(is_array($val))
			{
				$filter = implode('","', $val);
				$str_sql .= $key .' in ("' . $filter . '") and ';
			}
			else 
			{
				$str_sql .= $key . '="' . $val . '" and ';
			}
		}
		$str_sql = rtrim($str_sql,' and ');
		return $str_sql;
	}

    /**
     * 依据条件查询切片队列信息
     * @param $dc
     * @param $sql
     * @return array
     */
    static public function query_by_sql($dc, $sql)
    {
        $result = nl_query_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (isset($result) && !empty($result)) ? $result : null;
        return self::return_data(0, 'OK' . $sql, $result);
    }
}