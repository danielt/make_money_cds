<?php
include_once dirname(dirname(dirname(__FILE__))) . '/mgtv_v2/mgtv_init.php';

include_once dirname(__FILE__) . '/asset_online.class.php';
$dc = nl_get_dc(array (
				"db_policy" => NL_DB_WRITE,
				"cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE
		));
$fail_params = array(
				'nns_import_id' => '213',
				'nns_category_id' => '6676565',
				'nns_org_id' => 'sdf',
				'nns_status' => array(2,4,5,6,7),
				'nns_cp_id' => 0,
				);
$result = nl_asset_online::del_by_condition($dc,$fail_params);