<?php
/**
 * 媒资上线日志管理
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_asset_online_log extends nl_public
{
	static $fields = array(
			'nns_id',
			'nns_video_name',
			'nns_import_id',
			'nns_category_id',
			'nns_action',
			'nns_reason',
			'nns_import_content',
			'nns_callback_content',
			'nns_org_id',
			'nns_status',
			'nns_message_id',
			'nns_asset_online_id',
			'nns_create_time',
			'nns_modify_time',
			'nns_vod_id',
			'nns_type'
	);
	
	static $base_table='nns_import_asset_online_log';
	
	/**
	 * 查询日志列表
	 * @param object $dc 数据库对象
	 * @param array $params 查询参数
	 * @param array $page_info 分页信息
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-11-26
	 */
	static public function query($dc,$params=null,$page_info=null)
	{
		if(isset($params['where']) && !empty($params['where']))
		{
			foreach ($params['where'] as $where_key=>$where_val)
			{
				self::$str_where.=" {$where_key} = '{$where_val}' and ";
			}	
		}
		if(isset($params['like']) && !empty($params['like']))
		{
			foreach ($params['like'] as $like_key=>$like_val)
			{
				self::$str_where.=" {$like_key} like '%{$like_val}%' and ";
			}
		}
		self::$str_where = (strlen(self::$str_where) > 5) ? trim(self::$str_where,'and ') : '';
		$str_limit = self::make_page_limit($page_info);
		$sql="select * from " . self::$base_table . " " . self::$str_where . " order by nns_create_time desc {$str_limit} ";
		$sql_count = "select count(*) as count from " . self::$base_table . " " . self::$str_where;
		$result_count = nl_query_by_db($sql_count,  $dc->db());
		$result = nl_query_by_db($sql, $dc->db());
		self::init_where();
		if(!$result || !$result_count)
		{
			return self::return_data(1,"查询数据失败".$sql);
		}
		return self::return_data(0,"查询数据成功",$result,$result_count);
	}
	
	/**
	 * 查询单个日志列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-11-26
	 */
	static public function query_by_id($dc,$nns_id)
	{
		if(empty($nns_id))
		{
			return self::return_data(1,'id为空');
		}
		$sql="select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
// 		echo $sql;die;
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,"查询数据失败".$sql);
		}
		return self::return_data(0,"查询数据成功",$result);
	}
	
	/**
	 * 添加日志
	 * @param object $dc 数据库对象
	 * @param array $params 插入参数
	 * @return array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-11-26
	 */
	static public function add($dc,$params)
	{
		$params = self::except_useless_params(self::$fields, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		//$params['nns_id'] = np_guid_rand();
		$params['nns_create_time'] = $params['nns_modify_time'] = date("Y-m-d H:i:s");
		$sql = self::make_insert_sql(self::$base_table, $params);
		//echo $sql;die;
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 修改日志
	 * @param object $dc 数据库对象
	 * @param array $params 插入参数
	 * @param array $where_params 插入参数条件
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-11-26
	 */
	static public function update($dc,$params,$where_params)
	{
		$params = self::except_useless_params(self::$fields, $params,$where_params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$params['nns_modify_time'] = date("Y-m-d H:i:s");
		$sql = self::make_update_sql(self::$base_table, $params,$where_params);
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 删除日志
	 * @param object $dc 数据库对象
	 * @param array|string $mix_id id
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-11-26
	 */
	static public function del_by_id($dc,$mix_id)
	{
		$str_sql='';
		if(empty($mix_id))
		{
			return self::return_data(1,'参数id为空');
		}
		if(is_string($mix_id))
		{
			$str_sql=$mix_id;
		}
		else if(is_array($mix_id))
		{
			$str_sql = implode("','", $mix_id);
		}
		else
		{
			return self::return_data(1,'参数id非法'.var_export($mix_id,true));
		}
		if(empty($str_sql))
		{
			return self::return_data(1,'参数id为空');
		}
		$sql = "delete from " . self::$base_table . " where nns_id in('{$str_sql}')";
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
}