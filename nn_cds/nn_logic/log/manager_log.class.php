<?php
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
class nl_manager_log {
	/**
	 * 添加后台操作日志
	 * @param String 日志内容
	 * @param String 操作方式 <option value ="edit" >修改</option>
 								<option value ="delete">删除</option>
  								<option value="add">添加</option>
  								<option value="login">登录</option>
  								<option value="audit">审核\取消审核</option>
  								<option value="lock">锁定\解锁用户</option>
  								<option value="order">排序</option>
  								<option value="unline">下线影片</option>
  								<option value="reset">恢复回收数据</option>
  								<option value="real_delete">彻底删除数据</option>
  								<option value="move_category">移动栏目</option>
  		@return TRUE 添加成功
  				 FALSE 添加失败
	 */
	public static function add($dc,$log,$action){
//		'$op_admin_id','$op_type',now(),'$op_desc',$org_type,'$org_id')";
		$sql='insert into nns_op_log' .
		'(nns_id,' .
		'nns_op_admin_id,' .
		'nns_op_type,' .
		'nns_op_time,' .
		'nns_op_desc,' .
		'nns_org_type,' .
		'nns_org_id)' .
		'values(' .
		'\''.np_guid_rand().'\','.
		'\''.$_SESSION["nns_mgr_id"].'\','.
		'\''.$action.'\','.
		'now(),'.
		'\''.$log.'\','.
		'\''.$_SESSION["nns_manager_type"].'\','.
		'\''.$_SESSION["nns_org_id"].'\' )';
		
		return nl_execute_by_db($sql, $dc->db());
	}
	
	/**
	 * 批量查询视频名称
	 * @param array 视频ID组
	 * @param String 视频类型  vod | live 
  	  @return array 视频名称数组
  	  
  	  			array(
  	  				'id'=>array('nns_name'=>'','nns_id'=>'')
  	  						
  	  			)
  				 
	 */
	public static function get_names_by_video_id($dc,$ids,$type='vod'){
		$table_name='nns_'.$type;
		
		$sql='select nns_name,nns_id,nns_cp_id,nns_import_source from '. $table_name;
		$id_query='';
		foreach ($ids as $id_item){
			$id_query.='\''.$id_item.'\',';
		}
		
		$id_query=rtrim($id_query,',');
		
		$sql.=' where nns_id in ('.$id_query.') ';
		
		$result=nl_query_by_db($sql,$dc->db());
		
		
		
		if ($result===TRUE || $result===FALSE) return $result;
		
		$result=np_array_rekey($result,'nns_id');
		
		return $result;
	}
	
	/* 批量查询包栏目名称
	 * @param string 包ID
	 * @param String 包类型  asset | topic 
  	  @return array 视频名称数组
  	  
  	  			array(
  	  				'id'=>array('nns_name'=>'','nns_id'=>'')
  	  						
  	  			)
  				 
	 */
	public static function get_names_by_packet_id($dc,$packet_id,$type='asset'){
		if ($type=='asset'){
			$table_name=' nns_assists ';
		}elseif ($type=='topic'){
			$table_name=' nns_topic ';
		}elseif ($type=='service'){
			$table_name=' nns_service ';
		}
//		$table_name='nns_'.$type;
		
		$sql='select * from '. $table_name .' where nns_id=\''.$packet_id.'\'';
		
		$category_result=nl_query_by_db($sql,$dc->db());
		
		if ($category_result===TRUE || $category_result===FALSE) return $category_result;
		
		$category_str=$category_result[0]['nns_category'];
		$dom = new DOMDocument('1.0', 'utf-8');
		$dom->loadXML($category_str);
		$node=NULL;
		$category = $dom->getElementsByTagName('category');
		$result=array();
		foreach ($category as $item){
			$category_id=$item->getAttribute('id');
//			if (in_array($category_id,$ids)){
				$result[$category_id]=$item->getAttribute('name');
//			}
			
		}
		$result['1000']=$category_result[0]['nns_name'];
		
		return $result;
	}
}
?>