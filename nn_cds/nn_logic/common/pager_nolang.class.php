<?php
/*
 */
class nl_pager {
    private $cur_page;
    private $total_pages;
    private $next_page;
    private $pre_page;
    private $page_size;
    private $url;
    private $total_count;
    private $shorter;
    private $is_num;
    function __construct($count, $page_size=30, $cur_page=1,$url='?',$shorter=false,$is_num=1){
        //
        $url = str_replace('?&','?',$url);
        $this->is_num = $is_num;
        $this->shorter = $shorter;
        $this->total_count = $count;
        $this->page_size = $page_size;
        if($page_size<0) $this->page_size = 1;
        $this->cur_page = $cur_page;
        if($cur_page<0) $this->cur_page = 1;

        $this->total_pages = ceil($count/$this->page_size);
        if($cur_page >$this->total_pages){
            $this->cur_page = 1;
        }


        if($cur_page == 1) {
            $this->pre_page=1;
        }else{
            $this->pre_page = ($cur_page-1);
        }

        if($cur_page == $this->total_pages){
            $this->next_page =1;
        }else{
            $this->next_page = ($cur_page+1);
        }
        $this->url = $url;

    }
    function nav(){
        return $this->nav_1();
    }
    public function nav_1(){

        $html = '';
        $html .= '<div class="pagecontrol">';
        if($this->total_count==0){
            $html .= "<span>首页</span>&nbsp;&nbsp;&nbsp;&nbsp;";
            $html .= "<span>上一页</span>&nbsp;&nbsp;&nbsp;&nbsp;";
            $html .= "<span>下一页</span>&nbsp;&nbsp;&nbsp;&nbsp;";
            $html .= "<span>末页</span>&nbsp;&nbsp;&nbsp;&nbsp;";
        }else{
            if(false !== $pos=strpos($this->url,'?')){
                $len = strlen($this->url);
                if($pos==$len-1){
                    $page = 'page';
                }else{
                    $page = '&page';
                }

            }
            $html .= '记录总数:' . $this->total_count . '条&nbsp;&nbsp;&nbsp;&nbsp;';
            if($this->cur_page !=1){
                $html .= '<a target="_self" href="'.$this->url.'&page=1">首页</a>&nbsp;&nbsp;&nbsp;&nbsp;';
            }else{
                $html .=  '<span>第一页</span>&nbsp;&nbsp;&nbsp;&nbsp;';
            }

            if($this->cur_page> 1){
                $html .= '<a target="_self" href="'.$this->url.$page.'='.$this->pre_page.'">上一页</a>&nbsp;&nbsp;&nbsp;&nbsp;' ;
            }else{
                $html .= '<span>上一页</span>&nbsp;&nbsp;&nbsp;&nbsp;';
            }
            if($this->cur_page != $this->total_pages && $this->total_pages>1){
                $html .= '<a target="_self" href="'.$this->url.$page."=".$this->next_page.'">下一页</a>&nbsp;&nbsp;&nbsp;&nbsp;' ;
            }else{
                $html .= '<span>下一页</span>&nbsp;&nbsp;&nbsp;&nbsp;';
            }
            if($this->total_pages>1 && $this->cur_page != $this->total_pages){
                $html .= '<a target="_self" href="'.$this->url.$page."=".$this->total_pages.'">末页</a>&nbsp;&nbsp;&nbsp;&nbsp;';
            }else{
                $html .= '<span>末页</span>&nbsp;&nbsp;&nbsp;&nbsp;';
            }
        }
        if (!$this->shorter){
            if($this->is_num){
                $html .= '跳转到第<input type="text" style="width:20px;" value="'.$this->cur_page.'" id="go_page_num" name="go_page_num"> 页 &nbsp;&nbsp;';
                $html .= '<a href="javascript:go_page_num(\''.$this->url.'\','.$this->total_pages.');">GO&gt;&gt;</a>&nbsp;&nbsp;';
            }
            $html .= '当前 <span style="font-weight:bold;color:#ff0000;">'.$this->cur_page.'/'.$this->total_pages.'</span>页&nbsp;&nbsp;';
            if($this->is_num){
                $html .= '|&nbsp;&nbsp;分页条数&nbsp;';
                $html .= '<input type="text" style="width:24px;" value="'.$this->page_size.'" id="nns_list_max_num" name="nns_list_max_num">&nbsp;&nbsp;';
                $html .= '<input type="button" onclick="refresh_prepage_list();" value="确定">&nbsp;&nbsp;';
            }

        }
        $html .= '</div>';
        return $html;
    }
    function _nav_1__javascript(){
        $js ='<script>';
$js .= <<<EOD
function refresh_prepage_list(){
    setCookie("page_max_num",$("#nns_list_max_num").val());
    var url = window.location.href;

    window.location.href = url.replace(/\&page=(\d)*/i, "&page=1");

}
EOD;

        $js .='</script>';
        return $js;
    }
    function _javascript(){
        $js = '';
        $js .='<script> function  _go2page(url,maxpage,key){
            var pagenum=$("#go_page_num").val();
            if (isNaN(pagenum) || pagenum.indexOf(".")>=0){
                alert("所填内容必须为整数!");
                return;
    }
    if (pagenum>maxpage){
        pagenum=maxpage;
    }
    if (pagenum<1){
        pagenum=1;
    }
    var page_size=$("#page_size").val();
    if (isNaN(pagenum) || pagenum.indexOf(".")>=0){
        alert("请输入数字");
        return;
    }
    url += "&page_size="+page_size;
    if (key==undefined || key==""){
        window.location.href=url+"&page="+pagenum;
    }else{
        window.location.href=url+"&"+key+"="+pagenum;
    }

    }</script>';
    return $js;
    }
}
