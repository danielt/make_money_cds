<?php
include_once dirname(dirname(__FILE__)).'/public.class.php';
class nl_comm_query extends nl_public
{
    /**
	 * 通过注入id  和 cp——id  查询数据
     * @param unknown $dc
     * @param unknown $video_type
     * @param unknown $cp_id
     * @param unknown $import_id
     * @param string $params
     * @param string $cp_config
     */
	public static function get_info_by_import_id_and_other($dc, $video_type,$cp_id, $import_id ,$params=null,$cp_config = null)
	{
		if (strlen($cp_id) < 1)
		{
			return self::return_data('1', 'cp_id为空', '');
		}
		if (strlen($video_type) < 1)
		{
			return self::return_data('1', 'video_type为空', '');
		}
		if (strlen($import_id) < 1)
		{
		    return self::return_data('1', '注入id为空', '');
		}
		if(isset($cp_config) && !empty($cp_config) && is_array($cp_config))
		{
		    $cp_config = self::query_cp_info($dc, $cp_id);
		    if($cp_config['ret'] !=0)
		    {
		        return $cp_config;
		    }
		    $cp_config = $cp_config['data_info'];
		}
		
		
	}
	
	/**
	 * 通过注入id  和 cp——id  查询数据
	 * @param unknown $dc
	 * @param unknown $video_type
	 * @param unknown $cp_id
	 * @param unknown $import_id
	 * @param string $params
	 */
	private static function query_video_info($dc, $video_type,$cp_id, $import_id ,$params=null)
	{
	    switch ($video_type)
	    {
	        case 'video':
	            $table_name ='nns_vod';
	            $str_import_id = 'nns_asset_import_id';
	            break;
            case 'index':
                $table_name ='nns_vod_index';
				$str_import_id = 'nns_import_id';
                break;
            case 'media':
                $table_name ='nns_vod_media';
				$str_import_id = 'nns_import_id';
                break;
            case 'live':
                $table_name ='nns_live';
				$str_import_id = 'nns_import_id';
                break;
            case 'live_index':
                $table_name ='nns_live_index';
				$str_import_id = 'nns_import_id';
                break;
            case 'live_media':
                $table_name ='nns_live_media';
				$str_import_id = 'nns_import_id';
                break;
            case 'playbill':
                $table_name ='nns_live_playbill_item';
				$str_import_id = 'nns_playbill_import_id';
                break;
            default:
                return self::return_data(1,'不能查询此表，此表不存在video_type:'.$video_type);
	    }
	    $sql="select * from {$table_name} where {$str_import_id}='{$import_id}' and nns_cp_id='{$cp_id}'";
	    if(!empty($params) && is_array($params))
	    {
	        foreach ($params as $key=>$val)
	        {
	            $key = (substr($key, 0,4) == 'nns_') ? $key : 'nns_'.$key;
	            $sql.=" and {$key}='{$val}' ";
	        }
	    }
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'sql error:'.$sql);
	    }
	    return self::return_data(1,'ok:'.$sql,$result);
	}
	
	/**
	 * 查询cp信息
	 * @param unknown $dc
	 * @param unknown $cp_id
	 */
	private function query_cp_info($dc,$cp_id)
	{
	    include_once dirname(dirname(__FILE__)).'/cp/cp.class.php';
	    $result_cp_config = nl_cp::query_by_id($dc, $cp_id);
	    if($result_cp_config['ret'] !=0)
	    {
	        return $result_cp_config;
	    }
	    if(!isset($result_cp_config['data_info']['nns_config']) || empty($result_cp_config['data_info']['nns_config']))
	    {
	        return self::return_data(1,'查询cp信息为空：cp_id['.$cp_id.']'.$result_cp_config['data_info']['nns_config']);
	    }
	    $result_cp_config = json_decode($result_cp_config['data_info']['nns_config'],true);
	    if(!is_array($result_cp_config))
	    {
	        return self::return_data(1,'查询cp信息为空：cp_id['.$cp_id.'],json解析失败'.$result_cp_config['data_info']['nns_config']);
	    }
	    return self::return_data(0,'ok',$result_cp_config);
	}
}
