<?php
/**
 * @author txc   by 2012-12-6
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
class nl_global {
	
/**
 * 获取所有的配置模块
 */
	static public function get_module($dc){
		if(empty($dc)){
			return false;
		}
		$db=$dc->db();
		$sql="select nns_module from nns_cms_global group by nns_module";
		$result=nl_query_by_db($sql,$db);
		if(is_array($result)){
			$i=0;
			foreach($result as $item){
				$res[$i]=$item['nns_module'];	
				$i++;
			}
			$result=$res;	
		}		
		return $result;
	}
	
/**
 * @param DC $dc模块
 * @param string $module模块类型
 * @return array(配置列表结果
 * (
 * "audit"=>自动审核
 * "sen_word"=>敏感词汇集以“，”分割
 * )
 * )
 * 失败返回			   false
 * 成功结果集为空返回   true  
 */
	
   static public function get_global($dc,$module) {
   		if(empty($dc)||empty($module)){
   			return false;
   		}
   		$db=$dc->db();
   		$sql="select * from nns_cms_global where nns_module='$module'";
   		$result=nl_query_by_db($sql,$db);
   		if(is_array($result)){
   			foreach($result as $item){
   				switch($item['nns_key']){
   					case 'audit':
   						$data['audit']=$item['nns_value'];
   					break;
   					case 'sen_word':
   						$data['sen_word']=$item['nns_value'];
   					break;
   				}
   			}
   			return $data;
   		}
   		
  		return $result; 	
    }
    
    /**
     * @param DC $dc模块
     * @param string $module 模块类型
     * @param string $key  参数键
     * @param string $value 参数值
     * @return 
     * 成功返回true 
     * 失败返回false
     */
    static public function modify_global($dc,$module,$key,$value){
    	if(empty($dc)){
    		return false;
    	}
    	if(empty($module)||empty($key)){
    		return false;
    	}
    	$db=$dc->db();
    	$key=trim($key);
    	$value=trim($value);
    	$sql='select nns_id from nns_cms_global where nns_module='."'$module'".'and nns_key='."'$key'";
    	$result=nl_query_by_db($sql,$db);
    	if($result==false){
    		return false;
    	}elseif(is_array($result)){
    		$sql="update nns_cms_global set nns_value='$value' where nns_module='$module' and nns_key='$key'";
    		$result=nl_execute_by_db($sql,$db);	
    	}else{
    		$nns_id = np_guid_rand();
    		$sql="insert into nns_cms_global (nns_id,nns_module,nns_key,nns_value) values ('$nns_id','$module','$key','$value')";
    		$result=nl_execute_by_db($sql,$db);
    	}
    	return $result;
    	
    }
}
?>