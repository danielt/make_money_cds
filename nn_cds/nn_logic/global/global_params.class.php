<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-3-10
 * Time: 下午10:30
 * To change this template use File | Settings | File Templates.
 */

include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
class nl_global_prams {
    const CACHE_TIME_OUT=300;

    /**
     * 获取系统下行参数列表
     * @param dc DC模块
     * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
     * @return
     *      Array 媒资包信息结
     *      FALSE 查询失败
     *      TRUE  查询成功，但无数据
     */
   static function get_global_params_list($dc,$policy = NL_DC_AUTO)
   {
       $result = null;
       if ($policy == NL_DC_AUTO)
       {
           if ($dc->is_cache_enabled())
           {
               $result = self::_get_global_params_list_by_cache($dc->cache());
               if ($result === FALSE )
               {
                   $result = self::_get_global_params_list_by_db($dc->db());
                   self::_set_global_params_list_by_cache($dc->cache(),$result);
               }
           }
           else
           {
               $result = self::_get_global_params_list_by_db($dc->db());
           }
       }
       elseif($policy == NL_DC_DB)
       {
           $result = self::_get_global_params_list_by_db($dc->db());
       }
       elseif($policy == NL_DC_CACHE)
       {
           $result = self::_get_global_params_list_by_cache($dc->cache());
       }

       return $result;
   }

    static private function _get_global_params_list_by_db($db)
    {
        $str_sql = "select * from nns_gxcatv_params ";
        $result = nl_query_by_db($str_sql, $db);
        return $result;
    }

    static private function _get_global_params_list_by_cache($cache)
    {
        if ($cache===NULL) return FALSE;

        return unserialize($cache->get('global_params|#'));
    }

    static private function _set_global_params_list_by_cache($cache,$data)
    {
        if ($cache===NULL) return FALSE;
        return $cache->set('global_params|#',serialize($data),self::CACHE_TIME_OUT);
    }

    /**
     * 删除下行参数
     * @param $dc
     * @param $id
     * @return bool
     */

    static function delete_global_params($dc,$id)
    {
        $str_sql = "delete from nns_gxcatv_params where nns_id='$id'";
        $result =  nl_query_by_db($str_sql, $dc->db());
        if($result){
           $result =  self::_delete_global_params_by_cache($dc,$id);
        }
        return $result;
    }

    static function _delete_global_params_by_cache($dc,$key)
    {
        if ($dc->cache()===NULL) return FALSE;
        return $dc->cache()->delete('global_params|#',$key);
    }

    /**
     * 根据key获取value
     * @param $key
     * @return bool
     */
    static function get_global_params_info($dc,$key,$policy = NL_DC_AUTO){
        $result = null;
        if ($policy == NL_DC_AUTO){
            if ($dc->is_cache_enabled()) {
                $result = self::_get_global_params_info_by_cache($dc->cache(),$key);
                if ($result === FALSE ) {
                    $result = self::_get_global_params_info_by_db($dc->db(),$key);
                    self::_set_global_params_info_by_cache($dc->cache(),$key,$result);
                }
            }
            else {
                $result = self::_get_global_params_info_by_db($dc->db(),$key);
            }
        }elseif($policy == NL_DC_DB){
            $result = self::_get_global_params_info_by_db($dc->db(),$key);
        }elseif($policy == NL_DC_CACHE){
            $result = self::_get_global_params_info_by_cache($dc->cache(),$key);
        }

        return $result;
    }
    static function _get_global_params_info_by_db($db,$key)
    {
        $str_sql = "select * from nns_gxcatv_params where nns_key='$key'";
        $result = nl_query_by_db ($str_sql,$db);
        return $result;
    }
    static function _get_global_params_info_by_cache($cache,$key)
    {
        if ($cache===NULL) return FALSE;
        return unserialize($cache->get('global_params_info|#'.$key));
    }
    static function _set_global_params_info_by_cache($cache,$key,$data)
    {
        if ($cache===NULL) return FALSE;
        return unserialize($cache->set('global_params_info|#'.$key,serialize($data),self::CACHE_TIME_OUT));
    }
    static function modify_global_params($dc,$id,$key,$value)
    {
        $str_sql = "update nns_gxcatv_params set nns_value='$value',nns_key='$key' where nns_id='$id'";
        $result = nl_execute_by_db ($str_sql,$dc->db());
        //删除缓存
        self::_delete_global_params_by_cache($dc,$id);
        return $result;
    }

    static function add_global_params($dc,$key,$value)
    {
        $result = self::_get_global_params_info_by_db($dc->db(),$key);
        if (count($result["data"])>0){
            $result = self::modify_global_params($dc,$result['data']['0']['nns_id'],$key,$value);
        }else{
            //生成GUID
            $guid = np_guid_rand();
            $str_sql = "insert into nns_gxcatv_params (nns_id,nns_key,nns_value) values ('$guid','$key','$value')";
            $result =  nl_execute_by_db($str_sql,$dc->db());
        }
        //设置缓存
        $data = self::_get_global_params_info_by_db($dc->db(),$key);
        self::_set_global_params_info_by_cache($dc->cache(),$key,$data);
        return $result;
    }
    static function get_global_qam_params_list($dc,$id,$policy = NL_DC_AUTO)
    {
        $result = null;
        if ($policy == NL_DC_AUTO)
        {
            if ($dc->is_cache_enabled())
            {
                $result = self::_get_global_qam_params_list_by_cache($dc->cache(),$id);
                if ($result === FALSE )
                {
                    $result = self::_get_global_qam_params_list_by_db($dc->db(),$id);
                    self::_set_global_qam_params_list_by_cache($dc->cache(),$id,$result);
                }
            }
            else
            {
                $result = self::_get_global_qam_params_list_by_db($dc->db(),$id);
            }
        }
        elseif($policy == NL_DC_DB)
        {
            $result = self::_get_global_qam_params_list_by_db($dc->db(),$id);
        }
        elseif($policy == NL_DC_CACHE)
        {
            $result = self::_get_global_qam_params_list_by_cache($dc->cache(),$id);
        }

        return $result;
    }
    static function _get_global_qam_params_list_by_db($db,$id)
    {
        //$id = 'gxcatv';
        $str_sql = "select * from nns_gxcatv_qam_global where nns_id='$id'";
        $result = nl_query_by_db($str_sql,$db);
        return $result;
    }
    static function _get_global_qam_params_list_by_cache($cache,$id)
    {
        if ($cache===NULL) return FALSE;
        return unserialize($cache->get('global_qam_params_list|#'.$id));
    }
    static function _set_global_qam_params_list_by_cache($cache,$id,$data)
    {
        if ($cache===NULL) return FALSE;
        return unserialize($cache->set('global_qam_params_list|#'.$id,serialize($data),self::CACHE_TIME_OUT));
    }
//    static function _delete_global_qam_params_by_cache($dc,$key)
//    {
//        if ($dc->cache()===NULL) return FALSE;
//        return $dc->cache()->delete('global_qam__params|#',$key);
//    }

    static function modify_global_qam_params($dc,$id,$frequence,$symbol_rate,$qam_mode)
    {
        $str_sql = "select count(1) from nns_gxcatv_qam_global where nns_id='$id'";
        $result = nl_execute_by_db($str_sql,$dc->db());
        if ($result["ret"]==0)
        {
            if (count($result["data"])>0)
            {
                $str_sql = "update nns_gxcatv_qam_global set nns_frequence=$frequence,nns_symbol_rate=$symbol_rate,nns_qam_mode='$qam_mode' where nns_id='$id'";
                $result = nl_execute_by_db($str_sql,$dc->db());
            }
            else
            {
                $str_sql = "insert into nns_gxcatv_qam_global (nns_id,nns_frequence,nns_symbol_rate,nns_qam_mode) values ('$id',$frequence,$symbol_rate,'$qam_mode')";
                $result = nl_execute_by_db($str_sql,$dc->db());
            }
            //删除缓存
            // self::_delete_global_qam_params_by_cache($dc,$id);
        }
        return $result;
    }

}
