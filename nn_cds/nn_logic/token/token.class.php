<?php
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
class nl_token {
	  /**
	  * 构建TOKEN串
	  * @param Array(
	  * 			"nns_user_id"=>用户ID
	  * 			"nns_device_id"=>设备ID
	  * 			"nns_smart_card_id"=>智能卡ID
	  * 			
	  * 		)
	  * @return String TOKEN串
	  */
	static public function encode_login_token($params){
 	$nns_user_id=isset($params['nns_user_id'])?$params['nns_user_id']:'';
 	$nns_device_id=isset($params['nns_device_id'])?$params['nns_device_id']:'';
 	$nns_smart_card_id=isset($params['nns_smart_card_id'])?$params['nns_smart_card_id']:'';
 	$nns_mac_id=isset($params['nns_mac_id'])?$params['nns_mac_id']:'';
 	$tag=isset($params['nns_tag'])?$params['nns_tag']:'';
 	
 	$return_str=$nns_user_id.'|#'.
 				$nns_device_id.'|#'.
 				$nns_smart_card_id.'|#'.
 				$nns_mac_id.'|#'.
 				$tag.'|#'.
 				date('Ymd',strtotime('+1 month'));
 				
 		return np_authcode($return_str,'ENCODE');
 	}
 	
 	 /**
	  * 解析TOKEN串
	  * @param TOKEN串
	  * @return array(
	  * 	"nns_user_id"=>用户ID
	  * 			"nns_device_id"=>设备ID
	  * 			"nns_smart_card_id"=>智能卡ID
	  * 	"valid_date"=>有效期限 201212 年月
	  * ) 
	  */
	 static public function decode_login_token($token){
	 	if (empty($token)) return NULL;
	 	$token_data=np_authcode($token,'DECODE');
	 	$token_params=explode('|#',$token_data);	
	 	return array(
	 			'nns_user_id'=>$token_params[0],
	 			'nns_device_id'=>$token_params[1],
	 			'nns_smart_card_id'=>$token_params[2],
	 			'nns_mac_id'=>$token_params[3],
	 			'nns_tag'=>$token_params[4],
	 			'date'=>$token_params[5]
	 		);
	 }
	 
	 /**
	  * 检查TOKEN串合法性
	  * @param TOKEN串
	  * @return 
	  *  0 合法
	  *  1 不合法
	  *  2 重新认证 认证超时 
	  */
 
	 static public function check_login_token_valid($token){
	 	$token_params=self::decode_login_token($token);
	 	if (empty($token_params['nns_user_id'])){
	 		return 1;
	 	}
	// 	检查时效性
	 	if (date('Ymd')>$token_params['date']){
	 		return 2;
	 	}
	 	return 0;
	 }
}

?>