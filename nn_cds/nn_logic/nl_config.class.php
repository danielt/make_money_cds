<?php
include_once dirname(__FILE__).'/nl_config.php';
class nl_config {
	static public function sub_platform_enabled(){
		if (defined('g_sub_platform_enable')){
			return g_sub_platform_enable;
		}
		return 0;
	}
	
	static public function sub_platform_cache_server(){
		if (defined('g_sub_platform_memcache_server') && g_sub_platform_memcache_server!=''){
			return g_sub_platform_memcache_server;
		}else{
			return NULL;
		}
	}
	
	static public function main_platform_cache_server(){
		if (defined('g_memcache_server') && g_memcache_server!=''){
			return g_memcache_server;
		}else{
			return NULL;
		}
	}
}
?>