<?php
/**
 * 新的日志对象，这个对象是对np里面的日志对象的封装实现单态
 * Created by IntelliJ IDEA.
 * User: yxt
 * Date: 2014/5/8
 * Time: 13:36
 */
include_once dirname(dirname(dirname(__FILE__))) . '/np/np_log_v2.class.php';

class nl_log_v2
{

    public static $obj_log_v2 = null;
    private static $str_level = null; //日志等级，null全部不记录，error只记录日志，info只记录info
    private static $bool_init = false; //是否初始化成功

    private function __construct()
    {

    }

    public static function get_log_v2()
    {
        if (self::$obj_log_v2 == null) {
            self::$obj_log_v2 = new np_log_v2();
        }
        return self::$obj_log_v2;
    }

    public static function info($str_info, $force_save = false)
    {
        //如果没有初始化成功返回false
        if (self::$bool_init === false) {
            return false;
        }
        self::get_log_v2();
        if (self::$str_level == 'info') {
            self::$obj_log_v2->info($str_info);
            if ($force_save) {
                self::$obj_log_v2->save();
            }
        }
    }

    public static function error($str_error, $force_save = true)
    {
        //如果没有初始化成功返回false
        if (self::$bool_init === false) {
            return false;
        }
        self::get_log_v2();
        if (self::$str_level == 'error' || self::$str_level == 'info') {
            self::$obj_log_v2->error($str_error);
            if ($force_save) {
                self::$obj_log_v2->save();
            }
        }
    }

    public static function set_dir($str_dir)
    {
        self::get_log_v2();
        self::$obj_log_v2->set_dir($str_dir);
    }

    /**
     * log等级设置
     * @param string $str_level log类型只能够是类属性：level_error、level_info
     * @param ing $int_file_size 每个日志文件的大小 单位是（M）,0表示不限制
     * @param ing $int_file_count 该类型目录下的日志文件数量上限,0表示不限制
     * @param string $str_file_prefix 该类型目录下的日志文件的前缀，不能够为空
     * @param ing $int_second 日志每隔多少秒生成一个新的，0表示不限制，默认1小时
     * @return boolean 返回设置是否成功
     */
    public static function set_level_config($str_level, $int_file_size, $int_file_count, $str_file_prefix, $int_valid_time = 3600)
    {
        self::get_log_v2();
        self::$obj_log_v2->set_level_config($str_level, $int_file_size, $int_file_count, $str_file_prefix, $int_valid_time);
        self::$bool_init = true;
    }

    public static function set_current_level($str_current_level)
    {
        self::$str_level = $str_current_level;
    }
}

/**
 * 记录info日志
 * @param $str_module 模块名称
 * @param $str_info 日志信息
 * @param bool $force_save 是否立即保存，默认否
 */
function nl_log_v2_info($str_module, $str_info, $force_save = false)
{
    nl_log_v2::info('[' . $str_module . ']' . $str_info, $force_save);
}

/**
 * 记录error日志
 * @param $str_module 模块名称
 * @param $str_error 日志信息
 * @param bool $force_save 是否立即保存，默认是，如果都是立即保存会影响性能
 */
//第三个是否立马存入文件，这几个函数放在一个新的文件里面，这个文件来include
function nl_log_v2_error($str_module, $str_error, $force_save = true)
{
    nl_log_v2::error('[' . $str_module . ']' . $str_error, $force_save);
}

//程序执行结束自动调用日志保存
register_shutdown_function(array(nl_log_v2::get_log_v2(), "save"));
/**
 * @auth yxt
 * 2014年5月8日14:53:21
 * 前端epg日志初始化
 * 包括实例化对象，配置日志路径，配置日志等级，配置日志文件数据
 * @param string $str_module 模块名称比如n35_a_1
 */
function nl_log_v2_init_epg($str_module)
{
    $str_ini_config = dirname(dirname(__FILE__)) . '/nn_cms_config/global_epg_log.ini';
    $arr_api_ini = parse_ini_file($str_ini_config, true);
    //如果配置文件存放路径则返回false，配置无效
    if (!isset($arr_api_ini['default']['log_dir']) || empty($arr_api_ini['default']['log_dir'])) {
        return false;
    }
    //配置日志存放路径
    $str_dir = dirname(dirname(__FILE__)) .'/'. $arr_api_ini['default']['log_dir'] .'/'. $arr_api_ini['default']['log_sub_dir'];
    nl_log_v2::set_dir($str_dir);
    //配置日志等级
    $str_level = isset($arr_api_ini[$str_module]['log_level'])?$arr_api_ini[$str_module]['log_level']:'';
    $arr_level = array('null', 'error', 'info');
    if (!in_array($str_level, $arr_level)) { //如果当前模块没有配置，或者没在配置范围内，获取默认配置
        $str_level = $arr_api_ini['default']['log_level'];
        if (!in_array($str_level, $arr_level)) { //如果默认配置没有配置，则为null
            $str_level = 'null';
        }
    }
    //如果配置不是空，根据全局的修改配置规则，规则是当前模块的不能够比全局的输出的内容多。
    if ($str_level !== 'null') {
        if($arr_api_ini['global']['log_level']==='null'){
            $str_level='null';
        }elseif($arr_api_ini['global']['log_level']==='error'){
            $str_level='error';
        }
    }
    nl_log_v2::set_current_level($str_level);
    //组装日志文件配置数据
    $int_file_size = $arr_api_ini['default']['file_size'];
//    $int_file_count = $arr_api_ini['default']['file_count'];
    $int_file_prefix = $arr_api_ini['default']['file_prefix'];
    $int_valid_time = $arr_api_ini['default']['valid_time'];
    //如果有一个没有配置则初始化失败
    if (empty($int_file_size) || empty($int_file_prefix) || empty($int_valid_time)) {
        return false;
    }
    nl_log_v2::set_level_config('error', $int_file_size, $int_file_prefix, $int_valid_time);
    nl_log_v2::set_level_config('info', $int_file_size, $int_file_prefix, $int_valid_time);
}

/**
 * @auth yxt
 * 2014年5月8日14:53:21
 * 后台manager日志初始化
 * 包括实例化对象，配置日志路径，配置日志等级，配置日志文件数据
 * @param string $str_module 模块名称比如n35_a_1
 */
function nl_log_v2_init_manager($str_module)
{
    $str_ini_config = dirname(dirname(__FILE__)) . '/nn_cms_config/global_manager_log.ini';
    $arr_api_ini = parse_ini_file($str_ini_config, true);
    //配置日志存放路径
    $str_dir = dirname(dirname(__FILE__)) .'/'. $arr_api_ini['default']['log_dir'] .'/'. $arr_api_ini['default']['log_sub_dir'];
    nl_log_v2::set_dir($str_dir);
    //获取当前模块日志等级
    $str_level = isset($arr_api_ini[$str_module]['log_level'])?$arr_api_ini[$str_module]['log_level']:'';
    $arr_level = array('null', 'error', 'info');
    if (!in_array($str_level, $arr_level)) { //如果当前模块没有配置，或者没在配置范围内，获取默认配置
        $str_level = $arr_api_ini['default']['log_level'];
        if (!in_array($str_level, $arr_level)) { //如果默认配置没有配置，则为null
            $str_level = 'null';
        }
    }
    //如果配置不是空，根据全局的修改配置规则，规则是当前模块的不能够比全局的输出的内容多。
    if ($str_level !== 'null') {
        if($arr_api_ini['global']['log_level']==='null'){
            $str_level='null';
        }elseif($arr_api_ini['global']['log_level']==='error'){
            $str_level='error';
        }
    }
    nl_log_v2::set_current_level($str_level);
    //组装日志文件配置数据
    $int_file_size = $arr_api_ini['default']['file_size'];
    $int_file_prefix = $arr_api_ini['default']['file_prefix'];
    $int_valid_time = $arr_api_ini['default']['valid_time'];
    //如果有一个没有配置则初始化失败
    if (empty($int_file_size) || empty($int_file_prefix) || empty($int_valid_time)) {
        return false;
    }
    nl_log_v2::set_level_config('error', $int_file_size, $int_file_prefix, $int_valid_time);
    nl_log_v2::set_level_config('info', $int_file_size, $int_file_prefix, $int_valid_time);
}