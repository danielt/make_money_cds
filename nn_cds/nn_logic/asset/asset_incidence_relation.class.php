<?php

/**
 * 媒资信息关联关系类
 * @author xingcheng.hu
 * @date 2016/04/15
 */
include_once dirname(dirname(__FILE__)).'/public.class.php';
class nl_asset_incidence_relation extends nl_public
{

	static $table_vod = 'nns_vod';
	static $table_vod_index = 'nns_vod_index';
	static $table_vod_media = 'nns_vod_media';
	static $table_live = 'nns_live';
	static $table_live_index = 'nns_live_index';
	static $table_live_media = 'nns_live_media';
	static $table_live_playbill = 'nns_live_playbill_item';
	/**
	 * @param   object  $dc 		  数据操作对象
	 * @param   string  $type 		  自身类型
	 * @param 	 string  $import_id   注入id
	 * @param 	 int 	 $cp_id 	  cp
	 * @param 	 int 	 $query_type  查询类型   0 查询 同级的数据  | 1 查询父级关联数据  | 2 查询子级关联数据
	 * @param   bool    $flag true   返回数据数组 |  false 只返回是否存在的状态 0存在 -1不存在
	 * @return  array
	 * @author xingcheng.hu
	 * @date 	2016/04/15
	 */
	public static function query_asset_relation($dc,$type,$import_id,$cp_id=0,$query_type=0,$flag=true,$import_source='')
	{
		if(strlen($import_id)<1)
		{
			return self::return_data(1,'注入ID为空');
		}
		switch($type)
		{
			case 'live':
				if($query_type == 0)
				{
// 				    $str_where = (isset($import_source) && strlen($import_source)>0) ? " and nns_import_source='{$import_source}' " : '';
					$sql = "select * from " . self::$table_live . " where nns_import_id='{$import_id}' and nns_cp_id='{$cp_id}' {$str_where} and nns_deleted != '1' limit 1";
				}
				if($query_type == 1)
				{
					return self::return_data(1,'主媒资没有父级');
				}
				if($query_type == 2)
				{
// 				    $str_where = (isset($import_source) && strlen($import_source)>0) ? " and live.nns_import_source='{$import_source}' " : '';
					$sql = "select index.* from ".self::$table_live_index." as index left join " . self::$table_live .
							" as live on index.nns_live_id=live.nns_id where live.nns_import_id='{$import_id}' and index.nns_cp_id='{$cp_id}' {$str_where} ";
				}
				break;
			case 'live_index':
				 break;
			case 'live_media':
				if($query_type == 0)
				{
					$sql = "select * from " . self::$table_live_media . " where nns_content_id='{$import_id}' and nns_cp_id='{$cp_id}' and nns_deleted != '1' limit 1";
				}
				if($query_type == 1)
				{
					$sql = "select live.* from ".self::$table_live." as live left join " . self::$table_live_media .
							" as media on live.nns_id=media.nns_live_id where live.nns_import_id='{$import_id}' and live.nns_cp_id='{$cp_id}'";
				}
				if($query_type == 2)
				{
					$sql = "select playbill.* from ".self::$table_live_playbill." as playbill left join " . self::$table_live_media .
							" as media on playbill.nns_live_media_id=media.nns_id where media.nns_content_id='{$import_id}' and playbill.nns_cp_id='{$cp_id}'";
				}
				break;
			case 'live_playbill':
				if($query_type == 0)
				{
					$sql = "select * from " . self::$table_live_playbill . " where nns_playbill_import_id='{$import_id}' and nns_cp_id='{$cp_id}' and nns_state != '1' limit 1";
				}
				if($query_type == 1)
				{
					$sql = "select media.* from ".self::$table_live_media." as media left join " . self::$table_live_playbill .
							" as playbill on media.nns_id=playbill.nns_live_media_id where media.nns_playbill_import_id='{$import_id}' and media.nns_cp_id='{$cp_id}' and media.nns_deleted != '1' limit 1";
				}
				if($query_type == 2)
				{
					return self::return_data(1,'节目单无子级');
				}
				break;
			case 'video':
				if($query_type===0)
				{
					$sql = "select * from " . self::$table_vod . " where nns_asset_import_id='{$import_id}' and nns_cp_id='{$cp_id}' and nns_deleted !='1' limit 1";
				}
				if($query_type===1)
				{
					return self::return_data(1,'主媒资没有父级');
				}
				if($query_type===2)
				{
					$sql = "select nvi.* from ".self::$table_vod_index." as nvi left join " . self::$table_vod .
						" as nv on nvi.nns_vod_id=nv.nns_id where nv.nns_asset_import_id='{$import_id}' and nvi.nns_cp_id='{$cp_id}' and nvi.nns_deleted !='1' ";
				}
				break;
			case 'index':
				if($query_type===0)
				{
					$sql = "select * from " . self::$table_vod_index . " where nns_import_id='{$import_id}' and nns_cp_id='{$cp_id}' and nns_deleted !='1' limit 1";
				}
				if($query_type===1)
				{
					$sql = "select nv.* from ".self::$table_vod." as nv left join " . self::$table_vod_index . " as nvi on nv.nns_id=nvi.nns_vod_id where nvi.nns_import_id='{$import_id}' and nv.nns_cp_id='{$cp_id}'and nv.nns_deleted !='1' limit 1";
				}
				if($query_type===2)
				{
					$sql = "select nvm.* from ".self::$table_vod_media." as nvm left join " . self::$table_vod_index . " as nvi on nvm.nns_vod_index_id=nvi.nns_id where nvi.nns_import_id='{$import_id}' and nvm.nns_cp_id='{$cp_id}'and nvm.nns_deleted !='1'";
				}
				break;
			case 'media':
				if($query_type===0)
				{
					$sql = "select * from " . self::$table_vod_media . " where nns_import_id='{$import_id}' and nns_cp_id='{$cp_id}'and nns_deleted !='1' limit 1";
				}
				if($query_type===1)
				{
					$sql = "select nvi.* from ".self::$table_vod_index." as nvi left join " . self::$table_vod_media . " as nvm on nvi.nns_id=nvm.nns_vod_index_id where nvm.nns_import_id='{$import_id}' and nvi.nns_cp_id='{$cp_id}' and  nvi.nns_deleted !='1' limit 1";
				}
				if($query_type===2)
				{
					return self::return_data(1,'片源没有子级');
				}
				break;
			default:
				return self::return_data(1,'没有此媒资类型'.var_export($type,true));
		}
		$result = nl_query_by_db($sql,$dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		if ($flag) {
			return self::return_data(0,'ok',$result);
		}
		if(is_array($result)) {
			return self::return_data(0,'存在',0);
		}else{
			return self::return_data(0,'不存在',1);
		}
	}
	
	/**
	 * @param unknown $dc
	 * @param unknown $type
	 * @param unknown $import_id
	 * @param number $cp_id
	 */
	public static function query_asset_relation_by_id($dc,$type,$import_id,$cp_id=0)
	{
	    if(strlen($import_id)<1)
	    {
	        return self::return_data(1,'注入ID为空');
	    }
	    switch($type)
	    {
	        case 'live':
	            $sql = "select * from " . self::$table_live . " where nns_id='{$import_id}' and nns_cp_id='{$cp_id}' and nns_deleted != '1' limit 1";
	            break;
	        case 'live_index':
	            $sql = "select * from " . self::$table_live_index. " where nns_id='{$import_id}' and nns_cp_id='{$cp_id}' and nns_deleted != '1' limit 1";
	            break;
	        case 'live_media':
	            $sql = "select * from " . self::$table_live_media . " where nns_id='{$import_id}' and nns_cp_id='{$cp_id}' and nns_deleted != '1' limit 1";
	            break;
	        case 'live_playbill':
	            $sql = "select * from " . self::$table_live_playbill . " where nns_id='{$import_id}' and nns_cp_id='{$cp_id}' and nns_state != '1' limit 1";
	            break;
	        case 'video':
	            $sql = "select * from " . self::$table_vod . " where nns_id='{$import_id}' and nns_cp_id='{$cp_id}' and nns_deleted !='1' limit 1";
	            break;
	        case 'index':
	            $sql = "select * from " . self::$table_vod_index . " where nns_id='{$import_id}' and nns_cp_id='{$cp_id}' and nns_deleted !='1' limit 1";
	            break;
	        case 'media':
	            $sql = "select * from " . self::$table_vod_media . " where nns_id='{$import_id}' and nns_cp_id='{$cp_id}'and nns_deleted !='1' limit 1";
	            break;
	        default:
	            return self::return_data(1,'没有此媒资类型'.var_export($type,true));
	    }
	    $result = nl_query_by_db($sql,$dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库执行失败'.$sql);
	    }
	    if(isset($result[0]) && is_array($result[0]) && !empty($result[0])) 
	    {
	        return self::return_data(0,'存在',$result[0]);
	    }
	    else
	    {
	        return self::return_data(1,'不存在数据');
	    }
	}
}