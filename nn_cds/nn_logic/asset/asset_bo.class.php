<?php

include_once 'asset.class.php';

class nl_asset_bo {

        /**
         * 从DC模块根据包栏目ID获取该栏目属性
         * @param dc DC模块
         * @param String 媒资包ID
         * @param String 媒资包栏目ID
         * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
         * @return *
         * 			Array 媒资包栏目信息
         * 			FALSE 查询失败
         * 			TRUE  查询成功，但无数据
         */
        static public function get_category_info_by_category_id($dc, $asset_id, $category_id, $policy = NL_DC_AUTO) {
                $data = nl_asset::get_asset_info($dc, $asset_id);
                if ($data === TRUE || $data === FALSE || $data[0]['nns_category_root_id'] == $category_id) {
                        return $data;
                }
                $category_str = $data[0]['nns_category'];
                $xml = simplexml_load_string($category_str);
                $category = $xml->category;
                $result = array();
                $count = count($category);
                for ($i = 0; $i < $count; $i++) {
                        $list = json_decode(@json_encode($xml->category[$i], 1), 1);
                        $list = $list['@attributes'];
                        if ($list['id'] == $category_id) {
                                $result = $list;
                                break;
                        }
                }
                return $result;
        }

        static public function get_category_info_by_id($dom_str, $category_id) {

                if (empty($category_id) || $category_id === '1000')
                        return TRUE;
                $stbclass_xml = simplexml_load_string($dom_str);
                $arr = json_decode(json_encode($stbclass_xml), true);
                //var_dump($arr);exit;
//	去除多语言节点信息
                $node = self::_check_category_id_by_child($arr, $category_id);

                return $node;
        }

        static public function _check_category_id_by_child($arr, $category_id) {
                if (array_key_exists('category', $arr)) {
                        if (isset($arr['category'][0])) {
                                foreach ($arr['category'] as $category) {
                                        //var_dump($category);exit;
                                        if (isset($category['@attributes']) && $category['@attributes']['id'] === $category_id) {
                                                return $category;
                                        } else {
                                                $node = self::_check_category_id_by_child($category, $category_id);
                                                if ($node !== NULL)
                                                        return $node;
                                        }
                                }
                        }else {
                                if ($arr['category']['@attributes']['id'] === $category_id) {
                                        return $arr['category'];
                                } else {
                                        return self::_check_category_id_by_child($arr['category'], $category_id);
                                }
                        }
                }
                return NULL;
        }

}

?>