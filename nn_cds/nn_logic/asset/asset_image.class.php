<?php

class nl_asset_image {

    static public function epg_ceate_image_url($img_url) {
        global $g_bk_version_number;
        $bk_version_number = $g_bk_version_number;
        unset($g_bk_version_number);
        if($bk_version_number == '1')
        {
            global $g_bk_web_url;
            $bk_web_url = $g_bk_web_url;
            unset($g_bk_web_url);
            $bk_web_url = trim(trim(trim($bk_web_url,'/'),'\\')).'/data/downimg/';
            $img_url = trim(trim(trim($img_url,'/'),'\\'));
            return $bk_web_url.$img_url;
        }
	 	global $g_image_host;
	 	global $g_webdir;
	 	if (empty($img_url)) return '';

	 	$img_url=ltrim($img_url,'/');

	 	if (empty($g_image_host)) return $g_webdir.$img_url;

	 	$image_hosts=explode(';',$g_image_host);
	 	$host_num=rand(0,count($image_hosts)-1);
	 	$img_host =$image_hosts[$host_num];
	 	return $img_host.$img_url;
    }
}
?>