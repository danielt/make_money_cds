<?php
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
class nl_asset {
	const CACHE_TIME_OUT=300;
	static function get_asset_list($dc,$start=0,$size=500,$order=NL_ORDER_NUM_ASC,$policy=NL_DC_AUTO){
		return self::get_asset_list_by_db($dc->db(),$start,$size,$order);
	}
	static function get_asset_list_by_db($db,$start=0,$size=500,$order=NL_ORDER_NUM_ASC,$policy=NL_DC_AUTO){
		$sql = "select * from nns_assists ";
		$sql .= " limit $start,$size";
		return nl_db_get_all($sql,$db);
	}
	/**
	 * 从DC模块获取媒资包信息
	 * @param dc DC模块
	 * @param String 媒资包ID
	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
	 * @return *
	 * 			Array 媒资包信息结果
	 * 			FALSE 查询失败
	 * 			TRUE  查询成功，但无数据
	 */
	static public function get_asset_info($dc,$asset_id,$policy=NL_DC_AUTO) {
		$result = null;
		if ($policy == NL_DC_AUTO){
			if ($dc->is_cache_enabled()) {
				$result = self::_get_asset_info_by_cache($dc->cache(),$asset_id);
				if ($result === FALSE ) {
					$result = self::_get_asset_info_by_db($dc->db(),$asset_id);
					self::_set_asset_info_by_cache($dc->cache(),$asset_id,$result);
				}
			}
			else {
				$result = self::_get_asset_info_by_db($dc->db(),$asset_id);
			}
		}elseif($policy == NL_DC_DB){
			$result = self::_get_asset_info_by_db($dc->db(),$asset_id);
		}elseif($policy == NL_DC_CACHE){
			$result = self::_get_asset_info_by_cache($dc->cache(),$asset_id);
		}

		return $result;
	}

	static private function _get_asset_info_by_db($db,$asset_id) {

			$sql = ' select * from nns_assists where ' .
					'nns_id=\''.$asset_id.'\'';
					
			$result=nl_query_by_db($sql, $db);
			
			
			if (is_bool($result)) return $result;
			
			$sql = ' select * from nns_metadata where nns_metadata_type=\'asset\' and ' .
					'nns_metadata_id=\''.$asset_id.'\'';	
					
			$metadata=nl_query_by_db($sql, $db);
			
			$result[0]['metadata']=$metadata;

		return $result;
	}

	static private function _get_asset_info_by_cache($cache,$asset_id) {
		if ($cache===NULL) return FALSE;

		return unserialize($cache->get('asset_info|#'.$asset_id));
	}

	static private function _set_asset_info_by_cache($cache,$asset_id,$data) {
		if ($cache===NULL) return FALSE;
		return $cache->set('asset_info|#'.$asset_id,serialize($data),self::CACHE_TIME_OUT);
	}
//	对于包含子目录的栏目查询 只能删除该包下所有内容缓冲
	static public function delete_asset_item_list_by_child_cache($dc,$asset_id) {
		if ($dc->cache()===NULL) return FALSE;
		return $dc->cache()->delete('asset_info|#'.$asset_id);
	}


	static public function update_virtual_category_kv($dc,$asset_id,$category_id,$key,$vaule){

		$result=self::_get_virtual_category_params_by_db($dc->db(),$asset_id,$category_id);

		if ($result===TRUE || !array_key_exists($key,$result)){
			$sql = ' insert into nns_virtual_category_content (' .
					'nns_media_asset_id,'.
					'nns_category_id,'.
					'nns_key,'.
					'nns_value)' .
					'values'.
					'(\''.$asset_id.'\',' .
					'\''.$category_id.'\',' .
					'\''.$key.'\',' .
					'\''.$vaule.'\')';
		}else{
			$sql = ' update nns_virtual_category_content set ' .
					'nns_value=\''.$vaule.'\' ' .
					'where ' .
					'nns_media_asset_id=\''.$asset_id.'\' and '.
					'nns_category_id=\''.$category_id.'\' and '.
					'nns_key=\''.$key.'\' ';

		}
		return nl_execute_by_db($sql, $dc->db());
	}
	/**
	 * 获取虚栏目键值对
	 * @param 包ID
	 * @param 栏目ID
	 * @param 策略
	 * @return ARRAY
	 * 			TRUE
	 * 			FALSE
	 */
	static public function get_virtual_category_params($dc,$asset_id,$category_id,$policy=NL_DC_AUTO) {
		$result = null;
		if ($policy == NL_DC_AUTO){
			if ($dc->is_cache_enabled()) {
				$result = self::_get_virtual_category_params_by_cache($dc->cache(),$asset_id,$category_id);
				if ($result === FALSE ) {
					$result = self::_get_virtual_category_params_by_db($dc->db(),$asset_id,$category_id);
					self::_set_virtual_category_params_by_cache($dc->cache(),$asset_id,$category_id,$result);
				}
			}
			else {
				$result = self::_get_virtual_category_params_by_db($dc->db(),$asset_id,$category_id);
			}
		}elseif($policy == NL_DC_DB){
			$result = self::_get_virtual_category_params_by_db($dc->db(),$asset_id,$category_id);
		}elseif($policy == NL_DC_CACHE){
			$result = self::_get_virtual_category_params_by_cache($dc->cache(),$asset_id,$category_id);
		}

		return $result;
	}



	static private function _get_virtual_category_params_by_db($db,$asset_id,$category_id){
		$sql = ' select * from nns_virtual_category_content ' .
					'where '.
					'nns_category_id=\''.$category_id.'\' and '.
					'nns_media_asset_id=\''.$asset_id.'\' ';
		$result=nl_query_by_db($sql, $db);
		if (is_bool($result)) return $result;

		$result=np_array_rekey($result,'nns_key');
		
		return $result;
	}

	static private function _get_virtual_category_params_by_cache($cache,$asset_id,$category_id){
		if ($cache===NULL) return FALSE;

		return unserialize($cache->get('asset_virtual|#'.$asset_id.'|#'.$category_id));
	}

	static private function _set_virtual_category_params_by_cache($cache,$asset_id,$category_id,$data){
		if ($cache===NULL) return FALSE;
		return $cache->set('asset_virtual|#'.$asset_id.'|#'.$category_id,serialize($data),self::CACHE_TIME_OUT);
	}
	static public function delete_virtual_category_params_by_cache($dc,$asset_id,$category_id){
		if ($dc->cache()===NULL) return FALSE;
		return $dc->cache()->delete('asset_virtual|#'.$asset_id.'|#'.$category_id);
	}
}
?>