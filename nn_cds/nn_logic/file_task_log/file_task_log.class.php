<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/11/13 11:46
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_file_task_log extends nl_public
{
    static $base_table='nns_mgtvbk_file_log';

    static $arr_filed = array(
        'nns_id',
        'nns_type',
        'nns_org_id',
        'nns_task_type',
        'nns_task_id',
        'nns_task_name',
        'nns_action',
        'nns_url',
        'nns_content',
        'nns_result',
        'nns_notify_result_url',
        'nns_notify_result',
        'nns_notify_time',
        'nns_modify_time',
        'nns_desc',
        'nns_create_time',
        'nns_modify_time',
        'nns_again',
        'nns_notify_content',
        'nns_send_time',
        'nns_ex_info',
        'nns_cdn_retry_time',
        'nns_cdn_send_time',
        'nns_notify_fail_reason',
        'nns_ex_group',
    );

    /**
     * 修改EPGFile CDN直播频道队列
     * @param $dc
     * @param $params
     * @param $nns_id
     * @return array
     */
    static public function edit($dc,$params,$nns_id)
    {
        $params = self::make_nns_pre($params);
        if((is_string($nns_id) && strlen($nns_id) < 1) || (is_array($nns_id) && empty($nns_id)))
        {
            return self::return_data(1,'EPGFile注入CDN日志队列guid为空');
        }
        $params = self::except_useless_params(self::$arr_filed, $params);
        if(empty($params))
        {
            return self::return_data(1,'参数为空');
        }
        if(!isset($params['nns_modify_time']) || strlen($params['nns_modify_time'])<1)
        {
            $params['nns_modify_time'] = date("Y-m-d H:i:s");
        }
        $sql = self::make_update_sql(self::$base_table, $params,array('nns_id'=>$nns_id));
        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库执行失败'.$sql);
        }
        return self::return_data(0,'ok'.$sql);
    }

    /**
     * guid查询epg_file表的数据
     * @param $dc
     * @param $nns_id
     * @return array
     */
    static public function query_data_by_id($dc,$nns_id)
    {
        $sql="select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
        $result = nl_query_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库执行失败'.$sql);
        }
        $result = (isset($result[0]) && !empty($result[0]) && is_array($result[0])) ? $result[0] : null;
        return self::return_data(0,'ok'.$sql,$result);
    }

    /**
     * 查询file日志通过task_id
     * @param $dc
     * @param $nns_task_id
     * @param null $arr_action
     * @return array
     */
    static public function query_by_task_id($dc,$nns_task_id,$arr_action=NULL)
    {
        $str_action = '';
        if(is_array($arr_action) && !empty($arr_action))
        {
            $str_action = " and nns_action in('".implode("','", $arr_action)."')";
        }
        $sql="select * from " . self::$base_table . " where nns_task_id='{$nns_task_id}' {$str_action} order by nns_create_time desc";
        $result = nl_query_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库执行失败'.$sql);
        }
        return self::return_data(0,'ok',$result);
    }

    /**
     * 查询epg_file日志通过task_id
     * @param $dc
     * @param $nns_task_id
     * @param $task_type
     * @param $sp_id
     * @return array
     */
    static public function query_by_task_id_type($dc,$nns_task_id,$task_type,$sp_id)
    {
        $sql="select * from " . self::$base_table . " where nns_task_id='{$nns_task_id}' and nns_task_type='{$task_type}' and nns_org_id='{$sp_id}' and nns_action in('UPDATE','REGIST','DELETE') order by nns_create_time desc limit 1";
        $result = nl_query_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库执行失败'.$sql);
        }
        $result = (isset($result[0]) && is_array($result[0]) && !empty($result[0])) ? $result[0] : null;
        return self::return_data(0,'ok',$result);
    }

    /**
     * 添加CDN队列
     * @param $dc
     * @param $params
     * @return array
     */
    static public function add($dc,$params)
    {
        $params = self::make_nns_pre($params);
        if(!isset($params['nns_id']) || strlen($params['nns_id'])<1)
        {
            $params['nns_id'] = np_guid_rand();
        }
        $date_time = date("Y-m-d H:i:s");
        if(!isset($params['nns_create_time']) || strlen($params['nns_create_time'])<1)
        {
            $params['nns_create_time'] = $date_time;
        }
        if(!isset($params['nns_modify_time']) || strlen($params['nns_modify_time'])<1)
        {
            $params['nns_modify_time'] = $date_time;
        }
        $params = self::except_useless_params(self::$arr_filed, $params);
        if(empty($params))
        {
            return self::return_data(1,'参数为空');
        }
        $sql = self::make_insert_sql(self::$base_table, $params);
        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库执行失败'.$sql);
        }
        return self::return_data(0,'ok');
    }

    /**
     * 分集参数判断是否注入
     * @param $dc
     * @param $params
     * @return array
     */
    static public function check_task_exist($dc,$params)
    {
        if (empty($params) || !is_array($params))
        {
            return self::return_data(1,'参数错误');
        }
        $where = '';
        foreach ($params as $key=>$val)
        {
            $where .= "$key='{$val}' and ";
        }
        $where = rtrim($where," and ");
        if(!empty($where))
        {
            $where = " where " . $where;
        }
        $sql = "select * from " . self::$base_table . $where . " limit 1";
        $result = nl_query_by_db($sql, $dc->db());
        if(!is_array($result))
        {
            return self::return_data(1,'没有数据'.$sql);
        }
        return self::return_data(0,'ok',$result);
    }

    /**
     * 删除日志  通过 状态和日期，目前只支持删除节目单
     * @param $dc
     * @param $sp_id
     * @param $str_date
     * @return array
     */
    static public function del_by_date_and_state($dc,$sp_id,$str_date)
    {
        if(strlen($sp_id) <1)
        {
            return self::return_data(1,'删除的$sp_id为空');
        }
        if(strlen($str_date) <1)
        {
            return self::return_data(1,'删除的创建日期为空');
        }
        $sql = "delete from " . self::$base_table . " where nns_org_id='{$sp_id}' and nns_task_type='Schedule' and nns_result='[0]' and nns_notify_result='0' and nns_create_time <='{$str_date}'";
        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库执行失败'.$sql);
        }
        return self::return_data(0,'ok '.$sql);
    }
    /**
     * 根据条件获取C2日志数据
     * @param object $dc
     * @param array $params 查询条件
     * @param $since
     * @param $num
     * @return array(ret,reason,data_info)
     */
    static public function query_by_condition($dc,$params,$since=0,$num=0,$str_order = '')
    {
        if (empty($params) || !is_array($params))
        {
            return self::return_data(1,'参数错误');
        }
        $where = '';
        foreach ($params as $key=>$val)
        {
            $where .= "$key='{$val}' and ";
        }
        $where = rtrim($where," and ");
        if(!empty($where))
        {
            $where = " where " . $where;
        }

        if(!empty($str_order) && strlen($str_order) >0)
        {
            $where .= " {$str_order} ";
        }

        if (strlen($since) > 0 && strlen($num) > 0 && $num > 0)
        {
            $limit = ' limit ' . $since . ',' . $num;
        }
        else
        {
            $limit = '';
        }
        $sql = "select * from " . self::$base_table . $where . $limit;
        $result = nl_query_by_db($sql, $dc->db());
        if(!is_array($result))
        {
            return self::return_data(1,'没有数据'.$sql);
        }
        return self::return_data(0,'ok',$result);
    }

    /**
     * 根据条件查询并返回条数
     * @param $sp_id
     * @param $dc
     * @param null $filter
     * @param int $start
     * @param int $size
     * @return array
     */
    static public function get_file_log_list($dc, $sp_id,$filter=null,$start=0,$size=100){
        $db = $dc->db();
        $sql = "select  * from " . self::$base_table . " where nns_org_id='".$sp_id."' ";
        $sql_count = "select  count(*) from " . self::$base_table . " where nns_org_id='".$sp_id."' ";
        if($filter != null){
            $wh_arr = array();
            if(!empty($filter['nns_task_name'])){
                $wh_arr[] = " nns_task_name like '%".$filter['nns_task_name']."%'";
            }
            if(!empty($filter['nns_id'])){
                $wh_arr[] = " nns_id='".$filter['nns_id']."'";
            }
            if(!empty($filter['nns_task_type'])){
                $wh_arr[] = " nns_task_type='".$filter['nns_task_type']."'";
            }
            if(!empty($filter['nns_action'])){
                $wh_arr[] = " nns_action='".$filter['nns_action']."'";
            }

            if(!empty($filter['day_picker_start'])){
                $wh_arr[] = " nns_create_time >='".$filter['day_picker_start']."' ";
            }
            if(!empty($filter['day_picker_end'])){
                $wh_arr[] = " nns_create_time <='".$filter['day_picker_end']."' ";
            }
            if(!empty($filter['notify_time_start'])){
                $wh_arr[] = "  nns_notify_time >='".$filter['notify_time_start']."'";
            }
            if(!empty($filter['notify_time_end'])){
                $wh_arr[] = "  nns_notify_time <='".$filter['notify_time_end']."'";
            }
            if(strlen($filter['nns_cdn_send_time'])>0)
            {
                $wh_arr[] = " nns_cdn_send_time ='".$filter['nns_cdn_send_time']."'";
            }
            if(!empty($filter['nns_result'])){
                if($filter['nns_result'] == '[0]')
                {
                    $wh_arr[] = " nns_result like '[0]%'";
                }
                else
                {
                    $wh_arr[] = " nns_result not like '[0]%'";
                }
            }
            if(!empty($filter['nns_state'])){
                $state = $filter['nns_state'];
                switch($state){
                    case 'request_state_succ':
                        $wh_arr[] = " nns_result like '[0]%'";
                        break;
                    case 'request_state_fail':
                        $wh_arr[] = " nns_result like '[-1]%'";
                        break;
                    case 'notify_state_wait':
                        $wh_arr[] = " nns_notify_result='5' ";
                        break;
                    case 'notify_state_succ':
                        $wh_arr[] = " nns_notify_result='0'";
                        break;
                    case 'notify_state_fail':
                        $wh_arr[] = " nns_notify_result='-1'";
                        break;
                    case 'resend_fail':
                        $wh_arr[] = " nns_again=-1";
                        break;
                }

            }
            if(!empty($wh_arr))	$sql .= " and ".implode(" and ",$wh_arr);
            if(!empty($wh_arr))	$sql_count .= " and ".implode(" and ",$wh_arr);
        }
        $sql .= " order by nns_create_time desc ";
        $sql .= " limit $start,$size";
        $data = nl_db_get_all($sql,$db);
        $rows = nl_db_get_col($sql_count,$db);
        return array('data'=>$data,'rows'=>$rows);
    }
}