<?php
/**
 * 消息队列管理
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_message_group_list extends nl_public
{
	static $base_table='nns_message_group_list';
	static $arr_filed = array(
			'nns_id',
			'nns_message_group_id',
	        'nns_import_id',
			'nns_type',
	        'nns_video_id',
			'nns_state',
			'nns_desc',
			'nns_create_time',
			'nns_modify_time',
	        'nns_ext_url',
	);

	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function query_by_id($dc,$nns_id)
	{
		$sql = "select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
		return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function query_by_cp_id($dc,$nns_cp_id)
	{
	    $sql = "select * from " . self::$base_table . " where nns_cp_id='{$nns_cp_id}'";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 查询任务完成的数量
	 * @param unknown $dc
	 * @param unknown $message_group_id
	 */
	static public function query_state_last_count($dc,$message_group_id)
	{
	    $sql=" select count(*) as total from " . self::$base_table . " where nns_message_group_id='{$message_group_id}' and nns_state !=0 ";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result[0]['total']) && $result[0]['total'] >0) ? $result[0]['total'] : 0;
	    return self::return_data(0,'OK'.$sql,$result);
	}
	
	
	/**
	 * 查询任务完成的数量
	 * @param unknown $dc
	 * @param unknown $message_group_id
	 */
	static public function query_group_count($dc,$message_group_id)
	{
	    $sql=" select count(*) as total from " . self::$base_table . " where nns_message_group_id='{$message_group_id}'";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result[0]['total']) && $result[0]['total'] >0) ? $result[0]['total'] : 0;
	    return self::return_data(0,'OK'.$sql,$result);
	}
	
	/**
	 * 查询任务完成的数量
	 * @param unknown $dc
	 * @param unknown $message_group_id
	 */
	static public function query_group_all($dc,$message_group_id)
	{
	    $sql=" select * from " . self::$base_table . " where nns_message_group_id='{$message_group_id}'";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    return self::return_data(0,'OK'.$sql,$result);
	}
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function query_by_video_message_id($dc,$video_id,$message_id)
	{
	    $sql = "select * from " . self::$base_table . " where nns_message_group_id='{$message_id}' and nns_video_id='{$video_id}' limit 1";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result[0]) && !empty($result[0]) && is_array($result[0])) ? $result[0] : null;
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function query_by_ids($dc,$nns_id)
	{
	    $str_id = is_array($nns_id) ? implode("','", $nns_id) : $nns_id;
	    $sql = "select * from " . self::$base_table . " where nns_id in('{$nns_id}')";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 添加内容提供商数据
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function add($dc,$params)
	{
	    $params['nns_id'] = (isset($params['nns_id']) && !empty($params['nns_id'])) ? $params['nns_id'] : np_guid_rand();
		$params['nns_create_time'] = $params['nns_modify_time'] = date('Y-m-d H:i:s');
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$sql = self::make_insert_sql(self::$base_table, $params);
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 修改内容提供商数据
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function edit($dc,$params,$nns_id)
	{
		if(strlen($nns_id)<0)
		{
			return self::return_data(1,'内容提供商数据_id为空');
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$params['nns_modify_time'] = date("Y-m-d H:i:s");
		$sql = self::make_update_sql(self::$base_table, $params,array('nns_id'=>$nns_id));
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}

	/**
	 * 依据条件修改数据
	 * @param unknown $dc
	 * @param unknown $edit_params
	 * @param unknown $where_params
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 */
	static public function edit_by_params($dc,$edit_params,$where_params)
	{
	    $edit_params = self::except_useless_params(self::$arr_filed, $edit_params);
	    $where_params = self::except_useless_params(self::$arr_filed, $where_params);
	    if(empty($edit_params) || !is_array($edit_params))
	    {
	        return self::return_data(1,'修改参数非法'.var_export($edit_params,true));
	    }
	    if(empty($where_params) || !is_array($where_params))
	    {
	        return self::return_data(1,'条件参数非法'.var_export($where_params,true));
	    }
	    $edit_params['nns_modify_time'] = date("Y-m-d H:i:s");
	    $temp_where_params = $temp_edit_params = array();
	    foreach ($edit_params as $key=>$val)
	    {
	        $temp_edit_params[] = "{$key}='{$val}'";
	    }
	    foreach ($where_params as $key=>$val)
	    {
	        $temp_where_params[] = "{$key}='{$val}'";
	    }
	    $sql = "update " . self::$base_table . " set " . implode(',', $temp_edit_params) . " where " . implode(" and ", $temp_where_params);
	    $result = nl_execute_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库执行失败'.$sql);
	    }
	    return self::return_data(0,'ok'.$sql);
	}
	
	/**
	 * 查询内容提供商数据列表
	 * @param object $dc 数据库对象
	 * @param array $params 查询参数
	 * @param array $page_info 分页信息
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function query($dc,$params,$page_info=null)
	{
		if(isset($params['where']) && !empty($params['where']))
		{
			foreach ($params['where'] as $where_key=>$where_val)
			{
				self::$str_where.=" {$where_key} = '{$where_val}' and ";
			}	
		}
		if(isset($params['like']) && !empty($params['like']))
		{
			foreach ($params['like'] as $like_key=>$like_val)
			{
				self::$str_where.=" {$like_key} like '%{$like_val}%' and ";
			}
		}
		self::$str_where = (strlen(self::$str_where) > 5) ? trim(self::$str_where,'and ') : '';
		$str_limit = self::make_page_limit($page_info);
		$sql="select * from " . self::$base_table . " " . self::$str_where . " order by nns_create_time desc {$str_limit} ";
		$sql_count = "select count(*) as count from " . self::$base_table . " " . self::$str_where;
		$result_count = nl_query_by_db($sql_count,  $dc->db());
		$result = nl_query_by_db($sql, $dc->db());
		self::init_where();
		if(!$result || !$result_count)
		{
			return self::return_data(1,"查询数据失败".$sql.$sql_count);
		}
		return self::return_data(0,"查询数据成功".$sql_count.$sql,$result,$result_count);
	}
	
	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function delete($dc,$nns_id)
	{
		if(strlen($nns_id) <1)
		{
			return self::return_data(1,'内容提供商数据_id为空');
		}
		$sql = "delete from " . self::$base_table . " where nns_id='{$nns_id}' ";
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		return self::return_data(0,'OK');
	}
}