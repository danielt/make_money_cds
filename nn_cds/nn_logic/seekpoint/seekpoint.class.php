<?php
/**
 * 打点操作类
 * @author LZ
 * created on 2014-04-21
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'public.class.php';
class nl_seekpoint extends nl_public {
    static $base_table = 'nns_vod_index_seekpoint';
	static $fields = array(
			'nns_id',
			'nns_name',
			'nns_image',
			'nns_begin',
			'nns_end',
			'nns_type',
			'nns_video_id',
			'nns_video_index',
            'nns_import_id',
            'nns_import_source',
            'nns_cp_id',
            'nns_fragment_type',
            'nns_deleted',
            'nns_vod_index_id'
	);

	/**
	 * 添加
	 * @param $dc
	 * @param $info = array(
	 * 'nns_video_id'=>'影片ID',
	 * 'nns_video_index'=>'影片分集号',
	 * 'nns_image'=>'图片地址',
	 * 'nns_type'=>'0片中，1片头，2片尾',
	 * 'nns_name' => '打点信息',
	 * 'nns_begin' => '开始时间秒',
	 * 'nns_end' => '结束时间秒'
	 * )
	 */
	static function add_seekpoint($dc,$info) {
		$info = nl_except_params($info, self::$fields);
		$info['nns_id']= np_guid_rand();
		$result = nl_db_insert($dc->db(), 'nns_vod_index_seekpoint', $info);
		if ($result === false) {
			return false;
		}
		if ($result === true) {
			return true;
		}
	}
	/**
	 * 修改
	 * @param $dc
	 * @param $params array(
	 *  要修改的数据
	 * )
	 * @param $where array(
	 *  'nns_id' => '打点ID',
	 *  'nns_video_id' => '影片ID',
	 *  'nns_video_index' => '分集号'
	 * )
	 */
	static function modify($dc, $params, $where) {
		//修改之前查看数据是否存在
		$selec = self::_select_data($dc, $where);
		if ($selec === true) {
			//数据不存在
			return 3;
		}
		if ($selec === 2) {
			//数据存在
			$str_sql=  self::_set_update_sql($dc, $params, 'nns_vod_index_seekpoint') .self::_set_where_sql($where);
			$result = nl_execute_by_db($str_sql, $dc->db());
			if ($result === false)
			{
				return false;
			}
			if ($result === true)
			{
				return true;
			}
		}
		if ($selec === false)
		{
			return false;
		}
	}
	/**
	 * 删除
	 * @param $dc
	 * @param $where = array(
	 * 条件
	 * )
	 */
	static function delete($dc,$where) {
		$where = nl_except_params($where, self::$fields);
		$sql = 'delete from nns_vod_index_seekpoint '.self::_set_where_sql($where);
		$result = nl_execute_by_db($sql, $dc->db());
		if ($result === false) {
			return false;
		}
		if ($result === true) {
			return true;
		}
	}



	/**
	 * 获取列表
	 * @param $dc
	 * @param $params = array() 查询数组
	 * @param $since,$num
	 */
    static function query($dc, $params, $since = null, $num = null) {
    	$params = nl_except_params($params, self::$fields);
		$where = null;
		if (is_array($params) and !empty($params)) {
			$and = 'and';
			foreach ($params as $key => $val) {
				if (strlen($val) != 0) {
					if($key == 'nns_id') {
						$id_arr = explode(',', $val);
						$str = "";
						foreach($id_arr as $k=>$value) {
							if(strlen($value) != 0) {
								$str .= "'".$value."',";
							}
						}
						$str = rtrim($str,',');
						$where .= " {$and} {$key} in ({$str}) ";
					} else {
						$where .= " {$and} {$key}='{$val}' ";
					}					
				}
			}
		}
			
		if (strlen($since) > 0 && strlen($num) > 0) {
			$limit = ' limit ' . $since . ',' . $num;
		} else {
			$limit = '';
		}
		//-->start xingcheng.hu 去掉where1=1
		if(!empty($where))
		{
			$where_tem = 'where ';
			$where = ltrim($where);
			$where = ltrim($where,'and');
			$where_tem .= $where;
			$where = $where_tem;
		}
		//<--end
		$sql = 'select * from nns_vod_index_seekpoint ' . $where . $limit;
		$result = nl_query_by_db($sql, $dc->db());
		if ($result === false) {
			return false;
		}
		if (is_array($result)) {
			//统计条数
			if (strlen($since) > 0 && strlen($num) > 0) {
				$sql_nums = 'select  count(1) as num from nns_vod_index_seekpoint ' . $where;
				$nums = nl_query_by_db($sql_nums, $dc->db());
				$arr = array(
						'pager' => array(
								'count' => $nums[0]['num'],
								'size' => $num,
								'num' => ($since / $num) + 1
						),
						'item' => $result
				);
				//var_dump($arr);die;
				return $arr;
			}
			return $result;
		}
		if ($result === true) {
			return true;
		}
    }
	//查询指定数据是否存在
	static function _select_data($dc, $where) {
		$where_sql = self::_set_where_sql($where,true);
		$sql_num='select nns_id from nns_vod_index_seekpoint'.$where_sql;
		$result = nl_query_by_db($sql_num, $dc->db());
		if ($result === false) {
			return false;
		}
		if (is_array($result)) {
			return 2;
		}
		if ($result === true) {
			return true;
		}
	}
	static private function _set_update_sql($dc, $params, $table_name)
	{
		$upd_sql = 'UPDATE `'.$table_name.'` SET ';
		foreach($params as $key=>$val)
		{
			$sql_arr[] = $key.'=\''.$val.'\'';
		}
		$upd_sql .= implode(',',$sql_arr);
		return $upd_sql;
	}
	/*
	 * @param array $where 条件
	 * @param bollean $if_and and还是or，默认采用and
	 * @return string $where_sql;
	 */
	static private function _set_where_sql($where,$if_and = true)
	{
	
		$cate = $if_and?' and ':' or ';
		$where_sql = ' where ';
		foreach ($where as $key=>$val)
		{
			$where_sql .= $key.'=\''.$val.'\''.$cate;
		}
		$where_sql =  rtrim($where_sql,$cate);
	
		return $where_sql;
	}
	/**
	 * 独立于业务 or cms
	 * @param $dc
	 * @param $video_id  影片ID
	 * @param $video_index 影片分集号 默认为0
	 */
	static function get_seekpoint_by_video_index($dc,$video_id,$video_index=null) {
		$where = array();		
		if(strlen($video_id) != 0)
			$where['nns_video_id'] = $video_id;
		if(strlen($video_index) != 0) 
			$where['nns_video_index'] = $video_index;
		if(empty($where)) return $where;
		$where_sql = self::_set_where_sql($where,true);
		$sql = 'select * from nns_vod_index_seekpoint'.$where_sql;
		$result = nl_query_by_db($sql, $dc->db());
		if (is_array($result)) {
			return $result;
		} else {
			return array();
		}
	}
	
	static function get_seekpoint_info_group_by_video_id($dc,$flag=true,$int_start=0,$int_limit=10)
	{
		if($flag)
		{
			$int_start = (int) $int_start;
			$int_limit = (int) $int_limit;
			$str_start = $int_start*$int_limit;
			$sql="select nns_video_id from nns_vod_index_seekpoint group by nns_video_id limit {$str_start},{$int_limit}";
		}
		else
		{
			$sql="select count(*) as num from (select nns_video_id as num from nns_vod_index_seekpoint group by nns_video_id) as temp";
		}
		return nl_query_by_db($sql, $dc->db());
	}
	
	static function get_seekpoint_info_by_video_id($dc,$video_id)
	{
		$sql="select nns_video_index from nns_vod_index_seekpoint where nns_video_id='{$video_id}' group by nns_video_index ";
		return nl_query_by_db($sql, $dc->db());
	}
	
	static function get_seekpoint_info_by_video_index_id($dc,$video_id,$index)
	{
		$sql="select seek.*,inde.nns_id as nns_index_id from nns_vod_index_seekpoint as seek left join nns_vod_index as inde " .
		 " on inde.nns_vod_id=seek.nns_video_id and inde.nns_index=seek.nns_video_index where seek.nns_video_id='{$video_id}' and seek.nns_video_index='{$index}' ";
		return nl_query_by_db($sql, $dc->db());
	}



    /**
     * 依据条件查询打点信息
     * @param $dc
     * @param $params
     * @return array
     */
    static public function query_by_condition($dc, $params)
    {
        $params = nl_except_params($params,self::$fields);
        if (!is_array($params) || empty($params))
        {
            return self::return_data(1, '查询条件为空不允许查询');
        }
        $sql = self::make_query_sql(self::$base_table, $params);
        $result = nl_query_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (isset($result) && !empty($result)) ? $result : null;
        return self::return_data(0, 'OK' . $sql, $result);
    }


    /**
     * 修改打点信息
     * @param object $dc 数据库对象
     * @param array $params 数据数组
     * @param array|string $nns_id GUID
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     * @date 2016-09-24
     */
    static public function edit_v2($dc, $params, $nns_id)
    {
        $params = nl_except_params($params,self::$fields);
        if (isset($params['nns_id']))
        {
            unset($params['nns_id']);
        }
        $params = self::make_nns_pre($params);
        if (strlen($nns_id) < 0)
        {
            return self::return_data(1, '队列guid为空');
        }
        $params = self::except_useless_params(self::$fields, $params);
        if (empty($params))
        {
            return self::return_data(1, '参数为空');
        }
        if (!isset($params['nns_modify_time']) || strlen($params['nns_modify_time']) < 1)
        {
            $params['nns_modify_time'] = date("Y-m-d H:i:s");
        }
        $sql = self::make_update_sql(self::$base_table, $params, array('nns_id' => $nns_id));
        $result = nl_execute_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        return self::return_data(0, 'ok');
    }


    /**
     * 添加打点信息
     * @param object $dc 数据库对象
     * @param array $params 数据数组
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     * @date 2016-03-06
     */
    static public function add_v2($dc, $params)
    {
        $params = nl_except_params($params,self::$fields);
        if (empty($params) || !is_array($params))
        {
            return self::return_data(1, '参数为空');
        }
        $params['nns_create_time'] = $params['nns_modify_time'] = date('Y-m-d H:i:s');
        $sql = self::make_insert_sql(self::$base_table, $params);
        $result = nl_execute_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        return self::return_data(0, 'ok:' . $sql);
    }
    /**
     * 根据打点信息ID获取打点信息
     * @param $dc
     * @param $arr_id 打点ID一维数组
     * @return array
     */
    static public function query_seekpoint_by_id($dc,$arr_id)
    {
        if(empty($arr_id))
        {
            return self::return_data(1,'参数错误');
        }
        $str_ids = implode("','",$arr_id);
        $sql = "select * from " . self::$base_table . " where nns_id in ('{$str_ids}')";
        $seekpoint_re = nl_query_by_db($sql,$dc->db());
        if(!is_array($seekpoint_re))
        {
            return self::return_data(1,'数据库执行失败或数据查询为空：'.$sql);
        }
        $result = array();
        foreach ($seekpoint_re as $info)
        {
            $result[$info['nns_id']]['base_info'] = $info;
        }
        if(empty($result))
        {
            return self::return_data(1,'vod_index_seekpoint数据查询结果为空');
        }
        return self::return_data(0,'ok'.$sql,$result);
    }

}