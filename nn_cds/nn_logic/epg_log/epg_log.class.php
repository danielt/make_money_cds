<?php
/**
 * 媒资上线管理
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_epg_log extends nl_public
{
	static $base_table='nns_mgtvbk_import_epg_log';
	
	static $arr_filed = array(
			'nns_id',
			'nns_video_id',
			'nns_video_type',
			'nns_org_id',
			'nns_create_time',
			'nns_action',
			'nns_status',
			'nns_reason',
			'nns_data',
			'nns_name',
			'nns_file_dir',
	);
	
	/**
	 * 修改CDN直播频道队列
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @param string $nns_id GUID
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function edit($dc,$params,$nns_id)
	{
	    $params = self::make_nns_pre($params);
	    if((is_string($nns_id) && strlen($nns_id) <1) || (is_array($nns_id) && !empty($nns_id)))
	    {
	        return self::return_data(1,'CDN直播频道队列guid为空');
	    }
	    $params = self::except_useless_params(self::$arr_filed, $params);
	    if(empty($params))
	    {
	        return self::return_data(1,'参数为空');
	    }
	    if(!isset($params['nns_modify_time']) || strlen($params['nns_modify_time'])<1)
	    {
	        $params['nns_modify_time'] = date("Y-m-d H:i:s");
	    }
	    $sql = self::make_update_sql(self::$base_table, $params,array('nns_id'=>$nns_id));
	    $result = nl_execute_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库执行失败'.$sql);
	    }
	    return self::return_data(0,'ok'.$sql);
	}
	
	
	/**
	 * guid查询C2 表的 数据
	 * @param unknown $dc
	 * @param unknown $nns_id
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据'), 
	 */
	static public function query_data_by_id($dc,$nns_id)
	{
		$sql="select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		$result = (isset($result[0]) && !empty($result[0]) && is_array($result[0])) ? $result[0] : null; 
		return self::return_data(0,'ok'.$sql,$result);
	}
	
	/**
	 * 查询c2日志通过task_id
	 * @param unknown $dc
	 * @param unknown $nns_task_id
	 */
	static public function query_by_task_id($dc,$nns_task_id)
	{
	    $sql="select * from " . self::$base_table . " where nns_task_id='{$nns_task_id}' order by nns_create_time desc";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库执行失败'.$sql);
	    }
	    return self::return_data(0,'ok',$result);
	}
	
// 	static public function add($dc,$c2_log)
// 	{
// 		$c2_log['nns_type'] = 'std';
// 		$c2_log['nns_org_id'] = $sp_id;
// 		$dt = date('Y-m-d H:i:s');
// 		$c2_log['nns_create_time'] = $dt;
// 		$c2_log['nns_send_time'] = $dt;
// 	}
	
	
	
	/**
	 * 添加CDN直播频道队列
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function add($dc,$params)
	{
		$params = self::make_nns_pre($params);
		if(!isset($params['nns_id']) || strlen($params['nns_id'])<1)
		{
			$params['nns_id'] = np_guid_rand();
		}
		$date_time = date("Y-m-d H:i:s");
		if(!isset($params['nns_create_time']) || strlen($params['nns_create_time'])<1)
		{
			$params['nns_create_time'] = $date_time;
		}
		if(!isset($params['nns_modify_time']) || strlen($params['nns_modify_time'])<1)
		{
		    $params['nns_modify_time'] = $date_time;
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$sql = self::make_insert_sql(self::$base_table, $params);
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 删除日志  通过 状态和日期，目前只支持删除节目单
	 * @param unknown $dc
	 * @param unknown $sp_id
	 * @param unknown $str_date
	 */
	static public function del_by_date_and_state($dc,$sp_id,$str_date)
	{
	    if(strlen($sp_id) <1)
	    {
	        return self::return_data(1,'删除的$sp_id为空');
	    }
	    if(strlen($str_date) <1)
	    {
	        return self::return_data(1,'删除的创建日期为空');
	    }
	    $sql = "delete from " . self::$base_table . " where nns_org_id='{$sp_id}' and nns_video_type='playbill' and nns_status='0' and nns_create_time <='{$str_date}'";
	    $result = nl_execute_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库执行失败'.$sql);
	    }
	    return self::return_data(0,'ok '.$sql);
	}
}