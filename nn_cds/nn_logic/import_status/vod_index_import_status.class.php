<?php
/**
 * 分集注入状态
 * @author zhiyong.luo
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_vod_index_import_status extends nl_public
{
	static public $table = "nns_vod_index_import_status";
	static public $vod_index_bind_table = "nns_vod_index_bind";
	/**
	 * 添加分集注入状态
	 * @param object $dc
	 * @param array $params
	 * @author zhiyong.luo 
	 * @date 2017-06-01
	 */
	static public function add($dc,$params)
	{
		if(empty($params) || !is_array($params))
		{
			return self::return_data(1,'参数错误');
		}
		$params['nns_id'] = np_guid_rand();
		$params['nns_create_time'] = date('Y-m-d H:i:s',time());
		$params['nns_modify_time'] = $params['nns_create_time'];
		$result = nl_db_insert($dc->db(), self::$table, $params);
		if($result)
		{
			return self::return_data(0,'OK');
		}
		else
		{
			self::return_data(1,'mysql execute failed');
		}
	}
	/**
	 * 修改分集注入状态
	 * @param object $dc
	 * @param array $params
	 * @param array $where
	 * @author zhiyong.luo 
	 * @date 2017-06-01
	 */
	static public function modify($dc,$params,$where)
	{
		if(empty($where) || !is_array($where) || empty($params) || !is_array($params))
		{
			return self::return_data(1,'参数错误');
		}
		$update_str = '';
		$where_str = '';
		$params['nns_modify_time'] = date('Y-m-d H:i:s',time());
		foreach ($params as $key=>$value)
		{
			$update_str .= " {$key} = '{$value}',";
		}
		$update_str = rtrim($update_str,',');
		foreach ($where as $k=>$val)
		{
			if($k == 'in' && is_array($val))
			{
				foreach ($val as $l => $v)
				{
					$where_str .= " {$l} in ('" . implode("'.'", $v) . "') AND ";
				}
			}
			elseif(!empty($val) || $val == '0')
			{
				$where_str .= " {$k} = '{$val}' AND ";
			}			
		}
		$where_str = rtrim($where_str,' AND ');
		$sql = "update " . self::$table . " set " . $update_str . " where " . $where_str;
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'失败'.$sql);
		}
		return self::return_data(0,'成功');
	}
	/**
	 * 获取分集注入状态
	 * @param object $dc
	 * @param array $params
	 * @param string $since
	 * @param string $num
	 * @param boolean $is_count
	 * @author zhiyong.luo 
	 * @date 2017-06-01
	 */
	static public function get_list($dc,$params,$since=null,$num=null,$is_count=false)
	{
		$where = '';
		$str_limit = '';
		if(is_array($params))
		{
			foreach ($params as $key=>$value)
			{
				if($key == 'in' && is_array($value))
				{
					foreach ($value as $l => $v)
					{
						$where .= " {$l} in ('" . implode("'.'", $v) . "') AND ";
					}
				}
				elseif(strlen($value) > 0)
				{
					$where .= " {$key} = '{$value}' AND ";
				}
			}
		}
		if(!empty($where))
		{
			$where = rtrim($where," AND ");
			$where = " where " . $where;
		}
		if(strlen($since) > 0 && strlen($num) > 0)
		{
			$str_limit = " limit {$since},{$num}";
		}
		$sql = "select * from " . self::$table . $where . " order by nns_create_time desc " . $str_limit;
		$data = nl_query_by_db($sql, $dc->db());
		if($data === false)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		if($data === true)
		{
			$data = array();
		}
		$count_info = array();
		if($is_count)
		{
			$count_sql = "select count(1) as count from " . self::$table . $where;
			$count_info = nl_query_by_db($count_sql, $dc->db());
			if($count_info === false)
			{
				return self::return_data(1,'数据库执行失败'.$str_sql);
			}
		}
		return self::return_data(0,'ok',$data,$count_info);
	}
	/**
	 * 根据主媒资信息ID获取分集注入状态列表
	 * @param object $dc
	 * @param string $vod_id
	 * @author zhiyong.luo 
	 * @date 2017-06-01
	 */
	static public function get_list_by_vod_id($dc,$vod_id,$since=null,$num=null,$is_count=false)
	{
		$sql = "select i.*,b.nns_type from nns_vod_index_import_status as i left join nns_vod_index_bind as b on i.nns_index_id=b.nns_index_id where b.nns_vod_id='{$vod_id}'  and b.nns_deleted='0' order by b.nns_index,i.nns_create_time desc";
		if(strlen($since) > 0 && strlen($num) > 0)
		{
			$sql .= " limit {$since},{$num}";
		}
		$data = nl_query_by_db($sql, $dc->db());
		if($data === false)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		if($data === true)
		{
			$data = array();
		}
		if($is_count)
		{
			$count_sql = "select count(1) from nns_vod_index_import_status as i left join nns_vod_index_bind as b on i.nns_index_id=b.nns_index_id where b.nns_vod_id='{$vod_id}'  and b.nns_deleted='0'";
			$count_info = nl_query_by_db($count_sql, $dc->db());
			if($count_info === false)
			{
				return self::return_data(1,'数据库执行失败'.$str_sql);
			}
		}
		return self::return_data(0,'ok',$data,$count_info);
	}
}