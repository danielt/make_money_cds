<?php
/**
 * 明星库
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2017/12/13 15:35
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_actor extends nl_public
{
    static $arr_filed = array(
        'nns_id',
        'nns_name',
        'nns_import_id',
        'nns_import_source',
        'nns_cp_id',
        'nns_area',
        'nns_alias_name',
        'nns_profession',
        'nns_pinyin',
        'nns_works',
        'nns_info',
        'nns_label_id',
        'nns_image_v',
        'nns_image_s',
        'nns_image_h',
        'nns_create_time',
        'nns_modify_time',
        'nns_old_name',
        'nns_eng_name',
        'nns_country',
        'nns_view_len',
        'nns_sex',
        'nns_full_pinyin',
    );
    static $base_table='nns_actor';
    static $base_table_ex='nns_actor_ex';

    /**
     * 扩展信息表字段
     * @var array
     */
    static $arr_filed_ex = array(

    );

    /**
     * @description:添加明星
     * @author:xinxin.deng
     * @date: 2017/12/13 15:48
     * @param $dc
     * @param $params
     * @return array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    static public function add($dc,$params)
    {
        $params = self::make_nns_pre($params);
        if(!isset($params['nns_id']) || strlen($params['nns_id'])<1)
        {
            $params['nns_id'] = np_guid_rand();
        }
        $date_time = date("Y-m-d H:i:s");
        if(!isset($params['nns_create_time']) || strlen($params['nns_create_time'])<1)
        {
            $params['nns_create_time'] = $date_time;
        }
        if(!isset($params['nns_modify_time']) || strlen($params['nns_modify_time'])<1)
        {
            $params['nns_modify_time'] = $date_time;
        }
        $params = self::except_useless_params(self::$arr_filed, $params);
        if(empty($params))
        {
            return self::return_data(1,'参数为空');
        }
        $sql = self::make_insert_sql(self::$base_table, $params);
        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库执行失败'.$sql);
        }
        return self::return_data(0,'ok');
    }
    /**
     * @description:删除明星
     * @author:xinxin.deng
     * @date: 2017/12/5 14:07
     * @param $dc
     * @param $params
     * @return array
     */
    static public function del($dc,$params)
    {
        $params = self::make_nns_pre($params);
        if(empty($params) || !is_array($params))
        {
            return self::return_data(1,'参数为空');
        }
        $sql = self::make_delete_sql(self::$base_table,$params);
        if(strlen($sql) <1)
        {
            return self::return_data(1,'数据组装sql为空，不允许删除');
        }
        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库查询失败,sql:'.$sql);
        }
        return self::return_data(0,'OK'.$sql);
    }

    /**
     * @description:修改明星
     * @author:xinxin.deng
     * @date: 2017/12/13 15:49
     * @param object $dc 数据库对象
     * @param array $params 数据数组
     * @param string $nns_id GUID
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    static public function edit($dc,$params,$nns_id)
    {
        $params = self::make_nns_pre($params);
        if(strlen($nns_id)<0)
        {
            return self::return_data(1,'CDN明星队列guid为空');
        }
        $params = self::except_useless_params(self::$arr_filed, $params);
        if(empty($params))
        {
            return self::return_data(1,'参数为空');
        }
        if(!isset($params['nns_modify_time']) || strlen($params['nns_modify_time'])<1)
        {
            $params['nns_modify_time'] = date("Y-m-d H:i:s");
        }
        $sql = self::make_update_sql(self::$base_table, $params,array('nns_id'=>$nns_id));
        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库执行失败'.$sql);
        }
        return self::return_data(0,'ok');
    }

    /**
     * @description:依据条件查询明星信息
     * @author:xinxin.deng
     * @date: 2017/12/13 15:50
     * @param $dc
     * @param $params
     * @return array
     */
    static public function query_by_condition($dc,$params)
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if(!is_array($params) || empty($params))
        {
            return self::return_data(1,'查询条件为空不允许查询');
        }
        $sql = self::make_query_sql(self::$base_table,$params);
        $result = nl_query_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库查询失败,sql:'.$sql);
        }
        $result = (isset($result) && !empty($result)) ? $result : null;
        return self::return_data(0,'OK'.$sql,$result);
    }

    /**
     * 删除扩展信息
     * @param $dc
     * @param $params
     * @return array
     */
    static public function del_ex_by_condition($dc,$params)
    {
        $params = self::make_nns_pre($params);
        if(empty($params) || !is_array($params))
        {
            return self::return_data(1,'参数为空');
        }
        $sql = self::make_delete_sql(self::$base_table_ex,$params);
        if(strlen($sql) <1)
        {
            return self::return_data(1,'数据组装sql为空，不允许删除');
        }
        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库查询失败,sql:'.$sql);
        }
        return self::return_data(0,'OK'.$sql);
    }


    /**
     * @description:更新扩展信息
     * @author:xinxin.deng
     * @date: 2017/12/5 15:01
     * @param $dc
     * @param $params
     * @return array
     */
    static public function update_ex_info($dc,$params, $where)
    {
        $params = self::make_nns_pre($params);
        if(empty($params) || !is_array($params))
        {
            return self::return_data(1,'参数为空');
        }
        $sql = self::make_update_sql(self::$base_table_ex,$params, $where);
        if(strlen($sql) <1)
        {
            return self::return_data(1,'数据组装sql为空，不允许删除');
        }
        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库查询失败,sql:'.$sql);
        }
        return self::return_data(0,'OK'.$sql);
    }

    /**
     * @description:向扩展表添加数据
     * @author:xinxin.deng
     * @date: 2017/12/5 15:18
     * @param $dc
     * @param $params
     * @return array
     */
    static public function add_ex($dc,$params)
    {
        $params = self::make_nns_pre($params);
        $params = self::except_useless_params(self::$arr_filed_ex, $params);
        if(empty($params))
        {
            return self::return_data(1,'参数为空');
        }
        $sql = self::make_insert_sql(self::$base_table_ex, $params);
        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库执行失败'.$sql);
        }
        return self::return_data(0,'ok');
    }

    /**
     * @description:查询扩展
     * @author:xinxin.deng
     * @date: 2017/12/5 14:07
     * @param $dc
     * @param $params
     * @return array
     */
    static public function query_ex($dc,$params)
    {
        $params = self::make_nns_pre($params);
        if(empty($params) || !is_array($params))
        {
            return self::return_data(1,'参数为空');
        }
        $sql = self::make_query_sql(self::$base_table_ex,$params);
        if(strlen($sql) <1)
        {
            return self::return_data(1,'数据组装sql为空，不允许查询');
        }
        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库查询失败,sql:'.$sql);
        }
        return self::return_data(0,'OK'.$sql);
    }

}
