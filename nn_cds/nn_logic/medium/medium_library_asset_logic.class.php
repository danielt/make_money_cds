<?php
/**
 * Created by PhpStorm.
 * Use : 介质库管理-介质库CP管理
 * User: kan.yang@starcor.cn
 * Date: 18-3-23
 * Time: 下午8:25
 */

include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'medium_library_base_logic.class.php';
class medium_library_asset_logic extends medium_library_base_logic
{
    /**
     * 初始化函数
     */
    public static function init()
    {
        parent::$base_table='nns_medium_library_asset';
        parent::$arr_filed = array(
            'nns_id',
            'nns_cp_id',
            'nns_disk_id',
            'nns_asset_id',
            'nns_asset_name',
            'nns_asset_path',
            'nns_asset_type',
            'nns_asset_summary',
            'nns_create_time',
            'nns_modify_time',
            'nns_state',
        );
        parent::$arr_query_filed = array(
            'nns_id'         => array('str'),
            'nns_cp_id'      => array('str'),
            'nns_disk_id'    => array('str'),
            'nns_asset_id'   => array('str'),
            'nns_asset_name' => array('like'),
            'nns_asset_path' => array('like'),
            'nns_asset_type' => array('int'),
            'nns_asset_summary' => array('like'),
            'nns_start_time' => array('>='),
            'nns_end_time'   => array('<='),
            'nns_state'      => array('int'),
        );
    }

    /**
     * 添加介质库CP列表
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_params array(
            'nns_cp_id',          => 'CP ID',
            'nns_disk_id',        => '磁盘ID',
            'nns_asset_name',     => '分类名称',
            'nns_asset_path',     => '分类地址名称',
            'nns_asset_type',     => '分类类型。0单集；1多集',
            'nns_asset_summary',  => '分类描述',
     * )
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function add($obj_dc,$arr_params)
    {
        self::init();
        return parent::add($obj_dc,$arr_params);
    }

    /**
     * 更新介质库CP列表
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_update_where  array(  均为可选
            'nns_id',             => '主键',
            'nns_cp_id',          => 'CP ID',
            'nns_disk_id',        => '磁盘ID',
            'nns_asset_name',     => '分类名称',
            'nns_asset_path',     => '分类地址名称',
            'nns_asset_type',     => '分类类型。0单集；1多集',
            'nns_state',            => '状态。0启用；1禁用',
     * )
     * @param array  $arr_update_params array(  均为可选
            'nns_cp_id',          => 'CP ID',
            'nns_disk_id',        => '磁盘ID',
            'nns_asset_name',     => '分类名称',
            'nns_asset_path',     => '分类地址名称',
            'nns_asset_type',     => '分类类型。0单集；1多集',
            'nns_asset_summary',  => '分类描述',
     * )
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function edit($obj_dc,$arr_update_where,$arr_update_params)
    {
        self::init();
        return parent::edit($obj_dc,$arr_update_where,$arr_update_params);
    }

    /**
     * 替换介质库分类列表
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_params array(
            'nns_id',             => '主键 ID',
            'nns_cp_id',          => 'CP ID',
            'nns_disk_id',        => '磁盘ID',
            'nns_asset_name',     => '分类名称',
            'nns_asset_path',     => '分类地址名称',
            'nns_asset_type',     => '分类类型。0单集；1多集',
            'nns_asset_summary',  => '分类描述',
     * )
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function replace($obj_dc,$arr_params)
    {
        self::init();
        return parent::replace($obj_dc,$arr_params);
    }

    /**
     * 删除介质库CP
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_del_where array(
            'nns_id',             => '主键',
            'nns_cp_id',          => 'CP ID',
            'nns_disk_id',        => '磁盘ID',
            'nns_asset_name',     => '分类名称',
            'nns_asset_path',     => '分类地址名称',
            'nns_asset_type',     => '分类类型。0单集；1多集',
            'nns_state',            => '状态。0启用；1禁用',
     * )
     * @param bool   $bool_sys_where   是否系统拼接查询语句查询条件
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function del($obj_dc,$arr_del_where,$bool_sys_where = true)
    {
        self::init();
        return parent::del($obj_dc,$arr_del_where);
    }

    /**
     * 查询介质库CP列表
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_query_params array(
            'nns_id',             => '主键',
            'nns_cp_id',          => 'CP ID',
            'nns_disk_id',        => '磁盘ID',
            'nns_asset_name',     => '分类名称',
            'nns_asset_path',     => '分类地址名称',
            'nns_asset_type',     => '分类类型。0单集；1多集',
            'nns_state',            => '状态。0启用；1禁用',
     * )
     * @param array $arr_limit
     * @param string $str_order
     * @param string $str_group
     * @param bool   $bool_sys_where   是否系统拼接查询语句查询条件
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function query_list($obj_dc,$arr_query_params,$arr_limit = array(0,10),$str_order = 'nns_modify_time desc',$str_group = '',$bool_sys_where = true)
    {
        self::init();
        return parent::query_list($obj_dc,$arr_query_params,$arr_limit,$str_order,$str_group,$bool_sys_where);
    }

    /**
     * 查询磁盘列表
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_query_params array(
            'nns_id',             => '主键',
            'nns_cp_id',          => 'CP ID',
            'nns_disk_id',        => '磁盘ID',
            'nns_asset_name',     => '分类名称',
            'nns_asset_path',     => '分类地址名称',
            'nns_asset_type',     => '分类类型。0单集；1多集',
            'nns_state',            => '状态。0启用；1禁用',
     * )
     * @param bool   $bool_sys_where   是否系统拼接查询语句查询条件
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function query_one($obj_dc,$arr_query_params,$bool_sys_where = true)
    {
        self::init();
        return parent::query_one($obj_dc,$arr_query_params,$bool_sys_where);
    }
}