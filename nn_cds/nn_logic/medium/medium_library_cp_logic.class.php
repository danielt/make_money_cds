<?php
/**
 * Created by PhpStorm.
 * Use : 介质库管理-介质库CP管理
 * User: kan.yang@starcor.cn
 * Date: 18-3-22
 * Time: 下午4:40
 */

include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'medium_library_base_logic.class.php';
class medium_library_cp_logic extends medium_library_base_logic
{
    /**
     * 初始化函数
     */
    public static function init()
    {
        parent::$base_table='nns_medium_library_cp';
        parent::$arr_filed = array(
            'nns_id',
            'nns_cp_name',
            'nns_contactor',
            'nns_telephone',
            'nns_email',
            'nns_addr',
            'nns_cp_summary',
            'nns_image_h',
            'nns_image_v',
            'nns_image_s',
            'nns_create_time',
            'nns_modify_time',
            'nns_state'
        );
        parent::$arr_query_filed = array(
            'nns_id'         => array('str'),
            'nns_cp_name'    => array('like'),
            'nns_contactor'  => array('like'),
            'nns_telephone'  => array('str'),
            'nns_email'      => array('str'),
            'nns_addr'       => array('like'),
            'nns_cp_summary' => array('like'),
            'nns_image_h'    => array('str'),
            'nns_image_v'    => array('str'),
            'nns_image_s'    => array('str'),
            'nns_start_time' => array('>='),
            'nns_end_time'   => array('<='),
            'nns_state'      => array('int'),
        );
    }

    /**
     * 添加介质库CP列表
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_params array(
            'nns_cp_name',          => 'CP名称',
            'nns_contactor',        => '联系人',
            'nns_telephone',        => '电话',
            'nns_email',            => 'Email',
            'nns_addr',             => '地址',
            'nns_cp_summary',       => 'CP描述',
            'nns_image_h',          => '横海报'
            'nns_image_v',          => '竖海报'
            'nns_image_s',          => '方海报'
     * )
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function add($obj_dc,$arr_params)
    {
        self::init();
        return parent::add($obj_dc,$arr_params);
    }

    /**
     * 更新介质库CP列表
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_update_where  array(  均为可选
            'nns_id',               => 'CP ID',
            'nns_cp_name',          => 'CP名称',
            'nns_contactor',        => '联系人',
            'nns_telephone',        => '电话',
            'nns_email',            => 'Email'
     * )
     * @param array  $arr_update_params array(  均为可选
            'nns_cp_name',          => 'CP名称',
            'nns_contactor',        => '联系人',
            'nns_telephone',        => '电话',
            'nns_email',            => 'Email',
            'nns_addr',             => '地址',
            'nns_cp_summary',       => 'CP描述',
            'nns_image_h',          => '横海报'
            'nns_image_v',          => '竖海报'
            'nns_image_s',          => '方海报'
     * )
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function edit($obj_dc,$arr_update_where,$arr_update_params)
    {
        self::init();
        return parent::edit($obj_dc,$arr_update_where,$arr_update_params);
    }

    /**
     * 替换介质库CP列表
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_params array(
            'nns_cp_name',          => 'CP名称',
            'nns_contactor',        => '联系人',
            'nns_telephone',        => '电话',
            'nns_email',            => 'Email',
            'nns_addr',             => '地址',
            'nns_cp_summary',       => 'CP描述',
            'nns_image_h',          => '横海报'
            'nns_image_v',          => '竖海报'
            'nns_image_s',          => '方海报'
     * )
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function replace($obj_dc,$arr_params)
    {
        self::init();
        return parent::replace($obj_dc,$arr_params);
    }

    /**
     * 删除介质库CP
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_del_where array(
            'nns_id',               => 'CP ID',
            'nns_cp_name',          => 'CP名称',
            'nns_contactor',        => '联系人',
            'nns_telephone',        => '电话',
            'nns_email',            => 'Email',
            'nns_start_time',       => '创建开始时间',
            'nns_end_time',         => '创建结束时间',
            'nns_state',            => '状态。0启用；1禁用',
     * )
     * @param bool   $bool_sys_where   是否系统拼接查询语句查询条件
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function del($obj_dc,$arr_del_where,$bool_sys_where = true)
    {
        self::init();
        return parent::del($obj_dc,$arr_del_where);
    }

    /**
     * 查询介质库CP列表
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_query_params array(
            'nns_id',               => 'CP ID',
            'nns_cp_name',          => 'CP名称',
            'nns_contactor',        => '联系人',
            'nns_telephone',        => '电话',
            'nns_email',            => 'Email',
            'nns_start_time',       => '创建开始时间',
            'nns_end_time',         => '创建结束时间',
            'nns_state',            => '状态。0启用；1禁用',
     * )
     * @param array $arr_limit
     * @param string $str_order
     * @param string $str_group
     * @param bool   $bool_sys_where   是否系统拼接查询语句查询条件
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function query_list($obj_dc,$arr_query_params,$arr_limit = array(0,10),$str_order = 'nns_modify_time desc',$str_group = '',$bool_sys_where = true)
    {
        self::init();
        return parent::query_list($obj_dc,$arr_query_params,$arr_limit,$str_order,$str_group,$bool_sys_where);
    }

    /**
     * 查询磁盘列表
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_query_params array(
            'nns_id',               => 'CP ID',
            'nns_cp_name',          => 'CP名称',
            'nns_contactor',        => '联系人',
            'nns_telephone',        => '电话',
            'nns_email',            => 'Email',
            'nns_start_time',       => '创建开始时间',
            'nns_end_time',         => '创建结束时间',
            'nns_state',            => '状态。0启用；1禁用',
     * )
     * @param bool   $bool_sys_where   是否系统拼接查询语句查询条件
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function query_one($obj_dc,$arr_query_params,$bool_sys_where = true)
    {
        self::init();
        return parent::query_one($obj_dc,$arr_query_params,$bool_sys_where);
    }
}