<?php
/**
 * Created by PhpStorm.
 * Use : 介质库管理-磁盘管理
 * User: kan.yang@starcor.cn
 * Date: 18-3-22
 * Time: 下午4:40
 */

include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class medium_library_disk_logic extends nl_public
{
    static $base_table='nns_medium_library_disk';
    static $arr_filed = array(
        'nns_id',
        'nns_server_id',
        'nns_disk_name',
        'nns_disk_path',
        'nns_disk_extension',
        'nns_summary',
        'nns_create_time',
        'nns_modify_time',
        'nns_state'
    );

    /**
     * 添加磁盘列表
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_params array(
            'nns_server_id',      => '服务器ID',
            'nns_disk_name',      => '磁盘名称',
            'nns_disk_path',      => '磁盘地址',
            'nns_disk_extension', => '磁盘支持文件扩展名',
            'nns_summary',        => '磁盘描述',
            'nns_state',          => '状态。0启用；1禁用'
     * )
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function add($obj_dc,$arr_params)
    {
        //处理默认前缀[nns_]
        $arr_params = self::make_nns_pre($arr_params);
        //过滤非法参数
        $arr_params = self::except_useless_params(self::$arr_filed, $arr_params);
        if(empty($arr_params))
        {
            return self::return_data(1,'参数为空');
        }
        //主键
        if(!isset($arr_params['nns_id']) || strlen($arr_params['nns_id']) < 1)
        {
            $arr_params['nns_id'] = np_guid_rand();
        }
        //记录生成时间
        $arr_params['nns_create_time'] = $arr_params['nns_modify_time'] = date("Y-m-d H:i:s");
        //组装插入记录SQL语句
        $str_sql = self::make_insert_sql(self::$base_table, $arr_params);
        //执行SQL语句
        $bool_result = nl_execute_by_db($str_sql, $obj_dc->db());
        //返回结果
        return $bool_result ? self::return_data(0,'success') : self::return_data(1,'数据库执行失败，SQL语句：' . $str_sql);
    }

    /**
     * 更新磁盘列表
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_update_where  array(  均为可选
            'nns_id',             => '磁盘ID',
            'nns_server_id',      => '服务器ID',
            'nns_disk_name',      => '磁盘名称',
            'nns_disk_extension', => '磁盘支持文件扩展名',
            'nns_state',          => '状态。0启用；1禁用'
     * )
     * @param array  $arr_update_params array(  均为可选
            'nns_server_id',      => '服务器ID',
            'nns_disk_name',      => '磁盘名称',
            'nns_disk_path',      => '磁盘地址',
            'nns_disk_extension', => '磁盘支持文件扩展名',
            'nns_summary',        => '磁盘描述',
            'nns_state',          => '状态。0启用；1禁用'
     * )
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function edit($obj_dc,$arr_update_where,$arr_update_params)
    {
        //处理默认前缀[nns_]
        $arr_update_where = self::make_nns_pre($arr_update_where);
        $arr_update_params= self::make_nns_pre($arr_update_params);
        //过滤非法参数
        $arr_update_where = self::except_useless_params(self::$arr_filed, $arr_update_where);
        $arr_update_params= self::except_useless_params(self::$arr_filed, $arr_update_params);
        //验证参数
        if(empty($arr_update_where) || empty($arr_update_params))
        {
            return self::return_data(1,'参数为空');
        }
        //更新时间
        if(!isset($arr_update_params['nns_modify_time']) || strlen($arr_update_params['nns_modify_time'])<1)
        {
            $arr_update_params['nns_modify_time'] = date("Y-m-d H:i:s");
        }
        //组装更新记录SQL语句
        $str_sql = self::make_update_sql(self::$base_table, $arr_update_params, $arr_update_where);
        //执行SQL语句
        $bool_result = nl_execute_by_db($str_sql, $obj_dc->db());
        //返回结果
        return $bool_result ? self::return_data(0,'success') : self::return_data(1,'数据库执行失败，SQL语句：' . $str_sql);
    }

    /**
     * 删除磁盘
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_del_where array(
            'nns_id',             => '磁盘ID',
            'nns_server_id',      => '服务器ID',
            'nns_disk_name',      => '磁盘名称',
            'nns_disk_extension', => '磁盘支持文件扩展名',
            'nns_state',          => '状态。0启用；1禁用'
     * )
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function del($obj_dc,$arr_del_where)
    {
        //处理默认前缀[nns_]
        $arr_del_where = self::make_nns_pre($arr_del_where);
        //过滤非法参数
        $arr_del_where = self::except_useless_params(self::$arr_filed, $arr_del_where);
        //验证参数
        if(empty($arr_del_where))
        {
            return self::return_data(1,'参数为空');
        }
        //组装删除记录SQL语句
        $str_sql = 'delete from ' . self::$base_table . ' where ' . self::_create_query_sql($arr_del_where);
        //执行SQL语句
        $bool_result = nl_execute_by_db($str_sql, $obj_dc->db());
        //返回结果
        return $bool_result ? self::return_data(0,'success') : self::return_data(1,'数据库执行失败，SQL语句：' . $str_sql);
    }

    /**
     * 查询磁盘列表
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_query_params array(
            'nns_id',             => '磁盘ID',
            'nns_server_id',      => '服务器ID',
            'nns_disk_name',      => '磁盘名称',
            'nns_disk_extension', => '磁盘支持文件扩展名',
            'nns_start_time',       => '创建开始时间',
            'nns_end_time',         => '创建结束时间',
            'nns_state',            => '状态。0启用；1禁用',
     * )
     * @param array $arr_limit
     * @param string $str_order
     * @param string $str_group
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function query_list($obj_dc,$arr_query_params,$arr_limit = array(0,10),$str_order = 'nns_modify_time desc',$str_group = '')
    {
        //处理默认前缀[nns_]
        $arr_query_params = self::make_nns_pre($arr_query_params);
        //组装查询SQL语句
        $str_where = ' from ' . self::$base_table . ' where ' . self::_create_query_sql($arr_query_params);
        //查询总数
        $str_sql = 'select count(1) total ' . $str_where;
        $arr_count = nl_db_get_one($str_sql,$obj_dc->db());
        if(is_array($arr_count) && $arr_count['total'] > 0)
        {
            self::$page_info = $arr_limit;
            self::$page_info['total_count'] = $arr_count['total'];
            //分组
            if(isset($str_group) && strlen($str_group) > 0)
            {
                $str_where .= ' group by ' . $str_group;
            }
            //排序
            if(isset($str_order) && strlen($str_order) > 0)
            {
                $str_where .= ' order by ' . $str_order;
            }
            //分页
            if(isset($arr_limit) && !empty($arr_limit))
            {
                if(is_string($arr_limit))
                {
                    $arr_limit = explode(',',$arr_limit);
                }
                $str_where .= ' limit ' . $arr_limit[0] * $arr_limit[1] . ',' . $arr_limit[1];
                self::$page_info['page_count'] = (int)(($arr_limit[0] + $arr_limit[1] - 1) / $arr_limit[1]);
            }
            $arr_server_list = nl_query_by_db('select * ' . $str_where,$obj_dc->db());
            if(is_array($arr_server_list) && !empty($arr_server_list))
            {
                return self::return_data(0,'success',$arr_server_list,array($arr_count));
            }
        }
        else
        {
            $arr_count['total'] = 0;
            self::$page_info['page_count'] = 0;
            self::$page_info['total_count'] = 0;
        }
        return self::return_data(0,'success',array(),array($arr_count));
    }

    /**
     * 查询磁盘列表
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_query_params array(
            'nns_id',             => '磁盘ID',
            'nns_server_id',      => '服务器ID',
            'nns_disk_name',      => '磁盘名称',
            'nns_disk_extension', => '磁盘支持文件扩展名',
            'nns_start_time',       => '创建开始时间',
            'nns_end_time',         => '创建结束时间',
            'nns_state',            => '状态。0启用；1禁用',
     * )
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function query_one($obj_dc,$arr_query_params)
    {
        //处理默认前缀[nns_]
        $arr_query_params = self::make_nns_pre($arr_query_params);
        //组装查询SQL语句
        $str_sql = 'select * from ' . self::$base_table . ' where ' . self::_create_query_sql($arr_query_params);
        $arr_info = nl_db_get_one($str_sql,$obj_dc->db());

        return self::return_data(0,'success',$arr_info);
    }

    /**
     * 拼接SQL条件语句
     * @param array  $arr_query_params array(
            'nns_id',             => '磁盘ID',
            'nns_server_id',      => '服务器ID',
            'nns_disk_name',      => '磁盘名称',
            'nns_disk_extension', => '磁盘支持文件扩展名',
            'nns_start_time',       => '创建开始时间',
            'nns_end_time',         => '创建结束时间',
            'nns_state',            => '状态。0启用；1禁用',
     * )
     * @return string 查询条件
     */
    private static function _create_query_sql($arr_query_params)
    {
        $str_where = '1=1';
        //主键ID
        if(isset($arr_query_params['nns_id']))
        {
            if(is_array($arr_query_params['nns_id']))
            {
                $str_where .= " and nns_id in ('" . implode("','",$arr_query_params['nns_id']) . "')";
            }
            else
            {
                $str_where .= " and nns_id = '{$arr_query_params['nns_id']}'";
            }
        }
        //服务器ID
        if(isset($arr_query_params['nns_server_id']) && strlen($arr_query_params['nns_server_id']) > 0)
        {
            $str_where .= " and " . self::$base_table . ".nns_server_id = '{$arr_query_params['nns_server_id']}'";
        }
        //磁盘名称
        if(isset($arr_query_params['nns_disk_name']) && strlen($arr_query_params['nns_disk_name']) > 0)
        {
            $str_where .= " and " . self::$base_table . ".nns_disk_name like '%{$arr_query_params['nns_disk_name']}%'";
        }
        //磁盘支持文件扩展名
        if(isset($arr_query_params['nns_disk_extension']) && strlen($arr_query_params['nns_disk_extension']) > 0)
        {
            $str_where .= " and " . self::$base_table . ".nns_disk_extension like '%{$arr_query_params['nns_disk_extension']}%'";
        }
        //磁盘创建开始时间
        if(isset($arr_query_params['nns_start_time']) && strlen($arr_query_params['nns_start_time']) > 0)
        {
            $str_where .= " and " . self::$base_table . ".nns_create_time >= '{$arr_query_params['nns_start_time']}'";
        }
        //磁盘创建结束时间
        if(isset($arr_query_params['nns_end_time']) && strlen($arr_query_params['nns_end_time']) > 0)
        {
            $str_where .= " and " . self::$base_table . ".nns_create_time <= '{$arr_query_params['nns_end_time']}'";
        }
        //磁盘状态
        if(isset($arr_query_params['nns_state']) && strlen($arr_query_params['nns_state']) > 0)
        {
            $str_where .= " and " . self::$base_table . ".nns_state = {$arr_query_params['nns_state']}";
        }
        return $str_where;
    }
} 