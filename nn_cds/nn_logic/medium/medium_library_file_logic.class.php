<?php
/**
 * Created by PhpStorm.
 * Use : 介质库管理-介质库文件管理
 * User: kan.yang@starcor.cn
 * Date: 18-3-22
 * Time: 下午4:40
 */

include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'medium_library_base_logic.class.php';
class medium_library_file_logic extends medium_library_base_logic
{
    /**
     * 初始化函数
     */
    public static function init()
    {
        parent::$base_table='nns_medium_library_file';
        parent::$arr_filed = array(
            'nns_id',
            'nns_cp_id',
            'nns_disk_id',
            'nns_asset_id',
            'nns_asset_type',
            'nns_file_index',
            'nns_file_name',
            'nns_file_url',
            'nns_file_size',
            'nns_file_type',
            'nns_file_duration',
            'nns_general_fomart',
            'nns_general_bit_rate_mode',
            'nns_videos_kbps',
            'nns_videos_bit_rate_mode',
            'nns_videos_format',
            'nns_audios_kbps',
            'nns_audios_bit_rate_mode',
            'nns_audios_format',
            'nns_ext_url',
            'nns_base_info',
            'nns_create_time',
            'nns_modify_time',
            'nns_state',
            'nns_is_analysis',
            'nns_again',
            'nns_copyright_start_time',
            'nns_copyright_end_time',
            'nns_summary',
        );
        parent::$arr_query_filed = array(
            'nns_id'         => array('str'),
            'nns_cp_id'      => array('str'),
            'nns_disk_id'    => array('str'),
            'nns_asset_id'   => array('str'),
            'nns_asset_type' => array('int'),
            'nns_file_index' => array('int'),
            'nns_file_name'  => array('like'),
            'nns_file_url'   => array('str'),
            'nns_file_size'  => array('int'),
            'nns_file_type'  => array('str'),
            'nns_file_kbps'  => array('int'),
            'nns_file_length'=> array('int'),
            'nns_file_coding'=> array('str'),
            'nns_ext_url'    => array('str'),
            'nns_base_info'  => array('like'),
            'nns_file_type'  => array('str'),
            'nns_start_time' => array('>='),
            'nns_end_time'   => array('<='),
            'nns_again'      => array('<='),
            'nns_state'      => array('int'),
            'nns_is_analysis'=> array('int'),
            'nns_copyright_start_time'=>array('>='),
            'nns_copyright_end_time'=>array('<='),
            'nns_summary'=>array('str'),
        );
    }

    /**
     * 添加介质库文件列表
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_params array(
            'nns_cp_id'        => 'CP ID',
            'nns_disk_id',     => '磁盘ID',
            'nns_asset_id',    => '文件分类ID',
            'nns_asset_type',  => '文件分类类型。0单集；1多集',
            'nns_file_index',  => '文件集数',
            'nns_file_name',   => '文件名称',
            'nns_file_url',    => '文件物理路径',
            'nns_file_size',   => '文件大小，字节',
            'nns_file_type',   => '文件格式',
            'nns_file_kbps',   => '文件码率（视频文件）',
            'nns_file_length', => '播放时长（视频文件）',
            'nns_file_coding', => '视频编码（视频文件）',
            'nns_ext_url',     => '文件额外地址',
            'nns_base_info',   => '文件元数据',
            'nns_is_analysis', => '是否解析。0未解析；1解析成功；2解析失败',
     * )
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function add($obj_dc,$arr_params)
    {
        self::init();
        return parent::add($obj_dc,$arr_params);
    }

    /**
     * 更新介质库文件列表
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_update_where  array(  均为可选
            'nns_id'           => '主键',
            'nns_cp_id'        => 'CP ID',
            'nns_disk_id',     => '磁盘ID',
            'nns_asset_id',    => '文件分类ID',
            'nns_asset_type',  => '文件分类类型。0单集；1多集',
            'nns_file_index',  => '文件集数',
            'nns_file_name',   => '文件名称',
            'nns_file_type',   => '文件格式',
            'nns_file_kbps',   => '文件码率（视频文件）',
            'nns_file_length', => '播放时长（视频文件）',
            'nns_file_coding', => '视频编码（视频文件）',
     * )
     * @param array  $arr_update_params array(  均为可选
            'nns_cp_id'        => 'CP ID',
            'nns_disk_id',     => '磁盘ID',
            'nns_asset_id',    => '文件分类ID',
            'nns_asset_type',  => '文件分类类型。0单集；1多集',
            'nns_file_index',  => '文件集数',
            'nns_file_name',   => '文件名称',
            'nns_file_url',    => '文件物理路径',
            'nns_file_size',   => '文件大小，字节',
            'nns_file_type',   => '文件格式',
            'nns_file_kbps',   => '文件码率（视频文件）',
            'nns_file_length', => '播放时长（视频文件）',
            'nns_file_coding', => '视频编码（视频文件）',
            'nns_ext_url',     => '文件额外地址',
            'nns_base_info',   => '文件元数据',
            'nns_is_analysis', => '是否解析。0未解析；1解析成功；2解析失败',
     * )
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function edit($obj_dc,$arr_update_where,$arr_update_params)
    {
        //echo json_encode($arr_update_params);die;
        self::init();
        return parent::edit($obj_dc,$arr_update_where,$arr_update_params);
    }

    /**
     * 添加介质库文件列表
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_params array(
            'nns_id'           => '主键ID',
            'nns_cp_id'        => 'CP ID',
            'nns_disk_id',     => '磁盘ID',
            'nns_asset_id',    => '文件分类ID',
            'nns_file_index',  => '文件集数',
            'nns_file_name',   => '文件名称',
            'nns_file_url',    => '文件物理路径',
            'nns_file_size',   => '文件大小，字节',
            'nns_file_type',   => '文件格式',
            'nns_file_kbps',   => '文件码率（视频文件）',
            'nns_file_length', => '播放时长（视频文件）',
            'nns_file_coding', => '视频编码（视频文件）',
            'nns_ext_url',     => '文件额外地址',
            'nns_base_info',   => '文件元数据',
            'nns_is_analysis', => '是否解析。0未解析；1解析成功；2解析失败',
     * )
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function replace($obj_dc,$arr_params)
    {
        self::init();
        return parent::replace($obj_dc,$arr_params);
    }

    /**
     * 删除介质库文件
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_del_where array(
            'nns_id'           => '主键',
            'nns_cp_id'        => 'CP ID',
            'nns_disk_id',     => '磁盘ID',
            'nns_asset_id',    => '文件分类ID',
            'nns_asset_type',  => '文件分类类型。0单集；1多集',
            'nns_file_index',  => '文件集数',
            'nns_file_name',   => '文件名称',
            'nns_file_type',   => '文件格式',
            'nns_file_kbps',   => '文件码率（视频文件）',
            'nns_file_length', => '播放时长（视频文件）',
            'nns_file_coding', => '视频编码（视频文件）',
            'nns_start_time',       => '创建开始时间',
            'nns_end_time',         => '创建结束时间',
            'nns_state',            => '状态。0启用；1禁用',
            'nns_is_analysis', => '是否解析。0未解析；1解析成功；2解析失败',
     * )
     * @param bool   $bool_sys_where   是否系统拼接查询语句查询条件
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function del($obj_dc,$arr_del_where,$bool_sys_where = true)
    {
        self::init();
        return parent::del($obj_dc,$arr_del_where);
    }

    /**
     * 查询介质库文件列表
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_query_params array(
            'nns_id'           => '主键',
            'nns_cp_id'        => 'CP ID',
            'nns_disk_id',     => '磁盘ID',
            'nns_asset_id',    => '文件分类ID',
            'nns_asset_type',  => '文件分类类型。0单集；1多集',
            'nns_file_index',  => '文件集数',
            'nns_file_name',   => '文件名称',
            'nns_file_type',   => '文件格式',
            'nns_file_kbps',   => '文件码率（视频文件）',
            'nns_file_length', => '播放时长（视频文件）',
            'nns_file_coding', => '视频编码（视频文件）',
            'nns_start_time',       => '创建开始时间',
            'nns_end_time',         => '创建结束时间',
            'nns_state',            => '状态。0启用；1禁用',
            'nns_is_analysis', => '是否解析。0未解析；1解析成功；2解析失败',
     * )
     * @param array $arr_limit
     * @param string $str_order
     * @param string $str_group
     * @param bool   $bool_sys_where   是否系统拼接查询语句查询条件
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function query_list($obj_dc,$arr_query_params,$arr_limit = array(0,10),$str_order = 'nns_modify_time desc',$str_group = '',$bool_sys_where = true)
    {
        self::init();
        return parent::query_list($obj_dc,$arr_query_params,$arr_limit,$str_order,$str_group,$bool_sys_where);
    }

    
    public static function query_export($obj_dc,$arr_query_params,$str_order = 'order by nns_cp_id asc,nns_file_name asc',$bool_sys_where = true)
    {
        self::init();
        return parent::query_export($obj_dc,$arr_query_params,$str_order = 'order by nns_cp_id asc,nns_file_name asc',$bool_sys_where = true);
    }
    
    /**
     * 查询介质库文件列表
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_query_params array(
            'nns_id'           => '主键',
            'nns_cp_id'        => 'CP ID',
            'nns_disk_id',     => '磁盘ID',
            'nns_asset_id',    => '文件分类ID',
            'nns_asset_type',  => '文件分类类型。0单集；1多集',
            'nns_file_index',  => '文件集数',
            'nns_file_name',   => '文件名称',
            'nns_file_type',   => '文件格式',
            'nns_file_kbps',   => '文件码率（视频文件）',
            'nns_file_length', => '播放时长（视频文件）',
            'nns_file_coding', => '视频编码（视频文件）',
            'nns_start_time',       => '创建开始时间',
            'nns_end_time',         => '创建结束时间',
            'nns_state',            => '状态。0启用；1禁用',
            'nns_is_analysis', => '是否解析。0未解析；1解析成功；2解析失败',
     * )
     * @param bool   $bool_sys_where   是否系统拼接查询语句查询条件
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function query_one($obj_dc,$arr_query_params,$bool_sys_where = true)
    {
        self::init();
        return parent::query_one($obj_dc,$arr_query_params,$bool_sys_where);
    }
    
    
    
    
    public static function query_sql($obj_dc,$str_sql)
    {
        self::init();
        return parent::query_sql($obj_dc,$str_sql);
    }

    public static function execute_sql($obj_dc,$str_sql)
    {
        self::init();
        return parent::execute_sql($obj_dc,$str_sql);
    }
}