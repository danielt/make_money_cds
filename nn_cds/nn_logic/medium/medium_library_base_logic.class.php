<?php
/**
 * Created by PhpStorm.
 * Use : 介质库管理：基类
 * User: kan.yang@starcor.cn
 * Date: 18-3-22
 * Time: 下午4:30
 */

include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class medium_library_base_logic extends nl_public
{
    protected static $base_table='';
    protected static $arr_filed = array();
    protected static $arr_query_filed = array();

    /**
     * 添加记录
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_params 插入记录字段
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function add($obj_dc,$arr_params)
    {
        if(is_array($arr_params))
        {
            //处理默认前缀[nns_]
            $arr_params = self::make_nns_pre($arr_params);
            //过滤非法参数
            $arr_params = self::except_useless_params(self::$arr_filed, $arr_params);
        }
        if(empty($arr_params))
        {
            return self::return_data(1,'参数为空');
        }
        //主键
        if(!isset($arr_params['nns_id']) || strlen($arr_params['nns_id']) < 1)
        {
            $arr_params['nns_id'] = np_guid_rand();
        }
        //记录生成时间
        $arr_params['nns_create_time'] = $arr_params['nns_modify_time'] = date("Y-m-d H:i:s");
        //组装插入记录SQL语句
        $str_sql = self::make_insert_sql(self::$base_table, $arr_params);
        //执行SQL语句
        $bool_result = nl_execute_by_db($str_sql, $obj_dc->db());
        //返回结果
        return $bool_result ? self::return_data(0,'success') : self::return_data(1,'数据库执行失败，SQL语句：' . $str_sql);
    }

    /**
     * 更新记录
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_update_where  更新条件
     * @param array  $arr_update_params 更新字段
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function edit($obj_dc,$arr_update_where,$arr_update_params)
    {
        if(is_array($arr_update_where))
        {
            //处理默认前缀[nns_]
            $arr_update_where = self::make_nns_pre($arr_update_where);
            //过滤非法参数
            $arr_update_where = self::except_useless_params(self::$arr_filed, $arr_update_where);
        }
        if(is_array($arr_update_params))
        {
            //处理默认前缀[nns_]
            $arr_update_params = self::make_nns_pre($arr_update_params);
            //过滤非法参数
            $arr_update_params = self::except_useless_params(self::$arr_filed, $arr_update_params);
        }
        //验证参数
        if(empty($arr_update_where) || empty($arr_update_params))
        {
            return self::return_data(1,'参数为空');
        }
        //更新时间
        if(!isset($arr_update_params['nns_modify_time']) || strlen($arr_update_params['nns_modify_time'])<1)
        {
            $arr_update_params['nns_modify_time'] = date("Y-m-d H:i:s");
        }
        //组装更新记录SQL语句
        $str_sql = self::make_update_sql(self::$base_table, $arr_update_params, $arr_update_where);
        //执行SQL语句
        $bool_result = nl_execute_by_db($str_sql, $obj_dc->db());
        //返回结果
        return $bool_result ? self::return_data(0,'success') : self::return_data(1,'数据库执行失败，SQL语句：' . $str_sql);
    }

    /**
     * 替换记录
     * @param $obj_dc
     * @param $arr_params
     * @return array
     */
    public static function replace($obj_dc,$arr_params)
    {
        if(is_array($arr_params))
        {
            //处理默认前缀[nns_]
            $arr_params = self::make_nns_pre($arr_params);
            //过滤非法参数
            $arr_params = self::except_useless_params(self::$arr_filed, $arr_params);
        }
        if(empty($arr_params))
        {
            return self::return_data(1,'参数为空');
        }
        //主键
        if(!isset($arr_params['nns_id']) || strlen($arr_params['nns_id']) < 1)
        {
            $arr_params['nns_id'] = np_guid_rand();
        }
        //记录生成时间
        $arr_params['nns_create_time'] = $arr_params['nns_modify_time'] = date("Y-m-d H:i:s");
        //组装插入记录SQL语句
        $str_sql = self::make_replace_sql(self::$base_table, $arr_params);
        //执行SQL语句
        $bool_result = nl_execute_by_db($str_sql, $obj_dc->db());
        //返回结果
        return $bool_result ? self::return_data(0,'success') : self::return_data(1,'数据库执行失败，SQL语句：' . $str_sql);
    }

    /**
     * 删除记录
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_del_where 删除条件
     * @param bool   $bool_sys_where   是否系统拼接查询语句查询条件
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function del($obj_dc,$arr_del_where,$bool_sys_where = true)
    {
        if(is_array($arr_del_where))
        {
            //处理默认前缀[nns_]
            $arr_del_where = self::make_nns_pre($arr_del_where);
            //过滤非法参数
            $arr_del_where = self::except_useless_params(self::$arr_filed, $arr_del_where);
        }
        //验证参数
        if(empty($arr_del_where))
        {
            return self::return_data(1,'参数为空');
        }
        //组装删除记录SQL语句
        $str_where = $bool_sys_where ? self::_create_query_sql($arr_del_where) : $arr_del_where;
        $str_sql = 'delete from ' . self::$base_table . ' where ' . $str_where;
        //执行SQL语句
        $bool_result = nl_execute_by_db($str_sql, $obj_dc->db());
        //返回结果
        return $bool_result ? self::return_data(0,'success') : self::return_data(1,'数据库执行失败，SQL语句：' . $str_sql);
    }

    /**
     * 查询多条记录
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_query_params 查询条件
     * @param array $arr_limit
     * @param string $str_order
     * @param string $str_group
     * @param bool   $bool_sys_where   是否系统拼接查询语句查询条件
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function query_list($obj_dc,$arr_query_params,$arr_limit = array(0,10),$str_order = 'nns_modify_time desc',$str_group = '',$bool_sys_where = true)
    {
        if(is_array($arr_query_params))
        {
            //处理默认前缀[nns_]
            $arr_query_params = self::make_nns_pre($arr_query_params);
            //过滤非法参数
//             $arr_query_params = self::except_useless_params(self::$arr_filed, $arr_query_params);
        }
        //组装查询SQL语句
        $str_where = $bool_sys_where ? self::_create_query_sql($arr_query_params) : $arr_query_params;
        $str_where = ' from ' . self::$base_table . ' where ' . $str_where;
        //查询总数
        $str_sql = 'select count(1) total ' . $str_where;
        $arr_count = nl_db_get_one($str_sql,$obj_dc->db());
        if(is_array($arr_count) && $arr_count['total'] > 0)
        {
            self::$page_info = $arr_limit;
            self::$page_info['total_count'] = $arr_count['total'];
            //分组
            if(isset($str_group) && strlen($str_group) > 0)
            {
                $str_where .= ' group by ' . $str_group;
            }
            //排序
            if(isset($str_order) && strlen($str_order) > 0)
            {
                $str_where .= ' order by ' . $str_order;
            }
            //分页
            if(isset($arr_limit) && !empty($arr_limit))
            {
                if(is_string($arr_limit))
                {
                    $arr_limit = explode(',',$arr_limit);
                }
                $str_where .= ' limit ' . $arr_limit[0] * $arr_limit[1] . ',' . $arr_limit[1];
                self::$page_info['page_count'] = (int)(($arr_limit[0] + $arr_limit[1] - 1) / $arr_limit[1]);
            }
            $arr_server_list = nl_query_by_db('select * ' . $str_where,$obj_dc->db());
            if(is_array($arr_server_list) && !empty($arr_server_list))
            {
                return self::return_data(0,'success',$arr_server_list,array($arr_count));
            }
        }
        else
        {
            $arr_count['total'] = 0;
            self::$page_info['page_count'] = 0;
            self::$page_info['total_count'] = 0;
        }
        return self::return_data(0,'success',array(),array($arr_count));
    }

    /**
     * 查询多条记录
     * @param object $obj_dc 数据库缓存操作类
     * @param array  $arr_query_params 查询条件
     * @param array $arr_limit
     * @param string $str_order
     * @param string $str_group
     * @param bool   $bool_sys_where   是否系统拼接查询语句查询条件
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function query_export($obj_dc,$arr_query_params,$str_order = 'order by nns_cp_id asc,nns_file_name asc',$bool_sys_where = true)
    {
        if(is_array($arr_query_params))
        {
            //处理默认前缀[nns_]
            $arr_query_params = self::make_nns_pre($arr_query_params);
            //过滤非法参数
            //             $arr_query_params = self::except_useless_params(self::$arr_filed, $arr_query_params);
        }
        //组装查询SQL语句
        $str_where = $bool_sys_where ? self::_create_query_sql($arr_query_params) : $arr_query_params;
        $str_sql = 'select nns_cp_id,nns_file_index,nns_file_name,nns_file_url,nns_file_size,nns_file_duration,nns_general_fomart,nns_general_bit_rate_mode,nns_videos_kbps,".
            "nns_videos_format,nns_videos_bit_rate_mode,nns_audios_kbps,nns_audios_format,nns_audios_bit_rate_mode from ' . self::$base_table . ' where ' . $str_where.' '.$str_order;
        $arr_server_list = nl_query_by_db($str_sql,$obj_dc->db());
        return self::return_data(0,'success',$arr_server_list);
    }
    
    
    /**
     * 查询一条记录
     * @param object $obj_dc           数据库缓存操作类
     * @param array  $arr_query_params 查询条件。数组或字符串
     * @param bool   $bool_sys_where   是否系统拼接查询语句查询条件
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function query_one($obj_dc,$arr_query_params,$bool_sys_where = true)
    {
        if(is_array($arr_query_params))
        {
            //处理默认前缀[nns_]
            $arr_query_params = self::make_nns_pre($arr_query_params);
            //过滤非法参数
            $arr_query_params = self::except_useless_params(self::$arr_filed, $arr_query_params);
        }
        //组装查询SQL语句
        $str_where = $bool_sys_where ? self::_create_query_sql($arr_query_params) : $arr_query_params;
        $str_sql = 'select * from ' . self::$base_table . ' where ' . $str_where;
        $arr_info = nl_db_get_one($str_sql,$obj_dc->db());

        return self::return_data(0,'success',$arr_info);
    }

    public static function execute_sql($obj_dc,$str_sql)
    {
        $bool_result = nl_execute_by_db($str_sql, $obj_dc->db());
        //返回结果
        return $bool_result ? self::return_data(0,'success') : self::return_data(1,'数据库执行失败，SQL语句：' . $str_sql);
    }
    /**
     * 复杂SQL语句语句直接执行调用
     * @param object $obj_dc  数据库缓存操作类
     * @param string $str_sql 原始SQL语句
     * @return array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public static function query_sql($obj_dc,$str_sql)
    {
        $arr_list = nl_query_by_db($str_sql,$obj_dc->db());
        if(is_array($arr_list) && !empty($arr_list))
        {
            return self::return_data(0,'success',$arr_list);
        }
        return self::return_data(0,'success',array());
    }

    /**
     * 拼接SQL条件语句
     * @param array  $arr_query_params array( 查询条件
            'k' => 'v'
     * )
     * @return string 查询条件
     */
    private static function _create_query_sql($arr_query_params)
    {
        $str_where = '1=1';
        //为空返回
        if(empty($arr_query_params))
        {
            return $str_where;
        }
        //遍历构造
        foreach($arr_query_params as $key => $param)
        {
            $temp_key = ($key == 'nns_start_time' || $key == 'nns_end_time') ? 'nns_modify_time' : $key;
            switch(self::$arr_query_filed[$key][0])
            {
                case 'int':
                case 'str':
                    if(is_array($param))
                    {
                        $str_where .= " and $temp_key in ('" . implode("','",$param) . "')";
                    }
                    else
                    {
                        $str_where .= " and $temp_key = '{$param}'";
                    }
                    break;
                case 'like':
                    $str_where .= ' and ' . $temp_key . ' like \'%' . addslashes($param) . '%\'';
                    break;
                case '>=':
                    $str_where .= ' and ' . $temp_key . ' >= \'' . addslashes($param) . '\'';
                    break;
                case '<=':
                    $str_where .= ' and ' . $temp_key . ' <= \'' . addslashes($param) . '\'';
                    break;
                default:
                    break;
            }
        }
        return $str_where;
    }
} 