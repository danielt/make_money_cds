<?php

//点播权限审核
define('NNS_VOD_CHECK_WAITING',0); //点播目录  等待审核
define('NNS_VOD_CHECK_OK',1); //点播目录 审核通过
define('NNS_VOD_AND_MEDIA_CHECK_OK',2); //点播目录和媒体源审核权限均为 通过

define('NNS_VOD_MEDIA_CHECK_WAITING',0); //媒体 等待审核
define('NNS_VOD_MEDIA_CHECK_OK',1);//媒体  审核通过


//点播状态上下线

define('NNS_VOD_STATE_ONLINE',1); //点播目录 上线
define('NNS_VOD_STATE_WAITING',0); //点播目录 等待上线
define('NNS_VOD_AND_MEDIA_STATE_ONLINE',2); //点播目录和媒体源 均上线
define('NNS_VOD_STATE_OFFLINE',3);//点播目录 下线

define('NNS_VOD_MEDIA_STATE_WAITING',0);//媒体  等待上线
define('NNS_VOD_MEDIA_STATE_ONLINE',1); //媒体 上线
define('NNS_VOD_MEDIA_STATE_OFFLINE',3);//媒体 下线

//直播权限审核
define('NNS_LIVE_CHECK_WAITING',0); //点播目录  等待审核
define('NNS_LIVE_CHECK_OK',1); //点播目录 审核通过
define('NNS_LIVE_AND_MEDIA_CHECK_OK',2); //点播目录和媒体源审核权限均为 通过

define('NNS_LIVE_MEDIA_CHECK_WAITING',0); //媒体 等待审核
define('NNS_LIVE_MEDIA_CHECK_OK',1);//媒体  审核通过


//直播状态上下线

define('NNS_LIVE_STATE_ONLINE',1); //点播目录 上线
define('NNS_LIVE_STATE_WAITING',0); //点播目录 等待上线
define('NNS_LIVE_AND_MEDIA_STATE_ONLINE',2); //点播目录和媒体源 均上线
define('NNS_LIVE_STATE_OFFLINE',3);//点播目录 下线

define('NNS_LIVE_MEDIA_STATE_WAITING',0);//媒体  等待上线
define('NNS_LIVE_MEDIA_STATE_ONLINE',1); //媒体 上线
define('NNS_LIVE_MEDIA_STATE_OFFLINE',3);//媒体 下线

//终端类型
$nns_device_type = array();
$nns_device_type[0]="pc";
$nns_device_type[1]="phone";
$nns_device_type[2]="stb";
$nns_device_type[3]="ipad";

//码流模式
$nns_vod_mode=array();
$nns_vod_mode[0]="STD";//标清
$nns_vod_mode[1]="HD";//高清
$nns_vod_mode[2]="LOW";//流畅
$nns_vod_mode[3]="4K";//4K

//点播是否已删除
define('NNS_VOD_DELETED_TRUE', 1); //标记vod，已经删除
define('NNS_VOD_DELETED_FALSE', 0);//标记vod，未删除
define('NNS_VOD_MEDIA_DELETED_TRUE', 1);//标记vod media 已经删除
define('NNS_VOD_MEDIA_DELETED_FALSE', 0);//标记vod media 未删除

//直播是否已删除
define('NNS_LIVE_DELETED_TRUE', 1); //标记LIVE，已经删除
define('NNS_LIVE_DELETED_FALSE', 0);//标记LIVE，未删除
define('NNS_LIVE_MEDIA_DELETED_TRUE', 1);//标记LIVE media 已经删除
define('NNS_LIVE_MEDIA_DELETED_FALSE', 0);//标记LIVE media 未删除

//直播节目单是否已删除
define('NNS_LIVE_PLAYBILL_DELETED_TRUE',1);
define('NNS_LIVE_PLAYBILL_DELETED_FALSE',0);

//调用核心平台管理9000
define('NNS_VOD_MEDIA_CORE_ACTION_NOT_FOUND', 9000);//调用核心管理平台的action参数不存在


//产品计费类型
define('NNS_PRODUCT_TIME_BY_TIME', 0); //即时计费
define('NNS_PRODUCT_BY_MONTH', 1);//包月计费
define('NNS_PRODUCT_BY_YEAR', 2);//包年计费
define('NNS_PRODUCT_BY_DAY', 3);//按天数周期计费
define('NNS_PRODUCT_BY_VOD', 4);//按影片数计费

//产品内容绑定方式
//0为全部影片，1为所有直播影片，2为所有点播影片，3为按栏目绑定，4为按直播影片绑定，5为按直播片源绑定，6为按点播影片绑定，7为按点播片源绑定，
define('NNS_PRODUCT_BIND_ALL', 0);
define('NNS_PRODUCT_BIND_ALL_LIVE', 1);
define('NNS_PRODUCT_BIND_ALL_VOD', 2);
define('NNS_PRODUCT_BIND_CATEGORY', 3);
define('NNS_PRODUCT_BIND_LIVE', 4);
define('NNS_PRODUCT_BIND_LIVE_MEDIA', 5);
define('NNS_PRODUCT_BIND_VOD', 6);
define('NNS_PRODUCT_BIND_VOD_MEDIA', 7);

define('NNS_ERROR_CORE_CONTENT_FAILED',8);

define('NNS_ERROR_DEPOT_NO_DATA',1);
//媒体类型
$nns_media_type = array();
$nns_media_type[0] = "ts";
$nns_media_type[1] = "flv";
$nns_media_type[2] = "mp4";
$nns_media_type[3] = "wmv";
$nns_media_type[4] = "m3u8";

//TAG常量定义
define('NNS_PC_TAG', 31);
define('NNS_ANDROID_PAD_TAG', 30);
define('NNS_ANDROID_PHONE_TAG', 29);
define('NNS_APPLE_PAD_TAG', 28);
define('NNS_APPLE_PHONE_TAG', 27);
define('NNS_PLAYER_STD_TAG', 26);
//MODEL层输出错误码
define ('ML_STATE_OK', "ML_STATE_OK");
define ('ML_STATE_PARAM_ERROR', "ML_STATE_PARAM_ERROR");
define ('ML_STATE_DB_ERROR', "ML_STATE_DB_ERROR");
define ('ML_STATE_FUNC_NOT_EXISTS', "ML_STATE_FUNC_NOT_EXISTS");
define ('ML_STATE_FAIL', "ML_STATE_FAIL");
?>
