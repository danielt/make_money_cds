<?php

/* 接口名替换规则数组 */
$fileNameArr = array(
    'N1_A' => 'STBindex',
    'N2_A' => 'ClientIndex',
    'N3_A_2' => 'EPGindex',
    'N3_A_A' => 'BasicIndex',
    'N3_A_D' => 'MAPindex',
    'N3_A_G' => 'LabelIndex',
    'N3_A_H' => 'ProductIndex',
    'N100_A' => 'AddedServiceIndex',
    'N200_A' => 'MgtvIndex',
    'N201_A' => 'MgtvPayment',
    'N21_A' => 'MgtvSearchHotWord',
    'N13_A' => 'StillIndex',
    'N22_A' => 'UpdateVersion',
    'N202_A' => 'TCLindex',
    'N23_A' => 'FAQindex',
    'N203_A' => 'SamsungIndex',
    'N205_A' => 'CHindex', //长虹
    'N206_A' => 'FactoryIndex', //公共推荐
);
?>
