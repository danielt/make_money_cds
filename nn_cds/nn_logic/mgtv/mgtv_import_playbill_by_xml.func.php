<?php

/*
 * Created on 2013-1-16
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

if (!isset($_FILES['Filedata'])) {
        exit('FILES 不存在');
}

/** MGTV节目单导入主体方法
 * @param String 
 * @return 错误描述
 * 			成功为"success"
 */
function mgtv_import_playbill() {
        $fileParts = pathinfo($_FILES['Filedata']['name']);

        if (strtolower($fileParts['extension']) == 'txt') {
//		导入文件为TXT 则调用TXT解析
                $state = mgtv_import_playbill_by_txt();
        } elseif (strtolower($fileParts['extension']) == 'xls') {
//		导入文件为EXCEL 则调用EXCEL解析
                $state = mgtv_import_playbill_by_excel();
        } else {
                $state = 'Error';
        }

        return $state;
}

/* * 解析TXT并写入数据库的方法的方法写这里。。。。* */

function mgtv_import_playbill_by_txt() {
        $tempFile = $_FILES['Filedata']['tmp_name'];
        $fileArr = file($tempFile);
        $Channel = trim(str_replace('Channel:', '', array_shift($fileArr))); //Channel:CCTV-1
        $list = array();
        $deltime = array();
        foreach ($fileArr as $v) {
                if (empty($v))
                        continue;
                if ($strpos = strripos($v, ':')) {
                        $key = trim(substr($v, $strpos + 1, -1));
                        $deltime[] = $key;
                        continue;
                }
                preg_match('/(?P<startime>\w+)-(?P<endtime>\w+)\|(?P<name>.*)/i', $v, $text);
                $addarr = array();
                $name = iconv('gbk', 'utf-8', $text['name']);
                $addarr['name'] = $name;
                $startime = strtotime($key . $text['startime']);
                $endtime = strtotime($key . $text['endtime']);
                $addarr['begin_time'] = date('Y-m-d H:i:s', $startime);
                $addarr['time_len'] = ($startime > $endtime ? $endtime + 86400 : $endtime) - $startime;
                $list[] = $addarr;
        }

        $result = mgtv_add_playbill($list, $deltime);
        return $result;
}

/* * 解析EXCEL的方法并写入数据库的方法写这里。。。。* */

function mgtv_import_playbill_by_excel() {
        require_once dirname(dirname(dirname(__FILE__))) . '/models/excel_reader/reader.php';
        $excel_reader = new Spreadsheet_Excel_Reader();
        $excel_reader->setOutputEncoding('utf-8'); //CP936
        $excel_reader->setRowColOffset(0);
        $excel_reader->read($_FILES['Filedata']['tmp_name']);
        $count_sheetData = count($excel_reader->sheets[0]['numRows']);
        $list = $excel_reader->sheets[0]['cells'];
        $result = array();
        $deltime = array();
        foreach ($list as $v) {
                $begin_time = str_replace('.', '-', $v[1]); //2013.03.29 19:30:00
                $addarr = array();
                $deltime[] = date('Ymd', strtotime($begin_time));
                $addarr['name'] = $v[0];
                $addarr['begin_time'] = $begin_time;
                $addarr['time_len'] = strtotime(str_replace('.', '-', $v[2])) - strtotime($begin_time);
                $result[] = $addarr;
        }
        $deltime = array_unique($deltime);
        $result = mgtv_add_playbill($result, $deltime);
        return $result;
}

/*
 * 根据导入数据 写入数据库
 */

function mgtv_add_playbill($list, $deltime) {

        if (!is_array($list) || !is_array($deltime)) {
                return false;
        }
        set_time_limit(300);
        $dc = nl_get_dc(array(
            'db_policy' => NL_DB_WRITE,
            'cache_policy' => NP_KV_CACHE_TYPE_MEMCACHE
        ));
        $dc->open();
        $file_name = $_FILES['Filedata']['name'];
        $file_info = explode('__', $file_name);
        $pro_id = $file_info[0];
        $live_list = nl_live_bo::get_video_ids_by_pid($dc->db(), $pro_id);
        set_time_limit(300);
        /*
          if ($live_list===FALSE || $live_list===TRUE) { 判断语句有误。。。。
          return '该频道不存在！';
          } */
        if (!is_array($live_list) || empty($pro_id)) {
                return '该频道不存在！';
        }
        $resultnum = 0;
        foreach ($live_list as $live_item) {
                $video_id = $live_item['nns_live_id'];
                foreach ($deltime as $dt) {
                        $endtime = date('Ymd', strtotime($dt) + 86400);
                        nl_playbill_item::del_playbill_item_info_by_time($dc, $dt, $endtime, $video_id);
                }

                foreach ($list as $sql) {
                        if (nl_playbill_item::add_playbill_item($dc, $video_id, $sql)) {
                                $resultnum++;
                        }
                }
        }

        if ($resultnum > 0)
                return 'Success';
        else
                return '导入失败';
}

?>
