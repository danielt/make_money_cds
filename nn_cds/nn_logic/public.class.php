<?php
class nl_public
{
	//分页默认信息
	public static $page_info = array(
			'page_size' => 10,
			'page_num' => 1,
			'page_count'=>1,
			'total_count'=>0
	);
	
	//where条件语句
	public static $str_where="where";
	//返回结果
	public static $return = null;
	
	public static function init_where()
	{
		self::$str_where="where";
	}
	
	/**
	 * 排除非法参数
	 * @param array $use_params
	 * @param array $params
	 * @return null | array
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function except_useless_params($use_params,$params,$where_params=null)
	{
		$resturn_data = array();
		if((empty($use_params) && !is_array($use_params)) || (empty($params) && !is_array($params)))
		{
			return $resturn_data;
		}
		if(isset($params[0]) && is_array($params[0]) && !empty($params[0]))
		{
		    foreach ($params as $key=>$val)
		    {
		        if(!is_array($val) || empty($val))
		        {
		            continue;
		        }
		        foreach ($val as $_key=>$_val)
		        {
		            if (strpos($_key, 'nns_') === FALSE)
		            {
		                unset($val[$_key]);
		                $val['nns_' . $_key] = $_val;
		            }
		        }
		        foreach ($val as $_key=>$_val)
		        {
		            if(in_array($_key, $use_params) && !isset($where_params[$key]))
		            {
		                $val[$_key] = $_val;
		            }
		            else
		            {
		                unset($val[$_key]);
		            }
		        }
		        if(is_array($val) && !empty($val))
		        {
		            $resturn_data[] = $val;
		        }
		    }
		}
		else
		{
    		foreach ($params as $k => $v)
    		{
    			if (strpos($k, 'nns_') === FALSE)
    			{
    				unset($params[$k]);
    				$params['nns_' . $k] = $v;
    			}
    		}
    		foreach ($params as $key=>$val)
    		{
    			if(in_array($key, $use_params) && !isset($where_params[$key]))
    			{
    				$resturn_data[$key] = $val;
    			}
    		}
		}
		return $resturn_data;
	}
	
	
	/**
	 * 添加数据库参数前缀
	 * @param string $params
	 * @return NULL|multitype:unknown
	 * @author liangpan
	 * @date 2016-09-24
	 */
	public static function make_nns_pre($params=null)
	{
		if(!is_array($params) || empty($params))
		{
			return null;
		}
		$return_params = array();
		foreach ($params as $key=>$val)
		{
			if (strpos($key, 'nns_') === FALSE)
			{
				$key = 'nns_' . $key;
			}
			$return_params[$key] = $val;
		}
		unset($params);
		return $return_params;
	}
	
	/**
	 * 组装 insert sql语句
	 * @param string $table 表
	 * @param array $params 数据
	 * @return string
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function make_insert_sql($table,$params)
	{
		$field_str = '';
		$value_str = '';
		if (is_array($params) && !empty($params))
		{
		    if(isset($params[0]) && !empty($params[0]) && is_array($params[0]))
		    {
		        foreach ($params as $k=>$v)
		        {
		            if(!is_array($v) || empty($v))
		            {
		                continue;
		            }
		            foreach ($v as $_k=>$_v)
		            {
		                if($k == 0)
		                {
    		                if (strpos($_k, 'nns_') === FALSE)
    		                {
    		                    $_k = 'nns_' . $_k;
    		                }
    		                $field_str .= $_k . ',';
		                }
		                $value_str .= "'$_v',";
		            }
		            $value_str = rtrim($value_str, ',');
		            $value_str.="),(";
		        }
		        $value_str = rtrim($value_str,'),(');
		    }
		    else
		    {
    			foreach ($params as $k => $v)
    			{
    				if (strpos($k, 'nns_') === FALSE)
    				{
    					$k = 'nns_' . $k;
    				}
    				$field_str .= $k . ',';
    				$value_str .= "'$v',";
    			}
		    }
		}
		$field_str = rtrim($field_str, ',');
		$value_str = rtrim($value_str, ',');
		$sql = "insert into $table ($field_str) values($value_str)";
		return $sql;
	}
	
	/**
	 * 组装 insert sql语句
	 * @param string $table 表
	 * @param array $params 数据
	 * @return string
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function make_replace_sql($table,$params)
	{
		$field_str = '';
		$value_str = '';
		if (is_array($params) && !empty($params))
		{
			foreach ($params as $k => $v)
			{
				if (strpos($k, 'nns_') === FALSE)
				{
					$k = 'nns_' . $k;
				}
				$field_str .= $k . ',';
				$value_str .= "'$v',";
			}
		}
		$field_str = rtrim($field_str, ',');
		$value_str = rtrim($value_str, ',');
		$sql = "replace into $table ($field_str) values($value_str)";
		return $sql;
	}
	
	
	/**
	 * 组装 update sql语句
	 * @param string $table 表
	 * @param array $params 数据
	 * @return string
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function make_update_sql($table, $params, $where=null,$other_params=null)
	{
		$set_str = '';
		if (is_string($params))
		{
			$set_str .= $params;
		}
		else if (is_array($params))
		{
			foreach ($params as $k => $v)
			{
				$set_str .= "$k='$v',";
			}
		}
		if (is_string($other_params) && !empty($other_params))
		{
			$set_str .= $other_params.',';
		}
		else if (is_array($other_params))
		{
			foreach ($other_params as $v)
			{
				$set_str .= "$v,";
			}
		}
		$set_str = rtrim($set_str, ',');
		$wh = array ();
		if (is_array($where))
		{
			foreach ($where as $k => $v)
			{
				if(is_string($v) || is_int($v))
				{
					$wh[] = "$k='$v'";
				}
				else if(is_array($v))
				{
					if(!empty($v))
					{
						$wh[] = "$k in ('" . implode("','", $v) . "') ";
					}
				}
			}
			!empty($wh) && $where = implode(' and ', $wh);
		}
		if (is_string($where) && $where)
		{
			$where = "where $where";
		}
		$sql = "update $table set $set_str  $where";
		return $sql;
	}
	
	
	
	/**
	 * 组装 update sql语句
	 * @param string $table 表
	 * @param array $params 数据
	 * @return string
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function make_query_sql($table, $where)
	{
	    $wh = array ();
	    if (is_array($where))
	    {
	        foreach ($where as $k => $v)
	        {
	            if(is_string($v) || is_int($v))
	            {
	                $wh[] = "$k='$v'";
	            }
	            else if(is_array($v))
	            {
	                if(!empty($v))
	                {
	                    $wh[] = "$k in ('" . implode("','", $v) . "') ";
	                }
	            }
	        }
	        $where = (!empty($wh) && $where) ? implode(' and ', $wh) : '';
	    }
	    if (is_string($where) && $where)
	    {
	        $where = "where $where";
	    }
	    $sql = "select * from $table  $where";
	    return $sql;
	}
	
	/**
	 * 组装 update sql语句
	 * @param string $table 表
	 * @param array $params 数据
	 * @return string
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function make_delete_sql($table, $where)
	{
	    $wh = array ();
	    if (is_array($where))
	    {
	        foreach ($where as $k => $v)
	        {
	            if(is_string($v) || is_int($v))
	            {
	                $wh[] = "$k='$v'";
	            }
	            else if(is_array($v))
	            {
	                if(!empty($v))
	                {
	                    $wh[] = "$k in ('" . implode("','", $v) . "') ";
	                }
	            }
	        }
	        $where = (!empty($wh) && $where) ? implode(' and ', $wh) : '';
	    }
	    if(strlen($where) <1)
	    {
	        return '';
	    }
	    $sql = "delete from $table where $where";
	    return $sql;
	}
	
	
	/**
	 * 公共返回数据
	 * @param number $ret 状态码
	 * @param string $reason 原因描述
	 * @param array|string $data 返回数据
	 * @param array $page_data 分页数据
	 * @param array $other_data 其他扩展数据
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function return_data($ret,$reason=null,$data=null,$page_data=null,$other_data=null)
	{
		$temp_array=array(
				'ret'=>$ret,
				'reason'=>$reason,
				'data_info'=>null
		);
		if($data !== null)
		{
			$temp_array['data_info']=$data;
		}
		if(!empty($page_data))
		{
			$temp_array['page_info']=self::$page_info;
			$temp_array['page_info']['total_count'] = isset($page_data[0]) ? array_pop($page_data[0]) : 0;
		}
		if(!empty($other_data))
		{
			$temp_array['other_info']=$other_data;
		}
		return $temp_array;
	}
	
	/**
	 * 分页信息组装
	 * @param array $page_info 分页信息
	 * @return string
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function make_page_limit($page_info)
	{
		if(isset($page_info['page_count']))
		{
			if($page_info['page_count'] == '0')
			{
				return '';
			}
			self::$page_info = array(
					'page_count'=>intval($page_info['page_count']),
			);
			return " limit ".self::$page_info['page_count'];
		}
		if(isset($page_info['page_size']) && isset($page_info['page_num']))
		{
			self::$page_info = array(
					'page_size'=>intval($page_info['page_size']),
					'page_num'=>intval($page_info['page_num']),
			);
		}
		$str_start = (self::$page_info['page_num'] - 1) * self::$page_info['page_size'];
		return " limit {$str_start} , ".self::$page_info['page_size'];
	}
	
	/**
	 * 排序组装
	 * @param string $order_info
	 * @return string
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function make_order($order_info=null)
	{
		$str_order='';
		if(empty($order_info))
		{
			return $str_order;
		}
		$str_order = "order by";
		if(is_array($order_info))
		{
			foreach ($order_info as $order_val)
			{
				$str_order.=" {$order_val},";
			}
		}
		else if(is_string($order_info))
		{
			$str_order.=" {$order_info},";
		}
		else
		{
			return '';
		}
		$str_order = trim($order_info,',');
		if(strlen($str_order) > 8)
		{
			return $str_order;
		}
		return '';
	}
	/**
	 * 分组组装
	 * @param string $group_info
	 * @return string
	 * @author liangpan
	 * @date 2015-09-07
	 */
	public static function make_group($group_info=null)
	{
		$str_group='';
		if(empty($group_info))
		{
			return $str_group;
		}
		$str_group = "group by";
		if(is_array($group_info))
		{
			foreach ($group_info as $group_val)
			{
				$str_group.=" {$group_val},";
			}
		}
		else if(is_string($group_info))
		{
			$str_group.=" {$group_info},";
		}
		else
		{
			return '';
		}
		$str_group = trim($str_group,',');
		if(strlen($str_group) > 8)
		{
			return $str_group;
		}
		return '';
	}
}