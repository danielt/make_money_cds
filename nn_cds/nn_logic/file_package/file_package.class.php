<?php
/**
 * 上游下发的文件包
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_file_package extends nl_public
{
	static $base_table='nns_file_package';
	static $arr_filed = array(
			'nns_id',
			'nns_name',
			'nns_desc',
	        'nns_cp_id',
			'nns_import_id',
	        'nns_import_source',
			'nns_deleted',
			'nns_file_url',
	        'nns_service_url',
			'nns_file_size',
	        'nns_domain',
	        'nns_integer_id',
			'nns_hot_degree',
			'nns_deliver_time',
	        'nns_create_time',
	        'nns_modify_time',
	        'nns_ext_url',
	);
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function query_by_id($dc,$nns_id)
	{
		$sql = "select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
		return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function query_by_cp_id($dc,$nns_cp_id)
	{
	    $sql = "select * from " . self::$base_table . " where nns_cp_id='{$nns_cp_id}'";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
	
	
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function query_by_ids($dc,$nns_id)
	{
	    $str_id = is_array($nns_id) ? implode("','", $nns_id) : $nns_id;
	    $sql = "select * from " . self::$base_table . " where nns_id in('{$nns_id}')";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 添加内容提供商数据
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function add($dc,$params)
	{
	    $params['nns_id'] = (isset($params['nns_id']) && !empty($params['nns_id'])) ? $params['nns_id'] : np_guid_rand();
		$params['nns_create_time'] = $params['nns_modify_time'] = date('Y-m-d H:i:s');
		if(!isset($params['nns_deliver_time']) || empty($params['nns_deliver_time']))
		{
		    $params['nns_deliver_time'] = date("Y-m-d H:i:s");
		}
		$params['nns_deliver_time'] = (!isset($params['nns_deliver_time']) || empty($params['nns_deliver_time'])) ?  date("Y-m-d H:i:s") : date("Y-m-d H:i:s",strtotime($params['nns_deliver_time']));
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$sql = self::make_insert_sql(self::$base_table, $params);
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 修改内容提供商数据
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function edit($dc,$params,$nns_id)
	{
		if(strlen($nns_id)<0)
		{
			return self::return_data(1,'内容提供商数据_id为空');
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$params['nns_modify_time'] = date("Y-m-d H:i:s");
		$sql = self::make_update_sql(self::$base_table, $params,array('nns_id'=>$nns_id));
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}

	
	/**
	 * 查询内容提供商数据列表
	 * @param object $dc 数据库对象
	 * @param array $params 查询参数
	 * @param array $page_info 分页信息
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-02-27
	 */
	static public function query($dc,$params,$page_info=null)
	{
		if(isset($params['where']) && !empty($params['where']))
		{
			foreach ($params['where'] as $where_key=>$where_val)
			{
				self::$str_where.=" {$where_key} = '{$where_val}' and ";
			}	
		}
		if(isset($params['like']) && !empty($params['like']))
		{
			foreach ($params['like'] as $like_key=>$like_val)
			{
				self::$str_where.=" {$like_key} like '%{$like_val}%' and ";
			}
		}
		self::$str_where = (strlen(self::$str_where) > 5) ? trim(self::$str_where,'and ') : '';
		$str_limit = self::make_page_limit($page_info);
		$sql="select * from " . self::$base_table . " " . self::$str_where . " order by nns_create_time desc {$str_limit} ";
		$sql_count = "select count(*) as count from " . self::$base_table . " " . self::$str_where;
		$result_count = nl_query_by_db($sql_count,  $dc->db());
		$result = nl_query_by_db($sql, $dc->db());
		self::init_where();
		if(!$result || !$result_count)
		{
			return self::return_data(1,"查询数据失败".$sql.$sql_count);
		}
		return self::return_data(0,"查询数据成功".$sql_count.$sql,$result,$result_count);
	}
	
	/**
	 * 查询是否存在
	 * @param unknown $dc
	 * @param string $str_param
	 */
	static public function query_unique($dc,$arr_param)
	{
	    $arr_param = self::except_useless_params(self::$arr_filed, $arr_param);
	    if(empty($arr_param) || !is_array($arr_param))
	    {
	        return self::return_data(1,"查询数据包唯一性参数为空".$arr_param);
	    }
	    $where_params = array();
	    foreach ($arr_param as $key=>$val)
	    {
	        $where_params[] = "{$key}='{$val}'";
	    }
	    $sql="select * from " . self::$base_table . " where ".implode(" and ", $where_params);
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,"查询数据失败".$sql);
	    }
	    return self::return_data(0,"查询数据成功".$sql,$result);
	}
	
	
	/**
	 * 依据条件修改数据
	 * @param unknown $dc
	 * @param unknown $edit_params
	 * @param unknown $where_params
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 */
	static public function edit_by_params($dc,$edit_params,$where_params)
	{
	    $edit_params = self::except_useless_params(self::$arr_filed, $edit_params);
	    $where_params = self::except_useless_params(self::$arr_filed, $where_params);
	    if(empty($edit_params) || !is_array($edit_params))
	    {
	        return self::return_data(1,'修改参数非法'.var_export($edit_params,true));
	    }
	    if(empty($where_params) || !is_array($where_params))
	    {
	        return self::return_data(1,'条件参数非法'.var_export($where_params,true));
	    }
	    $edit_params['nns_modify_time'] = date("Y-m-d H:i:s");
	    $temp_where_params = $temp_edit_params = array();
	    foreach ($edit_params as $key=>$val)
	    {
	        $temp_edit_params[] = "{$key}='{$val}'";
	    }
	    foreach ($where_params as $key=>$val)
	    {
	        $temp_where_params[] = "{$key}='{$val}'";
	    }
	    $sql = "update " . self::$base_table . " set " . implode(',', $temp_edit_params) . " where " . implode(" and ", $temp_where_params);
	    $result = nl_execute_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库执行失败'.$sql);
	    }
	    return self::return_data(0,'ok'.$sql);
	}
    /**
     * 根据文件包ID获取文件包信息
     * @param $dc
     * @param $arr_id 文件包ID一维数组
     * @return array
     */
    static public function query__by_id($dc,$arr_id)
    {
        if(empty($arr_id))
        {
            return self::return_data(1,'参数错误');
        }
        $str_ids = implode("','",$arr_id);
        $sql = "select * from " . self::$base_table . " where nns_id in ('{$str_ids}')";
        $file_re = nl_query_by_db($sql,$dc->db());
        if(!is_array($file_re))
        {
            return self::return_data(1,'数据库执行失败或数据查询为空：'.$sql);
        }
        $result = array();
        foreach ($file_re as $info)
        {
            $result[$info['nns_id']]['base_info'] = $info;
        }
        if(empty($result))
        {
            return self::return_data(1,'file_package数据查询结果为空');
        }
        return self::return_data(0,'ok'.$sql,$result);
    }

    /**
     * 根据sql执行查询
     * @param $dc
     * @param $sql
     * @return array
     */
    static public function query_by_sql($dc, $sql)
    {
        if (empty($sql))
        {
            return self::return_data(1, 'sql为空');
        }
        $result = nl_query_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, "查询数据信息失败" . $sql);
        }
        return self::return_data(0, "查询数据信息成功", $result);
    }
}
