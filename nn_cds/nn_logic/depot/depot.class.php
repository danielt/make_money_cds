<?php
/**
 * @author guanghu.zhou
 * @refactor date 2013-04-16
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'public.class.php';
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'depot/depot_v2.class.php';

class nl_depot extends nl_public
{
    static $base_table = 'nns_depot';
    //缓存时间
    static private $prefix = "depot_logic#|#";

    static $arr_filed = array(
        'nns_id',
        'nns_name',
        'nns_category',
        'nns_org_id',
        'nns_org_type',
        'nns_type',
        'nns_create_time',
        'nns_modify_time',
    );

    /**
     * @subpackage 获取depot列表信息
     * @param $param = array(
     *      'name'=>$name,
     *      'org_type'=>$nns_org_type,
     *      'org_id'=>$nns_org_id,
     *      'depot_type'=>$depot_type,
     *      );
     * @return array
     * @notice 原函数名称 nns_db_depot_list
     * @access public
     * @param dc  obj
     */

    static public function get_depot_list($dc, $policy, $param = null)
    {
        /*$param = array(
         'name'=>$name,
         'org_type'=>$nns_org_type,
         'org_id'=>$nns_org_id,
         'depot_type'=>$depot_type,
        );*/
        $key = 'depot_list' . implode('-', $param);
        switch ($policy)
        {
            case NL_DC_DB :
                $result = self::_get_depot_list_by_db($dc->db(), $param);
                break;
            case NL_DC_CACHE :
                $result = $dc->cache()->get(self::$prefix . $key);
                break;
            case NL_DC_AUTO :
                if ($dc->is_cache_enabled())
                {
                    $result = $dc->cache()->get(self::$prefix . $key);
                    if ($result === FALSE)
                    {
                        $result = self::_get_depot_list_by_db($dc->db(), $param);
                        $dc->cache()->set(self::$prefix . $key, $result);
                    }
                } else
                {
                    $result = self::_get_depot_list_by_db($dc->db(), $param);
                }
                break;
        }
        return $result;
    }

    /**
     * @subpackage 通过数据库获取depot列表信息
     * @param param array
     * @return array
     * @access private
     * @param dc  obj
     */

    static private function _get_depot_list_by_db($db, $param)
    {
        extract($param);
        unset($param);
        $str_sql = "select * from nns_depot ";
        $str_sql .= self::_get_depot_where(0, 0, $name, $org_type, $org_id, $depot_type);
        $data = nl_db_get_all($str_sql, $db);

        $result = array('ret' => 0, 'data' => $data, 'msg' => null);
        return $result;
    }

    /**
     * @subpackage 组织sql语句
     * @param name    string
     * @param org_type    integer
     * @param org_id    string
     * @param depot_type    integer
     * @param min    integer
     * @param num integer
     * @return array
     * @notice 原函数名称 nns_db_depot_where
     * @access private
     */

    static private function _get_depot_where($min, $num, $name, $org_type, $org_id, $depot_type)
    {
        $where = "";

        if (isset($name))
        {
            if (!empty($name))
            {
                if (empty($where))
                {
                    $where .= " where nns_name like '%$name%' ";
                } else
                {
                    $where .= " and nns_name like '%$name%'";
                }
            }
        }
        if (isset($org_type))
        {
            if ($org_type !== null && $org_type !== '' && $org_type != -1)
            {
                $org_type = ( int )$org_type;
                if (empty($where))
                {
                    $where .= " where nns_org_type = $org_type ";
                } else
                {
                    $where .= " and nns_org_type = $org_type";
                }
            }
        }
        if (isset($org_id))
        {
            if (!empty($org_id))
            {
                if (empty($where))
                {
                    $where .= " where nns_org_id = '$org_id' ";
                } else
                {
                    $where .= " and nns_org_id = '$org_id'";
                }
            }
        }
        if (isset($depot_type))
        {
            if ($depot_type !== null && $depot_type !== '' && $depot_type != -1)
            {
                $depot_type = ( int )$depot_type;
                if (empty($where))
                {
                    $where .= " where nns_type = $depot_type ";
                } else
                {
                    $where .= " and nns_type = $depot_type";
                }
            }
        }
        if (isset($min) && isset($num))
        {
            if (!empty($num))
            {
                $min = ( int )$min;
                $num = ( int )$num;
                $where .= " limit $min,$num";
            }
        }
        return $where;
    }

    /**
     * 获取资源库栏目列表
     * @param $dc //数据库对象
     * @param array $params 参数数组
     * @param string $type video 点播 | live  直播
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     * @date 2016-04-20
     */
    static public function get_depot_info($dc, $params = null, $type = 'video')
    {
        $type = ($type == 'video') ? '0' : '1';
        $sql = "select * from " . self::$base_table . " where nns_type='{$type}' ";
        if (is_array($params) && !empty($params))
        {
            foreach ($params as $key => $val)
            {
                if (strpos($key, 'nns_') === false)
                {
                    $key = "nns_" . $key;
                }
                $sql .= " and {$key} = '{$val}' ";
            }
        }
        $sql .= " order by nns_create_time asc ";
        $result = nl_query_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, "查询数据失败" . $sql);
        }
        return self::return_data(0, "查询数据成功" . $sql, $result);
    }

    /**
     * 获取资源库栏目列表
     * @param onject $dc 数据库对象
     * @param array $params 参数数组
     * @param string $type video 点播 | live  直播
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     * @date 2016-04-20
     */
    static public function get_depot_by_id($dc, $nns_id)
    {
        $sql = "select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
        $result = nl_query_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, "查询数据失败" . $sql);
        }
        $result = (isset($result[0]) && is_array($result[0]) && !empty($result[0])) ? $result[0] : null;
        return self::return_data(0, "查询数据成功", $result);
    }


    /**
     * 修改栏目信息
     * @param object $dc 数据库对象
     * @param array $params 数据数组
     * @param string $nns_id GUID
     * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     * @date 2016-09-24
     */
    static public function edit($dc, $params, $nns_id)
    {
        if (isset($params['nns_id']))
        {
            unset($params['nns_id']);
        }
        $params = self::make_nns_pre($params);
        if (strlen($nns_id) < 0)
        {
            return self::return_data(1, 'CDN直播频道队列guid为空');
        }
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (empty($params))
        {
            return self::return_data(1, '参数为空');
        }
        if (!isset($params['nns_modify_time']) || strlen($params['nns_modify_time']) < 1)
        {
            $params['nns_modify_time'] = date("Y-m-d H:i:s");
        }
        $sql = self::make_update_sql(self::$base_table, $params, array('nns_id' => $nns_id));
        $result = nl_execute_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        return self::return_data(0, 'ok');
    }

    /**
     * 依据条件查询栏目信息
     * @param $dc
     * @param $params
     * @return array
     */
    static public function query_by_condition($dc, $params)
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (!is_array($params) || empty($params))
        {
            return self::return_data(1, '查询条件为空不允许查询');
        }
        $sql = self::make_query_sql(self::$base_table, $params);
        $sql .= " order by nns_create_time asc ";
        $result = nl_query_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (isset($result) && !empty($result)) ? $result : null;
        return self::return_data(0, 'OK' . $sql, $result);
    }


    /**
     * 添加栏目信息
     * @param object $dc 数据库对象
     * @param array $params 数据数组
     * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     * @date 2016-03-06
     */
    static public function add($dc, $params)
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (empty($params) || !is_array($params))
        {
            return self::return_data(1, '参数为空');
        }
        $params['nns_create_time'] = $params['nns_modify_time'] = date('Y-m-d H:i:s');
        $sql = self::make_insert_sql(self::$base_table, $params);
        $result = nl_execute_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库执行失败' . $sql);
        }
        return self::return_data(0, 'ok:' . $sql);
    }
}
