<?php
/**
 * 资源库栏目v2
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/12/4 14:50
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_depot_v2 extends nl_public
{
    static public $table = "nns_depot_v2";
    static public $arr_filed = array(
        'nns_id',
        'nns_name',
        'nns_category_id',
        'nns_parent_id',
        'nns_import_category_id',
        'nns_import_parent_category_id',
        'nns_depot_id',
        'nns_org_id',
        'nns_order',
        'nns_type',
        'nns_create_time',
        'nns_modify_time',
    );

    /**
     * 添加资源库栏目
     * @param $dc
     * @param $params
     * @param $is_filter_name
     * @return array
     */
    static public function add($dc, $params, $is_filter_name=true)
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if(empty($params) || !is_array($params))
        {
            return self::return_data(1,'参数错误');
        }
        if (!isset($params['nns_name']) || !isset($params['nns_category_id']))
        {
            return self::return_data(1,'栏目名称和栏目id不能为空');
        }

        $params['nns_id'] = np_guid_rand();
        $params['nns_create_time'] = date('Y-m-d H:i:s',time());
        $params['nns_modify_time'] = $params['nns_create_time'];
        $params['nns_type'] = isset($params['nns_type']) ? $params['nns_type'] : 0;

        if($is_filter_name )
        {
            /**
             * 进行名称重复判断
             */
            $parent_category = substr($params['nns_category_id'], 0, -3);
            if($parent_category == '10000')
            {
                $parent_category = '0';
            }
            if(isset($params["nns_name"]))
            {
                $nns_name = $params['nns_name'];
                $where = " where nns_name = '{$nns_name}' and nns_type = '{$params['nns_type']}' and nns_parent_id = '{$parent_category}' and nns_category_id != '{$params['nns_category_id']}'";

                $query_sql = "select * from " . self::$table . $where;
                $re_exist = nl_query_by_db($query_sql, $dc->db());
            }

            if(isset($re_exist) && is_array($re_exist))
            {
                return self::return_data(1, '名称重复');
            }
        }

        $result = nl_db_insert($dc->db(), self::$table, $params);
        if($result)
        {
            return self::return_data(0,'OK');
        }
        else
        {
            self::return_data(1,'mysql execute failed');
        }
    }

    /**
     * 修改资源库栏目
     * @param $dc
     * @param $params
     * @param $where
     * @return array
     */
    static public function modify($dc,$params,$where)
    {
        if(empty($where) || !is_array($where) || empty($params) || !is_array($params))
        {
            return self::return_data(1,'参数错误');
        }
        $update_str = '';
        $where_str = '';
        $params['nns_modify_time'] = date('Y-m-d H:i:s',time());
        foreach ($params as $key=>$value)
        {
            $update_str .= " {$key} = '{$value}',";
        }
        $update_str = rtrim($update_str,',');
        foreach ($where as $k=>$val)
        {
            if($k == 'in' && is_array($val))
            {
                foreach ($val as $l => $v)
                {
                    $where_str .= " {$l} in ('" . implode("'.'", $v) . "') AND ";
                }
            }
            elseif(!empty($val) || $val == '0')
            {
                $where_str .= " {$k} = '{$val}' AND ";
            }
        }
        $where_str = rtrim($where_str,' AND ');
        $sql = "update " . self::$table . " set " . $update_str . " where " . $where_str;
        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'失败'.$sql);
        }
        return self::return_data(0,'成功');
    }

    /**
     * @param $dc
     * @param $category_id //栏目id
     * @param $depot_id //资源库id
     * @param int $type //资源类型
     * @param $params
     * @param bool $is_filter_name //是否检查名称重复
     * @return array
     */
    static public function edit($dc, $category_id, $depot_id, $type=0, $params, $is_filter_name=true)
    {
        $add_data = self::except_useless_params(self::$arr_filed, $params);
        $date_time = date('Y-m-d H:i:s');
        $add_data['nns_modify_time'] = $date_time;
        $arr_where = array();

        if($is_filter_name)
        {
            /**
             * 进行名称重复判断
             */
            $parent_category = substr($category_id,0,-3);
            if($parent_category == '10000')
            {
                $parent_category = '0';
            }
            if(isset($add_data["nns_name"]))
            {
                $nns_name = $add_data['nns_name'];
                $where = " where nns_name = '{$nns_name}' and nns_type = '{$type}' and nns_parent_id = '{$parent_category}' and nns_category_id != '{$category_id}'";

                $query_sql = "select * from " . self::$table . $where;
                $re_exist = nl_query_by_db($query_sql, $dc->db());
            }

            if(isset($re_exist) && is_array($re_exist))
            {
                return self::return_data(1, '名称重复');
            }
        }

        if(strlen($category_id) > 0)
        {
            $arr_where['nns_category_id'] = $category_id;
        }
        if(strlen($depot_id) > 0)
        {
            $arr_where['nns_depot_id'] = $depot_id;
        }
        $arr_where['nns_type'] = $type;
        $result = self::modify($dc, $add_data, $arr_where);
        if($result['ret'] != 0)
        {
            return self::return_data(1, '修改失败' . $result['reason']);
        }
        return self::return_data(0, 'success');
    }

    /**
     * 根据条件获取资源库栏目
     * @param $dc
     * @param $params
     * @param null $since
     * @param null $num
     * @param bool $is_count
     * @return array
     */
    static public function get_depot_list($dc,$params,$since=null,$num=null,$is_count=false)
    {
        $where = '';
        $str_limit = '';
        if(is_array($params))
        {
            foreach ($params as $key=>$value)
            {
                if($key === 'nns_content' && strlen($value) > 0)
                {
                    $where .= " {$key} like '%{$value}%' AND ";
                }
                elseif(strlen($value) > 0)
                {
                    $where .= " {$key} = '{$value}' AND ";
                }
            }
        }
        if(!empty($where))
        {
            $where = rtrim($where," AND ");
            $where = " where " . $where;
        }
        if(strlen($since) > 0 && strlen($num) > 0)
        {
            $str_limit = " limit {$since},{$num}";
        }
        $sql = "select * from " . self::$table . $where . " order by nns_create_time desc " . $str_limit;
        $data = nl_query_by_db($sql, $dc->db());
        if($data === false)
        {
            return self::return_data(1,'数据库执行失败'.$sql);
        }
        if($data === true)
        {
            $data = array();
        }
        $count_info = array();
        if($is_count)
        {
            $count_sql = "select count(1) as count from " . self::$table . $where;
            $count_info = nl_query_by_db($count_sql, $dc->db());
            if($count_info === false)
            {
                return self::return_data(1,'数据库执行失败'.$count_sql);
            }
        }
        return self::return_data(0,'ok',$data,$count_info);
    }

    /**
     * 根据id查询单个资源库栏目信息
     * @param $dc
     * @param $nns_id
     * @return array
     */
    static public function query_by_id($dc,$nns_id)
    {
        if(empty($nns_id))
        {
            return self::return_data(1,'id为空');
        }
        $sql="select * from " . self::$table . " where nns_id='{$nns_id}' limit 1";
        $result = nl_query_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,"失败".$sql);
        }
        return self::return_data(0,"成功",$result);
    }

    /**
     * 依据条件查询媒资信息
     * @param $dc
     * @param $params
     * @return array
     */
    static public function query_by_condition($dc, $params, $need_parent_name = false)
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (!is_array($params) || empty($params))
        {
            return self::return_data(1, '查询条件为空不允许查询');
        }
        $sql = self::make_query_sql(self::$table, $params);

        $result = nl_query_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (isset($result) && !empty($result)) ? $result : null;
        return self::return_data(0, 'OK' . $sql, $result);
    }

    /**
     * @param $dc
     * @param $params
     * @param string $field
     * @param string $order
     * @return array
     */
    static public function query($dc, $params, $field = '*', $order = '')
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (!is_array($params) || empty($params))
        {
            return self::return_data(1, '查询条件为空不允许查询');
        }

        $wh = array();
        $where = '';
        if (is_array($params))
        {
            foreach ($params as $k => $v)
            {
                if(is_string($v) || is_int($v))
                {
                    $wh[] = "$k='$v'";
                }
                else if(is_array($v))
                {
                    if(!empty($v))
                    {
                        $wh[] = "$k in ('" . implode("','", $v) . "') ";
                    }
                }
            }
            $where = (!empty($wh) && $params) ? implode(' and ', $wh) : '';
        }
        if (is_string($where) && $where)
        {
            $where = " where $where";
        }

        if (strlen($order) > 0)
        {
            $order = ' order by ' . $order;
        }

        $sql = 'select ' . $field . ' from ' . self::$table . $where . $order;

        $result = nl_query_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (isset($result) && !empty($result)) ? $result : null;
        return self::return_data(0, 'OK' . $sql, $result);
    }

    static public function query_by_condition_v2($dc, $params)
    {

    }
    /**
     * 删除资源库栏目，
     * 删除之前请判断是否绑定有媒资
     * @param $dc
     * @param $where
     * @return array
     */
    static public function delete($dc, $where)
    {
        if(!is_array($where) || empty($where))
        {
            return self::return_data(1,'参数错误');
        }
        $wh = array();

        if (!empty($where['nns_category_id']) && isset($where['nns_category_id']))
        {
            $wh[] = " nns_category_id like '{$where['nns_category_id']}%'";
        }
        unset($where['nns_category_id']);

        foreach ($where as $k => $v)
        {
            if(is_string($v) || is_int($v))
            {
                $wh[] = "$k='$v'";
            }
            else if(is_array($v))
            {
                if(!empty($v))
                {
                    $wh[] = "$k in ('" . implode("','", $v) . "') ";
                }
            }
        }
        $where = (!empty($wh) && $where) ? implode(' and ', $wh) : '';

        $where_str = '';
        if (is_string($where) && $where)
        {
            $where_str = " where $where";
        }

        $str_sql = "delete from " . self::$table . $where_str;

        $result = nl_execute_by_db($str_sql, $dc->db());
        if($result === false)
        {
            return self::return_data(1,'mysql execute failed');
        }
        else
        {
            return self::return_data(0,'OK');
        }
    }
}