<?php
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
class nl_template {
	const CACHE_TIME_OUT = 300;
    
    
/**
 * 取得从物理磁盘入库的模板
 * @param DC模块
 * @param Array(
 * 				"type"=>类型查询
 * 			)
 * @return Array 模板列表
 * 			TRUE 查询成功 无数据
 * 			FALSE 查询失败
 */
    public static function get_template_list($dc,$params=NULL){
		
		$sql = 'select * ' .
				'from nns_template ' ;
				
		if ($params){
			$sql.=' where (nns_type=\''.$params['type'].'\' or nns_type=\'\')';
		}
		
		return nl_query_by_db($sql, $dc->db());
    }
    
  /**
 * 模板入库
 * @param DC模块
 * @param String 模板类型
 * @param String 模板ID
 * @param ARRAY
 * 			Array(
 * 				"name"=>模板名称
 * 				"summary"=>模板说明
 * 				"tags"=>EPG标识
 * 				"type"=>模板类型
 * 			)
 * @return 
 * 			TRUE 成功
 * 			FALSE 失败
 */
    public static function add_template($dc,$template_type,$template_id,$params){
    	$sql = 'insert into nns_template (nns_template_id,nns_template_type,nns_name,nns_summary,nns_tag,nns_type,nns_create_time) ' .
    			'values (' .
				'\''.$template_id.'\',' .
				'\''.$template_type.'\', ' .
				'\''.$params['name'].'\',' .
				'\''.$params['summary'].'\', ' .
				'\''.$params['summary'].'\', ' .
				'\''.$params['type'].'\', ' .
				' now() ' .
				') ';
		
		return nl_execute_by_db($sql, $dc->db());
    }
    
      /**
 * 模板出库
 * @param DC模块
 * @param String 模板类型
 * @param String 模板ID
 * @return 
 * 			TRUE 成功 
 * 			FALSE 失败
 */
    public static function delete_template($dc,$template_type,$template_id){
    	$sql = 'delete from nns_template ' .
    			'where ' .
    			'nns_template_type=\''.$template_type.'\' and '.
    			'nns_template_id=\''.$template_id.'\'  ';
				
//		echo $sql;die;
		return nl_execute_by_db($sql, $dc->db());
    }
    
 /**
 * 修改模板状态
 * @param DC模块
 * @param String 模板类型
 * @param String 模板ID
 * @param ARRAY
 * 			Array(
 * 				"name"=>模板名称
 * 				"summary"=>模板说明
 * 				"tags"=>模板EPG
 * 				"type"=>模板类型
 * 			)
 * @return 
 * 			TRUE 成功 
 * 			FALSE 失败
 */
    public static function modify_template($dc,$template_type,$template_id,$params){
    	$sql = 'update nns_template set  ' .
    			'nns_name=\''.$params['name'].'\',' .
    			'nns_summary=\''.$params['summary'].'\',' .
    			'nns_type=\''.$params['type'].'\',' .
    			'nns_tag=\''.$params['tags'].'\' ' .
    			'where ' .
    			'nns_template_type=\''.$template_type.'\' and '.
    			'nns_template_id=\''.$template_id.'\'  ';
				
//		echo $sql;die;
		return nl_execute_by_db($sql, $dc->db());
    }
    
     /**
 * 获取模板状态
 * @param DC模块
 * @param String 模板类型
 * @param String 模板ID
 * @return 
 * 			Array 模板信息
 * 			TRUE 成功 
 * 			FALSE 失败
 */
    public static function get_template_info($dc,$template_type,$template_id){
    	$sql = 'select * from nns_template  ' .
    			'where ' .
    			'nns_template_type=\''.$template_type.'\' and '.
    			'nns_template_id=\''.$template_id.'\'  ';
				
//		echo $sql;die;
		$result=nl_query_by_db($sql, $dc->db());
		if ($result===TRUE || $result===FALSE) return $result;
		return $result[0];
    }
}
?>