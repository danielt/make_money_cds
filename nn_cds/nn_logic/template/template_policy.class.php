<?php
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
class nl_template_policy {
	const CACHE_TIME_OUT = 300;
    
    
/**
 * 取得库模板生效策略组
 * @param DC模块
 * @param "packet_type"包类型
 * @param "packet_id"包ID
 * @param Array(
 * 				"category_id"=>栏目ID 
 * 				"tag"=>EPG输出
 * 				"item_id"=>资源ID
 * 			)
 * @param  策略
 * @return Array 模板列表
 * 			TRUE 查询成功 无数据
 * 			FALSE 查询失败
 */
    public static function get_template_policy_list($dc,$packet_type,$packet_id,$params,$policy=NL_DC_AUTO){
		$result = null;
		$params['category_id']=empty($params['category_id'])?'1000':$params['category_id'];
		if ($policy == NL_DC_AUTO){
			if ($dc->is_cache_enabled()) {
				$result = self::_get_template_policy_list_by_cache($dc->cache(),$packet_type,$packet_id,$params);
				if ($result === FALSE ) {
					$result = self::_get_template_policy_list_by_db($dc->db(),$packet_type,$packet_id,$params);
					self::_set_template_policy_list_by_cache($dc->cache(),$packet_type,$packet_id,$params,$result);
				}
			}
			else {
				$result = self::_get_template_policy_list_by_db($dc->db(),$packet_type,$packet_id,$params);
			}
		}elseif($policy == NL_DC_DB){
			$result = self::_get_template_policy_list_by_db($dc->db(),$packet_type,$packet_id,$params);
		}elseif($policy == NL_DC_CACHE){
			$result = self::_get_template_policy_list_by_cache($dc->cache(),$packet_type,$packet_id,$params);
		}
		
		return $result;
    }
    
    
    private static function _get_template_policy_list_by_db($db,$packet_type,$packet_id,$params){
    	$sql = 'select * ' .
				'from nns_template_content ' .
				'where ' .
				'nns_packet_type = \''.$packet_type.'\' and ' .
				'nns_packet_id = \''.$packet_id.'\' and ' .
				'LOCATE(nns_category_id,\''.$params['category_id'].'\')=1 ' ;
		if (array_key_exists('tag',$params)){
			if (!empty($params['tag']) || $params['tag']=='0'){
				$tag=$params['tag'];
				$sql .='and (LOCATE(\','.$tag.',\',CONCAT(\',\',nns_tag))>0)  ';
			}
		}
		
		if (array_key_exists('item_id',$params)){
			if (!empty($params['item_id'])){
				$sql .='and ( nns_item_id = \''.$params['item_id'].'\'  ' .
						'or  nns_item_id = \'\' or ISNULL(nns_item_id) ) ';
			}
		}
					
		$sql .=' order by nns_item_id,nns_category_id desc ';
		//var_dump($sql);die;
		return nl_query_by_db($sql, $db);
    }
    
    private static function _get_template_policy_list_by_cache($cache,$packet_type,$packet_id,$params){
    	if ($cache===NULL) return FALSE;
    	//		获取GUID标识
		$comp_key=$cache->get('index_template_policy|#'.$packet_type.'|#'.$packet_id);
		if ($comp_key===FALSE) return $comp_key;
		
    	$key=$comp_key.'|#'.$params['category_id'].'|#'.$params['tag'].'|#'.$params['item_id'];
		return unserialize($cache->get('template_policy|#'.$key));
    }
    
    private static function _set_template_policy_list_by_cache($cache,$packet_type,$packet_id,$params,$data){
    	if ($cache===NULL) return FALSE;
    	$comp_key=$cache->get('index_template_policy|#'.$packet_type.'|#'.$packet_id);
		$comp_key=($comp_key===FALSE)?np_guid_rand():$comp_key;
		$cache->set('index_template_policy|#'.$packet_type.'|#'.$packet_id,$comp_key);
		
    	$key=$comp_key.'|#'.$params['category_id'].'|#'.$params['tag'].'|#'.$params['item_id'];
		return $cache->set('template_policy|#'.$key,serialize($data),self::CACHE_TIME_OUT);
    }
    
    public static function delete_template_policy_list_by_cache($dc,$packet_type,$packet_id){
    	if ($dc->cache()===NULL) return FALSE;
		return $dc->cache()->delete('index_template_policy|#'.$packet_type.'|#'.$packet_id);
    }
    
  /**
 * 添加模板策略
 * @param DC模块
 * @param "packet_type"包类型
 * @param "packet_id"包ID
 * @param Array(
 * 				"template_type"=>模板类型
 * 				"template_id"=>模板ID
 * 				"category_id"=>栏目ID 
 * 				"tag"=>EPG输出,
 * 				"item_id"=>资源ID 
 * 			)
 * @return 
 * 			TRUE 成功
 * 			FALSE 失败
 */
    public static function add_template_policy ($dc,$packet_type,$packt_id,$params){
    	$params['id']=np_guid_rand();
    	$params['packet_type']=$packet_type;
    	$params['packet_id']=$packt_id;
    	
    	$result=nl_db_insert(
    	$dc->db(),
		'nns_template_content',
		$params
		);
//		var_dump($sql);die;
//    	$result=nl_execute_by_db($sql, $dc->db());
    	
    	if ($result===TRUE) self::delete_template_policy_list_by_cache($dc,$packet_type,$packt_id);
		
		return $result;
    }
    
      /**
 * 删除模板所有策略 未清缓存
 * @param DC模块 
 * @param String 模板类型
 * @param String 模板ID
 * @return 
 * 			TRUE 成功 
 * 			FALSE 失败
 */
    public static function delete_template_policy($dc,$template_type,$template_id){
    	$sql = 'delete from nns_template_content ' .
    			'where ' .
    			'nns_template_type=\''.$template_type.'\' and '.
    			'nns_template_id=\''.$template_id.'\'  ';
				
//		echo $sql;die;
		
		$result=nl_execute_by_db($sql, $dc->db());
    	
//    	if ($result===TRUE) self::delete_template_policy_list_by_cache($dc,$packet_type,$packt_id);
			
		return $result;
    }
    
        /**
 * 删除模板策略
 * @param DC模块
 * @param String 策略ID
 * @return 
 * 			TRUE 成功 
 * 			FALSE 失败
 */
    public static function delete_template_policy_by_id($dc,$id){
    	$policy_info=self::get_template_policy_info($dc,$id);
    	
    	
    	
    	$sql = 'delete from nns_template_content ' .
    			'where ' .
    			'nns_id=\''.$id.'\'  ';
    	
    	$result=nl_execute_by_db($sql, $dc->db());
    	
    	if ($result===TRUE) self::delete_template_policy_list_by_cache(
    	$dc,
    	$policy_info['nns_packet_type'],
    	$policy_info['nns_packet_id']);
				
//		echo $sql;die;
		return $result;
    }
    
 /**
 * 修改策略
 * @param DC模块
 * @param String 策略ID
 * @param String  模板EPG
 * @return 
 * 			TRUE 成功 
 * 			FALSE 失败
 */
    public static function modify_template_policy_tag($dc,$id,$tag){
    	$policy_info=self::get_template_policy_info($dc,$id);
    	
    	
    	$sql = 'update nns_template_content set  ' .
    			'nns_tag=\''.$tag.'\' ' .
    			'where ' .
    			'nns_id=\''.$id.'\'   ';
    			
    	$result=nl_execute_by_db($sql, $dc->db());
    	
    	if ($result===TRUE) self::delete_template_policy_list_by_cache(
    	$dc,
    	$policy_info['nns_packet_type'],
    	$policy_info['nns_packet_id']);
				
//		echo $sql;die;
		return $result;
    }
    
     /**
 * 获取模版策略详细
 * @param DC模块
 * @param String 策略ID
 * @return 
 * 			Array 模板策略信息
 * 			TRUE 成功 
 * 			FALSE 失败
 */
    public static function get_template_policy_info($dc,$id){
    	$sql = 'select * from nns_template_content  ' .
    			'where ' .
    			'nns_id=\''.$id.'\'  ';
				
//		echo $sql;die;
		$result=nl_query_by_db($sql, $dc->db());
		if ($result===TRUE || $result===FALSE) return $result;
		return $result[0];
    }
}
?>