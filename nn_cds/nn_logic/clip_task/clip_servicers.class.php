<?php
/**
 * 切片服务器管理
 * @author chenjing
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_clip_servicers extends nl_public
{
	static $base_table='nns_mgtvbk_clip_servicers';
	static $arr_filed = array(
			'nns_id',
			'nns_tom3u8_id',
			'nns_name',
			'nns_ip',
			'nns_create_time',
			'nns_modify_time',
			'nns_desc',
	);
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id  $nns_tom3u8_id
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_by_id($dc,$nns_id)
	{
		$sql = "select * from " . self::$base_table . " where nns_id='{$nns_id}' or nns_tom3u8_id='{$nns_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
		return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 查询列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_by_ids($dc,$arr_id)
	{
	    if(!is_array($arr_id) || empty($arr_id))
	    {
	        return self::return_data(0,'OK');
	    }
	    $str_id = implode("','", $arr_id);
	    $sql = "select * from " . self::$base_table . " where nns_id='{$str_id}' ";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 依据条件查询数据
	 * @param unknown $dc
	 * @param unknown $arr_params
	 * @param string $limit
	 */
	static public function query_by_params($dc,$arr_params,$limit=null)
	{
	    $arr_params = self::except_useless_params(self::$arr_filed, $arr_params);
	    if(empty($arr_params) || !is_array($arr_params) )
	    {
	        return self::return_data(1,'查询参数为空');
	    }
	    $limit = ($limit === null || strlen($limit)<1) ? 0 : (int)$limit;
	    $str_limit = $limit >0 ? " limit {$limit} " : '';
	    $arr_where=null;
	    foreach ($arr_params as $key=>$val)
	    {
	        if(is_array($val))
	        {
	            $val = array_values($val);
	            $arr_where[]= " {$key} in('" . implode("','", $val) . "') ";
	        }
	        else
	        {
	            $arr_where[]= " {$key}='{$val}' ";
	        }
	    }
	    $sql="select * from " . self::$base_table . " where " . implode(" and ", $arr_where) . " order by nns_priority desc,nns_create_time asc  {$str_limit} ";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (is_array($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
    /**
     * 查询列表
     * @param object $dc 数据库对象
     * @param array $params 查询参数
     * @param array $page_info 分页信息
     * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     * @date 2016-03-06
     */
    static public function query($dc,$params,$page_info=null)
    {
        if(isset($params['where']) && !empty($params['where']))
        {
            foreach ($params['where'] as $where_key=>$where_val)
            {
                self::$str_where.=" {$where_key} = '{$where_val}' and ";
            }
        }
        if(isset($params['like']) && !empty($params['like']))
        {
            foreach ($params['like'] as $like_key=>$like_val)
            {
                self::$str_where.=" {$like_key} like '%{$like_val}%' and ";
            }
        }
        self::$str_where = (strlen(self::$str_where) > 5) ? trim(self::$str_where,'and ') : '';
        $str_limit = self::make_page_limit($page_info);
        $sql="select * from " . self::$base_table . " " . self::$str_where . " order by nns_create_time desc {$str_limit} ";
        $sql_count = "select count(*) as count from " . self::$base_table . " " . self::$str_where;
        $result_count = nl_query_by_db($sql_count,  $dc->db());
        $result = nl_query_by_db($sql, $dc->db());
        self::init_where();
        if(!$result || !$result_count)
        {
            return self::return_data(1,"查询数据失败".$sql.$sql_count);
        }
        return self::return_data(0,"查询数据成功".$sql_count.$sql,$result,$result_count);
    }
	/**
	 * 添加切片服务器信息
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author  chenjing
	 * @date 2018-03-21
	 */
	static public function add($dc,$params)
	{
		if(!isset($params['nns_tom3u8_id']) || strlen($params['nns_tom3u8_id'])<1)
		{
			return self::return_data(1,'nns_tom3u8_id 为空');
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$date_time = date("Y-m-d H:i:s");
		if(!isset($params['nns_create_time']) || empty($params['nns_create_time']))
		{
			$params['nns_create_time'] = $date_time;
		}
		if(!isset($params['nns_modify_time']) || empty($params['nns_modify_time']))
		{
			$params['nns_modify_time'] = $date_time;
		}
		$sql = self::make_insert_sql(self::$base_table, $params);
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 检查数据是否存在
	 * @param object $dc
	 * @param string $media_id 片源id
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function check_exsist($dc,$media_id)
	{
		$sql="select nns_id from " . self::$base_table . " where nns_video_media_id='{$media_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result[0]['nns_id']) && !empty($result[0]['nns_id'])) ? $result[0]['nns_id'] : null;
		return self::return_data(0,'OK',$result);
	}
	
	
	/**
	 * 修改切片服务器信息
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author chenjing
	 * @date 2018-03-21
	 */
	static public function edit($dc,$params,$nns_id)
	{
		if((!is_string($nns_id) && !is_array($nns_id)) || (is_string($nns_id) && strlen($nns_id)<0) || (is_array($nns_id) && empty($nns_id)))
		{
			return self::return_data(1,'nns_id为空');
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(!isset($params['nns_modify_time']) || empty($params['nns_modify_time']))
		{
			$params['nns_modify_time'] = date("Y-m-d H:i:s");
		}
		if(empty($params))
		{
		    return self::return_data(1,'参数为空');
		}
		$sql = self::make_update_sql(self::$base_table, $params,array('nns_id'=>$nns_id));
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}

	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function delete($dc,$nns_id)
	{
	    if(strlen($nns_id) <1)
	    {
	        return self::return_data(1,'nns_id为空');
	    }
	    $sql = "delete from " . self::$base_table . " where nns_id='{$nns_id}' ";
	    $result = nl_execute_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    return self::return_data(0,'OK');
	}
}
