<?php
/**
 * CP管理
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_clip_task extends nl_public
{
	static $base_table='nns_mgtvbk_clip_task';
	static $arr_filed = array(
			'nns_id',
			'nns_task_type',
			'nns_org_id',
			'nns_video_id',
			'nns_video_type',
			'nns_video_index_id',
			'nns_content',
			'nns_state',
			'nns_desc',
			'nns_priority',
			'nns_alive_time',
			'nns_start_time',
			'nns_end_time',
			'nns_create_time',
			'nns_modify_time',
			'nns_force',
			'nns_date',
			'nns_new_dir',
			'nns_video_media_id',
			'nns_task_name',
			'nns_op_id',
			'nns_file_hit',
	        'nns_cp_id',
	);
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_by_id($dc,$nns_id)
	{
		$sql = "select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
		return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 查询列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_by_ids($dc,$arr_id)
	{
	    if(!is_array($arr_id) || empty($arr_id))
	    {
	        return self::return_data(0,'OK');
	    }
	    $str_id = implode("','", $arr_id);
	    $sql = "select * from " . self::$base_table . " where nns_id='{$str_id}' ";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 依据条件查询数据
	 * @param unknown $dc
	 * @param unknown $arr_params
	 * @param string $limit
	 */
	static public function query_by_params($dc,$arr_params,$limit=null)
	{
	    $arr_params = self::except_useless_params(self::$arr_filed, $arr_params);
	    if(empty($arr_params) || !is_array($arr_params) )
	    {
	        return self::return_data(1,'查询参数为空');
	    }
	    $limit = ($limit === null || strlen($limit)<1) ? 0 : (int)$limit;
	    $str_limit = $limit >0 ? " limit {$limit} " : '';
	    $arr_where=null;
	    foreach ($arr_params as $key=>$val)
	    {
	        if(is_array($val))
	        {
	            $val = array_values($val);
	            $arr_where[]= " {$key} in('" . implode("','", $val) . "') ";
	        }
	        else
	        {
	            $arr_where[]= " {$key}='{$val}' ";
	        }
	    }
	    $sql="select * from " . self::$base_table . " where " . implode(" and ", $arr_where) . " order by nns_priority desc,nns_create_time asc  {$str_limit} ";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (is_array($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 添加切片信息
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function add($dc,$params)
	{
		if(!isset($params['nns_id']) || strlen($params['nns_id'])<1)
		{
			return self::return_data(1,'clip task id 为空');
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$date_time = date("Y-m-d H:i:s");
		if(!isset($params['nns_alive_time']) || empty($params['nns_alive_time']))
		{
			$params['nns_alive_time'] = $date_time;
		}
		if(!isset($params['nns_start_time']) || empty($params['nns_start_time']))
		{
			$params['nns_start_time'] = $date_time;
		}
		if(!isset($params['nns_end_time']) || empty($params['nns_end_time']))
		{
			$params['nns_end_time'] = $date_time;
		}
		if(!isset($params['nns_create_time']) || empty($params['nns_create_time']))
		{
			$params['nns_create_time'] = $date_time;
		}
		if(!isset($params['nns_modify_time']) || empty($params['nns_modify_time']))
		{
			$params['nns_modify_time'] = $date_time;
		}
		$sql = self::make_insert_sql(self::$base_table, $params);
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 检查数据是否存在
	 * @param object $dc
	 * @param string $media_id 片源id
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function check_exsist($dc,$media_id)
	{
		$sql="select nns_id from " . self::$base_table . " where nns_video_media_id='{$media_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result[0]['nns_id']) && !empty($result[0]['nns_id'])) ? $result[0]['nns_id'] : null;
		return self::return_data(0,'OK',$result);
	}
	
	
	/**
	 * 修改CP
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function edit($dc,$params,$nns_id)
	{
		if((!is_string($nns_id) && !is_array($nns_id)) || (is_string($nns_id) && strlen($nns_id)<0) || (is_array($nns_id) && empty($nns_id)))
		{
			return self::return_data(1,'CP_id为空');
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(!isset($params['nns_modify_time']) || empty($params['nns_modify_time']))
		{
			$params['nns_modify_time'] = date("Y-m-d H:i:s");
		}
		if(empty($params))
		{
		    return self::return_data(1,'参数为空');
		}
		$sql = self::make_update_sql(self::$base_table, $params,array('nns_id'=>$nns_id));
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 检查切片队列某状态消息等待的数据条数
	 * @param unknown $dc
	 * @param unknown $sp_id
	 * @param unknown $state
	 */
	static public function check_state_num_by_sp($dc,$sp_id,$state)
	{
	    if(strlen($sp_id) <1)
	    {
	        return self::return_data(1,'SP_id为空');
	    }
	    if(strlen($state) <1)
	    {
	        return self::return_data(1,'状态为空');
	    }
	    $sql="select count(*) as total from " . self::$base_table . " where nns_org_id='{$sp_id}' and nns_state='{$state}' ";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result[0]['total']) && $result[0]['total'] >0) ? $result[0]['total'] : 0;
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 获取状态sp的数据条数
	 * @param unknown $dc
	 * @param unknown $sp_id
	 * @param unknown $state
	 * @param unknown $num
	 */
	static public function get_info_by_state_sp($dc,$sp_id,$state,$num=null)
	{
	    if(strlen($sp_id) <1)
	    {
	        return self::return_data(1,'SP_id为空');
	    }
	    if(strlen($state) <1)
	    {
	        return self::return_data(1,'状态为空');
	    }
	    $str_limit = (isset($num) && strlen($num) >0) ? "limit {$num}" : '';
	    $sql="select * from " . self::$base_table . " where nns_org_id='{$sp_id}' and nns_state='{$state}' order by nns_priority desc,nns_create_time asc {$str_limit} ";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 
	 * @param unknown $dc
	 * @param unknown $sp_id
	 * @param string $flag
	 */
	static public function get_max_priority($dc,$sp_id)
	{
	    if(strlen($sp_id) <1)
	    {
	        return self::return_data(1,'SP_id为空');
	    }
	    $sql="select max(nns_priority) as max_priority from " . self::$base_table . " where  (ISNULL(nns_state) or nns_state='' or nns_state='add')  and nns_org_id='{$sp_id}' limit 1";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result[0]) && !empty($result[0])) ? (int)$result[0]['max_priority'] : 0;
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 获取等待切片的数据
	 * @param unknown $dc
	 * @param unknown $sp_id
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 */
	static public function timer_get_wait_clip_data($dc,$sp_id)
	{
	    $sql = "select count(*) as count from " . self::$base_table . " where (ISNULL(nns_state) or nns_state='' or nns_state='add') and nns_org_id='{$sp_id}' ";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result[0]) && !empty($result[0])) ? (int)$result[0]['count'] : 0;
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function delete($dc,$nns_id)
	{
	    if(strlen($nns_id) <1)
	    {
	        return self::return_data(1,'nns_id为空');
	    }
	    $sql = "delete from " . self::$base_table . " where nns_id='{$nns_id}' ";
	    $result = nl_execute_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    return self::return_data(0,'OK');
	}

    /**
     * 依据条件查询切片队列信息
     * @param $dc
     * @param $sql
     * @return array
     */
    static public function query_by_sql($dc, $sql)
    {
        $result = nl_query_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (isset($result) && !empty($result)) ? $result : null;
        return self::return_data(0, 'OK' . $sql, $result);
    }
}
