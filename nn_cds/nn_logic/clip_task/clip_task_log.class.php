<?php
/**
 * CP管理
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_clip_task_log extends nl_public
{
	static $base_table='nns_mgtvbk_clip_task_log';
	static $arr_filed = array(
			'nns_id',
			'nns_task_id',
			'nns_state',
			'nns_desc',
			'nns_create_time',
			'nns_modify_time',
			'nns_video_type',
			'nns_video_id',
			'nns_video_index_id',
			'nns_org_id',
			'nns_create_mic_time',
	);
	
	
	
	/**
	 * 添加切片日志信息
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function add($dc,$params)
	{
		if(!isset($params['nns_id']) || strlen($params['nns_id'])<1)
		{
			$params['nns_id'] = np_guid_rand();
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$date_time = date("Y-m-d H:i:s");
		if(!isset($params['nns_create_time']) || empty($params['nns_create_time']))
		{
			$params['nns_create_time'] = $date_time;
		}
		if(!isset($params['nns_modify_time']) || empty($params['nns_modify_time']))
		{
			$params['nns_modify_time'] = $date_time;
		}
		$sql = self::make_insert_sql(self::$base_table, $params);
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
}
